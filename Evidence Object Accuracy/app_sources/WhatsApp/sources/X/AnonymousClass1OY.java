package X;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape5S0100000_I0_5;
import com.facebook.redex.ViewOnClickCListenerShape1S0100000_I0_1;
import com.whatsapp.Conversation;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.biz.product.view.activity.ProductDetailActivity;
import com.whatsapp.conversation.conversationrow.ConversationRow$ConversationRowDialogFragment;
import com.whatsapp.conversation.conversationrow.DynamicButtonsLayout;
import com.whatsapp.conversation.conversationrow.InteractiveMessageView;
import com.whatsapp.conversation.conversationrow.TemplateQuickReplyButtonsLayout;
import com.whatsapp.jid.UserJid;
import com.whatsapp.preference.WaFontListPreference;
import com.whatsapp.reactions.ReactionsBottomSheetDialogFragment;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape14S0100000_I0_1;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/* renamed from: X.1OY */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1OY extends AnonymousClass1OZ implements AbstractC28571Oc {
    public static float A1f;
    public static float A1g;
    public int A00 = 0;
    public int A01;
    public int A02;
    public View A03;
    public View A04;
    public ViewGroup A05;
    public ViewGroup A06;
    public ViewGroup A07;
    public FrameLayout A08;
    public ImageView A09;
    public ImageView A0A;
    public ImageView A0B;
    public LinearLayout A0C;
    public TextView A0D;
    public TextView A0E;
    public TextView A0F;
    public TextView A0G;
    public AnonymousClass04v A0H;
    public AnonymousClass12P A0I;
    public C14900mE A0J;
    public AnonymousClass18U A0K;
    public C15570nT A0L;
    public C239613r A0M;
    public C15450nH A0N;
    public C18790t3 A0O;
    public TextEmojiLabel A0P;
    public TextEmojiLabel A0Q;
    public C16170oZ A0R;
    public C19850um A0S;
    public AnonymousClass19Q A0T;
    public C253619c A0U;
    public C238013b A0V;
    public C22640zP A0W;
    public C15550nR A0X;
    public C22700zV A0Y;
    public C15610nY A0Z;
    public C21270x9 A0a;
    public C253318z A0b;
    public AnonymousClass1AO A0c;
    public AnonymousClass19K A0d;
    public C52512ay A0e;
    public C50382Pi A0f;
    public AnonymousClass38S A0g;
    public AnonymousClass19P A0h;
    public C17000q6 A0i;
    public C18640sm A0j;
    public C14830m7 A0k;
    public C17170qN A0l;
    public C14820m6 A0m;
    public C19990v2 A0n;
    public C20830wO A0o;
    public C15650ng A0p;
    public C242814x A0q;
    public C15600nX A0r;
    public C20040v7 A0s;
    public AnonymousClass1BK A0t;
    public C22440z5 A0u;
    public C21250x7 A0v;
    public C18470sV A0w;
    public AnonymousClass132 A0x;
    public C21400xM A0y;
    public C15670ni A0z;
    public AnonymousClass19M A10;
    public C20710wC A11;
    public C14410lO A12;
    public AnonymousClass13H A13;
    public C22910zq A14;
    public C17220qS A15;
    public AnonymousClass19J A16;
    public C26151Cf A17;
    public AnonymousClass18T A18;
    public C22710zW A19;
    public C17070qD A1A;
    public C21190x1 A1B;
    public C63873Dg A1C;
    public C16630pM A1D;
    public AnonymousClass12V A1E;
    public C18170s1 A1F;
    public C18120rw A1G;
    public C26701Em A1H;
    public C240514a A1I;
    public AnonymousClass12F A1J;
    public AnonymousClass1AB A1K;
    public C26671Ej A1L;
    public C252018m A1M;
    public C23000zz A1N;
    public AnonymousClass19O A1O;
    public AbstractC14440lR A1P;
    public AnonymousClass19Z A1Q;
    public C237512w A1R;
    public Runnable A1S;
    public Runnable A1T;
    public boolean A1U;
    public boolean A1V = false;
    public boolean A1W = false;
    public boolean A1X = true;
    public final View.OnClickListener A1Y = new ViewOnClickCListenerShape1S0100000_I0_1(this, 34);
    public final View.OnKeyListener A1Z = new View.OnKeyListener() { // from class: X.3Me
        @Override // android.view.View.OnKeyListener
        public final boolean onKey(View view, int i, KeyEvent keyEvent) {
            AnonymousClass1OY r5 = AnonymousClass1OY.this;
            if (i != 23) {
                return false;
            }
            if (keyEvent.isLongPress()) {
                r5.A0r();
                return true;
            }
            AbstractC13890kV r0 = ((AbstractC28551Oa) r5).A0a;
            if (r0 == null || !r0.AIM() || keyEvent.getAction() != 1 || keyEvent.getEventTime() - keyEvent.getDownTime() > 500) {
                return false;
            }
            r5.A1B(r5.getFMessage());
            return true;
        }
    };
    public final View.OnLongClickListener A1a = new View.OnLongClickListener() { // from class: X.4n1
        @Override // android.view.View.OnLongClickListener
        public final boolean onLongClick(View view) {
            AnonymousClass1OY.this.A0r();
            return true;
        }
    };
    public final AnonymousClass3CO A1b = new AnonymousClass3CO(this);
    public final AnonymousClass4KN A1c = new AnonymousClass4KN(this);
    public final AnonymousClass4KO A1d = new AnonymousClass4KO(this);
    public final Runnable A1e = new RunnableBRunnable0Shape5S0100000_I0_5(this, 4);

    public void A0y() {
    }

    public boolean A1I() {
        return true;
    }

    public int getMessageCount() {
        return 1;
    }

    public AnonymousClass1OY(Context context, AbstractC13890kV r14, AbstractC15340mz r15) {
        super(context, r14, r15);
        AnonymousClass018 r7;
        Rect rect;
        int i;
        int i2;
        int i3;
        int i4;
        if (r14 != null) {
            super.A0R = r14.ABe() == 2;
        }
        setClipToPadding(false);
        setClipChildren(false);
        int A01 = super.A0b.A01(context);
        int A00 = C27531Hw.A00(context);
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.normal_bubble_vertical_margin);
        if (super.A0R) {
            int dimensionPixelSize2 = getContext().getResources().getDimensionPixelSize(R.dimen.album_item_padding);
            setPadding(0, dimensionPixelSize2, 0, dimensionPixelSize2);
        } else if (r15.A0C != 6 || r15.A0y == 8) {
            if (A0g()) {
                r7 = super.A0K;
                Rect rect2 = AbstractC28551Oa.A0f;
                i = rect2.left;
                i2 = rect2.top + dimensionPixelSize;
                i3 = rect2.right;
                i4 = rect2.bottom;
            } else {
                boolean z = r15.A0z.A02;
                r7 = super.A0K;
                if (z) {
                    rect = AbstractC28551Oa.A0h;
                } else {
                    rect = AbstractC28551Oa.A0g;
                }
                i = rect.left + A00;
                i2 = rect.top + dimensionPixelSize;
                i3 = A00 + rect.right;
                i4 = rect.bottom;
            }
            C42941w9.A0A(this, r7, i, i2, i3, A01 + i4);
            setMinimumHeight((int) getResources().getDimension(R.dimen.conversation_row_min_height));
        } else {
            Rect rect3 = AbstractC28551Oa.A0g;
            setPadding(A00, (dimensionPixelSize + rect3.top) - getResources().getDimensionPixelSize(R.dimen.date_balloon_alt_padding_top), A00, (A01 + rect3.bottom) - getResources().getDimensionPixelSize(R.dimen.date_balloon_alt_padding_top));
        }
        setBackground(new C73073fd(this));
        this.A0E = (TextView) findViewById(R.id.date);
        this.A0B = (ImageView) findViewById(R.id.status);
        this.A03 = !super.A0R ? findViewById(R.id.name_in_group) : null;
        this.A05 = (ViewGroup) findViewById(R.id.date_wrapper);
        A1H(false);
        if (A1J()) {
            setLongClickable(true);
            setOnLongClickListener(this.A1a);
        }
        AbstractC13890kV r0 = super.A0a;
        if (r0 == null || !r0.AIM() || !A1I()) {
            View view = super.A0E;
            if (view != null) {
                view.setVisibility(8);
            }
            C52512ay r02 = this.A0e;
            if (r02 != null) {
                r02.setRowSelected(false);
            }
        } else {
            A0u();
            C52512ay r1 = this.A0e;
            AnonymousClass009.A05(r14);
            r1.setRowSelected(r14.AJm(r15));
        }
        this.A1C = new C63873Dg();
    }

    public static float A00(Resources resources) {
        float f = A1f;
        if (f == 0.0f) {
            f = resources.getDimension(R.dimen.conversation_divider_row_tv) / resources.getDisplayMetrics().scaledDensity;
            A1f = f;
        }
        int i = 0;
        int i2 = WaFontListPreference.A00;
        if (i2 == -1) {
            i = -2;
        } else if (i2 == 1) {
            i = 4;
        }
        return f + ((float) i);
    }

    public static float A01(Resources resources, AnonymousClass018 r2) {
        return (A02(resources, r2, WaFontListPreference.A00) * 24.0f) / 27.0f;
    }

    public static float A02(Resources resources, AnonymousClass018 r3, int i) {
        if (A1g == 0.0f) {
            A1g = resources.getDimension(R.dimen.conversation_text_row_tv) / resources.getDisplayMetrics().scaledDensity;
        }
        int i2 = 0;
        if (i == -1) {
            i2 = -2;
        } else if (i == 1) {
            i2 = 4;
        }
        if (r3.A06().equals("ar") || r3.A06().equals("fa")) {
            i2++;
        }
        return A1g + ((float) i2);
    }

    public static int A03(AbstractC28551Oa r1) {
        if (TextUtils.isEmpty(((AbstractC16130oV) r1.A0O).A15())) {
            return R.drawable.broadcast_status_icon_onmedia;
        }
        return R.drawable.broadcast_status_icon;
    }

    public static int A04(AbstractC28551Oa r0, DynamicButtonsLayout dynamicButtonsLayout, int i) {
        return i + dynamicButtonsLayout.A01(r0.A0D.getMeasuredWidth());
    }

    public static int A05(AbstractC28551Oa r2, TemplateQuickReplyButtonsLayout templateQuickReplyButtonsLayout) {
        return r2.getMeasuredHeight() + templateQuickReplyButtonsLayout.A00(r2.A0D.getMeasuredWidth());
    }

    public static Drawable A06(View view) {
        Drawable A04 = AnonymousClass00T.A04(view.getContext(), R.drawable.ic_ephemeral_v2);
        AnonymousClass009.A05(A04);
        return AnonymousClass2GE.A04(A04, AnonymousClass00T.A00(view.getContext(), R.color.conversationRowEphemeralIconTint));
    }

    public static AnonymousClass2P6 A07(AbstractC28561Ob r0) {
        return (AnonymousClass2P6) ((AnonymousClass2P5) r0.generatedComponent());
    }

    public static AnonymousClass01J A08(AnonymousClass2P6 r1, AbstractC28551Oa r2) {
        AnonymousClass01J r12 = r1.A06;
        r2.A0L = (C14850m9) r12.A04.get();
        r2.A0P = (AnonymousClass1CY) r12.ABO.get();
        r2.A0F = (AbstractC15710nm) r12.A4o.get();
        r2.A0N = (C244415n) r12.AAg.get();
        r2.A0J = (AnonymousClass01d) r12.ALI.get();
        r2.A0K = (AnonymousClass018) r12.ANb.get();
        r2.A0M = (C22050yP) r12.A7v.get();
        r2.A0G = (AnonymousClass19I) r12.A4a.get();
        return r12;
    }

    public static C18640sm A09(AnonymousClass01J r1, AnonymousClass1OY r2, Object obj) {
        r2.A0Y = (C22700zV) obj;
        r2.A0m = (C14820m6) r1.AN3.get();
        r2.A0W = (C22640zP) r1.A3Z.get();
        r2.A19 = (C22710zW) r1.AF7.get();
        r2.A0T = (AnonymousClass19Q) r1.A2u.get();
        r2.A1K = (AnonymousClass1AB) r1.AKI.get();
        r2.A18 = (AnonymousClass18T) r1.AE9.get();
        r2.A0r = (C15600nX) r1.A8x.get();
        r2.A0u = (C22440z5) r1.AG6.get();
        r2.A1D = (C16630pM) r1.AIc.get();
        return (C18640sm) r1.A3u.get();
    }

    public static AbstractView$OnClickListenerC34281fs A0A(TextView textView, AbstractC42671vd r5, AbstractC16130oV r6) {
        r5.A16(textView, Collections.singletonList(r6), r6.A01);
        textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_download, 0, 0, 0);
        AbstractView$OnClickListenerC34281fs r0 = r5.A08;
        textView.setOnClickListener(r0);
        return r0;
    }

    public static Object A0B(AnonymousClass01J r1, AnonymousClass1OY r2) {
        r2.A1F = r1.A41();
        r2.A1E = (AnonymousClass12V) r1.A0p.get();
        r2.A1I = (C240514a) r1.AJL.get();
        r2.A1O = (AnonymousClass19O) r1.ACO.get();
        r2.A17 = (C26151Cf) r1.ADi.get();
        r2.A1H = (C26701Em) r1.ABc.get();
        r2.A0x = (AnonymousClass132) r1.ALx.get();
        r2.A0S = (C19850um) r1.A2v.get();
        r2.A0y = (C21400xM) r1.ABd.get();
        r2.A0z = (C15670ni) r1.AIb.get();
        r2.A1N = (C23000zz) r1.ALg.get();
        return r1.AMN.get();
    }

    public static void A0D(Context context, View view, TextView textView) {
        textView.setBackgroundResource(R.drawable.date_balloon);
        textView.setCompoundDrawablePadding(context.getResources().getDimensionPixelSize(R.dimen.conversation_row_padding));
        textView.setTextSize(A00(view.getResources()));
    }

    public static void A0E(Intent intent, View view, View view2, Object obj) {
        AbstractC454421p.A08(view.getContext(), intent, view2, new AnonymousClass2TT(view.getContext()), AbstractC42671vd.A0Z(obj.toString()));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0018, code lost:
        r8 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001d, code lost:
        if (((X.C42051ua) r0).A04 == 1) goto L_0x0020;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001f, code lost:
        r8 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0020, code lost:
        r11.A0c.A00(java.lang.Boolean.valueOf(r14), java.lang.Boolean.valueOf(r8), 3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0030, code lost:
        if (r10 == null) goto L_0x005a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0032, code lost:
        r0 = r10.second;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0034, code lost:
        if (r0 == null) goto L_0x005a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0036, code lost:
        r4 = ((X.C42051ua) r0).A0C;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003a, code lost:
        r0 = X.C248917h.A04(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x003e, code lost:
        if (r0 == null) goto L_0x0041;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0040, code lost:
        r5 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0041, code lost:
        r2 = com.whatsapp.PhoneHyperLinkDialogFragment.A00(r4, r5, r13, r7, r8, r14);
        r1 = (X.ActivityC13810kN) X.AnonymousClass12P.A00(r11.getContext());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0054, code lost:
        if (r1.AJN() != false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0056, code lost:
        r1.Adl(r2, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0059, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x005a, code lost:
        r4 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000f, code lost:
        if ((!((X.C42351v4) r10.first).A00()) != false) goto L_0x0011;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0012, code lost:
        if (r10 != null) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0014, code lost:
        r0 = r10.second;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0016, code lost:
        if (r0 == null) goto L_0x001f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ void A0F(android.util.Pair r10, X.AnonymousClass1OY r11, java.lang.String r12, java.lang.String r13, boolean r14) {
        /*
            r5 = r12
            r1 = 1
            if (r10 == 0) goto L_0x0011
            java.lang.Object r0 = r10.first
            X.1v4 r0 = (X.C42351v4) r0
            boolean r0 = r0.A00()
            r0 = r0 ^ 1
            r7 = 0
            if (r0 == 0) goto L_0x0014
        L_0x0011:
            r7 = 1
            if (r10 == 0) goto L_0x001f
        L_0x0014:
            java.lang.Object r0 = r10.second
            if (r0 == 0) goto L_0x001f
            X.1ua r0 = (X.C42051ua) r0
            int r0 = r0.A04
            r8 = 1
            if (r0 == r1) goto L_0x0020
        L_0x001f:
            r8 = 0
        L_0x0020:
            X.1AO r3 = r11.A0c
            r2 = 3
            r9 = r14
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r14)
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r8)
            r3.A00(r1, r0, r2)
            r3 = 0
            if (r10 == 0) goto L_0x005a
            java.lang.Object r0 = r10.second
            if (r0 == 0) goto L_0x005a
            X.1ua r0 = (X.C42051ua) r0
            com.whatsapp.jid.UserJid r4 = r0.A0C
        L_0x003a:
            java.lang.String r0 = X.C248917h.A04(r4)
            if (r0 == 0) goto L_0x0041
            r5 = r0
        L_0x0041:
            r6 = r13
            com.whatsapp.PhoneHyperLinkDialogFragment r2 = com.whatsapp.PhoneHyperLinkDialogFragment.A00(r4, r5, r6, r7, r8, r9)
            android.content.Context r0 = r11.getContext()
            android.app.Activity r1 = X.AnonymousClass12P.A00(r0)
            X.0kN r1 = (X.ActivityC13810kN) r1
            boolean r0 = r1.AJN()
            if (r0 != 0) goto L_0x0059
            r1.Adl(r2, r3)
        L_0x0059:
            return
        L_0x005a:
            r4 = r3
            goto L_0x003a
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1OY.A0F(android.util.Pair, X.1OY, java.lang.String, java.lang.String, boolean):void");
    }

    public static void A0G(View view, AbstractC28551Oa r6) {
        View view2 = r6.A0D;
        view.layout(view2.getLeft(), view2.getBottom(), view2.getRight(), view.getMeasuredHeight() + view2.getBottom());
    }

    public static void A0H(View view, AnonymousClass1OY r2, AbstractC15340mz r3, Object obj) {
        AnonymousClass028.A0k(view, AbstractC42671vd.A0Z(obj.toString()));
        AnonymousClass028.A0k(r2.A0E, AbstractC42671vd.A0Y(r3));
        ImageView imageView = r2.A0B;
        if (imageView != null) {
            StringBuilder sb = new StringBuilder("status-transition-");
            sb.append(r3.A0z);
            AnonymousClass028.A0k(imageView, sb.toString());
        }
    }

    public static void A0I(AnonymousClass2P6 r1, AnonymousClass01J r2, AnonymousClass1OY r3, C18640sm r4) {
        r3.A0j = r4;
        r3.A1L = (C26671Ej) r2.AKR.get();
        r3.A1G = r2.A42();
        r3.A0o = (C20830wO) r2.A4W.get();
        r3.A0q = (C242814x) r2.A71.get();
        r3.A0d = (AnonymousClass19K) r2.AFh.get();
        r3.A16 = (AnonymousClass19J) r2.ACA.get();
        r3.A1R = (C237512w) r2.AAD.get();
        r3.A0c = (AnonymousClass1AO) r2.AFg.get();
        r3.A0l = (C17170qN) r2.AMt.get();
        r3.A0i = (C17000q6) r2.ACv.get();
        r3.A1B = (C21190x1) r2.A3G.get();
        r3.A0f = r1.A02();
    }

    public static void A0J(AnonymousClass2P6 r1, AnonymousClass01J r2, C60852yk r3) {
        r3.A02 = (C14850m9) r2.A04.get();
        r3.A00 = (C14330lG) r2.A7B.get();
        r3.A03 = (AnonymousClass1CH) r2.AAy.get();
        r3.A01 = r1.A01();
    }

    public static void A0K(AnonymousClass01J r1, AnonymousClass1OY r2) {
        r2.A0U = (C253619c) r1.AId.get();
        r2.A0Z = (C15610nY) r1.AMe.get();
        r2.A1M = (C252018m) r1.A7g.get();
        r2.A1A = (C17070qD) r1.AFC.get();
        r2.A0t = (AnonymousClass1BK) r1.AFZ.get();
        r2.A0b = (C253318z) r1.A4B.get();
        r2.A0p = (C15650ng) r1.A4m.get();
        r2.A0V = (C238013b) r1.A1Z.get();
        r2.A11 = (C20710wC) r1.A8m.get();
        r2.A14 = (C22910zq) r1.A9O.get();
        r2.A1J = (AnonymousClass12F) r1.AJM.get();
    }

    public static void A0L(AnonymousClass01J r1, AnonymousClass1OY r2) {
        r2.A0k = (C14830m7) r1.ALb.get();
        r2.A0J = (C14900mE) r1.A8X.get();
        r2.A13 = (AnonymousClass13H) r1.ABY.get();
        r2.A1P = (AbstractC14440lR) r1.ANe.get();
        r2.A0L = (C15570nT) r1.AAr.get();
        r2.A0h = (AnonymousClass19P) r1.ACQ.get();
        r2.A0M = (C239613r) r1.AI9.get();
        r2.A0O = (C18790t3) r1.AJw.get();
        r2.A0n = (C19990v2) r1.A3M.get();
        r2.A10 = (AnonymousClass19M) r1.A6R.get();
        r2.A0N = (C15450nH) r1.AII.get();
    }

    public static void A0M(AnonymousClass01J r1, AnonymousClass1OY r2) {
        r2.A0v = (C21250x7) r1.AJh.get();
        r2.A0w = (C18470sV) r1.AK8.get();
        r2.A0R = (C16170oZ) r1.AM4.get();
        r2.A1Q = (AnonymousClass19Z) r1.A2o.get();
        r2.A0K = (AnonymousClass18U) r1.AAU.get();
        r2.A12 = (C14410lO) r1.AB3.get();
        r2.A0I = (AnonymousClass12P) r1.A0H.get();
        r2.A0a = (C21270x9) r1.A4A.get();
        r2.A0s = (C20040v7) r1.AAK.get();
        r2.A15 = (C17220qS) r1.ABt.get();
        r2.A0X = (C15550nR) r1.A45.get();
    }

    public static void A0N(AnonymousClass01J r1, C60752yZ r2) {
        r2.A00 = C18000rk.A00(r1.ADw);
        r2.A02 = C18000rk.A00(r1.AID);
        r2.A01 = C18000rk.A00(r1.AGl);
    }

    public static void A0O(AnonymousClass01J r1, AbstractC42671vd r2) {
        r2.A01 = (C15890o4) r1.AN1.get();
        r2.A05 = (C22370yy) r1.AB0.get();
        r2.A06 = (C26161Cg) r1.AB4.get();
        r2.A04 = (AnonymousClass109) r1.AI8.get();
        r2.A03 = r1.A2e();
        r2.A02 = (C26171Ch) r1.A2a.get();
    }

    public static void A0P(C16150oX r2, AbstractC16130oV r3, StringBuilder sb, boolean z) {
        sb.append(z);
        sb.append(" type:");
        sb.append((int) r3.A0y);
        sb.append(" url:");
        sb.append(C37611mi.A00(r3.A08));
        sb.append(" file:");
        sb.append(r2.A0F);
        sb.append(" progress:");
        sb.append(r2.A0C);
        sb.append(" transferred:");
        sb.append(r2.A0P);
        sb.append(" transferring:");
        sb.append(r2.A0a);
        sb.append(" fileSize:");
        sb.append(r2.A0A);
        sb.append(" media_size:");
        sb.append(r3.A01);
        sb.append(" timestamp:");
        sb.append(r3.A0I);
        Log.i(sb.toString());
    }

    public static void A0Q(TextEmojiLabel textEmojiLabel, AnonymousClass1OY r4, String str) {
        SpannableString valueOf = SpannableString.valueOf(Html.fromHtml(str));
        r4.A12(valueOf);
        textEmojiLabel.setAccessibilityHelper(new AnonymousClass2eq(textEmojiLabel, ((AbstractC28551Oa) r4).A0J));
        textEmojiLabel.setText(valueOf);
    }

    public static void A0R(AnonymousClass1OY r1, InteractiveMessageView interactiveMessageView, AbstractC15340mz r3) {
        interactiveMessageView.setLayoutView(!r3.A0z.A02 ? 1 : 0);
        View.OnLongClickListener onLongClickListener = r1.A1a;
        interactiveMessageView.setOnLongClickListener(onLongClickListener);
        interactiveMessageView.A05.setOnLongClickListener(onLongClickListener);
    }

    public static void A0S(AbstractC42671vd r3, AnonymousClass1IS r4) {
        Log.w("viewmessage/ no file");
        if (r3.A1O()) {
            return;
        }
        if (((AbstractC28551Oa) r3).A0b.A08()) {
            ActivityC13810kN r1 = (ActivityC13810kN) AbstractC35731ia.A01(r3.getContext(), ActivityC13810kN.class);
            if (r1 != null) {
                ((AbstractC28551Oa) r3).A0P.A01(r1);
                return;
            }
            return;
        }
        r3.getContext().startActivity(C14960mK.A0D(r3.getContext(), r4.A00, r4.hashCode()));
    }

    public static boolean A0T(AbstractC28551Oa r0) {
        return TextUtils.isEmpty(((AbstractC16130oV) r0.A0O).A15());
    }

    public static boolean A0U(AbstractC42671vd r2) {
        return RequestPermissionActivity.A0W(r2.getContext(), r2.A01);
    }

    public static boolean A0V(AbstractC15340mz r6, int i, boolean z) {
        AnonymousClass1IS r1 = r6.A0z;
        if (r1.A02 || !C15380n4.A0J(r1.A00)) {
            return false;
        }
        if (i <= 1 || r6.A0F != 0 || r6.A0y != 0 || C30041Vv.A0q(r6) || (C30051Vw.A19(r6) && !z)) {
            return true;
        }
        return false;
    }

    public int A0m(int i) {
        if (C37381mH.A00(i, 13) >= 0 || C37381mH.A00(i, 5) >= 0) {
            return R.drawable.message_got_receipt_from_target;
        }
        if (i == 4) {
            return R.drawable.message_got_receipt_from_server;
        }
        return R.drawable.message_unsent;
    }

    public int A0n(int i) {
        if (C37381mH.A00(i, 13) >= 0) {
            return R.color.msgStatusReadTint;
        }
        return R.color.msgStatusTint;
    }

    public Conversation A0o() {
        return (Conversation) AbstractC35731ia.A01(getContext(), Conversation.class);
    }

    public ReactionsBottomSheetDialogFragment A0p(AbstractC14640lm r4, C40481rf r5) {
        AnonymousClass009.A05(r4);
        ReactionsBottomSheetDialogFragment reactionsBottomSheetDialogFragment = new ReactionsBottomSheetDialogFragment();
        reactionsBottomSheetDialogFragment.A0D = r4;
        reactionsBottomSheetDialogFragment.A0E = r5;
        reactionsBottomSheetDialogFragment.A0H = false;
        reactionsBottomSheetDialogFragment.A0A = null;
        return reactionsBottomSheetDialogFragment;
    }

    public CharSequence A0q(CharSequence charSequence) {
        List highlightTerms;
        if (TextUtils.isEmpty(charSequence) || (highlightTerms = getHighlightTerms()) == null || highlightTerms.isEmpty()) {
            return charSequence;
        }
        return (CharSequence) AnonymousClass3J9.A00(getContext(), super.A0K, AnonymousClass3J9.A01, charSequence, highlightTerms, false).A00;
    }

    public void A0r() {
        AbstractC13890kV r1;
        if (A1J() && (r1 = super.A0a) != null && !r1.AIM()) {
            r1.AeE(getFMessage());
            A1A(getFMessage());
            AnonymousClass19J r2 = this.A16;
            r2.A01 = r2.A04.A00();
            r2.A02 = UUID.randomUUID().toString();
            r2.A00 = 1;
            r2.A03 = r2.A07.A00();
            AnonymousClass19J r12 = this.A16;
            if (r12.A01(1)) {
                r12.A00 = 2;
                r12.A00(0);
            }
        }
    }

    public void A0s() {
        A1H(false);
    }

    public void A0t() {
        if (this instanceof C60852yk) {
            C60852yk r1 = (C60852yk) this;
            AbstractC15340mz r4 = ((AbstractC28551Oa) r1).A0O;
            r1.A04 = true;
            AnonymousClass19O r2 = r1.A1O;
            AnonymousClass009.A05(r2);
            r2.A0B(r1.A0G, r4, r1.A0H, r4.A0z, false);
        } else if (this instanceof C60822yh) {
            C64853Hd r12 = ((C60822yh) this).A04;
            r12.A01 = true;
            AnonymousClass19O r22 = r12.A0J;
            AnonymousClass009.A05(r22);
            AbstractC16130oV r42 = r12.A00;
            r22.A0B(r12.A0H, r42, r12.A0I, r42.A0z, false);
        } else if (this instanceof C60632yL) {
            C60632yL r43 = (C60632yL) this;
            AbstractC15340mz r3 = ((AbstractC28551Oa) r43).A0O;
            r43.A02 = true;
            r43.A1O.A0D(r3);
            r43.A1O.A07(r43.A0I, r3, r43.A0J);
        } else if (this instanceof C60782yd) {
            C60782yd r13 = (C60782yd) this;
            AbstractC15340mz r44 = ((AbstractC28551Oa) r13).A0O;
            r13.A07 = true;
            AnonymousClass19O r23 = r13.A1O;
            AnonymousClass009.A05(r23);
            r23.A0B(r13.A06, r44, r13.A08, r44.A0z, false);
        } else if (this instanceof C60762yb) {
            C60762yb r14 = (C60762yb) this;
            C16460p3 A0G = ((AbstractC16130oV) ((AbstractC28551Oa) r14).A0O).A0G();
            AnonymousClass009.A05(A0G);
            if (A0G.A04()) {
                AnonymousClass19O r24 = r14.A1O;
                AnonymousClass009.A05(r24);
                AbstractC15340mz r45 = ((AbstractC28551Oa) r14).A0O;
                r24.A0A(r14.A08, r45, r14.A0H, r45.A0z, 480, false, false);
            }
        }
    }

    public void A0u() {
        View view = super.A0E;
        if (view != null) {
            view.setVisibility(0);
            super.A0E.bringToFront();
            return;
        }
        C52512ay r1 = new C52512ay(getContext(), this);
        this.A0e = r1;
        super.A0E = r1;
        r1.setClickable(true);
        super.A0E.setOnClickListener(new ViewOnClickCListenerShape1S0100000_I0_1(this, 36));
        setClipToPadding(false);
        addView(super.A0E, new RelativeLayout.LayoutParams(-1, -1));
    }

    public void A0v() {
        UserJid A01;
        AbstractC15340mz fMessage = getFMessage();
        C30041Vv.A0C(fMessage);
        Conversation A0o = A0o();
        if (A0o != null) {
            if (C30041Vv.A0e(fMessage)) {
                A01 = ((C30511Xs) fMessage).A00;
            } else {
                AnonymousClass1IS r1 = fMessage.A0z;
                if (C15380n4.A0J(r1.A00) && (!r1.A02 || fMessage.A0C == 6)) {
                    A01 = C20710wC.A01(fMessage);
                }
                StringBuilder sb = new StringBuilder("conversation/getdialogitems/remote_resource is null! ");
                sb.append(C30041Vv.A0C(fMessage));
                Log.i(sb.toString());
            }
            if (A01 != null) {
                A0o.Adl(ConversationRow$ConversationRowDialogFragment.A00(A01), null);
                return;
            }
            StringBuilder sb = new StringBuilder("conversation/getdialogitems/remote_resource is null! ");
            sb.append(C30041Vv.A0C(fMessage));
            Log.i(sb.toString());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x009f, code lost:
        if (r0 == 0) goto L_0x014c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00bc, code lost:
        if (r0 == 0) goto L_0x014c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00d9, code lost:
        if (r0 == 0) goto L_0x014c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00f7, code lost:
        if (r0 == 0) goto L_0x014c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0128, code lost:
        if (r0 == 0) goto L_0x014c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x014a, code lost:
        if (r0 == 0) goto L_0x014c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x014c, code lost:
        r1 = com.whatsapp.R.color.media_message_progress_indeterminate;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x014f, code lost:
        r3.A0C = X.AnonymousClass00T.A00(r2, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0155, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0w() {
        /*
        // Method dump skipped, instructions count: 342
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1OY.A0w():void");
    }

    /* JADX DEBUG: Multi-variable search result rejected for r3v4, resolved type: X.2yk */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x01db, code lost:
        if ((!A0g()) != false) goto L_0x01dd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x01dd, code lost:
        r3 = r5.A0I;
        r2 = X.AnonymousClass4AT.FORWARD;
        r0 = 7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x0192, code lost:
        if (r0 == false) goto L_0x0194;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x01a4, code lost:
        if (r0 == false) goto L_0x01dd;
     */
    /* JADX WARNING: Removed duplicated region for block: B:108:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00b1  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0102  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0x() {
        /*
        // Method dump skipped, instructions count: 484
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1OY.A0x():void");
    }

    public final void A0z() {
        Handler handler = getHandler();
        if (handler == null) {
            Runnable runnable = this.A1e;
            removeCallbacks(runnable);
            post(runnable);
            return;
        }
        Runnable runnable2 = this.A1e;
        if (!handler.hasMessages(0, runnable2)) {
            Message obtain = Message.obtain(handler, runnable2);
            obtain.what = 0;
            obtain.obj = runnable2;
            obtain.sendToTarget();
        }
    }

    public void A10(int i) {
        int i2;
        Resources resources;
        int i3;
        super.A01 = i;
        View view = this.A03;
        int i4 = 0;
        if (view != null) {
            int i5 = 8;
            if (A0V(getFMessage(), i, super.A0R)) {
                i5 = 0;
            }
            view.setVisibility(i5);
        }
        int extraBottomPadding = getExtraBottomPadding();
        if (this instanceof C42441vD) {
            i4 = getResources().getDimensionPixelOffset(R.dimen.date_balloon_alt_padding_top);
            i2 = getResources().getDimensionPixelOffset(R.dimen.date_balloon_alt_padding_bottom);
        } else {
            i2 = 0;
        }
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int i6 = (int) ((getContext().getResources().getDisplayMetrics().density * 4.0f) / 3.0f);
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.normal_bubble_vertical_margin);
        int i7 = super.A01;
        if (i7 == 1) {
            if (this.A1V) {
                i4 = getResources().getDimensionPixelOffset(R.dimen.date_balloon_alt_padding_top);
            }
            setPadding(paddingLeft, dimensionPixelSize - i4, paddingRight, (i6 - i2) + extraBottomPadding);
        } else if (i7 == 2) {
            setPadding(paddingLeft, i6 - i4, paddingRight, (i6 - i2) + extraBottomPadding);
        } else if (i7 != 3) {
            if (this.A1V) {
                i4 = getResources().getDimensionPixelOffset(R.dimen.date_balloon_alt_padding_top);
            }
            setPadding(paddingLeft, dimensionPixelSize - i4, paddingRight, dimensionPixelSize - i2);
            resources = getResources();
            i3 = R.dimen.conversation_row_min_height;
            setMinimumHeight((int) resources.getDimension(i3));
        } else {
            setPadding(paddingLeft, i6 - i4, paddingRight, (dimensionPixelSize - i2) + extraBottomPadding);
        }
        resources = getResources();
        i3 = R.dimen.conversation_row_min_height_collapse_body;
        setMinimumHeight((int) resources.getDimension(i3));
    }

    public final void A11(int i) {
        boolean z = false;
        if (!A0i()) {
            C53182d3 r4 = super.A0H;
            if (r4 != null) {
                r4.A00(new C40481rf(this.A0L, Collections.emptyList()), false);
                return;
            }
            return;
        }
        if (super.A0H == null) {
            C53182d3 r2 = new C53182d3(getContext());
            super.A0H = r2;
            r2.setOnClickListener(new ViewOnClickCListenerShape14S0100000_I0_1(this, 11));
            addView(super.A0H);
            super.A0H.bringToFront();
        }
        C53182d3 r22 = super.A0H;
        C40481rf messageReactions = getMessageReactions();
        if (i == 28 || i == -1) {
            z = true;
        }
        r22.A00(messageReactions, z);
    }

    public void A12(Spannable spannable) {
        URLSpan[] uRLSpanArr = (URLSpan[]) spannable.getSpans(0, spannable.length(), URLSpan.class);
        for (URLSpan uRLSpan : uRLSpanArr) {
            int spanStart = spannable.getSpanStart(uRLSpan);
            int spanEnd = spannable.getSpanEnd(uRLSpan);
            spannable.removeSpan(uRLSpan);
            spannable.setSpan(new C58272oQ(getContext(), this.A0K, this.A0J, super.A0J, uRLSpan.getURL()), spanStart, spanEnd, 0);
        }
    }

    public final void A13(Spannable spannable, TextEmojiLabel textEmojiLabel, AbstractC15340mz r26, boolean z, boolean z2, boolean z3) {
        int i;
        int i2;
        String string;
        C58272oQ r15;
        int i3;
        int length;
        String A0B = this.A0m.A0B();
        C17070qD r4 = this.A1A;
        C22710zW r3 = this.A19;
        if (z3) {
            try {
                Linkify.addLinks(spannable, 2);
                C33771f3.A06(spannable);
                C63263Ax.A00(spannable, A0B);
                C63243Av.A00(spannable, r3, r4);
                C63253Aw.A00(spannable, r3, r4);
            } catch (Exception unused) {
            }
        }
        ArrayList A05 = C42971wC.A05(spannable);
        if (A05 == null || A05.isEmpty()) {
            i2 = 0;
            i = 0;
        } else {
            if (C65213Iq.A02(this.A0L, this.A0X, this.A0o, this.A0v, this.A11, r26)) {
                AnonymousClass1BK r7 = this.A0t;
                i = 0;
                Iterator it = A05.iterator();
                while (it.hasNext()) {
                    if (r7.A00(r26.A0C(), r26, ((URLSpan) it.next()).getURL()) != null) {
                        i++;
                    }
                }
                Iterator it2 = A05.iterator();
                i2 = 0;
                while (it2.hasNext()) {
                    URLSpan uRLSpan = (URLSpan) it2.next();
                    String url = uRLSpan.getURL();
                    int spanStart = spannable.getSpanStart(uRLSpan);
                    int spanEnd = spannable.getSpanEnd(uRLSpan);
                    int spanFlags = spannable.getSpanFlags(uRLSpan);
                    Set A00 = this.A0t.A00(r26.A0C(), r26, url);
                    if (A00 != null) {
                        r15 = new C58412p1(getContext(), this.A0K, this.A0J, super.A0J, url, A00);
                    } else if (url.startsWith("wapay") || url.startsWith("upi")) {
                        r15 = new C58272oQ(getContext(), this.A18, this.A0J, super.A0J, url);
                    } else if (!super.A0L.A07(1215) || !url.startsWith("tel")) {
                        r15 = new C58272oQ(getContext(), this.A0K, this.A0J, super.A0J, url);
                        if (r26 instanceof C28861Ph) {
                            r15.A01 = ((C28861Ph) r26).A00;
                            r15.A04 = true;
                        }
                        AbstractC14640lm r10 = r26.A0z.A00;
                        if (C15380n4.A0J(r10)) {
                            i3 = 3;
                        } else if (r10 instanceof UserJid) {
                            i3 = 2;
                        }
                        r15.A00 = i3;
                    } else {
                        r15 = new C58432p3(getContext(), this.A0K, this.A0J, this, super.A0J, r26, url);
                    }
                    C253619c r102 = this.A0U;
                    if (r102.A01.A07(355) && r102.A03.AKA() && r102.A00.A01(url)) {
                        r15.A02 = new AnonymousClass5TE() { // from class: X.52x
                            @Override // X.AnonymousClass5TE
                            public final void A7H() {
                                C253619c r42 = C253619c.this;
                                AnonymousClass30V r1 = new AnonymousClass30V();
                                r1.A01 = 4;
                                r1.A00 = 1;
                                r1.A02 = null;
                                r42.A02.A07(r1);
                            }
                        };
                    }
                    AnonymousClass36k[] r12 = (AnonymousClass36k[]) spannable.getSpans(spanStart, spanEnd, AnonymousClass36k.class);
                    if (r12 == null || (length = r12.length) <= 0) {
                        spannable.setSpan(r15, spanStart, spanEnd, spanFlags);
                        i2++;
                    } else {
                        r15.A05 = true;
                        int i4 = 0;
                        do {
                            r12[i4].A02 = true;
                            i4++;
                        } while (i4 < length);
                        spannable.setSpan(r15, spanStart, spanEnd, spanFlags);
                        i2++;
                    }
                }
            } else {
                i = 0;
                i2 = 0;
            }
            Iterator it3 = A05.iterator();
            while (it3.hasNext()) {
                spannable.removeSpan(it3.next());
            }
        }
        if (!z && i2 <= 0) {
            if (textEmojiLabel.A06 != null) {
                textEmojiLabel.setFocusable(false);
            }
            textEmojiLabel.setAccessibilityHelper(null);
        } else if (textEmojiLabel.A06 == null) {
            textEmojiLabel.setAccessibilityHelper(new AnonymousClass2eq(textEmojiLabel, super.A0J));
            textEmojiLabel.setFocusable(false);
        }
        if (i > 0) {
            if (this.A07 == null) {
                ViewGroup viewGroup = (ViewGroup) findViewById(R.id.suspicious_link_indicator_holder);
                this.A07 = viewGroup;
                if (viewGroup != null) {
                    LayoutInflater.from(getContext()).inflate(R.layout.suspicious_link_indicator, this.A07, true);
                    C27531Hw.A06((TextView) this.A07.findViewById(R.id.suspicious_link_indicator));
                }
            }
            ViewGroup viewGroup2 = this.A07;
            if (viewGroup2 != null) {
                viewGroup2.setVisibility(0);
                TextView textView = (TextView) this.A07.findViewById(R.id.suspicious_link_indicator);
                if (i2 > 1) {
                    string = super.A0K.A0I(new Object[]{Integer.valueOf(i)}, R.plurals.suspicious_links_label, (long) i);
                } else {
                    string = getContext().getString(R.string.suspicious_link_label);
                }
                textView.setText(string);
            }
            C53082ci r5 = super.A0I;
            if (r5 != null && AnonymousClass4AT.FORWARD == r5.A00) {
                r5.A01.setVisibility(8);
            }
        } else {
            ViewGroup viewGroup3 = this.A07;
            if (viewGroup3 != null) {
                viewGroup3.setVisibility(8);
            }
        }
        if (i2 > 0 || z2) {
            textEmojiLabel.setText(A0q(spannable), TextView.BufferType.SPANNABLE);
        }
    }

    public void A14(View view, AnonymousClass1XV r13, boolean z) {
        UserJid userJid = r13.A01;
        if (userJid == null) {
            this.A0J.A07(R.string.catalog_something_went_wrong_error, 0);
            return;
        }
        this.A0T.A00(3);
        AnonymousClass19Q r3 = this.A0T;
        int i = 18;
        int i2 = 33;
        if (z) {
            i = 17;
            i2 = 32;
        }
        r3.A03(userJid, Integer.valueOf(i2), r13.A06, i);
        ProductDetailActivity.A09(getContext(), view, this.A0S, r13, this.A1O, 1, z, this.A0L.A0F(userJid));
    }

    public final void A15(TextView textView, AbstractC15340mz r6, int i) {
        if (r6.A0r) {
            AnonymousClass1IS r1 = r6.A0z;
            if (r1.A02 && !C15380n4.A0F(r1.A00)) {
                int i2 = R.color.transparent;
                if (i == R.drawable.broadcast_status_icon) {
                    i2 = R.color.messageBroadcastColor;
                }
                Drawable A01 = AnonymousClass2GE.A01(getContext(), i, i2);
                if (super.A0K.A04().A06) {
                    textView.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, new AnonymousClass2GF(A01, super.A0K), (Drawable) null);
                    return;
                } else {
                    textView.setCompoundDrawablesWithIntrinsicBounds(A01, (Drawable) null, (Drawable) null, (Drawable) null);
                    return;
                }
            }
        }
        textView.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
    }

    public void A16(TextView textView, List list, long j) {
        long j2;
        C14430lQ A01;
        Iterator it = list.iterator();
        long j3 = 0;
        int i = 0;
        while (it.hasNext()) {
            AbstractC16130oV r0 = (AbstractC16130oV) it.next();
            C16150oX r9 = r0.A02;
            AnonymousClass009.A05(r9);
            C14410lO r10 = this.A12;
            long j4 = r0.A01;
            if (r9.A0X) {
                j2 = 0;
            } else {
                String str = r9.A0I;
                j2 = 0;
                if (!(str == null || (A01 = r10.A01(str)) == null)) {
                    j2 = A01.A0A;
                }
            }
            j3 += j4 - j2;
            if (this.A12.A01(r9.A0I) == null) {
                break;
            }
            i++;
        }
        if (i == list.size()) {
            textView.setText(C30041Vv.A0B(super.A0K, j3));
            return;
        }
        textView.setText(C30041Vv.A0B(super.A0K, j));
        this.A1P.Aaz(new AnonymousClass37X(textView, super.A0K, this.A12, list), new Void[0]);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0037, code lost:
        if (r5 != 1) goto L_0x0039;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00af, code lost:
        if (r0.A00 == Integer.MIN_VALUE) goto L_0x00b1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A17(com.whatsapp.TextEmojiLabel r20, X.AbstractC15340mz r21, java.lang.String r22, boolean r23, boolean r24) {
        /*
            r19 = this;
            r8 = r20
            android.content.Context r13 = r8.getContext()
            r6 = r19
            if (r24 == 0) goto L_0x0011
            float r0 = r6.getTextFontSize()
            r8.setTextSize(r0)
        L_0x0011:
            r4 = r22
            android.text.SpannableStringBuilder r7 = new android.text.SpannableStringBuilder
            r7.<init>(r4)
            if (r23 == 0) goto L_0x0021
            X.01d r1 = r6.A0J
            X.0pM r0 = r6.A1D
            X.C42971wC.A03(r1, r0, r7)
        L_0x0021:
            X.0kV r3 = r6.A0a
            r2 = 1
            r9 = r21
            if (r3 == 0) goto L_0x00d1
            int r5 = r3.AH8(r9)
            if (r5 != 0) goto L_0x002f
            r5 = 1
        L_0x002f:
            int r1 = r9.A05
            r0 = 127(0x7f, float:1.78E-43)
            if (r1 < r0) goto L_0x0039
            r1 = 308(0x134, float:4.32E-43)
            if (r5 == r2) goto L_0x003b
        L_0x0039:
            r1 = 768(0x300, float:1.076E-42)
        L_0x003b:
            X.1cV r0 = new X.1cV
            r0.<init>(r5, r1)
            android.text.TextPaint r14 = r8.getPaint()
            r18 = 1067869798(0x3fa66666, float:1.3)
            X.19M r15 = r6.A10
            r17 = r7
            r16 = r0
            X.AbstractC36671kL.A00(r13, r14, r15, r16, r17, r18)
            int r1 = r0.A02
            r12 = 0
            if (r1 <= 0) goto L_0x00cf
            int r0 = r7.length()
            if (r1 >= r0) goto L_0x00cf
            int r0 = r1 + -1
            int r0 = r4.codePointAt(r0)
            int r0 = java.lang.Character.charCount(r0)
            int r1 = r1 + r0
            int r1 = r1 - r2
            int r0 = r7.length()
            if (r1 == r0) goto L_0x00cf
            int r0 = r7.length()
            r7.delete(r1, r0)
            r0 = 2131891138(0x7f1213c2, float:1.9416988E38)
            java.lang.String r0 = r13.getString(r0)
            android.text.SpannableStringBuilder r4 = new android.text.SpannableStringBuilder
            r4.<init>(r0)
            X.3xU r2 = new X.3xU
            r2.<init>(r13, r6)
            int r1 = r4.length()
            r0 = 18
            r4.setSpan(r2, r12, r1, r0)
            java.lang.String r0 = "... "
            r7.append(r0)
            r7.append(r4)
            r10 = 1
        L_0x0097:
            X.13H r1 = r6.A13
            java.util.List r0 = r9.A0o
            r11 = 1
            r1.A02(r13, r7, r0)
            r4 = 0
            if (r3 == 0) goto L_0x00a6
            X.3DI r4 = r3.AAl()
        L_0x00a6:
            X.0DW r0 = r8.A06
            if (r0 == 0) goto L_0x00b1
            int r1 = r0.A00
            r0 = -2147483648(0xffffffff80000000, float:-0.0)
            r2 = 1
            if (r1 != r0) goto L_0x00b2
        L_0x00b1:
            r2 = 0
        L_0x00b2:
            if (r4 == 0) goto L_0x00ca
            r6.A13(r7, r8, r9, r10, r11, r12)
            X.1IS r1 = r9.A0z
            X.558 r0 = new X.558
            r0.<init>(r8, r6, r9, r10)
            r4.A00(r8, r0, r7, r1)
        L_0x00c1:
            if (r2 == 0) goto L_0x00c9
            r1 = 0
            r0 = 64
            r6.performAccessibilityAction(r0, r1)
        L_0x00c9:
            return
        L_0x00ca:
            r12 = 1
            r6.A13(r7, r8, r9, r10, r11, r12)
            goto L_0x00c1
        L_0x00cf:
            r10 = 0
            goto L_0x0097
        L_0x00d1:
            r5 = 0
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1OY.A17(com.whatsapp.TextEmojiLabel, X.0mz, java.lang.String, boolean, boolean):void");
    }

    public void A18(AbstractC14640lm r4, C40481rf r5) {
        if (r5 == null) {
            Log.e("ConversationRow/onReactionViewClicked null message reactions.");
            return;
        }
        ActivityC000900k r0 = (ActivityC000900k) AbstractC35731ia.A01(getContext(), AbstractActivityC13750kH.class);
        if (r0 != null) {
            A0p(r4, r5).A1F(r0.A0V(), "reactionsheet");
        }
    }

    public void A19(AbstractC15340mz r8) {
        TextView dateView = getDateView();
        ViewGroup dateWrapper = getDateWrapper();
        int i = 0;
        if (dateView != null) {
            dateView.setVisibility(0);
            dateView.setText(AnonymousClass3JK.A00(super.A0K, this.A0k.A02(r8.A0I)));
            A15(dateView, r8, getBroadcastDrawableId());
        }
        if (dateWrapper != null) {
            dateWrapper.setVisibility(0);
            if (!(this instanceof C60832yi)) {
                boolean z = r8.A0v;
                ImageView imageView = this.A0A;
                if (z) {
                    if (imageView == null) {
                        this.A0A = new ImageView(getContext());
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
                        layoutParams.gravity = 16;
                        ImageView imageView2 = this.A0A;
                        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.star_padding);
                        imageView2.setLayoutParams(layoutParams);
                        imageView2.setContentDescription(imageView2.getContext().getString(R.string.starred));
                        C42941w9.A08(imageView2, super.A0K, 0, dimensionPixelSize);
                        dateWrapper.addView(imageView2, 0);
                        dateWrapper.setClipChildren(false);
                        this.A0A.setImageDrawable(getStarDrawable());
                    }
                    imageView = this.A0A;
                } else if (imageView != null) {
                    i = 8;
                } else {
                    return;
                }
                imageView.setVisibility(i);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00da, code lost:
        r2 = r2.A05;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A1A(X.AbstractC15340mz r14) {
        /*
        // Method dump skipped, instructions count: 474
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1OY.A1A(X.0mz):void");
    }

    public void A1B(AbstractC15340mz r3) {
        AbstractC13890kV r0;
        if (A1J() && (r0 = super.A0a) != null) {
            this.A0e.setRowSelected(r0.Af1(r3));
        }
    }

    public void A1C(AbstractC15340mz r2, int i) {
        setFMessage(r2);
        A11(i);
        A10(super.A01);
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x0043 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A1D(X.AbstractC15340mz r4, boolean r5) {
        /*
            r3 = this;
            X.0mz r1 = r3.getFMessage()
            r0 = 0
            if (r1 == r4) goto L_0x0008
            r0 = 1
        L_0x0008:
            r3.setFMessage(r4)
            if (r0 != 0) goto L_0x000f
            if (r5 == 0) goto L_0x0012
        L_0x000f:
            r3.A1H(r0)
        L_0x0012:
            X.0kV r2 = r3.A0a
            if (r2 == 0) goto L_0x0034
            boolean r0 = r2.AIM()
            if (r0 == 0) goto L_0x0034
            boolean r0 = r3.A1I()
            if (r0 == 0) goto L_0x0034
            r3.A0u()
            X.2ay r1 = r3.A0e
            boolean r0 = r2.AJm(r4)
        L_0x002b:
            r1.setRowSelected(r0)
        L_0x002e:
            X.3Dg r0 = r3.A1C
            java.util.Set r1 = r0.A00
            monitor-enter(r1)
            goto L_0x0043
        L_0x0034:
            android.view.View r1 = r3.A0E
            if (r1 == 0) goto L_0x003d
            r0 = 8
            r1.setVisibility(r0)
        L_0x003d:
            X.2ay r1 = r3.A0e
            if (r1 == 0) goto L_0x002e
            r0 = 0
            goto L_0x002b
        L_0x0043:
            r1.clear()     // Catch: all -> 0x0048
            monitor-exit(r1)     // Catch: all -> 0x0048
            return
        L_0x0048:
            r0 = move-exception
            monitor-exit(r1)     // Catch: all -> 0x0048
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1OY.A1D(X.0mz, boolean):void");
    }

    public void A1E(AbstractC15340mz r2, boolean z) {
        A1D(r2, true);
    }

    public void A1F(boolean z) {
        ScaleAnimation scaleAnimation;
        if (!(this instanceof C60822yh) && this.A0A != null) {
            if (z) {
                scaleAnimation = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, 1, 0.5f, 1, 0.5f);
                scaleAnimation.setInterpolator(new OvershootInterpolator());
                this.A0A.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver$OnPreDrawListenerC101854oC(this));
            } else {
                scaleAnimation = new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, 1, 0.5f, 1, 0.5f);
                this.A0A.setVisibility(0);
                scaleAnimation.setAnimationListener(new C83223wt(this));
                scaleAnimation.setInterpolator(new AnticipateInterpolator());
            }
            scaleAnimation.setDuration(500);
            this.A0A.startAnimation(scaleAnimation);
        }
    }

    public void A1G(boolean z) {
        int i;
        this.A1V = z;
        TextView textView = this.A0D;
        if (z) {
            if (textView == null) {
                TextView textView2 = new TextView(getContext());
                this.A0D = textView2;
                textView2.setId(R.id.conversation_row_date_divider);
                this.A0D.setTypeface(C27531Hw.A03(getContext()));
                this.A0D.setTextColor(AnonymousClass00T.A00(getContext(), R.color.conversation_divider_text));
                this.A0D.setBackgroundResource(R.drawable.date_balloon_normal);
                this.A0D.setGravity(17);
                ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams(-2, -2);
                int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.conversation_date_divider_marginbottom);
                marginLayoutParams.bottomMargin = dimensionPixelSize;
                int dimensionPixelSize2 = dimensionPixelSize - getResources().getDimensionPixelSize(R.dimen.date_balloon_alt_padding_bottom);
                marginLayoutParams.bottomMargin = dimensionPixelSize2;
                if (this instanceof C42441vD) {
                    marginLayoutParams.bottomMargin = dimensionPixelSize2 - getResources().getDimensionPixelSize(R.dimen.date_balloon_alt_padding_top);
                }
                addView(this.A0D, marginLayoutParams);
                textView = this.A0D;
                super.A0C = textView;
            }
            textView.setText(C38131nZ.A09(super.A0K, getFMessage().A0I));
            this.A0D.setTextSize(A00(getResources()));
            textView = this.A0D;
            i = 0;
        } else if (textView != null) {
            i = 8;
        } else {
            return;
        }
        textView.setVisibility(i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0040, code lost:
        if (r4 != null) goto L_0x0042;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A1H(boolean r19) {
        /*
        // Method dump skipped, instructions count: 1672
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1OY.A1H(boolean):void");
    }

    public boolean A1J() {
        if ((this instanceof C60462xs) || (this instanceof C60472xv) || (this instanceof AnonymousClass2xu) || (this instanceof C42441vD) || (this instanceof C60482xw) || (this instanceof AnonymousClass2xt)) {
            return false;
        }
        return this.A1X;
    }

    public boolean A1K() {
        return !(this instanceof C60822yh);
    }

    public boolean A1L(AnonymousClass1IS r2) {
        if (!(this instanceof C37331m9)) {
            return getFMessage().A0z.equals(r2);
        }
        return false;
    }

    @Override // X.AbstractC28571Oc
    public void A5j(AbstractC14780m2 r2) {
        this.A1C.A01(r2);
    }

    @Override // android.view.ViewGroup, android.view.View
    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        this.A1C.A00();
    }

    @Override // android.view.ViewGroup, android.view.View
    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (super.dispatchKeyEvent(keyEvent)) {
            return true;
        }
        return this.A1Z.onKey(this, keyEvent.getKeyCode(), keyEvent);
    }

    @Override // android.view.ViewGroup, android.view.View
    public void dispatchSetPressed(boolean z) {
        super.dispatchSetPressed(isPressed());
        for (FrameLayout frameLayout : getInnerFrameLayouts()) {
            if (frameLayout != null) {
                frameLayout.setPressed(isPressed());
                frameLayout.setForeground(getInnerFrameForegroundDrawable());
            }
        }
    }

    public int getBroadcastDrawableId() {
        return R.drawable.broadcast_status_icon;
    }

    @Override // X.AbstractC28551Oa
    public int getBubbleMarginStart() {
        int i;
        int A00 = C27531Hw.A00(getContext());
        if (getFailedMessage() != null) {
            i = getResources().getDimensionPixelSize(R.dimen.bubble_margin_failed);
        } else {
            i = 0;
        }
        return A00 + i + super.A0b.A00(getContext());
    }

    public AnonymousClass1J1 getContactPhotosLoader() {
        Activity A00 = AbstractC35731ia.A00(getContext());
        if (A00 instanceof AbstractC14010kh) {
            return ((AbstractC14010kh) A00).ABb();
        }
        return null;
    }

    public TextView getDateView() {
        return this.A0E;
    }

    public ViewGroup getDateWrapper() {
        return this.A05;
    }

    private int getExtraBottomPadding() {
        C53182d3 r0;
        int i = super.A01;
        if ((i != 1 && i != 2) || (r0 = super.A0H) == null || r0.getVisibility() == 8) {
            return 0;
        }
        return getResources().getDimensionPixelSize(R.dimen.reaction_bubble_bottom_padding);
    }

    private AbstractC15340mz getFailedMessage() {
        return C30041Vv.A06(this.A0k, this.A14, getFMessage());
    }

    public List getHighlightTerms() {
        AbstractC13890kV r0 = super.A0a;
        if (r0 == null) {
            return null;
        }
        return r0.AGT();
    }

    public Drawable getInnerFrameForegroundDrawable() {
        Drawable A04;
        int i;
        boolean isPressed = isPressed();
        boolean z = getFMessage().A0z.A02;
        if (isPressed) {
            Context context = getContext();
            if (z) {
                A04 = AnonymousClass00T.A04(context, R.drawable.balloon_outgoing_frame);
                i = R.color.bubble_color_outgoing_pressed;
            } else {
                A04 = AnonymousClass00T.A04(context, R.drawable.balloon_incoming_frame);
                i = R.color.bubble_color_incoming_pressed;
            }
            int A00 = AnonymousClass00T.A00(context, i);
            AnonymousClass009.A05(A04);
            return AnonymousClass2GE.A04(A04, A00);
        }
        Context context2 = getContext();
        if (z) {
            return C92994Ym.A01(context2);
        }
        return C92994Ym.A00(context2);
    }

    public Set getInnerFrameLayouts() {
        HashSet hashSet = new HashSet();
        FrameLayout frameLayout = this.A08;
        if (frameLayout != null) {
            hashSet.add(frameLayout);
        }
        View findViewById = findViewById(R.id.link_preview_frame);
        if (findViewById != null) {
            hashSet.add(findViewById);
        }
        return hashSet;
    }

    public Drawable getKeepDrawable() {
        return AnonymousClass2GE.A01(getContext(), R.drawable.keep, R.color.messageStarColor);
    }

    public C40481rf getMessageReactions() {
        this.A1H.A02(getFMessage(), null, (byte) 56);
        return getFMessage().A0V;
    }

    public byte getMessageType() {
        return getFMessage().A0y;
    }

    public String getMoreInfoString() {
        Context context;
        int i;
        String string;
        C32141bg r3;
        C32351c1 A00 = this.A0u.A00(getFMessage().A11);
        if (A00 == null || (r3 = A00.A00) == null) {
            context = getContext();
        } else {
            C38301nr r0 = new C38301nr(r3, 3, 1);
            context = getContext();
            switch (r0.A01()) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                    break;
                case 5:
                case 6:
                case 9:
                case 10:
                    i = R.string.conflict_modal_message_fb_hosted;
                    string = context.getString(i);
                    break;
                case 7:
                case 8:
                    i = R.string.conflict_modal_message_bsp_hosted;
                    string = context.getString(i);
                    break;
                default:
                    string = null;
                    break;
            }
            return C42941w9.A01(super.A0K, string);
        }
        i = R.string.conflict_modal_message_e2ee;
        string = context.getString(i);
        return C42941w9.A01(super.A0K, string);
    }

    private float getNameInGroupTextFontSize() {
        return A01(getResources(), super.A0K);
    }

    public Drawable getPopupDrawable() {
        return AnonymousClass2GE.A01(getContext(), R.drawable.message_star_teal_anim, R.color.messageStarAnimationColor);
    }

    public int getSecondaryTextColor() {
        boolean z = getFMessage().A0z.A02;
        int i = R.color.secondary_text;
        if (z) {
            i = R.color.secondary_text_on_right_bubble;
        }
        return AnonymousClass00T.A00(getContext(), i);
    }

    public Drawable getStarDrawable() {
        return AnonymousClass2GE.A01(getContext(), R.drawable.message_star, R.color.messageStarColor);
    }

    public float getTextFontSize() {
        return A02(getResources(), super.A0K, WaFontListPreference.A00);
    }

    public int getTopAttributeTextAnchorId() {
        return R.id.quoted_message_holder;
    }

    @Override // android.view.View
    public boolean isPressed() {
        if (!super.isPressed()) {
            return false;
        }
        C52512ay r0 = this.A0e;
        return r0 == null || !r0.A02;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    @Override // X.AbstractC28551Oa, android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int paddingTop;
        int width;
        int paddingTop2;
        int width2;
        super.onLayout(z, i, i2, i3, i4);
        View view = super.A0E;
        if (view != null) {
            view.layout(0, 0, getWidth(), getHeight());
        }
        View view2 = this.A03;
        if (view2 != null && view2.getVisibility() == 0) {
            View view3 = (View) view2.getParent();
            View findViewById = findViewById(R.id.pushname_in_group_tv);
            view2.layout(view3.getPaddingLeft(), view3.getPaddingTop(), view3.getWidth() - view3.getPaddingRight(), view3.getPaddingTop() + view2.getMeasuredHeight());
            if (super.A0K.A04().A06) {
                width = view2.getPaddingLeft();
                paddingTop2 = view2.getPaddingTop();
                width2 = view2.getPaddingLeft() + findViewById.getWidth();
            } else {
                width = (view2.getWidth() - view2.getPaddingRight()) - findViewById.getWidth();
                paddingTop2 = view2.getPaddingTop();
                width2 = view2.getWidth() - view2.getPaddingRight();
            }
            findViewById.layout(width, paddingTop2, width2, view2.getPaddingTop() + findViewById.getHeight());
        }
        ImageView imageView = this.A09;
        if (imageView != null) {
            int intrinsicWidth = imageView.getDrawable().getIntrinsicWidth();
            int intrinsicHeight = this.A09.getDrawable().getIntrinsicHeight();
            View view4 = super.A0D;
            int top = view4.getTop();
            if (super.A0R) {
                paddingTop = getResources().getDimensionPixelSize(R.dimen.conversation_row_layout_top_size);
            } else {
                paddingTop = view4.getPaddingTop() + ((((view4.getHeight() - view4.getPaddingTop()) - view4.getPaddingBottom()) - intrinsicHeight) >> 1);
            }
            int i5 = top + paddingTop;
            int A00 = C27531Hw.A00(getContext()) + getResources().getDimensionPixelSize(R.dimen.conversation_row_layout_top_size);
            C64533Fx r1 = super.A0b;
            AnonymousClass009.A05(r1);
            boolean A04 = r1.A04();
            boolean A06 = r1.A06();
            if (A04) {
                A06 = false;
                if (A06 == getFMessage().A0z.A02) {
                    A06 = true;
                }
            }
            ImageView imageView2 = this.A09;
            if (A06) {
                imageView2.layout((getWidth() - intrinsicWidth) - A00, i5, getWidth() - A00, intrinsicHeight + i5);
            } else {
                imageView2.layout(A00, i5, intrinsicWidth + A00, intrinsicHeight + i5);
            }
        }
    }

    @Override // X.AbstractC28551Oa, android.view.View
    public void onMeasure(int i, int i2) {
        int min;
        int mode;
        if (this.A01 != 0) {
            if (View.MeasureSpec.getMode(i2) == 0) {
                min = this.A01;
                mode = Integer.MIN_VALUE;
            } else {
                min = Math.min(this.A01, View.MeasureSpec.getSize(i2));
                mode = View.MeasureSpec.getMode(i2);
            }
            i2 = View.MeasureSpec.makeMeasureSpec(min, mode);
        }
        super.onMeasure(i, i2);
    }

    public void setCacheInflated(boolean z) {
        this.A1U = z;
    }

    public void setForwardButtonAction(Runnable runnable) {
        this.A1S = runnable;
    }

    public void setIgnorePressedStateUpdates(boolean z) {
        this.A1W = z;
    }

    public void setMaxHeight(int i) {
        this.A01 = i;
    }

    public void setMessageText(String str, TextEmojiLabel textEmojiLabel, AbstractC15340mz r9) {
        A17(textEmojiLabel, r9, str, true, true);
    }

    @Override // android.view.View
    public void setPressed(boolean z) {
        if (!this.A1W) {
            super.setPressed(z);
        }
    }

    public void setSearchButtonAction(Runnable runnable) {
        this.A1T = runnable;
    }

    public void setSelectable(boolean z) {
        this.A1X = z;
    }

    @Override // android.view.View
    public void setSelected(boolean z) {
        C52512ay r0 = this.A0e;
        if (r0 != null) {
            r0.setRowSelected(z);
        }
    }
}
