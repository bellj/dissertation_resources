package X;

import com.whatsapp.util.Log;
import com.whatsapp.wamsys.JniBridge;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

/* renamed from: X.1Qx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC29111Qx {
    public final CountDownLatch A00 = new CountDownLatch(1);
    public volatile Object A01;

    public static Object A00(AbstractC29111Qx r1) {
        try {
            r1.A01();
            r1.A00.await();
            return r1.A01;
        } catch (Exception e) {
            Log.e("AsyncRunnable/runAndWait", e);
            return null;
        }
    }

    public void A01() {
        if (this instanceof AnonymousClass1RR) {
            AnonymousClass1RR r0 = (AnonymousClass1RR) this;
            JniBridge.jvidispatchIOOOO(1, r0.A02, r0.A03, new AnonymousClass1RQ(r0), r0.A04);
        } else if (this instanceof C29141Ra) {
            C29141Ra r02 = (C29141Ra) this;
            JniBridge.jvidispatchIOOOOO(r02.A03, r02.A04, r02.A02, new AnonymousClass1RZ(r02), r02.A05);
        } else if (this instanceof AnonymousClass1R3) {
            AnonymousClass1R3 r03 = (AnonymousClass1R3) this;
            AnonymousClass1R2 r7 = new AnonymousClass1R2(r03);
            String str = r03.A06;
            int i = r03.A00;
            byte[] bArr = r03.A0B;
            String str2 = r03.A04;
            String str3 = r03.A05;
            byte[] bArr2 = r03.A0A;
            byte[] bArr3 = r03.A09;
            String str4 = r03.A03;
            Map map = r03.A08;
            JniBridge.jvidispatchIIOOOOOOOOOO((long) i, str, str2, str3, str4, r03.A07, r7, bArr, bArr2, bArr3, map);
        } else if (this instanceof AnonymousClass1R7) {
            AnonymousClass1R7 r04 = (AnonymousClass1R7) this;
            AnonymousClass1R6 r72 = new AnonymousClass1R6(r04);
            String str5 = r04.A06;
            String str6 = r04.A04;
            String str7 = r04.A05;
            byte[] bArr4 = r04.A0B;
            byte[] bArr5 = r04.A0A;
            byte[] bArr6 = r04.A09;
            JniBridge.jvidispatchIOOOOOOOOOOOO(str5, str6, str7, r04.A02, r04.A03, r04.A07, r72, bArr4, bArr5, bArr6, null, r04.A08);
        } else if (this instanceof AnonymousClass1RI) {
            AnonymousClass1RI r05 = (AnonymousClass1RI) this;
            AnonymousClass1RH r6 = new AnonymousClass1RH(r05);
            String str8 = r05.A05;
            String str9 = r05.A03;
            String str10 = r05.A04;
            byte[] bArr7 = r05.A09;
            byte[] bArr8 = r05.A08;
            JniBridge.jvidispatchIOOOOOOOOO(0, str8, str9, str10, r05.A02, r05.A06, r6, bArr7, bArr8, r05.A07);
        } else if (this instanceof AnonymousClass1RG) {
            AnonymousClass1RG r06 = (AnonymousClass1RG) this;
            AnonymousClass1RF r73 = new AnonymousClass1RF(r06);
            String str11 = r06.A04;
            String str12 = r06.A05;
            byte[] bArr9 = r06.A0A;
            byte[] bArr10 = r06.A09;
            JniBridge.jvidispatchIOOOOOOOOOO(1, str11, str12, r06.A06, r06.A03, r06.A02, r06.A07, r73, bArr9, bArr10, r06.A08);
        } else if (this instanceof AnonymousClass1RB) {
            AnonymousClass1RB r07 = (AnonymousClass1RB) this;
            AnonymousClass1RA r12 = new AnonymousClass1RA(r07);
            String str13 = r07.A08;
            String str14 = r07.A09;
            byte[] bArr11 = r07.A0F;
            byte[] bArr12 = r07.A0D;
            String str15 = r07.A0A;
            String str16 = r07.A07;
            String str17 = r07.A06;
            int i2 = r07.A01;
            int i3 = r07.A02;
            int i4 = r07.A00;
            byte[] bArr13 = r07.A0E;
            String str18 = r07.A05;
            Map map2 = r07.A0C;
            JniBridge.jvidispatchIIIIOOOOOOOOOOOO((long) i2, (long) i3, (long) i4, str13, str14, str15, str16, str17, str18, r07.A0B, r12, bArr11, bArr12, bArr13, map2);
        } else if (!(this instanceof C29101Qw)) {
            AnonymousClass1RU r08 = (AnonymousClass1RU) this;
            AnonymousClass1RT r62 = new AnonymousClass1RT(r08);
            JniBridge.jvidispatchIOOOOOOOOO(1, r08.A04, r08.A05, r08.A03, r08.A06, r08.A02, r62, r08.A09, r08.A08, r08.A07);
        } else {
            C29101Qw r09 = (C29101Qw) this;
            C29091Qv r63 = new C29091Qv(r09);
            String str19 = r09.A04;
            String str20 = r09.A05;
            byte[] bArr14 = r09.A0A;
            byte[] bArr15 = r09.A08;
            String str21 = r09.A03;
            byte[] bArr16 = r09.A09;
            JniBridge.jvidispatchIOOOOOOOOOO(0, str19, str20, str21, r09.A02, r09.A06, r63, bArr14, bArr15, bArr16, r09.A07);
        }
    }

    public void A02(Object obj) {
        this.A01 = obj;
        this.A00.countDown();
    }
}
