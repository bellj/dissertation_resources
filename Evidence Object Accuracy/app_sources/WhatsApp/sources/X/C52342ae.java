package X;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.transition.Transition;
import android.transition.TransitionValues;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;

/* renamed from: X.2ae  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52342ae extends Transition {
    public static final String[] A02;
    public final boolean A00;
    public final boolean A01;

    static {
        String[] A08 = C13000ix.A08();
        A08[0] = "circleTransition:transforms";
        A02 = A08;
    }

    public C52342ae(boolean z, boolean z2) {
        this.A01 = z;
        this.A00 = z2;
    }

    @Override // android.transition.Transition
    public void captureEndValues(TransitionValues transitionValues) {
        View view = transitionValues.view;
        if (view.getWidth() > 0 && view.getHeight() > 0) {
            transitionValues.values.put("circleTransition:transforms", new C64103Eg(transitionValues.view));
        }
    }

    @Override // android.transition.Transition
    public void captureStartValues(TransitionValues transitionValues) {
        View view = transitionValues.view;
        if (view.getWidth() > 0 && view.getHeight() > 0) {
            transitionValues.values.put("circleTransition:transforms", new C64103Eg(transitionValues.view));
        }
    }

    @Override // android.transition.Transition
    public Animator createAnimator(ViewGroup viewGroup, TransitionValues transitionValues, TransitionValues transitionValues2) {
        if (!(transitionValues == null || transitionValues2 == null)) {
            C64103Eg r1 = (C64103Eg) transitionValues.values.get("circleTransition:transforms");
            C64103Eg r12 = (C64103Eg) transitionValues2.values.get("circleTransition:transforms");
            if (!(r1 == null || r12 == null || r1.equals(r12))) {
                float f = ((float) r1.A05) * r1.A00;
                int i = r12.A05;
                float f2 = (float) i;
                float f3 = r12.A00;
                float f4 = ((float) ((-r12.A06) + r1.A06)) + ((f - (f2 * f3)) / 2.0f) + r1.A02;
                float f5 = ((float) r1.A04) * r1.A01;
                int i2 = r12.A04;
                float f6 = (float) i2;
                float f7 = r12.A01;
                float f8 = ((float) ((-r12.A07) + r1.A07)) + ((f5 - (f6 * f7)) / 2.0f) + r1.A03;
                ObjectAnimator ofFloat = ObjectAnimator.ofFloat(transitionValues2.view, View.TRANSLATION_X, View.TRANSLATION_Y, getPathMotion().getPath(f4, f8, r12.A02, r12.A03));
                transitionValues2.view.setTranslationX(f4);
                transitionValues2.view.setTranslationY(f8);
                float min = Math.min(f / f2, f5 / f6);
                ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(transitionValues2.view, View.SCALE_X, min, f3);
                ObjectAnimator ofFloat3 = ObjectAnimator.ofFloat(transitionValues2.view, View.SCALE_Y, min, f7);
                float min2 = (float) (Math.min(i, i2) / 2);
                float sqrt = (float) (Math.sqrt((double) ((i * i) + (i2 * i2))) / 2.0d);
                View view = transitionValues2.view;
                int i3 = i / 2;
                int i4 = i2 / 2;
                float f9 = sqrt;
                if (this.A01) {
                    f9 = min2;
                }
                boolean z = this.A00;
                if (!z) {
                    min2 = sqrt;
                }
                AnonymousClass2YD r4 = new AnonymousClass2YD(ViewAnimationUtils.createCircularReveal(view, i3, i4, f9, min2));
                transitionValues2.view.setAlpha(0.0f);
                r4.addListener(new C72753f7(transitionValues2, this));
                if (z) {
                    transitionValues2.view.setOutlineProvider(new C73803gp(r12, this));
                    transitionValues2.view.setClipToOutline(true);
                    transitionValues2.view.invalidateOutline();
                }
                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playTogether(ofFloat, r4, ofFloat2, ofFloat3);
                animatorSet.setInterpolator(getInterpolator());
                return animatorSet;
            }
        }
        return null;
    }

    @Override // android.transition.Transition
    public String[] getTransitionProperties() {
        return A02;
    }
}
