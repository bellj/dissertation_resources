package X;

/* renamed from: X.5ew  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C119685ew extends AbstractC18860tB {
    public final /* synthetic */ C118155bM A00;

    public C119685ew(C118155bM r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC18860tB
    public void A01(AbstractC15340mz r4, int i) {
        C16700pc.A0E(r4, 0);
        if (r4 instanceof C16380ov) {
            AnonymousClass1IS r2 = r4.A0z;
            C118155bM r1 = this.A00;
            if (C16700pc.A0O(r2, r1.A0A)) {
                r1.A06((C16380ov) r4);
            }
        }
    }
}
