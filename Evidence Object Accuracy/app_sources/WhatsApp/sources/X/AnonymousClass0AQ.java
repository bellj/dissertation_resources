package X;

import android.os.Handler;
import android.os.Message;
import android.view.GestureDetector;

/* renamed from: X.0AQ  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0AQ extends Handler {
    public final /* synthetic */ AnonymousClass0Y4 A00;

    public AnonymousClass0AQ(AnonymousClass0Y4 r1) {
        this.A00 = r1;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        int i = message.what;
        if (i == 1) {
            AnonymousClass0Y4 r0 = this.A00;
            r0.A0K.onShowPress(r0.A09);
        } else if (i == 2) {
            AnonymousClass0Y4 r2 = this.A00;
            r2.A0J.removeMessages(3);
            r2.A0E = false;
            r2.A0F = true;
            r2.A0K.onLongPress(r2.A09);
        } else if (i == 3) {
            AnonymousClass0Y4 r22 = this.A00;
            GestureDetector.OnDoubleTapListener onDoubleTapListener = r22.A08;
            if (onDoubleTapListener == null) {
                return;
            }
            if (!r22.A0I) {
                onDoubleTapListener.onSingleTapConfirmed(r22.A09);
            } else {
                r22.A0E = true;
            }
        } else {
            StringBuilder sb = new StringBuilder("Unknown message ");
            sb.append(message);
            throw new RuntimeException(sb.toString());
        }
    }
}
