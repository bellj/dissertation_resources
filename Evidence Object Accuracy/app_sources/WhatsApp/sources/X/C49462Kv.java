package X;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.2Kv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C49462Kv extends AbstractC49422Kr {
    public final C14830m7 A00;
    public final C450720b A01;
    public final C230810h A02;
    public final C18610sj A03;
    public final C49432Ks A04;
    public final Map A05;

    public C49462Kv(AbstractC15710nm r10, C14830m7 r11, C14850m9 r12, C16120oU r13, C450720b r14, C230810h r15, C18610sj r16, C49432Ks r17, Map map) {
        super(r10, r12, r13, r14, map);
        this.A00 = r11;
        this.A02 = r15;
        this.A03 = r16;
        this.A04 = r17;
        this.A01 = r14;
        HashMap hashMap = new HashMap();
        hashMap.put("w:gp2", 209);
        hashMap.put("contacts", 25);
        hashMap.put("business", 202);
        hashMap.put("account_sync", 203);
        hashMap.put("devices", 204);
        hashMap.put("server_sync", 210);
        hashMap.put("encrypted_backup", 228);
        hashMap.put("disappearing_mode", 229);
        hashMap.put("psa", 104);
        hashMap.put("privacy_token", 234);
        hashMap.put("server", 235);
        hashMap.put("picture", 189);
        hashMap.put("status", 236);
        hashMap.put("gdpr", 238);
        hashMap.put("location", 240);
        hashMap.put("encrypt", 241);
        hashMap.put("mediaretry", 97);
        hashMap.put("pay", 247);
        hashMap.put("authkey_rotation", 248);
        this.A05 = Collections.unmodifiableMap(hashMap);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:108:0x034a, code lost:
        if (r0 == false) goto L_0x02ef;
     */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x00b3  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x00b9  */
    @Override // X.AbstractC49422Kr
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(X.AnonymousClass1V8 r34) {
        /*
        // Method dump skipped, instructions count: 902
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C49462Kv.A01(X.1V8):void");
    }
}
