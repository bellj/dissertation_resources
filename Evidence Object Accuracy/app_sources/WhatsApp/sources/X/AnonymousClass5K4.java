package X;

import com.whatsapp.avatar.home.AvatarHomeViewModel;

/* renamed from: X.5K4  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5K4 extends AnonymousClass1WI implements AnonymousClass1J7 {
    public final /* synthetic */ AvatarHomeViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass5K4(AvatarHomeViewModel avatarHomeViewModel) {
        super(1);
        this.this$0 = avatarHomeViewModel;
    }

    @Override // X.AnonymousClass1J7
    public /* bridge */ /* synthetic */ Object AJ4(Object obj) {
        AvatarHomeViewModel.A00(this.this$0, C12970iu.A1Y(obj));
        return AnonymousClass1WZ.A00;
    }
}
