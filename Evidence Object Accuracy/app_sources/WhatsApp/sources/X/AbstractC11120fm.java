package X;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

/* renamed from: X.0fm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC11120fm extends AbstractC114245Kt implements AbstractC11610gZ {
    public static final /* synthetic */ AtomicReferenceFieldUpdater A00 = AtomicReferenceFieldUpdater.newUpdater(AbstractC11120fm.class, Object.class, "_delayed");
    public static final /* synthetic */ AtomicReferenceFieldUpdater A01 = AtomicReferenceFieldUpdater.newUpdater(AbstractC11120fm.class, Object.class, "_queue");
    public volatile /* synthetic */ Object _delayed = null;
    public volatile /* synthetic */ int _isCompleted = 0;
    public volatile /* synthetic */ Object _queue = null;

    @Override // X.AbstractC10990fX
    public final void A04(Runnable runnable, AnonymousClass5X4 r2) {
        A0H(runnable);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001d, code lost:
        if (((X.C94454bq) r3).A04() == false) goto L_0x001f;
     */
    @Override // X.AbstractC114165Kl
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long A05() {
        /*
            r6 = this;
            long r1 = super.A05()
            r4 = 0
            int r0 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r0 == 0) goto L_0x001f
            java.lang.Object r3 = r6._queue
            r1 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            if (r3 == 0) goto L_0x0025
            boolean r0 = r3 instanceof X.C94454bq
            if (r0 == 0) goto L_0x0020
            X.4bq r3 = (X.C94454bq) r3
            boolean r0 = r3.A04()
            if (r0 != 0) goto L_0x0025
        L_0x001f:
            return r4
        L_0x0020:
            X.4VA r0 = X.C88534Ga.A00
            if (r3 != r0) goto L_0x001f
            return r1
        L_0x0025:
            java.lang.Object r0 = r6._delayed
            if (r0 == 0) goto L_0x002b
            monitor-enter(r0)
            monitor-exit(r0)
        L_0x002b:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC11120fm.A05():long");
    }

    @Override // X.AbstractC114165Kl
    public void A07() {
        C94654cI.A00();
        this._isCompleted = 1;
        A0F();
        do {
        } while (A0D() <= 0);
        A0G();
    }

    public long A0D() {
        if (!A0A()) {
            AnonymousClass4WX r1 = (AnonymousClass4WX) this._delayed;
            if (r1 != null && !r1.A01()) {
                System.nanoTime();
                synchronized (r1) {
                }
            }
            Runnable A0E = A0E();
            if (A0E == null) {
                return A05();
            }
            A0E.run();
        }
        return 0;
    }

    public final Runnable A0E() {
        while (true) {
            Object obj = this._queue;
            if (obj == null) {
                return null;
            }
            if (obj instanceof C94454bq) {
                C94454bq r2 = (C94454bq) obj;
                Object A012 = r2.A01();
                if (A012 != C94454bq.A04) {
                    return (Runnable) A012;
                }
                AnonymousClass0KN.A00(this, obj, r2.A02(), A01);
            } else if (obj == C88534Ga.A00) {
                return null;
            } else {
                if (AnonymousClass0KN.A00(this, obj, null, A01)) {
                    return (Runnable) obj;
                }
            }
        }
    }

    public final void A0F() {
        boolean A002;
        do {
            Object obj = this._queue;
            if (obj == null) {
                A002 = AnonymousClass0KN.A00(this, null, C88534Ga.A00, A01);
                continue;
            } else if (obj instanceof C94454bq) {
                ((C94454bq) obj).A03();
                return;
            } else if (obj != C88534Ga.A00) {
                C94454bq r1 = new C94454bq(8, true);
                r1.A00(obj);
                A002 = AnonymousClass0KN.A00(this, obj, r1, A01);
                continue;
            } else {
                return;
            }
        } while (!A002);
    }

    public final void A0G() {
        System.nanoTime();
        AnonymousClass4WX r0 = (AnonymousClass4WX) this._delayed;
        if (r0 != null) {
            r0.A00();
        }
    }

    public void A0H(Runnable runnable) {
        if (A0J(runnable)) {
            A0C();
        } else {
            RunnableC114235Ks.A01.A0H(runnable);
        }
    }

    public boolean A0I() {
        AnonymousClass4WX r0;
        if (A09() && ((r0 = (AnonymousClass4WX) this._delayed) == null || r0.A01())) {
            Object obj = this._queue;
            if (obj == null) {
                return true;
            }
            if (obj instanceof C94454bq) {
                return ((C94454bq) obj).A04();
            }
            if (obj == C88534Ga.A00) {
                return true;
            }
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0013, code lost:
        return true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A0J(java.lang.Runnable r6) {
        /*
            r5 = this;
        L_0x0000:
            java.lang.Object r4 = r5._queue
            int r0 = r5._isCompleted
            r3 = 0
            if (r0 != 0) goto L_0x0049
            r2 = 1
            if (r4 != 0) goto L_0x0014
            java.util.concurrent.atomic.AtomicReferenceFieldUpdater r1 = X.AbstractC11120fm.A01
            r0 = 0
            boolean r0 = X.AnonymousClass0KN.A00(r5, r0, r6, r1)
        L_0x0011:
            if (r0 == 0) goto L_0x0000
        L_0x0013:
            return r2
        L_0x0014:
            boolean r0 = r4 instanceof X.C94454bq
            if (r0 == 0) goto L_0x0031
            r0 = r4
            X.4bq r0 = (X.C94454bq) r0
            int r1 = r0.A00(r6)
            if (r1 == 0) goto L_0x0013
            if (r1 == r2) goto L_0x0027
            r0 = 2
            if (r1 == r0) goto L_0x0049
            goto L_0x0000
        L_0x0027:
            java.util.concurrent.atomic.AtomicReferenceFieldUpdater r1 = X.AbstractC11120fm.A01
            X.4bq r0 = r0.A02()
            X.AnonymousClass0KN.A00(r5, r4, r0, r1)
            goto L_0x0000
        L_0x0031:
            X.4VA r0 = X.C88534Ga.A00
            if (r4 == r0) goto L_0x0049
            r0 = 8
            X.4bq r1 = new X.4bq
            r1.<init>(r0, r2)
            r1.A00(r4)
            r1.A00(r6)
            java.util.concurrent.atomic.AtomicReferenceFieldUpdater r0 = X.AbstractC11120fm.A01
            boolean r0 = X.AnonymousClass0KN.A00(r5, r4, r1, r0)
            goto L_0x0011
        L_0x0049:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC11120fm.A0J(java.lang.Runnable):boolean");
    }
}
