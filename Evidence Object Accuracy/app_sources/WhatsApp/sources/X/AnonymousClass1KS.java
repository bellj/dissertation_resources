package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import java.util.ArrayList;

/* renamed from: X.1KS  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1KS implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C100434lu();
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public AnonymousClass1KB A04;
    public String A05;
    public String A06;
    public String A07;
    public String A08;
    public String A09;
    public String A0A;
    public String A0B;
    public String A0C;
    public String A0D;
    public String A0E;
    public String A0F;
    public boolean A0G;
    public boolean A0H;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass1KS() {
    }

    public /* synthetic */ AnonymousClass1KS(Parcel parcel) {
        if (parcel != null) {
            this.A0C = parcel.readString();
            this.A07 = parcel.readString();
            this.A0A = parcel.readString();
            this.A0B = parcel.readString();
            this.A02 = parcel.readInt();
            this.A03 = parcel.readInt();
            this.A0E = parcel.readString();
            this.A08 = parcel.readString();
            this.A00 = parcel.readInt();
            this.A0F = parcel.readString();
            this.A05 = parcel.readString();
            this.A06 = parcel.readString();
            boolean z = false;
            this.A0H = parcel.readInt() == 1;
            this.A09 = parcel.readString();
            this.A0G = parcel.readInt() == 1 ? true : z;
        }
    }

    public static String A00(C37471mS[] r4) {
        int length = r4.length;
        ArrayList arrayList = new ArrayList(length);
        for (C37471mS r0 : r4) {
            arrayList.add(r0.toString());
        }
        return TextUtils.join(" ", arrayList);
    }

    /* renamed from: A01 */
    public AnonymousClass1KS clone() {
        AnonymousClass1KS r1 = new AnonymousClass1KS();
        r1.A0C = this.A0C;
        String str = this.A08;
        if (str != null) {
            int i = this.A01;
            r1.A08 = str;
            r1.A01 = i;
        }
        r1.A0F = this.A0F;
        r1.A07 = this.A07;
        r1.A05 = this.A05;
        r1.A0B = this.A0B;
        r1.A0A = this.A0A;
        r1.A00 = this.A00;
        r1.A03 = this.A03;
        r1.A02 = this.A02;
        r1.A04 = this.A04;
        r1.A06 = this.A06;
        r1.A0H = this.A0H;
        r1.A09 = this.A09;
        r1.A0G = this.A0G;
        return r1;
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuffer stringBuffer = new StringBuffer("Sticker{");
        stringBuffer.append(", mimeType='");
        stringBuffer.append(this.A0B);
        stringBuffer.append('\'');
        stringBuffer.append(", height=");
        stringBuffer.append(this.A02);
        stringBuffer.append(", width=");
        stringBuffer.append(this.A03);
        stringBuffer.append(", metadata=");
        stringBuffer.append(this.A04);
        stringBuffer.append(", saltedFileHash='");
        stringBuffer.append(this.A0D);
        stringBuffer.append('\'');
        stringBuffer.append(", fileSize=");
        stringBuffer.append(this.A00);
        stringBuffer.append('}');
        return stringBuffer.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A0C);
        parcel.writeString(this.A07);
        parcel.writeString(this.A0A);
        parcel.writeString(this.A0B);
        parcel.writeInt(this.A02);
        parcel.writeInt(this.A03);
        parcel.writeString(this.A0E);
        parcel.writeString(this.A08);
        parcel.writeInt(this.A00);
        parcel.writeString(this.A0F);
        parcel.writeString(this.A05);
        parcel.writeString(this.A06);
        parcel.writeInt(this.A0H ? 1 : 0);
        parcel.writeString(this.A09);
        parcel.writeInt(this.A0G ? 1 : 0);
    }
}
