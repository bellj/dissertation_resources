package X;

/* renamed from: X.5sj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C126345sj {
    public final AnonymousClass1V8 A00;

    public C126345sj(String str, String str2, String str3) {
        C41141sy r0 = new C41141sy("elo");
        if (AnonymousClass3JT.A0E(str, 1, 10000, false)) {
            C41141sy.A01(r0, "device_signature", str);
        }
        if (AnonymousClass3JT.A0E(str2, 1, 10000, false)) {
            C41141sy.A01(r0, "wallet_signature", str2);
        }
        if (AnonymousClass3JT.A0E(str3, 1, 10000, false)) {
            C41141sy.A01(r0, "challenge_id", str3);
        }
        this.A00 = r0.A03();
    }
}
