package X;

/* renamed from: X.6Cp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133916Cp implements AbstractC136356Mf {
    public final /* synthetic */ C123505nG A00;

    @Override // X.AbstractC136356Mf
    public void AOm() {
    }

    public C133916Cp(C123505nG r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC136356Mf
    public void AOE() {
        C123505nG r3 = this.A00;
        r3.A05(1, 3);
        C12960it.A0t(C117295Zj.A05(((AbstractC118085bF) r3).A06), "payments_home_account_recovery_banner_dismissed", true);
        AbstractC118085bF.A00((AbstractC118085bF) r3);
    }
}
