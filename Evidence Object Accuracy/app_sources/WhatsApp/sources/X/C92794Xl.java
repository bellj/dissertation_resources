package X;

/* renamed from: X.4Xl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C92794Xl {
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C92794Xl) {
                C92794Xl r5 = (C92794Xl) obj;
                if (!(this.A00 == r5.A00 && this.A03 == r5.A03 && this.A02 == r5.A02 && this.A01 == r5.A01)) {
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (((((this.A00 * 31) + this.A03) * 31) + this.A02) * 31) + this.A01;
    }

    public C92794Xl(int i, int i2, int i3, int i4) {
        this.A00 = i;
        this.A03 = i2;
        this.A02 = i3;
        this.A01 = i4;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("BadgeIconSize(extraSmall=");
        A0k.append(this.A00);
        A0k.append(", small=");
        A0k.append(this.A03);
        A0k.append(", medium=");
        A0k.append(this.A02);
        A0k.append(", large=");
        A0k.append(this.A01);
        return C12970iu.A0u(A0k);
    }
}
