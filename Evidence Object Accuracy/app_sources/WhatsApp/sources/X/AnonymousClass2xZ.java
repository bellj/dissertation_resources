package X;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import com.whatsapp.Conversation;
import com.whatsapp.R;
import com.whatsapp.community.SubgroupPileView;

/* renamed from: X.2xZ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2xZ extends AbstractC36781kZ {
    public AnonymousClass1YT A00;
    public C864547j A01;
    public AnonymousClass37R A02;
    public AnonymousClass1YS A03;
    public final AbstractC15710nm A04;
    public final C22640zP A05;
    public final C15550nR A06;
    public final C14830m7 A07;
    public final C14820m6 A08;
    public final C18750sx A09;
    public final C15600nX A0A;
    public final C236812p A0B;
    public final AnonymousClass132 A0C;
    public final C15370n3 A0D;
    public final C14850m9 A0E;
    public final C20710wC A0F;
    public final AnonymousClass1E5 A0G;
    public final C15580nU A0H;
    public final C22280yp A0I;
    public final C255819y A0J;
    public final AbstractC47972Dm A0K = new C70893c1(this);
    public final AnonymousClass12G A0L;
    public final AnonymousClass5V9 A0M = new AnonymousClass5V9() { // from class: X.5Ax
        @Override // X.AnonymousClass5V9
        public final void ANW(AnonymousClass1YT r2) {
            AnonymousClass2xZ.this.A00 = r2;
        }
    };
    public final AnonymousClass5VA A0N = new AnonymousClass5VA() { // from class: X.3c5
        @Override // X.AnonymousClass5VA
        public final void ARe(AnonymousClass1YS r4) {
            AnonymousClass2xZ r2 = AnonymousClass2xZ.this;
            StringBuilder A0k = C12960it.A0k("groupconversationmenu/fetchJoinableCallLogCallback groupJid: ");
            A0k.append(r2.A0R);
            C12960it.A1F(A0k);
            if (!C29941Vi.A00(r4, r2.A03)) {
                r2.A03 = r4;
                if (r4 != null) {
                    r2.A06(r4.A00);
                }
                ((AbstractC36781kZ) r2).A01.invalidateOptionsMenu();
            }
        }
    };

    public AnonymousClass2xZ(ActivityC000800j r37, AbstractC15710nm r38, AbstractC13860kS r39, C14900mE r40, C15570nT r41, C15450nH r42, C16170oZ r43, C18330sH r44, AnonymousClass2TT r45, C22330yu r46, C243915i r47, C22640zP r48, C15550nR r49, AnonymousClass10S r50, C22700zV r51, AbstractC13920kY r52, C255319t r53, AnonymousClass1A6 r54, C17050qB r55, C14830m7 r56, C14820m6 r57, AnonymousClass018 r58, C14950mJ r59, C18750sx r60, C19990v2 r61, C20830wO r62, C15600nX r63, C236812p r64, AnonymousClass132 r65, C15370n3 r66, C22100yW r67, C14850m9 r68, C20710wC r69, C244215l r70, AnonymousClass1E5 r71, C15580nU r72, C22280yp r73, C15860o1 r74, AnonymousClass12F r75, AnonymousClass12U r76, C255819y r77, C255719x r78, AbstractC14440lR r79, AnonymousClass12G r80, C21280xA r81) {
        super(r37, r39, r40, r41, r42, r43, r44, r45, r46, r47, r50, r51, r52, r53, r54, r55, r57, r58, r59, r61, r62, r66, r67, r70, r72, r74, r75, r76, r78, r79, r81);
        this.A07 = r56;
        this.A0E = r68;
        this.A04 = r38;
        this.A06 = r49;
        this.A0I = r73;
        this.A0L = r80;
        this.A0F = r69;
        this.A09 = r60;
        this.A0C = r65;
        this.A08 = r57;
        this.A05 = r48;
        this.A0B = r64;
        this.A0A = r63;
        this.A0J = r77;
        this.A0H = r72;
        this.A0D = r49.A0B(r72);
        this.A0G = r71;
    }

    public final void A06(long j) {
        C18750sx r1 = this.A09;
        AnonymousClass1YT A01 = r1.A01(j);
        if (A01 != null) {
            this.A00 = A01;
        } else if (this.A01 == null) {
            C864547j r2 = new C864547j(r1, this.A0M, j);
            this.A01 = r2;
            this.A0W.Ab5(r2, new Void[0]);
        }
    }

    public final boolean A07() {
        if (super.A0F.AIN()) {
            return false;
        }
        C15580nU r6 = this.A0H;
        C15370n3 r4 = super.A00;
        C20710wC r5 = this.A0F;
        return AnonymousClass1SF.A0I(super.A04, super.A05, this.A06, this.A0A, r4, r5, r6);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0056, code lost:
        if (r10.A0F.A0Y(r10.A00) != false) goto L_0x0058;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0068, code lost:
        if (r5.equals(r3.groupJid) == false) goto L_0x006a;
     */
    @Override // X.AbstractC36781kZ, X.AbstractC36791ka
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AOk(android.view.Menu r11) {
        /*
        // Method dump skipped, instructions count: 477
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2xZ.AOk(android.view.Menu):void");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC36781kZ, X.AbstractC36791ka
    public boolean ATH(MenuItem menuItem) {
        View A05;
        int itemId = menuItem.getItemId();
        if (itemId != 12) {
            switch (itemId) {
                case 21:
                    ActivityC000800j r3 = super.A01;
                    Intent A0O = C14960mK.A0O(r3, super.A00.A0D, true, false, false);
                    if (this.A05.A0A(this.A0H)) {
                        A05 = ((SubgroupPileView) AnonymousClass00T.A05(r3, R.id.subgroup_facepile_toolbar_photo)).A02;
                    } else {
                        A05 = AnonymousClass00T.A05(r3, R.id.transition_start);
                    }
                    Bundle A052 = AbstractC454421p.A05(r3, A05, super.A08.A00(R.string.transition_photo));
                    C35741ib.A00(A0O, C12980iv.A0r(r3));
                    r3.startActivity(A0O, A052);
                    return true;
                case 22:
                    break;
                case 23:
                    AbstractC13860kS r5 = super.A02;
                    r5.Ady(0, R.string.register_wait_message);
                    this.A0I.A03(this.A0R);
                    this.A0W.Aaz(new AnonymousClass37I(r5, this.A0C, this.A0H), new Object[0]);
                    return true;
                case 24:
                    C15550nR r2 = this.A06;
                    C15580nU r32 = this.A0H;
                    AnonymousClass1SF.A0E(super.A01, r2, r32, AnonymousClass1SF.A0D(super.A04, this.A0A, r32), null, 24, true);
                    break;
                case 25:
                    this.A0J.A00(6);
                    super.A0F.AZU(super.A00, false);
                    return true;
                case 26:
                    this.A0J.A00(7);
                    super.A0F.AZU(super.A00, true);
                    return true;
                case 27:
                    AnonymousClass1YT r33 = this.A00;
                    if (r33 != null) {
                        Conversation conversation = (Conversation) super.A0F;
                        conversation.A42.A06(conversation, r33, 9);
                        return true;
                    }
                    this.A04.AaV("LinkedCallLogPrefetchNotCompletedOnTime", "groupConversationHeader", false);
                    return true;
                default:
                    return super.ATH(menuItem);
            }
            return true;
        }
        ActivityC000800j r34 = super.A01;
        C15580nU r22 = this.A0H;
        r34.startActivity(C14960mK.A0E(r34, r22, this.A06.A0B(r22).A01, 3));
        return true;
    }

    @Override // X.AbstractC36781kZ, X.AbstractC36791ka
    public boolean AUA(Menu menu) {
        StringBuilder A0k = C12960it.A0k("groupconversationmenu/onprepareoptionsmenu ");
        A0k.append(menu.size());
        C12960it.A1F(A0k);
        if (menu.size() == 0) {
            return false;
        }
        A04(menu.findItem(4));
        menu.findItem(1).getSubMenu();
        return super.AUA(menu);
    }

    @Override // X.AbstractC36781kZ, X.C28791Pa, android.app.Application.ActivityLifecycleCallbacks
    public void onActivityCreated(Activity activity, Bundle bundle) {
        super.onActivityCreated(activity, bundle);
        this.A0L.A03(this.A0K);
    }

    @Override // X.AbstractC36781kZ, X.C28791Pa, android.app.Application.ActivityLifecycleCallbacks
    public void onActivityDestroyed(Activity activity) {
        super.onActivityDestroyed(activity);
        this.A0L.A04(this.A0K);
        AnonymousClass37R r0 = this.A02;
        if (r0 != null) {
            r0.A03(true);
            this.A02 = null;
        }
        C864547j r02 = this.A01;
        if (r02 != null) {
            r02.A03(true);
            this.A01 = null;
        }
    }
}
