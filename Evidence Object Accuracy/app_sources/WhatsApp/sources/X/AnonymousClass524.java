package X;

/* renamed from: X.524  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass524 implements AnonymousClass5T7 {
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0041, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000f, code lost:
        if ((r6 instanceof X.C82933wQ) == false) goto L_0x0011;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001f, code lost:
        if ((r5 instanceof X.C82933wQ) == false) goto L_0x0021;
     */
    @Override // X.AnonymousClass5T7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A9i(X.AbstractC94534c0 r5, X.AbstractC94534c0 r6, X.AnonymousClass4RG r7) {
        /*
            r4 = this;
            boolean r0 = r6 instanceof X.C83003wX
            boolean r0 = X.C72473ef.A00(r0)
            r1 = 0
            if (r0 == 0) goto L_0x0011
            X.4c0 r6 = X.C83003wX.A00(r6)
            boolean r0 = r6 instanceof X.C82933wQ
            if (r0 != 0) goto L_0x0041
        L_0x0011:
            X.3wZ r3 = r6.A07()
            boolean r0 = r5 instanceof X.C83003wX
            if (r0 == 0) goto L_0x0021
            X.4c0 r5 = X.C83003wX.A00(r5)
            boolean r0 = r5 instanceof X.C82933wQ
            if (r0 != 0) goto L_0x0041
        L_0x0021:
            X.3wZ r0 = r5.A07()
            java.util.List r0 = r0.A00
            java.util.Iterator r2 = r0.iterator()
        L_0x002b:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x003f
            java.lang.Object r1 = r2.next()
            java.util.List r0 = r3.A00
            boolean r0 = r0.contains(r1)
            if (r0 != 0) goto L_0x002b
            r0 = 0
            return r0
        L_0x003f:
            r0 = 1
            return r0
        L_0x0041:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass524.A9i(X.4c0, X.4c0, X.4RG):boolean");
    }
}
