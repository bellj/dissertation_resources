package X;

import com.facebook.redex.RunnableBRunnable0Shape16S0100000_I1_2;
import com.whatsapp.status.playback.widget.StatusPlaybackProgressView;
import com.whatsapp.util.Log;

/* renamed from: X.2Fd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C48242Fd {
    public final /* synthetic */ AbstractC33621eg A00;
    public final /* synthetic */ AbstractC35561iG A01;

    public C48242Fd(AbstractC35561iG r1) {
        this.A01 = r1;
        this.A00 = r1;
    }

    public void A00() {
        StringBuilder sb = new StringBuilder("playbackPage/onPlaybackContentFinished page=");
        sb.append(this);
        sb.append("; host=");
        AbstractC33621eg r3 = this.A00;
        sb.append(r3.A0L.A01);
        Log.i(sb.toString());
        StatusPlaybackProgressView statusPlaybackProgressView = r3.A02;
        if (statusPlaybackProgressView.A03 == r3.A0M) {
            statusPlaybackProgressView.A03 = null;
        }
        statusPlaybackProgressView.invalidate();
        if (!r3.A04) {
            r3.A08.post(new RunnableBRunnable0Shape16S0100000_I1_2(this, 13));
        }
    }

    public void A01() {
        StringBuilder sb = new StringBuilder("playbackPage/onPlaybackContentStarted page=");
        sb.append(this);
        sb.append("; host=");
        AbstractC33621eg r1 = this.A00;
        sb.append(r1.A0L.A01);
        Log.i(sb.toString());
        r1.A0C();
    }
}
