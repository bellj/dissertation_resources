package X;

/* renamed from: X.5OC  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5OC extends AnonymousClass5OF implements AbstractC115555Rz {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int[] A05;

    public AnonymousClass5OC() {
        this.A05 = new int[16];
        reset();
    }

    public AnonymousClass5OC(AnonymousClass5OC r2) {
        super(r2);
        this.A05 = new int[16];
        A0O(r2);
    }

    public final void A0O(AnonymousClass5OC r5) {
        super.A0M(r5);
        this.A00 = r5.A00;
        this.A01 = r5.A01;
        this.A02 = r5.A02;
        this.A03 = r5.A03;
        int[] iArr = r5.A05;
        System.arraycopy(iArr, 0, this.A05, 0, iArr.length);
        this.A04 = r5.A04;
    }

    @Override // X.AnonymousClass5WS
    public AnonymousClass5WS A7l() {
        return new AnonymousClass5OC(this);
    }

    @Override // X.AnonymousClass5XI
    public int A97(byte[] bArr, int i) {
        A0K();
        A00(bArr, this.A00, i);
        A00(bArr, this.A01, i + 4);
        A00(bArr, this.A02, i + 8);
        A00(bArr, this.A03, i + 12);
        reset();
        return 16;
    }

    @Override // X.AnonymousClass5XI
    public String AAf() {
        return "MD5";
    }

    @Override // X.AnonymousClass5XI
    public int ACZ() {
        return 16;
    }

    @Override // X.AnonymousClass5WS
    public void Aag(AnonymousClass5WS r1) {
        A0O((AnonymousClass5OC) r1);
    }

    @Override // X.AnonymousClass5OF, X.AnonymousClass5XI
    public void reset() {
        super.reset();
        this.A00 = 1732584193;
        this.A01 = -271733879;
        this.A02 = -1732584194;
        this.A03 = 271733878;
        this.A04 = 0;
        int i = 0;
        while (true) {
            int[] iArr = this.A05;
            if (i != iArr.length) {
                iArr[i] = 0;
                i++;
            } else {
                return;
            }
        }
    }

    public static final void A00(byte[] bArr, int i, int i2) {
        bArr[i2] = (byte) i;
        C72463ee.A0W(bArr, i, i2 + 1);
        bArr[i2 + 2] = (byte) (i >>> 16);
        bArr[i2 + 3] = (byte) (i >>> 24);
    }

    @Override // X.AnonymousClass5OF
    public void A0L() {
        int i = this.A00;
        int i2 = this.A01;
        int i3 = this.A02;
        int i4 = this.A03;
        int A0D = AnonymousClass5OF.A0D(i3, i2, i4, i);
        int[] iArr = this.A05;
        int i5 = iArr[0];
        int i6 = (A0D + i5) - 680876936;
        int A0F = AnonymousClass5OF.A0F(i6, 25, i6 << 7, i2);
        int A0D2 = AnonymousClass5OF.A0D(i2, A0F, i3, i4);
        int i7 = iArr[1];
        int i8 = (A0D2 + i7) - 389564586;
        int A0F2 = AnonymousClass5OF.A0F(i8, 20, i8 << 12, A0F);
        int A0D3 = AnonymousClass5OF.A0D(A0F, A0F2, i2, i3);
        int i9 = iArr[2];
        int i10 = A0D3 + i9 + 606105819;
        int A0F3 = AnonymousClass5OF.A0F(i10, 15, i10 << 17, A0F2);
        int A0D4 = AnonymousClass5OF.A0D(A0F2, A0F3, A0F, i2);
        int i11 = iArr[3];
        int i12 = (A0D4 + i11) - 1044525330;
        int A0F4 = AnonymousClass5OF.A0F(i12, 10, i12 << 22, A0F3);
        int A0D5 = AnonymousClass5OF.A0D(A0F3, A0F4, A0F2, A0F);
        int i13 = iArr[4];
        int i14 = (A0D5 + i13) - 176418897;
        int A0F5 = AnonymousClass5OF.A0F(i14, 25, i14 << 7, A0F4);
        int A0D6 = AnonymousClass5OF.A0D(A0F4, A0F5, A0F3, A0F2);
        int i15 = iArr[5];
        int A0A = AnonymousClass5OF.A0A(A0D6 + i15, 1200080426, A0F5);
        int A0D7 = AnonymousClass5OF.A0D(A0F5, A0A, A0F4, A0F3);
        int i16 = iArr[6];
        int i17 = (A0D7 + i16) - 1473231341;
        int A0F6 = AnonymousClass5OF.A0F(i17, 15, i17 << 17, A0A);
        int A0D8 = AnonymousClass5OF.A0D(A0A, A0F6, A0F5, A0F4);
        int i18 = iArr[7];
        int i19 = (A0D8 + i18) - 45705983;
        int A0F7 = AnonymousClass5OF.A0F(i19, 10, i19 << 22, A0F6);
        int A0D9 = AnonymousClass5OF.A0D(A0F6, A0F7, A0A, A0F5);
        int i20 = iArr[8];
        int i21 = A0D9 + i20 + 1770035416;
        int A0F8 = AnonymousClass5OF.A0F(i21, 25, i21 << 7, A0F7);
        int A0D10 = AnonymousClass5OF.A0D(A0F7, A0F8, A0F6, A0A);
        int i22 = iArr[9];
        int A0A2 = AnonymousClass5OF.A0A(A0D10 + i22, -1958414417, A0F8);
        int A0D11 = AnonymousClass5OF.A0D(A0F8, A0A2, A0F7, A0F6);
        int i23 = iArr[10];
        int i24 = (A0D11 + i23) - 42063;
        int A0F9 = AnonymousClass5OF.A0F(i24, 15, i24 << 17, A0A2);
        int A0D12 = AnonymousClass5OF.A0D(A0A2, A0F9, A0F8, A0F7);
        int i25 = iArr[11];
        int i26 = (A0D12 + i25) - 1990404162;
        int A0F10 = AnonymousClass5OF.A0F(i26, 10, i26 << 22, A0F9);
        int A0D13 = AnonymousClass5OF.A0D(A0F9, A0F10, A0A2, A0F8);
        int i27 = iArr[12];
        int i28 = A0D13 + i27 + 1804603682;
        int A0F11 = AnonymousClass5OF.A0F(i28, 25, i28 << 7, A0F10);
        int A0D14 = AnonymousClass5OF.A0D(A0F10, A0F11, A0F9, A0A2);
        int i29 = iArr[13];
        int A0A3 = AnonymousClass5OF.A0A(A0D14 + i29, -40341101, A0F11);
        int i30 = A0A3 ^ -1;
        int i31 = iArr[14];
        int i32 = ((A0F9 + ((i30 & A0F10) | (A0F11 & A0A3))) + i31) - 1502002290;
        int A0F12 = AnonymousClass5OF.A0F(i32, 15, i32 << 17, A0A3);
        int i33 = A0F12 ^ -1;
        int i34 = iArr[15];
        int i35 = A0F10 + ((i33 & A0F11) | (A0A3 & A0F12)) + i34 + 1236535329;
        int A0F13 = AnonymousClass5OF.A0F(i35, 10, i35 << 22, A0F12);
        int i36 = ((A0F11 + ((A0F13 & A0A3) | (A0F12 & i30))) + i7) - 165796510;
        int A0F14 = AnonymousClass5OF.A0F(i36, 27, i36 << 5, A0F13);
        int A0C = AnonymousClass5OF.A0C(A0A3 + ((A0F14 & A0F12) | (A0F13 & i33)) + i16, -1069501632, A0F14);
        int A09 = AnonymousClass5OF.A09(AnonymousClass5OF.A0G(A0C, A0F13, A0F14, A0F12, i25), 643717713, A0C);
        int A0G = AnonymousClass5OF.A0G(A09, A0F14, A0C, A0F13, i5) - 373897302;
        int A0F15 = AnonymousClass5OF.A0F(A0G, 12, A0G << 20, A09);
        int A0G2 = AnonymousClass5OF.A0G(A0F15, A0C, A09, A0F14, i15) - 701558691;
        int A0F16 = AnonymousClass5OF.A0F(A0G2, 27, A0G2 << 5, A0F15);
        int A0C2 = AnonymousClass5OF.A0C(AnonymousClass5OF.A0G(A0F16, A09, A0F15, A0C, i23), 38016083, A0F16);
        int A092 = AnonymousClass5OF.A09(AnonymousClass5OF.A0G(A0C2, A0F15, A0F16, A09, i34), -660478335, A0C2);
        int A0G3 = AnonymousClass5OF.A0G(A092, A0F16, A0C2, A0F15, i13) - 405537848;
        int A0F17 = AnonymousClass5OF.A0F(A0G3, 12, A0G3 << 20, A092);
        int A0G4 = AnonymousClass5OF.A0G(A0F17, A0C2, A092, A0F16, i22) + 568446438;
        int A0F18 = AnonymousClass5OF.A0F(A0G4, 27, A0G4 << 5, A0F17);
        int A0C3 = AnonymousClass5OF.A0C(AnonymousClass5OF.A0G(A0F18, A092, A0F17, A0C2, i31), -1019803690, A0F18);
        int A093 = AnonymousClass5OF.A09(AnonymousClass5OF.A0G(A0C3, A0F17, A0F18, A092, i11), -187363961, A0C3);
        int A0G5 = AnonymousClass5OF.A0G(A093, A0F18, A0C3, A0F17, i20) + 1163531501;
        int A0F19 = AnonymousClass5OF.A0F(A0G5, 12, A0G5 << 20, A093);
        int A0G6 = AnonymousClass5OF.A0G(A0F19, A0C3, A093, A0F18, i29) - 1444681467;
        int A0F20 = AnonymousClass5OF.A0F(A0G6, 27, A0G6 << 5, A0F19);
        int A0C4 = AnonymousClass5OF.A0C(AnonymousClass5OF.A0G(A0F20, A093, A0F19, A0C3, i9), -51403784, A0F20);
        int A094 = AnonymousClass5OF.A09(AnonymousClass5OF.A0G(A0C4, A0F19, A0F20, A093, i18), 1735328473, A0C4);
        int A0G7 = AnonymousClass5OF.A0G(A094, A0F20, A0C4, A0F19, i27) - 1926607734;
        int A0F21 = AnonymousClass5OF.A0F(A0G7, 12, A0G7 << 20, A094);
        int A0E = (AnonymousClass5OF.A0E(A0F21, A094, A0C4, A0F20) + i15) - 378558;
        int A0F22 = AnonymousClass5OF.A0F(A0E, 28, A0E << 4, A0F21);
        int A0B = AnonymousClass5OF.A0B(AnonymousClass5OF.A0E(A0F22, A0F21, A094, A0C4) + i20, -2022574463, A0F22);
        int A0E2 = AnonymousClass5OF.A0E(A0B, A0F22, A0F21, A094) + i25 + 1839030562;
        int A0F23 = AnonymousClass5OF.A0F(A0E2, 16, A0E2 << 16, A0B);
        int A0E3 = (AnonymousClass5OF.A0E(A0F23, A0B, A0F22, A0F21) + i31) - 35309556;
        int A0F24 = AnonymousClass5OF.A0F(A0E3, 9, A0E3 << 23, A0F23);
        int A0E4 = (AnonymousClass5OF.A0E(A0F24, A0F23, A0B, A0F22) + i7) - 1530992060;
        int A0F25 = AnonymousClass5OF.A0F(A0E4, 28, A0E4 << 4, A0F24);
        int A0B2 = AnonymousClass5OF.A0B(AnonymousClass5OF.A0E(A0F25, A0F24, A0F23, A0B) + i13, 1272893353, A0F25);
        int A0E5 = (AnonymousClass5OF.A0E(A0B2, A0F25, A0F24, A0F23) + i18) - 155497632;
        int A0F26 = AnonymousClass5OF.A0F(A0E5, 16, A0E5 << 16, A0B2);
        int A0E6 = (AnonymousClass5OF.A0E(A0F26, A0B2, A0F25, A0F24) + i23) - 1094730640;
        int A0F27 = AnonymousClass5OF.A0F(A0E6, 9, A0E6 << 23, A0F26);
        int A0E7 = AnonymousClass5OF.A0E(A0F27, A0F26, A0B2, A0F25) + i29 + 681279174;
        int A0F28 = AnonymousClass5OF.A0F(A0E7, 28, A0E7 << 4, A0F27);
        int A0B3 = AnonymousClass5OF.A0B(AnonymousClass5OF.A0E(A0F28, A0F27, A0F26, A0B2) + i5, -358537222, A0F28);
        int A0E8 = (AnonymousClass5OF.A0E(A0B3, A0F28, A0F27, A0F26) + i11) - 722521979;
        int A0F29 = AnonymousClass5OF.A0F(A0E8, 16, A0E8 << 16, A0B3);
        int A0E9 = AnonymousClass5OF.A0E(A0F29, A0B3, A0F28, A0F27) + i16 + 76029189;
        int A0F30 = AnonymousClass5OF.A0F(A0E9, 9, A0E9 << 23, A0F29);
        int A0E10 = (AnonymousClass5OF.A0E(A0F30, A0F29, A0B3, A0F28) + i22) - 640364487;
        int A0F31 = AnonymousClass5OF.A0F(A0E10, 28, A0E10 << 4, A0F30);
        int A0B4 = AnonymousClass5OF.A0B(AnonymousClass5OF.A0E(A0F31, A0F30, A0F29, A0B3) + i27, -421815835, A0F31);
        int A0E11 = AnonymousClass5OF.A0E(A0B4, A0F31, A0F30, A0F29) + i34 + 530742520;
        int A0F32 = AnonymousClass5OF.A0F(A0E11, 16, A0E11 << 16, A0B4);
        int A0E12 = (AnonymousClass5OF.A0E(A0F32, A0B4, A0F31, A0F30) + i9) - 995338651;
        int A0F33 = AnonymousClass5OF.A0F(A0E12, 9, A0E12 << 23, A0F32);
        int A0H = AnonymousClass5OF.A0H(A0B4, A0F33, A0F32, A0F31, i5) - 198630844;
        int A0F34 = AnonymousClass5OF.A0F(A0H, 26, A0H << 6, A0F33);
        int A08 = AnonymousClass5OF.A08(AnonymousClass5OF.A0H(A0F32, A0F34, A0F33, A0B4, i18) + 1126891415) + A0F34;
        int A0H2 = AnonymousClass5OF.A0H(A0F33, A08, A0F34, A0F32, i31) - 1416354905;
        int A0F35 = AnonymousClass5OF.A0F(A0H2, 17, A0H2 << 15, A08);
        int A0H3 = AnonymousClass5OF.A0H(A0F34, A0F35, A08, A0F33, i15) - 57434055;
        int A0F36 = AnonymousClass5OF.A0F(A0H3, 11, A0H3 << 21, A0F35);
        int A0H4 = AnonymousClass5OF.A0H(A08, A0F36, A0F35, A0F34, i27) + 1700485571;
        int A0F37 = AnonymousClass5OF.A0F(A0H4, 26, A0H4 << 6, A0F36);
        int A082 = AnonymousClass5OF.A08(AnonymousClass5OF.A0H(A0F35, A0F37, A0F36, A08, i11) - 1894986606) + A0F37;
        int A0H5 = AnonymousClass5OF.A0H(A0F36, A082, A0F37, A0F35, i23) - 1051523;
        int A0F38 = AnonymousClass5OF.A0F(A0H5, 17, A0H5 << 15, A082);
        int A0H6 = AnonymousClass5OF.A0H(A0F37, A0F38, A082, A0F36, i7) - 2054922799;
        int A0F39 = AnonymousClass5OF.A0F(A0H6, 11, A0H6 << 21, A0F38);
        int A0H7 = AnonymousClass5OF.A0H(A082, A0F39, A0F38, A0F37, i20) + 1873313359;
        int A0F40 = AnonymousClass5OF.A0F(A0H7, 26, A0H7 << 6, A0F39);
        int A083 = AnonymousClass5OF.A08(AnonymousClass5OF.A0H(A0F38, A0F40, A0F39, A082, i34) - 30611744) + A0F40;
        int A0H8 = AnonymousClass5OF.A0H(A0F39, A083, A0F40, A0F38, i16) - 1560198380;
        int A0F41 = AnonymousClass5OF.A0F(A0H8, 17, A0H8 << 15, A083);
        int A0H9 = AnonymousClass5OF.A0H(A0F40, A0F41, A083, A0F39, i29) + 1309151649;
        int A0F42 = AnonymousClass5OF.A0F(A0H9, 11, A0H9 << 21, A0F41);
        int A0H10 = AnonymousClass5OF.A0H(A083, A0F42, A0F41, A0F40, i13) - 145523070;
        int A0F43 = AnonymousClass5OF.A0F(A0H10, 26, A0H10 << 6, A0F42);
        int A084 = AnonymousClass5OF.A08(AnonymousClass5OF.A0H(A0F41, A0F43, A0F42, A083, i25) - 1120210379) + A0F43;
        int A0H11 = AnonymousClass5OF.A0H(A0F42, A084, A0F43, A0F41, i9) + 718787259;
        int A0F44 = AnonymousClass5OF.A0F(A0H11, 17, A0H11 << 15, A084);
        int A0H12 = AnonymousClass5OF.A0H(A0F43, A0F44, A084, A0F42, i22) - 343485551;
        int A0F45 = AnonymousClass5OF.A0F(A0H12, 11, A0H12 << 21, A0F44);
        this.A00 = i + A0F43;
        this.A01 = i2 + A0F45;
        this.A02 = i3 + A0F44;
        this.A03 = i4 + A084;
        this.A04 = 0;
        for (int i37 = 0; i37 != iArr.length; i37++) {
            iArr[i37] = 0;
        }
    }
}
