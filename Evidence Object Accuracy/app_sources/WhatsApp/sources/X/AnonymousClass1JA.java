package X;

/* JADX WARN: Init of enum A0I can be incorrect */
/* JADX WARN: Init of enum A0A can be incorrect */
/* JADX WARN: Init of enum A08 can be incorrect */
/* JADX WARN: Init of enum A03 can be incorrect */
/* JADX WARN: Init of enum A02 can be incorrect */
/* JADX WARN: Init of enum A0E can be incorrect */
/* JADX WARN: Init of enum A0C can be incorrect */
/* JADX WARN: Init of enum A0G can be incorrect */
/* JADX WARN: Init of enum A09 can be incorrect */
/* JADX WARN: Init of enum A01 can be incorrect */
/* JADX WARN: Init of enum A05 can be incorrect */
/* JADX WARN: Init of enum A06 can be incorrect */
/* JADX WARN: Init of enum A0H can be incorrect */
/* JADX WARN: Init of enum A07 can be incorrect */
/* JADX WARN: Init of enum A04 can be incorrect */
/* JADX WARN: Init of enum EF61 can be incorrect */
/* JADX WARN: Init of enum A0J can be incorrect */
/* JADX WARN: Init of enum A0B can be incorrect */
/* JADX WARN: Init of enum A0D can be incorrect */
/* JADX WARN: Init of enum A0K can be incorrect */
/* JADX WARN: Init of enum A0F can be incorrect */
/* renamed from: X.1JA  reason: invalid class name */
/* loaded from: classes2.dex */
public enum AnonymousClass1JA {
    A0I(r4, "REGISTRATION_FULL", 0, 0),
    A0A(r4, "INTERACTIVE_FULL", 1, 1),
    A08(r4, "INTERACTIVE_DELTA", 2, 2),
    A03(r4, "BACKGROUND_FULL", 3, 3),
    A02(r4, "BACKGROUND_DELTA", 4, 4),
    A0E(r29, "NOTIFICATION_CONTACT", 5, 5),
    A0C(r29, "INTERACTIVE_QUERY", 6, 6),
    A0G(r41, "NOTIFICATION_SIDELIST", 7, 7),
    A09(r41, "INTERACTIVE_DELTA_SIDELIST", 8, 8),
    A01(r29, "ADD_QUERY", 9, 9),
    A05(r49, "BACKGROUND_QUERY_PICTURES", 10, 16),
    A06(r49, "BACKGROUND_QUERY_PICTURES_PREVIEW", 11, 17),
    A0H(r49, "OUT_CONTACT_DISCOVERY", 12, 19),
    A07(r49, "CONTACT_DISCOVERY_DELTA", 13, 20),
    A04(r49, "BACKGROUND_MULTI_PROTOCOL_QUERY", 14, 21),
    /* Fake field, exist only in values array */
    EF61(r49, "BACKGROUND_FULL_MULTI_PROTOCOL_QUERY", 15, 22),
    A0J(r49, "REGISTRATION_FULL_MULTI_PROTOCOL_QUERY", 16, 23),
    A0B(r49, "INTERACTIVE_MULTI_PROTOCOL_QUERY", 17, 24),
    A0D(r49, "MESSAGE_MULTI_PROTOCOL_QUERY", 18, 25),
    A0K(r49, "VOIP_MULTI_PROTOCOL_QUERY", 19, 26),
    A0F(r49, "NOTIFICATION_MULTI_PROTOCOL_QUERY", 20, 27);
    
    public final int code;
    public final EnumC42291uy context;
    public final EnumC42301uz mode;
    public final EnumC42711vh scope;

    static {
        EnumC42291uy r2 = EnumC42291uy.REGISTRATION;
        EnumC42301uz r3 = EnumC42301uz.FULL;
        EnumC42711vh r4 = EnumC42711vh.PHONEBOOK_AND_SIDELIST;
        EnumC42291uy r8 = EnumC42291uy.INTERACTIVE;
        EnumC42301uz r15 = EnumC42301uz.DELTA;
        EnumC42291uy r20 = EnumC42291uy.BACKGROUND;
        EnumC42291uy r27 = EnumC42291uy.NOTIFICATION;
        EnumC42711vh r29 = EnumC42711vh.PHONEBOOK;
        EnumC42301uz r34 = EnumC42301uz.QUERY;
        EnumC42711vh r41 = EnumC42711vh.SIDELIST;
        EnumC42711vh r49 = EnumC42711vh.MULTI_PROTOCOLS_QUERY;
        EnumC42291uy r54 = EnumC42291uy.CONTACT_DISCOVERY;
    }

    AnonymousClass1JA(EnumC42711vh r3, String str, int i, int i2) {
        this.context = r1;
        this.mode = r2;
        this.scope = r3;
        this.code = i2;
    }
}
