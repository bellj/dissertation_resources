package X;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import java.lang.ref.WeakReference;

/* renamed from: X.38D  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass38D extends AbstractC16350or {
    public final int A00;
    public final int A01;
    public final int A02;
    public final Bitmap.CompressFormat A03;
    public final Bitmap A04;
    public final Rect A05;
    public final Uri A06;
    public final AnonymousClass01d A07;
    public final C14950mJ A08;
    public final WeakReference A09;
    public final boolean A0A;

    public AnonymousClass38D(Activity activity, Bitmap.CompressFormat compressFormat, Bitmap bitmap, Rect rect, Uri uri, AnonymousClass01d r7, C14950mJ r8, int i, int i2, int i3, boolean z) {
        this.A08 = r8;
        this.A07 = r7;
        this.A09 = C12970iu.A10(activity);
        this.A06 = uri;
        this.A03 = compressFormat;
        this.A01 = i;
        this.A04 = bitmap;
        this.A05 = rect;
        this.A0A = z;
        this.A00 = i2;
        this.A02 = i3;
    }
}
