package X;

import android.database.Cursor;
import com.whatsapp.jid.GroupJid;

/* renamed from: X.11D  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass11D {
    public final C18460sU A00;
    public final C232010t A01;

    public AnonymousClass11D(C18460sU r1, C232010t r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public Integer A00(GroupJid groupJid) {
        Integer num;
        C16310on A01 = this.A01.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT member_count FROM group_membership_count WHERE jid_row_id = ?", new String[]{Long.toString(this.A00.A01(groupJid))});
            if (A09.moveToNext()) {
                num = Integer.valueOf(A09.getInt(A09.getColumnIndexOrThrow("member_count")));
            } else {
                num = null;
            }
            A09.close();
            A01.close();
            return num;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
