package X;

import android.graphics.Bitmap;
import com.whatsapp.gallery.MediaGalleryFragmentBase;

/* renamed from: X.3XE  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3XE implements AnonymousClass23E {
    public final /* synthetic */ AbstractC35611iN A00;
    public final /* synthetic */ AnonymousClass2TW A01;
    public final /* synthetic */ AnonymousClass2T3 A02;
    public final /* synthetic */ AnonymousClass23D A03;

    @Override // X.AnonymousClass23E
    public /* synthetic */ void AQ8() {
    }

    public AnonymousClass3XE(AbstractC35611iN r1, AnonymousClass2TW r2, AnonymousClass2T3 r3, AnonymousClass23D r4) {
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = r4;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass23E
    public void A6L() {
        AnonymousClass2T3 r1 = this.A02;
        r1.setBackgroundColor(this.A01.A05.A01);
        r1.setImageDrawable(null);
    }

    @Override // X.AnonymousClass23E
    public void AWw(Bitmap bitmap, boolean z) {
        Bitmap bitmap2 = bitmap;
        MediaGalleryFragmentBase mediaGalleryFragmentBase = this.A01.A05;
        if (mediaGalleryFragmentBase.A0B() != null) {
            AnonymousClass2T3 r6 = this.A02;
            if (r6.getTag() == this.A03) {
                AbstractC35611iN r5 = this.A00;
                if (bitmap == MediaGalleryFragmentBase.A0U) {
                    bitmap2 = null;
                }
                AnonymousClass3GG.A01(bitmap2, mediaGalleryFragmentBase.A04, r5, r6, mediaGalleryFragmentBase.A01, !z);
            }
        }
    }
}
