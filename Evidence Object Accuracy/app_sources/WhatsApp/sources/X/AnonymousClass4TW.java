package X;

import java.util.List;

/* renamed from: X.4TW  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4TW {
    public final Double A00;
    public final String A01;
    public final String A02;
    public final String A03;
    public final List A04;
    public final List A05;
    public final List A06;

    public AnonymousClass4TW(Double d, String str, String str2, String str3, List list, List list2, List list3) {
        this.A05 = list;
        this.A04 = list2;
        this.A06 = list3;
        this.A01 = str;
        this.A03 = str2;
        this.A00 = d;
        this.A02 = str3;
    }
}
