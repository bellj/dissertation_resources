package X;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

/* renamed from: X.1mg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C37591mg implements HostnameVerifier {
    public final String A00;
    public final HostnameVerifier A01;

    public C37591mg(String str, HostnameVerifier hostnameVerifier) {
        this.A00 = str;
        this.A01 = hostnameVerifier;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && C37591mg.class == obj.getClass()) {
            C37591mg r4 = (C37591mg) obj;
            if (this.A00.equals(r4.A00)) {
                return this.A01.equals(r4.A01);
            }
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return (this.A00.hashCode() * 31) + this.A01.hashCode();
    }

    @Override // javax.net.ssl.HostnameVerifier
    public boolean verify(String str, SSLSession sSLSession) {
        return this.A01.verify(this.A00, sSLSession);
    }
}
