package X;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.webkit.WebView;
import android.widget.FrameLayout;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: X.3hK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C74103hK extends FrameLayout implements AnonymousClass5ST {
    public float A00 = 0.08f;
    public float A01 = 0.0533f;
    public C95274dQ A02 = C95274dQ.A06;
    public List A03 = Collections.emptyList();
    public final WebView A04;
    public final C73723gh A05;

    public C74103hK(Context context) {
        super(context, null);
        C73723gh r1 = new C73723gh(context, null);
        this.A05 = r1;
        C74063hF r0 = new C74063hF(context, this);
        this.A04 = r0;
        r0.setBackgroundColor(0);
        addView(r1);
        addView(r0);
    }

    public static String A00(int i) {
        Object[] objArr = new Object[4];
        C12960it.A1O(objArr, Color.red(i));
        C12980iv.A1T(objArr, Color.green(i));
        C12990iw.A1V(objArr, Color.blue(i));
        objArr[3] = Double.valueOf(((double) Color.alpha(i)) / 255.0d);
        return C72463ee.A0G("rgba(%d,%d,%d,%.3f)", objArr);
    }

    public static String A01(CharSequence charSequence) {
        return C88304Fc.A00.matcher(Html.escapeHtml(charSequence)).replaceAll("<br>");
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x002b  */
    /* JADX WARNING: Removed duplicated region for block: B:17:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String A02(float r5, int r6) {
        /*
            r4 = this;
            int r3 = r4.getHeight()
            int r1 = r4.getHeight()
            int r0 = r4.getPaddingTop()
            int r1 = r1 - r0
            int r0 = r4.getPaddingBottom()
            int r1 = r1 - r0
            r2 = -8388609(0xffffffffff7fffff, float:-3.4028235E38)
            int r0 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            if (r0 == 0) goto L_0x0023
            float r1 = (float) r1
            if (r6 == 0) goto L_0x0026
            r0 = 1
            float r1 = (float) r3
            if (r6 == r0) goto L_0x0026
            r0 = 2
            if (r6 == r0) goto L_0x0027
        L_0x0023:
            java.lang.String r0 = "unset"
            return r0
        L_0x0026:
            float r5 = r5 * r1
        L_0x0027:
            int r0 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            if (r0 == 0) goto L_0x0023
            android.content.Context r0 = r4.getContext()
            android.content.res.Resources r0 = r0.getResources()
            android.util.DisplayMetrics r0 = r0.getDisplayMetrics()
            float r0 = r0.density
            float r5 = r5 / r0
            r0 = 1
            java.lang.Object[] r2 = new java.lang.Object[r0]
            r1 = 0
            java.lang.Float r0 = java.lang.Float.valueOf(r5)
            r2[r1] = r0
            java.lang.String r0 = "%.2fpx"
            java.lang.String r0 = X.C72463ee.A0G(r0, r2)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C74103hK.A02(float, int):java.lang.String");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:115:0x0300, code lost:
        if (((android.text.style.TypefaceSpan) r12).getFamily() != null) goto L_0x0302;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:227:0x0528, code lost:
        if (r10 != false) goto L_0x052a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:228:0x052a, code lost:
        r8 = r30;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:229:0x052d, code lost:
        if (r10 != false) goto L_0x052f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:230:0x052f, code lost:
        r30 = r29;
        r1 = 2;
        r29 = r8;
     */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:198:0x046d  */
    /* JADX WARNING: Removed duplicated region for block: B:205:0x0480  */
    /* JADX WARNING: Removed duplicated region for block: B:207:0x0490  */
    /* JADX WARNING: Removed duplicated region for block: B:211:0x049c  */
    /* JADX WARNING: Removed duplicated region for block: B:227:0x0528  */
    /* JADX WARNING: Removed duplicated region for block: B:231:0x0536  */
    /* JADX WARNING: Removed duplicated region for block: B:233:0x0540  */
    /* JADX WARNING: Removed duplicated region for block: B:235:0x0548  */
    /* JADX WARNING: Removed duplicated region for block: B:252:0x05e2 A[LOOP:8: B:250:0x05dc->B:252:0x05e2, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00ec  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0102  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0119  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x012e  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0141  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x016b  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0180  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x01da  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x022f  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0232  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A03() {
        /*
        // Method dump skipped, instructions count: 1569
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C74103hK.A03():void");
    }

    @Override // X.AnonymousClass5ST
    public void AfK(C95274dQ r10, List list, float f, float f2, int i) {
        this.A02 = r10;
        this.A01 = f;
        this.A00 = f2;
        ArrayList A0l = C12960it.A0l();
        ArrayList A0l2 = C12960it.A0l();
        for (int i2 = 0; i2 < list.size(); i2++) {
            C93834ao r1 = (C93834ao) list.get(i2);
            if (r1.A0C != null) {
                A0l.add(r1);
            } else {
                A0l2.add(r1);
            }
        }
        if (!this.A03.isEmpty() || !A0l2.isEmpty()) {
            this.A03 = A0l2;
            A03();
        }
        this.A05.AfK(r10, A0l, f, f2, 0);
        invalidate();
    }

    @Override // android.widget.FrameLayout, android.view.View, android.view.ViewGroup
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        if (z && !this.A03.isEmpty()) {
            A03();
        }
    }
}
