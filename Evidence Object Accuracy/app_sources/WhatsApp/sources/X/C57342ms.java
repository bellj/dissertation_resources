package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2ms  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57342ms extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57342ms A04;
    public static volatile AnonymousClass255 A05;
    public int A00;
    public int A01 = 1;
    public int A02;
    public String A03 = "";

    static {
        C57342ms r0 = new C57342ms();
        A04 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r7, Object obj, Object obj2) {
        AnonymousClass4BF r0;
        switch (r7.ordinal()) {
            case 0:
                return A04;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                C57342ms r9 = (C57342ms) obj2;
                int i = this.A00;
                boolean A1R = C12960it.A1R(i);
                String str = this.A03;
                int i2 = r9.A00;
                this.A03 = r8.Afy(str, r9.A03, A1R, C12960it.A1R(i2));
                this.A02 = r8.Afp(this.A02, r9.A02, C12960it.A1V(i & 2, 2), C12960it.A1V(i2 & 2, 2));
                this.A01 = r8.Afp(this.A01, r9.A01, C12960it.A1V(i & 4, 4), C12960it.A1V(i2 & 4, 4));
                if (r8 == C463025i.A00) {
                    this.A00 = i | i2;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        int A03 = r82.A03();
                        if (A03 == 0) {
                            break;
                        } else if (A03 == 10) {
                            String A0A = r82.A0A();
                            this.A00 = 1 | this.A00;
                            this.A03 = A0A;
                        } else if (A03 == 16) {
                            int A02 = r82.A02();
                            if (A02 == 0) {
                                r0 = AnonymousClass4BF.A03;
                            } else if (A02 == 1) {
                                r0 = AnonymousClass4BF.A01;
                            } else if (A02 == 2) {
                                r0 = AnonymousClass4BF.A02;
                            } else if (A02 != 3) {
                                r0 = null;
                            } else {
                                r0 = AnonymousClass4BF.A04;
                            }
                            if (r0 == null) {
                                super.A0X(2, A02);
                            } else {
                                this.A00 |= 2;
                                this.A02 = A02;
                            }
                        } else if (A03 == 24) {
                            this.A00 |= 4;
                            this.A01 = r82.A02();
                        } else if (!A0a(r82, A03)) {
                            break;
                        }
                    } catch (C28971Pt e) {
                        throw AbstractC27091Fz.A0J(e, this);
                    } catch (IOException e2) {
                        throw AbstractC27091Fz.A0K(this, e2);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C57342ms();
            case 5:
                return new C82033uy();
            case 6:
                break;
            case 7:
                if (A05 == null) {
                    synchronized (C57342ms.class) {
                        if (A05 == null) {
                            A05 = AbstractC27091Fz.A09(A04);
                        }
                    }
                }
                return A05;
            default:
                throw C12970iu.A0z();
        }
        return A04;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            i2 = AbstractC27091Fz.A04(1, this.A03, 0);
        }
        int i3 = this.A00;
        if ((i3 & 2) == 2) {
            i2 = AbstractC27091Fz.A03(2, this.A02, i2);
        }
        if ((i3 & 4) == 4) {
            i2 += CodedOutputStream.A03(3, this.A01);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A03);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0E(2, this.A02);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0E(3, this.A01);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
