package X;

/* renamed from: X.0Fa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02830Fa extends AbstractC05330Pd {
    public final /* synthetic */ C07740a0 A00;

    @Override // X.AbstractC05330Pd
    public String A01() {
        return "UPDATE workspec SET run_attempt_count=run_attempt_count+1 WHERE id=?";
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C02830Fa(AnonymousClass0QN r1, C07740a0 r2) {
        super(r1);
        this.A00 = r2;
    }
}
