package X;

/* renamed from: X.5M5  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5M5 extends C95294dS {
    public AnonymousClass5M5(C94464br r1) {
        super(r1);
    }

    @Override // X.C95294dS
    public void A0D(C95404de r4, AnonymousClass4YW r5, int i, int i2) {
        super.A0D(r4, r5, i, i2);
        C95294dS r2 = new C95294dS(null);
        A0E(r2, r5, 0);
        this.A05 = r2.A05;
        this.A06 = r2.A06;
        this.A02 = 0;
        this.A07 = r2.A07;
        this.A08 = r2.A08;
        this.A03 = r2.A03;
        this.A00 = r2.A00;
        this.A04 = r2.A04;
    }
}
