package X;

import android.os.Handler;
import com.whatsapp.payments.ui.NoviSelfieCameraView;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;

/* renamed from: X.5oe  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124125oe extends AbstractC16350or {
    public final File A00;
    public final WeakReference A01;
    public final byte[] A02;

    public C124125oe(NoviSelfieCameraView noviSelfieCameraView, File file, byte[] bArr) {
        this.A02 = bArr;
        this.A00 = file;
        this.A01 = C12970iu.A10(noviSelfieCameraView);
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        Throwable e;
        StringBuilder sb;
        String str;
        Log.i("SavePictureTask/doInBackground start");
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(this.A00);
            fileOutputStream.write(this.A02);
            Log.i("SavePictureTask/doInBackground end");
            fileOutputStream.close();
            return null;
        } catch (FileNotFoundException e2) {
            e = e2;
            sb = C12960it.A0h();
            str = "PAY: NoviSelfieCameraView/File not found: ";
            sb.append(str);
            Log.e(C12960it.A0d(e.getMessage(), sb), e);
            return null;
        } catch (IOException e3) {
            e = e3;
            sb = C12960it.A0h();
            str = "PAY: NoviSelfieCameraView/Error accessing file: ";
            sb.append(str);
            Log.e(C12960it.A0d(e.getMessage(), sb), e);
            return null;
        }
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        NoviSelfieCameraView noviSelfieCameraView = (NoviSelfieCameraView) this.A01.get();
        if (noviSelfieCameraView != null) {
            Log.i("SavePictureTask/onPostExecute");
            noviSelfieCameraView.A0B(noviSelfieCameraView.A05, 240000, 0);
            Runnable runnable = noviSelfieCameraView.A08;
            if (runnable != null) {
                runnable.run();
            }
            Handler A0E = C12970iu.A0E();
            noviSelfieCameraView.A01 = A0E;
            RunnableC134926Gm r2 = new Runnable() { // from class: X.6Gm
                @Override // java.lang.Runnable
                public final void run() {
                    NoviSelfieCameraView noviSelfieCameraView2 = NoviSelfieCameraView.this;
                    noviSelfieCameraView2.AeT();
                    Runnable runnable2 = noviSelfieCameraView2.A06;
                    if (runnable2 != null) {
                        runnable2.run();
                    }
                }
            };
            noviSelfieCameraView.A07 = r2;
            A0E.postDelayed(r2, (long) noviSelfieCameraView.A00);
        }
    }
}
