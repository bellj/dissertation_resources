package X;

import java.util.Arrays;

/* renamed from: X.2oa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58292oa extends AnonymousClass3HU {
    public boolean A00 = true;
    public final C231510o A01;

    public /* synthetic */ C58292oa(C231510o r2) {
        this.A01 = r2;
    }

    @Override // X.AnonymousClass3HU
    public void A01(C37471mS[] r2) {
        C37471mS[] r0 = super.A00;
        if (r0 != null ? !Arrays.equals(r0, r2) : r2 != null) {
            this.A00 = true;
        }
        super.A01(r2);
    }
}
