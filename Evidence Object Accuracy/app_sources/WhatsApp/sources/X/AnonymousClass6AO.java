package X;

import com.whatsapp.payments.ui.IndiaUpiSendPaymentActivity;

/* renamed from: X.6AO  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6AO implements AnonymousClass6MQ {
    public final /* synthetic */ IndiaUpiSendPaymentActivity A00;

    public AnonymousClass6AO(IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity) {
        this.A00 = indiaUpiSendPaymentActivity;
    }

    @Override // X.AnonymousClass6MQ
    public void AOb(C119705ey r5) {
        IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A00;
        ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0k = false;
        indiaUpiSendPaymentActivity.AaN();
        if (r5 != null && !indiaUpiSendPaymentActivity.A3a(r5)) {
            C30931Zj r3 = ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0m;
            StringBuilder A0k = C12960it.A0k("starting onContactVpa for jid: ");
            A0k.append(((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0C);
            A0k.append(" vpa: ");
            A0k.append(r5.A02);
            A0k.append(" receiverVpaId: ");
            r3.A06(C12960it.A0d(r5.A03, A0k));
            ((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A08 = r5.A02;
            ((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A0M = r5.A03;
            if (!AnonymousClass1ZS.A02(r5.A01)) {
                ((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A06 = r5.A01;
            }
            indiaUpiSendPaymentActivity.A3c();
        }
    }

    @Override // X.AnonymousClass6MQ
    public void APo(C452120p r5) {
        IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A00;
        ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0k = false;
        indiaUpiSendPaymentActivity.AaN();
        if (!AnonymousClass69E.A02(indiaUpiSendPaymentActivity, "upi-get-vpa", r5.A00, false)) {
            C30931Zj r2 = ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0m;
            StringBuilder A0k = C12960it.A0k("could not get vpa for jid: ");
            A0k.append(((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0C);
            r2.A06(C12960it.A0d("; showErrorAndFinish", A0k));
            indiaUpiSendPaymentActivity.A39();
        }
    }
}
