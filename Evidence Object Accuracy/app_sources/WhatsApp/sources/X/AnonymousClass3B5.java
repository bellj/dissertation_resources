package X;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* renamed from: X.3B5  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3B5 {
    public static final BlockingQueue A00;
    public static final ThreadFactory A01;
    public static final ThreadPoolExecutor A02;

    static {
        LinkedBlockingQueue linkedBlockingQueue = new LinkedBlockingQueue(4);
        A00 = linkedBlockingQueue;
        ThreadFactoryC71523d4 r8 = new ThreadFactoryC71523d4();
        A01 = r8;
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(0, 4, 1, TimeUnit.SECONDS, linkedBlockingQueue, r8);
        A02 = threadPoolExecutor;
        threadPoolExecutor.setRejectedExecutionHandler(new AnonymousClass5E9());
    }
}
