package X;

import android.os.Message;
import android.os.SystemClock;
import com.facebook.redex.RunnableBRunnable0Shape0S0510000_I0;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;

/* renamed from: X.0zr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22920zr {
    public long A00 = 0;
    public long A01 = 0;
    public boolean A02;
    public boolean A03;
    public C29211Rh[] A04;
    public final C14900mE A05;
    public final C15570nT A06;
    public final C16240og A07;
    public final C26561Dy A08;
    public final C14830m7 A09;
    public final C14820m6 A0A;
    public final C15990oG A0B;
    public final C18240s8 A0C;
    public final C17220qS A0D;
    public final AnonymousClass100 A0E;
    public final C26621Ee A0F;
    public final AnonymousClass105 A0G;
    public final AnonymousClass1VN A0H = new AnonymousClass1VN(10, 610);
    public final AbstractC14440lR A0I;

    public C22920zr(C14900mE r6, C15570nT r7, C16240og r8, C26561Dy r9, C14830m7 r10, C14820m6 r11, C15990oG r12, C18240s8 r13, C17220qS r14, AnonymousClass100 r15, C26621Ee r16, AnonymousClass105 r17, AbstractC14440lR r18) {
        this.A09 = r10;
        this.A05 = r6;
        this.A06 = r7;
        this.A0I = r18;
        this.A0D = r14;
        this.A0C = r13;
        this.A07 = r8;
        this.A0B = r12;
        this.A0A = r11;
        this.A0E = r15;
        this.A0G = r17;
        this.A08 = r9;
        this.A0F = r16;
    }

    public synchronized void A00() {
        this.A00 = 0;
    }

    public synchronized void A01() {
        if (this.A07.A06) {
            long j = this.A00;
            if (j == 0 || SystemClock.uptimeMillis() - j > 60000) {
                this.A0D.A08(Message.obtain(null, 0, 88, 0), false);
                this.A00 = SystemClock.uptimeMillis();
            }
        }
        StringBuilder sb = new StringBuilder();
        sb.append("MyPreKeysManager/send-get-pre-key-digest/skip-digest-check last:");
        sb.append(this.A00);
        Log.i(sb.toString());
    }

    public synchronized void A02() {
        long uptimeMillis = SystemClock.uptimeMillis();
        if (this.A07.A06) {
            long j = this.A01;
            if (j == 0 || uptimeMillis - j > 180000) {
                A04(uptimeMillis);
                A05(true);
            }
        }
        StringBuilder sb = new StringBuilder();
        sb.append("MyPreKeysManager/sendSetPreKey/skip last:");
        sb.append(this.A01);
        Log.i(sb.toString());
    }

    public final synchronized void A03() {
        if (this.A03) {
            this.A03 = false;
            this.A0H.A02();
        }
    }

    public final synchronized void A04(long j) {
        this.A01 = j;
    }

    public final void A05(boolean z) {
        this.A0C.A00();
        C15990oG r3 = this.A0B;
        int A01 = r3.A07.A01();
        byte[] A0k = r3.A0k();
        Lock A00 = r3.A0J.A00(r3.A0I());
        try {
            if (A00 == null) {
                r3.A0I.A00();
            } else {
                A00.lock();
            }
            C31191a9 r1 = r3.A00.A00;
            r1.A08.A00();
            List<AnonymousClass1RW> A012 = r1.A04.A01();
            ArrayList arrayList = new ArrayList();
            for (AnonymousClass1RW r5 : A012) {
                try {
                    arrayList.add(C31191a9.A00(new C31761b4(r5.A01), r5.A00));
                } catch (IOException e) {
                    StringBuilder sb = new StringBuilder("error reading prekey ");
                    sb.append(r5.A00);
                    Log.e(sb.toString(), e);
                }
            }
            StringBuilder sb2 = new StringBuilder("axolotl reporting back ");
            sb2.append(arrayList.size());
            sb2.append(" prekeys for sending to the server");
            Log.i(sb2.toString());
            C29211Rh[] r12 = (C29211Rh[]) arrayList.toArray(new C29211Rh[0]);
            C29211Rh A0M = r3.A0M();
            this.A05.A0H(new RunnableBRunnable0Shape0S0510000_I0(this, A0k, C16050oM.A03(A01), r12, A0M, 2, z));
        } finally {
            if (A00 != null) {
                A00.unlock();
            }
        }
    }
}
