package X;

/* renamed from: X.20G  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass20G implements AnonymousClass20H {
    public static final byte[] A09 = AnonymousClass1T7.A03("expand 32-byte k");
    public static final byte[] A0A = AnonymousClass1T7.A03("expand 16-byte k");
    public static final int[] A0B;
    public int A00;
    public int A01;
    public int A02;
    public int A03 = 0;
    public int A04 = 20;
    public boolean A05 = false;
    public byte[] A06 = new byte[64];
    public int[] A07 = new int[16];
    public int[] A08 = new int[16];

    static {
        byte[] A03 = AnonymousClass1T7.A03("expand 16-byte kexpand 32-byte k");
        int i = 0;
        int[] iArr = new int[8];
        int i2 = 0;
        do {
            iArr[i2] = AbstractC95434di.A00(A03, i);
            i += 4;
            i2++;
        } while (i2 < 8);
        A0B = iArr;
    }

    public static void A00(int[] iArr, int[] iArr2, int i) {
        if (iArr.length != 16) {
            throw new IllegalArgumentException();
        } else if (iArr2.length != 16) {
            throw new IllegalArgumentException();
        } else if (i % 2 == 0) {
            int i2 = iArr[0];
            int i3 = iArr[1];
            int i4 = iArr[2];
            int i5 = iArr[3];
            int i6 = iArr[4];
            int i7 = iArr[5];
            int i8 = iArr[6];
            int i9 = iArr[7];
            int i10 = iArr[8];
            int i11 = 9;
            int i12 = iArr[9];
            int i13 = iArr[10];
            int i14 = iArr[11];
            int i15 = iArr[12];
            int i16 = 13;
            int i17 = iArr[13];
            int i18 = iArr[14];
            int i19 = iArr[15];
            while (i > 0) {
                int rotateLeft = Integer.rotateLeft(i2 + i15, 7) ^ i6;
                int rotateLeft2 = i10 ^ Integer.rotateLeft(rotateLeft + i2, i11);
                int rotateLeft3 = i15 ^ Integer.rotateLeft(rotateLeft2 + rotateLeft, i16);
                int rotateLeft4 = Integer.rotateLeft(rotateLeft3 + rotateLeft2, 18) ^ i2;
                int rotateLeft5 = i12 ^ Integer.rotateLeft(i7 + i3, 7);
                int rotateLeft6 = i17 ^ Integer.rotateLeft(rotateLeft5 + i7, i11);
                int rotateLeft7 = i3 ^ Integer.rotateLeft(rotateLeft6 + rotateLeft5, i16);
                int rotateLeft8 = Integer.rotateLeft(rotateLeft7 + rotateLeft6, 18) ^ i7;
                int rotateLeft9 = i18 ^ Integer.rotateLeft(i13 + i8, 7);
                int rotateLeft10 = i4 ^ Integer.rotateLeft(rotateLeft9 + i13, i11);
                int rotateLeft11 = i8 ^ Integer.rotateLeft(rotateLeft10 + rotateLeft9, 13);
                int rotateLeft12 = i13 ^ Integer.rotateLeft(rotateLeft11 + rotateLeft10, 18);
                int rotateLeft13 = i5 ^ Integer.rotateLeft(i19 + i14, 7);
                int rotateLeft14 = i9 ^ Integer.rotateLeft(rotateLeft13 + i19, i11);
                int rotateLeft15 = i14 ^ Integer.rotateLeft(rotateLeft14 + rotateLeft13, 13);
                int rotateLeft16 = i19 ^ Integer.rotateLeft(rotateLeft15 + rotateLeft14, 18);
                i3 = rotateLeft7 ^ Integer.rotateLeft(rotateLeft4 + rotateLeft13, 7);
                i4 = rotateLeft10 ^ Integer.rotateLeft(i3 + rotateLeft4, i11);
                i5 = rotateLeft13 ^ Integer.rotateLeft(i4 + i3, 13);
                i2 = rotateLeft4 ^ Integer.rotateLeft(i5 + i4, 18);
                i8 = rotateLeft11 ^ Integer.rotateLeft(rotateLeft8 + rotateLeft, 7);
                i9 = rotateLeft14 ^ Integer.rotateLeft(i8 + rotateLeft8, i11);
                i6 = Integer.rotateLeft(i9 + i8, 13) ^ rotateLeft;
                i7 = rotateLeft8 ^ Integer.rotateLeft(i6 + i9, 18);
                i14 = rotateLeft15 ^ Integer.rotateLeft(rotateLeft12 + rotateLeft5, 7);
                int rotateLeft17 = Integer.rotateLeft(i14 + rotateLeft12, i11) ^ rotateLeft2;
                i16 = 13;
                i12 = rotateLeft5 ^ Integer.rotateLeft(rotateLeft17 + i14, 13);
                i13 = rotateLeft12 ^ Integer.rotateLeft(i12 + rotateLeft17, 18);
                i15 = rotateLeft3 ^ Integer.rotateLeft(rotateLeft16 + rotateLeft9, 7);
                i17 = rotateLeft6 ^ Integer.rotateLeft(i15 + rotateLeft16, 9);
                i18 = rotateLeft9 ^ Integer.rotateLeft(i17 + i15, 13);
                i19 = rotateLeft16 ^ Integer.rotateLeft(i18 + i17, 18);
                i -= 2;
                i10 = rotateLeft17;
                i11 = 9;
            }
            iArr2[0] = i2 + iArr[0];
            iArr2[1] = i3 + iArr[1];
            iArr2[2] = i4 + iArr[2];
            iArr2[3] = i5 + iArr[3];
            iArr2[4] = i6 + iArr[4];
            iArr2[5] = i7 + iArr[5];
            iArr2[6] = i8 + iArr[6];
            iArr2[7] = i9 + iArr[7];
            iArr2[8] = i10 + iArr[8];
            iArr2[i11] = i12 + iArr[i11];
            iArr2[10] = i13 + iArr[10];
            iArr2[11] = i14 + iArr[11];
            iArr2[12] = i15 + iArr[12];
            iArr2[i16] = i17 + iArr[i16];
            iArr2[14] = i18 + iArr[14];
            iArr2[15] = i19 + iArr[15];
        } else {
            throw new IllegalArgumentException("Number of rounds must be even");
        }
    }

    public void A01(byte[] bArr, byte[] bArr2, int i, int i2, int i3) {
        if (!this.A05) {
            StringBuilder sb = new StringBuilder("XSalsa20");
            sb.append(" not initialised");
            throw new IllegalStateException(sb.toString());
        } else if (i + i2 > bArr.length) {
            throw new AnonymousClass5O2("input buffer too short");
        } else if (i3 + i2 <= bArr2.length) {
            int i4 = this.A00 + i2;
            this.A00 = i4;
            if (i4 < i2 && i4 >= 0) {
                int i5 = this.A01 + 1;
                this.A01 = i5;
                if (i5 == 0) {
                    int i6 = this.A02 + 1;
                    this.A02 = i6;
                    if ((i6 & 32) != 0) {
                        throw new AnonymousClass5O3("2^70 byte limit per IV would be exceeded; Change IV");
                    }
                }
            }
            for (int i7 = 0; i7 < i2; i7++) {
                byte[] bArr3 = this.A06;
                int i8 = this.A03;
                bArr2[i7 + i3] = (byte) (bArr3[i8] ^ bArr[i7 + i]);
                int i9 = (i8 + 1) & 63;
                this.A03 = i9;
                if (i9 == 0) {
                    int[] iArr = this.A07;
                    int i10 = iArr[8] + 1;
                    iArr[8] = i10;
                    if (i10 == 0) {
                        iArr[9] = iArr[9] + 1;
                    }
                    int i11 = this.A04;
                    int[] iArr2 = this.A08;
                    A00(iArr, iArr2, i11);
                    int i12 = 0;
                    for (int i13 : iArr2) {
                        AbstractC95434di.A02(bArr3, i13, i12);
                        i12 += 4;
                    }
                }
            }
        } else {
            throw new C114975Nu("output buffer too short");
        }
    }
}
