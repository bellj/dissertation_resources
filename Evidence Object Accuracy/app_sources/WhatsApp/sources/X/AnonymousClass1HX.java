package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.whatsapp.util.Log;

/* renamed from: X.1HX  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1HX extends BroadcastReceiver {
    public final /* synthetic */ AnonymousClass12O A00;

    public AnonymousClass1HX(AnonymousClass12O r1) {
        this.A00 = r1;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        AnonymousClass1MY.A08();
        AnonymousClass12O r4 = this.A00;
        C43831xf A01 = r4.A08.A01();
        if (A01 != null) {
            int i = A01.A00;
            StringBuilder sb = new StringBuilder("UserNoticeManager/handleLocaleChange/notice id:");
            sb.append(i);
            Log.i(sb.toString());
            AnonymousClass12M r1 = r4.A07;
            r1.A04(i);
            if (r4.A05.A01() && !C43901xo.A01(r4.A03, A01)) {
                r1.A05(i);
            }
        }
    }
}
