package X;

import com.whatsapp.phonematching.CountryPicker;

/* renamed from: X.4rL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103804rL implements AnonymousClass07L {
    public final /* synthetic */ CountryPicker A00;

    @Override // X.AnonymousClass07L
    public boolean AUY(String str) {
        return false;
    }

    public C103804rL(CountryPicker countryPicker) {
        this.A00 = countryPicker;
    }

    @Override // X.AnonymousClass07L
    public boolean AUX(String str) {
        this.A00.A04.getFilter().filter(str);
        return false;
    }
}
