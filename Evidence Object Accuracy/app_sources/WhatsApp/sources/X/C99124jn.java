package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

/* renamed from: X.4jn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C99124jn implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        ArrayList arrayList = null;
        String str = null;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c != 1) {
                str = C95664e9.A09(parcel, str, c, 2, readInt);
            } else {
                arrayList = C95664e9.A0A(parcel, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78673pI(str, arrayList);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new C78673pI[i];
    }
}
