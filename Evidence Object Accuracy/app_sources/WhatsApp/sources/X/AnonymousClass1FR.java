package X;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import com.whatsapp.util.Log;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;

/* renamed from: X.1FR  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1FR implements AbstractC15920o8 {
    public final AnonymousClass1FQ A00;

    public AnonymousClass1FR(AnonymousClass1FQ r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC15920o8
    public int[] ADF() {
        return new int[]{179, 223};
    }

    @Override // X.AbstractC15920o8
    public boolean AI8(Message message, int i) {
        if (i == 179) {
            Bundle data = message.getData();
            String string = data.getString("nonce");
            AnonymousClass009.A05(string);
            String string2 = data.getString("apiKey");
            AnonymousClass009.A05(string2);
            AnonymousClass1FQ r6 = this.A00;
            Context context = r6.A00.A00;
            int A00 = AnonymousClass1UB.A00(context);
            if (A00 == 0) {
                AnonymousClass2RO r0 = new AnonymousClass2RO(context);
                byte[] bytes = string.getBytes();
                AnonymousClass1U8 r02 = r0.A05;
                AnonymousClass2RR r4 = new AnonymousClass2RR(r02, string2, bytes);
                r02.A05(r4);
                AnonymousClass2RV r3 = new AnonymousClass2RV(new AnonymousClass2RT());
                AnonymousClass2RY r2 = AnonymousClass2RX.A00;
                C13690kA r1 = new C13690kA();
                r4.A00(new AnonymousClass2RZ(r4, r3, r2, r1));
                C13600jz r22 = r1.A00;
                C50802Rb r03 = new AbstractC13640k3() { // from class: X.2Rb
                    @Override // X.AbstractC13640k3
                    public final void AX4(Object obj) {
                        String str;
                        AnonymousClass1FQ r23 = AnonymousClass1FQ.this;
                        C77973oA r04 = ((C1091150l) ((AbstractC117095Yk) ((AnonymousClass2RU) obj).A00)).A01;
                        if (r04 == null) {
                            str = null;
                        } else {
                            str = r04.A00;
                        }
                        C20660w7 r24 = r23.A01;
                        if (str != null) {
                            r24.A0I(str, null, 0);
                        } else {
                            r24.A0I(null, "Attestation API returned NULL as JWS result", 1000);
                        }
                    }
                };
                Executor executor = C13650k5.A00;
                r22.A06(r03, executor);
                r22.A05(new AbstractC13630k2() { // from class: X.2Rc
                    @Override // X.AbstractC13630k2
                    public final void AQA(Exception exc) {
                        int i2;
                        AnonymousClass1FQ r12 = AnonymousClass1FQ.this;
                        String message2 = exc.getMessage();
                        if (exc instanceof C630439z) {
                            i2 = ((C630439z) exc).mStatus.A01;
                        } else {
                            i2 = 500;
                        }
                        r12.A01.A0I(null, message2, i2);
                        Log.e("requestAttestation/onError", exc);
                    }
                }, executor);
                return true;
            }
            C20660w7 r42 = r6.A01;
            StringBuilder sb = new StringBuilder("Google Play Services Unavailable. Connection result code: ");
            sb.append(A00);
            r42.A0I(null, sb.toString(), 1001);
            Log.i("requestAttestation/cannot request attestation Google APIs unavailable");
            return true;
        } else if (i != 223) {
            return false;
        } else {
            int i2 = message.getData().getInt("maxAppsCount");
            AnonymousClass1FQ r62 = this.A00;
            Context context2 = r62.A00.A00;
            int A002 = AnonymousClass1UB.A00(context2);
            if (A002 == 0) {
                AnonymousClass1U8 r04 = new AnonymousClass2RO(context2).A05;
                C50822Rd r43 = new C50822Rd(r04);
                r04.A05(r43);
                AnonymousClass2RV r32 = new AnonymousClass2RV(new C50832Re());
                AnonymousClass2RY r23 = AnonymousClass2RX.A00;
                C13690kA r12 = new C13690kA();
                r43.A00(new AnonymousClass2RZ(r43, r32, r23, r12));
                C13600jz r24 = r12.A00;
                C50842Rf r05 = new AbstractC13640k3(i2) { // from class: X.2Rf
                    public final /* synthetic */ int A00;

                    {
                        this.A00 = r2;
                    }

                    @Override // X.AbstractC13640k3
                    public final void AX4(Object obj) {
                        List<C78313oi> asList;
                        AnonymousClass1FQ r8 = AnonymousClass1FQ.this;
                        int i3 = this.A00;
                        C78393oq r06 = ((C1091250m) ((AbstractC117105Yl) ((AnonymousClass2RU) obj).A00)).A01;
                        if (r06 == null) {
                            asList = Collections.emptyList();
                        } else {
                            asList = Arrays.asList(r06.A03);
                        }
                        C32581cO r63 = new C32581cO(asList.size());
                        int i4 = 0;
                        for (C78313oi r13 : asList) {
                            if (i4 >= i3) {
                                break;
                            }
                            r63.A01.add(new C49602Ll(r13.A01, AnonymousClass1US.A0B(r13.A02), r13.A00));
                            i4++;
                        }
                        r8.A01.A0G(r63, null, 0);
                    }
                };
                Executor executor2 = C13650k5.A00;
                r24.A06(r05, executor2);
                r24.A05(new AbstractC13630k2() { // from class: X.2Rg
                    @Override // X.AbstractC13630k2
                    public final void AQA(Exception exc) {
                        int i3;
                        AnonymousClass1FQ r13 = AnonymousClass1FQ.this;
                        Log.e("requestHarmfulApps/onError", exc);
                        String message2 = exc.getMessage();
                        if (exc instanceof C630439z) {
                            i3 = ((C630439z) exc).mStatus.A01;
                        } else {
                            i3 = 500;
                        }
                        r13.A01.A0G(null, message2, i3);
                    }
                }, executor2);
                return true;
            }
            C20660w7 r44 = r62.A01;
            StringBuilder sb2 = new StringBuilder("Google Play Services Unavailable. Connection result code: ");
            sb2.append(A002);
            r44.A0G(null, sb2.toString(), 1001);
            Log.i("requestHarmfulApps/Google APIs unavailable");
            return true;
        }
    }
}
