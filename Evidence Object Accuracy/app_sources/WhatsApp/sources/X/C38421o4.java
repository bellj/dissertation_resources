package X;

import com.whatsapp.util.Log;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/* renamed from: X.1o4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38421o4 {
    public AbstractC16130oV A00;
    public final CopyOnWriteArrayList A01;

    public C38421o4(List list) {
        this.A01 = new CopyOnWriteArrayList(list);
        AbstractC16130oV r0 = (AbstractC16130oV) list.get(0);
        this.A00 = r0;
        C16150oX r2 = r0.A02;
        AnonymousClass009.A0A("First media data is null", r2 != null);
        Iterator it = this.A01.iterator();
        while (it.hasNext()) {
            AbstractC16130oV r4 = (AbstractC16130oV) it.next();
            C16150oX r3 = r4.A02;
            AnonymousClass009.A0A("Media data is null", r3 != null);
            AnonymousClass009.A0A("Media type mismatch", this.A00.A0y == r4.A0y);
            AnonymousClass009.A0A("Origin mismatch", ((AbstractC15340mz) this.A00).A08 == ((AbstractC15340mz) r4).A08);
            AnonymousClass009.A0A("Caption mismatch", C29941Vi.A00(this.A00.A15(), r4.A15()));
            AnonymousClass009.A0A("Hash mismatch", C29941Vi.A00(this.A00.A05, r4.A05));
            AnonymousClass009.A0A("Encrypted hash mismatch", C29941Vi.A00(this.A00.A04, r4.A04));
            boolean z = false;
            if (this.A00.A00 == r4.A00) {
                z = true;
            }
            AnonymousClass009.A0A("Duration mismatch", z);
            AnonymousClass009.A0A("Mime mismatch", C29941Vi.A00(this.A00.A06, r4.A06));
            AnonymousClass009.A0A("Name mismatch", C29941Vi.A00(this.A00.A16(), r4.A16()));
            AnonymousClass009.A0A("Multicast id mismatch", C29941Vi.A00(this.A00.A09, r4.A09));
            AnonymousClass009.A05(r2);
            String str = r2.A0I;
            AnonymousClass009.A05(r3);
            AnonymousClass009.A0A("Media Job Id mismatch", C29941Vi.A00(str, r3.A0I));
        }
    }

    public synchronized AbstractC16130oV A00() {
        return this.A00;
    }

    public String A01() {
        StringBuilder sb = new StringBuilder();
        Iterator it = this.A01.iterator();
        while (it.hasNext()) {
            AbstractC15340mz r1 = (AbstractC15340mz) it.next();
            if (sb.length() != 0) {
                sb.append(',');
            }
            sb.append(r1.A0z);
        }
        return sb.toString();
    }

    public synchronized void A02(AnonymousClass1IS r6) {
        AbstractC16130oV r1;
        StringBuilder sb = new StringBuilder();
        sb.append("messagelist/remove ");
        sb.append(r6);
        sb.append(" from ");
        sb.append(A01());
        Log.i(sb.toString());
        CopyOnWriteArrayList copyOnWriteArrayList = this.A01;
        if (r6 != null) {
            Iterator it = copyOnWriteArrayList.iterator();
            while (it.hasNext()) {
                r1 = (AbstractC16130oV) it.next();
                if (r6.equals(r1.A0z)) {
                    break;
                }
            }
        }
        r1 = null;
        copyOnWriteArrayList.remove(r1);
        if (!copyOnWriteArrayList.isEmpty()) {
            this.A00 = (AbstractC16130oV) copyOnWriteArrayList.get(0);
        }
    }

    public boolean A03() {
        Iterator it = this.A01.iterator();
        while (it.hasNext()) {
            if (!C15380n4.A0N(((AbstractC15340mz) it.next()).A0z.A00)) {
                return true;
            }
        }
        return false;
    }

    public boolean A04() {
        Iterator it = this.A01.iterator();
        while (it.hasNext()) {
            if (C15380n4.A0N(((AbstractC15340mz) it.next()).A0z.A00)) {
                return true;
            }
        }
        return false;
    }
}
