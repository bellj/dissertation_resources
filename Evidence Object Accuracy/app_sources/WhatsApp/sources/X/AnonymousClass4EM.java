package X;

/* renamed from: X.4EM  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4EM {
    public static void A00(C16330op r1) {
        r1.A0B("CREATE INDEX IF NOT EXISTS message_chat_sort_id_index ON message (chat_row_id, sort_id)");
        r1.A0B("CREATE INDEX IF NOT EXISTS message_starred_sort_id_index ON message (starred, sort_id)");
    }
}
