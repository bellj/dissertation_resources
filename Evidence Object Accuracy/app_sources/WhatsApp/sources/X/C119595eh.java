package X;

import java.util.List;
import java.util.Map;

/* renamed from: X.5eh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119595eh extends AbstractC17440qo {
    public AbstractC136556Mz A00;

    public C119595eh(AbstractC136556Mz r4) {
        super("bk.action.qpl.userflow.AnnotateV2", "bk.action.qpl.userflow.EndFlowCancelV2", "bk.action.qpl.userflow.EndFlowFailureV2", "bk.action.qpl.userflow.EndFlowSuccessV2", "bk.action.qpl.userflow.MarkErrorV2", "bk.action.qpl.userflow.MarkPointV2", "bk.action.qpl.userflow.StartFlowV2");
        this.A00 = r4;
    }

    @Override // X.AbstractC17450qp
    public /* bridge */ /* synthetic */ Object A9j(C14220l3 r13, C1093651k r14, C14240l5 r15) {
        String str = r14.A00;
        short s = -1;
        switch (str.hashCode()) {
            case -1507852311:
                s = C117305Zk.A0t("bk.action.qpl.userflow.AnnotateV2", str);
                break;
            case -1330718402:
                s = C117305Zk.A0u("bk.action.qpl.userflow.EndFlowFailureV2", str);
                break;
            case -921635786:
                s = C117305Zk.A0v("bk.action.qpl.userflow.MarkErrorV2", str);
                break;
            case -782725013:
                s = C117305Zk.A0w("bk.action.qpl.userflow.StartFlowV2", str);
                break;
            case 136195447:
                if (str.equals("bk.action.qpl.userflow.EndFlowSuccessV2")) {
                    s = 4;
                    break;
                }
                break;
            case 156743102:
                if (str.equals("bk.action.qpl.userflow.MarkPointV2")) {
                    s = 5;
                    break;
                }
                break;
            case 959076350:
                if (str.equals("bk.action.qpl.userflow.EndFlowCancelV2")) {
                    s = 6;
                    break;
                }
                break;
        }
        switch (s) {
            case 0:
                AbstractC136556Mz r4 = this.A00;
                List list = r13.A00;
                r4.A5v(r15, (Map) list.get(2), C12960it.A05(list.get(0)), C12960it.A05(C117315Zl.A0I(list)));
                return null;
            case 1:
                AbstractC136556Mz r6 = this.A00;
                List list2 = r13.A00;
                r6.A9S(r15, C12960it.A0g(list2, 2), C12960it.A0g(list2, 3), C12960it.A05(list2.get(0)), C12960it.A05(C117315Zl.A0I(list2)));
                return null;
            case 2:
                AbstractC136556Mz r62 = this.A00;
                List list3 = r13.A00;
                r62.AKq(r15, C12960it.A0g(list3, 2), C12960it.A0g(list3, 3), C12960it.A05(list3.get(0)), C12960it.A05(C117315Zl.A0I(list3)));
                return null;
            case 3:
                List list4 = r13.A00;
                AnonymousClass28D r2 = (AnonymousClass28D) list4.get(2);
                boolean equals = "cancel".equals(r2.A0I(35));
                String A0I = r2.A0I(36);
                if (A0I == null) {
                    A0I = "WaBloks";
                }
                this.A00.AeI(r15, A0I, C12960it.A05(C12980iv.A0o(list4)), C12960it.A05(C117315Zl.A0I(list4)), equals);
                return null;
            case 4:
                AbstractC136556Mz r3 = this.A00;
                List list5 = r13.A00;
                r3.A9U(r15, C12960it.A05(list5.get(0)), C12960it.A05(C117315Zl.A0I(list5)));
                return null;
            case 5:
                AbstractC136556Mz r63 = this.A00;
                List list6 = r13.A00;
                r63.AKr(r15, C12960it.A0g(list6, 2), (Map) list6.get(3), C12960it.A05(list6.get(0)), C12960it.A05(C117315Zl.A0I(list6)));
                return null;
            case 6:
                AbstractC136556Mz r42 = this.A00;
                List list7 = r13.A00;
                r42.A9R(r15, C12960it.A0g(list7, 2), C12960it.A05(list7.get(0)), C12960it.A05(C117315Zl.A0I(list7)));
                return null;
            default:
                return null;
        }
    }
}
