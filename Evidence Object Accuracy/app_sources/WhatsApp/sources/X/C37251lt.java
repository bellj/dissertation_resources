package X;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import com.facebook.redex.ViewOnClickCListenerShape0S0200000_I0;
import com.whatsapp.R;
import com.whatsapp.WaImageButton;
import com.whatsapp.calling.CallPictureGrid;
import com.whatsapp.voipcalling.CallInfo;
import com.whatsapp.voipcalling.Voip;
import com.whatsapp.voipcalling.VoipActivityV2;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: X.1lt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37251lt extends AnonymousClass02M {
    public int A00;
    public AnonymousClass1J1 A01;
    public CallInfo A02;
    public AnonymousClass1L6 A03;
    public final C15550nR A04;
    public final AnonymousClass018 A05;
    public final Integer A06 = 1;
    public final Integer A07 = 0;
    public final List A08 = new ArrayList();
    public final /* synthetic */ CallPictureGrid A09;

    public C37251lt(CallPictureGrid callPictureGrid, C15550nR r3, AnonymousClass018 r4, int i) {
        this.A09 = callPictureGrid;
        this.A04 = r3;
        this.A05 = r4;
        this.A00 = i;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A08.size();
    }

    /* renamed from: A0E */
    public void A06(C75593k6 r11, List list, int i) {
        int i2;
        int i3;
        CallInfo callInfo;
        int paddingLeft;
        int i4;
        VoipActivityV2 voipActivityV2;
        String A2l;
        int i5;
        List list2 = this.A08;
        AbstractC14640lm r4 = (AbstractC14640lm) list2.get(i);
        if (list.isEmpty() || list.contains(this.A07)) {
            if (this.A03 == null || (callInfo = this.A02) == null) {
                AnonymousClass009.A07("getPeerParticipantStatusString is not set yet");
            } else {
                AnonymousClass1S6 r6 = (AnonymousClass1S6) callInfo.participants.get(r4);
                if (r6 != null) {
                    WaImageButton waImageButton = r11.A03;
                    waImageButton.setVisibility(8);
                    CallInfo callInfo2 = this.A02;
                    int i6 = 0;
                    if (!callInfo2.isGroupCall() || ((A2l = (voipActivityV2 = (VoipActivityV2) this.A03).A2l(callInfo2, r6)) == null && (A2l = voipActivityV2.A2m(callInfo2, r6, false)) == null)) {
                        r11.A00.setVisibility(8);
                    } else {
                        r11.A00.setVisibility(0);
                        r11.A02.setText(A2l);
                        if (this.A02.isGroupCall() && (((i5 = r6.A01) == 2 || i5 == 3) && r6.A0A)) {
                            waImageButton.setVisibility(0);
                        }
                    }
                    CallInfo callInfo3 = this.A02;
                    if (!callInfo3.isGroupCall() || callInfo3.callState != Voip.CallState.ACTIVE || (!((i4 = r6.A01) == 2 || i4 == 3) || r6.A0F)) {
                        r11.A01.clearAnimation();
                    } else {
                        ImageView imageView = r11.A01;
                        if (imageView.getAnimation() == null) {
                            AlphaAnimation alphaAnimation = new AlphaAnimation(0.9f, 0.5f);
                            alphaAnimation.setDuration(1500);
                            alphaAnimation.setRepeatCount(-1);
                            alphaAnimation.setRepeatMode(2);
                            alphaAnimation.setStartOffset((long) 0);
                            imageView.startAnimation(alphaAnimation);
                        }
                    }
                    if (waImageButton.getVisibility() != 0) {
                        i6 = this.A09.getResources().getDimensionPixelSize(R.dimen.call_cancel_button_touch_padding);
                    }
                    boolean z = this.A05.A04().A06;
                    TextView textView = r11.A02;
                    if (z) {
                        paddingLeft = i6;
                    } else {
                        paddingLeft = textView.getPaddingLeft();
                    }
                    int paddingTop = textView.getPaddingTop();
                    if (z) {
                        i6 = textView.getPaddingRight();
                    }
                    textView.setPadding(paddingLeft, paddingTop, i6, textView.getPaddingBottom());
                }
            }
        }
        if (list.isEmpty() || list.contains(this.A06)) {
            this.A01.A02(r11.A01, this.A09.A04, this.A04.A0B(r4), true);
        }
        if (list.isEmpty()) {
            View view = r11.A0H;
            AnonymousClass0FA r62 = (AnonymousClass0FA) view.getLayoutParams();
            if (list2.size() == 0 || this.A00 == 0) {
                i2 = 0;
            } else {
                int size = list2.size();
                if (size >= 3) {
                    int i7 = (size + 1) >> 1;
                    if (!(size == 3 || (i3 = size % 2) == 0 || i % 2 < i3)) {
                        i7--;
                    }
                    size = i7;
                }
                i2 = (int) (((float) this.A00) / ((float) size));
            }
            ((ViewGroup.MarginLayoutParams) r62).height = i2;
            int size2 = list2.size();
            boolean z2 = true;
            if (size2 > 2 && !(size2 == 3 && i == 2)) {
                z2 = false;
            }
            r62.A01 = z2;
            view.setLayoutParams(r62);
            r11.A03.setOnClickListener(new ViewOnClickCListenerShape0S0200000_I0(this, 6, r4));
        }
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r2, int i) {
        A06((C75593k6) r2, Collections.emptyList(), i);
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        return new C75593k6(LayoutInflater.from(this.A09.getContext()).inflate(R.layout.audio_call_participant_view, viewGroup, false), this);
    }
}
