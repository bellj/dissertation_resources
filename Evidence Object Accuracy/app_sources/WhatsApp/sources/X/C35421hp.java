package X;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.whatsapp.R;
import com.whatsapp.conversation.conversationrow.album.MediaAlbumActivity;
import java.util.HashSet;
import java.util.List;

/* renamed from: X.1hp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C35421hp extends BaseAdapter {
    public List A00;
    public final AnonymousClass3FL A01;
    public final /* synthetic */ MediaAlbumActivity A02;

    @Override // android.widget.Adapter
    public long getItemId(int i) {
        return (long) i;
    }

    @Override // android.widget.BaseAdapter, android.widget.Adapter
    public int getViewTypeCount() {
        return 95;
    }

    public /* synthetic */ C35421hp(MediaAlbumActivity mediaAlbumActivity) {
        this.A02 = mediaAlbumActivity;
        this.A01 = new AnonymousClass3FL(mediaAlbumActivity);
    }

    public void A00(List list) {
        this.A00 = list;
        notifyDataSetChanged();
        MediaAlbumActivity mediaAlbumActivity = this.A02;
        if (mediaAlbumActivity.A00 != null) {
            mediaAlbumActivity.A2e().setSelectionFromTop(mediaAlbumActivity.A00.getInt("top_index"), mediaAlbumActivity.A00.getInt("top_offset"));
            mediaAlbumActivity.A00 = null;
            return;
        }
        int intExtra = mediaAlbumActivity.getIntent().getIntExtra("start_index", 0);
        if (intExtra < getCount()) {
            this.A01.A02(this, intExtra);
        }
    }

    @Override // android.widget.Adapter
    public int getCount() {
        List list = this.A00;
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    @Override // android.widget.Adapter
    public /* bridge */ /* synthetic */ Object getItem(int i) {
        List list = this.A00;
        if (list == null) {
            return null;
        }
        return list.get(i);
    }

    @Override // android.widget.BaseAdapter, android.widget.Adapter
    public int getItemViewType(int i) {
        AbstractC15340mz r0;
        AnonymousClass1OX r1 = ((AbstractActivityC13750kH) this.A02).A0F;
        List list = this.A00;
        if (list == null) {
            r0 = null;
        } else {
            r0 = (AbstractC15340mz) list.get(i);
        }
        AnonymousClass009.A05(r0);
        return r1.A00(r0);
    }

    @Override // android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        AbstractC15340mz r0;
        AnonymousClass1OY r7;
        MediaAlbumActivity mediaAlbumActivity;
        List list = this.A00;
        if (list == null) {
            r0 = null;
        } else {
            r0 = (AbstractC15340mz) list.get(i);
        }
        AnonymousClass009.A05(r0);
        boolean z = false;
        if (view == null) {
            mediaAlbumActivity = this.A02;
            r7 = ((AbstractActivityC13750kH) mediaAlbumActivity).A0F.A02(viewGroup.getContext(), mediaAlbumActivity, r0);
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) r7.getChildAt(0).getLayoutParams();
            marginLayoutParams.leftMargin = 0;
            marginLayoutParams.rightMargin = 0;
            View findViewById = r7.findViewById(R.id.media_container);
            if (findViewById != null) {
                findViewById.setPadding(0, 0, 0, 0);
            }
        } else {
            r7 = (AnonymousClass1OY) view;
            mediaAlbumActivity = this.A02;
            HashSet hashSet = mediaAlbumActivity.A0S;
            AnonymousClass1IS r2 = r0.A0z;
            if (hashSet.contains(r2) || mediaAlbumActivity.A0R.contains(r2) || ((AbstractActivityC13750kH) mediaAlbumActivity).A01 != null) {
                z = true;
            }
            r7.A1D(r0, z);
            mediaAlbumActivity.A0R.remove(r2);
        }
        HashSet hashSet2 = mediaAlbumActivity.A0S;
        AnonymousClass1IS r22 = r0.A0z;
        if (hashSet2.contains(r22)) {
            hashSet2.remove(r22);
            if (!(!((ActivityC13810kN) mediaAlbumActivity).A0E)) {
                r7.A1F(r0.A0v);
            }
        }
        AnonymousClass3FL r23 = this.A01;
        AnonymousClass1OY r1 = r7;
        if (r23.A00 == i) {
            r7.A01 = r23.A01;
        } else {
            r7.A01 = 0;
            if (r23.A04 == r7) {
                r1 = null;
            }
            return r7;
        }
        r23.A04 = r1;
        return r7;
    }
}
