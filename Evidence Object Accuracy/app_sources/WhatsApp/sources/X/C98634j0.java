package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.TokenData;
import java.util.ArrayList;

/* renamed from: X.4j0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98634j0 implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new TokenData[i];
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        String str = null;
        Long l = null;
        ArrayList arrayList = null;
        String str2 = null;
        int i = 0;
        boolean z = false;
        boolean z2 = false;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 1:
                    i = C95664e9.A02(parcel, readInt);
                    break;
                case 2:
                    str = C95664e9.A08(parcel, readInt);
                    break;
                case 3:
                    int A03 = C95664e9.A03(parcel, readInt);
                    if (A03 != 0) {
                        C95664e9.A0E(parcel, A03, 8);
                        l = Long.valueOf(parcel.readLong());
                        break;
                    } else {
                        l = null;
                        break;
                    }
                case 4:
                    z = C12960it.A1S(C95664e9.A02(parcel, readInt));
                    break;
                case 5:
                    z2 = C12960it.A1S(C95664e9.A02(parcel, readInt));
                    break;
                case 6:
                    arrayList = C95664e9.A0A(parcel, readInt);
                    break;
                case 7:
                    str2 = C95664e9.A08(parcel, readInt);
                    break;
                default:
                    C95664e9.A0D(parcel, readInt);
                    break;
            }
        }
        C95664e9.A0C(parcel, A01);
        return new TokenData(l, str, str2, arrayList, i, z, z2);
    }
}
