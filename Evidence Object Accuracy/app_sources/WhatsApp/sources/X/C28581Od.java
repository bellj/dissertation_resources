package X;

import android.text.TextUtils;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* renamed from: X.1Od  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28581Od extends AbstractC15340mz implements AbstractC16400ox, AbstractC16420oz {
    public int A00;
    public long A01;
    public C15580nU A02;
    public UserJid A03;
    public String A04;
    public String A05;
    public String A06;
    public boolean A07;

    public C28581Od(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 24, j);
    }

    public C28581Od(AnonymousClass1IS r9, C28581Od r10, long j) {
        super(r10, r9, j, true);
        this.A03 = r10.A03;
        this.A02 = r10.A02;
        this.A05 = r10.A05;
        this.A01 = r10.A01;
        this.A06 = r10.A06;
        this.A07 = r10.A07;
        this.A00 = r10.A00;
        this.A04 = r10.A04;
    }

    @Override // X.AbstractC16420oz
    public void A6k(C39971qq r11) {
        long j;
        AnonymousClass1G3 r2 = r11.A03;
        C57602nK r0 = ((C27081Fy) r2.A00).A0H;
        if (r0 == null) {
            r0 = C57602nK.A09;
        }
        AnonymousClass1G4 A0T = r0.A0T();
        if (this.A02 != null) {
            String str = this.A06;
            if (str != null) {
                A0T.A03();
                C57602nK r1 = (C57602nK) A0T.A00;
                r1.A00 |= 2;
                r1.A08 = str;
            } else {
                Log.e("FMessageGroupInvite/buildE2eMessage missing invite hash");
            }
            if (!r11.A08 || !this.A07) {
                j = this.A01;
            } else {
                j = 0;
            }
            A0T.A03();
            C57602nK r12 = (C57602nK) A0T.A00;
            r12.A00 |= 4;
            r12.A02 = j;
            String str2 = this.A05;
            if (str2 != null) {
                A0T.A03();
                C57602nK r13 = (C57602nK) A0T.A00;
                r13.A00 |= 8;
                r13.A07 = str2;
            }
            String rawString = this.A02.getRawString();
            A0T.A03();
            C57602nK r14 = (C57602nK) A0T.A00;
            r14.A00 |= 1;
            r14.A06 = rawString;
            if (!TextUtils.isEmpty(this.A04)) {
                String str3 = this.A04;
                A0T.A03();
                C57602nK r15 = (C57602nK) A0T.A00;
                r15.A00 |= 32;
                r15.A05 = str3;
            }
            C16460p3 A0G = A0G();
            if (!(A0G == null || A0G.A07() == null)) {
                byte[] A07 = A0G.A07();
                AbstractC27881Jp A01 = AbstractC27881Jp.A01(A07, 0, A07.length);
                A0T.A03();
                C57602nK r16 = (C57602nK) A0T.A00;
                r16.A00 |= 16;
                r16.A03 = A01;
            }
            AnonymousClass1PG r6 = r11.A04;
            byte[] bArr = r11.A09;
            if (C32411c7.A0U(r6, this, bArr)) {
                C43261wh A0P = C32411c7.A0P(r11.A00, r11.A02, r6, this, bArr, r11.A06);
                A0T.A03();
                C57602nK r17 = (C57602nK) A0T.A00;
                r17.A04 = A0P;
                r17.A00 |= 64;
            }
            r2.A03();
            C27081Fy r22 = (C27081Fy) r2.A00;
            r22.A0H = (C57602nK) A0T.A02();
            r22.A00 |= 4194304;
            return;
        }
        Log.w("FMessageGroupInvite/buildE2eMessage failed to build e2e message");
    }

    @Override // X.AbstractC16400ox
    public AbstractC15340mz A7M(AnonymousClass1IS r4) {
        return new C28581Od(r4, this, this.A0I);
    }
}
