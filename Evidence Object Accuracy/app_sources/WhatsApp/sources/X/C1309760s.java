package X;

import android.text.TextUtils;
import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.60s  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C1309760s {
    public long A00;
    public long A01;
    public String A02;
    public String A03;

    public C1309760s() {
    }

    public C1309760s(AnonymousClass1V8 r9) {
        this.A02 = r9.A0I("action", null);
        this.A03 = r9.A0I("status", null);
        String A0I = r9.A0I("pause-start-ts", null);
        if (A0I != null) {
            this.A01 = C28421Nd.A01(A0I, 0) * 1000;
        }
        String A0I2 = r9.A0I("pause-end-ts", null);
        if (A0I2 != null) {
            this.A00 = C28421Nd.A01(A0I2, 0) * 1000;
        }
    }

    public C1309760s(String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                JSONObject A05 = C13000ix.A05(str);
                this.A02 = A05.optString("action");
                this.A03 = A05.optString("status");
                this.A01 = A05.optLong("pauseStartTs", -1);
                this.A00 = A05.optLong("pauseEndTs", -1);
            } catch (JSONException e) {
                Log.w("PAY: IndiaUpiMandateMetadata:PauseResumeStatusDetails threw: ", e);
            }
        }
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("[ action: ");
        C1309060l.A03(A0k, this.A02);
        A0k.append(" status: ");
        C1309060l.A03(A0k, this.A03);
        A0k.append(" pauseStartDate: ");
        StringBuilder A0h = C12960it.A0h();
        A0h.append(this.A01);
        C1309060l.A03(A0k, A0h.toString());
        A0k.append(" pauseEndDate: ");
        StringBuilder A0h2 = C12960it.A0h();
        A0h2.append(this.A00);
        C1309060l.A03(A0k, C12960it.A0d("", A0h2));
        return C12960it.A0d("]", A0k);
    }
}
