package X;

import java.util.Arrays;
import java.util.Set;

/* renamed from: X.1v4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C42351v4 {
    public static final C42351v4 A02 = new C42351v4(null, 6);
    public static final C42351v4 A03 = new C42351v4(null, 4);
    public static final C42351v4 A04 = new C42351v4(null, 0);
    public static final C42351v4 A05 = new C42351v4(null, 5);
    public static final C42351v4 A06 = new C42351v4(null, 3);
    public static final C42351v4 A07 = new C42351v4(null, 2);
    public static final C42351v4 A08 = new C42351v4(null, 1);
    public final int A00;
    public final Set A01;

    public C42351v4(Set set, int i) {
        this.A00 = i;
        this.A01 = set;
    }

    public boolean A00() {
        int i = this.A00;
        return i == 2 || i == 3 || i == 1;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C42351v4 r5 = (C42351v4) obj;
            if (this.A00 != r5.A00 || !C29941Vi.A00(this.A01, r5.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.A00), this.A01});
    }
}
