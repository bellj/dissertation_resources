package X;

import java.util.List;

/* renamed from: X.2W4  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2W4 implements AbstractC33171dZ {
    public C20830wO A00;
    public final C15610nY A01;
    public final List A02;

    public AnonymousClass2W4(C15610nY r1, C20830wO r2, List list) {
        this.A00 = r2;
        this.A01 = r1;
        this.A02 = list;
    }

    @Override // X.AbstractC33171dZ
    public boolean A9v(AbstractC14640lm r5) {
        C15370n3 r3 = (C15370n3) this.A00.A04().get(r5);
        if (r3 == null || C15380n4.A0O(r3.A0D) || !this.A01.A0M(r3, this.A02, true)) {
            return false;
        }
        return true;
    }
}
