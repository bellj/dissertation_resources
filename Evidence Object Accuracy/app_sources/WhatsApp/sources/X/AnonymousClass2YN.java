package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import com.whatsapp.ClearableEditText;
import com.whatsapp.R;

/* renamed from: X.2YN  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2YN extends AnimatorListenerAdapter {
    public final /* synthetic */ C470928x A00;

    public AnonymousClass2YN(C470928x r1) {
        this.A00 = r1;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        C470928x r3 = this.A00;
        r3.A04.setVisibility(4);
        ClearableEditText clearableEditText = r3.A0A;
        clearableEditText.setVisibility(0);
        r3.A07.setImageResource(R.drawable.ic_shape_picker_search_active);
        clearableEditText.requestFocus();
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        C470928x r2 = this.A00;
        r2.A04.setClickable(false);
        r2.A06.setVisibility(4);
    }
}
