package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.0nX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15600nX {
    public final AbstractC15710nm A00;
    public final C14900mE A01;
    public final C15570nT A02;
    public final C15550nR A03;
    public final C19990v2 A04;
    public final C247616t A05;
    public final C245215v A06;
    public final C16490p7 A07;
    public final C20590w0 A08;
    public final C18680sq A09;
    public final AnonymousClass11B A0A;
    public final C14850m9 A0B;
    public final C244215l A0C;

    public C15600nX(AbstractC15710nm r1, C14900mE r2, C15570nT r3, C15550nR r4, C19990v2 r5, C247616t r6, C245215v r7, C16490p7 r8, C20590w0 r9, C18680sq r10, AnonymousClass11B r11, C14850m9 r12, C244215l r13) {
        this.A0B = r12;
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
        this.A04 = r5;
        this.A03 = r4;
        this.A07 = r8;
        this.A09 = r10;
        this.A06 = r7;
        this.A08 = r9;
        this.A0A = r11;
        this.A0C = r13;
        this.A05 = r6;
    }

    public int A00(AbstractC15590nW r7) {
        AnonymousClass1YM r0;
        C16310on A01;
        C18680sq r2 = this.A09;
        if (r2.A0B()) {
            StringBuilder sb = new StringBuilder("participant-user-store/getGroupParticipantsCount/");
            sb.append(r7);
            Log.i(sb.toString());
            r0 = (AnonymousClass1YM) r2.A06.A01.get(r7);
            if (r0 == null) {
                String valueOf = String.valueOf(r2.A07.A01(r7));
                A01 = r2.A08.get();
                try {
                    Cursor A09 = A01.A03.A09("SELECT COUNT(1) as count FROM group_participant_user WHERE group_jid_row_id = ?", new String[]{valueOf});
                    if (A09.moveToFirst()) {
                        int i = A09.getInt(A09.getColumnIndexOrThrow("count"));
                        A09.close();
                        A01.close();
                        return i;
                    }
                    A09.close();
                    A01.close();
                    return 0;
                } finally {
                }
            }
        } else {
            C20590w0 r22 = this.A08;
            StringBuilder sb2 = new StringBuilder("participant-user-store/getGroupParticipantsCount/");
            sb2.append(r7);
            Log.i(sb2.toString());
            r0 = (AnonymousClass1YM) r22.A07.A01.get(r7);
            if (r0 == null) {
                A01 = r22.A08.get();
                try {
                    Cursor A092 = A01.A03.A09("SELECT COUNT(*) as count FROM group_participants WHERE gjid = ?", new String[]{r7.getRawString()});
                    if (A092.moveToFirst()) {
                        int i2 = A092.getInt(A092.getColumnIndexOrThrow("count"));
                        A092.close();
                        A01.close();
                        return i2;
                    }
                    A092.close();
                    A01.close();
                    return 0;
                } finally {
                }
            }
        }
        return r0.A02.size();
    }

    public AnonymousClass1YO A01(AbstractC15590nW r2, UserJid userJid) {
        return (AnonymousClass1YO) A02(r2).A02.get(userJid);
    }

    public AnonymousClass1YM A02(AbstractC15590nW r4) {
        C245215v r1;
        AnonymousClass1YL r0;
        C18680sq r2 = this.A09;
        if (r2.A0B()) {
            r1 = r2.A06;
            r0 = r2.A05;
        } else {
            C20590w0 r02 = this.A08;
            r1 = r02.A07;
            r0 = r02.A06;
        }
        return r1.A00(r0, r4);
    }

    public String A03(AbstractC15590nW r3) {
        Set A00;
        if (this.A06.A01.containsKey(r3)) {
            return A02(r3).A09();
        }
        C18680sq r1 = this.A09;
        if (r1.A0B()) {
            A00 = r1.A02(r3);
        } else {
            A00 = this.A08.A00(r3);
        }
        return AnonymousClass1YM.A00(A00);
    }

    public Set A04(AbstractC14640lm r2) {
        if (r2 instanceof AbstractC15590nW) {
            return A02((AbstractC15590nW) r2).A0B();
        }
        return new HashSet();
    }

    public Set A05(UserJid userJid) {
        String rawString;
        C18680sq r1 = this.A09;
        if (r1.A0B()) {
            return r1.A03(userJid);
        }
        C20590w0 r12 = this.A08;
        HashSet hashSet = new HashSet();
        if (r12.A01.A0F(userJid)) {
            rawString = "";
        } else {
            rawString = userJid.getRawString();
        }
        C16310on A01 = r12.A08.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT gjid FROM group_participants WHERE jid = ?", new String[]{rawString});
            while (A09.moveToNext()) {
                AbstractC15590nW A05 = AbstractC15590nW.A05(A09.getString(0));
                if (A05 != null) {
                    hashSet.add(A05);
                }
            }
            A09.close();
            A01.close();
            return hashSet;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public Set A06(UserJid userJid, Set set) {
        C18680sq r4 = this.A09;
        if (!r4.A0B()) {
            return this.A08.A02(userJid);
        }
        HashSet hashSet = new HashSet();
        if (set.isEmpty()) {
            return hashSet;
        }
        C16310on A01 = r4.A08.get();
        try {
            Iterator it = new C29841Uw((DeviceJid[]) set.toArray(new DeviceJid[0]), 975).iterator();
            while (it.hasNext()) {
                DeviceJid[] deviceJidArr = (DeviceJid[]) it.next();
                C16330op r9 = A01.A03;
                int length = deviceJidArr.length;
                StringBuilder sb = new StringBuilder("SELECT DISTINCT(group_jid_row_id) FROM group_participant_user AS user JOIN group_participant_device AS device ON  user._id =  device.group_participant_row_id WHERE ");
                sb.append("device_jid_row_id IN ");
                sb.append(AnonymousClass1Ux.A00(length));
                sb.append(" AND ");
                sb.append("sent_sender_key = 1");
                String obj = sb.toString();
                String[] strArr = new String[length];
                for (int i = 0; i < length; i++) {
                    strArr[i] = String.valueOf(r4.A07.A01(deviceJidArr[i]));
                }
                Cursor A09 = r9.A09(obj, strArr);
                int columnIndexOrThrow = A09.getColumnIndexOrThrow("group_jid_row_id");
                HashSet hashSet2 = new HashSet();
                while (A09.moveToNext()) {
                    hashSet2.add(Long.valueOf(A09.getLong(columnIndexOrThrow)));
                }
                for (AbstractC15590nW r0 : r4.A07.A09(AbstractC15590nW.class, hashSet2).values()) {
                    if (r0 != null) {
                        hashSet.add(r0);
                    }
                }
                A09.close();
            }
            A01.close();
            return hashSet;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A07(AnonymousClass1YM r5) {
        C16310on A02 = this.A07.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            C18680sq r1 = this.A09;
            if (r1.A0C()) {
                r1.A05(r5);
            }
            if (!r1.A0B()) {
                this.A08.A04(r5);
            }
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A08(AbstractC15590nW r14, Long l, List list) {
        String rawString;
        C16310on A02 = this.A07.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            C18680sq r5 = this.A09;
            if (r5.A0C()) {
                StringBuilder sb = new StringBuilder("participant-user-store/updateGroupParticipants/");
                sb.append(r14);
                sb.append(" ");
                sb.append(list);
                Log.i(sb.toString());
                C16310on A022 = r5.A08.A02();
                AnonymousClass1Lx A002 = A022.A00();
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    r5.A04((AnonymousClass1YO) it.next(), r14);
                }
                A002.A00();
                A002.close();
                A022.close();
            }
            if (!r5.A0B()) {
                C20590w0 r6 = this.A08;
                StringBuilder sb2 = new StringBuilder("msgstore/updategroupparticipants/");
                sb2.append(r14);
                sb2.append(" ");
                sb2.append(list);
                Log.i(sb2.toString());
                ContentValues contentValues = new ContentValues(4);
                C16310on A023 = r6.A08.A02();
                AnonymousClass1Lx A003 = A023.A00();
                Iterator it2 = list.iterator();
                while (it2.hasNext()) {
                    AnonymousClass1YO r2 = (AnonymousClass1YO) it2.next();
                    C15570nT r0 = r6.A01;
                    UserJid userJid = r2.A03;
                    if (r0.A0F(userJid)) {
                        rawString = "";
                    } else {
                        rawString = userJid.getRawString();
                    }
                    contentValues.put("gjid", r14.getRawString());
                    contentValues.put("jid", rawString);
                    contentValues.put("admin", Integer.valueOf(r2.A01));
                    int i = 0;
                    if (r2.A02) {
                        i = 1;
                    }
                    contentValues.put("pending", Integer.valueOf(i));
                    String[] strArr = {r14.getRawString(), rawString};
                    C16330op r22 = A023.A03;
                    if (r22.A00("group_participants", contentValues, "gjid = ? AND jid = ?", strArr) == 0) {
                        r22.A02(contentValues, "group_participants");
                    }
                }
                A003.A00();
                A003.close();
                A023.close();
            }
            if (l != null && (r14 instanceof C15580nU)) {
                this.A05.A01((C15580nU) r14, l.longValue());
            }
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A09(UserJid userJid) {
        AnonymousClass1YO r0;
        C16310on A02 = this.A07.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            C18680sq r3 = this.A09;
            if (r3.A0C()) {
                StringBuilder sb = new StringBuilder("participant-user-store/resetSentSenderKeyFor/");
                sb.append(userJid);
                Log.i(sb.toString());
                C16310on A022 = r3.A08.A02();
                try {
                    AnonymousClass1Lx A002 = A022.A00();
                    AnonymousClass170 r2 = r3.A09;
                    UserJid userJid2 = userJid;
                    StringBuilder sb2 = new StringBuilder("participant-device-store/resetSentSenderKey/");
                    sb2.append(userJid);
                    Log.i(sb2.toString());
                    AnonymousClass009.A0B("participant-user-store/invalid-jid", !TextUtils.isEmpty(userJid.getRawString()));
                    C18460sU r1 = r2.A02;
                    C15570nT r02 = r2.A01;
                    r02.A08();
                    C27631Ih r03 = r02.A05;
                    AnonymousClass009.A05(r03);
                    if (userJid.equals(r03)) {
                        userJid2 = C29831Uv.A00;
                    }
                    long A01 = r1.A01(userJid2);
                    A02 = r2.A03.A02();
                    try {
                        AnonymousClass1YE A0A = A02.A03.A0A("UPDATE group_participant_device SET sent_sender_key = ? WHERE group_participant_row_id IN (SELECT _id FROM group_participant_user WHERE user_jid_row_id = ?)");
                        A0A.A00.bindAllArgsAsStrings(new String[]{"0", String.valueOf(A01)});
                        A0A.A00();
                        A02.close();
                        ConcurrentHashMap concurrentHashMap = r3.A06.A01;
                        Iterator it = new HashSet(concurrentHashMap.keySet()).iterator();
                        while (it.hasNext()) {
                            AnonymousClass1YM r04 = (AnonymousClass1YM) concurrentHashMap.get((AbstractC15590nW) it.next());
                            if (!(r04 == null || (r0 = (AnonymousClass1YO) r04.A02.get(userJid)) == null)) {
                                C18680sq.A00(r0);
                            }
                        }
                        A002.A00();
                        A002.close();
                        A022.close();
                    } finally {
                    }
                } finally {
                }
            }
            if (!r3.A0B()) {
                C20590w0 r6 = this.A08;
                StringBuilder sb3 = new StringBuilder("msgstore/markParticipantAsHavingNoSenderKeys; participantJid=");
                sb3.append(userJid);
                Log.i(sb3.toString());
                C16310on A023 = r6.A08.A02();
                try {
                    ContentValues contentValues = new ContentValues(1);
                    contentValues.put("sent_sender_key", Boolean.FALSE);
                    if (A023.A03.A00("group_participants", contentValues, "jid = ?", new String[]{userJid.getRawString()}) > 0) {
                        ConcurrentHashMap concurrentHashMap2 = r6.A07.A01;
                        Iterator it2 = new HashSet(concurrentHashMap2.keySet()).iterator();
                        while (it2.hasNext()) {
                            AnonymousClass1YM r12 = (AnonymousClass1YM) concurrentHashMap2.get((AbstractC15590nW) it2.next());
                            if (!(r12 == null || ((AnonymousClass1YO) r12.A02.get(userJid)) == null)) {
                                r6.A03((AnonymousClass1YO) r12.A02.get(userJid), r12, false);
                            }
                        }
                    }
                    A023.close();
                } finally {
                    try {
                        A023.close();
                    } catch (Throwable unused) {
                    }
                }
            }
            A00.A00();
            A00.close();
            A02.close();
        } finally {
            try {
                A02.close();
            } catch (Throwable unused2) {
            }
        }
    }

    public void A0A(UserJid userJid, List list) {
        String rawString;
        C16310on A02 = this.A07.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            C18680sq r4 = this.A09;
            if (r4.A0C()) {
                StringBuilder sb = new StringBuilder("participant-user-store/removeParticipantFromGroups/");
                sb.append(list);
                sb.append(" ");
                sb.append(userJid);
                Log.i(sb.toString());
                A02 = r4.A08.A02();
                try {
                    AnonymousClass1Lx A002 = A02.A00();
                    Iterator it = list.iterator();
                    while (it.hasNext()) {
                        r4.A0E((AbstractC15590nW) it.next(), userJid);
                    }
                    A002.A00();
                    A002.close();
                    A02.close();
                } finally {
                }
            }
            if (!r4.A0B()) {
                C20590w0 r5 = this.A08;
                StringBuilder sb2 = new StringBuilder("msgstore/removeparticipantfromgroups/");
                sb2.append(list);
                sb2.append(" ");
                sb2.append(userJid);
                Log.i(sb2.toString());
                C16310on A022 = r5.A08.A02();
                try {
                    AnonymousClass1Lx A003 = A022.A00();
                    Iterator it2 = list.iterator();
                    while (it2.hasNext()) {
                        String[] strArr = new String[2];
                        strArr[0] = ((AbstractC15590nW) it2.next()).getRawString();
                        if (r5.A01.A0F(userJid)) {
                            rawString = "";
                        } else {
                            rawString = userJid.getRawString();
                        }
                        strArr[1] = rawString;
                        A022.A03.A01("group_participants", "gjid = ? AND jid = ?", strArr);
                    }
                    A003.A00();
                    A003.close();
                    A022.close();
                } finally {
                }
            }
            A00.A00();
            A00.close();
            A02.close();
        } finally {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
        }
    }

    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        	at java.util.BitSet.or(BitSet.java:940)
        	at jadx.core.utils.BlockUtils.lambda$getPathCross$3(BlockUtils.java:689)
        	at java.util.ArrayList.forEach(ArrayList.java:1259)
        	at jadx.core.utils.BlockUtils.getPathCross(BlockUtils.java:689)
        	at jadx.core.utils.BlockUtils.getPathCross(BlockUtils.java:728)
        	at jadx.core.dex.visitors.regions.IfMakerHelper.restructureIf(IfMakerHelper.java:88)
        	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:707)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:156)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:95)
        	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:732)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:156)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:95)
        	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:732)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:156)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:95)
        	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:52)
        */
    public void A0B(X.AnonymousClass1XB r29) {
        /*
        // Method dump skipped, instructions count: 2094
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15600nX.A0B(X.1XB):void");
    }

    public boolean A0C(GroupJid groupJid) {
        return A02(groupJid).A0H(this.A02);
    }

    public boolean A0D(GroupJid groupJid) {
        AnonymousClass1YO r0;
        AnonymousClass1YM A02 = A02(groupJid);
        C15570nT r02 = this.A02;
        r02.A08();
        C27631Ih r2 = r02.A05;
        if (r2 == null || (r0 = (AnonymousClass1YO) A02.A02.get(r2)) == null || r0.A01 == 0) {
            return false;
        }
        return true;
    }

    public boolean A0E(AbstractC15590nW r4) {
        for (AnonymousClass1YO r0 : A02(r4).A02.values()) {
            C15370n3 A0A = this.A03.A0A(r0.A03);
            if (A0A != null && A0A.A0J()) {
                return true;
            }
        }
        return false;
    }

    public boolean A0F(C15580nU r4) {
        C15370n3 A0A;
        Iterator it = A02(r4).A0A().iterator();
        while (it.hasNext()) {
            C15570nT r0 = this.A02;
            UserJid userJid = ((AnonymousClass1YO) it.next()).A03;
            if (!r0.A0F(userJid) && (A0A = this.A03.A0A(userJid)) != null && A0A.A0C != null) {
                return true;
            }
        }
        return false;
    }

    public boolean A0G(C15580nU r5) {
        AnonymousClass1YO r0;
        AnonymousClass1YM A02 = A02(r5);
        C15570nT r02 = this.A02;
        r02.A08();
        C27631Ih r1 = r02.A05;
        if (r1 == null || (r0 = (AnonymousClass1YO) A02.A02.get(r1)) == null || r0.A01 != 2) {
            return false;
        }
        return true;
    }
}
