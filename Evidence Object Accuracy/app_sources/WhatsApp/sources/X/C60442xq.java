package X;

import android.content.Context;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.2xq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60442xq extends AnonymousClass1OY {
    public boolean A00;
    public final TextView A01;

    public C60442xq(Context context, AbstractC13890kV r5, C30381Xe r6) {
        super(context, r5, r6);
        A0Z();
        setClickable(false);
        TextView A0J = C12960it.A0J(this, R.id.info);
        this.A01 = A0J;
        AnonymousClass1OY.A0D(context, this, A0J);
        A0J.setOnLongClickListener(this.A1a);
        setLongClickable(true);
        setWillNotDraw(false);
        A1M();
    }

    @Override // X.AnonymousClass1OZ, X.AbstractC28561Ob
    public void A0Z() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2P6 A07 = AnonymousClass1OY.A07(this);
            AnonymousClass01J A08 = AnonymousClass1OY.A08(A07, this);
            AnonymousClass1OY.A0L(A08, this);
            AnonymousClass1OY.A0M(A08, this);
            AnonymousClass1OY.A0K(A08, this);
            AnonymousClass1OY.A0I(A07, A08, this, AnonymousClass1OY.A09(A08, this, AnonymousClass1OY.A0B(A08, this)));
        }
    }

    @Override // X.AnonymousClass1OY
    public void A0s() {
        A1M();
        A1H(false);
    }

    @Override // X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r2, boolean z) {
        boolean A1X = C12960it.A1X(r2, ((AbstractC28551Oa) this).A0O);
        super.A1D(r2, z);
        if (z || A1X) {
            A1M();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0086  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A1M() {
        /*
            r10 = this;
            X.0mz r7 = r10.A0O
            X.1Xe r7 = (X.C30381Xe) r7
            int r1 = r7.A17()
            r9 = 0
            r3 = 1
            if (r1 == 0) goto L_0x0026
            if (r1 == r3) goto L_0x0082
            r0 = 2
            if (r1 == r0) goto L_0x007e
            r0 = 3
            if (r1 == r0) goto L_0x007a
            java.lang.String r0 = "unknown call type "
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            int r0 = r7.A17()
            java.lang.String r0 = X.C12960it.A0f(r1, r0)
            X.AnonymousClass009.A0A(r0, r9)
        L_0x0026:
            r8 = 2131892857(0x7f121a79, float:1.9420474E38)
        L_0x0029:
            X.0m7 r2 = r10.A0k
            long r0 = r7.A0I
            long r1 = r2.A02(r0)
            android.widget.TextView r4 = r10.A01
            X.018 r6 = r10.A0K
            android.content.Context r5 = r10.getContext()
            java.lang.Object[] r3 = new java.lang.Object[r3]
            X.018 r0 = r10.A0K
            java.lang.String r0 = X.AnonymousClass3JK.A00(r0, r1)
            java.lang.String r0 = X.C12960it.A0X(r5, r0, r3, r9, r8)
            java.lang.String r0 = X.AnonymousClass3JK.A01(r6, r0, r1)
            r4.setText(r0)
            r0 = 36
            X.C12960it.A13(r4, r10, r7, r0)
            X.018 r3 = r10.A0K
            android.content.Context r2 = r10.getContext()
            boolean r0 = r7.A18()
            r1 = 2131231989(0x7f0804f5, float:1.8080075E38)
            if (r0 == 0) goto L_0x0063
            r1 = 2131232448(0x7f0806c0, float:1.8081006E38)
        L_0x0063:
            r0 = 2131100452(0x7f060324, float:1.7813286E38)
            android.graphics.drawable.Drawable r2 = X.AnonymousClass2GE.A01(r2, r1, r0)
            boolean r0 = X.C28141Kv.A00(r3)
            r1 = 0
            if (r0 == 0) goto L_0x0086
            X.2GF r0 = new X.2GF
            r0.<init>(r2, r3)
            r4.setCompoundDrawablesWithIntrinsicBounds(r1, r1, r0, r1)
            return
        L_0x007a:
            r8 = 2131892742(0x7f121a06, float:1.942024E38)
            goto L_0x0029
        L_0x007e:
            r8 = 2131892859(0x7f121a7b, float:1.9420478E38)
            goto L_0x0029
        L_0x0082:
            r8 = 2131892739(0x7f121a03, float:1.9420235E38)
            goto L_0x0029
        L_0x0086:
            r4.setCompoundDrawablesWithIntrinsicBounds(r2, r1, r1, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C60442xq.A1M():void");
    }

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_divider;
    }

    @Override // X.AbstractC28551Oa
    public C30381Xe getFMessage() {
        return (C30381Xe) ((AbstractC28551Oa) this).A0O;
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_divider;
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_divider;
    }

    @Override // X.AbstractC28551Oa
    public void setFMessage(AbstractC15340mz r2) {
        AnonymousClass009.A0F(r2 instanceof C30381Xe);
        ((AbstractC28551Oa) this).A0O = r2;
    }
}
