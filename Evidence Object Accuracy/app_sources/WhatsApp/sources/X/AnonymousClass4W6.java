package X;

/* renamed from: X.4W6  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4W6 {
    public final int A00;
    public final int A01;
    public final long A02;
    public final AnonymousClass4U4 A03;
    public final int[] A04;
    public final int[] A05;
    public final long[] A06;
    public final long[] A07;

    public AnonymousClass4W6(AnonymousClass4U4 r7, int[] iArr, int[] iArr2, long[] jArr, long[] jArr2, int i, long j) {
        int length = iArr.length;
        int length2 = jArr2.length;
        boolean z = false;
        C95314dV.A03(C12960it.A1V(length, length2));
        int length3 = jArr.length;
        C95314dV.A03(C12960it.A1V(length3, length2));
        int length4 = iArr2.length;
        C95314dV.A03(length4 == length2 ? true : z);
        this.A03 = r7;
        this.A06 = jArr;
        this.A05 = iArr;
        this.A00 = i;
        this.A07 = jArr2;
        this.A04 = iArr2;
        this.A02 = j;
        this.A01 = length3;
        if (length4 > 0) {
            int i2 = length4 - 1;
            iArr2[i2] = iArr2[i2] | 536870912;
        }
    }

    public int A00(long j) {
        long[] jArr = this.A07;
        for (int A05 = AnonymousClass3JZ.A05(jArr, j, true); A05 < jArr.length; A05++) {
            if ((this.A04[A05] & 1) != 0) {
                return A05;
            }
        }
        return -1;
    }
}
