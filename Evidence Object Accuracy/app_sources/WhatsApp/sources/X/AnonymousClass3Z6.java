package X;

import com.whatsapp.util.Log;

/* renamed from: X.3Z6  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3Z6 implements AbstractC21730xt {
    public final /* synthetic */ AnonymousClass2D8 A00;
    public final /* synthetic */ C244315m A01;

    public AnonymousClass3Z6(AnonymousClass2D8 r1, C244315m r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        Log.e(C12960it.A0d(str, C12960it.A0k("EncryptedBackupProtocolHelper/sendDeleteAccountIq/onDeliveryFailure id=")));
        this.A00.APr("delivery failure", 3);
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r2, String str) {
        C244315m.A00(r2, this.A00, str);
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r4, String str) {
        AnonymousClass2D8 r2 = this.A00;
        Log.i(C12960it.A0d(str, C12960it.A0k("EncryptedBackupProtocolHelper/deleteAccountOnSuccess id=")));
        if (r4.A0E("success") == null) {
            Log.e(C12960it.A0d(str, C12960it.A0k("EncryptedBackupProtocolHelper/deleteAccountOnSuccess success was empty id=")));
            r2.APr("success was empty", 1);
            return;
        }
        r2.AWu();
    }
}
