package X;

/* renamed from: X.2g7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54062g7 extends AnonymousClass0Q0 {
    public final /* synthetic */ C71203cY A00;
    public final /* synthetic */ C71203cY A01;
    public final /* synthetic */ C91574Sg A02;

    public C54062g7(C71203cY r1, C71203cY r2, C91574Sg r3) {
        this.A02 = r3;
        this.A01 = r1;
        this.A00 = r2;
    }

    @Override // X.AnonymousClass0Q0
    public int A00() {
        return this.A00.A00.size();
    }

    @Override // X.AnonymousClass0Q0
    public int A01() {
        return this.A01.A00.size();
    }

    @Override // X.AnonymousClass0Q0
    public Object A02(int i, int i2) {
        Object obj = this.A01.A00.get(i);
        Object obj2 = this.A00.A00.get(i2);
        if (obj != null && obj2 != null) {
            return null;
        }
        throw new AssertionError();
    }

    @Override // X.AnonymousClass0Q0
    public boolean A03(int i, int i2) {
        Object obj = this.A01.A00.get(i);
        Object obj2 = this.A00.A00.get(i2);
        if (obj != null) {
            if (obj2 != null) {
                return this.A02.A02.A00.A00(obj, obj2);
            }
        } else if (obj2 == null) {
            return true;
        }
        throw new AssertionError();
    }

    @Override // X.AnonymousClass0Q0
    public boolean A04(int i, int i2) {
        Object obj = this.A01.A00.get(i);
        Object obj2 = this.A00.A00.get(i2);
        if (obj == null) {
            return obj2 == null;
        }
        if (obj2 != null) {
            return this.A02.A02.A00.A01(obj, obj2);
        }
        return false;
    }
}
