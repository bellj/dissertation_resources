package X;

/* renamed from: X.1qd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C39841qd extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Boolean A02;
    public Boolean A03;
    public Boolean A04;
    public Boolean A05;
    public Boolean A06;
    public Boolean A07;
    public Double A08;
    public Double A09;
    public Double A0A;
    public Integer A0B;
    public Integer A0C;
    public Integer A0D;
    public Integer A0E;
    public Integer A0F;
    public Integer A0G;
    public Integer A0H;
    public Integer A0I;
    public Integer A0J;
    public Long A0K;
    public Long A0L;
    public Long A0M;
    public Long A0N;
    public Long A0O;
    public Long A0P;
    public Long A0Q;
    public Long A0R;
    public Long A0S;
    public Long A0T;
    public Long A0U;
    public Long A0V;
    public Long A0W;
    public Long A0X;
    public Long A0Y;
    public Long A0Z;
    public Long A0a;
    public Long A0b;
    public Long A0c;
    public Long A0d;
    public Long A0e;
    public String A0f;
    public String A0g;
    public String A0h;
    public String A0i;
    public String A0j;

    public C39841qd() {
        super(1588, new AnonymousClass00E(1, 10, 10), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(43, this.A0B);
        r3.Abe(34, this.A0f);
        r3.Abe(32, this.A0g);
        r3.Abe(33, this.A0h);
        r3.Abe(45, this.A08);
        r3.Abe(28, this.A0K);
        r3.Abe(31, this.A0L);
        r3.Abe(30, this.A00);
        r3.Abe(29, this.A0M);
        r3.Abe(49, this.A01);
        r3.Abe(46, this.A0N);
        r3.Abe(42, this.A0C);
        r3.Abe(4, this.A0O);
        r3.Abe(10, this.A0P);
        r3.Abe(41, this.A0i);
        r3.Abe(37, this.A0Q);
        r3.Abe(38, this.A0R);
        r3.Abe(5, this.A0j);
        r3.Abe(36, this.A02);
        r3.Abe(16, this.A03);
        r3.Abe(13, this.A04);
        r3.Abe(40, this.A0D);
        r3.Abe(7, this.A09);
        r3.Abe(1, this.A0E);
        r3.Abe(6, this.A0S);
        r3.Abe(12, this.A0F);
        r3.Abe(9, this.A0T);
        r3.Abe(3, this.A0U);
        r3.Abe(8, this.A0V);
        r3.Abe(15, this.A0W);
        r3.Abe(39, this.A0G);
        r3.Abe(44, this.A0H);
        r3.Abe(35, this.A0I);
        r3.Abe(14, this.A0X);
        r3.Abe(17, this.A0Y);
        r3.Abe(20, this.A0Z);
        r3.Abe(19, this.A05);
        r3.Abe(18, this.A0a);
        r3.Abe(27, this.A0A);
        r3.Abe(22, this.A0b);
        r3.Abe(25, this.A0c);
        r3.Abe(24, this.A06);
        r3.Abe(26, this.A07);
        r3.Abe(23, this.A0d);
        r3.Abe(21, this.A0e);
        r3.Abe(48, this.A0J);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        String obj3;
        String obj4;
        String obj5;
        String obj6;
        String obj7;
        String obj8;
        String obj9;
        StringBuilder sb = new StringBuilder("WamMediaUpload2 {");
        Integer num = this.A0B;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "connectionType", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "debugMediaException", this.A0f);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "debugMediaIp", this.A0g);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "debugUrl", this.A0h);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "estimatedBandwidth", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "finalizeConnectT", this.A0K);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "finalizeHttpCode", this.A0L);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "finalizeIsReuse", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "finalizeNetworkT", this.A0M);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "isViewOnce", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mediaId", this.A0N);
        Integer num2 = this.A0C;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "networkStack", obj2);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "overallAttemptCount", this.A0O);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "overallConnBlockFetchT", this.A0P);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "overallConnectionClass", this.A0i);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "overallCumT", this.A0Q);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "overallCumUserVisibleT", this.A0R);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "overallDomain", this.A0j);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "overallIsFinal", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "overallIsForward", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "overallIsManual", this.A04);
        Integer num3 = this.A0D;
        if (num3 == null) {
            obj3 = null;
        } else {
            obj3 = num3.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "overallMediaKeyReuse", obj3);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "overallMediaSize", this.A09);
        Integer num4 = this.A0E;
        if (num4 == null) {
            obj4 = null;
        } else {
            obj4 = num4.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "overallMediaType", obj4);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "overallMmsVersion", this.A0S);
        Integer num5 = this.A0F;
        if (num5 == null) {
            obj5 = null;
        } else {
            obj5 = num5.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "overallOptimisticFlag", obj5);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "overallQueueT", this.A0T);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "overallRetryCount", this.A0U);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "overallT", this.A0V);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "overallTranscodeT", this.A0W);
        Integer num6 = this.A0G;
        if (num6 == null) {
            obj6 = null;
        } else {
            obj6 = num6.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "overallUploadMode", obj6);
        Integer num7 = this.A0H;
        if (num7 == null) {
            obj7 = null;
        } else {
            obj7 = num7.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "overallUploadOrigin", obj7);
        Integer num8 = this.A0I;
        if (num8 == null) {
            obj8 = null;
        } else {
            obj8 = num8.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "overallUploadResult", obj8);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "overallUserVisibleT", this.A0X);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "resumeConnectT", this.A0Y);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "resumeHttpCode", this.A0Z);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "resumeIsReuse", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "resumeNetworkT", this.A0a);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "uploadBytesTransferred", this.A0A);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "uploadConnectT", this.A0b);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "uploadHttpCode", this.A0c);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "uploadIsReuse", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "uploadIsStreaming", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "uploadNetworkT", this.A0d);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "uploadResumePoint", this.A0e);
        Integer num9 = this.A0J;
        if (num9 == null) {
            obj9 = null;
        } else {
            obj9 = num9.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "uploadSource", obj9);
        sb.append("}");
        return sb.toString();
    }
}
