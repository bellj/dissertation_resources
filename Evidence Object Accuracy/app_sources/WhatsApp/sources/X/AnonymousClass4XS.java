package X;

import android.media.AudioAttributes;
import android.media.AudioFormat;
import android.media.AudioTrack;

/* renamed from: X.4XS  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4XS {
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final int A04;
    public final int A05;
    public final int A06;
    public final C100614mC A07;
    public final AnonymousClass5Xx[] A08;

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003a, code lost:
        if (r19 == 5) goto L_0x003c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass4XS(X.C100614mC r12, X.AnonymousClass5Xx[] r13, int r14, int r15, int r16, int r17, int r18, int r19) {
        /*
            r11 = this;
            r11.<init>()
            r11.A07 = r12
            r11.A01 = r14
            r11.A04 = r15
            r0 = r16
            r11.A05 = r0
            r2 = r17
            r11.A06 = r2
            r0 = r18
            r11.A02 = r0
            r4 = r19
            r11.A03 = r4
            r11.A08 = r13
            if (r15 == 0) goto L_0x0065
            r0 = 1
            if (r15 == r0) goto L_0x002e
            r0 = 2
            if (r15 != r0) goto L_0x0060
            r2 = 250000(0x3d090, double:1.235164E-318)
        L_0x0026:
            switch(r19) {
                case 5: goto L_0x0032;
                case 6: goto L_0x0036;
                case 7: goto L_0x003f;
                case 8: goto L_0x0043;
                case 9: goto L_0x0047;
                case 10: goto L_0x004b;
                case 11: goto L_0x004f;
                case 12: goto L_0x0052;
                case 13: goto L_0x0029;
                case 14: goto L_0x0055;
                case 15: goto L_0x0059;
                case 16: goto L_0x005c;
                case 17: goto L_0x0090;
                case 18: goto L_0x0036;
                default: goto L_0x0029;
            }
        L_0x0029:
            java.lang.IllegalArgumentException r0 = X.C72453ed.A0h()
            throw r0
        L_0x002e:
            r2 = 50000000(0x2faf080, double:2.47032823E-316)
            goto L_0x0026
        L_0x0032:
            r1 = 80000(0x13880, float:1.12104E-40)
            goto L_0x003c
        L_0x0036:
            r1 = 768000(0xbb800, float:1.076197E-39)
            r0 = 5
            if (r4 != r0) goto L_0x0093
        L_0x003c:
            int r1 = r1 << 1
            goto L_0x0093
        L_0x003f:
            r1 = 192000(0x2ee00, float:2.6905E-40)
            goto L_0x0093
        L_0x0043:
            r1 = 2250000(0x225510, float:3.152922E-39)
            goto L_0x0093
        L_0x0047:
            r1 = 40000(0x9c40, float:5.6052E-41)
            goto L_0x0093
        L_0x004b:
            r1 = 100000(0x186a0, float:1.4013E-40)
            goto L_0x0093
        L_0x004f:
            r1 = 16000(0x3e80, float:2.2421E-41)
            goto L_0x0093
        L_0x0052:
            r1 = 7000(0x1b58, float:9.809E-42)
            goto L_0x0093
        L_0x0055:
            r1 = 3062500(0x2ebae4, float:4.291477E-39)
            goto L_0x0093
        L_0x0059:
            r1 = 8000(0x1f40, float:1.121E-41)
            goto L_0x0093
        L_0x005c:
            r1 = 256000(0x3e800, float:3.58732E-40)
            goto L_0x0093
        L_0x0060:
            java.lang.IllegalStateException r0 = X.C72463ee.A0D()
            throw r0
        L_0x0065:
            int r10 = android.media.AudioTrack.getMinBufferSize(r2, r0, r4)
            r0 = -2
            boolean r0 = X.C12980iv.A1V(r10, r0)
            X.C95314dV.A04(r0)
            int r9 = r10 << 2
            r0 = 250000(0x3d090, double:1.235164E-318)
            long r7 = (long) r2
            long r0 = r0 * r7
            r5 = 1000000(0xf4240, double:4.940656E-318)
            long r0 = r0 / r5
            int r4 = (int) r0
            int r3 = r11.A05
            int r4 = r4 * r3
            r1 = 750000(0xb71b0, double:3.70549E-318)
            long r1 = r1 * r7
            long r1 = r1 / r5
            int r0 = (int) r1
            int r0 = r0 * r3
            int r0 = java.lang.Math.max(r10, r0)
            int r2 = X.C72463ee.A03(r9, r0, r4)
            goto L_0x0099
        L_0x0090:
            r1 = 336000(0x52080, float:4.70836E-40)
        L_0x0093:
            long r0 = (long) r1
            long r0 = X.C72453ed.A0W(r2, r0)
            int r2 = (int) r0
        L_0x0099:
            r11.A00 = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass4XS.<init>(X.4mC, X.5Xx[], int, int, int, int, int, int):void");
    }

    public final AudioTrack A00(AnonymousClass3HR r9, int i, boolean z) {
        AudioAttributes A00;
        if (z) {
            A00 = new AudioAttributes.Builder().setContentType(3).setFlags(16).setUsage(1).build();
        } else {
            A00 = r9.A00();
        }
        return new AudioTrack(A00, C106534vr.A03(this.A06, this.A02, this.A03), this.A00, 1, i);
    }

    public final AudioTrack A01(AnonymousClass3HR r4, int i, boolean z) {
        AudioAttributes A00;
        AudioFormat A03 = C106534vr.A03(this.A06, this.A02, this.A03);
        if (z) {
            A00 = new AudioAttributes.Builder().setContentType(3).setFlags(16).setUsage(1).build();
        } else {
            A00 = r4.A00();
        }
        AudioTrack.Builder audioFormat = new AudioTrack.Builder().setAudioAttributes(A00).setAudioFormat(A03);
        boolean z2 = true;
        AudioTrack.Builder sessionId = audioFormat.setTransferMode(1).setBufferSizeInBytes(this.A00).setSessionId(i);
        if (this.A04 != 1) {
            z2 = false;
        }
        return sessionId.setOffloadedPlayback(z2).build();
    }
}
