package X;

import java.util.concurrent.Callable;

/* renamed from: X.6KS  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6KS implements Callable {
    public final /* synthetic */ C128425w5 A00;
    public final /* synthetic */ AnonymousClass661 A01;

    public AnonymousClass6KS(C128425w5 r1, AnonymousClass661 r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        C128965wx r0 = this.A01.A0L;
        r0.A01.A02(this.A00);
        return null;
    }
}
