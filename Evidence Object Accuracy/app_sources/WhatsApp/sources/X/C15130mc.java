package X;

import android.content.Context;
import android.os.PowerManager;
import android.os.SystemClock;
import android.os.WorkSource;
import android.text.TextUtils;
import android.util.Log;
import com.facebook.redex.RunnableBRunnable0Shape0S0100000_I0;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.0mc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15130mc {
    public static final long A0G = TimeUnit.DAYS.toMillis(366);
    public static final Object A0H = new Object();
    public static volatile ScheduledExecutorService A0I;
    public int A00 = 0;
    public int A01;
    public long A02;
    public WorkSource A03;
    public AbstractC115095Qe A04 = C108454z5.A00;
    public AnonymousClass5BC A05;
    public Future A06;
    public AtomicInteger A07 = new AtomicInteger(0);
    public boolean A08 = true;
    public final Context A09;
    public final PowerManager.WakeLock A0A;
    public final Object A0B = new Object();
    public final String A0C;
    public final Map A0D = new HashMap();
    public final Set A0E = new HashSet();
    public final ScheduledExecutorService A0F;

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00c1, code lost:
        if (r10.length() != 0) goto L_0x00f5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00f3, code lost:
        if (r10.length() == 0) goto L_0x00fd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00f5, code lost:
        r0 = r1.concat(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00f9, code lost:
        android.util.Log.e("WorkSourceUtil", r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00fd, code lost:
        r0 = new java.lang.String(r1);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C15130mc(android.content.Context r12, java.lang.String r13) {
        /*
        // Method dump skipped, instructions count: 332
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15130mc.<init>(android.content.Context, java.lang.String):void");
    }

    public void A00() {
        if (this.A07.decrementAndGet() < 0) {
            Log.e("WakeLock", String.valueOf(this.A0C).concat(" release without a matched acquire!"));
        }
        synchronized (this.A0B) {
            if (this.A08) {
                TextUtils.isEmpty(null);
            }
            Map map = this.A0D;
            if (map.containsKey(null)) {
                C93174Zk r1 = (C93174Zk) map.get(null);
                if (r1 != null) {
                    int i = r1.A00 - 1;
                    r1.A00 = i;
                    if (i == 0) {
                        map.remove(null);
                    }
                }
            } else {
                Log.w("WakeLock", String.valueOf(this.A0C).concat(" counter does not exist"));
            }
            A01();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00ac, code lost:
        if (r6.A05 != null) goto L_0x00ae;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A01() {
        /*
            r6 = this;
            java.lang.Object r3 = r6.A0B
            monitor-enter(r3)
            boolean r0 = r6.A03()     // Catch: all -> 0x00b2
            if (r0 == 0) goto L_0x00b0
            boolean r0 = r6.A08     // Catch: all -> 0x00b2
            r4 = 0
            if (r0 == 0) goto L_0x0017
            int r0 = r6.A00     // Catch: all -> 0x00b2
            int r0 = r0 + -1
            r6.A00 = r0     // Catch: all -> 0x00b2
            if (r0 > 0) goto L_0x00b0
            goto L_0x0019
        L_0x0017:
            r6.A00 = r4     // Catch: all -> 0x00b2
        L_0x0019:
            java.util.Set r2 = r6.A0E     // Catch: all -> 0x00b2
            boolean r0 = r2.isEmpty()     // Catch: all -> 0x00b2
            if (r0 != 0) goto L_0x0034
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch: all -> 0x00b2
            r1.<init>(r2)     // Catch: all -> 0x00b2
            r2.clear()     // Catch: all -> 0x00b2
            int r0 = r1.size()     // Catch: all -> 0x00b2
            if (r0 <= 0) goto L_0x0034
            r1.get(r4)     // Catch: all -> 0x00b2
            r0 = 0
            throw r0     // Catch: all -> 0x00b2
        L_0x0034:
            java.util.Map r2 = r6.A0D     // Catch: all -> 0x00b2
            java.util.Collection r0 = r2.values()     // Catch: all -> 0x00b2
            java.util.Iterator r1 = r0.iterator()     // Catch: all -> 0x00b2
        L_0x003e:
            boolean r0 = r1.hasNext()     // Catch: all -> 0x00b2
            if (r0 == 0) goto L_0x004d
            java.lang.Object r0 = r1.next()     // Catch: all -> 0x00b2
            X.4Zk r0 = (X.C93174Zk) r0     // Catch: all -> 0x00b2
            r0.A00 = r4     // Catch: all -> 0x00b2
            goto L_0x003e
        L_0x004d:
            r2.clear()     // Catch: all -> 0x00b2
            java.util.concurrent.Future r0 = r6.A06     // Catch: all -> 0x00b2
            r5 = 0
            if (r0 == 0) goto L_0x005e
            r0.cancel(r4)     // Catch: all -> 0x00b2
            r6.A06 = r5     // Catch: all -> 0x00b2
            r0 = 0
            r6.A02 = r0     // Catch: all -> 0x00b2
        L_0x005e:
            r6.A01 = r4     // Catch: all -> 0x00b2
            android.os.PowerManager$WakeLock r1 = r6.A0A     // Catch: all -> 0x00b2
            boolean r0 = r1.isHeld()     // Catch: all -> 0x00b2
            if (r0 == 0) goto L_0x0098
            r1.release()     // Catch: RuntimeException -> 0x006c, all -> 0x0090
            goto L_0x00aa
        L_0x006c:
            r4 = move-exception
            java.lang.Class r1 = r4.getClass()     // Catch: all -> 0x0090
            java.lang.Class<java.lang.RuntimeException> r0 = java.lang.RuntimeException.class
            boolean r0 = r1.equals(r0)     // Catch: all -> 0x0090
            if (r0 == 0) goto L_0x008f
            java.lang.String r2 = "WakeLock"
            java.lang.String r0 = r6.A0C     // Catch: all -> 0x0090
            java.lang.String r1 = java.lang.String.valueOf(r0)     // Catch: all -> 0x0090
            java.lang.String r0 = " failed to release!"
            java.lang.String r0 = r1.concat(r0)     // Catch: all -> 0x0090
            android.util.Log.e(r2, r0, r4)     // Catch: all -> 0x0090
            X.5BC r0 = r6.A05     // Catch: all -> 0x00b2
            if (r0 == 0) goto L_0x00b0
            goto L_0x00ae
        L_0x008f:
            throw r4     // Catch: all -> 0x0090
        L_0x0090:
            r1 = move-exception
            X.5BC r0 = r6.A05     // Catch: all -> 0x00b2
            if (r0 == 0) goto L_0x0097
            r6.A05 = r5     // Catch: all -> 0x00b2
        L_0x0097:
            throw r1     // Catch: all -> 0x00b2
        L_0x0098:
            java.lang.String r2 = "WakeLock"
            java.lang.String r0 = r6.A0C     // Catch: all -> 0x00b2
            java.lang.String r1 = java.lang.String.valueOf(r0)     // Catch: all -> 0x00b2
            java.lang.String r0 = " should be held!"
            java.lang.String r0 = r1.concat(r0)     // Catch: all -> 0x00b2
            android.util.Log.e(r2, r0)     // Catch: all -> 0x00b2
            goto L_0x00b0
        L_0x00aa:
            X.5BC r0 = r6.A05     // Catch: all -> 0x00b2
            if (r0 == 0) goto L_0x00b0
        L_0x00ae:
            r6.A05 = r5     // Catch: all -> 0x00b2
        L_0x00b0:
            monitor-exit(r3)     // Catch: all -> 0x00b2
            return
        L_0x00b2:
            r0 = move-exception
            monitor-exit(r3)     // Catch: all -> 0x00b2
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15130mc.A01():void");
    }

    public void A02(long j) {
        this.A07.incrementAndGet();
        long j2 = Long.MAX_VALUE;
        long max = Math.max(Math.min(Long.MAX_VALUE, A0G), 1L);
        if (j > 0) {
            max = Math.min(j, max);
        }
        synchronized (this.A0B) {
            if (!A03()) {
                this.A05 = AnonymousClass5BC.A00;
                this.A0A.acquire();
                SystemClock.elapsedRealtime();
            }
            this.A00++;
            this.A01++;
            if (this.A08) {
                TextUtils.isEmpty(null);
            }
            Map map = this.A0D;
            C93174Zk r6 = (C93174Zk) map.get(null);
            if (r6 == null) {
                r6 = new C93174Zk(null);
                map.put(null, r6);
            }
            r6.A00++;
            long elapsedRealtime = SystemClock.elapsedRealtime();
            if (Long.MAX_VALUE - elapsedRealtime > max) {
                j2 = elapsedRealtime + max;
            }
            if (j2 > this.A02) {
                this.A02 = j2;
                Future future = this.A06;
                if (future != null) {
                    future.cancel(false);
                }
                this.A06 = this.A0F.schedule(new RunnableBRunnable0Shape0S0100000_I0(this, 8), max, TimeUnit.MILLISECONDS);
            }
        }
    }

    public boolean A03() {
        boolean z;
        synchronized (this.A0B) {
            z = false;
            if (this.A00 > 0) {
                z = true;
            }
        }
        return z;
    }
}
