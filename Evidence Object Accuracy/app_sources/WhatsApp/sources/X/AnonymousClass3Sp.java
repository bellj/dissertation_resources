package X;

import java.io.IOException;

/* renamed from: X.3Sp  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3Sp implements AbstractC28711Os, AnonymousClass5Q3 {
    public AnonymousClass4P0 A00;
    public AnonymousClass1Or A01;
    public final /* synthetic */ AbstractC56052kF A02;

    public AnonymousClass3Sp(AbstractC56052kF r5) {
        this.A02 = r5;
        this.A01 = new AnonymousClass1Or(null, r5.A03.A02, 0);
        this.A00 = new AnonymousClass4P0(null, ((AbstractC67703Sn) r5).A02.A02, 0);
    }

    public final C28731Ou A00(C28731Ou r11) {
        long j = r11.A04;
        long j2 = r11.A03;
        if (j == j && j2 == j2) {
            return r11;
        }
        int i = r11.A00;
        int i2 = r11.A02;
        return new C28731Ou(r11.A05, r11.A06, i, i2, r11.A01, j, j2);
    }

    public final boolean A01(C28741Ov r4, int i) {
        if (r4 != null) {
            AbstractC56052kF r2 = this.A02;
            if (r2 instanceof C56032kD) {
                Object obj = r4.A04;
                Object obj2 = ((C56032kD) r2).A01.A00;
                if (obj2 != null && obj2.equals(obj)) {
                    obj = C56072kH.A02;
                }
                r4 = r4.A01(obj);
            } else if (r2 instanceof C56022kC) {
                C56022kC r22 = (C56022kC) r2;
                if (r22.A00 != Integer.MAX_VALUE) {
                    r4 = (C28741Ov) r22.A02.get(r4);
                }
            }
            if (r4 == null) {
                return false;
            }
        } else {
            r4 = null;
        }
        AbstractC56052kF r23 = this.A02;
        AnonymousClass1Or r1 = this.A01;
        if (r1.A00 != i || !AnonymousClass3JZ.A0H(r1.A01, r4)) {
            this.A01 = new AnonymousClass1Or(r4, r23.A03.A02, i);
        }
        AnonymousClass4P0 r12 = this.A00;
        if (r12.A00 == i && AnonymousClass3JZ.A0H(r12.A01, r4)) {
            return true;
        }
        this.A00 = new AnonymousClass4P0(r4, ((AbstractC67703Sn) r23).A02.A02, i);
        return true;
    }

    @Override // X.AbstractC28711Os
    public void APT(C28731Ou r3, C28741Ov r4, int i) {
        if (A01(r4, i)) {
            this.A01.A05(A00(r3));
        }
    }

    @Override // X.AbstractC28711Os
    public void ARu(C28721Ot r3, C28731Ou r4, C28741Ov r5, int i) {
        if (A01(r5, i)) {
            this.A01.A01(r3, A00(r4));
        }
    }

    @Override // X.AbstractC28711Os
    public void ARv(C28721Ot r3, C28731Ou r4, C28741Ov r5, int i) {
        if (A01(r5, i)) {
            this.A01.A02(r3, A00(r4));
        }
    }

    @Override // X.AbstractC28711Os
    public void ARy(C28721Ot r3, C28731Ou r4, C28741Ov r5, IOException iOException, int i, boolean z) {
        if (A01(r5, i)) {
            this.A01.A04(r3, A00(r4), iOException, z);
        }
    }

    @Override // X.AbstractC28711Os
    public void AS4(C28721Ot r3, C28731Ou r4, C28741Ov r5, int i) {
        if (A01(r5, i)) {
            this.A01.A03(r3, A00(r4));
        }
    }
}
