package X;

import android.os.SystemClock;

/* renamed from: X.4yW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C108114yW implements AbstractC116615Wd {
    public long A00;
    public long A01;
    public C94344be A02 = C94344be.A03;
    public boolean A03;
    public final AnonymousClass5Xd A04;

    public C108114yW(AnonymousClass5Xd r2) {
        this.A04 = r2;
    }

    public void A00(long j) {
        this.A01 = j;
        if (this.A03) {
            this.A00 = SystemClock.elapsedRealtime();
        }
    }

    @Override // X.AbstractC116615Wd
    public C94344be AFk() {
        return this.A02;
    }

    @Override // X.AbstractC116615Wd
    public long AFr() {
        long j;
        long j2 = this.A01;
        if (!this.A03) {
            return j2;
        }
        long elapsedRealtime = SystemClock.elapsedRealtime() - this.A00;
        C94344be r4 = this.A02;
        if (r4.A01 == 1.0f) {
            j = C95214dK.A01(elapsedRealtime);
        } else {
            j = elapsedRealtime * ((long) r4.A02);
        }
        return j2 + j;
    }

    @Override // X.AbstractC116615Wd
    public void AcX(C94344be r3) {
        if (this.A03) {
            A00(AFr());
        }
        this.A02 = r3;
    }
}
