package X;

import android.content.Context;
import android.util.AttributeSet;

/* renamed from: X.2D4  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2D4 extends AnonymousClass03X implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;

    public AnonymousClass2D4(Context context) {
        super(context, null);
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
    }

    public AnonymousClass2D4(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
    }

    public AnonymousClass2D4(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }
}
