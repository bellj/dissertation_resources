package X;

import android.net.Uri;
import android.os.Bundle;

/* renamed from: X.0iD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public interface AbstractC12620iD {
    AnonymousClass0SR A6j();

    void Ac8(int i);

    void AcH(Uri uri);

    void setExtras(Bundle bundle);
}
