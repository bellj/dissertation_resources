package X;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/* renamed from: X.16X  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass16X {
    public final C15570nT A00;
    public final C245916c A01;
    public final C14840m8 A02;

    public AnonymousClass16X(C15570nT r1, C245916c r2, C14840m8 r3) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
    }

    public Collection A00() {
        if (this.A02.A03()) {
            return new ArrayList(this.A01.A00().A00.values());
        }
        this.A00.A08();
        return Collections.emptyList();
    }
}
