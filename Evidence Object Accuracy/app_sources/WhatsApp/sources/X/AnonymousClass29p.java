package X;

import android.app.Dialog;
import android.os.Handler;

/* renamed from: X.29p  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass29p {
    public void A00() {
        C56322kh r3 = (C56322kh) this;
        DialogInterface$OnCancelListenerC56312kg r2 = (DialogInterface$OnCancelListenerC56312kg) r3.A01.A00;
        r2.A04.set(null);
        Handler handler = r2.A03.A06;
        handler.sendMessage(handler.obtainMessage(3));
        Dialog dialog = r3.A00;
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }
}
