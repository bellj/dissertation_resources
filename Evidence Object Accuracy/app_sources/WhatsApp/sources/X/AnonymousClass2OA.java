package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.2OA  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2OA extends AnonymousClass1JW {
    public int A00;
    public int A01;
    public byte[] A02;
    public final UserJid A03;

    public AnonymousClass2OA(AbstractC15710nm r1, C15450nH r2, UserJid userJid) {
        super(r1, r2);
        this.A03 = userJid;
    }
}
