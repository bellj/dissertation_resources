package X;

import java.io.File;
import java.util.HashMap;

/* renamed from: X.1BI  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1BI extends AnonymousClass1BJ {
    public final HashMap A00 = new HashMap();

    public AnonymousClass1BI(AbstractC15710nm r2, C18790t3 r3, C18640sm r4, C14830m7 r5, C16590pI r6, C17170qN r7, C14820m6 r8, AnonymousClass1BH r9, C18810t5 r10, C18800t4 r11, AbstractC14440lR r12) {
        super(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12);
    }

    public synchronized HashMap A0E() {
        HashMap hashMap;
        C39451pv A02;
        String[] list;
        hashMap = this.A00;
        if (hashMap.isEmpty() && (A02 = A02()) != null) {
            String A022 = A02.A02();
            File filesDir = this.A08.A00.getFilesDir();
            StringBuilder sb = new StringBuilder();
            sb.append("downloadable/filter_");
            sb.append(A022);
            File file = new File(filesDir, sb.toString());
            if (!file.exists() || (list = file.list()) == null) {
                A06();
            } else {
                String absolutePath = file.getAbsolutePath();
                for (String str : list) {
                    hashMap.put(str, new File(absolutePath, str));
                }
            }
        }
        return hashMap;
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0065 A[Catch: all -> 0x0095, TryCatch #2 {all -> 0x0099, blocks: (B:15:0x0018, B:16:0x0039, B:20:0x004b, B:23:0x0053, B:27:0x0065, B:32:0x0078, B:36:0x0085, B:34:0x007d, B:31:0x0072, B:19:0x0045, B:29:0x006a, B:5:0x0003), top: B:40:0x0003 }] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0085 A[Catch: all -> 0x0095, TRY_LEAVE, TryCatch #2 {all -> 0x0099, blocks: (B:15:0x0018, B:16:0x0039, B:20:0x004b, B:23:0x0053, B:27:0x0065, B:32:0x0078, B:36:0x0085, B:34:0x007d, B:31:0x0072, B:19:0x0045, B:29:0x006a, B:5:0x0003), top: B:40:0x0003 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void A0F(X.AbstractC39501q0 r6, int r7) {
        /*
            r5 = this;
            r4 = r5
            monitor-enter(r4)
            r0 = -1
            int r3 = r5.A01(r0)     // Catch: all -> 0x0097
            r1 = 5
            r2 = 1
            if (r3 == 0) goto L_0x004b
            if (r3 == r2) goto L_0x0039
            r0 = 2
            if (r3 == r0) goto L_0x004b
            r0 = 3
            if (r3 == r0) goto L_0x0039
            r0 = 4
            if (r3 == r0) goto L_0x004b
            if (r3 == r1) goto L_0x0083
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch: all -> 0x0095
            r2.<init>()     // Catch: all -> 0x0095
            java.lang.String r0 = "FilterManager/getFilesAsync/Unexpected state "
            r2.append(r0)     // Catch: all -> 0x0095
            java.util.HashMap r1 = X.AnonymousClass1BJ.A0G     // Catch: all -> 0x0095
            java.lang.Integer r0 = java.lang.Integer.valueOf(r3)     // Catch: all -> 0x0095
            java.lang.Object r0 = r1.get(r0)     // Catch: all -> 0x0095
            java.lang.String r0 = (java.lang.String) r0     // Catch: all -> 0x0095
            r2.append(r0)     // Catch: all -> 0x0095
            java.lang.String r0 = r2.toString()     // Catch: all -> 0x0095
            com.whatsapp.util.Log.e(r0)     // Catch: all -> 0x0095
            goto L_0x0063
        L_0x0039:
            java.util.HashMap r1 = X.AnonymousClass1BJ.A0G     // Catch: all -> 0x0095
            java.lang.Integer r0 = java.lang.Integer.valueOf(r3)     // Catch: all -> 0x0095
            r1.get(r0)     // Catch: all -> 0x0095
            if (r6 == 0) goto L_0x0093
            r0 = -1
            r5.A0A(r6, r0)     // Catch: all -> 0x0049
            goto L_0x0093
        L_0x0049:
            r0 = move-exception
            throw r0     // Catch: all -> 0x0095
        L_0x004b:
            boolean r0 = r5.A0G()     // Catch: all -> 0x0095
            if (r0 != 0) goto L_0x007c
            if (r7 != 0) goto L_0x0069
            X.0m6 r1 = r5.A0A     // Catch: all -> 0x0095
            X.0sm r0 = r5.A06     // Catch: all -> 0x0095
            int r0 = r0.A05(r2)     // Catch: all -> 0x0095
            int r0 = X.C38441o6.A00(r1, r0)     // Catch: all -> 0x0095
            r0 = r0 & r2
            if (r0 == 0) goto L_0x0063
            goto L_0x0069
        L_0x0063:
            if (r6 == 0) goto L_0x0093
            r6.APk()     // Catch: all -> 0x0095
            goto L_0x0093
        L_0x0069:
            r0 = -1
            r5.A09(r2, r0)     // Catch: all -> 0x006e
            goto L_0x0070
        L_0x006e:
            r0 = move-exception
            throw r0     // Catch: all -> 0x0095
        L_0x0070:
            if (r6 == 0) goto L_0x0078
            r5.A0A(r6, r0)     // Catch: all -> 0x0076
            goto L_0x0078
        L_0x0076:
            r0 = move-exception
            throw r0     // Catch: all -> 0x0095
        L_0x0078:
            r5.A08(r7, r0)     // Catch: all -> 0x0095
            goto L_0x0093
        L_0x007c:
            r0 = -1
            r5.A09(r1, r0)     // Catch: all -> 0x0081
            goto L_0x0083
        L_0x0081:
            r0 = move-exception
            throw r0     // Catch: all -> 0x0095
        L_0x0083:
            if (r6 == 0) goto L_0x0093
            boolean r0 = r5.A0G()     // Catch: all -> 0x0095
            X.AnonymousClass009.A0F(r0)     // Catch: all -> 0x0095
            java.util.HashMap r0 = r5.A0E()     // Catch: all -> 0x0095
            r6.AUc(r0)     // Catch: all -> 0x0095
        L_0x0093:
            monitor-exit(r4)
            return
        L_0x0095:
            r0 = move-exception
            throw r0     // Catch: all -> 0x0099
        L_0x0097:
            r0 = move-exception
            throw r0     // Catch: all -> 0x0099
        L_0x0099:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1BI.A0F(X.1q0, int):void");
    }

    public synchronized boolean A0G() {
        return !A0E().isEmpty();
    }
}
