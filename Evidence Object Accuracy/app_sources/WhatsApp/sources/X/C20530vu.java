package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* renamed from: X.0vu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20530vu {
    public final C18460sU A00;
    public final C16490p7 A01;

    public C20530vu(C18460sU r1, C16490p7 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public final void A00(ContentValues contentValues, AnonymousClass1XJ r4, long j) {
        contentValues.put("message_row_id", Long.valueOf(j));
        UserJid userJid = r4.A00;
        if (userJid != null) {
            contentValues.put("business_owner_jid", Long.valueOf(this.A00.A01(userJid)));
        }
        C30021Vq.A04(contentValues, "title", r4.A02);
        C30021Vq.A04(contentValues, "description", r4.A01);
    }

    public void A01(AnonymousClass1XJ r7, long j) {
        boolean z = true;
        boolean z2 = false;
        if (r7.A08() == 2) {
            z2 = true;
        }
        StringBuilder sb = new StringBuilder("CatalogMessageStore/insertOrUpdateQuotedCatalogMessage/message in main storage; key=");
        sb.append(r7.A0z);
        AnonymousClass009.A0B(sb.toString(), z2);
        try {
            C16310on A02 = this.A01.A02();
            ContentValues contentValues = new ContentValues();
            A00(contentValues, r7, j);
            if (A02.A03.A06(contentValues, "message_quoted_product", 5) != j) {
                z = false;
            }
            AnonymousClass009.A0C("CatalogMessageStore/insertOrUpdateQuotedCatalogMessage/inserted row should have same row_id", z);
            A02.close();
        } catch (SQLiteConstraintException e) {
            StringBuilder sb2 = new StringBuilder("CatalogMessageStore/insertOrUpdateQuotedCatalogMessage/fail to insert. Error message is: ");
            sb2.append(e);
            Log.e(sb2.toString());
        }
    }

    public final void A02(AnonymousClass1XJ r8, String str) {
        boolean z = false;
        if (r8.A11 > 0) {
            z = true;
        }
        StringBuilder sb = new StringBuilder("CatalogMessageStore/fillCatalogDataIfAvailable/message must have row_id set; key=");
        sb.append(r8.A0z);
        AnonymousClass009.A0B(sb.toString(), z);
        String[] strArr = {String.valueOf(r8.A11)};
        C16310on A01 = this.A01.get();
        try {
            Cursor A09 = A01.A03.A09(str, strArr);
            if (A09 != null) {
                if (A09.moveToLast()) {
                    r8.A00 = (UserJid) this.A00.A07(UserJid.class, A09.getLong(A09.getColumnIndexOrThrow("business_owner_jid")));
                    r8.A02 = A09.getString(A09.getColumnIndexOrThrow("title"));
                    r8.A01 = A09.getString(A09.getColumnIndexOrThrow("description"));
                }
                A09.close();
            }
            A01.close();
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
