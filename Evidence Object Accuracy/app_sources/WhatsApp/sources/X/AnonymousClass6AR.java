package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.6AR  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6AR implements AnonymousClass6MQ {
    public final /* synthetic */ UserJid A00;
    public final /* synthetic */ C117905ax A01;

    public AnonymousClass6AR(UserJid userJid, C117905ax r2) {
        this.A01 = r2;
        this.A00 = userJid;
    }

    @Override // X.AnonymousClass6MQ
    public void AOb(C119705ey r3) {
        this.A01.A00.A0B(C117315Zl.A05(r3, this.A00));
    }

    @Override // X.AnonymousClass6MQ
    public void APo(C452120p r3) {
        this.A01.A01.A0B(C117315Zl.A05(r3, this.A00));
    }
}
