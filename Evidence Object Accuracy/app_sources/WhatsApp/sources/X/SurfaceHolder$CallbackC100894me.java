package X;

import android.view.SurfaceHolder;
import com.whatsapp.videoplayback.VideoSurfaceView;

/* renamed from: X.4me  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class SurfaceHolder$CallbackC100894me implements SurfaceHolder.Callback {
    public final /* synthetic */ VideoSurfaceView A00;

    public SurfaceHolder$CallbackC100894me(VideoSurfaceView videoSurfaceView) {
        this.A00 = videoSurfaceView;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001f, code lost:
        if (r2.A07 != r7) goto L_0x0021;
     */
    @Override // android.view.SurfaceHolder.Callback
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void surfaceChanged(android.view.SurfaceHolder r4, int r5, int r6, int r7) {
        /*
            r3 = this;
            java.lang.String r0 = "videoview/surfaceChanged: "
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            r1.append(r6)
            java.lang.String r0 = "x"
            java.lang.String r0 = X.C12960it.A0e(r0, r1, r7)
            com.whatsapp.util.Log.i(r0)
            com.whatsapp.videoplayback.VideoSurfaceView r2 = r3.A00
            r2.A05 = r6
            r2.A04 = r7
            int r0 = r2.A08
            if (r0 != r6) goto L_0x0021
            int r0 = r2.A07
            r1 = 1
            if (r0 == r7) goto L_0x0022
        L_0x0021:
            r1 = 0
        L_0x0022:
            android.media.MediaPlayer r0 = r2.A0C
            if (r0 == 0) goto L_0x0037
            if (r1 == 0) goto L_0x0037
            int r0 = r2.A03
            if (r0 < 0) goto L_0x002f
            r2.seekTo(r0)
        L_0x002f:
            int r1 = r2.A06
            r0 = 3
            if (r1 != r0) goto L_0x0037
            r2.start()
        L_0x0037:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.SurfaceHolder$CallbackC100894me.surfaceChanged(android.view.SurfaceHolder, int, int, int):void");
    }

    @Override // android.view.SurfaceHolder.Callback
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        VideoSurfaceView videoSurfaceView = this.A00;
        videoSurfaceView.A0E = surfaceHolder;
        videoSurfaceView.A02();
    }

    @Override // android.view.SurfaceHolder.Callback
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        VideoSurfaceView videoSurfaceView = this.A00;
        videoSurfaceView.A03 = videoSurfaceView.getCurrentPosition();
        videoSurfaceView.A0E = null;
        videoSurfaceView.A04(true);
    }
}
