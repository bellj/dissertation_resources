package X;

import android.os.IBinder;
import android.os.IInterface;

/* renamed from: X.4iU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98314iU implements IInterface {
    public final IBinder A00;

    public C98314iU(IBinder iBinder) {
        this.A00 = iBinder;
    }

    @Override // android.os.IInterface
    public final IBinder asBinder() {
        return this.A00;
    }
}
