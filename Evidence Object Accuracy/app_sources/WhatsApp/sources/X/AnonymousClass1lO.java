package X;

import android.app.Activity;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.jid.Jid;
import com.whatsapp.util.ViewOnClickCListenerShape2S0300000_I0;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1lO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1lO extends BaseAdapter {
    public int A00 = 4;
    public int A01 = 3;
    public List A02;
    public boolean A03;
    public boolean A04;
    public final Activity A05;
    public final LayoutInflater A06;
    public final AnonymousClass2TT A07;
    public final C15610nY A08;
    public final AnonymousClass1J1 A09;
    public final C14850m9 A0A;
    public final AnonymousClass12F A0B;
    public final AbstractC14440lR A0C;

    @Override // android.widget.Adapter
    public long getItemId(int i) {
        return (long) i;
    }

    @Override // android.widget.BaseAdapter, android.widget.Adapter
    public int getViewTypeCount() {
        return 1;
    }

    public AnonymousClass1lO(Activity activity, AnonymousClass2TT r3, C15610nY r4, AnonymousClass1J1 r5, C14850m9 r6, AnonymousClass12F r7, AbstractC14440lR r8) {
        this.A0A = r6;
        this.A05 = activity;
        this.A0C = r8;
        this.A08 = r4;
        this.A07 = r3;
        this.A0B = r7;
        this.A09 = r5;
        this.A06 = LayoutInflater.from(activity);
        this.A02 = new ArrayList();
    }

    @Override // android.widget.Adapter
    public int getCount() {
        int size;
        if (!this.A04) {
            List list = this.A02;
            if (list == null) {
                size = 0;
            } else {
                size = list.size();
            }
            int i = this.A00;
            if (size > i) {
                return i;
            }
        }
        List list2 = this.A02;
        if (list2 == null) {
            return 0;
        }
        return list2.size();
    }

    @Override // android.widget.Adapter
    public /* bridge */ /* synthetic */ Object getItem(int i) {
        return this.A02.get(i);
    }

    @Override // android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        AnonymousClass4RY r0;
        int size;
        int i2;
        int size2;
        if (view == null) {
            view = this.A06.inflate(R.layout.participant_list_row, viewGroup, false);
            r0 = new AnonymousClass4RY();
            r0.A03 = new C28801Pb(view, this.A08, this.A0B, (int) R.id.name);
            r0.A02 = (TextEmojiLabel) view.findViewById(R.id.aboutInfo);
            r0.A01 = (ImageView) view.findViewById(R.id.avatar);
            r0.A00 = view.findViewById(R.id.divider);
            view.setTag(r0);
        } else {
            r0 = (AnonymousClass4RY) view.getTag();
        }
        int count = getCount() - 1;
        View view2 = r0.A00;
        if (i == count) {
            view2.setVisibility(8);
        } else {
            view2.setVisibility(0);
        }
        if (!this.A04) {
            List list = this.A02;
            if (list == null) {
                size = 0;
            } else {
                size = list.size();
            }
            if (size > this.A00 && i == (i2 = this.A01)) {
                C28801Pb r10 = r0.A03;
                Activity activity = this.A05;
                Resources resources = activity.getResources();
                List list2 = this.A02;
                if (list2 == null) {
                    size2 = 0;
                } else {
                    size2 = list2.size();
                }
                int i3 = size2 - i2;
                r10.A08(resources.getQuantityString(R.plurals.n_more, i3, Integer.valueOf(i3)));
                r0.A03.A04(AnonymousClass00T.A00(activity, R.color.list_item_sub_title));
                r0.A02.setVisibility(8);
                r0.A01.setImageResource(R.drawable.ic_more_participants);
                r0.A01.setClickable(false);
                return view;
            }
        }
        C15370n3 r6 = (C15370n3) this.A02.get(i);
        AnonymousClass009.A05(r6);
        r0.A03.A04(AnonymousClass00T.A00(this.A05, R.color.list_item_title));
        r0.A03.A06(r6);
        ImageView imageView = r0.A01;
        StringBuilder sb = new StringBuilder();
        sb.append(this.A07.A00(R.string.transition_avatar));
        Jid jid = r6.A0D;
        AnonymousClass009.A05(jid);
        sb.append(jid.getRawString());
        AnonymousClass028.A0k(imageView, sb.toString());
        r0.A02.setVisibility(0);
        r0.A02.setTag(r6.A0D);
        C15610nY r8 = this.A08;
        String str = (String) r8.A09.get(r6.A0B(AbstractC15590nW.class));
        if (str != null) {
            r0.A02.setText(str);
        } else {
            r0.A02.setText("");
            this.A0C.Aaz(new AnonymousClass37C(r0.A02, r8, (C15580nU) r6.A0B(C15580nU.class)), new Void[0]);
        }
        this.A09.A06(r0.A01, r6);
        r0.A01.setClickable(true);
        r0.A01.setOnClickListener(new ViewOnClickCListenerShape2S0300000_I0(this, r6, r0, 1));
        return view;
    }
}
