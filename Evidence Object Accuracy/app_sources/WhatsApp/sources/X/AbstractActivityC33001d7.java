package X;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.transition.Fade;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.fragment.app.DialogFragment;
import com.whatsapp.ListItemWithLeftIcon;
import com.whatsapp.R;
import com.whatsapp.WaTextView;
import com.whatsapp.chatinfo.ContactInfoActivity;
import com.whatsapp.chatinfo.ListChatInfo;
import com.whatsapp.chatinfo.view.custom.ChatInfoLayoutV2;
import com.whatsapp.conversation.ChatMediaEphemeralVisibilityDialog;
import com.whatsapp.conversation.ChatMediaVisibilityDialog;
import com.whatsapp.group.GroupChatInfo;
import com.whatsapp.ui.media.MediaCard;
import java.util.HashSet;

/* renamed from: X.1d7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC33001d7 extends AbstractActivityC33011d8 {
    public long A00;
    public C16170oZ A01;
    public C19850um A02;
    public AnonymousClass19Q A03;
    public C243915i A04;
    public AnonymousClass38W A05;
    public C15550nR A06;
    public C15890o4 A07;
    public AnonymousClass018 A08;
    public C19990v2 A09;
    public C15650ng A0A;
    public AnonymousClass1BB A0B;
    public C15600nX A0C;
    public C20000v3 A0D;
    public C20050v8 A0E;
    public C15660nh A0F;
    public AnonymousClass150 A0G;
    public C20710wC A0H;
    public C17900ra A0I;
    public C22710zW A0J;
    public C17070qD A0K;
    public AnonymousClass14X A0L;
    public AbstractC16130oV A0M;
    public C15860o1 A0N;
    public C255719x A0O;
    public AnonymousClass19O A0P;
    public boolean A0Q;
    public final HashSet A0R = new HashSet();

    public AbstractC14640lm A2g() {
        if (this instanceof GroupChatInfo) {
            return ((GroupChatInfo) this).A2u();
        }
        if (!(this instanceof ListChatInfo)) {
            return ((ContactInfoActivity) this).A2v();
        }
        return ((ListChatInfo) this).A2u();
    }

    public void A2h() {
        if (this instanceof GroupChatInfo) {
            GroupChatInfo groupChatInfo = (GroupChatInfo) this;
            groupChatInfo.A2l();
            AnonymousClass2BM r0 = groupChatInfo.A0z;
            if (r0 != null) {
                r0.A03(true);
                groupChatInfo.A0z = null;
            }
            AbstractC16350or r02 = groupChatInfo.A1V;
            if (r02 != null) {
                r02.A03(true);
                groupChatInfo.A0z = null;
            }
        } else if (this instanceof ListChatInfo) {
            ListChatInfo listChatInfo = (ListChatInfo) this;
            listChatInfo.A2l();
            C60342wg r1 = listChatInfo.A07;
            if (r1 != null) {
                r1.A03(true);
                listChatInfo.A07 = null;
            }
        } else if (!(this instanceof ContactInfoActivity)) {
            A2l();
        } else {
            ContactInfoActivity contactInfoActivity = (ContactInfoActivity) this;
            contactInfoActivity.A2l();
            AnonymousClass2Dx r12 = contactInfoActivity.A0Y;
            if (r12 != null) {
                r12.A03(true);
                contactInfoActivity.A0Y = null;
            }
        }
    }

    public void A2i() {
        C51442Ut r1;
        if (this instanceof GroupChatInfo) {
            AnonymousClass31K r12 = ((GroupChatInfo) this).A0w;
            if (r12 != null) {
                r12.A07 = Boolean.TRUE;
            }
        } else if (!(this instanceof ListChatInfo) && (r1 = ((ContactInfoActivity) this).A0x) != null) {
            r1.A06 = Boolean.TRUE;
        }
    }

    public void A2j() {
        DialogFragment A00;
        AbstractC14640lm A2g = A2g();
        AbstractC14640lm A2g2 = A2g();
        C19990v2 r1 = this.A09;
        C15550nR r0 = this.A06;
        if (A2g2 == null || C32341c0.A00(r0, r1, A2g2) <= 0) {
            A00 = ChatMediaVisibilityDialog.A00(new AnonymousClass4KC(this), A2g);
        } else {
            A00 = new ChatMediaEphemeralVisibilityDialog();
        }
        Adm(A00);
    }

    public void A2k() {
        A2l();
        AnonymousClass018 r3 = this.A08;
        C20050v8 r5 = this.A0E;
        C15660nh r6 = this.A0F;
        AbstractC14640lm A2g = A2g();
        AnonymousClass009.A05(A2g);
        AnonymousClass38W r2 = new AnonymousClass38W(r3, this.A0B, r5, r6, A2g, (MediaCard) findViewById(R.id.media_card_view));
        this.A05 = r2;
        ((ActivityC13830kP) this).A05.Aaz(r2, new Void[0]);
    }

    public void A2l() {
        AnonymousClass38W r1 = this.A05;
        if (r1 != null) {
            r1.A03(true);
            this.A05 = null;
        }
    }

    public void A2m(int i) {
        if (C28391Mz.A02()) {
            getWindow().setStatusBarColor(i);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0019, code lost:
        if (r1 <= 0) goto L_0x001b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A2n(int r6) {
        /*
            r5 = this;
            r0 = 2131364336(0x7f0a09f0, float:1.8348506E38)
            android.view.View r4 = X.AnonymousClass00T.A05(r5, r0)
            r3 = 2131889301(0x7f120c95, float:1.9413262E38)
            X.0lm r2 = r5.A2g()
            X.0v2 r1 = r5.A09
            X.0nR r0 = r5.A06
            if (r2 == 0) goto L_0x001b
            int r1 = X.C32341c0.A00(r0, r1, r2)
            r0 = 1
            if (r1 > 0) goto L_0x001c
        L_0x001b:
            r0 = 0
        L_0x001c:
            r2 = 0
            if (r0 != 0) goto L_0x0037
            r1 = 1
            if (r6 == 0) goto L_0x0029
            r1 = 0
            r0 = 2
            if (r0 != r6) goto L_0x0029
            r3 = 2131889302(0x7f120c96, float:1.9413264E38)
        L_0x0029:
            boolean r0 = r4 instanceof com.whatsapp.ListItemWithLeftIcon
            if (r0 == 0) goto L_0x0036
            com.whatsapp.ListItemWithLeftIcon r4 = (com.whatsapp.ListItemWithLeftIcon) r4
            if (r1 == 0) goto L_0x0039
            r0 = 8
            r4.setDescriptionVisibility(r0)
        L_0x0036:
            return
        L_0x0037:
            r1 = 0
            goto L_0x0029
        L_0x0039:
            java.lang.String r0 = r5.getString(r3)
            r4.setDescription(r0)
            r4.setDescriptionVisibility(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractActivityC33001d7.A2n(int):void");
    }

    public void A2o(long j) {
        View findViewById = findViewById(R.id.starred_messages_layout);
        View findViewById2 = findViewById(R.id.starred_messages_separator);
        if (j == 0) {
            findViewById.setVisibility(8);
            if (findViewById2 != null) {
                findViewById2.setVisibility(8);
                return;
            }
            return;
        }
        findViewById.setVisibility(0);
        if (findViewById2 != null) {
            findViewById2.setVisibility(0);
        }
        TextView textView = (TextView) findViewById.findViewById(R.id.starred_messages_count);
        if (textView == null) {
            if (findViewById instanceof ListItemWithLeftIcon) {
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
                textView = new WaTextView(this);
                textView.setId(R.id.starred_messages_count);
                textView.setLayoutParams(layoutParams);
                ((ViewGroup) AnonymousClass028.A0D(findViewById, R.id.right_view_container)).addView(textView);
            } else {
                return;
            }
        }
        textView.setText(this.A08.A0J().format(j));
    }

    public void A2p(Bitmap bitmap) {
        View view;
        ChatInfoLayoutV2 chatInfoLayoutV2 = (ChatInfoLayoutV2) ((AnonymousClass2Ew) findViewById(R.id.content));
        AnonymousClass028.A0D(chatInfoLayoutV2, R.id.photo_progress).setVisibility(8);
        chatInfoLayoutV2.A06.setImageBitmap(bitmap);
        if (chatInfoLayoutV2.A0C) {
            view = chatInfoLayoutV2.A06;
        } else {
            view = chatInfoLayoutV2.A04;
        }
        view.setOnClickListener(((AnonymousClass2Ew) chatInfoLayoutV2).A0A);
        AnonymousClass0PI r5 = new AnonymousClass0PI(bitmap);
        new AnonymousClass0AI(r5, new C67453Ro(this)).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, r5.A01);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0095, code lost:
        if (r15.A09() != false) goto L_0x0097;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A2q(android.view.View r13, android.widget.CompoundButton.OnCheckedChangeListener r14, X.C33181da r15) {
        /*
        // Method dump skipped, instructions count: 314
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractActivityC33001d7.A2q(android.view.View, android.widget.CompoundButton$OnCheckedChangeListener, X.1da):void");
    }

    public void A2r(Integer num) {
        View view;
        AnonymousClass2Ew r4 = (AnonymousClass2Ew) findViewById(R.id.content);
        ChatInfoLayoutV2 chatInfoLayoutV2 = (ChatInfoLayoutV2) r4;
        AnonymousClass028.A0D(chatInfoLayoutV2, R.id.photo_progress).setVisibility(8);
        if (num != null) {
            ImageView imageView = chatInfoLayoutV2.A06;
            int intValue = num.intValue();
            int dimension = (int) chatInfoLayoutV2.getResources().getDimension(R.dimen.chat_info_profile_photo_max_size);
            imageView.setImageBitmap(AnonymousClass130.A00(imageView.getContext(), chatInfoLayoutV2.A00, intValue, dimension));
        }
        if (chatInfoLayoutV2.A0C) {
            view = chatInfoLayoutV2.A06;
        } else {
            view = chatInfoLayoutV2.A04;
        }
        view.setOnClickListener(((AnonymousClass2Ew) chatInfoLayoutV2).A0A);
        r4.setColor(AnonymousClass00T.A00(this, R.color.primary));
        findViewById(R.id.bottom_shade).setBackgroundColor(0);
        findViewById(R.id.top_shade).setBackgroundColor(0);
    }

    public void A2s(String str, int i) {
        View A0D = AnonymousClass028.A0D(((ActivityC13810kN) this).A00, R.id.exit_group_btn);
        if (A0D instanceof ListItemWithLeftIcon) {
            AbstractC58392on r1 = (AbstractC58392on) A0D;
            r1.setTitle(str);
            r1.setIcon(i);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0096, code lost:
        if (r10 != 29) goto L_0x00b8;
     */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00c5  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00e4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A2t(java.util.ArrayList r23) {
        /*
        // Method dump skipped, instructions count: 241
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractActivityC33001d7.A2t(java.util.ArrayList):void");
    }

    @Override // android.app.Activity
    public void finishAfterTransition() {
        A2h();
        super.finishAfterTransition();
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 34 && i2 == -1) {
            viewMedia(null);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        AnonymousClass1IS A03;
        if (AbstractC454421p.A00) {
            Window window = getWindow();
            window.requestFeature(13);
            if (getIntent().getBooleanExtra("circular_transition", false)) {
                window.requestFeature(12);
                C52342ae r2 = new C52342ae(true, false);
                r2.addTarget(new AnonymousClass2TT(this).A00(R.string.transition_photo));
                window.setSharedElementEnterTransition(r2);
                r2.addListener(new C83823xx(this));
            }
            Fade fade = new Fade();
            fade.excludeTarget(16908335, true);
            fade.excludeTarget(16908336, true);
            window.setReturnTransition(fade);
            window.setEnterTransition(fade);
            window.addFlags(Integer.MIN_VALUE);
            window.clearFlags(67108864);
        }
        A1b(5);
        super.onCreate(bundle);
        if (bundle != null && (A03 = C38211ni.A03(bundle, "requested_message")) != null) {
            this.A0M = (AbstractC16130oV) this.A0A.A0K.A03(A03);
        }
    }

    @Override // X.ActivityC13770kJ, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        A2h();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        if (isFinishing()) {
            A2h();
        }
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        AbstractC16130oV r0 = this.A0M;
        if (r0 != null) {
            C38211ni.A08(bundle, r0.A0z, "requested_message");
        }
    }

    public final void viewMedia(View view) {
        if (this.A0M != null) {
            AnonymousClass2TS r1 = new AnonymousClass2TS(this);
            AbstractC14640lm A2g = A2g();
            AnonymousClass009.A05(A2g);
            r1.A03 = A2g;
            r1.A04 = this.A0M.A0z;
            r1.A00 = 34;
            Intent A00 = r1.A00();
            if (view != null) {
                AbstractC454421p.A08(this, A00, view, new AnonymousClass2TT(this), AbstractC42671vd.A0Z(this.A0M.A0z.toString()));
            } else {
                startActivity(A00);
            }
        }
    }
}
