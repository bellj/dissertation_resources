package X;

import android.text.Editable;
import android.text.TextWatcher;
import com.google.android.material.textfield.TextInputLayout;

/* renamed from: X.4mJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C100684mJ implements TextWatcher {
    public final /* synthetic */ TextInputLayout A00;

    @Override // android.text.TextWatcher
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    @Override // android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public C100684mJ(TextInputLayout textInputLayout) {
        this.A00 = textInputLayout;
    }

    @Override // android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        TextInputLayout textInputLayout = this.A00;
        textInputLayout.A0G(!textInputLayout.A0d, false);
        if (textInputLayout.A0R) {
            textInputLayout.A0D(editable.length());
        }
    }
}
