package X;

import com.facebook.common.util.TriState;

/* renamed from: X.0M7  reason: invalid class name */
/* loaded from: classes.dex */
public /* synthetic */ class AnonymousClass0M7 {
    public static final /* synthetic */ int[] A00;

    static {
        int[] iArr = new int[TriState.values().length];
        A00 = iArr;
        try {
            iArr[TriState.YES.ordinal()] = 1;
        } catch (NoSuchFieldError unused) {
        }
        try {
            iArr[TriState.NO.ordinal()] = 2;
        } catch (NoSuchFieldError unused2) {
        }
        try {
            iArr[TriState.UNSET.ordinal()] = 3;
        } catch (NoSuchFieldError unused3) {
        }
    }
}
