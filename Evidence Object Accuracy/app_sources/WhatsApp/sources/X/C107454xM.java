package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

/* renamed from: X.4xM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107454xM implements AnonymousClass5YX {
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(9);
    public final String A00;
    public final String A01;
    public final byte[] A02;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C107454xM(Parcel parcel) {
        this.A02 = parcel.createByteArray();
        this.A00 = parcel.readString();
        this.A01 = parcel.readString();
    }

    public C107454xM(String str, String str2, byte[] bArr) {
        this.A02 = bArr;
        this.A00 = str;
        this.A01 = str2;
    }

    @Override // X.AnonymousClass5YX
    public /* synthetic */ byte[] AHp() {
        return null;
    }

    @Override // X.AnonymousClass5YX
    public /* synthetic */ C100614mC AHq() {
        return null;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || C107454xM.class != obj.getClass()) {
            return false;
        }
        return Arrays.equals(this.A02, ((C107454xM) obj).A02);
    }

    @Override // java.lang.Object
    public int hashCode() {
        return Arrays.hashCode(this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        Object[] objArr = new Object[3];
        objArr[0] = this.A00;
        objArr[1] = this.A01;
        C12990iw.A1V(objArr, this.A02.length);
        return String.format("ICY: title=\"%s\", url=\"%s\", rawMetadata.length=\"%s\"", objArr);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByteArray(this.A02);
        parcel.writeString(this.A00);
        parcel.writeString(this.A01);
    }
}
