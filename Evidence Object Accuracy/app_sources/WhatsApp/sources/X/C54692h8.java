package X;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.LruCache;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.2h8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54692h8 extends AbstractC018308n {
    public int A00;
    public Rect A01 = C12980iv.A0J();
    public LruCache A02 = new LruCache(50);
    public View A03;
    public boolean A04;
    public final AbstractC33071dF A05;

    public C54692h8(Context context, RecyclerView recyclerView, AbstractC33071dF r5, AnonymousClass4JZ r6) {
        this.A05 = r5;
        recyclerView.A14.add(new AnonymousClass3S1(new AnonymousClass0MU(context, new C73613gW(this, r6)), r5, this, r6));
        ((AnonymousClass02M) r5).A01.registerObserver(new C74863iu(this));
    }

    public static /* synthetic */ void A00(C54692h8 r2, int i, int i2) {
        LruCache lruCache = r2.A02;
        if (lruCache.size() != 0) {
            for (int i3 = i; i3 <= i + i2; i3++) {
                lruCache.remove(Integer.valueOf(i3));
            }
        }
    }

    @Override // X.AbstractC018308n
    public void A02(Canvas canvas, C05480Ps r11, RecyclerView recyclerView) {
        AnonymousClass02H layoutManager;
        View A0D;
        AbstractC33071dF r8 = this.A05;
        if (!(!r8.AdR() || (layoutManager = recyclerView.getLayoutManager()) == null || (A0D = layoutManager.A0D(0)) == null)) {
            int A00 = RecyclerView.A00(A0D);
            if (A00 == -1) {
                View view = this.A03;
                canvas.save();
                canvas.translate((float) this.A01.left, 0.0f);
                view.draw(canvas);
            } else {
                int ADJ = r8.ADJ(A00);
                if (ADJ == -1) {
                    this.A04 = true;
                    return;
                }
                this.A04 = false;
                if (A00 == ADJ) {
                    layoutManager.A0J(A0D, this.A01);
                }
                LruCache lruCache = this.A02;
                Integer valueOf = Integer.valueOf(ADJ);
                View view2 = (View) lruCache.get(valueOf);
                if (view2 == null) {
                    AnonymousClass03U AOl = r8.AOl(recyclerView, r8.getItemViewType(ADJ));
                    r8.ANH(AOl, ADJ);
                    view2 = AOl.A0H;
                    lruCache.put(valueOf, view2);
                }
                Rect rect = this.A01;
                view2.measure(ViewGroup.getChildMeasureSpec(C12980iv.A04(recyclerView.getWidth()), recyclerView.getPaddingLeft() + recyclerView.getPaddingRight(), view2.getLayoutParams().width), ViewGroup.getChildMeasureSpec(View.MeasureSpec.makeMeasureSpec(recyclerView.getHeight(), 0), recyclerView.getPaddingTop() + recyclerView.getPaddingBottom(), view2.getLayoutParams().height));
                this.A00 = view2.getMeasuredHeight() + rect.bottom + rect.top;
                view2.layout(0, 0, view2.getMeasuredWidth() + rect.left + rect.right, this.A00);
                this.A03 = view2;
                int bottom = view2.getBottom() - rect.top;
                for (int i = 0; i < recyclerView.getChildCount(); i++) {
                    View childAt = recyclerView.getChildAt(i);
                    Rect A0J = C12980iv.A0J();
                    RecyclerView.A03(childAt, A0J);
                    if (A0J.bottom > bottom && A0J.top <= bottom) {
                        if (childAt != null) {
                            int A002 = RecyclerView.A00(childAt);
                            if (A002 == -1 || !r8.AJU(A002)) {
                                canvas.save();
                                canvas.translate((float) rect.left, 0.0f);
                            } else {
                                canvas.save();
                                canvas.translate((float) rect.left, (float) (childAt.getTop() - view2.getHeight()));
                            }
                            view2.draw(canvas);
                        } else {
                            return;
                        }
                    }
                }
                return;
            }
            canvas.restore();
        }
    }
}
