package X;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.CountDownTimer;
import android.widget.Button;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.3FQ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3FQ {
    public int A00;
    public long A01;
    public CountDownTimer A02;
    public AnonymousClass4L2 A03;
    public final int A04;
    public final int A05;
    public final int A06;
    public final Button A07;
    public final TextView A08;
    public final AnonymousClass018 A09;

    public AnonymousClass3FQ(Activity activity, AnonymousClass018 r4, int i, int i2, int i3, int i4, int i5, int i6) {
        this.A09 = r4;
        this.A06 = i3;
        this.A04 = i4;
        this.A00 = i5;
        this.A05 = i6;
        Button button = (Button) activity.findViewById(i);
        this.A07 = button;
        this.A08 = (TextView) activity.findViewById(i2);
        button.setAllCaps(false);
        A02(true);
    }

    public void A00() {
        CountDownTimer countDownTimer = this.A02;
        if (countDownTimer != null) {
            if (this.A01 <= 300000) {
                countDownTimer.cancel();
                this.A02 = null;
                this.A01 = 0;
            } else {
                return;
            }
        }
        A01(300000, false);
    }

    public final void A01(long j, boolean z) {
        if (j < 3000) {
            A02(true);
            return;
        }
        A02(false);
        CountDownTimer countDownTimer = this.A02;
        if (countDownTimer != null) {
            countDownTimer.cancel();
            this.A01 = 0;
        }
        C38131nZ.A0C(this.A08, this.A09, C12980iv.A0D(j));
        System.currentTimeMillis();
        this.A02 = new CountDownTimerC51932Zs(this, j, z).start();
    }

    public void A02(boolean z) {
        Drawable A0C;
        Context context;
        int i;
        Button button = this.A07;
        button.setEnabled(z);
        if (z) {
            CountDownTimer countDownTimer = this.A02;
            if (countDownTimer != null) {
                countDownTimer.cancel();
                this.A02 = null;
            }
            button.setEnabled(true);
            button.setText(this.A00);
            A0C = C12970iu.A0C(button.getContext(), this.A06);
            context = button.getContext();
            i = R.color.verify_sms_enabled_icon_color;
        } else {
            A0C = C12970iu.A0C(button.getContext(), this.A04);
            context = button.getContext();
            i = R.color.verify_sms_disabled_icon_color;
        }
        Drawable A03 = AnonymousClass2GE.A03(context, A0C, i);
        if (C28141Kv.A01(this.A09)) {
            button.setCompoundDrawablesWithIntrinsicBounds(A03, (Drawable) null, (Drawable) null, (Drawable) null);
        } else {
            button.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, A03, (Drawable) null);
        }
        this.A08.setVisibility(8);
    }
}
