package X;

import android.graphics.PointF;
import java.util.Locale;
import java.util.regex.Pattern;

/* renamed from: X.4aD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C93464aD {
    public static final Pattern A02 = Pattern.compile("\\\\an(\\d+)");
    public static final Pattern A03 = Pattern.compile("\\{([^}]*)\\}");
    public static final Pattern A04;
    public static final Pattern A05;
    public final int A00;
    public final PointF A01;

    static {
        Locale locale = Locale.US;
        A05 = Pattern.compile(String.format(locale, "\\\\pos\\((%1$s),(%1$s)\\)", "\\s*\\d+(?:\\.\\d+)?\\s*"));
        A04 = Pattern.compile(String.format(locale, "\\\\move\\(%1$s,%1$s,(%1$s),(%1$s)(?:,%1$s,%1$s)?\\)", "\\s*\\d+(?:\\.\\d+)?\\s*"));
    }

    public C93464aD(PointF pointF, int i) {
        this.A00 = i;
        this.A01 = pointF;
    }
}
