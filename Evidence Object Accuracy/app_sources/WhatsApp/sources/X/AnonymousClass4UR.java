package X;

/* renamed from: X.4UR  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass4UR {
    public void A00() {
        if (this instanceof C80723sq) {
            ((C80723sq) this).A01.run();
        } else if (!(this instanceof C80713sp)) {
            ((C80703so) this).A00.A04 = null;
        } else {
            ViewTreeObserver$OnGlobalLayoutListenerC33691ev r1 = ((C80713sp) this).A00;
            r1.A02(0);
            r1.A05.clear();
        }
    }
}
