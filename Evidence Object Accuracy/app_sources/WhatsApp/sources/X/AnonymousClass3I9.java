package X;

import android.graphics.Paint;
import android.os.Build;

/* renamed from: X.3I9  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3I9 {
    public static final C006202y A00 = new C006202y(50);

    public static Paint.FontMetricsInt A00(Paint paint) {
        int i;
        if (Build.VERSION.SDK_INT > 23 || !Build.MANUFACTURER.equalsIgnoreCase("samsung")) {
            return paint.getFontMetricsInt();
        }
        int A05 = C12990iw.A05((Float.floatToIntBits(paint.getTextSize()) + 31) * 31, paint.getTextSkewX());
        float f = 0.0f;
        if (paint.isFakeBoldText()) {
            f = 1.0f;
        }
        int A052 = C12990iw.A05(A05, f);
        if (paint.getTypeface() != null) {
            i = paint.getTypeface().hashCode();
        } else {
            i = 0;
        }
        C006202y r2 = A00;
        Integer valueOf = Integer.valueOf(A052 + i);
        Paint.FontMetricsInt fontMetricsInt = (Paint.FontMetricsInt) r2.A04(valueOf);
        if (fontMetricsInt != null) {
            return fontMetricsInt;
        }
        Paint.FontMetricsInt fontMetricsInt2 = paint.getFontMetricsInt();
        r2.A08(valueOf, fontMetricsInt2);
        return fontMetricsInt2;
    }
}
