package X;

import android.content.Context;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1Aa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C25601Aa {
    public List A00(Context context) {
        ArrayList arrayList = new ArrayList();
        AnonymousClass4BM r1 = AnonymousClass4BM.A04;
        arrayList.add(new AnonymousClass3M2(r1.id, context.getString(R.string.biz_chips_cat_restaurant), AnonymousClass4E7.A00(r1.id)));
        AnonymousClass4BM r12 = AnonymousClass4BM.A02;
        arrayList.add(new AnonymousClass3M2(r12.id, context.getString(R.string.biz_chips_cat_grocery_store), AnonymousClass4E7.A00(r12.id)));
        AnonymousClass4BM r13 = AnonymousClass4BM.A01;
        arrayList.add(new AnonymousClass3M2(r13.id, context.getString(R.string.biz_chips_cat_apparel_clothing), AnonymousClass4E7.A00(r13.id)));
        arrayList.add(new AnonymousClass3M2(null, context.getString(R.string.more), 0));
        return arrayList;
    }
}
