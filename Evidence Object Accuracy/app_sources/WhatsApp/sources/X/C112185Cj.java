package X;

import com.whatsapp.polls.PollVoterViewModel;
import java.util.Comparator;

/* renamed from: X.5Cj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C112185Cj implements Comparator {
    public final /* synthetic */ PollVoterViewModel A00;

    public C112185Cj(PollVoterViewModel pollVoterViewModel) {
        this.A00 = pollVoterViewModel;
    }

    @Override // java.util.Comparator
    public int compare(Object obj, Object obj2) {
        return (((C27701Iu) obj).A01 > ((C27701Iu) obj2).A01 ? 1 : (((C27701Iu) obj).A01 == ((C27701Iu) obj2).A01 ? 0 : -1));
    }
}
