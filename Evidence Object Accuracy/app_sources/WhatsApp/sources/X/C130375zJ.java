package X;

import java.util.HashMap;
import java.util.Map;

/* renamed from: X.5zJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130375zJ {
    public static final Map A00;

    static {
        HashMap hashMap = new HashMap(1);
        A00 = hashMap;
        hashMap.put("BR", "bloks_pay4");
        hashMap.put("US", "bloks_pay6");
        hashMap.put("GT", "bloks_pay6");
    }

    public static String A00(String str) {
        return C12970iu.A0t(str, A00);
    }
}
