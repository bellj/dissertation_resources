package X;

/* renamed from: X.2SM  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2SM implements AnonymousClass2SN {
    @Override // X.AnonymousClass2SN
    public String A64(String str, Object obj) {
        return "***";
    }

    public boolean equals(Object obj) {
        return obj != null && getClass() == obj.getClass();
    }

    public int hashCode() {
        return getClass().hashCode();
    }
}
