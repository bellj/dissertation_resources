package X;

/* renamed from: X.0Tn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C06420Tn {
    public AnonymousClass5T4 A00;

    public C06420Tn() {
        this(null, null, 1);
    }

    public C06420Tn(AnonymousClass5T4 r2) {
        this.A00 = null;
    }

    public /* synthetic */ C06420Tn(AnonymousClass5T4 r2, C51012Sk r3, int i) {
        this(null);
    }

    public final AnonymousClass5T4 A00() {
        return this.A00;
    }

    public void A01() {
        throw new C87394Bi(C16700pc.A08("An operation is not implemented: ", "Not yet implemented"));
    }

    public synchronized void A02() {
        this.A00 = null;
    }

    public synchronized void A03(AnonymousClass5T4 r2) {
        this.A00 = r2;
    }

    public synchronized void A04(AnonymousClass5T4 r2) {
        C16700pc.A0E(r2, 0);
        if (!C16700pc.A0O(this.A00, r2)) {
            this.A00 = null;
        }
    }
}
