package X;

import com.facebook.redex.RunnableBRunnable0Shape2S0100000_I0_2;
import com.whatsapp.util.Log;

/* renamed from: X.16v  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C247816v implements AbstractC18630sl {
    public final C16590pI A00;
    public final AbstractC14440lR A01;

    public C247816v(C16590pI r1, AbstractC14440lR r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC18630sl
    public void ASI(boolean z) {
        if (z) {
            Log.i("BackupLoginObserver/onLoginChanged delete backup key");
            this.A01.Ab2(new RunnableBRunnable0Shape2S0100000_I0_2(this, 8));
        }
    }
}
