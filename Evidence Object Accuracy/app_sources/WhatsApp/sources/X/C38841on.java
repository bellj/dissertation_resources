package X;

import android.content.ContentValues;
import android.database.Cursor;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1on  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38841on implements AnonymousClass1BW {
    public final C38851oo A00;
    public volatile List A01;

    public C38841on(C19450u8 r3, boolean z) {
        this.A00 = new C38851oo((C240413z) r3.A00.A01.AKZ.get(), z);
    }

    @Override // X.AnonymousClass1BW
    public AbstractC38871oq A8G(Object obj, float f) {
        return new C38881or((C38861op) obj, f);
    }

    @Override // X.AnonymousClass1BW
    public /* bridge */ /* synthetic */ Object ADE(String str) {
        for (C38881or r0 : this.A01) {
            C38861op r2 = r0.A01;
            if (str.equals(r2.A01)) {
                return r2;
            }
        }
        AnonymousClass1KS r1 = new AnonymousClass1KS();
        r1.A0C = str;
        return new C38861op(r1, str, null);
    }

    @Override // X.AnonymousClass1BW
    public String ADj(Object obj) {
        return ((C38861op) obj).A01;
    }

    @Override // X.AnonymousClass1BW
    public List AIX() {
        String str;
        AnonymousClass009.A00();
        C38851oo r3 = this.A00;
        ArrayList arrayList = new ArrayList();
        String[] strArr = new String[1];
        if (r3.A01) {
            str = "1";
        } else {
            str = "0";
        }
        strArr[0] = str;
        C16310on A01 = r3.A00.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT plaintext_hash, entry_weight, hash_of_image_part, url, enc_hash, direct_path, mimetype, media_key, file_size, width, height, emojis, is_first_party, is_avocado FROM recent_stickers WHERE is_avocado = ? ORDER BY entry_weight DESC", strArr);
            int columnIndexOrThrow = A09.getColumnIndexOrThrow("plaintext_hash");
            int columnIndexOrThrow2 = A09.getColumnIndexOrThrow("entry_weight");
            int columnIndexOrThrow3 = A09.getColumnIndexOrThrow("hash_of_image_part");
            int columnIndexOrThrow4 = A09.getColumnIndexOrThrow("url");
            int columnIndexOrThrow5 = A09.getColumnIndexOrThrow("enc_hash");
            int columnIndexOrThrow6 = A09.getColumnIndexOrThrow("direct_path");
            int columnIndexOrThrow7 = A09.getColumnIndexOrThrow("mimetype");
            int columnIndexOrThrow8 = A09.getColumnIndexOrThrow("media_key");
            int columnIndexOrThrow9 = A09.getColumnIndexOrThrow("file_size");
            int columnIndexOrThrow10 = A09.getColumnIndexOrThrow("width");
            int columnIndexOrThrow11 = A09.getColumnIndexOrThrow("height");
            int columnIndexOrThrow12 = A09.getColumnIndexOrThrow("emojis");
            int columnIndexOrThrow13 = A09.getColumnIndexOrThrow("is_first_party");
            while (A09.moveToNext()) {
                String string = A09.getString(columnIndexOrThrow);
                float f = A09.getFloat(columnIndexOrThrow2);
                String string2 = A09.getString(columnIndexOrThrow3);
                AnonymousClass1KS r1 = new AnonymousClass1KS();
                r1.A0C = string;
                r1.A0F = A09.getString(columnIndexOrThrow4);
                r1.A07 = A09.getString(columnIndexOrThrow5);
                r1.A05 = A09.getString(columnIndexOrThrow6);
                r1.A0B = A09.getString(columnIndexOrThrow7);
                r1.A0A = A09.getString(columnIndexOrThrow8);
                r1.A00 = A09.getInt(columnIndexOrThrow9);
                r1.A03 = A09.getInt(columnIndexOrThrow10);
                r1.A02 = A09.getInt(columnIndexOrThrow11);
                r1.A06 = A09.getString(columnIndexOrThrow12);
                boolean z = true;
                if (A09.getInt(columnIndexOrThrow13) != 1) {
                    z = false;
                }
                r1.A0H = z;
                r1.A09 = string2;
                C37431mO.A00(r1);
                arrayList.add(new C38881or(new C38861op(r1, string, string2), f));
            }
            A09.close();
            A01.close();
            this.A01 = arrayList;
            return this.A01;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    @Override // X.AnonymousClass1BW
    public void AZH(List list) {
        String str;
        AnonymousClass009.A00();
        this.A01 = new ArrayList(list);
        C38851oo r1 = this.A00;
        List<C38881or> list2 = this.A01;
        C16310on A02 = r1.A00.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            String[] strArr = new String[1];
            if (r1.A01) {
                str = "1";
            } else {
                str = "0";
            }
            strArr[0] = str;
            C16330op r5 = A02.A03;
            r5.A01("recent_stickers", "is_avocado = ?", strArr);
            for (C38881or r7 : list2) {
                ContentValues contentValues = new ContentValues();
                C38861op r2 = r7.A01;
                contentValues.put("plaintext_hash", r2.A01);
                contentValues.put("entry_weight", Float.valueOf(r7.A00));
                contentValues.put("hash_of_image_part", r2.A02);
                AnonymousClass1KS r72 = r2.A00;
                contentValues.put("url", r72.A0F);
                contentValues.put("enc_hash", r72.A07);
                contentValues.put("direct_path", r72.A05);
                contentValues.put("mimetype", r72.A0B);
                contentValues.put("media_key", r72.A0A);
                contentValues.put("file_size", Integer.valueOf(r72.A00));
                contentValues.put("width", Integer.valueOf(r72.A03));
                contentValues.put("height", Integer.valueOf(r72.A02));
                contentValues.put("emojis", r72.A06);
                int i = 0;
                if (r72.A0H) {
                    i = 1;
                }
                contentValues.put("is_first_party", Integer.valueOf(i));
                int i2 = 0;
                if (r72.A0G) {
                    i2 = 1;
                }
                contentValues.put("is_avocado", Integer.valueOf(i2));
                r5.A04(contentValues, "recent_stickers");
            }
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
