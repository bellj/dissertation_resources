package X;

import android.content.ContentValues;
import com.whatsapp.util.Log;

/* renamed from: X.1oo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38851oo {
    public final C240413z A00;
    public final boolean A01;

    public C38851oo(C240413z r1, boolean z) {
        this.A00 = r1;
        this.A01 = z;
    }

    public void A00(AnonymousClass1KS r8) {
        if (r8.A0C == null) {
            Log.e("RecentStickerDBStorage/updateSticker/sticker filehash is null, could not be updated");
            return;
        }
        C16310on A02 = this.A00.A02();
        int i = 1;
        try {
            String[] strArr = {r8.A0C};
            ContentValues contentValues = new ContentValues();
            contentValues.put("url", r8.A0F);
            contentValues.put("enc_hash", r8.A07);
            contentValues.put("direct_path", r8.A05);
            contentValues.put("mimetype", r8.A0B);
            contentValues.put("media_key", r8.A0A);
            contentValues.put("file_size", Integer.valueOf(r8.A00));
            contentValues.put("width", Integer.valueOf(r8.A03));
            contentValues.put("height", Integer.valueOf(r8.A02));
            contentValues.put("emojis", r8.A06);
            int i2 = 0;
            if (r8.A0H) {
                i2 = 1;
            }
            contentValues.put("is_first_party", Integer.valueOf(i2));
            if (!r8.A0G) {
                i = 0;
            }
            contentValues.put("is_avocado", Integer.valueOf(i));
            A02.A03.A00("recent_stickers", contentValues, "plaintext_hash = ?", strArr);
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
