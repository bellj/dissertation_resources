package X;

import android.os.SystemClock;
import com.whatsapp.util.Log;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.3Zy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69623Zy implements AnonymousClass5WG {
    public final /* synthetic */ C14310lE A00;

    public C69623Zy(C14310lE r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5WG
    public void AQL(Map map) {
        Iterator A0o = C12960it.A0o(map);
        while (A0o.hasNext()) {
            Log.e(A0o.next().toString());
        }
        C14310lE r2 = this.A00;
        C14320lF r0 = r2.A01;
        if (r0 != null) {
            String str = r0.A0K;
            long elapsedRealtime = SystemClock.elapsedRealtime();
            C14900mE r3 = r2.A0D;
            AbstractC14440lR r7 = r2.A0S;
            C49132Jk.A00(r3, r2.A0E, new AnonymousClass3UR(r2, elapsedRealtime), r2.A0O, r7, str);
        }
    }

    @Override // X.AnonymousClass5WG
    public void AQM(AnonymousClass3M3 r4) {
        C14310lE r2 = this.A00;
        C14320lF r1 = r2.A01;
        if (r1 != null && (r1 instanceof C83783xt)) {
            C83783xt r12 = (C83783xt) r1;
            r12.A00 = r4;
            String str = r4.A04;
            if (str != null) {
                r12.A0E = str;
            }
            String str2 = r4.A02;
            if (str2 != null) {
                r12.A0B = str2;
            }
            byte[] A00 = r4.A00();
            if (A00 != null) {
                r12.A0H = A00;
            }
            r2.A0A.A0A(r2.A01);
        }
    }
}
