package X;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.google.android.gms.dynamic.IObjectWrapper;

/* renamed from: X.3Li  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C65873Li implements IInterface {
    public final IBinder A00;
    public final String A01;

    public C65873Li(IBinder iBinder, String str) {
        this.A00 = iBinder;
        this.A01 = str;
    }

    @Override // android.os.IInterface
    public final IBinder asBinder() {
        return this.A00;
    }

    public static IObjectWrapper A00(Parcel parcel, C65873Li r1, int i) {
        Parcel A02 = r1.A02(i, parcel);
        IObjectWrapper A01 = AbstractBinderC79573qo.A01(A02.readStrongBinder());
        A02.recycle();
        return A01;
    }

    public final Parcel A01() {
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken(this.A01);
        return obtain;
    }

    public final Parcel A02(int i, Parcel parcel) {
        try {
            parcel = Parcel.obtain();
            C12990iw.A18(this.A00, parcel, parcel, i);
            return parcel;
        } catch (RuntimeException e) {
            throw e;
        } finally {
            parcel.recycle();
        }
    }

    public final void A03(int i, Parcel parcel) {
        Parcel obtain = Parcel.obtain();
        try {
            C12990iw.A18(this.A00, parcel, obtain, i);
        } finally {
            parcel.recycle();
            obtain.recycle();
        }
    }
}
