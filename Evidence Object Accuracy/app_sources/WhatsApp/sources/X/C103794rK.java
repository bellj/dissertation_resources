package X;

/* renamed from: X.4rK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103794rK implements AnonymousClass07L {
    public final /* synthetic */ AbstractC36001jA A00;

    @Override // X.AnonymousClass07L
    public boolean AUX(String str) {
        return false;
    }

    public C103794rK(AbstractC36001jA r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass07L
    public boolean AUY(String str) {
        AbstractC36001jA r3 = this.A00;
        r3.A0l = false;
        r3.A0J(r3.A03(), str, Math.max(r3.A01(), 50000), true);
        return false;
    }
}
