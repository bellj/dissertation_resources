package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import java.io.InputStream;

/* renamed from: X.49F  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass49F extends InputStream implements AnonymousClass5UR {
    public int A00 = -1;
    public int A01 = 0;
    public int A02 = 0;
    public byte[] A03 = new byte[EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH];

    @Override // java.io.InputStream
    public boolean markSupported() {
        return true;
    }

    public synchronized void A00() {
        this.A00 = this.A02;
    }

    @Override // X.AnonymousClass5UR
    public void A60(byte[] bArr, int i, int i2) {
        byte[] bArr2;
        byte[] bArr3 = this.A03;
        if (bArr3 == null) {
            throw C12990iw.A0i("Stream is closed.");
        } else if (i2 == 0) {
        } else {
            if (0 + i2 <= bArr.length) {
                int i3 = this.A02;
                int i4 = this.A01;
                int i5 = i4;
                int i6 = i4;
                if (i3 == i4 && this.A00 == -1) {
                    this.A01 = 0;
                    i5 = 0;
                    i6 = 0;
                    this.A02 = 0;
                    i3 = 0;
                }
                int i7 = i5 + i2;
                int length = bArr3.length;
                if (i7 > length) {
                    int i8 = this.A00;
                    int i9 = i5 - i8;
                    if (i8 == -1) {
                        i9 = i5 - i3;
                    }
                    int i10 = length - i9;
                    if (i10 < i2) {
                        bArr2 = new byte[length + (((int) Math.ceil(((double) (i2 - i10)) / 1024.0d)) << 10)];
                        if (i8 != -1) {
                            System.arraycopy(bArr3, i8, bArr2, 0, i5 - i8);
                            int i11 = this.A02;
                            int i12 = this.A00;
                            this.A02 = i11 - i12;
                            i6 = this.A01 - i12;
                            this.A01 = i6;
                            this.A00 = 0;
                        }
                        i6 = i5 - i3;
                        System.arraycopy(bArr3, i3, bArr2, 0, i6);
                        this.A02 = 0;
                        this.A01 = i6;
                        this.A00 = -1;
                    } else {
                        bArr2 = new byte[length];
                        if (i8 != -1) {
                            System.arraycopy(bArr3, i8, bArr2, 0, i9);
                            int i11 = this.A02;
                            int i12 = this.A00;
                            this.A02 = i11 - i12;
                            i6 = this.A01 - i12;
                            this.A01 = i6;
                            this.A00 = 0;
                        }
                        i6 = i5 - i3;
                        System.arraycopy(bArr3, i3, bArr2, 0, i6);
                        this.A02 = 0;
                        this.A01 = i6;
                        this.A00 = -1;
                    }
                    this.A03 = bArr2;
                    bArr3 = bArr2;
                }
                System.arraycopy(bArr, 0, bArr3, i6, i2);
                this.A01 += i2;
                return;
            }
            StringBuilder A0k = C12960it.A0k("Len ");
            A0k.append(i2);
            throw C12990iw.A0i(C12960it.A0d(" exceeds supplied buffer limits.", A0k));
        }
    }

    @Override // java.io.InputStream
    public int available() {
        if (this.A03 == null) {
            return -1;
        }
        return this.A01 - this.A02;
    }

    @Override // java.io.InputStream, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        if (this.A03 != null) {
            super.close();
            this.A03 = null;
            this.A01 = 0;
            this.A02 = 0;
            this.A00 = -1;
            return;
        }
        throw C12990iw.A0i("Stream is already closed.");
    }

    @Override // java.io.InputStream
    public synchronized void mark(int i) {
        A00();
    }

    @Override // java.io.InputStream
    public int read() {
        byte[] bArr = new byte[1];
        int read = read(bArr);
        if (read > 1) {
            throw C12990iw.A0i("Read returned more than 1 byte");
        } else if (read == 1) {
            return (short) (((short) (bArr[0] & 255)) | 0);
        } else {
            return -1;
        }
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr) {
        if (bArr != null) {
            return read(bArr, 0, bArr.length);
        }
        throw C12990iw.A0i("Dst buffer is null");
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr, int i, int i2) {
        if (bArr == null) {
            throw C12990iw.A0i("Dst buffer is null");
        } else if (i2 == 0) {
            return i2;
        } else {
            if (i + i2 <= bArr.length) {
                int available = available();
                if (available < 1) {
                    return available;
                }
                int min = Math.min(available, i2);
                System.arraycopy(this.A03, this.A02, bArr, i, min);
                this.A02 += min;
                return min;
            }
            throw C12990iw.A0i("Not enough space in destination buffer.");
        }
    }

    @Override // java.io.InputStream
    public synchronized void reset() {
        if (this.A03 != null) {
            int i = this.A00;
            if (i != -1) {
                this.A02 = i;
                this.A00 = -1;
            } else {
                throw C12990iw.A0i("No marked position found.");
            }
        } else {
            throw C12990iw.A0i("Stream is closed.");
        }
    }

    @Override // java.io.InputStream
    public long skip(long j) {
        if (this.A03 == null) {
            throw C12990iw.A0i("Stream is closed.");
        } else if (j <= 0) {
            return 0;
        } else {
            long j2 = ((long) this.A02) + j;
            int i = this.A01;
            if (j2 >= ((long) i)) {
                int available = available();
                this.A02 = i;
                return (long) available;
            }
            this.A02 = (int) j2;
            return j;
        }
    }
}
