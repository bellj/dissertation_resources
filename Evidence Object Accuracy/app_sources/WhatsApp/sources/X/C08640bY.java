package X;

import android.util.Log;
import java.io.PrintWriter;
import java.io.StringWriter;

/* renamed from: X.0bY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08640bY implements AbstractC12710iN {
    public static final C08640bY A01 = new C08640bY();
    public int A00 = 5;

    public final void A00(int i, String str, String str2) {
        StringBuilder sb = new StringBuilder();
        sb.append("unknown");
        sb.append(":");
        sb.append(str);
        Log.println(i, sb.toString(), str2);
    }

    public final void A01(String str, String str2, Throwable th, int i) {
        StringBuilder sb = new StringBuilder();
        sb.append("unknown");
        sb.append(":");
        sb.append(str);
        String obj = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append(str2);
        sb2.append('\n');
        StringWriter stringWriter = new StringWriter();
        th.printStackTrace(new PrintWriter(stringWriter));
        sb2.append(stringWriter.toString());
        Log.println(i, obj, sb2.toString());
    }

    @Override // X.AbstractC12710iN
    public void A9G(String str, String str2) {
        A00(6, str, str2);
    }

    @Override // X.AbstractC12710iN
    public void A9H(String str, String str2, Throwable th) {
        A01(str, str2, th, 6);
    }

    @Override // X.AbstractC12710iN
    public void AIU(String str, String str2) {
        A00(4, "OpticE2EConfig", str2);
    }

    @Override // X.AbstractC12710iN
    public boolean AJi(int i) {
        return this.A00 <= i;
    }

    @Override // X.AbstractC12710iN
    public void Aff(String str, String str2) {
        A00(2, str, str2);
    }

    @Override // X.AbstractC12710iN
    public void Ag0(String str, String str2) {
        A00(5, str, str2);
    }

    @Override // X.AbstractC12710iN
    public void Ag1(String str, String str2, Throwable th) {
        A01(str, str2, th, 5);
    }

    @Override // X.AbstractC12710iN
    public void AgJ(String str, String str2) {
        A00(6, str, str2);
    }

    @Override // X.AbstractC12710iN
    public void AgK(String str, String str2, Throwable th) {
        A01("FixedOrientationCompat", str2, th, 6);
    }
}
