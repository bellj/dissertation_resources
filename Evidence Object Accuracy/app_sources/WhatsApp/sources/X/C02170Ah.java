package X;

import android.graphics.PointF;
import android.util.Property;

/* renamed from: X.0Ah  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C02170Ah extends Property {
    public C02170Ah() {
        super(PointF.class, "topLeft");
    }

    @Override // android.util.Property
    public Object get(Object obj) {
        return null;
    }

    @Override // android.util.Property
    public void set(Object obj, Object obj2) {
        AnonymousClass0O3 r7 = (AnonymousClass0O3) obj;
        PointF pointF = (PointF) obj2;
        int round = Math.round(pointF.x);
        r7.A02 = round;
        int round2 = Math.round(pointF.y);
        r7.A04 = round2;
        int i = r7.A05 + 1;
        r7.A05 = i;
        if (i == r7.A01) {
            AnonymousClass0U3.A04.A06(r7.A06, round, round2, r7.A03, r7.A00);
            r7.A05 = 0;
            r7.A01 = 0;
        }
    }
}
