package X;

import android.graphics.SurfaceTexture;
import android.opengl.GLES20;
import com.whatsapp.util.Log;
import java.nio.ByteBuffer;

/* renamed from: X.4Vx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C92444Vx {
    public int A00;
    public final SurfaceTexture A01;
    public final ByteBuffer A02 = C72453ed.A0x(64);
    public final float[] A03 = new float[16];
    public volatile int A04;

    public C92444Vx() {
        int[] iArr = new int[1];
        GLES20.glGenTextures(1, iArr, 0);
        int i = iArr[0];
        GLES20.glBindTexture(36197, i);
        GLES20.glTexParameterf(36197, 10241, 9729.0f);
        GLES20.glTexParameterf(36197, 10240, 9729.0f);
        GLES20.glTexParameterf(36197, 10242, 33071.0f);
        GLES20.glTexParameterf(36197, 10243, 33071.0f);
        C93054Yu.A01("generateTexture");
        this.A00 = i;
        SurfaceTexture surfaceTexture = new SurfaceTexture(i);
        this.A01 = surfaceTexture;
        this.A04 = 0;
        Log.i(C12960it.A0b("voip/video/SurfaceTextureHolder/createSurfaceTexture, surfaceTexture = ", surfaceTexture));
    }
}
