package X;

import com.facebook.redex.RunnableBRunnable0Shape0S1200000_I0;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.HashSet;

/* renamed from: X.1xV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C43731xV implements AbstractC21730xt {
    public final AbstractC15710nm A00;
    public final C14900mE A01;
    public final C238013b A02;
    public final C14830m7 A03;
    public final C14820m6 A04;
    public final C17220qS A05;
    public final C37241lo A06;

    public C43731xV(AbstractC15710nm r1, C14900mE r2, C238013b r3, C14830m7 r4, C14820m6 r5, C17220qS r6, C37241lo r7) {
        this.A03 = r4;
        this.A01 = r2;
        this.A00 = r1;
        this.A05 = r6;
        this.A02 = r3;
        this.A04 = r5;
        this.A06 = r7;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        StringBuilder sb = new StringBuilder("getblocklistprotocolhelper/onDeliveryFailure iq=");
        sb.append(str);
        Log.i(sb.toString());
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r4, String str) {
        int A00 = C41151sz.A00(r4);
        StringBuilder sb = new StringBuilder("getblocklistprotocolhelper/onError, iq=");
        sb.append(str);
        sb.append("; errorCode=");
        sb.append(A00);
        Log.i(sb.toString());
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r9, String str) {
        AnonymousClass1V8 A0E = r9.A0E("list");
        if (A0E != null) {
            HashSet hashSet = new HashSet();
            String A0I = A0E.A0I("dhash", null);
            AnonymousClass1V8[] r5 = A0E.A03;
            if (r5 != null) {
                for (AnonymousClass1V8 r3 : r5) {
                    AnonymousClass1V8.A01(r3, "item");
                    hashSet.add(r3.A0A(this.A00, UserJid.class, "jid"));
                }
            }
            this.A01.A0I(new RunnableBRunnable0Shape0S1200000_I0(this, hashSet, A0I, 28));
        } else {
            this.A04.A00.edit().putLong("block_list_receive_time", this.A03.A00()).apply();
        }
        C37241lo r1 = this.A06;
        if (r1 != null) {
            r1.A00(4);
        }
    }
}
