package X;

import android.graphics.Outline;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewOutlineProvider;

/* renamed from: X.3gm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73773gm extends ViewOutlineProvider {
    public final /* synthetic */ AnonymousClass28D A00;

    public C73773gm(AnonymousClass28D r1) {
        this.A00 = r1;
    }

    @Override // android.view.ViewOutlineProvider
    public void getOutline(View view, Outline outline) {
        Drawable background = view.getBackground();
        if (background != null) {
            background.getOutline(outline);
            outline.setAlpha(this.A00.A09(65, 1.0f));
        }
    }
}
