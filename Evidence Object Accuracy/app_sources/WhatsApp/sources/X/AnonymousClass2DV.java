package X;

import com.whatsapp.jobqueue.job.GenerateTcTokenJob;

/* renamed from: X.2DV  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2DV implements AbstractC21730xt {
    public final /* synthetic */ GenerateTcTokenJob A00;
    public final /* synthetic */ AnonymousClass1VC A01;

    public AnonymousClass2DV(GenerateTcTokenJob generateTcTokenJob, AnonymousClass1VC r2) {
        this.A00 = generateTcTokenJob;
        this.A01 = r2;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        this.A01.A00(new AnonymousClass2JZ(str));
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r3, String str) {
        this.A01.A00(new C47732Ce(r3, str));
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r4, String str) {
        GenerateTcTokenJob generateTcTokenJob = this.A00;
        generateTcTokenJob.A00.A08(generateTcTokenJob.A01, generateTcTokenJob.timestamp);
        this.A01.A01(null);
    }
}
