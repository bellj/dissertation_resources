package X;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.stickers.StickerStorePackPreviewActivity;

/* renamed from: X.3jL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75123jL extends AbstractC05270Ox {
    public final /* synthetic */ StickerStorePackPreviewActivity A00;

    public C75123jL(StickerStorePackPreviewActivity stickerStorePackPreviewActivity) {
        this.A00 = stickerStorePackPreviewActivity;
    }

    @Override // X.AbstractC05270Ox
    public void A00(RecyclerView recyclerView, int i) {
        StickerStorePackPreviewActivity stickerStorePackPreviewActivity = this.A00;
        int i2 = 0;
        boolean A1U = C12960it.A1U(recyclerView.computeVerticalScrollOffset());
        View view = stickerStorePackPreviewActivity.A02;
        if (view != null) {
            if (!A1U) {
                i2 = 8;
            }
            view.setVisibility(i2);
        }
    }

    @Override // X.AbstractC05270Ox
    public void A01(RecyclerView recyclerView, int i, int i2) {
        StickerStorePackPreviewActivity stickerStorePackPreviewActivity = this.A00;
        int i3 = 0;
        boolean A1U = C12960it.A1U(recyclerView.computeVerticalScrollOffset());
        View view = stickerStorePackPreviewActivity.A02;
        if (view != null) {
            if (!A1U) {
                i3 = 8;
            }
            view.setVisibility(i3);
        }
    }
}
