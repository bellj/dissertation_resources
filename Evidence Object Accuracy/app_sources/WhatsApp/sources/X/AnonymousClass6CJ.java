package X;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiSendPaymentActivity;
import com.whatsapp.payments.ui.PaymentBottomSheet;
import com.whatsapp.payments.ui.widget.PaymentDescriptionRow;
import com.whatsapp.payments.ui.widget.PaymentMethodRow;

/* renamed from: X.6CJ  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6CJ implements AbstractC124835qC {
    public final /* synthetic */ AbstractC30791Yv A00;
    public final /* synthetic */ IndiaUpiSendPaymentActivity A01;
    public final /* synthetic */ PaymentBottomSheet A02;

    @Override // X.AbstractC124835qC
    public String ACJ(AbstractC28901Pl r2) {
        return null;
    }

    @Override // X.AbstractC124835qC
    public String ACK(AbstractC28901Pl r2) {
        return null;
    }

    @Override // X.AbstractC124835qC
    public String ACf(AbstractC28901Pl r2, int i) {
        return null;
    }

    @Override // X.AbstractC124835qC
    public String AEO(AbstractC28901Pl r2) {
        return null;
    }

    @Override // X.AbstractC124835qC
    public boolean AdO(AbstractC28901Pl r2, int i) {
        return false;
    }

    @Override // X.AbstractC124835qC
    public boolean AdU(AbstractC28901Pl r2) {
        return false;
    }

    @Override // X.AbstractC124835qC
    public boolean AdV() {
        return false;
    }

    @Override // X.AbstractC124835qC
    public void Adj(AbstractC28901Pl r1, PaymentMethodRow paymentMethodRow) {
    }

    @Override // X.AbstractC124835qC
    public boolean Adt() {
        return false;
    }

    public AnonymousClass6CJ(AbstractC30791Yv r1, IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity, PaymentBottomSheet paymentBottomSheet) {
        this.A01 = indiaUpiSendPaymentActivity;
        this.A02 = paymentBottomSheet;
        this.A00 = r1;
    }

    @Override // X.AbstractC124835qC
    public void A6F(ViewGroup viewGroup) {
        IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A01;
        C12960it.A0I(indiaUpiSendPaymentActivity.getLayoutInflater().inflate(R.layout.confirm_payment_total_amount_row, viewGroup, true), R.id.amount).setText(this.A00.AAA(((AbstractActivityC121545iU) indiaUpiSendPaymentActivity).A01, ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0A, 0));
    }

    @Override // X.AbstractC124835qC
    public String ABX(AbstractC28901Pl r3, int i) {
        return this.A01.getString(R.string.request_payment);
    }

    @Override // X.AbstractC124835qC
    public void AMM(ViewGroup viewGroup) {
        IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A01;
        C129515xq r3 = ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0X;
        boolean A3Y = indiaUpiSendPaymentActivity.A3Y();
        int i = 0;
        boolean A1W = C12960it.A1W(((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A0F);
        if (!A3Y || A1W || !r3.A01.A07(1718)) {
            i = 8;
        }
        viewGroup.setVisibility(i);
        PaymentDescriptionRow paymentDescriptionRow = (PaymentDescriptionRow) AnonymousClass028.A0D(viewGroup, R.id.payment_description_row);
        ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0V = paymentDescriptionRow;
        paymentDescriptionRow.A01(((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0a);
    }

    @Override // X.AbstractC124835qC
    public void AMN(ViewGroup viewGroup) {
        IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A01;
        View inflate = indiaUpiSendPaymentActivity.getLayoutInflater().inflate(R.layout.use_another_payment_account_bottom_sheet_header, viewGroup, true);
        C117295Zj.A0n(C117295Zj.A07(indiaUpiSendPaymentActivity, inflate, C12960it.A0I(inflate, R.id.text), R.string.request_payment), this.A02, 71);
    }

    @Override // X.AbstractC124835qC
    public void AMP(ViewGroup viewGroup) {
        IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A01;
        View inflate = indiaUpiSendPaymentActivity.getLayoutInflater().inflate(R.layout.india_upi_confirm_payment_recipient_row, viewGroup, true);
        ImageView A0K = C12970iu.A0K(inflate, R.id.payment_recipient_profile_pic);
        TextView A0I = C12960it.A0I(inflate, R.id.payment_recipient_direction_label);
        TextView A0I2 = C12960it.A0I(inflate, R.id.payment_recipient_name);
        TextView A0I3 = C12960it.A0I(inflate, R.id.payment_recipient_vpa);
        C117305Zk.A15(inflate, R.id.expand_receiver_details_button);
        A0I.setText(R.string.payments_send_payment_to);
        ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A01.A05(A0K, R.drawable.avatar_contact);
        C117315Zl.A0N(A0I2, C117295Zj.A0R(((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A06));
        Object obj = ((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A08.A00;
        AnonymousClass009.A05(obj);
        A0I3.setText(C12960it.A0X(indiaUpiSendPaymentActivity, obj, new Object[1], 0, R.string.india_upi_payment_id_with_upi_label));
    }

    @Override // X.AbstractC124835qC
    public void AQh(ViewGroup viewGroup, AbstractC28901Pl r4) {
        IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A01;
        AbstractActivityC119235dO.A0p(indiaUpiSendPaymentActivity.getLayoutInflater(), viewGroup, indiaUpiSendPaymentActivity);
    }
}
