package X;

import android.view.View;

/* renamed from: X.3io  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74803io extends AnonymousClass0FJ {
    public C64313Fb A00;

    public C74803io(AnonymousClass4AR r2, Float f) {
        this.A00 = new C64313Fb(r2, f);
    }

    @Override // X.AnonymousClass0FJ, X.AnonymousClass0FB
    public View A01(AnonymousClass02H r2) {
        return this.A00.A02(r2);
    }

    @Override // X.AnonymousClass0FJ, X.AnonymousClass0FB
    public int[] A04(View view, AnonymousClass02H r3) {
        return this.A00.A03(view, r3);
    }
}
