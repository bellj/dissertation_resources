package X;

import java.io.FilterInputStream;
import java.io.InputStream;

/* renamed from: X.2Ti  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C51212Ti extends FilterInputStream {
    public int A00;
    public boolean A01;

    @Override // java.io.FilterInputStream, java.io.InputStream
    public int available() {
        return 0;
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public boolean markSupported() {
        return false;
    }

    public C51212Ti(InputStream inputStream) {
        super(inputStream);
        if (inputStream == null) {
            throw new NullPointerException("in may not be null");
        }
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public synchronized void mark(int i) {
        throw new UnsupportedOperationException();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0119, code lost:
        return -1;
     */
    @Override // java.io.FilterInputStream, java.io.InputStream
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int read() {
        /*
        // Method dump skipped, instructions count: 282
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C51212Ti.read():int");
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public int read(byte[] bArr, int i, int i2) {
        for (int i3 = 0; i3 < i2; i3++) {
            int read = read();
            if (read != -1) {
                bArr[i + i3] = (byte) read;
            } else if (i3 <= 0) {
                return -1;
            } else {
                return i3;
            }
        }
        return i2;
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public synchronized void reset() {
        throw new UnsupportedOperationException();
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public long skip(long j) {
        for (long j2 = 0; j2 < j; j2++) {
            if (read() == -1) {
                return j2;
            }
        }
        return j;
    }
}
