package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.io.IOException;
import org.json.JSONObject;

/* renamed from: X.2uG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59262uG extends AnonymousClass32K {
    public final C14650lo A00;
    public final C19850um A01;
    public final C92164Uu A02;
    public final AnonymousClass19T A03;
    public final C19830uk A04;
    public final C18640sm A05;
    public final C15680nj A06;
    public final AnonymousClass4TA A07;
    public final C19870uo A08;
    public final C17220qS A09;
    public final C19840ul A0A;

    public C59262uG(C14650lo r9, C19850um r10, C92164Uu r11, AnonymousClass19T r12, C19810ui r13, C17560r0 r14, AnonymousClass4RQ r15, C17570r1 r16, C19830uk r17, C18640sm r18, C15680nj r19, AnonymousClass4TA r20, C19870uo r21, C17220qS r22, C19840ul r23, AbstractC14440lR r24) {
        super(r13, r14, r15, r16, r24, 2);
        this.A0A = r23;
        this.A00 = r9;
        this.A03 = r12;
        this.A06 = r19;
        this.A01 = r10;
        this.A07 = r20;
        this.A04 = r17;
        this.A09 = r22;
        this.A05 = r18;
        this.A08 = r21;
        this.A02 = r11;
    }

    @Override // X.AnonymousClass44L
    public void A00(AnonymousClass3H5 r7, JSONObject jSONObject, int i) {
        A04(null, "/onErrorResponse", i, r7.A00, true);
    }

    public final void A04(Exception exc, String str, int i, int i2, boolean z) {
        Log.e("GetProductGraphQLService/onError/response-error");
        this.A0A.A02("view_product_tag");
        AnonymousClass4TA r1 = this.A07;
        if (!A03(r1.A00, i2, z)) {
            String A0d = C12960it.A0d(str, C12960it.A0j("GetProductGraphQLService"));
            if (exc != null) {
                Log.e(A0d, exc);
            } else {
                Log.e(A0d);
            }
            this.A02.A00(r1, i);
        }
    }

    @Override // X.AbstractC44401yr
    public void AOz(IOException iOException) {
        A04(iOException, "/onDeliveryFailure", 0, -1, false);
    }

    @Override // X.AnonymousClass1W3
    public void APB(UserJid userJid) {
        Log.e(C12960it.A0d(userJid.getRawString(), C12960it.A0k("GetProductGraphQLService/direct-connection-error/jid=")));
        this.A02.A00(this.A07, 0);
    }

    @Override // X.AnonymousClass1W3
    public void APC(UserJid userJid) {
        A02();
    }

    @Override // X.AbstractC44401yr
    public void APp(Exception exc) {
        A04(exc, "/onError", 0, 0, false);
    }
}
