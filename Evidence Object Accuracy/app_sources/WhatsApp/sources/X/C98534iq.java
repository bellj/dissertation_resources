package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

/* renamed from: X.4iq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98534iq implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new C78923pl[i];
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        ArrayList arrayList = null;
        ArrayList arrayList2 = null;
        ArrayList arrayList3 = null;
        ArrayList arrayList4 = null;
        ArrayList arrayList5 = null;
        int i = 0;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 1:
                    i = C95664e9.A02(parcel, readInt);
                    break;
                case 2:
                    arrayList = C95664e9.A0A(parcel, readInt);
                    break;
                case 3:
                    arrayList2 = C95664e9.A0A(parcel, readInt);
                    break;
                case 4:
                    arrayList3 = C95664e9.A0A(parcel, readInt);
                    break;
                case 5:
                    arrayList4 = C95664e9.A0A(parcel, readInt);
                    break;
                case 6:
                    arrayList5 = C95664e9.A0A(parcel, readInt);
                    break;
                default:
                    C95664e9.A0D(parcel, readInt);
                    break;
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78923pl(arrayList, arrayList2, arrayList3, arrayList4, arrayList5, i);
    }
}
