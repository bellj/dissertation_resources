package X;

/* renamed from: X.1FU  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1FU extends AbstractC17250qV {
    public final AnonymousClass10G A00;
    public final C16590pI A01;
    public final C21690xp A02;
    public final C20660w7 A03;

    public AnonymousClass1FU(AbstractC15710nm r8, AnonymousClass10G r9, C16590pI r10, C17220qS r11, C21690xp r12, C20660w7 r13, C17230qT r14, AbstractC14440lR r15) {
        super(r8, r11, r14, r15, new int[]{235}, true);
        this.A01 = r10;
        this.A03 = r13;
        this.A00 = r9;
        this.A02 = r12;
    }
}
