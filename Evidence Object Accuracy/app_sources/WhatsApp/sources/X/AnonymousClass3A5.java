package X;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

@Deprecated
/* renamed from: X.3A5  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3A5 {
    @Deprecated
    public static byte[] A00(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        C13020j0.A01(inputStream);
        byte[] bArr = new byte[4096];
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }
}
