package X;

import android.database.Cursor;

/* renamed from: X.3YX  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3YX implements AnonymousClass5UQ {
    public final C16510p9 A00;
    public final C16490p7 A01;
    public final AbstractC14640lm A02;

    public AnonymousClass3YX(C16510p9 r1, C16490p7 r2, AbstractC14640lm r3) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
    }

    @Override // X.AnonymousClass5UQ
    public Cursor ACG(int i, long j, long j2, long j3) {
        String[] strArr = {String.valueOf(j), String.valueOf(j2), String.valueOf(this.A00.A02(this.A02)), String.valueOf(j3), String.valueOf(5000)};
        C16310on A01 = this.A01.get();
        try {
            Cursor A09 = A01.A03.A09(C32301bw.A0L, strArr);
            A01.close();
            return A09;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
