package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

/* renamed from: X.3mk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77103mk extends AbstractC107404xH {
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(12);
    public final int A00;
    public final int A01;
    public final long A02;
    public final long A03;
    public final String A04;
    public final AbstractC107404xH[] A05;

    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: X.4xH[] */
    /* JADX WARN: Multi-variable type inference failed */
    public C77103mk(Parcel parcel) {
        super("CHAP");
        this.A04 = parcel.readString();
        this.A01 = parcel.readInt();
        this.A00 = parcel.readInt();
        this.A03 = parcel.readLong();
        this.A02 = parcel.readLong();
        int readInt = parcel.readInt();
        this.A05 = new AbstractC107404xH[readInt];
        for (int i = 0; i < readInt; i++) {
            this.A05[i] = C12990iw.A0I(parcel, AbstractC107404xH.class);
        }
    }

    public C77103mk(String str, AbstractC107404xH[] r3, int i, int i2, long j, long j2) {
        super("CHAP");
        this.A04 = str;
        this.A01 = i;
        this.A00 = i2;
        this.A03 = j;
        this.A02 = j2;
        this.A05 = r3;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C77103mk.class != obj.getClass()) {
                return false;
            }
            C77103mk r7 = (C77103mk) obj;
            if (!(this.A01 == r7.A01 && this.A00 == r7.A00 && this.A03 == r7.A03 && this.A02 == r7.A02 && AnonymousClass3JZ.A0H(this.A04, r7.A04) && Arrays.equals(this.A05, r7.A05))) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return ((((((C72453ed.A05(this.A01) + this.A00) * 31) + ((int) this.A03)) * 31) + ((int) this.A02)) * 31) + C72453ed.A0E(this.A04);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A04);
        parcel.writeInt(this.A01);
        parcel.writeInt(this.A00);
        parcel.writeLong(this.A03);
        parcel.writeLong(this.A02);
        AbstractC107404xH[] r4 = this.A05;
        int length = r4.length;
        parcel.writeInt(length);
        for (AbstractC107404xH r0 : r4) {
            parcel.writeParcelable(r0, 0);
        }
    }
}
