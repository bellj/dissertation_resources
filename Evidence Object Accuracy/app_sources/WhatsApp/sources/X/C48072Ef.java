package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;
import android.view.ViewPropertyAnimator;

/* renamed from: X.2Ef  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C48072Ef extends AnimatorListenerAdapter {
    public final /* synthetic */ View A00;
    public final /* synthetic */ ViewPropertyAnimator A01;
    public final /* synthetic */ C48052Ed A02;
    public final /* synthetic */ C48042Ec A03;

    public C48072Ef(View view, ViewPropertyAnimator viewPropertyAnimator, C48052Ed r3, C48042Ec r4) {
        this.A03 = r4;
        this.A02 = r3;
        this.A01 = viewPropertyAnimator;
        this.A00 = view;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        this.A01.setListener(null);
        View view = this.A00;
        view.setAlpha(1.0f);
        view.setTranslationX(0.0f);
        view.setTranslationY(0.0f);
        C48042Ec r3 = this.A03;
        C48052Ed r2 = this.A02;
        r3.A03(r2.A04);
        r3.A03.remove(r2.A04);
        r3.A0H();
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        this.A03.A0C = true;
    }
}
