package X;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.regex.Pattern;

/* renamed from: X.4c0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC94534c0 {
    public static int A01(AbstractC94534c0 r1, AbstractC94534c0 r2) {
        return r1.A04().A00.compareTo(r2.A04().A00);
    }

    public static AbstractC94534c0 A02(Object obj) {
        char charAt;
        if (obj == null) {
            return AbstractC116885Xh.A02;
        }
        if (obj instanceof AbstractC94534c0) {
            return (AbstractC94534c0) obj;
        }
        if (obj instanceof Class) {
            return new C82943wR((Class) obj);
        }
        if (obj instanceof String) {
            String trim = obj.toString().trim();
            if (trim.length() > 0 && ((charAt = trim.charAt(0)) == '@' || charAt == '$')) {
                try {
                    C94314bb.A00(trim, new AnonymousClass5T6[0]);
                    return new C82963wT(C94314bb.A00(obj.toString().toString(), new AnonymousClass5T6[0]), false, false);
                } catch (Exception unused) {
                }
            }
            String trim2 = obj.toString().trim();
            int length = trim2.length();
            if (length > 1) {
                char charAt2 = trim2.charAt(0);
                char charAt3 = trim2.charAt(length - 1);
                if (charAt2 != '[' ? charAt2 == '{' && charAt3 == '}' : charAt3 == ']') {
                    try {
                        new AnonymousClass5LL(new C94764cV(-1).A00).A0A(trim2, AnonymousClass4ZZ.A02.A00);
                        return new C83003wX((CharSequence) obj.toString());
                    } catch (Exception unused2) {
                    }
                }
            }
            return new C82973wU(obj.toString(), true);
        } else if (obj instanceof Character) {
            return new C82973wU(obj.toString(), false);
        } else {
            if (obj instanceof Number) {
                return new C83013wY(obj.toString());
            }
            if (obj instanceof Boolean) {
                if (Boolean.parseBoolean(obj.toString().toString())) {
                    return AbstractC116885Xh.A01;
                }
                return AbstractC116885Xh.A00;
            } else if (obj instanceof Pattern) {
                return new C82993wW((Pattern) obj);
            } else {
                if (obj instanceof OffsetDateTime) {
                    return new C82983wV(obj.toString());
                }
                throw new AnonymousClass5H8("Could not determine value type");
            }
        }
    }

    public C83003wX A03() {
        if (this instanceof C83003wX) {
            return (C83003wX) this;
        }
        throw C82843wH.A00("Expected json node");
    }

    public C83013wY A04() {
        if (this instanceof C82973wU) {
            try {
                return new C83013wY(new BigDecimal(((C82973wU) this).A01));
            } catch (NumberFormatException unused) {
                return C83013wY.A01;
            }
        } else if (this instanceof C83013wY) {
            return (C83013wY) this;
        } else {
            throw C82843wH.A00("Expected number node");
        }
    }

    public C82983wV A05() {
        if (this instanceof C82983wV) {
            return (C82983wV) this;
        }
        throw C82843wH.A00("Expected offsetDateTime node");
    }

    public C82973wU A06() {
        if (this instanceof C82973wU) {
            return (C82973wU) this;
        }
        if (this instanceof C83013wY) {
            return new C82973wU(((C83013wY) this).A00.toString(), false);
        }
        throw C82843wH.A00("Expected string node");
    }

    public C83023wZ A07() {
        if (this instanceof C83023wZ) {
            return (C83023wZ) this;
        }
        throw C82843wH.A00("Expected value list node");
    }
}
