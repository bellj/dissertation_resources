package X;

import android.content.DialogInterface;
import android.os.Looper;
import java.util.concurrent.Executor;

/* renamed from: X.0EP  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0EP extends AnonymousClass015 {
    public int A00 = 0;
    public int A01 = 0;
    public DialogInterface.OnClickListener A02;
    public AnonymousClass0OP A03;
    public AnonymousClass0PW A04;
    public AnonymousClass0U4 A05;
    public C05000Nw A06;
    public AnonymousClass0NI A07;
    public AnonymousClass016 A08;
    public AnonymousClass016 A09;
    public AnonymousClass016 A0A;
    public AnonymousClass016 A0B;
    public AnonymousClass016 A0C;
    public AnonymousClass016 A0D;
    public AnonymousClass016 A0E;
    public AnonymousClass016 A0F;
    public CharSequence A0G;
    public Executor A0H;
    public boolean A0I;
    public boolean A0J;
    public boolean A0K;
    public boolean A0L = true;
    public boolean A0M;
    public boolean A0N;

    public static void A00(AnonymousClass016 r2, Object obj) {
        if (Thread.currentThread() == Looper.getMainLooper().getThread()) {
            r2.A0B(obj);
        } else {
            r2.A0A(obj);
        }
    }

    public void A04(int i) {
        AnonymousClass016 r1 = this.A0C;
        if (r1 == null) {
            r1 = new AnonymousClass016();
            this.A0C = r1;
        }
        A00(r1, Integer.valueOf(i));
    }

    public void A05(CharSequence charSequence) {
        AnonymousClass016 r0 = this.A0B;
        if (r0 == null) {
            r0 = new AnonymousClass016();
            this.A0B = r0;
        }
        A00(r0, charSequence);
    }

    public void A06(boolean z) {
        AnonymousClass016 r1 = this.A0F;
        if (r1 == null) {
            r1 = new AnonymousClass016();
            this.A0F = r1;
        }
        A00(r1, Boolean.valueOf(z));
    }
}
