package X;

import com.whatsapp.payments.ui.NoviPayBloksActivity;

/* renamed from: X.6Ay  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133486Ay implements AnonymousClass6MV {
    public final /* synthetic */ AnonymousClass3FE A00;
    public final /* synthetic */ NoviPayBloksActivity A01;
    public final /* synthetic */ String A02;
    public final /* synthetic */ String A03;
    public final /* synthetic */ String A04;
    public final /* synthetic */ String A05;
    public final /* synthetic */ boolean A06;
    public final /* synthetic */ int[] A07;

    public C133486Ay(AnonymousClass3FE r1, NoviPayBloksActivity noviPayBloksActivity, String str, String str2, String str3, String str4, int[] iArr, boolean z) {
        this.A01 = noviPayBloksActivity;
        this.A02 = str;
        this.A04 = str2;
        this.A07 = iArr;
        this.A05 = str3;
        this.A06 = z;
        this.A03 = str4;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass6MV
    public void APo(C452120p r3) {
        ((AbstractActivityC123635nW) this.A01).A05.A02(r3, null, null);
        C117305Zk.A1E(this.A00);
    }

    @Override // X.AnonymousClass6MV
    public void AVE(AnonymousClass6B7 r13) {
        NoviPayBloksActivity noviPayBloksActivity = this.A01;
        String str = this.A02;
        String str2 = this.A04;
        int[] iArr = this.A07;
        int i = iArr[0];
        int i2 = iArr[1];
        String str3 = this.A05;
        boolean z = this.A06;
        String str4 = this.A03;
        ((AbstractActivityC123635nW) noviPayBloksActivity).A08.A0B(new AnonymousClass6BX(this.A00, r13, noviPayBloksActivity, str, str2, str3, str4, i, i2, z));
    }
}
