package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.facebook.redex.IDxCreatorShape1S0000000_2_I1;

/* renamed from: X.3Lz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C66023Lz implements Parcelable {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape1S0000000_2_I1(65);
    public final int A00;
    public final AnonymousClass1ZI A01;
    public final AnonymousClass1ZI A02;
    public final String A03;
    public final String A04;
    public final String A05;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C66023Lz(AnonymousClass1ZI r1, AnonymousClass1ZI r2, String str, String str2, String str3, int i) {
        this.A05 = str;
        this.A04 = str2;
        this.A03 = str3;
        this.A01 = r1;
        this.A02 = r2;
        this.A00 = i;
    }

    public C66023Lz(Parcel parcel) {
        this.A05 = C12990iw.A0p(parcel);
        this.A04 = parcel.readString();
        this.A03 = C12990iw.A0p(parcel);
        Parcelable A0I = C12990iw.A0I(parcel, AnonymousClass1ZI.class);
        AnonymousClass009.A05(A0I);
        this.A01 = (AnonymousClass1ZI) A0I;
        this.A02 = (AnonymousClass1ZI) C12990iw.A0I(parcel, AnonymousClass1ZI.class);
        this.A00 = parcel.readInt();
    }

    public String A00() {
        String str = this.A04;
        return TextUtils.isEmpty(str) ? this.A05 : str;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A05);
        parcel.writeString(this.A04);
        parcel.writeString(this.A03);
        parcel.writeParcelable(this.A01, i);
        parcel.writeParcelable(this.A02, i);
        parcel.writeInt(this.A00);
    }
}
