package X;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.AdaptiveIconDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.3J2  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3J2 {
    public static final AtomicInteger A00 = new AtomicInteger((int) SystemClock.elapsedRealtime());

    public static PendingIntent A00(Context context, Intent intent) {
        return PendingIntent.getBroadcast(context, A00.incrementAndGet(), C12990iw.A0E("com.google.firebase.MESSAGING_EVENT").setComponent(new ComponentName(context, "com.google.firebase.iid.FirebaseInstanceIdReceiver")).putExtra("wrapped_intent", intent), 1073741824);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(16:2|173|3|(2:5|(10:7|11|(3:13|171|14)|17|(1:19)|20|(36:22|(1:71)|26|(1:28)|29|(3:31|(1:66)(1:35)|(1:37))|38|(1:40)(4:55|(1:57)|58|(2:63|(1:65))(1:62))|41|(2:43|(1:45))|46|(1:50)|51|(4:53|(3:167|83|(1:90))|85|(3:178|87|(0)))|91|(1:93)|94|(2:96|(1:115)(1:99))|100|(2:102|(1:114)(1:105))|106|(2:108|(1:110)(1:113))|111|(3:169|116|(1:118))|123|(3:177|125|(4:127|(1:129)|181|133)(2:130|131))|134|(4:136|180|137|(2:139|(5:141|150|(1:152)|154|155)(2:142|143))(2:144|145))|156|(1:158)|159|(1:161)|162|(1:164)|165|166)|72|(2:175|76)|80))|10|11|(0)|17|(0)|20|(0)|72|(1:74)|175|76|(2:(0)|(0))) */
    /* JADX WARNING: Code restructure failed: missing block: B:153:0x0472, code lost:
        if (r2 == 0) goto L_0x0474;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0213, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0214, code lost:
        r4 = java.lang.String.valueOf(r0);
        r1 = X.C12980iv.A0t(r4.length() + 35);
        r1.append("Couldn't get own application info: ");
        android.util.Log.w("FirebaseMessaging", X.C12960it.A0d(r4, r1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x022e, code lost:
        if (r3 != 0) goto L_0x0230;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0234, code lost:
        if (A03(r7, r3) == false) goto L_0x0236;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0236, code lost:
        r3 = 17301651;
     */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x048a  */
    /* JADX WARNING: Removed duplicated region for block: B:161:0x0494  */
    /* JADX WARNING: Removed duplicated region for block: B:164:0x04a5  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0093  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x0279  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass4MN A01(android.content.Context r12, X.C64903Hj r13) {
        /*
        // Method dump skipped, instructions count: 1214
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3J2.A01(android.content.Context, X.3Hj):X.4MN");
    }

    public static String A02(Context context, Bundle bundle, String str) {
        String str2;
        if (Build.VERSION.SDK_INT >= 26) {
            try {
                if (context.getPackageManager().getApplicationInfo(context.getPackageName(), 0).targetSdkVersion >= 26) {
                    NotificationManager notificationManager = (NotificationManager) context.getSystemService(NotificationManager.class);
                    if (!TextUtils.isEmpty(str)) {
                        if (notificationManager.getNotificationChannel(str) == null) {
                            StringBuilder A0t = C12980iv.A0t(C12970iu.A07(str) + 122);
                            A0t.append("Notification Channel requested (");
                            A0t.append(str);
                            Log.w("FirebaseMessaging", C12960it.A0d(") has not been created by the app. Manifest configuration, or default, value will be used.", A0t));
                        }
                        return str;
                    }
                    str = bundle.getString("com.google.firebase.messaging.default_notification_channel_id");
                    if (!TextUtils.isEmpty(str)) {
                        if (notificationManager.getNotificationChannel(str) == null) {
                            str2 = "Notification Channel set in AndroidManifest.xml has not been created by the app. Default value will be used.";
                        }
                        return str;
                    }
                    str2 = "Missing Default Notification Channel metadata in AndroidManifest. Default value will be used.";
                    Log.w("FirebaseMessaging", str2);
                    str = "fcm_fallback_notification_channel";
                    if (notificationManager.getNotificationChannel(str) == null) {
                        notificationManager.createNotificationChannel(new NotificationChannel(str, context.getString(context.getResources().getIdentifier("fcm_fallback_notification_channel_label", "string", context.getPackageName())), 3));
                    }
                    return str;
                }
            } catch (PackageManager.NameNotFoundException unused) {
                return null;
            }
        }
        return null;
    }

    public static boolean A03(Resources resources, int i) {
        if (Build.VERSION.SDK_INT == 26) {
            try {
                if (resources.getDrawable(i, null) instanceof AdaptiveIconDrawable) {
                    Log.e("FirebaseMessaging", C12960it.A0e("Adaptive icons cannot be used in notifications. Ignoring icon id: ", C12980iv.A0t(77), i));
                    return false;
                }
            } catch (Resources.NotFoundException unused) {
                StringBuilder A0t = C12980iv.A0t(66);
                A0t.append("Couldn't find resource ");
                A0t.append(i);
                Log.e("FirebaseMessaging", C12960it.A0d(", treating it as an invalid icon", A0t));
                return false;
            }
        }
        return true;
    }
}
