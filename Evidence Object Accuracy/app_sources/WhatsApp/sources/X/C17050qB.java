package X;

import android.os.Build;
import android.os.Environment;
import java.io.File;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/* renamed from: X.0qB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17050qB {
    public final C15810nw A00;
    public final C15890o4 A01;
    public final C14950mJ A02;
    public final C21780xy A03;
    public final AbstractC14440lR A04;
    public final AnonymousClass01H A05;
    public final List A06 = new CopyOnWriteArrayList();

    public C17050qB(C15810nw r4, C15890o4 r5, C14950mJ r6, C21780xy r7, AbstractC14440lR r8) {
        this.A04 = r8;
        this.A00 = r4;
        this.A02 = r6;
        this.A01 = r5;
        this.A03 = r7;
        this.A05 = new C002601e(null, new AnonymousClass01N(r5, r6) { // from class: X.1cp
            public final /* synthetic */ C15890o4 A01;
            public final /* synthetic */ C14950mJ A02;

            {
                this.A02 = r3;
                this.A01 = r2;
            }

            @Override // X.AnonymousClass01N, X.AnonymousClass01H
            public final Object get() {
                return new C32861cr(C15810nw.this, this.A01, this.A02);
            }
        });
    }

    public C32871cs A00(File file) {
        return new C32871cs(((C32861cr) this.A05.get()).A02, file);
    }

    public File A01(File file) {
        C27421Hj r1;
        if (this.A00.A0B(file)) {
            r1 = ((C32861cr) this.A05.get()).A02;
        } else {
            r1 = this.A03.A00;
        }
        return r1.A00("");
    }

    public boolean A02() {
        AnonymousClass01H r1 = this.A05;
        return ((C32861cr) r1.get()).A00 || ((C32861cr) r1.get()).A01;
    }

    public boolean A03(AbstractC32851cq r4) {
        String externalStorageState = Environment.getExternalStorageState();
        if (!"mounted".equals(externalStorageState) && !"mounted_ro".equals(externalStorageState)) {
            r4.AXw(externalStorageState);
            return false;
        } else if (Build.VERSION.SDK_INT < 23 || this.A01.A02("android.permission.READ_EXTERNAL_STORAGE") != -1) {
            return true;
        } else {
            r4.AXx();
            return false;
        }
    }

    public boolean A04(AbstractC32851cq r4) {
        String externalStorageState = Environment.getExternalStorageState();
        if ("mounted_ro".equals(externalStorageState)) {
            r4.AUa(externalStorageState);
            return false;
        } else if (!"mounted".equals(externalStorageState)) {
            r4.AXw(externalStorageState);
            return false;
        } else if (this.A01.A02("android.permission.WRITE_EXTERNAL_STORAGE") != -1) {
            return true;
        } else {
            r4.AUb();
            return false;
        }
    }
}
