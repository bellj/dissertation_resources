package X;

import android.graphics.PointF;
import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3oL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78083oL extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C99154jq();
    public final int A00;
    public final PointF[] A01;

    public C78083oL(PointF[] pointFArr, int i) {
        this.A01 = pointFArr;
        this.A00 = i;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A0I(parcel, this.A01, 2, i);
        C95654e8.A07(parcel, 3, this.A00);
        C95654e8.A06(parcel, A00);
    }
}
