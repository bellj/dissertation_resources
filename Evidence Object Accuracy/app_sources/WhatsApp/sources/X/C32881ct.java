package X;

import java.util.Random;

/* renamed from: X.1ct  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C32881ct {
    public int A00 = 0;
    public boolean A01 = false;
    public final int A02;
    public final long A03;
    public final Random A04;

    public C32881ct(Random random) {
        this.A04 = random;
        this.A02 = 20;
        this.A03 = 3600000;
    }

    public C32881ct(Random random, int i, long j) {
        this.A04 = random;
        this.A02 = i;
        this.A03 = j;
    }

    public synchronized Long A00() {
        Long l;
        int i = this.A00;
        if (i >= this.A02) {
            l = null;
        } else {
            int i2 = i + 1;
            this.A00 = i2;
            if (!this.A01) {
                long min = (1 << Math.min(i2, 50)) * 1000;
                long abs = (min / 2) + Math.abs(this.A04.nextLong() % min);
                long j = this.A03;
                if (abs >= j) {
                    this.A01 = true;
                    l = Long.valueOf(j);
                } else {
                    l = Long.valueOf(abs);
                }
            } else {
                l = Long.valueOf(this.A03);
            }
        }
        return l;
    }

    public synchronized void A01() {
        this.A00 = 0;
    }
}
