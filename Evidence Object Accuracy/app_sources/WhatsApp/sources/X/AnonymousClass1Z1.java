package X;

import android.icu.text.DecimalFormat;
import android.icu.text.DecimalFormatSymbols;
import android.os.Build;
import java.math.BigDecimal;
import java.util.Locale;

/* renamed from: X.1Z1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1Z1 {
    public static final boolean A02;
    public final DecimalFormat A00;
    public final AnonymousClass3D3 A01;

    static {
        boolean z = false;
        if (Build.VERSION.SDK_INT >= 24) {
            z = true;
        }
        A02 = z;
    }

    public AnonymousClass1Z1(String str, Locale locale) {
        if (A02) {
            this.A00 = new DecimalFormat(str, DecimalFormatSymbols.getInstance(locale));
        } else {
            this.A01 = new AnonymousClass3D3(str, locale);
        }
    }

    public Number A00(String str) {
        if (A02) {
            DecimalFormat decimalFormat = this.A00;
            AnonymousClass009.A05(decimalFormat);
            return decimalFormat.parse(str);
        }
        AnonymousClass3D3 r0 = this.A01;
        AnonymousClass009.A05(r0);
        return r0.A04.parse(str.replace(String.valueOf(r0.A01), ""));
    }

    public String A01(double d) {
        if (A02) {
            DecimalFormat decimalFormat = this.A00;
            AnonymousClass009.A05(decimalFormat);
            return decimalFormat.format(d);
        }
        AnonymousClass3D3 r1 = this.A01;
        AnonymousClass009.A05(r1);
        return r1.A00(r1.A04.format(d));
    }

    public String A02(BigDecimal bigDecimal) {
        if (A02) {
            DecimalFormat decimalFormat = this.A00;
            AnonymousClass009.A05(decimalFormat);
            return decimalFormat.format(bigDecimal);
        }
        AnonymousClass3D3 r1 = this.A01;
        AnonymousClass009.A05(r1);
        return r1.A00(r1.A04.format(bigDecimal));
    }

    public void A03(int i) {
        if (A02) {
            DecimalFormat decimalFormat = this.A00;
            AnonymousClass009.A05(decimalFormat);
            decimalFormat.setMinimumFractionDigits(i);
            decimalFormat.setMaximumFractionDigits(i);
            return;
        }
        AnonymousClass3D3 r0 = this.A01;
        AnonymousClass009.A05(r0);
        java.text.DecimalFormat decimalFormat2 = r0.A04;
        decimalFormat2.setMinimumFractionDigits(i);
        decimalFormat2.setMaximumFractionDigits(i);
    }
}
