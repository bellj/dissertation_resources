package X;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;

/* renamed from: X.0a2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07760a2 implements AbstractC11500gO {
    public final Handler A00 = new Handler(Looper.getMainLooper());
    public final ExecutorC10610eu A01;
    public final Executor A02 = new ExecutorC10590es(this);

    public C07760a2(Executor executor) {
        this.A01 = new ExecutorC10610eu(executor);
    }
}
