package X;

import android.app.NotificationChannel;
import android.net.Uri;
import android.provider.Settings;
import android.text.TextUtils;
import com.whatsapp.util.Log;

/* renamed from: X.1dj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C33271dj extends C33181da {
    public final C33161dY A00;

    public C33271dj(C15450nH r1, AnonymousClass01d r2, C14830m7 r3, C15890o4 r4, C15860o1 r5, C33161dY r6) {
        super(r1, r2, r3, r4, r5);
        this.A00 = r6;
    }

    @Override // X.C33181da
    public long A00() {
        NotificationChannel A03;
        if (!this.A0I || (A03 = this.A00.A03(this.A0C)) == null || A03.getImportance() >= 3) {
            return A01();
        }
        StringBuilder sb = new StringBuilder("chat-settings-store/getMuteEndTime notification channel muted for:");
        sb.append(C15380n4.A04(this.A0C));
        Log.i(sb.toString());
        return -1;
    }

    @Override // X.C33181da
    public boolean A0A() {
        NotificationChannel A03;
        if (!this.A0I || (A03 = this.A00.A03(this.A0C)) == null || A03.getImportance() != 0) {
            return super.A0A();
        }
        StringBuilder sb = new StringBuilder("chat-settings-store/getShowNotifications notification channel disabled for:");
        sb.append(C15380n4.A04(this.A0C));
        Log.i(sb.toString());
        return false;
    }

    public String A0C() {
        String str;
        Uri parse;
        C33291dl r1 = C33161dY.A0M;
        String A00 = r1.A00(this.A0C);
        if (TextUtils.isEmpty(A00)) {
            if (this.A0I) {
                StringBuilder sb = new StringBuilder("chat-settings-store/getNotificationChannelId missing channel for chat with custom notifications:");
                sb.append(C15380n4.A04(this.A0C));
                Log.i(sb.toString());
                C33161dY r4 = this.A00;
                String str2 = this.A0C;
                CharSequence A06 = r4.A06(str2);
                int i = 4;
                if (A0B()) {
                    i = 3;
                }
                String str3 = this.A08;
                String str4 = this.A0B;
                String str5 = this.A0A;
                if (str5 == null) {
                    parse = Uri.EMPTY;
                } else {
                    parse = Uri.parse(str5);
                }
                if (!TextUtils.isEmpty(str5) && !C14350lI.A0I(parse, r4.A09, r4.A0C, true)) {
                    parse = Settings.System.DEFAULT_NOTIFICATION_URI;
                }
                r4.A07(parse, A06, str2, str3, str4, "channel_group_chats", i);
                return r1.A00(this.A0C);
            }
            if (C15380n4.A0J(AbstractC14640lm.A01(this.A0C))) {
                str = "group_chat_defaults";
            } else {
                str = "individual_chat_defaults";
            }
            A00 = r1.A00(str);
            if (!this.A0J.A05(AbstractC15460nI.A0n)) {
                C33161dY r12 = this.A00;
                int i2 = 4;
                if (A0B()) {
                    i2 = 3;
                }
                return r12.A0A(A00, A05(), A08(), A07(), i2);
            }
        }
        return A00;
    }

    public String A0D() {
        return this.A00.A08(C33161dY.A0M.A00("silent_notifications"));
    }

    public String A0E() {
        String A00 = C33161dY.A0M.A00("voip_notification");
        C33161dY r1 = this.A00;
        if (A00 == null) {
            return r1.A07(null, r1.A06("voip_notification"), "voip_notification", null, null, null, 4);
        }
        return r1.A09(A00);
    }
}
