package X;

/* renamed from: X.1rs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C40611rs extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Boolean A02;
    public Boolean A03;
    public Boolean A04;
    public Boolean A05;
    public Boolean A06;
    public Boolean A07;
    public Boolean A08;
    public Boolean A09;
    public Boolean A0A;
    public Integer A0B;
    public Integer A0C;
    public Integer A0D;
    public Integer A0E;
    public Integer A0F;
    public Integer A0G;
    public Integer A0H;
    public Long A0I;
    public Long A0J;
    public Long A0K;
    public Long A0L;
    public Long A0M;
    public Long A0N;
    public Long A0O;
    public Long A0P;
    public Long A0Q;
    public Long A0R;
    public Long A0S;
    public Long A0T;
    public Long A0U;
    public Long A0V;
    public Long A0W;
    public Long A0X;
    public Long A0Y;
    public Long A0Z;
    public Long A0a;
    public Long A0b;
    public Long A0c;
    public Long A0d;
    public Long A0e;
    public Long A0f;
    public Long A0g;
    public Long A0h;
    public Long A0i;
    public Long A0j;
    public Long A0k;
    public Long A0l;
    public Long A0m;
    public Long A0n;
    public Long A0o;
    public Long A0p;
    public Long A0q;
    public Long A0r;
    public Long A0s;
    public Long A0t;
    public Long A0u;
    public Long A0v;
    public Long A0w;
    public Long A0x;
    public Long A0y;
    public Long A0z;
    public Long A10;
    public Long A11;
    public Long A12;
    public Long A13;
    public Long A14;
    public Long A15;
    public Long A16;
    public String A17;
    public String A18;
    public String A19;
    public String A1A;

    public C40611rs() {
        super(1644, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(56, this.A0I);
        r3.Abe(60, this.A0B);
        r3.Abe(65, this.A0J);
        r3.Abe(33, this.A0C);
        r3.Abe(30, this.A0K);
        r3.Abe(29, this.A0L);
        r3.Abe(27, this.A0M);
        r3.Abe(26, this.A0N);
        r3.Abe(70, this.A0O);
        r3.Abe(71, this.A0P);
        r3.Abe(72, this.A0Q);
        r3.Abe(78, this.A0R);
        r3.Abe(73, this.A0S);
        r3.Abe(74, this.A0T);
        r3.Abe(86, this.A0U);
        r3.Abe(15, this.A0V);
        r3.Abe(8, this.A0D);
        r3.Abe(79, this.A0W);
        r3.Abe(2, this.A0E);
        r3.Abe(44, this.A0X);
        r3.Abe(41, this.A0Y);
        r3.Abe(40, this.A0Z);
        r3.Abe(59, this.A0F);
        r3.Abe(47, this.A17);
        r3.Abe(46, this.A18);
        r3.Abe(14, this.A0a);
        r3.Abe(13, this.A0b);
        r3.Abe(69, this.A0c);
        r3.Abe(25, this.A0d);
        r3.Abe(22, this.A0G);
        r3.Abe(57, this.A0e);
        r3.Abe(75, this.A00);
        r3.Abe(51, this.A0f);
        r3.Abe(52, this.A0g);
        r3.Abe(19, this.A0h);
        r3.Abe(6, this.A01);
        r3.Abe(5, this.A02);
        r3.Abe(10, this.A03);
        r3.Abe(32, this.A04);
        r3.Abe(36, this.A05);
        r3.Abe(35, this.A06);
        r3.Abe(37, this.A07);
        r3.Abe(62, this.A08);
        r3.Abe(9, this.A09);
        r3.Abe(55, this.A0i);
        r3.Abe(4, this.A0j);
        r3.Abe(3, this.A0k);
        r3.Abe(12, this.A0l);
        r3.Abe(11, this.A0m);
        r3.Abe(68, this.A0A);
        r3.Abe(38, this.A0n);
        r3.Abe(39, this.A0o);
        r3.Abe(42, this.A0p);
        r3.Abe(61, this.A0q);
        r3.Abe(64, this.A0r);
        r3.Abe(63, this.A0s);
        r3.Abe(58, this.A0t);
        r3.Abe(21, this.A0u);
        r3.Abe(80, this.A0v);
        r3.Abe(20, this.A0w);
        r3.Abe(31, this.A0x);
        r3.Abe(7, this.A0y);
        r3.Abe(50, this.A0z);
        r3.Abe(49, this.A10);
        r3.Abe(66, this.A19);
        r3.Abe(67, this.A1A);
        r3.Abe(28, this.A11);
        r3.Abe(85, this.A0H);
        r3.Abe(76, this.A12);
        r3.Abe(18, this.A13);
        r3.Abe(17, this.A14);
        r3.Abe(16, this.A15);
        r3.Abe(77, this.A16);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        String obj3;
        String obj4;
        String obj5;
        String obj6;
        String obj7;
        StringBuilder sb = new StringBuilder("WamChatMessageCounts {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "awayMsgsSent", this.A0I);
        Integer num = this.A0B;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "bizCatalogType", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "bizConversationDepth", this.A0J);
        Integer num2 = this.A0C;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "blockReason", obj2);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "broadcastMsgsReceived", this.A0K);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "broadcastMsgsSent", this.A0L);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "callOffersReceived", this.A0M);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "callOffersSent", this.A0N);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "callsResultBusy", this.A0O);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "callsResultCancelled", this.A0P);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "callsResultConnected", this.A0Q);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "callsResultError", this.A0R);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "callsResultMissed", this.A0S);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "callsResultRejected", this.A0T);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "cartViews", this.A0U);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "chatEphemeralityDuration", this.A0V);
        Integer num3 = this.A0D;
        if (num3 == null) {
            obj3 = null;
        } else {
            obj3 = num3.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "chatMuted", obj3);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "chatOverflowClicks", this.A0W);
        Integer num4 = this.A0E;
        if (num4 == null) {
            obj4 = null;
        } else {
            obj4 = num4.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "chatTypeInd", obj4);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "collectionInquiriesSent", this.A0X);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "commerceMsgsReceived", this.A0Y);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "commerceMsgsSent", this.A0Z);
        Integer num5 = this.A0F;
        if (num5 == null) {
            obj5 = null;
        } else {
            obj5 = num5.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "disappearingChatInitiator", obj5);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "entryPointConversionApp", this.A17);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "entryPointConversionSource", this.A18);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "ephemeralMessagesReceived", this.A0a);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "ephemeralMessagesSent", this.A0b);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "ephemeralMessagesUnreadExpired", this.A0c);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "firstResponseTime", this.A0d);
        Integer num6 = this.A0G;
        if (num6 == null) {
            obj6 = null;
        } else {
            obj6 = num6.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "gaStatus", obj6);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "greetingMsgsSent", this.A0e);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "groupContainsBiz", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "groupMembershipReplies", this.A0f);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "groupPrivateReplies", this.A0g);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "groupSize", this.A0h);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "isAContact", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "isAGroup", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "isArchived", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "isBlocked", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "isCartAddClicked", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "isCommerceViewed", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "isCtaOnPdpClicked", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "isOppositePartyInitiated", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "isPinned", this.A09);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "labelledMsgs", this.A0i);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "messagesReceived", this.A0j);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "messagesSent", this.A0k);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "messagesStarred", this.A0l);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "messagesUnread", this.A0m);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "newThread", this.A0A);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "ordersSent", this.A0n);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "paymentsSent", this.A0o);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "pdpInquiriesSent", this.A0p);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "pdpViews", this.A0q);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "profileReplies", this.A0r);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "profileViews", this.A0s);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "quickRepliesSent", this.A0t);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "receiverDefaultDisappearingDuration", this.A0u);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "repliesSent", this.A0v);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "senderDefaultDisappearingDuration", this.A0w);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "spamReports", this.A0x);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "startTime", this.A0y);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "statusReplies", this.A0z);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "statusViews", this.A10);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "threadDs", this.A19);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "threadId", this.A1A);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "totalCallDuration", this.A11);
        Integer num7 = this.A0H;
        if (num7 == null) {
            obj7 = null;
        } else {
            obj7 = num7.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "typeOfGroup", obj7);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "videoCallsOffered", this.A12);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "viewOnceMessagesOpened", this.A13);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "viewOnceMessagesReceived", this.A14);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "viewOnceMessagesSent", this.A15);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "voiceCallsOffered", this.A16);
        sb.append("}");
        return sb.toString();
    }
}
