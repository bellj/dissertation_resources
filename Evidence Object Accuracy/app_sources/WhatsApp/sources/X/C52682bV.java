package X;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.components.SelectionCheckView;
import com.whatsapp.contact.picker.PhoneContactsSelector;
import java.util.List;

/* renamed from: X.2bV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52682bV extends ArrayAdapter {
    public final /* synthetic */ PhoneContactsSelector A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C52682bV(Context context, PhoneContactsSelector phoneContactsSelector, List list) {
        super(context, (int) R.layout.phone_contact_row, list);
        this.A00 = phoneContactsSelector;
    }

    @Override // android.widget.ArrayAdapter, android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        AnonymousClass3BS r0;
        Object item = getItem(i);
        AnonymousClass009.A05(item);
        C51492Vb r6 = (C51492Vb) item;
        if (view == null) {
            view = this.A00.getLayoutInflater().inflate(R.layout.phone_contact_row, viewGroup, false);
            r0 = new AnonymousClass3BS(view);
            view.setTag(r0);
        } else {
            r0 = (AnonymousClass3BS) view.getTag();
        }
        PhoneContactsSelector phoneContactsSelector = this.A00;
        AnonymousClass130 r3 = phoneContactsSelector.A0C;
        ImageView imageView = r0.A01;
        r3.A05(imageView, R.drawable.avatar_contact);
        phoneContactsSelector.A0F.A05(imageView, r6);
        r0.A02.A0G(phoneContactsSelector.A0V, r6.A06);
        SelectionCheckView selectionCheckView = r0.A04;
        selectionCheckView.A04(r6.A03, false);
        selectionCheckView.setTag(r6);
        return view;
    }
}
