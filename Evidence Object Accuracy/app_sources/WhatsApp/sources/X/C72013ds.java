package X;

import android.os.Bundle;
import com.whatsapp.gallery.GalleryRecentsFragment;

/* renamed from: X.3ds  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C72013ds extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ C50982Sg this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C72013ds(C50982Sg r2) {
        super(0);
        this.this$0 = r2;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        GalleryRecentsFragment galleryRecentsFragment = new GalleryRecentsFragment();
        C50982Sg r3 = this.this$0;
        Bundle A0D = C12970iu.A0D();
        A0D.putInt("include", r3.A00);
        galleryRecentsFragment.A0U(A0D);
        galleryRecentsFragment.A01 = r3.A03;
        galleryRecentsFragment.A00 = r3.A02;
        return galleryRecentsFragment;
    }
}
