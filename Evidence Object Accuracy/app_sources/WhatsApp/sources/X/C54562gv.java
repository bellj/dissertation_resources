package X;

import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;

/* renamed from: X.2gv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54562gv extends AbstractC018308n {
    @Override // X.AbstractC018308n
    public void A01(Rect rect, View view, C05480Ps r7, RecyclerView recyclerView) {
        super.A01(rect, view, r7, recyclerView);
        int A00 = RecyclerView.A00(view);
        if (recyclerView.A0N != null && A00 == 0) {
            AnonymousClass028.A0e(view, AnonymousClass028.A07(view), (int) view.getResources().getDimension(R.dimen.product_list_section_top_padding), AnonymousClass028.A06(view), view.getPaddingBottom());
        }
    }
}
