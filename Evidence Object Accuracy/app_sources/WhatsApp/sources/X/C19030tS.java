package X;

import android.content.ContentValues;
import android.database.Cursor;
import com.whatsapp.util.Log;
import java.util.ArrayList;

/* renamed from: X.0tS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19030tS extends AbstractC18500sY implements AbstractC19010tQ {
    public final C20050v8 A00;

    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AM5() {
    }

    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AND() {
    }

    public C19030tS(C20050v8 r3, C18480sW r4) {
        super(r4, "message_link", 2);
        this.A00 = r3;
    }

    @Override // X.AbstractC18500sY
    public AnonymousClass2Ez A09(Cursor cursor) {
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow("_id");
        int columnIndexOrThrow2 = cursor.getColumnIndexOrThrow("chat_row_id");
        int columnIndexOrThrow3 = cursor.getColumnIndexOrThrow("data");
        int columnIndexOrThrow4 = cursor.getColumnIndexOrThrow("media_caption");
        int columnIndexOrThrow5 = cursor.getColumnIndexOrThrow("message_type");
        C16310on A02 = this.A05.A02();
        long j = -1;
        int i = 0;
        while (cursor.moveToNext()) {
            try {
                int i2 = cursor.getInt(columnIndexOrThrow5);
                String str = null;
                if (i2 == 0) {
                    str = cursor.getString(columnIndexOrThrow3);
                } else if (i2 == 1 || i2 == 3 || i2 == 13 || i2 == 23 || i2 == 25 || i2 == 37 || i2 == 28 || i2 == 29) {
                    str = cursor.getString(columnIndexOrThrow4);
                }
                j = cursor.getLong(columnIndexOrThrow);
                ArrayList A04 = C33771f3.A04(str);
                long j2 = cursor.getLong(columnIndexOrThrow2);
                if (j2 >= 0) {
                    if (A04 != null) {
                        for (int i3 = 0; i3 < A04.size(); i3++) {
                            ContentValues contentValues = new ContentValues();
                            contentValues.put("chat_row_id", Long.valueOf(j2));
                            contentValues.put("message_row_id", Long.valueOf(j));
                            contentValues.put("link_index", Integer.valueOf(i3));
                            A02.A03.A06(contentValues, "message_link", 4);
                        }
                    }
                    i++;
                }
            } catch (Throwable th) {
                try {
                    A02.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
        A02.close();
        return new AnonymousClass2Ez(j, i);
    }

    @Override // X.AbstractC19010tQ
    public void onRollback() {
        C16310on A02 = this.A05.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            A02.A03.A01("message_link", null, null);
            C21390xL r1 = this.A06;
            r1.A03("links_ready");
            r1.A03("migration_link_index");
            r1.A03("migration_link_retry");
            A00.A00();
            A00.close();
            A02.close();
            Log.i("LinkMessageStore/LinkMessageDatabaseMigration/resetMigration/done");
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
