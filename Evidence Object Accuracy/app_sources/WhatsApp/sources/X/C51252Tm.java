package X;

import java.io.FilterOutputStream;
import java.io.OutputStream;

/* renamed from: X.2Tm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C51252Tm extends FilterOutputStream {
    public C51252Tm(OutputStream outputStream) {
        super(outputStream);
    }

    @Override // java.io.FilterOutputStream, java.io.OutputStream
    public void write(int i) {
        write(new byte[]{(byte) i});
    }

    @Override // java.io.FilterOutputStream, java.io.OutputStream
    public void write(byte[] bArr) {
        write(bArr, 0, bArr.length);
    }

    @Override // java.io.FilterOutputStream, java.io.OutputStream
    public void write(byte[] bArr, int i, int i2) {
        if (i2 < 16777216) {
            ((FilterOutputStream) this).out.write(new byte[]{(byte) (i2 >> 16), (byte) (i2 >> 8), (byte) i2});
            ((FilterOutputStream) this).out.write(bArr, i, i2);
            ((FilterOutputStream) this).out.flush();
            return;
        }
        StringBuilder sb = new StringBuilder("data too large to write; length=");
        sb.append(i2);
        throw new C47752Ch(sb.toString());
    }
}
