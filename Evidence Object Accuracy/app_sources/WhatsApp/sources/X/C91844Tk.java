package X;

import android.media.MediaCodec;

/* renamed from: X.4Tk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C91844Tk {
    public int A00;
    public int A01;
    public byte[] A02;
    public byte[] A03;
    public int[] A04;
    public int[] A05;
    public final MediaCodec.CryptoInfo A06;
    public final C93314Zy A07;

    public C91844Tk() {
        MediaCodec.CryptoInfo cryptoInfo = new MediaCodec.CryptoInfo();
        this.A06 = cryptoInfo;
        this.A07 = AnonymousClass3JZ.A01 >= 24 ? new C93314Zy(cryptoInfo) : null;
    }
}
