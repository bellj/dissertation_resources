package X;

import android.content.res.Resources;
import com.whatsapp.WaImageView;

/* renamed from: X.2yy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60912yy extends AnonymousClass4UX {
    public WaImageView A00;
    public final Resources A01;
    public final AnonymousClass018 A02;
    public final AbstractC41521tf A03 = new C70283b2(this);
    public final AnonymousClass19O A04;

    public C60912yy(C16590pI r2, AnonymousClass018 r3, AnonymousClass19O r4) {
        this.A01 = C16590pI.A00(r2);
        this.A02 = r3;
        this.A04 = r4;
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0039  */
    @Override // X.AnonymousClass4UX
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(android.widget.FrameLayout r14, X.AnonymousClass1OY r15, X.AbstractC15340mz r16, X.C16470p4 r17) {
        /*
            r13 = this;
            r14.removeAllViews()
            android.content.Context r0 = r14.getContext()
            X.2qp r1 = new X.2qp
            r1.<init>(r0)
            r14.addView(r1)
            r3 = r17
            X.1Z6 r0 = r3.A02
            r11 = 0
            r2 = 8
            r9 = r16
            if (r0 == 0) goto L_0x0077
            java.lang.String r10 = r0.A01
            boolean r0 = android.text.TextUtils.isEmpty(r10)
            if (r0 != 0) goto L_0x0077
            com.whatsapp.TextEmojiLabel r8 = r1.A01
            r8.setVisibility(r11)
            X.AnonymousClass009.A05(r10)
            r12 = 0
            r7 = r15
            r7.A17(r8, r9, r10, r11, r12)
        L_0x002f:
            com.whatsapp.WaImageView r5 = r1.A02
            r13.A00 = r5
            X.1ZC r7 = r3.A04
            com.whatsapp.TextEmojiLabel r8 = r1.A00
            if (r7 == 0) goto L_0x0073
            r8.setVisibility(r11)
            int r0 = r7.A00()
            long r0 = (long) r0
            X.018 r6 = r13.A02
            r4 = 2131755247(0x7f1000ef, float:1.9141368E38)
            java.lang.Object[] r3 = X.C12970iu.A1b()
            X.C12980iv.A1U(r3, r11, r0)
            java.lang.String r0 = r6.A0I(r3, r4, r0)
            r8.setText(r0)
        L_0x0054:
            X.0p3 r0 = r9.A0G()
            if (r0 == 0) goto L_0x0081
            boolean r0 = r0.A05()
            if (r0 == 0) goto L_0x0081
            if (r7 == 0) goto L_0x0081
            X.19O r1 = r13.A04
            X.1tf r0 = r13.A03
            r1.A07(r5, r9, r0)
            X.1ZH r0 = r7.A01
            boolean r0 = r0.A00
            if (r0 != 0) goto L_0x007d
            r5.setVisibility(r11)
            return
        L_0x0073:
            r8.setVisibility(r2)
            goto L_0x0054
        L_0x0077:
            com.whatsapp.TextEmojiLabel r0 = r1.A01
            r0.setVisibility(r2)
            goto L_0x002f
        L_0x007d:
            r5.setVisibility(r2)
            return
        L_0x0081:
            com.whatsapp.WaImageView r0 = r13.A00
            r0.setVisibility(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C60912yy.A00(android.widget.FrameLayout, X.1OY, X.0mz, X.0p4):void");
    }
}
