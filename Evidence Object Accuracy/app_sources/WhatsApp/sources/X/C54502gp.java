package X;

import android.os.Handler;
import android.os.Looper;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import java.util.List;

/* renamed from: X.2gp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54502gp extends AnonymousClass02M {
    public int A00;
    public int A01;
    public RecyclerView A02;
    public AnonymousClass28D A03;
    public List A04;
    public final Handler A05;
    public final C14260l7 A06;
    public final C89044Il A07;

    public C54502gp() {
        this.A04 = C12960it.A0l();
        this.A07 = null;
        A07(true);
    }

    public C54502gp(C14260l7 r4, AnonymousClass28D r5, int i) {
        C89044Il r2 = new C89044Il(i);
        r4.A01.get(R.id.bk_context_key_rendercore_extensions_creator);
        this.A04 = C12960it.A0l();
        this.A07 = r2;
        A07(true);
        this.A06 = r4;
        this.A03 = r5;
        this.A05 = new AnonymousClass2a1(Looper.getMainLooper(), this);
    }

    @Override // X.AnonymousClass02M
    public long A00(int i) {
        return (long) ((C89954Ma) this.A04.get(i)).A01.A00;
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void A08(AnonymousClass03U r4) {
        AbstractC14200l1 A0G;
        AnonymousClass28D A05 = AnonymousClass28D.A05(((C89954Ma) ((C75383jl) r4).A00).A01);
        if (A05 != null && A05.A01 == 13366 && (A0G = A05.A0G(38)) != null) {
            C65093Ic.A00();
            Handler handler = this.A05;
            handler.sendMessage(handler.obtainMessage(0, A0G));
        }
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void A09(AnonymousClass03U r4) {
        AbstractC14200l1 A0G;
        AnonymousClass28D A05 = AnonymousClass28D.A05(((C89954Ma) ((C75383jl) r4).A00).A01);
        if (A05 != null && A05.A01 == 13366 && (A0G = A05.A0G(40)) != null) {
            C65093Ic.A00();
            Handler handler = this.A05;
            handler.sendMessage(handler.obtainMessage(0, A0G));
        }
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void A0A(AnonymousClass03U r3) {
        C75383jl r32 = (C75383jl) r3;
        ((AnonymousClass5SE) r32.A0H).setRenderTree(null);
        r32.A00 = null;
    }

    @Override // X.AnonymousClass02M
    public void A0B(RecyclerView recyclerView) {
        this.A02 = recyclerView;
    }

    @Override // X.AnonymousClass02M
    public void A0C(RecyclerView recyclerView) {
        this.A02 = null;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A04.size();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00e5, code lost:
        if (r4 != null) goto L_0x00e7;
     */
    @Override // X.AnonymousClass02M
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ void ANH(X.AnonymousClass03U r13, int r14) {
        /*
        // Method dump skipped, instructions count: 250
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C54502gp.ANH(X.03U, int):void");
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        return new C75383jl(new C55982k5(viewGroup.getContext()));
    }
}
