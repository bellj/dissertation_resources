package X;

import android.view.View;
import com.facebook.redex.EmptyBaseRunnable0;

/* renamed from: X.3lD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class RunnableC76183lD extends EmptyBaseRunnable0 implements Runnable {
    public long A00;
    public boolean A01;
    public final View A02;
    public final AnonymousClass3MP A03;

    public RunnableC76183lD(View view, AnonymousClass3MP r2) {
        this.A02 = view;
        this.A03 = r2;
    }

    @Override // java.lang.Runnable
    public void run() {
        if (!this.A01) {
            long j = this.A00;
            if (j == 0) {
                j = System.currentTimeMillis();
                this.A00 = j;
            }
            if (((float) (System.currentTimeMillis() - j)) / ((float) 0) >= 1.0f) {
                this.A01 = true;
            }
            View view = this.A02;
            view.invalidate();
            if (!this.A01) {
                view.post(this);
            }
        }
    }
}
