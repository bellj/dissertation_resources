package X;

import java.util.ArrayList;

/* renamed from: X.3Hu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C65013Hu {
    public static double A00(long j, long j2, boolean z) {
        if (z) {
            j++;
        }
        double exp = Math.exp((((((double) j) * 1.0d) / ((double) j2)) - 0.5d) * 4.0d * 3.141592653589793d);
        return -((Math.asin((exp - 1.0d) / (exp + 1.0d)) * 180.0d) / 3.141592653589793d);
    }

    public static ArrayList A01(double d, double d2, int i) {
        if (i <= 0 || d < -85.05112878d || d > 85.05112878d || d2 < -180.0d || d2 > 180.0d) {
            return null;
        }
        ArrayList A0l = C12960it.A0l();
        long j = (long) (2 << (i - 1));
        double d3 = (double) j;
        A0l.add(Long.valueOf((long) Math.min(Math.max(((d2 + 180.0d) / 360.0d) * d3, 0.0d), (double) (j - 1))));
        double sin = Math.sin((Math.min(Math.max(d, -85.05112878d), 85.05112878d) * 3.141592653589793d) / 180.0d);
        A0l.add(Long.valueOf((long) Math.min(Math.max((0.5d - (Math.log((sin + 1.0d) / (1.0d - sin)) / 12.566370614359172d)) * d3, 0.0d), d3 - 1.0d)));
        return A0l;
    }

    public static ArrayList A02(int i, long j, long j2) {
        long j3 = (long) (2 << (i - 1));
        ArrayList A0l = C12960it.A0l();
        A0l.add(Double.valueOf((A00(j2, j3, true) + A00(j2, j3, false)) / 2.0d));
        A0l.add(Double.valueOf(((360.0d / ((double) j3)) * (((double) j) + 0.5d)) - 180.0d));
        return A0l;
    }
}
