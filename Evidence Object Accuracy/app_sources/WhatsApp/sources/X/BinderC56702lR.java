package X;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import java.util.Set;

/* renamed from: X.2lR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class BinderC56702lR extends AbstractBinderC80563sT implements AbstractC14980mM, AbstractC15000mO {
    public static final AbstractC77683ng A07 = C88804He.A00;
    public AnonymousClass5SZ A00;
    public AnonymousClass5Yh A01;
    public final Context A02;
    public final Handler A03;
    public final AbstractC77683ng A04;
    public final AnonymousClass3BW A05;
    public final Set A06;

    public BinderC56702lR(Context context, Handler handler, AnonymousClass3BW r5) {
        AbstractC77683ng r1 = A07;
        this.A02 = context;
        this.A03 = handler;
        this.A05 = r5;
        this.A06 = r5.A05;
        this.A04 = r1;
    }

    @Override // X.AbstractBinderC80563sT, X.AnonymousClass5YR
    public final void AgR(C78323oj r3) {
        C12980iv.A18(this.A03, r3, this, 13);
    }

    @Override // X.AbstractC14990mN
    public final void onConnected(Bundle bundle) {
        this.A01.AgU(this);
    }

    @Override // X.AbstractC15010mP
    public final void onConnectionFailed(C56492ky r2) {
        this.A00.AgX(r2);
    }

    @Override // X.AbstractC14990mN
    public final void onConnectionSuspended(int i) {
        this.A01.A8y();
    }
}
