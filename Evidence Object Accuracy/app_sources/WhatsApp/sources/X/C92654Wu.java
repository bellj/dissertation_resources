package X;

/* renamed from: X.4Wu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92654Wu {
    public final AbstractC15340mz A00;
    public final Boolean A01;

    public C92654Wu(AbstractC15340mz r1, Boolean bool) {
        this.A01 = bool;
        this.A00 = r1;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C92654Wu r5 = (C92654Wu) obj;
            if (!C29941Vi.A00(this.A01, r5.A01) || !C29941Vi.A00(this.A00, r5.A00)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] objArr = new Object[3];
        objArr[0] = this.A01;
        objArr[1] = this.A00;
        return C12980iv.A0B(null, objArr, 2);
    }
}
