package X;

import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;

/* renamed from: X.1KK  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1KK implements AnonymousClass02B {
    public AnonymousClass02N A00;
    public final ExecutorC27271Gr A01;
    public final /* synthetic */ AnonymousClass02O A02;
    public final /* synthetic */ AnonymousClass02P A03;
    public final /* synthetic */ AnonymousClass1AW A04;

    public AnonymousClass1KK(AnonymousClass02O r4, AnonymousClass02P r5, AnonymousClass1AW r6) {
        this.A04 = r6;
        this.A02 = r4;
        this.A03 = r5;
        this.A01 = new ExecutorC27271Gr(r6.A00, true);
    }

    @Override // X.AnonymousClass02B
    public void ANq(Object obj) {
        AnonymousClass02N r0 = this.A00;
        if (r0 != null) {
            r0.A01();
        }
        RunnableBRunnable0Shape8S0200000_I0_8 runnableBRunnable0Shape8S0200000_I0_8 = new RunnableBRunnable0Shape8S0200000_I0_8(this, obj);
        this.A00 = ((AnonymousClass1KL) runnableBRunnable0Shape8S0200000_I0_8.A00).A00;
        ExecutorC27271Gr r02 = this.A01;
        r02.A00();
        r02.execute(runnableBRunnable0Shape8S0200000_I0_8);
    }
}
