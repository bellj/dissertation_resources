package X;

/* renamed from: X.3q6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C79133q6 extends AbstractC108634zP {
    public AbstractC79123q5 A00;
    public boolean A01 = false;
    public final AbstractC79123q5 A02;

    public C79133q6(AbstractC79123q5 r3) {
        this.A02 = r3;
        this.A00 = (AbstractC79123q5) r3.A05(4);
    }

    @Override // X.AbstractC115625Sh
    public final /* synthetic */ AbstractC117125Yn AhB() {
        return this.A02;
    }

    public void A00() {
        if (this.A01) {
            AbstractC79123q5 r2 = (AbstractC79123q5) this.A00.A05(4);
            C72453ed.A0d(r2).AhG(r2, this.A00);
            this.A00 = r2;
            this.A01 = false;
        }
    }

    @Override // X.AnonymousClass5Yo
    public /* synthetic */ AbstractC117125Yn AhC() {
        boolean z = this.A01;
        AbstractC79123q5 r1 = this.A00;
        if (z) {
            return r1;
        }
        C72453ed.A0d(r1).AhF(r1);
        this.A01 = true;
        return this.A00;
    }

    @Override // X.AbstractC108634zP, java.lang.Object
    public /* synthetic */ Object clone() {
        C79133q6 r3 = (C79133q6) this.A02.A05(5);
        r3.A00();
        AbstractC79123q5 r1 = r3.A00;
        C72453ed.A0d(r1).AhG(r1, (AbstractC79123q5) AhC());
        return r3;
    }
}
