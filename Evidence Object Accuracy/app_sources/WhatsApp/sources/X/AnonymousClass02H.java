package X;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;

/* renamed from: X.02H  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass02H {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public AnonymousClass0QT A05;
    public AbstractC05520Pw A06;
    public RecyclerView A07;
    public AnonymousClass0P8 A08;
    public AnonymousClass0P8 A09;
    public boolean A0A = false;
    public boolean A0B = false;
    public boolean A0C;
    public boolean A0D = false;
    public final AbstractC12650iG A0E;
    public final AbstractC12650iG A0F;

    public void A0M(AbstractC11870h0 r1, int i) {
    }

    public boolean A0T() {
        return false;
    }

    public int A0Y(AnonymousClass0QS r2, C05480Ps r3, int i) {
        return 0;
    }

    public int A0Z(AnonymousClass0QS r2, C05480Ps r3, int i) {
        return 0;
    }

    public int A0a(C05480Ps r2) {
        return 0;
    }

    public int A0b(C05480Ps r2) {
        return 0;
    }

    public int A0c(C05480Ps r2) {
        return 0;
    }

    public int A0d(C05480Ps r2) {
        return 0;
    }

    public int A0e(C05480Ps r2) {
        return 0;
    }

    public int A0f(C05480Ps r2) {
        return 0;
    }

    public Parcelable A0g() {
        return null;
    }

    public View A0h(View view, AnonymousClass0QS r3, C05480Ps r4, int i) {
        return null;
    }

    public abstract AnonymousClass0B6 A0i();

    public void A0n(int i) {
    }

    public void A0o(int i) {
    }

    public void A0q(Parcelable parcelable) {
    }

    public void A0t(AbstractC11870h0 r1, C05480Ps r2, int i, int i2) {
    }

    public void A0w(C05480Ps r1) {
    }

    public void A0y(RecyclerView recyclerView) {
    }

    public void A0z(RecyclerView recyclerView, int i, int i2) {
    }

    public void A10(RecyclerView recyclerView, int i, int i2) {
    }

    public void A11(RecyclerView recyclerView, int i, int i2, int i3) {
    }

    public boolean A14() {
        return false;
    }

    public boolean A15() {
        return false;
    }

    public boolean A17() {
        return false;
    }

    public boolean A18(AnonymousClass0B6 r2) {
        return r2 != null;
    }

    public AnonymousClass02H() {
        AnonymousClass0ZA r2 = new AnonymousClass0ZA(this);
        this.A0E = r2;
        AnonymousClass0ZB r1 = new AnonymousClass0ZB(this);
        this.A0F = r1;
        this.A08 = new AnonymousClass0P8(r2);
        this.A09 = new AnonymousClass0P8(r1);
    }

    public static int A00(int i, int i2, int i3) {
        int mode = View.MeasureSpec.getMode(i);
        int size = View.MeasureSpec.getSize(i);
        if (mode != Integer.MIN_VALUE) {
            return mode != 1073741824 ? Math.max(i2, i3) : size;
        }
        return Math.min(size, Math.max(i2, i3));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0029, code lost:
        if (r5 == 1073741824) goto L_0x002b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0016, code lost:
        if (r5 == 1073741824) goto L_0x002d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A01(int r4, int r5, int r6, int r7, boolean r8) {
        /*
            int r4 = r4 - r6
            r0 = 0
            int r4 = java.lang.Math.max(r0, r4)
            r3 = -2
            r2 = -1
            r0 = -2147483648(0xffffffff80000000, float:-0.0)
            r1 = 1073741824(0x40000000, float:2.0)
            if (r8 == 0) goto L_0x001f
            if (r7 >= 0) goto L_0x002f
            if (r7 != r2) goto L_0x0018
            if (r5 == r0) goto L_0x002d
            if (r5 == 0) goto L_0x0018
            if (r5 == r1) goto L_0x002d
        L_0x0018:
            r5 = 0
            r7 = 0
        L_0x001a:
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r7, r5)
            return r0
        L_0x001f:
            if (r7 >= 0) goto L_0x002f
            if (r7 == r2) goto L_0x002d
            if (r7 != r3) goto L_0x0018
            if (r5 == r0) goto L_0x002b
            r0 = r5
            r5 = 0
            if (r0 != r1) goto L_0x002d
        L_0x002b:
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
        L_0x002d:
            r7 = r4
            goto L_0x001a
        L_0x002f:
            r5 = 1073741824(0x40000000, float:2.0)
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass02H.A01(int, int, int, int, boolean):int");
    }

    public static int A02(View view) {
        return ((AnonymousClass0B6) view.getLayoutParams()).A00();
    }

    public static C04800Nc A03(Context context, AttributeSet attributeSet, int i, int i2) {
        C04800Nc r3 = new C04800Nc();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C04310Lf.A00, i, i2);
        r3.A00 = obtainStyledAttributes.getInt(0, 1);
        r3.A01 = obtainStyledAttributes.getInt(9, 1);
        r3.A02 = obtainStyledAttributes.getBoolean(8, false);
        r3.A03 = obtainStyledAttributes.getBoolean(10, false);
        obtainStyledAttributes.recycle();
        return r3;
    }

    public static void A04(View view, int i, int i2, int i3, int i4) {
        AnonymousClass0B6 r2 = (AnonymousClass0B6) view.getLayoutParams();
        Rect rect = r2.A03;
        view.layout(i + rect.left + ((ViewGroup.MarginLayoutParams) r2).leftMargin, i2 + rect.top + ((ViewGroup.MarginLayoutParams) r2).topMargin, (i3 - rect.right) - ((ViewGroup.MarginLayoutParams) r2).rightMargin, (i4 - rect.bottom) - ((ViewGroup.MarginLayoutParams) r2).bottomMargin);
    }

    public static boolean A05(int i, int i2, int i3) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (i3 <= 0 || i == i3) {
            if (mode != Integer.MIN_VALUE) {
                if (mode == 0) {
                    return true;
                }
                if (mode == 1073741824 && size == i) {
                    return true;
                }
            } else if (size >= i) {
                return true;
            }
        }
        return false;
    }

    public int A06() {
        AnonymousClass0QT r0 = this.A05;
        if (r0 != null) {
            return r0.A00();
        }
        return 0;
    }

    public int A07() {
        AnonymousClass02M r0;
        RecyclerView recyclerView = this.A07;
        if (recyclerView == null || (r0 = recyclerView.A0N) == null) {
            return 0;
        }
        return r0.A0D();
    }

    public int A08() {
        RecyclerView recyclerView = this.A07;
        if (recyclerView != null) {
            return recyclerView.getPaddingBottom();
        }
        return 0;
    }

    public int A09() {
        RecyclerView recyclerView = this.A07;
        if (recyclerView != null) {
            return recyclerView.getPaddingLeft();
        }
        return 0;
    }

    public int A0A() {
        RecyclerView recyclerView = this.A07;
        if (recyclerView != null) {
            return recyclerView.getPaddingRight();
        }
        return 0;
    }

    public int A0B() {
        RecyclerView recyclerView = this.A07;
        if (recyclerView != null) {
            return recyclerView.getPaddingTop();
        }
        return 0;
    }

    public View A0C(int i) {
        int A06 = A06();
        for (int i2 = 0; i2 < A06; i2++) {
            View A0D = A0D(i2);
            AnonymousClass03U A01 = RecyclerView.A01(A0D);
            if (A01 != null) {
                int i3 = A01.A06;
                if (i3 == -1) {
                    i3 = A01.A05;
                }
                if (i3 == i && !A01.A06() && (this.A07.A0y.A08 || (A01.A00 & 8) == 0)) {
                    return A0D;
                }
            }
        }
        return null;
    }

    public View A0D(int i) {
        AnonymousClass0QT r0 = this.A05;
        if (r0 != null) {
            return r0.A03(i);
        }
        return null;
    }

    public void A0E() {
        RecyclerView recyclerView = this.A07;
        if (recyclerView != null) {
            recyclerView.requestLayout();
        }
    }

    public void A0F(int i, int i2) {
        this.A03 = View.MeasureSpec.getSize(i);
        int mode = View.MeasureSpec.getMode(i);
        this.A04 = mode;
        if (mode == 0 && !RecyclerView.A1C) {
            this.A03 = 0;
        }
        this.A00 = View.MeasureSpec.getSize(i2);
        int mode2 = View.MeasureSpec.getMode(i2);
        this.A01 = mode2;
        if (mode2 == 0 && !RecyclerView.A1C) {
            this.A00 = 0;
        }
    }

    public void A0G(int i, int i2) {
        int A06 = A06();
        if (A06 == 0) {
            this.A07.A0b(i, i2);
            return;
        }
        int i3 = Integer.MIN_VALUE;
        int i4 = Integer.MIN_VALUE;
        int i5 = Integer.MAX_VALUE;
        int i6 = Integer.MAX_VALUE;
        for (int i7 = 0; i7 < A06; i7++) {
            View A0D = A0D(i7);
            Rect rect = this.A07.A0s;
            RecyclerView.A03(A0D, rect);
            int i8 = rect.left;
            if (i8 < i5) {
                i5 = i8;
            }
            int i9 = rect.right;
            if (i9 > i3) {
                i3 = i9;
            }
            int i10 = rect.top;
            if (i10 < i6) {
                i6 = i10;
            }
            int i11 = rect.bottom;
            if (i11 > i4) {
                i4 = i11;
            }
        }
        this.A07.A0s.set(i5, i6, i3, i4);
        A0p(this.A07.A0s, i, i2);
    }

    public void A0H(Rect rect, View view) {
        Matrix matrix;
        Rect rect2 = ((AnonymousClass0B6) view.getLayoutParams()).A03;
        rect.set(-rect2.left, -rect2.top, view.getWidth() + rect2.right, view.getHeight() + rect2.bottom);
        if (!(this.A07 == null || (matrix = view.getMatrix()) == null || matrix.isIdentity())) {
            RectF rectF = this.A07.A0u;
            rectF.set(rect);
            matrix.mapRect(rectF);
            rect.set((int) Math.floor((double) rectF.left), (int) Math.floor((double) rectF.top), (int) Math.ceil((double) rectF.right), (int) Math.ceil((double) rectF.bottom));
        }
        rect.offset(view.getLeft(), view.getTop());
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0102  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0I(android.view.View r10, int r11, boolean r12) {
        /*
        // Method dump skipped, instructions count: 293
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass02H.A0I(android.view.View, int, boolean):void");
    }

    public void A0J(View view, Rect rect) {
        RecyclerView recyclerView = this.A07;
        if (recyclerView == null) {
            rect.set(0, 0, 0, 0);
        } else {
            rect.set(recyclerView.A0A(view));
        }
    }

    public void A0K(View view, AnonymousClass04Z r4) {
        AnonymousClass03U A01 = RecyclerView.A01(view);
        if (A01 != null && (A01.A00 & 8) == 0) {
            AnonymousClass0QT r0 = this.A05;
            if (!r0.A02.contains(A01.A0H)) {
                RecyclerView recyclerView = this.A07;
                A0r(view, r4, recyclerView.A0w, recyclerView.A0y);
            }
        }
    }

    public void A0L(View view, AnonymousClass0QS r6) {
        AnonymousClass0QT r3 = this.A05;
        RecyclerView recyclerView = ((AnonymousClass0Z0) r3.A01).A00;
        int indexOfChild = recyclerView.indexOfChild(view);
        if (indexOfChild >= 0) {
            if (r3.A00.A07(indexOfChild)) {
                r3.A08(view);
            }
            View childAt = recyclerView.getChildAt(indexOfChild);
            if (childAt != null) {
                recyclerView.A0h(childAt);
                childAt.clearAnimation();
            }
            recyclerView.removeViewAt(indexOfChild);
        }
        r6.A05(view);
    }

    public void A0N(AnonymousClass0QS r5) {
        int A06 = A06();
        while (true) {
            A06--;
            if (A06 >= 0) {
                View A0D = A0D(A06);
                AnonymousClass03U A01 = RecyclerView.A01(A0D);
                if (!A01.A06()) {
                    if ((A01.A00 & 4) == 0 || (A01.A00 & 8) != 0 || this.A07.A0N.A00) {
                        A0D(A06);
                        this.A05.A05(A06);
                        r5.A06(A0D);
                        this.A07.A11.A04(A01);
                    } else {
                        if (A0D(A06) != null) {
                            this.A05.A06(A06);
                        }
                        r5.A08(A01);
                    }
                }
            } else {
                return;
            }
        }
    }

    public void A0O(AnonymousClass0QS r3) {
        int A06 = A06();
        while (true) {
            A06--;
            if (A06 < 0) {
                return;
            }
            if (!RecyclerView.A01(A0D(A06)).A06()) {
                A0Q(r3, A06);
            }
        }
    }

    public void A0P(AnonymousClass0QS r8) {
        ArrayList arrayList = r8.A05;
        int size = arrayList.size();
        for (int i = size - 1; i >= 0; i--) {
            View view = ((AnonymousClass03U) arrayList.get(i)).A0H;
            AnonymousClass03U A01 = RecyclerView.A01(view);
            if (!A01.A06()) {
                A01.A05(false);
                if ((A01.A00 & 256) != 0) {
                    this.A07.removeDetachedView(view, false);
                }
                AnonymousClass04Y r0 = this.A07.A0R;
                if (r0 != null) {
                    r0.A0A(A01);
                }
                A01.A05(true);
                AnonymousClass03U A012 = RecyclerView.A01(view);
                A012.A09 = null;
                A012.A0G = false;
                A012.A00 &= -33;
                r8.A08(A012);
            }
        }
        arrayList.clear();
        ArrayList arrayList2 = r8.A04;
        if (arrayList2 != null) {
            arrayList2.clear();
        }
        if (size > 0) {
            this.A07.invalidate();
        }
    }

    public void A0Q(AnonymousClass0QS r3, int i) {
        View A0D = A0D(i);
        if (A0D(i) != null) {
            this.A05.A06(i);
        }
        r3.A05(A0D);
    }

    public void A0R(AbstractC05520Pw r5) {
        AbstractC05520Pw r1 = this.A06;
        if (!(r1 == null || r5 == r1 || !r1.A05)) {
            r1.A01();
        }
        this.A06 = r5;
        RecyclerView recyclerView = this.A07;
        if (r5.A06) {
            StringBuilder sb = new StringBuilder("An instance of ");
            String simpleName = r5.getClass().getSimpleName();
            sb.append(simpleName);
            sb.append(" was started ");
            sb.append("more than once. Each instance of");
            sb.append(simpleName);
            sb.append(" ");
            sb.append("is intended to only be used once. You should create a new instance for ");
            sb.append("each use.");
            Log.w("RecyclerView", sb.toString());
        }
        r5.A03 = recyclerView;
        r5.A02 = this;
        int i = r5.A00;
        if (i != -1) {
            recyclerView.A0y.A06 = i;
            r5.A05 = true;
            r5.A04 = true;
            r5.A01 = recyclerView.A0S.A0C(i);
            r5.A03.A0z.A01();
            r5.A06 = true;
            return;
        }
        throw new IllegalArgumentException("Invalid target position");
    }

    public boolean A0S() {
        RecyclerView recyclerView = this.A07;
        return recyclerView != null && recyclerView.A0d;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x00a4, code lost:
        if ((r1.bottom - r5) > r6) goto L_0x00a7;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0U(android.graphics.Rect r12, android.view.View r13, androidx.recyclerview.widget.RecyclerView r14, boolean r15, boolean r16) {
        /*
            r11 = this;
            r0 = 2
            int[] r8 = new int[r0]
            int r4 = r11.A09()
            int r3 = r11.A0B()
            int r2 = r11.A03
            int r0 = r11.A0A()
            int r2 = r2 - r0
            int r1 = r11.A00
            int r0 = r11.A08()
            int r1 = r1 - r0
            int r9 = r13.getLeft()
            int r0 = r12.left
            int r9 = r9 + r0
            int r0 = r13.getScrollX()
            int r9 = r9 - r0
            int r7 = r13.getTop()
            int r0 = r12.top
            int r7 = r7 + r0
            int r0 = r13.getScrollY()
            int r7 = r7 - r0
            int r6 = r12.width()
            int r6 = r6 + r9
            int r0 = r12.height()
            int r0 = r0 + r7
            int r9 = r9 - r4
            r4 = 0
            int r10 = java.lang.Math.min(r4, r9)
            int r7 = r7 - r3
            int r5 = java.lang.Math.min(r4, r7)
            int r6 = r6 - r2
            int r3 = java.lang.Math.max(r4, r6)
            int r0 = r0 - r1
            int r2 = java.lang.Math.max(r4, r0)
            androidx.recyclerview.widget.RecyclerView r0 = r11.A07
            int r1 = X.AnonymousClass028.A05(r0)
            r0 = 1
            if (r1 != r0) goto L_0x00b1
            if (r3 != 0) goto L_0x005f
            int r3 = java.lang.Math.max(r10, r6)
        L_0x005f:
            if (r5 != 0) goto L_0x0065
            int r5 = java.lang.Math.min(r7, r2)
        L_0x0065:
            r8[r4] = r3
            r8[r0] = r5
            r10 = 0
            r4 = r8[r4]
            r9 = 1
            if (r16 == 0) goto L_0x00a7
            android.view.View r8 = r14.getFocusedChild()
            if (r8 == 0) goto L_0x00a6
            int r7 = r11.A09()
            int r6 = r11.A0B()
            int r3 = r11.A03
            int r0 = r11.A0A()
            int r3 = r3 - r0
            int r2 = r11.A00
            int r0 = r11.A08()
            int r2 = r2 - r0
            androidx.recyclerview.widget.RecyclerView r0 = r11.A07
            android.graphics.Rect r1 = r0.A0s
            androidx.recyclerview.widget.RecyclerView.A03(r8, r1)
            int r0 = r1.left
            int r0 = r0 - r4
            if (r0 >= r3) goto L_0x00a6
            int r0 = r1.right
            int r0 = r0 - r4
            if (r0 <= r7) goto L_0x00a6
            int r0 = r1.top
            int r0 = r0 - r5
            if (r0 >= r2) goto L_0x00a6
            int r0 = r1.bottom
            int r0 = r0 - r5
            if (r0 > r6) goto L_0x00a7
        L_0x00a6:
            return r10
        L_0x00a7:
            if (r4 != 0) goto L_0x00ab
            if (r5 == 0) goto L_0x00a6
        L_0x00ab:
            if (r15 == 0) goto L_0x00b9
            r14.scrollBy(r4, r5)
            return r9
        L_0x00b1:
            if (r10 != 0) goto L_0x00b7
            int r10 = java.lang.Math.min(r9, r3)
        L_0x00b7:
            r3 = r10
            goto L_0x005f
        L_0x00b9:
            r14.A0d(r4, r5)
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass02H.A0U(android.graphics.Rect, android.view.View, androidx.recyclerview.widget.RecyclerView, boolean, boolean):boolean");
    }

    public boolean A0V(View view, AnonymousClass0B6 r4, int i, int i2) {
        return view.isLayoutRequested() || !A05(view.getWidth(), i, ((ViewGroup.MarginLayoutParams) r4).width) || !A05(view.getHeight(), i2, ((ViewGroup.MarginLayoutParams) r4).height);
    }

    public int A0W(AnonymousClass0QS r4, C05480Ps r5) {
        AnonymousClass02M r1;
        RecyclerView recyclerView = this.A07;
        if (recyclerView == null || (r1 = recyclerView.A0N) == null || !A14()) {
            return 1;
        }
        return r1.A0D();
    }

    public int A0X(AnonymousClass0QS r4, C05480Ps r5) {
        AnonymousClass02M r1;
        RecyclerView recyclerView = this.A07;
        if (recyclerView == null || (r1 = recyclerView.A0N) == null || !A15()) {
            return 1;
        }
        return r1.A0D();
    }

    public AnonymousClass0B6 A0j(Context context, AttributeSet attributeSet) {
        return new AnonymousClass0B6(context, attributeSet);
    }

    public AnonymousClass0B6 A0k(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof AnonymousClass0B6) {
            return new AnonymousClass0B6((AnonymousClass0B6) layoutParams);
        }
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return new AnonymousClass0B6((ViewGroup.MarginLayoutParams) layoutParams);
        }
        return new AnonymousClass0B6(layoutParams);
    }

    public void A0l(int i) {
        RecyclerView recyclerView = this.A07;
        if (recyclerView != null) {
            int A00 = recyclerView.A0K.A00();
            for (int i2 = 0; i2 < A00; i2++) {
                recyclerView.A0K.A03(i2).offsetLeftAndRight(i);
            }
        }
    }

    public void A0m(int i) {
        RecyclerView recyclerView = this.A07;
        if (recyclerView != null) {
            int A00 = recyclerView.A0K.A00();
            for (int i2 = 0; i2 < A00; i2++) {
                recyclerView.A0K.A03(i2).offsetTopAndBottom(i);
            }
        }
    }

    public void A0p(Rect rect, int i, int i2) {
        int width = rect.width() + A09() + A0A();
        int height = rect.height() + A0B() + A08();
        this.A07.setMeasuredDimension(A00(i, width, this.A07.getMinimumWidth()), A00(i2, height, this.A07.getMinimumHeight()));
    }

    public void A0r(View view, AnonymousClass04Z r9, AnonymousClass0QS r10, C05480Ps r11) {
        int i;
        int i2;
        if (A15()) {
            i = A02(view);
        } else {
            i = 0;
        }
        if (A14()) {
            i2 = A02(view);
        } else {
            i2 = 0;
        }
        r9.A0J(C06410Tm.A01(i, 1, i2, 1, false, false));
    }

    public void A0s(AccessibilityEvent accessibilityEvent) {
        RecyclerView recyclerView = this.A07;
        if (recyclerView != null) {
            boolean z = true;
            if (!recyclerView.canScrollVertically(1) && !this.A07.canScrollVertically(-1) && !this.A07.canScrollHorizontally(-1) && !this.A07.canScrollHorizontally(1)) {
                z = false;
            }
            accessibilityEvent.setScrollable(z);
            AnonymousClass02M r0 = this.A07.A0N;
            if (r0 != null) {
                accessibilityEvent.setItemCount(r0.A0D());
            }
        }
    }

    public void A0u(AnonymousClass0QS r3, C05480Ps r4) {
        Log.e("RecyclerView", "You must override onLayoutChildren(Recycler recycler, State state) ");
    }

    public void A0v(AnonymousClass0QS r1, RecyclerView recyclerView) {
    }

    public void A0x(C05480Ps r3, RecyclerView recyclerView, int i) {
        Log.e("RecyclerView", "You must override smoothScrollToPosition to support smooth scrolling");
    }

    public void A12(RecyclerView recyclerView, Object obj, int i, int i2) {
    }

    public void A13(String str) {
        RecyclerView recyclerView = this.A07;
        if (recyclerView != null) {
            recyclerView.A0q(str);
        }
    }

    public boolean A16() {
        return this.A0A;
    }
}
