package X;

import android.animation.ValueAnimator;
import android.graphics.Bitmap;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.biz.catalog.view.widgets.QuantitySelector;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.math.BigDecimal;
import java.util.List;

/* renamed from: X.2uQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59302uQ extends AbstractC59312uU {
    public final int A00;
    public final C15570nT A01;
    public final AnonymousClass19Q A02;
    public final AnonymousClass1lN A03;
    public final QuantitySelector A04;
    public final AnonymousClass5TV A05;

    public C59302uQ(View view, C15570nT r11, AnonymousClass19Q r12, C37071lG r13, AnonymousClass1lM r14, AnonymousClass1lN r15, AnonymousClass5TV r16, AbstractC116525Vu r17, AnonymousClass018 r18, UserJid userJid) {
        super(view, r13, r14, r18, userJid);
        this.A01 = r11;
        this.A02 = r12;
        this.A03 = r15;
        this.A05 = r16;
        this.A00 = view.getResources().getColor(R.color.disabled_text);
        QuantitySelector quantitySelector = (QuantitySelector) AnonymousClass028.A0D(view, R.id.product_item_quantity_selector);
        this.A04 = quantitySelector;
        quantitySelector.setCollapsible(true);
        quantitySelector.setVisibility(0);
        quantitySelector.A05 = new AnonymousClass5TU(r14, r15, this, r17) { // from class: X.3VT
            public final /* synthetic */ AnonymousClass1lM A00;
            public final /* synthetic */ AnonymousClass1lN A01;
            public final /* synthetic */ C59302uQ A02;
            public final /* synthetic */ AbstractC116525Vu A03;

            {
                this.A02 = r3;
                this.A00 = r1;
                this.A01 = r2;
                this.A03 = r4;
            }

            @Override // X.AnonymousClass5TU
            public final void AUU(long j) {
                String str;
                C59302uQ r3 = this.A02;
                AnonymousClass1lM r0 = this.A00;
                AnonymousClass1lN r2 = this.A01;
                AbstractC116525Vu r4 = this.A03;
                int i = ((AnonymousClass03U) r3).A06;
                if (i == -1) {
                    i = ((AnonymousClass03U) r3).A05;
                }
                C44691zO AFw = r0.AFw(i);
                String str2 = null;
                if (r2 != null) {
                    int i2 = ((AnonymousClass03U) r3).A06;
                    if (i2 == -1) {
                        i2 = ((AnonymousClass03U) r3).A05;
                    }
                    C90174Mw ABV = r2.ABV(i2);
                    if (ABV != null) {
                        str2 = ABV.A00;
                        str = ABV.A01;
                        r4.AUV(AFw, str2, str, i, j);
                    }
                }
                str = null;
                r4.AUV(AFw, str2, str, i, j);
            }
        };
        quantitySelector.A04 = new AnonymousClass5TT(r14, this, r17) { // from class: X.53u
            public final /* synthetic */ AnonymousClass1lM A00;
            public final /* synthetic */ C59302uQ A01;
            public final /* synthetic */ AbstractC116525Vu A02;

            {
                this.A01 = r2;
                this.A00 = r1;
                this.A02 = r3;
            }

            @Override // X.AnonymousClass5TT
            public final void ARm(long j) {
                C59302uQ r4 = this.A01;
                AnonymousClass1lM r3 = this.A00;
                AbstractC116525Vu r2 = this.A02;
                int i = ((AnonymousClass03U) r4).A06;
                if (i == -1) {
                    i = ((AnonymousClass03U) r4).A05;
                }
                r2.ARn(r3.AFw(i), j);
            }
        };
        AbstractView$OnClickListenerC34281fs.A00(view, this, 22);
    }

    public void A0A() {
        QuantitySelector quantitySelector = this.A04;
        if (quantitySelector.A0B) {
            quantitySelector.A0D.removeCallbacksAndMessages(null);
            ValueAnimator valueAnimator = quantitySelector.A02;
            if (valueAnimator != null) {
                valueAnimator.end();
                quantitySelector.A02.removeAllUpdateListeners();
                quantitySelector.A0A = false;
            }
            quantitySelector.A06 = EnumC870449z.COLLAPSED;
            quantitySelector.A04(quantitySelector.A01, quantitySelector.A00);
        }
    }

    /* renamed from: A0B */
    public void A09(C84693zj r18) {
        TextView textView;
        QuantitySelector quantitySelector;
        ImageView imageView;
        float f;
        int A00 = A00();
        FrameLayout frameLayout = ((AbstractC59312uU) this).A03;
        if (A00 == 0) {
            frameLayout.setPadding(0, (int) this.A0H.getResources().getDimension(R.dimen.product_catalog_list_thumb_margin_vertical), 0, 0);
        } else {
            frameLayout.setPadding(0, 0, 0, 0);
        }
        C44691zO AFw = ((AbstractC59312uU) this).A09.AFw(A00);
        TextEmojiLabel textEmojiLabel = ((AbstractC59312uU) this).A07;
        textEmojiLabel.A0G(null, AFw.A04);
        String str = AFw.A0A;
        boolean A0C = AnonymousClass1US.A0C(str);
        TextEmojiLabel textEmojiLabel2 = ((AbstractC59312uU) this).A06;
        if (!A0C) {
            textEmojiLabel2.setVisibility(0);
            textEmojiLabel2.A0F(str, null, 0, true);
        } else {
            textEmojiLabel2.setVisibility(8);
        }
        if (AFw.A05 == null || AFw.A03 == null) {
            textView = ((AbstractC59312uU) this).A05;
            textView.setVisibility(8);
        } else {
            textView = ((AbstractC59312uU) this).A05;
            textView.setVisibility(0);
            BigDecimal bigDecimal = AFw.A05;
            C30711Yn r10 = AFw.A03;
            SpannableStringBuilder A0J = C12990iw.A0J(C65003Ht.A01(textView.getContext(), AFw.A02, r10, ((AbstractC59312uU) this).A0A, bigDecimal, ((AbstractC59312uU) this).A0C));
            if (1 == AFw.A00) {
                A0J.append((CharSequence) " • ").append((CharSequence) textView.getContext().getString(R.string.out_of_stock));
            }
            textView.setText(A0J);
        }
        if (AFw.A00 != 0) {
            int i = this.A00;
            textEmojiLabel.setTextColor(i);
            textEmojiLabel2.setTextColor(i);
            textView.setTextColor(i);
            quantitySelector = this.A04;
            quantitySelector.setVisibility(8);
            imageView = ((AbstractC59312uU) this).A04;
            f = 0.5f;
        } else {
            textEmojiLabel.setTextColor(((AbstractC59312uU) this).A02);
            textEmojiLabel2.setTextColor(((AbstractC59312uU) this).A00);
            textView.setTextColor(((AbstractC59312uU) this).A01);
            quantitySelector = this.A04;
            quantitySelector.setVisibility(0);
            imageView = ((AbstractC59312uU) this).A04;
            f = 1.0f;
        }
        imageView.setAlpha(f);
        AnonymousClass4Dz.A00(imageView);
        List list = AFw.A06;
        if (list.isEmpty()) {
            Log.w("ProductBaseViewHolder/bindViewInSection/no-product-images");
        }
        if (!AFw.A02() && !list.isEmpty()) {
            ((AbstractC59312uU) this).A08.A02(imageView, (C44741zT) list.get(0), null, new AnonymousClass2E5() { // from class: X.3V8
                @Override // X.AnonymousClass2E5
                public final void AS6(Bitmap bitmap, C68203Um r4, boolean z) {
                    ImageView imageView2 = (ImageView) r4.A09.get();
                    if (imageView2 != null) {
                        imageView2.setBackgroundColor(0);
                        imageView2.setImageBitmap(bitmap);
                        C12990iw.A1E(imageView2);
                    }
                }
            }, 2);
        }
        quantitySelector.A04(r18.A00, r18.A01.A08);
    }
}
