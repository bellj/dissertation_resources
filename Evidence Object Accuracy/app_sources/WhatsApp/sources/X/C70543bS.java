package X;

/* renamed from: X.3bS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class C70543bS implements AbstractC14590lg {
    public final /* synthetic */ String A00;

    public /* synthetic */ C70543bS(String str) {
        this.A00 = str;
    }

    @Override // X.AbstractC14590lg
    public final void accept(Object obj) {
        String str = this.A00;
        AnonymousClass1EV r8 = (AnonymousClass1EV) obj;
        C91774Tb r5 = (C91774Tb) r8.A03.get(str);
        if (r5 != null && r5.A03 == 0) {
            r5.A03 = System.currentTimeMillis();
            C614930o r4 = new C614930o();
            r4.A05 = C12980iv.A0l(r5.A00);
            r4.A02 = Long.valueOf(r5.A04 - r5.A02);
            r4.A03 = Long.valueOf(r5.A06 - r5.A04);
            r4.A00 = Long.valueOf(r5.A05 - r5.A06);
            r4.A01 = Long.valueOf(r5.A03 - r5.A05);
            r4.A04 = Long.valueOf(r5.A03 - r5.A04);
            r8.A01.A07(r4);
            AnonymousClass1Q5 r3 = r8.A02;
            int hashCode = str.hashCode();
            r3.A01(hashCode, "iq_processing");
            short s = 2;
            if (r5.A01) {
                s = 87;
            }
            r3.A05(hashCode, s);
        }
    }
}
