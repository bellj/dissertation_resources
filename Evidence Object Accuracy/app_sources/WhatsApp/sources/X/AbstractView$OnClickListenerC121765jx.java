package X;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.CopyableTextView;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiBankAccountDetailsActivity;
import com.whatsapp.payments.ui.IndiaUpiPaymentsAccountSetupActivity;
import com.whatsapp.payments.ui.NoviPaymentBankDetailsActivity;
import com.whatsapp.payments.ui.NoviPaymentCardDetailsActivity;
import com.whatsapp.payments.ui.PaymentDeleteAccountActivity;
import com.whatsapp.payments.ui.widget.PayToolbar;
import com.whatsapp.util.Log;

/* renamed from: X.5jx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractView$OnClickListenerC121765jx extends AbstractActivityC119275dS implements View.OnClickListener {
    public int A00;
    public ImageView A01;
    public TextView A02;
    public TextView A03;
    public CopyableTextView A04;
    public C14900mE A05;
    public AnonymousClass19Y A06;
    public C18790t3 A07;
    public AnonymousClass018 A08;
    public AbstractC28901Pl A09;
    public C21860y6 A0A;
    public C25861Bc A0B;
    public C17900ra A0C;
    public C17070qD A0D;
    public C124295ov A0E;
    public C130035yh A0F;
    public PayToolbar A0G;
    public AbstractC14440lR A0H;
    public boolean A0I;
    public final AbstractC460624h A0J = new AbstractC460624h() { // from class: X.69K
        @Override // X.AbstractC460624h
        public final void ATX(AbstractC28901Pl r6, AnonymousClass1V8 r7) {
            AbstractView$OnClickListenerC121765jx r4 = AbstractView$OnClickListenerC121765jx.this;
            C30931Zj r3 = r4.A0K;
            StringBuilder A0k = C12960it.A0k("paymentMethodNotificationObserver is called ");
            boolean z = true;
            A0k.append(C12960it.A1W(r6));
            C117295Zj.A1F(r3, A0k);
            if (r4.A09 != null) {
                z = false;
            }
            r4.A2i(r6, z);
        }
    };
    public final C30931Zj A0K = C117305Zk.A0V("PaymentMethodDetailsActivity", "payment-settings");

    @Override // X.ActivityC13810kN
    public void A2A(int i) {
        if (i == R.string.payment_method_is_removed) {
            finish();
        }
    }

    public final int A2e(int i) {
        TypedArray typedArray;
        try {
            typedArray = obtainStyledAttributes(i, new int[]{16843071});
        } catch (Resources.NotFoundException e) {
            Log.e(e.getMessage());
            typedArray = null;
        }
        if (typedArray != null) {
            return typedArray.getDimensionPixelOffset(0, -1);
        }
        return -1;
    }

    public AnonymousClass04S A2f(CharSequence charSequence, String str, boolean z) {
        int i = 201;
        if (z) {
            i = 200;
        }
        C004802e r2 = new C004802e(this, R.style.FbPayDialogTheme);
        r2.A0A(charSequence);
        r2.A0B(true);
        r2.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener(i) { // from class: X.62c
            public final /* synthetic */ int A00;

            {
                this.A00 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i2) {
                C36021jC.A00(AbstractView$OnClickListenerC121765jx.this, this.A00);
            }
        });
        r2.A03(new DialogInterface.OnClickListener(i, z) { // from class: X.62l
            public final /* synthetic */ int A00;
            public final /* synthetic */ boolean A02;

            {
                this.A00 = r2;
                this.A02 = r3;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i2) {
                AbstractView$OnClickListenerC121765jx r22 = AbstractView$OnClickListenerC121765jx.this;
                int i3 = this.A00;
                boolean z2 = this.A02;
                C36021jC.A00(r22, i3);
                r22.A2j(z2);
            }
        }, str);
        r2.A08(new DialogInterface.OnCancelListener(i) { // from class: X.62C
            public final /* synthetic */ int A00;

            {
                this.A00 = r2;
            }

            @Override // android.content.DialogInterface.OnCancelListener
            public final void onCancel(DialogInterface dialogInterface) {
                C36021jC.A00(AbstractView$OnClickListenerC121765jx.this, this.A00);
            }
        });
        if (!z) {
            r2.setTitle(getString(R.string.delete_payment_method_dialog_title));
        }
        return r2.create();
    }

    public void A2g() {
        C12960it.A1E(new C124135of(this.A0D, this.A0K, new C125915s1(this)), this.A0H);
    }

    public void A2h() {
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0M(true);
            int currentContentInsetRight = this.A0G.getCurrentContentInsetRight();
            this.A0G.A0B(A2e(2131952619), currentContentInsetRight);
        }
    }

    public void A2i(AbstractC28901Pl r3, boolean z) {
        int i;
        AaN();
        if (r3 == null) {
            finish();
            return;
        }
        this.A09 = r3;
        this.A0I = C12960it.A1V(r3.A01, 2);
        C117315Zl.A0N(this.A03, C117295Zj.A0R(r3.A09));
        ImageView A06 = C117305Zk.A06(this, R.id.payment_method_icon);
        if (r3 instanceof C30881Ze) {
            i = C1311161i.A00(((C30881Ze) r3).A01);
        } else {
            Bitmap A05 = r3.A05();
            if (A05 != null) {
                A06.setImageBitmap(A05);
                this.A0F.A01(r3);
            }
            i = R.drawable.av_bank;
        }
        A06.setImageResource(i);
        this.A0F.A01(r3);
    }

    public void A2j(boolean z) {
        if (this instanceof NoviPaymentBankDetailsActivity) {
            return;
        }
        if (!(this instanceof IndiaUpiBankAccountDetailsActivity)) {
            AbstractActivityC121755jw r4 = (AbstractActivityC121755jw) this;
            r4.A2C(R.string.register_wait_message);
            C1328668n r2 = new C1328668n(null, r4, 0);
            if (z) {
                C14900mE r5 = ((AbstractView$OnClickListenerC121765jx) r4).A05;
                AbstractC14440lR r1 = ((AbstractView$OnClickListenerC121765jx) r4).A0H;
                C18590sh r0 = r4.A0C;
                C17070qD r12 = ((AbstractView$OnClickListenerC121765jx) r4).A0D;
                new C129965ya(r4, r5, ((ActivityC13810kN) r4).A07, r4.A01, r4.A03, r4.A05, r4.A06, r4.A08, r12, r0, r1).A00(r2);
                return;
            }
            r4.A06.A0B(r2, null, ((AbstractView$OnClickListenerC121765jx) r4).A09.A0A, null);
            return;
        }
        IndiaUpiBankAccountDetailsActivity indiaUpiBankAccountDetailsActivity = (IndiaUpiBankAccountDetailsActivity) this;
        if (z) {
            indiaUpiBankAccountDetailsActivity.A0H.A06("unlinking the payment account.");
            Intent A0D = C12990iw.A0D(indiaUpiBankAccountDetailsActivity, PaymentDeleteAccountActivity.class);
            A0D.putExtra("extra_remove_payment_account", 1);
            indiaUpiBankAccountDetailsActivity.startActivityForResult(A0D, 0);
            return;
        }
        indiaUpiBankAccountDetailsActivity.A2C(R.string.register_wait_message);
        indiaUpiBankAccountDetailsActivity.A08.AeG();
        AnonymousClass6BE r22 = indiaUpiBankAccountDetailsActivity.A08;
        C1328968q r13 = new C1328968q(new C1328668n(r22, indiaUpiBankAccountDetailsActivity, 13), r22, indiaUpiBankAccountDetailsActivity);
        AnonymousClass1ZY r3 = indiaUpiBankAccountDetailsActivity.A00.A08;
        AnonymousClass009.A06(r3, indiaUpiBankAccountDetailsActivity.A0H.A02("IndiaUpiBankAccountDetailsActivity onRemovePaymentMethod Unable to get IndiaUpiMethodData"));
        C119755f3 r32 = (C119755f3) r3;
        C120515gJ r122 = indiaUpiBankAccountDetailsActivity.A07;
        AnonymousClass1ZR r23 = r32.A09;
        String str = r32.A0F;
        AnonymousClass1ZR r11 = r32.A06;
        String str2 = indiaUpiBankAccountDetailsActivity.A00.A0A;
        if (AnonymousClass1ZS.A02(r23)) {
            Context context = r122.A00;
            C14850m9 r02 = r122.A05;
            C14900mE r15 = r122.A01;
            C15570nT r10 = r122.A02;
            C17220qS r9 = r122.A06;
            C17070qD r8 = r122.A0B;
            C21860y6 r7 = r122.A08;
            C18610sj r6 = r122.A0A;
            AnonymousClass102 r52 = r122.A04;
            AnonymousClass6BE r42 = r122.A0C;
            new C120495gH(context, r15, r10, r52, r02, r9, r122.A07, r7, r122.A09, null, r6, r8, r42, r122.A0D).A01(new AnonymousClass6AT(r11, r13, r122, str2));
            return;
        }
        r122.A00(r23, r11, r13, str, str2);
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 0 && i2 == -1) {
            Intent A0A = C12970iu.A0A();
            int i3 = 0;
            if (intent != null) {
                i3 = intent.getIntExtra("extra_remove_payment_account", 0);
            }
            A0A.putExtra("extra_remove_payment_account", i3);
            setResult(-1, A0A);
            finish();
        }
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        if (view.getId() == R.id.default_payment_method_row) {
            if (!this.A0I) {
                A2C(R.string.register_wait_message);
                if (this instanceof AbstractActivityC121755jw) {
                    AbstractActivityC121755jw r6 = (AbstractActivityC121755jw) this;
                    r6.A2m(new C1329268t(null, null, r6, 0), ((AbstractView$OnClickListenerC121765jx) r6).A09.A0A, null);
                } else if (!(this instanceof NoviPaymentBankDetailsActivity)) {
                    IndiaUpiBankAccountDetailsActivity indiaUpiBankAccountDetailsActivity = (IndiaUpiBankAccountDetailsActivity) this;
                    if (indiaUpiBankAccountDetailsActivity.A04.A0L()) {
                        C1329668y r1 = indiaUpiBankAccountDetailsActivity.A04;
                        if (!r1.A0N(r1.A07())) {
                            Intent A0D = C12990iw.A0D(indiaUpiBankAccountDetailsActivity, IndiaUpiPaymentsAccountSetupActivity.class);
                            A0D.putExtra("extra_setup_mode", 2);
                            A0D.putExtra("extra_payments_entry_type", 7);
                            A0D.putExtra("extra_referral_screen", "payment_bank_account_details");
                            indiaUpiBankAccountDetailsActivity.A2G(A0D, true);
                            return;
                        }
                    }
                    indiaUpiBankAccountDetailsActivity.A2C(R.string.register_wait_message);
                    indiaUpiBankAccountDetailsActivity.A08.AeG();
                    C1329268t r12 = new C1329268t(indiaUpiBankAccountDetailsActivity.A0A, indiaUpiBankAccountDetailsActivity.A08, indiaUpiBankAccountDetailsActivity, 15);
                    AnonymousClass1ZY r3 = indiaUpiBankAccountDetailsActivity.A00.A08;
                    AnonymousClass009.A06(r3, indiaUpiBankAccountDetailsActivity.A0H.A02("onMakeDefaultPaymentMethod Unable to get IndiaUpiMethodData"));
                    C119755f3 r32 = (C119755f3) r3;
                    C120515gJ r11 = indiaUpiBankAccountDetailsActivity.A07;
                    AnonymousClass1ZR r2 = r32.A09;
                    String str = r32.A0F;
                    AnonymousClass1ZR r10 = r32.A06;
                    String str2 = indiaUpiBankAccountDetailsActivity.A00.A0A;
                    if (AnonymousClass1ZS.A02(r2)) {
                        Context context = r11.A00;
                        C14850m9 r0 = r11.A05;
                        C14900mE r15 = r11.A01;
                        C15570nT r14 = r11.A02;
                        C17220qS r9 = r11.A06;
                        C17070qD r8 = r11.A0B;
                        C21860y6 r7 = r11.A08;
                        C18610sj r62 = r11.A0A;
                        AnonymousClass102 r5 = r11.A04;
                        AnonymousClass6BE r4 = r11.A0C;
                        new C120495gH(context, r15, r14, r5, r0, r9, r11.A07, r7, r11.A09, null, r62, r8, r4, r11.A0D).A01(new AnonymousClass6AU(r10, r12, r11, str2));
                        return;
                    }
                    r11.A01(r2, r10, r12, str, str2, true);
                }
            }
        } else if (view.getId() != R.id.help_row) {
        } else {
            if (this instanceof NoviPaymentCardDetailsActivity) {
                NoviPaymentCardDetailsActivity noviPaymentCardDetailsActivity = (NoviPaymentCardDetailsActivity) this;
                AnonymousClass60Y.A02(noviPaymentCardDetailsActivity.A00, "GET_HELP_CLICK", "NOVI_HUB", "FI_INFO", "BUTTON");
                ((ActivityC13790kL) noviPaymentCardDetailsActivity).A00.A06(noviPaymentCardDetailsActivity, AnonymousClass608.A00(((AbstractView$OnClickListenerC121765jx) noviPaymentCardDetailsActivity).A08));
            } else if (!(this instanceof NoviPaymentBankDetailsActivity)) {
                AbstractC14440lR r33 = this.A0H;
                C124295ov r02 = this.A0E;
                if (r02 != null && r02.A00() == 1) {
                    this.A0E.A03(false);
                }
                Bundle A0D2 = C12970iu.A0D();
                A0D2.putString("com.whatsapp.support.DescribeProblemActivity.from", "payments:account-details");
                AnonymousClass1ZY r03 = this.A09.A08;
                if (r03 != null) {
                    A0D2.putString("com.whatsapp.support.DescribeProblemActivity.paymentBankPhone", r03.A08());
                }
                C18790t3 r92 = this.A07;
                C15450nH r82 = ((ActivityC13810kN) this).A06;
                C124295ov r42 = new C124295ov(A0D2, this, this.A06, r82, r92, this.A08, this.A09, null, ((ActivityC13810kN) this).A0D, this.A0C, "payments:account-details");
                this.A0E = r42;
                C12960it.A1E(r42, r33);
            } else {
                NoviPaymentBankDetailsActivity noviPaymentBankDetailsActivity = (NoviPaymentBankDetailsActivity) this;
                AnonymousClass60Y.A02(noviPaymentBankDetailsActivity.A01, "GET_HELP_CLICK", "NOVI_HUB", "FI_INFO", "BUTTON");
                ((ActivityC13790kL) noviPaymentBankDetailsActivity).A00.A06(noviPaymentBankDetailsActivity, AnonymousClass608.A00(((AbstractView$OnClickListenerC121765jx) noviPaymentBankDetailsActivity).A08));
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x00ac  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0123  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0149  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0158  */
    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r10) {
        /*
        // Method dump skipped, instructions count: 460
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractView$OnClickListenerC121765jx.onCreate(android.os.Bundle):void");
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        int i2;
        switch (i) {
            case 200:
                i2 = R.string.delete_payment_method_dialog_title;
                break;
            case 201:
                return A2f(C12960it.A0X(this, C1311161i.A02(this, this.A08, this.A09, this.A0D, true), new Object[1], 0, R.string.delete_payment_method_dialog_message), getString(R.string.remove), false);
            case 202:
                i2 = R.string.delete_payment_accounts_dialog_title_with_warning;
                break;
            default:
                return super.onCreateDialog(i);
        }
        return A2f(AbstractC36671kL.A05(this, ((ActivityC13810kN) this).A0B, getString(i2)), getString(R.string.remove), true);
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, R.id.menuitem_remove_payment_method, 0, getString(R.string.remove_payment_method));
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == 16908332) {
            finish();
            return true;
        } else if (menuItem.getItemId() != R.id.menuitem_remove_payment_method) {
            return super.onOptionsItemSelected(menuItem);
        } else {
            A2g();
            return true;
        }
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStop() {
        this.A0B.A04(this.A0J);
        super.onStop();
    }
}
