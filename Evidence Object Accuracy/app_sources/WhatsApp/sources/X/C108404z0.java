package X;

import android.os.Looper;
import java.lang.ref.WeakReference;
import java.util.concurrent.locks.Lock;

/* renamed from: X.4z0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C108404z0 implements AnonymousClass5Sa {
    public final AnonymousClass1UE A00;
    public final WeakReference A01;
    public final boolean A02;

    public C108404z0(AnonymousClass1UE r2, C108324ys r3, boolean z) {
        this.A01 = C12970iu.A10(r3);
        this.A00 = r2;
        this.A02 = z;
    }

    @Override // X.AnonymousClass5Sa
    public final void AV1(C56492ky r5) {
        C108324ys r3 = (C108324ys) this.A01.get();
        if (r3 != null) {
            C13020j0.A04("onReportServiceBinding must be called on the GoogleApiClient handler thread", C12970iu.A1Z(Looper.myLooper(), r3.A0F.A05.A07));
            Lock lock = r3.A0K;
            lock.lock();
            try {
                if (r3.A07(0)) {
                    if (r5.A01 != 0) {
                        r3.A04(r5, this.A00, this.A02);
                    }
                    if (r3.A06()) {
                        r3.A02();
                    }
                }
            } finally {
                lock.unlock();
            }
        }
    }
}
