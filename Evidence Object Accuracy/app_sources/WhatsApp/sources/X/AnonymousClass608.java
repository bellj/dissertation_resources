package X;

import android.content.Intent;
import android.net.Uri;

/* renamed from: X.608  reason: invalid class name */
/* loaded from: classes4.dex */
public final class AnonymousClass608 {
    public StringBuilder A00;
    public final AnonymousClass018 A01;

    public AnonymousClass608(AnonymousClass018 r3) {
        this.A01 = r3;
        StringBuilder A0h = C12960it.A0h();
        this.A00 = A0h;
        A0h.append("https://novi.com/help/");
        A0h.append("whatsapp/");
        A0h.append("?entrypoint=");
    }

    public AnonymousClass608(AnonymousClass018 r2, String str) {
        this.A01 = r2;
        this.A00 = C12960it.A0k(C12960it.A0d(str, C12960it.A0k("https://novi.com/help/whatsapp/")));
    }

    public static Intent A00(AnonymousClass018 r3) {
        AnonymousClass608 r2 = new AnonymousClass608(r3);
        r2.A00.append("WA");
        return new Intent("android.intent.action.VIEW", r2.A01());
    }

    public Uri A01() {
        return C117295Zj.A06(C12970iu.A14(this.A01), this.A00.toString());
    }
}
