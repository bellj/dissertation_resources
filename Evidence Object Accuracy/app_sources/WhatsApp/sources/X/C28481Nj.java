package X;

/* renamed from: X.1Nj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28481Nj {
    public final int A00;
    public final String A01;
    public final String A02;
    public final String A03;
    public final String A04;
    public final String A05;
    public final boolean A06;

    public C28481Nj(String str, String str2, String str3, String str4, String str5, int i, boolean z) {
        this.A05 = str;
        this.A02 = str2;
        this.A01 = str3;
        this.A04 = str4;
        this.A03 = str5;
        this.A00 = i;
        this.A06 = z;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("SelectedRoute{authority='");
        sb.append(this.A02);
        sb.append("' authorityType='");
        sb.append(this.A00);
        sb.append("'");
        sb.append('}');
        return sb.toString();
    }
}
