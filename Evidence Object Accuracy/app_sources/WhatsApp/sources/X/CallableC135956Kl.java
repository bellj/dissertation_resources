package X;

import android.hardware.camera2.CaptureRequest;
import android.os.SystemClock;
import java.util.concurrent.Callable;

/* renamed from: X.6Kl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class CallableC135956Kl implements Callable {
    public final /* synthetic */ long A00;
    public final /* synthetic */ CaptureRequest.Builder A01;
    public final /* synthetic */ C1308060a A02;
    public final /* synthetic */ AnonymousClass66I A03;

    public CallableC135956Kl(CaptureRequest.Builder builder, C1308060a r2, AnonymousClass66I r3, long j) {
        this.A02 = r2;
        this.A01 = builder;
        this.A03 = r3;
        this.A00 = j;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        CaptureRequest.Builder builder;
        C1308060a r7 = this.A02;
        if (!r7.A0D) {
            throw C12960it.A0U("Not recording video.");
        } else if (r7.A0B == null || r7.A05 == null || r7.A04 == null || r7.A02 == null || r7.A01 == null) {
            throw C12960it.A0U("Cannot stop recording video, camera is closed");
        } else if (r7.A06 != null) {
            long elapsedRealtime = SystemClock.elapsedRealtime() - r7.A00;
            if (elapsedRealtime < 500) {
                SystemClock.sleep(500 - elapsedRealtime);
            }
            AnonymousClass60Q r4 = r7.A06;
            Exception A02 = r7.A02();
            C119085cr r0 = r7.A04;
            C125475rJ r5 = AbstractC130685zo.A0A;
            if (!(C12960it.A05(r0.A03(r5)) == 0 || (builder = this.A01) == null)) {
                C129765yG r1 = new C129765yG();
                r1.A01(r5, 0);
                r7.A04.A05(r1.A00());
                C1308660g.A02(builder, r7.A04, r7.A05, 0);
                r7.A02.A05();
            }
            if (A02 == null) {
                r4.A02(AnonymousClass60Q.A0Q, Long.valueOf(this.A00));
                return r4;
            }
            throw A02;
        } else {
            throw C12960it.A0U("Cannot stop recording video, VideoCaptureInfo is null");
        }
    }
}
