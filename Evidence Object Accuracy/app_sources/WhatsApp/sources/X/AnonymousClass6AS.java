package X;

import com.whatsapp.util.Log;

/* renamed from: X.6AS  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6AS implements AnonymousClass6MQ {
    public final /* synthetic */ C119755f3 A00;
    public final /* synthetic */ AbstractC136246Lu A01;
    public final /* synthetic */ C120505gI A02;

    public AnonymousClass6AS(C119755f3 r1, AbstractC136246Lu r2, C120505gI r3) {
        this.A02 = r3;
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass6MQ
    public void AOb(C119705ey r6) {
        C120505gI r4 = this.A02;
        AnonymousClass1ZR r3 = r6.A02;
        AnonymousClass009.A05(r3);
        r4.A00(r3, this.A00, this.A01, r6.A03);
    }

    @Override // X.AnonymousClass6MQ
    public void APo(C452120p r2) {
        Log.w("PAY: IndiaUpiOtpAction: could not fetch VPA information to request OTP");
        AbstractC136246Lu r0 = this.A01;
        if (r0 != null) {
            r0.AV4(r2);
        }
    }
}
