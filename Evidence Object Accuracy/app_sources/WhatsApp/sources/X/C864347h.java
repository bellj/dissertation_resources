package X;

/* renamed from: X.47h  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C864347h extends AbstractC16350or {
    public final AnonymousClass1BT A00;
    public final AnonymousClass1KX A01;

    public /* synthetic */ C864347h(AnonymousClass1BT r1, AnonymousClass1KX r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        AbstractC470728v[] r5 = (AbstractC470728v[]) objArr;
        AnonymousClass009.A05(r5);
        for (AbstractC470728v r1 : r5) {
            this.A00.A07(r1);
        }
        return null;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        this.A01.A0G();
    }
}
