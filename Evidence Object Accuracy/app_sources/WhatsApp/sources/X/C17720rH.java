package X;

import android.content.SharedPreferences;
import com.whatsapp.jid.UserJid;
import java.util.HashMap;

/* renamed from: X.0rH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17720rH {
    public final C17690rE A00;
    public final C17710rG A01;
    public final C14850m9 A02;
    public final C16120oU A03;
    public final HashMap A04;

    public C17720rH(C17690rE r2, C17710rG r3, C14850m9 r4, C16120oU r5, HashMap hashMap) {
        C16700pc.A0E(r4, 1);
        C16700pc.A0E(r5, 2);
        C16700pc.A0E(r2, 3);
        this.A02 = r4;
        this.A03 = r5;
        this.A00 = r2;
        this.A01 = r3;
        this.A04 = hashMap;
    }

    public final C92834Xp A00(UserJid userJid) {
        HashMap hashMap = this.A04;
        C92834Xp r4 = (C92834Xp) hashMap.get(userJid);
        if (r4 != null) {
            return r4;
        }
        C92834Xp r42 = new C92834Xp(30, null, false);
        hashMap.put(userJid, new C92834Xp(30, null, false));
        return r42;
    }

    public final void A01(UserJid userJid, int i) {
        if (userJid != null) {
            C92834Xp A00 = A00(userJid);
            if ((this.A00.A00(userJid) instanceof C455922g) && this.A02.A07(1681)) {
                C614030f r6 = new C614030f();
                r6.A01 = Integer.valueOf(i);
                if (A00.A04) {
                    r6.A03 = A00.A00;
                    r6.A04 = A00.A03;
                    r6.A00 = Boolean.valueOf(A00.A02);
                }
                C17710rG r8 = this.A01;
                C14820m6 r7 = r8.A01;
                SharedPreferences sharedPreferences = r7.A00;
                long j = sharedPreferences.getLong("pref_ctwa_customer_logging_counter_timestamp", -1);
                C14830m7 r82 = r8.A00;
                if (((float) (r82.A00() - j)) / 8.64E7f >= 1.0f) {
                    r7.A0q("pref_ctwa_customer_logging_counter_timestamp", r82.A00());
                    sharedPreferences.edit().putLong("pref_ctwa_customer_logging_counter", 0).apply();
                }
                r6.A02 = Long.valueOf(sharedPreferences.getLong("pref_ctwa_customer_logging_counter", 0));
                long j2 = sharedPreferences.getLong("pref_ctwa_customer_logging_counter", 0);
                r7.A0q("pref_ctwa_customer_logging_counter_timestamp", r82.A00());
                sharedPreferences.edit().putLong("pref_ctwa_customer_logging_counter", j2 + 1).apply();
                this.A03.A07(r6);
            }
        }
    }
}
