package X;

import android.animation.ObjectAnimator;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import com.whatsapp.mediaview.MediaViewBaseFragment;

/* renamed from: X.3Nw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ViewTreeObserver$OnPreDrawListenerC66513Nw implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ int A02;
    public final /* synthetic */ int A03;
    public final /* synthetic */ View A04;
    public final /* synthetic */ AbstractC35501i8 A05;
    public final /* synthetic */ AnonymousClass33Z A06;

    public ViewTreeObserver$OnPreDrawListenerC66513Nw(View view, AbstractC35501i8 r2, AnonymousClass33Z r3, int i, int i2, int i3, int i4) {
        this.A06 = r3;
        this.A04 = view;
        this.A01 = i;
        this.A02 = i2;
        this.A03 = i3;
        this.A00 = i4;
        this.A05 = r2;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        View view = this.A04;
        C12980iv.A1G(view, this);
        int[] A07 = C13000ix.A07();
        view.getLocationOnScreen(A07);
        AnonymousClass33Z r4 = this.A06;
        r4.A02 = this.A01 - A07[0];
        r4.A04 = this.A02 - A07[1];
        float f = (float) this.A03;
        r4.A01 = f / C12990iw.A02(view);
        float f2 = (float) this.A00;
        float A03 = f2 / C12990iw.A03(view);
        r4.A00 = A03;
        float f3 = r4.A01;
        if (f3 < A03) {
            r4.A01 = A03;
            r4.A02 = (int) (((float) r4.A02) - (((C12990iw.A02(view) * r4.A01) - f) / 2.0f));
        } else {
            r4.A00 = f3;
            r4.A04 = (int) (((float) r4.A04) - (((C12990iw.A03(view) * r4.A00) - f2) / 2.0f));
        }
        AbstractC35501i8 r8 = this.A05;
        MediaViewBaseFragment mediaViewBaseFragment = r4.A06;
        r4.A03 = mediaViewBaseFragment.A02().getConfiguration().orientation;
        Drawable drawable = r4.A05;
        int[] A072 = C13000ix.A07();
        // fill-array-data instruction
        A072[0] = 0;
        A072[1] = 255;
        ObjectAnimator ofInt = ObjectAnimator.ofInt(drawable, "alpha", A072);
        ofInt.setDuration(220L);
        ofInt.setInterpolator(new AccelerateInterpolator());
        ofInt.start();
        AnonymousClass443 r9 = mediaViewBaseFragment.A09;
        r9.setPivotX(0.0f);
        r9.setPivotY(0.0f);
        r9.setScaleX(r4.A01);
        r9.setScaleY(r4.A00);
        r9.setTranslationX((float) r4.A02);
        r9.setTranslationY((float) r4.A04);
        View findViewWithTag = mediaViewBaseFragment.A09.findViewWithTag(mediaViewBaseFragment.A1A());
        if (findViewWithTag != null) {
            findViewWithTag.setAlpha(0.0f);
            findViewWithTag.animate().setDuration(110).alpha(1.0f).setInterpolator(new DecelerateInterpolator(2.0f));
        }
        C12970iu.A0I(r9, 220).setListener(new AnonymousClass2YK(r8, r4));
        return true;
    }
}
