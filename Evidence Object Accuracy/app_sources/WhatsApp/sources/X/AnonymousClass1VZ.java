package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.jid.UserJid;

/* renamed from: X.1VZ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1VZ extends UserJid implements Parcelable {
    public static final AnonymousClass1VZ A00;
    public static final Parcelable.Creator CREATOR = new C30011Vp();

    @Override // com.whatsapp.jid.UserJid, com.whatsapp.jid.Jid
    public String getServer() {
        return "s.whatsapp.net";
    }

    @Override // com.whatsapp.jid.UserJid, com.whatsapp.jid.Jid
    public int getType() {
        return 7;
    }

    static {
        try {
            A00 = new AnonymousClass1VZ();
        } catch (AnonymousClass1MW e) {
            throw new IllegalStateException(e);
        }
    }

    public AnonymousClass1VZ() {
        super("0");
    }

    public AnonymousClass1VZ(Parcel parcel) {
        super(parcel);
    }

    @Override // com.whatsapp.jid.UserJid, com.whatsapp.jid.Jid
    public String getObfuscatedString() {
        return getRawString();
    }
}
