package X;

import com.whatsapp.util.Log;

/* renamed from: X.6AU  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6AU implements AnonymousClass6MQ {
    public final /* synthetic */ AnonymousClass1ZR A00;
    public final /* synthetic */ AnonymousClass1FK A01;
    public final /* synthetic */ C120515gJ A02;
    public final /* synthetic */ String A03;
    public final /* synthetic */ boolean A04 = true;

    public AnonymousClass6AU(AnonymousClass1ZR r2, AnonymousClass1FK r3, C120515gJ r4, String str) {
        this.A02 = r4;
        this.A00 = r2;
        this.A03 = str;
        this.A01 = r3;
    }

    @Override // X.AnonymousClass6MQ
    public void AOb(C119705ey r8) {
        C120515gJ r0 = this.A02;
        AnonymousClass1ZR r1 = r8.A02;
        AnonymousClass009.A05(r1);
        String str = r8.A03;
        r0.A01(r1, this.A00, this.A01, str, this.A03, this.A04);
    }

    @Override // X.AnonymousClass6MQ
    public void APo(C452120p r2) {
        Log.w("PAY: IndiaUpiPaymentMethodAction: could not fetch VPA information to set default payment method");
        this.A01.AV3(r2);
    }
}
