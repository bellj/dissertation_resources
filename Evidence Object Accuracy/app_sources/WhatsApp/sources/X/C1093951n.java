package X;

/* renamed from: X.51n  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C1093951n implements AnonymousClass5T6 {
    public final AnonymousClass5T6 A00;

    public /* synthetic */ C1093951n(AnonymousClass5T6 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5T6
    public boolean A65(AnonymousClass4RG r2) {
        return this.A00.A65(r2);
    }

    public String toString() {
        StringBuilder A0k;
        String str;
        String obj = this.A00.toString();
        if (obj.startsWith("(")) {
            A0k = C12960it.A0k("[?");
            A0k.append(obj);
            str = "]";
        } else {
            A0k = C12960it.A0k("[?(");
            A0k.append(obj);
            str = ")]";
        }
        return C12960it.A0d(str, A0k);
    }
}
