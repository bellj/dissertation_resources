package X;

/* renamed from: X.4Hb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C88774Hb {
    public static final C78603pB A00;
    public static final C78603pB A01;
    public static final C78603pB A02;
    public static final C78603pB A03;
    public static final C78603pB[] A04;

    static {
        C78603pB r6 = new C78603pB("wearable_services", 1);
        A00 = r6;
        C78603pB r5 = new C78603pB("carrier_auth", 1);
        A01 = r5;
        C78603pB r2 = new C78603pB("wear3_oem_companion", 1);
        A02 = r2;
        C78603pB r1 = new C78603pB("wear_fast_pair_account_key_sync", 1);
        A03 = r1;
        C78603pB[] r0 = new C78603pB[4];
        C72453ed.A1F(r6, r5, r2, r1, r0);
        A04 = r0;
    }
}
