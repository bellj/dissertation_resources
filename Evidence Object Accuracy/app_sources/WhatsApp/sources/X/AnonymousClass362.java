package X;

import android.text.Editable;
import android.text.TextUtils;
import com.google.android.material.chip.Chip;
import com.whatsapp.jid.UserJid;
import com.whatsapp.search.SearchViewModel;
import com.whatsapp.search.views.TokenizedSearchInput;
import com.whatsapp.text.FinalBackspaceAwareEntry;
import java.util.List;

/* renamed from: X.362  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass362 extends C469928m {
    public final /* synthetic */ FinalBackspaceAwareEntry A00;

    public AnonymousClass362(FinalBackspaceAwareEntry finalBackspaceAwareEntry) {
        this.A00 = finalBackspaceAwareEntry;
    }

    @Override // X.C469928m, android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        Chip chip;
        if (editable != null) {
            FinalBackspaceAwareEntry finalBackspaceAwareEntry = this.A00;
            String obj = editable.toString();
            if ((TextUtils.isEmpty(obj) || obj.charAt(0) != FinalBackspaceAwareEntry.A04) && !finalBackspaceAwareEntry.A02) {
                List<AnonymousClass4L6> list = finalBackspaceAwareEntry.A01;
                if (list != null) {
                    for (AnonymousClass4L6 r0 : list) {
                        TokenizedSearchInput tokenizedSearchInput = r0.A00;
                        SearchViewModel searchViewModel = tokenizedSearchInput.A0C;
                        if (searchViewModel != null) {
                            UserJid A08 = searchViewModel.A08();
                            int A05 = tokenizedSearchInput.A0C.A05();
                            int i = tokenizedSearchInput.A00;
                            if (i == 1) {
                                chip = tokenizedSearchInput.A0R;
                            } else if (i == 2) {
                                chip = tokenizedSearchInput.A0P;
                            } else if (i == 3) {
                                chip = tokenizedSearchInput.A0Q;
                            } else if (i == 0) {
                                if (A08 != null) {
                                    tokenizedSearchInput.setFocus(2);
                                } else if (A05 != 0) {
                                    tokenizedSearchInput.setFocus(1);
                                } else if (tokenizedSearchInput.A0C.A0B() != null) {
                                    tokenizedSearchInput.setFocus(3);
                                }
                            }
                            TokenizedSearchInput.A00(chip, tokenizedSearchInput);
                        }
                    }
                }
                finalBackspaceAwareEntry.A0A(editable);
            }
            List<AnonymousClass4L6> list2 = finalBackspaceAwareEntry.A01;
            if (list2 != null) {
                for (AnonymousClass4L6 r2 : list2) {
                    String obj2 = finalBackspaceAwareEntry.A08(editable).toString();
                    SearchViewModel searchViewModel2 = r2.A00.A0C;
                    if (!(searchViewModel2 == null || obj2 == null)) {
                        searchViewModel2.A0V(obj2);
                    }
                }
            }
        }
    }
}
