package X;

/* renamed from: X.67W  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass67W implements AnonymousClass2QI {
    public final byte[] A00;

    public AnonymousClass67W(byte[] bArr) {
        this.A00 = bArr;
    }

    @Override // X.AnonymousClass2QI
    public C37891nB A8r(byte[] bArr) {
        byte[][] A06 = C16050oM.A06(C32891cu.A01(bArr, this.A00, "walibra_hkdf_info".getBytes(), 44), 32, 12);
        return new C37891nB(A06[0], C117305Zk.A1a(32), A06[1]);
    }
}
