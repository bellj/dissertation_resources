package X;

import android.os.Build;

/* renamed from: X.14H  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass14H {
    public AbstractC116735Wp A00() {
        if (Build.VERSION.SDK_INT > 18) {
            return new C69123Ya();
        }
        return new C69133Yb();
    }
}
