package X;

import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CaptureRequest;
import java.util.List;

/* renamed from: X.60y  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1310360y {
    public final CameraCaptureSession A00;

    public C1310360y(CameraCaptureSession cameraCaptureSession) {
        this.A00 = cameraCaptureSession;
    }

    public static void A01(CameraDevice cameraDevice, AnonymousClass66N r3, List list) {
        cameraDevice.createCaptureSession(list, new C117425Zw(r3), null);
    }

    public void A02() {
        this.A00.abortCaptures();
    }

    public void A03() {
        this.A00.close();
    }

    public void A04(CaptureRequest captureRequest, AbstractC136416Ml r5) {
        C117415Zv r0;
        CameraCaptureSession cameraCaptureSession = this.A00;
        if (r5 != null) {
            r0 = new C117415Zv(this, r5);
        } else {
            r0 = null;
        }
        cameraCaptureSession.capture(captureRequest, r0, null);
    }

    public void A05(CaptureRequest captureRequest, AbstractC136416Ml r5) {
        C117415Zv r0;
        CameraCaptureSession cameraCaptureSession = this.A00;
        if (r5 != null) {
            r0 = new C117415Zv(this, r5);
        } else {
            r0 = null;
        }
        cameraCaptureSession.setRepeatingRequest(captureRequest, r0, null);
    }
}
