package X;

import com.facebook.redex.RunnableBRunnable0Shape4S0200000_I0_4;
import com.whatsapp.util.Log;
import java.util.List;

/* renamed from: X.1QD  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1QD implements AnonymousClass1FK {
    public final /* synthetic */ C15650ng A00;
    public final /* synthetic */ AnonymousClass1QA A01;
    public final /* synthetic */ AbstractC16390ow A02;
    public final /* synthetic */ AbstractC14440lR A03;

    public AnonymousClass1QD(C15650ng r1, AnonymousClass1QA r2, AbstractC16390ow r3, AbstractC14440lR r4) {
        this.A01 = r2;
        this.A03 = r4;
        this.A00 = r1;
        this.A02 = r3;
    }

    @Override // X.AnonymousClass1FK
    public void AV3(C452120p r3) {
        StringBuilder sb = new StringBuilder("PAY: InteractiveMessageCheckoutInfoConverter verifyFromServer/onRequestError. paymentNetworkError: ");
        sb.append(r3);
        Log.e(sb.toString());
        this.A01.AWX();
    }

    @Override // X.AnonymousClass1FK
    public void AVA(C452120p r3) {
        StringBuilder sb = new StringBuilder("PAY: InteractiveMessageCheckoutInfoConverter verifyFromServer/onResponseError. paymentNetworkError: ");
        sb.append(r3);
        Log.e(sb.toString());
        this.A01.AWX();
    }

    @Override // X.AnonymousClass1FK
    public void AVB(C452220q r6) {
        if (r6 instanceof AnonymousClass46P) {
            List list = ((AnonymousClass46P) r6).A01;
            if (list != null && list.size() > 0) {
                int i = ((AnonymousClass1IR) list.get(0)).A02;
                if (!AnonymousClass1QC.A01.contains(Integer.valueOf(i)) && !AnonymousClass1QC.A00.contains(Integer.valueOf(i))) {
                    this.A03.Ab2(new RunnableBRunnable0Shape4S0200000_I0_4(this.A02, 47, this.A00));
                    this.A01.AWZ();
                    return;
                }
            } else {
                return;
            }
        }
        this.A01.AWX();
    }
}
