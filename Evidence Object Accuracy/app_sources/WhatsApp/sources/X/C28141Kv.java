package X;

import java.text.NumberFormat;

/* renamed from: X.1Kv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C28141Kv {
    public boolean A00;
    public final AnonymousClass02S A01;
    public final AnonymousClass1MZ A02;
    public final C27261Gq A03;
    public final NumberFormat A04;
    public final NumberFormat A05;
    public final boolean A06;
    public final boolean A07;

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002e, code lost:
        if (r2.equals(r2) == false) goto L_0x0030;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C28141Kv(android.content.Context r6, java.util.Locale r7) {
        /*
            r5 = this;
            r5.<init>()
            int r0 = X.AnonymousClass069.A00(r7)
            r3 = 1
            if (r0 == r3) goto L_0x000b
            r3 = 0
        L_0x000b:
            r5.A06 = r3
            java.lang.String r1 = r7.getLanguage()
            java.lang.String r0 = "en"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0030
            java.lang.String r4 = r7.getCountry()
            java.lang.String r2 = "en-US"
            r1 = r2
            X.1NC r0 = X.AnonymousClass1Tf.A00     // Catch: IllegalArgumentException -> 0x0029
            java.lang.Object r0 = r0.A01(r4)     // Catch: IllegalArgumentException -> 0x0029
            if (r0 == 0) goto L_0x0029
            r2 = r0
        L_0x0029:
            boolean r1 = r1.equals(r2)
            r0 = 1
            if (r1 != 0) goto L_0x0031
        L_0x0030:
            r0 = 0
        L_0x0031:
            r5.A07 = r0
            int r1 = android.os.Build.VERSION.SDK_INT
            r2 = 1
            r0 = 21
            if (r1 < r0) goto L_0x004a
            java.lang.String r1 = r7.getLanguage()
            java.lang.String r0 = "sr"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x004a
            r7.getScript()
        L_0x004a:
            int r0 = X.AbstractC27291Gt.A00(r7)
            if (r0 == 0) goto L_0x0057
            boolean r0 = android.text.format.DateFormat.is24HourFormat(r6)
            if (r0 != 0) goto L_0x0057
            r2 = 0
        L_0x0057:
            r5.A00 = r2
            X.06A r0 = new X.06A
            r0.<init>(r3)
            X.02S r0 = r0.A00()
            r5.A01 = r0
            java.text.NumberFormat r0 = java.text.NumberFormat.getPercentInstance(r7)
            r5.A05 = r0
            java.text.NumberFormat r0 = java.text.NumberFormat.getInstance(r7)
            r5.A04 = r0
            android.content.res.Resources r1 = r6.getResources()
            X.1MZ r0 = new X.1MZ
            r0.<init>(r6, r1, r7)
            r5.A02 = r0
            android.content.res.Resources r3 = r6.getResources()
            java.util.Locale r2 = r0.A02
            X.1Te r1 = r0.A01
            X.1Gq r0 = new X.1Gq
            r0.<init>(r6, r3, r1, r2)
            r5.A03 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28141Kv.<init>(android.content.Context, java.util.Locale):void");
    }

    public static boolean A00(AnonymousClass018 r0) {
        return r0.A04().A06;
    }

    public static boolean A01(AnonymousClass018 r0) {
        return !r0.A04().A06;
    }
}
