package X;

import android.os.SystemClock;
import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;
import com.whatsapp.util.Log;

/* renamed from: X.27s  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C468027s extends AnonymousClass015 {
    public int A00;
    public long A01;
    public AbstractC15710nm A02;
    public C15570nT A03 = this.A03;
    public C22640zP A04;
    public AnonymousClass1CS A05;
    public C15550nR A06;
    public C15610nY A07;
    public C21270x9 A08;
    public C18640sm A09;
    public C14830m7 A0A;
    public C14820m6 A0B;
    public AnonymousClass018 A0C;
    public C19990v2 A0D;
    public C21320xE A0E;
    public C15600nX A0F;
    public AnonymousClass15K A0G;
    public C16120oU A0H;
    public C20710wC A0I;
    public C15580nU A0J;
    public C17220qS A0K;
    public C20660w7 A0L;
    public AbstractC14440lR A0M;
    public boolean A0N;
    public final AnonymousClass016 A0O = new AnonymousClass016();
    public final AnonymousClass016 A0P = new AnonymousClass016();
    public final AnonymousClass016 A0Q = new AnonymousClass016();
    public final AnonymousClass016 A0R = new AnonymousClass016();
    public final C27151Gf A0S;
    public final C15580nU A0T;
    public final C36161jQ A0U = new C36161jQ(0);
    public final C36161jQ A0V = new C36161jQ(0);
    public final C36161jQ A0W = new C36161jQ(-1);
    public final String A0X;
    public final boolean A0Y;

    public C468027s(C22640zP r4, AnonymousClass1CS r5, C15550nR r6, C15610nY r7, C18640sm r8, C14830m7 r9, AnonymousClass018 r10, C21320xE r11, C15600nX r12, C20710wC r13, C15580nU r14, C15580nU r15, C20660w7 r16, String str, int i, boolean z) {
        AnonymousClass27r r1 = new AnonymousClass27r(this);
        this.A0S = r1;
        this.A0A = r9;
        this.A0L = r16;
        this.A06 = r6;
        this.A07 = r7;
        this.A0C = r10;
        this.A0I = r13;
        this.A04 = r4;
        this.A0E = r11;
        this.A05 = r5;
        this.A0F = r12;
        this.A09 = r8;
        r11.A03(r1);
        this.A00 = i;
        this.A0T = r14;
        this.A0J = r15;
        this.A0X = str;
        this.A0Y = z;
    }

    @Override // X.AnonymousClass015
    public void A03() {
        this.A0E.A04(this.A0S);
    }

    public final void A04() {
        SystemClock.sleep(Math.max(0L, 300 - (System.currentTimeMillis() - this.A01)));
        A05(4);
        int i = this.A00;
        if ((i == 4 || i == 3) && this.A0T != null) {
            this.A0M.Ab2(new RunnableBRunnable0Shape4S0100000_I0_4(this, 1));
        }
    }

    public void A05(int i) {
        this.A0U.A0A(Integer.valueOf(i));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x001c, code lost:
        if (r2 == 2) goto L_0x001e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A06(X.C63373Bi r6) {
        /*
            r5 = this;
            int r2 = r5.A00
            r1 = 1
            r0 = 2
            if (r2 == r0) goto L_0x000e
            if (r2 == r1) goto L_0x000e
            if (r2 == 0) goto L_0x000e
            r0 = 5
            if (r2 == r0) goto L_0x000e
            r1 = 0
        L_0x000e:
            r3 = 3
            r4 = 2
            if (r1 == 0) goto L_0x001c
            int r1 = r6.A00
            r2 = 1
            if (r1 == r4) goto L_0x001a
            if (r1 != r3) goto L_0x005a
            r2 = 5
        L_0x001a:
            r5.A00 = r2
        L_0x001c:
            if (r2 != r4) goto L_0x0048
        L_0x001e:
            X.0zP r1 = r5.A04
            X.0nU r0 = r6.A03
            java.util.List r0 = r1.A01(r0)
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x0048
            X.0m6 r0 = r5.A0B
            android.content.SharedPreferences r2 = r0.A00
            java.lang.String r1 = "about_community_nux"
            r0 = 0
            boolean r0 = r2.getBoolean(r1, r0)
            X.1jQ r1 = r5.A0V
            if (r0 == 0) goto L_0x0043
            java.lang.Integer r0 = java.lang.Integer.valueOf(r4)
        L_0x003f:
            r1.A0A(r0)
            return
        L_0x0043:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r3)
            goto L_0x003f
        L_0x0048:
            X.0nX r1 = r5.A0F
            X.0nU r0 = r6.A03
            boolean r0 = r1.A0C(r0)
            if (r0 == 0) goto L_0x0069
            X.1jQ r1 = r5.A0V
            r0 = 4
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            goto L_0x003f
        L_0x005a:
            X.0zP r0 = r5.A04
            boolean r0 = r0.A07()
            if (r0 == 0) goto L_0x0067
            if (r1 != r2) goto L_0x0067
            r5.A00 = r4
            goto L_0x001e
        L_0x0067:
            r2 = 0
            goto L_0x001a
        L_0x0069:
            X.016 r2 = r5.A0R
            r1 = -1
            X.4Ws r0 = new X.4Ws
            r0.<init>(r6, r1)
            r2.A0A(r0)
            X.1PD r0 = r6.A05
            java.lang.String r3 = r0.A02
            boolean r0 = android.text.TextUtils.isEmpty(r3)
            if (r0 == 0) goto L_0x009a
            int r1 = r5.A00
            r0 = 3
            if (r1 == r0) goto L_0x0086
            r0 = 5
            if (r1 != r0) goto L_0x0096
        L_0x0086:
            r3 = 0
            r0 = 0
        L_0x0088:
            X.016 r2 = r5.A0P
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            android.util.Pair r0 = new android.util.Pair
            r0.<init>(r1, r3)
            r2.A0A(r0)
        L_0x0096:
            r5.A05(r4)
            return
        L_0x009a:
            r0 = 2
            goto L_0x0088
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C468027s.A06(X.3Bi):void");
    }

    public void A07(boolean z) {
        if (!z && ((Number) this.A0U.A01()).intValue() != 0) {
            return;
        }
        if (!this.A09.A0B()) {
            this.A0R.A0A(new C92634Ws(null, 499));
            A05(5);
            return;
        }
        A05(1);
        int i = this.A00;
        if (i == 4 || i == 3) {
            C15580nU r7 = this.A0T;
            AnonymousClass009.A05(r7);
            C15580nU r6 = this.A0J;
            AnonymousClass009.A05(r6);
            AnonymousClass1CS r5 = this.A05;
            new C63773Cw(r5.A00, r7, r5.A02, new AnonymousClass3ZU(new AnonymousClass024() { // from class: X.4rb
                @Override // X.AnonymousClass024
                public final void accept(Object obj) {
                    C468027s.this.A06((C63373Bi) obj);
                }
            }, new AnonymousClass4KF(this), r5, r6)).A00(r6);
        } else if (i == 2 || i == 1 || i == 0 || i == 5) {
            String str = this.A0X;
            AnonymousClass009.A05(str);
            this.A0L.A03(new C69343Yw(this), str);
        } else {
            Log.e("JoinLinkedSubGroupViewModelloadGroupInfo/no data to load group info from");
            AnonymousClass009.A07("JoinLinkedSubGroupViewModelloadGroupInfo/no data to load group info from");
        }
    }
}
