package X;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

/* renamed from: X.4ba  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C94304ba {
    public static final /* synthetic */ AtomicReferenceFieldUpdater A00 = AtomicReferenceFieldUpdater.newUpdater(C94304ba.class, Object.class, "_cur");
    public volatile /* synthetic */ Object _cur = new C94454bq(8, false);

    public final int A00() {
        long j = ((C94454bq) this._cur)._state;
        return 1073741823 & (((int) ((j & 1152921503533105152L) >> 30)) - ((int) ((1073741823 & j) >> 0)));
    }

    public final Object A01() {
        while (true) {
            C94454bq r2 = (C94454bq) this._cur;
            Object A01 = r2.A01();
            if (A01 != C94454bq.A04) {
                return A01;
            }
            AnonymousClass0KN.A00(this, r2, r2.A02(), A00);
        }
    }

    public final boolean A02(Object obj) {
        while (true) {
            C94454bq r2 = (C94454bq) this._cur;
            int A002 = r2.A00(obj);
            if (A002 == 0) {
                return true;
            }
            if (A002 == 1) {
                AnonymousClass0KN.A00(this, r2, r2.A02(), A00);
            } else if (A002 == 2) {
                return false;
            }
        }
    }
}
