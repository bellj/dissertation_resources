package X;

import java.util.concurrent.TimeUnit;

/* renamed from: X.1Q5  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1Q5 {
    public long A00;
    public C41791uA A01;
    public boolean A02;
    public final long A03;
    public final C14830m7 A04;
    public final C16120oU A05;
    public final AnonymousClass1Q6 A06;
    public final C21230x5 A07;
    public final AbstractC21180x0 A08;
    public final String A09;

    public AnonymousClass1Q5(C14830m7 r8, C16120oU r9, C21230x5 r10, AbstractC21180x0 r11, String str, int i) {
        this(r8, r9, new AnonymousClass1Q6(i), r10, r11, str);
    }

    public AnonymousClass1Q5(C14830m7 r3, C16120oU r4, AnonymousClass1Q6 r5, C21230x5 r6, AbstractC21180x0 r7, String str) {
        this.A00 = -1;
        this.A02 = false;
        this.A08 = r7;
        this.A09 = str;
        this.A04 = r3;
        this.A05 = r4;
        this.A06 = r5;
        this.A03 = System.nanoTime();
        this.A07 = r6;
    }

    public final void A00() {
        C41791uA r8 = this.A01;
        if (r8 != null && !this.A02) {
            r8.A02 = Integer.valueOf(this.A06.A00);
            TimeUnit timeUnit = TimeUnit.MILLISECONDS;
            long nanoTime = System.nanoTime();
            long j = this.A00;
            if (j <= 0) {
                j = this.A03;
            }
            r8.A05 = Long.valueOf(timeUnit.convert(nanoTime - j, TimeUnit.NANOSECONDS));
            this.A05.A07(this.A01);
            this.A02 = true;
        }
    }

    public void A01(int i, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("_end");
        A03(i, sb.toString());
    }

    public void A02(int i, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("_start");
        A03(i, sb.toString());
    }

    public void A03(int i, String str) {
        AnonymousClass1Q6 r2 = this.A06;
        if (r2.A00()) {
            this.A08.ALC(r2.A05, str, i);
        }
    }

    public void A04(int i, String str, boolean z) {
        AnonymousClass1Q6 r3 = this.A06;
        boolean A00 = r3.A00();
        if (A00 || r3.A04) {
            this.A00 = -1;
            if (r3.A04) {
                this.A01 = new C41791uA();
            }
            if (A00) {
                C21230x5 r2 = this.A07;
                int i2 = r3.A05;
                boolean z2 = r3.A02;
                String str2 = "Censored";
                if (!r2.A00()) {
                    str2 = str;
                } else if (!z2) {
                    return;
                }
                r2.A01.ALH("perf_origin", str2, i2, i, z);
            }
        }
    }

    public void A05(int i, short s) {
        AnonymousClass1Q6 r2 = this.A06;
        if (r2.A00()) {
            this.A08.AL6(r2.A05, i, s);
        }
        if (r2.A04) {
            A00();
        }
    }

    public void A06(long j) {
        AbstractC21180x0 r5 = this.A08;
        int i = this.A06.A05;
        StringBuilder sb = new StringBuilder("launch_2_");
        String str = this.A09;
        sb.append(str);
        sb.append("_start");
        String obj = sb.toString();
        TimeUnit timeUnit = TimeUnit.NANOSECONDS;
        r5.ALD(obj, timeUnit, i, j);
        StringBuilder sb2 = new StringBuilder();
        sb2.append("launch_2_");
        sb2.append(str);
        sb2.append("_end");
        String obj2 = sb2.toString();
        long j2 = this.A03;
        r5.ALD(obj2, timeUnit, i, j2);
        StringBuilder sb3 = new StringBuilder("init_2_");
        sb3.append(str);
        sb3.append("_start");
        r5.ALD(sb3.toString(), timeUnit, i, j2);
        StringBuilder sb4 = new StringBuilder();
        sb4.append("init_2_");
        sb4.append(str);
        A07(sb4.toString());
    }

    public void A07(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("_end");
        A09(sb.toString());
    }

    public void A08(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("_start");
        A09(sb.toString());
    }

    public void A09(String str) {
        AnonymousClass1Q6 r2 = this.A06;
        if (r2.A00()) {
            this.A08.ALB(r2.A05, str);
        }
    }

    public void A0A(String str, String str2, boolean z) {
        if (z) {
            this.A08.AKw(this.A06.A05, str, str2);
        } else {
            this.A07.AKw(this.A06.A05, str, str2);
        }
    }

    public void A0B(String str, boolean z, boolean z2) {
        if (z2) {
            this.A08.AKx(this.A06.A05, str, z);
        } else {
            this.A07.AKx(this.A06.A05, str, z);
        }
    }

    public void A0C(short s) {
        AnonymousClass1Q6 r2 = this.A06;
        if (r2.A00()) {
            this.A08.AL7(r2.A05, s);
        }
        if (r2.A04) {
            A00();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0036, code lost:
        if (r1 != false) goto L_0x0038;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0D(java.lang.String r10, long r11) {
        /*
            r9 = this;
            X.1Q6 r3 = r9.A06
            boolean r1 = r3.A00()
            if (r1 != 0) goto L_0x000e
            boolean r0 = r3.A04
            if (r0 != 0) goto L_0x000e
            r0 = 0
            return r0
        L_0x000e:
            r7 = r11
            r9.A00 = r11
            boolean r0 = r3.A04
            if (r0 == 0) goto L_0x001c
            X.1uA r0 = new X.1uA
            r0.<init>()
            r9.A01 = r0
        L_0x001c:
            if (r1 == 0) goto L_0x0040
            r1 = 0
            int r0 = (r11 > r1 ? 1 : (r11 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x0044
            X.0x5 r2 = r9.A07
            int r6 = r3.A05
            java.util.concurrent.TimeUnit r5 = java.util.concurrent.TimeUnit.NANOSECONDS
            boolean r1 = r3.A02
            java.lang.String r3 = "perf_origin"
            java.lang.String r4 = "Censored"
            boolean r0 = r2.A00()
            if (r0 == 0) goto L_0x0042
            if (r1 == 0) goto L_0x003d
        L_0x0038:
            X.0x0 r2 = r2.A01
            r2.ALI(r3, r4, r5, r6, r7)
        L_0x003d:
            r9.A06(r11)
        L_0x0040:
            r0 = 1
            return r0
        L_0x0042:
            r4 = r10
            goto L_0x0038
        L_0x0044:
            X.0x5 r5 = r9.A07
            int r4 = r3.A05
            boolean r3 = r3.A02
            java.lang.String r2 = "perf_origin"
            java.lang.String r1 = "Censored"
            boolean r0 = r5.A00()
            if (r0 == 0) goto L_0x0057
            if (r3 == 0) goto L_0x0040
            r10 = r1
        L_0x0057:
            X.0x0 r0 = r5.A01
            r0.ALG(r4, r2, r10)
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1Q5.A0D(java.lang.String, long):boolean");
    }
}
