package X;

import android.content.Context;
import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.jid.Jid;
import com.whatsapp.util.Log;

/* renamed from: X.3J0  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3J0 {
    public static AbstractC14640lm A00(C15570nT r3, C15370n3 r4, AbstractC15340mz r5) {
        if (r5 == null || r5.A0C == 6 || (!r4.A0K() && !C15380n4.A0F(r4.A0D))) {
            return null;
        }
        if (r5.A0z.A02) {
            r3.A08();
            return r3.A05;
        }
        AbstractC14640lm A0B = r5.A0B();
        if (A0B != null) {
            return A0B;
        }
        Log.e(C12960it.A0d(C30041Vv.A0C(r5), C12960it.A0k("conversations_row/missing_rmt_src:")));
        return null;
    }

    public static CharSequence A01(Context context, C15570nT r3, C15610nY r4, AnonymousClass018 r5, C15370n3 r6, boolean z) {
        String string;
        Jid jid;
        if (z) {
            string = context.getString(R.string.group_subject_changed_you_pronoun);
        } else if (r6 == null || (jid = r6.A0D) == null || r3.A0F(jid)) {
            string = "";
        } else {
            string = C15610nY.A01(r4, r6);
        }
        if (TextUtils.isEmpty(string)) {
            return "";
        }
        StringBuilder A0h = C12960it.A0h();
        A0h.append((Object) string);
        String A0d = C12960it.A0d(": ", A0h);
        CharSequence[] charSequenceArr = new CharSequence[3];
        char c = 8207;
        if (C28141Kv.A01(r5)) {
            c = 8206;
        }
        String valueOf = String.valueOf(c);
        charSequenceArr[0] = valueOf;
        charSequenceArr[1] = A0d;
        charSequenceArr[2] = valueOf;
        return TextUtils.concat(charSequenceArr);
    }

    public static CharSequence A02(CharSequence charSequence, CharSequence charSequence2) {
        if (TextUtils.isEmpty(charSequence2)) {
            return charSequence;
        }
        boolean A0G = C42941w9.A0G(charSequence2);
        CharSequence[] charSequenceArr = new CharSequence[4];
        charSequenceArr[0] = charSequence;
        char c = 8207;
        if (A0G) {
            c = 8206;
        }
        String valueOf = String.valueOf(c);
        charSequenceArr[1] = valueOf;
        charSequenceArr[2] = charSequence2;
        charSequenceArr[3] = valueOf;
        return TextUtils.concat(charSequenceArr);
    }
}
