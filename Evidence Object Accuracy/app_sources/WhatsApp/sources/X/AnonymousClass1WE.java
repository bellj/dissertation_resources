package X;

/* renamed from: X.1WE  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1WE {
    public String A00;
    public final long A01;
    public final Long A02;
    public final String A03;
    public final String A04;
    public final String A05;
    public final String A06;
    public final String A07;
    public final String A08;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof AnonymousClass1WE) {
                AnonymousClass1WE r8 = (AnonymousClass1WE) obj;
                if (!C16700pc.A0O(this.A08, r8.A08) || !C16700pc.A0O(this.A05, r8.A05) || !C16700pc.A0O(this.A03, r8.A03) || this.A01 != r8.A01 || !C16700pc.A0O(this.A00, r8.A00) || !C16700pc.A0O(this.A06, r8.A06) || !C16700pc.A0O(this.A04, r8.A04) || !C16700pc.A0O(this.A02, r8.A02) || !C16700pc.A0O(this.A07, r8.A07)) {
                }
            }
            return false;
        }
        return true;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("BloksLinks(url=");
        sb.append(this.A08);
        sb.append(", locale=");
        sb.append(this.A05);
        sb.append(", appId=");
        sb.append(this.A03);
        sb.append(", expiresData=");
        sb.append(this.A01);
        sb.append(", version=");
        sb.append((Object) this.A00);
        sb.append(", platform=");
        sb.append(this.A06);
        sb.append(", bizJid=");
        sb.append((Object) this.A04);
        sb.append(", flowVersionId=");
        sb.append(this.A02);
        sb.append(", signature=");
        sb.append((Object) this.A07);
        sb.append(')');
        return sb.toString();
    }

    public AnonymousClass1WE(Long l, String str, String str2, String str3, String str4, String str5, String str6, String str7, long j) {
        this.A08 = str;
        this.A05 = str2;
        this.A03 = str3;
        this.A01 = j;
        this.A00 = str4;
        this.A06 = str5;
        this.A04 = str6;
        this.A02 = l;
        this.A07 = str7;
    }

    public int hashCode() {
        long j = this.A01;
        int hashCode = ((((((this.A08.hashCode() * 31) + this.A05.hashCode()) * 31) + this.A03.hashCode()) * 31) + ((int) (j ^ (j >>> 32)))) * 31;
        String str = this.A00;
        int i = 0;
        int hashCode2 = (((hashCode + (str == null ? 0 : str.hashCode())) * 31) + this.A06.hashCode()) * 31;
        String str2 = this.A04;
        int hashCode3 = (hashCode2 + (str2 == null ? 0 : str2.hashCode())) * 31;
        Long l = this.A02;
        int hashCode4 = (hashCode3 + (l == null ? 0 : l.hashCode())) * 31;
        String str3 = this.A07;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return hashCode4 + i;
    }
}
