package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.4Ae  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class EnumC87094Ae extends Enum {
    public static final /* synthetic */ EnumC87094Ae[] A00;
    public static final EnumC87094Ae A01;
    public static final EnumC87094Ae A02;
    public static final EnumC87094Ae A03;
    public static final EnumC87094Ae A04;
    public static final EnumC87094Ae A05;

    public static EnumC87094Ae valueOf(String str) {
        return (EnumC87094Ae) Enum.valueOf(EnumC87094Ae.class, str);
    }

    public static EnumC87094Ae[] values() {
        return (EnumC87094Ae[]) A00.clone();
    }

    static {
        EnumC87094Ae r6 = new EnumC87094Ae("CPU_ACQUIRED", 0);
        A02 = r6;
        EnumC87094Ae r5 = new EnumC87094Ae("BLOCKING", 1);
        A01 = r5;
        EnumC87094Ae r4 = new EnumC87094Ae("PARKING", 2);
        A04 = r4;
        EnumC87094Ae r3 = new EnumC87094Ae("DORMANT", 3);
        A03 = r3;
        EnumC87094Ae r1 = new EnumC87094Ae("TERMINATED", 4);
        A05 = r1;
        EnumC87094Ae[] r0 = new EnumC87094Ae[5];
        C72453ed.A1F(r6, r5, r4, r3, r0);
        r0[4] = r1;
        A00 = r0;
    }

    public EnumC87094Ae(String str, int i) {
    }
}
