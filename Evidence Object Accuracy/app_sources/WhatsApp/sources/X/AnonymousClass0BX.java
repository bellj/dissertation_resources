package X;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

/* renamed from: X.0BX  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0BX extends FrameLayout {
    public ViewGroup A00;
    public AnonymousClass0Bb A01;
    public final EnumC03910Jp A02;

    public AnonymousClass0BX(Context context, EnumC03910Jp r5) {
        super(context);
        this.A02 = r5;
        Context context2 = getContext();
        this.A00 = new FrameLayout(context2);
        AnonymousClass0Bb r1 = new AnonymousClass0Bb(context2);
        this.A01 = r1;
        r1.setImportantForAccessibility(1);
        addView(this.A01);
        addView(this.A00);
    }

    public AnonymousClass0Bb getContentPager() {
        return this.A01;
    }

    public ViewGroup getHeaderContainer() {
        return this.A00;
    }

    @Override // android.widget.FrameLayout, android.view.View, android.view.ViewGroup
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        this.A01.layout(0, 0, getMeasuredWidth(), getMeasuredHeight());
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(getMeasuredWidth()), 1073741824);
        int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(getMeasuredHeight()), Integer.MIN_VALUE);
        ViewGroup viewGroup = this.A00;
        viewGroup.measure(makeMeasureSpec, makeMeasureSpec2);
        viewGroup.layout(0, 0, viewGroup.getMeasuredWidth(), viewGroup.getMeasuredHeight());
    }

    @Override // android.widget.FrameLayout, android.view.View
    public void onMeasure(int i, int i2) {
        int i3 = 1073741824;
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(i), 1073741824);
        int size = View.MeasureSpec.getSize(i2);
        if (this.A02 == EnumC03910Jp.FLEXIBLE_SHEET) {
            i3 = Integer.MIN_VALUE;
        }
        int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(size, i3);
        AnonymousClass0Bb r0 = this.A01;
        r0.measure(makeMeasureSpec, makeMeasureSpec2);
        setMeasuredDimension(r0.getMeasuredWidth(), r0.getMeasuredHeight());
    }
}
