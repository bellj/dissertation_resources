package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.4A4  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4A4 extends Enum {
    public static final /* synthetic */ AnonymousClass4A4[] A00;
    public static final AnonymousClass4A4 A01;
    public static final AnonymousClass4A4 A02;

    public static AnonymousClass4A4 valueOf(String str) {
        return (AnonymousClass4A4) Enum.valueOf(AnonymousClass4A4.class, str);
    }

    public static AnonymousClass4A4[] values() {
        return (AnonymousClass4A4[]) A00.clone();
    }

    static {
        AnonymousClass4A4 r3 = new AnonymousClass4A4("PUSH", 0);
        A02 = r3;
        AnonymousClass4A4 r1 = new AnonymousClass4A4("MODAL", 1);
        A01 = r1;
        AnonymousClass4A4[] r0 = new AnonymousClass4A4[2];
        C72453ed.A1J(r3, r1, r0);
        A00 = r0;
    }

    public AnonymousClass4A4(String str, int i) {
    }
}
