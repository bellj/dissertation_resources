package X;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import com.facebook.redex.RunnableBRunnable0Shape0S0110000_I0;
import com.whatsapp.util.Log;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1Cd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC26131Cd {
    public final C14850m9 A00;
    public final C21860y6 A01;
    public final C22710zW A02;

    public AbstractC26131Cd(C14850m9 r1, C21860y6 r2, C22710zW r3) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
    }

    public void A00() {
        boolean z;
        SharedPreferences.Editor edit;
        String str;
        if (!(this instanceof C26141Ce)) {
            z = true;
            edit = ((C26121Cc) this).A01.A00.edit();
            str = "payments_incentive_banner_dismissed";
        } else {
            z = true;
            edit = ((C26141Ce) this).A01.A00.edit();
            str = "payments_onboarding_chat_banner_dismmissed";
        }
        edit.putBoolean(str, z).apply();
    }

    public void A01(Context context) {
        String str;
        if (!(this instanceof C26141Ce)) {
            C26121Cc r2 = (C26121Cc) this;
            C17070qD r1 = r2.A02;
            Intent ADR = r1.A02().ADR(context);
            if (ADR == null) {
                str = "Pay : PaymentsIncentiveBannerManager/getIncentivesOnboardingIntent intent is null";
            } else {
                context.startActivity(ADR);
                AbstractC38191ng AFJ = r1.A02().AFJ();
                if (AFJ != null && !AFJ.A07.A07(979)) {
                    r2.A00();
                    return;
                }
                return;
            }
        } else {
            C26141Ce r3 = (C26141Ce) this;
            Intent AFe = r3.A03.A02().AFe(context, "in_app_banner", true);
            if (AFe == null) {
                str = "Pay : PaymentsOnboardingBannerManager/showPaymentsOnboardingScreen intent is null";
            } else {
                context.startActivity(AFe);
                r3.A00();
                return;
            }
        }
        Log.e(str);
    }

    public boolean A02() {
        AbstractC38191ng AFJ;
        if (!(this instanceof C26141Ce)) {
            C26121Cc r4 = (C26121Cc) this;
            C14850m9 r8 = ((AbstractC26131Cd) r4).A00;
            if (!r8.A07(884) || !((AbstractC26131Cd) r4).A02.A07()) {
                return false;
            }
            C14820m6 r7 = r4.A01;
            SharedPreferences sharedPreferences = r7.A00;
            if (sharedPreferences.getBoolean("payments_incentive_banner_dismissed", false)) {
                return false;
            }
            long A02 = ((long) r8.A02(905)) * 60000;
            long currentTimeMillis = System.currentTimeMillis();
            long j = sharedPreferences.getLong("payments_incentive_banner_start_cool_off_timestamp", -1);
            if ((j != -1 && currentTimeMillis <= j + A02) || (AFJ = r4.A02.A02().AFJ()) == null || !AFJ.A07.A07(842)) {
                return false;
            }
            AnonymousClass2S0 A00 = r4.A03.A00();
            C50942Ry r11 = A00.A01;
            C50932Rx r6 = A00.A02;
            boolean A0A = AFJ.A0A(r11, r6);
            if (r11 == null || A0A) {
                r4.A04.Ab2(new RunnableBRunnable0Shape0S0110000_I0(r4, 15, A0A));
            }
            if (r11 == null || A00.A00(TimeUnit.MILLISECONDS.toSeconds(r4.A00.A00())) != 1) {
                return false;
            }
            if (r6 != null && (!r6.A04 || r6.A01 >= 1 || r6.A00 >= 1)) {
                return false;
            }
            if (sharedPreferences.getLong("payments_incentive_banner_start_timestamp", -1) == -1) {
                r7.A0q("payments_incentive_banner_start_timestamp", System.currentTimeMillis());
                r7.A0q("payments_incentive_banner_last_seen_timestamp", System.currentTimeMillis());
                r4.A03(0);
            } else if (r7.A1J("payments_incentive_banner_last_seen_timestamp", 86400000)) {
                int A022 = r8.A02(885);
                if (sharedPreferences.getInt("payments_incentive_banner_total_days", 0) >= A022) {
                    r4.A03(A022);
                    r4.A00();
                } else {
                    r4.A03(sharedPreferences.getInt("payments_incentive_banner_total_days", 0) + 1);
                }
                r7.A0q("payments_incentive_banner_last_seen_timestamp", System.currentTimeMillis());
            }
            if (sharedPreferences.getInt("payments_incentive_banner_total_days", 0) < r8.A02(885)) {
                return true;
            }
            return false;
        }
        C26141Ce r5 = (C26141Ce) this;
        C14850m9 r1 = ((AbstractC26131Cd) r5).A00;
        C26141Ce.A04 = r1.A02(486);
        if (!r1.A07(484)) {
            return false;
        }
        C14820m6 r3 = r5.A01;
        SharedPreferences sharedPreferences2 = r3.A00;
        if (sharedPreferences2.getBoolean("payments_onboarding_banner_registration_started", false) || !((AbstractC26131Cd) r5).A02.A07()) {
            return false;
        }
        C21860y6 r12 = ((AbstractC26131Cd) r5).A01;
        if (r12.A0A() || r12.A0E("tos_no_wallet")) {
            return false;
        }
        long currentTimeMillis2 = System.currentTimeMillis();
        long j2 = sharedPreferences2.getLong("payments_onboarding_banner_start_cool_off_timestamp", -1);
        if ((j2 != -1 && currentTimeMillis2 <= j2 + 604800000) || sharedPreferences2.getBoolean("payments_onboarding_chat_banner_dismmissed", false)) {
            return false;
        }
        if (sharedPreferences2.getLong("payments_onboarding_banner_start_timestamp", -1) == -1) {
            r3.A0q("payments_onboarding_banner_start_timestamp", System.currentTimeMillis());
            r3.A0q("payments_onboarding_banner_last_seen_timestamp", System.currentTimeMillis());
            r5.A03(0);
        } else if (r3.A1J("payments_onboarding_banner_last_seen_timestamp", 86400000)) {
            int i = sharedPreferences2.getInt("payments_onboarding_banner_total_days", 0);
            int i2 = C26141Ce.A04;
            if (i >= i2) {
                r5.A03(i2);
                r5.A00();
            } else {
                r5.A03(sharedPreferences2.getInt("payments_onboarding_banner_total_days", 0) + 1);
            }
            r3.A0q("payments_onboarding_banner_last_seen_timestamp", System.currentTimeMillis());
        }
        if (sharedPreferences2.getInt("payments_onboarding_banner_total_days", 0) < C26141Ce.A04) {
            return true;
        }
        return false;
    }
}
