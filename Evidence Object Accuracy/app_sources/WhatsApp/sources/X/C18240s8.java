package X;

import com.facebook.redex.RunnableBRunnable0Shape5S0100000_I0_5;
import java.lang.ref.WeakReference;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0s8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18240s8 {
    public final ThreadPoolExecutor A00 = new ThreadPoolExecutor(0, 1, 300, TimeUnit.SECONDS, new LinkedBlockingQueue(), new ThreadFactory() { // from class: X.1Zt
        @Override // java.util.concurrent.ThreadFactory
        public final Thread newThread(Runnable runnable) {
            C18240s8 r3 = C18240s8.this;
            AnonymousClass1MS r1 = new AnonymousClass1MS(new RunnableBRunnable0Shape5S0100000_I0_5(runnable, 44), "SignalExecutor");
            r3.A01 = new WeakReference(r1);
            return r1;
        }
    });
    public volatile WeakReference A01;
    public volatile boolean A02 = false;

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0011, code lost:
        if (java.lang.Thread.currentThread() != r0.get()) goto L_0x0013;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00() {
        /*
            r3 = this;
            boolean r0 = r3.A02
            if (r0 != 0) goto L_0x0019
            java.lang.ref.WeakReference r0 = r3.A01
            if (r0 == 0) goto L_0x0013
            java.lang.Thread r2 = java.lang.Thread.currentThread()
            java.lang.Object r0 = r0.get()
            r1 = 1
            if (r2 == r0) goto L_0x0014
        L_0x0013:
            r1 = 0
        L_0x0014:
            java.lang.String r0 = "Not running on SignalExecutor thread"
            X.AnonymousClass009.A0A(r0, r1)
        L_0x0019:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C18240s8.A00():void");
    }
}
