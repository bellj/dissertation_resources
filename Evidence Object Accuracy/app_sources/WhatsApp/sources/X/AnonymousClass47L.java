package X;

import android.text.Editable;
import com.whatsapp.registration.RegisterName;

/* renamed from: X.47L  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass47L extends C469928m {
    public final /* synthetic */ RegisterName A00;

    public AnonymousClass47L(RegisterName registerName) {
        this.A00 = registerName;
    }

    @Override // X.C469928m, android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        editable.toString();
        throw C12970iu.A0z();
    }
}
