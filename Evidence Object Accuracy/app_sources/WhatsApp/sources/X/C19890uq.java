package X;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.util.Pair;
import com.facebook.redex.RunnableBRunnable0Shape0S0211000_I0;
import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;
import com.facebook.redex.RunnableBRunnable0Shape6S0200000_I0_6;
import com.facebook.redex.RunnableBRunnable0Shape8S0100000_I0_8;
import com.whatsapp.jid.UserJid;
import com.whatsapp.messaging.MessageService;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.0uq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19890uq {
    public static CountDownLatch A0z = new CountDownLatch(1);
    public static final long A10 = TimeUnit.MINUTES.toMillis(15);
    public static final AtomicBoolean A11 = new AtomicBoolean();
    public int A00;
    public long A01 = -1;
    public long A02 = 0;
    public long A03;
    public HandlerThread A04;
    public UserJid A05;
    public HandlerThreadC26611Ed A06;
    public AnonymousClass10C A07;
    public AnonymousClass10C A08;
    public AnonymousClass1VL A09;
    public boolean A0A = false;
    public boolean A0B = false;
    public boolean A0C = false;
    public final BroadcastReceiver A0D = new C43331wq(this);
    public final BroadcastReceiver A0E;
    public final Handler A0F = new Handler(new Handler.Callback() { // from class: X.1wr
        @Override // android.os.Handler.Callback
        public final boolean handleMessage(Message message) {
            return C19890uq.A03(message, C19890uq.this);
        }
    });
    public final C16210od A0G;
    public final AbstractC15710nm A0H;
    public final C15570nT A0I;
    public final C16240og A0J;
    public final C19500uD A0K;
    public final C231010j A0L;
    public final C18230s7 A0M;
    public final C18640sm A0N;
    public final C247716u A0O;
    public final AnonymousClass01d A0P;
    public final C15410nB A0Q;
    public final C14830m7 A0R;
    public final C16590pI A0S;
    public final C18360sK A0T;
    public final C16490p7 A0U;
    public final C14850m9 A0V;
    public final C22050yP A0W;
    public final C43371wu A0X;
    public final C43321wp A0Y = new C43321wp("message_handler/logged_flag/disconnected", true);
    public final C43321wp A0Z = new C43321wp("message_handler/logged_flag/must_ignore_network_once", false);
    public final C43321wp A0a = new C43321wp("message_handler/logged_flag/must_reconnect", true);
    public final C230610f A0b;
    public final C17220qS A0c;
    public final HandlerC43301wn A0d = new HandlerC43301wn(Looper.getMainLooper(), this);
    public final HandlerC43361wt A0e;
    public final AbstractC43401wx A0f;
    public final C22360yx A0g;
    public final C230710g A0h;
    public final AnonymousClass1EV A0i;
    public final C17230qT A0j;
    public final C14910mF A0k;
    public final C22900zp A0l;
    public final C231210l A0m;
    public final AnonymousClass1VN A0n;
    public final C21280xA A0o;
    public final C14860mA A0p;
    public final Object A0q = new Object();
    public final Random A0r = new Random();
    public final AtomicBoolean A0s = new AtomicBoolean(false);
    public final AtomicBoolean A0t = new AtomicBoolean();
    public final AtomicBoolean A0u = new AtomicBoolean();
    public final AnonymousClass01N A0v;
    public volatile boolean A0w = true;
    public volatile boolean A0x;
    public volatile boolean A0y;

    public C19890uq(C16210od r13, AbstractC15710nm r14, C15570nT r15, C15450nH r16, C16240og r17, C19500uD r18, C231010j r19, C18230s7 r20, C18640sm r21, C247716u r22, AnonymousClass01d r23, C15410nB r24, C14830m7 r25, C16590pI r26, C18360sK r27, C16490p7 r28, C14850m9 r29, C22050yP r30, C230610f r31, C17220qS r32, C22360yx r33, C230710g r34, AnonymousClass1EV r35, C17230qT r36, C14910mF r37, C22900zp r38, C231210l r39, C21280xA r40, C14860mA r41, AnonymousClass01N r42) {
        AbstractC43401wx r7;
        C43351ws r4 = new C43351ws(this);
        this.A0E = r4;
        this.A0S = r26;
        this.A0R = r25;
        this.A0V = r29;
        this.A0M = r20;
        this.A0H = r14;
        this.A0I = r15;
        this.A0p = r41;
        this.A0c = r32;
        this.A0o = r40;
        this.A0P = r23;
        this.A0k = r37;
        this.A0l = r38;
        this.A0J = r17;
        this.A0K = r18;
        this.A0W = r30;
        this.A0Q = r24;
        this.A0j = r36;
        this.A0g = r33;
        this.A0U = r28;
        this.A0O = r22;
        this.A0T = r27;
        this.A0b = r31;
        this.A0v = r42;
        this.A0G = r13;
        this.A0N = r21;
        this.A0h = r34;
        this.A0i = r35;
        this.A0m = r39;
        this.A0L = r19;
        this.A0n = new AnonymousClass1VN(1, 17280);
        this.A0e = new HandlerC43361wt(Looper.getMainLooper(), this);
        this.A0X = new C43371wu(Looper.getMainLooper(), r16, r17, r20, r23, r26);
        Context context = r26.A00;
        IntentFilter intentFilter = new IntentFilter("com.whatsapp.MessageHandler.LOGOUT_ACTION");
        String str = AnonymousClass01V.A09;
        context.registerReceiver(r4, intentFilter, str, null);
        context.registerReceiver(new C43381wv(this), new IntentFilter("com.whatsapp.MessageHandler.RECONNECT_ACTION"), str, null);
        if (Build.VERSION.SDK_INT >= 29) {
            r7 = new C43391ww(this.A0P, this.A0Q, this);
        } else {
            C16590pI r11 = this.A0S;
            C15410nB r10 = this.A0Q;
            r7 = new C43421wz(this.A0N, this.A0O, r10, r11, this);
        }
        this.A0f = r7;
        r13.A03(new C43411wy(this));
    }

    public static /* synthetic */ void A00(C19890uq r4) {
        if (!r4.A0y) {
            Log.i("xmpp/handler/start");
            r4.A0y = true;
            if (r4.A0V.A07(1740)) {
                C17220qS r1 = r4.A0c;
                AnonymousClass1EV r0 = r4.A0i;
                Set set = r1.A0A;
                synchronized (set) {
                    set.add(r0);
                }
            }
            HandlerThreadC26611Ed r3 = (HandlerThreadC26611Ed) r4.A0v.get();
            r4.A06 = r3;
            HandlerC43301wn r2 = r4.A0d;
            boolean z = false;
            if (r3.A02 == null) {
                z = true;
            }
            AnonymousClass009.A0C("callback is already set", z);
            r3.A02 = r2;
            r3.start();
        }
    }

    public static /* synthetic */ void A01(C19890uq r18, int i, boolean z) {
        byte[] A0D;
        byte[] A0F;
        AbstractC14640lm r0;
        boolean containsKey;
        Context context = r18.A0S.A00;
        synchronized (r18.A0q) {
            r18.A0Y.A00(false);
            if (!r18.A0A && Build.VERSION.SDK_INT < 29) {
                r18.A0A = r18.A0f.isConnected();
                Log.i("xmpp/handler/handleConnected setting isNetworkUp to true");
            }
            r18.A00 = i;
            AnonymousClass10C r6 = r18.A08;
            AnonymousClass009.A01();
            r6.A10.A03();
            C16240og r5 = r6.A0B;
            r5.A04 = 2;
            r5.A02 = true;
            r5.A05 = z;
            r5.A03.open();
            AnonymousClass009.A01();
            for (AbstractC18870tC r02 : r5.A01()) {
                r02.AR9();
            }
            C22520zD r1 = r6.A04;
            r1.A00 = false;
            r6.A0r.A02 = false;
            r1.A01 = false;
            C22910zq r03 = r6.A0b;
            Map map = r03.A01;
            synchronized (map) {
                map.clear();
            }
            AnonymousClass101 r12 = r6.A08;
            synchronized (r12) {
                r12.A08.clear();
            }
            C22920zr r2 = r6.A07;
            synchronized (r2) {
                r2.A02 = false;
                r2.A00 = 0;
                r2.A04(0);
            }
            Log.i("server connected");
            C14820m6 r9 = r6.A0L;
            SharedPreferences sharedPreferences = r9.A00;
            sharedPreferences.edit().putBoolean("spam_banned", false).apply();
            r9.A0q("spam_banned_expiry_timestamp", 0);
            sharedPreferences.edit().putBoolean("underage_account_banned", false).apply();
            C22600zL r13 = r6.A0u;
            r13.A02 = true;
            r13.A0C();
            r6.A05.A08();
            AbstractC14440lR r14 = r6.A11;
            r14.Ab2(new RunnableBRunnable0Shape8S0100000_I0_8(r6, 34));
            r14.Ab2(new RunnableBRunnable0Shape8S0100000_I0_8(r6, 27));
            if (r6.A0W.A07(877)) {
                r14.Ab2(new RunnableBRunnable0Shape8S0100000_I0_8(r6.A0x, 26));
            }
            r14.Ab2(new RunnableBRunnable0Shape8S0100000_I0_8(r6, 28));
            C16490p7 r132 = r6.A0R;
            if (r132.A00) {
                AnonymousClass109 r15 = r6.A0a;
                RunnableBRunnable0Shape8S0100000_I0_8 runnableBRunnable0Shape8S0100000_I0_8 = new RunnableBRunnable0Shape8S0100000_I0_8(r6, 33);
                C22390z0 r16 = r15.A0M;
                synchronized (r16) {
                    r16.A03.clear();
                }
                ArrayList arrayList = new ArrayList();
                synchronized (r15.A0S) {
                    for (Map.Entry entry : r15.A0T.entrySet()) {
                        C14540lb r22 = r15.A0K;
                        AbstractC14470lU r17 = (AbstractC14470lU) entry.getValue();
                        synchronized (r22) {
                            containsKey = r22.A01.containsKey(r17);
                        }
                        if (containsKey) {
                            arrayList.addAll(Collections.unmodifiableList(((C38421o4) entry.getKey()).A01));
                        }
                    }
                }
                C43431x0 r23 = new C43431x0(r15, arrayList);
                r15.A0Q.Ab2(r23);
                r23.A01(new AbstractC14590lg(runnableBRunnable0Shape8S0100000_I0_8, arrayList) { // from class: X.1x1
                    public final /* synthetic */ Runnable A01;
                    public final /* synthetic */ List A02;

                    {
                        this.A02 = r3;
                        this.A01 = r2;
                    }

                    @Override // X.AbstractC14590lg
                    public final void accept(Object obj) {
                        AnonymousClass109 r52 = AnonymousClass109.this;
                        List<AbstractC16130oV> list = this.A02;
                        Runnable runnable = this.A01;
                        for (AbstractC16130oV r62 : list) {
                            if (C26751Er.A01(r62, true)) {
                                r52.A09.A08(r62, -1);
                                r52.A0Q.Ab2(new RunnableBRunnable0Shape0S0211000_I0(r52, r62, 7, 2, false));
                            }
                        }
                        runnable.run();
                    }
                }, r15.A0U);
            }
            C17220qS r92 = r6.A0c;
            C230510e r04 = r92.A06;
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            LinkedHashMap linkedHashMap2 = r04.A00;
            synchronized (linkedHashMap2) {
                Iterator it = linkedHashMap2.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry entry2 = (Map.Entry) it.next();
                    if (((Integer) ((Pair) entry2.getValue()).second).intValue() < 3) {
                        linkedHashMap.put((String) entry2.getKey(), (Message) ((Pair) entry2.getValue()).first);
                    } else {
                        it.remove();
                    }
                }
                StringBuilder sb = new StringBuilder();
                sb.append("unacked-messages/getUnackedMessages: ");
                sb.append(linkedHashMap.size());
                Log.i(sb.toString());
            }
            for (Map.Entry entry3 : linkedHashMap.entrySet()) {
                r92.A07((Message) entry3.getValue(), (String) entry3.getKey(), true);
            }
            C22280yp r24 = r6.A0l;
            Set set = r24.A07;
            HashSet hashSet = new HashSet(set);
            set.clear();
            Iterator it2 = hashSet.iterator();
            while (it2.hasNext()) {
                r24.A04((AbstractC14640lm) it2.next());
            }
            C43451x2 r10 = new Object() { // from class: X.1x2
            };
            List<AnonymousClass1V7> list = r03.A00;
            synchronized (list) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("in-flight-messages/for-each/send-pending-requests: ");
                sb2.append(list.size());
                Log.i(sb2.toString());
                for (AnonymousClass1V7 r05 : list) {
                    String str = r05.A01;
                    Message message = r05.A00;
                    boolean z2 = r05.A02;
                    C17220qS r19 = AnonymousClass10C.this.A0c;
                    if (z2) {
                        r19.A07(message, str, true);
                    } else {
                        r19.A07(message, str, false);
                    }
                }
                list.clear();
            }
            C14910mF r3 = r6.A0k;
            if (r3.A00 != 3) {
                AnonymousClass1m8 A00 = r6.A01.A00();
                if (A00.A02 && (r0 = (AbstractC14640lm) A00.A00().A2c.A0B(AbstractC14640lm.class)) != null) {
                    r24.A04(r0);
                }
            }
            boolean z3 = r5.A01;
            boolean z4 = false;
            if (r3.A00 == 3) {
                z4 = true;
            }
            if (z3 == z4) {
                if (!z4) {
                    C21810y1 r110 = r6.A0m;
                    r110.A00 = true;
                    r110.A00();
                } else {
                    C21810y1 r06 = r6.A0m;
                    r06.A00 = false;
                    r06.A01();
                }
            }
            if (sharedPreferences.getBoolean("future_proof_processing_needed", false)) {
                r132.A04();
                if (r132.A01) {
                    r14.Ab4(new RunnableBRunnable0Shape8S0100000_I0_8(r6, 30), "MessageHandlerCallback/processFutureMessages");
                }
            }
            if (sharedPreferences.getBoolean("client_version_upgraded", false)) {
                r132.A04();
                if (r132.A01) {
                    r14.Ab2(new RunnableBRunnable0Shape8S0100000_I0_8(r6, 31));
                }
                r14.Ab2(new RunnableBRunnable0Shape8S0100000_I0_8(r6, 32));
                C18850tA r32 = r6.A0E;
                r32.A0e.Ab2(new RunnableBRunnable0Shape4S0100000_I0_4(r32, 19));
                sharedPreferences.edit().remove("client_version_upgraded").apply();
            }
            r6.A0X.A0F(false);
            MessageService.A00(context);
            C22360yx r52 = r18.A0g;
            if (r52.A00()) {
                r18.A0A();
            }
            if (r18.A0U.A00 && C32781cj.A0D(context) && (A0F = C32781cj.A0F((A0D = C003501n.A0D(16)))) != null) {
                AbstractC15710nm r33 = r18.A0H;
                C32781cj.A0B(r33, A0F);
                int length = A0D.length;
                if (length != 16) {
                    StringBuilder sb3 = new StringBuilder("");
                    sb3.append(length);
                    r33.AaV("crypto-iq-incorrect-account-salt-size", sb3.toString(), true);
                }
                r18.A0K.A01(null, A0F, A0D, 1);
                Arrays.toString(A0D);
                Arrays.toString(A0F);
            }
            r18.A0n.A02();
            r18.A09();
            C43371wu r4 = r18.A0X;
            AnonymousClass1VL r34 = r18.A09;
            r4.A04 = r52.A00();
            r4.A08.post(new RunnableBRunnable0Shape6S0200000_I0_6(r4, 7, r34));
            r18.A0T.A04(10, null);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x007f, code lost:
        if (r1.A00.A09.isEmpty() != false) goto L_0x0083;
     */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00aa A[Catch: all -> 0x011a, TryCatch #0 {, blocks: (B:16:0x002d, B:19:0x003f, B:20:0x004e, B:22:0x0061, B:24:0x006a, B:26:0x0077, B:30:0x0084, B:32:0x00aa, B:34:0x0106, B:36:0x010e, B:37:0x0111, B:39:0x0113), top: B:46:0x002d }] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0106 A[Catch: all -> 0x011a, TryCatch #0 {, blocks: (B:16:0x002d, B:19:0x003f, B:20:0x004e, B:22:0x0061, B:24:0x006a, B:26:0x0077, B:30:0x0084, B:32:0x00aa, B:34:0x0106, B:36:0x010e, B:37:0x0111, B:39:0x0113), top: B:46:0x002d }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ void A02(X.C19890uq r8, boolean r9) {
        /*
        // Method dump skipped, instructions count: 288
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C19890uq.A02(X.0uq, boolean):void");
    }

    public static /* synthetic */ boolean A03(Message message, C19890uq r9) {
        boolean z;
        boolean z2;
        boolean z3 = false;
        if (message.arg1 != 0) {
            z3 = true;
        }
        long j = message.getData().getLong("networkId");
        synchronized (r9.A0q) {
            z = false;
            if (r9.A0A != z3) {
                if (z3) {
                    Log.i("xmpp/handler/network/up");
                    r9.A0J(false, true, false);
                } else {
                    Log.i("xmpp/handler/network/down");
                    AnonymousClass1VL r0 = r9.A09;
                    if (r0 != null) {
                        r0.Abb(true);
                    }
                }
                r9.A0A = z3;
                r9.A01 = j;
                z = true;
            } else if (z3) {
                long j2 = r9.A01;
                if (j != j2) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("xmpp/handler/network/switch old=");
                    sb.append(j2);
                    sb.append(" new=");
                    sb.append(j);
                    Log.i(sb.toString());
                    AnonymousClass1VL r02 = r9.A09;
                    if (r02 != null) {
                        r02.Abb(true);
                    }
                    r9.A01 = j;
                    z2 = true;
                } else {
                    z2 = false;
                }
                r9.A0J(false, false, true);
                z = z2;
            }
        }
        if (z) {
            r9.A0W.A06(r9.A0N.A07());
        }
        return true;
    }

    public void A04() {
        Message obtain = Message.obtain(null, 0, 0, 0);
        obtain.getData().putBoolean("should_register", true);
        this.A0e.sendMessage(obtain);
    }

    public void A05() {
        Message obtain = Message.obtain(null, 0, 0, 0);
        obtain.getData().putBoolean("should_register", false);
        this.A0e.sendMessage(obtain);
    }

    public void A06() {
        synchronized (this.A0E) {
            Log.i("xmpp/handler/logout-timer/reset");
            if (A0K()) {
                A0A();
            }
        }
    }

    public void A07() {
        Bundle bundle = new Bundle();
        bundle.putBoolean("long_connect", true);
        this.A0e.sendMessage(Message.obtain(null, 0, 3, 0, bundle));
        A0C(0, true, false, false, false);
    }

    public final void A08() {
        Context context = this.A0S.A00;
        synchronized (this.A0E) {
            Log.i("xmpp/handler/logout-timer/cancel");
            try {
                PendingIntent A01 = AnonymousClass1UY.A01(context, 0, new Intent("com.whatsapp.MessageHandler.LOGOUT_ACTION").setPackage("com.whatsapp"), 536870912);
                if (A01 != null) {
                    AlarmManager A04 = this.A0P.A04();
                    if (A04 != null) {
                        A04.cancel(A01);
                    } else {
                        Log.w("MessageHandler/cancelLogoutTimer AlarmManager is null");
                    }
                    A01.cancel();
                }
            } catch (RuntimeException e) {
                if (e.getCause() instanceof DeadObjectException) {
                    this.A0H.AaV("messagehandler/deadOS", null, false);
                } else {
                    throw e;
                }
            }
        }
    }

    public final void A09() {
        synchronized (this.A0q) {
            C43321wp r2 = this.A0a;
            boolean z = false;
            if (!this.A0g.A00()) {
                z = true;
            }
            r2.A00(z);
        }
    }

    public final void A0A() {
        Context context = this.A0S.A00;
        synchronized (this.A0E) {
            Log.i("xmpp/handler/logout-timer/start");
            if (!this.A0M.A02(AnonymousClass1UY.A01(context, 0, new Intent("com.whatsapp.MessageHandler.LOGOUT_ACTION").setPackage("com.whatsapp"), 134217728), 2, SystemClock.elapsedRealtime() + (((long) this.A0V.A02(431)) * 60 * 1000))) {
                Log.w("MessageHandler/startLogoutTimer AlarmManager is null");
            }
        }
    }

    public void A0B(int i) {
        Message obtain = Message.obtain(null, 0, 2, 0);
        obtain.getData().putBoolean("force", true);
        obtain.getData().putInt("connect_reason", i);
        this.A0e.sendMessage(obtain);
    }

    public void A0C(int i, boolean z, boolean z2, boolean z3, boolean z4) {
        A0F(null, null, i, z, z2, z3, z4, false, false);
    }

    public void A0D(long j, boolean z) {
        Handler handler = this.A0F;
        Message obtain = Message.obtain(handler, 0, z ? 1 : 0, 0);
        obtain.getData().putLong("networkId", j);
        handler.sendMessage(obtain);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:65:0x012b, code lost:
        if (r14.A0p.A0N() != false) goto L_0x012d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x014f, code lost:
        if (r9.A00 == false) goto L_0x0151;
     */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x0020 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0E(java.lang.String r15, java.lang.String r16, int r17, boolean r18, boolean r19, boolean r20, boolean r21, boolean r22) {
        /*
        // Method dump skipped, instructions count: 345
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C19890uq.A0E(java.lang.String, java.lang.String, int, boolean, boolean, boolean, boolean, boolean):void");
    }

    public void A0F(String str, String str2, int i, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6) {
        Message obtain = Message.obtain(null, 0, 2, 0);
        obtain.getData().putBoolean("force", z);
        obtain.getData().putBoolean("force_no_ongoing_backoff", z2);
        obtain.getData().putBoolean("reset", z3);
        obtain.getData().putBoolean("check_connection", z4);
        obtain.getData().putBoolean("notify_on_failure", z5);
        obtain.getData().putString("ip_address", str);
        obtain.getData().putString("cl_sess", str2);
        obtain.getData().putBoolean("fgservice", z6);
        obtain.getData().putInt("connect_reason", i);
        this.A0e.sendMessage(obtain);
    }

    public void A0G(boolean z) {
        StringBuilder sb = new StringBuilder("xmpp/service/stop/unregister:");
        sb.append(z);
        Log.i(sb.toString());
        this.A0J.A06 = false;
        Message obtain = Message.obtain(null, 0, 1, 0);
        obtain.getData().putBoolean("should_unregister", z);
        this.A0e.sendMessage(obtain);
    }

    public final void A0H(boolean z) {
        long elapsedRealtime = SystemClock.elapsedRealtime();
        long j = this.A03;
        if (j <= 0 || elapsedRealtime >= j) {
            Context context = this.A0S.A00;
            if (this.A0C) {
                this.A0n.A03(this.A02);
                this.A0C = false;
            }
            AnonymousClass1VN r0 = this.A0n;
            long A01 = r0.A01();
            this.A02 = r0.A00();
            long j2 = A01 * 10000;
            if (j2 == 0) {
                Log.i("xmpp/handler/schedule-reconnect/immediate");
                A0B(0);
                return;
            }
            Random random = this.A0r;
            long nextLong = (j2 / 2) + ((random.nextLong() & Long.MAX_VALUE) % j2);
            if (z) {
                long j3 = A10;
                if (nextLong > j3) {
                    nextLong = j3 + ((long) (random.nextInt(60) - 30));
                    Log.i("xmpp/handler/schedule-reconnect/backoff clamped to ~15mins");
                }
            }
            StringBuilder sb = new StringBuilder("xmpp/handler/schedule-reconnect/backoff:");
            sb.append(nextLong);
            Log.i(sb.toString());
            Intent intent = new Intent("com.whatsapp.MessageHandler.RECONNECT_ACTION").setPackage("com.whatsapp");
            intent.putExtra("connect_reason", 3);
            long j4 = elapsedRealtime + nextLong;
            if (this.A0M.A02(AnonymousClass1UY.A01(context, 0, intent, 0), 2, j4)) {
                this.A03 = j4;
                return;
            }
            Log.w("MessageHandler/scheduleReconnect AlarmManager is null");
            this.A03 = 0;
            return;
        }
        Log.i("xmpp/handler/schedule-reconnect/already-pending");
    }

    public void A0I(boolean z, boolean z2) {
        Context context = this.A0S.A00;
        AlarmManager A04 = this.A0P.A04();
        if (A04 != null) {
            Intent intent = new Intent("com.whatsapp.MessageHandler.CONNECTIVITY_RETRY_ACTION").setPackage("com.whatsapp");
            if (!z) {
                PendingIntent A01 = AnonymousClass1UY.A01(context, 0, intent, 536870912);
                if (A01 != null) {
                    Log.i("connectivity retry alarm canceled");
                    A04.cancel(A01);
                    A01.cancel();
                }
            } else if (!z2) {
                this.A0M.A02(AnonymousClass1UY.A01(context, 0, intent, 134217728), 2, SystemClock.elapsedRealtime() + 60000);
                Log.i("connectivity retry alarm set for 60000ms from now");
                return;
            }
        } else {
            Log.w("MessageHandler/onCaptivePortalDetectionAndWaitDone AlarmManager is null");
        }
        A11.set(z);
        A0z.countDown();
    }

    public final void A0J(boolean z, boolean z2, boolean z3) {
        A0E(null, null, 0, z, z2, z3, false, false);
    }

    public final boolean A0K() {
        boolean z;
        synchronized (this.A0E) {
            z = false;
            if (AnonymousClass1UY.A01(this.A0S.A00, 0, new Intent("com.whatsapp.MessageHandler.LOGOUT_ACTION").setPackage("com.whatsapp"), 536870912) != null) {
                z = true;
            }
            StringBuilder sb = new StringBuilder();
            sb.append("xmpp/handler/logout-timer/has=");
            sb.append(z);
            Log.i(sb.toString());
        }
        return z;
    }
}
