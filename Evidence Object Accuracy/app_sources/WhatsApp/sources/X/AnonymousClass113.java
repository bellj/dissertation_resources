package X;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import com.whatsapp.util.Log;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* renamed from: X.113  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass113 extends AbstractC16280ok {
    public final C231410n A00;

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    public AnonymousClass113(Context context, AbstractC15710nm r9, C231410n r10) {
        super(context, r9, "migration_export_metadata.db", new ReentrantReadWriteLock(), 1, true);
        this.A00 = r10;
    }

    @Override // X.AbstractC16280ok
    public C16330op A03() {
        try {
            return AnonymousClass1Tx.A01(super.A00(), this.A00);
        } catch (SQLiteException e) {
            Log.e("Failed to open writable export metadata db.", e);
            return AnonymousClass1Tx.A01(super.A00(), this.A00);
        }
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE exported_files_metadata(_id INTEGER PRIMARY KEY AUTOINCREMENT, local_path TEXT NOT NULL, exported_path TEXT UNIQUE NOT NULL, file_size INTEGER, required INTEGER, encryption_iv TEXT NOT NULL)");
        sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS exported_files_metadata_local_path_index ON exported_files_metadata (local_path)");
    }
}
