package X;

import android.view.ViewTreeObserver;

/* renamed from: X.3Nl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ViewTreeObserver$OnPreDrawListenerC66403Nl implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ AbstractC36001jA A00;

    public ViewTreeObserver$OnPreDrawListenerC66403Nl(AbstractC36001jA r1) {
        this.A00 = r1;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        AbstractC36001jA r4 = this.A00;
        C12980iv.A1G(r4.A0A, this);
        C92254Vd r2 = r4.A0b;
        r2.A02.ATC(C12990iw.A03(r4.A0A));
        r4.A0J(r4.A03(), null, r4.A01(), false);
        r4.A0P(null, true);
        return true;
    }
}
