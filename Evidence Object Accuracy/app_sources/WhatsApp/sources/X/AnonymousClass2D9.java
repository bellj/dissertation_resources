package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.2D9  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2D9 extends AbstractC34011fR {
    public final UserJid A00;
    public final C22230yk A01;

    public AnonymousClass2D9(UserJid userJid, C22230yk r2) {
        this.A01 = r2;
        this.A00 = userJid;
    }
}
