package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2mc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57182mc extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57182mc A03;
    public static volatile AnonymousClass255 A04;
    public int A00;
    public AbstractC27881Jp A01 = AbstractC27881Jp.A01;
    public String A02 = "";

    static {
        C57182mc r0 = new C57182mc();
        A03 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r7, Object obj, Object obj2) {
        switch (r7.ordinal()) {
            case 0:
                return A03;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                C57182mc r9 = (C57182mc) obj2;
                int i = this.A00;
                boolean A1R = C12960it.A1R(i);
                String str = this.A02;
                int i2 = r9.A00;
                this.A02 = r8.Afy(str, r9.A02, A1R, C12960it.A1R(i2));
                this.A01 = r8.Afm(this.A01, r9.A01, C12960it.A1V(i & 2, 2), C12960it.A1V(i2 & 2, 2));
                if (r8 == C463025i.A00) {
                    this.A00 |= r9.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        try {
                            int A032 = r82.A03();
                            if (A032 == 0) {
                                break;
                            } else if (A032 == 10) {
                                String A0A = r82.A0A();
                                this.A00 = 1 | this.A00;
                                this.A02 = A0A;
                            } else if (A032 == 18) {
                                this.A00 |= 2;
                                this.A01 = r82.A08();
                            } else if (!A0a(r82, A032)) {
                                break;
                            }
                        } catch (IOException e) {
                            throw AbstractC27091Fz.A0K(this, e);
                        }
                    } catch (C28971Pt e2) {
                        throw AbstractC27091Fz.A0J(e2, this);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C57182mc();
            case 5:
                return new C82103v5();
            case 6:
                break;
            case 7:
                if (A04 == null) {
                    synchronized (C57182mc.class) {
                        if (A04 == null) {
                            A04 = AbstractC27091Fz.A09(A03);
                        }
                    }
                }
                return A04;
            default:
                throw C12970iu.A0z();
        }
        return A03;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            i2 = AbstractC27091Fz.A04(1, this.A02, 0);
        }
        if ((this.A00 & 2) == 2) {
            i2 = AbstractC27091Fz.A05(this.A01, 2, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A02);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0K(this.A01, 2);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
