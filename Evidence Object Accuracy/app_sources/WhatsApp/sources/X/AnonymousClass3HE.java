package X;

import com.facebook.redex.IDxNFunctionShape17S0100000_2_I1;
import java.util.List;

/* renamed from: X.3HE  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3HE {
    public final AnonymousClass1V8 A00;
    public final AnonymousClass3EF A01;
    public final AnonymousClass3E1 A02;
    public final AnonymousClass3E2 A03;
    public final AnonymousClass3E3 A04;
    public final AnonymousClass3E4 A05;

    public AnonymousClass3HE(AbstractC15710nm r6, AnonymousClass1V8 r7) {
        AnonymousClass1V8.A01(r7, "fds");
        this.A01 = (AnonymousClass3EF) AnonymousClass3JT.A05(r7, new IDxNFunctionShape17S0100000_2_I1(r6, 13), new String[0]);
        this.A05 = (AnonymousClass3E4) A00(r7, new IDxNFunctionShape17S0100000_2_I1(r6, 12), new String[]{"states"});
        this.A02 = (AnonymousClass3E1) A00(r7, new IDxNFunctionShape17S0100000_2_I1(r6, 15), new String[]{"next_screens"});
        this.A04 = (AnonymousClass3E3) A00(r7, new IDxNFunctionShape17S0100000_2_I1(r6, 16), new String[]{"screen_data"});
        this.A03 = (AnonymousClass3E2) A00(r7, new IDxNFunctionShape17S0100000_2_I1(r6, 14), new String[]{"persist_data"});
        this.A00 = r7;
    }

    public static Object A00(AnonymousClass1V8 r10, AbstractC116095Uc r11, String[] strArr) {
        List A0B = AnonymousClass3JT.A0B(r10, r11, strArr, 0, 1);
        if (A0B.isEmpty()) {
            return null;
        }
        return A0B.get(0);
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || AnonymousClass3HE.class != obj.getClass()) {
                return false;
            }
            AnonymousClass3HE r5 = (AnonymousClass3HE) obj;
            if (!C29941Vi.A00(this.A05, r5.A05) || !C29941Vi.A00(this.A02, r5.A02) || !C29941Vi.A00(this.A04, r5.A04) || !C29941Vi.A00(this.A03, r5.A03) || !this.A01.equals(r5.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] objArr = new Object[5];
        objArr[0] = this.A05;
        objArr[1] = this.A02;
        objArr[2] = this.A04;
        objArr[3] = this.A03;
        return C12980iv.A0B(this.A01, objArr, 4);
    }
}
