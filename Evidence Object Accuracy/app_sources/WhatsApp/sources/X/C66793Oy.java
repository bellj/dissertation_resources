package X;

import android.text.TextUtils;
import com.whatsapp.gallery.MediaGalleryActivity;

/* renamed from: X.3Oy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C66793Oy implements AnonymousClass07L {
    public final /* synthetic */ MediaGalleryActivity A00;

    @Override // X.AnonymousClass07L
    public boolean AUY(String str) {
        return false;
    }

    public C66793Oy(MediaGalleryActivity mediaGalleryActivity) {
        this.A00 = mediaGalleryActivity;
    }

    @Override // X.AnonymousClass07L
    public boolean AUX(String str) {
        MediaGalleryActivity mediaGalleryActivity = this.A00;
        if (TextUtils.equals(mediaGalleryActivity.A0o, str)) {
            return false;
        }
        mediaGalleryActivity.A0o = str;
        mediaGalleryActivity.A0p = C32751cg.A02(((ActivityC13830kP) mediaGalleryActivity).A01, str);
        AbstractC35481hz A02 = MediaGalleryActivity.A02(mediaGalleryActivity);
        if (A02 == null) {
            return false;
        }
        C15250mo r1 = mediaGalleryActivity.A0M;
        r1.A04(mediaGalleryActivity.A0p);
        r1.A03(str);
        A02.AVc(r1);
        return false;
    }
}
