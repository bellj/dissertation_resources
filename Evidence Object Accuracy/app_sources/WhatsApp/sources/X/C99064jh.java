package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4jh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C99064jh implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        String str = null;
        byte[] bArr = null;
        int i = 0;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 2) {
                str = C95664e9.A08(parcel, readInt);
            } else if (c == 3) {
                bArr = C95664e9.A0I(parcel, readInt);
            } else if (c != 4) {
                C95664e9.A0D(parcel, readInt);
            } else {
                C95664e9.A0F(parcel, readInt, 4);
                i = parcel.readInt();
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78313oi(str, bArr, i);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new C78313oi[i];
    }
}
