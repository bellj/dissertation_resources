package X;

import java.util.Comparator;

/* renamed from: X.4dR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC95284dR {
    public static final AbstractC95284dR ACTIVE = new C81003tJ();
    public static final AbstractC95284dR GREATER = new C80993tI(1);
    public static final AbstractC95284dR LESS = new C80993tI(-1);

    public abstract AbstractC95284dR compare(int i, int i2);

    public abstract AbstractC95284dR compare(Object obj, Object obj2, Comparator comparator);

    public abstract AbstractC95284dR compareFalseFirst(boolean z, boolean z2);

    public abstract AbstractC95284dR compareTrueFirst(boolean z, boolean z2);

    public abstract int result();

    public AbstractC95284dR() {
    }

    public /* synthetic */ AbstractC95284dR(C81003tJ r2) {
        this();
    }

    public static AbstractC95284dR start() {
        return ACTIVE;
    }
}
