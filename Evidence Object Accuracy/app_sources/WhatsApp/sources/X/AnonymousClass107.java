package X;

/* renamed from: X.107  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass107 extends AbstractC18860tB implements AnonymousClass108 {
    public final AnonymousClass10S A00;
    public final C21320xE A01;
    public final AnonymousClass12H A02;
    public final AnonymousClass132 A03;
    public final C15440nG A04;
    public final AnonymousClass133 A05;
    public final AbstractC14440lR A06;

    @Override // X.AnonymousClass108
    public void AS9() {
    }

    public AnonymousClass107(AnonymousClass10S r1, C21320xE r2, AnonymousClass12H r3, AnonymousClass132 r4, C15440nG r5, AnonymousClass133 r6, AbstractC14440lR r7) {
        this.A06 = r7;
        this.A00 = r1;
        this.A02 = r3;
        this.A03 = r4;
        this.A01 = r2;
        this.A04 = r5;
        this.A05 = r6;
    }

    @Override // X.AnonymousClass108
    public void ASA() {
        if (this.A04.A00.A05(AbstractC15460nI.A18)) {
            this.A05.A01();
        }
    }
}
