package X;

import java.util.Iterator;
import java.util.NoSuchElementException;

/* renamed from: X.5DF  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5DF implements Iterator {
    public boolean canRemove;
    public AnonymousClass4Y5 currentEntry;
    public final Iterator entryIterator;
    public int laterCount;
    public final AnonymousClass5Z2 multiset;
    public int totalCount;

    public AnonymousClass5DF(AnonymousClass5Z2 r1, Iterator it) {
        this.multiset = r1;
        this.entryIterator = it;
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        return this.laterCount > 0 || this.entryIterator.hasNext();
    }

    @Override // java.util.Iterator
    public Object next() {
        if (hasNext()) {
            int i = this.laterCount;
            if (i == 0) {
                AnonymousClass4Y5 r0 = (AnonymousClass4Y5) this.entryIterator.next();
                this.currentEntry = r0;
                i = r0.getCount();
                this.laterCount = i;
                this.totalCount = i;
            }
            this.laterCount = i - 1;
            this.canRemove = true;
            return this.currentEntry.getElement();
        }
        throw new NoSuchElementException();
    }

    @Override // java.util.Iterator
    public void remove() {
        C28251Mi.checkRemove(this.canRemove);
        if (this.totalCount == 1) {
            this.entryIterator.remove();
        } else {
            this.multiset.remove(this.currentEntry.getElement());
        }
        this.totalCount--;
        this.canRemove = false;
    }
}
