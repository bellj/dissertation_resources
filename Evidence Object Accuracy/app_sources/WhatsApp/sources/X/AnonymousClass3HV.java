package X;

/* renamed from: X.3HV  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3HV {
    public final long A00;
    public final long A01;
    public final AnonymousClass3FI A02;
    public final String A03;
    public final String A04;
    public final String A05;
    public final String A06;

    public AnonymousClass3HV(AnonymousClass3FI r1, String str, String str2, String str3, String str4, long j, long j2) {
        AnonymousClass009.A05(str);
        this.A06 = str;
        AnonymousClass009.A05(str2);
        this.A05 = str2;
        AnonymousClass009.A05(str3);
        this.A04 = str3;
        AnonymousClass009.A05(str4);
        this.A03 = str4;
        this.A00 = j;
        this.A01 = j2;
        this.A02 = r1;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:36:0x0078 */
    /* JADX DEBUG: Multi-variable search result rejected for r9v3, resolved type: java.lang.String */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r9v1, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r9v2, types: [X.3HV] */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0005, code lost:
        if (r23 != null) goto L_0x0007;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass3HV A00(X.C15820nx r21, java.lang.String r22, java.lang.String r23, org.json.JSONObject r24, long r25) {
        /*
            r11 = r23
            if (r22 != 0) goto L_0x0007
            r1 = 0
            if (r23 == 0) goto L_0x0008
        L_0x0007:
            r1 = 1
        L_0x0008:
            java.lang.String r0 = "Either prefix or file upload path must be non-null"
            X.AnonymousClass009.A0B(r0, r1)
            java.lang.String r5 = "name"
            r0 = r24
            java.lang.String r12 = r0.optString(r5)
            java.lang.String r1 = "mimeType"
            java.lang.String r13 = r0.optString(r1)
            java.lang.String r4 = "md5Hash"
            java.lang.String r6 = r0.optString(r4)
            java.lang.String r3 = "sizeBytes"
            java.lang.String r9 = r0.optString(r3)
            java.lang.String r1 = "updateTime"
            java.lang.String r8 = r0.optString(r1)
            boolean r2 = android.text.TextUtils.isEmpty(r12)
            if (r2 != 0) goto L_0x00e3
            boolean r2 = android.text.TextUtils.isEmpty(r13)
            if (r2 != 0) goto L_0x00e3
            boolean r2 = android.text.TextUtils.isEmpty(r6)
            if (r2 != 0) goto L_0x00e3
            boolean r2 = android.text.TextUtils.isEmpty(r8)
            if (r2 != 0) goto L_0x00e3
            r2 = 2
            byte[] r2 = android.util.Base64.decode(r6, r2)
            java.lang.String r14 = X.C003501n.A04(r2)
            if (r23 != 0) goto L_0x005a
            int r2 = r22.length()
            java.lang.String r11 = r12.substring(r2)
        L_0x005a:
            r6 = r25
            long r15 = X.C28421Nd.A01(r9, r6)
            android.text.format.Time r6 = new android.text.format.Time
            r6.<init>()
            r6.parse3339(r8)
            r2 = 1
            long r17 = r6.toMillis(r2)
            java.lang.String r2 = "metadata"
            java.lang.String r7 = r0.optString(r2)
            java.lang.String r6 = "\""
            java.lang.String r9 = "gdrive/file-metadata/failed to parse metadata \""
            r10 = 0
            r8 = r21
            java.lang.String r0 = r8.A00(r7)     // Catch: Exception -> 0x00cb
            if (r0 == 0) goto L_0x00dd
            org.json.JSONObject r2 = X.C13000ix.A05(r0)     // Catch: JSONException | TimeFormatException -> 0x00bb, Exception -> 0x00cb
            java.lang.String r21 = r2.optString(r5)     // Catch: JSONException | TimeFormatException -> 0x00bb, Exception -> 0x00cb
            java.lang.String r22 = r2.optString(r4)     // Catch: JSONException | TimeFormatException -> 0x00bb, Exception -> 0x00cb
            long r23 = r2.optLong(r3)     // Catch: JSONException | TimeFormatException -> 0x00bb, Exception -> 0x00cb
            java.lang.String r1 = r2.optString(r1)     // Catch: JSONException | TimeFormatException -> 0x00bb, Exception -> 0x00cb
            boolean r2 = android.text.TextUtils.isEmpty(r21)     // Catch: JSONException | TimeFormatException -> 0x00bb, Exception -> 0x00cb
            if (r2 != 0) goto L_0x00dd
            boolean r2 = android.text.TextUtils.isEmpty(r22)     // Catch: JSONException | TimeFormatException -> 0x00bb, Exception -> 0x00cb
            if (r2 != 0) goto L_0x00dd
            boolean r2 = android.text.TextUtils.isEmpty(r1)     // Catch: JSONException | TimeFormatException -> 0x00bb, Exception -> 0x00cb
            if (r2 != 0) goto L_0x00dd
            android.text.format.Time r2 = new android.text.format.Time     // Catch: JSONException | TimeFormatException -> 0x00bb, Exception -> 0x00cb
            r2.<init>()     // Catch: JSONException | TimeFormatException -> 0x00bb, Exception -> 0x00cb
            r2.parse3339(r1)     // Catch: JSONException | TimeFormatException -> 0x00bb, Exception -> 0x00cb
            r1 = 1
            long r25 = r2.toMillis(r1)     // Catch: JSONException | TimeFormatException -> 0x00bb, Exception -> 0x00cb
            r20 = r8
            X.3FI r19 = new X.3FI     // Catch: JSONException | TimeFormatException -> 0x00bb, Exception -> 0x00cb
            r19.<init>(r20, r21, r22, r23, r25)     // Catch: JSONException | TimeFormatException -> 0x00bb, Exception -> 0x00cb
            goto L_0x00db
        L_0x00bb:
            r2 = move-exception
            java.lang.StringBuilder r1 = X.C12960it.A0j(r9)     // Catch: Exception -> 0x00cb
            r1.append(r0)     // Catch: Exception -> 0x00cb
            java.lang.String r0 = X.C12960it.A0d(r6, r1)     // Catch: Exception -> 0x00cb
            com.whatsapp.util.Log.e(r0, r2)     // Catch: Exception -> 0x00cb
            goto L_0x00dd
        L_0x00cb:
            r1 = move-exception
            java.lang.StringBuilder r0 = X.C12960it.A0j(r9)
            r0.append(r7)
            java.lang.String r0 = X.C12960it.A0d(r6, r0)
            com.whatsapp.util.Log.e(r0, r1)
            goto L_0x00dd
        L_0x00db:
            r10 = r19
        L_0x00dd:
            X.3HV r9 = new X.3HV
            r9.<init>(r10, r11, r12, r13, r14, r15, r17)
            return r9
        L_0x00e3:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3HV.A00(X.0nx, java.lang.String, java.lang.String, org.json.JSONObject, long):X.3HV");
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || AnonymousClass3HV.class != obj.getClass()) {
                return false;
            }
            AnonymousClass3HV r7 = (AnonymousClass3HV) obj;
            if (this.A00 != r7.A00 || this.A01 != r7.A01 || !this.A05.equals(r7.A05) || !this.A06.equals(r7.A06) || !this.A04.equals(r7.A04) || !this.A03.equals(r7.A03) || !C29941Vi.A00(this.A02, r7.A02)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] objArr = new Object[7];
        objArr[0] = this.A05;
        objArr[1] = this.A06;
        objArr[2] = this.A04;
        objArr[3] = this.A03;
        objArr[4] = Long.valueOf(this.A00);
        objArr[5] = Long.valueOf(this.A01);
        return C12980iv.A0B(this.A02, objArr, 6);
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("RemoteFile{name='");
        char A00 = C12990iw.A00(this.A05, A0k);
        A0k.append(", uploadTitle='");
        A0k.append(this.A06);
        A0k.append(A00);
        A0k.append(", mimeType='");
        A0k.append(this.A04);
        A0k.append(A00);
        A0k.append(", md5Hash='");
        A0k.append(this.A03);
        A0k.append(A00);
        A0k.append(", sizeBytes=");
        A0k.append(this.A00);
        A0k.append(", updateTime=");
        A0k.append(this.A01);
        A0k.append(", metadata=");
        A0k.append(this.A02);
        return C12970iu.A0v(A0k);
    }
}
