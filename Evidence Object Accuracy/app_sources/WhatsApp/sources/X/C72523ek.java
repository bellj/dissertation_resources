package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;

/* renamed from: X.3ek  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C72523ek extends AnimatorListenerAdapter {
    public final /* synthetic */ C47342Ag A00;

    public C72523ek(C47342Ag r1) {
        this.A00 = r1;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        super.onAnimationEnd(animator);
        ValueAnimator valueAnimator = this.A00.A0C;
        valueAnimator.removeAllUpdateListeners();
        valueAnimator.removeAllListeners();
    }
}
