package X;

import java.util.Locale;

/* renamed from: X.1xF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C43571xF {
    public final String A00;
    public final Locale A01;

    public C43571xF(String str, Locale locale) {
        this.A01 = new Locale(locale.getLanguage(), locale.getCountry());
        this.A00 = str;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            C43571xF r5 = (C43571xF) obj;
            if (this.A01.equals(r5.A01)) {
                String str = this.A00;
                String str2 = r5.A00;
                if (str != null) {
                    return str.equals(str2);
                }
                if (str2 != null) {
                    return false;
                }
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = this.A01.hashCode() * 31;
        String str = this.A00;
        if (str != null) {
            i = str.hashCode();
        }
        return hashCode + i;
    }
}
