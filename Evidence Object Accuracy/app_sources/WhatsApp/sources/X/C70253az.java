package X;

import android.graphics.Bitmap;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.util.Log;

/* renamed from: X.3az  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70253az implements AbstractC41521tf {
    public final /* synthetic */ C64853Hd A00;

    public C70253az(C64853Hd r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC41521tf
    public int AGm() {
        return C12960it.A09(this.A00.A0H).getDimensionPixelSize(R.dimen.conversation_row_sticker_size);
    }

    @Override // X.AbstractC41521tf
    public void AQV() {
        Log.w("ConversationRowSticker/onFileReadError");
        this.A00.A01 = false;
    }

    @Override // X.AbstractC41521tf
    public void Adg(Bitmap bitmap, View view, AbstractC15340mz r5) {
        if (bitmap == null || !(r5 instanceof AbstractC16130oV)) {
            C64853Hd r1 = this.A00;
            r1.A01 = false;
            r1.A0H.setImageResource(R.drawable.sticker_error_in_conversation);
            return;
        }
        this.A00.A0H.setImageBitmap(bitmap);
    }

    @Override // X.AbstractC41521tf
    public void Adu(View view) {
        Log.w("ConversationRowSticker/showPlaceholder");
        C64853Hd r1 = this.A00;
        r1.A01 = false;
        r1.A0H.setImageResource(R.drawable.sticker_error_in_conversation);
    }
}
