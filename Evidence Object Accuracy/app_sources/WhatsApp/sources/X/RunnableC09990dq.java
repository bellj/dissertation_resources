package X;

import android.view.View;

/* renamed from: X.0dq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09990dq implements Runnable {
    public final /* synthetic */ int A00;
    public final /* synthetic */ View A01;
    public final /* synthetic */ AnonymousClass0BC A02;

    public RunnableC09990dq(View view, AnonymousClass0BC r2, int i) {
        this.A02 = r2;
        this.A01 = view;
        this.A00 = i;
    }

    @Override // java.lang.Runnable
    public void run() {
        AnonymousClass0BC r6 = this.A02;
        if (r6.A06 != null) {
            int height = r6.getHeight();
            AbstractC12080hL r0 = r6.A06;
            View view = this.A01;
            C06210Sp r3 = r6.A07;
            int i = r3.A03;
            int i2 = this.A00;
            r3.A08 = view;
            r3.A02 = -1;
            if (r3.A0A(height - r0.AFn(view, height), 0, i2)) {
                r6.postInvalidateOnAnimation();
                return;
            }
            AnonymousClass0P3 r02 = r6.A04;
            if (r02 != null && i == 0) {
                AbstractC12080hL r2 = r6.A06;
                AnonymousClass09V r1 = r02.A00;
                r1.A09.A07.A03();
                if (r2 == AnonymousClass09V.A0H) {
                    if (!r1.A0D) {
                        r1.A03(EnumC03710Iv.SWIPE_AWAY);
                    }
                    r1.A00();
                }
            }
        }
    }
}
