package X;

import android.animation.Animator;
import android.animation.FloatEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Rect;
import android.transition.Transition;
import android.transition.TransitionValues;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;

/* renamed from: X.2ad  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52332ad extends Transition {
    public int A00;
    public int A01;
    public final Context A02;
    public final Rect A03 = C12980iv.A0J();
    public final AnonymousClass2TT A04;
    public final boolean A05;
    public final int[] A06 = C13000ix.A07();

    public C52332ad(Context context, AnonymousClass2TT r3, boolean z) {
        this.A04 = r3;
        this.A05 = z;
        this.A02 = context;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0057, code lost:
        if (r0 == 0) goto L_0x0059;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ void A00(android.animation.ValueAnimator r7, android.transition.TransitionValues r8, X.C52332ad r9) {
        /*
            android.view.View r4 = r8.view
            boolean r0 = r4 instanceof com.whatsapp.mediaview.PhotoView
            if (r0 != 0) goto L_0x000a
            boolean r0 = r4 instanceof com.whatsapp.components.button.ThumbnailButton
            if (r0 == 0) goto L_0x0063
        L_0x000a:
            float r7 = X.C12960it.A00(r7)
            boolean r0 = r9.A05
            if (r0 == 0) goto L_0x0016
            r0 = 1065353216(0x3f800000, float:1.0)
            float r7 = r0 - r7
        L_0x0016:
            int[] r1 = r9.A06
            r4.getLocationOnScreen(r1)
            r0 = 1
            r8 = r1[r0]
            int r0 = r4.getHeight()
            int r1 = r8 + r0
            android.graphics.Rect r5 = r9.A03
            r6 = 0
            r5.left = r6
            int r0 = r4.getWidth()
            r5.right = r0
            int r0 = r9.A00
            r3 = 0
            if (r1 <= r0) goto L_0x0067
            if (r0 <= 0) goto L_0x0067
            int r0 = (r7 > r3 ? 1 : (r7 == r3 ? 0 : -1))
            if (r0 <= 0) goto L_0x0067
            int r2 = r4.getHeight()
            int r0 = r9.A00
            int r1 = r1 - r0
            float r0 = (float) r1
            float r0 = r0 * r7
            int r0 = (int) r0
            int r2 = r2 - r0
        L_0x0045:
            r5.bottom = r2
            int r1 = r9.A01
            if (r8 >= r1) goto L_0x0064
            if (r1 <= 0) goto L_0x0064
            int r0 = (r7 > r3 ? 1 : (r7 == r3 ? 0 : -1))
            if (r0 <= 0) goto L_0x0064
            int r1 = r1 - r8
            float r0 = (float) r1
            float r7 = r7 * r0
            int r0 = (int) r7
            r5.top = r0
            if (r0 != 0) goto L_0x006c
        L_0x0059:
            int r0 = r4.getHeight()
            if (r2 != r0) goto L_0x006c
            r0 = 0
            X.AnonymousClass028.A0f(r4, r0)
        L_0x0063:
            return
        L_0x0064:
            r5.top = r6
            goto L_0x0059
        L_0x0067:
            int r2 = r4.getHeight()
            goto L_0x0045
        L_0x006c:
            X.AnonymousClass028.A0f(r4, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C52332ad.A00(android.animation.ValueAnimator, android.transition.TransitionValues, X.2ad):void");
    }

    @Override // android.transition.Transition
    public void captureEndValues(TransitionValues transitionValues) {
        if (!this.A05) {
            AnonymousClass2TT r3 = this.A04;
            if (r3.A00(R.string.transition_clipper_top).equals(AnonymousClass028.A0J(transitionValues.view))) {
                View view = transitionValues.view;
                int[] iArr = this.A06;
                view.getLocationOnScreen(iArr);
                this.A01 = iArr[1];
            }
            if (r3.A00(R.string.transition_clipper_bottom).equals(AnonymousClass028.A0J(transitionValues.view))) {
                View view2 = transitionValues.view;
                int[] iArr2 = this.A06;
                view2.getLocationOnScreen(iArr2);
                this.A00 = iArr2[1] + transitionValues.view.getHeight();
            }
        }
    }

    @Override // android.transition.Transition
    public void captureStartValues(TransitionValues transitionValues) {
        if (this.A05) {
            AnonymousClass2TT r3 = this.A04;
            if (r3.A00(R.string.transition_clipper_top).equals(AnonymousClass028.A0J(transitionValues.view))) {
                View view = transitionValues.view;
                int[] iArr = this.A06;
                view.getLocationOnScreen(iArr);
                this.A01 = iArr[1];
            }
            if (r3.A00(R.string.transition_clipper_bottom).equals(AnonymousClass028.A0J(transitionValues.view))) {
                View view2 = transitionValues.view;
                int[] iArr2 = this.A06;
                view2.getLocationOnScreen(iArr2);
                this.A00 = iArr2[1] + transitionValues.view.getHeight();
            }
        }
    }

    @Override // android.transition.Transition
    public Animator createAnimator(ViewGroup viewGroup, TransitionValues transitionValues, TransitionValues transitionValues2) {
        if (transitionValues == null || transitionValues2 == null) {
            return null;
        }
        FloatEvaluator floatEvaluator = new FloatEvaluator();
        Object[] A1a = C12980iv.A1a();
        A1a[0] = 0;
        A1a[1] = 1;
        ValueAnimator ofObject = ValueAnimator.ofObject(floatEvaluator, A1a);
        ofObject.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(transitionValues2, this) { // from class: X.4eg
            public final /* synthetic */ TransitionValues A00;
            public final /* synthetic */ C52332ad A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                C52332ad.A00(valueAnimator, this.A00, this.A01);
            }
        });
        return ofObject;
    }
}
