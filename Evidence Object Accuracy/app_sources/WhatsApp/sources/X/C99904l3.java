package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.fieldstats.extension.WamCallExtendedField;

/* renamed from: X.4l3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C99904l3 implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new WamCallExtendedField(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new WamCallExtendedField[i];
    }
}
