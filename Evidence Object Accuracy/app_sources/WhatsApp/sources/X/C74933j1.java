package X;

import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.gifsearch.GifSearchContainer;

/* renamed from: X.3j1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74933j1 extends AbstractC018308n {
    public final int A00;
    public final /* synthetic */ GifSearchContainer A01;

    public C74933j1(GifSearchContainer gifSearchContainer) {
        this.A01 = gifSearchContainer;
        this.A00 = gifSearchContainer.getResources().getDimensionPixelSize(R.dimen.selected_contacts_top_offset);
    }

    @Override // X.AbstractC018308n
    public void A01(Rect rect, View view, C05480Ps r5, RecyclerView recyclerView) {
        int i = this.A00;
        rect.set(0, i, i, 0);
    }
}
