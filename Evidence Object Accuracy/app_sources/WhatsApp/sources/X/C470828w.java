package X;

import android.content.Context;
import com.whatsapp.R;

/* renamed from: X.28w  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C470828w implements AbstractC470728v {
    public final AnonymousClass1KS A00;
    public final AnonymousClass1AB A01;

    @Override // X.AbstractC470728v
    public boolean A6v() {
        return true;
    }

    @Override // X.AbstractC470728v
    public boolean Aac() {
        return true;
    }

    public C470828w(AnonymousClass1KS r1, AnonymousClass1AB r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC470728v
    public AbstractC454821u A8X(Context context, AnonymousClass018 r6, boolean z) {
        int i = R.dimen.doodle_sticker_shape_size;
        if (z) {
            i = R.dimen.doodle_shape_picker_sticker_size;
        }
        AnonymousClass009.A00();
        return new AnonymousClass33D(context, this.A00, this.A01, context.getResources().getDimensionPixelSize(i));
    }

    @Override // X.AbstractC470728v
    public C37471mS[] ACh() {
        C37471mS[] r0;
        AnonymousClass1KB r02 = this.A00.A04;
        return (r02 == null || (r0 = r02.A08) == null) ? AbstractC470728v.A00 : r0;
    }

    @Override // X.AbstractC470728v
    public String AH5() {
        StringBuilder sb = new StringBuilder("StickerShapeCreator:");
        String str = this.A00.A0C;
        AnonymousClass009.A05(str);
        sb.append(str);
        return sb.toString();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof C470828w)) {
            return false;
        }
        String str = ((C470828w) obj).A00.A0C;
        AnonymousClass009.A05(str);
        return str.equals(this.A00.A0C);
    }

    public int hashCode() {
        String str = this.A00.A0C;
        AnonymousClass009.A05(str);
        return str.hashCode();
    }
}
