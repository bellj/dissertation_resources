package X;

import com.facebook.redex.RunnableBRunnable0Shape10S0100000_I0_10;

/* renamed from: X.1GO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1GO implements AbstractC22060yS {
    public final AbstractC21180x0 A00;
    public final AbstractC14440lR A01;
    public final Runnable A02 = new RunnableBRunnable0Shape10S0100000_I0_10(this);

    @Override // X.AbstractC22060yS
    public void AMG() {
    }

    public AnonymousClass1GO(AbstractC21180x0 r2, AbstractC14440lR r3) {
        this.A01 = r3;
        this.A00 = r2;
    }

    @Override // X.AbstractC22060yS
    public void AMF() {
        this.A01.Ab4(this.A02, "qpl_on_app_bg");
    }
}
