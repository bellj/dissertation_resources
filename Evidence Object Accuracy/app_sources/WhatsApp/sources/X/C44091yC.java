package X;

import android.util.Pair;
import android.util.SparseIntArray;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1yC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C44091yC extends ArrayList<AnonymousClass2Vu> {
    public static final C51652Vt A00 = new C51652Vt(Integer.valueOf((int) R.string.kic_search_results_header), 1);
    public static final C51652Vt A01 = new C51652Vt(Integer.valueOf((int) R.string.search_section_messages), 1);
    public static final C51652Vt A02 = new C51652Vt(false, 13);
    public static final C51652Vt A03 = new C51652Vt(true, 13);
    public static final C51652Vt A04 = new C51652Vt(Integer.valueOf((int) R.string.search_section_starred_messages), 1);
    public AnonymousClass2WL latestBucket = null;
    public final AnonymousClass2WM timeBucketsProvider;

    public C44091yC(C16590pI r3, AnonymousClass018 r4) {
        this.timeBucketsProvider = new AnonymousClass2WM(r3.A00, r4);
    }

    public int A00(int i) {
        if (i < 0 || i >= size()) {
            return -1;
        }
        return get(i).A00;
    }

    public AbstractC51662Vw A01(int i) {
        return (AbstractC51662Vw) get(i);
    }

    public AbstractC15340mz A02(int i) {
        return (AbstractC15340mz) get(i).A01;
    }

    public void A03(SparseIntArray sparseIntArray) {
        add(new C51652Vt(sparseIntArray.clone(), 4));
    }

    public void A04(AbstractC15340mz r5, boolean z) {
        AnonymousClass2WL A002 = this.timeBucketsProvider.A00(r5.A0I);
        if (!C29941Vi.A00(A002, this.latestBucket)) {
            add(new C51652Vt(new Pair(A002.toString(), Boolean.valueOf(z)), 12));
            this.latestBucket = A002;
        }
    }

    public void A05(List list) {
        add(new C51652Vt(new ArrayList(list), 6));
    }

    public void A06(List list) {
        add(new C51652Vt(list, 23));
    }

    public void A07(List list) {
        add(new C51652Vt(new ArrayList(list), 22));
    }

    public void A08(List list, boolean z) {
        list.size();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            AbstractC15340mz r2 = (AbstractC15340mz) it.next();
            if (r2 instanceof AnonymousClass1XR) {
                if (z) {
                    A04(r2, true);
                }
                add(new C51652Vt(r2, 8));
            }
        }
    }

    public void A09(List list, boolean z) {
        list.size();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            AbstractC15340mz r2 = (AbstractC15340mz) it.next();
            if (r2 instanceof AnonymousClass1X7) {
                if (z) {
                    A04(r2, true);
                }
                add(new C51652Vt(r2, 9));
            }
        }
    }

    public void A0A(List list, boolean z) {
        AnonymousClass2Vu r2;
        list.size();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            AbstractC15340mz r1 = (AbstractC15340mz) it.next();
            if (r1 != null) {
                if (z) {
                    A04(r1, false);
                }
                if (r1 instanceof C28861Ph) {
                    if (C33771f3.A05(r1.A0I(), true) != null) {
                        r2 = new C51652Vt(r1, 19);
                    } else {
                        r2 = new C51652Vt(r1, 7);
                    }
                } else if (r1 instanceof C16440p1) {
                    r2 = new C51652Vt(r1, 11);
                } else if (r1 instanceof AnonymousClass1XP) {
                    r2 = new C51652Vt(r1, 20);
                } else if (r1 instanceof C30421Xi) {
                    C30421Xi r12 = (C30421Xi) r1;
                    if (r12.A1C()) {
                        r2 = new C51652Vt(r12, 15);
                    } else {
                        r2 = new C51652Vt(r12, 14);
                    }
                } else {
                    if (!(r1 instanceof AnonymousClass1XV)) {
                        if (r1 instanceof AnonymousClass1X7) {
                            r2 = new C51652Vt(r1, 16);
                        } else if (r1 instanceof AnonymousClass1X2) {
                            r2 = new C51652Vt(r1, 17);
                        } else if (r1 instanceof AnonymousClass1XR) {
                            r2 = new C51652Vt(r1, 18);
                        } else if (r1 instanceof C30411Xh) {
                            r2 = new C51652Vt(r1, 21);
                        } else if (r1 instanceof C30351Xb) {
                            r2 = new C51652Vt(r1, 21);
                        }
                    }
                    r2 = new AnonymousClass2WN(r1);
                }
                add(r2);
            }
        }
    }

    public void A0B(List list, boolean z) {
        list.size();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            AbstractC15340mz r2 = (AbstractC15340mz) it.next();
            if (z) {
                A04(r2, true);
            }
            if (r2 instanceof AnonymousClass1X2) {
                add(new C51652Vt(r2, 10));
            }
        }
    }
}
