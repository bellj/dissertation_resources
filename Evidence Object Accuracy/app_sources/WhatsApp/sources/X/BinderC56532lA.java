package X;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

/* renamed from: X.2lA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class BinderC56532lA extends BinderC73273fx implements IInterface {
    public AbstractC95064d1 A00;
    public final int A01;

    public BinderC56532lA(AbstractC95064d1 r2, int i) {
        super("com.google.android.gms.common.internal.IGmsCallbacks");
        this.A00 = r2;
        this.A01 = i;
    }

    @Override // X.BinderC73273fx
    public final boolean A00(int i, Parcel parcel, Parcel parcel2, int i2) {
        C56412kq r2;
        if (i == 1) {
            int readInt = parcel.readInt();
            IBinder readStrongBinder = parcel.readStrongBinder();
            C13020j0.A02(this.A00, "onPostInitComplete can be called only once per call to getRemoteService");
            this.A00.A08((Bundle) C12970iu.A0F(parcel, Bundle.CREATOR), readStrongBinder, readInt, this.A01);
        } else if (i == 2) {
            parcel.readInt();
            Parcelable.Creator creator = Bundle.CREATOR;
            if (parcel.readInt() != 0) {
                creator.createFromParcel(parcel);
            }
            Log.wtf("GmsClient", "received deprecated onAccountValidationComplete callback, ignoring", new Exception());
            parcel2.writeNoException();
            return true;
        } else if (i != 3) {
            return false;
        } else {
            int readInt2 = parcel.readInt();
            IBinder readStrongBinder2 = parcel.readStrongBinder();
            C56472kw r7 = (C56472kw) C12970iu.A0F(parcel, C56472kw.CREATOR);
            AbstractC95064d1 r1 = this.A00;
            C13020j0.A02(r1, "onPostInitCompleteWithConnectionInfo can be called only once per call togetRemoteService");
            C13020j0.A01(r7);
            r1.A0Q = r7;
            if (r1.A0A()) {
                C56422kr r0 = r7.A02;
                C94784cX A00 = C94784cX.A00();
                if (r0 == null) {
                    r2 = null;
                } else {
                    r2 = r0.A01;
                }
                synchronized (A00) {
                    if (r2 == null) {
                        r2 = C94784cX.A02;
                    } else {
                        C56412kq r02 = A00.A00;
                        if (r02 != null) {
                            if (r02.A00 < r2.A00) {
                            }
                        }
                    }
                    A00.A00 = r2;
                }
            }
            Bundle bundle = r7.A01;
            C13020j0.A02(this.A00, "onPostInitComplete can be called only once per call to getRemoteService");
            this.A00.A08(bundle, readStrongBinder2, readInt2, this.A01);
        }
        this.A00 = null;
        parcel2.writeNoException();
        return true;
    }
}
