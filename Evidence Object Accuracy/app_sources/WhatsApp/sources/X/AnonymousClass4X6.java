package X;

import android.content.Context;
import android.os.Looper;
import android.view.accessibility.CaptioningManager;
import java.util.Locale;

/* renamed from: X.4X6  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4X6 {
    public int A00 = 0;
    public AnonymousClass1Mr A01;
    public AnonymousClass1Mr A02;

    @Deprecated
    public AnonymousClass4X6() {
        AnonymousClass1Mr of = AnonymousClass1Mr.of();
        this.A01 = of;
        this.A02 = of;
    }

    public final void A00(Context context) {
        CaptioningManager captioningManager;
        String obj;
        int i = AnonymousClass3JZ.A01;
        if ((i >= 23 || Looper.myLooper() != null) && (captioningManager = (CaptioningManager) context.getSystemService("captioning")) != null && captioningManager.isEnabled()) {
            this.A00 = 1088;
            Locale locale = captioningManager.getLocale();
            if (locale != null) {
                if (i >= 21) {
                    obj = AnonymousClass3JZ.A0B(locale);
                } else {
                    obj = locale.toString();
                }
                this.A02 = AnonymousClass1Mr.of((Object) obj);
            }
        }
    }
}
