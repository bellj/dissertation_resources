package X;

import java.io.File;
import java.io.FilenameFilter;

/* renamed from: X.1O2  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1O2 implements FilenameFilter {
    public final /* synthetic */ AnonymousClass1O0 A00;

    public AnonymousClass1O2(AnonymousClass1O0 r1) {
        this.A00 = r1;
    }

    @Override // java.io.FilenameFilter
    public boolean accept(File file, String str) {
        return AnonymousClass1O0.A01.matcher(str).matches();
    }
}
