package X;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.mediacomposer.doodle.shapepicker.ShapeItemView;
import com.whatsapp.util.Log;
import java.lang.ref.Reference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.1KX  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1KX extends AnonymousClass02M implements AnonymousClass1KY {
    public int A00 = 0;
    public int A01 = 0;
    public AnonymousClass4VM A02;
    public LinkedHashMap A03 = new LinkedHashMap();
    public List A04 = new ArrayList();
    public final /* synthetic */ AnonymousClass1KW A05;

    public AnonymousClass1KX(AnonymousClass1KW r2) {
        this.A05 = r2;
    }

    @Override // X.AnonymousClass02M
    public void A0A(AnonymousClass03U r3) {
        if (r3 instanceof C54952hY) {
            ((C54952hY) r3).A01.setImageDrawable(null);
        } else if (r3 instanceof C75523jz) {
            ((C75523jz) r3).A00.setImageDrawable(null);
        }
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        AnonymousClass1KW r1 = this.A05;
        boolean z = true;
        boolean A04 = r1.A0O.A04(1);
        C470028n r3 = r1.A0N;
        int i = 0;
        if (r3 == null || ((Number) r3.A03.A01()).intValue() != 0) {
            z = false;
        }
        int size = this.A04.size();
        if (z && ((String) r3.A02.A01()).isEmpty()) {
            i = this.A01 + (A04 ? 1 : 0);
        }
        return size + i;
    }

    public final C94834cc A0E(int i) {
        Object obj;
        if (i < this.A04.size()) {
            obj = this.A04.get(i);
        } else if (this.A05.A0O.A04(1) && i == A0D() - 1) {
            return new C94834cc(4);
        } else {
            int size = i - this.A04.size();
            for (List list : this.A03.values()) {
                if (size < list.size()) {
                    obj = list.get(size);
                } else {
                    size -= list.size();
                }
            }
            StringBuilder sb = new StringBuilder("Could not translate adapter position ");
            sb.append(i);
            sb.append(" to a grid item.");
            throw new IllegalArgumentException(sb.toString());
        }
        return (C94834cc) obj;
    }

    public final List A0F(AnonymousClass1KZ r6) {
        Collection<C470828w> collection = (Collection) this.A05.A0b.get(r6.A0D);
        AnonymousClass009.A05(collection);
        ArrayList arrayList = new ArrayList();
        arrayList.add(new C94834cc(r6.A0F, 1));
        int i = 0;
        for (C470828w r1 : collection) {
            arrayList.add(new C94834cc(r1, i));
            i++;
        }
        return arrayList;
    }

    public void A0G() {
        ArrayList arrayList;
        int size = this.A04.size();
        this.A04.clear();
        AnonymousClass1KW r14 = this.A05;
        int i = this.A00 << 1;
        if (((Number) r14.A0N.A03.A01()).intValue() == 0) {
            arrayList = new ArrayList();
            AnonymousClass33Q r6 = r14.A0T;
            if (r6 != null && !r6.A01) {
                r6.A01 = true;
                C54452gk r2 = r6.A09;
                r2.A02.add(0);
                Collections.sort(r2.A02);
                r2.A02();
                r2.A0E();
                r6.A01();
            }
            AnonymousClass1BT r3 = r14.A0E;
            boolean z = false;
            if (r3.A03 != null) {
                z = true;
            }
            if (!z) {
                arrayList.add(new C94834cc(r14.A05.getString(R.string.emoji_recents_title), 1));
                arrayList.add(new C94834cc(3));
            } else if (r3.A00() > 0) {
                arrayList.add(new C94834cc(r14.A05.getString(R.string.emoji_recents_title), 1));
                int i2 = 0;
                for (AbstractC470728v r1 : r3.A03(i)) {
                    arrayList.add(new C94834cc(r1, i2));
                    i2++;
                }
            } else if (r6 != null && r6.A01) {
                r6.A01 = false;
                C54452gk r12 = r6.A09;
                r12.A02.remove(0);
                Collections.sort(r12.A02);
                r12.A02();
                r12.A0E();
                r6.A01();
            }
            EnumC869149l[] values = EnumC869149l.values();
            for (EnumC869149l r32 : values) {
                arrayList.add(new C94834cc(r14.A05.getString(r32.sectionResId), 1));
                AbstractC470728v[] r9 = r32.shapeData;
                int i3 = 0;
                for (AbstractC470728v r13 : r9) {
                    arrayList.add(new C94834cc(r13, i3));
                    i3++;
                }
            }
            AnonymousClass016 r15 = r14.A0O.A00;
            C90424Nv r0 = (C90424Nv) r15.A01();
            if (r0 != null && r0.A01) {
                Set<C470828w> set = r14.A0c;
                if (set.size() > 0) {
                    arrayList.add(new C94834cc(r14.A05.getString(R.string.shape_picker_favorite_stickers), 1));
                    int i4 = 0;
                    for (C470828w r16 : set) {
                        arrayList.add(new C94834cc(r16, i4));
                        i4++;
                    }
                }
            }
            C90424Nv r02 = (C90424Nv) r15.A01();
            if (r02 == null || !r02.A01) {
                arrayList.add(new C94834cc(r14.A05.getString(R.string.shape_picker_favorite_stickers), 1));
                arrayList.add(new C94834cc(3));
            }
        } else {
            arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            EnumC452920z[] values2 = EnumC452920z.values();
            for (EnumC452920z r22 : values2) {
                arrayList2.add(Integer.valueOf(arrayList.size()));
                arrayList.add(new C94834cc(r14.A05.getString(r22.titleResId), 1));
                int i5 = 0;
                for (int[] iArr : r22.emojiData) {
                    AnonymousClass3YL r23 = new AnonymousClass3YL(new C37471mS(iArr), r14.A0C);
                    arrayList.add(new C94834cc(r23, i5));
                    i5++;
                    r14.A0f.put(r23.AH5(), r23);
                }
            }
            AnonymousClass33R r33 = r14.A0S;
            boolean z2 = false;
            if (arrayList2.size() == AnonymousClass33R.A01.length) {
                z2 = true;
            }
            AnonymousClass009.A0E(z2);
            C54452gk r03 = r33.A09;
            r03.A02 = arrayList2;
            Collections.sort(arrayList2);
            r03.A02();
            r03.A0E();
            r33.A01();
        }
        this.A04 = arrayList;
        super.A01.A04(null, 0, Math.max(size, arrayList.size()));
        if (size != this.A04.size()) {
            A0I();
        }
    }

    public final void A0H() {
        this.A01 = 0;
        for (List list : this.A03.values()) {
            if (list.size() > 1) {
                this.A01 += list.size();
            } else {
                return;
            }
        }
    }

    public final void A0I() {
        AnonymousClass1KW r6 = this.A05;
        AnonymousClass33Q r2 = r6.A0T;
        if (r2 != null) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            int i = 0;
            for (Map.Entry entry : this.A03.entrySet()) {
                if (i >= this.A01 - 1) {
                    break;
                }
                arrayList.add(r6.A0a.get(entry.getKey()));
                arrayList2.add(Integer.valueOf(this.A04.size() + i));
                i += ((List) entry.getValue()).size();
            }
            boolean z = false;
            if (arrayList.size() == arrayList2.size()) {
                z = true;
            }
            AnonymousClass009.A0E(z);
            List list = r2.A03;
            list.clear();
            list.addAll(arrayList);
            ArrayList arrayList3 = new ArrayList(arrayList2);
            if (r2.A01) {
                arrayList3.add(0);
            }
            C54452gk r0 = r2.A09;
            r0.A02 = arrayList3;
            Collections.sort(arrayList3);
            r0.A02();
            r0.A0E();
            r2.A01();
        }
    }

    public void A0J(String str) {
        List list = (List) this.A03.get(str);
        if (list != null) {
            int i = 0;
            for (Map.Entry entry : this.A03.entrySet()) {
                if (str.equals(entry.getKey())) {
                    int size = i + this.A04.size();
                    this.A03.remove(str);
                    A0H();
                    super.A01.A03(size, list.size());
                    A0I();
                    return;
                }
                i += ((List) entry.getValue()).size();
            }
            StringBuilder sb = new StringBuilder("Sticker pack id ");
            sb.append(str);
            sb.append(" is not contained in data set");
            throw new IllegalArgumentException(sb.toString());
        }
    }

    @Override // X.AnonymousClass02M
    public void ANH(AnonymousClass03U r11, int i) {
        String A0H;
        C90414Nu r1;
        C94834cc A0E = A0E(i);
        int i2 = A0E.A02;
        if (i2 == 0) {
            C54952hY r112 = (C54952hY) r11;
            ShapeItemView shapeItemView = r112.A01;
            AbstractC470728v r5 = A0E.A03;
            AnonymousClass009.A05(r5);
            r112.A00 = r5;
            shapeItemView.A01 = r5.AH5();
            AbstractC470728v r4 = r112.A00;
            if (r4.Aac()) {
                AnonymousClass1KW r2 = this.A05;
                Reference reference = (Reference) r2.A0d.get(r5.AH5());
                if (reference == null || (r1 = (C90414Nu) reference.get()) == null) {
                    shapeItemView.setImageDrawable(new AnonymousClass2ZO());
                    shapeItemView.setContentDescription(null);
                    HandlerC52072aC r22 = r2.A0K;
                    Message obtain = Message.obtain(r22, 0, 0, 0, shapeItemView);
                    obtain.setData(AnonymousClass1KW.A00(r5.AH5()));
                    r22.sendMessageAtFrontOfQueue(obtain);
                    return;
                }
                shapeItemView.setImageDrawable(r1.A00);
                A0H = r1.A01;
            } else {
                Context context = shapeItemView.getContext();
                AnonymousClass1KW r23 = this.A05;
                AbstractC454821u A8X = r4.A8X(context, r23.A0B, true);
                A8X.A09(r23.A01);
                if (A8X.A0K()) {
                    A8X.A0O(r23.A00);
                }
                shapeItemView.setImageDrawable(new AnonymousClass2ZP(A8X));
                A0H = A8X.A0H(r23.A05);
            }
            shapeItemView.setContentDescription(A0H);
        } else if (i2 == 1) {
            ((C75443jr) r11).A00.setText(A0E.A04);
        } else if (i2 == 2) {
            AnonymousClass1KW r0 = this.A05;
            AnonymousClass19M r9 = r0.A0C;
            Activity activity = r0.A05;
            Drawable A05 = r9.A05(activity.getResources(), new C39511q1(new int[]{129335}), -1);
            C75523jz r113 = (C75523jz) r11;
            r113.A01.setText(activity.getString(R.string.stickers_no_results, A0E.A04));
            r113.A00.setImageDrawable(A05);
        } else if (i2 != 3 && i2 != 4) {
            StringBuilder sb = new StringBuilder("shapepicker/onBindViewHolder/invalid state ");
            sb.append(i2);
            Log.e(sb.toString());
        }
    }

    @Override // X.AnonymousClass02M
    public AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        if (i == 0) {
            return new C54952hY(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.shape_picker_grid_item, viewGroup, false), this);
        }
        if (i == 1) {
            return new C75443jr(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.shape_picker_section, viewGroup, false), this);
        }
        if (i == 2) {
            return new C75523jz(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.shape_picker_no_results, viewGroup, false), this);
        }
        if (i == 3) {
            return new C75343jh(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.shape_picker_loading_section, viewGroup, false), this);
        }
        if (i == 4) {
            return new C75353ji(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.shape_picker_sticker_packs_loading, viewGroup, false), this);
        }
        StringBuilder sb = new StringBuilder("shapepicker/onCreateViewHolder/invalid state ");
        sb.append(i);
        Log.e(sb.toString());
        return null;
    }

    @Override // X.AnonymousClass1KY
    public void AVQ(AnonymousClass4VM r6) {
        if (r6.equals(this.A02)) {
            this.A04 = new ArrayList();
            List list = r6.A01;
            if (list.size() > 0) {
                for (int i = 0; i < list.size(); i++) {
                    AbstractC470728v r2 = (AbstractC470728v) list.get(i);
                    this.A05.A0f.put(r2.AH5(), r2);
                    this.A04.add(new C94834cc((AbstractC470728v) list.get(i), i));
                }
            } else {
                this.A04.add(new C94834cc((String) this.A05.A0N.A02.A01(), 2));
            }
            A02();
        }
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        return A0E(i).A02;
    }
}
