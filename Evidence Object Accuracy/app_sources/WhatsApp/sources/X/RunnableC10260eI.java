package X;

import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemalarm.RescheduleReceiver;
import java.util.HashSet;

/* renamed from: X.0eI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC10260eI implements Runnable {
    public static final String A02 = C06390Tk.A01("EnqueueRunnable");
    public final C07580Zi A00 = new C07580Zi();
    public final AnonymousClass03v A01;

    public RunnableC10260eI(AnonymousClass03v r2) {
        this.A01 = r2;
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r12v5, types: [java.util.List] */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x02d1  */
    /* JADX WARNING: Removed duplicated region for block: B:149:0x0326  */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x0342  */
    /* JADX WARNING: Removed duplicated region for block: B:162:0x037b  */
    /* JADX WARNING: Removed duplicated region for block: B:253:0x03a8 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:256:0x02cb A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0145  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A00(X.AnonymousClass03v r25) {
        /*
        // Method dump skipped, instructions count: 1102
        */
        throw new UnsupportedOperationException("Method not decompiled: X.RunnableC10260eI.A00(X.03v):boolean");
    }

    @Override // java.lang.Runnable
    public void run() {
        try {
            AnonymousClass03v r5 = this.A01;
            if (!AnonymousClass03v.A01(r5, new HashSet())) {
                AnonymousClass022 r3 = r5.A03;
                WorkDatabase workDatabase = r3.A04;
                workDatabase.A03();
                boolean A00 = A00(r5);
                workDatabase.A05();
                workDatabase.A04();
                if (A00) {
                    AnonymousClass0RF.A00(r3.A01, RescheduleReceiver.class, true);
                    AnonymousClass0TI.A01(r3.A02, workDatabase, r3.A07);
                }
                this.A00.A00(AbstractC12800iW.A01);
                return;
            }
            throw new IllegalStateException(String.format("WorkContinuation has cycles (%s)", r5));
        } catch (Throwable th) {
            this.A00.A00(new AnonymousClass0GP(th));
        }
    }
}
