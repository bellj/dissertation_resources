package X;

import com.whatsapp.catalogcategory.view.CategoryThumbnailLoader;
import com.whatsapp.catalogcategory.view.fragment.CatalogCategoryExpandableGroupsListFragment;

/* renamed from: X.3dk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C71933dk extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ CatalogCategoryExpandableGroupsListFragment this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C71933dk(CatalogCategoryExpandableGroupsListFragment catalogCategoryExpandableGroupsListFragment) {
        super(0);
        this.this$0 = catalogCategoryExpandableGroupsListFragment;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        CatalogCategoryExpandableGroupsListFragment catalogCategoryExpandableGroupsListFragment = this.this$0;
        AnonymousClass4JB r0 = catalogCategoryExpandableGroupsListFragment.A03;
        if (r0 != null) {
            return new CategoryThumbnailLoader(catalogCategoryExpandableGroupsListFragment, new C37071lG(C12990iw.A0V(r0.A00.A01.A1E)));
        }
        throw C16700pc.A06("thumbnailLoaderFactory");
    }
}
