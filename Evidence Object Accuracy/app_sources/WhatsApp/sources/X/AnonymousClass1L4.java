package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.voipcalling.CallInfo;
import com.whatsapp.voipcalling.Voip;

/* renamed from: X.1L4  reason: invalid class name */
/* loaded from: classes2.dex */
public interface AnonymousClass1L4 {
    void Afh(UserJid userJid);

    void Afi(CallInfo callInfo);

    void callStateChanged(Voip.CallState callState, CallInfo callInfo);

    void finish();
}
