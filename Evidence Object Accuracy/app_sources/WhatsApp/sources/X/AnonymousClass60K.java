package X;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.60K  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass60K {
    public static List A02 = Arrays.asList("urn:xmpp:whatsapp:account", "w:pay");
    public final C14900mE A00;
    public final C17220qS A01;

    public AnonymousClass60K(C14900mE r1, C17220qS r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public final AnonymousClass1V8 A00(C126685tH r6, String str, Map map) {
        Map map2 = (Map) map.get("properties");
        Map map3 = (Map) map.get("children");
        if ("accept_pay".equals(str) && map2 != null && map2.containsKey("merchant")) {
            map2.containsKey("merchant-fees");
        }
        ArrayList A0l = C12960it.A0l();
        if (map2 != null && map2.size() > 0) {
            Iterator A0n = C12960it.A0n(map2);
            while (A0n.hasNext()) {
                Map.Entry A15 = C12970iu.A15(A0n);
                C117295Zj.A1M(C12990iw.A0r(A15), C117305Zk.A0m(A15), A0l);
            }
        }
        return new AnonymousClass1V8(str, C117305Zk.A1b(A0l), A02(r6, map3));
    }

    public final Map A01(AnonymousClass1V8 r8) {
        HashMap A11 = C12970iu.A11();
        if (r8 != null) {
            HashMap A112 = C12970iu.A11();
            AnonymousClass1W9[] A0L = r8.A0L();
            if (A0L != null) {
                for (AnonymousClass1W9 r0 : A0L) {
                    A112.put(r0.A02, r0.A03);
                }
            }
            A11.put("properties", AnonymousClass3GC.A01(A112));
            HashMap A113 = C12970iu.A11();
            AnonymousClass1V8[] r2 = r8.A03;
            if (r2 != null) {
                for (AnonymousClass1V8 r02 : r2) {
                    A113.put(r02.A00, A01(r02));
                }
            }
            HashMap A114 = C12970iu.A11();
            Iterator A0s = C12990iw.A0s(A113);
            while (A0s.hasNext()) {
                Map.Entry A15 = C12970iu.A15(A0s);
                A114.put(A15.getKey(), A15.getValue());
            }
            A11.put("children", A114);
        }
        HashMap A115 = C12970iu.A11();
        Iterator A0s2 = C12990iw.A0s(A11);
        while (A0s2.hasNext()) {
            Map.Entry A152 = C12970iu.A15(A0s2);
            A115.put(A152.getKey(), A152.getValue());
        }
        return A115;
    }

    public final AnonymousClass1V8[] A02(C126685tH r5, Map map) {
        ArrayList A0l = C12960it.A0l();
        if (map != null && map.size() > 0) {
            Iterator A0n = C12960it.A0n(map);
            while (A0n.hasNext()) {
                Map.Entry A15 = C12970iu.A15(A0n);
                A0l.add(A00(r5, C12990iw.A0r(A15), (Map) A15.getValue()));
            }
        }
        return (AnonymousClass1V8[]) A0l.toArray(new AnonymousClass1V8[0]);
    }
}
