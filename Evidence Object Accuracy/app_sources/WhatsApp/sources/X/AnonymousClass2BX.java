package X;

import android.util.SparseArray;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/* renamed from: X.2BX  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2BX implements AnonymousClass1N9 {
    public AnonymousClass1NA[] A00;
    public final int A01;
    public final int A02;
    public final AnonymousClass23W A03;
    public final AnonymousClass16F A04;
    public final RandomAccessFile A05;

    @Override // X.AnonymousClass1N9
    public void AKX() {
    }

    @Override // X.AnonymousClass1N9
    public void Aau() {
    }

    public AnonymousClass2BX(AnonymousClass23W r1, AnonymousClass16F r2, RandomAccessFile randomAccessFile, int i, int i2) {
        this.A05 = randomAccessFile;
        this.A03 = r1;
        this.A02 = i;
        this.A01 = i2;
        this.A04 = r2;
    }

    @Override // X.AnonymousClass1N9
    public boolean A6w() {
        AnonymousClass1NA[] r2 = this.A00;
        return r2[(this.A03.A01 + 1) % r2.length].A05();
    }

    @Override // X.AnonymousClass1N9
    public void A7C() {
        for (AnonymousClass1NA r0 : this.A00) {
            r0.A00();
        }
    }

    @Override // X.AnonymousClass1N9
    public AnonymousClass1NA A8e() {
        return this.A00[this.A03.A01];
    }

    @Override // X.AnonymousClass1N9
    public void A9F(List list) {
        if (AIB()) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                int intValue = ((Number) it.next()).intValue();
                if (intValue != this.A03.A01 && !this.A00[intValue].A05()) {
                    this.A00[intValue].A00();
                }
            }
            return;
        }
        throw new Error("InMemoryMultipleEventBuffersManager: Tried to drop empty buffer");
    }

    @Override // X.AnonymousClass1N9
    public boolean AA2() {
        try {
            for (AnonymousClass1NA r0 : this.A00) {
                r0.A01();
            }
            return true;
        } catch (IOException unused) {
            Log.e("InMemoryMultipleEventBufferManager/flushEventBuffers: error while event buffer flush");
            return false;
        }
    }

    @Override // X.AnonymousClass1N9
    public SparseArray ACP() {
        SparseArray sparseArray = new SparseArray();
        AnonymousClass23W r3 = this.A03;
        int i = r3.A01 + 1;
        while (true) {
            AnonymousClass1NA[] r1 = this.A00;
            int length = i % r1.length;
            if (length == r3.A01) {
                return sparseArray;
            }
            AnonymousClass1NA r12 = r1[length];
            if (!r12.A05()) {
                ByteBuffer byteBuffer = r12.A04.A05;
                sparseArray.put(length, Arrays.copyOf(byteBuffer.array(), byteBuffer.position()));
            }
            i = length + 1;
        }
    }

    @Override // X.AnonymousClass1N9
    public int AEc(int i) {
        return (i + 1) % this.A00.length;
    }

    @Override // X.AnonymousClass1N9
    public boolean AIB() {
        AnonymousClass1NA[] r5 = this.A00;
        for (AnonymousClass1NA r1 : r5) {
            if (!(r1.A04() || r1.A05())) {
                return true;
            }
        }
        return false;
    }

    @Override // X.AnonymousClass1N9
    public void AIm() {
        AIx();
        int i = 0;
        while (true) {
            AnonymousClass1NA[] r1 = this.A00;
            if (i < r1.length) {
                AnonymousClass23W r4 = this.A03;
                AnonymousClass23X r6 = r4.A05[i];
                try {
                    r1[i].A02();
                    long seconds = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
                    if (i == r4.A01) {
                        long j = r6.A04;
                        if (j > seconds) {
                            StringBuilder sb = new StringBuilder("InMemoryMultipleEventBuffersManager/initfromfile: current event buffer timestamp is ");
                            sb.append(j - seconds);
                            sb.append(" seconds in the future");
                            Log.w(sb.toString());
                            r6.A04 = seconds;
                        }
                    }
                    Locale locale = Locale.US;
                    Object[] objArr = new Object[6];
                    AnonymousClass1NA A8e = A8e();
                    if (A8e.A04()) {
                        objArr[0] = Integer.valueOf(A8e.A01);
                        AnonymousClass1NA A8e2 = A8e();
                        if (A8e2.A04()) {
                            objArr[1] = Integer.valueOf(A8e2.A00);
                            AnonymousClass1NA A8e3 = A8e();
                            if (A8e3.A04()) {
                                objArr[2] = Integer.valueOf(A8e3.A03.A00.size());
                                objArr[3] = Integer.valueOf(A8e().A04.A05.position());
                                AnonymousClass1NA A8e4 = A8e();
                                objArr[4] = Long.valueOf(A8e4.A05.A05[A8e4.A02].A04);
                                AnonymousClass1NA[] r5 = this.A00;
                                int i2 = 0;
                                for (AnonymousClass1NA r12 : r5) {
                                    if (!r12.A04()) {
                                        i2 += r12.A04.A05.position();
                                    }
                                }
                                objArr[5] = Integer.valueOf(i2);
                                Log.i(String.format(locale, "InMemoryMultipleEventBuffersManager/initfromfile: opened existing wam file: record_count = %d, event_count = %d, attribute_count = %d, size = %d, create_ts = %d, rotated_size = %d", objArr));
                                i++;
                            } else {
                                throw new UnsupportedOperationException("No attribute count available for rotated buffers");
                            }
                        } else {
                            throw new UnsupportedOperationException("No event count available for rotated buffers");
                        }
                    } else {
                        throw new UnsupportedOperationException("No record count available for rotated buffers");
                    }
                } catch (AnonymousClass2CB e) {
                    throw new AnonymousClass2BT(e.toString());
                }
            } else {
                return;
            }
        }
    }

    @Override // X.AnonymousClass1N9
    public void AIx() {
        this.A00 = new AnonymousClass1NA[this.A02];
        int i = 0;
        while (true) {
            AnonymousClass1NA[] r1 = this.A00;
            if (i < r1.length) {
                RandomAccessFile randomAccessFile = this.A05;
                r1[i] = new AnonymousClass1NA(this.A03, this.A04, randomAccessFile, i, this.A01);
                i++;
            } else {
                return;
            }
        }
    }
}
