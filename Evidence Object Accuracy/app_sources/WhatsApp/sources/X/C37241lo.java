package X;

/* renamed from: X.1lo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37241lo {
    public int A00 = 0;
    public boolean A01;
    public final C14820m6 A02;
    public final /* synthetic */ C22950zu A03;

    public /* synthetic */ C37241lo(C14820m6 r2, C22950zu r3, int i, boolean z) {
        this.A03 = r3;
        this.A02 = r2;
        this.A01 = z;
        this.A00 = i;
    }

    public synchronized void A00(int i) {
        int i2 = this.A00 - 1;
        this.A00 = i2;
        if (this.A01 && i2 == 0) {
            this.A03.A08.A0H("account_sync", null);
        }
        if (i == 1) {
            this.A02.A00.edit().remove("account_sync_status_num_retries").apply();
        } else if (i == 2) {
            this.A02.A00.edit().remove("account_sync_picture_num_retries").apply();
        } else if (i == 3) {
            this.A02.A00.edit().remove("account_sync_privacy_num_retries").apply();
        } else if (i == 4) {
            this.A02.A00.edit().remove("account_sync_blocklist_num_retries").apply();
        }
    }
}
