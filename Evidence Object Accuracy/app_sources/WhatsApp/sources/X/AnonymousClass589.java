package X;

/* renamed from: X.589  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass589 implements AnonymousClass1FK {
    public final /* synthetic */ AnonymousClass5UV A00;
    public final /* synthetic */ AnonymousClass1A8 A01;
    public final /* synthetic */ String A02;

    public AnonymousClass589(AnonymousClass5UV r1, AnonymousClass1A8 r2, String str) {
        this.A01 = r2;
        this.A02 = str;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass1FK
    public void AV3(C452120p r4) {
        AnonymousClass1A8.A00(this.A00, this.A01, this.A02);
    }

    @Override // X.AnonymousClass1FK
    public void AVA(C452120p r4) {
        AnonymousClass1A8.A00(this.A00, this.A01, this.A02);
    }

    @Override // X.AnonymousClass1FK
    public void AVB(C452220q r4) {
        AnonymousClass1A8 r2 = this.A01;
        String str = this.A02;
        r2.A00.remove(str);
        r2.A01.remove(str);
    }
}
