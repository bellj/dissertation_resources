package X;

import android.util.SparseArray;
import android.util.SparseIntArray;
import java.lang.ref.SoftReference;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Queue;
import java.util.Set;

/* renamed from: X.4uq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC105924uq implements AbstractC12900ik {
    public boolean A00;
    public final SparseArray A01;
    public final AbstractC11580gW A02;
    public final AnonymousClass4VD A03;
    public final AnonymousClass4VD A04;
    public final C93604aR A05;
    public final AbstractC115035Pl A06;
    public final Class A07 = getClass();
    public final Set A08;

    public AbstractC105924uq(AbstractC11580gW r11, C93604aR r12, AbstractC115035Pl r13) {
        this.A02 = r11;
        this.A05 = r12;
        this.A06 = r13;
        this.A01 = new SparseArray();
        SparseIntArray sparseIntArray = new SparseIntArray(0);
        synchronized (this) {
            SparseArray sparseArray = this.A01;
            sparseArray.clear();
            SparseIntArray sparseIntArray2 = this.A05.A03;
            if (sparseIntArray2 != null) {
                for (int i = 0; i < sparseIntArray2.size(); i++) {
                    int keyAt = sparseIntArray2.keyAt(i);
                    sparseArray.put(keyAt, new AnonymousClass4XA(keyAt, sparseIntArray2.valueAt(i), sparseIntArray.get(keyAt, 0)));
                }
                this.A00 = false;
            } else {
                this.A00 = true;
            }
        }
        this.A08 = Collections.newSetFromMap(new IdentityHashMap());
        this.A03 = new AnonymousClass4VD();
        this.A04 = new AnonymousClass4VD();
    }

    public static void A00(Class cls, Object obj, String str, int i) {
        AnonymousClass0UN.A01(cls, Integer.valueOf(System.identityHashCode(obj)), Integer.valueOf(i), str);
    }

    public int A01(int i) {
        if (!(this instanceof AbstractC75883ke)) {
            C75923ki r0 = (C75923ki) this;
            if (i > 0) {
                int[] iArr = r0.A00;
                for (int i2 : iArr) {
                    if (i2 >= i) {
                        return i2;
                    }
                }
                return i;
            }
            throw new C113185Gm(Integer.valueOf(i));
        }
        AbstractC75883ke r02 = (AbstractC75883ke) this;
        if (i > 0) {
            int[] iArr2 = r02.A00;
            for (int i2 : iArr2) {
                if (i2 >= i) {
                    return i2;
                }
            }
            return i;
        }
        throw new C113185Gm(Integer.valueOf(i));
    }

    public int A02(Object obj) {
        if (!(this instanceof AbstractC75883ke)) {
            return ((byte[]) obj).length;
        }
        return ((AnonymousClass5XR) obj).AGm();
    }

    public synchronized AnonymousClass4XA A03(int i) {
        AnonymousClass4XA r2;
        SparseArray sparseArray = this.A01;
        r2 = (AnonymousClass4XA) sparseArray.get(i);
        if (r2 == null && this.A00) {
            if (AnonymousClass0UN.A00.AJi(2)) {
                AnonymousClass0UN.A02(this.A07, Integer.valueOf(i), "creating new bucket %s");
            }
            if (!(this instanceof C75913kh)) {
                r2 = new AnonymousClass4XA(i, Integer.MAX_VALUE, 0);
            } else {
                r2 = new C75903kg(i, this.A05.A00);
            }
            sparseArray.put(i, r2);
        }
        return r2;
    }

    public Object A04(int i) {
        if (!(this instanceof C75923ki)) {
            return new AnonymousClass5BE(i);
        }
        return new byte[i];
    }

    public synchronized Object A05(AnonymousClass4XA r3) {
        Object A00;
        A00 = r3.A00();
        if (A00 != null) {
            r3.A00++;
        }
        return A00;
    }

    public final void A06() {
        AbstractC12710iN r8 = AnonymousClass0UN.A00;
        if (r8.AJi(2)) {
            Class cls = this.A07;
            AnonymousClass4VD r1 = this.A04;
            Integer valueOf = Integer.valueOf(r1.A00);
            Integer valueOf2 = Integer.valueOf(r1.A01);
            AnonymousClass4VD r12 = this.A03;
            Integer valueOf3 = Integer.valueOf(r12.A00);
            Integer valueOf4 = Integer.valueOf(r12.A01);
            String simpleName = cls.getSimpleName();
            Object[] objArr = new Object[4];
            C12970iu.A1U(valueOf, valueOf2, objArr);
            objArr[2] = valueOf3;
            objArr[3] = valueOf4;
            r8.Aff(simpleName, String.format(null, "Used = (%d, %d); Free = (%d, %d)", objArr));
        }
    }

    public synchronized void A07(int i) {
        AnonymousClass4VD r6 = this.A04;
        int i2 = r6.A01;
        AnonymousClass4VD r5 = this.A03;
        int i3 = r5.A01;
        int i4 = i2 + i3;
        int min = Math.min(i4 - i, i3);
        if (min > 0) {
            if (AnonymousClass0UN.A00.AJi(2)) {
                AnonymousClass0UN.A00(this.A07, Integer.valueOf(i), Integer.valueOf(i4), Integer.valueOf(min), "trimToSize: TargetSize = %d; Initial Size = %d; Bytes to free = %d");
            }
            A06();
            int i5 = 0;
            while (true) {
                SparseArray sparseArray = this.A01;
                if (i5 >= sparseArray.size() || min <= 0) {
                    break;
                }
                AnonymousClass4XA r1 = (AnonymousClass4XA) sparseArray.valueAt(i5);
                while (true) {
                    Object A00 = r1.A00();
                    if (A00 != null) {
                        A08(A00);
                        int i6 = r1.A01;
                        min -= i6;
                        r5.A00(i6);
                        if (min > 0) {
                        }
                    }
                }
                i5++;
            }
            A06();
            if (AnonymousClass0UN.A00.AJi(2)) {
                AnonymousClass0UN.A01(this.A07, Integer.valueOf(i), Integer.valueOf(r6.A01 + r5.A01), "trimToSize: TargetSize = %d; Final Size = %d");
            }
        }
    }

    public void A08(Object obj) {
        if (this instanceof AbstractC75883ke) {
            ((AnonymousClass5XR) obj).close();
        }
    }

    public synchronized boolean A09() {
        return C72463ee.A0Y(this.A04.A01 + this.A03.A01, this.A05.A02);
    }

    public boolean A0A(Object obj) {
        if (!(this instanceof AbstractC75883ke)) {
            return true;
        }
        return !((AnonymousClass5XR) obj).isClosed();
    }

    @Override // X.AbstractC12900ik, X.AbstractC12240hb
    public void Aa4(Object obj) {
        int A02 = A02(obj);
        synchronized (this) {
            AnonymousClass4XA r3 = (AnonymousClass4XA) this.A01.get(A02);
            if (!this.A08.remove(obj)) {
                Class cls = this.A07;
                Object[] objArr = new Object[2];
                C12960it.A1P(objArr, System.identityHashCode(obj), 0);
                C12960it.A1P(objArr, A02, 1);
                AbstractC12710iN r2 = AnonymousClass0UN.A00;
                if (r2.AJi(6)) {
                    r2.A9G(cls.getSimpleName(), String.format(null, "release (free, value unrecognized) (object, size) = (%x, %s)", objArr));
                }
                A08(obj);
            } else {
                if (r3 != null) {
                    int i = r3.A00;
                    Queue queue = r3.A03;
                    if (i + queue.size() > r3.A02 || A09() || !A0A(obj)) {
                        int i2 = r3.A00;
                        AnonymousClass0RA.A01(C12960it.A1U(i2));
                        r3.A00 = i2 - 1;
                    } else {
                        int i3 = r3.A00;
                        if (i3 > 0) {
                            r3.A00 = i3 - 1;
                            if (!(r3 instanceof C75903kg)) {
                                queue.add(obj);
                            } else {
                                C75903kg r32 = (C75903kg) r3;
                                AnonymousClass0OW r1 = (AnonymousClass0OW) r32.A00.poll();
                                if (r1 == null) {
                                    r1 = new AnonymousClass0OW();
                                }
                                r1.A00 = new SoftReference(obj);
                                r1.A01 = new SoftReference(obj);
                                r1.A02 = new SoftReference(obj);
                                r32.A03.add(r1);
                            }
                        } else {
                            Object[] objArr2 = {obj};
                            AbstractC12710iN r12 = AnonymousClass0UN.A00;
                            if (r12.AJi(6)) {
                                r12.A9G("BUCKET", String.format(null, "Tried to release value %s from an empty bucket!", objArr2));
                            }
                        }
                        AnonymousClass4VD r13 = this.A03;
                        r13.A00++;
                        r13.A01 += A02;
                        this.A04.A00(A02);
                        if (AnonymousClass0UN.A00.AJi(2)) {
                            A00(this.A07, obj, "release (reuse) (object, size) = (%x, %s)", A02);
                        }
                    }
                }
                if (AnonymousClass0UN.A00.AJi(2)) {
                    A00(this.A07, obj, "release (free) (object, size) = (%x, %s)", A02);
                }
                A08(obj);
                this.A04.A00(A02);
            }
            A06();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000d, code lost:
        if (r10.A03.A01 == 0) goto L_0x000f;
     */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0085 A[Catch: all -> 0x00ff, TryCatch #6 {all -> 0x00ff, blocks: (B:13:0x0019, B:15:0x001f, B:17:0x0025, B:19:0x0050, B:20:0x0057, B:22:0x0059, B:30:0x007e, B:32:0x0081, B:35:0x0085, B:37:0x0092, B:38:0x0098, B:68:0x00f0, B:69:0x00fb, B:23:0x005a, B:25:0x0066, B:27:0x0071, B:28:0x0075), top: B:83:0x0019 }] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x00f0 A[Catch: all -> 0x00ff, TRY_ENTER, TryCatch #6 {all -> 0x00ff, blocks: (B:13:0x0019, B:15:0x001f, B:17:0x0025, B:19:0x0050, B:20:0x0057, B:22:0x0059, B:30:0x007e, B:32:0x0081, B:35:0x0085, B:37:0x0092, B:38:0x0098, B:68:0x00f0, B:69:0x00fb, B:23:0x005a, B:25:0x0066, B:27:0x0071, B:28:0x0075), top: B:83:0x0019 }] */
    @Override // X.AbstractC12900ik
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object get(int r11) {
        /*
        // Method dump skipped, instructions count: 261
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC105924uq.get(int):java.lang.Object");
    }
}
