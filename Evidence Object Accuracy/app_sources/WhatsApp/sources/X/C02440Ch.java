package X;

import java.lang.ref.WeakReference;

/* renamed from: X.0Ch  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C02440Ch extends AnonymousClass0PV {
    public final WeakReference A00;

    public C02440Ch(AnonymousClass0EP r2) {
        this.A00 = new WeakReference(r2);
    }

    @Override // X.AnonymousClass0PV
    public void A00() {
        WeakReference weakReference = this.A00;
        if (weakReference.get() != null && ((AnonymousClass0EP) weakReference.get()).A0I) {
            AnonymousClass0EP r2 = (AnonymousClass0EP) weakReference.get();
            AnonymousClass016 r1 = r2.A0D;
            if (r1 == null) {
                r1 = new AnonymousClass016();
                r2.A0D = r1;
            }
            AnonymousClass0EP.A00(r1, true);
        }
    }

    @Override // X.AnonymousClass0PV
    public void A01(int i, CharSequence charSequence) {
        WeakReference weakReference = this.A00;
        if (weakReference.get() != null && !((AnonymousClass0EP) weakReference.get()).A0J && ((AnonymousClass0EP) weakReference.get()).A0I) {
            AnonymousClass0EP r2 = (AnonymousClass0EP) weakReference.get();
            AnonymousClass0P5 r1 = new AnonymousClass0P5(i, charSequence);
            AnonymousClass016 r0 = r2.A08;
            if (r0 == null) {
                r0 = new AnonymousClass016();
                r2.A08 = r0;
            }
            AnonymousClass0EP.A00(r0, r1);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0032, code lost:
        if ((r1 & 32768) != 0) goto L_0x0034;
     */
    @Override // X.AnonymousClass0PV
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02(X.C04700Ms r5) {
        /*
            r4 = this;
            java.lang.ref.WeakReference r3 = r4.A00
            java.lang.Object r0 = r3.get()
            if (r0 == 0) goto L_0x004e
            java.lang.Object r0 = r3.get()
            X.0EP r0 = (X.AnonymousClass0EP) r0
            boolean r0 = r0.A0I
            if (r0 == 0) goto L_0x004e
            int r1 = r5.A00
            r0 = -1
            if (r1 != r0) goto L_0x003a
            X.0U4 r2 = r5.A01
            java.lang.Object r0 = r3.get()
            X.0EP r0 = (X.AnonymousClass0EP) r0
            X.0Nw r1 = r0.A06
            if (r1 == 0) goto L_0x0034
            X.0U4 r0 = r0.A05
            int r1 = X.AnonymousClass0QX.A00(r0, r1)
            r0 = r1 & 32767(0x7fff, float:4.5916E-41)
            if (r0 == 0) goto L_0x0034
            r0 = 32768(0x8000, float:4.5918E-41)
            r1 = r1 & r0
            r0 = 2
            if (r1 == 0) goto L_0x0035
        L_0x0034:
            r0 = -1
        L_0x0035:
            X.0Ms r5 = new X.0Ms
            r5.<init>(r2, r0)
        L_0x003a:
            java.lang.Object r1 = r3.get()
            X.0EP r1 = (X.AnonymousClass0EP) r1
            X.016 r0 = r1.A0A
            if (r0 != 0) goto L_0x004b
            X.016 r0 = new X.016
            r0.<init>()
            r1.A0A = r0
        L_0x004b:
            X.AnonymousClass0EP.A00(r0, r5)
        L_0x004e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C02440Ch.A02(X.0Ms):void");
    }
}
