package X;

import android.os.Build;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextDirectionHeuristic;
import android.text.TextDirectionHeuristics;
import android.text.TextPaint;
import android.text.TextUtils;

/* renamed from: X.3JB  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3JB {
    public static final C006202y A07 = new C006202y(100);
    public int A00 = Integer.MAX_VALUE;
    public int A01 = 2;
    public int A02 = 0;
    public int A03 = 2;
    public Layout A04 = null;
    public boolean A05 = true;
    public final C64273Ex A06 = new C64273Ex();

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:11:0x0001 */
    /* JADX DEBUG: Multi-variable search result rejected for r14v0, resolved type: android.text.Layout$Alignment */
    /* JADX DEBUG: Multi-variable search result rejected for r15v0, resolved type: android.text.TextPaint */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r15v1 */
    /* JADX WARN: Type inference failed for: r14v1 */
    /* JADX WARN: Type inference failed for: r14v2, types: [int] */
    /* JADX WARN: Type inference failed for: r15v2, types: [int] */
    public static StaticLayout A00(Layout.Alignment alignment, TextPaint textPaint, TextUtils.TruncateAt truncateAt, AnonymousClass02T r17, CharSequence charSequence, float f, float f2, int i, int i2, int i3, int i4, boolean z) {
        try {
            alignment = i3;
            textPaint = i4;
            return new StaticLayout(charSequence, 0, i, textPaint, i2, alignment, A01(r17), f, f2, z, truncateAt, alignment, textPaint);
        } catch (IllegalArgumentException e) {
            if (e.getMessage().contains("utext_close")) {
                return new StaticLayout(charSequence, 0, i, textPaint, i2, alignment, A01(r17), f, f2, z, truncateAt, alignment == true ? 1 : 0, textPaint == true ? 1 : 0);
            }
            throw e;
        }
    }

    public static TextDirectionHeuristic A01(AnonymousClass02T r1) {
        if (r1 == AnonymousClass02U.A04) {
            return TextDirectionHeuristics.LTR;
        }
        if (r1 == AnonymousClass02U.A05) {
            return TextDirectionHeuristics.RTL;
        }
        if (r1 != AnonymousClass02U.A01) {
            if (r1 == AnonymousClass02U.A02) {
                return TextDirectionHeuristics.FIRSTSTRONG_RTL;
            }
            if (r1 == AnonymousClass02U.A00) {
                return TextDirectionHeuristics.ANYRTL_LTR;
            }
            if (r1 == AnonymousClass02U.A03) {
                return TextDirectionHeuristics.LOCALE;
            }
        }
        return TextDirectionHeuristics.FIRSTSTRONG_LTR;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:132:0x0202, code lost:
        continue;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:0x0202, code lost:
        continue;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01ce, code lost:
        r3 = android.text.StaticLayout.class.getDeclaredField("mLines");
        r3.setAccessible(true);
        r1 = android.text.StaticLayout.class.getDeclaredField("mColumns");
        r1.setAccessible(true);
        r13 = (int[]) r3.get(r2);
        r12 = r1.getInt(r2);
        r11 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x01ed, code lost:
        if (r11 >= r12) goto L_0x01ff;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x01ef, code lost:
        r10 = (r12 * r14) + r11;
        r9 = r10 + r12;
        r3 = r13[r10];
        r13[r10] = r13[r9];
        r13[r9] = r3;
        r11 = r11 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x01ff, code lost:
        r1 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x0201, code lost:
        r1 = true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.text.Layout A02() {
        /*
        // Method dump skipped, instructions count: 741
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3JB.A02():android.text.Layout");
    }

    public void A03() {
        C64273Ex r1 = this.A06;
        if (r1.A0F.getLetterSpacing() != 0.0f) {
            r1.A00();
            r1.A0F.setLetterSpacing(0.0f);
            this.A04 = null;
        }
    }

    public void A04(int i) {
        C64273Ex r1 = this.A06;
        if (r1.A07 != i) {
            r1.A07 = i;
            if (Build.VERSION.SDK_INT >= 23) {
                this.A04 = null;
            }
        }
    }

    public void A05(int i) {
        C64273Ex r1 = this.A06;
        if (r1.A08 != i) {
            r1.A08 = i;
            if (Build.VERSION.SDK_INT >= 26) {
                this.A04 = null;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001d, code lost:
        if (r4 != null) goto L_0x001f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A06(java.lang.CharSequence r4) {
        /*
            r3 = this;
            X.3Ex r2 = r3.A06
            java.lang.CharSequence r0 = r2.A0I
            if (r4 == r0) goto L_0x002d
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 21
            if (r1 < r0) goto L_0x001d
            boolean r0 = r4 instanceof android.text.SpannableStringBuilder
            if (r0 == 0) goto L_0x001d
            r4.hashCode()     // Catch: NullPointerException -> 0x0014
            goto L_0x001f
        L_0x0014:
            r2 = move-exception
            java.lang.String r1 = "The given text contains a null span. Due to an Android framework bug, this will cause an exception later down the line."
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>(r1, r2)
            throw r0
        L_0x001d:
            if (r4 == 0) goto L_0x0028
        L_0x001f:
            java.lang.CharSequence r0 = r2.A0I
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0028
            return
        L_0x0028:
            r2.A0I = r4
            r0 = 0
            r3.A04 = r0
        L_0x002d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3JB.A06(java.lang.CharSequence):void");
    }
}
