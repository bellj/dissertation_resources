package X;

import java.util.Comparator;

/* renamed from: X.6KE  reason: invalid class name */
/* loaded from: classes4.dex */
public final /* synthetic */ class AnonymousClass6KE implements Comparator {
    @Override // java.util.Comparator
    public final int compare(Object obj, Object obj2) {
        long j = ((AnonymousClass1IR) obj).A05;
        long j2 = ((AnonymousClass1IR) obj2).A05;
        if (j == j2) {
            return 0;
        }
        return j < j2 ? 1 : -1;
    }
}
