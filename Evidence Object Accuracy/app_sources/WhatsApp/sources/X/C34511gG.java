package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.1gG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C34511gG extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C34511gG A03;
    public static volatile AnonymousClass255 A04;
    public int A00;
    public C34351g0 A01;
    public boolean A02;

    static {
        C34511gG r0 = new C34511gG();
        A03 = r0;
        r0.A0W();
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A00(1);
        }
        if ((i3 & 2) == 2) {
            C34351g0 r0 = this.A01;
            if (r0 == null) {
                r0 = C34351g0.A04;
            }
            i2 += CodedOutputStream.A0A(r0, 2);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0J(1, this.A02);
        }
        if ((this.A00 & 2) == 2) {
            C34351g0 r0 = this.A01;
            if (r0 == null) {
                r0 = C34351g0.A04;
            }
            codedOutputStream.A0L(r0, 2);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
