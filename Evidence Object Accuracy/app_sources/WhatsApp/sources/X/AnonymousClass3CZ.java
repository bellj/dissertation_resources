package X;

import java.util.Map;

/* renamed from: X.3CZ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3CZ {
    public final Map A00 = C12970iu.A11();
    public final Map A01 = C12970iu.A11();

    public void A00(Object obj, Object obj2) {
        Map map = this.A01;
        if (map.containsKey(obj2) && !C29941Vi.A00(map.get(obj2), obj)) {
            map.put(obj2, obj);
            Object obj3 = this.A00.get(obj2);
            AnonymousClass009.A05(obj3);
            ((AnonymousClass02B) obj3).ANq(obj);
        }
    }
}
