package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4ik  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98474ik implements Parcelable.ClassLoaderCreator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new C74423hx(parcel, (ClassLoader) null);
    }

    @Override // android.os.Parcelable.ClassLoaderCreator
    public Object createFromParcel(Parcel parcel, ClassLoader classLoader) {
        return new C74423hx(parcel, classLoader);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C74423hx[i];
    }
}
