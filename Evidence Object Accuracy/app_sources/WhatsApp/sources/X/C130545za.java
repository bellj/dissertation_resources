package X;

import java.util.ArrayList;

/* renamed from: X.5za  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C130545za {
    public static final ArrayList A01;
    public static final ArrayList A02;
    public final AnonymousClass1V8 A00;

    static {
        String[] strArr = new String[3];
        strArr[0] = "initial";
        strArr[1] = "reset";
        A01 = C12960it.A0m("rotate", strArr, 2);
        String[] strArr2 = new String[2];
        strArr2[0] = "1";
        A02 = C12960it.A0m("2", strArr2, 1);
    }

    public C130545za(AnonymousClass3CT r11, String str, String str2, String str3, String str4) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy A0N = C117295Zj.A0N(A0M);
        C41141sy.A01(A0N, "action", "upi-get-token");
        if (AnonymousClass3JT.A0E(str, 0, 1000, false)) {
            C41141sy.A01(A0N, "challenge", str);
        }
        if (AnonymousClass3JT.A0E(str2, 1, 1000, false)) {
            C41141sy.A01(A0N, "device-id", str2);
        }
        if (str3 != null && AnonymousClass3JT.A0E(str3, 1, 10, true)) {
            C41141sy.A01(A0N, "provider-type", str3);
        }
        A0N.A0A(str4, "token-type", A01);
        this.A00 = C117295Zj.A0J(A0N, A0M, r11);
    }
}
