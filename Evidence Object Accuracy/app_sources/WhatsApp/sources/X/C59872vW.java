package X;

import android.view.View;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.WaTextView;

/* renamed from: X.2vW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59872vW extends AbstractC37191le {
    public final ImageView A00;
    public final WaTextView A01;
    public final AnonymousClass1CN A02;

    public C59872vW(View view, AnonymousClass1CN r3) {
        super(view);
        this.A02 = r3;
        this.A00 = C12970iu.A0K(view, R.id.category_icon);
        this.A01 = C12960it.A0N(view, R.id.category_name);
    }
}
