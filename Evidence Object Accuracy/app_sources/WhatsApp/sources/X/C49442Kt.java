package X;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/* renamed from: X.2Kt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C49442Kt extends AbstractC49422Kr {
    public static final Set A04 = new HashSet(Arrays.asList("offer", "accept", "reject", "enc_rekey"));
    public final C15570nT A00;
    public final C14830m7 A01;
    public final C14840m8 A02;
    public final C230810h A03;

    public C49442Kt(AbstractC15710nm r7, C15570nT r8, C14830m7 r9, C14850m9 r10, C16120oU r11, C14840m8 r12, C450720b r13, C230810h r14, Map map) {
        super(r7, r10, r11, r13, map);
        this.A01 = r9;
        this.A00 = r8;
        this.A02 = r12;
        this.A03 = r14;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r14v1, types: [com.whatsapp.jid.Jid, X.0lm] */
    /* JADX WARN: Type inference failed for: r0v149, types: [X.0lm] */
    /* JADX WARNING: Code restructure failed: missing block: B:171:0x0552, code lost:
        if ("sender".equals(r13) != false) goto L_0x0554;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x020b, code lost:
        if (r44.A00.A0F(r6.A01) == false) goto L_0x020d;
     */
    /* JADX WARNING: Removed duplicated region for block: B:146:0x04fa  */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x0502  */
    /* JADX WARNING: Removed duplicated region for block: B:151:0x050d  */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x0518  */
    /* JADX WARNING: Removed duplicated region for block: B:155:0x051c  */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x0527  */
    /* JADX WARNING: Removed duplicated region for block: B:161:0x0532  */
    /* JADX WARNING: Removed duplicated region for block: B:164:0x053d  */
    /* JADX WARNING: Removed duplicated region for block: B:167:0x0545  */
    /* JADX WARNING: Removed duplicated region for block: B:170:0x054b  */
    /* JADX WARNING: Removed duplicated region for block: B:175:0x055d A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:182:0x057b A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:187:0x058f  */
    /* JADX WARNING: Removed duplicated region for block: B:193:0x05aa  */
    /* JADX WARNING: Removed duplicated region for block: B:198:0x05d9  */
    /* JADX WARNING: Removed duplicated region for block: B:211:0x0699  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0137  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0198  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x019f  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x01c0  */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // X.AbstractC49422Kr
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(X.AnonymousClass1V8 r45) {
        /*
        // Method dump skipped, instructions count: 2030
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C49442Kt.A01(X.1V8):void");
    }
}
