package X;

import android.content.Context;
import android.icu.text.DateTimePatternGenerator;
import android.os.Build;
import android.text.format.Time;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

/* renamed from: X.1nY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C38121nY {
    public static final AnonymousClass4WV A00 = new AnonymousClass47p();
    public static final AnonymousClass4WV A01 = new C864847m();
    public static final AnonymousClass4WV A02 = new AnonymousClass47q();
    public static final AnonymousClass4WV A03 = new C865047o();
    public static final AnonymousClass4WV A04 = new C864947n();

    public static int A00(long j, long j2) {
        Time time = new Time();
        time.set(j);
        time.set(j2);
        return ((int) ((j + (time.gmtoff * 1000)) / 86400000)) - ((int) ((j2 + (time.gmtoff * 1000)) / 86400000));
    }

    public static long A01(long j) {
        long j2;
        long timeInMillis;
        long currentTimeMillis = System.currentTimeMillis() - j;
        long j3 = 3600000;
        if (currentTimeMillis < 3600000) {
            j3 = 60000;
        } else if (currentTimeMillis >= 86400000) {
            j2 = 0;
            GregorianCalendar gregorianCalendar = new GregorianCalendar();
            gregorianCalendar.add(5, 1);
            gregorianCalendar.set(11, 0);
            gregorianCalendar.set(12, 0);
            gregorianCalendar.set(13, 0);
            timeInMillis = gregorianCalendar.getTimeInMillis();
            if (j2 != 0 || j2 > timeInMillis) {
                return timeInMillis;
            }
            return j2;
        }
        j2 = j + ((currentTimeMillis / j3) * j3) + j3;
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.add(5, 1);
        gregorianCalendar.set(11, 0);
        gregorianCalendar.set(12, 0);
        gregorianCalendar.set(13, 0);
        timeInMillis = gregorianCalendar.getTimeInMillis();
        if (j2 != 0) {
        }
        return timeInMillis;
    }

    public static String A02(long j) {
        return ((DateFormat) A01.A01()).format(new Date(j));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0059, code lost:
        if (r6.equals(r0) == false) goto L_0x001d;
     */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0021 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00ca  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00d2 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00d3 A[PHI: r6 
      PHI: (r6v1 java.lang.String) = (r6v0 java.lang.String), (r6v0 java.lang.String), (r6v2 java.lang.String), (r6v4 java.lang.String), (r6v12 java.lang.String) binds: [B:31:0x0059, B:13:0x001e, B:56:0x00d0, B:35:0x006c, B:43:0x0090] A[DONT_GENERATE, DONT_INLINE]] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:62:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String A03(android.content.Context r5, java.lang.String r6, java.util.Date r7, java.util.Locale r8, boolean r9) {
        /*
        // Method dump skipped, instructions count: 296
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C38121nY.A03(android.content.Context, java.lang.String, java.util.Date, java.util.Locale, boolean):java.lang.String");
    }

    public static String A04(Context context, Date date, Locale locale) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 24) {
            return A09(date, locale);
        }
        if (i >= 18) {
            return A08(date, locale);
        }
        DateFormat dateTimeInstance = DateFormat.getDateTimeInstance(1, 3, locale);
        TimeZone timeZone = dateTimeInstance.getTimeZone();
        return MessageFormat.format(AnonymousClass1MZ.A01(context, locale, 0), dateTimeInstance.format(date), timeZone.getDisplayName(timeZone.inDaylightTime(date), 0, locale));
    }

    public static String A05(AnonymousClass018 r2, String str, String str2) {
        return MessageFormat.format(r2.A08(179), str2, str);
    }

    public static String A06(String str, Date date, Locale locale, boolean z) {
        if (z) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append("jjmm");
            str = sb.toString();
        }
        return new SimpleDateFormat(android.text.format.DateFormat.getBestDateTimePattern(locale, str), locale).format(date);
    }

    public static String A07(String str, Date date, Locale locale, boolean z) {
        DateTimePatternGenerator instance = DateTimePatternGenerator.getInstance(locale);
        if (z) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append("jjmm");
            str = sb.toString();
        }
        return new android.icu.text.SimpleDateFormat(instance.getBestPattern(str), locale).format(date);
    }

    public static String A08(Date date, Locale locale) {
        return new SimpleDateFormat(android.text.format.DateFormat.getBestDateTimePattern(locale, "yyyyMMMMdEEEEjjmmz"), locale).format(date);
    }

    public static String A09(Date date, Locale locale) {
        return new android.icu.text.SimpleDateFormat(DateTimePatternGenerator.getInstance(locale).getBestPattern("yyyyMMMMdEEEEjjmmz"), locale).format(date);
    }

    public static boolean A0A(long j, long j2) {
        Calendar calendar = (Calendar) A04.A01();
        calendar.setTimeInMillis(j);
        Calendar calendar2 = (Calendar) A03.A01();
        calendar2.setTimeInMillis(j2);
        if (calendar.get(1) == calendar2.get(1) && calendar.get(2) == calendar2.get(2) && calendar.get(5) == calendar2.get(5)) {
            return true;
        }
        return false;
    }

    public static boolean A0B(long j, long j2) {
        Calendar calendar = (Calendar) A04.A01();
        calendar.setTimeInMillis(j);
        Calendar calendar2 = (Calendar) A03.A01();
        calendar2.setTimeInMillis(j2);
        if (calendar.get(1) != calendar2.get(1)) {
            return false;
        }
        return true;
    }
}
