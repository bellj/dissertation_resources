package X;

import android.widget.EditText;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.367  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass367 extends C469928m {
    public int A00;
    public Runnable A01;
    public final int A02;
    public final int A03;
    public final EditText A04;
    public final TextView A05;
    public final AnonymousClass01d A06;
    public final AnonymousClass018 A07;
    public final AnonymousClass19M A08;
    public final C16630pM A09;
    public final boolean A0A;

    public AnonymousClass367(EditText editText, TextView textView, AnonymousClass01d r8, AnonymousClass018 r9, AnonymousClass19M r10, C16630pM r11, int i, int i2, boolean z) {
        this.A08 = r10;
        this.A06 = r8;
        this.A07 = r9;
        this.A09 = r11;
        this.A04 = editText;
        this.A05 = textView;
        this.A02 = i;
        this.A03 = i2;
        this.A0A = z;
        AnonymousClass028.A0g(editText, new C53512eZ(this));
        if (textView != null) {
            long j = (long) i;
            Object[] A1b = C12970iu.A1b();
            C12960it.A1P(A1b, i, 0);
            textView.setContentDescription(r9.A0I(A1b, R.plurals.text_limit_characters_remaining_description, j));
            if (i != 0 && i2 == 0) {
                textView.setText(r9.A0J().format(j));
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0039, code lost:
        if (r10 < r1) goto L_0x003b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003b, code lost:
        r2 = new com.facebook.redex.RunnableBRunnable0Shape1S0101000_I1(r12, r10, 13);
        r12.A01 = r2;
        r7.postDelayed(r2, 1000);
        r7.setVisibility(0);
        r5 = r12.A07;
        r2 = (long) r10;
        r7.setText(r5.A0J().format(r2));
        r0 = X.C12970iu.A1b();
        X.C12960it.A1P(r0, r10, 0);
        r7.setContentDescription(r5.A0I(r0, com.whatsapp.R.plurals.text_limit_characters_remaining_description, r2));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0097, code lost:
        if (r10 != r8) goto L_0x003b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x009a, code lost:
        r7.setVisibility(r0);
     */
    @Override // X.C469928m, android.text.TextWatcher
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void afterTextChanged(android.text.Editable r13) {
        /*
            r12 = this;
            boolean r0 = r12.A0A
            android.widget.EditText r4 = r12.A04
            r7 = r13
            if (r0 == 0) goto L_0x009e
            android.content.Context r5 = r4.getContext()
            X.19M r9 = r12.A08
            X.01d r8 = r12.A06
            X.0pM r10 = r12.A09
            android.text.TextPaint r6 = r4.getPaint()
            X.C42971wC.A06(r5, r6, r7, r8, r9, r10)
        L_0x0018:
            int r8 = r12.A02
            if (r8 <= 0) goto L_0x008a
            java.lang.Runnable r1 = r12.A01
            if (r1 == 0) goto L_0x0025
            android.widget.TextView r0 = r12.A05
            r0.removeCallbacks(r1)
        L_0x0025:
            java.lang.String r6 = r13.toString()
            int r9 = X.AnonymousClass2VC.A00(r6)
            android.widget.TextView r7 = r12.A05
            if (r7 == 0) goto L_0x006c
            int r10 = r8 - r9
            int r1 = r12.A03
            if (r1 <= 0) goto L_0x0096
            r0 = 8
            if (r10 >= r1) goto L_0x009a
        L_0x003b:
            r0 = 13
            com.facebook.redex.RunnableBRunnable0Shape1S0101000_I1 r2 = new com.facebook.redex.RunnableBRunnable0Shape1S0101000_I1
            r2.<init>(r12, r10, r0)
            r12.A01 = r2
            r0 = 1000(0x3e8, double:4.94E-321)
            r7.postDelayed(r2, r0)
            r11 = 0
            r7.setVisibility(r11)
            X.018 r5 = r12.A07
            java.text.NumberFormat r0 = r5.A0J()
            long r2 = (long) r10
            java.lang.String r0 = r0.format(r2)
            r7.setText(r0)
            r1 = 2131755323(0x7f10013b, float:1.9141522E38)
            java.lang.Object[] r0 = X.C12970iu.A1b()
            X.C12960it.A1P(r0, r10, r11)
            java.lang.String r0 = r5.A0I(r0, r1, r2)
            r7.setContentDescription(r0)
        L_0x006c:
            if (r9 < r8) goto L_0x008b
            int r0 = r12.A00
            if (r0 != 0) goto L_0x008b
            int r1 = r4.getInputType()
            r12.A00 = r1
            if (r1 == 0) goto L_0x008a
            r0 = 524288(0x80000, float:7.34684E-40)
            r1 = r1 | r0
            r4.setInputType(r1)
            r4.setText(r6)
            int r0 = r6.length()
            r4.setSelection(r0)
        L_0x008a:
            return
        L_0x008b:
            int r0 = r12.A00
            if (r0 == 0) goto L_0x008a
            r4.setInputType(r0)
            r0 = 0
            r12.A00 = r0
            return
        L_0x0096:
            r0 = 4
            if (r10 == r8) goto L_0x009a
            goto L_0x003b
        L_0x009a:
            r7.setVisibility(r0)
            goto L_0x006c
        L_0x009e:
            android.content.Context r3 = r4.getContext()
            android.text.TextPaint r2 = r4.getPaint()
            X.19M r1 = r12.A08
            r0 = 1067869798(0x3fa66666, float:1.3)
            X.AbstractC36671kL.A07(r3, r2, r13, r1, r0)
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass367.afterTextChanged(android.text.Editable):void");
    }
}
