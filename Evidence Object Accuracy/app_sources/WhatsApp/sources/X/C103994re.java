package X;

/* renamed from: X.4re  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final /* synthetic */ class C103994re implements AnonymousClass024 {
    public final /* synthetic */ int A00;
    public final /* synthetic */ boolean A01 = true;

    public /* synthetic */ C103994re(int i) {
        this.A00 = i;
    }

    @Override // X.AnonymousClass024
    public final void accept(Object obj) {
        int i = this.A00;
        boolean z = this.A01;
        C40591rq r4 = (C40591rq) obj;
        if (i == 0) {
            r4.A08 = z;
        } else if (i == 1) {
            r4.A07 = z;
        } else if (i == 2) {
            r4.A09 = z;
        } else {
            throw C12970iu.A0f(C12960it.A0W(i, "CommerceRowCount/setFlag/Unhandled flag: "));
        }
    }
}
