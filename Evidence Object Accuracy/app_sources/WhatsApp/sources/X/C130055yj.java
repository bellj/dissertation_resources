package X;

import android.os.Handler;
import android.util.Log;
import android.util.Pair;
import java.util.UUID;

/* renamed from: X.5yj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130055yj {
    public C125385rA A00;
    public String A01;
    public final C1308560f A02;
    public volatile UUID A03;
    public volatile boolean A04;
    public volatile boolean A05;

    public C130055yj(C1308560f r1) {
        this.A02 = r1;
    }

    public UUID A00(Handler handler, String str) {
        UUID uuid;
        C1308560f r1 = this.A02;
        synchronized (r1) {
            if (this.A04) {
                A01(this.A01, str);
            }
            if (A03()) {
                A01(this.A01, str);
            }
            r1.A00 = handler;
            this.A01 = str;
            this.A04 = true;
            uuid = this.A03;
        }
        return uuid;
    }

    public final void A01(String str, String str2) {
        StringBuilder A0j = C12960it.A0j(str);
        A0j.append(" has been evicted. ");
        A0j.append(str2);
        Log.e("SessionManager", C12960it.A0d(" now owns the camera device", A0j));
        new Pair(str, str2);
        AnonymousClass616.A00();
        C125385rA r0 = this.A00;
        if (r0 != null) {
            C1308560f r2 = this.A02;
            RunnableC135746Jq r1 = new RunnableC135746Jq(r0, this, str, str2);
            synchronized (r2) {
                Handler handler = r2.A00;
                if (handler != null) {
                    handler.post(r1);
                } else {
                    AnonymousClass61K.A00(r1);
                }
            }
            this.A00 = null;
        }
    }

    public void A02(UUID uuid) {
        C1308560f r2 = this.A02;
        synchronized (r2) {
            if (uuid != null) {
                if (uuid.equals(this.A03)) {
                    A03();
                    r2.A00 = null;
                    this.A00 = null;
                    this.A04 = false;
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x004f A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A03() {
        /*
            r7 = this;
            r6 = 0
            java.util.UUID r0 = r7.A03
            if (r0 == 0) goto L_0x0040
            X.60f r5 = r7.A02
            java.util.UUID r4 = r7.A03
            monitor-enter(r5)
            java.util.UUID r0 = r5.A01     // Catch: all -> 0x003c
            boolean r0 = r4.equals(r0)     // Catch: all -> 0x003c
            if (r0 != 0) goto L_0x0013
            goto L_0x003f
        L_0x0013:
            android.os.Handler r0 = r5.A03     // Catch: all -> 0x003c
            boolean r3 = r0.hasMessages(r6, r4)     // Catch: all -> 0x003c
            r0.removeCallbacksAndMessages(r4)     // Catch: all -> 0x003c
            android.os.Handler r0 = r5.A00     // Catch: all -> 0x003c
            if (r0 == 0) goto L_0x002b
            boolean r0 = r0.hasMessages(r6, r4)     // Catch: all -> 0x003c
            r3 = r3 | r0
            android.os.Handler r0 = r5.A00     // Catch: all -> 0x003c
            r0.removeCallbacksAndMessages(r4)     // Catch: all -> 0x003c
            goto L_0x0043
        L_0x002b:
            java.lang.Class<X.61K> r2 = X.AnonymousClass61K.class
            monitor-enter(r2)     // Catch: all -> 0x003c
            android.os.Handler r1 = X.AnonymousClass61K.A00     // Catch: all -> 0x0039
            boolean r0 = r1.hasMessages(r6, r4)     // Catch: all -> 0x0039
            r1.removeCallbacksAndMessages(r4)     // Catch: all -> 0x0039
            monitor-exit(r2)     // Catch: all -> 0x003c
            goto L_0x0042
        L_0x0039:
            r0 = move-exception
            monitor-exit(r2)     // Catch: all -> 0x003c
            throw r0     // Catch: all -> 0x003c
        L_0x003c:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        L_0x003f:
            monitor-exit(r5)
        L_0x0040:
            r3 = 0
            goto L_0x0044
        L_0x0042:
            r3 = r3 | r0
        L_0x0043:
            monitor-exit(r5)
        L_0x0044:
            java.util.UUID r0 = java.util.UUID.randomUUID()
            r7.A03 = r0
            X.60f r1 = r7.A02
            java.util.UUID r0 = r7.A03
            monitor-enter(r1)
            r1.A01 = r0     // Catch: all -> 0x0053
            monitor-exit(r1)
            return r3
        L_0x0053:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C130055yj.A03():boolean");
    }
}
