package X;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Picture;
import android.graphics.RectF;
import android.text.TextPaint;
import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.Locale;
import org.json.JSONObject;

/* renamed from: X.33N  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass33N extends AnonymousClass33A {
    public double A00;
    public double A01;
    public float A02;
    public Picture A03;
    public Picture A04;
    public String A05;
    public String A06;
    public boolean A07;
    public C91474Rw[] A08;
    public C91474Rw[] A09;
    public final Paint A0A;
    public final Paint A0B;
    public final Paint A0C;
    public final Paint A0D;
    public final Paint A0E;
    public final Paint A0F;
    public final Paint A0G;
    public final TextPaint A0H;
    public final AnonymousClass01J A0I;
    public final AbstractC92674Wx A0J;
    public final AnonymousClass3C8 A0K;
    public final AnonymousClass3EU A0L;
    public final boolean A0M;
    public final boolean A0N;

    @Override // X.AbstractC454821u
    public void A09(int i) {
    }

    @Override // X.AbstractC454821u
    public String A0G() {
        return "location";
    }

    @Override // X.AbstractC454821u
    public boolean A0J() {
        return false;
    }

    public AnonymousClass33N(Context context, AnonymousClass018 r8, String str, boolean z) {
        super(context);
        String str2;
        this.A0C = C12990iw.A0G(1);
        this.A0A = C12990iw.A0G(1);
        this.A0D = C12990iw.A0G(1);
        this.A0G = C12990iw.A0G(1);
        this.A0B = C12990iw.A0G(1);
        this.A0F = C12990iw.A0G(1);
        this.A0E = C12990iw.A0G(1);
        TextPaint textPaint = new TextPaint(1);
        this.A0H = textPaint;
        int i = 0;
        this.A07 = false;
        this.A0J = new AnonymousClass45M(this);
        this.A0M = z;
        AnonymousClass01J A0S = C12990iw.A0S(context);
        this.A0I = A0S;
        boolean A07 = A0S.A3L().A07(1548);
        this.A0N = A07;
        if (C28391Mz.A02()) {
            str2 = "ic_content_sticker_location_60_percent_black.svg";
        } else {
            str2 = "ic_content_sticker_location_black.svg";
        }
        str2 = A07 ? "ic_content_sticker_location_emerald.svg" : str2;
        Context context2 = ((AnonymousClass33A) this).A00;
        Picture A02 = AnonymousClass33A.A02(context2, str2);
        AnonymousClass009.A05(A02);
        this.A03 = A02;
        Picture A022 = AnonymousClass33A.A02(context2, "ic_content_sticker_location.svg");
        AnonymousClass009.A05(A022);
        this.A04 = A022;
        AnonymousClass009.A0F(C12960it.A1V(this.A03.getWidth(), this.A04.getWidth()));
        TextPaint textPaint2 = this.A0H;
        textPaint2.setTextSize(46.0f);
        textPaint2.setTextAlign(Paint.Align.CENTER);
        textPaint2.setTypeface(C27531Hw.A03(context2));
        this.A06 = str;
        Picture picture = this.A04;
        float width = 1000.0f - ((float) (picture != null ? picture.getWidth() : i));
        boolean z2 = this.A0N;
        this.A05 = TextUtils.ellipsize(!A07 ? str.toUpperCase(Locale.getDefault()) : str, textPaint, (width - (z2 ? 75.0f : 106.0f)) - (z2 ? 26.0f : 14.0f), TextUtils.TruncateAt.END).toString();
        A0S();
        this.A0L = new AnonymousClass3EU(context, r8);
        this.A0K = new AnonymousClass3C8();
    }

    public AnonymousClass33N(Context context, AnonymousClass018 r7, JSONObject jSONObject) {
        this(context, r7, context.getString(R.string.attach_location), false);
        super.A0A(jSONObject);
        this.A00 = jSONObject.getDouble("latitude");
        this.A01 = jSONObject.getDouble("longitude");
        this.A06 = jSONObject.getString("Location");
        this.A05 = jSONObject.getString("displayLocation");
        this.A07 = jSONObject.getBoolean("theme");
        A0S();
        RectF rectF = super.A02;
        float width = rectF.width();
        float height = rectF.height();
        float f = rectF.left;
        float f2 = rectF.top;
        rectF.set(f, f2, width + f, height + f2);
        rectF.sort();
    }

    @Override // X.AbstractC454821u
    public void A04() {
        RectF rectF = super.A02;
        if (rectF.height() < AbstractC454821u.A03) {
            float width = rectF.width() / rectF.height();
            rectF.set(rectF.centerX() - ((AbstractC454821u.A03 * width) / 2.0f), rectF.centerY() - (AbstractC454821u.A03 / 2.0f), rectF.centerX() + ((AbstractC454821u.A03 * width) / 2.0f), rectF.centerY() + (AbstractC454821u.A03 / 2.0f));
        }
    }

    @Override // X.AbstractC454821u
    public void A05() {
        this.A0L.A00 = false;
    }

    @Override // X.AbstractC454821u
    public void A06(float f) {
        AbstractC454821u.A00(this, f);
    }

    @Override // X.AbstractC454821u
    public void A08(float f, int i) {
        A07(f, 2);
        this.A0L.A00(f);
    }

    @Override // X.AbstractC454821u
    public boolean A0C() {
        return this.A0J.A01;
    }

    @Override // X.AbstractC454821u
    public boolean A0E() {
        this.A0K.A00(this.A0J);
        return true;
    }

    @Override // X.AbstractC454821u
    public String A0H(Context context) {
        return context.getString(R.string.doodle_item_location);
    }

    @Override // X.AbstractC454821u
    public void A0I(Canvas canvas) {
        A0P(canvas);
    }

    @Override // X.AbstractC454821u
    public void A0N(JSONObject jSONObject) {
        super.A0N(jSONObject);
        jSONObject.put("latitude", this.A00);
        jSONObject.put("longitude", this.A01);
        jSONObject.put("Location", this.A06);
        jSONObject.put("displayLocation", this.A05);
        jSONObject.put("theme", this.A07);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0115, code lost:
        if (r10 != false) goto L_0x009a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00a6  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00c0 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0101  */
    /* JADX WARNING: Removed duplicated region for block: B:54:? A[RETURN, SYNTHETIC] */
    @Override // X.AbstractC454821u
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0P(android.graphics.Canvas r17) {
        /*
        // Method dump skipped, instructions count: 310
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass33N.A0P(android.graphics.Canvas):void");
    }

    @Override // X.AnonymousClass45J, X.AbstractC454821u
    public void A0Q(RectF rectF, float f, float f2, float f3, float f4) {
        float f5;
        boolean z;
        float f6;
        float f7 = f3 - f;
        float f8 = f4 - f2;
        boolean z2 = this.A0M;
        if (z2) {
            f5 = f7;
            float f9 = f7 / 3.0f;
            z = this.A0N;
            float f10 = 130.0f;
            if (z) {
                f10 = 105.0f;
            }
            f6 = Math.min(f9, (f10 * f7) / this.A02);
        } else {
            f5 = this.A02;
            z = this.A0N;
            f6 = 130.0f;
            if (z) {
                f6 = 105.0f;
            }
        }
        float f11 = (f7 / 2.0f) + f;
        if (!z2) {
            f = f11 - (f5 / 2.0f);
        }
        float f12 = f2 + ((f8 / 2.0f) - (f6 / 2.0f));
        RectF rectF2 = super.A02;
        rectF2.set(f, f12, f + f5, f6 + f12);
        float f13 = f7 * 2.0f;
        if (!z2 && f5 > f13) {
            float f14 = 106.0f;
            if (z) {
                f14 = 75.0f;
            }
            A06(f13 / (f5 + f14));
        }
        rectF2.sort();
        this.A0L.A00(rectF.width() / 1020.0f);
    }

    public final void A0S() {
        float f;
        Picture picture;
        float f2 = 0.0f;
        if (this.A03 == null || (picture = this.A04) == null) {
            Log.w("Location/initThemes/Error when loading pin");
            f = 0.0f;
        } else {
            float f3 = 14.0f;
            if (this.A0N) {
                f3 = 26.0f;
            }
            f = f3 + ((float) picture.getWidth());
        }
        boolean z = this.A0N;
        if (!z || !this.A0M) {
            f2 = this.A0H.measureText(this.A05);
        }
        float f4 = 106.0f;
        if (z) {
            f4 = 75.0f;
        }
        this.A02 = Math.max(300.0f, f + f4 + f2);
        float f5 = Resources.getSystem().getDisplayMetrics().density * 8.0f;
        if (z) {
            Paint paint = this.A0G;
            paint.setColor(-1);
            this.A09 = new C91474Rw[]{new C91474Rw(paint, 0.0f, 0.0f, this.A02, 105.0f, f5, f5)};
            Paint paint2 = this.A0B;
            C12980iv.A12(((AnonymousClass33A) this).A00, paint2, R.color.wds_cool_gray_alpha_50);
            this.A08 = new C91474Rw[]{new C91474Rw(paint2, 0.0f, 0.0f, this.A02, 105.0f, f5, f5)};
            return;
        }
        Paint paint3 = this.A0E;
        paint3.setColor(Color.parseColor("#666666"));
        Paint paint4 = this.A0F;
        paint4.setColor(-1);
        C91474Rw[] r4 = new C91474Rw[6];
        this.A09 = r4;
        r4[0] = new C91474Rw(paint4, 0.0f, 20.0f, this.A02, 130.0f - 20.0f, 20.0f, 20.0f);
        this.A09[1] = new C91474Rw(paint4, 20.0f, 0.0f, this.A02 - 20.0f, 130.0f, 20.0f, 20.0f);
        this.A09[2] = new C91474Rw(paint3, 10.0f, 30.0f, this.A02 - 10.0f, 130.0f - 30.0f, 10.0f, 10.0f);
        this.A09[3] = new C91474Rw(paint3, 30.0f, 10.0f, this.A02 - 30.0f, 130.0f - 10.0f, 10.0f, 10.0f);
        this.A09[4] = new C91474Rw(paint4, 15.0f, 35.0f, this.A02 - 15.0f, 130.0f - 35.0f, 6.0f, 6.0f);
        this.A09[5] = new C91474Rw(paint4, 35.0f, 15.0f, this.A02 - 35.0f, 130.0f - 15.0f, 6.0f, 6.0f);
        Paint paint5 = this.A0C;
        C12970iu.A16(-16777216, paint5);
        paint5.setAlpha(90);
        Paint paint6 = this.A0A;
        paint6.setColor(-1);
        Paint.Style style = Paint.Style.STROKE;
        paint6.setStyle(style);
        paint6.setColor(-1);
        paint6.setStyle(style);
        paint6.setStrokeWidth(5.0f);
        C91474Rw[] r1 = new C91474Rw[2];
        this.A08 = r1;
        r1[0] = new C91474Rw(paint5, 0.0f, 0.0f, this.A02, 130.0f, 75.0f, 75.0f);
        this.A08[1] = new C91474Rw(paint6, 0.0f, 0.0f, this.A02, 130.0f, 75.0f, 75.0f);
    }
}
