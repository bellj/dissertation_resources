package X;

/* renamed from: X.1h1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C34981h1 {
    public final int A00;
    public final long A01;
    public final long A02;

    public C34981h1(int i, long j, long j2) {
        this.A02 = j;
        this.A01 = j2;
        this.A00 = i;
    }

    public static C34981h1 A00(int i) {
        return new C34981h1(i, Long.MIN_VALUE, -1);
    }
}
