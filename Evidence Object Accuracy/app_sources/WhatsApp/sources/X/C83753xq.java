package X;

import android.content.Context;
import android.view.inputmethod.BaseInputConnection;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import com.whatsapp.WaEditText;

/* renamed from: X.3xq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83753xq extends WaEditText {
    public C83753xq(Context context) {
        super(context);
    }

    @Override // com.whatsapp.WaEditText, X.AnonymousClass011, android.widget.TextView, android.view.View
    public InputConnection onCreateInputConnection(EditorInfo editorInfo) {
        editorInfo.actionLabel = null;
        editorInfo.inputType = 0;
        BaseInputConnection baseInputConnection = new BaseInputConnection(this, false);
        try {
            InputConnection onCreateInputConnection = super.onCreateInputConnection(editorInfo);
            if (onCreateInputConnection != null) {
                return new C74023hB(baseInputConnection, onCreateInputConnection);
            }
            return null;
        } catch (Exception unused) {
            return super.onCreateInputConnection(editorInfo);
        }
    }
}
