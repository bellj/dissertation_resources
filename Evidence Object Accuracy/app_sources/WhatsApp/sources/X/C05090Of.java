package X;

import java.util.List;

/* renamed from: X.0Of  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05090Of {
    public final char A00;
    public final double A01;
    public final String A02;
    public final String A03;
    public final List A04;

    public C05090Of(String str, String str2, List list, char c, double d) {
        this.A04 = list;
        this.A00 = c;
        this.A01 = d;
        this.A03 = str;
        this.A02 = str2;
    }

    public int hashCode() {
        return ((((0 + this.A00) * 31) + this.A02.hashCode()) * 31) + this.A03.hashCode();
    }
}
