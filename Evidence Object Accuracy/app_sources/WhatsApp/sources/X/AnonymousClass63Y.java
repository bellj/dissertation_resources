package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

/* renamed from: X.63Y  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass63Y implements Parcelable {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(16);
    public final AbstractC30791Yv A00;
    public final AbstractC30791Yv A01;
    public final AbstractC30791Yv A02;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass63Y(AbstractC30791Yv r3, AbstractC30791Yv r4, String str) {
        this.A01 = r3;
        this.A02 = r4;
        if (!TextUtils.isEmpty(str) && ((AbstractC30781Yu) r4).A00 == 1 && ((C30801Yw) r4).A00(r3)) {
            r3 = r4;
        }
        this.A00 = r3;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        AbstractC30791Yv r2 = this.A01;
        parcel.writeString(((AbstractC30781Yu) r2).A04);
        AbstractC30791Yv r1 = this.A02;
        parcel.writeString(((AbstractC30781Yu) r1).A04);
        r2.writeToParcel(parcel, i);
        r1.writeToParcel(parcel, i);
    }
}
