package X;

/* renamed from: X.0ov  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16380ov extends AbstractC15340mz implements AbstractC16390ow, AbstractC16400ox, AbstractC16410oy, AbstractC16420oz {
    public C16470p4 A00;

    public C16380ov(AnonymousClass1IS r1, byte b, long j) {
        super(r1, b, j);
    }

    public C16380ov(AnonymousClass1IS r8, C16380ov r9, long j, boolean z) {
        super(r9, r8, j, z);
        this.A00 = r9.A00;
        A14();
    }

    /* JADX WARNING: Removed duplicated region for block: B:58:0x00fd  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0142  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x014a  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0156  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x016b A[Catch: 1MW -> 0x01b4, TryCatch #0 {1MW -> 0x01b4, blocks: (B:76:0x0162, B:78:0x016b, B:79:0x016f, B:81:0x0175, B:83:0x0186, B:84:0x018a, B:86:0x0190, B:87:0x01a1, B:88:0x01aa), top: B:93:0x0162 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C16380ov(X.C57742nY r25, X.AnonymousClass1IS r26, byte r27, long r28) {
        /*
        // Method dump skipped, instructions count: 487
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C16380ov.<init>(X.2nY, X.1IS, byte, long):void");
    }

    public final void A14() {
        C16460p3 A0G = A0G();
        AnonymousClass009.A05(A0G);
        byte[] A01 = AnonymousClass3GO.A01(this);
        if (A01 != null) {
            A0G.A03(A01, true);
        } else {
            A0G.A02(null);
        }
    }

    @Override // X.AbstractC16420oz
    public void A6k(C39971qq r2) {
        if (ACL() != null) {
            ACL().A0A(this, r2);
        }
    }

    @Override // X.AbstractC16410oy
    public /* bridge */ /* synthetic */ AbstractC15340mz A7L(AnonymousClass1IS r8, long j) {
        AnonymousClass009.A05(ACL());
        boolean z = false;
        if (ACL().A02() != AnonymousClass24E.A02) {
            z = true;
        }
        AnonymousClass009.A0E(z);
        return new C16380ov(r8, this, j, false);
    }

    @Override // X.AbstractC16400ox
    public AbstractC15340mz A7M(AnonymousClass1IS r7) {
        return new C16380ov(r7, this, this.A0I, true);
    }

    @Override // X.AbstractC16390ow
    public C16470p4 ABf() {
        return this.A00;
    }

    @Override // X.AbstractC16390ow
    public C33711ex ACL() {
        C16470p4 r1 = this.A00;
        if (r1 == null) {
            return null;
        }
        return AnonymousClass4EY.A00(r1, r1.A00);
    }

    @Override // X.AbstractC16390ow
    public void Abv(C16470p4 r1) {
        this.A00 = r1;
        A14();
    }
}
