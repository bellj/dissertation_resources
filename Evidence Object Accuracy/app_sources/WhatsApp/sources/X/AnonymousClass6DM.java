package X;

/* renamed from: X.6DM  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6DM implements AbstractC21730xt {
    public final /* synthetic */ C127205u7 A00;
    public final /* synthetic */ C128655wS A01;

    public AnonymousClass6DM(C127205u7 r1, C128655wS r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        this.A01.A00(0);
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r3, String str) {
        this.A01.A00(1);
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r4, String str) {
        C12970iu.A1B(((C14820m6) ((C134526Ey) ((AnonymousClass18X) this.A00.A01.get())).A01.get()).A00.edit(), "shops_privacy_notice", Integer.valueOf(C117295Zj.A0W(r4.A0E("shops_notice"), "tos_version")).intValue());
        this.A01.A00(2);
    }
}
