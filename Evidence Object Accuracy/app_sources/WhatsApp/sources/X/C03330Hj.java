package X;

/* renamed from: X.0Hj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C03330Hj extends AnonymousClass0OO implements AbstractC12140hR {
    public String A00;

    public C03330Hj(String str) {
        this.A00 = str;
    }

    @Override // X.AbstractC12140hR
    public C03440Hu AH9() {
        return null;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("TextChild: '");
        sb.append(this.A00);
        sb.append("'");
        return sb.toString();
    }
}
