package X;

import java.util.List;
import org.json.JSONObject;

/* renamed from: X.5h3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120975h3 extends C1309860t {
    public final String A00;
    public final String A01;

    public C120975h3(AnonymousClass1V8 r4) {
        super(r4);
        String A0H = r4.A0H("type");
        this.A00 = A0H;
        String A0W = C117295Zj.A0W(r4, "zip_format");
        if (!"ZIP".equals(A0H)) {
            A0W = null;
        } else if (A0W == null) {
            A0W = "xxxxx";
        }
        this.A01 = A0W;
    }

    public C120975h3(String str, String str2, String str3, String str4, List list, boolean z, boolean z2) {
        super(str2, str3, list, z, z2);
        this.A00 = str;
        this.A01 = str4;
    }

    public C120975h3(JSONObject jSONObject) {
        super(jSONObject);
        this.A00 = jSONObject.getString("type");
        this.A01 = jSONObject.optString("zip_format", null);
    }

    @Override // X.C1309860t
    public JSONObject A00() {
        JSONObject put = super.A00().put("type", this.A00);
        String str = this.A01;
        if (str != null) {
            put.put("zip_format", str);
        }
        return put;
    }
}
