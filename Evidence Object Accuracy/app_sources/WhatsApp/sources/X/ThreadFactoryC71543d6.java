package X;

import com.facebook.redex.RunnableBRunnable0Shape10S0200000_I1;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.3d6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ThreadFactoryC71543d6 implements ThreadFactory {
    public final int A00 = 10;
    public final String A01;
    public final AtomicInteger A02 = new AtomicInteger(1);
    public final boolean A03;

    public ThreadFactoryC71543d6(String str) {
        this.A01 = str;
        this.A03 = true;
    }

    @Override // java.util.concurrent.ThreadFactory
    public Thread newThread(Runnable runnable) {
        String str;
        RunnableBRunnable0Shape10S0200000_I1 runnableBRunnable0Shape10S0200000_I1 = new RunnableBRunnable0Shape10S0200000_I1(this, 0, runnable);
        if (this.A03) {
            StringBuilder A0h = C12960it.A0h();
            A0h.append(this.A01);
            A0h.append("-");
            str = C12960it.A0f(A0h, this.A02.getAndIncrement());
        } else {
            str = this.A01;
        }
        return new Thread(runnableBRunnable0Shape10S0200000_I1, str);
    }
}
