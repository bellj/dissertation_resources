package X;

import android.graphics.Bitmap;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaImageView;

/* renamed from: X.3b1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70273b1 implements AbstractC41521tf {
    public final /* synthetic */ C60902yx A00;

    @Override // X.AbstractC41521tf
    public /* synthetic */ void AQV() {
    }

    public C70273b1(C60902yx r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC41521tf
    public int AGm() {
        return this.A00.A01.getDimensionPixelSize(R.dimen.payment_bitmap_image_url_max_size);
    }

    @Override // X.AbstractC41521tf
    public void Adg(Bitmap bitmap, View view, AbstractC15340mz r6) {
        C60902yx r2 = this.A00;
        WaImageView waImageView = r2.A00;
        if (bitmap != null) {
            waImageView.setVisibility(0);
            r2.A00.setImageBitmap(bitmap);
            return;
        }
        waImageView.setVisibility(8);
    }

    @Override // X.AbstractC41521tf
    public void Adu(View view) {
        C12990iw.A0x(view.getContext(), this.A00.A00, R.drawable.ic_receipt_24dp);
    }
}
