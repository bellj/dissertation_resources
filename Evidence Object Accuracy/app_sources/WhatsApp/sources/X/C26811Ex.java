package X;

import android.content.Context;

/* renamed from: X.1Ex  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26811Ex extends AbstractC26791Ev {
    public final C14330lG A00;
    public final AnonymousClass018 A01;
    public final AnonymousClass19M A02;
    public final AnonymousClass1AB A03;

    public C26811Ex(C14330lG r3, AnonymousClass018 r4, AnonymousClass19M r5, AnonymousClass1AB r6, AbstractC14440lR r7) {
        super(new C39401pq(r7, "ProcessDoodleQueue"));
        this.A00 = r3;
        this.A02 = r5;
        this.A01 = r4;
        this.A03 = r6;
    }

    public void A06(Context context, AbstractC39801qZ r12, AbstractC14470lU r13, String str) {
        if (str == null) {
            r12.AS1(null);
            return;
        }
        AnonymousClass459 r1 = new AnonymousClass459(context, this.A00, this.A01, this.A02, r12, r13, this.A03, str);
        A01(r1.A03, r1);
    }
}
