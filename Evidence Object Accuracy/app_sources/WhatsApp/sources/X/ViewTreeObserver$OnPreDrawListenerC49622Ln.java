package X;

import android.view.View;
import android.view.ViewTreeObserver;
import com.whatsapp.HomeActivity;

/* renamed from: X.2Ln  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ViewTreeObserver$OnPreDrawListenerC49622Ln implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ View A00;
    public final /* synthetic */ HomeActivity A01;

    public ViewTreeObserver$OnPreDrawListenerC49622Ln(View view, HomeActivity homeActivity) {
        this.A01 = homeActivity;
        this.A00 = view;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        this.A00.getViewTreeObserver().removeOnPreDrawListener(this);
        return true;
    }
}
