package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0Xi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07270Xi implements AbstractC11720gk {
    public int A00;
    public int A01 = 1;
    public int A02;
    public AbstractC11720gk A03 = null;
    public EnumC03760Ja A04 = EnumC03760Ja.UNKNOWN;
    public C02580Cy A05 = null;
    public AbstractC07280Xj A06;
    public List A07 = new ArrayList();
    public List A08 = new ArrayList();
    public boolean A09 = false;
    public boolean A0A = false;
    public boolean A0B = false;

    public C07270Xi(AbstractC07280Xj r4) {
        this.A06 = r4;
    }

    public void A00() {
        this.A08.clear();
        this.A07.clear();
        this.A0B = false;
        this.A02 = 0;
        this.A0A = false;
        this.A09 = false;
    }

    public void A01(int i) {
        if (!this.A0B) {
            this.A0B = true;
            this.A02 = i;
            for (AbstractC11720gk r0 : this.A07) {
                r0.AfH(r0);
            }
        }
    }

    public void A02(AbstractC11720gk r2) {
        this.A07.add(r2);
        if (this.A0B) {
            r2.AfH(r2);
        }
    }

    @Override // X.AbstractC11720gk
    public void AfH(AbstractC11720gk r7) {
        List<C07270Xi> list = this.A08;
        for (C07270Xi r0 : list) {
            if (!r0.A0B) {
                return;
            }
        }
        this.A0A = true;
        AbstractC11720gk r02 = this.A03;
        if (r02 != null) {
            r02.AfH(this);
        }
        if (this.A09) {
            this.A06.AfH(this);
            return;
        }
        C07270Xi r4 = null;
        int i = 0;
        for (C07270Xi r1 : list) {
            if (!(r1 instanceof C02580Cy)) {
                i++;
                r4 = r1;
            }
        }
        if (r4 != null && i == 1 && r4.A0B) {
            C02580Cy r2 = this.A05;
            if (r2 != null) {
                if (r2.A0B) {
                    this.A00 = this.A01 * r2.A02;
                } else {
                    return;
                }
            }
            A01(r4.A02 + this.A00);
        }
        AbstractC11720gk r03 = this.A03;
        if (r03 != null) {
            r03.AfH(this);
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.A06.A03.A0f);
        sb.append(":");
        sb.append(this.A04);
        sb.append("(");
        sb.append(this.A0B ? Integer.valueOf(this.A02) : "unresolved");
        sb.append(") <t=");
        sb.append(this.A08.size());
        sb.append(":d=");
        sb.append(this.A07.size());
        sb.append(">");
        return sb.toString();
    }
}
