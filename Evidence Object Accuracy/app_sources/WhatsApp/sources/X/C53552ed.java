package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.conversation.conversationrow.DynamicButtonsLayout;

/* renamed from: X.2ed  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53552ed extends AnonymousClass04v {
    public final /* synthetic */ int A00;
    public final /* synthetic */ DynamicButtonsLayout A01;

    public C53552ed(DynamicButtonsLayout dynamicButtonsLayout, int i) {
        this.A01 = dynamicButtonsLayout;
        this.A00 = i;
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r7) {
        super.A06(view, r7);
        DynamicButtonsLayout dynamicButtonsLayout = this.A01;
        C12970iu.A1O(r7, C12960it.A0X(dynamicButtonsLayout.getContext(), ((AnonymousClass1ZN) dynamicButtonsLayout.A02.get(this.A00)).A03, C12970iu.A1b(), 0, R.string.accessibility_action_click_message_button));
    }
}
