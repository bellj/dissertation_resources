package X;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape10S0100000_I0_10;
import com.facebook.redex.ViewOnClickCListenerShape0S0200000_I0;
import com.facebook.redex.ViewOnClickCListenerShape0S0300000_I0;
import com.facebook.redex.ViewOnClickCListenerShape3S0100000_I0_3;
import com.whatsapp.R;
import com.whatsapp.registration.RegisterPhone;
import com.whatsapp.registration.report.BanReportViewModel;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.util.Iterator;

/* renamed from: X.20u  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC452520u extends AbstractActivityC452620v implements AbstractC452720w {
    public static int A0O = 7;
    public static int A0P;
    public static long A0Q;
    public static String A0R;
    public static String A0S;
    public static String A0T;
    public ProgressDialog A00;
    public C22690zU A01;
    public C20640w5 A02;
    public AnonymousClass19Y A03;
    public C21740xu A04;
    public C15890o4 A05;
    public C25771At A06;
    public C22040yO A07;
    public AnonymousClass11G A08;
    public C91794Td A09;
    public C22660zR A0A;
    public C64323Fc A0B;
    public C20800wL A0C;
    public C18350sJ A0D;
    public C25661Ag A0E;
    public BanReportViewModel A0F;
    public AnonymousClass12U A0G;
    public AnonymousClass1R1 A0H;
    public C22650zQ A0I;
    public String A0J;
    public boolean A0K = true;
    public boolean A0L = false;
    public boolean A0M;
    public boolean A0N;

    public static int A03(C22680zT r9, String str, String str2) {
        try {
            int length = str.length();
            if (length < 1 || length > 3) {
                StringBuilder sb = new StringBuilder();
                sb.append("enterphone/cc/bad-length cc=");
                sb.append(str);
                Log.w(sb.toString());
                return 2;
            }
            try {
                if (AnonymousClass1NE.A01.indexOfKey(Integer.parseInt(str)) >= 0) {
                    if (str2 == null || str2.length() == 0 || str2.replaceAll("\\D", "").length() == 0) {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("enterphone/num/error/empty cc=");
                        sb2.append(str);
                        Log.w(sb2.toString());
                        return 4;
                    }
                    int parseInt = Integer.parseInt(str);
                    String A02 = r9.A02(parseInt, str2.replaceAll("\\D", ""));
                    int length2 = A02.length();
                    r9.A05();
                    Iterator it = r9.A02.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        AnonymousClass1ND r1 = (AnonymousClass1ND) it.next();
                        if (r1.A00 == parseInt) {
                            int A00 = r1.A00(length2);
                            if (A00 != 0) {
                                StringBuilder sb3 = new StringBuilder();
                                sb3.append("enterphone/num/error/invalid cc=");
                                sb3.append(parseInt);
                                sb3.append(" phone=");
                                sb3.append(A02);
                                sb3.append(" res=");
                                sb3.append(A00);
                                Log.w(sb3.toString());
                                return A00 < 0 ? 5 : 6;
                            }
                        }
                    }
                    int i = length + length2;
                    if (i <= 15 && i >= 8) {
                        return 1;
                    }
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append("enterphone/num/error/length cc=");
                    sb4.append(str);
                    sb4.append(" ph=");
                    sb4.append(A02);
                    Log.w(sb4.toString());
                    return 7;
                }
            } catch (NumberFormatException unused) {
            }
            StringBuilder sb5 = new StringBuilder();
            sb5.append("enterphone/cc/bad-name ");
            sb5.append(str);
            Log.w(sb5.toString());
            return 3;
        } catch (IOException e) {
            Log.e("enterphone/error trimLeadingZero or nativeNameFromCallingCode from CountryPhoneInfo IOException", e);
            return 7;
        }
    }

    public static int A09(C22680zT r3, String str, String str2) {
        int A03 = A03(r3, str, str2);
        if (A03 != 7 && A03 != 5 && A03 != 6) {
            return A03;
        }
        int length = str.length() + str2.length();
        if (length > 17 || length < 6) {
            StringBuilder sb = new StringBuilder("enterphone/num/allow-landline/error/length input=");
            sb.append(str2);
            Log.w(sb.toString());
            return 7;
        }
        StringBuilder sb2 = new StringBuilder("enterphone/num/allow-landline/ok/length input=");
        sb2.append(str2);
        Log.i(sb2.toString());
        return 1;
    }

    public void A2e() {
        C36021jC.A01(this, 9);
    }

    public void A2f(int i) {
        if (this instanceof RegisterPhone) {
            A0O = i;
            SharedPreferences.Editor edit = ((RegisterPhone) this).getPreferences(0).edit();
            edit.putInt("com.whatsapp.registration.RegisterPhone.verification_state", A0O);
            if (!edit.commit()) {
                Log.w("registerphone/savestate/commit failed");
            }
        }
    }

    public void A2g(String str, String str2, String str3) {
        this.A0D.A0C(str, str2, str3);
        this.A0A.A05(false);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        C41691tw.A03(this, R.color.lightStatusBarBackgroundColor);
        this.A0B = new C64323Fc(this, ((ActivityC13810kN) this).A09);
        BanReportViewModel banReportViewModel = (BanReportViewModel) new AnonymousClass02A(this).A00(BanReportViewModel.class);
        this.A0F = banReportViewModel;
        banReportViewModel.A01.A05(this, new AnonymousClass02B() { // from class: X.3Qe
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                AbstractActivityC452520u r1 = AbstractActivityC452520u.this;
                C36021jC.A00(r1, 127);
                C36021jC.A00(r1, 128);
                AbstractC15420nE.A00(r1, (String) obj);
            }
        });
        this.A0F.A02.A05(this, new AnonymousClass02B() { // from class: X.3Qd
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                AbstractActivityC452520u r4 = AbstractActivityC452520u.this;
                int A05 = C12960it.A05(obj);
                int i = 128;
                if (A05 != 1) {
                    if (A05 != 2) {
                        i = 129;
                        if (A05 != 3) {
                            if (A05 == 4) {
                                C36021jC.A00(r4, 129);
                                return;
                            }
                            return;
                        }
                    } else {
                        C36021jC.A00(r4, 127);
                    }
                    C36021jC.A01(r4, i);
                    return;
                }
                C36021jC.A00(r4, 127);
                C36021jC.A00(r4, 128);
                ((ActivityC13810kN) r4).A05.A0H(new RunnableBRunnable0Shape10S0100000_I0_10(r4, 33));
            }
        });
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        ProgressDialog progressDialog;
        int i2;
        if (i == 9) {
            ProgressDialog progressDialog2 = new ProgressDialog(this);
            progressDialog2.setMessage(getString(R.string.register_connecting));
            progressDialog2.setIndeterminate(true);
            progressDialog2.setCancelable(false);
            this.A00 = progressDialog2;
            return progressDialog2;
        } else if (i == 22) {
            Log.w("enterphone/dialog/unrecoverable-error");
            StringBuilder sb = new StringBuilder("register-phone2 +");
            sb.append(A0R);
            sb.append(A0S);
            String obj = sb.toString();
            C004802e r2 = new C004802e(this);
            r2.A06(R.string.register_unrecoverable_error);
            r2.setPositiveButton(R.string.register_contact_support, new DialogInterface.OnClickListener(obj) { // from class: X.4gh
                public final /* synthetic */ String A01;

                {
                    this.A01 = r2;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i3) {
                    AbstractActivityC452520u r4 = AbstractActivityC452520u.this;
                    String str = this.A01;
                    C36021jC.A00(r4, 22);
                    r4.A06.A00(null, r4, str, false);
                }
            });
            r2.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() { // from class: X.4g0
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i3) {
                    C36021jC.A00(AbstractActivityC452520u.this, 22);
                }
            });
            return r2.create();
        } else if (i == 109) {
            AbstractC14440lR r0 = ((ActivityC13830kP) this).A05;
            return AnonymousClass23M.A03(this, this.A03, ((ActivityC13810kN) this).A07, ((ActivityC13810kN) this).A08, this.A05, this.A08, this.A0C, r0);
        } else if (i != 114) {
            switch (i) {
                case 124:
                    return AnonymousClass23M.A04(this, this.A03, ((ActivityC13830kP) this).A01, this.A08, null, A0R, A0S);
                case 125:
                    return AnonymousClass23M.A05(this, this.A03, this.A08, A0R, A0S);
                case 126:
                    AnonymousClass19Y r11 = this.A03;
                    AnonymousClass018 r12 = ((ActivityC13830kP) this).A01;
                    AnonymousClass11G r13 = this.A08;
                    String str = A0R;
                    String str2 = A0S;
                    return AnonymousClass23M.A09(((ActivityC13790kL) this).A00, this, ((ActivityC13810kN) this).A05, r11, r12, r13, this.A0H, null, str, str2);
                case 127:
                    progressDialog = new ProgressDialog(this);
                    i2 = R.string.register_user_is_banned_preparing_report;
                    break;
                case 128:
                    return AnonymousClass23M.A06(this, null, new RunnableBRunnable0Shape10S0100000_I0_10(this, 34), new RunnableBRunnable0Shape10S0100000_I0_10(this, 31));
                case 129:
                    progressDialog = new ProgressDialog(this);
                    i2 = R.string.register_user_is_banned_report_delete_wait_progress;
                    break;
                case 130:
                    AnonymousClass018 r4 = ((ActivityC13830kP) this).A01;
                    String str3 = A0R;
                    String str4 = A0S;
                    RunnableBRunnable0Shape10S0100000_I0_10 runnableBRunnable0Shape10S0100000_I0_10 = new RunnableBRunnable0Shape10S0100000_I0_10(this, 32);
                    int A00 = C88034Ea.A00(this.A0H.A02);
                    String A0G = r4.A0G(AnonymousClass23M.A0E(str3, str4));
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(A0G);
                    sb2.append("\n\n");
                    sb2.append(getString(A00));
                    SpannableString spannableString = new SpannableString(sb2.toString());
                    spannableString.setSpan(new StyleSpan(1), 0, A0G.length() + 2, 33);
                    View inflate = LayoutInflater.from(this).inflate(R.layout.mtrl_alert_dialog_actions_vertical, (ViewGroup) null);
                    C004802e r42 = new C004802e(this);
                    r42.A0A(spannableString);
                    r42.setView(inflate);
                    r42.A0B(false);
                    TextView textView = (TextView) AnonymousClass028.A0D(inflate, R.id.button3);
                    TextView textView2 = (TextView) AnonymousClass028.A0D(inflate, R.id.button1);
                    TextView textView3 = (TextView) AnonymousClass028.A0D(inflate, R.id.button2);
                    textView.setVisibility(0);
                    textView.setText(R.string.ok);
                    textView2.setVisibility(0);
                    textView2.setText(R.string.register_user_support_button);
                    textView3.setVisibility(0);
                    textView3.setText(R.string.register_user_is_banned_request_report);
                    textView.setOnClickListener(new ViewOnClickCListenerShape0S0200000_I0(this, 43, null));
                    textView2.setOnClickListener(new ViewOnClickCListenerShape3S0100000_I0_3(this, 33));
                    textView3.setOnClickListener(new ViewOnClickCListenerShape0S0300000_I0(this, runnableBRunnable0Shape10S0100000_I0_10, null, 13));
                    return r42.create();
                default:
                    return super.onCreateDialog(i);
            }
            progressDialog.setMessage(getString(i2));
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            return progressDialog;
        } else {
            C14830m7 r14 = ((ActivityC13790kL) this).A05;
            C21740xu r122 = this.A04;
            DialogC58312oc r8 = new DialogC58312oc(this, ((ActivityC13790kL) this).A00, this.A02, r122, ((ActivityC13810kN) this).A08, r14, ((ActivityC13830kP) this).A01);
            r8.setOnCancelListener(new DialogInterface$OnCancelListenerC96114es(this));
            return r8;
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        C64323Fc r1 = this.A0B;
        r1.A02 = true;
        AnonymousClass23M.A0J(r1.A04, AnonymousClass23M.A00);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        this.A0B.A00();
    }
}
