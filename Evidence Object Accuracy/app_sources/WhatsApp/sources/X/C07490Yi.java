package X;

import android.os.Handler;
import android.widget.TextView;
import androidx.biometric.FingerprintDialogFragment;

/* renamed from: X.0Yi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07490Yi implements AnonymousClass02B {
    public final /* synthetic */ FingerprintDialogFragment A00;

    public C07490Yi(FingerprintDialogFragment fingerprintDialogFragment) {
        this.A00 = fingerprintDialogFragment;
    }

    @Override // X.AnonymousClass02B
    public /* bridge */ /* synthetic */ void ANq(Object obj) {
        CharSequence charSequence = (CharSequence) obj;
        FingerprintDialogFragment fingerprintDialogFragment = this.A00;
        Handler handler = fingerprintDialogFragment.A05;
        Runnable runnable = fingerprintDialogFragment.A06;
        handler.removeCallbacks(runnable);
        TextView textView = fingerprintDialogFragment.A03;
        if (textView != null) {
            textView.setText(charSequence);
        }
        handler.postDelayed(runnable, 2000);
    }
}
