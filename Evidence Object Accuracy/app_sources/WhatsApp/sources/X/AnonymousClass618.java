package X;

import android.hardware.Camera;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import org.chromium.net.UrlRequest;

/* renamed from: X.618  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass618 {
    public static final AtomicInteger A05 = new AtomicInteger(Integer.MIN_VALUE);
    public final int A00;
    public final Camera.Parameters A01;
    public final Camera A02;
    public final C119055co A03;
    public final C119075cq A04;

    public AnonymousClass618(Camera.Parameters parameters, Camera camera, C119055co r3, C119075cq r4, int i) {
        this.A02 = camera;
        this.A01 = parameters;
        this.A03 = r3;
        this.A04 = r4;
        this.A00 = i;
    }

    public static String A00(int i) {
        if (i == 0) {
            return "auto";
        }
        switch (i) {
            case 2:
                return "action";
            case 3:
                return "portrait";
            case 4:
                return "landscape";
            case 5:
                return "night";
            case 6:
                return "night-portrait";
            case 7:
                return "theatre";
            case 8:
                return "beach";
            case 9:
                return "snow";
            case 10:
                return "sunset";
            case 11:
                return "steadyphoto";
            case 12:
                return "fireworks";
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                return "sports";
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                return "party";
            case 15:
                return "candlelight";
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                return "barcode";
            case 17:
                return "hdr";
            default:
                return null;
        }
    }

    public static boolean A01(List list, Object obj) {
        return (list == null || obj == null || !list.contains(obj)) ? false : true;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v58, resolved type: android.hardware.Camera$Parameters */
    /* JADX DEBUG: Multi-variable search result rejected for r1v61, resolved type: android.hardware.Camera$Parameters */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:236:0x04df  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A02(X.C125475rJ r11, java.lang.Object r12) {
        /*
        // Method dump skipped, instructions count: 1426
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass618.A02(X.5rJ, java.lang.Object):boolean");
    }
}
