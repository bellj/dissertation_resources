package X;

import android.util.LongSparseArray;
import android.view.View;
import com.facebook.rendercore.RenderTreeNode;
import java.util.Locale;

/* renamed from: X.3H1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3H1 {
    public final int A00;
    public final int A01;
    public final LongSparseArray A02 = new LongSparseArray();
    public final RenderTreeNode A03;
    public final RenderTreeNode[] A04;

    public AnonymousClass3H1(RenderTreeNode renderTreeNode, RenderTreeNode[] renderTreeNodeArr, int i, int i2) {
        this.A03 = renderTreeNode;
        this.A04 = renderTreeNodeArr;
        this.A01 = i;
        this.A00 = i2;
        int i3 = 0;
        while (true) {
            RenderTreeNode[] renderTreeNodeArr2 = this.A04;
            if (i3 < renderTreeNodeArr2.length) {
                RenderTreeNode renderTreeNode2 = renderTreeNodeArr2[i3];
                LongSparseArray longSparseArray = this.A02;
                if (longSparseArray.get(renderTreeNode2.A07.A02()) == null) {
                    this.A02.put(this.A04[i3].A07.A02(), Integer.valueOf(i3));
                    i3++;
                } else {
                    int A05 = C12960it.A05(longSparseArray.get(renderTreeNode2.A07.A02()));
                    RenderTreeNode renderTreeNode3 = renderTreeNodeArr2[A05];
                    Locale locale = Locale.US;
                    Object[] objArr = new Object[5];
                    C12960it.A1P(objArr, i3, 0);
                    objArr[1] = renderTreeNode2.A00(null);
                    C12960it.A1P(objArr, A05, 2);
                    objArr[3] = renderTreeNode3.A00(null);
                    StringBuilder A0h = C12960it.A0h();
                    String A00 = A00(this.A01);
                    String A002 = A00(this.A00);
                    A0h.append("RenderTree details:\n");
                    Object[] A1a = C12980iv.A1a();
                    A1a[0] = A00;
                    A1a[1] = A002;
                    A0h.append(String.format(locale, "WidthSpec=%s; HeightSpec=%s\n", A1a));
                    Object[] objArr2 = new Object[1];
                    int length = renderTreeNodeArr2.length;
                    C12960it.A1P(objArr2, length, 0);
                    A0h.append(String.format(locale, "Full child list (size = %d):\n", objArr2));
                    for (int i4 = 0; i4 < length; i4++) {
                        A0h.append(String.format(locale, "%s\n", renderTreeNodeArr2[i4].A00(this)));
                    }
                    objArr[4] = A0h.toString();
                    throw C12960it.A0U(String.format(locale, "RenderTrees must not have RenderUnits with the same ID:\nAttempted to add item with existing ID at index %d: %s\nExisting item at index %d: %s\nFull RenderTree: %s", objArr));
                }
            } else {
                return;
            }
        }
    }

    public static String A00(int i) {
        String str;
        int size = View.MeasureSpec.getSize(i);
        int mode = View.MeasureSpec.getMode(i);
        if (mode == Integer.MIN_VALUE) {
            str = "AT_MOST";
        } else {
            str = mode == 1073741824 ? "EXACTLY" : mode == 0 ? "UNSPECIFIED" : "INVALID";
        }
        Locale locale = Locale.US;
        Object[] A1a = C12980iv.A1a();
        C12960it.A1O(A1a, size);
        A1a[1] = str;
        return String.format(locale, "[%d, %s]", A1a);
    }
}
