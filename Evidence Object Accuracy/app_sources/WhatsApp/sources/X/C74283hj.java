package X;

import android.graphics.Typeface;

/* renamed from: X.3hj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74283hj extends AnonymousClass08K {
    public final /* synthetic */ AnonymousClass2Zd A00;

    @Override // X.AnonymousClass08K
    public void A01(int i) {
    }

    public C74283hj(AnonymousClass2Zd r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass08K
    public void A02(Typeface typeface) {
        AnonymousClass2Zd r1 = this.A00;
        r1.A0m = true;
        r1.A05();
        r1.invalidateSelf();
    }
}
