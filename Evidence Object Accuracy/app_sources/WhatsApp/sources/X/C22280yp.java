package X;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import com.facebook.redex.RunnableBRunnable0Shape1S0300000_I0_1;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.0yp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22280yp {
    public final Handler A00 = new Handler(Looper.getMainLooper());
    public final C22330yu A01;
    public final C18780t0 A02;
    public final C20660w7 A03;
    public final AbstractC14440lR A04;
    public final HashMap A05 = new HashMap();
    public final HashMap A06 = new HashMap();
    public final Set A07 = new HashSet();
    public final AtomicBoolean A08 = new AtomicBoolean(false);

    public C22280yp(C22330yu r3, C18780t0 r4, C20660w7 r5, AbstractC14440lR r6) {
        this.A04 = r6;
        this.A03 = r5;
        this.A01 = r3;
        this.A02 = r4;
    }

    public int A00(AbstractC14640lm r8, UserJid userJid) {
        C43601xI r6;
        C33911fH r62 = (C33911fH) this.A06.get(r8);
        if (r62 == null) {
            return -1;
        }
        if (userJid == null || !C15380n4.A0J(r8)) {
            long j = r62.A03;
            if (j == 0 || j + 25000 <= SystemClock.elapsedRealtime()) {
                return -1;
            }
            return r62.A00;
        }
        HashMap hashMap = r62.A05;
        if (hashMap == null || (r6 = (C43601xI) hashMap.get(userJid)) == null) {
            return -1;
        }
        long j2 = r6.A01;
        if (j2 == 0 || j2 + 25000 <= SystemClock.elapsedRealtime()) {
            return -1;
        }
        return r6.A00;
    }

    public GroupJid A01(AbstractC14640lm r7, int i, long j) {
        HashMap hashMap;
        C43601xI r0;
        HashMap hashMap2 = this.A06;
        C33911fH r3 = (C33911fH) hashMap2.get(r7);
        if (r3 == null) {
            r3 = new C33911fH();
            hashMap2.put(r7, r3);
        }
        if (j == 0) {
            r3.A04 = 0;
        } else {
            r3.A04 = j;
        }
        r3.A03 = 0;
        r3.A01 = i;
        for (Map.Entry entry : hashMap2.entrySet()) {
            if (!(!C15380n4.A0J((Jid) entry.getKey()) || (hashMap = ((C33911fH) entry.getValue()).A05) == null || (r0 = (C43601xI) hashMap.get(r7)) == null)) {
                r0.A01 = 0;
                return GroupJid.of((Jid) entry.getKey());
            }
        }
        return null;
    }

    public void A02() {
        HashMap hashMap = this.A06;
        HashSet hashSet = new HashSet(hashMap.keySet());
        hashMap.clear();
        HashMap hashMap2 = this.A05;
        for (RunnableBRunnable0Shape1S0300000_I0_1 runnableBRunnable0Shape1S0300000_I0_1 : hashMap2.values()) {
            this.A00.removeCallbacks(runnableBRunnable0Shape1S0300000_I0_1);
        }
        hashMap2.clear();
        Iterator it = hashSet.iterator();
        while (it.hasNext()) {
            this.A01.A05((AbstractC14640lm) it.next());
        }
    }

    public void A03(AbstractC14640lm r8) {
        C33911fH r5;
        HashMap hashMap;
        if (!(!C15380n4.A0J(r8) || (r5 = (C33911fH) this.A06.get(r8)) == null || (hashMap = r5.A05) == null)) {
            for (Map.Entry entry : hashMap.entrySet()) {
                ((C43601xI) entry.getValue()).A01 = 0;
                StringBuilder sb = new StringBuilder();
                sb.append(r8.getRawString());
                sb.append(((Jid) entry.getKey()).getRawString());
                RunnableBRunnable0Shape1S0300000_I0_1 runnableBRunnable0Shape1S0300000_I0_1 = (RunnableBRunnable0Shape1S0300000_I0_1) this.A05.get(sb.toString());
                if (runnableBRunnable0Shape1S0300000_I0_1 != null) {
                    this.A00.removeCallbacks(runnableBRunnable0Shape1S0300000_I0_1);
                }
            }
            r5.A03 = 0;
        }
    }

    public void A04(AbstractC14640lm r4) {
        int type;
        if (C15380n4.A0F(r4)) {
            return;
        }
        if ((r4 == null || !((type = r4.getType()) == 8 || type == 7)) && !C15380n4.A0O(r4)) {
            this.A04.Ab5(new AnonymousClass372(r4, this), new Void[0]);
        }
    }

    public void A05(AbstractC14640lm r6, UserJid userJid) {
        String obj;
        HashMap hashMap = this.A06;
        C33911fH r4 = (C33911fH) hashMap.get(r6);
        if (r4 == null) {
            r4 = new C33911fH();
            hashMap.put(r6, r4);
        }
        if (userJid != null && C15380n4.A0J(r6)) {
            HashMap hashMap2 = r4.A05;
            if (hashMap2 == null) {
                hashMap2 = new HashMap();
                r4.A05 = hashMap2;
            }
            C43601xI r1 = (C43601xI) hashMap2.get(userJid);
            if (r1 == null) {
                r1 = new C43601xI();
                r4.A05.put(userJid, r1);
            }
            r1.A01 = 0;
        }
        r4.A03 = 0;
        if (userJid == null) {
            obj = r6.getRawString();
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append(r6.getRawString());
            sb.append(userJid.getRawString());
            obj = sb.toString();
        }
        RunnableBRunnable0Shape1S0300000_I0_1 runnableBRunnable0Shape1S0300000_I0_1 = (RunnableBRunnable0Shape1S0300000_I0_1) this.A05.get(obj);
        if (runnableBRunnable0Shape1S0300000_I0_1 != null) {
            this.A00.removeCallbacks(runnableBRunnable0Shape1S0300000_I0_1);
        }
    }

    public boolean A06(AbstractC14640lm r7) {
        C33911fH r0;
        if (C15380n4.A0J(r7) || ((r0 = (C33911fH) this.A06.get(r7)) != null && r0.A04 == 1)) {
            return true;
        }
        return false;
    }
}
