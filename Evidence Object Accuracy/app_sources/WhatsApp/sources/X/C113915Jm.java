package X;

import android.content.Intent;
import android.provider.MediaStore;

/* renamed from: X.5Jm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C113915Jm extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ int $mediaType;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C113915Jm(int i) {
        super(0);
        this.$mediaType = i;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        String str;
        Intent intent = new Intent("android.intent.action.PICK", MediaStore.Video.Media.INTERNAL_CONTENT_URI);
        int i = this.$mediaType;
        if (i == 2) {
            str = "image/gif";
        } else if (i != 4) {
            str = "image/*";
        } else {
            str = "video/*";
        }
        intent.setType(str);
        return intent;
    }
}
