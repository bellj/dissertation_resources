package X;

import android.os.Bundle;
import android.os.Message;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jobqueue.job.SendMediaErrorReceiptJob;
import com.whatsapp.jobqueue.job.SendPlayedReceiptJobV2;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Future;

/* renamed from: X.0w7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20660w7 {
    public final C20670w8 A00;
    public final C16240og A01;
    public final C18280sC A02;
    public final C243915i A03;
    public final AnonymousClass143 A04;
    public final C16590pI A05;
    public final C17220qS A06;
    public final C246316g A07;
    public final C238213d A08;
    public final C17230qT A09;
    public final C20700wB A0A;

    public C20660w7(C20670w8 r1, C16240og r2, C18280sC r3, C243915i r4, AnonymousClass143 r5, C16590pI r6, C17220qS r7, C246316g r8, C238213d r9, C17230qT r10, C20700wB r11) {
        this.A05 = r6;
        this.A00 = r1;
        this.A06 = r7;
        this.A03 = r4;
        this.A0A = r11;
        this.A08 = r9;
        this.A01 = r2;
        this.A07 = r8;
        this.A09 = r10;
        this.A02 = r3;
        this.A04 = r5;
    }

    public Future A00(C32561cM r11, C15580nU r12, AnonymousClass1P4 r13, List list) {
        C16240og r1 = this.A01;
        if (r1.A06) {
            boolean z = false;
            if (r1.A04 == 2) {
                z = true;
            }
            if (z) {
                String A01 = r13 == null ? this.A06.A01() : r13.A01;
                try {
                    return this.A06.A04(Message.obtain(null, 0, 210, 0, new C32571cN(r11, r12, r13, A01, list)), A01, false);
                } catch (AnonymousClass1VI unused) {
                }
            }
        }
        return null;
    }

    public Future A01(AbstractC32481cE r11, AbstractC32491cF r12, AnonymousClass1P4 r13, C32511cH r14) {
        C16240og r1 = this.A01;
        if (r1.A06) {
            boolean z = false;
            if (r1.A04 == 2) {
                z = true;
            }
            if (z) {
                String A01 = r13 == null ? this.A06.A01() : r13.A01;
                try {
                    return this.A06.A04(Message.obtain(null, 0, 209, 0, new C32521cI(r11, r12, r13, r14, A01)), A01, false);
                } catch (AnonymousClass1VI unused) {
                }
            }
        }
        return null;
    }

    public Future A02(AbstractC32481cE r7, AbstractC32491cF r8, String str) {
        C16240og r1 = this.A01;
        if (r1.A06) {
            boolean z = false;
            if (r1.A04 == 2) {
                z = true;
            }
            if (z) {
                C17220qS r4 = this.A06;
                String A01 = r4.A01();
                try {
                    return r4.A04(Message.obtain(null, 0, C43951xu.A03, 0, new C32501cG(r7, r8, A01, str)), A01, false);
                } catch (AnonymousClass1VI unused) {
                }
            }
        }
        return null;
    }

    public Future A03(AbstractC32461cC r7, String str) {
        C16240og r1 = this.A01;
        if (r1.A06) {
            boolean z = false;
            if (r1.A04 == 2) {
                z = true;
            }
            if (z) {
                C17220qS r4 = this.A06;
                String A01 = r4.A01();
                try {
                    return r4.A04(Message.obtain(null, 0, 107, 0, new C32471cD(r7, A01, str)), A01, false);
                } catch (AnonymousClass1VI unused) {
                }
            }
        }
        return null;
    }

    public void A04() {
        if (this.A01.A06) {
            Log.i("sendmethods/sendGetBroadcastLists");
            this.A06.A08(Message.obtain(null, 0, 59, 0), false);
        }
    }

    public void A05() {
        if (this.A01.A06) {
            this.A06.A08(Message.obtain(null, 0, 13, 0), false);
        }
    }

    public void A06(RunnableC32531cJ r5) {
        if (this.A01.A06) {
            Log.i("sendmethods/sendLeaveGroup");
            this.A03.A00(r5.A01, 5);
            this.A06.A08(Message.obtain(null, 0, 16, 0, r5), false);
        }
    }

    public void A07(RunnableC32531cJ r5, C15580nU r6, int i) {
        if (this.A01.A06) {
            Log.i("sendmethods/sendSetGroupEphemeralSetting");
            C17220qS r3 = this.A06;
            Message obtain = Message.obtain(null, 0, 224, 0, r5);
            obtain.getData().putParcelable("gjid", r6);
            obtain.getData().putInt("ephemeralDuration", i);
            r3.A08(obtain, false);
        }
    }

    public void A08(RunnableC32531cJ r5, C15580nU r6, boolean z) {
        if (this.A01.A06) {
            Log.i("sendmethods/sendSetGroupAnnouncements");
            C17220qS r3 = this.A06;
            Message obtain = Message.obtain(null, 0, 161, 0, r5);
            obtain.getData().putParcelable("gjid", r6);
            obtain.getData().putBoolean("announcements_only", z);
            r3.A08(obtain, false);
        }
    }

    public void A09(RunnableC32531cJ r5, C15580nU r6, boolean z) {
        if (this.A01.A06) {
            Log.i("sendmethods/sendSetGroupNoFrequentlyForwarded");
            C17220qS r3 = this.A06;
            Message obtain = Message.obtain(null, 0, 213, 0, r5);
            obtain.getData().putParcelable("gjid", r6);
            obtain.getData().putBoolean("no_frequently_forwarded", z);
            r3.A08(obtain, false);
        }
    }

    public void A0A(RunnableC32531cJ r5, C15580nU r6, boolean z) {
        if (this.A01.A06) {
            Log.i("sendmethods/sendSetGroupRestrictMode");
            C17220qS r3 = this.A06;
            Message obtain = Message.obtain(null, 0, 159, 0, r5);
            obtain.getData().putParcelable("gjid", r6);
            obtain.getData().putBoolean("restrict_mode", z);
            r3.A08(obtain, false);
        }
    }

    public void A0B(AbstractC14640lm r6, AbstractC14640lm r7, Integer num, String str, String str2, String str3) {
        if (this.A01.A06) {
            C17220qS r3 = this.A06;
            Message obtain = Message.obtain(null, 0, 117, 0, r6);
            obtain.getData().putString("messageKeyId", str);
            if (r7 != null) {
                obtain.getData().putParcelable("remoteResource", r7);
            }
            if (num != null) {
                obtain.getData().putInt("errorCode", num.intValue());
            }
            if (str2 != null) {
                obtain.getData().putString("subType", str2);
            }
            if (str3 != null) {
                obtain.getData().putString("buttonIndex", str3);
            }
            r3.A08(obtain, false);
        }
    }

    public void A0C(C15580nU r5) {
        Log.i("sendmethods/sendGetGroupDescription");
        if (this.A01.A06) {
            C17220qS r3 = this.A06;
            Message obtain = Message.obtain(null, 0, 158, 0);
            obtain.getData().putParcelable("gid", r5);
            r3.A08(obtain, false);
        }
    }

    public void A0D(C15580nU r5, String str, int i) {
        if (!this.A01.A06) {
            return;
        }
        if (!this.A0A.A00.A02(r5)) {
            Log.w("sendmethods/skip sendGetGroupInfo");
            return;
        }
        Log.w("sendmethods/sendGetGroupInfo");
        C17220qS r3 = this.A06;
        Message obtain = Message.obtain(null, 0, 20, 0);
        obtain.getData().putParcelable("gid", r5);
        obtain.getData().putString("context", str);
        obtain.getData().putInt("syncDeviceType", i);
        r3.A08(obtain, false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0032, code lost:
        if ("played".equals(r3) == false) goto L_0x0034;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0E(X.AnonymousClass1OT r5) {
        /*
            r4 = this;
            X.0qT r2 = r4.A09
            long r0 = r5.A00
            r2.A03(r0)
            X.0og r0 = r4.A01
            boolean r0 = r0.A06
            if (r0 == 0) goto L_0x0064
            java.lang.String r1 = r5.A05
            java.lang.String r0 = "receipt"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0065
            java.lang.String r3 = r5.A08
            java.lang.String r0 = "read"
            boolean r2 = r0.equals(r3)
            X.16g r0 = r4.A07
            X.0m9 r1 = r0.A00
            r0 = 361(0x169, float:5.06E-43)
            boolean r0 = r1.A07(r0)
            if (r0 == 0) goto L_0x0034
            java.lang.String r0 = "played"
            boolean r1 = r0.equals(r3)
            r0 = 1
            if (r1 != 0) goto L_0x0035
        L_0x0034:
            r0 = 0
        L_0x0035:
            if (r2 != 0) goto L_0x0039
            if (r0 == 0) goto L_0x0065
        L_0x0039:
            X.13d r1 = r4.A08
            com.whatsapp.jid.Jid r0 = r5.A01
            X.0lm r0 = X.C15380n4.A00(r0)
            boolean r0 = r1.A01(r0)
            r1 = r0 ^ 1
            android.os.Bundle r2 = new android.os.Bundle
            r2.<init>()
            java.lang.String r0 = "stanzaKey"
            r2.putParcelable(r0, r5)
            java.lang.String r0 = "disable"
            r2.putBoolean(r0, r1)
            r1 = 0
            r3 = 0
            r0 = 96
            android.os.Message r0 = android.os.Message.obtain(r1, r3, r0, r3, r2)
            X.0qS r2 = r4.A06
        L_0x0061:
            r2.A08(r0, r3)
        L_0x0064:
            return
        L_0x0065:
            X.0qS r2 = r4.A06
            r1 = 0
            r3 = 0
            r0 = 76
            android.os.Message r0 = android.os.Message.obtain(r1, r3, r0, r3, r5)
            goto L_0x0061
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20660w7.A0E(X.1OT):void");
    }

    public void A0F(AbstractC16130oV r5) {
        C16150oX r0;
        byte[] bArr;
        AnonymousClass1IS r1 = r5.A0z;
        if (!r1.A02) {
            AbstractC14640lm r02 = r1.A00;
            if ((r02 == null || r02.getType() != 9) && (r0 = r5.A02) != null && (bArr = r0.A0U) != null) {
                C20670w8 r2 = this.A00;
                AnonymousClass009.A05(r0);
                AnonymousClass009.A05(bArr);
                r2.A00(new SendMediaErrorReceiptJob(r5, null, bArr));
            }
        }
    }

    public void A0G(C32581cO r5, String str, int i) {
        Object obj;
        if (this.A01.A06) {
            StringBuilder sb = new StringBuilder("sendmethods/sendSafetyNetVerifyAppsResult appsListSize=");
            if (r5 != null) {
                obj = Integer.valueOf(r5.A01.size());
            } else {
                obj = "null";
            }
            sb.append(obj);
            sb.append(" errorCode=");
            sb.append(i);
            sb.append(" errorMessage=");
            sb.append(str);
            Log.i(sb.toString());
            C17220qS r3 = this.A06;
            Message obtain = Message.obtain(null, 0, 273, 0, r5);
            obtain.getData().putInt("errorCode", i);
            obtain.getData().putString("errorMessage", str);
            r3.A08(obtain, false);
        }
    }

    public void A0H(String str, Long l) {
        if (this.A01.A06) {
            Log.i("sendmethods/sendClearDirty");
            C17220qS r5 = this.A06;
            Message obtain = Message.obtain(null, 0, 18, 0);
            obtain.getData().putString("category", str);
            if (l != null) {
                obtain.getData().putLong("timestamp", l.longValue());
            }
            r5.A08(obtain, false);
        }
    }

    public void A0I(String str, String str2, int i) {
        if (this.A01.A06) {
            StringBuilder sb = new StringBuilder("sendmethods/sendAttestationResult jws=");
            sb.append(str);
            sb.append(" errorCode=");
            sb.append(i);
            sb.append(" errorMessage=");
            sb.append(str2);
            Log.i(sb.toString());
            C17220qS r3 = this.A06;
            Message obtain = Message.obtain(null, 0, 194, 0);
            obtain.getData().putString("result", str);
            obtain.getData().putInt("errorCode", i);
            obtain.getData().putString("errorMessage", str2);
            r3.A08(obtain, false);
        }
    }

    public void A0J(String str, String str2, List list) {
        if (this.A01.A06) {
            this.A06.A08(Message.obtain(null, 0, 1, 0, new C32451cB(str, str2, list)), false);
        }
    }

    public void A0K(List list) {
        StringBuilder sb = new StringBuilder("app/send-get-identities jids=");
        sb.append(list);
        Log.i(sb.toString());
        C16240og r1 = this.A01;
        if (r1.A06 && r1.A04 == 2) {
            C17220qS r4 = this.A06;
            Bundle bundle = new Bundle();
            bundle.putParcelableArray("jids", (Jid[]) list.toArray(new DeviceJid[0]));
            r4.A08(Message.obtain(null, 0, 153, 0, bundle), false);
        }
    }

    public void A0L(Set set, boolean z) {
        HashMap hashMap = new HashMap();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            AbstractC15340mz r7 = (AbstractC15340mz) it.next();
            AnonymousClass1IS r6 = r7.A0z;
            C32381c4 r5 = new C32381c4(r6.A00, r7.A0B(), r7 instanceof AnonymousClass1Iv);
            if (hashMap.containsKey(r5)) {
                ((List) hashMap.get(r5)).add(new C32021bU(Long.valueOf(r7.A11), r6.A01));
            } else {
                ArrayList arrayList = new ArrayList();
                arrayList.add(new C32021bU(Long.valueOf(r7.A11), r6.A01));
                hashMap.put(r5, arrayList);
            }
        }
        for (C32551cL r2 : C246316g.A00(hashMap)) {
            this.A00.A00(new SendPlayedReceiptJobV2(r2, z));
        }
    }

    public void A0M(boolean z) {
        if (this.A01.A06) {
            Log.i("sendmethods/sendGetServerProps");
            C17220qS r4 = this.A06;
            Bundle bundle = new Bundle();
            bundle.putBoolean("forceRefresh", z);
            r4.A08(Message.obtain(null, 0, 21, 0, bundle), false);
        }
    }
}
