package X;

import android.provider.Settings;

/* renamed from: X.5hK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C121145hK extends AnonymousClass69C {
    public C121145hK(C16590pI r1, C18600si r2) {
        super(r1, r2);
    }

    @Override // X.AnonymousClass69C
    public String A00() {
        if (C12970iu.A01(this.A01.A01(), "payments_device_id_algorithm") >= 2) {
            return super.A00();
        }
        return Settings.Secure.getString(this.A00.A00.getContentResolver(), "android_id");
    }
}
