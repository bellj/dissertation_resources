package X;

import com.google.android.gms.common.api.Status;
import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;

/* renamed from: X.4a8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C93414a8 {
    public static final Status A02 = new Status(8, "The connection to Google Play services was lost");
    public final AnonymousClass4IR A00 = new AnonymousClass4IR(this);
    public final Set A01 = Collections.synchronizedSet(Collections.newSetFromMap(new WeakHashMap()));
}
