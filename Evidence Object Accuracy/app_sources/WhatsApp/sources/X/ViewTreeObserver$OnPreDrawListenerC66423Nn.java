package X;

import android.view.View;
import android.view.ViewTreeObserver;

/* renamed from: X.3Nn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ViewTreeObserver$OnPreDrawListenerC66423Nn implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ View A00;
    public final /* synthetic */ C67833Tb A01;

    public ViewTreeObserver$OnPreDrawListenerC66423Nn(View view, C67833Tb r2) {
        this.A00 = view;
        this.A01 = r2;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        View view = this.A00;
        float A02 = C12990iw.A02(view);
        C67833Tb r4 = this.A01;
        view.setTranslationX(C12990iw.A01(r4.A06, A02, r4.A0C ? 1 : 0));
        view.setTranslationY(C12990iw.A01(r4.A07, C12990iw.A03(view), r4.A0D ? 1 : 0));
        if (r4.A09) {
            view.setPivotX(C12990iw.A01(r4.A01, C12990iw.A02(view), r4.A0A ? 1 : 0));
            view.setPivotY(C12990iw.A01(r4.A02, C12990iw.A03(view), r4.A0B ? 1 : 0));
        }
        C12980iv.A1G(view, this);
        return true;
    }
}
