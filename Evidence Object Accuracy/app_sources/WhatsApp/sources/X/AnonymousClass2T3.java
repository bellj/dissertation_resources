package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.widget.ImageView;
import com.whatsapp.R;

/* renamed from: X.2T3  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2T3 extends AnonymousClass2T4 {
    public Bitmap A00;
    public Drawable A01;
    public Drawable A02;
    public Drawable A03;
    public AnonymousClass018 A04;
    public AbstractC35611iN A05;
    public boolean A06;

    public AnonymousClass2T3(Context context) {
        super(context);
        Drawable A04 = AnonymousClass00T.A04(context, R.drawable.selector_orange_gradient);
        this.A03 = A04;
        A04.setCallback(this);
    }

    public void A01(Canvas canvas) {
        AnonymousClass2T1 r5;
        Drawable drawable;
        if ((this instanceof AnonymousClass2T1) && (drawable = (r5 = (AnonymousClass2T1) this).A00) != null) {
            int intrinsicHeight = drawable.getIntrinsicHeight() >> 2;
            r5.A00.setBounds(intrinsicHeight, (r5.getHeight() - r5.A00.getIntrinsicHeight()) - intrinsicHeight, r5.A00.getIntrinsicWidth() + intrinsicHeight, r5.getHeight() - intrinsicHeight);
            r5.A00.draw(canvas);
        }
    }

    @Override // android.view.View
    public void draw(Canvas canvas) {
        super.draw(canvas);
        Drawable drawable = this.A03;
        if (drawable != null) {
            drawable.setBounds(0, 0, getWidth(), getHeight());
            drawable.draw(canvas);
        }
    }

    @Override // android.widget.ImageView, android.view.View
    public void drawableHotspotChanged(float f, float f2) {
        if (Build.VERSION.SDK_INT >= 21) {
            super.drawableHotspotChanged(f, f2);
            Drawable drawable = this.A03;
            if (drawable != null) {
                drawable.setHotspot(f, f2);
            }
        }
    }

    @Override // X.AnonymousClass03X, android.widget.ImageView, android.view.View
    public void drawableStateChanged() {
        super.drawableStateChanged();
        Drawable drawable = this.A03;
        if (drawable != null && drawable.isStateful()) {
            drawable.setState(getDrawableState());
        }
    }

    public AbstractC35611iN getMediaItem() {
        return this.A05;
    }

    public Bitmap getThumbnail() {
        return this.A00;
    }

    public Uri getUri() {
        return this.A05.AAE();
    }

    @Override // android.widget.ImageView, android.view.View
    public void jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState();
        Drawable drawable = this.A03;
        if (drawable != null) {
            drawable.jumpToCurrentState();
        }
    }

    @Override // android.widget.ImageView, android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        A01(canvas);
        if (this.A06) {
            if (this.A01 == null) {
                this.A01 = AnonymousClass00T.A04(getContext(), R.drawable.photo_check);
            }
            canvas.drawColor(1073741824);
            int width = (getWidth() - this.A01.getIntrinsicWidth()) >> 1;
            int height = (getHeight() - this.A01.getIntrinsicHeight()) >> 1;
            Drawable drawable = this.A01;
            drawable.setBounds(width, height, drawable.getIntrinsicWidth() + width, this.A01.getIntrinsicHeight() + height);
            this.A01.draw(canvas);
        }
        Drawable drawable2 = this.A02;
        if (drawable2 != null) {
            drawable2.setBounds(0, 0, getWidth(), getHeight());
            this.A02.draw(canvas);
        }
    }

    @Override // android.widget.ImageView, android.view.View
    public void onMeasure(int i, int i2) {
        int defaultSize = ImageView.getDefaultSize(getSuggestedMinimumWidth(), i);
        setMeasuredDimension(defaultSize, defaultSize);
    }

    public void setChecked(boolean z) {
        if (this.A06 != z) {
            this.A06 = z;
            setSelected(z);
            invalidate();
        }
    }

    public void setFrameDrawable(Drawable drawable) {
        this.A02 = drawable;
        invalidate();
    }

    public void setMediaItem(AbstractC35611iN r3) {
        int i;
        this.A05 = r3;
        if (r3 != null) {
            int type = r3.getType();
            if (type == 0) {
                i = R.string.conversations_most_recent_image;
            } else if (type == 1) {
                i = R.string.conversations_most_recent_video;
            } else if (type == 2) {
                i = R.string.conversations_most_recent_gif;
            } else if (type == 3) {
                i = R.string.conversations_most_recent_audio;
            } else if (type == 4) {
                i = R.string.conversations_most_recent_document;
            } else {
                return;
            }
            setContentDescription(getContext().getString(i));
        }
    }

    public void setSelector(Drawable drawable) {
        Drawable drawable2 = this.A03;
        if (drawable2 != drawable) {
            if (drawable2 != null) {
                drawable2.setCallback(null);
            }
            this.A03 = drawable;
            if (drawable != null) {
                drawable.setCallback(this);
            }
        }
    }

    public void setThumbnail(Bitmap bitmap) {
        this.A00 = bitmap;
    }

    @Override // android.widget.ImageView, android.view.View
    public boolean verifyDrawable(Drawable drawable) {
        return drawable == this.A03 || super.verifyDrawable(drawable);
    }
}
