package X;

/* renamed from: X.1nW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38101nW {
    public boolean A00;
    public boolean A01;
    public byte[] A02;
    public int[] A03;
    public final AbstractC16130oV A04;
    public transient boolean A05;

    public C38101nW(AbstractC16130oV r1) {
        this.A04 = r1;
    }

    public static boolean A00(C14370lK r2) {
        return r2 == C14370lK.A0X || r2 == C14370lK.A0W || r2 == C14370lK.A0B || r2 == C14370lK.A0Z || r2 == C14370lK.A0a || r2 == C14370lK.A0G || r2 == C14370lK.A0V || r2 == C14370lK.A0R || r2 == C14370lK.A0E || r2 == C14370lK.A0F || r2 == C14370lK.A06 || r2 == C14370lK.A0L || r2 == C14370lK.A07;
    }

    public synchronized void A01() {
        this.A03 = null;
        this.A02 = null;
        C16150oX r1 = this.A04.A02;
        AnonymousClass009.A05(r1);
        r1.A0M = false;
        this.A01 = true;
        this.A00 = true;
    }

    public synchronized void A02(byte[] bArr, int[] iArr) {
        if (iArr != null) {
            this.A03 = iArr;
        }
        if (bArr != null && bArr.length > 0) {
            AbstractC16130oV r1 = this.A04;
            if ((r1 instanceof AnonymousClass1X2) || this.A03 != null) {
                C16150oX r0 = r1.A02;
                AnonymousClass009.A05(r0);
                r0.A0M = true;
                this.A02 = bArr;
                this.A00 = true;
            }
        }
        C16150oX r12 = this.A04.A02;
        AnonymousClass009.A05(r12);
        r12.A0M = false;
        bArr = null;
        this.A02 = bArr;
        this.A00 = true;
    }

    public synchronized void A03(byte[] bArr, int[] iArr) {
        A02(bArr, iArr);
        this.A01 = true;
    }

    public synchronized boolean A04() {
        return this.A00;
    }

    public synchronized byte[] A05() {
        return this.A02;
    }

    public synchronized int[] A06() {
        return this.A03;
    }
}
