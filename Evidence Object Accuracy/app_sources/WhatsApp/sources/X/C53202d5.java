package X;

import android.content.Context;
import android.text.TextUtils;
import android.widget.LinearLayout;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import java.util.List;

/* renamed from: X.2d5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53202d5 extends LinearLayout implements AnonymousClass004 {
    public AnonymousClass018 A00;
    public AnonymousClass2P7 A01;
    public boolean A02;
    public boolean A03;
    public final int A04;
    public final int A05;
    public final TextEmojiLabel A06;
    public final TextEmojiLabel A07;
    public final TextEmojiLabel A08;

    public C53202d5(Context context) {
        super(context);
        if (!this.A03) {
            this.A03 = true;
            this.A00 = C12960it.A0R(AnonymousClass2P6.A00(generatedComponent()));
        }
        this.A02 = false;
        setOrientation(1);
        setGravity(16);
        LinearLayout.inflate(getContext(), R.layout.file_attachment_metadata, this);
        TextEmojiLabel A0T = C12970iu.A0T(this, R.id.file_attachment_metadata_title);
        this.A08 = A0T;
        this.A06 = C12970iu.A0T(this, R.id.file_attachment_metadata_description);
        this.A07 = C12970iu.A0T(this, R.id.file_attachment_metadata_subtext);
        this.A04 = AnonymousClass00T.A00(context, R.color.list_item_sub_title);
        this.A05 = AnonymousClass00T.A00(context, R.color.list_item_title);
        C27531Hw.A06(A0T);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A01;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A01 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.widget.LinearLayout, android.view.View
    public void onMeasure(int i, int i2) {
        this.A02 = true;
        TextEmojiLabel textEmojiLabel = this.A08;
        measureChild(textEmojiLabel, i, i2);
        setupTitleAndDescriptionMaxLines(C12980iv.A0q(textEmojiLabel));
        super.onMeasure(i, i2);
    }

    public void setSubText(String str, List list) {
        TextEmojiLabel textEmojiLabel = this.A07;
        textEmojiLabel.setVisibility(C13010iy.A00(TextUtils.isEmpty(str) ? 1 : 0));
        if (!TextUtils.isEmpty(str)) {
            textEmojiLabel.A0G(null, AnonymousClass3J9.A01(getContext(), this.A00, str, list));
        }
    }

    public void setTitleAndDescription(String str, String str2, List list) {
        TextEmojiLabel textEmojiLabel;
        int i;
        CharSequence charSequence;
        TextEmojiLabel textEmojiLabel2 = this.A06;
        boolean z = false;
        textEmojiLabel2.setVisibility(C13010iy.A00(TextUtils.isEmpty(str2) ? 1 : 0));
        if (this.A02 || this.A08.getMeasuredWidth() != 0) {
            z = true;
        }
        this.A02 = z;
        if (z) {
            setupTitleAndDescriptionMaxLines(str);
        }
        if (list == null || list.isEmpty()) {
            textEmojiLabel = this.A08;
            i = this.A05;
        } else {
            textEmojiLabel = this.A08;
            i = this.A04;
        }
        textEmojiLabel.setTextColor(i);
        Context context = getContext();
        AnonymousClass018 r2 = this.A00;
        textEmojiLabel.A0G(null, AnonymousClass3J9.A01(context, r2, str, list));
        if (str2 != null) {
            charSequence = AnonymousClass3J9.A01(getContext(), r2, str2, list);
        } else {
            charSequence = "";
        }
        textEmojiLabel2.A0G(null, charSequence);
    }

    private void setupTitleAndDescriptionMaxLines(String str) {
        TextEmojiLabel textEmojiLabel = this.A08;
        if (textEmojiLabel.getPaint().measureText(str) > ((float) textEmojiLabel.getMeasuredWidth())) {
            textEmojiLabel.setMaxLines(2);
            this.A06.setMaxLines(1);
            return;
        }
        textEmojiLabel.setMaxLines(1);
        this.A06.setMaxLines(2);
    }
}
