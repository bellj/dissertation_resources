package X;

import java.io.File;

/* renamed from: X.45G  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass45G extends AbstractC39731qS {
    public Boolean A00;
    public String A01;
    public final int A02;

    public AnonymousClass45G(File file, Boolean bool, String str, byte[] bArr, int i, boolean z) {
        super(file, null, bArr, z);
        this.A01 = str;
        this.A02 = i;
        this.A00 = bool;
    }
}
