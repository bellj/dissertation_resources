package X;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Build;
import android.text.TextUtils;
import java.util.WeakHashMap;

/* renamed from: X.3Ir  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C65223Ir {
    public static final EnumC630039m A00 = EnumC630039m.TEXT_START;
    public static final WeakHashMap A01 = new WeakHashMap();
    public static final TextUtils.TruncateAt[] A02 = TextUtils.TruncateAt.values();

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x001b, code lost:
        if (r1 != 8388613) goto L_0x0003;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.EnumC630039m A00(int r0, int r1) {
        /*
            switch(r0) {
                case 0: goto L_0x0006;
                case 1: goto L_0x0006;
                case 2: goto L_0x0021;
                case 3: goto L_0x001e;
                case 4: goto L_0x0030;
                case 5: goto L_0x0027;
                case 6: goto L_0x0024;
                default: goto L_0x0003;
            }
        L_0x0003:
            X.39m r0 = X.C65223Ir.A00
            return r0
        L_0x0006:
            r0 = 8388615(0x800007, float:1.1754953E-38)
            r1 = r1 & r0
            r0 = 1
            if (r1 == r0) goto L_0x0030
            r0 = 3
            if (r1 == r0) goto L_0x002d
            r0 = 5
            if (r1 == r0) goto L_0x002a
            r0 = 8388611(0x800003, float:1.1754948E-38)
            if (r1 == r0) goto L_0x0027
            r0 = 8388613(0x800005, float:1.175495E-38)
            if (r1 == r0) goto L_0x0024
            goto L_0x0003
        L_0x001e:
            X.39m r0 = X.EnumC630039m.TEXT_END
            return r0
        L_0x0021:
            X.39m r0 = X.EnumC630039m.TEXT_START
            return r0
        L_0x0024:
            X.39m r0 = X.EnumC630039m.LAYOUT_END
            return r0
        L_0x0027:
            X.39m r0 = X.EnumC630039m.LAYOUT_START
            return r0
        L_0x002a:
            X.39m r0 = X.EnumC630039m.RIGHT
            return r0
        L_0x002d:
            X.39m r0 = X.EnumC630039m.LEFT
            return r0
        L_0x0030:
            X.39m r0 = X.EnumC630039m.CENTER
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C65223Ir.A00(int, int):X.39m");
    }

    public static C71163cU A01(Context context) {
        C71163cU r4;
        TypedArray typedArray;
        TypedArray typedArray2;
        TypedArray typedArray3;
        TypedArray typedArray4;
        Resources.Theme theme = context.getTheme();
        WeakHashMap weakHashMap = A01;
        synchronized (weakHashMap) {
            r4 = (C71163cU) weakHashMap.get(theme);
        }
        if (r4 == null) {
            r4 = new C71163cU();
            C94614cC.A01("LoadTextStyle");
            Resources.Theme theme2 = context.getTheme();
            int i = Build.VERSION.SDK_INT;
            if (i <= 22) {
                synchronized (theme2) {
                    typedArray = context.obtainStyledAttributes(null, AnonymousClass4HM.A01, 0, 0);
                }
            } else {
                typedArray = context.obtainStyledAttributes(null, AnonymousClass4HM.A01, 0, 0);
            }
            int resourceId = typedArray.getResourceId(0, -1);
            typedArray.recycle();
            if (resourceId != -1) {
                if (i <= 22) {
                    synchronized (theme2) {
                        typedArray4 = theme2.obtainStyledAttributes(resourceId, AnonymousClass4HM.A00);
                    }
                } else {
                    typedArray4 = theme2.obtainStyledAttributes(resourceId, AnonymousClass4HM.A00);
                }
                A02(typedArray4, r4);
                typedArray4.recycle();
            }
            if (i <= 22) {
                synchronized (theme2) {
                    typedArray2 = context.obtainStyledAttributes(null, AnonymousClass4HM.A02, 0, 0);
                }
            } else {
                typedArray2 = context.obtainStyledAttributes(null, AnonymousClass4HM.A02, 0, 0);
            }
            int resourceId2 = typedArray2.getResourceId(0, -1);
            typedArray2.recycle();
            if (resourceId2 != -1) {
                if (i <= 22) {
                    synchronized (theme2) {
                        typedArray3 = theme2.obtainStyledAttributes(resourceId2, AnonymousClass4HM.A00);
                    }
                } else {
                    typedArray3 = theme2.obtainStyledAttributes(resourceId2, AnonymousClass4HM.A00);
                }
                A02(typedArray3, r4);
                typedArray3.recycle();
            }
            C94614cC.A00();
            synchronized (weakHashMap) {
                weakHashMap.put(theme, r4);
            }
        }
        return r4.A00();
    }

    public static void A02(TypedArray typedArray, C71163cU r10) {
        AnonymousClass4A9 r0;
        int indexCount = typedArray.getIndexCount();
        int i = 1;
        int i2 = 0;
        for (int i3 = 0; i3 < indexCount; i3++) {
            int index = typedArray.getIndex(i3);
            if (index == 2) {
                r10.A0P = typedArray.getColorStateList(index);
                r10.A0M = 0;
            } else if (index == 0) {
                r10.A0N = typedArray.getDimensionPixelSize(index, 0);
            } else if (index == 5) {
                int integer = typedArray.getInteger(index, 0);
                if (integer > 0) {
                    r10.A0R = A02[integer - 1];
                }
            } else if (index == 25) {
                if (Build.VERSION.SDK_INT >= 17) {
                    i = typedArray.getInt(index, -1);
                    r10.A0T = A00(i, i2);
                }
            } else if (index == 6) {
                i2 = typedArray.getInt(index, -1);
                r10.A0T = A00(i, i2);
                int i4 = i2 & 112;
                if (i4 == 16) {
                    r0 = AnonymousClass4A9.CENTER;
                } else if (i4 == 48 || i4 != 80) {
                    r0 = AnonymousClass4A9.TOP;
                } else {
                    r0 = AnonymousClass4A9.BOTTOM;
                }
                r10.A0U = r0;
            } else if (index == 15) {
                r10.A0W = typedArray.getBoolean(index, false);
            } else if (index == 11) {
                r10.A0J = typedArray.getInteger(index, -1);
            } else if (index == 10) {
                r10.A0G = typedArray.getInteger(index, -1);
            } else if (index == 14) {
                r10.A0X = typedArray.getBoolean(index, false);
            } else if (index == 4) {
                r10.A0C = typedArray.getColor(index, 0);
            } else if (index == 3) {
                r10.A08 = typedArray.getColor(index, 0);
            } else if (index == 1) {
                r10.A0O = typedArray.getInteger(index, 0);
            } else if (index == 20) {
                r10.A00 = (float) typedArray.getDimensionPixelOffset(index, 0);
            } else if (index == 21) {
                r10.A06 = typedArray.getFloat(index, 0.0f);
            } else if (index == 17) {
                r10.A03 = typedArray.getFloat(index, 0.0f);
            } else if (index == 18) {
                r10.A04 = typedArray.getFloat(index, 0.0f);
            } else if (index == 19) {
                r10.A05 = typedArray.getFloat(index, 0.0f);
            } else if (index == 16) {
                r10.A0L = typedArray.getColor(index, 0);
            } else if (index == 13) {
                r10.A0I = typedArray.getInteger(index, -1);
            } else if (index == 12) {
                r10.A0F = typedArray.getInteger(index, -1);
            } else if (index == 8) {
                r10.A0K = typedArray.getDimensionPixelSize(index, 0);
            } else if (index == 7) {
                r10.A0H = typedArray.getDimensionPixelSize(index, Integer.MAX_VALUE);
            } else if (index == 24) {
                String string = typedArray.getString(index);
                if (string != null) {
                    r10.A0Q = Typeface.create(string, 0);
                }
            } else if (index == 26) {
                if (Build.VERSION.SDK_INT >= 23) {
                    r10.A07 = typedArray.getInt(index, 0);
                }
            } else if (index == 27) {
                if (Build.VERSION.SDK_INT >= 23) {
                    r10.A0A = typedArray.getInt(index, 0);
                }
            } else if (index == 28 && Build.VERSION.SDK_INT >= 26) {
                r10.A0B = typedArray.getInt(index, 0);
            }
        }
    }
}
