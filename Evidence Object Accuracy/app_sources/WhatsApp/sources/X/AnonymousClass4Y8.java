package X;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.4Y8  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4Y8 {
    public final Map builderMap = C28371Mx.preservesInsertionOrderOnPutsMap();

    public AbstractC80953tE build() {
        return C81063tP.fromMapEntries(this.builderMap.entrySet(), null);
    }

    public Collection newMutableValueCollection() {
        return C12960it.A0l();
    }

    public AnonymousClass4Y8 put(Object obj, Object obj2) {
        C28251Mi.checkEntryNotNull(obj, obj2);
        Collection collection = (Collection) this.builderMap.get(obj);
        if (collection == null) {
            Map map = this.builderMap;
            collection = newMutableValueCollection();
            map.put(obj, collection);
        }
        collection.add(obj2);
        return this;
    }

    public AnonymousClass4Y8 putAll(Object obj, Iterable iterable) {
        if (obj == null) {
            throw C12980iv.A0n(C72453ed.A0r("null key in entry: null=", AnonymousClass3JP.toString(iterable)));
        }
        Collection collection = (Collection) this.builderMap.get(obj);
        Iterator it = iterable.iterator();
        if (collection != null) {
            while (it.hasNext()) {
                Object next = it.next();
                C28251Mi.checkEntryNotNull(obj, next);
                collection.add(next);
            }
        } else if (it.hasNext()) {
            Collection newMutableValueCollection = newMutableValueCollection();
            while (it.hasNext()) {
                Object next2 = it.next();
                C28251Mi.checkEntryNotNull(obj, next2);
                newMutableValueCollection.add(next2);
            }
            this.builderMap.put(obj, newMutableValueCollection);
            return this;
        }
        return this;
    }

    public AnonymousClass4Y8 putAll(Object obj, Object... objArr) {
        putAll(obj, Arrays.asList(objArr));
        return this;
    }
}
