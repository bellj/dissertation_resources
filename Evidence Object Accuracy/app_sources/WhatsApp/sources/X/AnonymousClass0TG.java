package X;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.view.ContextThemeWrapper;
import com.facebook.common.build.BuildConstants;
import com.whatsapp.R;

/* renamed from: X.0TG  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0TG {
    public static int A00(Context context, EnumC03700Iu r5, C14260l7 r6) {
        TypedArray typedArray;
        int i;
        if (context != null) {
            try {
                typedArray = null;
                try {
                    typedArray = A01(context, r6).obtainStyledAttributes(new int[]{r5.attr});
                    i = typedArray.getColor(0, r5.lightModeFallBackColorInt);
                } catch (Resources.NotFoundException e) {
                    if (!BuildConstants.isDebugBuild()) {
                        i = r5.lightModeFallBackColorInt;
                        if (typedArray == null) {
                            return i;
                        }
                    } else {
                        throw e;
                    }
                }
                typedArray.recycle();
                return i;
            } catch (Throwable th) {
                if (typedArray != null) {
                    typedArray.recycle();
                }
                throw th;
            }
        } else if (!BuildConstants.isDebugBuild()) {
            return r5.lightModeFallBackColorInt;
        } else {
            throw new NullPointerException("Context is null. This exception is only thrown in debug. Please fix context");
        }
    }

    public static Context A01(Context context, C14260l7 r3) {
        boolean A02 = A02(context, r3);
        int i = R.style.CDSLightMode;
        if (A02) {
            i = R.style.CDSDarkMode;
        }
        return new ContextThemeWrapper(context, i);
    }

    public static boolean A02(Context context, C14260l7 r1) {
        return r1 != null ? r1.A03() : AnonymousClass0LW.A00(context);
    }
}
