package X;

import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import com.facebook.redex.IDxCListenerShape8S0100000_1_I1;
import com.facebook.redex.IDxCListenerShape9S0100000_2_I1;
import com.facebook.redex.RunnableBRunnable0Shape10S0100000_I0_10;
import com.facebook.redex.RunnableBRunnable0Shape16S0100000_I1_2;
import com.whatsapp.MessageDialogFragment;
import com.whatsapp.R;
import com.whatsapp.qrcode.DevicePairQrScannerActivity;
import com.whatsapp.util.Log;

/* renamed from: X.3Zg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69443Zg implements AnonymousClass27W {
    public final /* synthetic */ DevicePairQrScannerActivity A00;

    public C69443Zg(DevicePairQrScannerActivity devicePairQrScannerActivity) {
        this.A00 = devicePairQrScannerActivity;
    }

    public final void A00() {
        DevicePairQrScannerActivity devicePairQrScannerActivity = this.A00;
        if (devicePairQrScannerActivity.AJN()) {
            return;
        }
        if (devicePairQrScannerActivity.A0B.A0L.A03() || C65303Iz.A04(((ActivityC13810kN) devicePairQrScannerActivity).A06)) {
            C004802e A0S = C12980iv.A0S(devicePairQrScannerActivity);
            A0S.A07(R.string.device_linking_failed_title);
            C12970iu.A1I(A0S);
            A0S.A04(new DialogInterface.OnDismissListener() { // from class: X.4hG
                @Override // android.content.DialogInterface.OnDismissListener
                public final void onDismiss(DialogInterface dialogInterface) {
                    C69443Zg.this.A00.finish();
                }
            });
            A0S.A06(R.string.device_linking_failed_message);
            A0S.A05();
            return;
        }
        ((ActivityC13810kN) devicePairQrScannerActivity).A05.A0E(C12960it.A0X(devicePairQrScannerActivity, "web.whatsapp.com", new Object[1], 0, R.string.invalid_qr_code), 1);
        devicePairQrScannerActivity.finish();
    }

    @Override // X.AnonymousClass27W
    public void AP6() {
        Log.i("QrScannerActivity/onDevicePairingRequested");
        DevicePairQrScannerActivity devicePairQrScannerActivity = this.A00;
        if (devicePairQrScannerActivity.A0B.A0L.A03()) {
            devicePairQrScannerActivity.A2C(R.string.logging_in_device);
        } else {
            devicePairQrScannerActivity.A1g(true);
        }
        Runnable runnable = devicePairQrScannerActivity.A0M;
        if (runnable != null) {
            ((ActivityC13810kN) devicePairQrScannerActivity).A00.removeCallbacks(runnable);
        }
        View view = ((ActivityC13810kN) devicePairQrScannerActivity).A00;
        Runnable runnable2 = devicePairQrScannerActivity.A0M;
        if (runnable2 == null) {
            runnable2 = new RunnableBRunnable0Shape10S0100000_I0_10(devicePairQrScannerActivity, 2);
            devicePairQrScannerActivity.A0M = runnable2;
        }
        view.postDelayed(runnable2, DevicePairQrScannerActivity.A0R);
    }

    @Override // X.AnonymousClass27W
    public void APl(int i) {
        DevicePairQrScannerActivity devicePairQrScannerActivity = this.A00;
        devicePairQrScannerActivity.A05.AKe(2, (long) i);
        devicePairQrScannerActivity.A2i();
        if (i != 403) {
            if (i == 419) {
                ((ActivityC13810kN) devicePairQrScannerActivity).A05.A07(R.string.error_message_max_device_paired, 1);
                devicePairQrScannerActivity.finish();
                return;
            } else if (i != 450) {
                if (i != 452) {
                    A00();
                    return;
                } else if (!devicePairQrScannerActivity.AJN()) {
                    AnonymousClass2AC A01 = MessageDialogFragment.A01(new Object[0], R.string.linked_device_unsupported_primary_version);
                    A01.A02(new IDxCListenerShape8S0100000_1_I1(devicePairQrScannerActivity, 27), R.string.upgrade);
                    IDxCListenerShape9S0100000_2_I1 iDxCListenerShape9S0100000_2_I1 = new IDxCListenerShape9S0100000_2_I1(devicePairQrScannerActivity, 51);
                    A01.A04 = R.string.cancel;
                    A01.A07 = iDxCListenerShape9S0100000_2_I1;
                    A01.A00 = 1000;
                    C12960it.A16(A01.A01(), devicePairQrScannerActivity);
                    return;
                } else {
                    return;
                }
            }
        }
        ((AbstractActivityC41101su) devicePairQrScannerActivity).A03.Aab();
        ((ActivityC13810kN) devicePairQrScannerActivity).A05.A0J(devicePairQrScannerActivity.A0Q, DevicePairQrScannerActivity.A0S);
    }

    @Override // X.AnonymousClass27W
    public void ARS() {
        DevicePairQrScannerActivity devicePairQrScannerActivity = this.A00;
        devicePairQrScannerActivity.A05.AKd(1);
        devicePairQrScannerActivity.A2i();
        ((ActivityC13810kN) devicePairQrScannerActivity).A05.A07(R.string.companion_device_time_incorrect_error, 1);
    }

    @Override // X.AnonymousClass27W
    public void ART() {
        DevicePairQrScannerActivity devicePairQrScannerActivity = this.A00;
        devicePairQrScannerActivity.A2i();
        if (devicePairQrScannerActivity.A0B.A0L.A03() || C65303Iz.A04(((ActivityC13810kN) devicePairQrScannerActivity).A06)) {
            C004802e A0S = C12980iv.A0S(devicePairQrScannerActivity);
            A0S.A07(R.string.invalid_qr_code_title);
            C12970iu.A1I(A0S);
            A0S.A04(new DialogInterface.OnDismissListener() { // from class: X.4hF
                @Override // android.content.DialogInterface.OnDismissListener
                public final void onDismiss(DialogInterface dialogInterface) {
                    ((AbstractActivityC41101su) C69443Zg.this.A00).A03.Aab();
                }
            });
            A0S.A06(R.string.invalid_qr_code_description);
            A0S.A05();
            return;
        }
        ((ActivityC13810kN) devicePairQrScannerActivity).A05.A0E(C12960it.A0X(devicePairQrScannerActivity, "web.whatsapp.com", new Object[1], 0, R.string.invalid_qr_code), 0);
        ((AbstractActivityC41101su) devicePairQrScannerActivity).A03.postDelayed(new RunnableBRunnable0Shape16S0100000_I1_2(devicePairQrScannerActivity, 5), 3000);
    }

    @Override // X.AnonymousClass27W
    public void AUt() {
        DevicePairQrScannerActivity devicePairQrScannerActivity = this.A00;
        Intent A0A = C12970iu.A0A();
        A0A.putExtra("has_removed_all_devices", true);
        devicePairQrScannerActivity.setResult(-1, A0A);
    }

    @Override // X.AnonymousClass27W
    public void AWu() {
        DevicePairQrScannerActivity devicePairQrScannerActivity = this.A00;
        if (devicePairQrScannerActivity.A0H.A00().ACX() == null) {
            devicePairQrScannerActivity.A2j();
        }
    }

    @Override // X.AnonymousClass27W
    public void AXK() {
        DevicePairQrScannerActivity devicePairQrScannerActivity = this.A00;
        devicePairQrScannerActivity.A05.AKd(1);
        devicePairQrScannerActivity.A2i();
        A00();
    }
}
