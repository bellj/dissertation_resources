package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.2nX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57732nX extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57732nX A06;
    public static volatile AnonymousClass255 A07;
    public int A00;
    public int A01 = 0;
    public AnonymousClass1K6 A02 = AnonymousClass277.A01;
    public AnonymousClass229 A03;
    public AnonymousClass229 A04;
    public Object A05;

    static {
        C57732nX r0 = new C57732nX();
        A06 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x006e, code lost:
        if (r9.A01 == 1) goto L_0x0084;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0073, code lost:
        if (r9.A01 == 2) goto L_0x0084;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0078, code lost:
        if (r9.A01 == 3) goto L_0x0084;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x007d, code lost:
        if (r9.A01 == 4) goto L_0x0084;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0082, code lost:
        if (r9.A01 == 5) goto L_0x0084;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0084, code lost:
        r7 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0085, code lost:
        r9.A05 = r11.Afv(r9.A05, r12.A05, r7);
     */
    @Override // X.AbstractC27091Fz
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object A0V(X.AnonymousClass25B r10, java.lang.Object r11, java.lang.Object r12) {
        /*
        // Method dump skipped, instructions count: 594
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C57732nX.A0V(X.25B, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    public AnonymousClass39r A0b() {
        int i = this.A01;
        if (i == 0) {
            return AnonymousClass39r.A05;
        }
        if (i == 1) {
            return AnonymousClass39r.A01;
        }
        if (i == 2) {
            return AnonymousClass39r.A02;
        }
        if (i == 3) {
            return AnonymousClass39r.A03;
        }
        if (i == 4) {
            return AnonymousClass39r.A06;
        }
        if (i != 5) {
            return null;
        }
        return AnonymousClass39r.A04;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i;
        int i2 = ((AbstractC27091Fz) this).A00;
        if (i2 != -1) {
            return i2;
        }
        if (this.A01 == 1) {
            i = CodedOutputStream.A0A((AnonymousClass1G0) this.A05, 1) + 0;
        } else {
            i = 0;
        }
        if (this.A01 == 2) {
            i = AbstractC27091Fz.A08((AnonymousClass1G0) this.A05, 2, i);
        }
        if (this.A01 == 3) {
            i = AbstractC27091Fz.A08((AnonymousClass1G0) this.A05, 3, i);
        }
        if (this.A01 == 4) {
            i = AbstractC27091Fz.A08((AnonymousClass1G0) this.A05, 4, i);
        }
        if (this.A01 == 5) {
            i = AbstractC27091Fz.A08((AnonymousClass1G0) this.A05, 5, i);
        }
        if ((this.A00 & 32) == 32) {
            AnonymousClass229 r0 = this.A03;
            if (r0 == null) {
                r0 = AnonymousClass229.A0A;
            }
            i = AbstractC27091Fz.A08(r0, 6, i);
        }
        if ((this.A00 & 64) == 64) {
            AnonymousClass229 r02 = this.A04;
            if (r02 == null) {
                r02 = AnonymousClass229.A0A;
            }
            i = AbstractC27091Fz.A08(r02, 7, i);
        }
        for (int i3 = 0; i3 < this.A02.size(); i3++) {
            i = AbstractC27091Fz.A08((AnonymousClass1G1) this.A02.get(i3), 8, i);
        }
        return AbstractC27091Fz.A07(this, i);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if (this.A01 == 1) {
            AbstractC27091Fz.A0O(codedOutputStream, this.A05, 1);
        }
        if (this.A01 == 2) {
            AbstractC27091Fz.A0O(codedOutputStream, this.A05, 2);
        }
        if (this.A01 == 3) {
            AbstractC27091Fz.A0O(codedOutputStream, this.A05, 3);
        }
        if (this.A01 == 4) {
            AbstractC27091Fz.A0O(codedOutputStream, this.A05, 4);
        }
        if (this.A01 == 5) {
            AbstractC27091Fz.A0O(codedOutputStream, this.A05, 5);
        }
        if ((this.A00 & 32) == 32) {
            AnonymousClass229 r0 = this.A03;
            if (r0 == null) {
                r0 = AnonymousClass229.A0A;
            }
            codedOutputStream.A0L(r0, 6);
        }
        if ((this.A00 & 64) == 64) {
            AnonymousClass229 r02 = this.A04;
            if (r02 == null) {
                r02 = AnonymousClass229.A0A;
            }
            codedOutputStream.A0L(r02, 7);
        }
        int i = 0;
        while (i < this.A02.size()) {
            i = AbstractC27091Fz.A06(codedOutputStream, this.A02, i, 8);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
