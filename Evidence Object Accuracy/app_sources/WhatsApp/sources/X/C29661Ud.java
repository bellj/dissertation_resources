package X;

import androidx.fragment.app.DialogFragment;
import com.whatsapp.dialogs.ProgressDialogFragment;

/* renamed from: X.1Ud  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C29661Ud {
    public static boolean A02;
    public static final String A03 = ProgressDialogFragment.class.getName();
    public DialogFragment A00;
    public final ActivityC13810kN A01;

    public C29661Ud(ActivityC13810kN r1) {
        this.A01 = r1;
    }
}
