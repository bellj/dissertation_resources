package X;

/* renamed from: X.0SV  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0SV {
    public static final AnonymousClass0SV A02 = new AnonymousClass0SV(EnumC03780Jc.xMidYMid, AnonymousClass0JA.meet);
    public static final AnonymousClass0SV A03 = new AnonymousClass0SV(EnumC03780Jc.none, null);
    public EnumC03780Jc A00;
    public AnonymousClass0JA A01;

    public AnonymousClass0SV(EnumC03780Jc r1, AnonymousClass0JA r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass0SV r5 = (AnonymousClass0SV) obj;
            if (!(this.A00 == r5.A00 && this.A01 == r5.A01)) {
                return false;
            }
        }
        return true;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.A00);
        sb.append(" ");
        sb.append(this.A01);
        return sb.toString();
    }
}
