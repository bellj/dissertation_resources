package X;

import android.animation.ObjectAnimator;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;

/* renamed from: X.0C9  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0C9 extends AbstractC05490Pt {
    public final ObjectAnimator A00;
    public final boolean A01;

    public AnonymousClass0C9(AnimationDrawable animationDrawable, boolean z, boolean z2) {
        int numberOfFrames = animationDrawable.getNumberOfFrames();
        int i = z ? numberOfFrames - 1 : 0;
        int i2 = z ? 0 : numberOfFrames - 1;
        C06610Ui r3 = new C06610Ui(animationDrawable, z);
        ObjectAnimator ofInt = ObjectAnimator.ofInt(animationDrawable, "currentIndex", i, i2);
        if (Build.VERSION.SDK_INT >= 18) {
            ofInt.setAutoCancel(true);
        }
        ofInt.setDuration((long) r3.A01);
        ofInt.setInterpolator(r3);
        this.A01 = z2;
        this.A00 = ofInt;
    }

    @Override // X.AbstractC05490Pt
    public void A00() {
        this.A00.reverse();
    }

    @Override // X.AbstractC05490Pt
    public void A01() {
        this.A00.start();
    }

    @Override // X.AbstractC05490Pt
    public void A02() {
        this.A00.cancel();
    }

    @Override // X.AbstractC05490Pt
    public boolean A03() {
        return this.A01;
    }
}
