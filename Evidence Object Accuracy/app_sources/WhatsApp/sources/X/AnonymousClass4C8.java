package X;

/* renamed from: X.4C8  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4C8 extends Exception {
    public EnumC87074Ac errorType;
    public String message;

    public AnonymousClass4C8(EnumC87074Ac r1, String str) {
        super(str);
        this.message = str;
        this.errorType = r1;
    }

    @Override // java.lang.Throwable, java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("Error type: ");
        A0k.append(this.errorType);
        A0k.append(". ");
        return C12960it.A0d(this.message, A0k);
    }
}
