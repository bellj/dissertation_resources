package X;

import android.view.View;

/* renamed from: X.5aq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117865aq extends AnonymousClass0Y9 {
    public final /* synthetic */ C117795ag A00;
    public final /* synthetic */ boolean A01;

    public C117865aq(C117795ag r1, boolean z) {
        this.A00 = r1;
        this.A01 = z;
    }

    @Override // X.AnonymousClass0Y9, X.AbstractC12530i4
    public void AMC(View view) {
        view.setVisibility(C12960it.A02(this.A01 ? 1 : 0));
    }
}
