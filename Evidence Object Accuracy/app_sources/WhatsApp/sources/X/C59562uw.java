package X;

/* renamed from: X.2uw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59562uw extends C37171lc {
    public C48122Ek A00;
    public AbstractView$OnClickListenerC34281fs A01;

    public C59562uw(C48122Ek r2, AbstractView$OnClickListenerC34281fs r3) {
        super(AnonymousClass39o.A0Y);
        this.A00 = r2;
        this.A01 = r3;
    }

    @Override // X.C37171lc
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
            return false;
        }
        return this.A00.equals(((C59562uw) obj).A00);
    }

    @Override // X.C37171lc
    public int hashCode() {
        return this.A00.hashCode();
    }
}
