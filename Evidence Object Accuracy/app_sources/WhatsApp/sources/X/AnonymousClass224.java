package X;

import android.view.ViewTreeObserver;
import com.facebook.redex.RunnableBRunnable0Shape7S0100000_I0_7;
import com.whatsapp.location.WaMapView;

/* renamed from: X.224  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass224 implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ C244415n A00;
    public final /* synthetic */ WaMapView A01;

    public AnonymousClass224(C244415n r1, WaMapView waMapView) {
        this.A01 = waMapView;
        this.A00 = r1;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        WaMapView waMapView = this.A01;
        AnonymousClass226 r0 = waMapView.A01;
        AnonymousClass009.A03(r0);
        r0.getViewTreeObserver().removeOnPreDrawListener(this);
        waMapView.A01.post(new RunnableBRunnable0Shape7S0100000_I0_7(this, 42));
        return true;
    }
}
