package X;

import java.util.Arrays;

/* renamed from: X.5NQ  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5NQ extends AnonymousClass1TL implements AnonymousClass5VP {
    public final byte[] A00;

    public AnonymousClass5NQ(byte[] bArr) {
        this.A00 = bArr;
    }

    @Override // X.AnonymousClass1TL
    public void A08(AnonymousClass1TP r3, boolean z) {
        r3.A06(this.A00, 26, z);
    }

    @Override // X.AnonymousClass1TL
    public boolean A09() {
        return false;
    }

    @Override // X.AnonymousClass5VP
    public String AGy() {
        return AnonymousClass1T7.A02(this.A00);
    }

    @Override // X.AnonymousClass1TL, X.AnonymousClass1TM
    public int hashCode() {
        return AnonymousClass1TT.A00(this.A00);
    }

    @Override // X.AnonymousClass1TL
    public int A05() {
        return C72453ed.A0H(this.A00);
    }

    @Override // X.AnonymousClass1TL
    public boolean A0A(AnonymousClass1TL r3) {
        if (!(r3 instanceof AnonymousClass5NQ)) {
            return false;
        }
        return Arrays.equals(this.A00, ((AnonymousClass5NQ) r3).A00);
    }

    public String toString() {
        return AnonymousClass1T7.A02(this.A00);
    }
}
