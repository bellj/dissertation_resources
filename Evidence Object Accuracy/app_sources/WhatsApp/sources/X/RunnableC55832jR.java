package X;

import android.util.Log;
import com.facebook.redex.EmptyBaseRunnable0;
import com.google.android.gms.common.api.Status;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/* renamed from: X.2jR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class RunnableC55832jR extends EmptyBaseRunnable0 implements Runnable {
    public static final C63703Cp A02 = new C63703Cp("RevokeAccessOperation", new String[0]);
    public final C77773np A00 = new C77773np(null);
    public final String A01;

    public RunnableC55832jR(String str) {
        C13020j0.A05(str);
        this.A01 = str;
    }

    @Override // java.lang.Runnable
    public final void run() {
        C63703Cp r3;
        String str;
        String str2;
        Status status = Status.A07;
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(C12960it.A0c(String.valueOf(this.A01), "https://accounts.google.com/o/oauth2/revoke?token=")).openConnection();
            httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode == 200) {
                status = Status.A09;
            } else {
                Log.e("RevokeAccessOperation", A02.A03.concat("Unable to revoke access!"));
            }
            A02.A00(C12960it.A0e("Response Code: ", C12980iv.A0t(26), responseCode), new Object[0]);
        } catch (IOException e) {
            r3 = A02;
            str = String.valueOf(e.toString());
            str2 = "IOException when revoking access: ";
            Log.e("RevokeAccessOperation", r3.A03.concat(C12960it.A0c(str, str2)));
            this.A00.A05(status);
        } catch (Exception e2) {
            r3 = A02;
            str = String.valueOf(e2.toString());
            str2 = "Exception when revoking access: ";
            Log.e("RevokeAccessOperation", r3.A03.concat(C12960it.A0c(str, str2)));
            this.A00.A05(status);
        }
        this.A00.A05(status);
    }
}
