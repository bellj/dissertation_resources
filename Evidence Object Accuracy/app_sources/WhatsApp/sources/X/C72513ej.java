package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;

/* renamed from: X.3ej  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C72513ej extends AnimatorListenerAdapter {
    public final /* synthetic */ AnonymousClass3YM A00;

    public C72513ej(AnonymousClass3YM r1) {
        this.A00 = r1;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        super.onAnimationEnd(animator);
        ValueAnimator valueAnimator = this.A00.A01;
        valueAnimator.removeAllListeners();
        valueAnimator.removeAllUpdateListeners();
    }
}
