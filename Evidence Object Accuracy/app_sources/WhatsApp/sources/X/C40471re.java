package X;

/* renamed from: X.1re  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C40471re {
    public static String A00 = A01((byte) 68);
    public static String A01 = A01((byte) 56);
    public static final String A02;
    public static final String A03;

    static {
        StringBuilder sb = new StringBuilder("SELECT ");
        sb.append(AnonymousClass1Ux.A01("message_add_on", AnonymousClass4HC.A00));
        sb.append(" FROM ");
        sb.append("message_add_on");
        sb.append(" WHERE ");
        sb.append("message_add_on.chat_row_id");
        sb.append(" = ? AND ");
        sb.append("message_add_on.key_id");
        sb.append(" = ? AND ");
        sb.append("message_add_on.from_me");
        sb.append(" = ? ");
        A03 = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("SELECT ");
        String str = AnonymousClass2RI.A00;
        sb2.append(AnonymousClass1Ux.A01("message_add_on_orphan", str));
        sb2.append(" FROM ");
        sb2.append("message_add_on_orphan");
        A02 = sb2.toString();
        AnonymousClass1Ux.A01("message_add_on_orphan", str);
    }

    public static String A00(byte b) {
        String A012;
        String str;
        String str2 = "message_add_on_keep_in_chat";
        if (b == 56) {
            A012 = AnonymousClass1Ux.A01("message_add_on_reaction", AnonymousClass4HB.A00);
            str = "message_add_on_reaction.message_add_on_row_id";
            str2 = "message_add_on_reaction";
        } else if (b == 67) {
            A012 = AnonymousClass1Ux.A01("message_add_on_poll_vote", AnonymousClass4HA.A00);
            str = "message_add_on_poll_vote.message_add_on_row_id";
            str2 = "message_add_on_poll_vote";
        } else if (b == 68) {
            A012 = AnonymousClass1Ux.A01(str2, AnonymousClass4H9.A00);
            str = "message_add_on_keep_in_chat.message_add_on_row_id";
        } else {
            throw new IllegalArgumentException("Not supported type.");
        }
        StringBuilder sb = new StringBuilder("SELECT ");
        sb.append(AnonymousClass1Ux.A01("message_add_on", AnonymousClass4HC.A00));
        sb.append(",");
        sb.append(A012);
        sb.append(" FROM ");
        sb.append("message_add_on");
        sb.append(" LEFT JOIN ");
        sb.append(str2);
        sb.append(" ON ");
        sb.append("message_add_on._id = ");
        sb.append(str);
        sb.append(" ");
        return sb.toString();
    }

    public static String A01(byte b) {
        String str = "SELECT COUNT(message_add_on._id) as unread_count, MAX(message_add_on._id) as last_message_add_on_row_id, message_add_on.parent_message_row_id as parent_message_row_id FROM message_add_on";
        if (b == 68) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(" LEFT JOIN ");
            sb.append("message_add_on_keep_in_chat");
            sb.append(" ON ");
            sb.append("message_add_on._id=message_add_on_keep_in_chat.message_add_on_row_id");
            str = sb.toString();
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append(str);
        sb2.append(" WHERE ");
        sb2.append("message_add_on.chat_row_id = ?");
        sb2.append(" AND ");
        sb2.append("message_add_on.message_add_on_type = ");
        sb2.append((int) b);
        sb2.append(" AND ");
        sb2.append("message_add_on.status = ?");
        sb2.append(" AND ");
        sb2.append("message_add_on.from_me = 0");
        sb2.append(" AND ");
        sb2.append("message_add_on._id > ?");
        String obj = sb2.toString();
        if (b == 68) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append(obj);
            sb3.append(" AND ");
            sb3.append("message_add_on_keep_in_chat.keep_in_chat_state=1");
            sb3.append(" AND ");
            sb3.append("message_add_on_keep_in_chat.keep_count <= 1");
            obj = sb3.toString();
        }
        StringBuilder sb4 = new StringBuilder();
        sb4.append(obj);
        sb4.append(" GROUP BY ");
        sb4.append("message_add_on.parent_message_row_id");
        sb4.append(" ORDER BY ");
        sb4.append("last_message_add_on_row_id");
        sb4.append(" DESC LIMIT ?");
        return sb4.toString();
    }

    public static String A02(byte b, int i) {
        StringBuilder sb = new StringBuilder("SELECT  DISTINCT (message_add_on.sender_jid_row_id), message_add_on.parent_message_row_id FROM message_add_on WHERE message_add_on.chat_row_id = ? AND message_add_on.message_add_on_type = ");
        sb.append((int) b);
        sb.append(" AND ");
        sb.append("message_add_on.status = ?");
        sb.append(" AND ");
        sb.append("message_add_on.from_me = 0");
        sb.append(" AND ");
        sb.append("message_add_on._id > ?");
        sb.append(" AND ");
        sb.append("message_add_on.parent_message_row_id IN ");
        sb.append(AnonymousClass1Ux.A00(i));
        return sb.toString();
    }
}
