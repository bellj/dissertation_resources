package X;

import java.io.IOException;
import java.util.Iterator;

/* renamed from: X.5Bg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C111905Bg implements Iterable {
    public final /* synthetic */ AnonymousClass4PP A00;
    public final /* synthetic */ CharSequence A01;

    public C111905Bg(AnonymousClass4PP r1, CharSequence charSequence) {
        this.A00 = r1;
        this.A01 = charSequence;
    }

    @Override // java.lang.Iterable
    public final Iterator iterator() {
        AnonymousClass4PP r3 = this.A00;
        return new C79593qq(r3.A01, r3, this.A01);
    }

    @Override // java.lang.Object
    public final String toString() {
        CharSequence charSequence;
        StringBuilder A0k = C12960it.A0k("[");
        Iterator it = iterator();
        try {
            if (it.hasNext()) {
                Object next = it.next();
                if (next instanceof CharSequence) {
                    charSequence = (CharSequence) next;
                } else {
                    charSequence = next.toString();
                }
                while (true) {
                    A0k.append(charSequence);
                    if (!it.hasNext()) {
                        break;
                    }
                    A0k.append((CharSequence) ", ");
                    Object next2 = it.next();
                    charSequence = next2 instanceof CharSequence ? (CharSequence) next2 : next2.toString();
                }
            }
            return C72453ed.A0t(A0k);
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }
}
