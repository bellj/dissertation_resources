package X;

import android.util.Base64;
import com.whatsapp.util.Log;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5xx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129585xx {
    public final C130125yq A00;
    public final String A01;
    public final JSONObject A02;

    public C129585xx(C130125yq r1, String str, JSONObject jSONObject) {
        this.A00 = r1;
        this.A01 = str;
        this.A02 = jSONObject;
    }

    public static String A00(C130125yq r0, C129585xx r1) {
        KeyPair A02 = r0.A02();
        AnonymousClass009.A05(A02);
        String A01 = r1.A01(A02);
        AnonymousClass009.A05(A01);
        return A01;
    }

    public String A01(KeyPair keyPair) {
        byte[] bArr;
        JSONObject A02 = A02();
        if (A02 == null) {
            return null;
        }
        AnonymousClass61O r3 = new AnonymousClass61O(Base64.encodeToString(AnonymousClass61L.A03(keyPair.getPublic().getEncoded()), 10), A02);
        PrivateKey privateKey = keyPair.getPrivate();
        byte[] A04 = r3.A04();
        try {
            Signature instance = Signature.getInstance("SHA256withECDSA");
            instance.initSign(privateKey);
            instance.update(A04);
            bArr = instance.sign();
        } catch (InvalidKeyException | NoSuchAlgorithmException | SignatureException unused) {
            Log.e("PAY: NoviEncryptionManager/sign exception");
            bArr = null;
        }
        AnonymousClass009.A06(bArr, "PAY: NoviEncryptionManager/buildJwtPayload null signature");
        r3.A01 = Base64.encodeToString(AnonymousClass61O.A01(bArr), 11);
        return r3.A02();
    }

    public JSONObject A02() {
        JSONObject A0a = C117295Zj.A0a();
        try {
            A0a.put("topic", this.A01);
            A0a.put("jws_api_version", 1);
            A0a.put("payload_version", 1);
            JSONObject jSONObject = this.A02;
            AnonymousClass009.A05(jSONObject);
            A0a.put("payload_b64", C117305Zk.A0n(jSONObject.toString().getBytes()));
            return A0a;
        } catch (JSONException unused) {
            Log.e("PAY: NoviSignedIntent/toJson can't construct json");
            return null;
        }
    }
}
