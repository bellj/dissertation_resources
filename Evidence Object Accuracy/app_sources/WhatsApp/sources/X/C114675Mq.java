package X;

import java.math.BigInteger;
import java.util.Hashtable;

/* renamed from: X.5Mq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114675Mq extends AnonymousClass1TM {
    public static final Hashtable A01 = new Hashtable();
    public static final String[] A02 = C72453ed.A1b();
    public AnonymousClass5ND A00;

    public C114675Mq(int i) {
        this.A00 = new AnonymousClass5ND(i);
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        return this.A00;
    }

    public static C114675Mq A00(Object obj) {
        if (!(obj instanceof C114675Mq)) {
            if (obj == null) {
                return null;
            }
            int A0B = AnonymousClass5ND.A00(obj).A0B();
            Integer valueOf = Integer.valueOf(A0B);
            Hashtable hashtable = A01;
            if (!hashtable.containsKey(valueOf)) {
                hashtable.put(valueOf, new C114675Mq(A0B));
            }
            obj = hashtable.get(valueOf);
        }
        return (C114675Mq) obj;
    }

    public String toString() {
        String str;
        int intValue = new BigInteger(this.A00.A01).intValue();
        if (intValue < 0 || intValue > 10) {
            str = "invalid";
        } else {
            str = A02[intValue];
        }
        return C12960it.A0d(str, C12960it.A0k("CRLReason: "));
    }
}
