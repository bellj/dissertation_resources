package X;

import android.os.Handler;
import android.os.Looper;
import com.facebook.redex.RunnableBRunnable0Shape7S0100000_I0_7;
import java.util.ArrayList;

/* renamed from: X.0ys  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22310ys {
    public boolean A00;
    public final Handler A01 = new Handler(Looper.getMainLooper());
    public final C18640sm A02;
    public final C14830m7 A03;
    public final C14850m9 A04;
    public final AnonymousClass12J A05;
    public final C22370yy A06;
    public final AnonymousClass12K A07;
    public final AbstractC14440lR A08;
    public final C21260x8 A09;
    public final C21280xA A0A;
    public final Runnable A0B;
    public final ArrayList A0C = new ArrayList();

    public C22310ys(C18640sm r3, C14830m7 r4, C14850m9 r5, AnonymousClass12J r6, C22370yy r7, AnonymousClass12K r8, AbstractC14440lR r9, C21260x8 r10, C21280xA r11) {
        this.A03 = r4;
        this.A04 = r5;
        this.A08 = r9;
        this.A09 = r10;
        this.A0A = r11;
        this.A05 = r6;
        this.A06 = r7;
        this.A07 = r8;
        this.A02 = r3;
        this.A0B = new RunnableBRunnable0Shape7S0100000_I0_7(this, 46);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00ca, code lost:
        if (r2 != false) goto L_0x00c7;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(X.AbstractC28771Oy r23, X.AbstractC16130oV r24, int r25, boolean r26) {
        /*
        // Method dump skipped, instructions count: 238
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22310ys.A00(X.1Oy, X.0oV, int, boolean):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0050, code lost:
        if (r6 >= r2) goto L_0x0052;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00b2, code lost:
        if (X.AnonymousClass14P.A02(X.C14370lK.A01(r15.A0y, ((X.AbstractC15340mz) r15).A08)) == false) goto L_0x00b4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0129, code lost:
        if ((r4.A04.A00() - r6.getLong("status_tab_last_opened_time", 0)) > 1209600000) goto L_0x0018;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x014f, code lost:
        if (r0.A0C != null) goto L_0x0153;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x0155, code lost:
        if (r6.A0C != null) goto L_0x0017;
     */
    /* JADX WARNING: Removed duplicated region for block: B:108:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x001c  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x005b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A01(X.AbstractC16130oV r15, int r16, boolean r17) {
        /*
        // Method dump skipped, instructions count: 390
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22310ys.A01(X.0oV, int, boolean):void");
    }
}
