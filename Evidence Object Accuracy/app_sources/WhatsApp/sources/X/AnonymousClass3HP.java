package X;

/* renamed from: X.3HP  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3HP {
    public final float A00;
    public final float A01;

    public AnonymousClass3HP(float f, float f2) {
        this.A00 = f;
        this.A01 = f2;
    }

    public static float A00(AnonymousClass3HP r4, AnonymousClass3HP r5) {
        float f = r4.A00;
        float f2 = r4.A01;
        float f3 = f - r5.A00;
        float f4 = f2 - r5.A01;
        return (float) Math.sqrt((double) ((f3 * f3) + (f4 * f4)));
    }

    public final boolean equals(Object obj) {
        if (obj instanceof AnonymousClass3HP) {
            AnonymousClass3HP r4 = (AnonymousClass3HP) obj;
            if (this.A00 == r4.A00 && this.A01 == r4.A01) {
                return true;
            }
        }
        return false;
    }

    public final int hashCode() {
        return (Float.floatToIntBits(this.A00) * 31) + Float.floatToIntBits(this.A01);
    }

    public final String toString() {
        StringBuilder A0k = C12960it.A0k("(");
        A0k.append(this.A00);
        A0k.append(',');
        A0k.append(this.A01);
        return C12970iu.A0u(A0k);
    }
}
