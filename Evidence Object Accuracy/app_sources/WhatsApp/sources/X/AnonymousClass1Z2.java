package X;

/* renamed from: X.1Z2  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1Z2 {
    public final C91304Rf A00;
    public final C91304Rf A01;
    public final boolean A02;

    public AnonymousClass1Z2(String str, boolean z) {
        this.A02 = z;
        int indexOf = str.indexOf(59);
        if (indexOf == -1) {
            C91304Rf r0 = new C91304Rf(str, z);
            this.A00 = r0;
            this.A01 = r0;
            return;
        }
        this.A01 = new C91304Rf(str.substring(0, indexOf), z);
        this.A00 = new C91304Rf(str.substring(indexOf + 1), z);
    }
}
