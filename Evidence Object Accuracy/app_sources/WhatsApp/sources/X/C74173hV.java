package X;

import android.content.Context;
import android.view.animation.Interpolator;
import android.widget.Scroller;

/* renamed from: X.3hV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74173hV extends Scroller {
    public float A00;

    public C74173hV(Context context, Interpolator interpolator) {
        super(context, interpolator);
    }

    @Override // android.widget.Scroller
    public void startScroll(int i, int i2, int i3, int i4, int i5) {
        float f = this.A00;
        if (f != 0.0f) {
            i5 = (int) (f * ((float) i5));
        }
        super.startScroll(i, i2, i3, i4, i5);
    }
}
