package X;

import android.content.Context;
import android.view.View;
import com.whatsapp.SuspiciousLinkWarningDialogFragment;
import java.util.Set;

/* renamed from: X.2p1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58412p1 extends C58272oQ {
    public final Set A00;

    public C58412p1(Context context, AnonymousClass12Q r2, C14900mE r3, AnonymousClass01d r4, String str, Set set) {
        super(context, r2, r3, r4, str);
        this.A00 = set;
    }

    @Override // X.C58272oQ, X.AbstractC116465Vn
    public void onClick(View view) {
        Set set = this.A00;
        if (set != null) {
            ((ActivityC13810kN) AnonymousClass12P.A02(view)).Adm(SuspiciousLinkWarningDialogFragment.A00(this.A09, set));
            return;
        }
        super.onClick(view);
    }
}
