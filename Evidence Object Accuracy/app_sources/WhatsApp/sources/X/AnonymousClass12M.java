package X;

import android.content.Context;
import android.net.Uri;
import com.facebook.redex.RunnableBRunnable0Shape13S0100000_I0_13;
import com.whatsapp.Me;
import com.whatsapp.R;
import com.whatsapp.usernotice.UserNoticeContentWorker;
import com.whatsapp.usernotice.UserNoticeIconWorker;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;

/* renamed from: X.12M  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass12M {
    public C43891xl A00;
    public final C15570nT A01;
    public final C16590pI A02;
    public final C17170qN A03;
    public final AnonymousClass018 A04;
    public final C14850m9 A05;
    public final AnonymousClass12R A06;
    public final AnonymousClass12S A07;
    public final AbstractC14440lR A08;
    public final C21710xr A09;

    public AnonymousClass12M(C15570nT r1, C16590pI r2, C17170qN r3, AnonymousClass018 r4, C14850m9 r5, AnonymousClass12R r6, AnonymousClass12S r7, AbstractC14440lR r8, C21710xr r9) {
        this.A05 = r5;
        this.A02 = r2;
        this.A01 = r1;
        this.A08 = r8;
        this.A04 = r4;
        this.A09 = r9;
        this.A07 = r7;
        this.A03 = r3;
        this.A06 = r6;
    }

    public static C43841xg A00(C14850m9 r10, boolean z) {
        C43861xi r4;
        int i = 358;
        if (z) {
            i = 357;
        }
        int A02 = r10.A02(i);
        if (A02 == 0) {
            StringBuilder sb = new StringBuilder("GreenAlertUtils/buildModal/dismissible: ");
            sb.append(z);
            sb.append(", no start time received");
            Log.i(sb.toString());
            return null;
        }
        if (!z) {
            A02 += r10.A02(365);
        }
        String str = null;
        if (z) {
            r4 = new C43861xi(new long[]{86400000}, -1);
        } else {
            r4 = null;
        }
        C43881xk r1 = new C43881xk(r4, new C43871xj(((long) A02) * 1000), null);
        ArrayList arrayList = new ArrayList();
        if (z) {
            str = "";
        }
        return new C43841xg(r1, "", "", "", "", "", null, null, str, arrayList);
    }

    public static final File A01(Context context, int i) {
        File A02 = A02(context.getFilesDir(), "user_notice");
        if (A02 == null) {
            return null;
        }
        return A02(A02, String.valueOf(i));
    }

    public static File A02(File file, String str) {
        File file2 = new File(file, str);
        if (file2.exists() || file2.mkdir()) {
            return file2;
        }
        StringBuilder sb = new StringBuilder("UserNoticeContentManager/getDir/could not make directory ");
        sb.append(file2.getAbsolutePath());
        Log.e(sb.toString());
        return null;
    }

    public C43891xl A03(C43831xf r13) {
        String str;
        int i = r13.A00;
        C14850m9 r6 = this.A05;
        C43921xq r7 = null;
        if (C43901xo.A00(r6, i)) {
            StringBuilder sb = new StringBuilder("UserNoticeContentManager/getUserNoticeContentFromLocal/green alert disabled, notice id: ");
            sb.append(i);
            str = sb.toString();
        } else if (C43901xo.A01(r6, r13)) {
            C16590pI r1 = this.A02;
            int A02 = r6.A02(356);
            if (A02 == 0) {
                Log.i("GreenAlertUtils/buildBanner/no duration received");
            } else {
                r7 = new C43921xq(new C43881xk(new C43861xi(null, ((long) A02) * 3600000), new C43871xj(1609459200000L), null), r1.A00.getString(R.string.green_alert_banner));
            }
            C43841xg A00 = A00(r6, true);
            C43841xg A002 = A00(r6, false);
            if (r7 == null || A00 == null || A002 == null) {
                return null;
            }
            return new C43891xl(r7, A00, A002, 1, 1);
        } else {
            int i2 = r13.A02;
            int i3 = r13.A01;
            StringBuilder sb2 = new StringBuilder("UserNoticeContentManager/getUserNoticeContentFromLocal/notice id: ");
            sb2.append(i);
            sb2.append(" version: ");
            sb2.append(i2);
            sb2.append(" stage: ");
            sb2.append(i3);
            Log.i(sb2.toString());
            if (i3 == 5) {
                str = "UserNoticeContentManager/getUserNoticeContentFromLocal/end stage, skip local content";
            } else {
                C43891xl r12 = this.A00;
                if (r12 != null && r12.A00 == i && r12.A01 == i2) {
                    StringBuilder sb3 = new StringBuilder("UserNoticeContentManager/getUserNoticeContentFromLocal/has content for notice id: ");
                    sb3.append(i);
                    sb3.append(" version: ");
                    sb3.append(i2);
                    Log.i(sb3.toString());
                    A06(this.A00, i);
                    return this.A00;
                }
                if (A09(new String[]{"content.json"}, i)) {
                    try {
                        FileInputStream fileInputStream = new FileInputStream(new File(A01(this.A02.A00, i), "content.json"));
                        C43891xl A003 = C43931xr.A00(fileInputStream, i);
                        this.A00 = A003;
                        if (A003 != null) {
                            A06(A003, i);
                            C43891xl r0 = this.A00;
                            fileInputStream.close();
                            return r0;
                        }
                        Log.e("UserNoticeContentManager/getUserNoticeContentFromLocal/error parsing");
                        A04(i);
                        this.A07.A02(3);
                        fileInputStream.close();
                        return null;
                    } catch (IOException e) {
                        Log.e("UserNoticeContentManager/getUserNoticeContentFromLocal/exception", e);
                        return null;
                    }
                }
                return null;
            }
        }
        Log.i(str);
        return null;
    }

    public void A04(int i) {
        StringBuilder sb = new StringBuilder("UserNoticeContentManager/deleteUserNoticeData/notice id: ");
        sb.append(i);
        Log.i(sb.toString());
        File A01 = A01(this.A02.A00, i);
        if (A01 != null) {
            this.A08.Ab2(new RunnableBRunnable0Shape13S0100000_I0_13(A01, 11));
        }
        this.A00 = null;
    }

    public void A05(int i) {
        String str;
        StringBuilder sb = new StringBuilder("UserNoticeContentManager/fetchAndStoreUserNoticeContent/notice id ");
        sb.append(i);
        Log.i(sb.toString());
        C006403a r9 = new C006403a();
        r9.A01("notice_id", i);
        C15570nT r0 = this.A01;
        r0.A08();
        Me me = r0.A00;
        if (me == null) {
            StringBuilder sb2 = new StringBuilder("UserNoticeContentManager/fetchAndStoreUserNoticeContent/could not create notice uri for notice id ");
            sb2.append(i);
            Log.e(sb2.toString());
            return;
        }
        Uri.Builder appendQueryParameter = new Uri.Builder().scheme("https").authority("whatsapp.com").appendPath("user-notice").appendPath("v1").appendQueryParameter("id", String.valueOf(i));
        AnonymousClass018 r3 = this.A04;
        Uri.Builder appendQueryParameter2 = appendQueryParameter.appendQueryParameter("lg", r3.A06()).appendQueryParameter("lc", r3.A05()).appendQueryParameter("cc", C22650zQ.A00(me.cc)).appendQueryParameter("platform", "android");
        if (this.A02.A00.getResources().getDisplayMetrics().densityDpi <= 240) {
            str = "hdpi";
        } else {
            str = "xxhdpi";
        }
        Uri build = appendQueryParameter2.appendQueryParameter("img-size", str).build();
        build.toString();
        r9.A00.put("url", build.toString());
        C006503b A00 = r9.A00();
        C003901s r1 = new C003901s();
        r1.A01 = EnumC004001t.CONNECTED;
        C004101u r8 = new C004101u(r1);
        C004201x r32 = new C004201x(UserNoticeContentWorker.class);
        r32.A01.add("tag.whatsapp.usernotice.content.fetch");
        r32.A00.A09 = r8;
        EnumC007503u r7 = EnumC007503u.EXPONENTIAL;
        TimeUnit timeUnit = TimeUnit.HOURS;
        r32.A03(r7, timeUnit, 1);
        r32.A00.A0A = A00;
        C004201x r5 = new C004201x(UserNoticeIconWorker.class);
        r5.A01.add("tag.whatsapp.usernotice.icon.fetch");
        r5.A00.A09 = r8;
        r5.A03(r7, timeUnit, 1);
        r5.A00.A0A = r9.A00();
        StringBuilder sb3 = new StringBuilder("tag.whatsapp.usernotice.content.fetch.");
        sb3.append(i);
        String obj = sb3.toString();
        ((AnonymousClass022) this.A09.get()).A01(AnonymousClass023.REPLACE, (AnonymousClass021) r32.A00(), obj).A02((AnonymousClass021) r5.A00()).A03();
    }

    public final void A06(C43891xl r4, int i) {
        StringBuilder sb = new StringBuilder("UserNoticeContentManager/populateIconFiles/notice id: ");
        sb.append(i);
        Log.i(sb.toString());
        A07(r4.A02, "banner_icon_light.png", "banner_icon_dark.png", i);
        A07(r4.A04, "modal_icon_light.png", "modal_icon_dark.png", i);
        A07(r4.A03, "blocking_modal_icon_light.png", "blocking_modal_icon_dark.png", i);
    }

    public final void A07(C43851xh r3, String str, String str2, int i) {
        if (r3 != null && A09(new String[]{str, str2}, i)) {
            File A01 = A01(this.A02.A00, i);
            r3.A01 = new File(A01, str);
            r3.A00 = new File(A01, str2);
        }
    }

    public boolean A08(InputStream inputStream, String str, int i) {
        try {
            File A01 = A01(this.A02.A00, i);
            if (A01 == null) {
                return false;
            }
            StringBuilder sb = new StringBuilder();
            sb.append("UserNoticeContentWorker/storeUserNoticeContent/storing user notice for ");
            sb.append(i);
            Log.i(sb.toString());
            FileOutputStream fileOutputStream = new FileOutputStream(new File(A01, str));
            C14350lI.A0G(inputStream, fileOutputStream);
            fileOutputStream.close();
            return true;
        } catch (IOException e) {
            Log.e("UserNoticeContentWorker/storeUserNoticeContent/failed to store", e);
            return false;
        }
    }

    public final boolean A09(String[] strArr, int i) {
        File[] listFiles;
        HashSet hashSet = new HashSet();
        Collections.addAll(hashSet, strArr);
        File A01 = A01(this.A02.A00, i);
        if (!(A01 == null || (listFiles = A01.listFiles()) == null)) {
            for (File file : listFiles) {
                hashSet.remove(file.getName());
            }
        }
        boolean isEmpty = hashSet.isEmpty();
        StringBuilder sb = new StringBuilder("UserNoticeContentManager/userNoticeFilesExist/notice id ");
        sb.append(i);
        sb.append(" files exists: ");
        sb.append(isEmpty);
        Log.i(sb.toString());
        return isEmpty;
    }
}
