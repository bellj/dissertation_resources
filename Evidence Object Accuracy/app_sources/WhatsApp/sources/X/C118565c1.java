package X;

import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import org.json.JSONArray;

/* renamed from: X.5c1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118565c1 extends AnonymousClass02M {
    public int A00 = -1;
    public final LayoutInflater A01;
    public final AnonymousClass024 A02;
    public final C38721ob A03;
    public final String A04;
    public final String A05;
    public final String A06;
    public final String A07;
    public final String A08;
    public final String A09;
    public final String A0A;
    public final JSONArray A0B;
    public final boolean A0C;
    public final boolean A0D;

    public C118565c1(LayoutInflater layoutInflater, AnonymousClass024 r3, C38721ob r4, String str, String str2, String str3, String str4, String str5, String str6, String str7, JSONArray jSONArray, boolean z, boolean z2) {
        this.A0B = jSONArray;
        this.A08 = str;
        this.A09 = str2;
        this.A07 = str3;
        this.A0A = str4;
        this.A04 = str5;
        this.A05 = str6;
        this.A06 = str7;
        this.A0D = z;
        this.A0C = z2;
        this.A03 = r4;
        this.A01 = layoutInflater;
        this.A02 = r3;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A0B.length();
    }

    /* JADX WARN: Type inference failed for: r1v4, types: [boolean, int] */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // X.AnonymousClass02M
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ void ANH(X.AnonymousClass03U r11, int r12) {
        /*
        // Method dump skipped, instructions count: 376
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C118565c1.ANH(X.03U, int):void");
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        View A0F = C12960it.A0F(this.A01, viewGroup, R.layout.wa_list_view_row);
        if (this.A0D) {
            TypedValue typedValue = new TypedValue();
            viewGroup.getContext().getTheme().resolveAttribute(16843534, typedValue, true);
            A0F.setBackgroundResource(typedValue.resourceId);
        }
        return new C118795cO(A0F, this);
    }
}
