package X;

import android.graphics.Outline;
import android.view.View;
import android.view.ViewOutlineProvider;

/* renamed from: X.3gr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73823gr extends ViewOutlineProvider {
    public int A00;
    public boolean A01;

    public void A00(int i) {
        this.A00 = i;
    }

    public void A01(boolean z) {
        this.A01 = z;
    }

    @Override // android.view.ViewOutlineProvider
    public void getOutline(View view, Outline outline) {
        if (this.A01) {
            outline.setRoundRect(0, 0, view.getWidth(), view.getHeight(), (float) this.A00);
        } else {
            outline.setRect(0, 0, view.getWidth(), view.getHeight());
        }
    }
}
