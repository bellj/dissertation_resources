package X;

/* renamed from: X.12e  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C235712e {
    public final /* synthetic */ C19960ux A00;

    public C235712e(C19960ux r1) {
        this.A00 = r1;
    }

    public C22210yi A00(boolean z) {
        AnonymousClass01J r1 = this.A00.A01;
        C14330lG r4 = (C14330lG) r1.A7B.get();
        C22230yk r8 = (C22230yk) r1.ANT.get();
        AnonymousClass146 r9 = (AnonymousClass146) r1.AKM.get();
        C235512c r10 = (C235512c) r1.AKS.get();
        C15660nh r7 = (C15660nh) r1.ABE.get();
        return new C22210yi((C19450u8) r1.A7H.get(), (AnonymousClass18Z) r1.A7I.get(), r4, (C14900mE) r1.A8X.get(), (C002701f) r1.AHU.get(), r7, r8, r9, r10, (AbstractC14440lR) r1.ANe.get(), z);
    }
}
