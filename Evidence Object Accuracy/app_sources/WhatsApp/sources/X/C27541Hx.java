package X;

import java.util.List;

/* renamed from: X.1Hx */
/* loaded from: classes2.dex */
public final class C27541Hx {
    public final C58722rH A00;
    public final C58702rF A01;
    public final List A02;
    public final List A03;
    public final boolean A04;
    public final boolean A05;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C27541Hx) {
                C27541Hx r5 = (C27541Hx) obj;
                if (this.A05 != r5.A05 || !C16700pc.A0O(this.A03, r5.A03) || !C16700pc.A0O(this.A02, r5.A02) || !C16700pc.A0O(this.A00, r5.A00) || !C16700pc.A0O(this.A01, r5.A01) || this.A04 != r5.A04) {
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        boolean z = this.A05;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int hashCode = ((((i2 * 31) + this.A03.hashCode()) * 31) + this.A02.hashCode()) * 31;
        C58722rH r0 = this.A00;
        int i5 = 0;
        int hashCode2 = (hashCode + (r0 == null ? 0 : r0.hashCode())) * 31;
        C58702rF r02 = this.A01;
        if (r02 != null) {
            i5 = r02.hashCode();
        }
        int i6 = (hashCode2 + i5) * 31;
        if (!this.A04) {
            i = 0;
        }
        return i6 + i;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("ViewState(isSaving=");
        sb.append(this.A05);
        sb.append(", avatarPoses=");
        sb.append(this.A03);
        sb.append(", avatarBackgrounds=");
        sb.append(this.A02);
        sb.append(", selectedBackground=");
        sb.append(this.A00);
        sb.append(", selectedPose=");
        sb.append(this.A01);
        sb.append(", isLoading=");
        sb.append(this.A04);
        sb.append(')');
        return sb.toString();
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C27541Hx() {
        /*
            r7 = this;
            r1 = 0
            r5 = 0
            X.1WF r3 = X.AnonymousClass1WF.A00
            r0 = r7
            r2 = r1
            r4 = r3
            r6 = r5
            r0.<init>(r1, r2, r3, r4, r5, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C27541Hx.<init>():void");
    }

    public C27541Hx(C58722rH r2, C58702rF r3, List list, List list2, boolean z, boolean z2) {
        C16700pc.A0E(list2, 3);
        this.A05 = z;
        this.A03 = list;
        this.A02 = list2;
        this.A00 = r2;
        this.A01 = r3;
        this.A04 = z2;
    }

    public static /* synthetic */ C27541Hx A00(C58722rH r7, C58702rF r8, C27541Hx r9, List list, List list2, int i, boolean z, boolean z2) {
        boolean z3 = z2;
        List list3 = list;
        boolean z4 = z;
        C58702rF r2 = r8;
        List list4 = list2;
        C58722rH r1 = r7;
        if ((i & 1) != 0) {
            z4 = r9.A05;
        }
        if ((i & 2) != 0) {
            list3 = r9.A03;
        }
        if ((i & 4) != 0) {
            list4 = r9.A02;
        }
        if ((i & 8) != 0) {
            r1 = r9.A00;
        }
        if ((i & 16) != 0) {
            r2 = r9.A01;
        }
        if ((i & 32) != 0) {
            z3 = r9.A04;
        }
        C16700pc.A0E(list3, 1);
        C16700pc.A0E(list4, 2);
        return new C27541Hx(r1, r2, list3, list4, z4, z3);
    }
}
