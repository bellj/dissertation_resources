package X;

/* renamed from: X.01t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public enum EnumC004001t {
    NOT_REQUIRED,
    CONNECTED,
    UNMETERED,
    NOT_ROAMING,
    METERED,
    TEMPORARILY_UNMETERED
}
