package X;

/* renamed from: X.5bO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118175bO extends AnonymousClass015 implements AnonymousClass1In {
    public AbstractC001200n A00;
    public AnonymousClass016 A01 = new AnonymousClass016(C12960it.A0l());
    public C16590pI A02;
    public C27691It A03 = C13000ix.A03();
    public final C14900mE A04;
    public final C20370ve A05;
    public final AnonymousClass6BE A06;
    public final AbstractC14440lR A07;

    public C118175bO(AbstractC001200n r3, C14900mE r4, C16590pI r5, C20370ve r6, AnonymousClass6BE r7, AbstractC14440lR r8) {
        this.A00 = r3;
        this.A02 = r5;
        this.A04 = r4;
        this.A07 = r8;
        this.A05 = r6;
        this.A06 = r7;
    }

    public void A04(C127085tv r7) {
        AnonymousClass6BE r0;
        int A0i;
        Integer num;
        boolean z;
        String str;
        String str2;
        int i = r7.A00;
        if (i != 0) {
            z = true;
            if (i == 1) {
                this.A07.Ab2(new AnonymousClass6HG(this));
                return;
            } else if (i == 2) {
                r0 = this.A06;
                A0i = 1;
                str = "mandate_payment_screen";
                str2 = "payment_home";
                num = 1;
            } else {
                return;
            }
        } else {
            this.A07.Ab2(new AnonymousClass6HG(this));
            r0 = this.A06;
            A0i = C12980iv.A0i();
            num = null;
            z = true;
            str = "mandate_payment_screen";
            str2 = "payment_home";
        }
        r0.AKh(A0i, num, str, str2, z);
    }

    @Override // X.AnonymousClass1In
    public void ATd() {
        this.A07.Ab2(new AnonymousClass6HG(this));
    }
}
