package X;

import android.view.ViewGroup;
import java.util.List;

/* renamed from: X.2gl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54462gl extends AnonymousClass02M {
    public AnonymousClass3EY A00;
    public final AnonymousClass01d A01;
    public final C16120oU A02;
    public final C253719d A03;
    public final AnonymousClass5UF A04;
    public final C16630pM A05;

    public C54462gl(AnonymousClass01d r1, C16120oU r2, C253719d r3, AnonymousClass5UF r4, C16630pM r5) {
        this.A03 = r3;
        this.A02 = r2;
        this.A01 = r1;
        this.A05 = r5;
        this.A04 = r4;
    }

    @Override // X.AnonymousClass02M
    public void A08(AnonymousClass03U r1) {
        ((AbstractC51682Vy) r1).A08();
    }

    @Override // X.AnonymousClass02M
    public void A09(AnonymousClass03U r1) {
        ((AbstractC51682Vy) r1).A09();
    }

    @Override // X.AnonymousClass02M
    public synchronized int A0D() {
        int i;
        AnonymousClass3EY r0 = this.A00;
        if (r0 == null) {
            i = 0;
        } else {
            i = r0.A04.size() + (C12960it.A1W(this.A00.A00) ? 1 : 0);
        }
        return i;
    }

    public void A0E(AnonymousClass3EY r2) {
        if (r2.equals(this.A00)) {
            A02();
        }
    }

    public synchronized void A0F(AnonymousClass3EY r2) {
        AnonymousClass3EY r0 = this.A00;
        if (r0 != null) {
            r0.A01.remove(this);
        }
        this.A00 = r2;
        r2.A01.add(this);
        if (!r2.A04.isEmpty()) {
            A0E(r2);
        }
        A02();
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r10, int i) {
        C66013Ly r0;
        boolean z;
        C617331x r102 = (C617331x) r10;
        synchronized (this) {
            if (i < this.A00.A04.size()) {
                AnonymousClass3EY r5 = this.A00;
                List list = r5.A04;
                if (((double) (1 + i)) >= ((double) list.size()) * 0.75d && !r5.A03) {
                    if (!r5.A02) {
                        String str = r5.A00;
                        if (r5 instanceof AnonymousClass327) {
                            AnonymousClass327 r3 = (AnonymousClass327) r5;
                            if (str != null) {
                                C12960it.A1E(new AnonymousClass323(r3, r3.A01, str), r3.A00.A0A);
                            }
                            z = false;
                        } else if (r5 instanceof AnonymousClass325) {
                            AnonymousClass325 r2 = (AnonymousClass325) r5;
                            if (str != null) {
                                C12960it.A1E(new AnonymousClass371(r2, str), r2.A00.A0A);
                            }
                            z = false;
                        } else if (!(r5 instanceof AnonymousClass326)) {
                            AnonymousClass324 r22 = (AnonymousClass324) r5;
                            if (str != null) {
                                C12960it.A1E(new AnonymousClass370(r22, str), r22.A00.A0A);
                            }
                            z = false;
                        } else {
                            AnonymousClass326 r32 = (AnonymousClass326) r5;
                            if (str != null) {
                                C12960it.A1E(new AnonymousClass322(r32, r32.A01, str), r32.A00.A0A);
                            }
                            z = false;
                        }
                        r5.A03 = z;
                    }
                    z = true;
                    r5.A03 = z;
                }
                r0 = (C66013Ly) list.get(i);
            } else {
                r0 = null;
            }
            r102.A00 = r0;
        }
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        C253719d r4 = this.A03;
        C16120oU r3 = this.A02;
        return new C617331x(viewGroup, this.A01, r3, r4, this.A04, this.A05);
    }
}
