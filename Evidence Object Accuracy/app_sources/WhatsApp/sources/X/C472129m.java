package X;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.Log;
import com.whatsapp.R;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.util.Locale;
import org.chromium.net.UrlRequest;

/* renamed from: X.29m  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C472129m {
    public static Locale A00;
    public static final AnonymousClass00O A01 = new AnonymousClass00O();

    public static String A00(Context context) {
        String packageName = context.getPackageName();
        try {
            Context context2 = C15080mX.A00(context).A00;
            return context2.getPackageManager().getApplicationLabel(context2.getPackageManager().getApplicationInfo(packageName, 0)).toString();
        } catch (PackageManager.NameNotFoundException | NullPointerException unused) {
            String str = context.getApplicationInfo().name;
            return TextUtils.isEmpty(str) ? packageName : str;
        }
    }

    public static String A01(Context context, int i) {
        int i2;
        String str;
        Resources resources = context.getResources();
        String A002 = A00(context);
        if (i == 1) {
            i2 = R.string.common_google_play_services_install_text;
        } else if (i != 2) {
            if (i != 3) {
                if (i == 5) {
                    str = "common_google_play_services_invalid_account_text";
                } else if (i == 7) {
                    str = "common_google_play_services_network_error_text";
                } else if (i == 9) {
                    i2 = R.string.common_google_play_services_unsupported_text;
                } else if (i != 20) {
                    switch (i) {
                        case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                            str = "common_google_play_services_api_unavailable_text";
                            break;
                        case 17:
                            str = "common_google_play_services_sign_in_failed_text";
                            break;
                        case 18:
                            i2 = R.string.common_google_play_services_updating_text;
                            break;
                        default:
                            i2 = R.string.common_google_play_services_unknown_issue;
                            break;
                    }
                } else {
                    str = "common_google_play_services_restricted_profile_text";
                }
                Resources resources2 = context.getResources();
                String A03 = A03(context, str);
                if (A03 == null) {
                    A03 = resources2.getString(R.string.common_google_play_services_unknown_issue);
                }
                return String.format(resources2.getConfiguration().locale, A03, A002);
            }
            i2 = R.string.common_google_play_services_enable_text;
        } else if (C472629u.A00(context)) {
            return resources.getString(R.string.common_google_play_services_wear_update_text);
        } else {
            i2 = R.string.common_google_play_services_update_text;
        }
        return resources.getString(i2, A002);
    }

    public static String A02(Context context, int i) {
        int i2;
        String str;
        String str2;
        Resources resources = context.getResources();
        switch (i) {
            case 1:
                i2 = R.string.common_google_play_services_install_title;
                return resources.getString(i2);
            case 2:
                i2 = R.string.common_google_play_services_update_title;
                return resources.getString(i2);
            case 3:
                i2 = R.string.common_google_play_services_enable_title;
                return resources.getString(i2);
            case 4:
            case 6:
            case 18:
                return null;
            case 5:
                Log.e("GoogleApiAvailability", "An invalid account was specified when connecting. Please provide a valid account.");
                str = "common_google_play_services_invalid_account_title";
                return A03(context, str);
            case 7:
                Log.e("GoogleApiAvailability", "Network error occurred. Please retry request later.");
                str = "common_google_play_services_network_error_title";
                return A03(context, str);
            case 8:
                str2 = "Internal error occurred. Please see logs for detailed information";
                Log.e("GoogleApiAvailability", str2);
                return null;
            case 9:
                str2 = "Google Play services is invalid. Cannot recover.";
                Log.e("GoogleApiAvailability", str2);
                return null;
            case 10:
                str2 = "Developer error occurred. Please see logs for detailed information";
                Log.e("GoogleApiAvailability", str2);
                return null;
            case 11:
                str2 = "The application is not licensed to the user.";
                Log.e("GoogleApiAvailability", str2);
                return null;
            case 12:
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
            case 15:
            case 19:
            default:
                StringBuilder sb = new StringBuilder(33);
                sb.append("Unexpected error code ");
                sb.append(i);
                str2 = sb.toString();
                Log.e("GoogleApiAvailability", str2);
                return null;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                str2 = "One of the API components you attempted to connect to is not available.";
                Log.e("GoogleApiAvailability", str2);
                return null;
            case 17:
                Log.e("GoogleApiAvailability", "The specified account could not be signed in.");
                str = "common_google_play_services_sign_in_failed_title";
                return A03(context, str);
            case C43951xu.A01:
                Log.e("GoogleApiAvailability", "The current user profile is restricted and could not use authenticated features.");
                str = "common_google_play_services_restricted_profile_title";
                return A03(context, str);
        }
    }

    public static String A03(Context context, String str) {
        Resources resources;
        String str2;
        String str3;
        AnonymousClass00O r3 = A01;
        synchronized (r3) {
            Locale AAS = C04100Kj.A00(context.getResources().getConfiguration()).A00.AAS(0);
            if (!AAS.equals(A00)) {
                r3.clear();
                A00 = AAS;
            }
            String str4 = (String) r3.get(str);
            if (str4 == null) {
                try {
                    resources = context.getPackageManager().getResourcesForApplication("com.google.android.gms");
                } catch (PackageManager.NameNotFoundException unused) {
                    resources = null;
                }
                str4 = null;
                if (resources != null) {
                    int identifier = resources.getIdentifier(str, "string", "com.google.android.gms");
                    if (identifier == 0) {
                        str2 = "GoogleApiAvailability";
                        if (str.length() != 0) {
                            str3 = "Missing resource: ".concat(str);
                        } else {
                            str3 = new String("Missing resource: ");
                        }
                    } else {
                        String string = resources.getString(identifier);
                        if (TextUtils.isEmpty(string)) {
                            str2 = "GoogleApiAvailability";
                            if (str.length() != 0) {
                                str3 = "Got empty resource: ".concat(str);
                            } else {
                                str3 = new String("Got empty resource: ");
                            }
                        } else {
                            r3.put(str, string);
                            return string;
                        }
                    }
                    Log.w(str2, str3);
                }
            }
            return str4;
        }
    }
}
