package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0111000_I0;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.0u1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19380u1 {
    public C008504i A00;
    public C008604j A01;
    public AtomicInteger A02 = new AtomicInteger();
    public boolean A03 = false;
    public boolean[] A04;
    public boolean[] A05;
    public final AbstractC15710nm A06;
    public final C15450nH A07;
    public final C14820m6 A08;
    public final AnonymousClass12H A09;
    public final C14850m9 A0A;
    public final C16120oU A0B;
    public final AnonymousClass16B A0C;
    public final ExecutorC27271Gr A0D;
    public final AbstractC14440lR A0E;

    public C19380u1(AbstractC15710nm r3, C15450nH r4, C14820m6 r5, AnonymousClass12H r6, C14850m9 r7, C16120oU r8, AnonymousClass16B r9, AbstractC14440lR r10) {
        this.A0A = r7;
        this.A06 = r3;
        this.A0E = r10;
        this.A0B = r8;
        this.A07 = r4;
        this.A09 = r6;
        this.A0C = r9;
        this.A08 = r5;
        this.A0D = new ExecutorC27271Gr(r10, false);
        this.A05 = new boolean[8];
        this.A04 = new boolean[8];
    }

    public void A00(int i, boolean z) {
        this.A0D.execute(new RunnableBRunnable0Shape0S0111000_I0(this, i, 0, z));
    }

    /* JADX WARNING: Removed duplicated region for block: B:101:0x02cf A[ORIG_RETURN, RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x002d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A01(java.lang.String r24) {
        /*
        // Method dump skipped, instructions count: 720
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C19380u1.A01(java.lang.String):void");
    }
}
