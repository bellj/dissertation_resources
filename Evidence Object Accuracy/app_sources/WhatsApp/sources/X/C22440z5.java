package X;

/* renamed from: X.0z5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22440z5 {
    public final C22700zV A00;
    public final C20090vC A01;
    public final C16490p7 A02;

    public C22440z5(C22700zV r1, C20090vC r2, C16490p7 r3) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x005e, code lost:
        if (r8 != null) goto L_0x0060;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C32351c1 A00(long r10) {
        /*
            r9 = this;
            X.0p7 r0 = r9.A02
            X.0on r7 = r0.get()
            X.0op r4 = r7.A03     // Catch: all -> 0x0067
            java.lang.String r3 = "SELECT host_storage, actual_actors, privacy_mode_ts, business_name FROM message_privacy_state WHERE message_row_id = ?"
            r0 = 1
            java.lang.String[] r2 = new java.lang.String[r0]     // Catch: all -> 0x0067
            r1 = 0
            java.lang.String r0 = java.lang.Long.toString(r10)     // Catch: all -> 0x0067
            r2[r1] = r0     // Catch: all -> 0x0067
            android.database.Cursor r8 = r4.A09(r3, r2)     // Catch: all -> 0x0067
            if (r8 == 0) goto L_0x005d
            java.lang.String r0 = "host_storage"
            int r3 = r8.getColumnIndexOrThrow(r0)     // Catch: all -> 0x0058
            java.lang.String r0 = "actual_actors"
            int r2 = r8.getColumnIndexOrThrow(r0)     // Catch: all -> 0x0058
            java.lang.String r0 = "privacy_mode_ts"
            int r1 = r8.getColumnIndexOrThrow(r0)     // Catch: all -> 0x0058
            java.lang.String r0 = "business_name"
            int r6 = r8.getColumnIndexOrThrow(r0)     // Catch: all -> 0x0058
            boolean r0 = r8.moveToNext()     // Catch: all -> 0x0058
            if (r0 == 0) goto L_0x005d
            X.1c1 r5 = new X.1c1     // Catch: all -> 0x0058
            r5.<init>(r9)     // Catch: all -> 0x0058
            int r4 = r8.getInt(r3)     // Catch: all -> 0x0058
            int r3 = r8.getInt(r2)     // Catch: all -> 0x0058
            int r0 = r8.getInt(r1)     // Catch: all -> 0x0058
            long r1 = (long) r0     // Catch: all -> 0x0058
            X.1bg r0 = new X.1bg     // Catch: all -> 0x0058
            r0.<init>(r4, r1, r3)     // Catch: all -> 0x0058
            r5.A00 = r0     // Catch: all -> 0x0058
            java.lang.String r0 = r8.getString(r6)     // Catch: all -> 0x0058
            r5.A01 = r0     // Catch: all -> 0x0058
            goto L_0x0060
        L_0x0058:
            r0 = move-exception
            r8.close()     // Catch: all -> 0x005c
        L_0x005c:
            throw r0     // Catch: all -> 0x0067
        L_0x005d:
            r5 = 0
            if (r8 == 0) goto L_0x0063
        L_0x0060:
            r8.close()     // Catch: all -> 0x0067
        L_0x0063:
            r7.close()
            return r5
        L_0x0067:
            r0 = move-exception
            r7.close()     // Catch: all -> 0x006b
        L_0x006b:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22440z5.A00(long):X.1c1");
    }

    public void A01(long j) {
        C16310on A02 = this.A02.A02();
        try {
            A02.A03.A01("message_privacy_state", "message_row_id = ?", new String[]{String.valueOf(j)});
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A02(AbstractC15340mz r3) {
        C32351c1 A00 = A00(r3.A11);
        if (A00 != null) {
            r3.A0h = A00.A01;
            r3.A0U = A00.A00;
        }
    }
}
