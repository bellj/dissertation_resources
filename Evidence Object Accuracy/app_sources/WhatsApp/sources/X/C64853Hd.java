package X;

import android.view.View;
import android.widget.ImageView;
import com.whatsapp.CircularProgressBar;
import com.whatsapp.R;
import com.whatsapp.WaButton;
import com.whatsapp.stickers.StickerView;
import com.whatsapp.util.ViewOnClickCListenerShape18S0100000_I1_1;

/* renamed from: X.3Hd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64853Hd {
    public static final int A0N;
    public static final int A0O;
    public AbstractC16130oV A00;
    public boolean A01;
    public boolean A02;
    public final View.OnClickListener A03 = new ViewOnClickCListenerShape18S0100000_I1_1(this, 15);
    public final View A04;
    public final View A05;
    public final ImageView A06;
    public final CircularProgressBar A07;
    public final C239613r A08;
    public final C16170oZ A09;
    public final WaButton A0A;
    public final C15890o4 A0B;
    public final AnonymousClass018 A0C;
    public final C14850m9 A0D;
    public final AnonymousClass109 A0E;
    public final C22370yy A0F;
    public final AnonymousClass1AB A0G;
    public final StickerView A0H;
    public final AbstractC41521tf A0I = new C70253az(this);
    public final AnonymousClass19O A0J;
    public final AbstractView$OnClickListenerC34281fs A0K = new ViewOnClickCListenerShape18S0100000_I1_1(this, 12);
    public final AbstractView$OnClickListenerC34281fs A0L = new ViewOnClickCListenerShape18S0100000_I1_1(this, 13);
    public final AbstractView$OnClickListenerC34281fs A0M = new ViewOnClickCListenerShape18S0100000_I1_1(this, 14);

    static {
        boolean z = C28111Kr.A00;
        int i = 7;
        int i2 = 1;
        if (z) {
            i2 = 7;
        }
        A0N = i2;
        if (!z) {
            i = 3;
        }
        A0O = i;
    }

    public C64853Hd(View view, C239613r r4, C16170oZ r5, C15890o4 r6, AnonymousClass018 r7, C14850m9 r8, AnonymousClass109 r9, C22370yy r10, AnonymousClass1AB r11, AnonymousClass19O r12) {
        this.A05 = view;
        this.A0H = (StickerView) view.findViewById(R.id.sticker_image);
        CircularProgressBar circularProgressBar = (CircularProgressBar) view.findViewById(R.id.progress_bar);
        this.A07 = circularProgressBar;
        circularProgressBar.A0B = 0;
        this.A06 = C12970iu.A0L(view, R.id.cancel_download);
        this.A04 = view.findViewById(R.id.control_frame);
        this.A0A = (WaButton) view.findViewById(R.id.control_btn);
        this.A0D = r8;
        this.A0G = r11;
        this.A08 = r4;
        this.A09 = r5;
        this.A0C = r7;
        this.A0J = r12;
        this.A0B = r6;
        this.A0F = r10;
        this.A0E = r9;
    }

    public void A00() {
        StickerView stickerView;
        AbstractView$OnClickListenerC34281fs r0;
        View view = this.A04;
        view.setVisibility(0);
        CircularProgressBar circularProgressBar = this.A07;
        ImageView imageView = this.A06;
        WaButton waButton = this.A0A;
        AbstractC42671vd.A0a(view, circularProgressBar, waButton, imageView, false, false, false);
        AbstractC16130oV r1 = this.A00;
        if (!r1.A0z.A02 || C30041Vv.A11(r1)) {
            stickerView = this.A0H;
            C12960it.A0r(stickerView.getContext(), stickerView, R.string.button_download);
            waButton.setText(C30041Vv.A0B(this.A0C, this.A00.A01));
            waButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_download, 0, 0, 0);
            r0 = this.A0L;
        } else {
            stickerView = this.A0H;
            C12960it.A0r(stickerView.getContext(), stickerView, R.string.retry);
            waButton.setText(R.string.retry);
            waButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_upload, 0, 0, 0);
            r0 = this.A0M;
        }
        waButton.setOnClickListener(r0);
        stickerView.setOnClickListener(r0);
    }

    public void A01() {
        boolean z = this.A00.A0z.A02;
        View view = this.A04;
        if (!z) {
            view.setVisibility(0);
            CircularProgressBar circularProgressBar = this.A07;
            ImageView imageView = this.A06;
            WaButton waButton = this.A0A;
            AbstractC42671vd.A0a(view, circularProgressBar, waButton, imageView, true, false, false);
            StickerView stickerView = this.A0H;
            C12960it.A0r(stickerView.getContext(), stickerView, R.string.image_transfer_in_progress);
            AbstractView$OnClickListenerC34281fs r0 = this.A0K;
            waButton.setOnClickListener(r0);
            circularProgressBar.setOnClickListener(r0);
        } else {
            view.setVisibility(8);
        }
        this.A0H.setOnClickListener(null);
    }

    public void A02() {
        View view = this.A04;
        view.setVisibility(8);
        CircularProgressBar circularProgressBar = this.A07;
        ImageView imageView = this.A06;
        WaButton waButton = this.A0A;
        AbstractC42671vd.A0a(view, circularProgressBar, waButton, imageView, false, false, false);
        waButton.setOnClickListener(null);
        this.A0H.setOnClickListener(this.A03);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0077, code lost:
        if (r3 != null) goto L_0x003c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A03(X.C30061Vy r13, boolean r14) {
        /*
            r12 = this;
            r12.A00 = r13
            r3 = 0
            if (r14 == 0) goto L_0x000a
            com.whatsapp.stickers.StickerView r0 = r12.A0H
            r0.setImageDrawable(r3)
        L_0x000a:
            X.1KS r5 = r13.A1C()
            X.0oX r1 = X.AbstractC15340mz.A00(r13)
            com.whatsapp.stickers.StickerView r4 = r12.A0H
            android.content.res.Resources r2 = X.C12960it.A09(r4)
            r0 = 2131165744(0x7f070230, float:1.7945714E38)
            int r8 = r2.getDimensionPixelSize(r0)
            r4.setOnClickListener(r3)
            java.lang.String r0 = r5.A08
            if (r0 == 0) goto L_0x0046
            byte[] r0 = com.whatsapp.stickers.WebpUtils.fetchWebpMetadata(r0)
            X.1KB r3 = X.AnonymousClass1KB.A00(r0)
            X.0m9 r2 = r12.A0D
            r0 = 1396(0x574, float:1.956E-42)
            boolean r0 = r2.A07(r0)
            if (r0 == 0) goto L_0x0077
            if (r3 == 0) goto L_0x0046
            r13.A01 = r3
        L_0x003c:
            X.1mS[] r0 = r3.A08
            if (r0 == 0) goto L_0x0046
            java.lang.String r0 = X.AnonymousClass1KS.A00(r0)
            r5.A06 = r0
        L_0x0046:
            android.content.Context r0 = r4.getContext()
            java.lang.String r0 = X.C28111Kr.A01(r0, r5)
            r4.setContentDescription(r0)
            java.lang.String r0 = r5.A0C
            if (r0 == 0) goto L_0x0073
            java.io.File r0 = r1.A0F
            if (r0 != 0) goto L_0x005f
            java.lang.String r0 = r13.A16()
            if (r0 == 0) goto L_0x0073
        L_0x005f:
            X.1AB r3 = r12.A0G
            X.3a9 r6 = new X.3a9
            r6.<init>(r1, r12, r13, r14)
            r7 = 1
            r11 = 0
            r10 = 1
            r9 = r8
            r3.A04(r4, r5, r6, r7, r8, r9, r10, r11)
        L_0x006d:
            android.view.View r0 = r12.A05
            r0.invalidate()
            return
        L_0x0073:
            r12.A04(r13, r14)
            goto L_0x006d
        L_0x0077:
            if (r3 == 0) goto L_0x0046
            goto L_0x003c
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C64853Hd.A03(X.1Vy, boolean):void");
    }

    public final void A04(C30061Vy r8, boolean z) {
        if (!this.A01 || z) {
            this.A01 = false;
            this.A0J.A07(this.A0H, r8, this.A0I);
            return;
        }
        this.A01 = false;
        this.A0J.A0B(this.A0H, r8, this.A0I, r8.A0z, false);
    }
}
