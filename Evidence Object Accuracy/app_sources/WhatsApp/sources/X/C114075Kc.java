package X;

/* renamed from: X.5Kc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C114075Kc extends AbstractC111945Bk {
    public static final C114075Kc A00 = new C114075Kc(1, 0);

    public C114075Kc(int i, int i2) {
        super(i, i2);
    }

    public Integer A03() {
        return Integer.valueOf(this.A01);
    }

    public Integer A04() {
        return Integer.valueOf(this.A00);
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (!(obj instanceof C114075Kc)) {
            return false;
        }
        int i = this.A00;
        int i2 = this.A01;
        if (i > i2) {
            AbstractC111945Bk r0 = (AbstractC111945Bk) obj;
            if (r0.A00 > r0.A01) {
                return true;
            }
        }
        AbstractC111945Bk r5 = (AbstractC111945Bk) obj;
        if (i == r5.A00 && i2 == r5.A01) {
            return true;
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        int i = this.A00;
        int i2 = this.A01;
        int i3 = (i * 31) + i2;
        if (i > i2) {
            return -1;
        }
        return i3;
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0h = C12960it.A0h();
        A0h.append(this.A00);
        A0h.append("..");
        return C12960it.A0f(A0h, this.A01);
    }
}
