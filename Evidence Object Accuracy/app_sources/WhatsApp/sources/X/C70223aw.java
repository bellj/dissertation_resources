package X;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import com.whatsapp.R;

/* renamed from: X.3aw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70223aw implements AbstractC41521tf {
    public final /* synthetic */ C42511vK A00;

    @Override // X.AbstractC41521tf
    public /* synthetic */ void AQV() {
    }

    public C70223aw(C42511vK r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC41521tf
    public int AGm() {
        return this.A00.getResources().getDimensionPixelSize(R.dimen.conversation_row_message_thumb_size);
    }

    @Override // X.AbstractC41521tf
    public void Adg(Bitmap bitmap, View view, AbstractC15340mz r5) {
        ImageView imageView = this.A00.A0E;
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        } else {
            imageView.setImageResource(R.drawable.media_location);
        }
    }

    @Override // X.AbstractC41521tf
    public void Adu(View view) {
        ImageView imageView = this.A00.A0E;
        imageView.setImageDrawable(null);
        imageView.setBackgroundColor(-7829368);
    }
}
