package X;

import java.util.ArrayList;

/* renamed from: X.5zY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C130525zY {
    public static final ArrayList A01;
    public static final ArrayList A02;
    public final AnonymousClass1V8 A00;

    static {
        String[] strArr = new String[2];
        strArr[0] = "0";
        A02 = C12960it.A0m("1", strArr, 1);
        String[] strArr2 = new String[2];
        strArr2[0] = "0";
        A01 = C12960it.A0m("1", strArr2, 1);
    }

    public C130525zY(AnonymousClass3CT r11, String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy A0N = C117295Zj.A0N(A0M);
        C41141sy.A01(A0N, "action", "upi-register-vpa");
        if (C117295Zj.A1W(str, 1, false)) {
            C41141sy.A01(A0N, "device-id", str);
        }
        if (AnonymousClass3JT.A0E(str2, 1, 100000, false)) {
            C41141sy.A01(A0N, "upi-bank-info", str2);
        }
        if (str3 != null && AnonymousClass3JT.A0E(str3, 1, 10, true)) {
            C41141sy.A01(A0N, "provider-type", str3);
        }
        if (str4 != null && C117305Zk.A1Y(str4, true)) {
            C41141sy.A01(A0N, "vpa", str4);
        }
        if (str5 != null && AnonymousClass3JT.A0E(str5, 1, 100, true)) {
            C41141sy.A01(A0N, "vpa-id", str5);
        }
        A0N.A0A(str6, "default-debit", A02);
        A0N.A0A(str7, "default-credit", A01);
        this.A00 = C117295Zj.A0J(A0N, A0M, r11);
    }
}
