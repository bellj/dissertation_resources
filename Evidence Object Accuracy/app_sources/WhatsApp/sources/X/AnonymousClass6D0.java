package X;

import android.content.Context;
import com.whatsapp.R;
import java.math.BigDecimal;

/* renamed from: X.6D0  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6D0 implements AbstractC136126Lc {
    public C30821Yy A00;
    public final Context A01;
    public final AnonymousClass018 A02;
    public final AbstractC30791Yv A03;
    public final C30821Yy A04;
    public final C30821Yy A05;
    public final C125905s0 A06;

    public AnonymousClass6D0(Context context, AnonymousClass018 r2, AbstractC30791Yv r3, C30821Yy r4, C30821Yy r5, C30821Yy r6, C125905s0 r7) {
        this.A01 = context;
        this.A06 = r7;
        this.A03 = r3;
        this.A02 = r2;
        this.A00 = r4;
        this.A05 = r5;
        this.A04 = r6;
    }

    public final C127165u3 A00(String str, BigDecimal bigDecimal, int i, boolean z) {
        C30821Yy r0;
        int i2;
        if (i == 0) {
            r0 = this.A00;
        } else {
            r0 = this.A04;
        }
        BigDecimal bigDecimal2 = r0.A00;
        C125905s0 r1 = this.A06;
        int compareTo = bigDecimal.compareTo(bigDecimal2);
        if (r1 != null) {
            if (compareTo > 0 || (bigDecimal.compareTo(bigDecimal2) == 0 && str.endsWith(".") && !z)) {
                i2 = 3;
            } else {
                i2 = 0;
            }
            return new C127165u3(i2, "");
        } else if (compareTo <= 0 && (bigDecimal.compareTo(bigDecimal2) != 0 || !str.endsWith(".") || z)) {
            return new C127165u3(0, "");
        } else {
            return new C127165u3(3, C12960it.A0X(this.A01, this.A03.AAB(this.A02, bigDecimal2, 0), C12970iu.A1b(), 0, R.string.payments_send_payment_max_amount));
        }
    }
}
