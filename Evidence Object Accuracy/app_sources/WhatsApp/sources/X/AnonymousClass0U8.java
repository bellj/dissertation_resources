package X;

import android.graphics.PointF;
import android.view.animation.Interpolator;

/* renamed from: X.0U8  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0U8 {
    public float A00;
    public float A01;
    public float A02;
    public float A03;
    public int A04;
    public int A05;
    public PointF A06;
    public PointF A07;
    public Float A08;
    public Object A09;
    public final float A0A;
    public final Interpolator A0B;
    public final Interpolator A0C;
    public final Interpolator A0D;
    public final C05540Py A0E;
    public final Object A0F;

    public AnonymousClass0U8(Interpolator interpolator, C05540Py r3, Float f, Object obj, Object obj2, float f2) {
        this.A03 = -3987645.8f;
        this.A01 = -3987645.8f;
        this.A05 = 784923401;
        this.A04 = 784923401;
        this.A02 = Float.MIN_VALUE;
        this.A00 = Float.MIN_VALUE;
        this.A06 = null;
        this.A07 = null;
        this.A0E = r3;
        this.A0F = obj;
        this.A09 = obj2;
        this.A0B = interpolator;
        this.A0C = null;
        this.A0D = null;
        this.A0A = f2;
        this.A08 = f;
    }

    public AnonymousClass0U8(Interpolator interpolator, Interpolator interpolator2, C05540Py r5, Object obj, Object obj2, float f) {
        this.A03 = -3987645.8f;
        this.A01 = -3987645.8f;
        this.A05 = 784923401;
        this.A04 = 784923401;
        this.A02 = Float.MIN_VALUE;
        this.A00 = Float.MIN_VALUE;
        this.A06 = null;
        this.A07 = null;
        this.A0E = r5;
        this.A0F = obj;
        this.A09 = obj2;
        this.A0B = null;
        this.A0C = interpolator;
        this.A0D = interpolator2;
        this.A0A = f;
        this.A08 = null;
    }

    public AnonymousClass0U8(Interpolator interpolator, Interpolator interpolator2, Interpolator interpolator3, C05540Py r5, Float f, Object obj, Object obj2, float f2) {
        this.A03 = -3987645.8f;
        this.A01 = -3987645.8f;
        this.A05 = 784923401;
        this.A04 = 784923401;
        this.A02 = Float.MIN_VALUE;
        this.A00 = Float.MIN_VALUE;
        this.A06 = null;
        this.A07 = null;
        this.A0E = r5;
        this.A0F = obj;
        this.A09 = obj2;
        this.A0B = interpolator;
        this.A0C = interpolator2;
        this.A0D = interpolator3;
        this.A0A = f2;
        this.A08 = f;
    }

    public AnonymousClass0U8(Object obj) {
        this.A03 = -3987645.8f;
        this.A01 = -3987645.8f;
        this.A05 = 784923401;
        this.A04 = 784923401;
        this.A02 = Float.MIN_VALUE;
        this.A00 = Float.MIN_VALUE;
        this.A06 = null;
        this.A07 = null;
        this.A0E = null;
        this.A0F = obj;
        this.A09 = obj;
        this.A0B = null;
        this.A0C = null;
        this.A0D = null;
        this.A0A = Float.MIN_VALUE;
        this.A08 = Float.valueOf(Float.MAX_VALUE);
    }

    public float A00() {
        C05540Py r4 = this.A0E;
        if (r4 == null) {
            return 1.0f;
        }
        float f = this.A00;
        if (f != Float.MIN_VALUE) {
            return f;
        }
        Float f2 = this.A08;
        if (f2 == null) {
            this.A00 = 1.0f;
            return 1.0f;
        }
        float A01 = A01() + ((f2.floatValue() - this.A0A) / (r4.A00 - r4.A02));
        this.A00 = A01;
        return A01;
    }

    public float A01() {
        C05540Py r3 = this.A0E;
        if (r3 == null) {
            return 0.0f;
        }
        float f = this.A02;
        if (f != Float.MIN_VALUE) {
            return f;
        }
        float f2 = this.A0A;
        float f3 = r3.A02;
        float f4 = (f2 - f3) / (r3.A00 - f3);
        this.A02 = f4;
        return f4;
    }

    public boolean A02() {
        return this.A0B == null && this.A0C == null && this.A0D == null;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Keyframe{startValue=");
        sb.append(this.A0F);
        sb.append(", endValue=");
        sb.append(this.A09);
        sb.append(", startFrame=");
        sb.append(this.A0A);
        sb.append(", endFrame=");
        sb.append(this.A08);
        sb.append(", interpolator=");
        sb.append(this.A0B);
        sb.append('}');
        return sb.toString();
    }
}
