package X;

import android.content.Context;
import android.os.Build;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.FrameLayout;
import com.facebook.redex.ViewOnClickCListenerShape3S0200000_I1_1;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import java.util.List;

/* renamed from: X.2gq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54512gq extends AnonymousClass02M implements Filterable {
    public int A00;
    public int A01 = -1;
    public int A02;
    public C52862bo A03;
    public C71283cg A04;
    public String A05 = "";
    public List A06 = C12960it.A0l();
    public List A07 = C12960it.A0l();
    public boolean A08;
    public final int A09;
    public final int A0A;
    public final C15570nT A0B;
    public final C15610nY A0C;
    public final AnonymousClass1J1 A0D;
    public final AnonymousClass018 A0E;
    public final C14850m9 A0F;
    public final AbstractC37031lC A0G;
    public final AnonymousClass13H A0H;
    public final AnonymousClass12F A0I;

    public C54512gq(Context context, C15570nT r4, C15610nY r5, C21270x9 r6, AnonymousClass018 r7, C14850m9 r8, AbstractC37031lC r9, AnonymousClass13H r10, AnonymousClass12F r11, boolean z, boolean z2) {
        int i;
        this.A0F = r8;
        this.A0H = r10;
        this.A0B = r4;
        this.A0C = r5;
        this.A0E = r7;
        this.A0I = r11;
        this.A0D = r6.A03(context, "mentions-adapter");
        this.A0G = r9;
        if (z) {
            this.A00 = AnonymousClass00T.A00(context, R.color.mention_primary_text_color_dark_theme);
            this.A02 = AnonymousClass00T.A00(context, R.color.mention_pushname_dark_theme);
            i = R.color.mention_divider_dark_theme;
        } else {
            this.A00 = AnonymousClass00T.A00(context, R.color.list_item_title);
            this.A02 = AnonymousClass00T.A00(context, R.color.list_item_info);
            i = R.color.divider_gray;
        }
        this.A09 = AnonymousClass00T.A00(context, i);
        this.A0A = (int) context.getResources().getDimension(R.dimen.mention_picker_divider_padding);
        this.A08 = z2;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A07.size();
    }

    public final CharSequence A0E(String str) {
        String lowerCase = str.toLowerCase();
        String str2 = this.A05;
        int indexOf = lowerCase.indexOf(str2);
        if (str2.length() <= 0 || indexOf < 0) {
            return str;
        }
        SpannableStringBuilder A0J = C12990iw.A0J(str);
        A0J.setSpan(new StyleSpan(1), indexOf, this.A05.length() + indexOf, 33);
        return A0J;
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r8, int i) {
        String str;
        String A01;
        C55032hg r82 = (C55032hg) r8;
        C15370n3 r3 = (C15370n3) this.A07.get(i);
        C28801Pb r4 = r82.A04;
        AnonymousClass13H r2 = this.A0H;
        StringBuilder A0h = C12960it.A0h();
        String str2 = "";
        if (AnonymousClass13H.A05) {
            str = "⁨";
        } else {
            str = str2;
        }
        A0h.append(str);
        if (r3.A0L()) {
            A01 = C15610nY.A02(r3, false);
        } else if (r2.A03.A07(604)) {
            A01 = r2.A02.A0B(r3, 1, false);
        } else if (!TextUtils.isEmpty(r3.A0K)) {
            A01 = r3.A0K;
        } else {
            A01 = C248917h.A01(r3);
        }
        A0h.append(A01);
        if (AnonymousClass13H.A06) {
            str2 = "⁩";
        }
        r4.A09(null, A0E(C12960it.A0d(str2, A0h)));
        r4.A05(r3.A0M() ? 1 : 0);
        this.A0D.A06(r82.A05, r3);
        r82.A02.setOnClickListener(new ViewOnClickCListenerShape3S0200000_I1_1(this, 21, r3));
        View view = r82.A01;
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) view.getLayoutParams();
        int i2 = this.A01;
        int i3 = Build.VERSION.SDK_INT;
        if (i != i2) {
            int i4 = this.A0A;
            if (i3 >= 17) {
                layoutParams.setMarginStart(i4);
            } else {
                layoutParams.setMargins(i4, 0, 0, 0);
            }
        } else if (i3 >= 17) {
            layoutParams.setMarginStart(0);
        } else {
            layoutParams.setMargins(0, 0, 0, 0);
        }
        view.setBackgroundColor(this.A09);
        view.setLayoutParams(layoutParams);
        if (this.A08) {
            if (i == C12980iv.A0C(this.A07)) {
                view.setVisibility(8);
            } else {
                view.setVisibility(0);
            }
        }
        if (!r3.A0L()) {
            C15610nY r22 = this.A0C;
            if (C15610nY.A03(r3) && !this.A0F.A07(604)) {
                TextEmojiLabel textEmojiLabel = r82.A03;
                textEmojiLabel.setText(A0E(r22.A09(r3)));
                textEmojiLabel.setVisibility(0);
                return;
            }
        }
        r82.A03.setVisibility(8);
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        return new C55032hg((FrameLayout) C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.mentions_row), this);
    }

    @Override // android.widget.Filterable
    public Filter getFilter() {
        C52862bo r0 = this.A03;
        if (r0 != null) {
            return r0;
        }
        C52862bo r02 = new C52862bo(this);
        this.A03 = r02;
        return r02;
    }
}
