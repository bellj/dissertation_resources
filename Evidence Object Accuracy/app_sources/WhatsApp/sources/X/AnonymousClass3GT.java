package X;

import java.lang.reflect.Method;

/* renamed from: X.3GT  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3GT {
    public static final Method A00;
    public static final Method A01;

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0036 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    static {
        /*
            java.lang.Class<java.lang.String> r9 = java.lang.String.class
            int r7 = android.os.Build.VERSION.SDK_INT
            r4 = 6
            r5 = 0
            r6 = 24
            java.lang.String r3 = "JobSchedulerCompat"
            r8 = 0
            r0 = 4
            if (r7 < r6) goto L_0x0031
            java.lang.Class[] r2 = new java.lang.Class[r0]     // Catch: NoSuchMethodException -> 0x0026
            java.lang.Class<android.app.job.JobInfo> r0 = android.app.job.JobInfo.class
            X.C12990iw.A1P(r0, r9, r2)     // Catch: NoSuchMethodException -> 0x0026
            r1 = 2
            java.lang.Class r0 = java.lang.Integer.TYPE     // Catch: NoSuchMethodException -> 0x0026
            r2[r1] = r0     // Catch: NoSuchMethodException -> 0x0026
            r0 = 3
            r2[r0] = r9     // Catch: NoSuchMethodException -> 0x0026
            java.lang.Class<android.app.job.JobScheduler> r1 = android.app.job.JobScheduler.class
            java.lang.String r0 = "scheduleAsPackage"
            java.lang.reflect.Method r0 = r1.getDeclaredMethod(r0, r2)     // Catch: NoSuchMethodException -> 0x0026
            goto L_0x0032
        L_0x0026:
            boolean r0 = android.util.Log.isLoggable(r3, r4)
            if (r0 == 0) goto L_0x0031
            java.lang.String r0 = "No scheduleAsPackage method available, falling back to schedule"
            android.util.Log.e(r3, r0)
        L_0x0031:
            r0 = r8
        L_0x0032:
            X.AnonymousClass3GT.A00 = r0
            if (r7 < r6) goto L_0x004c
            java.lang.Class<android.os.UserHandle> r2 = android.os.UserHandle.class
            java.lang.String r1 = "myUserId"
            java.lang.Class[] r0 = new java.lang.Class[r5]     // Catch: NoSuchMethodException -> 0x0041
            java.lang.reflect.Method r8 = r2.getDeclaredMethod(r1, r0)     // Catch: NoSuchMethodException -> 0x0041
            goto L_0x004c
        L_0x0041:
            boolean r0 = android.util.Log.isLoggable(r3, r4)
            if (r0 == 0) goto L_0x004c
            java.lang.String r0 = "No myUserId method available"
            android.util.Log.e(r3, r0)
        L_0x004c:
            X.AnonymousClass3GT.A01 = r8
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3GT.<clinit>():void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0053 A[Catch: IllegalAccessException | InvocationTargetException -> 0x0057, TryCatch #0 {IllegalAccessException | InvocationTargetException -> 0x0057, blocks: (B:17:0x003f, B:19:0x0053), top: B:26:0x003f }] */
    /* JADX WARNING: Removed duplicated region for block: B:30:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A00(android.app.job.JobInfo r7, android.content.Context r8) {
        /*
            java.lang.String r0 = "jobscheduler"
            java.lang.Object r4 = r8.getSystemService(r0)
            android.app.job.JobScheduler r4 = (android.app.job.JobScheduler) r4
            java.lang.reflect.Method r6 = X.AnonymousClass3GT.A00
            if (r6 == 0) goto L_0x0061
            java.lang.String r0 = "android.permission.UPDATE_DEVICE_STATS"
            int r0 = r8.checkSelfPermission(r0)
            if (r0 != 0) goto L_0x0061
            java.lang.reflect.Method r2 = X.AnonymousClass3GT.A01
            r0 = 0
            if (r2 == 0) goto L_0x0039
            java.lang.Class<android.os.UserHandle> r1 = android.os.UserHandle.class
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch: IllegalAccessException | InvocationTargetException -> 0x002a
            java.lang.Object r0 = r2.invoke(r1, r0)     // Catch: IllegalAccessException | InvocationTargetException -> 0x002a
            java.lang.Number r0 = (java.lang.Number) r0     // Catch: IllegalAccessException | InvocationTargetException -> 0x002a
            if (r0 == 0) goto L_0x0039
            int r5 = r0.intValue()     // Catch: IllegalAccessException | InvocationTargetException -> 0x002a
            goto L_0x003a
        L_0x002a:
            r2 = move-exception
            r0 = 6
            java.lang.String r1 = "JobSchedulerCompat"
            boolean r0 = android.util.Log.isLoggable(r1, r0)
            if (r0 == 0) goto L_0x0039
            java.lang.String r0 = "myUserId invocation illegal"
            android.util.Log.e(r1, r0, r2)
        L_0x0039:
            r5 = 0
        L_0x003a:
            java.lang.String r3 = "com.google.android.gms"
            java.lang.String r2 = "DispatchAlarm"
            r0 = 4
            java.lang.Object[] r1 = new java.lang.Object[r0]     // Catch: IllegalAccessException | InvocationTargetException -> 0x0057
            X.C12990iw.A1P(r7, r3, r1)     // Catch: IllegalAccessException | InvocationTargetException -> 0x0057
            r0 = 2
            X.C12960it.A1P(r1, r5, r0)     // Catch: IllegalAccessException | InvocationTargetException -> 0x0057
            r0 = 3
            r1[r0] = r2     // Catch: IllegalAccessException | InvocationTargetException -> 0x0057
            java.lang.Object r0 = r6.invoke(r4, r1)     // Catch: IllegalAccessException | InvocationTargetException -> 0x0057
            java.lang.Number r0 = (java.lang.Number) r0     // Catch: IllegalAccessException | InvocationTargetException -> 0x0057
            if (r0 == 0) goto L_0x0064
            r0.intValue()     // Catch: IllegalAccessException | InvocationTargetException -> 0x0057
            return
        L_0x0057:
            r1 = move-exception
            java.lang.String r0 = "error calling scheduleAsPackage"
            android.util.Log.e(r2, r0, r1)
            r4.schedule(r7)
            return
        L_0x0061:
            r4.schedule(r7)
        L_0x0064:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3GT.A00(android.app.job.JobInfo, android.content.Context):void");
    }
}
