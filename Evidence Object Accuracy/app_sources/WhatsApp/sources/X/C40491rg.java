package X;

import android.util.Pair;
import java.util.HashMap;
import java.util.TreeSet;

/* renamed from: X.1rg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C40491rg implements Comparable {
    public int A00 = 0;
    public final C15570nT A01;
    public final String A02;
    public final HashMap A03 = new HashMap();
    public final TreeSet A04 = new TreeSet();

    public C40491rg(C15570nT r2, AnonymousClass2VH r3, String str) {
        this.A01 = r2;
        this.A02 = str;
        A00(r3);
    }

    public void A00(AnonymousClass2VH r5) {
        AbstractC14640lm r3 = r5.A04;
        Pair pair = new Pair(r3, Long.valueOf(r5.A00));
        HashMap hashMap = this.A03;
        if (!hashMap.containsKey(pair)) {
            hashMap.put(pair, r5);
            this.A04.add(r5);
            if (this.A01.A0F(r3)) {
                this.A00++;
            }
        }
    }

    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(Object obj) {
        C40491rg r8 = (C40491rg) obj;
        boolean z = false;
        if (this.A00 > 0) {
            z = true;
        }
        boolean z2 = false;
        if (r8.A00 > 0) {
            z2 = true;
        }
        if (z != z2) {
            return z ? 1 : -1;
        }
        TreeSet treeSet = this.A04;
        TreeSet treeSet2 = r8.A04;
        int i = (((long) treeSet.size()) > ((long) treeSet2.size()) ? 1 : (((long) treeSet.size()) == ((long) treeSet2.size()) ? 0 : -1));
        if (i != 0) {
            return i;
        }
        int i2 = (((AnonymousClass2VH) treeSet.first()).A02 > ((AnonymousClass2VH) treeSet2.first()).A02 ? 1 : (((AnonymousClass2VH) treeSet.first()).A02 == ((AnonymousClass2VH) treeSet2.first()).A02 ? 0 : -1));
        if (i2 == 0) {
            return this.A02.compareTo(r8.A02);
        }
        return i2;
    }
}
