package X;

/* renamed from: X.4Hj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C88854Hj {
    public static final C78603pB A00;
    public static final C78603pB A01;
    public static final C78603pB A02;
    public static final C78603pB A03;
    public static final C78603pB A04;
    public static final C78603pB A05;
    public static final C78603pB[] A06;

    static {
        C78603pB r9 = new C78603pB("auth_blockstore", 3);
        A00 = r9;
        C78603pB r8 = new C78603pB("blockstore_data_transfer", 1);
        A01 = r8;
        C78603pB r7 = new C78603pB("blockstore_notify_app_restore", 1);
        A02 = r7;
        C78603pB r6 = new C78603pB("blockstore_store_bytes_with_options", 1);
        A03 = r6;
        C78603pB r3 = new C78603pB("blockstore_is_end_to_end_encryption_available", 1);
        A04 = r3;
        C78603pB r2 = new C78603pB("blockstore_enable_cloud_backup", 1);
        A05 = r2;
        C78603pB[] r1 = new C78603pB[6];
        C72453ed.A1F(r9, r8, r7, r6, r1);
        r1[4] = r3;
        r1[5] = r2;
        A06 = r1;
    }
}
