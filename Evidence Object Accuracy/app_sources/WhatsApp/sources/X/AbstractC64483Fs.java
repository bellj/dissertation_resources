package X;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.3Fs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC64483Fs {
    public static void A00(AnonymousClass1IS r2, AnonymousClass1Z8 r3, AbstractMap abstractMap) {
        abstractMap.put("message_id", r2.A01);
        abstractMap.put("action_name", r3.A00);
        AbstractC14640lm r0 = r2.A00;
        if (r0 != null) {
            abstractMap.put("chat_id", r0.getRawString());
        }
    }

    public String A01() {
        if (this instanceof C61002zF) {
            return "wa_payment_transaction_details";
        }
        if (this instanceof C60992zE) {
            return "wa_payment_fbpin_reset";
        }
        if (this instanceof C60982zD) {
            return "wa_payment_learn_more";
        }
        if (this instanceof C60972zC) {
            return !(((C60972zC) this) instanceof C61042zJ) ? "novi_view_code" : "novi_tpp_complete_transaction";
        }
        if (this instanceof C61032zI) {
            return "novi_report_transaction";
        }
        if (this instanceof C61022zH) {
            return "novi_login";
        }
        if (this instanceof C61012zG) {
            return "novi_hub";
        }
        if (this instanceof C60962zB) {
            return "novi_view_bank_detail";
        }
        if (!(this instanceof C61062zL)) {
            return "address_message";
        }
        return "galaxy_message";
    }

    public String A02(Context context, AnonymousClass1Z8 r5) {
        int i;
        if (this instanceof C61002zF) {
            i = R.string.native_flow_view_payment;
        } else if (this instanceof C60992zE) {
            i = R.string.native_flow_reset_pin;
        } else if (this instanceof C60982zD) {
            i = R.string.native_flow_learn_more;
        } else if (!(this instanceof C60972zC)) {
            if (this instanceof C61032zI) {
                i = R.string.native_flow_view_report_transaction;
            } else if (this instanceof C61022zH) {
                i = R.string.native_flow_view_login;
            } else if (this instanceof C61012zG) {
                i = R.string.native_flow_view_account_label;
            } else if (this instanceof C60962zB) {
                i = R.string.native_flow_view_bank_account;
            } else if (!(this instanceof C61062zL)) {
                i = R.string.native_flow_view_address_capture;
            } else {
                Map A01 = C65053Hy.A01(r5.A01);
                return A01.containsKey("flow_cta") ? A01.get("flow_cta").toString() : "";
            }
        } else if (!(((C60972zC) this) instanceof C61042zJ)) {
            i = R.string.native_flow_view_withdraw_code;
        } else {
            i = R.string.native_flow_view_complete_tpp_transaction;
        }
        return context.getString(i);
    }

    public void A03(Activity activity, AnonymousClass1IS r6, AnonymousClass1Z8 r7, Class cls) {
        Intent intent;
        String str;
        String str2;
        Intent intent2;
        if (this instanceof AbstractC60952zA) {
            return;
        }
        if (!(this instanceof C61002zF)) {
            if (this instanceof C60992zE) {
                intent2 = C12990iw.A0D(activity, cls);
                AnonymousClass009.A05(r7);
                intent2.putExtra("screen_name", "brpay_p_pin_change_verify");
            } else if (this instanceof C60982zD) {
                intent2 = C12990iw.A0D(activity, cls);
                AnonymousClass009.A05(r7);
                String str3 = r7.A01;
                if (TextUtils.isEmpty(str3)) {
                    str3 = "{}";
                }
                String optString = C13000ix.A05(str3).optString("url");
                if (TextUtils.isEmpty(optString)) {
                    str2 = "[NFM]: ConversationRow -- NFM url is unavailable to redirect.";
                    Log.e(str2);
                    return;
                }
                intent2.putExtra("webview_url", optString);
                intent2.putExtra("webview_hide_url", true);
                intent2.putExtra("webview_javascript_enabled", true);
                intent2.putExtra("webview_avoid_external", true);
            } else if (this instanceof C60972zC) {
                if (!(((C60972zC) this) instanceof C61042zJ)) {
                    intent = C12990iw.A0D(activity, cls);
                    AnonymousClass009.A05(r7);
                    String str4 = r7.A01;
                    if (TextUtils.isEmpty(str4)) {
                        str4 = "{}";
                    }
                    str = C13000ix.A05(str4).optString("id");
                    if (TextUtils.isEmpty(str)) {
                        str2 = "[PAY]: ConversationRow -- NFM transaction-id is unavailable";
                    }
                    intent.putExtra("referral_screen", "chat");
                    intent.putExtra("extra_transaction_id", str);
                    activity.startActivity(intent);
                }
                Intent A0D = C12990iw.A0D(activity, cls);
                HashMap A11 = C12970iu.A11();
                A11.put("login_entry_point", "novi_care_navigate_to_payment");
                A0D.putExtra("screen_params", A11);
                HashMap A112 = C12970iu.A11();
                A00(r6, r7, A112);
                A0D.putExtra("finish_activity_result", A112);
                A0D.putExtra("screen_name", "novipay_p_login_password");
                String str5 = r7.A01;
                if (TextUtils.isEmpty(str5)) {
                    str5 = "{}";
                }
                String optString2 = C13000ix.A05(str5).optString("id");
                if (TextUtils.isEmpty(optString2)) {
                    str2 = "[PAY]: ConversationRow -- NFM tpp transaction-id is unavailable";
                } else {
                    A0D.putExtra("extra_tpp_transaction_request_id", optString2);
                    activity.startActivityForResult(A0D, 903);
                    activity.overridePendingTransition(R.anim.abc_slide_in_bottom, R.anim.abc_fade_out);
                    return;
                }
                Log.e(str2);
                return;
            } else if (this instanceof C61032zI) {
                Intent A0D2 = C12990iw.A0D(activity, cls);
                A0D2.putExtra("report_transaction_origin", "report_transaction_nfm");
                HashMap A113 = C12970iu.A11();
                A00(r6, r7, A113);
                A0D2.putExtra("screen_params", A113);
                A0D2.putExtra("screen_name", "novipay_p_report_transaction");
                activity.startActivityForResult(A0D2, 901);
                return;
            } else if (this instanceof C61022zH) {
                Intent A0D3 = C12990iw.A0D(activity, cls);
                HashMap A114 = C12970iu.A11();
                A114.put("login_entry_point", "novi_care_navigate_to_login");
                A00(r6, r7, A114);
                A0D3.putExtra("screen_params", A114);
                A0D3.putExtra("screen_name", "novipay_p_login_password");
                activity.startActivityForResult(A0D3, 900);
                return;
            } else if (!(this instanceof C61012zG)) {
                intent2 = C12990iw.A0D(activity, cls);
                AnonymousClass009.A05(r7);
                String str6 = r7.A01;
                if (TextUtils.isEmpty(str6)) {
                    str6 = "{}";
                }
                String optString3 = C13000ix.A05(str6).optString("id");
                if (TextUtils.isEmpty(optString3)) {
                    str2 = "[PAY]: ConversationRow -- NFM novi bank account or card id unavailable";
                    Log.e(str2);
                    return;
                }
                intent2.putExtra("extra_bank_account_or_card_credential_id", optString3);
            } else {
                Intent A0D4 = C12990iw.A0D(activity, cls);
                HashMap A115 = C12970iu.A11();
                A115.put("login_entry_point", "novi_care_navigate_to_hub");
                A0D4.putExtra("screen_params", A115);
                A0D4.putExtra("screen_name", "novipay_p_login_password");
                activity.startActivity(A0D4);
                activity.overridePendingTransition(R.anim.abc_slide_in_bottom, R.anim.abc_fade_out);
                return;
            }
            activity.startActivity(intent2);
            return;
        }
        intent = C12990iw.A0D(activity, cls);
        AnonymousClass009.A05(r7);
        String str7 = r7.A01;
        if (TextUtils.isEmpty(str7)) {
            str7 = "{}";
        }
        str = C13000ix.A05(str7).optString("id");
        if (TextUtils.isEmpty(str)) {
            str2 = "[NFM]: WaViewTransactionAction - ConversationRow -- transaction-id is unavailable";
            Log.e(str2);
            return;
        }
        intent.putExtra("referral_screen", "chat");
        intent.putExtra("extra_transaction_id", str);
        activity.startActivity(intent);
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0069  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A04(android.content.Intent r9, X.C15650ng r10, X.C16120oU r11, X.AbstractC14440lR r12, int r13) {
        /*
            r8 = this;
            if (r9 == 0) goto L_0x0068
            android.os.Bundle r0 = r9.getExtras()
            if (r0 == 0) goto L_0x0068
            android.os.Bundle r2 = r9.getExtras()
            java.lang.String r1 = "screen_params"
            java.io.Serializable r0 = r2.getSerializable(r1)
            boolean r0 = r0 instanceof java.util.Map
            r7 = 0
            if (r0 == 0) goto L_0x0084
            java.io.Serializable r1 = r2.getSerializable(r1)
            java.util.Map r1 = (java.util.Map) r1
            if (r1 == 0) goto L_0x0089
            java.lang.String r0 = "message_id"
            java.lang.String r2 = X.C12970iu.A0t(r0, r1)
            java.lang.String r0 = "chat_id"
            java.lang.String r3 = X.C12970iu.A0t(r0, r1)
            java.lang.String r0 = "action_name"
            java.lang.String r7 = X.C12970iu.A0t(r0, r1)
        L_0x0031:
            boolean r0 = android.text.TextUtils.isEmpty(r7)
            r6 = -1
            if (r0 != 0) goto L_0x0069
            X.30q r5 = new X.30q
            r5.<init>()
            java.lang.Integer r0 = X.C12980iv.A0j()
            r5.A01 = r0
            java.lang.Integer r4 = X.C12960it.A0V()
            r5.A03 = r4
            java.lang.Integer r0 = X.C12970iu.A0g()
            r5.A02 = r0
            java.lang.String r0 = "{  \"cta\":\""
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            r1.append(r7)
            java.lang.String r0 = "\"}"
            java.lang.String r0 = X.C12960it.A0d(r0, r1)
            r5.A05 = r0
            if (r13 == r6) goto L_0x006c
            r5.A02 = r4
            r11.A05(r5)
        L_0x0068:
            return
        L_0x0069:
            if (r13 != r6) goto L_0x0068
            goto L_0x006f
        L_0x006c:
            r11.A05(r5)
        L_0x006f:
            boolean r0 = android.text.TextUtils.isEmpty(r2)
            if (r0 != 0) goto L_0x0068
            boolean r0 = android.text.TextUtils.isEmpty(r3)
            if (r0 != 0) goto L_0x0068
            X.2il r0 = new X.2il
            r0.<init>(r10, r2, r3)
            r12.Ab2(r0)
            return
        L_0x0084:
            java.lang.String r0 = "missing screen_params in result intent"
            com.whatsapp.util.Log.e(r0)
        L_0x0089:
            r2 = r7
            r3 = r7
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC64483Fs.A04(android.content.Intent, X.0ng, X.0oU, X.0lR, int):void");
    }

    public boolean A05(C57442n3 r3, AnonymousClass39x r4) {
        return r4 == AnonymousClass39x.A01 || r4 == AnonymousClass39x.A05 || r4 == AnonymousClass39x.A07 || r4 == AnonymousClass39x.A03 || r4 == AnonymousClass39x.A02;
    }
}
