package X;

import android.text.TextUtils;
import java.util.Iterator;

/* renamed from: X.5xj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129445xj {
    public final /* synthetic */ C118125bJ A00;

    public C129445xj(C118125bJ r1) {
        this.A00 = r1;
    }

    public void A00() {
        boolean z;
        C118125bJ r2 = this.A00;
        Iterator it = r2.A0C.iterator();
        while (true) {
            if (it.hasNext()) {
                if (TextUtils.isEmpty(((C127455uW) it.next()).A02)) {
                    z = false;
                    break;
                }
            } else {
                z = true;
                break;
            }
        }
        C27691It r22 = r2.A0B;
        int i = 4;
        if (z) {
            i = 3;
        }
        r22.A0B(new C127765v1(i));
    }

    public void A01(String str, int i) {
        C118125bJ r5 = this.A00;
        C127995vO r4 = ((C127455uW) r5.A0C.get(i)).A00;
        AnonymousClass610 r3 = new AnonymousClass610("TEXT_INPUT_CLICK", "TEXT_INPUT", str);
        String str2 = r4.A01;
        C128365vz r2 = r3.A00;
        r2.A0K = str2;
        r2.A0k = r4.A03;
        r2.A06 = Long.valueOf((long) (i + 1));
        r5.A04(r3);
    }
}
