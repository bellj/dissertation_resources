package X;

import com.whatsapp.util.Log;
import java.io.Closeable;

/* renamed from: X.3cO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C71103cO implements Closeable {
    public boolean A00;
    public final C26011Br A01;

    public C71103cO(C26011Br r1) {
        this.A01 = r1;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        boolean z;
        C26011Br r2;
        int i;
        AnonymousClass3LK r1;
        synchronized (this) {
            z = true;
            if (!this.A00) {
                this.A00 = true;
            } else {
                z = false;
            }
        }
        if (z) {
            C26011Br r4 = this.A01;
            StringBuilder A0k = C12960it.A0k("svc-client/onSessionClosed; service=");
            String str = r4.A08;
            String A0d = C12960it.A0d(str, A0k);
            if (!(this instanceof AnonymousClass351)) {
                r2 = r4;
            } else {
                r2 = ((AnonymousClass351) this).A00;
            }
            if (r2 == r4) {
                synchronized (r4) {
                    int i2 = r4.A00;
                    if (i2 <= 0) {
                        Log.e(C12960it.A0e(", imbalanced ref_cnt=", C12960it.A0j(A0d), i2));
                        AbstractC15710nm r3 = r4.A05;
                        StringBuilder A0h = C12960it.A0h();
                        A0h.append("name=");
                        A0h.append(str);
                        A0h.append(", counter=");
                        r3.AaV("svc-client-reference-counter-imbalance", C12960it.A0f(A0h, r4.A00), false);
                        r4.A00 = 0;
                        i = 0;
                    } else {
                        i = i2 - 1;
                        r4.A00 = i;
                    }
                    r1 = null;
                    if (i == 0) {
                        AnonymousClass3LK r0 = r4.A01;
                        r4.A01 = null;
                        r1 = r0;
                    }
                }
                if (r1 != null) {
                    r1.A01(false);
                    return;
                }
                return;
            }
            throw C12970iu.A0f(C12960it.A0d(r2.A08, C12960it.A0k("A session from a different client has been provided. Client: ")));
        }
    }
}
