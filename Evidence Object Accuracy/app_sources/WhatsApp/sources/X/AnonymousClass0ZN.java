package X;

import android.view.View;
import java.util.ArrayList;

/* renamed from: X.0ZN  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0ZN implements AbstractC018608t {
    public final /* synthetic */ View A00;
    public final /* synthetic */ AnonymousClass0EH A01;
    public final /* synthetic */ ArrayList A02;

    @Override // X.AbstractC018608t
    public void AXr(AnonymousClass072 r1) {
    }

    @Override // X.AbstractC018608t
    public void AXs(AnonymousClass072 r1) {
    }

    @Override // X.AbstractC018608t
    public void AXt(AnonymousClass072 r1) {
    }

    public AnonymousClass0ZN(View view, AnonymousClass0EH r2, ArrayList arrayList) {
        this.A01 = r2;
        this.A00 = view;
        this.A02 = arrayList;
    }

    @Override // X.AbstractC018608t
    public void AXq(AnonymousClass072 r6) {
        r6.A09(this);
        this.A00.setVisibility(8);
        ArrayList arrayList = this.A02;
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            ((View) arrayList.get(i)).setVisibility(0);
        }
    }
}
