package X;

import android.view.View;
import com.whatsapp.R;

/* renamed from: X.31w  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C617231w extends AbstractC51682Vy {
    public final View A00 = AnonymousClass028.A0D(this.A0H, R.id.search_no_matches);

    public C617231w(View view) {
        super(view);
    }

    public void A0E(Boolean bool) {
        View view;
        int dimensionPixelSize;
        if (bool == null || bool.booleanValue()) {
            view = this.A0H;
            dimensionPixelSize = C12960it.A09(view).getDimensionPixelSize(R.dimen.search_no_result_tall_top_margin);
        } else {
            view = this.A0H;
            dimensionPixelSize = C12960it.A09(view).getDimensionPixelSize(R.dimen.search_no_result_short_top_margin) - C12960it.A09(view).getDimensionPixelSize(R.dimen.search_header_bottom_margin);
        }
        this.A00.setPadding(view.getPaddingLeft(), dimensionPixelSize, view.getPaddingRight(), view.getPaddingBottom());
    }
}
