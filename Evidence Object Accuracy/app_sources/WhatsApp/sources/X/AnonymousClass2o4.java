package X;

import android.view.View;
import android.view.animation.Animation;

/* renamed from: X.2o4  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2o4 extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ AnonymousClass2xV A00;
    public final /* synthetic */ AnonymousClass4KL A01;

    public AnonymousClass2o4(AnonymousClass2xV r1, AnonymousClass4KL r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        AnonymousClass2xV r3 = this.A00;
        AnonymousClass4KL r2 = this.A01;
        View view = r3.A00;
        AnonymousClass009.A03(view);
        view.setVisibility(8);
        r3.A04.removeView(r3.A00);
        r3.A00 = null;
        r2.A00.A01 = null;
    }
}
