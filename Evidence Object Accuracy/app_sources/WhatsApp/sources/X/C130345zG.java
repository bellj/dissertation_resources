package X;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.whatsapp.R;

/* renamed from: X.5zG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130345zG {
    public static String A00(Context context, int i) {
        int i2;
        if (i != 1) {
            if (!(i == 2 || i == 9)) {
                if (i == 10) {
                    i2 = R.string.transaction_detail_requester_label;
                } else if (i == 20 || i == 40) {
                    i2 = R.string.transaction_detail_requestee_label;
                } else if (i == 100) {
                    i2 = R.string.transaction_detail_merchant_receiver_label;
                } else if (i != 200) {
                    return "";
                }
            }
            i2 = R.string.transaction_detail_sender_label;
        } else {
            i2 = R.string.transaction_detail_receiver_label;
        }
        return context.getString(i2);
    }

    public static void A01(Context context, AnonymousClass1IR r4, AbstractC16830pp r5, String str, String str2, int i) {
        if (r5 != null) {
            AbstractC14640lm r2 = r4.A0C;
            Intent A0D = C12990iw.A0D(context, r5.AFc());
            C117305Zk.A11(A0D, r4, r2);
            A0D.putExtra("extra_transaction_detail_data", r4);
            A0D.putExtra("referral_screen", str2);
            if (!TextUtils.isEmpty(str)) {
                A0D.putExtra("extra_origin_screen", str);
            }
            A0D.putExtra("extra_payment_flow_entry_point", i);
            context.startActivity(A0D);
        }
    }
}
