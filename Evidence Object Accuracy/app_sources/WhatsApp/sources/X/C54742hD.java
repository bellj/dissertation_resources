package X;

import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.gallery.MediaGalleryActivity;

/* renamed from: X.2hD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54742hD extends AbstractC05270Ox {
    public final /* synthetic */ MediaGalleryActivity A00;

    public C54742hD(MediaGalleryActivity mediaGalleryActivity) {
        this.A00 = mediaGalleryActivity;
    }

    @Override // X.AbstractC05270Ox
    public void A00(RecyclerView recyclerView, int i) {
        MediaGalleryActivity mediaGalleryActivity = this.A00;
        MenuItem menuItem = mediaGalleryActivity.A04;
        if (menuItem != null && menuItem.isActionViewExpanded() && mediaGalleryActivity.getCurrentFocus() != null) {
            InputMethodManager A0Q = ((ActivityC13810kN) mediaGalleryActivity).A08.A0Q();
            AnonymousClass009.A05(A0Q);
            A0Q.hideSoftInputFromWindow(recyclerView.getWindowToken(), 2);
        }
    }
}
