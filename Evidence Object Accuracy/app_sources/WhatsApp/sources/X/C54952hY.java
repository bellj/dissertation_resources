package X;

import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.mediacomposer.doodle.shapepicker.ShapeItemView;

/* renamed from: X.2hY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54952hY extends AnonymousClass03U {
    public AbstractC470728v A00;
    public ShapeItemView A01;
    public final /* synthetic */ AnonymousClass1KX A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C54952hY(View view, AnonymousClass1KX r4) {
        super(view);
        this.A02 = r4;
        ShapeItemView shapeItemView = (ShapeItemView) view.findViewById(R.id.shape_item_view);
        this.A01 = shapeItemView;
        ViewGroup.LayoutParams layoutParams = shapeItemView.getLayoutParams();
        layoutParams.height = -1;
        layoutParams.width = -1;
        this.A01.setLayoutParams(layoutParams);
        C12960it.A0x(view, this, 45);
    }
}
