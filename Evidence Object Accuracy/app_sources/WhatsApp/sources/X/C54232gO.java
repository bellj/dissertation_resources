package X;

import android.text.TextUtils;
import android.view.ViewGroup;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.invites.InviteGroupParticipantsActivity;
import java.util.List;

/* renamed from: X.2gO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54232gO extends AnonymousClass02M {
    public List A00;
    public final /* synthetic */ InviteGroupParticipantsActivity A01;

    public /* synthetic */ C54232gO(InviteGroupParticipantsActivity inviteGroupParticipantsActivity) {
        this.A01 = inviteGroupParticipantsActivity;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return C12970iu.A09(this.A00);
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r7, int i) {
        int i2;
        C75433jq r72 = (C75433jq) r7;
        C15370n3 r4 = (C15370n3) this.A00.get(i);
        InviteGroupParticipantsActivity inviteGroupParticipantsActivity = this.A01;
        TextView textView = r72.A01;
        if (!TextUtils.isEmpty(r4.A0K)) {
            textView.setText(r4.A0K);
        } else if (r4.A0L()) {
            textView.setText(C15610nY.A02(r4, false));
            textView.setSingleLine(false);
        } else {
            String A09 = inviteGroupParticipantsActivity.A09.A09(C15370n3.A02(r4));
            if (!TextUtils.isEmpty(A09)) {
                textView.setSingleLine(false);
            } else if (!TextUtils.isEmpty(r4.A0U)) {
                A09 = inviteGroupParticipantsActivity.A04.A09(r4);
                textView.setSingleLine(false);
                i2 = R.color.secondary_text;
                C12960it.A0s(inviteGroupParticipantsActivity, textView, i2);
                textView.setText(A09);
                inviteGroupParticipantsActivity.A05.A07(r72.A00, r4, false);
            } else {
                A09 = inviteGroupParticipantsActivity.A08.A0G(C248917h.A01(r4));
                textView.setSingleLine(true);
            }
            i2 = R.color.primary_text;
            C12960it.A0s(inviteGroupParticipantsActivity, textView, i2);
            textView.setText(A09);
            inviteGroupParticipantsActivity.A05.A07(r72.A00, r4, false);
        }
        C12960it.A0s(inviteGroupParticipantsActivity, textView, R.color.primary_text);
        inviteGroupParticipantsActivity.A05.A07(r72.A00, r4, false);
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        return new C75433jq(C12960it.A0F(this.A01.A00, viewGroup, R.layout.accept_invite_participant));
    }
}
