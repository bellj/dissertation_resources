package X;

import android.util.Base64;
import com.whatsapp.util.Log;

/* renamed from: X.5oi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124165oi extends AbstractC16350or {
    public final AbstractC136436Mn A00;
    public final C18590sh A01;
    public final String A02;
    public final String A03;
    public final C126615tA A04;

    public /* synthetic */ C124165oi(AbstractC136436Mn r1, C18590sh r2, String str, String str2, C126615tA r5) {
        this.A01 = r2;
        this.A02 = str;
        this.A04 = r5;
        this.A03 = str2;
        this.A00 = r1;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        C126615tA r1 = this.A04;
        String str = this.A03;
        String A01 = this.A01.A01();
        String str2 = this.A02;
        r1.A00.A09("onboarding", "registerApp called", new C31021Zs[]{new C31021Zs("app_id", "com.whatsapp"), new C31021Zs("mobile", str), new C31021Zs("device_id", A01)});
        String encodeToString = Base64.encodeToString(AnonymousClass61J.A03(Base64.decode(str2, 0), AnonymousClass61J.A01(C126615tA.A01.A01())), 0);
        StringBuilder A0j = C12960it.A0j("com.whatsapp");
        A0j.append("|");
        A0j.append(str);
        A0j.append("|");
        return Boolean.valueOf(Base64.encodeToString(AnonymousClass61J.A02(C12960it.A0d(A01, A0j)), 0).equalsIgnoreCase(encodeToString));
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        Boolean bool = (Boolean) obj;
        AbstractC136436Mn r1 = this.A00;
        if (r1 != null) {
            Log.i(C12960it.A0b("PAY: IndiaUpiSetupCoordinator/registered: ", bool));
            r1.AUn(bool.booleanValue());
        }
        C129315xW.A0D = null;
    }
}
