package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3ou  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C78433ou extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C99164jr();
    public final float A00;
    public final float A01;
    public final float A02;
    public final float A03;
    public final float A04;
    public final float A05;
    public final float A06;
    public final float A07;
    public final float A08;
    public final float A09;
    public final float A0A;
    public final int A0B;
    public final int A0C;
    public final C78403or[] A0D;
    public final C78083oL[] A0E;

    public C78433ou(C78403or[] r1, C78083oL[] r2, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, float f11, int i, int i2) {
        this.A0C = i;
        this.A0B = i2;
        this.A00 = f;
        this.A01 = f2;
        this.A02 = f3;
        this.A03 = f4;
        this.A04 = f5;
        this.A05 = f6;
        this.A06 = f7;
        this.A0D = r1;
        this.A07 = f8;
        this.A08 = f9;
        this.A09 = f10;
        this.A0E = r2;
        this.A0A = f11;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 1, this.A0C);
        C95654e8.A07(parcel, 2, this.A0B);
        C95654e8.A05(parcel, this.A00, 3);
        C95654e8.A05(parcel, this.A01, 4);
        C95654e8.A05(parcel, this.A02, 5);
        C95654e8.A05(parcel, this.A03, 6);
        C95654e8.A05(parcel, this.A04, 7);
        C95654e8.A05(parcel, this.A05, 8);
        C95654e8.A0I(parcel, this.A0D, 9, i);
        C95654e8.A05(parcel, this.A07, 10);
        C95654e8.A05(parcel, this.A08, 11);
        C95654e8.A05(parcel, this.A09, 12);
        C95654e8.A0I(parcel, this.A0E, 13, i);
        C95654e8.A05(parcel, this.A06, 14);
        C95654e8.A05(parcel, this.A0A, 15);
        C95654e8.A06(parcel, A00);
    }
}
