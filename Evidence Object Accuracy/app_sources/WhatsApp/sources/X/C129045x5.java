package X;

import android.content.Context;
import android.os.Bundle;
import android.util.Base64;
import com.whatsapp.util.Log;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONObject;

/* renamed from: X.5x5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129045x5 {
    public String A00;
    public String A01;
    public JSONObject A02;
    public C130495zV A03;

    public void A00(Context context, Bundle bundle, String str) {
        try {
            String string = bundle.getString("keyCode");
            this.A00 = string;
            if (string == null || string.isEmpty()) {
                throw new C124775q6(context, "Key code has not been provided in input");
            }
            try {
                String string2 = bundle.getString("keyXmlPayload");
                if (string2 == null || string2.isEmpty()) {
                    throw new C124775q6(context, "XML Payload has not been provided in input");
                }
                this.A03 = new C130495zV(string2);
                try {
                    String string3 = bundle.getString("controls");
                    if (string3 == null || string3.isEmpty()) {
                        JSONObject A0a = C117295Zj.A0a();
                        A0a.put("type", "PIN");
                        A0a.put("subtype", "MPIN");
                        A0a.put("dType", "NUM|ALPH");
                        A0a.put("dLength", 6);
                        JSONArray A0L = C117315Zl.A0L();
                        A0L.put(A0a);
                        C117295Zj.A0a().put("CredAllowed", A0L);
                    } else {
                        new JSONObject(string3);
                    }
                    try {
                        String string4 = bundle.getString("configuration");
                        if (string4 == null || string4.isEmpty()) {
                            Log.w("PAY: Configuration is not received");
                        } else {
                            new JSONObject(string4);
                        }
                        try {
                            String string5 = bundle.getString("salt");
                            if (string5 == null || string5.isEmpty()) {
                                throw new C124775q6(context, "Salt has not been provided in input");
                            }
                            this.A02 = C13000ix.A05(string5);
                            try {
                                String string6 = bundle.getString("trust");
                                this.A01 = string6;
                                if (string6 == null || string6.isEmpty()) {
                                    throw new C124775q6(context, "Trust has not been provided");
                                }
                                try {
                                    String A00 = C130495zV.A00(this.A02.optString("txnId"), this.A02.optString("txnAmount"), this.A02.optString("appId"), this.A02.optString("deviceId"), this.A02.optString("mobileNumber"), this.A02.optString("payerAddr"), this.A02.optString("payeeAddr"));
                                    String str2 = this.A01;
                                    try {
                                        String encodeToString = Base64.encodeToString(AnonymousClass61J.A02(A00), 2);
                                        String encodeToString2 = Base64.encodeToString(AnonymousClass61J.A03(Base64.decode(str2, 2), AnonymousClass61J.A01(str)), 2);
                                        if (encodeToString2 == null || encodeToString2.equalsIgnoreCase(encodeToString)) {
                                            try {
                                                String string7 = bundle.getString("payInfo");
                                                if (string7 == null || string7.isEmpty()) {
                                                    Log.w("PAY: Pay Info not received");
                                                }
                                                try {
                                                    String string8 = bundle.getString("languagePref");
                                                    if (string8 == null || string8.isEmpty()) {
                                                        string8 = "en_US";
                                                    }
                                                    new Locale(string8);
                                                } catch (Exception unused) {
                                                    throw new C124775q6(context, "Error while parsing Locale from input");
                                                }
                                            } catch (Exception unused2) {
                                                throw new C124775q6(context, "Error while parsing Pay Info from input");
                                            }
                                        } else {
                                            throw new C124795q8(EnumC124485pc.A03);
                                        }
                                    } catch (Exception unused3) {
                                        Log.e("PAY: Failed to check the trust");
                                        throw new C124795q8(EnumC124485pc.A04);
                                    }
                                } catch (C124795q8 unused4) {
                                    throw new C124775q6(context, "Trust is not valid");
                                }
                            } catch (C124775q6 e) {
                                throw e;
                            } catch (Exception unused5) {
                                throw new C124775q6(context, "Error while parsing XML Payload from input");
                            }
                        } catch (C124775q6 e2) {
                            throw e2;
                        } catch (Exception unused6) {
                            throw new C124775q6(context, "Error while parsing salt from input");
                        }
                    } catch (Exception unused7) {
                        throw new C124775q6(context, "Error while parsing configuration from input");
                    }
                } catch (Exception unused8) {
                    throw new C124775q6(context, "Error while parsing controls from input");
                }
            } catch (C124775q6 e3) {
                throw e3;
            } catch (C124795q8 unused9) {
                throw new C124775q6(context, "Technical Issue, please try after some time");
            } catch (Exception unused10) {
                throw new C124775q6(context, "Error while parsing XML Payload from input");
            }
        } catch (C124775q6 e4) {
            throw e4;
        } catch (Exception unused11) {
            throw new C124775q6(context, "Error while parsing Key Code from input");
        }
    }
}
