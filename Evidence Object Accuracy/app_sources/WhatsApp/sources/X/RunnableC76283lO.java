package X;

import com.facebook.redex.EmptyBaseRunnable0;

/* renamed from: X.3lO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final /* synthetic */ class RunnableC76283lO extends EmptyBaseRunnable0 implements Runnable {
    public final /* synthetic */ AnonymousClass1WE A00;
    public final /* synthetic */ C65963Lt A01;
    public final /* synthetic */ C16680pa A02;
    public final /* synthetic */ C129625y2 A03;
    public final /* synthetic */ String A04;

    public /* synthetic */ RunnableC76283lO(AnonymousClass1WE r1, C65963Lt r2, C16680pa r3, C129625y2 r4, String str) {
        this.A02 = r3;
        this.A00 = r1;
        this.A04 = str;
        this.A03 = r4;
        this.A01 = r2;
    }

    @Override // java.lang.Runnable
    public final void run() {
        C16680pa r4 = this.A02;
        AnonymousClass1WE r3 = this.A00;
        String str = this.A04;
        C16680pa.A00(r3, this.A01, r4, this.A03, str);
    }
}
