package X;

import java.lang.ref.WeakReference;

/* renamed from: X.37Z  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass37Z extends AbstractC16350or {
    public final C15550nR A00;
    public final AnonymousClass10T A01;
    public final C14850m9 A02;
    public final WeakReference A03;

    public AnonymousClass37Z(C15550nR r2, AnonymousClass10T r3, C36891ko r4, C14850m9 r5) {
        this.A03 = C12970iu.A10(r4);
        this.A00 = r2;
        this.A01 = r3;
        this.A02 = r5;
    }
}
