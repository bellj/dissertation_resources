package X;

import android.os.Bundle;
import android.os.Message;
import com.facebook.redex.RunnableBRunnable0Shape0S0311000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0400000_I0;
import com.facebook.redex.RunnableBRunnable0Shape1S0300000_I0_1;
import com.whatsapp.jid.UserJid;

/* renamed from: X.1FW */
/* loaded from: classes2.dex */
public class AnonymousClass1FW implements AbstractC15920o8 {
    public final C14830m7 A00;
    public final C16590pI A01;
    public final C15650ng A02;
    public final C14850m9 A03;
    public final C18050rp A04;
    public final C22170ye A05;
    public final C20660w7 A06;
    public final C17230qT A07;
    public final C22480z9 A08;
    public final C18600si A09;
    public final AnonymousClass10P A0A;
    public final C22710zW A0B;
    public final C17070qD A0C;
    public final AnonymousClass103 A0D;
    public final C30931Zj A0E = C30931Zj.A00("PaymentsXmppMessageHandler", "notification", "COMMON");
    public final AnonymousClass17Z A0F;
    public final C22140ya A0G;
    public final AbstractC14440lR A0H;

    public AnonymousClass1FW(C14830m7 r4, C16590pI r5, C15650ng r6, C14850m9 r7, C18050rp r8, C22170ye r9, C20660w7 r10, C17230qT r11, C22480z9 r12, C18600si r13, AnonymousClass10P r14, C22710zW r15, C17070qD r16, AnonymousClass103 r17, AnonymousClass17Z r18, C22140ya r19, AbstractC14440lR r20) {
        this.A00 = r4;
        this.A03 = r7;
        this.A01 = r5;
        this.A0H = r20;
        this.A06 = r10;
        this.A04 = r8;
        this.A05 = r9;
        this.A0C = r16;
        this.A02 = r6;
        this.A09 = r13;
        this.A07 = r11;
        this.A0G = r19;
        this.A0B = r15;
        this.A0D = r17;
        this.A0F = r18;
        this.A0A = r14;
        this.A08 = r12;
    }

    public static /* synthetic */ void A00(Bundle bundle, UserJid userJid, AnonymousClass1FW r9, AnonymousClass1OT r10) {
        AnonymousClass009.A05(userJid);
        boolean z = bundle.getBoolean("isMerchant");
        String string = bundle.getString("dataHash");
        r9.A0E.A06("onPaymentMerchantStatusUpdate");
        C17070qD r0 = r9.A0C;
        r0.A03();
        C241414j r3 = r0.A09;
        Boolean valueOf = Boolean.valueOf(z);
        synchronized (r3) {
            r3.A0K(userJid, valueOf, string, null, null);
        }
        r9.A05.A03.A0B(r10);
    }

    @Override // X.AbstractC15920o8
    public int[] ADF() {
        return new int[]{133, 211, 217, 219, 222};
    }

    @Override // X.AbstractC15920o8
    public boolean AI8(Message message, int i) {
        AbstractC14440lR r2;
        int i2;
        Bundle data = message.getData();
        AnonymousClass1OT r7 = (AnonymousClass1OT) data.getParcelable("stanzaKey");
        AnonymousClass1V4 A00 = this.A07.A00(2, r7.A00);
        if (A00 != null) {
            A00.A02(3);
        }
        if (i == 133) {
            r2 = this.A0H;
            i2 = 18;
        } else if (i == 211) {
            this.A0H.Ab2(new RunnableBRunnable0Shape0S0400000_I0(this, r7, data.getParcelable("jid"), data, 27));
            return true;
        } else if (i == 217) {
            r2 = this.A0H;
            i2 = 19;
        } else if (i != 219) {
            if (i == 222) {
                C14850m9 r1 = this.A03;
                if (r1.A07(423) || r1.A07(544)) {
                    this.A0H.Ab2(new RunnableBRunnable0Shape0S0311000_I0(this, r7, data.getParcelable("jid"), data.getInt("service"), 3, data.getBoolean("inviteUsed")));
                    return true;
                }
            }
            return false;
        } else {
            r2 = this.A0H;
            i2 = 17;
        }
        r2.Ab2(new RunnableBRunnable0Shape1S0300000_I0_1(this, r7, data, i2));
        return true;
    }
}
