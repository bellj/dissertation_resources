package X;

/* renamed from: X.4XH  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4XH {
    public final int A00;
    public final AnonymousClass4X8 A01;
    public final C30031Vu A02;
    public final boolean A03;

    public AnonymousClass4XH(AnonymousClass4X8 r1, C30031Vu r2, int i, boolean z) {
        this.A02 = r2;
        this.A03 = z;
        this.A01 = r1;
        this.A00 = i;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass4XH r5 = (AnonymousClass4XH) obj;
            if (this.A03 != r5.A03 || !this.A02.equals(r5.A02) || !this.A01.equals(r5.A01) || this.A00 != r5.A00) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return C12990iw.A08(this.A01, ((((this.A02.hashCode() * 31) + (this.A03 ? 1 : 0)) * 31) + this.A00) * 31);
    }
}
