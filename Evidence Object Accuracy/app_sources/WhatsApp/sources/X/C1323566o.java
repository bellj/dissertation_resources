package X;

import com.whatsapp.authentication.FingerprintBottomSheet;
import java.security.Signature;

/* renamed from: X.66o */
/* loaded from: classes4.dex */
public class C1323566o implements AnonymousClass21K {
    public final /* synthetic */ AnonymousClass21K A00;
    public final /* synthetic */ C119455e0 A01;

    @Override // X.AnonymousClass21K
    public /* synthetic */ void AMg(Signature signature) {
    }

    public C1323566o(AnonymousClass21K r1, C119455e0 r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass21K
    public void AMb(int i, CharSequence charSequence) {
        this.A00.AMb(i, charSequence);
    }

    @Override // X.AnonymousClass21K
    public void AMc() {
        this.A00.AMc();
    }

    @Override // X.AnonymousClass21K
    public void AMe(int i, CharSequence charSequence) {
        this.A00.AMe(i, charSequence);
    }

    @Override // X.AnonymousClass21K
    public void AMf(byte[] bArr) {
        FingerprintBottomSheet fingerprintBottomSheet = this.A01.A02;
        fingerprintBottomSheet.A1G(false);
        fingerprintBottomSheet.A03.setEnabled(false);
        fingerprintBottomSheet.A02.setEnabled(false);
        this.A00.AMf(bArr);
    }
}
