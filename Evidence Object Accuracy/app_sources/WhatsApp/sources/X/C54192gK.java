package X;

import android.graphics.Bitmap;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.facebook.redex.RunnableBRunnable0Shape14S0100000_I1;
import com.whatsapp.CatalogImageListActivity;
import com.whatsapp.R;
import com.whatsapp.util.ViewOnClickCListenerShape2S0101000_I1;

/* renamed from: X.2gK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54192gK extends AnonymousClass02M {
    public final AnonymousClass2TT A00;
    public final /* synthetic */ CatalogImageListActivity A01;

    public C54192gK(CatalogImageListActivity catalogImageListActivity, AnonymousClass2TT r2) {
        this.A01 = catalogImageListActivity;
        this.A00 = r2;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A01.A05.A06.size();
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r10, int i) {
        C75543k1 r102 = (C75543k1) r10;
        r102.A00 = C12960it.A1V(i, this.A01.A00);
        CatalogImageListActivity catalogImageListActivity = r102.A03;
        AnonymousClass3V9 r7 = new AnonymousClass2E5() { // from class: X.3V9
            @Override // X.AnonymousClass2E5
            public final void AS6(Bitmap bitmap, C68203Um r6, boolean z) {
                C75543k1 r1 = C75543k1.this;
                ImageView imageView = r1.A01;
                imageView.setImageBitmap(bitmap);
                if (r1.A00) {
                    r1.A00 = false;
                    imageView.post(new RunnableBRunnable0Shape14S0100000_I1(r1.A03, 32));
                }
            }
        };
        C1098153d r6 = new AnonymousClass5TN() { // from class: X.53d
            @Override // X.AnonymousClass5TN
            public final void AML(C68203Um r3) {
                C75543k1.this.A01.setImageResource(R.color.light_gray);
            }
        };
        ImageView imageView = r102.A01;
        catalogImageListActivity.A08.A02(imageView, (C44741zT) catalogImageListActivity.A05.A06.get(i), r6, r7, 1);
        imageView.setOnClickListener(new ViewOnClickCListenerShape2S0101000_I1(r102, i, 0));
        AnonymousClass028.A0k(imageView, AbstractC42671vd.A0Z(AnonymousClass19N.A00(i, catalogImageListActivity.A05.A0D)));
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        CatalogImageListActivity catalogImageListActivity = this.A01;
        return new C75543k1(C12960it.A0F(catalogImageListActivity.getLayoutInflater(), viewGroup, R.layout.business_product_catalog_image_list_item), catalogImageListActivity, this.A00);
    }
}
