package X;

import android.graphics.Path;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0OG  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0OG {
    public List A00 = new ArrayList();

    public void A00(Path path) {
        List list = this.A00;
        int size = list.size();
        while (true) {
            size--;
            if (size >= 0) {
                C07950aL r1 = (C07950aL) list.get(size);
                if (r1 != null && !r1.A06) {
                    AnonymousClass0UV.A04(path, ((AnonymousClass0H1) r1.A02).A09() / 100.0f, ((AnonymousClass0H1) r1.A00).A09() / 100.0f, ((AnonymousClass0H1) r1.A01).A09() / 360.0f);
                }
            } else {
                return;
            }
        }
    }
}
