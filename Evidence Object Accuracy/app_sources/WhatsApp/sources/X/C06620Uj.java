package X;

import android.animation.TypeEvaluator;

/* renamed from: X.0Uj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06620Uj implements TypeEvaluator {
    public C014506w[] A00;

    @Override // android.animation.TypeEvaluator
    public /* bridge */ /* synthetic */ Object evaluate(float f, Object obj, Object obj2) {
        C014506w[] r11 = (C014506w[]) obj;
        C014506w[] r12 = (C014506w[]) obj2;
        if (C014306u.A01(r11, r12)) {
            C014506w[] r4 = this.A00;
            if (!C014306u.A01(r4, r11)) {
                if (r11 == null) {
                    r4 = null;
                } else {
                    int length = r11.length;
                    r4 = new C014506w[length];
                    for (int i = 0; i < length; i++) {
                        r4[i] = new C014506w(r11[i]);
                    }
                }
                this.A00 = r4;
            }
            for (int i2 = 0; i2 < r11.length; i2++) {
                C014506w r7 = r4[i2];
                C014506w r6 = r11[i2];
                C014506w r5 = r12[i2];
                r7.A00 = r6.A00;
                int i3 = 0;
                while (true) {
                    float[] fArr = r6.A01;
                    if (i3 < fArr.length) {
                        r7.A01[i3] = (fArr[i3] * (1.0f - f)) + (r5.A01[i3] * f);
                        i3++;
                    }
                }
            }
            return r4;
        }
        throw new IllegalArgumentException("Can't interpolate between two incompatible pathData");
    }
}
