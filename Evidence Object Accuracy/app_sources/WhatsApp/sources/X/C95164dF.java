package X;

import java.nio.ByteBuffer;

/* renamed from: X.4dF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C95164dF {
    public static final AbstractC94284bY A00 = ((!C95624e5.A06 || !C95624e5.A07) ? new C79443qb() : new C79453qc());

    public static int A00(CharSequence charSequence) {
        int length = charSequence.length();
        int i = 0;
        int i2 = 0;
        while (i2 < length && charSequence.charAt(i2) < 128) {
            i2++;
        }
        int i3 = length;
        while (true) {
            if (i2 >= length) {
                break;
            }
            char charAt = charSequence.charAt(i2);
            if (charAt < 2048) {
                i3 += (127 - charAt) >>> 31;
                i2++;
            } else {
                int length2 = charSequence.length();
                while (i2 < length2) {
                    char charAt2 = charSequence.charAt(i2);
                    if (charAt2 < 2048) {
                        i += (127 - charAt2) >>> 31;
                    } else {
                        i += 2;
                        if (55296 <= charAt2 && charAt2 <= 57343) {
                            if (Character.codePointAt(charSequence, i2) >= 65536) {
                                i2++;
                            } else {
                                throw new AnonymousClass4CM(i2, length2);
                            }
                        }
                    }
                    i2++;
                }
                i3 += i;
            }
        }
        if (i3 >= length) {
            return i3;
        }
        throw C12970iu.A0f(C72453ed.A0n(i3));
    }

    public static void A01(CharSequence charSequence, ByteBuffer byteBuffer) {
        char c;
        int i;
        long j;
        byte b;
        int i2;
        char charAt;
        AbstractC94284bY r4 = A00;
        if (byteBuffer.hasArray()) {
            int arrayOffset = byteBuffer.arrayOffset();
            i2 = r4.A01(charSequence, byteBuffer.array(), byteBuffer.position() + arrayOffset, byteBuffer.remaining()) - arrayOffset;
        } else if (!byteBuffer.isDirect() || !(r4 instanceof C79453qc)) {
            AbstractC94284bY.A00(charSequence, byteBuffer);
            return;
        } else {
            AnonymousClass4YX r9 = C95624e5.A02;
            long A05 = r9.A05(byteBuffer, C95624e5.A01);
            long position = ((long) byteBuffer.position()) + A05;
            long limit = ((long) byteBuffer.limit()) + A05;
            int length = charSequence.length();
            if (((long) length) <= limit - position) {
                int i3 = 0;
                while (true) {
                    c = 128;
                    if (i3 >= length || (charAt = charSequence.charAt(i3)) >= 128) {
                        break;
                    }
                    position = 1 + position;
                    r9.A07(position, (byte) charAt);
                    i3++;
                }
                if (i3 != length) {
                    while (i3 < length) {
                        char charAt2 = charSequence.charAt(i3);
                        if (charAt2 >= c || position >= limit) {
                            if (charAt2 < 2048 && position <= limit - 2) {
                                j = position + 1;
                                r9.A07(position, (byte) ((charAt2 >>> 6) | 960));
                                position = j + 1;
                                b = (byte) ((charAt2 & '?') | 128);
                            } else if ((charAt2 < 55296 || 57343 < charAt2) && position <= limit - 3) {
                                long j2 = position + 1;
                                r9.A07(position, (byte) ((charAt2 >>> '\f') | 480));
                                j = j2 + 1;
                                r9.A07(j2, (byte) (((charAt2 >>> 6) & 63) | 128));
                                position = j + 1;
                                b = (byte) ((charAt2 & '?') | 128);
                            } else if (position <= limit - 4) {
                                int i4 = i3 + 1;
                                if (i4 != length) {
                                    char charAt3 = charSequence.charAt(i4);
                                    if (Character.isSurrogatePair(charAt2, charAt3)) {
                                        int codePoint = Character.toCodePoint(charAt2, charAt3);
                                        long j3 = position + 1;
                                        r9.A07(position, (byte) ((codePoint >>> 18) | 240));
                                        long j4 = j3 + 1;
                                        r9.A07(j3, (byte) (((codePoint >>> 12) & 63) | 128));
                                        long j5 = j4 + 1;
                                        r9.A07(j4, (byte) (((codePoint >>> 6) & 63) | 128));
                                        position = j5 + 1;
                                        r9.A07(j5, (byte) ((codePoint & 63) | 128));
                                        i3 = i4;
                                    } else {
                                        i3 = i4;
                                    }
                                }
                                throw new AnonymousClass4CM(i3 - 1, length);
                            } else if (55296 > charAt2 || charAt2 > 57343 || ((i = i3 + 1) != length && Character.isSurrogatePair(charAt2, charSequence.charAt(i)))) {
                                throw C72453ed.A0g(charAt2, position);
                            } else {
                                throw new AnonymousClass4CM(i3, length);
                            }
                            r9.A07(j, b);
                        } else {
                            position++;
                            r9.A07(position, (byte) charAt2);
                        }
                        i3++;
                        c = 128;
                    }
                }
                i2 = (int) (position - A05);
            } else {
                char charAt4 = charSequence.charAt(length - 1);
                int limit2 = byteBuffer.limit();
                StringBuilder A0t = C12980iv.A0t(37);
                A0t.append("Failed writing ");
                A0t.append(charAt4);
                throw new ArrayIndexOutOfBoundsException(C12960it.A0e(" at index ", A0t, limit2));
            }
        }
        byteBuffer.position(i2);
    }
}
