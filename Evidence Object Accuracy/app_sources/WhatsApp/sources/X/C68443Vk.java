package X;

/* renamed from: X.3Vk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68443Vk implements AnonymousClass2J6 {
    public final /* synthetic */ C44341yl A00;

    public C68443Vk(C44341yl r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass2J6
    public C59452uk A85(AnonymousClass2K4 r21, AnonymousClass2K3 r22, C48122Ek r23, AnonymousClass2K1 r24, AnonymousClass1B4 r25, C30211Wn r26, String str, String str2, String str3) {
        AnonymousClass01J r1 = this.A00.A03;
        C14850m9 A0S = C12960it.A0S(r1);
        C14900mE A0R = C12970iu.A0R(r1);
        C16590pI A0X = C12970iu.A0X(r1);
        AbstractC15710nm A0Q = C12970iu.A0Q(r1);
        AbstractC14440lR A0T = C12960it.A0T(r1);
        AnonymousClass018 A0R2 = C12960it.A0R(r1);
        C17170qN A0a = C12990iw.A0a(r1);
        return new C59452uk(A0Q, A0R, C12990iw.A0W(r1), r21, r22, r23, r24, r25, (C16340oq) r1.A5z.get(), r26, A0X, A0a, A0R2, A0S, A0T, str3, str, str2);
    }
}
