package X;

import android.util.Pair;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.4EN  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4EN {
    public static List A00(AnonymousClass1MJ r4) {
        if (r4 == null) {
            return null;
        }
        ArrayList A0l = C12960it.A0l();
        String str = r4.A01;
        if (str != null) {
            A0l.add(new Pair("Entry point", str));
        }
        String str2 = r4.A00;
        if (str2 != null) {
            A0l.add(new Pair("Cms ids", str2));
        }
        return A0l;
    }
}
