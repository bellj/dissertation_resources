package X;

import java.util.Set;

/* renamed from: X.2vD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C59682vD extends AnonymousClass4UW {
    public final Set A00;
    public final boolean A01;

    public C59682vD(Set set, boolean z) {
        this.A00 = set;
        this.A01 = z;
    }

    @Override // X.AnonymousClass4UW
    public boolean A00() {
        return this.A01;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C59682vD) {
                C59682vD r5 = (C59682vD) obj;
                if (!C16700pc.A0O(this.A00, r5.A00) || this.A01 != r5.A01) {
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        int hashCode = this.A00.hashCode() * 31;
        boolean z = this.A01;
        if (z) {
            z = true;
        }
        int i = z ? 1 : 0;
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        return hashCode + i;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("GroupedCategoriesChip(categories=");
        A0k.append(this.A00);
        A0k.append(", isSelected=");
        A0k.append(this.A01);
        return C12970iu.A0u(A0k);
    }
}
