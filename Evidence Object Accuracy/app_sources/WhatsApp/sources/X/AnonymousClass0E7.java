package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.0E7  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0E7 extends AbstractC015707l {
    public static final Parcelable.Creator CREATOR = new AnonymousClass0VH();
    public boolean A00;

    public AnonymousClass0E7(Parcel parcel, ClassLoader classLoader) {
        super(parcel, classLoader);
        this.A00 = ((Boolean) parcel.readValue(null)).booleanValue();
    }

    public AnonymousClass0E7(Parcelable parcelable) {
        super(parcelable);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("SearchView.SavedState{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" isIconified=");
        sb.append(this.A00);
        sb.append("}");
        return sb.toString();
    }

    @Override // X.AbstractC015707l, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeValue(Boolean.valueOf(this.A00));
    }
}
