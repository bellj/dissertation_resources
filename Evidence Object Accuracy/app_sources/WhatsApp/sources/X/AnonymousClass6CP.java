package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.payments.ui.ConfirmPaymentFragment;
import com.whatsapp.payments.ui.IndiaUpiEditTransactionDescriptionFragment;
import com.whatsapp.payments.ui.PaymentBottomSheet;
import com.whatsapp.payments.ui.PaymentMethodsListPickerFragment;
import com.whatsapp.payments.ui.widget.PaymentMethodRow;

/* renamed from: X.6CP  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6CP implements AnonymousClass6N0 {
    public final /* synthetic */ C14580lf A00;
    public final /* synthetic */ C30821Yy A01;
    public final /* synthetic */ AnonymousClass2S0 A02;
    public final /* synthetic */ ConfirmPaymentFragment A03;
    public final /* synthetic */ AbstractActivityC121525iS A04;

    @Override // X.AnonymousClass6N0
    public void ATc(PaymentBottomSheet paymentBottomSheet, int i) {
    }

    @Override // X.AnonymousClass6N0
    public void ATg(PaymentBottomSheet paymentBottomSheet, int i) {
    }

    @Override // X.AnonymousClass6N0
    public void AXm(PaymentBottomSheet paymentBottomSheet) {
    }

    public AnonymousClass6CP(C14580lf r1, C30821Yy r2, AnonymousClass2S0 r3, ConfirmPaymentFragment confirmPaymentFragment, AbstractActivityC121525iS r5) {
        this.A04 = r5;
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = confirmPaymentFragment;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00bf, code lost:
        if (r4 == false) goto L_0x00c1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A00(com.whatsapp.payments.ui.PaymentBottomSheet r18) {
        /*
        // Method dump skipped, instructions count: 243
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass6CP.A00(com.whatsapp.payments.ui.PaymentBottomSheet):void");
    }

    @Override // X.AnonymousClass6N0
    public void AOW(View view, View view2, AnonymousClass1ZO r8, AbstractC28901Pl r9, PaymentBottomSheet paymentBottomSheet) {
        AbstractActivityC121525iS r4 = this.A04;
        if (AbstractActivityC119235dO.A1k(r9, r4)) {
            ATW(paymentBottomSheet, 0);
            return;
        }
        C14580lf r3 = this.A00;
        if (r3 != null) {
            r4.A2C(R.string.register_wait_message);
            AnonymousClass61P r2 = ((AbstractActivityC121685jC) r4).A0V;
            C117315Zl.A0R(r2.A00, r3, new C134376Ej(r4, new C133796Cd(this, paymentBottomSheet), r2));
            return;
        }
        A00(paymentBottomSheet);
    }

    @Override // X.AnonymousClass6N0
    public void ATW(PaymentBottomSheet paymentBottomSheet, int i) {
        String str;
        AbstractActivityC121525iS r2 = this.A04;
        boolean A01 = r2.A0X.A01(r2.A0B, r2.A0o);
        AnonymousClass6BE r7 = ((AbstractActivityC121665jA) r2).A0D;
        if (A01) {
            str = "add_credential_prompt";
        } else {
            str = "payment_confirm_prompt";
        }
        String str2 = r2.A0d;
        boolean A1p = AbstractActivityC121685jC.A1p(r2);
        r7.AKj(AnonymousClass61I.A00(((ActivityC13790kL) r2).A05, this.A01, this.A02, null, true), 1, 84, str, str2, ((AbstractActivityC121685jC) r2).A0g, ((AbstractActivityC121685jC) r2).A0f, false, A1p);
        PaymentMethodsListPickerFragment A00 = PaymentMethodsListPickerFragment.A00(r2.A0g);
        A00.A08 = new AnonymousClass6CU(r2, A00);
        A00.A06 = new AnonymousClass6CQ(r2);
        A00.A0X(this.A03, 0);
        paymentBottomSheet.A1L(A00);
    }

    @Override // X.AnonymousClass6N0
    public void ATZ(AbstractC28901Pl r2, PaymentMethodRow paymentMethodRow) {
        AbstractActivityC121525iS.A02(r2, this.A04);
    }

    @Override // X.AnonymousClass6N0
    public void AXn(String str) {
        AbstractActivityC121525iS r0 = this.A04;
        r0.A0a = str;
        r0.A0V.A01(str);
    }

    @Override // X.AnonymousClass6N0
    public void AXo(PaymentBottomSheet paymentBottomSheet) {
        AbstractActivityC121525iS r3 = this.A04;
        IndiaUpiEditTransactionDescriptionFragment A00 = IndiaUpiEditTransactionDescriptionFragment.A00(r3.A0a);
        r3.A3O(64, "payment_confirm_prompt");
        A00.A08 = new C133986Cw(this);
        paymentBottomSheet.A1L(A00);
    }
}
