package X;

/* renamed from: X.69e  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1330269e implements AnonymousClass6MM {
    public final /* synthetic */ C128995x0 A00;
    public final /* synthetic */ C129135xE A01;

    public C1330269e(C128995x0 r1, C129135xE r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass6MM
    public void APo(C452120p r5) {
        C128995x0 r3 = this.A00;
        if (r3.A01.compareAndSet(false, true)) {
            r3.A02.decrementAndGet();
            r3.A00.APo(r5);
        }
    }

    @Override // X.AnonymousClass6MM
    public void AX5(String str) {
        this.A00.A00(0, str);
    }
}
