package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.15K  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass15K {
    public C232010t A00;

    public AnonymousClass15K(C232010t r1) {
        this.A00 = r1;
    }

    public C15580nU A00(C15580nU r12) {
        try {
            C16310on A01 = this.A00.get();
            Cursor A08 = A01.A03.A08("group_relationship", "subgroup_raw_id = ?", null, null, new String[]{"parent_raw_jid"}, new String[]{r12.getRawString()});
            if (A08 != null) {
                try {
                    if (A08.moveToFirst()) {
                        C15580nU A04 = C15580nU.A04(A08.getString(A08.getColumnIndexOrThrow("parent_raw_jid")));
                        A08.close();
                        A01.close();
                        return A04;
                    }
                    A08.close();
                } catch (Throwable th) {
                    try {
                        A08.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            }
            A01.close();
            return null;
        } catch (SQLiteDatabaseCorruptException e) {
            Log.e("chat-settings-store/get-wallpaper-files", e);
            return null;
        }
    }

    public List A01(C15580nU r14) {
        ArrayList arrayList = new ArrayList();
        String[] strArr = {r14.getRawString()};
        C16310on A01 = this.A00.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT subgroups.subgroup_raw_jid, subject, subject_ts, group_type FROM subgroup_info subgroups INNER JOIN group_relationship relationship ON subgroups.subgroup_raw_jid = relationship.subgroup_raw_id WHERE relationship.parent_raw_jid = ?", strArr);
            while (A09.moveToNext()) {
                int columnIndexOrThrow = A09.getColumnIndexOrThrow("subject");
                int columnIndexOrThrow2 = A09.getColumnIndexOrThrow("subject_ts");
                try {
                    arrayList.add(new AnonymousClass1OU(C15580nU.A03(A09.getString(A09.getColumnIndexOrThrow("subgroup_raw_jid"))), A09.getString(columnIndexOrThrow), A09.getInt(A09.getColumnIndexOrThrow("group_type")), A09.getLong(columnIndexOrThrow2)));
                } catch (AnonymousClass1MW e) {
                    Log.e("SubgroupStore/invalid subgroup jid", e);
                }
            }
            A09.close();
            A01.close();
            return arrayList;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A02(GroupJid groupJid) {
        try {
            try {
                C16310on A02 = this.A00.A02();
                try {
                    AnonymousClass1Lx A00 = A02.A00();
                    String rawString = groupJid.getRawString();
                    C16330op r3 = A02.A03;
                    r3.A01("subgroup_info", "subgroup_raw_jid = ?", new String[]{rawString});
                    r3.A01("group_relationship", "subgroup_raw_id = ?", new String[]{rawString});
                    A00.A00();
                    A00.close();
                    A02.close();
                } catch (Throwable th) {
                    try {
                        A02.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            } catch (Error | RuntimeException e) {
                Log.e(e);
                throw e;
            }
        } catch (SQLiteDatabaseCorruptException e2) {
            Log.e(e2);
        }
    }

    public void A03(GroupJid groupJid, Collection collection) {
        A04(groupJid, collection);
        List<AnonymousClass1OU> A01 = A01(C15580nU.A02(groupJid));
        A01.removeAll(collection);
        for (AnonymousClass1OU r0 : A01) {
            A02(r0.A02);
        }
    }

    public void A04(GroupJid groupJid, Collection collection) {
        try {
            try {
                C232010t r7 = this.A00;
                C16310on A02 = r7.A02();
                try {
                    AnonymousClass1Lx A00 = A02.A00();
                    Iterator it = collection.iterator();
                    while (it.hasNext()) {
                        AnonymousClass1OU r9 = (AnonymousClass1OU) it.next();
                        GroupJid groupJid2 = r9.A02;
                        String str = r9.A03;
                        long j = r9.A01;
                        String rawString = groupJid2.getRawString();
                        boolean z = true;
                        String[] strArr = {groupJid2.getRawString()};
                        A02 = r7.get();
                        try {
                            Cursor A09 = A02.A03.A09("SELECT subject_ts FROM subgroup_info WHERE subgroup_raw_jid = ?", strArr);
                            if (A09.moveToFirst()) {
                                if (A09.getLong(A09.getColumnIndexOrThrow("subject_ts")) <= j) {
                                    z = false;
                                }
                                A09.close();
                                A02.close();
                                if (z) {
                                }
                            } else {
                                A09.close();
                                A02.close();
                            }
                            ContentValues contentValues = new ContentValues(3);
                            contentValues.put("subgroup_raw_jid", rawString);
                            contentValues.put("subject", str);
                            contentValues.put("subject_ts", Long.valueOf(j));
                            contentValues.put("group_type", Integer.valueOf(r9.A00));
                            C16330op r6 = A02.A03;
                            if (r6.A00("subgroup_info", contentValues, "subgroup_raw_jid = ?", new String[]{rawString}) == 0) {
                                r6.A02(contentValues, "subgroup_info");
                            }
                            ContentValues contentValues2 = new ContentValues(2);
                            contentValues2.put("parent_raw_jid", groupJid.getRawString());
                            contentValues2.put("subgroup_raw_id", rawString);
                            if (r6.A00("group_relationship", contentValues2, "subgroup_raw_id = ?", new String[]{rawString}) == 0) {
                                r6.A02(contentValues2, "group_relationship");
                            }
                        } finally {
                        }
                    }
                    A00.A00();
                    A00.close();
                    A02.close();
                } finally {
                }
            } catch (SQLiteDatabaseCorruptException e) {
                Log.e(e);
            }
        } catch (Error | RuntimeException e2) {
            Log.e(e2);
            throw e2;
        }
    }
}
