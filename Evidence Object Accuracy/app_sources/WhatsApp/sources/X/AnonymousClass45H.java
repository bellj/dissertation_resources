package X;

import java.io.File;

/* renamed from: X.45H  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass45H extends AbstractC39731qS {
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final boolean A04;
    public final int[] A05;

    public /* synthetic */ AnonymousClass45H(File file, String str, byte[] bArr, int[] iArr, int i, int i2, int i3, int i4, boolean z, boolean z2) {
        super(file, str, bArr, z);
        this.A02 = i;
        this.A03 = i2;
        this.A00 = i3;
        this.A01 = i4;
        this.A04 = z2;
        if (iArr == null || iArr.length < 2) {
            this.A05 = new int[0];
        } else {
            this.A05 = iArr;
        }
    }
}
