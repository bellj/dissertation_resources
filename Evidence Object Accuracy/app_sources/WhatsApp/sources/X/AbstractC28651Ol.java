package X;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import com.facebook.redex.RunnableBRunnable0Shape13S0100000_I0_13;
import com.whatsapp.util.Log;
import java.io.File;

/* renamed from: X.1Ol  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC28651Ol {
    public static AbstractC28651Ol A00(C16590pI r3, C14850m9 r4, File file, int i) {
        boolean z;
        if (r4 != null) {
            z = A01(r4);
        } else {
            z = false;
        }
        if (r3 != null && z && file.getAbsolutePath().endsWith(".opus")) {
            return new AnonymousClass36Q(r3.A00, r4, file, i);
        }
        if (!file.getAbsolutePath().endsWith(".opus")) {
            AnonymousClass2CK r2 = new AnonymousClass2CK(i);
            r2.A00.setDataSource(file.getAbsolutePath());
            return r2;
        }
        StringBuilder sb = new StringBuilder("AudioPlayer/create exoplayer enabled:");
        sb.append(z);
        sb.append(" Build.MANUFACTURER:");
        sb.append(Build.MANUFACTURER);
        sb.append(" Build.DEVICE:");
        sb.append(Build.DEVICE);
        sb.append(" SDK_INT:");
        sb.append(Build.VERSION.SDK_INT);
        Log.e(sb.toString());
        return new AnonymousClass47U(file, i);
    }

    public static boolean A01(C14850m9 r2) {
        return Build.VERSION.SDK_INT >= 21 && r2.A07(751) && !C38241nl.A02();
    }

    public int A02() {
        if (!(this instanceof AnonymousClass36Q)) {
            return ((AnonymousClass2CK) this).A00.getCurrentPosition();
        }
        return (int) ((AnonymousClass36Q) this).A07.AC9();
    }

    public int A03() {
        if (!(this instanceof AnonymousClass36Q)) {
            return ((AnonymousClass2CK) this).A00.getDuration();
        }
        return ((AnonymousClass36Q) this).A00;
    }

    public void A04() {
        if (!(this instanceof AnonymousClass36Q)) {
            ((AnonymousClass2CK) this).A00.pause();
        } else {
            ((AnonymousClass36Q) this).A07.AcW(false);
        }
    }

    public void A05() {
        if (!(this instanceof AnonymousClass36Q)) {
            ((AnonymousClass2CK) this).A00.prepare();
            return;
        }
        AnonymousClass36Q r1 = (AnonymousClass36Q) this;
        C47492Ax r3 = r1.A07;
        AbstractC47452At r0 = r1.A02;
        if (r0 == null) {
            r0 = new C107854y3();
            r1.A02 = r0;
        }
        C67723Sq r2 = new C67723Sq(r0);
        Uri uri = r1.A06;
        AnonymousClass3DU r02 = new AnonymousClass3DU();
        r02.A06 = uri;
        AnonymousClass4XB r03 = r02.A00().A02;
        Uri uri2 = r03.A00;
        AbstractC47452At r7 = r2.A02;
        AnonymousClass5SK r6 = r2.A00;
        AnonymousClass5QO r8 = r2.A01;
        Object obj = r03.A01;
        if (obj == null) {
            obj = null;
        }
        r3.A08(new C56042kE(uri2, r6, r7, r8, obj), true);
    }

    public void A06() {
        if (!(this instanceof AnonymousClass36Q)) {
            AnonymousClass2CK r1 = (AnonymousClass2CK) this;
            r1.A01.postDelayed(new RunnableBRunnable0Shape13S0100000_I0_13(r1, 13), 100);
            return;
        }
        AnonymousClass36Q r12 = (AnonymousClass36Q) this;
        r12.A04 = null;
        C47492Ax r13 = r12.A07;
        r13.A0A(true);
        r13.A01();
    }

    public void A07() {
        if (!(this instanceof AnonymousClass36Q)) {
            ((AnonymousClass2CK) this).A00.start();
        } else {
            ((AnonymousClass36Q) this).A07.AcW(true);
        }
    }

    public void A08() {
        if (!(this instanceof AnonymousClass36Q)) {
            ((AnonymousClass2CK) this).A00.start();
        } else {
            ((AnonymousClass36Q) this).A07.AcW(true);
        }
    }

    public void A09() {
        if (!(this instanceof AnonymousClass36Q)) {
            ((AnonymousClass2CK) this).A00.stop();
        } else {
            ((AnonymousClass36Q) this).A07.A0A(true);
        }
    }

    public void A0A(int i) {
        if (!(this instanceof AnonymousClass36Q)) {
            ((AnonymousClass2CK) this).A00.seekTo(i);
            return;
        }
        C47492Ax r3 = ((AnonymousClass36Q) this).A07;
        r3.AbS(r3.ACF(), (long) i);
    }

    public void A0B(MediaPlayer.OnErrorListener onErrorListener) {
        if (!(this instanceof AnonymousClass36Q)) {
            ((AnonymousClass2CK) this).A00.setOnErrorListener(onErrorListener);
        }
    }

    public void A0C(AnonymousClass4KR r2) {
        if (this instanceof AnonymousClass36Q) {
            ((AnonymousClass36Q) this).A04 = r2;
        }
    }

    public boolean A0D() {
        if (!(this instanceof AnonymousClass36Q)) {
            return ((AnonymousClass2CK) this).A00.isPlaying();
        }
        C47492Ax r3 = ((AnonymousClass36Q) this).A07;
        if (r3 == null) {
            return false;
        }
        int AFl = r3.AFl();
        if ((AFl == 3 || AFl == 2) && r3.AFj()) {
            return true;
        }
        return false;
    }

    public boolean A0E(AbstractC15710nm r12, float f) {
        AnonymousClass36Q r0 = (AnonymousClass36Q) this;
        r0.A03 = r12;
        float f2 = -1.0f;
        try {
            C47492Ax r2 = r0.A07;
            r2.A03();
            C76513ll r4 = r2.A0P;
            f2 = r4.A05.A04.A01;
            if (Math.abs(f2 - f) < 0.1f) {
                return true;
            }
            C94344be r1 = new C94344be(f, 1.0f);
            r2.A03();
            C95034cy r22 = r4.A05;
            if (r22.A04.equals(r1)) {
                return true;
            }
            C95034cy A04 = r22.A04(r1);
            r4.A02++;
            ((C107914yA) r4.A0B.A0Z).A00.obtainMessage(4, r1).sendToTarget();
            r4.A06(A04, 4, 0, 1, false, false);
            return true;
        } catch (IllegalArgumentException | IllegalStateException unused) {
            StringBuilder sb = new StringBuilder("currSpeed: ");
            sb.append(f2);
            sb.append(" , newSpeed: ");
            sb.append(f);
            r12.AaV("exoaudioplayer/setPlaybackSpeed failed", sb.toString(), true);
            StringBuilder sb2 = new StringBuilder("exoaudioplayer/setPlaybackSpeed failed, currSpeed: ");
            sb2.append(f2);
            sb2.append(" , newSpeed: ");
            sb2.append(f);
            Log.e(sb2.toString());
            return false;
        }
    }
}
