package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.6Jk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final /* synthetic */ class RunnableC135686Jk implements Runnable {
    public final /* synthetic */ long A00;
    public final /* synthetic */ UserJid A01;
    public final /* synthetic */ C118025b9 A02;

    public /* synthetic */ RunnableC135686Jk(UserJid userJid, C118025b9 r2, long j) {
        this.A02 = r2;
        this.A01 = userJid;
        this.A00 = j;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0064, code lost:
        if (r1 == 3) goto L_0x0066;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x006b, code lost:
        if (r5 == 3) goto L_0x006d;
     */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x006f A[EXC_TOP_SPLITTER, SYNTHETIC] */
    @Override // java.lang.Runnable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r11 = this;
            X.5b9 r6 = r11.A02
            com.whatsapp.jid.UserJid r9 = r11.A01
            long r0 = r11.A00
            if (r9 == 0) goto L_0x006d
            X.0qD r3 = r6.A03
            X.1ZO r8 = X.C117305Zk.A0F(r9, r3)
            X.17Z r2 = r6.A04
            X.2S0 r2 = r2.A00()
            X.1ng r7 = X.C117305Zk.A0K(r3)
            if (r7 == 0) goto L_0x006d
            int r5 = r2.A00(r0)
            X.2Ry r10 = r2.A01
            X.2Rx r4 = r2.A02
            if (r10 == 0) goto L_0x0066
            X.0m9 r0 = r7.A07
            boolean r0 = X.C117305Zk.A1U(r0)
            r3 = 3
            if (r0 == 0) goto L_0x0039
            if (r4 == 0) goto L_0x0039
            int r2 = r10.A03
            int r1 = r4.A01
            int r0 = r4.A00
            int r1 = r1 + r0
            if (r2 > r1) goto L_0x0058
            r3 = 2
        L_0x0039:
            int r1 = r7.A07(r8, r9, r10)
            r7 = 3
            if (r3 == r7) goto L_0x0066
            if (r1 == r7) goto L_0x0066
            r0 = 2
            if (r3 != r0) goto L_0x005e
            r7 = 4
        L_0x0046:
            if (r5 == 0) goto L_0x006d
            r0 = 4
            if (r5 != r0) goto L_0x006a
            r7 = 1
        L_0x004c:
            X.016 r4 = r6.A00
            X.17Z r0 = r6.A04
            X.2Ry r3 = r0.A02()
            java.lang.Object r2 = X.AnonymousClass17Z.A0H
            monitor-enter(r2)
            goto L_0x006f
        L_0x0058:
            boolean r0 = r4.A04
            if (r0 == 0) goto L_0x0039
            r3 = 1
            goto L_0x0039
        L_0x005e:
            if (r1 == 0) goto L_0x0046
            if (r1 == r0) goto L_0x0068
            r0 = 3
            r7 = 0
            if (r1 != r0) goto L_0x0046
        L_0x0066:
            r7 = 6
            goto L_0x0046
        L_0x0068:
            r7 = 5
            goto L_0x0046
        L_0x006a:
            r0 = 3
            if (r5 != r0) goto L_0x004c
        L_0x006d:
            r7 = 6
            goto L_0x004c
        L_0x006f:
            X.2Rx r1 = r0.A01     // Catch: all -> 0x007f
            monitor-exit(r2)     // Catch: all -> 0x007f
            X.2S0 r0 = new X.2S0
            r0.<init>(r3, r1, r7)
            X.617 r0 = X.AnonymousClass617.A01(r0)
            r4.A0A(r0)
            return
        L_0x007f:
            r0 = move-exception
            monitor-exit(r2)     // Catch: all -> 0x007f
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.RunnableC135686Jk.run():void");
    }
}
