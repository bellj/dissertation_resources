package X;

/* renamed from: X.19s  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C255219s {
    public final C14330lG A00;
    public final C15570nT A01;
    public final AnonymousClass11L A02;
    public final C15550nR A03;
    public final C15610nY A04;
    public final C14830m7 A05;
    public final C16590pI A06;
    public final AnonymousClass018 A07;
    public final C16510p9 A08;
    public final C15650ng A09;
    public final C16490p7 A0A;
    public final C20370ve A0B;
    public final AnonymousClass14X A0C;

    public C255219s(C14330lG r1, C15570nT r2, AnonymousClass11L r3, C15550nR r4, C15610nY r5, C14830m7 r6, C16590pI r7, AnonymousClass018 r8, C16510p9 r9, C15650ng r10, C16490p7 r11, C20370ve r12, AnonymousClass14X r13) {
        this.A06 = r7;
        this.A05 = r6;
        this.A08 = r9;
        this.A01 = r2;
        this.A00 = r1;
        this.A0C = r13;
        this.A03 = r4;
        this.A04 = r5;
        this.A07 = r8;
        this.A09 = r10;
        this.A0A = r11;
        this.A0B = r12;
        this.A02 = r3;
    }
}
