package X;

/* renamed from: X.1FN  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1FN extends AbstractC17250qV {
    public final C14900mE A00;
    public final C14830m7 A01;
    public final AnonymousClass12H A02;
    public final C26531Dv A03;

    public AnonymousClass1FN(AbstractC15710nm r8, C14900mE r9, C14830m7 r10, AnonymousClass12H r11, C17220qS r12, C17230qT r13, C26531Dv r14, AbstractC14440lR r15) {
        super(r8, r12, r13, r15, new int[]{238}, false);
        this.A01 = r10;
        this.A00 = r9;
        this.A03 = r14;
        this.A02 = r11;
    }
}
