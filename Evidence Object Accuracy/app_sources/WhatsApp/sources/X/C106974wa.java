package X;

import android.net.Uri;
import java.util.ArrayDeque;
import java.util.List;
import java.util.Map;

/* renamed from: X.4wa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C106974wa implements AnonymousClass5WY, AbstractC116785Ww {
    public static final AnonymousClass5SK A0M = new AnonymousClass5SK() { // from class: X.4wP
        @Override // X.AnonymousClass5SK
        public /* synthetic */ AbstractC116785Ww[] A8I(Uri uri, Map map) {
            return new AbstractC116785Ww[]{new C106974wa()};
        }
    };
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04 = 0;
    public int A05;
    public int A06;
    public int A07;
    public int A08 = -1;
    public long A09;
    public long A0A;
    public AbstractC14070ko A0B;
    public C95304dT A0C;
    public C91244Qz[] A0D;
    public long[][] A0E;
    public final C93574aO A0F = new C93574aO();
    public final C95304dT A0G = C95304dT.A05(16);
    public final C95304dT A0H = C95304dT.A05(4);
    public final C95304dT A0I = new C95304dT(C95464dl.A02);
    public final C95304dT A0J = new C95304dT();
    public final ArrayDeque A0K = new ArrayDeque();
    public final List A0L = C12960it.A0l();

    @Override // X.AnonymousClass5WY
    public boolean AK2() {
        return true;
    }

    public static long A00(AnonymousClass4W6 r3, long j, long j2) {
        int A06 = AnonymousClass3JZ.A06(r3.A07, j, false);
        while (true) {
            if (A06 >= 0) {
                if ((r3.A04[A06] & 1) != 0) {
                    break;
                }
                A06--;
            } else {
                A06 = r3.A00(j);
                if (A06 == -1) {
                    return j2;
                }
            }
        }
        return Math.min(r3.A06[A06], j2);
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x01f1, code lost:
        if (r3 != 757935405) goto L_0x02ed;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x01f3, code lost:
        r4 = null;
        r13 = null;
        r3 = null;
        r2 = -1;
        r1 = -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x01f8, code lost:
        r15 = r11.A01;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x01fa, code lost:
        if (r15 >= r5) goto L_0x022e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x01fc, code lost:
        r16 = r11.A07();
        r0 = r11.A07();
        r11.A0T(4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x020b, code lost:
        if (r0 != 1835360622) goto L_0x0214;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x020d, code lost:
        r13 = r11.A0N(r16 - 12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x0217, code lost:
        if (r0 != 1851878757) goto L_0x0220;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x0219, code lost:
        r3 = r11.A0N(r16 - 12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x0223, code lost:
        if (r0 != 1684108385) goto L_0x0228;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x0225, code lost:
        r2 = r15;
        r1 = r16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x0228, code lost:
        r11.A0T(r16 - 12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x022e, code lost:
        if (r13 == null) goto L_0x0314;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0230, code lost:
        if (r3 == null) goto L_0x0314;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x0233, code lost:
        if (r2 == -1) goto L_0x0314;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x0235, code lost:
        r11.A0S(r2);
        r11.A0T(16);
        r4 = new X.C77063mg(r13, r3, r11.A0N(r1 - 16));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x0249, code lost:
        r1 = 16777215 & r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x0250, code lost:
        if (r1 != 6516084) goto L_0x0285;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x0252, code lost:
        r2 = r11.A07();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x025d, code lost:
        if (r11.A07() != 1684108385) goto L_0x0271;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x025f, code lost:
        r11.A0T(8);
        r1 = r11.A0N(r2 - 16);
        r4 = new X.C77053mf("und", r1, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x0271, code lost:
        android.util.Log.w("MetadataUtil", X.C12960it.A0d(X.AbstractC93864as.A00(r3), X.C12960it.A0k("Failed to parse comment attribute: ")));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:127:0x0288, code lost:
        if (r1 == 7233901) goto L_0x030c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:0x028d, code lost:
        if (r1 == 7631467) goto L_0x030c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x0292, code lost:
        if (r1 == 6516589) goto L_0x0305;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:0x0297, code lost:
        if (r1 == 7828084) goto L_0x0305;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:135:0x029c, code lost:
        if (r1 != 6578553) goto L_0x02a5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:136:0x029e, code lost:
        r4 = X.C95144dD.A02(r11, "TDRC", r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:138:0x02a8, code lost:
        if (r1 != 4280916) goto L_0x02b1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:139:0x02aa, code lost:
        r4 = X.C95144dD.A02(r11, "TPE1", r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:141:0x02b4, code lost:
        if (r1 != 7630703) goto L_0x02bd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:0x02b6, code lost:
        r4 = X.C95144dD.A02(r11, "TSSE", r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:0x02c0, code lost:
        if (r1 != 6384738) goto L_0x02c9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:145:0x02c2, code lost:
        r4 = X.C95144dD.A02(r11, "TALB", r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:147:0x02cc, code lost:
        if (r1 != 7108978) goto L_0x02d5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:148:0x02ce, code lost:
        r4 = X.C95144dD.A02(r11, "USLT", r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:0x02d8, code lost:
        if (r1 != 6776174) goto L_0x02e1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:0x02da, code lost:
        r4 = X.C95144dD.A02(r11, "TCON", r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:153:0x02e4, code lost:
        if (r1 != 6779504) goto L_0x02ed;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:0x02e6, code lost:
        r4 = X.C95144dD.A02(r11, "TIT1", r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x02ed, code lost:
        r1 = X.C12960it.A0h();
        r1.append("Skipped unknown metadata entry: ");
        android.util.Log.d("MetadataUtil", X.C12960it.A0d(X.AbstractC93864as.A00(r3), r1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:156:0x0303, code lost:
        r4 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:157:0x0305, code lost:
        r4 = X.C95144dD.A02(r11, "TCOM", r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x030c, code lost:
        r4 = X.C95144dD.A02(r11, "TIT2", r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:159:0x0313, code lost:
        r4 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:160:0x0314, code lost:
        r11.A0S(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:161:0x0317, code lost:
        if (r4 == null) goto L_0x0094;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:162:0x0319, code lost:
        r17.add(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:164:0x0324, code lost:
        if (r17.isEmpty() != false) goto L_0x0334;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:165:0x0326, code lost:
        r3 = new X.C100624mD(r17);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0089, code lost:
        r11.A0S(r8);
        r8 = r8 + r2;
        r11.A0T(8);
        r17 = X.C12960it.A0l();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:248:0x0518, code lost:
        if (r21 != null) goto L_0x051a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0094, code lost:
        r5 = r11.A01;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0096, code lost:
        if (r5 >= r8) goto L_0x0320;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0098, code lost:
        r5 = r5 + r11.A07();
        r3 = r11.A07();
        r1 = (r3 >> 24) & 255;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00a7, code lost:
        if (r1 == 169) goto L_0x0249;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:296:0x0616, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:297:0x0617, code lost:
        r11.A0S(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:298:0x061a, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00ab, code lost:
        if (r1 == 253) goto L_0x0249;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00b0, code lost:
        if (r3 != 1735291493) goto L_0x00ec;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00b2, code lost:
        r11.A0T(4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00bd, code lost:
        if (r11.A07() != 1684108385) goto L_0x00c4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00bf, code lost:
        r3 = X.C95304dT.A01(r11, 8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00c4, code lost:
        android.util.Log.w("MetadataUtil", "Failed to parse uint8 attribute value");
        r3 = -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00cc, code lost:
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00cd, code lost:
        if (r3 <= 0) goto L_0x00e3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00cf, code lost:
        r1 = X.C95144dD.A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00d2, code lost:
        if (r3 > r1.length) goto L_0x00e3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00d4, code lost:
        r1 = r1[r3 - 1];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00d8, code lost:
        if (r1 == null) goto L_0x00e3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00da, code lost:
        r4 = new X.C77033md("TCON", null, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00e3, code lost:
        android.util.Log.w("MetadataUtil", "Failed to parse standard genre code");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00ef, code lost:
        if (r3 != 1684632427) goto L_0x00f9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00f1, code lost:
        r4 = X.C95144dD.A01(r11, "TPOS", r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00fc, code lost:
        if (r3 != 1953655662) goto L_0x0106;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00fe, code lost:
        r4 = X.C95144dD.A01(r11, "TRCK", r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x010a, code lost:
        if (r3 != 1953329263) goto L_0x0114;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x010c, code lost:
        r4 = X.C95144dD.A00(r11, "TBPM", r3, true, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0117, code lost:
        if (r3 != 1668311404) goto L_0x0121;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0119, code lost:
        r4 = X.C95144dD.A00(r11, "TCMP", r3, true, true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0124, code lost:
        if (r3 != 1668249202) goto L_0x016c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0126, code lost:
        r13 = r11.A07();
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0134, code lost:
        if (r11.A07() != 1684108385) goto L_0x0165;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0136, code lost:
        r4 = r11.A07() & 16777215;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0140, code lost:
        if (r4 != 13) goto L_0x0157;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0142, code lost:
        r3 = "image/jpeg";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0144, code lost:
        r11.A0T(4);
        r4 = r13 - 16;
        r0 = new byte[r4];
        r11.A0V(r0, 0, r4);
        r4 = new X.C77073mh(r3, null, r0, 3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0159, code lost:
        if (r4 != 14) goto L_0x015e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x015b, code lost:
        r3 = "image/png";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x015e, code lost:
        r0 = X.C12960it.A0W(r4, "Unrecognized cover art flags: ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0165, code lost:
        r0 = "Failed to parse cover art attribute";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0167, code lost:
        android.util.Log.w("MetadataUtil", r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x016f, code lost:
        if (r3 != 1631670868) goto L_0x0179;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0171, code lost:
        r4 = X.C95144dD.A02(r11, "TPE2", r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x017c, code lost:
        if (r3 != 1936682605) goto L_0x0186;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x017e, code lost:
        r4 = X.C95144dD.A02(r11, "TSOT", r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0189, code lost:
        if (r3 != 1936679276) goto L_0x0193;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x018b, code lost:
        r4 = X.C95144dD.A02(r11, "TSO2", r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x0196, code lost:
        if (r3 != 1936679282) goto L_0x01a0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0198, code lost:
        r4 = X.C95144dD.A02(r11, "TSOA", r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x01a3, code lost:
        if (r3 != 1936679265) goto L_0x01ad;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x01a5, code lost:
        r4 = X.C95144dD.A02(r11, "TSOP", r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x01b0, code lost:
        if (r3 != 1936679791) goto L_0x01ba;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x01b2, code lost:
        r4 = X.C95144dD.A02(r11, "TSOC", r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x01bd, code lost:
        if (r3 != 1920233063) goto L_0x01c7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x01bf, code lost:
        r4 = X.C95144dD.A00(r11, "ITUNESADVISORY", r3, false, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x01ca, code lost:
        if (r3 != 1885823344) goto L_0x01d4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x01cc, code lost:
        r4 = X.C95144dD.A00(r11, "ITUNESGAPLESS", r3, false, true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x01d7, code lost:
        if (r3 != 1936683886) goto L_0x01e1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x01d9, code lost:
        r4 = X.C95144dD.A02(r11, "TVSHOWSORT", r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x01e4, code lost:
        if (r3 != 1953919848) goto L_0x01ee;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x01e6, code lost:
        r4 = X.C95144dD.A02(r11, "TVSHOW", r3);
     */
    /* JADX WARNING: Removed duplicated region for block: B:253:0x052a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A01(long r33) {
        /*
        // Method dump skipped, instructions count: 1574
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C106974wa.A01(long):void");
    }

    @Override // X.AnonymousClass5WY
    public long ACc() {
        return this.A0A;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002b, code lost:
        if (r3 == -1) goto L_0x002d;
     */
    @Override // X.AnonymousClass5WY
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C92684Xa AGX(long r13) {
        /*
            r12 = this;
            X.4Qz[] r1 = r12.A0D
            int r0 = r1.length
            if (r0 == 0) goto L_0x002d
            int r0 = r12.A03
            r4 = -1
            r10 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            if (r0 == r4) goto L_0x005d
            r0 = r1[r0]
            X.4W6 r9 = r0.A03
            long[] r2 = r9.A07
            r1 = 1
            r0 = 0
            int r3 = X.AnonymousClass3JZ.A06(r2, r13, r0)
        L_0x001b:
            if (r3 < 0) goto L_0x0027
            int[] r0 = r9.A04
            r0 = r0[r3]
            r0 = r0 & r1
            if (r0 != 0) goto L_0x0035
            int r3 = r3 + -1
            goto L_0x001b
        L_0x0027:
            int r3 = r9.A00(r13)
            if (r3 != r4) goto L_0x0035
        L_0x002d:
            X.4bc r1 = X.C94324bc.A02
            X.4Xa r0 = new X.4Xa
            r0.<init>(r1, r1)
            return r0
        L_0x0035:
            long[] r2 = r9.A07
            r7 = r2[r3]
            long[] r1 = r9.A06
            r5 = r1[r3]
            int r0 = (r7 > r13 ? 1 : (r7 == r13 ? 0 : -1))
            if (r0 >= 0) goto L_0x0055
            int r0 = r9.A01
            int r0 = r0 + -1
            if (r3 >= r0) goto L_0x0055
            int r0 = r9.A00(r13)
            if (r0 == r4) goto L_0x0055
            if (r0 == r3) goto L_0x0055
            r3 = r2[r0]
            r1 = r1[r0]
        L_0x0053:
            r13 = r7
            goto L_0x0069
        L_0x0055:
            r1 = -1
            r3 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            goto L_0x0053
        L_0x005d:
            r5 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r1 = -1
            r3 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
        L_0x0069:
            r8 = 0
        L_0x006a:
            X.4Qz[] r7 = r12.A0D
            int r0 = r7.length
            if (r8 >= r0) goto L_0x0086
            int r0 = r12.A03
            if (r8 == r0) goto L_0x0083
            r0 = r7[r8]
            X.4W6 r7 = r0.A03
            long r5 = A00(r7, r13, r5)
            int r0 = (r3 > r10 ? 1 : (r3 == r10 ? 0 : -1))
            if (r0 == 0) goto L_0x0083
            long r1 = A00(r7, r3, r1)
        L_0x0083:
            int r8 = r8 + 1
            goto L_0x006a
        L_0x0086:
            X.4bc r7 = new X.4bc
            r7.<init>(r13, r5)
            int r0 = (r3 > r10 ? 1 : (r3 == r10 ? 0 : -1))
            if (r0 != 0) goto L_0x0095
            X.4Xa r0 = new X.4Xa
            r0.<init>(r7, r7)
            return r0
        L_0x0095:
            X.4Xa r0 = X.C92684Xa.A00(r7, r3, r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C106974wa.AGX(long):X.4Xa");
    }

    @Override // X.AbstractC116785Ww
    public void AIa(AbstractC14070ko r1) {
        this.A0B = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:234:0x044f, code lost:
        if (r5 == false) goto L_0x0451;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:236:0x0453, code lost:
        if (r12 < r16) goto L_0x0455;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:237:0x0455, code lost:
        r5 = r1;
        r16 = r12;
        r22 = r6;
        r18 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:239:0x045e, code lost:
        if (r7 >= r20) goto L_0x0464;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:240:0x0460, code lost:
        r14 = r1;
        r3 = r6;
        r20 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:243:0x0468, code lost:
        if (true == r5) goto L_0x0451;
     */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x0220  */
    /* JADX WARNING: Removed duplicated region for block: B:303:0x046b A[SYNTHETIC] */
    @Override // X.AbstractC116785Ww
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int AZn(X.AnonymousClass5Yf r27, X.AnonymousClass4IG r28) {
        /*
        // Method dump skipped, instructions count: 1456
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C106974wa.AZn(X.5Yf, X.4IG):int");
    }

    @Override // X.AbstractC116785Ww
    public void AbQ(long j, long j2) {
        this.A0K.clear();
        this.A00 = 0;
        this.A08 = -1;
        this.A05 = 0;
        this.A06 = 0;
        this.A07 = 0;
        if (j != 0) {
            C91244Qz[] r7 = this.A0D;
            if (r7 != null) {
                for (C91244Qz r4 : r7) {
                    AnonymousClass4W6 r3 = r4.A03;
                    int A06 = AnonymousClass3JZ.A06(r3.A07, j2, false);
                    while (true) {
                        if (A06 < 0) {
                            A06 = r3.A00(j2);
                            break;
                        } else if ((r3.A04[A06] & 1) == 0) {
                            A06--;
                        }
                    }
                    r4.A00 = A06;
                }
            }
        } else if (this.A04 != 3) {
            this.A04 = 0;
            this.A00 = 0;
        } else {
            C93574aO r1 = this.A0F;
            r1.A02.clear();
            r1.A00 = 0;
            this.A0L.clear();
        }
    }

    @Override // X.AbstractC116785Ww
    public boolean Ae5(AnonymousClass5Yf r2) {
        return AnonymousClass4Z0.A00(r2, false);
    }
}
