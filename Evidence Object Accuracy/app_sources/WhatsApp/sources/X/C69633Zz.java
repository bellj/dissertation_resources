package X;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.status.StatusesFragment;
import com.whatsapp.util.Log;
import java.util.List;

/* renamed from: X.3Zz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69633Zz implements AbstractC116185Ul {
    public final long A00;
    public final /* synthetic */ StatusesFragment A01;

    public C69633Zz(StatusesFragment statusesFragment, long j) {
        this.A01 = statusesFragment;
        this.A00 = j;
    }

    @Override // X.AbstractC116185Ul
    public View AHa(Context context, View view, ViewGroup viewGroup, AnonymousClass1J1 r14, List list, List list2, List list3, List list4, boolean z) {
        int i;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.list_section_status, viewGroup, false);
            AnonymousClass028.A0a(view, 1);
        }
        TextView A0J = C12960it.A0J(view, R.id.title);
        C27531Hw.A06(A0J);
        long j = this.A00;
        if (j == 0) {
            i = R.string.recent_updates;
        } else if (j == 1) {
            i = R.string.viewed_updates;
        } else {
            if (j != 2) {
                Log.e(C12970iu.A0w(C12960it.A0k("statusesFragment/invalid id: "), j));
            }
            i = R.string.muted_updates;
        }
        A0J.setText(i);
        ImageView A0L = C12970iu.A0L(view, R.id.muted_statuses_icon);
        AnonymousClass028.A0a(A0L, 2);
        StatusesFragment statusesFragment = this.A01;
        if (!statusesFragment.A11 || j != 2 || statusesFragment.A0z) {
            A0L.setVisibility(4);
            AnonymousClass028.A0g(view, null);
            view.setOnClickListener(null);
            view.setClickable(false);
        } else {
            A0L.setVisibility(0);
            boolean z2 = statusesFragment.A0y;
            int i2 = R.string.accessibility_hide_muted_statuses_prompt;
            if (z2) {
                i2 = R.string.accessibility_show_muted_statuses_prompt;
            }
            AnonymousClass23N.A02(view, i2);
            boolean z3 = statusesFragment.A0y;
            Resources A02 = statusesFragment.A02();
            int i3 = R.drawable.ic_chevron_up;
            if (z3) {
                i3 = R.drawable.ic_chevron_down;
            }
            A0L.setImageDrawable(A02.getDrawable(i3));
            view.setClickable(true);
            C12960it.A14(view, this, A0L, 39);
        }
        AnonymousClass23N.A04(view, true);
        return view;
    }
}
