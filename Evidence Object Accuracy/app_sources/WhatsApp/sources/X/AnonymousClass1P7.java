package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.1P7  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1P7 extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass1P7 A0D;
    public static volatile AnonymousClass255 A0E;
    public byte A00 = -1;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public AbstractC27881Jp A06;
    public AnonymousClass1K6 A07;
    public AnonymousClass1K6 A08;
    public AnonymousClass1K6 A09;
    public AnonymousClass1K6 A0A;
    public AnonymousClass1K6 A0B;
    public C57632nN A0C;

    static {
        AnonymousClass1P7 r0 = new AnonymousClass1P7();
        A0D = r0;
        r0.A0W();
    }

    public AnonymousClass1P7() {
        AnonymousClass277 r1 = AnonymousClass277.A01;
        this.A07 = r1;
        this.A0B = r1;
        this.A09 = r1;
        this.A06 = AbstractC27881Jp.A01;
        this.A0A = r1;
        this.A08 = r1;
    }

    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r8, Object obj, Object obj2) {
        C82433vc r1;
        switch (r8.ordinal()) {
            case 0:
                byte b = this.A00;
                if (b != 1) {
                    if (b != 0) {
                        boolean booleanValue = ((Boolean) obj).booleanValue();
                        if ((this.A01 & 1) == 1) {
                            int i = 0;
                            while (true) {
                                if (i >= this.A07.size()) {
                                    for (int i2 = 0; i2 < this.A0B.size(); i2++) {
                                        if (((AbstractC27091Fz) this.A0B.get(i2)).A0Z()) {
                                        }
                                    }
                                    if (booleanValue) {
                                        this.A00 = 1;
                                        break;
                                    }
                                } else if (((AbstractC27091Fz) this.A07.get(i)).A0Z()) {
                                    i++;
                                }
                            }
                        }
                        if (booleanValue) {
                            this.A00 = 0;
                        }
                    }
                    return null;
                }
                break;
            case 1:
                AbstractC462925h r9 = (AbstractC462925h) obj;
                AnonymousClass1P7 r10 = (AnonymousClass1P7) obj2;
                boolean z = true;
                if ((this.A01 & 1) != 1) {
                    z = false;
                }
                int i3 = this.A04;
                boolean z2 = true;
                if ((r10.A01 & 1) != 1) {
                    z2 = false;
                }
                this.A04 = r9.Afp(i3, r10.A04, z, z2);
                this.A07 = r9.Afr(this.A07, r10.A07);
                this.A0B = r9.Afr(this.A0B, r10.A0B);
                int i4 = this.A01;
                boolean z3 = false;
                if ((i4 & 2) == 2) {
                    z3 = true;
                }
                int i5 = this.A02;
                int i6 = r10.A01;
                boolean z4 = false;
                if ((i6 & 2) == 2) {
                    z4 = true;
                }
                this.A02 = r9.Afp(i5, r10.A02, z3, z4);
                boolean z5 = false;
                if ((i4 & 4) == 4) {
                    z5 = true;
                }
                int i7 = this.A03;
                boolean z6 = false;
                if ((i6 & 4) == 4) {
                    z6 = true;
                }
                this.A03 = r9.Afp(i7, r10.A03, z5, z6);
                this.A09 = r9.Afr(this.A09, r10.A09);
                this.A0C = (C57632nN) r9.Aft(this.A0C, r10.A0C);
                boolean z7 = false;
                if ((this.A01 & 16) == 16) {
                    z7 = true;
                }
                AbstractC27881Jp r3 = this.A06;
                boolean z8 = false;
                if ((r10.A01 & 16) == 16) {
                    z8 = true;
                }
                this.A06 = r9.Afm(r3, r10.A06, z7, z8);
                boolean z9 = false;
                if ((this.A01 & 32) == 32) {
                    z9 = true;
                }
                int i8 = this.A05;
                boolean z10 = false;
                if ((r10.A01 & 32) == 32) {
                    z10 = true;
                }
                this.A05 = r9.Afp(i8, r10.A05, z9, z10);
                this.A0A = r9.Afr(this.A0A, r10.A0A);
                this.A08 = r9.Afr(this.A08, r10.A08);
                if (r9 == C463025i.A00) {
                    this.A01 |= r10.A01;
                }
                return this;
            case 2:
                AnonymousClass253 r92 = (AnonymousClass253) obj;
                AnonymousClass254 r102 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        int A03 = r92.A03();
                        switch (A03) {
                            case 0:
                                break;
                            case 8:
                                int A02 = r92.A02();
                                if (AnonymousClass1P9.A00(A02) == null) {
                                    super.A0X(1, A02);
                                    break;
                                } else {
                                    this.A01 |= 1;
                                    this.A04 = A02;
                                    break;
                                }
                            case 18:
                                AnonymousClass1K6 r12 = this.A07;
                                if (!((AnonymousClass1K7) r12).A00) {
                                    r12 = AbstractC27091Fz.A0G(r12);
                                    this.A07 = r12;
                                }
                                r12.add((AnonymousClass1PB) r92.A09(r102, AnonymousClass1PB.A0e.A0U()));
                                break;
                            case 26:
                                AnonymousClass1K6 r13 = this.A0B;
                                if (!((AnonymousClass1K7) r13).A00) {
                                    r13 = AbstractC27091Fz.A0G(r13);
                                    this.A0B = r13;
                                }
                                r13.add((AnonymousClass1G6) r92.A09(r102, AnonymousClass1G6.A0k.A0U()));
                                break;
                            case 40:
                                this.A01 |= 2;
                                this.A02 = r92.A02();
                                break;
                            case 48:
                                this.A01 |= 4;
                                this.A03 = r92.A02();
                                break;
                            case 58:
                                AnonymousClass1K6 r14 = this.A09;
                                if (!((AnonymousClass1K7) r14).A00) {
                                    r14 = AbstractC27091Fz.A0G(r14);
                                    this.A09 = r14;
                                }
                                r14.add((C40901sW) r92.A09(r102, C40901sW.A03.A0U()));
                                break;
                            case 66:
                                if ((this.A01 & 8) == 8) {
                                    r1 = (C82433vc) this.A0C.A0T();
                                } else {
                                    r1 = null;
                                }
                                C57632nN r0 = (C57632nN) r92.A09(r102, C57632nN.A0B.A0U());
                                this.A0C = r0;
                                if (r1 != null) {
                                    r1.A04(r0);
                                    this.A0C = (C57632nN) r1.A01();
                                }
                                this.A01 |= 8;
                                break;
                            case 74:
                                this.A01 |= 16;
                                this.A06 = r92.A08();
                                break;
                            case 80:
                                this.A01 |= 32;
                                this.A05 = r92.A02();
                                break;
                            case 90:
                                AnonymousClass1K6 r15 = this.A0A;
                                if (!((AnonymousClass1K7) r15).A00) {
                                    r15 = AbstractC27091Fz.A0G(r15);
                                    this.A0A = r15;
                                }
                                r15.add((AnonymousClass1PN) r92.A09(r102, AnonymousClass1PN.A0C.A0U()));
                                break;
                            case 98:
                                AnonymousClass1K6 r16 = this.A08;
                                if (!((AnonymousClass1K7) r16).A00) {
                                    r16 = AbstractC27091Fz.A0G(r16);
                                    this.A08 = r16;
                                }
                                r16.add((AnonymousClass1PP) r92.A09(r102, AnonymousClass1PP.A03.A0U()));
                                break;
                            default:
                                if (!A0a(r92, A03)) {
                                    break;
                                } else {
                                    break;
                                }
                        }
                    } catch (C28971Pt e) {
                        e.unfinishedMessage = this;
                        throw new RuntimeException(e);
                    } catch (IOException e2) {
                        C28971Pt r17 = new C28971Pt(e2.getMessage());
                        r17.unfinishedMessage = this;
                        throw new RuntimeException(r17);
                    }
                }
            case 3:
                ((AnonymousClass1K7) this.A07).A00 = false;
                ((AnonymousClass1K7) this.A0B).A00 = false;
                ((AnonymousClass1K7) this.A09).A00 = false;
                ((AnonymousClass1K7) this.A0A).A00 = false;
                ((AnonymousClass1K7) this.A08).A00 = false;
                return null;
            case 4:
                return new AnonymousClass1P7();
            case 5:
                return new AnonymousClass1P8();
            case 6:
                break;
            case 7:
                if (A0E == null) {
                    synchronized (AnonymousClass1P7.class) {
                        if (A0E == null) {
                            A0E = new AnonymousClass255(A0D);
                        }
                    }
                }
                return A0E;
            default:
                throw new UnsupportedOperationException();
        }
        return A0D;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i;
        int i2 = ((AbstractC27091Fz) this).A00;
        if (i2 != -1) {
            return i2;
        }
        if ((this.A01 & 1) == 1) {
            i = CodedOutputStream.A02(1, this.A04) + 0;
        } else {
            i = 0;
        }
        for (int i3 = 0; i3 < this.A07.size(); i3++) {
            i += CodedOutputStream.A0A((AnonymousClass1G1) this.A07.get(i3), 2);
        }
        for (int i4 = 0; i4 < this.A0B.size(); i4++) {
            i += CodedOutputStream.A0A((AnonymousClass1G1) this.A0B.get(i4), 3);
        }
        int i5 = this.A01;
        if ((i5 & 2) == 2) {
            i += CodedOutputStream.A04(5, this.A02);
        }
        if ((i5 & 4) == 4) {
            i += CodedOutputStream.A04(6, this.A03);
        }
        for (int i6 = 0; i6 < this.A09.size(); i6++) {
            i += CodedOutputStream.A0A((AnonymousClass1G1) this.A09.get(i6), 7);
        }
        if ((this.A01 & 8) == 8) {
            C57632nN r0 = this.A0C;
            if (r0 == null) {
                r0 = C57632nN.A0B;
            }
            i += CodedOutputStream.A0A(r0, 8);
        }
        int i7 = this.A01;
        if ((i7 & 16) == 16) {
            i += CodedOutputStream.A09(this.A06, 9);
        }
        if ((i7 & 32) == 32) {
            i += CodedOutputStream.A04(10, this.A05);
        }
        for (int i8 = 0; i8 < this.A0A.size(); i8++) {
            i += CodedOutputStream.A0A((AnonymousClass1G1) this.A0A.get(i8), 11);
        }
        for (int i9 = 0; i9 < this.A08.size(); i9++) {
            i += CodedOutputStream.A0A((AnonymousClass1G1) this.A08.get(i9), 12);
        }
        int A00 = i + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A01 & 1) == 1) {
            codedOutputStream.A0E(1, this.A04);
        }
        for (int i = 0; i < this.A07.size(); i++) {
            codedOutputStream.A0L((AnonymousClass1G1) this.A07.get(i), 2);
        }
        for (int i2 = 0; i2 < this.A0B.size(); i2++) {
            codedOutputStream.A0L((AnonymousClass1G1) this.A0B.get(i2), 3);
        }
        if ((this.A01 & 2) == 2) {
            codedOutputStream.A0F(5, this.A02);
        }
        if ((this.A01 & 4) == 4) {
            codedOutputStream.A0F(6, this.A03);
        }
        for (int i3 = 0; i3 < this.A09.size(); i3++) {
            codedOutputStream.A0L((AnonymousClass1G1) this.A09.get(i3), 7);
        }
        if ((this.A01 & 8) == 8) {
            C57632nN r0 = this.A0C;
            if (r0 == null) {
                r0 = C57632nN.A0B;
            }
            codedOutputStream.A0L(r0, 8);
        }
        if ((this.A01 & 16) == 16) {
            codedOutputStream.A0K(this.A06, 9);
        }
        if ((this.A01 & 32) == 32) {
            codedOutputStream.A0F(10, this.A05);
        }
        for (int i4 = 0; i4 < this.A0A.size(); i4++) {
            codedOutputStream.A0L((AnonymousClass1G1) this.A0A.get(i4), 11);
        }
        for (int i5 = 0; i5 < this.A08.size(); i5++) {
            codedOutputStream.A0L((AnonymousClass1G1) this.A08.get(i5), 12);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
