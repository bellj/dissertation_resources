package X;

import android.os.Handler;
import android.os.SystemClock;
import com.facebook.redex.RunnableBRunnable0Shape0S1100200_I1;
import com.facebook.redex.RunnableBRunnable0Shape1S1100000_I1;
import com.facebook.redex.RunnableBRunnable0Shape3S0300000_I1;
import com.google.android.exoplayer2.decoder.SimpleOutputBuffer;
import com.google.android.exoplayer2.ext.opus.OpusDecoder;

/* renamed from: X.3lq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C76563lq extends AbstractC106444vi implements AbstractC116615Wd {
    public int A00;
    public int A01;
    public int A02;
    public long A03;
    public C100614mC A04;
    public AnonymousClass5XF A05;
    public AnonymousClass4U3 A06;
    public C76763mA A07;
    public SimpleOutputBuffer A08;
    public AnonymousClass5Q2 A09;
    public AnonymousClass5Q2 A0A;
    public boolean A0B;
    public boolean A0C;
    public boolean A0D;
    public boolean A0E;
    public boolean A0F;
    public boolean A0G;
    public final C92474Wb A0H;
    public final AnonymousClass5XY A0I;
    public final C76763mA A0J;

    @Override // X.AbstractC117055Yb, X.AnonymousClass5WX
    public String getName() {
        return "LibopusAudioRenderer";
    }

    public C76563lq(Handler handler, AbstractC72373eU r3, AnonymousClass5XY r4) {
        this(handler, r3, r4, 0);
    }

    public C76563lq(Handler handler, AbstractC72373eU r5, AnonymousClass5XY r6, int i) {
        super(1);
        this.A0H = new C92474Wb(handler, r5);
        this.A0I = r6;
        ((C106534vr) r6).A0G = new C106514vp(this);
        this.A0J = new C76763mA(0);
        this.A00 = 0;
        this.A0D = true;
    }

    @Override // X.AbstractC106444vi
    public void A08() {
        this.A04 = null;
        this.A0D = true;
        try {
            this.A0A = null;
            A0C();
            this.A0I.reset();
        } finally {
            this.A0H.A00(this.A06);
        }
    }

    @Override // X.AbstractC106444vi
    public void A09(long j, boolean z) {
        this.A0I.flush();
        this.A03 = j;
        this.A0B = true;
        this.A0C = true;
        this.A0F = false;
        this.A0G = false;
        if (this.A05 == null) {
            return;
        }
        if (this.A00 != 0) {
            A0C();
            A0B();
            return;
        }
        this.A07 = null;
        SimpleOutputBuffer simpleOutputBuffer = this.A08;
        if (simpleOutputBuffer != null) {
            simpleOutputBuffer.release();
            this.A08 = null;
        }
        this.A05.flush();
        this.A0E = false;
    }

    @Override // X.AbstractC106444vi
    public void A0A(boolean z, boolean z2) {
        AnonymousClass4U3 r3 = new AnonymousClass4U3();
        this.A06 = r3;
        C92474Wb r2 = this.A0H;
        Handler handler = r2.A00;
        if (handler != null) {
            C12980iv.A18(handler, r2, r3, 4);
        }
        boolean z3 = super.A04.A00;
        C106534vr r32 = (C106534vr) this.A0I;
        if (z3) {
            C95314dV.A04(C12990iw.A1X(AnonymousClass3JZ.A01, 21));
            C95314dV.A04(r32.A0Q);
            if (!r32.A0X) {
                r32.A0X = true;
            } else {
                return;
            }
        } else if (r32.A0X) {
            r32.A0X = false;
        } else {
            return;
        }
        r32.flush();
    }

    public final void A0B() {
        if (this.A05 == null) {
            AnonymousClass5Q2 r0 = this.A0A;
            this.A09 = r0;
            if (r0 == null || ((C106604vy) r0).A00 != null) {
                try {
                    long elapsedRealtime = SystemClock.elapsedRealtime();
                    C95074d5.A02("createAudioDecoder");
                    C100614mC r4 = this.A04;
                    C95074d5.A02("createOpusDecoder");
                    boolean A1V = C12960it.A1V(this.A0I.AD7(AnonymousClass3JZ.A08(4, r4.A06, r4.A0F)), 2);
                    int i = r4.A0A;
                    if (i == -1) {
                        i = 5760;
                    }
                    OpusDecoder opusDecoder = new OpusDecoder(r4.A0U, i, A1V);
                    C95074d5.A00();
                    this.A05 = opusDecoder;
                    C95074d5.A00();
                    long elapsedRealtime2 = SystemClock.elapsedRealtime();
                    C92474Wb r7 = this.A0H;
                    String name = this.A05.getName();
                    long j = elapsedRealtime2 - elapsedRealtime;
                    Handler handler = r7.A00;
                    if (handler != null) {
                        handler.post(new RunnableBRunnable0Shape0S1100200_I1(r7, name, 0, elapsedRealtime2, j));
                    }
                    this.A06.A00++;
                } catch (AnonymousClass4CJ | OutOfMemoryError e) {
                    throw A01(this.A04, e, false);
                }
            }
        }
    }

    public final void A0C() {
        this.A07 = null;
        this.A08 = null;
        this.A00 = 0;
        this.A0E = false;
        AnonymousClass5XF r2 = this.A05;
        if (r2 != null) {
            this.A06.A01++;
            r2.release();
            C92474Wb r4 = this.A0H;
            String name = this.A05.getName();
            Handler handler = r4.A00;
            if (handler != null) {
                handler.post(new RunnableBRunnable0Shape1S1100000_I1(0, name, r4));
            }
            this.A05 = null;
        }
        this.A09 = null;
    }

    public final void A0D() {
        long ACA = this.A0I.ACA(AJN());
        if (ACA != Long.MIN_VALUE) {
            if (!this.A0C) {
                ACA = Math.max(this.A03, ACA);
            }
            this.A03 = ACA;
            this.A0C = false;
        }
    }

    public final void A0E(C89864Lr r12) {
        int i;
        Handler handler;
        RunnableBRunnable0Shape3S0300000_I1 runnableBRunnable0Shape3S0300000_I1;
        C100614mC r7 = r12.A00;
        AnonymousClass5Q2 r2 = r12.A01;
        this.A0A = r2;
        C100614mC r6 = this.A04;
        this.A04 = r7;
        this.A01 = r7.A07;
        this.A02 = r7.A08;
        AnonymousClass5XF r1 = this.A05;
        if (r1 == null) {
            A0B();
            C92474Wb r5 = this.A0H;
            C100614mC r22 = this.A04;
            handler = r5.A00;
            if (handler != null) {
                runnableBRunnable0Shape3S0300000_I1 = new RunnableBRunnable0Shape3S0300000_I1(r5, r22, null, 1);
            } else {
                return;
            }
        } else {
            AnonymousClass5Q2 r0 = this.A09;
            String name = r1.getName();
            if (r2 != r0) {
                i = 128;
            } else {
                i = 1;
            }
            AnonymousClass4XN r52 = new AnonymousClass4XN(r6, r7, name, 0, i);
            if (this.A0E) {
                this.A00 = 1;
            } else {
                A0C();
                A0B();
                this.A0D = true;
            }
            C92474Wb r23 = this.A0H;
            C100614mC r13 = this.A04;
            handler = r23.A00;
            if (handler != null) {
                runnableBRunnable0Shape3S0300000_I1 = new RunnableBRunnable0Shape3S0300000_I1(r23, r13, r52, 1);
            } else {
                return;
            }
        }
        handler.post(runnableBRunnable0Shape3S0300000_I1);
    }

    @Override // X.AbstractC116615Wd
    public C94344be AFk() {
        return ((C106534vr) this.A0I).A08().A02;
    }

    @Override // X.AbstractC116615Wd
    public long AFr() {
        if (super.A01 == 2) {
            A0D();
        }
        return this.A03;
    }

    @Override // X.AbstractC117055Yb
    public boolean AJN() {
        if (!this.A0G) {
            return false;
        }
        C106534vr r1 = (C106534vr) this.A0I;
        if (r1.A0D != null) {
            return r1.A0R && !r1.AIH();
        }
        return true;
    }

    @Override // X.AbstractC117055Yb
    public boolean AJx() {
        boolean AJx;
        if (this.A0I.AIH()) {
            return true;
        }
        if (this.A04 == null) {
            return false;
        }
        if (AIJ()) {
            AJx = super.A06;
        } else {
            AJx = super.A05.AJx();
        }
        if (AJx || this.A08 != null) {
            return true;
        }
        return false;
    }

    @Override // X.AbstractC117055Yb
    public void AaS(long j, long j2) {
        if (this.A0G) {
            try {
                C106534vr r1 = (C106534vr) this.A0I;
                if (!r1.A0R && r1.A0D != null && r1.A0F()) {
                    r1.A09();
                    r1.A0R = true;
                }
            } catch (AnonymousClass4C4 e) {
                throw A01(e.format, e, e.isRecoverable);
            }
        } else {
            if (this.A04 == null) {
                C89864Lr r4 = super.A0A;
                r4.A01 = null;
                r4.A00 = null;
                C76763mA r3 = this.A0J;
                r3.clear();
                int A00 = A00(r4, r3, true);
                if (A00 == -5) {
                    A0E(r4);
                } else if (A00 == -4) {
                    C95314dV.A04(AnonymousClass4YO.A00(r3));
                    this.A0F = true;
                    try {
                        this.A0G = true;
                        C106534vr r12 = (C106534vr) this.A0I;
                        if (!r12.A0R && r12.A0D != null && r12.A0F()) {
                            r12.A09();
                            r12.A0R = true;
                            return;
                        }
                        return;
                    } catch (AnonymousClass4C4 e2) {
                        throw A01(null, e2, false);
                    }
                } else {
                    return;
                }
            }
            A0B();
            if (this.A05 != null) {
                try {
                    C95074d5.A02("drainAndFeed");
                    while (true) {
                        SimpleOutputBuffer simpleOutputBuffer = this.A08;
                        if (simpleOutputBuffer == null) {
                            simpleOutputBuffer = (SimpleOutputBuffer) this.A05.A8o();
                            this.A08 = simpleOutputBuffer;
                            if (simpleOutputBuffer == null) {
                                break;
                            }
                            int i = simpleOutputBuffer.skippedOutputBufferCount;
                            if (i > 0) {
                                this.A06.A08 += i;
                                ((C106534vr) this.A0I).A0V = true;
                            }
                        }
                        if (!AnonymousClass4YO.A00(simpleOutputBuffer)) {
                            if (this.A0D) {
                                OpusDecoder opusDecoder = (OpusDecoder) this.A05;
                                int i2 = 2;
                                if (opusDecoder.A05) {
                                    i2 = 4;
                                }
                                C93844ap r2 = new C93844ap(AnonymousClass3JZ.A08(i2, opusDecoder.A01, 48000));
                                r2.A05 = this.A01;
                                r2.A06 = this.A02;
                                this.A0I.A7W(new C100614mC(r2), null, 0);
                                this.A0D = false;
                            }
                            AnonymousClass5XY r32 = this.A0I;
                            SimpleOutputBuffer simpleOutputBuffer2 = this.A08;
                            if (!r32.AHx(simpleOutputBuffer2.data, 1, simpleOutputBuffer2.timeUs)) {
                                break;
                            }
                            this.A06.A06++;
                            this.A08.release();
                            this.A08 = null;
                        } else if (this.A00 == 2) {
                            A0C();
                            A0B();
                            this.A0D = true;
                        } else {
                            simpleOutputBuffer.release();
                            this.A08 = null;
                            try {
                                this.A0G = true;
                                C106534vr r13 = (C106534vr) this.A0I;
                                if (!r13.A0R && r13.A0D != null && r13.A0F()) {
                                    r13.A09();
                                    r13.A0R = true;
                                }
                            } catch (AnonymousClass4C4 e3) {
                                throw A01(e3.format, e3, e3.isRecoverable);
                            }
                        }
                    }
                    while (true) {
                        AnonymousClass5XF r33 = this.A05;
                        if (r33 == null || this.A00 == 2 || this.A0F) {
                            break;
                        }
                        C76763mA r14 = this.A07;
                        if (r14 == null) {
                            r14 = (C76763mA) r33.A8n();
                            this.A07 = r14;
                            if (r14 == null) {
                                break;
                            }
                        }
                        if (this.A00 == 1) {
                            r14.flags = 4;
                            this.A05.AZl(r14);
                            this.A07 = null;
                            this.A00 = 2;
                            break;
                        }
                        C89864Lr r22 = super.A0A;
                        r22.A01 = null;
                        r22.A00 = null;
                        int A002 = A00(r22, r14, false);
                        if (A002 == -5) {
                            A0E(r22);
                        } else if (A002 == -4) {
                            C76763mA r15 = this.A07;
                            if (AnonymousClass4YO.A00(r15)) {
                                this.A0F = true;
                                this.A05.AZl(r15);
                                this.A07 = null;
                                break;
                            }
                            r15.A00();
                            C76763mA r9 = this.A07;
                            if (this.A0B && !C12960it.A1V(r9.flags & Integer.MIN_VALUE, Integer.MIN_VALUE)) {
                                long j3 = r9.A00;
                                if (Math.abs(j3 - this.A03) > 500000) {
                                    this.A03 = j3;
                                }
                                this.A0B = false;
                            }
                            this.A05.AZl(r9);
                            this.A0E = true;
                            this.A06.A04++;
                            this.A07 = null;
                        } else if (A002 != -3) {
                            throw C72463ee.A0D();
                        }
                    }
                    C95074d5.A00();
                    synchronized (this.A06) {
                    }
                } catch (AnonymousClass4C3 e4) {
                    throw A01(e4.format, e4, e4.isRecoverable);
                } catch (AnonymousClass4C4 e5) {
                    throw A01(e5.format, e5, e5.isRecoverable);
                } catch (AnonymousClass4CG e6) {
                    throw A01(e6.format, e6, false);
                } catch (AnonymousClass4CJ e7) {
                    throw A01(this.A04, e7, false);
                }
            }
        }
    }

    @Override // X.AbstractC116615Wd
    public void AcX(C94344be r2) {
        this.A0I.AcX(r2);
    }

    @Override // X.AnonymousClass5WX
    public final int Aeb(C100614mC r6) {
        int i;
        String str = r6.A0T;
        int i2 = 0;
        if (!C95554dx.A03(str)) {
            return 0;
        }
        int i3 = 0;
        boolean z = true;
        if (r6.A0N != null) {
            z = false;
        }
        if ("audio/opus".equalsIgnoreCase(str)) {
            i3 = 2;
            if (((C106534vr) this.A0I).AD7(AnonymousClass3JZ.A08(2, r6.A06, r6.A0F)) == 0) {
                i3 = 1;
            } else if (z) {
                if (AnonymousClass3JZ.A01 >= 21) {
                    i2 = 32;
                }
                i = 12;
                return i | i2;
            }
        }
        i = i3 | 0;
        return i | i2;
    }
}
