package X;

/* renamed from: X.4B9  reason: invalid class name */
/* loaded from: classes3.dex */
public enum AnonymousClass4B9 {
    A02(0),
    A03(2),
    A01(1),
    /* Fake field, exist only in values array */
    EF35(3);
    
    public final int value;

    AnonymousClass4B9(int i) {
        this.value = i;
    }
}
