package X;

import android.content.Context;
import com.whatsapp.R;

/* renamed from: X.42C  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass42C extends C64533Fx {
    @Override // X.C64533Fx
    public int A02(Context context) {
        return 0;
    }

    @Override // X.C64533Fx
    public int A03(Context context) {
        return 0;
    }

    @Override // X.C64533Fx
    public boolean A04() {
        return false;
    }

    @Override // X.C64533Fx
    public boolean A07() {
        return true;
    }

    @Override // X.C64533Fx
    public boolean A08() {
        return true;
    }

    @Override // X.C64533Fx
    public boolean A0A() {
        return true;
    }

    public AnonymousClass42C(C16590pI r1, AnonymousClass018 r2) {
        super(r1, r2);
    }

    @Override // X.C64533Fx
    public int A00(Context context) {
        return context.getResources().getDimensionPixelSize(R.dimen.bubble_margin_starred);
    }

    @Override // X.C64533Fx
    public int A01(Context context) {
        return context.getResources().getDimensionPixelSize(R.dimen.starred_bubble_vertical_margin);
    }

    @Override // X.C64533Fx
    public boolean A06() {
        return C28141Kv.A00(this.A02);
    }

    @Override // X.C64533Fx
    public boolean A09() {
        return !C28141Kv.A00(this.A02);
    }
}
