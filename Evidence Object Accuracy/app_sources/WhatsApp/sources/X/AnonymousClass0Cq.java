package X;

import java.util.Arrays;

/* renamed from: X.0Cq  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Cq extends C07240Xf {
    public int A00 = 0;
    public AnonymousClass0NX A01;
    public C08940c6 A02 = new C08940c6(this, this);
    public AnonymousClass0QC[] A03 = new AnonymousClass0QC[128];
    public AnonymousClass0QC[] A04 = new AnonymousClass0QC[128];

    public AnonymousClass0Cq(AnonymousClass0NX r3) {
        super(r3);
        this.A01 = r3;
    }

    @Override // X.C07240Xf
    public void A01(C07240Xf r18, boolean z) {
        AnonymousClass0QC r6 = r18.A02;
        if (r6 != null) {
            AbstractC12730iP r5 = r18.A01;
            int ACD = r5.ACD();
            for (int i = 0; i < ACD; i++) {
                AnonymousClass0QC AHV = r5.AHV(i);
                float AHW = r5.AHW(i);
                C08940c6 r11 = this.A02;
                r11.A01 = AHV;
                boolean z2 = true;
                int i2 = 0;
                if (AHV.A07) {
                    int i3 = 0;
                    do {
                        float[] fArr = AHV.A09;
                        float f = fArr[i3] + (r6.A09[i3] * AHW);
                        fArr[i3] = f;
                        if (Math.abs(f) < 1.0E-4f) {
                            fArr[i3] = 0.0f;
                        } else {
                            z2 = false;
                        }
                        i3++;
                    } while (i3 < 9);
                    if (z2) {
                        r11.A02.A07(AHV);
                    }
                } else {
                    do {
                        float f2 = r6.A09[i2];
                        if (f2 != 0.0f) {
                            float f3 = f2 * AHW;
                            if (Math.abs(f3) < 1.0E-4f) {
                                f3 = 0.0f;
                            }
                            AHV.A09[i2] = f3;
                        } else {
                            AHV.A09[i2] = 0.0f;
                        }
                        i2++;
                    } while (i2 < 9);
                    A06(AHV);
                }
                super.A00 += r18.A00 * AHW;
            }
            A07(r6);
        }
    }

    public final void A06(AnonymousClass0QC r8) {
        AnonymousClass0QC[] r1;
        int i = this.A00 + 1;
        AnonymousClass0QC[] r12 = this.A03;
        int length = r12.length;
        if (i > length) {
            AnonymousClass0QC[] r13 = (AnonymousClass0QC[]) Arrays.copyOf(r12, length << 1);
            this.A03 = r13;
            this.A04 = (AnonymousClass0QC[]) Arrays.copyOf(r13, r13.length << 1);
        }
        AnonymousClass0QC[] r6 = this.A03;
        int i2 = this.A00;
        r6[i2] = r8;
        int i3 = i2 + 1;
        this.A00 = i3;
        if (i3 > 1 && r6[i3 - 1].A02 > r8.A02) {
            int i4 = 0;
            while (true) {
                r1 = this.A04;
                if (i4 >= i3) {
                    break;
                }
                r1[i4] = r6[i4];
                i4++;
            }
            Arrays.sort(r1, 0, i3, new C10350eT(this));
            for (int i5 = 0; i5 < this.A00; i5++) {
                this.A03[i5] = this.A04[i5];
            }
        }
        r8.A07 = true;
        r8.A01(this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0013, code lost:
        r1 = r4 + 1;
        r2[r4] = r2[r1];
        r4 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001b, code lost:
        r6.A00 = r0;
        r7.A07 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001f, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x000f, code lost:
        r0 = r3 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0011, code lost:
        if (r4 >= r0) goto L_0x001b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A07(X.AnonymousClass0QC r7) {
        /*
            r6 = this;
            r5 = 0
            r4 = 0
        L_0x0002:
            int r3 = r6.A00
            if (r4 >= r3) goto L_0x001f
            X.0QC[] r2 = r6.A03
            r0 = r2[r4]
            if (r0 == r7) goto L_0x000f
            int r4 = r4 + 1
            goto L_0x0002
        L_0x000f:
            int r0 = r3 + -1
            if (r4 >= r0) goto L_0x001b
            int r1 = r4 + 1
            r0 = r2[r1]
            r2[r4] = r0
            r4 = r1
            goto L_0x000f
        L_0x001b:
            r6.A00 = r0
            r7.A07 = r5
        L_0x001f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0Cq.A07(X.0QC):void");
    }

    @Override // X.C07240Xf, X.AbstractC11690gh
    public AnonymousClass0QC AFh(C06240Ss r10, boolean[] zArr) {
        int i = -1;
        for (int i2 = 0; i2 < this.A00; i2++) {
            AnonymousClass0QC[] r1 = this.A03;
            AnonymousClass0QC r8 = r1[i2];
            if (!zArr[r8.A02]) {
                this.A02.A01 = r8;
                if (i == -1) {
                    int i3 = 8;
                    do {
                        float f = r8.A09[i3];
                        if (f > 0.0f) {
                            break;
                        }
                        if (f < 0.0f) {
                            i = i2;
                            break;
                        }
                        i3--;
                    } while (i3 >= 0);
                } else {
                    AnonymousClass0QC r4 = r1[i];
                    int i4 = 8;
                    while (true) {
                        float f2 = r4.A09[i4];
                        float f3 = r8.A09[i4];
                        if (f3 == f2) {
                            i4--;
                            if (i4 < 0) {
                                break;
                            }
                        } else if (f3 >= f2) {
                        }
                    }
                }
            }
        }
        if (i == -1) {
            return null;
        }
        return this.A03[i];
    }

    @Override // X.C07240Xf
    public String toString() {
        StringBuilder sb = new StringBuilder("");
        sb.append(" goal -> (");
        sb.append(super.A00);
        sb.append(") : ");
        String obj = sb.toString();
        for (int i = 0; i < this.A00; i++) {
            AnonymousClass0QC r1 = this.A03[i];
            C08940c6 r0 = this.A02;
            r0.A01 = r1;
            StringBuilder sb2 = new StringBuilder();
            sb2.append(obj);
            sb2.append(r0);
            sb2.append(" ");
            obj = sb2.toString();
        }
        return obj;
    }
}
