package X;

import android.text.TextUtils;
import android.view.View;
import com.whatsapp.R;

/* renamed from: X.0DZ  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0DZ extends AnonymousClass0Q6 {
    public AnonymousClass0DZ() {
        super(CharSequence.class, R.id.tag_accessibility_pane_title, 8, 28);
    }

    @Override // X.AnonymousClass0Q6
    public Object A01(View view) {
        return AnonymousClass0US.A00(view);
    }

    @Override // X.AnonymousClass0Q6
    public void A03(View view, Object obj) {
        AnonymousClass0US.A02(view, (CharSequence) obj);
    }

    @Override // X.AnonymousClass0Q6
    public boolean A04(Object obj, Object obj2) {
        return !TextUtils.equals((CharSequence) obj, (CharSequence) obj2);
    }
}
