package X;

import android.text.TextUtils;

/* renamed from: X.1h5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C35011h5 {
    public static String A00(AbstractC15340mz r3) {
        String A01 = A01(r3);
        if (TextUtils.isEmpty(A01)) {
            return null;
        }
        StringBuilder sb = new StringBuilder(A01);
        C30441Xk r1 = r3.A0F().A00;
        if (!TextUtils.isEmpty(r1.A01)) {
            sb.append("\n");
            sb.append(r1.A01);
        }
        return sb.toString();
    }

    public static String A01(AbstractC15340mz r1) {
        C30441Xk r12;
        if (!r1.A0y() || (r12 = r1.A0F().A00) == null || TextUtils.isEmpty(r12.A00)) {
            return null;
        }
        return r12.A00;
    }

    public static String A02(C30441Xk r4, String str) {
        StringBuilder sb = new StringBuilder();
        if (r4 != null) {
            if (!TextUtils.isEmpty(str)) {
                sb.append(str);
            }
            String str2 = r4.A00;
            if (!TextUtils.isEmpty(str2)) {
                if (sb.length() > 0) {
                    sb.append("\n");
                }
                sb.append(str2);
            }
            String str3 = r4.A01;
            if (!TextUtils.isEmpty(str3)) {
                if (sb.length() > 0) {
                    sb.append("\n");
                }
                sb.append(str3);
            }
        }
        return sb.toString();
    }

    public static void A03(C56852m3 r7, C30441Xk r8) {
        AnonymousClass4B3 r2;
        if (r8 != null) {
            String str = r8.A00;
            if (str != null) {
                r7.A03();
                C57552nF r1 = (C57552nF) r7.A00;
                r1.A00 |= 32;
                r1.A06 = str;
            }
            String str2 = r8.A01;
            if (str2 != null) {
                r7.A03();
                C57552nF r12 = (C57552nF) r7.A00;
                r12.A00 |= 64;
                r12.A07 = str2;
            }
            for (AnonymousClass1ZN r5 : r8.A02) {
                AnonymousClass1G4 A0T = C57442n3.A05.A0T();
                String str3 = r5.A04;
                A0T.A03();
                C57442n3 r13 = (C57442n3) A0T.A00;
                r13.A00 |= 1;
                r13.A04 = str3;
                int i = r5.A01;
                if (i == 2) {
                    r2 = AnonymousClass4B3.A01;
                } else {
                    r2 = i == 1 ? AnonymousClass4B3.A02 : AnonymousClass4B3.A03;
                }
                A0T.A03();
                C57442n3 r14 = (C57442n3) A0T.A00;
                r14.A00 |= 4;
                r14.A01 = r2.value;
                AnonymousClass1G4 A0T2 = C56932mC.A02.A0T();
                String str4 = r5.A03;
                A0T2.A03();
                C56932mC r15 = (C56932mC) A0T2.A00;
                r15.A00 |= 1;
                r15.A01 = str4;
                A0T.A03();
                C57442n3 r16 = (C57442n3) A0T.A00;
                r16.A02 = (C56932mC) A0T2.A02();
                r16.A00 |= 2;
                AbstractC27091Fz A02 = A0T.A02();
                r7.A03();
                C57552nF r22 = (C57552nF) r7.A00;
                AnonymousClass1K6 r17 = r22.A03;
                if (!((AnonymousClass1K7) r17).A00) {
                    r17 = AbstractC27091Fz.A0G(r17);
                    r22.A03 = r17;
                }
                r17.add(A02);
            }
        }
    }

    public static boolean A04(AbstractC15340mz r1) {
        return r1.A0y() && r1.A0F().A00 != null;
    }
}
