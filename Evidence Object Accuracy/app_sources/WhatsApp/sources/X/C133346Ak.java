package X;

import com.whatsapp.util.Log;

/* renamed from: X.6Ak  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133346Ak implements AnonymousClass6MV {
    public final /* synthetic */ AbstractC129955yZ A00;

    public C133346Ak(AbstractC129955yZ r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass6MV
    public void APo(C452120p r4) {
        Log.e("PAY: BrazilVerifyCardOTPSendAction getProviderEncryptionKeyAsync iq returned null");
        this.A00.A01(C117305Zk.A0L(), null);
    }

    @Override // X.AnonymousClass6MV
    public void AVE(AnonymousClass6B7 r3) {
        this.A00.A01(null, r3);
    }
}
