package X;

/* renamed from: X.1a6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C31161a6 {
    public final C14830m7 A00;
    public final AnonymousClass1RO A01;

    public C31161a6(C14830m7 r1, AnonymousClass1RO r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public static String A00(AnonymousClass1IS r2) {
        StringBuilder sb = new StringBuilder("msg_key_remote_jid = ? AND recipient_id = ? AND recipient_type = ? AND device_id = ? AND msg_key_from_me");
        sb.append(r2.A02 ? " != " : " = ");
        sb.append("0 AND ");
        sb.append("msg_key_id");
        sb.append(" = ?");
        return sb.toString();
    }
}
