package X;

/* renamed from: X.4vd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C106394vd implements AnonymousClass5Pu {
    public float A00;
    public float A01;
    public float A02;
    public long A03;
    public long A04;
    public long A05;
    public long A06;
    public long A07;
    public long A08;
    public long A09;
    public long A0A;
    public long A0B;
    public final float A0C = 1.03f;
    public final float A0D = 0.97f;
    public final float A0E;
    public final float A0F = 1.0E-7f;
    public final long A0G;
    public final long A0H = 1000;
    public final long A0I;

    public /* synthetic */ C106394vd(long j, long j2) {
        this.A0G = j;
        this.A0I = j2;
        this.A0E = 0.999f;
        this.A07 = -9223372036854775807L;
        this.A0B = -9223372036854775807L;
        this.A08 = -9223372036854775807L;
        this.A06 = -9223372036854775807L;
        this.A02 = 0.97f;
        this.A01 = 1.03f;
        this.A00 = 1.0f;
        this.A05 = -9223372036854775807L;
        this.A04 = -9223372036854775807L;
        this.A03 = -9223372036854775807L;
        this.A0A = -9223372036854775807L;
        this.A09 = -9223372036854775807L;
    }

    public final void A00() {
        long j = this.A07;
        if (j != -9223372036854775807L) {
            long j2 = this.A0B;
            if (j2 != -9223372036854775807L) {
                j = j2;
            }
            long j3 = this.A08;
            if (j3 != -9223372036854775807L && j < j3) {
                j = j3;
            }
            long j4 = this.A06;
            if (j4 != -9223372036854775807L && j > j4) {
                j = j4;
            }
        } else {
            j = -9223372036854775807L;
        }
        if (this.A04 != j) {
            this.A04 = j;
            this.A03 = j;
            this.A0A = -9223372036854775807L;
            this.A09 = -9223372036854775807L;
            this.A05 = -9223372036854775807L;
        }
    }
}
