package X;

import com.facebook.redex.EmptyBaseRunnable0;

/* renamed from: X.2iT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class RunnableC55442iT extends EmptyBaseRunnable0 implements Runnable {
    public final ServiceConnectionC15180mh A00;

    public RunnableC55442iT(ServiceConnectionC15180mh r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00ae, code lost:
        return;
     */
    @Override // java.lang.Runnable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r10 = this;
            X.0mh r2 = r10.A00
        L_0x0002:
            monitor-enter(r2)
            int r0 = r2.A00     // Catch: all -> 0x00af
            r3 = 2
            if (r0 != r3) goto L_0x0013
            java.util.Queue r1 = r2.A04     // Catch: all -> 0x00af
            boolean r0 = r1.isEmpty()     // Catch: all -> 0x00af
            if (r0 == 0) goto L_0x0016
            r2.A00()     // Catch: all -> 0x00af
        L_0x0013:
            monitor-exit(r2)     // Catch: all -> 0x00af
            goto L_0x00ae
        L_0x0016:
            java.lang.Object r7 = r1.poll()     // Catch: all -> 0x00af
            X.0jy r7 = (X.AbstractC13590jy) r7     // Catch: all -> 0x00af
            android.util.SparseArray r0 = r2.A03     // Catch: all -> 0x00af
            int r4 = r7.A00     // Catch: all -> 0x00af
            r0.put(r4, r7)     // Catch: all -> 0x00af
            X.0jw r6 = r2.A05     // Catch: all -> 0x00af
            java.util.concurrent.ScheduledExecutorService r9 = r6.A03     // Catch: all -> 0x00af
            r0 = 26
            com.facebook.redex.RunnableBRunnable0Shape10S0200000_I1 r8 = new com.facebook.redex.RunnableBRunnable0Shape10S0200000_I1     // Catch: all -> 0x00af
            r8.<init>(r2, r0, r7)     // Catch: all -> 0x00af
            r0 = 30
            java.util.concurrent.TimeUnit r5 = java.util.concurrent.TimeUnit.SECONDS     // Catch: all -> 0x00af
            r9.schedule(r8, r0, r5)     // Catch: all -> 0x00af
            monitor-exit(r2)     // Catch: all -> 0x00af
            java.lang.String r8 = "MessengerIpcClient"
            r0 = 3
            boolean r0 = android.util.Log.isLoggable(r8, r0)
            if (r0 == 0) goto L_0x0059
            java.lang.String r5 = java.lang.String.valueOf(r7)
            int r0 = r5.length()
            int r0 = r0 + 8
            java.lang.StringBuilder r1 = X.C12980iv.A0t(r0)
            java.lang.String r0 = "Sending "
            r1.append(r0)
            java.lang.String r0 = X.C12960it.A0d(r5, r1)
            android.util.Log.d(r8, r0)
        L_0x0059:
            android.content.Context r6 = r6.A02
            android.os.Messenger r1 = r2.A02
            android.os.Message r5 = android.os.Message.obtain()
            int r0 = r7.A01
            r5.what = r0
            r5.arg1 = r4
            r5.replyTo = r1
            android.os.Bundle r4 = X.C12970iu.A0D()
            java.lang.String r1 = "oneWay"
            boolean r0 = r7.A03()
            r4.putBoolean(r1, r0)
            java.lang.String r1 = "pkg"
            java.lang.String r0 = r6.getPackageName()
            r4.putString(r1, r0)
            java.lang.String r1 = "data"
            android.os.Bundle r0 = r7.A02
            r4.putBundle(r1, r0)
            r5.setData(r4)
            X.3BJ r1 = r2.A01     // Catch: RemoteException -> 0x00a4
            android.os.Messenger r0 = r1.A00     // Catch: RemoteException -> 0x00a4
            if (r0 == 0) goto L_0x0094
            r0.send(r5)     // Catch: RemoteException -> 0x00a4
            goto L_0x0002
        L_0x0094:
            X.0jv r0 = r1.A01     // Catch: RemoteException -> 0x00a4
            if (r0 == 0) goto L_0x009d
            r0.A00(r5)     // Catch: RemoteException -> 0x00a4
            goto L_0x0002
        L_0x009d:
            java.lang.String r0 = "Both messengers are null"
            java.lang.IllegalStateException r0 = X.C12960it.A0U(r0)     // Catch: RemoteException -> 0x00a4
            throw r0     // Catch: RemoteException -> 0x00a4
        L_0x00a4:
            r0 = move-exception
            java.lang.String r0 = r0.getMessage()
            r2.A01(r3, r0)
            goto L_0x0002
        L_0x00ae:
            return
        L_0x00af:
            r0 = move-exception
            monitor-exit(r2)     // Catch: all -> 0x00af
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.RunnableC55442iT.run():void");
    }
}
