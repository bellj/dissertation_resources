package X;

/* renamed from: X.51r  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1094351r implements AnonymousClass5T7 {
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004b, code lost:
        if (r0 == 0) goto L_0x004d;
     */
    @Override // X.AnonymousClass5T7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A9i(X.AbstractC94534c0 r5, X.AbstractC94534c0 r6, X.AnonymousClass4RG r7) {
        /*
            r4 = this;
            boolean r0 = r5 instanceof X.C82973wU
            boolean r0 = X.C72473ef.A00(r0)
            r3 = 1
            r1 = 0
            if (r0 == 0) goto L_0x001f
            X.3wU r0 = r5.A06()
            java.lang.String r0 = r0.A01
            boolean r2 = r0.isEmpty()
        L_0x0014:
            boolean r0 = r6 instanceof X.C82953wS
            if (r0 != 0) goto L_0x005a
            java.lang.String r0 = "Expected boolean node"
            X.3wH r0 = X.C82843wH.A00(r0)
            throw r0
        L_0x001f:
            boolean r0 = r5 instanceof X.C83003wX
            if (r0 != 0) goto L_0x0024
            return r1
        L_0x0024:
            X.3wX r1 = r5.A03()
            java.lang.Object r0 = r1.A08()
            boolean r0 = r0 instanceof java.util.List
            r2 = 0
            if (r0 != 0) goto L_0x004f
            java.lang.Object r0 = r1.A08()
            boolean r0 = r0 instanceof java.util.Map
            if (r0 != 0) goto L_0x004f
            java.lang.Object r0 = r1.A08()
            boolean r0 = r0 instanceof java.lang.String
            if (r0 == 0) goto L_0x004d
            java.lang.Object r0 = r1.A08()
            java.lang.String r0 = (java.lang.String) r0
            int r0 = r0.length()
        L_0x004b:
            if (r0 != 0) goto L_0x0014
        L_0x004d:
            r2 = 1
            goto L_0x0014
        L_0x004f:
            java.lang.Object r0 = r1.A08()
            java.util.Collection r0 = (java.util.Collection) r0
            int r0 = r0.size()
            goto L_0x004b
        L_0x005a:
            X.3wS r6 = (X.C82953wS) r6
            java.lang.Boolean r0 = r6.A00
            boolean r0 = r0.booleanValue()
            if (r2 == r0) goto L_0x0065
            r3 = 0
        L_0x0065:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C1094351r.A9i(X.4c0, X.4c0, X.4RG):boolean");
    }
}
