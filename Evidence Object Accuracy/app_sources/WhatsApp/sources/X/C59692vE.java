package X;

/* renamed from: X.2vE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C59692vE extends AnonymousClass4UW {
    public final C30211Wn A00;
    public final boolean A01;

    public C59692vE(C30211Wn r2, boolean z) {
        C16700pc.A0E(r2, 2);
        this.A01 = z;
        this.A00 = r2;
    }

    @Override // X.AnonymousClass4UW
    public boolean A00() {
        return this.A01;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C59692vE) {
                C59692vE r5 = (C59692vE) obj;
                if (this.A01 != r5.A01 || !C16700pc.A0O(this.A00, r5.A00)) {
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        boolean z = this.A01;
        if (z) {
            z = true;
        }
        int i = z ? 1 : 0;
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        return C12990iw.A08(this.A00, i * 31);
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("SingleCategoryChip(isSelected=");
        A0k.append(this.A01);
        A0k.append(", category=");
        return C12960it.A0a(this.A00, A0k);
    }
}
