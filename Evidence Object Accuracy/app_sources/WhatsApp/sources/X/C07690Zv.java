package X;

import android.database.Cursor;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0Zv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C07690Zv implements AbstractC11980hB {
    public final AbstractC02880Ff A00;
    public final AnonymousClass0QN A01;

    public C07690Zv(AnonymousClass0QN r2) {
        this.A01 = r2;
        this.A00 = new AnonymousClass0FM(r2, this);
    }

    @Override // X.AbstractC11980hB
    public List ACW(String str) {
        AnonymousClass0ZJ A00 = AnonymousClass0ZJ.A00("SELECT work_spec_id FROM dependency WHERE prerequisite_id=?", 1);
        if (str == null) {
            A00.A6T(1);
        } else {
            A00.A6U(1, str);
        }
        AnonymousClass0QN r0 = this.A01;
        r0.A02();
        Cursor A002 = AnonymousClass0LC.A00(r0, A00, false);
        try {
            ArrayList arrayList = new ArrayList(A002.getCount());
            while (A002.moveToNext()) {
                arrayList.add(A002.getString(0));
            }
            return arrayList;
        } finally {
            A002.close();
            A00.A01();
        }
    }
}
