package X;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

/* renamed from: X.5F5  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass5F5 implements AbstractC12270he {
    public static final /* synthetic */ AtomicReferenceFieldUpdater A01 = AtomicReferenceFieldUpdater.newUpdater(AnonymousClass5F5.class, Object.class, "onCloseHandler");
    public final AnonymousClass5LE A00 = new AnonymousClass5LE();
    public volatile /* synthetic */ Object onCloseHandler = null;

    public String A01() {
        return "";
    }

    public Object A00(Object obj) {
        AnonymousClass5LD A03;
        do {
            A03 = A03();
            if (A03 == null) {
                return AnonymousClass4HY.A01;
            }
        } while (A03.A0A() == null);
        AnonymousClass5L7 r1 = (AnonymousClass5L7) A03;
        r1.A01.A00 = obj;
        C114215Kq r12 = r1.A00;
        r12.A07(((AnonymousClass5LK) r12).A00);
        return AnonymousClass4HY.A02;
    }

    public final AnonymousClass5LE A02() {
        return this.A00;
    }

    public AnonymousClass5LD A03() {
        C94524by r3;
        AnonymousClass5LE r4 = this.A00;
        while (true) {
            r3 = (C94524by) r4.A01();
            if (r3 == r4 || !(r3 instanceof AnonymousClass5LD)) {
                break;
            }
            C94524by A05 = r3.A05();
            if (A05 == null) {
                break;
            }
            while (true) {
                Object A012 = A05.A01();
                if (!(A012 instanceof AnonymousClass4V9)) {
                    break;
                }
                A05 = ((AnonymousClass4V9) A012).A00;
            }
            A05.A02();
        }
        r3 = null;
        return (AnonymousClass5LD) r3;
    }

    @Override // X.AbstractC12270he
    public final Object Af7(Object obj) {
        Object A00 = A00(obj);
        if (A00 == AnonymousClass4HY.A02) {
            return AnonymousClass1WZ.A00;
        }
        if (A00 == AnonymousClass4HY.A01) {
            this.A00.A04();
            return AbstractC88544Gb.A00;
        }
        throw C12960it.A0U(C16700pc.A08("trySend returned ", A00));
    }

    public String toString() {
        String A08;
        StringBuilder A0h = C12960it.A0h();
        A0h.append(C72453ed.A0p(this, A0h));
        A0h.append('{');
        C94524by r4 = this.A00;
        C94524by A03 = r4.A03();
        if (A03 == r4) {
            A08 = "EmptyQueue";
        } else {
            if (A03 instanceof AnonymousClass5LD) {
                A08 = "ReceiveQueued";
            } else {
                A08 = C16700pc.A08("UNEXPECTED:", A03);
            }
            if (r4.A04() != A03) {
                StringBuilder A0j = C12960it.A0j(A08);
                A0j.append(",queueSize=");
                int i = 0;
                for (C94524by r2 = (C94524by) r4.A01(); !C16700pc.A0O(r2, r4); r2 = r2.A03()) {
                    if (r2 != null) {
                        i++;
                    }
                }
                A08 = C12960it.A0f(A0j, i);
            }
        }
        A0h.append(A08);
        A0h.append('}');
        return C12960it.A0d(A01(), A0h);
    }
}
