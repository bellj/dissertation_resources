package X;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.text.TextUtils;
import android.util.Pair;
import com.whatsapp.stickers.WebpUtils;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;

/* renamed from: X.37f  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C624337f extends AbstractC16350or {
    public final AnonymousClass5UH A00;
    public final AnonymousClass1A3 A01;
    public final boolean A02 = true;
    public final /* synthetic */ AnonymousClass1A4 A03;

    public /* synthetic */ C624337f(AnonymousClass5UH r2, AnonymousClass1A4 r3, AnonymousClass1A3 r4) {
        this.A03 = r3;
        this.A01 = r4;
        this.A00 = r2;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        boolean exists;
        AnonymousClass1KS r0;
        String[] strArr = (String[]) objArr;
        AnonymousClass009.A05(strArr);
        AnonymousClass009.A0E(C12970iu.A1W(strArr.length));
        AnonymousClass1A3 r6 = this.A01;
        String str = strArr[0];
        boolean z = this.A02;
        C28181Ma r7 = new C28181Ma("StickerContextualSuggestionStore fetchMatchingStickersByTextFromDb");
        List A02 = r6.A03.A02(str, null, 200, z);
        r7.A02(C12960it.A0f(C12960it.A0k("Found and parsed emojis:"), A02.size()));
        if (A02.isEmpty()) {
            return C12980iv.A0w(0);
        }
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        r7.A02("Start search for stickers");
        int size = A02.size();
        StringBuilder A0k = C12960it.A0k("SELECT plaintext_hash, from_third_party_pack_db, sticker_pack_identifier, hash_of_image_part FROM (SELECT plaintext_hash as plaintext_hash, emojis as emojis, 10001 as primary_ordering, entry_weight as secondary_ordering, hash_of_image_part as hash_of_image_part, '' as sticker_pack_identifier, 0 as from_third_party_pack_db FROM recent_stickers UNION SELECT plaintext_hash as plaintext_hash, emojis as emojis, 10000 as primary_ordering, timestamp as secondary_ordering, hash_of_image_part as hash_of_image_part, '' as sticker_pack_identifier, 0 as from_third_party_pack_db FROM starred_stickers UNION SELECT plain_file_hash as plaintext_hash, emojis as emojis, sticker_pack_order.pack_order as primary_ordering, 0 as secondary_ordering, hash_of_image_part as hash_of_image_part, sticker_pack_order.sticker_pack_id as sticker_pack_identifier, 0 as from_third_party_pack_db FROM stickers LEFT JOIN sticker_pack_order ON sticker_pack_order.sticker_pack_id=stickers.sticker_pack_id UNION SELECT plaintext_hash as plaintext_hash, emojis as emojis, sticker_pack_order.pack_order as primary_ordering, 0 as secondary_ordering, hash_of_image_part as hash_of_image_part, (third_party_sticker_emoji_mapping.authority || ' ' || third_party_sticker_emoji_mapping.sticker_pack_id) as sticker_pack_identifier, 1 as from_third_party_pack_db FROM third_party_sticker_emoji_mapping LEFT JOIN sticker_pack_order ON sticker_pack_order.sticker_pack_id=(third_party_sticker_emoji_mapping.authority || ' ' || third_party_sticker_emoji_mapping.sticker_pack_id) LEFT JOIN third_party_whitelist_packs ON third_party_sticker_emoji_mapping.sticker_pack_id=third_party_whitelist_packs.sticker_pack_id WHERE avoid_cache = 0 ) WHERE emojis LIKE ");
        A0k.append(TextUtils.join(" OR emojis LIKE ", Collections.nCopies(size, "?")));
        A0k.append(" GROUP BY ");
        A0k.append("plaintext_hash");
        A0k.append(" ORDER BY primary_ordering DESC, secondary_ordering DESC ");
        String A0d = C12960it.A0d(" LIMIT 500", A0k);
        try {
            C16310on A01 = r6.A02.get();
            C16330op r12 = A01.A03;
            int size2 = A02.size();
            String[] strArr2 = new String[size2];
            for (int i = 0; i < size2; i++) {
                StringBuilder A0k2 = C12960it.A0k("%");
                C12970iu.A1V((C37471mS) A02.get(i), A0k2);
                strArr2[i] = C12960it.A0d("%", A0k2);
            }
            Cursor A09 = r12.A09(A0d, strArr2);
            StringBuilder A0h = C12960it.A0h();
            A0h.append("Found stickers:");
            r7.A02(C12960it.A0f(A0h, A09.getCount()));
            HashSet hashSet = new HashSet(A09.getCount());
            while (A09.moveToNext()) {
                StringBuilder A0h2 = C12960it.A0h();
                A0h2.append("Sticker #");
                r7.A02(C12960it.A0f(A0h2, A09.getPosition()));
                String A0n = C12970iu.A0n(A09, "plaintext_hash");
                boolean A1S = C12960it.A1S(C12990iw.A06(A09, "from_third_party_pack_db"));
                String A0n2 = C12970iu.A0n(A09, "sticker_pack_identifier");
                String A0n3 = C12970iu.A0n(A09, "hash_of_image_part");
                if (!hashSet.contains(A0n3)) {
                    if (!AnonymousClass1US.A0C(A0n3)) {
                        hashSet.add(A0n3);
                    }
                    AnonymousClass1KS r2 = new AnonymousClass1KS();
                    r2.A0C = A0n;
                    File A04 = r6.A01.A04(A0n);
                    if (A04.exists()) {
                        r7.A02("Sticker file stored internally");
                        r2.A08 = A04.getAbsolutePath();
                        r2.A01 = 1;
                        r2.A04 = AnonymousClass1KB.A00(WebpUtils.fetchWebpMetadata(A04.getAbsolutePath()));
                        C37431mO.A00(r2);
                        linkedHashSet.add(r2);
                    } else {
                        r7.A02("Sticker file not managed internally");
                        if (A1S && A0n2 != null && !A0n2.isEmpty() && A0n2.contains(" ") && AnonymousClass144.A00(A0n2) != null) {
                            Pair A00 = AnonymousClass144.A00(A0n2);
                            AnonymousClass009.A05(A00);
                            AnonymousClass145 r22 = r6.A04;
                            String str2 = (String) A00.first;
                            String str3 = (String) A00.second;
                            synchronized (r22) {
                                exists = r22.A00(str2, str3).exists();
                            }
                            if (exists) {
                                String str4 = (String) A00.first;
                                String str5 = (String) A00.second;
                                synchronized (r22) {
                                    List A012 = r22.A01(str4, str5, A0n);
                                    r0 = !A012.isEmpty() ? (AnonymousClass1KS) A012.get(0) : null;
                                }
                                if (r0 != null) {
                                    linkedHashSet.add(r0);
                                }
                            } else {
                                r7.A02("File not in cache, skipping");
                            }
                        }
                    }
                }
            }
            A09.close();
            A01.close();
            r7.A02(C12960it.A0f(C12960it.A0k("Finished parsing stickers:"), linkedHashSet.size()));
            r7.A01();
            return C12980iv.A0x(linkedHashSet);
        } catch (SQLiteDatabaseCorruptException e) {
            Log.e(e);
            r6.A00.AaV("StickerContexualSuggestionStore/fetchMatchingStickersByEmojisFromDb", e.getMessage(), true);
            return C12960it.A0l();
        }
    }
}
