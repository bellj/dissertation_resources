package X;

import android.view.View;
import android.widget.RelativeLayout;
import com.whatsapp.R;

/* renamed from: X.4Yj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92974Yj {
    public static void A00(View view, AnonymousClass018 r3) {
        int i;
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
        if (!C28141Kv.A00(r3)) {
            layoutParams.addRule(0, 0);
            i = 11;
        } else {
            layoutParams.addRule(1, 0);
            i = 9;
        }
        layoutParams.addRule(i);
    }

    public static void A01(View view, AnonymousClass018 r3) {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
        int i = 0;
        if (!C28141Kv.A00(r3)) {
            layoutParams.addRule(11, 0);
        } else {
            layoutParams.addRule(9, 0);
            i = 1;
        }
        layoutParams.addRule(i, R.id.send);
    }
}
