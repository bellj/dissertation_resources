package X;

import android.graphics.Bitmap;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;

/* renamed from: X.2o6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58072o6 extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ Bitmap A00;
    public final /* synthetic */ C469728k A01;

    public C58072o6(Bitmap bitmap, C469728k r2) {
        this.A01 = r2;
        this.A00 = bitmap;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        ImageView imageView = this.A01.A06;
        imageView.setImageBitmap(this.A00);
        AlphaAnimation A0J = C12970iu.A0J();
        A0J.setDuration(100);
        imageView.startAnimation(A0J);
    }
}
