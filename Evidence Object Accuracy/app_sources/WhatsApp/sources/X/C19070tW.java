package X;

import android.database.Cursor;
import com.whatsapp.util.Log;
import java.util.Collections;

/* renamed from: X.0tW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19070tW extends AbstractC18500sY implements AbstractC19010tQ {
    public final C20510vs A00;

    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AM5() {
    }

    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AND() {
    }

    public C19070tW(C20510vs r3, C18480sW r4) {
        super(r4, "message_vcard", 1);
        this.A00 = r3;
    }

    @Override // X.AbstractC18500sY
    public AnonymousClass2Ez A09(Cursor cursor) {
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow("_id");
        int columnIndexOrThrow2 = cursor.getColumnIndexOrThrow("media_wa_type");
        long j = -1;
        int i = 0;
        while (cursor.moveToNext()) {
            j = cursor.getLong(columnIndexOrThrow);
            int i2 = cursor.getInt(columnIndexOrThrow2);
            if (i2 == 4) {
                C20510vs.A00(this.A00, Collections.singletonList(cursor.getString(cursor.getColumnIndexOrThrow("data"))), j);
            } else if (i2 == 14) {
                C20510vs.A00(this.A00, AnonymousClass2GM.A00(cursor.getBlob(cursor.getColumnIndexOrThrow("raw_data"))), j);
            }
            i++;
        }
        return new AnonymousClass2Ez(j, i);
    }

    @Override // X.AbstractC19010tQ
    public void onRollback() {
        C16310on A02 = this.A05.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            C16330op r2 = A02.A03;
            r2.A01("message_vcard", null, null);
            r2.A01("message_vcard_jid", null, null);
            C21390xL r1 = this.A06;
            r1.A03("new_vcards_ready");
            r1.A03("migration_vcard_index");
            r1.A03("migration_vcard_retry");
            A00.A00();
            A00.close();
            A02.close();
            Log.i("VCardMessageStore/VCardMessageStoreDatabaseMigration/resetMigration/done");
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
