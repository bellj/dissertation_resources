package X;

import android.content.Intent;

/* renamed from: X.5uw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C127715uw {
    public final Intent A00;
    public final String A01;
    public final String A02;
    public final String A03;

    public C127715uw(Intent intent, String str, String str2, String str3) {
        this.A00 = intent;
        this.A03 = str;
        this.A02 = str2;
        this.A01 = str3;
    }
}
