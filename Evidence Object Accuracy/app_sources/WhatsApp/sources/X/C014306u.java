package X;

import android.graphics.Path;

/* renamed from: X.06u  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C014306u {
    public static Path A00(String str) {
        Path path = new Path();
        C014506w[] A02 = A02(str);
        if (A02 == null) {
            return null;
        }
        try {
            C014506w.A01(path, A02);
            return path;
        } catch (RuntimeException e) {
            StringBuilder sb = new StringBuilder("Error in parsing ");
            sb.append(str);
            throw new RuntimeException(sb.toString(), e);
        }
    }

    public static boolean A01(C014506w[] r5, C014506w[] r6) {
        int length;
        if (!(r5 == null || r6 == null || (length = r5.length) != r6.length)) {
            for (int i = 0; i < length; i++) {
                if (r5[i].A00 == r6[i].A00 && r5[i].A01.length == r6[i].A01.length) {
                }
            }
            return true;
        }
        return false;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x00e3 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C014506w[] A02(java.lang.String r15) {
        /*
        // Method dump skipped, instructions count: 276
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C014306u.A02(java.lang.String):X.06w[]");
    }
}
