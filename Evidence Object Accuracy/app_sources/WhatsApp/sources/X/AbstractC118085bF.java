package X;

import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.math.BigDecimal;
import java.util.List;

/* renamed from: X.5bF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC118085bF extends AnonymousClass015 {
    public AnonymousClass016 A00 = C12980iv.A0T();
    public List A01;
    public List A02;
    public List A03;
    public final C14830m7 A04;
    public final AnonymousClass018 A05;
    public final C18600si A06;
    public final C17070qD A07;
    public final AbstractC16870pt A08;

    public AbstractC118085bF(C14830m7 r2, AnonymousClass018 r3, C18600si r4, C17070qD r5, AbstractC16870pt r6) {
        this.A04 = r2;
        this.A05 = r3;
        this.A07 = r5;
        this.A06 = r4;
        this.A08 = r6;
        this.A03 = C12960it.A0l();
        this.A01 = C12960it.A0l();
        this.A02 = C12960it.A0l();
    }

    public static void A00(AbstractC118085bF r1) {
        r1.A00.A0A(new AnonymousClass60P());
    }

    public AnonymousClass60P A04(AnonymousClass2S0 r20, int i) {
        int A01;
        C1310260x r10;
        C50942Ry r0 = r20.A01;
        if (r0 == null) {
            Log.e("PAY: PaymentSettingsViewModel/getIncentiveBannerConfig/offerInfo is NULL");
            return null;
        }
        AbstractC38191ng A0K = C117305Zk.A0K(this.A07);
        C1310260x r11 = new C1310260x("", 0);
        if (i == 2) {
            AnonymousClass009.A05(r0);
            AnonymousClass20C r3 = r0.A07;
            int A012 = r3.A01() / r3.A00;
            C1310260x r9 = new C1310260x(new Object[0], -1);
            if (A0K != null) {
                r10 = new C1310260x(new Object[0], A0K.A03());
                int A02 = A0K.A02();
                Object[] objArr = new Object[2];
                int i2 = r0.A03;
                C12960it.A1P(objArr, i2, 0);
                objArr[1] = r3.A01.AAB(this.A05, new BigDecimal(A012 * i2), 0);
                r11 = new C1310260x(objArr, A02);
            } else {
                r10 = r11;
            }
            return new AnonymousClass60P(new AnonymousClass609(-1, R.drawable.ic_incentive_main, R.dimen.incentive_banner_icon_height, R.dimen.incentive_banner_icon_width), new C133956Ct(this, r0.A08.A01), r9, r10, r11, R.id.payment_incentive_nux_view, 0, 8, 0, 1);
        }
        if (A0K != null) {
            if (!A0B()) {
                A01 = A0K.A04();
            } else {
                A01 = A0K.A01();
            }
            r11 = new C1310260x(new Object[0], A01);
        }
        AnonymousClass009.A05(r0);
        return new AnonymousClass60P(new AnonymousClass609(-1, R.drawable.ic_incentive_main, R.dimen.incentive_banner_icon_height, R.dimen.incentive_banner_icon_width), new C133956Ct(this, r0.A08.A01), r11, new C1310260x(r0.A0F, 0), new C1310260x(r0.A0C, new Object[]{r0.A0B, "learn-more"}, R.string.incentives_learn_more_desc_text), R.id.payment_incentive_nux_view, 0, 0, 8, 1);
    }

    public void A05(int i, int i2) {
        AbstractC16870pt r5 = this.A08;
        AnonymousClass2SP A8H = r5.A8H();
        A8H.A09 = Integer.valueOf(i);
        if (i2 >= 0) {
            A8H.A08 = Integer.valueOf(i2);
        }
        A8H.A0Z = "payment_home";
        Object[] A1a = C12980iv.A1a();
        A1a[0] = "payment_home";
        A1a[1] = "notify_verification_banner";
        A8H.A0Y = String.format("%s.%s", A1a);
        AnonymousClass3FW r1 = new AnonymousClass3FW(null, new AnonymousClass3FW[0]);
        r1.A01("section", "notify_verification_banner");
        A8H.A0X = r1.toString();
        r5.AKf(A8H);
    }

    public void A06(AnonymousClass2S0 r7) {
        AnonymousClass61I.A01(AnonymousClass61I.A00(this.A04, null, r7, null, false), this.A08, 39, "payment_home", null, 1);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x005e, code lost:
        if (r0.A08.A01 == X.C12980iv.A0E(r20.A06.A01(), "payment_incentive_offer_dismissed")) goto L_0x0060;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0097, code lost:
        if (r6 == 0) goto L_0x0099;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x024f, code lost:
        if ((r3.A01 + r3.A00) < r21.A01.A03) goto L_0x0060;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A07(X.AnonymousClass2S0 r21, X.AnonymousClass60P r22) {
        /*
        // Method dump skipped, instructions count: 601
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC118085bF.A07(X.2S0, X.60P):void");
    }

    public void A08(AnonymousClass2S0 r7, Integer num, String str) {
        AnonymousClass61I.A01(AnonymousClass61I.A00(this.A04, null, r7, str, false), this.A08, num, "payment_home", null, 1);
    }

    public void A09(AnonymousClass2S0 r7, Integer num, String str) {
        AnonymousClass61I.A01(AnonymousClass61I.A00(this.A04, null, r7, str, false), this.A08, num, "payment_home", null, 1);
    }

    public void A0A(AnonymousClass2S0 r8, String str, String str2) {
        if (this instanceof C123505nG) {
            C123505nG r6 = (C123505nG) this;
            AbstractC16870pt r5 = ((AbstractC118085bF) r6).A08;
            if (r5 instanceof AnonymousClass6BE) {
                AnonymousClass6BE r52 = (AnonymousClass6BE) r5;
                boolean A0D = r6.A0D();
                AnonymousClass6BE.A00(r52.A02(0, null, "payment_home", str), AnonymousClass61I.A00(((AbstractC118085bF) r6).A04, null, r8, str2, false), r52, A0D);
            }
        } else if ("notify_verification_banner".equals(str2)) {
            A05(0, -1);
        } else {
            AnonymousClass61I.A03(AnonymousClass61I.A00(this.A04, null, r8, str2, false), this.A08, "payment_home", str);
        }
    }

    public boolean A0B() {
        if (this instanceof C123505nG) {
            return ((C123505nG) this).A06.A0A();
        }
        if (this instanceof C123495nF) {
            C123495nF r1 = (C123495nF) this;
            if (r1.A00.A06.A03() && !C117295Zj.A0Z(r1.A07).isEmpty()) {
                return true;
            }
        }
        return false;
    }
}
