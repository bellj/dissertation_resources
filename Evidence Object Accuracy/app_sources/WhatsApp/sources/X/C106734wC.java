package X;

import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.util.SparseIntArray;
import java.util.Collections;
import java.util.List;

/* renamed from: X.4wC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C106734wC implements AbstractC116785Ww {
    public int A00;
    public int A01;
    public int A02;
    public AbstractC14070ko A03;
    public C76803mG A04;
    public AbstractC116595Wb A05;
    public boolean A06;
    public boolean A07;
    public boolean A08;
    public final int A09 = 1;
    public final int A0A = 112800;
    public final SparseArray A0B;
    public final SparseBooleanArray A0C;
    public final SparseBooleanArray A0D;
    public final SparseIntArray A0E;
    public final C91904Tr A0F;
    public final AnonymousClass5Q8 A0G = new C107254x2(0);
    public final C95304dT A0H;
    public final List A0I;

    public C106734wC() {
        AnonymousClass4YB r3 = new AnonymousClass4YB(0);
        this.A0I = Collections.singletonList(r3);
        this.A0H = new C95304dT(new byte[9400], 0);
        this.A0C = new SparseBooleanArray();
        this.A0D = new SparseBooleanArray();
        this.A0B = new SparseArray();
        this.A0E = new SparseIntArray();
        this.A0F = new C91904Tr();
        this.A01 = -1;
        this.A0C.clear();
        SparseArray sparseArray = this.A0B;
        sparseArray.clear();
        SparseArray sparseArray2 = new SparseArray();
        int size = sparseArray2.size();
        for (int i = 0; i < size; i++) {
            sparseArray.put(sparseArray2.keyAt(i), sparseArray2.valueAt(i));
        }
        sparseArray.put(0, new C107264x3(new C107224wz(this)));
        this.A05 = null;
    }

    @Override // X.AbstractC116785Ww
    public void AIa(AbstractC14070ko r1) {
        this.A03 = r1;
    }

    @Override // X.AbstractC116785Ww
    public int AZn(AnonymousClass5Yf r21, AnonymousClass4IG r22) {
        AbstractC116595Wb r11;
        AbstractC14070ko r13;
        AnonymousClass5WY r12;
        long j;
        long j2;
        long j3;
        long length = r21.getLength();
        if (this.A08) {
            if (!(length == -1 || this.A09 == 2)) {
                C91904Tr r4 = this.A0F;
                if (!r4.A03) {
                    int i = this.A01;
                    if (i > 0) {
                        if (!r4.A05) {
                            int min = (int) Math.min((long) r4.A06, length);
                            j = length - ((long) min);
                            if (r21.AFo() == j) {
                                C95304dT r112 = r4.A07;
                                r112.A0Q(min);
                                r21.Aaj();
                                C95304dT.A06(r21, r112, min);
                                int i2 = r112.A01;
                                int i3 = r112.A00;
                                while (true) {
                                    i3--;
                                    if (i3 < i2) {
                                        j3 = -9223372036854775807L;
                                        break;
                                    } else if (r112.A02[i3] == 71) {
                                        j3 = AnonymousClass4DH.A00(r112, i3, i);
                                        if (j3 != -9223372036854775807L) {
                                            break;
                                        }
                                    }
                                }
                                r4.A02 = j3;
                                r4.A05 = true;
                                return 0;
                            }
                            r22.A00 = j;
                            return 1;
                        } else if (r4.A02 != -9223372036854775807L) {
                            if (!r4.A04) {
                                int min2 = (int) Math.min((long) r4.A06, length);
                                j = (long) 0;
                                if (r21.AFo() == j) {
                                    C95304dT r8 = r4.A07;
                                    r8.A0Q(min2);
                                    r21.Aaj();
                                    C95304dT.A06(r21, r8, min2);
                                    int i4 = r8.A01;
                                    int i5 = r8.A00;
                                    while (true) {
                                        if (i4 >= i5) {
                                            j2 = -9223372036854775807L;
                                            break;
                                        }
                                        if (r8.A02[i4] == 71) {
                                            j2 = AnonymousClass4DH.A00(r8, i4, i);
                                            if (j2 != -9223372036854775807L) {
                                                break;
                                            }
                                        }
                                        i4++;
                                    }
                                    r4.A01 = j2;
                                    r4.A04 = true;
                                    return 0;
                                }
                                r22.A00 = j;
                                return 1;
                            }
                            long j4 = r4.A01;
                            if (j4 != -9223372036854775807L) {
                                AnonymousClass4YB r5 = r4.A08;
                                r4.A00 = r5.A03(r4.A02) - r5.A03(j4);
                            }
                        }
                    }
                    C95304dT r2 = r4.A07;
                    byte[] bArr = AnonymousClass3JZ.A0A;
                    r2.A0U(bArr, bArr.length);
                    r4.A03 = true;
                    r21.Aaj();
                    return 0;
                }
            }
            if (!this.A06) {
                this.A06 = true;
                C91904Tr r52 = this.A0F;
                long j5 = r52.A00;
                if (j5 != -9223372036854775807L) {
                    C76803mG r122 = new C76803mG(r52.A08, this.A01, this.A0A, j5, length);
                    this.A04 = r122;
                    r13 = this.A03;
                    r12 = r122.A02;
                } else {
                    r13 = this.A03;
                    r12 = new C106904wT(j5, 0);
                }
                r13.AbR(r12);
            }
            if (this.A07) {
                this.A07 = false;
                AbQ(0, 0);
                if (r21.AFo() != 0) {
                    r22.A00 = 0;
                    return 1;
                }
            }
            C76803mG r3 = this.A04;
            if (!(r3 == null || r3.A00 == null)) {
                return r3.A00(r21, r22);
            }
        }
        C95304dT r53 = this.A0H;
        byte[] bArr2 = r53.A02;
        int i6 = r53.A01;
        if (9400 - i6 < 188) {
            int i7 = r53.A00 - i6;
            if (i7 > 0) {
                System.arraycopy(bArr2, i6, bArr2, 0, i7);
            }
            r53.A0U(bArr2, i7);
        }
        while (true) {
            int i8 = r53.A00;
            int i9 = r53.A01;
            if (i8 - i9 < 188) {
                int read = r21.read(bArr2, i8, 9400 - i8);
                if (read == -1) {
                    return -1;
                }
                r53.A0R(i8 + read);
            } else {
                byte[] bArr3 = r53.A02;
                int i10 = i9;
                while (i10 < i8 && bArr3[i10] != 71) {
                    i10++;
                }
                r53.A0S(i10);
                int i11 = i10 + 188;
                if (i11 > i8) {
                    int i12 = this.A00 + (i10 - i9);
                    this.A00 = i12;
                    if (this.A09 == 2 && i12 > 376) {
                        throw AnonymousClass496.A00("Cannot find sync byte. Most likely not a Transport Stream.");
                    }
                } else {
                    this.A00 = 0;
                }
                int i13 = r53.A00;
                if (i11 <= i13) {
                    int A07 = r53.A07();
                    if ((8388608 & A07) == 0) {
                        int i14 = (C12960it.A1S(4194304 & A07) ? 1 : 0) | 0;
                        int i15 = (2096896 & A07) >> 8;
                        boolean A1S = C12960it.A1S(A07 & 32);
                        if (!((A07 & 16) == 0 || (r11 = (AbstractC116595Wb) this.A0B.get(i15)) == null)) {
                            int i16 = this.A09;
                            if (i16 != 2) {
                                int i17 = A07 & 15;
                                SparseIntArray sparseIntArray = this.A0E;
                                int i18 = sparseIntArray.get(i15, i17 - 1);
                                sparseIntArray.put(i15, i17);
                                if (i18 != i17) {
                                    if (i17 != ((i18 + 1) & 15)) {
                                        r11.AbP();
                                    }
                                }
                            }
                            if (A1S) {
                                int A0C = r53.A0C();
                                int i19 = 0;
                                if ((r53.A0C() & 64) != 0) {
                                    i19 = 2;
                                }
                                i14 |= i19;
                                r53.A0T(A0C - 1);
                            }
                            boolean z = this.A08;
                            if (i16 == 2 || z || !this.A0D.get(i15, false)) {
                                r53.A0R(i11);
                                r11.A7b(r53, i14);
                                r53.A0R(i13);
                            }
                            if (i16 != 2 && !z && this.A08 && length != -1) {
                                this.A07 = true;
                            }
                        }
                    }
                    r53.A0S(i11);
                }
                return 0;
            }
        }
    }

    @Override // X.AbstractC116785Ww
    public void AbQ(long j, long j2) {
        C76803mG r0;
        C95314dV.A04(C12980iv.A1V(this.A09, 2));
        List list = this.A0I;
        int size = list.size();
        for (int i = 0; i < size; i++) {
            AnonymousClass4YB r6 = (AnonymousClass4YB) list.get(i);
            if (r6.A01() == -9223372036854775807L || !(r6.A01() == 0 || r6.A00() == j2)) {
                synchronized (r6) {
                    r6.A00 = j2;
                    r6.A01 = -9223372036854775807L;
                }
            }
        }
        if (!(j2 == 0 || (r0 = this.A04) == null)) {
            r0.A01(j2);
        }
        this.A0H.A0Q(0);
        this.A0E.clear();
        int i2 = 0;
        while (true) {
            SparseArray sparseArray = this.A0B;
            if (i2 < sparseArray.size()) {
                ((AbstractC116595Wb) sparseArray.valueAt(i2)).AbP();
                i2++;
            } else {
                this.A00 = 0;
                return;
            }
        }
    }

    @Override // X.AbstractC116785Ww
    public boolean Ae5(AnonymousClass5Yf r7) {
        byte[] bArr = this.A0H.A02;
        r7.AZ4(bArr, 0, 940);
        int i = 0;
        do {
            int i2 = 0;
            while (bArr[(i2 * 188) + i] == 71) {
                i2++;
                if (i2 >= 5) {
                    r7.Ae3(i);
                    return true;
                }
            }
            i++;
        } while (i < 188);
        return false;
    }
}
