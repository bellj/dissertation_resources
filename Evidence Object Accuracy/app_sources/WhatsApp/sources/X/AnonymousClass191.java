package X;

/* renamed from: X.191  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass191 extends AnonymousClass192 {
    public final C14820m6 A00;

    public AnonymousClass191(C18640sm r11, C14830m7 r12, C14820m6 r13, AnonymousClass018 r14, AnonymousClass196 r15, AnonymousClass195 r16, C231710q r17, AnonymousClass197 r18, C16120oU r19, AbstractC14440lR r20) {
        super(r11, r12, r14, r15, r16, r18, r19, r17, r20);
        this.A00 = r13;
    }

    @Override // X.AnonymousClass192
    public C93724ad A00() {
        if (this.A00.A00.getInt("emoji_search_algorithm_version", 0) == 2) {
            return super.A00();
        }
        return new C93724ad();
    }

    @Override // X.AnonymousClass192
    public boolean A01(C93724ad r5) {
        boolean A01 = super.A01(r5);
        if (A01) {
            this.A00.A00.edit().putInt("emoji_search_algorithm_version", 2).apply();
        }
        return A01;
    }
}
