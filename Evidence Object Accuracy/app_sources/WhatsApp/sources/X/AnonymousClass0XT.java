package X;

import android.os.Handler;
import android.os.SystemClock;
import android.view.MenuItem;
import java.util.List;

/* renamed from: X.0XT  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0XT implements AbstractC12310hi {
    public final /* synthetic */ AnonymousClass0CM A00;

    public AnonymousClass0XT(AnonymousClass0CM r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC12310hi
    public void ARb(MenuItem menuItem, AnonymousClass07H r9) {
        AnonymousClass0CM r1 = this.A00;
        Handler handler = r1.A0J;
        AnonymousClass0NH r0 = null;
        handler.removeCallbacksAndMessages(null);
        List list = r1.A0O;
        int size = list.size();
        for (int i = 0; i < size; i++) {
            if (r9 == ((AnonymousClass0NH) list.get(i)).A01) {
                if (i != -1) {
                    int i2 = i + 1;
                    if (i2 < list.size()) {
                        r0 = (AnonymousClass0NH) list.get(i2);
                    }
                    handler.postAtTime(new RunnableC10010ds(menuItem, this, r0, r9), r9, SystemClock.uptimeMillis() + 200);
                    return;
                } else {
                    return;
                }
            }
        }
    }

    @Override // X.AbstractC12310hi
    public void ARc(MenuItem menuItem, AnonymousClass07H r3) {
        this.A00.A0J.removeCallbacksAndMessages(r3);
    }
}
