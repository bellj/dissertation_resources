package X;

import android.content.Context;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.5n3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123375n3 extends AbstractC118045bB {
    public List A00 = C12960it.A0l();
    public List A01 = C12960it.A0l();
    public final C20920wX A02;
    public final C14830m7 A03;
    public final C16590pI A04;
    public final C14850m9 A05;
    public final AnonymousClass61M A06;
    public final C130155yt A07;
    public final C130125yq A08;
    public final AnonymousClass60Y A09;
    public final C129435xi A0A;
    public final C129235xO A0B;
    public final AnonymousClass61C A0C;
    public final AbstractC14440lR A0D;
    public final String A0E;

    public C123375n3(C20920wX r2, C14830m7 r3, C16590pI r4, C14850m9 r5, AnonymousClass61M r6, C130155yt r7, C130125yq r8, AnonymousClass60Y r9, AnonymousClass61F r10, C129435xi r11, C129235xO r12, AnonymousClass61C r13, AbstractC14440lR r14, String str) {
        super(r10);
        this.A04 = r4;
        this.A03 = r3;
        this.A05 = r5;
        this.A0D = r14;
        this.A09 = r9;
        this.A02 = r2;
        this.A07 = r7;
        this.A0B = r12;
        this.A08 = r8;
        this.A0C = r13;
        this.A0A = r11;
        this.A0E = str;
        this.A06 = r6;
    }

    public final void A07(AnonymousClass602 r14) {
        String str;
        String str2;
        String str3;
        List list = this.A00;
        list.clear();
        String str4 = this.A0E;
        boolean equals = str4.equals("withdrawal");
        boolean equals2 = str4.equals("payment_settings");
        if (equals) {
            str = "ADD_NEW_FI_CLICK";
            str2 = "WITHDRAW_MONEY";
            str3 = "WITHDRAW_METHOD";
        } else {
            str = "ADD_FI_CLICK";
            str2 = equals2 ? "NOVI_HUB" : "SEND_MONEY";
            str3 = "PAYMENT_METHODS";
        }
        AnonymousClass610 r0 = new AnonymousClass610(str, str2, str3, "LIST");
        Iterator it = r14.A01(str4).iterator();
        while (it.hasNext()) {
            String A0x = C12970iu.A0x(it);
            if (A0x.equals("BANK")) {
                Context context = this.A04.A00;
                String string = context.getString(R.string.novi_add_bank_title);
                int i = R.string.novi_add_bank_subtitle;
                if (equals) {
                    i = R.string.novi_withdraw_to_bank_subtitle;
                }
                list.add(new C122915mJ(new View.OnClickListener(r0, this, A0x) { // from class: X.64O
                    public final /* synthetic */ AnonymousClass610 A00;
                    public final /* synthetic */ C123375n3 A01;
                    public final /* synthetic */ String A02;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                        this.A02 = r3;
                    }

                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        C123375n3 r4 = this.A01;
                        AnonymousClass610 r1 = this.A00;
                        String str5 = this.A02;
                        AnonymousClass60Y r3 = r4.A09;
                        C128365vz r2 = r1.A00;
                        r2.A0T = str5;
                        r2.A0L = r4.A04.A00.getString(R.string.novi_add_bank_title);
                        r3.A05(r2);
                        C127055ts.A00(((AbstractC118045bB) r4).A01, 601);
                    }
                }, string, context.getString(i), R.drawable.ic_bank, true));
            } else if (A0x.equals("DEBIT")) {
                Context context2 = this.A04.A00;
                list.add(new C122915mJ(new View.OnClickListener(r0, this, A0x) { // from class: X.64N
                    public final /* synthetic */ AnonymousClass610 A00;
                    public final /* synthetic */ C123375n3 A01;
                    public final /* synthetic */ String A02;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                        this.A02 = r3;
                    }

                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        C123375n3 r4 = this.A01;
                        AnonymousClass610 r1 = this.A00;
                        String str5 = this.A02;
                        AnonymousClass60Y r3 = r4.A09;
                        C128365vz r2 = r1.A00;
                        r2.A0T = str5;
                        r2.A0L = r4.A04.A00.getString(R.string.novi_add_debit_card_title);
                        r3.A05(r2);
                        C127055ts.A00(((AbstractC118045bB) r4).A01, 600);
                    }
                }, context2.getString(R.string.novi_add_debit_card_title), context2.getString(R.string.novi_add_debit_card_subtitle), R.drawable.ic_add_debit_card, true));
            } else if (A0x.equals("CASH")) {
                Context context3 = this.A04.A00;
                list.add(new C122915mJ(new View.OnClickListener(r0, this, A0x) { // from class: X.64P
                    public final /* synthetic */ AnonymousClass610 A00;
                    public final /* synthetic */ C123375n3 A01;
                    public final /* synthetic */ String A02;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                        this.A02 = r3;
                    }

                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        C123375n3 r4 = this.A01;
                        AnonymousClass610 r1 = this.A00;
                        String str5 = this.A02;
                        AnonymousClass60Y r3 = r4.A09;
                        C128365vz r2 = r1.A00;
                        r2.A0T = str5;
                        r2.A0L = r4.A04.A00.getString(R.string.novi_get_cash_title);
                        r3.A05(r2);
                        C127055ts.A00(((AbstractC118045bB) r4).A01, 602);
                    }
                }, context3.getString(R.string.novi_get_cash_title), context3.getString(R.string.novi_get_cash_subtitle), R.drawable.ic_withdraw_cash_circle, true));
            } else {
                Log.e(C12960it.A0d(A0x, C12960it.A0k("PAY: NoviPayHubAddPaymentMethodViewModel/updateViewData/unknown funding option type - value: ")));
            }
        }
    }
}
