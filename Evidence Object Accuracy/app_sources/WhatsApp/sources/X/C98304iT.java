package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/* renamed from: X.4iT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98304iT implements Handler.Callback, AnonymousClass5SL {
    public final Handler A00;
    public final /* synthetic */ C76933mT A01;

    public C98304iT(AnonymousClass5XG r3, C76933mT r4) {
        this.A01 = r4;
        Looper myLooper = Looper.myLooper();
        C95314dV.A01(myLooper);
        Handler handler = new Handler(myLooper, this);
        this.A00 = handler;
        r3.AcN(handler, this);
    }

    public final void A00(long j) {
        C76933mT r3 = this.A01;
        if (this != r3.A0N) {
            return;
        }
        if (j == Long.MAX_VALUE) {
            r3.A0p = true;
            return;
        }
        try {
            r3.A0O(j);
            r3.A0Z();
            ((AbstractC76533ln) r3).A0L.A06++;
            r3.A0W();
            r3.A0N(j);
        } catch (AnonymousClass3A1 e) {
            ((AbstractC76533ln) r3).A0H = e;
        }
    }

    @Override // X.AnonymousClass5SL
    public void AQp(AnonymousClass5XG r7, long j, long j2) {
        if (AnonymousClass3JZ.A01 < 30) {
            Handler handler = this.A00;
            handler.sendMessageAtFrontOfQueue(Message.obtain(handler, 0, (int) (j >> 32), (int) j));
            return;
        }
        A00(j);
    }

    @Override // android.os.Handler.Callback
    public boolean handleMessage(Message message) {
        if (message.what != 0) {
            return false;
        }
        A00(C72453ed.A0T(message.arg1, message.arg2));
        return true;
    }
}
