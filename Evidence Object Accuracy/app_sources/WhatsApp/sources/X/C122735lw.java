package X;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5lw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122735lw extends AbstractC118835cS {
    public final View A00;
    public final ImageView A01;
    public final TextView A02;
    public final C14850m9 A03;
    public final C22460z7 A04;

    public C122735lw(View view, C14850m9 r3, C22460z7 r4) {
        super(view);
        this.A03 = r3;
        this.A04 = r4;
        this.A02 = C12960it.A0I(view, R.id.display_payment_amount);
        this.A00 = AnonymousClass028.A0D(view, R.id.payment_expressive_background_container);
        this.A01 = C12970iu.A0K(view, R.id.payment_expressive_background);
    }

    @Override // X.AbstractC118835cS
    public void A08(AbstractC125975s7 r9, int i) {
        C123245mq r92 = (C123245mq) r9;
        TextView textView = this.A02;
        textView.setText(r92.A02);
        C12980iv.A14(this.A0H.getResources(), textView, R.color.payments_currency_amount_text_color);
        textView.setAlpha(1.0f);
        textView.setContentDescription(r92.A01);
        boolean z = r92.A03;
        if (z) {
            C92984Yl.A00(textView);
        } else {
            C92984Yl.A01(textView);
        }
        C14850m9 r1 = this.A03;
        if (r1.A07(605) || r1.A07(629)) {
            C30921Zi r4 = r92.A00;
            View view = this.A00;
            if (r4 != null) {
                view.setVisibility(0);
                textView.setTextColor(r4.A0C);
                if (z) {
                    textView.setAlpha(0.54f);
                }
                ImageView imageView = this.A01;
                imageView.setBackgroundColor(r4.A0A);
                String str = r4.A01;
                if (!TextUtils.isEmpty(str)) {
                    imageView.setContentDescription(str);
                }
                float f = ((float) r4.A0D) / ((float) r4.A09);
                int i2 = imageView.getLayoutParams().width;
                int i3 = (int) (((float) i2) / f);
                imageView.getLayoutParams().height = i3;
                imageView.requestLayout();
                this.A04.A01(imageView, r4, i2, i3, true);
                return;
            }
            view.setVisibility(8);
        }
    }
}
