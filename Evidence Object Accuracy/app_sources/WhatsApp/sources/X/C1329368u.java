package X;

import android.text.TextUtils;

/* renamed from: X.68u  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1329368u implements AnonymousClass1FK, AbstractC136336Md {
    public AbstractC35651iS A00;
    public C17070qD A01;
    public C124285ou A02;
    public final ActivityC13790kL A03;
    public final C20380vf A04;
    public final C21860y6 A05;
    public final C18660so A06;
    public final C20390vg A07;
    public final C18600si A08;
    public final C243515e A09;
    public final C18610sj A0A;
    public final AnonymousClass1A7 A0B;
    public final C30931Zj A0C = C117305Zk.A0V("PaymentDataPresenter", "payment");
    public final C30941Zk A0D;
    public final AbstractC136336Md A0E;
    public final AnonymousClass6M5 A0F;
    public final AbstractC136346Me A0G;
    public final AbstractC14440lR A0H;
    public final boolean A0I;
    public final boolean A0J;

    public C1329368u(ActivityC13790kL r4, C20380vf r5, C21860y6 r6, C18660so r7, C20390vg r8, C18600si r9, C243515e r10, C18610sj r11, C17070qD r12, AnonymousClass1A7 r13, C30941Zk r14, AbstractC136336Md r15, AnonymousClass6M5 r16, AbstractC136346Me r17, AbstractC14440lR r18, boolean z) {
        this.A03 = r4;
        this.A0H = r18;
        this.A0D = r14;
        this.A01 = r12;
        this.A08 = r9;
        this.A05 = r6;
        this.A0A = r11;
        this.A0B = r13;
        this.A06 = r7;
        this.A0F = r16;
        this.A09 = r10;
        this.A04 = r5;
        this.A07 = r8;
        this.A0E = r15;
        this.A0G = r17;
        this.A0I = true;
        this.A0J = z;
    }

    public synchronized void A00(boolean z) {
        A01(z, 3);
    }

    public synchronized void A01(boolean z, int i) {
        C124285ou r1 = this.A02;
        if (r1 != null) {
            r1.A03(true);
        }
        ActivityC13790kL r2 = this.A03;
        AbstractC14440lR r7 = this.A0H;
        C124285ou r12 = new C124285ou(r2, this.A04, this.A07, this.A01, this.A0D, r7, C12970iu.A10(this.A0F), C12970iu.A10(this.A0G), i, this.A0I, this.A0J, z);
        this.A02 = r12;
        r7.Ab5(r12, new Void[0]);
    }

    public void A02(boolean z, boolean z2) {
        boolean z3 = this.A0J;
        if (z3) {
            AnonymousClass69U r1 = new AnonymousClass69U(this);
            this.A00 = r1;
            this.A09.A03(r1);
        }
        if (this.A05.A0C() || this.A06.A0C()) {
            if (z2 || this.A08.A0M()) {
                this.A0A.A08(this, 2);
            }
            if (z3 && z) {
                this.A0B.A00(this, null, null, null);
            }
        }
    }

    @Override // X.AbstractC136336Md
    public void ALz(boolean z) {
        AbstractC136336Md r0 = this.A0E;
        if (r0 != null) {
            r0.ALz(z);
        }
    }

    @Override // X.AbstractC136336Md
    public void ATY(AbstractC28901Pl r2) {
        AbstractC136336Md r0 = this.A0E;
        if (r0 != null) {
            r0.ATY(r2);
        }
    }

    @Override // X.AnonymousClass1FK
    public void AV3(C452120p r3) {
        this.A0C.A05(C12960it.A0b("getPaymentMethods/getPaymentTransactions/onRequestError. paymentNetworkError: ", r3));
    }

    @Override // X.AnonymousClass1FK
    public void AVA(C452120p r3) {
        this.A0C.A05(C12960it.A0b("getPaymentMethods/getPaymentTransactions/onResponseError. paymentNetworkError: ", r3));
    }

    @Override // X.AnonymousClass1FK
    public void AVB(C452220q r5) {
        if (r5 instanceof AnonymousClass46O) {
            this.A0C.A04("init/getMethods/onResponseSuccess");
        } else if (r5 instanceof AnonymousClass46P) {
            C30931Zj r2 = this.A0C;
            r2.A04("init/getTransactions/onResponseSuccess");
            AnonymousClass46P r52 = (AnonymousClass46P) r5;
            AnonymousClass20A r1 = r52.A00;
            if (r1 == null) {
                r2.A04("unexpected payment transaction result type.");
            } else if (!r1.A02 && !TextUtils.isEmpty(r1.A00)) {
                this.A0B.A00(this, null, null, r52.A00.A00);
            }
        } else {
            return;
        }
        A00(false);
    }
}
