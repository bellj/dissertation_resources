package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.payments.ui.widget.PaymentMethodRow;
import com.whatsapp.util.Log;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.61i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1311161i {
    public static int A00(int i) {
        if (i == 1) {
            return R.drawable.av_visa;
        }
        if (i == 2) {
            return R.drawable.av_mc;
        }
        if (i != 3) {
            return i != 4 ? R.drawable.av_card : R.drawable.av_discover;
        }
        return R.drawable.av_amex;
    }

    public static int A01(List list) {
        for (int i = 0; i < list.size(); i++) {
            if (C117315Zl.A08(list, i).A01 == 2) {
                return i;
            }
        }
        return 0;
    }

    public static String A02(Context context, AnonymousClass018 r6, AbstractC28901Pl r7, C17070qD r8, boolean z) {
        if (r7 instanceof C30881Ze) {
            return A05(context, (C30881Ze) r7);
        }
        if (r7 instanceof C30841Za) {
            C30841Za r72 = (C30841Za) r7;
            String string = context.getString(R.string.unknown);
            String str = (String) C117295Zj.A0R(r72.A09);
            String str2 = r72.A0B;
            if (!TextUtils.isEmpty(str)) {
                string = str;
            } else if (!TextUtils.isEmpty(str2)) {
                string = str2;
            }
            C30821Yy r4 = r72.A01;
            AnonymousClass1ZZ r3 = (AnonymousClass1ZZ) r72.A08;
            if (r4 == null || r3 == null) {
                return string;
            }
            StringBuilder A0j = C12960it.A0j(string);
            A0j.append(" ");
            A0j.append("•");
            A0j.append(" ");
            return C12960it.A0d(((C119805f8) r3).A01.AAA(r6, r4, 0), A0j);
        } else if (r7 instanceof AnonymousClass1ZW) {
            String string2 = context.getString(R.string.unknown);
            String str3 = (String) C117295Zj.A0R(r7.A09);
            String str4 = r7.A0B;
            if (!TextUtils.isEmpty(str3)) {
                return str3;
            }
            if (!TextUtils.isEmpty(str4)) {
                return str4;
            }
            return string2;
        } else if (r7 instanceof C119725f0) {
            return (String) C117295Zj.A0R(r7.A09);
        } else {
            String string3 = context.getString(R.string.unknown);
            String str5 = r7.A0B;
            if (str5 != null) {
                string3 = str5;
                AnonymousClass1ZR r1 = r7.A09;
                if (!AnonymousClass1ZS.A03(r1)) {
                    String A08 = A08((String) AnonymousClass1ZS.A01(r1));
                    StringBuilder A0j2 = C12960it.A0j(str5);
                    A0j2.append(" ••");
                    string3 = C12960it.A0d(A08, A0j2);
                }
            }
            if (!z) {
                return string3;
            }
            Object[] A1a = C12980iv.A1a();
            A1a[0] = string3;
            return C12960it.A0X(context, context.getString(r8.A02().AFC()), A1a, 1, R.string.transaction_payment_method_info);
        }
    }

    public static String A03(Context context, C30861Zc r3) {
        String string = context.getString(R.string.unknown);
        AnonymousClass1ZR r1 = r3.A09;
        if (AnonymousClass1ZS.A03(r1)) {
            return string;
        }
        Object obj = r1.A00;
        AnonymousClass009.A05(obj);
        String A08 = A08((String) obj);
        StringBuilder A0h = C12960it.A0h();
        A0h.append(r3.A0B);
        C117325Zm.A09(A0h);
        return C12960it.A0d(A08, A0h);
    }

    public static String A04(Context context, C30881Ze r5) {
        String str;
        int i;
        AbstractC30871Zd r3 = (AbstractC30871Zd) r5.A08;
        if (r3 == null || (str = r3.A0I) == null) {
            Log.w(C12960it.A0d(r5.A0A, C12960it.A0k("PaymentMethod/getCardDisplayStateText: Card has no display state: ")));
            return "";
        }
        switch (str.hashCode()) {
            case -1757659853:
                if (str.equals("VOIDED")) {
                    i = R.string.payment_method_voided;
                    return context.getString(i);
                }
                return "";
            case -591252731:
                if (str.equals("EXPIRED")) {
                    i = R.string.payment_method_expired;
                    return context.getString(i);
                }
                return "";
            case 1124965819:
                if (str.equals("SUSPENDED")) {
                    i = R.string.payment_method_suspended;
                    return context.getString(i);
                }
                return "";
            case 1925346054:
                if (str.equals("ACTIVE") && r3.A0Z && !r3.A0A()) {
                    i = R.string.payment_method_unverified;
                    return context.getString(i);
                }
                return "";
            default:
                return "";
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:28:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String A05(android.content.Context r7, X.C30881Ze r8) {
        /*
            r0 = 2131892447(0x7f1218df, float:1.9419643E38)
            java.lang.String r3 = r7.getString(r0)
            int r0 = r8.A01
            java.lang.String r6 = X.C30881Ze.A08(r0)
            java.lang.String r5 = ""
            if (r6 != 0) goto L_0x0012
            r6 = r5
        L_0x0012:
            int r2 = r8.A00
            r4 = 1
            r1 = 2131890228(0x7f121034, float:1.9415142E38)
            if (r2 == r4) goto L_0x0026
            r0 = 4
            r1 = 2131890227(0x7f121033, float:1.941514E38)
            if (r2 == r0) goto L_0x0026
            r0 = 6
            if (r2 != r0) goto L_0x0069
            r1 = 2131890226(0x7f121032, float:1.9415138E38)
        L_0x0026:
            java.lang.String r0 = r7.getString(r1)
        L_0x002a:
            if (r0 == 0) goto L_0x0043
            int r2 = r8.A00
            r1 = 2131890228(0x7f121034, float:1.9415142E38)
            if (r2 == r4) goto L_0x003f
            r0 = 4
            r1 = 2131890227(0x7f121033, float:1.941514E38)
            if (r2 == r0) goto L_0x003f
            r0 = 6
            if (r2 != r0) goto L_0x0064
            r1 = 2131890226(0x7f121032, float:1.9415138E38)
        L_0x003f:
            java.lang.String r5 = r7.getString(r1)
        L_0x0043:
            X.1ZR r1 = r8.A09
            boolean r0 = X.AnonymousClass1ZS.A03(r1)
            if (r0 != 0) goto L_0x0063
            java.lang.String r0 = X.C117315Zl.A0K(r1)
            java.lang.String r3 = A08(r0)
            r2 = 2131890305(0x7f121081, float:1.9415298E38)
            r0 = 3
            java.lang.Object[] r1 = new java.lang.Object[r0]
            r0 = 0
            r1[r0] = r6
            r1[r4] = r5
            r0 = 2
            java.lang.String r3 = X.C12960it.A0X(r7, r3, r1, r0, r2)
        L_0x0063:
            return r3
        L_0x0064:
            java.lang.String r5 = X.AbstractC28901Pl.A02(r2)
            goto L_0x0043
        L_0x0069:
            java.lang.String r0 = X.AbstractC28901Pl.A02(r2)
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C1311161i.A05(android.content.Context, X.1Ze):java.lang.String");
    }

    public static String A06(Context context, AbstractC28901Pl r4) {
        if (TextUtils.isEmpty(null) && r4.A01 == 2) {
            return context.getString(R.string.payments_default_payment_method);
        }
        return null;
    }

    public static String A07(AbstractC28901Pl r0) {
        return A08((String) AnonymousClass1ZS.A01(r0.A09));
    }

    public static String A08(String str) {
        int length = str.length();
        if (length > 4) {
            str = str.substring(length - 4);
        }
        int i = 0;
        int length2 = str.length();
        while (true) {
            length2--;
            if (length2 < 0 || !Character.isDigit(str.charAt(length2))) {
                break;
            }
            i++;
        }
        if (i != length2) {
            return str.substring(length2 - i);
        }
        return str;
    }

    public static String A09(List list) {
        Iterator it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            AbstractC28901Pl A0H = C117305Zk.A0H(it);
            if (A0H.A01 == 2) {
                AnonymousClass1ZY r0 = A0H.A08;
                if (r0 != null) {
                    return (String) C117295Zj.A0R(r0.A06());
                }
                C117305Zk.A1N("PaymentMethodUtils", "getDefaultAccountHolderName/null country data");
            }
        }
        return null;
    }

    public static void A0A(AbstractC28901Pl r1, PaymentMethodRow paymentMethodRow) {
        int i;
        if (r1 instanceof C30881Ze) {
            i = A00(((C30881Ze) r1).A01);
        } else {
            Bitmap A05 = r1.A05();
            if (A05 != null) {
                paymentMethodRow.A01.setImageBitmap(A05);
                return;
            }
            i = R.drawable.av_bank;
        }
        paymentMethodRow.A01.setImageResource(i);
    }

    public static boolean A0B(AbstractC28901Pl r2) {
        int A04 = r2.A04();
        if (A04 == 4 || A04 == 1 || A04 == 6 || A04 == 8 || A04 == 7) {
            return true;
        }
        return false;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: SSATransform
        jadx.core.utils.exceptions.JadxRuntimeException: Not initialized variable reg: 7, insn: 0x00a8: MOVE  (r3 I:??[OBJECT, ARRAY]) = (r7 I:??[OBJECT, ARRAY]), block:B:25:0x00a8
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVarsInBlock(SSATransform.java:171)
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:143)
        	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:60)
        	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
        */
    public static byte[] A0C(
/*
[212] Method generation error in method: X.61i.A0C(X.0t3, java.lang.String):byte[], file: classes4.dex
    jadx.core.utils.exceptions.JadxRuntimeException: Code variable not set in r12v0 ??
    	at jadx.core.dex.instructions.args.SSAVar.getCodeVar(SSAVar.java:228)
    	at jadx.core.codegen.MethodGen.addMethodArguments(MethodGen.java:195)
    	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:151)
    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:344)
    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
    	at java.util.ArrayList.forEach(ArrayList.java:1259)
    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
    
*/
}
