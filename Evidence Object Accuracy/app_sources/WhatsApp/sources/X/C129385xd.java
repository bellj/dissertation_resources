package X;

/* renamed from: X.5xd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129385xd {
    public C128075vW A00;
    public C30931Zj A01 = C30931Zj.A00("BrazilNetworkManager", "onboarding", "BR");
    public boolean A02;
    public boolean A03;
    public final C14900mE A04;
    public final C15570nT A05;
    public final C18640sm A06;
    public final C14830m7 A07;
    public final C16590pI A08;
    public final C241414j A09;
    public final C17220qS A0A;
    public final C120045fX A0B;
    public final AnonymousClass60Z A0C;
    public final C18650sn A0D;
    public final C18600si A0E;
    public final C18610sj A0F;
    public final AnonymousClass60T A0G;
    public final C129095xA A0H;

    public C129385xd(C14900mE r4, C15570nT r5, C18640sm r6, C14830m7 r7, C16590pI r8, C241414j r9, C17220qS r10, C120045fX r11, AnonymousClass60Z r12, C18650sn r13, C18600si r14, C18610sj r15, AnonymousClass60T r16, C129095xA r17) {
        this.A07 = r7;
        this.A08 = r8;
        this.A04 = r4;
        this.A05 = r5;
        this.A09 = r9;
        this.A0A = r10;
        this.A0E = r14;
        this.A0F = r15;
        this.A0H = r17;
        this.A0C = r12;
        this.A0D = r13;
        this.A06 = r6;
        this.A0G = r16;
        this.A0B = r11;
    }

    public void A00(String str) {
        C128075vW r3 = this.A00;
        if (r3 == null) {
            String A06 = this.A0E.A06();
            if (A06 == null) {
                this.A01.A06("No wallet Id stored on client, ELO node cannot be inserted in request");
            }
            r3 = new C128075vW(null, str, A06, 5);
            this.A00 = r3;
        }
        r3.A02 = str;
    }
}
