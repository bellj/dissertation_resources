package X;

/* renamed from: X.2T7  reason: invalid class name */
/* loaded from: classes2.dex */
public enum AnonymousClass2T7 {
    A01(0),
    /* Fake field, exist only in values array */
    EF17(1),
    /* Fake field, exist only in values array */
    EF27(2),
    /* Fake field, exist only in values array */
    EF37(3),
    /* Fake field, exist only in values array */
    EF47(4),
    /* Fake field, exist only in values array */
    EF57(5),
    /* Fake field, exist only in values array */
    EF67(6),
    /* Fake field, exist only in values array */
    EF77(7),
    /* Fake field, exist only in values array */
    EF88(8),
    /* Fake field, exist only in values array */
    EF99(9),
    /* Fake field, exist only in values array */
    EF110(10),
    /* Fake field, exist only in values array */
    EF121(11),
    /* Fake field, exist only in values array */
    EF132(12),
    /* Fake field, exist only in values array */
    EF143(13),
    /* Fake field, exist only in values array */
    EF154(14),
    /* Fake field, exist only in values array */
    EF165(15),
    /* Fake field, exist only in values array */
    EF174(16),
    /* Fake field, exist only in values array */
    EF183(17),
    /* Fake field, exist only in values array */
    EF192(18),
    /* Fake field, exist only in values array */
    EF201(19),
    /* Fake field, exist only in values array */
    EF210(20),
    /* Fake field, exist only in values array */
    EF219(21),
    /* Fake field, exist only in values array */
    EF228(22),
    /* Fake field, exist only in values array */
    EF237(23),
    /* Fake field, exist only in values array */
    EF246(24),
    /* Fake field, exist only in values array */
    EF255(25),
    /* Fake field, exist only in values array */
    EF264(26),
    /* Fake field, exist only in values array */
    EF273(27),
    /* Fake field, exist only in values array */
    EF282(28);
    
    public final int value;

    AnonymousClass2T7(int i) {
        this.value = i;
    }
}
