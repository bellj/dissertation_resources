package X;

import android.view.animation.Animation;

/* renamed from: X.3wu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83233wu extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ C42511vK A00;

    public C83233wu(C42511vK r1) {
        this.A00 = r1;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationRepeat(Animation animation) {
        animation.setStartOffset(300);
    }
}
