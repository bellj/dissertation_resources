package X;

import android.app.PendingIntent;
import android.content.Intent;

/* renamed from: X.11U  reason: invalid class name */
/* loaded from: classes2.dex */
public interface AnonymousClass11U {
    PendingIntent A8F(C15370n3 v, AbstractC15340mz v2);

    void AHy(Intent intent);

    boolean AdL(AbstractC15340mz v);
}
