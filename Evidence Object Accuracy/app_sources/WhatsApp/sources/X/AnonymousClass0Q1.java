package X;

/* renamed from: X.0Q1  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass0Q1 {
    public abstract void A00(C06340Tf v, C06340Tf v2);

    public abstract void A01(C06340Tf v, Thread thread);

    public abstract boolean A02(C05940Ro v, C05940Ro v2, AnonymousClass041 v3);

    public abstract boolean A03(C06340Tf v, C06340Tf v2, AnonymousClass041 v3);

    public abstract boolean A04(AnonymousClass041 v, Object obj, Object obj2);
}
