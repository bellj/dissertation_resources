package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/* renamed from: X.18o  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C252218o extends AbstractC15850o0 {
    public final AnonymousClass016 A00 = new AnonymousClass016(0);
    public final C14900mE A01;
    public final C14830m7 A02;
    public final C252118n A03;
    public final C22190yg A04;
    public final AbstractC14440lR A05;

    public C252218o(C14900mE r14, C15570nT r15, C15820nx r16, C15810nw r17, C17050qB r18, C14830m7 r19, C16590pI r20, C19490uC r21, C21780xy r22, C252118n r23, AbstractC15870o2 r24, C16600pJ r25, AnonymousClass15D r26, C22190yg r27, AbstractC14440lR r28) {
        super(r15, r16, r17, r18, r20, r21, r22, r24, r25, r26);
        this.A02 = r19;
        this.A01 = r14;
        this.A04 = r27;
        this.A05 = r28;
        this.A03 = r23;
    }

    public final Drawable A0A(Context context, C33191db r7) {
        int i = 0;
        try {
            String str = r7.A02;
            if (str != null) {
                i = Integer.parseInt(str);
            }
        } catch (NumberFormatException unused) {
        }
        int[] intArray = context.getResources().getIntArray(R.array.solid_color_wallpaperv2_colors);
        Bitmap createBitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.RGB_565);
        createBitmap.setPixel(0, 0, intArray[i]);
        return new BitmapDrawable(context.getResources(), createBitmap);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00c0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.AnonymousClass2JR A0B(android.content.Context r8, X.C33191db r9, boolean r10) {
        /*
            r7 = this;
            java.lang.String r3 = r9.A01
            X.AnonymousClass009.A05(r3)
            android.content.res.Resources r4 = r8.getResources()
            int r0 = r3.hashCode()
            r1 = 2
            r2 = 1
            r5 = 0
            switch(r0) {
                case -2032180703: goto L_0x001c;
                case -1770733785: goto L_0x0029;
                case -899329064: goto L_0x002d;
                case 175331287: goto L_0x00b8;
                case 1804184360: goto L_0x00d8;
                default: goto L_0x0013;
            }
        L_0x0013:
            r2 = 0
        L_0x0014:
            java.lang.Integer r1 = r9.A00
            X.2JR r0 = new X.2JR
            r0.<init>(r2, r1, r3, r10)
            return r0
        L_0x001c:
            java.lang.String r0 = "DEFAULT"
            boolean r0 = r3.equals(r0)
            if (r0 == 0) goto L_0x0013
            android.graphics.drawable.Drawable r2 = X.AnonymousClass2JP.A00(r8, r4)
            goto L_0x0014
        L_0x0029:
            java.lang.String r0 = "DOWNLOADED"
            goto L_0x00ba
        L_0x002d:
            java.lang.String r0 = "COLOR_WITH_WA_OVERLAY"
            boolean r0 = r3.equals(r0)
            if (r0 == 0) goto L_0x0013
            r4 = 0
            java.lang.String r0 = r9.A02     // Catch: NumberFormatException -> 0x003e
            if (r0 == 0) goto L_0x003e
            int r4 = java.lang.Integer.parseInt(r0)     // Catch: NumberFormatException -> 0x003e
        L_0x003e:
            android.graphics.drawable.Drawable r0 = r7.A0A(r8, r9)
            android.graphics.drawable.Drawable[] r6 = new android.graphics.drawable.Drawable[r1]
            r6[r5] = r0
            android.content.res.Resources r1 = r8.getResources()
            r0 = 2130903070(0x7f03001e, float:1.7412948E38)
            int[] r0 = r1.getIntArray(r0)
            r1 = r0[r4]
            r0 = 2131232854(0x7f080856, float:1.808183E38)
            android.graphics.drawable.Drawable r0 = X.AnonymousClass00T.A04(r8, r0)
            X.AnonymousClass009.A05(r0)
            android.graphics.drawable.Drawable r0 = X.AnonymousClass2GE.A04(r0, r1)
            r6[r2] = r0
            android.graphics.drawable.LayerDrawable r4 = new android.graphics.drawable.LayerDrawable
            r4.<init>(r6)
            boolean r0 = r4 instanceof android.graphics.drawable.BitmapDrawable
            if (r0 == 0) goto L_0x0083
            r1 = r4
            android.graphics.drawable.BitmapDrawable r1 = (android.graphics.drawable.BitmapDrawable) r1
            android.graphics.Bitmap r0 = r1.getBitmap()
            if (r0 == 0) goto L_0x0083
            android.graphics.Bitmap r6 = r1.getBitmap()
        L_0x0079:
            android.content.res.Resources r0 = r8.getResources()
            android.graphics.drawable.BitmapDrawable r2 = new android.graphics.drawable.BitmapDrawable
            r2.<init>(r0, r6)
            goto L_0x0014
        L_0x0083:
            int r0 = r4.getIntrinsicWidth()
            if (r0 <= 0) goto L_0x00b1
            int r0 = r4.getIntrinsicHeight()
            if (r0 <= 0) goto L_0x00b1
            int r2 = r4.getIntrinsicWidth()
            int r1 = r4.getIntrinsicHeight()
            android.graphics.Bitmap$Config r0 = android.graphics.Bitmap.Config.ARGB_8888
            android.graphics.Bitmap r6 = android.graphics.Bitmap.createBitmap(r2, r1, r0)
        L_0x009d:
            android.graphics.Canvas r2 = new android.graphics.Canvas
            r2.<init>(r6)
            int r1 = r2.getWidth()
            int r0 = r2.getHeight()
            r4.setBounds(r5, r5, r1, r0)
            r4.draw(r2)
            goto L_0x0079
        L_0x00b1:
            android.graphics.Bitmap$Config r0 = android.graphics.Bitmap.Config.ARGB_8888
            android.graphics.Bitmap r6 = android.graphics.Bitmap.createBitmap(r2, r2, r0)
            goto L_0x009d
        L_0x00b8:
            java.lang.String r0 = "USER_PROVIDED"
        L_0x00ba:
            boolean r0 = r3.equals(r0)
            if (r0 == 0) goto L_0x0013
            java.lang.String r0 = r9.A02
            android.net.Uri r0 = android.net.Uri.parse(r0)
            java.lang.String r1 = r0.getPath()
            X.AnonymousClass009.A05(r1)
            java.io.File r0 = new java.io.File
            r0.<init>(r1)
            android.graphics.drawable.Drawable r2 = X.AnonymousClass2JP.A01(r8, r4, r0)
            goto L_0x0014
        L_0x00d8:
            java.lang.String r0 = "COLOR_ONLY"
            boolean r0 = r3.equals(r0)
            if (r0 == 0) goto L_0x0013
            android.graphics.drawable.Drawable r2 = r7.A0A(r8, r9)
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C252218o.A0B(android.content.Context, X.1db, boolean):X.2JR");
    }

    public final C33191db A0C(Context context, BitmapDrawable bitmapDrawable, AbstractC14640lm r8) {
        String A01 = C003501n.A01(String.valueOf(System.currentTimeMillis()));
        if (A01 == null) {
            A01 = String.valueOf(System.currentTimeMillis());
        }
        File file = new File(context.getFilesDir(), "Wallpapers");
        file.mkdirs();
        File file2 = new File(file, A01);
        if (!file2.exists()) {
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(file2);
                bitmapDrawable.getBitmap().compress(Bitmap.CompressFormat.JPEG, 90, fileOutputStream);
                fileOutputStream.close();
            } catch (IOException e) {
                Log.e("wallpaper/v2/save-wallpaper-file/failed to save wallpaper", e);
            }
        }
        C33191db r0 = new C33191db(25, "USER_PROVIDED", Uri.fromFile(file2).toString());
        A0F(context, r8, r0);
        return r0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0070  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.C33191db A0D(android.content.Context r10, boolean r11) {
        /*
            r9 = this;
            X.0o2 r0 = r9.A08
            r5 = 0
            X.1db r2 = r0.AHh(r5, r11)
            if (r2 != 0) goto L_0x0027
            X.18n r1 = r9.A03
            r0 = 1
            X.2JQ r1 = r1.A0A(r10, r0)
            java.lang.String r6 = r1.A02
            int r0 = r6.hashCode()
            r8 = 0
            switch(r0) {
                case -1770733785: goto L_0x0068;
                case -899329064: goto L_0x002e;
                case 175331287: goto L_0x002b;
                case 1804184360: goto L_0x0028;
                default: goto L_0x001a;
            }
        L_0x001a:
            r1 = 0
        L_0x001b:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r8)
            X.1db r2 = new X.1db
            r2.<init>(r0, r6, r1)
            r9.A0F(r10, r5, r2)
        L_0x0027:
            return r2
        L_0x0028:
            java.lang.String r0 = "COLOR_ONLY"
            goto L_0x0030
        L_0x002b:
            java.lang.String r0 = "USER_PROVIDED"
            goto L_0x006a
        L_0x002e:
            java.lang.String r0 = "COLOR_WITH_WA_OVERLAY"
        L_0x0030:
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x001a
            java.lang.Integer r0 = r1.A00
            int r2 = r0.intValue()
            android.content.res.Resources r1 = r10.getResources()
            r0 = 2130903065(0x7f030019, float:1.7412937E38)
            int[] r0 = r1.getIntArray(r0)
            r7 = r0[r2]
            android.content.res.Resources r1 = r10.getResources()
            r0 = 2130903066(0x7f03001a, float:1.741294E38)
            int[] r4 = r1.getIntArray(r0)
            int r3 = r4.length
            r2 = 0
            r1 = 0
        L_0x0057:
            if (r2 >= r3) goto L_0x0062
            r0 = r4[r2]
            if (r0 == r7) goto L_0x0063
            int r1 = r1 + 1
            int r2 = r2 + 1
            goto L_0x0057
        L_0x0062:
            r1 = 0
        L_0x0063:
            java.lang.String r1 = java.lang.String.valueOf(r1)
            goto L_0x001b
        L_0x0068:
            java.lang.String r0 = "DOWNLOADED"
        L_0x006a:
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x001a
            java.io.File r2 = r10.getFilesDir()
            java.lang.String r0 = "wallpaper.jpg"
            java.io.File r1 = new java.io.File
            r1.<init>(r2, r0)
            boolean r0 = r1.exists()
            if (r0 == 0) goto L_0x001a
            android.net.Uri r0 = android.net.Uri.fromFile(r1)
            java.lang.String r1 = r0.toString()
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C252218o.A0D(android.content.Context, boolean):X.1db");
    }

    public final void A0E() {
        File[] listFiles;
        C15860o1 r3 = (C15860o1) this.A08;
        C16310on A02 = r3.A03().A02();
        try {
            C16330op r1 = A02.A03;
            r1.A0B("UPDATE settings SET wallpaper_light_type = NULL, wallpaper_light_value = NULL, wallpaper_dark_type = NULL, wallpaper_dark_value = NULL, wallpaper_dark_opacity = NULL WHERE jid != 'individual_chat_defaults'");
            r1.A0B("UPDATE settings SET wallpaper_light_type = 'DEFAULT', wallpaper_light_value = NULL, wallpaper_dark_type = 'DEFAULT', wallpaper_dark_value = NULL, wallpaper_dark_opacity = NULL WHERE jid = 'individual_chat_defaults'");
            A02.close();
            r3.A0X.clear();
            File file = new File(super.A05.A00.getFilesDir(), "Wallpapers");
            if (file.exists() && (listFiles = file.listFiles()) != null) {
                for (File file2 : listFiles) {
                    file2.delete();
                }
            }
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0028, code lost:
        if (r1.equalsIgnoreCase(r10.A02) == false) goto L_0x002a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002b, code lost:
        if (r6 != null) goto L_0x002d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0035, code lost:
        if ("USER_PROVIDED".equalsIgnoreCase(r6.A01) == false) goto L_0x004e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0037, code lost:
        if (r4 != false) goto L_0x004e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0039, code lost:
        r1 = android.net.Uri.parse(r6.A02).getPath();
        X.AnonymousClass009.A05(r1);
        new java.io.File(r1).delete();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0015, code lost:
        if ("USER_PROVIDED".equalsIgnoreCase(r10.A01) == false) goto L_0x0017;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0F(android.content.Context r8, X.AbstractC14640lm r9, X.C33191db r10) {
        /*
            r7 = this;
            boolean r3 = X.C41691tw.A08(r8)
            X.0o2 r2 = r7.A08
            X.1db r6 = r2.AHh(r9, r3)
            if (r10 == 0) goto L_0x0017
            java.lang.String r1 = r10.A01
            java.lang.String r0 = "USER_PROVIDED"
            boolean r1 = r0.equalsIgnoreCase(r1)
            r0 = 1
            if (r1 != 0) goto L_0x0018
        L_0x0017:
            r0 = 0
        L_0x0018:
            r5 = 1
            if (r0 == 0) goto L_0x002a
            if (r6 == 0) goto L_0x004e
            java.lang.String r1 = r6.A02
            if (r1 == 0) goto L_0x002a
            java.lang.String r0 = r10.A02
            boolean r0 = r1.equalsIgnoreCase(r0)
            r4 = 1
            if (r0 != 0) goto L_0x002d
        L_0x002a:
            r4 = 0
            if (r6 == 0) goto L_0x004e
        L_0x002d:
            java.lang.String r1 = r6.A01
            java.lang.String r0 = "USER_PROVIDED"
            boolean r0 = r0.equalsIgnoreCase(r1)
            if (r0 == 0) goto L_0x004e
            if (r4 != 0) goto L_0x004e
            java.lang.String r0 = r6.A02
            android.net.Uri r0 = android.net.Uri.parse(r0)
            java.lang.String r1 = r0.getPath()
            X.AnonymousClass009.A05(r1)
            java.io.File r0 = new java.io.File
            r0.<init>(r1)
            r0.delete()
        L_0x004e:
            r7.A00 = r5
            X.0o1 r2 = (X.C15860o1) r2
            if (r9 != 0) goto L_0x0078
            X.1da r0 = r2.A05()
        L_0x0058:
            if (r3 == 0) goto L_0x0075
            r0.A04 = r10
        L_0x005c:
            r2.A0M(r0)
            if (r9 == 0) goto L_0x0071
            r3 = r3 ^ r5
            java.lang.String r0 = r9.getRawString()
            X.1da r0 = r2.A08(r0)
            if (r3 == 0) goto L_0x0072
            r0.A04 = r10
        L_0x006e:
            r2.A0M(r0)
        L_0x0071:
            return
        L_0x0072:
            r0.A05 = r10
            goto L_0x006e
        L_0x0075:
            r0.A05 = r10
            goto L_0x005c
        L_0x0078:
            java.lang.String r0 = r9.getRawString()
            X.1da r0 = r2.A08(r0)
            goto L_0x0058
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C252218o.A0F(android.content.Context, X.0lm, X.1db):void");
    }
}
