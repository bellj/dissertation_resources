package X;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

/* renamed from: X.5DI  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5DI implements Iterator {
    public int currentIndex;
    public int expectedMetadata;
    public int indexToRemove = -1;
    public final /* synthetic */ AnonymousClass5I9 this$0;

    public AnonymousClass5DI(AnonymousClass5I9 r2) {
        this.this$0 = r2;
        this.expectedMetadata = r2.metadata;
        this.currentIndex = r2.firstEntryIndex();
    }

    private void checkForConcurrentModification() {
        if (this.this$0.metadata != this.expectedMetadata) {
            throw new ConcurrentModificationException();
        }
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        return C12990iw.A1W(this.currentIndex);
    }

    public void incrementExpectedModCount() {
        this.expectedMetadata += 32;
    }

    @Override // java.util.Iterator
    public Object next() {
        checkForConcurrentModification();
        if (hasNext()) {
            int i = this.currentIndex;
            this.indexToRemove = i;
            AnonymousClass5I9 r0 = this.this$0;
            Object obj = r0.element(i);
            this.currentIndex = r0.getSuccessor(i);
            return obj;
        }
        throw new NoSuchElementException();
    }

    @Override // java.util.Iterator
    public void remove() {
        checkForConcurrentModification();
        C28251Mi.checkRemove(C12990iw.A1W(this.indexToRemove));
        incrementExpectedModCount();
        AnonymousClass5I9 r1 = this.this$0;
        r1.remove(r1.element(this.indexToRemove));
        this.currentIndex = this.this$0.adjustAfterRemove(this.currentIndex, this.indexToRemove);
        this.indexToRemove = -1;
    }
}
