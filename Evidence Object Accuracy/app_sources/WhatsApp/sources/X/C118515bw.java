package X;

import android.view.ViewGroup;
import com.facebook.redex.IDxCListenerShape2S0200000_3_I1;
import com.whatsapp.payments.ui.NoviClaimableTransactionListActivity;
import com.whatsapp.payments.ui.NoviPayHubTransactionHistoryActivity;

/* renamed from: X.5bw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118515bw extends AnonymousClass02M {
    public final /* synthetic */ NoviPayHubTransactionHistoryActivity A00;

    public C118515bw(NoviPayHubTransactionHistoryActivity noviPayHubTransactionHistoryActivity) {
        this.A00 = noviPayHubTransactionHistoryActivity;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A00.A0B.size();
    }

    @Override // X.AnonymousClass02M
    public void ANH(AnonymousClass03U r16, int i) {
        NoviPayHubTransactionHistoryActivity noviPayHubTransactionHistoryActivity = this.A00;
        if (noviPayHubTransactionHistoryActivity.A0B.isEmpty()) {
            noviPayHubTransactionHistoryActivity.A0E.A05("Transaction items size zero even when binding");
            return;
        }
        Object obj = noviPayHubTransactionHistoryActivity.A0B.get(i);
        AnonymousClass009.A05(obj);
        C117695aS r2 = ((C118725cH) r16).A00;
        r2.A01.setImageDrawable(null);
        r2.A04.setText("");
        r2.A05.setText("");
        r2.A02.setText("");
        r2.A03.setText("");
        C14830m7 r8 = ((ActivityC13790kL) noviPayHubTransactionHistoryActivity).A05;
        AnonymousClass14X r13 = noviPayHubTransactionHistoryActivity.A0A;
        AnonymousClass018 r9 = ((ActivityC13830kP) noviPayHubTransactionHistoryActivity).A01;
        C128205vj r5 = new C128205vj(noviPayHubTransactionHistoryActivity.A00, noviPayHubTransactionHistoryActivity.A01, r8, r9, (AnonymousClass1IR) obj, noviPayHubTransactionHistoryActivity.A05, new AnonymousClass6M8() { // from class: X.6Ck
            @Override // X.AnonymousClass6M8
            public final void AO4(String str, long j) {
                AnonymousClass60Y r4 = C118515bw.this.A00.A06;
                C128365vz r1 = new AnonymousClass610("VIEW_TRANSACTION_DETAILS_CLICK", "PAYMENT_HISTORY", "PAYMENT_HISTORY", "LIST").A00;
                r1.A06 = Long.valueOf(j);
                r1.A0m = str;
                r4.A05(r1);
            }
        }, r13, true);
        r2.A6W(r5);
        if (noviPayHubTransactionHistoryActivity instanceof NoviClaimableTransactionListActivity) {
            r2.setOnClickListener(new IDxCListenerShape2S0200000_3_I1(noviPayHubTransactionHistoryActivity, 18, r5));
        }
        boolean z = true;
        if (i + 1 >= noviPayHubTransactionHistoryActivity.A0B.size()) {
            z = false;
        }
        r2.A02(z);
    }

    @Override // X.AnonymousClass02M
    public AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        return new C118725cH(new C117695aS(viewGroup.getContext()));
    }
}
