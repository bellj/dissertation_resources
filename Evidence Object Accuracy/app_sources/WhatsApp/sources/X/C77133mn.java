package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3mn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77133mn extends AbstractC107394xG {
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(30);
    public final long A00;
    public final long A01;

    public C77133mn(long j, long j2) {
        this.A01 = j;
        this.A00 = j2;
    }

    public static long A00(C95304dT r8, long j) {
        long A0C = (long) r8.A0C();
        if ((128 & A0C) != 0) {
            return 8589934591L & ((((A0C & 1) << 32) | r8.A0I()) + j);
        }
        return -9223372036854775807L;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.A01);
        parcel.writeLong(this.A00);
    }
}
