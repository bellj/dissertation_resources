package X;

/* renamed from: X.1fB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C33851fB extends AbstractC16110oT {
    public Long A00;
    public Long A01;
    public String A02;

    public C33851fB() {
        super(3316, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
        r3.Abe(2, this.A02);
        r3.Abe(3, this.A01);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("WamBadInteraction {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "actual", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "name", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "threshold", this.A01);
        sb.append("}");
        return sb.toString();
    }
}
