package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4lS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C100154lS implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        String readString = parcel.readString();
        AnonymousClass009.A05(readString);
        return new C460324e(readString, C12970iu.A1W(parcel.readByte()));
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C460324e[i];
    }
}
