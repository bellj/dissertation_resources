package X;

import android.view.View;
import androidx.core.widget.NestedScrollView;
import com.whatsapp.R;
import com.whatsapp.registration.EULA;

/* renamed from: X.4rv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C104164rv implements AbstractC11770gq {
    public final /* synthetic */ EULA A00;

    public C104164rv(EULA eula) {
        this.A00 = eula;
    }

    @Override // X.AbstractC11770gq
    public void AVa(NestedScrollView nestedScrollView, int i, int i2, int i3, int i4) {
        View findViewById = this.A00.findViewById(R.id.fade_view);
        int i5 = 8;
        if (i2 > 0) {
            i5 = 0;
        }
        findViewById.setVisibility(i5);
    }
}
