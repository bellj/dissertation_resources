package X;

import java.util.Map;
import java.util.concurrent.Callable;

/* renamed from: X.6Kn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class CallableC135976Kn implements Callable {
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ C129535xs A02;
    public final /* synthetic */ AnonymousClass662 A03;
    public final /* synthetic */ AbstractC1311561m A04;

    public CallableC135976Kn(C129535xs r1, AnonymousClass662 r2, AbstractC1311561m r3, int i, int i2) {
        this.A03 = r2;
        this.A02 = r1;
        this.A04 = r3;
        this.A00 = i;
        this.A01 = i2;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        C119085cr r0;
        AnonymousClass616.A00();
        AnonymousClass662 r4 = this.A03;
        if (!(r4.A0i == null || r4.A0i == this.A02.A02)) {
            r4.A0i.A01();
        }
        C129535xs r1 = this.A02;
        r4.A0i = r1.A02;
        r4.A0A = null;
        r4.A0A = new C124855qE();
        r4.A07 = r1;
        AbstractC1311561m r3 = this.A04;
        r4.A0B = r3;
        Map map = (Map) r3.AAQ(AbstractC1311561m.A01);
        if (!map.isEmpty()) {
            C130175yv r12 = r4.A0U;
            if (!map.isEmpty()) {
                r12.A00 = map;
                if (r12.A03.A09()) {
                    r12.A08();
                }
            }
        }
        r4.A01 = this.A00;
        r4.A0K = C12970iu.A1Y(r3.AAQ(AbstractC1311561m.A09));
        C130175yv r2 = r4.A0U;
        if (r2.A04 == null) {
            r2.A03.A06("Number of cameras must be loaded on background thread.");
            r2.A07();
        }
        if (r2.A04.length != 0) {
            int i = this.A01;
            if (r2.A03.A09()) {
                int i2 = 1;
                if (i == 1) {
                    i2 = 0;
                }
                if (!r2.A09(Integer.valueOf(i2))) {
                    if (r2.A04 == null) {
                        throw C12990iw.A0m("Logical cameras not initialised!");
                    } else if (r2.A04.length == 0) {
                        throw new C118895cY();
                    } else if (i == 0) {
                        if (r2.A09(C12980iv.A0i())) {
                            AnonymousClass616.A02("CameraInventory", "Requested back camera doesn't exist, using front instead");
                            i = 1;
                        }
                        StringBuilder A0k = C12960it.A0k("found ");
                        A0k.append(r2.A04.length);
                        throw C12990iw.A0m(C12960it.A0d(" cameras with bad facing constants", A0k));
                    } else {
                        if (i == 1 && r2.A09(1)) {
                            AnonymousClass616.A02("CameraInventory", "Requested front camera doesn't exist, using back instead");
                            i = 0;
                        }
                        StringBuilder A0k = C12960it.A0k("found ");
                        A0k.append(r2.A04.length);
                        throw C12990iw.A0m(C12960it.A0d(" cameras with bad facing constants", A0k));
                    }
                }
                r4.A09 = new C1308260c();
                String A06 = r2.A06(i);
                try {
                    AnonymousClass662.A06(r4, A06);
                    AnonymousClass662.A07(r4, A06);
                    AnonymousClass662.A05(r4);
                    AnonymousClass662.A08(r4, A06);
                    AnonymousClass616.A00();
                    int i3 = r4.A00;
                    AbstractC130695zp ABG = r4.ABG();
                    if (r4.isConnected() && (r0 = r4.A0C) != null) {
                        return new C127255uC(new C127245uB(ABG, r0, i3));
                    }
                    throw new C136076Kx("Cannot get camera settings");
                } catch (Exception e) {
                    AnonymousClass616.A00();
                    r4.A8z(null);
                    throw e;
                }
            } else {
                throw C12990iw.A0m("Cannot resolve camera facing, not on the Optic thread");
            }
        } else {
            throw new C118895cY();
        }
    }
}
