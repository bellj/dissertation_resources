package X;

import android.media.MediaPlayer;
import com.whatsapp.search.views.itemviews.MessageGifVideoPlayer;

/* renamed from: X.4iA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final /* synthetic */ class C98114iA implements MediaPlayer.OnErrorListener {
    public static final /* synthetic */ C98114iA A00 = new C98114iA();

    @Override // android.media.MediaPlayer.OnErrorListener
    public final boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        return MessageGifVideoPlayer.A01();
    }
}
