package X;

/* renamed from: X.2KW  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2KW {
    public final int A00;
    public final int A01;

    public AnonymousClass2KW(int i, int i2) {
        this.A01 = i;
        this.A00 = i2;
    }

    public byte[] A00() {
        AnonymousClass2KV r8 = (AnonymousClass2KV) this;
        int i = ((AnonymousClass2KW) r8).A01;
        int i2 = ((AnonymousClass2KW) r8).A00;
        int i3 = r8.A01;
        if (i == i3 && i2 == r8.A00) {
            return r8.A04;
        }
        int i4 = i * i2;
        byte[] bArr = new byte[i4];
        int i5 = (r8.A03 * i3) + r8.A02;
        if (i == i3) {
            System.arraycopy(r8.A04, i5, bArr, 0, i4);
            return bArr;
        }
        for (int i6 = 0; i6 < i2; i6++) {
            System.arraycopy(r8.A04, i5, bArr, i6 * i, i);
            i5 += i3;
        }
        return bArr;
    }

    public byte[] A01(byte[] bArr, int i) {
        AnonymousClass2KV r1 = (AnonymousClass2KV) this;
        if (i < 0 || i >= ((AnonymousClass2KW) r1).A00) {
            throw new IllegalArgumentException("Requested row is outside the image: ".concat(String.valueOf(i)));
        }
        int i2 = ((AnonymousClass2KW) r1).A01;
        if (bArr == null || bArr.length < i2) {
            bArr = new byte[i2];
        }
        System.arraycopy(r1.A04, ((i + r1.A03) * r1.A01) + r1.A02, bArr, 0, i2);
        return bArr;
    }

    public final String toString() {
        int i = this.A01;
        byte[] bArr = new byte[i];
        int i2 = this.A00;
        StringBuilder sb = new StringBuilder((i + 1) * i2);
        for (int i3 = 0; i3 < i2; i3++) {
            bArr = A01(bArr, i3);
            for (int i4 = 0; i4 < i; i4++) {
                int i5 = bArr[i4] & 255;
                char c = '#';
                if (i5 >= 64) {
                    c = '+';
                    if (i5 >= 128) {
                        c = ' ';
                        if (i5 < 192) {
                            c = '.';
                        }
                    }
                }
                sb.append(c);
            }
            sb.append('\n');
        }
        return sb.toString();
    }
}
