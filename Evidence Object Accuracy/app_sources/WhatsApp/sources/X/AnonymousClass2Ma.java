package X;

import com.whatsapp.HomeActivity;

/* renamed from: X.2Ma  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Ma implements AnonymousClass07L {
    public final /* synthetic */ HomeActivity A00;

    @Override // X.AnonymousClass07L
    public boolean AUY(String str) {
        return false;
    }

    public AnonymousClass2Ma(HomeActivity homeActivity) {
        this.A00 = homeActivity;
    }

    @Override // X.AnonymousClass07L
    public boolean AUX(String str) {
        HomeActivity homeActivity = this.A00;
        AbstractC14770m1 A2i = homeActivity.A2i(HomeActivity.A02(((ActivityC13830kP) homeActivity).A01, homeActivity.A03));
        if (A2i == null) {
            return false;
        }
        AnonymousClass2LB r0 = homeActivity.A1u;
        r0.A01 = str;
        A2i.A68(r0);
        return false;
    }
}
