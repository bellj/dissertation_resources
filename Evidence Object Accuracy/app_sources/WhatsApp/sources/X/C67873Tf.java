package X;

/* renamed from: X.3Tf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67873Tf implements AnonymousClass5T3 {
    public final /* synthetic */ C93294Zw A00;
    public final /* synthetic */ AnonymousClass28D A01;

    public C67873Tf(C93294Zw r1, AnonymousClass28D r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5T3
    public boolean Afk(AnonymousClass28D r7) {
        Object A04;
        Object obj;
        C93294Zw r5 = this.A00;
        C89824Ln r4 = r5.A00;
        if (!(r4 == null || (obj = r4.A01.get(r7)) == null)) {
            r5.A01.A01.put(r7, obj);
        }
        long j = (long) r7.A00;
        if (r4 == null || (A04 = r4.A00.A04(j, null)) == null) {
            return false;
        }
        r5.A01.A00.A09(j, A04);
        return false;
    }
}
