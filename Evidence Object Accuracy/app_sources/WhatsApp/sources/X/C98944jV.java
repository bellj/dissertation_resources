package X;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4jV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98944jV implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        C56452ku r7 = null;
        IBinder iBinder = null;
        PendingIntent pendingIntent = null;
        IBinder iBinder2 = null;
        IBinder iBinder3 = null;
        int i = 1;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 1:
                    i = C95664e9.A02(parcel, readInt);
                    break;
                case 2:
                    r7 = (C56452ku) C95664e9.A07(parcel, C56452ku.CREATOR, readInt);
                    break;
                case 3:
                    iBinder = C95664e9.A06(parcel, readInt);
                    break;
                case 4:
                    pendingIntent = (PendingIntent) C95664e9.A07(parcel, PendingIntent.CREATOR, readInt);
                    break;
                case 5:
                    iBinder2 = C95664e9.A06(parcel, readInt);
                    break;
                case 6:
                    iBinder3 = C95664e9.A06(parcel, readInt);
                    break;
                default:
                    C95664e9.A0D(parcel, readInt);
                    break;
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78413os(pendingIntent, iBinder, iBinder2, iBinder3, r7, i);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new C78413os[i];
    }
}
