package X;

import com.facebook.redex.RunnableBRunnable0Shape0S1100000_I0;
import com.facebook.redex.RunnableBRunnable0Shape1S0200000_I0_1;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.Voip;
import java.util.Map;

/* renamed from: X.3CL  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3CL {
    public final /* synthetic */ C29631Ua A00;

    public AnonymousClass3CL(C29631Ua r1) {
        this.A00 = r1;
    }

    public void A00(DeviceJid deviceJid) {
        byte byteValue;
        Log.i(C12960it.A0b("VoiceService/notifyNewSessionEstablished ", deviceJid));
        C29631Ua r4 = this.A00;
        r4.A0f(deviceJid, Voip.getCurrentCallId(), false);
        Map map = r4.A2Z;
        String A0t = C12970iu.A0t(deviceJid, map);
        if (A0t != null) {
            Log.i(C12960it.A0b("voip/sendOfferRetryRequest for jid:", deviceJid));
            map.remove(deviceJid);
            r4.A0x.execute(new RunnableBRunnable0Shape0S1100000_I0(17, A0t, deviceJid));
        }
        Number number = (Number) r4.A2a.get(deviceJid);
        if (number != null && (byteValue = number.byteValue()) >= 0 && byteValue <= 4) {
            StringBuilder A0k = C12960it.A0k("voip/sendPendingRekeyRequest for jid:");
            A0k.append(deviceJid);
            A0k.append(", retry:");
            A0k.append(number);
            C12960it.A1F(A0k);
            r4.A0x.execute(new RunnableBRunnable0Shape1S0200000_I0_1(deviceJid, 41, number));
        }
    }
}
