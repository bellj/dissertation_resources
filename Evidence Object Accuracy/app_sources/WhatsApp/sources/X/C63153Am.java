package X;

import android.net.Uri;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.3Am  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C63153Am {
    public static final Map A00(Uri uri) {
        String query = uri.getQuery();
        if (query == null) {
            return null;
        }
        List A09 = AnonymousClass03B.A09(query, new String[]{"&"}, 0);
        int A03 = C17540qy.A03(C16760pi.A0D(A09));
        if (A03 < 16) {
            A03 = 16;
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap(A03);
        Iterator it = A09.iterator();
        while (it.hasNext()) {
            List A092 = AnonymousClass03B.A09(C12970iu.A0x(it), new String[]{"="}, 0);
            C17520qw r0 = new C17520qw(A092.get(0), A092.get(1));
            linkedHashMap.put(r0.first, r0.second);
        }
        return linkedHashMap;
    }
}
