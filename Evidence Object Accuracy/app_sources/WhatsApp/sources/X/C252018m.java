package X;

import android.net.Uri;
import android.text.TextUtils;
import android.util.Pair;

/* renamed from: X.18m  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C252018m {
    public final AnonymousClass018 A00;
    public final C22650zQ A01;

    public C252018m(AnonymousClass018 r1, C22650zQ r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public static String A00(C252018m r0, String str) {
        return r0.A03(str).toString();
    }

    public Uri.Builder A01() {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https");
        builder.authority("faq.whatsapp.com");
        return builder;
    }

    public final Uri A02(Pair pair, String str, String str2, String str3) {
        Uri.Builder A01 = A01();
        A01.appendPath(str);
        if (!TextUtils.isEmpty(str2)) {
            A01.appendPath(str2);
        }
        A05(A01);
        if (pair != null) {
            A01.appendQueryParameter((String) pair.first, (String) pair.second);
        }
        if (!TextUtils.isEmpty(str3)) {
            A01.encodedFragment(str3);
        }
        return A01.build();
    }

    public Uri A03(String str) {
        return A02(null, "general", str, null);
    }

    public Uri A04(String str, String str2) {
        Uri.Builder A01 = A01();
        A01.appendPath("general");
        A01.appendPath(str);
        A01.appendPath(str2);
        A05(A01);
        return A01.build();
    }

    public final void A05(Uri.Builder builder) {
        AnonymousClass018 r2 = this.A00;
        builder.appendQueryParameter("lg", r2.A06());
        builder.appendQueryParameter("lc", r2.A05());
        builder.appendQueryParameter("eea", this.A01.A04() ? "1" : "0");
    }
}
