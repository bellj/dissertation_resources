package X;

/* renamed from: X.4Y1  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4Y1 {
    public final long A00;
    public final long A01;
    public final long A02;
    public final long A03;
    public final C28741Ov A04;
    public final boolean A05;
    public final boolean A06;
    public final boolean A07;

    public AnonymousClass4Y1(C28741Ov r1, long j, long j2, long j3, long j4, boolean z, boolean z2, boolean z3) {
        this.A04 = r1;
        this.A03 = j;
        this.A02 = j2;
        this.A01 = j3;
        this.A00 = j4;
        this.A06 = z;
        this.A07 = z2;
        this.A05 = z3;
    }

    public AnonymousClass4Y1 A00(long j) {
        if (j == this.A02) {
            return this;
        }
        return new AnonymousClass4Y1(this.A04, this.A03, j, this.A01, this.A00, this.A06, this.A07, this.A05);
    }

    public AnonymousClass4Y1 A01(long j) {
        if (j == this.A03) {
            return this;
        }
        return new AnonymousClass4Y1(this.A04, j, this.A02, this.A01, this.A00, this.A06, this.A07, this.A05);
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || AnonymousClass4Y1.class != obj.getClass()) {
                return false;
            }
            AnonymousClass4Y1 r7 = (AnonymousClass4Y1) obj;
            if (!(this.A03 == r7.A03 && this.A02 == r7.A02 && this.A01 == r7.A01 && this.A00 == r7.A00 && this.A06 == r7.A06 && this.A07 == r7.A07 && this.A05 == r7.A05 && AnonymousClass3JZ.A0H(this.A04, r7.A04))) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return ((((((((((((C72453ed.A05(this.A04.hashCode()) + ((int) this.A03)) * 31) + ((int) this.A02)) * 31) + ((int) this.A01)) * 31) + ((int) this.A00)) * 31) + (this.A06 ? 1 : 0)) * 31) + (this.A07 ? 1 : 0)) * 31) + (this.A05 ? 1 : 0);
    }
}
