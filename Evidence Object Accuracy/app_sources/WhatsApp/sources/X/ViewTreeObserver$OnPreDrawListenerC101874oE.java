package X;

import android.view.View;
import android.view.ViewTreeObserver;

/* renamed from: X.4oE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnPreDrawListenerC101874oE implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ AbstractView$OnCreateContextMenuListenerC35851ir A00;

    public ViewTreeObserver$OnPreDrawListenerC101874oE(AbstractView$OnCreateContextMenuListenerC35851ir r1) {
        this.A00 = r1;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        AbstractView$OnCreateContextMenuListenerC35851ir r1 = this.A00;
        if (r1.A0M.getHeight() <= 0 || !r1.A1S.isEmpty()) {
            return true;
        }
        C12980iv.A1G(r1.A0M, this);
        View view = r1.A0M;
        view.setTranslationY((float) view.getHeight());
        return true;
    }
}
