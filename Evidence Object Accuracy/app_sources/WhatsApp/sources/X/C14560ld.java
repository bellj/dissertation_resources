package X;

import com.facebook.redex.RunnableBRunnable0Shape8S0100000_I0_8;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.0ld  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14560ld extends AbstractRunnableC14570le {
    public long A00;
    public C37741mv A01;
    public C455221z A02;
    public File A03;
    public String A04;
    public AtomicReference A05 = new AtomicReference(null);
    public boolean A06;
    public final AbstractC15710nm A07;
    public final C14330lG A08;
    public final C14900mE A09;
    public final C002701f A0A;
    public final C15450nH A0B;
    public final C18790t3 A0C;
    public final C14620lj A0D;
    public final C14620lj A0E;
    public final C14620lj A0F;
    public final C14620lj A0G;
    public final C14620lj A0H;
    public final C14830m7 A0I;
    public final C16590pI A0J;
    public final AnonymousClass2QI A0K;
    public final AbstractC14500lX A0L;
    public final C15660nh A0M;
    public final C241114g A0N;
    public final C14850m9 A0O;
    public final C19950uw A0P;
    public final C20110vE A0Q;
    public final C19940uv A0R;
    public final AnonymousClass14Q A0S;
    public final AnonymousClass14D A0T;
    public final AnonymousClass155 A0U;
    public final C14510lY A0V;
    public final C91434Rs A0W;
    public final C29171Rd A0X;
    public final C14520lZ A0Y;
    public final AnonymousClass14C A0Z;
    public final AnonymousClass153 A0a;
    public final AnonymousClass14R A0b;
    public final C455121y A0c;
    public final C16630pM A0d;
    public final AbstractC37811n3 A0e;
    public final C22600zL A0f;
    public final AbstractC14440lR A0g;
    public final boolean A0h;
    public final boolean A0i;
    public volatile long A0j;
    public volatile boolean A0k;

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x011b, code lost:
        if (r6.A02 <= 52428800) goto L_0x011d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C14560ld(X.AbstractC15710nm r16, X.C14330lG r17, X.C14900mE r18, X.C002701f r19, X.C15450nH r20, X.C18790t3 r21, X.C14830m7 r22, X.C16590pI r23, X.C15660nh r24, X.C241114g r25, X.C14850m9 r26, X.C19950uw r27, X.C20110vE r28, X.C19940uv r29, X.AnonymousClass14Q r30, X.AnonymousClass14D r31, X.AnonymousClass155 r32, X.C14510lY r33, X.AnonymousClass14C r34, X.AnonymousClass153 r35, X.AnonymousClass14R r36, X.C16630pM r37, X.C22600zL r38, X.AbstractC14440lR r39) {
        /*
        // Method dump skipped, instructions count: 301
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14560ld.<init>(X.0nm, X.0lG, X.0mE, X.01f, X.0nH, X.0t3, X.0m7, X.0pI, X.0nh, X.14g, X.0m9, X.0uw, X.0vE, X.0uv, X.14Q, X.14D, X.155, X.0lY, X.14C, X.153, X.14R, X.0pM, X.0zL, X.0lR):void");
    }

    @Override // X.AbstractRunnableC14570le, X.C14580lf
    public void A04() {
        super.A04();
        this.A0D.A01();
        this.A0G.A01();
        this.A0H.A01();
        this.A0F.A01();
        this.A0E.A01();
    }

    public final AnonymousClass4TD A07(C37741mv r11, C455221z r12, String str) {
        AnonymousClass14R r0 = this.A0b;
        AnonymousClass14Q r4 = r0.A03;
        AnonymousClass3DM r02 = new AnonymousClass3DM(r0.A00, r0.A01, r0.A02, r4, r11, r12, str);
        AnonymousClass3FM A00 = r02.A00();
        C39861qf r42 = r02.A05;
        AnonymousClass4AV r3 = A00.A02;
        if (r3 == null || r3 == AnonymousClass4AV.FAILURE) {
            StringBuilder sb = new StringBuilder("mediaupload/the resume request and the fallback mms resume request failed; request=");
            sb.append(this.A0V);
            Log.i(sb.toString());
            return new AnonymousClass4TD(r42, null, null, 17, 0, false);
        } else if (r3 == AnonymousClass4AV.COMPLETE) {
            StringBuilder sb2 = new StringBuilder("mediaupload/object already existed on media server; upload ending; request=");
            sb2.append(this.A0V);
            Log.i(sb2.toString());
            return new AnonymousClass4TD(r42, A00.A03, A00.A05, 0, 0, true);
        } else if (r3 == AnonymousClass4AV.RESUME) {
            StringBuilder sb3 = new StringBuilder("mediaupload/resume from ");
            sb3.append(A00.A01);
            sb3.append("; request=");
            sb3.append(this.A0V);
            Log.i(sb3.toString());
            return new AnonymousClass4TD(r42, null, null, 0, A00.A01, false);
        } else {
            StringBuilder sb4 = new StringBuilder("unhandled result type in checkForResumePoint, type=");
            sb4.append(r3);
            throw new IllegalStateException(sb4.toString());
        }
    }

    public void A08() {
        int i;
        int i2;
        C14380lL r2 = this.A0V.A02;
        C14370lK r1 = r2.A05;
        if (r1 == C14370lK.A0X || r1 == C14370lK.A04 || r1 == C14370lK.A0a) {
            try {
                File file = r2.A06;
                AnonymousClass009.A05(file);
                C38991p4 r12 = new C38991p4(file);
                if (r12.A02()) {
                    i = r12.A01;
                } else {
                    i = r12.A03;
                }
                if (r12.A02()) {
                    i2 = r12.A03;
                } else {
                    i2 = r12.A01;
                }
                C29171Rd r13 = this.A0X;
                synchronized (r13) {
                    r13.A03 = Integer.valueOf(i2);
                    r13.A04 = Integer.valueOf(i);
                }
            } catch (AnonymousClass47W e) {
                Log.w("MMS upload unable to get video meta", e);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0050, code lost:
        if (r3 != 0) goto L_0x0052;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A09(long r6) {
        /*
            r5 = this;
            long r1 = r5.A0j
            int r0 = (r6 > r1 ? 1 : (r6 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x0019
            X.0t3 r4 = r5.A0C
            long r0 = r5.A0j
            long r2 = r6 - r0
            X.0lY r0 = r5.A0V
            X.0lV r0 = r0.A03
            boolean r1 = r0.A01
            r0 = 0
            if (r1 == 0) goto L_0x0016
            r0 = 4
        L_0x0016:
            r4.A04(r2, r0)
        L_0x0019:
            r5.A0j = r6
            X.0lZ r2 = r5.A0Y
            long r0 = r5.A0j
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            r2.A08 = r0
            java.util.concurrent.FutureTask r0 = r5.A02
            boolean r0 = r0.isCancelled()
            if (r0 != 0) goto L_0x0061
            X.0lY r1 = r5.A0V
            boolean r0 = r1 instanceof X.C37991nL
            if (r0 == 0) goto L_0x0066
            r0 = r1
            X.1nL r0 = (X.C37991nL) r0
            boolean r0 = r0.A01
            if (r0 == 0) goto L_0x0062
            X.0lL r0 = r1.A02
            java.io.File r1 = r0.A06
            X.AnonymousClass009.A05(r1)
            boolean r0 = r1.exists()
            X.AnonymousClass009.A0F(r0)
            long r3 = r1.length()
        L_0x004c:
            r1 = 0
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x0057
        L_0x0052:
            r0 = 100
            long r6 = r6 * r0
            long r1 = r6 / r3
        L_0x0057:
            int r0 = (int) r1
            X.0lj r1 = r5.A0G
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r1.A04(r0)
        L_0x0061:
            return
        L_0x0062:
            r3 = 65536(0x10000, double:3.2379E-319)
            goto L_0x0052
        L_0x0066:
            X.0lL r0 = r1.A02
            long r3 = r0.A02
            goto L_0x004c
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14560ld.A09(long):void");
    }

    public final void A0A(Integer num) {
        C29171Rd r6;
        boolean z;
        if (this.A0k) {
            this.A0k = false;
            this.A0f.A04(this.A0e);
        }
        C14510lY r5 = this.A0V;
        if (r5.A02.A0B) {
            C29171Rd r4 = this.A0X;
            r4.A07(this.A0S.A00(C88044Eb.A00(r4.A03(), this.A0I.A00())));
        }
        if (num.intValue() == 18) {
            this.A0R.A00();
        }
        if (((AbstractRunnableC14570le) this).A02.isCancelled()) {
            num = 1;
        }
        C14620lj r1 = this.A0H;
        boolean z2 = this instanceof C39261pY;
        C29171Rd r3 = this.A0X;
        synchronized (r3) {
            r6 = new C29171Rd();
            r6.A00 = r3.A00;
            r6.A0I = r3.A0I;
            r6.A0K = r3.A0K;
            r6.A0L = r3.A0L;
            r6.A03 = r3.A03;
            r6.A04 = r3.A04;
            r6.A09 = r3.A09;
            r6.A08 = r3.A08;
            r6.A02 = r3.A02;
            r6.A0B = r3.A0B;
            r6.A07 = r3.A07;
            r6.A05 = r3.A05;
            r6.A0F = r3.A0F;
            r6.A0H = r3.A0H;
            r6.A0G = r3.A0G;
            r6.A0D = r3.A0D;
            r6.A0C = r3.A0C;
            r6.A0J = r3.A0J;
            r6.A01 = r3.A01;
            r6.A06 = r3.A06;
            r6.A0A = r3.A0A;
            r6.A0E = r3.A0E;
        }
        int intValue = num.intValue();
        boolean z3 = this.A06;
        if (!z2) {
            z = false;
        } else {
            z = true;
        }
        r1.A04(new C39721qR(r5, r6, this.A03, intValue, z3, z));
    }

    public final boolean A0B() {
        C14380lL r2 = this.A0V.A02;
        C14370lK r1 = r2.A05;
        if (!C38101nW.A00(r1)) {
            return false;
        }
        if (r1 == C14370lK.A0X || r1 == C14370lK.A0W || r1 == C14370lK.A0a) {
            return true;
        }
        int[] iArr = r2.A0E;
        return iArr != null && iArr.length > 0;
    }

    @Override // X.AbstractRunnableC14570le, X.AbstractC14600lh
    public void cancel() {
        if (this.A05.get() != null && (this instanceof C39261pY)) {
            AnonymousClass009.A05(this.A02);
            this.A0g.Ab2(new RunnableBRunnable0Shape8S0100000_I0_8(this, 2));
        }
        super.cancel();
    }
}
