package X;

import android.content.ContentValues;
import android.database.Cursor;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* renamed from: X.13c  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C238113c {
    public final C15570nT A00;
    public final C14830m7 A01;
    public final C22320yt A02;
    public final C16370ot A03;
    public final C20650w6 A04;
    public final C16510p9 A05;
    public final C19990v2 A06;
    public final C15650ng A07;
    public final C242814x A08;
    public final C242614v A09;
    public final C20560vx A0A;
    public final C21380xK A0B;
    public final C20280vV A0C;
    public final C18460sU A0D;
    public final C20090vC A0E;
    public final C21620xi A0F;
    public final C242914y A0G;
    public final AnonymousClass12H A0H;
    public final C16490p7 A0I;
    public final C20500vr A0J;
    public final C22440z5 A0K;
    public final C22810zg A0L;
    public final C20860wR A0M;
    public final C18470sV A0N;
    public final C242314s A0O;
    public final C20950wa A0P;
    public final C20510vs A0Q;
    public final C22290yq A0R;
    public final C21400xM A0S;
    public final C21660xm A0T;
    public final C14850m9 A0U;
    public final C16120oU A0V;
    public final C22370yy A0W;
    public final C22170ye A0X;
    public final C22230yk A0Y;
    public final C20320vZ A0Z;
    public final C240614b A0a;
    public final Object A0b = new Object();

    public C238113c(C15570nT r2, C14830m7 r3, C22320yt r4, C16370ot r5, C20650w6 r6, C16510p9 r7, C19990v2 r8, C15650ng r9, C242814x r10, C242614v r11, C20560vx r12, C21380xK r13, C20280vV r14, C18460sU r15, C20090vC r16, C21620xi r17, C242914y r18, AnonymousClass12H r19, C16490p7 r20, C20500vr r21, C22440z5 r22, C22810zg r23, C20860wR r24, C18470sV r25, C242314s r26, C20950wa r27, C20510vs r28, C22290yq r29, C21400xM r30, C21660xm r31, C14850m9 r32, C16120oU r33, C22370yy r34, C22170ye r35, C22230yk r36, C20320vZ r37, C240614b r38) {
        this.A01 = r3;
        this.A0U = r32;
        this.A0D = r15;
        this.A05 = r7;
        this.A00 = r2;
        this.A06 = r8;
        this.A0V = r33;
        this.A04 = r6;
        this.A0N = r25;
        this.A0X = r35;
        this.A0B = r13;
        this.A0L = r23;
        this.A0Y = r36;
        this.A0Z = r37;
        this.A0R = r29;
        this.A07 = r9;
        this.A0E = r16;
        this.A0H = r19;
        this.A0J = r21;
        this.A0Q = r28;
        this.A02 = r4;
        this.A03 = r5;
        this.A0S = r30;
        this.A0F = r17;
        this.A0I = r20;
        this.A0M = r24;
        this.A0W = r34;
        this.A0O = r26;
        this.A09 = r11;
        this.A0P = r27;
        this.A0T = r31;
        this.A0C = r14;
        this.A0A = r12;
        this.A0K = r22;
        this.A08 = r10;
        this.A0G = r18;
        this.A0a = r38;
    }

    public C43961xv A00(Cursor cursor) {
        Jid A07;
        Jid A072;
        String string = cursor.getString(cursor.getColumnIndexOrThrow("key_id"));
        boolean z = false;
        if (cursor.getLong(cursor.getColumnIndexOrThrow("from_me")) > 0) {
            z = true;
        }
        AbstractC14640lm A05 = this.A05.A05(cursor.getLong(cursor.getColumnIndexOrThrow("chat_row_id")));
        if (A05 == null) {
            return null;
        }
        cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
        AnonymousClass1IS r6 = new AnonymousClass1IS(A05, string, z);
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow("sender_jid_row_id");
        if (cursor.isNull(columnIndexOrThrow)) {
            A07 = null;
        } else {
            A07 = this.A0D.A07(AbstractC14640lm.class, cursor.getLong(columnIndexOrThrow));
        }
        AbstractC14640lm r4 = (AbstractC14640lm) A07;
        long j = cursor.getLong(cursor.getColumnIndexOrThrow("timestamp"));
        int i = cursor.getInt(cursor.getColumnIndexOrThrow("message_type"));
        String string2 = cursor.getString(cursor.getColumnIndexOrThrow("revoked_key_id"));
        int columnIndexOrThrow2 = cursor.getColumnIndexOrThrow("admin_jid_row_id");
        if (cursor.isNull(columnIndexOrThrow2)) {
            A072 = null;
        } else {
            A072 = this.A0D.A07(UserJid.class, cursor.getLong(columnIndexOrThrow2));
        }
        return new C43961xv(r4, (UserJid) A072, r6, string2, i, cursor.getInt(cursor.getColumnIndexOrThrow("retry_count")), j);
    }

    public void A01(C43961xv r19) {
        String l;
        C43961xv A00;
        AnonymousClass1IS r5 = r19.A05;
        C16510p9 r11 = this.A05;
        AbstractC14640lm r10 = r5.A00;
        AnonymousClass009.A05(r10);
        String l2 = Long.toString(r11.A02(r10));
        AbstractC14640lm r12 = r19.A03;
        if (r12 == null) {
            l = "";
        } else {
            l = Long.toString(this.A0D.A01(r12));
        }
        C16310on A01 = this.A0I.get();
        try {
            AnonymousClass1Lx A002 = A01.A00();
            C16330op r6 = A01.A03;
            String[] strArr = new String[4];
            String str = r5.A01;
            int i = 0;
            strArr[0] = str;
            boolean z = r5.A02;
            int i2 = 0;
            if (z) {
                i2 = 1;
            }
            strArr[1] = String.valueOf(i2);
            strArr[2] = l2;
            strArr[3] = l;
            Cursor A09 = r6.A09("SELECT _id, key_id, from_me, chat_row_id, sender_jid_row_id, timestamp, message_type, revoked_key_id, retry_count, admin_jid_row_id  FROM message_orphaned_edit WHERE key_id = ? AND from_me = ? AND chat_row_id = ? AND sender_jid_row_id = ?", strArr);
            try {
                Long l3 = null;
                if (!A09.moveToLast() || (A00 = A00(A09)) == null || (A00.A01 == 1 && r19.A01 == 0)) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("key_id", str);
                    if (z) {
                        i = 1;
                    }
                    contentValues.put("from_me", Integer.valueOf(i));
                    AnonymousClass009.A05(r10);
                    contentValues.put("chat_row_id", Long.valueOf(r11.A02(r10)));
                    contentValues.put("sender_jid_row_id", Long.valueOf(r12 == null ? 0 : this.A0D.A01(r12)));
                    contentValues.put("timestamp", Long.valueOf(r19.A02));
                    contentValues.put("message_type", Integer.valueOf(r19.A01));
                    contentValues.put("revoked_key_id", r19.A06);
                    contentValues.put("retry_count", Integer.valueOf(r19.A00));
                    UserJid userJid = r19.A04;
                    if (userJid != null) {
                        l3 = Long.valueOf(this.A0D.A01(userJid));
                    }
                    contentValues.put("admin_jid_row_id", l3);
                    r6.A04(contentValues, "message_orphaned_edit");
                } else {
                    StringBuilder sb = new StringBuilder();
                    sb.append("msgstore/skipping-edit-store old message exists; msg.key=");
                    sb.append(r5);
                    Log.i(sb.toString());
                }
                A002.A00();
                A09.close();
                A002.close();
                A01.close();
            } catch (Throwable th) {
                if (A09 != null) {
                    try {
                        A09.close();
                    } catch (Throwable unused) {
                    }
                }
                throw th;
            }
        } catch (Throwable th2) {
            try {
                A01.close();
            } catch (Throwable unused2) {
            }
            throw th2;
        }
    }

    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    public final void A02(X.C30331Wz r29, int r30, boolean r31, boolean r32) {
        /*
        // Method dump skipped, instructions count: 2477
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C238113c.A02(X.1Wz, int, boolean, boolean):void");
    }

    public void A03(C30331Wz r5, boolean z) {
        String str;
        StringBuilder sb = new StringBuilder("msgstore/edit/revoke ");
        AnonymousClass1IS r2 = r5.A0z;
        if (r2.A02) {
            StringBuilder sb2 = new StringBuilder("send deleteMedia=");
            sb2.append(z);
            str = sb2.toString();
        } else {
            str = "recv";
        }
        sb.append(str);
        sb.append(" key=");
        sb.append(r2);
        Log.i(sb.toString());
        A02(r5, -1, true, z);
    }
}
