package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.4uJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C105594uJ implements AbstractC009404s {
    public final AnonymousClass3EX A00;
    public final UserJid A01;

    public C105594uJ(AnonymousClass3EX r1, UserJid userJid) {
        this.A01 = userJid;
        this.A00 = r1;
    }

    @Override // X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        return new C53872fS(this.A00, this.A01);
    }
}
