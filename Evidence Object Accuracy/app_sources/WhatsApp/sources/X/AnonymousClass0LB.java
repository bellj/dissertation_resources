package X;

import android.database.Cursor;

/* renamed from: X.0LB  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0LB {
    public static int A00(Cursor cursor, String str) {
        int columnIndex = cursor.getColumnIndex(str);
        if (columnIndex >= 0) {
            return columnIndex;
        }
        StringBuilder sb = new StringBuilder("`");
        sb.append(str);
        sb.append("`");
        return cursor.getColumnIndexOrThrow(sb.toString());
    }
}
