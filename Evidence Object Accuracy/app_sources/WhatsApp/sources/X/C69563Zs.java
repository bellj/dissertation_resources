package X;

import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.Voip;

/* renamed from: X.3Zs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69563Zs implements AbstractC44971zr {
    public final /* synthetic */ C29631Ua A00;

    public C69563Zs(C29631Ua r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC44971zr
    public void AVX(boolean z) {
        StringBuilder A0k = C12960it.A0k("voip/onScreenLockChanged:");
        A0k.append(z);
        C12960it.A1F(A0k);
        C29631Ua r1 = this.A00;
        r1.A1G = z;
        if (Voip.A09(Voip.getCallInfo()) && !r1.A1G && r1.A13 && r1.A0P != null) {
            Log.i("onScreenLockChanged screen is turned on, but ear-near is still true, release and reacquire the proximity wake lock.");
            r1.A0V();
            r1.A0L.sendEmptyMessageDelayed(14, 6000);
        }
    }
}
