package X;

/* renamed from: X.4XW  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass4XW {
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002d, code lost:
        return r20 + r3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A00(java.lang.CharSequence r18, byte[] r19, int r20, int r21) {
        /*
        // Method dump skipped, instructions count: 486
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass4XW.A00(java.lang.CharSequence, byte[], int, int):int");
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:163:0x00ff */
    /* JADX WARNING: Code restructure failed: missing block: B:183:?, code lost:
        return r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:185:?, code lost:
        return -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:186:?, code lost:
        return -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:187:?, code lost:
        return -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:188:?, code lost:
        return -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:189:?, code lost:
        return -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x006b, code lost:
        return -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0092, code lost:
        if (r1 == 0) goto L_0x00be;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0095, code lost:
        if (r1 == 1) goto L_0x00b1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0097, code lost:
        if (r1 != 2) goto L_0x00d6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0099, code lost:
        r4 = X.C95634e6.A00(r14, r2);
        r1 = X.C95634e6.A00(r14, r2 + 1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00a4, code lost:
        if (r7 > -12) goto L_0x006b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00a6, code lost:
        if (r4 > -65) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00a8, code lost:
        if (r1 > -65) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00aa, code lost:
        r7 = r7 ^ (r4 << 8);
        r0 = r1 << 16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00b0, code lost:
        return r7 ^ r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00b1, code lost:
        r1 = X.C95634e6.A00(r14, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00b7, code lost:
        if (r7 > -12) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00b9, code lost:
        if (r1 > -65) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00bb, code lost:
        r0 = r1 << 8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00c0, code lost:
        if (r7 <= -12) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x00db, code lost:
        throw new java.lang.AssertionError();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A01(byte[] r14, int r15, int r16) {
        /*
        // Method dump skipped, instructions count: 408
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass4XW.A01(byte[], int, int):int");
    }
}
