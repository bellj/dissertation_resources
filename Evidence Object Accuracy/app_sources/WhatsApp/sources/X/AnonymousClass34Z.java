package X;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import com.whatsapp.R;
import java.util.List;

/* renamed from: X.34Z  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass34Z extends AbstractC863546x {
    public C53202d5 A00;
    public AnonymousClass2VI A01;
    public boolean A02;
    public final C15570nT A03;
    public final AnonymousClass130 A04;
    public final AnonymousClass1J1 A05;
    public final C14830m7 A06;
    public final AnonymousClass018 A07;
    public final C20830wO A08;
    public final C16030oK A09;
    public final C244415n A0A;

    public AnonymousClass34Z(Context context, C15570nT r2, AnonymousClass130 r3, AnonymousClass1J1 r4, C14830m7 r5, AnonymousClass018 r6, C20830wO r7, C16030oK r8, C244415n r9) {
        super(context);
        A00();
        this.A06 = r5;
        this.A03 = r2;
        this.A0A = r9;
        this.A04 = r3;
        this.A07 = r6;
        this.A05 = r4;
        this.A09 = r8;
        this.A08 = r7;
        A03();
    }

    @Override // X.AbstractC74143hO
    public void A00() {
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
    }

    @Override // X.AnonymousClass46z
    public View A01() {
        this.A00 = new C53202d5(getContext());
        FrameLayout.LayoutParams A0M = C12990iw.A0M();
        int A06 = C12980iv.A06(this);
        C42941w9.A0A(this.A00, this.A07, A06, 0, A06, 0);
        this.A00.setLayoutParams(A0M);
        return this.A00;
    }

    @Override // X.AnonymousClass46z
    public View A02() {
        Context context = getContext();
        C14830m7 r5 = this.A06;
        C15570nT r2 = this.A03;
        C244415n r8 = this.A0A;
        this.A01 = new AnonymousClass2VI(context, r2, this.A04, this.A05, r5, this.A08, this.A09, r8);
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.search_attachment_height_regular);
        this.A01.setLayoutParams(new FrameLayout.LayoutParams(dimensionPixelSize, dimensionPixelSize));
        return this.A01;
    }

    public void setMessage(AnonymousClass1XP r12, List list) {
        String string;
        long A04;
        String A01;
        String str = "";
        if (r12 instanceof AnonymousClass1XO) {
            AnonymousClass1XO r0 = (AnonymousClass1XO) r12;
            string = r0.A01;
            if (string == null) {
                string = str;
            }
            A01 = r0.A00;
            String A16 = r0.A16();
            if (A16 != null) {
                Uri parse = Uri.parse(A16);
                if (parse.getHost() != null) {
                    str = parse.getHost();
                }
            }
            if (TextUtils.isEmpty(string) && TextUtils.isEmpty(A01)) {
                string = getContext().getString(R.string.pinned_location);
            }
        } else {
            C30341Xa r9 = (C30341Xa) r12;
            string = getContext().getString(R.string.live_location);
            C16030oK r8 = this.A09;
            if (r9.A0z.A02) {
                A04 = r8.A05(r9);
            } else {
                A04 = r8.A04(r9);
            }
            C14830m7 r6 = this.A06;
            A01 = C65173Ik.A01(getContext(), this.A03, r6, this.A07, r8, r9, C65173Ik.A02(r6, r9, A04));
        }
        this.A00.setTitleAndDescription(string, A01, list);
        if (str != null) {
            this.A00.setSubText(str, null);
        }
        this.A01.setMessage(r12);
    }
}
