package X;

import android.view.View;

/* renamed from: X.0cI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09060cI implements Runnable {
    public final /* synthetic */ AnonymousClass0XR A00;

    public RunnableC09060cI(AnonymousClass0XR r1) {
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        AnonymousClass0XR r1 = this.A00;
        View view = r1.A0A;
        if (view != null && view.getWindowToken() != null) {
            r1.Ade();
        }
    }
}
