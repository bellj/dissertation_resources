package X;

import java.net.InetAddress;
import java.net.Socket;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

/* renamed from: X.3dX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C71803dX extends SSLSocketFactory {
    public final int A00 = 3;
    public final int A01 = 3;
    public final C18790t3 A02;
    public final SSLSocketFactory A03 = C88074Ee.A00();

    public C71803dX(C18790t3 r3) {
        this.A02 = r3;
    }

    public static C71793dW A00(C71803dX r4, Object obj) {
        int i = r4.A00;
        return new C71793dW(r4.A02, (SSLSocket) obj, i, r4.A01);
    }

    @Override // javax.net.SocketFactory
    public Socket createSocket(String str, int i) {
        return A00(this, this.A03.createSocket(str, i));
    }

    @Override // javax.net.SocketFactory
    public Socket createSocket(String str, int i, InetAddress inetAddress, int i2) {
        return A00(this, this.A03.createSocket(str, i, inetAddress, i2));
    }

    @Override // javax.net.SocketFactory
    public Socket createSocket(InetAddress inetAddress, int i) {
        return A00(this, this.A03.createSocket(inetAddress, i));
    }

    @Override // javax.net.SocketFactory
    public Socket createSocket(InetAddress inetAddress, int i, InetAddress inetAddress2, int i2) {
        return A00(this, this.A03.createSocket(inetAddress, i, inetAddress2, i2));
    }

    @Override // javax.net.ssl.SSLSocketFactory
    public Socket createSocket(Socket socket, String str, int i, boolean z) {
        return A00(this, this.A03.createSocket(socket, str, i, z));
    }

    @Override // javax.net.ssl.SSLSocketFactory
    public String[] getDefaultCipherSuites() {
        return this.A03.getDefaultCipherSuites();
    }

    @Override // javax.net.ssl.SSLSocketFactory
    public String[] getSupportedCipherSuites() {
        return this.A03.getSupportedCipherSuites();
    }
}
