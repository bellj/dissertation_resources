package X;

import android.animation.Animator;
import android.view.View;
import android.view.ViewGroup;
import java.util.Map;

/* renamed from: X.0G0  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass0G0 extends AnonymousClass072 {
    public static final String[] A01 = {"android:visibility:visibility", "android:visibility:parent"};
    public int A00 = 3;

    public abstract Animator A0V(View view, ViewGroup viewGroup, C05350Pf v, C05350Pf v2);

    public abstract Animator A0W(View view, ViewGroup viewGroup, C05350Pf v, C05350Pf v2);

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0060, code lost:
        if (r9 == null) goto L_0x0062;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0064, code lost:
        if (r5.A01 == 0) goto L_0x0066;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x006f, code lost:
        if (r1 == 0) goto L_0x0071;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0078, code lost:
        if (r5.A03 == null) goto L_0x0071;
     */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0030  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final X.C05030Nz A00(X.C05350Pf r8, X.C05350Pf r9) {
        /*
            X.0Nz r5 = new X.0Nz
            r5.<init>()
            r6 = 0
            r5.A05 = r6
            r5.A04 = r6
            java.lang.String r3 = "android:visibility:parent"
            r4 = 0
            r7 = -1
            java.lang.String r2 = "android:visibility:visibility"
            if (r8 == 0) goto L_0x007e
            java.util.Map r1 = r8.A02
            boolean r0 = r1.containsKey(r2)
            if (r0 == 0) goto L_0x007e
            java.lang.Object r0 = r1.get(r2)
            java.lang.Number r0 = (java.lang.Number) r0
            int r0 = r0.intValue()
            r5.A01 = r0
            java.lang.Object r0 = r1.get(r3)
            android.view.ViewGroup r0 = (android.view.ViewGroup) r0
            r5.A03 = r0
        L_0x002e:
            if (r9 == 0) goto L_0x007b
            java.util.Map r1 = r9.A02
            boolean r0 = r1.containsKey(r2)
            if (r0 == 0) goto L_0x007b
            java.lang.Object r0 = r1.get(r2)
            java.lang.Number r0 = (java.lang.Number) r0
            int r0 = r0.intValue()
            r5.A00 = r0
            java.lang.Object r4 = r1.get(r3)
            android.view.ViewGroup r4 = (android.view.ViewGroup) r4
        L_0x004a:
            r5.A02 = r4
            r3 = 1
            if (r8 == 0) goto L_0x005c
            if (r9 == 0) goto L_0x0062
            int r2 = r5.A01
            int r1 = r5.A00
            if (r2 != r1) goto L_0x006b
            android.view.ViewGroup r0 = r5.A03
            if (r0 != r4) goto L_0x006b
        L_0x005b:
            return r5
        L_0x005c:
            int r0 = r5.A00
            if (r0 == 0) goto L_0x0071
            if (r9 != 0) goto L_0x005b
        L_0x0062:
            int r0 = r5.A01
            if (r0 != 0) goto L_0x005b
        L_0x0066:
            r5.A04 = r6
        L_0x0068:
            r5.A05 = r3
            return r5
        L_0x006b:
            if (r2 == r1) goto L_0x0074
            if (r2 == 0) goto L_0x0066
            if (r1 != 0) goto L_0x005b
        L_0x0071:
            r5.A04 = r3
            goto L_0x0068
        L_0x0074:
            if (r4 == 0) goto L_0x0066
            android.view.ViewGroup r0 = r5.A03
            if (r0 != 0) goto L_0x005b
            goto L_0x0071
        L_0x007b:
            r5.A00 = r7
            goto L_0x004a
        L_0x007e:
            r5.A01 = r7
            r5.A03 = r4
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0G0.A00(X.0Pf, X.0Pf):X.0Nz");
    }

    public static final void A03(C05350Pf r3) {
        int visibility = r3.A00.getVisibility();
        Map map = r3.A02;
        map.put("android:visibility:visibility", Integer.valueOf(visibility));
        map.put("android:visibility:parent", r3.A00.getParent());
        int[] iArr = new int[2];
        r3.A00.getLocationOnScreen(iArr);
        map.put("android:visibility:screenLocation", iArr);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0036, code lost:
        if (A00(A0A(r2, false), A0B(r2, false)).A05 != false) goto L_0x0038;
     */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0058 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x012b  */
    @Override // X.AnonymousClass072
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.animation.Animator A0Q(android.view.ViewGroup r10, X.C05350Pf r11, X.C05350Pf r12) {
        /*
        // Method dump skipped, instructions count: 349
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0G0.A0Q(android.view.ViewGroup, X.0Pf, X.0Pf):android.animation.Animator");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0003, code lost:
        if (r6 == null) goto L_0x0005;
     */
    @Override // X.AnonymousClass072
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0R(X.C05350Pf r5, X.C05350Pf r6) {
        /*
            r4 = this;
            r3 = 0
            if (r5 != 0) goto L_0x0006
            if (r6 != 0) goto L_0x0019
        L_0x0005:
            return r3
        L_0x0006:
            if (r6 == 0) goto L_0x0019
            java.util.Map r0 = r6.A02
            java.lang.String r2 = "android:visibility:visibility"
            boolean r1 = r0.containsKey(r2)
            java.util.Map r0 = r5.A02
            boolean r0 = r0.containsKey(r2)
            if (r1 == r0) goto L_0x0019
            return r3
        L_0x0019:
            X.0Nz r1 = A00(r5, r6)
            boolean r0 = r1.A05
            if (r0 == 0) goto L_0x0005
            int r0 = r1.A01
            if (r0 == 0) goto L_0x0029
            int r0 = r1.A00
            if (r0 != 0) goto L_0x0005
        L_0x0029:
            r3 = 1
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0G0.A0R(X.0Pf, X.0Pf):boolean");
    }

    @Override // X.AnonymousClass072
    public String[] A0S() {
        return A01;
    }

    @Override // X.AnonymousClass072
    public void A0T(C05350Pf r1) {
        A03(r1);
    }

    @Override // X.AnonymousClass072
    public void A0U(C05350Pf r1) {
        A03(r1);
    }
}
