package X;

import java.security.cert.CertPathValidatorException;
import java.security.cert.Certificate;
import java.security.cert.PKIXRevocationChecker;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.5Hn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C113425Hn extends PKIXRevocationChecker implements AnonymousClass5WR {
    public static final Map A04;
    public AnonymousClass4TK A00;
    public final AnonymousClass5S2 A01;
    public final AnonymousClass5GJ A02;
    public final AnonymousClass5GK A03;

    public C113425Hn(AnonymousClass5S2 r2) {
        this.A01 = r2;
        this.A02 = new AnonymousClass5GJ(r2);
        this.A03 = new AnonymousClass5GK(r2, this);
    }

    @Override // X.AnonymousClass5WR
    public void AIv(AnonymousClass4TK r2) {
        this.A00 = r2;
        this.A02.AIv(r2);
        this.A03.AIv(r2);
    }

    @Override // java.security.cert.PKIXCertPathChecker
    public Set getSupportedExtensions() {
        return null;
    }

    @Override // java.security.cert.PKIXCertPathChecker, java.security.cert.CertPathChecker
    public boolean isForwardCheckingSupported() {
        return false;
    }

    static {
        HashMap A11 = C12970iu.A11();
        A04 = A11;
        A11.put(C72453ed.A12("1.2.840.113549.1.1.5"), "SHA1WITHRSA");
        A11.put(AnonymousClass1TJ.A2D, "SHA224WITHRSA");
        A11.put(AnonymousClass1TJ.A2E, "SHA256WITHRSA");
        C72453ed.A1L(AnonymousClass1TJ.A2F, A11);
        C72453ed.A1M(AbstractC116955Xo.A0G, A11);
    }

    @Override // java.security.cert.PKIXCertPathChecker
    public void check(Certificate certificate, Collection collection) {
        X509Certificate x509Certificate = (X509Certificate) certificate;
        if (!getOptions().contains(PKIXRevocationChecker.Option.ONLY_END_ENTITY) || x509Certificate.getBasicConstraints() == -1) {
            if (getOptions().contains(PKIXRevocationChecker.Option.PREFER_CRLS)) {
                try {
                    this.A02.check(certificate);
                } catch (C113375Hg e) {
                    if (!getOptions().contains(PKIXRevocationChecker.Option.NO_FALLBACK)) {
                        this.A03.check(certificate);
                        return;
                    }
                    throw e;
                }
            } else {
                try {
                    this.A03.check(certificate);
                } catch (C113375Hg e2) {
                    if (!getOptions().contains(PKIXRevocationChecker.Option.NO_FALLBACK)) {
                        this.A02.check(certificate);
                        return;
                    }
                    throw e2;
                }
            }
        }
    }

    @Override // java.security.cert.PKIXRevocationChecker
    public List getSoftFailExceptions() {
        return null;
    }

    @Override // java.security.cert.PKIXCertPathChecker, java.security.cert.CertPathChecker
    public void init(boolean z) {
        this.A00 = null;
        AnonymousClass5GJ r1 = this.A02;
        if (!z) {
            r1.A01 = null;
            r1.A00 = new Date();
            AnonymousClass5GK r12 = this.A03;
            r12.A01 = null;
            r12.A02 = C94664cJ.A01("ocsp.enable");
            r12.A00 = C94664cJ.A00("ocsp.responderURL");
            return;
        }
        throw new CertPathValidatorException("forward checking not supported");
    }
}
