package X;

/* renamed from: X.3iG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74533iG extends AnonymousClass0FH {
    public final /* synthetic */ C69233Yl A00;

    public C74533iG(C69233Yl r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass0FH, X.AbstractC008704k
    public boolean A0F(AnonymousClass03U r2, int i, int i2, int i3, int i4) {
        if (!(r2 instanceof C55152hs) || ((C55152hs) r2).A00) {
            return super.A0F(r2, i, i2, i3, i4);
        }
        return false;
    }
}
