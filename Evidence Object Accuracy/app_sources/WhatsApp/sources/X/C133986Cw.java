package X;

/* renamed from: X.6Cw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133986Cw implements AbstractC136366Mg {
    public final /* synthetic */ AnonymousClass6CP A00;

    public C133986Cw(AnonymousClass6CP r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC136366Mg
    public boolean A6x() {
        return C12960it.A1W(this.A00.A04.A0Q);
    }

    @Override // X.AbstractC136366Mg
    public void ATU(String str) {
        this.A00.AXn(str);
    }
}
