package X;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.util.AttributeSet;
import android.view.InflateException;
import android.view.animation.Interpolator;
import org.xmlpull.v1.XmlPullParser;

/* renamed from: X.0Wp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class animation.InterpolatorC07080Wp implements Interpolator {
    public float[] A00;
    public float[] A01;

    public animation.InterpolatorC07080Wp(Context context, AttributeSet attributeSet, XmlPullParser xmlPullParser) {
        float f;
        float f2;
        float f3;
        float f4;
        String string;
        TypedArray A01 = AnonymousClass06r.A01(context.getTheme(), context.getResources(), attributeSet, AnonymousClass06q.A05);
        if (AnonymousClass06r.A02("pathData", xmlPullParser)) {
            if (!AnonymousClass06r.A02("pathData", xmlPullParser)) {
                string = null;
            } else {
                string = A01.getString(4);
            }
            Path A00 = C014306u.A00(string);
            if (A00 != null) {
                A00(A00);
            } else {
                StringBuilder sb = new StringBuilder("The path is null, which is created from ");
                sb.append(string);
                throw new InflateException(sb.toString());
            }
        } else if (!AnonymousClass06r.A02("controlX1", xmlPullParser)) {
            throw new InflateException("pathInterpolator requires the controlX1 attribute");
        } else if (AnonymousClass06r.A02("controlY1", xmlPullParser)) {
            if (!AnonymousClass06r.A02("controlX1", xmlPullParser)) {
                f = 0.0f;
            } else {
                f = A01.getFloat(0, 0.0f);
            }
            if (!AnonymousClass06r.A02("controlY1", xmlPullParser)) {
                f2 = 0.0f;
            } else {
                f2 = A01.getFloat(1, 0.0f);
            }
            boolean A02 = AnonymousClass06r.A02("controlX2", xmlPullParser);
            if (A02 != AnonymousClass06r.A02("controlY2", xmlPullParser)) {
                throw new InflateException("pathInterpolator requires both controlX2 and controlY2 for cubic Beziers.");
            } else if (!A02) {
                Path path = new Path();
                path.moveTo(0.0f, 0.0f);
                path.quadTo(f, f2, 1.0f, 1.0f);
                A00(path);
            } else {
                if (!AnonymousClass06r.A02("controlX2", xmlPullParser)) {
                    f3 = 0.0f;
                } else {
                    f3 = A01.getFloat(2, 0.0f);
                }
                if (!AnonymousClass06r.A02("controlY2", xmlPullParser)) {
                    f4 = 0.0f;
                } else {
                    f4 = A01.getFloat(3, 0.0f);
                }
                Path path2 = new Path();
                path2.moveTo(0.0f, 0.0f);
                path2.cubicTo(f, f2, f3, f4, 1.0f, 1.0f);
                A00(path2);
            }
        } else {
            throw new InflateException("pathInterpolator requires the controlY1 attribute");
        }
        A01.recycle();
    }

    public final void A00(Path path) {
        int i;
        float[] fArr;
        float[] fArr2;
        int i2 = 0;
        PathMeasure pathMeasure = new PathMeasure(path, false);
        float length = pathMeasure.getLength();
        int min = Math.min(3000, ((int) (length / 0.002f)) + 1);
        if (min > 0) {
            this.A00 = new float[min];
            this.A01 = new float[min];
            float[] fArr3 = new float[2];
            int i3 = 0;
            do {
                i = min - 1;
                pathMeasure.getPosTan((((float) i3) * length) / ((float) i), fArr3, null);
                fArr = this.A00;
                fArr[i3] = fArr3[0];
                fArr2 = this.A01;
                fArr2[i3] = fArr3[1];
                i3++;
            } while (i3 < min);
            float f = fArr[0];
            if (((double) Math.abs(f)) > 1.0E-5d || ((double) Math.abs(fArr2[0])) > 1.0E-5d || ((double) Math.abs(fArr[i] - 1.0f)) > 1.0E-5d || ((double) Math.abs(fArr2[i] - 1.0f)) > 1.0E-5d) {
                StringBuilder sb = new StringBuilder("The Path must start at (0,0) and end at (1,1) start: ");
                sb.append(f);
                sb.append(",");
                sb.append(this.A01[0]);
                sb.append(" end:");
                int i4 = min - 1;
                sb.append(this.A00[i4]);
                sb.append(",");
                sb.append(this.A01[i4]);
                throw new IllegalArgumentException(sb.toString());
            }
            float f2 = 0.0f;
            int i5 = 0;
            do {
                int i6 = i5 + 1;
                float f3 = fArr[i5];
                if (f3 >= f2) {
                    fArr[i2] = f3;
                    i2++;
                    f2 = f3;
                    i5 = i6;
                } else {
                    StringBuilder sb2 = new StringBuilder("The Path cannot loop back on itself, x :");
                    sb2.append(f3);
                    throw new IllegalArgumentException(sb2.toString());
                }
            } while (i2 < min);
            if (pathMeasure.nextContour()) {
                throw new IllegalArgumentException("The Path should be continuous, can't have 2+ contours");
            }
            return;
        }
        StringBuilder sb3 = new StringBuilder("The Path has a invalid length ");
        sb3.append(length);
        throw new IllegalArgumentException(sb3.toString());
    }

    @Override // android.animation.TimeInterpolator
    public float getInterpolation(float f) {
        if (f <= 0.0f) {
            return 0.0f;
        }
        if (f >= 1.0f) {
            return 1.0f;
        }
        int i = 0;
        float[] fArr = this.A00;
        int length = fArr.length - 1;
        while (length - i > 1) {
            int i2 = (i + length) >> 1;
            if (f < fArr[i2]) {
                length = i2;
            } else {
                i = i2;
            }
        }
        float f2 = fArr[length];
        float f3 = fArr[i];
        float f4 = f2 - f3;
        if (f4 == 0.0f) {
            return this.A01[i];
        }
        float[] fArr2 = this.A01;
        float f5 = fArr2[i];
        return f5 + (((f - f3) / f4) * (fArr2[length] - f5));
    }
}
