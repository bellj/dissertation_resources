package X;

/* renamed from: X.5yV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC129915yV {
    public String A00;
    public String A01;
    public String A02;
    public boolean A03;
    public boolean A04;

    public AbstractC129915yV(String str, String str2, String str3, boolean z, boolean z2) {
        this.A04 = z;
        this.A03 = z2;
        this.A01 = str;
        this.A00 = str2;
        this.A02 = str3;
    }

    /* JADX WARNING: Removed duplicated region for block: B:41:0x00ba  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.Map A00(java.lang.String r9, int r10) {
        /*
            r8 = this;
            boolean r0 = r8 instanceof X.C121275ha
            if (r0 != 0) goto L_0x0009
            java.util.Map r0 = X.C130315zD.A01(r8)
            return r0
        L_0x0009:
            r7 = r8
            X.5ha r7 = (X.C121275ha) r7
            int r0 = r9.hashCode()
            switch(r0) {
                case 2388619: goto L_0x00b2;
                case 818562265: goto L_0x00af;
                case 972473721: goto L_0x001c;
                case 1857560300: goto L_0x0018;
                default: goto L_0x0013;
            }
        L_0x0013:
            java.util.HashMap r5 = X.C12970iu.A11()
            return r5
        L_0x0018:
            java.lang.String r0 = "NATIONALITY_AND_DOB"
            goto L_0x00b4
        L_0x001c:
            java.lang.String r0 = "RESIDENTIAL_ADDRESS"
            boolean r0 = r9.equals(r0)
            if (r0 == 0) goto L_0x0013
            java.util.HashMap r5 = X.C12970iu.A11()
            java.lang.String r1 = r7.A00
            java.lang.String r4 = ""
            if (r1 == 0) goto L_0x0052
            java.lang.String r0 = "KycTextQuestion"
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x00ac
            java.lang.String r0 = "KycDropdownQuestion"
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x00a9
            r2 = 0
        L_0x003f:
            java.lang.StringBuilder r1 = X.C12960it.A0h()
            r1.append(r10)
            java.lang.String r0 = "-format"
            java.lang.String r0 = X.C12960it.A0d(r0, r1)
            if (r2 != 0) goto L_0x004f
            r2 = r4
        L_0x004f:
            r5.put(r0, r2)
        L_0x0052:
            java.lang.StringBuilder r1 = X.C12960it.A0h()
            r1.append(r10)
            java.lang.String r0 = "-title"
            java.lang.String r1 = X.C12960it.A0d(r0, r1)
            java.lang.String r0 = r7.A01
            r5.put(r1, r0)
            java.lang.StringBuilder r1 = X.C12960it.A0h()
            r1.append(r10)
            java.lang.String r0 = "-editable"
            java.lang.String r6 = X.C12960it.A0d(r0, r1)
            boolean r1 = r7.A03
            java.lang.String r3 = "1"
            java.lang.String r2 = "0"
            r0 = r2
            if (r1 == 0) goto L_0x007b
            r0 = r3
        L_0x007b:
            r5.put(r6, r0)
            java.lang.StringBuilder r1 = X.C12960it.A0h()
            r1.append(r10)
            java.lang.String r0 = "-optional"
            java.lang.String r1 = X.C12960it.A0d(r0, r1)
            boolean r0 = r7.A04
            if (r0 != 0) goto L_0x0090
            r3 = r2
        L_0x0090:
            r5.put(r1, r3)
            java.lang.String r2 = r7.A00
            java.lang.StringBuilder r1 = X.C12960it.A0h()
            r1.append(r10)
            java.lang.String r0 = "-value"
            java.lang.String r0 = X.C12960it.A0d(r0, r1)
            if (r2 == 0) goto L_0x00a5
            r4 = r2
        L_0x00a5:
            r5.put(r0, r4)
            return r5
        L_0x00a9:
            java.lang.String r2 = "dropdown"
            goto L_0x003f
        L_0x00ac:
            java.lang.String r2 = "text"
            goto L_0x003f
        L_0x00af:
            java.lang.String r0 = "NATIONALITY_AND_BIRTH_INFO"
            goto L_0x00b4
        L_0x00b2:
            java.lang.String r0 = "NAME"
        L_0x00b4:
            boolean r0 = r9.equals(r0)
            if (r0 == 0) goto L_0x0013
            java.util.Map r5 = X.C130315zD.A01(r7)
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC129915yV.A00(java.lang.String, int):java.util.Map");
    }
}
