package X;

/* renamed from: X.5Xi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public interface AbstractC116895Xi {
    public static final AnonymousClass1TK A00;
    public static final AnonymousClass1TK A01;
    public static final AnonymousClass1TK A02;
    public static final AnonymousClass1TK A03;
    public static final AnonymousClass1TK A04;

    static {
        AnonymousClass1TK A12 = C72453ed.A12("1.3.101");
        A04 = A12;
        A02 = AnonymousClass1TL.A02("110", A12).A0B();
        A03 = AnonymousClass1TL.A02("111", A12).A0B();
        A00 = AnonymousClass1TL.A02("112", A12).A0B();
        A01 = AnonymousClass1TL.A02("113", A12).A0B();
    }
}
