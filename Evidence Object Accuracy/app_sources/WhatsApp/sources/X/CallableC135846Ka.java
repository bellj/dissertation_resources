package X;

import android.hardware.camera2.CameraDevice;
import java.util.List;
import java.util.concurrent.Callable;

/* renamed from: X.6Ka  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class CallableC135846Ka implements Callable {
    public final /* synthetic */ AnonymousClass61Q A00;
    public final /* synthetic */ List A01;

    public CallableC135846Ka(AnonymousClass61Q r1, List list) {
        this.A00 = r1;
        this.A01 = list;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        AnonymousClass61Q r3 = this.A00;
        C04160Kp.A00(r3.A01, "CameraDevice should not be null for createCaptureSession!");
        C04160Kp.A00(r3.A0A, "mPreviewSetupDelegate should not be null for createCaptureSession!");
        CameraDevice cameraDevice = r3.A01;
        List list = this.A01;
        AnonymousClass66N r0 = r3.A0K;
        C1310360y.A01(cameraDevice, r0, list);
        return r0;
    }
}
