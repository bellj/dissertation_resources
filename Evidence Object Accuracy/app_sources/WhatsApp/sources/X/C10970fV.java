package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0fV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C10970fV extends C10980fW {
    public static final List A00(Object[] objArr) {
        int length = objArr.length - 2;
        if (length < 0) {
            length = 0;
        }
        return A03(objArr, length);
    }

    public static final List A01(Object[] objArr) {
        int length = objArr.length;
        if (length == 0) {
            return C16770pj.A0G();
        }
        if (length != 1) {
            return A02(objArr);
        }
        return C16780pk.A0K(objArr[0]);
    }

    public static final List A02(Object[] objArr) {
        return new ArrayList(C16770pj.A0F(objArr));
    }

    public static final List A03(Object[] objArr, int i) {
        if (i < 0) {
            StringBuilder sb = new StringBuilder("Requested element count ");
            sb.append(i);
            sb.append(" is less than zero.");
            throw new IllegalArgumentException(sb.toString());
        } else if (i == 0) {
            return C16770pj.A0G();
        } else {
            int length = objArr.length;
            if (i >= length) {
                return A01(objArr);
            }
            if (i == 1) {
                return C16780pk.A0K(objArr[length - 1]);
            }
            ArrayList arrayList = new ArrayList(i);
            for (int i2 = length - i; i2 < length; i2++) {
                arrayList.add(objArr[i2]);
            }
            return arrayList;
        }
    }
}
