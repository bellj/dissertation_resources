package X;

import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1xr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C43931xr {
    public static C43891xl A00(InputStream inputStream, int i) {
        C43911xp r3;
        try {
            JSONObject A02 = AnonymousClass1P1.A02(inputStream);
            AnonymousClass009.A05(A02);
            int i2 = A02.getInt("policyVersion");
            JSONObject optJSONObject = A02.optJSONObject("banner");
            if (optJSONObject == null) {
                r3 = null;
            } else {
                String string = optJSONObject.getString("text");
                String string2 = optJSONObject.getString("iconDescription");
                String string3 = optJSONObject.getString("action");
                JSONObject jSONObject = optJSONObject.getJSONObject("icon");
                r3 = new C43911xp(A02(optJSONObject.getJSONObject("timing")), string, jSONObject.getString("light"), jSONObject.getString("dark"), string2, string3);
            }
            return new C43891xl(r3, A03(A02.optJSONObject("modal"), true), A03(A02.optJSONObject("blocking-modal"), false), i, i2);
        } catch (IOException | JSONException e) {
            StringBuilder sb = new StringBuilder("Failed to parse user notice content for notice id: ");
            sb.append(i);
            Log.e(sb.toString(), e);
            return null;
        }
    }

    public static C43871xj A01(JSONObject jSONObject) {
        TimeZone timeZone;
        if (jSONObject == null) {
            return null;
        }
        String string = jSONObject.getString("time");
        String string2 = jSONObject.getString("reference");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
        if ("utc".equalsIgnoreCase(string2)) {
            timeZone = TimeZone.getTimeZone("UTC");
        } else {
            timeZone = TimeZone.getDefault();
        }
        try {
            simpleDateFormat.setTimeZone(timeZone);
            Date parse = simpleDateFormat.parse(string);
            AnonymousClass009.A05(parse);
            return new C43871xj(parse.getTime());
        } catch (ParseException e) {
            StringBuilder sb = new StringBuilder("UserNoticeTiming/getDate/Unable to parse date: ");
            sb.append(string);
            sb.append(" reference: ");
            sb.append(string2);
            Log.e(sb.toString());
            throw new RuntimeException(e);
        }
    }

    public static C43881xk A02(JSONObject jSONObject) {
        C43871xj A01 = A01(jSONObject.optJSONObject("start"));
        JSONObject optJSONObject = jSONObject.optJSONObject("duration");
        C43861xi r9 = null;
        long[] jArr = null;
        if (optJSONObject != null) {
            int optInt = optJSONObject.optInt("static", -1);
            long j = -1;
            if (optInt != -1) {
                j = ((long) optInt) * 3600000;
            }
            JSONArray optJSONArray = optJSONObject.optJSONArray("repeat");
            if (optJSONArray != null) {
                jArr = new long[optJSONArray.length()];
                for (int i = 0; i < optJSONArray.length(); i++) {
                    jArr[i] = ((long) optJSONArray.getInt(i)) * 3600000;
                }
            }
            r9 = new C43861xi(jArr, j);
        }
        return new C43881xk(r9, A01, A01(jSONObject.optJSONObject("end")));
    }

    public static C43841xg A03(JSONObject jSONObject, boolean z) {
        String str;
        String str2;
        String str3 = null;
        if (jSONObject == null) {
            return null;
        }
        String string = jSONObject.getString("title");
        String string2 = jSONObject.getString("iconDescription");
        String string3 = jSONObject.getString("buttonText");
        JSONObject jSONObject2 = jSONObject.getJSONObject("icon");
        String string4 = jSONObject2.getString("light");
        String string5 = jSONObject2.getString("dark");
        C43881xk A02 = A02(jSONObject.getJSONObject("timing"));
        String str4 = null;
        String str5 = null;
        ArrayList arrayList = new ArrayList();
        JSONArray jSONArray = jSONObject.getJSONArray("bullets");
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject jSONObject3 = jSONArray.getJSONObject(i);
            String string6 = jSONObject3.getString("text");
            JSONObject optJSONObject = jSONObject3.optJSONObject("icon");
            if (optJSONObject != null) {
                str2 = optJSONObject.getString("light");
                str = optJSONObject.getString("dark");
            } else {
                str = null;
                str2 = null;
            }
            arrayList.add(new C92274Vf(string6, str2, str));
        }
        String optString = jSONObject.optString("body");
        if (!TextUtils.isEmpty(optString)) {
            str3 = optString;
        }
        String optString2 = jSONObject.optString("footer");
        if (!TextUtils.isEmpty(optString2)) {
            str4 = optString2;
        }
        if (z) {
            str5 = jSONObject.getString("dismissText");
        }
        return new C43841xg(A02, string4, string5, string2, string, string3, str3, str4, str5, arrayList);
    }
}
