package X;

import android.content.Context;
import android.os.Parcel;

/* renamed from: X.2kf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56302kf extends AnonymousClass2RP implements AnonymousClass5Yj {
    public static final AbstractC77683ng A00;
    public static final AnonymousClass4DN A01;
    public static final AnonymousClass1UE A02;

    static {
        AnonymousClass4DN r3 = new AnonymousClass4DN();
        A01 = r3;
        C77613nZ r2 = new C77613nZ();
        A00 = r2;
        A02 = new AnonymousClass1UE(r2, r3, "ClientTelemetry.API");
    }

    public C56302kf(Context context, C108174yc r8) {
        super(null, context, r8, A02, C93404a7.A02);
    }

    @Override // X.AnonymousClass5Yj
    public final C13600jz AKY(C56402kp r5) {
        AnonymousClass3H0 r3 = new AnonymousClass3H0(null);
        r3.A03 = new C78603pB[]{C88734Gx.A00};
        r3.A02 = false;
        r3.A01 = new AnonymousClass5SY() { // from class: X.3T0
            /* JADX INFO: finally extract failed */
            @Override // X.AnonymousClass5SY
            public final void A5Y(Object obj, Object obj2) {
                C56402kp r4 = C56402kp.this;
                C13690kA r7 = (C13690kA) obj2;
                C98384ib r2 = (C98384ib) ((AbstractC95064d1) obj).A03();
                Parcel obtain = Parcel.obtain();
                obtain.writeInterfaceToken(r2.A01);
                if (r4 == null) {
                    obtain.writeInt(0);
                } else {
                    obtain.writeInt(1);
                    r4.writeToParcel(obtain, 0);
                }
                try {
                    r2.A00.transact(1, obtain, null, 1);
                    obtain.recycle();
                    r7.A01(null);
                } catch (Throwable th) {
                    obtain.recycle();
                    throw th;
                }
            }
        };
        return A01(r3.A00(), 2);
    }
}
