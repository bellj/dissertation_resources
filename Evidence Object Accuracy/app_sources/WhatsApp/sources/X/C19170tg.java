package X;

import android.database.Cursor;
import android.database.SQLException;
import android.text.TextUtils;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* renamed from: X.0tg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19170tg extends AbstractC18500sY {
    public final C15570nT A00;
    public final C19990v2 A01;
    public final C18680sq A02;

    public C19170tg(C15570nT r3, C19990v2 r4, C18680sq r5, C18480sW r6) {
        super(r6, "participant_user", Integer.MIN_VALUE);
        this.A00 = r3;
        this.A01 = r4;
        this.A02 = r5;
    }

    @Override // X.AbstractC18500sY
    public long A06() {
        return super.A06();
    }

    @Override // X.AbstractC18500sY
    public AnonymousClass2Ez A09(Cursor cursor) {
        StringBuilder sb;
        String str;
        UserJid nullable;
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow("_id");
        int columnIndexOrThrow2 = cursor.getColumnIndexOrThrow("gjid");
        int columnIndexOrThrow3 = cursor.getColumnIndexOrThrow("jid");
        int columnIndexOrThrow4 = cursor.getColumnIndexOrThrow("admin");
        int columnIndexOrThrow5 = cursor.getColumnIndexOrThrow("pending");
        int columnIndexOrThrow6 = cursor.getColumnIndexOrThrow("sent_sender_key");
        long j = -1;
        int i = 0;
        while (cursor.moveToNext()) {
            j = cursor.getLong(columnIndexOrThrow);
            i++;
            int i2 = cursor.getInt(columnIndexOrThrow4);
            boolean z = true;
            boolean z2 = false;
            if (cursor.getInt(columnIndexOrThrow5) != 0) {
                z2 = true;
            }
            if (cursor.getInt(columnIndexOrThrow6) == 0) {
                z = false;
            }
            String A02 = AnonymousClass1Tx.A02(cursor, columnIndexOrThrow2);
            AbstractC15590nW A05 = AbstractC15590nW.A05(A02);
            if (A05 == null) {
                sb = new StringBuilder();
                sb.append("participant-user-db-migration/process-batch: groupJid=");
                sb.append(A02);
                sb.append(", rowId=");
                sb.append(j);
                str = " SKIP Due to invalid MultipleParticipantJid.";
            } else if (!z2 || this.A01.A06(A05) != null) {
                String A022 = AnonymousClass1Tx.A02(cursor, columnIndexOrThrow3);
                if (TextUtils.isEmpty(A022)) {
                    C15570nT r0 = this.A00;
                    r0.A08();
                    nullable = r0.A05;
                } else {
                    nullable = UserJid.getNullable(A022);
                }
                if (nullable == null) {
                    sb = new StringBuilder("participant-user-db-migration/process-batch: participantUserJid=");
                    sb.append(A022);
                    sb.append(", rowId=");
                    sb.append(j);
                    str = " SKIP Due to invalid UserJid.";
                } else {
                    this.A02.A04(new AnonymousClass1YO(nullable, i2, z2, z), A05);
                }
            } else {
                sb = new StringBuilder();
                sb.append("participant-user-db-migration/process-batch: groupJid=");
                sb.append(A02);
                sb.append(", rowId=");
                sb.append(j);
                str = " SKIP Due to pending group which no longer exists.";
            }
            sb.append(str);
            Log.w(sb.toString());
        }
        return new AnonymousClass2Ez(j, i);
    }

    @Override // X.AbstractC18500sY
    public void A0H() {
        super.A0H();
        this.A06.A04("participant_user_ready", 2);
    }

    @Override // X.AbstractC18500sY
    public void A0J() {
        if (this.A06.A00("participant_user_ready", 0) == 1) {
            C18680sq r6 = this.A02;
            Log.i("ParticipantUserStore/resetMigration");
            try {
                C16310on A02 = r6.A08.A02();
                AnonymousClass1Lx A00 = A02.A00();
                C16330op r2 = A02.A03;
                r2.A01("group_participant_user", null, null);
                r2.A01("group_participant_device", null, null);
                C21390xL r1 = r6.A0A;
                r1.A03("participant_user_ready");
                r1.A03("migration_participant_user_index");
                r1.A03("migration_participant_user_retry");
                r1.A03("broadcast_me_jid_ready");
                r1.A03("migration_broadcast_me_jid_index");
                r1.A03("migration_broadcast_me_jid_retry");
                r6.A0D.A01(false);
                A00.A00();
                Log.i("ParticipantUserStore/resetMigration success");
                A00.close();
                A02.close();
            } catch (SQLException e) {
                Log.e("ParticipantUserStore/resetMigration failed", e);
            }
        }
    }
}
