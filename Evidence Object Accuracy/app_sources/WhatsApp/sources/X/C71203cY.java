package X;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.3cY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C71203cY implements Iterable {
    public static final C71203cY A01 = new C71203cY(Collections.emptyList());
    public final List A00;

    public C71203cY(List list) {
        this.A00 = list;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || C71203cY.class != obj.getClass()) {
            return false;
        }
        return this.A00.equals(((C71203cY) obj).A00);
    }

    @Override // java.lang.Object
    public int hashCode() {
        return this.A00.hashCode();
    }

    @Override // java.lang.Iterable
    public Iterator iterator() {
        return new AnonymousClass5D6(this.A00.iterator());
    }

    @Override // java.lang.Object
    public String toString() {
        return this.A00.toString();
    }
}
