package X;

import com.facebook.redex.RunnableBRunnable0Shape1S0101000_I1;
import com.facebook.redex.RunnableBRunnable0Shape1S0400000_I1;

/* renamed from: X.3VG  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3VG implements AbstractC116505Vs {
    public final /* synthetic */ AnonymousClass28G A00;
    public final /* synthetic */ AnonymousClass1J7 A01;

    public AnonymousClass3VG(AnonymousClass28G r1, AnonymousClass1J7 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC116505Vs
    public void AQ9(AnonymousClass28H r5, int i) {
        AnonymousClass28G r3 = this.A00;
        r3.A00.A0H(new RunnableBRunnable0Shape1S0101000_I1(this.A01, i, 4));
    }

    @Override // X.AbstractC116505Vs
    public void AXA(C44721zR r8, AnonymousClass28H r9) {
        AnonymousClass28G r2 = this.A00;
        r2.A00.A0H(new RunnableBRunnable0Shape1S0400000_I1(r2, r9, this.A01, r8, 0));
    }
}
