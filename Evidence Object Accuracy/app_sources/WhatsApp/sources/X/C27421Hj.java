package X;

import android.os.Environment;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

/* renamed from: X.1Hj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C27421Hj {
    public File A00;
    public final C15890o4 A01;
    public final C14950mJ A02;
    public final File A03;

    public C27421Hj(C15890o4 r1, C14950mJ r2, File file) {
        this.A03 = file;
        this.A02 = r2;
        this.A01 = r1;
    }

    public File A00(String str) {
        File file;
        String obj;
        int i;
        File file2;
        String obj2;
        int i2;
        synchronized (this) {
            A01();
            File file3 = this.A00;
            if (file3 == null || !file3.exists()) {
                int i3 = 0;
                File file4 = this.A03;
                File file5 = new File(file4, UUID.randomUUID().toString());
                this.A00 = file5;
                while (true) {
                    i2 = i3 + 1;
                    if (i3 >= 10 || file5.mkdir()) {
                        break;
                    }
                    file5 = new File(file4, UUID.randomUUID().toString());
                    this.A00 = file5;
                    i3 = i2;
                }
                if (i2 > 10) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("trash/createtempdir/failed ");
                    sb.append(this.A00.toString());
                    Log.w(sb.toString());
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("trash/createtempdir/failed total-storage:");
                    C14950mJ r3 = this.A02;
                    sb2.append(r3.A03());
                    sb2.append(" free-storage:");
                    sb2.append(r3.A01());
                    Log.w(sb2.toString());
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("trash/createtempdir/failed external-storage-state:");
                    sb3.append(Environment.getExternalStorageState());
                    Log.w(sb3.toString());
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append("trash/createtempdir/failed base-dir:");
                    sb4.append(file4);
                    sb4.append(" exists:");
                    sb4.append(file4.exists());
                    sb4.append(" writable:");
                    sb4.append(file4.canWrite());
                    sb4.append(" directory:");
                    sb4.append(file4.isDirectory());
                    Log.w(sb4.toString());
                    try {
                        File canonicalFile = file4.getCanonicalFile();
                        StringBuilder sb5 = new StringBuilder();
                        sb5.append("trash/createtempdir/failed canonical-base-dir:");
                        sb5.append(canonicalFile);
                        sb5.append(" exists:");
                        sb5.append(canonicalFile.exists());
                        sb5.append(" writable:");
                        sb5.append(canonicalFile.canWrite());
                        Log.w(sb5.toString());
                        StringBuilder sb6 = new StringBuilder();
                        sb6.append("trash/createtempdir/failed StoragePermission?:");
                        sb6.append(this.A01.A0A(Environment.getExternalStorageState()));
                        Log.w(sb6.toString());
                    } catch (IOException e) {
                        Log.w("trash/createtempdir/failed unable to resolve trashDir", e);
                    }
                    throw new IOException("max retries reached while creating temp dir");
                }
            }
        }
        synchronized (this) {
            file = this.A00;
        }
        StringBuilder sb7 = new StringBuilder();
        sb7.append(UUID.randomUUID().toString());
        if (TextUtils.isEmpty(str)) {
            obj = "";
        } else {
            StringBuilder sb8 = new StringBuilder();
            sb8.append(".");
            sb8.append(str);
            obj = sb8.toString();
        }
        sb7.append(obj);
        File file6 = new File(file, sb7.toString());
        int i4 = 0;
        while (true) {
            i = i4 + 1;
            if (i4 >= 10 || file6.createNewFile()) {
                break;
            }
            synchronized (this) {
                file2 = this.A00;
            }
            StringBuilder sb9 = new StringBuilder();
            sb9.append(UUID.randomUUID().toString());
            if (TextUtils.isEmpty(str)) {
                obj2 = "";
            } else {
                StringBuilder sb10 = new StringBuilder();
                sb10.append(".");
                sb10.append(str);
                obj2 = sb10.toString();
            }
            sb9.append(obj2);
            file6 = new File(file2, sb9.toString());
            i4 = i;
        }
        if (i <= 10) {
            return file6;
        }
        throw new IOException("max retries reached while creating temp file");
    }

    public final void A01() {
        File file = this.A03;
        if (file.exists() && !file.isDirectory()) {
            StringBuilder sb = new StringBuilder("trash/create-trash-dir/removing ");
            sb.append(file);
            Log.w(sb.toString());
            if (!file.delete() && file.exists()) {
                StringBuilder sb2 = new StringBuilder("trash/create-trash-dir/failed ");
                sb2.append(file);
                sb2.append(" is not a directory");
                Log.e(sb2.toString());
            }
        }
        if (!file.exists()) {
            file.mkdirs();
            if (!file.exists() && !file.mkdir()) {
                Log.w("trash/create-trash-dir/failed");
            }
        }
    }
}
