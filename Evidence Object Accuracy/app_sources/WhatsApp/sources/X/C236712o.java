package X;

import android.view.View;
import com.facebook.redex.RunnableBRunnable0Shape0S0300000_I0;
import com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3;
import com.facebook.redex.RunnableBRunnable0Shape3S0200000_I0_3;
import com.facebook.redex.RunnableBRunnable0Shape7S0100000_I0_7;
import com.whatsapp.Conversation;
import com.whatsapp.HomeActivity;
import com.whatsapp.util.Log;

/* renamed from: X.12o  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C236712o {
    public void A00() {
        AnonymousClass11P r1;
        View view;
        if (!(this instanceof C40701s7)) {
            if (this instanceof AnonymousClass1s5) {
                HomeActivity homeActivity = ((AnonymousClass1s5) this).A00;
                if (((ActivityC13810kN) homeActivity).A0C.A07(931)) {
                    r1 = homeActivity.A0n;
                    view = homeActivity.A0C;
                } else {
                    return;
                }
            } else if (this instanceof AnonymousClass1s1) {
                Conversation conversation = ((AnonymousClass1s1) this).A00;
                if (((ActivityC13810kN) conversation).A0C.A07(931)) {
                    r1 = ((AbstractActivityC13750kH) conversation).A0I;
                    view = ((ActivityC13810kN) conversation).A00;
                } else {
                    return;
                }
            } else {
                return;
            }
            C40691s6.A03(view, r1);
            return;
        }
        Log.i("CameraUi/onShowingIncomingCallUI");
        AnonymousClass1s8 r6 = ((C40701s7) this).A00;
        AnonymousClass1s9 r0 = r6.A0A;
        if (r0 != null && r0.AJy()) {
            r6.A0W = false;
            boolean z = false;
            if (System.currentTimeMillis() - r6.A0F.A00 > 1000) {
                z = true;
            }
            r6.A0O(z);
        }
    }

    public void A01(AnonymousClass1YT r6) {
        Object obj;
        if (this instanceof AnonymousClass1GM) {
            C22310ys r3 = ((AnonymousClass1GM) this).A00;
            r3.A08.Ab2(new RunnableBRunnable0Shape7S0100000_I0_7(r3, 47));
        } else if (this instanceof C40681rz) {
            AnonymousClass1s3 r0 = ((C40681rz) this).A00.A02;
            if (r0 != null) {
                r0.A00.A02();
            }
        } else if (this instanceof C236612n) {
            C236612n r32 = (C236612n) this;
            r32.A02.Ab2(new RunnableBRunnable0Shape3S0200000_I0_3(r32, 35, r6));
        } else if (this instanceof C40551rm) {
            C20020v5 r4 = ((C40551rm) this).A00;
            C14850m9 r1 = r4.A0M;
            if (r1.A07(1022)) {
                if (!r6.A0B()) {
                    obj = r6.A03().A01;
                } else if (r1.A07(1251)) {
                    obj = r6.A04;
                } else {
                    return;
                }
                if (obj != null) {
                    r4.A0R.execute(new RunnableBRunnable0Shape0S0300000_I0(r4, obj, r6, 24));
                }
            }
        } else if (this instanceof AnonymousClass1s0) {
            ((AnonymousClass1s0) this).A00.setVisibility(8);
        } else if (this instanceof AnonymousClass1s1) {
            ((AnonymousClass1s1) this).A00.invalidateOptionsMenu();
        }
    }

    public void A02(AnonymousClass1YT r6) {
        AnonymousClass11P r1;
        View view;
        if (this instanceof C40681rz) {
            AnonymousClass1s3 r0 = ((C40681rz) this).A00.A02;
            if (r0 != null) {
                r0.A00.A02();
            }
        } else if (this instanceof C236612n) {
            C236612n r3 = (C236612n) this;
            r3.A02.Ab2(new RunnableBRunnable0Shape3S0200000_I0_3(r3, 34, r6));
        } else if (!(this instanceof AnonymousClass1s0)) {
            if (this instanceof AnonymousClass1s5) {
                HomeActivity homeActivity = ((AnonymousClass1s5) this).A00;
                homeActivity.A0n.A04();
                if (((ActivityC13810kN) homeActivity).A0C.A07(931)) {
                    r1 = homeActivity.A0n;
                    view = homeActivity.A0C;
                } else {
                    return;
                }
            } else if (this instanceof AnonymousClass1s1) {
                Conversation conversation = ((AnonymousClass1s1) this).A00;
                ((AbstractActivityC13750kH) conversation).A0I.A04();
                conversation.invalidateOptionsMenu();
                AbstractC35401hl r12 = conversation.A3t;
                if (r12 != null && r12.ADU()) {
                    r12.AYz();
                }
                if (((ActivityC13810kN) conversation).A0C.A07(931)) {
                    r1 = ((AbstractActivityC13750kH) conversation).A0I;
                    view = ((ActivityC13810kN) conversation).A00;
                } else {
                    return;
                }
            } else {
                return;
            }
            C40691s6.A03(view, r1);
        } else {
            AnonymousClass1s0 r4 = (AnonymousClass1s0) this;
            C36821kh r32 = r4.A00;
            boolean z = false;
            if (r6.A0F != null) {
                z = true;
            }
            r32.A0F = z;
            r32.A03();
            r32.postDelayed(new RunnableBRunnable0Shape3S0100000_I0_3(r4, 28), 2000);
        }
    }

    public void A03(AnonymousClass1YT r6, boolean z) {
        if (this instanceof AnonymousClass1GN) {
            C20230vQ r4 = ((AnonymousClass1GN) this).A00;
            r4.A03();
            if (r4.A00.isEmpty()) {
                C14820m6 r0 = r4.A09;
                r0.A00.edit().putLong("first_missed_call", r6.A09).apply();
            }
            synchronized (r4) {
                r4.A00.add(r6);
            }
            r4.A04(z);
            r4.A0C.A05();
        }
    }
}
