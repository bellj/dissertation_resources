package X;

import android.text.TextUtils;
import java.lang.ref.Reference;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1A8  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1A8 {
    public HashSet A00 = new HashSet();
    public HashSet A01 = new HashSet();
    public List A02 = new ArrayList();
    public final C14900mE A03;
    public final C15570nT A04;
    public final C17070qD A05;
    public final AnonymousClass1A7 A06;

    public AnonymousClass1A8(C14900mE r2, C15570nT r3, C17070qD r4, AnonymousClass1A7 r5) {
        this.A03 = r2;
        this.A04 = r3;
        this.A05 = r4;
        this.A06 = r5;
    }

    public static /* synthetic */ void A00(AnonymousClass5UV r1, AnonymousClass1A8 r2, String str) {
        r2.A00.add(str);
        r2.A01.remove(str);
        if (r1 != null) {
            r1.AOr(str);
        }
        List<Reference> list = r2.A02;
        int size = list.size();
        while (true) {
            size--;
            if (size < 0) {
                break;
            } else if (((Reference) list.get(size)).get() == null) {
                list.remove(size);
            }
        }
        for (Reference reference : list) {
            AnonymousClass5UV r0 = (AnonymousClass5UV) reference.get();
            if (r0 != null) {
                r0.AOr(str);
            }
        }
    }

    public void A01(AnonymousClass5UV r4) {
        List list = this.A02;
        int size = list.size();
        while (true) {
            size--;
            if (size >= 0) {
                Object obj = ((Reference) list.get(size)).get();
                if (obj == null || obj == r4) {
                    list.remove(size);
                }
            } else {
                return;
            }
        }
    }

    public final void A02(AnonymousClass5UV r6, List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            String str = (String) it.next();
            HashSet hashSet = this.A01;
            if (!hashSet.contains(str) && !this.A00.contains(str)) {
                hashSet.add(str);
                this.A06.A01(new AnonymousClass589(r6, this, str), str, true);
            }
        }
    }

    public final void A03(AnonymousClass1IS r17, String str) {
        if (!TextUtils.isEmpty(str) && r17 != null) {
            AbstractC14640lm r4 = r17.A00;
            if (C15380n4.A0J(r4)) {
                HashSet hashSet = this.A01;
                if (!hashSet.contains(str) && !this.A00.contains(str)) {
                    hashSet.add(str);
                    AnonymousClass1A7 r8 = this.A06;
                    r8.A08.A0F(new C620033l(r8.A02.A00, r8.A00, new AnonymousClass588(this, str), r8.A05, r8, r17), new AnonymousClass1V8("account", new AnonymousClass1W9[]{new AnonymousClass1W9("action", "get-missing-group-transaction-details"), new AnonymousClass1W9("id", str), new AnonymousClass1W9(r4, "group")}), "get", 0);
                }
            }
        }
    }

    public void A04(List list) {
        AbstractC30891Zf r0;
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            AnonymousClass1IR r2 = (AnonymousClass1IR) it.next();
            if ((r2.A03 == 1000 && !TextUtils.isEmpty(r2.A0K)) || ((r0 = r2.A0A) != null && r0.A0X())) {
                arrayList.add(r2.A0K);
            }
        }
        if (!arrayList.isEmpty()) {
            A02(null, arrayList);
        }
    }
}
