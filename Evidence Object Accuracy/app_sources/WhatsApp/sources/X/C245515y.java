package X;

/* renamed from: X.15y  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C245515y {
    public final C22700zV A00;
    public final AnonymousClass10Y A01;
    public final C14850m9 A02;
    public final C16120oU A03;
    public final C23000zz A04;
    public final ExecutorC27271Gr A05;

    public C245515y(C22700zV r3, AnonymousClass10Y r4, C14850m9 r5, C16120oU r6, C23000zz r7, AbstractC14440lR r8) {
        this.A02 = r5;
        this.A03 = r6;
        this.A01 = r4;
        this.A04 = r7;
        this.A00 = r3;
        this.A05 = new ExecutorC27271Gr(r8, false);
    }

    public void A00(int i) {
        AnonymousClass433 r1 = new AnonymousClass433();
        r1.A00 = Integer.valueOf(i);
        this.A03.A07(r1);
    }
}
