package X;

import android.content.Context;
import com.facebook.redex.RunnableBRunnable0Shape0S2310000_I0;
import com.facebook.redex.RunnableBRunnable0Shape9S0100000_I0_9;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;

/* renamed from: X.1IZ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1IZ {
    public String A00;
    public final Context A01;
    public final AbstractC13860kS A02;
    public final C14900mE A03;
    public final C15570nT A04;
    public final AnonymousClass18T A05;
    public final C22710zW A06;
    public final C17070qD A07;
    public final C74503iB A08;
    public final Runnable A09;
    public final Runnable A0A;
    public final boolean A0B;

    public AnonymousClass1IZ(Context context, AbstractC13860kS r2, C14900mE r3, C15570nT r4, AnonymousClass18T r5, C22710zW r6, C17070qD r7, C74503iB r8, Runnable runnable, Runnable runnable2, boolean z) {
        this.A01 = context;
        this.A03 = r3;
        this.A04 = r4;
        this.A02 = r2;
        this.A07 = r7;
        this.A06 = r6;
        this.A08 = r8;
        this.A05 = r5;
        this.A0B = z;
        this.A0A = runnable;
        this.A09 = runnable2;
    }

    public void A00(AbstractC14640lm r20, AbstractC15340mz r21, String str, String str2, int i, boolean z) {
        C14900mE r2;
        Context context;
        int i2;
        if (i == 5) {
            r2 = this.A03;
            context = this.A01;
            i2 = R.string.payments_gating_not_supported_in_group;
        } else if (i == 3) {
            r2 = this.A03;
            context = this.A01;
            i2 = R.string.payments_gating_not_eligible_contact_in_group;
        } else if (i == 1) {
            r2 = this.A03;
            context = this.A01;
            i2 = R.string.payments_gating_generic_ineligibility_message;
        } else {
            C14900mE r6 = this.A03;
            C17070qD r7 = this.A07;
            AnonymousClass3FP r3 = new AnonymousClass3FP(this.A01, this.A02, r6, r7, this.A08, new RunnableBRunnable0Shape0S2310000_I0(this, r20, r21, str, str2, 1, z), new RunnableBRunnable0Shape9S0100000_I0_9(this, 22), this.A0B);
            UserJid of = UserJid.of(r20);
            if (of == null || !r3.A02()) {
                A01(r20, r21, str, str2, z);
                return;
            } else {
                r3.A01(of, new C1111258e(this), str2);
                return;
            }
        }
        r2.A0E(context.getString(i2), 1);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0017, code lost:
        if (r11.A04.A0F(r13.A0L.A0E) == false) goto L_0x0019;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A01(X.AbstractC14640lm r12, X.AbstractC15340mz r13, java.lang.String r14, java.lang.String r15, boolean r16) {
        /*
            r11 = this;
            if (r13 == 0) goto L_0x0019
            X.1IR r0 = r13.A0L
            if (r0 == 0) goto L_0x0019
            boolean r0 = r0.A0D()
            if (r0 == 0) goto L_0x0019
            X.0nT r1 = r11.A04
            X.1IR r0 = r13.A0L
            com.whatsapp.jid.UserJid r0 = r0.A0E
            boolean r0 = r1.A0F(r0)
            r7 = 1
            if (r0 != 0) goto L_0x001a
        L_0x0019:
            r7 = 0
        L_0x001a:
            X.0zW r1 = r11.A06
            X.18T r0 = r11.A05
            X.3Ce r10 = new X.3Ce
            r10.<init>(r0, r1)
            android.content.Context r3 = r11.A01
            java.lang.String r5 = r11.A00
            java.lang.Class<java.lang.String> r6 = java.lang.String.class
            X.18T r0 = r10.A00
            r4 = 0
            android.content.Intent r2 = r0.A00(r3, r7, r4)
            java.lang.String r0 = "referral_screen"
            r2.putExtra(r0, r15)
            java.lang.String r0 = "extra_payment_note"
            r2.putExtra(r0, r14)
            java.lang.String r9 = "upiHandle"
            java.lang.String r8 = "extra_payment_handle"
            if (r13 == 0) goto L_0x00bc
            java.lang.String r0 = "extra_conversation_message_type"
            r5 = 2
            r2.putExtra(r0, r5)
            X.0lm r0 = r13.A0B()
            com.whatsapp.jid.UserJid r1 = com.whatsapp.jid.UserJid.of(r0)
            X.0zW r0 = r10.A01
            int r0 = r0.A00(r1)
            if (r0 != r5) goto L_0x0079
            java.lang.String r1 = X.C15380n4.A03(r1)
            java.lang.String r0 = "extra_receiver_jid"
            r2.putExtra(r0, r1)
            X.1IR r0 = r13.A0L
            if (r0 == 0) goto L_0x0079
            X.1Zf r0 = r0.A0A
            if (r0 == 0) goto L_0x0079
            X.2SM r5 = new X.2SM
            r5.<init>()
            java.lang.String r1 = r0.A0F()
            X.1ZR r0 = new X.1ZR
            r0.<init>(r5, r6, r1, r9)
            r2.putExtra(r8, r0)
        L_0x0079:
            long r0 = r13.A11
            java.lang.String r5 = "extra_quoted_msg_row_id"
            r2.putExtra(r5, r0)
            if (r7 == 0) goto L_0x00a1
            X.1IS r0 = r13.A0z
            java.lang.String r1 = r0.A01
            java.lang.String r0 = "extra_request_message_key"
            r2.putExtra(r0, r1)
            X.1IR r0 = r13.A0L
            X.AnonymousClass009.A05(r0)
            X.1Yy r0 = r0.A08
            X.AnonymousClass009.A05(r0)
            java.lang.String r1 = r0.toString()
            java.lang.String r0 = "extra_payment_preset_amount"
            r2.putExtra(r0, r1)
            r2.putExtra(r5, r4)
        L_0x00a1:
            java.lang.String r0 = "extra_should_open_transaction_detail_after_send_override"
            r1 = r16
            r2.putExtra(r0, r1)
            java.lang.String r1 = r12.getRawString()
            java.lang.String r0 = "extra_jid"
            r2.putExtra(r0, r1)
            r3.startActivity(r2)
            java.lang.Runnable r0 = r11.A0A
            if (r0 == 0) goto L_0x00bb
            r0.run()
        L_0x00bb:
            return
        L_0x00bc:
            boolean r0 = android.text.TextUtils.isEmpty(r5)
            if (r0 != 0) goto L_0x00a1
            X.2SM r1 = new X.2SM
            r1.<init>()
            X.1ZR r0 = new X.1ZR
            r0.<init>(r1, r6, r5, r9)
            r2.putExtra(r8, r0)
            goto L_0x00a1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1IZ.A01(X.0lm, X.0mz, java.lang.String, java.lang.String, boolean):void");
    }
}
