package X;

import java.util.concurrent.CancellationException;

/* renamed from: X.3dT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C71763dT extends CancellationException {
    public final AbstractC02760Dw job;

    public C71763dT(String str, Throwable th, AbstractC02760Dw r3) {
        super(str);
        this.job = r3;
        if (th != null) {
            initCause(th);
        }
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C71763dT)) {
            return false;
        }
        C71763dT r3 = (C71763dT) obj;
        return C16700pc.A0O(r3.getMessage(), getMessage()) && C16700pc.A0O(r3.job, this.job) && C16700pc.A0O(r3.getCause(), getCause());
    }

    @Override // java.lang.Throwable
    public Throwable fillInStackTrace() {
        setStackTrace(new StackTraceElement[0]);
        return this;
    }

    @Override // java.lang.Object
    public int hashCode() {
        String message = getMessage();
        C16700pc.A0C(message);
        int A08 = C12990iw.A08(this.job, message.hashCode() * 31) * 31;
        Throwable cause = getCause();
        return A08 + (cause == null ? 0 : cause.hashCode());
    }

    @Override // java.lang.Throwable, java.lang.Object
    public String toString() {
        StringBuilder A0h = C12960it.A0h();
        A0h.append(super.toString());
        A0h.append("; job=");
        return C12970iu.A0s(this.job, A0h);
    }
}
