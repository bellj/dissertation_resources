package X;

import java.util.Arrays;
import java.util.Collection;

/* renamed from: X.0rh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC17970rh extends AbstractC17980ri {
    public Object[] contents;
    public boolean forceCopy;
    public int size = 0;

    public AbstractC17970rh(int i) {
        C28251Mi.checkNonnegative(i, "initialCapacity");
        this.contents = new Object[i];
    }

    @Override // X.AbstractC17980ri
    public AbstractC17970rh add(Object obj) {
        getReadyToExpandTo(this.size + 1);
        Object[] objArr = this.contents;
        int i = this.size;
        this.size = i + 1;
        objArr[i] = obj;
        return this;
    }

    public AbstractC17980ri add(Object... objArr) {
        addAll(objArr, objArr.length);
        return this;
    }

    @Override // X.AbstractC17980ri
    public AbstractC17980ri addAll(Iterable iterable) {
        if (iterable instanceof Collection) {
            Collection collection = (Collection) iterable;
            getReadyToExpandTo(this.size + collection.size());
            if (collection instanceof AbstractC17950rf) {
                this.size = ((AbstractC17950rf) collection).copyIntoArray(this.contents, this.size);
                return this;
            }
        }
        super.addAll(iterable);
        return this;
    }

    public final void addAll(Object[] objArr, int i) {
        C28331Mt.checkElementsNotNull(objArr, i);
        getReadyToExpandTo(this.size + i);
        System.arraycopy(objArr, 0, this.contents, this.size, i);
        this.size += i;
    }

    private void getReadyToExpandTo(int i) {
        Object[] objArr;
        Object[] objArr2 = this.contents;
        int length = objArr2.length;
        if (length < i) {
            objArr = Arrays.copyOf(objArr2, AbstractC17980ri.expandedCapacity(length, i));
        } else if (this.forceCopy) {
            objArr = (Object[]) objArr2.clone();
        } else {
            return;
        }
        this.contents = objArr;
        this.forceCopy = false;
    }
}
