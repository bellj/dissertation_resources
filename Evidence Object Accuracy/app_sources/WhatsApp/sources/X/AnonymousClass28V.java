package X;

/* renamed from: X.28V  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass28V {
    public static boolean A00(Object obj, Object obj2) {
        if (obj != obj2) {
            return obj != null && obj.equals(obj2);
        }
        return true;
    }
}
