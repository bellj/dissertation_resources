package X;

import android.view.View;
import com.whatsapp.catalogcategory.view.CategoryThumbnailLoader;

/* renamed from: X.40w  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C850340w extends AbstractC75663kD {
    public final CategoryThumbnailLoader A00;
    public final AbstractC16710pd A01;
    public final AbstractC16710pd A02;
    public final AnonymousClass1J7 A03;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C850340w(View view, CategoryThumbnailLoader categoryThumbnailLoader, AnonymousClass1J7 r5) {
        super(view);
        C16700pc.A0E(categoryThumbnailLoader, 2);
        this.A00 = categoryThumbnailLoader;
        this.A03 = r5;
        this.A02 = new AnonymousClass1WL(new AnonymousClass5JZ(view));
        this.A01 = new AnonymousClass1WL(new AnonymousClass5JY(view));
    }
}
