package X;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.Date;

/* renamed from: X.10C  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass10C {
    public final C17200qQ A00;
    public final C22490zA A01;
    public final AbstractC15710nm A02;
    public final C14900mE A03;
    public final C22520zD A04;
    public final C15570nT A05;
    public final C20870wS A06;
    public final C22920zr A07;
    public final AnonymousClass101 A08;
    public final C22890zo A09;
    public final C20670w8 A0A;
    public final C16240og A0B;
    public final C14650lo A0C;
    public final AnonymousClass10B A0D;
    public final C18850tA A0E;
    public final C15550nR A0F;
    public final C22700zV A0G;
    public final C15410nB A0H;
    public final C14830m7 A0I;
    public final C16590pI A0J;
    public final C18360sK A0K;
    public final C14820m6 A0L;
    public final AnonymousClass018 A0M;
    public final C17650rA A0N;
    public final C15680nj A0O;
    public final C15650ng A0P;
    public final AnonymousClass106 A0Q;
    public final C16490p7 A0R;
    public final C20370ve A0S;
    public final C22830zi A0T;
    public final C21400xM A0U;
    public final AnonymousClass102 A0V;
    public final C14850m9 A0W;
    public final C16120oU A0X;
    public final C22960zv A0Y;
    public final AnonymousClass107 A0Z;
    public final AnonymousClass109 A0a;
    public final C22910zq A0b;
    public final C17220qS A0c;
    public final C19890uq A0d;
    public final C43521xA A0e;
    public final C20660w7 A0f;
    public final C20220vP A0g;
    public final C17070qD A0h;
    public final AnonymousClass103 A0i;
    public final C21840y4 A0j;
    public final C14910mF A0k;
    public final C22280yp A0l;
    public final C21810y1 A0m;
    public final C22940zt A0n;
    public final C22980zx A0o;
    public final C22950zu A0p;
    public final C20320vZ A0q;
    public final C22270yo A0r;
    public final C22660zR A0s;
    public final C18350sJ A0t;
    public final C22600zL A0u;
    public final C22900zp A0v;
    public final C22990zy A0w;
    public final C23000zz A0x;
    public final C18370sL A0y;
    public final C20700wB A0z;
    public final C28181Ma A10 = new C28181Ma(true);
    public final AbstractC14440lR A11;
    public final C22870zm A12;

    public AnonymousClass10C(C17200qQ r42, C22490zA r43, AbstractC15710nm r44, C14900mE r45, C22930zs r46, C22820zh r47, C22520zD r48, C15570nT r49, C20870wS r50, C22920zr r51, AnonymousClass101 r52, C22890zo r53, C20670w8 r54, C16240og r55, C22880zn r56, C14650lo r57, AnonymousClass10A r58, C22970zw r59, C17030q9 r60, C22330yu r61, AnonymousClass10B r62, C18850tA r63, C15550nR r64, C22700zV r65, C20730wE r66, C15410nB r67, C14830m7 r68, C16590pI r69, C18360sK r70, C14820m6 r71, AnonymousClass018 r72, C15990oG r73, C18240s8 r74, C17650rA r75, C19480uB r76, C19490uC r77, C20830wO r78, C15680nj r79, C15650ng r80, AnonymousClass106 r81, C16490p7 r82, C20370ve r83, C22830zi r84, C21400xM r85, C22130yZ r86, AnonymousClass102 r87, C14850m9 r88, C16120oU r89, C22960zv r90, AnonymousClass107 r91, AnonymousClass109 r92, C22310ys r93, AnonymousClass104 r94, C22910zq r95, C17220qS r96, C19890uq r97, C22170ye r98, C20660w7 r99, C17230qT r100, C20220vP r101, C17070qD r102, AnonymousClass103 r103, C21840y4 r104, C14910mF r105, C22280yp r106, C21810y1 r107, C22940zt r108, C22980zx r109, C22950zu r110, C20320vZ r111, C22270yo r112, C22660zR r113, C18350sJ r114, C22600zL r115, C22900zp r116, AnonymousClass100 r117, C20780wJ r118, AnonymousClass105 r119, C22990zy r120, C23000zz r121, C14920mG r122, C18370sL r123, C20700wB r124, AbstractC14440lR r125, C22870zm r126) {
        this.A0J = r69;
        this.A0I = r68;
        this.A0W = r88;
        this.A03 = r45;
        this.A02 = r44;
        this.A05 = r49;
        this.A11 = r125;
        this.A0y = r123;
        this.A0X = r89;
        this.A0f = r99;
        this.A0E = r63;
        this.A12 = r126;
        this.A00 = r42;
        this.A0s = r113;
        this.A0A = r54;
        this.A0c = r96;
        this.A0u = r115;
        this.A0F = r64;
        this.A06 = r50;
        this.A0d = r97;
        this.A09 = r53;
        this.A0M = r72;
        this.A0q = r111;
        this.A0r = r112;
        this.A0k = r105;
        this.A0h = r102;
        this.A0l = r106;
        this.A0P = r80;
        this.A0z = r124;
        this.A0v = r116;
        this.A0b = r95;
        this.A07 = r51;
        this.A0B = r55;
        this.A0j = r104;
        this.A0n = r108;
        this.A0H = r67;
        this.A0p = r110;
        this.A0Y = r90;
        this.A04 = r48;
        this.A0U = r85;
        this.A0g = r101;
        this.A0R = r82;
        this.A0o = r109;
        this.A0m = r107;
        this.A0t = r114;
        this.A0w = r120;
        this.A0x = r121;
        this.A0G = r65;
        this.A0L = r71;
        this.A0O = r79;
        this.A0T = r84;
        this.A08 = r52;
        this.A0K = r70;
        this.A0C = r57;
        this.A0V = r87;
        this.A0Q = r81;
        this.A0Z = r91;
        this.A0S = r83;
        this.A0a = r92;
        this.A0N = r75;
        this.A0D = r62;
        this.A01 = r43;
        this.A0i = r103;
        this.A0e = new C43521xA(r44, r45, r46, r47, r56, r58, r59, r60, r61, r65, r66, r69, r73, r74, r76, r77, r78, r80, r86, r93, r94, r97, r98, r99, r100, r106, r117, r118, r119, r122, r125);
    }

    public void A00() {
        C16240og r3 = this.A0B;
        boolean z = false;
        if (r3.A04 == 2) {
            z = true;
        }
        r3.A04 = 3;
        r3.A05 = false;
        r3.A03.close();
        AnonymousClass009.A01();
        for (AbstractC18870tC r0 : r3.A01()) {
            r0.ARB();
        }
        C21810y1 r6 = this.A0m;
        r6.A00 = false;
        C22270yo r2 = this.A0r;
        r2.A01 = false;
        r2.A00 = 0;
        Log.i("server disconnected");
        SharedPreferences sharedPreferences = this.A0L.A00;
        if (sharedPreferences.getBoolean("spam_banned", false)) {
            Context context = this.A0J.A00;
            long j = sharedPreferences.getLong("spam_banned_expiry_timestamp", -1);
            long j2 = -1;
            if (j != -1) {
                j = (j - System.currentTimeMillis()) / 1000;
            }
            if (j > 0) {
                j2 = j;
            }
            Intent intent = new Intent();
            intent.setClassName(context.getPackageName(), "com.whatsapp.spamwarning.SpamWarningActivity");
            intent.putExtra("expiry_in_seconds", (int) j2);
            if (!TextUtils.isEmpty(null)) {
                intent.putExtra("spam_warning_message_key", (String) null);
            }
            if (!TextUtils.isEmpty(null)) {
                intent.putExtra("faq_url_key", (String) null);
            }
            intent.setFlags(268435456);
            context.startActivity(intent);
        }
        C22600zL r22 = this.A0u;
        r22.A02 = false;
        Log.i("routeselector/cancelrouterequesttimer");
        r22.A04.removeMessages(0);
        this.A0H.A00();
        this.A0l.A02();
        if (z) {
            C20700wB r1 = this.A0z;
            r1.A00.A00();
            r1.A01.A00();
            r1.A02.A00();
        }
        int i = this.A0k.A00;
        if (i == 2) {
            C21840y4 r23 = this.A0j;
            r23.A04.A00();
            StringBuilder sb = new StringBuilder("presencestatemanager/setUnavailable previous-state: ");
            C14910mF r12 = r23.A05;
            sb.append(r12);
            Log.i(sb.toString());
            r12.A00 = 3;
        } else if (i == 1 && z) {
            long j3 = this.A0d.A03;
            if (j3 <= 0 || SystemClock.elapsedRealtime() >= j3) {
                r6.A00 = true;
                r6.A00();
            }
        }
    }

    public final void A01() {
        AbstractC13860kS r2 = this.A03.A00;
        if (r2 == null || !C43511x9.A02(r2, this.A0d, this.A0g)) {
            StringBuilder sb = new StringBuilder("message-handler-callback/handlerconnected/displaysoftwareexpired/notification ");
            sb.append(new Date());
            sb.append(" ");
            sb.append(System.currentTimeMillis());
            Log.w(sb.toString());
            boolean A03 = C38241nl.A03();
            int i = R.string.software_expired;
            if (A03) {
                i = R.string.software_deprecated;
            }
            Context context = this.A0J.A00;
            C29671Ue.A01(context, this.A0K, context.getString(R.string.expiry_notification_title, context.getString(R.string.localized_app_name)), context.getString(i, context.getString(R.string.localized_app_name)), 2);
            this.A0g.A03 = true;
        }
    }
}
