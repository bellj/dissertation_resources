package X;

import android.os.SystemClock;
import com.whatsapp.jid.DeviceJid;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.0zg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22810zg {
    public final C006202y A00 = new C006202y(250);
    public final C14830m7 A01;
    public final C15650ng A02;
    public final C20290vW A03;
    public final C16490p7 A04;
    public final C22830zi A05;
    public final C20620w3 A06;
    public final C20580vz A07;
    public final Set A08 = new HashSet();

    public C22810zg(C14830m7 r3, C15650ng r4, C20290vW r5, C16490p7 r6, C22830zi r7, C20620w3 r8, C20580vz r9) {
        this.A01 = r3;
        this.A02 = r4;
        this.A06 = r8;
        this.A03 = r5;
        this.A04 = r6;
        this.A05 = r7;
        this.A07 = r9;
    }

    public C39921ql A00(AbstractC15340mz r9) {
        long uptimeMillis = SystemClock.uptimeMillis();
        AnonymousClass1IS r4 = r9.A0z;
        C006202y r3 = this.A00;
        C39921ql r5 = (C39921ql) r3.A04(r4);
        if (r5 == null) {
            C20620w3 r52 = this.A06;
            if (r52.A03.A00("receipt_user_ready", 0) == 2) {
                r5 = r52.A00(r9.A11);
            } else {
                AbstractC14640lm r1 = r4.A00;
                if (C15380n4.A0J(r1) || C15380n4.A0N(r1)) {
                    r5 = this.A07.A01(r4);
                } else {
                    r5 = this.A07.A00(r9);
                }
            }
            r3.A08(r4, r5);
            this.A03.A00("ReceiptManager/getMessageReceipts", SystemClock.uptimeMillis() - uptimeMillis);
        }
        return r5;
    }

    public boolean A01(DeviceJid deviceJid, AbstractC15340mz r8) {
        AbstractC20610w2 r0;
        if (r8 == null || deviceJid == null) {
            return false;
        }
        C22830zi r1 = this.A05;
        if (r8 instanceof AnonymousClass1Iv) {
            r0 = r1.A01;
        } else {
            r0 = r1.A02;
        }
        C32621cS r02 = (C32621cS) r0.A00(r8).A00.get(deviceJid);
        if (r02 == null || r02.A00 <= 0) {
            return false;
        }
        return true;
    }
}
