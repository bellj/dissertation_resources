package X;

import android.graphics.Path;

/* renamed from: X.0fG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C10820fG extends ThreadLocal {
    @Override // java.lang.ThreadLocal
    public Object initialValue() {
        return new Path();
    }
}
