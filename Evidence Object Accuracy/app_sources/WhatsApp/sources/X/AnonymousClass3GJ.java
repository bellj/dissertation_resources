package X;

/* renamed from: X.3GJ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3GJ {
    public static AbstractC15340mz A00(C15350n0 r5, AbstractC15340mz r6) {
        AbstractC15340mz r4 = null;
        if (r5.getCursor() != null) {
            int count = r5.getCount();
            int A03 = r5.A03(r6);
            if (A03 >= 0) {
                int i = A03 + 1;
                r4 = r5.getItem(i);
                while (i <= count && r4 != null && (r4.A0z.A00 == null || r4.A0y == 10)) {
                    r4 = r5.getItem(i);
                    i++;
                }
            }
        }
        return r4;
    }

    public static boolean A01(C14850m9 r4, AbstractC15340mz r5, C30421Xi r6, boolean z) {
        boolean A1W;
        boolean A1W2;
        C16150oX r0;
        if (!(r5 instanceof C30421Xi)) {
            return false;
        }
        C30421Xi r2 = (C30421Xi) r5;
        if (r4.A07(1040)) {
            A1W = r6.A1C();
            A1W2 = r2.A1C();
        } else {
            A1W = C12970iu.A1W(((AbstractC15340mz) r6).A08);
            A1W2 = C12970iu.A1W(((AbstractC15340mz) r2).A08);
        }
        if (!A1W || !A1W2) {
            return false;
        }
        if ((z || !r5.A0z.A02 || r6.A0z.A02) && (r0 = ((AbstractC16130oV) r2).A02) != null && r0.A0F != null && C30041Vv.A13(r2)) {
            return true;
        }
        return false;
    }
}
