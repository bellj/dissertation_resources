package X;

import android.text.TextUtils;
import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.68x  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1329568x implements AnonymousClass17Y {
    public final C21860y6 A00;
    public final C18600si A01;
    public final C126745tN A02;
    public final AnonymousClass60U A03;

    @Override // X.AnonymousClass17Y
    public boolean A8l(String str, boolean z) {
        return false;
    }

    @Override // X.AnonymousClass17Y
    public boolean AfM() {
        return false;
    }

    @Override // X.AnonymousClass17Y
    public boolean AfV(AnonymousClass1ZY r2) {
        return false;
    }

    public C1329568x(C21860y6 r1, C18600si r2, C126745tN r3, AnonymousClass60U r4) {
        this.A03 = r4;
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
    }

    @Override // X.AnonymousClass17Y
    public void A8k() {
        this.A01.A0F(null);
        this.A03.A05("personal");
        C126745tN r2 = this.A02;
        C130005ye r0 = (C130005ye) r2.A01.A00.get();
        if (r0 != null) {
            r0.A03("alias-payments-br-trusted-device-key");
        }
        try {
            C18600si r22 = r2.A00;
            String A04 = r22.A04();
            if (!TextUtils.isEmpty(A04)) {
                JSONObject A05 = C13000ix.A05(A04);
                A05.remove("td");
                C117295Zj.A1E(r22, A05);
            }
        } catch (JSONException e) {
            Log.w("PAY: TrustedDeviceKeyStore delete failed", e);
        }
    }

    @Override // X.AnonymousClass17Y
    public boolean AdJ(AbstractC30891Zf r3) {
        return !C12980iv.A1W(this.A01.A01(), "payments_card_can_receive_payment") || !this.A00.A0C();
    }
}
