package X;

import com.whatsapp.R;
import com.whatsapp.authentication.AppAuthenticationActivity;
import com.whatsapp.util.Log;

/* renamed from: X.2eB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53382eB extends AnonymousClass0PW {
    public final /* synthetic */ AppAuthenticationActivity A00;

    public C53382eB(AppAuthenticationActivity appAuthenticationActivity) {
        this.A00 = appAuthenticationActivity;
    }

    @Override // X.AnonymousClass0PW
    public void A00() {
        Log.i("AppAuthenticationActivity/failed");
        this.A00.A01 = 3;
    }

    @Override // X.AnonymousClass0PW
    public void A01(int i, CharSequence charSequence) {
        Log.i("AppAuthenticationActivity/error");
        AppAuthenticationActivity appAuthenticationActivity = this.A00;
        appAuthenticationActivity.A01 = 3;
        appAuthenticationActivity.A06.A01(true);
        if (i == 7) {
            Log.i("AppAuthenticationActivity/error-too-many-attempts");
            C14900mE r4 = ((ActivityC13810kN) appAuthenticationActivity).A05;
            Object[] objArr = new Object[1];
            C12960it.A1P(objArr, 30, 0);
            r4.A0E(appAuthenticationActivity.getString(R.string.app_auth_lockout_error, objArr), 1);
        }
    }

    @Override // X.AnonymousClass0PW
    public void A02(C04700Ms r4) {
        Log.i("AppAuthenticationActivity/success");
        AppAuthenticationActivity appAuthenticationActivity = this.A00;
        appAuthenticationActivity.A01 = 3;
        appAuthenticationActivity.A06.A01(false);
        appAuthenticationActivity.A2T();
        appAuthenticationActivity.finish();
    }
}
