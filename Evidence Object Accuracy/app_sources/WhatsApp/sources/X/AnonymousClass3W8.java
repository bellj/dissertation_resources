package X;

/* renamed from: X.3W8  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3W8 implements AnonymousClass5W0 {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AnonymousClass3M8 A01;
    public final /* synthetic */ C68563Vw A02;

    public AnonymousClass3W8(AnonymousClass3M8 r1, C68563Vw r2, int i) {
        this.A02 = r2;
        this.A01 = r1;
        this.A00 = i;
    }

    @Override // X.AnonymousClass5W0
    public void ASk() {
        C68563Vw r4 = this.A02;
        C63363Bh r3 = r4.A05;
        r3.A02 = 4;
        r3.A05 = new AnonymousClass4N3(this.A01, this.A00);
        r4.A02 = true;
        r4.A03();
    }

    @Override // X.AnonymousClass5W0
    public void AUI() {
        C68563Vw r4 = this.A02;
        C63363Bh r3 = r4.A05;
        r3.A02 = 3;
        r3.A05 = new AnonymousClass4N3(this.A01, this.A00);
        r4.A02 = true;
        r4.A03();
    }
}
