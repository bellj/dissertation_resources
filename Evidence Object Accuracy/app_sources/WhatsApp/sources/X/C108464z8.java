package X;

import android.content.Context;

/* renamed from: X.4z8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C108464z8 implements AbstractC116365Vd {
    public final int A00;

    public C108464z8(int i) {
        this.A00 = i;
    }

    @Override // X.AbstractC116365Vd
    public final int Agj(Context context, String str) {
        return this.A00;
    }

    @Override // X.AbstractC116365Vd
    public final int Ah1(Context context, String str, boolean z) {
        return 0;
    }
}
