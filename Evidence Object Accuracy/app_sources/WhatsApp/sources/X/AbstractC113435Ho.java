package X;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Principal;
import java.security.Provider;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.CRLException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509CRL;
import java.security.cert.X509CRLEntry;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import javax.security.auth.x500.X500Principal;

/* renamed from: X.5Ho  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC113435Ho extends X509CRL {
    public String A00;
    public AnonymousClass5MU A01;
    public AnonymousClass5S2 A02;
    public boolean A03;
    public byte[] A04;

    public AbstractC113435Ho(String str, AnonymousClass5MU r2, AnonymousClass5S2 r3, byte[] bArr, boolean z) {
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = str;
        this.A04 = bArr;
        this.A03 = z;
    }

    @Override // java.security.cert.X509Extension
    public Set getCriticalExtensionOIDs() {
        return A01(true);
    }

    @Override // java.security.cert.X509CRL
    public byte[] getEncoded() {
        try {
            return this.A01.A02("DER");
        } catch (IOException e) {
            throw new CRLException(e.toString());
        }
    }

    @Override // java.security.cert.X509Extension
    public Set getNonCriticalExtensionOIDs() {
        return A01(false);
    }

    @Override // java.security.cert.X509CRL
    public String getSigAlgName() {
        return this.A00;
    }

    @Override // java.security.cert.X509CRL
    public byte[] getSigAlgParams() {
        return AnonymousClass1TT.A02(this.A04);
    }

    @Override // java.security.cert.X509CRL
    public void verify(PublicKey publicKey) {
        A03(publicKey, new AnonymousClass5GL(this));
    }

    @Override // java.security.cert.X509CRL
    public void verify(PublicKey publicKey, String str) {
        A03(publicKey, new AnonymousClass5GN(str, this));
    }

    public static AnonymousClass5N2 A00(AbstractC114775Na r1, AnonymousClass5N2 r2, AnonymousClass5MS r3) {
        if (r1.A0B() == 3) {
            AnonymousClass5MX A03 = r3.A03();
            C114715Mu r0 = (C114715Mu) A03.A00.get(C114715Mu.A0A);
            if (r0 != null) {
                AnonymousClass5N1[] r32 = C114705Mt.A00(r0.A03()).A00;
                int length = r32.length;
                AnonymousClass5N1[] r12 = new AnonymousClass5N1[length];
                System.arraycopy(r32, 0, r12, 0, length);
                return AnonymousClass5N2.A00(r12[0].A01);
            }
        }
        return r2;
    }

    public final Set A01(boolean z) {
        AnonymousClass5MX r4;
        if (getVersion() != 2 || (r4 = this.A01.A03.A04) == null) {
            return null;
        }
        HashSet A12 = C12970iu.A12();
        Enumeration elements = r4.A01.elements();
        while (elements.hasMoreElements()) {
            AnonymousClass1TK r1 = (AnonymousClass1TK) elements.nextElement();
            if (z == AnonymousClass5MX.A00(r1, r4).A02) {
                A12.add(r1.A01);
            }
        }
        return A12;
    }

    public final void A02(PublicKey publicKey, Signature signature, AnonymousClass1TN r6, byte[] bArr) {
        if (r6 != null) {
            C95444dj.A03(signature, r6);
        }
        signature.initVerify(publicKey);
        try {
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new AnonymousClass49G(signature), 512);
            this.A01.A03.A00(bufferedOutputStream, "DER");
            bufferedOutputStream.close();
            if (!signature.verify(bArr)) {
                throw new SignatureException("CRL does not verify with supplied public key.");
            }
        } catch (IOException e) {
            throw new CRLException(e.toString());
        }
    }

    public final void A03(PublicKey publicKey, AnonymousClass5VR r10) {
        AnonymousClass5MU r7 = this.A01;
        C114725Mv r2 = r7.A02;
        if (r2.equals(r7.A03.A03)) {
            if (AbstractC116995Xs.A0C.A04(r2.A01)) {
                AbstractC114775Na A04 = AbstractC114775Na.A04(r2.A00);
                AbstractC114775Na A042 = AbstractC114775Na.A04(AnonymousClass5MA.A00(r7.A01).A0B());
                boolean z = false;
                for (int i = 0; i != A042.A0B(); i++) {
                    C114725Mv A00 = C114725Mv.A00(A04.A0D(i));
                    try {
                        A02(publicKey, r10.A8Z(C95444dj.A01(A00)), A00.A00, AnonymousClass5MA.A00(A042.A0D(i)).A0B());
                        z = true;
                    } catch (InvalidKeyException | NoSuchAlgorithmException unused) {
                    } catch (SignatureException e) {
                        throw e;
                    }
                }
                if (!z) {
                    throw new InvalidKeyException("no matching key found");
                }
                return;
            }
            Signature A8Z = r10.A8Z(this.A00);
            byte[] bArr = this.A04;
            if (bArr == null) {
                A02(publicKey, A8Z, null, getSignature());
                return;
            }
            try {
                A02(publicKey, A8Z, AnonymousClass1TL.A03(bArr), getSignature());
            } catch (IOException e2) {
                throw new SignatureException(C12960it.A0d(e2.getMessage(), C12960it.A0k("cannot decode signature parameters: ")));
            }
        } else {
            throw new CRLException("Signature algorithm on CertificateList does not match TBSCertList.");
        }
    }

    @Override // java.security.cert.X509Extension
    public byte[] getExtensionValue(String str) {
        AnonymousClass5NH r0;
        C114715Mu A00;
        AnonymousClass5MX r1 = this.A01.A03.A04;
        if (r1 == null || (A00 = AnonymousClass5MX.A00(C72453ed.A12(str), r1)) == null) {
            r0 = null;
        } else {
            r0 = A00.A01;
        }
        if (r0 == null) {
            return null;
        }
        try {
            return r0.A01();
        } catch (Exception e) {
            throw C12960it.A0U(C12960it.A0d(e.toString(), C12960it.A0k("error parsing ")));
        }
    }

    @Override // java.security.cert.X509CRL
    public Principal getIssuerDN() {
        return new C114925Np(AnonymousClass5N2.A00(this.A01.A03.A02.A01));
    }

    @Override // java.security.cert.X509CRL
    public X500Principal getIssuerX500Principal() {
        try {
            return new X500Principal(this.A01.A03.A02.A01());
        } catch (IOException unused) {
            throw C12960it.A0U("can't encode issuer DN");
        }
    }

    @Override // java.security.cert.X509CRL
    public Date getNextUpdate() {
        C114765Mz r0 = this.A01.A03.A05;
        if (r0 == null) {
            return null;
        }
        return r0.A04();
    }

    @Override // java.security.cert.X509CRL
    public X509CRLEntry getRevokedCertificate(BigInteger bigInteger) {
        Enumeration r5;
        AnonymousClass5MN r1 = this.A01.A03;
        AbstractC114775Na r0 = r1.A01;
        if (r0 == null) {
            r5 = new C112325Cy(r1);
        } else {
            r5 = new AnonymousClass5D3(r0.A0C(), r1);
        }
        AnonymousClass5N2 r3 = null;
        while (r5.hasMoreElements()) {
            AnonymousClass5MS r2 = (AnonymousClass5MS) r5.nextElement();
            AbstractC114775Na r12 = r2.A00;
            if (AnonymousClass5NG.A00(AbstractC114775Na.A00(r12)).A0C(bigInteger)) {
                return new C113445Hp(r3, r2, this.A03);
            }
            if (this.A03) {
                r3 = A00(r12, r3, r2);
            }
        }
        return null;
    }

    @Override // java.security.cert.X509CRL
    public Set getRevokedCertificates() {
        Enumeration r4;
        HashSet A12 = C12970iu.A12();
        AnonymousClass5MN r1 = this.A01.A03;
        AbstractC114775Na r0 = r1.A01;
        if (r0 == null) {
            r4 = new C112325Cy(r1);
        } else {
            r4 = new AnonymousClass5D3(r0.A0C(), r1);
        }
        AnonymousClass5N2 r3 = null;
        while (r4.hasMoreElements()) {
            AnonymousClass5MS r2 = (AnonymousClass5MS) r4.nextElement();
            boolean z = this.A03;
            A12.add(new C113445Hp(r3, r2, z));
            if (z) {
                r3 = A00(r2.A00, r3, r2);
            }
        }
        if (!A12.isEmpty()) {
            return Collections.unmodifiableSet(A12);
        }
        return null;
    }

    @Override // java.security.cert.X509CRL
    public String getSigAlgOID() {
        return this.A01.A02.A01.A01;
    }

    @Override // java.security.cert.X509CRL
    public byte[] getSignature() {
        AnonymousClass5MA r1 = this.A01.A01;
        if (r1.A00 == 0) {
            return AnonymousClass1TT.A02(r1.A01);
        }
        throw C12960it.A0U("attempt to get non-octet aligned data from BIT STRING");
    }

    @Override // java.security.cert.X509CRL
    public byte[] getTBSCertList() {
        try {
            return this.A01.A03.A02("DER");
        } catch (IOException e) {
            throw new CRLException(e.toString());
        }
    }

    @Override // java.security.cert.X509CRL
    public Date getThisUpdate() {
        return this.A01.A03.A06.A04();
    }

    @Override // java.security.cert.X509CRL
    public int getVersion() {
        AnonymousClass5NG r0 = this.A01.A03.A00;
        if (r0 == null) {
            return 1;
        }
        return r0.A0B() + 1;
    }

    @Override // java.security.cert.X509Extension
    public boolean hasUnsupportedCriticalExtension() {
        Set criticalExtensionOIDs = getCriticalExtensionOIDs();
        if (criticalExtensionOIDs == null) {
            return false;
        }
        criticalExtensionOIDs.remove(C114715Mu.A0K.A01);
        criticalExtensionOIDs.remove(C114715Mu.A0C.A01);
        return !criticalExtensionOIDs.isEmpty();
    }

    @Override // java.security.cert.CRL
    public boolean isRevoked(Certificate certificate) {
        Enumeration r4;
        AnonymousClass5MS r7;
        AnonymousClass5N2 r0;
        C114715Mu A00;
        if (certificate.getType().equals("X.509")) {
            AnonymousClass5MN r1 = this.A01.A03;
            AbstractC114775Na r02 = r1.A01;
            if (r02 == null) {
                r4 = new C112325Cy(r1);
            } else {
                r4 = new AnonymousClass5D3(r02.A0C(), r1);
            }
            AnonymousClass5N2 r2 = r1.A02;
            if (r4.hasMoreElements()) {
                X509Certificate x509Certificate = (X509Certificate) certificate;
                BigInteger serialNumber = x509Certificate.getSerialNumber();
                while (true) {
                    if (!r4.hasMoreElements()) {
                        break;
                    }
                    Object nextElement = r4.nextElement();
                    if (nextElement instanceof AnonymousClass5MS) {
                        r7 = (AnonymousClass5MS) nextElement;
                    } else {
                        r7 = nextElement != null ? new AnonymousClass5MS(AbstractC114775Na.A04(nextElement)) : null;
                    }
                    if (this.A03 && r7.A00.A0B() == 3 && (A00 = AnonymousClass5MX.A00(C114715Mu.A0A, r7.A03())) != null) {
                        r2 = AnonymousClass5N2.A00(C114705Mt.A01(A00.A03())[0].A01);
                    }
                    if (AnonymousClass5NG.A00(r7.A00.A0D(0)).A0C(serialNumber)) {
                        if (certificate instanceof X509Certificate) {
                            r0 = AnonymousClass5N2.A00(x509Certificate.getIssuerX500Principal().getEncoded());
                        } else {
                            try {
                                r0 = C114565Mf.A00(certificate.getEncoded()).A03.A05;
                            } catch (CertificateEncodingException e) {
                                throw C12970iu.A0f(C12960it.A0d(e.getMessage(), C12960it.A0k("Cannot process certificate: ")));
                            }
                        }
                        if (r2.equals(r0)) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        throw C12970iu.A0f("X.509 CRL used with non X.509 Cert");
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x007b  */
    @Override // java.lang.Object, java.security.cert.CRL
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String toString() {
        /*
        // Method dump skipped, instructions count: 335
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC113435Ho.toString():java.lang.String");
    }

    @Override // java.security.cert.X509CRL
    public void verify(PublicKey publicKey, Provider provider) {
        try {
            A03(publicKey, new AnonymousClass5GO(provider, this));
        } catch (NoSuchProviderException e) {
            throw new NoSuchAlgorithmException(C12960it.A0d(e.getMessage(), C12960it.A0k("provider issue: ")));
        }
    }
}
