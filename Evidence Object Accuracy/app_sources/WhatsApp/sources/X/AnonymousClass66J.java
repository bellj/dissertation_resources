package X;

import java.util.Collections;
import java.util.Map;
import org.chromium.net.UrlRequest;

/* renamed from: X.66J  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass66J implements AbstractC1311561m {
    public static final Boolean A05 = Boolean.FALSE;
    public static final Boolean A06 = Boolean.TRUE;
    public static final Map A07 = Collections.emptyMap();
    public final AnonymousClass61N A00;
    public final C128395w2 A01;
    public final EnumC124575pl A02;
    public final EnumC124575pl A03;
    public final boolean A04;

    public AnonymousClass66J(AnonymousClass61N r1, C128395w2 r2, EnumC124575pl r3, EnumC124575pl r4, boolean z) {
        this.A02 = r3;
        this.A03 = r4;
        this.A00 = r1;
        this.A01 = r2;
        this.A04 = z;
    }

    @Override // X.AbstractC1311561m
    public Object AAQ(C125455rH r3) {
        int i = r3.A00;
        if (i == 3) {
            return Boolean.valueOf(this.A04);
        }
        switch (i) {
            case 0:
            case 7:
                return A06;
            case 1:
            case 4:
            case 5:
            case 12:
                return false;
            case 2:
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
            default:
                throw C12990iw.A0m(C12960it.A0W(i, "Invalid Settings key: "));
            case 3:
            case 6:
            case 8:
            case 9:
            case 10:
            case 11:
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                return A05;
            case 15:
                return A07;
        }
    }
}
