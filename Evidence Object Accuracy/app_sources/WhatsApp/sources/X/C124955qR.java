package X;

import android.net.Uri;
import android.text.TextUtils;

/* renamed from: X.5qR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final /* synthetic */ class C124955qR {
    public static boolean A00(Uri uri, AnonymousClass17W r3) {
        String queryParameter = uri.getQueryParameter(r3.ABE());
        String AAu = r3.AAu();
        return !TextUtils.isEmpty(AAu) && !TextUtils.isEmpty(queryParameter) && AAu.contains(queryParameter);
    }
}
