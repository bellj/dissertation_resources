package X;

/* renamed from: X.41J  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass41J extends AnonymousClass4RX {
    public final boolean A00;

    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof AnonymousClass41J) && this.A00 == ((AnonymousClass41J) obj).A00);
    }

    public int hashCode() {
        boolean z = this.A00;
        if (z) {
            return 1;
        }
        return z ? 1 : 0;
    }

    public AnonymousClass41J(boolean z) {
        super(null, 14, z, false, false);
        this.A00 = z;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("CategoryList(isCategoriesEnabled=");
        A0k.append(this.A00);
        return C12970iu.A0u(A0k);
    }
}
