package X;

import android.hardware.Camera;
import android.util.Log;
import java.util.List;
import java.util.UUID;

/* renamed from: X.639  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass639 implements Camera.ErrorCallback {
    public final /* synthetic */ AnonymousClass661 A00;

    public AnonymousClass639(AnonymousClass661 r1) {
        this.A00 = r1;
    }

    @Override // android.hardware.Camera.ErrorCallback
    public void onError(int i, Camera camera) {
        String str;
        boolean z = false;
        if (i != 1) {
            if (i == 2) {
                str = "Camera evicted. Camera service was likely given to another customer. Camera resources will be released.";
            } else if (i != 100) {
                str = C12960it.A0W(i, "Unknown error code: ");
            } else {
                str = "Camera server died. Camera resources will be released.";
            }
            z = true;
        } else {
            str = "Unknown error";
        }
        AnonymousClass661 r4 = this.A00;
        List list = r4.A0P.A00;
        UUID uuid = r4.A0S.A03;
        Log.e("Camera1Device", str);
        r4.A0T.A05(new RunnableC135716Jn(r4, list, uuid, z), uuid);
    }
}
