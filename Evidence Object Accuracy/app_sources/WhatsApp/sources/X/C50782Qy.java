package X;

import com.whatsapp.jid.DeviceJid;

/* renamed from: X.2Qy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C50782Qy {
    public final int A00;
    public final DeviceJid A01;
    public final DeviceJid A02;
    public final AnonymousClass1IS A03;

    public C50782Qy(DeviceJid deviceJid, DeviceJid deviceJid2, AnonymousClass1IS r3, int i) {
        this.A03 = r3;
        this.A01 = deviceJid;
        this.A02 = deviceJid2;
        this.A00 = i;
    }
}
