package X;

/* renamed from: X.5po  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public enum EnumC124605po implements AnonymousClass5UX {
    VPA(0),
    /* Fake field, exist only in values array */
    VPA_FBID(1);
    
    public final String fieldName;

    EnumC124605po(int i) {
        this.fieldName = r2;
    }

    @Override // X.AnonymousClass5UX
    public String ACu() {
        return this.fieldName;
    }
}
