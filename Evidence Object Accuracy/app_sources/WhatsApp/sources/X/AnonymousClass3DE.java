package X;

import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape1S0310000_I1;
import com.whatsapp.jid.UserJid;

/* renamed from: X.3DE  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3DE {
    public final /* synthetic */ TextView A00;
    public final /* synthetic */ AnonymousClass1IR A01;
    public final /* synthetic */ UserJid A02;
    public final /* synthetic */ AnonymousClass1In A03;
    public final /* synthetic */ AnonymousClass18P A04;
    public final /* synthetic */ boolean A05 = true;

    public AnonymousClass3DE(TextView textView, AnonymousClass1IR r3, UserJid userJid, AnonymousClass1In r5, AnonymousClass18P r6) {
        this.A04 = r6;
        this.A01 = r3;
        this.A02 = userJid;
        this.A03 = r5;
        this.A00 = textView;
    }

    public void A00() {
        this.A04.A00.A0H(new RunnableBRunnable0Shape1S0310000_I1(this, this.A00, this.A01, 0, this.A05));
    }
}
