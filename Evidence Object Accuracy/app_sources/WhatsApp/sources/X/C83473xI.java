package X;

import android.view.View;
import android.view.animation.Animation;

/* renamed from: X.3xI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83473xI extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ C14680lr A00;

    public C83473xI(C14680lr r1) {
        this.A00 = r1;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        C14680lr r1 = this.A00;
        r1.A02.clearAnimation();
        View view = r1.A01;
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setFocusableInTouchMode(false);
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationStart(Animation animation) {
        this.A00.A02.setVisibility(0);
    }
}
