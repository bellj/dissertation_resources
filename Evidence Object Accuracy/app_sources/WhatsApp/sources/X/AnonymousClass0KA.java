package X;

import android.content.res.Configuration;

/* renamed from: X.0KA  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0KA {
    public static void A00(Configuration configuration, Configuration configuration2, Configuration configuration3) {
        int i = configuration.colorMode & 3;
        int i2 = configuration2.colorMode & 3;
        if (i != i2) {
            configuration3.colorMode |= i2;
        }
        int i3 = configuration.colorMode & 12;
        int i4 = configuration2.colorMode & 12;
        if (i3 != i4) {
            configuration3.colorMode |= i4;
        }
    }
}
