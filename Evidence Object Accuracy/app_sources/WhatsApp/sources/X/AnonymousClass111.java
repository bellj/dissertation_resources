package X;

import android.content.Context;
import java.io.File;

/* renamed from: X.111  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass111 {
    public final Context A00;

    public AnonymousClass111(Context context) {
        this.A00 = context;
    }

    public File A00(String str) {
        Context context = this.A00;
        new File(context.getFilesDir(), "migration/export/sandbox").mkdirs();
        return File.createTempFile("sandbox", str, new File(context.getFilesDir(), "migration/export/sandbox"));
    }

    public void A01() {
        C14350lI.A0C(new File(this.A00.getFilesDir(), "migration/export/sandbox"));
    }
}
