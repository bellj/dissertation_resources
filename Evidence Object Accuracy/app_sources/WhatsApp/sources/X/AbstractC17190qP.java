package X;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.SortedMap;

/* renamed from: X.0qP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC17190qP<K, V> implements Map<K, V>, Serializable {
    public static final Map.Entry[] EMPTY_ENTRY_ARRAY = new Map.Entry[0];
    public transient AbstractC17940re entrySet;
    public transient AbstractC17940re keySet;
    public transient AbstractC17950rf values;

    public abstract AbstractC17940re createEntrySet();

    public abstract AbstractC17940re createKeySet();

    public abstract AbstractC17950rf createValues();

    @Override // java.util.Map
    public abstract Object get(Object obj);

    public static C28241Mh builder() {
        return new C28241Mh();
    }

    public static C28241Mh builderWithExpectedSize(int i) {
        C28251Mi.checkNonnegative(i, "expectedSize");
        return new C28241Mh(i);
    }

    @Override // java.util.Map
    @Deprecated
    public final void clear() {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.Map
    public boolean containsKey(Object obj) {
        return get(obj) != null;
    }

    @Override // java.util.Map
    public boolean containsValue(Object obj) {
        return values().contains(obj);
    }

    public static AbstractC17190qP copyOf(Iterable iterable) {
        int i = 4;
        if (iterable instanceof Collection) {
            i = ((Collection) iterable).size();
        }
        C28241Mh r0 = new C28241Mh(i);
        r0.putAll(iterable);
        return r0.build();
    }

    public static AbstractC17190qP copyOf(Map map) {
        if (!(map instanceof AbstractC17190qP) || (map instanceof SortedMap)) {
            return copyOf(map.entrySet());
        }
        return (AbstractC17190qP) map;
    }

    @Override // java.util.Map
    public AbstractC17940re entrySet() {
        AbstractC17940re r0 = this.entrySet;
        if (r0 != null) {
            return r0;
        }
        AbstractC17940re createEntrySet = createEntrySet();
        this.entrySet = createEntrySet;
        return createEntrySet;
    }

    @Override // java.util.Map, java.lang.Object
    public boolean equals(Object obj) {
        return C28271Mk.equalsImpl(this, obj);
    }

    @Override // java.util.Map
    public final Object getOrDefault(Object obj, Object obj2) {
        Object obj3 = get(obj);
        return obj3 != null ? obj3 : obj2;
    }

    @Override // java.util.Map, java.lang.Object
    public int hashCode() {
        return C28281Ml.hashCodeImpl(entrySet());
    }

    @Override // java.util.Map
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override // java.util.Map
    public AbstractC17940re keySet() {
        AbstractC17940re r0 = this.keySet;
        if (r0 != null) {
            return r0;
        }
        AbstractC17940re createKeySet = createKeySet();
        this.keySet = createKeySet;
        return createKeySet;
    }

    public static AbstractC17190qP of() {
        return C28261Mj.EMPTY;
    }

    public static AbstractC17190qP of(Object obj, Object obj2) {
        C28251Mi.checkEntryNotNull(obj, obj2);
        return C28261Mj.create(1, new Object[]{obj, obj2});
    }

    public static AbstractC17190qP of(Object obj, Object obj2, Object obj3, Object obj4) {
        C28251Mi.checkEntryNotNull(obj, obj2);
        C28251Mi.checkEntryNotNull(obj3, obj4);
        return C28261Mj.create(2, new Object[]{obj, obj2, obj3, obj4});
    }

    public static AbstractC17190qP of(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8) {
        C28251Mi.checkEntryNotNull(obj, obj2);
        C28251Mi.checkEntryNotNull(obj3, obj4);
        C28251Mi.checkEntryNotNull(obj5, obj6);
        C28251Mi.checkEntryNotNull(obj7, obj8);
        return C28261Mj.create(4, new Object[]{obj, obj2, obj3, obj4, obj5, obj6, obj7, obj8});
    }

    public static AbstractC17190qP of(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8, Object obj9, Object obj10) {
        C28251Mi.checkEntryNotNull(obj, obj2);
        C28251Mi.checkEntryNotNull(obj3, obj4);
        C28251Mi.checkEntryNotNull(obj5, obj6);
        C28251Mi.checkEntryNotNull(obj7, obj8);
        C28251Mi.checkEntryNotNull(obj9, obj10);
        return C28261Mj.create(5, new Object[]{obj, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9, obj10});
    }

    @Override // java.util.Map
    @Deprecated
    public final Object put(Object obj, Object obj2) {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.Map
    @Deprecated
    public final void putAll(Map map) {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.Map
    @Deprecated
    public final Object remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    @Override // java.lang.Object
    public String toString() {
        return C28271Mk.toStringImpl(this);
    }

    @Override // java.util.Map
    public AbstractC17950rf values() {
        AbstractC17950rf r0 = this.values;
        if (r0 != null) {
            return r0;
        }
        AbstractC17950rf createValues = createValues();
        this.values = createValues;
        return createValues;
    }

    public Object writeReplace() {
        return new AnonymousClass1Mm(this);
    }
}
