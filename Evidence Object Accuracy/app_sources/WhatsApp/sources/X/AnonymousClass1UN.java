package X;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import com.whatsapp.service.WebClientService;
import com.whatsapp.util.Log;

/* renamed from: X.1UN  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1UN extends Handler {
    public long A00 = 60000;
    public final /* synthetic */ C14860mA A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass1UN(C14860mA r3) {
        super(Looper.getMainLooper());
        this.A01 = r3;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        C14860mA r4 = this.A01;
        Context context = r4.A0I.A00;
        int i = message.what;
        boolean z = true;
        if (i != 1) {
            if (i != 2) {
                if (i != 3) {
                    StringBuilder sb = new StringBuilder("qrsession/fservice unknown message: ");
                    sb.append(i);
                    sb.append(" uptime:");
                    sb.append(SystemClock.uptimeMillis());
                    Log.e(sb.toString());
                    return;
                }
                Log.i("qrsession/fservice/delayed exec");
            }
            StringBuilder sb2 = new StringBuilder("qrsession/fservice/kill kill:");
            sb2.append(hasMessages(2));
            sb2.append(" delayed:");
            sb2.append(hasMessages(3));
            sb2.append(" uptime:");
            sb2.append(SystemClock.uptimeMillis());
            Log.i(sb2.toString());
            removeMessages(2);
            removeMessages(3);
            this.A00 = 60000;
            r4.A0L.A01(context, WebClientService.class);
            return;
        }
        StringBuilder sb3 = new StringBuilder("qrsession/fservice/start kill:");
        sb3.append(hasMessages(2));
        sb3.append(" delayed:");
        sb3.append(hasMessages(3));
        sb3.append(" uptime:");
        sb3.append(SystemClock.uptimeMillis());
        Log.i(sb3.toString());
        removeMessages(1);
        Intent intent = new Intent();
        if (message.arg1 != 1) {
            z = false;
        }
        intent.putExtra("isPortal", z);
        r4.A0L.A03(context, intent, WebClientService.class);
    }
}
