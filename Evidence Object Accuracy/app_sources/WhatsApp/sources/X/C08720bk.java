package X;

import android.content.Context;
import android.view.View;

/* renamed from: X.0bk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08720bk implements AnonymousClass5WW {
    public final /* synthetic */ C14260l7 A00;
    public final /* synthetic */ AnonymousClass28D A01;
    public final /* synthetic */ AbstractC14200l1 A02;

    public C08720bk(C14260l7 r1, AnonymousClass28D r2, AbstractC14200l1 r3) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5WW
    public /* bridge */ /* synthetic */ void A6O(Context context, Object obj, Object obj2, Object obj3) {
        ((View) obj).setOnClickListener(new AnonymousClass0WJ(this));
    }

    @Override // X.AnonymousClass5WW
    public boolean Adc(Object obj, Object obj2, Object obj3, Object obj4) {
        return true;
    }

    @Override // X.AnonymousClass5WW
    public void Af8(Context context, Object obj, Object obj2, Object obj3) {
        View view = (View) obj;
        view.setOnClickListener(null);
        view.setClickable(false);
    }
}
