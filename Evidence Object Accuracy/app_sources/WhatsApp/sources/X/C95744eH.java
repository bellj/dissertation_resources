package X;

import android.animation.LayoutTransition;
import android.view.View;
import android.view.ViewGroup;
import com.facebook.redex.RunnableBRunnable0Shape15S0100000_I1_1;

/* renamed from: X.4eH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C95744eH implements LayoutTransition.TransitionListener {
    public final /* synthetic */ LayoutTransition A00;
    public final /* synthetic */ View A01;
    public final /* synthetic */ ViewGroup A02;

    @Override // android.animation.LayoutTransition.TransitionListener
    public void startTransition(LayoutTransition layoutTransition, ViewGroup viewGroup, View view, int i) {
    }

    public C95744eH(LayoutTransition layoutTransition, View view, ViewGroup viewGroup) {
        this.A01 = view;
        this.A00 = layoutTransition;
        this.A02 = viewGroup;
    }

    @Override // android.animation.LayoutTransition.TransitionListener
    public void endTransition(LayoutTransition layoutTransition, ViewGroup viewGroup, View view, int i) {
        if (view == this.A01) {
            this.A00.removeTransitionListener(this);
            ViewGroup viewGroup2 = this.A02;
            viewGroup2.postDelayed(new RunnableBRunnable0Shape15S0100000_I1_1(viewGroup2, 49), 100);
        }
    }
}
