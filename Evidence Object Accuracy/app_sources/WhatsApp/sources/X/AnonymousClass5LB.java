package X;

/* renamed from: X.5LB  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5LB extends AnonymousClass5LC {
    public final /* synthetic */ Object A00;
    public final /* synthetic */ C10710f4 A01;
    public final /* synthetic */ C94524by A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass5LB(Object obj, C10710f4 r2, C94524by r3) {
        super(r3);
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = obj;
    }

    @Override // X.AnonymousClass5LI
    public /* bridge */ /* synthetic */ Object A01(Object obj) {
        return A02();
    }

    public Object A02() {
        if (this.A01.A09() == this.A00) {
            return null;
        }
        return C88574Ge.A00;
    }
}
