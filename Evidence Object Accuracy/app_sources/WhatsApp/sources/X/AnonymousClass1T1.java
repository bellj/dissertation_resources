package X;

import android.os.Process;
import com.facebook.profilo.mmapbuf.core.Buffer;
import com.facebook.profilo.writer.NativeTraceWriter;
import com.facebook.profilo.writer.NativeTraceWriterCallbacks;

/* renamed from: X.1T1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1T1 extends Thread {
    public final long A00;
    public final C106154vF A01;
    public final NativeTraceWriter A02;
    public final String A03;
    public final String A04;
    public final Buffer[] A05;

    public AnonymousClass1T1(NativeTraceWriterCallbacks nativeTraceWriterCallbacks, String str, String str2, Buffer[] bufferArr, long j) {
        super("Prflo:Logger");
        this.A00 = j;
        this.A03 = str;
        this.A04 = str2;
        this.A05 = bufferArr;
        C106154vF r3 = new C106154vF(nativeTraceWriterCallbacks, bufferArr.length <= 1 ? false : true);
        this.A01 = r3;
        Buffer buffer = bufferArr[0];
        StringBuilder sb = new StringBuilder();
        sb.append(str2);
        sb.append("-0");
        this.A02 = new NativeTraceWriter(buffer, str, sb.toString(), r3);
    }

    @Override // java.lang.Thread, java.lang.Runnable
    public void run() {
        try {
            try {
                Process.setThreadPriority(5);
                this.A02.loop();
                Buffer[] bufferArr = this.A05;
                int length = bufferArr.length;
                if (length > 1) {
                    String str = this.A04;
                    StringBuilder sb = new StringBuilder(str.length() + 2);
                    for (int i = 1; i < length; i++) {
                        sb.setLength(0);
                        sb.append(str);
                        sb.append('-');
                        sb.append(i);
                        new NativeTraceWriter(bufferArr[i], this.A03, sb.toString(), null).dump(this.A00);
                    }
                }
            } catch (RuntimeException e) {
                this.A01.onTraceWriteException(this.A00, e);
            }
        } finally {
            this.A01.A00();
        }
    }
}
