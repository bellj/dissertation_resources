package X;

import android.app.Activity;
import android.os.Build;
import com.whatsapp.Mp4Ops;
import com.whatsapp.audioRecording.AudioRecordFactory;
import com.whatsapp.audioRecording.OpusRecorderFactory;
import com.whatsapp.calling.di.ActivityModule;
import com.whatsapp.gallery.MediaPickerFragmentModule;
import com.whatsapp.gallery.di.GalleryModule;
import com.whatsapp.stickers.di.StickersModule;

/* renamed from: X.2FL  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2FL extends AnonymousClass2FJ {
    public AnonymousClass01N A00;
    public AnonymousClass01N A01;
    public AnonymousClass01N A02;
    public AnonymousClass01N A03;
    public AnonymousClass01N A04;
    public AnonymousClass01N A05;
    public AnonymousClass01N A06;
    public AnonymousClass01N A07;
    public AnonymousClass01N A08;
    public AnonymousClass01N A09;
    public AnonymousClass01N A0A;
    public AnonymousClass01N A0B;
    public AnonymousClass01N A0C;
    public AnonymousClass01N A0D;
    public AnonymousClass01N A0E;
    public AnonymousClass01N A0F;
    public AnonymousClass01N A0G;
    public AnonymousClass01N A0H;
    public AnonymousClass01N A0I;
    public AnonymousClass01N A0J;
    public AnonymousClass01N A0K;
    public AnonymousClass01N A0L;
    public AnonymousClass01N A0M;
    public AnonymousClass01N A0N;
    public AnonymousClass01N A0O;
    public AnonymousClass01N A0P;
    public AnonymousClass01N A0Q;
    public AnonymousClass01N A0R;
    public AnonymousClass01N A0S;
    public AnonymousClass01N A0T;
    public AnonymousClass01N A0U;
    public AnonymousClass01N A0V;
    public AnonymousClass01N A0W;
    public AnonymousClass01N A0X;
    public AnonymousClass01N A0Y;
    public AnonymousClass01N A0Z;
    public AnonymousClass01N A0a;
    public AnonymousClass01N A0b;
    public AnonymousClass01N A0c;
    public AnonymousClass01N A0d;
    public AnonymousClass01N A0e;
    public AnonymousClass01N A0f;
    public AnonymousClass01N A0g;
    public AnonymousClass01N A0h;
    public AnonymousClass01N A0i;
    public AnonymousClass01N A0j;
    public AnonymousClass01N A0k;
    public AnonymousClass01N A0l;
    public AnonymousClass01N A0m;
    public AnonymousClass01N A0n;
    public AnonymousClass01N A0o;
    public AnonymousClass01N A0p;
    public AnonymousClass01N A0q;
    public AnonymousClass01N A0r;
    public AnonymousClass01N A0s;
    public AnonymousClass01N A0t;
    public AnonymousClass01N A0u;
    public AnonymousClass01N A0v;
    public AnonymousClass01N A0w;
    public AnonymousClass01N A0x;
    public AnonymousClass01N A0y;
    public AnonymousClass01N A0z;
    public AnonymousClass01N A10;
    public AnonymousClass01N A11;
    public AnonymousClass01N A12;
    public AnonymousClass01N A13;
    public AnonymousClass01N A14;
    public AnonymousClass01N A15;
    public AnonymousClass01N A16;
    public AnonymousClass01N A17;
    public AnonymousClass01N A18;
    public AnonymousClass01N A19;
    public AnonymousClass01N A1A;
    public final Activity A1B;
    public final AnonymousClass2FL A1C = this;
    public final C48722Hj A1D;
    public final AnonymousClass01J A1E;
    public final ActivityModule A1F;
    public final MediaPickerFragmentModule A1G;
    public final GalleryModule A1H;
    public final StickersModule A1I;

    public /* synthetic */ AnonymousClass2FL(Activity activity, C48722Hj r4, AnonymousClass01J r5, ActivityModule activityModule, MediaPickerFragmentModule mediaPickerFragmentModule, GalleryModule galleryModule, StickersModule stickersModule) {
        this.A1E = r5;
        this.A1D = r4;
        this.A1B = activity;
        this.A1F = activityModule;
        this.A1G = mediaPickerFragmentModule;
        this.A1H = galleryModule;
        this.A1I = stickersModule;
        this.A08 = C18000rk.A01(new C48302Fl(this, r4, r5, 1));
        this.A07 = C18000rk.A01(new C48302Fl(this, r4, r5, 0));
        this.A0H = C19970uy.A00(new C48302Fl(this, r4, r5, 2));
        this.A0S = C19970uy.A00(new C48302Fl(this, r4, r5, 4));
        this.A0d = C19970uy.A00(new C48302Fl(this, r4, r5, 5));
        this.A0o = C19970uy.A00(new C48302Fl(this, r4, r5, 3));
        this.A0u = C19970uy.A00(new C48302Fl(this, r4, r5, 6));
        this.A0v = C19970uy.A00(new C48302Fl(this, r4, r5, 7));
        this.A0w = C19970uy.A00(new C48302Fl(this, r4, r5, 8));
        this.A12 = C18000rk.A01(new C48302Fl(this, r4, r5, 9));
        this.A10 = C19970uy.A00(new C48302Fl(this, r4, r5, 10));
        this.A0D = C19970uy.A00(new C48302Fl(this, r4, r5, 11));
        this.A0x = C19970uy.A00(new C48302Fl(this, r4, r5, 12));
        this.A0y = C19970uy.A00(new C48302Fl(this, r4, r5, 13));
        this.A13 = C19970uy.A00(new C48302Fl(this, r4, r5, 14));
        this.A0I = C19970uy.A00(new C48302Fl(this, r4, r5, 15));
        this.A0J = C19970uy.A00(new C48302Fl(this, r4, r5, 16));
        this.A0K = C19970uy.A00(new C48302Fl(this, r4, r5, 17));
        this.A1A = C19970uy.A00(new C48302Fl(this, r4, r5, 18));
        this.A0L = C19970uy.A00(new C48302Fl(this, r4, r5, 19));
        this.A0B = new C48302Fl(this, r4, r5, 21);
        this.A18 = new C48302Fl(this, r4, r5, 22);
        this.A0G = new C48302Fl(this, r4, r5, 20);
        this.A0M = C19970uy.A00(new C48302Fl(this, r4, r5, 23));
        this.A0N = C19970uy.A00(new C48302Fl(this, r4, r5, 24));
        this.A0O = C19970uy.A00(new C48302Fl(this, r4, r5, 25));
        this.A0C = C19970uy.A00(new C48302Fl(this, r4, r5, 26));
        this.A15 = new C48302Fl(this, r4, r5, 27);
        this.A0P = C19970uy.A00(new C48302Fl(this, r4, r5, 28));
        this.A0Q = C19970uy.A00(new C48302Fl(this, r4, r5, 29));
        this.A0R = C19970uy.A00(new C48302Fl(this, r4, r5, 30));
        this.A0T = C19970uy.A00(new C48302Fl(this, r4, r5, 31));
        this.A19 = C19970uy.A00(new C48302Fl(this, r4, r5, 32));
        this.A0U = C19970uy.A00(new C48302Fl(this, r4, r5, 34));
        this.A0V = C19970uy.A00(new C48302Fl(this, r4, r5, 33));
        this.A0W = C19970uy.A00(new C48302Fl(this, r4, r5, 35));
        this.A0X = C19970uy.A00(new C48302Fl(this, r4, r5, 36));
        this.A00 = C19970uy.A00(new C48302Fl(this, r4, r5, 37));
        this.A0Y = C19970uy.A00(new C48302Fl(this, r4, r5, 38));
        this.A0Z = C19970uy.A00(new C48302Fl(this, r4, r5, 39));
        this.A0a = C19970uy.A00(new C48302Fl(this, r4, r5, 40));
        this.A0b = C19970uy.A00(new C48302Fl(this, r4, r5, 41));
        this.A0c = C19970uy.A00(new C48302Fl(this, r4, r5, 42));
        this.A0e = C19970uy.A00(new C48302Fl(this, r4, r5, 43));
        this.A0f = C19970uy.A00(new C48302Fl(this, r4, r5, 44));
        this.A01 = C18000rk.A01(new C48302Fl(this, r4, r5, 45));
        this.A03 = C18000rk.A01(new C48302Fl(this, r4, r5, 46));
        this.A02 = C18000rk.A01(new C48302Fl(this, r4, r5, 47));
        this.A04 = C18000rk.A01(new C48302Fl(this, r4, r5, 48));
        this.A05 = C18000rk.A01(new C48302Fl(this, r4, r5, 49));
        this.A06 = C18000rk.A01(new C48302Fl(this, r4, r5, 50));
        this.A0z = C18000rk.A01(new C48302Fl(this, r4, r5, 51));
        this.A0g = C19970uy.A00(new C48302Fl(this, r4, r5, 52));
        this.A0h = C19970uy.A00(new C48302Fl(this, r4, r5, 53));
        this.A0F = new C48302Fl(this, r4, r5, 54);
        this.A0E = new C48302Fl(this, r4, r5, 55);
        this.A11 = C18000rk.A01(new C48302Fl(this, r4, r5, 56));
        this.A0i = C19970uy.A00(new C48302Fl(this, r4, r5, 57));
        this.A16 = new C48302Fl(this, r4, r5, 58);
        this.A14 = C19970uy.A00(new C48302Fl(this, r4, r5, 60));
        this.A17 = C18000rk.A01(new C48302Fl(this, r4, r5, 59));
        this.A0j = C19970uy.A00(new C48302Fl(this, r4, r5, 61));
        this.A0k = C19970uy.A00(new C48302Fl(this, r4, r5, 62));
        this.A0l = C19970uy.A00(new C48302Fl(this, r4, r5, 63));
        this.A0m = C19970uy.A00(new C48302Fl(this, r4, r5, 64));
        this.A0n = C19970uy.A00(new C48302Fl(this, r4, r5, 65));
        this.A09 = C18000rk.A01(new C48302Fl(this, r4, r5, 66));
        this.A0p = C19970uy.A00(new C48302Fl(this, r4, r5, 68));
        this.A0q = C19970uy.A00(new C48302Fl(this, r4, r5, 69));
        this.A0r = C19970uy.A00(new C48302Fl(this, r4, r5, 70));
        this.A0s = C19970uy.A00(new C48302Fl(this, r4, r5, 71));
        this.A0t = C19970uy.A00(new C48302Fl(this, r4, r5, 72));
        this.A0A = C18000rk.A01(new C48302Fl(this, r4, r5, 67));
    }

    public static /* synthetic */ C48842Hz A00(AnonymousClass2FL r8) {
        AnonymousClass01J r1 = r8.A1E;
        AbstractC15710nm r2 = (AbstractC15710nm) r1.A4o.get();
        C15550nR r5 = (C15550nR) r1.A45.get();
        r8.A03();
        r1.A2D.get();
        return new C48842Hz(r2, (C48832Hy) r8.A0U.get(), (C16430p0) r1.A5y.get(), r5, (C14830m7) r1.ALb.get(), (C22610zM) r1.A6b.get());
    }

    public final AnonymousClass1CQ A02() {
        AnonymousClass01J r1 = this.A1E;
        return new AnonymousClass1CQ((C14650lo) r1.A2V.get(), (C14850m9) r1.A04.get());
    }

    public final C48822Hx A03() {
        AnonymousClass01J r1 = this.A1E;
        return new C48822Hx((AnonymousClass12P) r1.A0H.get(), (C15550nR) r1.A45.get(), (C20730wE) r1.A4J.get(), (C14830m7) r1.ALb.get(), (C22610zM) r1.A6b.get());
    }

    public final AnonymousClass2ID A04() {
        Object obj;
        AnonymousClass01J r1 = this.A1E;
        C14820m6 r6 = (C14820m6) r1.AN3.get();
        C14850m9 r5 = (C14850m9) r1.A04.get();
        AnonymousClass01H A00 = C18000rk.A00(this.A16);
        AnonymousClass01H A002 = C18000rk.A00(this.A17);
        if (!r6.A00.getBoolean("detect_device_foldable", false) && r5.A07(1674)) {
            obj = A00.get();
        } else if (!AnonymousClass1SF.A0L(r6, r5)) {
            return new AnonymousClass2IE();
        } else {
            obj = A002.get();
        }
        AnonymousClass2ID r0 = (AnonymousClass2ID) obj;
        if (r0 != null) {
            return r0;
        }
        throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }

    public final AnonymousClass2I2 A05() {
        AnonymousClass01J r1 = this.A1E;
        return new AnonymousClass2I2((C14850m9) r1.A04.get(), (AnonymousClass1AT) r1.AG2.get(), (C253118x) r1.AAW.get());
    }

    public final AnonymousClass2FO A06() {
        AnonymousClass01J r1 = this.A1E;
        return new AnonymousClass2FO((AbstractC15710nm) r1.A4o.get(), (C16590pI) r1.AMg.get(), (C15890o4) r1.AN1.get(), (C19390u2) r1.AFY.get());
    }

    public final AnonymousClass1BP A07() {
        AnonymousClass01J r1 = this.A1E;
        AbstractC15710nm r2 = (AbstractC15710nm) r1.A4o.get();
        C15450nH r3 = (C15450nH) r1.AII.get();
        C235812f r12 = (C235812f) r1.A13.get();
        AnonymousClass01d r4 = (AnonymousClass01d) r1.ALI.get();
        AnonymousClass018 r6 = (AnonymousClass018) r1.ANb.get();
        C18170s1 A41 = r1.A41();
        AnonymousClass12V r9 = (AnonymousClass12V) r1.A0p.get();
        return new AnonymousClass1BP(r2, r3, r4, (C14820m6) r1.AN3.get(), r6, (C14850m9) r1.A04.get(), (C16630pM) r1.AIc.get(), r9, A41, (C20980wd) r1.A0t.get(), r12, (C252718t) r1.A9K.get());
    }

    public final AnonymousClass69D A08() {
        AnonymousClass01J r1 = this.A1E;
        AnonymousClass018 r3 = (AnonymousClass018) r1.ANb.get();
        AnonymousClass61M r6 = (AnonymousClass61M) r1.AF1.get();
        return new AnonymousClass69D((C16590pI) r1.AMg.get(), r3, (C14850m9) r1.A04.get(), (C18660so) r1.AEf.get(), r6, (C22710zW) r1.AF7.get());
    }

    public final C129065x7 A09() {
        AnonymousClass01J r1 = this.A1E;
        return new C129065x7((AbstractC15710nm) r1.A4o.get(), (C14900mE) r1.A8X.get(), (C16590pI) r1.AMg.get(), (C17220qS) r1.ABt.get(), (C18650sn) r1.AEe.get());
    }

    public final C129665y6 A0A() {
        AnonymousClass01J r1 = this.A1E;
        AnonymousClass018 r2 = (AnonymousClass018) r1.ANb.get();
        C130155yt r4 = (C130155yt) r1.ADA.get();
        return new C129665y6(r2, (AnonymousClass102) r1.AEL.get(), r4, (C130025yg) r1.ADX.get(), (C129675y7) r1.AKB.get());
    }

    public final AnonymousClass2I6 A0B() {
        AnonymousClass01J r1 = this.A1E;
        return new AnonymousClass2I6((C15650ng) r1.A4m.get(), (C20370ve) r1.AEu.get(), A09());
    }

    public final C129735yD A0C() {
        AnonymousClass01J r14 = this.A1E;
        C14900mE r13 = (C14900mE) r14.A8X.get();
        r14.AMg.get();
        C18590sh r11 = (C18590sh) r14.AES.get();
        C17070qD r10 = (C17070qD) r14.AFC.get();
        C15650ng r9 = (C15650ng) r14.A4m.get();
        C18600si r8 = (C18600si) r14.AEo.get();
        AnonymousClass69D A08 = A08();
        C18610sj r7 = (C18610sj) r14.AF0.get();
        C22710zW r6 = (C22710zW) r14.AF7.get();
        C130015yf r5 = (C130015yf) r14.AEk.get();
        C129925yW r4 = (C129925yW) r14.AEW.get();
        C18620sk r3 = (C18620sk) r14.AFB.get();
        C18640sm r2 = (C18640sm) r14.A3u.get();
        C25871Bd r1 = (C25871Bd) r14.ABZ.get();
        return new C129735yD(r13, r2, r9, (C14850m9) r14.A04.get(), A08, r4, (C18650sn) r14.AEe.get(), r8, r7, r6, r3, r10, r14.A3Z(), r5, r1, r11, (AbstractC14440lR) r14.ANe.get());
    }

    public final AnonymousClass61P A0D() {
        AnonymousClass01J r1 = this.A1E;
        C14900mE r2 = (C14900mE) r1.A8X.get();
        C16170oZ r3 = (C16170oZ) r1.AM4.get();
        C20320vZ r7 = (C20320vZ) r1.A7A.get();
        return new AnonymousClass61P(r2, r3, (C15650ng) r1.A4m.get(), (C14850m9) r1.A04.get(), (C18610sj) r1.AF0.get(), r7, (AbstractC14440lR) r1.ANe.get());
    }

    public final C129175xI A0E() {
        AnonymousClass01J r1 = this.A1E;
        return new C129175xI((C14900mE) r1.A8X.get(), (C16170oZ) r1.AM4.get(), (C14410lO) r1.AB3.get(), (C18610sj) r1.AF0.get(), r1.A3c(), (C20360vd) r1.AEO.get());
    }

    public final AnonymousClass60W A0F() {
        AnonymousClass01J r1 = this.A1E;
        C14830m7 r6 = (C14830m7) r1.ALb.get();
        C15570nT r3 = (C15570nT) r1.AAr.get();
        C15450nH r4 = (C15450nH) r1.AII.get();
        AnonymousClass01d r5 = (AnonymousClass01d) r1.ALI.get();
        C22680zT r2 = (C22680zT) r1.AGW.get();
        C1308460e r10 = (C1308460e) r1.A9c.get();
        C18610sj r12 = (C18610sj) r1.AF0.get();
        AnonymousClass6BE r13 = (AnonymousClass6BE) r1.A9X.get();
        return new AnonymousClass60W(r2, r3, r4, r5, r6, (C16590pI) r1.AMg.get(), (C14850m9) r1.A04.get(), (C17220qS) r1.ABt.get(), r10, (C1329668y) r1.A9d.get(), r12, r13, (C22120yY) r1.ANn.get());
    }

    public final AnonymousClass2I0 A0G() {
        AnonymousClass01J r1 = this.A1E;
        C17220qS r5 = (C17220qS) r1.ABt.get();
        return new AnonymousClass2I0((AbstractC15710nm) r1.A4o.get(), (C15550nR) r1.A45.get(), (C14820m6) r1.AN3.get(), r5, new AnonymousClass1BM(), (AbstractC14440lR) r1.ANe.get());
    }

    public final AnonymousClass2I5 A0H() {
        AnonymousClass01J r1 = this.A1E;
        C17220qS r5 = (C17220qS) r1.ABt.get();
        return new AnonymousClass2I5((AbstractC15710nm) r1.A4o.get(), (C15550nR) r1.A45.get(), (C14820m6) r1.AN3.get(), r5, new AnonymousClass1BM(), (AbstractC14440lR) r1.ANe.get());
    }

    public final AnonymousClass2I8 A0I() {
        AnonymousClass01J r1 = this.A1E;
        C17220qS r5 = (C17220qS) r1.ABt.get();
        return new AnonymousClass2I8((AbstractC15710nm) r1.A4o.get(), (C15550nR) r1.A45.get(), (C14820m6) r1.AN3.get(), r5, new AnonymousClass1BM(), (AbstractC14440lR) r1.ANe.get());
    }

    public final AnonymousClass2GK A0J() {
        AnonymousClass01J r0 = this.A1E;
        C14900mE r1 = (C14900mE) r0.A8X.get();
        C15570nT r12 = (C15570nT) r0.AAr.get();
        C16120oU r13 = (C16120oU) r0.ANE.get();
        AnonymousClass17S r14 = (AnonymousClass17S) r0.A0F.get();
        C15450nH r15 = (C15450nH) r0.AII.get();
        AnonymousClass18U r16 = (AnonymousClass18U) r0.AAU.get();
        C17220qS r17 = (C17220qS) r0.ABt.get();
        C15550nR r152 = (C15550nR) r0.A45.get();
        AnonymousClass01d r142 = (AnonymousClass01d) r0.ALI.get();
        C15610nY r132 = (C15610nY) r0.AMe.get();
        C22260yn r122 = (C22260yn) r0.A5I.get();
        C17070qD r11 = (C17070qD) r0.AFC.get();
        C253318z r10 = (C253318z) r0.A4B.get();
        C22700zV r9 = (C22700zV) r0.AMN.get();
        C15680nj r8 = (C15680nj) r0.A4e.get();
        C22710zW r7 = (C22710zW) r0.AF7.get();
        C22410z2 r6 = (C22410z2) r0.ANH.get();
        C251118d r5 = (C251118d) r0.A2D.get();
        C18640sm r4 = (C18640sm) r0.A3u.get();
        C26311Cv r3 = (C26311Cv) r0.AAQ.get();
        C22610zM r2 = (C22610zM) r0.A6b.get();
        C17170qN r18 = (C17170qN) r0.AMt.get();
        return new AnonymousClass2GK(r14, r122, r1, r16, r12, r15, (C250918b) r0.A2B.get(), r5, r152, r3, r9, r132, r10, r4, r142, (C14830m7) r0.ALb.get(), r18, r2, r8, (C14850m9) r0.A04.get(), r13, r17, r6, r7, r11, (AbstractC14440lR) r0.ANe.get());
    }

    public final AnonymousClass2I1 A0K() {
        AnonymousClass01J r1 = this.A1E;
        return new AnonymousClass2I1((C16590pI) r1.AMg.get(), (AnonymousClass146) r1.AKM.get(), (C235512c) r1.AKS.get(), (AnonymousClass147) r1.ALS.get());
    }

    public final AnonymousClass2I7 A0L() {
        return new AnonymousClass2I7((C21740xu) this.A1E.AM1.get());
    }

    public final AbstractC35401hl A0M() {
        AnonymousClass01J r1 = this.A1E;
        C16590pI r9 = (C16590pI) r1.AMg.get();
        Activity activity = this.A1B;
        Mp4Ops mp4Ops = (Mp4Ops) r1.ACh.get();
        C14850m9 r11 = (C14850m9) r1.A04.get();
        C14900mE r5 = (C14900mE) r1.A8X.get();
        AbstractC15710nm r4 = (AbstractC15710nm) r1.A4o.get();
        AbstractC14440lR r13 = (AbstractC14440lR) r1.ANe.get();
        C18790t3 r7 = (C18790t3) r1.AJw.get();
        C16120oU r12 = (C16120oU) r1.ANE.get();
        AnonymousClass12P r3 = (AnonymousClass12P) r1.A0H.get();
        AnonymousClass01d r8 = (AnonymousClass01d) r1.ALI.get();
        AnonymousClass018 r10 = (AnonymousClass018) r1.ANb.get();
        if (Build.VERSION.SDK_INT >= 19) {
            return new AnonymousClass2I3(activity, r3, r4, r5, mp4Ops, r7, r8, r9, r10, r11, r12, r13);
        }
        return new AnonymousClass2I4();
    }

    public final AnonymousClass2IC A0N() {
        AnonymousClass01J r15 = this.A1E;
        C14900mE r0 = (C14900mE) r15.A8X.get();
        AbstractC15710nm r02 = (AbstractC15710nm) r15.A4o.get();
        C14330lG r03 = (C14330lG) r15.A7B.get();
        AnonymousClass199 r04 = (AnonymousClass199) r15.A0h.get();
        C15450nH r05 = (C15450nH) r15.AII.get();
        C16170oZ r06 = (C16170oZ) r15.AM4.get();
        C14410lO r07 = (C14410lO) r15.AB3.get();
        C254419k r08 = (C254419k) r15.AE4.get();
        AnonymousClass01d r09 = (AnonymousClass01d) r15.ALI.get();
        AnonymousClass018 r010 = (AnonymousClass018) r15.ANb.get();
        C20320vZ r011 = (C20320vZ) r15.A7A.get();
        C22050yP r012 = (C22050yP) r15.A7v.get();
        C21840y4 r13 = (C21840y4) r15.ACr.get();
        AudioRecordFactory audioRecordFactory = (AudioRecordFactory) r15.AGS.get();
        C21310xD r11 = (C21310xD) r15.AMS.get();
        AnonymousClass19A r10 = (AnonymousClass19A) r15.AMR.get();
        C14820m6 r9 = (C14820m6) r15.AN3.get();
        OpusRecorderFactory opusRecorderFactory = (OpusRecorderFactory) r15.AGj.get();
        C18280sC r7 = (C18280sC) r15.A1O.get();
        C17020q8 r6 = (C17020q8) r15.A6G.get();
        AnonymousClass109 r5 = (AnonymousClass109) r15.AI8.get();
        C16630pM r4 = (C16630pM) r15.AIc.get();
        C255819y r3 = (C255819y) r15.A38.get();
        AnonymousClass2I9 r2 = (AnonymousClass2I9) this.A0v.get();
        AnonymousClass11P r1 = (AnonymousClass11P) r15.ABp.get();
        AnonymousClass2IA r16 = new AnonymousClass2IA((C14850m9) r15.A04.get());
        return new AnonymousClass2IC(r02, r2, (AnonymousClass2IB) this.A0w.get(), r03, r0, r05, r06, audioRecordFactory, opusRecorderFactory, r7, r1, r16, r09, (C14830m7) r15.ALb.get(), r9, r010, r11, (C14850m9) r15.A04.get(), r012, r07, r5, r4, r13, r011, (AnonymousClass1AL) r15.AGw.get(), r04, r3, (AbstractC14440lR) r15.ANe.get(), r6, r08, r10, (C21260x8) r15.A2k.get());
    }
}
