package X;

import com.whatsapp.payments.ui.PaymentBottomSheet;
import com.whatsapp.payments.ui.widget.PaymentView;

/* renamed from: X.6Cd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133796Cd implements AnonymousClass61g {
    public final /* synthetic */ AnonymousClass6CP A00;
    public final /* synthetic */ PaymentBottomSheet A01;

    public C133796Cd(AnonymousClass6CP r1, PaymentBottomSheet paymentBottomSheet) {
        this.A00 = r1;
        this.A01 = paymentBottomSheet;
    }

    @Override // X.AnonymousClass61g
    public void A91() {
        PaymentBottomSheet paymentBottomSheet = this.A01;
        if (paymentBottomSheet != null) {
            paymentBottomSheet.A1B();
        }
    }

    @Override // X.AnonymousClass61g
    public void ASe(AnonymousClass1KC r4) {
        String str;
        AnonymousClass6CP r2 = this.A00;
        AbstractActivityC121525iS r1 = r2.A04;
        r1.A0D = r4;
        if (r4 == null) {
            str = null;
        } else {
            C14430lQ r0 = r4.A0J;
            AnonymousClass009.A05(r0);
            str = r0.A0D;
        }
        r1.A0c = str;
        r2.A00(this.A01);
    }

    @Override // X.AbstractC136476Mr
    public void AaH() {
        PaymentView paymentView = this.A00.A04.A0W;
        if (paymentView != null) {
            paymentView.A04();
        }
    }

    @Override // X.AbstractC136476Mr
    public void AaN() {
        this.A00.A04.AaN();
    }

    @Override // X.AbstractC136476Mr
    public void AaQ() {
        PaymentView paymentView = this.A00.A04.A0W;
        if (paymentView != null) {
            paymentView.A05();
        }
    }
}
