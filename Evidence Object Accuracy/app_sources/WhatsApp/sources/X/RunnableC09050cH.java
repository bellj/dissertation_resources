package X;

import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.View;

/* renamed from: X.0cH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09050cH implements Runnable {
    public final /* synthetic */ AnonymousClass0WW A00;

    public RunnableC09050cH(AnonymousClass0WW r1) {
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        AnonymousClass0WW r3 = this.A00;
        r3.A01();
        View view = r3.A07;
        if (view.isEnabled() && !view.isLongClickable() && r3.A03()) {
            view.getParent().requestDisallowInterceptTouchEvent(true);
            long uptimeMillis = SystemClock.uptimeMillis();
            MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
            view.onTouchEvent(obtain);
            obtain.recycle();
            r3.A03 = true;
        }
    }
}
