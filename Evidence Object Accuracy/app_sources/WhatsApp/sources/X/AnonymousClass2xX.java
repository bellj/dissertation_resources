package X;

import android.view.Menu;
import android.view.MenuItem;
import com.whatsapp.R;

/* renamed from: X.2xX  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2xX extends AbstractC36781kZ {
    public AnonymousClass2xX(ActivityC000800j r1, AbstractC13860kS r2, C14900mE r3, C15570nT r4, C15450nH r5, C16170oZ r6, C18330sH r7, AnonymousClass2TT r8, C22330yu r9, C243915i r10, AnonymousClass10S r11, C22700zV r12, AbstractC13920kY r13, C255319t r14, AnonymousClass1A6 r15, C17050qB r16, C14820m6 r17, AnonymousClass018 r18, C14950mJ r19, C19990v2 r20, C20830wO r21, C15370n3 r22, C22100yW r23, C244215l r24, C29901Ve r25, C15860o1 r26, AnonymousClass12F r27, AnonymousClass12U r28, C255719x r29, AbstractC14440lR r30, C21280xA r31) {
        super(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31);
    }

    @Override // X.AbstractC36781kZ, X.AbstractC36791ka
    public boolean ATH(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId == 21) {
            ActivityC000800j r4 = this.A01;
            r4.startActivity(C14960mK.A0K(r4, this.A00.A0D), AbstractC454421p.A05(r4, r4.findViewById(R.id.transition_start), this.A08.A00(R.string.transition_photo)));
        } else if (itemId != 22) {
            return super.ATH(menuItem);
        }
        return true;
    }

    @Override // X.AbstractC36781kZ, X.AbstractC36791ka
    public boolean AUA(Menu menu) {
        StringBuilder A0k = C12960it.A0k("listconversationmenu/onprepareoptionsmenu ");
        A0k.append(menu.size());
        C12960it.A1F(A0k);
        if (menu.size() == 0) {
            return false;
        }
        return super.AUA(menu);
    }
}
