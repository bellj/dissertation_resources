package X;

import android.view.ViewTreeObserver;

/* renamed from: X.4oF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnPreDrawListenerC101884oF implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ AbstractC36001jA A00;

    public ViewTreeObserver$OnPreDrawListenerC101884oF(AbstractC36001jA r1) {
        this.A00 = r1;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        AbstractC36001jA r2 = this.A00;
        C12980iv.A1G(r2.A0J, this);
        r2.A0G(r2.A0J.getHeight());
        r2.A0S(false);
        r2.A0O(null, false);
        return true;
    }
}
