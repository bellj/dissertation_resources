package X;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.whatsapp.R;
import java.util.List;

/* renamed from: X.2VF  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2VF extends AnonymousClass02M {
    public final AbstractC001200n A00;
    public final C15570nT A01;
    public final AnonymousClass130 A02;
    public final C15550nR A03;
    public final C15610nY A04;
    public final AnonymousClass131 A05;
    public final AnonymousClass018 A06;
    public final AnonymousClass3D7 A07;
    public final AnonymousClass2VG A08;

    public AnonymousClass2VF(AbstractC001200n r3, C15570nT r4, AnonymousClass130 r5, C15550nR r6, C15610nY r7, AnonymousClass131 r8, AnonymousClass018 r9, AnonymousClass3D7 r10, AnonymousClass2VG r11) {
        this.A01 = r4;
        this.A02 = r5;
        this.A03 = r6;
        this.A04 = r7;
        this.A06 = r9;
        this.A05 = r8;
        this.A00 = r3;
        this.A07 = r10;
        this.A08 = r11;
        r10.A02.A05(r3, new AnonymousClass02B() { // from class: X.4tQ
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                AnonymousClass2VF r1 = AnonymousClass2VF.this;
                int i = r1.A07.A01;
                if (i >= 0) {
                    r1.A05(i);
                } else {
                    r1.A02();
                }
            }
        });
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return ((List) this.A07.A02.A01()).size();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0145, code lost:
        if (r0 != false) goto L_0x0147;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0151, code lost:
        if (r11 != false) goto L_0x0133;
     */
    @Override // X.AnonymousClass02M
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ void ANH(X.AnonymousClass03U r14, int r15) {
        /*
        // Method dump skipped, instructions count: 340
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2VF.ANH(X.03U, int):void");
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        boolean z = this.A08.A08;
        LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
        if (z) {
            return new C862846n(from.inflate(R.layout.reactions_bottom_sheet_row_item_media, viewGroup, false));
        }
        return new C75563k3(from.inflate(R.layout.reactions_bottom_sheet_row_item, viewGroup, false));
    }
}
