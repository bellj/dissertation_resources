package X;

import android.widget.AbsListView;

/* renamed from: X.4oq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C102254oq implements AbsListView.OnScrollListener {
    public int A00 = 0;
    public final /* synthetic */ AbstractActivityC36611kC A01;

    @Override // android.widget.AbsListView.OnScrollListener
    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
    }

    public C102254oq(AbstractActivityC36611kC r2) {
        this.A01 = r2;
    }

    @Override // android.widget.AbsListView.OnScrollListener
    public void onScrollStateChanged(AbsListView absListView, int i) {
        int i2 = this.A00;
        if (i2 == 0 && i != i2) {
            ((ActivityC13790kL) this.A01).A0D.A01(absListView);
        }
        this.A00 = i;
    }
}
