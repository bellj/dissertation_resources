package X;

/* renamed from: X.4Zz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C93324Zz {
    public final int A00;
    public final long A01;

    public C93324Zz(int i, long j) {
        this.A00 = i;
        this.A01 = j;
    }

    public static C93324Zz A00(AnonymousClass5Yf r3, C95304dT r4) {
        r3.AZ4(r4.A02, 0, 8);
        return new C93324Zz(C95304dT.A03(r4, 0), r4.A0G());
    }
}
