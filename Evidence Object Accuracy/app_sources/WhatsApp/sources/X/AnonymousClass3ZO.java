package X;

import android.os.SystemClock;
import java.net.MalformedURLException;
import java.net.URL;

/* renamed from: X.3ZO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3ZO implements AbstractC21730xt {
    public final long A00 = SystemClock.elapsedRealtime();
    public final C14850m9 A01;
    public final AbstractC14640lm A02;
    public final C17220qS A03;
    public final AnonymousClass5WE A04;
    public final String A05;
    public final String A06;

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
    }

    public AnonymousClass3ZO(C14850m9 r3, AbstractC14640lm r4, C17220qS r5, AnonymousClass5WE r6, String str, String str2) {
        this.A01 = r3;
        this.A03 = r5;
        this.A06 = str;
        this.A02 = r4;
        this.A05 = str2;
        this.A04 = r6;
    }

    public final boolean A00() {
        String str = this.A06;
        if (!"preview".equals(str) || !this.A01.A07(101)) {
            return "image".equals(str) && this.A01.A07(102);
        }
        return true;
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r11, String str) {
        int A00 = C41151sz.A00(r11);
        if (A00 == 404) {
            this.A04.AUK(new C41831uE(this.A02, null, null, null, -1, C12980iv.A03("preview".equals(this.A06) ? 1 : 0)), this.A00);
        } else {
            this.A04.AUJ(this.A02, this.A06, A00, this.A00);
        }
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r12, String str) {
        byte[] bArr;
        String str2;
        int parseInt;
        AnonymousClass1V8 A0E = r12.A0E("picture");
        String str3 = this.A05;
        URL url = null;
        if (A0E != null) {
            bArr = A0E.A01;
            if (A00()) {
                str2 = A0E.A0I("direct_path", null);
            } else {
                str2 = null;
            }
            String A0I = A0E.A0I("url", null);
            if (A0I != null) {
                try {
                    url = new URL(A0I);
                } catch (MalformedURLException unused) {
                    throw new AnonymousClass1V9("Malformed picture url");
                }
            }
            str3 = A0E.A0I("id", null);
        } else {
            bArr = null;
            str2 = null;
        }
        if (str3 == null) {
            parseInt = -1;
        } else {
            try {
                parseInt = Integer.parseInt(str3);
            } catch (NumberFormatException unused2) {
                throw new AnonymousClass1V9(C12960it.A0d(str3, C12960it.A0k("Malformed photo id=")));
            }
        }
        this.A04.AUK(new C41831uE(this.A02, str2, url, bArr, parseInt, C12980iv.A03("preview".equals(this.A06) ? 1 : 0)), this.A00);
    }
}
