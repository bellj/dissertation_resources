package X;

import com.facebook.redex.RunnableBRunnable0Shape1S0200000_I0_1;
import com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3;

/* renamed from: X.3Vj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68433Vj implements AnonymousClass1P6 {
    public C48112Ej A00;
    public AbstractC49252Jz A01;
    public final AnonymousClass016 A02;
    public final C16430p0 A03;
    public final AnonymousClass3ER A04;
    public final AnonymousClass1B0 A05;
    public final AnonymousClass1B3 A06;
    public final C15890o4 A07;
    public final AbstractC14440lR A08;

    public C68433Vj(C16430p0 r10, AnonymousClass3ER r11, AnonymousClass1CO r12, AnonymousClass1B0 r13, AnonymousClass1B3 r14, AbstractC49242Jy r15, AbstractC49252Jz r16, C15890o4 r17, AbstractC14440lR r18) {
        int i;
        int i2;
        AnonymousClass016 A0T = C12980iv.A0T();
        this.A02 = A0T;
        this.A08 = r18;
        this.A03 = r10;
        this.A07 = r17;
        this.A04 = r11;
        this.A05 = r13;
        this.A06 = r14;
        this.A00 = new C48112Ej(r12, r13, r14, r15, r18);
        C21330xF r4 = this.A05.A02;
        if (!r4.A00().getBoolean("show_request_permission_dialog", true) || C12980iv.A1W(r4.A00(), "location_access_granted")) {
            C12960it.A0t(r4.A00().edit(), "show_request_permission_dialog", false);
            i = 2;
        } else {
            C16430p0 r3 = this.A03;
            C28531Ny r0 = new C28531Ny();
            i2 = 0;
            r0.A06 = 0;
            r3.A03(r0);
            i = this.A07.A03() ? 1 : i;
            A0T.A0B(i2);
            this.A01 = r16;
        }
        i2 = Integer.valueOf(i);
        A0T.A0B(i2);
        this.A01 = r16;
    }

    public static Integer A00(C68433Vj r0) {
        return Integer.valueOf(r0.A02());
    }

    public int A01() {
        switch (this.A00.A00) {
            case 0:
                return 0;
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 5;
            case 4:
                return 4;
            case 5:
                return 3;
            case 6:
                return 6;
            case 7:
                return 7;
            default:
                throw C12960it.A0U("SearchLocationItemLiveData/getWamLocationState Invalid location state");
        }
    }

    public int A02() {
        C48122Ek r0 = this.A00.A01;
        if (r0 == null) {
            return 2;
        }
        return r0.A01();
    }

    public void A03() {
        C16430p0 r3;
        int i;
        int i2 = this.A00.A00;
        if (i2 == 0 || i2 == 2) {
            r3 = this.A03;
            i = 26;
        } else if (i2 == 4 || i2 == 5) {
            r3 = this.A03;
            i = 27;
        } else {
            return;
        }
        r3.A06(A00(this), i, A01());
    }

    public void A04() {
        this.A05.A02(true);
        A05();
        C48112Ej r3 = this.A00;
        r3.A07.Ab2(new RunnableBRunnable0Shape3S0100000_I0_3(r3, 22));
    }

    public void A05() {
        C12960it.A0t(this.A05.A02.A00().edit(), "show_request_permission_dialog", false);
        C12960it.A1A(this.A02, 2);
    }

    public void A06() {
        C48112Ej r3 = this.A00;
        r3.A07.Ab2(new RunnableBRunnable0Shape3S0100000_I0_3(r3, 22));
    }

    public void A07(C90924Pt r8) {
        C48112Ej r2 = this.A00;
        if (r2.A00 == 1) {
            r2.A02.removeCallbacks(r2.A08);
        }
        this.A04.A01(r8.A01, this, r8.A02, "device", r8.A00);
    }

    @Override // X.AnonymousClass1P6
    public void ARL(int i) {
        C48112Ej r2 = this.A00;
        if (r2.A00 == 1) {
            int i2 = 7;
            if (i == -1) {
                i2 = 6;
            }
            r2.A00 = i2;
            r2.A02.removeCallbacks(r2.A08);
            r2.A0B(r2.A0C());
        }
        if (i == 4) {
            this.A01.ARK(i);
        }
    }

    @Override // X.AnonymousClass1P6
    public void ARM(C48122Ek r5) {
        C48112Ej r3 = this.A00;
        if (r3.A00 == 1) {
            r3.A02.removeCallbacks(r3.A08);
            r3.A07.Ab2(new RunnableBRunnable0Shape1S0200000_I0_1(r3, 27, r5));
        }
    }
}
