package X;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.mentions.MentionableEntry;
import java.util.Collections;
import java.util.List;

/* renamed from: X.3UW  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3UW implements AbstractC42541vN {
    public List A00;
    public final Activity A01;
    public final C14900mE A02;
    public final C239613r A03;
    public final C15550nR A04;
    public final C15610nY A05;
    public final C15890o4 A06;
    public final AnonymousClass19M A07;
    public final AbstractC14640lm A08;
    public final MentionableEntry A09;
    public final C22190yg A0A;

    public AnonymousClass3UW(Context context, C14900mE r3, C239613r r4, C15550nR r5, C15610nY r6, C15890o4 r7, AnonymousClass19M r8, AbstractC14640lm r9, MentionableEntry mentionableEntry, C22190yg r11) {
        this.A01 = AnonymousClass12P.A00(context);
        this.A03 = r4;
        this.A02 = r3;
        this.A09 = mentionableEntry;
        this.A08 = r9;
        this.A06 = r7;
        this.A0A = r11;
        this.A04 = r5;
        this.A05 = r6;
        this.A07 = r8;
    }

    public final void A00(List list) {
        if (list == null || list.isEmpty()) {
            this.A02.A07(R.string.share_failed, 0);
        } else if (!this.A06.A07()) {
            Activity activity = this.A01;
            int i = Build.VERSION.SDK_INT;
            int i2 = R.string.permission_storage_need_write_access_on_sending_media_v30;
            if (i < 30) {
                i2 = R.string.permission_storage_need_write_access_on_sending_media;
            }
            RequestPermissionActivity.A0L(activity, R.string.permission_storage_need_write_access_on_sending_media_request, i2, 29);
            this.A00 = list;
        } else {
            C239613r r1 = this.A03;
            List singletonList = Collections.singletonList(this.A08);
            Activity activity2 = this.A01;
            r1.A00(activity2, (AbstractC13860kS) activity2, new AnonymousClass531(this), null, "", singletonList, list, 9, false, false);
        }
    }

    @Override // X.AbstractC42541vN
    public boolean ALt(Intent intent, int i, int i2) {
        if (i != 29 || i2 != -1) {
            return false;
        }
        A00(this.A00);
        return true;
    }
}
