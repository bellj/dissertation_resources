package X;

import android.text.TextUtils;
import java.util.Collections;
import java.util.HashSet;
import org.json.JSONObject;

/* renamed from: X.0rR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17820rR implements AbstractC16940q0 {
    public final AbstractC16940q0 A00;

    public C17820rR(AbstractC16940q0 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC16940q0
    public /* bridge */ /* synthetic */ Object A7i(JSONObject jSONObject, long j) {
        C44691zO r2;
        JSONObject optJSONObject;
        JSONObject optJSONObject2;
        HashSet hashSet = new HashSet();
        Collections.addAll(hashSet, "xwa_product_catalog_get_product");
        JSONObject jSONObject2 = null;
        if (AnonymousClass3GH.A01(hashSet, jSONObject) && (optJSONObject = jSONObject.optJSONObject("xwa_product_catalog_get_product")) != null) {
            HashSet hashSet2 = new HashSet();
            Collections.addAll(hashSet2, "product_catalog");
            if (AnonymousClass3GH.A01(hashSet2, optJSONObject) && (optJSONObject2 = optJSONObject.optJSONObject("product_catalog")) != null) {
                HashSet hashSet3 = new HashSet();
                Collections.addAll(hashSet3, "product");
                if (AnonymousClass3GH.A01(hashSet3, optJSONObject2)) {
                    jSONObject2 = optJSONObject2;
                }
            }
        }
        boolean z = false;
        if (jSONObject2 == null) {
            r2 = null;
        } else {
            r2 = (C44691zO) this.A00.A7i(jSONObject2.optJSONObject("product"), j);
            String A00 = AnonymousClass3GH.A00("cart_enabled", jSONObject2);
            if (!TextUtils.isEmpty(A00) && A00.equals("CARTENABLED_TRUE")) {
                z = true;
            }
        }
        return new C90124Mr(r2, z);
    }
}
