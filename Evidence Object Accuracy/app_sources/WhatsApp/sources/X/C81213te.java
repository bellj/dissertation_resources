package X;

import java.io.Serializable;

/* renamed from: X.3te  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C81213te extends AbstractC112285Cu implements Serializable {
    public static final long serialVersionUID = 0;
    public final AbstractC112285Cu forwardOrder;

    public C81213te(AbstractC112285Cu r1) {
        this.forwardOrder = r1;
    }

    @Override // X.AbstractC112285Cu, java.util.Comparator
    public int compare(Object obj, Object obj2) {
        return this.forwardOrder.compare(obj2, obj);
    }

    @Override // java.util.Comparator, java.lang.Object
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof C81213te) {
            return this.forwardOrder.equals(((C81213te) obj).forwardOrder);
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return -this.forwardOrder.hashCode();
    }

    @Override // X.AbstractC112285Cu
    public AbstractC112285Cu reverse() {
        return this.forwardOrder;
    }

    @Override // java.lang.Object
    public String toString() {
        String valueOf = String.valueOf(this.forwardOrder);
        StringBuilder A0t = C12980iv.A0t(valueOf.length() + 10);
        A0t.append(valueOf);
        return C12960it.A0d(".reverse()", A0t);
    }
}
