package X;

/* renamed from: X.1uV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C42001uV extends AbstractC16110oT {
    public Integer A00;

    public C42001uV() {
        super(3044, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        StringBuilder sb = new StringBuilder("WamAdvPrimaryIdentityMissing {");
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "primaryIdentityMissingProtoType", obj);
        sb.append("}");
        return sb.toString();
    }
}
