package X;

import com.facebook.redex.RunnableBRunnable0Shape3S0200000_I0_3;
import com.whatsapp.jid.UserJid;

/* renamed from: X.15L  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass15L {
    public final C22320yt A00;
    public final C16370ot A01;
    public final C16510p9 A02;
    public final C19990v2 A03;
    public final C20850wQ A04;

    public AnonymousClass15L(C22320yt r1, C16370ot r2, C16510p9 r3, C19990v2 r4, C20850wQ r5) {
        this.A02 = r3;
        this.A03 = r4;
        this.A00 = r1;
        this.A01 = r2;
        this.A04 = r5;
    }

    public C30511Xs A00(UserJid userJid) {
        AnonymousClass1PE A06 = this.A03.A06(userJid);
        if (A06 == null) {
            return null;
        }
        long j = A06.A0B;
        if (j == -1) {
            return null;
        }
        AbstractC15340mz A00 = this.A01.A00(j);
        if (!(A00 instanceof AnonymousClass1XB) || ((AnonymousClass1XB) A00).A00 != 28) {
            return null;
        }
        return (C30511Xs) A00;
    }

    public void A01(UserJid userJid) {
        AnonymousClass1PE A06 = this.A03.A06(userJid);
        if (A06 != null) {
            A06.A0B = -1;
            this.A00.A01(new RunnableBRunnable0Shape3S0200000_I0_3(this, 47, A06), 19);
        }
    }
}
