package X;

import java.util.List;

/* renamed from: X.2OM  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2OM extends AnonymousClass1JW {
    public final long A00;
    public final long A01;
    public final AbstractC14640lm A02;
    public final List A03;
    public final List A04;

    public AnonymousClass2OM(AbstractC15710nm r1, C15450nH r2, AbstractC14640lm r3, List list, List list2, long j, long j2) {
        super(r1, r2);
        this.A02 = r3;
        this.A01 = j;
        this.A00 = j2;
        this.A04 = list;
        this.A03 = list2;
    }
}
