package X;

/* renamed from: X.50V  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass50V implements AbstractC116395Vg {
    public AbstractC116395Vg[] A00;

    public AnonymousClass50V(AbstractC116395Vg... r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC116395Vg
    public final boolean Ags(Class cls) {
        for (AbstractC116395Vg r0 : this.A00) {
            if (r0.Ags(cls)) {
                return true;
            }
        }
        return false;
    }

    @Override // X.AbstractC116395Vg
    public final AbstractC115245Qt Ah6(Class cls) {
        AbstractC116395Vg[] r4 = this.A00;
        for (AbstractC116395Vg r1 : r4) {
            if (r1.Ags(cls)) {
                return r1.Ah6(cls);
            }
        }
        throw C12980iv.A0u(C12960it.A0c(cls.getName(), "No factory is available for message type: "));
    }
}
