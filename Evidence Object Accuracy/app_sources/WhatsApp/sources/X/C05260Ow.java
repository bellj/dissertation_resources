package X;

import android.content.Context;
import android.util.Log;
import androidx.work.impl.WorkDatabase;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.Executor;

/* renamed from: X.0Ow  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05260Ow {
    public EnumC03830Jh A00;
    public AbstractC11890h2 A01;
    public ArrayList A02;
    public Set A03;
    public Executor A04;
    public Executor A05;
    public boolean A06;
    public boolean A07;
    public boolean A08;
    public final Context A09;
    public final AnonymousClass0MX A0A;
    public final Class A0B = WorkDatabase.class;
    public final String A0C;

    public C05260Ow(Context context, String str) {
        this.A09 = context;
        this.A0C = str;
        this.A00 = EnumC03830Jh.AUTOMATIC;
        this.A08 = true;
        this.A0A = new AnonymousClass0MX();
    }

    public void A00(AnonymousClass0OL... r10) {
        if (this.A03 == null) {
            this.A03 = new HashSet();
        }
        for (AnonymousClass0OL r2 : r10) {
            this.A03.add(Integer.valueOf(r2.A01));
            this.A03.add(Integer.valueOf(r2.A00));
        }
        AnonymousClass0MX r7 = this.A0A;
        for (AnonymousClass0OL r5 : r10) {
            int i = r5.A01;
            int i2 = r5.A00;
            HashMap hashMap = r7.A00;
            Integer valueOf = Integer.valueOf(i);
            AbstractMap abstractMap = (AbstractMap) hashMap.get(valueOf);
            if (abstractMap == null) {
                abstractMap = new TreeMap();
                hashMap.put(valueOf, abstractMap);
            }
            Integer valueOf2 = Integer.valueOf(i2);
            Object obj = abstractMap.get(valueOf2);
            if (obj != null) {
                StringBuilder sb = new StringBuilder("Overriding migration ");
                sb.append(obj);
                sb.append(" with ");
                sb.append(r5);
                Log.w("ROOM", sb.toString());
            }
            abstractMap.put(valueOf2, r5);
        }
    }
}
