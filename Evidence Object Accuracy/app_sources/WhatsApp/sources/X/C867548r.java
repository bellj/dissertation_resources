package X;

import java.io.IOException;

/* renamed from: X.48r  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C867548r extends IOException {
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C867548r(int r3, int r4) {
        /*
            r2 = this;
            r0 = 108(0x6c, float:1.51E-43)
            java.lang.StringBuilder r1 = X.C12980iv.A0t(r0)
            java.lang.String r0 = "CodedOutputStream was writing to a flat byte array and ran out of space (pos "
            r1.append(r0)
            r1.append(r3)
            java.lang.String r0 = " limit "
            r1.append(r0)
            r1.append(r4)
            java.lang.String r0 = ")."
            java.lang.String r0 = X.C12960it.A0d(r0, r1)
            r2.<init>(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C867548r.<init>(int, int):void");
    }
}
