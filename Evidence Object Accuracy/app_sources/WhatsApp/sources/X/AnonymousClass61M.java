package X;

import android.content.Context;
import android.content.DialogInterface;
import com.whatsapp.R;

/* renamed from: X.61M  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass61M {
    public static AnonymousClass04S A00(Context context, DialogInterface.OnDismissListener onDismissListener, DialogInterface.OnDismissListener onDismissListener2, int i) {
        if (!(i == 8 || i == 9 || i == 444 || i == 478)) {
            if (i != 1448) {
                return null;
            }
            onDismissListener = onDismissListener2;
        }
        return A02(context, onDismissListener, context.getString(R.string.payments_generic_error));
    }

    public static AnonymousClass04S A01(Context context, DialogInterface.OnDismissListener onDismissListener, DialogInterface.OnDismissListener onDismissListener2, String str, int i) {
        int i2;
        String str2;
        int i3;
        if (!(i == 6 || i == 7)) {
            switch (i) {
                case -2:
                    break;
                case 400:
                case 403:
                case 2826001:
                    if (str == null) {
                        str = context.getString(R.string.payments_sender_generic_error);
                    }
                    return A02(context, onDismissListener, str);
                case 443:
                    i2 = R.string.payments_upgrade_error;
                    return A02(context, onDismissListener2, context.getString(i2));
                case 500:
                case 503:
                case 4002:
                case 2826004:
                    i2 = R.string.payments_generic_error;
                    return A02(context, onDismissListener2, context.getString(i2));
                case 10702:
                    i2 = R.string.payments_bank_generic_error;
                    return A02(context, onDismissListener2, context.getString(i2));
                case 2826007:
                    str2 = context.getString(R.string.payments_risk_blocked);
                    i3 = R.string.describe_problem_contact_support;
                    return A03(context, onDismissListener2, context.getString(i3), str2);
                case 2826009:
                    str2 = context.getString(R.string.payments_risk_try_again_later);
                    i3 = R.string.payments_try_again_later;
                    return A03(context, onDismissListener2, context.getString(i3), str2);
                case 2826012:
                    i2 = R.string.payments_risk_method_blocked;
                    return A02(context, onDismissListener2, context.getString(i2));
                default:
                    return null;
            }
        }
        i2 = R.string.no_internet_message;
        return A02(context, onDismissListener2, context.getString(i2));
    }

    public static AnonymousClass04S A02(Context context, DialogInterface.OnDismissListener onDismissListener, String str) {
        C004802e A0S = C12980iv.A0S(context);
        A0S.A0A(str);
        C12970iu.A1I(A0S);
        AnonymousClass04S create = A0S.create();
        create.setOnDismissListener(onDismissListener);
        return create;
    }

    public static AnonymousClass04S A03(Context context, DialogInterface.OnDismissListener onDismissListener, String str, String str2) {
        C004802e A0S = C12980iv.A0S(context);
        A0S.setTitle(str);
        A0S.A0A(str2);
        C12970iu.A1I(A0S);
        AnonymousClass04S create = A0S.create();
        create.setOnDismissListener(onDismissListener);
        return create;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002a, code lost:
        if (r11 != 2826008) goto L_0x002c;
     */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00b9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass04S A04(android.content.Context r5, android.content.DialogInterface.OnDismissListener r6, android.content.DialogInterface.OnDismissListener r7, android.content.DialogInterface.OnDismissListener r8, java.lang.String r9, java.lang.String r10, int r11) {
        /*
            r4 = this;
            r0 = 405(0x195, float:5.68E-43)
            r1 = 2131890531(0x7f121163, float:1.9415756E38)
            if (r11 == r0) goto L_0x00be
            r0 = 406(0x196, float:5.69E-43)
            r1 = 2131890528(0x7f121160, float:1.941575E38)
            if (r11 == r0) goto L_0x00be
            r0 = 409(0x199, float:5.73E-43)
            if (r11 == r0) goto L_0x00bb
            r0 = 410(0x19a, float:5.75E-43)
            r1 = 2131890530(0x7f121162, float:1.9415754E38)
            if (r11 == r0) goto L_0x00be
            r0 = 426(0x1aa, float:5.97E-43)
            r1 = 2131890527(0x7f12115f, float:1.9415748E38)
            if (r11 == r0) goto L_0x00be
            r0 = 460(0x1cc, float:6.45E-43)
            r1 = 2131890532(0x7f121164, float:1.9415758E38)
            if (r11 == r0) goto L_0x00be
            r0 = 2826008(0x2b1f18, float:3.96008E-39)
            if (r11 == r0) goto L_0x00bb
        L_0x002c:
            X.04S r0 = A00(r5, r7, r8, r11)
            if (r0 != 0) goto L_0x0049
            X.04S r0 = A01(r5, r7, r8, r10, r11)
            if (r0 != 0) goto L_0x0049
            r3 = 0
            switch(r11) {
                case 2001: goto L_0x00a9;
                case 2304: goto L_0x00a0;
                case 2321: goto L_0x00a0;
                case 10010: goto L_0x009c;
                case 10775: goto L_0x0093;
                case 12750: goto L_0x0086;
                case 2826003: goto L_0x007b;
                case 2826006: goto L_0x006a;
                case 2826015: goto L_0x0059;
                case 2826018: goto L_0x0055;
                case 2896003: goto L_0x004a;
                case 2896004: goto L_0x004a;
                default: goto L_0x003c;
            }
        L_0x003c:
            r0 = 2131890446(0x7f12110e, float:1.9415584E38)
            java.lang.String r10 = r5.getString(r0)
        L_0x0043:
            if (r3 == 0) goto L_0x00d9
            X.04S r0 = A03(r5, r8, r3, r10)
        L_0x0049:
            return r0
        L_0x004a:
            r0 = 2131888901(0x7f120b05, float:1.941245E38)
            java.lang.String r10 = r5.getString(r0)
            r0 = 2131888902(0x7f120b06, float:1.9412452E38)
            goto L_0x0076
        L_0x0055:
            r0 = 2131890321(0x7f121091, float:1.941533E38)
            goto L_0x005c
        L_0x0059:
            r0 = 2131890320(0x7f121090, float:1.9415328E38)
        L_0x005c:
            if (r10 != 0) goto L_0x0062
            java.lang.String r10 = r5.getString(r0)
        L_0x0062:
            r0 = 2131890741(0x7f121235, float:1.9416182E38)
            java.lang.String r3 = r5.getString(r0)
            goto L_0x009a
        L_0x006a:
            if (r10 != 0) goto L_0x0073
            r0 = 2131890601(0x7f1211a9, float:1.9415898E38)
            java.lang.String r10 = r5.getString(r0)
        L_0x0073:
            r0 = 2131890508(0x7f12114c, float:1.941571E38)
        L_0x0076:
            java.lang.String r3 = r5.getString(r0)
            goto L_0x00a7
        L_0x007b:
            r0 = 2131890412(0x7f1210ec, float:1.9415515E38)
            java.lang.String r10 = r5.getString(r0)
            r0 = 2131890741(0x7f121235, float:1.9416182E38)
            goto L_0x00b3
        L_0x0086:
            r2 = 2131890544(0x7f121170, float:1.9415783E38)
            java.lang.Object[] r1 = X.C12970iu.A1b()
            r0 = 0
            java.lang.String r10 = X.C12960it.A0X(r5, r9, r1, r0, r2)
            goto L_0x00a7
        L_0x0093:
            r0 = 2131890313(0x7f121089, float:1.9415314E38)
            java.lang.String r10 = r5.getString(r0)
        L_0x009a:
            r6 = r8
            goto L_0x00b7
        L_0x009c:
            r0 = 2131890401(0x7f1210e1, float:1.9415493E38)
            goto L_0x00a3
        L_0x00a0:
            r0 = 2131890224(0x7f121030, float:1.9415134E38)
        L_0x00a3:
            java.lang.String r10 = r5.getString(r0)
        L_0x00a7:
            r6 = r7
            goto L_0x00b7
        L_0x00a9:
            r0 = 2131890475(0x7f12112b, float:1.9415643E38)
            java.lang.String r10 = r5.getString(r0)
            r0 = 2131890476(0x7f12112c, float:1.9415645E38)
        L_0x00b3:
            java.lang.String r3 = r5.getString(r0)
        L_0x00b7:
            if (r10 == 0) goto L_0x003c
            r8 = r6
            goto L_0x0043
        L_0x00bb:
            r1 = 2131890529(0x7f121161, float:1.9415752E38)
        L_0x00be:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r1)
            if (r0 == 0) goto L_0x002c
            int r2 = r0.intValue()
            X.AnonymousClass009.A05(r9)
            java.lang.Object[] r1 = X.C12970iu.A1b()
            r0 = 0
            java.lang.String r0 = X.C12960it.A0X(r5, r9, r1, r0, r2)
            X.04S r0 = A02(r5, r7, r0)
            return r0
        L_0x00d9:
            X.04S r0 = A02(r5, r8, r10)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass61M.A04(android.content.Context, android.content.DialogInterface$OnDismissListener, android.content.DialogInterface$OnDismissListener, android.content.DialogInterface$OnDismissListener, java.lang.String, java.lang.String, int):X.04S");
    }
}
