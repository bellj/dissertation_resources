package X;

import com.whatsapp.R;

/* renamed from: X.5nQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123605nQ extends C128405w3 {
    @Override // X.C128405w3
    public int A00(int i) {
        if (i == 403) {
            return R.string.br_virality_payments_not_enabled_description_ineligible_number;
        }
        return super.A00(i);
    }
}
