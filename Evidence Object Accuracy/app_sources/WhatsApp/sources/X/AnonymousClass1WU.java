package X;

/* renamed from: X.1WU  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1WU extends AnonymousClass038 {
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0076  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.lang.Long A0N(java.lang.String r16) {
        /*
            r9 = 10
            r0 = 0
            r10 = r16
            int r8 = r10.length()
            r16 = 0
            if (r8 == 0) goto L_0x0051
            r2 = 0
            char r3 = r10.charAt(r0)
            r0 = 48
            if (r3 >= r0) goto L_0x006a
            r1 = -1
        L_0x0017:
            r14 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            r7 = 1
            if (r1 >= 0) goto L_0x0068
            if (r8 == r7) goto L_0x0051
            r0 = 45
            if (r3 != r0) goto L_0x0063
            r14 = -9223372036854775808
            r2 = 1
        L_0x0028:
            r3 = -256204778801521550(0xfc71c71c71c71c72, double:-2.772000429909333E291)
            r5 = 0
            r12 = -256204778801521550(0xfc71c71c71c71c72, double:-2.772000429909333E291)
        L_0x0034:
            if (r2 >= r8) goto L_0x006f
            int r11 = r2 + 1
            char r0 = r10.charAt(r2)
            int r2 = java.lang.Character.digit(r0, r9)
            if (r2 < 0) goto L_0x0051
            int r0 = (r5 > r12 ? 1 : (r5 == r12 ? 0 : -1))
            if (r0 >= 0) goto L_0x0052
            int r0 = (r12 > r3 ? 1 : (r12 == r3 ? 0 : -1))
            if (r0 != 0) goto L_0x0051
            long r0 = (long) r9
            long r12 = r14 / r0
            int r0 = (r5 > r12 ? 1 : (r5 == r12 ? 0 : -1))
            if (r0 >= 0) goto L_0x0052
        L_0x0051:
            return r16
        L_0x0052:
            long r0 = (long) r9
            long r5 = r5 * r0
            long r3 = (long) r2
            long r1 = r14 + r3
            int r0 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x0051
            long r5 = r5 - r3
            r2 = r11
            r3 = -256204778801521550(0xfc71c71c71c71c72, double:-2.772000429909333E291)
            goto L_0x0034
        L_0x0063:
            r0 = 43
            if (r3 != r0) goto L_0x0051
            r2 = 1
        L_0x0068:
            r7 = 0
            goto L_0x0028
        L_0x006a:
            r1 = 1
            if (r3 != r0) goto L_0x0017
            r1 = 0
            goto L_0x0017
        L_0x006f:
            if (r7 == 0) goto L_0x0076
            java.lang.Long r16 = java.lang.Long.valueOf(r5)
            return r16
        L_0x0076:
            long r0 = -r5
            java.lang.Long r16 = java.lang.Long.valueOf(r0)
            return r16
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1WU.A0N(java.lang.String):java.lang.Long");
    }
}
