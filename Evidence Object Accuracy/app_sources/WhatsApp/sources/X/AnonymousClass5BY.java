package X;

/* renamed from: X.5BY  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5BY implements Cloneable {
    public static final AnonymousClass5BX A03 = new AnonymousClass5BX();
    public int A00;
    public int[] A01;
    public AnonymousClass5BX[] A02;

    public AnonymousClass5BY() {
        this(10);
    }

    public AnonymousClass5BY(int i) {
        int i2 = i << 2;
        int i3 = 4;
        while (true) {
            int i4 = (1 << i3) - 12;
            if (i2 > i4) {
                i3++;
                if (i3 >= 32) {
                    break;
                }
            } else {
                i2 = i4;
                break;
            }
        }
        int i5 = i2 / 4;
        this.A01 = new int[i5];
        this.A02 = new AnonymousClass5BX[i5];
        this.A00 = 0;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v1, resolved type: X.5BX[] */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.lang.Object
    public final /* synthetic */ Object clone() {
        int i = this.A00;
        AnonymousClass5BY r4 = new AnonymousClass5BY(i);
        System.arraycopy(this.A01, 0, r4.A01, 0, i);
        for (int i2 = 0; i2 < i; i2++) {
            AnonymousClass5BX[] r2 = this.A02;
            if (r2[i2] != null) {
                r4.A02[i2] = r2[i2].clone();
            }
        }
        r4.A00 = i;
        return r4;
    }

    @Override // java.lang.Object
    public final boolean equals(Object obj) {
        if (obj != this) {
            if (obj instanceof AnonymousClass5BY) {
                AnonymousClass5BY r9 = (AnonymousClass5BY) obj;
                int i = this.A00;
                if (i == r9.A00) {
                    int[] iArr = this.A01;
                    int[] iArr2 = r9.A01;
                    int i2 = 0;
                    while (true) {
                        if (i2 < i) {
                            if (iArr[i2] != iArr2[i2]) {
                                break;
                            }
                            i2++;
                        } else {
                            AnonymousClass5BX[] r4 = this.A02;
                            AnonymousClass5BX[] r3 = r9.A02;
                            for (int i3 = 0; i3 < i; i3++) {
                                if (r4[i3].equals(r3[i3])) {
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public final int hashCode() {
        int i = 17;
        for (int i2 = 0; i2 < this.A00; i2++) {
            i = C12990iw.A08(this.A02[i2], ((i * 31) + this.A01[i2]) * 31);
        }
        return i;
    }
}
