package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.2Pj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C50392Pj extends AnonymousClass1JW {
    public final UserJid A00;
    public final String A01;

    public C50392Pj(AbstractC15710nm r2, C15450nH r3, UserJid userJid, String str, String str2, int i, boolean z) {
        super(r2, r3);
        this.A05 = 27;
        this.A0O = z;
        this.A0G = str;
        super.A01 = i;
        this.A01 = str2;
        this.A00 = userJid;
    }
}
