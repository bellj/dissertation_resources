package X;

import com.whatsapp.authentication.AppAuthSettingsActivity;
import com.whatsapp.authentication.FingerprintBottomSheet;
import com.whatsapp.util.Log;

/* renamed from: X.2r9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58662r9 extends AbstractC58672rA {
    public final /* synthetic */ AppAuthSettingsActivity A00;

    public C58662r9(AppAuthSettingsActivity appAuthSettingsActivity) {
        this.A00 = appAuthSettingsActivity;
    }

    @Override // X.AnonymousClass4UT
    public void A00() {
        Log.i("AppAuthSettingsActivity/fingerprint-success-animation-end");
        AppAuthSettingsActivity appAuthSettingsActivity = this.A00;
        FingerprintBottomSheet fingerprintBottomSheet = appAuthSettingsActivity.A0A;
        if (fingerprintBottomSheet != null && fingerprintBottomSheet.A0e()) {
            appAuthSettingsActivity.A0A.A1C();
        }
        appAuthSettingsActivity.A05.setChecked(true);
        appAuthSettingsActivity.A2f(true);
    }
}
