package X;

import android.os.Bundle;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.08z  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C019108z implements AnonymousClass05A {
    public final Set A00 = new HashSet();

    public C019108z(AnonymousClass058 r2) {
        r2.A02(this, "androidx.savedstate.Restarter");
    }

    @Override // X.AnonymousClass05A
    public Bundle AbH() {
        Bundle bundle = new Bundle();
        bundle.putStringArrayList("classes_to_restore", new ArrayList<>(this.A00));
        return bundle;
    }
}
