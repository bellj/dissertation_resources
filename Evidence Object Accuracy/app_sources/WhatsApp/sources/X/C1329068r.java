package X;

import com.whatsapp.payments.pin.ui.PinBottomSheetDialogFragment;
import com.whatsapp.util.Log;

/* renamed from: X.68r  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1329068r implements AnonymousClass1FK {
    public final /* synthetic */ int A00;
    public final /* synthetic */ ActivityC13790kL A01;
    public final /* synthetic */ PinBottomSheetDialogFragment A02;
    public final /* synthetic */ C123585nO A03;
    public final /* synthetic */ String A04;

    public C1329068r(ActivityC13790kL r1, PinBottomSheetDialogFragment pinBottomSheetDialogFragment, C123585nO r3, String str, int i) {
        this.A03 = r3;
        this.A00 = i;
        this.A01 = r1;
        this.A02 = pinBottomSheetDialogFragment;
        this.A04 = str;
    }

    @Override // X.AnonymousClass1FK
    public void AV3(C452120p r3) {
        Log.i(C12960it.A0b("PAY: BrazilPaymentCardDetailsViewModel/createCallbackForRemovePaymentMethod/onRequestError. paymentNetworkError: ", r3));
        C117305Zk.A1B(((AbstractC118055bC) this.A03).A01, C12980iv.A0i(), r3);
    }

    @Override // X.AnonymousClass1FK
    public void AVA(C452120p r6) {
        Log.i(C12960it.A0b("PAY: BrazilPaymentCardDetailsViewModel/createCallbackForRemovePaymentMethod/onResponseError. paymentNetworkError: ", r6));
        C123585nO r4 = this.A03;
        String str = this.A04;
        PinBottomSheetDialogFragment pinBottomSheetDialogFragment = this.A02;
        if (!r4.A06(r6, pinBottomSheetDialogFragment, str)) {
            int i = this.A00;
            ActivityC13790kL r1 = this.A01;
            if (i != 1) {
                r1.AaN();
            } else if (pinBottomSheetDialogFragment != null) {
                pinBottomSheetDialogFragment.A1M();
                pinBottomSheetDialogFragment.A1C();
            }
            C117305Zk.A1B(((AbstractC118055bC) r4).A01, C12980iv.A0i(), r6);
        }
    }

    @Override // X.AnonymousClass1FK
    public void AVB(C452220q r7) {
        C123585nO r3 = this.A03;
        int i = this.A00;
        ActivityC13790kL r4 = this.A01;
        PinBottomSheetDialogFragment pinBottomSheetDialogFragment = this.A02;
        if (i != 1) {
            r4.AaN();
        } else if (pinBottomSheetDialogFragment != null) {
            pinBottomSheetDialogFragment.A1C();
        }
        if (this.A04.equals("FB")) {
            C17070qD r0 = r3.A03;
            r0.A03();
            if (r0.A09.A0A().size() < 1) {
                C21860y6 r1 = r3.A01;
                r1.A05(r1.A01("add_card"));
            }
        }
        ((AbstractC118055bC) r3).A02.A0A(0);
    }
}
