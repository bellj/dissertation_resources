package X;

import java.io.DataOutputStream;

/* renamed from: X.57x  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1110557x implements AnonymousClass5XW {
    public final AnonymousClass5XW A00;
    public final DataOutputStream A01;

    public C1110557x(AnonymousClass5XW r1, DataOutputStream dataOutputStream) {
        this.A00 = r1;
        this.A01 = dataOutputStream;
    }

    @Override // X.AnonymousClass5XW
    public boolean AIK() {
        return this.A00.AIK();
    }

    @Override // X.AnonymousClass5XW
    public void AZq(byte[] bArr) {
        this.A00.AZq(bArr);
        this.A01.write(bArr);
    }

    @Override // X.AnonymousClass5XW
    public long AaC() {
        return this.A00.AaC();
    }

    @Override // X.AnonymousClass5XW
    public void AcZ(long j) {
        AZq(new byte[(int) (j - this.A00.position())]);
    }

    @Override // X.AnonymousClass5XW
    public void close() {
        this.A00.close();
        this.A01.close();
    }

    @Override // X.AnonymousClass5XW
    public long position() {
        return this.A00.position();
    }

    @Override // X.AnonymousClass5XW
    public byte readByte() {
        byte readByte = this.A00.readByte();
        this.A01.write(readByte);
        return readByte;
    }

    @Override // X.AnonymousClass5XW
    public int readInt() {
        int readInt = this.A00.readInt();
        this.A01.writeInt(readInt);
        return readInt;
    }

    @Override // X.AnonymousClass5XW
    public long readLong() {
        long readLong = this.A00.readLong();
        this.A01.writeLong(readLong);
        return readLong;
    }

    @Override // X.AnonymousClass5XW
    public short readShort() {
        short readShort = this.A00.readShort();
        this.A01.writeShort(readShort);
        return readShort;
    }
}
