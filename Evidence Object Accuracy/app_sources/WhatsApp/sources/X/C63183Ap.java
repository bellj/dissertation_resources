package X;

import android.content.Context;
import com.whatsapp.util.Log;
import java.util.concurrent.Executor;

/* renamed from: X.3Ap  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63183Ap {
    public static void A00(C16590pI r2, AnonymousClass5WF r3) {
        Context context = r2.A00;
        if (AnonymousClass1UB.A00(context) == 0) {
            C13600jz A01 = new C56282kd(context).A01(new C77803ns(), 1);
            AnonymousClass511 r0 = new AbstractC13640k3() { // from class: X.511
                @Override // X.AbstractC13640k3
                public final void AX4(Object obj) {
                    AnonymousClass5WF r1 = AnonymousClass5WF.this;
                    Log.i("registerphone/smsretriever/onsuccess");
                    r1.AeF();
                }
            };
            Executor executor = C13650k5.A00;
            A01.A06(r0, executor);
            A01.A05(new AbstractC13630k2() { // from class: X.50y
                @Override // X.AbstractC13630k2
                public final void AQA(Exception exc) {
                    AnonymousClass5WF r1 = AnonymousClass5WF.this;
                    Log.e("registerphone/smsretriever/onfailure/ ", exc);
                    r1.AZW();
                }
            }, executor);
            return;
        }
        r3.AZW();
    }
}
