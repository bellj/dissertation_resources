package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.24g  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C460524g implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C99824kv();
    public final C460424f A00;
    public final Integer A01;
    public final String A02;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C460524g(C460424f r1, Integer num, String str) {
        this.A02 = str;
        this.A01 = num;
        this.A00 = r1;
    }

    public /* synthetic */ C460524g(Parcel parcel) {
        Integer valueOf;
        this.A02 = parcel.readString();
        int readInt = parcel.readInt();
        if (readInt == -1) {
            valueOf = null;
        } else {
            valueOf = Integer.valueOf(readInt);
        }
        this.A01 = valueOf;
        this.A00 = (C460424f) parcel.readParcelable(C460424f.class.getClassLoader());
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x008c A[Catch: 1V9 -> 0x00a4, TryCatch #0 {1V9 -> 0x00a4, blocks: (B:3:0x0003, B:5:0x0011, B:6:0x0027, B:8:0x002d, B:11:0x0044, B:12:0x0048, B:15:0x0053, B:17:0x0066, B:19:0x006f, B:20:0x0074, B:22:0x007c, B:23:0x0081, B:24:0x0086, B:26:0x008c, B:27:0x0095, B:29:0x009b), top: B:35:0x0003 }] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x009b A[Catch: 1V9 -> 0x00a4, TryCatch #0 {1V9 -> 0x00a4, blocks: (B:3:0x0003, B:5:0x0011, B:6:0x0027, B:8:0x002d, B:11:0x0044, B:12:0x0048, B:15:0x0053, B:17:0x0066, B:19:0x006f, B:20:0x0074, B:22:0x007c, B:23:0x0081, B:24:0x0086, B:26:0x008c, B:27:0x0095, B:29:0x009b), top: B:35:0x0003 }] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00a1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C460524g A00(X.AnonymousClass1V8 r9) {
        /*
            java.lang.String r5 = "kyc-rejection-code"
            r2 = 0
            java.lang.String r0 = "kyc-state"
            java.lang.String r4 = r9.A0I(r0, r2)     // Catch: 1V9 -> 0x00a4
            java.lang.String r0 = "kyc-actions-requested"
            X.1V8 r3 = r9.A0E(r0)     // Catch: 1V9 -> 0x00a4
            if (r3 == 0) goto L_0x0064
            java.util.ArrayList r6 = new java.util.ArrayList     // Catch: 1V9 -> 0x00a4
            r6.<init>()     // Catch: 1V9 -> 0x00a4
            java.lang.String r0 = "obligation"
            java.lang.String r1 = r3.A0H(r0)     // Catch: 1V9 -> 0x00a4
            java.lang.String r0 = "upload-document"
            java.util.List r0 = r3.A0J(r0)     // Catch: 1V9 -> 0x00a4
            java.util.Iterator r8 = r0.iterator()     // Catch: 1V9 -> 0x00a4
        L_0x0027:
            boolean r0 = r8.hasNext()     // Catch: 1V9 -> 0x00a4
            if (r0 == 0) goto L_0x0066
            java.lang.Object r7 = r8.next()     // Catch: 1V9 -> 0x00a4
            X.1V8 r7 = (X.AnonymousClass1V8) r7     // Catch: 1V9 -> 0x00a4
            java.lang.String r0 = "type"
            java.lang.String r7 = r7.A0H(r0)     // Catch: 1V9 -> 0x00a4
            java.lang.String r0 = "PROOF_OF_IDENTITY"
            boolean r0 = r0.equals(r7)     // Catch: 1V9 -> 0x00a4
            if (r0 == 0) goto L_0x0048
            java.lang.String r0 = "UPLOAD_DOC_IDENTITY"
        L_0x0044:
            r6.add(r0)     // Catch: 1V9 -> 0x00a4
            goto L_0x0027
        L_0x0048:
            java.lang.String r0 = "PROOF_OF_ADDRESS"
            boolean r0 = r0.equals(r7)     // Catch: 1V9 -> 0x00a4
            if (r0 == 0) goto L_0x0053
            java.lang.String r0 = "UPLOAD_DOC_ADDRESS"
            goto L_0x0044
        L_0x0053:
            java.lang.String r1 = "PAY: KycActionsRequested/fromProtocolTreeNode non-supported action type: "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch: 1V9 -> 0x00a4
            r0.<init>(r1)     // Catch: 1V9 -> 0x00a4
            r0.append(r7)     // Catch: 1V9 -> 0x00a4
            java.lang.String r0 = r0.toString()     // Catch: 1V9 -> 0x00a4
            com.whatsapp.util.Log.e(r0)     // Catch: 1V9 -> 0x00a4
        L_0x0064:
            r3 = r2
            goto L_0x0086
        L_0x0066:
            java.lang.String r0 = "verify-card"
            X.1V8 r0 = r3.A0E(r0)     // Catch: 1V9 -> 0x00a4
            if (r0 == 0) goto L_0x0074
            java.lang.String r0 = "VERIFY_CARD"
            r6.add(r0)     // Catch: 1V9 -> 0x00a4
        L_0x0074:
            java.lang.String r0 = "provide-ssn-last4"
            X.1V8 r0 = r3.A0E(r0)     // Catch: 1V9 -> 0x00a4
            if (r0 == 0) goto L_0x0081
            java.lang.String r0 = "PROVIDE_SSN_LAST4"
            r6.add(r0)     // Catch: 1V9 -> 0x00a4
        L_0x0081:
            X.24f r3 = new X.24f     // Catch: 1V9 -> 0x00a4
            r3.<init>(r1, r6)     // Catch: 1V9 -> 0x00a4
        L_0x0086:
            java.lang.String r0 = r9.A0I(r5, r2)     // Catch: 1V9 -> 0x00a4
            if (r0 == 0) goto L_0x00a1
            r0 = 0
            int r0 = r9.A05(r5, r0)     // Catch: 1V9 -> 0x00a4
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)     // Catch: 1V9 -> 0x00a4
        L_0x0095:
            boolean r0 = X.AnonymousClass1US.A0C(r4)     // Catch: 1V9 -> 0x00a4
            if (r0 != 0) goto L_0x00aa
            X.24g r0 = new X.24g     // Catch: 1V9 -> 0x00a4
            r0.<init>(r3, r1, r4)     // Catch: 1V9 -> 0x00a4
            goto L_0x00a3
        L_0x00a1:
            r1 = r2
            goto L_0x0095
        L_0x00a3:
            return r0
        L_0x00a4:
            r1 = move-exception
            java.lang.String r0 = "PAY: PaymentKycInfo/fromProtocolTreeNode "
            com.whatsapp.util.Log.e(r0, r1)
        L_0x00aa:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C460524g.A00(X.1V8):X.24g");
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        int i2;
        parcel.writeString(this.A02);
        Integer num = this.A01;
        if (num != null) {
            i2 = num.intValue();
        } else {
            i2 = -1;
        }
        parcel.writeInt(i2);
        parcel.writeParcelable(this.A00, i);
    }
}
