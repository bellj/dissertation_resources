package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaImageButton;
import com.whatsapp.WaImageView;
import com.whatsapp.WaTextView;

/* renamed from: X.40b  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C848540b extends AbstractC848840e {
    public C848540b(View view) {
        super(view);
    }

    @Override // X.AbstractC37191le
    public /* bridge */ /* synthetic */ void A09(Object obj) {
        int i;
        WaImageButton waImageButton;
        AnonymousClass40G r6 = (AnonymousClass40G) obj;
        super.A0A(r6);
        WaImageView waImageView = ((AbstractC848840e) this).A01;
        View view = this.A0H;
        waImageView.setImageDrawable(AnonymousClass2GE.A00(view.getContext(), R.drawable.ic_location_nearby));
        ((AbstractC848840e) this).A03.setText(R.string.biz_user_current_location);
        boolean z = r6.A00;
        WaTextView waTextView = ((AbstractC848840e) this).A02;
        if (z) {
            waTextView.setText(R.string.biz_user_current_location_imprecise_disclaimer);
            i = 0;
            waTextView.setVisibility(0);
            waImageButton = ((AbstractC848840e) this).A00;
            waImageButton.setImageDrawable(AnonymousClass2GE.A00(view.getContext(), R.drawable.ic_action_info));
        } else {
            i = 8;
            waTextView.setVisibility(8);
            waImageButton = ((AbstractC848840e) this).A00;
        }
        waImageButton.setVisibility(i);
    }
}
