package X;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;
import com.whatsapp.util.Log;
import java.util.Random;

/* renamed from: X.2CH  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2CH implements AbstractC41131sx {
    public String A00;
    public final C14900mE A01;
    public final C16590pI A02;
    public final C22420z3 A03;
    public final C22230yk A04;
    public final AnonymousClass27W A05;
    public final C14890mD A06;
    public final AnonymousClass1UU A07 = new AnonymousClass2CG(this);
    public final C14860mA A08;

    @Override // X.AbstractC41131sx
    public void A5V() {
    }

    @Override // X.AbstractC41131sx
    public AnonymousClass1JM ACX() {
        return null;
    }

    public AnonymousClass2CH(C14900mE r2, C16590pI r3, C22420z3 r4, C22230yk r5, AnonymousClass27W r6, C14890mD r7, C14860mA r8) {
        this.A01 = r2;
        this.A02 = r3;
        this.A06 = r7;
        this.A08 = r8;
        this.A04 = r5;
        this.A03 = r4;
        this.A05 = r6;
    }

    @Override // X.AbstractC41131sx
    public void AI4(String str) {
        AnonymousClass1PA A01;
        C32051bX r7;
        String str2;
        C14860mA r4 = this.A08;
        String[] split = str.split(",");
        if (split.length < 3) {
            str2 = "qrsession/processQR/error/invalid_code parts";
        } else {
            byte[] bArr = new byte[64];
            r4.A03().nextBytes(bArr);
            C91714Su r3 = new C91714Su(split[0], split[1], split[2], bArr);
            if (this.A06.A02()) {
                this.A04.A0I(false, false);
            }
            String str3 = r3.A01;
            byte[] bArr2 = r3.A04;
            C31291aJ A00 = C15940oB.A00();
            try {
                A01 = C15940oB.A01(C33991fP.A00(new byte[]{5}, Base64.decode(str3, 0)));
                r7 = A00.A00;
            } catch (AnonymousClass1RY | IllegalArgumentException e) {
                Log.e("qrsession/encryptSecret/curve error ", e);
            }
            if (r7.A00 == 5) {
                byte[] A002 = C32891cu.A00(C32001bS.A00().A03(A01.A01, r7.A01), null, 80);
                byte[] bArr3 = new byte[32];
                System.arraycopy(A002, 0, bArr3, 0, 32);
                byte[] bArr4 = new byte[32];
                System.arraycopy(A002, 32, bArr4, 0, 32);
                byte[] bArr5 = new byte[16];
                System.arraycopy(A002, 64, bArr5, 0, 16);
                byte[] A02 = C33991fP.A02(bArr3, bArr5, bArr2);
                byte[] bArr6 = A00.A01.A01;
                if (A02 == null) {
                    str2 = "qrsession/encryptSecret fail null enc: true";
                } else {
                    byte[] A012 = C33991fP.A01(bArr4, C33991fP.A00(bArr6, A02));
                    if (A012 == null) {
                        str2 = "qrsession/encryptSecret fail null hmac: true";
                    } else {
                        String encodeToString = Base64.encodeToString(C33991fP.A00(bArr6, C33991fP.A00(A012, A02)), 2);
                        if (encodeToString != null) {
                            C22420z3 r72 = this.A03;
                            String str4 = r3.A02;
                            String str5 = r3.A03;
                            String str6 = r3.A00;
                            r72.A02(str4, str5, str6, encodeToString, 0);
                            Context applicationContext = this.A02.A00.getApplicationContext();
                            C14820m6 r0 = r4.A0J;
                            AnonymousClass009.A05(str6);
                            SharedPreferences sharedPreferences = r0.A00;
                            String string = sharedPreferences.getString("web_session_verification_browser_ids", null);
                            if (string != null) {
                                StringBuilder sb = new StringBuilder();
                                sb.append(string);
                                sb.append(",");
                                sb.append(str6);
                                str6 = sb.toString();
                            }
                            sharedPreferences.edit().putString("web_session_verification_browser_ids", str6).apply();
                            if (sharedPreferences.getLong("web_session_verification_when_millis", -1) < 0) {
                                long currentTimeMillis = System.currentTimeMillis() + ((long) (new Random().nextDouble() * 1.08E7d)) + 3600000;
                                if (currentTimeMillis >= 0) {
                                    sharedPreferences.edit().putLong("web_session_verification_when_millis", currentTimeMillis).apply();
                                    r4.A0G(applicationContext, currentTimeMillis);
                                } else {
                                    throw new IllegalArgumentException("When millis cannot be less than 0");
                                }
                            }
                            this.A00 = str6;
                            this.A05.AP6();
                            return;
                        }
                        this.A05.ART();
                    }
                }
            } else {
                throw new AssertionError("PublicKey or PrivateKey type is invalid");
            }
        }
        Log.e(str2);
        this.A05.ART();
    }
}
