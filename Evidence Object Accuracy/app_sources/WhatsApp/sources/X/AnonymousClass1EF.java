package X;

import android.content.ContentValues;

/* renamed from: X.1EF  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1EF {
    public final C16490p7 A00;

    public AnonymousClass1EF(C16490p7 r1) {
        this.A00 = r1;
    }

    public static void A00(C16310on r3, AnonymousClass1X9 r4, long j) {
        ContentValues contentValues = new ContentValues(3);
        contentValues.put("message_add_on_row_id", Long.valueOf(j));
        contentValues.put("reaction", r4.A01);
        contentValues.put("sender_timestamp", Long.valueOf(r4.A00));
        r3.A03.A03(contentValues, "message_add_on_reaction");
    }
}
