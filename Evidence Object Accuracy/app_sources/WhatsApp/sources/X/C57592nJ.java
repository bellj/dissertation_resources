package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2nJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57592nJ extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57592nJ A09;
    public static volatile AnonymousClass255 A0A;
    public int A00;
    public int A01 = -1;
    public int A02 = -1;
    public long A03;
    public long A04;
    public AbstractC27881Jp A05;
    public AbstractC27881Jp A06;
    public AbstractC41941uP A07;
    public AbstractC41941uP A08;

    static {
        C57592nJ r0 = new C57592nJ();
        A09 = r0;
        r0.A0W();
    }

    public C57592nJ() {
        AbstractC27881Jp r1 = AbstractC27881Jp.A01;
        this.A06 = r1;
        C56822m0 r0 = C56822m0.A02;
        this.A08 = r0;
        this.A05 = r1;
        this.A07 = r0;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r12, Object obj, Object obj2) {
        AbstractC41941uP r2;
        int A02;
        int A04;
        switch (r12.ordinal()) {
            case 0:
                return A09;
            case 1:
                AbstractC462925h r4 = (AbstractC462925h) obj;
                C57592nJ r14 = (C57592nJ) obj2;
                this.A06 = r4.Afm(this.A06, r14.A06, C12960it.A1R(this.A00), C12960it.A1R(r14.A00));
                this.A04 = r4.Afs(this.A04, r14.A04, C12960it.A1V(this.A00 & 2, 2), C12960it.A1V(r14.A00 & 2, 2));
                this.A08 = r4.Afq(this.A08, r14.A08);
                this.A05 = r4.Afm(this.A05, r14.A05, C12960it.A1V(this.A00 & 4, 4), C12960it.A1V(r14.A00 & 4, 4));
                this.A03 = r4.Afs(this.A03, r14.A03, C12960it.A1V(this.A00 & 8, 8), C12960it.A1V(r14.A00 & 8, 8));
                this.A07 = r4.Afq(this.A07, r14.A07);
                if (r4 == C463025i.A00) {
                    this.A00 |= r14.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r42 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        try {
                            int A03 = r42.A03();
                            if (A03 == 0) {
                                break;
                            } else if (A03 == 10) {
                                this.A00 |= 1;
                                this.A06 = r42.A08();
                            } else if (A03 != 16) {
                                if (A03 != 24) {
                                    if (A03 == 26) {
                                        A04 = r42.A04(r42.A02());
                                        AbstractC41941uP r1 = this.A08;
                                        if (!((AnonymousClass1K7) r1).A00 && r42.A00() > 0) {
                                            this.A08 = AbstractC27091Fz.A0F(r1);
                                        }
                                        while (r42.A00() > 0) {
                                            C56822m0 r22 = (C56822m0) this.A08;
                                            r22.A02(r22.A00, r42.A02());
                                        }
                                    } else if (A03 == 66) {
                                        this.A00 |= 4;
                                        this.A05 = r42.A08();
                                    } else if (A03 == 72) {
                                        this.A00 |= 8;
                                        this.A03 = r42.A06();
                                    } else if (A03 == 80) {
                                        r2 = this.A07;
                                        if (!((AnonymousClass1K7) r2).A00) {
                                            r2 = AbstractC27091Fz.A0F(r2);
                                            this.A07 = r2;
                                        }
                                        A02 = r42.A02();
                                    } else if (A03 == 82) {
                                        A04 = r42.A04(r42.A02());
                                        AbstractC41941uP r13 = this.A07;
                                        if (!((AnonymousClass1K7) r13).A00 && r42.A00() > 0) {
                                            this.A07 = AbstractC27091Fz.A0F(r13);
                                        }
                                        while (r42.A00() > 0) {
                                            C56822m0 r23 = (C56822m0) this.A07;
                                            r23.A02(r23.A00, r42.A02());
                                        }
                                    } else if (!A0a(r42, A03)) {
                                        break;
                                    }
                                    r42.A03 = A04;
                                    r42.A0B();
                                } else {
                                    r2 = this.A08;
                                    if (!((AnonymousClass1K7) r2).A00) {
                                        r2 = AbstractC27091Fz.A0F(r2);
                                        this.A08 = r2;
                                    }
                                    A02 = r42.A02();
                                }
                                C56822m0 r24 = (C56822m0) r2;
                                r24.A02(r24.A00, A02);
                            } else {
                                this.A00 |= 2;
                                this.A04 = r42.A06();
                            }
                        } catch (IOException e) {
                            throw AbstractC27091Fz.A0K(this, e);
                        }
                    } catch (C28971Pt e2) {
                        throw AbstractC27091Fz.A0J(e2, this);
                    }
                }
                break;
            case 3:
                ((AnonymousClass1K7) this.A08).A00 = false;
                ((AnonymousClass1K7) this.A07).A00 = false;
                return null;
            case 4:
                return new C57592nJ();
            case 5:
                return new C81623uJ();
            case 6:
                break;
            case 7:
                if (A0A == null) {
                    synchronized (C57592nJ.class) {
                        if (A0A == null) {
                            A0A = AbstractC27091Fz.A09(A09);
                        }
                    }
                }
                return A0A;
            default:
                throw C12970iu.A0z();
        }
        return A09;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i;
        int i2;
        int i3;
        int i4 = ((AbstractC27091Fz) this).A00;
        if (i4 != -1) {
            return i4;
        }
        int i5 = this.A00;
        if ((i5 & 1) == 1) {
            i = CodedOutputStream.A09(this.A06, 1) + 0;
        } else {
            i = 0;
        }
        if ((i5 & 2) == 2) {
            i += CodedOutputStream.A06(2, this.A04);
        }
        int i6 = 0;
        for (int i7 = 0; i7 < this.A08.size(); i7++) {
            C56822m0 r0 = (C56822m0) this.A08;
            r0.A01(i7);
            i6 += CodedOutputStream.A01(r0.A01[i7]);
        }
        int i8 = i + i6;
        if (!this.A08.isEmpty()) {
            int i9 = i8 + 1;
            if (i6 >= 0) {
                i3 = CodedOutputStream.A01(i6);
            } else {
                i3 = 10;
            }
            i8 = i9 + i3;
        }
        this.A02 = i6;
        int i10 = this.A00;
        if ((i10 & 4) == 4) {
            i8 = AbstractC27091Fz.A05(this.A05, 8, i8);
        }
        if ((i10 & 8) == 8) {
            i8 += CodedOutputStream.A06(9, this.A03);
        }
        int i11 = 0;
        for (int i12 = 0; i12 < this.A07.size(); i12++) {
            C56822m0 r02 = (C56822m0) this.A07;
            r02.A01(i12);
            i11 += CodedOutputStream.A01(r02.A01[i12]);
        }
        int i13 = i8 + i11;
        if (!this.A07.isEmpty()) {
            int i14 = i13 + 1;
            if (i11 >= 0) {
                i2 = CodedOutputStream.A01(i11);
            } else {
                i2 = 10;
            }
            i13 = i14 + i2;
        }
        this.A01 = i11;
        return AbstractC27091Fz.A07(this, i13);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        AGd();
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0K(this.A06, 1);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0H(2, this.A04);
        }
        if (this.A08.size() > 0) {
            codedOutputStream.A0C(26);
            codedOutputStream.A0C(this.A02);
        }
        for (int i = 0; i < this.A08.size(); i++) {
            C56822m0 r0 = (C56822m0) this.A08;
            r0.A01(i);
            codedOutputStream.A0C(r0.A01[i]);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0K(this.A05, 8);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0H(9, this.A03);
        }
        if (this.A07.size() > 0) {
            codedOutputStream.A0C(82);
            codedOutputStream.A0C(this.A01);
        }
        for (int i2 = 0; i2 < this.A07.size(); i2++) {
            C56822m0 r02 = (C56822m0) this.A07;
            r02.A01(i2);
            codedOutputStream.A0C(r02.A01[i2]);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
