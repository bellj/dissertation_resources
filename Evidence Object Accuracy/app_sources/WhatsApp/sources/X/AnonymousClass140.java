package X;

import android.content.ContentValues;
import android.database.Cursor;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.140  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass140 {
    public final C240413z A00;

    public AnonymousClass140(C240413z r1) {
        this.A00 = r1;
    }

    public Set A00() {
        HashSet hashSet = new HashSet();
        C16310on A01 = this.A00.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT pack_id FROM unseen_sticker_packs", null);
            int columnIndexOrThrow = A09.getColumnIndexOrThrow("pack_id");
            while (A09.moveToNext()) {
                hashSet.add(A09.getString(columnIndexOrThrow));
            }
            A09.close();
            A01.close();
            return hashSet;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A01(String str) {
        C16310on A02 = this.A00.A02();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("pack_id", str);
            A02.A03.A06(contentValues, "unseen_sticker_packs", 5);
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
