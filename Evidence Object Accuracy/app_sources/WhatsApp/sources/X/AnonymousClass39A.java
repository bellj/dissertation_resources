package X;

/* renamed from: X.39A  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass39A extends AbstractC107844y2 {
    public final C16590pI A00;
    public final AnonymousClass237 A01;
    public final AnonymousClass1KC A02;

    public AnonymousClass39A(C16590pI r2, AnonymousClass109 r3, AnonymousClass1X4 r4) {
        this.A00 = r2;
        this.A02 = r3.A01(r4);
        this.A01 = null;
    }

    public AnonymousClass39A(C16590pI r2, AnonymousClass237 r3, AnonymousClass109 r4, AnonymousClass1X4 r5) {
        this.A00 = r2;
        this.A02 = r4.A01(r5);
        this.A01 = r3;
    }

    @Override // X.AbstractC47452At
    public AnonymousClass2BW A8D() {
        AnonymousClass1KC r3 = this.A02;
        C16590pI r0 = this.A00;
        if (r3 != null) {
            return new AnonymousClass300(r0.A00, this.A01, r3);
        }
        return new C56112kL(r0.A00);
    }
}
