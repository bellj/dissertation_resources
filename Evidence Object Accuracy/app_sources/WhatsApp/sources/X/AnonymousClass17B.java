package X;

import java.security.PublicKey;
import java.security.cert.X509Certificate;

/* renamed from: X.17B  reason: invalid class name */
/* loaded from: classes2.dex */
public interface AnonymousClass17B {
    void AZD(AnonymousClass3EB v, AnonymousClass4VZ v2, Integer num, PublicKey publicKey, X509Certificate x509Certificate);

    void AZF(C64063Ec v, AnonymousClass3EB v2, AnonymousClass4VZ v3, Integer num, PublicKey publicKey, X509Certificate x509Certificate);

    void AZG(C64063Ec v, AnonymousClass3EB v2, AnonymousClass4VZ v3, Integer num, PublicKey publicKey, X509Certificate x509Certificate);
}
