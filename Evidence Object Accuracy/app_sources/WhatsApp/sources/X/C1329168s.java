package X;

import android.content.DialogInterface;
import com.facebook.redex.IDxDListenerShape14S0100000_3_I1;
import com.whatsapp.R;
import com.whatsapp.payments.ui.ReTosFragment;

/* renamed from: X.68s  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1329168s implements AnonymousClass1FK {
    public final /* synthetic */ ReTosFragment A00;

    public C1329168s(ReTosFragment reTosFragment) {
        this.A00 = reTosFragment;
    }

    public final void A00(C452120p r7) {
        ReTosFragment reTosFragment = this.A00;
        reTosFragment.A1G(true);
        reTosFragment.A00.setVisibility(0);
        reTosFragment.A01.setVisibility(8);
        ActivityC000900k A0B = reTosFragment.A0B();
        if (A0B != null) {
            IDxDListenerShape14S0100000_3_I1 iDxDListenerShape14S0100000_3_I1 = new IDxDListenerShape14S0100000_3_I1(this, 22);
            DialogInterface$OnDismissListenerC1314362t r2 = new DialogInterface.OnDismissListener() { // from class: X.62t
                @Override // android.content.DialogInterface.OnDismissListener
                public final void onDismiss(DialogInterface dialogInterface) {
                }
            };
            int i = r7.A00;
            AnonymousClass04S A01 = AnonymousClass61M.A01(A0B, iDxDListenerShape14S0100000_3_I1, r2, reTosFragment.A03.A00(i), i);
            if (A01 == null) {
                A01 = AnonymousClass61M.A02(A0B, r2, reTosFragment.A0I(R.string.payments_generic_error));
            }
            A01.show();
        }
    }

    @Override // X.AnonymousClass1FK
    public void AV3(C452120p r1) {
        A00(r1);
    }

    @Override // X.AnonymousClass1FK
    public void AVA(C452120p r1) {
        A00(r1);
    }

    @Override // X.AnonymousClass1FK
    public void AVB(C452220q r5) {
        ReTosFragment reTosFragment = this.A00;
        C30931Zj r2 = reTosFragment.A07;
        StringBuilder A0k = C12960it.A0k("accept-tos/result=");
        A0k.append(r5.A02);
        C117295Zj.A1F(r2, A0k);
        if (r5.A02) {
            reTosFragment.A1G(true);
            reTosFragment.A00.setVisibility(0);
            reTosFragment.A01.setVisibility(8);
            reTosFragment.A1B();
            return;
        }
        A00(new C452120p(0));
    }
}
