package X;

import android.content.ComponentCallbacks;
import android.content.res.Configuration;

/* renamed from: X.0V1  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0V1 implements ComponentCallbacks {
    public final /* synthetic */ AnonymousClass04L A00;

    @Override // android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
    }

    public AnonymousClass0V1(AnonymousClass04L r1) {
        this.A00 = r1;
    }

    @Override // android.content.ComponentCallbacks
    public void onLowMemory() {
        this.A00.A05();
    }
}
