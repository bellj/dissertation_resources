package X;

/* renamed from: X.2PX  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2PX {
    public static void A00(String str, Object[] objArr, boolean z) {
        if (!z) {
            throw new IllegalStateException(String.format(str, objArr));
        }
    }

    public static void A01(boolean z) {
        A00("onAttach called multiple times with different Context! Hilt Fragments should not be retained.", new Object[0], z);
    }
}
