package X;

import android.text.TextUtils;
import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

/* renamed from: X.1cg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C32751cg {
    public static final ThreadLocal A00 = new AnonymousClass5HI();
    public static final ThreadLocal A01 = new AnonymousClass5HH();
    public static final Pattern A02 = Pattern.compile("[\\p{ASCII}&&[^\\p{Alnum}]]");

    public static AnonymousClass01T A00(AnonymousClass018 r8, String str, boolean z) {
        String A08;
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        if (!TextUtils.isEmpty(str)) {
            String replaceAll = A02.matcher(str).replaceAll(" ");
            BreakIterator A012 = A01(r8);
            A012.setText(replaceAll);
            int first = A012.first();
            while (true) {
                int next = A012.next();
                first = next;
                if (next == -1) {
                    break;
                }
                String substring = replaceAll.substring(first, next);
                if (z) {
                    A08 = AnonymousClass1US.A07(substring);
                } else {
                    A08 = AnonymousClass1US.A08(substring);
                }
                if (!TextUtils.isEmpty(A08) && !Character.isSpaceChar(A08.codePointAt(0))) {
                    arrayList2.add(Integer.valueOf(first));
                    arrayList2.add(Integer.valueOf(next));
                    arrayList.add(A08);
                }
            }
        }
        return new AnonymousClass01T(arrayList2, arrayList);
    }

    public static BreakIterator A01(AnonymousClass018 r3) {
        Locale A002 = AnonymousClass018.A00(r3.A00);
        ThreadLocal threadLocal = A01;
        if (A002.equals(threadLocal.get())) {
            return (BreakIterator) A00.get();
        }
        BreakIterator wordInstance = BreakIterator.getWordInstance(A002);
        threadLocal.set(A002);
        A00.set(wordInstance);
        return wordInstance;
    }

    public static ArrayList A02(AnonymousClass018 r1, String str) {
        Object obj = A00(r1, str, true).A01;
        AnonymousClass009.A05(obj);
        return (ArrayList) obj;
    }

    public static boolean A03(AnonymousClass018 r9, String str, List list, boolean z) {
        boolean equals;
        if (str == null) {
            return false;
        }
        String replaceAll = A02.matcher(str).replaceAll(" ");
        Iterator it = list.iterator();
        while (it.hasNext()) {
            String str2 = (String) it.next();
            BreakIterator A012 = A01(r9);
            A012.setText(replaceAll);
            int first = A012.first();
            do {
                int next = A012.next();
                first = next;
                if (next == -1) {
                    return false;
                }
                String A07 = AnonymousClass1US.A07(replaceAll.substring(first, next));
                if (z) {
                    equals = A07.startsWith(str2);
                    continue;
                } else {
                    equals = A07.equals(str2);
                    continue;
                }
            } while (!equals);
        }
        return true;
    }
}
