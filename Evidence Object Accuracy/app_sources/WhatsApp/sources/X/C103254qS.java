package X;

import android.content.Context;
import com.whatsapp.profile.CapturePhoto;

/* renamed from: X.4qS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103254qS implements AbstractC009204q {
    public final /* synthetic */ CapturePhoto A00;

    public C103254qS(CapturePhoto capturePhoto) {
        this.A00 = capturePhoto;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        CapturePhoto capturePhoto = this.A00;
        if (!capturePhoto.A03) {
            capturePhoto.A03 = true;
            AnonymousClass01J r1 = ((AnonymousClass2FL) ((AnonymousClass2FJ) capturePhoto.generatedComponent())).A1E;
            capturePhoto.A00 = (C14900mE) r1.A8X.get();
            capturePhoto.A02 = C12960it.A0R(r1);
            capturePhoto.A01 = (C15890o4) r1.AN1.get();
        }
    }
}
