package X;

import java.nio.ByteBuffer;

/* renamed from: X.4bY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC94284bY {
    public static void A00(CharSequence charSequence, ByteBuffer byteBuffer) {
        int length = charSequence.length();
        int position = byteBuffer.position();
        int i = 0;
        while (i < length) {
            try {
                char charAt = charSequence.charAt(i);
                if (charAt >= 128) {
                    break;
                }
                byteBuffer.put(position + i, (byte) charAt);
                i++;
            } catch (IndexOutOfBoundsException unused) {
                char charAt2 = charSequence.charAt(i);
                StringBuilder A0t = C12980iv.A0t(37);
                A0t.append("Failed writing ");
                A0t.append(charAt2);
                throw new ArrayIndexOutOfBoundsException(C12960it.A0e(" at index ", A0t, byteBuffer.position() + Math.max(i, (position - byteBuffer.position()) + 1)));
            }
        }
        if (i == length) {
            byteBuffer.position(position + i);
            return;
        }
        position += i;
        while (i < length) {
            char charAt3 = charSequence.charAt(i);
            char c = charAt3;
            if (charAt3 >= 128) {
                if (charAt3 < 2048) {
                    int i2 = position + 1;
                    try {
                        byteBuffer.put(position, (byte) ((charAt3 >>> 6) | 192));
                        byteBuffer.put(i2, (byte) ((charAt3 & '?') | 128));
                        position = i2;
                    } catch (IndexOutOfBoundsException unused2) {
                        position = i2;
                        char charAt2 = charSequence.charAt(i);
                        StringBuilder A0t = C12980iv.A0t(37);
                        A0t.append("Failed writing ");
                        A0t.append(charAt2);
                        throw new ArrayIndexOutOfBoundsException(C12960it.A0e(" at index ", A0t, byteBuffer.position() + Math.max(i, (position - byteBuffer.position()) + 1)));
                    }
                } else if (charAt3 < 55296 || 57343 < charAt3) {
                    int i3 = position + 1;
                    byteBuffer.put(position, (byte) ((charAt3 >>> '\f') | 224));
                    position = i3 + 1;
                    byteBuffer.put(i3, (byte) (((charAt3 >>> 6) & 63) | 128));
                    c = (charAt3 & '?') | 128;
                } else {
                    int i4 = i + 1;
                    if (i4 != length) {
                        try {
                            char charAt4 = charSequence.charAt(i4);
                            if (Character.isSurrogatePair(charAt3, charAt4)) {
                                int codePoint = Character.toCodePoint(charAt3, charAt4);
                                int i5 = position + 1;
                                try {
                                    byteBuffer.put(position, (byte) ((codePoint >>> 18) | 240));
                                    int i6 = i5 + 1;
                                    byteBuffer.put(i5, (byte) (((codePoint >>> 12) & 63) | 128));
                                    int i7 = i6 + 1;
                                    byteBuffer.put(i6, (byte) (((codePoint >>> 6) & 63) | 128));
                                    byteBuffer.put(i7, (byte) ((codePoint & 63) | 128));
                                    position = i7;
                                    i = i4;
                                } catch (IndexOutOfBoundsException unused3) {
                                    position = i5;
                                    i = i4;
                                    char charAt2 = charSequence.charAt(i);
                                    StringBuilder A0t = C12980iv.A0t(37);
                                    A0t.append("Failed writing ");
                                    A0t.append(charAt2);
                                    throw new ArrayIndexOutOfBoundsException(C12960it.A0e(" at index ", A0t, byteBuffer.position() + Math.max(i, (position - byteBuffer.position()) + 1)));
                                }
                            } else {
                                i = i4;
                            }
                        } catch (IndexOutOfBoundsException unused4) {
                        }
                    }
                    throw new AnonymousClass4CM(i, length);
                }
                i++;
                position++;
            }
            byteBuffer.put(position, c == 1 ? (byte) 1 : 0);
            i++;
            position++;
        }
        byteBuffer.position(position);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002d, code lost:
        return r20 + r3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A01(java.lang.CharSequence r18, byte[] r19, int r20, int r21) {
        /*
        // Method dump skipped, instructions count: 486
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC94284bY.A01(java.lang.CharSequence, byte[], int, int):int");
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:167:0x012f */
    /* JADX WARNING: Code restructure failed: missing block: B:183:?, code lost:
        return r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:185:?, code lost:
        return -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:186:?, code lost:
        return -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:187:?, code lost:
        return -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:188:?, code lost:
        return -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:189:?, code lost:
        return -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0082, code lost:
        return -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00b0, code lost:
        if (r1 == 0) goto L_0x00e9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00b3, code lost:
        if (r1 == 1) goto L_0x00d7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00b5, code lost:
        if (r1 != 2) goto L_0x0106;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00b7, code lost:
        r9 = X.C95624e5.A02;
        r0 = X.C95624e5.A00;
        r2 = r9.A01(r19, r0 + r4);
        r1 = r9.A01(r19, r0 + (r4 + 1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00ca, code lost:
        if (r7 > -12) goto L_0x0082;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00cc, code lost:
        if (r2 > -65) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00ce, code lost:
        if (r1 > -65) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00d0, code lost:
        r7 = r7 ^ (r2 << 8);
        r0 = r1 << 16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00d6, code lost:
        return r7 ^ r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00d7, code lost:
        r1 = X.C95624e5.A02.A01(r19, X.C95624e5.A00 + r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00e2, code lost:
        if (r7 > -12) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00e4, code lost:
        if (r1 > -65) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00e6, code lost:
        r0 = r1 << 8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00eb, code lost:
        if (r7 <= -12) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x010b, code lost:
        throw new java.lang.AssertionError();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A02(byte[] r19, int r20, int r21) {
        /*
        // Method dump skipped, instructions count: 456
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC94284bY.A02(byte[], int, int):int");
    }
}
