package X;

import android.app.Application;
import android.content.Context;
import android.content.ContextWrapper;

/* renamed from: X.0Yp  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Yp implements AbstractC001400p, AbstractC001500q, AbstractC001800t {
    public C009804x A00 = null;
    public AbstractC009404s A01;
    public C010004z A02 = null;
    public final AnonymousClass01E A03;
    public final AnonymousClass05C A04;

    public AnonymousClass0Yp(AnonymousClass01E r2, AnonymousClass05C r3) {
        this.A03 = r2;
        this.A04 = r3;
    }

    public void A00() {
        if (this.A00 == null) {
            this.A00 = new C009804x(this);
            this.A02 = new C010004z(this);
        }
    }

    @Override // X.AbstractC001800t
    public AbstractC009404s ACV() {
        AnonymousClass01E r3 = this.A03;
        AbstractC009404s ACV = r3.ACV();
        if (!ACV.equals(r3.A0M)) {
            this.A01 = ACV;
        } else {
            ACV = this.A01;
            if (ACV == null) {
                Application application = null;
                Context applicationContext = r3.A01().getApplicationContext();
                while (true) {
                    if (!(applicationContext instanceof ContextWrapper)) {
                        break;
                    } else if (applicationContext instanceof Application) {
                        application = (Application) applicationContext;
                        break;
                    } else {
                        applicationContext = ((ContextWrapper) applicationContext).getBaseContext();
                    }
                }
                AnonymousClass05E r1 = new AnonymousClass05E(application, r3.A05, this);
                this.A01 = r1;
                return r1;
            }
        }
        return ACV;
    }

    @Override // X.AbstractC001200n
    public AbstractC009904y ADr() {
        A00();
        return this.A00;
    }

    @Override // X.AbstractC001500q
    public AnonymousClass058 AGO() {
        A00();
        return this.A02.A00;
    }

    @Override // X.AbstractC001400p
    public AnonymousClass05C AHb() {
        A00();
        return this.A04;
    }
}
