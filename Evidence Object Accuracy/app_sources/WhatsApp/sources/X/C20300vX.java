package X;

import android.database.Cursor;
import android.os.SystemClock;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0vX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20300vX {
    public final C14830m7 A00;
    public final C16370ot A01;
    public final C16510p9 A02;
    public final C20290vW A03;
    public final C20280vV A04;
    public final C16490p7 A05;

    public C20300vX(C14830m7 r1, C16370ot r2, C16510p9 r3, C20290vW r4, C20280vV r5, C16490p7 r6) {
        this.A01 = r2;
        this.A04 = r5;
        this.A05 = r6;
        this.A00 = r1;
        this.A02 = r3;
        this.A03 = r4;
    }

    public C16380ov A00(String str) {
        AnonymousClass009.A00();
        C20280vV r0 = this.A04;
        AnonymousClass009.A00();
        C16310on A01 = r0.A00.get();
        try {
            Cursor A09 = A01.A03.A09(C38251nm.A01, new String[]{str});
            A01.close();
            try {
                C16380ov r02 = A09.moveToNext() ? (C16380ov) this.A01.A01(A09) : null;
                A09.close();
                return r02;
            } catch (Throwable th) {
                if (A09 != null) {
                    try {
                        A09.close();
                    } catch (Throwable unused) {
                    }
                }
                throw th;
            }
        } catch (Throwable th2) {
            try {
                A01.close();
            } catch (Throwable unused2) {
            }
            throw th2;
        }
    }

    public List A01(AbstractC14640lm r13) {
        AnonymousClass009.A00();
        ArrayList arrayList = new ArrayList();
        C16310on A01 = this.A05.get();
        try {
            long uptimeMillis = SystemClock.uptimeMillis();
            Cursor A09 = A01.A03.A09(C38251nm.A00, new String[]{Integer.toString(4), Long.toString(this.A02.A02(r13)), Long.toString(System.currentTimeMillis() - 2419200000L)});
            if (A09 == null) {
                Log.e("OrderMessageManager/getCheckoutMessages no cursor!");
            } else {
                while (A09.moveToNext()) {
                    AbstractC15340mz A02 = this.A01.A02(A09, r13, false, true);
                    if (A02 != null) {
                        arrayList.add(A02);
                    }
                }
            }
            if (A09 != null) {
                A09.close();
            }
            this.A03.A00("OrderMessageManager/getCheckoutMessages", SystemClock.uptimeMillis() - uptimeMillis);
            A01.close();
            return arrayList;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
