package X;

import android.animation.ValueAnimator;
import org.npci.commonlibrary.widget.FormItemEditText;

/* renamed from: X.61s  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1312161s implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ FormItemEditText A00;

    public C1312161s(FormItemEditText formItemEditText) {
        this.A00 = formItemEditText;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.A00.A09.setAlpha(((Number) valueAnimator.getAnimatedValue()).intValue());
    }
}
