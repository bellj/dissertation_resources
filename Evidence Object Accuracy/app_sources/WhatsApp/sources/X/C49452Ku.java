package X;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.util.Log;
import java.util.Map;

/* renamed from: X.2Ku  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C49452Ku extends AbstractC49422Kr {
    public final C15570nT A00;

    public C49452Ku(AbstractC15710nm r7, C15570nT r8, C14850m9 r9, C16120oU r10, C450720b r11, Map map) {
        super(r7, r9, r10, r11, map);
        this.A00 = r8;
    }

    @Override // X.AbstractC49422Kr
    public void A01(AnonymousClass1V8 r10) {
        AbstractC450820c r5;
        Bundle bundle;
        Handler handler;
        int i;
        int i2;
        Jid A0A = r10.A0A(this.A01, Jid.class, "from");
        if (A0A instanceof DeviceJid) {
            if (this.A00.A0E((DeviceJid) A0A)) {
                this.A04.A00.AYY(Message.obtain(null, 0, 213, 0, r10));
                return;
            }
            return;
        }
        AbstractC14640lm A00 = C15380n4.A00(A0A);
        if (A00 != null) {
            String A0I = r10.A0I("type", null);
            String A0I2 = r10.A0I("name", null);
            String A0I3 = r10.A0I("presence", null);
            if ("unavailable".equals(A0I)) {
                C450720b r6 = this.A04;
                long A002 = C40981sh.A00(r10);
                StringBuilder sb = new StringBuilder("xmpp/reader/read/presence/unavailable ");
                sb.append(A00);
                sb.append(" ");
                sb.append(A002);
                sb.append(" ");
                sb.append(A0I3);
                Log.i(sb.toString());
                r5 = r6.A00;
                bundle = new Bundle();
                bundle.putParcelable("jid", A00);
                bundle.putString("pushName", A0I2);
                bundle.putLong("lastSeen", A002);
                bundle.putString("presence", A0I3);
                handler = null;
                i = 0;
                i2 = 64;
            } else if ("unsubscribe".equals(A0I)) {
                C450720b r2 = this.A04;
                StringBuilder sb2 = new StringBuilder("xmpp/reader/read/presence/unsubscribe ");
                sb2.append(A00);
                Log.i(sb2.toString());
                r5 = r2.A00;
                bundle = new Bundle();
                bundle.putParcelable("jid", A00);
                bundle.putString("pushName", A0I2);
                handler = null;
                i = 0;
                i2 = 88;
            } else if (A0I == null || "available".equals(A0I)) {
                C450720b r22 = this.A04;
                StringBuilder sb3 = new StringBuilder("xmpp/reader/read/presence/available ");
                sb3.append(A00);
                Log.i(sb3.toString());
                r5 = r22.A00;
                bundle = new Bundle();
                bundle.putParcelable("jid", A00);
                bundle.putString("pushName", A0I2);
                handler = null;
                i = 0;
                i2 = 5;
            } else {
                return;
            }
            r5.AYY(Message.obtain(handler, i, i2, i, bundle));
        }
    }
}
