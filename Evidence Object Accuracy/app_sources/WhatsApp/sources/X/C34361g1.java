package X;

import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1g1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C34361g1 {
    public final long A00;
    public final long A01;
    public final Set A02;
    public final Set A03;

    public C34361g1(Set set, Set set2, long j, long j2) {
        this.A00 = j;
        this.A01 = j2;
        this.A02 = set;
        this.A03 = set2;
    }

    public static int A00(C34361g1 r1, C34361g1 r2) {
        boolean A03 = A03(r1, r2);
        boolean A032 = A03(r2, r1);
        return A03 ? A032 ? 2 : 0 : A032 ? 1 : 3;
    }

    public static C34361g1 A01(C34361g1 r10, C34361g1 r11) {
        long max = Math.max(r10.A00, r11.A00);
        long max2 = Math.max(r10.A01, r11.A01);
        HashSet hashSet = new HashSet(r10.A02);
        hashSet.addAll(r11.A02);
        Iterator it = hashSet.iterator();
        while (it.hasNext()) {
            if (((C461824w) it.next()).A00 < max) {
                it.remove();
            }
        }
        if (hashSet.size() > 1000) {
            ArrayList arrayList = new ArrayList(hashSet);
            Collections.sort(arrayList, new Comparator() { // from class: X.5CF
                @Override // java.util.Comparator
                public final int compare(Object obj, Object obj2) {
                    return (((C461824w) obj2).A00 > ((C461824w) obj).A00 ? 1 : (((C461824w) obj2).A00 == ((C461824w) obj).A00 ? 0 : -1));
                }
            });
            List subList = arrayList.subList(0, 1000);
            hashSet = new HashSet(subList);
            max = ((C461824w) subList.get(subList.size() - 1)).A00;
        }
        HashSet hashSet2 = new HashSet(r10.A03);
        hashSet2.addAll(r11.A03);
        if (max2 <= max) {
            max2 = 0;
        }
        return new C34361g1(hashSet, hashSet2, max, max2);
    }

    public static C34361g1 A02(C34351g0 r17, boolean z) {
        if (z) {
            TimeUnit timeUnit = TimeUnit.SECONDS;
            long millis = timeUnit.toMillis(r17.A01);
            long millis2 = timeUnit.toMillis(r17.A02);
            int i = r17.A00;
            if ((i & 1) == 1 && (i & 2) == 2 && millis2 <= millis) {
                throw new C34331fy(2);
            }
            AnonymousClass1K6<AnonymousClass25S> r0 = r17.A03;
            HashSet hashSet = new HashSet();
            HashSet hashSet2 = new HashSet();
            for (AnonymousClass25S r1 : r0) {
                if ((r1.A00 & 1) == 1) {
                    AnonymousClass1G8 r3 = r1.A02;
                    if (r3 == null) {
                        r3 = AnonymousClass1G8.A05;
                    }
                    int i2 = r3.A00;
                    if ((i2 & 2) != 2) {
                        throw new C34331fy(8);
                    } else if ((i2 & 4) != 4) {
                        throw new C34331fy(9);
                    } else if ((i2 & 1) == 1) {
                        AbstractC14640lm A01 = AbstractC14640lm.A01(r3.A03);
                        if (A01 != null) {
                            UserJid nullable = UserJid.getNullable(r3.A02);
                            boolean z2 = r3.A04;
                            if (!C15380n4.A0J(A01) || z2 || nullable != null) {
                                C461824w r6 = new C461824w(A01, nullable, r3.A01, TimeUnit.SECONDS.toMillis(r1.A01), z2);
                                if (r6.A00 == 0) {
                                    hashSet2.add(r6);
                                } else {
                                    hashSet.add(r6);
                                }
                            } else {
                                throw new C34331fy(11);
                            }
                        } else {
                            throw new C34331fy(10);
                        }
                    } else {
                        throw new C34331fy(7);
                    }
                } else {
                    throw new C34331fy(6);
                }
            }
            if (hashSet.size() <= 1000) {
                return new C34361g1(hashSet, hashSet2, millis, millis2);
            }
            throw new C34331fy(5);
        }
        throw new C34331fy(1);
    }

    public static boolean A03(C34361g1 r8, C34361g1 r9) {
        for (Object obj : r9.A03) {
            if (!(r8.A02.contains(obj) || r8.A03.contains(obj))) {
                return false;
            }
        }
        for (C461824w r5 : r9.A02) {
            if (!(r5.A00 <= r8.A00 || r8.A02.contains(r5) || r8.A03.contains(r5))) {
                return false;
            }
        }
        return true;
    }

    public C34351g0 A04() {
        AnonymousClass1G4 A0T = C34351g0.A04.A0T();
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        long seconds = timeUnit.toSeconds(this.A00);
        long seconds2 = timeUnit.toSeconds(this.A01);
        if (seconds > 0) {
            A0T.A03();
            C34351g0 r1 = (C34351g0) A0T.A00;
            r1.A00 |= 1;
            r1.A01 = seconds;
        }
        if (seconds2 > 0) {
            A0T.A03();
            C34351g0 r12 = (C34351g0) A0T.A00;
            r12.A00 |= 2;
            r12.A02 = seconds2;
        }
        for (C461824w r0 : this.A02) {
            AnonymousClass25S A00 = r0.A00();
            A0T.A03();
            C34351g0 r2 = (C34351g0) A0T.A00;
            AnonymousClass1K6 r13 = r2.A03;
            if (!((AnonymousClass1K7) r13).A00) {
                r13 = AbstractC27091Fz.A0G(r13);
                r2.A03 = r13;
            }
            r13.add(A00);
        }
        for (C461824w r02 : this.A03) {
            AnonymousClass25S A002 = r02.A00();
            A0T.A03();
            C34351g0 r22 = (C34351g0) A0T.A00;
            AnonymousClass1K6 r14 = r22.A03;
            if (!((AnonymousClass1K7) r14).A00) {
                r14 = AbstractC27091Fz.A0G(r14);
                r22.A03 = r14;
            }
            r14.add(A002);
        }
        return (C34351g0) A0T.A02();
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C34361g1 r7 = (C34361g1) obj;
            if (this.A00 != r7.A00 || this.A01 != r7.A01 || !this.A02.equals(r7.A02) || !this.A03.equals(r7.A03)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Long.valueOf(this.A00), Long.valueOf(this.A01), this.A02, this.A03});
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("SyncdMessageRange{lastMessageTimestamp=");
        sb.append(this.A00);
        sb.append(", lastSystemMessageTimestamp=");
        sb.append(this.A01);
        sb.append(", messages=");
        sb.append(this.A02);
        sb.append(", messagesWithoutTimestamp=");
        sb.append(this.A03);
        sb.append('}');
        return sb.toString();
    }
}
