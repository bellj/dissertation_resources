package X;

import android.graphics.Typeface;
import android.text.TextPaint;

/* renamed from: X.3hk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74293hk extends AnonymousClass08K {
    public final /* synthetic */ TextPaint A00;
    public final /* synthetic */ AnonymousClass08K A01;
    public final /* synthetic */ C64353Ff A02;

    public C74293hk(TextPaint textPaint, AnonymousClass08K r2, C64353Ff r3) {
        this.A02 = r3;
        this.A00 = textPaint;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass08K
    public void A01(int i) {
        C64353Ff r1 = this.A02;
        r1.A00();
        r1.A01 = true;
        this.A01.A01(i);
    }

    @Override // X.AnonymousClass08K
    public void A02(Typeface typeface) {
        C64353Ff r1 = this.A02;
        r1.A00 = Typeface.create(typeface, r1.A07);
        r1.A03(typeface, this.A00);
        r1.A01 = true;
        this.A01.A02(typeface);
    }
}
