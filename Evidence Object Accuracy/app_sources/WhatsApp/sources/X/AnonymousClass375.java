package X;

import com.whatsapp.status.playback.MyStatusesActivity;

/* renamed from: X.375  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass375 extends AbstractC16350or {
    public final AbstractC15340mz A00;
    public final /* synthetic */ MyStatusesActivity A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass375(AbstractC15340mz r1, MyStatusesActivity myStatusesActivity) {
        super(myStatusesActivity);
        this.A01 = myStatusesActivity;
        this.A00 = r1;
    }
}
