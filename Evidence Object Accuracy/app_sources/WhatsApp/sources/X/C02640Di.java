package X;

import android.view.View;

/* renamed from: X.0Di  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02640Di extends AnonymousClass0Y9 {
    public boolean A00 = false;
    public final /* synthetic */ int A01;
    public final /* synthetic */ C017408d A02;

    public C02640Di(C017408d r2, int i) {
        this.A02 = r2;
        this.A01 = i;
    }

    @Override // X.AnonymousClass0Y9, X.AbstractC12530i4
    public void AMB(View view) {
        this.A00 = true;
    }

    @Override // X.AnonymousClass0Y9, X.AbstractC12530i4
    public void AMC(View view) {
        if (!this.A00) {
            this.A02.A09.setVisibility(this.A01);
        }
    }

    @Override // X.AnonymousClass0Y9, X.AbstractC12530i4
    public void AMD(View view) {
        this.A02.A09.setVisibility(0);
    }
}
