package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.4b9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C94034b9 {
    public final int A00;
    public final UserJid A01;
    public final String A02;

    public C94034b9(UserJid userJid, String str) {
        this.A02 = str;
        this.A01 = userJid;
        this.A00 = 0;
    }

    public C94034b9(String str, int i) {
        this.A02 = str;
        this.A01 = null;
        this.A00 = i;
    }

    public String toString() {
        return this.A02;
    }
}
