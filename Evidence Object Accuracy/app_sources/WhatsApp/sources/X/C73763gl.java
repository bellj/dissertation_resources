package X;

import android.graphics.Outline;
import android.view.View;
import android.view.ViewOutlineProvider;
import com.google.android.material.chip.Chip;

/* renamed from: X.3gl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73763gl extends ViewOutlineProvider {
    public final /* synthetic */ Chip A00;

    public C73763gl(Chip chip) {
        this.A00 = chip;
    }

    @Override // android.view.ViewOutlineProvider
    public void getOutline(View view, Outline outline) {
        AnonymousClass2Zd r0 = this.A00.A04;
        if (r0 != null) {
            r0.getOutline(outline);
        } else {
            outline.setAlpha(0.0f);
        }
    }
}
