package X;

import android.content.Context;
import android.content.IntentFilter;
import android.os.SystemClock;
import com.facebook.redex.RunnableBRunnable0Shape0S0101200_I1;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import java.lang.ref.Reference;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.3Sr */
/* loaded from: classes2.dex */
public final class C67733Sr implements AnonymousClass5QN, AnonymousClass5QP {
    public static C67733Sr A0D;
    public static final AnonymousClass1Mr A0E = AnonymousClass1Mr.of((Object) 218000L, (Object) 159000L, (Object) 145000L, (Object) 130000L, (Object) 112000L);
    public static final AnonymousClass1Mr A0F = AnonymousClass1Mr.of((Object) 2200000L, (Object) 1300000L, (Object) 930000L, (Object) 730000L, (Object) 530000L);
    public static final AnonymousClass1Mr A0G = AnonymousClass1Mr.of((Object) 4800000L, (Object) 2700000L, (Object) 1800000L, (Object) 1200000L, (Object) 630000L);
    public static final AnonymousClass1Mr A0H = AnonymousClass1Mr.of((Object) 12000000L, (Object) 8800000L, (Object) 5900000L, (Object) 3500000L, (Object) 1800000L);
    public static final AnonymousClass1Mr A0I = AnonymousClass1Mr.of((Object) 6100000L, (Object) 3800000L, (Object) 2100000L, (Object) 1300000L, (Object) 590000L);
    public static final C81063tP A0J;
    public int A00;
    public int A01;
    public long A02;
    public long A03;
    public long A04;
    public long A05;
    public long A06;
    public long A07;
    public final Context A08;
    public final AnonymousClass3CF A09;
    public final AnonymousClass5Xd A0A;
    public final C64683Gm A0B;
    public final AbstractC17190qP A0C;

    static {
        C81043tN builder = C81063tP.builder();
        builder.putAll((Object) "AD", (Object[]) new Integer[]{1, 2, 0, 0, 2});
        Integer[] A07 = A07(1, 4);
        A07[2] = 4;
        A07[3] = 4;
        A07[4] = 1;
        builder.putAll((Object) "AE", (Object[]) A07);
        Integer[] A05 = A05(4);
        A05[2] = 3;
        A05[3] = 4;
        A05[4] = 2;
        builder.putAll((Object) "AF", (Object[]) A05);
        Integer[] A052 = A05(2);
        A052[2] = 1;
        A052[3] = 1;
        A052[4] = 2;
        builder.putAll((Object) "AG", (Object[]) A052);
        Integer[] A072 = A07(1, 2);
        A04(A072, 2);
        builder.putAll((Object) "AI", (Object[]) A072);
        Integer[] A053 = A05(1);
        A053[2] = 0;
        A053[3] = 1;
        A053[4] = 2;
        builder.putAll((Object) "AL", (Object[]) A053);
        Integer[] A054 = A05(2);
        A054[2] = 1;
        A054[3] = 2;
        A054[4] = 2;
        builder.putAll((Object) "AM", (Object[]) A054);
        Integer[] A073 = A07(3, 4);
        A073[2] = 4;
        A073[3] = 2;
        A073[4] = 2;
        builder.putAll((Object) "AO", (Object[]) A073);
        Integer[] A074 = A07(2, 4);
        A03(A074, 2);
        builder.putAll((Object) "AR", (Object[]) A074);
        Integer[] A055 = A05(2);
        A055[2] = 4;
        A055[3] = 3;
        A055[4] = 2;
        builder.putAll((Object) "AS", (Object[]) A055);
        Integer[] A075 = A07(0, 3);
        A075[2] = 0;
        A075[3] = 0;
        A075[4] = 2;
        builder.putAll((Object) "AT", (Object[]) A075);
        Integer[] A076 = A07(0, 2);
        A076[2] = 0;
        A076[3] = 1;
        A076[4] = 1;
        builder.putAll((Object) "AU", (Object[]) A076);
        Integer[] A077 = A07(1, 2);
        A077[2] = 0;
        A077[3] = 4;
        A077[4] = 2;
        builder.putAll((Object) "AW", (Object[]) A077);
        Integer[] A078 = A07(0, 2);
        A04(A078, 2);
        builder.putAll((Object) "AX", (Object[]) A078);
        Integer[] A06 = A06(3);
        A06[3] = 4;
        A06[4] = 2;
        builder.putAll((Object) "AZ", (Object[]) A06);
        Integer[] A056 = A05(1);
        A056[2] = 0;
        A056[3] = 1;
        A056[4] = 2;
        builder.putAll((Object) "BA", (Object[]) A056);
        Integer[] A079 = A07(0, 2);
        A079[2] = 0;
        A079[3] = 0;
        A079[4] = 2;
        builder.putAll((Object) "BB", (Object[]) A079);
        Integer[] A0710 = A07(2, 0);
        A0710[2] = 3;
        A0710[3] = 3;
        A0710[4] = 2;
        builder.putAll((Object) "BD", (Object[]) A0710);
        Integer[] A0711 = A07(0, 1);
        A0711[2] = 2;
        A0711[3] = 3;
        A0711[4] = 2;
        builder.putAll((Object) "BE", (Object[]) A0711);
        Integer[] A062 = A06(4);
        A062[3] = 2;
        A062[4] = 2;
        builder.putAll((Object) "BF", (Object[]) A062);
        Integer[] A0712 = A07(0, 1);
        A0712[2] = 0;
        A0712[3] = 0;
        A0712[4] = 2;
        builder.putAll((Object) "BG", (Object[]) A0712);
        Integer[] A0713 = A07(1, 0);
        A0713[2] = 2;
        A0713[3] = 4;
        A0713[4] = 2;
        builder.putAll((Object) "BH", (Object[]) A0713);
        Integer[] A063 = A06(4);
        A063[3] = 4;
        A063[4] = 2;
        builder.putAll((Object) "BI", (Object[]) A063);
        Integer[] A057 = A05(4);
        A057[2] = 3;
        A057[3] = 4;
        A057[4] = 2;
        builder.putAll((Object) "BJ", (Object[]) A057);
        Integer[] A0714 = A07(1, 2);
        A04(A0714, 2);
        builder.putAll((Object) "BL", (Object[]) A0714);
        Integer[] A0715 = A07(1, 2);
        A0715[2] = 0;
        A0715[3] = 0;
        A0715[4] = 2;
        builder.putAll((Object) "BM", (Object[]) A0715);
        Integer[] A0716 = A07(4, 0);
        A0716[2] = 1;
        A0716[3] = 1;
        A0716[4] = 2;
        builder.putAll((Object) "BN", (Object[]) A0716);
        Integer[] A0717 = A07(2, 3);
        A0717[2] = 3;
        A0717[3] = 2;
        A0717[4] = 2;
        builder.putAll((Object) "BO", (Object[]) A0717);
        Integer[] A0718 = A07(1, 2);
        A0718[2] = 1;
        A0718[3] = 2;
        A0718[4] = 2;
        builder.putAll((Object) "BQ", (Object[]) A0718);
        Integer[] A0719 = A07(2, 4);
        A0719[2] = 2;
        A0719[3] = 1;
        A0719[4] = 2;
        builder.putAll((Object) "BR", (Object[]) A0719);
        Integer[] A0720 = A07(3, 2);
        A0720[2] = 2;
        A0720[3] = 3;
        A0720[4] = 2;
        builder.putAll((Object) "BS", (Object[]) A0720);
        Integer[] A0721 = A07(3, 0);
        A0721[2] = 3;
        A0721[3] = 2;
        A0721[4] = 2;
        builder.putAll((Object) "BT", (Object[]) A0721);
        Integer[] A0722 = A07(3, 4);
        A03(A0722, 2);
        builder.putAll((Object) "BW", (Object[]) A0722);
        Integer[] A0723 = A07(1, 0);
        A0723[2] = 2;
        A0723[3] = 1;
        A0723[4] = 2;
        builder.putAll((Object) "BY", (Object[]) A0723);
        Integer[] A064 = A06(2);
        A064[3] = 1;
        A064[4] = 2;
        builder.putAll((Object) "BZ", (Object[]) A064);
        Integer[] A0724 = A07(0, 3);
        A0724[2] = 1;
        A0724[3] = 2;
        A0724[4] = 3;
        builder.putAll((Object) "CA", (Object[]) A0724);
        Integer[] A0725 = A07(4, 3);
        A03(A0725, 2);
        builder.putAll((Object) "CD", (Object[]) A0725);
        Integer[] A0726 = A07(4, 2);
        A04(A0726, 2);
        builder.putAll((Object) "CF", (Object[]) A0726);
        Integer[] A0727 = A07(3, 4);
        A0727[2] = 1;
        A0727[3] = 1;
        A0727[4] = 2;
        builder.putAll((Object) "CG", (Object[]) A0727);
        Integer[] A0728 = A07(0, 1);
        A03(A0728, 0);
        builder.putAll((Object) "CH", (Object[]) A0728);
        Integer[] A065 = A06(3);
        A065[3] = 3;
        A065[4] = 2;
        builder.putAll((Object) "CI", (Object[]) A065);
        Integer[] A0729 = A07(3, 2);
        A0729[2] = 1;
        A0729[3] = 0;
        A0729[4] = 2;
        builder.putAll((Object) "CK", (Object[]) A0729);
        Integer[] A058 = A05(1);
        A058[2] = 2;
        A058[3] = 3;
        A058[4] = 2;
        builder.putAll((Object) "CL", (Object[]) A058);
        Integer[] A0730 = A07(3, 4);
        A0730[2] = 3;
        A0730[3] = 2;
        A0730[4] = 2;
        builder.putAll((Object) "CM", (Object[]) A0730);
        Integer[] A066 = A06(2);
        A066[3] = 1;
        A066[4] = 3;
        builder.putAll((Object) "CN", (Object[]) A066);
        Integer[] A0731 = A07(2, 4);
        A0731[2] = 3;
        A0731[3] = 2;
        A0731[4] = 2;
        builder.putAll((Object) "CO", (Object[]) A0731);
        Integer[] A0732 = A07(2, 3);
        A0732[2] = 4;
        A0732[3] = 4;
        A0732[4] = 2;
        builder.putAll((Object) "CR", (Object[]) A0732);
        Integer[] A059 = A05(4);
        A059[2] = 2;
        A059[3] = 1;
        A059[4] = 2;
        builder.putAll((Object) "CU", (Object[]) A059);
        Integer[] A0733 = A07(2, 3);
        A0733[2] = 3;
        A0733[3] = 3;
        A0733[4] = 2;
        builder.putAll((Object) "CV", (Object[]) A0733);
        Integer[] A0734 = A07(1, 2);
        A0734[2] = 0;
        A0734[3] = 0;
        A0734[4] = 2;
        builder.putAll((Object) "CW", (Object[]) A0734);
        Integer[] A0735 = A07(1, 2);
        A0735[2] = 0;
        A0735[3] = 0;
        A0735[4] = 2;
        builder.putAll((Object) "CY", (Object[]) A0735);
        Integer[] A0736 = A07(0, 1);
        A0736[2] = 0;
        A0736[3] = 0;
        A0736[4] = 2;
        builder.putAll((Object) "CZ", (Object[]) A0736);
        Integer[] A0737 = A07(0, 1);
        A0737[2] = 1;
        A0737[3] = 2;
        A0737[4] = 0;
        builder.putAll((Object) "DE", (Object[]) A0737);
        Integer[] A0738 = A07(4, 1);
        A0738[2] = 4;
        A0738[3] = 4;
        A0738[4] = 2;
        builder.putAll((Object) "DJ", (Object[]) A0738);
        Integer[] A0510 = A05(0);
        A0510[2] = 1;
        A0510[3] = 0;
        A0510[4] = 2;
        builder.putAll((Object) "DK", (Object[]) A0510);
        Integer[] A0739 = A07(1, 2);
        A04(A0739, 2);
        builder.putAll((Object) "DM", (Object[]) A0739);
        Integer[] A0740 = A07(3, 4);
        A0740[2] = 4;
        A0740[3] = 4;
        A0740[4] = 2;
        builder.putAll((Object) "DO", (Object[]) A0740);
        Integer[] A0741 = A07(3, 2);
        A0741[2] = 4;
        A0741[3] = 4;
        A0741[4] = 2;
        builder.putAll((Object) "DZ", (Object[]) A0741);
        Integer[] A0742 = A07(2, 4);
        A0742[2] = 3;
        A0742[3] = 2;
        A0742[4] = 2;
        builder.putAll((Object) "EC", (Object[]) A0742);
        Integer[] A067 = A06(0);
        A067[3] = 0;
        A067[4] = 2;
        builder.putAll((Object) "EE", (Object[]) A067);
        Integer[] A0743 = A07(3, 4);
        A0743[2] = 2;
        A0743[3] = 1;
        A0743[4] = 2;
        builder.putAll((Object) "EG", (Object[]) A0743);
        Integer[] A068 = A06(2);
        A068[3] = 2;
        A068[4] = 2;
        builder.putAll((Object) "EH", (Object[]) A068);
        Integer[] A0744 = A07(4, 2);
        A04(A0744, 2);
        builder.putAll((Object) "ER", (Object[]) A0744);
        Integer[] A0745 = A07(0, 1);
        A0745[2] = 2;
        A0745[3] = 1;
        A0745[4] = 2;
        builder.putAll((Object) "ES", (Object[]) A0745);
        Integer[] A069 = A06(4);
        A069[3] = 1;
        A069[4] = 2;
        builder.putAll((Object) "ET", (Object[]) A069);
        Integer[] A0511 = A05(0);
        A0511[2] = 1;
        A0511[3] = 0;
        A0511[4] = 0;
        builder.putAll((Object) "FI", (Object[]) A0511);
        Integer[] A0746 = A07(3, 0);
        A0746[2] = 3;
        A0746[3] = 3;
        A0746[4] = 2;
        builder.putAll((Object) "FJ", (Object[]) A0746);
        Integer[] A0610 = A06(2);
        A0610[3] = 2;
        A0610[4] = 2;
        builder.putAll((Object) "FK", (Object[]) A0610);
        Integer[] A0747 = A07(4, 2);
        A0747[2] = 4;
        A0747[3] = 3;
        A0747[4] = 2;
        builder.putAll((Object) "FM", (Object[]) A0747);
        Integer[] A0748 = A07(0, 2);
        A0748[2] = 0;
        A0748[3] = 0;
        A0748[4] = 2;
        builder.putAll((Object) "FO", (Object[]) A0748);
        Integer[] A0749 = A07(1, 0);
        A0749[2] = 2;
        A0749[3] = 1;
        A0749[4] = 2;
        builder.putAll((Object) "FR", (Object[]) A0749);
        Integer[] A0512 = A05(3);
        A0512[2] = 1;
        A0512[3] = 0;
        A0512[4] = 2;
        builder.putAll((Object) "GA", (Object[]) A0512);
        Integer[] A0513 = A05(0);
        A0513[2] = 1;
        A0513[3] = 2;
        A0513[4] = 2;
        builder.putAll((Object) "GB", (Object[]) A0513);
        Integer[] A0750 = A07(1, 2);
        A04(A0750, 2);
        builder.putAll((Object) "GD", (Object[]) A0750);
        Integer[] A0751 = A07(1, 0);
        A0751[2] = 1;
        A0751[3] = 3;
        A0751[4] = 2;
        builder.putAll((Object) "GE", (Object[]) A0751);
        Integer[] A0611 = A06(2);
        A0611[3] = 4;
        A0611[4] = 2;
        builder.putAll((Object) "GF", (Object[]) A0611);
        Integer[] A0752 = A07(0, 2);
        A0752[2] = 0;
        A0752[3] = 0;
        A0752[4] = 2;
        builder.putAll((Object) "GG", (Object[]) A0752);
        Integer[] A0753 = A07(3, 2);
        A0753[2] = 3;
        A0753[3] = 2;
        A0753[4] = 2;
        builder.putAll((Object) "GH", (Object[]) A0753);
        Integer[] A0754 = A07(0, 2);
        A0754[2] = 0;
        A0754[3] = 0;
        A0754[4] = 2;
        builder.putAll((Object) "GI", (Object[]) A0754);
        Integer[] A0755 = A07(1, 2);
        A0755[2] = 2;
        A0755[3] = 1;
        A0755[4] = 2;
        builder.putAll((Object) "GL", (Object[]) A0755);
        Integer[] A0756 = A07(4, 3);
        A0756[2] = 2;
        A0756[3] = 4;
        A0756[4] = 2;
        builder.putAll((Object) "GM", (Object[]) A0756);
        Integer[] A0757 = A07(4, 3);
        A0757[2] = 4;
        A0757[3] = 2;
        A0757[4] = 2;
        builder.putAll((Object) "GN", (Object[]) A0757);
        Integer[] A0514 = A05(2);
        A0514[2] = 3;
        A0514[3] = 4;
        A0514[4] = 2;
        builder.putAll((Object) "GP", (Object[]) A0514);
        Integer[] A0758 = A07(4, 2);
        A0758[2] = 3;
        A0758[3] = 4;
        A0758[4] = 2;
        builder.putAll((Object) "GQ", (Object[]) A0758);
        Integer[] A0515 = A05(1);
        A0515[2] = 0;
        A0515[3] = 1;
        A0515[4] = 2;
        builder.putAll((Object) "GR", (Object[]) A0515);
        Integer[] A0759 = A07(3, 2);
        A0759[2] = 3;
        A0759[3] = 2;
        A0759[4] = 2;
        builder.putAll((Object) "GT", (Object[]) A0759);
        Integer[] A0760 = A07(1, 2);
        A0760[2] = 4;
        A0760[3] = 4;
        A0760[4] = 2;
        builder.putAll((Object) "GU", (Object[]) A0760);
        Integer[] A0761 = A07(3, 4);
        A0761[2] = 4;
        A0761[3] = 3;
        A0761[4] = 2;
        builder.putAll((Object) "GW", (Object[]) A0761);
        Integer[] A0516 = A05(3);
        A0516[2] = 1;
        A0516[3] = 0;
        A0516[4] = 2;
        builder.putAll((Object) "GY", (Object[]) A0516);
        Integer[] A0762 = A07(0, 2);
        A0762[2] = 3;
        A0762[3] = 4;
        A0762[4] = 2;
        builder.putAll((Object) "HK", (Object[]) A0762);
        Integer[] A0763 = A07(3, 0);
        A0763[2] = 3;
        A0763[3] = 3;
        A0763[4] = 2;
        builder.putAll((Object) "HN", (Object[]) A0763);
        Integer[] A0517 = A05(1);
        A0517[2] = 0;
        A0517[3] = 1;
        A0517[4] = 2;
        builder.putAll((Object) "HR", (Object[]) A0517);
        Integer[] A0764 = A07(4, 3);
        A0764[2] = 4;
        A0764[3] = 4;
        A0764[4] = 2;
        builder.putAll((Object) "HT", (Object[]) A0764);
        Integer[] A0765 = A07(0, 1);
        A0765[2] = 0;
        A0765[3] = 0;
        A0765[4] = 2;
        builder.putAll((Object) "HU", (Object[]) A0765);
        Integer[] A0766 = A07(3, 2);
        A0766[2] = 2;
        A0766[3] = 3;
        A0766[4] = 2;
        builder.putAll((Object) "ID", (Object[]) A0766);
        Integer[] A0518 = A05(0);
        A0518[2] = 1;
        A0518[3] = 1;
        A0518[4] = 2;
        builder.putAll((Object) "IE", (Object[]) A0518);
        Integer[] A0767 = A07(1, 0);
        A0767[2] = 2;
        A0767[3] = 3;
        A0767[4] = 2;
        builder.putAll((Object) "IL", (Object[]) A0767);
        Integer[] A0768 = A07(0, 2);
        A0768[2] = 0;
        A0768[3] = 1;
        A0768[4] = 2;
        builder.putAll((Object) "IM", (Object[]) A0768);
        Integer[] A0769 = A07(2, 1);
        A0769[2] = 3;
        A0769[3] = 3;
        A0769[4] = 2;
        builder.putAll((Object) "IN", (Object[]) A0769);
        Integer[] A0770 = A07(4, 2);
        A0770[2] = 2;
        A0770[3] = 4;
        A0770[4] = 2;
        builder.putAll((Object) "IO", (Object[]) A0770);
        Integer[] A0771 = A07(3, 2);
        A0771[2] = 4;
        A0771[3] = 3;
        A0771[4] = 2;
        builder.putAll((Object) "IQ", (Object[]) A0771);
        Integer[] A0772 = A07(4, 2);
        A0772[2] = 3;
        A0772[3] = 4;
        A0772[4] = 2;
        builder.putAll((Object) "IR", (Object[]) A0772);
        Integer[] A0773 = A07(0, 2);
        A0773[2] = 0;
        A0773[3] = 0;
        A0773[4] = 2;
        builder.putAll((Object) "IS", (Object[]) A0773);
        Integer[] A0519 = A05(0);
        A0519[2] = 1;
        A0519[3] = 1;
        A0519[4] = 2;
        builder.putAll((Object) "IT", (Object[]) A0519);
        Integer[] A0520 = A05(2);
        A0520[2] = 0;
        A0520[3] = 2;
        A0520[4] = 2;
        builder.putAll((Object) "JE", (Object[]) A0520);
        Integer[] A0521 = A05(3);
        A0521[2] = 4;
        A0521[3] = 4;
        A0521[4] = 2;
        builder.putAll((Object) "JM", (Object[]) A0521);
        Integer[] A0774 = A07(1, 2);
        A0774[2] = 1;
        A0774[3] = 1;
        A0774[4] = 2;
        builder.putAll((Object) "JO", (Object[]) A0774);
        Integer[] A0775 = A07(0, 2);
        A0775[2] = 0;
        A0775[3] = 1;
        A0775[4] = 3;
        builder.putAll((Object) "JP", (Object[]) A0775);
        Integer[] A0776 = A07(3, 4);
        A03(A0776, 2);
        builder.putAll((Object) "KE", (Object[]) A0776);
        Integer[] A0777 = A07(1, 0);
        A03(A0777, 2);
        builder.putAll((Object) "KG", (Object[]) A0777);
        Integer[] A0778 = A07(2, 0);
        A0778[2] = 4;
        A0778[3] = 3;
        A0778[4] = 2;
        builder.putAll((Object) "KH", (Object[]) A0778);
        Integer[] A0779 = A07(4, 2);
        A0779[2] = 3;
        A0779[3] = 1;
        A0779[4] = 2;
        builder.putAll((Object) "KI", (Object[]) A0779);
        Integer[] A0780 = A07(4, 2);
        A0780[2] = 2;
        A0780[3] = 3;
        A0780[4] = 2;
        builder.putAll((Object) "KM", (Object[]) A0780);
        Integer[] A0781 = A07(1, 2);
        A04(A0781, 2);
        builder.putAll((Object) "KN", (Object[]) A0781);
        Integer[] A0782 = A07(4, 2);
        A04(A0782, 2);
        builder.putAll((Object) "KP", (Object[]) A0782);
        Integer[] A0783 = A07(0, 2);
        A03(A0783, 1);
        builder.putAll((Object) "KR", (Object[]) A0783);
        Integer[] A0784 = A07(2, 3);
        A03(A0784, 1);
        builder.putAll((Object) "KW", (Object[]) A0784);
        Integer[] A0785 = A07(1, 2);
        A0785[2] = 0;
        A0785[3] = 0;
        A0785[4] = 2;
        builder.putAll((Object) "KY", (Object[]) A0785);
        Integer[] A0786 = A07(1, 2);
        A0786[2] = 2;
        A0786[3] = 3;
        A0786[4] = 2;
        builder.putAll((Object) "KZ", (Object[]) A0786);
        Integer[] A0522 = A05(2);
        A0522[2] = 1;
        A0522[3] = 1;
        A0522[4] = 2;
        builder.putAll((Object) "LA", (Object[]) A0522);
        Integer[] A0787 = A07(3, 2);
        A0787[2] = 0;
        A0787[3] = 0;
        A0787[4] = 2;
        builder.putAll((Object) "LB", (Object[]) A0787);
        Integer[] A0523 = A05(1);
        A0523[2] = 0;
        A0523[3] = 0;
        A0523[4] = 2;
        builder.putAll((Object) "LC", (Object[]) A0523);
        Integer[] A0788 = A07(0, 2);
        A04(A0788, 2);
        builder.putAll((Object) "LI", (Object[]) A0788);
        Integer[] A0789 = A07(2, 0);
        A0789[2] = 2;
        A0789[3] = 3;
        A0789[4] = 2;
        builder.putAll((Object) "LK", (Object[]) A0789);
        Integer[] A0790 = A07(3, 4);
        A0790[2] = 3;
        A0790[3] = 2;
        A0790[4] = 2;
        builder.putAll((Object) "LR", (Object[]) A0790);
        Integer[] A0524 = A05(3);
        A0524[2] = 2;
        A0524[3] = 3;
        A0524[4] = 2;
        builder.putAll((Object) "LS", (Object[]) A0524);
        Integer[] A0612 = A06(0);
        A0612[3] = 0;
        A0612[4] = 2;
        builder.putAll((Object) "LT", (Object[]) A0612);
        Integer[] A0613 = A06(0);
        A0613[3] = 0;
        A0613[4] = 2;
        builder.putAll((Object) "LU", (Object[]) A0613);
        Integer[] A0614 = A06(0);
        A0614[3] = 0;
        A0614[4] = 2;
        builder.putAll((Object) "LV", (Object[]) A0614);
        Integer[] A0791 = A07(4, 2);
        A0791[2] = 4;
        A0791[3] = 3;
        A0791[4] = 2;
        builder.putAll((Object) "LY", (Object[]) A0791);
        Integer[] A0792 = A07(2, 1);
        A0792[2] = 2;
        A0792[3] = 1;
        A0792[4] = 2;
        builder.putAll((Object) "MA", (Object[]) A0792);
        Integer[] A0793 = A07(0, 2);
        A04(A0793, 2);
        builder.putAll((Object) "MC", (Object[]) A0793);
        Integer[] A0794 = A07(1, 2);
        A0794[2] = 0;
        A0794[3] = 0;
        A0794[4] = 2;
        builder.putAll((Object) "MD", (Object[]) A0794);
        Integer[] A0795 = A07(1, 2);
        A0795[2] = 1;
        A0795[3] = 2;
        A0795[4] = 2;
        builder.putAll((Object) "ME", (Object[]) A0795);
        Integer[] A0796 = A07(1, 2);
        A0796[2] = 1;
        A0796[3] = 0;
        A0796[4] = 2;
        builder.putAll((Object) "MF", (Object[]) A0796);
        Integer[] A0797 = A07(3, 4);
        A0797[2] = 3;
        A0797[3] = 3;
        A0797[4] = 2;
        builder.putAll((Object) "MG", (Object[]) A0797);
        Integer[] A0798 = A07(4, 2);
        A0798[2] = 2;
        A0798[3] = 4;
        A0798[4] = 2;
        builder.putAll((Object) "MH", (Object[]) A0798);
        Integer[] A0799 = A07(1, 0);
        A0799[2] = 0;
        A0799[3] = 0;
        A0799[4] = 2;
        builder.putAll((Object) "MK", (Object[]) A0799);
        Integer[] A0525 = A05(4);
        A0525[2] = 1;
        A0525[3] = 1;
        A0525[4] = 2;
        builder.putAll((Object) "ML", (Object[]) A0525);
        Integer[] A07100 = A07(2, 3);
        A03(A07100, 2);
        builder.putAll((Object) "MM", (Object[]) A07100);
        Integer[] A07101 = A07(2, 4);
        A07101[2] = 1;
        A07101[3] = 1;
        A07101[4] = 2;
        builder.putAll((Object) "MN", (Object[]) A07101);
        Integer[] A07102 = A07(0, 2);
        A07102[2] = 4;
        A07102[3] = 4;
        A07102[4] = 2;
        builder.putAll((Object) "MO", (Object[]) A07102);
        Integer[] A07103 = A07(0, 2);
        A04(A07103, 2);
        builder.putAll((Object) "MP", (Object[]) A07103);
        Integer[] A0615 = A06(2);
        A0615[3] = 3;
        A0615[4] = 2;
        builder.putAll((Object) "MQ", (Object[]) A0615);
        Integer[] A07104 = A07(3, 0);
        A07104[2] = 4;
        A07104[3] = 2;
        A07104[4] = 2;
        builder.putAll((Object) "MR", (Object[]) A07104);
        Integer[] A07105 = A07(1, 2);
        A04(A07105, 2);
        builder.putAll((Object) "MS", (Object[]) A07105);
        Integer[] A07106 = A07(0, 2);
        A07106[2] = 0;
        A07106[3] = 1;
        A07106[4] = 2;
        builder.putAll((Object) "MT", (Object[]) A07106);
        Integer[] A07107 = A07(3, 1);
        A07107[2] = 2;
        A07107[3] = 3;
        A07107[4] = 2;
        builder.putAll((Object) "MU", (Object[]) A07107);
        Integer[] A07108 = A07(4, 3);
        A07108[2] = 1;
        A07108[3] = 4;
        A07108[4] = 2;
        builder.putAll((Object) "MV", (Object[]) A07108);
        Integer[] A07109 = A07(4, 1);
        A07109[2] = 1;
        A07109[3] = 0;
        A07109[4] = 2;
        builder.putAll((Object) "MW", (Object[]) A07109);
        Integer[] A07110 = A07(2, 4);
        A07110[2] = 3;
        A07110[3] = 3;
        A07110[4] = 2;
        builder.putAll((Object) "MX", (Object[]) A07110);
        Integer[] A07111 = A07(2, 0);
        A07111[2] = 3;
        A07111[3] = 3;
        A07111[4] = 2;
        builder.putAll((Object) "MY", (Object[]) A07111);
        Integer[] A0526 = A05(3);
        A0526[2] = 2;
        A0526[3] = 3;
        A0526[4] = 2;
        builder.putAll((Object) "MZ", (Object[]) A0526);
        Integer[] A07112 = A07(4, 3);
        A03(A07112, 2);
        builder.putAll((Object) "NA", (Object[]) A07112);
        Integer[] A07113 = A07(2, 0);
        A07113[2] = 4;
        A07113[3] = 4;
        A07113[4] = 2;
        builder.putAll((Object) "NC", (Object[]) A07113);
        Integer[] A0616 = A06(4);
        A0616[3] = 4;
        A0616[4] = 2;
        builder.putAll((Object) "NE", (Object[]) A0616);
        Integer[] A0617 = A06(2);
        A0617[3] = 2;
        A0617[4] = 2;
        builder.putAll((Object) "NF", (Object[]) A0617);
        Integer[] A0527 = A05(3);
        A03(A0527, 2);
        builder.putAll((Object) "NG", (Object[]) A0527);
        Integer[] A07114 = A07(3, 1);
        A07114[2] = 4;
        A07114[3] = 4;
        A07114[4] = 2;
        builder.putAll((Object) "NI", (Object[]) A07114);
        Integer[] A07115 = A07(0, 2);
        A07115[2] = 4;
        A07115[3] = 2;
        A07115[4] = 0;
        builder.putAll((Object) "NL", (Object[]) A07115);
        Integer[] A07116 = A07(0, 1);
        A07116[2] = 1;
        A07116[3] = 0;
        A07116[4] = 2;
        builder.putAll((Object) "NO", (Object[]) A07116);
        Integer[] A07117 = A07(2, 0);
        A07117[2] = 4;
        A07117[3] = 3;
        A07117[4] = 2;
        builder.putAll((Object) "NP", (Object[]) A07117);
        Integer[] A07118 = A07(4, 2);
        A07118[2] = 3;
        A07118[3] = 1;
        A07118[4] = 2;
        builder.putAll((Object) "NR", (Object[]) A07118);
        Integer[] A07119 = A07(4, 2);
        A04(A07119, 2);
        builder.putAll((Object) "NU", (Object[]) A07119);
        Integer[] A07120 = A07(0, 2);
        A07120[2] = 1;
        A07120[3] = 2;
        A07120[4] = 4;
        builder.putAll((Object) "NZ", (Object[]) A07120);
        Integer[] A0528 = A05(2);
        A0528[2] = 0;
        A0528[3] = 2;
        A0528[4] = 2;
        builder.putAll((Object) "OM", (Object[]) A0528);
        Integer[] A07121 = A07(1, 3);
        A07121[2] = 3;
        A07121[3] = 4;
        A07121[4] = 2;
        builder.putAll((Object) "PA", (Object[]) A07121);
        Integer[] A07122 = A07(2, 4);
        A07122[2] = 4;
        A07122[3] = 4;
        A07122[4] = 2;
        builder.putAll((Object) "PE", (Object[]) A07122);
        Integer[] A0529 = A05(2);
        A0529[2] = 1;
        A0529[3] = 1;
        A0529[4] = 2;
        builder.putAll((Object) "PF", (Object[]) A0529);
        Integer[] A07123 = A07(4, 3);
        A07123[2] = 3;
        A07123[3] = 2;
        A07123[4] = 2;
        builder.putAll((Object) "PG", (Object[]) A07123);
        Integer[] A07124 = A07(3, 0);
        A07124[2] = 3;
        A07124[3] = 4;
        A07124[4] = 4;
        builder.putAll((Object) "PH", (Object[]) A07124);
        Integer[] A07125 = A07(3, 2);
        A07125[2] = 3;
        A07125[3] = 3;
        A07125[4] = 2;
        builder.putAll((Object) "PK", (Object[]) A07125);
        Integer[] A07126 = A07(1, 0);
        A03(A07126, 2);
        builder.putAll((Object) "PL", (Object[]) A07126);
        Integer[] A07127 = A07(0, 2);
        A04(A07127, 2);
        builder.putAll((Object) "PM", (Object[]) A07127);
        Integer[] A07128 = A07(1, 2);
        A07128[2] = 2;
        A07128[3] = 3;
        A07128[4] = 4;
        builder.putAll((Object) "PR", (Object[]) A07128);
        Integer[] A0530 = A05(3);
        A03(A0530, 2);
        builder.putAll((Object) "PS", (Object[]) A0530);
        Integer[] A0531 = A05(1);
        A0531[2] = 0;
        A0531[3] = 0;
        A0531[4] = 2;
        builder.putAll((Object) "PT", (Object[]) A0531);
        Integer[] A07129 = A07(1, 2);
        A07129[2] = 3;
        A07129[3] = 0;
        A07129[4] = 2;
        builder.putAll((Object) "PW", (Object[]) A07129);
        Integer[] A07130 = A07(2, 0);
        A07130[2] = 3;
        A07130[3] = 3;
        A07130[4] = 2;
        builder.putAll((Object) "PY", (Object[]) A07130);
        Integer[] A07131 = A07(2, 3);
        A07131[2] = 1;
        A07131[3] = 2;
        A07131[4] = 2;
        builder.putAll((Object) "QA", (Object[]) A07131);
        Integer[] A07132 = A07(1, 0);
        A07132[2] = 2;
        A07132[3] = 1;
        A07132[4] = 2;
        builder.putAll((Object) "RE", (Object[]) A07132);
        Integer[] A0618 = A06(1);
        A0618[3] = 2;
        A0618[4] = 2;
        builder.putAll((Object) "RO", (Object[]) A0618);
        Integer[] A07133 = A07(1, 2);
        A07133[2] = 0;
        A07133[3] = 0;
        A07133[4] = 2;
        builder.putAll((Object) "RS", (Object[]) A07133);
        Integer[] A07134 = A07(0, 1);
        A07134[2] = 0;
        A07134[3] = 1;
        A07134[4] = 2;
        builder.putAll((Object) "RU", (Object[]) A07134);
        Integer[] A07135 = A07(4, 3);
        A07135[2] = 3;
        A07135[3] = 4;
        A07135[4] = 2;
        builder.putAll((Object) "RW", (Object[]) A07135);
        Integer[] A0619 = A06(2);
        A0619[3] = 1;
        A0619[4] = 2;
        builder.putAll((Object) "SA", (Object[]) A0619);
        Integer[] A07136 = A07(4, 2);
        A07136[2] = 4;
        A07136[3] = 2;
        A07136[4] = 2;
        builder.putAll((Object) "SB", (Object[]) A07136);
        Integer[] A07137 = A07(4, 2);
        A07137[2] = 0;
        A07137[3] = 1;
        A07137[4] = 2;
        builder.putAll((Object) "SC", (Object[]) A07137);
        Integer[] A0620 = A06(4);
        A0620[3] = 3;
        A0620[4] = 2;
        builder.putAll((Object) "SD", (Object[]) A0620);
        Integer[] A0621 = A06(0);
        A0621[3] = 0;
        A0621[4] = 2;
        builder.putAll((Object) "SE", (Object[]) A0621);
        Integer[] A0532 = A05(0);
        A0532[2] = 3;
        A0532[3] = 3;
        A0532[4] = 4;
        builder.putAll((Object) "SG", (Object[]) A0532);
        Integer[] A07138 = A07(4, 2);
        A04(A07138, 2);
        builder.putAll((Object) "SH", (Object[]) A07138);
        Integer[] A07139 = A07(0, 1);
        A07139[2] = 0;
        A07139[3] = 0;
        A07139[4] = 2;
        builder.putAll((Object) "SI", (Object[]) A07139);
        Integer[] A0622 = A06(2);
        A0622[3] = 2;
        A0622[4] = 2;
        builder.putAll((Object) "SJ", (Object[]) A0622);
        Integer[] A07140 = A07(0, 1);
        A07140[2] = 0;
        A07140[3] = 0;
        A07140[4] = 2;
        builder.putAll((Object) "SK", (Object[]) A07140);
        Integer[] A07141 = A07(4, 3);
        A07141[2] = 3;
        A07141[3] = 1;
        A07141[4] = 2;
        builder.putAll((Object) "SL", (Object[]) A07141);
        Integer[] A07142 = A07(0, 2);
        A04(A07142, 2);
        builder.putAll((Object) "SM", (Object[]) A07142);
        Integer[] A0623 = A06(4);
        A0623[3] = 3;
        A0623[4] = 2;
        builder.putAll((Object) "SN", (Object[]) A0623);
        Integer[] A07143 = A07(3, 4);
        A07143[2] = 4;
        A07143[3] = 4;
        A07143[4] = 2;
        builder.putAll((Object) "SO", (Object[]) A07143);
        Integer[] A07144 = A07(3, 2);
        A07144[2] = 3;
        A07144[3] = 1;
        A07144[4] = 2;
        builder.putAll((Object) "SR", (Object[]) A07144);
        Integer[] A07145 = A07(4, 1);
        A07145[2] = 4;
        A07145[3] = 2;
        A07145[4] = 2;
        builder.putAll((Object) "SS", (Object[]) A07145);
        Integer[] A0533 = A05(2);
        A0533[2] = 1;
        A0533[3] = 2;
        A0533[4] = 2;
        builder.putAll((Object) "ST", (Object[]) A0533);
        Integer[] A07146 = A07(2, 1);
        A07146[2] = 4;
        A07146[3] = 4;
        A07146[4] = 2;
        builder.putAll((Object) "SV", (Object[]) A07146);
        Integer[] A0534 = A05(2);
        A0534[2] = 1;
        A0534[3] = 0;
        A0534[4] = 2;
        builder.putAll((Object) "SX", (Object[]) A0534);
        Integer[] A07147 = A07(4, 3);
        A03(A07147, 2);
        builder.putAll((Object) "SY", (Object[]) A07147);
        Integer[] A07148 = A07(3, 4);
        A07148[2] = 3;
        A07148[3] = 4;
        A07148[4] = 2;
        builder.putAll((Object) "SZ", (Object[]) A07148);
        Integer[] A07149 = A07(1, 2);
        A07149[2] = 1;
        A07149[3] = 0;
        A07149[4] = 2;
        builder.putAll((Object) "TC", (Object[]) A07149);
        Integer[] A0624 = A06(4);
        A0624[3] = 4;
        A0624[4] = 2;
        builder.putAll((Object) "TD", (Object[]) A0624);
        Integer[] A07150 = A07(3, 2);
        A07150[2] = 1;
        A07150[3] = 0;
        A07150[4] = 2;
        builder.putAll((Object) "TG", (Object[]) A07150);
        Integer[] A07151 = A07(1, 3);
        A07151[2] = 4;
        A07151[3] = 3;
        A07151[4] = 0;
        builder.putAll((Object) "TH", (Object[]) A07151);
        Integer[] A0625 = A06(4);
        A0625[3] = 4;
        A0625[4] = 2;
        builder.putAll((Object) "TJ", (Object[]) A0625);
        Integer[] A07152 = A07(4, 1);
        A07152[2] = 4;
        A07152[3] = 4;
        A07152[4] = 2;
        builder.putAll((Object) "TL", (Object[]) A07152);
        Integer[] A07153 = A07(4, 2);
        A07153[2] = 1;
        A07153[3] = 2;
        A07153[4] = 2;
        builder.putAll((Object) "TM", (Object[]) A07153);
        Integer[] A07154 = A07(2, 1);
        A07154[2] = 1;
        A07154[3] = 1;
        A07154[4] = 2;
        builder.putAll((Object) "TN", (Object[]) A07154);
        Integer[] A0535 = A05(3);
        A0535[2] = 4;
        A0535[3] = 2;
        A0535[4] = 2;
        builder.putAll((Object) "TO", (Object[]) A0535);
        Integer[] A07155 = A07(1, 2);
        A07155[2] = 1;
        A07155[3] = 1;
        A07155[4] = 2;
        builder.putAll((Object) "TR", (Object[]) A07155);
        Integer[] A07156 = A07(1, 3);
        A07156[2] = 1;
        A07156[3] = 3;
        A07156[4] = 2;
        builder.putAll((Object) "TT", (Object[]) A07156);
        Integer[] A07157 = A07(3, 2);
        A07157[2] = 2;
        A07157[3] = 4;
        A07157[4] = 2;
        builder.putAll((Object) "TV", (Object[]) A07157);
        Integer[] A0626 = A06(0);
        A0626[3] = 0;
        A0626[4] = 1;
        builder.putAll((Object) "TW", (Object[]) A0626);
        Integer[] A0627 = A06(3);
        A0627[3] = 2;
        A0627[4] = 2;
        builder.putAll((Object) "TZ", (Object[]) A0627);
        Integer[] A07158 = A07(0, 3);
        A07158[2] = 0;
        A07158[3] = 0;
        A07158[4] = 2;
        builder.putAll((Object) "UA", (Object[]) A07158);
        Integer[] A07159 = A07(3, 2);
        A07159[2] = 2;
        A07159[3] = 3;
        A07159[4] = 2;
        builder.putAll((Object) "UG", (Object[]) A07159);
        Integer[] A07160 = A07(0, 1);
        A03(A07160, 3);
        builder.putAll((Object) "US", (Object[]) A07160);
        Integer[] A07161 = A07(2, 1);
        A07161[2] = 1;
        A07161[3] = 1;
        A07161[4] = 2;
        builder.putAll((Object) "UY", (Object[]) A07161);
        Integer[] A07162 = A07(2, 0);
        A07162[2] = 3;
        A07162[3] = 2;
        A07162[4] = 2;
        builder.putAll((Object) "UZ", (Object[]) A07162);
        Integer[] A0628 = A06(2);
        A0628[3] = 2;
        A0628[4] = 2;
        builder.putAll((Object) "VC", (Object[]) A0628);
        Integer[] A0629 = A06(4);
        A0629[3] = 4;
        A0629[4] = 2;
        builder.putAll((Object) "VE", (Object[]) A0629);
        Integer[] A0536 = A05(2);
        A0536[2] = 1;
        A0536[3] = 2;
        A0536[4] = 2;
        builder.putAll((Object) "VG", (Object[]) A0536);
        Integer[] A07163 = A07(1, 2);
        A07163[2] = 2;
        A07163[3] = 4;
        A07163[4] = 2;
        builder.putAll((Object) "VI", (Object[]) A07163);
        Integer[] A07164 = A07(0, 1);
        A07164[2] = 4;
        A07164[3] = 4;
        A07164[4] = 2;
        builder.putAll((Object) "VN", (Object[]) A07164);
        Integer[] A07165 = A07(4, 1);
        A07165[2] = 3;
        A07165[3] = 1;
        A07165[4] = 2;
        builder.putAll((Object) "VU", (Object[]) A07165);
        Integer[] A07166 = A07(3, 1);
        A07166[2] = 4;
        A07166[3] = 2;
        A07166[4] = 2;
        builder.putAll((Object) "WS", (Object[]) A07166);
        Integer[] A0630 = A06(1);
        A0630[3] = 0;
        A0630[4] = 2;
        builder.putAll((Object) "XK", (Object[]) A0630);
        Integer[] A0631 = A06(4);
        A0631[3] = 4;
        A0631[4] = 2;
        builder.putAll((Object) "YE", (Object[]) A0631);
        Integer[] A07167 = A07(3, 2);
        A07167[2] = 1;
        A07167[3] = 3;
        A07167[4] = 2;
        builder.putAll((Object) "YT", (Object[]) A07167);
        Integer[] A07168 = A07(2, 3);
        A03(A07168, 2);
        builder.putAll((Object) "ZA", (Object[]) A07168);
        Integer[] A07169 = A07(3, 2);
        A07169[2] = 2;
        A07169[3] = 3;
        A07169[4] = 2;
        builder.putAll((Object) "ZM", (Object[]) A07169);
        Integer[] A0632 = A06(3);
        A0632[3] = 3;
        A0632[4] = 2;
        builder.putAll((Object) "ZW", (Object[]) A0632);
        A0J = builder.build();
        A0I = AnonymousClass1Mr.of((Object) 6100000L, (Object) 3800000L, (Object) 2100000L, (Object) 1300000L, (Object) 590000L);
        A0E = AnonymousClass1Mr.of((Object) 218000L, (Object) 159000L, (Object) 145000L, (Object) 130000L, (Object) 112000L);
        A0F = AnonymousClass1Mr.of((Object) 2200000L, (Object) 1300000L, (Object) 930000L, (Object) 730000L, (Object) 530000L);
        A0G = AnonymousClass1Mr.of((Object) 4800000L, (Object) 2700000L, (Object) 1800000L, (Object) 1200000L, (Object) 630000L);
        A0H = AnonymousClass1Mr.of((Object) 12000000L, (Object) 8800000L, (Object) 5900000L, (Object) 3500000L, (Object) 1800000L);
    }

    @Deprecated
    public C67733Sr() {
        this(null, AnonymousClass5Xd.A00, AbstractC17190qP.of());
    }

    public C67733Sr(Context context, AnonymousClass5Xd r6, Map map) {
        Context applicationContext;
        int A04;
        C51812Yx r3;
        if (context == null) {
            applicationContext = null;
        } else {
            applicationContext = context.getApplicationContext();
        }
        this.A08 = applicationContext;
        this.A0C = AbstractC17190qP.copyOf(map);
        this.A09 = new AnonymousClass3CF();
        this.A0B = new C64683Gm();
        this.A0A = r6;
        if (context == null) {
            A04 = 0;
        } else {
            A04 = AnonymousClass3JZ.A04(context);
        }
        this.A00 = A04;
        this.A02 = A08(A04);
        if (context != null) {
            synchronized (C51812Yx.class) {
                if (C51812Yx.A02 == null) {
                    C51812Yx.A02 = new C51812Yx();
                    IntentFilter intentFilter = new IntentFilter();
                    intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
                    context.registerReceiver(C51812Yx.A02, intentFilter);
                }
                r3 = C51812Yx.A02;
            }
            synchronized (r3) {
                ArrayList arrayList = r3.A01;
                int size = arrayList.size();
                while (true) {
                    size--;
                    if (size < 0) {
                        arrayList.add(C12970iu.A10(this));
                        C12980iv.A18(r3.A00, r3, this, 7);
                    } else if (((Reference) arrayList.get(size)).get() == null) {
                        arrayList.remove(size);
                    }
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x002e A[Catch: all -> 0x00a3, TryCatch #0 {, blocks: (B:4:0x0003, B:8:0x000b, B:10:0x0019, B:13:0x0024, B:15:0x002e, B:16:0x0034, B:18:0x0040, B:19:0x0048), top: B:24:0x0003 }] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0040 A[Catch: all -> 0x00a3, TryCatch #0 {, blocks: (B:4:0x0003, B:8:0x000b, B:10:0x0019, B:13:0x0024, B:15:0x002e, B:16:0x0034, B:18:0x0040, B:19:0x0048), top: B:24:0x0003 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized X.C67733Sr A00(android.content.Context r11) {
        /*
            java.lang.Class<X.3Sr> r10 = X.C67733Sr.class
            monitor-enter(r10)
            X.3Sr r1 = X.C67733Sr.A0D     // Catch: all -> 0x00a3
            if (r1 != 0) goto L_0x00a1
            if (r11 != 0) goto L_0x000b
            r5 = 0
            goto L_0x0024
        L_0x000b:
            android.content.Context r5 = r11.getApplicationContext()     // Catch: all -> 0x00a3
            java.lang.String r0 = "phone"
            java.lang.Object r0 = r11.getSystemService(r0)     // Catch: all -> 0x00a3
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0     // Catch: all -> 0x00a3
            if (r0 == 0) goto L_0x0024
            java.lang.String r1 = r0.getNetworkCountryIso()     // Catch: all -> 0x00a3
            boolean r0 = android.text.TextUtils.isEmpty(r1)     // Catch: all -> 0x00a3
            if (r0 != 0) goto L_0x0024
            goto L_0x002c
        L_0x0024:
            java.util.Locale r0 = java.util.Locale.getDefault()     // Catch: all -> 0x00a3
            java.lang.String r1 = r0.getCountry()     // Catch: all -> 0x00a3
        L_0x002c:
            if (r1 == 0) goto L_0x0034
            java.util.Locale r0 = java.util.Locale.US     // Catch: all -> 0x00a3
            java.lang.String r1 = r1.toUpperCase(r0)     // Catch: all -> 0x00a3
        L_0x0034:
            X.3tP r0 = X.C67733Sr.A0J     // Catch: all -> 0x00a3
            X.1Mr r7 = r0.get(r1)     // Catch: all -> 0x00a3
            boolean r0 = r7.isEmpty()     // Catch: all -> 0x00a3
            if (r0 == 0) goto L_0x0048
            java.lang.Integer r0 = X.C12970iu.A0g()     // Catch: all -> 0x00a3
            X.1Mr r7 = X.AnonymousClass1Mr.of(r0, r0, r0, r0, r0)     // Catch: all -> 0x00a3
        L_0x0048:
            r0 = 6
            java.util.HashMap r6 = new java.util.HashMap     // Catch: all -> 0x00a3
            r6.<init>(r0)     // Catch: all -> 0x00a3
            r4 = 0
            java.lang.Integer r2 = java.lang.Integer.valueOf(r4)     // Catch: all -> 0x00a3
            r0 = 1000000(0xf4240, double:4.940656E-318)
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch: all -> 0x00a3
            r6.put(r2, r0)     // Catch: all -> 0x00a3
            r9 = 2
            java.lang.Integer r0 = java.lang.Integer.valueOf(r9)     // Catch: all -> 0x00a3
            X.1Mr r3 = X.C67733Sr.A0I     // Catch: all -> 0x00a3
            A02(r0, r6, r7, r3, r4)     // Catch: all -> 0x00a3
            r8 = 3
            java.lang.Integer r2 = java.lang.Integer.valueOf(r8)     // Catch: all -> 0x00a3
            X.1Mr r1 = X.C67733Sr.A0E     // Catch: all -> 0x00a3
            r0 = 1
            A02(r2, r6, r7, r1, r0)     // Catch: all -> 0x00a3
            r2 = 4
            java.lang.Integer r1 = java.lang.Integer.valueOf(r2)     // Catch: all -> 0x00a3
            X.1Mr r0 = X.C67733Sr.A0F     // Catch: all -> 0x00a3
            A02(r1, r6, r7, r0, r9)     // Catch: all -> 0x00a3
            java.lang.Integer r1 = X.C12980iv.A0k()     // Catch: all -> 0x00a3
            X.1Mr r0 = X.C67733Sr.A0G     // Catch: all -> 0x00a3
            A02(r1, r6, r7, r0, r8)     // Catch: all -> 0x00a3
            r0 = 9
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)     // Catch: all -> 0x00a3
            X.1Mr r0 = X.C67733Sr.A0H     // Catch: all -> 0x00a3
            A02(r1, r6, r7, r0, r2)     // Catch: all -> 0x00a3
            r0 = 7
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch: all -> 0x00a3
            A02(r0, r6, r7, r3, r4)     // Catch: all -> 0x00a3
            X.5Xd r0 = X.AnonymousClass5Xd.A00     // Catch: all -> 0x00a3
            X.3Sr r1 = new X.3Sr     // Catch: all -> 0x00a3
            r1.<init>(r5, r0, r6)     // Catch: all -> 0x00a3
            X.C67733Sr.A0D = r1     // Catch: all -> 0x00a3
        L_0x00a1:
            monitor-exit(r10)
            return r1
        L_0x00a3:
            r0 = move-exception
            monitor-exit(r10)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C67733Sr.A00(android.content.Context):X.3Sr");
    }

    public static /* synthetic */ void A01(C67733Sr r11) {
        int i;
        synchronized (r11) {
            Context context = r11.A08;
            int A04 = context == null ? 0 : AnonymousClass3JZ.A04(context);
            if (r11.A00 != A04) {
                r11.A00 = A04;
                if (!(A04 == 1 || A04 == 0 || A04 == 8)) {
                    long A08 = r11.A08(A04);
                    r11.A02 = A08;
                    long elapsedRealtime = SystemClock.elapsedRealtime();
                    if (r11.A01 > 0) {
                        i = (int) (elapsedRealtime - r11.A05);
                    } else {
                        i = 0;
                    }
                    r11.A09(i, r11.A04, A08);
                    r11.A05 = elapsedRealtime;
                    r11.A04 = 0;
                    r11.A06 = 0;
                    r11.A07 = 0;
                    C64683Gm r1 = r11.A0B;
                    r1.A05.clear();
                    r1.A00 = -1;
                    r1.A01 = 0;
                    r1.A03 = 0;
                }
            }
        }
    }

    public static void A02(Object obj, AbstractMap abstractMap, List list, List list2, int i) {
        abstractMap.put(obj, list2.get(((Integer) list.get(i)).intValue()));
    }

    public static void A03(Object[] objArr, Object obj) {
        objArr[2] = obj;
        objArr[3] = obj;
        objArr[4] = obj;
    }

    public static void A04(Object[] objArr, Object obj) {
        objArr[2] = obj;
        objArr[3] = obj;
        objArr[4] = obj;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v1, resolved type: java.lang.Integer[] */
    /* JADX WARN: Multi-variable type inference failed */
    public static Integer[] A05(Object obj) {
        Integer[] numArr = new Integer[5];
        numArr[0] = obj;
        numArr[1] = obj;
        return numArr;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v1, resolved type: java.lang.Integer[] */
    /* JADX WARN: Multi-variable type inference failed */
    public static Integer[] A06(Object obj) {
        Integer[] numArr = new Integer[5];
        numArr[0] = obj;
        numArr[1] = obj;
        numArr[2] = obj;
        return numArr;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v1, resolved type: java.lang.Integer[] */
    /* JADX WARN: Multi-variable type inference failed */
    public static Integer[] A07(Object obj, Object obj2) {
        Integer[] numArr = new Integer[5];
        numArr[0] = obj;
        numArr[1] = obj2;
        return numArr;
    }

    public final long A08(int i) {
        AbstractC17190qP r1 = this.A0C;
        Number number = (Number) r1.get(Integer.valueOf(i));
        if (number == null && (number = (Number) r1.get(C12980iv.A0i())) == null) {
            number = Long.valueOf((long) SearchActionVerificationClientService.MS_TO_NS);
        }
        return number.longValue();
    }

    public final void A09(int i, long j, long j2) {
        if (i != 0 || j != 0 || j2 != this.A03) {
            this.A03 = j2;
            Iterator it = this.A09.A00.iterator();
            while (it.hasNext()) {
                AnonymousClass4PD r3 = (AnonymousClass4PD) it.next();
                if (!r3.A00) {
                    r3.A01.post(new RunnableBRunnable0Shape0S0101200_I1(r3, i, 1, j, j2));
                }
            }
        }
    }
}
