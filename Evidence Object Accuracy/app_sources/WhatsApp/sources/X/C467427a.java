package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0101000_I0;
import com.facebook.redex.RunnableBRunnable0Shape10S0100000_I0_10;
import com.facebook.redex.RunnableBRunnable0Shape7S0200000_I0_7;
import com.whatsapp.qrcode.QrScannerViewV2;

/* renamed from: X.27a  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C467427a implements AbstractC467527b {
    public final /* synthetic */ QrScannerViewV2 A00;

    @Override // X.AbstractC467527b
    public void AMh(float f, float f2) {
    }

    @Override // X.AbstractC467527b
    public void AMi(boolean z) {
    }

    @Override // X.AbstractC467527b
    public void AYD() {
    }

    public C467427a(QrScannerViewV2 qrScannerViewV2) {
        this.A00 = qrScannerViewV2;
    }

    @Override // X.AbstractC467527b
    public void ANb(int i) {
        QrScannerViewV2 qrScannerViewV2 = this.A00;
        if (qrScannerViewV2.A05 != null) {
            qrScannerViewV2.A08.post(new RunnableBRunnable0Shape0S0101000_I0(this, i, 19));
        }
    }

    @Override // X.AbstractC467527b
    public void AUF() {
        QrScannerViewV2 qrScannerViewV2 = this.A00;
        if (qrScannerViewV2.A05 != null) {
            qrScannerViewV2.A08.post(new RunnableBRunnable0Shape10S0100000_I0_10(this, 15));
        }
    }

    @Override // X.AbstractC467527b
    public void AUS(C49262Kb r4) {
        QrScannerViewV2 qrScannerViewV2 = this.A00;
        if (qrScannerViewV2.A05 != null) {
            qrScannerViewV2.A08.post(new RunnableBRunnable0Shape7S0200000_I0_7(this, 28, r4));
        }
    }
}
