package X;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.ServiceConfigurationError;
import java.util.ServiceLoader;
import java.util.logging.Level;
import java.util.logging.Logger;

/* renamed from: X.4cf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC94864cf {
    public static final Logger zza = C72463ee.A0M(AnonymousClass4Z3.class);
    public static final String zzb = "com.google.android.gms.internal.gtm.zzsq";

    public abstract C94754cU zza();

    public static C94754cU zzb(Class cls) {
        String str;
        ClassLoader classLoader = AbstractC94864cf.class.getClassLoader();
        if (cls.equals(C94754cU.class)) {
            str = zzb;
        } else if (cls.getPackage().equals(AbstractC94864cf.class.getPackage())) {
            Object[] A1a = C12980iv.A1a();
            A1a[0] = cls.getPackage().getName();
            A1a[1] = cls.getSimpleName();
            str = String.format("%s.BlazeGenerated%sLoader", A1a);
        } else {
            throw C12970iu.A0f(cls.getName());
        }
        try {
            try {
                try {
                    try {
                        Class.forName(str, true, classLoader).getConstructor(new Class[0]).newInstance(new Object[0]);
                        return (C94754cU) cls.cast(new C79713r3(null));
                    } catch (IllegalAccessException e) {
                        throw new IllegalStateException(e);
                    }
                } catch (InvocationTargetException e2) {
                    throw new IllegalStateException(e2);
                }
            } catch (InstantiationException e3) {
                throw new IllegalStateException(e3);
            } catch (NoSuchMethodException e4) {
                throw new IllegalStateException(e4);
            }
        } catch (ClassNotFoundException unused) {
            Iterator it = ServiceLoader.load(AbstractC94864cf.class, classLoader).iterator();
            ArrayList A0l = C12960it.A0l();
            while (it.hasNext()) {
                try {
                    it.next();
                    A0l.add(cls.cast(new C79713r3(null)));
                } catch (ServiceConfigurationError e5) {
                    zza.logp(Level.SEVERE, "com.google.protobuf.GeneratedExtensionRegistryLoader", "load", C12960it.A0c(cls.getSimpleName(), "Unable to load "), (Throwable) e5);
                }
            }
            if (A0l.size() == 1) {
                return (C94754cU) A0l.get(0);
            }
            if (A0l.size() == 0) {
                return null;
            }
            try {
                return (C94754cU) cls.getMethod("combine", Collection.class).invoke(null, A0l);
            } catch (IllegalAccessException e6) {
                throw new IllegalStateException(e6);
            } catch (NoSuchMethodException e7) {
                throw new IllegalStateException(e7);
            } catch (InvocationTargetException e8) {
                throw new IllegalStateException(e8);
            }
        }
    }
}
