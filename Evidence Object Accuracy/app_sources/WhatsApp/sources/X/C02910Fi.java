package X;

import android.database.sqlite.SQLiteDatabase;

/* renamed from: X.0Fi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02910Fi extends AnonymousClass0OL {
    public C02910Fi() {
        super(4, 5);
    }

    @Override // X.AnonymousClass0OL
    public void A00(AbstractC12920im r3) {
        SQLiteDatabase sQLiteDatabase = ((AnonymousClass0ZE) r3).A00;
        sQLiteDatabase.execSQL("ALTER TABLE workspec ADD COLUMN `trigger_content_update_delay` INTEGER NOT NULL DEFAULT -1");
        sQLiteDatabase.execSQL("ALTER TABLE workspec ADD COLUMN `trigger_max_content_delay` INTEGER NOT NULL DEFAULT -1");
    }
}
