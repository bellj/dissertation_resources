package X;

import com.whatsapp.payments.ui.BrazilPayBloksActivity;
import com.whatsapp.payments.ui.PaymentsUnavailableDialogFragment;
import java.util.HashMap;

/* renamed from: X.5wc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final /* synthetic */ class C128755wc {
    public final /* synthetic */ AnonymousClass3FE A00;
    public final /* synthetic */ BrazilPayBloksActivity A01;

    public /* synthetic */ C128755wc(AnonymousClass3FE r1, BrazilPayBloksActivity brazilPayBloksActivity) {
        this.A01 = brazilPayBloksActivity;
        this.A00 = r1;
    }

    public final void A00(C452120p r5) {
        BrazilPayBloksActivity brazilPayBloksActivity = this.A01;
        AnonymousClass3FE r2 = this.A00;
        if (r5 == null) {
            C117315Zl.A0T(r2);
            return;
        }
        brazilPayBloksActivity.A0E.A01.A06(C12960it.A0b("maybeHandleUnderageError: ", r5));
        if (r5.A00 == 2896001) {
            brazilPayBloksActivity.Adm(PaymentsUnavailableDialogFragment.A01());
            return;
        }
        HashMap A11 = C12970iu.A11();
        C117295Zj.A1D(r5, A11);
        r2.A01("on_failure", A11);
    }
}
