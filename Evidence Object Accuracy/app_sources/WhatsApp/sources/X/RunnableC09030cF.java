package X;

/* renamed from: X.0cF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09030cF implements Runnable {
    public final /* synthetic */ C02360Bs A00;

    public RunnableC09030cF(C02360Bs r1) {
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        C02360Bs r1 = this.A00;
        r1.A06 = null;
        r1.drawableStateChanged();
    }
}
