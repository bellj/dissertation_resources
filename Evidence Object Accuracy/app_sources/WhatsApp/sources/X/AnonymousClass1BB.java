package X;

import android.database.Cursor;

/* renamed from: X.1BB  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1BB {
    public final C16510p9 A00;
    public final C15240mn A01;
    public final C16490p7 A02;

    public AnonymousClass1BB(C16510p9 r1, C15240mn r2, C16490p7 r3) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
    }

    public int A00(AnonymousClass02N r8, AbstractC14640lm r9) {
        C16310on A01 = this.A02.get();
        try {
            Cursor A07 = A01.A03.A07(r8, "SELECT COUNT(*) as count FROM available_message_view WHERE message_type IN ('9', '26' ) AND origin != 7 AND chat_row_id = ?", new String[]{String.valueOf(this.A00.A02(r9))});
            if (A07.moveToFirst()) {
                int i = A07.getInt(A07.getColumnIndexOrThrow("count"));
                A07.close();
                A01.close();
                return i;
            }
            A07.close();
            A01.close();
            return 0;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
