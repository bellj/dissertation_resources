package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4jH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98804jH implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        String str = null;
        String str2 = null;
        C78513p2 r3 = null;
        int i = 0;
        int i2 = 0;
        boolean z = false;
        int i3 = 0;
        boolean z2 = false;
        int i4 = 0;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 1:
                    i = C95664e9.A02(parcel, readInt);
                    break;
                case 2:
                    i2 = C95664e9.A02(parcel, readInt);
                    break;
                case 3:
                    z = C12960it.A1S(C95664e9.A02(parcel, readInt));
                    break;
                case 4:
                    i3 = C95664e9.A02(parcel, readInt);
                    break;
                case 5:
                    z2 = C12960it.A1S(C95664e9.A02(parcel, readInt));
                    break;
                case 6:
                    str = C95664e9.A08(parcel, readInt);
                    break;
                case 7:
                    i4 = C95664e9.A02(parcel, readInt);
                    break;
                case '\b':
                    str2 = C95664e9.A08(parcel, readInt);
                    break;
                case '\t':
                    r3 = (C78513p2) C95664e9.A07(parcel, C78513p2.CREATOR, readInt);
                    break;
                default:
                    C95664e9.A0D(parcel, readInt);
                    break;
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78633pE(r3, str, str2, i, i2, i3, i4, z, z2);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new C78633pE[i];
    }
}
