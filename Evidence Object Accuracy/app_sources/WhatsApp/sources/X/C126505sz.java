package X;

/* renamed from: X.5sz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C126505sz {
    public final AnonymousClass1V8 A00;

    public C126505sz(AnonymousClass3CS r13, String str, String str2, String str3, long j) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy A0N = C117295Zj.A0N(A0M);
        C41141sy.A01(A0N, "action", "upi-get-accounts");
        if (C117295Zj.A1V(str, 1, false)) {
            C41141sy.A01(A0N, "device-id", str);
        }
        if (AnonymousClass3JT.A0D(Long.valueOf(j), -9007199254740991L, false)) {
            C117315Zl.A0X(A0N, "code", j);
        }
        if (AnonymousClass3JT.A0E(str2, 1, 10, false)) {
            C41141sy.A01(A0N, "provider-type", str2);
        }
        if (str3 != null && C117295Zj.A1W(str3, 0, true)) {
            C41141sy.A01(A0N, "bank-ref-id", str3);
        }
        this.A00 = C117295Zj.A0I(A0N, A0M, r13);
    }
}
