package X;

import android.database.sqlite.SQLiteFullException;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1t7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C41191t7 {
    public static final C28601Of A0J;
    public static final String[] A0K = new String[0];
    public final AbstractC15710nm A00;
    public final C15570nT A01;
    public final C240313y A02;
    public final C234511s A03;
    public final C41181t6 A04;
    public final C233411h A05;
    public final C233711k A06;
    public final C233811l A07;
    public final C233311g A08;
    public final C234011n A09;
    public final C230910i A0A;
    public final C14830m7 A0B;
    public final C233611j A0C;
    public final C21910yB A0D;
    public final C22100yW A0E;
    public final C232510y A0F;
    public final C233511i A0G;
    public final C14850m9 A0H;
    public final C32881ct A0I;

    static {
        AnonymousClass1YB r2 = new AnonymousClass1YB();
        r2.A01(400, 64);
        r2.A01(404, 65);
        r2.A01(405, 66);
        r2.A01(406, 67);
        A0J = r2.A00();
    }

    public C41191t7(AbstractC15710nm r2, C15570nT r3, C240313y r4, C234511s r5, C41181t6 r6, C233411h r7, C233711k r8, C233811l r9, C233311g r10, C234011n r11, C230910i r12, C14830m7 r13, C233611j r14, C21910yB r15, C22100yW r16, C232510y r17, C233511i r18, C14850m9 r19, C32881ct r20) {
        this.A0B = r13;
        this.A0H = r19;
        this.A00 = r2;
        this.A01 = r3;
        this.A0D = r15;
        this.A08 = r10;
        this.A09 = r11;
        this.A05 = r7;
        this.A0G = r18;
        this.A0A = r12;
        this.A07 = r9;
        this.A0F = r17;
        this.A0E = r16;
        this.A0C = r14;
        this.A06 = r8;
        this.A02 = r4;
        this.A03 = r5;
        this.A0I = r20;
        this.A04 = r6;
    }

    public final void A00() {
        this.A0I.A01();
        C41181t6 r2 = this.A04;
        Log.i("sync-manager/onFailed");
        C18850tA r1 = r2.A03;
        C18850tA.A00(r1, false);
        if (r1.A0P()) {
            r2.A01.A05();
        }
    }

    public final void A01(C34891gs r5, String str) {
        StringBuilder sb = new StringBuilder("SyncResponseHandler/handleMutationException failed with MutationException, reason ");
        sb.append(r5.reason);
        Log.e(sb.toString(), r5);
        int i = r5.reason;
        if (i == 0) {
            A02(r5, str, 19);
            throw new RuntimeException("Redex: Unreachable code after no-return invoke");
        } else if (i == 1) {
            A02(r5, str, 21);
            throw new RuntimeException("Redex: Unreachable code after no-return invoke");
        } else if (i == 2) {
            A02(r5, str, 23);
            throw new RuntimeException("Redex: Unreachable code after no-return invoke");
        } else if (i == 3) {
            A02(r5, str, 24);
            throw new RuntimeException("Redex: Unreachable code after no-return invoke");
        } else if (i == 4) {
            A02(r5, str, 22);
            throw new RuntimeException("Redex: Unreachable code after no-return invoke");
        } else if (i == 6) {
            this.A05.A01.A06("unsupported_action_counter", 1);
        } else if (i == 7) {
            A02(r5, str, 25);
            throw new RuntimeException("Redex: Unreachable code after no-return invoke");
        } else {
            StringBuilder sb2 = new StringBuilder("Unhandled MutationException with reason: ");
            sb2.append(i);
            throw new RuntimeException(sb2.toString());
        }
    }

    public final void A02(C34891gs r5, String str, int i) {
        this.A00.AaV("malformed_syncd_mutation", r5.getMessage(), false);
        this.A05.A01.A06("invalid_action_counter", 1);
        throw new AnonymousClass1K1(i, str);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0069, code lost:
        if (r10 != false) goto L_0x0027;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A03(java.lang.Long r8, boolean r9, boolean r10) {
        /*
            r7 = this;
            if (r9 == 0) goto L_0x0069
            X.11k r6 = r7.A06
            android.content.SharedPreferences r0 = r6.A01()
            java.lang.String r5 = "first_transient_server_failure_timestamp"
            r3 = 0
            long r1 = r0.getLong(r5, r3)
            int r0 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r0 != 0) goto L_0x0053
            long r1 = java.lang.System.currentTimeMillis()
            android.content.SharedPreferences r0 = r6.A01()
            android.content.SharedPreferences$Editor r0 = r0.edit()
            android.content.SharedPreferences$Editor r0 = r0.putLong(r5, r1)
            r0.apply()
        L_0x0027:
            if (r8 == 0) goto L_0x0097
            X.1t6 r2 = r7.A04
            long r3 = r8.longValue()
            java.lang.String r1 = "sync-manager/onRetryNeeded "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r3)
            java.lang.String r0 = r0.toString()
            com.whatsapp.util.Log.i(r0)
            X.0tA r6 = r2.A03
            r0 = 0
            X.C18850tA.A00(r6, r0)
            boolean r0 = r6.A0P()
            if (r0 == 0) goto L_0x006c
            X.10w r0 = r2.A01
            r0.A05()
            return
        L_0x0053:
            long r3 = java.lang.System.currentTimeMillis()
            long r3 = r3 - r1
            r1 = 604800000(0x240c8400, double:2.988109026E-315)
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x0027
            r0 = 1
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            r0 = 0
            r7.A04(r0, r1)
            return
        L_0x0069:
            if (r10 == 0) goto L_0x0097
            goto L_0x0027
        L_0x006c:
            monitor-enter(r6)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: all -> 0x0094
            r1.<init>()     // Catch: all -> 0x0094
            java.lang.String r0 = "sync-manager/scheduleSync with delay "
            r1.append(r0)     // Catch: all -> 0x0094
            r1.append(r3)     // Catch: all -> 0x0094
            java.lang.String r0 = r1.toString()     // Catch: all -> 0x0094
            com.whatsapp.util.Log.i(r0)     // Catch: all -> 0x0094
            X.0lR r5 = r6.A0e     // Catch: all -> 0x0094
            java.lang.String r2 = "SyncManager/scheduleSync"
            r1 = 1
            com.whatsapp.util.RunnableTRunnableShape17S0100000_I0 r0 = new com.whatsapp.util.RunnableTRunnableShape17S0100000_I0     // Catch: all -> 0x0094
            r0.<init>(r6, r1)     // Catch: all -> 0x0094
            java.lang.Runnable r0 = r5.AbK(r0, r2, r3)     // Catch: all -> 0x0094
            r6.A01 = r0     // Catch: all -> 0x0094
            monitor-exit(r6)     // Catch: all -> 0x0094
            return
        L_0x0094:
            r0 = move-exception
            monitor-exit(r6)     // Catch: all -> 0x0094
            throw r0
        L_0x0097:
            r7.A00()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C41191t7.A03(java.lang.Long, boolean, boolean):void");
    }

    public void A04(String str, Integer num) {
        String obj;
        StringBuilder sb = new StringBuilder("SyncResponseHandler/fatalFailure reason:");
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        sb.append(obj);
        Log.e(sb.toString());
        AbstractC15710nm r2 = this.A00;
        StringBuilder sb2 = new StringBuilder("errorCode:");
        sb2.append(num);
        sb2.append("; collectionName:");
        sb2.append(str);
        r2.AaV("app-sate-sync-handle-fatal-exception", sb2.toString(), false);
        if (num != null) {
            C233411h r22 = this.A05;
            int intValue = num.intValue();
            C41291tI r1 = new C41291tI();
            r1.A01 = Integer.valueOf(intValue);
            r1.A00 = C233411h.A03(str);
            r22.A06.A07(r1);
        }
        this.A0I.A01();
        this.A06.A01().edit().remove("first_transient_server_failure_timestamp").apply();
        C41181t6 r3 = this.A04;
        StringBuilder sb3 = new StringBuilder("sync-manager/onFatalFailure for collection ");
        sb3.append(str);
        Log.i(sb3.toString());
        r3.A01.A05();
        C18850tA r23 = r3.A03;
        C18850tA.A00(r23, false);
        Iterator it = r3.A04.A01().iterator();
        while (it.hasNext()) {
            it.next();
        }
        synchronized (r23) {
            r23.A00 = null;
        }
        r3.A00.A08();
        r23.A0J(1);
    }

    public void A05(Throwable th) {
        boolean z;
        boolean z2;
        Long A00;
        if (th instanceof AnonymousClass1K1) {
            AnonymousClass1K1 r5 = (AnonymousClass1K1) th;
            A04(r5.collectionName, Integer.valueOf(r5.errorCode));
        } else if (th instanceof AnonymousClass1KE) {
            A00();
        } else {
            if (th instanceof AnonymousClass1t4) {
                z = ((AnonymousClass1t4) th).isServerTransient;
                z2 = !z;
                A00 = this.A0I.A00();
            } else {
                boolean z3 = th instanceof SQLiteFullException;
                z = false;
                z2 = true;
                A00 = this.A0I.A00();
                if (!z3) {
                    A03(A00, true, false);
                    return;
                }
            }
            A03(A00, z, z2);
        }
    }

    public final void A06(List list) {
        List list2;
        Iterator it = list.iterator();
        while (it.hasNext()) {
            C41301tJ r3 = (C41301tJ) it.next();
            String str = r3.A01;
            StringBuilder sb = new StringBuilder("SyncResponseHandler/handleIncomingPatches for collection=");
            sb.append(str);
            sb.append("; size=");
            List list3 = r3.A02;
            sb.append(list3.size());
            sb.append("; hasMorePatch=");
            boolean z = r3.A03;
            sb.append(z);
            Log.i(sb.toString());
            C15570nT r1 = this.A01;
            r1.A08();
            AnonymousClass1K3 r32 = r3.A00;
            if (r32 != null) {
                this.A0F.A00(str);
                r1.A08();
                AnonymousClass009.A0B("Only companion devices that are undergoing bootstrap should receive and handle snapshots.", false);
                this.A0B.A00();
                try {
                    AnonymousClass1t2 r4 = new AnonymousClass1t2(this.A03, r32, str);
                    C234511s.A00(r4, str, true);
                    try {
                        try {
                            File file = r4.A00;
                            C41221tA r9 = (C41221tA) AbstractC27091Fz.A0E(C41221tA.A05, AnonymousClass1NM.A00(file));
                            if (r4.A01.delete()) {
                                file.delete();
                            }
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("external-mutations-downloader: downloaded snapshot= ");
                            sb2.append(r9);
                            Log.i(sb2.toString());
                            if ((r9.A00 & 1) == 1) {
                                AnonymousClass1tB r12 = r9.A04;
                                if (r12 == null) {
                                    r12 = AnonymousClass1tB.A02;
                                }
                                if ((r12.A00 & 1) == 1) {
                                    long j = r12.A01;
                                    StringBuilder sb3 = new StringBuilder("SyncResponseHandler/handleIncomingPatches applying snapshot for collection ");
                                    sb3.append(str);
                                    sb3.append(" with version: ");
                                    sb3.append(j);
                                    Log.i(sb3.toString());
                                    ArrayList arrayList = new ArrayList();
                                    for (C27931Ju r11 : r9.A02) {
                                        AnonymousClass1G4 A0T = C27941Jv.A03.A0T();
                                        EnumC27951Jw r122 = EnumC27951Jw.A02;
                                        A0T.A03();
                                        C27941Jv r42 = (C27941Jv) A0T.A00;
                                        r42.A00 |= 1;
                                        r42.A01 = r122.value;
                                        A0T.A03();
                                        C27941Jv r43 = (C27941Jv) A0T.A00;
                                        r43.A02 = r11;
                                        r43.A00 |= 2;
                                        arrayList.add(A0T.A02());
                                    }
                                    if (!A07(null, r9, str, arrayList, j)) {
                                        continue;
                                    }
                                }
                            }
                            Log.e("SyncResponseHandler/handleIncomingPatches snapshot has no version, cannot apply");
                        } catch (IOException e) {
                            Log.e("external-mutations-downloader/failed to read snapshot", e);
                            throw new AnonymousClass1KE("Failed to read snapshot from file", e);
                        }
                    } catch (C28971Pt e2) {
                        Log.e("external-mutations-downloader/failed to parse snapshot into ProtoBuf", e2);
                        throw new AnonymousClass1K1(70, str);
                    }
                } catch (Exception e3) {
                    throw e3;
                }
            }
            Iterator it2 = list3.iterator();
            while (true) {
                if (it2.hasNext()) {
                    AnonymousClass1K4 r10 = (AnonymousClass1K4) it2.next();
                    if ((r10.A00 & 1) == 1) {
                        AnonymousClass1tB r13 = r10.A08;
                        if (r13 == null) {
                            r13 = AnonymousClass1tB.A02;
                        }
                        if ((r13.A00 & 1) == 1) {
                            long j2 = r13.A01;
                            StringBuilder sb4 = new StringBuilder("SyncResponseHandler/handleIncomingPatches applying patch for collection ");
                            sb4.append(str);
                            sb4.append(" with version: ");
                            sb4.append(j2);
                            Log.i(sb4.toString());
                            this.A0B.A00();
                            try {
                                if ((r10.A00 & 2) == 2) {
                                    C234511s r14 = this.A03;
                                    AnonymousClass1K3 r0 = r10.A06;
                                    if (r0 == null) {
                                        r0 = AnonymousClass1K3.A07;
                                    }
                                    AnonymousClass1t2 r92 = new AnonymousClass1t2(r14, r0, str);
                                    C234511s.A00(r92, str, false);
                                    try {
                                        try {
                                            File file2 = r92.A00;
                                            AnonymousClass1K5 r5 = (AnonymousClass1K5) AbstractC27091Fz.A0E(AnonymousClass1K5.A01, AnonymousClass1NM.A00(file2));
                                            if (r92.A01.delete()) {
                                                file2.delete();
                                            }
                                            StringBuilder sb5 = new StringBuilder();
                                            sb5.append("external-mutations-downloader: downloaded mutations= ");
                                            sb5.append(r5);
                                            Log.i(sb5.toString());
                                            list2 = r5.A00;
                                        } catch (IOException e4) {
                                            Log.e("external-mutations-downloader/failed to read mutations", e4);
                                            throw new AnonymousClass1KE("Failed to read mutations from file", e4);
                                        }
                                    } catch (C28971Pt e5) {
                                        Log.e("external-mutations-downloader/failed to parse mutations into ProtoBuf", e5);
                                        throw new AnonymousClass1K1(26, str);
                                    }
                                } else {
                                    list2 = r10.A04;
                                }
                                this.A05.A01.A06("mutation_counter", (long) list2.size());
                                if (!A07(r10, null, str, list2, j2)) {
                                    break;
                                }
                            } catch (Exception e6) {
                                throw e6;
                            }
                        }
                    }
                    Log.e("SyncResponseHandler/handleIncomingPatches incoming patch has no version. Cannot apply.");
                } else if (!z) {
                    this.A0F.A02(str);
                }
            }
        }
    }

    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    /* JADX WARNING: Removed duplicated region for block: B:326:0x06a3 A[Catch: 1tG -> 0x0c1d, all -> 0x0c44, TryCatch #20 {all -> 0x0c49, blocks: (B:11:0x0027, B:541:0x0b30, B:576:0x0c15, B:16:0x0031, B:19:0x0042, B:20:0x0046, B:23:0x005a, B:24:0x0084, B:31:0x008d, B:32:0x009f, B:34:0x00a5, B:36:0x00b1, B:38:0x00b5, B:40:0x00b9, B:41:0x00bb, B:43:0x00c2, B:45:0x00c6, B:46:0x00c8, B:48:0x00cd, B:50:0x00d1, B:52:0x00d5, B:53:0x00d7, B:55:0x00dc, B:57:0x00e0, B:59:0x00e4, B:61:0x00e8, B:62:0x00ea, B:64:0x00ef, B:66:0x00f3, B:68:0x0102, B:69:0x0104, B:72:0x0110, B:73:0x0113, B:74:0x0116, B:75:0x0118, B:76:0x011c, B:77:0x011f, B:78:0x0132, B:79:0x0133, B:80:0x0136, B:81:0x0138, B:83:0x013c, B:84:0x013e, B:85:0x0155, B:86:0x015c, B:87:0x015d, B:88:0x0164, B:89:0x0165, B:90:0x016c, B:91:0x016d, B:92:0x0174, B:93:0x0175, B:94:0x017c, B:99:0x0183, B:101:0x0189, B:103:0x018d, B:104:0x018f, B:107:0x0195, B:108:0x01a1, B:110:0x01a4, B:112:0x01ab, B:114:0x01af, B:115:0x01b1, B:117:0x01b6, B:118:0x01c4, B:120:0x01d2, B:121:0x01d7, B:122:0x01dc, B:124:0x01ed, B:126:0x01f7, B:128:0x01fb, B:129:0x0210, B:131:0x0216, B:132:0x0222, B:134:0x0228, B:135:0x022a, B:137:0x0242, B:138:0x0247, B:139:0x026e, B:141:0x0274, B:142:0x027a, B:144:0x02c5, B:145:0x02cd, B:147:0x02d7, B:152:0x02e5, B:154:0x02ee, B:158:0x02f5, B:160:0x02f9, B:163:0x0303, B:165:0x0308, B:167:0x030c, B:168:0x030f, B:173:0x0329, B:175:0x032f, B:176:0x033a, B:177:0x033b, B:179:0x0341, B:180:0x034c, B:181:0x034d, B:182:0x0351, B:184:0x037a, B:186:0x0382, B:188:0x038f, B:190:0x0396, B:191:0x039c, B:192:0x039d, B:193:0x03a2, B:194:0x03a3, B:196:0x03a7, B:199:0x03b7, B:201:0x03c2, B:203:0x03c9, B:204:0x03cf, B:205:0x03d0, B:206:0x03d5, B:207:0x03d6, B:208:0x03ec, B:209:0x03f3, B:211:0x03f6, B:212:0x0401, B:213:0x0402, B:214:0x0415, B:215:0x0416, B:216:0x0423, B:217:0x0424, B:218:0x0435, B:220:0x043e, B:221:0x0447, B:222:0x0448, B:223:0x044f, B:225:0x0451, B:227:0x045d, B:228:0x0464, B:230:0x046e, B:231:0x047f, B:232:0x04af, B:234:0x04b5, B:236:0x04c1, B:237:0x04c7, B:239:0x04dd, B:242:0x04e5, B:243:0x04ee, B:245:0x04f4, B:246:0x04f6, B:248:0x050a, B:250:0x0519, B:251:0x051e, B:252:0x0525, B:253:0x0526, B:255:0x052e, B:258:0x0536, B:259:0x053f, B:261:0x0545, B:262:0x0547, B:264:0x055b, B:265:0x0560, B:267:0x0566, B:275:0x05a0, B:276:0x05a3, B:277:0x05ac, B:279:0x05b2, B:280:0x05b8, B:282:0x05c2, B:284:0x05cd, B:285:0x05d6, B:286:0x05da, B:288:0x05e0, B:289:0x05e6, B:291:0x05f1, B:292:0x05fa, B:293:0x0605, B:295:0x060b, B:297:0x0617, B:298:0x061d, B:299:0x0621, B:301:0x0627, B:302:0x064d, B:304:0x0653, B:306:0x065c, B:308:0x0666, B:312:0x067e, B:317:0x0689, B:324:0x069b, B:326:0x06a3, B:327:0x06a8, B:329:0x06ae, B:330:0x06b8, B:332:0x06cb, B:333:0x06d7, B:334:0x06db, B:336:0x06e1, B:521:0x0a8c, B:522:0x0a93, B:523:0x0a97, B:525:0x0a9d, B:528:0x0aaa, B:538:0x0b27, B:545:0x0b38, B:546:0x0b44, B:548:0x0b46, B:549:0x0b52, B:550:0x0b53, B:551:0x0b5f, B:552:0x0b60, B:553:0x0b6c, B:555:0x0b6e, B:556:0x0b79, B:558:0x0b7b, B:559:0x0b87, B:560:0x0b88, B:561:0x0b94, B:563:0x0b96, B:564:0x0ba2, B:574:0x0bfe, B:575:0x0c09, B:581:0x0c1e, B:582:0x0c36, B:583:0x0c37, B:584:0x0c43, B:268:0x056e, B:274:0x059d, B:269:0x0572, B:270:0x057d, B:272:0x0583, B:273:0x058f, B:529:0x0ad6, B:537:0x0b24, B:530:0x0ada, B:532:0x0ade, B:533:0x0af9, B:534:0x0afd, B:536:0x0b01), top: B:613:0x0027 }] */
    /* JADX WARNING: Removed duplicated region for block: B:327:0x06a8 A[Catch: 1tG -> 0x0c1d, all -> 0x0c44, TryCatch #20 {all -> 0x0c49, blocks: (B:11:0x0027, B:541:0x0b30, B:576:0x0c15, B:16:0x0031, B:19:0x0042, B:20:0x0046, B:23:0x005a, B:24:0x0084, B:31:0x008d, B:32:0x009f, B:34:0x00a5, B:36:0x00b1, B:38:0x00b5, B:40:0x00b9, B:41:0x00bb, B:43:0x00c2, B:45:0x00c6, B:46:0x00c8, B:48:0x00cd, B:50:0x00d1, B:52:0x00d5, B:53:0x00d7, B:55:0x00dc, B:57:0x00e0, B:59:0x00e4, B:61:0x00e8, B:62:0x00ea, B:64:0x00ef, B:66:0x00f3, B:68:0x0102, B:69:0x0104, B:72:0x0110, B:73:0x0113, B:74:0x0116, B:75:0x0118, B:76:0x011c, B:77:0x011f, B:78:0x0132, B:79:0x0133, B:80:0x0136, B:81:0x0138, B:83:0x013c, B:84:0x013e, B:85:0x0155, B:86:0x015c, B:87:0x015d, B:88:0x0164, B:89:0x0165, B:90:0x016c, B:91:0x016d, B:92:0x0174, B:93:0x0175, B:94:0x017c, B:99:0x0183, B:101:0x0189, B:103:0x018d, B:104:0x018f, B:107:0x0195, B:108:0x01a1, B:110:0x01a4, B:112:0x01ab, B:114:0x01af, B:115:0x01b1, B:117:0x01b6, B:118:0x01c4, B:120:0x01d2, B:121:0x01d7, B:122:0x01dc, B:124:0x01ed, B:126:0x01f7, B:128:0x01fb, B:129:0x0210, B:131:0x0216, B:132:0x0222, B:134:0x0228, B:135:0x022a, B:137:0x0242, B:138:0x0247, B:139:0x026e, B:141:0x0274, B:142:0x027a, B:144:0x02c5, B:145:0x02cd, B:147:0x02d7, B:152:0x02e5, B:154:0x02ee, B:158:0x02f5, B:160:0x02f9, B:163:0x0303, B:165:0x0308, B:167:0x030c, B:168:0x030f, B:173:0x0329, B:175:0x032f, B:176:0x033a, B:177:0x033b, B:179:0x0341, B:180:0x034c, B:181:0x034d, B:182:0x0351, B:184:0x037a, B:186:0x0382, B:188:0x038f, B:190:0x0396, B:191:0x039c, B:192:0x039d, B:193:0x03a2, B:194:0x03a3, B:196:0x03a7, B:199:0x03b7, B:201:0x03c2, B:203:0x03c9, B:204:0x03cf, B:205:0x03d0, B:206:0x03d5, B:207:0x03d6, B:208:0x03ec, B:209:0x03f3, B:211:0x03f6, B:212:0x0401, B:213:0x0402, B:214:0x0415, B:215:0x0416, B:216:0x0423, B:217:0x0424, B:218:0x0435, B:220:0x043e, B:221:0x0447, B:222:0x0448, B:223:0x044f, B:225:0x0451, B:227:0x045d, B:228:0x0464, B:230:0x046e, B:231:0x047f, B:232:0x04af, B:234:0x04b5, B:236:0x04c1, B:237:0x04c7, B:239:0x04dd, B:242:0x04e5, B:243:0x04ee, B:245:0x04f4, B:246:0x04f6, B:248:0x050a, B:250:0x0519, B:251:0x051e, B:252:0x0525, B:253:0x0526, B:255:0x052e, B:258:0x0536, B:259:0x053f, B:261:0x0545, B:262:0x0547, B:264:0x055b, B:265:0x0560, B:267:0x0566, B:275:0x05a0, B:276:0x05a3, B:277:0x05ac, B:279:0x05b2, B:280:0x05b8, B:282:0x05c2, B:284:0x05cd, B:285:0x05d6, B:286:0x05da, B:288:0x05e0, B:289:0x05e6, B:291:0x05f1, B:292:0x05fa, B:293:0x0605, B:295:0x060b, B:297:0x0617, B:298:0x061d, B:299:0x0621, B:301:0x0627, B:302:0x064d, B:304:0x0653, B:306:0x065c, B:308:0x0666, B:312:0x067e, B:317:0x0689, B:324:0x069b, B:326:0x06a3, B:327:0x06a8, B:329:0x06ae, B:330:0x06b8, B:332:0x06cb, B:333:0x06d7, B:334:0x06db, B:336:0x06e1, B:521:0x0a8c, B:522:0x0a93, B:523:0x0a97, B:525:0x0a9d, B:528:0x0aaa, B:538:0x0b27, B:545:0x0b38, B:546:0x0b44, B:548:0x0b46, B:549:0x0b52, B:550:0x0b53, B:551:0x0b5f, B:552:0x0b60, B:553:0x0b6c, B:555:0x0b6e, B:556:0x0b79, B:558:0x0b7b, B:559:0x0b87, B:560:0x0b88, B:561:0x0b94, B:563:0x0b96, B:564:0x0ba2, B:574:0x0bfe, B:575:0x0c09, B:581:0x0c1e, B:582:0x0c36, B:583:0x0c37, B:584:0x0c43, B:268:0x056e, B:274:0x059d, B:269:0x0572, B:270:0x057d, B:272:0x0583, B:273:0x058f, B:529:0x0ad6, B:537:0x0b24, B:530:0x0ada, B:532:0x0ade, B:533:0x0af9, B:534:0x0afd, B:536:0x0b01), top: B:613:0x0027 }] */
    /* JADX WARNING: Removed duplicated region for block: B:683:0x06ae A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:687:0x064d A[SYNTHETIC] */
    public final boolean A07(X.AnonymousClass1K4 r48, X.C41221tA r49, java.lang.String r50, java.util.List r51, long r52) {
        /*
        // Method dump skipped, instructions count: 3244
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C41191t7.A07(X.1K4, X.1tA, java.lang.String, java.util.List, long):boolean");
    }
}
