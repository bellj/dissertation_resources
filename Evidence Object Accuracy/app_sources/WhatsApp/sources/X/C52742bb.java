package X;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.TextView;
import com.whatsapp.R;
import java.util.List;
import java.util.Locale;

/* renamed from: X.2bb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52742bb extends ArrayAdapter {
    public Context A00;
    public AnonymousClass018 A01;
    public List A02;
    public final boolean A03;

    @Override // android.widget.Adapter, android.widget.BaseAdapter
    public int getViewTypeCount() {
        return 1;
    }

    public C52742bb(Context context, AnonymousClass018 r3, List list, boolean z) {
        super(context, R.layout.language_selector_item);
        this.A00 = context;
        this.A01 = r3;
        this.A02 = list;
        this.A03 = z;
    }

    @Override // android.widget.ArrayAdapter, android.widget.Adapter
    public int getCount() {
        return this.A02.size();
    }

    @Override // android.widget.ArrayAdapter, android.widget.Adapter
    public /* bridge */ /* synthetic */ Object getItem(int i) {
        return this.A02.get(i);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // android.widget.ArrayAdapter, android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        Locale locale;
        int i2;
        String string;
        Context context = this.A00;
        View A0O = C12980iv.A0O(LayoutInflater.from(context), R.layout.language_selector_item);
        A0O.setId(AnonymousClass028.A02());
        CompoundButton compoundButton = (CompoundButton) AnonymousClass028.A0D(A0O, R.id.language_checkbox);
        TextView A0I = C12960it.A0I(A0O, R.id.language_name);
        List list = this.A02;
        A0I.setText(((C90284Nh) list.get(i)).A00);
        TextView A0I2 = C12960it.A0I(A0O, R.id.language_name_translated);
        if (i == (!this.A03)) {
            C12970iu.A19(context, A0I2, R.string.language_selector_phone_language);
        } else {
            String str = ((C90284Nh) list.get(i)).A01;
            String displayLanguage = AbstractC27291Gt.A09(str).getDisplayLanguage(Locale.getDefault());
            if (displayLanguage.length() > str.length() || !str.startsWith(displayLanguage)) {
                locale = Locale.getDefault();
            } else {
                locale = Resources.getSystem().getConfiguration().locale;
            }
            Locale A09 = AbstractC27291Gt.A09(str);
            String language = A09.getLanguage();
            switch (language.hashCode()) {
                case 3116:
                    if (language.equals("am") && locale.getLanguage().equals("om")) {
                        i2 = R.string.amharic_translated_in_oromo;
                        string = context.getString(i2);
                        break;
                    }
                    string = AbstractC27291Gt.A09(str).getDisplayLanguage(locale);
                    break;
                case 3588:
                    if (language.equals("pt")) {
                        boolean contains = AnonymousClass1Th.A00.contains(A09.getCountry());
                        i2 = R.string.language_name_portuguese_brazil;
                        if (contains) {
                            i2 = R.string.language_name_portuguese_portugal;
                        }
                        string = context.getString(i2);
                        break;
                    }
                    string = AbstractC27291Gt.A09(str).getDisplayLanguage(locale);
                    break;
                case 3886:
                    if (language.equals("zh")) {
                        if ("HK".equals(A09.getCountry())) {
                            i2 = R.string.language_name_traditional_chinese_hongkong;
                        } else {
                            boolean equals = "Hans".equals(AbstractC27291Gt.A02(A09));
                            i2 = R.string.language_name_traditional_chinese_taiwan;
                            if (equals) {
                                i2 = R.string.language_name_simplified_chinese;
                            }
                        }
                        string = context.getString(i2);
                        break;
                    }
                    string = AbstractC27291Gt.A09(str).getDisplayLanguage(locale);
                    break;
                default:
                    string = AbstractC27291Gt.A09(str).getDisplayLanguage(locale);
                    break;
            }
            String A00 = AnonymousClass3J4.A00(string);
            A0I2.setText(A00);
            A0I.setContentDescription(A00);
        }
        if (i == 0) {
            compoundButton.toggle();
        }
        AnonymousClass028.A0a(A0I2, 2);
        return A0O;
    }
}
