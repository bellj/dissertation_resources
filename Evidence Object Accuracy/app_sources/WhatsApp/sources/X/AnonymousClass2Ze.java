package X;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.view.View;

/* renamed from: X.2Ze  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Ze extends GradientDrawable {
    public int A00;
    public final Paint A01;
    public final RectF A02 = C12980iv.A0K();

    public AnonymousClass2Ze() {
        Paint A0A = C12960it.A0A();
        this.A01 = A0A;
        A0A.setStyle(Paint.Style.FILL_AND_STROKE);
        A0A.setColor(-1);
        C12990iw.A14(A0A, PorterDuff.Mode.DST_OUT);
    }

    public void A00(float f, float f2, float f3, float f4) {
        RectF rectF = this.A02;
        if (f != rectF.left || f2 != rectF.top || f3 != rectF.right || f4 != rectF.bottom) {
            rectF.set(f, f2, f3, f4);
            invalidateSelf();
        }
    }

    @Override // android.graphics.drawable.GradientDrawable, android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        int saveLayer;
        Drawable.Callback callback = getCallback();
        if (callback instanceof View) {
            ((View) callback).setLayerType(2, null);
        } else {
            int i = Build.VERSION.SDK_INT;
            float width = (float) canvas.getWidth();
            float height = (float) canvas.getHeight();
            if (i >= 21) {
                saveLayer = canvas.saveLayer(0.0f, 0.0f, width, height, null);
            } else {
                saveLayer = canvas.saveLayer(0.0f, 0.0f, width, height, null, 31);
            }
            this.A00 = saveLayer;
        }
        super.draw(canvas);
        canvas.drawRect(this.A02, this.A01);
        if (!(getCallback() instanceof View)) {
            canvas.restoreToCount(this.A00);
        }
    }
}
