package X;

import com.whatsapp.util.Log;

/* renamed from: X.1JC  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1JC {
    public final AnonymousClass016 A00 = new AnonymousClass016();
    public final C32701cb A01;
    public final AnonymousClass1JD A02;
    public final /* synthetic */ C14310lE A03;

    public AnonymousClass1JC(C14310lE r2, C32701cb r3, AnonymousClass1JD r4) {
        this.A03 = r2;
        this.A02 = r4;
        this.A01 = r3;
    }

    public void A00(int i) {
        StringBuilder sb = new StringBuilder("WebPagePreviewViewModel/CTWAListener/errorCode/");
        sb.append(i);
        Log.e(sb.toString());
        this.A00.A0A(new AnonymousClass1JE(null, this.A02, false));
    }
}
