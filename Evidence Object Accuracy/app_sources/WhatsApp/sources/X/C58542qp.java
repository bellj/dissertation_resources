package X;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaFrameLayout;
import com.whatsapp.WaImageView;

/* renamed from: X.2qp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58542qp extends WaFrameLayout {
    public final TextEmojiLabel A00 = C12970iu.A0T(this, R.id.items_count);
    public final TextEmojiLabel A01 = C12970iu.A0T(this, R.id.header_title);
    public final WaImageView A02 = C12980iv.A0X(this, R.id.thumbnail);

    public C58542qp(Context context) {
        super(context, null);
        LayoutInflater.from(context).inflate(R.layout.conversation_row_header_product, (ViewGroup) this, true);
    }
}
