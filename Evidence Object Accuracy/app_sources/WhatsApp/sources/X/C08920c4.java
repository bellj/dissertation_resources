package X;

import android.content.ComponentName;

/* renamed from: X.0c4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08920c4 implements Comparable {
    public final ComponentName A00;
    public final C007603x A01;

    public C08920c4(ComponentName componentName, C007603x r2) {
        this.A01 = r2;
        this.A00 = componentName;
    }

    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(Object obj) {
        return this.A01.A02 - ((C08920c4) obj).A01.A02;
    }
}
