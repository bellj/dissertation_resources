package X;

/* renamed from: X.548  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass548 implements AbstractC116535Vv {
    public final /* synthetic */ C91064Qh A00;
    public final /* synthetic */ AnonymousClass5WN A01;
    public final /* synthetic */ AbstractC16850pr A02;

    public AnonymousClass548(C91064Qh r1, AnonymousClass5WN r2, AbstractC16850pr r3) {
        this.A02 = r3;
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC116535Vv
    public void AOU(AnonymousClass3JI r4) {
        AnonymousClass5B5 r0 = new AnonymousClass5B5(r4);
        AnonymousClass5WN r2 = this.A01;
        C91064Qh r1 = new C91064Qh();
        r1.A01 = r0.A00;
        r1.A00 = 5;
        r2.AVH(r1);
    }

    @Override // X.AbstractC116535Vv
    public void APq(String str) {
        this.A02.A01(this.A00, this.A01, str);
    }
}
