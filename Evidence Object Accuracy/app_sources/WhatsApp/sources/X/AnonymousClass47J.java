package X;

/* renamed from: X.47J  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass47J extends C469928m {
    public final /* synthetic */ AnonymousClass1J7 A00;

    public AnonymousClass47J(AnonymousClass1J7 r1) {
        this.A00 = r1;
    }

    @Override // X.C469928m, android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        C16700pc.A0E(charSequence, 0);
        this.A00.AJ4(charSequence);
    }
}
