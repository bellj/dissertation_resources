package X;

import android.content.Context;

/* renamed from: X.5va  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128115va {
    public final Context A00;
    public final AbstractC15710nm A01;
    public final C14900mE A02;
    public final C17220qS A03;
    public final C18650sn A04;
    public final C125785ro A05;
    public final C18590sh A06;
    public final String A07;

    public C128115va(Context context, AbstractC15710nm r2, C14900mE r3, C17220qS r4, C18650sn r5, C125785ro r6, C18590sh r7, String str) {
        this.A00 = context;
        this.A02 = r3;
        this.A01 = r2;
        this.A03 = r4;
        this.A06 = r7;
        this.A04 = r5;
        this.A07 = str;
        this.A05 = r6;
    }
}
