package X;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* renamed from: X.51S  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass51S implements AnonymousClass5XH {
    public transient Map asMap;
    public transient Set keySet;
    public transient Collection values;

    public abstract Map createAsMap();

    public abstract Set createKeySet();

    public abstract Collection createValues();

    @Override // X.AnonymousClass5XH
    public abstract boolean put(Object obj, Object obj2);

    public abstract Iterator valueIterator();

    @Override // X.AnonymousClass5XH
    public Map asMap() {
        Map map = this.asMap;
        if (map != null) {
            return map;
        }
        Map createAsMap = createAsMap();
        this.asMap = createAsMap;
        return createAsMap;
    }

    public boolean containsValue(Object obj) {
        Iterator A0o = C12960it.A0o(asMap());
        while (A0o.hasNext()) {
            if (((Collection) A0o.next()).contains(obj)) {
                return true;
            }
        }
        return false;
    }

    public boolean equals(Object obj) {
        return AnonymousClass4Yf.equalsImpl(this, obj);
    }

    public int hashCode() {
        return asMap().hashCode();
    }

    @Override // X.AnonymousClass5XH
    public boolean isEmpty() {
        return C12960it.A1T(size());
    }

    public Set keySet() {
        Set set = this.keySet;
        if (set != null) {
            return set;
        }
        Set createKeySet = createKeySet();
        this.keySet = createKeySet;
        return createKeySet;
    }

    public String toString() {
        return asMap().toString();
    }

    @Override // X.AnonymousClass5XH
    public Collection values() {
        Collection collection = this.values;
        if (collection != null) {
            return collection;
        }
        Collection createValues = createValues();
        this.values = createValues;
        return createValues;
    }
}
