package X;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.GridLayoutManager;
import com.whatsapp.R;
import com.whatsapp.mediacomposer.doodle.shapepicker.ShapePickerRecyclerView;
import java.util.List;
import java.util.Map;

/* renamed from: X.2gk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54452gk extends AnonymousClass02M {
    public int A00 = -1;
    public int A01;
    public List A02 = C12960it.A0l();
    public final /* synthetic */ AbstractC64423Fm A03;

    public C54452gk(AbstractC64423Fm r2) {
        this.A03 = r2;
    }

    @Override // X.AnonymousClass02M
    public long A00(int i) {
        if (!super.A00) {
            return -1;
        }
        AbstractC64423Fm r6 = this.A03;
        if (!(r6 instanceof AnonymousClass33Q)) {
            throw C12980iv.A0u("You must override getStableId");
        }
        AnonymousClass33Q r62 = (AnonymousClass33Q) r6;
        boolean z = r62.A01;
        if (z && i == 0) {
            return -1;
        }
        List list = r62.A03;
        if (z) {
            i--;
        }
        String str = ((AnonymousClass1KZ) list.get(i)).A0D;
        Map map = r62.A04;
        Number number = (Number) map.get(str);
        if (number == null) {
            long j = r62.A00;
            r62.A00 = 1 + j;
            number = Long.valueOf(j);
            map.put(str, number);
        }
        return number.longValue();
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A02.size();
    }

    public final void A0E() {
        int i;
        ShapePickerRecyclerView shapePickerRecyclerView = this.A03.A08;
        GridLayoutManager gridLayoutManager = shapePickerRecyclerView.A04;
        if (gridLayoutManager != null) {
            int A19 = gridLayoutManager.A19();
            GridLayoutManager gridLayoutManager2 = shapePickerRecyclerView.A04;
            if (gridLayoutManager2 != null) {
                int A1B = gridLayoutManager2.A1B();
                if (A19 == 0) {
                    i = 0;
                } else if (A1B == shapePickerRecyclerView.getAdapterItemCount() - 1) {
                    i = C12980iv.A0C(this.A02);
                } else {
                    i = -1;
                    int i2 = Integer.MAX_VALUE;
                    for (int i3 = 0; i3 < this.A02.size(); i3++) {
                        int min = Math.min(C12980iv.A05(C12960it.A05(this.A02.get(i3)), A19), C12980iv.A05(i3 < C12980iv.A0C(this.A02) ? C12960it.A05(this.A02.get(i3 + 1)) - 1 : Integer.MAX_VALUE, (A19 + A1B) >> 1));
                        if (min < i2) {
                            i = i3;
                            i2 = min;
                        }
                    }
                }
                A0G(i);
                return;
            }
            throw C12960it.A0U("Must set adapter first");
        }
        throw C12960it.A0U("Must set adapter first");
    }

    public final void A0F(int i) {
        AbstractC64423Fm r1 = this.A03;
        r1.A02 = false;
        A0G(i);
        ShapePickerRecyclerView shapePickerRecyclerView = r1.A08;
        int A05 = C12960it.A05(this.A02.get(i));
        GridLayoutManager gridLayoutManager = shapePickerRecyclerView.A04;
        if (gridLayoutManager != null) {
            AbstractC05520Pw r0 = shapePickerRecyclerView.A05;
            r0.A00 = A05;
            gridLayoutManager.A0R(r0);
            return;
        }
        throw C12960it.A0U("Must set adapter first");
    }

    public final void A0G(int i) {
        int i2 = this.A00;
        this.A00 = i;
        A03(i2);
        A03(this.A00);
        int max = Math.max(this.A00 - (this.A01 >> 1), 0);
        AbstractC64423Fm r2 = this.A03;
        AbstractC05520Pw r1 = r2.A05;
        if (max != r1.A00) {
            r1.A00 = max;
            r2.A03.A0R(r1);
        }
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r9, int i) {
        View view;
        String A0X;
        C75453js r92 = (C75453js) r9;
        int i2 = 0;
        boolean A1V = C12960it.A1V(this.A00, i);
        AbstractC64423Fm r6 = this.A03;
        if (!(r6 instanceof AnonymousClass33Q)) {
            ImageView imageView = r92.A01;
            imageView.setImageResource(AnonymousClass33R.A01[i]);
            float f = 0.55f;
            if (A1V) {
                f = 1.0f;
            }
            imageView.setAlpha(f);
            View view2 = r92.A0H;
            C12960it.A0r(view2.getContext(), view2, AnonymousClass33R.A02[i]);
        } else {
            AnonymousClass33Q r62 = (AnonymousClass33Q) r6;
            boolean z = r62.A01;
            if (!z || i != 0) {
                List list = r62.A03;
                if (z) {
                    i--;
                }
                AnonymousClass1KZ r4 = (AnonymousClass1KZ) list.get(i);
                String str = r4.A0D;
                ImageView imageView2 = r92.A01;
                if (!str.equals(imageView2.getTag())) {
                    r62.A02.A0I(r4, new C69903aQ(imageView2, str));
                    view = r92.A0H;
                    A0X = C12960it.A0X(view.getContext(), r4.A0F, C12970iu.A1b(), 0, R.string.shape_picker_sticker_pack_subcategory_content_description);
                }
            } else {
                ImageView imageView3 = r92.A01;
                imageView3.setTag(null);
                imageView3.setImageResource(R.drawable.ic_stickers_recents);
                view = r92.A0H;
                A0X = view.getContext().getString(R.string.shape_picker_recents_subcategory_content_description);
            }
            view.setContentDescription(A0X);
        }
        View view3 = r92.A00;
        if (!A1V) {
            i2 = 8;
        }
        view3.setVisibility(i2);
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        AbstractC64423Fm r3 = this.A03;
        C75453js r2 = new C75453js(C12960it.A0F(C12960it.A0E(r3.A06), viewGroup, R.layout.shape_picker_subcategory_item));
        r3.A02(r2, r3.A00);
        C12960it.A14(r2.A0H, this, r2, 18);
        return r2;
    }
}
