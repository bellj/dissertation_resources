package X;

/* renamed from: X.4wp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107124wp implements AnonymousClass5X7 {
    public int A00;
    public int A01;
    public int A02 = 0;
    public int A03;
    public long A04;
    public long A05;
    public C100614mC A06;
    public AnonymousClass5X6 A07;
    public String A08;
    public final C95304dT A09 = new C95304dT(new byte[18]);
    public final String A0A;

    @Override // X.AnonymousClass5X7
    public void AYo() {
    }

    public C107124wp(String str) {
        this.A0A = str;
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x00fc  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x013b  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0235  */
    @Override // X.AnonymousClass5X7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A7a(X.C95304dT r23) {
        /*
        // Method dump skipped, instructions count: 637
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C107124wp.A7a(X.4dT):void");
    }

    @Override // X.AnonymousClass5X7
    public void A8b(AbstractC14070ko r2, C92824Xo r3) {
        r3.A03();
        this.A08 = r3.A02();
        this.A07 = C92824Xo.A00(r2, r3);
    }

    @Override // X.AnonymousClass5X7
    public void AYp(long j, int i) {
        this.A05 = j;
    }

    @Override // X.AnonymousClass5X7
    public void AbP() {
        this.A02 = 0;
        this.A00 = 0;
        this.A03 = 0;
    }
}
