package X;

/* renamed from: X.3eY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public interface AbstractC72413eY {
    public static final AnonymousClass1TK A00;
    public static final AnonymousClass1TK A01;
    public static final AnonymousClass1TK A02;
    public static final AnonymousClass1TK A03;
    public static final AnonymousClass1TK A04;
    public static final AnonymousClass1TK A05;
    public static final AnonymousClass1TK A06;
    public static final AnonymousClass1TK A07;
    public static final AnonymousClass1TK A08;
    public static final AnonymousClass1TK A09;
    public static final AnonymousClass1TK A0A = new AnonymousClass1TK("0.2.262.1.10.12.0");
    public static final AnonymousClass1TK A0B;
    public static final AnonymousClass1TK A0C;
    public static final AnonymousClass1TK A0D;
    public static final AnonymousClass1TK A0E;
    public static final AnonymousClass1TK A0F;
    public static final AnonymousClass1TK A0G;
    public static final AnonymousClass1TK A0H;
    public static final AnonymousClass1TK A0I;
    public static final AnonymousClass1TK A0J;

    static {
        AnonymousClass1TK r1 = new AnonymousClass1TK("1.3.36.8");
        A00 = r1;
        AnonymousClass1TK A022 = AnonymousClass1TL.A02("1", r1);
        A0I = A022;
        A0J = AnonymousClass1TL.A02("1", A022);
        AnonymousClass1TK A023 = AnonymousClass1TL.A02("3", r1);
        A01 = A023;
        A07 = AnonymousClass1TL.A02("1", A023);
        A0E = AnonymousClass1TL.A02("2", A023);
        A04 = AnonymousClass1TL.A02("3", A023);
        A0B = AnonymousClass1TL.A02("4", A023);
        A08 = AnonymousClass1TL.A02("5", A023);
        A09 = AnonymousClass1TL.A02("6", A023);
        A02 = AnonymousClass1TL.A02("7", A023);
        A0G = AnonymousClass1TL.A02("8", A023);
        A0H = AnonymousClass1TL.A02("9", A023);
        A0F = AnonymousClass1TL.A02("10", A023);
        A0D = AnonymousClass1TL.A02("11", A023);
        A06 = AnonymousClass1TL.A02("12", A023);
        A05 = AnonymousClass1TL.A02("13", A023);
        A0C = AnonymousClass1TL.A02("14", A023);
        A03 = AnonymousClass1TL.A02("15", A023);
    }
}
