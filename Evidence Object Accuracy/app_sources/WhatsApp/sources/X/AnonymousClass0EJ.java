package X;

import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.0EJ  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0EJ extends AbstractC06180Sm {
    public AnonymousClass0EJ(ViewGroup viewGroup) {
        super(viewGroup);
    }

    public static void A00(AnonymousClass00N r1, Collection collection) {
        Iterator it = r1.entrySet().iterator();
        while (it.hasNext()) {
            if (!collection.contains(AnonymousClass028.A0J((View) ((Map.Entry) it.next()).getValue()))) {
                it.remove();
            }
        }
    }

    public void A07(View view, ArrayList arrayList) {
        Boolean bool;
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            if (Build.VERSION.SDK_INT < 21 ? ((bool = (Boolean) viewGroup.getTag(R.id.tag_transition_group)) == null || !bool.booleanValue()) && viewGroup.getBackground() == null && AnonymousClass028.A0J(viewGroup) == null : !C04210Ku.A00(viewGroup)) {
                int childCount = viewGroup.getChildCount();
                for (int i = 0; i < childCount; i++) {
                    View childAt = viewGroup.getChildAt(i);
                    if (childAt.getVisibility() == 0) {
                        A07(childAt, arrayList);
                    }
                }
            } else if (!arrayList.contains(view)) {
                arrayList.add(viewGroup);
            }
        } else if (!arrayList.contains(view)) {
            arrayList.add(view);
        }
    }

    public void A08(View view, Map map) {
        String A0J = AnonymousClass028.A0J(view);
        if (A0J != null) {
            map.put(A0J, view);
        }
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int childCount = viewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = viewGroup.getChildAt(i);
                if (childAt.getVisibility() == 0) {
                    A08(childAt, map);
                }
            }
        }
    }
}
