package X;

import android.os.Bundle;

@Deprecated
/* renamed from: X.4ye  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C108194ye implements AbstractC116855Xe {
    public static final C108194ye A02 = new C108194ye(new C93334a0());
    public final String A00;
    public final boolean A01;

    public C108194ye(C93334a0 r2) {
        this.A01 = r2.A00.booleanValue();
        this.A00 = r2.A01;
    }

    public final Bundle A00() {
        Bundle A0D = C12970iu.A0D();
        A0D.putString("consumer_package", null);
        A0D.putBoolean("force_save_dialog", this.A01);
        A0D.putString("log_session_id", this.A00);
        return A0D;
    }

    public boolean equals(Object obj) {
        if (obj != this) {
            if (obj instanceof C108194ye) {
                C108194ye r5 = (C108194ye) obj;
                if (this.A01 != r5.A01 || !C13300jT.A00(this.A00, r5.A00)) {
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        Object[] objArr = new Object[3];
        objArr[0] = null;
        objArr[1] = Boolean.valueOf(this.A01);
        return C12980iv.A0B(this.A00, objArr, 2);
    }
}
