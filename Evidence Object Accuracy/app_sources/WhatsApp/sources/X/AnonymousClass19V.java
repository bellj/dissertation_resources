package X;

import android.util.Base64;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

/* renamed from: X.19V  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass19V implements AnonymousClass19W {
    public String A00 = "";
    public final long A01;
    public final C18790t3 A02;
    public final C14820m6 A03;
    public final C14850m9 A04;
    public final AnonymousClass18L A05;
    public final AnonymousClass01H A06;
    public final String A07;
    public final Map A08;
    public final AnonymousClass01N A09;
    public final AnonymousClass01N A0A;

    public String A00() {
        return null;
    }

    public AnonymousClass19V(C18790t3 r2, C14820m6 r3, C14850m9 r4, AnonymousClass18L r5, AnonymousClass01H r6, String str, Map map, AnonymousClass01N r9, AnonymousClass01N r10, long j) {
        this.A04 = r4;
        this.A02 = r2;
        this.A03 = r3;
        this.A06 = r6;
        this.A09 = r9;
        this.A0A = r10;
        this.A01 = j;
        this.A07 = str;
        this.A08 = map;
        this.A05 = r5;
    }

    public String A01() {
        if (!(this instanceof AnonymousClass32J)) {
            return null;
        }
        return "";
    }

    public void A02(JSONObject jSONObject) {
        int indexOf;
        int indexOf2;
        if (this instanceof AnonymousClass32J) {
            AnonymousClass32J r6 = (AnonymousClass32J) this;
            JSONObject jSONObject2 = new JSONObject();
            String str = r6.A00;
            AnonymousClass1W0 r4 = null;
            if (str.startsWith("AesKey=") && (indexOf = str.indexOf(";IV=")) > 0 && (indexOf2 = str.indexOf(";Data=")) > 0) {
                String substring = str.substring(7, indexOf);
                String substring2 = str.substring(indexOf + 4, indexOf2);
                String substring3 = str.substring(indexOf2 + 6);
                byte[] decode = Base64.decode(substring, 2);
                byte[] decode2 = Base64.decode(substring2, 2);
                byte[] decode3 = Base64.decode(substring3, 2);
                if (!(decode == null || decode2 == null || decode3 == null)) {
                    r4 = new AnonymousClass1W0(decode, decode3, decode2);
                }
            }
            jSONObject2.put("encrypted_flow_data", Base64.encodeToString(r4.A00, 2));
            jSONObject2.put("encrypted_aes_key", Base64.encodeToString(r4.A01, 2));
            jSONObject2.put("initial_vector", Base64.encodeToString(r4.A02, 2));
            jSONObject2.put("flow_data_endpoint", r6.A01);
            jSONObject.put("variables", jSONObject2.toString());
        } else if (this instanceof AnonymousClass32I) {
            AnonymousClass32I r42 = (AnonymousClass32I) this;
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("app_id", "dev.app.id");
            jSONObject3.put("request_token", r42.A02);
            JSONObject jSONObject4 = new JSONObject();
            jSONObject4.put("description", r42.A01);
            jSONObject4.put("debug_info", r42.A00);
            jSONObject3.put("user_request", jSONObject4);
            jSONObject.put("variables", jSONObject3.toString());
        } else if (this instanceof AnonymousClass32H) {
            JSONObject jSONObject5 = new JSONObject();
            jSONObject5.put("app_id", "dev.app.id");
            jSONObject5.put("request_token", ((AnonymousClass32H) this).A00);
            jSONObject.put("variables", jSONObject5.toString());
        } else if (this instanceof AnonymousClass19U) {
            AnonymousClass19U r3 = (AnonymousClass19U) this;
            JSONObject jSONObject6 = new JSONObject();
            String str2 = r3.A01;
            if (str2 != null) {
                jSONObject6.put("fbid", str2);
                Boolean bool = true;
                jSONObject6.put("stitch_images", bool.toString());
                String str3 = r3.A00;
                if (str3 != null) {
                    jSONObject6.put("ent_type", str3);
                }
                jSONObject.put("variables", jSONObject6);
                return;
            }
            throw new IllegalStateException("GraphqlRequest: fbId is required");
        } else if (this instanceof AbstractC44381yp) {
            AbstractC44381yp r32 = (AbstractC44381yp) this;
            String str4 = r32.A01;
            if (str4 == null || str4.length() == 0) {
                throw new IllegalArgumentException("GetBanReportRequest: auth_token cannot be null. ");
            }
            JSONObject jSONObject7 = new JSONObject();
            jSONObject7.put("auth_token", Base64.encodeToString(str4.getBytes(), 2));
            jSONObject7.put("app_id", "com.whatsapp.w4b");
            jSONObject7.put("user_agent", r32.A00.A00());
            jSONObject7.put("version", "1");
            jSONObject.put("variables", jSONObject7);
        } else if ((this instanceof AnonymousClass32E) || (this instanceof AnonymousClass32D) || (this instanceof AnonymousClass32C)) {
            JSONObject jSONObject8 = new JSONObject();
            jSONObject8.put("version", 1);
            jSONObject8.put("sticker_pack", "v1");
            JSONObject jSONObject9 = new JSONObject();
            jSONObject9.put("params", jSONObject8);
            jSONObject.put("variables", jSONObject9);
        } else if (!(this instanceof AnonymousClass32B)) {
            if (this instanceof AnonymousClass32G) {
                JSONObject jSONObject10 = new JSONObject();
                jSONObject10.put("client_mutation_id", 7417448104995275L);
                jSONObject10.put("user_id", ((AnonymousClass32G) this).A00);
                jSONObject.put("variables", jSONObject10);
            } else if (this instanceof C59202uA) {
                C59202uA r43 = (C59202uA) this;
                JSONObject jSONObject11 = new JSONObject();
                C91864Tn r5 = r43.A00;
                UserJid userJid = r5.A03;
                jSONObject11.put("biz_jid", userJid.getRawString());
                jSONObject11.put("id", r5.A05);
                jSONObject11.put("limit", r5.A01);
                jSONObject11.put("width", r5.A02);
                jSONObject11.put("height", r5.A00);
                jSONObject11.put("is_category", r5.A07);
                String str5 = r5.A06;
                if (str5 != null) {
                    jSONObject11.put("catalog_session_id", str5);
                }
                String str6 = r5.A04;
                if (str6 != null) {
                    jSONObject11.put("after", str6);
                }
                r43.A03(userJid, "collection", jSONObject, jSONObject11);
            } else if (this instanceof C59222uC) {
                C59222uC r52 = (C59222uC) this;
                JSONObject jSONObject12 = new JSONObject();
                AnonymousClass3EQ r8 = r52.A00;
                UserJid userJid2 = r8.A00;
                jSONObject12.put("jid", userJid2.getRawString());
                JSONArray jSONArray = new JSONArray();
                for (Object obj : r8.A03) {
                    JSONObject jSONObject13 = new JSONObject();
                    jSONObject13.put("id", obj);
                    jSONArray.put(jSONObject13);
                }
                jSONObject12.put("products", jSONArray);
                jSONObject12.put("width", r8.A02);
                jSONObject12.put("height", r8.A01);
                Object obj2 = r52.A01;
                if (obj2 != null) {
                    jSONObject12.put("catalog_session_id", obj2);
                }
                r52.A03(userJid2, "product_list", jSONObject, jSONObject12);
            } else if (this instanceof C59192u9) {
                C59192u9 r44 = (C59192u9) this;
                JSONObject jSONObject14 = new JSONObject();
                AnonymousClass4TA r53 = r44.A00;
                UserJid userJid3 = r53.A00;
                jSONObject14.put("jid", userJid3.getRawString());
                jSONObject14.put("product_id", r53.A03);
                jSONObject14.put("width", String.valueOf(r53.A02));
                jSONObject14.put("height", String.valueOf(r53.A01));
                jSONObject14.put("catalog_session_id", r53.A04);
                if (r53.A05) {
                    jSONObject14.put("fetch_compliance_info", "true");
                }
                r44.A03(userJid3, "product", jSONObject, jSONObject14);
            } else if (this instanceof C59212uB) {
                C59212uB r62 = (C59212uB) this;
                JSONObject jSONObject15 = new JSONObject();
                AnonymousClass28H r2 = r62.A01;
                UserJid userJid4 = r2.A05;
                jSONObject15.put("jid", userJid4.getRawString());
                jSONObject15.put("limit", String.valueOf(r2.A02));
                jSONObject15.put("width", String.valueOf(r2.A04));
                jSONObject15.put("height", String.valueOf(r2.A03));
                String str7 = r2.A06;
                if (str7 != null) {
                    jSONObject15.put("after", str7);
                }
                String str8 = r2.A07;
                if (str8 != null) {
                    jSONObject15.put("catalog_session_id", str8);
                }
                if (Boolean.TRUE.equals(r2.A01)) {
                    jSONObject15.put("allow_shop_source", "ALLOWSHOPSOURCE_TRUE");
                }
                AnonymousClass28J r33 = r2.A00;
                if (r33 != null) {
                    JSONObject jSONObject16 = new JSONObject();
                    jSONObject16.put("value", r33.A00);
                    jSONObject16.put("version", r33.A01);
                    jSONObject15.put("query", jSONObject16);
                }
                r62.A03(userJid4, "product_catalog", jSONObject, jSONObject15);
            } else if (!(this instanceof AnonymousClass2u8)) {
                C59182u7 r82 = (C59182u7) this;
                C92404Vt r9 = r82.A00;
                UserJid userJid5 = r9.A02;
                JSONObject jSONObject17 = new JSONObject();
                jSONObject17.put("width", r9.A01);
                jSONObject17.put("height", r9.A00);
                JSONObject jSONObject18 = new JSONObject();
                jSONObject18.put("biz_jid", userJid5.getRawString());
                jSONObject18.put("image_dimensions", jSONObject17);
                Set set = r9.A04;
                if (!set.isEmpty()) {
                    ArrayList arrayList = new ArrayList(set);
                    JSONArray jSONArray2 = new JSONArray();
                    for (int i = 0; i < arrayList.size(); i++) {
                        JSONObject jSONObject19 = new JSONObject();
                        jSONObject19.put("category_id", arrayList.get(i));
                        jSONArray2.put(jSONObject19);
                    }
                    jSONObject18.put("category_ids", jSONArray2);
                }
                jSONObject18.put("catalog_session_id", r9.A03);
                r82.A03(userJid5, "categories", jSONObject, jSONObject18);
            } else {
                AnonymousClass2u8 r45 = (AnonymousClass2u8) this;
                JSONObject jSONObject20 = new JSONObject();
                AnonymousClass4Tm r54 = r45.A00;
                UserJid userJid6 = r54.A05;
                jSONObject20.put("biz_jid", userJid6.getRawString());
                jSONObject20.put("collection_limit", r54.A00);
                jSONObject20.put("item_limit", r54.A02);
                jSONObject20.put("width", r54.A03);
                jSONObject20.put("height", r54.A01);
                String str9 = r54.A07;
                if (str9 != null) {
                    jSONObject20.put("catalog_session_id", str9);
                }
                String str10 = r54.A06;
                if (str10 != null) {
                    jSONObject20.put("after", str10);
                }
                r45.A03(userJid6, "collections", jSONObject, jSONObject20);
            }
        }
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:86:0x0024 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:58:0x01e2 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v1, types: [X.1yr] */
    /* JADX WARN: Type inference failed for: r2v2 */
    /* JADX WARN: Type inference failed for: r2v3, types: [X.1yr] */
    /* JADX WARN: Type inference failed for: r2v4, types: [android.content.SharedPreferences] */
    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    @Override // X.AnonymousClass19W
    public void AZO(X.AbstractC44401yr r21) {
        /*
        // Method dump skipped, instructions count: 607
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass19V.AZO(X.1yr):void");
    }
}
