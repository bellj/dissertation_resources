package X;

import android.view.Menu;
import com.whatsapp.wabloks.ui.WaBloksActivity;

/* renamed from: X.487  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass487 extends AbstractC119415dw {
    @Override // X.AbstractC119415dw
    public void A00(AbstractC115815Ta r1) {
    }

    @Override // X.AbstractC119415dw, X.AbstractC36791ka
    public void AOk(Menu menu) {
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass487(AnonymousClass018 r2, WaBloksActivity waBloksActivity) {
        super(r2, waBloksActivity);
        C16700pc.A0E(r2, 1);
    }
}
