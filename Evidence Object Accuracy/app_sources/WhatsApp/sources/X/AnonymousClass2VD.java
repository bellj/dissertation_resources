package X;

import android.net.Uri;
import android.os.Bundle;
import java.util.Arrays;

/* renamed from: X.2VD  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2VD {
    public final int A00;
    public final Uri A01;
    public final AnonymousClass1JV A02;
    public final String A03;

    public AnonymousClass2VD(Uri uri, AnonymousClass1JV r2, String str, int i) {
        this.A02 = r2;
        this.A01 = uri;
        this.A03 = str;
        this.A00 = i;
    }

    public static AnonymousClass2VD A00(Bundle bundle) {
        AnonymousClass1JV A03 = AnonymousClass1JV.A03(bundle.getString("key_raw_jid"));
        if (A03 != null) {
            String string = bundle.getString("key_group_name");
            if (!AnonymousClass1US.A0C(string)) {
                String string2 = bundle.getString("key_raw_photo_uri");
                Uri uri = null;
                if (string2 != null) {
                    uri = Uri.parse(string2);
                }
                return new AnonymousClass2VD(uri, A03, string, bundle.getInt("key_ephemeral_duration"));
            }
            throw new IllegalStateException("Group name missing");
        }
        throw new IllegalStateException("Pending group raw jid missing or invalid");
    }

    public Bundle A01() {
        String str;
        Bundle bundle = new Bundle();
        bundle.putString("key_raw_jid", this.A02.getRawString());
        Uri uri = this.A01;
        if (uri != null) {
            str = uri.toString();
        } else {
            str = null;
        }
        bundle.putString("key_raw_photo_uri", str);
        bundle.putString("key_group_name", this.A03);
        bundle.putInt("key_ephemeral_duration", this.A00);
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AnonymousClass2VD)) {
            return false;
        }
        AnonymousClass2VD r4 = (AnonymousClass2VD) obj;
        if (!C29941Vi.A00(this.A01, r4.A01) || !C29941Vi.A00(this.A03, r4.A03) || !C29941Vi.A00(Integer.valueOf(this.A00), Integer.valueOf(r4.A00))) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A03, this.A01, Integer.valueOf(this.A00)});
    }
}
