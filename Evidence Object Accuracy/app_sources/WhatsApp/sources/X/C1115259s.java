package X;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import com.whatsapp.R;

/* renamed from: X.59s  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1115259s implements AbstractC41521tf {
    public final /* synthetic */ C60502xy A00;

    @Override // X.AbstractC41521tf
    public int AGm() {
        return 96;
    }

    @Override // X.AbstractC41521tf
    public /* synthetic */ void AQV() {
    }

    public C1115259s(C60502xy r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC41521tf
    public void Adg(Bitmap bitmap, View view, AbstractC15340mz r5) {
        ImageView imageView = this.A00.A05;
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        } else {
            imageView.setImageResource(R.drawable.avatar_group);
        }
    }

    @Override // X.AbstractC41521tf
    public void Adu(View view) {
        this.A00.A05.setImageResource(R.drawable.avatar_group);
    }
}
