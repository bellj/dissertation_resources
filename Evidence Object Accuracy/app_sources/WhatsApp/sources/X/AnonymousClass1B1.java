package X;

import com.whatsapp.R;
import java.util.List;

/* renamed from: X.1B1  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1B1 {
    public int A00;
    public final C16590pI A01;
    public final List A02;

    public AnonymousClass1B1(C16590pI r3) {
        C16700pc.A0E(r3, 1);
        this.A01 = r3;
        String[] stringArray = r3.A00.getResources().getStringArray(R.array.directory_search_hints);
        C16700pc.A0B(stringArray);
        this.A02 = C10970fV.A02(stringArray);
    }
}
