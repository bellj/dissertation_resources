package X;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;
import com.facebook.redex.ViewOnClickCListenerShape0S0200000_I0;
import com.whatsapp.R;

/* renamed from: X.1PY  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1PY extends AnonymousClass1PZ {
    public long A00;
    public Handler A01;
    public Runnable A02;
    public final C14830m7 A03;
    public final C19990v2 A04;

    public AnonymousClass1PY(ActivityC000800j r27, C253519b r28, C14900mE r29, AnonymousClass2TT r30, AnonymousClass10A r31, C22330yu r32, AnonymousClass130 r33, AnonymousClass10S r34, C15610nY r35, AnonymousClass1J1 r36, AnonymousClass131 r37, C14830m7 r38, AnonymousClass018 r39, C17720rH r40, C19990v2 r41, C20830wO r42, C15600nX r43, C15370n3 r44, AnonymousClass19M r45, C14850m9 r46, C20710wC r47, C244215l r48, AbstractC14640lm r49, AnonymousClass12F r50, AbstractC14440lR r51) {
        super(r27, r28, r29, r30, r31, r32, r33, r34, r35, r36, r37, r39, r40, r41, r42, r43, r44, r45, r46, r47, r48, r49, r50, r51);
        this.A03 = r38;
        this.A04 = r41;
    }

    public String A05() {
        boolean z = false;
        if (System.currentTimeMillis() - this.A00 >= 5000) {
            z = true;
        }
        if (C41861uH.A00(this.A0b, this.A0f)) {
            return this.A0H.getApplicationContext().getString(R.string.wac_conversation_trust_signal_title, "WhatsApp");
        }
        if (!this.A0F.A0J() || z) {
            return this.A0I.A00(this.A0F);
        }
        return this.A0H.getApplicationContext().getString(R.string.conversation_contact_business_account);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0014, code lost:
        if (r0 <= 0) goto L_0x0016;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A06() {
        /*
            r3 = this;
            X.0n3 r1 = r3.A0F
            java.lang.Class<com.whatsapp.jid.UserJid> r0 = com.whatsapp.jid.UserJid.class
            com.whatsapp.jid.Jid r1 = r1.A0B(r0)
            X.0lm r1 = (X.AbstractC14640lm) r1
            r2 = 0
            if (r1 == 0) goto L_0x0016
            X.0v2 r0 = r3.A04
            int r0 = r0.A01(r1)
            r1 = 1
            if (r0 > 0) goto L_0x0017
        L_0x0016:
            r1 = 0
        L_0x0017:
            com.whatsapp.WaImageView r0 = r3.A0C
            if (r1 != 0) goto L_0x001d
            r2 = 8
        L_0x001d:
            r0.setVisibility(r2)
            if (r1 == 0) goto L_0x002a
            com.whatsapp.WaImageView r1 = r3.A0C
            r0 = 2131231867(0x7f08047b, float:1.8079827E38)
            r1.setImageResource(r0)
        L_0x002a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1PY.A06():void");
    }

    @Override // X.AnonymousClass1PZ, X.C28791Pa, android.app.Application.ActivityLifecycleCallbacks
    public void onActivityCreated(Activity activity, Bundle bundle) {
        super.onActivityCreated(activity, bundle);
        this.A00 = System.currentTimeMillis();
        if (this.A0F.A0J()) {
            this.A02 = new RunnableBRunnable0Shape4S0100000_I0_4(this, 42);
            Handler handler = new Handler(Looper.getMainLooper());
            this.A01 = handler;
            handler.postDelayed(this.A02, 5000);
        }
        super.A03.setOnClickListener(new ViewOnClickCListenerShape0S0200000_I0(this, 16, activity));
        AnonymousClass23N.A02(super.A03, R.string.accessibility_action_click_conversation_title_for_contact_info);
        A06();
    }

    @Override // X.AnonymousClass1PZ, X.C28791Pa, android.app.Application.ActivityLifecycleCallbacks
    public void onActivityDestroyed(Activity activity) {
        Runnable runnable;
        Handler handler = this.A01;
        if (!(handler == null || (runnable = this.A02) == null)) {
            handler.removeCallbacks(runnable);
        }
        super.onActivityDestroyed(activity);
    }
}
