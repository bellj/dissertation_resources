package X;

import android.content.Context;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import java.util.ArrayList;

/* renamed from: X.0XD  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0XD implements AnonymousClass02Q {
    public final Context A00;
    public final ActionMode.Callback A01;
    public final AnonymousClass00O A02 = new AnonymousClass00O();
    public final ArrayList A03 = new ArrayList();

    public AnonymousClass0XD(Context context, ActionMode.Callback callback) {
        this.A00 = context;
        this.A01 = callback;
    }

    public ActionMode A00(AbstractC009504t r6) {
        ArrayList arrayList = this.A03;
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            C02290At r1 = (C02290At) arrayList.get(i);
            if (r1 != null && r1.A01 == r6) {
                return r1;
            }
        }
        C02290At r0 = new C02290At(this.A00, r6);
        arrayList.add(r0);
        return r0;
    }

    @Override // X.AnonymousClass02Q
    public boolean ALr(MenuItem menuItem, AbstractC009504t r6) {
        return this.A01.onActionItemClicked(A00(r6), new AnonymousClass0CJ(this.A00, (AbstractMenuItemC017808h) menuItem));
    }

    @Override // X.AnonymousClass02Q
    public boolean AOg(Menu menu, AbstractC009504t r8) {
        ActionMode.Callback callback = this.A01;
        ActionMode A00 = A00(r8);
        AnonymousClass00O r3 = this.A02;
        Menu menu2 = (Menu) r3.get(menu);
        if (menu2 == null) {
            menu2 = new AnonymousClass0CI(this.A00, (AnonymousClass07I) menu);
            r3.put(menu, menu2);
        }
        return callback.onCreateActionMode(A00, menu2);
    }

    @Override // X.AnonymousClass02Q
    public void AP3(AbstractC009504t r3) {
        this.A01.onDestroyActionMode(A00(r3));
    }

    @Override // X.AnonymousClass02Q
    public boolean AU7(Menu menu, AbstractC009504t r8) {
        ActionMode.Callback callback = this.A01;
        ActionMode A00 = A00(r8);
        AnonymousClass00O r3 = this.A02;
        Menu menu2 = (Menu) r3.get(menu);
        if (menu2 == null) {
            menu2 = new AnonymousClass0CI(this.A00, (AnonymousClass07I) menu);
            r3.put(menu, menu2);
        }
        return callback.onPrepareActionMode(A00, menu2);
    }
}
