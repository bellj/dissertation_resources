package X;

import android.os.Handler;

/* renamed from: X.4PD  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4PD {
    public boolean A00;
    public final Handler A01;
    public final AnonymousClass5QM A02;

    public AnonymousClass4PD(Handler handler, AnonymousClass5QM r2) {
        this.A01 = handler;
        this.A02 = r2;
    }
}
