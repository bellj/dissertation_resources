package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.SignInAccount;

/* renamed from: X.4iy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98614iy implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        String str = "";
        GoogleSignInAccount googleSignInAccount = null;
        String str2 = str;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 4) {
                str = C95664e9.A08(parcel, readInt);
            } else if (c != 7) {
                str2 = C95664e9.A09(parcel, str2, c, 8, readInt);
            } else {
                googleSignInAccount = (GoogleSignInAccount) C95664e9.A07(parcel, GoogleSignInAccount.CREATOR, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new SignInAccount(googleSignInAccount, str, str2);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new SignInAccount[i];
    }
}
