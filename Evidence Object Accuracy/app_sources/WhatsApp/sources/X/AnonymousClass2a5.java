package X;

import android.os.Handler;
import android.os.Looper;

/* renamed from: X.2a5  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2a5 extends Handler {
    public final /* synthetic */ C35191hP A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2a5(Looper looper, C35191hP r2) {
        super(looper);
        this.A00 = r2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x00ad A[LOOP:1: B:33:0x00ad->B:35:0x00b0, LOOP_START, PHI: r6 
      PHI: (r6v1 int) = (r6v0 int), (r6v2 int) binds: [B:32:0x00ab, B:35:0x00b0] A[DONT_GENERATE, DONT_INLINE]] */
    @Override // android.os.Handler
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void handleMessage(android.os.Message r15) {
        /*
            r14 = this;
            X.1hP r7 = r14.A00
            X.1Ol r0 = r7.A0P
            if (r0 == 0) goto L_0x0015
            boolean r0 = r0.A0D()
            if (r0 != 0) goto L_0x0016
            boolean r0 = r7.A0U
            if (r0 != 0) goto L_0x0015
            r1 = 1
            r0 = 0
            r7.A0H(r1, r0)
        L_0x0015:
            return
        L_0x0016:
            int r2 = r7.A02()
            r7.A04 = r2
            X.2MF r0 = r7.A0K
            if (r0 == 0) goto L_0x0035
            X.1Xi r0 = r0.ACr()
            X.1IS r1 = r0.A0z
            X.1Xi r0 = r7.A0O
            X.1IS r0 = r0.A0z
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0035
            X.2MF r0 = r7.A0K
            r0.AUL(r2)
        L_0x0035:
            boolean r0 = r7.A0Y
            if (r0 == 0) goto L_0x0054
            X.19C r8 = r7.A0o
            X.11P r0 = r8.A0C
            X.1hP r0 = r0.A00
            if (r0 != r7) goto L_0x0054
            long r5 = java.lang.System.currentTimeMillis()
            long r0 = r8.A00
            long r3 = r5 - r0
            r1 = 1000(0x3e8, double:4.94E-321)
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x0054
            r8.A01(r7)
            r8.A00 = r5
        L_0x0054:
            X.5Tz r8 = r7.A0L
            if (r8 == 0) goto L_0x00bd
            android.media.audiofx.Visualizer r0 = r7.A0I
            if (r0 != 0) goto L_0x00bd
            int r0 = r7.A07
            int r11 = r0 + 1
            r7.A07 = r11
            byte[] r7 = X.C35191hP.A0z
            r6 = 0
            if (r7 != 0) goto L_0x00a9
            r0 = 128(0x80, float:1.794E-43)
            r10 = 128(0x80, float:1.794E-43)
            byte[] r7 = new byte[r0]
            X.C35191hP.A0z = r7
            r9 = 0
        L_0x0070:
            double r4 = (double) r9
            r0 = 4614256656552045848(0x400921fb54442d18, double:3.141592653589793)
            double r4 = r4 * r0
            r0 = 4629700416936869888(0x4040000000000000, double:32.0)
            double r0 = r0 * r4
            double r2 = (double) r10
            double r0 = r0 / r2
            double r0 = java.lang.Math.sin(r0)
            r12 = 4616189618054758400(0x4010000000000000, double:4.0)
            double r12 = r12 * r4
            double r12 = r12 / r2
            double r12 = java.lang.Math.sin(r12)
            double r12 = java.lang.Math.abs(r12)
            double r0 = r0 * r12
            r12 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r4 = r4 * r12
            double r4 = r4 / r2
            double r2 = java.lang.Math.sin(r4)
            double r2 = java.lang.Math.abs(r2)
            double r0 = r0 * r2
            r4 = 4638707616191610880(0x4060000000000000, double:128.0)
            r2 = 4634204016564240384(0x4050000000000000, double:64.0)
            double r0 = r0 * r2
            double r0 = r0 + r4
            int r2 = (int) r0
            byte r0 = (byte) r2
            r7[r9] = r0
            int r9 = r9 + 1
            if (r9 >= r10) goto L_0x00a9
            goto L_0x0070
        L_0x00a9:
            int r0 = r11 % 4
            if (r0 != 0) goto L_0x00ba
        L_0x00ad:
            int r0 = r7.length
            if (r6 >= r0) goto L_0x00ba
            byte r0 = r7[r6]
            int r0 = 256 - r0
            byte r0 = (byte) r0
            r7[r6] = r0
            int r6 = r6 + 1
            goto L_0x00ad
        L_0x00ba:
            r8.AYS(r7)
        L_0x00bd:
            X.C12990iw.A17(r14)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2a5.handleMessage(android.os.Message):void");
    }
}
