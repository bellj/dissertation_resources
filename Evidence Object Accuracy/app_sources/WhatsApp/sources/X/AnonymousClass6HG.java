package X;

import android.content.Context;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.6HG  reason: invalid class name */
/* loaded from: classes4.dex */
public final /* synthetic */ class AnonymousClass6HG implements Runnable {
    public final /* synthetic */ C118175bO A00;

    public /* synthetic */ AnonymousClass6HG(C118175bO r1) {
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public final void run() {
        C118175bO r5 = this.A00;
        C20370ve r8 = r5.A05;
        List A0b = r8.A0b(new Integer[]{20, 401}, new Integer[]{40}, 3);
        Integer[] numArr = new Integer[2];
        C12960it.A1P(numArr, 417, 0);
        C12960it.A1P(numArr, 418, 1);
        r5.A04.A0H(new Runnable(A0b, r8.A0b(numArr, new Integer[]{40}, 3)) { // from class: X.6Jc
            public final /* synthetic */ List A01;
            public final /* synthetic */ List A02;

            {
                this.A01 = r2;
                this.A02 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                C118175bO r4 = C118175bO.this;
                List list = this.A01;
                List list2 = this.A02;
                ArrayList A0l = C12960it.A0l();
                Context context = r4.A02.A00;
                C122795m7 r1 = new C122795m7(context.getString(R.string.upi_mandate_expandable_view_pending_title));
                r1.A00 = context.getString(R.string.upi_mandate_history_pending_description);
                A0l.add(r1);
                C122835mB r12 = new C122835mB();
                r12.A01 = r4;
                r12.A02 = context.getString(R.string.upi_mandate_expandable_view_pending_empty_text);
                r12.A03 = context.getString(R.string.upi_mandate_expandable_view_see_all);
                r12.A00 = C117305Zk.A0A(r4, 151);
                r12.A05 = list;
                A0l.add(r12);
                C122955mN r2 = new C122955mN(102);
                C122835mB r13 = new C122835mB();
                r13.A01 = r4;
                r13.A04 = context.getString(R.string.upi_mandate_expandable_view_scheduled_title);
                r13.A02 = context.getString(R.string.upi_mandate_expandable_view_scheduled_empty_text);
                r13.A03 = context.getString(R.string.upi_mandate_expandable_view_see_all);
                r13.A05 = list2;
                r13.A00 = C117305Zk.A0A(r4, 150);
                A0l.add(r2);
                A0l.add(r13);
                A0l.add(r2);
                r4.A01.A0B(A0l);
            }
        });
    }
}
