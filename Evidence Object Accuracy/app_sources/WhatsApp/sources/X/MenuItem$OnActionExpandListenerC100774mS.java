package X;

import android.view.MenuItem;
import com.whatsapp.contact.picker.PhoneContactsSelector;

/* renamed from: X.4mS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class MenuItem$OnActionExpandListenerC100774mS implements MenuItem.OnActionExpandListener {
    public final /* synthetic */ PhoneContactsSelector A00;

    @Override // android.view.MenuItem.OnActionExpandListener
    public boolean onMenuItemActionExpand(MenuItem menuItem) {
        return true;
    }

    public MenuItem$OnActionExpandListenerC100774mS(PhoneContactsSelector phoneContactsSelector) {
        this.A00 = phoneContactsSelector;
    }

    @Override // android.view.MenuItem.OnActionExpandListener
    public boolean onMenuItemActionCollapse(MenuItem menuItem) {
        PhoneContactsSelector phoneContactsSelector = this.A00;
        phoneContactsSelector.A0V = null;
        PhoneContactsSelector.A03(phoneContactsSelector);
        return true;
    }
}
