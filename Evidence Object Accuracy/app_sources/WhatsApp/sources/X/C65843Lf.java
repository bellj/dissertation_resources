package X;

import android.os.Handler;
import android.os.Message;

/* renamed from: X.3Lf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C65843Lf implements Handler.Callback {
    public final /* synthetic */ AnonymousClass39B A00;

    public C65843Lf(AnonymousClass39B r1) {
        this.A00 = r1;
    }

    @Override // android.os.Handler.Callback
    public boolean handleMessage(Message message) {
        AnonymousClass21T r0;
        AnonymousClass39B r5 = this.A00;
        if (r5.A0F) {
            if (!r5.A09 && (r0 = r5.A06) != null && r0.A0B() && r5.A06.A02() != 0) {
                int A01 = (int) ((((long) r5.A06.A01()) * 1000) / ((long) r5.A06.A02()));
                r5.A0c.setProgress(A01);
                if (!r5.A0G) {
                    r5.A0d.setProgress(A01);
                    r5.A0e.setText(AnonymousClass2Bd.A01(r5.A0j, r5.A0k, (long) r5.A06.A01()));
                }
            }
            C12990iw.A17(r5.A0I);
        }
        return true;
    }
}
