package X;

import com.whatsapp.payments.ui.BrazilPayBloksActivity;

/* renamed from: X.5wf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128785wf {
    public final /* synthetic */ AnonymousClass3FE A00;
    public final /* synthetic */ BrazilPayBloksActivity A01;

    public C128785wf(AnonymousClass3FE r1, BrazilPayBloksActivity brazilPayBloksActivity) {
        this.A01 = brazilPayBloksActivity;
        this.A00 = r1;
    }

    public void A00(C130765zw r3) {
        C452120p r0 = r3.A00;
        if (r0 == null) {
            r0 = C117305Zk.A0L();
        }
        if (r0.A00 == 25554) {
            this.A01.A2l(this.A00);
        } else {
            C117315Zl.A0S(this.A00);
        }
    }
}
