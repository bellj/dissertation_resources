package X;

import android.view.animation.Animation;
import android.widget.TextView;
import com.whatsapp.payments.ui.widget.PaymentAmountInputField;

/* renamed from: X.5d1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119165d1 extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ TextView A00;
    public final /* synthetic */ PaymentAmountInputField A01;

    public C119165d1(TextView textView, PaymentAmountInputField paymentAmountInputField) {
        this.A01 = paymentAmountInputField;
        this.A00 = textView;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        this.A00.setVisibility(8);
    }
}
