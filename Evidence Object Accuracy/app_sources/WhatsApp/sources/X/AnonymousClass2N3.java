package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0300100_I0;
import com.whatsapp.util.Log;

/* renamed from: X.2N3  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2N3 implements AbstractC21730xt {
    public C16160oY A00;
    public final C17220qS A01;

    public AnonymousClass2N3(C17220qS r1) {
        this.A01 = r1;
    }

    public String A00(byte[] bArr, String str) {
        String str2;
        if (this.A00 == null) {
            str2 = "ACSSender/requestToSign need to set iq response listener first";
        } else {
            C17220qS r10 = this.A01;
            String A01 = r10.A01();
            if (r10.A0D(this, new AnonymousClass1V8(new AnonymousClass1V8("sign_credential", new AnonymousClass1W9[]{new AnonymousClass1W9("version", "2")}, new AnonymousClass1V8[]{new AnonymousClass1V8("blinded_credential", bArr, (AnonymousClass1W9[]) null), new AnonymousClass1V8("project_name", str, (AnonymousClass1W9[]) null)}), "iq", new AnonymousClass1W9[]{new AnonymousClass1W9("xmlns", "privatestats"), new AnonymousClass1W9("id", A01), new AnonymousClass1W9("type", "get"), new AnonymousClass1W9(AnonymousClass1VY.A00, "to")}), A01, 278, 32000)) {
                return A01;
            }
            str2 = "ACSSender/requestToSign failed to send iq request";
        }
        Log.e(str2);
        return null;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        StringBuilder sb = new StringBuilder("ACSSender/onDeliveryFailure iqId = ");
        sb.append(str);
        Log.e(sb.toString());
        C16160oY r2 = this.A00;
        if (r2 == null) {
            return;
        }
        if (!str.equalsIgnoreCase(r2.A0E)) {
            Log.e("ACSToken/onSendFailure mismatched iq id, reset");
            r2.A05.A01(12);
            r2.A00();
            return;
        }
        r2.A01(5);
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r5, String str) {
        StringBuilder sb = new StringBuilder("ACSSender/onError iqId = ");
        sb.append(str);
        Log.e(sb.toString());
        AnonymousClass1V8 A0F = r5.A0F("error");
        int A06 = A0F.A06(A0F.A0H("code"), "code");
        A0F.A0I("text", "");
        C16160oY r2 = this.A00;
        if (r2 == null) {
            return;
        }
        if (!str.equalsIgnoreCase(r2.A0E)) {
            Log.e("ACSToken/onIqResponseError mismatched iq id, reset");
            r2.A00();
        } else if (A06 == 500) {
            r2.A01(3);
        } else {
            Log.e("ACSToken/onIqResponseError iq errors, stop attempting to send iq");
            r2.A05.A01(11);
            r2.A03(false);
        }
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r9, String str) {
        AnonymousClass1V8 A0F = r9.A0F("sign_credential");
        long A09 = A0F.A09(A0F.A0H("t"), "t");
        byte[] bArr = A0F.A0F("signed_credential").A01;
        byte[] bArr2 = A0F.A0F("acs_public_key").A01;
        AnonymousClass1V8 A0E = A0F.A0E("dleq_proof");
        if (A0E != null) {
            A0E.A0F("c");
            A0E.A0F("s");
        }
        C16160oY r2 = this.A00;
        if (r2 != null) {
            synchronized (r2) {
                if (!str.equalsIgnoreCase(r2.A0E)) {
                    Log.e("ACSToken/onReceiveSignedToken iq requests messed up, reset");
                    r2.A00();
                } else if (bArr2 == null || bArr == null) {
                    r2.A05.A01(10);
                    r2.A03(false);
                } else {
                    r2.A09.execute(new RunnableBRunnable0Shape0S0300100_I0(r2, bArr2, bArr, 1, A09));
                }
            }
        }
    }
}
