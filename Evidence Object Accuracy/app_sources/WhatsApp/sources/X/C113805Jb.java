package X;

import com.whatsapp.catalogsearch.view.fragment.CatalogSearchFragment;

/* renamed from: X.5Jb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C113805Jb extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ CatalogSearchFragment this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C113805Jb(CatalogSearchFragment catalogSearchFragment) {
        super(0);
        this.this$0 = catalogSearchFragment;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        return CatalogSearchFragment.A00(this.this$0, AnonymousClass41B.A00);
    }
}
