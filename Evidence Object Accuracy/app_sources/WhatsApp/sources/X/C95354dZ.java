package X;

import java.security.cert.X509Certificate;
import javax.security.auth.x500.X500Principal;

/* renamed from: X.4dZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C95354dZ {
    public static AnonymousClass5N2 A00(Object obj) {
        if (obj instanceof X509Certificate) {
            return A01((X509Certificate) obj);
        }
        throw C12980iv.A0n("getIssuer");
    }

    public static AnonymousClass5N2 A01(X509Certificate x509Certificate) {
        if (x509Certificate instanceof AnonymousClass5S0) {
            AnonymousClass5N2 r0 = ((AbstractC113485Ht) ((AnonymousClass5S0) x509Certificate)).c.A03.A05;
            if (r0 != null) {
                return r0;
            }
            throw C72463ee.A0D();
        } else if (x509Certificate != null) {
            return A03(x509Certificate.getIssuerX500Principal());
        } else {
            throw C72463ee.A0D();
        }
    }

    public static AnonymousClass5N2 A02(X509Certificate x509Certificate) {
        if (!(x509Certificate instanceof AnonymousClass5S0)) {
            return A03(x509Certificate.getSubjectX500Principal());
        }
        AnonymousClass5N2 r0 = ((AbstractC113485Ht) ((AnonymousClass5S0) x509Certificate)).c.A03.A06;
        if (r0 != null) {
            return r0;
        }
        throw C72463ee.A0D();
    }

    public static AnonymousClass5N2 A03(X500Principal x500Principal) {
        if (x500Principal != null) {
            byte[] encoded = x500Principal.getEncoded();
            if (encoded != null) {
                AnonymousClass5N2 A00 = AnonymousClass5N2.A00(encoded);
                if (A00 != null) {
                    return A00;
                }
                throw C72463ee.A0D();
            }
            throw C72463ee.A0D();
        }
        throw C72463ee.A0D();
    }
}
