package X;

import android.graphics.drawable.Drawable;

/* renamed from: X.2iC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C55312iC extends AnonymousClass03J {
    public final /* synthetic */ C60822yh A00;
    public final /* synthetic */ C30061Vy A01;

    public C55312iC(C60822yh r1, C30061Vy r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass03J
    public void A01(Drawable drawable) {
        AbstractC13890kV r1;
        if ((drawable instanceof C39631qF) && (r1 = ((AbstractC28551Oa) this.A00).A0a) != null) {
            r1.AfZ(this.A01);
        }
    }
}
