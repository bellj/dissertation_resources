package X;

import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0Rn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05930Rn {
    public static final String A03 = C06390Tk.A01("DelayedWorkTracker");
    public final AbstractC11410gF A00;
    public final C07680Zs A01;
    public final Map A02 = new HashMap();

    public C05930Rn(AbstractC11410gF r2, C07680Zs r3) {
        this.A01 = r3;
        this.A00 = r2;
    }
}
