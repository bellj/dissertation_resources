package X;

/* renamed from: X.2Q6  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Q6 extends AnonymousClass1JW {
    public final AbstractC14640lm A00;
    public final String A01;
    public final boolean A02;

    public AnonymousClass2Q6(AbstractC15710nm r1, C15450nH r2, AbstractC14640lm r3, String str, boolean z) {
        super(r1, r2);
        this.A00 = r3;
        this.A01 = str;
        this.A02 = z;
    }
}
