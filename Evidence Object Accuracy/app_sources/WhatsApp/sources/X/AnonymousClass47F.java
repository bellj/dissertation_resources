package X;

import com.whatsapp.Conversation;

/* renamed from: X.47F  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass47F extends AnonymousClass1KM {
    public final /* synthetic */ Conversation A00;

    public AnonymousClass47F(Conversation conversation) {
        this.A00 = conversation;
    }

    @Override // X.AnonymousClass1KM
    public void A00() {
        Conversation conversation = this.A00;
        if (conversation.AIN()) {
            AbstractC14670lq r0 = conversation.A3y;
            AnonymousClass009.A05(r0);
            r0.A03();
        }
    }
}
