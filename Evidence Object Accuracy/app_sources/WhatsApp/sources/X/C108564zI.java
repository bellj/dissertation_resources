package X;

import com.whatsapp.voipcalling.GlVideoRenderer;
import org.chromium.net.UrlRequest;

/* renamed from: X.4zI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C108564zI implements AbstractC115605Sf {
    @Override // X.AbstractC115605Sf
    public final /* synthetic */ AbstractC115135Qi Ah4(int i) {
        switch (i) {
            case -1:
                return EnumC87354Be.A02;
            case 0:
                return EnumC87354Be.A03;
            case 1:
                return EnumC87354Be.A04;
            case 2:
                return EnumC87354Be.A05;
            case 3:
                return EnumC87354Be.A06;
            case 4:
                return EnumC87354Be.A07;
            case 5:
                return EnumC87354Be.A08;
            case 6:
                return EnumC87354Be.A09;
            case 7:
                return EnumC87354Be.A0A;
            case 8:
                return EnumC87354Be.A0B;
            case 9:
                return EnumC87354Be.A0C;
            case 10:
                return EnumC87354Be.A0D;
            case 11:
                return EnumC87354Be.A0E;
            case 12:
                return EnumC87354Be.A0F;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                return EnumC87354Be.A0G;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                return EnumC87354Be.A0H;
            case 15:
                return EnumC87354Be.A0I;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                return EnumC87354Be.A0J;
            case 17:
                return EnumC87354Be.A0K;
            default:
                return null;
        }
    }
}
