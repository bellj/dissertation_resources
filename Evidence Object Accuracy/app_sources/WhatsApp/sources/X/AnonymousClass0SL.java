package X;

/* renamed from: X.0SL  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0SL {
    public float A00;
    public int A01;
    public int A02;
    public AnonymousClass0JY A03;
    public String A04;
    public String A05;
    public boolean A06;

    public AnonymousClass0SL(AnonymousClass0JY r1, Object obj, String str) {
        this.A04 = str;
        this.A03 = r1;
        A00(obj);
    }

    public AnonymousClass0SL(AnonymousClass0SL r2, Object obj) {
        this.A04 = r2.A04;
        this.A03 = r2.A03;
        A00(obj);
    }

    public void A00(Object obj) {
        switch (this.A03.ordinal()) {
            case 0:
                this.A02 = ((Number) obj).intValue();
                return;
            case 1:
            case 6:
                this.A00 = ((Number) obj).floatValue();
                return;
            case 2:
            case 3:
                this.A01 = ((Number) obj).intValue();
                return;
            case 4:
                this.A05 = (String) obj;
                return;
            case 5:
                this.A06 = ((Boolean) obj).booleanValue();
                return;
            default:
                return;
        }
    }
}
