package X;

import java.math.BigDecimal;

/* renamed from: X.27y  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C468527y {
    public static AbstractC30791Yv A00;
    public static AbstractC30791Yv A01;
    public static AbstractC30791Yv A02 = new C30801Yw("USDP", "USDP", "USD", new BigDecimal(Integer.MAX_VALUE), new BigDecimal("0.01"), new String[]{"USD", "GTQ"}, 1000000, 2, 50, true);

    static {
        BigDecimal bigDecimal = new BigDecimal(Integer.MAX_VALUE);
        BigDecimal bigDecimal2 = C30771Yt.A07;
        A01 = new C30771Yt("USD", "$", "#", "#", bigDecimal, bigDecimal2, 0, 100, 2, 0);
        A00 = new C30771Yt("GTQ", "Q", "#", "#", new BigDecimal(Integer.MAX_VALUE), bigDecimal2, 0, 100, 2, 0);
    }
}
