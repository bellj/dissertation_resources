package X;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.ContextWrapper;
import android.os.Bundle;
import java.util.WeakHashMap;

/* renamed from: X.3K4  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3K4 implements Application.ActivityLifecycleCallbacks {
    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityPaused(Activity activity) {
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityResumed(Activity activity) {
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityStarted(Activity activity) {
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityStopped(Activity activity) {
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityCreated(Activity activity, Bundle bundle) {
        synchronized (AnonymousClass3IH.A01) {
            if (AnonymousClass3IH.A03.containsKey(activity)) {
                throw C12960it.A0U("The MountContentPools has a reference to an activity that has just been created");
            }
        }
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityDestroyed(Activity activity) {
        synchronized (AnonymousClass3IH.A01) {
            AnonymousClass3IH.A01(activity, AnonymousClass3IH.A03);
            AnonymousClass3IH.A01(activity, AnonymousClass3IH.A02);
            WeakHashMap weakHashMap = AnonymousClass3IH.A04;
            Activity activity2 = activity;
            while ((activity2 instanceof ContextWrapper) && !(activity2 instanceof Activity) && !(activity2 instanceof Application) && !(activity2 instanceof Service)) {
                activity2 = activity2.getBaseContext();
            }
            weakHashMap.put(activity2, Boolean.TRUE);
        }
    }
}
