package X;

import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.media.AudioRecord;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.PowerManager;
import android.os.SystemClock;
import android.os.Vibrator;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.redex.RunnableBRunnable0Shape0S0400000_I0;
import com.facebook.redex.RunnableBRunnable0Shape13S0100000_I0_13;
import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;
import com.facebook.redex.ViewOnClickCListenerShape5S0100000_I0_5;
import com.whatsapp.Conversation;
import com.whatsapp.R;
import com.whatsapp.audioRecording.AudioRecordFactory;
import com.whatsapp.audioRecording.OpusRecorderFactory;
import com.whatsapp.conversation.waveforms.VoiceVisualizer;
import com.whatsapp.util.ClippingLayout;
import com.whatsapp.util.Log;
import com.whatsapp.util.OpusRecorder;
import java.io.File;
import java.io.IOException;

/* renamed from: X.0lq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC14670lq {
    public static int A1V;
    public static int A1W;
    public static int A1X;
    public static int A1Y;
    public static int A1Z;
    public static int A1a;
    public static SoundPool A1b;
    public float A00;
    public float A01;
    public float A02;
    public float A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public int A09;
    public long A0A;
    public long A0B;
    public long A0C;
    public long A0D;
    public AnimatorSet A0E;
    public ObjectAnimator A0F;
    public PowerManager.WakeLock A0G;
    public AnonymousClass2KT A0H;
    public C32701cb A0I;
    public AbstractC14640lm A0J;
    public C37991nL A0K;
    public AbstractC15340mz A0L;
    public HandlerC28161Kx A0M;
    public AbstractC28651Ol A0N;
    public AnonymousClass2CF A0O;
    public C14690ls A0P;
    public File A0Q;
    public File A0R;
    public boolean A0S;
    public boolean A0T;
    public boolean A0U;
    public boolean A0V;
    public boolean A0W;
    public final float A0X;
    public final Rect A0Y;
    public final Handler A0Z;
    public final Handler A0a;
    public final Handler A0b;
    public final View A0c;
    public final View A0d;
    public final View A0e;
    public final View A0f;
    public final ImageView A0g;
    public final TextView A0h;
    public final TextView A0i;
    public final ActivityC000900k A0j;
    public final AnonymousClass4YC A0k;
    public final C28151Kw A0l;
    public final C28151Kw A0m;
    public final AbstractC15710nm A0n;
    public final AbstractC13860kS A0o;
    public final C14330lG A0p;
    public final C14900mE A0q;
    public final C16170oZ A0r;
    public final AudioRecordFactory A0s;
    public final OpusRecorderFactory A0t;
    public final AbstractC22240yl A0u;
    public final C18280sC A0v;
    public final AnonymousClass11P A0w;
    public final AnonymousClass2IA A0x;
    public final AnonymousClass01d A0y;
    public final C14830m7 A0z;
    public final C14820m6 A10;
    public final AnonymousClass018 A11;
    public final C21310xD A12;
    public final C14850m9 A13;
    public final C14410lO A14;
    public final AnonymousClass109 A15;
    public final C16630pM A16;
    public final C20320vZ A17;
    public final AnonymousClass1AL A18;
    public final AnonymousClass199 A19;
    public final C255819y A1A;
    public final ClippingLayout A1B;
    public final ClippingLayout A1C;
    public final AbstractC14440lR A1D;
    public final C17020q8 A1E;
    public final C254419k A1F;
    public final C14680lr A1G;
    public final AnonymousClass19A A1H;
    public final C63683Cn A1I;
    public final AnonymousClass2SX A1J;
    public final AnonymousClass4SB A1K;
    public final AnonymousClass2SV A1L;
    public final C236712o A1M;
    public final C21260x8 A1N;
    public final Runnable A1O;
    public final Runnable A1P;
    public final Runnable A1Q;
    public final Runnable A1R = new RunnableBRunnable0Shape13S0100000_I0_13(this, 27);
    public final boolean A1S;
    public final boolean A1T;
    public final boolean A1U;

    public AbstractC14670lq(View view, ActivityC000900k r26, AbstractC15710nm r27, AbstractC13860kS r28, C14330lG r29, C14900mE r30, C15450nH r31, C16170oZ r32, AudioRecordFactory audioRecordFactory, OpusRecorderFactory opusRecorderFactory, C18280sC r35, AnonymousClass11P r36, AnonymousClass2IA r37, AnonymousClass01d r38, C14830m7 r39, C14820m6 r40, AnonymousClass018 r41, C21310xD r42, C14850m9 r43, C22050yP r44, C14410lO r45, AnonymousClass109 r46, C16630pM r47, C20320vZ r48, AnonymousClass1AL r49, AnonymousClass199 r50, C255819y r51, AbstractC14440lR r52, C17020q8 r53, C254419k r54, C14680lr r55, AnonymousClass19A r56, C63683Cn r57, C21260x8 r58, boolean z, boolean z2) {
        Handler handler = new Handler(Looper.getMainLooper());
        this.A0a = handler;
        this.A0l = new C28151Kw(0.0d);
        C28151Kw r5 = new C28151Kw(Double.MIN_VALUE);
        this.A0m = r5;
        this.A0Y = new Rect();
        this.A08 = -1;
        AnonymousClass481 r4 = new AnonymousClass481(this);
        this.A1M = r4;
        this.A0Z = new Handler(Looper.getMainLooper());
        this.A1O = new AnonymousClass2CE(this);
        this.A0u = new AbstractC22240yl() { // from class: X.53J
            /* JADX WARNING: Code restructure failed: missing block: B:5:0x0023, code lost:
                if (r2.getKeepScreenOn() == false) goto L_0x0025;
             */
            @Override // X.AbstractC22240yl
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void ANB(X.C32441cA r7) {
                /*
                    r6 = this;
                    X.0lq r2 = X.AbstractC14670lq.this
                    double r4 = r7.A00()
                    java.lang.String r0 = "voicenoterecordingui/onevent/battery change percentage: "
                    java.lang.StringBuilder r0 = X.C12960it.A0k(r0)
                    r0.append(r4)
                    java.lang.String r0 = r0.toString()
                    com.whatsapp.util.Log.i(r0)
                    r0 = 4625196817309499392(0x4030000000000000, double:16.0)
                    int r3 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
                    android.view.View r2 = r2.A0f
                    if (r2 == 0) goto L_0x0025
                    boolean r1 = r2.getKeepScreenOn()
                    r0 = 1
                    if (r1 != 0) goto L_0x0026
                L_0x0025:
                    r0 = 0
                L_0x0026:
                    if (r3 < 0) goto L_0x0037
                    if (r0 != 0) goto L_0x0036
                    if (r2 == 0) goto L_0x0036
                    boolean r0 = r2.getKeepScreenOn()
                    if (r0 != 0) goto L_0x0036
                    r0 = 1
                L_0x0033:
                    r2.setKeepScreenOn(r0)
                L_0x0036:
                    return
                L_0x0037:
                    if (r0 == 0) goto L_0x0036
                    if (r2 == 0) goto L_0x0036
                    boolean r0 = r2.getKeepScreenOn()
                    if (r0 == 0) goto L_0x0036
                    r0 = 0
                    goto L_0x0033
                */
                throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass53J.ANB(X.1cA):void");
            }
        };
        this.A0z = r39;
        this.A13 = r43;
        this.A0q = r30;
        this.A18 = r49;
        this.A0n = r27;
        this.A1D = r52;
        this.A0p = r29;
        this.A19 = r50;
        this.A0r = r32;
        this.A14 = r45;
        this.A1N = r58;
        this.A1F = r54;
        this.A0y = r38;
        this.A11 = r41;
        this.A17 = r48;
        this.A0s = audioRecordFactory;
        this.A1G = r55;
        this.A12 = r42;
        this.A10 = r40;
        this.A1H = r56;
        this.A0v = r35;
        this.A1E = r53;
        this.A15 = r46;
        this.A16 = r47;
        this.A0t = opusRecorderFactory;
        this.A0x = r37;
        this.A1I = r57;
        this.A1S = z;
        this.A1T = z2;
        this.A1A = r51;
        this.A0w = r36;
        this.A0j = r26;
        this.A0o = r28;
        this.A0f = view;
        this.A1U = r43.A07(1139);
        this.A1K = new AnonymousClass4SB(view, r41);
        ImageView imageView = (ImageView) view.findViewById(R.id.voice_note_btn_slider);
        this.A0g = imageView;
        imageView.setImageResource(R.drawable.input_mic_white_large);
        imageView.setBackgroundResource(R.drawable.input_circle_large);
        imageView.setPadding(0, 0, 0, 0);
        PowerManager A0I = r38.A0I();
        if (A0I == null) {
            Log.w("voicenoterecordingui pm=null");
        } else {
            this.A0G = C39151pN.A00(A0I, "voicenote", 6);
        }
        if (A1b == null) {
            SoundPool soundPool = new SoundPool(1, 1, 0);
            A1b = soundPool;
            A1a = soundPool.load(r26, R.raw.wa_ptt_start_record, 0);
            A1X = A1b.load(r26, R.raw.wa_ptt_quick_cancel, 0);
            A1Y = A1b.load(r26, R.raw.wa_ptt_sent, 0);
            A1Z = A1b.load(r26, R.raw.wa_ptt_slide_to_cancel, 0);
            A1W = A1b.load(r26, R.raw.wa_ptt_stop_record, 0);
            A1V = A1b.load(r26, R.raw.wa_ptt_quick_cancel, 0);
        }
        this.A1C = (ClippingLayout) view.findViewById(R.id.voice_note_clipping_layout);
        this.A0h = (TextView) view.findViewById(R.id.entry);
        TextView textView = (TextView) view.findViewById(R.id.voice_note_slide_to_cancel);
        this.A0i = textView;
        View findViewById = view.findViewById(R.id.input_layout);
        this.A0d = findViewById;
        this.A1B = (ClippingLayout) view.findViewById(R.id.footer);
        this.A0c = findViewById.findViewById(R.id.entry);
        r43.A07(746);
        if (!r41.A04().A06) {
            textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.voice_note_slide_to_cancel, 0, 0, 0);
        } else {
            textView.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, new AnonymousClass2GF(AnonymousClass00T.A04(r26, R.drawable.voice_note_slide_to_cancel), r41), (Drawable) null);
        }
        View findViewById2 = view.findViewById(R.id.voice_note_slide_to_cancel_scroller);
        findViewById2.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver$OnPreDrawListenerC66453Nq(findViewById2, this));
        ((ViewGroup) view.findViewById(R.id.voice_recorder_decor)).addView(new C73713gg(r26, this), -1, -1);
        AnonymousClass4YC A01 = C94254bV.A00().A01();
        this.A0k = A01;
        A01.A05 = new C93374a4(440.0d, 21.0d);
        this.A0e = view.findViewById(R.id.quoted_message_preview_container);
        this.A0X = ((float) ViewConfiguration.get(r26).getScaledTouchSlop()) * 1.5f;
        r58.A03(r4);
        C28641Ok r8 = new C28641Ok(this);
        this.A1J = new AnonymousClass2SX(r40, r44, r8);
        C70863by r14 = new AbstractC28661Om() { // from class: X.3by
            @Override // X.AbstractC28661Om
            public final void A6q(Object obj) {
                AbstractC14670lq r2 = AbstractC14670lq.this;
                File file = (File) obj;
                AnonymousClass109 r410 = r2.A15;
                AbstractC14640lm r3 = r2.A0J;
                Log.i("app/mediajobmanager/enqueuevoicenoteupload enqueuing file ");
                AnonymousClass1K9 A00 = AnonymousClass1K9.A00(Uri.fromFile(file), null, null, new C14480lV(true, false, true), C14370lK.A0I, null, null, 1, false, false, true, false);
                C14300lD r82 = r410.A0E;
                AnonymousClass1KC A04 = r82.A04(A00, true);
                C14450lS r1 = A04.A0K;
                AnonymousClass009.A05(r1);
                r1.A05(2);
                AnonymousClass009.A05(r1);
                r1.A03();
                A04.A0U = "mms";
                A04.A08.A04(new C39871qg(file, false));
                A04.A03(
                /*  JADX ERROR: Method code generation error
                    jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0058: INVOKE  
                      (r7v1 'A04' X.1KC)
                      (wrap: X.3bg : 0x0053: CONSTRUCTOR  (r1v3 X.3bg A[REMOVE]) = (r3v0 'r3' X.0lm), (r4v0 'r410' X.109), (r7v1 'A04' X.1KC) call: X.3bg.<init>(X.0lm, X.109, X.1KC):void type: CONSTRUCTOR)
                      (wrap: java.util.concurrent.Executor : 0x0056: IGET  (r0v6 java.util.concurrent.Executor A[REMOVE]) = (r4v0 'r410' X.109) X.109.A0U java.util.concurrent.Executor)
                     type: VIRTUAL call: X.1KC.A03(X.0lg, java.util.concurrent.Executor):void in method: X.3by.A6q(java.lang.Object):void, file: classes2.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                    	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                    	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                    	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                    	at java.util.ArrayList.forEach(ArrayList.java:1259)
                    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                    Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0053: CONSTRUCTOR  (r1v3 X.3bg A[REMOVE]) = (r3v0 'r3' X.0lm), (r4v0 'r410' X.109), (r7v1 'A04' X.1KC) call: X.3bg.<init>(X.0lm, X.109, X.1KC):void type: CONSTRUCTOR in method: X.3by.A6q(java.lang.Object):void, file: classes2.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                    	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                    	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                    	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                    	... 15 more
                    Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.3bg, state: NOT_LOADED
                    	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                    	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                    	... 21 more
                    */
                /*
                    this = this;
                    r6 = r20
                    r0 = r19
                    X.0lq r2 = X.AbstractC14670lq.this
                    java.io.File r6 = (java.io.File) r6
                    X.109 r4 = r2.A15
                    X.0lm r3 = r2.A0J
                    java.lang.String r0 = "app/mediajobmanager/enqueuevoicenoteupload enqueuing file "
                    com.whatsapp.util.Log.i(r0)
                    r0 = 1
                    r5 = 0
                    X.0lV r10 = new X.0lV
                    r10.<init>(r0, r5, r0)
                    X.0lK r11 = X.C14370lK.A0I
                    android.net.Uri r7 = android.net.Uri.fromFile(r6)
                    r8 = 0
                    r12 = r8
                    r13 = r8
                    r16 = 0
                    r17 = 1
                    r18 = 0
                    r15 = 0
                    r14 = 1
                    r9 = r8
                    X.1K9 r1 = X.AnonymousClass1K9.A00(r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18)
                    X.0lD r8 = r4.A0E
                    X.1KC r7 = r8.A04(r1, r0)
                    X.0lS r1 = r7.A0K
                    X.AnonymousClass009.A05(r1)
                    r0 = 2
                    r1.A05(r0)
                    X.AnonymousClass009.A05(r1)
                    r1.A03()
                    java.lang.String r0 = "mms"
                    r7.A0U = r0
                    X.1qg r1 = new X.1qg
                    r1.<init>(r6, r5)
                    X.0lj r0 = r7.A08
                    r0.A04(r1)
                    X.3bg r1 = new X.3bg
                    r1.<init>(r3, r4, r7)
                    java.util.concurrent.Executor r0 = r4.A0U
                    r7.A03(r1, r0)
                    X.0lL r1 = r7.A00()
                    X.1nL r0 = new X.1nL
                    r0.<init>(r7, r1)
                    r8.A09(r7, r0)
                    r2.A0K = r0
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: X.C70863by.A6q(java.lang.Object):void");
            }
        };
        C70883c0 r13 = new AbstractC28661Om() { // from class: X.3c0
            @Override // X.AbstractC28661Om
            public final void A6q(Object obj) {
                AbstractC14670lq r6 = AbstractC14670lq.this;
                int A05 = C12960it.A05(obj);
                if (A05 != r6.A04) {
                    AnonymousClass4SB r410 = r6.A1K;
                    boolean z3 = r6.A0W;
                    String A04 = C38131nZ.A04(r410.A03, (long) A05);
                    r410.A01.setText(A04);
                    TextView textView2 = r410.A00;
                    textView2.setText(A04);
                    if (z3) {
                        textView2.setVisibility(4);
                    }
                    r6.A04 = A05;
                }
            }
        };
        this.A0b = new HandlerC52102aF(r5, r32, r55, new AnonymousClass5V6() { // from class: X.5Al
            @Override // X.AnonymousClass5V6
            public final void A6p() {
                AbstractC14670lq.this.A0T(true, false);
            }
        }, new AnonymousClass5V6() { // from class: X.5Am
            @Override // X.AnonymousClass5V6
            public final void A6p() {
                AbstractC14670lq r2 = AbstractC14670lq.this;
                r2.A0D = r2.A0z.A00();
            }
        }, new AnonymousClass5V6() { // from class: X.3bx
            @Override // X.AnonymousClass5V6
            public final void A6p() {
                AbstractC14670lq r410 = AbstractC14670lq.this;
                C14690ls r0 = r410.A0P;
                if (r0 == null || r0.A09.isRecording()) {
                    int i = r410.A07 + 1;
                    r410.A07 = i;
                    if (r410.A1U && i == 6) {
                        r410.A1G.A0C.setVisibility(4);
                        if (r410.A13.A07(1140)) {
                            SharedPreferences sharedPreferences = r410.A10.A00;
                            if (!C12980iv.A1W(sharedPreferences, "ptt_draft_preview_error_count_reached")) {
                                C12960it.A0t(sharedPreferences.edit(), "ptt_draft_preview_error_count_reached", true);
                                r410.A0n.AaV("voice_visualization_error_count_reached_critical_event_name", null, true);
                            }
                        }
                    }
                }
            }
        }, new AnonymousClass5V6() { // from class: X.5Ak
            @Override // X.AnonymousClass5V6
            public final void A6p() {
                AbstractC14670lq.this.A06++;
            }
        }, r8, r14, r13, ((long) r31.A02(AbstractC15460nI.A1p)) * 1048576);
        this.A1Q = new RunnableBRunnable0Shape0S0400000_I0(new AbstractC28661Om() { // from class: X.3bz
            @Override // X.AbstractC28661Om
            public final void A6q(Object obj) {
                long j;
                AbstractC14670lq r0 = AbstractC14670lq.this;
                boolean A1Y2 = C12970iu.A1Y(obj);
                C14680lr r59 = r0.A1G;
                AbstractC28651Ol r02 = r0.A0N;
                if (r02 != null) {
                    int A02 = r02.A02();
                    boolean A0D = r02.A0D();
                    int A03 = r02.A03();
                    if (A1Y2) {
                        A02 = 0;
                        A0D = false;
                    }
                    r59.A0G.setProgress(A02);
                    r59.A0D.setPlaybackPercentage(((float) A02) / ((float) A03));
                    if (A0D) {
                        j = (long) A02;
                    } else {
                        j = (long) A03;
                    }
                    C38131nZ.A0C(r59.A0A, r59.A0E, C12980iv.A0D(j));
                }
            }
        }, new AbstractC28661Om() { // from class: X.5Aq
            @Override // X.AbstractC28661Om
            public final void A6q(Object obj) {
                AbstractC14670lq r3 = AbstractC14670lq.this;
                if (C12970iu.A1Y(obj)) {
                    C14680lr r1 = r3.A1G;
                    ImageButton imageButton = r1.A08;
                    imageButton.setImageResource(R.drawable.inline_audio_pause);
                    imageButton.setContentDescription(r1.A00.getString(R.string.pause));
                } else {
                    r3.A0A();
                }
                r3.A0B();
            }
        }, handler, r8, 32);
        this.A1P = new RunnableBRunnable0Shape8S0200000_I0_8(this, 41, r28);
        this.A1L = new AnonymousClass2SV() { // from class: X.5Ap
            @Override // X.AnonymousClass2SV
            public final void AYR() {
                AbstractC14670lq r1 = AbstractC14670lq.this;
                if (r1.A0V()) {
                    r1.A1J.A00();
                    r1.A03();
                    return;
                }
                r1.A05();
            }
        };
    }

    public static /* synthetic */ void A00(AbstractC14670lq r3) {
        AbstractC28651Ol r2 = r3.A0N;
        if (r2 != null) {
            try {
                int i = r3.A08;
                if (i != -1) {
                    r2.A0A(i);
                    if (r3.A0N.A0D()) {
                        r3.A0a.post(r3.A1Q);
                        C14680lr r1 = r3.A1G;
                        ImageButton imageButton = r1.A08;
                        imageButton.setImageResource(R.drawable.inline_audio_pause);
                        imageButton.setContentDescription(r1.A00.getString(R.string.pause));
                    }
                }
            } catch (IOException e) {
                StringBuilder sb = new StringBuilder("Error resuming playback after seek ");
                sb.append(e.getMessage());
                Log.e(sb.toString());
            }
        }
    }

    public static /* synthetic */ void A01(AbstractC14670lq r6, int i, boolean z) {
        AbstractC28651Ol r0 = r6.A0N;
        if (r0 != null && z) {
            r6.A08 = i;
            if (i == 0 && !r0.A0D()) {
                i = r6.A0N.A03();
            }
            C14680lr r02 = r6.A1G;
            r02.A0A.setText(C38131nZ.A04(r02.A0E, ((long) i) / 1000));
            if (!r6.A0N.A0D()) {
                r6.A0A();
            }
        }
    }

    public void A02() {
        A0R(false);
        A0T(false, false);
        this.A0a.removeCallbacks(this.A1Q);
        if (this.A0N != null) {
            A04();
            this.A0N.A06();
            this.A0N = null;
        }
        this.A1N.A04(this.A1M);
    }

    public void A03() {
        if (A0V()) {
            C14690ls r2 = this.A0P;
            if (r2 == null) {
                Log.e("VoiceNoteRecordingUI/pauseRecording/voice recorder ir null");
                return;
            }
            try {
                try {
                    OpusRecorder opusRecorder = r2.A09;
                    opusRecorder.pause();
                    r2.A01 = opusRecorder.getPageNumber();
                    try {
                        this.A0P.A01();
                    } catch (IOException e) {
                        Log.e("VoiceNoteRecordingUI/pauseRecording/stop waveform creation failed", e);
                    }
                    this.A1J.A03 = true;
                    long elapsedRealtime = (SystemClock.elapsedRealtime() - this.A0B) + this.A0A;
                    this.A0A = elapsedRealtime;
                    if (elapsedRealtime < 1000) {
                        A0T(false, false);
                        return;
                    }
                    A1b.play(A1W, 1.0f, 1.0f, 0, 0, 1.0f);
                    A0F();
                    HandlerC28161Kx r0 = this.A0M;
                    if (r0 != null) {
                        r0.A00();
                        this.A0M = null;
                    }
                    this.A19.A00();
                    if (this.A1T) {
                        this.A0j.setRequestedOrientation(-1);
                    }
                    this.A0b.removeCallbacksAndMessages(null);
                    C14680lr r3 = this.A1G;
                    r3.A0H.clear();
                    C14690ls r02 = this.A0P;
                    A0M(r02.A0A, r02.A0B, false, true);
                    this.A12.A00 = false;
                    AnonymousClass19A r1 = this.A1H;
                    Log.i("voicenote/voicenotestopped");
                    for (AnonymousClass2SY r12 : r1.A01()) {
                        if (r12 instanceof AnonymousClass39I) {
                            ((AnonymousClass39I) r12).A00.invalidateOptionsMenu();
                        }
                    }
                    VoiceVisualizer voiceVisualizer = r3.A0C;
                    voiceVisualizer.A06 = false;
                    voiceVisualizer.A02 = 0;
                    voiceVisualizer.A03 = 166;
                    ActivityC000900k r22 = this.A0j;
                    AnonymousClass23N.A00(r22, this.A0y, r22.getString(R.string.voice_note_draft_pause_announcement));
                } catch (IOException e2) {
                    Log.e("VoiceNoteRecordingUI/pauseRecording/pause failed", e2);
                    try {
                        this.A0P.A01();
                    } catch (IOException e3) {
                        Log.e("VoiceNoteRecordingUI/pauseRecording/stop waveform creation failed", e3);
                    }
                }
            } catch (Throwable th) {
                try {
                    this.A0P.A01();
                    throw th;
                } catch (IOException e4) {
                    Log.e("VoiceNoteRecordingUI/pauseRecording/stop waveform creation failed", e4);
                    throw th;
                }
            }
        }
    }

    public void A04() {
        AbstractC28651Ol r0 = this.A0N;
        if (r0 != null && r0.A0D()) {
            this.A0N.A04();
        }
        A0A();
        this.A0a.removeCallbacks(this.A1Q);
    }

    public void A05() {
        if (this.A18.A03(this.A0j, this.A0o, this.A0J)) {
            this.A0w.A04();
            this.A19.A01();
            if (this.A0P != null) {
                A09();
                return;
            }
            File file = this.A0Q;
            File file2 = this.A0R;
            C111735An r7 = new AnonymousClass5V6() { // from class: X.5An
                @Override // X.AnonymousClass5V6
                public final void A6p() {
                    AbstractC14670lq.this.A09();
                }
            };
            if (file != null) {
                this.A1D.Ab2(new RunnableBRunnable0Shape0S0400000_I0(this, file, file2, r7, 33));
            }
        }
    }

    public void A06() {
        SharedPreferences sharedPreferences;
        SharedPreferences.Editor edit;
        String str;
        AnonymousClass2SX r7 = this.A1J;
        AbstractC14640lm r2 = r7.A06.A00.A0J;
        if (r2 != null) {
            if (C15380n4.A0F(r2)) {
                sharedPreferences = r7.A04.A00;
                edit = sharedPreferences.edit();
                str = "ptt_record_broadcast";
            } else {
                boolean A0J = C15380n4.A0J(r2);
                sharedPreferences = r7.A04.A00;
                edit = sharedPreferences.edit();
                if (A0J) {
                    str = "ptt_record_group";
                } else {
                    str = "ptt_record_individual";
                }
            }
            edit.putLong(str, sharedPreferences.getLong(str, 0) + 1).apply();
        }
        r7.A03 = false;
        r7.A00 = 0;
        r7.A01 = 0;
        r7.A02 = 0;
        AnonymousClass1AL r22 = this.A18;
        AbstractC13860kS r14 = this.A0o;
        ActivityC000900k r10 = this.A0j;
        if (!r22.A03(r10, r14, this.A0J)) {
            return;
        }
        if (this.A0P != null) {
            Log.e("voicenote/startvoicenote/inprogress");
            return;
        }
        View view = this.A0c;
        view.clearFocus();
        view.setFocusable(false);
        this.A0w.A04();
        if (this.A1T) {
            A07();
        }
        PowerManager.WakeLock wakeLock = this.A0G;
        if (wakeLock != null) {
            wakeLock.acquire();
        }
        this.A19.A01();
        A0O(false);
        A0F();
        float f = 1.0f;
        this.A09 = A1b.play(A1a, 1.0f, 1.0f, 0, 0, 1.0f);
        AnonymousClass4SB r72 = this.A1K;
        TextView textView = r72.A00;
        AnonymousClass018 r5 = r72.A03;
        textView.setText(C38131nZ.A04(r5, 0));
        textView.setVisibility(0);
        r72.A01.setText(C38131nZ.A04(r5, 0));
        this.A06 = 0;
        this.A07 = 0;
        C14680lr r9 = this.A1G;
        r9.A0H.clear();
        this.A04 = 0;
        C28151Kw r73 = this.A0l;
        r73.A00.set(Double.doubleToRawLongBits(1.0d));
        View view2 = this.A0f;
        View findViewById = view2.findViewById(R.id.voice_note_pulse);
        findViewById.setVisibility(0);
        if (this.A0F == null) {
            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(findViewById, "alpha", 0.0f, 1.0f);
            this.A0F = ofFloat;
            ofFloat.setInterpolator(new C95754eI(r73));
            this.A0F.setDuration(500L);
            this.A0F.setRepeatMode(2);
            this.A0F.setRepeatCount(-1);
        }
        this.A0F.start();
        ImageView imageView = this.A0g;
        imageView.setVisibility(0);
        float width = ((float) imageView.getWidth()) / 5.5f;
        AnonymousClass018 r15 = this.A11;
        if (r15.A04().A06) {
            width = -width;
        }
        imageView.setTranslationX(width);
        imageView.setTranslationY((float) (imageView.getHeight() / 4));
        imageView.setScaleX(0.5f);
        imageView.setScaleY(0.5f);
        imageView.requestFocus();
        AnonymousClass4YC r74 = this.A0k;
        r74.A06.clear();
        r74.A03(new C55432iS(this, 0));
        r74.A02(1.0d);
        view2.findViewById(R.id.buttons).setVisibility(4);
        view2.findViewById(R.id.emoji_picker_btn).setVisibility(0);
        View findViewById2 = view2.findViewById(R.id.voice_cancel_trashcan);
        findViewById2.clearAnimation();
        findViewById2.setVisibility(8);
        View findViewById3 = view2.findViewById(R.id.voice_cancel_animation);
        findViewById3.clearAnimation();
        findViewById3.setVisibility(8);
        View findViewById4 = view2.findViewById(R.id.voice_cancel_trashcan_lid);
        findViewById4.clearAnimation();
        findViewById4.setVisibility(8);
        view2.findViewById(R.id.voice_note_slide_to_cancel_layout).setVisibility(0);
        View findViewById5 = view2.findViewById(R.id.voice_note_slide_to_cancel_animation);
        findViewById5.setVisibility(0);
        findViewById5.post(new RunnableBRunnable0Shape8S0200000_I0_8(this, 40, findViewById5));
        View findViewById6 = view2.findViewById(R.id.voice_note_layout);
        findViewById6.setVisibility(0);
        findViewById6.setClickable(true);
        AnonymousClass028.A0a(findViewById6, 2);
        if (!(!r15.A04().A06)) {
            f = -1.0f;
        }
        TranslateAnimation translateAnimation = new TranslateAnimation(1, f, 1, 0.0f, 1, 0.0f, 1, 0.0f);
        translateAnimation.setDuration(160);
        findViewById6.startAnimation(translateAnimation);
        View findViewById7 = view2.findViewById(R.id.input_layout_content);
        findViewById7.clearAnimation();
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(160);
        alphaAnimation.setFillBefore(true);
        alphaAnimation.setFillAfter(true);
        findViewById7.startAnimation(alphaAnimation);
        view2.findViewById(R.id.voice_recorder_decor).setVisibility(0);
        findViewById7.setFocusable(false);
        AnonymousClass028.A0a(findViewById7, 4);
        File A0M = this.A0p.A0M(C22200yh.A0L());
        C14830m7 r13 = this.A0z;
        C14850m9 r12 = this.A13;
        AudioRecordFactory audioRecordFactory = this.A0s;
        OpusRecorderFactory opusRecorderFactory = this.A0t;
        String absolutePath = A0M.getAbsolutePath();
        this.A0P = new C14690ls(AnonymousClass028.A0D(view2, R.id.voice_note_flashing_recording_view), audioRecordFactory, opusRecorderFactory, (VoiceVisualizer) AnonymousClass028.A0D(view2, R.id.voice_note_draft_audio_visualizer), r13, r12, absolutePath);
        this.A12.A00 = true;
        this.A0C = SystemClock.elapsedRealtime();
        this.A0B = SystemClock.elapsedRealtime();
        this.A0A = 0;
        try {
            this.A0P.A09.prepare();
        } catch (Exception unused) {
            AnonymousClass1AL.A00(this.A0P, 0, false);
            A0T(false, false);
            r14.Ado(R.string.error_setup_recorder);
        }
        Runnable runnable = this.A1P;
        view2.removeCallbacks(runnable);
        view2.postDelayed(runnable, 160);
        this.A0V = false;
        this.A0S = false;
        if (this.A1S) {
            view2.findViewById(R.id.voice_note_lock_container).setVisibility(0);
            if (this.A0O == null) {
                ViewGroup viewGroup = (ViewGroup) view2.findViewById(R.id.voice_note_lock_container);
                AnonymousClass2CF r3 = new AnonymousClass2CF(r10);
                this.A0O = r3;
                r3.setVisibility(4);
                FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
                int i = 83;
                if (!r15.A04().A06) {
                    i = 85;
                }
                layoutParams.gravity = i;
                viewGroup.addView(this.A0O, layoutParams);
            }
            this.A0T = false;
            this.A0W = false;
            this.A0Z.post(this.A1O);
            r9.A01(R.drawable.ic_pause_draft_preview);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0022, code lost:
        if (r3 == 1) goto L_0x0024;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A07() {
        /*
            r5 = this;
            X.00k r4 = r5.A0j
            android.view.WindowManager r0 = r4.getWindowManager()
            android.view.Display r0 = r0.getDefaultDisplay()
            int r3 = r0.getRotation()
            android.content.res.Resources r0 = r4.getResources()
            android.content.res.Configuration r0 = r0.getConfiguration()
            int r2 = r0.orientation
            r1 = 1
            if (r2 == r1) goto L_0x0029
            r0 = 2
            if (r2 != r0) goto L_0x0028
            if (r3 == 0) goto L_0x0024
            r0 = 8
            if (r3 != r1) goto L_0x0025
        L_0x0024:
            r0 = 0
        L_0x0025:
            r4.setRequestedOrientation(r0)
        L_0x0028:
            return
        L_0x0029:
            if (r3 == 0) goto L_0x0030
            if (r3 == r1) goto L_0x0030
            r0 = 9
            goto L_0x0025
        L_0x0030:
            r4.setRequestedOrientation(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC14670lq.A07():void");
    }

    public final void A08() {
        View view = this.A0f;
        view.findViewById(R.id.voice_note_pulse).setVisibility(4);
        ObjectAnimator objectAnimator = this.A0F;
        if (objectAnimator != null) {
            objectAnimator.end();
        }
        int i = A1Z;
        if (i != 0) {
            A1b.play(i, 1.0f, 1.0f, 0, 0, 1.0f);
        }
        View findViewById = view.findViewById(R.id.voice_cancel_trashcan);
        findViewById.setVisibility(0);
        TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
        translateAnimation.setDuration(213);
        translateAnimation.setStartOffset(640);
        translateAnimation.setFillBefore(true);
        findViewById.startAnimation(translateAnimation);
        View findViewById2 = view.findViewById(R.id.voice_cancel_animation);
        findViewById2.setVisibility(0);
        AnimationSet animationSet = new AnimationSet(true);
        animationSet.setInterpolator(new DecelerateInterpolator(1.1f));
        TranslateAnimation translateAnimation2 = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, -3.0f);
        translateAnimation2.setDuration(640);
        translateAnimation2.setRepeatMode(2);
        translateAnimation2.setRepeatCount(1);
        RotateAnimation rotateAnimation = new RotateAnimation(0.0f, 360.0f, 1, 0.5f, 1, 0.5f);
        rotateAnimation.setDuration(640);
        ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 0.61f, 1.0f, 0.61f, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setDuration(320);
        scaleAnimation.setStartOffset(960);
        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(rotateAnimation);
        animationSet.addAnimation(translateAnimation2);
        View findViewById3 = view.findViewById(R.id.voice_cancel_trashcan_lid);
        AnimationSet animationSet2 = new AnimationSet(true);
        TranslateAnimation translateAnimation3 = new TranslateAnimation(1, 0.0f, 1, -0.3f, 1, 0.0f, 1, 0.0f);
        translateAnimation3.setDuration(160);
        translateAnimation3.setStartOffset(746);
        translateAnimation3.setFillAfter(true);
        RotateAnimation rotateAnimation2 = new RotateAnimation(0.0f, -60.0f, 1, 0.5f, 1, 0.5f);
        rotateAnimation2.setDuration(160);
        rotateAnimation2.setStartOffset(746);
        rotateAnimation2.setFillAfter(true);
        animationSet2.addAnimation(rotateAnimation2);
        animationSet2.addAnimation(translateAnimation3);
        animationSet2.setFillAfter(true);
        findViewById3.setVisibility(0);
        findViewById3.startAnimation(animationSet2);
        View findViewById4 = view.findViewById(R.id.emoji_picker_btn);
        findViewById4.setVisibility(4);
        findViewById2.startAnimation(animationSet);
        animationSet.setAnimationListener(new C58112oA(findViewById2, findViewById, findViewById3, findViewById4, this));
    }

    public final void A09() {
        C14690ls r0 = this.A0P;
        if (r0 == null) {
            Log.e("VoiceNoteRecordingUI/resumeVoiceNoteRecording/resume voice recorder is null");
            return;
        }
        this.A0W = true;
        C14680lr r3 = this.A1G;
        r3.A0F.Ab2(new RunnableBRunnable0Shape8S0200000_I0_8(r3, 38, r0.A0B));
        this.A0B = SystemClock.elapsedRealtime();
        AbstractC28651Ol r02 = this.A0N;
        if (r02 != null && r02.A0D()) {
            this.A0N.A09();
        }
        try {
            A0D();
            if (this.A1T) {
                A07();
            }
            this.A12.A00 = true;
            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
            alphaAnimation.setDuration(150);
            alphaAnimation.setAnimationListener(new C58052o3(r3));
            r3.A06.startAnimation(alphaAnimation);
            Animation A00 = C88154Em.A00(false);
            Animation A002 = C88154Em.A00(true);
            A00.setAnimationListener(new C83533xO(A002, r3, false));
            A002.setAnimationListener(new C83333x4(r3));
            r3.A09.startAnimation(A00);
            C28151Kw r32 = this.A0m;
            C14690ls r2 = this.A0P;
            HandlerThread handlerThread = new HandlerThread("PushToTalkVoiceVisualizerHandler");
            handlerThread.start();
            HandlerC28161Kx r1 = new HandlerC28161Kx(handlerThread, r32, r2);
            this.A0M = r1;
            synchronized (r1) {
                r1.sendEmptyMessage(0);
            }
            this.A0b.sendEmptyMessage(0);
            ActivityC000900k r22 = this.A0j;
            AnonymousClass23N.A00(r22, this.A0y, r22.getString(R.string.voice_note_draft_resume_announcement));
        } catch (IOException e) {
            Log.e("VoiceNoteRecordingUI/resumeRecording/resume failed", e);
            A0T(false, false);
            this.A0o.Ado(R.string.error_setup_recorder);
        }
    }

    public final void A0A() {
        C14680lr r0 = this.A1G;
        ImageButton imageButton = r0.A08;
        Context context = r0.A00;
        imageButton.setImageDrawable(AnonymousClass00T.A04(context, R.drawable.inline_audio_play));
        imageButton.setContentDescription(context.getString(R.string.play));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0065, code lost:
        if (r9.A0N.A0D() != false) goto L_0x0067;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0B() {
        /*
            r9 = this;
            X.1Ol r0 = r9.A0N
            if (r0 == 0) goto L_0x0048
            X.0lr r4 = r9.A1G
            com.whatsapp.voicerecorder.VoiceNoteSeekBar r3 = r4.A0G
            int r1 = r3.getVisibility()
            r0 = 0
            if (r1 != 0) goto L_0x0010
            r0 = 1
        L_0x0010:
            r8 = 0
            r2 = 1
            if (r0 == 0) goto L_0x004f
            int r0 = r3.getProgress()
            if (r0 != 0) goto L_0x0049
            X.1Ol r0 = r9.A0N
            boolean r0 = r0.A0D()
            if (r0 != 0) goto L_0x0049
            r1 = 1
            int r0 = r3.getMax()
        L_0x0027:
            X.00k r7 = r9.A0j
            r6 = 2131892854(0x7f121a76, float:1.9420468E38)
            if (r1 == 0) goto L_0x0031
            r6 = 2131892470(0x7f1218f6, float:1.941969E38)
        L_0x0031:
            java.lang.Object[] r5 = new java.lang.Object[r2]
            X.018 r2 = r9.A11
            long r0 = (long) r0
            java.lang.String r0 = X.C38131nZ.A06(r2, r0)
            r5[r8] = r0
            java.lang.String r1 = r7.getString(r6, r5)
            r3.setContentDescription(r1)
            com.whatsapp.conversation.waveforms.VoiceVisualizer r0 = r4.A0D
            r0.setContentDescription(r1)
        L_0x0048:
            return
        L_0x0049:
            r1 = 0
            int r0 = r3.getProgress()
            goto L_0x0027
        L_0x004f:
            com.whatsapp.conversation.waveforms.VoiceVisualizer r1 = r4.A0D
            int r0 = r1.getVisibility()
            if (r0 != 0) goto L_0x0076
            float r1 = r1.A00
            r0 = 0
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 != 0) goto L_0x0067
            X.1Ol r0 = r9.A0N
            boolean r0 = r0.A0D()
            r1 = 1
            if (r0 == 0) goto L_0x0068
        L_0x0067:
            r1 = 0
        L_0x0068:
            X.1Ol r0 = r9.A0N
            if (r1 == 0) goto L_0x0071
            int r0 = r0.A03()
            goto L_0x0027
        L_0x0071:
            int r0 = r0.A02()
            goto L_0x0027
        L_0x0076:
            r1 = 1
            r0 = 0
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC14670lq.A0B():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001c, code lost:
        if (X.C42971wC.A0C(r5.A0y, r5.A16, ((com.whatsapp.mentions.MentionableEntry) r0).getStringText()) == false) goto L_0x001e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0C() {
        /*
            r5 = this;
            android.view.View r1 = r5.A0f
            r0 = 2131362422(0x7f0a0276, float:1.8344624E38)
            android.view.View r4 = r1.findViewById(r0)
            android.view.View r0 = r5.A0c
            if (r0 == 0) goto L_0x001e
            X.01d r2 = r5.A0y
            X.0pM r1 = r5.A16
            com.whatsapp.mentions.MentionableEntry r0 = (com.whatsapp.mentions.MentionableEntry) r0
            java.lang.String r0 = r0.getStringText()
            boolean r0 = X.C42971wC.A0C(r2, r1, r0)
            r3 = 1
            if (r0 != 0) goto L_0x001f
        L_0x001e:
            r3 = 0
        L_0x001f:
            r2 = 8
            r1 = 0
            r0 = 2131365828(0x7f0a0fc4, float:1.8351532E38)
            android.view.View r0 = r4.findViewById(r0)
            if (r3 == 0) goto L_0x0041
            r0.setVisibility(r1)
            r0 = 2131366690(0x7f0a1322, float:1.835328E38)
            android.view.View r0 = r4.findViewById(r0)
            r0.setVisibility(r2)
        L_0x0038:
            r4.setVisibility(r1)
            android.view.View r0 = r5.A0d
            r0.requestFocus()
            return
        L_0x0041:
            r0.setVisibility(r2)
            r0 = 2131366690(0x7f0a1322, float:1.835328E38)
            android.view.View r0 = r4.findViewById(r0)
            r0.setVisibility(r1)
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC14670lq.A0C():void");
    }

    public final void A0D() {
        C14690ls r1 = this.A0P;
        if (r1 != null) {
            r1.A09.start();
            AudioRecord audioRecord = r1.A05;
            if (audioRecord.getState() == 1) {
                audioRecord.startRecording();
            }
            AnonymousClass19A r12 = this.A1H;
            Log.i("voicenote/voicenotestarted");
            for (AnonymousClass2SY r13 : r12.A01()) {
                if (r13 instanceof AnonymousClass39I) {
                    Conversation conversation = ((AnonymousClass39I) r13).A00;
                    conversation.invalidateOptionsMenu();
                    AbstractC35401hl r14 = conversation.A3t;
                    if (r14 != null && r14.ADU()) {
                        r14.AYz();
                    }
                }
            }
        }
    }

    public final void A0E() {
        if (this.A1S) {
            this.A0Z.removeCallbacks(this.A1O);
            View view = this.A0f;
            if (view != null && view.getKeepScreenOn()) {
                view.setKeepScreenOn(false);
            }
            if (this.A0U) {
                this.A0U = false;
                this.A0v.A04(this.A0u);
            }
        }
    }

    public final void A0F() {
        try {
            if (Settings.System.getInt(this.A0j.getContentResolver(), "haptic_feedback_enabled") != 0) {
                Vibrator A0K = this.A0y.A0K();
                AnonymousClass009.A05(A0K);
                A0K.vibrate(75);
            }
        } catch (Settings.SettingNotFoundException e) {
            Log.e("voicenote/vibrate", e);
        }
    }

    public final void A0G(float f, float f2, int i, int i2) {
        ActivityC000900k r1 = this.A0j;
        String string = r1.getString(i2);
        if (this.A0P == null) {
            AnonymousClass23N.A00(r1, this.A0y, string);
        }
        AnonymousClass4SB r0 = this.A1K;
        TextView textView = r0.A02;
        textView.setBackground(new AnonymousClass2GF(r1.getResources().getDrawable(i), r0.A03));
        textView.setText(string);
        textView.setTranslationY(f);
        textView.setTranslationX(f2);
        textView.setVisibility(0);
        textView.setAlpha(0.0f);
        textView.animate().alpha(1.0f).setDuration(320).start();
        C14900mE r3 = this.A0q;
        Runnable runnable = this.A1R;
        r3.A0G(runnable);
        r3.A0J(runnable, 3500);
    }

    public final void A0H(long j, boolean z) {
        try {
            try {
                C14690ls r2 = this.A0P;
                OpusRecorder opusRecorder = r2.A09;
                opusRecorder.stop();
                r2.A01 = opusRecorder.getPageNumber();
            } catch (Exception e) {
                if (!z || j < 1000) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("voicenote/stopandreleasevoicerecorder/stop ");
                    sb.append(e.toString());
                    Log.i(sb.toString());
                } else {
                    Log.e("voicenote/stopandreleasevoicerecorder/stop ", e);
                }
            }
            try {
                C14690ls r1 = this.A0P;
                r1.A09.close();
                r1.A05.release();
            } catch (Exception e2) {
                Log.e("voicenote/stopandreleasevoicerecorder/release", e2);
            }
            AnonymousClass19A r12 = this.A1H;
            Log.i("voicenote/voicenotestopped");
            for (AnonymousClass2SY r13 : r12.A01()) {
                if (r13 instanceof AnonymousClass39I) {
                    ((AnonymousClass39I) r13).A00.invalidateOptionsMenu();
                }
            }
        } finally {
            this.A18.A02(this.A0P, j, z);
        }
    }

    public final void A0I(File file) {
        if (file != null) {
            this.A1D.Ab2(new RunnableBRunnable0Shape13S0100000_I0_13(file, 29));
        }
    }

    public final void A0J(File file) {
        AnonymousClass009.A05(file);
        try {
            AbstractC28651Ol r0 = this.A0N;
            if (r0 != null) {
                r0.A06();
            }
            AbstractC28651Ol A00 = AbstractC28651Ol.A00(null, null, file, 3);
            this.A0N = A00;
            A00.A05();
        } catch (IOException e) {
            this.A0N = null;
            Log.e("voicenoterecordingui/prepareVoiceNoteDraftPlayer/ error creating audio player for voice note preview ", e);
        }
    }

    public final void A0K(File file, int i, boolean z) {
        C32701cb r1;
        StringBuilder sb = new StringBuilder("voicenote/onrecordingstopped ");
        sb.append(z);
        Log.i(sb.toString());
        this.A0K.A01 = true;
        if (z) {
            C16150oX r11 = new C16150oX();
            r11.A0L = true;
            File file2 = this.A0K.A02.A06;
            AnonymousClass009.A05(file2);
            r11.A0F = file2;
            C20320vZ r10 = this.A17;
            AbstractC14640lm r12 = this.A0J;
            AnonymousClass009.A05(r12);
            long j = this.A0D;
            String name = file2.getName();
            long length = r11.A0F.length();
            AbstractC15340mz r8 = this.A0L;
            AbstractC15340mz A01 = r10.A01(r10.A07.A02(r12, true), (byte) 2, j);
            if (A01 instanceof AbstractC16130oV) {
                AbstractC16130oV r3 = (AbstractC16130oV) A01;
                r3.A02 = r11;
                ((AbstractC15340mz) r3).A02 = 1;
                r3.A0Y(0);
                r3.A07 = name;
                r3.A01 = length;
                ((AbstractC15340mz) r3).A08 = 1;
                r10.A04(r3, r8);
                C30421Xi r32 = (C30421Xi) r3;
                ((AbstractC16130oV) r32).A00 = i;
                AnonymousClass2KT r2 = this.A0H;
                if (!(r2 == null || (r1 = this.A0I) == null)) {
                    r32.A0N = this.A0x.A00(r2, r1);
                    r32.A0T(EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH);
                }
                this.A18.A01(this.A0K.A00, r32, file);
            } else {
                StringBuilder sb2 = new StringBuilder("FMessageFactory/newFMessageMedia/wrong message type; mediaWaType=");
                sb2.append(2);
                throw new IllegalArgumentException(sb2.toString());
            }
        } else {
            AnonymousClass109 r13 = this.A15;
            C37991nL r22 = this.A0K;
            Log.i("app/mediajobmanager/cancelVoiceNoteUpload");
            r13.A0E.A06(r22.A00);
        }
        this.A0K = null;
    }

    public void A0L(File file, File file2, boolean z) {
        long j;
        C32701cb r3;
        AnonymousClass009.A05(file);
        AnonymousClass009.A05(this.A0J);
        AnonymousClass2SX r32 = this.A1J;
        long length = file.length();
        AbstractC28651Ol r2 = this.A0N;
        if (r2 != null) {
            j = (long) r2.A03();
        } else {
            j = -1;
        }
        r32.A02(length, j, this.A0W);
        File A0F = C22200yh.A0F(this.A0p, this.A16, C14370lK.A0I, file, 1);
        if (!file.renameTo(A0F)) {
            StringBuilder sb = new StringBuilder("voicenote/sendvoicenotefile/failed to rename ");
            sb.append(file);
            sb.append(" to ");
            sb.append(A0F);
            Log.e(sb.toString());
            A0F = file;
        }
        C16150oX r7 = new C16150oX();
        r7.A0F = A0F;
        AbstractC16130oV A03 = this.A14.A03(null, r7, null, this.A0J, this.A0L, null, null, null, null, (byte) 2, 1, 0, z);
        AnonymousClass2KT r4 = this.A0H;
        if (!(r4 == null || (r3 = this.A0I) == null)) {
            A03.A0N = this.A0x.A00(r4, r3);
            A03.A0T(EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH);
        }
        if (A03.A00 == 0) {
            A03.A00 = C22200yh.A07(file);
        }
        this.A18.A01(null, (C30421Xi) A03, file2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0041, code lost:
        if (r0.A09.isRecording() != false) goto L_0x0043;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0M(java.io.File r8, java.io.File r9, boolean r10, boolean r11) {
        /*
            r7 = this;
            r7.A0J(r8)
            X.1Ol r0 = r7.A0N
            if (r0 == 0) goto L_0x00dd
            int r0 = r0.A03()
            long r0 = (long) r0
            r7.A0A = r0
            r7.A0A()
            X.0lr r2 = r7.A1G
            X.2SW r4 = new X.2SW
            r4.<init>(r7)
            android.widget.ImageButton r3 = r2.A08
            r1 = 23
            com.facebook.redex.ViewOnClickCListenerShape5S0100000_I0_5 r0 = new com.facebook.redex.ViewOnClickCListenerShape5S0100000_I0_5
            r0.<init>(r4, r1)
            r3.setOnClickListener(r0)
            long r0 = r7.A0A
            android.widget.TextView r6 = r2.A0A
            X.018 r5 = r2.A0E
            r3 = 1000(0x3e8, double:4.94E-321)
            long r0 = r0 / r3
            java.lang.String r0 = X.C38131nZ.A04(r5, r0)
            r6.setText(r0)
            X.1Ol r4 = r7.A0N
            X.0ls r0 = r7.A0P
            if (r0 == 0) goto L_0x0043
            com.whatsapp.util.OpusRecorder r0 = r0.A09
            boolean r0 = r0.isRecording()
            r3 = 1
            if (r0 == 0) goto L_0x0044
        L_0x0043:
            r3 = 0
        L_0x0044:
            java.util.List r1 = r2.A0H
            boolean r0 = r1.isEmpty()
            if (r0 != 0) goto L_0x00de
            r2.A02(r4, r1)
            r2.A03(r11, r3)
        L_0x0052:
            r7.A0B()
            android.view.View r1 = r7.A0f
            r0 = 2131366708(0x7f0a1334, float:1.8353317E38)
            android.view.View r0 = r1.findViewById(r0)
            r5 = 8
            r0.setVisibility(r5)
            r0 = 2131363959(0x7f0a0877, float:1.8347742E38)
            android.view.View r0 = r1.findViewById(r0)
            r4 = 4
            r0.setVisibility(r4)
            r0 = 2131362422(0x7f0a0276, float:1.8344624E38)
            android.view.View r0 = r1.findViewById(r0)
            r3 = 0
            r0.setVisibility(r3)
            r0 = 2131366690(0x7f0a1322, float:1.835328E38)
            android.view.View r0 = r1.findViewById(r0)
            r0.setVisibility(r5)
            r0 = 2131365828(0x7f0a0fc4, float:1.8351532E38)
            android.view.View r1 = r1.findViewById(r0)
            X.3hq r0 = new X.3hq
            r0.<init>(r7)
            X.AnonymousClass028.A0g(r1, r0)
            android.widget.ImageView r0 = r7.A0g
            r0.setVisibility(r5)
            X.2CF r0 = r7.A0O
            if (r0 == 0) goto L_0x009e
            r0.setVisibility(r4)
        L_0x009e:
            android.view.ViewGroup r1 = r2.A05
            r1.setVisibility(r3)
            r0 = 1
            r1.setClickable(r0)
            X.5Ai r4 = new X.5Ai
            r4.<init>(r8)
            android.widget.ImageButton r3 = r2.A07
            r1 = 22
            com.facebook.redex.ViewOnClickCListenerShape5S0100000_I0_5 r0 = new com.facebook.redex.ViewOnClickCListenerShape5S0100000_I0_5
            r0.<init>(r4, r1)
            r3.setOnClickListener(r0)
            X.4pM r1 = new X.4pM
            r1.<init>(r7)
            com.whatsapp.voicerecorder.VoiceNoteSeekBar r0 = r2.A0G
            r0.setOnSeekBarChangeListener(r1)
            X.55R r1 = new X.55R
            r1.<init>(r7)
            com.whatsapp.conversation.waveforms.VoiceVisualizer r0 = r2.A0D
            r0.A04 = r1
            if (r10 == 0) goto L_0x00d9
            X.0lR r2 = r7.A1D
            r1 = 26
            com.facebook.redex.RunnableBRunnable0Shape13S0100000_I0_13 r0 = new com.facebook.redex.RunnableBRunnable0Shape13S0100000_I0_13
            r0.<init>(r7, r1)
            r2.Ab2(r0)
        L_0x00d9:
            r7.A0Q = r8
            r7.A0R = r9
        L_0x00dd:
            return
        L_0x00de:
            r2.A03(r11, r3)
            android.view.ViewGroup r0 = r2.A06
            android.view.ViewTreeObserver r1 = r0.getViewTreeObserver()
            X.3Nc r0 = new X.3Nc
            r0.<init>(r4, r2, r9)
            r1.addOnGlobalLayoutListener(r0)
            goto L_0x0052
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC14670lq.A0M(java.io.File, java.io.File, boolean, boolean):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x005b, code lost:
        if (X.C42971wC.A0C(r18.A0y, r18.A16, ((com.whatsapp.mentions.MentionableEntry) r0).getStringText()) == false) goto L_0x005d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0N(boolean r19) {
        /*
        // Method dump skipped, instructions count: 233
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC14670lq.A0N(boolean):void");
    }

    public void A0O(boolean z) {
        TextView textView = this.A1K.A02;
        if (textView.getVisibility() == 0) {
            this.A0q.A0G(this.A1R);
            if (z) {
                textView.setVisibility(8);
                AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
                alphaAnimation.setDuration(320);
                textView.startAnimation(alphaAnimation);
                return;
            }
            textView.clearAnimation();
            textView.setVisibility(8);
        }
    }

    public void A0P(boolean z) {
        SharedPreferences sharedPreferences;
        SharedPreferences.Editor edit;
        String str;
        this.A0W = true;
        AnonymousClass2SX r6 = this.A1J;
        AbstractC14640lm r1 = r6.A06.A00.A0J;
        if (r1 != null) {
            if (C15380n4.A0F(r1)) {
                sharedPreferences = r6.A04.A00;
                edit = sharedPreferences.edit();
                str = "ptt_lock_broadcast";
            } else {
                boolean A0J = C15380n4.A0J(r1);
                sharedPreferences = r6.A04.A00;
                edit = sharedPreferences.edit();
                if (A0J) {
                    str = "ptt_lock_group";
                } else {
                    str = "ptt_lock_individual";
                }
            }
            edit.putLong(str, sharedPreferences.getLong(str, 0) + 1).apply();
        }
        r6.A05.A0G.A07(new AnonymousClass42n());
        View view = this.A0f;
        view.findViewById(R.id.voice_note_btn).setVisibility(8);
        view.findViewById(R.id.buttons).setVisibility(4);
        C18280sC r7 = this.A0v;
        if (r7.A00.A00() >= 16.0d) {
            if (!view.getKeepScreenOn()) {
                view.setKeepScreenOn(true);
            }
            if (!this.A0U) {
                this.A0U = true;
                r7.A03(this.A0u);
            }
        }
        C14680lr r62 = this.A1G;
        C111695Aj r72 = new AnonymousClass2SZ(z) { // from class: X.5Aj
            public final /* synthetic */ boolean A01;

            {
                this.A01 = r2;
            }

            @Override // X.AnonymousClass2SZ
            public final void APD() {
                AbstractC14670lq.this.A0U(false, this.A01, false, false);
            }
        };
        C111745Ao r5 = new AnonymousClass2SV() { // from class: X.5Ao
            @Override // X.AnonymousClass2SV
            public final void AYR() {
                AbstractC14670lq r12 = AbstractC14670lq.this;
                C14690ls r0 = r12.A0P;
                if (r0 == null || !r0.A09.isRecording()) {
                    r12.A05();
                    return;
                }
                r12.A1J.A00();
                r12.A03();
            }
        };
        r62.A07.setOnClickListener(new ViewOnClickCListenerShape5S0100000_I0_5(r72, 22));
        r62.A09.setOnClickListener(new ViewOnClickCListenerShape5S0100000_I0_5(r5, 24));
        r62.A05.setVisibility(0);
        r62.A00();
        VoiceVisualizer voiceVisualizer = r62.A0C;
        voiceVisualizer.setEnabled(true);
        if (r62.A0I) {
            voiceVisualizer.setVisibility(0);
        }
        r62.A04.setVisibility(0);
        r62.A06.setVisibility(8);
        AnonymousClass19A r12 = this.A1H;
        Log.i("voicenote/notifyVoiceNoteLocked");
        AnonymousClass009.A01();
        for (AnonymousClass2SY r13 : r12.A01()) {
            if (r13 instanceof AnonymousClass39I) {
                Conversation conversation = ((AnonymousClass39I) r13).A00;
                AbstractC15340mz r0 = conversation.A20.A07;
                if (r0 != null) {
                    conversation.AUy(r0, false);
                }
            }
        }
        View findViewById = view.findViewById(R.id.voice_note_slide_to_cancel_layout);
        findViewById.animate().setDuration(200).alpha(0.0f).setListener(new C72623eu(findViewById, this)).start();
        AnonymousClass2CF r11 = this.A0O;
        if (r11 != null) {
            RunnableBRunnable0Shape13S0100000_I0_13 runnableBRunnable0Shape13S0100000_I0_13 = new RunnableBRunnable0Shape13S0100000_I0_13(this, 28);
            r11.setPivotX((float) (r11.getWidth() / 2));
            r11.setPivotY((float) (r11.A0I / 2));
            AnimatorSet animatorSet = new AnimatorSet();
            ValueAnimator ofFloat = ValueAnimator.ofFloat(1.0f, 1.3f);
            ofFloat.setDuration(250L);
            ofFloat.setRepeatMode(2);
            ofFloat.setRepeatCount(2);
            ofFloat.setInterpolator(new AccelerateDecelerateInterpolator());
            ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: X.4ea
                @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                    AnonymousClass2CF r14 = AnonymousClass2CF.this;
                    float A00 = C12960it.A00(valueAnimator);
                    r14.setScaleX(A00);
                    r14.setScaleY(A00);
                }
            });
            ValueAnimator ofObject = ValueAnimator.ofObject(new ArgbEvaluator(), Integer.valueOf(r11.A0O), Integer.valueOf(r11.A0N));
            ofObject.setDuration(250L);
            ofObject.setInterpolator(new AccelerateDecelerateInterpolator());
            ofObject.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: X.4ed
                @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                    AnonymousClass2CF.A01(valueAnimator, AnonymousClass2CF.this);
                }
            });
            animatorSet.addListener(new C72613et(r11, runnableBRunnable0Shape13S0100000_I0_13));
            animatorSet.playTogether(ofFloat, ofObject);
            animatorSet.start();
        }
        this.A0g.setVisibility(8);
        View findViewById2 = view.findViewById(R.id.voice_note_slide_to_cancel_animation);
        findViewById2.post(new RunnableBRunnable0Shape13S0100000_I0_13(this, 25));
        findViewById2.setVisibility(8);
        view.findViewById(R.id.voice_recorder_decor).setVisibility(8);
    }

    public void A0Q(boolean z) {
        File file = this.A0Q;
        if (file != null) {
            this.A1F.A00(file);
            A0L(this.A0Q, this.A0R, z);
            this.A0Q = null;
            int i = A1Y;
            if (i != 0) {
                A1b.play(i, 1.0f, 1.0f, 0, 0, 1.0f);
            }
            this.A0R = null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:85:0x01d0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0R(boolean r18) {
        /*
        // Method dump skipped, instructions count: 614
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC14670lq.A0R(boolean):void");
    }

    public final void A0S(boolean z) {
        View view = this.A0f;
        view.findViewById(R.id.voice_recorder_decor).setVisibility(8);
        if (this.A1S) {
            AnonymousClass2CF r0 = this.A0O;
            if (r0 != null) {
                r0.A03();
            }
            view.findViewById(R.id.voice_note_lock_container).setVisibility(8);
            view.findViewById(R.id.send).setEnabled(!TextUtils.isEmpty(this.A0h.getText().toString().trim()));
            view.findViewById(R.id.voice_note_cancel_btn).setVisibility(8);
            C14680lr r1 = this.A1G;
            r1.A05.setVisibility(8);
            r1.A03.setVisibility(8);
            VoiceVisualizer voiceVisualizer = r1.A0C;
            voiceVisualizer.A0F.clear();
            voiceVisualizer.A06 = false;
            voiceVisualizer.A02 = 0;
            voiceVisualizer.A03 = 166;
        }
        AnonymousClass4YC r9 = this.A0k;
        r9.A06.clear();
        if (r9.A09.A00 == 0.0d || !z) {
            r9.A02(0.0d);
            ImageView imageView = this.A0g;
            imageView.setVisibility(4);
            imageView.setScaleX(0.0f);
            imageView.setScaleY(0.0f);
            A0C();
        } else {
            r9.A03(new C865647x(this, (int) this.A0g.getTranslationX()));
            r9.A02(0.0d);
        }
        View findViewById = view.findViewById(R.id.voice_note_layout);
        findViewById.setVisibility(8);
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(160);
        findViewById.startAnimation(alphaAnimation);
        View findViewById2 = view.findViewById(R.id.input_layout_content);
        findViewById2.clearAnimation();
        AlphaAnimation alphaAnimation2 = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation2.setDuration(160);
        alphaAnimation2.setFillBefore(true);
        alphaAnimation2.setFillAfter(true);
        findViewById2.startAnimation(alphaAnimation2);
        ((ClippingLayout) view.findViewById(R.id.footer)).setClipBounds(null);
        this.A1C.setClipBounds(null);
        View findViewById3 = view.findViewById(R.id.voice_note_pulse);
        findViewById3.clearAnimation();
        ((ImageView) findViewById3).getDrawable().setAlpha(255);
    }

    public void A0T(boolean z, boolean z2) {
        A0U(z, z2, true, false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x0277, code lost:
        if (r32 != false) goto L_0x00ef;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00f1, code lost:
        if (r4 <= 99) goto L_0x00f3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x0273, code lost:
        if (r4 > 99) goto L_0x011c;
     */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0119  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0127  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0149  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x019a  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0239  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0U(boolean r32, boolean r33, boolean r34, boolean r35) {
        /*
        // Method dump skipped, instructions count: 643
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC14670lq.A0U(boolean, boolean, boolean, boolean):void");
    }

    public boolean A0V() {
        C14690ls r0 = this.A0P;
        return r0 != null && r0.A09.isRecording();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003a, code lost:
        if (java.lang.Math.abs(r15) <= r22.A0X) goto L_0x003c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0055, code lost:
        if (r11 >= 0.0f) goto L_0x0057;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0014, code lost:
        if (r6 != 3) goto L_0x0016;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0W(android.view.MotionEvent r23, android.view.View r24, boolean r25) {
        /*
        // Method dump skipped, instructions count: 758
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC14670lq.A0W(android.view.MotionEvent, android.view.View, boolean):boolean");
    }
}
