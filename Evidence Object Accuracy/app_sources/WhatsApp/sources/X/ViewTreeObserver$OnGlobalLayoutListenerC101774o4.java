package X;

import android.view.View;
import android.view.ViewTreeObserver;

/* renamed from: X.4o4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC101774o4 implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ View A00;
    public final /* synthetic */ AbstractC36001jA A01;
    public final /* synthetic */ Float A02;

    public ViewTreeObserver$OnGlobalLayoutListenerC101774o4(View view, AbstractC36001jA r2, Float f) {
        this.A01 = r2;
        this.A00 = view;
        this.A02 = f;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        View view = this.A00;
        C12980iv.A1E(view, this);
        this.A01.A0N(this.A02, view.getMeasuredHeight(), false);
    }
}
