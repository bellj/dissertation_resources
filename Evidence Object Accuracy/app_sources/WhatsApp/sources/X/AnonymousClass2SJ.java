package X;

import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.2SJ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2SJ {
    public final AnonymousClass3HN A00;

    public AnonymousClass2SJ(AnonymousClass3HN r1) {
        this.A00 = r1;
    }

    public static AnonymousClass2SK A00(AnonymousClass1V8 r8) {
        String A0G;
        int i;
        String A0G2;
        boolean z = false;
        if (r8 == null) {
            return new AnonymousClass2SK(null, null, 0, false);
        }
        AnonymousClass1V8 A0E = r8.A0E("status");
        AnonymousClass1V8 A0E2 = r8.A0E("can_appeal");
        AnonymousClass1V8 A0E3 = r8.A0E("reject_reason");
        AnonymousClass1V8 A0E4 = r8.A0E("commerce_url");
        String str = null;
        if (A0E == null) {
            A0G = null;
        } else {
            A0G = A0E.A0G();
        }
        if (AnonymousClass1US.A0C(A0G) || "approved".equalsIgnoreCase(A0G)) {
            i = 0;
        } else {
            i = 1;
            if ("rejected".equalsIgnoreCase(A0G)) {
                i = 2;
            }
        }
        if (A0E3 == null) {
            A0G2 = null;
        } else {
            A0G2 = A0E3.A0G();
        }
        if (A0E4 != null) {
            str = A0E4.A0G();
        }
        if (A0E2 != null && Boolean.parseBoolean(A0E2.A0G())) {
            z = true;
        }
        return new AnonymousClass2SK(A0G2, str, i, z);
    }

    public C44671zM A01(AnonymousClass1V8 r8) {
        String A0G;
        String A0G2;
        List<AnonymousClass1V8> A0J = r8.A0J("product");
        ArrayList arrayList = new ArrayList();
        for (AnonymousClass1V8 r1 : A0J) {
            C44691zO A02 = this.A00.A02(r1);
            if (A02 != null) {
                arrayList.add(A02);
            }
        }
        AnonymousClass1V8 A0E = r8.A0E("id");
        AnonymousClass1V8 A0E2 = r8.A0E("name");
        AnonymousClass1V8 A0E3 = r8.A0E("status_info");
        if (A0E == null) {
            A0G = null;
        } else {
            A0G = A0E.A0G();
        }
        if (A0E2 == null) {
            A0G2 = null;
        } else {
            A0G2 = A0E2.A0G();
        }
        if (!(A0G == null || A0G2 == null)) {
            return new C44671zM(A00(A0E3), null, A0G, A0G2, arrayList);
        }
        Log.e("CollectionParser/parseCollectionNode/required fields missing");
        return null;
    }
}
