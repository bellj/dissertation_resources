package X;

import com.whatsapp.R;

/* renamed from: X.48M  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass48M extends AbstractC93424a9 {
    public AnonymousClass48M() {
        super(new C92794Xl(R.dimen.wds_badge_icon_dimen_16, R.dimen.wds_badge_icon_dimen_20, R.dimen.wds_badge_icon_dimen_24, R.dimen.wds_badge_icon_dimen_32), new AnonymousClass48I());
    }
}
