package X;

import com.whatsapp.payments.ui.widget.PaymentView;

/* renamed from: X.6Cn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133896Cn implements AbstractC136486Ms {
    public final /* synthetic */ AbstractC1310961f A00;
    public final /* synthetic */ PaymentView A01;

    public C133896Cn(AbstractC1310961f r1, PaymentView paymentView) {
        this.A01 = paymentView;
        this.A00 = r1;
    }

    @Override // X.AbstractC136486Ms
    public void AMA(String str) {
        this.A00.AMA(str);
    }

    @Override // X.AbstractC136486Ms
    public void APq(String str) {
        this.A01.A0F(str);
        this.A00.APq(str);
    }

    @Override // X.AbstractC136486Ms
    public void AQf(String str, boolean z) {
        this.A00.AQf(str, z);
    }
}
