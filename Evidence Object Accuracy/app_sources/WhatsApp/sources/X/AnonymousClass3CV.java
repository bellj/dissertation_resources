package X;

import java.util.List;
import java.util.Map;

/* renamed from: X.3CV  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3CV {
    public final Object A00 = C12970iu.A0l();
    public final Map A01 = C12970iu.A11();

    public int A00(AnonymousClass28D r6, List list) {
        int incrementAndGet;
        C92564Wk r4 = new C92564Wk(list, r6.A00);
        synchronized (this.A00) {
            Map map = this.A01;
            Integer num = (Integer) map.get(r4);
            if (num != null) {
                incrementAndGet = num.intValue();
            } else {
                incrementAndGet = C88454Fs.A00.incrementAndGet();
                map.put(r4, Integer.valueOf(incrementAndGet));
            }
        }
        return incrementAndGet;
    }
}
