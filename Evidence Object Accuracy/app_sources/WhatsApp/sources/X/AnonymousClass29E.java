package X;

import com.facebook.redex.RunnableBRunnable0Shape2S0100000_I0_2;
import com.whatsapp.util.Log;
import java.util.TimerTask;

/* renamed from: X.29E  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass29E extends TimerTask {
    public final /* synthetic */ AnonymousClass29F A00;

    public AnonymousClass29E(AnonymousClass29F r1) {
        this.A00 = r1;
    }

    @Override // java.util.TimerTask, java.lang.Runnable
    public void run() {
        StringBuilder sb = new StringBuilder("gdrive-activity/one-time-setup/not-finished-in-");
        AnonymousClass29F r4 = this.A00;
        sb.append(r4.A07.A00() / 1000);
        sb.append("-seconds");
        Log.i(sb.toString());
        r4.A00.A0H(new RunnableBRunnable0Shape2S0100000_I0_2(this, 16));
    }
}
