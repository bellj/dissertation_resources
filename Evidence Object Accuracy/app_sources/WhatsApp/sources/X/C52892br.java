package X;

import android.animation.AnimatorSet;
import android.text.TextUtils;
import android.widget.Filter;
import com.whatsapp.status.StatusesFragment;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.2br  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52892br extends Filter {
    public final /* synthetic */ StatusesFragment A00;

    public /* synthetic */ C52892br(StatusesFragment statusesFragment) {
        this.A00 = statusesFragment;
    }

    public List A00(ArrayList arrayList, List list) {
        ArrayList A0l = C12960it.A0l();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            AnonymousClass1V2 r4 = (AnonymousClass1V2) it.next();
            StatusesFragment statusesFragment = this.A00;
            if (statusesFragment.A0F.A0M(statusesFragment.A0D.A0B(r4.A0A), arrayList, true)) {
                A0l.add(new C69643a0(r4));
            }
        }
        return A0l;
    }

    @Override // android.widget.Filter
    public Filter.FilterResults performFiltering(CharSequence charSequence) {
        Filter.FilterResults filterResults = new Filter.FilterResults();
        ArrayList A0l = C12960it.A0l();
        ArrayList arrayList = null;
        if (TextUtils.isEmpty(charSequence)) {
            StatusesFragment statusesFragment = this.A00;
            AnonymousClass1V2 r7 = statusesFragment.A0q.A00;
            if (r7 == null) {
                r7 = new AnonymousClass1V2(statusesFragment.A0L, C29831Uv.A00, 0, 0, -1, -1, -1, -1, -1, 0);
            }
            A0l.add(new C69643a0(r7));
            if (r7 == null && statusesFragment.A0j.A01()) {
                A0l.add(new C69653a1(statusesFragment));
            }
        }
        if (!TextUtils.isEmpty(charSequence)) {
            arrayList = C32751cg.A02(this.A00.A0P, charSequence.toString());
        }
        StatusesFragment statusesFragment2 = this.A00;
        List A00 = A00(arrayList, statusesFragment2.A0q.A02);
        List A002 = A00(arrayList, statusesFragment2.A0q.A03);
        List A003 = A00(arrayList, statusesFragment2.A0q.A01);
        if (!A00.isEmpty()) {
            A0l.add(new C69633Zz(statusesFragment2, 0));
            A0l.addAll(A00);
        }
        if (!A002.isEmpty()) {
            A0l.add(new C69633Zz(statusesFragment2, 1));
            A0l.addAll(A002);
        }
        filterResults.values = new AnonymousClass4OM(A0l, A003);
        filterResults.count = A0l.size();
        return filterResults;
    }

    @Override // android.widget.Filter
    public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
        String charSequence2;
        Object obj = filterResults.values;
        if (obj != null) {
            AnonymousClass4OM r1 = (AnonymousClass4OM) obj;
            StatusesFragment statusesFragment = this.A00;
            statusesFragment.A0v = r1.A00;
            List list = r1.A01;
            statusesFragment.A0w = list;
            if (!list.isEmpty()) {
                statusesFragment.A0v.add(new C69633Zz(statusesFragment, 2));
                if (!statusesFragment.A11 || statusesFragment.A0z || !statusesFragment.A0y) {
                    statusesFragment.A0v.addAll(statusesFragment.A0w);
                }
            }
        }
        StatusesFragment statusesFragment2 = this.A00;
        statusesFragment2.A0t = charSequence;
        if (charSequence == null) {
            charSequence2 = null;
        } else {
            charSequence2 = charSequence.toString();
        }
        statusesFragment2.A0u = C32751cg.A02(statusesFragment2.A0P, charSequence2);
        statusesFragment2.A1C();
        AnimatorSet animatorSet = statusesFragment2.A00;
        if (animatorSet != null) {
            animatorSet.cancel();
            statusesFragment2.A00 = null;
        }
        statusesFragment2.A0g.notifyDataSetChanged();
    }
}
