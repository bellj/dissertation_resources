package X;

/* renamed from: X.4W8  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4W8 {
    public int A00 = -1;
    public int A01;
    public int A02;
    public int A03;
    public long A04;
    public final int A05;
    public final C95304dT A06;
    public final C95304dT A07;
    public final boolean A08;

    public AnonymousClass4W8(C95304dT r3, C95304dT r4, boolean z) {
        this.A07 = r3;
        this.A06 = r4;
        this.A08 = z;
        this.A05 = C95304dT.A02(r4, 12);
        this.A03 = C95304dT.A02(r3, 12);
        C95314dV.A02("first_chunk must be 1", C12970iu.A1W(r3.A07()));
    }

    public boolean A00() {
        long A0I;
        int i = this.A00 + 1;
        this.A00 = i;
        if (i == this.A05) {
            return false;
        }
        boolean z = this.A08;
        C95304dT r0 = this.A06;
        if (z) {
            A0I = r0.A0J();
        } else {
            A0I = r0.A0I();
        }
        this.A04 = A0I;
        if (i == this.A01) {
            C95304dT r1 = this.A07;
            this.A02 = r1.A0E();
            r1.A0T(4);
            int i2 = this.A03 - 1;
            this.A03 = i2;
            this.A01 = i2 > 0 ? r1.A0E() - 1 : -1;
        }
        return true;
    }
}
