package X;

import com.whatsapp.util.Log;
import java.io.Closeable;
import java.util.AbstractMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* renamed from: X.0on  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16310on implements Closeable {
    public boolean A00 = false;
    public final AbstractC15710nm A01;
    public final C29621Ty A02;
    public final C16330op A03;
    public final ReentrantReadWriteLock.ReadLock A04;

    public C16310on(AbstractC15710nm r3, AbstractC16300om r4, ReentrantReadWriteLock.ReadLock readLock, boolean z) {
        this.A04 = readLock;
        this.A02 = r4.AEm();
        this.A01 = r3;
        if (readLock != null) {
            readLock.lock();
            Thread.currentThread().getId();
        }
        try {
            if (z) {
                this.A03 = r4.AHr();
            } else {
                this.A03 = r4.AG6();
            }
        } catch (Exception e) {
            Log.e("DatabaseSession/failed to get database", e);
            close();
            throw e;
        }
    }

    public AnonymousClass1Lx A00() {
        AnonymousClass009.A00();
        return new AnonymousClass1Lx(null, this.A02, this.A03);
    }

    @Deprecated
    public AnonymousClass1Lx A01() {
        return new AnonymousClass1Lx(null, this.A02, this.A03);
    }

    public C16330op A02() {
        return this.A03;
    }

    public void A03(Runnable runnable) {
        AnonymousClass009.A0F(this.A03.A00.inTransaction());
        C29621Ty r0 = this.A02;
        Object obj = new Object();
        AnonymousClass1YC r1 = new AnonymousClass1YC(r0, runnable);
        Object obj2 = r0.A01.get();
        AnonymousClass009.A05(obj2);
        ((AbstractMap) obj2).put(obj, r1);
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        if (!this.A00) {
            ReentrantReadWriteLock.ReadLock readLock = this.A04;
            if (readLock != null) {
                Thread.currentThread().getId();
                readLock.unlock();
            }
            this.A00 = true;
        }
    }
}
