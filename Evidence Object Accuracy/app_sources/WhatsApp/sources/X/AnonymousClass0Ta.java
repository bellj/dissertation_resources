package X;

/* renamed from: X.0Ta  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0Ta {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public AnonymousClass01E A05;
    public AnonymousClass05I A06;
    public AnonymousClass05I A07;

    public AnonymousClass0Ta() {
    }

    public AnonymousClass0Ta(AnonymousClass01E r2, int i) {
        this.A00 = i;
        this.A05 = r2;
        AnonymousClass05I r0 = AnonymousClass05I.RESUMED;
        this.A07 = r0;
        this.A06 = r0;
    }

    public AnonymousClass0Ta(AnonymousClass01E r2, AnonymousClass05I r3) {
        this.A00 = 10;
        this.A05 = r2;
        this.A07 = r2.A0J;
        this.A06 = r3;
    }
}
