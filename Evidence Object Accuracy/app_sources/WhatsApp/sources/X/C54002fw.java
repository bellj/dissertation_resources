package X;

import android.content.Context;
import android.database.Cursor;

/* renamed from: X.2fw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54002fw extends AnonymousClass0ER {
    public Cursor A00;
    public AnonymousClass02N A01;
    public final AbstractC20010v4 A02;
    public final AbstractC14640lm A03;
    public final String A04;

    public C54002fw(Context context, AbstractC20010v4 r2, AbstractC14640lm r3, String str) {
        super(context);
        this.A04 = str;
        this.A02 = r2;
        this.A03 = r3;
    }

    @Override // X.AnonymousClass0QL
    public void A01() {
        A00();
        Cursor cursor = this.A00;
        if (cursor != null && !cursor.isClosed()) {
            this.A00.close();
        }
        this.A00 = null;
    }

    @Override // X.AnonymousClass0QL
    public void A02() {
        A00();
    }

    @Override // X.AnonymousClass0QL
    public void A03() {
        Cursor cursor = this.A00;
        if (cursor != null) {
            A04(cursor);
        }
        boolean z = super.A03;
        super.A03 = false;
        super.A04 |= z;
        if (z || this.A00 == null) {
            A09();
        }
    }

    @Override // X.AnonymousClass0ER
    public /* bridge */ /* synthetic */ Object A06() {
        AnonymousClass02N r4;
        Cursor cursor;
        synchronized (this) {
            try {
                if (!C12960it.A1W(((AnonymousClass0ER) this).A01)) {
                    r4 = new AnonymousClass02N();
                    this.A01 = r4;
                } else {
                    throw new AnonymousClass04U(null);
                }
            } catch (Throwable th) {
                throw th;
            }
        }
        try {
            AbstractC14640lm r2 = this.A03;
            if (r2 != null) {
                cursor = this.A02.AEL(r4, r2, this.A04);
            } else {
                cursor = this.A02.AEK(r4, this.A04);
            }
            if (cursor != null) {
                try {
                    cursor.getCount();
                } catch (RuntimeException e) {
                    cursor.close();
                    throw e;
                }
            }
            synchronized (this) {
                try {
                    this.A01 = null;
                } catch (Throwable th2) {
                    throw th2;
                }
            }
            return cursor;
        } catch (Throwable th3) {
            synchronized (this) {
                try {
                    this.A01 = null;
                    throw th3;
                } catch (Throwable th4) {
                    throw th4;
                }
            }
        }
    }

    @Override // X.AnonymousClass0ER
    public void A07() {
        synchronized (this) {
            AnonymousClass02N r0 = this.A01;
            if (r0 != null) {
                r0.A01();
            }
        }
    }

    @Override // X.AnonymousClass0ER
    public void A0B(Object obj) {
        Cursor cursor = (Cursor) obj;
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
    }

    /* renamed from: A0C */
    public void A04(Cursor cursor) {
        if (!this.A05) {
            Cursor cursor2 = this.A00;
            this.A00 = cursor;
            if (this.A06) {
                super.A04(cursor);
            }
            if (cursor2 != null && cursor2 != cursor && !cursor2.isClosed()) {
                cursor2.close();
            }
        } else if (cursor != null) {
            cursor.close();
        }
    }
}
