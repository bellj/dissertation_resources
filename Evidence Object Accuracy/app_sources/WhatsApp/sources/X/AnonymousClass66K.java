package X;

import android.media.Image;
import android.media.ImageReader;
import android.os.Handler;
import android.util.Log;
import android.view.Surface;
import java.nio.ByteBuffer;

/* renamed from: X.66K  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass66K implements AbstractC136526Mw {
    public ImageReader A00;
    public final ImageReader.OnImageAvailableListener A01 = new AnonymousClass63J(this);
    public volatile C128475wA A02;

    @Override // X.AbstractC136526Mw
    public int ADQ() {
        return 256;
    }

    public static /* synthetic */ void A00(ImageReader imageReader, AnonymousClass66K r7) {
        ImageReader imageReader2 = r7.A00;
        byte[] bArr = null;
        if (imageReader2 != null) {
            imageReader2.setOnImageAvailableListener(null, null);
        }
        C128475wA r4 = r7.A02;
        r7.A02 = null;
        try {
            if (r4 != null) {
                try {
                    Image acquireNextImage = imageReader.acquireNextImage();
                    if (acquireNextImage != null) {
                        try {
                            Image.Plane[] planes = acquireNextImage.getPlanes();
                            if (planes != null && planes.length > 0) {
                                ByteBuffer buffer = planes[0].getBuffer();
                                buffer.rewind();
                                bArr = new byte[buffer.remaining()];
                                buffer.get(bArr);
                            }
                            acquireNextImage.close();
                        } catch (Throwable th) {
                            try {
                                acquireNextImage.close();
                            } catch (Throwable unused) {
                            }
                            throw th;
                        }
                    }
                } catch (Exception e) {
                    StringBuilder A0h = C12960it.A0h();
                    A0h.append("Failed to acquire image: ");
                    Log.e("DefaultPhotoProcessor", C12960it.A0d(e.getMessage(), A0h), e);
                }
            }
        } finally {
            if (C130215yz.A00()) {
                bArr = C130215yz.A01();
            }
            r4.A00(new C130635zj(bArr));
        }
    }

    @Override // X.AbstractC136526Mw
    public void AIY(int i, int i2, int i3) {
        this.A00 = ImageReader.newInstance(i, i2, 256, 1);
    }

    @Override // X.AbstractC136526Mw
    public void AZT(Handler handler, C128475wA r4) {
        if (this.A00 != null) {
            this.A02 = r4;
            this.A00.setOnImageAvailableListener(this.A01, handler);
        }
    }

    @Override // X.AbstractC136526Mw
    public Surface getSurface() {
        ImageReader imageReader = this.A00;
        if (imageReader != null) {
            return imageReader.getSurface();
        }
        return null;
    }

    @Override // X.AbstractC136526Mw
    public void release() {
        ImageReader imageReader = this.A00;
        if (imageReader != null) {
            imageReader.setOnImageAvailableListener(null, null);
            this.A00.close();
            this.A00 = null;
        }
        this.A02 = null;
    }
}
