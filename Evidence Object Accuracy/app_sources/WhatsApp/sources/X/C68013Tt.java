package X;

import android.content.SharedPreferences;
import android.view.View;
import com.whatsapp.R;

/* renamed from: X.3Tt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68013Tt implements AbstractC116635Wf {
    public View A00;
    public String A01;
    public final AnonymousClass12P A02;
    public final C53042cM A03;
    public final C14820m6 A04;
    public final C16120oU A05;
    public final AnonymousClass01N A06;

    public C68013Tt(AnonymousClass12P r1, C53042cM r2, C14820m6 r3, C16120oU r4, AnonymousClass01N r5) {
        this.A05 = r4;
        this.A02 = r1;
        this.A03 = r2;
        this.A04 = r3;
        this.A06 = r5;
    }

    @Override // X.AbstractC116635Wf
    public void AIR() {
        C12970iu.A1G(this.A00);
    }

    @Override // X.AbstractC116635Wf
    public boolean AdK() {
        long j;
        C90814Pi r0 = (C90814Pi) this.A06.get();
        if (r0 == null) {
            return false;
        }
        long j2 = r0.A01;
        long j3 = r0.A00;
        C14820m6 r8 = this.A04;
        if (j2 > 5000000000L) {
            j = 500000000;
        } else {
            j = (long) (((float) (j2 * ((long) ((int) ((((float) 500000000) * 100.0f) / 5.0E9f))))) / 100.0f);
        }
        int i = (j3 > j ? 1 : (j3 == j ? 0 : -1));
        boolean z = false;
        if (i <= 0) {
            z = true;
        }
        SharedPreferences sharedPreferences = r8.A00;
        if (sharedPreferences.getBoolean("storage_usage_banner_dismissed", false)) {
            if (!z) {
                C12960it.A0t(sharedPreferences.edit(), "storage_usage_banner_dismissed", false);
                return false;
            }
        } else if (!z) {
            return false;
        }
        if (!sharedPreferences.getBoolean("storage_usage_banner_dismissed", false)) {
            return true;
        }
        return false;
    }

    @Override // X.AbstractC116635Wf
    public void AfF() {
        if (this.A01 == null) {
            this.A01 = AnonymousClass3GM.A00(this.A05, 3);
        }
        if (this.A00 == null) {
            C53042cM r2 = this.A03;
            View A0F = C12960it.A0F(C12960it.A0E(r2), r2, R.layout.storage_usage_banner);
            this.A00 = A0F;
            C12960it.A10(A0F, this, 42);
            C12960it.A10(AnonymousClass028.A0D(this.A00, R.id.storage_usage_full_close_container), this, 43);
            r2.addView(this.A00);
        }
        View view = this.A00;
        AnonymousClass009.A03(view);
        view.setVisibility(0);
        this.A00.setBackgroundResource(R.color.warning_banner_background_color);
    }
}
