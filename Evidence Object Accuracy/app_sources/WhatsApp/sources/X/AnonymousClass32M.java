package X;

import android.text.TextUtils;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaTextView;
import com.whatsapp.group.GroupChatInfo;
import java.util.ArrayList;

/* renamed from: X.32M  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass32M extends AnonymousClass4V1 {
    public final WaTextView A00;
    public final /* synthetic */ GroupChatInfo A01;

    public AnonymousClass32M(View view, GroupChatInfo groupChatInfo) {
        this.A01 = groupChatInfo;
        this.A00 = C12960it.A0N(view, R.id.search_no_matches);
    }

    @Override // X.AnonymousClass4V1
    public void A00(AbstractC36121jM r7, C92434Vw r8, ArrayList arrayList) {
        super.A00 = r7;
        String str = ((C36131jN) r7).A00;
        if (TextUtils.isEmpty(str)) {
            this.A00.setText(R.string.search_no_results_found);
        } else {
            this.A00.setText(C12960it.A0X(this.A01, str, C12970iu.A1b(), 0, R.string.search_no_results));
        }
    }
}
