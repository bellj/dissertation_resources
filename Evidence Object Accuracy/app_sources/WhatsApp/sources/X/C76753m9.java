package X;

import java.nio.ByteBuffer;

/* renamed from: X.3m9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C76753m9 extends C76763mA {
    public int A00 = 32;
    public int A01;
    public long A02;

    public C76753m9() {
        super(2);
    }

    public boolean A02(C76763mA r5) {
        ByteBuffer byteBuffer;
        C95314dV.A03(!C12960it.A1V(r5.flags & 1073741824, 1073741824));
        C95314dV.A03(!C12960it.A1V(r5.flags & 268435456, 268435456));
        C95314dV.A03(!AnonymousClass4YO.A00(r5));
        int i = this.A01;
        if (i > 0) {
            if (i >= this.A00 || C12960it.A1V(r5.flags & Integer.MIN_VALUE, Integer.MIN_VALUE) != C12960it.A1V(this.flags & Integer.MIN_VALUE, Integer.MIN_VALUE)) {
                return false;
            }
            ByteBuffer byteBuffer2 = r5.A01;
            if (!(byteBuffer2 == null || (byteBuffer = super.A01) == null || byteBuffer.position() + byteBuffer2.remaining() <= 3072000)) {
                return false;
            }
        }
        int i2 = this.A01;
        this.A01 = i2 + 1;
        if (i2 == 0) {
            super.A00 = r5.A00;
            if (C12960it.A1V(r5.flags & 1, 1)) {
                this.flags = 1;
            }
        }
        if (C12960it.A1V(r5.flags & Integer.MIN_VALUE, Integer.MIN_VALUE)) {
            this.flags = Integer.MIN_VALUE;
        }
        ByteBuffer byteBuffer3 = r5.A01;
        if (byteBuffer3 != null) {
            A01(byteBuffer3.remaining());
            super.A01.put(byteBuffer3);
        }
        this.A02 = r5.A00;
        return true;
    }

    @Override // X.C76763mA, X.AnonymousClass4YO
    public void clear() {
        super.clear();
        this.A01 = 0;
    }
}
