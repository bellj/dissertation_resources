package X;

import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.WaImageView;

/* renamed from: X.5ls  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122695ls extends AbstractC118835cS {
    public final View A00;
    public final TextView A01;
    public final WaImageView A02;

    public C122695ls(View view) {
        super(view);
        this.A01 = C12960it.A0I(view, R.id.description);
        this.A02 = C12980iv.A0X(view, R.id.asset_id);
        this.A00 = view;
    }

    @Override // X.AbstractC118835cS
    public void A08(AbstractC125975s7 r4, int i) {
        C123155mh r42 = (C123155mh) r4;
        TextView textView = this.A01;
        textView.setText(r42.A01);
        if (TextUtils.isEmpty(r42.A01)) {
            textView.setVisibility(8);
        }
        this.A02.setVisibility(8);
        this.A00.setOnClickListener(r42.A00);
    }
}
