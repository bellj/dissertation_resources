package X;

/* renamed from: X.5sr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C126425sr {
    public final AnonymousClass1V8 A00;

    public C126425sr(AnonymousClass3CT r18, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy A0N = C117295Zj.A0N(A0M);
        C41141sy.A01(A0N, "action", "upi-change-mpin");
        if (C117305Zk.A1Y(str, false)) {
            C41141sy.A01(A0N, "credential-id", str);
        }
        if (AnonymousClass3JT.A0E(str2, 35, 35, false)) {
            C41141sy.A01(A0N, "seq-no", str2);
        }
        if (AnonymousClass3JT.A0E(str3, 0, 1000, false)) {
            C41141sy.A01(A0N, "old-mpin", str3);
        }
        if (AnonymousClass3JT.A0E(str4, 0, 1000, false)) {
            C41141sy.A01(A0N, "new-mpin", str4);
        }
        if (C117295Zj.A1V(str5, 1, false)) {
            C41141sy.A01(A0N, "device-id", str5);
        }
        if (str6 != null && AnonymousClass3JT.A0E(str6, 0, 100, true)) {
            C41141sy.A01(A0N, "vpa", str6);
        }
        if (str7 != null && AnonymousClass3JT.A0E(str7, 1, 100, true)) {
            C41141sy.A01(A0N, "vpa-id", str7);
        }
        if (str8 != null && C117295Zj.A1Y(str8, true)) {
            C41141sy.A01(A0N, "upi-bank-info", str8);
        }
        this.A00 = C117295Zj.A0J(A0N, A0M, r18);
    }
}
