package X;

import java.math.BigInteger;
import java.util.Arrays;

/* renamed from: X.1ad  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C31491ad implements Comparable {
    public final byte[] A00;

    public C31491ad(byte[] bArr) {
        this.A00 = bArr;
    }

    public byte[] A00() {
        return C31241aE.A00(new byte[]{5}, this.A00);
    }

    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(Object obj) {
        return new BigInteger(this.A00).compareTo(new BigInteger(((C31491ad) obj).A00));
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof C31491ad)) {
            return false;
        }
        return Arrays.equals(this.A00, ((C31491ad) obj).A00);
    }

    @Override // java.lang.Object
    public int hashCode() {
        return Arrays.hashCode(this.A00);
    }
}
