package X;

import android.content.Context;
import android.util.SparseArray;
import java.util.Map;

/* renamed from: X.0Rw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06020Rw {
    public final Context A00;
    public final SparseArray A01;
    public final C64173En A02;
    public final Map A03;
    public final int[] A04;

    public C06020Rw(Context context, SparseArray sparseArray, C64173En r3, Map map, int[] iArr) {
        this.A00 = context;
        this.A02 = r3;
        this.A01 = sparseArray;
        this.A03 = map;
        this.A04 = iArr;
    }

    public static C06020Rw A00(Context context, SparseArray sparseArray, C64173En r3, Map map, int[] iArr) {
        return new C06020Rw(context, sparseArray, r3, map, iArr);
    }
}
