package X;

import android.content.Context;

/* renamed from: X.5gk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120785gk extends C120895gv {
    public final /* synthetic */ C120375g5 A00;
    public final /* synthetic */ C128815wi A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120785gk(Context context, C14900mE r8, C18650sn r9, C64513Fv r10, C120375g5 r11, C128815wi r12) {
        super(context, r8, r9, r10, "upi-sign-qr-code");
        this.A00 = r11;
        this.A01 = r12;
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A02(C452120p r3) {
        super.A02(r3);
        this.A01.A00(r3, null);
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A03(C452120p r3) {
        super.A03(r3);
        this.A01.A00(r3, null);
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A04(AnonymousClass1V8 r5) {
        super.A04(r5);
        AnonymousClass1V8 A0c = C117305Zk.A0c(r5);
        if (A0c != null) {
            this.A01.A00(null, A0c.A0I("signed-qr-code", null));
        }
    }
}
