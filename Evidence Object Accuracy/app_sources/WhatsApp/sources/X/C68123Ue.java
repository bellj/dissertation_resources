package X;

import java.util.Iterator;

/* renamed from: X.3Ue  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68123Ue implements AnonymousClass5TI {
    public long A00 = 0;
    public final /* synthetic */ C58872rq A01;

    public C68123Ue(C58872rq r3) {
        this.A01 = r3;
    }

    @Override // X.AnonymousClass5TI
    public void AOs(long j) {
        long j2;
        long j3 = this.A00 + j;
        this.A00 = j3;
        if (j > 0) {
            C58872rq r0 = this.A01;
            AnonymousClass10N r5 = r0.A01;
            AnonymousClass3HV r3 = r0.A02;
            AnonymousClass3FI r02 = r3.A02;
            if (r02 != null) {
                j2 = r02.A00;
            } else {
                j2 = r3.A00;
            }
            Iterator A00 = AbstractC16230of.A00(r5);
            while (A00.hasNext()) {
                ((AnonymousClass10L) A00.next()).ASm(j3, j2);
            }
        }
    }
}
