package X;

import java.util.List;

/* renamed from: X.22M  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass22M extends AbstractC28131Kt {
    public final AbstractC14640lm A00;
    public final C34081fY A01;
    public final List A02;
    public final List A03;

    public AnonymousClass22M(AbstractC14640lm r1, C34081fY r2, C34021fS r3, String str, List list, List list2) {
        super(r3, str);
        this.A00 = r1;
        this.A03 = list;
        this.A02 = list2;
        this.A01 = r2;
    }
}
