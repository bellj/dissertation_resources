package X;

import android.os.Build;
import android.os.PowerManager;

/* renamed from: X.143  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass143 extends AbstractC16220oe {
    public final AnonymousClass01d A00;
    public volatile Boolean A01;

    public AnonymousClass143(AnonymousClass01d r1) {
        this.A00 = r1;
    }

    public void A05(boolean z) {
        this.A01 = Boolean.valueOf(z);
        for (C22230yk r1 : A01()) {
            r1.A02(r1.A06.A00, z);
        }
    }

    public boolean A06() {
        Boolean bool;
        if (this.A01 == null) {
            if (Build.VERSION.SDK_INT >= 21) {
                PowerManager A0I = this.A00.A0I();
                if (A0I == null) {
                    bool = Boolean.TRUE;
                } else {
                    bool = Boolean.valueOf(A0I.isPowerSaveMode());
                }
            } else {
                bool = Boolean.FALSE;
            }
            this.A01 = bool;
        }
        return this.A01.booleanValue();
    }
}
