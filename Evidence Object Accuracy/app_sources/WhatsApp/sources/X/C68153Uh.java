package X;

import android.os.Bundle;
import com.whatsapp.backup.google.viewmodel.SettingsGoogleDriveViewModel;
import com.whatsapp.util.Log;

/* renamed from: X.3Uh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68153Uh implements AnonymousClass10L {
    public int A00;
    public long A01 = -1;
    public long A02 = -1;
    public final C14900mE A03;
    public final AnonymousClass10K A04;
    public final SettingsGoogleDriveViewModel A05;
    public final C18640sm A06;
    public final C14820m6 A07;

    public C68153Uh(C14900mE r3, AnonymousClass10K r4, SettingsGoogleDriveViewModel settingsGoogleDriveViewModel, C18640sm r6, C14820m6 r7) {
        this.A03 = r3;
        this.A07 = r7;
        this.A06 = r6;
        this.A04 = r4;
        this.A05 = settingsGoogleDriveViewModel;
    }

    public static int A00(int i, long j, long j2) {
        if (i > 0) {
            return (int) ((j * 100) / j2);
        }
        return -1;
    }

    public static void A01(C68153Uh r3) {
        r3.A03(null, 2, -1);
    }

    public final void A02(AbstractC87994Dv r7, int i, int i2) {
        A04(r7, i, i2, true, false);
    }

    public final void A03(AbstractC87994Dv r7, int i, int i2) {
        A04(r7, i, i2, false, false);
    }

    public final void A04(AbstractC87994Dv r8, int i, int i2, boolean z, boolean z2) {
        AnonymousClass016 r0;
        SettingsGoogleDriveViewModel settingsGoogleDriveViewModel;
        if (C12980iv.A1V(i, this.A00) && i == 4) {
            Log.i("settings-gdrive/set-message/show-indeterminate");
        }
        this.A00 = i;
        boolean z3 = null;
        if (i == 1) {
            Log.i("settings-gdrive/set-message/show-nothing");
            SettingsGoogleDriveViewModel settingsGoogleDriveViewModel2 = this.A05;
            settingsGoogleDriveViewModel2.A07.A0A(false);
            settingsGoogleDriveViewModel2.A0L.A0A(false);
            settingsGoogleDriveViewModel2.A09.A0A(false);
            settingsGoogleDriveViewModel2.A0M.A0A(false);
            settingsGoogleDriveViewModel2.A0B.A0A(false);
            settingsGoogleDriveViewModel2.A06.A0A(false);
            r0 = settingsGoogleDriveViewModel2.A08;
        } else if (i != 2) {
            if (i == 3) {
                AnonymousClass009.A05(r8);
                Log.i("settings-gdrive/set-message/show-determinate");
                settingsGoogleDriveViewModel = this.A05;
                z3 = false;
                settingsGoogleDriveViewModel.A07.A0A(false);
                settingsGoogleDriveViewModel.A0L.A0A(true);
                settingsGoogleDriveViewModel.A0J.A0A(false);
                settingsGoogleDriveViewModel.A09.A0A(Boolean.valueOf(z));
                settingsGoogleDriveViewModel.A06.A0A(true);
                if (i2 >= 0) {
                    C12970iu.A1Q(settingsGoogleDriveViewModel.A0K, i2);
                }
                settingsGoogleDriveViewModel.A08.A0A(r8);
                settingsGoogleDriveViewModel.A0M.A0A(Boolean.valueOf(z2));
            } else if (i == 4) {
                AnonymousClass009.A05(r8);
                settingsGoogleDriveViewModel = this.A05;
                z3 = false;
                settingsGoogleDriveViewModel.A07.A0A(false);
                settingsGoogleDriveViewModel.A0M.A0A(false);
                settingsGoogleDriveViewModel.A0L.A0A(true);
                settingsGoogleDriveViewModel.A0J.A0A(true);
                settingsGoogleDriveViewModel.A09.A0A(Boolean.valueOf(z));
                settingsGoogleDriveViewModel.A06.A0A(true);
                StringBuilder A0j = C12960it.A0j("settings-gdrive/set-message ");
                A0j.append(r8);
                C12960it.A1F(A0j);
                settingsGoogleDriveViewModel.A08.A0A(r8);
            } else {
                return;
            }
            r0 = settingsGoogleDriveViewModel.A0B;
        } else {
            Log.i("settings-gdrive/set-message/show-backup-button");
            SettingsGoogleDriveViewModel settingsGoogleDriveViewModel3 = this.A05;
            settingsGoogleDriveViewModel3.A07.A0A(true);
            settingsGoogleDriveViewModel3.A0L.A0A(false);
            settingsGoogleDriveViewModel3.A09.A0A(false);
            settingsGoogleDriveViewModel3.A0M.A0A(false);
            settingsGoogleDriveViewModel3.A0B.A0A(true);
            settingsGoogleDriveViewModel3.A06.A0A(false);
            settingsGoogleDriveViewModel3.A05.A0A(true);
            settingsGoogleDriveViewModel3.A08.A0A(null);
            C14900mE.A00(this.A03, settingsGoogleDriveViewModel3, 20);
            if (r8 != null) {
                throw C12970iu.A0f("message should be null when button has to be displayed.");
            }
            return;
        }
        r0.A0A(z3);
    }

    @Override // X.AnonymousClass10L
    public void ALq(boolean z) {
        Log.e("settings-gdrive-observer/account-deletion-end/unexpected-state");
    }

    @Override // X.AnonymousClass10L
    public void AMs() {
        Log.i("settings-gdrive-observer/backup-cancelled");
        A01(this);
    }

    @Override // X.AnonymousClass10L
    public void AMt(boolean z) {
        StringBuilder A0k = C12960it.A0k("settings-gdrive-observer/backup-end ");
        A0k.append(z);
        C12960it.A1F(A0k);
        A01(this);
        if (z && this.A05.A0g.get()) {
            this.A04.A03();
        }
    }

    @Override // X.AnonymousClass10L
    public void AMz(long j, long j2) {
        Log.i("settings-gdrive-observer/backup-paused/no-data-connection");
        A02(new C58922rz(8), 3, A00((j2 > 0 ? 1 : (j2 == 0 ? 0 : -1)), j, j2));
    }

    @Override // X.AnonymousClass10L
    public void AN0(long j, long j2) {
        Log.i("settings-gdrive-observer/backup-paused/low-battery");
        A02(new C58922rz(9), 3, A00((j2 > 0 ? 1 : (j2 == 0 ? 0 : -1)), j, j2));
    }

    @Override // X.AnonymousClass10L
    public void AN1(long j, long j2) {
        Log.i("settings-gdrive-observer/backup-paused/sdcard-missing");
        A02(new C58922rz(11), 3, A00((j2 > 0 ? 1 : (j2 == 0 ? 0 : -1)), j, j2));
    }

    @Override // X.AnonymousClass10L
    public void AN2(long j, long j2) {
        Log.i("settings-gdrive-observer/backup-paused/sdcard-unmounted");
        A02(new C58922rz(10), 3, A00((j2 > 0 ? 1 : (j2 == 0 ? 0 : -1)), j, j2));
    }

    @Override // X.AnonymousClass10L
    public void AN3(long j, long j2) {
        Log.i("settings-gdrive-observer/backup-paused/no-wifi");
        int A00 = A00((j2 > 0 ? 1 : (j2 == 0 ? 0 : -1)), j, j2);
        int i = 7;
        if (this.A06.A05(true) == 2) {
            i = 6;
        }
        A02(new C58922rz(i), 3, A00);
    }

    @Override // X.AnonymousClass10L
    public void AN4(int i) {
        if (i >= 0) {
            AnonymousClass009.A00();
            A02(new C84273yh(i), 4, i);
        }
    }

    @Override // X.AnonymousClass10L
    public void AN5() {
        Log.i("settings-gdrive-observer/backup-prep-start");
        A02(new C58922rz(5), 4, -1);
    }

    @Override // X.AnonymousClass10L
    public void AN6(long j, long j2) {
        if (j2 <= 0) {
            StringBuilder A0k = C12960it.A0k("settings-gdrive-observer/backup-progress incorrect invocation: ");
            A0k.append(j);
            A0k.append("/");
            Log.e(C12970iu.A0w(A0k, j2));
            return;
        }
        long j3 = this.A02;
        int i = (int) ((100 * j) / j2);
        if (((int) ((j3 * 100) / j2)) != i || j != j3) {
            this.A02 = j;
            A02(new C84293yj(j, j2), 3, i);
        }
    }

    @Override // X.AnonymousClass10L
    public void AN7() {
        Log.i("settings-gdrive-observer/backup-start");
        this.A02 = -1;
        AN4(0);
    }

    @Override // X.AnonymousClass10L
    public void APg() {
        C14820m6 r1 = this.A07;
        if (r1.A06(r1.A09()) == 2) {
            C14900mE.A00(this.A03, this.A05, 20);
        }
    }

    @Override // X.AnonymousClass10L
    public void APw(int i, Bundle bundle) {
        if (i != 10) {
            A01(this);
        }
        C12970iu.A1Q(this.A05.A0D, i);
    }

    @Override // X.AnonymousClass10L
    public void APx(int i, Bundle bundle) {
        if (i != 10) {
            A03(null, 1, -1);
        }
        this.A05.A0E.A0A(new C90054Mk(i, bundle));
    }

    @Override // X.AnonymousClass10L
    public void APy(int i, Bundle bundle) {
        Log.e("settings-gdrive-observer/msgstore-download-error/unexpected-state");
    }

    @Override // X.AnonymousClass10L
    public void ASU() {
        Log.i("settings-gdrive-observer/restore-cancelled");
        this.A05.A06(false);
        A01(this);
        this.A01 = -1;
        this.A02 = -1;
    }

    @Override // X.AnonymousClass10L
    public void ASV(long j, long j2, boolean z) {
        StringBuilder A0k = C12960it.A0k("settings-gdrive-observer/restore-end ");
        A0k.append(z);
        C12960it.A1F(A0k);
        A01(this);
        this.A01 = -1;
        this.A02 = -1;
        if (z && this.A05.A0g.get()) {
            this.A04.A03();
        }
    }

    @Override // X.AnonymousClass10L
    public void ASW(long j, long j2) {
        Log.i("settings-gdrive-observer/restore-paused/no-data-connection");
        A03(new C58922rz(1), 3, A00((j2 > 0 ? 1 : (j2 == 0 ? 0 : -1)), j, j2));
    }

    @Override // X.AnonymousClass10L
    public void ASX(long j, long j2) {
        Log.i("settings-gdrive-observer/restore-paused/low-battery");
        int A00 = A00((j2 > 0 ? 1 : (j2 == 0 ? 0 : -1)), j, j2);
        this.A05.A0f.set(true);
        A04(new C58922rz(2), 3, A00, false, true);
    }

    @Override // X.AnonymousClass10L
    public void ASY(long j, long j2) {
        Log.i("settings-gdrive-observer/restore-paused/sdcard-missing");
        A03(new C58922rz(4), 3, A00((j2 > 0 ? 1 : (j2 == 0 ? 0 : -1)), j, j2));
    }

    @Override // X.AnonymousClass10L
    public void ASZ(long j, long j2) {
        Log.i("settings-gdrive-observer/restore-paused/sdcard-unmounted");
        A03(new C58922rz(3), 3, A00((j2 > 0 ? 1 : (j2 == 0 ? 0 : -1)), j, j2));
    }

    @Override // X.AnonymousClass10L
    public void ASa(long j, long j2) {
        Log.i("settings-gdrive-observer/restore-paused/no-wifi");
        int A00 = A00((j2 > 0 ? 1 : (j2 == 0 ? 0 : -1)), j, j2);
        this.A05.A0f.set(false);
        if (this.A06.A05(true) == 2) {
            Log.i("settings-gdrive-observer/restore-paused/cellular-available");
            A04(new C58922rz(0), 3, A00, false, true);
            return;
        }
        A03(new C58922rz(0), 3, A00);
    }

    @Override // X.AnonymousClass10L
    public void ASb(int i) {
        if (i >= 0) {
            A03(new C84283yi(i), 4, i);
        }
    }

    @Override // X.AnonymousClass10L
    public void ASc() {
        Log.i("settings-gdrive-observer/restore-start");
        A03(new C58922rz(13), 4, -1);
    }

    @Override // X.AnonymousClass10L
    public void ASd(long j, long j2, long j3) {
        if (j != this.A01) {
            this.A01 = j;
            A03(new C84303yk(j, j3), 3, (int) ((j * 100) / j3));
        }
    }

    @Override // X.AnonymousClass10L
    public void ASl(boolean z) {
        Log.e("settings-gdrive-observer/msgstore-download-end/unexpected-state");
    }

    @Override // X.AnonymousClass10L
    public void ASm(long j, long j2) {
        Log.e("settings-gdrive-observer/msgstore-download-progress/unexpected-state");
        StringBuilder A0k = C12960it.A0k("settings-gdrive-observer/msgstore-download-progress/downloaded: ");
        A0k.append(j);
        A0k.append(" total: ");
        A0k.append(j2);
        C12960it.A1F(A0k);
    }

    @Override // X.AnonymousClass10L
    public void ASn() {
        Log.e("settings-gdrive-observer/msgstore-download-start/unexpected-state");
    }

    @Override // X.AnonymousClass10L
    public void AVb() {
        Log.i("settings-gdrive-observer/post-backup-scrub-start");
        A02(new C58922rz(12), 4, -1);
    }

    @Override // X.AnonymousClass10L
    public void AY3() {
        C14900mE.A00(this.A03, this.A05, 20);
    }
}
