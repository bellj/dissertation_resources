package X;

/* renamed from: X.317  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass317 extends AbstractC16110oT {
    public Double A00;
    public Double A01;
    public Integer A02;
    public Long A03;
    public Long A04;
    public Long A05;
    public Long A06;
    public Long A07;
    public Long A08;

    public AnonymousClass317() {
        super(2900, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(10, this.A03);
        r3.Abe(2, this.A04);
        r3.Abe(5, this.A00);
        r3.Abe(7, this.A05);
        r3.Abe(1, this.A06);
        r3.Abe(8, this.A07);
        r3.Abe(4, this.A01);
        r3.Abe(6, this.A08);
        r3.Abe(9, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamEncryptedBackupsInitial {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "backupRestoreInSessionRetryCount", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "reencryptionMediaFilesEncrypted", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "reencryptionMediaProgress", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "reencryptionMediaTime", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "reencryptionOverallFilesEncrypted", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "reencryptionOverallMibytes", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "reencryptionOverallProgress", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "reencryptionOverallTime", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "reencryptionResult", C12960it.A0Y(this.A02));
        return C12960it.A0d("}", A0k);
    }
}
