package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2mE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56952mE extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C56952mE A02;
    public static volatile AnonymousClass255 A03;
    public int A00;
    public AnonymousClass1G8 A01;

    static {
        C56952mE r0 = new C56952mE();
        A02 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r5, Object obj, Object obj2) {
        AnonymousClass1G9 r1;
        switch (r5.ordinal()) {
            case 0:
                return A02;
            case 1:
                AbstractC462925h r6 = (AbstractC462925h) obj;
                C56952mE r7 = (C56952mE) obj2;
                this.A01 = (AnonymousClass1G8) r6.Aft(this.A01, r7.A01);
                if (r6 == C463025i.A00) {
                    this.A00 |= r7.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r62 = (AnonymousClass253) obj;
                AnonymousClass254 r72 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        try {
                            int A032 = r62.A03();
                            if (A032 == 0) {
                                break;
                            } else if (A032 == 10) {
                                if ((this.A00 & 1) == 1) {
                                    r1 = (AnonymousClass1G9) this.A01.A0T();
                                } else {
                                    r1 = null;
                                }
                                AnonymousClass1G8 r0 = (AnonymousClass1G8) AbstractC27091Fz.A0H(r62, r72, AnonymousClass1G8.A05);
                                this.A01 = r0;
                                if (r1 != null) {
                                    this.A01 = (AnonymousClass1G8) AbstractC27091Fz.A0C(r1, r0);
                                }
                                this.A00 |= 1;
                            } else if (!A0a(r62, A032)) {
                                break;
                            }
                        } catch (IOException e) {
                            throw AbstractC27091Fz.A0K(this, e);
                        }
                    } catch (C28971Pt e2) {
                        throw AbstractC27091Fz.A0J(e2, this);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C56952mE();
            case 5:
                return new C81853ug();
            case 6:
                break;
            case 7:
                if (A03 == null) {
                    synchronized (C56952mE.class) {
                        if (A03 == null) {
                            A03 = AbstractC27091Fz.A09(A02);
                        }
                    }
                }
                return A03;
            default:
                throw C12970iu.A0z();
        }
        return A02;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            AnonymousClass1G8 r0 = this.A01;
            if (r0 == null) {
                r0 = AnonymousClass1G8.A05;
            }
            i2 = AbstractC27091Fz.A08(r0, 1, 0);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            AnonymousClass1G8 r0 = this.A01;
            if (r0 == null) {
                r0 = AnonymousClass1G8.A05;
            }
            codedOutputStream.A0L(r0, 1);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
