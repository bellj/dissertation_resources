package X;

import android.graphics.Bitmap;
import android.widget.ImageView;
import com.whatsapp.voipcalling.VoipActivityV2;

/* renamed from: X.54y  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1102854y implements AnonymousClass28F {
    public final /* synthetic */ VoipActivityV2 A00;

    public C1102854y(VoipActivityV2 voipActivityV2) {
        this.A00 = voipActivityV2;
    }

    @Override // X.AnonymousClass28F
    public void Adh(Bitmap bitmap, ImageView imageView, boolean z) {
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000b, code lost:
        if (r1 == false) goto L_0x000d;
     */
    @Override // X.AnonymousClass28F
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void Adv(android.widget.ImageView r3) {
        /*
            r2 = this;
            com.whatsapp.voipcalling.CallInfo r0 = com.whatsapp.voipcalling.Voip.getCallInfo()
            if (r0 == 0) goto L_0x000d
            boolean r1 = r0.videoEnabled
            r0 = 2131231143(0x7f0801a7, float:1.8078359E38)
            if (r1 != 0) goto L_0x0010
        L_0x000d:
            r0 = 2131231144(0x7f0801a8, float:1.807836E38)
        L_0x0010:
            r3.setImageResource(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C1102854y.Adv(android.widget.ImageView):void");
    }
}
