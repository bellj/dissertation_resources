package X;

/* renamed from: X.4Dh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C87864Dh {
    public static int A00(AnonymousClass28D r1) {
        String A0I = r1.A0I(42);
        if (A0I == null || A0I.equals("column")) {
            return 1;
        }
        if (A0I.equals("row")) {
            return 0;
        }
        throw C12970iu.A0f(C12960it.A0d(A0I, C12960it.A0k("Unknown direction: ")));
    }
}
