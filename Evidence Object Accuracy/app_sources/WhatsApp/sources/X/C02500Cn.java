package X;

/* renamed from: X.0Cn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02500Cn extends AbstractC08750bn {
    public final /* synthetic */ C08740bm A00;

    public C02500Cn(C08740bm r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC08750bn
    public String A04() {
        C05380Pi r2 = (C05380Pi) this.A00.A01.get();
        if (r2 == null) {
            return "Completer object has been garbage collected, future will fail soon";
        }
        StringBuilder sb = new StringBuilder("tag=[");
        sb.append(r2.A02);
        sb.append("]");
        return sb.toString();
    }
}
