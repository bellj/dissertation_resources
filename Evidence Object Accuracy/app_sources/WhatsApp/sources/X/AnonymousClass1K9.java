package X;

import android.net.Uri;
import java.io.File;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

/* renamed from: X.1K9  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1K9 {
    public final C14380lL A00;
    public final AnonymousClass1qQ A01;
    public final C14480lV A02;
    public final String A03;

    public AnonymousClass1K9(C14380lL r1, AnonymousClass1qQ r2, C14480lV r3, String str) {
        this.A03 = str;
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = r1;
    }

    public static AnonymousClass1K9 A00(Uri uri, C14390lM r30, AnonymousClass1KA r31, C14480lV r32, C14370lK r33, AnonymousClass1KB r34, String str, int i, boolean z, boolean z2, boolean z3, boolean z4) {
        AnonymousClass1qQ r4 = new AnonymousClass1qQ(r31, r33, r34, null, null, uri.toString(), null, null, 0, i, 0, 0, z, z2, z2, false, false);
        boolean z5 = false;
        if (i == 3) {
            z5 = true;
        }
        return new AnonymousClass1K9(new C14380lL(r30, null, r33, null, null, null, "optimistic", null, i, 0, 0, true, z3, z4, z5), r4, r32, str);
    }

    public static AnonymousClass1K9 A01(C15570nT r37, C38421o4 r38, C26471Dp r39, C239713s r40, C14480lV r41, boolean z) {
        boolean z2;
        AnonymousClass1KB r9;
        boolean z3;
        C14390lM r7;
        int[] iArr;
        C16150oX r0 = r38.A00().A02;
        AnonymousClass009.A05(r0);
        String str = r0.A0I;
        C89494Ke r3 = new Object() { // from class: X.4Ke
        };
        CopyOnWriteArrayList copyOnWriteArrayList = r38.A01;
        Iterator it = copyOnWriteArrayList.iterator();
        while (true) {
            if (!it.hasNext()) {
                z2 = false;
                break;
            }
            if (C239713s.this.A07((AbstractC16130oV) it.next())) {
                z2 = true;
                break;
            }
        }
        AbstractC16130oV A00 = r38.A00();
        boolean A04 = r38.A04();
        C16150oX r4 = A00.A02;
        AnonymousClass009.A05(r4);
        if (A00 instanceof C30061Vy) {
            r9 = ((C30061Vy) A00).A01;
        } else {
            r9 = null;
        }
        byte b = A00.A0y;
        AnonymousClass1KA A002 = r39.A00(b, A04);
        String str2 = r4.A0H;
        AnonymousClass1qQ r42 = new AnonymousClass1qQ(A002, C14370lK.A01(b, ((AbstractC15340mz) A00).A08), r9, r4.A0F, str2, A00.A16(), A00.A05, A00.A06, r4.A05, ((AbstractC15340mz) A00).A08, r4.A0D, r4.A0E, A04, z2, !r4.A0O, z, r4.A0N);
        AbstractC16130oV A003 = r38.A00();
        Iterator it2 = copyOnWriteArrayList.iterator();
        while (true) {
            if (it2.hasNext()) {
                if (!C30041Vv.A0P(r37, (AbstractC15340mz) it2.next())) {
                    z3 = true;
                    break;
                }
            } else {
                z3 = false;
                break;
            }
        }
        C16150oX r2 = A003.A02;
        AnonymousClass009.A05(r2);
        long j = A003.A0I;
        byte[] bArr = r2.A0U;
        long j2 = r2.A0B;
        if (bArr == null) {
            r7 = null;
        } else {
            if (j2 <= 0) {
                j2 = j;
            }
            r7 = new C14390lM(bArr, j2);
        }
        C38101nW A14 = A003.A14();
        C14370lK A01 = C14370lK.A01(A003.A0y, ((AbstractC15340mz) A003).A08);
        File file = r2.A0F;
        long j3 = A003.A01;
        String str3 = A003.A05;
        String str4 = A003.A04;
        int i = ((AbstractC15340mz) A003).A08;
        if (A14 != null) {
            iArr = A14.A06();
        } else {
            iArr = null;
        }
        return new AnonymousClass1K9(new C14380lL(r7, null, A01, file, str3, str4, "mms", iArr, i, r2.A04, j3, z3, true, false, C30041Vv.A0i(A003)), r42, r41, str);
    }
}
