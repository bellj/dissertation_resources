package X;

/* renamed from: X.4HW  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4HW {
    public static final AbstractC77683ng A00;
    public static final AnonymousClass4DN A01;
    @Deprecated
    public static final AnonymousClass1UE A02;
    @Deprecated
    public static final AnonymousClass5R3 A03 = new AnonymousClass517();

    static {
        AnonymousClass4DN r3 = new AnonymousClass4DN();
        A01 = r3;
        C77673nf r2 = new C77673nf();
        A00 = r2;
        A02 = new AnonymousClass1UE(r2, r3, "Wearable.API");
    }
}
