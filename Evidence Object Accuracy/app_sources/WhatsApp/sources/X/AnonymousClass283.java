package X;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ScrollView;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import com.facebook.redex.RunnableBRunnable0Shape0S0600000_I0;
import com.facebook.redex.RunnableBRunnable0Shape14S0100000_I1;
import com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3;
import com.facebook.redex.ViewOnClickCListenerShape0S0100000_I0;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaButton;
import com.whatsapp.WaTextView;
import com.whatsapp.biz.catalog.view.CatalogCarouselDetailImageView;
import com.whatsapp.biz.catalog.view.CatalogMediaCard;
import com.whatsapp.biz.catalog.view.EllipsizedTextEmojiLabel;
import com.whatsapp.biz.catalog.view.PostcodeChangeBottomSheet;
import com.whatsapp.biz.catalog.view.widgets.QuantitySelector;
import com.whatsapp.biz.product.view.fragment.ProductMoreInfoFragment;
import com.whatsapp.components.Button;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.ViewOnClickCListenerShape13S0100000_I0;
import com.whatsapp.util.ViewOnClickCListenerShape4S0200000_I0;
import java.io.File;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.283  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass283 extends AnonymousClass284 implements AnonymousClass285 {
    public int A00 = 0;
    public int A01;
    public ObjectAnimator A02;
    public View A03;
    public View A04;
    public View A05;
    public ViewTreeObserver.OnScrollChangedListener A06;
    public ScrollView A07;
    public TextView A08;
    public TextView A09;
    public TextView A0A;
    public C34271fr A0B;
    public C48882Ih A0C;
    public TextEmojiLabel A0D;
    public TextEmojiLabel A0E;
    public C16170oZ A0F;
    public WaButton A0G;
    public WaTextView A0H;
    public C14650lo A0I;
    public C25831Az A0J;
    public C25791Av A0K;
    public C21770xx A0L;
    public C53852fO A0M;
    public AnonymousClass19N A0N;
    public AnonymousClass1FZ A0O;
    public C19850um A0P;
    public C44691zO A0Q;
    public AnonymousClass19Q A0R;
    public AnonymousClass19T A0S;
    public C252918v A0T;
    public C37071lG A0U;
    public CatalogCarouselDetailImageView A0V;
    public CatalogMediaCard A0W;
    public EllipsizedTextEmojiLabel A0X;
    public PostcodeChangeBottomSheet A0Y;
    public QuantitySelector A0Z;
    public C53912fi A0a;
    public Button A0b;
    public C15550nR A0c;
    public C26311Cv A0d;
    public C22700zV A0e;
    public C20020v5 A0f;
    public C17170qN A0g;
    public UserJid A0h;
    public C19840ul A0i;
    public String A0j;
    public String A0k;
    public String A0l;
    public String A0m;
    public boolean A0n;
    public boolean A0o;
    public final AnonymousClass4UV A0p = new C84443zJ(this);

    public static void A02(Context context, Intent intent, UserJid userJid, Integer num, Integer num2, String str, int i, boolean z) {
        Bundle A03;
        Activity A01 = AbstractC35731ia.A01(context, ActivityC000800j.class);
        if (A01 != null) {
            intent.putExtra("product", str);
            intent.putExtra("disable_report", z);
            intent.putExtra("jid", userJid.getRawString());
            if (num2 != null) {
                intent.putExtra("thumb_height", num2);
            }
            if (num != null) {
                intent.putExtra("thumb_width", num);
            }
            intent.putExtra("view_product_origin", i);
            if (!AnonymousClass3I4.A00) {
                A03 = null;
            } else {
                A03 = C018108l.A02(A01, new AnonymousClass01T[0]).A03();
            }
            A01.startActivityForResult(intent, 0, A03);
        }
    }

    public static /* synthetic */ void A03(AnonymousClass283 r8, boolean z) {
        View view;
        if (z && (view = ((ActivityC13810kN) r8).A00) != null) {
            C34271fr r4 = r8.A0B;
            if (r4 == null || Build.VERSION.SDK_INT <= 18) {
                r4 = C34271fr.A00(view, r8.getResources().getString(R.string.item_added_to_cart), 0);
                r4.A07(r4.A02.getText(R.string.view_cart), new ViewOnClickCListenerShape13S0100000_I0(r8, 22));
                r8.A0B = r4;
            }
            if (!r4.A05()) {
                if (Build.VERSION.SDK_INT <= 18) {
                    new Handler().postDelayed(new RunnableBRunnable0Shape3S0100000_I0_3(r8, 14), 200);
                } else {
                    r8.A0B.A03();
                }
            }
            if (r8.A02 == null) {
                ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(r8.findViewById(R.id.menu_cart), PropertyValuesHolder.ofFloat("scaleX", 1.5f), PropertyValuesHolder.ofFloat("scaleY", 1.5f));
                r8.A02 = ofPropertyValuesHolder;
                ofPropertyValuesHolder.setDuration(180L);
                r8.A02.setRepeatMode(2);
                r8.A02.setRepeatCount(1);
            }
            if (!r8.A02.isRunning()) {
                r8.A02.start();
            }
            r8.A0i.A06("cart_add_tag", true);
            for (AnonymousClass4UV r0 : r8.A0K.A01()) {
                r0.A00();
            }
        }
    }

    public void A2e() {
        this.A0M.A00.A05(this, new AnonymousClass02B() { // from class: X.4sh
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                AnonymousClass283 r1 = AnonymousClass283.this;
                if (C12970iu.A1Y(obj)) {
                    r1.updateButton(r1.A03);
                }
            }
        });
        updateButton(this.A0G);
    }

    public final void A2f() {
        if (!this.A0n) {
            this.A0R.A01(this.A0h, null, (Boolean) this.A0M.A00.A01(), 31, null, null, this.A0l, null, this.A0k, this.A0m, 12);
            this.A0n = true;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0037, code lost:
        if (r6 == 404) goto L_0x0039;
     */
    @Override // X.AnonymousClass285
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AQP(java.lang.String r5, int r6) {
        /*
            r4 = this;
            r4.A2f()
            r0 = 3
            r4.A00 = r0
            X.1FZ r0 = r4.A0O
            java.lang.Iterable r0 = r0.A01()
            java.util.Iterator r3 = r0.iterator()
        L_0x0010:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x0045
            java.lang.Object r1 = r3.next()
            X.2SH r1 = (X.AnonymousClass2SH) r1
            boolean r0 = r1 instanceof X.C59152u4
            if (r0 == 0) goto L_0x0010
            X.2u4 r1 = (X.C59152u4) r1
            com.whatsapp.biz.product.view.activity.ProductDetailActivity r2 = r1.A00
            X.1zO r0 = r2.A0Q
            if (r0 == 0) goto L_0x0030
            java.lang.String r0 = r0.A0D
            boolean r0 = r0.equals(r5)
            if (r0 == 0) goto L_0x0010
        L_0x0030:
            r0 = 406(0x196, float:5.69E-43)
            if (r6 == r0) goto L_0x0039
            r1 = 404(0x194, float:5.66E-43)
            r0 = 3
            if (r6 != r1) goto L_0x003a
        L_0x0039:
            r0 = 2
        L_0x003a:
            r2.A00 = r0
            X.0um r0 = r2.A0P
            r0.A0G(r5)
            r2.A2e()
            goto L_0x0010
        L_0x0045:
            X.0ul r2 = r4.A0i
            java.lang.String r1 = "view_product_tag"
            r0 = 0
            r2.A06(r1, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass283.AQP(java.lang.String, int):void");
    }

    @Override // X.AnonymousClass285
    public void AQQ(AnonymousClass4TA r4, String str) {
        this.A00 = 0;
        for (AnonymousClass2SH r0 : this.A0O.A01()) {
            r0.A00(str);
        }
        this.A0i.A06("view_product_tag", true);
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        C44691zO r1;
        super.onActivityResult(i, i2, intent);
        if (i2 == -1 && (r1 = this.A0Q) != null) {
            if (i == 3) {
                List A07 = C15380n4.A07(AbstractC14640lm.class, intent.getStringArrayListExtra("jids"));
                File file = new File(intent.getStringExtra("file_path"));
                C16170oZ r8 = this.A0F;
                r8.A1Q.Ab2(new RunnableBRunnable0Shape0S0600000_I0(Uri.fromFile(file), r8, this.A0Q, this.A0h, null, A07));
                if (A07.size() == 1) {
                    ((ActivityC13790kL) this).A00.A07(this, new C14960mK().A0g(this, this.A0c.A0B((AbstractC14640lm) A07.get(0))));
                } else {
                    A2a(A07);
                }
            } else if (i == 66) {
                this.A0N.A02(this, this.A0U, null, this.A0h, Collections.singletonList(r1), 3, 0, 0);
            }
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        String str;
        AnonymousClass3I4.A00(this);
        super.onCreate(bundle);
        this.A0i.A01(774775607, "view_product_tag", "ProductBaseActivity");
        this.A01 = getIntent().getIntExtra("view_product_origin", 0);
        this.A0K.A03(this.A0p);
        UserJid nullable = UserJid.getNullable(getIntent().getStringExtra("jid"));
        AnonymousClass009.A05(nullable);
        this.A0h = nullable;
        String stringExtra = getIntent().getStringExtra("product");
        AnonymousClass009.A05(stringExtra);
        this.A0l = stringExtra;
        this.A0o = getIntent().getBooleanExtra("disable_report", false);
        this.A0k = getIntent().getStringExtra("collection_index");
        this.A0m = getIntent().getStringExtra("product_index");
        setContentView(R.layout.business_product_catalog_detail);
        this.A0V = (CatalogCarouselDetailImageView) findViewById(R.id.catalog_carousel_detail_image_view);
        this.A0E = (TextEmojiLabel) findViewById(R.id.catalog_detail_title);
        this.A09 = (TextView) findViewById(R.id.catalog_detail_price);
        this.A0X = (EllipsizedTextEmojiLabel) findViewById(R.id.catalog_detail_description);
        this.A08 = (TextView) findViewById(R.id.catalog_detail_link);
        this.A0A = (TextView) findViewById(R.id.catalog_detail_sku);
        this.A0D = (TextEmojiLabel) findViewById(R.id.loading_product_text);
        this.A0W = (CatalogMediaCard) findViewById(R.id.product_message_catalog_media_card);
        this.A0H = (WaTextView) findViewById(R.id.product_availability_label);
        this.A0b = (Button) findViewById(R.id.pdp_action_button);
        this.A07 = (ScrollView) findViewById(R.id.pdp_scroll_view);
        this.A05 = findViewById(R.id.shadow_bottom);
        View findViewById = findViewById(R.id.quantity_selector_cart_container);
        this.A03 = findViewById;
        this.A06 = new ViewTreeObserver.OnScrollChangedListener() { // from class: X.4oW
            @Override // android.view.ViewTreeObserver.OnScrollChangedListener
            public final void onScrollChanged() {
                AnonymousClass283 r4 = AnonymousClass283.this;
                int i = 0;
                int bottom = r4.A07.getChildAt(0).getBottom();
                int height = r4.A07.getHeight() + r4.A07.getScrollY();
                View view = r4.A05;
                if (bottom <= height) {
                    i = 8;
                }
                view.setVisibility(i);
            }
        };
        findViewById.setVisibility(8);
        QuantitySelector quantitySelector = (QuantitySelector) findViewById(R.id.pdp_quantity_selector);
        this.A0Z = quantitySelector;
        quantitySelector.A04 = new AnonymousClass5TT() { // from class: X.3VQ
            @Override // X.AnonymousClass5TT
            public final void ARm(long j) {
                AnonymousClass283 r0 = AnonymousClass283.this;
                C12960it.A0w(r0.A0b, ((ActivityC13830kP) r0).A01, j);
            }
        };
        quantitySelector.A05 = new AnonymousClass5TU() { // from class: X.53v
            @Override // X.AnonymousClass5TU
            public final void AUU(long j) {
                AnonymousClass283 r0 = AnonymousClass283.this;
                r0.A0a.A04(r0.A0Q, r0.A0h, r0.A0k, r0.A0m, j);
            }
        };
        View findViewById2 = findViewById(R.id.request_report_btn_container);
        this.A04 = findViewById2;
        findViewById2.setVisibility(8);
        findViewById(R.id.report_btn).setOnClickListener(new ViewOnClickCListenerShape0S0100000_I0(this, 41));
        ViewOnClickCListenerShape13S0100000_I0 viewOnClickCListenerShape13S0100000_I0 = new ViewOnClickCListenerShape13S0100000_I0(this, 19);
        WaButton waButton = (WaButton) findViewById(R.id.message_business_btn);
        this.A0G = waButton;
        waButton.setVisibility(8);
        this.A0G.setOnClickListener(viewOnClickCListenerShape13S0100000_I0);
        Toolbar toolbar = (Toolbar) AnonymousClass00T.A05(this, R.id.product_detail_image_toolbar);
        toolbar.setTitle("");
        toolbar.A07();
        A1e(toolbar);
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0M(true);
        }
        toolbar.setNavigationIcon(new AnonymousClass2GF(AnonymousClass00T.A04(this, R.drawable.ic_back_shadow), ((ActivityC13830kP) this).A01));
        this.A0Q = this.A0P.A05(this.A0h, this.A0l);
        C37071lG r1 = this.A0U;
        if (r1 != null) {
            r1.A00();
        }
        this.A0U = new C37071lG(this.A0T);
        this.A0S.A0M.add(this);
        if (this.A01 == 6) {
            ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape3S0100000_I0_3(this, 13));
        }
        this.A0M = (C53852fO) new AnonymousClass02A(new AnonymousClass3RX(this.A0C, this.A0h), this).A00(C53852fO.class);
        C30101Wc A00 = this.A0I.A00(((ActivityC13810kN) this).A03, this.A0h, null);
        UserJid userJid = this.A0h;
        AbstractC14440lR r2 = ((ActivityC13830kP) this).A05;
        C68263Us r7 = new C68263Us(this.A0I, A00, this.A0L, this.A0R, userJid, r2);
        AnonymousClass4E0 r8 = new AnonymousClass4E0();
        C53912fi r12 = (C53912fi) new AnonymousClass02A(new C105644uO(((ActivityC13810kN) this).A06, this.A0S, r7, r8, ((ActivityC13810kN) this).A07, ((ActivityC13810kN) this).A09, userJid, this.A01), this).A00(C53912fi.class);
        this.A0a = r12;
        r12.A09.A05(this, new AnonymousClass02B() { // from class: X.4sg
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                AnonymousClass283.A03(AnonymousClass283.this, C12970iu.A1Y(obj));
            }
        });
        this.A0a.A06.A05(this, new AnonymousClass02B() { // from class: X.3Py
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                View view;
                AnonymousClass283 r4 = AnonymousClass283.this;
                if (C12970iu.A1Y(obj) && (view = ((ActivityC13810kN) r4).A00) != null) {
                    C34271fr.A00(view, r4.getResources().getString(R.string.catalog_something_went_wrong_error), 0).A03();
                    r4.A0i.A06("cart_add_tag", false);
                }
            }
        });
        this.A0a.A08.A05(this, new AnonymousClass02B() { // from class: X.3Pz
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                View view;
                AnonymousClass283 r4 = AnonymousClass283.this;
                if (C12970iu.A1Y(obj) && (view = ((ActivityC13810kN) r4).A00) != null) {
                    C34271fr A002 = C34271fr.A00(view, r4.getResources().getString(R.string.cant_add_more_items), -2);
                    A002.A07(A002.A02.getText(R.string.ok), new ViewOnClickCListenerShape4S0200000_I0(A002, 13, r4));
                    A002.A03();
                    r4.A0i.A06("cart_add_tag", false);
                }
            }
        });
        this.A0a.A04.A05(this, new AnonymousClass02B() { // from class: X.3Q0
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                long j;
                long j2;
                Button button;
                int i;
                AnonymousClass283 r5 = AnonymousClass283.this;
                List list = (List) obj;
                r5.A0j = r5.A0M.A04(((ActivityC13830kP) r5).A01, list);
                TextView A0M = C12970iu.A0M(r5, R.id.cart_total_quantity);
                if (A0M != null) {
                    A0M.setText(r5.A0j);
                }
                C44691zO r0 = r5.A0Q;
                if (r0 != null) {
                    j = r0.A08;
                } else {
                    j = 99;
                }
                String str2 = r5.A0l;
                Iterator it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        j2 = 0;
                        break;
                    }
                    C92584Wm r3 = (C92584Wm) it.next();
                    if (r3.A01.A0D.equals(str2)) {
                        j2 = r3.A00;
                        break;
                    }
                }
                r5.A0Z.A04(j2, j);
                int i2 = (j2 > 0 ? 1 : (j2 == 0 ? 0 : -1));
                QuantitySelector quantitySelector2 = r5.A0Z;
                if (i2 > 0) {
                    quantitySelector2.setVisibility(0);
                    r5.A0b.setText(C12960it.A0X(r5, r5.A0j, C12970iu.A1b(), 0, R.string.product_list_view_cart));
                    button = r5.A0b;
                    i = 42;
                } else {
                    quantitySelector2.setVisibility(8);
                    C12970iu.A19(r5, r5.A0b, R.string.catalog_product_add_to_cart);
                    button = r5.A0b;
                    i = 43;
                }
                button.setOnClickListener(new ViewOnClickCListenerShape0S0100000_I0(r5, i));
            }
        });
        boolean z = true;
        this.A0i.A05("view_product_tag", "IsConsumer", !((ActivityC13790kL) this).A01.A0F(this.A0h));
        C19840ul r22 = this.A0i;
        if (this.A0Q == null) {
            z = false;
        }
        r22.A05("view_product_tag", "Cached", z);
        int i = this.A01;
        switch (i) {
            case 1:
            case 7:
                str = "Message";
                break;
            case 2:
                str = "EditProduct";
                break;
            case 3:
                str = "Catalog";
                break;
            case 4:
                str = "ContactInfo";
                break;
            case 5:
                str = "Product";
                break;
            case 6:
                str = "Deeplink";
                break;
            case 8:
                str = "Cart";
                break;
            case 9:
                str = "Order";
                break;
            default:
                StringBuilder sb = new StringBuilder("ProductDetailActivity/startViewProductQpl/Unexpected value: ");
                sb.append(i);
                throw new IllegalStateException(sb.toString());
        }
        this.A0i.A04("view_product_tag", "EntryPoint", str);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0045, code lost:
        if (r1.startsWith("91") == false) goto L_0x0047;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0030, code lost:
        if (r1 == false) goto L_0x0032;
     */
    @Override // X.ActivityC13790kL, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onCreateOptionsMenu(android.view.Menu r8) {
        /*
            r7 = this;
            X.2fi r2 = r7.A0a
            int r1 = r7.A00
            X.1zO r0 = r7.A0Q
            boolean r6 = r2.A06(r0, r1)
            com.whatsapp.jid.UserJid r0 = r7.A0h
            boolean r1 = r0 instanceof X.C27631Ih
            r0 = 2131364348(0x7f0a09fc, float:1.834853E38)
            android.view.MenuItem r5 = r8.findItem(r0)
            r0 = 2131364359(0x7f0a0a07, float:1.8348553E38)
            android.view.MenuItem r4 = r8.findItem(r0)
            r0 = 2131364351(0x7f0a09ff, float:1.8348537E38)
            android.view.MenuItem r3 = r8.findItem(r0)
            r0 = 2131364356(0x7f0a0a04, float:1.8348547E38)
            android.view.MenuItem r2 = r8.findItem(r0)
            r3.setVisible(r6)
            if (r6 == 0) goto L_0x0032
            r0 = 1
            if (r1 != 0) goto L_0x0033
        L_0x0032:
            r0 = 0
        L_0x0033:
            r4.setVisible(r0)
            com.whatsapp.jid.UserJid r0 = r7.A0h
            java.lang.String r1 = X.C248917h.A03(r0)
            if (r1 == 0) goto L_0x0047
            java.lang.String r0 = "91"
            boolean r1 = r1.startsWith(r0)
            r0 = 1
            if (r1 != 0) goto L_0x0048
        L_0x0047:
            r0 = 0
        L_0x0048:
            r2.setVisible(r0)
            r0 = 2131559282(0x7f0d0372, float:1.8743904E38)
            r5.setActionView(r0)
            android.view.View r0 = r5.getActionView()
            X.AnonymousClass23N.A01(r0)
            android.view.View r2 = r5.getActionView()
            r1 = 21
            com.whatsapp.util.ViewOnClickCListenerShape13S0100000_I0 r0 = new com.whatsapp.util.ViewOnClickCListenerShape13S0100000_I0
            r0.<init>(r7, r1)
            r2.setOnClickListener(r0)
            android.view.View r1 = r5.getActionView()
            r0 = 2131362564(0x7f0a0304, float:1.8344912E38)
            android.view.View r1 = r1.findViewById(r0)
            android.widget.TextView r1 = (android.widget.TextView) r1
            java.lang.String r0 = r7.A0j
            if (r0 == 0) goto L_0x007a
            r1.setText(r0)
        L_0x007a:
            X.2fO r0 = r7.A0M
            X.016 r1 = r0.A00
            X.4uD r0 = new X.4uD
            r0.<init>(r3, r4, r5, r7)
            r1.A05(r7, r0)
            boolean r0 = super.onCreateOptionsMenu(r8)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass283.onCreateOptionsMenu(android.view.Menu):boolean");
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        this.A0K.A04(this.A0p);
        CatalogMediaCard catalogMediaCard = this.A0W;
        if (catalogMediaCard != null) {
            catalogMediaCard.A02();
        }
        this.A0S.A0M.remove(this);
        C37071lG r0 = this.A0U;
        if (r0 != null) {
            r0.A00();
        }
        this.A0i.A06("view_product_tag", false);
        this.A0i.A06("cart_add_tag", false);
        super.onDestroy();
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (16908332 == menuItem.getItemId()) {
            onBackPressed();
        } else if (R.id.menu_more_info == menuItem.getItemId()) {
            if (!A2R()) {
                Adm(ProductMoreInfoFragment.A00(this.A0h, this.A0l));
                return true;
            }
        } else if (R.id.menu_forward == menuItem.getItemId()) {
            C53912fi r2 = this.A0a;
            int i = this.A00;
            C44691zO r1 = this.A0Q;
            if (r2.A06(r1, i)) {
                this.A0N.A02(this, this.A0U, null, this.A0h, Collections.singletonList(r1), 3, 0, 0);
                return true;
            }
        } else if (R.id.menu_share != menuItem.getItemId()) {
            return super.onOptionsItemSelected(menuItem);
        } else {
            UserJid userJid = this.A0h;
            String str = this.A0l;
            Intent intent = new Intent();
            intent.setClassName(getPackageName(), "com.whatsapp.ShareProductLinkActivity");
            intent.setAction("android.intent.action.VIEW");
            intent.putExtra("jid", userJid.getRawString());
            intent.putExtra("product_id", str);
            startActivity(intent);
            return true;
        }
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        if (this.A06 != null) {
            this.A07.getViewTreeObserver().removeOnScrollChangedListener(this.A06);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        A2e();
        C68263Us r3 = this.A0a.A0D;
        r3.A0A.Ab2(new RunnableBRunnable0Shape14S0100000_I1(r3, 36));
        if (this.A06 != null) {
            this.A07.getViewTreeObserver().addOnScrollChangedListener(this.A06);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        super.onStart();
        this.A0S.A07(new AnonymousClass4TA(this.A0h, Integer.valueOf(getIntent().getIntExtra("thumb_width", (int) getResources().getDimension(R.dimen.medium_thumbnail_size))), Integer.valueOf(getIntent().getIntExtra("thumb_height", (int) getResources().getDimension(R.dimen.medium_thumbnail_size))), this.A0l, this.A0R.A00, false));
        if (this.A0Q == null) {
            this.A00 = 1;
        }
    }

    public void updateButton(View view) {
        int i = 8;
        if (this.A0a.A06(this.A0Q, this.A00)) {
            i = 0;
        }
        view.setVisibility(i);
    }
}
