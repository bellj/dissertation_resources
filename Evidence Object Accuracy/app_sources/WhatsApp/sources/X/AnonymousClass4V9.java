package X;

/* renamed from: X.4V9  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4V9 {
    public final C94524by A00;

    public AnonymousClass4V9(C94524by r1) {
        this.A00 = r1;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("Removed[");
        A0k.append(this.A00);
        return C72453ed.A0t(A0k);
    }
}
