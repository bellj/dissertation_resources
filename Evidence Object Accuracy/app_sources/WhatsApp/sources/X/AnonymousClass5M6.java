package X;

/* renamed from: X.5M6  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5M6 extends AnonymousClass4YT {
    public static final int[] A0n = {0, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 2, 2, 1, 1, 1, 0, 0, 1, 2, 1, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, -1, 0, -1, -1, -1, -1, -1, -2, -1, -2, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3, -4, -3, -4, -3, -3, -3, -3, -1, -2, 1, 1, 1, 2, 2, 2, 0, -1, -2, -1, -2, -1, -2, -1, -2, -1, -2, -1, -2, -1, -2, -1, -2, -1, -2, -1, -2, 0, 0, 0, 0, -1, -1, -1, -1, -1, -1, -1, -2, -1, -2, -1, -2, 0, 1, 0, 1, -1, -1, 0, 0, 1, 1, -1, 0, -1, 0, 0, 0, -3, -1, -1, -3, -3, -1, -1, -1, -1, -1, -1, -2, -2, -2, -2, -2, -2, -2, -2, 0, 1, 0, -1, -1, -1, -2, -1, -2, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, -1, -1, 0, 0};
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public int A09;
    public int A0A;
    public int A0B;
    public int A0C;
    public int A0D;
    public C95474dn A0E;
    public C95474dn A0F;
    public C95474dn A0G;
    public C95474dn A0H;
    public C95474dn A0I;
    public C95474dn A0J;
    public C94914ck A0K;
    public C95044cz A0L;
    public C95044cz A0M;
    public C95044cz A0N;
    public C95044cz A0O;
    public C95044cz A0P;
    public C95044cz A0Q;
    public C94854ce A0R;
    public C94854ce A0S;
    public C94464br A0T;
    public C94464br A0U;
    public C94464br A0V;
    public boolean A0W;
    public boolean A0X;
    public int[] A0Y;
    public int[] A0Z;
    public C95474dn[] A0a;
    public C95474dn[] A0b;
    public final int A0c;
    public final int A0d;
    public final int A0e;
    public final int A0f;
    public final int A0g;
    public final int A0h;
    public final String A0i;
    public final String A0j;
    public final C95044cz A0k = new C95044cz();
    public final AnonymousClass4YW A0l;
    public final int[] A0m;

    public AnonymousClass5M6(String str, String str2, String str3, AnonymousClass4YW r8, String[] strArr, int i, int i2) {
        int A02;
        int length;
        this.A0l = r8;
        this.A0c = "<init>".equals(str) ? 262144 | i : i;
        this.A0f = r8.A02(str);
        this.A0j = str;
        this.A0e = r8.A02(str2);
        this.A0i = str2;
        if (str3 == null) {
            A02 = 0;
        } else {
            A02 = r8.A02(str3);
        }
        this.A0h = A02;
        if (strArr != null && (length = strArr.length) > 0) {
            this.A0g = length;
            this.A0m = new int[length];
            for (int i3 = 0; i3 < this.A0g; i3++) {
                this.A0m[i3] = r8.A0A(strArr[i3], 7).A03;
            }
        }
        this.A0d = i2;
        if (i2 != 0) {
            int A00 = C95574dz.A00(str2) >> 2;
            A00 = (i & 8) != 0 ? A00 - 1 : A00;
            this.A06 = A00;
            this.A00 = A00;
            C94464br r0 = new C94464br();
            this.A0U = r0;
            A08(r0);
        }
    }

    /*  JADX ERROR: JadxOverflowException in pass: LoopRegionVisitor
        jadx.core.utils.exceptions.JadxOverflowException: LoopRegionVisitor.assignOnlyInLoop endless recursion
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:57)
        	at jadx.core.utils.ErrorsCounter.error(ErrorsCounter.java:31)
        	at jadx.core.dex.attributes.nodes.NotificationAttrNode.addError(NotificationAttrNode.java:15)
        */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x01c4  */
    @Override // X.AnonymousClass4YT
    public void A03(int r14, int r15) {
        /*
        // Method dump skipped, instructions count: 513
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5M6.A03(int, int):void");
    }

    @Override // X.AnonymousClass4YT
    public void A0B(Object[] objArr, int i, Object[] objArr2, int i2, int i3) {
        C95044cz r2;
        int i4;
        int i5 = this.A0d;
        if (i5 != 4) {
            if (i5 == 3) {
                C94464br r0 = this.A0T;
                C95294dS r3 = r0.A02;
                if (r3 == null) {
                    AnonymousClass5M5 r32 = new AnonymousClass5M5(r0);
                    r0.A02 = r32;
                    r32.A0B(this.A0i, this.A0l, this.A0c, i2);
                } else if (i == -1) {
                    AnonymousClass4YW r6 = this.A0l;
                    int i6 = 0;
                    for (int i7 = 0; i7 < i2; i7++) {
                        int i8 = i6 + 1;
                        r3.A05[i6] = C95294dS.A00(objArr[i7], r6);
                        if (objArr[i7] == AbstractC116905Xj.A03 || objArr[i7] == AbstractC116905Xj.A00) {
                            i6 = i8 + 1;
                            r3.A05[i8] = 4194304;
                        } else {
                            i6 = i8;
                        }
                    }
                    while (true) {
                        int[] iArr = r3.A05;
                        if (i6 >= iArr.length) {
                            break;
                        }
                        iArr[i6] = 4194304;
                        i6++;
                    }
                    int i9 = 0;
                    for (int i10 = 0; i10 < i3; i10++) {
                        if (objArr2[i10] == AbstractC116905Xj.A03 || objArr2[i10] == AbstractC116905Xj.A00) {
                            i9++;
                        }
                    }
                    r3.A06 = new int[i9 + i3];
                    int i11 = 0;
                    for (int i12 = 0; i12 < i3; i12++) {
                        int i13 = i11 + 1;
                        r3.A06[i11] = C95294dS.A00(objArr2[i12], r6);
                        if (objArr2[i12] == AbstractC116905Xj.A03 || objArr2[i12] == AbstractC116905Xj.A00) {
                            i11 = i13 + 1;
                            r3.A06[i13] = 4194304;
                        } else {
                            i11 = i13;
                        }
                    }
                    r3.A03 = 0;
                    r3.A00 = 0;
                }
                this.A0T.A02.A0C(this);
            } else {
                if (i == -1) {
                    if (this.A0Z == null) {
                        String str = this.A0i;
                        C95294dS r22 = new C95294dS(new C94464br());
                        r22.A0B(str, this.A0l, this.A0c, C95574dz.A00(str) >> 2);
                        r22.A0C(this);
                    }
                    this.A00 = i2;
                    int A0C = A0C(this.A0k.A00, i2, i3);
                    for (int i14 = 0; i14 < i2; i14++) {
                        A0C++;
                        this.A0Y[A0C] = C95294dS.A00(objArr[i14], this.A0l);
                    }
                    for (int i15 = 0; i15 < i3; i15++) {
                        A0C++;
                        this.A0Y[A0C] = C95294dS.A00(objArr2[i15], this.A0l);
                    }
                    A0D();
                } else if (this.A0l.A03 >= 50) {
                    C95044cz r9 = this.A0Q;
                    if (r9 == null) {
                        r9 = new C95044cz();
                        this.A0Q = r9;
                        r2 = this.A0k;
                        i4 = r2.A00;
                    } else {
                        r2 = this.A0k;
                        i4 = (r2.A00 - this.A0A) - 1;
                        if (i4 < 0) {
                            if (i != 3) {
                                throw C72463ee.A0D();
                            }
                            return;
                        }
                    }
                    if (i == 0) {
                        this.A00 = i2;
                        r9.A02(255);
                        r9.A04(i4);
                        r9.A04(i2);
                        for (int i16 = 0; i16 < i2; i16++) {
                            A0G(objArr[i16]);
                        }
                        this.A0Q.A04(i3);
                        for (int i17 = 0; i17 < i3; i17++) {
                            A0G(objArr2[i17]);
                        }
                    } else if (i != 1) {
                        int i18 = 251;
                        if (i == 2) {
                            this.A00 -= i2;
                            i18 = 251 - i2;
                        } else if (i != 3) {
                            if (i == 4) {
                                if (i4 < 64) {
                                    r9.A02(i4 + 64);
                                } else {
                                    r9.A02(247);
                                    r9.A04(i4);
                                }
                                A0G(objArr2[0]);
                            } else {
                                throw C72453ed.A0h();
                            }
                        } else if (i4 < 64) {
                            r9.A02(i4);
                        }
                        r9.A02(i18);
                        r9.A04(i4);
                    } else {
                        this.A00 += i2;
                        r9.A02(i2 + 251);
                        r9.A04(i4);
                        for (int i19 = 0; i19 < i2; i19++) {
                            A0G(objArr[i19]);
                        }
                    }
                    this.A0A = r2.A00;
                    this.A0C++;
                } else {
                    throw C12970iu.A0f("Class versions V1_5 or less must use F_NEW frames.");
                }
                if (i5 == 2) {
                    this.A0B = i3;
                    int i20 = i3;
                    for (int i21 = 0; i21 < i3; i21++) {
                        if (objArr2[i21] == AbstractC116905Xj.A03 || objArr2[i21] == AbstractC116905Xj.A00) {
                            i20++;
                            this.A0B = i20;
                        }
                    }
                    if (i20 > this.A07) {
                        this.A07 = i20;
                    }
                }
            }
            this.A08 = Math.max(this.A08, i3);
            this.A06 = Math.max(this.A06, this.A00);
        }
    }

    public int A0C(int i, int i2, int i3) {
        int i4 = i2 + 3 + i3;
        int[] iArr = this.A0Y;
        if (iArr == null || iArr.length < i4) {
            iArr = new int[i4];
            this.A0Y = iArr;
        }
        iArr[0] = i;
        iArr[1] = i2;
        iArr[2] = i3;
        return 3;
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x008c A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0094 A[LOOP:0: B:32:0x008a->B:36:0x0094, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x009b  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00d8  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x005f A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0D() {
        /*
        // Method dump skipped, instructions count: 240
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5M6.A0D():void");
    }

    public final void A0E() {
        int i = this.A0d;
        if (i == 4) {
            C94464br r2 = new C94464br();
            r2.A02 = new C95294dS(r2);
            C95044cz r0 = this.A0k;
            r2.A03(r0.A01, r0.A00);
            this.A0V.A03 = r2;
            this.A0V = r2;
        } else if (i == 1) {
            this.A0T.A08 = (short) this.A07;
        } else {
            return;
        }
        this.A0T = null;
    }

    public final void A0F(int i, int i2) {
        String str;
        int i3;
        char c;
        while (i < i2) {
            AnonymousClass4YW r4 = this.A0l;
            int i4 = this.A0Y[i];
            C95044cz r3 = this.A0Q;
            int i5 = (-67108864 & i4) >> 26;
            if (i5 == 0) {
                int i6 = i4 & 1048575;
                int i7 = i4 & 62914560;
                if (i7 == 4194304) {
                    r3.A02(i6);
                    i++;
                } else if (i7 == 8388608) {
                    r3.A02(7);
                    str = r4.A09[i6].A08;
                } else if (i7 == 12582912) {
                    r3.A02(8);
                    i3 = (int) r4.A09[i6].A05;
                    r3.A04(i3);
                    i++;
                } else {
                    throw new AssertionError();
                }
            } else {
                StringBuilder A0h = C12960it.A0h();
                while (true) {
                    int i8 = i5 - 1;
                    if (i5 > 0) {
                        A0h.append('[');
                        i5 = i8;
                    } else {
                        if ((i4 & 62914560) == 8388608) {
                            A0h.append('L');
                            A0h.append(r4.A09[i4 & 1048575].A08);
                            c = ';';
                        } else {
                            int i9 = i4 & 1048575;
                            c = 'I';
                            if (i9 != 1) {
                                c = 'F';
                                if (i9 != 2) {
                                    c = 'D';
                                    if (i9 != 3) {
                                        if (i9 != 4) {
                                            switch (i9) {
                                                case 9:
                                                    c = 'Z';
                                                    break;
                                                case 10:
                                                    c = 'B';
                                                    break;
                                                case 11:
                                                    c = 'C';
                                                    break;
                                                case 12:
                                                    c = 'S';
                                                    break;
                                                default:
                                                    throw new AssertionError();
                                            }
                                        } else {
                                            c = 'J';
                                        }
                                    }
                                }
                            }
                        }
                        A0h.append(c);
                        r3.A02(7);
                        str = A0h.toString();
                    }
                }
            }
            i3 = r4.A0A(str, 7).A03;
            r3.A04(i3);
            i++;
        }
    }

    public final void A0G(Object obj) {
        int i;
        if (obj instanceof Integer) {
            this.A0Q.A02(C12960it.A05(obj));
            return;
        }
        boolean z = obj instanceof String;
        C95044cz r2 = this.A0Q;
        if (z) {
            r2.A02(7);
            i = this.A0l.A0A((String) obj, 7).A03;
        } else {
            r2.A02(8);
            i = ((C94464br) obj).A00;
        }
        r2.A04(i);
    }

    public final void A0H(C94464br r4, int i) {
        C94464br r2 = this.A0T;
        r2.A01 = new C91134Qo(r2.A01, r4, i);
    }

    public final void A0I(C94464br r6, C94464br[] r7) {
        C94464br r2 = this.A0T;
        if (r2 != null) {
            int i = this.A0d;
            if (i == 4) {
                r2.A02.A0D(null, null, 171, 0);
                A0H(r6, 0);
                C95294dS r0 = r6.A02;
                if (r0 != null) {
                    r6 = r0.A01;
                }
                r6.A05 = (short) (r6.A05 | 2);
                for (C94464br r1 : r7) {
                    A0H(r1, 0);
                    C95294dS r02 = r1.A02;
                    if (r02 != null) {
                        r1 = r02.A01;
                    }
                    r1.A05 = (short) (r1.A05 | 2);
                }
            } else if (i == 1) {
                int i2 = this.A0B - 1;
                this.A0B = i2;
                A0H(r6, i2);
                for (C94464br r03 : r7) {
                    A0H(r03, i2);
                }
            }
            A0E();
        }
    }
}
