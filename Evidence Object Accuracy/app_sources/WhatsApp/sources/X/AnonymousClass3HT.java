package X;

import android.graphics.Point;
import android.graphics.Rect;

/* renamed from: X.3HT  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3HT {
    public static final AnonymousClass3HT A03 = new AnonymousClass3HT(null, null, 0);
    public final int A00;
    public final Point A01;
    public final Rect A02;

    public AnonymousClass3HT(Point point, Rect rect, int i) {
        this.A00 = i;
        this.A02 = rect;
        this.A01 = point;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass3HT r5 = (AnonymousClass3HT) obj;
            if (this.A00 != r5.A00 || !C29941Vi.A00(this.A02, r5.A02) || !C29941Vi.A00(this.A01, r5.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] objArr = new Object[3];
        C12960it.A1O(objArr, this.A00);
        objArr[1] = this.A02;
        return C12980iv.A0B(this.A01, objArr, 2);
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("FlexState{separationType=");
        A0k.append(this.A00);
        A0k.append(", bounds=");
        A0k.append(this.A02);
        A0k.append(", parentDimensions=");
        A0k.append(this.A01);
        return C12970iu.A0v(A0k);
    }
}
