package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.msys.mci.DefaultCrypto;
import com.google.protobuf.CodedOutputStream;
import java.io.IOException;
import org.wawebrtc.MediaCodecVideoEncoder;

/* renamed from: X.1PB  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1PB extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass1PB A0e;
    public static volatile AnonymousClass255 A0f;
    public byte A00 = -1;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public long A09;
    public long A0A;
    public long A0B;
    public long A0C;
    public long A0D;
    public long A0E;
    public long A0F;
    public AbstractC27881Jp A0G;
    public AbstractC27881Jp A0H;
    public AnonymousClass1K6 A0I;
    public AnonymousClass1K6 A0J;
    public AnonymousClass1PH A0K;
    public C462425c A0L;
    public String A0M;
    public String A0N;
    public String A0O = "";
    public String A0P;
    public String A0Q;
    public String A0R;
    public String A0S;
    public String A0T;
    public boolean A0U;
    public boolean A0V;
    public boolean A0W;
    public boolean A0X;
    public boolean A0Y;
    public boolean A0Z;
    public boolean A0a;
    public boolean A0b;
    public boolean A0c;
    public boolean A0d;

    static {
        AnonymousClass1PB r0 = new AnonymousClass1PB();
        A0e = r0;
        r0.A0W();
    }

    public AnonymousClass1PB() {
        AnonymousClass277 r0 = AnonymousClass277.A01;
        this.A0I = r0;
        this.A0Q = "";
        this.A0R = "";
        this.A0P = "";
        this.A0S = "";
        this.A0J = r0;
        AbstractC27881Jp r02 = AbstractC27881Jp.A01;
        this.A0H = r02;
        this.A0G = r02;
        this.A0M = "";
        this.A0N = "";
        this.A0T = "";
    }

    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r21, Object obj, Object obj2) {
        C463525n r3;
        AnonymousClass1PI r32;
        switch (r21.ordinal()) {
            case 0:
                byte b = this.A00;
                if (b != 1) {
                    if (b != 0) {
                        boolean booleanValue = ((Boolean) obj).booleanValue();
                        if ((this.A01 & 1) == 1) {
                            int i = 0;
                            while (true) {
                                if (i >= this.A0I.size()) {
                                    for (int i2 = 0; i2 < this.A0J.size(); i2++) {
                                        if (((AbstractC27091Fz) this.A0J.get(i2)).A0Z()) {
                                        }
                                    }
                                    if (booleanValue) {
                                        this.A00 = 1;
                                        break;
                                    }
                                } else if (((AbstractC27091Fz) this.A0I.get(i)).A0Z()) {
                                    i++;
                                }
                            }
                        }
                        if (booleanValue) {
                            this.A00 = 0;
                        }
                    }
                    return null;
                }
                break;
            case 1:
                AbstractC462925h r13 = (AbstractC462925h) obj;
                AnonymousClass1PB r1 = (AnonymousClass1PB) obj2;
                boolean z = true;
                if ((this.A01 & 1) != 1) {
                    z = false;
                }
                String str = this.A0O;
                boolean z2 = true;
                if ((r1.A01 & 1) != 1) {
                    z2 = false;
                }
                this.A0O = r13.Afy(str, r1.A0O, z, z2);
                this.A0I = r13.Afr(this.A0I, r1.A0I);
                int i3 = this.A01;
                boolean z3 = false;
                if ((i3 & 2) == 2) {
                    z3 = true;
                }
                String str2 = this.A0Q;
                int i4 = r1.A01;
                boolean z4 = false;
                if ((2 & i4) == 2) {
                    z4 = true;
                }
                this.A0Q = r13.Afy(str2, r1.A0Q, z3, z4);
                boolean z5 = false;
                if ((i3 & 4) == 4) {
                    z5 = true;
                }
                String str3 = this.A0R;
                boolean z6 = false;
                if ((i4 & 4) == 4) {
                    z6 = true;
                }
                this.A0R = r13.Afy(str3, r1.A0R, z5, z6);
                boolean z7 = false;
                if ((i3 & 8) == 8) {
                    z7 = true;
                }
                long j = this.A0C;
                boolean z8 = false;
                if ((i4 & 8) == 8) {
                    z8 = true;
                }
                this.A0C = r13.Afs(j, r1.A0C, z7, z8);
                boolean z9 = false;
                if ((i3 & 16) == 16) {
                    z9 = true;
                }
                int i5 = this.A07;
                boolean z10 = false;
                if ((i4 & 16) == 16) {
                    z10 = true;
                }
                this.A07 = r13.Afp(i5, r1.A07, z9, z10);
                boolean z11 = false;
                if ((i3 & 32) == 32) {
                    z11 = true;
                }
                boolean z12 = this.A0a;
                boolean z13 = false;
                if ((i4 & 32) == 32) {
                    z13 = true;
                }
                this.A0a = r13.Afl(z11, z12, z13, r1.A0a);
                boolean z14 = false;
                if ((i3 & 64) == 64) {
                    z14 = true;
                }
                boolean z15 = this.A0V;
                boolean z16 = false;
                if ((i4 & 64) == 64) {
                    z16 = true;
                }
                this.A0V = r13.Afl(z14, z15, z16, r1.A0V);
                boolean z17 = false;
                if ((i3 & 128) == 128) {
                    z17 = true;
                }
                int i6 = this.A04;
                boolean z18 = false;
                if ((i4 & 128) == 128) {
                    z18 = true;
                }
                this.A04 = r13.Afp(i6, r1.A04, z17, z18);
                boolean z19 = false;
                if ((i3 & 256) == 256) {
                    z19 = true;
                }
                long j2 = this.A0B;
                boolean z20 = false;
                if ((i4 & 256) == 256) {
                    z20 = true;
                }
                this.A0B = r13.Afs(j2, r1.A0B, z19, z20);
                boolean z21 = false;
                if ((i3 & 512) == 512) {
                    z21 = true;
                }
                int i7 = this.A03;
                boolean z22 = false;
                if ((i4 & 512) == 512) {
                    z22 = true;
                }
                this.A03 = r13.Afp(i7, r1.A03, z21, z22);
                boolean z23 = false;
                if ((i3 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
                    z23 = true;
                }
                long j3 = this.A09;
                boolean z24 = false;
                if ((i4 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
                    z24 = true;
                }
                this.A09 = r13.Afs(j3, r1.A09, z23, z24);
                boolean z25 = false;
                if ((i3 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
                    z25 = true;
                }
                String str4 = this.A0P;
                boolean z26 = false;
                if ((i4 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
                    z26 = true;
                }
                this.A0P = r13.Afy(str4, r1.A0P, z25, z26);
                boolean z27 = false;
                if ((i3 & 4096) == 4096) {
                    z27 = true;
                }
                String str5 = this.A0S;
                boolean z28 = false;
                if ((i4 & 4096) == 4096) {
                    z28 = true;
                }
                this.A0S = r13.Afy(str5, r1.A0S, z27, z28);
                boolean z29 = false;
                if ((i3 & DefaultCrypto.BUFFER_SIZE) == 8192) {
                    z29 = true;
                }
                boolean z30 = this.A0Z;
                boolean z31 = false;
                if ((i4 & DefaultCrypto.BUFFER_SIZE) == 8192) {
                    z31 = true;
                }
                this.A0Z = r13.Afl(z29, z30, z31, r1.A0Z);
                boolean z32 = false;
                if ((i3 & 16384) == 16384) {
                    z32 = true;
                }
                boolean z33 = this.A0U;
                boolean z34 = false;
                if ((i4 & 16384) == 16384) {
                    z34 = true;
                }
                this.A0U = r13.Afl(z32, z33, z34, r1.A0U);
                this.A0K = (AnonymousClass1PH) r13.Aft(this.A0K, r1.A0K);
                int i8 = this.A01;
                boolean z35 = false;
                if ((i8 & 65536) == 65536) {
                    z35 = true;
                }
                int i9 = this.A08;
                int i10 = r1.A01;
                boolean z36 = false;
                if ((i10 & 65536) == 65536) {
                    z36 = true;
                }
                this.A08 = r13.Afp(i9, r1.A08, z35, z36);
                boolean z37 = false;
                if ((i8 & C25981Bo.A0F) == 131072) {
                    z37 = true;
                }
                boolean z38 = this.A0Y;
                boolean z39 = false;
                if ((i10 & C25981Bo.A0F) == 131072) {
                    z39 = true;
                }
                this.A0Y = r13.Afl(z37, z38, z39, r1.A0Y);
                this.A0J = r13.Afr(this.A0J, r1.A0J);
                boolean z40 = false;
                if ((this.A01 & 262144) == 262144) {
                    z40 = true;
                }
                AbstractC27881Jp r6 = this.A0H;
                boolean z41 = false;
                if ((r1.A01 & 262144) == 262144) {
                    z41 = true;
                }
                this.A0H = r13.Afm(r6, r1.A0H, z40, z41);
                int i11 = this.A01;
                boolean z42 = false;
                if ((i11 & 524288) == 524288) {
                    z42 = true;
                }
                long j4 = this.A0F;
                int i12 = r1.A01;
                boolean z43 = false;
                if ((i12 & 524288) == 524288) {
                    z43 = true;
                }
                this.A0F = r13.Afs(j4, r1.A0F, z42, z43);
                boolean z44 = false;
                if ((i11 & 1048576) == 1048576) {
                    z44 = true;
                }
                AbstractC27881Jp r62 = this.A0G;
                boolean z45 = false;
                if ((i12 & 1048576) == 1048576) {
                    z45 = true;
                }
                this.A0G = r13.Afm(r62, r1.A0G, z44, z45);
                int i13 = this.A01;
                boolean z46 = false;
                if ((i13 & 2097152) == 2097152) {
                    z46 = true;
                }
                int i14 = this.A06;
                int i15 = r1.A01;
                boolean z47 = false;
                if ((i15 & 2097152) == 2097152) {
                    z47 = true;
                }
                this.A06 = r13.Afp(i14, r1.A06, z46, z47);
                boolean z48 = false;
                if ((i13 & 4194304) == 4194304) {
                    z48 = true;
                }
                long j5 = this.A0D;
                boolean z49 = false;
                if ((i15 & 4194304) == 4194304) {
                    z49 = true;
                }
                this.A0D = r13.Afs(j5, r1.A0D, z48, z49);
                this.A0L = (C462425c) r13.Aft(this.A0L, r1.A0L);
                int i16 = this.A01;
                boolean z50 = false;
                if ((i16 & EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING) == 16777216) {
                    z50 = true;
                }
                int i17 = this.A05;
                int i18 = r1.A01;
                boolean z51 = false;
                if ((i18 & EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING) == 16777216) {
                    z51 = true;
                }
                this.A05 = r13.Afp(i17, r1.A05, z50, z51);
                boolean z52 = false;
                if ((i16 & 33554432) == 33554432) {
                    z52 = true;
                }
                long j6 = this.A0E;
                boolean z53 = false;
                if ((i18 & 33554432) == 33554432) {
                    z53 = true;
                }
                this.A0E = r13.Afs(j6, r1.A0E, z52, z53);
                boolean z54 = false;
                if ((i16 & 67108864) == 67108864) {
                    z54 = true;
                }
                boolean z55 = this.A0c;
                boolean z56 = false;
                if ((i18 & 67108864) == 67108864) {
                    z56 = true;
                }
                this.A0c = r13.Afl(z54, z55, z56, r1.A0c);
                boolean z57 = false;
                if ((i16 & 134217728) == 134217728) {
                    z57 = true;
                }
                boolean z58 = this.A0d;
                boolean z59 = false;
                if ((i18 & 134217728) == 134217728) {
                    z59 = true;
                }
                this.A0d = r13.Afl(z57, z58, z59, r1.A0d);
                boolean z60 = false;
                if ((i16 & 268435456) == 268435456) {
                    z60 = true;
                }
                long j7 = this.A0A;
                boolean z61 = false;
                if ((i18 & 268435456) == 268435456) {
                    z61 = true;
                }
                this.A0A = r13.Afs(j7, r1.A0A, z60, z61);
                boolean z62 = false;
                if ((i16 & 536870912) == 536870912) {
                    z62 = true;
                }
                String str6 = this.A0M;
                boolean z63 = false;
                if ((i18 & 536870912) == 536870912) {
                    z63 = true;
                }
                this.A0M = r13.Afy(str6, r1.A0M, z62, z63);
                boolean z64 = false;
                if ((i16 & 1073741824) == 1073741824) {
                    z64 = true;
                }
                String str7 = this.A0N;
                boolean z65 = false;
                if ((i18 & 1073741824) == 1073741824) {
                    z65 = true;
                }
                this.A0N = r13.Afy(str7, r1.A0N, z64, z65);
                boolean z66 = false;
                if ((i16 & Integer.MIN_VALUE) == Integer.MIN_VALUE) {
                    z66 = true;
                }
                boolean z67 = this.A0b;
                boolean z68 = false;
                if ((i18 & Integer.MIN_VALUE) == Integer.MIN_VALUE) {
                    z68 = true;
                }
                this.A0b = r13.Afl(z66, z67, z68, r1.A0b);
                int i19 = this.A02;
                boolean z69 = true;
                if ((i19 & 1) != 1) {
                    z69 = false;
                }
                boolean z70 = this.A0X;
                int i20 = r1.A02;
                boolean z71 = true;
                if ((i20 & 1) != 1) {
                    z71 = false;
                }
                this.A0X = r13.Afl(z69, z70, z71, r1.A0X);
                boolean z72 = false;
                if ((i19 & 2) == 2) {
                    z72 = true;
                }
                boolean z73 = this.A0W;
                boolean z74 = false;
                if ((i20 & 2) == 2) {
                    z74 = true;
                }
                this.A0W = r13.Afl(z72, z73, z74, r1.A0W);
                boolean z75 = false;
                if ((i19 & 4) == 4) {
                    z75 = true;
                }
                String str8 = this.A0T;
                boolean z76 = false;
                if ((i20 & 4) == 4) {
                    z76 = true;
                }
                this.A0T = r13.Afy(str8, r1.A0T, z75, z76);
                if (r13 == C463025i.A00) {
                    this.A01 = i16 | i18;
                    this.A02 = i19 | i20;
                }
                return this;
            case 2:
                AnonymousClass253 r132 = (AnonymousClass253) obj;
                AnonymousClass254 r12 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        try {
                            int A03 = r132.A03();
                            switch (A03) {
                                case 0:
                                    break;
                                case 10:
                                    String A0A = r132.A0A();
                                    this.A01 |= 1;
                                    this.A0O = A0A;
                                    break;
                                case 18:
                                    AnonymousClass1K6 r33 = this.A0I;
                                    if (!((AnonymousClass1K7) r33).A00) {
                                        r33 = AbstractC27091Fz.A0G(r33);
                                        this.A0I = r33;
                                    }
                                    r33.add((C40761sH) r132.A09(r12, C40761sH.A04.A0U()));
                                    break;
                                case 26:
                                    String A0A2 = r132.A0A();
                                    this.A01 |= 2;
                                    this.A0Q = A0A2;
                                    break;
                                case 34:
                                    String A0A3 = r132.A0A();
                                    this.A01 |= 4;
                                    this.A0R = A0A3;
                                    break;
                                case 40:
                                    this.A01 |= 8;
                                    this.A0C = r132.A06();
                                    break;
                                case 48:
                                    this.A01 |= 16;
                                    this.A07 = r132.A02();
                                    break;
                                case 56:
                                    this.A01 |= 32;
                                    this.A0a = r132.A0F();
                                    break;
                                case 64:
                                    this.A01 |= 64;
                                    this.A0V = r132.A0F();
                                    break;
                                case C43951xu.A02 /* 72 */:
                                    this.A01 |= 128;
                                    this.A04 = r132.A02();
                                    break;
                                case 80:
                                    this.A01 |= 256;
                                    this.A0B = r132.A06();
                                    break;
                                case 88:
                                    int A02 = r132.A02();
                                    if (A02 != 0 && A02 != 1) {
                                        super.A0X(11, A02);
                                        break;
                                    } else {
                                        this.A01 |= 512;
                                        this.A03 = A02;
                                        break;
                                    }
                                case 96:
                                    this.A01 |= EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
                                    this.A09 = r132.A06();
                                    break;
                                case 106:
                                    String A0A4 = r132.A0A();
                                    this.A01 |= EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
                                    this.A0P = A0A4;
                                    break;
                                case 114:
                                    String A0A5 = r132.A0A();
                                    this.A01 |= 4096;
                                    this.A0S = A0A5;
                                    break;
                                case 120:
                                    this.A01 |= DefaultCrypto.BUFFER_SIZE;
                                    this.A0Z = r132.A0F();
                                    break;
                                case 128:
                                    this.A01 |= 16384;
                                    this.A0U = r132.A0F();
                                    break;
                                case 138:
                                    if ((this.A01 & 32768) == 32768) {
                                        r32 = (AnonymousClass1PI) this.A0K.A0T();
                                    } else {
                                        r32 = null;
                                    }
                                    AnonymousClass1PH r2 = (AnonymousClass1PH) r132.A09(r12, AnonymousClass1PH.A02.A0U());
                                    this.A0K = r2;
                                    if (r32 != null) {
                                        r32.A04(r2);
                                        this.A0K = (AnonymousClass1PH) r32.A01();
                                    }
                                    this.A01 |= 32768;
                                    break;
                                case MediaCodecVideoEncoder.MIN_ENCODER_HEIGHT /* 144 */:
                                    this.A01 |= 65536;
                                    this.A08 = r132.A02();
                                    break;
                                case 152:
                                    this.A01 |= C25981Bo.A0F;
                                    this.A0Y = r132.A0F();
                                    break;
                                case 162:
                                    AnonymousClass1K6 r34 = this.A0J;
                                    if (!((AnonymousClass1K7) r34).A00) {
                                        r34 = AbstractC27091Fz.A0G(r34);
                                        this.A0J = r34;
                                    }
                                    r34.add((C40861sS) r132.A09(r12, C40861sS.A04.A0U()));
                                    break;
                                case 170:
                                    this.A01 |= 262144;
                                    this.A0H = r132.A08();
                                    break;
                                case MediaCodecVideoEncoder.MIN_ENCODER_WIDTH /* 176 */:
                                    this.A01 |= 524288;
                                    this.A0F = r132.A06();
                                    break;
                                case 186:
                                    this.A01 |= 1048576;
                                    this.A0G = r132.A08();
                                    break;
                                case 192:
                                    this.A01 |= 2097152;
                                    this.A06 = r132.A02();
                                    break;
                                case 200:
                                    this.A01 |= 4194304;
                                    this.A0D = r132.A06();
                                    break;
                                case 210:
                                    if ((this.A01 & 8388608) == 8388608) {
                                        r3 = (C463525n) this.A0L.A0T();
                                    } else {
                                        r3 = null;
                                    }
                                    C462425c r22 = (C462425c) r132.A09(r12, C462425c.A03.A0U());
                                    this.A0L = r22;
                                    if (r3 != null) {
                                        r3.A04(r22);
                                        this.A0L = (C462425c) r3.A01();
                                    }
                                    this.A01 |= 8388608;
                                    break;
                                case 216:
                                    int A022 = r132.A02();
                                    if (AnonymousClass4BT.A00(A022) == null) {
                                        super.A0X(27, A022);
                                        break;
                                    } else {
                                        this.A01 |= EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING;
                                        this.A05 = A022;
                                        break;
                                    }
                                case 224:
                                    this.A01 |= 33554432;
                                    this.A0E = r132.A06();
                                    break;
                                case 232:
                                    this.A01 |= 67108864;
                                    this.A0c = r132.A0F();
                                    break;
                                case 240:
                                    this.A01 |= 134217728;
                                    this.A0d = r132.A0F();
                                    break;
                                case 248:
                                    this.A01 |= 268435456;
                                    this.A0A = r132.A06();
                                    break;
                                case 258:
                                    String A0A6 = r132.A0A();
                                    this.A01 |= 536870912;
                                    this.A0M = A0A6;
                                    break;
                                case 266:
                                    String A0A7 = r132.A0A();
                                    this.A01 |= 1073741824;
                                    this.A0N = A0A7;
                                    break;
                                case 272:
                                    this.A01 |= Integer.MIN_VALUE;
                                    this.A0b = r132.A0F();
                                    break;
                                case 280:
                                    this.A02 |= 1;
                                    this.A0X = r132.A0F();
                                    break;
                                case 288:
                                    this.A02 |= 2;
                                    this.A0W = r132.A0F();
                                    break;
                                case 298:
                                    String A0A8 = r132.A0A();
                                    this.A02 |= 4;
                                    this.A0T = A0A8;
                                    break;
                                default:
                                    if (!A0a(r132, A03)) {
                                        break;
                                    } else {
                                        break;
                                    }
                            }
                        } catch (IOException e) {
                            C28971Pt r14 = new C28971Pt(e.getMessage());
                            r14.unfinishedMessage = this;
                            throw new RuntimeException(r14);
                        }
                    } catch (C28971Pt e2) {
                        e2.unfinishedMessage = this;
                        throw new RuntimeException(e2);
                    }
                }
                break;
            case 3:
                ((AnonymousClass1K7) this.A0I).A00 = false;
                ((AnonymousClass1K7) this.A0J).A00 = false;
                return null;
            case 4:
                return new AnonymousClass1PB();
            case 5:
                return new AnonymousClass1PC();
            case 6:
                break;
            case 7:
                if (A0f == null) {
                    synchronized (AnonymousClass1PB.class) {
                        if (A0f == null) {
                            A0f = new AnonymousClass255(A0e);
                        }
                    }
                }
                return A0f;
            default:
                throw new UnsupportedOperationException();
        }
        return A0e;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i;
        int i2 = ((AbstractC27091Fz) this).A00;
        if (i2 != -1) {
            return i2;
        }
        if ((this.A01 & 1) == 1) {
            i = CodedOutputStream.A07(1, this.A0O) + 0;
        } else {
            i = 0;
        }
        for (int i3 = 0; i3 < this.A0I.size(); i3++) {
            i += CodedOutputStream.A0A((AnonymousClass1G1) this.A0I.get(i3), 2);
        }
        if ((this.A01 & 2) == 2) {
            i += CodedOutputStream.A07(3, this.A0Q);
        }
        if ((this.A01 & 4) == 4) {
            i += CodedOutputStream.A07(4, this.A0R);
        }
        int i4 = this.A01;
        if ((i4 & 8) == 8) {
            i += CodedOutputStream.A06(5, this.A0C);
        }
        if ((i4 & 16) == 16) {
            i += CodedOutputStream.A04(6, this.A07);
        }
        if ((i4 & 32) == 32) {
            i += CodedOutputStream.A00(7);
        }
        if ((i4 & 64) == 64) {
            i += CodedOutputStream.A00(8);
        }
        if ((i4 & 128) == 128) {
            i += CodedOutputStream.A04(9, this.A04);
        }
        if ((i4 & 256) == 256) {
            i += CodedOutputStream.A05(10, this.A0B);
        }
        if ((i4 & 512) == 512) {
            i += CodedOutputStream.A02(11, this.A03);
        }
        if ((i4 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            i += CodedOutputStream.A06(12, this.A09);
        }
        if ((i4 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            i += CodedOutputStream.A07(13, this.A0P);
        }
        if ((this.A01 & 4096) == 4096) {
            i += CodedOutputStream.A07(14, this.A0S);
        }
        int i5 = this.A01;
        if ((i5 & DefaultCrypto.BUFFER_SIZE) == 8192) {
            i += CodedOutputStream.A00(15);
        }
        if ((i5 & 16384) == 16384) {
            i += CodedOutputStream.A00(16);
        }
        if ((i5 & 32768) == 32768) {
            AnonymousClass1PH r0 = this.A0K;
            if (r0 == null) {
                r0 = AnonymousClass1PH.A02;
            }
            i += CodedOutputStream.A0A(r0, 17);
        }
        int i6 = this.A01;
        if ((i6 & 65536) == 65536) {
            i += CodedOutputStream.A04(18, this.A08);
        }
        if ((i6 & C25981Bo.A0F) == 131072) {
            i += CodedOutputStream.A00(19);
        }
        for (int i7 = 0; i7 < this.A0J.size(); i7++) {
            i += CodedOutputStream.A0A((AnonymousClass1G1) this.A0J.get(i7), 20);
        }
        int i8 = this.A01;
        if ((i8 & 262144) == 262144) {
            i += CodedOutputStream.A09(this.A0H, 21);
        }
        if ((i8 & 524288) == 524288) {
            i += CodedOutputStream.A06(22, this.A0F);
        }
        if ((i8 & 1048576) == 1048576) {
            i += CodedOutputStream.A09(this.A0G, 23);
        }
        if ((i8 & 2097152) == 2097152) {
            i += CodedOutputStream.A04(24, this.A06);
        }
        if ((i8 & 4194304) == 4194304) {
            i += CodedOutputStream.A06(25, this.A0D);
        }
        if ((i8 & 8388608) == 8388608) {
            C462425c r02 = this.A0L;
            if (r02 == null) {
                r02 = C462425c.A03;
            }
            i += CodedOutputStream.A0A(r02, 26);
        }
        int i9 = this.A01;
        if ((i9 & EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING) == 16777216) {
            i += CodedOutputStream.A02(27, this.A05);
        }
        if ((i9 & 33554432) == 33554432) {
            i += CodedOutputStream.A06(28, this.A0E);
        }
        if ((i9 & 67108864) == 67108864) {
            i += CodedOutputStream.A00(29);
        }
        if ((i9 & 134217728) == 134217728) {
            i += CodedOutputStream.A00(30);
        }
        if ((i9 & 268435456) == 268435456) {
            i += CodedOutputStream.A06(31, this.A0A);
        }
        if ((i9 & 536870912) == 536870912) {
            i += CodedOutputStream.A07(32, this.A0M);
        }
        if ((this.A01 & 1073741824) == 1073741824) {
            i += CodedOutputStream.A07(33, this.A0N);
        }
        if ((this.A01 & Integer.MIN_VALUE) == Integer.MIN_VALUE) {
            i += CodedOutputStream.A00(34);
        }
        int i10 = this.A02;
        if ((i10 & 1) == 1) {
            i += CodedOutputStream.A00(35);
        }
        if ((i10 & 2) == 2) {
            i += CodedOutputStream.A00(36);
        }
        if ((i10 & 4) == 4) {
            i += CodedOutputStream.A07(37, this.A0T);
        }
        int A00 = i + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A01 & 1) == 1) {
            codedOutputStream.A0I(1, this.A0O);
        }
        for (int i = 0; i < this.A0I.size(); i++) {
            codedOutputStream.A0L((AnonymousClass1G1) this.A0I.get(i), 2);
        }
        if ((this.A01 & 2) == 2) {
            codedOutputStream.A0I(3, this.A0Q);
        }
        if ((this.A01 & 4) == 4) {
            codedOutputStream.A0I(4, this.A0R);
        }
        if ((this.A01 & 8) == 8) {
            codedOutputStream.A0H(5, this.A0C);
        }
        if ((this.A01 & 16) == 16) {
            codedOutputStream.A0F(6, this.A07);
        }
        if ((this.A01 & 32) == 32) {
            codedOutputStream.A0J(7, this.A0a);
        }
        if ((this.A01 & 64) == 64) {
            codedOutputStream.A0J(8, this.A0V);
        }
        if ((this.A01 & 128) == 128) {
            codedOutputStream.A0F(9, this.A04);
        }
        if ((this.A01 & 256) == 256) {
            codedOutputStream.A0H(10, this.A0B);
        }
        if ((this.A01 & 512) == 512) {
            codedOutputStream.A0E(11, this.A03);
        }
        if ((this.A01 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            codedOutputStream.A0H(12, this.A09);
        }
        if ((this.A01 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            codedOutputStream.A0I(13, this.A0P);
        }
        if ((this.A01 & 4096) == 4096) {
            codedOutputStream.A0I(14, this.A0S);
        }
        if ((this.A01 & DefaultCrypto.BUFFER_SIZE) == 8192) {
            codedOutputStream.A0J(15, this.A0Z);
        }
        if ((this.A01 & 16384) == 16384) {
            codedOutputStream.A0J(16, this.A0U);
        }
        if ((this.A01 & 32768) == 32768) {
            AnonymousClass1PH r0 = this.A0K;
            if (r0 == null) {
                r0 = AnonymousClass1PH.A02;
            }
            codedOutputStream.A0L(r0, 17);
        }
        if ((this.A01 & 65536) == 65536) {
            codedOutputStream.A0F(18, this.A08);
        }
        if ((this.A01 & C25981Bo.A0F) == 131072) {
            codedOutputStream.A0J(19, this.A0Y);
        }
        for (int i2 = 0; i2 < this.A0J.size(); i2++) {
            codedOutputStream.A0L((AnonymousClass1G1) this.A0J.get(i2), 20);
        }
        if ((this.A01 & 262144) == 262144) {
            codedOutputStream.A0K(this.A0H, 21);
        }
        if ((this.A01 & 524288) == 524288) {
            codedOutputStream.A0H(22, this.A0F);
        }
        if ((this.A01 & 1048576) == 1048576) {
            codedOutputStream.A0K(this.A0G, 23);
        }
        if ((this.A01 & 2097152) == 2097152) {
            codedOutputStream.A0F(24, this.A06);
        }
        if ((this.A01 & 4194304) == 4194304) {
            codedOutputStream.A0H(25, this.A0D);
        }
        if ((this.A01 & 8388608) == 8388608) {
            C462425c r02 = this.A0L;
            if (r02 == null) {
                r02 = C462425c.A03;
            }
            codedOutputStream.A0L(r02, 26);
        }
        if ((this.A01 & EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING) == 16777216) {
            codedOutputStream.A0E(27, this.A05);
        }
        if ((this.A01 & 33554432) == 33554432) {
            codedOutputStream.A0H(28, this.A0E);
        }
        if ((this.A01 & 67108864) == 67108864) {
            codedOutputStream.A0J(29, this.A0c);
        }
        if ((this.A01 & 134217728) == 134217728) {
            codedOutputStream.A0J(30, this.A0d);
        }
        if ((this.A01 & 268435456) == 268435456) {
            codedOutputStream.A0H(31, this.A0A);
        }
        if ((this.A01 & 536870912) == 536870912) {
            codedOutputStream.A0I(32, this.A0M);
        }
        if ((this.A01 & 1073741824) == 1073741824) {
            codedOutputStream.A0I(33, this.A0N);
        }
        if ((this.A01 & Integer.MIN_VALUE) == Integer.MIN_VALUE) {
            codedOutputStream.A0J(34, this.A0b);
        }
        if ((this.A02 & 1) == 1) {
            codedOutputStream.A0J(35, this.A0X);
        }
        if ((this.A02 & 2) == 2) {
            codedOutputStream.A0J(36, this.A0W);
        }
        if ((this.A02 & 4) == 4) {
            codedOutputStream.A0I(37, this.A0T);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
