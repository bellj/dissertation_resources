package X;

import android.view.View;
import com.whatsapp.Mp4Ops;
import com.whatsapp.util.ViewOnClickCListenerShape15S0100000_I0_2;

/* renamed from: X.1iG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC35561iG extends AbstractC33621eg {
    public AbstractC33641ei A00;
    public final AbstractC15710nm A01;
    public final Mp4Ops A02;
    public final C18790t3 A03;
    public final C16170oZ A04;
    public final AnonymousClass1J1 A05;
    public final C16590pI A06;
    public final C14850m9 A07;
    public final AnonymousClass109 A08;
    public final AbstractC15340mz A09;
    public final AnonymousClass1BD A0A;
    public final AnonymousClass1CW A0B;
    public final AnonymousClass1CZ A0C;
    public final AbstractView$OnClickListenerC34281fs A0D = new ViewOnClickCListenerShape15S0100000_I0_2(this, 41);
    public final AbstractView$OnClickListenerC34281fs A0E = new ViewOnClickCListenerShape15S0100000_I0_2(this, 43);
    public final AbstractView$OnClickListenerC34281fs A0F = new ViewOnClickCListenerShape15S0100000_I0_2(this, 42);

    public AbstractC35561iG(AnonymousClass12P r20, AbstractC15710nm r21, C14900mE r22, Mp4Ops mp4Ops, C239613r r24, C18790t3 r25, C16170oZ r26, AnonymousClass1J1 r27, C16590pI r28, AnonymousClass018 r29, C15650ng r30, C22810zg r31, C18470sV r32, C14850m9 r33, C244415n r34, AnonymousClass109 r35, AnonymousClass1CH r36, AbstractC15340mz r37, C15860o1 r38, AnonymousClass1BD r39, AnonymousClass1CW r40, AnonymousClass1CZ r41, C48252Fe r42, AnonymousClass1CY r43, AbstractC14440lR r44, C17020q8 r45) {
        super(r20, r22, r24, r26, r29, r30, r31, r32, r34, r36, r38, r39, r42, r43, r44, r45);
        this.A06 = r28;
        this.A02 = mp4Ops;
        this.A01 = r21;
        this.A03 = r25;
        this.A07 = r33;
        this.A09 = r37;
        this.A04 = r26;
        this.A0B = r40;
        this.A08 = r35;
        this.A0C = r41;
        this.A0A = r39;
        this.A05 = r27;
    }

    @Override // X.AbstractC33621eg
    public void A0F() {
        super.A0F();
        AbstractC15340mz r1 = this.A09;
        if (r1 instanceof AbstractC16130oV) {
            C16150oX r12 = ((AbstractC16130oV) r1).A02;
            AnonymousClass009.A05(r12);
            if (r12.A0F != null && !r12.A03()) {
                AnonymousClass1CY r13 = this.A0N;
                View view = ((AbstractC33631eh) this).A00;
                AnonymousClass009.A03(view);
                r13.A02((ActivityC13810kN) AnonymousClass12P.A00(view.getContext()));
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x003a, code lost:
        if (r6 == 0) goto L_0x003c;
     */
    @Override // X.AbstractC33621eg
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0H() {
        /*
            r11 = this;
            X.1iH r3 = r11.A0B()
            X.1CH r0 = r11.A0I
            X.0mz r6 = r11.A09
            boolean r0 = X.AnonymousClass3I8.A01(r0, r6)
            r2 = 8
            if (r0 != 0) goto L_0x00b5
            boolean r0 = r6.A13
            if (r0 != 0) goto L_0x00b5
            X.0oV r6 = (X.AbstractC16130oV) r6
            X.0oX r9 = r6.A02
            X.AnonymousClass009.A05(r9)
            boolean r0 = r9.A0a
            r5 = 0
            r4 = 0
            if (r0 == 0) goto L_0x005d
            android.view.View r0 = r3.A04
            r0.setVisibility(r4)
            android.view.View r0 = r3.A04
            r0.setBackground(r5)
            com.whatsapp.CircularProgressBar r10 = r3.A0E
            long r0 = r9.A0C
            r7 = 0
            int r6 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r6 == 0) goto L_0x003c
            r7 = 100
            int r6 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            r0 = 0
            if (r6 != 0) goto L_0x003d
        L_0x003c:
            r0 = 1
        L_0x003d:
            r10.setIndeterminate(r0)
            com.whatsapp.CircularProgressBar r7 = r3.A0E
            long r0 = r9.A0C
            int r6 = (int) r0
            r7.setProgress(r6)
            com.whatsapp.CircularProgressBar r0 = r3.A0E
            r0.setVisibility(r4)
            com.whatsapp.CircularProgressBar r0 = r3.A0E
            r0.setOnClickListener(r5)
            android.widget.TextView r0 = r3.A0B
            r0.setVisibility(r2)
        L_0x0057:
            android.view.View r0 = r3.A00
        L_0x0059:
            r0.setVisibility(r2)
            return
        L_0x005d:
            int r1 = r9.A07
            r0 = 1
            if (r1 != r0) goto L_0x007d
            android.view.View r0 = r3.A04
            r0.setVisibility(r2)
            byte r5 = r6.A0y
            android.widget.TextView r2 = r3.A0C
            r1 = 2
            r0 = 2131888367(0x7f1208ef, float:1.9411367E38)
            if (r5 != r1) goto L_0x0074
            r0 = 2131888366(0x7f1208ee, float:1.9411365E38)
        L_0x0074:
            r2.setText(r0)
            android.widget.TextView r0 = r3.A0C
            r0.setVisibility(r4)
            return
        L_0x007d:
            boolean r0 = r9.A0P
            if (r0 != 0) goto L_0x00b5
            android.view.View r1 = r3.A04
            r0 = 2131231390(0x7f08029e, float:1.807886E38)
            r1.setBackgroundResource(r0)
            android.view.View r0 = r3.A04
            r0.setVisibility(r4)
            com.whatsapp.CircularProgressBar r0 = r3.A0E
            r0.setVisibility(r2)
            com.whatsapp.CircularProgressBar r0 = r3.A0E
            r0.setOnClickListener(r5)
            android.widget.TextView r0 = r3.A0B
            r0.setVisibility(r4)
            android.widget.TextView r1 = r3.A0B
            r0 = 2131886849(0x7f120301, float:1.9408288E38)
            r1.setText(r0)
            android.widget.TextView r1 = r3.A0B
            r0 = 2131231201(0x7f0801e1, float:1.8078476E38)
            r1.setCompoundDrawablesWithIntrinsicBounds(r0, r4, r4, r4)
            android.widget.TextView r1 = r3.A0B
            X.1fs r0 = r11.A0D
            r1.setOnClickListener(r0)
            goto L_0x0057
        L_0x00b5:
            android.view.View r0 = r3.A04
            goto L_0x0059
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC35561iG.A0H():void");
    }

    @Override // X.AbstractC33621eg
    public void A0J(int i) {
        C63413Bm r0;
        AnonymousClass4UK r2;
        super.A0J(i);
        AbstractC15340mz r6 = this.A09;
        AnonymousClass1IS r22 = r6.A0z;
        int i2 = 3;
        if (r22.A02 || (A0A().A0A() && ((AbstractC33621eg) this).A03)) {
            i2 = 1;
        } else if (r6 instanceof AbstractC16130oV) {
            C16150oX r02 = ((AbstractC16130oV) r6).A02;
            AnonymousClass009.A05(r02);
            if (r02.A0a) {
                i2 = 2;
            }
        }
        C28181Ma r8 = this.A0O;
        r8.A00();
        C28181Ma r7 = this.A0Q;
        r7.A00();
        this.A0P.A00();
        A0A().A01();
        AnonymousClass1BD r1 = this.A0A;
        long A01 = A0A().A01();
        long A00 = r8.A00();
        long A002 = r7.A00();
        C458423j r03 = r1.A01;
        if (r03 != null && (r0 = (C63413Bm) r03.A0F.get(AnonymousClass1BD.A01(r6))) != null && (r2 = (AnonymousClass4UK) r0.A08.get(r22)) != null) {
            r2.A04 = i2;
            r2.A06 = A01;
            r2.A07 += A00;
            r2.A08 += A002;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0088, code lost:
        r0 = r3.A02();
     */
    @Override // X.AbstractC33621eg
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0L(java.lang.Integer r13, int r14, boolean r15) {
        /*
        // Method dump skipped, instructions count: 395
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC35561iG.A0L(java.lang.Integer, int, boolean):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0030, code lost:
        if (r5 == 100) goto L_0x0032;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0Q() {
        /*
            r11 = this;
            X.1iH r3 = r11.A0B()
            X.0mz r1 = r11.A09
            boolean r0 = r1 instanceof X.AbstractC16130oV
            r5 = 0
            if (r0 == 0) goto L_0x00a6
            X.0oV r1 = (X.AbstractC16130oV) r1
            X.0oX r10 = r1.A02
        L_0x000f:
            r2 = 8
            r4 = 0
            if (r10 == 0) goto L_0x0093
            boolean r0 = r10.A0P
            if (r0 != 0) goto L_0x0093
            boolean r0 = r10.A0a
            if (r0 == 0) goto L_0x005a
            android.view.View r0 = r3.A04
            r0.setVisibility(r4)
            com.whatsapp.CircularProgressBar r9 = r3.A0E
            long r5 = r10.A0C
            r7 = 0
            int r0 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r0 == 0) goto L_0x0032
            r7 = 100
            int r1 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            r0 = 0
            if (r1 != 0) goto L_0x0033
        L_0x0032:
            r0 = 1
        L_0x0033:
            r9.setIndeterminate(r0)
            com.whatsapp.CircularProgressBar r1 = r3.A0E
            long r5 = r10.A0C
            int r0 = (int) r5
            r1.setProgress(r0)
            com.whatsapp.CircularProgressBar r0 = r3.A0E
            r0.setVisibility(r4)
            android.widget.TextView r0 = r3.A0B
            r0.setVisibility(r2)
            android.view.View r0 = r3.A00
            r0.setVisibility(r4)
            com.whatsapp.CircularProgressBar r1 = r3.A0E
            X.1fs r0 = r11.A0E
            r1.setOnClickListener(r0)
        L_0x0054:
            android.view.ViewGroup r0 = r3.A08
            r0.setVisibility(r2)
        L_0x0059:
            return
        L_0x005a:
            android.view.View r1 = r3.A04
            r0 = 2131231390(0x7f08029e, float:1.807886E38)
            r1.setBackgroundResource(r0)
            android.view.View r0 = r3.A04
            r0.setVisibility(r4)
            com.whatsapp.CircularProgressBar r0 = r3.A0E
            r0.setVisibility(r2)
            com.whatsapp.CircularProgressBar r0 = r3.A0E
            r0.setOnClickListener(r5)
            android.widget.TextView r0 = r3.A0B
            r0.setVisibility(r4)
            android.widget.TextView r1 = r3.A0B
            r0 = 2131891380(0x7f1214b4, float:1.9417478E38)
            r1.setText(r0)
            android.widget.TextView r1 = r3.A0B
            r0 = 2131231241(0x7f080209, float:1.8078557E38)
            r1.setCompoundDrawablesWithIntrinsicBounds(r0, r4, r4, r4)
            android.widget.TextView r1 = r3.A0B
            X.1fs r0 = r11.A0F
            r1.setOnClickListener(r0)
            android.view.View r0 = r3.A00
            r0.setVisibility(r2)
            goto L_0x0054
        L_0x0093:
            android.view.View r0 = r3.A04
            r0.setVisibility(r2)
            android.view.ViewGroup r0 = r3.A08
            int r0 = r0.getVisibility()
            if (r0 != r2) goto L_0x0059
            android.view.ViewGroup r0 = r3.A08
            r0.setVisibility(r4)
            return
        L_0x00a6:
            r10 = r5
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC35561iG.A0Q():void");
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" for ");
        AbstractC15340mz r2 = this.A09;
        sb.append(r2.A0z);
        sb.append(" ");
        sb.append(r2.A0B());
        sb.append(" ");
        sb.append((int) r2.A0y);
        return sb.toString();
    }
}
