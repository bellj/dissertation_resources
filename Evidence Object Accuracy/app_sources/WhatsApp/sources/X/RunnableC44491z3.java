package X;

import com.facebook.redex.EmptyBaseRunnable0;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.wamsys.JniBridge;
import java.util.Set;

/* renamed from: X.1z3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class RunnableC44491z3 extends EmptyBaseRunnable0 implements Runnable {
    public final long A00;
    public final long A01;
    public final long A02;
    public final AbstractC15710nm A03;
    public final C15570nT A04;
    public final C20870wS A05;
    public final C20670w8 A06;
    public final C14830m7 A07;
    public final C15990oG A08;
    public final C18240s8 A09;
    public final C21410xN A0A;
    public final C15600nX A0B;
    public final C26731Ep A0C;
    public final C22830zi A0D;
    public final C22100yW A0E;
    public final C14850m9 A0F;
    public final Jid A0G;
    public final UserJid A0H;
    public final C14840m8 A0I;
    public final AbstractC15340mz A0J;
    public final C239913u A0K;
    public final C20780wJ A0L;
    public final AnonymousClass1VC A0M;
    public final JniBridge A0N;
    public final Runnable A0O;
    public final Set A0P;
    public final boolean A0Q;
    public final boolean A0R;

    public RunnableC44491z3(AbstractC15710nm r3, C15570nT r4, C20870wS r5, C20670w8 r6, C14830m7 r7, C15990oG r8, C18240s8 r9, C21410xN r10, C15600nX r11, C26731Ep r12, C22830zi r13, C22100yW r14, C14850m9 r15, Jid jid, UserJid userJid, C14840m8 r18, AbstractC15340mz r19, C239913u r20, C20780wJ r21, AnonymousClass1VC r22, JniBridge jniBridge, Runnable runnable, Set set, long j, long j2, long j3, boolean z, boolean z2) {
        this.A07 = r7;
        this.A0F = r15;
        this.A03 = r3;
        this.A04 = r4;
        this.A0N = jniBridge;
        this.A06 = r6;
        this.A05 = r5;
        this.A09 = r9;
        this.A0K = r20;
        this.A0I = r18;
        this.A08 = r8;
        this.A0D = r13;
        this.A0A = r10;
        this.A0E = r14;
        this.A0L = r21;
        this.A0B = r11;
        this.A0C = r12;
        this.A0J = r19;
        this.A0G = jid;
        this.A0H = userJid;
        this.A0P = set;
        this.A0R = z;
        this.A0Q = z2;
        this.A00 = j;
        this.A01 = j2;
        this.A02 = j3;
        this.A0O = runnable;
        this.A0M = r22;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v57, types: [java.util.Map] */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x0348, code lost:
        if (r21 != false) goto L_0x034a;
     */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // java.lang.Runnable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
        // Method dump skipped, instructions count: 1142
        */
        throw new UnsupportedOperationException("Method not decompiled: X.RunnableC44491z3.run():void");
    }
}
