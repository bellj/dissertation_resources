package X;

/* renamed from: X.5F0  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5F0 implements AnonymousClass5VG, AbstractC02790Dz {
    public static final AnonymousClass5F0 A00 = new AnonymousClass5F0();

    @Override // X.AbstractC02790Dz
    public boolean A79(Throwable th) {
        return false;
    }

    @Override // X.AnonymousClass5VG
    public void dispose() {
    }

    public String toString() {
        return "NonDisposableHandle";
    }
}
