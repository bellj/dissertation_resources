package X;

import android.content.Context;
import com.whatsapp.wds.components.profilephoto.WDSProfilePhoto;

/* renamed from: X.5Js  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C113975Js extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ Context $context;
    public final /* synthetic */ WDSProfilePhoto this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C113975Js(Context context, WDSProfilePhoto wDSProfilePhoto) {
        super(0);
        this.$context = context;
        this.this$0 = wDSProfilePhoto;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        Context context = this.$context;
        AnonymousClass018 whatsAppLocale = this.this$0.getWhatsAppLocale();
        WDSProfilePhoto wDSProfilePhoto = this.this$0;
        AnonymousClass4BH r5 = wDSProfilePhoto.A03;
        AnonymousClass4BK r4 = wDSProfilePhoto.A02;
        boolean z = wDSProfilePhoto.A06;
        AbstractC93424a9 r6 = wDSProfilePhoto.A04;
        C16700pc.A0E(context, 0);
        C16700pc.A0E(r5, 3);
        C16700pc.A0E(r4, 4);
        return new AnonymousClass4WD(context, whatsAppLocale, new C106174vH(context, whatsAppLocale, wDSProfilePhoto, r4, r5, r6), r4, r5, z);
    }
}
