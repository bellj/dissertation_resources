package X;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import com.whatsapp.R;

/* renamed from: X.1ex  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C33711ex {
    public final C16470p4 A00;

    public C33711ex(C16470p4 r1) {
        this.A00 = r1;
    }

    public static void A00(CharSequence charSequence, String str, StringBuilder sb) {
        if (!TextUtils.isEmpty(charSequence)) {
            if (sb.length() > 0) {
                sb.append(str);
            }
            sb.append(charSequence);
        }
    }

    public Drawable A01(Context context) {
        int i;
        int i2;
        if (this instanceof C33721ey) {
            i = R.drawable.msg_status_shop;
            i2 = R.color.msgStatusTint;
        } else if (!(this instanceof C34991h2)) {
            return null;
        } else {
            i = R.drawable.ic_receipt_16dp;
            i2 = R.color.recharge_quoted_message_icon_color;
        }
        return AnonymousClass2GE.A01(context, i, i2);
    }

    public AnonymousClass24E A02() {
        if ((this instanceof C33721ey) || (this instanceof AnonymousClass34K)) {
            return AnonymousClass24E.A00;
        }
        return AnonymousClass24E.A02;
    }

    public CharSequence A03(Context context, Paint paint, AnonymousClass018 r7) {
        String str;
        String str2;
        StringBuilder sb = new StringBuilder();
        C16470p4 r2 = this.A00;
        if (r2 != null) {
            str = r2.A07;
        } else {
            str = null;
        }
        A00(str, "\n", sb);
        if (r2 != null) {
            str2 = r2.A08;
        } else {
            str2 = null;
        }
        A00(str2, "\n", sb);
        return sb.toString();
    }

    public String A04() {
        AnonymousClass1Z6 r0;
        C16470p4 r02 = this.A00;
        if (r02 == null || (r0 = r02.A02) == null) {
            return null;
        }
        return r0.A01;
    }

    public String A05(Context context) {
        C16470p4 r0 = this.A00;
        if (r0 != null) {
            return r0.A07;
        }
        return null;
    }

    public String A06(Context context) {
        String str;
        String str2;
        StringBuilder sb = new StringBuilder();
        A00(A04(), " ", sb);
        C16470p4 r1 = this.A00;
        if (r1 != null) {
            str = r1.A07;
        } else {
            str = null;
        }
        A00(str, " ", sb);
        if (r1 != null) {
            str2 = r1.A08;
        } else {
            str2 = null;
        }
        A00(str2, " ", sb);
        return sb.toString();
    }

    public CharSequence A07(Context context, Paint paint, AnonymousClass018 r6) {
        String str;
        AnonymousClass1Z6 r0;
        StringBuilder sb = new StringBuilder();
        String A04 = A04();
        if (!TextUtils.isEmpty(A04)) {
            sb.append(A04.trim());
        }
        C16470p4 r02 = this.A00;
        if (r02 == null || (r0 = r02.A02) == null) {
            str = null;
        } else {
            str = r0.A00;
        }
        A00(str, "\n", sb);
        A00(A03(context, paint, r6), "\n", sb);
        Drawable A01 = A01(context);
        if (A01 != null) {
            return C52252aV.A01(paint, A01, sb);
        }
        return sb;
    }

    public String A08(Context context) {
        String str;
        String str2;
        StringBuilder sb = new StringBuilder();
        String A04 = A04();
        if (!TextUtils.isEmpty(A04)) {
            sb.append("*");
            sb.append(A04.trim());
            sb.append("*\n");
        }
        C16470p4 r2 = this.A00;
        if (r2 != null) {
            str = r2.A07;
        } else {
            str = null;
        }
        A00(str, "\n", sb);
        if (r2 != null) {
            str2 = r2.A08;
        } else {
            str2 = null;
        }
        A00(str2, "\n", sb);
        return sb.toString();
    }

    public String A09(AnonymousClass018 r5) {
        String str;
        String str2;
        String str3;
        AnonymousClass1Z6 r0;
        StringBuilder sb = new StringBuilder();
        A00(A04(), " ", sb);
        C16470p4 r1 = this.A00;
        if (r1 == null || (r0 = r1.A02) == null) {
            str = null;
        } else {
            str = r0.A00;
        }
        A00(str, " ", sb);
        if (r1 != null) {
            str2 = r1.A07;
        } else {
            str2 = null;
        }
        A00(str2, " ", sb);
        if (r1 != null) {
            str3 = r1.A08;
        } else {
            str3 = null;
        }
        A00(str3, " ", sb);
        return sb.toString();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0142, code lost:
        if (r1 == false) goto L_0x009f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0A(X.AbstractC15340mz r13, X.C39971qq r14) {
        /*
        // Method dump skipped, instructions count: 332
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C33711ex.A0A(X.0mz, X.1qq):void");
    }
}
