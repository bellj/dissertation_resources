package X;

import com.whatsapp.util.RunnableTRunnableShape13S0200000_I0;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.Executor;

/* renamed from: X.1Gr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ExecutorC27271Gr implements Executor {
    public Runnable A00;
    public final AbstractC14440lR A01;
    public final Object A02;
    public final Queue A03;
    public final boolean A04;
    public volatile long A05;

    public ExecutorC27271Gr(AbstractC14440lR r2) {
        this(r2, false);
    }

    public ExecutorC27271Gr(AbstractC14440lR r2, boolean z) {
        this.A02 = new Object();
        this.A03 = new ArrayDeque();
        this.A01 = r2;
        this.A04 = z;
    }

    public synchronized void A00() {
        this.A03.clear();
    }

    public final synchronized void A01() {
        Runnable runnable = (Runnable) this.A03.poll();
        this.A00 = runnable;
        if (runnable != null) {
            if (this.A04) {
                this.A01.Ab6(runnable);
            } else {
                this.A01.Ab2(runnable);
            }
        }
    }

    public synchronized void A02(Runnable runnable, long j) {
        AbstractC14440lR r3 = this.A01;
        StringBuilder sb = new StringBuilder();
        sb.append("SerialExecutor/executeDelayed/");
        sb.append(runnable.getClass().getName());
        r3.AbK(new RunnableTRunnableShape13S0200000_I0(this, runnable, sb.toString(), 6), "SerialExecutor/executeDelayed", j);
    }

    @Override // java.util.concurrent.Executor
    public synchronized void execute(Runnable runnable) {
        Queue queue = this.A03;
        StringBuilder sb = new StringBuilder();
        sb.append("SerialExecutor/execute/");
        sb.append(runnable.getClass().getName());
        queue.offer(new RunnableTRunnableShape13S0200000_I0(this, runnable, sb.toString(), 5));
        if (this.A00 == null) {
            A01();
        }
    }
}
