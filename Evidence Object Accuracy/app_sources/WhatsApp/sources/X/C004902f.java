package X;

import android.util.Log;
import java.io.PrintWriter;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

/* renamed from: X.02f  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C004902f implements AnonymousClass02g, AbstractC005002h {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public CharSequence A08;
    public CharSequence A09;
    public String A0A;
    public ArrayList A0B;
    public ArrayList A0C;
    public ArrayList A0D;
    public boolean A0E;
    public boolean A0F;
    public boolean A0G;
    public boolean A0H;
    public final C010605f A0I;
    public final AnonymousClass01F A0J;
    public final ClassLoader A0K;

    @Deprecated
    public C004902f() {
        this.A0B = new ArrayList();
        this.A0F = true;
        this.A0H = false;
        this.A0I = null;
        this.A0K = null;
    }

    public C004902f(AnonymousClass01F r4) {
        ClassLoader classLoader;
        C010605f A0B = r4.A0B();
        AnonymousClass05V r0 = r4.A07;
        if (r0 != null) {
            classLoader = r0.A01.getClassLoader();
        } else {
            classLoader = null;
        }
        this.A0B = new ArrayList();
        this.A0F = true;
        this.A0H = false;
        this.A0I = A0B;
        this.A0K = classLoader;
        this.A04 = -1;
        this.A0J = r4;
    }

    public int A00(boolean z) {
        int i;
        if (!this.A0G) {
            if (AnonymousClass01F.A01(2)) {
                StringBuilder sb = new StringBuilder("Commit: ");
                sb.append(this);
                Log.v("FragmentManager", sb.toString());
                PrintWriter printWriter = new PrintWriter(new C03690It());
                A0E(printWriter, "  ", true);
                printWriter.close();
            }
            this.A0G = true;
            if (this.A0E) {
                i = this.A0J.A0a.getAndIncrement();
            } else {
                i = -1;
            }
            this.A04 = i;
            this.A0J.A0c(this, z);
            return this.A04;
        }
        throw new IllegalStateException("commit already called");
    }

    public void A01() {
        A00(false);
    }

    public void A02() {
        A00(true);
    }

    public void A03() {
        if (!this.A0E) {
            this.A0F = false;
            this.A0J.A0d(this, true);
            return;
        }
        throw new IllegalStateException("This transaction is already being added to the back stack");
    }

    public void A04(int i) {
        if (this.A0E) {
            if (AnonymousClass01F.A01(2)) {
                StringBuilder sb = new StringBuilder("Bump nesting in ");
                sb.append(this);
                sb.append(" by ");
                sb.append(i);
                Log.v("FragmentManager", sb.toString());
            }
            ArrayList arrayList = this.A0B;
            int size = arrayList.size();
            for (int i2 = 0; i2 < size; i2++) {
                AnonymousClass0Ta r6 = (AnonymousClass0Ta) arrayList.get(i2);
                AnonymousClass01E r1 = r6.A05;
                if (r1 != null) {
                    r1.A00 += i;
                    if (AnonymousClass01F.A01(2)) {
                        StringBuilder sb2 = new StringBuilder("Bump nesting of ");
                        sb2.append(r6.A05);
                        sb2.append(" to ");
                        sb2.append(r6.A05.A00);
                        Log.v("FragmentManager", sb2.toString());
                    }
                }
            }
        }
    }

    public void A05(AnonymousClass01E r3) {
        AnonymousClass01F r1 = r3.A0H;
        if (r1 == null || r1 == this.A0J) {
            A0D(new AnonymousClass0Ta(r3, 3));
            return;
        }
        StringBuilder sb = new StringBuilder("Cannot remove Fragment attached to a different FragmentManager. Fragment ");
        sb.append(r3.toString());
        sb.append(" is already attached to a FragmentManager.");
        throw new IllegalStateException(sb.toString());
    }

    public void A06(AnonymousClass01E r3, int i) {
        A0C(r3, null, i, 1);
    }

    public void A07(AnonymousClass01E r2, int i) {
        A0B(r2, null, i);
    }

    public void A08(AnonymousClass01E r4, AnonymousClass05I r5) {
        AnonymousClass01F r0 = r4.A0H;
        AnonymousClass01F r2 = this.A0J;
        if (r0 != r2) {
            StringBuilder sb = new StringBuilder("Cannot setMaxLifecycle for Fragment not attached to FragmentManager ");
            sb.append(r2);
            throw new IllegalArgumentException(sb.toString());
        } else if (r5 == AnonymousClass05I.INITIALIZED && r4.A03 > -1) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Cannot set maximum Lifecycle to ");
            sb2.append(r5);
            sb2.append(" after the Fragment has been created");
            throw new IllegalArgumentException(sb2.toString());
        } else if (r5 != AnonymousClass05I.DESTROYED) {
            A0D(new AnonymousClass0Ta(r4, r5));
        } else {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("Cannot set maximum Lifecycle to ");
            sb3.append(r5);
            sb3.append(". Use remove() to remove the fragment from the FragmentManager and trigger its destruction.");
            throw new IllegalArgumentException(sb3.toString());
        }
    }

    public void A09(AnonymousClass01E r3, String str) {
        A0C(r3, str, 0, 1);
    }

    public void A0A(AnonymousClass01E r2, String str, int i) {
        A0C(r2, str, i, 1);
    }

    public void A0B(AnonymousClass01E r3, String str, int i) {
        if (i != 0) {
            A0C(r3, str, i, 2);
            return;
        }
        throw new IllegalArgumentException("Must use non-zero containerViewId");
    }

    public void A0C(AnonymousClass01E r5, String str, int i, int i2) {
        Class<?> cls = r5.getClass();
        int modifiers = cls.getModifiers();
        if (cls.isAnonymousClass() || !Modifier.isPublic(modifiers) || (cls.isMemberClass() && !Modifier.isStatic(modifiers))) {
            StringBuilder sb = new StringBuilder("Fragment ");
            sb.append(cls.getCanonicalName());
            sb.append(" must be a public static class to be  properly recreated from instance state.");
            throw new IllegalStateException(sb.toString());
        }
        if (str != null) {
            String str2 = r5.A0R;
            if (str2 == null || str.equals(str2)) {
                r5.A0R = str;
            } else {
                StringBuilder sb2 = new StringBuilder("Can't change tag of fragment ");
                sb2.append(r5);
                sb2.append(": was ");
                sb2.append(r5.A0R);
                sb2.append(" now ");
                sb2.append(str);
                throw new IllegalStateException(sb2.toString());
            }
        }
        if (i != 0) {
            if (i != -1) {
                int i3 = r5.A02;
                if (i3 == 0 || i3 == i) {
                    r5.A02 = i;
                    r5.A01 = i;
                } else {
                    StringBuilder sb3 = new StringBuilder("Can't change container ID of fragment ");
                    sb3.append(r5);
                    sb3.append(": was ");
                    sb3.append(r5.A02);
                    sb3.append(" now ");
                    sb3.append(i);
                    throw new IllegalStateException(sb3.toString());
                }
            } else {
                StringBuilder sb4 = new StringBuilder("Can't add fragment ");
                sb4.append(r5);
                sb4.append(" with tag ");
                sb4.append(str);
                sb4.append(" to container view with no id");
                throw new IllegalArgumentException(sb4.toString());
            }
        }
        A0D(new AnonymousClass0Ta(r5, i2));
        r5.A0H = this.A0J;
    }

    public void A0D(AnonymousClass0Ta r2) {
        this.A0B.add(r2);
        r2.A01 = this.A02;
        r2.A02 = this.A03;
        r2.A03 = this.A05;
        r2.A04 = this.A06;
    }

    public void A0E(PrintWriter printWriter, String str, boolean z) {
        String str2;
        if (z) {
            printWriter.print(str);
            printWriter.print("mName=");
            printWriter.print(this.A0A);
            printWriter.print(" mIndex=");
            printWriter.print(this.A04);
            printWriter.print(" mCommitted=");
            printWriter.println(this.A0G);
            if (this.A07 != 0) {
                printWriter.print(str);
                printWriter.print("mTransition=#");
                printWriter.print(Integer.toHexString(this.A07));
            }
            if (!(this.A02 == 0 && this.A03 == 0)) {
                printWriter.print(str);
                printWriter.print("mEnterAnim=#");
                printWriter.print(Integer.toHexString(this.A02));
                printWriter.print(" mExitAnim=#");
                printWriter.println(Integer.toHexString(this.A03));
            }
            if (!(this.A05 == 0 && this.A06 == 0)) {
                printWriter.print(str);
                printWriter.print("mPopEnterAnim=#");
                printWriter.print(Integer.toHexString(this.A05));
                printWriter.print(" mPopExitAnim=#");
                printWriter.println(Integer.toHexString(this.A06));
            }
            if (!(this.A01 == 0 && this.A09 == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbTitleRes=#");
                printWriter.print(Integer.toHexString(this.A01));
                printWriter.print(" mBreadCrumbTitleText=");
                printWriter.println(this.A09);
            }
            if (!(this.A00 == 0 && this.A08 == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbShortTitleRes=#");
                printWriter.print(Integer.toHexString(this.A00));
                printWriter.print(" mBreadCrumbShortTitleText=");
                printWriter.println(this.A08);
            }
        }
        ArrayList arrayList = this.A0B;
        if (!arrayList.isEmpty()) {
            printWriter.print(str);
            printWriter.println("Operations:");
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                AnonymousClass0Ta r4 = (AnonymousClass0Ta) arrayList.get(i);
                switch (r4.A00) {
                    case 0:
                        str2 = "NULL";
                        break;
                    case 1:
                        str2 = "ADD";
                        break;
                    case 2:
                        str2 = "REPLACE";
                        break;
                    case 3:
                        str2 = "REMOVE";
                        break;
                    case 4:
                        str2 = "HIDE";
                        break;
                    case 5:
                        str2 = "SHOW";
                        break;
                    case 6:
                        str2 = "DETACH";
                        break;
                    case 7:
                        str2 = "ATTACH";
                        break;
                    case 8:
                        str2 = "SET_PRIMARY_NAV";
                        break;
                    case 9:
                        str2 = "UNSET_PRIMARY_NAV";
                        break;
                    case 10:
                        str2 = "OP_SET_MAX_LIFECYCLE";
                        break;
                    default:
                        StringBuilder sb = new StringBuilder("cmd=");
                        sb.append(r4.A00);
                        str2 = sb.toString();
                        break;
                }
                printWriter.print(str);
                printWriter.print("  Op #");
                printWriter.print(i);
                printWriter.print(": ");
                printWriter.print(str2);
                printWriter.print(" ");
                printWriter.println(r4.A05);
                if (z) {
                    if (!(r4.A01 == 0 && r4.A02 == 0)) {
                        printWriter.print(str);
                        printWriter.print("enterAnim=#");
                        printWriter.print(Integer.toHexString(r4.A01));
                        printWriter.print(" exitAnim=#");
                        printWriter.println(Integer.toHexString(r4.A02));
                    }
                    if (r4.A03 != 0 || r4.A04 != 0) {
                        printWriter.print(str);
                        printWriter.print("popEnterAnim=#");
                        printWriter.print(Integer.toHexString(r4.A03));
                        printWriter.print(" popExitAnim=#");
                        printWriter.println(Integer.toHexString(r4.A04));
                    }
                }
            }
        }
    }

    public void A0F(String str) {
        if (this.A0F) {
            this.A0E = true;
            this.A0A = str;
            return;
        }
        throw new IllegalStateException("This FragmentTransaction is not allowed to be added to the back stack.");
    }

    @Override // X.AnonymousClass02g
    public boolean AAG(ArrayList arrayList, ArrayList arrayList2) {
        if (AnonymousClass01F.A01(2)) {
            StringBuilder sb = new StringBuilder("Run: ");
            sb.append(this);
            Log.v("FragmentManager", sb.toString());
        }
        arrayList.add(this);
        arrayList2.add(Boolean.FALSE);
        if (!this.A0E) {
            return true;
        }
        AnonymousClass01F r1 = this.A0J;
        ArrayList arrayList3 = r1.A0E;
        if (arrayList3 == null) {
            arrayList3 = new ArrayList();
            r1.A0E = arrayList3;
        }
        arrayList3.add(this);
        return true;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("BackStackEntry{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        int i = this.A04;
        if (i >= 0) {
            sb.append(" #");
            sb.append(i);
        }
        String str = this.A0A;
        if (str != null) {
            sb.append(" ");
            sb.append(str);
        }
        sb.append("}");
        return sb.toString();
    }
}
