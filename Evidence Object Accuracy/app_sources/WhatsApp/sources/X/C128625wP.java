package X;

import com.whatsapp.util.Log;

/* renamed from: X.5wP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128625wP {
    public final /* synthetic */ C123595nP A00;

    public C128625wP(C123595nP r1) {
        this.A00 = r1;
    }

    public void A00() {
        Log.i("DyiViewModel/download-report/on-error");
        C123595nP r3 = this.A00;
        AnonymousClass60U.A01(r3.A02, r3.A08, r3.A0A);
        C117305Zk.A1B(r3.A03, C12970iu.A0h(), new C452120p(500));
    }
}
