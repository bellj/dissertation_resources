package X;

import android.view.animation.Animation;
import com.whatsapp.payments.ui.widget.PaymentView;

/* renamed from: X.5d0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119155d0 extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ PaymentView A00;

    public C119155d0(PaymentView paymentView) {
        this.A00 = paymentView;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        this.A00.A0J.setVisibility(8);
    }
}
