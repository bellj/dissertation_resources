package X;

import com.facebook.redex.RunnableBRunnable0Shape7S0100000_I0_7;
import com.whatsapp.NativeMediaHandler;
import com.whatsapp.util.Log;

/* renamed from: X.0y9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21890y9 {
    public boolean A00;
    public final C14330lG A01;
    public final C14900mE A02;
    public final C17050qB A03;
    public final C14830m7 A04;
    public final C18260sA A05;
    public final C21790xz A06;

    public C21890y9(C14330lG r1, C14900mE r2, C17050qB r3, C14830m7 r4, C18260sA r5, C21790xz r6) {
        this.A04 = r4;
        this.A02 = r2;
        this.A01 = r1;
        this.A06 = r6;
        this.A03 = r3;
        this.A05 = r5;
    }

    public final void A00() {
        if (this.A00) {
            C21790xz r3 = this.A06;
            if (r3.A04 == null) {
                synchronized (r3) {
                    if (r3.A04 == null) {
                        r3.A04 = new C27411Hi(((C32861cr) r3.A00.A05.get()).A02, r3.A02);
                    }
                }
            }
            r3.A04.A00();
        }
    }

    public void A01(boolean z, boolean z2) {
        C17050qB r3 = this.A03;
        AnonymousClass01H r1 = r3.A05;
        if (!((C32861cr) r1.get()).A00) {
            if (!((C32861cr) r1.get()).A01) {
                if (z) {
                    this.A01.A0P();
                }
                NativeMediaHandler.initFileHandlingCallbacks(this.A01.A01);
                if (!this.A00) {
                    this.A00 = true;
                    Log.i("media-state-manager/refresh-media-state/writable-media");
                    if (!z2) {
                        A00();
                    }
                }
            }
            C18260sA r5 = this.A05;
            if (r5.A02(r5.A0L.A00)) {
                C44981zs r4 = new C44981zs();
                r4.A04 = Long.valueOf(System.currentTimeMillis());
                r4.A03 = 2;
                r5.A01(new C44991zt(null, r5, r4), -1);
            }
            this.A02.A0I(new RunnableBRunnable0Shape7S0100000_I0_7(r3, 43));
        }
    }
}
