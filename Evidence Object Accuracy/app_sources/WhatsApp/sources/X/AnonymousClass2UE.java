package X;

import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.2UE  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2UE extends AbstractC018308n {
    public final /* synthetic */ int A00;
    public final /* synthetic */ C49632Lo A01;
    public final /* synthetic */ AnonymousClass018 A02;

    public AnonymousClass2UE(C49632Lo r1, AnonymousClass018 r2, int i) {
        this.A01 = r1;
        this.A02 = r2;
        this.A00 = i;
    }

    @Override // X.AbstractC018308n
    public void A01(Rect rect, View view, C05480Ps r6, RecyclerView recyclerView) {
        boolean z = !this.A02.A04().A06;
        int i = this.A00;
        if (z) {
            rect.set(0, 0, i, 0);
        } else {
            rect.set(i, 0, 0, 0);
        }
    }
}
