package X;

import android.content.Context;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5aV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117725aV extends LinearLayout implements AnonymousClass004 {
    public TextView A00;
    public C15450nH A01;
    public AnonymousClass018 A02;
    public AnonymousClass2P7 A03;
    public boolean A04;

    public C117725aV(Context context) {
        super(context);
        if (!this.A04) {
            this.A04 = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A02 = C12960it.A0R(A00);
            this.A01 = (C15450nH) A00.AII.get();
        }
        this.A00 = C12960it.A0J(C12960it.A0E(this).inflate(R.layout.payment_help_support_information_row, (ViewGroup) this, true), R.id.contact_bank_details);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A03;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A03 = r0;
        }
        return r0.generatedComponent();
    }

    public void setContactInformation(String str) {
        String A03 = this.A01.A03(AbstractC15460nI.A2N);
        if (TextUtils.isEmpty(A03) || !C31001Zq.A09(str)) {
            if (!TextUtils.isEmpty(A03)) {
                str = null;
            } else {
                setVisibility(8);
                return;
            }
        }
        setWhatsAppContactDetails(A03, str);
    }

    public final void setWhatsAppContactDetails(String str, String str2) {
        int i;
        Object[] objArr;
        boolean A09 = C31001Zq.A09(str2);
        AnonymousClass018 r2 = this.A02;
        if (A09) {
            i = R.string.contact_support_for_payment;
            objArr = C12980iv.A1a();
            objArr[0] = str;
            objArr[1] = str2;
        } else {
            i = R.string.contact_support_for_payment_no_transaction;
            objArr = new Object[]{str};
        }
        String A0B = r2.A0B(i, objArr);
        SpannableString spannableString = new SpannableString(A0B);
        C117295Zj.A0l(spannableString, C12960it.A0d(str, C12960it.A0k("tel:")), A0B, str);
        TextView textView = this.A00;
        textView.setText(spannableString);
        textView.setVisibility(0);
    }
}
