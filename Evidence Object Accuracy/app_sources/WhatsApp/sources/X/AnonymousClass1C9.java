package X;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import com.whatsapp.util.Log;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* renamed from: X.1C9  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1C9 extends AbstractC16280ok {
    public final C16590pI A00;
    public final C231410n A01;

    public AnonymousClass1C9(AbstractC15710nm r8, C16590pI r9, C231410n r10) {
        super(r9.A00, r8, "daily_metrics.db", new ReentrantReadWriteLock(), 1, true);
        this.A00 = r9;
        this.A01 = r10;
    }

    @Override // X.AbstractC16280ok
    public C16330op A03() {
        Log.i("DailyMetricsDbHelper/initDatabase");
        try {
            return AnonymousClass1Tx.A01(super.A00(), this.A01);
        } catch (SQLiteException e) {
            Log.e("failed to open writable daily metrics store", e);
            return AnonymousClass1Tx.A01(super.A00(), this.A01);
        }
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        Log.i("DailyMetricsDbHelper/onCreate; version=1");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS community_home_action_logging");
        sQLiteDatabase.execSQL("CREATE TABLE community_home_action_logging(jid_row_id INTEGER PRIMARY KEY, home_view_count INTEGER NOT NULL DEFAULT 0, home_group_navigation_count INTEGER NOT NULL DEFAULT 0, home_group_discovery_count INTEGER NOT NULL DEFAULT 0, home_group_join_count INTEGER NOT NULL DEFAULT 0)");
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        StringBuilder sb = new StringBuilder("DailyMetricsDbHelper/downgrade from ");
        sb.append(i);
        sb.append(" to ");
        sb.append(i2);
        Log.i(sb.toString());
        onCreate(sQLiteDatabase);
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        Log.i("DailyMetricsDbHelper/onUpgrade");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS community_home_action_logging");
        sQLiteDatabase.execSQL("CREATE TABLE community_home_action_logging(jid_row_id INTEGER PRIMARY KEY, home_view_count INTEGER NOT NULL DEFAULT 0, home_group_navigation_count INTEGER NOT NULL DEFAULT 0, home_group_discovery_count INTEGER NOT NULL DEFAULT 0, home_group_join_count INTEGER NOT NULL DEFAULT 0)");
    }
}
