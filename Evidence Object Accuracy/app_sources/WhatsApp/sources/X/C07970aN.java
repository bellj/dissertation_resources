package X;

import android.graphics.Path;
import java.util.List;

/* renamed from: X.0aN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07970aN implements AbstractC12850if, AbstractC12030hG {
    public AnonymousClass0OG A00 = new AnonymousClass0OG();
    public boolean A01;
    public final Path A02 = new Path();
    public final AnonymousClass0AA A03;
    public final AnonymousClass0QR A04;
    public final String A05;
    public final boolean A06;

    public C07970aN(AnonymousClass0AA r3, C08230an r4, AbstractC08070aX r5) {
        this.A05 = r4.A02;
        this.A06 = r4.A03;
        this.A03 = r3;
        AnonymousClass0Gs r0 = new AnonymousClass0Gs(r4.A01.A00);
        this.A04 = r0;
        r5.A03(r0);
        r0.A07.add(this);
    }

    @Override // X.AbstractC12850if
    public Path AF0() {
        boolean z = this.A01;
        Path path = this.A02;
        if (!z) {
            path.reset();
            if (!this.A06) {
                path.set((Path) this.A04.A03());
                path.setFillType(Path.FillType.EVEN_ODD);
                this.A00.A00(path);
            }
            this.A01 = true;
        }
        return path;
    }

    @Override // X.AbstractC12030hG
    public void AYB() {
        this.A01 = false;
        this.A03.invalidateSelf();
    }

    @Override // X.AbstractC12470hy
    public void Aby(List list, List list2) {
        for (int i = 0; i < list.size(); i++) {
            AbstractC12470hy r2 = (AbstractC12470hy) list.get(i);
            if (r2 instanceof C07950aL) {
                C07950aL r22 = (C07950aL) r2;
                if (r22.A03 == AnonymousClass0J5.SIMULTANEOUSLY) {
                    this.A00.A00.add(r22);
                    r22.A05.add(this);
                }
            }
        }
    }

    @Override // X.AbstractC12470hy
    public String getName() {
        return this.A05;
    }
}
