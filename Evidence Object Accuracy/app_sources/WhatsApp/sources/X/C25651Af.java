package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDiskIOException;
import com.facebook.redex.RunnableBRunnable0Shape6S0100000_I0_6;
import com.whatsapp.jobqueue.job.GetStatusPrivacyJob;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.1Af  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C25651Af {
    public final C20670w8 A00;
    public final C15550nR A01;
    public final C20730wE A02;
    public final C25631Ad A03;
    public final C20650w6 A04;
    public final C25641Ae A05;
    public final C25621Ac A06;
    public final C245215v A07;
    public final C15600nX A08;
    public final C20820wN A09;
    public final C20850wQ A0A;
    public final C18470sV A0B;
    public final C245916c A0C;
    public final C249317l A0D;
    public final C20710wC A0E;
    public final C20660w7 A0F;
    public final C20780wJ A0G;

    public C25651Af(C20670w8 r2, C15550nR r3, C20730wE r4, C25631Ad r5, C20650w6 r6, C25641Ae r7, C25621Ac r8, C245215v r9, C15600nX r10, C20820wN r11, C20850wQ r12, C18470sV r13, C245916c r14, C249317l r15, C20710wC r16, C20660w7 r17, C20780wJ r18) {
        this.A04 = r6;
        this.A0F = r17;
        this.A0B = r13;
        this.A00 = r2;
        this.A0C = r14;
        this.A01 = r3;
        this.A06 = r8;
        this.A0E = r16;
        this.A02 = r4;
        this.A07 = r9;
        this.A0G = r18;
        this.A03 = r5;
        this.A05 = r7;
        this.A08 = r10;
        this.A09 = r11;
        this.A0A = r12;
        this.A0D = r15;
    }

    /* JADX INFO: finally extract failed */
    public void A00() {
        long longValue;
        int i;
        this.A04.A07(false);
        C245916c r9 = this.A0C;
        C16310on A02 = r9.A02.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            C246116e r6 = r9.A03;
            C16310on A022 = r6.A01.A02();
            AnonymousClass1Lx A002 = A022.A00();
            A022.A03.A01("user_device_info", null, null);
            A002.A00();
            A022.A03(new RunnableBRunnable0Shape6S0100000_I0_6(r6, 11));
            A002.close();
            A022.close();
            C246016d r62 = r9.A05;
            C16310on A023 = r62.A01.A02();
            AnonymousClass1Lx A003 = A023.A00();
            try {
                A023.A03.A01("user_device", null, null);
                A003.A00();
                A023.A03(new RunnableBRunnable0Shape6S0100000_I0_6(r62, 17));
                A003.close();
                A023.close();
                A00.A00();
                A00.close();
                A02.close();
                this.A0A.A01();
                C25631Ad r1 = this.A03;
                Log.i("BroadcastListChatStore/getBroadcastLists");
                ArrayList arrayList = new ArrayList();
                try {
                    C16310on A01 = r1.A00.get();
                    Cursor A09 = A01.A03.A09("SELECT raw_string_jid, subject, created_timestamp FROM chat_view WHERE raw_string_jid LIKE '%@broadcast' AND (chat_view.hidden IS NULL OR hidden=0)", null);
                    if (A09 != null) {
                        try {
                            int columnIndexOrThrow = A09.getColumnIndexOrThrow("raw_string_jid");
                            int columnIndexOrThrow2 = A09.getColumnIndexOrThrow("subject");
                            int columnIndexOrThrow3 = A09.getColumnIndexOrThrow("created_timestamp");
                            while (A09.moveToNext()) {
                                C29901Ve A024 = C29901Ve.A02(A09.getString(columnIndexOrThrow));
                                if (A024 == null) {
                                    Log.w("BroadcastListChatStore/getBroadcastLists/jid is null or invalid!");
                                } else {
                                    String string = A09.getString(columnIndexOrThrow2);
                                    long j = 0;
                                    if (!A09.isNull(2)) {
                                        j = A09.getLong(columnIndexOrThrow3);
                                    }
                                    arrayList.add(new AnonymousClass2KC(A024, string, j));
                                }
                            }
                            A09.close();
                        } catch (Throwable th) {
                            try {
                                A09.close();
                            } catch (Throwable unused) {
                            }
                            throw th;
                        }
                    }
                    A01.close();
                } catch (SQLiteDiskIOException e) {
                    Log.e("BroadcastListChatStore/getBroadcastLists/error ", e);
                }
                C15550nR r12 = this.A01;
                Log.i("contactmanager/populateNamesFromBroadcasts");
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    AnonymousClass2KC r0 = (AnonymousClass2KC) it.next();
                    r12.A07(r0.A01, r0.A02, r0.A00);
                }
                C25621Ac r13 = this.A06;
                Log.i("msgstore/getPersistedGroupInfo");
                ArrayList arrayList2 = new ArrayList();
                try {
                    C16310on A012 = r13.A05.get();
                    Cursor A092 = A012.A03.A09("SELECT raw_string_jid, subject, created_timestamp, ephemeral_expiration FROM chat_view WHERE raw_string_jid LIKE '%@g.us' AND (chat_view.hidden IS NULL OR chat_view.hidden = 0)", new String[0]);
                    if (A092 != null) {
                        while (A092.moveToNext()) {
                            String string2 = A092.getString(0);
                            String string3 = A092.getString(1);
                            Long l = null;
                            if (!A092.isNull(2)) {
                                l = Long.valueOf(A092.getLong(2));
                            }
                            if (!A092.isNull(3)) {
                                i = A092.getInt(3);
                            } else {
                                i = 0;
                            }
                            C15580nU A04 = C15580nU.A04(string2);
                            if (A04 != null) {
                                arrayList2.add(new AnonymousClass2KD(A04, l, string3, i));
                            }
                        }
                        A092.close();
                    }
                    A012.close();
                } catch (SQLiteDiskIOException e2) {
                    Log.e("msgstore/groupinfo/error ", e2);
                }
                Log.i("contactmanager/populateNamesFromBroadcasts");
                Iterator it2 = arrayList2.iterator();
                while (it2.hasNext()) {
                    AnonymousClass2KD r3 = (AnonymousClass2KD) it2.next();
                    C15370n3 r132 = new C15370n3(r3.A01);
                    String str = r3.A03;
                    Long l2 = r3.A02;
                    if (l2 == null) {
                        longValue = Long.MIN_VALUE;
                    } else {
                        longValue = l2.longValue();
                    }
                    r12.A0O(r132, null, AnonymousClass1PD.A04, str, r3.A00, longValue, false, false, false, false, false, false);
                }
                C15600nX r5 = this.A08;
                C16310on A025 = r5.A07.A02();
                try {
                    AnonymousClass1Lx A004 = A025.A00();
                    C18680sq r63 = r5.A09;
                    if (r63.A0C()) {
                        Log.i("participant-user-store/resetSentSenderKeyForAllParticipants");
                        C16310on A026 = r63.A08.A02();
                        AnonymousClass1Lx A005 = A026.A00();
                        AnonymousClass170 r32 = r63.A09;
                        Log.i("participant-device-store/resetSentSenderKeyForAllParticipants");
                        ContentValues contentValues = new ContentValues(1);
                        contentValues.put("sent_sender_key", (Integer) 0);
                        C16310on A027 = r32.A03.A02();
                        A027.A03.A00("group_participant_device", contentValues, null, null);
                        A027.close();
                        ConcurrentHashMap concurrentHashMap = r63.A06.A01;
                        Iterator it3 = new HashSet(concurrentHashMap.keySet()).iterator();
                        while (it3.hasNext()) {
                            AnonymousClass1YM r02 = (AnonymousClass1YM) concurrentHashMap.get((AbstractC15590nW) it3.next());
                            if (r02 != null) {
                                r63.A06(r02);
                            }
                        }
                        A005.A00();
                        A005.close();
                        A026.close();
                    }
                    if (!r63.A0B()) {
                        C20590w0 r8 = r5.A08;
                        C16310on A028 = r8.A08.A02();
                        ContentValues contentValues2 = new ContentValues(1);
                        contentValues2.put("sent_sender_key", Boolean.FALSE);
                        if (A028.A03.A00("group_participants", contentValues2, null, null) > 0) {
                            ConcurrentHashMap concurrentHashMap2 = r8.A07.A01;
                            Iterator it4 = new HashSet(concurrentHashMap2.keySet()).iterator();
                            while (it4.hasNext()) {
                                AnonymousClass1YM r4 = (AnonymousClass1YM) concurrentHashMap2.get((AbstractC15590nW) it4.next());
                                if (r4 != null) {
                                    Iterator it5 = r4.A07().iterator();
                                    while (it5.hasNext()) {
                                        r8.A03((AnonymousClass1YO) it5.next(), r4, false);
                                    }
                                }
                            }
                        }
                        A028.close();
                    }
                    A004.A00();
                    A004.close();
                    A025.close();
                    this.A05.A00();
                    this.A0D.A01();
                } catch (Throwable th2) {
                    try {
                        A025.close();
                    } catch (Throwable unused2) {
                    }
                    throw th2;
                }
            } catch (Throwable th3) {
                try {
                    A003.close();
                } catch (Throwable unused3) {
                }
                throw th3;
            }
        } catch (Throwable th4) {
            try {
                A02.close();
            } catch (Throwable unused4) {
            }
            throw th4;
        }
    }

    public void A01() {
        this.A02.A04();
        this.A07.A01.clear();
        this.A0E.A0F(3, true, false);
        this.A0F.A04();
        C16490p7 r0 = this.A09.A00;
        r0.A04();
        if (r0.A07.exists() && !this.A0B.A0F()) {
            this.A00.A00(GetStatusPrivacyJob.A00());
        }
    }
}
