package X;

import android.os.Looper;
import android.util.Log;
import android.util.Pair;
import com.facebook.redex.IDxEventShape18S0100000_2_I1;
import com.facebook.redex.RunnableBRunnable0Shape10S0200000_I1;
import com.google.android.exoplayer2.Timeline;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArraySet;

/* renamed from: X.3ll  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C76513ll extends AbstractC47502Ay implements AnonymousClass2B0 {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public long A04;
    public C95034cy A05;
    public C94224bS A06;
    public AnonymousClass5XM A07;
    public boolean A08;
    public final Looper A09;
    public final AnonymousClass5Pt A0A;
    public final C107544xV A0B;
    public final AnonymousClass4YJ A0C;
    public final C106424vg A0D;
    public final AnonymousClass5QA A0E;
    public final AnonymousClass4M7 A0F;
    public final AnonymousClass4R7 A0G;
    public final AnonymousClass5QN A0H;
    public final AnonymousClass5Xd A0I;
    public final AnonymousClass5QQ A0J;
    public final C92874Xt A0K;
    public final List A0L;
    public final boolean A0M = true;
    public final AbstractC117055Yb[] A0N;

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x00c8, code lost:
        if (r30.A06.A03.isEmpty() != false) goto L_0x00ca;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C76513ll(android.os.Looper r25, X.AnonymousClass5Pu r26, X.AnonymousClass5Pv r27, X.AbstractC47512Az r28, X.C94224bS r29, X.C106424vg r30, X.AnonymousClass5QA r31, X.AnonymousClass4M7 r32, X.AnonymousClass5QN r33, X.AnonymousClass5Xd r34, X.AbstractC117055Yb[] r35) {
        /*
        // Method dump skipped, instructions count: 291
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C76513ll.<init>(android.os.Looper, X.5Pu, X.5Pv, X.2Az, X.4bS, X.4vg, X.5QA, X.4M7, X.5QN, X.5Xd, X.5Yb[]):void");
    }

    public static void A00(C92874Xt r1, Object obj, int i, int i2) {
        r1.A02(new IDxEventShape18S0100000_2_I1(obj, i), i2);
    }

    public final int A01() {
        C95034cy r1 = this.A05;
        Timeline timeline = r1.A05;
        if (C12960it.A1T(timeline.A01())) {
            return this.A00;
        }
        return AnonymousClass4YJ.A00(this.A0C, timeline, r1.A07.A04);
    }

    public final Pair A02(Timeline timeline, int i, long j) {
        int i2 = i;
        if (C12960it.A1T(timeline.A01())) {
            this.A00 = i;
            if (j == -9223372036854775807L) {
                j = 0;
            }
            this.A04 = j;
            return null;
        }
        if (i == -1 || i >= timeline.A01()) {
            i2 = timeline.A05(false);
            timeline.A0B(super.A00, i2, 0);
            j = 0;
        }
        return timeline.A07(this.A0C, super.A00, i2, C95214dK.A01(j));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x009e, code lost:
        if (r27.A09(r3, r1, false).A00 != X.AnonymousClass4YJ.A00(r3, r27, r14.A04)) goto L_0x00a0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0010, code lost:
        if (r25 != null) goto L_0x0012;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.C95034cy A03(android.util.Pair r25, X.C95034cy r26, com.google.android.exoplayer2.Timeline r27) {
        /*
        // Method dump skipped, instructions count: 319
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C76513ll.A03(android.util.Pair, X.4cy, com.google.android.exoplayer2.Timeline):X.4cy");
    }

    public void A04(int i, int i2, boolean z) {
        C95034cy r1 = this.A05;
        if (r1.A0D != z || r1.A01 != i) {
            this.A02++;
            C95034cy A02 = r1.A02(i, z);
            ((C107914yA) this.A0B.A0Z).A00.obtainMessage(1, z ? 1 : 0, i).sendToTarget();
            A06(A02, 4, 0, i2, false, false);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0013, code lost:
        if (r8 > r3.size()) goto L_0x0015;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A05(X.AnonymousClass3A1 r24, boolean r25) {
        /*
        // Method dump skipped, instructions count: 296
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C76513ll.A05(X.3A1, boolean):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:46:0x011b, code lost:
        if (r5 == 0) goto L_0x011e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0123, code lost:
        if (r4 == 0) goto L_0x0126;
     */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0064  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0088 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00c0  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00df  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00ed  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00fb  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x010e  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0118 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0120 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0128  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x013a  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0144  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0154  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0160  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0047  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A06(X.C95034cy r18, int r19, int r20, int r21, boolean r22, boolean r23) {
        /*
        // Method dump skipped, instructions count: 466
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C76513ll.A06(X.4cy, int, int, int, boolean, boolean):void");
    }

    @Override // X.AbstractC47512Az
    public void A5h(AnonymousClass5XZ r4) {
        C92874Xt r1 = this.A0K;
        if (!r1.A00) {
            r1.A07.add(new AnonymousClass0PK(r1.A04, r4));
        }
    }

    @Override // X.AbstractC47512Az
    public long AB1() {
        long j;
        if (AJt()) {
            C95034cy r0 = this.A05;
            if (!r0.A06.equals(r0.A07)) {
                return ACb();
            }
            j = this.A05.A0F;
        } else {
            C95034cy r6 = this.A05;
            Timeline timeline = r6.A05;
            if (C12960it.A1T(timeline.A01())) {
                return this.A04;
            }
            if (r6.A06.A03 != r6.A07.A03) {
                j = C72463ee.A0B(super.A00, timeline, ACF()).A02;
            } else {
                long j2 = r6.A0F;
                C95034cy r7 = this.A05;
                C28741Ov r62 = r7.A06;
                if (r62.A00()) {
                    AnonymousClass4YJ A0A = r7.A05.A0A(this.A0C, r62.A04);
                    r7 = this.A05;
                    r62 = r7.A06;
                    j2 = A0A.A03.A02[r62.A00];
                    if (j2 == Long.MIN_VALUE) {
                        j2 = A0A.A01;
                    }
                }
                long A02 = C95214dK.A02(j2);
                Timeline timeline2 = r7.A05;
                Object obj = r62.A04;
                AnonymousClass4YJ r02 = this.A0C;
                timeline2.A0A(r02, obj);
                return A02 + C95214dK.A02(r02.A02);
            }
        }
        return C95214dK.A02(j);
    }

    @Override // X.AbstractC47512Az
    public long ABh() {
        if (!AJt()) {
            return AC9();
        }
        C95034cy r0 = this.A05;
        Timeline timeline = r0.A05;
        Object obj = r0.A07.A04;
        AnonymousClass4YJ r6 = this.A0C;
        timeline.A0A(r6, obj);
        C95034cy r3 = this.A05;
        long j = r3.A02;
        if (j != -9223372036854775807L) {
            return C95214dK.A02(r6.A02) + C95214dK.A02(j);
        }
        r3.A05.A0B(super.A00, ACF(), 0);
        return 0;
    }

    @Override // X.AbstractC47512Az
    public int AC2() {
        if (AJt()) {
            return this.A05.A07.A00;
        }
        return -1;
    }

    @Override // X.AbstractC47512Az
    public int AC3() {
        if (AJt()) {
            return this.A05.A07.A01;
        }
        return -1;
    }

    @Override // X.AbstractC47512Az
    public int AC8() {
        C95034cy r2 = this.A05;
        Timeline timeline = r2.A05;
        if (C12960it.A1T(timeline.A01())) {
            return 0;
        }
        return timeline.A04(r2.A07.A04);
    }

    @Override // X.AbstractC47512Az
    public long AC9() {
        C95034cy r1 = this.A05;
        if (C12960it.A1T(r1.A05.A01())) {
            return this.A04;
        }
        C28741Ov r5 = r1.A07;
        boolean A00 = r5.A00();
        long A02 = C95214dK.A02(r1.A0G);
        if (A00) {
            return A02;
        }
        Timeline timeline = this.A05.A05;
        Object obj = r5.A04;
        AnonymousClass4YJ r0 = this.A0C;
        timeline.A0A(r0, obj);
        return A02 + C95214dK.A02(r0.A02);
    }

    @Override // X.AbstractC47512Az
    public Timeline ACE() {
        return this.A05.A05;
    }

    @Override // X.AbstractC47512Az
    public int ACF() {
        int A01 = A01();
        if (A01 == -1) {
            return 0;
        }
        return A01;
    }

    @Override // X.AbstractC47512Az
    public long ACb() {
        long j;
        boolean AJt = AJt();
        C95034cy r0 = this.A05;
        if (AJt) {
            C28741Ov r3 = r0.A07;
            Timeline timeline = r0.A05;
            Object obj = r3.A04;
            AnonymousClass4YJ r2 = this.A0C;
            timeline.A0A(r2, obj);
            j = r2.A04(r3.A00, r3.A01);
        } else {
            Timeline timeline2 = r0.A05;
            if (C12960it.A1T(timeline2.A01())) {
                return -9223372036854775807L;
            }
            j = C72463ee.A0B(super.A00, timeline2, ACF()).A02;
        }
        return C95214dK.A02(j);
    }

    @Override // X.AbstractC47512Az
    public boolean AFj() {
        return this.A05.A0D;
    }

    @Override // X.AbstractC47512Az
    public int AFl() {
        return this.A05.A00;
    }

    @Override // X.AbstractC47512Az
    public long AHF() {
        return C95214dK.A02(this.A05.A0H);
    }

    @Override // X.AbstractC47512Az
    public boolean AJt() {
        return this.A05.A07.A00();
    }

    @Override // X.AbstractC47512Az
    public void AaK(AnonymousClass5XZ r8) {
        C92874Xt r6 = this.A0K;
        CopyOnWriteArraySet copyOnWriteArraySet = r6.A07;
        Iterator it = copyOnWriteArraySet.iterator();
        while (it.hasNext()) {
            AnonymousClass0PK r3 = (AnonymousClass0PK) it.next();
            Object obj = r3.A03;
            if (obj.equals(r8)) {
                AnonymousClass5SV r1 = r6.A03;
                r3.A02 = true;
                if (r3.A01) {
                    r1.AJ8(r3.A00, obj);
                }
                copyOnWriteArraySet.remove(r3);
            }
        }
    }

    @Override // X.AbstractC47512Az
    public void AbS(int i, long j) {
        C95034cy r3 = this.A05;
        Timeline timeline = r3.A05;
        if (i < 0 || (!C12960it.A1T(timeline.A01()) && i >= timeline.A01())) {
            throw new AnonymousClass4CR(timeline, i, j);
        }
        int i2 = 1;
        this.A02++;
        if (AJt()) {
            Log.w("ExoPlayerImpl", "seekTo ignored because an ad is playing");
            AnonymousClass4XP r32 = new AnonymousClass4XP(this.A05);
            r32.A00(1);
            C76513ll r2 = ((C106384vc) this.A0A).A00;
            ((C107914yA) r2.A0J).A00.post(new RunnableBRunnable0Shape10S0200000_I1(r2, 1, r32));
            return;
        }
        if (r3.A00 != 1) {
            i2 = 2;
        }
        C95034cy A03 = A03(A02(timeline, i, j), r3.A01(i2), timeline);
        C107914yA.A00(this.A0B.A0Z, 3, new C90684Ov(timeline, i, C95214dK.A01(j)));
        A06(A03, 1, 0, 1, true, true);
    }

    @Override // X.AbstractC47512Az
    public void AcW(boolean z) {
        A04(0, 1, z);
    }
}
