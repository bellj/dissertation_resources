package X;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.os.PersistableBundle;
import android.os.SystemClock;

/* renamed from: X.2lH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56602lH extends AbstractC15040mS {
    public boolean A00;
    public boolean A01;
    public Integer A02;
    public final AlarmManager A03 = ((AlarmManager) ((C15050mT) this).A00.A00.getSystemService("alarm"));

    public C56602lH(C14160kx r3) {
        super(r3);
    }

    private final int A00() {
        Integer num = this.A02;
        if (num == null) {
            num = Integer.valueOf(C12960it.A0c(String.valueOf(((C15050mT) this).A00.A00.getPackageName()), "analytics").hashCode());
            this.A02 = num;
        }
        return num.intValue();
    }

    public final void A0H() {
        this.A01 = false;
        try {
            AlarmManager alarmManager = this.A03;
            Context context = ((C15050mT) this).A00.A00;
            alarmManager.cancel(PendingIntent.getBroadcast(context, 0, C12990iw.A0E("com.google.android.gms.analytics.ANALYTICS_DISPATCH").setComponent(new ComponentName(context, "com.google.android.gms.analytics.AnalyticsReceiver")), C88394Fm.A00));
        } catch (NullPointerException unused) {
        }
        if (Build.VERSION.SDK_INT >= 24) {
            int A00 = A00();
            A0D("Cancelling job. JobID", Integer.valueOf(A00));
            ((JobScheduler) ((C15050mT) this).A00.A00.getSystemService("jobscheduler")).cancel(A00);
        }
    }

    public final void A0I() {
        A0G();
        C13020j0.A04("Receiver not registered", this.A00);
        long A0G = C12980iv.A0G(C88904Hw.A0U.A00());
        if (A0G > 0) {
            A0H();
            C14160kx r2 = ((C15050mT) this).A00;
            long elapsedRealtime = SystemClock.elapsedRealtime() + A0G;
            this.A01 = true;
            C88904Hw.A0H.A00();
            if (Build.VERSION.SDK_INT >= 24) {
                A09("Scheduling upload with JobScheduler");
                Context context = r2.A00;
                ComponentName componentName = new ComponentName(context, "com.google.android.gms.analytics.AnalyticsJobService");
                int A00 = A00();
                PersistableBundle persistableBundle = new PersistableBundle();
                persistableBundle.putString("action", "com.google.android.gms.analytics.ANALYTICS_DISPATCH");
                JobInfo build = new JobInfo.Builder(A00, componentName).setMinimumLatency(A0G).setOverrideDeadline(A0G + A0G).setExtras(persistableBundle).build();
                A0D("Scheduling job. JobID", Integer.valueOf(A00));
                AnonymousClass3GT.A00(build, context);
                return;
            }
            A09("Scheduling upload with AlarmManager");
            AlarmManager alarmManager = this.A03;
            Context context2 = r2.A00;
            alarmManager.setInexactRepeating(2, elapsedRealtime, A0G, PendingIntent.getBroadcast(context2, 0, C12990iw.A0E("com.google.android.gms.analytics.ANALYTICS_DISPATCH").setComponent(new ComponentName(context2, "com.google.android.gms.analytics.AnalyticsReceiver")), C88394Fm.A00));
        }
    }
}
