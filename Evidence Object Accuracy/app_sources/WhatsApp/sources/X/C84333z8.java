package X;

import com.whatsapp.Conversation;
import com.whatsapp.jid.UserJid;

/* renamed from: X.3z8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C84333z8 extends AnonymousClass2Cu {
    public final /* synthetic */ Conversation A00;

    public C84333z8(Conversation conversation) {
        this.A00 = conversation;
    }

    @Override // X.AnonymousClass2Cu
    public void A01(UserJid userJid) {
        Conversation conversation = this.A00;
        if (userJid != null && userJid.equals(conversation.A2s)) {
            conversation.A1y.A04();
            conversation.invalidateOptionsMenu();
        }
    }
}
