package X;

import java.util.concurrent.Executor;

/* renamed from: X.4ca  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C94814ca {
    public static final C94814ca A03 = new C94814ca();
    public C94814ca A00;
    public final Runnable A01;
    public final Executor A02;

    public C94814ca() {
        this.A01 = null;
        this.A02 = null;
    }

    public C94814ca(Runnable runnable, Executor executor) {
        this.A01 = runnable;
        this.A02 = executor;
    }
}
