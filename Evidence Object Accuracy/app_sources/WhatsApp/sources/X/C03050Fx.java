package X;

import android.graphics.Rect;

/* renamed from: X.0Fx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C03050Fx extends AnonymousClass0OE {
    public final /* synthetic */ Rect A00;
    public final /* synthetic */ AnonymousClass0EH A01;

    public C03050Fx(Rect rect, AnonymousClass0EH r2) {
        this.A01 = r2;
        this.A00 = rect;
    }

    @Override // X.AnonymousClass0OE
    public Rect A00(AnonymousClass072 r3) {
        Rect rect = this.A00;
        if (rect == null || rect.isEmpty()) {
            return null;
        }
        return rect;
    }
}
