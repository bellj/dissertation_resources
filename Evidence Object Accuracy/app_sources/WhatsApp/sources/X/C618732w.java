package X;

import android.location.Location;
import com.whatsapp.location.LocationPicker;
import com.whatsapp.nativelibloader.WhatsAppLibLoader;

/* renamed from: X.32w  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C618732w extends AbstractC36001jA {
    public AbstractC12500i1 A00 = new C105784uc(this);
    public final /* synthetic */ LocationPicker A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C618732w(AnonymousClass12P r34, AbstractC15710nm r35, C244615p r36, C14900mE r37, C15570nT r38, C18790t3 r39, C16170oZ r40, AnonymousClass130 r41, C22700zV r42, AnonymousClass131 r43, AnonymousClass01d r44, C14830m7 r45, C16590pI r46, C15890o4 r47, C14820m6 r48, AnonymousClass018 r49, C15650ng r50, AnonymousClass19M r51, C231510o r52, AnonymousClass193 r53, C14850m9 r54, C253719d r55, C18810t5 r56, LocationPicker locationPicker, C16030oK r58, C244415n r59, AnonymousClass3DJ r60, WhatsAppLibLoader whatsAppLibLoader, C16630pM r62, C252018m r63, C252718t r64, AbstractC14440lR r65) {
        super(r34, r35, r36, r37, r38, r39, r40, r41, r42, r43, r44, r45, r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r56, r58, r59, r60, whatsAppLibLoader, r62, r63, r64, r65);
        this.A01 = locationPicker;
    }

    @Override // X.AbstractC36001jA, android.location.LocationListener
    public void onLocationChanged(Location location) {
        AnonymousClass04Q r6;
        if (location != null) {
            LocationPicker locationPicker = this.A01;
            if (locationPicker.A0M.A06 == null && (r6 = locationPicker.A03) != null) {
                AnonymousClass03T r4 = new AnonymousClass03T(location.getLatitude(), location.getLongitude());
                C05200Oq r0 = new C05200Oq();
                r0.A06 = r4;
                r6.A0A(r0);
            }
            if (locationPicker.A0M.A0s && locationPicker.A03 != null) {
                if (locationPicker.A07 == null) {
                    A04();
                }
                LocationPicker.A03(new AnonymousClass03T(location.getLatitude(), location.getLongitude()), locationPicker);
            }
            if (locationPicker.A0M.A0r && locationPicker.A03 != null) {
                AnonymousClass03T r2 = new AnonymousClass03T(location.getLatitude(), location.getLongitude());
                AnonymousClass04Q r1 = locationPicker.A03;
                C05200Oq r02 = new C05200Oq();
                r02.A06 = r2;
                r1.A09(r02);
            }
            super.onLocationChanged(location);
        }
    }
}
