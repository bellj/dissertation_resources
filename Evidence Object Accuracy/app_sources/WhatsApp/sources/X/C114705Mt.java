package X;

/* renamed from: X.5Mt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114705Mt extends AnonymousClass1TM {
    public final AnonymousClass5N1[] A00;

    public C114705Mt(AnonymousClass5N1 r3) {
        this.A00 = new AnonymousClass5N1[]{r3};
    }

    public C114705Mt(AbstractC114775Na r4) {
        this.A00 = new AnonymousClass5N1[r4.A0B()];
        for (int i = 0; i != r4.A0B(); i++) {
            this.A00[i] = AnonymousClass5N1.A01(r4.A0D(i));
        }
    }

    public static C114705Mt A00(Object obj) {
        if (obj instanceof C114705Mt) {
            return (C114705Mt) obj;
        }
        if (obj != null) {
            return new C114705Mt(AbstractC114775Na.A04(obj));
        }
        return null;
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        return new AnonymousClass5NZ(this.A00);
    }

    public static AnonymousClass5N1[] A01(Object obj) {
        AnonymousClass5N1[] r3 = A00(obj).A00;
        int length = r3.length;
        AnonymousClass5N1[] r1 = new AnonymousClass5N1[length];
        System.arraycopy(r3, 0, r1, 0, length);
        return r1;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        String str = AnonymousClass1T7.A00;
        stringBuffer.append("GeneralNames:");
        stringBuffer.append(str);
        int i = 0;
        while (true) {
            AnonymousClass5N1[] r1 = this.A00;
            if (i == r1.length) {
                return stringBuffer.toString();
            }
            stringBuffer.append("    ");
            stringBuffer.append(r1[i]);
            stringBuffer.append(str);
            i++;
        }
    }
}
