package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

/* renamed from: X.09D  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass09D extends AnimatorListenerAdapter {
    public boolean A00 = false;
    public final /* synthetic */ AnonymousClass0F7 A01;

    public AnonymousClass09D(AnonymousClass0F7 r2) {
        this.A01 = r2;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
        this.A00 = true;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        if (this.A00) {
            this.A00 = false;
            return;
        }
        AnonymousClass0F7 r2 = this.A01;
        if (((Number) r2.A0K.getAnimatedValue()).floatValue() == 0.0f) {
            r2.A02 = 0;
            r2.A04(0);
            return;
        }
        r2.A02 = 2;
        r2.A0B.invalidate();
    }
}
