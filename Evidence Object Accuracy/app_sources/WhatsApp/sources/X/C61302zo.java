package X;

import com.facebook.redex.IDxComparatorShape3S0000000_2_I1;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/* renamed from: X.2zo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C61302zo extends AbstractC451920n {
    public final C241414j A00;
    public final Set A01;

    public C61302zo(AbstractC451720l r3, C241414j r4, AbstractC248317b r5, AbstractC14440lR r6, List list) {
        super(r3, r4, r5, r6);
        this.A00 = r4;
        TreeSet treeSet = new TreeSet(new IDxComparatorShape3S0000000_2_I1(14));
        this.A01 = treeSet;
        treeSet.addAll(list);
    }
}
