package X;

import android.text.TextUtils;
import android.view.MenuItem;
import androidx.viewpager.widget.ViewPager;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.gallery.MediaGalleryActivity;

/* renamed from: X.3TX  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3TX implements AbstractC467627l {
    public String A00 = "";
    public boolean A01 = true;
    public final /* synthetic */ ViewPager A02;
    public final /* synthetic */ MediaGalleryActivity A03;

    @Override // X.AbstractC467727m
    public void AXN(AnonymousClass3FN r1) {
    }

    public AnonymousClass3TX(ViewPager viewPager, MediaGalleryActivity mediaGalleryActivity) {
        this.A03 = mediaGalleryActivity;
        this.A02 = viewPager;
    }

    @Override // X.AbstractC467727m
    public void AXO(AnonymousClass3FN r7) {
        this.A02.setCurrentItem(r7.A00);
        MediaGalleryActivity mediaGalleryActivity = this.A03;
        int i = r7.A00;
        mediaGalleryActivity.A00 = i;
        if (i != mediaGalleryActivity.A02) {
            RequestPermissionActivity.A0W(mediaGalleryActivity, mediaGalleryActivity.A0J);
        }
        int i2 = mediaGalleryActivity.A00;
        int i3 = mediaGalleryActivity.A03;
        MenuItem menuItem = mediaGalleryActivity.A04;
        if (i2 == i3) {
            if (menuItem != null) {
                if (menuItem.isActionViewExpanded()) {
                    this.A00 = mediaGalleryActivity.A0o;
                    mediaGalleryActivity.A04.collapseActionView();
                }
                mediaGalleryActivity.A04.setVisible(false);
            }
            this.A01 = true;
            return;
        }
        if (menuItem != null) {
            menuItem.setVisible(true);
            if (!TextUtils.isEmpty(mediaGalleryActivity.A0o) || TextUtils.isEmpty(this.A00) || !this.A01) {
                AbstractC35481hz A02 = MediaGalleryActivity.A02(mediaGalleryActivity);
                if (A02 != null) {
                    C15250mo r1 = mediaGalleryActivity.A0M;
                    r1.A03(mediaGalleryActivity.A0o);
                    r1.A04(mediaGalleryActivity.A0p);
                    A02.AVc(r1);
                }
            } else {
                mediaGalleryActivity.A0o = this.A00;
                mediaGalleryActivity.A04.expandActionView();
                C12960it.A0J(mediaGalleryActivity.A04.getActionView(), R.id.search_src_text).setText(mediaGalleryActivity.A0o);
            }
        }
        this.A01 = false;
    }
}
