package X;

/* renamed from: X.5M8  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5M8 extends AnonymousClass5NB {
    public AnonymousClass5M8(byte[] bArr, int i, boolean z) {
        super(bArr, i, z);
    }

    @Override // X.AnonymousClass1TL
    public void A08(AnonymousClass1TP r4, boolean z) {
        int i = 64;
        if (this.A01) {
            i = 96;
        }
        r4.A05(this.A02, i, this.A00, z);
    }
}
