package X;

/* renamed from: X.0UN  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0UN {
    public static AbstractC12710iN A00 = C08640bY.A01;

    public static void A00(Class cls, Object obj, Object obj2, Object obj3, String str) {
        if (A00.AJi(2)) {
            String format = String.format(null, str, obj, obj2, obj3);
            AbstractC12710iN r1 = A00;
            if (r1.AJi(2)) {
                r1.Aff(cls.getSimpleName(), format);
            }
        }
    }

    public static void A01(Class cls, Object obj, Object obj2, String str) {
        AbstractC12710iN r3 = A00;
        if (r3.AJi(2)) {
            r3.Aff(cls.getSimpleName(), String.format(null, str, obj, obj2));
        }
    }

    public static void A02(Class cls, Object obj, String str) {
        AbstractC12710iN r3 = A00;
        if (r3.AJi(2)) {
            r3.Aff(cls.getSimpleName(), String.format(null, str, obj));
        }
    }

    public static void A03(String str, String str2, Object... objArr) {
        AbstractC12710iN r1 = A00;
        if (r1.AJi(5)) {
            r1.Ag0(str, String.format(null, str2, objArr));
        }
    }

    public static void A04(String str, String str2, Object... objArr) {
        AbstractC12710iN r1 = A00;
        if (r1.AJi(6)) {
            r1.AgJ(str, String.format(null, str2, objArr));
        }
    }
}
