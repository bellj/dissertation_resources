package X;

import java.util.Collections;
import java.util.List;

/* renamed from: X.4xr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107734xr implements AbstractC116805Wy {
    public final List A00;
    public final List A01;

    public C107734xr(List list, List list2) {
        this.A01 = list;
        this.A00 = list2;
    }

    @Override // X.AbstractC116805Wy
    public List ABx(long j) {
        int i;
        List list = this.A00;
        Long valueOf = Long.valueOf(j);
        int binarySearch = Collections.binarySearch(list, valueOf);
        if (binarySearch < 0) {
            i = -(binarySearch + 2);
        } else {
            do {
                binarySearch--;
                if (binarySearch < 0) {
                    break;
                }
            } while (((Comparable) list.get(binarySearch)).compareTo(valueOf) == 0);
            i = binarySearch + 1;
        }
        if (i == -1) {
            return Collections.emptyList();
        }
        return (List) this.A01.get(i);
    }

    @Override // X.AbstractC116805Wy
    public long ACn(int i) {
        boolean z = true;
        C95314dV.A03(C12990iw.A1W(i));
        List list = this.A00;
        if (i >= list.size()) {
            z = false;
        }
        C95314dV.A03(z);
        return C72453ed.A0Z(list, i);
    }

    @Override // X.AbstractC116805Wy
    public int ACo() {
        return this.A00.size();
    }

    @Override // X.AbstractC116805Wy
    public int AEd(long j) {
        List list = this.A00;
        Long valueOf = Long.valueOf(j);
        int binarySearch = Collections.binarySearch(list, valueOf);
        if (binarySearch >= 0) {
            int size = list.size();
            do {
                binarySearch++;
                if (binarySearch >= size) {
                    break;
                }
            } while (((Comparable) list.get(binarySearch)).compareTo(valueOf) == 0);
        } else {
            binarySearch ^= -1;
        }
        if (binarySearch >= list.size()) {
            return -1;
        }
        return binarySearch;
    }
}
