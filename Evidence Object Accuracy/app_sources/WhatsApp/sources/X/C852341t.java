package X;

import android.content.DialogInterface;
import com.whatsapp.voipcalling.VoipActivityV2;

/* renamed from: X.41t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C852341t extends C50102Ob {
    public final /* synthetic */ VoipActivityV2 A00;

    public C852341t(VoipActivityV2 voipActivityV2) {
        this.A00 = voipActivityV2;
    }

    @Override // X.C50102Ob
    public void A01(DialogInterface dialogInterface) {
        dialogInterface.dismiss();
        this.A00.finish();
    }
}
