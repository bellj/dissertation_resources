package X;

import android.net.Uri;
import java.lang.ref.WeakReference;

/* renamed from: X.1pg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C39331pg extends AbstractC16350or {
    public final Uri A00;
    public final C14900mE A01;
    public final AnonymousClass018 A02;
    public final AbstractC39321pf A03;
    public final C22190yg A04;
    public final WeakReference A05;

    public C39331pg(Uri uri, AbstractC13860kS r3, C14900mE r4, AnonymousClass018 r5, AbstractC39321pf r6, C22190yg r7) {
        this.A01 = r4;
        this.A04 = r7;
        this.A02 = r5;
        this.A05 = new WeakReference(r3);
        this.A00 = uri;
        this.A03 = r6;
    }
}
