package X;

import android.text.method.LinkMovementMethod;
import android.widget.TextView;

/* renamed from: X.2aM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52162aM extends LinkMovementMethod {
    public AbstractC116465Vn A00;
    public Runnable A01 = null;

    public static void A00(TextView textView) {
        textView.setMovementMethod(new C52162aM());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x008a, code lost:
        if (r3 > 1) goto L_0x008c;
     */
    @Override // android.text.method.LinkMovementMethod, android.text.method.ScrollingMovementMethod, android.text.method.BaseMovementMethod, android.text.method.MovementMethod
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.widget.TextView r18, android.text.Spannable r19, android.view.MotionEvent r20) {
        /*
            r17 = this;
            java.lang.Class<X.5Vn> r12 = X.AbstractC116465Vn.class
            r7 = r20
            int r9 = r7.getAction()
            r0 = 3
            r8 = r17
            r16 = r18
            if (r9 != r0) goto L_0x0018
            X.5Vn r1 = r8.A00
            if (r1 == 0) goto L_0x0018
            r0 = r16
            r1.AXY(r7, r0)
        L_0x0018:
            r10 = 0
            r0 = 1
            if (r9 == r0) goto L_0x001f
            if (r9 == 0) goto L_0x001f
        L_0x001e:
            return r10
        L_0x001f:
            float r0 = r7.getX()
            int r15 = (int) r0
            float r0 = r7.getY()
            int r1 = (int) r0
            int r0 = r16.getTotalPaddingLeft()
            int r15 = r15 - r0
            int r0 = r16.getTotalPaddingTop()
            int r1 = r1 - r0
            int r0 = r16.getScrollX()
            int r15 = r15 + r0
            int r0 = r16.getScrollY()
            int r1 = r1 + r0
            android.text.Layout r14 = r16.getLayout()
            int r11 = r14.getLineForVertical(r1)
            int r6 = r14.getLineStart(r11)
            int r5 = r14.getLineEnd(r11)
            r13 = r19
            java.lang.Object[] r4 = r13.getSpans(r6, r5, r12)
            X.5Vn[] r4 = (X.AbstractC116465Vn[]) r4
            int r3 = r4.length
            if (r3 == 0) goto L_0x001e
            int r0 = r5 - r6
            r2 = 256(0x100, float:3.59E-43)
            if (r0 <= r2) goto L_0x0090
        L_0x005e:
            if (r6 >= r5) goto L_0x0090
            char r1 = r13.charAt(r6)
            r0 = 8206(0x200e, float:1.1499E-41)
            if (r1 == r0) goto L_0x0084
            r0 = 8207(0x200f, float:1.15E-41)
            if (r1 == r0) goto L_0x0084
            r0 = 1564(0x61c, float:2.192E-42)
            if (r1 == r0) goto L_0x0084
            r0 = 1807(0x70f, float:2.532E-42)
            if (r1 == r0) goto L_0x0084
            r0 = 8234(0x202a, float:1.1538E-41)
            if (r1 < r0) goto L_0x007c
            r0 = 8238(0x202e, float:1.1544E-41)
            if (r1 <= r0) goto L_0x0084
        L_0x007c:
            r0 = 8294(0x2066, float:1.1622E-41)
            if (r1 < r0) goto L_0x008d
            r0 = 8297(0x2069, float:1.1627E-41)
            if (r1 > r0) goto L_0x008d
        L_0x0084:
            int r10 = r10 + 1
            if (r10 <= r2) goto L_0x008d
            r10 = 1
            r1 = 0
            if (r3 <= r10) goto L_0x009d
        L_0x008c:
            return r1
        L_0x008d:
            int r6 = r6 + 1
            goto L_0x005e
        L_0x0090:
            float r0 = (float) r15
            int r0 = r14.getOffsetForHorizontal(r11, r0)
            java.lang.Object[] r4 = r13.getSpans(r0, r0, r12)
            X.5Vn[] r4 = (X.AbstractC116465Vn[]) r4
            r1 = 0
            r10 = 1
        L_0x009d:
            int r0 = r4.length
            if (r0 == 0) goto L_0x008c
            java.lang.Runnable r0 = r8.A01
            if (r0 == 0) goto L_0x00a9
            if (r9 != r10) goto L_0x00a9
            r0.run()
        L_0x00a9:
            r1 = r4[r1]
            r8.A00 = r1
            r0 = r16
            r1.AXY(r7, r0)
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C52162aM.onTouchEvent(android.widget.TextView, android.text.Spannable, android.view.MotionEvent):boolean");
    }
}
