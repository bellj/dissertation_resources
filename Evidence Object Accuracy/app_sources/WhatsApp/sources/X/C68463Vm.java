package X;

import com.whatsapp.util.Log;
import java.util.List;

/* renamed from: X.3Vm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68463Vm implements AnonymousClass2K1 {
    public final /* synthetic */ AnonymousClass2K0 A00;

    public /* synthetic */ C68463Vm(AnonymousClass2K0 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass2K1
    public void APl(int i) {
        C68553Vv r2 = this.A00.A06;
        if (r2 != null) {
            AnonymousClass4N6 r1 = (AnonymousClass4N6) r2.A07.get(C12960it.A0V());
            if (r1 != null) {
                r1.A00 = 2;
            }
            r2.A03(i);
        }
    }

    @Override // X.AnonymousClass2K1
    public /* bridge */ /* synthetic */ void AX4(Object obj) {
        List list = (List) obj;
        C68553Vv r4 = this.A00.A06;
        if (r4 != null) {
            synchronized (C68553Vv.class) {
                AnonymousClass4N6 r2 = (AnonymousClass4N6) C12990iw.A0l(r4.A07, 1);
                if (list.isEmpty()) {
                    C91524Sb r1 = r4.A05;
                    r1.A01 = 4;
                    r1.A00 = 3;
                    if (r2 != null) {
                        r2.A00 = 2;
                    }
                    Log.e("HomeWidgetsDelegate/onFetchWidgetsSuccess widgets list cannot be empty");
                    r4.A01();
                } else {
                    if (r2 != null) {
                        r2.A00 = 1;
                        r2.A01 = list;
                    }
                    r4.A02();
                }
            }
        }
    }
}
