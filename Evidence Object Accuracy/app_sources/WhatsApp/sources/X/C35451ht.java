package X;

import com.facebook.redex.RunnableBRunnable0Shape5S0100000_I0_5;
import java.util.HashMap;

/* renamed from: X.1ht  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C35451ht {
    public boolean A00;
    public final C14900mE A01;
    public final AbstractC18860tB A02;
    public final AnonymousClass12H A03;
    public final HashMap A04 = new HashMap();

    public C35451ht(C14900mE r2, AbstractC35461hu r3, C35451ht r4, AnonymousClass12H r5) {
        if (r4 != null) {
            r4.A00();
        }
        this.A01 = r2;
        this.A03 = r5;
        this.A00 = false;
        C35341hf r0 = new C35341hf(r3, this);
        this.A02 = r0;
        r5.A03(r0);
    }

    public void A00() {
        this.A00 = true;
        C14900mE r2 = this.A01;
        r2.A02.post(new RunnableBRunnable0Shape5S0100000_I0_5(this, 13));
    }
}
