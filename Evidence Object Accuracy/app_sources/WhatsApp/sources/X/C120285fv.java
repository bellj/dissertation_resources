package X;

import android.content.Context;
import com.whatsapp.R;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.5fv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120285fv extends AnonymousClass17T {
    public final AnonymousClass12P A00;
    public final C14900mE A01;
    public final AnonymousClass102 A02;
    public final C18610sj A03;
    public final C17070qD A04;
    public final C120905gw A05;
    public final C129255xQ A06;
    public final AnonymousClass17Q A07;
    public final C130065yk A08;
    public final C18590sh A09;
    public final AnonymousClass17R A0A;

    public C120285fv(AnonymousClass12P r1, C14900mE r2, AnonymousClass102 r3, C18610sj r4, C17070qD r5, C120905gw r6, C129255xQ r7, AnonymousClass17Q r8, C130065yk r9, C18590sh r10, AnonymousClass17R r11) {
        this.A01 = r2;
        this.A07 = r8;
        this.A0A = r11;
        this.A00 = r1;
        this.A09 = r10;
        this.A04 = r5;
        this.A03 = r4;
        this.A02 = r3;
        this.A05 = r6;
        this.A08 = r9;
        this.A06 = r7;
    }

    @Override // X.AnonymousClass17T
    public void A00(Context context, String str) {
        C14900mE r4 = this.A01;
        r4.A06(0, R.string.loading_spinner);
        HashMap A11 = C12970iu.A11();
        A11.put("action", "start");
        HashMap A112 = C12970iu.A11();
        A112.put("type", "modal");
        HashMap A113 = C12970iu.A11();
        A113.put("style", A112);
        A11.put("presentation", A113);
        HashMap A114 = C12970iu.A11();
        A114.put("device_id", this.A09.A01());
        AnonymousClass17Q r5 = this.A07;
        if (!A114.isEmpty()) {
            r5.A0C.putAll(A114);
        }
        AnonymousClass3FB r8 = new AnonymousClass3FB("br_merchant_onboarding", null, A11);
        C133726Bw r3 = new AbstractC116075Ua() { // from class: X.6Bw
            @Override // X.AbstractC116075Ua
            public final void AZa(AnonymousClass1V8 r6) {
                C120285fv r42 = C120285fv.this;
                AnonymousClass1V8 A0E = r6.A0E("pay");
                AnonymousClass009.A05(A0E);
                AnonymousClass1V8 A0E2 = A0E.A0E("merchant");
                AnonymousClass009.A05(A0E2);
                C119785f6 r2 = new C119785f6();
                r2.A01(r42.A02, A0E2, 0);
                r42.A04.A00().A03(null, r2.A05());
            }
        };
        C133676Br r6 = new AnonymousClass5UY() { // from class: X.6Br
            @Override // X.AnonymousClass5UY
            public final void AQc(String str2) {
                C120285fv.this.A01.A03();
            }
        };
        C14580lf r2 = new C14580lf();
        r5.A0D.put("BRMerchantData", r3);
        r5.A06(r6, new AnonymousClass5UZ(r2, this) { // from class: X.6Bv
            public final /* synthetic */ C14580lf A00;
            public final /* synthetic */ C120285fv A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AnonymousClass5UZ
            public final void AQd(Map map) {
                Boolean bool;
                C120285fv r62 = this.A01;
                C14580lf r52 = this.A00;
                r62.A07.A0D.remove("BRMerchantData");
                if (map == null) {
                    C117305Zk.A1N("BrazilPaymentMerchantHelper", "triggerMerchantOnboarding :: terminalParams is null");
                    bool = Boolean.FALSE;
                } else {
                    AnonymousClass3FA r0 = (AnonymousClass3FA) map.get(AnonymousClass49R.A00.key);
                    if (r0 != null) {
                        if (489 == r0.A00) {
                            r62.A03.A08(new C1328068h(r52, r62), 2);
                        } else {
                            r52.A02(Boolean.FALSE);
                        }
                    }
                    bool = Boolean.TRUE;
                }
                r52.A02(bool);
            }
        }, r8, str, null);
        C117315Zl.A0R(r4, r2, new AbstractC14590lg(context, this) { // from class: X.6EO
            public final /* synthetic */ Context A00;
            public final /* synthetic */ C120285fv A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC14590lg
            public final void accept(Object obj) {
                String str2;
                Context context2 = this.A00;
                if (!C12970iu.A1Y(obj)) {
                    str2 = "triggerMerchantOnboarding -> merchant onboarding failed. Something went wrong";
                } else if (context2 == null) {
                    str2 = "triggerMerchantOnboarding -> the context became null";
                } else {
                    throw new UnsupportedOperationException();
                }
                C117305Zk.A1N("BrazilPaymentMerchantHelper", str2);
            }
        });
    }

    @Override // X.AnonymousClass17T
    public void A01(AnonymousClass1FK r2) {
        if (this.A08.A06.A03()) {
            this.A06.A00(r2);
        } else {
            this.A05.A00(r2);
        }
    }
}
