package X;

import android.os.CountDownTimer;
import com.whatsapp.CircularProgressBar;
import com.whatsapp.spamwarning.SpamWarningActivity;

/* renamed from: X.2Zv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class CountDownTimerC51962Zv extends CountDownTimer {
    public final /* synthetic */ CircularProgressBar A00;
    public final /* synthetic */ SpamWarningActivity A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public CountDownTimerC51962Zv(CircularProgressBar circularProgressBar, SpamWarningActivity spamWarningActivity, long j) {
        super(j, 10);
        this.A01 = spamWarningActivity;
        this.A00 = circularProgressBar;
    }

    @Override // android.os.CountDownTimer
    public void onFinish() {
        this.A01.finish();
    }

    @Override // android.os.CountDownTimer
    public void onTick(long j) {
        int i = (int) j;
        SpamWarningActivity spamWarningActivity = this.A01;
        CircularProgressBar circularProgressBar = this.A00;
        circularProgressBar.setCenterText(C38131nZ.A04(((ActivityC13830kP) spamWarningActivity).A01, (long) (i / 1000)));
        circularProgressBar.setProgress(i);
    }
}
