package X;

import java.util.List;

/* renamed from: X.0aj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08190aj implements AbstractC12040hH {
    public final float A00;
    public final AnonymousClass0H4 A01;
    public final AnonymousClass0H9 A02;
    public final AnonymousClass0H9 A03;
    public final AnonymousClass0HA A04;
    public final EnumC03720Iw A05;
    public final EnumC03820Jg A06;
    public final String A07;
    public final List A08;
    public final boolean A09;

    public C08190aj(AnonymousClass0H4 r1, AnonymousClass0H9 r2, AnonymousClass0H9 r3, AnonymousClass0HA r4, EnumC03720Iw r5, EnumC03820Jg r6, String str, List list, float f, boolean z) {
        this.A07 = str;
        this.A02 = r2;
        this.A08 = list;
        this.A01 = r1;
        this.A04 = r4;
        this.A03 = r3;
        this.A05 = r5;
        this.A06 = r6;
        this.A00 = f;
        this.A09 = z;
    }

    @Override // X.AbstractC12040hH
    public AbstractC12470hy Aes(AnonymousClass0AA r2, AbstractC08070aX r3) {
        return new C03210Gq(r2, this, r3);
    }
}
