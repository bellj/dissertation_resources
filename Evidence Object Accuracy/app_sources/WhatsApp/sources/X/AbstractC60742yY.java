package X;

import android.content.Context;
import android.text.TextUtils;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.2yY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC60742yY extends AnonymousClass42A {
    public abstract void A1M(List list, boolean z);

    public abstract int getMaxAlbumSize();

    @Override // X.AnonymousClass1OY
    public abstract int getMessageCount();

    public abstract int getMinAlbumSize();

    public AbstractC60742yY(Context context, AbstractC13890kV r2, AbstractC16130oV r3) {
        super(context, r2, r3);
    }

    public static int A0Y(AbstractC15340mz r4) {
        byte b = r4.A0y;
        if (b == 1 || b == 3) {
            return TextUtils.isEmpty(((AbstractC16130oV) r4).A15()) ? 1 : -1;
        }
        if (b == 20 && r4.A0L == null) {
            return 2;
        }
        return -1;
    }

    @Override // X.AnonymousClass1OY
    public boolean A1L(AnonymousClass1IS r3) {
        if (!(this instanceof C60832yi)) {
            List list = ((AnonymousClass2y2) this).A05;
            if (list == null) {
                return false;
            }
            Iterator it = list.iterator();
            while (it.hasNext()) {
                if (C12980iv.A0f(it).A0z.equals(r3)) {
                    return true;
                }
            }
            return false;
        }
        List list2 = ((C60832yi) this).A06;
        if (list2 == null) {
            return false;
        }
        Iterator it2 = list2.iterator();
        while (it2.hasNext()) {
            if (C12980iv.A0f(it2).A0z.equals(r3)) {
                return true;
            }
        }
        return false;
    }
}
