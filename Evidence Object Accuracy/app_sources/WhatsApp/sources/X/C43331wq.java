package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* renamed from: X.1wq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C43331wq extends BroadcastReceiver {
    public final /* synthetic */ C19890uq A00;

    public C43331wq(C19890uq r1) {
        this.A00 = r1;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if ("com.whatsapp.MessageHandler.CONNECTIVITY_RETRY_ACTION".equals(intent.getAction())) {
            this.A00.A0f.AaZ();
        }
    }
}
