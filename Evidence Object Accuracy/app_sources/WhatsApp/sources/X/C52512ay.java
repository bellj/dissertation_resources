package X;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.View;

/* renamed from: X.2ay  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52512ay extends View implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;
    public boolean A02;
    public final Rect A03;
    public final /* synthetic */ AnonymousClass1OY A04;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C52512ay(Context context, AnonymousClass1OY r3) {
        super(context);
        this.A04 = r3;
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
        this.A03 = C12980iv.A0J();
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View
    public boolean isSelected() {
        return this.A02;
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        int A02;
        super.onDraw(canvas);
        if (this.A02) {
            Rect rect = this.A03;
            getDrawingRect(rect);
            AnonymousClass1OY r3 = this.A04;
            if (((AbstractC28551Oa) r3).A0R) {
                rect.top += r3.getPaddingTop();
                A02 = rect.bottom - r3.getPaddingBottom();
            } else {
                C64533Fx r5 = ((AbstractC28551Oa) r3).A0b;
                if (r5.A04()) {
                    if (r3.A1V) {
                        rect.top += ((AbstractC28551Oa) r3).A0D.getTop();
                    }
                    if (r3.A1K()) {
                        Rect rect2 = ((AbstractC28551Oa) r3).A09;
                        rect.top = rect2.top - r5.A03(getContext());
                        int A022 = rect2.bottom + r5.A02(getContext());
                        rect.bottom = A022;
                        C53182d3 r0 = ((AbstractC28551Oa) r3).A0H;
                        if (r0 != null) {
                            rect.bottom = A022 + (r0.getHeight() - r3.getReactionsViewVerticalOverlap());
                        }
                    } else {
                        rect.top -= r5.A03(getContext());
                        A02 = rect.bottom + r5.A02(getContext());
                    }
                }
                canvas.drawRect(rect, ((AbstractC28551Oa) r3).A0b.A00);
            }
            rect.bottom = A02;
            canvas.drawRect(rect, ((AbstractC28551Oa) r3).A0b.A00);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0012, code lost:
        if (r2.isPressed() == false) goto L_0x0014;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setRowSelected(boolean r4) {
        /*
            r3 = this;
            boolean r0 = r3.A02
            if (r0 == r4) goto L_0x001b
            r3.A02 = r4
            r3.setSelected(r4)
            X.1OY r2 = r3.A04
            if (r4 != 0) goto L_0x0014
            boolean r1 = r2.isPressed()
            r0 = 1
            if (r1 != 0) goto L_0x0015
        L_0x0014:
            r0 = 0
        L_0x0015:
            r2.dispatchSetPressed(r0)
            r3.invalidate()
        L_0x001b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C52512ay.setRowSelected(boolean):void");
    }
}
