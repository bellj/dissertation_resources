package X;

import android.util.Base64;
import com.whatsapp.util.Log;

/* renamed from: X.1Wv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30291Wv extends AbstractC15340mz implements AbstractC16420oz, AbstractC30301Ww {
    public long A00;
    public String A01;
    public String A02;
    public String A03;

    public C30291Wv(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 69, j);
    }

    public C30291Wv(C40831sP r7, AnonymousClass1IS r8, long j) {
        super(r8, (byte) 69, j);
        int i = r7.A00;
        if ((i & 1) == 1) {
            this.A03 = r7.A0J;
        }
        if ((i & 16) == 16) {
            this.A00 = r7.A04;
        }
        if ((i & 8) == 8) {
            byte[] A04 = r7.A07.A04();
            int length = A04.length;
            if (length != 32) {
                StringBuilder sb = new StringBuilder("FMessageMediaExpessPathNotify/bogus sha-256 hash received; length=");
                sb.append(length);
                sb.append("; message.key=");
                sb.append(r8);
                Log.w(sb.toString());
            } else {
                this.A02 = Base64.encodeToString(A04, 2);
            }
        }
        if ((r7.A00 & 256) == 256) {
            byte[] A042 = r7.A06.A04();
            int length2 = A042.length;
            if (length2 != 32) {
                StringBuilder sb2 = new StringBuilder("FMessageMediaExpessPathNotify/bogus sha-256 enc hash received; length=");
                sb2.append(length2);
                sb2.append("; message.key=");
                sb2.append(r8);
                Log.e(sb2.toString());
                return;
            }
            this.A01 = Base64.encodeToString(A042, 2);
        }
    }

    @Override // X.AbstractC16420oz
    public void A6k(C39971qq r7) {
        if (this.A03 == null || this.A02 == null || this.A01 == null || this.A00 <= 0) {
            Log.w("FMessageMediaExpressPathNotify/buildE2EMessage unable to send media express path noitfy message due to missing url or fileHash");
            return;
        }
        AnonymousClass1G3 r4 = r7.A03;
        C40831sP r0 = ((C27081Fy) r4.A00).A0C;
        if (r0 == null) {
            r0 = C40831sP.A0L;
        }
        AnonymousClass1G4 A0T = r0.A0T();
        String str = this.A03;
        A0T.A03();
        C40831sP r1 = (C40831sP) A0T.A00;
        r1.A00 |= 1;
        r1.A0J = str;
        long j = this.A00;
        A0T.A03();
        C40831sP r5 = (C40831sP) A0T.A00;
        r5.A00 |= 16;
        r5.A04 = j;
        try {
            byte[] decode = Base64.decode(this.A02, 2);
            AbstractC27881Jp A01 = AbstractC27881Jp.A01(decode, 0, decode.length);
            A0T.A03();
            C40831sP r12 = (C40831sP) A0T.A00;
            r12.A00 |= 8;
            r12.A07 = A01;
            byte[] decode2 = Base64.decode(this.A01, 2);
            AbstractC27881Jp A012 = AbstractC27881Jp.A01(decode2, 0, decode2.length);
            A0T.A03();
            C40831sP r13 = (C40831sP) A0T.A00;
            r13.A00 |= 256;
            r13.A06 = A012;
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder("fmessagemediaexpresspathnotify/createdocumentmessagebuilder ex = ");
            sb.append(e.toString());
            Log.e(sb.toString());
        }
        A0T.A03();
        C40831sP r14 = (C40831sP) A0T.A00;
        r14.A00 |= 2;
        r14.A0G = "document";
        r4.A03();
        C27081Fy r15 = (C27081Fy) r4.A00;
        r15.A0C = (C40831sP) A0T.A02();
        r15.A00 |= 64;
    }
}
