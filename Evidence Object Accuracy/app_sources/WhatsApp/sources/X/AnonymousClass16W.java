package X;

import android.database.Cursor;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

/* renamed from: X.16W  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass16W {
    public final C21910yB A00;

    public AnonymousClass16W(C21910yB r1) {
        this.A00 = r1;
    }

    public int A00() {
        C16310on A01 = this.A00.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT MAX ( epoch ) FROM crypto_info", null);
            if (A09 != null) {
                if (A09.moveToFirst()) {
                    int i = (int) A09.getLong(0);
                    A09.close();
                    A01.close();
                    return i;
                }
                A09.close();
            }
            A01.close();
            return 0;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public AnonymousClass1JZ A01() {
        C16310on A01 = this.A00.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT device_id, epoch, key_data, timestamp, fingerprint FROM crypto_info ORDER BY epoch DESC, device_id ASC LIMIT 1", null);
            if (A09 != null) {
                if (A09.moveToFirst()) {
                    AnonymousClass1JZ A02 = A02(A09);
                    A09.close();
                    A01.close();
                    return A02;
                }
                A09.close();
            }
            A01.close();
            return null;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final AnonymousClass1JZ A02(Cursor cursor) {
        try {
            C34321fx A00 = C34321fx.A00((C34311fw) AbstractC27091Fz.A0E(C34311fw.A05, cursor.getBlob(cursor.getColumnIndexOrThrow("fingerprint"))));
            AnonymousClass009.A05(A00);
            return new AnonymousClass1JZ(new C27821Ji(A00, cursor.getBlob(cursor.getColumnIndexOrThrow("key_data")), cursor.getLong(cursor.getColumnIndexOrThrow("timestamp"))), new AnonymousClass1JR((int) cursor.getLong(cursor.getColumnIndexOrThrow("device_id")), (int) cursor.getLong(cursor.getColumnIndexOrThrow("epoch"))));
        } catch (C28971Pt e) {
            throw new IllegalStateException("SyncdCryptoStore/createSyncdKey", e);
        }
    }

    public void A03(Collection collection, long j) {
        HashSet hashSet = new HashSet(collection);
        hashSet.remove(null);
        if (!hashSet.isEmpty()) {
            C16310on A02 = this.A00.A02();
            try {
                Iterator it = hashSet.iterator();
                while (it.hasNext()) {
                    AnonymousClass1JR r5 = (AnonymousClass1JR) it.next();
                    A02.A03.A0C("UPDATE crypto_info SET stale_timestamp = ?  WHERE device_id = ?  AND epoch = ? ", new String[]{String.valueOf(j), String.valueOf(r5.A00()), String.valueOf(r5.A01())});
                }
                A02.close();
            } catch (Throwable th) {
                try {
                    A02.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
    }
}
