package X;

import java.io.Closeable;
import java.util.Map;

/* renamed from: X.0bz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08870bz implements Closeable, Cloneable {
    public static final AbstractC12230ha A04 = new C08660ba();
    public static final AbstractC12240hb A05 = new C08670bb();
    public boolean A00 = false;
    public final AbstractC12230ha A01;
    public final AnonymousClass0SW A02;
    public final Throwable A03;

    public C08870bz(AbstractC12230ha r2, AnonymousClass0SW r3, Throwable th) {
        this.A02 = r3;
        synchronized (r3) {
            r3.A01();
            r3.A00++;
        }
        this.A01 = r2;
        this.A03 = th;
    }

    public C08870bz(AbstractC12230ha r3, AbstractC12240hb r4, Object obj) {
        this.A02 = new AnonymousClass0SW(r4, obj);
        this.A01 = r3;
        this.A03 = null;
    }

    public static C08870bz A00(AbstractC12240hb r2, Object obj) {
        AbstractC12230ha r1 = A04;
        if (obj != null) {
            return new C08870bz(r1, r2, obj);
        }
        return null;
    }

    public static boolean A01(C08870bz r2) {
        boolean z;
        if (r2 != null) {
            synchronized (r2) {
                z = !r2.A00;
            }
            if (z) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: A02 */
    public C08870bz clone() {
        boolean z;
        synchronized (this) {
            z = !this.A00;
        }
        AnonymousClass0RA.A01(z);
        return new C08870bz(this.A01, this.A02, this.A03);
    }

    public synchronized C08870bz A03() {
        return this.A00 ^ true ? clone() : null;
    }

    public synchronized Object A04() {
        boolean z = false;
        if (!this.A00) {
            z = true;
        }
        AnonymousClass0RA.A01(z);
        return this.A02.A00();
    }

    public void A05() {
        try {
            synchronized (this) {
                if (!this.A00) {
                    this.A01.AaW(this.A02, this.A03);
                    close();
                }
            }
        } finally {
            super.finalize();
        }
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        int i;
        Object obj;
        synchronized (this) {
            if (!this.A00) {
                this.A00 = true;
                AnonymousClass0SW r3 = this.A02;
                synchronized (r3) {
                    r3.A01();
                    boolean z = false;
                    if (r3.A00 > 0) {
                        z = true;
                    }
                    AnonymousClass0RA.A00(z);
                    i = r3.A00 - 1;
                    r3.A00 = i;
                }
                if (i == 0) {
                    synchronized (r3) {
                        obj = r3.A01;
                        r3.A01 = null;
                    }
                    r3.A02.Aa4(obj);
                    Map map = AnonymousClass0SW.A03;
                    synchronized (map) {
                        Integer num = (Integer) map.get(obj);
                        if (num == null) {
                            AnonymousClass0UN.A04("SharedReference", "No entry in sLiveObjects for value of type %s", obj.getClass());
                        } else {
                            int intValue = num.intValue();
                            if (intValue == 1) {
                                map.remove(obj);
                            } else {
                                map.put(obj, Integer.valueOf(intValue - 1));
                            }
                        }
                    }
                }
            }
        }
    }

    @Override // java.lang.Object
    public void finalize() {
        try {
            synchronized (this) {
                if (!this.A00) {
                    AnonymousClass0SW r2 = this.A02;
                    AnonymousClass0UN.A03("DefaultCloseableReference", "Finalized without closing: %x %x (type = %s)", Integer.valueOf(System.identityHashCode(this)), Integer.valueOf(System.identityHashCode(r2)), r2.A00().getClass().getName());
                    this.A01.AaW(r2, this.A03);
                    close();
                }
            }
        } finally {
            A05();
        }
    }
}
