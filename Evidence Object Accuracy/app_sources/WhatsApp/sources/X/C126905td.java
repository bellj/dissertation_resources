package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.5td  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C126905td {
    public final String A00;
    public final String A01;

    public C126905td(C20920wX r4, UserJid userJid) {
        String str = userJid.user;
        try {
            StringBuilder A0h = C12960it.A0h();
            A0h.append("+");
            C71133cR A0E = r4.A0E(C12960it.A0d(str, A0h), "ZZ");
            this.A00 = Integer.toString(A0E.countryCode_);
            this.A01 = Long.toString(A0E.nationalNumber_);
        } catch (AnonymousClass4C8 unused) {
            throw C12990iw.A0m("Can't get phone number");
        }
    }
}
