package X;

import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.3Eb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64053Eb {
    public C74773il A00;
    public final AnonymousClass4IB A01;
    public final C54502gp A02;
    public final C63423Bn A03;
    public volatile AnonymousClass0FH A04;
    public volatile AnonymousClass0FB A05;
    public volatile C54652h4 A06;

    public C64053Eb(AnonymousClass4IB r1, C54502gp r2, C63423Bn r3) {
        this.A03 = r3;
        this.A01 = r1;
        this.A02 = r2;
    }

    public void A00(int i, int i2, boolean z) {
        if (AnonymousClass3J3.A03()) {
            C63423Bn r1 = this.A03;
            RecyclerView recyclerView = r1.A06;
            if (recyclerView == null) {
                r1.A01 = i;
                r1.A02 = i2;
                r1.A09 = z;
            } else if (z) {
                recyclerView.A0d(i, i2);
            } else {
                recyclerView.scrollBy(i, i2);
            }
        } else {
            throw C12990iw.A0m("Cannot doScrollBy off the main thread!");
        }
    }

    public void A01(int i, boolean z) {
        if (AnonymousClass3J3.A03()) {
            C63423Bn r1 = this.A03;
            RecyclerView recyclerView = r1.A06;
            if (recyclerView == null) {
                r1.A00 = i;
                r1.A08 = z;
            } else if (z) {
                recyclerView.A0Z(i);
            } else {
                recyclerView.A0Y(i);
            }
        } else {
            throw C12990iw.A0m("Cannot doScrollTo off the main thread!");
        }
    }
}
