package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.whatsapp.registration.VerifyPhoneNumber;
import java.lang.ref.WeakReference;

/* renamed from: X.2aE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class HandlerC52092aE extends Handler {
    public final C862946p A00;
    public final AnonymousClass105 A01;
    public final String A02;
    public final String A03;
    public final WeakReference A04;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public HandlerC52092aE(Looper looper, VerifyPhoneNumber verifyPhoneNumber, C862946p r4, AnonymousClass105 r5, String str, String str2) {
        super(looper);
        AnonymousClass009.A05(looper);
        this.A02 = str;
        this.A03 = str2;
        this.A01 = r5;
        this.A00 = r4;
        this.A04 = C12970iu.A10(verifyPhoneNumber);
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        VerifyPhoneNumber verifyPhoneNumber;
        if (message.what == 1 && (verifyPhoneNumber = (VerifyPhoneNumber) this.A04.get()) != null) {
            String str = this.A02;
            String str2 = this.A03;
            AnonymousClass009.A05(str2);
            verifyPhoneNumber.A3L(this.A00, AnonymousClass4AY.RETRIED, (String) message.obj, str, str2, "sms", null);
        }
    }
}
