package X;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import androidx.core.app.NotificationCompat$BigTextStyle;
import com.whatsapp.R;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.VoiceFGService;
import com.whatsapp.voipcalling.Voip;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.12r  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C237012r {
    public final C16210od A00;
    public final C15450nH A01;
    public final AnonymousClass130 A02;
    public final C15550nR A03;
    public final C15610nY A04;
    public final AnonymousClass10T A05;
    public final C21270x9 A06;
    public final AnonymousClass131 A07;
    public final AnonymousClass01d A08;
    public final C16590pI A09;
    public final C18360sK A0A;
    public final C15600nX A0B;
    public final C21250x7 A0C;
    public final C20710wC A0D;
    public final C15860o1 A0E;
    public final AbstractC14440lR A0F;

    public C237012r(C16210od r2, C15450nH r3, AnonymousClass130 r4, C15550nR r5, C15610nY r6, AnonymousClass10T r7, C21270x9 r8, AnonymousClass131 r9, AnonymousClass01d r10, C16590pI r11, C18360sK r12, C15600nX r13, C21250x7 r14, C20710wC r15, C15860o1 r16, AbstractC14440lR r17) {
        this.A09 = r11;
        this.A0F = r17;
        this.A01 = r3;
        this.A0C = r14;
        this.A06 = r8;
        this.A02 = r4;
        this.A03 = r5;
        this.A08 = r10;
        this.A04 = r6;
        this.A0D = r15;
        this.A0E = r16;
        this.A05 = r7;
        this.A0A = r12;
        this.A0B = r13;
        this.A00 = r2;
        this.A07 = r9;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r3v54, resolved type: android.text.SpannableString */
    /* JADX DEBUG: Multi-variable search result rejected for r10v17, resolved type: android.text.SpannableString */
    /* JADX WARN: Multi-variable type inference failed */
    public Notification A00(Context context, C20970wc r45, C50082Nz r46, int i, boolean z) {
        String str;
        int i2;
        Intent A01;
        Intent A012;
        int i3;
        String str2;
        Intent A013;
        int i4;
        String str3;
        int i5;
        StringBuilder sb = new StringBuilder("voip/CallNotificationBuilder type = ");
        if (i == 1) {
            str = "NOTIFICATION_HEADS_UP";
        } else if (i != 2) {
            StringBuilder sb2 = new StringBuilder("UNKNOWN notification type ");
            sb2.append(i);
            AnonymousClass009.A07(sb2.toString());
            str = "NOTIFICATION_INVALID";
        } else {
            str = "NOTIFICATION_MUTE";
        }
        sb.append(str);
        Log.i(sb.toString());
        long j = r46.A00;
        boolean z2 = r46.A06;
        GroupJid groupJid = r46.A01;
        C15550nR r13 = this.A03;
        C15610nY r2 = this.A04;
        C21250x7 r15 = this.A0C;
        C20710wC r12 = this.A0D;
        String A09 = AnonymousClass1SF.A09(r13, r2, r15, r12, groupJid);
        Context context2 = this.A09.A00;
        boolean z3 = false;
        if (A09 != null) {
            z3 = true;
        }
        String A02 = A02(context2, r46, z3);
        String A022 = A02(context2, r46, false);
        int i6 = (j > 0 ? 1 : (j == 0 ? 0 : -1));
        boolean z4 = r46.A0C;
        if (i6 > 0) {
            i2 = R.drawable.notify_ongoing_call;
            if (z4) {
                i2 = R.drawable.notify_ongoing_video;
            }
        } else if (z2) {
            i2 = R.drawable.notify_outgoing_call;
            if (z4) {
                i2 = R.drawable.notify_outgoing_video;
            }
        } else {
            i2 = R.drawable.notify_incoming_call;
            if (z4) {
                i2 = R.drawable.notify_incoming_video;
            }
        }
        Voip.CallState callState = r46.A03;
        Voip.CallState callState2 = Voip.CallState.ACTIVE_ELSEWHERE;
        int i7 = 3;
        if (callState != callState2 || !r46.A08) {
            A01 = A01(context, r46, i, z);
            A01.putExtra("lobbyEntryPoint", 1);
            A012 = A01(context, r46, i, z);
            A012.putExtra("lobbyEntryPoint", 6);
            i7 = 1;
            i3 = 4;
        } else {
            A01 = new Intent().setClassName(context.getPackageName(), "com.whatsapp.HomeActivity").setAction("com.whatsapp.intent.action.CALLS");
            A012 = A01;
            i3 = 3;
        }
        PendingIntent A00 = AnonymousClass1UY.A00(context, i7, A01, 134217728);
        PendingIntent A002 = AnonymousClass1UY.A00(context, i3, A012, 134217728);
        Resources resources = context.getResources();
        int min = Math.min(resources.getDimensionPixelSize(17104901), resources.getDimensionPixelSize(17104902));
        Bitmap bitmap = null;
        if (min > 0) {
            if (Build.VERSION.SDK_INT < 21 || (r46.A07 && groupJid == null)) {
                i5 = 0;
            } else {
                i5 = -1;
            }
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = new ArrayList();
            C15370n3 A023 = AnonymousClass1SF.A02(r13, r15, r12, groupJid);
            if (A023 == null) {
                for (AbstractC14640lm r14 : r46.A05) {
                    if (arrayList3.size() >= 3) {
                        break;
                    }
                    arrayList3.add(r13.A0B(r14));
                }
            } else {
                arrayList3.add(A023);
            }
            Iterator it = arrayList3.iterator();
            while (it.hasNext()) {
                C15370n3 r142 = (C15370n3) it.next();
                float f = (float) i5;
                Object A003 = this.A05.A02.A01().A00(r142.A0E(f, min));
                if (A003 == null) {
                    A003 = this.A02.A04(r142, f, min);
                    if (r142.A0X) {
                        arrayList.add(r142);
                    }
                }
                arrayList2.add(A003);
            }
            if (!arrayList2.isEmpty()) {
                if (arrayList2.size() == 1) {
                    bitmap = (Bitmap) arrayList2.get(0);
                } else {
                    bitmap = C21270x9.A01(arrayList2, resources.getDimension(R.dimen.small_avatar_radius));
                }
            }
            if (!arrayList.isEmpty()) {
                this.A0F.Aaz(new AnonymousClass2O1(context, r45, this.A07, arrayList, min, i5, i), new Void[0]);
            }
        } else {
            Log.w("voip/CallNotificationBuilder/thumb-size-is-0");
        }
        C15370n3 A0B = r13.A0B(r46.A02);
        long j2 = j;
        C005602s A004 = C22630zO.A00(context);
        A004.A0I = "call";
        boolean z5 = true;
        A004.A03 = 1;
        if (j <= 0) {
            z5 = false;
        }
        A004.A0W = z5;
        if (j <= 0) {
            j2 = System.currentTimeMillis();
        }
        A004.A05(j2);
        A004.A09(A02);
        NotificationCompat$BigTextStyle notificationCompat$BigTextStyle = new NotificationCompat$BigTextStyle();
        notificationCompat$BigTextStyle.A09(A02);
        A004.A08(notificationCompat$BigTextStyle);
        A004.A09 = A00;
        A004.A06(bitmap);
        C18360sK.A01(A004, i2);
        if (i == 1) {
            A004.A0A = A002;
            Notification notification = A004.A07;
            notification.flags = 128 | notification.flags;
        }
        A03(context, A004, r46, A09, false);
        if (z2 || j > 0) {
            Intent intent = new Intent(context, VoiceFGService.class);
            intent.setAction("hangup_call");
            intent.putExtra("end_call_reason", 1);
            A004.A04(R.drawable.ic_action_end_call, context.getString(R.string.hang_up), AnonymousClass1UY.A03(context, intent, 134217728));
        } else {
            if (!r46.A08 || !(callState == Voip.CallState.NONE || callState == callState2)) {
                Intent intent2 = new Intent(context, VoiceFGService.class);
                intent2.setAction("reject_call");
                String str4 = r46.A04;
                intent2.putExtra("call_id", str4);
                boolean z6 = r46.A0B;
                int i8 = 4;
                if (z6) {
                    i8 = 11;
                }
                intent2.putExtra("call_ui_action", i8);
                PendingIntent A03 = AnonymousClass1UY.A03(context, intent2, 134217728);
                boolean z7 = r46.A07;
                int i9 = R.string.reject_the_call;
                int i10 = R.color.call_notification_action_end_call;
                if (z7) {
                    i9 = R.string.voip_joinable_ignore;
                    i10 = R.color.call_notification_action_end_joinable_call;
                }
                String string = context.getString(i9);
                if (Build.VERSION.SDK_INT < 25 || i != 1) {
                    str2 = string;
                } else {
                    SpannableString spannableString = new SpannableString(string);
                    spannableString.setSpan(new ForegroundColorSpan(context.getColor(i10)), 0, spannableString.length(), 0);
                    str2 = spannableString;
                }
                AnonymousClass03l r4 = new AnonymousClass03l(R.drawable.ic_action_end_call, str2, A03);
                ArrayList arrayList4 = A004.A0N;
                arrayList4.add(r4);
                if (!z7) {
                    int i11 = 3;
                    if (z6) {
                        i11 = 10;
                    }
                    A013 = new C14960mK().A0n(context, str4, i11, !this.A00.A00);
                    A013.putExtra("fgservice_start_failed", z);
                } else {
                    A013 = A01(context, r46, i, z);
                    A013.putExtra("lobbyEntryPoint", 2);
                    A013.setAction(z6 ? "com.whatsapp.intent.action.SHOW_INCOMING_PENDING_CALL_ON_LOCK_SCREEN" : "join_call");
                }
                PendingIntent A005 = AnonymousClass1UY.A00(context, 2, A013, 134217728);
                int i12 = R.drawable.ic_action_call;
                if (z4) {
                    i12 = R.drawable.ic_action_videocall;
                }
                if (z7) {
                    i4 = R.string.voip_joinable_open;
                } else {
                    i4 = R.string.answer_the_call;
                    if (z6) {
                        i4 = R.string.voip_call_waiting_end_and_accept;
                    }
                }
                String string2 = context.getString(i4);
                if (Build.VERSION.SDK_INT < 25 || i != 1) {
                    str3 = string2;
                } else {
                    SpannableString spannableString2 = new SpannableString(string2);
                    spannableString2.setSpan(new ForegroundColorSpan(context.getColor(R.color.call_notification_action_accept)), 0, spannableString2.length(), 0);
                    str3 = spannableString2;
                }
                arrayList4.add(new AnonymousClass03l(i12, str3, A005));
            }
            A004.A0K = "call_notification_group";
            A004.A0S = true;
        }
        if (Build.VERSION.SDK_INT >= 21) {
            long j3 = j;
            C005602s A006 = C22630zO.A00(context);
            A006.A0I = "call";
            boolean z8 = true;
            A006.A03 = 1;
            if (j <= 0) {
                z8 = false;
            }
            A006.A0W = z8;
            if (j <= 0) {
                j3 = System.currentTimeMillis();
            }
            A006.A05(j3);
            A006.A09(A022);
            A03(context, A006, null, null, true);
            C18360sK.A01(A006, i2);
            try {
                A004.A08 = A006.A01();
            } catch (SecurityException e) {
                if (!C38241nl.A0A()) {
                    throw e;
                }
            }
            A04(A004, A0B, r46, i);
        }
        try {
            Notification A014 = A004.A01();
            if (j > 0 && A014.bigContentView != null) {
                try {
                    A014.bigContentView.setViewVisibility(Class.forName("com.android.internal.R$id").getDeclaredField("time").getInt(null), 8);
                } catch (Exception e2) {
                    Log.e("voip/service/notification/time-ui-gone", e2);
                }
            }
            StringBuilder sb3 = new StringBuilder("voip/CallNotificationBuilder ");
            sb3.append(A014);
            Log.i(sb3.toString());
            return A014;
        } catch (SecurityException e3) {
            if (C38241nl.A0A()) {
                C005602s A007 = C22630zO.A00(context);
                A007.A09(A02);
                A007.A09 = A00;
                C18360sK.A01(A007, i2);
                A03(context, A007, r46, A09, false);
                if (Build.VERSION.SDK_INT >= 21) {
                    A04(A007, A0B, r46, i);
                }
                return A007.A01();
            }
            throw e3;
        }
    }

    public Intent A01(Context context, C50082Nz r6, int i, boolean z) {
        Intent A0l = new C14960mK().A0l(context, r6.A02, Boolean.valueOf(!this.A00.A00));
        String str = r6.A04;
        A0l.setData(Uri.parse(str));
        A0l.putExtra("notification_type", i);
        A0l.putExtra("call_id", str);
        if (r6.A0B) {
            A0l.setAction("com.whatsapp.intent.action.SHOW_INCOMING_PENDING_CALL_ON_LOCK_SCREEN");
        }
        if (r6.A08 && r6.A03 == Voip.CallState.NONE) {
            A0l.putExtra("joinable", true);
        }
        A0l.putExtra("fgservice_start_failed", z);
        return A0l;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x003b, code lost:
        if (r0 != false) goto L_0x003d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String A02(android.content.Context r7, X.C50082Nz r8, boolean r9) {
        /*
            r6 = this;
            long r1 = r8.A00
            boolean r5 = r8.A06
            boolean r0 = r8.A09
            if (r0 == 0) goto L_0x0010
            r1 = 2131893020(0x7f121b1c, float:1.9420805E38)
        L_0x000b:
            java.lang.String r0 = r7.getString(r1)
            return r0
        L_0x0010:
            r3 = 0
            int r0 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r0 <= 0) goto L_0x002e
            boolean r1 = r8.A07
            boolean r0 = r8.A0C
            if (r1 != 0) goto L_0x0025
            r1 = 2131890042(0x7f120f7a, float:1.9414765E38)
            if (r0 == 0) goto L_0x000b
            r1 = 2131892743(0x7f121a07, float:1.9420243E38)
            goto L_0x000b
        L_0x0025:
            r1 = 2131890041(0x7f120f79, float:1.9414763E38)
            if (r0 == 0) goto L_0x000b
            r1 = 2131890040(0x7f120f78, float:1.941476E38)
            goto L_0x000b
        L_0x002e:
            if (r5 == 0) goto L_0x0041
            com.whatsapp.voipcalling.Voip$CallState r1 = r8.A03
            com.whatsapp.voipcalling.Voip$CallState r0 = com.whatsapp.voipcalling.Voip.CallState.PRE_ACCEPT_RECEIVED
            if (r1 != r0) goto L_0x003d
            boolean r0 = r8.A0A
            r1 = 2131891396(0x7f1214c4, float:1.941751E38)
            if (r0 == 0) goto L_0x000b
        L_0x003d:
            r1 = 2131886906(0x7f12033a, float:1.9408404E38)
            goto L_0x000b
        L_0x0041:
            boolean r0 = r8.A07
            if (r0 == 0) goto L_0x0098
            java.util.List r0 = r8.A05
            int r0 = r0.size()
            if (r0 <= 0) goto L_0x0098
            if (r9 == 0) goto L_0x0073
            X.0nR r1 = r6.A03
            com.whatsapp.jid.UserJid r0 = r8.A02
            X.0n3 r5 = r1.A0B(r0)
            boolean r0 = r8.A0C
            r4 = 2131888259(0x7f120883, float:1.9411148E38)
            if (r0 == 0) goto L_0x0061
            r4 = 2131888257(0x7f120881, float:1.9411144E38)
        L_0x0061:
            r0 = 1
            java.lang.Object[] r3 = new java.lang.Object[r0]
            r2 = 0
            X.0nY r1 = r6.A04
            r0 = -1
            java.lang.String r0 = r1.A0A(r5, r0)
            r3[r2] = r0
            java.lang.String r0 = r7.getString(r4, r3)
            return r0
        L_0x0073:
            boolean r0 = r8.A08
            if (r0 == 0) goto L_0x0081
            com.whatsapp.voipcalling.Voip$CallState r1 = r8.A03
            com.whatsapp.voipcalling.Voip$CallState r0 = com.whatsapp.voipcalling.Voip.CallState.NONE
            if (r1 == r0) goto L_0x008c
            com.whatsapp.voipcalling.Voip$CallState r0 = com.whatsapp.voipcalling.Voip.CallState.ACTIVE_ELSEWHERE
            if (r1 == r0) goto L_0x008c
        L_0x0081:
            boolean r0 = r8.A0C
            r1 = 2131888917(0x7f120b15, float:1.9412483E38)
            if (r0 == 0) goto L_0x000b
            r1 = 2131888915(0x7f120b13, float:1.9412479E38)
            goto L_0x000b
        L_0x008c:
            boolean r0 = r8.A0C
            r1 = 2131892995(0x7f121b03, float:1.9420754E38)
            if (r0 == 0) goto L_0x000b
            r1 = 2131892992(0x7f121b00, float:1.9420748E38)
            goto L_0x000b
        L_0x0098:
            boolean r0 = r8.A0C
            r1 = 2131888919(0x7f120b17, float:1.9412487E38)
            if (r0 == 0) goto L_0x000b
            r1 = 2131892735(0x7f1219ff, float:1.9420227E38)
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C237012r.A02(android.content.Context, X.2Nz, boolean):java.lang.String");
    }

    public final void A03(Context context, C005602s r13, C50082Nz r14, String str, boolean z) {
        AnonymousClass2OC r2;
        Object[] objArr;
        int i;
        if (z) {
            str = context.getString(R.string.app_name);
        } else if (str == null) {
            if (r14 == null) {
                return;
            }
            if (!r14.A07) {
                str = AbstractC32741cf.A02(this.A04.A04(this.A03.A0B(r14.A02)));
            } else if (r14.A00 <= 0) {
                C15550nR r9 = this.A03;
                C15610nY r8 = this.A04;
                List list = r14.A05;
                ArrayList arrayList = new ArrayList();
                int i2 = 0;
                while (i2 < list.size()) {
                    arrayList.add(r8.A0A(r9.A0B((AbstractC14640lm) list.get(i2)), -1));
                    i2++;
                    if (i2 >= 2) {
                        break;
                    }
                }
                if (list.size() > 2) {
                    r2 = new AnonymousClass2OB(new Object[]{arrayList.get(0), Integer.valueOf(list.size() - 1)}, R.plurals.group_voip_call_participants_label, list.size() - 1);
                } else {
                    if (list.size() == 2) {
                        objArr = new Object[]{arrayList.get(0), arrayList.get(1)};
                        i = R.string.voip_call_log_two_participants;
                    } else if (list.size() == 3) {
                        objArr = new Object[]{arrayList.get(0), arrayList.get(1), arrayList.get(2)};
                        i = R.string.voip_call_log_three_participants;
                    } else if (list.size() == 1) {
                        r2 = new AnonymousClass2OE((String) arrayList.get(0));
                    } else {
                        AnonymousClass009.A0A("Number of names not supported", false);
                        str = null;
                    }
                    r2 = new AnonymousClass2OD(objArr, i);
                }
                str = r2.A00(context);
            } else {
                return;
            }
        }
        r13.A0A(str);
    }

    public final void A04(C005602s r7, C15370n3 r8, C50082Nz r9, int i) {
        C33271dj r3;
        String str;
        Voip.CallState callState;
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 26) {
            C15860o1 r1 = this.A0E;
            Jid A0B = r8.A0B(AbstractC14640lm.class);
            AnonymousClass009.A05(A0B);
            r3 = (C33271dj) r1.A08(A0B.getRawString());
        } else {
            r3 = null;
        }
        if (i == 1 || (r9.A08 && ((callState = r9.A03) == Voip.CallState.NONE || callState == Voip.CallState.ACTIVE_ELSEWHERE))) {
            r7.A03 = 1;
            if (i2 >= 26) {
                str = r3.A0E();
                r7.A0J = str;
            }
        } else if (i != 2) {
            StringBuilder sb = new StringBuilder("UNKNOWN NOTIFICATION TYPE ");
            sb.append(i);
            AnonymousClass009.A07(sb.toString());
        } else if (i2 >= 26) {
            str = r3.A0D();
            r7.A0J = str;
        }
        ContentResolver A0C = this.A08.A0C();
        if (A0C != null) {
            Uri A05 = this.A03.A05(A0C, r8);
            if (A05 != null) {
                r7.A0C(A05.toString());
                return;
            }
            return;
        }
        Log.w("voip/CallNotificationBuilder/addContactToNotification cr == null");
    }
}
