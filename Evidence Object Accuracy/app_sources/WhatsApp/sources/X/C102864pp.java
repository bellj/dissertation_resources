package X;

import android.content.Context;
import com.whatsapp.businessdirectory.view.activity.DirectorySetLocationMapActivity;

/* renamed from: X.4pp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C102864pp implements AbstractC009204q {
    public final /* synthetic */ DirectorySetLocationMapActivity A00;

    public C102864pp(DirectorySetLocationMapActivity directorySetLocationMapActivity) {
        this.A00 = directorySetLocationMapActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
