package X;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;

/* renamed from: X.3SY  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3SY implements AnonymousClass5WW {
    @Override // X.AnonymousClass5WW
    public void A6O(Context context, Object obj, Object obj2, Object obj3) {
        AnonymousClass02H layoutManager;
        C56002kA r12 = (C56002kA) obj2;
        if (obj3 != null) {
            C54502gp r7 = r12.A08;
            C90674Ou r13 = (C90674Ou) obj3;
            int i = r7.A00;
            int i2 = r13.A00;
            boolean z = true;
            boolean z2 = false;
            boolean A1V = C12980iv.A1V(i, i2);
            int i3 = r7.A01;
            int i4 = r13.A01;
            boolean A1V2 = C12980iv.A1V(i3, i4);
            RecyclerView recyclerView = r7.A02;
            if (!(recyclerView == null || (layoutManager = recyclerView.getLayoutManager()) == null)) {
                if ((!layoutManager.A14() || !A1V) && (!layoutManager.A15() || !A1V2)) {
                    z = false;
                }
                z2 = z;
            }
            r7.A01 = i4;
            r7.A00 = i2;
            List list = r7.A04;
            r7.A04 = r13.A02;
            if (z2) {
                r7.A02();
            } else {
                AnonymousClass0RD.A00(new C54032g4(r7, list)).A02(r7);
            }
        } else {
            throw C12960it.A0U("List data was not computed during layout");
        }
    }

    @Override // X.AnonymousClass5WW
    public boolean Adc(Object obj, Object obj2, Object obj3, Object obj4) {
        C90674Ou r5 = (C90674Ou) obj3;
        C90674Ou r6 = (C90674Ou) obj4;
        return (r5.A01 == r6.A01 && r5.A00 == r6.A00 && r5.A02.equals(r6.A02)) ? false : true;
    }

    @Override // X.AnonymousClass5WW
    public /* bridge */ /* synthetic */ void Af8(Context context, Object obj, Object obj2, Object obj3) {
    }
}
