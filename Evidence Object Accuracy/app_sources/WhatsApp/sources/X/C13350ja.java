package X;

import com.google.firebase.iid.FirebaseInstanceId;

/* renamed from: X.0ja  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class C13350ja implements AbstractC13110jA {
    public static final AbstractC13110jA A00 = new C13350ja();

    @Override // X.AbstractC13110jA
    public final Object A80(AbstractC13170jG r5) {
        return new FirebaseInstanceId((C13030j1) r5.A02(C13030j1.class), (C13270jQ) r5.A02(C13270jQ.class), (C13130jC) r5.A02(C13130jC.class));
    }
}
