package X;

import android.graphics.Rect;
import android.os.Build;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.whatsapp.voipcalling.DefaultCryptoCallback;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import org.chromium.net.UrlRequest;

/* renamed from: X.5cr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C119085cr extends AbstractC130685zo implements Cloneable {
    public Rect A00;
    public Rect A01;
    public C129845yO A02;
    public C129845yO A03;
    public C129845yO A04;
    public C129845yO A05;
    public C129845yO A06;
    public Boolean A07;
    public Boolean A08;
    public Boolean A09;
    public Boolean A0A;
    public Boolean A0B;
    public Boolean A0C;
    public Boolean A0D;
    public Boolean A0E;
    public Boolean A0F;
    public Boolean A0G;
    public Boolean A0H;
    public Boolean A0I;
    public Double A0J;
    public Double A0K;
    public Double A0L;
    public Float A0M;
    public Float A0N;
    public Float A0O = Float.valueOf(-1.0f);
    public Integer A0P;
    public Integer A0Q;
    public Integer A0R;
    public Integer A0S;
    public Integer A0T;
    public Integer A0U;
    public Integer A0V;
    public Integer A0W;
    public Integer A0X;
    public Integer A0Y;
    public Integer A0Z;
    public Integer A0a;
    public Integer A0b;
    public Integer A0c;
    public Integer A0d;
    public Integer A0e;
    public Long A0f;
    public Long A0g;
    public String A0h;
    public HashMap A0i;
    public List A0j;
    public List A0k;
    public float[] A0l;
    public int[] A0m;
    public final AbstractC130695zp A0n;
    public final int[] A0o;

    public C119085cr(AbstractC130695zp r7) {
        Integer A0i = C12980iv.A0i();
        this.A0U = A0i;
        this.A0e = A0i;
        this.A0T = A0i;
        Boolean bool = Boolean.FALSE;
        this.A0H = bool;
        this.A0E = bool;
        this.A0F = bool;
        this.A0G = bool;
        this.A0A = bool;
        this.A0I = bool;
        this.A0D = bool;
        this.A07 = bool;
        this.A0i = C12970iu.A11();
        this.A0C = bool;
        Boolean bool2 = Boolean.TRUE;
        this.A08 = bool2;
        this.A0o = new int[2];
        this.A0V = A0i;
        this.A0d = -1;
        this.A0P = A0i;
        this.A0R = A0i;
        this.A0X = A0i;
        this.A0Y = A0i;
        this.A0a = A0i;
        this.A0b = A0i;
        this.A0Z = A0i;
        this.A0W = A0i;
        Long A0f = C117305Zk.A0f();
        this.A0f = A0f;
        Float valueOf = Float.valueOf(0.0f);
        this.A0M = valueOf;
        Integer A0V = C12960it.A0V();
        this.A0Q = A0V;
        this.A0c = A0i;
        this.A0S = A0V;
        this.A0g = A0f;
        Double valueOf2 = Double.valueOf(0.0d);
        this.A0J = valueOf2;
        this.A0L = valueOf2;
        this.A0K = valueOf2;
        this.A0h = "";
        List list = Collections.EMPTY_LIST;
        this.A0j = list;
        this.A0k = list;
        this.A09 = bool2;
        this.A0N = valueOf;
        this.A0B = bool;
        this.A0n = r7;
    }

    public static void A00(C119085cr r1, C125475rJ r2, int i) {
        r1.A04(r2, Integer.valueOf(i));
    }

    public static void A01(C119085cr r1, C125475rJ r2, boolean z) {
        r1.A04(r2, Boolean.valueOf(z));
    }

    public void A04(C125475rJ r6, Object obj) {
        int i = r6.A00;
        Rect rect = null;
        int i2 = 0;
        switch (i) {
            case 1:
                this.A0I = (Boolean) obj;
                return;
            case 2:
                if (C117295Zj.A1S(AbstractC130695zp.A0G, this.A0n)) {
                    boolean A1Y = C12970iu.A1Y(obj);
                    if (A1Y) {
                        i2 = 17;
                    }
                    A00(this, AbstractC130685zo.A0o, i2);
                    if (A1Y) {
                        A04(AbstractC130685zo.A0n, Boolean.FALSE);
                        return;
                    }
                    return;
                }
                return;
            case 3:
                this.A0H = (Boolean) obj;
                return;
            case 4:
                this.A0E = (Boolean) obj;
                return;
            case 5:
                this.A0D = (Boolean) obj;
                return;
            case 6:
                this.A07 = (Boolean) obj;
                return;
            case 7:
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
            case C43951xu.A01:
            case 28:
            case 29:
            case 40:
            case 41:
            case 43:
            case 57:
            case 59:
            default:
                throw C12990iw.A0m(C12960it.A0W(i, "Cannot directly set: "));
            case 8:
                this.A0G = (Boolean) obj;
                return;
            case 9:
                this.A0V = (Integer) obj;
                return;
            case 10:
                int A05 = C12960it.A05(obj);
                if (A05 != -1) {
                    i2 = A05;
                }
                this.A0U = Integer.valueOf(i2);
                return;
            case 11:
                this.A0P = (Integer) obj;
                return;
            case 12:
                this.A0R = (Integer) obj;
                return;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                this.A0T = (Integer) obj;
                return;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                this.A0X = (Integer) obj;
                return;
            case 15:
                this.A0Y = (Integer) obj;
                return;
            case 17:
                this.A0a = (Integer) obj;
                return;
            case 18:
            case 52:
                return;
            case 19:
                this.A0b = (Integer) obj;
                return;
            case 21:
                this.A0Z = (Integer) obj;
                return;
            case 22:
                this.A0W = (Integer) obj;
                return;
            case 23:
                this.A0c = (Integer) obj;
                return;
            case 24:
                this.A0d = (Integer) obj;
                return;
            case 25:
                this.A0e = (Integer) obj;
                return;
            case 26:
                this.A0O = (Float) obj;
                return;
            case 27:
                this.A0g = (Long) obj;
                return;
            case C25991Bp.A0S:
                this.A0J = (Double) obj;
                return;
            case 31:
                this.A0L = (Double) obj;
                return;
            case 32:
                this.A0K = (Double) obj;
                return;
            case 33:
                C129845yO r7 = (C129845yO) obj;
                this.A04 = r7;
                if (r7 != null) {
                    rect = new Rect(0, 0, r7.A02, r7.A01);
                }
                this.A01 = rect;
                return;
            case 34:
                C129845yO r72 = (C129845yO) obj;
                this.A03 = r72;
                if (r72 != null) {
                    rect = new Rect(0, 0, r72.A02, r72.A01);
                }
                this.A00 = rect;
                return;
            case 35:
                this.A05 = (C129845yO) obj;
                return;
            case 36:
                this.A02 = (C129845yO) obj;
                return;
            case 37:
                this.A0j = C130365zI.A00((List) obj);
                return;
            case 38:
                this.A0k = C130365zI.A00((List) obj);
                return;
            case 39:
                int[] iArr = (int[]) obj;
                if (iArr != null && iArr.length == 2) {
                    int[] iArr2 = this.A0o;
                    iArr2[0] = iArr[0];
                    iArr2[1] = iArr[1];
                    return;
                }
                return;
            case 42:
                this.A0h = (String) obj;
                return;
            case 44:
                this.A0C = (Boolean) obj;
                return;
            case 45:
                this.A08 = (Boolean) obj;
                return;
            case DefaultCryptoCallback.E2E_EXTENDED_V2_KEY_LENGTH /* 46 */:
                this.A0f = (Long) obj;
                return;
            case 47:
                this.A0S = (Integer) obj;
                return;
            case 48:
                this.A0M = (Float) obj;
                return;
            case 49:
                if (Build.VERSION.SDK_INT >= 21) {
                    this.A0l = (float[]) obj;
                    return;
                }
                return;
            case SearchActionVerificationClientService.TIME_TO_SLEEP_IN_MS /* 50 */:
                this.A0Q = (Integer) obj;
                return;
            case 51:
                if (Build.VERSION.SDK_INT >= 21) {
                    this.A0m = (int[]) obj;
                    return;
                }
                return;
            case 53:
                this.A09 = (Boolean) obj;
                return;
            case 54:
                this.A0N = (Float) obj;
                return;
            case 55:
                this.A0i = (HashMap) obj;
                return;
            case 56:
                this.A0B = (Boolean) obj;
                return;
            case 58:
                this.A06 = (C129845yO) obj;
                return;
            case 60:
                this.A0F = (Boolean) obj;
                return;
        }
    }

    public boolean A05(C128385w1 r5) {
        boolean z;
        if (r5.A0w) {
            A00(this, AbstractC130685zo.A0A, r5.A0C);
            z = true;
        } else {
            z = false;
        }
        if (r5.A0s) {
            A00(this, AbstractC130685zo.A08, r5.A0B);
            z = true;
        }
        if (r5.A17) {
            A01(this, AbstractC130685zo.A0T, r5.A16);
            z = true;
        }
        if (r5.A1X) {
            A01(this, AbstractC130685zo.A0W, r5.A1W);
            z = true;
        }
        if (r5.A1P) {
            A01(this, AbstractC130685zo.A0V, r5.A1O);
            z = true;
        }
        if (r5.A1G) {
            A01(this, AbstractC130685zo.A0U, r5.A1F);
            z = true;
        }
        if (r5.A1R) {
            A01(this, AbstractC130685zo.A0n, r5.A1Q);
            z = true;
        }
        if (r5.A0l) {
            A01(this, AbstractC130685zo.A0O, r5.A0k);
            z = true;
        }
        if (r5.A1M) {
            A00(this, AbstractC130685zo.A0k, r5.A0K);
            z = true;
        }
        if (r5.A1L) {
            A04(AbstractC130685zo.A0j, r5.A1d);
            z = true;
        }
        if (r5.A0y) {
            A00(this, AbstractC130685zo.A0C, r5.A0D);
            z = true;
        }
        if (r5.A00) {
            A01(this, AbstractC130685zo.A0L, r5.A0h);
            z = true;
        }
        if (r5.A1D) {
            A04(AbstractC130685zo.A0a, Float.valueOf(r5.A05));
            z = true;
        }
        if (r5.A10) {
            A01(this, AbstractC130685zo.A0R, r5.A0z);
            z = true;
        }
        if (r5.A0e) {
            A01(this, AbstractC130685zo.A0J, r5.A0d);
            z = true;
        }
        if (r5.A0c) {
            A04(AbstractC130685zo.A02, r5.A0X);
            z = true;
        }
        if (r5.A1Y) {
            A00(this, AbstractC130685zo.A0t, r5.A0N);
            z = true;
        }
        if (r5.A0a) {
            A00(this, AbstractC130685zo.A00, r5.A07);
            z = true;
        }
        if (r5.A0p) {
            A00(this, AbstractC130685zo.A06, r5.A09);
            z = true;
        }
        if (r5.A1A) {
            A00(this, AbstractC130685zo.A0X, r5.A0F);
            z = true;
        }
        if (r5.A1B) {
            A00(this, AbstractC130685zo.A0Y, r5.A0G);
            z = true;
        }
        if (r5.A1C) {
            A04(AbstractC130685zo.A0Z, r5.A0R);
            z = true;
        }
        if (r5.A1I) {
            A00(this, AbstractC130685zo.A0e, r5.A0I);
            z = true;
        }
        if (r5.A1H) {
            A00(this, AbstractC130685zo.A0c, r5.A0H);
            z = true;
        }
        if (r5.A19) {
            A00(this, AbstractC130685zo.A0I, r5.A0E);
            z = true;
        }
        if (r5.A0v) {
            A04(AbstractC130685zo.A09, Long.valueOf(r5.A0P));
            z = true;
        }
        if (r5.A0q) {
            A00(this, AbstractC130685zo.A07, r5.A0A);
            z = true;
        }
        if (r5.A0b) {
            A04(AbstractC130685zo.A01, Float.valueOf(r5.A04));
            z = true;
        }
        if (r5.A0m) {
            A04(AbstractC130685zo.A03, r5.A1b);
            z = true;
        }
        if (r5.A0n) {
            A00(this, AbstractC130685zo.A04, r5.A08);
            z = true;
        }
        if (r5.A0o) {
            A04(AbstractC130685zo.A05, r5.A1c);
            z = true;
        }
        if (r5.A1S) {
            A00(this, AbstractC130685zo.A0o, r5.A0L);
            z = true;
        }
        if (r5.A1a) {
            A00(this, AbstractC130685zo.A0v, r5.A0O);
            z = true;
        }
        if (r5.A1T) {
            A04(AbstractC130685zo.A0p, Float.valueOf(r5.A06));
            z = true;
        }
        if (r5.A15) {
            A04(AbstractC130685zo.A0H, Long.valueOf(r5.A0Q));
            z = true;
        }
        if (r5.A11) {
            A04(AbstractC130685zo.A0D, Double.valueOf(r5.A01));
            z = true;
        }
        if (r5.A13) {
            A04(AbstractC130685zo.A0F, Double.valueOf(r5.A03));
            z = true;
        }
        if (r5.A12) {
            A04(AbstractC130685zo.A0E, Double.valueOf(r5.A02));
            z = true;
        }
        if (r5.A14) {
            A04(AbstractC130685zo.A0G, r5.A0W);
            z = true;
        }
        if (r5.A0x) {
            A04(AbstractC130685zo.A0B, r5.A0Y);
            z = true;
        }
        if (r5.A1E) {
            A04(AbstractC130685zo.A0b, r5.A0Z);
            z = true;
        }
        if (r5.A1N) {
            A04(AbstractC130685zo.A0m, r5.A0T);
            z = true;
        }
        if (r5.A1J) {
            A04(AbstractC130685zo.A0g, r5.A0S);
            z = true;
        }
        if (r5.A1Z) {
            A04(AbstractC130685zo.A0u, r5.A0V);
            z = true;
        }
        if (r5.A1V) {
            A04(AbstractC130685zo.A0s, r5.A0U);
            z = true;
        }
        if (r5.A0u) {
            A01(this, AbstractC130685zo.A0P, r5.A0t);
            z = true;
        }
        if (r5.A0g) {
            A01(this, AbstractC130685zo.A0K, r5.A0f);
            z = true;
        }
        if (r5.A0r) {
            A04(AbstractC130685zo.A0h, null);
            z = true;
        }
        if (!r5.A0j) {
            return z;
        }
        A01(this, AbstractC130685zo.A0N, r5.A0i);
        return true;
    }

    @Override // java.lang.Object
    public Object clone() {
        return super.clone();
    }
}
