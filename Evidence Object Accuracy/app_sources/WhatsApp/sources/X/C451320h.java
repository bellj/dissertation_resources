package X;

import android.content.Context;

/* renamed from: X.20h  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C451320h extends AbstractC451020e {
    public final /* synthetic */ AnonymousClass1FK A00;
    public final /* synthetic */ C18610sj A01;
    public final /* synthetic */ C451220g A02;
    public final /* synthetic */ String A03;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C451320h(Context context, C14900mE r2, AnonymousClass1FK r3, C18650sn r4, C18610sj r5, C451220g r6, String str) {
        super(context, r2, r4);
        this.A01 = r5;
        this.A00 = r3;
        this.A03 = str;
        this.A02 = r6;
    }

    @Override // X.AbstractC451020e
    public void A02(C452120p r5) {
        C18610sj r3 = this.A01;
        C30931Zj r2 = r3.A0I;
        StringBuilder sb = new StringBuilder("get-methods: on-request-error=");
        sb.append(r5);
        r2.A05(sb.toString());
        AbstractC16870pt ACx = r3.A0G.A02().ACx();
        if (ACx != null) {
            ACx.AKa(r5, 12);
        }
        AnonymousClass1FK r0 = this.A00;
        if (r0 != null) {
            r0.AV3(r5);
        }
    }

    @Override // X.AbstractC451020e
    public void A03(C452120p r5) {
        C18610sj r3 = this.A01;
        C30931Zj r2 = r3.A0I;
        StringBuilder sb = new StringBuilder("get-methods: on-response-error=");
        sb.append(r5);
        r2.A05(sb.toString());
        AbstractC16870pt ACx = r3.A0G.A02().ACx();
        if (ACx != null) {
            ACx.AKa(r5, 12);
        }
        AnonymousClass1FK r0 = this.A00;
        if (r0 != null) {
            r0.AVA(r5);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:59:0x014c, code lost:
        if (r2 > 1) goto L_0x014e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0116  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0155  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0168  */
    @Override // X.AbstractC451020e
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A04(X.AnonymousClass1V8 r21) {
        /*
        // Method dump skipped, instructions count: 384
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C451320h.A04(X.1V8):void");
    }
}
