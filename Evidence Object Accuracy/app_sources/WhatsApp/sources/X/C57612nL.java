package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2nL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57612nL extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57612nL A0A;
    public static volatile AnonymousClass255 A0B;
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public long A04;
    public AbstractC27881Jp A05;
    public AbstractC27881Jp A06;
    public AbstractC27881Jp A07;
    public String A08 = "";
    public String A09 = "";

    static {
        C57612nL r0 = new C57612nL();
        A0A = r0;
        r0.A0W();
    }

    public C57612nL() {
        AbstractC27881Jp r0 = AbstractC27881Jp.A01;
        this.A06 = r0;
        this.A07 = r0;
        this.A05 = r0;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r15, Object obj, Object obj2) {
        switch (r15.ordinal()) {
            case 0:
                return A0A;
            case 1:
                AbstractC462925h r7 = (AbstractC462925h) obj;
                C57612nL r4 = (C57612nL) obj2;
                this.A06 = r7.Afm(this.A06, r4.A06, C12960it.A1R(this.A00), C12960it.A1R(r4.A00));
                int i = this.A00;
                boolean A1V = C12960it.A1V(i & 2, 2);
                long j = this.A04;
                int i2 = r4.A00;
                this.A04 = r7.Afs(j, r4.A04, A1V, C12960it.A1V(i2 & 2, 2));
                this.A07 = r7.Afm(this.A07, r4.A07, C12960it.A1V(i & 4, 4), C12960it.A1V(i2 & 4, 4));
                this.A05 = r7.Afm(this.A05, r4.A05, C12960it.A1V(this.A00 & 8, 8), C12960it.A1V(r4.A00 & 8, 8));
                int i3 = this.A00;
                boolean A1V2 = C12960it.A1V(i3 & 16, 16);
                String str = this.A08;
                int i4 = r4.A00;
                this.A08 = r7.Afy(str, r4.A08, A1V2, C12960it.A1V(i4 & 16, 16));
                this.A03 = r7.Afp(this.A03, r4.A03, C12960it.A1V(i3 & 32, 32), C12960it.A1V(i4 & 32, 32));
                this.A01 = r7.Afp(this.A01, r4.A01, C12960it.A1V(i3 & 64, 64), C12960it.A1V(i4 & 64, 64));
                this.A09 = r7.Afy(this.A09, r4.A09, C12960it.A1V(i3 & 128, 128), C12960it.A1V(i4 & 128, 128));
                this.A02 = r7.Afp(this.A02, r4.A02, C12960it.A1V(i3 & 256, 256), C12960it.A1V(i4 & 256, 256));
                if (r7 == C463025i.A00) {
                    this.A00 = i3 | i4;
                }
                return this;
            case 2:
                AnonymousClass253 r72 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        int A03 = r72.A03();
                        if (A03 == 0) {
                            break;
                        } else if (A03 == 10) {
                            this.A00 |= 1;
                            this.A06 = r72.A08();
                        } else if (A03 == 16) {
                            this.A00 |= 2;
                            this.A04 = r72.A06();
                        } else if (A03 == 26) {
                            this.A00 |= 4;
                            this.A07 = r72.A08();
                        } else if (A03 == 34) {
                            this.A00 |= 8;
                            this.A05 = r72.A08();
                        } else if (A03 == 42) {
                            String A0A2 = r72.A0A();
                            this.A00 |= 16;
                            this.A08 = A0A2;
                        } else if (A03 == 48) {
                            int A02 = r72.A02();
                            if (AnonymousClass4BZ.A00(A02) == null) {
                                super.A0X(6, A02);
                            } else {
                                this.A00 |= 32;
                                this.A03 = A02;
                            }
                        } else if (A03 == 56) {
                            this.A00 |= 64;
                            this.A01 = r72.A02();
                        } else if (A03 == 66) {
                            String A0A3 = r72.A0A();
                            this.A00 |= 128;
                            this.A09 = A0A3;
                        } else if (A03 == 72) {
                            this.A00 |= 256;
                            this.A02 = r72.A02();
                        } else if (!A0a(r72, A03)) {
                            break;
                        }
                    } catch (C28971Pt e) {
                        throw AbstractC27091Fz.A0J(e, this);
                    } catch (IOException e2) {
                        throw AbstractC27091Fz.A0K(this, e2);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C57612nL();
            case 5:
                return new C81953uq();
            case 6:
                break;
            case 7:
                if (A0B == null) {
                    synchronized (C57612nL.class) {
                        if (A0B == null) {
                            A0B = AbstractC27091Fz.A09(A0A);
                        }
                    }
                }
                return A0B;
            default:
                throw C12970iu.A0z();
        }
        return A0A;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i2 = AbstractC27091Fz.A05(this.A06, 1, 0);
        }
        if ((i3 & 2) == 2) {
            i2 += CodedOutputStream.A06(2, this.A04);
        }
        if ((i3 & 4) == 4) {
            i2 = AbstractC27091Fz.A05(this.A07, 3, i2);
        }
        if ((i3 & 8) == 8) {
            i2 = AbstractC27091Fz.A05(this.A05, 4, i2);
        }
        if ((i3 & 16) == 16) {
            i2 = AbstractC27091Fz.A04(5, this.A08, i2);
        }
        int i4 = this.A00;
        if ((i4 & 32) == 32) {
            i2 = AbstractC27091Fz.A03(6, this.A03, i2);
        }
        if ((i4 & 64) == 64) {
            i2 = AbstractC27091Fz.A02(7, this.A01, i2);
        }
        if ((i4 & 128) == 128) {
            i2 = AbstractC27091Fz.A04(8, this.A09, i2);
        }
        if ((this.A00 & 256) == 256) {
            i2 = AbstractC27091Fz.A02(9, this.A02, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0K(this.A06, 1);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0H(2, this.A04);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0K(this.A07, 3);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0K(this.A05, 4);
        }
        if ((this.A00 & 16) == 16) {
            codedOutputStream.A0I(5, this.A08);
        }
        if ((this.A00 & 32) == 32) {
            codedOutputStream.A0E(6, this.A03);
        }
        if ((this.A00 & 64) == 64) {
            codedOutputStream.A0F(7, this.A01);
        }
        if ((this.A00 & 128) == 128) {
            codedOutputStream.A0I(8, this.A09);
        }
        if ((this.A00 & 256) == 256) {
            codedOutputStream.A0F(9, this.A02);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
