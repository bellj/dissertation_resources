package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.2Lq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C49652Lq {
    public int A00;
    public UserJid A01;
    public C49662Lr A02;
    public Integer A03;
    public String A04;

    public C49652Lq(C49662Lr r2, String str) {
        this.A00 = 4;
        this.A03 = 0;
        this.A02 = r2;
        this.A04 = str;
    }

    public C49652Lq(UserJid userJid, Integer num, String str, int i) {
        this.A00 = i;
        this.A03 = num;
        this.A01 = userJid;
        this.A04 = str;
    }
}
