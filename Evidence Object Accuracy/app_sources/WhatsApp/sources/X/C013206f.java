package X;

import android.os.Build;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.Window;
import java.util.List;

/* renamed from: X.06f  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C013206f extends Window$CallbackC013306g {
    public final /* synthetic */ LayoutInflater$Factory2C011505o A00;

    @Override // X.Window$CallbackC013306g, android.view.Window.Callback
    public void onContentChanged() {
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C013206f(Window.Callback callback, LayoutInflater$Factory2C011505o r2) {
        super(callback);
        this.A00 = r2;
    }

    @Override // X.Window$CallbackC013306g, android.view.Window.Callback
    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return this.A00.A0W(keyEvent) || super.dispatchKeyEvent(keyEvent);
    }

    @Override // X.Window$CallbackC013306g, android.view.Window.Callback
    public boolean dispatchKeyShortcutEvent(KeyEvent keyEvent) {
        AnonymousClass07H r0;
        AnonymousClass07H r02;
        if (super.dispatchKeyShortcutEvent(keyEvent)) {
            return true;
        }
        LayoutInflater$Factory2C011505o r6 = this.A00;
        int keyCode = keyEvent.getKeyCode();
        r6.A0O();
        AbstractC005102i r03 = r6.A0B;
        if (r03 != null && r03.A0V(keyCode, keyEvent)) {
            return true;
        }
        AnonymousClass08Y r2 = r6.A0G;
        if (r2 != null) {
            int keyCode2 = keyEvent.getKeyCode();
            if (!keyEvent.isSystem() && ((r2.A0D || r6.A0X(keyEvent, r2)) && (r02 = r2.A0A) != null && r02.performShortcut(keyCode2, keyEvent, 1))) {
                AnonymousClass08Y r04 = r6.A0G;
                if (r04 == null) {
                    return true;
                }
                r04.A0B = true;
                return true;
            }
        }
        if (r6.A0G != null) {
            return false;
        }
        AnonymousClass08Y A0L = r6.A0L(0);
        r6.A0X(keyEvent, A0L);
        int keyCode3 = keyEvent.getKeyCode();
        boolean z = false;
        if (!keyEvent.isSystem() && ((A0L.A0D || r6.A0X(keyEvent, A0L)) && (r0 = A0L.A0A) != null)) {
            z = r0.performShortcut(keyCode3, keyEvent, 1);
        }
        A0L.A0D = false;
        if (z) {
            return true;
        }
        return false;
    }

    @Override // X.Window$CallbackC013306g, android.view.Window.Callback
    public boolean onCreatePanelMenu(int i, Menu menu) {
        if (i != 0 || (menu instanceof AnonymousClass07H)) {
            return super.onCreatePanelMenu(i, menu);
        }
        return false;
    }

    @Override // X.Window$CallbackC013306g, android.view.Window.Callback
    public boolean onMenuOpened(int i, Menu menu) {
        super.onMenuOpened(i, menu);
        LayoutInflater$Factory2C011505o r1 = this.A00;
        if (i != 108) {
            return true;
        }
        r1.A0O();
        AbstractC005102i r12 = r1.A0B;
        if (r12 == null) {
            return true;
        }
        r12.A0K(true);
        return true;
    }

    @Override // X.Window$CallbackC013306g, android.view.Window.Callback
    public void onPanelClosed(int i, Menu menu) {
        super.onPanelClosed(i, menu);
        LayoutInflater$Factory2C011505o r3 = this.A00;
        if (i == 108) {
            r3.A0O();
            AbstractC005102i r0 = r3.A0B;
            if (r0 != null) {
                r0.A0K(false);
            }
        } else if (i == 0) {
            AnonymousClass08Y A0L = r3.A0L(i);
            if (A0L.A0C) {
                r3.A0T(A0L, false);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x000f, code lost:
        if (r2 != null) goto L_0x0011;
     */
    @Override // X.Window$CallbackC013306g, android.view.Window.Callback
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onPreparePanel(int r4, android.view.View r5, android.view.Menu r6) {
        /*
            r3 = this;
            boolean r0 = r6 instanceof X.AnonymousClass07H
            if (r0 == 0) goto L_0x000d
            r2 = r6
            X.07H r2 = (X.AnonymousClass07H) r2
        L_0x0007:
            r1 = 0
            if (r4 != 0) goto L_0x000f
            if (r2 != 0) goto L_0x0011
            return r1
        L_0x000d:
            r2 = 0
            goto L_0x0007
        L_0x000f:
            if (r2 == 0) goto L_0x0014
        L_0x0011:
            r0 = 1
            r2.A0I = r0
        L_0x0014:
            boolean r0 = super.onPreparePanel(r4, r5, r6)
            if (r2 == 0) goto L_0x001c
            r2.A0I = r1
        L_0x001c:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C013206f.onPreparePanel(int, android.view.View, android.view.Menu):boolean");
    }

    @Override // X.Window$CallbackC013306g, android.view.Window.Callback
    public void onProvideKeyboardShortcuts(List list, Menu menu, int i) {
        AnonymousClass07H r0 = this.A00.A0L(0).A0A;
        if (r0 != null) {
            super.onProvideKeyboardShortcuts(list, r0, i);
        } else {
            super.onProvideKeyboardShortcuts(list, menu, i);
        }
    }

    @Override // X.Window$CallbackC013306g, android.view.Window.Callback
    public ActionMode onWindowStartingActionMode(ActionMode.Callback callback) {
        if (Build.VERSION.SDK_INT >= 23) {
            return null;
        }
        LayoutInflater$Factory2C011505o r2 = this.A00;
        if (!r2.A0Y) {
            return super.onWindowStartingActionMode(callback);
        }
        AnonymousClass0XD r1 = new AnonymousClass0XD(r2.A0k, callback);
        AbstractC009504t A05 = r2.A05(r1);
        if (A05 != null) {
            return r1.A00(A05);
        }
        return null;
    }

    @Override // X.Window$CallbackC013306g, android.view.Window.Callback
    public ActionMode onWindowStartingActionMode(ActionMode.Callback callback, int i) {
        LayoutInflater$Factory2C011505o r2 = this.A00;
        if (!r2.A0Y || i != 0) {
            return super.onWindowStartingActionMode(callback, i);
        }
        AnonymousClass0XD r1 = new AnonymousClass0XD(r2.A0k, callback);
        AbstractC009504t A05 = r2.A05(r1);
        if (A05 != null) {
            return r1.A00(A05);
        }
        return null;
    }
}
