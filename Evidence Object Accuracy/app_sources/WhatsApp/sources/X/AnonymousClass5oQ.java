package X;

/* renamed from: X.5oQ  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass5oQ extends AbstractC16350or {
    public final C15650ng A00;
    public final Runnable A01;

    public /* synthetic */ AnonymousClass5oQ(C15650ng r1, Runnable runnable) {
        this.A01 = runnable;
        this.A00 = r1;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        synchronized (this) {
            C15650ng r1 = this.A00;
            r1.A0M(null, null);
            r1.A0G();
        }
        return null;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        Runnable runnable = this.A01;
        if (runnable != null) {
            runnable.run();
        }
    }
}
