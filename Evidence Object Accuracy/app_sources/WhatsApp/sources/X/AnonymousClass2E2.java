package X;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.util.Log;

/* renamed from: X.2E2  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2E2 implements View.OnLongClickListener {
    public final C14900mE A00;
    public final AnonymousClass01d A01;
    public final String A02;

    public AnonymousClass2E2(C14900mE r1, AnonymousClass01d r2, String str) {
        this.A00 = r1;
        this.A02 = str;
        this.A01 = r2;
    }

    @Override // android.view.View.OnLongClickListener
    public boolean onLongClick(View view) {
        ClipboardManager A0B = this.A01.A0B();
        if (A0B == null) {
            return true;
        }
        try {
            String str = this.A02;
            A0B.setPrimaryClip(ClipData.newPlainText(str, str));
            this.A00.A07(R.string.phone_copied, 0);
            return true;
        } catch (NullPointerException | SecurityException e) {
            Log.e("contactinfo/copy", e);
            this.A00.A07(R.string.view_contact_unsupport, 0);
            return true;
        }
    }
}
