package X;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;

/* renamed from: X.0IZ  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0IZ extends C10930fR implements ExecutorService {
    public AnonymousClass0IZ(Executor executor) {
        super(new LinkedBlockingQueue(), executor);
    }

    @Override // X.C10930fR, java.util.concurrent.Executor
    public synchronized void execute(Runnable runnable) {
        super.execute(runnable);
    }
}
