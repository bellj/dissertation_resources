package X;

import android.view.ViewTreeObserver;

/* renamed from: X.4oM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnPreDrawListenerC101954oM implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ C15190mi A00;
    public final /* synthetic */ View$OnFocusChangeListenerC66043Mb A01;

    public ViewTreeObserver$OnPreDrawListenerC101954oM(C15190mi r1, View$OnFocusChangeListenerC66043Mb r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        C15190mi r1 = this.A00;
        C12980iv.A1G(r1, this);
        r1.setSelection(r1.length());
        return true;
    }
}
