package X;

import android.app.Activity;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.function.Consumer;

/* renamed from: X.0fe  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C11040fe extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ ClassLoader $classLoader;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C11040fe(ClassLoader classLoader) {
        super(0);
        this.$classLoader = classLoader;
    }

    /* renamed from: A00 */
    public final Boolean AJ3() {
        Class<?> loadClass = this.$classLoader.loadClass("androidx.window.extensions.layout.WindowLayoutComponent");
        boolean z = false;
        Method method = loadClass.getMethod("addWindowLayoutInfoListener", Activity.class, Consumer.class);
        Method method2 = loadClass.getMethod("removeWindowLayoutInfoListener", Consumer.class);
        C16700pc.A0B(method);
        if (Modifier.isPublic(method.getModifiers())) {
            C16700pc.A0B(method2);
            if (Modifier.isPublic(method2.getModifiers())) {
                z = true;
            }
        }
        return Boolean.valueOf(z);
    }
}
