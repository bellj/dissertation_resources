package X;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import com.facebook.redex.IDxLAdapterShape1S0100000_2_I1;
import com.whatsapp.R;
import com.whatsapp.mediaview.MediaViewBaseFragment;
import java.util.ArrayList;
import java.util.Collection;

/* renamed from: X.21p  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC454421p {
    public static final boolean A00;

    static {
        boolean z = false;
        if (Build.VERSION.SDK_INT >= 22) {
            z = true;
        }
        A00 = z;
    }

    public static Bundle A04(Activity activity, View view) {
        if (view == null) {
            return null;
        }
        ArrayList<String> arrayList = new ArrayList<>();
        A09(activity.getWindow().getDecorView(), arrayList);
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        Bundle bundle = new Bundle();
        bundle.putInt("x", iArr[0]);
        bundle.putInt("y", iArr[1]);
        bundle.putInt("width", view.getWidth());
        bundle.putInt("height", view.getHeight());
        bundle.putStringArrayList("visible_shared_elements", arrayList);
        return bundle;
    }

    public static Bundle A05(Activity activity, View view, String str) {
        if (!A00 || view == null) {
            return null;
        }
        return C018108l.A01(activity, view, str).A03();
    }

    public static View A06(View view, String str) {
        if (str.equals(AnonymousClass028.A0J(view))) {
            return view;
        }
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int childCount = viewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View A06 = A06(viewGroup.getChildAt(i), str);
                if (A06 != null) {
                    return A06;
                }
            }
        }
        return null;
    }

    public static void A07(Context context, Intent intent, View view) {
        Activity A01 = AbstractC35731ia.A01(context, ActivityC000800j.class);
        if (A01 != null) {
            intent.putExtra("animation_bundle", A04(A01, view));
        }
    }

    public static void A08(Context context, Intent intent, View view, AnonymousClass2TT r5, String str) {
        ActivityC000800j r1 = (ActivityC000800j) AbstractC35731ia.A01(context, ActivityC000800j.class);
        if (!A00 || r1 == null) {
            context.startActivity(intent);
            if (r1 != null) {
                r1.overridePendingTransition(0, 0);
                return;
            }
            return;
        }
        C454321o.A02(intent, view, r1, r5, str);
    }

    public static void A09(View view, Collection collection) {
        if (!TextUtils.isEmpty(AnonymousClass028.A0J(view))) {
            collection.add(AnonymousClass028.A0J(view));
        }
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int childCount = viewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                A09(viewGroup.getChildAt(i), collection);
            }
        }
    }

    public void A0A() {
        MediaViewBaseFragment mediaViewBaseFragment = ((AnonymousClass33Z) this).A06;
        if (mediaViewBaseFragment.A1A() != null) {
            mediaViewBaseFragment.A0C().overridePendingTransition(0, 0);
        }
    }

    public void A0B(Bundle bundle) {
        AnonymousClass33Z r6 = (AnonymousClass33Z) this;
        MediaViewBaseFragment mediaViewBaseFragment = r6.A06;
        if (mediaViewBaseFragment.A1A() == null) {
            mediaViewBaseFragment.A1E();
            return;
        }
        AnonymousClass443 r3 = mediaViewBaseFragment.A09;
        Object A1C = mediaViewBaseFragment.A1C(r3.getCurrentItem());
        if (mediaViewBaseFragment.A02().getConfiguration().orientation != r6.A03 || A1C == null || !A1C.equals(mediaViewBaseFragment.A1B())) {
            r3.setPivotX((float) (r3.getWidth() / 2));
            r3.setPivotY((float) (r3.getHeight() / 2));
            r6.A02 = 0;
            r6.A04 = 0;
        }
        r3.animate().setDuration(240).scaleX(r6.A01).scaleY(r6.A00).translationX((float) r6.A02).translationY((float) r6.A04).alpha(0.0f).setListener(new IDxLAdapterShape1S0100000_2_I1(r6, 12));
        ObjectAnimator ofInt = ObjectAnimator.ofInt(r6.A05, "alpha", 255, 0);
        ofInt.setDuration(240L);
        ofInt.setInterpolator(new DecelerateInterpolator());
        ofInt.start();
    }

    public void A0C(Bundle bundle, AbstractC35501i8 r12) {
        AnonymousClass33Z r5 = (AnonymousClass33Z) this;
        MediaViewBaseFragment mediaViewBaseFragment = r5.A06;
        AnonymousClass443 r3 = mediaViewBaseFragment.A09;
        int i = bundle.getInt("x", 0);
        int i2 = bundle.getInt("y", 0);
        int i3 = bundle.getInt("width", 0);
        int i4 = bundle.getInt("height", 0);
        AbstractC005102i A1U = ((ActivityC000800j) mediaViewBaseFragment.A0C()).A1U();
        AnonymousClass009.A05(A1U);
        A1U.A06();
        mediaViewBaseFragment.A0G = false;
        View findViewById = mediaViewBaseFragment.A05().findViewById(R.id.background);
        ColorDrawable colorDrawable = new ColorDrawable(-16777216);
        r5.A05 = colorDrawable;
        findViewById.setBackground(colorDrawable);
        r3.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver$OnPreDrawListenerC66513Nw(r3, r12, r5, i, i2, i3, i4));
    }
}
