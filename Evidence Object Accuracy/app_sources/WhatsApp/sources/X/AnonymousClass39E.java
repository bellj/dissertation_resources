package X;

import android.content.Context;
import android.media.MediaPlayer;
import com.whatsapp.util.Log;

/* renamed from: X.39E  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass39E extends AnonymousClass21T {
    public final AnonymousClass2CP A00;

    public AnonymousClass39E(Context context, String str, boolean z) {
        AnonymousClass2CP r1 = new AnonymousClass2CP(context, this);
        this.A00 = r1;
        r1.A0B = str;
        r1.A07 = new MediaPlayer.OnErrorListener() { // from class: X.3LX
            @Override // android.media.MediaPlayer.OnErrorListener
            public final boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
                AnonymousClass39E r3 = AnonymousClass39E.this;
                StringBuilder A0k = C12960it.A0k("VideoPlayerOnTextureView/error ");
                A0k.append(i);
                Log.e(C12960it.A0e(" ", A0k, i2));
                AnonymousClass5V3 r0 = r3.A02;
                if (r0 == null) {
                    return false;
                }
                r0.APt(null, true);
                return false;
            }
        };
        r1.A06 = new MediaPlayer.OnCompletionListener() { // from class: X.4i3
            @Override // android.media.MediaPlayer.OnCompletionListener
            public final void onCompletion(MediaPlayer mediaPlayer) {
                AnonymousClass39E r12 = AnonymousClass39E.this;
                AnonymousClass5V2 r0 = r12.A01;
                if (r0 != null) {
                    r0.AOT(r12);
                }
            }
        };
        r1.setLooping(z);
    }
}
