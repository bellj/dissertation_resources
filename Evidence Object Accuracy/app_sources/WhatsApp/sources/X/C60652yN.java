package X;

import android.content.Context;
import android.view.MotionEvent;

/* renamed from: X.2yN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60652yN extends C60842yj {
    public boolean A00;

    @Override // X.AbstractC28551Oa, android.view.ViewGroup
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return false;
    }

    @Override // X.AbstractC28551Oa, android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        return false;
    }

    public C60652yN(Context context, C28861Ph r3) {
        super(context, null, r3);
        A0Z();
    }

    @Override // X.AnonymousClass2xk, X.AnonymousClass1OZ, X.AbstractC28561Ob
    public void A0Z() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2P6 A07 = AnonymousClass1OY.A07(this);
            AnonymousClass01J A08 = AnonymousClass1OY.A08(A07, this);
            AnonymousClass1OY.A0L(A08, this);
            AnonymousClass1OY.A0M(A08, this);
            AnonymousClass1OY.A0K(A08, this);
            AnonymousClass1OY.A0I(A07, A08, this, AnonymousClass1OY.A09(A08, this, AnonymousClass1OY.A0B(A08, this)));
            ((C60842yj) this).A06 = (C22180yf) A08.A5U.get();
            ((C60842yj) this).A05 = A08.A2e();
            ((C60842yj) this).A04 = (C26171Ch) A08.A2a.get();
        }
    }
}
