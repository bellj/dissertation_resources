package X;

import android.os.SystemClock;
import java.net.URL;

/* renamed from: X.1wW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C43161wW {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public long A04;
    public long A05;
    public AnonymousClass1RN A06;
    public Boolean A07;
    public Float A08;
    public Integer A09;
    public Long A0A;
    public Long A0B;
    public Long A0C;
    public Long A0D;
    public Long A0E;
    public Long A0F;
    public Long A0G;
    public Long A0H;
    public Long A0I;
    public Long A0J;
    public Long A0K;
    public String A0L;
    public String A0M;
    public String A0N;
    public String A0O;
    public URL A0P;
    public boolean A0Q;
    public final long A0R = SystemClock.elapsedRealtime();
    public final C14370lK A0S;

    public C43161wW(C14370lK r3) {
        this.A0S = r3;
    }

    public C43161wW(C14370lK r3, int i, int i2) {
        this.A00 = i;
        this.A0S = r3;
        this.A01 = i2;
    }

    public long A00() {
        Long l = this.A0D;
        if (l == null) {
            return 0;
        }
        Long l2 = this.A0G;
        if (l2 != null) {
            return l2.longValue();
        }
        if (this.A0B == null) {
            return SystemClock.elapsedRealtime() - l.longValue();
        }
        return 0;
    }

    public synchronized long A01() {
        return this.A04;
    }

    public synchronized long A02() {
        return this.A05;
    }

    public Long A03() {
        long elapsedRealtime;
        long longValue;
        Long l = this.A0D;
        if (l == null) {
            longValue = 0;
        } else {
            Long l2 = this.A0B;
            if (l2 != null) {
                elapsedRealtime = l2.longValue();
            } else {
                elapsedRealtime = SystemClock.elapsedRealtime();
            }
            longValue = elapsedRealtime - l.longValue();
        }
        return Long.valueOf(longValue);
    }

    public void A04() {
        Long l = this.A0D;
        boolean z = false;
        if (l != null) {
            z = true;
        }
        AnonymousClass009.A0F(z);
        this.A0A = Long.valueOf(SystemClock.elapsedRealtime() - l.longValue());
        this.A02 = 1;
    }

    public void A05() {
        Long l = this.A0E;
        boolean z = false;
        if (l != null) {
            z = true;
        }
        AnonymousClass009.A0F(z);
        this.A0F = Long.valueOf(SystemClock.elapsedRealtime() - l.longValue());
    }

    public void A06() {
        this.A0E = Long.valueOf(SystemClock.elapsedRealtime());
        this.A02 = 3;
    }

    public void A07() {
        Long l = this.A0D;
        boolean z = true;
        boolean z2 = false;
        if (l != null) {
            z2 = true;
        }
        AnonymousClass009.A0F(z2);
        if (this.A0A == null) {
            z = false;
        }
        AnonymousClass009.A0F(z);
        this.A0K = Long.valueOf(SystemClock.elapsedRealtime() - l.longValue());
        this.A02 = 2;
    }

    public void A08() {
        Long l = this.A0D;
        boolean z = false;
        if (l != null) {
            z = true;
        }
        AnonymousClass009.A0F(z);
        this.A0G = Long.valueOf(SystemClock.elapsedRealtime() - l.longValue());
    }

    public synchronized void A09(long j, long j2) {
        this.A05 = j;
        this.A04 += j2;
    }

    public void A0A(Exception exc) {
        if (exc instanceof C37651mm) {
            Throwable cause = exc.getCause();
            if (cause == null) {
                cause = exc;
            }
            exc = cause;
        } else if (exc == null) {
            return;
        }
        this.A0M = exc.getClass().getName();
    }

    public String toString() {
        String obj;
        Long valueOf;
        long j;
        StringBuilder sb = new StringBuilder("MMS type: ");
        sb.append(this.A0S);
        sb.append(" retry count: ");
        sb.append(this.A0I);
        sb.append(" network stack: ");
        sb.append(this.A01);
        sb.append(" connction type: ");
        sb.append(this.A09);
        sb.append(" connection class: ");
        sb.append(this.A0L);
        sb.append(" url: ");
        URL url = this.A0P;
        if (url == null) {
            obj = null;
        } else {
            obj = url.toString();
        }
        sb.append(obj);
        sb.append(" download time: ");
        sb.append(A03());
        sb.append(" queue time: ");
        Long l = this.A0D;
        if (l == null) {
            valueOf = null;
        } else {
            valueOf = Long.valueOf(l.longValue() - this.A0R);
        }
        sb.append(valueOf);
        sb.append(" connction time: ");
        Long l2 = this.A0D;
        long j2 = 0;
        if (l2 != null) {
            Long l3 = this.A0A;
            if (l3 != null) {
                j2 = l3.longValue();
            } else if (this.A0B == null) {
                j2 = SystemClock.elapsedRealtime() - l2.longValue();
            }
        }
        sb.append(j2);
        sb.append(" route selection delay: ");
        sb.append(this.A0J);
        sb.append(" network time: ");
        sb.append(A00());
        sb.append(" connection reused: ");
        sb.append(this.A07);
        sb.append(" response code: ");
        sb.append(this.A0H);
        sb.append(" total bytes transferred: ");
        sb.append(A02());
        sb.append(" media ip: ");
        sb.append(this.A0O);
        sb.append(" exception: ");
        sb.append(this.A0M);
        sb.append(" download stage: ");
        sb.append(this.A02);
        sb.append(" download resume point: ");
        Long l4 = this.A0C;
        if (l4 != null) {
            j = l4.longValue();
        } else {
            j = 0;
        }
        sb.append(j);
        return sb.toString();
    }
}
