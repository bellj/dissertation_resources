package X;

import android.graphics.Insets;

/* renamed from: X.0U7  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0U7 {
    public static final AnonymousClass0U7 A04 = new AnonymousClass0U7(0, 0, 0, 0);
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;

    public AnonymousClass0U7(int i, int i2, int i3, int i4) {
        this.A01 = i;
        this.A03 = i2;
        this.A02 = i3;
        this.A00 = i4;
    }

    public static AnonymousClass0U7 A00(int i, int i2, int i3, int i4) {
        if (i == 0 && i2 == 0 && i3 == 0 && i4 == 0) {
            return A04;
        }
        return new AnonymousClass0U7(i, i2, i3, i4);
    }

    public static AnonymousClass0U7 A01(Insets insets) {
        return A00(insets.left, insets.top, insets.right, insets.bottom);
    }

    public Insets A02() {
        return C04030Kc.A00(this.A01, this.A03, this.A02, this.A00);
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj != null && AnonymousClass0U7.class == obj.getClass()) {
                AnonymousClass0U7 r5 = (AnonymousClass0U7) obj;
                if (!(this.A00 == r5.A00 && this.A01 == r5.A01 && this.A02 == r5.A02 && this.A03 == r5.A03)) {
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (((((this.A01 * 31) + this.A03) * 31) + this.A02) * 31) + this.A00;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Insets{left=");
        sb.append(this.A01);
        sb.append(", top=");
        sb.append(this.A03);
        sb.append(", right=");
        sb.append(this.A02);
        sb.append(", bottom=");
        sb.append(this.A00);
        sb.append('}');
        return sb.toString();
    }
}
