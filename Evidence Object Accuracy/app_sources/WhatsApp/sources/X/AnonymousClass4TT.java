package X;

import java.util.List;
import java.util.Map;

/* renamed from: X.4TT  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4TT {
    public final AnonymousClass4PX A00;
    public final AnonymousClass28D A01;
    public final AnonymousClass28D A02;
    public final List A03;
    public final Map A04;
    public final Map A05;
    public final Map A06;

    public AnonymousClass4TT(AnonymousClass4PX r1, AnonymousClass28D r2, AnonymousClass28D r3, List list, Map map, Map map2, Map map3) {
        this.A02 = r2;
        this.A01 = r3;
        this.A06 = map;
        this.A05 = map2;
        this.A04 = map3;
        this.A03 = list;
        this.A00 = r1;
    }
}
