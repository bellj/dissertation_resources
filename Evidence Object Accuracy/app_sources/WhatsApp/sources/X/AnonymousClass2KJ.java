package X;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

/* renamed from: X.2KJ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2KJ {
    public static void A00(Context context, Intent intent) {
        intent.setPackage(context.getPackageName());
        Intent intent2 = new Intent();
        intent2.setComponent(new ComponentName(context.getPackageName(), "FakeClass"));
        intent.putExtra("authentication_token", AnonymousClass1UY.A00(context.getApplicationContext(), 0, intent2, 0));
    }
}
