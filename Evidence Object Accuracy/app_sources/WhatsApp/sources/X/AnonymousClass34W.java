package X;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import com.whatsapp.R;
import com.whatsapp.conversation.conversationrow.ConversationRowAudioPreview;
import com.whatsapp.search.views.itemviews.AudioPlayerView;

/* renamed from: X.34W  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass34W extends AbstractC44071y9 {
    public ConversationRowAudioPreview A00 = ((ConversationRowAudioPreview) AnonymousClass028.A0D(this, R.id.search_row_audio_preview));
    public AnonymousClass19D A01;
    public AnonymousClass018 A02;
    public AudioPlayerView A03 = ((AudioPlayerView) AnonymousClass028.A0D(this, R.id.search_row_audio_controls));
    public AnonymousClass01H A04;
    public boolean A05;

    public AnonymousClass34W(Context context) {
        super(context);
        A00();
        setOrientation(0);
        setGravity(16);
        LinearLayout.inflate(context, R.layout.search_attachment_audio, this);
        setBackground(AnonymousClass2GE.A03(getContext(), C12970iu.A0C(context, R.drawable.search_attachment_background), R.color.search_attachment_background));
        C863246t r4 = new C863246t(this);
        AnonymousClass59E r3 = new AbstractC116165Uj() { // from class: X.59E
            @Override // X.AbstractC116165Uj
            public final C30421Xi ACs() {
                return AnonymousClass34W.this.A09;
            }
        };
        AudioPlayerView audioPlayerView = this.A03;
        audioPlayerView.setPlaybackListener(new AnonymousClass3OU(super.A03, audioPlayerView, r3, r4, this.A04));
    }

    public final void A02() {
        C30421Xi r11 = this.A09;
        AnonymousClass55I r2 = new AnonymousClass5U0() { // from class: X.55I
            @Override // X.AnonymousClass5U0
            public final void APZ(int i) {
                AnonymousClass34W r0 = AnonymousClass34W.this;
                r0.A00.setDuration(C38131nZ.A04(r0.A02, (long) i));
            }
        };
        AnonymousClass55L r3 = new AnonymousClass5U1() { // from class: X.55L
            @Override // X.AnonymousClass5U1
            public final void AW0(boolean z) {
                View findViewById;
                Activity A00 = AbstractC35731ia.A00(AnonymousClass34W.this.getContext());
                if (A00 != null && (findViewById = A00.findViewById(R.id.proximity_overlay)) != null) {
                    int i = 4;
                    if (z) {
                        i = 0;
                    }
                    findViewById.setVisibility(i);
                }
            }
        };
        AudioPlayerView audioPlayerView = this.A03;
        C60922z1 r0 = new C60922z1(this.A00, r2, r3, r3, this, audioPlayerView);
        C14850m9 r10 = super.A05;
        AnonymousClass599 r12 = new AbstractC116135Ug() { // from class: X.599
            @Override // X.AbstractC116135Ug
            public final void ATp(int i, String str) {
                ConversationRowAudioPreview conversationRowAudioPreview = AnonymousClass34W.this.A00;
                conversationRowAudioPreview.setDuration(str);
                if (i == 0) {
                    conversationRowAudioPreview.A03.setVisibility(0);
                    conversationRowAudioPreview.A00.setVisibility(8);
                } else if (i == 1) {
                    conversationRowAudioPreview.A00();
                }
            }
        };
        AnonymousClass3JG.A01(r0, super.A03, this.A02, r10, r11, r12, audioPlayerView);
    }
}
