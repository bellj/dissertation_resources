package X;

/* renamed from: X.4CK  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4CK extends Exception {
    public AnonymousClass4CK() {
    }

    public AnonymousClass4CK(String str) {
        super(str);
    }

    public AnonymousClass4CK(Throwable th) {
        super(th);
    }
}
