package X;

/* renamed from: X.5FI  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5FI implements AnonymousClass5VN {
    @Override // X.AnonymousClass5VN
    public void AgH(Appendable appendable, Object obj, C94884ch r7) {
        appendable.append('[');
        boolean z = true;
        for (Object obj2 : (Iterable) obj) {
            if (z) {
                z = false;
            } else {
                appendable.append(',');
            }
            if (obj2 == null) {
                appendable.append("null");
            } else {
                AnonymousClass4ZZ.A00(appendable, obj2, r7);
            }
        }
        appendable.append(']');
    }
}
