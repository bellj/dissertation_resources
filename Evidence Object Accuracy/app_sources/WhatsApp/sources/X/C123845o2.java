package X;

import android.text.TextUtils;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5o2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123845o2 extends AbstractC18440sS implements AbstractC18450sT {
    public AnonymousClass4LT A00;
    public final C128985wz A01;
    public final AnonymousClass01d A02;
    public final C16630pM A03;

    public C123845o2(C128985wz r1, AnonymousClass01d r2, AnonymousClass14A r3, C16630pM r4) {
        super(r3);
        this.A02 = r2;
        this.A03 = r4;
        this.A01 = r1;
    }

    @Override // X.AbstractC18440sS
    public void A00(boolean z) {
        AnonymousClass4LT r0 = this.A00;
        if (r0 != null) {
            r0.A00.A01.A06(-1);
        }
    }

    @Override // X.AbstractC18450sT
    public int AAT() {
        return AnonymousClass2BK.A02(this.A02, this.A03);
    }

    @Override // X.AbstractC18450sT
    public void AIp(C18840t8 r20) {
        C39051pB A08;
        C128985wz r7 = this.A01;
        C39041pA r2 = r7.A00;
        if (r2 != null) {
            HashMap A11 = C12970iu.A11();
            try {
                JSONArray jSONArray = new JSONArray(r7.A03.A01("bloks").getString("bk_cache_lookup_map", "{}"));
                for (int i = 0; i < jSONArray.length(); i++) {
                    String obj = jSONArray.get(i).toString();
                    if (!TextUtils.isEmpty(obj)) {
                        try {
                            JSONObject A05 = C13000ix.A05(obj);
                            C128975wy r12 = new C128975wy(A05.getString("shard-key"), A05.getString("entry-key"), A05.getLong("expiration-time"), A05.getLong("create-time"));
                            if (System.currentTimeMillis() > r12.A01 + r12.A00) {
                                StringBuilder A0h = C12960it.A0h();
                                A0h.append(r12.A03);
                                A0h.append(":");
                                try {
                                    r2.A0B(C12960it.A0d(r12.A02, A0h));
                                } catch (IOException unused) {
                                    Log.e("BkCacheSaveOnDiskHelper/saveOnDisk failed to remove the bk-cache");
                                }
                            } else {
                                StringBuilder A0h2 = C12960it.A0h();
                                A0h2.append(r12.A03);
                                A0h2.append(":");
                                A11.put(C12960it.A0d(r12.A02, A0h2), r12);
                            }
                        } catch (JSONException unused2) {
                            Log.e("BkCacheSaveOnDiskHelper:BkCacheValueHelper/fromJsonString threw exception");
                        }
                    }
                }
            } catch (JSONException unused3) {
                Log.e("BkCacheSaveOnDiskHelper/syncLookUpMapToDisk parsing lookUpMap from disk threw exception");
            }
            r7.A02 = A11;
            r7.A00();
            for (C128975wy r4 : r7.A02.values()) {
                String str = null;
                try {
                    StringBuilder A0h3 = C12960it.A0h();
                    A0h3.append(r4.A03);
                    A0h3.append(":");
                    A08 = r2.A08(C12960it.A0d(r4.A02, A0h3));
                } catch (IOException unused4) {
                    Log.e("BkCacheSaveOnDiskHelper/initDiskCache unable to fetch content from disk");
                }
                if (A08 == null) {
                    Log.i("BkCacheSaveOnDiskHelper/initDiskCache snapshot is null");
                } else {
                    InputStreamReader inputStreamReader = new InputStreamReader(A08.A00[0], C39041pA.A0D);
                    StringWriter stringWriter = new StringWriter();
                    char[] cArr = new char[EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH];
                    while (true) {
                        int read = inputStreamReader.read(cArr);
                        if (read == -1) {
                            break;
                        }
                        stringWriter.write(cArr, 0, read);
                    }
                    String obj2 = stringWriter.toString();
                    inputStreamReader.close();
                    str = obj2;
                    if (!TextUtils.isEmpty(str)) {
                        r20.A02(new C94824cb(str, r4.A01, r4.A00), r4.A03, r4.A02);
                    }
                }
            }
        }
    }

    @Override // X.AbstractC18450sT
    public void Aa2(AnonymousClass4LT r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC18450sT
    public void AaJ(String str, String str2) {
        C128985wz r3 = this.A01;
        StringBuilder A0j = C12960it.A0j(str);
        A0j.append(":");
        String A0d = C12960it.A0d(str2, A0j);
        C39041pA r0 = r3.A00;
        if (r0 == null) {
            Log.e("BkCacheSaveOnDiskHelper/removeOnDisk disk cache is not setup for bk cache");
        } else {
            try {
                r0.A0B(A0d);
            } catch (IOException unused) {
                Log.e("BkCacheSaveOnDiskHelper/saveOnDisk failed to remove the bk-cache");
            }
        }
        Map map = r3.A02;
        StringBuilder A0j2 = C12960it.A0j(str);
        A0j2.append(":");
        map.remove(C12960it.A0d(str2, A0j2));
        r3.A00();
    }

    @Override // X.AbstractC18450sT
    public void AbI(C94824cb r15, String str, String str2) {
        OutputStream outputStream;
        Throwable th;
        OutputStreamWriter outputStreamWriter;
        String str3;
        C128985wz r4 = this.A01;
        C39041pA r1 = r4.A00;
        if (r1 == null) {
            str3 = "BkCacheSaveOnDiskHelper/saveOnDisk disk cache is not setup for bk cache";
        } else {
            Object obj = r15.A02;
            if (obj == null) {
                str3 = "BkCacheSaveOnDiskHelper/saveOnDisk invalid value in CacheValue";
            } else {
                try {
                    StringBuilder A0j = C12960it.A0j(str);
                    A0j.append(":");
                    C39061pE A07 = r1.A07(C12960it.A0d(str2, A0j));
                    String str4 = (String) obj;
                    OutputStreamWriter outputStreamWriter2 = null;
                    try {
                        outputStream = A07.A00();
                        try {
                            outputStreamWriter = new OutputStreamWriter(outputStream, C39041pA.A0D);
                        } catch (Throwable th2) {
                            th = th2;
                        }
                    } catch (Throwable th3) {
                        th = th3;
                        outputStream = null;
                    }
                    try {
                        outputStreamWriter.write(str4);
                        C39041pA.A03(outputStreamWriter);
                        C39041pA.A03(outputStream);
                        A07.A01();
                        Map map = r4.A02;
                        StringBuilder A0j2 = C12960it.A0j(str);
                        A0j2.append(":");
                        map.put(C12960it.A0d(str2, A0j2), new C128975wy(str, str2, r15.A01, r15.A00));
                        r4.A00();
                        return;
                    } catch (Throwable th4) {
                        th = th4;
                        outputStreamWriter2 = outputStreamWriter;
                        C39041pA.A03(outputStreamWriter2);
                        C39041pA.A03(outputStream);
                        throw th;
                    }
                } catch (IOException unused) {
                    Log.e("BkCacheSaveOnDiskHelper/saveOnDisk failed to save the bk-cache");
                    return;
                }
            }
        }
        Log.e(str3);
    }
}
