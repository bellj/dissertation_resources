package X;

import android.content.Context;
import android.graphics.Paint;
import com.whatsapp.R;

/* renamed from: X.3Fx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64533Fx {
    public final Paint A00;
    public final C16590pI A01;
    public final AnonymousClass018 A02;

    public int A00(Context context) {
        return 0;
    }

    public boolean A04() {
        return true;
    }

    public boolean A05() {
        return false;
    }

    public boolean A07() {
        return false;
    }

    public boolean A08() {
        return false;
    }

    public boolean A0A() {
        return false;
    }

    public C64533Fx(C16590pI r4, AnonymousClass018 r5) {
        Paint A0F = C12990iw.A0F();
        this.A00 = A0F;
        this.A01 = r4;
        this.A02 = r5;
        C12970iu.A16(C16590pI.A00(r4).getColor(R.color.conversationRowSelectionColor), A0F);
        A0F.setAntiAlias(true);
    }

    public int A01(Context context) {
        return context.getResources().getDimensionPixelSize(R.dimen.normal_bubble_vertical_margin);
    }

    public int A02(Context context) {
        return Math.max(1, (int) (C12960it.A01(context) / 2.0f));
    }

    public int A03(Context context) {
        return context.getResources().getDimensionPixelSize(R.dimen.selection_padding_top);
    }

    public boolean A06() {
        return C28141Kv.A01(this.A02);
    }

    public boolean A09() {
        return C28141Kv.A00(this.A02);
    }
}
