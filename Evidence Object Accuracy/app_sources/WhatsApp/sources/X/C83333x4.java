package X;

import android.view.animation.Animation;
import android.widget.ImageButton;

/* renamed from: X.3x4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83333x4 extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ C14680lr A00;

    public C83333x4(C14680lr r1) {
        this.A00 = r1;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        ImageButton imageButton = this.A00.A09;
        imageButton.setClickable(true);
        imageButton.clearAnimation();
    }
}
