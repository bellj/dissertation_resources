package X;

import java.security.cert.CRLException;

/* renamed from: X.5OM  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5OM extends AbstractC113435Ho {
    public final byte[] A00;

    public AnonymousClass5OM(String str, AnonymousClass5MU r8, AnonymousClass5S2 r9, byte[] bArr, byte[] bArr2, boolean z) {
        super(str, r8, r9, bArr, z);
        this.A00 = bArr2;
    }

    @Override // X.AbstractC113435Ho, java.security.cert.X509CRL
    public byte[] getEncoded() {
        byte[] bArr = this.A00;
        if (bArr != null) {
            return bArr;
        }
        throw new CRLException();
    }
}
