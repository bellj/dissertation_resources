package X;

import com.whatsapp.jid.Jid;

/* renamed from: X.2J3  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2J3 implements AnonymousClass2J4 {
    public final /* synthetic */ C48302Fl A00;

    public AnonymousClass2J3(C48302Fl r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass2J4
    public C59412ug A84(AnonymousClass2K1 r14, AnonymousClass1B4 r15, Jid jid) {
        AnonymousClass01J r1 = this.A00.A03;
        r1.AMg.get();
        AbstractC15710nm r2 = (AbstractC15710nm) r1.A4o.get();
        AnonymousClass018 r9 = (AnonymousClass018) r1.ANb.get();
        C14850m9 r10 = (C14850m9) r1.A04.get();
        C16340oq r7 = (C16340oq) r1.A5z.get();
        C17170qN r8 = (C17170qN) r1.AMt.get();
        return new C59412ug(r2, (C14900mE) r1.A8X.get(), (C16430p0) r1.A5y.get(), r14, r15, r7, r8, r9, r10, jid, (AbstractC14440lR) r1.ANe.get());
    }
}
