package X;

import com.whatsapp.avatar.profilephoto.AvatarProfilePhotoActivity;

/* renamed from: X.3e4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C72133e4 extends AnonymousClass1WI implements AnonymousClass1J7 {
    public final /* synthetic */ AvatarProfilePhotoActivity this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C72133e4(AvatarProfilePhotoActivity avatarProfilePhotoActivity) {
        super(1);
        this.this$0 = avatarProfilePhotoActivity;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:28:0x0087 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r8v0, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r8v1, types: [X.1WF] */
    /* JADX WARN: Type inference failed for: r8v2, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // X.AnonymousClass1J7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ java.lang.Object AJ4(java.lang.Object r14) {
        /*
            r13 = this;
            r6 = r14
            X.4Ds r6 = (X.AbstractC87964Ds) r6
            r11 = 0
            X.C16700pc.A0E(r6, r11)
            boolean r0 = r6 instanceof X.C58702rF
            if (r0 == 0) goto L_0x009a
            com.whatsapp.avatar.profilephoto.AvatarProfilePhotoActivity r0 = r13.this$0
            X.0pd r0 = r0.A0B
            java.lang.Object r2 = r0.getValue()
            com.whatsapp.avatar.profilephoto.AvatarProfilePhotoViewModel r2 = (com.whatsapp.avatar.profilephoto.AvatarProfilePhotoViewModel) r2
            X.2rF r6 = (X.C58702rF) r6
            X.C16700pc.A0E(r6, r11)
            java.lang.String r0 = "AvatarProfilePhotoViewModel/onPoseSelected(item="
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            r1.append(r6)
            r0 = 41
            r1.append(r0)
            X.C12960it.A1F(r1)
            X.016 r4 = r2.A00
            java.lang.Object r0 = r4.A01()
            X.1Hx r0 = (X.C27541Hx) r0
            if (r0 == 0) goto L_0x0085
            java.util.List r0 = r0.A03
            if (r0 == 0) goto L_0x0085
            java.util.ArrayList r3 = X.C12960it.A0l()
            java.util.Iterator r2 = r0.iterator()
        L_0x0041:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0053
            java.lang.Object r1 = r2.next()
            boolean r0 = r1 instanceof X.C58702rF
            if (r0 == 0) goto L_0x0041
            r3.add(r1)
            goto L_0x0041
        L_0x0053:
            java.util.ArrayList r8 = X.C16760pi.A0E(r3)
            java.util.Iterator r5 = r3.iterator()
        L_0x005b:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x0087
            java.lang.Object r1 = r5.next()
            X.2rF r1 = (X.C58702rF) r1
            android.graphics.Bitmap r3 = r1.A01
            android.graphics.Bitmap r0 = r6.A01
            boolean r2 = X.C16700pc.A0O(r3, r0)
            int r0 = r1.A00
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            int r1 = r0.intValue()
            X.C16700pc.A0E(r3, r11)
            X.2rF r0 = new X.2rF
            r0.<init>(r3, r1, r2)
            r8.add(r0)
            goto L_0x005b
        L_0x0085:
            X.1WF r8 = X.AnonymousClass1WF.A00
        L_0x0087:
            X.1Hx r7 = X.C16700pc.A04(r4)
            r5 = 0
            r10 = 45
            r12 = 0
            r9 = r5
            X.1Hx r0 = X.C27541Hx.A00(r5, r6, r7, r8, r9, r10, r11, r12)
            r4.A0B(r0)
            X.1WZ r0 = X.AnonymousClass1WZ.A00
            return r0
        L_0x009a:
            java.lang.String r0 = "Adapter can only handle poses."
            java.lang.IllegalStateException r0 = X.C12960it.A0U(r0)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C72133e4.AJ4(java.lang.Object):java.lang.Object");
    }
}
