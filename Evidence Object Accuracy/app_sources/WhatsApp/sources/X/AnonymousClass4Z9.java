package X;

/* renamed from: X.4Z9  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4Z9 {
    public static final char[] A00 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ $%*+-./:".toCharArray();

    public static char A00(int i) {
        char[] cArr = A00;
        if (i < cArr.length) {
            return cArr[i];
        }
        throw C82573vq.A00();
    }
}
