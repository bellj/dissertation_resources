package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.4BK  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4BK extends Enum {
    public static final /* synthetic */ AnonymousClass4BK[] A00;
    public static final AnonymousClass4BK A01;
    public final int attributeId;
    public final AbstractC91994Ua shape;

    public static AnonymousClass4BK valueOf(String str) {
        return (AnonymousClass4BK) Enum.valueOf(AnonymousClass4BK.class, str);
    }

    public static AnonymousClass4BK[] values() {
        return (AnonymousClass4BK[]) A00.clone();
    }

    static {
        AnonymousClass4BK r4 = new AnonymousClass4BK(new AnonymousClass48A(), "CIRCLE", 0, 0);
        A01 = r4;
        AnonymousClass4BK[] r0 = new AnonymousClass4BK[2];
        C72453ed.A1J(r4, new AnonymousClass4BK(new AnonymousClass48B(), "SQUIRCLE", 1, 1), r0);
        A00 = r0;
    }

    public AnonymousClass4BK(AbstractC91994Ua r1, String str, int i, int i2) {
        this.attributeId = i2;
        this.shape = r1;
    }
}
