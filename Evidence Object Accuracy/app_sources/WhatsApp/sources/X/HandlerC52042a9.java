package X;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/* renamed from: X.2a9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class HandlerC52042a9 extends Handler {
    public final Context A00;
    public final AnonymousClass19M A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public /* synthetic */ HandlerC52042a9(Context context, Looper looper, AnonymousClass19M r4) {
        super(looper);
        AnonymousClass009.A05(looper);
        this.A00 = context.getApplicationContext();
        this.A01 = r4;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        long j;
        C52502ax r2;
        Message obtain;
        AnonymousClass4RJ r0 = (AnonymousClass4RJ) message.obj;
        AbstractC32691cZ r5 = null;
        if (r0 != null) {
            r2 = r0.A01;
            r5 = r0.A02;
            j = r0.A00;
        } else {
            j = -1;
            r2 = null;
        }
        AnonymousClass19M r3 = this.A01;
        Resources resources = this.A00.getResources();
        AnonymousClass009.A05(r5);
        Drawable A04 = r3.A04(resources, r5, 0.6f, j);
        if (r2 != null) {
            r2.setTag(A04);
            obtain = Message.obtain(AnonymousClass3IZ.A0V, 0, 0, 0, C12990iw.A0L(Long.valueOf(j), r2));
        } else {
            obtain = Message.obtain(AnonymousClass3IZ.A0V, 1, 0, 0, C12990iw.A0L(Long.valueOf(j), A04));
        }
        obtain.sendToTarget();
    }
}
