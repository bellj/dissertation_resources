package X;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.facebook.redex.RunnableBRunnable0Shape1S0210000_I1;
import com.whatsapp.gesture.VerticalSwipeDismissBehavior;

/* renamed from: X.2ev  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53722ev extends AnonymousClass04X {
    public int A00 = -1;
    public int A01;
    public final /* synthetic */ VerticalSwipeDismissBehavior A02;

    public C53722ev(VerticalSwipeDismissBehavior verticalSwipeDismissBehavior) {
        this.A02 = verticalSwipeDismissBehavior;
    }

    @Override // X.AnonymousClass04X
    public void A00(int i) {
        AnonymousClass5X1 r0 = this.A02.A05;
        if (r0 != null) {
            r0.APV(i);
        }
    }

    @Override // X.AnonymousClass04X
    public int A02(View view) {
        return view.getHeight();
    }

    @Override // X.AnonymousClass04X
    public int A03(View view, int i, int i2) {
        return view.getLeft();
    }

    @Override // X.AnonymousClass04X
    public int A04(View view, int i, int i2) {
        int width = this.A01 - view.getWidth();
        return Math.min(Math.max(width, i), this.A01 + view.getWidth());
    }

    @Override // X.AnonymousClass04X
    public void A05(View view, float f, float f2) {
        int i;
        boolean z;
        AnonymousClass5X1 r0;
        this.A00 = -1;
        VerticalSwipeDismissBehavior verticalSwipeDismissBehavior = this.A02;
        if (verticalSwipeDismissBehavior.A0J(view, f2, this.A01)) {
            int top = view.getTop();
            int i2 = this.A01;
            int height = view.getHeight();
            if (top < i2) {
                i = i2 - height;
            } else {
                i = i2 + height;
            }
            z = true;
        } else {
            i = this.A01;
            z = false;
        }
        if (verticalSwipeDismissBehavior.A07) {
            if (!z || (r0 = verticalSwipeDismissBehavior.A05) == null) {
                if (!verticalSwipeDismissBehavior.A04.A0C(view.getLeft(), i)) {
                    return;
                }
                view.postOnAnimation(new RunnableBRunnable0Shape1S0210000_I1(verticalSwipeDismissBehavior, view, 1, z));
            }
            r0.APG(view);
            return;
        }
        if (!verticalSwipeDismissBehavior.A04.A0C(view.getLeft(), i)) {
            if (!z || (r0 = verticalSwipeDismissBehavior.A05) == null) {
                return;
            }
            r0.APG(view);
            return;
        }
        view.postOnAnimation(new RunnableBRunnable0Shape1S0210000_I1(verticalSwipeDismissBehavior, view, 1, z));
    }

    @Override // X.AnonymousClass04X
    public void A06(View view, int i) {
        if (this.A00 == -1) {
            this.A00 = i;
            this.A01 = view.getTop();
        }
        ViewParent parent = view.getParent();
        if (parent != null) {
            if (parent instanceof CoordinatorLayout) {
                ViewGroup viewGroup = (ViewGroup) parent;
                int childCount = viewGroup.getChildCount();
                for (int i2 = 0; i2 < childCount; i2++) {
                    if (((AnonymousClass0B5) viewGroup.getChildAt(i2).getLayoutParams()).A0A == this.A02) {
                        return;
                    }
                }
            }
            parent.requestDisallowInterceptTouchEvent(true);
        }
    }

    @Override // X.AnonymousClass04X
    public void A07(View view, int i, int i2, int i3, int i4) {
        AnonymousClass5X1 r3 = this.A02.A05;
        if (r3 != null) {
            r3.AWA(view, Math.min(1.0f, (((float) Math.abs(i2)) * 1.0f) / C12990iw.A03(view)));
        }
    }

    @Override // X.AnonymousClass04X
    public boolean A08(View view, int i) {
        return !this.A02.A0B;
    }
}
