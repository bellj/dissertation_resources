package X;

/* renamed from: X.4x0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107234x0 implements AnonymousClass5VY {
    public C100614mC A00;
    public AnonymousClass5X6 A01;
    public AnonymousClass4YB A02;

    public C107234x0(String str) {
        C93844ap A00 = C93844ap.A00();
        A00.A0R = str;
        this.A00 = new C100614mC(A00);
    }

    @Override // X.AnonymousClass5VY
    public void A7a(C95304dT r14) {
        long j;
        AnonymousClass4YB r0 = this.A02;
        C95314dV.A01(r0);
        long A01 = r0.A01();
        if (A01 != -9223372036854775807L) {
            C100614mC r5 = this.A00;
            if (A01 != r5.A0J) {
                C93844ap r02 = new C93844ap(r5);
                r02.A0H = A01;
                C100614mC r1 = new C100614mC(r02);
                this.A00 = r1;
                this.A01.AA6(r1);
            }
            int A00 = C95304dT.A00(r14);
            this.A01.AbC(r14, A00);
            AnonymousClass5X6 r6 = this.A01;
            AnonymousClass4YB r52 = this.A02;
            synchronized (r52) {
                long j2 = r52.A01;
                j = -9223372036854775807L;
                if (j2 != -9223372036854775807L) {
                    j = r52.A02 + j2;
                } else {
                    long j3 = r52.A00;
                    if (j3 != Long.MAX_VALUE) {
                        j = j3;
                    }
                }
            }
            r6.AbG(null, 1, A00, 0, j);
        }
    }

    @Override // X.AnonymousClass5VY
    public void AIe(AbstractC14070ko r3, C92824Xo r4, AnonymousClass4YB r5) {
        this.A02 = r5;
        r4.A03();
        AnonymousClass5X6 Af4 = r3.Af4(r4.A01(), 5);
        this.A01 = Af4;
        Af4.AA6(this.A00);
    }
}
