package X;

/* renamed from: X.5sX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C126225sX {
    public final AnonymousClass1V8 A00;

    public C126225sX(AnonymousClass3CT r8, String str, String str2, String str3) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy A0N = C117295Zj.A0N(A0M);
        C41141sy.A01(A0N, "action", "br-device-registration");
        if (C117295Zj.A1W(str, 1, false)) {
            C41141sy.A01(A0N, "nonce", str);
        }
        C41141sy r3 = new C41141sy("elo");
        if (C117305Zk.A1X(str2, 1, false)) {
            C41141sy.A01(r3, "ciphered_binding_info", str2);
        }
        if (C117295Zj.A1V(str3, 1, false)) {
            C41141sy.A01(r3, "network_device_id", str3);
        }
        C117295Zj.A1H(r3, A0N);
        this.A00 = C117295Zj.A0J(A0N, A0M, r8);
    }
}
