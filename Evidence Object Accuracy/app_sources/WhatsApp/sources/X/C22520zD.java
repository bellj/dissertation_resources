package X;

import android.app.ActivityManager;
import android.text.TextUtils;
import com.facebook.redex.RunnableBRunnable0Shape0S0200000_I0;
import com.whatsapp.push.RegistrationIntentService;
import com.whatsapp.util.Log;
import java.util.List;

/* renamed from: X.0zD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22520zD extends AbstractC18860tB {
    public boolean A00;
    public boolean A01;
    public final C22490zA A02;
    public final C22260yn A03;
    public final C15570nT A04;
    public final C002701f A05;
    public final C22330yu A06;
    public final C15550nR A07;
    public final C21830y3 A08;
    public final C22160yd A09;
    public final C15410nB A0A;
    public final C16590pI A0B;
    public final C14820m6 A0C;
    public final C22500zB A0D;
    public final C21540xa A0E;
    public final C17650rA A0F;
    public final C17660rB A0G;
    public final C17720rH A0H;
    public final C22320yt A0I;
    public final C20830wO A0J;
    public final C15650ng A0K;
    public final C22340yv A0L;
    public final C22350yw A0M;
    public final C22440z5 A0N;
    public final C22300yr A0O;
    public final C22430z4 A0P;
    public final C22290yq A0Q;
    public final C22180yf A0R;
    public final C14850m9 A0S;
    public final C22050yP A0T;
    public final C16030oK A0U;
    public final C17040qA A0V;
    public final C22310ys A0W;
    public final C22370yy A0X;
    public final C22400z1 A0Y;
    public final C22380yz A0Z;
    public final C22390z0 A0a;
    public final C19890uq A0b;
    public final C22360yx A0c;
    public final C22170ye A0d;
    public final C20740wF A0e;
    public final C20660w7 A0f;
    public final C22410z2 A0g;
    public final C22470z8 A0h;
    public final C22420z3 A0i;
    public final C22230yk A0j;
    public final C22510zC A0k;
    public final C20220vP A0l;
    public final C22480z9 A0m;
    public final C22460z7 A0n;
    public final C20360vd A0o;
    public final C22280yp A0p;
    public final C20320vZ A0q;
    public final C22270yo A0r;
    public final C22210yi A0s;
    public final C21300xC A0t;
    public final C22450z6 A0u;
    public final C22190yg A0v;
    public final ExecutorC27271Gr A0w;
    public final AbstractC14440lR A0x;
    public final C14890mD A0y;

    public C22520zD(C22490zA r4, C22260yn r5, C15570nT r6, C002701f r7, C22330yu r8, C15550nR r9, C21830y3 r10, C22160yd r11, C15410nB r12, C16590pI r13, C14820m6 r14, C22500zB r15, C21540xa r16, C17650rA r17, C17660rB r18, C17720rH r19, C22320yt r20, C20830wO r21, C15650ng r22, C22340yv r23, C22350yw r24, C22440z5 r25, C22300yr r26, C22430z4 r27, C22290yq r28, C22180yf r29, C14850m9 r30, C22050yP r31, C16030oK r32, C17040qA r33, C22310ys r34, C22370yy r35, C22400z1 r36, C22380yz r37, C22390z0 r38, C19890uq r39, C22360yx r40, C22170ye r41, C20740wF r42, C20660w7 r43, C22410z2 r44, C22470z8 r45, C22420z3 r46, C22230yk r47, C22510zC r48, C20220vP r49, C22480z9 r50, C22460z7 r51, C20360vd r52, C22280yp r53, C20320vZ r54, C22270yo r55, C22210yi r56, C21300xC r57, C22450z6 r58, C22190yg r59, AbstractC14440lR r60, C14890mD r61) {
        this.A0B = r13;
        this.A0S = r30;
        this.A04 = r6;
        this.A0x = r60;
        this.A09 = r11;
        this.A0y = r61;
        this.A0f = r43;
        this.A0d = r41;
        this.A07 = r9;
        this.A0R = r29;
        this.A0v = r59;
        this.A0b = r39;
        this.A0s = r56;
        this.A0j = r47;
        this.A03 = r5;
        this.A0q = r54;
        this.A0r = r55;
        this.A0p = r53;
        this.A0Q = r28;
        this.A0t = r57;
        this.A0K = r22;
        this.A0O = r26;
        this.A0T = r31;
        this.A0W = r34;
        this.A0A = r12;
        this.A0u = r58;
        this.A0I = r20;
        this.A06 = r8;
        this.A0L = r23;
        this.A0M = r24;
        this.A0e = r42;
        this.A0V = r33;
        this.A0c = r40;
        this.A0l = r49;
        this.A0C = r14;
        this.A0X = r35;
        this.A0Z = r37;
        this.A0a = r38;
        this.A0Y = r36;
        this.A0g = r44;
        this.A0i = r46;
        this.A0U = r32;
        this.A0F = r17;
        this.A0P = r27;
        this.A0o = r52;
        this.A0N = r25;
        this.A0J = r21;
        this.A0n = r51;
        this.A05 = r7;
        this.A0h = r45;
        this.A0G = r18;
        this.A0m = r50;
        this.A08 = r10;
        this.A02 = r4;
        this.A0D = r15;
        this.A0E = r16;
        this.A0k = r48;
        this.A0H = r19;
        this.A0w = new ExecutorC27271Gr(r60, false);
    }

    public static boolean A00(AbstractC15340mz r3) {
        if (r3 instanceof AbstractC30571Xy) {
            AbstractC30571Xy r32 = (AbstractC30571Xy) r3;
            int i = ((AnonymousClass1XB) r32).A00;
            if (i == 65 || i == 66 || !r32.A01) {
                return true;
            }
            return false;
        } else if (!(r3 instanceof AnonymousClass1XB) || ((AnonymousClass1XB) r3).A00 != 42) {
            return false;
        } else {
            return true;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:105:0x0212, code lost:
        if (r19.A00 == false) goto L_0x0214;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:0x02b7, code lost:
        if (r19.A00 == false) goto L_0x02b9;
     */
    @Override // X.AbstractC18860tB
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(X.AbstractC15340mz r20, int r21) {
        /*
        // Method dump skipped, instructions count: 1320
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22520zD.A01(X.0mz, int):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0045, code lost:
        if (r1 != 13) goto L_0x0060;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00c4, code lost:
        if (r4 != false) goto L_0x0039;
     */
    @Override // X.AbstractC18860tB
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02(X.AbstractC15340mz r12, int r13) {
        /*
        // Method dump skipped, instructions count: 696
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22520zD.A02(X.0mz, int):void");
    }

    public final void A03(long j) {
        ActivityManager A03;
        String obj;
        C22270yo r3 = this.A0r;
        if (!r3.A02 && j > 900000 && this.A0c.A00()) {
            RegistrationIntentService.A01(this.A0B.A00);
            try {
                A03 = this.A0A.A00.A03();
            } catch (Exception e) {
                Log.w("app/logprocess/error", e);
            }
            if (A03 == null) {
                obj = "app/logprocess am=null";
            } else {
                List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = A03.getRunningAppProcesses();
                if (runningAppProcesses != null) {
                    for (ActivityManager.RunningAppProcessInfo runningAppProcessInfo : runningAppProcesses) {
                        if (TextUtils.equals(runningAppProcessInfo.processName, "com.google.process.gapps")) {
                            StringBuilder sb = new StringBuilder();
                            sb.append("app/logprocess/procinfo ");
                            sb.append(runningAppProcessInfo.processName);
                            sb.append(' ');
                            sb.append(runningAppProcessInfo.pid);
                            obj = sb.toString();
                        }
                    }
                }
                r3.A02 = true;
            }
            Log.i(obj);
            r3.A02 = true;
        }
    }

    public final void A04(AbstractC15340mz r9) {
        AnonymousClass1IR r0;
        C30921Zi A01;
        C14850m9 r1 = this.A0S;
        if ((r1.A07(605) || r1.A07(629)) && (r0 = r9.A0L) != null && (A01 = r0.A01()) != null) {
            C22460z7 r6 = this.A0n;
            if (!r6.A05.A07(1084) || A01.A02(r6.A00.A09()).exists()) {
                C14820m6 r5 = r6.A02;
                if (r5.A00.getLong("payment_backgrounds_batch_last_fetch_timestamp", -1) == -1) {
                    r6.A0D.Aaz(new C455422b(null, r6), new Void[0]);
                } else if (!A01.A02(r6.A00.A09()).exists()) {
                    r5.A19(true);
                }
            } else {
                r6.A0C.A03(A01, r6.A0A);
            }
        }
    }

    public final void A05(AbstractC15340mz r6) {
        String str;
        C30061Vy r4 = (C30061Vy) r6.A0E();
        if (r4 != null) {
            C15650ng r0 = this.A0K;
            AbstractC15340mz A03 = r0.A0K.A03(r4.A0z);
            if (A03 == null) {
                this.A0W.A00(new AbstractC28771Oy(r6) { // from class: X.22l
                    public final /* synthetic */ AbstractC15340mz A01;

                    {
                        this.A01 = r2;
                    }

                    @Override // X.AbstractC28771Oy
                    public /* synthetic */ void APN(long j) {
                    }

                    @Override // X.AbstractC28771Oy
                    public /* synthetic */ void APP(boolean z) {
                    }

                    @Override // X.AbstractC28771Oy
                    public final void APQ(AnonymousClass1RN r42, C28781Oz r5) {
                        C22520zD r2 = C22520zD.this;
                        AbstractC15340mz r1 = this.A01;
                        if (r42.A00 == 0) {
                            r2.A0K.A0X(r1);
                        }
                    }
                }, r4, 1, false);
                return;
            }
            if (A03 instanceof AbstractC16130oV) {
                C28921Pn A032 = this.A0X.A03((AbstractC16130oV) A03);
                if (A032 != null) {
                    A032.A5g(new C28761Ox(this, r6, r4));
                    return;
                }
                str = "MainMessageObserver/downloadQuotedMessageForSticker original downloader is null";
            } else {
                StringBuilder sb = new StringBuilder("MainMessageObserver/downloadQuotedMessageForSticker originalMessage not mediaMessage: ");
                sb.append(A03.getClass().getName());
                str = sb.toString();
            }
            Log.e(str);
        }
    }

    public final void A06(AbstractC15340mz r4) {
        AbstractC14440lR r2;
        int i;
        if (r4 instanceof AbstractC16130oV) {
            r2 = this.A0x;
            i = 18;
        } else {
            if ((r4 instanceof C28861Ph) && r4.A0T != null) {
                C22400z1 r1 = this.A0Y;
                if (r1.A01(r4)) {
                    r1.A06.A06(r4, 1);
                }
            } else if (r4 instanceof C28851Pg) {
                r2 = this.A0x;
                i = 19;
            }
            this.A0x.Ab2(new RunnableBRunnable0Shape0S0200000_I0(this, 17, r4));
        }
        r2.Ab2(new RunnableBRunnable0Shape0S0200000_I0(this, i, r4));
        this.A0x.Ab2(new RunnableBRunnable0Shape0S0200000_I0(this, 17, r4));
    }
}
