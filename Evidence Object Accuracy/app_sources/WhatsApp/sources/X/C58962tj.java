package X;

import java.io.File;

/* renamed from: X.2tj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58962tj extends AbstractC38761of implements AbstractC38611oO {
    public final C15450nH A00;
    public final C18790t3 A01;
    public final C14850m9 A02;
    public final C16120oU A03;
    public final C20110vE A04;
    public final C22600zL A05;

    @Override // X.AbstractC38611oO
    public /* synthetic */ void APO(long j) {
    }

    public C58962tj(C14900mE r10, C15450nH r11, C18790t3 r12, AbstractC38741od r13, C14850m9 r14, C16120oU r15, C20110vE r16, C22600zL r17, File file, String str, int i) {
        super(r10, r13, file, str, i, 16777216);
        this.A02 = r14;
        this.A01 = r12;
        this.A00 = r11;
        this.A03 = r15;
        this.A05 = r17;
        this.A04 = r16;
    }
}
