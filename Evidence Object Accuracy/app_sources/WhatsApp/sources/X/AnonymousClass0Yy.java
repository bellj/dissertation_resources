package X;

import android.text.TextUtils;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import com.whatsapp.R;

/* renamed from: X.0Yy  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0Yy implements AbstractC11840gx {
    public static AnonymousClass0Yy A00;

    @Override // X.AbstractC11840gx
    public CharSequence AZc(Preference preference) {
        ListPreference listPreference = (ListPreference) preference;
        if (TextUtils.isEmpty(listPreference.A0T())) {
            return ((Preference) listPreference).A05.getString(R.string.not_set);
        }
        return listPreference.A0T();
    }
}
