package X;

import java.io.Serializable;

/* renamed from: X.3tf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C81223tf extends AbstractC112285Cu implements Serializable {
    public static final C81223tf INSTANCE = new C81223tf();
    public static final long serialVersionUID = 0;

    @Override // java.lang.Object
    public String toString() {
        return "Ordering.natural()";
    }

    public int compare(Comparable comparable, Comparable comparable2) {
        return comparable.compareTo(comparable2);
    }

    private Object readResolve() {
        return INSTANCE;
    }

    @Override // X.AbstractC112285Cu
    public AbstractC112285Cu reverse() {
        return C81233tg.INSTANCE;
    }
}
