package X;

import android.database.Cursor;
import java.util.HashMap;

/* renamed from: X.1Wx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30311Wx extends AnonymousClass1Iv implements AbstractC16420oz {
    public int A00;
    public int A01;
    public long A02;

    public C30311Wx(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 68, j);
    }

    @Override // X.AnonymousClass1Iv
    public void A15(Cursor cursor, C18460sU r7, HashMap hashMap) {
        super.A15(cursor, r7, hashMap);
        int A00 = AnonymousClass1Tx.A00("sender_timestamp", hashMap);
        int A002 = AnonymousClass1Tx.A00("keep_in_chat_state", hashMap);
        int A003 = AnonymousClass1Tx.A00("keep_count", hashMap);
        long j = cursor.getLong(A00);
        int i = cursor.getInt(A002);
        int i2 = cursor.getInt(A003);
        this.A02 = j;
        this.A01 = i;
        int i3 = 0;
        if (i == 0) {
            i3 = 7;
        }
        ((AbstractC15340mz) this).A01 = i3;
        this.A00 = i2;
    }

    @Override // X.AbstractC16420oz
    public void A6k(C39971qq r7) {
        AbstractC14640lm r0;
        AnonymousClass4BS r2;
        AnonymousClass1G3 r4 = r7.A03;
        C47802Cs r02 = ((C27081Fy) r4.A00).A0M;
        if (r02 == null) {
            r02 = C47802Cs.A04;
        }
        AnonymousClass1G4 A0T = r02.A0T();
        AnonymousClass1G8 r03 = ((C47802Cs) A0T.A00).A03;
        if (r03 == null) {
            r03 = AnonymousClass1G8.A05;
        }
        AnonymousClass1G9 r22 = (AnonymousClass1G9) r03.A0T();
        AnonymousClass1IS A14 = A14();
        C40711sC r04 = ((AnonymousClass1Iv) this).A02;
        if (r04 == null) {
            r0 = null;
        } else {
            r0 = r04.A00;
        }
        AnonymousClass009.A05(A14);
        C40721sD.A02(r0, r22, A14);
        A0T.A03();
        C47802Cs r1 = (C47802Cs) A0T.A00;
        r1.A03 = (AnonymousClass1G8) r22.A02();
        r1.A00 |= 1;
        if (this.A01 == 1) {
            r2 = AnonymousClass4BS.A01;
        } else {
            r2 = AnonymousClass4BS.A02;
        }
        A0T.A03();
        C47802Cs r12 = (C47802Cs) A0T.A00;
        r12.A00 |= 2;
        r12.A01 = r2.value;
        long j = this.A02;
        A0T.A03();
        C47802Cs r13 = (C47802Cs) A0T.A00;
        r13.A00 |= 4;
        r13.A02 = j;
        r4.A03();
        C27081Fy r14 = (C27081Fy) r4.A00;
        r14.A0M = (C47802Cs) A0T.A02();
        r14.A01 |= 256;
    }
}
