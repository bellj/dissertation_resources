package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import com.whatsapp.jid.Jid;
import com.whatsapp.util.Log;
import java.util.LinkedHashMap;
import java.util.Map;

/* renamed from: X.0yr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C22300yr {
    public final AnonymousClass1F9 A00;
    public final C18460sU A01;
    public final Map A02;
    public final Map A03;

    public C22300yr(AnonymousClass1F9 r3, C18460sU r4) {
        C16700pc.A0E(r4, 1);
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        this.A01 = r4;
        this.A00 = r3;
        this.A02 = linkedHashMap;
        this.A03 = linkedHashMap2;
    }

    public C27631Ih A00(AnonymousClass1MU r13) {
        long j;
        C16700pc.A0E(r13, 0);
        C18460sU r9 = this.A01;
        long A01 = r9.A01(r13);
        if (A01 < 0) {
            return null;
        }
        Map map = this.A02;
        Long valueOf = Long.valueOf(A01);
        Number number = (Number) map.get(valueOf);
        if (number == null) {
            C16310on A012 = this.A00.A00.get();
            try {
                Cursor A09 = A012.A03.A09("SELECT jid_row_id FROM jid_map WHERE lid_row_id = ?", new String[]{String.valueOf(A01)});
                int columnIndex = A09.getColumnIndex("jid_row_id");
                if (columnIndex < 0 || !A09.moveToFirst()) {
                    A09.close();
                    A012.close();
                    j = -1;
                } else {
                    j = (long) A09.getInt(columnIndex);
                    A09.close();
                    A012.close();
                }
            } catch (Throwable th) {
                try {
                    A012.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } else {
            j = number.longValue();
        }
        Jid A03 = r9.A03(j);
        if (!(A03 instanceof C27631Ih)) {
            return null;
        }
        C27631Ih r6 = (C27631Ih) A03;
        Long valueOf2 = Long.valueOf(j);
        map.put(valueOf, valueOf2);
        this.A03.put(valueOf2, valueOf);
        return r6;
    }

    public void A01(AnonymousClass1MU r11, C27631Ih r12) {
        Number number;
        C16700pc.A0E(r11, 0);
        C16700pc.A0E(r12, 1);
        C18460sU r0 = this.A01;
        long A01 = r0.A01(r11);
        long A012 = r0.A01(r12);
        if (A01 >= 0 && A012 >= 0 && A01 != A012) {
            Map map = this.A02;
            Long valueOf = Long.valueOf(A01);
            if (!map.containsKey(valueOf) || (number = (Number) map.get(valueOf)) == null || number.longValue() != A012) {
                try {
                    C16310on A02 = this.A00.A00.A02();
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("lid_row_id", valueOf);
                    Long valueOf2 = Long.valueOf(A012);
                    contentValues.put("jid_row_id", valueOf2);
                    A02.A03.A06(contentValues, "jid_map", 5);
                    A02.close();
                    map.put(valueOf, valueOf2);
                    this.A03.put(valueOf2, valueOf);
                } catch (SQLException e) {
                    Log.e(e);
                }
            }
        }
    }
}
