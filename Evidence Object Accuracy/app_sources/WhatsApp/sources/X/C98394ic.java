package X;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

/* renamed from: X.4ic  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C98394ic implements IInterface {
    public final IBinder A00;
    public final String A01;

    public C98394ic(IBinder iBinder, String str) {
        this.A00 = iBinder;
        this.A01 = str;
    }

    @Override // android.os.IInterface
    public final IBinder asBinder() {
        return this.A00;
    }

    public final Parcel A00(int i, Parcel parcel) {
        try {
            parcel = Parcel.obtain();
            C12990iw.A18(this.A00, parcel, parcel, i);
            return parcel;
        } catch (RuntimeException e) {
            throw e;
        } finally {
            parcel.recycle();
        }
    }
}
