package X;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Debug;
import android.os.StatFs;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Locale;

/* renamed from: X.0ty  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19350ty {
    public final C16210od A00;
    public final AbstractC15710nm A01;
    public final C20640w5 A02;
    public final C15450nH A03;
    public final AnonymousClass01d A04;
    public final C16590pI A05;
    public final C14820m6 A06;
    public final C14850m9 A07;
    public final C16630pM A08;
    public volatile AbstractC18730sv A09;

    public C19350ty(C16210od r1, AbstractC15710nm r2, C20640w5 r3, C15450nH r4, AnonymousClass01d r5, C16590pI r6, C14820m6 r7, C14850m9 r8, C16630pM r9) {
        this.A07 = r8;
        this.A05 = r6;
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = r4;
        this.A04 = r5;
        this.A06 = r7;
        this.A08 = r9;
        this.A00 = r1;
    }

    public File A00() {
        File file = new File(this.A05.A00.getFilesDir(), "crash_in_video_sentinel");
        if (!file.createNewFile()) {
            StringBuilder sb = new StringBuilder("mediatranscodequeue/failed-to-create/");
            sb.append(file.getAbsolutePath());
            Log.w(sb.toString());
        }
        return file;
    }

    public void A01() {
        File file = new File(this.A05.A00.getFilesDir(), "crash_in_video_sentinel");
        if (file.exists() && !file.delete()) {
            Log.w("crashlogs/failed-delete-crash-sentinel-file");
        }
    }

    public void A02() {
        C15450nH r7 = this.A03;
        if (r7.A05(AbstractC15460nI.A0I)) {
            File file = new File(this.A05.A00.getFilesDir(), "crash_counter");
            int[] iArr = new int[2];
            try {
                if (!file.exists()) {
                    file.createNewFile();
                } else {
                    try {
                        ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file));
                        String[] split = ((String) objectInputStream.readObject()).split(",");
                        for (int i = 0; i < split.length; i++) {
                            iArr[i] = Integer.parseInt(split[i]);
                        }
                        objectInputStream.close();
                    } catch (Exception e) {
                        Log.e("Unable to read from crash counter file", e);
                    }
                }
                if (this.A00.A00) {
                    iArr[0] = iArr[0] + 1;
                } else {
                    iArr[1] = iArr[1] + 1;
                }
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(file));
                StringBuilder sb = new StringBuilder();
                sb.append(iArr[0]);
                sb.append(",");
                sb.append(iArr[1]);
                objectOutputStream.writeObject(sb.toString());
                objectOutputStream.close();
                if (this.A08.A03()) {
                    if (iArr[0] >= r7.A02(AbstractC15460nI.A1F)) {
                        AbstractC15710nm r8 = this.A01;
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("Crashes count foreground: ");
                        sb2.append(iArr[0]);
                        sb2.append(" Crashes count background: ");
                        sb2.append(iArr[1]);
                        r8.AaV("ForegroundAppCrashLoop", sb2.toString(), false);
                    } else if (iArr[1] >= r7.A02(AbstractC15460nI.A1E)) {
                        AbstractC15710nm r82 = this.A01;
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("Crashes count foreground: ");
                        sb3.append(iArr[0]);
                        sb3.append(" Crashes count background: ");
                        sb3.append(iArr[1]);
                        r82.AaV("BackgroundAppCrashLoop", sb3.toString(), false);
                    }
                }
            } catch (Exception e2) {
                Log.e("Unable to use crash counter file", e2);
            }
            if (r7.A05(AbstractC15460nI.A0H)) {
                C16630pM r2 = this.A08;
                if (!r2.A03()) {
                    return;
                }
                if (iArr[0] >= r7.A02(AbstractC15460nI.A1F) || iArr[1] >= r7.A02(AbstractC15460nI.A1E)) {
                    Context context = r2.A00;
                    File file2 = new File(context.getFilesDir().getParent(), "shared_prefs");
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append("ab-props-backup");
                    sb4.append(".xml");
                    File file3 = new File(file2, sb4.toString());
                    File file4 = new File(context.getFilesDir().getParent(), "shared_prefs");
                    StringBuilder sb5 = new StringBuilder();
                    sb5.append("ab-props");
                    sb5.append(".xml");
                    File file5 = new File(file4, sb5.toString());
                    if (file3.exists()) {
                        file3.renameTo(file5);
                    }
                    AbstractC15710nm r22 = this.A01;
                    StringBuilder sb6 = new StringBuilder("Crashes count foreground:");
                    sb6.append(iArr[0]);
                    sb6.append(" Crashes count background: ");
                    sb6.append(iArr[1]);
                    r22.AaV("RevertToPreviousStableABPropsCopy", sb6.toString(), false);
                }
            }
        }
    }

    public void A03(AbstractC18730sv r1) {
        this.A09 = r1;
    }

    public void A04(String str) {
        SharedPreferences.Editor edit = this.A06.A00.edit();
        edit.putString("crash_state_manager:system_exit", str);
        if (!edit.commit()) {
            Log.w("reportSystemExit/failed-to-save-preferences");
        }
        System.exit(0);
    }

    public void A05(Throwable th) {
        int currentTimeMillis;
        String str;
        File[] listFiles;
        C28541Nz r8 = new C28541Nz(this.A05.A00, this.A09, this.A02, this.A04, this.A07);
        StringBuilder sb = new StringBuilder("OOM/WhatsAppWorkers state: ");
        sb.append(AnonymousClass168.A04.toString());
        Log.i(sb.toString());
        AbstractC18730sv r1 = r8.A01;
        if (r1 != null) {
            r1.AKm("OOM");
        }
        if (r8.A02.A03()) {
            currentTimeMillis = 120;
        } else {
            currentTimeMillis = (int) ((System.currentTimeMillis() - 1659716253000L) / 86400000);
        }
        if (currentTimeMillis > r8.A00) {
            str = "OOMHandler/hprof dump not allowed";
        } else {
            AnonymousClass1O0 r5 = r8.A04;
            long j = AnonymousClass01V.A00;
            Context context = r5.A00;
            StatFs statFs = new StatFs(context.getCacheDir().getPath());
            if (((long) statFs.getBlockSize()) * ((long) statFs.getAvailableBlocks()) <= j * 3 || !(!AnonymousClass1O1.A00()) || (((listFiles = new File(context.getCacheDir().getPath()).listFiles(new AnonymousClass1O2(r5))) != null && listFiles.length > 0) || C003501n.A0B(r8.A03))) {
                str = "OOMHandler/hprof dump conditions not met";
            } else {
                try {
                    Debug.dumpHprofData(String.format(Locale.US, "%s/dump.hprof", context.getCacheDir().getPath()));
                    Log.i("OOMHandler/dump successful");
                    return;
                } catch (IOException unused) {
                    Log.w("OOMHandler/IOException trying to write dump", th);
                    return;
                }
            }
        }
        Log.i(str);
    }
}
