package X;

import android.content.ContentValues;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.text.TextUtils;
import com.facebook.redex.RunnableBRunnable0Shape10S0200000_I1;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.2lB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56542lB extends C15050mT implements AbstractC116345Vb {
    public static DecimalFormat A03;
    public final Uri A00;
    public final C14160kx A01;
    public final String A02;

    @Override // X.AbstractC116345Vb
    public final Uri Ah3() {
        return this.A00;
    }

    public C56542lB(C14160kx r3, String str) {
        super(r3);
        C13020j0.A05(str);
        this.A01 = r3;
        this.A02 = str;
        C13020j0.A05(str);
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("uri");
        builder.authority("google-analytics.com");
        builder.path(str);
        this.A00 = builder.build();
    }

    public static String A00(int i, String str) {
        if (i > 0) {
            return C12960it.A0e(str, C12980iv.A0t(str.length() + 11), i);
        }
        AnonymousClass3A6.A00("index out of range for prefix", str);
        return "";
    }

    /* JADX WARNING: Removed duplicated region for block: B:122:0x005d A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x001f A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.Map A01(X.AnonymousClass3IT r11) {
        /*
        // Method dump skipped, instructions count: 867
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C56542lB.A01(X.3IT):java.util.Map");
    }

    public static void A02(String str, String str2, Map map) {
        if (!TextUtils.isEmpty(str2)) {
            map.put(str, str2);
        }
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:78:0x013c */
    /* JADX DEBUG: Multi-variable search result rejected for r7v1, resolved type: X.0mR */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r7v0, types: [java.lang.Object, java.lang.String] */
    /* JADX WARN: Type inference failed for: r7v2, types: [X.0mR, X.0mT, X.0mS] */
    @Override // X.AbstractC116345Vb
    public final void AhM(AnonymousClass3IT r26) {
        long j;
        C56582lF r5;
        Map A01;
        String str;
        C13020j0.A01(r26);
        C13020j0.A03("Can't deliver not submitted measurement", r26.A05);
        C13020j0.A06("deliver should be called on worker thread");
        AnonymousClass3IT r1 = new AnonymousClass3IT(r26);
        C56232kY r52 = (C56232kY) r1.A01(C56232kY.class);
        if (TextUtils.isEmpty(r52.A00)) {
            r5 = super.A00.A0C;
            C14160kx.A01(r5);
            A01 = A01(r1);
            str = "Ignoring measurement without type";
        } else if (TextUtils.isEmpty(r52.A01)) {
            r5 = super.A00.A0C;
            C14160kx.A01(r5);
            A01 = A01(r1);
            str = "Ignoring measurement without client id";
        } else {
            C77403nE r8 = this.A01.A02;
            C13020j0.A01(r8);
            C13020j0.A03("Analytics instance not initialized", C12960it.A1S(r8.A01 ? 1 : 0));
            Map A012 = A01(r1);
            A012.put("v", "1");
            A012.put("_v", AnonymousClass4H2.A01);
            C15030mR r7 = this.A02;
            A012.put("tid", r7);
            C13020j0.A03("Analytics instance not initialized", C12960it.A1S(r8.A01 ? 1 : 0));
            if (r8.A02) {
                StringBuilder A0h = C12960it.A0h();
                Iterator A0n = C12960it.A0n(A012);
                while (A0n.hasNext()) {
                    Map.Entry A15 = C12970iu.A15(A0n);
                    if (A0h.length() != 0) {
                        A0h.append(", ");
                    }
                    A0h.append(C12990iw.A0r(A15));
                    A0h.append("=");
                    A0h.append((String) A15.getValue());
                }
                C15050mT.A06(this, A0h.toString(), null, null, "Dry run is enabled. GoogleAnalytics would have sent", 4);
                return;
            }
            HashMap A11 = C12970iu.A11();
            C64933Hm.A02("uid", r52.A02, A11);
            C56252ka r82 = (C56252ka) ((AbstractC64703Go) r26.A0A.get(C56252ka.class));
            if (r82 != null) {
                C64933Hm.A02("an", r82.A00, A11);
                C64933Hm.A02("aid", r82.A02, A11);
                C64933Hm.A02("av", r82.A01, A11);
                C64933Hm.A02("aiid", r82.A03, A11);
            }
            try {
                AnonymousClass3BQ r53 = new AnonymousClass3BQ(r52.A01, r7, A11, 0, !TextUtils.isEmpty(r52.A03));
                C14160kx r15 = super.A00;
                C56572lE r4 = r15.A06;
                C14160kx.A01(r4);
                r4.A0G();
                C14170ky.A00();
                r7 = r4.A00;
                r7.A0G();
                C14170ky.A00();
                try {
                    C15120mb r10 = r7.A04;
                    r10.A0G();
                    r10.A0I().beginTransaction();
                    String str2 = r53.A01;
                    C13020j0.A05(str2);
                    r10.A0G();
                    C14170ky.A00();
                    int delete = r10.A0I().delete("properties", "app_uid=? AND cid<>?", new String[]{"0", str2});
                    if (delete > 0) {
                        r10.A0D("Deleted property records", Integer.valueOf(delete));
                    }
                    String str3 = r53.A02;
                    C13020j0.A05(str2);
                    C13020j0.A05(str3);
                    r10.A0G();
                    C14170ky.A00();
                    String[] A1b = C12990iw.A1b("0", str2, 3);
                    A1b[2] = str3;
                    j = C15120mb.A00(r10, "SELECT hits_count FROM properties WHERE app_uid=? AND cid=? AND tid=?", A1b);
                    r53.A00 = 1 + j;
                    r10.A0G();
                    C14170ky.A00();
                    SQLiteDatabase A0I = r10.A0I();
                    Map map = r53.A03;
                    C13020j0.A01(map);
                    Uri.Builder builder = new Uri.Builder();
                    Iterator A0n2 = C12960it.A0n(map);
                    while (A0n2.hasNext()) {
                        Map.Entry A152 = C12970iu.A15(A0n2);
                        builder.appendQueryParameter(C12990iw.A0r(A152), (String) A152.getValue());
                    }
                    String encodedQuery = builder.build().getEncodedQuery();
                    if (encodedQuery == null) {
                        encodedQuery = "";
                    }
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("app_uid", (Long) 0L);
                    contentValues.put("cid", str2);
                    contentValues.put("tid", str3);
                    contentValues.put("adid", Integer.valueOf(r53.A04 ? 1 : 0));
                    contentValues.put("hits_count", Long.valueOf(r53.A00));
                    contentValues.put("params", encodedQuery);
                    try {
                        if (A0I.insertWithOnConflict("properties", null, contentValues, 5) == -1) {
                            r10.A08("Failed to insert/update a property (got -1)");
                        }
                    } catch (SQLiteException e) {
                        r10.A0C("Error storing a property", e);
                    }
                    r10.A0G();
                    r10.A0I().setTransactionSuccessful();
                    try {
                        r10.A0G();
                        r10.A0I().endTransaction();
                    } catch (SQLiteException e2) {
                        r7.A0C("Failed to end transaction", e2);
                    }
                    if (j == 0) {
                        C14170ky.A00();
                        r7.A0B("Sending first hit to property", str3);
                        C14160kx r83 = r7.A00;
                        C15070mW r9 = r83.A0D;
                        C14160kx.A01(r9);
                        if (!new C93924ay(((C15050mT) r9).A00.A04, r9.A0H()).A00(C12980iv.A0G(C88904Hw.A06.A00()))) {
                            C14160kx.A01(r9);
                            C14170ky.A00();
                            r9.A0G();
                            SharedPreferences sharedPreferences = r9.A01;
                            String str4 = null;
                            String string = sharedPreferences.getString("installation_campaign", null);
                            if (!TextUtils.isEmpty(string)) {
                                str4 = string;
                            }
                            if (!TextUtils.isEmpty(str4)) {
                                C56582lF r0 = r83.A0C;
                                C14160kx.A01(r0);
                                C56242kZ A00 = C64933Hm.A00(r0, str4);
                                r7.A0B("Found relevant installation campaign", A00);
                                C15030mR.A03(A00, r53, r7);
                            }
                        }
                    }
                } catch (SQLiteException e3) {
                    r7.A0C("Failed to update Analytics property", e3);
                    try {
                        C15120mb r02 = r7.A04;
                        r02.A0G();
                        r02.A0I().endTransaction();
                    } catch (SQLiteException e4) {
                        r7.A0C("Failed to end transaction", e4);
                    }
                    j = -1;
                }
                A012.put("_s", String.valueOf(j));
                C56582lF r2 = r15.A0C;
                C14160kx.A01(r2);
                AnonymousClass3H2 r14 = new AnonymousClass3H2(r2, null, A012, 0, r26.A00, 0, true);
                C14160kx.A01(r4);
                r4.A0G();
                r4.A0B("Hit delivery requested", r14);
                C14170ky r22 = ((C15050mT) r4).A00.A03;
                C13020j0.A01(r22);
                r22.A03.submit(new RunnableBRunnable0Shape10S0200000_I1(r14, 15, r4));
                return;
            } catch (Throwable th) {
                try {
                    C15120mb r03 = r7.A04;
                    r03.A0G();
                    r03.A0I().endTransaction();
                    throw th;
                } catch (SQLiteException e5) {
                    r7.A0C("Failed to end transaction", e5);
                    throw th;
                }
            }
        }
        StringBuilder A0h2 = C12960it.A0h();
        Iterator A0n3 = C12960it.A0n(A01);
        while (A0n3.hasNext()) {
            Map.Entry A153 = C12970iu.A15(A0n3);
            if (A0h2.length() > 0) {
                A0h2.append(',');
            }
            A0h2.append(C12990iw.A0r(A153));
            A0h2.append('=');
            A0h2.append((String) A153.getValue());
        }
        r5.A0E(C12960it.A0c(str, "Discarding hit. "), A0h2.toString());
    }
}
