package X;

import android.view.ViewTreeObserver;

/* renamed from: X.4oN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnPreDrawListenerC101964oN implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ AnonymousClass2TW A00;
    public final /* synthetic */ AnonymousClass2T3 A01;

    public ViewTreeObserver$OnPreDrawListenerC101964oN(AnonymousClass2TW r1, AnonymousClass2T3 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        C12980iv.A1G(this.A01, this);
        this.A00.A05.A0C().A0d();
        return true;
    }
}
