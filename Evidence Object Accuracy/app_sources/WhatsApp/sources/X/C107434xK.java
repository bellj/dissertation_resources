package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4xK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107434xK implements AnonymousClass5YX {
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(7);
    public final String A00;
    public final String A01;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C107434xK(Parcel parcel) {
        this.A00 = parcel.readString();
        this.A01 = parcel.readString();
    }

    public C107434xK(String str, String str2) {
        this.A00 = str;
        this.A01 = str2;
    }

    @Override // X.AnonymousClass5YX
    public /* synthetic */ byte[] AHp() {
        return null;
    }

    @Override // X.AnonymousClass5YX
    public /* synthetic */ C100614mC AHq() {
        return null;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C107434xK.class != obj.getClass()) {
                return false;
            }
            C107434xK r5 = (C107434xK) obj;
            if (!this.A00.equals(r5.A00) || !this.A01.equals(r5.A01)) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return C72453ed.A05(this.A00.hashCode()) + this.A01.hashCode();
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("VC: ");
        A0k.append(this.A00);
        A0k.append("=");
        return C12960it.A0d(this.A01, A0k);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A00);
        parcel.writeString(this.A01);
    }
}
