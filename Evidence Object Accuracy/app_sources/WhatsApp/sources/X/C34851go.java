package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.1go  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C34851go extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C34851go A02;
    public static volatile AnonymousClass255 A03;
    public int A00;
    public AbstractC27881Jp A01 = AbstractC27881Jp.A01;

    static {
        C34851go r0 = new C34851go();
        A02 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r5, Object obj, Object obj2) {
        switch (r5.ordinal()) {
            case 0:
                return A02;
            case 1:
                AbstractC462925h r6 = (AbstractC462925h) obj;
                C34851go r7 = (C34851go) obj2;
                boolean z = true;
                if ((this.A00 & 1) != 1) {
                    z = false;
                }
                AbstractC27881Jp r2 = this.A01;
                boolean z2 = true;
                if ((r7.A00 & 1) != 1) {
                    z2 = false;
                }
                this.A01 = r6.Afm(r2, r7.A01, z, z2);
                if (r6 == C463025i.A00) {
                    this.A00 |= r7.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r62 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        int A032 = r62.A03();
                        if (A032 == 0) {
                            break;
                        } else if (A032 == 10) {
                            this.A00 |= 1;
                            this.A01 = r62.A08();
                        } else if (!A0a(r62, A032)) {
                            break;
                        }
                    } catch (C28971Pt e) {
                        e.unfinishedMessage = this;
                        throw new RuntimeException(e);
                    } catch (IOException e2) {
                        C28971Pt r1 = new C28971Pt(e2.getMessage());
                        r1.unfinishedMessage = this;
                        throw new RuntimeException(r1);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C34851go();
            case 5:
                return new C81733uU();
            case 6:
                break;
            case 7:
                if (A03 == null) {
                    synchronized (C34851go.class) {
                        if (A03 == null) {
                            A03 = new AnonymousClass255(A02);
                        }
                    }
                }
                return A03;
            default:
                throw new UnsupportedOperationException();
        }
        return A02;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A09(this.A01, 1);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0K(this.A01, 1);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
