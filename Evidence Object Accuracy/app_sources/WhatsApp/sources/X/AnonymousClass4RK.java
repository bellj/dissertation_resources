package X;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.4RK  reason: invalid class name */
/* loaded from: classes3.dex */
public final /* synthetic */ class AnonymousClass4RK {
    public final /* synthetic */ AnonymousClass1DB A00;
    public final /* synthetic */ List A01;
    public final /* synthetic */ AtomicBoolean A02;
    public final /* synthetic */ boolean A03;

    public /* synthetic */ AnonymousClass4RK(AnonymousClass1DB r1, List list, AtomicBoolean atomicBoolean, boolean z) {
        this.A00 = r1;
        this.A01 = list;
        this.A03 = z;
        this.A02 = atomicBoolean;
    }
}
