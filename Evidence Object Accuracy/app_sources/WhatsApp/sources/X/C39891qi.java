package X;

import android.graphics.Bitmap;
import android.util.Pair;
import com.whatsapp.stickers.WebpUtils;
import com.whatsapp.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/* renamed from: X.1qi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C39891qi {
    public final AbstractC15710nm A00;
    public final C22590zK A01;
    public final C26511Dt A02;
    public final C26521Du A03;

    public C39891qi(AbstractC15710nm r1, C22590zK r2, C26511Dt r3, C26521Du r4) {
        this.A00 = r1;
        this.A02 = r3;
        this.A03 = r4;
        this.A01 = r2;
    }

    public C39901qj A00(C39881qh r8) {
        byte[] bArr;
        IOException e;
        byte[] A09;
        Pair pair;
        byte[] A03;
        int i;
        int i2;
        byte[] bArr2;
        Pair pair2;
        C14370lK r1 = r8.A00;
        if (AnonymousClass14P.A01(r1)) {
            if (!r8.A03) {
                bArr2 = this.A03.A04(r8.A01);
            } else {
                bArr2 = null;
            }
            File file = r8.A01;
            Pair A0E = C22200yh.A0E(file);
            if (((Integer) A0E.second).intValue() > ((Integer) A0E.first).intValue()) {
                pair2 = C63233Au.A00(file);
                return new C39901qj(A0E, pair2, bArr2, null);
            }
            pair2 = null;
            return new C39901qj(A0E, pair2, bArr2, null);
        } else if (r1 == C14370lK.A09 || r1 == C14370lK.A0C || r1 == C14370lK.A0Y || r1 == C14370lK.A0A || r1 == C14370lK.A0H) {
            try {
                FileInputStream fileInputStream = new FileInputStream(r8.A01);
                C03590Ij r3 = new C03590Ij(fileInputStream.available());
                byte[] bArr3 = new byte[4096];
                while (true) {
                    int read = fileInputStream.read(bArr3);
                    if (read == -1) {
                        break;
                    }
                    r3.write(bArr3, 0, read);
                }
                bArr = r3.toByteArray();
                try {
                    fileInputStream.close();
                } catch (IOException e2) {
                    e = e2;
                    Log.e("ThumbnailGenerator/createThumbnailForThumbnailDownload/failed", e);
                    return new C39901qj(null, null, bArr, null);
                }
            } catch (IOException e3) {
                e = e3;
                bArr = null;
            }
            return new C39901qj(null, null, bArr, null);
        } else if (r1 == C14370lK.A08 || r1 == C14370lK.A0T) {
            if (r8.A03) {
                A09 = null;
            } else {
                A09 = this.A02.A09(r8.A01, r8.A02);
            }
            return new C39901qj(null, null, A09, null);
        } else if (AnonymousClass14P.A02(r1)) {
            try {
                C38991p4 r32 = new C38991p4(r8.A01);
                if (r32.A02()) {
                    i = r32.A01;
                } else {
                    i = r32.A03;
                }
                if (r32.A02()) {
                    i2 = r32.A03;
                } else {
                    i2 = r32.A01;
                }
                pair = Pair.create(Integer.valueOf(i), Integer.valueOf(i2));
            } catch (AnonymousClass47W e4) {
                Log.w("thumbnailgenerator/video/unable to get video meta", e4);
                pair = null;
            }
            if (r8.A03) {
                A03 = null;
            } else {
                A03 = C26521Du.A03(C26521Du.A01(r8.A01));
            }
            return new C39901qj(pair, null, A03, null);
        } else if (r1 != C14370lK.A0S) {
            return null;
        } else {
            if (r8.A03) {
                return new C39901qj(null, null, null, WebpUtils.fetchWebpMetadata(r8.A01.getAbsolutePath()));
            }
            try {
                C22590zK r2 = this.A01;
                File file2 = r8.A01;
                Bitmap A032 = r2.A03(file2, file2.getName(), 64, 64);
                if (A032 != null) {
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    A032.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    A032.recycle();
                    return new C39901qj(new Pair(64, 64), null, byteArray, null);
                }
            } catch (OutOfMemoryError e5) {
                Log.e("ThumbnailGenerator/createThumbnailForSticker/failed", e5);
            }
            return new C39901qj(null, null, null, null);
        }
    }
}
