package X;

import android.os.IBinder;
import android.os.IInterface;

/* renamed from: X.3px  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79043px extends C98384ib implements IInterface {
    public C79043px(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.service.IClientTelemetryService");
    }
}
