package X;

import android.content.SharedPreferences;
import com.facebook.redex.RunnableBRunnable0Shape0S1100000_I0;
import com.facebook.redex.RunnableBRunnable0Shape6S0200000_I0_6;
import com.facebook.redex.RunnableBRunnable0Shape9S0100000_I0_9;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1Cu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26301Cu implements AbstractC16990q5 {
    public final C25841Ba A00;
    public final C20370ve A01;
    public final C14850m9 A02;
    public final C22710zW A03;
    public final C17070qD A04;
    public final C22460z7 A05;
    public final C26291Ct A06;
    public final C26281Cs A07;
    public final AnonymousClass1A8 A08;
    public final AbstractC14440lR A09;

    public C26301Cu(C25841Ba r1, C20370ve r2, C14850m9 r3, C22710zW r4, C17070qD r5, C22460z7 r6, C26291Ct r7, C26281Cs r8, AnonymousClass1A8 r9, AbstractC14440lR r10) {
        this.A01 = r2;
        this.A05 = r6;
        this.A06 = r7;
        this.A07 = r8;
        this.A03 = r4;
        this.A04 = r5;
        this.A08 = r9;
        this.A00 = r1;
        this.A02 = r3;
        this.A09 = r10;
    }

    @Override // X.AbstractC16990q5
    public void AOo() {
        C456522m A00;
        C22710zW r6 = this.A03;
        if (r6.A07()) {
            List A0b = this.A01.A0b(new Integer[]{20, 401, 417, 418}, new Integer[]{40}, -1);
            if (A0b.size() > 0) {
                C26291Ct r2 = this.A06;
                C456522m A002 = r2.A00();
                this.A04.A02().AZN(A002, A0b);
                r2.A01(A002);
            }
        }
        C26281Cs r5 = this.A07;
        synchronized (r5) {
            C26291Ct r3 = r5.A01;
            long A003 = r3.A01.A00();
            long j = A003 - (A003 % 86400000);
            ArrayList arrayList = new ArrayList();
            SharedPreferences sharedPreferences = r3.A00;
            if (sharedPreferences == null) {
                sharedPreferences = r3.A02.A01("payment_daily_usage_preferences");
                r3.A00 = sharedPreferences;
            }
            Map<String, ?> all = sharedPreferences.getAll();
            SharedPreferences sharedPreferences2 = r3.A00;
            if (sharedPreferences2 == null) {
                sharedPreferences2 = r3.A02.A01("payment_daily_usage_preferences");
                r3.A00 = sharedPreferences2;
            }
            SharedPreferences.Editor edit = sharedPreferences2.edit();
            for (Map.Entry<String, ?> entry : all.entrySet()) {
                String obj = entry.getValue().toString();
                if (!obj.isEmpty() && (A00 = C456522m.A00(obj)) != null && A00.A0F < j) {
                    arrayList.add(A00);
                    edit.remove(entry.getKey());
                }
            }
            edit.apply();
            arrayList.size();
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                C456522m r4 = (C456522m) it.next();
                C16120oU r32 = r5.A00;
                C856943u r22 = new C856943u();
                r22.A05 = Long.valueOf(r4.A0G);
                r22.A06 = Long.valueOf(r4.A02);
                r22.A09 = Long.valueOf(r4.A05);
                r22.A07 = Long.valueOf(r4.A03);
                r22.A08 = Long.valueOf(r4.A04);
                r22.A0F = Long.valueOf(r4.A0B);
                r22.A0E = Long.valueOf(r4.A0A);
                r22.A0C = Long.valueOf(r4.A08);
                r22.A0B = Long.valueOf(r4.A07);
                r22.A0D = Long.valueOf(r4.A09);
                r22.A0A = Long.valueOf(r4.A06);
                r22.A04 = Long.valueOf(r4.A01);
                r22.A00 = Long.valueOf((long) r4.A0C.size());
                r22.A01 = Long.valueOf((long) r4.A0E.size());
                r22.A03 = Long.valueOf(r4.A00);
                r22.A02 = Long.valueOf((long) r4.A0D.size());
                r32.A07(r22);
            }
        }
        if (r6.A07() && this.A02.A07(991)) {
            C25841Ba r62 = this.A00;
            int nextInt = new Random().nextInt(C25841Ba.A0C);
            String.format(Locale.US, "BloksAssetManager/triggerBackgroundFetchWithJitter triggering bloks fetch in %d ms", Integer.valueOf(nextInt));
            ((AbstractC18830t7) r62).A06.AbK(new RunnableBRunnable0Shape0S1100000_I0(r62), "BloksAssetmanager/trigger-bg-fetch", (long) nextInt);
        }
        C14850m9 r1 = this.A02;
        if (r1.A07(629) || r1.A07(605)) {
            C22460z7 r42 = this.A05;
            C14820m6 r33 = r42.A02;
            if (r33.A00.getBoolean("payment_background_batch_require_fetch", false) && r33.A1J("payment_backgrounds_batch_last_fetch_timestamp", TimeUnit.DAYS.toMillis(7))) {
                r42.A01.A0H(new RunnableBRunnable0Shape6S0200000_I0_6(r42, 41, r42.A07.A00()));
            }
        }
    }

    @Override // X.AbstractC16990q5
    public void AOp() {
        this.A09.Ab2(new RunnableBRunnable0Shape9S0100000_I0_9(this, 14));
    }
}
