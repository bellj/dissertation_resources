package X;

import java.security.SecureRandom;
import java.util.Arrays;

/* renamed from: X.5G5  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5G5 implements AnonymousClass5X5 {
    public SecureRandom A00;
    public C112955Fl A01;
    public C113075Fx A02;
    public boolean A03;

    public AnonymousClass5G5(AnonymousClass5XE r2) {
        this.A01 = new C112955Fl(r2);
    }

    @Override // X.AnonymousClass5X5
    public String AAf() {
        StringBuilder A0h = C12960it.A0h();
        C72463ee.A0U(A0h, this.A01.A01);
        return C12960it.A0d("/RFC3211Wrap", A0h);
    }

    @Override // X.AnonymousClass5X5
    public void AIf(AnonymousClass20L r4, boolean z) {
        this.A03 = z;
        if (r4 instanceof C113025Fs) {
            C113025Fs r42 = (C113025Fs) r4;
            this.A00 = r42.A00;
            AnonymousClass20L r1 = r42.A01;
            if (r1 instanceof C113075Fx) {
                this.A02 = (C113075Fx) r1;
                return;
            }
            throw C12970iu.A0f("RFC3211Wrap requires an IV");
        }
        if (z) {
            this.A00 = C95234dM.A00();
        }
        if (r4 instanceof C113075Fx) {
            this.A02 = (C113075Fx) r4;
            return;
        }
        throw C12970iu.A0f("RFC3211Wrap requires an IV");
    }

    @Override // X.AnonymousClass5X5
    public byte[] AfE(byte[] bArr, int i, int i2) {
        int i3;
        if (!this.A03) {
            C112955Fl r3 = this.A01;
            int AAt = r3.A01.AAt();
            if (i2 >= (AAt << 1)) {
                byte[] bArr2 = new byte[i2];
                byte[] bArr3 = new byte[AAt];
                boolean z = false;
                System.arraycopy(bArr, 0, bArr2, 0, i2);
                System.arraycopy(bArr, 0, bArr3, 0, AAt);
                r3.AIf(new C113075Fx(this.A02.A00, bArr3), false);
                for (int i4 = AAt; i4 < i2; i4 += AAt) {
                    r3.AZY(bArr2, bArr2, i4, i4);
                }
                System.arraycopy(bArr2, i2 - AAt, bArr3, 0, AAt);
                r3.AIf(new C113075Fx(this.A02.A00, bArr3), false);
                r3.AZY(bArr2, bArr2, 0, 0);
                r3.AIf(this.A02, false);
                for (int i5 = 0; i5 < i2; i5 += AAt) {
                    r3.AZY(bArr2, bArr2, i5, i5);
                }
                int i6 = bArr2[0] & 255;
                int i7 = i2 - 4;
                boolean z2 = true;
                if (i6 <= i7) {
                    z2 = false;
                    i7 = i6;
                }
                byte[] bArr4 = new byte[i7];
                System.arraycopy(bArr2, 4, bArr4, 0, i7);
                int i8 = 0;
                int i9 = 0;
                do {
                    i3 = i8 + 1;
                    i9 |= bArr2[i8 + 4] ^ ((byte) (bArr2[i3] ^ -1));
                    i8 = i3;
                } while (i3 != 3);
                Arrays.fill(bArr2, (byte) 0);
                if (i9 != 0) {
                    z = true;
                }
                if (!z2 && !z) {
                    return bArr4;
                }
                throw new C114965Nt("wrapped key corrupted");
            }
            throw new C114965Nt("input too short");
        }
        throw C12960it.A0U("not set for unwrapping");
    }

    @Override // X.AnonymousClass5X5
    public byte[] Ag9(byte[] bArr, int i, int i2) {
        if (!this.A03) {
            throw C12960it.A0U("not set for wrapping");
        } else if (i2 > 255 || i2 < 0) {
            throw C12970iu.A0f("input must be from 0 to 255 bytes");
        } else {
            C112955Fl r4 = this.A01;
            r4.AIf(this.A02, true);
            int AAt = r4.A01.AAt();
            int i3 = i2 + 4;
            int i4 = AAt << 1;
            if (i3 >= i4) {
                i4 = i3 % AAt == 0 ? i3 : ((i3 / AAt) + 1) * AAt;
            }
            byte[] bArr2 = new byte[i4];
            bArr2[0] = (byte) i2;
            System.arraycopy(bArr, 0, bArr2, 4, i2);
            int i5 = i4 - i3;
            byte[] bArr3 = new byte[i5];
            this.A00.nextBytes(bArr3);
            System.arraycopy(bArr3, 0, bArr2, i3, i5);
            bArr2[1] = (byte) (bArr2[4] ^ -1);
            bArr2[2] = (byte) (bArr2[5] ^ -1);
            bArr2[3] = (byte) (bArr2[6] ^ -1);
            for (int i6 = 0; i6 < i4; i6 += AAt) {
                r4.AZY(bArr2, bArr2, i6, i6);
            }
            for (int i7 = 0; i7 < i4; i7 += AAt) {
                r4.AZY(bArr2, bArr2, i7, i7);
            }
            return bArr2;
        }
    }
}
