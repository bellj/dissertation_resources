package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4jP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98884jP implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new C78053oI[i];
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        String str = null;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c != 1) {
                str = C95664e9.A09(parcel, str, c, 2, readInt);
            } else {
                C95664e9.A0F(parcel, readInt, 4);
                parcel.readInt();
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78053oI(str);
    }
}
