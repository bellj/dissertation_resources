package X;

import android.app.Activity;
import android.graphics.Rect;
import androidx.window.extensions.layout.FoldingFeature;
import androidx.window.extensions.layout.WindowLayoutInfo;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0S4  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0S4 {
    public static final AnonymousClass0PZ A00(Activity activity, WindowLayoutInfo windowLayoutInfo) {
        C16700pc.A0E(activity, 0);
        List<FoldingFeature> displayFeatures = windowLayoutInfo.getDisplayFeatures();
        C16700pc.A0B(displayFeatures);
        ArrayList arrayList = new ArrayList();
        for (FoldingFeature foldingFeature : displayFeatures) {
            if (foldingFeature instanceof FoldingFeature) {
                AnonymousClass0S4 r0 = new AnonymousClass0S4();
                C16700pc.A0B(foldingFeature);
                AbstractC12840ie A01 = r0.A01(activity, foldingFeature);
                if (A01 != null) {
                    arrayList.add(A01);
                }
            }
        }
        return new AnonymousClass0PZ(arrayList);
    }

    public final AbstractC12840ie A01(Activity activity, FoldingFeature foldingFeature) {
        AnonymousClass0SA r3;
        AnonymousClass0S9 r2;
        C16700pc.A0E(foldingFeature, 1);
        int type = foldingFeature.getType();
        if (type != 1) {
            if (type == 2) {
                r3 = AnonymousClass0SA.A02;
            }
            return null;
        }
        r3 = AnonymousClass0SA.A01;
        int state = foldingFeature.getState();
        if (state != 1) {
            if (state == 2) {
                r2 = AnonymousClass0S9.A02;
            }
            return null;
        }
        r2 = AnonymousClass0S9.A01;
        Rect bounds = foldingFeature.getBounds();
        C16700pc.A0B(bounds);
        C05400Pk r8 = new C05400Pk(bounds);
        C05400Pk r0 = C07560Ze.A00.A05(activity).A00;
        Rect rect = new Rect(r0.A01, r0.A03, r0.A02, r0.A00);
        int i = r8.A00 - r8.A03;
        if (!(i == 0 && r8.A02 - r8.A01 == 0)) {
            int i2 = r8.A02 - r8.A01;
            if (i2 != rect.width() && i != rect.height()) {
                return null;
            }
            if (i2 < rect.width() && i < rect.height()) {
                return null;
            }
            if (i2 == rect.width() && i == rect.height()) {
                return null;
            }
            Rect bounds2 = foldingFeature.getBounds();
            C16700pc.A0B(bounds2);
            return new AnonymousClass0ZZ(new C05400Pk(bounds2), r2, r3);
        }
        return null;
    }
}
