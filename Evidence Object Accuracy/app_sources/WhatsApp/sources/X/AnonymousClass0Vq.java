package X;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import androidx.appcompat.widget.SearchView;

/* renamed from: X.0Vq  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Vq implements TextWatcher {
    public final /* synthetic */ SearchView A00;

    @Override // android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
    }

    @Override // android.text.TextWatcher
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public AnonymousClass0Vq(SearchView searchView) {
        this.A00 = searchView;
    }

    @Override // android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        SearchView searchView = this.A00;
        Editable text = searchView.A0k.getText();
        searchView.A0H = text;
        boolean z = !TextUtils.isEmpty(text);
        searchView.A0H(z);
        boolean z2 = !z;
        int i4 = 8;
        if (searchView.A0P && !searchView.A0J() && z2) {
            searchView.A0g.setVisibility(8);
            i4 = 0;
        }
        searchView.A0i.setVisibility(i4);
        searchView.A0A();
        searchView.A0C();
        if (searchView.A0B != null && !TextUtils.equals(charSequence, searchView.A0F)) {
            searchView.A0B.AUX(charSequence.toString());
        }
        searchView.A0F = charSequence.toString();
    }
}
