package X;

import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.mediacomposer.doodle.shapepicker.ShapePickerRecyclerView;

/* renamed from: X.3Fm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC64423Fm {
    public boolean A00;
    public boolean A01;
    public boolean A02;
    public final LinearLayoutManager A03;
    public final AbstractC05270Ox A04;
    public final AbstractC05520Pw A05;
    public final RecyclerView A06;
    public final C89554Kk A07;
    public final ShapePickerRecyclerView A08;
    public final C54452gk A09;

    public AbstractC64423Fm(RecyclerView recyclerView, C89554Kk r6, ShapePickerRecyclerView shapePickerRecyclerView, boolean z) {
        C75103jJ r1 = new C75103jJ(this);
        this.A04 = r1;
        C54452gk r3 = new C54452gk(this);
        this.A09 = r3;
        r3.A07(z);
        recyclerView.setItemAnimator(null);
        this.A08 = shapePickerRecyclerView;
        this.A07 = r6;
        shapePickerRecyclerView.A0n(r1);
        LinearLayoutManager A0Q = C12990iw.A0Q(recyclerView);
        this.A03 = A0Q;
        this.A05 = new C74763ik(recyclerView.getContext(), this);
        this.A06 = recyclerView;
        recyclerView.setAdapter(r3);
        recyclerView.setLayoutManager(A0Q);
        recyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() { // from class: X.4mu
            @Override // android.view.View.OnLayoutChangeListener
            public final void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                AbstractC64423Fm r0 = AbstractC64423Fm.this;
                int i9 = i3 - i;
                if (i7 - i5 != i9) {
                    C54452gk r2 = r0.A09;
                    AbstractC64423Fm r12 = r2.A03;
                    r2.A01 = r12.A00(i9, r12.A00);
                }
            }
        });
    }

    public int A00(int i, boolean z) {
        if (!(this instanceof AnonymousClass33Q)) {
            return AnonymousClass33R.A01.length;
        }
        Resources A09 = C12960it.A09(this.A06);
        int i2 = R.dimen.shape_picker_sticker_subcategory_item_portrait_width;
        if (z) {
            i2 = R.dimen.shape_picker_sticker_subcategory_item_landscape_width;
        }
        return i / A09.getDimensionPixelSize(i2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0013, code lost:
        if (r4.A09.A02.size() <= 0) goto L_0x0015;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A01() {
        /*
            r4 = this;
            androidx.recyclerview.widget.RecyclerView r3 = r4.A06
            int r2 = r3.getVisibility()
            boolean r0 = r4.A01
            if (r0 == 0) goto L_0x0015
            X.2gk r0 = r4.A09
            java.util.List r0 = r0.A02
            int r1 = r0.size()
            r0 = 0
            if (r1 > 0) goto L_0x0017
        L_0x0015:
            r0 = 8
        L_0x0017:
            r3.setVisibility(r0)
            if (r2 == r0) goto L_0x0037
            X.4Kk r1 = r4.A07
            int r0 = r3.getVisibility()
            boolean r2 = X.C12960it.A1T(r0)
            X.1KW r0 = r1.A00
            com.whatsapp.mediacomposer.doodle.shapepicker.ShapePickerRecyclerView r1 = r0.A0P
            X.1jQ r0 = r0.A0Y
            java.lang.Object r0 = r0.A01()
            boolean r0 = X.C12970iu.A1Y(r0)
            r1.A11(r0, r2)
        L_0x0037:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC64423Fm.A01():void");
    }

    public void A02(C75453js r5, boolean z) {
        View view = r5.A00;
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        Resources A09 = C12960it.A09(this.A06);
        int i = R.dimen.shape_picker_subcategory_selected_portrait_dimen;
        if (z) {
            i = R.dimen.shape_picker_subcategory_selected_landscape_dimen;
        }
        int dimensionPixelSize = A09.getDimensionPixelSize(i);
        layoutParams.width = dimensionPixelSize;
        layoutParams.height = dimensionPixelSize;
        view.setLayoutParams(layoutParams);
    }

    public void A03(boolean z) {
        RecyclerView recyclerView = this.A06;
        ViewGroup.LayoutParams layoutParams = recyclerView.getLayoutParams();
        Resources A09 = C12960it.A09(recyclerView);
        int i = R.dimen.shape_picker_subcategories_portrait_height;
        if (z) {
            i = R.dimen.shape_picker_subcategories_landscape_height;
        }
        layoutParams.height = A09.getDimensionPixelSize(i);
        recyclerView.setLayoutParams(layoutParams);
        for (int i2 = 0; i2 < this.A09.A02.size(); i2++) {
            C75453js r0 = (C75453js) recyclerView.A0C(i2);
            if (r0 != null) {
                A02(r0, z);
            }
        }
        this.A00 = z;
    }
}
