package X;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Pair;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1dY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C33161dY {
    public static final long A0L = TimeUnit.DAYS.toMillis(60);
    public static final C33291dl A0M = new C33291dl();
    public NotificationManager A00;
    public Handler A01;
    public boolean A02 = false;
    public boolean A03 = false;
    public final C14900mE A04;
    public final C15450nH A05;
    public final C27131Gd A06;
    public final AnonymousClass10S A07;
    public final C15610nY A08;
    public final AnonymousClass01d A09;
    public final C14830m7 A0A;
    public final C16590pI A0B;
    public final C15890o4 A0C;
    public final C14820m6 A0D;
    public final C19990v2 A0E;
    public final C20830wO A0F;
    public final C27151Gf A0G;
    public final C21320xE A0H;
    public final AbstractC33331dp A0I;
    public final C244215l A0J;
    public final C244115k A0K;

    public static int A00(boolean z) {
        return z ? 3 : 4;
    }

    public C33161dY(C14900mE r4, C15450nH r5, AnonymousClass10S r6, C15610nY r7, AnonymousClass01d r8, C14830m7 r9, C16590pI r10, C15890o4 r11, C14820m6 r12, C19990v2 r13, C20830wO r14, C21320xE r15, C244215l r16, C244115k r17) {
        C33311dn r0;
        C33321do r02;
        boolean z = C33261di.A00;
        C33141dV r1 = null;
        if (z) {
            r0 = new C33311dn(this);
        } else {
            r0 = null;
        }
        this.A06 = r0;
        if (z) {
            r02 = new C33321do(this);
        } else {
            r02 = null;
        }
        this.A0I = r02;
        this.A0G = z ? new C33141dV(this) : r1;
        this.A0A = r9;
        this.A04 = r4;
        this.A0B = r10;
        this.A0E = r13;
        this.A05 = r5;
        this.A09 = r8;
        this.A08 = r7;
        this.A07 = r6;
        this.A0C = r11;
        this.A0D = r12;
        this.A0H = r15;
        this.A0K = r17;
        this.A0F = r14;
        this.A0J = r16;
    }

    public static String A01(String str) {
        Pair A00 = C33341dq.A00(str);
        if (A00 == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(C15380n4.A04((String) A00.first));
        sb.append("_");
        sb.append(A00.second);
        return sb.toString();
    }

    public static final void A02(NotificationChannel notificationChannel, AbstractC14640lm r4) {
        String str;
        String rawString = r4.getRawString();
        boolean A0J = C15380n4.A0J(r4);
        C33291dl r1 = A0M;
        if (A0J) {
            str = "group_chat_defaults";
        } else {
            str = "individual_chat_defaults";
        }
        String A00 = r1.A00(str);
        if (A00 != null) {
            notificationChannel.setConversationId(A00, rawString);
            A01(A00);
        }
    }

    public NotificationChannel A03(String str) {
        C33291dl r2 = A0M;
        if (r2.A04(str)) {
            return C33301dm.A00(A04(), r2.A00(str));
        }
        return null;
    }

    public synchronized NotificationManager A04() {
        NotificationManager notificationManager;
        notificationManager = this.A00;
        if (notificationManager == null) {
            notificationManager = this.A09.A08();
            this.A00 = notificationManager;
        }
        return notificationManager;
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x004f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.content.ContentValues A05(android.app.NotificationChannel r7, java.lang.String r8, java.lang.String r9, java.lang.String r10, boolean r11) {
        /*
            r6 = this;
            android.content.ContentValues r4 = new android.content.ContentValues
            r4.<init>()
            java.lang.Integer r1 = X.C22630zO.A03(r8)
            java.lang.String r5 = "message_light"
            boolean r0 = r7.shouldShowLights()
            if (r1 == 0) goto L_0x00c0
            if (r0 != 0) goto L_0x0085
            java.lang.String r0 = "000000"
        L_0x0015:
            r4.put(r5, r0)
        L_0x0018:
            long[] r2 = X.C22630zO.A09(r9)
            java.lang.String r1 = "message_vibrate"
            boolean r0 = r7.shouldVibrate()
            if (r2 == 0) goto L_0x0080
            if (r0 != 0) goto L_0x002b
            java.lang.String r0 = "0"
        L_0x0028:
            r4.put(r1, r0)
        L_0x002b:
            boolean r2 = android.text.TextUtils.isEmpty(r10)
            java.lang.String r1 = "message_tone"
            android.net.Uri r0 = r7.getSound()
            if (r2 != 0) goto L_0x0067
            if (r0 != 0) goto L_0x005a
            java.lang.String r0 = ""
        L_0x003b:
            r4.put(r1, r0)
        L_0x003e:
            r0 = 4
            if (r11 == 0) goto L_0x0042
            r0 = 3
        L_0x0042:
            int r2 = r7.getImportance()
            if (r2 == r0) goto L_0x0059
            r0 = 3
            if (r2 < r0) goto L_0x0059
            r1 = 3
            r0 = 0
            if (r2 != r1) goto L_0x0050
            r0 = 1
        L_0x0050:
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r0)
            java.lang.String r0 = "low_pri_notifications"
            r4.put(r0, r1)
        L_0x0059:
            return r4
        L_0x005a:
            android.net.Uri r0 = r7.getSound()
            java.lang.String r0 = r0.toString()
            boolean r0 = r10.equals(r0)
            goto L_0x0075
        L_0x0067:
            if (r0 == 0) goto L_0x003e
            android.net.Uri r0 = r7.getSound()
            java.lang.String r0 = r0.toString()
            boolean r0 = android.text.TextUtils.isEmpty(r0)
        L_0x0075:
            if (r0 != 0) goto L_0x003e
            android.net.Uri r0 = r7.getSound()
            java.lang.String r0 = r0.toString()
            goto L_0x003b
        L_0x0080:
            if (r0 == 0) goto L_0x002b
            java.lang.String r0 = "1"
            goto L_0x0028
        L_0x0085:
            int r0 = r7.getLightColor()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0018
            int r1 = r7.getLightColor()
            r0 = 16777215(0xffffff, float:2.3509886E-38)
            r1 = r1 & r0
            java.lang.String r3 = java.lang.Integer.toHexString(r1)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            int r1 = r3.length()
            java.lang.String r0 = "000000"
            java.lang.String r0 = r0.substring(r1)
            r2.append(r0)
            r2.append(r3)
            java.lang.String r1 = r2.toString()
            java.util.Locale r0 = java.util.Locale.US
            java.lang.String r0 = r1.toUpperCase(r0)
            goto L_0x0015
        L_0x00c0:
            if (r0 == 0) goto L_0x0018
            java.lang.String r0 = "FFFFFF"
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C33161dY.A05(android.app.NotificationChannel, java.lang.String, java.lang.String, java.lang.String, boolean):android.content.ContentValues");
    }

    public CharSequence A06(String str) {
        Context context;
        int i;
        if ("individual_chat_defaults".equals(str)) {
            context = this.A0B.A00;
            i = R.string.settings_notification;
        } else if ("group_chat_defaults".equals(str)) {
            context = this.A0B.A00;
            i = R.string.settings_group_notification;
        } else if ("silent_notifications".equals(str)) {
            context = this.A0B.A00;
            i = R.string.category_silent_notifications;
        } else if ("voip_notification".equals(str)) {
            context = this.A0B.A00;
            i = R.string.category_voip;
        } else {
            AbstractC14640lm A01 = AbstractC14640lm.A01(str);
            if (A01 == null) {
                return null;
            }
            return this.A08.A04(this.A0F.A01(A01));
        }
        return context.getString(i);
    }

    public synchronized String A07(Uri uri, CharSequence charSequence, String str, String str2, String str3, String str4, int i) {
        C33291dl r4 = A0M;
        if (r4.A04(str)) {
            StringBuilder sb = new StringBuilder();
            sb.append("chat-settings-store/addNotificationChannel channel already exists for settingsId:");
            sb.append(C15380n4.A04(str));
            Log.e(sb.toString());
            return r4.A00(str);
        }
        SharedPreferences sharedPreferences = this.A0D.A00;
        int i2 = sharedPreferences.getInt("num_notification_channels_created", 0) + 1;
        StringBuilder sb2 = new StringBuilder();
        sb2.append(str);
        sb2.append("_");
        sb2.append(i2);
        String obj = sb2.toString();
        NotificationChannel notificationChannel = new NotificationChannel(obj, charSequence, i);
        if (!TextUtils.isEmpty(str4)) {
            notificationChannel.setGroup(str4);
        }
        Integer A03 = C22630zO.A03(str2);
        if (A03 != null) {
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(A03.intValue());
        } else {
            notificationChannel.enableLights(false);
        }
        long[] A09 = C22630zO.A09(str3);
        if (A09 != null) {
            notificationChannel.setVibrationPattern(A09);
            notificationChannel.enableVibration(true);
        } else {
            notificationChannel.enableVibration(false);
        }
        notificationChannel.setSound(uri, Notification.AUDIO_ATTRIBUTES_DEFAULT);
        notificationChannel.setLockscreenVisibility(0);
        if (!"group_chat_defaults".equals(str) && !"individual_chat_defaults".equals(str)) {
            AbstractC14640lm A01 = AbstractC14640lm.A01(str);
            if (Build.VERSION.SDK_INT >= 30 && A01 != null) {
                A02(notificationChannel, A01);
            }
        }
        StringBuilder sb3 = new StringBuilder();
        sb3.append("chat-settings-store/addNotificationChannel adding channel with id:");
        sb3.append(A01(obj));
        sb3.append(" importance:");
        sb3.append(i);
        sb3.append(" lights:");
        sb3.append(notificationChannel.shouldShowLights());
        sb3.append(" color:");
        String hexString = Integer.toHexString(notificationChannel.getLightColor() & 16777215);
        StringBuilder sb4 = new StringBuilder();
        sb4.append("000000".substring(hexString.length()));
        sb4.append(hexString);
        sb3.append(sb4.toString());
        sb3.append(" vibrate:");
        sb3.append(notificationChannel.shouldVibrate());
        sb3.append(" sounduri:");
        sb3.append(notificationChannel.getSound());
        Log.i(sb3.toString());
        C33301dm.A02(notificationChannel, A04());
        sharedPreferences.edit().putInt("num_notification_channels_created", i2).apply();
        r4.A02(str, obj);
        return obj;
    }

    public String A08(String str) {
        String A01 = A0M.A01(str);
        if (!"silent_notifications".equals(A01) || C33301dm.A00(A04(), str).getImportance() <= 2) {
            return str;
        }
        StringBuilder sb = new StringBuilder("chat-settings-store/repairSilentNotificationChannel repairing channel:");
        sb.append(C15380n4.A04(A01));
        Log.i(sb.toString());
        A0K(A01);
        return A07(null, A06(A01), A01, null, null, null, 2);
    }

    public String A09(String str) {
        String A01 = A0M.A01(str);
        if ("voip_notification".equals(A01) && C33301dm.A00(A04(), str).getImportance() < 4) {
            StringBuilder sb = new StringBuilder("chat-settings-store/repairVoIPNotificationChannel repairing channel:");
            sb.append(C15380n4.A04(A01));
            Log.i(sb.toString());
            try {
                A0K(A01);
                return A07(null, A06("voip_notification"), "voip_notification", null, null, null, 4);
            } catch (SecurityException unused) {
                Log.w("chat-settings-store/repairVoIPNotificationChannel SecurityException in deleteNotificationChannel");
            }
        }
        return str;
    }

    public String A0A(String str, String str2, String str3, String str4, int i) {
        Uri parse;
        String A01 = A0M.A01(str);
        if ((!"group_chat_defaults".equals(A01) && !"individual_chat_defaults".equals(A01)) || C33301dm.A00(A04(), str).getImportance() >= 3) {
            return str;
        }
        StringBuilder sb = new StringBuilder("chat-settings-store/repairDefaultChannelIfNeeded repairing channel:");
        sb.append(C15380n4.A04(A01));
        Log.i(sb.toString());
        A0K(A01);
        CharSequence A06 = A06(A01);
        if (str4 == null) {
            parse = Settings.System.DEFAULT_NOTIFICATION_URI;
        } else {
            parse = Uri.parse(str4);
        }
        return A07(parse, A06, A01, str2, str3, "channel_group_chats", i);
    }

    public void A0B() {
        NotificationManager A04 = A04();
        AnonymousClass009.A05(A04);
        for (NotificationChannel notificationChannel : C33301dm.A01(A04)) {
            if (!C27381He.A01.contains(notificationChannel.getId()) && !"miscellaneous".equals(notificationChannel.getId())) {
                StringBuilder sb = new StringBuilder("chat-settings-store/deleteAllNotificationChannels/Deleting notification channel: ");
                sb.append(notificationChannel.getId());
                Log.i(sb.toString());
                C33301dm.A03(A04, notificationChannel.getId());
            }
        }
        C33291dl r1 = A0M;
        synchronized (r1) {
            r1.A01.clear();
            r1.A00.clear();
        }
    }

    public void A0C() {
        if (Build.VERSION.SDK_INT >= 26) {
            Log.i("chat-settings-store/deleteDatabaseFiles success");
            A0B();
        }
    }

    public synchronized void A0D() {
        Looper looper;
        if (Build.VERSION.SDK_INT >= 26 && this.A01 == null) {
            C244115k r3 = this.A0K;
            synchronized (r3) {
                if (r3.A00 == null) {
                    HandlerThread handlerThread = new HandlerThread("wa-shared-handler", 10);
                    r3.A00 = handlerThread;
                    handlerThread.start();
                }
                looper = r3.A00.getLooper();
            }
            this.A01 = new Handler(looper, new Handler.Callback() { // from class: X.1dr
                @Override // android.os.Handler.Callback
                public final boolean handleMessage(Message message) {
                    C33161dY.this.A0E();
                    return true;
                }
            });
        }
        Handler handler = this.A01;
        AnonymousClass009.A05(handler);
        if (!handler.hasMessages(1)) {
            handler.sendEmptyMessageDelayed(1, 3000);
        }
    }

    public synchronized void A0E() {
        String str;
        if (this.A02) {
            for (NotificationChannel notificationChannel : C33301dm.A01(A04())) {
                if (!C27381He.A01.contains(notificationChannel.getId()) && !"miscellaneous".equals(notificationChannel.getId()) && "channel_group_chats".equals(notificationChannel.getGroup())) {
                    Pair A00 = C33341dq.A00(notificationChannel.getId());
                    if (A00 == null || (str = (String) A00.first) == null) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("chat-settings-store/updateChannelNames ignoring channel:");
                        sb.append(notificationChannel.getId());
                        Log.i(sb.toString());
                    } else {
                        CharSequence name = notificationChannel.getName();
                        CharSequence A06 = A06(str);
                        if (!TextUtils.equals(name, A06)) {
                            notificationChannel.getId();
                            C33301dm.A02(new NotificationChannel(notificationChannel.getId(), A06, notificationChannel.getImportance()), A04());
                        }
                    }
                }
            }
        }
    }

    public synchronized void A0F(NotificationChannel notificationChannel, String str, int i) {
        SharedPreferences sharedPreferences = this.A0D.A00;
        int i2 = sharedPreferences.getInt("num_notification_channels_created", 0) + 1;
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("_");
        sb.append(i2);
        String obj = sb.toString();
        NotificationChannel notificationChannel2 = new NotificationChannel(obj, A06(str), i);
        notificationChannel2.setGroup("channel_group_chats");
        notificationChannel2.enableLights(notificationChannel.shouldShowLights());
        notificationChannel2.setLightColor(notificationChannel.getLightColor());
        notificationChannel2.enableVibration(notificationChannel.shouldVibrate());
        notificationChannel2.setVibrationPattern(notificationChannel.getVibrationPattern());
        notificationChannel2.setSound(notificationChannel.getSound(), notificationChannel.getAudioAttributes());
        notificationChannel2.setLockscreenVisibility(notificationChannel.getLockscreenVisibility());
        notificationChannel2.setShowBadge(notificationChannel.canShowBadge());
        notificationChannel2.setBypassDnd(notificationChannel.canBypassDnd());
        C33291dl r2 = A0M;
        r2.A03(str, notificationChannel.getId());
        C33301dm.A03(A04(), notificationChannel.getId());
        C33301dm.A02(notificationChannel2, A04());
        StringBuilder sb2 = new StringBuilder();
        sb2.append("chat-settings-store/unMuteChannelBySettingsId creating new channel:");
        sb2.append(notificationChannel2);
        Log.i(sb2.toString());
        r2.A02(str, obj);
        sharedPreferences.edit().putInt("num_notification_channels_created", i2).apply();
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(18:101|4|5|8|(4:10|(4:13|(1:114)(5:107|15|(1:17)(1:27)|18|(3:109|20|115)(3:108|21|(3:111|23|116)(3:110|24|(3:112|26|118)(1:117))))|113|11)|106|(6:31|(4:34|(3:123|42|129)|124|32)|119|93|94|95))|43|100|44|(1:46)(3:(16:49|(1:51)|52|(2:(3:134|58|(1:136))|137)|62|(1:64)|65|(1:67)|(1:69)(1:70)|71|(1:75)|76|142|137|102|47)|132|77)|83|(1:85)|86|(1:88)|89|(1:91)|92|94|95) */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x01b0, code lost:
        r5 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01b1, code lost:
        com.whatsapp.util.Log.e("chat-settings-store/syncNotificationChannels", r5);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void A0G(android.database.sqlite.SQLiteDatabase r27) {
        /*
        // Method dump skipped, instructions count: 554
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C33161dY.A0G(android.database.sqlite.SQLiteDatabase):void");
    }

    public void A0H(Uri uri, CharSequence charSequence, String str, String str2, String str3, boolean z, boolean z2) {
        int i;
        C15380n4.A04(str);
        String A00 = A0M.A00(str);
        if (!C27381He.A01.contains(A00)) {
            if (A00 != null) {
                NotificationChannel A002 = C33301dm.A00(A04(), A00);
                boolean z3 = false;
                Integer A03 = C22630zO.A03(str2);
                boolean shouldShowLights = A002.shouldShowLights();
                if (A03 == null ? shouldShowLights : !(shouldShowLights && A03.equals(Integer.valueOf(A002.getLightColor())))) {
                    z3 = true;
                }
                long[] A09 = C22630zO.A09(str3);
                boolean shouldVibrate = A002.shouldVibrate();
                if (A09 == null ? shouldVibrate : !shouldVibrate) {
                    z3 = true;
                }
                if (!AnonymousClass08r.A00(uri, A002.getSound())) {
                    z3 = true;
                }
                i = A002.getImportance();
                int i2 = 4;
                if (z) {
                    i2 = 3;
                }
                if (i == i2) {
                    i = i2;
                } else if (i >= 3 || (!this.A05.A05(AbstractC15460nI.A0n) && ("group_chat_defaults".equals(str) || "individual_chat_defaults".equals(str)))) {
                    i = i2;
                    z3 = true;
                } else {
                    C15380n4.A04(str);
                }
                if ((Build.VERSION.SDK_INT < 30 || !z2 || !TextUtils.isEmpty(A002.getConversationId())) && !z3) {
                    C15380n4.A04(str);
                    return;
                }
                A0K(str);
            } else {
                i = 4;
                if (z) {
                    i = 3;
                }
            }
            A07(uri, charSequence, str, str2, str3, "channel_group_chats", i);
        }
    }

    public synchronized void A0I(C16310on r2) {
        A0G(r2.A03.A00);
    }

    public void A0J(C16330op r3) {
        if (Build.VERSION.SDK_INT >= 26) {
            Log.i("chat-settings-store/onOpen targeting api 26");
            A0G(r3.A00);
        }
    }

    public void A0K(String str) {
        C33291dl r1 = A0M;
        String A00 = r1.A00(str);
        if (A00 != null && !C27381He.A01.contains(A00)) {
            C33301dm.A03(A04(), A00);
            r1.A03(str, A00);
            StringBuilder sb = new StringBuilder("chat-settings-store/deleteNotificationChannel/deleting channelId:");
            sb.append(A01(A00));
            sb.append(" for settingsId:");
            sb.append(C15380n4.A04(str));
            Log.i(sb.toString());
        }
    }

    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    public boolean A0L(android.app.NotificationChannel r24, X.C16310on r25) {
        /*
        // Method dump skipped, instructions count: 416
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C33161dY.A0L(android.app.NotificationChannel, X.0on):boolean");
    }
}
