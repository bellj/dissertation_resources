package X;

import androidx.sharetarget.ShortcutInfoCompatSaverImpl;
import java.util.Map;
import java.util.concurrent.Future;

/* renamed from: X.0dE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09610dE implements Runnable {
    public final /* synthetic */ C02510Co A00;
    public final /* synthetic */ ShortcutInfoCompatSaverImpl A01;

    public RunnableC09610dE(C02510Co r1, ShortcutInfoCompatSaverImpl shortcutInfoCompatSaverImpl) {
        this.A01 = shortcutInfoCompatSaverImpl;
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        ShortcutInfoCompatSaverImpl shortcutInfoCompatSaverImpl = this.A01;
        shortcutInfoCompatSaverImpl.A04.clear();
        Map map = shortcutInfoCompatSaverImpl.A03;
        for (Future future : map.values()) {
            future.cancel(false);
        }
        map.clear();
        shortcutInfoCompatSaverImpl.A04(this.A00);
    }
}
