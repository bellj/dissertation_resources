package X;

import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;
import com.whatsapp.conversation.ConversationListView;

/* renamed from: X.4UZ  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass4UZ {
    public void A00() {
        if (!(this instanceof AnonymousClass46M)) {
            ConversationListView conversationListView = ((AnonymousClass46L) this).A00.A1g;
            conversationListView.post(new RunnableBRunnable0Shape4S0100000_I0_4(conversationListView, 49));
            return;
        }
        ((AnonymousClass46M) this).A00.A04(false);
    }
}
