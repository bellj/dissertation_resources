package X;

import android.accounts.Account;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.text.TextUtils;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Scope;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.3Iv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C65263Iv {
    public static C65263Iv A02;
    public static final Lock A03 = new ReentrantLock();
    public final SharedPreferences A00;
    public final Lock A01 = new ReentrantLock();

    public C65263Iv(Context context) {
        this.A00 = context.getSharedPreferences("com.google.android.gms.signin", 0);
    }

    public static C65263Iv A00(Context context) {
        C13020j0.A01(context);
        Lock lock = A03;
        lock.lock();
        try {
            C65263Iv r1 = A02;
            if (r1 == null) {
                r1 = new C65263Iv(context.getApplicationContext());
                A02 = r1;
            }
            return r1;
        } finally {
            lock.unlock();
        }
    }

    public static final String A01(String str, String str2) {
        StringBuilder A0t = C12980iv.A0t(str.length() + 1 + C12970iu.A07(str2));
        A0t.append(str);
        A0t.append(":");
        return C12960it.A0d(str2, A0t);
    }

    public GoogleSignInAccount A02() {
        String A04;
        Uri uri;
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String A042 = A04("defaultGoogleSignInAccount");
        if (!TextUtils.isEmpty(A042) && (A04 = A04(A01("googleSignInAccount", A042))) != null) {
            try {
                String str6 = null;
                if (!TextUtils.isEmpty(A04)) {
                    JSONObject A05 = C13000ix.A05(A04);
                    String optString = A05.optString("photoUrl");
                    if (!TextUtils.isEmpty(optString)) {
                        uri = Uri.parse(optString);
                    } else {
                        uri = null;
                    }
                    long parseLong = Long.parseLong(A05.getString("expirationTime"));
                    HashSet A12 = C12970iu.A12();
                    JSONArray jSONArray = A05.getJSONArray("grantedScopes");
                    int length = jSONArray.length();
                    for (int i = 0; i < length; i++) {
                        A12.add(new Scope(1, jSONArray.getString(i)));
                    }
                    String optString2 = A05.optString("id");
                    if (A05.has("tokenId")) {
                        str = A05.optString("tokenId");
                    } else {
                        str = null;
                    }
                    if (A05.has("email")) {
                        str2 = A05.optString("email");
                    } else {
                        str2 = null;
                    }
                    if (A05.has("displayName")) {
                        str3 = A05.optString("displayName");
                    } else {
                        str3 = null;
                    }
                    if (A05.has("givenName")) {
                        str4 = A05.optString("givenName");
                    } else {
                        str4 = null;
                    }
                    if (A05.has("familyName")) {
                        str5 = A05.optString("familyName");
                    } else {
                        str5 = null;
                    }
                    Long valueOf = Long.valueOf(parseLong);
                    String string = A05.getString("obfuscatedIdentifier");
                    long longValue = valueOf.longValue();
                    C13020j0.A05(string);
                    GoogleSignInAccount googleSignInAccount = new GoogleSignInAccount(uri, optString2, str, str2, str3, null, string, str4, str5, C12980iv.A0x(A12), 3, longValue);
                    if (A05.has("serverAuthCode")) {
                        str6 = A05.optString("serverAuthCode");
                    }
                    googleSignInAccount.A06 = str6;
                    return googleSignInAccount;
                }
            } catch (JSONException unused) {
            }
        }
        return null;
    }

    public GoogleSignInOptions A03() {
        String A04;
        String str;
        Account account;
        String str2;
        String A042 = A04("defaultGoogleSignInAccount");
        if (!TextUtils.isEmpty(A042) && (A04 = A04(A01("googleSignInOptions", A042))) != null) {
            try {
                String str3 = null;
                if (!TextUtils.isEmpty(A04)) {
                    JSONObject A05 = C13000ix.A05(A04);
                    HashSet A12 = C12970iu.A12();
                    JSONArray jSONArray = A05.getJSONArray("scopes");
                    int length = jSONArray.length();
                    for (int i = 0; i < length; i++) {
                        A12.add(new Scope(1, jSONArray.getString(i)));
                    }
                    if (A05.has("accountName")) {
                        str = A05.optString("accountName");
                    } else {
                        str = null;
                    }
                    if (!TextUtils.isEmpty(str)) {
                        account = new Account(str, "com.google");
                    } else {
                        account = null;
                    }
                    ArrayList A0x = C12980iv.A0x(A12);
                    boolean z = A05.getBoolean("idTokenRequested");
                    boolean z2 = A05.getBoolean("serverAuthRequested");
                    boolean z3 = A05.getBoolean("forceCodeForRefreshToken");
                    if (A05.has("serverClientId")) {
                        str2 = A05.optString("serverClientId");
                    } else {
                        str2 = null;
                    }
                    if (A05.has("hostedDomain")) {
                        str3 = A05.optString("hostedDomain");
                    }
                    return new GoogleSignInOptions(account, str2, str3, null, A0x, C12970iu.A11(), 3, z, z2, z3);
                }
            } catch (JSONException unused) {
            }
        }
        return null;
    }

    public final String A04(String str) {
        Lock lock = this.A01;
        lock.lock();
        try {
            return this.A00.getString(str, null);
        } finally {
            lock.unlock();
        }
    }

    public final void A05(String str, String str2) {
        Lock lock = this.A01;
        lock.lock();
        try {
            C12970iu.A1D(this.A00.edit(), str, str2);
        } finally {
            lock.unlock();
        }
    }
}
