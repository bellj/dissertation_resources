package X;

import android.content.Context;
import com.whatsapp.qrcode.DevicePairQrScannerActivity;

/* renamed from: X.4qX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103304qX implements AbstractC009204q {
    public final /* synthetic */ DevicePairQrScannerActivity A00;

    public C103304qX(DevicePairQrScannerActivity devicePairQrScannerActivity) {
        this.A00 = devicePairQrScannerActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
