package X;

import com.whatsapp.util.Log;

/* renamed from: X.6Al  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133356Al implements AnonymousClass6MV {
    public final /* synthetic */ C120365g4 A00;

    public C133356Al(C120365g4 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass6MV
    public void APo(C452120p r3) {
        Log.e("PAY: BrazilVerifyCardOTPSendAction/provider key iq returned null");
        C120365g4 r1 = this.A00;
        r1.A03(r1.A09);
    }

    @Override // X.AnonymousClass6MV
    public void AVE(AnonymousClass6B7 r5) {
        C120365g4 r3 = this.A00;
        r3.A03(r3.A03.A02((AnonymousClass6B5) r5.A00, r3.A09));
    }
}
