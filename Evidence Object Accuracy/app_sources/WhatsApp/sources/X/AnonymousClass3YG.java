package X;

/* renamed from: X.3YG  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3YG implements AnonymousClass5W4 {
    public final /* synthetic */ AbstractC471028y A00;
    public final /* synthetic */ C91494Ry A01;

    @Override // X.AnonymousClass5W4
    public void AY2() {
    }

    public AnonymousClass3YG(AbstractC471028y r1, C91494Ry r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass5W4
    public void AOJ(float f, int i) {
        C91494Ry r2 = this.A01;
        r2.A01 = i;
        AbstractC471028y r1 = this.A00;
        r1.A08.setTextColor(i);
        r1.A07.A01(i);
        r1.A08.setFontStyle(r2.A02);
    }
}
