package X;

import android.graphics.Path;
import android.graphics.RectF;

/* renamed from: X.51L  reason: invalid class name */
/* loaded from: classes3.dex */
public final /* synthetic */ class AnonymousClass51L implements AbstractC469028d {
    public static final /* synthetic */ AnonymousClass51L A00 = new AnonymousClass51L();

    @Override // X.AbstractC469028d
    public final Object apply(Object obj) {
        Path path = new Path();
        path.addOval((RectF) obj, Path.Direction.CW);
        path.close();
        return path;
    }
}
