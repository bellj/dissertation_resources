package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.6Dd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C134056Dd implements AnonymousClass5Wu {
    public TextView A00;
    public TextView A01;
    public final C15570nT A02;
    public final AnonymousClass018 A03;
    public final AbstractC116085Ub A04;

    public C134056Dd(C15570nT r1, AnonymousClass018 r2, AbstractC116085Ub r3) {
        this.A02 = r1;
        this.A03 = r2;
        this.A04 = r3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0089, code lost:
        if (((X.C30801Yw) r11).A00(r8.A00) == false) goto L_0x008b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00ad, code lost:
        if (r13 == false) goto L_0x00af;
     */
    @Override // X.AnonymousClass5Wu
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ void A6Q(java.lang.Object r20) {
        /*
        // Method dump skipped, instructions count: 273
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C134056Dd.A6Q(java.lang.Object):void");
    }

    @Override // X.AnonymousClass5Wu
    public int ADq() {
        return R.layout.novi_conversation_row_payment_amount_summary;
    }

    @Override // X.AnonymousClass5Wu
    public void AYL(View view) {
        this.A00 = C12960it.A0I(view, R.id.amount_container);
        this.A01 = C12960it.A0I(view, R.id.secondary_amount_container);
    }
}
