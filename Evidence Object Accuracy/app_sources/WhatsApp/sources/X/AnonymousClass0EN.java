package X;

import android.os.Bundle;

/* renamed from: X.0EN  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0EN extends AnonymousClass016 implements AbstractC11310g5 {
    public AbstractC001200n A00;
    public AnonymousClass0Ym A01;
    public AnonymousClass0QL A02;
    public final Bundle A03;
    public final AnonymousClass0QL A04;

    public AnonymousClass0EN(Bundle bundle, AnonymousClass0QL r4, AnonymousClass0QL r5) {
        this.A03 = bundle;
        this.A04 = r4;
        this.A02 = r5;
        if (r4.A01 == null) {
            r4.A01 = this;
            return;
        }
        throw new IllegalStateException("There is already a listener registered");
    }

    @Override // X.AnonymousClass017
    public void A02() {
        AnonymousClass0QL r1 = this.A04;
        r1.A06 = true;
        r1.A05 = false;
        r1.A02 = false;
        r1.A03();
    }

    @Override // X.AnonymousClass017
    public void A03() {
        AnonymousClass0QL r1 = this.A04;
        r1.A06 = false;
        r1.A02();
    }

    @Override // X.AnonymousClass017
    public void A09(AnonymousClass02B r2) {
        super.A09(r2);
        this.A00 = null;
        this.A01 = null;
    }

    @Override // X.AnonymousClass017
    public void A0B(Object obj) {
        super.A0B(obj);
        AnonymousClass0QL r1 = this.A02;
        if (r1 != null) {
            r1.A01();
            r1.A05 = true;
            r1.A06 = false;
            r1.A02 = false;
            r1.A03 = false;
            r1.A04 = false;
            this.A02 = null;
        }
    }

    public AnonymousClass0QL A0C(boolean z) {
        AnonymousClass0QL r3 = this.A04;
        r3.A00();
        r3.A02 = true;
        AnonymousClass0Ym r2 = this.A01;
        if (r2 != null) {
            A09(r2);
            if (z && r2.A00) {
                r2.A01.AS8(r2.A02);
            }
        }
        AbstractC11310g5 r0 = r3.A01;
        if (r0 == null) {
            throw new IllegalStateException("No listener register");
        } else if (r0 == this) {
            r3.A01 = null;
            if ((r2 == null || r2.A00) && !z) {
                return r3;
            }
            r3.A01();
            r3.A05 = true;
            r3.A06 = false;
            r3.A02 = false;
            r3.A03 = false;
            r3.A04 = false;
            return this.A02;
        } else {
            throw new IllegalArgumentException("Attempting to unregister the wrong listener");
        }
    }

    public void A0D() {
        AbstractC001200n r1 = this.A00;
        AnonymousClass0Ym r0 = this.A01;
        if (r1 != null && r0 != null) {
            super.A09(r0);
            A05(r1, r0);
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(64);
        sb.append("LoaderInfo{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" #");
        sb.append(0);
        sb.append(" : ");
        C04150Ko.A00(this.A04, sb);
        sb.append("}}");
        return sb.toString();
    }
}
