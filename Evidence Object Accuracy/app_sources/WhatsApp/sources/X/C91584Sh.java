package X;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.whatsapp.R;

/* renamed from: X.4Sh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C91584Sh {
    public final Context A00;
    public final View A01;
    public final View A02;
    public final ViewGroup A03;
    public final ImageView A04;

    public C91584Sh(Context context, ViewGroup viewGroup) {
        this.A03 = viewGroup;
        this.A00 = context;
        this.A01 = AnonymousClass028.A0D(viewGroup, R.id.sticker_onboarding_badge);
        this.A02 = AnonymousClass028.A0D(viewGroup, R.id.sticker_tab);
        this.A04 = C12970iu.A0K(viewGroup, R.id.sticker_tab_icon);
    }
}
