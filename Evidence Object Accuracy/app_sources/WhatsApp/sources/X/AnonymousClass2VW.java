package X;

import android.view.View;

/* renamed from: X.2VW  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2VW extends AnonymousClass04v {
    public final /* synthetic */ boolean A00;

    public AnonymousClass2VW(boolean z) {
        this.A00 = z;
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r3) {
        super.A06(view, r3);
        r3.A0L(this.A00);
    }
}
