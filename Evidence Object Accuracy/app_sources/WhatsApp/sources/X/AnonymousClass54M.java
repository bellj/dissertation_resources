package X;

/* renamed from: X.54M  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass54M implements AbstractC116555Vx {
    public final /* synthetic */ C53862fQ A00;

    public AnonymousClass54M(C53862fQ r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC116555Vx
    public void ARZ(AnonymousClass3F6 r3) {
        if (r3.A01.equals("-1")) {
            C53862fQ r1 = this.A00;
            if (r1.A01 != 0) {
                r1.A0P(true);
            }
        }
    }

    @Override // X.AbstractC116555Vx
    public void AVV() {
        this.A00.A0E();
    }
}
