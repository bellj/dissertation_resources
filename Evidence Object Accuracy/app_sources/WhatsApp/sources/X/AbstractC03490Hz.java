package X;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/* renamed from: X.0Hz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC03490Hz extends AnonymousClass0I0 implements AbstractC12490i0, AbstractC12720iO {
    public String A00 = null;
    public List A01 = new ArrayList();
    public Set A02 = null;
    public Set A03 = null;
    public Set A04 = null;
    public Set A05 = null;

    @Override // X.AbstractC12720iO
    public Set AH3() {
        return null;
    }

    @Override // X.AbstractC12490i0
    public void A5f(AnonymousClass0OO r2) {
        this.A01.add(r2);
    }

    @Override // X.AbstractC12490i0
    public List ABO() {
        return this.A01;
    }

    @Override // X.AbstractC12720iO
    public String AGA() {
        return this.A00;
    }

    @Override // X.AbstractC12720iO
    public Set AGB() {
        return this.A02;
    }

    @Override // X.AbstractC12720iO
    public Set AGC() {
        return this.A03;
    }

    @Override // X.AbstractC12720iO
    public Set AGD() {
        return this.A04;
    }

    @Override // X.AbstractC12720iO
    public void Acj(String str) {
        this.A00 = str;
    }

    @Override // X.AbstractC12720iO
    public void Ack(Set set) {
        this.A02 = set;
    }

    @Override // X.AbstractC12720iO
    public void Acl(Set set) {
        this.A03 = set;
    }

    @Override // X.AbstractC12720iO
    public void Acm(Set set) {
        this.A04 = set;
    }

    @Override // X.AbstractC12720iO
    public void Acy(Set set) {
        this.A05 = set;
    }
}
