package X;

import android.view.ViewTreeObserver;
import com.whatsapp.location.GroupChatLiveLocationsActivity;

/* renamed from: X.4nm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC101594nm implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ GroupChatLiveLocationsActivity A00;

    public ViewTreeObserver$OnGlobalLayoutListenerC101594nm(GroupChatLiveLocationsActivity groupChatLiveLocationsActivity) {
        this.A00 = groupChatLiveLocationsActivity;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        GroupChatLiveLocationsActivity groupChatLiveLocationsActivity = this.A00;
        C12980iv.A1F(groupChatLiveLocationsActivity.A0K, this);
        if (groupChatLiveLocationsActivity.A0K.getWidth() > 0 && groupChatLiveLocationsActivity.A0K.getHeight() > 0) {
            groupChatLiveLocationsActivity.A2i(false);
        }
    }
}
