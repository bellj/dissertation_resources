package X;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/* renamed from: X.0E2  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass0E2 extends AnonymousClass0BR {
    public int A00;
    public int A01;
    public LayoutInflater A02;

    @Deprecated
    public AnonymousClass0E2(Context context, int i) {
        super(context, null, true);
        this.A00 = i;
        this.A01 = i;
        this.A02 = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    @Override // X.AnonymousClass0BR
    public View A01(Context context, Cursor cursor, ViewGroup viewGroup) {
        return this.A02.inflate(this.A00, viewGroup, false);
    }

    @Override // X.AnonymousClass0BR
    public View A03(Context context, Cursor cursor, ViewGroup viewGroup) {
        return this.A02.inflate(this.A01, viewGroup, false);
    }
}
