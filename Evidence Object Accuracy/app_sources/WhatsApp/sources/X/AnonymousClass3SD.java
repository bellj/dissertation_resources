package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.location.GroupChatLiveLocationsActivity;

/* renamed from: X.3SD  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3SD implements AbstractC12150hS {
    public final View A00;
    public final /* synthetic */ GroupChatLiveLocationsActivity A01;

    public AnonymousClass3SD(GroupChatLiveLocationsActivity groupChatLiveLocationsActivity) {
        this.A01 = groupChatLiveLocationsActivity;
        View A0F = C12960it.A0F(groupChatLiveLocationsActivity.getLayoutInflater(), null, R.layout.live_location_map_info_window);
        this.A00 = A0F;
        AnonymousClass028.A0c(A0F, 3);
    }

    @Override // X.AbstractC12150hS
    public View ADS(AnonymousClass03R r13) {
        int A00;
        AnonymousClass1YO A01;
        C30751Yr r10 = ((C35991j8) r13.A0L).A02;
        View view = this.A00;
        GroupChatLiveLocationsActivity groupChatLiveLocationsActivity = this.A01;
        C28801Pb r7 = new C28801Pb(view, groupChatLiveLocationsActivity.A0C, groupChatLiveLocationsActivity.A0Q, (int) R.id.name_in_group_tv);
        TextView A0J = C12960it.A0J(view, R.id.participant_info);
        View findViewById = view.findViewById(R.id.info_btn);
        C15570nT r0 = ((ActivityC13790kL) groupChatLiveLocationsActivity).A01;
        UserJid userJid = r10.A06;
        if (r0.A0F(userJid)) {
            C28801Pb.A00(groupChatLiveLocationsActivity, r7, R.color.live_location_bubble_me_text);
            r7.A02();
            findViewById.setVisibility(8);
        } else {
            C15580nU A02 = C15580nU.A02(groupChatLiveLocationsActivity.A0L.A0c);
            if (A02 == null || (A01 = groupChatLiveLocationsActivity.A0H.A01(A02, userJid)) == null) {
                A00 = AnonymousClass00T.A00(groupChatLiveLocationsActivity, R.color.live_location_bubble_unknown_text);
            } else {
                int[] intArray = groupChatLiveLocationsActivity.getResources().getIntArray(R.array.group_participant_name_colors);
                A00 = intArray[A01.A00 % intArray.length];
            }
            r7.A04(A00);
            r7.A06(groupChatLiveLocationsActivity.A0A.A0B(userJid));
            findViewById.setVisibility(0);
        }
        C27531Hw.A06(r7.A01);
        String str = "";
        int i = r10.A03;
        if (i != -1) {
            StringBuilder A0j = C12960it.A0j(str);
            Object[] A1b = C12970iu.A1b();
            C12960it.A1P(A1b, i, 0);
            str = C12960it.A0d(((ActivityC13830kP) groupChatLiveLocationsActivity).A01.A0I(A1b, R.plurals.location_accuracy, (long) i), A0j);
        }
        C12980iv.A1J(A0J, str);
        return view;
    }
}
