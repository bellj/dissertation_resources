package X;

import java.io.InputStream;
import java.io.OutputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;

/* renamed from: X.3dC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C71603dC implements HttpEntity {
    public final /* synthetic */ C71623dE A00;
    public final /* synthetic */ HttpEntity A01;

    public C71603dC(C71623dE r1, HttpEntity httpEntity) {
        this.A00 = r1;
        this.A01 = httpEntity;
    }

    public void consumeContent() {
        this.A01.consumeContent();
    }

    public InputStream getContent() {
        InputStream content = this.A01.getContent();
        C71623dE r0 = this.A00;
        return new AnonymousClass1oJ(r0.A01, content, Integer.valueOf(r0.A00), C12970iu.A0h());
    }

    public Header getContentEncoding() {
        return this.A01.getContentEncoding();
    }

    public long getContentLength() {
        return this.A01.getContentLength();
    }

    public Header getContentType() {
        return this.A01.getContentType();
    }

    public boolean isChunked() {
        return this.A01.isChunked();
    }

    public boolean isRepeatable() {
        return this.A01.isRepeatable();
    }

    public boolean isStreaming() {
        return this.A01.isStreaming();
    }

    public void writeTo(OutputStream outputStream) {
        InputStream content = getContent();
        try {
            byte[] bArr = new byte[4096];
            while (true) {
                int read = content.read(bArr);
                if (read < 0) {
                    content.close();
                    return;
                }
                outputStream.write(bArr, 0, read);
            }
        } catch (Throwable th) {
            try {
                content.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
