package X;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.OrientationEventListener;
import com.whatsapp.calling.callgrid.viewmodel.OrientationViewModel;

/* renamed from: X.2ah  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52372ah extends OrientationEventListener {
    public int A00 = -1;
    public final Handler A01 = new Handler(Looper.getMainLooper(), new Handler.Callback() { // from class: X.4iN
        @Override // android.os.Handler.Callback
        public final boolean handleMessage(Message message) {
            C52372ah r3 = C52372ah.this;
            if (message.what == 1) {
                OrientationViewModel.A01(r3.A02, r3.A00);
            }
            return true;
        }
    });
    public final /* synthetic */ OrientationViewModel A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C52372ah(Context context, OrientationViewModel orientationViewModel) {
        super(context);
        this.A02 = orientationViewModel;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0060, code lost:
        if (r4 >= (r1 + 270)) goto L_0x0062;
     */
    @Override // android.view.OrientationEventListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onOrientationChanged(int r7) {
        /*
            r6 = this;
            r5 = -1
            if (r7 == r5) goto L_0x004e
            int r4 = r7 % 360
            com.whatsapp.calling.callgrid.viewmodel.OrientationViewModel r1 = r6.A02
            int r2 = r1.A05
            int r0 = 360 - r2
            r3 = 1
            if (r4 >= r0) goto L_0x0064
            if (r4 < r2) goto L_0x0064
            int r1 = r1.A04
            int r0 = 90 - r1
            if (r4 < r0) goto L_0x004f
            int r0 = r1 + 90
            if (r4 >= r0) goto L_0x004f
            r2 = 1
        L_0x001b:
            int r0 = r6.A00
            if (r2 == r0) goto L_0x004e
            if (r2 == r5) goto L_0x004e
            java.lang.String r0 = "voip/OrientationViewModel/VideoOrientationListener/onOrientationChanged Degress =  "
            java.lang.String r0 = X.C12960it.A0W(r4, r0)
            com.whatsapp.util.Log.i(r0)
            java.lang.String r0 = "voip/OrientationViewModel/VideoOrientationListener/onOrientationChanged from: "
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            int r0 = r6.A00
            r1.append(r0)
            java.lang.String r0 = " to: "
            r1.append(r0)
            r1.append(r2)
            X.C12960it.A1F(r1)
            r6.A00 = r2
            android.os.Handler r2 = r6.A01
            r2.removeMessages(r3)
            r0 = 800(0x320, double:3.953E-321)
            r2.sendEmptyMessageDelayed(r3, r0)
        L_0x004e:
            return
        L_0x004f:
            int r0 = 180 - r2
            if (r4 < r0) goto L_0x0059
            int r0 = r2 + 180
            if (r4 >= r0) goto L_0x0059
            r2 = 2
            goto L_0x001b
        L_0x0059:
            int r0 = 270 - r1
            if (r4 < r0) goto L_0x0062
            int r0 = r1 + 270
            r2 = 3
            if (r4 < r0) goto L_0x001b
        L_0x0062:
            r2 = -1
            goto L_0x001b
        L_0x0064:
            r2 = 0
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C52372ah.onOrientationChanged(int):void");
    }
}
