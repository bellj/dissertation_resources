package X;

import android.graphics.Rect;

/* renamed from: X.0Fw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C03040Fw extends AnonymousClass0OE {
    public final /* synthetic */ Rect A00;
    public final /* synthetic */ AnonymousClass0EH A01;

    public C03040Fw(Rect rect, AnonymousClass0EH r2) {
        this.A01 = r2;
        this.A00 = rect;
    }

    @Override // X.AnonymousClass0OE
    public Rect A00(AnonymousClass072 r2) {
        return this.A00;
    }
}
