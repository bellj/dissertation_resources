package X;

/* renamed from: X.5MW  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5MW extends AnonymousClass1TM {
    public AnonymousClass1TN A00;
    public AnonymousClass1TK A01;

    public AnonymousClass5MW(AbstractC114775Na r2) {
        this.A01 = (AnonymousClass1TK) AbstractC114775Na.A00(r2);
        this.A00 = AbstractC114775Na.A01(r2);
    }

    public static AnonymousClass5MW A00(Object obj) {
        if (obj instanceof AnonymousClass5MW) {
            return (AnonymousClass5MW) obj;
        }
        if (obj != null) {
            return new AnonymousClass5MW(AbstractC114775Na.A04(obj));
        }
        throw C12970iu.A0f("null value in getInstance()");
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        C94954co A00 = C94954co.A00();
        A00.A06(this.A01);
        return C94954co.A01(this.A00, A00);
    }
}
