package X;

/* renamed from: X.2IL  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2IL {
    public final AnonymousClass1AO A00;
    public final AnonymousClass1EB A01;
    public final C15600nX A02;
    public final AnonymousClass1AN A03;
    public final AnonymousClass161 A04;
    public final AnonymousClass1AM A05;
    public final AbstractC14440lR A06;

    public AnonymousClass2IL(AnonymousClass1AO r1, AnonymousClass1EB r2, C15600nX r3, AnonymousClass1AN r4, AnonymousClass161 r5, AnonymousClass1AM r6, AbstractC14440lR r7) {
        this.A06 = r7;
        this.A02 = r3;
        this.A04 = r5;
        this.A05 = r6;
        this.A01 = r2;
        this.A03 = r4;
        this.A00 = r1;
    }
}
