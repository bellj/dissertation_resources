package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.1fY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C34081fY {
    public int A00;
    public int A01;
    public long A02;
    public long A03;
    public long A04;
    public long A05;
    public AbstractC14640lm A06;
    public UserJid A07;
    public UserJid A08;
    public AnonymousClass1IS A09;
    public boolean A0A;
    public AnonymousClass1IS[] A0B;

    public C34081fY(AbstractC14640lm r1, int i) {
        this.A06 = r1;
        this.A01 = i;
    }

    public C34081fY(AbstractC14640lm r5, int i, long j) {
        this.A06 = r5;
        this.A01 = i;
        this.A02 = 0;
        this.A00 = 0;
        this.A09 = null;
        this.A04 = 0;
        this.A03 = j;
    }

    public C34081fY(AbstractC14640lm r5, int i, long j, long j2) {
        this.A06 = r5;
        this.A01 = i;
        this.A02 = j;
        this.A00 = 0;
        this.A09 = null;
        this.A04 = j2;
        this.A03 = 0;
    }

    public C34081fY(AbstractC14640lm r4, AnonymousClass1IS r5, int i) {
        this.A06 = r4;
        this.A01 = i;
        this.A02 = 0;
        this.A00 = 0;
        this.A09 = r5;
        this.A04 = 0;
        this.A03 = 0;
    }

    public C34081fY(AbstractC14640lm r4, AnonymousClass1IS r5, AnonymousClass1IS[] r6, int i, boolean z) {
        this.A06 = r4;
        this.A01 = i;
        this.A02 = 0;
        this.A00 = 0;
        this.A09 = r5;
        this.A04 = 0;
        this.A03 = 0;
        this.A0B = r6;
        this.A0A = z;
    }

    public C34081fY A00() {
        C34081fY r2 = new C34081fY(this.A06, this.A01);
        r2.A05 = this.A05;
        r2.A02 = this.A02;
        r2.A00 = this.A00;
        r2.A09 = this.A09;
        r2.A0B = this.A0B;
        r2.A04 = this.A04;
        r2.A03 = this.A03;
        r2.A08 = this.A08;
        r2.A07 = this.A07;
        r2.A0A = this.A0A;
        return r2;
    }
}
