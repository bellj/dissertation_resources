package X;

import android.graphics.Bitmap;

/* renamed from: X.4uz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106014uz implements AnonymousClass5XK {
    public int A00 = -1;
    public C08870bz A01;

    @Override // X.AnonymousClass5XK
    public void AQo(C08870bz r1, int i, int i2) {
    }

    public final synchronized void A00() {
        C08870bz r0 = this.A01;
        if (r0 != null) {
            r0.close();
        }
        this.A01 = null;
        this.A00 = -1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000c, code lost:
        if (X.C08870bz.A01(r2.A01) == false) goto L_0x000e;
     */
    @Override // X.AnonymousClass5XK
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean A7c(int r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            int r0 = r2.A00     // Catch: all -> 0x0011
            if (r3 != r0) goto L_0x000e
            X.0bz r0 = r2.A01     // Catch: all -> 0x0011
            boolean r1 = X.C08870bz.A01(r0)     // Catch: all -> 0x0011
            r0 = 1
            if (r1 != 0) goto L_0x000f
        L_0x000e:
            r0 = 0
        L_0x000f:
            monitor-exit(r2)
            return r0
        L_0x0011:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C106014uz.A7c(int):boolean");
    }

    @Override // X.AnonymousClass5XK
    public synchronized C08870bz AAr(int i, int i2, int i3) {
        C08870bz A03;
        C08870bz r0 = this.A01;
        A03 = r0 != null ? r0.A03() : null;
        A00();
        return A03;
    }

    @Override // X.AnonymousClass5XK
    public synchronized C08870bz AB6(int i) {
        C08870bz r0;
        C08870bz r02;
        if (this.A00 != i || (r02 = this.A01) == null) {
            r0 = null;
        } else {
            r0 = r02.A03();
        }
        return r0;
    }

    @Override // X.AnonymousClass5XK
    public synchronized C08870bz ACt(int i) {
        C08870bz r0;
        r0 = this.A01;
        return r0 != null ? r0.A03() : null;
    }

    @Override // X.AnonymousClass5XK
    public synchronized void AQq(C08870bz r3, int i, int i2) {
        if (this.A01 == null || !((Bitmap) r3.A04()).equals(this.A01.A04())) {
            C08870bz r0 = this.A01;
            if (r0 != null) {
                r0.close();
            }
            this.A01 = r3.A03();
            this.A00 = i;
        }
    }

    @Override // X.AnonymousClass5XK
    public synchronized void clear() {
        A00();
    }
}
