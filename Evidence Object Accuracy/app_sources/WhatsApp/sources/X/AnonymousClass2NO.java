package X;

import com.whatsapp.voipcalling.camera.VoipPhysicalCamera;

/* renamed from: X.2NO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2NO implements AnonymousClass2NP {
    @Override // X.AnonymousClass2NP
    public void AOD(VoipPhysicalCamera voipPhysicalCamera) {
        if (this instanceof AnonymousClass2NN) {
            ((AnonymousClass2NN) this).A00.closeCurrentCamera(voipPhysicalCamera);
        }
    }

    @Override // X.AnonymousClass2NP
    public void AQ0(VoipPhysicalCamera voipPhysicalCamera) {
        if (this instanceof C49902Nf) {
            ((C49902Nf) this).A00.A0E.sendEmptyMessage(11);
        }
    }

    @Override // X.AnonymousClass2NP
    public void AVt(VoipPhysicalCamera voipPhysicalCamera) {
        if (this instanceof C49902Nf) {
            ((C49902Nf) this).A00.A0E.sendEmptyMessage(12);
        }
    }

    @Override // X.AnonymousClass2NP
    public void AXz(VoipPhysicalCamera voipPhysicalCamera) {
        if (this instanceof C49902Nf) {
            ((C49902Nf) this).A00.A0E.sendEmptyMessage(12);
        }
    }
}
