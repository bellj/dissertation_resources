package X;

import android.app.Application;
import com.facebook.redex.IDxObserverShape3S0100000_1_I1;
import com.facebook.redex.IDxObserverShape4S0100000_2_I1;
import com.facebook.redex.ViewOnClickCListenerShape1S0300000_I1;
import com.whatsapp.R;
import com.whatsapp.jid.Jid;
import com.whatsapp.util.ViewOnClickCListenerShape17S0100000_I1;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

/* renamed from: X.2fQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53862fQ extends AnonymousClass014 implements AnonymousClass2Jw, AbstractC49232Jx, AbstractC49242Jy, AbstractC49252Jz {
    public int A00;
    public int A01;
    public AnonymousClass4T8 A02;
    public Jid A03;
    public Stack A04;
    public boolean A05;
    public boolean A06;
    public final AnonymousClass017 A07;
    public final AnonymousClass017 A08;
    public final AnonymousClass017 A09;
    public final AnonymousClass02P A0A;
    public final AnonymousClass02P A0B;
    public final AnonymousClass07E A0C;
    public final C251118d A0D;
    public final C16430p0 A0E;
    public final AnonymousClass2K0 A0F;
    public final C92174Uv A0G;
    public final C68563Vw A0H;
    public final AnonymousClass2K2 A0I;
    public final AnonymousClass4N8 A0J;
    public final AnonymousClass4RU A0K;
    public final C68433Vj A0L;
    public final AnonymousClass018 A0M;
    public final C27691It A0N;
    public final C27691It A0O;
    public final C27691It A0P;
    public final C27691It A0Q = C13000ix.A03();
    public final C27691It A0R = C13000ix.A03();
    public final List A0S;
    public final Set A0T;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C53862fQ(Application application, AnonymousClass07E r19, C89154Iw r20, C251118d r21, C16430p0 r22, AnonymousClass2K0 r23, C92174Uv r24, AbstractC115855Te r25, AbstractC115915Tk r26, AnonymousClass4N8 r27, AbstractC115925Tl r28, C30211Wn r29, AnonymousClass018 r30, Jid jid, String str, Set set, boolean z) {
        super(application);
        int i;
        boolean z2 = z;
        AnonymousClass02P r6 = new AnonymousClass02P();
        this.A0A = r6;
        C27691It A03 = C13000ix.A03();
        this.A0O = A03;
        C27691It A032 = C13000ix.A03();
        this.A0P = A032;
        AnonymousClass02P r3 = new AnonymousClass02P();
        this.A0B = r3;
        this.A0N = C13000ix.A03();
        this.A04 = new Stack();
        this.A0C = r19;
        this.A0M = r30;
        this.A0D = r21;
        AnonymousClass2K2 A7v = r26.A7v(new AbstractC115895Ti() { // from class: X.54V
            @Override // X.AbstractC115895Ti
            public final boolean AJK() {
                C53862fQ r0 = C53862fQ.this;
                C92174Uv r1 = r0.A0G;
                C48122Ek r2 = r0.A0L.A00.A01;
                C14850m9 r12 = r1.A00.A00;
                return r12.A07(450) && r12.A07(1882) && r2 != null && r2.A04();
            }
        });
        this.A0I = A7v;
        this.A0E = r22;
        this.A0G = r24;
        this.A0T = set;
        this.A0F = r23;
        this.A0J = r27;
        C68563Vw A81 = r25.A81(new AbstractC115865Tf() { // from class: X.54Q
            @Override // X.AbstractC115865Tf
            public final boolean AJq() {
                return C53862fQ.this.A0Q();
            }
        }, new AbstractC115875Tg() { // from class: X.54S
            @Override // X.AbstractC115875Tg
            public final C48122Ek AGS() {
                return C53862fQ.this.A0L.A00.A01;
            }
        }, new AbstractC115885Th() { // from class: X.3W0
            @Override // X.AbstractC115885Th
            public final String AGZ() {
                C53862fQ r4 = C53862fQ.this;
                C27691It r1 = r4.A0O;
                if (r1.A01() == null) {
                    return null;
                }
                C30211Wn r32 = (C30211Wn) r1.A01();
                C30211Wn r2 = (C30211Wn) r4.A0P.A01();
                if (r2 != null) {
                    Stack stack = r4.A04;
                    if (!stack.isEmpty() && C12960it.A05(stack.peek()) == 2) {
                        r32 = r2;
                    }
                }
                return r32.A00;
            }
        }, A7v, this, 1);
        this.A0H = A81;
        Map map = r19.A02;
        this.A05 = map.get("saved_force_root_category") != null ? C12970iu.A1Y(map.get("saved_force_root_category")) : z2;
        AnonymousClass4RU r13 = new AnonymousClass4RU(new C89364Jr(this), C12970iu.A0X(r20.A00.A04));
        this.A0K = r13;
        AnonymousClass016 r10 = r27.A00;
        this.A08 = r10;
        AnonymousClass016 r1 = A81.A04;
        this.A07 = r1;
        AnonymousClass016 r0 = r13.A00;
        this.A09 = r0;
        A7v.A08(r19);
        if (map.get("saved_parent_category") != null) {
            A03.A0B(map.get("saved_parent_category"));
        }
        if (map.get("saved_second_level_category") != null) {
            A032.A0B(map.get("saved_second_level_category"));
        }
        if (map.get("saved_search_state_stack") != null) {
            Stack stack = new Stack();
            this.A04 = stack;
            stack.addAll((Collection) map.get("saved_search_state_stack"));
        }
        if (map.get("saved_search_state") != null) {
            i = C12960it.A05(map.get("saved_search_state"));
        } else {
            i = 0;
        }
        this.A01 = i;
        if (A032.A01() == null && r29 != null) {
            this.A0O.A0B(r29);
            this.A00 = 2;
            if (AnonymousClass4BM.A00(r29.A00)) {
                this.A01 = 2;
            } else {
                this.A0P.A0B(r29);
                this.A01 = 1;
            }
        }
        if (jid != null) {
            this.A03 = jid;
            this.A01 = 3;
        }
        if ("nearby_business".equals(str)) {
            this.A01 = 4;
        }
        r3.A0D(r10, new IDxObserverShape3S0100000_1_I1(this, 39));
        r3.A0D(r1, new IDxObserverShape3S0100000_1_I1(this, 38));
        r3.A0D(r0, new IDxObserverShape3S0100000_1_I1(this, 40));
        r6.A0D(A03, new IDxObserverShape4S0100000_2_I1(this, 19));
        r6.A0D(A032, new IDxObserverShape4S0100000_2_I1(this, 19));
        r23.A07 = r27;
        r23.A08 = r13;
        r23.A05 = A81;
        this.A0S = C12960it.A0l();
        C68433Vj A7w = r28.A7w(this, this);
        this.A0L = A7w;
        r3.A0D(A7w.A00, new IDxObserverShape3S0100000_1_I1(this, 37));
    }

    @Override // X.AnonymousClass015
    public void A03() {
        C68433Vj r2 = this.A0L;
        C48112Ej r0 = r2.A00;
        r0.A02.removeCallbacks(r0.A08);
        r2.A04.A00();
        r2.A01 = null;
        AnonymousClass2K0 r02 = this.A0F;
        r02.A07 = null;
        r02.A08 = null;
        r02.A05 = null;
    }

    public final AnonymousClass2K4 A04() {
        AnonymousClass017 r1 = this.A07;
        if (r1.A01() != null) {
            return ((C63363Bh) r1.A01()).A03;
        }
        return new AnonymousClass2K4(150, null);
    }

    public final String A05() {
        String str;
        C30211Wn r0 = (C30211Wn) this.A0P.A01();
        if (r0 != null) {
            str = r0.A00;
        } else {
            str = "";
        }
        C30211Wn r2 = this.A0I.A00;
        if (r2 == null) {
            return str;
        }
        StringBuilder A0j = C12960it.A0j(str);
        A0j.append(",");
        return C12960it.A0d(r2.A00, A0j);
    }

    public final List A06() {
        int i;
        int i2;
        C37171lc A02;
        ArrayList A0l = C12960it.A0l();
        C48112Ej r2 = this.A0L.A00;
        Object A01 = r2.A01();
        if (A01 != null) {
            A0l.add(A01);
            Integer[] numArr = new Integer[3];
            numArr[0] = 2;
            numArr[1] = C12980iv.A0k();
            C12960it.A1P(numArr, 0, 2);
            List asList = Arrays.asList(numArr);
            int i3 = this.A01;
            if ((i3 == 1 || i3 == 4) && asList.contains(Integer.valueOf(r2.A00))) {
                AnonymousClass2K2 r1 = this.A0I;
                if (!(r1.A01 == null || (A02 = r1.A02(this)) == null)) {
                    A0l.add(A02);
                }
            }
        }
        if (!(this.A01 != 0 || (i2 = r2.A00) == 5 || i2 == 4)) {
            Iterator it = this.A0T.iterator();
            if (it.hasNext()) {
                it.next();
                throw C12980iv.A0n("restartVisibilityState");
            }
        }
        int i4 = r2.A00;
        if (this.A0D.A00.A07(1806) && (((i = this.A01) == 2 || i == 0) && i4 != 4)) {
            LinkedList linkedList = new LinkedList();
            linkedList.add(new AnonymousClass3F6(null, "-1", ((AnonymousClass014) this).A00.getString(R.string.all)));
            if (this.A01 == 2) {
                C27691It r12 = this.A0O;
                if (r12.A01() != null) {
                    C30211Wn r3 = (C30211Wn) r12.A01();
                    linkedList.add(new AnonymousClass3F6(r3, r3.A00, r3.A01));
                }
            }
            A0l.add(new C59502uq(new AnonymousClass54M(this), linkedList));
        }
        return A0l;
    }

    public final List A07(AnonymousClass4T8 r3) {
        if (r3 == null) {
            return Collections.emptyList();
        }
        if (this.A01 == 4) {
            return r3.A05;
        }
        return r3.A04;
    }

    public void A08() {
        int i = this.A01;
        if (i != 0) {
            if (i != 1) {
                if (i == 2) {
                    int i2 = this.A0L.A00.A00;
                    if (!(i2 == 4 || i2 == 3 || i2 == 7 || i2 == 6)) {
                        A0P(this.A05);
                        return;
                    }
                } else if (i != 3) {
                    if (i == 4) {
                        C68563Vw r2 = this.A0H;
                        if (!r2.A02) {
                            this.A0E.A0D(r2.A01());
                        }
                    } else {
                        return;
                    }
                }
                C12960it.A1A(this.A0R, 4);
                return;
            }
            C48112Ej r22 = this.A0L.A00;
            int i3 = r22.A00;
            if (i3 == 4 || i3 == 3 || i3 == 7 || i3 == 6) {
                C12960it.A1A(this.A0R, 4);
            } else if (A0R()) {
                this.A04.pop();
                AnonymousClass2K2.A00(this);
                this.A01 = 2;
                if (!this.A06 && this.A09.A01() != null) {
                    AnonymousClass4RU r0 = this.A0K;
                    r0.A00.A0B(r0.A01);
                } else if (r22.A01 != null) {
                    A0C();
                }
                this.A0P.A0A(null);
                C27691It r1 = this.A0O;
                r1.A0A(r1.A01());
            } else {
                A0P(this.A05);
            }
            C68563Vw r23 = this.A0H;
            if (!r23.A02) {
                this.A0E.A0D(r23.A01());
                return;
            }
            return;
        }
        A0E();
    }

    public void A09() {
        AnonymousClass2K2 r1 = this.A0I;
        if (r1.A00 == null && r1.A02.isEmpty()) {
            r1.A01 = null;
        }
        this.A0L.A06();
    }

    public void A0A() {
        C68563Vw r1 = this.A0H;
        C63363Bh r0 = (C63363Bh) r1.A04.A01();
        if (r0 != null && r0.A04 != null) {
            r1.A02();
            if (A0Q()) {
                int i = this.A01;
                if (i == 1) {
                    A0J(A04());
                } else if (i == 4) {
                    AnonymousClass2K0 r3 = this.A0F;
                    C48122Ek r2 = this.A0L.A00.A01;
                    r3.A01(A04(), this.A0I.A01(), r2);
                }
            }
        }
    }

    public final void A0B() {
        C84783zu r1 = new C84783zu();
        ArrayList A0l = C12960it.A0l();
        A0l.add(r1);
        A0M(A0l);
        this.A01 = 0;
        AnonymousClass2K0 r3 = this.A0F;
        C48122Ek r2 = this.A0L.A00.A01;
        AnonymousClass009.A06(r2, "Trying to fetch the categories, but the search location is null");
        r3.A00();
        C59442uj A87 = r3.A0E.A87(r2, r3, r3.A0G.A00);
        ((AbstractC16320oo) A87).A01 = "2.0";
        A87.A03();
        r3.A00 = A87;
    }

    public final void A0C() {
        C30211Wn r9 = (C30211Wn) this.A0O.A01();
        if (r9 != null) {
            C48112Ej r3 = this.A0L.A00;
            if (r3.A01 != null) {
                C84783zu r1 = new C84783zu();
                ArrayList A0l = C12960it.A0l();
                A0l.add(r1);
                A0M(A0l);
                this.A01 = 2;
                AnonymousClass2K0 r2 = this.A0F;
                C48122Ek r6 = r3.A01;
                AnonymousClass2K4 r4 = null;
                if (A0Q()) {
                    r4 = new AnonymousClass2K4(150, null);
                }
                r2.A00();
                C68483Vo r7 = new C68483Vo(r6, r2);
                r2.A04 = r7;
                C59452uk A85 = r2.A0B.A85(r4, null, r6, r7, r2.A0G.A00, r9, "current", "immediate_children", null);
                A85.A03();
                r2.A00 = A85;
            }
        }
    }

    public final void A0D() {
        this.A0B.A0A(A06());
        int i = this.A01;
        if (i == 1) {
            A0N(false);
        } else if (i == 4) {
            A0F();
        }
    }

    public final void A0E() {
        this.A0A.A0A("product_name");
        C12960it.A1A(this.A0R, 6);
    }

    public final void A0F() {
        this.A01 = 4;
        this.A00 = 4;
        this.A0A.A0A("nearby_business");
        this.A0H.A06(null);
        AnonymousClass401 r1 = new AnonymousClass401(false);
        ArrayList A0l = C12960it.A0l();
        A0l.add(r1);
        A0M(A0l);
        AnonymousClass2K0 r3 = this.A0F;
        C48122Ek r2 = this.A0L.A00.A01;
        r3.A01(A04(), this.A0I.A01(), r2);
    }

    public final void A0G() {
        AnonymousClass017 r1 = this.A07;
        if (r1.A01() != null) {
            ArrayList A0x = C12980iv.A0x(((C63363Bh) r1.A01()).A08);
            Object A01 = r1.A01();
            ArrayList A0l = C12960it.A0l();
            C30211Wn r4 = (C30211Wn) this.A0P.A01();
            if (!(this.A0I.A00 == null || r4 == null)) {
                A0l.add(new C59532ut(new ViewOnClickCListenerShape1S0300000_I1(this, A01, r4, 9), r4.A01));
            }
            A0x.addAll(A0l);
            List list = this.A0S;
            if (!list.isEmpty()) {
                A0x.add(new C84823zy());
                A0x.addAll(list);
                AnonymousClass009.A06(this.A02, "DirectorySearchFragmentViewModel/getSubCategoriesSection Current search results cannot be null");
                if (list.size() < A07(this.A02).size()) {
                    A0x.add(new AnonymousClass400(new ViewOnClickCListenerShape17S0100000_I1(this, 30)));
                }
            }
            A0M(A0x);
        }
    }

    public void A0H(int i) {
        Boolean bool;
        String str;
        String str2;
        C27691It r2 = this.A0O;
        if (r2.A01() != null && this.A02 != null) {
            C16430p0 r14 = this.A0E;
            String str3 = ((C30211Wn) r2.A01()).A00;
            String A05 = A05();
            long size = (long) this.A02.A03.size();
            long size2 = (long) A07(this.A02).size();
            C68433Vj r0 = this.A0L;
            int A02 = r0.A02();
            int i2 = this.A00;
            int A01 = r0.A01();
            String str4 = this.A02.A02;
            AnonymousClass2K2 r1 = this.A0I;
            if (r1.A07.AJK()) {
                bool = Boolean.valueOf(r1.A03);
            } else {
                bool = null;
            }
            if (r1.A04) {
                str = "has_catalog";
            } else {
                str = null;
            }
            if (r1.A05) {
                str2 = "open_now";
            } else {
                str2 = null;
            }
            String A052 = r1.A05();
            C28531Ny r12 = new C28531Ny();
            C13000ix.A06(r12, i);
            r12.A0X = str3;
            r12.A0Z = A05;
            r12.A0K = Long.valueOf(size);
            r12.A0N = Long.valueOf(size2);
            r12.A07 = Integer.valueOf(C12970iu.A06(r12, A02, i2, A01));
            r12.A0S = str4;
            r12.A00 = bool;
            r12.A0T = str;
            r12.A0U = str2;
            r12.A0V = A052;
            r14.A04(r12);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0043, code lost:
        if (r1.isEmpty() == false) goto L_0x0045;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0066, code lost:
        if (r1.isEmpty() == false) goto L_0x0068;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0I(int r5) {
        /*
            r4 = this;
            r0 = -1
            if (r5 == r0) goto L_0x002d
            r0 = 1
            if (r5 == r0) goto L_0x0050
            r0 = 2
            if (r5 == r0) goto L_0x0050
            r0 = 3
            if (r5 == r0) goto L_0x0050
            r0 = 4
            if (r5 != r0) goto L_0x002c
            X.02P r1 = r4.A0B
            java.util.ArrayList r0 = X.C12960it.A0l()
            r1.A0A(r0)
            X.1It r1 = r4.A0R
            r0 = 5
            X.C12960it.A1A(r1, r0)
            X.0p0 r3 = r4.A0E
            X.3Vj r0 = r4.A0L
            java.lang.Integer r2 = X.C68433Vj.A00(r0)
            r1 = 28
            r0 = 6
            r3.A05(r2, r1, r0)
        L_0x002c:
            return
        L_0x002d:
            X.017 r0 = r4.A07
            java.lang.Object r1 = r0.A01()
            X.3Bh r1 = (X.C63363Bh) r1
            boolean r0 = r4.A0Q()
            if (r0 == 0) goto L_0x004b
            if (r1 == 0) goto L_0x004b
            java.util.List r1 = r1.A08
            boolean r0 = r1.isEmpty()
            if (r0 != 0) goto L_0x004b
        L_0x0045:
            java.util.ArrayList r1 = X.C12980iv.A0x(r1)
            r3 = 1
            goto L_0x006d
        L_0x004b:
            java.util.ArrayList r1 = X.C12960it.A0l()
            goto L_0x0045
        L_0x0050:
            X.017 r0 = r4.A07
            java.lang.Object r1 = r0.A01()
            X.3Bh r1 = (X.C63363Bh) r1
            boolean r0 = r4.A0Q()
            if (r0 == 0) goto L_0x0086
            if (r1 == 0) goto L_0x0086
            java.util.List r1 = r1.A08
            boolean r0 = r1.isEmpty()
            if (r0 != 0) goto L_0x0086
        L_0x0068:
            java.util.ArrayList r1 = X.C12980iv.A0x(r1)
            r3 = 2
        L_0x006d:
            X.2ur r0 = new X.2ur
            r0.<init>(r4, r3)
            r1.add(r0)
            r4.A0M(r1)
            X.0p0 r2 = r4.A0E
            X.3Vj r0 = r4.A0L
            java.lang.Integer r1 = X.C68433Vj.A00(r0)
            r0 = 28
            r2.A05(r1, r0, r3)
            return
        L_0x0086:
            java.util.ArrayList r1 = X.C12960it.A0l()
            goto L_0x0068
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C53862fQ.A0I(int):void");
    }

    public final void A0J(AnonymousClass2K4 r14) {
        String str;
        boolean z;
        List list;
        C30211Wn r9 = (C30211Wn) this.A0P.A01();
        AnonymousClass2K2 r2 = this.A0I;
        C30211Wn r1 = r2.A00;
        if (r1 != null) {
            r9 = r1;
        }
        C48112Ej r3 = this.A0L.A00;
        if (r3.A01 != null && r9 != null) {
            if (r14 == null || r14.A01 == null) {
                if (r1 != null || !r2.A02.isEmpty() || ((list = r2.A01) != null && !list.isEmpty())) {
                    z = false;
                } else {
                    z = true;
                }
                AnonymousClass401 r12 = new AnonymousClass401(z);
                ArrayList A0l = C12960it.A0l();
                A0l.add(r12);
                A0M(A0l);
            }
            this.A01 = 1;
            AnonymousClass2K0 r13 = this.A0F;
            C48122Ek r6 = r3.A01;
            if (r2.A00 != null || !r2.A02.isEmpty() || r2.A05 || r2.A04) {
                str = "current";
            } else {
                str = "all_descendents";
            }
            AnonymousClass2K3 A01 = r2.A01();
            r13.A00();
            C68473Vn r7 = new C68473Vn(r6, r13);
            r13.A02 = r7;
            C59452uk A85 = r13.A0B.A85(r14, A01, r6, r7, r13.A0G.A00, r9, "all_descendents", str, null);
            A85.A03();
            r13.A00 = A85;
        }
    }

    public final void A0K(AnonymousClass4T8 r10) {
        this.A01 = 1;
        this.A0P.A0B(this.A0O.A01());
        C68563Vw r1 = this.A0H;
        r1.A06(null);
        List emptyList = Collections.emptyList();
        List list = r10.A03;
        List list2 = r10.A05;
        r1.ANS(new AnonymousClass4T8(r10.A00, r10.A01, r10.A02, list, emptyList, list2));
    }

    public void A0L(C30211Wn r13, int i) {
        String str;
        Boolean bool;
        String str2;
        String str3;
        C27691It r1 = this.A0O;
        if (r1.A01() != null) {
            if (r13 == null) {
                str = null;
            } else {
                str = r13.A00;
            }
            C16430p0 r6 = this.A0E;
            String str4 = ((C30211Wn) r1.A01()).A00;
            C68433Vj r0 = this.A0L;
            int A02 = r0.A02();
            int i2 = this.A00;
            int A01 = r0.A01();
            AnonymousClass2K2 r12 = this.A0I;
            if (r12.A07.AJK()) {
                bool = Boolean.valueOf(r12.A03);
            } else {
                bool = null;
            }
            if (r12.A05) {
                str2 = "open_now";
            } else {
                str2 = null;
            }
            String A05 = r12.A05();
            if (r12.A04) {
                str3 = "has_catalog";
            } else {
                str3 = null;
            }
            C28531Ny r14 = new C28531Ny();
            C13000ix.A06(r14, 63);
            r14.A0Y = str;
            r14.A0X = str4;
            r14.A03 = Integer.valueOf(A02);
            r14.A01 = Integer.valueOf(i2);
            r14.A08 = Integer.valueOf(i);
            if (A01 == 0) {
                A01 = 2;
            }
            r14.A07 = Integer.valueOf(A01);
            r14.A00 = bool;
            r14.A0U = str2;
            r14.A0V = A05;
            r14.A0T = str3;
            r6.A04(r14);
        }
    }

    public final void A0M(List list) {
        List A06 = A06();
        A06.addAll(list);
        this.A0B.A0A(A06);
    }

    public final void A0N(boolean z) {
        if (!z || !A0Q()) {
            this.A0H.A06(null);
        }
        if (A0Q()) {
            A0J(A04());
            if (z) {
                C68563Vw r4 = this.A0H;
                C63363Bh r3 = r4.A05;
                List list = r3.A08;
                if (!list.isEmpty()) {
                    list.add(new AnonymousClass405(0));
                    r3.A02 = 2;
                    r4.A03();
                    return;
                }
                return;
            }
            return;
        }
        A0J(null);
    }

    public final void A0O(boolean z) {
        Jid jid;
        int i = this.A01;
        if (i == 0) {
            A0B();
        } else if (i == 1) {
            A0N(z);
        } else if (i == 2) {
            A0C();
        } else if (i == 3) {
            C14850m9 r1 = this.A0D.A00;
            if (r1.A07(450) && r1.A07(1616) && (jid = this.A03) != null) {
                this.A01 = 3;
                this.A00 = 3;
                this.A0A.A0A("business_chaining");
                this.A0H.A06(null);
                AnonymousClass401 r12 = new AnonymousClass401(false);
                ArrayList A0l = C12960it.A0l();
                A0l.add(r12);
                A0M(A0l);
                AnonymousClass2K0 r3 = this.A0F;
                r3.A00();
                AnonymousClass54N r2 = new AnonymousClass54N(r3);
                r3.A01 = r2;
                C59412ug A84 = r3.A0C.A84(r2, r3.A0G.A00, jid);
                A84.A03();
                r3.A00 = A84;
            }
        } else if (i == 4) {
            A0F();
        }
    }

    public final void A0P(boolean z) {
        if (!this.A06) {
            AnonymousClass017 r2 = this.A08;
            if (r2.A01() != null && !((AnonymousClass4RT) r2.A01()).A03.isEmpty()) {
                AnonymousClass4N8 r0 = this.A0J;
                r0.A00.A0A(r0.A01);
                C16430p0 r4 = this.A0E;
                long size = (long) ((AnonymousClass4RT) r2.A01()).A03.size();
                C68433Vj r02 = this.A0L;
                r4.A02(r02.A02(), size, r02.A01());
                this.A0O.A0A(null);
                this.A0A.A0A("product_name");
                AnonymousClass2K2.A00(this);
                this.A01 = 0;
            }
        }
        if (this.A0L.A00.A01 != null) {
            if (A0R()) {
                this.A04.pop();
            }
            if (z) {
                A0B();
            } else {
                A0E();
                return;
            }
        }
        this.A0O.A0A(null);
        this.A0A.A0A("product_name");
        AnonymousClass2K2.A00(this);
        this.A01 = 0;
    }

    public boolean A0Q() {
        String str;
        C48122Ek r0 = this.A0L.A00.A01;
        if (r0 == null || (str = r0.A07) == null) {
            return false;
        }
        if (str.equals("device") || str.equals("pin_on_map")) {
            return true;
        }
        return false;
    }

    public final boolean A0R() {
        Stack stack = this.A04;
        return !stack.isEmpty() && C12960it.A05(stack.peek()) == 2;
    }

    @Override // X.AnonymousClass2Jw
    public void ANn() {
        C90954Pw A04 = this.A0I.A04();
        if (A04 != null) {
            this.A0N.A0A(A04);
        }
    }

    @Override // X.AbstractC49242Jy
    public void ANp() {
        int i;
        C27691It r1;
        Integer valueOf;
        C68433Vj r0 = this.A0L;
        r0.A03();
        int i2 = r0.A00.A00;
        if (i2 != 0) {
            i = 2;
            if (i2 != 2) {
                int i3 = 4;
                if (i2 == 4) {
                    r1 = this.A0Q;
                } else if (i2 == 5) {
                    r1 = this.A0Q;
                    if (this.A01 == 0) {
                        i3 = 3;
                    }
                    valueOf = Integer.valueOf(i3);
                    r1.A0B(valueOf);
                } else {
                    return;
                }
            } else {
                r1 = this.A0Q;
                i = 7;
                if (this.A01 == 0) {
                    i = 6;
                }
            }
        } else {
            r1 = this.A0Q;
            i = 1;
            if (this.A01 == 0) {
                i = 0;
            }
        }
        valueOf = Integer.valueOf(i);
        r1.A0B(valueOf);
    }

    @Override // X.AbstractC49232Jx
    public void ANy(int i) {
        C27691It r1;
        int i2;
        if (i == 0 || i == 7 || i == 6) {
            this.A0E.A05(C68433Vj.A00(this.A0L), 29, 0);
            r1 = this.A0Q;
            i2 = 8;
        } else if (i == 3) {
            this.A0E.A05(C68433Vj.A00(this.A0L), 29, 3);
            r1 = this.A0Q;
            i2 = 5;
        } else {
            return;
        }
        C12960it.A1A(r1, i2);
    }

    @Override // X.AnonymousClass2Jw
    public void ANz() {
        this.A0I.A07();
        A0L(null, 1);
        A0D();
    }

    @Override // X.AbstractC49232Jx
    public void AO0() {
        AnonymousClass2K2.A00(this);
        A0D();
        this.A0E.A05(C68433Vj.A00(this.A0L), 32, 5);
    }

    @Override // X.AnonymousClass2Jw
    public void APK(boolean z) {
        this.A0I.A03 = z;
        A0L(null, 1);
        A0D();
    }

    @Override // X.AnonymousClass2Jw
    public void AQY() {
        C90954Pw A04 = this.A0I.A04();
        if (A04 != null) {
            this.A0N.A0A(A04);
        }
        A0H(60);
    }

    @Override // X.AnonymousClass2Jw
    public void ARD(boolean z) {
        this.A0I.A04 = z;
        A0L(null, 1);
        A0D();
    }

    @Override // X.AbstractC49252Jz
    public void ARK(int i) {
        A0I(4);
    }

    @Override // X.AbstractC49232Jx
    public void ASG() {
        C12960it.A1A(this.A0R, 0);
        this.A0E.A05(C68433Vj.A00(this.A0L), 34, 0);
    }

    @Override // X.AnonymousClass2Jw
    public void ASq() {
        C90954Pw A04 = this.A0I.A04();
        if (A04 != null) {
            this.A0N.A0A(A04);
        }
        A0H(61);
    }

    @Override // X.AnonymousClass2Jw
    public void ATE(boolean z) {
        this.A0I.A05 = z;
        A0L(null, 1);
        A0D();
    }

    @Override // X.AbstractC49232Jx
    public void AVS() {
        C68433Vj r2 = this.A0L;
        r2.A06();
        C12960it.A1A(this.A0R, 3);
        this.A0E.A05(C68433Vj.A00(r2), 31, 0);
    }

    @Override // X.AbstractC49232Jx
    public void AVT() {
        A0O(true);
        this.A0E.A05(C68433Vj.A00(this.A0L), 30, 1);
    }

    @Override // X.AbstractC49232Jx
    public void AVi() {
        this.A0O.A0A(null);
        C12960it.A1A(this.A0R, 6);
        this.A0E.A05(C68433Vj.A00(this.A0L), 33, 5);
    }

    @Override // X.AnonymousClass2Jw
    public void AVl(C30211Wn r3) {
        this.A0I.A00 = null;
        A0D();
    }

    @Override // X.AnonymousClass2Jw
    public void AY1(C30211Wn r2) {
        this.A0I.A00 = r2;
        A0D();
        A0L(r2, 1);
    }
}
