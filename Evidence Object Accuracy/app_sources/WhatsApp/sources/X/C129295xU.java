package X;

import com.facebook.redex.IDxAListenerShape19S0100000_3_I1;
import com.whatsapp.R;

/* renamed from: X.5xU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129295xU {
    public final C20920wX A00;
    public final C14900mE A01;
    public final C14830m7 A02;
    public final C14850m9 A03;
    public final AnonymousClass61M A04;
    public final C17070qD A05;
    public final C130155yt A06;
    public final C130125yq A07;
    public final AnonymousClass61F A08;
    public final AnonymousClass61C A09;

    public C129295xU(C20920wX r1, C14900mE r2, C14830m7 r3, C14850m9 r4, AnonymousClass61M r5, C17070qD r6, C130155yt r7, C130125yq r8, AnonymousClass61F r9, AnonymousClass61C r10) {
        this.A02 = r3;
        this.A03 = r4;
        this.A01 = r2;
        this.A05 = r6;
        this.A00 = r1;
        this.A06 = r7;
        this.A08 = r9;
        this.A07 = r8;
        this.A04 = r5;
        this.A09 = r10;
    }

    public void A00(AnonymousClass12P r14, ActivityC13810kN r15, String str, int i) {
        r15.A2C(R.string.payments_loading);
        C14580lf A0C = C117305Zk.A0C(this.A05);
        AnonymousClass61D r1 = new AnonymousClass61D(this.A00, this.A02, this.A03, this.A06, this.A07, this.A09);
        AnonymousClass6A9 r6 = new AbstractC136196Lo(r14, r15, A0C, this, str, i) { // from class: X.6A9
            public final /* synthetic */ int A00;
            public final /* synthetic */ AnonymousClass12P A01;
            public final /* synthetic */ ActivityC13810kN A02;
            public final /* synthetic */ C14580lf A03;
            public final /* synthetic */ C129295xU A04;
            public final /* synthetic */ String A05;

            {
                this.A04 = r4;
                this.A03 = r3;
                this.A02 = r2;
                this.A05 = r5;
                this.A00 = r6;
                this.A01 = r1;
            }

            @Override // X.AbstractC136196Lo
            public final void AV8(C130785zy r10) {
                C129295xU r62 = this.A04;
                C117315Zl.A0R(r62.A01, this.A03, 
                /*  JADX ERROR: Method code generation error
                    jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0014: INVOKE  
                      (wrap: X.0mE : 0x0012: IGET  (r0v0 X.0mE A[REMOVE]) = (r6v0 'r62' X.5xU) X.5xU.A01 X.0mE)
                      (wrap: X.0lf : 0x0002: IGET  (r1v0 X.0lf A[REMOVE]) = (r9v0 'this' X.6A9 A[IMMUTABLE_TYPE, THIS]) X.6A9.A03 X.0lf)
                      (wrap: X.6Ep : 0x000f: CONSTRUCTOR  (r2v0 X.6Ep A[REMOVE]) = 
                      (wrap: X.12P : 0x000a: IGET  (r3v0 X.12P A[REMOVE]) = (r9v0 'this' X.6A9 A[IMMUTABLE_TYPE, THIS]) X.6A9.A01 X.12P)
                      (wrap: X.0kN : 0x0004: IGET  (r4v0 X.0kN A[REMOVE]) = (r9v0 'this' X.6A9 A[IMMUTABLE_TYPE, THIS]) X.6A9.A02 X.0kN)
                      (r10v0 'r10' X.5zy)
                      (r6v0 'r62' X.5xU)
                      (wrap: java.lang.String : 0x0006: IGET  (r7v0 java.lang.String A[REMOVE]) = (r9v0 'this' X.6A9 A[IMMUTABLE_TYPE, THIS]) X.6A9.A05 java.lang.String)
                      (wrap: int : 0x0008: IGET  (r8v0 int A[REMOVE]) = (r9v0 'this' X.6A9 A[IMMUTABLE_TYPE, THIS]) X.6A9.A00 int)
                     call: X.6Ep.<init>(X.12P, X.0kN, X.5zy, X.5xU, java.lang.String, int):void type: CONSTRUCTOR)
                     type: STATIC call: X.5Zl.A0R(X.0mE, X.0lf, X.0lg):void in method: X.6A9.AV8(X.5zy):void, file: classes4.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                    	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                    	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                    	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                    	at java.util.ArrayList.forEach(ArrayList.java:1259)
                    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                    Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x000f: CONSTRUCTOR  (r2v0 X.6Ep A[REMOVE]) = 
                      (wrap: X.12P : 0x000a: IGET  (r3v0 X.12P A[REMOVE]) = (r9v0 'this' X.6A9 A[IMMUTABLE_TYPE, THIS]) X.6A9.A01 X.12P)
                      (wrap: X.0kN : 0x0004: IGET  (r4v0 X.0kN A[REMOVE]) = (r9v0 'this' X.6A9 A[IMMUTABLE_TYPE, THIS]) X.6A9.A02 X.0kN)
                      (r10v0 'r10' X.5zy)
                      (r6v0 'r62' X.5xU)
                      (wrap: java.lang.String : 0x0006: IGET  (r7v0 java.lang.String A[REMOVE]) = (r9v0 'this' X.6A9 A[IMMUTABLE_TYPE, THIS]) X.6A9.A05 java.lang.String)
                      (wrap: int : 0x0008: IGET  (r8v0 int A[REMOVE]) = (r9v0 'this' X.6A9 A[IMMUTABLE_TYPE, THIS]) X.6A9.A00 int)
                     call: X.6Ep.<init>(X.12P, X.0kN, X.5zy, X.5xU, java.lang.String, int):void type: CONSTRUCTOR in method: X.6A9.AV8(X.5zy):void, file: classes4.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                    	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                    	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                    	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                    	... 15 more
                    Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6Ep, state: NOT_LOADED
                    	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                    	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                    	... 21 more
                    */
                /*
                    this = this;
                    X.5xU r6 = r9.A04
                    X.0lf r1 = r9.A03
                    X.0kN r4 = r9.A02
                    java.lang.String r7 = r9.A05
                    int r8 = r9.A00
                    X.12P r3 = r9.A01
                    r5 = r10
                    X.6Ep r2 = new X.6Ep
                    r2.<init>(r3, r4, r5, r6, r7, r8)
                    X.0mE r0 = r6.A01
                    X.C117315Zl.A0R(r0, r1, r2)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass6A9.AV8(X.5zy):void");
            }
        };
        r1.A03.A0B(new IDxAListenerShape19S0100000_3_I1(r6, 5), AnonymousClass61S.A01("novi-get-debit-card-schema"), "get", 3);
    }
}
