package X;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/* renamed from: X.3wO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C82913wO extends AbstractC1093851m {
    public List A00;
    public final AnonymousClass4BO A01;

    public C82913wO(AnonymousClass4BO r2, Collection collection) {
        ArrayList A0l = C12960it.A0l();
        this.A00 = A0l;
        A0l.addAll(collection);
        this.A01 = r2;
    }

    public C82913wO(AbstractC1093851m r3, AnonymousClass4BO r4) {
        ArrayList A0l = C12960it.A0l();
        this.A00 = A0l;
        A0l.add(r3);
        A0l.add(null);
        this.A01 = r4;
    }

    @Override // X.AnonymousClass5T6
    public boolean A65(AnonymousClass4RG r6) {
        AnonymousClass4BO r4 = this.A01;
        if (r4 == AnonymousClass4BO.OR) {
            for (AbstractC1093851m r0 : this.A00) {
                if (r0.A65(r6)) {
                    return true;
                }
            }
            return false;
        }
        AnonymousClass4BO r1 = AnonymousClass4BO.AND;
        List<AbstractC1093851m> list = this.A00;
        if (r4 != r1) {
            return !((AbstractC1093851m) list.get(0)).A65(r6);
        }
        for (AbstractC1093851m r02 : list) {
            if (!r02.A65(r6)) {
                return false;
            }
        }
        return true;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("(");
        StringBuilder A0k2 = C12960it.A0k(" ");
        A0k2.append(this.A01.operatorString);
        A0k.append(C95094d8.A00(this.A00, C12960it.A0d(" ", A0k2), ""));
        return C12960it.A0d(")", A0k);
    }
}
