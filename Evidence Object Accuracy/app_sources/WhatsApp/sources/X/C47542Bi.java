package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0310000_I0;
import com.facebook.redex.RunnableBRunnable0Shape1S0300000_I0_1;

/* renamed from: X.2Bi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C47542Bi implements AnonymousClass1GL {
    public final /* synthetic */ AnonymousClass1JM A00;
    public final /* synthetic */ C22100yW A01;

    public C47542Bi(AnonymousClass1JM r1, C22100yW r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass1GL
    public void AQ7(boolean z) {
        this.A01.A0N.Ab2(new RunnableBRunnable0Shape0S0310000_I0(this, this.A00, this, 5, z));
    }

    @Override // X.AnonymousClass1GL
    public void AWu() {
        this.A01.A0N.Ab2(new RunnableBRunnable0Shape1S0300000_I0_1(this, this.A00, this, 0));
    }
}
