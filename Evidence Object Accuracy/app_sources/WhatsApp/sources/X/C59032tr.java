package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.List;

/* renamed from: X.2tr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59032tr extends AbstractC59082tw {
    public final AbstractC15710nm A00;
    public final C14650lo A01;
    public final AnonymousClass2EK A02;
    public final AnonymousClass3HN A03;
    public final C15680nj A04;
    public final C19870uo A05;
    public final C17220qS A06;
    public final C19840ul A07;
    public final String A08;
    public final String A09;
    public final List A0A;

    public C59032tr(AbstractC15710nm r3, C14650lo r4, AnonymousClass2EK r5, AnonymousClass3HN r6, C15680nj r7, UserJid userJid, C19870uo r9, C17220qS r10, C19860un r11, String str, String str2, List list) {
        super(r4, userJid);
        this.A02 = r5;
        this.A07 = (C19840ul) ((AnonymousClass01J) AnonymousClass027.A00(AnonymousClass01J.class, r11.A00.A00)).A1Q.get();
        this.A06 = r10;
        this.A01 = r4;
        this.A04 = r7;
        this.A03 = r6;
        this.A00 = r3;
        this.A09 = str;
        this.A08 = str2;
        this.A0A = list;
        this.A05 = r9;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        this.A07.A02("plm_details_view_tag");
        Log.e("RequestBizProductListProtocolHelper/onDeliveryFailure");
        AnonymousClass2EK.A00(this.A02, 3);
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r5, String str) {
        this.A07.A02("plm_details_view_tag");
        UserJid userJid = ((AbstractC59082tw) this).A00;
        AnonymousClass3HN r2 = this.A03;
        C44721zR A01 = r2.A01(r5);
        r2.A03(this.A01, userJid, r5);
        if (A01 == null) {
            AnonymousClass2EK.A00(this.A02, 4);
            this.A00.AaV("RequestBizProductListProtocolHelper/get product catalog error", "error_code=0", true);
            return;
        }
        List list = A01.A01;
        AnonymousClass2EK r22 = this.A02;
        C90834Pk r0 = new C90834Pk(1);
        r0.A01 = list;
        r22.A02(r0);
    }
}
