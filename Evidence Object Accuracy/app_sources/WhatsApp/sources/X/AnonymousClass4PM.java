package X;

import android.net.Uri;

/* renamed from: X.4PM  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4PM {
    public final Uri A00;
    public final String A01;
    public final String A02;

    public AnonymousClass4PM(Uri uri, String str, String str2) {
        this.A00 = uri;
        this.A01 = str;
        this.A02 = str2;
    }
}
