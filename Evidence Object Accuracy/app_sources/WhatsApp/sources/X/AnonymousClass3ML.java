package X;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.text.Layout;
import android.text.style.LineBackgroundSpan;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import java.util.ArrayList;

/* renamed from: X.3ML  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3ML implements LineBackgroundSpan {
    public final int A00;
    public final int A01;
    public final int A02;
    public final TextEmojiLabel A03;
    public final String A04;
    public final ArrayList A05 = C12960it.A0l();
    public final boolean A06;

    public AnonymousClass3ML(TextEmojiLabel textEmojiLabel, String str, int i, int i2, int i3, boolean z) {
        this.A01 = i;
        this.A00 = i2;
        this.A03 = textEmojiLabel;
        this.A04 = str;
        this.A06 = z;
        this.A02 = i3;
    }

    @Override // android.text.style.LineBackgroundSpan
    public void drawBackground(Canvas canvas, Paint paint, int i, int i2, int i3, int i4, int i5, CharSequence charSequence, int i6, int i7, int i8) {
        int i9;
        float f;
        float f2;
        float f3;
        TextEmojiLabel textEmojiLabel = this.A03;
        Layout layout = textEmojiLabel.getLayout();
        int i10 = this.A01;
        int lineForOffset = layout.getLineForOffset(i10);
        int i11 = this.A00;
        int lineForOffset2 = layout.getLineForOffset(i11);
        if (lineForOffset <= i8 && i8 <= lineForOffset2) {
            if (i8 == lineForOffset) {
                i9 = (int) layout.getPrimaryHorizontal(i10);
                if (i8 == lineForOffset2) {
                    f3 = layout.getPrimaryHorizontal(i11);
                    int i12 = (int) f3;
                    int dimension = ((int) textEmojiLabel.getResources().getDimension(R.dimen.status_text_h_padding)) / 3;
                    RectF rectF = new RectF((float) (i9 - dimension), (float) i3, (float) (i12 + dimension), (float) i5);
                    Rect A0J = C12980iv.A0J();
                    textEmojiLabel.getGlobalVisibleRect(A0J);
                    int i13 = A0J.left;
                    int i14 = this.A02;
                    int i15 = A0J.top;
                    this.A05.add(new RectF((float) (((i9 + i13) - dimension) + i14), (float) (i3 + i15 + i14), (float) (i13 + i12 + dimension + i14), (float) (i15 + i5 + i14)));
                    int color = paint.getColor();
                    paint.setColor(C12960it.A09(textEmojiLabel).getColor(R.color.white_alpha_20));
                    float f4 = (float) i14;
                    canvas.drawRoundRect(rectF, f4, f4, paint);
                    paint.setColor(color);
                }
            } else {
                i10 = i6;
                if (i8 <= lineForOffset || i8 >= lineForOffset2) {
                    i9 = (int) layout.getPrimaryHorizontal(i10);
                    f = (float) i9;
                    f2 = paint.measureText(charSequence, i10, i11);
                    f3 = f + f2;
                    int i12 = (int) f3;
                    int dimension = ((int) textEmojiLabel.getResources().getDimension(R.dimen.status_text_h_padding)) / 3;
                    RectF rectF = new RectF((float) (i9 - dimension), (float) i3, (float) (i12 + dimension), (float) i5);
                    Rect A0J = C12980iv.A0J();
                    textEmojiLabel.getGlobalVisibleRect(A0J);
                    int i13 = A0J.left;
                    int i14 = this.A02;
                    int i15 = A0J.top;
                    this.A05.add(new RectF((float) (((i9 + i13) - dimension) + i14), (float) (i3 + i15 + i14), (float) (i13 + i12 + dimension + i14), (float) (i15 + i5 + i14)));
                    int color = paint.getColor();
                    paint.setColor(C12960it.A09(textEmojiLabel).getColor(R.color.white_alpha_20));
                    float f4 = (float) i14;
                    canvas.drawRoundRect(rectF, f4, f4, paint);
                    paint.setColor(color);
                }
                i9 = (int) layout.getPrimaryHorizontal(i10);
            }
            f = (float) i9;
            f2 = paint.measureText(charSequence, i10, i7);
            f3 = f + f2;
            int i12 = (int) f3;
            int dimension = ((int) textEmojiLabel.getResources().getDimension(R.dimen.status_text_h_padding)) / 3;
            RectF rectF = new RectF((float) (i9 - dimension), (float) i3, (float) (i12 + dimension), (float) i5);
            Rect A0J = C12980iv.A0J();
            textEmojiLabel.getGlobalVisibleRect(A0J);
            int i13 = A0J.left;
            int i14 = this.A02;
            int i15 = A0J.top;
            this.A05.add(new RectF((float) (((i9 + i13) - dimension) + i14), (float) (i3 + i15 + i14), (float) (i13 + i12 + dimension + i14), (float) (i15 + i5 + i14)));
            int color = paint.getColor();
            paint.setColor(C12960it.A09(textEmojiLabel).getColor(R.color.white_alpha_20));
            float f4 = (float) i14;
            canvas.drawRoundRect(rectF, f4, f4, paint);
            paint.setColor(color);
        }
    }
}
