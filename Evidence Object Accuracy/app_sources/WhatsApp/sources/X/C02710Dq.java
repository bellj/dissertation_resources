package X;

import android.graphics.Rect;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.WindowInsets;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/* renamed from: X.0Dq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02710Dq extends C06250St {
    public static Class A04;
    public static Field A05;
    public static Field A06;
    public static Method A07;
    public static boolean A08;
    public AnonymousClass0U7 A00;
    public AnonymousClass0U7 A01 = null;
    public C018408o A02;
    public final WindowInsets A03;

    public C02710Dq(C018408o r2, WindowInsets windowInsets) {
        super(r2);
        this.A03 = windowInsets;
    }

    @Override // X.C06250St
    public final AnonymousClass0U7 A03() {
        AnonymousClass0U7 r0 = this.A01;
        if (r0 != null) {
            return r0;
        }
        WindowInsets windowInsets = this.A03;
        AnonymousClass0U7 A00 = AnonymousClass0U7.A00(windowInsets.getSystemWindowInsetLeft(), windowInsets.getSystemWindowInsetTop(), windowInsets.getSystemWindowInsetRight(), windowInsets.getSystemWindowInsetBottom());
        this.A01 = A00;
        return A00;
    }

    @Override // X.C06250St
    public AnonymousClass0U7 A05(int i) {
        AnonymousClass0U7 A00;
        AnonymousClass0U7 r1;
        AnonymousClass0U7 r12;
        C06190Sn A062;
        AnonymousClass0U7 r7 = AnonymousClass0U7.A04;
        int i2 = 1;
        while (true) {
            if (i2 != 1) {
                AnonymousClass0U7 r13 = null;
                if (i2 == 2) {
                    AnonymousClass0U7 A03 = A03();
                    C018408o r0 = this.A02;
                    if (r0 != null) {
                        r13 = r0.A00.A01();
                    }
                    int i3 = A03.A00;
                    if (r13 != null) {
                        i3 = Math.min(i3, r13.A00);
                    }
                    A00 = AnonymousClass0U7.A00(A03.A01, 0, A03.A02, i3);
                } else if (i2 == 8) {
                    AnonymousClass0U7 A032 = A03();
                    C018408o r02 = this.A02;
                    if (r02 != null) {
                        r1 = r02.A00.A01();
                    } else {
                        r1 = r7;
                    }
                    int i4 = A032.A00;
                    int i5 = r1.A00;
                    if (i4 > i5 || ((r12 = this.A00) != null && !r12.equals(r7) && (i4 = r12.A00) > i5)) {
                        A00 = AnonymousClass0U7.A00(0, 0, 0, i4);
                    }
                    A00 = r7;
                } else if (i2 == 16) {
                    A00 = A02();
                } else if (i2 == 32) {
                    A00 = A00();
                } else if (i2 != 64) {
                    if (i2 == 128) {
                        C018408o r03 = this.A02;
                        if (r03 != null) {
                            A062 = r03.A00.A06();
                        } else {
                            A062 = A06();
                        }
                        if (A062 != null) {
                            A00 = AnonymousClass0U7.A00(A062.A02(), A062.A04(), A062.A03(), A062.A01());
                        }
                    }
                    A00 = r7;
                } else {
                    A00 = A04();
                }
            } else {
                A00 = AnonymousClass0U7.A00(0, A03().A03, 0, 0);
            }
            r7 = AnonymousClass0U7.A00(Math.max(r7.A01, A00.A01), Math.max(r7.A03, A00.A03), Math.max(r7.A02, A00.A02), Math.max(r7.A00, A00.A00));
            do {
                i2 <<= 1;
                if (i2 > 256) {
                    return r7;
                }
            } while ((7 & i2) == 0);
        }
    }

    @Override // X.C06250St
    public C018408o A0A(int i, int i2, int i3, int i4) {
        AnonymousClass0RW r1 = new AnonymousClass0RW(C018408o.A02(this.A03));
        AnonymousClass0U7 A00 = C018408o.A00(A03(), i, i2, i3, i4);
        AnonymousClass0PY r12 = r1.A00;
        r12.A02(A00);
        r12.A01(C018408o.A00(A01(), i, i2, i3, i4));
        return r12.A00();
    }

    @Override // X.C06250St
    public void A0B(View view) {
        AnonymousClass0U7 r0;
        Object invoke;
        if (Build.VERSION.SDK_INT < 30) {
            if (!A08) {
                try {
                    A07 = View.class.getDeclaredMethod("getViewRootImpl", new Class[0]);
                    Class<?> cls = Class.forName("android.view.View$AttachInfo");
                    A04 = cls;
                    A06 = cls.getDeclaredField("mVisibleInsets");
                    A05 = Class.forName("android.view.ViewRootImpl").getDeclaredField("mAttachInfo");
                    A06.setAccessible(true);
                    A05.setAccessible(true);
                } catch (ReflectiveOperationException e) {
                    StringBuilder sb = new StringBuilder("Failed to get visible insets. (Reflection error). ");
                    sb.append(e.getMessage());
                    Log.e("WindowInsetsCompat", sb.toString(), e);
                }
                A08 = true;
            }
            Method method = A07;
            if (!(method == null || A04 == null || A06 == null)) {
                try {
                    invoke = method.invoke(view, new Object[0]);
                } catch (ReflectiveOperationException e2) {
                    StringBuilder sb2 = new StringBuilder("Failed to get visible insets. (Reflection error). ");
                    sb2.append(e2.getMessage());
                    Log.e("WindowInsetsCompat", sb2.toString(), e2);
                }
                if (invoke == null) {
                    Log.w("WindowInsetsCompat", "Failed to get visible insets. getViewRootImpl() returned null from the provided view. This means that the view is either not attached or the method has been overridden", new NullPointerException());
                } else {
                    Rect rect = (Rect) A06.get(A05.get(invoke));
                    if (rect != null) {
                        r0 = AnonymousClass0U7.A00(rect.left, rect.top, rect.right, rect.bottom);
                        if (r0 == null) {
                        }
                        this.A00 = r0;
                        return;
                    }
                }
            }
            r0 = AnonymousClass0U7.A04;
            this.A00 = r0;
            return;
        }
        throw new UnsupportedOperationException("getVisibleInsets() should not be called on API >= 30. Use WindowInsets.isVisible() instead.");
    }

    @Override // X.C06250St
    public void A0D(C018408o r1) {
        this.A02 = r1;
    }

    @Override // X.C06250St
    public boolean A0F() {
        return this.A03.isRound();
    }

    @Override // X.C06250St
    public boolean equals(Object obj) {
        if (!super.equals(obj)) {
            return false;
        }
        return AnonymousClass08r.A00(this.A00, ((C02710Dq) obj).A00);
    }
}
