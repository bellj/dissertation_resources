package X;

import android.media.MediaDataSource;
import android.media.MediaMetadataRetriever;

/* renamed from: X.0L8  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0L8 {
    public static void A00(MediaDataSource mediaDataSource, MediaMetadataRetriever mediaMetadataRetriever) {
        mediaMetadataRetriever.setDataSource(mediaDataSource);
    }
}
