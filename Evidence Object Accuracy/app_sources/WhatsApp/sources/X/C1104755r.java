package X;

import com.whatsapp.status.playback.MessageReplyActivity;

/* renamed from: X.55r  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1104755r implements AbstractC32851cq {
    public final /* synthetic */ MessageReplyActivity A00;

    public C1104755r(MessageReplyActivity messageReplyActivity) {
        this.A00 = messageReplyActivity;
    }

    @Override // X.AbstractC32851cq
    public void AUa(String str) {
        this.A00.Adr(new Object[0], C72453ed.A04(), C72453ed.A03());
    }

    @Override // X.AbstractC32851cq
    public void AUb() {
        C72453ed.A1C(this.A00);
    }

    @Override // X.AbstractC32851cq
    public void AXw(String str) {
        this.A00.Adr(new Object[0], C72453ed.A04(), C72453ed.A03());
    }

    @Override // X.AbstractC32851cq
    public void AXx() {
        C72453ed.A1C(this.A00);
    }
}
