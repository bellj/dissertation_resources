package X;

import android.transition.Transition;

/* renamed from: X.3xx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83823xx extends AbstractC100714mM {
    public final /* synthetic */ AbstractActivityC33001d7 A00;

    public C83823xx(AbstractActivityC33001d7 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC100714mM, android.transition.Transition.TransitionListener
    public void onTransitionEnd(Transition transition) {
        this.A00.A0Q = false;
    }

    @Override // X.AbstractC100714mM, android.transition.Transition.TransitionListener
    public void onTransitionStart(Transition transition) {
        this.A00.A0Q = true;
    }
}
