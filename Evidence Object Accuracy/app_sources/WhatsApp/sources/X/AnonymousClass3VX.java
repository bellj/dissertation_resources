package X;

import com.whatsapp.biz.catalog.view.activity.ProductListActivity;

/* renamed from: X.3VX  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3VX implements AbstractC116525Vu {
    public final /* synthetic */ ProductListActivity A00;

    public AnonymousClass3VX(ProductListActivity productListActivity) {
        this.A00 = productListActivity;
    }

    @Override // X.AbstractC116525Vu
    public void ARn(C44691zO r3, long j) {
        ProductListActivity productListActivity = this.A00;
        C12960it.A0w(((ActivityC13810kN) productListActivity).A00, ((ActivityC13830kP) productListActivity).A01, j);
    }

    @Override // X.AbstractC116525Vu
    public void AUV(C44691zO r9, String str, String str2, int i, long j) {
        C53832fK r0 = this.A00.A0D;
        r0.A06.A01(r9, r0.A08, str, str2, j);
    }
}
