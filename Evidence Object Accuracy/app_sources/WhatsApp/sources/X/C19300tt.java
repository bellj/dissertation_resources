package X;

import android.database.Cursor;

/* renamed from: X.0tt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19300tt extends AbstractC18500sY {
    public final C14850m9 A00;

    public C19300tt(C18480sW r3, C14850m9 r4) {
        super(r3, "rename_deprecated_tables", 1);
        this.A00 = r4;
    }

    @Override // X.AbstractC18500sY
    public AnonymousClass2Ez A09(Cursor cursor) {
        return new AnonymousClass2Ez(0, 0);
    }
}
