package X;

import android.view.View;
import android.view.ViewGroup;

/* renamed from: X.0ZM  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass0ZM implements AbstractC12410hs {
    @Override // X.AbstractC12410hs
    public float ADB(View view, ViewGroup viewGroup) {
        return view.getTranslationX();
    }
}
