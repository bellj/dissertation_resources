package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.2Lu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C49692Lu extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C49692Lu A05;
    public static volatile AnonymousClass255 A06;
    public int A00;
    public int A01 = 0;
    public C43261wh A02;
    public AnonymousClass2Ly A03;
    public Object A04;

    static {
        C49692Lu r0 = new C49692Lu();
        A05 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0061, code lost:
        if (r6.A01 == 1) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0066, code lost:
        if (r6.A01 == 2) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0068, code lost:
        r4 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0069, code lost:
        r6.A04 = r8.Afv(r6.A04, r9.A04, r4);
     */
    @Override // X.AbstractC27091Fz
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object A0V(X.AnonymousClass25B r7, java.lang.Object r8, java.lang.Object r9) {
        /*
        // Method dump skipped, instructions count: 452
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C49692Lu.A0V(X.25B, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    public C57732nX A0b() {
        if (this.A01 == 1) {
            return (C57732nX) this.A04;
        }
        return C57732nX.A06;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.A01 == 1) {
            i2 = 0 + CodedOutputStream.A0A((AnonymousClass1G0) this.A04, 1);
        }
        if (this.A01 == 2) {
            i2 += CodedOutputStream.A0A((AnonymousClass1G0) this.A04, 2);
        }
        if ((this.A00 & 4) == 4) {
            C43261wh r0 = this.A02;
            if (r0 == null) {
                r0 = C43261wh.A0O;
            }
            i2 += CodedOutputStream.A0A(r0, 3);
        }
        if ((this.A00 & 8) == 8) {
            AnonymousClass2Ly r02 = this.A03;
            if (r02 == null) {
                r02 = AnonymousClass2Ly.A07;
            }
            i2 += CodedOutputStream.A0A(r02, 4);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if (this.A01 == 1) {
            codedOutputStream.A0L((AnonymousClass1G0) this.A04, 1);
        }
        if (this.A01 == 2) {
            codedOutputStream.A0L((AnonymousClass1G0) this.A04, 2);
        }
        if ((this.A00 & 4) == 4) {
            C43261wh r0 = this.A02;
            if (r0 == null) {
                r0 = C43261wh.A0O;
            }
            codedOutputStream.A0L(r0, 3);
        }
        if ((this.A00 & 8) == 8) {
            AnonymousClass2Ly r02 = this.A03;
            if (r02 == null) {
                r02 = AnonymousClass2Ly.A07;
            }
            codedOutputStream.A0L(r02, 4);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
