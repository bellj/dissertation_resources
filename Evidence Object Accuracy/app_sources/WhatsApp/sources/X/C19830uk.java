package X;

/* renamed from: X.0uk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19830uk {
    public final C18790t3 A00;
    public final C14650lo A01;
    public final AnonymousClass19Q A02;
    public final C17350qf A03;
    public final C14820m6 A04;
    public final C14850m9 A05;
    public final AnonymousClass18L A06;
    public final AnonymousClass01H A07;
    public final AnonymousClass01N A08;
    public final AnonymousClass01N A09;
    public final AnonymousClass01N A0A;
    public final AnonymousClass01N A0B;
    public final AnonymousClass01N A0C;
    public final AnonymousClass01N A0D;
    public final AnonymousClass01N A0E;

    public C19830uk(C18790t3 r2, C14650lo r3, AnonymousClass19Q r4, AnonymousClass1FH r5, AnonymousClass1FG r6, AnonymousClass1FF r7, C17350qf r8, C14820m6 r9, C14850m9 r10, AnonymousClass1FE r11, AnonymousClass1FE r12, AnonymousClass1FE r13, AnonymousClass12X r14, AnonymousClass18L r15, AnonymousClass01H r16) {
        this.A05 = r10;
        this.A00 = r2;
        this.A04 = r9;
        this.A01 = r3;
        this.A07 = r16;
        this.A08 = new AnonymousClass01N() { // from class: X.5EN
            @Override // X.AnonymousClass01N, X.AnonymousClass01H
            public final Object get() {
                return AnonymousClass12X.this;
            }
        };
        this.A0D = new AnonymousClass01N() { // from class: X.5EM
            @Override // X.AnonymousClass01N, X.AnonymousClass01H
            public final Object get() {
                return AnonymousClass1FE.this;
            }
        };
        this.A0C = new AnonymousClass01N() { // from class: X.5EL
            @Override // X.AnonymousClass01N, X.AnonymousClass01H
            public final Object get() {
                return AnonymousClass1FE.this;
            }
        };
        this.A0B = new AnonymousClass01N() { // from class: X.5EK
            @Override // X.AnonymousClass01N, X.AnonymousClass01H
            public final Object get() {
                return AnonymousClass1FE.this;
            }
        };
        this.A0E = new AnonymousClass01N() { // from class: X.5EJ
            @Override // X.AnonymousClass01N, X.AnonymousClass01H
            public final Object get() {
                return new AnonymousClass1FE(AnonymousClass1FF.this);
            }
        };
        this.A0A = new AnonymousClass01N() { // from class: X.5EI
            @Override // X.AnonymousClass01N, X.AnonymousClass01H
            public final Object get() {
                return new AnonymousClass1FE(AnonymousClass1FG.this);
            }
        };
        this.A09 = new AnonymousClass01N() { // from class: X.5EH
            @Override // X.AnonymousClass01N, X.AnonymousClass01H
            public final Object get() {
                return new AnonymousClass1FE(AnonymousClass1FH.this);
            }
        };
        this.A06 = r15;
        this.A03 = r8;
        this.A02 = r4;
    }
}
