package X;

import android.graphics.Color;
import java.util.Set;

/* renamed from: X.3JJ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3JJ {
    public static final float[] A07 = {0.25f, 0.25f, 0.25f, 0.25f, 0.75f, 0.75f, 0.75f, 0.75f};
    public static final float[] A08 = {0.25f, 0.3f, 0.75f, 0.8f, 0.25f, 0.3f, 0.75f, 0.8f};
    public static final int[] A09 = {14557250, 14687296, 3292341, 3292341, 1423676, 1423676, 15990016, 15990016};
    public static final int[] A0A = {4408575, 4408575, 10296875, 10296875, 4696576, 4696576, 3014647, 3014647};
    public final AbstractC15710nm A00;
    public final C15450nH A01;
    public final C17050qB A02;
    public final C16590pI A03;
    public final C14820m6 A04;
    public final C14850m9 A05;
    public final C239713s A06;

    public AnonymousClass3JJ(AbstractC15710nm r1, C15450nH r2, C17050qB r3, C16590pI r4, C14820m6 r5, C14850m9 r6, C239713s r7) {
        this.A03 = r4;
        this.A05 = r6;
        this.A00 = r1;
        this.A06 = r7;
        this.A01 = r2;
        this.A02 = r3;
        this.A04 = r5;
    }

    public static int A00(int i, int i2) {
        return Math.max(Math.max(C12980iv.A05(Color.red(i), Color.red(i2)), C12980iv.A05(Color.green(i), Color.green(i2))), Color.blue(i) - Color.blue(i2));
    }

    public static Integer A01(Set set, int i) {
        Integer valueOf = Integer.valueOf(i);
        do {
            int intValue = valueOf.intValue();
            int i2 = 2;
            if (intValue != 1) {
                if (intValue != 2) {
                    i2 = 4;
                    if (intValue != 3) {
                        if (intValue != 4) {
                            valueOf = null;
                        } else {
                            valueOf = 1;
                        }
                    }
                } else {
                    valueOf = 3;
                }
                if (valueOf != null || valueOf.intValue() == i) {
                    return null;
                }
            }
            valueOf = Integer.valueOf(i2);
            if (valueOf != null) {
            }
            return null;
        } while (set.contains(valueOf));
        return valueOf;
    }

    public static final void A02(int i) {
        StringBuilder A0h = C12960it.A0h();
        A0h.append(Integer.toHexString(i));
        A0h.append(" (");
        A0h.append(Color.red(i));
        A0h.append(",");
        A0h.append(Color.green(i));
        A0h.append(",");
        A0h.append(Color.blue(i));
        A0h.append(")");
        A0h.toString();
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:79:0x0246 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:81:0x024a */
    /* JADX DEBUG: Multi-variable search result rejected for r7v6, resolved type: byte[] */
    /* JADX DEBUG: Multi-variable search result rejected for r1v96, resolved type: byte */
    /* JADX DEBUG: Multi-variable search result rejected for r3v34, resolved type: byte */
    /* JADX DEBUG: Multi-variable search result rejected for r4v35, resolved type: byte */
    /* JADX DEBUG: Multi-variable search result rejected for r4v39, resolved type: int */
    /* JADX DEBUG: Multi-variable search result rejected for r3v42, resolved type: int */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r3v35 */
    /* JADX WARN: Type inference failed for: r4v36 */
    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    public void A03() {
        /*
        // Method dump skipped, instructions count: 1438
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3JJ.A03():void");
    }
}
