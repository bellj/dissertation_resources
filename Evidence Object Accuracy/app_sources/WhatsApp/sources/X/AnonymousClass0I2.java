package X;

import android.graphics.Path;

/* renamed from: X.0I2  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0I2 extends AnonymousClass0I4 {
    public Path A00;
    public final /* synthetic */ C06540Ua A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass0I2(Path path, C06540Ua r3, float f) {
        super(r3, f, 0.0f);
        this.A01 = r3;
        this.A00 = path;
    }

    @Override // X.AnonymousClass0I4, X.AnonymousClass0P4
    public void A00(String str) {
        C06540Ua r2 = this.A01;
        if (r2.A0j()) {
            AnonymousClass0S1 r1 = r2.A03;
            if (r1.A05) {
                r2.A01.drawTextOnPath(str, this.A00, ((AnonymousClass0I4) this).A00, super.A01, r1.A00);
            }
            AnonymousClass0S1 r12 = r2.A03;
            if (r12.A06) {
                r2.A01.drawTextOnPath(str, this.A00, ((AnonymousClass0I4) this).A00, super.A01, r12.A01);
            }
        }
        ((AnonymousClass0I4) this).A00 += r2.A03.A00.measureText(str);
    }
}
