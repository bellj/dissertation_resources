package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.5Nz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C115025Nz extends EnumC87374Bg {
    public C115025Nz() {
        super("ASCII", 0);
    }

    @Override // X.AnonymousClass5WQ
    public String AHM() {
        return "ASCII";
    }

    @Override // X.AnonymousClass5WQ
    public byte[] A7j(char[] cArr) {
        if (cArr == null) {
            return new byte[0];
        }
        int length = cArr.length;
        byte[] bArr = new byte[length];
        for (int i = 0; i != length; i++) {
            bArr[i] = (byte) cArr[i];
        }
        return bArr;
    }
}
