package X;

import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.MeteringRectangle;
import android.os.Build;
import android.util.Range;
import android.view.Surface;
import com.facebook.redex.IDxCallableShape15S0100000_3_I1;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.61Q  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass61Q {
    public static final Object A0S = C12970iu.A0l();
    public CameraCharacteristics A00;
    public CameraDevice A01;
    public CaptureRequest.Builder A02;
    public Surface A03;
    public Surface A04;
    public Surface A05;
    public Surface A06;
    public AnonymousClass61B A07;
    public AnonymousClass66I A08;
    public C1310360y A09;
    public C124855qE A0A;
    public AbstractC1311561m A0B;
    public C119085cr A0C;
    public C119095cs A0D;
    public AbstractC130695zp A0E;
    public MeteringRectangle[] A0F;
    public MeteringRectangle[] A0G;
    public final C129805yK A0H;
    public final AbstractC136186Li A0I = new AnonymousClass66D(this);
    public final AbstractC136186Li A0J = new AnonymousClass66C(this);
    public final AnonymousClass66N A0K = new AnonymousClass66N(new C125445rG(this));
    public final C129775yH A0L = new C129775yH();
    public final C129775yH A0M = new C129775yH();
    public final C1308560f A0N;
    public final List A0O = C12960it.A0l();
    public volatile C124805q9 A0P;
    public volatile boolean A0Q;
    public volatile boolean A0R;

    public AnonymousClass61Q(C1308560f r3) {
        this.A0N = r3;
        this.A0H = new C129805yK(r3);
    }

    public static void A01(Rect rect, CaptureRequest.Builder builder, AbstractC130695zp r4, MeteringRectangle[] meteringRectangleArr, MeteringRectangle[] meteringRectangleArr2, float f) {
        if (Build.VERSION.SDK_INT >= 30 && C117295Zj.A1S(AbstractC130695zp.A0J, r4)) {
            builder.set(CaptureRequest.CONTROL_ZOOM_RATIO, Float.valueOf(f / 100.0f));
        } else if (C117295Zj.A1S(AbstractC130695zp.A0W, r4)) {
            builder.set(CaptureRequest.SCALER_CROP_REGION, rect);
        }
        if (C117295Zj.A1S(AbstractC130695zp.A0O, r4)) {
            builder.set(CaptureRequest.CONTROL_AF_REGIONS, meteringRectangleArr);
        }
        if (C117295Zj.A1S(AbstractC130695zp.A0P, r4)) {
            builder.set(CaptureRequest.CONTROL_AE_REGIONS, meteringRectangleArr2);
        }
    }

    public static boolean A02(List list, int[] iArr) {
        if (!(list == null || iArr == null || (iArr[0] == 0 && iArr[1] == 0))) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                int[] iArr2 = (int[]) it.next();
                if (iArr2[0] == iArr[0] && iArr2[1] == iArr[1]) {
                    return true;
                }
            }
        }
        return false;
    }

    public CaptureRequest.Builder A03() {
        this.A0H.A00("Cannot get preview request builder.");
        CaptureRequest.Builder builder = this.A02;
        if (builder != null) {
            return builder;
        }
        throw C12960it.A0U("Trying to get mPreviewRequestBuilder before configuring preview.");
    }

    public C1310360y A04(AbstractC136186Li r6, boolean z, boolean z2) {
        C129805yK r2 = this.A0H;
        r2.A00("Cannot start preview.");
        AnonymousClass66I r1 = this.A08;
        r1.A0E = 1;
        r1.A07 = r6;
        r1.A09 = Boolean.TRUE;
        r1.A02 = null;
        r2.A00("Cannot get output surfaces.");
        AnonymousClass61B r12 = this.A07;
        ArrayList A0l = C12960it.A0l();
        A0l.add(this.A04);
        if (z && r12 != null) {
            A0l.add(r12.A03());
        }
        Surface surface = this.A06;
        if (!(surface == null && (surface = this.A03) == null)) {
            A0l.add(surface);
        }
        C1310360y r0 = this.A09;
        if (r0 != null) {
            r0.A03();
        }
        r2.A01("Method createCaptureSession must be called on Optic Thread");
        AnonymousClass66N r13 = this.A0K;
        r13.A03 = 1;
        r13.A02.A02(0);
        this.A09 = (C1310360y) this.A0N.A04("start_preview_on_camera_handler_thread", new CallableC135846Ka(this, A0l));
        A0E(z);
        A0C("Preview session was closed while starting preview", z2);
        this.A0Q = true;
        return this.A09;
    }

    public void A05() {
        this.A0H.A00("Cannot refresh camera preview.");
        try {
            A0C(null, false);
        } catch (Exception unused) {
        }
    }

    public void A06() {
        Surface surface;
        C129805yK r1 = this.A0H;
        r1.A01("Can only stop video recording on the Optic thread");
        r1.A01("Can only check if the prepared on the Optic thread");
        if (r1.A00) {
            CaptureRequest.Builder builder = this.A02;
            if (!(builder == null || (surface = this.A05) == null)) {
                builder.removeTarget(surface);
            }
            this.A05 = null;
        }
    }

    public void A07() {
        C128455w8 r1;
        this.A0H.A00("Cannot update frame metadata collection.");
        C119085cr r12 = this.A0C;
        if (r12 != null && this.A07 != null && this.A08 != null) {
            boolean A1Y = C12970iu.A1Y(r12.A03(AbstractC130685zo.A0R));
            AnonymousClass66I r2 = this.A08;
            if (A1Y) {
                r1 = this.A07.A07;
                if (r2.A05 == null) {
                    r2.A05 = new C129475xm();
                }
            } else {
                r1 = null;
            }
            r2.A0H = A1Y;
            r2.A03 = r1;
        }
    }

    public void A08(Rect rect, MeteringRectangle[] meteringRectangleArr, MeteringRectangle[] meteringRectangleArr2, float f) {
        CaptureRequest.Builder builder;
        AbstractC130695zp r2;
        C129805yK r1 = this.A0H;
        r1.A01("Can only apply zoom on the Optic thread");
        r1.A01("Can only check if the prepared on the Optic thread");
        if (r1.A00 && (builder = this.A02) != null && (r2 = this.A0E) != null) {
            A01(rect, builder, r2, meteringRectangleArr, meteringRectangleArr2, f);
            if (this.A0Q) {
                A05();
            }
        }
    }

    public void A09(SurfaceTexture surfaceTexture, C125425rE r9) {
        AbstractC130695zp r4;
        AbstractC130695zp r1;
        Integer valueOf;
        int i;
        AbstractC1311561m r12;
        Surface surface = new Surface(surfaceTexture);
        this.A0H.A00("Cannot configure camera preview.");
        this.A04 = surface;
        CaptureRequest.Builder createCaptureRequest = this.A01.createCaptureRequest(1);
        this.A02 = createCaptureRequest;
        this.A0G = (MeteringRectangle[]) createCaptureRequest.get(CaptureRequest.CONTROL_AF_REGIONS);
        this.A0F = (MeteringRectangle[]) this.A02.get(CaptureRequest.CONTROL_AE_REGIONS);
        this.A02.set(CaptureRequest.CONTROL_CAPTURE_INTENT, 1);
        this.A02.set(CaptureRequest.CONTROL_MODE, 1);
        boolean A1Y = C12970iu.A1Y(this.A0B.AAQ(AbstractC1311561m.A03));
        Integer A0i = C12980iv.A0i();
        if (!A1Y) {
            this.A02.set(CaptureRequest.CONTROL_SCENE_MODE, A0i);
        }
        CaptureRequest.Builder builder = this.A02;
        CaptureRequest.Key key = CaptureRequest.CONTROL_AE_LOCK;
        Boolean bool = Boolean.FALSE;
        builder.set(key, bool);
        this.A02.set(CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER, A0i);
        if (this.A0D != null) {
            int i2 = 4;
            if (!A0H(4)) {
                i2 = 3;
                if (!A0H(3)) {
                    if (A0H(1)) {
                        C119095cs.A00(this.A0D, AbstractC130685zo.A0C, 1);
                        this.A02.set(CaptureRequest.CONTROL_AF_MODE, 1);
                    }
                }
            }
            C119095cs r13 = this.A0D;
            C125475rJ r0 = AbstractC130685zo.A0C;
            Integer valueOf2 = Integer.valueOf(i2);
            C119095cs.A00(r13, r0, valueOf2);
            this.A02.set(CaptureRequest.CONTROL_AF_MODE, valueOf2);
        }
        if (this.A02 == null || this.A0D == null) {
            throw C12960it.A0U("Cannot initialize stabilization settings, preview closed.");
        }
        AbstractC130695zp r14 = this.A0E;
        if (r14 != null && C117295Zj.A1S(AbstractC130695zp.A0L, r14)) {
            this.A02.set(CaptureRequest.LENS_OPTICAL_STABILIZATION_MODE, 1);
            C119095cs.A00(this.A0D, AbstractC130685zo.A0U, Boolean.TRUE);
        }
        AbstractC130695zp r15 = this.A0E;
        if (r15 != null && C117295Zj.A1S(AbstractC130695zp.A0T, r15)) {
            this.A02.set(CaptureRequest.CONTROL_VIDEO_STABILIZATION_MODE, A0i);
            C119095cs.A00(this.A0D, AbstractC130685zo.A0W, bool);
        }
        AbstractC130695zp r16 = this.A0E;
        if (r16 != null && C117295Zj.A1S(AbstractC130695zp.A0M, r16) && (r12 = this.A0B) != null && C12970iu.A1Y(r12.AAQ(AbstractC1311561m.A06))) {
            this.A02.set(CaptureRequest.CONTROL_VIDEO_STABILIZATION_MODE, 1);
            C119095cs.A00(this.A0D, AbstractC130685zo.A0V, Boolean.TRUE);
        }
        if (this.A02 == null || (r4 = this.A0E) == null || this.A0D == null) {
            throw C12960it.A0U("Cannot initialize fps settings, preview closed.");
        }
        C128395w2 r17 = ((AnonymousClass66J) this.A0B).A01;
        List A0Y = C117295Zj.A0Y(AbstractC130695zp.A0p, r4);
        int[] A00 = r17.A00(A0Y);
        if (A02(A0Y, A00)) {
            C119095cs.A00(this.A0D, AbstractC130685zo.A0j, A00);
            boolean A1S = C117295Zj.A1S(AbstractC130695zp.A0d, this.A0E);
            int i3 = A00[0];
            if (A1S) {
                valueOf = Integer.valueOf(i3 / 1000);
                i = A00[1] / 1000;
            } else {
                valueOf = Integer.valueOf(i3);
                i = A00[1];
            }
            this.A02.set(CaptureRequest.CONTROL_AE_TARGET_FPS_RANGE, Range.create(valueOf, Integer.valueOf(i)));
        }
        if (this.A02 == null || (r1 = this.A0E) == null || this.A0C == null) {
            throw C12960it.A0U("Cannot initialize custom capture settings, preview closed.");
        }
        if (C117295Zj.A1S(AbstractC130695zp.A0B, r1)) {
            this.A0C.A03(AbstractC130685zo.A0h);
        }
        this.A02.set(CaptureRequest.CONTROL_AF_TRIGGER, A0i);
        if (C117295Zj.A1S(AbstractC130695zp.A0K, this.A0E) && C12970iu.A1Y(this.A0B.AAQ(AbstractC1311561m.A04))) {
            this.A02.set(CaptureRequest.NOISE_REDUCTION_MODE, 2);
        }
        if (C117295Zj.A1S(AbstractC130695zp.A0Q, this.A0E) && C12970iu.A1Y(this.A0B.AAQ(AbstractC1311561m.A05))) {
            this.A02.set(CaptureRequest.TONEMAP_MODE, 2);
        }
        this.A02.addTarget(this.A04);
        this.A08.A01 = r9;
        A07();
    }

    public void A0A(CameraCharacteristics cameraCharacteristics, CameraDevice cameraDevice, Surface surface, Surface surface2, AnonymousClass61B r8, C124855qE r9, AbstractC1311561m r10, C119085cr r11, C119095cs r12, AbstractC130695zp r13) {
        C129805yK r2 = this.A0H;
        r2.A01("Can only prepare on the Optic thread");
        this.A01 = cameraDevice;
        this.A0C = r11;
        this.A0D = r12;
        this.A0B = r10;
        this.A0E = r13;
        this.A00 = cameraCharacteristics;
        this.A0A = r9;
        this.A07 = r8;
        this.A03 = surface;
        this.A06 = surface2;
        this.A08 = new AnonymousClass66I();
        r2.A02("Failed to prepare PreviewController.", true);
    }

    public void A0B(Surface surface) {
        Surface surface2;
        C129805yK r2 = this.A0H;
        r2.A00("Cannot start video recording.");
        if (this.A02 == null || (surface2 = this.A04) == null) {
            throw C12960it.A0U("Cannot start video recording, preview closed.");
        }
        this.A05 = surface;
        List asList = Arrays.asList(surface2, surface);
        C1310360y r0 = this.A09;
        if (r0 != null) {
            r0.A03();
        }
        r2.A01("Method createCaptureSession must be called on Optic Thread");
        AnonymousClass66N r02 = this.A0K;
        r02.A03 = 1;
        r02.A02.A02(0);
        this.A09 = (C1310360y) this.A0N.A04("record_video_on_camera_thread", new CallableC135846Ka(this, asList));
        this.A02.addTarget(surface);
        AnonymousClass66I r1 = this.A08;
        r1.A0E = 7;
        r1.A09 = Boolean.TRUE;
        r1.A02 = null;
        A0E(false);
        A0C("Preview session was closed while starting recording.", true);
    }

    public void A0C(String str, boolean z) {
        CaptureRequest.Builder builder;
        this.A0H.A01("Method updatePreviewView must be invoked in the Optic background thread");
        synchronized (A0S) {
            C1310360y r2 = this.A09;
            if (r2 != null && (builder = this.A02) != null) {
                r2.A05(builder.build(), this.A08);
            } else if (z) {
                if (str == null) {
                    str = "Trying to update preview view while preview is closed";
                }
                throw new AnonymousClass6L0(str);
            }
        }
    }

    public void A0D(boolean z) {
        C129805yK r2 = this.A0H;
        r2.A02("Failed to release PreviewController.", false);
        this.A0Q = false;
        AnonymousClass61B r0 = this.A07;
        if (r0 != null) {
            r0.A04();
            this.A07 = null;
        }
        AnonymousClass66I r1 = this.A08;
        if (r1 != null) {
            r1.A0G = false;
            this.A08 = null;
        }
        if (z) {
            try {
                r2.A01("Method closeCameraSession must be called on Optic Thread.");
                AnonymousClass66N r6 = this.A0K;
                r6.A03 = 3;
                C129875yR r5 = r6.A02;
                r5.A02(0);
                C1308560f r4 = this.A0N;
                r4.A04("camera_session_abort_capture_on_camera_handler_thread", new IDxCallableShape15S0100000_3_I1(this, 17));
                r6.A03 = 2;
                r5.A02(0);
                r4.A04("camera_session_close_on_camera_handler_thread", new IDxCallableShape15S0100000_3_I1(this, 18));
            } catch (Exception unused) {
            }
        }
        if (this.A0A != null) {
            this.A0A = null;
        }
        Surface surface = this.A04;
        if (surface != null) {
            surface.release();
            this.A04 = null;
        }
        C1310360y r02 = this.A09;
        if (r02 != null) {
            r02.A03();
            this.A09 = null;
        }
        this.A05 = null;
        this.A02 = null;
        this.A0G = null;
        this.A0F = null;
        this.A01 = null;
        this.A0C = null;
        this.A0D = null;
        this.A0B = null;
        this.A0E = null;
        this.A00 = null;
    }

    public void A0E(boolean z) {
        this.A0H.A00("Cannot update preview builder for CPU frames.");
        AnonymousClass61B r0 = this.A07;
        CaptureRequest.Builder builder = this.A02;
        if (builder != null && r0 != null) {
            Surface A03 = r0.A03();
            if (z) {
                builder.addTarget(A03);
                this.A0R = true;
                return;
            }
            builder.removeTarget(A03);
            this.A0R = false;
        }
    }

    public void A0F(boolean z, boolean z2) {
        AbstractC136186Li r1;
        C129805yK r12 = this.A0H;
        r12.A01("Method restartPreview() must run on the Optic Background Thread.");
        if (this.A08 != null) {
            r12.A01("Can only check if the prepared on the Optic thread");
            if (r12.A00) {
                AnonymousClass66I r2 = this.A08;
                if (!r2.A0G || r2.A0E != 1) {
                    if (z2) {
                        r1 = this.A0J;
                    } else {
                        r1 = this.A0I;
                    }
                    this.A09 = A04(r1, z, false);
                    return;
                }
                this.A0O.add(new C126635tC(z, z2));
            }
        }
    }

    public boolean A0G() {
        return this.A0Q;
    }

    public final boolean A0H(int i) {
        int[] iArr = (int[]) this.A00.get(CameraCharacteristics.CONTROL_AF_AVAILABLE_MODES);
        if (iArr != null) {
            for (int i2 : iArr) {
                if (i2 == i) {
                    return true;
                }
            }
        }
        return false;
    }

    public MeteringRectangle[] A0I() {
        this.A0H.A00("Cannot get default AE regions.");
        return this.A0F;
    }

    public MeteringRectangle[] A0J() {
        this.A0H.A00("Cannot get default AF regions.");
        return this.A0G;
    }
}
