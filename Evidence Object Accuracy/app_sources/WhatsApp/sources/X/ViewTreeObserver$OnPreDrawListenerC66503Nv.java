package X;

import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.DecelerateInterpolator;
import com.whatsapp.R;
import com.whatsapp.settings.chat.wallpaper.WallpaperPreview;

/* renamed from: X.3Nv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ViewTreeObserver$OnPreDrawListenerC66503Nv implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ int A02;
    public final /* synthetic */ int A03;
    public final /* synthetic */ WallpaperPreview A04;

    public ViewTreeObserver$OnPreDrawListenerC66503Nv(WallpaperPreview wallpaperPreview, int i, int i2, int i3, int i4) {
        this.A04 = wallpaperPreview;
        this.A02 = i;
        this.A03 = i2;
        this.A01 = i3;
        this.A00 = i4;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        WallpaperPreview wallpaperPreview = this.A04;
        C12980iv.A1G(wallpaperPreview.A08, this);
        View findViewById = wallpaperPreview.findViewById(R.id.wallpaper_preview_mock_chat);
        int[] A07 = C13000ix.A07();
        wallpaperPreview.A09.getLocationOnScreen(A07);
        wallpaperPreview.A02 = this.A02 - A07[0];
        wallpaperPreview.A03 = this.A03 - A07[1];
        wallpaperPreview.A00 = ((float) this.A01) / C12990iw.A02(wallpaperPreview.A09);
        wallpaperPreview.A01 = ((float) this.A00) / C12990iw.A03(wallpaperPreview.A09);
        int A01 = (int) (C12960it.A01(wallpaperPreview) * 20.0f);
        wallpaperPreview.A09.setPivotX(0.0f);
        wallpaperPreview.A09.setPivotY(0.0f);
        wallpaperPreview.A09.setScaleX(wallpaperPreview.A00);
        wallpaperPreview.A09.setScaleY(wallpaperPreview.A01);
        wallpaperPreview.A09.setTranslationX((float) wallpaperPreview.A02);
        wallpaperPreview.A09.setTranslationY((float) wallpaperPreview.A03);
        wallpaperPreview.A05.setAlpha(0.0f);
        wallpaperPreview.A07.setAlpha(0.0f);
        if (findViewById != null) {
            findViewById.setAlpha(0.0f);
            findViewById.setTranslationY((float) A01);
        }
        DecelerateInterpolator decelerateInterpolator = new DecelerateInterpolator();
        wallpaperPreview.A06.setBackgroundColor(0);
        wallpaperPreview.A05.animate().setDuration(250).alpha(1.0f).setInterpolator(decelerateInterpolator);
        C12970iu.A0I(wallpaperPreview.A09, 250).setInterpolator(decelerateInterpolator).setListener(new AnonymousClass2YW(findViewById, decelerateInterpolator, this));
        return true;
    }
}
