package X;

import android.content.Context;
import android.util.SparseIntArray;

/* renamed from: X.3Gx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C64793Gx {
    public C471929k A00;
    public final SparseIntArray A01;

    public C64793Gx() {
        this(C471729i.A00);
    }

    public C64793Gx(C471929k r2) {
        this.A01 = new SparseIntArray();
        C13020j0.A01(r2);
        this.A00 = r2;
    }

    public final int A00(Context context, AbstractC72443eb r6) {
        int AET;
        SparseIntArray sparseIntArray;
        C13020j0.A01(context);
        C13020j0.A01(r6);
        int i = 0;
        if (r6.Aad() && (i = (sparseIntArray = this.A01).get((AET = r6.AET()), -1)) == -1) {
            int i2 = 0;
            while (true) {
                if (i2 >= sparseIntArray.size()) {
                    i = this.A00.A00(context, AET);
                    break;
                }
                int keyAt = sparseIntArray.keyAt(i2);
                if (keyAt > AET && sparseIntArray.get(keyAt) == 0) {
                    i = 0;
                    break;
                }
                i2++;
            }
            sparseIntArray.put(AET, i);
        }
        return i;
    }
}
