package X;

import android.content.Context;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/* renamed from: X.40j  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C849340j extends AnonymousClass5B1 {
    public final SurfaceHolder.Callback A00;
    public final SurfaceView A01;

    public C849340j(SurfaceView surfaceView, boolean z) {
        super("voip/VideoPort/SurfaceViewVideoPort/", false, z);
        SurfaceHolder$CallbackC100884md r1 = new SurfaceHolder$CallbackC100884md(this);
        this.A00 = r1;
        this.A01 = surfaceView;
        surfaceView.getHolder().addCallback(r1);
        A05();
    }

    @Override // com.whatsapp.voipcalling.VideoPort
    public Context getContext() {
        return this.A01.getContext();
    }

    @Override // X.AnonymousClass5B1, com.whatsapp.voipcalling.VideoPort
    public void release() {
        this.A01.getHolder().removeCallback(this.A00);
        super.release();
    }
}
