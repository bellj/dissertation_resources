package X;

import com.whatsapp.util.Log;
import java.io.File;

/* renamed from: X.1IK  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1IK {
    public final File A00;
    public final File A01;
    public final Object A02 = new Object();

    public AnonymousClass1IK(File file) {
        this.A01 = file;
        StringBuilder sb = new StringBuilder();
        sb.append(file.getPath());
        sb.append(".bak");
        this.A00 = new File(sb.toString());
    }

    public final void A00() {
        File file = this.A01;
        if (file.exists() && !file.delete()) {
            StringBuilder sb = new StringBuilder("DefaultSharedPreferencesStorage/Couldn't clean up partially-written file ");
            sb.append(file);
            Log.e(sb.toString());
        }
    }
}
