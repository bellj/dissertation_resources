package X;

/* renamed from: X.2Ko  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC49392Ko extends Exception {
    public static final boolean A00;
    public static final StackTraceElement[] A01 = new StackTraceElement[0];

    static {
        boolean z = false;
        if (System.getProperty("surefire.test.class.path") != null) {
            z = true;
        }
        A00 = z;
    }

    @Override // java.lang.Throwable
    public final synchronized Throwable fillInStackTrace() {
        return null;
    }
}
