package X;

import android.util.Pair;
import androidx.appcompat.widget.SwitchCompat;
import com.whatsapp.authentication.FingerprintBottomSheet;
import com.whatsapp.payments.ui.NoviPayHubSecurityActivity;

/* renamed from: X.5dy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119435dy extends AbstractC58672rA {
    public final /* synthetic */ Pair A00;
    public final /* synthetic */ SwitchCompat A01;
    public final /* synthetic */ FingerprintBottomSheet A02;
    public final /* synthetic */ NoviPayHubSecurityActivity A03;

    public C119435dy(Pair pair, SwitchCompat switchCompat, FingerprintBottomSheet fingerprintBottomSheet, NoviPayHubSecurityActivity noviPayHubSecurityActivity) {
        this.A03 = noviPayHubSecurityActivity;
        this.A00 = pair;
        this.A01 = switchCompat;
        this.A02 = fingerprintBottomSheet;
    }

    @Override // X.AnonymousClass4UT
    public void A00() {
        AnonymousClass61H.A03(this.A02);
    }

    @Override // X.AbstractC58672rA
    public void A02() {
        this.A02.A1B();
    }

    @Override // X.AbstractC58672rA
    public void A04(AnonymousClass02N r3, AnonymousClass21K r4) {
        this.A03.A08.A08(r3, r4, new byte[1]);
    }

    @Override // X.AbstractC58672rA
    public void A06(byte[] bArr) {
        NoviPayHubSecurityActivity noviPayHubSecurityActivity = this.A03;
        noviPayHubSecurityActivity.A2i(this.A00, this.A01, this.A02, noviPayHubSecurityActivity.A03);
    }
}
