package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0110000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S2100000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S2101000_I0;
import com.facebook.redex.RunnableBRunnable0Shape5S0100000_I0_5;
import com.whatsapp.conversationslist.ConversationsFragment;
import com.whatsapp.util.Log;

/* renamed from: X.1vT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C42591vT extends AbstractC42601vU {
    public int A00;
    public int A01;
    public long A02 = -1;
    public boolean A03;
    public final /* synthetic */ ConversationsFragment A04;

    public /* synthetic */ C42591vT(ConversationsFragment conversationsFragment) {
        this.A04 = conversationsFragment;
    }

    public final void A00(String str, String str2, int i, int i2, boolean z) {
        ActivityC000900k A0B;
        C14900mE r2;
        int i3;
        ConversationsFragment conversationsFragment = this.A04;
        if (conversationsFragment.A0c() && (A0B = conversationsFragment.A0B()) != null) {
            if (i == 1) {
                if (this.A00 != 1 && !A0B.isFinishing()) {
                    conversationsFragment.A0L.A0H(new RunnableBRunnable0Shape5S0100000_I0_5(this, 32));
                    this.A00 = 1;
                }
                if (!A0B.isFinishing()) {
                    r2 = conversationsFragment.A0L;
                    i3 = 4;
                    r2.A0H(new RunnableBRunnable0Shape0S2100000_I0(this, str2, str, i3));
                }
                if (z != this.A03) {
                }
            } else if (i != 2) {
                if (i == 3) {
                    AnonymousClass009.A05(str2);
                    if (this.A00 != 3 && !A0B.isFinishing()) {
                        conversationsFragment.A0L.A0H(new RunnableBRunnable0Shape5S0100000_I0_5(this, 31));
                        this.A00 = 3;
                    }
                    if (!A0B.isFinishing()) {
                        conversationsFragment.A0L.A0H(new RunnableBRunnable0Shape0S2101000_I0(this, str2, str, i2, 0));
                    }
                } else if (i == 4) {
                    AnonymousClass009.A05(str2);
                    if (this.A00 != 4) {
                        Log.i("conversations-gdrive-observer/set-message/show-indeterminate");
                        if (!A0B.isFinishing()) {
                            conversationsFragment.A0L.A0H(new RunnableBRunnable0Shape5S0100000_I0_5(this, 33));
                            this.A00 = 4;
                        }
                    }
                    if (!A0B.isFinishing()) {
                        r2 = conversationsFragment.A0L;
                        i3 = 5;
                        r2.A0H(new RunnableBRunnable0Shape0S2100000_I0(this, str2, str, i3));
                    }
                }
                if (z != this.A03 && !A0B.isFinishing()) {
                    conversationsFragment.A0L.A0H(new RunnableBRunnable0Shape0S0110000_I0(this, 6, z));
                    this.A03 = z;
                }
            } else {
                throw new IllegalStateException("unexpected state");
            }
        }
    }
}
