package X;

/* renamed from: X.1Ko  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C28091Ko extends AbstractC16110oT {
    public Integer A00;

    public C28091Ko() {
        super(3152, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        StringBuilder sb = new StringBuilder("WamGatedMessageReceived {");
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "chatGatedReason", obj);
        sb.append("}");
        return sb.toString();
    }
}
