package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.redex.IDxCreatorShape0S0000000_I1;

/* renamed from: X.0Ef  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02820Ef extends C02280As {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape0S0000000_I1(23);
    public int A00;
    public int A01;
    public int A02;

    public C02820Ef(Parcel parcel) {
        super(parcel);
        this.A02 = parcel.readInt();
        this.A01 = parcel.readInt();
        this.A00 = parcel.readInt();
    }

    public C02820Ef(Parcelable parcelable) {
        super(parcelable);
    }

    @Override // android.view.AbsSavedState, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.A02);
        parcel.writeInt(this.A01);
        parcel.writeInt(this.A00);
    }
}
