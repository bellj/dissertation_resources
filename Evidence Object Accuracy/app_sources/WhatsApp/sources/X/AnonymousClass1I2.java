package X;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

/* renamed from: X.1I2  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1I2 implements Iterable {
    public final AnonymousClass01H A00;
    public final ConcurrentLinkedQueue A01 = new ConcurrentLinkedQueue();

    public AnonymousClass1I2(AnonymousClass01H r2) {
        this.A00 = r2;
    }

    @Override // java.lang.Iterable
    public Iterator iterator() {
        return AnonymousClass1I4.unmodifiableIterator(AnonymousClass1I4.concat(((Set) this.A00.get()).iterator(), this.A01.iterator()));
    }
}
