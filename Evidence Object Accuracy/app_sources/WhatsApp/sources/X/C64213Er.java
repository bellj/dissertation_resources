package X;

import android.graphics.RectF;
import android.os.Handler;
import android.os.Vibrator;
import android.view.ViewGroup;
import com.whatsapp.R;
import java.util.Map;

/* renamed from: X.3Er  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64213Er {
    public float A00 = 0.0f;
    public float A01 = 0.0f;
    public RectF A02 = C12980iv.A0K();
    public Vibrator A03;
    public AbstractC454821u A04;
    public Map A05 = C12970iu.A11();
    public boolean A06 = false;
    public boolean A07 = false;
    public final float A08;
    public final Handler A09;
    public final ViewGroup A0A;

    public C64213Er(Handler handler, ViewGroup viewGroup, AnonymousClass01d r5) {
        this.A0A = viewGroup;
        this.A09 = handler;
        this.A08 = (float) viewGroup.getResources().getDimensionPixelSize(R.dimen.media_guideline_snapping_distance);
        this.A03 = r5.A0K();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003b, code lost:
        if (r1 == 2) goto L_0x003d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public float A00(android.graphics.PointF r11, X.AbstractC454821u r12, float r13) {
        /*
        // Method dump skipped, instructions count: 208
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C64213Er.A00(android.graphics.PointF, X.21u, float):float");
    }

    public final void A01() {
        AnonymousClass338 r2 = (AnonymousClass338) this.A05.get(C12970iu.A0h());
        if (r2 != null) {
            r2.A00 = 0.0f;
            r2.A03 = false;
        }
        this.A06 = false;
        this.A07 = false;
        this.A01 = 0.0f;
        this.A00 = 0.0f;
    }
}
