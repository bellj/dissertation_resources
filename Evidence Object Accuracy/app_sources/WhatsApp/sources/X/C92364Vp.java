package X;

import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;

/* renamed from: X.4Vp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92364Vp {
    public final AbstractC117035Xw A00;
    public final C93904aw A01;
    public final Collection A02;
    public final Set A03;

    public /* synthetic */ C92364Vp(AbstractC117035Xw r2, C93904aw r3, Collection collection, EnumSet enumSet) {
        C95094d8.A03(r3, "mappingProvider can not be null");
        C95094d8.A03(enumSet, "setOptions can not be null");
        C95094d8.A03(collection, "evaluationListeners can not be null");
        this.A00 = r2;
        this.A01 = r3;
        this.A03 = Collections.unmodifiableSet(enumSet);
        this.A02 = Collections.unmodifiableCollection(collection);
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C92364Vp r5 = (C92364Vp) obj;
            if (!(this.A00.getClass() == r5.A00.getClass() && this.A01.getClass() == r5.A01.getClass() && AnonymousClass08r.A00(this.A03, r5.A03))) {
                return false;
            }
        }
        return true;
    }
}
