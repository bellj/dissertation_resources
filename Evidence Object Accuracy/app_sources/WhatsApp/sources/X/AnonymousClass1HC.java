package X;

import android.view.View;
import android.view.ViewStub;

/* renamed from: X.1HC  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1HC {
    public static boolean A02;
    public View A00;
    public ViewStub A01;

    public AnonymousClass1HC(View view) {
        ViewStub viewStub;
        AnonymousClass009.A03(view);
        if (view instanceof ViewStub) {
            this.A01 = (ViewStub) view;
        } else {
            this.A00 = view;
        }
        if (A02) {
            View view2 = this.A00;
            if (view2 == null && (viewStub = this.A01) != null) {
                view2 = viewStub.inflate();
                this.A00 = view2;
                this.A01 = null;
            }
            AnonymousClass009.A06(view2, "View must be inflated in ViewStubHolder.getView()");
        }
    }

    public void A00(int i) {
        ViewStub viewStub;
        View view = this.A00;
        if (i != 8) {
            if (view == null && (viewStub = this.A01) != null) {
                view = viewStub.inflate();
                this.A00 = view;
                this.A01 = null;
            }
            AnonymousClass009.A06(view, "View must be inflated in ViewStubHolder.getView()");
        } else if (view == null) {
            return;
        }
        view.setVisibility(i);
    }
}
