package X;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

/* renamed from: X.4e9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C95664e9 {
    public static float A00(Parcel parcel, int i) {
        A0F(parcel, i, 4);
        return parcel.readFloat();
    }

    public static int A01(Parcel parcel) {
        int readInt = parcel.readInt();
        int A03 = A03(parcel, readInt);
        int dataPosition = parcel.dataPosition();
        if (((char) readInt) != 20293) {
            throw new C113235Gs(parcel, C12960it.A0c(String.valueOf(Integer.toHexString(readInt)), "Expected object header. Got 0x"));
        }
        int i = A03 + dataPosition;
        if (i >= dataPosition && i <= parcel.dataSize()) {
            return i;
        }
        StringBuilder A0t = C12980iv.A0t(54);
        A0t.append("Size read is invalid start=");
        A0t.append(dataPosition);
        throw new C113235Gs(parcel, C12960it.A0e(" end=", A0t, i));
    }

    public static int A02(Parcel parcel, int i) {
        A0F(parcel, i, 4);
        return parcel.readInt();
    }

    public static int A03(Parcel parcel, int i) {
        return (i & -65536) != -65536 ? (char) (i >> 16) : parcel.readInt();
    }

    public static long A04(Parcel parcel, int i) {
        A0F(parcel, i, 8);
        return parcel.readLong();
    }

    public static Bundle A05(Parcel parcel, int i) {
        int A03 = A03(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (A03 == 0) {
            return null;
        }
        Bundle readBundle = parcel.readBundle();
        parcel.setDataPosition(dataPosition + A03);
        return readBundle;
    }

    public static IBinder A06(Parcel parcel, int i) {
        int A03 = A03(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (A03 == 0) {
            return null;
        }
        IBinder readStrongBinder = parcel.readStrongBinder();
        parcel.setDataPosition(dataPosition + A03);
        return readStrongBinder;
    }

    public static Parcelable A07(Parcel parcel, Parcelable.Creator creator, int i) {
        int A03 = A03(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (A03 == 0) {
            return null;
        }
        Parcelable parcelable = (Parcelable) creator.createFromParcel(parcel);
        parcel.setDataPosition(dataPosition + A03);
        return parcelable;
    }

    public static String A08(Parcel parcel, int i) {
        int A03 = A03(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (A03 == 0) {
            return null;
        }
        String readString = parcel.readString();
        parcel.setDataPosition(dataPosition + A03);
        return readString;
    }

    public static String A09(Parcel parcel, String str, int i, int i2, int i3) {
        if (i == i2) {
            return A08(parcel, i3);
        }
        A0D(parcel, i3);
        return str;
    }

    public static ArrayList A0A(Parcel parcel, int i) {
        int A03 = A03(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (A03 == 0) {
            return null;
        }
        ArrayList<String> createStringArrayList = parcel.createStringArrayList();
        parcel.setDataPosition(dataPosition + A03);
        return createStringArrayList;
    }

    public static ArrayList A0B(Parcel parcel, Parcelable.Creator creator, int i) {
        int A03 = A03(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (A03 == 0) {
            return null;
        }
        ArrayList createTypedArrayList = parcel.createTypedArrayList(creator);
        parcel.setDataPosition(dataPosition + A03);
        return createTypedArrayList;
    }

    public static void A0C(Parcel parcel, int i) {
        if (parcel.dataPosition() != i) {
            throw new C113235Gs(parcel, C12960it.A0e("Overread allowed size end=", C12980iv.A0t(37), i));
        }
    }

    public static void A0D(Parcel parcel, int i) {
        parcel.setDataPosition(parcel.dataPosition() + A03(parcel, i));
    }

    public static void A0E(Parcel parcel, int i, int i2) {
        if (i != i2) {
            String hexString = Integer.toHexString(i);
            StringBuilder A0t = C12980iv.A0t(C12970iu.A07(hexString) + 46);
            A0t.append("Expected size ");
            A0t.append(i2);
            A0t.append(" got ");
            A0t.append(i);
            A0t.append(" (0x");
            A0t.append(hexString);
            throw new C113235Gs(parcel, C12960it.A0d(")", A0t));
        }
    }

    public static void A0F(Parcel parcel, int i, int i2) {
        int A03 = A03(parcel, i);
        if (A03 != i2) {
            String hexString = Integer.toHexString(A03);
            StringBuilder A0t = C12980iv.A0t(C12970iu.A07(hexString) + 46);
            A0t.append("Expected size ");
            A0t.append(i2);
            A0t.append(" got ");
            A0t.append(A03);
            A0t.append(" (0x");
            A0t.append(hexString);
            throw new C113235Gs(parcel, C12960it.A0d(")", A0t));
        }
    }

    public static boolean A0G(Parcel parcel, int i) {
        return C12960it.A1S(A02(parcel, i));
    }

    public static boolean A0H(Parcel parcel, int i, int i2, int i3, boolean z) {
        if (i == i2) {
            return A0G(parcel, i3);
        }
        A0D(parcel, i3);
        return z;
    }

    public static byte[] A0I(Parcel parcel, int i) {
        int A03 = A03(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (A03 == 0) {
            return null;
        }
        byte[] createByteArray = parcel.createByteArray();
        parcel.setDataPosition(dataPosition + A03);
        return createByteArray;
    }

    public static int[] A0J(Parcel parcel, int i) {
        int A03 = A03(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (A03 == 0) {
            return null;
        }
        int[] createIntArray = parcel.createIntArray();
        parcel.setDataPosition(dataPosition + A03);
        return createIntArray;
    }

    public static Object[] A0K(Parcel parcel, Parcelable.Creator creator, int i) {
        int A03 = A03(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (A03 == 0) {
            return null;
        }
        Object[] createTypedArray = parcel.createTypedArray(creator);
        parcel.setDataPosition(dataPosition + A03);
        return createTypedArray;
    }

    public static String[] A0L(Parcel parcel, int i) {
        int A03 = A03(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (A03 == 0) {
            return null;
        }
        String[] createStringArray = parcel.createStringArray();
        parcel.setDataPosition(dataPosition + A03);
        return createStringArray;
    }

    public static byte[][] A0M(Parcel parcel, int i) {
        int A03 = A03(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (A03 == 0) {
            return null;
        }
        int readInt = parcel.readInt();
        byte[][] bArr = new byte[readInt];
        for (int i2 = 0; i2 < readInt; i2++) {
            bArr[i2] = parcel.createByteArray();
        }
        parcel.setDataPosition(dataPosition + A03);
        return bArr;
    }
}
