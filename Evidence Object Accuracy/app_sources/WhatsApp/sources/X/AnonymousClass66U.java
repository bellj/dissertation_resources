package X;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.maps.internal.IGoogleMapDelegate;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.whatsapp.payments.ui.NoviWithdrawCashStoreLocatorActivity;

/* renamed from: X.66U  reason: invalid class name */
/* loaded from: classes4.dex */
public final /* synthetic */ class AnonymousClass66U implements AbstractC115755Su {
    public final /* synthetic */ NoviWithdrawCashStoreLocatorActivity A00;

    public /* synthetic */ AnonymousClass66U(NoviWithdrawCashStoreLocatorActivity noviWithdrawCashStoreLocatorActivity) {
        this.A00 = noviWithdrawCashStoreLocatorActivity;
    }

    @Override // X.AbstractC115755Su
    public final void ASP(C35961j4 r10) {
        C130645zk r1;
        LatLngBounds latLngBounds;
        C130575zd r8;
        NoviWithdrawCashStoreLocatorActivity noviWithdrawCashStoreLocatorActivity = this.A00;
        if (noviWithdrawCashStoreLocatorActivity.A04 == null && r10 != null) {
            noviWithdrawCashStoreLocatorActivity.A04 = r10;
            AnonymousClass009.A05(r10);
            noviWithdrawCashStoreLocatorActivity.A0A = new AnonymousClass3FV(r10);
            r10.A0M(false);
            noviWithdrawCashStoreLocatorActivity.A04.A0K(true);
            if (noviWithdrawCashStoreLocatorActivity.A04 != null && noviWithdrawCashStoreLocatorActivity.A09.A03()) {
                noviWithdrawCashStoreLocatorActivity.A04.A0L(true);
                noviWithdrawCashStoreLocatorActivity.A04.A01().A00();
            }
            noviWithdrawCashStoreLocatorActivity.A04.A0J(new C56462kv("[ { featureType: \"poi\", elementType: \"labels\", stylers: [ { visibility: \"off\" } ] } ]"));
            C35961j4 r0 = noviWithdrawCashStoreLocatorActivity.A04;
            AnonymousClass66S r12 = new AbstractC115735Ss() { // from class: X.66S
                @Override // X.AbstractC115735Ss
                public final void ASN() {
                    AnonymousClass60Y.A02(NoviWithdrawCashStoreLocatorActivity.this.A0I, "LOCATION_MAP_LOADED", "WITHDRAW_MONEY", "SELECT_LOCATION", "SCREEN");
                }
            };
            try {
                IGoogleMapDelegate iGoogleMapDelegate = r0.A01;
                BinderC79863rK r02 = new BinderC79863rK(r12);
                C65873Li r2 = (C65873Li) iGoogleMapDelegate;
                Parcel A01 = r2.A01();
                C65183In.A00(r02, A01);
                r2.A03(42, A01);
                noviWithdrawCashStoreLocatorActivity.A04.A0I(new AbstractC115745St() { // from class: X.66T
                    @Override // X.AbstractC115745St
                    public final boolean ASR(C36311jg r7) {
                        NoviWithdrawCashStoreLocatorActivity noviWithdrawCashStoreLocatorActivity2 = NoviWithdrawCashStoreLocatorActivity.this;
                        C128365vz r13 = new AnonymousClass610("SELECT_STORE_PIN_CLICK", "WITHDRAW_MONEY", "SELECT_LOCATION", "PIN").A00;
                        C1316163l r03 = (C1316163l) r7.A01();
                        if (r03 != null) {
                            r13.A0h = r03.A04;
                        }
                        noviWithdrawCashStoreLocatorActivity2.A0I.A05(r13);
                        C36311jg r14 = noviWithdrawCashStoreLocatorActivity2.A06;
                        if (r14 != null) {
                            r14.A05(noviWithdrawCashStoreLocatorActivity2.A05);
                        }
                        try {
                            AnonymousClass5YB r22 = C36321jh.A00;
                            C13020j0.A02(r22, "IBitmapDescriptorFactory is not initialized");
                            C65873Li r23 = (C65873Li) r22;
                            Parcel A02 = r23.A02(4, r23.A01());
                            IObjectWrapper A012 = AbstractBinderC79573qo.A01(A02.readStrongBinder());
                            A02.recycle();
                            r7.A05(new C36331ji(A012));
                            noviWithdrawCashStoreLocatorActivity2.A06 = r7;
                            return false;
                        } catch (RemoteException e) {
                            throw new C113245Gt(e);
                        }
                    }
                });
                noviWithdrawCashStoreLocatorActivity.A04.A0G(new AbstractC115715Sq() { // from class: X.66Q
                    @Override // X.AbstractC115715Sq
                    public final void ARO(C36311jg r5) {
                        NoviWithdrawCashStoreLocatorActivity noviWithdrawCashStoreLocatorActivity2 = NoviWithdrawCashStoreLocatorActivity.this;
                        C1316163l r22 = (C1316163l) r5.A01();
                        if (r22 != null) {
                            noviWithdrawCashStoreLocatorActivity2.A0J.A04().A00(new AnonymousClass6EY(r22, noviWithdrawCashStoreLocatorActivity2));
                        }
                    }
                });
                noviWithdrawCashStoreLocatorActivity.A04.A0H(new AbstractC115725Sr() { // from class: X.66R
                    @Override // X.AbstractC115725Sr
                    public final void ASM(LatLng latLng) {
                        NoviWithdrawCashStoreLocatorActivity noviWithdrawCashStoreLocatorActivity2 = NoviWithdrawCashStoreLocatorActivity.this;
                        C36311jg r13 = noviWithdrawCashStoreLocatorActivity2.A06;
                        if (r13 != null) {
                            r13.A05(noviWithdrawCashStoreLocatorActivity2.A05);
                        }
                    }
                });
                C127645up r03 = noviWithdrawCashStoreLocatorActivity.A0J.A02;
                if (r03 != null && (r1 = r03.A00) != null) {
                    C130575zd r04 = r1.A03;
                    if (r04 == null || (r8 = r1.A02) == null) {
                        latLngBounds = null;
                    } else {
                        latLngBounds = new LatLngBounds(new LatLng(r04.A00, r04.A01), new LatLng(r8.A00, r8.A01));
                    }
                    noviWithdrawCashStoreLocatorActivity.A0A.A02(((ActivityC13810kN) noviWithdrawCashStoreLocatorActivity).A00.getContext(), latLngBounds, false);
                }
            } catch (RemoteException e) {
                throw new C113245Gt(e);
            }
        }
    }
}
