package X;

import android.content.Context;
import com.whatsapp.contact.picker.invite.InviteNonWhatsAppContactPickerActivity;

/* renamed from: X.4q2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C102994q2 implements AbstractC009204q {
    public final /* synthetic */ InviteNonWhatsAppContactPickerActivity A00;

    public C102994q2(InviteNonWhatsAppContactPickerActivity inviteNonWhatsAppContactPickerActivity) {
        this.A00 = inviteNonWhatsAppContactPickerActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
