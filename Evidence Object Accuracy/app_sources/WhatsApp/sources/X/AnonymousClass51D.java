package X;

import androidx.viewpager.widget.ViewPager;

/* renamed from: X.51D  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass51D implements AbstractC467627l {
    public final ViewPager A00;

    @Override // X.AbstractC467727m
    public void AXN(AnonymousClass3FN r1) {
    }

    public AnonymousClass51D(ViewPager viewPager) {
        this.A00 = viewPager;
    }

    @Override // X.AbstractC467727m
    public void AXO(AnonymousClass3FN r3) {
        this.A00.setCurrentItem(r3.A00);
    }
}
