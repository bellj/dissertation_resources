package X;

import android.media.Image;
import android.media.ImageReader;

/* renamed from: X.63K  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass63K implements ImageReader.OnImageAvailableListener {
    public final /* synthetic */ AnonymousClass61B A00;

    public AnonymousClass63K(AnonymousClass61B r1) {
        this.A00 = r1;
    }

    @Override // android.media.ImageReader.OnImageAvailableListener
    public void onImageAvailable(ImageReader imageReader) {
        AnonymousClass61B r1 = this.A00;
        Image image = r1.A00;
        if (image != null) {
            image.close();
        }
        r1.A00 = imageReader.acquireNextImage();
        AnonymousClass61B.A02(r1);
    }
}
