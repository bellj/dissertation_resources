package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.msys.mci.DefaultCrypto;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.1sR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C40851sR extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C40851sR A0O;
    public static volatile AnonymousClass255 A0P;
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public long A05;
    public long A06;
    public AbstractC27881Jp A07;
    public AbstractC27881Jp A08;
    public AbstractC27881Jp A09;
    public AbstractC27881Jp A0A;
    public AbstractC27881Jp A0B;
    public AbstractC27881Jp A0C;
    public AbstractC27881Jp A0D;
    public AnonymousClass1K6 A0E;
    public C43261wh A0F;
    public String A0G;
    public String A0H;
    public String A0I = "";
    public String A0J;
    public String A0K;
    public String A0L = "";
    public boolean A0M;
    public boolean A0N;

    static {
        C40851sR r0 = new C40851sR();
        A0O = r0;
        r0.A0W();
    }

    public C40851sR() {
        AbstractC27881Jp r1 = AbstractC27881Jp.A01;
        this.A08 = r1;
        this.A0A = r1;
        this.A0G = "";
        this.A07 = r1;
        this.A0E = AnonymousClass277.A01;
        this.A0H = "";
        this.A09 = r1;
        this.A0B = r1;
        this.A0K = "";
        this.A0D = r1;
        this.A0C = r1;
        this.A0J = "";
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r16, Object obj, Object obj2) {
        C81603uH r2;
        switch (r16.ordinal()) {
            case 0:
                return A0O;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                C40851sR r1 = (C40851sR) obj2;
                int i = this.A00;
                boolean z = true;
                if ((i & 1) != 1) {
                    z = false;
                }
                String str = this.A0L;
                int i2 = r1.A00;
                boolean z2 = true;
                if ((i2 & 1) != 1) {
                    z2 = false;
                }
                this.A0L = r8.Afy(str, r1.A0L, z, z2);
                boolean z3 = false;
                if ((i & 2) == 2) {
                    z3 = true;
                }
                String str2 = this.A0I;
                boolean z4 = false;
                if ((i2 & 2) == 2) {
                    z4 = true;
                }
                this.A0I = r8.Afy(str2, r1.A0I, z3, z4);
                boolean z5 = false;
                if ((i & 4) == 4) {
                    z5 = true;
                }
                AbstractC27881Jp r3 = this.A08;
                boolean z6 = false;
                if ((i2 & 4) == 4) {
                    z6 = true;
                }
                this.A08 = r8.Afm(r3, r1.A08, z5, z6);
                int i3 = this.A00;
                boolean z7 = false;
                if ((i3 & 8) == 8) {
                    z7 = true;
                }
                long j = this.A05;
                int i4 = r1.A00;
                boolean z8 = false;
                if ((i4 & 8) == 8) {
                    z8 = true;
                }
                this.A05 = r8.Afs(j, r1.A05, z7, z8);
                boolean z9 = false;
                if ((i3 & 16) == 16) {
                    z9 = true;
                }
                int i5 = this.A03;
                boolean z10 = false;
                if ((i4 & 16) == 16) {
                    z10 = true;
                }
                this.A03 = r8.Afp(i5, r1.A03, z9, z10);
                boolean z11 = false;
                if ((i3 & 32) == 32) {
                    z11 = true;
                }
                AbstractC27881Jp r32 = this.A0A;
                boolean z12 = false;
                if ((i4 & 32) == 32) {
                    z12 = true;
                }
                this.A0A = r8.Afm(r32, r1.A0A, z11, z12);
                int i6 = this.A00;
                boolean z13 = false;
                if ((i6 & 64) == 64) {
                    z13 = true;
                }
                String str3 = this.A0G;
                int i7 = r1.A00;
                boolean z14 = false;
                if ((i7 & 64) == 64) {
                    z14 = true;
                }
                this.A0G = r8.Afy(str3, r1.A0G, z13, z14);
                boolean z15 = false;
                if ((i6 & 128) == 128) {
                    z15 = true;
                }
                boolean z16 = this.A0M;
                boolean z17 = false;
                if ((i7 & 128) == 128) {
                    z17 = true;
                }
                this.A0M = r8.Afl(z15, z16, z17, r1.A0M);
                boolean z18 = false;
                if ((i6 & 256) == 256) {
                    z18 = true;
                }
                int i8 = this.A02;
                boolean z19 = false;
                if ((i7 & 256) == 256) {
                    z19 = true;
                }
                this.A02 = r8.Afp(i8, r1.A02, z18, z19);
                boolean z20 = false;
                if ((i6 & 512) == 512) {
                    z20 = true;
                }
                int i9 = this.A04;
                boolean z21 = false;
                if ((i7 & 512) == 512) {
                    z21 = true;
                }
                this.A04 = r8.Afp(i9, r1.A04, z20, z21);
                boolean z22 = false;
                if ((i6 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
                    z22 = true;
                }
                AbstractC27881Jp r33 = this.A07;
                boolean z23 = false;
                if ((i7 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
                    z23 = true;
                }
                this.A07 = r8.Afm(r33, r1.A07, z22, z23);
                this.A0E = r8.Afr(this.A0E, r1.A0E);
                int i10 = this.A00;
                boolean z24 = false;
                if ((i10 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
                    z24 = true;
                }
                String str4 = this.A0H;
                int i11 = r1.A00;
                boolean z25 = false;
                if ((i11 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
                    z25 = true;
                }
                this.A0H = r8.Afy(str4, r1.A0H, z24, z25);
                boolean z26 = false;
                if ((i10 & 4096) == 4096) {
                    z26 = true;
                }
                long j2 = this.A06;
                boolean z27 = false;
                if ((i11 & 4096) == 4096) {
                    z27 = true;
                }
                this.A06 = r8.Afs(j2, r1.A06, z26, z27);
                boolean z28 = false;
                if ((i10 & DefaultCrypto.BUFFER_SIZE) == 8192) {
                    z28 = true;
                }
                AbstractC27881Jp r34 = this.A09;
                boolean z29 = false;
                if ((i11 & DefaultCrypto.BUFFER_SIZE) == 8192) {
                    z29 = true;
                }
                this.A09 = r8.Afm(r34, r1.A09, z28, z29);
                this.A0F = (C43261wh) r8.Aft(this.A0F, r1.A0F);
                boolean z30 = false;
                if ((this.A00 & 32768) == 32768) {
                    z30 = true;
                }
                AbstractC27881Jp r4 = this.A0B;
                boolean z31 = false;
                if ((r1.A00 & 32768) == 32768) {
                    z31 = true;
                }
                this.A0B = r8.Afm(r4, r1.A0B, z30, z31);
                int i12 = this.A00;
                boolean z32 = false;
                if ((i12 & 65536) == 65536) {
                    z32 = true;
                }
                int i13 = this.A01;
                int i14 = r1.A00;
                boolean z33 = false;
                if ((i14 & 65536) == 65536) {
                    z33 = true;
                }
                this.A01 = r8.Afp(i13, r1.A01, z32, z33);
                boolean z34 = false;
                if ((i12 & C25981Bo.A0F) == 131072) {
                    z34 = true;
                }
                boolean z35 = this.A0N;
                boolean z36 = false;
                if ((i14 & C25981Bo.A0F) == 131072) {
                    z36 = true;
                }
                this.A0N = r8.Afl(z34, z35, z36, r1.A0N);
                boolean z37 = false;
                if ((i12 & 262144) == 262144) {
                    z37 = true;
                }
                String str5 = this.A0K;
                boolean z38 = false;
                if ((i14 & 262144) == 262144) {
                    z38 = true;
                }
                this.A0K = r8.Afy(str5, r1.A0K, z37, z38);
                boolean z39 = false;
                if ((i12 & 524288) == 524288) {
                    z39 = true;
                }
                AbstractC27881Jp r35 = this.A0D;
                boolean z40 = false;
                if ((i14 & 524288) == 524288) {
                    z40 = true;
                }
                this.A0D = r8.Afm(r35, r1.A0D, z39, z40);
                boolean z41 = false;
                if ((this.A00 & 1048576) == 1048576) {
                    z41 = true;
                }
                AbstractC27881Jp r42 = this.A0C;
                boolean z42 = false;
                if ((r1.A00 & 1048576) == 1048576) {
                    z42 = true;
                }
                this.A0C = r8.Afm(r42, r1.A0C, z41, z42);
                int i15 = this.A00;
                boolean z43 = false;
                if ((i15 & 2097152) == 2097152) {
                    z43 = true;
                }
                String str6 = this.A0J;
                int i16 = r1.A00;
                boolean z44 = false;
                if ((i16 & 2097152) == 2097152) {
                    z44 = true;
                }
                this.A0J = r8.Afy(str6, r1.A0J, z43, z44);
                if (r8 == C463025i.A00) {
                    this.A00 = i15 | i16;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                AnonymousClass254 r12 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        try {
                            int A03 = r82.A03();
                            switch (A03) {
                                case 0:
                                    break;
                                case 10:
                                    String A0A = r82.A0A();
                                    this.A00 = 1 | this.A00;
                                    this.A0L = A0A;
                                    break;
                                case 18:
                                    String A0A2 = r82.A0A();
                                    this.A00 |= 2;
                                    this.A0I = A0A2;
                                    break;
                                case 26:
                                    this.A00 |= 4;
                                    this.A08 = r82.A08();
                                    break;
                                case 32:
                                    this.A00 |= 8;
                                    this.A05 = r82.A06();
                                    break;
                                case 40:
                                    this.A00 |= 16;
                                    this.A03 = r82.A02();
                                    break;
                                case SearchActionVerificationClientService.TIME_TO_SLEEP_IN_MS /* 50 */:
                                    this.A00 |= 32;
                                    this.A0A = r82.A08();
                                    break;
                                case 58:
                                    String A0A3 = r82.A0A();
                                    this.A00 |= 64;
                                    this.A0G = A0A3;
                                    break;
                                case 64:
                                    this.A00 |= 128;
                                    this.A0M = r82.A0F();
                                    break;
                                case C43951xu.A02 /* 72 */:
                                    this.A00 |= 256;
                                    this.A02 = r82.A02();
                                    break;
                                case 80:
                                    this.A00 |= 512;
                                    this.A04 = r82.A02();
                                    break;
                                case 90:
                                    this.A00 |= EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
                                    this.A07 = r82.A08();
                                    break;
                                case 98:
                                    AnonymousClass1K6 r22 = this.A0E;
                                    if (!((AnonymousClass1K7) r22).A00) {
                                        r22 = AbstractC27091Fz.A0G(r22);
                                        this.A0E = r22;
                                    }
                                    r22.add((C57262mk) r82.A09(r12, C57262mk.A04.A0U()));
                                    break;
                                case 106:
                                    String A0A4 = r82.A0A();
                                    this.A00 |= EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
                                    this.A0H = A0A4;
                                    break;
                                case 112:
                                    this.A00 |= 4096;
                                    this.A06 = r82.A06();
                                    break;
                                case 130:
                                    this.A00 |= DefaultCrypto.BUFFER_SIZE;
                                    this.A09 = r82.A08();
                                    break;
                                case 138:
                                    if ((this.A00 & 16384) == 16384) {
                                        r2 = (C81603uH) this.A0F.A0T();
                                    } else {
                                        r2 = null;
                                    }
                                    C43261wh r0 = (C43261wh) r82.A09(r12, C43261wh.A0O.A0U());
                                    this.A0F = r0;
                                    if (r2 != null) {
                                        r2.A04(r0);
                                        this.A0F = (C43261wh) r2.A01();
                                    }
                                    this.A00 |= 16384;
                                    break;
                                case 146:
                                    this.A00 |= 32768;
                                    this.A0B = r82.A08();
                                    break;
                                case 152:
                                    int A02 = r82.A02();
                                    if (A02 != 0 && A02 != 1 && A02 != 2) {
                                        super.A0X(19, A02);
                                        break;
                                    } else {
                                        this.A00 |= 65536;
                                        this.A01 = A02;
                                        break;
                                    }
                                    break;
                                case 160:
                                    this.A00 |= C25981Bo.A0F;
                                    this.A0N = r82.A0F();
                                    break;
                                case 170:
                                    String A0A5 = r82.A0A();
                                    this.A00 |= 262144;
                                    this.A0K = A0A5;
                                    break;
                                case 178:
                                    this.A00 |= 524288;
                                    this.A0D = r82.A08();
                                    break;
                                case 186:
                                    this.A00 |= 1048576;
                                    this.A0C = r82.A08();
                                    break;
                                case 194:
                                    String A0A6 = r82.A0A();
                                    this.A00 |= 2097152;
                                    this.A0J = A0A6;
                                    break;
                                default:
                                    if (A0a(r82, A03)) {
                                        break;
                                    } else {
                                        break;
                                    }
                            }
                        } catch (C28971Pt e) {
                            e.unfinishedMessage = this;
                            throw new RuntimeException(e);
                        }
                    } catch (IOException e2) {
                        C28971Pt r13 = new C28971Pt(e2.getMessage());
                        r13.unfinishedMessage = this;
                        throw new RuntimeException(r13);
                    }
                }
                break;
            case 3:
                ((AnonymousClass1K7) this.A0E).A00 = false;
                return null;
            case 4:
                return new C40851sR();
            case 5:
                return new C82373vW();
            case 6:
                break;
            case 7:
                if (A0P == null) {
                    synchronized (C40851sR.class) {
                        if (A0P == null) {
                            A0P = new AnonymousClass255(A0O);
                        }
                    }
                }
                return A0P;
            default:
                throw new UnsupportedOperationException();
        }
        return A0O;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i;
        int i2 = ((AbstractC27091Fz) this).A00;
        if (i2 != -1) {
            return i2;
        }
        if ((this.A00 & 1) == 1) {
            i = CodedOutputStream.A07(1, this.A0L) + 0;
        } else {
            i = 0;
        }
        if ((this.A00 & 2) == 2) {
            i += CodedOutputStream.A07(2, this.A0I);
        }
        int i3 = this.A00;
        if ((i3 & 4) == 4) {
            i += CodedOutputStream.A09(this.A08, 3);
        }
        if ((i3 & 8) == 8) {
            i += CodedOutputStream.A06(4, this.A05);
        }
        if ((i3 & 16) == 16) {
            i += CodedOutputStream.A04(5, this.A03);
        }
        if ((i3 & 32) == 32) {
            i += CodedOutputStream.A09(this.A0A, 6);
        }
        if ((i3 & 64) == 64) {
            i += CodedOutputStream.A07(7, this.A0G);
        }
        int i4 = this.A00;
        if ((i4 & 128) == 128) {
            i += CodedOutputStream.A00(8);
        }
        if ((i4 & 256) == 256) {
            i += CodedOutputStream.A04(9, this.A02);
        }
        if ((i4 & 512) == 512) {
            i += CodedOutputStream.A04(10, this.A04);
        }
        if ((i4 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            i += CodedOutputStream.A09(this.A07, 11);
        }
        for (int i5 = 0; i5 < this.A0E.size(); i5++) {
            i += CodedOutputStream.A0A((AnonymousClass1G1) this.A0E.get(i5), 12);
        }
        if ((this.A00 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            i += CodedOutputStream.A07(13, this.A0H);
        }
        int i6 = this.A00;
        if ((i6 & 4096) == 4096) {
            i += CodedOutputStream.A05(14, this.A06);
        }
        if ((i6 & DefaultCrypto.BUFFER_SIZE) == 8192) {
            i += CodedOutputStream.A09(this.A09, 16);
        }
        if ((i6 & 16384) == 16384) {
            C43261wh r0 = this.A0F;
            if (r0 == null) {
                r0 = C43261wh.A0O;
            }
            i += CodedOutputStream.A0A(r0, 17);
        }
        int i7 = this.A00;
        if ((i7 & 32768) == 32768) {
            i += CodedOutputStream.A09(this.A0B, 18);
        }
        if ((i7 & 65536) == 65536) {
            i += CodedOutputStream.A02(19, this.A01);
        }
        if ((i7 & C25981Bo.A0F) == 131072) {
            i += CodedOutputStream.A00(20);
        }
        if ((i7 & 262144) == 262144) {
            i += CodedOutputStream.A07(21, this.A0K);
        }
        int i8 = this.A00;
        if ((i8 & 524288) == 524288) {
            i += CodedOutputStream.A09(this.A0D, 22);
        }
        if ((i8 & 1048576) == 1048576) {
            i += CodedOutputStream.A09(this.A0C, 23);
        }
        if ((i8 & 2097152) == 2097152) {
            i += CodedOutputStream.A07(24, this.A0J);
        }
        int A00 = i + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A0L);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0I(2, this.A0I);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0K(this.A08, 3);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0H(4, this.A05);
        }
        if ((this.A00 & 16) == 16) {
            codedOutputStream.A0F(5, this.A03);
        }
        if ((this.A00 & 32) == 32) {
            codedOutputStream.A0K(this.A0A, 6);
        }
        if ((this.A00 & 64) == 64) {
            codedOutputStream.A0I(7, this.A0G);
        }
        if ((this.A00 & 128) == 128) {
            codedOutputStream.A0J(8, this.A0M);
        }
        if ((this.A00 & 256) == 256) {
            codedOutputStream.A0F(9, this.A02);
        }
        if ((this.A00 & 512) == 512) {
            codedOutputStream.A0F(10, this.A04);
        }
        if ((this.A00 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            codedOutputStream.A0K(this.A07, 11);
        }
        for (int i = 0; i < this.A0E.size(); i++) {
            codedOutputStream.A0L((AnonymousClass1G1) this.A0E.get(i), 12);
        }
        if ((this.A00 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            codedOutputStream.A0I(13, this.A0H);
        }
        if ((this.A00 & 4096) == 4096) {
            codedOutputStream.A0H(14, this.A06);
        }
        if ((this.A00 & DefaultCrypto.BUFFER_SIZE) == 8192) {
            codedOutputStream.A0K(this.A09, 16);
        }
        if ((this.A00 & 16384) == 16384) {
            C43261wh r0 = this.A0F;
            if (r0 == null) {
                r0 = C43261wh.A0O;
            }
            codedOutputStream.A0L(r0, 17);
        }
        if ((this.A00 & 32768) == 32768) {
            codedOutputStream.A0K(this.A0B, 18);
        }
        if ((this.A00 & 65536) == 65536) {
            codedOutputStream.A0E(19, this.A01);
        }
        if ((this.A00 & C25981Bo.A0F) == 131072) {
            codedOutputStream.A0J(20, this.A0N);
        }
        if ((this.A00 & 262144) == 262144) {
            codedOutputStream.A0I(21, this.A0K);
        }
        if ((this.A00 & 524288) == 524288) {
            codedOutputStream.A0K(this.A0D, 22);
        }
        if ((this.A00 & 1048576) == 1048576) {
            codedOutputStream.A0K(this.A0C, 23);
        }
        if ((this.A00 & 2097152) == 2097152) {
            codedOutputStream.A0I(24, this.A0J);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
