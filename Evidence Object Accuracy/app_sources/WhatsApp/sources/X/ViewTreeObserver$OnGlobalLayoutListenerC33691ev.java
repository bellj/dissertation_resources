package X;

import X.AbstractC001200n;
import X.AnonymousClass074;
import X.ViewTreeObserver$OnGlobalLayoutListenerC33691ev;
import android.os.Build;
import android.view.View;
import android.view.ViewTreeObserver;
import com.whatsapp.snackbar.WaSnackbar$$ExternalSyntheticLambda0;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1ev  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC33691ev implements ViewTreeObserver.OnGlobalLayoutListener {
    public final AbstractC009904y A00;
    public final AnonymousClass054 A01;
    public final AbstractC80733sr A02 = new C80713sp(this);
    public final C34271fr A03;
    public final AnonymousClass01d A04;
    public final List A05;
    public final boolean A06;

    public ViewTreeObserver$OnGlobalLayoutListenerC33691ev(AbstractC001200n r6, C34271fr r7, AnonymousClass01d r8, List list, boolean z) {
        WaSnackbar$$ExternalSyntheticLambda0 waSnackbar$$ExternalSyntheticLambda0 = new AnonymousClass054() { // from class: com.whatsapp.snackbar.WaSnackbar$$ExternalSyntheticLambda0
            @Override // X.AnonymousClass054
            public final void AWQ(AnonymousClass074 r3, AbstractC001200n r4) {
                ViewTreeObserver$OnGlobalLayoutListenerC33691ev r1 = ViewTreeObserver$OnGlobalLayoutListenerC33691ev.this;
                if (r3.equals(AnonymousClass074.ON_STOP)) {
                    r1.A00();
                }
            }
        };
        this.A01 = waSnackbar$$ExternalSyntheticLambda0;
        AbstractC009904y ADr = r6.ADr();
        this.A00 = ADr;
        AnonymousClass009.A0F(((C009804x) ADr).A02 != AnonymousClass05I.DESTROYED);
        this.A03 = r7;
        this.A04 = r8;
        this.A05 = list;
        this.A06 = z;
        ADr.A00(waSnackbar$$ExternalSyntheticLambda0);
    }

    public void A00() {
        this.A03.A04(3);
        this.A00.A01(this.A01);
    }

    public void A01() {
        if (((C009804x) this.A00).A02.compareTo(AnonymousClass05I.STARTED) >= 0) {
            C34271fr r2 = this.A03;
            r2.A05.getViewTreeObserver().addOnGlobalLayoutListener(this);
            AbstractC80733sr r1 = this.A02;
            if (r1 != null) {
                List list = ((AbstractC15160mf) r2).A01;
                if (list == null) {
                    list = new ArrayList();
                    ((AbstractC15160mf) r2).A01 = list;
                }
                list.add(r1);
            }
            r2.A03();
        }
    }

    public final void A02(int i) {
        for (View view : this.A05) {
            if (view != null) {
                view.animate().translationY((float) i).setDuration(250).setInterpolator(new C015907n()).start();
            }
        }
    }

    public void A03(Runnable runnable) {
        C34271fr r2 = this.A03;
        C80723sq r1 = new C80723sq(this, runnable);
        List list = ((AbstractC15160mf) r2).A01;
        if (list == null) {
            list = new ArrayList();
            ((AbstractC15160mf) r2).A01 = list;
        }
        list.add(r1);
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        C34291fu r3 = this.A03.A05;
        r3.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        A02(-r3.getHeight());
        if (this.A06) {
            AnonymousClass01d r2 = this.A04;
            if (Build.VERSION.SDK_INT >= 30) {
                r3.performHapticFeedback(16);
            } else {
                C51282Tp.A01(r2);
            }
        }
    }
}
