package X;

import android.os.PowerManager;

/* renamed from: X.0K8  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0K8 {
    public static boolean A00(PowerManager powerManager) {
        return powerManager.isPowerSaveMode();
    }
}
