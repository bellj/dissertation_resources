package X;

import android.os.Bundle;

/* renamed from: X.10L  reason: invalid class name */
/* loaded from: classes2.dex */
public interface AnonymousClass10L {
    void ALq(boolean z);

    void AMs();

    void AMt(boolean z);

    void AMz(long j, long j2);

    void AN0(long j, long j2);

    void AN1(long j, long j2);

    void AN2(long j, long j2);

    void AN3(long j, long j2);

    void AN4(int i);

    void AN5();

    void AN6(long j, long j2);

    void AN7();

    void APg();

    void APw(int i, Bundle bundle);

    void APx(int i, Bundle bundle);

    void APy(int i, Bundle bundle);

    void ASU();

    void ASV(long j, long j2, boolean z);

    void ASW(long j, long j2);

    void ASX(long j, long j2);

    void ASY(long j, long j2);

    void ASZ(long j, long j2);

    void ASa(long j, long j2);

    void ASb(int i);

    void ASc();

    void ASd(long j, long j2, long j3);

    void ASl(boolean z);

    void ASm(long j, long j2);

    void ASn();

    void AVb();

    void AY3();
}
