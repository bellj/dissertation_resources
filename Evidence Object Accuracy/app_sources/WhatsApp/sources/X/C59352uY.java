package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.biz.catalog.view.CategoryMediaCard;
import com.whatsapp.jid.UserJid;
import java.util.LinkedList;
import java.util.List;

/* renamed from: X.2uY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59352uY extends AbstractC75723kJ {
    public C53842fM A00;
    public final AnonymousClass12P A01;
    public final C37071lG A02;
    public final CategoryMediaCard A03;
    public final UserJid A04;

    public C59352uY(AnonymousClass12P r1, C37071lG r2, CategoryMediaCard categoryMediaCard, C53842fM r4, UserJid userJid) {
        super(categoryMediaCard);
        this.A01 = r1;
        this.A04 = userJid;
        this.A03 = categoryMediaCard;
        this.A02 = r2;
        this.A00 = r4;
    }

    @Override // X.AbstractC75723kJ
    public /* bridge */ /* synthetic */ void A09(AbstractC89244Jf r9) {
        List list = ((C84643ze) r9).A00;
        if (list != null && !list.isEmpty()) {
            LinkedList linkedList = new LinkedList();
            int i = 0;
            while (i < list.size()) {
                AnonymousClass4SX r0 = (AnonymousClass4SX) list.get(i);
                linkedList.add(new AnonymousClass4RR(null, new AnonymousClass5TR(r0, this, i) { // from class: X.3VM
                    public final /* synthetic */ int A00;
                    public final /* synthetic */ AnonymousClass4SX A01;
                    public final /* synthetic */ C59352uY A02;

                    {
                        this.A02 = r2;
                        this.A01 = r1;
                        this.A00 = r3;
                    }

                    @Override // X.AnonymousClass5TR
                    public final void AOA(View view, AnonymousClass4RR r11) {
                        Object r1;
                        C59352uY r12 = this.A02;
                        AnonymousClass4SX r02 = this.A01;
                        int i2 = this.A00;
                        C53842fM r2 = r12.A00;
                        boolean z = r02.A04;
                        UserJid userJid = r2.A0M;
                        String str = r02.A01;
                        if (z) {
                            r1 = new C84573zX(userJid, str, r02.A02);
                        } else {
                            r1 = new C84563zW(userJid, str);
                        }
                        r2.A06.A0B(r1);
                        r2.A0E.A01(userJid, str, 1, 1, i2, z);
                    }
                }, new C90154Mu(r0, this), r0.A02));
                i++;
                if (i >= 6) {
                    break;
                }
            }
            AnonymousClass4RR r4 = null;
            if (list.size() > 6) {
                CategoryMediaCard categoryMediaCard = this.A03;
                r4 = new AnonymousClass4RR(AnonymousClass00T.A04(categoryMediaCard.getContext(), R.drawable.catalog_product_placeholder_background), new AnonymousClass5TR() { // from class: X.3VL
                    @Override // X.AnonymousClass5TR
                    public final void AOA(View view, AnonymousClass4RR r5) {
                        C53842fM r02 = C59352uY.this.A00;
                        r02.A06.A0B(new C84553zV(r02.A0M));
                    }
                }, null, categoryMediaCard.getContext().getString(R.string.catalog_categories_all_category));
            }
            CategoryMediaCard categoryMediaCard2 = this.A03;
            categoryMediaCard2.setup(linkedList, r4);
            categoryMediaCard2.setVisibility(0);
        }
    }
}
