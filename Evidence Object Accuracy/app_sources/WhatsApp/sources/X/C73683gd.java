package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;

/* renamed from: X.3gd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73683gd extends View.BaseSavedState {
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(38);
    public int A00;

    public /* synthetic */ C73683gd(Parcel parcel) {
        super(parcel);
        this.A00 = parcel.readInt();
    }

    public C73683gd(Parcelable parcelable) {
        super(parcelable);
    }

    @Override // android.view.View.BaseSavedState, android.os.Parcelable, android.view.AbsSavedState
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.A00);
    }
}
