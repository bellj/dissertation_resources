package X;

/* renamed from: X.5O6  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5O6 extends AbstractC112995Fp {
    public int A00;
    public int A01;
    public int A02;
    public boolean A03 = true;
    public byte[] A04;
    public byte[] A05;
    public byte[] A06;
    public final int A07;
    public final AnonymousClass5XE A08;

    @Override // X.AnonymousClass5XE
    public int AAt() {
        return this.A07;
    }

    @Override // X.AnonymousClass5XE
    public int AZY(byte[] bArr, byte[] bArr2, int i, int i2) {
        int i3 = this.A07;
        A01(bArr, bArr2, i, i3, i2);
        return i3;
    }

    @Override // X.AnonymousClass5XE
    public void reset() {
        this.A03 = true;
        this.A00 = 0;
        this.A01 = 0;
        byte[] bArr = this.A04;
        System.arraycopy(bArr, 0, this.A06, 0, bArr.length);
        this.A02 = 0;
        this.A08.reset();
    }

    public AnonymousClass5O6(AnonymousClass5XE r3) {
        super(r3);
        this.A08 = r3;
        int AAt = r3.AAt();
        this.A07 = AAt;
        if (AAt == 8) {
            int AAt2 = r3.AAt();
            this.A04 = new byte[AAt2];
            this.A06 = new byte[AAt2];
            this.A05 = new byte[AAt2];
            return;
        }
        throw C12970iu.A0f("GCTR only for 64 bit block ciphers");
    }

    @Override // X.AnonymousClass5XE
    public String AAf() {
        StringBuilder A0h = C12960it.A0h();
        C72463ee.A0U(A0h, this.A08);
        return C12960it.A0d("/GCTR", A0h);
    }

    @Override // X.AnonymousClass5XE
    public void AIf(AnonymousClass20L r7, boolean z) {
        this.A03 = true;
        this.A00 = 0;
        this.A01 = 0;
        if (r7 instanceof C113075Fx) {
            C113075Fx r72 = (C113075Fx) r7;
            byte[] bArr = r72.A01;
            int length = bArr.length;
            byte[] bArr2 = this.A04;
            int length2 = bArr2.length;
            if (length < length2) {
                int i = length2 - length;
                System.arraycopy(bArr, 0, bArr2, i, length);
                for (int i2 = 0; i2 < i; i2++) {
                    bArr2[i2] = 0;
                }
            } else {
                System.arraycopy(bArr, 0, bArr2, 0, length2);
            }
            reset();
            r7 = r72.A00;
        } else {
            reset();
        }
        if (r7 != null) {
            this.A08.AIf(r7, true);
        }
    }
}
