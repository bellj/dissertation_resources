package X;

import android.os.Bundle;

/* renamed from: X.2fC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53772fC extends AnonymousClass07A {
    public final /* synthetic */ AnonymousClass2IF A00;
    public final /* synthetic */ C67363Rf A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C53772fC(Bundle bundle, AbstractC001500q r2, AnonymousClass2IF r3, C67363Rf r4) {
        super(bundle, r2);
        this.A01 = r4;
        this.A00 = r3;
    }

    @Override // X.AnonymousClass07A
    public AnonymousClass015 A02(AnonymousClass07E r5, Class cls, String str) {
        AnonymousClass2IF r1 = this.A00;
        r1.A00 = r5;
        C16640pN.A00(AnonymousClass07E.class, r5);
        AnonymousClass01J r3 = r1.A02;
        C44351ym r32 = (C44351ym) ((AbstractC44361yn) AnonymousClass027.A00(AbstractC44361yn.class, new C44351ym(r1.A00, r1.A01, r3)));
        C28241Mh builderWithExpectedSize = AbstractC17190qP.builderWithExpectedSize(59);
        builderWithExpectedSize.put("com.whatsapp.community.AddGroupsToCommunityViewModel", r32.A00);
        builderWithExpectedSize.put("com.whatsapp.conversationslist.ArchiveHeaderViewModel", r32.A01);
        builderWithExpectedSize.put("com.whatsapp.avatar.home.AvatarHomeViewModel", r32.A06);
        builderWithExpectedSize.put("com.whatsapp.avatar.profilephoto.AvatarProfilePhotoViewModel", r32.A07);
        builderWithExpectedSize.put("com.whatsapp.userban.ui.viewmodel.BanAppealViewModel", r32.A08);
        builderWithExpectedSize.put("com.whatsapp.registration.report.BanReportViewModel", r32.A0A);
        builderWithExpectedSize.put("com.whatsapp.blockbusiness.blockreasonlist.BlockReasonListViewModel", r32.A0B);
        builderWithExpectedSize.put("com.whatsapp.calling.controls.viewmodel.BottomSheetViewModel", r32.A0C);
        builderWithExpectedSize.put("com.whatsapp.report.BusinessActivityReportViewModel", r32.A0D);
        builderWithExpectedSize.put("com.whatsapp.businessdirectory.viewmodel.BusinessDirectoryActivityViewModel", r32.A0E);
        builderWithExpectedSize.put("com.whatsapp.businessdirectory.viewmodel.BusinessDirectorySearchQueryViewModel", r32.A0F);
        builderWithExpectedSize.put("com.whatsapp.payments.ui.viewmodel.BusinessHubViewModel", r32.A0G);
        builderWithExpectedSize.put("com.whatsapp.calling.controls.viewmodel.CallControlButtonsViewModel", r32.A0H);
        builderWithExpectedSize.put("com.whatsapp.calling.callgrid.viewmodel.CallGridViewModel", r32.A0I);
        builderWithExpectedSize.put("com.whatsapp.calling.callheader.viewmodel.CallHeaderViewModel", r32.A0J);
        builderWithExpectedSize.put("com.whatsapp.calling.calllink.viewmodel.CallLinkViewModel", r32.A0K);
        builderWithExpectedSize.put("com.whatsapp.catalogcategory.view.viewmodel.CatalogAllCategoryViewModel", r32.A0L);
        builderWithExpectedSize.put("com.whatsapp.catalogcategory.view.viewmodel.CatalogCategoryGroupsViewModel", r32.A0M);
        builderWithExpectedSize.put("com.whatsapp.catalogcategory.view.viewmodel.CatalogCategoryTabsViewModel", r32.A0N);
        builderWithExpectedSize.put("com.whatsapp.catalogsearch.view.viewmodel.CatalogSearchViewModel", r32.A0O);
        builderWithExpectedSize.put("com.whatsapp.community.CommunityTabViewModel", r32.A0P);
        builderWithExpectedSize.put("com.whatsapp.biz.product.viewmodel.ComplianceInfoViewModel", r32.A0Q);
        builderWithExpectedSize.put("com.whatsapp.community.ConversationCommunityViewModel", r32.A0R);
        builderWithExpectedSize.put("com.whatsapp.countrygating.viewmodel.CountryGatingViewModel", r32.A0S);
        builderWithExpectedSize.put("com.whatsapp.qrcode.DevicePairQrScannerViewModel", r32.A0U);
        builderWithExpectedSize.put("com.whatsapp.businessdirectory.viewmodel.DirectorySearchHistoryViewModel", r32.A0V);
        builderWithExpectedSize.put("com.whatsapp.businessdirectory.viewmodel.DirectorySetNeighborhoodViewModel", r32.A0W);
        builderWithExpectedSize.put("com.whatsapp.backup.encryptedbackup.EncBackupViewModel", r32.A0X);
        builderWithExpectedSize.put("com.whatsapp.migration.export.ui.ExportMigrationViewModel", r32.A0Y);
        builderWithExpectedSize.put("com.whatsapp.wabloks.base.GenericBkLayoutViewModel", r32.A0q);
        builderWithExpectedSize.put("com.whatsapp.backup.google.viewmodel.GoogleDriveNewUserSetupViewModel", r32.A0s);
        builderWithExpectedSize.put("com.whatsapp.migration.android.view.GoogleMigrateImporterViewModel", r32.A0t);
        builderWithExpectedSize.put("com.whatsapp.group.GroupSettingsViewModel", r32.A0u);
        builderWithExpectedSize.put("com.whatsapp.calling.callgrid.viewmodel.InCallBannerViewModel", r32.A0v);
        builderWithExpectedSize.put("com.whatsapp.payments.ui.mapper.register.IndiaUpiMapperLinkViewModel", r32.A0w);
        builderWithExpectedSize.put("com.whatsapp.companiondevice.LinkedDevicesSharedViewModel", r32.A0x);
        builderWithExpectedSize.put("com.whatsapp.companiondevice.LinkedDevicesViewModel", r32.A0y);
        builderWithExpectedSize.put("com.whatsapp.businessdirectory.viewmodel.LocationOptionPickerViewModel", r32.A0z);
        builderWithExpectedSize.put("com.whatsapp.calling.callgrid.viewmodel.MenuBottomSheetViewModel", r32.A10);
        builderWithExpectedSize.put("com.whatsapp.conversation.conversationrow.message.MessageDetailsViewModel", r32.A11);
        builderWithExpectedSize.put("com.whatsapp.conversation.conversationrow.messagerating.MessageRatingViewModel", r32.A12);
        builderWithExpectedSize.put("com.whatsapp.biz.order.viewmodel.OrderInfoViewModel", r32.A13);
        builderWithExpectedSize.put("com.whatsapp.calling.callgrid.viewmodel.OrientationViewModel", r32.A14);
        builderWithExpectedSize.put("com.whatsapp.calling.controls.viewmodel.ParticipantsListViewModel", r32.A15);
        builderWithExpectedSize.put("com.whatsapp.polls.PollCreatorViewModel", r32.A16);
        builderWithExpectedSize.put("com.whatsapp.polls.PollResultsViewModel", r32.A17);
        builderWithExpectedSize.put("com.whatsapp.polls.PollVoterViewModel", r32.A18);
        builderWithExpectedSize.put("com.whatsapp.wabloks.ui.PrivacyNotice.PrivacyNoticeFragmentViewModel", r32.A19);
        builderWithExpectedSize.put("com.whatsapp.reactions.ReactionsTrayViewModel", r32.A1A);
        builderWithExpectedSize.put("com.whatsapp.backup.google.viewmodel.RestoreFromBackupViewModel", r32.A1B);
        builderWithExpectedSize.put("com.whatsapp.settings.SettingsChatViewModel", r32.A1C);
        builderWithExpectedSize.put("com.whatsapp.settings.SettingsDataUsageViewModel", r32.A1D);
        builderWithExpectedSize.put("com.whatsapp.backup.google.viewmodel.SettingsGoogleDriveViewModel", r32.A1E);
        builderWithExpectedSize.put("com.whatsapp.shops.ShopsBkLayoutViewModel", r32.A1F);
        builderWithExpectedSize.put("com.whatsapp.shops.ShopsProductPreviewFragmentViewModel", r32.A1G);
        builderWithExpectedSize.put("com.whatsapp.biz.SmbViewModel", r32.A1H);
        builderWithExpectedSize.put("com.whatsapp.tosgating.viewmodel.ToSGatingViewModel", r32.A1I);
        builderWithExpectedSize.put("com.whatsapp.wabloks.commerce.ui.viewmodel.WaBkGalaxyLayoutViewModel", r32.A1J);
        builderWithExpectedSize.put("com.whatsapp.wabloks.commerce.ui.viewmodel.WaGalaxyNavBarViewModel", r32.A1K);
        AbstractC17190qP build = builderWithExpectedSize.build();
        String name = cls.getName();
        AnonymousClass01N r0 = (AnonymousClass01N) build.get(name);
        if (r0 != null) {
            return (AnonymousClass015) r0.get();
        }
        StringBuilder A0k = C12960it.A0k("Expected the @HiltViewModel-annotated class '");
        A0k.append(name);
        throw C12960it.A0U(C12960it.A0d("' to be available in the multi-binding of @HiltViewModelMap but none was found.", A0k));
    }
}
