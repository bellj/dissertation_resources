package X;

import java.io.File;

/* renamed from: X.1pV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C39231pV extends AbstractRunnableC39141pM {
    public final AbstractC15710nm A00;
    public final C14330lG A01;
    public final C002701f A02;
    public final C16590pI A03;
    public final C39221pU A04;
    public final C22590zK A05;
    public final C26511Dt A06;
    public final C26521Du A07;

    public C39231pV(AbstractC15710nm r1, C14330lG r2, C002701f r3, C16590pI r4, C39221pU r5, C22590zK r6, C26511Dt r7, C26521Du r8) {
        super(r5);
        this.A00 = r1;
        this.A03 = r4;
        this.A01 = r2;
        this.A06 = r7;
        this.A07 = r8;
        this.A05 = r6;
        this.A02 = r3;
        this.A04 = r5;
    }

    public final byte[] A01(File file) {
        C39901qj A00 = new C39891qi(this.A00, this.A05, this.A06, this.A07).A00(new C39881qh(C14370lK.A0S, file, "image/webp", false));
        if (A00 != null) {
            return A00.A02;
        }
        return null;
    }
}
