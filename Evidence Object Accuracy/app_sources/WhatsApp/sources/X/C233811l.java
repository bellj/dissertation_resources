package X;

import android.database.Cursor;
import com.whatsapp.util.Log;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.11l  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C233811l {
    public static final byte[] A0C = "WhatsApp Patch Integrity".getBytes(AnonymousClass01V.A0A);
    public final AbstractC15710nm A00;
    public final C15570nT A01;
    public final C15450nH A02;
    public final C233411h A03;
    public final C233711k A04;
    public final C233311g A05;
    public final C14830m7 A06;
    public final C233611j A07;
    public final C232510y A08;
    public final C233511i A09;
    public final C14850m9 A0A;
    public final AbstractC14440lR A0B;

    public C233811l(AbstractC15710nm r1, C15570nT r2, C15450nH r3, C233411h r4, C233711k r5, C233311g r6, C14830m7 r7, C233611j r8, C232510y r9, C233511i r10, C14850m9 r11, AbstractC14440lR r12) {
        this.A06 = r7;
        this.A0A = r11;
        this.A00 = r1;
        this.A01 = r2;
        this.A0B = r12;
        this.A02 = r3;
        this.A05 = r6;
        this.A03 = r4;
        this.A09 = r10;
        this.A08 = r9;
        this.A07 = r8;
        this.A04 = r5;
    }

    public static final String A00(byte[] bArr) {
        if (bArr == null) {
            return "NULL";
        }
        String A03 = C003501n.A03(bArr);
        int length = A03.length();
        if (length > 16) {
            return A03.substring(length - 16);
        }
        return A03;
    }

    public static byte[] A01(List list, byte[] bArr, byte[] bArr2, boolean z) {
        int i;
        boolean z2 = false;
        if (bArr.length == 128) {
            z2 = true;
        }
        AnonymousClass009.A0B("Current hash array must be of size 128", z2);
        byte[] bArr3 = new byte[128];
        System.arraycopy(bArr, 0, bArr3, 0, 128);
        ByteBuffer wrap = ByteBuffer.wrap(bArr3);
        wrap.order(ByteOrder.LITTLE_ENDIAN);
        for (Object obj : list) {
            AnonymousClass009.A05(obj);
            ByteBuffer wrap2 = ByteBuffer.wrap(C32891cu.A00((byte[]) obj, bArr2, 128));
            wrap2.order(ByteOrder.LITTLE_ENDIAN);
            wrap.mark();
            while (wrap.hasRemaining()) {
                int position = wrap.position();
                int i2 = wrap.getShort() & 65535;
                int i3 = wrap2.getShort() & 65535;
                if (z) {
                    i = i2 + i3;
                } else {
                    i = i2 - i3;
                }
                int position2 = wrap.position();
                wrap.position(position);
                wrap.putShort((short) (((short) i) & 65535));
                wrap.position(position2);
            }
            wrap.reset();
        }
        return wrap.array();
    }

    /* JADX INFO: finally extract failed */
    public final void A02(int i, String str) {
        C232510y r0 = this.A08;
        HashMap hashMap = new HashMap();
        C16310on A01 = r0.A00.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT collection_name, lt_hash FROM collection_versions", null);
            while (A09.moveToNext()) {
                String string = A09.getString(A09.getColumnIndexOrThrow("collection_name"));
                byte[] blob = A09.getBlob(A09.getColumnIndexOrThrow("lt_hash"));
                if (blob == null) {
                    blob = new byte[128];
                }
                hashMap.put(string, blob);
            }
            A09.close();
            A01.close();
            C233511i r02 = this.A09;
            HashMap hashMap2 = new HashMap();
            A01 = r02.A02.get();
            try {
                Cursor A092 = A01.A03.A09("SELECT collection_name, mutation_mac FROM syncd_mutations", null);
                int columnIndexOrThrow = A092.getColumnIndexOrThrow("collection_name");
                int columnIndexOrThrow2 = A092.getColumnIndexOrThrow("mutation_mac");
                while (A092.moveToNext()) {
                    String string2 = A092.getString(columnIndexOrThrow);
                    byte[] blob2 = A092.getBlob(columnIndexOrThrow2);
                    List list = (List) hashMap2.get(string2);
                    if (list == null) {
                        list = new ArrayList();
                        hashMap2.put(string2, list);
                    }
                    list.add(blob2);
                }
                A092.close();
                A01.close();
                HashSet hashSet = new HashSet();
                if (str == null) {
                    hashSet.addAll(hashMap.keySet());
                    hashSet.addAll(hashMap2.keySet());
                } else {
                    hashSet.add(str);
                }
                Iterator it = hashSet.iterator();
                while (it.hasNext()) {
                    String str2 = (String) it.next();
                    List list2 = (List) hashMap2.get(str2);
                    if (list2 == null) {
                        list2 = Collections.emptyList();
                    }
                    if (!Arrays.equals(A01(list2, new byte[128], A0C, true), (byte[]) hashMap.get(str2))) {
                        this.A03.A07(i, str2);
                    }
                }
            } finally {
                try {
                    A01.close();
                } catch (Throwable unused) {
                }
            }
        } catch (Throwable th) {
            throw th;
        }
    }

    public final byte[] A03(String str, StringBuilder sb, List list, Map map, Set set, String[] strArr) {
        byte[] bArr;
        C16310on A01 = this.A08.A00.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT * FROM collection_versions WHERE collection_name = ?", new String[]{str});
            if (A09 == null || !A09.moveToFirst()) {
                bArr = null;
            } else {
                bArr = A09.getBlob(A09.getColumnIndexOrThrow("lt_hash"));
            }
            if (A09 != null) {
                A09.close();
            }
            if (bArr == null) {
                bArr = new byte[128];
            }
            C233511i r2 = this.A09;
            HashMap hashMap = new HashMap();
            if (strArr.length != 0) {
                C29841Uw r1 = new C29841Uw(strArr, 975);
                ArrayList arrayList = new ArrayList();
                A01 = r2.A02.get();
                try {
                    Iterator it = r1.iterator();
                    while (it.hasNext()) {
                        String[] strArr2 = (String[]) it.next();
                        arrayList.clear();
                        arrayList.add(str);
                        arrayList.addAll(Arrays.asList(strArr2));
                        C16330op r22 = A01.A03;
                        int length = strArr2.length;
                        StringBuilder sb2 = new StringBuilder("SELECT mutation_index, mutation_mac FROM syncd_mutations WHERE collection_name = ? AND mutation_index IN ");
                        sb2.append(AnonymousClass1Ux.A00(length));
                        Cursor A092 = r22.A09(sb2.toString(), (String[]) arrayList.toArray(AnonymousClass01V.A0H));
                        int columnIndexOrThrow = A092.getColumnIndexOrThrow("mutation_index");
                        int columnIndexOrThrow2 = A092.getColumnIndexOrThrow("mutation_mac");
                        while (A092.moveToNext()) {
                            String string = A092.getString(columnIndexOrThrow);
                            byte[] blob = A092.getBlob(columnIndexOrThrow2);
                            if (blob != null) {
                                hashMap.put(string, blob);
                            } else {
                                StringBuilder sb3 = new StringBuilder();
                                sb3.append("SyncdMutationsStore/getLatestMutationsMac: Should never be null for index:");
                                sb3.append(string);
                                Log.e(sb3.toString());
                            }
                        }
                        A092.close();
                    }
                    A01.close();
                } finally {
                }
            }
            if (this.A0A.A07(624)) {
                Arrays.toString(strArr);
            }
            if (!(sb == null || map == null || set == null)) {
                HashSet hashSet = new HashSet();
                sb.append("macsToRemove:\n");
                for (Map.Entry entry : hashMap.entrySet()) {
                    String A00 = A00((byte[]) map.get(entry.getKey()));
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append(A00);
                    sb4.append(": ");
                    sb4.append(A00((byte[]) entry.getValue()));
                    sb4.append("\n");
                    String obj = sb4.toString();
                    if (set.contains(A00)) {
                        hashSet.add(obj);
                    } else {
                        sb.append(obj);
                    }
                }
                sb.append("macsToOverwrite:\n");
                Iterator it2 = hashSet.iterator();
                while (it2.hasNext()) {
                    sb.append((String) it2.next());
                }
            }
            ArrayList arrayList2 = new ArrayList(hashMap.values());
            byte[] bArr2 = A0C;
            byte[] A012 = A01(list, A01(arrayList2, bArr, bArr2, false), bArr2, true);
            if (sb != null) {
                StringBuilder sb5 = new StringBuilder("preLtHash=");
                sb5.append(A00(bArr));
                sb5.append("; newLtHash=");
                sb5.append(A00(A012));
                sb.append(sb5.toString());
                Log.i(sb.toString());
            }
            return A012;
        } finally {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
        }
    }
}
