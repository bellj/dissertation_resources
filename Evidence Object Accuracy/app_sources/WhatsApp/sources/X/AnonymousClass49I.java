package X;

import java.io.OutputStream;
import java.util.List;
import javax.net.ssl.SSLException;

/* renamed from: X.49I  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass49I extends OutputStream {
    public boolean A00 = false;
    public final AbstractC115355Rf A01;

    public AnonymousClass49I(AbstractC115355Rf r2) {
        this.A01 = r2;
    }

    @Override // java.io.OutputStream, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.A00 = true;
    }

    @Override // java.io.OutputStream
    public void write(int i) {
        if (!this.A00) {
            write(new byte[]{(byte) (i & 255)}, 0, 1);
            return;
        }
        throw C12990iw.A0i("Stream is closed.");
    }

    @Override // java.io.OutputStream
    public void write(byte[] bArr) {
        if (!this.A00) {
            write(bArr, 0, bArr.length);
            return;
        }
        throw C12990iw.A0i("Stream is closed.");
    }

    @Override // java.io.OutputStream
    public void write(byte[] bArr, int i, int i2) {
        List list;
        if (!this.A00) {
            C113565Ib r8 = (C113565Ib) this.A01;
            try {
                C92264Ve r6 = new C92264Ve(bArr, i, i2);
                if (r8.A0E && !r8.A0D) {
                    AnonymousClass584 r10 = r8.A04;
                    if (!(!r10.A0Y || r10.A0R == null || (list = r10.A0S) == null)) {
                        r10.A0Z = true;
                        long j = (long) i2;
                        long j2 = r10.A0C.A03.maxEarlyDataSize;
                        if (r10.A04 + j > j2) {
                            r10.A04 = j2;
                            long j3 = r10.A05 + j;
                            long j4 = r10.A06;
                            if (j3 <= j4) {
                                r10.A05 = j3;
                                list.add(r6);
                                return;
                            }
                            StringBuilder A0h = C12960it.A0h();
                            A0h.append("Client request exceeded the max spillover limit ");
                            A0h.append(j3);
                            A0h.append(" > ");
                            throw AnonymousClass1NR.A00(C12970iu.A0w(A0h, j4), (byte) 80);
                        }
                        r8.A08.A00(new AnonymousClass461(r6));
                        r8.A04.A0R.add(r6);
                        r8.A04.A04 += j;
                        return;
                    }
                }
                r8.A08.A00(new C861445w(r6));
            } catch (AnonymousClass1NR e) {
                r8.A0C(e.ex, (byte) 2, e.description, e.errorTransient);
            } catch (Exception e2) {
                r8.A0C(new SSLException(C72453ed.A0w(e2)), (byte) 2, (byte) 80, false);
            }
        } else {
            throw C12990iw.A0i("Stream is closed.");
        }
    }
}
