package X;

/* renamed from: X.5FM  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5FM implements AnonymousClass5VN {
    public final /* synthetic */ C94904cj A00;

    public AnonymousClass5FM(C94904cj r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5VN
    public void AgH(Appendable appendable, Object obj, C94884ch r4) {
        String obj2;
        Double d = (Double) obj;
        if (d.isInfinite()) {
            obj2 = "null";
        } else {
            obj2 = d.toString();
        }
        appendable.append(obj2);
    }
}
