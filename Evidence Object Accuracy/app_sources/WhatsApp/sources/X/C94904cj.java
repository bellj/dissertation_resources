package X;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.4cj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C94904cj {
    public static final AnonymousClass5VN A02 = new AnonymousClass5FJ();
    public static final AnonymousClass5VN A03 = new AnonymousClass5FI();
    public static final AnonymousClass5VN A04 = new AnonymousClass5FG();
    public static final AnonymousClass5VN A05 = new AnonymousClass5FH();
    public static final AnonymousClass5VN A06 = new AnonymousClass5FK();
    public static final AnonymousClass5VN A07 = new AnonymousClass5FF();
    public static final AnonymousClass5VN A08 = new AnonymousClass5FE();
    public static final AnonymousClass5VN A09 = new AnonymousClass5FC();
    public static final AnonymousClass5VN A0A = new AnonymousClass5FD();
    public static final AnonymousClass5VN A0B = new AnonymousClass5FL();
    public LinkedList A00 = new LinkedList();
    public ConcurrentHashMap A01 = new ConcurrentHashMap();

    public C94904cj() {
        A01(new AnonymousClass5FV(this), String.class);
        A01(new AnonymousClass5FM(this), Double.class);
        A01(new AnonymousClass5FN(this), Date.class);
        A01(new AnonymousClass5FO(this), Float.class);
        AnonymousClass5VN r2 = A0B;
        A01(r2, Integer.class, Long.class, Byte.class, Short.class, BigInteger.class, BigDecimal.class);
        A01(r2, Boolean.class);
        A01(new AnonymousClass5FP(this), int[].class);
        A01(new AnonymousClass5FQ(this), short[].class);
        A01(new AnonymousClass5FR(this), long[].class);
        A01(new AnonymousClass5FS(this), float[].class);
        A01(new AnonymousClass5FT(this), double[].class);
        A01(new AnonymousClass5FU(this), boolean[].class);
        A00(AbstractC117225Za.class, A07);
        A00(AnonymousClass5VK.class, A08);
        A00(AnonymousClass5ZZ.class, A04);
        A00(AnonymousClass5VJ.class, A05);
        A00(Map.class, A06);
        A00(Iterable.class, A03);
        A00(Enum.class, A02);
        A00(Number.class, r2);
    }

    public void A00(Class cls, AnonymousClass5VN r4) {
        this.A00.addLast(new C90564Oj(cls, r4));
    }

    public void A01(AnonymousClass5VN r5, Class... clsArr) {
        for (Class cls : clsArr) {
            this.A01.put(cls, r5);
        }
    }
}
