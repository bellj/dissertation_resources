package X;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.Window;

/* renamed from: X.05V  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass05V extends AnonymousClass05W implements AbstractC001400p, AbstractC001600r, AbstractC001700s, AnonymousClass05X {
    public final Activity A00;
    public final Context A01;
    public final Handler A02;
    public final AnonymousClass01F A03 = new AnonymousClass05Y();
    public final /* synthetic */ ActivityC000900k A04;

    public AnonymousClass05V(ActivityC000900k r3) {
        this.A04 = r3;
        Handler handler = new Handler();
        this.A00 = r3;
        this.A01 = r3;
        this.A02 = handler;
    }

    @Override // X.AnonymousClass05W
    public View A00(int i) {
        return this.A04.findViewById(i);
    }

    @Override // X.AnonymousClass05W
    public boolean A01() {
        Window window = this.A04.getWindow();
        return (window == null || window.peekDecorView() == null) ? false : true;
    }

    @Override // X.AbstractC001600r
    public AnonymousClass052 AAb() {
        return ((ActivityC001000l) this.A04).A03;
    }

    @Override // X.AbstractC001200n
    public AbstractC009904y ADr() {
        return this.A04.A04;
    }

    @Override // X.AbstractC001700s
    public AnonymousClass051 AEk() {
        return ((ActivityC001000l) this.A04).A04;
    }

    @Override // X.AbstractC001400p
    public AnonymousClass05C AHb() {
        return this.A04.AHb();
    }

    @Override // X.AnonymousClass05X
    public void AMO(AnonymousClass01E r2, AnonymousClass01F r3) {
        this.A04.A1T(r2);
    }
}
