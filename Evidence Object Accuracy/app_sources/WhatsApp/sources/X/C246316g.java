package X;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.16g  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C246316g {
    public final C14850m9 A00;

    public C246316g(C14850m9 r1) {
        this.A00 = r1;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.lang.Long[] */
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.String[] */
    /* JADX WARN: Multi-variable type inference failed */
    public static Set A00(HashMap hashMap) {
        HashSet hashSet = new HashSet();
        for (Map.Entry entry : hashMap.entrySet()) {
            AbstractC14640lm r6 = ((C32381c4) entry.getKey()).A00;
            AbstractC14640lm r5 = ((C32381c4) entry.getKey()).A01;
            List list = (List) entry.getValue();
            Long[] lArr = new Long[list.size()];
            String[] strArr = new String[list.size()];
            for (int i = 0; i < list.size(); i++) {
                lArr[i] = ((C32021bU) list.get(i)).A00;
                strArr[i] = ((C32021bU) list.get(i)).A01;
            }
            hashSet.add(new C32551cL(r6, r5, lArr, strArr));
        }
        return hashSet;
    }

    public static Set A01(Set set) {
        HashMap hashMap = new HashMap();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            C32551cL r6 = (C32551cL) it.next();
            int i = 0;
            C32381c4 r4 = new C32381c4(r6.A01, r6.A00, false);
            ArrayList arrayList = new ArrayList();
            while (true) {
                Long[] lArr = r6.A02;
                if (i >= lArr.length) {
                    break;
                }
                arrayList.add(new C32021bU(lArr[i], r6.A03[i]));
                i++;
            }
            if (hashMap.containsKey(r4)) {
                ((List) hashMap.get(r4)).addAll(arrayList);
            } else {
                hashMap.put(r4, arrayList);
            }
        }
        return A00(hashMap);
    }

    public static boolean A02(AbstractC15340mz r3) {
        int i;
        if (r3.A0z.A02) {
            return false;
        }
        if ((r3.A08 != 1 && !C30041Vv.A0I(r3.A0y)) || (i = r3.A0C) == 9 || i == 10) {
            return false;
        }
        return true;
    }
}
