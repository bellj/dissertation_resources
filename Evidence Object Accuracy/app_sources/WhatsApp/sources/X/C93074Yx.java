package X;

import android.os.Build;

/* renamed from: X.4Yx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C93074Yx {
    public static boolean A00;

    public static synchronized void A00() {
        synchronized (C93074Yx.class) {
            if (!A00) {
                if (Build.VERSION.SDK_INT <= 16) {
                    try {
                        AnonymousClass1QL.A00("fb_jpegturbo");
                    } catch (UnsatisfiedLinkError unused) {
                    }
                }
                AnonymousClass1QL.A00("c++_shared");
                AnonymousClass1QL.A00("static-webp");
                A00 = true;
            }
        }
    }
}
