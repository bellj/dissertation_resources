package X;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import java.lang.ref.WeakReference;

/* renamed from: X.1p1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class C38961p1 implements AbstractC14590lg {
    public final /* synthetic */ AnonymousClass12P A00;
    public final /* synthetic */ C14900mE A01;
    public final /* synthetic */ C16440p1 A02;
    public final /* synthetic */ WeakReference A03;

    public /* synthetic */ C38961p1(AnonymousClass12P r1, C14900mE r2, C16440p1 r3, WeakReference weakReference) {
        this.A03 = weakReference;
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
    }

    @Override // X.AbstractC14590lg
    public final void accept(Object obj) {
        WeakReference weakReference = this.A03;
        C14900mE r1 = this.A01;
        AnonymousClass12P r4 = this.A00;
        C16440p1 r3 = this.A02;
        Uri uri = (Uri) obj;
        if (weakReference.get() != null) {
            r1.A03();
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(uri, ((AbstractC16130oV) r3).A06);
            intent.setFlags(1);
            r4.A06((Context) weakReference.get(), intent);
        }
    }
}
