package X;

/* renamed from: X.4WM  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass4WM {
    public Object A00(Object obj) {
        boolean z;
        C94524by r1;
        AnonymousClass5LI r4 = (AnonymousClass5LI) this;
        Object obj2 = r4._consensus;
        Object obj3 = C88554Gc.A00;
        if (obj2 == obj3) {
            Object A01 = r4.A01(obj);
            obj2 = r4._consensus;
            if (obj2 == obj3) {
                if (!AnonymousClass0KN.A00(r4, obj3, A01, AnonymousClass5LI.A00)) {
                    A01 = r4._consensus;
                }
                obj2 = A01;
            }
        }
        AnonymousClass5LC r42 = (AnonymousClass5LC) r4;
        if (obj2 == null) {
            z = true;
            r1 = r42.A01;
        } else {
            z = false;
            r1 = r42.A00;
        }
        if (r1 != null && AnonymousClass0KN.A00(obj, r42, r1, C94524by.A00) && z) {
            C94524by r12 = r42.A01;
            C94524by r0 = r42.A00;
            C16700pc.A0C(r0);
            r12.A08(r0);
        }
        return obj2;
    }

    public String toString() {
        StringBuilder A0h = C12960it.A0h();
        return C12960it.A0d(C72453ed.A0p(this, A0h), A0h);
    }
}
