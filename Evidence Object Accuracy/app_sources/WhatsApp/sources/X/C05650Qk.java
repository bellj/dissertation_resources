package X;

import android.graphics.Bitmap;
import android.graphics.drawable.AdaptiveIconDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;

/* renamed from: X.0Qk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05650Qk {
    public static Drawable A00(Drawable drawable, Drawable drawable2) {
        return new AdaptiveIconDrawable(drawable, drawable2);
    }

    public static Icon A01(Bitmap bitmap) {
        return Icon.createWithAdaptiveBitmap(bitmap);
    }
}
