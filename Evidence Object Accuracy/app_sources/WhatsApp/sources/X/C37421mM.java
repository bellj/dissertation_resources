package X;

/* renamed from: X.1mM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37421mM {
    public String A00;
    public String A01;
    public boolean A02;
    public boolean A03;
    public final int A04;
    public final int A05;
    public final int A06;
    public final long A07;
    public final String A08;
    public final String A09;
    public final String A0A;
    public final String A0B;
    public final String A0C;
    public final String A0D;

    public C37421mM(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, int i, int i2, int i3, long j, boolean z, boolean z2) {
        AnonymousClass009.A05(str);
        this.A0A = str;
        this.A01 = str2;
        this.A07 = j;
        this.A0D = str3;
        this.A09 = str4;
        this.A08 = str5;
        this.A0C = str6;
        this.A0B = str7;
        this.A04 = i;
        this.A06 = i2;
        this.A05 = i3;
        this.A00 = str8;
        this.A03 = z;
        this.A02 = z2;
    }
}
