package X;

import android.widget.CompoundButton;
import androidx.preference.SwitchPreferenceCompat;

/* renamed from: X.0X2  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0X2 implements CompoundButton.OnCheckedChangeListener {
    public final /* synthetic */ SwitchPreferenceCompat A00;

    public AnonymousClass0X2(SwitchPreferenceCompat switchPreferenceCompat) {
        this.A00 = switchPreferenceCompat;
    }

    @Override // android.widget.CompoundButton.OnCheckedChangeListener
    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        SwitchPreferenceCompat switchPreferenceCompat = this.A00;
        if (!switchPreferenceCompat.A0Q(Boolean.valueOf(z))) {
            compoundButton.setChecked(!z);
        } else {
            switchPreferenceCompat.A0T(z);
        }
    }
}
