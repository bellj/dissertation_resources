package X;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiMandatePaymentActivity;
import com.whatsapp.payments.ui.IndiaUpiPaymentsAccountSetupActivity;

/* renamed from: X.5fZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120065fZ extends AbstractC38231nk {
    public final C14900mE A00;
    public final C15450nH A01;
    public final C18640sm A02;
    public final C14830m7 A03;
    public final C16590pI A04;
    public final AnonymousClass018 A05;
    public final C14850m9 A06;
    public final C21860y6 A07;
    public final C18650sn A08;
    public final AnonymousClass18P A09;
    public final C243515e A0A;
    public final C18610sj A0B;
    public final C17070qD A0C;
    public final AnonymousClass6BE A0D;
    public final C1310060v A0E;
    public final C18590sh A0F;
    public final AnonymousClass14X A0G;
    public final AbstractC14440lR A0H;

    public C120065fZ(C14900mE r3, C15450nH r4, C18640sm r5, C14830m7 r6, C16590pI r7, AnonymousClass018 r8, C14850m9 r9, C21860y6 r10, C18650sn r11, AnonymousClass18P r12, C243515e r13, C18610sj r14, C17070qD r15, AnonymousClass6BE r16, C1310060v r17, C18590sh r18, AnonymousClass14X r19, AbstractC14440lR r20) {
        super(r19);
        this.A03 = r6;
        this.A00 = r3;
        this.A0H = r20;
        this.A04 = r7;
        this.A01 = r4;
        this.A0G = r19;
        this.A05 = r8;
        this.A0F = r18;
        this.A0C = r15;
        this.A0E = r17;
        this.A07 = r10;
        this.A0B = r14;
        this.A0D = r16;
        this.A02 = r5;
        this.A08 = r11;
        this.A09 = r12;
        this.A0A = r13;
        this.A06 = r9;
    }

    @Override // X.AbstractC38231nk
    public String A00(AnonymousClass1IR r5) {
        AnonymousClass60R r0;
        int i;
        C119835fB r2 = (C119835fB) r5.A0A;
        String A04 = A04(r5, r2, true);
        if (r5.A03 == 1 && !TextUtils.isEmpty(A04)) {
            return A04;
        }
        if (r2 == null || ((r0 = r2.A0B) == null || !r0.A0K ? TextUtils.isEmpty(r2.A0G) : !((i = r5.A02) == 20 || i == 405 || i == 415 || i == 417 || i == 418))) {
            return super.A00(r5);
        }
        return r2.A0J;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0047, code lost:
        if (r1 != 417) goto L_0x0049;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x006c, code lost:
        if (r1 != 418) goto L_0x0049;
     */
    @Override // X.AbstractC38231nk
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A01(X.AnonymousClass1IR r15) {
        /*
        // Method dump skipped, instructions count: 697
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C120065fZ.A01(X.1IR):java.lang.String");
    }

    @Override // X.AbstractC38231nk
    public void A02(Context context, View view, View view2, View view3, Button button, AnonymousClass1IR r15, AnonymousClass1In r16, String str) {
        AnonymousClass60R r1;
        AnonymousClass60O r2;
        AnonymousClass60R r0;
        C119835fB r12 = (C119835fB) r15.A0A;
        if (r12.A0B != null) {
            int i = r15.A02;
            if (i != 20) {
                if (i == 417 || i == 418) {
                    view2.setVisibility(8);
                    view3.setVisibility(8);
                    if (!(button == null || (r1 = r12.A0B) == null || !r1.A0L || r15.A02 == 418 || ((r2 = r1.A0C) != null && "ACCEPT".equals(r2.A08) && "PENDING".equals(r2.A09)))) {
                        button.setVisibility(0);
                        button.setEnabled(true);
                        button.setText(R.string.cancel);
                        button.setOnClickListener(new View.OnClickListener(context, r15, this, str) { // from class: X.64U
                            public final /* synthetic */ Context A00;
                            public final /* synthetic */ AnonymousClass1IR A01;
                            public final /* synthetic */ C120065fZ A02;
                            public final /* synthetic */ String A03;

                            {
                                this.A02 = r3;
                                this.A00 = r1;
                                this.A01 = r2;
                                this.A03 = r4;
                            }

                            @Override // android.view.View.OnClickListener
                            public final void onClick(View view4) {
                                C120065fZ r3 = this.A02;
                                Context context2 = this.A00;
                                AnonymousClass1IR r02 = this.A01;
                                String str2 = this.A03;
                                context2.startActivity(IndiaUpiMandatePaymentActivity.A02(context2, r02, str2, 3));
                                r3.A0D.AKh(1, 3, str2, null, true);
                            }
                        });
                        if (this.A06.A07(1433)) {
                            AbstractC30891Zf r13 = r15.A0A;
                            C119835fB r22 = (C119835fB) r13;
                            if (!(r22 == null || (r0 = r22.A0B) == null || !C1310060v.A02(r0.A0E))) {
                                AnonymousClass009.A05(r13);
                                TextView A0J = C12960it.A0J(view, R.id.pause_resume_button);
                                A0J.setVisibility(0);
                                AnonymousClass60R r02 = r22.A0B;
                                AnonymousClass009.A05(r02);
                                int A00 = r02.A00();
                                if (A00 != 0) {
                                    if (A00 != 2) {
                                        if (!(A00 == 3 || A00 == 5)) {
                                            if (A00 != 6) {
                                                A0J.setVisibility(8);
                                            }
                                        }
                                    }
                                    A0J.setText(R.string.payments_resume_mandate);
                                    A0J.setOnClickListener(new View.OnClickListener(context, r15, this, str) { // from class: X.64X
                                        public final /* synthetic */ Context A00;
                                        public final /* synthetic */ AnonymousClass1IR A01;
                                        public final /* synthetic */ C120065fZ A02;
                                        public final /* synthetic */ String A03;

                                        {
                                            this.A02 = r3;
                                            this.A00 = r1;
                                            this.A01 = r2;
                                            this.A03 = r4;
                                        }

                                        @Override // android.view.View.OnClickListener
                                        public final void onClick(View view4) {
                                            C120065fZ r3 = this.A02;
                                            Context context2 = this.A00;
                                            AnonymousClass1IR r14 = this.A01;
                                            String str2 = this.A03;
                                            context2.startActivity(IndiaUpiMandatePaymentActivity.A02(context2, r14, str2, 6));
                                            r3.A0D.AKh(C12960it.A0V(), 118, str2, null, true);
                                        }
                                    });
                                }
                                A0J.setText(R.string.payments_pause_mandate);
                                A0J.setOnClickListener(new View.OnClickListener(context, r15, this, str) { // from class: X.64Y
                                    public final /* synthetic */ Context A00;
                                    public final /* synthetic */ AnonymousClass1IR A01;
                                    public final /* synthetic */ C120065fZ A02;
                                    public final /* synthetic */ String A03;

                                    {
                                        this.A02 = r3;
                                        this.A00 = r1;
                                        this.A01 = r2;
                                        this.A03 = r4;
                                    }

                                    @Override // android.view.View.OnClickListener
                                    public final void onClick(View view4) {
                                        C120065fZ r3 = this.A02;
                                        Context context2 = this.A00;
                                        AnonymousClass1IR r14 = this.A01;
                                        String str2 = this.A03;
                                        context2.startActivity(IndiaUpiMandatePaymentActivity.A02(context2, r14, str2, 5));
                                        r3.A0D.AKh(C12960it.A0V(), 117, str2, null, true);
                                    }
                                });
                            }
                        }
                        view.setVisibility(0);
                        return;
                    }
                }
                view.setVisibility(8);
            } else if (r12.A04 - this.A0G.A04.A00() > 0) {
                TextView textView = (TextView) view2;
                textView.setEnabled(true);
                textView.setOnClickListener(new View.OnClickListener(context, r15, this, str) { // from class: X.64W
                    public final /* synthetic */ Context A00;
                    public final /* synthetic */ AnonymousClass1IR A01;
                    public final /* synthetic */ C120065fZ A02;
                    public final /* synthetic */ String A03;

                    {
                        this.A02 = r3;
                        this.A00 = r1;
                        this.A01 = r2;
                        this.A03 = r4;
                    }

                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view4) {
                        C120065fZ r5 = this.A02;
                        Context context2 = this.A00;
                        AnonymousClass1IR r3 = this.A01;
                        String str2 = this.A03;
                        if (!r5.A07.A0B()) {
                            Intent A0D = C12990iw.A0D(context2, IndiaUpiPaymentsAccountSetupActivity.class);
                            A0D.putExtra("extra_setup_mode", 1);
                            C35741ib.A00(A0D, "mandateRequest");
                            context2.startActivity(A0D);
                        } else {
                            r5.A09.A01(context2, r3, new AnonymousClass69N(context2, r3, r5, str2), false);
                        }
                        r5.A0D.AKh(1, 104, str2, null, true);
                    }
                });
                textView.setText(R.string.upi_mandate_payment_bottom_sheet_approve_payment_cta);
                view3.setEnabled(true);
                view3.setOnClickListener(new View.OnClickListener(context, r15, this, str) { // from class: X.64V
                    public final /* synthetic */ Context A00;
                    public final /* synthetic */ AnonymousClass1IR A01;
                    public final /* synthetic */ C120065fZ A02;
                    public final /* synthetic */ String A03;

                    {
                        this.A02 = r3;
                        this.A00 = r1;
                        this.A01 = r2;
                        this.A03 = r4;
                    }

                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view4) {
                        C120065fZ r3 = this.A02;
                        Context context2 = this.A00;
                        AnonymousClass1IR r14 = this.A01;
                        String str2 = this.A03;
                        context2.startActivity(IndiaUpiMandatePaymentActivity.A02(context2, r14, str2, 2));
                        r3.A0D.AKh(C12960it.A0V(), 105, str2, null, true);
                    }
                });
                view.setVisibility(0);
                if (button != null) {
                    button.setVisibility(8);
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000e, code lost:
        if (r1 != false) goto L_0x0010;
     */
    @Override // X.AbstractC38231nk
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A03(android.content.Context r3, android.view.View r4, X.AnonymousClass1IR r5) {
        /*
            r2 = this;
            X.1Zf r0 = r5.A0A
            X.5fB r0 = (X.C119835fB) r0
            if (r0 == 0) goto L_0x0010
            java.lang.String r0 = r0.A0G
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            r0 = 8
            if (r1 == 0) goto L_0x0011
        L_0x0010:
            r0 = 0
        L_0x0011:
            r4.setVisibility(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C120065fZ.A03(android.content.Context, android.view.View, X.1IR):void");
    }

    public final String A04(AnonymousClass1IR r24, C119835fB r25, boolean z) {
        AnonymousClass14X r8 = this.A0G;
        String A0L = r8.A0L(r24);
        if (r25 == null) {
            return "";
        }
        int i = r25.A01;
        if (i == 403) {
            int i2 = r24.A02;
            if (i2 == 405) {
                Context context = this.A04.A00;
                if (z) {
                    return context.getString(R.string.transaction_status_sender_completed_notification_title);
                }
                return C12960it.A0X(context, A0L, C12970iu.A1b(), 0, R.string.p2p_transaction_status_sender_completed);
            } else if (!(i2 == 406 || i2 == 408 || i2 == 423 || i2 == 424)) {
                return "";
            }
        } else if (i != 423) {
            if (!(i == 424 && 408 == r24.A02)) {
                return "";
            }
        } else if (422 != r24.A02) {
            return "";
        }
        if (!z) {
            return r8.A0W(r8.A0I(r24), R.string.payments_transaction_update_pending_notif_no_timestamp, R.string.payments_transaction_update_pending_notif_today, R.string.payments_transaction_update_pending_notif_yesterday, R.string.payments_transaction_update_pending_notif_on_monday, R.string.payments_transaction_update_pending_notif_on_tueday, R.string.payments_transaction_update_pending_notif_on_wednesday, R.string.payments_transaction_update_pending_notif_on_thursday, R.string.payments_transaction_update_pending_notif_on_friday, R.string.payments_transaction_update_pending_notif_on_saturday, R.string.payments_transaction_update_pending_notif_on_sunday, R.string.payments_transaction_update_pending_notif_day_and_month, r24.A05);
        }
        return A0L;
    }
}
