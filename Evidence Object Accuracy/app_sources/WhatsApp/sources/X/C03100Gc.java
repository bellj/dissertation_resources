package X;

import android.content.Context;
import android.os.Build;

/* renamed from: X.0Gc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C03100Gc extends AnonymousClass0Zt {
    public C03100Gc(Context context, AbstractC11500gO r3) {
        super(C05990Rt.A00(context, r3).A02);
    }

    @Override // X.AnonymousClass0Zt
    public boolean A01(C004401z r4) {
        EnumC004001t r2 = r4.A09.A03;
        if (r2 != EnumC004001t.UNMETERED) {
            return Build.VERSION.SDK_INT >= 30 && r2 == EnumC004001t.TEMPORARILY_UNMETERED;
        }
        return true;
    }

    @Override // X.AnonymousClass0Zt
    public boolean A02(Object obj) {
        C05410Pl r3 = (C05410Pl) obj;
        return !r3.A00 || r3.A01;
    }
}
