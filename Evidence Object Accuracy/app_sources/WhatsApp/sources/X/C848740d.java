package X;

import android.view.View;
import com.whatsapp.R;

/* renamed from: X.40d  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C848740d extends AbstractC848840e {
    public C848740d(View view) {
        super(view);
    }

    @Override // X.AbstractC37191le
    public /* bridge */ /* synthetic */ void A09(Object obj) {
        super.A0A((AnonymousClass403) obj);
        ((AbstractC848840e) this).A01.setImageDrawable(AnonymousClass2GE.A00(this.A0H.getContext(), R.drawable.ic_action_search));
        ((AbstractC848840e) this).A03.setText(R.string.biz_dir_pick_neighborhood);
    }
}
