package X;

import android.graphics.drawable.Drawable;
import android.view.View;

/* renamed from: X.5mM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122945mM extends C122985mQ {
    public View.OnClickListener A00;
    public View.OnLongClickListener A01;
    public final int A02;
    public final Drawable A03;
    public final CharSequence A04;
    public final CharSequence A05;
    public final boolean A06;

    public C122945mM(Drawable drawable, CharSequence charSequence, CharSequence charSequence2, int i, boolean z) {
        super(1008);
        this.A05 = charSequence;
        this.A04 = charSequence2;
        this.A03 = drawable;
        this.A06 = z;
        this.A02 = i;
    }
}
