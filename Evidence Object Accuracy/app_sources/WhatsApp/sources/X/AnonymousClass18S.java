package X;

import android.app.Activity;
import com.whatsapp.R;
import com.whatsapp.blocklist.UnblockDialogFragment;
import com.whatsapp.jid.UserJid;

/* renamed from: X.18S  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass18S {
    public final C238013b A00;
    public final C15550nR A01;
    public final C15610nY A02;
    public final C18610sj A03;
    public final C17070qD A04;

    public AnonymousClass18S(C238013b r1, C15550nR r2, C15610nY r3, C18610sj r4, C17070qD r5) {
        this.A01 = r2;
        this.A02 = r3;
        this.A04 = r5;
        this.A00 = r1;
        this.A03 = r4;
    }

    public void A00(Activity activity, AnonymousClass1P3 r8, UserJid userJid, AnonymousClass1ZR r10, boolean z, boolean z2) {
        Object obj;
        boolean z3 = false;
        if (userJid != null) {
            z3 = true;
        }
        int i = R.string.payment_unblock_ask;
        if (z2) {
            i = R.string.payment_unblock_reject_ask;
        }
        Object[] objArr = new Object[1];
        if (z3) {
            obj = this.A02.A04(this.A01.A0B(userJid));
        } else if (r10 == null) {
            obj = null;
        } else {
            obj = r10.A00;
        }
        objArr[0] = obj;
        String string = activity.getString(i, objArr);
        AnonymousClass5TY r1 = null;
        if (z3) {
            r1 = new AnonymousClass5TY(activity, r8, userJid, this) { // from class: X.545
                public final /* synthetic */ Activity A00;
                public final /* synthetic */ AnonymousClass1P3 A01;
                public final /* synthetic */ UserJid A02;
                public final /* synthetic */ AnonymousClass18S A03;

                {
                    this.A03 = r4;
                    this.A00 = r1;
                    this.A01 = r2;
                    this.A02 = r3;
                }

                @Override // X.AnonymousClass5TY
                public final void AfB() {
                    AnonymousClass18S r12 = this.A03;
                    Activity activity2 = this.A00;
                    AnonymousClass1P3 r2 = this.A01;
                    UserJid userJid2 = this.A02;
                    C238013b r13 = r12.A00;
                    UserJid of = UserJid.of(userJid2);
                    AnonymousClass009.A05(of);
                    r13.A09(activity2, r2, of);
                }
            };
        } else if (this.A04.A02().ABo() != null) {
            r1 = new AnonymousClass5TY(activity, r8, r10, this) { // from class: X.3Vg
                public final /* synthetic */ Activity A00;
                public final /* synthetic */ AnonymousClass1P3 A01;
                public final /* synthetic */ AnonymousClass1ZR A02;
                public final /* synthetic */ AnonymousClass18S A03;

                {
                    this.A03 = r4;
                    this.A00 = r1;
                    this.A02 = r3;
                    this.A01 = r2;
                }

                @Override // X.AnonymousClass5TY
                public final void AfB() {
                    Object obj2;
                    AnonymousClass18S r3 = this.A03;
                    Activity activity2 = this.A00;
                    AnonymousClass1ZR r2 = this.A02;
                    AnonymousClass1P3 r12 = this.A01;
                    AnonymousClass18N ABo = r3.A04.A02().ABo();
                    C18610sj r7 = r3.A03;
                    if (r2 == null) {
                        obj2 = null;
                    } else {
                        obj2 = r2.A00;
                    }
                    ABo.Afe(activity2, 
                    /*  JADX ERROR: Method code generation error
                        jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x001f: INVOKE  
                          (r4v0 'ABo' X.18N)
                          (r5v0 'activity2' android.app.Activity)
                          (wrap: X.587 : 0x001b: CONSTRUCTOR  (r6v0 X.587 A[REMOVE]) = (r1v0 'r12' X.1P3) call: X.587.<init>(X.1P3):void type: CONSTRUCTOR)
                          (r7v0 'r7' X.0sj)
                          (wrap: java.lang.String : 0x0017: CHECK_CAST (r8v2 java.lang.String A[REMOVE]) = (java.lang.String) (r8v1 'obj2' java.lang.Object))
                          false
                         type: INTERFACE call: X.18N.Afe(android.app.Activity, X.5US, X.0sj, java.lang.String, boolean):void in method: X.3Vg.AfB():void, file: classes2.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                        	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                        	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                        	at java.util.ArrayList.forEach(ArrayList.java:1259)
                        	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                        	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                        Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x001b: CONSTRUCTOR  (r6v0 X.587 A[REMOVE]) = (r1v0 'r12' X.1P3) call: X.587.<init>(X.1P3):void type: CONSTRUCTOR in method: X.3Vg.AfB():void, file: classes2.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                        	... 15 more
                        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.587, state: NOT_LOADED
                        	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                        	... 21 more
                        */
                    /*
                        this = this;
                        X.18S r3 = r10.A03
                        android.app.Activity r5 = r10.A00
                        X.1ZR r2 = r10.A02
                        X.1P3 r1 = r10.A01
                        X.0qD r0 = r3.A04
                        X.0pp r0 = r0.A02()
                        X.18N r4 = r0.ABo()
                        X.0sj r7 = r3.A03
                        if (r2 != 0) goto L_0x0023
                        r8 = 0
                    L_0x0017:
                        java.lang.String r8 = (java.lang.String) r8
                        X.587 r6 = new X.587
                        r6.<init>(r1)
                        r9 = 0
                        r4.Afe(r5, r6, r7, r8, r9)
                        return
                    L_0x0023:
                        java.lang.Object r8 = r2.A00
                        goto L_0x0017
                    */
                    throw new UnsupportedOperationException("Method not decompiled: X.C68403Vg.AfB():void");
                }
            };
        }
        ((AbstractC13860kS) activity).Adm(UnblockDialogFragment.A00(r1, string, 0, z));
    }
}
