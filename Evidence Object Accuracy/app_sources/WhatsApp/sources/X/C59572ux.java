package X;

/* renamed from: X.2ux  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59572ux extends C37171lc {
    public final AbstractView$OnClickListenerC34281fs A00;
    public final String A01;
    public final String A02;

    public C59572ux(AbstractView$OnClickListenerC34281fs r2, String str, String str2) {
        super(AnonymousClass39o.A0Z);
        this.A01 = str;
        this.A02 = str2;
        this.A00 = r2;
    }

    @Override // X.C37171lc
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
                return false;
            }
            C59572ux r5 = (C59572ux) obj;
            if (!this.A01.equals(r5.A01) || !this.A02.equals(r5.A02) || !this.A00.equals(r5.A00)) {
                return false;
            }
        }
        return true;
    }

    @Override // X.C37171lc
    public int hashCode() {
        return this.A01.hashCode();
    }
}
