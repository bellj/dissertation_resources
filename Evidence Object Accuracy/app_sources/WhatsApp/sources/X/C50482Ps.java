package X;

import android.app.Activity;
import android.content.Context;
import com.whatsapp.util.Log;
import java.util.List;
import org.json.JSONException;

/* renamed from: X.2Ps  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C50482Ps implements AbstractC50412Pl {
    public C17000q6 A00;
    public AnonymousClass018 A01;

    @Override // X.AbstractC50412Pl
    public void AZA(Context context, AbstractC15340mz r6, C16470p4 r7, int i) {
        AnonymousClass1Z7 r0 = r7.A03;
        if (r0 != null) {
            List list = r0.A00;
            if (list.size() > i && (context instanceof Activity)) {
                AnonymousClass1Z9 r02 = (AnonymousClass1Z9) list.get(i);
                try {
                    this.A00.A01(AnonymousClass12P.A00(context), this.A01, r6, r02.A01);
                } catch (JSONException unused) {
                    Log.e("NativeFlowAction/perform : ConversationRow exception processing NFM message");
                }
            }
        }
    }
}
