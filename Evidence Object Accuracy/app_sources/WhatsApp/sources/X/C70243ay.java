package X;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import com.whatsapp.conversation.conversationrow.ConversationRowImage$RowImageView;

/* renamed from: X.3ay  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70243ay implements AbstractC41521tf {
    public final /* synthetic */ C60632yL A00;

    public C70243ay(C60632yL r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC41521tf
    public int AGm() {
        C60632yL r2 = this.A00;
        int i = 72;
        if (((AbstractC28551Oa) r2).A0R) {
            i = 100;
        }
        return AnonymousClass3GD.A01(r2.getContext(), i);
    }

    @Override // X.AbstractC41521tf
    public void AQV() {
        this.A00.A1O();
    }

    @Override // X.AbstractC41521tf
    public void Adg(Bitmap bitmap, View view, AbstractC15340mz r6) {
        int i;
        if (bitmap == null || !(r6 instanceof AbstractC16130oV)) {
            C60632yL r2 = this.A00;
            ConversationRowImage$RowImageView conversationRowImage$RowImageView = r2.A0I;
            conversationRowImage$RowImageView.setScaleType(ImageView.ScaleType.CENTER);
            AnonymousClass2GE.A06(r2, conversationRowImage$RowImageView);
            return;
        }
        C16150oX A00 = AbstractC15340mz.A00((AbstractC16130oV) r6);
        int i2 = A00.A08;
        if (!(i2 == 0 || (i = A00.A06) == 0)) {
            ConversationRowImage$RowImageView conversationRowImage$RowImageView2 = this.A00.A0I;
            conversationRowImage$RowImageView2.A04(i2, i);
            C12990iw.A1E(conversationRowImage$RowImageView2);
        }
        this.A00.A0I.setImageBitmap(bitmap);
    }

    @Override // X.AbstractC41521tf
    public void Adu(View view) {
        this.A00.A0I.setBackgroundColor(-7829368);
    }
}
