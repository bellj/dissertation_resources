package X;

/* renamed from: X.1qr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C39981qr extends AnonymousClass1JW {
    public final C20320vZ A00;
    public final C30381Xe A01;

    public C39981qr(AbstractC15710nm r1, C15450nH r2, C20320vZ r3, C30381Xe r4, boolean z) {
        super(r1, r2, z);
        this.A01 = r4;
        this.A00 = r3;
    }

    public static EnumC40021qv A03(int i) {
        if (i == 0) {
            return EnumC40021qv.A11;
        }
        if (i == 1) {
            return EnumC40021qv.A10;
        }
        if (i == 2) {
            return EnumC40021qv.A0z;
        }
        if (i == 3) {
            return EnumC40021qv.A0y;
        }
        StringBuilder sb = new StringBuilder("unexpected missed call type ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
}
