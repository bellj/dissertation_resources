package X;

import android.content.Intent;

/* renamed from: X.0dm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09950dm implements Runnable {
    public final int A00;
    public final Intent A01;
    public final C07620Zm A02;

    public RunnableC09950dm(Intent intent, C07620Zm r2, int i) {
        this.A02 = r2;
        this.A01 = intent;
        this.A00 = i;
    }

    @Override // java.lang.Runnable
    public void run() {
        this.A02.A03(this.A01, this.A00);
    }
}
