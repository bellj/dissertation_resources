package X;

/* renamed from: X.2SE  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2SE {
    public static C15930o9 A00(AnonymousClass1V8 r4) {
        int i;
        String A0I = r4.A0I("v", null);
        if ("1".equals(A0I) || "2".equals(A0I)) {
            int parseInt = Integer.parseInt(A0I);
            String A0I2 = r4.A0I("type", null);
            if ("msg".equals(A0I2)) {
                i = 0;
            } else if ("pkmsg".equals(A0I2)) {
                i = 1;
            } else if ("skmsg".equals(A0I2)) {
                i = 2;
            } else if ("frskmsg".equals(A0I2)) {
                i = 3;
            } else {
                StringBuilder sb = new StringBuilder("invalid encrypted node type provided: ");
                sb.append(A0I2);
                throw new AnonymousClass1V9(sb.toString());
            }
            return new C15930o9(r4.A01, parseInt, i);
        }
        StringBuilder sb2 = new StringBuilder("invalid encrypted node version provided: ");
        sb2.append(A0I);
        throw new AnonymousClass1V9(sb2.toString());
    }
}
