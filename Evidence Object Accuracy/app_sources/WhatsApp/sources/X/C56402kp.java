package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;

/* renamed from: X.2kp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C56402kp extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C98704j7();
    public List A00;
    public final int A01;

    public C56402kp(List list, int i) {
        this.A01 = i;
        this.A00 = list;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A01 = C95654e8.A01(parcel);
        C95654e8.A07(parcel, 1, this.A01);
        C95654e8.A0F(parcel, this.A00, 2, false);
        C95654e8.A06(parcel, A01);
    }
}
