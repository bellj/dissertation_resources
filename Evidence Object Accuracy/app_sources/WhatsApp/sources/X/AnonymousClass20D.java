package X;

/* renamed from: X.20D  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass20D {
    public final String A00;
    public final byte[] A01;
    public final byte[] A02;
    public final byte[] A03;
    public final byte[] A04;

    public AnonymousClass20D(String str, byte[] bArr, byte[] bArr2, byte[] bArr3, byte[] bArr4) {
        this.A02 = bArr;
        this.A03 = bArr2;
        this.A04 = bArr3;
        this.A00 = str;
        this.A01 = bArr4;
    }
}
