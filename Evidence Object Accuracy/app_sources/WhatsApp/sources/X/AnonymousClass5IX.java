package X;

import java.security.AccessController;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.crypto.MacSpi;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.crypto.spec.RC2ParameterSpec;

/* renamed from: X.5IX  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5IX extends MacSpi implements AnonymousClass5S1 {
    public static final Class A01 = AnonymousClass1TA.A00(AnonymousClass5IX.class, "javax.crypto.spec.GCMParameterSpec");
    public AnonymousClass20J A00;

    public AnonymousClass5IX(AnonymousClass20J r1) {
        this.A00 = r1;
    }

    @Override // javax.crypto.MacSpi
    public int engineGetMacLength() {
        return this.A00.AE2();
    }

    @Override // javax.crypto.MacSpi
    public void engineReset() {
        this.A00.reset();
    }

    @Override // javax.crypto.MacSpi
    public void engineUpdate(byte b) {
        this.A00.AfG(b);
    }

    @Override // javax.crypto.MacSpi
    public void engineUpdate(byte[] bArr, int i, int i2) {
        this.A00.update(bArr, i, i2);
    }

    @Override // javax.crypto.MacSpi
    public byte[] engineDoFinal() {
        AnonymousClass20J r2 = this.A00;
        byte[] bArr = new byte[r2.AE2()];
        r2.A97(bArr, 0);
        return bArr;
    }

    @Override // javax.crypto.MacSpi
    public void engineInit(Key key, AlgorithmParameterSpec algorithmParameterSpec) {
        AnonymousClass20L r1;
        if (key != null) {
            if (key instanceof AnonymousClass5ED) {
                AnonymousClass5ED r5 = (AnonymousClass5ED) key;
                AnonymousClass5ED.A00(r5);
                if (r5.param != null) {
                    AnonymousClass5ED.A00(r5);
                    r1 = r5.param;
                } else if (!(algorithmParameterSpec instanceof PBEParameterSpec)) {
                    throw C72463ee.A0J("PBE requires PBE parameters to be set.");
                } else if (algorithmParameterSpec != null) {
                    PBEParameterSpec pBEParameterSpec = (PBEParameterSpec) algorithmParameterSpec;
                    AnonymousClass5ED.A00(r5);
                    int i = r5.type;
                    AnonymousClass5ED.A00(r5);
                    AbstractC94944cn A012 = C95114dA.A01(i, r5.digest);
                    byte[] encoded = r5.getEncoded();
                    byte[] salt = pBEParameterSpec.getSalt();
                    int iterationCount = pBEParameterSpec.getIterationCount();
                    A012.A01 = encoded;
                    A012.A02 = salt;
                    A012.A00 = iterationCount;
                    AnonymousClass5ED.A00(r5);
                    r1 = A012.A02(r5.keySize);
                } else {
                    throw C12970iu.A0f("Need a PBEParameter spec with a PBE key.");
                }
            } else if (!(algorithmParameterSpec instanceof PBEParameterSpec)) {
                r1 = new AnonymousClass20K(key.getEncoded());
            } else {
                throw C72463ee.A0J(C12960it.A0d(C12980iv.A0s(algorithmParameterSpec), C12960it.A0k("inappropriate parameter type: ")));
            }
            AnonymousClass20L r4 = r1;
            if (r1 instanceof C113075Fx) {
                r4 = ((C113075Fx) r4).A00;
            }
            AnonymousClass20K r42 = (AnonymousClass20K) r4;
            if (algorithmParameterSpec instanceof AnonymousClass5IZ) {
                AnonymousClass5IZ r8 = (AnonymousClass5IZ) algorithmParameterSpec;
                r1 = new C113035Ft(r42, r8.getIV(), AnonymousClass1TT.A02(r8.A01), r8.A00);
            } else if (algorithmParameterSpec instanceof IvParameterSpec) {
                r1 = new C113075Fx(r42, ((IvParameterSpec) algorithmParameterSpec).getIV());
            } else if (algorithmParameterSpec instanceof RC2ParameterSpec) {
                byte[] bArr = r42.A00;
                RC2ParameterSpec rC2ParameterSpec = (RC2ParameterSpec) algorithmParameterSpec;
                rC2ParameterSpec.getEffectiveKeyBits();
                r1 = new C113075Fx(new AnonymousClass5OL(bArr), rC2ParameterSpec.getIV());
            } else if (algorithmParameterSpec instanceof AnonymousClass5C4) {
                Map map = ((AnonymousClass5C4) algorithmParameterSpec).A00;
                Hashtable hashtable = new Hashtable();
                Iterator A10 = C72453ed.A10(map);
                while (A10.hasNext()) {
                    Object next = A10.next();
                    hashtable.put(next, map.get(next));
                }
                Hashtable hashtable2 = new Hashtable();
                Enumeration keys = hashtable.keys();
                while (keys.hasMoreElements()) {
                    Object nextElement = keys.nextElement();
                    hashtable2.put(nextElement, hashtable.get(nextElement));
                }
                byte[] bArr2 = r42.A00;
                if (bArr2 != null) {
                    hashtable2.put(C12980iv.A0i(), bArr2);
                    r1 = new C113065Fw(hashtable2);
                } else {
                    throw C12970iu.A0f("Parameter value must not be null.");
                }
            } else if (algorithmParameterSpec == null) {
                r1 = new AnonymousClass20K(key.getEncoded());
            } else {
                Class cls = A01;
                if (cls != null && cls.isAssignableFrom(algorithmParameterSpec.getClass())) {
                    try {
                        r1 = (C113035Ft) AccessController.doPrivileged(new C112065Bx(algorithmParameterSpec, r42));
                    } catch (Exception unused) {
                        throw C72463ee.A0J("Cannot process GCMParameterSpec.");
                    }
                } else if (!(algorithmParameterSpec instanceof PBEParameterSpec)) {
                    throw C72463ee.A0J(C12960it.A0d(C12980iv.A0s(algorithmParameterSpec), C12960it.A0k("unknown parameter type: ")));
                }
            }
            try {
                this.A00.AIc(r1);
            } catch (Exception e) {
                throw C72463ee.A0J(C12960it.A0d(e.getMessage(), C12960it.A0k("cannot initialize MAC: ")));
            }
        } else {
            throw new InvalidKeyException("key is null");
        }
    }
}
