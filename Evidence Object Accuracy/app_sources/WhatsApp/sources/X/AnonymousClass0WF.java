package X;

import android.view.View;
import androidx.appcompat.widget.Toolbar;

/* renamed from: X.0WF  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0WF implements View.OnClickListener {
    public final /* synthetic */ Toolbar A00;

    public AnonymousClass0WF(Toolbar toolbar) {
        this.A00 = toolbar;
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        C07340Xp r0;
        AnonymousClass0XN r02 = this.A00.A0Q;
        if (r02 != null && (r0 = r02.A01) != null) {
            r0.collapseActionView();
        }
    }
}
