package X;

/* renamed from: X.0I4  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0I4 extends AnonymousClass0P4 {
    public float A00;
    public float A01;
    public final /* synthetic */ C06540Ua A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass0I4(C06540Ua r1, float f, float f2) {
        super(r1);
        this.A02 = r1;
        this.A00 = f;
        this.A01 = f2;
    }

    @Override // X.AnonymousClass0P4
    public void A00(String str) {
        C06540Ua r5 = this.A02;
        if (r5.A0j()) {
            AnonymousClass0S1 r4 = r5.A03;
            if (r4.A05) {
                r5.A01.drawText(str, this.A00, this.A01, r4.A00);
            }
            AnonymousClass0S1 r42 = r5.A03;
            if (r42.A06) {
                r5.A01.drawText(str, this.A00, this.A01, r42.A01);
            }
        }
        this.A00 += r5.A03.A00.measureText(str);
    }
}
