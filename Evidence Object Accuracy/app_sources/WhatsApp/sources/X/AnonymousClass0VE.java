package X;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import java.util.List;

/* renamed from: X.0VE  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0VE implements LocationListener {
    public Location A00;
    public C04630Ml A01;
    public AnonymousClass0VE A02;
    public boolean A03;
    public final Context A04;
    public final LocationManager A05;

    @Override // android.location.LocationListener
    public void onProviderDisabled(String str) {
    }

    @Override // android.location.LocationListener
    public void onProviderEnabled(String str) {
    }

    @Override // android.location.LocationListener
    public void onStatusChanged(String str, int i, Bundle bundle) {
    }

    public AnonymousClass0VE(Context context) {
        this.A04 = context;
        this.A05 = (LocationManager) context.getSystemService("location");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0036, code lost:
        if (r1 <= 200) goto L_0x0038;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A00(android.location.Location r7, android.location.Location r8) {
        /*
            r6 = 0
            if (r7 == 0) goto L_0x0055
            r5 = 1
            if (r8 == 0) goto L_0x004f
            long r3 = r7.getTime()
            long r0 = r8.getTime()
            long r3 = r3 - r0
            r1 = 120000(0x1d4c0, double:5.9288E-319)
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 > 0) goto L_0x004f
            r1 = -120000(0xfffffffffffe2b40, double:NaN)
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x0055
            r1 = 0
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            r4 = 0
            if (r0 <= 0) goto L_0x0025
            r4 = 1
        L_0x0025:
            float r1 = r7.getAccuracy()
            float r0 = r8.getAccuracy()
            float r1 = r1 - r0
            int r1 = (int) r1
            r3 = 1
            if (r1 <= 0) goto L_0x0038
            r3 = 0
            r0 = 200(0xc8, float:2.8E-43)
            r2 = 1
            if (r1 > r0) goto L_0x0039
        L_0x0038:
            r2 = 0
        L_0x0039:
            java.lang.String r0 = r7.getProvider()
            java.lang.String r1 = r8.getProvider()
            if (r0 != 0) goto L_0x0050
            r0 = 0
            if (r1 != 0) goto L_0x0047
            r0 = 1
        L_0x0047:
            if (r3 != 0) goto L_0x004f
            if (r4 == 0) goto L_0x0055
            if (r2 != 0) goto L_0x0055
            if (r0 == 0) goto L_0x0055
        L_0x004f:
            return r5
        L_0x0050:
            boolean r0 = r0.equals(r1)
            goto L_0x0047
        L_0x0055:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0VE.A00(android.location.Location, android.location.Location):boolean");
    }

    public void A01(boolean z) {
        if (this.A02 == null) {
            this.A02 = this;
        }
        this.A03 = z;
        if (z && Build.VERSION.SDK_INT >= 23) {
            Context context = this.A04;
            if (!(context.checkSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0 && context.checkSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0)) {
                synchronized (C06160Sk.A0I) {
                }
                return;
            }
        }
        boolean z2 = this.A03;
        AnonymousClass0VE r13 = this.A02;
        if (z2) {
            LocationManager locationManager = r13.A05;
            List<String> providers = locationManager.getProviders(true);
            Location location = r13.A00;
            if (providers != null && AnonymousClass0LU.A00()) {
                for (String str : providers) {
                    Location lastKnownLocation = locationManager.getLastKnownLocation(str);
                    if (A00(lastKnownLocation, location)) {
                        location = lastKnownLocation;
                    }
                }
            }
            if (location == null || System.currentTimeMillis() - location.getTime() >= 7200000) {
                r13.A00 = null;
            } else {
                r13.A00 = location;
                C04630Ml r0 = r13.A01;
                if (r0 != null) {
                    r0.A00.A0E.invalidate();
                }
            }
            Criteria criteria = new Criteria();
            try {
                criteria.setAccuracy(1);
                locationManager.requestLocationUpdates(locationManager.getBestProvider(criteria, true), 50, 0.0f, r13);
            } catch (Exception unused) {
            }
            try {
                criteria.setAccuracy(2);
                locationManager.requestLocationUpdates(locationManager.getBestProvider(criteria, true), 50, 0.0f, r13);
            } catch (Exception unused2) {
            }
        } else {
            r13.A05.removeUpdates(r13);
        }
    }

    @Override // android.location.LocationListener
    public void onLocationChanged(Location location) {
        if (A00(location, this.A00)) {
            this.A00 = location;
            C04630Ml r0 = this.A01;
            if (r0 != null) {
                r0.A00.A0E.invalidate();
            }
        }
    }
}
