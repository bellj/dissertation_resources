package X;

import android.view.animation.Interpolator;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0QR  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass0QR {
    public float A00 = -1.0f;
    public float A01 = -1.0f;
    public float A02 = 0.0f;
    public AnonymousClass0SF A03;
    public Object A04 = null;
    public boolean A05 = false;
    public final AbstractC12660iI A06;
    public final List A07 = new ArrayList(1);

    public abstract Object A04(AnonymousClass0U8 v, float f);

    public AnonymousClass0QR(List list) {
        AbstractC12660iI r0;
        if (list.isEmpty()) {
            r0 = new C08080aY();
        } else if (list.size() == 1) {
            r0 = new C08090aZ(list);
        } else {
            r0 = new C08100aa(list);
        }
        this.A06 = r0;
    }

    public float A00() {
        float f = this.A00;
        if (f != -1.0f) {
            return f;
        }
        float ACj = this.A06.ACj();
        this.A00 = ACj;
        return ACj;
    }

    public float A01() {
        AnonymousClass0U8 AC6 = this.A06.AC6();
        AnonymousClass0MI.A00();
        if (AC6.A02()) {
            return 0.0f;
        }
        return AC6.A0B.getInterpolation(A02());
    }

    public float A02() {
        if (this.A05) {
            return 0.0f;
        }
        AnonymousClass0U8 AC6 = this.A06.AC6();
        AnonymousClass0MI.A00();
        if (!AC6.A02()) {
            return (this.A02 - AC6.A01()) / (AC6.A00() - AC6.A01());
        }
        return 0.0f;
    }

    public Object A03() {
        Object A04;
        Interpolator interpolator;
        float A02 = A02();
        if (this.A03 == null && this.A06.AJF(A02)) {
            return this.A04;
        }
        AnonymousClass0U8 AC6 = this.A06.AC6();
        AnonymousClass0MI.A00();
        Interpolator interpolator2 = AC6.A0C;
        if (interpolator2 == null || (interpolator = AC6.A0D) == null) {
            A04 = A04(AC6, A01());
        } else {
            A04 = A05(AC6, A02, interpolator2.getInterpolation(A02), interpolator.getInterpolation(A02));
        }
        this.A04 = A04;
        return A04;
    }

    public Object A05(AnonymousClass0U8 r3, float f, float f2, float f3) {
        throw new UnsupportedOperationException("This animation does not support split dimensions!");
    }

    public void A06() {
        int i = 0;
        while (true) {
            List list = this.A07;
            if (i < list.size()) {
                ((AbstractC12030hG) list.get(i)).AYB();
                i++;
            } else {
                return;
            }
        }
    }

    public void A07(float f) {
        AbstractC12660iI r3 = this.A06;
        if (!r3.isEmpty()) {
            float f2 = this.A01;
            if (f2 == -1.0f) {
                f2 = r3.AGu();
                this.A01 = f2;
            }
            if (f < f2) {
                if (f2 == -1.0f) {
                    f2 = r3.AGu();
                    this.A01 = f2;
                }
                f = f2;
            } else if (f > A00()) {
                f = A00();
            }
            if (f != this.A02) {
                this.A02 = f;
                if (r3.AKI(f)) {
                    A06();
                }
            }
        }
    }

    public void A08(AnonymousClass0SF r3) {
        AnonymousClass0SF r1 = this.A03;
        if (r1 != null) {
            r1.A00 = null;
        }
        this.A03 = r3;
        if (r3 != null) {
            r3.A00 = this;
        }
    }
}
