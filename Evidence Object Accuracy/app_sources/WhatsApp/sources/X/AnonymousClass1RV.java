package X;

import java.util.Iterator;
import java.util.LinkedList;

/* renamed from: X.1RV  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1RV {
    public boolean A00;
    public final C31311aL A01;
    public final LinkedList A02;

    public AnonymousClass1RV() {
        this.A02 = new LinkedList();
        this.A00 = false;
        this.A00 = true;
        this.A01 = new C31311aL();
    }

    public AnonymousClass1RV(byte[] bArr) {
        this.A02 = new LinkedList();
        this.A00 = false;
        C32071bZ r2 = (C32071bZ) AbstractC27091Fz.A0E(C32071bZ.A03, bArr);
        C31321aM r1 = r2.A02;
        this.A01 = new C31311aL(r1 == null ? C31321aM.A0E : r1);
        this.A00 = false;
        for (C31321aM r22 : r2.A01) {
            this.A02.add(new C31311aL(r22));
        }
    }

    public byte[] A00() {
        LinkedList linkedList = new LinkedList();
        Iterator it = this.A02.iterator();
        while (it.hasNext()) {
            linkedList.add(((C31311aL) it.next()).A00);
        }
        AnonymousClass1G4 A0T = C32071bZ.A03.A0T();
        C31321aM r0 = this.A01.A00;
        A0T.A03();
        C32071bZ r1 = (C32071bZ) A0T.A00;
        r1.A02 = r0;
        r1.A00 |= 1;
        A0T.A03();
        C32071bZ r2 = (C32071bZ) A0T.A00;
        AnonymousClass1K6 r12 = r2.A01;
        if (!((AnonymousClass1K7) r12).A00) {
            r12 = AbstractC27091Fz.A0G(r12);
            r2.A01 = r12;
        }
        AnonymousClass1G5.A01(linkedList, r12);
        return A0T.A02().A02();
    }
}
