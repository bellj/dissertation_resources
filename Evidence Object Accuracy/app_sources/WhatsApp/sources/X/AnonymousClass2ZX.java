package X;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;

/* renamed from: X.2ZX  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2ZX extends Drawable {
    public Rect A00 = C12980iv.A0J();
    public final int A01;
    public final Paint A02;
    public final Path A03;
    public final Path A04;
    public final Path A05;
    public final Path A06;

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -2;
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
    }

    public AnonymousClass2ZX(int i, int i2) {
        this.A01 = i;
        Paint A0F = C12990iw.A0F();
        this.A02 = A0F;
        A0F.setColor(i2);
        this.A05 = A00(AnonymousClass49U.A02, i);
        this.A06 = A00(AnonymousClass49U.A03, i);
        this.A03 = A00(AnonymousClass49U.A00, i);
        this.A04 = A00(AnonymousClass49U.A01, i);
    }

    public static Path A00(AnonymousClass49U r5, int i) {
        Region region = new Region(((int) r5.left) * i, ((int) r5.top) * i, ((int) r5.right) * i, ((int) r5.bottom) * i);
        Path path = new Path();
        float f = (float) i;
        path.addCircle(f, f, f, Path.Direction.CW);
        Region region2 = new Region();
        region2.setPath(path, region);
        region.op(region2, Region.Op.DIFFERENCE);
        return region.getBoundaryPath();
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        int width = getBounds().width();
        int i = this.A01 << 1;
        Rect rect = this.A00;
        float f = (float) (((width - i) - rect.left) - rect.right);
        int height = getBounds().height() - i;
        Rect rect2 = this.A00;
        int i2 = rect2.top;
        canvas.translate((float) rect2.left, (float) i2);
        Path path = this.A05;
        Paint paint = this.A02;
        canvas.drawPath(path, paint);
        path.close();
        canvas.translate(f, 0.0f);
        Path path2 = this.A06;
        canvas.drawPath(path2, paint);
        path2.close();
        canvas.translate(0.0f, (float) ((height - i2) - rect2.bottom));
        Path path3 = this.A04;
        canvas.drawPath(path3, paint);
        path3.close();
        canvas.translate(-f, 0.0f);
        Path path4 = this.A03;
        canvas.drawPath(path4, paint);
        path4.close();
    }
}
