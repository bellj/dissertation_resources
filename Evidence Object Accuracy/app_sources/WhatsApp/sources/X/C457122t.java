package X;

import android.text.TextUtils;

/* renamed from: X.22t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C457122t {
    public final CharSequence A00;
    public final CharSequence A01;

    public C457122t(CharSequence charSequence, CharSequence charSequence2) {
        this.A00 = charSequence;
        this.A01 = charSequence2;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C457122t.class != obj.getClass()) {
                return false;
            }
            C457122t r5 = (C457122t) obj;
            if (!TextUtils.equals(this.A00, r5.A00) || !TextUtils.equals(this.A01, r5.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int i;
        CharSequence charSequence = this.A00;
        int i2 = 0;
        if (charSequence != null) {
            i = charSequence.hashCode();
        } else {
            i = 0;
        }
        int i3 = (i + 0) * 31;
        CharSequence charSequence2 = this.A01;
        if (charSequence2 != null) {
            i2 = charSequence2.hashCode();
        }
        return i3 + i2;
    }
}
