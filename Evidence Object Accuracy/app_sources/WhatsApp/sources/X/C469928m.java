package X;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import com.facebook.redex.RunnableBRunnable0Shape12S0100000_I0_12;
import com.facebook.redex.RunnableBRunnable0Shape15S0100000_I1_1;
import com.facebook.redex.RunnableBRunnable0Shape1S0201000_I1;
import com.facebook.redex.RunnableBRunnable0Shape3S0300000_I1;
import com.facebook.redex.RunnableBRunnable0Shape4S0200000_I0_4;
import com.facebook.redex.RunnableBRunnable0Shape6S0200000_I0_6;
import com.whatsapp.R;
import com.whatsapp.inappsupport.ui.ContactUsActivity;
import com.whatsapp.picker.search.StickerSearchDialogFragment;
import com.whatsapp.status.playback.widget.StatusEditText;

/* renamed from: X.28m  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C469928m implements TextWatcher {
    @Override // android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
    }

    @Override // android.text.TextWatcher
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public static void A00(View view, boolean z) {
        AlphaAnimation alphaAnimation;
        ScaleAnimation scaleAnimation;
        AnimationSet animationSet = new AnimationSet(true);
        if (z) {
            alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        } else {
            alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        }
        alphaAnimation.setDuration(160);
        animationSet.addAnimation(alphaAnimation);
        if (z) {
            scaleAnimation = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, 1, 0.5f, 1, 0.5f);
        } else {
            scaleAnimation = new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, 1, 0.5f, 1, 0.5f);
        }
        scaleAnimation.setDuration(160);
        animationSet.addAnimation(scaleAnimation);
        animationSet.setDuration(160);
        view.startAnimation(animationSet);
    }

    @Override // android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        int codePointCount;
        RunnableBRunnable0Shape1S0201000_I1 runnableBRunnable0Shape1S0201000_I1;
        int codePointCount2;
        int codePointCount3;
        if (this instanceof AnonymousClass2GR) {
            AnonymousClass2GR r3 = (AnonymousClass2GR) this;
            AnonymousClass2GS r2 = r3.A00;
            r2.A0E.A00(AnonymousClass1VX.A00);
            C14310lE r5 = r2.A0A;
            boolean z = false;
            if (i3 > i2 && ((codePointCount = Character.codePointCount(charSequence, i, i3 + i)) > 1 || (codePointCount == 1 && Character.isWhitespace(Character.codePointAt(charSequence, i))))) {
                z = true;
            }
            r5.A07 = z;
            if (r2.A0I) {
                StatusEditText statusEditText = r2.A0G;
                if (statusEditText.getText() != null && i + i3 == statusEditText.getText().length()) {
                    r2.A07.post(new RunnableBRunnable0Shape12S0100000_I0_12(r3, 44));
                }
            }
        } else if (this instanceof AnonymousClass364) {
            AnonymousClass364 r52 = (AnonymousClass364) this;
            StickerSearchDialogFragment stickerSearchDialogFragment = r52.A01;
            Runnable runnable = stickerSearchDialogFragment.A0F;
            if (runnable != null) {
                stickerSearchDialogFragment.A05.removeCallbacks(runnable);
            }
            View view = r52.A00;
            RunnableBRunnable0Shape3S0300000_I1 runnableBRunnable0Shape3S0300000_I1 = new RunnableBRunnable0Shape3S0300000_I1(r52, charSequence, view, 30);
            stickerSearchDialogFragment.A0F = runnableBRunnable0Shape3S0300000_I1;
            stickerSearchDialogFragment.A05.postDelayed(runnableBRunnable0Shape3S0300000_I1, 500);
            view.setVisibility(0);
        } else if (this instanceof C469828l) {
            C469828l r22 = (C469828l) this;
            Runnable runnable2 = r22.A00;
            if (runnable2 != null) {
                r22.A02.removeCallbacks(runnable2);
            }
            RunnableBRunnable0Shape6S0200000_I0_6 runnableBRunnable0Shape6S0200000_I0_6 = new RunnableBRunnable0Shape6S0200000_I0_6(r22, 1, charSequence);
            r22.A00 = runnableBRunnable0Shape6S0200000_I0_6;
            r22.A02.postDelayed(runnableBRunnable0Shape6S0200000_I0_6, 500);
        } else if (this instanceof AnonymousClass360) {
            C35891iw r32 = ((AnonymousClass360) this).A00.A0G;
            ContactUsActivity contactUsActivity = r32.A02;
            AnonymousClass009.A05(contactUsActivity);
            boolean z2 = false;
            if (charSequence.length() > 0) {
                z2 = true;
            }
            contactUsActivity.findViewById(R.id.contact_us_send_button).setEnabled(z2);
            if (r32.A02.A00.getText().toString().trim().length() >= 10) {
                ContactUsActivity contactUsActivity2 = r32.A02;
                if (contactUsActivity2.A01.getVisibility() == 0) {
                    contactUsActivity2.A00.setBackgroundDrawable(AnonymousClass00T.A04(contactUsActivity2, R.drawable.description_field_background_state_list));
                    contactUsActivity2.A01.setVisibility(8);
                }
            }
        } else if (this instanceof AnonymousClass2Be) {
            AnonymousClass2Be r4 = (AnonymousClass2Be) this;
            Runnable runnable3 = r4.A00;
            if (runnable3 != null) {
                r4.A02.A04.removeCallbacks(runnable3);
            }
            RunnableBRunnable0Shape4S0200000_I0_4 runnableBRunnable0Shape4S0200000_I0_4 = new RunnableBRunnable0Shape4S0200000_I0_4(r4, 48, charSequence);
            r4.A00 = runnableBRunnable0Shape4S0200000_I0_4;
            r4.A02.A04.postDelayed(runnableBRunnable0Shape4S0200000_I0_4, 500);
            View view2 = r4.A01;
            int i4 = 0;
            if (TextUtils.isEmpty(charSequence)) {
                i4 = 4;
            }
            view2.setVisibility(i4);
        } else if (this instanceof AnonymousClass35z) {
            AnonymousClass238 r53 = ((AnonymousClass35z) this).A00;
            if (r53.A0I == 2) {
                int length = charSequence.length();
                RunnableBRunnable0Shape1S0201000_I1 runnableBRunnable0Shape1S0201000_I12 = r53.A05;
                if (runnableBRunnable0Shape1S0201000_I12 == null) {
                    runnableBRunnable0Shape1S0201000_I1 = new RunnableBRunnable0Shape1S0201000_I1(new RunnableBRunnable0Shape15S0100000_I1_1(r53, 26), new RunnableBRunnable0Shape15S0100000_I1_1(r53, 27), length, 8);
                    r53.A05 = runnableBRunnable0Shape1S0201000_I1;
                } else {
                    r53.A0L.A0G(runnableBRunnable0Shape1S0201000_I12);
                    runnableBRunnable0Shape1S0201000_I1 = r53.A05;
                    runnableBRunnable0Shape1S0201000_I1.A00 = length;
                }
                r53.A0L.A0J(runnableBRunnable0Shape1S0201000_I1, 275);
            }
        } else if (this instanceof AnonymousClass369) {
            AnonymousClass369 r33 = (AnonymousClass369) this;
            boolean z3 = false;
            if (i3 > i2 && ((codePointCount2 = Character.codePointCount(charSequence, i, i3 + i)) > 1 || (codePointCount2 == 1 && Character.isWhitespace(Character.codePointAt(charSequence, i))))) {
                z3 = true;
            }
            r33.A00 = z3;
        } else if (this instanceof AnonymousClass368) {
            C14310lE r34 = ((AnonymousClass368) this).A00.A21;
            boolean z4 = false;
            if (i3 > i2 && ((codePointCount3 = Character.codePointCount(charSequence, i, i3 + i)) > 1 || (codePointCount3 == 1 && Character.isWhitespace(Character.codePointAt(charSequence, i))))) {
                z4 = true;
            }
            r34.A07 = z4;
        }
    }
}
