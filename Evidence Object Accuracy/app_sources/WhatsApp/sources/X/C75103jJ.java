package X;

import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.3jJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75103jJ extends AbstractC05270Ox {
    public final /* synthetic */ AbstractC64423Fm A00;

    public C75103jJ(AbstractC64423Fm r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC05270Ox
    public void A00(RecyclerView recyclerView, int i) {
        if (i == 1) {
            this.A00.A02 = true;
        } else if (i == 0) {
            this.A00.A09.A0E();
        }
    }

    @Override // X.AbstractC05270Ox
    public void A01(RecyclerView recyclerView, int i, int i2) {
        AbstractC64423Fm r1 = this.A00;
        if (r1.A02 && i2 != 0) {
            r1.A09.A0E();
        }
    }
}
