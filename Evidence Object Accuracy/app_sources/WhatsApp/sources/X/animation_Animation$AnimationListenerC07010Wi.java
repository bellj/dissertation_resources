package X;

import android.view.animation.Animation;

/* renamed from: X.0Wi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class animation.Animation$AnimationListenerC07010Wi implements Animation.AnimationListener {
    public final /* synthetic */ AnonymousClass0BD A00;

    @Override // android.view.animation.Animation.AnimationListener
    public void onAnimationRepeat(Animation animation) {
    }

    @Override // android.view.animation.Animation.AnimationListener
    public void onAnimationStart(Animation animation) {
    }

    public animation.Animation$AnimationListenerC07010Wi(AnonymousClass0BD r1) {
        this.A00 = r1;
    }

    @Override // android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        AnonymousClass0BD r4 = this.A00;
        AnonymousClass0BG r2 = new AnonymousClass0BG(r4);
        r4.A0J = r2;
        r2.setDuration(150);
        C02350Bi r1 = r4.A0K;
        r1.A01 = null;
        r1.clearAnimation();
        r1.startAnimation(r4.A0J);
    }
}
