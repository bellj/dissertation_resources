package X;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import com.facebook.redex.IDxCListenerShape5S0000000_3_I1;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.payments.ui.IndiaUpiBankAccountPickerActivity;
import com.whatsapp.payments.ui.IndiaUpiBankPickerActivity;
import com.whatsapp.payments.ui.IndiaUpiDeviceBindStepActivity;
import com.whatsapp.payments.ui.IndiaUpiOnboardingErrorEducationActivity;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.5jA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC121665jA extends AbstractActivityC121675jB {
    public int A00;
    public int A01;
    public int A02 = 0;
    public int A03;
    public C30861Zc A04;
    public C16120oU A05;
    public AnonymousClass1ZR A06;
    public AnonymousClass1ZR A07;
    public AnonymousClass1ZR A08;
    public C119865fE A09;
    public C1308460e A0A;
    public C1329668y A0B;
    public C18600si A0C;
    public AnonymousClass6BE A0D;
    public C1309960u A0E;
    public String A0F;
    public String A0G;
    public String A0H;
    public String A0I;
    public String A0J;
    public String A0K;
    public String A0L;
    public String A0M;
    public boolean A0N;
    public boolean A0O = true;
    public boolean A0P = false;
    public boolean A0Q = true;
    public final C30931Zj A0R = C30931Zj.A00("IndiaUpiBasePaymentsActivity", "payment", "IN");

    @Override // X.ActivityC13810kN
    public void A2A(int i) {
        A2q();
        finish();
    }

    public String A2o(String str) {
        UserJid A03 = ((ActivityC13790kL) this).A01.A03();
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-256");
            long A02 = C117315Zl.A02(this);
            byte[] bArr = new byte[8];
            for (int i = 7; i >= 0; i--) {
                bArr[i] = (byte) ((int) A02);
                A02 >>= 8;
            }
            instance.update(bArr);
            instance.update(A03.getRawString().getBytes());
            instance.update(C117305Zk.A1a(16));
            byte[] bArr2 = new byte[15];
            System.arraycopy(instance.digest(), 0, bArr2, 0, 15);
            return A2p(str, C003501n.A03(bArr2));
        } catch (NoSuchAlgorithmException e) {
            this.A0R.A08("payment", "generateUuid unable to hash due to missing sha256 algorithm", e);
            return null;
        }
    }

    public String A2p(String str, String str2) {
        int length = str.length();
        if (length <= 8) {
            String A0d = C12960it.A0d(str2, C12960it.A0j(str));
            return A0d.length() > 35 ? A0d.substring(0, 35) : A0d;
        }
        throw C12970iu.A0f(this.A0R.A02(C12960it.A0W(length, "prefixAndTruncate called with too long a prefix: ")));
    }

    public void A2q() {
        if (this instanceof AbstractActivityC121545iU) {
            AbstractActivityC121545iU r3 = (AbstractActivityC121545iU) this;
            ((AbstractActivityC121665jA) r3).A0A.A04.A01();
            C30931Zj r2 = r3.A0I;
            StringBuilder A0k = C12960it.A0k("clearStates: ");
            A0k.append(((AbstractActivityC121665jA) r3).A0A.A04);
            C117295Zj.A1F(r2, A0k);
            ((AbstractActivityC121665jA) r3).A0A.A09();
        } else if (this instanceof IndiaUpiOnboardingErrorEducationActivity) {
            this.A0A.A09();
        } else if (this instanceof IndiaUpiDeviceBindStepActivity) {
            IndiaUpiDeviceBindStepActivity indiaUpiDeviceBindStepActivity = (IndiaUpiDeviceBindStepActivity) this;
            C64513Fv r0 = indiaUpiDeviceBindStepActivity.A0C;
            if (r0 != null) {
                r0.A01();
                C30931Zj r22 = indiaUpiDeviceBindStepActivity.A0W;
                StringBuilder A0k2 = C12960it.A0k("clearStates: ");
                A0k2.append(indiaUpiDeviceBindStepActivity.A0C);
                C117295Zj.A1F(r22, A0k2);
            }
            ((AbstractActivityC121665jA) indiaUpiDeviceBindStepActivity).A0A.A09();
        } else if (this instanceof IndiaUpiBankPickerActivity) {
            IndiaUpiBankPickerActivity indiaUpiBankPickerActivity = (IndiaUpiBankPickerActivity) this;
            C64513Fv r02 = indiaUpiBankPickerActivity.A09;
            if (r02 != null) {
                r02.A01();
                indiaUpiBankPickerActivity.A0N.A06(C12960it.A0d(indiaUpiBankPickerActivity.A09.toString(), C12960it.A0k("clearStates: ")));
            }
            ((AbstractActivityC121665jA) indiaUpiBankPickerActivity).A0A.A09();
        } else if (this instanceof IndiaUpiBankAccountPickerActivity) {
            IndiaUpiBankAccountPickerActivity indiaUpiBankAccountPickerActivity = (IndiaUpiBankAccountPickerActivity) this;
            indiaUpiBankAccountPickerActivity.A0H.A01();
            indiaUpiBankAccountPickerActivity.A0X.A06(C12960it.A0d(indiaUpiBankAccountPickerActivity.A0H.toString(), C12960it.A0k("clearStates: ")));
            ((AbstractActivityC121665jA) indiaUpiBankAccountPickerActivity).A0A.A09();
        }
    }

    public void A2r() {
        if (!(this instanceof IndiaUpiBankAccountPickerActivity)) {
            C12970iu.A1G(findViewById(R.id.progress));
            return;
        }
        IndiaUpiBankAccountPickerActivity indiaUpiBankAccountPickerActivity = (IndiaUpiBankAccountPickerActivity) this;
        indiaUpiBankAccountPickerActivity.A0V = false;
        AnonymousClass02M r0 = indiaUpiBankAccountPickerActivity.A0B.A0N;
        if (r0 != null) {
            r0.A02();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x004d, code lost:
        if (r4.A0O != false) goto L_0x001a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0018, code lost:
        if (r1 != false) goto L_0x001a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A2s() {
        /*
            r4 = this;
            r0 = 1
            r4.A0P = r0
            android.app.AlertDialog$Builder r1 = new android.app.AlertDialog$Builder
            r1.<init>(r4)
            r0 = 2131890397(0x7f1210dd, float:1.9415485E38)
            android.app.AlertDialog$Builder r2 = r1.setTitle(r0)
            boolean r0 = r4 instanceof com.whatsapp.payments.ui.IndiaUpiBankPickerActivity
            if (r0 != 0) goto L_0x004a
            boolean r1 = r4.A0O
            r0 = 2131890394(0x7f1210da, float:1.9415479E38)
            if (r1 == 0) goto L_0x001d
        L_0x001a:
            r0 = 2131890399(0x7f1210df, float:1.9415489E38)
        L_0x001d:
            java.lang.String r1 = r4.getString(r0)
        L_0x0021:
            android.app.AlertDialog$Builder r3 = r2.setMessage(r1)
            r2 = 2131890396(0x7f1210dc, float:1.9415483E38)
            r1 = 15
            com.facebook.redex.IDxCListenerShape10S0100000_3_I1 r0 = new com.facebook.redex.IDxCListenerShape10S0100000_3_I1
            r0.<init>(r4, r1)
            android.app.AlertDialog$Builder r3 = r3.setPositiveButton(r2, r0)
            r2 = 2131890395(0x7f1210db, float:1.941548E38)
            r1 = 16
            com.facebook.redex.IDxCListenerShape10S0100000_3_I1 r0 = new com.facebook.redex.IDxCListenerShape10S0100000_3_I1
            r0.<init>(r4, r1)
            android.app.AlertDialog$Builder r1 = r3.setNegativeButton(r2, r0)
            r0 = 0
            android.app.AlertDialog$Builder r0 = r1.setCancelable(r0)
            r0.show()
            return
        L_0x004a:
            boolean r0 = r4.A0O
            r1 = 0
            if (r0 == 0) goto L_0x0021
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractActivityC121665jA.A2s():void");
    }

    public void A2t(int i, int i2) {
        Toolbar A08 = C117305Zk.A08(this);
        A1e(A08);
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            if (Build.VERSION.SDK_INT >= 21) {
                getWindow().addFlags(Integer.MIN_VALUE);
                getWindow().setStatusBarColor(AnonymousClass00T.A00(this, R.color.ob_status_bar));
            }
            A08.setBackgroundColor(AnonymousClass00T.A00(this, R.color.primary_surface));
            A1U.A0D(AnonymousClass00T.A04(this, i));
            A1U.A0P(false);
            A08.setOverflowIcon(AnonymousClass00T.A04(this, R.drawable.onboarding_actionbar_overflow_button));
            View findViewById = findViewById(i2);
            if (findViewById != null) {
                findViewById.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener(findViewById, A1U, this) { // from class: X.656
                    public final /* synthetic */ View A00;
                    public final /* synthetic */ AbstractC005102i A01;
                    public final /* synthetic */ AbstractActivityC121665jA A02;

                    {
                        this.A02 = r3;
                        this.A00 = r1;
                        this.A01 = r2;
                    }

                    @Override // android.view.ViewTreeObserver.OnScrollChangedListener
                    public final void onScrollChanged() {
                        this.A01.A07(C117295Zj.A00(this.A02, this.A00));
                    }
                });
            }
        }
    }

    public void A2u(int i, int i2, int i3) {
        A2t(R.drawable.onboarding_actionbar_home_close, i3);
        ViewGroup viewGroup = (ViewGroup) findViewById(R.id.toolbar);
        TextView textView = (TextView) C12960it.A0F(LayoutInflater.from(this), viewGroup, R.layout.onboarding_title_view);
        C12960it.A0s(this, textView, i2);
        textView.setText(i);
        viewGroup.addView(textView);
    }

    public void A2v(Intent intent) {
        intent.putExtra("extra_conversation_message_type", ((AbstractActivityC121685jC) this).A00);
        intent.putExtra("extra_jid", C15380n4.A03(((AbstractActivityC121685jC) this).A0E));
        intent.putExtra("extra_receiver_jid", C15380n4.A03(((AbstractActivityC121685jC) this).A0G));
        intent.putExtra("extra_quoted_msg_row_id", ((AbstractActivityC121685jC) this).A02);
        intent.putExtra("extra_payment_preset_amount", this.A0i);
        intent.putExtra("extra_transaction_id", this.A0n);
        intent.putExtra("extra_payment_preset_min_amount", this.A0k);
        intent.putExtra("extra_payment_preset_max_amount", this.A0j);
        intent.putExtra("extra_request_message_key", this.A0l);
        intent.putExtra("extra_is_pay_money_only", this.A0s);
        intent.putExtra("extra_payment_note", this.A0h);
        intent.putExtra("extra_payment_background", ((AbstractActivityC121685jC) this).A0C);
        intent.putExtra("extra_payment_sticker", this.A0c);
        intent.putExtra("extra_payment_sticker_send_origin", this.A0e);
        List list = this.A0q;
        if (list != null) {
            intent.putStringArrayListExtra("extra_mentioned_jids", C12980iv.A0x(C15380n4.A06(list)));
        }
        intent.putExtra("extra_inviter_jid", C15380n4.A03(((AbstractActivityC121685jC) this).A0F));
        intent.putExtra("extra_in_setup", this.A0N);
        intent.putExtra("extra_setup_mode", this.A03);
        intent.putExtra("extra_payment_handle", this.A08);
        intent.putExtra("extra_payment_handle_id", this.A0M);
        intent.putExtra("extra_merchant_code", this.A0H);
        intent.putExtra("extra_transaction_ref", this.A0L);
        intent.putExtra("extra_payee_name", this.A06);
        intent.putExtra("extra_transaction_ref_url", this.A0J);
        intent.putExtra("extra_purpose_code", this.A0I);
        intent.putExtra("extra_initiation_mode", this.A0G);
        intent.putExtra("extra_incoming_pay_request_id", this.A0F);
        intent.putExtra("extra_payment_bank_account_added_in_onboarding", this.A04);
        intent.putExtra("extra_payments_entry_type", this.A02);
        intent.putExtra("extra_is_first_payment_method", this.A0O);
        intent.putExtra("extra_skip_value_props_display", this.A0Q);
        intent.putExtra("extra_transaction_type", this.A0o);
        intent.putExtra("extra_transaction_token", this.A0p);
        intent.putExtra("extra_transaction_is_merchant", this.A0r);
        intent.putExtra("extra_transaction_is_valid_merchant", this.A0t);
        intent.putExtra("extra_banner_type", this.A00);
        intent.putExtra("extra_payment_flow_entry_point", this.A01);
        intent.putExtra("extra_referral_screen", this.A0K);
        intent.putExtra("extra_order_type", this.A0g);
        intent.putExtra("extra_payment_config_id", this.A0f);
    }

    public void A2w(Menu menu) {
        if (((ActivityC13810kN) this).A0C.A07(732)) {
            MenuItem add = menu.add(0, R.id.menuitem_help, 0, ((ActivityC13830kP) this).A01.A00.getResources().getString(R.string.settings_help));
            add.setIcon(R.drawable.ic_settings_help).setShowAsAction(9);
            AnonymousClass07G.A00(ColorStateList.valueOf(AnonymousClass00T.A00(this, R.color.ob_action_bar_icon)), add);
        }
    }

    public final void A2x(C004802e r5, String str) {
        r5.setPositiveButton(R.string.context_help_call_support_button_txt, new DialogInterface.OnClickListener(str) { // from class: X.62V
            public final /* synthetic */ String A01;

            {
                this.A01 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                AbstractActivityC121665jA r3 = AbstractActivityC121665jA.this;
                String str2 = this.A01;
                dialogInterface.dismiss();
                String string = r3.A0C.A01().getString("payments_support_phone_number", null);
                if (!TextUtils.isEmpty(string)) {
                    Intent intent = new Intent("android.intent.action.DIAL", Uri.fromParts("tel", string, null));
                    if (intent.resolveActivity(r3.getPackageManager()) != null) {
                        r3.startActivity(intent);
                    }
                }
                r3.A0D.AKg(C12960it.A0V(), 26, str2, null);
            }
        });
        r5.setNegativeButton(R.string.ok, new IDxCListenerShape5S0000000_3_I1(2));
        r5.A0B(true);
        r5.A05();
        this.A0D.AKg(C12980iv.A0i(), 39, str, null);
    }

    public final void A2y(C119755f3 r7, C452120p r8, AnonymousClass60V r9, String str) {
        this.A0D.AKg(C12980iv.A0i(), null, str, null);
        this.A0B.A8l(this.A0A.A05(r7), true);
        if (this instanceof IndiaUpiDeviceBindStepActivity) {
            IndiaUpiDeviceBindStepActivity indiaUpiDeviceBindStepActivity = (IndiaUpiDeviceBindStepActivity) this;
            if (r8.A00 == 11473) {
                indiaUpiDeviceBindStepActivity.A00 = 1;
                AbstractActivityC119235dO.A1h(indiaUpiDeviceBindStepActivity, R.string.get_accounts_failure_reason, true);
            } else {
                indiaUpiDeviceBindStepActivity.A00 = 7;
                indiaUpiDeviceBindStepActivity.A39(r9, true);
            }
        } else if (this instanceof IndiaUpiBankAccountPickerActivity) {
            IndiaUpiBankAccountPickerActivity indiaUpiBankAccountPickerActivity = (IndiaUpiBankAccountPickerActivity) this;
            if (r8.A00 == 11473) {
                indiaUpiBankAccountPickerActivity.A00 = 1;
                indiaUpiBankAccountPickerActivity.A33(new AnonymousClass60V(R.string.get_accounts_failure_reason), true);
            } else {
                indiaUpiBankAccountPickerActivity.A00 = 7;
                indiaUpiBankAccountPickerActivity.A33(r9, true);
            }
        }
        C1308460e r5 = this.A0A;
        ArrayList arrayList = r5.A06;
        if (arrayList != null && arrayList.size() > 1) {
            r5.A01++;
        }
        ArrayList A07 = r5.A07(r7);
        if (A07 != null) {
            int size = A07.size();
            r5.A00 = size;
            int i = r5.A02;
            if (i + 1 != size) {
                r5.A02 = i + 1;
                return;
            }
        }
        r5.A02 = 0;
    }

    public boolean A2z(C119755f3 r7, C452120p r8, String str) {
        AnonymousClass60V r1;
        int i;
        int i2 = r8.A00;
        if (i2 != 11473) {
            if (i2 == 11474) {
                i = R.string.upi_external_error;
            } else if (i2 != 11484) {
                if (i2 != 11498) {
                    if (i2 != 11500) {
                        if (i2 != 11534) {
                            if (i2 != 20686) {
                                switch (i2) {
                                    case 21143:
                                        break;
                                    case 21144:
                                    case 21145:
                                        break;
                                    default:
                                        switch (i2) {
                                        }
                                }
                            }
                        }
                    }
                    r1 = new AnonymousClass60V(i2, str);
                    A2y(r7, r8, r1, "retry_device_binding_on_error");
                    return true;
                }
                this.A0D.AKg(0, null, "updated_onboarding_error_strings", null);
                return false;
            } else {
                i = R.string.upi_external_npci_error;
            }
            r1 = new AnonymousClass60V(i);
            A2y(r7, r8, r1, "retry_device_binding_on_error");
            return true;
        } else if (((ActivityC13810kN) this).A0C.A07(1685)) {
            A2y(r7, r8, new AnonymousClass60V(r8.A00, str), "retry_device_binding_xh_error");
            return true;
        }
        return false;
    }

    @Override // X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 1000) {
            A2q();
            finish();
            return;
        }
        super.onActivityResult(i, i2, intent);
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        C30931Zj r2 = this.A0R;
        StringBuilder A0h = C12960it.A0h();
        A0h.append(this);
        r2.A06(C12960it.A0d(" onBackPressed", A0h));
        A2q();
        finish();
        super.onBackPressed();
    }

    @Override // X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.A0R.A06(C12960it.A0b("onCreate", this));
        this.A0E.A02(new AnonymousClass6MA() { // from class: X.6D2
            @Override // X.AnonymousClass6MA
            public final void AVY() {
                C1309960u.A01(AbstractActivityC121665jA.this);
            }
        });
        if (getIntent() != null) {
            boolean z = false;
            this.A0N = getIntent().getBooleanExtra("extra_in_setup", false);
            this.A03 = getIntent().getIntExtra("extra_setup_mode", 1);
            this.A08 = (AnonymousClass1ZR) getIntent().getParcelableExtra("extra_payment_handle");
            this.A0M = getIntent().getStringExtra("extra_payment_handle_id");
            this.A0H = getIntent().getStringExtra("extra_merchant_code");
            this.A0L = getIntent().getStringExtra("extra_transaction_ref");
            this.A06 = (AnonymousClass1ZR) getIntent().getParcelableExtra("extra_payee_name");
            this.A07 = (AnonymousClass1ZR) getIntent().getParcelableExtra("extra_payment_upi_number");
            this.A0J = getIntent().getStringExtra("extra_transaction_ref_url");
            this.A0I = getIntent().getStringExtra("extra_purpose_code");
            this.A0G = getIntent().getStringExtra("extra_initiation_mode");
            this.A0F = getIntent().getStringExtra("extra_incoming_pay_request_id");
            this.A04 = (C30861Zc) getIntent().getParcelableExtra("extra_payment_bank_account_added_in_onboarding");
            this.A02 = getIntent().getIntExtra("extra_payments_entry_type", 0);
            this.A0O = getIntent().getBooleanExtra("extra_is_first_payment_method", true);
            this.A0Q = getIntent().getBooleanExtra("extra_skip_value_props_display", true);
            this.A00 = getIntent().getIntExtra("extra_banner_type", 0);
            this.A01 = getIntent().getIntExtra("extra_payment_flow_entry_point", 0);
            boolean booleanExtra = getIntent().getBooleanExtra("extra_should_open_transaction_detail_after_send_override", false);
            int i = this.A01;
            if (i == 2 || i == 3 || booleanExtra) {
                z = true;
            }
            this.A0u = z;
            this.A0K = AbstractActivityC119235dO.A0N(this);
        }
        if (((AbstractActivityC121685jC) this).A0O.A03.A07(698)) {
            this.A09.A0A();
        }
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return false;
        }
        C30931Zj r2 = this.A0R;
        StringBuilder A0h = C12960it.A0h();
        A0h.append(this);
        r2.A06(C12960it.A0d(" action bar home", A0h));
        A2q();
        finish();
        return true;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        if (this.A0E.A03()) {
            C1309960u.A01(this);
        }
    }
}
