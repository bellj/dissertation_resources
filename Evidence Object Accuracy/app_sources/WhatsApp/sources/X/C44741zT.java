package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.1zT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C44741zT implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C99584kX();
    public String A00;
    public String A01;
    public final int A02;
    public final int A03;
    public final String A04;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C44741zT(Parcel parcel) {
        this.A04 = parcel.readString();
        this.A00 = parcel.readString();
        this.A01 = parcel.readString();
        this.A03 = parcel.readInt();
        this.A02 = parcel.readInt();
    }

    public C44741zT(String str, String str2, String str3, int i, int i2) {
        this.A04 = str;
        this.A00 = str2;
        this.A01 = str3;
        this.A03 = i;
        this.A02 = i2;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        return this == obj || (obj != null && (obj instanceof C44741zT) && this.A04.equals(((C44741zT) obj).A04));
    }

    @Override // java.lang.Object
    public int hashCode() {
        String str = this.A04;
        if (str == null) {
            return 0;
        }
        return str.hashCode();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A04);
        parcel.writeString(this.A00);
        parcel.writeString(this.A01);
        parcel.writeInt(this.A03);
        parcel.writeInt(this.A02);
    }
}
