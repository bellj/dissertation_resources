package X;

import java.util.Collections;
import java.util.HashMap;

/* renamed from: X.2Q7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Q7 extends C36261ja {
    public final /* synthetic */ C26651Eh A00;
    public final /* synthetic */ String A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2Q7(AbstractC14640lm r1, C26651Eh r2, String str, boolean z) {
        super(r1, z);
        this.A00 = r2;
        this.A01 = str;
    }

    @Override // X.C36261ja
    public void A01(int i) {
        super.A01(i);
        if (super.A01) {
            this.A00.A03(super.A00, this.A01, i / 1000);
        }
    }

    @Override // X.C36261ja
    public void A02(int i) {
        super.A02(i);
        if (!super.A01) {
            HashMap hashMap = new HashMap();
            hashMap.put("duration", Long.toString((long) (i / 1000)));
            this.A00.A17.A02(this.A01, Collections.emptyList(), Collections.emptyMap(), hashMap, 29, false);
        }
    }
}
