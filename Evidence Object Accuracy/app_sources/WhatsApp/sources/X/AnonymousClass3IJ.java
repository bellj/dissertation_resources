package X;

/* renamed from: X.3IJ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3IJ {
    public final int A00;
    public final int A01;
    public final String A02;
    public final String A03;
    public final boolean A04;

    public AnonymousClass3IJ(String str, String str2, int i, int i2, boolean z) {
        this.A03 = str;
        this.A02 = str2;
        this.A04 = z;
        this.A01 = i;
        this.A00 = i2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x003e, code lost:
        if (android.text.TextUtils.isEmpty(r1) == false) goto L_0x0040;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass3IJ A00(X.AbstractC15340mz r10) {
        /*
        // Method dump skipped, instructions count: 246
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3IJ.A00(X.0mz):X.3IJ");
    }
}
