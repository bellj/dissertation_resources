package X;

import java.util.Comparator;

/* renamed from: X.3tI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C80993tI extends AbstractC95284dR {
    public final int result;

    @Override // X.AbstractC95284dR
    public AbstractC95284dR compare(int i, int i2) {
        return this;
    }

    @Override // X.AbstractC95284dR
    public AbstractC95284dR compare(Object obj, Object obj2, Comparator comparator) {
        return this;
    }

    @Override // X.AbstractC95284dR
    public AbstractC95284dR compareFalseFirst(boolean z, boolean z2) {
        return this;
    }

    @Override // X.AbstractC95284dR
    public AbstractC95284dR compareTrueFirst(boolean z, boolean z2) {
        return this;
    }

    public C80993tI(int i) {
        super(null);
        this.result = i;
    }

    @Override // X.AbstractC95284dR
    public int result() {
        return this.result;
    }
}
