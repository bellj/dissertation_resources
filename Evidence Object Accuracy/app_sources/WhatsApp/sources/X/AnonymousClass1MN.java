package X;

import android.content.Context;
import android.util.AttributeSet;
import androidx.appcompat.widget.Toolbar;
import com.whatsapp.BidiToolbar;

/* renamed from: X.1MN  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1MN extends Toolbar implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;

    public AnonymousClass1MN(Context context) {
        super(context);
        A0I();
    }

    public AnonymousClass1MN(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A0I();
    }

    public AnonymousClass1MN(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A0I();
    }

    public void A0I() {
        if (this instanceof AnonymousClass2Q3) {
            AnonymousClass2Q3 r1 = (AnonymousClass2Q3) this;
            if (!r1.A00) {
                r1.A00 = true;
                ((BidiToolbar) r1).A00 = (AnonymousClass018) ((AnonymousClass2P6) ((AnonymousClass2P5) r1.generatedComponent())).A06.ANb.get();
            }
        } else if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }
}
