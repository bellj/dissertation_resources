package X;

import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.view.DragEvent;
import android.view.View;
import com.facebook.redex.IDxCListenerShape4S0200000_2_I1;
import com.facebook.redex.IDxCListenerShape9S0100000_2_I1;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.3MX  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3MX implements View.OnDragListener {
    public final Activity A00;
    public final AnonymousClass3UW A01;

    public AnonymousClass3MX(Context context, AnonymousClass3UW r3) {
        this.A01 = r3;
        this.A00 = AnonymousClass12P.A00(context);
    }

    @Override // android.view.View.OnDragListener
    public boolean onDrag(View view, DragEvent dragEvent) {
        int i;
        String str;
        ClipData.Item itemAt;
        int action = dragEvent.getAction();
        if (action != 1) {
            if (action == 3) {
                if (Build.VERSION.SDK_INT >= 24) {
                    this.A00.requestDragAndDropPermissions(dragEvent);
                }
                AnonymousClass3UW r2 = this.A01;
                ClipData clipData = dragEvent.getClipData();
                if (clipData == null || clipData.getDescription() == null) {
                    r2.A02.A07(R.string.share_failed, 0);
                    return true;
                } else if (clipData.getDescription().hasMimeType("text/plain")) {
                    if (clipData.getItemCount() != 1 || (itemAt = clipData.getItemAt(0)) == null || itemAt.getText() == null) {
                        str = "";
                    } else {
                        str = itemAt.getText().toString();
                    }
                    if (!TextUtils.isEmpty(str)) {
                        r2.A09.setText(str);
                        return true;
                    }
                } else {
                    ArrayList A0l = C12960it.A0l();
                    for (int i2 = 0; i2 < clipData.getItemCount(); i2++) {
                        ClipData.Item itemAt2 = clipData.getItemAt(i2);
                        if (!(itemAt2 == null || itemAt2.getUri() == null)) {
                            A0l.add(itemAt2.getUri());
                        }
                    }
                    Iterator it = A0l.iterator();
                    while (it.hasNext()) {
                        if (r2.A0A.A04((Uri) it.next()) == 9) {
                            C15370n3 A0A = r2.A04.A0A(r2.A08);
                            C15610nY r7 = r2.A05;
                            C63273Ay.A00(r2.A01, new DialogInterface.OnCancelListener() { // from class: X.4f2
                                @Override // android.content.DialogInterface.OnCancelListener
                                public final void onCancel(DialogInterface dialogInterface) {
                                    C36021jC.A00(AnonymousClass3UW.this.A01, 1);
                                }
                            }, new IDxCListenerShape4S0200000_2_I1(A0l, 5, r2), new IDxCListenerShape9S0100000_2_I1(r2, 29), r7, A0A, r2.A07, A0l, null).show();
                            return true;
                        }
                    }
                    r2.A00(A0l);
                    return true;
                }
            } else if (action != 4) {
                i = -2131824914;
                if (action != 5) {
                    if (action != 6) {
                        return false;
                    }
                }
                view.setBackgroundColor(i);
                return true;
            } else {
                view.setBackgroundColor(0);
            }
            return true;
        }
        view.setVisibility(0);
        i = -2134061876;
        view.setBackgroundColor(i);
        return true;
    }
}
