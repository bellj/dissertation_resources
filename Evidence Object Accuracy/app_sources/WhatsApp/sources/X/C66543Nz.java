package X;

import android.view.View;
import android.widget.AbsListView;
import com.whatsapp.contact.picker.ContactPickerFragment;

/* renamed from: X.3Nz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C66543Nz implements AbsListView.OnScrollListener {
    public final /* synthetic */ ContactPickerFragment A00;

    @Override // android.widget.AbsListView.OnScrollListener
    public void onScrollStateChanged(AbsListView absListView, int i) {
    }

    public C66543Nz(ContactPickerFragment contactPickerFragment) {
        this.A00 = contactPickerFragment;
    }

    @Override // android.widget.AbsListView.OnScrollListener
    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        ContactPickerFragment contactPickerFragment = this.A00;
        View view = contactPickerFragment.A0A;
        if (view == null) {
            return;
        }
        if (i != 0) {
            view.setVisibility(8);
        } else if (contactPickerFragment.A2a.containsKey(AnonymousClass1VX.A00)) {
            contactPickerFragment.A0A.setVisibility(0);
            View childAt = absListView.getChildAt(0);
            if (childAt != null) {
                contactPickerFragment.A0A.setAlpha(((float) (childAt.getHeight() + childAt.getTop())) / C12990iw.A03(childAt));
                contactPickerFragment.A0A.setTranslationY((float) childAt.getTop());
            }
        }
    }
}
