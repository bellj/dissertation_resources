package X;

import android.os.Build;
import com.facebook.redex.RunnableBRunnable0Shape0S2300100_I1;
import com.whatsapp.biz.product.view.activity.ProductDetailActivity;
import com.whatsapp.jid.UserJid;

/* renamed from: X.2fi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53912fi extends AnonymousClass015 {
    public boolean A00;
    public boolean A01;
    public final int A02;
    public final AnonymousClass016 A03;
    public final AnonymousClass016 A04;
    public final AnonymousClass016 A05 = C12980iv.A0T();
    public final AnonymousClass016 A06;
    public final AnonymousClass016 A07;
    public final AnonymousClass016 A08;
    public final AnonymousClass016 A09;
    public final AnonymousClass016 A0A;
    public final C15450nH A0B;
    public final AnonymousClass19T A0C;
    public final C68263Us A0D;
    public final AnonymousClass4E0 A0E;
    public final C18640sm A0F;
    public final C14820m6 A0G;
    public final UserJid A0H;
    public final C27691It A0I;

    public C53912fi(C15450nH r7, AnonymousClass19T r8, C68263Us r9, AnonymousClass4E0 r10, C18640sm r11, C14820m6 r12, UserJid userJid, int i) {
        AnonymousClass016 A0T = C12980iv.A0T();
        this.A06 = A0T;
        AnonymousClass016 A0T2 = C12980iv.A0T();
        this.A09 = A0T2;
        AnonymousClass016 A0T3 = C12980iv.A0T();
        this.A04 = A0T3;
        AnonymousClass016 A0T4 = C12980iv.A0T();
        this.A08 = A0T4;
        AnonymousClass016 A0T5 = C12980iv.A0T();
        this.A03 = A0T5;
        this.A07 = C12980iv.A0T();
        this.A0A = C12980iv.A0T();
        this.A0I = C13000ix.A03();
        this.A0E = r10;
        this.A0H = userJid;
        this.A0B = r7;
        this.A0D = r9;
        this.A0F = r11;
        this.A02 = i;
        this.A0C = r8;
        this.A0G = r12;
        r9.A02 = A0T;
        r9.A01 = A0T3;
        r9.A04 = A0T2;
        r9.A00 = A0T5;
        r9.A03 = A0T4;
    }

    public void A04(C44691zO r11, UserJid userJid, String str, String str2, long j) {
        if (r11 == null) {
            this.A06.A0A(Boolean.TRUE);
            return;
        }
        C68263Us r2 = this.A0D;
        r2.A0A.Ab2(new RunnableBRunnable0Shape0S2300100_I1(r2, userJid, r11, str, str2, 1, j));
    }

    public void A05(ProductDetailActivity productDetailActivity) {
        if (productDetailActivity.getIntent().getBooleanExtra("is_from_product_detail_screen", false)) {
            if (Build.VERSION.SDK_INT < 29) {
                productDetailActivity.onStateNotSaved();
            }
            this.A05.A0B(Boolean.TRUE);
            return;
        }
        productDetailActivity.onBackPressed();
    }

    public boolean A06(C44691zO r3, int i) {
        C44731zS r0;
        if (r3 == null || !r3.A0F || i != 0 || r3.A02() || (r0 = r3.A01) == null || r0.A00 != 0 || r3.A07) {
            return false;
        }
        return true;
    }
}
