package X;

import android.app.RemoteInput;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import java.util.Set;

/* renamed from: X.03o  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C007103o {
    public final Bundle A00;
    public final CharSequence A01;
    public final String A02;
    public final Set A03;
    public final boolean A04 = true;
    public final CharSequence[] A05;

    public C007103o(Bundle bundle, CharSequence charSequence, String str, Set set, CharSequence[] charSequenceArr) {
        this.A02 = str;
        this.A01 = charSequence;
        this.A05 = charSequenceArr;
        this.A00 = bundle;
        this.A03 = set;
    }

    public static Bundle A00(Intent intent) {
        Intent intent2;
        if (Build.VERSION.SDK_INT >= 20) {
            return C05610Qg.A01(intent);
        }
        ClipData clipData = intent.getClipData();
        if (clipData != null) {
            ClipDescription description = clipData.getDescription();
            if (description.hasMimeType("text/vnd.android.intent") && description.getLabel().toString().contentEquals("android.remoteinput.results") && (intent2 = clipData.getItemAt(0).getIntent()) != null) {
                return (Bundle) intent2.getExtras().getParcelable("android.remoteinput.resultsData");
            }
        }
        return null;
    }

    public static RemoteInput[] A01(C007103o[] r4) {
        int length = r4.length;
        RemoteInput[] remoteInputArr = new RemoteInput[length];
        for (int i = 0; i < length; i++) {
            remoteInputArr[i] = C05610Qg.A00(r4[i]);
        }
        return remoteInputArr;
    }
}
