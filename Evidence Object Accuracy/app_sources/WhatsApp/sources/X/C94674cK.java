package X;

import android.util.Log;

/* renamed from: X.4cK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C94674cK {
    public static final int[] A00 = {0, 1, 2, 3, 4, 5, 6, 8, -1, -1, -1, 7, 8, -1, 8, -1};
    public static final int[] A01 = {96000, 88200, 64000, 48000, 44100, 32000, 24000, 22050, 16000, 12000, 11025, 8000, 7350};

    public static int A00(int i) {
        if (i == 2) {
            return 10;
        }
        if (i == 5) {
            return 11;
        }
        if (i == 29) {
            return 12;
        }
        if (i == 42) {
            return 16;
        }
        if (i != 22) {
            return i != 23 ? 0 : 15;
        }
        return 1073741824;
    }

    public static C90704Ox A01(C95054d0 r9, boolean z) {
        int i;
        int A04 = r9.A04(5);
        if (A04 == 31) {
            A04 = r9.A04(6) + 32;
        }
        int A042 = r9.A04(4);
        if (A042 == 15) {
            i = r9.A04(24);
        } else if (A042 < 13) {
            i = A01[A042];
        } else {
            throw new AnonymousClass496();
        }
        int A043 = r9.A04(4);
        String A0W = C12960it.A0W(A04, "mp4a.40.");
        if (A04 == 5 || A04 == 29) {
            int A044 = r9.A04(4);
            if (A044 == 15) {
                i = r9.A04(24);
            } else if (A044 < 13) {
                i = A01[A044];
            } else {
                throw new AnonymousClass496();
            }
            A04 = r9.A04(5);
            if (A04 == 31) {
                A04 = r9.A04(6) + 32;
            }
            if (A04 == 22) {
                A043 = r9.A04(4);
            }
        }
        if (z) {
            if (!(A04 == 1 || A04 == 2 || A04 == 3 || A04 == 4 || A04 == 6 || A04 == 7 || A04 == 17)) {
                switch (A04) {
                    case 19:
                    case C43951xu.A01:
                    case 21:
                    case 22:
                    case 23:
                        break;
                    default:
                        throw AnonymousClass496.A00(C12960it.A0W(A04, "Unsupported audio object type: "));
                }
            }
            if (r9.A0C()) {
                Log.w("AacUtil", "Unexpected frameLengthFlag = 1");
            }
            if (r9.A0C()) {
                r9.A08(14);
            }
            boolean A0C = r9.A0C();
            if (A043 != 0) {
                if (A04 == 6 || A04 == 20) {
                    r9.A08(3);
                }
                if (A0C) {
                    if (A04 == 22) {
                        r9.A08(16);
                    } else if (A04 == 17 || A04 == 19 || A04 == 20 || A04 == 23) {
                        r9.A08(3);
                    }
                    r9.A08(1);
                }
                switch (A04) {
                    case 17:
                    case 19:
                    case C43951xu.A01:
                    case 21:
                    case 22:
                    case 23:
                        int A045 = r9.A04(2);
                        if (A045 == 2 || A045 == 3) {
                            throw AnonymousClass496.A00(C12960it.A0W(A045, "Unsupported epConfig: "));
                        }
                }
            } else {
                throw C12970iu.A0z();
            }
        }
        int i2 = A00[A043];
        if (i2 != -1) {
            return new C90704Ox(i, A0W, i2);
        }
        throw new AnonymousClass496();
    }
}
