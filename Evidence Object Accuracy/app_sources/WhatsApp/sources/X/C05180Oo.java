package X;

import android.os.Build;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/* renamed from: X.0Oo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C05180Oo {
    public final int A00;
    public final int A01;
    public final int A02;
    public final AbstractC11410gF A03 = new C07600Zk();
    public final AnonymousClass0S6 A04 = new AnonymousClass0S6();
    public final Executor A05 = Executors.newFixedThreadPool(Math.max(2, Math.min(Runtime.getRuntime().availableProcessors() - 1, 4)), new ThreadFactoryC10670f0(this, false));
    public final Executor A06 = Executors.newFixedThreadPool(Math.max(2, Math.min(Runtime.getRuntime().availableProcessors() - 1, 4)), new ThreadFactoryC10670f0(this, true));

    public C05180Oo(AnonymousClass0NM r4) {
        this.A00 = r4.A00;
        this.A02 = r4.A02;
        this.A01 = r4.A01;
    }

    public int A00() {
        if (Build.VERSION.SDK_INT == 23) {
            return 10;
        }
        return 20;
    }
}
