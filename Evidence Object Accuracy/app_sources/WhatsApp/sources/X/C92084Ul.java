package X;

import java.io.EOFException;

/* renamed from: X.4Ul  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C92084Ul {
    public final C95304dT A00 = C95304dT.A05(10);

    public C100624mD A00(AnonymousClass5Yf r10, AnonymousClass5SP r11) {
        C100624mD r8 = null;
        int i = 0;
        while (true) {
            try {
                C95304dT r5 = this.A00;
                r10.AZ4(r5.A02, 0, 10);
                r5.A0S(0);
                if (r5.A0D() != 4801587) {
                    break;
                }
                r5.A0T(3);
                int A0B = r5.A0B();
                int i2 = A0B + 10;
                if (r8 == null) {
                    byte[] bArr = new byte[i2];
                    System.arraycopy(r5.A02, 0, bArr, 0, 10);
                    r10.AZ4(bArr, 10, A0B);
                    r8 = new C76993mZ(r11).A06(bArr, i2);
                } else {
                    r10.A5r(A0B);
                }
                i += i2;
            } catch (EOFException unused) {
            }
        }
        r10.Aaj();
        r10.A5r(i);
        return r8;
    }
}
