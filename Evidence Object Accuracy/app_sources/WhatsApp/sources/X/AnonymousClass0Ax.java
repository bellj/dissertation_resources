package X;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.XmlResourceParser;
import android.util.Xml;
import android.view.InflateException;
import android.view.Menu;
import android.view.MenuInflater;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;

/* renamed from: X.0Ax  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Ax extends MenuInflater {
    public static final Class[] A04;
    public static final Class[] A05;
    public Context A00;
    public Object A01;
    public final Object[] A02;
    public final Object[] A03;

    static {
        Class[] clsArr = {Context.class};
        A05 = clsArr;
        A04 = clsArr;
    }

    public AnonymousClass0Ax(Context context) {
        super(context);
        this.A00 = context;
        Object[] objArr = {context};
        this.A03 = objArr;
        this.A02 = objArr;
    }

    public final Object A00(Object obj) {
        if ((obj instanceof Activity) || !(obj instanceof ContextWrapper)) {
            return obj;
        }
        return A00(((ContextWrapper) obj).getBaseContext());
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v37, resolved type: int */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x01db  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x01f2  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x01ff  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0202  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A01(android.util.AttributeSet r18, android.view.Menu r19, org.xmlpull.v1.XmlPullParser r20) {
        /*
        // Method dump skipped, instructions count: 615
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0Ax.A01(android.util.AttributeSet, android.view.Menu, org.xmlpull.v1.XmlPullParser):void");
    }

    @Override // android.view.MenuInflater
    public void inflate(int i, Menu menu) {
        XmlResourceParser xmlResourceParser;
        if (!(menu instanceof AnonymousClass07I)) {
            super.inflate(i, menu);
            return;
        }
        try {
            xmlResourceParser = null;
            try {
                try {
                    xmlResourceParser = this.A00.getResources().getLayout(i);
                    A01(Xml.asAttributeSet(xmlResourceParser), menu, xmlResourceParser);
                } catch (XmlPullParserException e) {
                    throw new InflateException("Error inflating menu XML", e);
                }
            } catch (IOException e2) {
                throw new InflateException("Error inflating menu XML", e2);
            }
        } finally {
            if (xmlResourceParser != null) {
                xmlResourceParser.close();
            }
        }
    }
}
