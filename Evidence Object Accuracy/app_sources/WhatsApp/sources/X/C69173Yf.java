package X;

import com.whatsapp.util.Log;

/* renamed from: X.3Yf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69173Yf implements AnonymousClass2S1 {
    public final /* synthetic */ AnonymousClass2S8 A00;

    public C69173Yf(AnonymousClass2S8 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass2S1
    public void APk() {
        Log.e("PAY: PaymentIncentiveManager/refreshUserClaimInfo/processSuccessfulGetClaimInfo failed");
        AnonymousClass2S1 r0 = this.A00.A01;
        if (r0 != null) {
            r0.APk();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001d, code lost:
        if (r2.A02 != false) goto L_0x001f;
     */
    @Override // X.AnonymousClass2S1
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AX0(X.C50932Rx r7) {
        /*
            r6 = this;
            X.2S8 r5 = r6.A00
            X.17Z r4 = r5.A02
            X.2Ry r2 = r4.A02()
            X.0m9 r1 = r4.A09
            r0 = 889(0x379, float:1.246E-42)
            boolean r0 = r1.A07(r0)
            if (r0 == 0) goto L_0x001f
            boolean r0 = r4.A0A()
            if (r0 == 0) goto L_0x001f
            if (r2 == 0) goto L_0x001f
            boolean r0 = r2.A02
            r3 = 1
            if (r0 == 0) goto L_0x0020
        L_0x001f:
            r3 = 0
        L_0x0020:
            X.0lR r2 = r4.A0G
            r1 = 8
            com.facebook.redex.RunnableBRunnable0Shape1S0110000_I1 r0 = new com.facebook.redex.RunnableBRunnable0Shape1S0110000_I1
            r0.<init>(r6, r1, r3)
            r2.Ab2(r0)
            X.2S1 r0 = r5.A01
            if (r0 == 0) goto L_0x0033
            r0.AX0(r7)
        L_0x0033:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C69173Yf.AX0(X.2Rx):void");
    }
}
