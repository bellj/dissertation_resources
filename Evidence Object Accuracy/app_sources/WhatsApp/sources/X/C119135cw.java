package X;

import android.view.View;
import com.whatsapp.payments.ui.ViralityLinkVerifierActivity;

/* renamed from: X.5cw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119135cw extends AnonymousClass2UH {
    public final /* synthetic */ ViralityLinkVerifierActivity A00;

    @Override // X.AnonymousClass2UH
    public void A00(View view, float f) {
    }

    public C119135cw(ViralityLinkVerifierActivity viralityLinkVerifierActivity) {
        this.A00 = viralityLinkVerifierActivity;
    }

    @Override // X.AnonymousClass2UH
    public void A01(View view, int i) {
        if (i == 5 || i == 4) {
            this.A00.finish();
        }
    }
}
