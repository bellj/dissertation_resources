package X;

import android.content.Context;
import android.widget.FrameLayout;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.biz.catalog.view.AspectRatioFrameLayout;
import com.whatsapp.search.views.MessageThumbView;

/* renamed from: X.34f  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass34f extends AbstractC84543zT {
    public boolean A00;
    public final WaImageView A01 = C12980iv.A0X(this, R.id.kept_status);
    public final WaImageView A02 = C12980iv.A0X(this, R.id.starred_status);
    public final MessageThumbView A03;

    public AnonymousClass34f(Context context) {
        super(context);
        A01();
        ((AspectRatioFrameLayout) this).A00 = 1.0f;
        FrameLayout.inflate(context, R.layout.search_message_image_preview, this);
        MessageThumbView messageThumbView = (MessageThumbView) AnonymousClass028.A0D(this, R.id.thumb_view);
        this.A03 = messageThumbView;
        C12960it.A0r(context, messageThumbView, R.string.image_preview_description);
    }

    @Override // X.AbstractC74163hQ
    public void A01() {
        if (!this.A00) {
            this.A00 = true;
            ((AbstractC84543zT) this).A01 = C12960it.A0S(AnonymousClass2P6.A00(generatedComponent()));
        }
    }

    public void setMessage(AnonymousClass1X7 r6) {
        super.A02 = r6;
        WaImageView waImageView = this.A02;
        WaImageView waImageView2 = this.A01;
        if (r6 != null) {
            if (r6.A0v) {
                waImageView.setVisibility(0);
            } else {
                waImageView.setVisibility(8);
            }
            waImageView2.setVisibility(8);
        }
        MessageThumbView messageThumbView = this.A03;
        messageThumbView.A00 = ((AbstractC84543zT) this).A00;
        messageThumbView.setMessage(r6);
    }
}
