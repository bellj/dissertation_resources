package X;

/* renamed from: X.12S  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass12S {
    public final C16120oU A00;
    public final AnonymousClass12N A01;

    public AnonymousClass12S(C16120oU r1, AnonymousClass12N r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public final void A00(C43831xf r4, Integer num) {
        if (r4 != null) {
            AnonymousClass43Z r2 = new AnonymousClass43Z();
            r2.A02 = Long.valueOf((long) r4.A00);
            r2.A01 = Long.valueOf((long) r4.A02);
            r2.A00 = num;
            this.A00.A07(r2);
        }
    }

    public final void A01(Integer num) {
        A00(this.A01.A01(), num);
    }

    public final void A02(Integer num) {
        C43831xf A01 = this.A01.A01();
        if (A01 != null) {
            C854943a r2 = new C854943a();
            r2.A02 = Long.valueOf((long) A01.A00);
            r2.A01 = Long.valueOf((long) A01.A02);
            r2.A00 = num;
            this.A00.A07(r2);
        }
    }
}
