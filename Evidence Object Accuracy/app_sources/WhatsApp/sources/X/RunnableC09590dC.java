package X;

import androidx.recyclerview.widget.RecyclerView;
import java.util.List;

/* renamed from: X.0dC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09590dC implements Runnable {
    public final /* synthetic */ C06590Ug A00;
    public final /* synthetic */ AnonymousClass0F6 A01;

    public RunnableC09590dC(C06590Ug r1, AnonymousClass0F6 r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        AnonymousClass0F6 r4 = this.A01;
        RecyclerView recyclerView = r4.A0M;
        if (recyclerView != null && recyclerView.A0j) {
            C06590Ug r1 = this.A00;
            if (!(r1.A05 || r1.A0C.A00() == -1)) {
                AnonymousClass04Y r0 = recyclerView.A0R;
                if (r0 == null || !r0.A0B()) {
                    List list = r4.A0O;
                    int size = list.size();
                    for (int i = 0; i < size; i++) {
                        if (((C06590Ug) list.get(i)).A03) {
                        }
                    }
                    return;
                }
                r4.A0M.post(this);
            }
        }
    }
}
