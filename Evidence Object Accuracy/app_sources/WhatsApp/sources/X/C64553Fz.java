package X;

import com.whatsapp.util.Log;
import java.util.Set;

/* renamed from: X.3Fz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C64553Fz {
    public final AbstractC17940re A00;
    public final AnonymousClass4EZ A01;

    public C64553Fz(AnonymousClass4EZ r2, Set set) {
        this.A00 = AbstractC17940re.copyOf(set);
        this.A01 = r2;
    }

    public void A00(AnonymousClass3B9 r4) {
        AnonymousClass1I5 it = this.A00.iterator();
        while (it.hasNext()) {
            try {
                ((C64553Fz) it.next()).A00(r4);
            } catch (Exception e) {
                Log.e("AutoconfManagerConsumerImpl/preload/feo2/soft_error", e);
            }
        }
    }

    public void A01(AnonymousClass3CC r4) {
        AnonymousClass1I5 it = this.A00.iterator();
        while (it.hasNext()) {
            try {
                ((C64553Fz) it.next()).A01(r4);
            } catch (Exception e) {
                Log.e("AutoconfManagerConsumerImpl/preload/feo2/soft_error", e);
            }
        }
    }

    public void A02(AnonymousClass4IA r4) {
        AnonymousClass1I5 it = this.A00.iterator();
        while (it.hasNext()) {
            try {
                ((C64553Fz) it.next()).A02(r4);
            } catch (Exception e) {
                Log.e("AutoconfManagerConsumerImpl/preload/feo2/soft_error", e);
            }
        }
    }

    public void A03(C92054Ui r4) {
        AnonymousClass1I5 it = this.A00.iterator();
        while (it.hasNext()) {
            try {
                ((C64553Fz) it.next()).A03(r4);
            } catch (Exception e) {
                Log.e("AutoconfManagerConsumerImpl/preload/feo2/soft_error", e);
            }
        }
    }

    public void A04(AnonymousClass3BA r4) {
        AnonymousClass1I5 it = this.A00.iterator();
        while (it.hasNext()) {
            try {
                ((C64553Fz) it.next()).A04(r4);
            } catch (Exception e) {
                Log.e("AutoconfManagerConsumerImpl/preload/feo2/soft_error", e);
            }
        }
    }

    public void A05(AnonymousClass3CD r4) {
        AnonymousClass1I5 it = this.A00.iterator();
        while (it.hasNext()) {
            try {
                ((C64553Fz) it.next()).A05(r4);
            } catch (Exception e) {
                Log.e("AutoconfManagerConsumerImpl/preload/feo2/soft_error", e);
            }
        }
    }

    public void A06(AnonymousClass3BB r4) {
        AnonymousClass1I5 it = this.A00.iterator();
        while (it.hasNext()) {
            try {
                ((C64553Fz) it.next()).A06(r4);
            } catch (Exception e) {
                Log.e("AutoconfManagerConsumerImpl/preload/feo2/soft_error", e);
            }
        }
    }

    public void A07(AnonymousClass3CE r4) {
        AnonymousClass1I5 it = this.A00.iterator();
        while (it.hasNext()) {
            try {
                ((C64553Fz) it.next()).A07(r4);
            } catch (Exception e) {
                Log.e("AutoconfManagerConsumerImpl/preload/feo2/soft_error", e);
            }
        }
    }

    public void A08(Throwable th) {
        AnonymousClass1I5 it = this.A00.iterator();
        while (it.hasNext()) {
            try {
                ((C64553Fz) it.next()).A08(th);
            } catch (Exception e) {
                Log.e("AutoconfManagerConsumerImpl/preload/feo2/soft_error", e);
            }
        }
    }

    public void A09(Throwable th) {
        AnonymousClass1I5 it = this.A00.iterator();
        while (it.hasNext()) {
            try {
                ((C64553Fz) it.next()).A09(th);
            } catch (Exception e) {
                Log.e("AutoconfManagerConsumerImpl/preload/feo2/soft_error", e);
            }
        }
    }

    public void A0A(Throwable th) {
        AnonymousClass1I5 it = this.A00.iterator();
        while (it.hasNext()) {
            try {
                ((C64553Fz) it.next()).A0A(th);
            } catch (Exception e) {
                Log.e("AutoconfManagerConsumerImpl/preload/feo2/soft_error", e);
            }
        }
    }

    public void A0B(Throwable th) {
        AnonymousClass1I5 it = this.A00.iterator();
        while (it.hasNext()) {
            try {
                ((C64553Fz) it.next()).A0B(th);
            } catch (Exception e) {
                Log.e("AutoconfManagerConsumerImpl/preload/feo2/soft_error", e);
            }
        }
    }
}
