package X;

import android.text.TextUtils;
import android.util.Base64;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1V8  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1V8 {
    public final String A00;
    public final byte[] A01;
    public final AnonymousClass1W9[] A02;
    public final AnonymousClass1V8[] A03;

    public AnonymousClass1V8(AnonymousClass1V8 r4, String str, AnonymousClass1W9[] r6) {
        this(str, null, r6, r4 != null ? new AnonymousClass1V8[]{r4} : null);
    }

    public AnonymousClass1V8(String str, String str2, AnonymousClass1W9[] r5) {
        this(str, str2 != null ? str2.getBytes() : null, r5, null);
    }

    public AnonymousClass1V8(String str, byte[] bArr, AnonymousClass1W9[] r4) {
        this(str, bArr, r4, null);
    }

    public AnonymousClass1V8(String str, byte[] bArr, AnonymousClass1W9[] r5, AnonymousClass1V8[] r6) {
        AnonymousClass009.A05(str);
        this.A00 = str;
        this.A02 = r5;
        this.A03 = r6;
        this.A01 = bArr;
        if (r6 != null && bArr != null) {
            throw new IllegalArgumentException("node may not have both data and children");
        }
    }

    public AnonymousClass1V8(String str, AnonymousClass1W9[] r3) {
        this(str, null, r3, null);
    }

    public AnonymousClass1V8(String str, AnonymousClass1W9[] r3, AnonymousClass1V8[] r4) {
        this(str, null, r3, r4);
    }

    public static AnonymousClass1V8 A00(Jid jid, AnonymousClass1V8 r3, AnonymousClass1W9[] r4, int i) {
        r4[i] = new AnonymousClass1W9(jid, "to");
        return new AnonymousClass1V8(r3, "iq", r4);
    }

    public static void A01(AnonymousClass1V8 r2, String str) {
        if (!A02(r2, str)) {
            StringBuilder sb = new StringBuilder("failed requireTag: node: ");
            sb.append(r2);
            sb.append(" tag: ");
            sb.append(str);
            throw new AnonymousClass1V9(sb.toString());
        }
    }

    public static boolean A02(AnonymousClass1V8 r0, String str) {
        return r0 != null && r0.A00.equals(str);
    }

    public static byte[] A03(AnonymousClass1V8 r3, int i) {
        byte[] bArr = r3.A01;
        if (bArr != null) {
            int length = bArr.length;
            if (length == i) {
                return bArr;
            }
            StringBuilder sb = new StringBuilder();
            sb.append("failed require. node ");
            sb.append(r3);
            sb.append(" data length ");
            sb.append(length);
            sb.append(" != required length ");
            sb.append(i);
            throw new AnonymousClass1V9(sb.toString());
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append("failed require. node ");
        sb2.append(r3);
        sb2.append(" missing data");
        throw new AnonymousClass1V9(sb2.toString());
    }

    public int A04(String str) {
        return A06(A0H(str), str);
    }

    public int A05(String str, int i) {
        String A0I = A0I(str, null);
        if (A0I == null) {
            return i;
        }
        return A06(A0I, str);
    }

    public int A06(String str, String str2) {
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException unused) {
            StringBuilder sb = new StringBuilder("attribute ");
            sb.append(str2);
            sb.append(" for tag ");
            sb.append(this.A00);
            sb.append(" is not integral: ");
            sb.append(str);
            throw new AnonymousClass1V9(sb.toString());
        }
    }

    public long A07(String str) {
        return A09(A0H(str), str);
    }

    public long A08(String str, long j) {
        String A0I = A0I(str, null);
        if (A0I == null) {
            return j;
        }
        return A09(A0I, str);
    }

    public long A09(String str, String str2) {
        try {
            return Long.parseLong(str);
        } catch (NumberFormatException unused) {
            StringBuilder sb = new StringBuilder("attribute ");
            sb.append(str2);
            sb.append(" for tag ");
            sb.append(this.A00);
            sb.append(" is not integral: ");
            sb.append(str);
            throw new AnonymousClass1V9(sb.toString());
        }
    }

    public Jid A0A(AbstractC15710nm r7, Class cls, String str) {
        AnonymousClass1W9 r1;
        Jid jid;
        int length;
        AnonymousClass1W9[] r4 = this.A02;
        if (r4 == null || (length = r4.length) <= 0) {
            r1 = null;
        } else {
            int i = 0;
            do {
                r1 = r4[i];
                if (TextUtils.equals(str, r1.A02)) {
                    break;
                }
                i++;
            } while (i < length);
            r1 = null;
        }
        if (r1 == null || (jid = r1.A01) == null) {
            jid = Jid.getNullable(A0I(str, null));
        }
        if (jid != null && !jid.isProtocolCompliant()) {
            StringBuilder sb = new StringBuilder("Jid: '");
            sb.append(jid);
            sb.append("' key: '");
            sb.append(str);
            sb.append("' tag: '");
            sb.append(this.A00);
            sb.append("'");
            r7.AaV("invalid jid!", sb.toString(), true);
        }
        if (cls == DeviceJid.class && (jid instanceof UserJid)) {
            jid = DeviceJid.of(jid);
        }
        try {
            return (Jid) cls.cast(jid);
        } catch (ClassCastException e) {
            StringBuilder sb2 = new StringBuilder("ProtocolTreeNode/getAttributeJid/failed to convert '");
            sb2.append(C15380n4.A03(jid));
            sb2.append("' to ");
            sb2.append(cls.getName());
            Log.e(sb2.toString(), e);
            r7.AaV("ProtocolTreeNode/getAttributeJid", "invalid-jid-attribute", true);
            return null;
        }
    }

    public Jid A0B(AbstractC15710nm r3, Class cls, String str) {
        Jid A0A = A0A(r3, cls, str);
        if (A0A != null) {
            return A0A;
        }
        StringBuilder sb = new StringBuilder("required attribute '");
        sb.append(str);
        sb.append("' missing for tag ");
        sb.append(this.A00);
        throw new AnonymousClass1V9(sb.toString());
    }

    public AnonymousClass1V8 A0C() {
        AnonymousClass1V8[] r1 = this.A03;
        if (r1 != null && r1.length != 0) {
            return r1[0];
        }
        StringBuilder sb = new StringBuilder("required first child missing for tag ");
        sb.append(this.A00);
        throw new AnonymousClass1V9(sb.toString());
    }

    public AnonymousClass1V8 A0D(int i) {
        AnonymousClass1V8[] r1 = this.A03;
        if (r1 == null || r1.length <= i) {
            return null;
        }
        return r1[i];
    }

    public AnonymousClass1V8 A0E(String str) {
        AnonymousClass1V8[] r5 = this.A03;
        if (r5 != null) {
            for (AnonymousClass1V8 r1 : r5) {
                if (TextUtils.equals(str, r1.A00)) {
                    return r1;
                }
            }
        }
        return null;
    }

    public AnonymousClass1V8 A0F(String str) {
        AnonymousClass1V8 A0E = A0E(str);
        if (A0E != null) {
            return A0E;
        }
        StringBuilder sb = new StringBuilder("required child ");
        sb.append(str);
        sb.append(" missing for tag ");
        sb.append(this.A00);
        throw new AnonymousClass1V9(sb.toString());
    }

    public String A0G() {
        byte[] bArr = this.A01;
        if (bArr == null) {
            return null;
        }
        try {
            return new String(bArr, AnonymousClass01V.A08);
        } catch (UnsupportedEncodingException unused) {
            return null;
        }
    }

    public String A0H(String str) {
        String A0I = A0I(str, null);
        if (A0I != null) {
            return A0I;
        }
        StringBuilder sb = new StringBuilder("required attribute '");
        sb.append(str);
        sb.append("' missing for tag ");
        sb.append(this.A00);
        throw new AnonymousClass1V9(sb.toString());
    }

    public String A0I(String str, String str2) {
        int length;
        AnonymousClass1W9[] r4 = this.A02;
        if (r4 == null || (length = r4.length) <= 0) {
            return str2;
        }
        int i = 0;
        do {
            AnonymousClass1W9 r1 = r4[i];
            if (TextUtils.equals(str, r1.A02)) {
                return r1.A03;
            }
            i++;
        } while (i < length);
        return str2;
    }

    public List A0J(String str) {
        AnonymousClass1V8[] r5 = this.A03;
        if (r5 == null) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList();
        for (AnonymousClass1V8 r1 : r5) {
            if (TextUtils.equals(str, r1.A00)) {
                arrayList.add(r1);
            }
        }
        return arrayList;
    }

    public final List A0K(List list) {
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            ArrayList arrayList2 = new ArrayList((Collection) it.next());
            if (arrayList2.size() > 1 && ((String) arrayList2.get(0)).equals(this.A00)) {
                arrayList2.remove(0);
                arrayList.add(arrayList2);
            }
        }
        return arrayList;
    }

    public AnonymousClass1W9[] A0L() {
        AnonymousClass1W9[] r1 = this.A02;
        if (r1 == null || r1.length != 0) {
            return r1;
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0041, code lost:
        if (r0 == null) goto L_0x0043;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x005f, code lost:
        r3 = r3 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0067, code lost:
        if (r11.A03 == null) goto L_0x0069;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r11) {
        /*
            r10 = this;
            r9 = 1
            if (r10 == r11) goto L_0x0084
            r8 = 0
            if (r11 == 0) goto L_0x0083
            java.lang.Class r1 = r10.getClass()
            java.lang.Class r0 = r11.getClass()
            if (r1 != r0) goto L_0x0083
            X.1V8 r11 = (X.AnonymousClass1V8) r11
            java.lang.String r1 = r10.A00
            java.lang.String r0 = r11.A00
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0083
            X.1W9[] r5 = r10.A02
            X.1W9[] r0 = r11.A02
            if (r5 == 0) goto L_0x0041
            if (r0 == 0) goto L_0x0083
            int r4 = r5.length
            int r0 = r0.length
            if (r4 != r0) goto L_0x0083
            r3 = 0
        L_0x0029:
            if (r3 >= r4) goto L_0x0043
            r2 = r5[r3]
            java.lang.String r1 = r2.A02
            r0 = 0
            java.lang.String r1 = r11.A0I(r1, r0)
            if (r1 == 0) goto L_0x0083
            java.lang.String r0 = r2.A03
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0083
            int r3 = r3 + 1
            goto L_0x0029
        L_0x0041:
            if (r0 != 0) goto L_0x0083
        L_0x0043:
            X.1V8[] r7 = r10.A03
            if (r7 == 0) goto L_0x0065
            X.1V8[] r6 = r11.A03
            if (r6 == 0) goto L_0x0083
            int r5 = r7.length
            int r4 = r6.length
            if (r5 != r4) goto L_0x0083
            r3 = 0
        L_0x0050:
            if (r3 >= r5) goto L_0x0069
            r2 = r7[r3]
            r1 = 0
        L_0x0055:
            if (r1 >= r4) goto L_0x0083
            r0 = r6[r1]
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x0062
            int r3 = r3 + 1
            goto L_0x0050
        L_0x0062:
            int r1 = r1 + 1
            goto L_0x0055
        L_0x0065:
            X.1V8[] r0 = r11.A03
            if (r0 != 0) goto L_0x0083
        L_0x0069:
            byte[] r3 = r10.A01
            if (r3 == 0) goto L_0x007c
            byte[] r2 = r11.A01
            if (r2 == 0) goto L_0x0081
            int r1 = r3.length
            int r0 = r2.length
            if (r1 != r0) goto L_0x0081
            boolean r0 = java.util.Arrays.equals(r3, r2)
            if (r0 == 0) goto L_0x0081
            return r9
        L_0x007c:
            byte[] r0 = r11.A01
            if (r0 != 0) goto L_0x0081
            return r9
        L_0x0081:
            r9 = 0
            return r9
        L_0x0083:
            return r8
        L_0x0084:
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1V8.equals(java.lang.Object):boolean");
    }

    public int hashCode() {
        int hashCode;
        int i;
        int hashCode2 = (this.A00.hashCode() + 31) * 31;
        byte[] bArr = this.A01;
        int i2 = 0;
        if (bArr == null) {
            hashCode = 0;
        } else {
            hashCode = Arrays.hashCode(bArr);
        }
        int i3 = (hashCode2 + hashCode) * 31;
        AnonymousClass1V8[] r4 = this.A03;
        if (r4 == null) {
            i = 0;
        } else {
            i = 0;
            for (AnonymousClass1V8 r0 : r4) {
                if (r0 != null) {
                    i += r0.hashCode();
                }
            }
        }
        int i4 = (i3 + i) * 31;
        AnonymousClass1W9[] r3 = this.A02;
        if (r3 != null) {
            for (AnonymousClass1W9 r02 : r3) {
                if (r02 != null) {
                    i2 += r02.hashCode();
                }
            }
        }
        return i4 + i2;
    }

    public String toString() {
        String str;
        boolean z;
        String str2;
        StringBuilder sb = new StringBuilder("<");
        String str3 = this.A00;
        sb.append(str3);
        AnonymousClass1W9[] r6 = this.A02;
        if (r6 == null) {
            r6 = new AnonymousClass1W9[0];
        }
        for (AnonymousClass1W9 r1 : r6) {
            sb.append(" ");
            sb.append(r1.A02);
            sb.append("='");
            sb.append(r1.A03);
            sb.append("'");
        }
        byte[] bArr = this.A01;
        if (bArr == null && this.A03 == null) {
            str = "/>";
        } else {
            str = ">";
            sb.append(str);
            AnonymousClass1V8[] r2 = this.A03;
            if (r2 == null) {
                r2 = new AnonymousClass1V8[0];
            }
            for (AnonymousClass1V8 r0 : r2) {
                if (r0 != null) {
                    sb.append(r0.toString());
                }
            }
            if (bArr != null) {
                try {
                    AnonymousClass01V.A0A.newDecoder().decode(ByteBuffer.wrap(bArr));
                    z = true;
                } catch (CharacterCodingException unused) {
                    z = false;
                }
                if (z) {
                    try {
                        str2 = new String(bArr, AnonymousClass01V.A08);
                    } catch (UnsupportedEncodingException unused2) {
                        str2 = null;
                    }
                } else {
                    str2 = Base64.encodeToString(bArr, 2);
                }
                sb.append(str2);
            }
            sb.append("</");
            sb.append(str3);
        }
        sb.append(str);
        return sb.toString();
    }
}
