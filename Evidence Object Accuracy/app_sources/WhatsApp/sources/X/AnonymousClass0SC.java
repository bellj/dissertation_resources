package X;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/* renamed from: X.0SC  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0SC {
    public static Executor A02;
    public static final Object A03 = new Object();
    public Executor A00;
    public final AnonymousClass02K A01;

    public AnonymousClass0SC(AnonymousClass02K r1) {
        this.A01 = r1;
    }

    public C04770Mz A00() {
        Executor executor = this.A00;
        if (executor == null) {
            synchronized (A03) {
                if (A02 == null) {
                    A02 = Executors.newFixedThreadPool(2);
                }
            }
            executor = A02;
            this.A00 = executor;
        }
        return new C04770Mz(this.A01, executor);
    }
}
