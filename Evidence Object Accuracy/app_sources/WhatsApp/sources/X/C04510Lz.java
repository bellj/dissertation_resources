package X;

/* renamed from: X.0Lz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public /* synthetic */ class C04510Lz {
    public static final /* synthetic */ int[] A00;

    static {
        int[] iArr = new int[EnumC03880Jm.values().length];
        A00 = iArr;
        try {
            iArr[EnumC03880Jm.x.ordinal()] = 1;
        } catch (NoSuchFieldError unused) {
        }
        try {
            iArr[EnumC03880Jm.y.ordinal()] = 2;
        } catch (NoSuchFieldError unused2) {
        }
        try {
            iArr[EnumC03880Jm.width.ordinal()] = 3;
        } catch (NoSuchFieldError unused3) {
        }
        try {
            iArr[EnumC03880Jm.height.ordinal()] = 4;
        } catch (NoSuchFieldError unused4) {
        }
        try {
            iArr[EnumC03880Jm.version.ordinal()] = 5;
        } catch (NoSuchFieldError unused5) {
        }
        try {
            iArr[EnumC03880Jm.href.ordinal()] = 6;
        } catch (NoSuchFieldError unused6) {
        }
        try {
            iArr[EnumC03880Jm.preserveAspectRatio.ordinal()] = 7;
        } catch (NoSuchFieldError unused7) {
        }
        try {
            iArr[EnumC03880Jm.d.ordinal()] = 8;
        } catch (NoSuchFieldError unused8) {
        }
        try {
            iArr[EnumC03880Jm.pathLength.ordinal()] = 9;
        } catch (NoSuchFieldError unused9) {
        }
        try {
            iArr[EnumC03880Jm.rx.ordinal()] = 10;
        } catch (NoSuchFieldError unused10) {
        }
        try {
            iArr[EnumC03880Jm.ry.ordinal()] = 11;
        } catch (NoSuchFieldError unused11) {
        }
        try {
            iArr[EnumC03880Jm.cx.ordinal()] = 12;
        } catch (NoSuchFieldError unused12) {
        }
        try {
            iArr[EnumC03880Jm.cy.ordinal()] = 13;
        } catch (NoSuchFieldError unused13) {
        }
        try {
            iArr[EnumC03880Jm.r.ordinal()] = 14;
        } catch (NoSuchFieldError unused14) {
        }
        try {
            iArr[EnumC03880Jm.x1.ordinal()] = 15;
        } catch (NoSuchFieldError unused15) {
        }
        try {
            iArr[EnumC03880Jm.y1.ordinal()] = 16;
        } catch (NoSuchFieldError unused16) {
        }
        try {
            iArr[EnumC03880Jm.x2.ordinal()] = 17;
        } catch (NoSuchFieldError unused17) {
        }
        try {
            iArr[EnumC03880Jm.y2.ordinal()] = 18;
        } catch (NoSuchFieldError unused18) {
        }
        try {
            iArr[EnumC03880Jm.dx.ordinal()] = 19;
        } catch (NoSuchFieldError unused19) {
        }
        try {
            iArr[EnumC03880Jm.dy.ordinal()] = 20;
        } catch (NoSuchFieldError unused20) {
        }
        try {
            iArr[EnumC03880Jm.requiredFeatures.ordinal()] = 21;
        } catch (NoSuchFieldError unused21) {
        }
        try {
            iArr[EnumC03880Jm.requiredExtensions.ordinal()] = 22;
        } catch (NoSuchFieldError unused22) {
        }
        try {
            iArr[EnumC03880Jm.systemLanguage.ordinal()] = 23;
        } catch (NoSuchFieldError unused23) {
        }
        try {
            iArr[EnumC03880Jm.requiredFormats.ordinal()] = 24;
        } catch (NoSuchFieldError unused24) {
        }
        try {
            iArr[EnumC03880Jm.requiredFonts.ordinal()] = 25;
        } catch (NoSuchFieldError unused25) {
        }
        try {
            iArr[EnumC03880Jm.refX.ordinal()] = 26;
        } catch (NoSuchFieldError unused26) {
        }
        try {
            iArr[EnumC03880Jm.refY.ordinal()] = 27;
        } catch (NoSuchFieldError unused27) {
        }
        try {
            iArr[EnumC03880Jm.markerWidth.ordinal()] = 28;
        } catch (NoSuchFieldError unused28) {
        }
        try {
            iArr[EnumC03880Jm.markerHeight.ordinal()] = 29;
        } catch (NoSuchFieldError unused29) {
        }
        try {
            iArr[EnumC03880Jm.markerUnits.ordinal()] = 30;
        } catch (NoSuchFieldError unused30) {
        }
        try {
            iArr[EnumC03880Jm.orient.ordinal()] = 31;
        } catch (NoSuchFieldError unused31) {
        }
        try {
            iArr[EnumC03880Jm.gradientUnits.ordinal()] = 32;
        } catch (NoSuchFieldError unused32) {
        }
        try {
            iArr[EnumC03880Jm.gradientTransform.ordinal()] = 33;
        } catch (NoSuchFieldError unused33) {
        }
        try {
            iArr[EnumC03880Jm.spreadMethod.ordinal()] = 34;
        } catch (NoSuchFieldError unused34) {
        }
        try {
            iArr[EnumC03880Jm.fx.ordinal()] = 35;
        } catch (NoSuchFieldError unused35) {
        }
        try {
            iArr[EnumC03880Jm.fy.ordinal()] = 36;
        } catch (NoSuchFieldError unused36) {
        }
        try {
            iArr[EnumC03880Jm.offset.ordinal()] = 37;
        } catch (NoSuchFieldError unused37) {
        }
        try {
            iArr[EnumC03880Jm.clipPathUnits.ordinal()] = 38;
        } catch (NoSuchFieldError unused38) {
        }
        try {
            iArr[EnumC03880Jm.startOffset.ordinal()] = 39;
        } catch (NoSuchFieldError unused39) {
        }
        try {
            iArr[EnumC03880Jm.patternUnits.ordinal()] = 40;
        } catch (NoSuchFieldError unused40) {
        }
        try {
            iArr[EnumC03880Jm.patternContentUnits.ordinal()] = 41;
        } catch (NoSuchFieldError unused41) {
        }
        try {
            iArr[EnumC03880Jm.patternTransform.ordinal()] = 42;
        } catch (NoSuchFieldError unused42) {
        }
        try {
            iArr[EnumC03880Jm.maskUnits.ordinal()] = 43;
        } catch (NoSuchFieldError unused43) {
        }
        try {
            iArr[EnumC03880Jm.maskContentUnits.ordinal()] = 44;
        } catch (NoSuchFieldError unused44) {
        }
        try {
            iArr[EnumC03880Jm.style.ordinal()] = 45;
        } catch (NoSuchFieldError unused45) {
        }
        try {
            iArr[EnumC03880Jm.CLASS.ordinal()] = 46;
        } catch (NoSuchFieldError unused46) {
        }
        try {
            iArr[EnumC03880Jm.fill.ordinal()] = 47;
        } catch (NoSuchFieldError unused47) {
        }
        try {
            iArr[EnumC03880Jm.fill_rule.ordinal()] = 48;
        } catch (NoSuchFieldError unused48) {
        }
        try {
            iArr[EnumC03880Jm.fill_opacity.ordinal()] = 49;
        } catch (NoSuchFieldError unused49) {
        }
        try {
            iArr[EnumC03880Jm.stroke.ordinal()] = 50;
        } catch (NoSuchFieldError unused50) {
        }
        try {
            iArr[EnumC03880Jm.stroke_opacity.ordinal()] = 51;
        } catch (NoSuchFieldError unused51) {
        }
        try {
            iArr[EnumC03880Jm.stroke_width.ordinal()] = 52;
        } catch (NoSuchFieldError unused52) {
        }
        try {
            iArr[EnumC03880Jm.stroke_linecap.ordinal()] = 53;
        } catch (NoSuchFieldError unused53) {
        }
        try {
            iArr[EnumC03880Jm.stroke_linejoin.ordinal()] = 54;
        } catch (NoSuchFieldError unused54) {
        }
        try {
            iArr[EnumC03880Jm.stroke_miterlimit.ordinal()] = 55;
        } catch (NoSuchFieldError unused55) {
        }
        try {
            iArr[EnumC03880Jm.stroke_dasharray.ordinal()] = 56;
        } catch (NoSuchFieldError unused56) {
        }
        try {
            iArr[EnumC03880Jm.stroke_dashoffset.ordinal()] = 57;
        } catch (NoSuchFieldError unused57) {
        }
        try {
            iArr[EnumC03880Jm.opacity.ordinal()] = 58;
        } catch (NoSuchFieldError unused58) {
        }
        try {
            iArr[EnumC03880Jm.color.ordinal()] = 59;
        } catch (NoSuchFieldError unused59) {
        }
        try {
            iArr[EnumC03880Jm.font.ordinal()] = 60;
        } catch (NoSuchFieldError unused60) {
        }
        try {
            iArr[EnumC03880Jm.font_family.ordinal()] = 61;
        } catch (NoSuchFieldError unused61) {
        }
        try {
            iArr[EnumC03880Jm.font_size.ordinal()] = 62;
        } catch (NoSuchFieldError unused62) {
        }
        try {
            iArr[EnumC03880Jm.font_weight.ordinal()] = 63;
        } catch (NoSuchFieldError unused63) {
        }
        try {
            iArr[EnumC03880Jm.font_style.ordinal()] = 64;
        } catch (NoSuchFieldError unused64) {
        }
        try {
            iArr[EnumC03880Jm.text_decoration.ordinal()] = 65;
        } catch (NoSuchFieldError unused65) {
        }
        try {
            iArr[EnumC03880Jm.direction.ordinal()] = 66;
        } catch (NoSuchFieldError unused66) {
        }
        try {
            iArr[EnumC03880Jm.text_anchor.ordinal()] = 67;
        } catch (NoSuchFieldError unused67) {
        }
        try {
            iArr[EnumC03880Jm.overflow.ordinal()] = 68;
        } catch (NoSuchFieldError unused68) {
        }
        try {
            iArr[EnumC03880Jm.marker.ordinal()] = 69;
        } catch (NoSuchFieldError unused69) {
        }
        try {
            iArr[EnumC03880Jm.marker_start.ordinal()] = 70;
        } catch (NoSuchFieldError unused70) {
        }
        try {
            iArr[EnumC03880Jm.marker_mid.ordinal()] = 71;
        } catch (NoSuchFieldError unused71) {
        }
        try {
            iArr[EnumC03880Jm.marker_end.ordinal()] = 72;
        } catch (NoSuchFieldError unused72) {
        }
        try {
            iArr[EnumC03880Jm.display.ordinal()] = 73;
        } catch (NoSuchFieldError unused73) {
        }
        try {
            iArr[EnumC03880Jm.visibility.ordinal()] = 74;
        } catch (NoSuchFieldError unused74) {
        }
        try {
            iArr[EnumC03880Jm.stop_color.ordinal()] = 75;
        } catch (NoSuchFieldError unused75) {
        }
        try {
            iArr[EnumC03880Jm.stop_opacity.ordinal()] = 76;
        } catch (NoSuchFieldError unused76) {
        }
        try {
            iArr[EnumC03880Jm.clip.ordinal()] = 77;
        } catch (NoSuchFieldError unused77) {
        }
        try {
            iArr[EnumC03880Jm.clip_path.ordinal()] = 78;
        } catch (NoSuchFieldError unused78) {
        }
        try {
            iArr[EnumC03880Jm.clip_rule.ordinal()] = 79;
        } catch (NoSuchFieldError unused79) {
        }
        try {
            iArr[EnumC03880Jm.mask.ordinal()] = 80;
        } catch (NoSuchFieldError unused80) {
        }
        try {
            iArr[EnumC03880Jm.solid_color.ordinal()] = 81;
        } catch (NoSuchFieldError unused81) {
        }
        try {
            iArr[EnumC03880Jm.solid_opacity.ordinal()] = 82;
        } catch (NoSuchFieldError unused82) {
        }
        try {
            iArr[EnumC03880Jm.viewport_fill.ordinal()] = 83;
        } catch (NoSuchFieldError unused83) {
        }
        try {
            iArr[EnumC03880Jm.viewport_fill_opacity.ordinal()] = 84;
        } catch (NoSuchFieldError unused84) {
        }
        try {
            iArr[EnumC03880Jm.vector_effect.ordinal()] = 85;
        } catch (NoSuchFieldError unused85) {
        }
        try {
            iArr[EnumC03880Jm.image_rendering.ordinal()] = 86;
        } catch (NoSuchFieldError unused86) {
        }
        try {
            iArr[EnumC03880Jm.viewBox.ordinal()] = 87;
        } catch (NoSuchFieldError unused87) {
        }
        try {
            iArr[EnumC03880Jm.type.ordinal()] = 88;
        } catch (NoSuchFieldError unused88) {
        }
        try {
            iArr[EnumC03880Jm.media.ordinal()] = 89;
        } catch (NoSuchFieldError unused89) {
        }
        AnonymousClass0JX.values();
    }
}
