package X;

import android.net.Uri;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/* renamed from: X.3DU  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3DU {
    public float A00 = -3.4028235E38f;
    public float A01 = -3.4028235E38f;
    public long A02 = Long.MIN_VALUE;
    public long A03 = -9223372036854775807L;
    public long A04 = -9223372036854775807L;
    public long A05 = -9223372036854775807L;
    public Uri A06;
    public String A07;
    public List A08 = Collections.emptyList();
    public List A09 = Collections.emptyList();
    public List A0A = Collections.emptyList();
    public Map A0B = Collections.emptyMap();

    public AnonymousClass4XL A00() {
        AnonymousClass4XB r11;
        C95314dV.A04(true);
        Uri uri = this.A06;
        if (uri != null) {
            r11 = new AnonymousClass4XB(uri, null, this.A09, this.A0A);
            String str = this.A07;
            if (str == null) {
                str = uri.toString();
            }
            this.A07 = str;
        } else {
            r11 = null;
        }
        return new AnonymousClass4XL(new AnonymousClass4WP(this.A02), new AnonymousClass4XK(this.A01, this.A00, this.A05, this.A04, this.A03), r11, new AnonymousClass4WF(), this.A07);
    }
}
