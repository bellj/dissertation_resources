package X;

/* renamed from: X.2Bp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C47592Bp {
    public static final byte[][] A00;

    static {
        A00 = r3;
        byte[][] bArr = {new byte[]{87, 65, 77, 3}, new byte[]{87, 65, 77, 4}, new byte[]{87, 65, 77, 5}};
    }

    public static byte[] A00(int i) {
        if (i >= 0) {
            byte[][] bArr = A00;
            if (i <= bArr.length - 1) {
                return bArr[i];
            }
        }
        StringBuilder sb = new StringBuilder("Invalid version: ");
        sb.append(i);
        throw new RuntimeException(sb.toString());
    }
}
