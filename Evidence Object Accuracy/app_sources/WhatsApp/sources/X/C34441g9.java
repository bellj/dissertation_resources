package X;

/* renamed from: X.1g9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C34441g9 extends AnonymousClass1JQ {
    public C34441g9(AnonymousClass1JR r10, String str, long j, boolean z) {
        super(C27791Jf.A03, r10, str, "regular_low", 4, j, z);
    }

    @Override // X.AnonymousClass1JQ
    public AnonymousClass271 A01() {
        AnonymousClass271 A01 = super.A01();
        AnonymousClass009.A05(A01);
        AnonymousClass1G4 A0T = C34431g8.A02.A0T();
        A0T.A03();
        C34431g8 r2 = (C34431g8) A0T.A00;
        r2.A00 |= 1;
        r2.A01 = true;
        A01.A03();
        C27831Jk r22 = (C27831Jk) A01.A00;
        r22.A03 = (C34431g8) A0T.A02();
        r22.A00 |= 1048576;
        return A01;
    }

    @Override // X.AnonymousClass1JQ
    public String toString() {
        StringBuilder sb = new StringBuilder("AndroidUnsupportedMutation{rowId=");
        sb.append(this.A07);
        sb.append(", timestamp=");
        sb.append(this.A04);
        sb.append(", operation=");
        sb.append(this.A05);
        sb.append(", collectionName=");
        sb.append(this.A06);
        sb.append(", keyId=");
        sb.append(this.A00);
        sb.append('}');
        return sb.toString();
    }
}
