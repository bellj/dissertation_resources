package X;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;

/* renamed from: X.3ng  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC77683ng extends AnonymousClass4DM {
    @Deprecated
    public AbstractC72443eb A00(Context context, Looper looper, AbstractC14980mM r17, AbstractC15000mO r18, AnonymousClass3BW r19, Object obj) {
        if (this instanceof C77673nf) {
            return new C56382kn(context, looper, r17, r18, r19);
        }
        if (this instanceof C77663ne) {
            throw null;
        } else if (this instanceof C77653nd) {
            return new C77953o8(context, C77953o8.A00(r19), looper, r17, r18, r19);
        } else {
            if (this instanceof C77643nc) {
                return new C56372km(context, looper, r17, r18, r19);
            }
            if (this instanceof C77633nb) {
                return new C56392ko(context, looper, r17, r18, r19);
            }
            if (this instanceof C77603nY) {
                return new C77893o2(context, looper, r17, r18, r19);
            }
            if (this instanceof C77593nX) {
                return new C77923o5(context, looper, r17, r18, r19);
            }
            if (this instanceof C77583nW) {
                return new C56362kl(context, looper, (GoogleSignInOptions) obj, r17, r18, r19);
            }
            if (this instanceof C77573nV) {
                return new C77903o3(context, looper, (C108194ye) obj, r17, r18, r19);
            }
            if (this instanceof C77563nU) {
                return new C77883o1(context, looper, r17, r18, r19);
            }
            if (this instanceof C77553nT) {
                return new C77913o4(context, looper, r17, r18, r19);
            }
            if (this instanceof C77623na) {
                return new C77933o6(context, looper, r17, r18, r19);
            }
            if (this instanceof C77613nZ) {
                return new C77943o7(context, looper, r17, r18, r19, (C108174yc) obj);
            }
            throw C12980iv.A0u("buildClient must be implemented");
        }
    }
}
