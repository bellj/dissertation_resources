package X;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.os.Build;
import android.text.TextUtils;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import com.whatsapp.backup.encryptedbackup.EncBackupViewModel;
import com.whatsapp.backup.encryptedbackup.EncryptionKeyFragment;

/* renamed from: X.3MM  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3MM implements ActionMode.Callback {
    public final /* synthetic */ EncryptionKeyFragment A00;

    @Override // android.view.ActionMode.Callback
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
        return true;
    }

    @Override // android.view.ActionMode.Callback
    public void onDestroyActionMode(ActionMode actionMode) {
    }

    public AnonymousClass3MM(EncryptionKeyFragment encryptionKeyFragment) {
        this.A00 = encryptionKeyFragment;
    }

    @Override // android.view.ActionMode.Callback
    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
        ClipData primaryClip;
        int itemId = menuItem.getItemId();
        if (itemId != 16908322 && itemId != 16908337) {
            return false;
        }
        EncryptionKeyFragment encryptionKeyFragment = this.A00;
        EncBackupViewModel encBackupViewModel = encryptionKeyFragment.A01;
        AnonymousClass01d r7 = encBackupViewModel.A0C;
        ClipboardManager A0B = r7.A0B();
        if (!(A0B == null || (primaryClip = A0B.getPrimaryClip()) == null)) {
            AnonymousClass016 r6 = encBackupViewModel.A02;
            String str = (String) r6.A01();
            String lowerCase = primaryClip.getItemAt(0).getText().toString().replaceAll("\\s", "").toLowerCase(C12970iu.A14(encBackupViewModel.A0E));
            if (!TextUtils.isEmpty(str) && lowerCase.length() != 64) {
                if (str != null) {
                    StringBuilder A0h = C12960it.A0h();
                    A0h.append(str.replaceAll("\\s", ""));
                    lowerCase = C12960it.A0d(lowerCase, A0h);
                    if (lowerCase.length() > 64) {
                        C51282Tp.A01(r7);
                    }
                }
            }
            r6.A0B(lowerCase);
        }
        encryptionKeyFragment.A1A((String) encryptionKeyFragment.A01.A02.A01());
        return true;
    }

    @Override // android.view.ActionMode.Callback
    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
        if (Build.VERSION.SDK_INT < 26) {
            return false;
        }
        menu.removeItem(16908355);
        return false;
    }
}
