package X;

import android.util.Base64;
import com.whatsapp.util.Log;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeUnit;

/* renamed from: X.11h  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C233411h implements AbstractC18890tE {
    public final C15570nT A00;
    public final C233711k A01;
    public final C14830m7 A02;
    public final C15990oG A03;
    public final C233511i A04;
    public final C14850m9 A05;
    public final C16120oU A06;

    public C233411h(C15570nT r1, AnonymousClass16U r2, C233711k r3, C14830m7 r4, C15990oG r5, C233511i r6, C14850m9 r7, C16120oU r8) {
        this.A02 = r4;
        this.A05 = r7;
        this.A00 = r1;
        this.A06 = r8;
        this.A03 = r5;
        this.A04 = r6;
        this.A01 = r3;
        r2.A03(this);
    }

    public static Integer A00(int i) {
        if (!(i == 0 || i == 1)) {
            if (i == 2 || i == 3) {
                return 2;
            }
            if (i != 4) {
                return null;
            }
        }
        return 1;
    }

    public static Integer A01(int i) {
        int i2 = 1;
        if (i != 0) {
            i2 = 5;
            if (i != 1) {
                i2 = 2;
                if (i != 2) {
                    i2 = 3;
                    if (i != 3) {
                        i2 = 4;
                        if (i != 4) {
                            return null;
                        }
                    }
                }
            }
        }
        return Integer.valueOf(i2);
    }

    public static final Integer A02(long j) {
        int i;
        if (j == 0) {
            i = 1;
        } else if (j == 1) {
            i = 2;
        } else if (j < 10) {
            i = 3;
        } else if (j < 100) {
            i = 4;
        } else if (j < 500) {
            i = 5;
        } else if (j < 1000) {
            i = 6;
        } else {
            i = 8;
            if (j < 5000) {
                i = 7;
            }
        }
        return Integer.valueOf(i);
    }

    public static Integer A03(String str) {
        if (str != null) {
            switch (str.hashCode()) {
                case -849492943:
                    if (str.equals("regular_low")) {
                        return 2;
                    }
                    break;
                case -564602779:
                    if (str.equals("regular_high")) {
                        return 3;
                    }
                    break;
                case -498584183:
                    if (str.equals("critical_unblock_low")) {
                        return 5;
                    }
                    break;
                case 207170541:
                    if (str.equals("critical_block")) {
                        return 4;
                    }
                    break;
                case 1086463900:
                    if (str.equals("regular")) {
                        return 1;
                    }
                    break;
            }
        }
        return null;
    }

    public static final String A04(AnonymousClass1JS r7, AnonymousClass1JS r8) {
        if (!(r7 == null || r8 == null)) {
            try {
                return Base64.encodeToString(MessageDigest.getInstance("SHA-256").digest(C16050oM.A05(r8.A00.A01, new byte[]{95}, r7.A00.A01)), 2);
            } catch (NoSuchAlgorithmException e) {
                Log.e("sync-stats-manager/createBootstrapSessionId unable to create id because sha256 instance could not created.", e);
            }
        }
        return null;
    }

    public AnonymousClass1JT A05(AnonymousClass1JS r6, String str) {
        String A04 = A04(r6, this.A03.A00.A04().A01);
        String str2 = null;
        if (str != null) {
            C15570nT r0 = this.A00;
            r0.A08();
            C27631Ih r3 = r0.A05;
            if (r3 == null) {
                Log.e("sync-stats-manager/createMDRegAttemptId myUserJid is null");
            } else {
                try {
                    MessageDigest instance = MessageDigest.getInstance("SHA-256");
                    StringBuilder sb = new StringBuilder();
                    sb.append(r3.user);
                    sb.append("_");
                    sb.append(str);
                    str2 = Base64.encodeToString(instance.digest(sb.toString().getBytes()), 2);
                } catch (NoSuchAlgorithmException e) {
                    Log.e("sync-stats-manager/createMDRegAttemptId unable to create id because sha256 instance could not created.", e);
                }
            }
        }
        if (A04 == null || str2 == null) {
            return null;
        }
        return new AnonymousClass1JT(A04, str2);
    }

    public void A06(int i) {
        if (this.A05.A07(624)) {
            C34801gj r1 = new C34801gj();
            r1.A00 = Integer.valueOf(i);
            this.A06.A07(r1);
        }
    }

    public void A07(int i, String str) {
        C34761gf r1 = new C34761gf();
        r1.A01 = Integer.valueOf(i);
        r1.A00 = A03(str);
        this.A06.A07(r1);
    }

    public void A08(long j, boolean z) {
        C34741gd r2 = new C34741gd();
        r2.A00 = Boolean.valueOf(z);
        r2.A01 = Long.valueOf(this.A02.A00() - j);
        this.A06.A07(r2);
    }

    public final void A09(AnonymousClass1JT r4, int i) {
        C34751ge r2 = new C34751ge();
        r2.A00 = Integer.valueOf(i);
        r2.A03 = r4.A01;
        r2.A02 = r4.A00;
        r2.A01 = Long.valueOf(this.A02.A00());
        this.A06.A07(r2);
    }

    public void A0A(AnonymousClass1JT r6, int i, int i2, long j, long j2, long j3, long j4, long j5) {
        if (r6 != null) {
            C34791gi r2 = new C34791gi();
            r2.A05 = Long.valueOf(j2);
            r2.A06 = Long.valueOf(j3);
            r2.A07 = Long.valueOf(j4);
            r2.A01 = Integer.valueOf(i2);
            r2.A0B = r6.A01;
            r2.A0A = r6.A00;
            r2.A09 = Long.valueOf(this.A02.A00());
            int i3 = 1;
            if (i2 != 1) {
                i3 = 2;
            }
            r2.A03 = Integer.valueOf(i3);
            r2.A00 = A01(i);
            r2.A02 = A00(i);
            r2.A04 = Long.valueOf(j);
            r2.A08 = Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(j5));
            this.A06.A07(r2);
        }
    }

    public void A0B(AnonymousClass1JT r4, int i, long j, long j2, boolean z) {
        if (r4 != null) {
            C34731gc r2 = new C34731gc();
            r2.A06 = r4.A01;
            r2.A05 = r4.A00;
            r2.A04 = Long.valueOf(this.A02.A00());
            r2.A02 = Long.valueOf(j);
            r2.A00 = Integer.valueOf(i);
            int i2 = 2;
            if (z) {
                i2 = 1;
            }
            r2.A01 = Integer.valueOf(i2);
            r2.A03 = Long.valueOf(j2);
            this.A06.A07(r2);
        }
    }

    public void A0C(AnonymousClass1JT r4, int i, long j, boolean z) {
        if (r4 != null) {
            C34721gb r2 = new C34721gb();
            r2.A07 = r4.A01;
            r2.A06 = r4.A00;
            r2.A05 = Long.valueOf(this.A02.A00());
            int i2 = 2;
            r2.A02 = 2;
            if (z) {
                i2 = 1;
            }
            r2.A03 = Integer.valueOf(i2);
            r2.A00 = A01(i);
            r2.A01 = A00(i);
            r2.A04 = Long.valueOf(j);
            this.A06.A07(r2);
        }
    }

    public void A0D(AnonymousClass1JT r4, int i, boolean z) {
        if (r4 != null) {
            C34721gb r2 = new C34721gb();
            r2.A07 = r4.A01;
            r2.A06 = r4.A00;
            r2.A05 = Long.valueOf(this.A02.A00());
            int i2 = 1;
            r2.A02 = 1;
            if (!z) {
                i2 = 2;
            }
            r2.A03 = Integer.valueOf(i2);
            r2.A01 = Integer.valueOf(i);
            this.A06.A07(r2);
        }
    }

    public void A0E(AbstractC30271Wt r8, int i) {
        if ((r8 instanceof C34771gg) && this.A05.A07(1183)) {
            C34771gg r82 = (C34771gg) r8;
            String str = r82.A0G;
            String str2 = r82.A0F;
            int i2 = r82.A03;
            int i3 = 1;
            boolean z = false;
            if (i == -1) {
                z = true;
            }
            if (str != null && str2 != null) {
                C34781gh r2 = new C34781gh();
                r2.A06 = str;
                r2.A05 = str2;
                r2.A01 = A00(i2);
                r2.A00 = A01(i2);
                r2.A04 = Long.valueOf(this.A02.A00());
                if (!z) {
                    i3 = 2;
                }
                r2.A02 = Integer.valueOf(i3);
                if (!z) {
                    r2.A03 = Long.valueOf((long) i);
                }
                this.A06.A07(r2);
            }
        }
    }

    @Override // X.AbstractC18890tE
    public void AQ6(AbstractC30271Wt r17) {
        if (r17 instanceof C34771gg) {
            C34771gg r2 = (C34771gg) r17;
            String str = r2.A0G;
            String str2 = r2.A0F;
            if (str != null && str2 != null) {
                A0A(new AnonymousClass1JT(str, str2), r2.A03, 1, (long) r2.A01, r2.A04, r2.A07, r2.A05, r2.A0A);
            }
        }
    }
}
