package X;

/* renamed from: X.28G  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass28G {
    public final C14900mE A00;
    public final C20100vD A01;
    public final AnonymousClass28I A02;
    public final AnonymousClass3DH A03;
    public final C18640sm A04;

    public AnonymousClass28G(C14900mE r2, C20100vD r3, AnonymousClass28I r4, AnonymousClass3DH r5, C18640sm r6) {
        C16700pc.A0E(r6, 4);
        C16700pc.A0E(r2, 5);
        this.A01 = r3;
        this.A03 = r5;
        this.A02 = r4;
        this.A04 = r6;
        this.A00 = r2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0009, code lost:
        if (r0.A01 != false) goto L_0x000b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A00(X.AnonymousClass28K r8, X.AnonymousClass1J7 r9) {
        /*
            r7 = this;
            X.28I r2 = r7.A02
            X.1zQ r0 = r2.A00
            if (r0 == 0) goto L_0x000b
            boolean r0 = r0.A01
            r1 = 0
            if (r0 == 0) goto L_0x000c
        L_0x000b:
            r1 = 1
        L_0x000c:
            X.0sm r0 = r7.A04
            boolean r0 = r0.A0B()
            if (r1 != 0) goto L_0x001a
            X.416 r0 = X.AnonymousClass416.A00
            r9.AJ4(r0)
            return
        L_0x001a:
            if (r0 != 0) goto L_0x0022
            X.418 r0 = X.AnonymousClass418.A00
            r9.AJ4(r0)
            return
        L_0x0022:
            X.1zQ r0 = r2.A00
            if (r0 != 0) goto L_0x003c
            r5 = 0
        L_0x0027:
            X.3DH r2 = r7.A03
            X.5KJ r6 = new X.5KJ
            r6.<init>(r7, r9)
            r3 = r8
            com.whatsapp.jid.UserJid r4 = r8.A00
            X.0lo r0 = r2.A01
            X.3Ur r1 = new X.3Ur
            r1.<init>(r3, r4, r5, r6)
            r0.A03(r1, r4)
            return
        L_0x003c:
            java.lang.String r5 = r0.A00
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass28G.A00(X.28K, X.1J7):void");
    }
}
