package X;

import android.view.View;
import android.widget.ImageView;
import com.whatsapp.R;

/* renamed from: X.3ju  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75473ju extends AnonymousClass03U {
    public final View A00;
    public final ImageView A01;

    public C75473ju(View view) {
        super(view);
        this.A01 = C12970iu.A0L(view, R.id.sticker_preview);
        this.A00 = view.findViewById(R.id.loading_progress);
    }
}
