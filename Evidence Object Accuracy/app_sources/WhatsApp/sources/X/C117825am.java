package X;

import android.util.Pair;
import androidx.appcompat.widget.SwitchCompat;
import com.whatsapp.payments.ui.NoviPayHubSecurityActivity;

/* renamed from: X.5am  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117825am extends AnonymousClass0PW {
    public final /* synthetic */ Pair A00;
    public final /* synthetic */ SwitchCompat A01;
    public final /* synthetic */ NoviPayHubSecurityActivity A02;

    public C117825am(Pair pair, SwitchCompat switchCompat, NoviPayHubSecurityActivity noviPayHubSecurityActivity) {
        this.A02 = noviPayHubSecurityActivity;
        this.A00 = pair;
        this.A01 = switchCompat;
    }

    @Override // X.AnonymousClass0PW
    public void A02(C04700Ms r6) {
        NoviPayHubSecurityActivity noviPayHubSecurityActivity = this.A02;
        noviPayHubSecurityActivity.A2i(this.A00, this.A01, null, noviPayHubSecurityActivity.A03);
    }
}
