package X;

import android.content.Context;

/* renamed from: X.5vf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128165vf {
    public final Context A00;
    public final C14900mE A01;
    public final C15570nT A02;
    public final C14830m7 A03;
    public final C17220qS A04;
    public final C18650sn A05;
    public final AnonymousClass60T A06;
    public final C129095xA A07;
    public final String A08;

    public C128165vf(Context context, C14900mE r2, C15570nT r3, C14830m7 r4, C17220qS r5, C18650sn r6, AnonymousClass60T r7, C129095xA r8, String str) {
        this.A03 = r4;
        this.A00 = context;
        this.A01 = r2;
        this.A02 = r3;
        this.A04 = r5;
        this.A07 = r8;
        this.A05 = r6;
        this.A06 = r7;
        this.A08 = str;
    }
}
