package X;

import android.graphics.Bitmap;
import android.graphics.Rect;
import com.whatsapp.voipcalling.GlVideoRenderer;
import org.chromium.net.UrlRequest;

/* renamed from: X.5yF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129755yF {
    public Bitmap A00;
    public Rect A01;
    public Rect A02;
    public C129975yb A03;
    public AnonymousClass60J A04;
    public Boolean A05;
    public Float A06;
    public Float A07;
    public Integer A08;
    public Integer A09;
    public Integer A0A;
    public Integer A0B;
    public Integer A0C;
    public Integer A0D;
    public Long A0E;
    public Long A0F;
    public byte[] A0G;
    public byte[] A0H;
    public final int A0I;
    public final int A0J;
    public final Rect A0K;

    public C129755yF(Rect rect, Rect rect2, int i, int i2) {
        this.A01 = rect;
        this.A0K = rect2;
        this.A0J = i;
        this.A0I = i2;
    }

    public Object A00(C125525rO r3) {
        int i = r3.A00;
        if (i == 0) {
            return this.A0G;
        }
        if (i == 1) {
            return this.A03;
        }
        if (i == 2) {
            return this.A02;
        }
        switch (i) {
            case 7:
                return this.A0E;
            case 8:
                return this.A0A;
            case 9:
                return this.A06;
            case 10:
                return this.A08;
            case 11:
                return this.A07;
            case 12:
                return this.A0F;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                return null;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                return this.A04;
            case 15:
                return this.A0B;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                return this.A0D;
            case 17:
                return this.A05;
            case 18:
                return this.A0C;
            case 19:
                return this.A0H;
            case C43951xu.A01:
                return this.A09;
            case 21:
                return this.A00;
            default:
                throw C12990iw.A0m(C12960it.A0W(i, "Failed to get photo capture value: "));
        }
    }

    public void A01(C125525rO r3, Object obj) {
        int i = r3.A00;
        if (i == 0) {
            this.A0G = (byte[]) obj;
        } else if (i == 1) {
            this.A03 = (C129975yb) obj;
        } else if (i != 2) {
            switch (i) {
                case 7:
                    this.A0E = (Long) obj;
                    return;
                case 8:
                    this.A0A = (Integer) obj;
                    return;
                case 9:
                    this.A06 = (Float) obj;
                    return;
                case 10:
                    this.A08 = (Integer) obj;
                    return;
                case 11:
                    this.A07 = (Float) obj;
                    return;
                case 12:
                    this.A0F = (Long) obj;
                    return;
                case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                    return;
                case UrlRequest.Status.READING_RESPONSE /* 14 */:
                    this.A04 = (AnonymousClass60J) obj;
                    return;
                case 15:
                    this.A0B = (Integer) obj;
                    return;
                case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                    this.A0D = (Integer) obj;
                    return;
                case 17:
                    this.A05 = (Boolean) obj;
                    return;
                case 18:
                    this.A0C = (Integer) obj;
                    return;
                case 19:
                    this.A0H = (byte[]) obj;
                    return;
                case C43951xu.A01:
                    this.A09 = (Integer) obj;
                    return;
                case 21:
                    this.A00 = (Bitmap) obj;
                    return;
                default:
                    throw C12990iw.A0m(C12960it.A0W(i, "Failed to set photo capture value: "));
            }
        } else {
            this.A02 = (Rect) obj;
        }
    }
}
