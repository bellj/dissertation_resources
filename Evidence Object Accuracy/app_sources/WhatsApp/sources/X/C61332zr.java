package X;

/* renamed from: X.2zr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C61332zr extends AnonymousClass1JU {
    public boolean A00;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C61332zr(X.AnonymousClass1JU r14) {
        /*
            r13 = this;
            com.whatsapp.jid.DeviceJid r1 = r14.A05
            X.1YI r2 = r14.A06
            java.lang.String r3 = r14.A07
            long r6 = r14.A00
            long r8 = r14.A04
            long r10 = r14.A01
            int r5 = r14.A03
            boolean r12 = r14.A08
            java.lang.String r4 = r14.A02
            r0 = r13
            r0.<init>(r1, r2, r3, r4, r5, r6, r8, r10, r12)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C61332zr.<init>(X.1JU):void");
    }

    @Override // X.AnonymousClass1JU
    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        C61332zr r2 = (C61332zr) obj;
        if (!super.equals(obj) || this.A00 != r2.A00) {
            return false;
        }
        return true;
    }

    @Override // X.AnonymousClass1JU
    public int hashCode() {
        Object[] A1a = C12980iv.A1a();
        C12960it.A1O(A1a, super.hashCode());
        return C12960it.A06(Boolean.valueOf(this.A00), A1a);
    }

    @Override // X.AnonymousClass1JU
    public String toString() {
        StringBuilder A0h = C12960it.A0h();
        A0h.append(super.toString());
        A0h.append(", isSyncing: ");
        A0h.append(this.A00);
        return A0h.toString();
    }
}
