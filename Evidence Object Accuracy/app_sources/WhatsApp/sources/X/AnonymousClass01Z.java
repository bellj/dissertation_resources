package X;

import java.util.Collection;

/* renamed from: X.01Z  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass01Z extends C002501a {
    public static final void A0B(Iterable iterable, Collection collection) {
        C16700pc.A0E(iterable, 1);
        if (iterable instanceof Collection) {
            collection.addAll((Collection) iterable);
            return;
        }
        for (Object obj : iterable) {
            collection.add(obj);
        }
    }
}
