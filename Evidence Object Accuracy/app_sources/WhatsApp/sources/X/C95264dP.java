package X;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.4dP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C95264dP {
    public EnumC870149w A00;
    public AnonymousClass5T9 A01;
    public C92574Wl A02;
    public Boolean A03 = Boolean.FALSE;
    public String A04;

    public C95264dP() {
    }

    public C95264dP(C92574Wl r2) {
        this.A02 = r2;
        this.A00 = EnumC870149w.PATH;
    }

    public C95264dP(String str) {
        this.A04 = str;
        this.A00 = EnumC870149w.JSON;
    }

    public static List A00(C94394bk r6, Class cls, List list) {
        ArrayList A0l = C12960it.A0l();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            Object obj = ((C95264dP) it.next()).A01.get();
            AbstractC117035Xw r1 = r6.A01.A00;
            if (obj instanceof List) {
                for (Object obj2 : r1.Aet(obj)) {
                    if (obj2 != null) {
                        if (cls.isAssignableFrom(obj2.getClass())) {
                            A0l.add(obj2);
                        } else if (cls == String.class) {
                            A0l.add(obj2.toString());
                        }
                    }
                }
            } else if (obj != null) {
                if (cls.isAssignableFrom(obj.getClass())) {
                    A0l.add(obj);
                } else if (cls == String.class) {
                    A0l.add(obj.toString());
                }
            }
        }
        return A0l;
    }
}
