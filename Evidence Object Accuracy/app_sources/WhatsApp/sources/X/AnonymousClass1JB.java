package X;

/* renamed from: X.1JB  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1JB {
    public static final AnonymousClass1JB A09 = new AnonymousClass1JB(true, true, true, true, true, true, true, true, true);
    public static final AnonymousClass1JB A0A = new AnonymousClass1JB(false, false, false, false, true, false, false, false, false);
    public static final AnonymousClass1JB A0B = new AnonymousClass1JB(true, false, false, false, false, false, false, false, false);
    public static final AnonymousClass1JB A0C = new AnonymousClass1JB(false, false, false, false, false, true, false, false, false);
    public static final AnonymousClass1JB A0D = new AnonymousClass1JB(false, false, false, false, false, false, false, true, false);
    public static final AnonymousClass1JB A0E = new AnonymousClass1JB(true, false, true, true, true, true, true, true, true);
    public static final AnonymousClass1JB A0F = new AnonymousClass1JB(false, true, true, true, true, true, true, true, true);
    public static final AnonymousClass1JB A0G = new AnonymousClass1JB(false, false, false, false, false, false, true, false, false);
    public static final AnonymousClass1JB A0H = new AnonymousClass1JB(false, false, false, true, false, false, false, false, false);
    public static final AnonymousClass1JB A0I = new AnonymousClass1JB(false, false, true, false, false, false, false, false, false);
    public final boolean A00;
    public final boolean A01;
    public final boolean A02;
    public final boolean A03;
    public final boolean A04;
    public final boolean A05;
    public final boolean A06;
    public final boolean A07;
    public final boolean A08;

    public AnonymousClass1JB(boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, boolean z9) {
        this.A04 = z9;
        this.A01 = z;
        this.A07 = z2;
        this.A08 = z3;
        this.A06 = z4;
        this.A00 = z5;
        this.A02 = z6;
        this.A05 = z7;
        this.A03 = z8;
    }

    public boolean A00() {
        return this.A01 | this.A07 | this.A08 | this.A06 | this.A00 | this.A02 | this.A05 | this.A03 | this.A04;
    }
}
