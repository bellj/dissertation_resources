package X;

import java.util.concurrent.Callable;

/* renamed from: X.5DZ  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5DZ implements Callable {
    public final /* synthetic */ C39041pA A00;

    public AnonymousClass5DZ(C39041pA r1) {
        this.A00 = r1;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        C39041pA r2 = this.A00;
        synchronized (r2) {
            if (r2.A03 != null) {
                r2.A09();
                if (r2.A0C()) {
                    r2.A0A();
                    r2.A00 = 0;
                }
            }
        }
        return null;
    }
}
