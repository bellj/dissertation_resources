package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4jL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98844jL implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        Parcel parcel2 = null;
        C78443ov r2 = null;
        int i = 0;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                i = C95664e9.A02(parcel, readInt);
            } else if (c == 2) {
                int A03 = C95664e9.A03(parcel, readInt);
                int dataPosition = parcel.dataPosition();
                if (A03 == 0) {
                    parcel2 = null;
                } else {
                    parcel2 = Parcel.obtain();
                    parcel2.appendFrom(parcel, dataPosition, A03);
                    parcel.setDataPosition(dataPosition + A03);
                }
            } else if (c != 3) {
                C95664e9.A0D(parcel, readInt);
            } else {
                r2 = (C78443ov) C95664e9.A07(parcel, C78443ov.CREATOR, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78763pV(parcel2, r2, i);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new C78763pV[i];
    }
}
