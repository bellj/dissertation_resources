package X;

import java.security.SecureRandom;

/* renamed from: X.0lM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14390lM {
    public final long A00;
    public final byte[] A01;

    public C14390lM(byte[] bArr, long j) {
        AnonymousClass009.A0F(j > 0);
        this.A01 = bArr;
        this.A00 = j;
    }

    public static boolean A00(C14390lM r4, long j) {
        return j - r4.A00 < ((long) new SecureRandom().nextInt(86400000)) + 172800000;
    }
}
