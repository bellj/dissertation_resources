package X;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.whatsapp.backup.google.viewmodel.SettingsGoogleDriveViewModel;
import com.whatsapp.util.Log;

/* renamed from: X.3LH  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3LH implements ServiceConnection {
    public final /* synthetic */ SettingsGoogleDriveViewModel A00;

    public AnonymousClass3LH(SettingsGoogleDriveViewModel settingsGoogleDriveViewModel) {
        this.A00 = settingsGoogleDriveViewModel;
    }

    @Override // android.content.ServiceConnection
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        SettingsGoogleDriveViewModel settingsGoogleDriveViewModel = this.A00;
        settingsGoogleDriveViewModel.A0g.set(true);
        AnonymousClass10K r1 = settingsGoogleDriveViewModel.A0W;
        if (!r1.A0B) {
            r1.A03();
        }
        C12990iw.A1O(settingsGoogleDriveViewModel.A0e, this, 19);
        settingsGoogleDriveViewModel.A01.open();
        settingsGoogleDriveViewModel.A05();
        Log.i("settings-gdrive/service-connected");
    }

    @Override // android.content.ServiceConnection
    public void onServiceDisconnected(ComponentName componentName) {
        SettingsGoogleDriveViewModel settingsGoogleDriveViewModel = this.A00;
        settingsGoogleDriveViewModel.A0g.set(false);
        settingsGoogleDriveViewModel.A01.close();
        Log.i("settings-gdrive/service-disconnected");
    }
}
