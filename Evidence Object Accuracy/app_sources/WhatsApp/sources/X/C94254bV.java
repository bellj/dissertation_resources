package X;

import android.os.SystemClock;
import android.view.Choreographer;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/* renamed from: X.4bV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C94254bV {
    public boolean A00 = true;
    public final AnonymousClass4SM A01;
    public final Map A02 = C12970iu.A11();
    public final Set A03 = new CopyOnWriteArraySet();
    public final CopyOnWriteArraySet A04 = new CopyOnWriteArraySet();

    public C94254bV(AnonymousClass4SM r2) {
        this.A01 = r2;
        r2.A01 = this;
    }

    public static C94254bV A00() {
        return new C94254bV(new AnonymousClass4SM(Choreographer.getInstance()));
    }

    public AnonymousClass4YC A01() {
        AnonymousClass4YC r3 = new AnonymousClass4YC(this);
        Map map = this.A02;
        String str = r3.A0D;
        if (!map.containsKey(str)) {
            map.put(str, r3);
            return r3;
        }
        throw C12970iu.A0f("spring is already registered");
    }

    public void A02(String str) {
        Object obj = this.A02.get(str);
        if (obj != null) {
            this.A03.add(obj);
            if (this.A00) {
                this.A00 = false;
                AnonymousClass4SM r2 = this.A01;
                if (!r2.A02) {
                    r2.A02 = true;
                    r2.A00 = SystemClock.uptimeMillis();
                    Choreographer choreographer = r2.A04;
                    Choreographer.FrameCallback frameCallback = r2.A03;
                    choreographer.removeFrameCallback(frameCallback);
                    choreographer.postFrameCallback(frameCallback);
                    return;
                }
                return;
            }
            return;
        }
        StringBuilder A0k = C12960it.A0k("springId ");
        A0k.append(str);
        throw C12970iu.A0f(C12960it.A0d(" does not reference a registered spring", A0k));
    }
}
