package X;

import android.os.StrictMode;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Random;

/* renamed from: X.3dK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C71673dK extends ThreadLocal {
    @Override // java.lang.ThreadLocal
    public Object initialValue() {
        StrictMode.ThreadPolicy allowThreadDiskReads;
        try {
            allowThreadDiskReads = StrictMode.allowThreadDiskReads();
            try {
                FileInputStream fileInputStream = new FileInputStream("/dev/urandom");
                try {
                    ByteBuffer allocate = ByteBuffer.allocate(8);
                    fileInputStream.read(allocate.array());
                    Random random = new Random(allocate.getLong());
                    fileInputStream.close();
                    return random;
                } catch (Throwable th) {
                    try {
                        fileInputStream.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            } catch (IOException e) {
                throw new RuntimeException("Cannot read from /dev/urandom", e);
            }
        } finally {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
        }
    }
}
