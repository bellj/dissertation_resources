package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.63m  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1316263m implements Parcelable {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(19);
    public final long A00;
    public final AnonymousClass6F2 A01;
    public final AnonymousClass6F2 A02;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C1316263m(AnonymousClass6F2 r1, AnonymousClass6F2 r2, long j) {
        this.A02 = r1;
        this.A01 = r2;
        this.A00 = j;
    }

    public static C1316263m A00(AnonymousClass102 r5, AnonymousClass1V8 r6) {
        return new C1316263m(AnonymousClass6F2.A00(r5, r6.A0F("primary")), AnonymousClass6F2.A00(r5, r6.A0F("local")), r6.A07("last_updated_time_usec"));
    }

    public static C1316263m A01(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            JSONObject A05 = C13000ix.A05(str);
            AnonymousClass6F2 A01 = AnonymousClass6F2.A01(A05.optString("local", A05.optString("fiat", "")));
            AnonymousClass6F2 A012 = AnonymousClass6F2.A01(A05.optString("primary", A05.optString("crypto", "")));
            long optLong = A05.optLong("updateTsInMicroSeconds", -1);
            AnonymousClass009.A05(A012);
            AnonymousClass009.A05(A01);
            return new C1316263m(A012, A01, optLong);
        } catch (JSONException unused) {
            Log.e("PAY: NoviBalance fromJsonString threw exception");
            return null;
        }
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.A02, i);
        parcel.writeParcelable(this.A01, i);
        parcel.writeLong(this.A00);
    }
}
