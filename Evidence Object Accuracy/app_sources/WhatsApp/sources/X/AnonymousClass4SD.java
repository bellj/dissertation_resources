package X;

import android.media.MediaCodecInfo;

/* renamed from: X.4SD  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4SD {
    public final int A00;
    public final MediaCodecInfo.CodecCapabilities A01;
    public final String A02;
    public final boolean A03;

    public AnonymousClass4SD(MediaCodecInfo.CodecCapabilities codecCapabilities, String str, int i, boolean z) {
        this.A02 = str;
        this.A00 = i;
        this.A01 = codecCapabilities;
        this.A03 = z;
    }
}
