package X;

import android.widget.PopupWindow;

/* renamed from: X.4pG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C102514pG implements PopupWindow.OnDismissListener {
    public final /* synthetic */ AbstractActivityC13750kH A00;

    public C102514pG(AbstractActivityC13750kH r1) {
        this.A00 = r1;
    }

    @Override // android.widget.PopupWindow.OnDismissListener
    public void onDismiss() {
        AbstractActivityC13750kH r2 = this.A00;
        C15270mq A2g = r2.A2g();
        if (r2.A2n() && A2g != null && !((AbstractC15280mr) A2g).A02) {
            r2.A0g.A04(0);
        }
    }
}
