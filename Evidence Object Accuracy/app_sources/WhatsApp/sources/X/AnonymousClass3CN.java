package X;

import com.facebook.redex.RunnableBRunnable0Shape3S0200000_I0_3;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.companiondevice.LinkedDevicesActivity;

/* renamed from: X.3CN  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3CN {
    public final /* synthetic */ LinkedDevicesActivity A00;

    public AnonymousClass3CN(LinkedDevicesActivity linkedDevicesActivity) {
        this.A00 = linkedDevicesActivity;
    }

    public void A00(TextEmojiLabel textEmojiLabel) {
        LinkedDevicesActivity linkedDevicesActivity = this.A00;
        AnonymousClass2I2 r1 = linkedDevicesActivity.A0C;
        textEmojiLabel.setText(r1.A03.A03(linkedDevicesActivity, new RunnableBRunnable0Shape3S0200000_I0_3(r1, 29, linkedDevicesActivity), linkedDevicesActivity.getString(R.string.personal_messages_are_e2ee_on_devices), "%s", R.color.primary_light));
        C52162aM.A00(textEmojiLabel);
        linkedDevicesActivity.A0M.A00(9, 0);
    }
}
