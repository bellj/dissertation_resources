package X;

import android.content.ContentValues;
import android.database.Cursor;
import com.whatsapp.jid.UserJid;

/* renamed from: X.0vx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20560vx {
    public final C16510p9 A00;
    public final C18460sU A01;
    public final C16490p7 A02;

    public C20560vx(C16510p9 r1, C18460sU r2, C16490p7 r3) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
    }

    public long A00(C15580nU r8, UserJid userJid) {
        long j = -1;
        if (r8 == null || userJid == null) {
            return -1;
        }
        C18460sU r3 = this.A01;
        String[] strArr = {Long.toString(r3.A01(r8)), Long.toString(r3.A01(userJid))};
        C16310on A01 = this.A02.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT message_row_id FROM message_group_invite WHERE group_jid_row_id = ? AND admin_jid_row_id = ? AND expired = 0 ORDER BY invite_time DESC", strArr);
            if (A09.moveToNext()) {
                j = A09.getLong(A09.getColumnIndexOrThrow("message_row_id"));
            }
            A09.close();
            A01.close();
            return j;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public long A01(C15580nU r7, UserJid userJid) {
        long j = -1;
        if (r7 == null || userJid == null) {
            return -1;
        }
        String[] strArr = {Long.toString(this.A01.A01(r7)), Long.toString(this.A00.A02(userJid))};
        C16310on A01 = this.A02.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT invite.message_row_id AS message_row_id FROM message_group_invite invite INNER JOIN message_view message ON invite.message_row_id = message._id WHERE invite.group_jid_row_id = ? AND message.chat_row_id = ? AND invite.expired = 0 ORDER BY invite.invite_time DESC", strArr);
            if (A09.moveToNext()) {
                j = A09.getLong(A09.getColumnIndexOrThrow("message_row_id"));
            }
            A09.close();
            A01.close();
            return j;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final ContentValues A02(C28581Od r5, long j) {
        String l;
        ContentValues contentValues = new ContentValues();
        contentValues.put("message_row_id", Long.toString(j));
        C15580nU r1 = r5.A02;
        String str = null;
        if (r1 == null) {
            l = null;
        } else {
            l = Long.toString(this.A01.A01(r1));
        }
        contentValues.put("group_jid_row_id", l);
        UserJid userJid = r5.A03;
        if (userJid != null) {
            str = Long.toString(this.A01.A01(userJid));
        }
        contentValues.put("admin_jid_row_id", str);
        contentValues.put("group_name", r5.A05);
        contentValues.put("invite_code", r5.A06);
        contentValues.put("expiration", Long.valueOf(r5.A01));
        contentValues.put("invite_time", Long.valueOf(r5.A0I));
        contentValues.put("expired", Integer.valueOf(r5.A07 ? 1 : 0));
        contentValues.put("group_type", Integer.valueOf(r5.A00));
        return contentValues;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x007b, code lost:
        if (r1 == null) goto L_0x007d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A03(X.C28581Od r15) {
        /*
            r14 = this;
            r0 = 1
            java.lang.String[] r2 = new java.lang.String[r0]
            long r0 = r15.A11
            java.lang.String r1 = java.lang.Long.toString(r0)
            r0 = 0
            r2[r0] = r1
            X.0p7 r0 = r14.A02
            X.0on r8 = r0.get()
            X.0op r1 = r8.A03     // Catch: all -> 0x00a1
            java.lang.String r0 = "SELECT group_jid_row_id, admin_jid_row_id, group_name, invite_code, expiration , expired , group_type FROM message_group_invite WHERE message_row_id = ?"
            android.database.Cursor r9 = r1.A09(r0, r2)     // Catch: all -> 0x00a1
            boolean r0 = r9.moveToNext()     // Catch: all -> 0x009a
            if (r0 == 0) goto L_0x0093
            java.lang.String r0 = "expiration"
            int r0 = r9.getColumnIndexOrThrow(r0)     // Catch: all -> 0x009a
            long r2 = r9.getLong(r0)     // Catch: all -> 0x009a
            java.lang.String r0 = "group_jid_row_id"
            int r0 = r9.getColumnIndexOrThrow(r0)     // Catch: all -> 0x009a
            long r4 = r9.getLong(r0)     // Catch: all -> 0x009a
            java.lang.String r0 = "admin_jid_row_id"
            int r0 = r9.getColumnIndexOrThrow(r0)     // Catch: all -> 0x009a
            long r0 = r9.getLong(r0)     // Catch: all -> 0x009a
            java.lang.String r6 = "group_name"
            int r6 = r9.getColumnIndexOrThrow(r6)     // Catch: all -> 0x009a
            java.lang.String r10 = r9.getString(r6)     // Catch: all -> 0x009a
            java.lang.String r6 = "invite_code"
            int r6 = r9.getColumnIndexOrThrow(r6)     // Catch: all -> 0x009a
            java.lang.String r7 = r9.getString(r6)     // Catch: all -> 0x009a
            java.lang.String r6 = "expired"
            int r6 = r9.getColumnIndexOrThrow(r6)     // Catch: all -> 0x009a
            int r13 = r9.getInt(r6)     // Catch: all -> 0x009a
            java.lang.String r6 = "group_type"
            int r6 = r9.getColumnIndexOrThrow(r6)     // Catch: all -> 0x009a
            int r6 = r9.getInt(r6)     // Catch: all -> 0x009a
            X.0sU r11 = r14.A01     // Catch: all -> 0x009a
            java.lang.Class<X.0nU> r12 = X.C15580nU.class
            com.whatsapp.jid.Jid r5 = r11.A07(r12, r4)     // Catch: all -> 0x009a
            X.0nU r5 = (X.C15580nU) r5     // Catch: all -> 0x009a
            java.lang.Class<com.whatsapp.jid.UserJid> r4 = com.whatsapp.jid.UserJid.class
            com.whatsapp.jid.Jid r1 = r11.A07(r4, r0)     // Catch: all -> 0x009a
            com.whatsapp.jid.UserJid r1 = (com.whatsapp.jid.UserJid) r1     // Catch: all -> 0x009a
            if (r5 == 0) goto L_0x007d
            r0 = 1
            if (r1 != 0) goto L_0x007e
        L_0x007d:
            r0 = 0
        L_0x007e:
            X.AnonymousClass009.A0F(r0)     // Catch: all -> 0x009a
            r0 = 0
            if (r13 == 0) goto L_0x0085
            r0 = 1
        L_0x0085:
            r15.A02 = r5     // Catch: all -> 0x009a
            r15.A03 = r1     // Catch: all -> 0x009a
            r15.A05 = r10     // Catch: all -> 0x009a
            r15.A06 = r7     // Catch: all -> 0x009a
            r15.A01 = r2     // Catch: all -> 0x009a
            r15.A07 = r0     // Catch: all -> 0x009a
            r15.A00 = r6     // Catch: all -> 0x009a
        L_0x0093:
            r9.close()     // Catch: all -> 0x00a1
            r8.close()
            return
        L_0x009a:
            r0 = move-exception
            if (r9 == 0) goto L_0x00a0
            r9.close()     // Catch: all -> 0x00a0
        L_0x00a0:
            throw r0     // Catch: all -> 0x00a1
        L_0x00a1:
            r0 = move-exception
            r8.close()     // Catch: all -> 0x00a5
        L_0x00a5:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20560vx.A03(X.1Od):void");
    }

    public void A04(C28581Od r6) {
        C16310on A02 = this.A02.A02();
        try {
            A02.A03.A06(A02(r6, r6.A11), "message_group_invite", 5);
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A05(C28581Od r6, long j) {
        C16310on A02 = this.A02.A02();
        try {
            A02.A03.A06(A02(r6, j), "message_quoted_group_invite", 5);
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
