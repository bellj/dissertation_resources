package X;

import android.text.TextUtils;

/* renamed from: X.20p  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C452120p {
    public int A00;
    public int A01;
    public long A02;
    public AnonymousClass1V8 A03;
    public AnonymousClass1V8 A04;
    public AnonymousClass1V8 A05;
    public String A06;
    public String A07;
    public String A08;
    public String A09;

    public C452120p() {
        this.A00 = 0;
    }

    public C452120p(int i) {
        this.A00 = i;
    }

    public C452120p(AnonymousClass1V8 r6) {
        this.A00 = C28421Nd.A00(r6.A0I("error-code", null), 0);
        this.A09 = r6.A0I("error-text", null);
        this.A08 = r6.A0I("display_title", null);
        this.A07 = r6.A0I("display_text", null);
        this.A01 = C28421Nd.A00(r6.A0I("remaining-retries", null), -1);
        this.A02 = C28421Nd.A01(r6.A0I("next-retry-ts", null), 0);
        this.A06 = r6.A0I("auth-ticket-fp", null);
        this.A04 = r6.A0E("offer_eligibility");
        int i = this.A00;
        if (i == 1448) {
            this.A03 = r6.A0E("key");
        } else if (i == 10718) {
            this.A01 = 0;
        } else if (i == 454) {
            this.A05 = r6.A0E("step_up");
        }
    }

    public static C452120p A00(AnonymousClass1V8 r2) {
        if (!TextUtils.isEmpty(r2.A0I("error-code", null)) || (r2 = r2.A0E("pin")) != null) {
            return new C452120p(r2);
        }
        return null;
    }

    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder("[ code: ");
        sb.append(this.A00);
        sb.append(" text: ");
        sb.append(this.A09);
        sb.append(" remaining-retries: ");
        sb.append(this.A01);
        sb.append(" next-attempt-ts: ");
        sb.append(this.A02);
        String str2 = this.A06;
        if (str2 != null) {
            StringBuilder sb2 = new StringBuilder(" auth-ticket-fp: ");
            sb2.append(str2);
            str = sb2.toString();
        } else {
            str = "";
        }
        sb.append(str);
        sb.append(" key-node: ");
        sb.append(this.A03 != null ? "set" : "null");
        sb.append(" ]");
        return sb.toString();
    }
}
