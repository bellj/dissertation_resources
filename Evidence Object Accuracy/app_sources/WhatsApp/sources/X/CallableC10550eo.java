package X;

import android.content.Context;
import java.lang.ref.WeakReference;
import java.util.concurrent.Callable;

/* renamed from: X.0eo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class CallableC10550eo implements Callable {
    public final /* synthetic */ int A00;
    public final /* synthetic */ Context A01;
    public final /* synthetic */ String A02;
    public final /* synthetic */ WeakReference A03;

    public CallableC10550eo(Context context, String str, WeakReference weakReference, int i) {
        this.A03 = weakReference;
        this.A01 = context;
        this.A00 = i;
        this.A02 = str;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        Context context = (Context) this.A03.get();
        if (context == null) {
            context = this.A01;
        }
        return C06550Ub.A00(context, this.A02, this.A00);
    }
}
