package X;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

/* renamed from: X.57m  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1109457m implements AbstractC116725Wo {
    public ByteArrayOutputStream A00;

    @Override // X.AbstractC116725Wo
    public long AEj() {
        return 0;
    }

    @Override // X.AbstractC116725Wo
    public void AfU() {
    }

    @Override // X.AbstractC116725Wo
    public OutputStream AYn(AbstractC37631mk r2) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        this.A00 = byteArrayOutputStream;
        return byteArrayOutputStream;
    }
}
