package X;

import android.os.Bundle;
import com.whatsapp.catalogcategory.view.fragment.CatalogCategoryExpandableGroupsListFragment;
import com.whatsapp.jid.UserJid;
import java.util.List;

/* renamed from: X.2fA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C53752fA extends AnonymousClass0GF {
    public List A00;

    public C53752fA(AnonymousClass01F r1) {
        super(r1);
    }

    @Override // X.AnonymousClass01A
    public int A01() {
        List list = this.A00;
        if (list != null) {
            return list.size();
        }
        throw C16700pc.A06("tabItemsList");
    }

    @Override // X.AnonymousClass01A
    public /* bridge */ /* synthetic */ CharSequence A04(int i) {
        List list = this.A00;
        if (list != null) {
            return ((AnonymousClass3F7) list.get(i)).A02;
        }
        throw C16700pc.A06("tabItemsList");
    }

    @Override // X.AnonymousClass0GF
    public /* bridge */ /* synthetic */ AnonymousClass01E A0F(int i) {
        List list = this.A00;
        if (list == null) {
            throw C16700pc.A06("tabItemsList");
        }
        String str = ((AnonymousClass3F7) list.get(i)).A01;
        List list2 = this.A00;
        if (list2 == null) {
            throw C16700pc.A06("tabItemsList");
        }
        UserJid userJid = ((AnonymousClass3F7) list2.get(i)).A00;
        C16700pc.A0F(str, userJid);
        Bundle A0D = C12970iu.A0D();
        A0D.putString("parent_category_id", str);
        A0D.putParcelable("category_biz_id", userJid);
        CatalogCategoryExpandableGroupsListFragment catalogCategoryExpandableGroupsListFragment = new CatalogCategoryExpandableGroupsListFragment();
        catalogCategoryExpandableGroupsListFragment.A0U(A0D);
        return catalogCategoryExpandableGroupsListFragment;
    }
}
