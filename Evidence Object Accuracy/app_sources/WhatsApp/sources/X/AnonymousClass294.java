package X;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.location.GroupChatLiveLocationsActivity;
import com.whatsapp.location.LocationPicker;

/* renamed from: X.294  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass294 extends AnonymousClass295 {
    public float A00;
    public float A01;
    public int A02 = 2;
    public long A03;
    public SensorManager A04 = this.A07.A0D();
    public Display A05 = this.A07.A0O().getDefaultDisplay();
    public AnonymousClass04Q A06;
    public AnonymousClass01d A07;
    public boolean A08;
    public final SensorEventListener A09 = new AnonymousClass3LP(this);
    public final AbstractC12200hX A0A = new AbstractC12200hX() { // from class: X.4ug
        @Override // X.AbstractC12200hX
        public final void ASO(AnonymousClass04Q r3) {
            AnonymousClass294 r1 = AnonymousClass294.this;
            if (r1.A06 == null) {
                r1.A06 = r3;
                r1.setLocationMode(r1.A02);
            }
        }
    };
    public final float[] A0B = new float[3];
    public final float[] A0C = new float[16];
    public final float[] A0D = new float[3];

    public AnonymousClass294(Context context, AnonymousClass0O0 r4) {
        super(context, r4);
    }

    public AnonymousClass04Q A0J(AbstractC12200hX r2) {
        AnonymousClass009.A01();
        AnonymousClass04Q r0 = this.A06;
        if (r0 != null) {
            r2.ASO(r0);
            return this.A06;
        }
        A0G(r2);
        return null;
    }

    public void A0K() {
        SensorManager sensorManager = this.A04;
        if (sensorManager != null) {
            Sensor defaultSensor = sensorManager.getDefaultSensor(11);
            boolean z = false;
            if (defaultSensor != null) {
                z = true;
            }
            this.A08 = z;
            if (defaultSensor != null) {
                sensorManager.registerListener(this.A09, defaultSensor, 16000);
            }
        }
    }

    public void A0L(int i) {
        LocationPicker locationPicker;
        ImageView imageView;
        int i2;
        GroupChatLiveLocationsActivity groupChatLiveLocationsActivity;
        ImageView imageView2;
        int i3;
        if (!(this instanceof C618132q)) {
            C618032p r2 = (C618032p) this;
            int i4 = 8;
            if (i == 0) {
                groupChatLiveLocationsActivity = r2.A00;
                AbstractView$OnCreateContextMenuListenerC35851ir r0 = groupChatLiveLocationsActivity.A0L;
                r0.A0u = true;
                r0.A0s = true;
                imageView2 = groupChatLiveLocationsActivity.A03;
                i3 = R.drawable.btn_compass_mode_tilt;
            } else if (i == 1) {
                groupChatLiveLocationsActivity = r2.A00;
                AbstractView$OnCreateContextMenuListenerC35851ir r02 = groupChatLiveLocationsActivity.A0L;
                r02.A0u = true;
                r02.A0s = true;
                imageView2 = groupChatLiveLocationsActivity.A03;
                i3 = R.drawable.btn_myl_active;
            } else if (i == 2) {
                GroupChatLiveLocationsActivity groupChatLiveLocationsActivity2 = r2.A00;
                groupChatLiveLocationsActivity2.A03.setImageResource(R.drawable.btn_myl);
                groupChatLiveLocationsActivity2.A0L.A0s = false;
                return;
            } else {
                return;
            }
            imageView2.setImageResource(i3);
            AbstractView$OnCreateContextMenuListenerC35851ir r03 = groupChatLiveLocationsActivity.A0L;
            View view = r03.A0U;
            if (r03.A0m == null) {
                i4 = 0;
            }
            view.setVisibility(i4);
            return;
        }
        C618132q r1 = (C618132q) this;
        if (i == 0) {
            locationPicker = r1.A00;
            imageView = locationPicker.A0M.A0S;
            i2 = R.drawable.btn_compass_mode_tilt;
        } else if (i == 1) {
            LocationPicker locationPicker2 = r1.A00;
            locationPicker2.A0M.A0S.setImageResource(R.drawable.btn_myl_active);
            locationPicker2.A0M.A0r = true;
            return;
        } else if (i == 2) {
            locationPicker = r1.A00;
            imageView = locationPicker.A0M.A0S;
            i2 = R.drawable.btn_myl;
        } else {
            return;
        }
        imageView.setImageResource(i2);
        locationPicker.A0M.A0r = false;
    }

    @Override // android.view.View, android.view.ViewGroup
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        if (this.A02 != 2) {
            this.A02 = 2;
            A0L(2);
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    public int getLocationMode() {
        return this.A02;
    }

    public Location getMyLocation() {
        AnonymousClass04Q A0J = A0J(this.A0A);
        if (A0J == null || !A0J.A0N || A0J.A0H == null) {
            return null;
        }
        return A0J.A0U.A00;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x006b, code lost:
        if (r3 != null) goto L_0x003c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setLocationMode(int r9) {
        /*
            r8 = this;
            X.0hX r0 = r8.A0A
            X.04Q r4 = r8.A0J(r0)
            if (r4 != 0) goto L_0x000b
            r8.A02 = r9
        L_0x000a:
            return
        L_0x000b:
            X.0Vj r3 = r4.A02()
            if (r9 == 0) goto L_0x001d
            r7 = 1
            r1 = 2
            if (r9 == r7) goto L_0x006e
            if (r9 != r1) goto L_0x000a
            r8.A02 = r1
            r8.A0L(r1)
            return
        L_0x001d:
            boolean r0 = r8.A08
            if (r0 == 0) goto L_0x000a
            float r0 = r3.A02
            r8.A01 = r0
            r0 = 0
            r8.A0L(r0)
            android.location.Location r0 = r8.getMyLocation()
            if (r0 == 0) goto L_0x0069
            double r5 = r0.getLatitude()
            double r0 = r0.getLongitude()
            X.03T r3 = new X.03T
            r3.<init>(r5, r0)
        L_0x003c:
            X.0OV r2 = new X.0OV
            r2.<init>()
            float r0 = r8.A00
            r2.A00 = r0
            float r1 = r8.A01
            r0 = 1097859072(0x41700000, float:15.0)
            float r0 = java.lang.Math.max(r1, r0)
            r2.A01 = r0
            r2.A02 = r3
            X.0Vj r0 = r2.A00()
            X.0Oq r0 = X.AnonymousClass0R9.A00(r0)
            r4.A09(r0)
        L_0x005c:
            r0 = 28
            com.facebook.redex.RunnableBRunnable0Shape5S0200000_I0_5 r2 = new com.facebook.redex.RunnableBRunnable0Shape5S0200000_I0_5
            r2.<init>(r8, r0, r4)
            r0 = 1000(0x3e8, double:4.94E-321)
            r8.postDelayed(r2, r0)
            return
        L_0x0069:
            X.03T r3 = r3.A03
            if (r3 == 0) goto L_0x005c
            goto L_0x003c
        L_0x006e:
            android.location.Location r0 = r8.getMyLocation()
            if (r0 == 0) goto L_0x00a1
            double r5 = r0.getLatitude()
            double r1 = r0.getLongitude()
            X.03T r0 = new X.03T
            r0.<init>(r5, r1)
            r8.A02 = r7
            r1 = 1
        L_0x0084:
            r8.A0L(r1)
            X.0OV r1 = new X.0OV
            r1.<init>()
            r1.A02 = r0
            float r0 = r3.A02
            r1.A01 = r0
            r0 = 0
            r1.A00 = r0
            X.0Vj r0 = r1.A00()
            X.0Oq r0 = X.AnonymousClass0R9.A00(r0)
            r4.A09(r0)
            return
        L_0x00a1:
            X.03T r0 = r3.A03
            r8.A02 = r1
            goto L_0x0084
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass294.setLocationMode(int):void");
    }
}
