package X;

import android.database.Cursor;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import org.json.JSONObject;

/* renamed from: X.0to  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC19250to extends AbstractC18500sY implements AbstractC19010tQ {
    public Map A00;
    public Set A01;
    public final AbstractC15710nm A02;
    public final AnonymousClass2FD A03;
    public final Random A04 = new Random();

    public abstract String A0W(Cursor cursor);

    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AM5() {
    }

    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AND() {
    }

    public AbstractC19250to(C18480sW r3) {
        super(r3, "message_main_verification", Integer.MIN_VALUE);
        this.A02 = r3.A00;
        this.A03 = new AnonymousClass2FD();
        this.A00 = new HashMap();
    }

    @Override // X.AbstractC18500sY
    public AnonymousClass2Ez A09(Cursor cursor) {
        int i;
        C21390xL r6 = this.A06;
        String A02 = r6.A02("message_main_verification_failed_message_ids");
        if (A02 != null) {
            this.A01 = new HashSet(Arrays.asList(A02.split(",")));
        }
        long j = -1;
        int i2 = 0;
        while (cursor.moveToNext()) {
            j = cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
            i2++;
            String A0W = A0W(cursor);
            if (!TextUtils.isEmpty(A0W)) {
                Set set = this.A01;
                if (set == null) {
                    set = new HashSet();
                    this.A01 = set;
                }
                String valueOf = String.valueOf(j);
                set.add(valueOf);
                Set set2 = this.A01;
                if (set2 != null) {
                    i = set2.size();
                } else {
                    i = 0;
                }
                this.A00.put(valueOf, A0W);
                if (i >= 50) {
                    A0Y();
                }
            }
        }
        Set set3 = this.A01;
        if (set3 != null && !set3.isEmpty()) {
            String join = TextUtils.join(",", this.A01);
            if (!TextUtils.isEmpty(join)) {
                r6.A06("message_main_verification_failed_message_ids", join);
            }
        }
        return new AnonymousClass2Ez(j, i2);
    }

    @Override // X.AbstractC18500sY
    public void A0H() {
        super.A0H();
        this.A06.A05(A0F(), ((C19240tn) this).A03.A01());
    }

    @Override // X.AbstractC18500sY
    public void A0I() {
        String A02 = this.A06.A02("message_main_verification_failed_message_ids");
        if (A02 != null) {
            this.A01 = new HashSet(Arrays.asList(A02.split(",")));
        }
        Set set = this.A01;
        if (set != null && set.size() > 0) {
            A0Y();
        }
    }

    @Override // X.AbstractC18500sY
    public void A0J() {
        int i;
        C16310on A02 = this.A05.A02();
        try {
            if (A0O()) {
                JSONObject A022 = this.A0A.A02(this.A0C);
                if (A022 != null) {
                    i = A022.optInt("new_data_sampling");
                    AnonymousClass2FD r1 = this.A03;
                    r1.A00 = i;
                    r1.A01 = -1;
                    A02.close();
                }
                i = 0;
                AnonymousClass2FD r1 = this.A03;
                r1.A00 = i;
                r1.A01 = -1;
                A02.close();
            }
            JSONObject A023 = this.A0A.A02(this.A0C);
            if (A023 != null) {
                i = A023.optInt("old_data_sampling");
                AnonymousClass2FD r1 = this.A03;
                r1.A00 = i;
                r1.A01 = -1;
                A02.close();
            }
            i = 0;
            AnonymousClass2FD r1 = this.A03;
            r1.A00 = i;
            r1.A01 = -1;
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    @Override // X.AbstractC18500sY
    public boolean A0V(C29001Pw r4) {
        int optInt;
        C21640xk r0 = this.A0A;
        String str = this.A0C;
        JSONObject A02 = r0.A02(str);
        if (A02 != null && (optInt = A02.optInt("run_sampling")) > 0 && this.A04.nextInt(optInt) == 0) {
            return super.A0V(r4);
        }
        StringBuilder sb = new StringBuilder("DatabaseMigrationVerifier/doMigration; name=");
        sb.append(str);
        sb.append("; sampled, skipping.");
        Log.i(sb.toString());
        return true;
    }

    public final void A0X() {
        this.A01 = null;
        this.A00.clear();
        C16310on A02 = this.A05.A02();
        try {
            this.A06.A03("message_main_verification_failed_message_ids");
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final void A0Y() {
        HashSet hashSet;
        int i;
        C16310on A01 = this.A05.get();
        try {
            Set set = this.A01;
            if (set == null || set.isEmpty()) {
                hashSet = null;
            } else {
                hashSet = new HashSet();
                for (String str : this.A01) {
                    Map map = this.A00;
                    if (map.containsKey(str)) {
                        hashSet.add((String) map.get(str));
                    } else {
                        StringBuilder sb = new StringBuilder();
                        sb.append("MIGRATION_GET_FAILED_ROW_QUERY_FOR_");
                        sb.append(this.A0C);
                        sb.toString();
                        Cursor A09 = A01.A03.A09(C38411o3.A01, new String[]{str});
                        if (A09.moveToFirst()) {
                            String A0W = A0W(A09);
                            if (!TextUtils.isEmpty(A0W)) {
                                hashSet.add(A0W);
                            }
                        }
                        A09.close();
                    }
                }
            }
            A01.close();
            A0X();
            if (hashSet != null && hashSet.size() > 0) {
                Iterator it = hashSet.iterator();
                while (it.hasNext()) {
                    String str2 = (String) it.next();
                    StringBuilder sb2 = new StringBuilder("DatabaseMigrationVerifier/collectFailureLogsAndThrowError/error; name=");
                    String str3 = this.A0C;
                    sb2.append(str3);
                    sb2.append(" ,Message: ");
                    sb2.append(str2);
                    Log.e(sb2.toString());
                    JSONObject A02 = this.A0A.A02(str3);
                    if (A02 != null) {
                        i = A02.optInt("critical_event_sampling");
                    } else {
                        i = 0;
                    }
                    StringBuilder sb3 = new StringBuilder("verification-failed-");
                    sb3.append(str3);
                    String obj = sb3.toString();
                    if (i != 0 && this.A04.nextInt(i) == 0) {
                        this.A02.AaV(obj, str2, false);
                    }
                }
                StringBuilder sb4 = new StringBuilder();
                sb4.append(this.A0C);
                sb4.append(": verification failed");
                throw new AnonymousClass2F1(sb4.toString());
            }
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    @Override // X.AbstractC19010tQ
    public void onRollback() {
        if (!(this instanceof C19240tn)) {
            A0X();
            return;
        }
        A0X();
        C16310on A02 = this.A05.A02();
        try {
            A02.A03.A0B("DROP VIEW IF EXISTS message_view_old_schema");
            this.A06.A03("message_main_verification_done");
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
