package X;

import android.os.Handler;
import java.util.concurrent.Executor;

/* renamed from: X.5E5  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5E5 implements Executor {
    public final Handler A00 = C12970iu.A0E();

    @Override // java.util.concurrent.Executor
    public void execute(Runnable runnable) {
        this.A00.post(runnable);
    }
}
