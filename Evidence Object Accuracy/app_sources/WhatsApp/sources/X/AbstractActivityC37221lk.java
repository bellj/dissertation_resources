package X;

import android.os.Bundle;
import android.text.InputFilter;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import com.facebook.redex.ViewOnClickCListenerShape1S0100000_I0_1;
import com.google.android.material.textfield.TextInputLayout;
import com.whatsapp.R;
import com.whatsapp.WaEditText;
import com.whatsapp.community.NewCommunityActivity;
import com.whatsapp.util.ViewOnClickCListenerShape13S0100000_I0;
import com.whatsapp.util.ViewOnClickCListenerShape17S0100000_I1;

/* renamed from: X.1lk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC37221lk extends AbstractActivityC37231ll {
    public ImageView A00;
    public WaEditText A01;
    public WaEditText A02;
    public AnonymousClass1BF A03;
    public AnonymousClass10T A04;
    public AnonymousClass131 A05;
    public AnonymousClass11F A06;
    public C22050yP A07;
    public C16630pM A08;
    public AnonymousClass10Z A09;

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        int i;
        super.onCreate(bundle);
        setContentView(R.layout.activity_new_community);
        this.A00 = (ImageView) AnonymousClass00T.A05(this, R.id.icon);
        this.A02 = (WaEditText) AnonymousClass00T.A05(this, R.id.group_name);
        this.A01 = (WaEditText) AnonymousClass00T.A05(this, R.id.community_description);
        A1e((Toolbar) findViewById(R.id.toolbar));
        boolean z = this instanceof NewCommunityActivity;
        AbstractC005102i A1U = A1U();
        AnonymousClass009.A05(A1U);
        A1U.A0P(true);
        if (!z) {
            A1U.A0M(true);
            i = R.string.edit_community;
        } else {
            A1U.A0M(true);
            i = R.string.new_community;
        }
        A1U.A0A(i);
        this.A00.setImageDrawable(this.A06.A00(getTheme(), getResources(), AnonymousClass51K.A00, R.drawable.avatar_parent_large));
        this.A00.setOnClickListener(new ViewOnClickCListenerShape1S0100000_I0_1(this, 20));
        TextInputLayout textInputLayout = (TextInputLayout) findViewById(R.id.name_text_container);
        if (textInputLayout != null) {
            textInputLayout.setHint(getString(R.string.community_name_hint));
        }
        this.A01 = (WaEditText) AnonymousClass00T.A05(this, R.id.community_description);
        int A02 = ((ActivityC13810kN) this).A06.A02(AbstractC15460nI.A1P);
        this.A01.setFilters(new InputFilter[]{new C100654mG(A02)});
        WaEditText waEditText = this.A01;
        waEditText.addTextChangedListener(new AnonymousClass367(waEditText, (TextView) findViewById(R.id.description_counter), ((ActivityC13810kN) this).A08, ((ActivityC13830kP) this).A01, ((ActivityC13810kN) this).A0B, this.A08, A02, A02 / 10, false));
        ScrollView scrollView = (ScrollView) AnonymousClass00T.A05(this, R.id.scrollView);
        scrollView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener(scrollView, this) { // from class: X.4nw
            public final /* synthetic */ ScrollView A00;
            public final /* synthetic */ AbstractActivityC37221lk A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
            public final void onGlobalLayout() {
                AbstractActivityC37221lk r0 = this.A01;
                ScrollView scrollView2 = this.A00;
                if (r0.A01.hasFocus()) {
                    scrollView2.smoothScrollBy(0, scrollView2.getMaxScrollAmount());
                }
            }
        });
        this.A01.setOnFocusChangeListener(new View.OnFocusChangeListener(scrollView) { // from class: X.4mm
            public final /* synthetic */ ScrollView A00;

            {
                this.A00 = r1;
            }

            @Override // android.view.View.OnFocusChangeListener
            public final void onFocusChange(View view, boolean z2) {
                ScrollView scrollView2 = this.A00;
                if (z2) {
                    scrollView2.requestLayout();
                }
            }
        });
        this.A01.setOnTouchListener(new View.OnTouchListener(scrollView, new AnonymousClass0MU(this, new C73623gX(scrollView, this))) { // from class: X.4nV
            public final /* synthetic */ ScrollView A00;
            public final /* synthetic */ AnonymousClass0MU A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // android.view.View.OnTouchListener
            public final boolean onTouch(View view, MotionEvent motionEvent) {
                AnonymousClass0MU r0 = this.A01;
                ScrollView scrollView2 = this.A00;
                r0.A00.AXZ(motionEvent);
                if ((motionEvent.getAction() & 255) == 1) {
                    scrollView2.requestDisallowInterceptTouchEvent(false);
                }
                return false;
            }
        });
        if (!z) {
            ImageView imageView = (ImageView) AnonymousClass00T.A05(this, R.id.new_community_next_button);
            imageView.setImageDrawable(AnonymousClass00T.A04(this, R.drawable.ic_fab_check));
            imageView.setOnClickListener(new ViewOnClickCListenerShape13S0100000_I0(this, 40));
            return;
        }
        ImageView imageView2 = (ImageView) AnonymousClass00T.A05(this, R.id.new_community_next_button);
        imageView2.setImageDrawable(new AnonymousClass2GF(AnonymousClass00T.A04(this, R.drawable.ic_fab_next), ((ActivityC13830kP) this).A01));
        imageView2.setOnClickListener(new ViewOnClickCListenerShape17S0100000_I1(this, 45));
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        onBackPressed();
        return true;
    }
}
