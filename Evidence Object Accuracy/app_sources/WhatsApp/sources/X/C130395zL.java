package X;

import android.text.TextUtils;
import java.util.regex.Pattern;

/* renamed from: X.5zL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130395zL {
    public static final Pattern A00 = Pattern.compile("[0-9]+");

    public static boolean A00(C14850m9 r3, String str) {
        if (!r3.A07(1458)) {
            return false;
        }
        String A03 = r3.A03(1459);
        if (TextUtils.isEmpty(A03) || TextUtils.isEmpty(str) || !A03.contains(str)) {
            return false;
        }
        return true;
    }
}
