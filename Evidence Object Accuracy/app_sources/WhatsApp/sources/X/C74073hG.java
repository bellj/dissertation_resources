package X;

import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.whatsapp.inappsupport.ui.FaqItemActivityV2;

/* renamed from: X.3hG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74073hG extends WebViewClient {
    public final /* synthetic */ FaqItemActivityV2 A00;

    public C74073hG(FaqItemActivityV2 faqItemActivityV2) {
        this.A00 = faqItemActivityV2;
    }

    @Override // android.webkit.WebViewClient
    public void onPageFinished(WebView webView, String str) {
        this.A00.A00.A00();
    }
}
