package X;

import android.os.Bundle;
import com.whatsapp.catalogcategory.view.fragment.CatalogAllCategoryFragment;
import com.whatsapp.jid.UserJid;

/* renamed from: X.3AT  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3AT {
    public static final CatalogAllCategoryFragment A00(AnonymousClass4A0 r3, UserJid userJid, String str) {
        C16700pc.A0E(r3, 2);
        Bundle A0D = C12970iu.A0D();
        A0D.putString("parent_category_id", str);
        A0D.putParcelable("category_biz_id", userJid);
        A0D.putString("category_display_context", r3.name());
        CatalogAllCategoryFragment catalogAllCategoryFragment = new CatalogAllCategoryFragment();
        catalogAllCategoryFragment.A0U(A0D);
        return catalogAllCategoryFragment;
    }
}
