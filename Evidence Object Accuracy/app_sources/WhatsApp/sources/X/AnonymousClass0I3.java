package X;

/* renamed from: X.0I3  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0I3 extends AnonymousClass0P4 {
    public float A00 = 0.0f;
    public final /* synthetic */ C06540Ua A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public /* synthetic */ AnonymousClass0I3(C06540Ua r2) {
        super(r2);
        this.A01 = r2;
    }

    @Override // X.AnonymousClass0P4
    public void A00(String str) {
        this.A00 += this.A01.A03.A00.measureText(str);
    }
}
