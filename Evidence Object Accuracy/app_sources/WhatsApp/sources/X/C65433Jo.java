package X;

import android.animation.ValueAnimator;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.3Jo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C65433Jo implements ValueAnimator.AnimatorUpdateListener {
    public final RecyclerView A00;
    public final C54652h4 A01;

    public /* synthetic */ C65433Jo(RecyclerView recyclerView, C54652h4 r2) {
        this.A00 = recyclerView;
        this.A01 = r2;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.A01.A00 = C12960it.A00(valueAnimator);
        this.A00.invalidate();
    }
}
