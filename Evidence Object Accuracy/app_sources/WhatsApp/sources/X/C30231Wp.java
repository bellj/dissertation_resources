package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.1Wp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30231Wp implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C99714kk();
    public final C30221Wo A00;
    public final C30221Wo A01;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C30231Wp(C30221Wo r1, C30221Wo r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public C30231Wp(Parcel parcel) {
        this.A00 = (C30221Wo) parcel.readParcelable(C30221Wo.class.getClassLoader());
        this.A01 = (C30221Wo) parcel.readParcelable(C30221Wo.class.getClassLoader());
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (!(obj instanceof C30231Wp)) {
            return false;
        }
        C30231Wp r4 = (C30231Wp) obj;
        if (!C29941Vi.A00(this.A00, r4.A00) || !C29941Vi.A00(this.A01, r4.A01)) {
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        int i;
        C30221Wo r0 = this.A00;
        int i2 = 0;
        if (r0 != null) {
            i = r0.hashCode();
        } else {
            i = 0;
        }
        int i3 = i * 31;
        C30221Wo r02 = this.A01;
        if (r02 != null) {
            i2 = r02.hashCode();
        }
        return i3 + i2;
    }

    @Override // java.lang.Object
    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder("LinkedAccounts:{'facebookPage'='");
        C30221Wo r0 = this.A00;
        String str2 = null;
        if (r0 != null) {
            str = r0.toString();
        } else {
            str = null;
        }
        sb.append(str);
        sb.append("', 'instagramPage'='");
        C30221Wo r02 = this.A01;
        if (r02 != null) {
            str2 = r02.toString();
        }
        sb.append(str2);
        sb.append("'}");
        return sb.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.A00, i);
        parcel.writeParcelable(this.A01, i);
    }
}
