package X;

import android.content.Context;
import android.content.Intent;
import com.whatsapp.util.Log;

/* renamed from: X.1Tv  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1Tv {
    public static boolean A00(Context context, Intent intent) {
        try {
            AnonymousClass00T.A0F(context, intent);
            return true;
        } catch (IllegalStateException e) {
            StringBuilder sb = new StringBuilder("Failed to start foreground service ");
            sb.append(intent.getComponent());
            Log.w(sb.toString(), e);
            return false;
        }
    }
}
