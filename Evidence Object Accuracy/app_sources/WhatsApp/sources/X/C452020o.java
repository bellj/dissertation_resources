package X;

import android.content.Context;
import com.facebook.redex.RunnableBRunnable0Shape6S0200000_I0_6;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.20o  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C452020o extends AbstractC451020e {
    public final /* synthetic */ AnonymousClass1FK A00;
    public final /* synthetic */ AnonymousClass1A7 A01;
    public final /* synthetic */ AnonymousClass20B A02;
    public final /* synthetic */ boolean A03;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C452020o(Context context, C14900mE r2, AnonymousClass1FK r3, C18650sn r4, AnonymousClass1A7 r5, AnonymousClass20B r6, boolean z) {
        super(context, r2, r4);
        this.A01 = r5;
        this.A02 = r6;
        this.A03 = z;
        this.A00 = r3;
    }

    @Override // X.AbstractC451020e
    public void A02(C452120p r1) {
        A03(r1);
    }

    @Override // X.AbstractC451020e
    public void A03(C452120p r3) {
        AbstractC16870pt ACx = this.A01.A09.A02().ACx();
        if (ACx != null) {
            ACx.AKa(r3, 11);
        }
        AnonymousClass1FK r0 = this.A00;
        if (r0 != null) {
            r0.AVA(r3);
        }
    }

    @Override // X.AbstractC451020e
    public void A04(AnonymousClass1V8 r14) {
        ArrayList arrayList;
        AnonymousClass1V8[] r0;
        int length;
        AnonymousClass1A7 r5 = this.A01;
        C17070qD r8 = r5.A09;
        AbstractC16870pt ACx = r8.A02().ACx();
        if (ACx != null) {
            ACx.AKa(null, 11);
        }
        AnonymousClass46P r4 = new AnonymousClass46P();
        C22980zx r11 = r5.A0A;
        AnonymousClass20B r10 = this.A02;
        AnonymousClass1V8 A0E = r14.A0E("account");
        if (A0E == null || (r0 = A0E.A03) == null || (length = r0.length) <= 0) {
            arrayList = null;
        } else {
            arrayList = new ArrayList(length);
            int i = 0;
            do {
                AnonymousClass1V8 A0D = A0E.A0D(i);
                AnonymousClass009.A05(A0D);
                if ("transaction".equals(A0D.A00)) {
                    arrayList.add(r11.A05(r10, A0D));
                }
                i++;
            } while (i < length);
        }
        r4.A01 = arrayList;
        AnonymousClass1V8 A0E2 = r14.A0E("account");
        if (A0E2 != null) {
            r4.A00 = C22980zx.A00(A0E2, "after");
        }
        if (this.A03) {
            C18600si r6 = r5.A06;
            long A00 = r6.A01.A00();
            r6.A01().edit().putLong("payments_all_transactions_last_sync_time", A00).apply();
            C30931Zj r62 = r6.A02;
            StringBuilder sb = new StringBuilder("updateAllTransactionsLastSyncTimeMilli to: ");
            sb.append(A00);
            r62.A06(sb.toString());
        }
        List list = r4.A01;
        if (list != null && list.size() > 0) {
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = new ArrayList();
            for (AnonymousClass1IR r9 : r4.A01) {
                AnonymousClass1IS r1 = new AnonymousClass1IS(r9.A0C, r9.A0L, r9.A0Q);
                if (r9.A0P || r1.A00 == null || r1.A01 == null) {
                    arrayList3.add(r9);
                } else {
                    arrayList2.add(new AnonymousClass01T(r1, r9));
                }
            }
            if (!arrayList3.isEmpty()) {
                r8.A03();
                C38051nR r3 = r8.A00;
                AnonymousClass009.A05(r3);
                AnonymousClass009.A05(r3);
                r3.A06(new RunnableBRunnable0Shape6S0200000_I0_6(this, 39, arrayList3), r4.A01);
            }
            if (!arrayList2.isEmpty()) {
                Iterator it = arrayList2.iterator();
                while (it.hasNext()) {
                    AnonymousClass01T r02 = (AnonymousClass01T) it.next();
                    r5.A03.A0K((AnonymousClass1IR) r02.A01, (AnonymousClass1IS) r02.A00);
                }
            }
        }
        AnonymousClass1FK r03 = this.A00;
        if (r03 != null) {
            r03.AVB(r4);
        }
    }
}
