package X;

import android.view.View;
import android.widget.ImageView;
import com.whatsapp.R;

/* renamed from: X.3aX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69973aX implements AnonymousClass5Wu {
    public ImageView A00;
    public final C16590pI A01;
    public final C22590zK A02;
    public final AbstractC14440lR A03;

    public C69973aX(C16590pI r1, C22590zK r2, AbstractC14440lR r3) {
        this.A01 = r1;
        this.A03 = r3;
        this.A02 = r2;
    }

    public int A00() {
        return (int) C16590pI.A00(this.A01).getDimension(R.dimen.payment_bubble_amount_height);
    }

    /* renamed from: A01 */
    public void A6Q(AnonymousClass4OZ r5) {
        Object obj;
        if (r5 != null && (obj = r5.A01) != null) {
            AnonymousClass4S0 r3 = (AnonymousClass4S0) obj;
            boolean z = r3.A03;
            ImageView imageView = this.A00;
            if (z) {
                C12960it.A1E(new AnonymousClass37L(imageView, this, r3), this.A03);
            } else {
                imageView.setImageResource(r3.A00);
            }
        }
    }

    @Override // X.AnonymousClass5Wu
    public int ADq() {
        return R.layout.conversation_invite_image_view;
    }

    @Override // X.AnonymousClass5Wu
    public void AYL(View view) {
        this.A00 = C12970iu.A0K(view, R.id.payment_invite_bubble_icon);
    }
}
