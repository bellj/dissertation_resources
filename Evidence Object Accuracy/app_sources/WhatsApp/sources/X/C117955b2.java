package X;

/* renamed from: X.5b2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117955b2 extends AnonymousClass015 {
    public AnonymousClass18O A00;
    public final AnonymousClass016 A01 = C12980iv.A0T();
    public final AnonymousClass016 A02 = C12980iv.A0T();

    public C117955b2(AnonymousClass18O r2) {
        this.A00 = r2;
    }

    public void A04(AnonymousClass2SO r3, C452120p r4) {
        if (r4 != null) {
            this.A01.A0B(r4);
        } else {
            this.A00.A01(r3);
        }
        this.A02.A0B(Boolean.FALSE);
    }
}
