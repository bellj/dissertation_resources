package X;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.CircularProgressBar;
import com.whatsapp.Conversation;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.conversation.conversationrow.ConversationRowVideo$RowVideoView;
import com.whatsapp.util.ViewOnClickCListenerShape18S0100000_I1_1;
import java.io.File;

/* renamed from: X.2yk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60852yk extends AbstractC60612yJ {
    public C14330lG A00;
    public C50362Pg A01;
    public C14850m9 A02;
    public AnonymousClass1CH A03;
    public boolean A04;
    public final View A05;
    public final View A06;
    public final FrameLayout A07;
    public final FrameLayout A08;
    public final ImageView A09;
    public final ImageView A0A;
    public final TextView A0B = C12960it.A0J(this, R.id.control_btn);
    public final TextView A0C;
    public final TextView A0D;
    public final CircularProgressBar A0E;
    public final TextEmojiLabel A0F;
    public final ConversationRowVideo$RowVideoView A0G = ((ConversationRowVideo$RowVideoView) AnonymousClass028.A0D(this, R.id.thumb));
    public final AbstractC41521tf A0H = new C70263b0(this);
    public final AbstractView$OnClickListenerC34281fs A0I = new ViewOnClickCListenerShape18S0100000_I1_1(this, 17);

    public C60852yk(Context context, AbstractC13890kV r5, AnonymousClass1X2 r6) {
        super(context, r5, r6);
        CircularProgressBar circularProgressBar = (CircularProgressBar) findViewById(R.id.progress_bar);
        this.A0E = circularProgressBar;
        this.A0C = C12960it.A0J(this, R.id.info);
        this.A08 = (FrameLayout) findViewById(R.id.play_frame);
        this.A0A = C12970iu.A0L(this, R.id.play_button);
        this.A09 = C12970iu.A0L(this, R.id.cancel_btn);
        this.A07 = (FrameLayout) findViewById(R.id.invisible_press_surface);
        this.A05 = findViewById(R.id.control_frame);
        this.A06 = findViewById(R.id.text_and_date);
        TextEmojiLabel A0U = C12970iu.A0U(this, R.id.caption);
        this.A0F = A0U;
        if (A0U != null) {
            AbstractC28491Nn.A03(A0U);
        }
        this.A0D = C12960it.A0I(this, R.id.media_transfer_eta);
        circularProgressBar.setMax(100);
        circularProgressBar.A0B = 0;
        circularProgressBar.A06 = 6.0f;
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.conversation_row_video_corner_progressbar_padding);
        circularProgressBar.setPadding(dimensionPixelSize, dimensionPixelSize, dimensionPixelSize, dimensionPixelSize);
        A0b(true);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0039, code lost:
        if (r7 <= 500) goto L_0x003b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00bf  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00fc A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0111  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A0b(boolean r22) {
        /*
        // Method dump skipped, instructions count: 555
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C60852yk.A0b(boolean):void");
    }

    @Override // X.AnonymousClass1OY
    public int A0n(int i) {
        if (!AnonymousClass1OY.A0T(this)) {
            return super.A0n(i);
        }
        return 0;
    }

    @Override // X.AnonymousClass1OY
    public void A0s() {
        A0b(false);
        A1H(false);
    }

    @Override // X.AbstractC42671vd, X.AnonymousClass1OY
    public void A0y() {
        String str;
        if (((AbstractC42671vd) this).A01 == null || AnonymousClass1OY.A0U(this)) {
            AbstractC16130oV r5 = (AbstractC16130oV) ((AbstractC28551Oa) this).A0O;
            C16150oX A00 = AbstractC15340mz.A00(r5);
            if (A00.A07 == 1) {
                ((AnonymousClass1OY) this).A0J.A05(R.string.gallery_unsafe_video_removed, 1);
                return;
            }
            C28921Pn A002 = this.A03.A00(A00);
            AnonymousClass1IS r3 = r5.A0z;
            boolean z = r3.A02;
            if (!z) {
                if (!A00.A0a || A002 == null || A002.A0h == null) {
                    if (!A00.A0P) {
                        return;
                    }
                }
                A1P();
            } else if (!A00.A0P && !A00.A0O && (((str = A00.A0H) != null || (A00.A0D >= 0 && A00.A0E > 0)) && ((A00.A0D >= 0 && A00.A0E > 0) || C22200yh.A0I(this.A00, str).exists()))) {
                boolean A12 = C30041Vv.A12(r5);
                int i = R.string.cannot_play_video_sending_failed;
                if (A12) {
                    i = R.string.cannot_play_video_still_in_process;
                }
                ((AnonymousClass1OY) this).A0J.A05(i, 1);
                return;
            }
            boolean z2 = false;
            File file = A00.A0F;
            if (file != null) {
                z2 = C12980iv.A0h(file).exists();
            }
            AnonymousClass1OY.A0P(A00, r5, C12960it.A0k("viewmessage/ from_me:"), z);
            if (!z2) {
                AnonymousClass1OY.A0S(this, r3);
                return;
            }
            A1P();
        }
    }

    @Override // X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r2, boolean z) {
        boolean A1X = C12960it.A1X(r2, ((AbstractC28551Oa) this).A0O);
        super.A1D(r2, z);
        if (z || A1X) {
            A0b(A1X);
        }
    }

    public final void A1P() {
        boolean A08 = ((AbstractC28551Oa) this).A0b.A08();
        int i = 1;
        if (A08) {
            i = 3;
        }
        AbstractC15340mz r1 = ((AbstractC28551Oa) this).A0O;
        AnonymousClass2TS r2 = new AnonymousClass2TS(getContext());
        r2.A07 = A08;
        AnonymousClass1IS r3 = r1.A0z;
        AbstractC14640lm r0 = r3.A00;
        AnonymousClass009.A05(r0);
        r2.A03 = r0;
        r2.A04 = r3;
        r2.A02 = i;
        r2.A06 = C12960it.A1W(AbstractC35731ia.A01(getContext(), Conversation.class));
        Intent A00 = r2.A00();
        ConversationRowVideo$RowVideoView conversationRowVideo$RowVideoView = this.A0G;
        if (conversationRowVideo$RowVideoView != null) {
            AbstractC454421p.A07(getContext(), A00, conversationRowVideo$RowVideoView);
        }
        AnonymousClass1OY.A0E(A00, this, conversationRowVideo$RowVideoView, r3);
    }

    @Override // X.AnonymousClass1OY, android.view.ViewGroup, android.view.View
    public void dispatchSetPressed(boolean z) {
        boolean isPressed;
        super.dispatchSetPressed(z);
        ConversationRowVideo$RowVideoView conversationRowVideo$RowVideoView = this.A0G;
        if (conversationRowVideo$RowVideoView != null && conversationRowVideo$RowVideoView.A0B != (isPressed = isPressed())) {
            conversationRowVideo$RowVideoView.A0B = isPressed;
            conversationRowVideo$RowVideoView.A00();
            conversationRowVideo$RowVideoView.invalidate();
        }
    }

    @Override // X.AnonymousClass1OY
    public int getBroadcastDrawableId() {
        return AnonymousClass1OY.A03(this);
    }

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_video_left;
    }

    @Override // X.AbstractC42671vd, X.AbstractC28551Oa
    public AnonymousClass1X2 getFMessage() {
        return (AnonymousClass1X2) ((AbstractC16130oV) ((AbstractC28551Oa) this).A0O);
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_video_left;
    }

    @Override // X.AnonymousClass1OY
    public Drawable getKeepDrawable() {
        if (AnonymousClass1OY.A0T(this)) {
            return AnonymousClass2GE.A01(getContext(), R.drawable.keep, R.color.white);
        }
        return super.getKeepDrawable();
    }

    @Override // X.AbstractC28551Oa
    public int getMainChildMaxWidth() {
        return this.A0G.A06.A03();
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_video_right;
    }

    @Override // X.AnonymousClass1OY
    public Drawable getStarDrawable() {
        if (AnonymousClass1OY.A0T(this)) {
            return AnonymousClass00T.A04(getContext(), R.drawable.message_star_media);
        }
        return super.getStarDrawable();
    }

    @Override // X.AbstractC28551Oa, android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    @Override // X.AbstractC42671vd, X.AbstractC28551Oa
    public void setFMessage(AbstractC15340mz r2) {
        AnonymousClass009.A0F(r2 instanceof AnonymousClass1X2);
        super.setFMessage(r2);
    }

    /* access modifiers changed from: private */
    public void setThumbnail(Drawable drawable) {
        this.A0G.setImageDrawable(drawable);
    }
}
