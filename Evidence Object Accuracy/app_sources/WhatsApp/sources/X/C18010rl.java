package X;

import java.util.HashMap;

/* renamed from: X.0rl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18010rl {
    public final AnonymousClass3CX A00 = new AnonymousClass3CX();
    public final HashMap A01 = new HashMap();

    public AnonymousClass3FE A00(C90184Mx r5, String str) {
        AnonymousClass3FE r1 = new AnonymousClass3FE(r5.A00, r5.A01, this.A00);
        this.A01.put(str, r1);
        return r1;
    }
}
