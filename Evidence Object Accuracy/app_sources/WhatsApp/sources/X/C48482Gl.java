package X;

import android.view.View;
import com.whatsapp.R;

/* renamed from: X.2Gl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C48482Gl extends AnonymousClass04v {
    public final /* synthetic */ ActivityC13810kN A00;

    public C48482Gl(ActivityC13810kN r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r4) {
        super.A06(view, r4);
        r4.A02.setContentDescription(this.A00.getResources().getString(R.string.back));
    }
}
