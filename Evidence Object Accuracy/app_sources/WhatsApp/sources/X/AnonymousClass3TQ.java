package X;

import android.os.Handler;
import android.os.SystemClock;
import com.google.android.gms.common.api.Status;

/* renamed from: X.3TQ  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3TQ implements AbstractC115775Sw {
    public final int A00;
    public final long A01;
    public final long A02;
    public final AnonymousClass4XF A03;
    public final C65863Lh A04;

    public AnonymousClass3TQ(AnonymousClass4XF r1, C65863Lh r2, int i, long j, long j2) {
        this.A04 = r2;
        this.A00 = i;
        this.A03 = r1;
        this.A01 = j;
        this.A02 = j2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0033 A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C56422kr A00(X.C14970mL r6, X.AbstractC95064d1 r7, int r8) {
        /*
            X.2kw r0 = r7.A0Q
            if (r0 != 0) goto L_0x001f
            r5 = 0
        L_0x0005:
            r4 = 0
            if (r5 == 0) goto L_0x0034
            boolean r0 = r5.A02
            if (r0 == 0) goto L_0x0034
            int[] r3 = r5.A04
            if (r3 != 0) goto L_0x0022
            int[] r3 = r5.A05
            if (r3 == 0) goto L_0x002d
            int r2 = r3.length
            r1 = 0
        L_0x0016:
            if (r1 >= r2) goto L_0x002d
            r0 = r3[r1]
            if (r0 == r8) goto L_0x0034
            int r1 = r1 + 1
            goto L_0x0016
        L_0x001f:
            X.2kr r5 = r0.A02
            goto L_0x0005
        L_0x0022:
            int r2 = r3.length
            r1 = 0
        L_0x0024:
            if (r1 >= r2) goto L_0x0034
            r0 = r3[r1]
            if (r0 == r8) goto L_0x002d
            int r1 = r1 + 1
            goto L_0x0024
        L_0x002d:
            int r1 = r6.A00
            int r0 = r5.A00
            if (r1 >= r0) goto L_0x0034
            return r5
        L_0x0034:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3TQ.A00(X.0mL, X.4d1, int):X.2kr");
    }

    @Override // X.AbstractC115775Sw
    public final void AOR(C13600jz r22) {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        long j;
        int i6;
        C65863Lh r7 = this.A04;
        if (r7.A06()) {
            C56412kq r8 = C94784cX.A00().A00;
            if (r8 == null || r8.A03) {
                C14970mL r9 = (C14970mL) r7.A09.get(this.A03);
                if (r9 != null) {
                    AbstractC72443eb r3 = r9.A04;
                    if (r3 instanceof AbstractC95064d1) {
                        AbstractC95064d1 r32 = (AbstractC95064d1) r3;
                        long j2 = this.A01;
                        boolean z = true;
                        long j3 = 0;
                        boolean A1U = C12960it.A1U((j2 > 0 ? 1 : (j2 == 0 ? 0 : -1)));
                        int i7 = r32.A0E;
                        if (r8 != null) {
                            A1U &= r8.A04;
                            i2 = r8.A01;
                            i3 = r8.A02;
                            i = r8.A00;
                            if (r32.A0Q != null && !r32.AJG()) {
                                C56422kr A00 = A00(r9, r32, this.A00);
                                if (A00 != null) {
                                    if (!A00.A03 || j2 <= 0) {
                                        z = false;
                                    }
                                    i3 = A00.A00;
                                    A1U = z;
                                } else {
                                    return;
                                }
                            }
                        } else {
                            i = 0;
                            i2 = 5000;
                            i3 = 100;
                        }
                        if (r22.A0A()) {
                            i4 = 0;
                            i5 = 0;
                        } else {
                            if (r22.A05) {
                                i4 = 100;
                            } else {
                                Exception A002 = r22.A00();
                                if (A002 instanceof C630439z) {
                                    Status status = ((C630439z) A002).mStatus;
                                    i4 = status.A01;
                                    C56492ky r2 = status.A03;
                                    if (r2 != null) {
                                        i5 = r2.A01;
                                    }
                                } else {
                                    i4 = 101;
                                }
                            }
                            i5 = -1;
                        }
                        if (A1U) {
                            j3 = j2;
                            j = System.currentTimeMillis();
                            i6 = (int) (SystemClock.elapsedRealtime() - this.A02);
                        } else {
                            j = 0;
                            i6 = -1;
                        }
                        C78423ot r92 = new C78423ot(null, null, this.A00, i4, i5, i7, i6, j3, j);
                        long j4 = (long) i2;
                        Handler handler = r7.A06;
                        handler.sendMessage(handler.obtainMessage(18, new AnonymousClass4RA(r92, i, i3, j4)));
                    }
                }
            }
        }
    }
}
