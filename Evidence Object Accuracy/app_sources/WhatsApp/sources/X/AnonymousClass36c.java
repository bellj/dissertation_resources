package X;

import android.view.View;
import com.whatsapp.util.Log;
import java.util.Map;

/* renamed from: X.36c  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass36c extends AbstractView$OnClickListenerC34281fs {
    public final /* synthetic */ C68023Tu A00;
    public final /* synthetic */ String A01;
    public final /* synthetic */ Map A02;
    public final /* synthetic */ boolean A03;

    public AnonymousClass36c(C68023Tu r1, String str, Map map, boolean z) {
        this.A00 = r1;
        this.A03 = z;
        this.A01 = str;
        this.A02 = map;
    }

    @Override // X.AbstractView$OnClickListenerC34281fs
    public void A04(View view) {
        C53042cM r4;
        Log.i("UserNoticeBanner/update/banner tapped");
        boolean z = this.A03;
        C68023Tu r5 = this.A00;
        AnonymousClass12O r0 = r5.A05;
        if (z) {
            r0.A06();
            AnonymousClass12N r1 = r0.A08;
            C12970iu.A1C(r1.A00().edit(), "current_user_notice_banner_dismiss_timestamp", r0.A01.A00());
            AnonymousClass1CT r2 = r5.A03;
            r4 = r5.A01;
            r2.A01(r4.getContext(), true);
        } else {
            r0.A04();
            AnonymousClass1CT r3 = r5.A03;
            String str = this.A01;
            Map map = this.A02;
            r4 = r5.A01;
            r3.A00(r4.getContext(), str, map);
        }
        r5.A04.A01(C12970iu.A0g());
        View view2 = r5.A00;
        AnonymousClass009.A03(view2);
        view2.setVisibility(8);
        AnonymousClass01N r12 = r5.A06;
        if (r12.get() != null) {
            r4.A01((C90814Pi) r12.get());
        }
    }
}
