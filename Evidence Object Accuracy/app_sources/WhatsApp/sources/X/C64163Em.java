package X;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.ViewGroup;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.internal.IMapViewDelegate;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/* renamed from: X.3Em  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C64163Em {
    public Bundle A00;
    public AbstractC115105Qf A01;
    public AbstractC115115Qg A02;
    public LinkedList A03;
    public final Context A04;
    public final ViewGroup A05;
    public final AbstractC115115Qg A06 = new AnonymousClass4z6(this);
    public final GoogleMapOptions A07;
    public final List A08 = C12960it.A0l();

    public C64163Em(Context context, ViewGroup viewGroup, GoogleMapOptions googleMapOptions) {
        this.A05 = viewGroup;
        this.A04 = context;
        this.A07 = googleMapOptions;
    }

    public final void A00(int i) {
        while (!this.A03.isEmpty() && ((AbstractC116355Vc) this.A03.getLast()).AgL() >= i) {
            this.A03.removeLast();
        }
    }

    public final void A01(Bundle bundle, AbstractC116355Vc r6) {
        IMapViewDelegate iMapViewDelegate;
        AbstractC115105Qf r0 = this.A01;
        if (r0 != null) {
            r6.AgQ(r0);
            return;
        }
        LinkedList linkedList = this.A03;
        if (linkedList == null) {
            linkedList = new LinkedList();
            this.A03 = linkedList;
        }
        linkedList.add(r6);
        if (bundle != null) {
            Bundle bundle2 = this.A00;
            if (bundle2 == null) {
                this.A00 = (Bundle) bundle.clone();
            } else {
                bundle2.putAll(bundle);
            }
        }
        AbstractC115115Qg r02 = this.A06;
        this.A02 = r02;
        if (r02 != null && this.A01 == null) {
            try {
                Context context = this.A04;
                C35781ij.A00(context);
                AnonymousClass5YK A01 = AnonymousClass3GU.A01(context);
                BinderC56502l7 r2 = new BinderC56502l7(context);
                GoogleMapOptions googleMapOptions = this.A07;
                C65873Li r3 = (C65873Li) A01;
                Parcel A012 = r3.A01();
                C65183In.A00(r2, A012);
                C65183In.A01(A012, googleMapOptions);
                Parcel A02 = r3.A02(3, A012);
                IBinder readStrongBinder = A02.readStrongBinder();
                if (readStrongBinder == null) {
                    iMapViewDelegate = null;
                } else {
                    IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IMapViewDelegate");
                    if (queryLocalInterface instanceof IMapViewDelegate) {
                        iMapViewDelegate = (IMapViewDelegate) queryLocalInterface;
                    } else {
                        iMapViewDelegate = new C79813rF(readStrongBinder);
                    }
                }
                A02.recycle();
                if (iMapViewDelegate != null) {
                    AbstractC115115Qg r22 = this.A02;
                    AnonymousClass3T2 r03 = new AnonymousClass3T2(this.A05, iMapViewDelegate);
                    C64163Em r32 = ((AnonymousClass4z6) r22).A00;
                    r32.A01 = r03;
                    Iterator it = r32.A03.iterator();
                    while (it.hasNext()) {
                        ((AbstractC116355Vc) it.next()).AgQ(r32.A01);
                    }
                    r32.A03.clear();
                    r32.A00 = null;
                    List<AbstractC115755Su> list = this.A08;
                    for (AbstractC115755Su r1 : list) {
                        ((AnonymousClass3T2) this.A01).A00(r1);
                    }
                    list.clear();
                }
            } catch (AnonymousClass29w unused) {
            } catch (RemoteException e) {
                throw new C113245Gt(e);
            }
        }
    }
}
