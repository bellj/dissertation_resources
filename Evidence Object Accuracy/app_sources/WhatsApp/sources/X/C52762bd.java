package X;

import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.whatsapp.R;
import com.whatsapp.WaTextView;
import com.whatsapp.inappsupport.ui.SupportTopicsActivity;
import java.util.ArrayList;

/* renamed from: X.2bd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52762bd extends BaseAdapter {
    public SupportTopicsActivity A00;
    public ArrayList A01;

    @Override // android.widget.Adapter
    public long getItemId(int i) {
        return (long) i;
    }

    public C52762bd(SupportTopicsActivity supportTopicsActivity, ArrayList arrayList) {
        this.A00 = supportTopicsActivity;
        this.A01 = arrayList;
    }

    @Override // android.widget.Adapter
    public int getCount() {
        return this.A01.size();
    }

    @Override // android.widget.Adapter
    public /* bridge */ /* synthetic */ Object getItem(int i) {
        return this.A01.get(i);
    }

    @Override // android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        AnonymousClass4QK r0;
        AnonymousClass3M6 r5 = (AnonymousClass3M6) this.A01.get(i);
        if (view == null) {
            r0 = new AnonymousClass4QK(this);
            view = LayoutInflater.from(this.A00).inflate(R.layout.support_topic_list_item, viewGroup, false);
            r0.A01 = (WaTextView) view.findViewById(R.id.topic_title);
            r0.A00 = view.findViewById(R.id.topic_divider);
            view.setTag(r0);
        } else {
            r0 = (AnonymousClass4QK) view.getTag();
        }
        WaTextView waTextView = r0.A01;
        waTextView.setText(r5.A03);
        C12960it.A14(waTextView, this, r5, 14);
        if (this.A00.A01 == 2) {
            int i2 = (int) (Resources.getSystem().getDisplayMetrics().density * 16.0f);
            waTextView.setPadding(i2, i2, i2, i2);
            r0.A00.setVisibility(0);
            return view;
        }
        r0.A00.setVisibility(8);
        return view;
    }
}
