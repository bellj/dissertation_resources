package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.25J  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass25J extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass25J A04;
    public static volatile AnonymousClass255 A05;
    public byte A00 = -1;
    public int A01;
    public long A02;
    public long A03;

    static {
        AnonymousClass25J r0 = new AnonymousClass25J();
        A04 = r0;
        r0.A0W();
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A01;
        if ((i3 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A06(44, this.A03);
        }
        if ((i3 & 2) == 2) {
            i2 += CodedOutputStream.A06(45, this.A02);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A01 & 1) == 1) {
            codedOutputStream.A0H(44, this.A03);
        }
        if ((this.A01 & 2) == 2) {
            codedOutputStream.A0H(45, this.A02);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
