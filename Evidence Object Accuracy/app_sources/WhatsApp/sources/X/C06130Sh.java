package X;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.InflateException;
import androidx.preference.Preference;
import androidx.preference.PreferenceGroup;
import androidx.preference.SwitchPreference;
import java.io.IOException;
import java.util.HashMap;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/* renamed from: X.0Sh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06130Sh {
    public static final HashMap A04 = new HashMap();
    public static final Class[] A05 = {Context.class, AttributeSet.class};
    public AnonymousClass047 A00;
    public String[] A01;
    public final Context A02;
    public final Object[] A03 = new Object[2];

    public C06130Sh(Context context, AnonymousClass047 r6) {
        this.A02 = context;
        this.A00 = r6;
        StringBuilder sb = new StringBuilder();
        sb.append(Preference.class.getPackage().getName());
        sb.append(".");
        StringBuilder sb2 = new StringBuilder();
        sb2.append(SwitchPreference.class.getPackage().getName());
        sb2.append(".");
        this.A01 = new String[]{sb.toString(), sb2.toString()};
    }

    public final Preference A00(AttributeSet attributeSet, String str) {
        try {
            if (-1 == str.indexOf(46)) {
                return A01(attributeSet, str, this.A01);
            }
            return A01(attributeSet, str, null);
        } catch (InflateException e) {
            throw e;
        } catch (ClassNotFoundException e2) {
            StringBuilder sb = new StringBuilder();
            sb.append(attributeSet.getPositionDescription());
            sb.append(": Error inflating class (not found)");
            sb.append(str);
            InflateException inflateException = new InflateException(sb.toString());
            inflateException.initCause(e2);
            throw inflateException;
        } catch (Exception e3) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(attributeSet.getPositionDescription());
            sb2.append(": Error inflating class ");
            sb2.append(str);
            InflateException inflateException2 = new InflateException(sb2.toString());
            inflateException2.initCause(e3);
            throw inflateException2;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0057, code lost:
        throw r4;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final androidx.preference.Preference A01(android.util.AttributeSet r11, java.lang.String r12, java.lang.String[] r13) {
        /*
            r10 = this;
            java.util.HashMap r8 = X.C06130Sh.A04
            java.lang.Object r1 = r8.get(r12)
            java.lang.reflect.Constructor r1 = (java.lang.reflect.Constructor) r1
            java.lang.String r3 = ": Error inflating class "
            r7 = 1
            if (r1 != 0) goto L_0x0068
            android.content.Context r0 = r10.A02     // Catch: ClassNotFoundException -> 0x0093, Exception -> 0x0073
            java.lang.ClassLoader r9 = r0.getClassLoader()     // Catch: ClassNotFoundException -> 0x0093, Exception -> 0x0073
            r6 = 0
            if (r13 == 0) goto L_0x0058
            int r5 = r13.length     // Catch: ClassNotFoundException -> 0x0093, Exception -> 0x0073
            if (r5 == 0) goto L_0x0058
            r4 = 0
            r2 = 0
        L_0x001b:
            r1 = r13[r2]     // Catch: ClassNotFoundException -> 0x0093, Exception -> 0x0073
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch: ClassNotFoundException -> 0x0033, Exception -> 0x0073
            r0.<init>()     // Catch: ClassNotFoundException -> 0x0033, Exception -> 0x0073
            r0.append(r1)     // Catch: ClassNotFoundException -> 0x0033, Exception -> 0x0073
            r0.append(r12)     // Catch: ClassNotFoundException -> 0x0033, Exception -> 0x0073
            java.lang.String r0 = r0.toString()     // Catch: ClassNotFoundException -> 0x0033, Exception -> 0x0073
            java.lang.Class r1 = java.lang.Class.forName(r0, r6, r9)     // Catch: ClassNotFoundException -> 0x0033, Exception -> 0x0073
            if (r1 != 0) goto L_0x005c
            goto L_0x0039
        L_0x0033:
            r4 = move-exception
            int r2 = r2 + 1
            if (r2 >= r5) goto L_0x0057
            goto L_0x001b
        L_0x0039:
            if (r4 != 0) goto L_0x0057
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: ClassNotFoundException -> 0x0093, Exception -> 0x0073
            r1.<init>()     // Catch: ClassNotFoundException -> 0x0093, Exception -> 0x0073
            java.lang.String r0 = r11.getPositionDescription()     // Catch: ClassNotFoundException -> 0x0093, Exception -> 0x0073
            r1.append(r0)     // Catch: ClassNotFoundException -> 0x0093, Exception -> 0x0073
            r1.append(r3)     // Catch: ClassNotFoundException -> 0x0093, Exception -> 0x0073
            r1.append(r12)     // Catch: ClassNotFoundException -> 0x0093, Exception -> 0x0073
            java.lang.String r1 = r1.toString()     // Catch: ClassNotFoundException -> 0x0093, Exception -> 0x0073
            android.view.InflateException r0 = new android.view.InflateException     // Catch: ClassNotFoundException -> 0x0093, Exception -> 0x0073
            r0.<init>(r1)     // Catch: ClassNotFoundException -> 0x0093, Exception -> 0x0073
            throw r0     // Catch: ClassNotFoundException -> 0x0093, Exception -> 0x0073
        L_0x0057:
            throw r4     // Catch: ClassNotFoundException -> 0x0093, Exception -> 0x0073
        L_0x0058:
            java.lang.Class r1 = java.lang.Class.forName(r12, r6, r9)     // Catch: ClassNotFoundException -> 0x0093, Exception -> 0x0073
        L_0x005c:
            java.lang.Class[] r0 = X.C06130Sh.A05     // Catch: ClassNotFoundException -> 0x0093, Exception -> 0x0073
            java.lang.reflect.Constructor r1 = r1.getConstructor(r0)     // Catch: ClassNotFoundException -> 0x0093, Exception -> 0x0073
            r1.setAccessible(r7)     // Catch: ClassNotFoundException -> 0x0093, Exception -> 0x0073
            r8.put(r12, r1)     // Catch: ClassNotFoundException -> 0x0093, Exception -> 0x0073
        L_0x0068:
            java.lang.Object[] r0 = r10.A03     // Catch: ClassNotFoundException -> 0x0093, Exception -> 0x0073
            r0[r7] = r11     // Catch: ClassNotFoundException -> 0x0093, Exception -> 0x0073
            java.lang.Object r0 = r1.newInstance(r0)     // Catch: ClassNotFoundException -> 0x0093, Exception -> 0x0073
            androidx.preference.Preference r0 = (androidx.preference.Preference) r0     // Catch: ClassNotFoundException -> 0x0093, Exception -> 0x0073
            return r0
        L_0x0073:
            r2 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r0 = r11.getPositionDescription()
            r1.append(r0)
            r1.append(r3)
            r1.append(r12)
            java.lang.String r1 = r1.toString()
            android.view.InflateException r0 = new android.view.InflateException
            r0.<init>(r1)
            r0.initCause(r2)
            throw r0
        L_0x0093:
            r0 = move-exception
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06130Sh.A01(android.util.AttributeSet, java.lang.String, java.lang.String[]):androidx.preference.Preference");
    }

    public final void A02(AttributeSet attributeSet, Preference preference, XmlPullParser xmlPullParser) {
        int depth = xmlPullParser.getDepth();
        while (true) {
            int next = xmlPullParser.next();
            if (next == 3) {
                if (xmlPullParser.getDepth() <= depth) {
                    return;
                }
            } else if (next == 1) {
                return;
            } else {
                if (next == 2) {
                    String name = xmlPullParser.getName();
                    if ("intent".equals(name)) {
                        try {
                            preference.A06 = Intent.parseIntent(this.A02.getResources(), xmlPullParser, attributeSet);
                        } catch (IOException e) {
                            XmlPullParserException xmlPullParserException = new XmlPullParserException("Error parsing preference");
                            xmlPullParserException.initCause(e);
                            throw xmlPullParserException;
                        }
                    } else if ("extra".equals(name)) {
                        Resources resources = this.A02.getResources();
                        Bundle bundle = preference.A08;
                        if (bundle == null) {
                            bundle = new Bundle();
                            preference.A08 = bundle;
                        }
                        resources.parseBundleExtra("extra", attributeSet, bundle);
                        try {
                            int depth2 = xmlPullParser.getDepth();
                            while (true) {
                                int next2 = xmlPullParser.next();
                                if (next2 != 1 && (next2 != 3 || xmlPullParser.getDepth() > depth2)) {
                                }
                            }
                        } catch (IOException e2) {
                            XmlPullParserException xmlPullParserException2 = new XmlPullParserException("Error parsing preference");
                            xmlPullParserException2.initCause(e2);
                            throw xmlPullParserException2;
                        }
                    } else {
                        Preference A00 = A00(attributeSet, name);
                        ((PreferenceGroup) preference).A0U(A00);
                        A02(attributeSet, A00, xmlPullParser);
                    }
                } else {
                    continue;
                }
            }
        }
    }
}
