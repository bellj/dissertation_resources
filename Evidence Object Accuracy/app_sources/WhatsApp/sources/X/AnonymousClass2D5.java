package X;

/* renamed from: X.2D5  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2D5 extends Thread {
    public AnonymousClass2D5() {
        super("DestructorThread");
    }

    @Override // java.lang.Thread, java.lang.Runnable
    public void run() {
        while (true) {
            try {
                AbstractC29071Qg r4 = (AbstractC29071Qg) C47842Cy.A03.remove();
                r4.destruct();
                if (r4.previous == null) {
                    AbstractC29071Qg r3 = (AbstractC29071Qg) C47842Cy.A01.A00.getAndSet(null);
                    while (r3 != null) {
                        AbstractC29071Qg r2 = r3.next;
                        AbstractC29071Qg r1 = C47842Cy.A00.A00;
                        r3.next = r1.next;
                        r1.next = r3;
                        AbstractC29071Qg r0 = r3.next;
                        if (r0 != null) {
                            r0.previous = r3;
                        }
                        r3.previous = r1;
                        r3 = r2;
                    }
                }
                AbstractC29071Qg r12 = r4.next;
                if (r12 != null) {
                    r12.previous = r4.previous;
                }
                AbstractC29071Qg r02 = r4.previous;
                if (r02 != null) {
                    r02.next = r12;
                }
            } catch (InterruptedException unused) {
            }
        }
    }
}
