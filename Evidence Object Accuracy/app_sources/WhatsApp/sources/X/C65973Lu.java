package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.redex.IDxCreatorShape1S0000000_2_I1;

/* renamed from: X.3Lu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C65973Lu implements Parcelable {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape1S0000000_2_I1(47);
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final String A04;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C65973Lu(Parcel parcel) {
        this.A03 = parcel.readInt();
        this.A04 = C12990iw.A0p(parcel);
        this.A01 = parcel.readInt();
        this.A00 = parcel.readInt();
        this.A02 = parcel.readInt();
    }

    public /* synthetic */ C65973Lu(String str, int i, int i2, int i3, int i4) {
        this.A03 = i;
        this.A04 = str;
        this.A01 = i2;
        this.A00 = i3;
        this.A02 = i4;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.A03);
        parcel.writeString(this.A04);
        parcel.writeInt(this.A01);
        parcel.writeInt(this.A00);
        parcel.writeInt(this.A02);
    }
}
