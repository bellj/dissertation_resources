package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaButton;

/* renamed from: X.5lg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122575lg extends AbstractC118835cS {
    public final WaButton A00;

    public C122575lg(View view) {
        super(view);
        this.A00 = (WaButton) AnonymousClass028.A0D(view, R.id.send_again_btn);
    }

    @Override // X.AbstractC118835cS
    public void A08(AbstractC125975s7 r3, int i) {
        this.A00.setOnClickListener(((C123095mb) r3).A00);
    }
}
