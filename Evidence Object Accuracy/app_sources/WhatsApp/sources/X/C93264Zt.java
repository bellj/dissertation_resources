package X;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

/* renamed from: X.4Zt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C93264Zt {
    public static final C93264Zt A00 = new C93264Zt();
    public static final AtomicReferenceFieldUpdater A01 = AtomicReferenceFieldUpdater.newUpdater(C93264Zt.class, AnonymousClass5RD.class, "cache");
    public volatile AnonymousClass5RD cache;
}
