package X;

import java.util.Arrays;

/* renamed from: X.0rg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17960rg extends AbstractC17970rh {
    public int hashCode;
    public Object[] hashTable;

    public C17960rg() {
        super(4);
    }

    public C17960rg(int i) {
        super(i);
        this.hashTable = new Object[AbstractC17940re.chooseTableSize(i)];
    }

    @Override // X.AbstractC17970rh, X.AbstractC17980ri
    public C17960rg add(Object obj) {
        if (this.hashTable == null || AbstractC17940re.chooseTableSize(this.size) > this.hashTable.length) {
            this.hashTable = null;
            super.add(obj);
            return this;
        }
        addDeduping(obj);
        return this;
    }

    @Override // X.AbstractC17970rh
    public C17960rg add(Object... objArr) {
        if (this.hashTable != null) {
            for (Object obj : objArr) {
                add(obj);
            }
        } else {
            super.add(objArr);
        }
        return this;
    }

    @Override // X.AbstractC17970rh, X.AbstractC17980ri
    public C17960rg addAll(Iterable iterable) {
        if (this.hashTable != null) {
            for (Object obj : iterable) {
                add(obj);
            }
        } else {
            super.addAll(iterable);
        }
        return this;
    }

    private void addDeduping(Object obj) {
        int length = this.hashTable.length - 1;
        int hashCode = obj.hashCode();
        int smear = C28301Mo.smear(hashCode);
        while (true) {
            int i = smear & length;
            Object[] objArr = this.hashTable;
            Object obj2 = objArr[i];
            if (obj2 == null) {
                objArr[i] = obj;
                this.hashCode += hashCode;
                super.add(obj);
                return;
            } else if (!obj2.equals(obj)) {
                smear = i + 1;
            } else {
                return;
            }
        }
    }

    public AbstractC17940re build() {
        AbstractC17940re r3;
        int i = this.size;
        if (i == 0) {
            return AbstractC17940re.of();
        }
        if (i == 1) {
            return AbstractC17940re.of(this.contents[0]);
        }
        if (this.hashTable == null || AbstractC17940re.chooseTableSize(i) != this.hashTable.length) {
            r3 = AbstractC17940re.construct(this.size, this.contents);
            this.size = r3.size();
        } else {
            int i2 = this.size;
            Object[] objArr = this.contents;
            if (AbstractC17940re.shouldTrim(i2, objArr.length)) {
                objArr = Arrays.copyOf(objArr, i2);
            }
            int i3 = this.hashCode;
            Object[] objArr2 = this.hashTable;
            r3 = new C28351Mv(objArr, i3, objArr2, objArr2.length - 1, this.size);
        }
        this.forceCopy = true;
        this.hashTable = null;
        return r3;
    }
}
