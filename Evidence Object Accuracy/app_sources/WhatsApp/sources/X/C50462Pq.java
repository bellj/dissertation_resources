package X;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import java.util.Locale;

/* renamed from: X.2Pq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C50462Pq implements AbstractC50412Pl {
    public final AbstractC15710nm A00;
    public final AnonymousClass18U A01;
    public final C253619c A02;
    public final C14850m9 A03;

    public C50462Pq(AbstractC15710nm r1, AnonymousClass18U r2, C253619c r3, C14850m9 r4) {
        this.A03 = r4;
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
    }

    @Override // X.AbstractC50412Pl
    public void AZA(Context context, AbstractC15340mz r8, C16470p4 r9, int i) {
        AbstractC15710nm r1;
        String str;
        String str2;
        AnonymousClass1ZE r0 = r9.A05;
        if (r0 == null || (str2 = r0.A02) == null) {
            r1 = this.A00;
            str = "missing content/shopId";
        } else {
            String A03 = this.A03.A03(1054);
            if (TextUtils.isEmpty(A03)) {
                r1 = this.A00;
                str = "missing url format";
            } else {
                this.A01.Ab9(context, Uri.parse(String.format(Locale.US, A03, str2)));
                C615130q r12 = new C615130q();
                r12.A01 = 6;
                r12.A02 = 5;
                r12.A03 = 33;
                this.A02.A02.A07(r12);
                return;
            }
        }
        r1.AaV("OpenShopAction/perform", str, false);
    }
}
