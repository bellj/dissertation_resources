package X;

import android.database.Cursor;
import android.text.TextUtils;
import com.whatsapp.jid.UserJid;

/* renamed from: X.1Xa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30341Xa extends AnonymousClass1XP implements AbstractC16400ox, AbstractC16420oz {
    public int A00;
    public long A01;
    public C30751Yr A02;
    public String A03;

    public C30341Xa(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 16, j);
    }

    public C30341Xa(AnonymousClass1IS r10, C30341Xa r11, long j) {
        super(r10, r11, r11.A0y, j, true);
        this.A00 = r11.A00;
        this.A01 = r11.A01;
        this.A03 = r11.A03;
        this.A02 = r11.A02;
    }

    @Override // X.AnonymousClass1XP
    public void A15(Cursor cursor, C15570nT r11) {
        UserJid A0C;
        super.A15(cursor, r11);
        this.A00 = cursor.getInt(cursor.getColumnIndexOrThrow("live_location_share_duration"));
        this.A01 = cursor.getLong(cursor.getColumnIndexOrThrow("live_location_sequence_number"));
        double d = cursor.getDouble(cursor.getColumnIndexOrThrow("live_location_final_latitude"));
        double d2 = cursor.getDouble(cursor.getColumnIndexOrThrow("live_location_final_longitude"));
        long j = cursor.getLong(cursor.getColumnIndexOrThrow("live_location_final_timestamp"));
        if (d != 0.0d || d2 != 0.0d || j != 0) {
            if (this.A0z.A02) {
                r11.A08();
                A0C = r11.A05;
            } else {
                A0C = A0C();
            }
            AnonymousClass009.A05(A0C);
            C30751Yr r0 = new C30751Yr(A0C);
            r0.A00 = d;
            r0.A01 = d2;
            r0.A05 = j;
            this.A02 = r0;
        }
    }

    @Override // X.AbstractC16420oz
    public void A6k(C39971qq r13) {
        C16460p3 A0G = A0G();
        AnonymousClass1G3 r5 = r13.A03;
        C35771ii r0 = ((C27081Fy) r5.A00).A0P;
        if (r0 == null) {
            r0 = C35771ii.A0B;
        }
        AnonymousClass1G4 A0T = r0.A0T();
        double d = ((AnonymousClass1XP) this).A00;
        A0T.A03();
        C35771ii r3 = (C35771ii) A0T.A00;
        r3.A04 |= 1;
        r3.A00 = d;
        double d2 = ((AnonymousClass1XP) this).A01;
        A0T.A03();
        C35771ii r32 = (C35771ii) A0T.A00;
        r32.A04 |= 2;
        r32.A01 = d2;
        boolean z = r13.A06;
        if (!z && A0G.A07() != null) {
            byte[] A07 = A0G.A07();
            AbstractC27881Jp A01 = AbstractC27881Jp.A01(A07, 0, A07.length);
            A0T.A03();
            C35771ii r1 = (C35771ii) A0T.A00;
            r1.A04 |= 256;
            r1.A08 = A01;
        }
        AnonymousClass1PG r8 = r13.A04;
        byte[] bArr = r13.A09;
        if (C32411c7.A0U(r8, this, bArr)) {
            C43261wh A0P = C32411c7.A0P(r13.A00, r13.A02, r8, this, bArr, z);
            A0T.A03();
            C35771ii r12 = (C35771ii) A0T.A00;
            r12.A09 = A0P;
            r12.A04 |= 512;
        }
        if (!TextUtils.isEmpty(this.A03)) {
            String str = this.A03;
            A0T.A03();
            C35771ii r14 = (C35771ii) A0T.A00;
            r14.A04 |= 32;
            r14.A0A = str;
        }
        long j = this.A01;
        A0T.A03();
        C35771ii r15 = (C35771ii) A0T.A00;
        r15.A04 |= 64;
        r15.A07 = j;
        r5.A03();
        C27081Fy r2 = (C27081Fy) r5.A00;
        r2.A0P = (C35771ii) A0T.A02();
        r2.A00 |= 65536;
    }

    @Override // X.AbstractC16400ox
    public AbstractC15340mz A7M(AnonymousClass1IS r4) {
        return new C30341Xa(r4, this, this.A0I);
    }
}
