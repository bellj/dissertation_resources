package X;

/* renamed from: X.1ZS  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1ZS {
    public static AnonymousClass1ZR A00(Object obj, String str) {
        return new AnonymousClass1ZR(new AnonymousClass2SM(), String.class, obj, str);
    }

    public static Object A01(AnonymousClass1ZR r0) {
        AnonymousClass009.A05(r0);
        Object obj = r0.A00;
        AnonymousClass009.A05(obj);
        return obj;
    }

    public static boolean A02(AnonymousClass1ZR r1) {
        return r1 == null || r1.A00();
    }

    public static boolean A03(AnonymousClass1ZR r1) {
        if (r1 == null || r1.A00 == null) {
            return true;
        }
        return false;
    }
}
