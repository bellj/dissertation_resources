package X;

import android.view.View;
import android.widget.LinearLayout;
import com.whatsapp.R;

/* renamed from: X.5lM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122375lM extends AbstractC118825cR {
    public final LinearLayout A00;
    public final AnonymousClass018 A01;

    public C122375lM(View view, AnonymousClass018 r3) {
        super(view);
        this.A01 = r3;
        this.A00 = C117305Zk.A07(view, R.id.payment_methods_list_container);
    }
}
