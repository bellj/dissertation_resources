package X;

import android.os.IBinder;

/* renamed from: X.3pj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78903pj extends C98414ie implements AnonymousClass5Y7 {
    public C78903pj(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.auth.api.accounttransfer.internal.IAccountTransferService");
    }
}
