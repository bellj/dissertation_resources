package X;

/* renamed from: X.1Es  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26761Es {
    public static final AnonymousClass00E A0C = new AnonymousClass00E(60, 60);
    public static final AnonymousClass00E A0D = new AnonymousClass00E(20, 20);
    public final AbstractC15710nm A00;
    public final C14900mE A01;
    public final C15570nT A02;
    public final C15450nH A03;
    public final C16590pI A04;
    public final C15650ng A05;
    public final AnonymousClass104 A06;
    public final C22390z0 A07;
    public final C244515o A08;
    public final C22900zp A09;
    public final AnonymousClass1BD A0A;
    public final AnonymousClass1CY A0B;

    public C26761Es(AbstractC15710nm r1, C14900mE r2, C15570nT r3, C15450nH r4, C16590pI r5, C15650ng r6, AnonymousClass104 r7, C22390z0 r8, C244515o r9, C22900zp r10, AnonymousClass1BD r11, AnonymousClass1CY r12) {
        this.A04 = r5;
        this.A0B = r12;
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
        this.A03 = r4;
        this.A05 = r6;
        this.A09 = r10;
        this.A08 = r9;
        this.A0A = r11;
        this.A07 = r8;
        this.A06 = r7;
    }
}
