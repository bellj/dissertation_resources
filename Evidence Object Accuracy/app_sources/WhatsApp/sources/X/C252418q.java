package X;

import com.whatsapp.Mp4Ops;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;

/* renamed from: X.18q  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C252418q extends AbstractC14550lc {
    public final C21040wk A00;
    public final AbstractC15710nm A01;
    public final C14330lG A02;
    public final C14900mE A03;
    public final C20870wS A04;
    public final Mp4Ops A05;
    public final C15450nH A06;
    public final C18790t3 A07;
    public final C14830m7 A08;
    public final C16590pI A09;
    public final C14950mJ A0A;
    public final C15650ng A0B;
    public final C15660nh A0C;
    public final AnonymousClass12H A0D;
    public final AnonymousClass1D2 A0E;
    public final C14850m9 A0F;
    public final C20110vE A0G;
    public final C19940uv A0H;
    public final C14410lO A0I;
    public final AnonymousClass14P A0J;
    public final AnonymousClass155 A0K;
    public final C17040qA A0L;
    public final C14420lP A0M;
    public final AnonymousClass12J A0N;
    public final C239713s A0O;
    public final C16630pM A0P;
    public final AnonymousClass160 A0Q;
    public final C22600zL A0R;
    public final C15860o1 A0S;
    public final C22590zK A0T;
    public final C26511Dt A0U;
    public final AnonymousClass19O A0V;
    public final C26481Dq A0W;
    public final C26521Du A0X;
    public final AbstractC14440lR A0Y;
    public final C26501Ds A0Z;
    public final C21710xr A0a;
    public final Executor A0b;

    public C252418q(C21040wk r5, AbstractC15710nm r6, C14330lG r7, C14900mE r8, C20870wS r9, Mp4Ops mp4Ops, C15450nH r11, C18790t3 r12, C14830m7 r13, C16590pI r14, C14950mJ r15, C15650ng r16, C15660nh r17, AnonymousClass12H r18, AnonymousClass1D2 r19, C14850m9 r20, C20110vE r21, C19940uv r22, C14410lO r23, AnonymousClass14P r24, AnonymousClass155 r25, C17040qA r26, C14420lP r27, AnonymousClass12J r28, C239713s r29, C16630pM r30, AnonymousClass160 r31, C22600zL r32, C15860o1 r33, C22590zK r34, C26511Dt r35, AnonymousClass19O r36, C26481Dq r37, C26521Du r38, AbstractC14440lR r39, C26501Ds r40, C21710xr r41) {
        super(new C002601e(null, new AnonymousClass01N() { // from class: X.22Q
            @Override // X.AnonymousClass01N, X.AnonymousClass01H
            public final Object get() {
                ThreadPoolExecutor A8a = AbstractC14440lR.this.A8a("ThumbnailDownloadQueue", new LinkedBlockingQueue(), 1, 2, 10, 5);
                A8a.allowCoreThreadTimeOut(true);
                return A8a;
            }
        }));
        this.A08 = r13;
        this.A05 = mp4Ops;
        this.A0F = r20;
        this.A03 = r8;
        this.A01 = r6;
        this.A09 = r14;
        this.A0Y = r39;
        this.A02 = r7;
        this.A0J = r24;
        this.A07 = r12;
        this.A00 = r5;
        this.A06 = r11;
        this.A0Z = r40;
        this.A0I = r23;
        this.A0A = r15;
        this.A0R = r32;
        this.A0U = r35;
        this.A04 = r9;
        this.A0O = r29;
        this.A0X = r38;
        this.A0T = r34;
        this.A0a = r41;
        this.A0K = r25;
        this.A0B = r16;
        this.A0D = r18;
        this.A0S = r33;
        this.A0M = r27;
        this.A0C = r17;
        this.A0V = r36;
        this.A0N = r28;
        this.A0L = r26;
        this.A0W = r37;
        this.A0H = r22;
        this.A0G = r21;
        this.A0P = r30;
        this.A0Q = r31;
        this.A0E = r19;
        this.A0b = new ExecutorC41461tZ(r39);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0030, code lost:
        if (r5 != null) goto L_0x0032;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0032, code lost:
        r0 = ((X.AbstractC16130oV) r94).A02;
        X.AnonymousClass009.A05(r0);
        r0.A0I = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x003c, code lost:
        r3 = r93.A02;
        r4 = r94.A0y;
        r12 = r1.A05;
        r6 = X.C14370lK.A00(r4);
        r11 = r1.A07;
        r60 = r3.A0C(r6, r12, r11, null, false, false);
        r59 = r3.A0D(r6, r11, null, false, false, false, true);
        r7 = r1.A04;
        r8 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x006e, code lost:
        if (r6 != X.C14370lK.A0H) goto L_0x0071;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0070, code lost:
        r8 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0071, code lost:
        r38 = new X.C41471ta(null, r6, null, r59, r60, null, r11, null, r5, r12, null, r7, null, null, null, r1.A09, 0, 0, 2, X.C33761f2.A00(r4, 0, r8), 1, 0, 0, 0, r82, false, false, false, false, false, false, false, false, true);
        r3 = r93.A08;
        r3 = r93.A05;
        r3 = r93.A0F;
        r3 = r93.A03;
        r3 = r93.A09;
        r3 = r93.A01;
        r3 = r93.A0Y;
        r3 = r93.A07;
        r3 = r93.A00;
        r3 = r93.A06;
        r3 = r93.A0Z;
        r3 = r93.A0I;
        r3 = r93.A0A;
        r3 = r93.A0R;
        r3 = r93.A0U;
        r15 = r93.A04;
        r3 = r93.A0O;
        r3 = r93.A0X;
        r3 = r93.A0T;
        r3 = r93.A0a;
        r14 = r93.A0K;
        r13 = r93.A0S;
        r12 = r93.A0C;
        r11 = r93.A0M;
        r8 = r93.A0N;
        r7 = r93.A0L;
        r6 = r93.A0W;
        r5 = r93.A0H;
        r4 = r93.A0G;
        r3 = r93.A0P;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x011e, code lost:
        if (r82 == false) goto L_0x0171;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0120, code lost:
        r51 = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0122, code lost:
        r9 = new X.C28921Pn(null, r3, r3, r3, r3, r15, r3, r3, r3, r3, r3, r3, r12, r3, r4, r5, r3, r14, r7, r11, r8, r38, r3, r3, r3, r13, r3, r3, r6, r3, r3, r3, r3, r95, r51, 1, false);
        r9.A0H.A03(new X.AnonymousClass22R(r93, r94, r1), r93.A0b);
        A01(r94, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0170, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0175, code lost:
        if (X.C15380n4.A0J(r9) == false) goto L_0x017a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0177, code lost:
        r51 = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x017a, code lost:
        if (r9 == null) goto L_0x0187;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0182, code lost:
        if (r9.getType() != 9) goto L_0x0187;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0184, code lost:
        r51 = 6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0187, code lost:
        r51 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0192, code lost:
        if (r3 != false) goto L_0x0032;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A06(X.AbstractC15340mz r94, int r95) {
        /*
        // Method dump skipped, instructions count: 406
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C252418q.A06(X.0mz, int):void");
    }
}
