package X;

import java.util.Random;

/* renamed from: X.1DA  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1DA {
    public final AbstractC15710nm A00;
    public final C14330lG A01;
    public final C002701f A02;
    public final C15650ng A03;
    public final C15660nh A04;
    public final C16490p7 A05;
    public final C14850m9 A06;
    public final C16120oU A07;
    public final AbstractC14440lR A08;
    public final Random A09 = new Random();

    public AnonymousClass1DA(AbstractC15710nm r2, C14330lG r3, C002701f r4, C15650ng r5, C15660nh r6, C16490p7 r7, C14850m9 r8, C16120oU r9, AbstractC14440lR r10) {
        this.A06 = r8;
        this.A00 = r2;
        this.A08 = r10;
        this.A01 = r3;
        this.A07 = r9;
        this.A03 = r5;
        this.A04 = r6;
        this.A05 = r7;
        this.A02 = r4;
    }
}
