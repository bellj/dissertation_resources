package X;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.conversation.conversationrow.ConversationRowVideo$RowVideoView;

/* renamed from: X.3as  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70183as implements AbstractC41521tf {
    public final /* synthetic */ C47412Ap A00;

    public C70183as(C47412Ap r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC41521tf
    public int AGm() {
        return AnonymousClass3GD.A02(this.A00);
    }

    @Override // X.AbstractC41521tf
    public void AQV() {
        this.A00.A1O();
    }

    @Override // X.AbstractC41521tf
    public void Adg(Bitmap bitmap, View view, AbstractC15340mz r8) {
        C47412Ap r4 = this.A00;
        ConversationRowVideo$RowVideoView conversationRowVideo$RowVideoView = r4.A0C;
        if (bitmap != null) {
            conversationRowVideo$RowVideoView.setImageDrawable(new BitmapDrawable(C12960it.A09(r4), bitmap));
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            if (r4.A00 <= 0 || r4.A01 <= 0) {
                r4.A00 = height;
                r4.A01 = width;
            }
            conversationRowVideo$RowVideoView.A02(width, height, false);
            return;
        }
        conversationRowVideo$RowVideoView.setImageDrawable(C12980iv.A0L(r4.getContext(), R.color.dark_gray));
    }

    @Override // X.AbstractC41521tf
    public void Adu(View view) {
        C12990iw.A1D(this.A00.A0C);
    }
}
