package X;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.group.GroupAdminPickerActivity;

/* renamed from: X.2hd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C55002hd extends AnonymousClass03U {
    public final ImageView A00;
    public final TextView A01;
    public final TextEmojiLabel A02;
    public final TextEmojiLabel A03;
    public final C28801Pb A04;
    public final /* synthetic */ GroupAdminPickerActivity A05;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C55002hd(View view, GroupAdminPickerActivity groupAdminPickerActivity) {
        super(view);
        this.A05 = groupAdminPickerActivity;
        C28801Pb r3 = new C28801Pb(view, groupAdminPickerActivity.A0A, groupAdminPickerActivity.A0M, (int) R.id.name);
        this.A04 = r3;
        TextEmojiLabel A0U = C12970iu.A0U(view, R.id.status);
        this.A03 = A0U;
        ImageView A0L = C12970iu.A0L(view, R.id.avatar);
        this.A00 = A0L;
        this.A01 = C12960it.A0J(view, R.id.owner);
        this.A02 = C12970iu.A0U(view, R.id.push_name);
        AnonymousClass028.A0a(A0L, 2);
        C28801Pb.A00(view.getContext(), r3, R.color.list_item_title);
        C12960it.A0s(view.getContext(), A0U, R.color.list_item_sub_title);
        view.setBackgroundResource(R.drawable.selector_orange_gradient);
        view.setOnClickListener(groupAdminPickerActivity.A0S);
    }
}
