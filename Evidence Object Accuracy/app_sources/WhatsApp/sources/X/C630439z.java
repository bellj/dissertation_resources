package X;

import com.google.android.gms.common.api.Status;

/* renamed from: X.39z  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C630439z extends Exception {
    @Deprecated
    public final Status mStatus;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C630439z(com.google.android.gms.common.api.Status r5) {
        /*
            r4 = this;
            int r3 = r5.A01
            java.lang.String r2 = r5.A04
            if (r2 != 0) goto L_0x0008
            java.lang.String r2 = ""
        L_0x0008:
            int r0 = X.C12970iu.A07(r2)
            int r0 = r0 + 13
            java.lang.StringBuilder r1 = X.C12980iv.A0t(r0)
            r1.append(r3)
            java.lang.String r0 = ": "
            r1.append(r0)
            java.lang.String r0 = X.C12960it.A0d(r2, r1)
            r4.<init>(r0)
            r4.mStatus = r5
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C630439z.<init>(com.google.android.gms.common.api.Status):void");
    }
}
