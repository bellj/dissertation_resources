package X;

import com.whatsapp.util.Log;

/* renamed from: X.1X2  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1X2 extends AnonymousClass1X3 implements AbstractC16400ox, AbstractC16420oz {
    public AnonymousClass1X2(C16150oX r9, AnonymousClass1IS r10, AnonymousClass1X2 r11, long j) {
        super(r9, r10, (AbstractC16130oV) r11, (byte) 3, j, false);
    }

    public AnonymousClass1X2(C16150oX r9, AnonymousClass1IS r10, AnonymousClass1X2 r11, long j, boolean z) {
        super(r9, r10, r11, r11.A0y, j, z);
    }

    public AnonymousClass1X2(AnonymousClass1IS r1, byte b, long j) {
        super(r1, b, j);
    }

    public AnonymousClass1X2(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 3, j);
    }

    public AnonymousClass1X2(C40851sR r2, AnonymousClass1IS r3, byte b, long j, boolean z, boolean z2) {
        super(r2, r3, (byte) 62, j, z, z2);
        A1D(r2);
    }

    public AnonymousClass1X2(C40851sR r9, AnonymousClass1IS r10, long j, boolean z, boolean z2) {
        super(r9, r10, (byte) 3, j, z, z2);
        A1D(r9);
    }

    @Override // X.AbstractC16420oz
    public void A6k(C39971qq r16) {
        AbstractC27091Fz r0;
        AbstractC27091Fz r02;
        StringBuilder sb;
        String str;
        if (this instanceof AnonymousClass1X1) {
            AnonymousClass1X1 r7 = (AnonymousClass1X1) this;
            if (r7.ACL() != null) {
                r7.ACL().A0A(r7, r16);
                AnonymousClass1G3 r4 = r16.A03;
                C57752nZ r03 = ((C27081Fy) r4.A00).A0K;
                if (r03 == null) {
                    r03 = C57752nZ.A07;
                }
                AnonymousClass1G4 A0T = r03.A0T();
                C57722nW r04 = ((C57752nZ) A0T.A00).A05;
                if (r04 == null) {
                    r04 = C57722nW.A06;
                }
                C82513vk r3 = (C82513vk) r04.A0T();
                r3.A05();
                C57722nW r1 = (C57722nW) r3.A00;
                if (r1.A01 == 7) {
                    r0 = (AbstractC27091Fz) r1.A02;
                } else {
                    r0 = C40851sR.A0O;
                }
                C82373vW r05 = (C82373vW) r0.A0T();
                r7.A1C(r05, r16);
                r3.A03();
                C57722nW r12 = (C57722nW) r3.A00;
                r12.A02 = r05.A02();
                r12.A01 = 7;
                A0T.A03();
                C57752nZ r13 = (C57752nZ) A0T.A00;
                r13.A05 = (C57722nW) r3.A02();
                r13.A00 |= 1;
                r4.A07((C57752nZ) A0T.A02());
            }
        } else if (!(this instanceof AnonymousClass1XS)) {
            AnonymousClass1G3 r32 = r16.A03;
            C40851sR r06 = ((C27081Fy) r32.A00).A0f;
            if (r06 == null) {
                r06 = C40851sR.A0O;
            }
            C82373vW r42 = (C82373vW) r06.A0T();
            A1C(r42, r16);
            if (!A0y() || A0F().A00 == null) {
                r32.A03();
                C27081Fy r14 = (C27081Fy) r32.A00;
                r14.A0f = (C40851sR) r42.A02();
                r14.A00 |= 256;
                return;
            }
            C57552nF r07 = ((C27081Fy) r32.A00).A03;
            if (r07 == null) {
                r07 = C57552nF.A08;
            }
            C56852m3 r2 = (C56852m3) r07.A0T();
            C35011h5.A03(r2, A0F().A00);
            r2.A03();
            C57552nF r15 = (C57552nF) r2.A00;
            r15.A05 = r42.A02();
            r15.A01 = 4;
            r2.A05(AnonymousClass39x.A07);
            r32.A06((C57552nF) r2.A02());
        } else {
            AnonymousClass1XS r72 = (AnonymousClass1XS) this;
            AnonymousClass1G3 r43 = r16.A03;
            AnonymousClass2Lw r33 = (AnonymousClass2Lw) r43.A05().A0T();
            C14850m9 r9 = r16.A02;
            C15570nT r8 = r16.A00;
            AnonymousClass2Ly r22 = r43.A05().A03;
            if (r22 == null) {
                r22 = AnonymousClass2Ly.A07;
            }
            if (r22.A01 == 4) {
                r02 = (AbstractC27091Fz) r22.A03;
            } else {
                r02 = C40851sR.A0O;
            }
            boolean z = r16.A08;
            C82373vW A1B = r72.A1B(r8, r9, (C82373vW) r02.A0T(), r16.A04, r16.A09, z, r16.A06);
            C16150oX r08 = ((AbstractC16130oV) r72).A02;
            if (r08 == null || ((!z && r08.A0U == null) || A1B == null)) {
                sb = new StringBuilder();
                str = "FMessageTemplateVideo/unable to send encrypted media message due to missing; media_wa_type=";
            } else {
                C38101nW A14 = r72.A14();
                AnonymousClass009.A05(A14);
                byte[] A05 = A14.A05();
                if (A05 != null) {
                    AbstractC27881Jp A01 = AbstractC27881Jp.A01(A05, 0, A05.length);
                    A1B.A03();
                    C40851sR r23 = (C40851sR) A1B.A00;
                    r23.A00 |= 32768;
                    r23.A0B = A01;
                }
                C28891Pk r09 = r72.A00;
                if (r09 != null) {
                    AnonymousClass2Lx A00 = C63173Ao.A00(r43, r09);
                    A00.A03();
                    AnonymousClass2Ly r17 = (AnonymousClass2Ly) A00.A00;
                    r17.A03 = A1B.A02();
                    r17.A01 = 4;
                    r33.A06(A00);
                    r33.A05(A00);
                    r43.A0B(r33);
                    return;
                }
                sb = new StringBuilder();
                str = "MessageTemplateVideo: cannot send encrypted media message, ";
            }
            sb.append(str);
            sb.append((int) r72.A0y);
            Log.w(sb.toString());
        }
    }

    @Override // X.AbstractC16400ox
    public /* bridge */ /* synthetic */ AbstractC15340mz A7M(AnonymousClass1IS r8) {
        if (this instanceof AnonymousClass1X1) {
            AnonymousClass1X1 r3 = (AnonymousClass1X1) this;
            return new AnonymousClass1X1(((AbstractC16130oV) r3).A02, r8, r3, r3.A0I, true);
        } else if (!(this instanceof AnonymousClass1XS)) {
            return new AnonymousClass1X2(((AbstractC16130oV) this).A02, r8, this, this.A0I, true);
        } else {
            AnonymousClass1XS r32 = (AnonymousClass1XS) this;
            return new AnonymousClass1XS(((AbstractC16130oV) r32).A02, r8, r32, r32.A0I);
        }
    }
}
