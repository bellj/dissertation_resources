package X;

import android.content.Context;
import com.whatsapp.R;
import java.math.BigDecimal;
import java.math.RoundingMode;

/* renamed from: X.6BA  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6BA implements AbstractC136446Mo {
    public final AbstractC30791Yv A00;
    public final AbstractC30791Yv A01;
    public final AnonymousClass63Y A02;
    public final AnonymousClass63Y A03;

    public AnonymousClass6BA(AnonymousClass63Y r2, AnonymousClass63Y r3) {
        this.A03 = r2;
        this.A02 = r3;
        this.A01 = r2.A00;
        this.A00 = r3.A00;
    }

    @Override // X.AbstractC136446Mo
    public AnonymousClass6F2 A9k(AnonymousClass6F2 r7, C1315863i r8, int i) {
        int i2;
        BigDecimal bigDecimal;
        AnonymousClass63Y r0;
        BigDecimal bigDecimal2;
        AbstractC30791Yv r02;
        AnonymousClass6F2 A00;
        C127695uu r2;
        if (C117305Zk.A1V(this.A00, ((AbstractC30781Yu) r7.A00).A04)) {
            if (i == 1) {
                i = 2;
            } else if (i == 0) {
                i = 5;
            }
            i2 = 0;
            if (i == 4) {
                A00 = C127695uu.A00(this.A02.A02, r7, r8.A02.A05, false);
                r2 = new C127695uu(this.A03.A02, r8.A05.A05, 0, true);
            } else if (i == 2) {
                AnonymousClass6F2 A002 = C127695uu.A00(this.A02.A02, r7, r8.A02.A05, false);
                BigDecimal bigDecimal3 = r8.A05.A05;
                AnonymousClass63Y r22 = this.A03;
                A00 = C127695uu.A00(r22.A02, A002, bigDecimal3, false);
                r2 = new C127695uu(r22.A01, r8.A03.A05, 1, true);
            } else if (i != 5) {
                return r7;
            } else {
                bigDecimal = r8.A02.A05;
                r0 = this.A02;
            }
            return A00.A05(r2);
        }
        if (i == 1) {
            i = 3;
        } else if (i == 0) {
            i = 5;
        }
        i2 = 0;
        if (i == 4) {
            bigDecimal = r8.A03.A05;
            r0 = this.A03;
        } else if (i == 2) {
            return r7;
        } else {
            AnonymousClass6F2 A003 = C127695uu.A00(this.A03.A02, r7, r8.A03.A05, false);
            C1316563p r03 = r8.A05;
            if (i == 5) {
                bigDecimal2 = r03.A05;
                r02 = this.A02.A02;
            } else {
                BigDecimal bigDecimal4 = r03.A05;
                AnonymousClass63Y r1 = this.A02;
                A003 = C127695uu.A01(A003, r1, bigDecimal4, 1, false);
                bigDecimal2 = r8.A02.A05;
                r02 = r1.A01;
            }
            return C127695uu.A00(r02, A003, bigDecimal2, true);
        }
        return C127695uu.A01(r7, r0, bigDecimal, i2, true);
    }

    @Override // X.AbstractC136446Mo
    public CharSequence AHI(Context context, AnonymousClass018 r9, C1315863i r10) {
        if (C117305Zk.A1V(this.A00, ((AbstractC30781Yu) this.A01).A04)) {
            return C130275z5.A00(context, r9, this.A03, r10);
        }
        BigDecimal bigDecimal = BigDecimal.ONE;
        int i = 4;
        BigDecimal A01 = C130275z5.A01(C130275z5.A01(bigDecimal.divide(r10.A03.A05, 4, RoundingMode.HALF_EVEN), r10.A05.A05), r10.A02.A05);
        Object[] objArr = new Object[2];
        objArr[0] = this.A03.A00.AAB(r9, bigDecimal, 2);
        AbstractC30791Yv r1 = this.A02.A00;
        if (BigDecimal.ONE.equals(A01)) {
            i = 0;
        }
        return C12960it.A0X(context, C117305Zk.A0k(r9, r1, A01, i), objArr, 1, R.string.novi_send_money_review_transaction_exchange_rate);
    }

    @Override // X.AbstractC136446Mo
    public boolean AKE(C1315863i r3) {
        return ((AbstractC30781Yu) this.A01).A04.equals(r3.A03.A04) && ((AbstractC30781Yu) this.A00).A04.equals(r3.A02.A04);
    }
}
