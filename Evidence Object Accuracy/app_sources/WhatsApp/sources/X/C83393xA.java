package X;

import android.view.animation.Animation;
import com.whatsapp.identity.IdentityVerificationActivity;

/* renamed from: X.3xA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83393xA extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ IdentityVerificationActivity A00;
    public final /* synthetic */ Runnable A01;

    public C83393xA(IdentityVerificationActivity identityVerificationActivity, Runnable runnable) {
        this.A00 = identityVerificationActivity;
        this.A01 = runnable;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        this.A00.A0Q.setVisibility(8);
        Runnable runnable = this.A01;
        if (runnable != null) {
            runnable.run();
        }
    }
}
