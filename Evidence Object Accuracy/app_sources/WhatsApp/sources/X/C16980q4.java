package X;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.text.TextUtils;
import com.facebook.redex.RunnableBRunnable0Shape12S0100000_I0_12;
import com.facebook.redex.RunnableBRunnable0Shape1S0300000_I0_1;
import com.facebook.redex.RunnableBRunnable0Shape7S0100000_I0_7;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.0q4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16980q4 implements AbstractC16990q5 {
    public C17030q9 A00;
    public C17010q7 A01;
    public C16970q3 A02;
    public C14830m7 A03;
    public C16590pI A04;
    public C17040qA A05;
    public C14420lP A06;
    public C17080qE A07;
    public C17090qF A08;
    public C17100qG A09;
    public C17110qH A0A;
    public AbstractC14440lR A0B;
    public C17020q8 A0C;
    public C17140qK A0D;
    public C17150qL A0E;
    public C17060qC A0F;

    @Override // X.AbstractC16990q5
    public void AOo() {
        SharedPreferences sharedPreferences;
        File[] listFiles;
        C16310on A02;
        long j;
        long j2;
        long j3;
        long j4;
        Double valueOf;
        Double valueOf2;
        Long valueOf3;
        Long valueOf4;
        Long valueOf5;
        Long valueOf6;
        Long valueOf7;
        Long valueOf8;
        C41441tX A00;
        C17040qA r7 = this.A05;
        long A01 = r7.A01();
        C26491Dr r3 = r7.A03;
        ArrayList arrayList = new ArrayList();
        SharedPreferences sharedPreferences2 = r3.A00;
        if (sharedPreferences2 == null) {
            sharedPreferences2 = r3.A01.A01("media_daily_usage_preferences_v1");
            r3.A00 = sharedPreferences2;
        }
        Map<String, ?> all = sharedPreferences2.getAll();
        SharedPreferences sharedPreferences3 = r3.A00;
        if (sharedPreferences3 == null) {
            sharedPreferences3 = r3.A01.A01("media_daily_usage_preferences_v1");
            r3.A00 = sharedPreferences3;
        }
        SharedPreferences.Editor edit = sharedPreferences3.edit();
        for (Map.Entry<String, ?> entry : all.entrySet()) {
            String obj = entry.getValue().toString();
            if (!obj.isEmpty() && (A00 = C41441tX.A00(obj)) != null && A00.A0C < A01) {
                arrayList.add(A00);
                edit.remove(entry.getKey());
            }
        }
        edit.apply();
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            C41441tX r6 = (C41441tX) it.next();
            C16120oU r9 = r7.A02;
            AnonymousClass31N r5 = new AnonymousClass31N();
            long j5 = r6.A01;
            Long l = null;
            if (j5 == 0) {
                valueOf = null;
            } else {
                valueOf = Double.valueOf((double) j5);
            }
            r5.A02 = valueOf;
            long j6 = r6.A00;
            if (j6 == 0) {
                valueOf2 = null;
            } else {
                valueOf2 = Double.valueOf((double) j6);
            }
            r5.A01 = valueOf2;
            long j7 = r6.A05;
            if (j7 == 0) {
                valueOf3 = null;
            } else {
                valueOf3 = Long.valueOf(j7);
            }
            r5.A09 = valueOf3;
            long j8 = r6.A04;
            if (j8 == 0) {
                valueOf4 = null;
            } else {
                valueOf4 = Long.valueOf(j8);
            }
            r5.A08 = valueOf4;
            long j9 = r6.A07;
            if (j9 == 0) {
                valueOf5 = null;
            } else {
                valueOf5 = Long.valueOf(j9);
            }
            r5.A0B = valueOf5;
            long j10 = r6.A02;
            if (j10 == 0) {
                valueOf6 = null;
            } else {
                valueOf6 = Long.valueOf(j10);
            }
            r5.A06 = valueOf6;
            long j11 = r6.A03;
            if (j11 == 0) {
                valueOf7 = null;
            } else {
                valueOf7 = Long.valueOf(j11);
            }
            r5.A07 = valueOf7;
            long j12 = r6.A06;
            if (j12 == 0) {
                valueOf8 = null;
            } else {
                valueOf8 = Long.valueOf(j12);
            }
            r5.A0A = valueOf8;
            long j13 = r6.A08;
            if (j13 != 0) {
                l = Long.valueOf(j13);
            }
            r5.A0C = l;
            r5.A0D = Long.valueOf(r6.A0C);
            r5.A04 = Integer.valueOf(r6.A0A);
            r5.A05 = Integer.valueOf(r6.A0B);
            r5.A03 = Integer.valueOf(r6.A09);
            r5.A00 = Boolean.valueOf(r6.A0D);
            r9.A07(r5);
        }
        C17060qC r62 = this.A0F;
        SharedPreferences A012 = r62.A01.A01("ntp-scheduler");
        long j14 = A012.getLong("/ntp/last_event_timestamp", 0);
        long currentTimeMillis = System.currentTimeMillis();
        if (j14 == 0) {
            synchronized (r62) {
                A012.edit().putLong("/ntp/last_event_timestamp", currentTimeMillis).apply();
            }
        } else {
            long j15 = currentTimeMillis - j14;
            synchronized (r62) {
                j = (long) A012.getInt("/ntp/started", 0);
                j2 = (long) A012.getInt("/ntp/succeeded", 0);
                j3 = (long) A012.getInt("/ntp/failed", 0);
                j4 = A012.getLong("/ntp/work_manager_init", -1);
                A012.edit().remove("/ntp/started").remove("/ntp/succeeded").remove("/ntp/failed").putLong("/ntp/last_event_timestamp", currentTimeMillis).apply();
            }
            C855643h r1 = new C855643h();
            r1.A02 = Long.valueOf(j);
            r1.A03 = Long.valueOf(j2);
            r1.A01 = Long.valueOf(j3);
            r1.A00 = Long.valueOf(j15);
            r1.A04 = Long.valueOf(j4);
            r62.A00.A07(r1);
        }
        C14420lP r63 = this.A06;
        synchronized (r63) {
            AnonymousClass009.A00();
            r63.A00.A06(-1);
            try {
                A02 = r63.A02.A02();
                try {
                    AnonymousClass1Lx A002 = A02.A00();
                    A02.A03.A01("media_job", "last_update_time < ?", new String[]{String.valueOf(r63.A01.A00() - 1209600000)});
                    A002.A00();
                    A002.close();
                    A02.close();
                } finally {
                }
            } catch (SQLiteDatabaseCorruptException e) {
                Log.e("mediajobdb/delete All Older than", e);
            }
        }
        C17030q9 r64 = this.A00;
        C22150yc r4 = r64.A01;
        Log.i("language-pack-store/delete-unused-language-packs");
        long A003 = (r4.A00.A00() - 604800000) / 1000;
        C43801xc r52 = r4.A01;
        C16310on A022 = r52.A02();
        try {
            AnonymousClass1Lx A004 = A022.A00();
            C16330op r72 = A022.A03;
            String l2 = Long.toString(A003);
            int A013 = r72.A01("packs", "length(data) == 0 AND timestamp < ?", new String[]{l2});
            int A014 = r72.A01("packs", "length(data) > 0 AND timestamp < ?", new String[]{l2});
            A004.A00();
            A004.close();
            A022.close();
            if (A013 > 0 || A014 > 0) {
                StringBuilder sb = new StringBuilder("language-pack-store/delete-unused-language-packs empty=");
                sb.append(A013);
                sb.append(" unused=");
                sb.append(A014);
                Log.i(sb.toString());
                C246516i r12 = r64.A03;
                synchronized (r12) {
                    r12.clear();
                    r64.A05.clear();
                }
                Log.i("language-pack-store/vacuum");
                A02 = r52.A02();
                try {
                    A02.A03.A0B("VACUUM");
                    A02.close();
                } finally {
                }
            }
            Context context = this.A04.A00;
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = new ArrayList();
            HashMap hashMap = AnonymousClass393.A0A;
            synchronized (hashMap) {
                for (AbstractC14640lm r0 : hashMap.keySet()) {
                    AnonymousClass393 r2 = (AnonymousClass393) hashMap.get(r0);
                    if (r2 != null) {
                        if (r2.A00() != 1) {
                            File A005 = AnonymousClass393.A00(context, r2);
                            if (A005.exists()) {
                                A005.delete();
                            }
                            arrayList2.add(r2);
                        } else {
                            File A006 = AnonymousClass393.A00(context, r2);
                            if (A006.exists()) {
                                arrayList3.add(A006);
                            }
                        }
                    }
                }
                Iterator it2 = arrayList2.iterator();
                while (it2.hasNext()) {
                    hashMap.remove(((AnonymousClass393) it2.next()).A06.A03);
                }
            }
            File file = new File(context.getCacheDir(), "ProfilePictureTemp");
            file.mkdirs();
            File[] listFiles2 = file.listFiles();
            if (listFiles2 != null) {
                for (File file2 : listFiles2) {
                    if (!arrayList3.contains(file2)) {
                        file2.delete();
                    }
                }
            }
            C17080qE r13 = this.A07;
            Context context2 = this.A04.A00;
            ArrayList arrayList4 = new ArrayList();
            ArrayList arrayList5 = new ArrayList();
            HashMap hashMap2 = r13.A0A;
            synchronized (hashMap2) {
                for (Map.Entry entry2 : hashMap2.entrySet()) {
                    C41841uF r14 = (C41841uF) entry2.getValue();
                    if (r14 != null) {
                        if (!r14.A08) {
                            C41831uE r02 = r14.A04;
                            File A007 = C17080qE.A00(context2, r02.A04, r02.A05);
                            if (A007.exists()) {
                                A007.delete();
                            }
                            arrayList4.add((String) entry2.getKey());
                        } else {
                            C41831uE r03 = r14.A04;
                            File A008 = C17080qE.A00(context2, r03.A04, r03.A05);
                            if (A008.exists()) {
                                arrayList5.add(A008);
                            }
                        }
                    }
                }
                Iterator it3 = arrayList4.iterator();
                while (it3.hasNext()) {
                    hashMap2.remove((String) it3.next());
                }
            }
            File file3 = new File(context2.getCacheDir(), "ProfilePictureTemp");
            file3.mkdirs();
            File[] listFiles3 = file3.listFiles();
            if (listFiles3 != null) {
                for (File file4 : listFiles3) {
                    if (!arrayList5.contains(file4)) {
                        C14350lI.A0M(file4);
                    }
                }
            }
            this.A0D.A01().edit().remove("start_video_call_no_preview").remove("enable_telecom_framework_caller").remove("call_enable_caller_message_buffer").remove("call_enable_callee_message_buffer").apply();
            C17110qH r10 = this.A0A;
            long A009 = this.A03.A00();
            synchronized (r10) {
                sharedPreferences = r10.A01;
                if (sharedPreferences == null) {
                    sharedPreferences = r10.A02.A01("triggered_block_prefs_purge_ts");
                    r10.A01 = sharedPreferences;
                }
                AnonymousClass009.A05(sharedPreferences);
            }
            if (A009 >= sharedPreferences.getLong("tb_purge_last_ts", 0) + 86400000) {
                if (!sharedPreferences.edit().putLong("tb_purge_last_ts", A009).commit()) {
                    Log.w("triggered-block/purgeIfNeeded unable to commit last purge ts.");
                }
                try {
                    ArrayList arrayList6 = new ArrayList();
                    for (Map.Entry<String, ?> entry3 : r10.A00().getAll().entrySet()) {
                        String key = entry3.getKey();
                        String str = (String) entry3.getValue();
                        if (!TextUtils.isEmpty(str)) {
                            JSONObject jSONObject = new JSONObject(str);
                            if (!jSONObject.has("tb_expired_ts") || A009 >= jSONObject.getLong("tb_expired_ts")) {
                                if (!jSONObject.has("tb_last_action_ts") || !jSONObject.has("tb_cooldown") || A009 >= jSONObject.getLong("tb_last_action_ts") + jSONObject.getLong("tb_cooldown")) {
                                    if (jSONObject.has("tb_last_action_ts") && !jSONObject.has("tb_cooldown") && A009 < jSONObject.getLong("tb_last_action_ts") + 31536000000L) {
                                    }
                                }
                            }
                        }
                        arrayList6.add(key);
                    }
                    SharedPreferences.Editor edit2 = r10.A00().edit();
                    Iterator it4 = arrayList6.iterator();
                    while (it4.hasNext()) {
                        edit2.remove((String) it4.next());
                    }
                    if (!edit2.commit()) {
                        Log.w("triggered-block/purge unable to commit after purge.");
                    }
                    arrayList6.size();
                } catch (JSONException unused) {
                }
            }
            File A07 = AnonymousClass1SF.A07(this.A0E.A02.A00);
            if (!(A07 == null || (listFiles = A07.listFiles()) == null)) {
                long currentTimeMillis2 = System.currentTimeMillis();
                for (File file5 : listFiles) {
                    long lastModified = currentTimeMillis2 - file5.lastModified();
                    if (lastModified > TimeUnit.DAYS.toMillis(1) || lastModified < 0) {
                        C14350lI.A0N(file5);
                    }
                }
            }
        } finally {
            try {
                A022.close();
            } catch (Throwable unused2) {
            }
        }
    }

    @Override // X.AbstractC16990q5
    public void AOp() {
        this.A0B.Ab2(new RunnableBRunnable0Shape7S0100000_I0_7(this, 26));
        C17090qF r5 = this.A08;
        if (r5.A08.A07(394)) {
            AnonymousClass31M r4 = new AnonymousClass31M();
            AnonymousClass00E r3 = r4.samplingRate;
            if (r3.A00()) {
                r5.A0B.Ab2(new RunnableBRunnable0Shape1S0300000_I0_1(r5, r4, r3, 46));
            }
        }
        C17100qG r32 = this.A09;
        Log.i("storage-usage-prefetcher/prefetch storage data");
        r32.A08.Ab2(new RunnableBRunnable0Shape12S0100000_I0_12(r32, 37));
    }
}
