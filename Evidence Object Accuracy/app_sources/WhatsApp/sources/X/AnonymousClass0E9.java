package X;

import java.io.InputStream;
import java.nio.ByteOrder;

/* renamed from: X.0E9  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0E9 extends C03660Iq {
    public AnonymousClass0E9(InputStream inputStream) {
        super(inputStream, ByteOrder.BIG_ENDIAN);
        if (inputStream.markSupported()) {
            this.A03.mark(Integer.MAX_VALUE);
            return;
        }
        throw new IllegalArgumentException("Cannot create SeekableByteOrderedDataInputStream with stream that does not support mark/reset");
    }

    public AnonymousClass0E9(byte[] bArr) {
        super(bArr);
        this.A03.mark(Integer.MAX_VALUE);
    }

    public void A01(long j) {
        long j2 = (long) this.A00;
        if (j2 > j) {
            this.A00 = 0;
            this.A03.reset();
        } else {
            j -= j2;
        }
        A00((int) j);
    }
}
