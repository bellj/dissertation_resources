package X;

import com.facebook.simplejni.NativeHolder;
import com.whatsapp.wamsys.JniBridge;

/* renamed from: X.1Rk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C29241Rk {
    public final NativeHolder A00;

    public C29241Rk(NativeHolder nativeHolder) {
        this.A00 = nativeHolder;
    }

    public C29241Rk(String str, String str2, byte[] bArr, byte[] bArr2, byte[] bArr3, int i, long j) {
        JniBridge.getInstance();
        this.A00 = new C29241Rk((NativeHolder) JniBridge.jvidispatchOIIIOOOOOO((long) i, j, 0, str, str2, null, bArr, bArr2, bArr3)).A00;
    }
}
