package X;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.conversationslist.ViewHolder;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.wds.components.profilephoto.WDSProfilePhoto;

/* renamed from: X.2za  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C61202za extends AbstractC65103Id implements AnonymousClass5RT {
    public AnonymousClass42Q A00;
    public C61222zc A01;
    public C92434Vw A02;
    public final int A03;
    public final C253519b A04;
    public final AnonymousClass130 A05;
    public final C15550nR A06;
    public final AnonymousClass1J1 A07;
    public final C63563Cb A08;
    public final AbstractC33091dK A09;
    public final C14820m6 A0A;
    public final C19990v2 A0B;
    public final C15600nX A0C;
    public final C236812p A0D;
    public final AnonymousClass10Y A0E;
    public final C21400xM A0F;
    public final C15860o1 A0G;
    public final AbstractC14440lR A0H;
    public final AbstractC93424a9 A0I = new AnonymousClass48O(EnumC87274Aw.A01);
    public final AbstractC93424a9 A0J = new AnonymousClass48O(EnumC87274Aw.A02);

    public C61202za(Activity activity, Context context, C253519b r31, C15570nT r32, C15450nH r33, AnonymousClass11L r34, C14650lo r35, C238013b r36, AnonymousClass130 r37, C15550nR r38, C15610nY r39, AnonymousClass1J1 r40, C63563Cb r41, AbstractC33091dK r42, C63543Bz r43, ViewHolder viewHolder, C14830m7 r45, C16590pI r46, C14820m6 r47, AnonymousClass018 r48, C19990v2 r49, C241714m r50, C15600nX r51, C236812p r52, AnonymousClass10Y r53, C21400xM r54, C14850m9 r55, C20710wC r56, AnonymousClass13H r57, C22710zW r58, C17070qD r59, AnonymousClass14X r60, C15860o1 r61, C92434Vw r62, AnonymousClass3J9 r63, AbstractC14440lR r64, int i) {
        super(activity, context, r32, r33, r34, r35, r36, r38, r39, r42, r43, viewHolder, r45, r46, r48, r49, r50, r55, r56, r57, r58, r59, r60, r63);
        this.A03 = i;
        this.A0H = r64;
        this.A02 = r62;
        this.A0B = r49;
        this.A05 = r37;
        this.A06 = r38;
        this.A04 = r31;
        this.A0E = r53;
        this.A0G = r61;
        this.A07 = r40;
        this.A0F = r54;
        this.A0A = r47;
        this.A0D = r52;
        this.A08 = r41;
        this.A0C = r51;
        this.A09 = r42;
    }

    public static void A00(View view, ViewHolder viewHolder) {
        view.setVisibility(8);
        viewHolder.A0G.setVisibility(8);
        viewHolder.A06.setVisibility(8);
        viewHolder.A0E.setVisibility(8);
        viewHolder.A0B.setVisibility(8);
        viewHolder.A0C.setVisibility(8);
        viewHolder.A0P.setVisibility(8);
        viewHolder.A0D.setVisibility(8);
        viewHolder.A0A.setVisibility(8);
        viewHolder.A03.setVisibility(8);
        viewHolder.A0K(false, false);
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x008c  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00a3  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00d4  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00dd  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0100  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0173  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x017c  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x01b5  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001e  */
    @Override // X.AbstractC65103Id
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ void A05(X.AbstractC51662Vw r20, X.AnonymousClass5U4 r21, int r22, boolean r23) {
        /*
        // Method dump skipped, instructions count: 441
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C61202za.A05(X.2Vw, X.5U4, int, boolean):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x004b, code lost:
        if (r2 == 5) goto L_0x004d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0328, code lost:
        if (r21 == false) goto L_0x032c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x0341, code lost:
        r0 = 0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A07(X.AnonymousClass3BZ r41, X.AnonymousClass5U4 r42, int r43) {
        /*
        // Method dump skipped, instructions count: 1087
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C61202za.A07(X.3BZ, X.5U4, int):void");
    }

    public final void A08(AbstractC14640lm r9) {
        int A00 = C32341c0.A00(this.A06, this.A0B, r9);
        boolean z = false;
        if (A00 > 0) {
            z = true;
            ImageView imageView = super.A0B.A08;
            if (imageView.getContentDescription() != null) {
                imageView.setContentDescription(C12960it.A0X(super.A01, imageView.getContentDescription(), new Object[1], 0, R.string.tb_ephemeral_chat_has_disappearing_messages_on));
            }
        }
        super.A0B.A0J(z, A00);
    }

    public final void A09(AbstractC14640lm r6, AnonymousClass1YS r7) {
        ImageView imageView;
        int i;
        Context context;
        int i2;
        WDSProfilePhoto wDSProfilePhoto;
        AbstractC93424a9 r0;
        if (!AnonymousClass1SF.A0P(super.A0H) || r7 == null || (context = super.A01) == null) {
            if (GroupJid.of(r6) != null) {
                ImageView imageView2 = super.A0B.A08;
                if (imageView2 instanceof WDSProfilePhoto) {
                    ((WDSProfilePhoto) imageView2).setProfileBadge(null);
                    A08(r6);
                    return;
                }
            }
            imageView = super.A0B.A07;
            i = 8;
        } else {
            boolean z = r7.A04;
            ViewHolder viewHolder = super.A0B;
            ImageView imageView3 = viewHolder.A08;
            boolean z2 = imageView3 instanceof WDSProfilePhoto;
            if (z) {
                if (z2) {
                    wDSProfilePhoto = (WDSProfilePhoto) imageView3;
                    r0 = this.A0I;
                    wDSProfilePhoto.setProfileBadge(r0);
                    return;
                }
                imageView = viewHolder.A07;
                imageView.setImageResource(R.drawable.ic_voip_chatlist_joinable_video);
                i2 = R.string.video_ongoing_call;
                C12960it.A0r(context, imageView, i2);
                i = 0;
            } else if (z2) {
                wDSProfilePhoto = (WDSProfilePhoto) imageView3;
                r0 = this.A0J;
                wDSProfilePhoto.setProfileBadge(r0);
                return;
            } else {
                imageView = viewHolder.A07;
                imageView.setImageResource(R.drawable.ic_voip_chatlist_joinable_voice);
                i2 = R.string.ongoing_voice_call;
                C12960it.A0r(context, imageView, i2);
                i = 0;
            }
        }
        imageView.setVisibility(i);
    }

    public final void A0A(C92434Vw r3, WDSProfilePhoto wDSProfilePhoto) {
        if (!super.A0H.A07(1533)) {
            return;
        }
        if (r3 == null || r3.A01 <= 0 || !r3.A00()) {
            wDSProfilePhoto.setStatusIndicatorEnabled(false);
            return;
        }
        wDSProfilePhoto.setStatusIndicatorEnabled(true);
        wDSProfilePhoto.setStatusIndicatorState(EnumC87284Ax.A01);
    }
}
