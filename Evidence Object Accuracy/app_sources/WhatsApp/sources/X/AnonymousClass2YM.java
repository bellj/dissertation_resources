package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import com.whatsapp.R;
import com.whatsapp.calling.CallDetailsLayout;

/* renamed from: X.2YM  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2YM extends AnimatorListenerAdapter {
    public final /* synthetic */ CallDetailsLayout A00;

    public AnonymousClass2YM(CallDetailsLayout callDetailsLayout) {
        this.A00 = callDetailsLayout;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
        super.onAnimationCancel(animator);
        CallDetailsLayout callDetailsLayout = this.A00;
        callDetailsLayout.A00 = 0;
        callDetailsLayout.clearAnimation();
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        super.onAnimationCancel(animator);
        CallDetailsLayout callDetailsLayout = this.A00;
        callDetailsLayout.A00 = 2;
        AnonymousClass071 r2 = new AnonymousClass071();
        r2.A04(400);
        r2.A05(new AccelerateDecelerateInterpolator());
        AnonymousClass073.A01((ViewGroup) callDetailsLayout.getParent(), r2);
        callDetailsLayout.A04.setVisibility(8);
        CallDetailsLayout.A01(callDetailsLayout, Integer.valueOf(callDetailsLayout.getResources().getDimensionPixelSize(R.dimen.joinable_call_top_bar_height)), null);
        callDetailsLayout.clearAnimation();
    }
}
