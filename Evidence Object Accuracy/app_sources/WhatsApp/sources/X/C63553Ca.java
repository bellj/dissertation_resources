package X;

import java.util.List;
import java.util.Map;

/* renamed from: X.3Ca  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63553Ca {
    public final Map A00 = C12970iu.A11();
    public volatile Class A01;

    public C63553Ca(List list) {
        for (Object obj : list) {
            if (obj != null) {
                Map map = this.A00;
                Class<?> cls = obj.getClass();
                if (!map.containsKey(cls)) {
                    this.A00.put(cls, obj);
                }
            }
            throw C12990iw.A0m(C12960it.A0b("Invalid banner ", obj));
        }
    }
}
