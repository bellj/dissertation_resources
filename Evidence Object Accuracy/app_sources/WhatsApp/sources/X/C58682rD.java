package X;

import android.view.View;
import com.whatsapp.avatar.profilephoto.AvatarProfilePhotoColorView;

/* renamed from: X.2rD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C58682rD extends AbstractC75643kB {
    public final AvatarProfilePhotoColorView A00;

    public C58682rD(View view) {
        super(view);
        this.A00 = (AvatarProfilePhotoColorView) view;
    }

    @Override // X.AbstractC75643kB
    public void A08(AbstractC87964Ds r5, AnonymousClass1J7 r6) {
        EnumC870249x r0;
        C58722rH r1 = (C58722rH) r5;
        if (r1.A02) {
            r0 = EnumC870249x.A02;
        } else {
            r0 = EnumC870249x.A01;
        }
        AvatarProfilePhotoColorView avatarProfilePhotoColorView = this.A00;
        int i = r1.A00;
        int i2 = r1.A01;
        avatarProfilePhotoColorView.A00 = r0;
        avatarProfilePhotoColorView.A01.setColor(i);
        avatarProfilePhotoColorView.A02.setColor(i2);
        avatarProfilePhotoColorView.invalidate();
        C12960it.A13(this.A0H, r6, r5, 3);
    }
}
