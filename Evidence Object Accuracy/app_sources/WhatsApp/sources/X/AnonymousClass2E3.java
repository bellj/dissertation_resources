package X;

/* renamed from: X.2E3  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2E3 extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Integer A02;
    public Integer A03;
    public Long A04;
    public Long A05;
    public String A06;

    public AnonymousClass2E3() {
        super(3002, new AnonymousClass00E(1, 1, 1), 2, 113760892);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(3, this.A02);
        r3.Abe(2, this.A03);
        r3.Abe(5, this.A04);
        r3.Abe(6, this.A05);
        r3.Abe(4, this.A00);
        r3.Abe(7, this.A01);
        r3.Abe(1, this.A06);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        StringBuilder sb = new StringBuilder("WamMessageTemplateBlocks {");
        Integer num = this.A02;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "blockEntryPoint", obj);
        Integer num2 = this.A03;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "blockReason", obj2);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "businessMessageTime", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "businessPhoneNumber", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "didUserReply", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "isUnsubBlock", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "templateId", this.A06);
        sb.append("}");
        return sb.toString();
    }
}
