package X;

import android.content.DialogInterface;
import com.whatsapp.R;
import com.whatsapp.twofactor.SettingsTwoFactorAuthActivity;
import com.whatsapp.util.Log;

/* renamed from: X.3KJ  reason: invalid class name */
/* loaded from: classes2.dex */
public final /* synthetic */ class AnonymousClass3KJ implements DialogInterface.OnClickListener {
    public final /* synthetic */ SettingsTwoFactorAuthActivity.ConfirmDisableDialog A00;

    public /* synthetic */ AnonymousClass3KJ(SettingsTwoFactorAuthActivity.ConfirmDisableDialog confirmDisableDialog) {
        this.A00 = confirmDisableDialog;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        SettingsTwoFactorAuthActivity settingsTwoFactorAuthActivity = (SettingsTwoFactorAuthActivity) this.A00.A0B();
        settingsTwoFactorAuthActivity.A2C(R.string.two_factor_auth_disabling);
        settingsTwoFactorAuthActivity.A0A.postDelayed(settingsTwoFactorAuthActivity.A0B, C14920mG.A0D);
        C14920mG r2 = settingsTwoFactorAuthActivity.A08;
        Log.i("twofactorauthmanager/disable-two-factor-auth");
        r2.A03("", null);
    }
}
