package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import com.facebook.redex.RunnableBRunnable0Shape5S0200000_I0_5;
import com.whatsapp.util.Log;

/* renamed from: X.0lP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14420lP {
    public final C006202y A00 = new C38021nO(this);
    public final C14830m7 A01;
    public final C16270oj A02;
    public final AbstractC14440lR A03;

    public C14420lP(C14830m7 r2, C16270oj r3, AbstractC14440lR r4) {
        this.A01 = r2;
        this.A03 = r4;
        this.A02 = r3;
    }

    public final ContentValues A00(C14430lQ r25) {
        String str = r25.A0D;
        long j = (long) r25.A0B;
        long j2 = r25.A0C;
        long j3 = r25.A09;
        long j4 = (long) r25.A03;
        long j5 = r25.A07;
        long j6 = r25.A08;
        int i = r25.A02;
        int i2 = r25.A00;
        long j7 = r25.A0A;
        int i3 = r25.A01;
        long j8 = r25.A04;
        long j9 = r25.A05;
        ContentValues contentValues = new ContentValues();
        contentValues.put("uuid", str);
        contentValues.put("job_type", Long.valueOf(j));
        contentValues.put("create_time", Long.valueOf(j2));
        contentValues.put("transfer_start_time", Long.valueOf(j3));
        contentValues.put("last_update_time", Long.valueOf(this.A01.A00()));
        contentValues.put("user_initiated_attempt_count", Long.valueOf(j4));
        contentValues.put("overall_cumulative_time", Long.valueOf(j5));
        contentValues.put("overall_cumulative_user_visible_time", Long.valueOf(j6));
        contentValues.put("streaming_playback_count", Integer.valueOf(i));
        contentValues.put("media_key_reuse_type", Integer.valueOf(i2));
        contentValues.put("transferred_bytes", Long.valueOf(j7));
        contentValues.put("reupload_attempt_count", Integer.valueOf(i3));
        contentValues.put("last_reupload_attempt_timestamp", Long.valueOf(j8));
        contentValues.put("last_reupload_success_timestamp", Long.valueOf(j9));
        return contentValues;
    }

    public C14430lQ A01(String str, int i) {
        C14830m7 r1 = this.A01;
        long A00 = r1.A00();
        long A002 = r1.A00();
        long A003 = r1.A00();
        boolean z = true;
        boolean z2 = false;
        if (A00 > 0) {
            z2 = true;
        }
        AnonymousClass009.A0F(z2);
        if (A003 <= 0) {
            z = false;
        }
        AnonymousClass009.A0F(z);
        AnonymousClass009.A05(str);
        C14430lQ r4 = new C14430lQ(str, i, 0, 0, 0, 0, A00, A002, A003, 0, 0, 0, 0, 0);
        this.A03.Ab2(new RunnableBRunnable0Shape5S0200000_I0_5(this, 42, r4));
        return r4;
    }

    public synchronized C14430lQ A02(String str, int i) {
        AnonymousClass009.A00();
        C006202y r3 = this.A00;
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(i);
        C14430lQ r0 = (C14430lQ) r3.A04(sb.toString());
        if (r0 != null) {
            return r0;
        }
        C16310on A01 = this.A02.get();
        Cursor A09 = A01.A03.A09("SELECT + _id, uuid, job_type , create_time, transfer_start_time, last_update_time, user_initiated_attempt_count, overall_cumulative_time, overall_cumulative_user_visible_time, streaming_playback_count, media_key_reuse_type, doodle_id, transferred_bytes, reupload_attempt_count, last_reupload_attempt_timestamp, last_reupload_success_timestamp FROM media_job WHERE uuid=? AND job_type=?", new String[]{str, Integer.toString(i)});
        if (A09 != null) {
            try {
                if (A09.moveToLast()) {
                    String string = A09.getString(1);
                    int i2 = A09.getInt(2);
                    long j = A09.getLong(3);
                    long j2 = A09.getLong(4);
                    long j3 = A09.getLong(5);
                    int i3 = A09.getInt(6);
                    long j4 = A09.getLong(7);
                    long j5 = A09.getLong(8);
                    int i4 = A09.getInt(9);
                    int i5 = A09.getInt(10);
                    long j6 = A09.getLong(12);
                    int i6 = A09.getInt(13);
                    long j7 = A09.getLong(14);
                    long j8 = A09.getLong(15);
                    boolean z = true;
                    boolean z2 = false;
                    if (j > 0) {
                        z2 = true;
                    }
                    AnonymousClass009.A0F(z2);
                    if (j3 <= 0) {
                        z = false;
                    }
                    AnonymousClass009.A0F(z);
                    AnonymousClass009.A05(string);
                    C14430lQ r8 = new C14430lQ(string, i2, i3, i4, i5, i6, j, j2, j3, j4, j5, j6, j7, j8);
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(r8.A0D);
                    sb2.append(r8.A0B);
                    r3.A08(sb2.toString(), r8);
                    A09.close();
                    A01.close();
                    return r8;
                }
            } catch (Throwable th) {
                try {
                    A09.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
        if (A09 != null) {
            A09.close();
        }
        A01.close();
        return null;
    }

    public void A03(C14430lQ r4) {
        r4.A06 = this.A01.A00();
        if (A05(r4)) {
            C006202y r2 = this.A00;
            StringBuilder sb = new StringBuilder();
            sb.append(r4.A0D);
            sb.append(r4.A0B);
            r2.A08(sb.toString(), r4);
        }
    }

    public synchronized void A04(C14430lQ r11) {
        AnonymousClass009.A00();
        try {
            C16310on A02 = this.A02.A02();
            try {
                AnonymousClass1Lx A00 = A02.A00();
                C16330op r8 = A02.A03;
                String str = r11.A0D;
                int i = r11.A0B;
                r8.A01("media_job", "uuid = ? AND job_type = ? ", new String[]{str, Integer.toString(i)});
                A00.A00();
                C006202y r1 = this.A00;
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(i);
                r1.A07(sb.toString());
                A00.close();
                A02.close();
            } catch (Throwable th) {
                try {
                    A02.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } catch (SQLiteDatabaseCorruptException e) {
            Log.e("mediajobdb/delete", e);
        }
    }

    public final synchronized boolean A05(C14430lQ r10) {
        AnonymousClass009.A00();
        try {
            try {
                C16310on A02 = this.A02.A02();
                try {
                    A02.A03.A00("media_job", A00(r10), "uuid = ? AND job_type = ? ", new String[]{r10.A0D, Integer.toString(r10.A0B)});
                    A02.close();
                } catch (Throwable th) {
                    try {
                        A02.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            } catch (SQLiteDatabaseCorruptException e) {
                Log.e("mediajobdb/update", e);
                return false;
            }
        } catch (Error | RuntimeException e2) {
            Log.e(e2);
            throw e2;
        }
        return true;
    }
}
