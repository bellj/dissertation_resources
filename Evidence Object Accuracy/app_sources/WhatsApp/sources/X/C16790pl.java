package X;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0pl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16790pl {
    public final Object A00;
    public final Object A01;

    public C16790pl(Object obj, Object obj2) {
        this.A00 = obj;
        this.A01 = obj2;
    }

    public static Map A00(Set set) {
        AnonymousClass00N r3 = new AnonymousClass00N();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            C16790pl r0 = (C16790pl) it.next();
            r3.put(r0.A00, r0.A01);
        }
        return r3;
    }
}
