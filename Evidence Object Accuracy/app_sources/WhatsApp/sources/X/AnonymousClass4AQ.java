package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.4AQ  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4AQ extends Enum {
    public static final /* synthetic */ AnonymousClass4AQ[] A00;
    public static final AnonymousClass4AQ A01;
    public static final AnonymousClass4AQ A02;
    public static final AnonymousClass4AQ A03;
    public static final AnonymousClass4AQ A04;

    static {
        AnonymousClass4AQ r14 = new AnonymousClass4AQ("OTHER", 0);
        AnonymousClass4AQ r12 = new AnonymousClass4AQ("ORIENTATION", 1);
        AnonymousClass4AQ r11 = new AnonymousClass4AQ("BYTE_SEGMENTS", 2);
        A01 = r11;
        AnonymousClass4AQ r10 = new AnonymousClass4AQ("ERROR_CORRECTION_LEVEL", 3);
        A02 = r10;
        AnonymousClass4AQ r9 = new AnonymousClass4AQ("ISSUE_NUMBER", 4);
        AnonymousClass4AQ r8 = new AnonymousClass4AQ("SUGGESTED_PRICE", 5);
        AnonymousClass4AQ r7 = new AnonymousClass4AQ("POSSIBLE_COUNTRY", 6);
        AnonymousClass4AQ r6 = new AnonymousClass4AQ("UPC_EAN_EXTENSION", 7);
        AnonymousClass4AQ r5 = new AnonymousClass4AQ("PDF417_EXTRA_METADATA", 8);
        AnonymousClass4AQ r4 = new AnonymousClass4AQ("STRUCTURED_APPEND_SEQUENCE", 9);
        A04 = r4;
        AnonymousClass4AQ r2 = new AnonymousClass4AQ("STRUCTURED_APPEND_PARITY", 10);
        A03 = r2;
        AnonymousClass4AQ[] r1 = new AnonymousClass4AQ[11];
        r1[0] = r14;
        r1[1] = r12;
        C12980iv.A1P(r11, r10, r9, r1);
        C12970iu.A1R(r8, r7, r6, r5, r1);
        r1[9] = r4;
        r1[10] = r2;
        A00 = r1;
    }

    public AnonymousClass4AQ(String str, int i) {
    }

    public static AnonymousClass4AQ valueOf(String str) {
        return (AnonymousClass4AQ) Enum.valueOf(AnonymousClass4AQ.class, str);
    }

    public static AnonymousClass4AQ[] values() {
        return (AnonymousClass4AQ[]) A00.clone();
    }
}
