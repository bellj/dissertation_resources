package X;

import android.transition.Transition;
import java.util.ArrayList;

/* renamed from: X.0Vw  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Vw implements Transition.TransitionListener {
    public final /* synthetic */ AnonymousClass0EI A00;
    public final /* synthetic */ Object A01;
    public final /* synthetic */ Object A02;
    public final /* synthetic */ ArrayList A03;
    public final /* synthetic */ ArrayList A04;

    @Override // android.transition.Transition.TransitionListener
    public void onTransitionCancel(Transition transition) {
    }

    @Override // android.transition.Transition.TransitionListener
    public void onTransitionPause(Transition transition) {
    }

    @Override // android.transition.Transition.TransitionListener
    public void onTransitionResume(Transition transition) {
    }

    public AnonymousClass0Vw(AnonymousClass0EI r1, Object obj, Object obj2, ArrayList arrayList, ArrayList arrayList2) {
        this.A00 = r1;
        this.A01 = obj;
        this.A03 = arrayList;
        this.A02 = obj2;
        this.A04 = arrayList2;
    }

    @Override // android.transition.Transition.TransitionListener
    public void onTransitionEnd(Transition transition) {
        transition.removeListener(this);
    }

    @Override // android.transition.Transition.TransitionListener
    public void onTransitionStart(Transition transition) {
        Object obj = this.A01;
        if (obj != null) {
            this.A00.A0H(obj, this.A03, null);
        }
        Object obj2 = this.A02;
        if (obj2 != null) {
            this.A00.A0H(obj2, this.A04, null);
        }
    }
}
