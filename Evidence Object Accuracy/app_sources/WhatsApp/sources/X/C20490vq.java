package X;

/* renamed from: X.0vq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20490vq {
    public final C16510p9 A00;
    public final C16490p7 A01;
    public final C21390xL A02;
    public final C20950wa A03;

    public C20490vq(C16510p9 r1, C16490p7 r2, C21390xL r3, C20950wa r4) {
        this.A00 = r1;
        this.A02 = r3;
        this.A03 = r4;
        this.A01 = r2;
    }

    public void A00(AbstractC15340mz r7, long j, boolean z) {
        StringBuilder sb = new StringBuilder("TextMessageStore/insertOrUpdateQuotedTextMessage/message must be a text message; key=");
        AnonymousClass1IS r5 = r7.A0z;
        sb.append(r5);
        AnonymousClass009.A0B(sb.toString(), r7 instanceof C28861Ph);
        boolean z2 = false;
        if (j > 0) {
            z2 = true;
        }
        StringBuilder sb2 = new StringBuilder("TextMessageStore/insertOrUpdateQuotedTextMessage/message must have row_id set; key=");
        sb2.append(r5);
        AnonymousClass009.A0B(sb2.toString(), z2);
        boolean z3 = false;
        if (r7.A08() == 2) {
            z3 = true;
        }
        StringBuilder sb3 = new StringBuilder("TextMessageStore/insertOrUpdateQuotedTextMessage/message in main storage; key=");
        sb3.append(r5);
        AnonymousClass009.A0B(sb3.toString(), z3);
        C28861Ph r72 = (C28861Ph) r7;
        if (z) {
            A01(r72, j, true);
        } else {
            A01(r72, j, false);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0008, code lost:
        if (r1 == 0) goto L_0x000a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A01(X.C28861Ph r10, long r11, boolean r13) {
        /*
            r9 = this;
            byte[] r0 = r10.A17()
            if (r0 == 0) goto L_0x000a
            int r1 = r0.length
            r0 = 1
            if (r1 != 0) goto L_0x000b
        L_0x000a:
            r0 = 0
        L_0x000b:
            java.lang.String r6 = "message_quoted_text"
            r5 = 0
            r7 = 1
            if (r0 == 0) goto L_0x0058
            X.0p7 r0 = r9.A01
            X.0on r3 = r0.A02()
            android.content.ContentValues r8 = new android.content.ContentValues     // Catch: all -> 0x0053
            r8.<init>()     // Catch: all -> 0x0053
            java.lang.String r1 = "message_row_id"
            java.lang.Long r0 = java.lang.Long.valueOf(r11)     // Catch: all -> 0x0053
            r8.put(r1, r0)     // Catch: all -> 0x0053
            java.lang.String r1 = "thumbnail"
            byte[] r0 = r10.A17()     // Catch: all -> 0x0053
            X.C30021Vq.A06(r8, r1, r0)     // Catch: all -> 0x0053
            X.0op r4 = r3.A03     // Catch: all -> 0x0053
            java.lang.String r2 = "message_row_id = ?"
            java.lang.String[] r1 = new java.lang.String[r7]     // Catch: all -> 0x0053
            java.lang.String r0 = java.lang.String.valueOf(r11)     // Catch: all -> 0x0053
            r1[r5] = r0     // Catch: all -> 0x0053
            int r0 = r4.A00(r6, r8, r2, r1)     // Catch: all -> 0x0053
            if (r0 == r7) goto L_0x004f
            long r1 = r4.A03(r8, r6)     // Catch: all -> 0x0053
            int r0 = (r1 > r11 ? 1 : (r1 == r11 ? 0 : -1))
            if (r0 != 0) goto L_0x004a
            r5 = 1
        L_0x004a:
            java.lang.String r0 = "TextMessageStore/insertOrUpdateQuotedTextMessage/inserted row should have same row_id"
            X.AnonymousClass009.A0C(r0, r5)     // Catch: all -> 0x0053
        L_0x004f:
            r3.close()
            return
        L_0x0053:
            r0 = move-exception
            r3.close()     // Catch: all -> 0x0057
        L_0x0057:
            throw r0
        L_0x0058:
            if (r13 == 0) goto L_0x0078
            X.0p7 r0 = r9.A01
            X.0on r4 = r0.A02()
            X.0op r3 = r4.A03     // Catch: all -> 0x0070
            java.lang.String r2 = "message_row_id = ?"
            java.lang.String[] r1 = new java.lang.String[r7]     // Catch: all -> 0x0070
            java.lang.String r0 = java.lang.String.valueOf(r11)     // Catch: all -> 0x0070
            r1[r5] = r0     // Catch: all -> 0x0070
            r3.A01(r6, r2, r1)     // Catch: all -> 0x0070
            goto L_0x0075
        L_0x0070:
            r0 = move-exception
            r4.close()     // Catch: all -> 0x0074
        L_0x0074:
            throw r0
        L_0x0075:
            r4.close()
        L_0x0078:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20490vq.A01(X.1Ph, long, boolean):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0023, code lost:
        if (r12.A00 != 0) goto L_0x0025;
     */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x012a  */
    /* JADX WARNING: Removed duplicated region for block: B:74:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A02(X.C28861Ph r12, boolean r13) {
        /*
        // Method dump skipped, instructions count: 304
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20490vq.A02(X.1Ph, boolean):void");
    }

    public boolean A03() {
        return this.A00.A0G() && this.A02.A01("text_ready", 0) == 1;
    }

    public final boolean A04(C28861Ph r9) {
        boolean z = false;
        if (!A03()) {
            return false;
        }
        boolean z2 = false;
        if (r9.A11 > 0) {
            z2 = true;
        }
        StringBuilder sb = new StringBuilder("TextMessageStore/isValidMessage/message must have row_id set; key=");
        AnonymousClass1IS r2 = r9.A0z;
        sb.append(r2);
        AnonymousClass009.A0B(sb.toString(), z2);
        if (r9.A08() == 1) {
            z = true;
        }
        StringBuilder sb2 = new StringBuilder("TextMessageStore/isValidMessage/message in main storage; key=");
        sb2.append(r2);
        AnonymousClass009.A0B(sb2.toString(), z);
        return true;
    }
}
