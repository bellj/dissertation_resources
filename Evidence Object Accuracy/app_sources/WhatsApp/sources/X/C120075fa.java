package X;

import com.whatsapp.payments.ui.BrazilPaymentActivity;

/* renamed from: X.5fa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120075fa extends AnonymousClass4UZ {
    public final /* synthetic */ BrazilPaymentActivity A00;

    public C120075fa(BrazilPaymentActivity brazilPaymentActivity) {
        this.A00 = brazilPaymentActivity;
    }

    @Override // X.AnonymousClass4UZ
    public void A00() {
        BrazilPaymentActivity brazilPaymentActivity = this.A00;
        brazilPaymentActivity.A02.A04();
        brazilPaymentActivity.A02 = C117305Zk.A0C(((AbstractActivityC121685jC) brazilPaymentActivity).A0P);
    }
}
