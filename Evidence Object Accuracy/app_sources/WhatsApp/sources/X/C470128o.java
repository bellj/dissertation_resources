package X;

import com.whatsapp.R;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.28o  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C470128o {
    public static final List A00;
    public static final Map A01;

    static {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new C470228p(0, null, R.string.filter_name_none));
        arrayList.add(new C470228p(1, "filter_pop.png", R.string.filter_name_pop));
        arrayList.add(new C470228p(2, "filter_bw.png", R.string.filter_name_bw));
        arrayList.add(new C470228p(3, "filter_cool.png", R.string.filter_name_cool));
        arrayList.add(new C470228p(4, "filter_chrome.png", R.string.filter_name_chrome));
        arrayList.add(new C470228p(5, "filter_film.png", R.string.filter_name_film));
        List<C470228p> unmodifiableList = Collections.unmodifiableList(arrayList);
        A00 = unmodifiableList;
        HashMap hashMap = new HashMap();
        for (C470228p r1 : unmodifiableList) {
            hashMap.put(r1.A02, r1);
        }
        A01 = Collections.unmodifiableMap(hashMap);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0009, code lost:
        if (r3 >= X.C470128o.A00.size()) goto L_0x000b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C470228p A00(int r3) {
        /*
            if (r3 < 0) goto L_0x000b
            java.util.List r0 = X.C470128o.A00
            int r0 = r0.size()
            r2 = 1
            if (r3 < r0) goto L_0x000c
        L_0x000b:
            r2 = 0
        L_0x000c:
            java.lang.String r1 = "Filter ID does not exist: "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r3)
            java.lang.String r0 = r0.toString()
            X.AnonymousClass009.A0B(r0, r2)
            java.util.List r0 = X.C470128o.A00
            java.lang.Object r0 = r0.get(r3)
            X.28p r0 = (X.C470228p) r0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C470128o.A00(int):X.28p");
    }
}
