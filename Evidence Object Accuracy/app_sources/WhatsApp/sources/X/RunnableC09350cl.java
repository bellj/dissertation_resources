package X;

import android.content.Intent;
import android.os.PowerManager;
import androidx.work.impl.background.systemalarm.SystemAlarmService;
import java.util.HashMap;
import java.util.List;
import java.util.WeakHashMap;

/* renamed from: X.0cl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09350cl implements Runnable {
    public final C07620Zm A00;

    public RunnableC09350cl(C07620Zm r1) {
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        boolean z;
        boolean z2;
        C07620Zm r3 = this.A00;
        C06390Tk A00 = C06390Tk.A00();
        String str = C07620Zm.A0A;
        A00.A02(str, "Checking if commands are complete.", new Throwable[0]);
        r3.A01();
        List list = r3.A09;
        synchronized (list) {
            if (r3.A00 != null) {
                C06390Tk.A00().A02(str, String.format("Removing command %s", r3.A00), new Throwable[0]);
                if (((Intent) list.remove(0)).equals(r3.A00)) {
                    r3.A00 = null;
                } else {
                    throw new IllegalStateException("Dequeue-d command is not the first.");
                }
            }
            ExecutorC10610eu r7 = ((C07760a2) r3.A08).A01;
            C07610Zl r0 = r3.A06;
            synchronized (r0.A01) {
                boolean isEmpty = r0.A02.isEmpty();
                z = false;
                if (!isEmpty) {
                    z = true;
                }
            }
            if (!z && list.isEmpty()) {
                synchronized (r7.A00) {
                    z2 = false;
                    if (!r7.A01.isEmpty()) {
                        z2 = true;
                    }
                }
                if (!z2) {
                    C06390Tk.A00().A02(str, "No more commands & intents.", new Throwable[0]);
                    AbstractC11420gG r6 = r3.A01;
                    if (r6 != null) {
                        SystemAlarmService systemAlarmService = (SystemAlarmService) r6;
                        systemAlarmService.A01 = true;
                        C06390Tk.A00().A02(SystemAlarmService.A02, "All commands completed in dispatcher", new Throwable[0]);
                        HashMap hashMap = new HashMap();
                        WeakHashMap weakHashMap = AnonymousClass0RM.A01;
                        synchronized (weakHashMap) {
                            hashMap.putAll(weakHashMap);
                        }
                        for (PowerManager.WakeLock wakeLock : hashMap.keySet()) {
                            if (wakeLock != null && wakeLock.isHeld()) {
                                C06390Tk.A00().A05(AnonymousClass0RM.A00, String.format("WakeLock held for %s", hashMap.get(wakeLock)), new Throwable[0]);
                            }
                        }
                        systemAlarmService.stopSelf();
                    }
                }
            }
            if (!list.isEmpty()) {
                r3.A02();
            }
        }
    }
}
