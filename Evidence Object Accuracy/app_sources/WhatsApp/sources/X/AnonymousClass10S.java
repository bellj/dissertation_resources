package X;

import com.whatsapp.jid.UserJid;
import java.util.Collection;

/* renamed from: X.10S  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass10S extends AbstractC16220oe {
    public AnonymousClass1GB A00;

    public void A05(AbstractC14640lm r3) {
        for (C27131Gd r0 : A01()) {
            r0.A00(r3);
        }
    }

    public void A06(AbstractC14640lm r3) {
        for (C27131Gd r0 : A01()) {
            r0.A01(r3);
        }
    }

    public void A07(UserJid userJid) {
        for (C27131Gd r0 : A01()) {
            r0.A02(userJid);
        }
    }

    public void A08(Collection collection) {
        for (C27131Gd r0 : A01()) {
            r0.A04(collection);
        }
    }
}
