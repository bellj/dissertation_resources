package X;

/* renamed from: X.1WD  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1WD {
    public final /* synthetic */ C65963Lt A00;
    public final /* synthetic */ C16680pa A01;
    public final /* synthetic */ C129625y2 A02;
    public final /* synthetic */ String A03;

    public AnonymousClass1WD(C65963Lt r1, C16680pa r2, C129625y2 r3, String str) {
        this.A01 = r2;
        this.A03 = str;
        this.A02 = r3;
        this.A00 = r1;
    }

    public void A00() {
        C16680pa r4 = this.A01;
        r4.A02.A01 = null;
        String str = this.A03;
        AnonymousClass1WE A01 = r4.A01(str);
        if (A01 == null) {
            this.A02.A00(new IllegalStateException("Bloks metadata should be provided"));
            return;
        }
        C129625y2 r5 = this.A02;
        r4.A01.A0I(new RunnableC76283lO(A01, this.A00, r4, r5, str));
    }
}
