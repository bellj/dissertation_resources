package X;

/* renamed from: X.3Du  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C63993Du {
    public final AnonymousClass1V8 A00;
    public final String A01;

    public C63993Du(AnonymousClass1V8 r9) {
        AnonymousClass1V8.A01(r9, "choice");
        String[] A08 = C13000ix.A08();
        A08[0] = "choice";
        this.A01 = (String) AnonymousClass3JT.A04(null, r9, String.class, C12970iu.A0j(), C12970iu.A0k(), null, A08, false);
        this.A00 = r9;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || C63993Du.class != obj.getClass()) {
            return false;
        }
        return this.A01.equals(((C63993Du) obj).A01);
    }

    public int hashCode() {
        return C12970iu.A08(this.A01, C12970iu.A1b());
    }
}
