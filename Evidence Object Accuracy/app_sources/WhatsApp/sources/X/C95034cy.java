package X;

import com.google.android.exoplayer2.Timeline;
import java.util.List;

/* renamed from: X.4cy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C95034cy {
    public static final C28741Ov A0I = new C28741Ov(C12970iu.A0l());
    public final int A00;
    public final int A01;
    public final long A02;
    public final AnonymousClass3A1 A03;
    public final C94344be A04;
    public final Timeline A05;
    public final C28741Ov A06;
    public final C28741Ov A07;
    public final C100564m7 A08;
    public final AnonymousClass4R7 A09;
    public final List A0A;
    public final boolean A0B;
    public final boolean A0C;
    public final boolean A0D;
    public final boolean A0E;
    public volatile long A0F;
    public volatile long A0G;
    public volatile long A0H;

    public C95034cy(AnonymousClass3A1 r3, C94344be r4, Timeline timeline, C28741Ov r6, C28741Ov r7, C100564m7 r8, AnonymousClass4R7 r9, List list, int i, int i2, long j, long j2, long j3, long j4, boolean z, boolean z2, boolean z3, boolean z4) {
        this.A05 = timeline;
        this.A07 = r6;
        this.A02 = j;
        this.A00 = i;
        this.A03 = r3;
        this.A0B = z;
        this.A08 = r8;
        this.A09 = r9;
        this.A0A = list;
        this.A06 = r7;
        this.A0D = z2;
        this.A01 = i2;
        this.A04 = r4;
        this.A0F = j2;
        this.A0H = j3;
        this.A0G = j4;
        this.A0C = z3;
        this.A0E = z4;
    }

    public static C95034cy A00(AnonymousClass4R7 r22) {
        Timeline timeline = Timeline.A00;
        C28741Ov r4 = A0I;
        return new C95034cy(null, C94344be.A03, timeline, r4, r4, C100564m7.A03, r22, AnonymousClass1Mr.of(), 1, 0, -9223372036854775807L, 0, 0, 0, false, false, false, false);
    }

    public C95034cy A01(int i) {
        Timeline timeline = this.A05;
        C28741Ov r1 = this.A07;
        long j = this.A02;
        AnonymousClass3A1 r12 = this.A03;
        boolean z = this.A0B;
        C100564m7 r13 = this.A08;
        AnonymousClass4R7 r15 = this.A09;
        List list = this.A0A;
        C28741Ov r132 = this.A06;
        boolean z2 = this.A0D;
        return new C95034cy(r12, this.A04, timeline, r1, r132, r13, r15, list, i, this.A01, j, this.A0F, this.A0H, this.A0G, z, z2, this.A0C, this.A0E);
    }

    public C95034cy A02(int i, boolean z) {
        Timeline timeline = this.A05;
        C28741Ov r1 = this.A07;
        long j = this.A02;
        int i2 = this.A00;
        AnonymousClass3A1 r12 = this.A03;
        boolean z2 = this.A0B;
        C100564m7 r14 = this.A08;
        AnonymousClass4R7 r13 = this.A09;
        List list = this.A0A;
        return new C95034cy(r12, this.A04, timeline, r1, this.A06, r14, r13, list, i2, i, j, this.A0F, this.A0H, this.A0G, z2, z, this.A0C, this.A0E);
    }

    public C95034cy A03(AnonymousClass3A1 r40) {
        Timeline timeline = this.A05;
        C28741Ov r1 = this.A07;
        long j = this.A02;
        int i = this.A00;
        boolean z = this.A0B;
        C100564m7 r12 = this.A08;
        AnonymousClass4R7 r15 = this.A09;
        List list = this.A0A;
        C28741Ov r13 = this.A06;
        boolean z2 = this.A0D;
        return new C95034cy(r40, this.A04, timeline, r1, r13, r12, r15, list, i, this.A01, j, this.A0F, this.A0H, this.A0G, z, z2, this.A0C, this.A0E);
    }

    public C95034cy A04(C94344be r41) {
        Timeline timeline = this.A05;
        C28741Ov r1 = this.A07;
        long j = this.A02;
        int i = this.A00;
        AnonymousClass3A1 r12 = this.A03;
        boolean z = this.A0B;
        C100564m7 r15 = this.A08;
        AnonymousClass4R7 r14 = this.A09;
        List list = this.A0A;
        return new C95034cy(r12, r41, timeline, r1, this.A06, r15, r14, list, i, this.A01, j, this.A0F, this.A0H, this.A0G, z, this.A0D, this.A0C, this.A0E);
    }

    public C95034cy A05(Timeline timeline) {
        C28741Ov r1 = this.A07;
        long j = this.A02;
        int i = this.A00;
        AnonymousClass3A1 r12 = this.A03;
        boolean z = this.A0B;
        C100564m7 r13 = this.A08;
        AnonymousClass4R7 r15 = this.A09;
        List list = this.A0A;
        C28741Ov r132 = this.A06;
        boolean z2 = this.A0D;
        return new C95034cy(r12, this.A04, timeline, r1, r132, r13, r15, list, i, this.A01, j, this.A0F, this.A0H, this.A0G, z, z2, this.A0C, this.A0E);
    }

    public C95034cy A06(C28741Ov r40) {
        Timeline timeline = this.A05;
        C28741Ov r1 = this.A07;
        long j = this.A02;
        int i = this.A00;
        AnonymousClass3A1 r12 = this.A03;
        boolean z = this.A0B;
        C100564m7 r15 = this.A08;
        AnonymousClass4R7 r14 = this.A09;
        List list = this.A0A;
        boolean z2 = this.A0D;
        return new C95034cy(r12, this.A04, timeline, r1, r40, r15, r14, list, i, this.A01, j, this.A0F, this.A0H, this.A0G, z, z2, this.A0C, this.A0E);
    }

    public C95034cy A07(C28741Ov r32, C100564m7 r33, AnonymousClass4R7 r34, List list, long j, long j2, long j3) {
        Timeline timeline = this.A05;
        int i = this.A00;
        AnonymousClass3A1 r9 = this.A03;
        boolean z = this.A0B;
        C28741Ov r13 = this.A06;
        boolean z2 = this.A0D;
        return new C95034cy(r9, this.A04, timeline, r32, r13, r33, r34, list, i, this.A01, j2, this.A0F, j3, j, z, z2, this.A0C, this.A0E);
    }

    public C95034cy A08(boolean z) {
        Timeline timeline = this.A05;
        C28741Ov r1 = this.A07;
        long j = this.A02;
        int i = this.A00;
        AnonymousClass3A1 r12 = this.A03;
        boolean z2 = this.A0B;
        C100564m7 r15 = this.A08;
        AnonymousClass4R7 r14 = this.A09;
        List list = this.A0A;
        C28741Ov r122 = this.A06;
        boolean z3 = this.A0D;
        return new C95034cy(r12, this.A04, timeline, r1, r122, r15, r14, list, i, this.A01, j, this.A0F, this.A0H, this.A0G, z2, z3, z, this.A0E);
    }
}
