package X;

import java.util.Collection;
import java.util.Iterator;

/* renamed from: X.03C  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass03C extends AnonymousClass1WU {
    public static final String A0I(String str, String str2, String str3) {
        int i = 0;
        int A03 = AnonymousClass03B.A03(str, str2, 0);
        if (A03 < 0) {
            return str;
        }
        int length = str2.length();
        int i2 = length;
        if (length < 1) {
            i2 = 1;
        }
        int length2 = str.length();
        int length3 = (length2 - length) + str3.length();
        if (length3 >= 0) {
            StringBuilder sb = new StringBuilder(length3);
            do {
                sb.append((CharSequence) str, i, A03);
                sb.append(str3);
                i = A03 + length;
                if (A03 >= length2) {
                    break;
                }
                A03 = AnonymousClass03B.A03(str, str2, A03 + i2);
            } while (A03 > 0);
            sb.append((CharSequence) str, i, length2);
            String obj = sb.toString();
            C16700pc.A0B(obj);
            return obj;
        }
        throw new OutOfMemoryError();
    }

    public static final boolean A0J(CharSequence charSequence) {
        C16700pc.A0E(charSequence, 0);
        if (charSequence.length() != 0) {
            C114075Kc A0A = AnonymousClass03B.A0A(charSequence);
            if (!(A0A instanceof Collection) || !((Collection) A0A).isEmpty()) {
                Iterator it = A0A.iterator();
                while (it.hasNext()) {
                    char charAt = charSequence.charAt(((AnonymousClass5DO) it).A00());
                    if (!Character.isWhitespace(charAt) && !Character.isSpaceChar(charAt)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public static final boolean A0K(String str, String str2) {
        C16700pc.A0E(str, 0);
        C16700pc.A0E(str2, 1);
        return str.endsWith(str2);
    }

    public static final boolean A0L(String str, String str2) {
        C16700pc.A0E(str, 0);
        C16700pc.A0E(str2, 1);
        return str.startsWith(str2);
    }

    public static final boolean A0M(String str, String str2, int i, int i2, boolean z) {
        C16700pc.A0E(str, 0);
        C16700pc.A0E(str2, 2);
        if (!z) {
            return str.regionMatches(0, str2, i, i2);
        }
        return str.regionMatches(z, 0, str2, i, i2);
    }
}
