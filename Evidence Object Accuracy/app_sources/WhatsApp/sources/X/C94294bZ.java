package X;

import java.util.ArrayList;
import java.util.Locale;

/* renamed from: X.4bZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C94294bZ {
    public AnonymousClass4YU A00;

    public static final boolean A00(char c) {
        return c == '<' || c == '>' || c == '=' || c == '~' || c == '!';
    }

    public C94294bZ(String str) {
        AnonymousClass4YU r0 = new AnonymousClass4YU(str);
        this.A00 = r0;
        r0.A05();
        if (this.A00.A08('[')) {
            AnonymousClass4YU r02 = this.A00;
            if (r02.A02.charAt(r02.A00) == ']') {
                AnonymousClass4YU r1 = this.A00;
                r1.A07(1);
                r1.A00--;
                r1.A05();
                if (this.A00.A08('?')) {
                    AnonymousClass4YU r03 = this.A00;
                    r03.A07(1);
                    r03.A05();
                    if (this.A00.A08('(')) {
                        AnonymousClass4YU r04 = this.A00;
                        if (r04.A02.charAt(r04.A00) == ')') {
                            return;
                        }
                    }
                    throw C82843wH.A00(C12960it.A0d(str, C12960it.A0k("Filter must start with '[?(' and end with ')]'. ")));
                }
                throw C82843wH.A00(C12960it.A0d(str, C12960it.A0k("Filter must start with '[?' and end with ']'. ")));
            }
        }
        throw C82843wH.A00(C12960it.A0d(str, C12960it.A0k("Filter must start with '[' and end with ']'. ")));
    }

    public final AbstractC1093851m A01() {
        C82953wS r0;
        AnonymousClass4YU r7 = this.A00;
        r7.A04();
        int i = r7.A01;
        r7.A04();
        if (r7.A08('!')) {
            r7.A06('!');
            r7.A04();
            char charAt = r7.A02.charAt(r7.A01);
            if (!(charAt == '$' || charAt == '@')) {
                return new C82913wO(A01(), AnonymousClass4BO.NOT);
            }
            r7.A01 = i;
        }
        r7.A04();
        if (r7.A08('(')) {
            r7.A06('(');
            AbstractC1093851m A02 = A02();
            r7.A06(')');
            return A02;
        }
        AbstractC94534c0 A03 = A03();
        try {
            r7.A04();
            int i2 = r7.A01;
            CharSequence charSequence = r7.A02;
            if (!A00(charSequence.charAt(i2))) {
                while (true) {
                    int i3 = r7.A01;
                    if (!r7.A0A(i3) || charSequence.charAt(i3) == ' ') {
                        break;
                    }
                    r7.A07(1);
                }
            } else {
                while (true) {
                    int i4 = r7.A01;
                    if (!(r7.A0A(i4) && A00(charSequence.charAt(i4)))) {
                        break;
                    }
                    r7.A07(1);
                }
            }
            String charSequence2 = charSequence.subSequence(i2, r7.A01).toString();
            String upperCase = charSequence2.toUpperCase(Locale.ROOT);
            AnonymousClass4BP[] values = AnonymousClass4BP.values();
            for (AnonymousClass4BP r1 : values) {
                if (r1.operatorString.equals(upperCase)) {
                    return new C82903wN(r1, A03, A03());
                }
            }
            StringBuilder A0k = C12960it.A0k("Filter operator ");
            A0k.append(charSequence2);
            throw C82843wH.A00(C12960it.A0d(" is not supported!", A0k));
        } catch (C82843wH unused) {
            r7.A01 = r7.A01;
            if (!(A03 instanceof C82963wT)) {
                throw C82843wH.A00("Expected path node");
            }
            C82963wT r5 = (C82963wT) A03;
            C82963wT r2 = new C82963wT(r5.A00, true, r5.A02);
            AnonymousClass4BP r12 = AnonymousClass4BP.A06;
            if (r2.A02) {
                r0 = AbstractC116885Xh.A01;
            } else {
                r0 = AbstractC116885Xh.A00;
            }
            return new C82903wN(r12, r2, r0);
        }
    }

    public final AbstractC1093851m A02() {
        AnonymousClass4YU r4;
        int i;
        AnonymousClass4BO r2;
        Object r0;
        int i2;
        AnonymousClass4BO r22;
        ArrayList A0l = C12960it.A0l();
        do {
            ArrayList A0l2 = C12960it.A0l();
            do {
                A0l2.add(A01());
                r4 = this.A00;
                i = r4.A01;
                r2 = AnonymousClass4BO.AND;
            } while (r4.A0B(r2.operatorString));
            r4.A01 = i;
            if (1 == A0l2.size()) {
                r0 = A0l2.get(0);
            } else {
                r0 = new C82913wO(r2, A0l2);
            }
            A0l.add(r0);
            i2 = r4.A01;
            r22 = AnonymousClass4BO.OR;
        } while (r4.A0B(r22.operatorString));
        r4.A01 = i2;
        if (1 == A0l.size()) {
            return (AbstractC1093851m) A0l.get(0);
        }
        return new C82913wO(r22, A0l);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:156:0x0284, code lost:
        if (r1 != false) goto L_0x0286;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:166:0x02a7, code lost:
        r8 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0044, code lost:
        if (r2 != '{') goto L_0x0046;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.AbstractC94534c0 A03() {
        /*
        // Method dump skipped, instructions count: 718
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C94294bZ.A03():X.4c0");
    }
}
