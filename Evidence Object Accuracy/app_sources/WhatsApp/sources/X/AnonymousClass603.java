package X;

import java.util.concurrent.TimeUnit;

/* renamed from: X.603  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass603 {
    public final long A00;
    public final AnonymousClass6F2 A01;
    public final AnonymousClass6F2 A02;
    public final AnonymousClass6F2 A03;
    public final String A04;

    public AnonymousClass603(AnonymousClass6F2 r1, AnonymousClass6F2 r2, AnonymousClass6F2 r3, String str, long j) {
        this.A04 = str;
        this.A02 = r1;
        this.A03 = r2;
        this.A01 = r3;
        this.A00 = j;
    }

    public static AnonymousClass603 A00(AnonymousClass102 r9, AnonymousClass1V8 r10) {
        AnonymousClass1V8 A0F = r10.A0F("source");
        AnonymousClass1V8 A0F2 = r10.A0F("target");
        AnonymousClass1V8 A0F3 = r10.A0F("fee");
        return new AnonymousClass603(AnonymousClass6F2.A00(r9, A0F), AnonymousClass6F2.A00(r9, A0F2), AnonymousClass6F2.A00(r9, A0F3), r10.A0H("id"), r10.A07("expiry-ts"));
    }

    public boolean A01() {
        return TimeUnit.MILLISECONDS.toMicros(System.currentTimeMillis()) < this.A00;
    }
}
