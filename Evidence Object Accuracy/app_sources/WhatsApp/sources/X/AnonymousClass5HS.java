package X;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

/* renamed from: X.5HS  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5HS extends WeakReference {
    public final int A00;

    public AnonymousClass5HS(Throwable th, ReferenceQueue referenceQueue) {
        super(th, referenceQueue);
        this.A00 = System.identityHashCode(th);
    }

    @Override // java.lang.Object
    public final boolean equals(Object obj) {
        if (obj != null && obj.getClass() == AnonymousClass5HS.class) {
            if (this != obj) {
                AnonymousClass5HS r5 = (AnonymousClass5HS) obj;
                if (!(this.A00 == r5.A00 && get() == r5.get())) {
                }
            }
            return true;
        }
        return false;
    }

    @Override // java.lang.Object
    public final int hashCode() {
        return this.A00;
    }
}
