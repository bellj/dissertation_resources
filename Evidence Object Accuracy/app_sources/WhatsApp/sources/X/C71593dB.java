package X;

import java.io.InputStream;
import java.io.OutputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;

/* renamed from: X.3dB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C71593dB implements HttpEntity {
    public final /* synthetic */ C71613dD A00;
    public final /* synthetic */ HttpEntity A01;

    public C71593dB(C71613dD r1, HttpEntity httpEntity) {
        this.A00 = r1;
        this.A01 = httpEntity;
    }

    public void consumeContent() {
        this.A01.consumeContent();
    }

    public InputStream getContent() {
        InputStream content = this.A01.getContent();
        C71613dD r0 = this.A00;
        return new AnonymousClass1oJ(r0.A02, content, Integer.valueOf(r0.A01), C12970iu.A0h());
    }

    public Header getContentEncoding() {
        return this.A01.getContentEncoding();
    }

    public long getContentLength() {
        return this.A01.getContentLength();
    }

    public Header getContentType() {
        return this.A01.getContentType();
    }

    public boolean isChunked() {
        return this.A01.isChunked();
    }

    public boolean isRepeatable() {
        return this.A01.isRepeatable();
    }

    public boolean isStreaming() {
        return this.A01.isStreaming();
    }

    public void writeTo(OutputStream outputStream) {
        HttpEntity httpEntity = this.A01;
        C71613dD r0 = this.A00;
        httpEntity.writeTo(new AnonymousClass23V(r0.A02, outputStream, Integer.valueOf(r0.A01), C12970iu.A0h()));
    }
}
