package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.2En  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C48152En extends C48142Em implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C99634kc();
    public String A00;

    public C48152En(Parcel parcel) {
        super(parcel);
        this.A00 = parcel.readString();
    }

    public C48152En(String str, long j) {
        super(j);
        this.A00 = str;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.A00.equals(((C48152En) obj).A00);
    }

    @Override // java.lang.Object
    public int hashCode() {
        return this.A00.hashCode();
    }

    @Override // X.C48142Em, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.A00);
    }
}
