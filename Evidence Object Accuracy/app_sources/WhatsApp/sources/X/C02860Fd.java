package X;

/* renamed from: X.0Fd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02860Fd extends AbstractC05330Pd {
    public final /* synthetic */ C07740a0 A00;

    @Override // X.AbstractC05330Pd
    public String A01() {
        return "UPDATE workspec SET schedule_requested_at=-1 WHERE state NOT IN (2, 3, 5)";
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C02860Fd(AnonymousClass0QN r1, C07740a0 r2) {
        super(r1);
        this.A00 = r2;
    }
}
