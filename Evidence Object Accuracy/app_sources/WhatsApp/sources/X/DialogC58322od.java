package X;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import androidx.appcompat.widget.Toolbar;
import com.facebook.redex.ViewOnClickCListenerShape8S0100000_I1_2;
import com.whatsapp.KeyboardPopupLayout;
import com.whatsapp.R;
import com.whatsapp.emoji.search.EmojiSearchContainer;
import com.whatsapp.mentions.MentionableEntry;

/* renamed from: X.2od  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class DialogC58322od extends AnonymousClass27U {
    public LinearLayout A00;
    public AbstractC116455Vm A01 = new AnonymousClass3UC(this);
    public KeyboardPopupLayout A02;
    public C15330mx A03;
    public MentionableEntry A04;
    public final AbstractC15710nm A05;
    public final C14820m6 A06;
    public final AnonymousClass19M A07;
    public final C231510o A08;
    public final AnonymousClass193 A09;
    public final C16630pM A0A;
    public final C28861Ph A0B;
    public final C252718t A0C;

    public DialogC58322od(Activity activity, AbstractC15710nm r9, AnonymousClass01d r10, C14830m7 r11, C14820m6 r12, AnonymousClass018 r13, AnonymousClass19M r14, C231510o r15, AnonymousClass193 r16, C16630pM r17, C28861Ph r18, C252718t r19) {
        super(activity, r10, r11, r13, R.layout.edit_message_dialog);
        this.A0B = r18;
        this.A0C = r19;
        this.A05 = r9;
        this.A07 = r14;
        this.A08 = r15;
        this.A09 = r16;
        this.A06 = r12;
        this.A0A = r17;
    }

    @Override // X.AnonymousClass27U, android.app.Dialog
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.edit_message_dialog_title);
        Activity activity = super.A01;
        toolbar.setTitleTextColor(AnonymousClass00T.A00(activity, R.color.white));
        C12970iu.A18(activity, toolbar, R.color.primary_dark);
        AnonymousClass018 r13 = super.A04;
        toolbar.setNavigationIcon(AnonymousClass2GF.A00(activity, r13, R.drawable.ic_back));
        toolbar.setNavigationContentDescription(R.string.back);
        toolbar.setNavigationOnClickListener(new ViewOnClickCListenerShape8S0100000_I1_2(this, 32));
        this.A02 = (KeyboardPopupLayout) findViewById(R.id.conversation_layout);
        this.A00 = (LinearLayout) findViewById(R.id.message_bubble_place_holder);
        this.A04 = (MentionableEntry) findViewById(R.id.entry);
        C28861Ph r1 = this.A0B;
        C60842yj r3 = new C60842yj(activity, null, r1);
        this.A00.addView(r3);
        r3.setEnabled(false);
        C252718t r4 = this.A0C;
        AbstractC15710nm r8 = this.A05;
        AnonymousClass19M r14 = this.A07;
        C231510o r15 = this.A08;
        AnonymousClass01d r11 = super.A02;
        AnonymousClass193 r0 = this.A09;
        C14820m6 r12 = this.A06;
        C16630pM r32 = this.A0A;
        C15270mq r5 = new C15270mq(activity, (ImageButton) findViewById(R.id.emoji_picker_btn), r8, this.A02, this.A04, r11, r12, r13, r14, r15, r0, r32, r4);
        r5.A0C(this.A01);
        C15330mx r7 = new C15330mx(activity, r13, r14, r5, r15, (EmojiSearchContainer) AnonymousClass028.A0D(this.A02, R.id.emoji_search_container), r32);
        this.A03 = r7;
        r7.A00 = new AbstractC14020ki() { // from class: X.56e
            @Override // X.AbstractC14020ki
            public final void APd(C37471mS r33) {
                DialogC58322od.this.A01.APc(r33.A00);
            }
        };
        Window window = getWindow();
        if (window != null) {
            WindowManager.LayoutParams attributes = window.getAttributes();
            attributes.width = -1;
            attributes.gravity = 48;
            window.setAttributes(attributes);
            if (Build.VERSION.SDK_INT >= 21) {
                window.addFlags(Integer.MIN_VALUE);
                window.clearFlags(67108864);
                window.setStatusBarColor(AnonymousClass00T.A00(getContext(), R.color.primary));
            }
        }
        getWindow().setSoftInputMode(5);
        this.A04.setText(r1.A0I());
        this.A04.setSelection(r1.A0I().length());
    }
}
