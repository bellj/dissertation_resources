package X;

import android.content.ContentValues;
import android.database.Cursor;
import com.whatsapp.util.Log;

/* renamed from: X.1RX  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1RX {
    public final C14830m7 A00;
    public final AnonymousClass1RO A01;

    public AnonymousClass1RX(C14830m7 r1, AnonymousClass1RO r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0009, code lost:
        if (r2 > 100) goto L_0x000b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.database.Cursor A00(java.util.Set r6) {
        /*
            r5 = this;
            int r2 = r6.size()
            if (r2 <= 0) goto L_0x000b
            r1 = 100
            r0 = 1
            if (r2 <= r1) goto L_0x000c
        L_0x000b:
            r0 = 0
        L_0x000c:
            X.AnonymousClass009.A0E(r0)
            X.1RO r0 = r5.A01
            X.0op r4 = r0.AHr()
            int r3 = r6.size()
            java.lang.String r0 = "SELECT record, recipient_id, recipient_type, device_id FROM sessions INNER JOIN (SELECT ? AS r, ? AS t, ? AS d"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>(r0)
            r1 = 1
        L_0x0021:
            if (r1 >= r3) goto L_0x002b
            java.lang.String r0 = " UNION ALL SELECT ? AS r, ? AS t, ? AS d"
            r2.append(r0)
            int r1 = r1 + 1
            goto L_0x0021
        L_0x002b:
            java.lang.String r0 = ") AS joined ON joined.r = sessions.recipient_id AND joined.t = sessions.recipient_type AND joined.d = sessions.device_id"
            r2.append(r0)
            java.lang.String r1 = r2.toString()
            java.lang.String[] r0 = X.C31971bP.A00(r6)
            android.database.Cursor r0 = r4.A09(r1, r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1RX.A00(java.util.Set):android.database.Cursor");
    }

    public void A01(C15950oC r6) {
        C16310on A02 = this.A01.A02();
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("axolotl deleted ");
            sb.append((long) A02.A03.A01("sessions", "recipient_id = ? AND recipient_type = ? AND device_id = ? ", r6.A00()));
            sb.append(" sessions with ");
            sb.append(r6);
            Log.i(sb.toString());
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A02(C15950oC r14, byte[] bArr) {
        String str = r14.A02;
        int i = r14.A01;
        int i2 = r14.A00;
        C16310on A02 = this.A01.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            ContentValues contentValues = new ContentValues();
            contentValues.put("record", bArr);
            C16330op r7 = A02.A03;
            if (r7.A00("sessions", contentValues, "recipient_id = ? AND recipient_type = ? AND device_id = ? ", r14.A00()) == 0) {
                long A002 = this.A00.A00() / 1000;
                contentValues.put("recipient_id", str);
                contentValues.put("recipient_type", Integer.valueOf(i));
                contentValues.put("device_id", Integer.valueOf(i2));
                contentValues.put("timestamp", Long.valueOf(A002));
                StringBuilder sb = new StringBuilder();
                sb.append("axolotl inserting new session for ");
                sb.append(r14);
                sb.append(" at ");
                sb.append(A002);
                Log.i(sb.toString());
                r7.A02(contentValues, "sessions");
            }
            A00.A00();
            A00.close();
            A02.close();
            StringBuilder sb2 = new StringBuilder("axolotl stored session for ");
            sb2.append(r14);
            Log.i(sb2.toString());
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public byte[] A03(C15950oC r11) {
        C16310on A01 = this.A01.get();
        try {
            Cursor A08 = A01.A03.A08("sessions", "recipient_id = ? AND recipient_type = ? AND device_id = ? ", null, null, new String[]{"record"}, r11.A00());
            if (!A08.moveToNext()) {
                StringBuilder sb = new StringBuilder();
                sb.append("axolotl cant load a session record for ");
                sb.append(r11);
                Log.i(sb.toString());
                A08.close();
                A01.close();
                return null;
            }
            byte[] blob = A08.getBlob(0);
            A08.close();
            A01.close();
            return blob;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
