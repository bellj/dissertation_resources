package X;

import android.content.Context;

/* renamed from: X.0O1  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0O1 {
    public final Context A00;
    public final AnonymousClass0M4 A01;
    public final AnonymousClass0M5 A02;
    public final AnonymousClass0LV A03;
    public final AnonymousClass0M6 A04;
    public final AbstractC12220hZ A05;

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        if (r3 != null) goto L_0x000e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass0O1(X.AnonymousClass0NU r5) {
        /*
            r4 = this;
            r4.<init>()
            android.content.Context r3 = r5.A02
            r4.A00 = r3
            X.0hZ r2 = r5.A01
            if (r2 != 0) goto L_0x000e
            r1 = 0
            if (r3 == 0) goto L_0x000f
        L_0x000e:
            r1 = 1
        L_0x000f:
            java.lang.String r0 = "Either a non-null context or a base directory path or supplier must be provided."
            if (r1 == 0) goto L_0x0061
            if (r2 != 0) goto L_0x001e
            if (r3 == 0) goto L_0x001e
            X.0bV r2 = new X.0bV
            r2.<init>(r4)
            r5.A01 = r2
        L_0x001e:
            r4.A05 = r2
            X.0LV r0 = r5.A00
            r4.A03 = r0
            java.lang.Class<X.0M4> r1 = X.AnonymousClass0M4.class
            monitor-enter(r1)
            X.0M4 r0 = X.AnonymousClass0M4.A00     // Catch: all -> 0x005e
            if (r0 != 0) goto L_0x0032
            X.0M4 r0 = new X.0M4     // Catch: all -> 0x005e
            r0.<init>()     // Catch: all -> 0x005e
            X.AnonymousClass0M4.A00 = r0     // Catch: all -> 0x005e
        L_0x0032:
            monitor-exit(r1)
            r4.A01 = r0
            java.lang.Class<X.0M5> r1 = X.AnonymousClass0M5.class
            monitor-enter(r1)
            X.0M5 r0 = X.AnonymousClass0M5.A00     // Catch: all -> 0x005b
            if (r0 != 0) goto L_0x0043
            X.0M5 r0 = new X.0M5     // Catch: all -> 0x005b
            r0.<init>()     // Catch: all -> 0x005b
            X.AnonymousClass0M5.A00 = r0     // Catch: all -> 0x005b
        L_0x0043:
            monitor-exit(r1)
            r4.A02 = r0
            java.lang.Class<X.0M6> r1 = X.AnonymousClass0M6.class
            monitor-enter(r1)
            X.0M6 r0 = X.AnonymousClass0M6.A00     // Catch: all -> 0x0058
            if (r0 != 0) goto L_0x0054
            X.0M6 r0 = new X.0M6     // Catch: all -> 0x0058
            r0.<init>()     // Catch: all -> 0x0058
            X.AnonymousClass0M6.A00 = r0     // Catch: all -> 0x0058
        L_0x0054:
            monitor-exit(r1)
            r4.A04 = r0
            return
        L_0x0058:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x005b:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x005e:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x0061:
            java.lang.String r1 = java.lang.String.valueOf(r0)
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0O1.<init>(X.0NU):void");
    }
}
