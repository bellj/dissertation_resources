package X;

import com.whatsapp.Mp4Ops;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;

/* renamed from: X.0va  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20330va extends AbstractC14550lc {
    public static final AnonymousClass00E A0X = new AnonymousClass00E(1, 60, 200);
    public final C21040wk A00;
    public final AbstractC15710nm A01;
    public final C14330lG A02;
    public final C14900mE A03;
    public final C20870wS A04;
    public final Mp4Ops A05;
    public final C15450nH A06;
    public final C18790t3 A07;
    public final C14830m7 A08;
    public final C16590pI A09;
    public final C14950mJ A0A;
    public final C15660nh A0B;
    public final C14850m9 A0C;
    public final C20110vE A0D;
    public final C19940uv A0E;
    public final C14410lO A0F;
    public final AnonymousClass155 A0G;
    public final C17040qA A0H;
    public final C14420lP A0I;
    public final AnonymousClass12J A0J;
    public final C239713s A0K;
    public final C16630pM A0L;
    public final C22600zL A0M;
    public final C15860o1 A0N;
    public final C22590zK A0O;
    public final C26511Dt A0P;
    public final C26481Dq A0Q;
    public final C26521Du A0R;
    public final AbstractC14440lR A0S;
    public final C26501Ds A0T;
    public final C21710xr A0U;
    public final Executor A0V;
    public final Executor A0W;

    public C20330va(C21040wk r5, AbstractC15710nm r6, C14330lG r7, C14900mE r8, C20870wS r9, Mp4Ops mp4Ops, C15450nH r11, C18790t3 r12, C14830m7 r13, C16590pI r14, C14950mJ r15, C15660nh r16, C14850m9 r17, C20110vE r18, C19940uv r19, C14410lO r20, AnonymousClass155 r21, C17040qA r22, C14420lP r23, AnonymousClass12J r24, C239713s r25, C16630pM r26, C22600zL r27, C15860o1 r28, C22590zK r29, C26511Dt r30, C26481Dq r31, C26521Du r32, AbstractC14440lR r33, C26501Ds r34, C21710xr r35) {
        super(new C002601e(null, new AnonymousClass01N() { // from class: X.1tY
            @Override // X.AnonymousClass01N, X.AnonymousClass01H
            public final Object get() {
                ThreadPoolExecutor A8a = AbstractC14440lR.this.A8a("FileDownloadQueue", new LinkedBlockingQueue(), 1, 1, 10, 5);
                A8a.allowCoreThreadTimeOut(true);
                return A8a;
            }
        }));
        this.A08 = r13;
        this.A05 = mp4Ops;
        this.A0C = r17;
        this.A03 = r8;
        this.A01 = r6;
        this.A09 = r14;
        this.A0S = r33;
        this.A02 = r7;
        this.A07 = r12;
        this.A00 = r5;
        this.A06 = r11;
        this.A0T = r34;
        this.A0F = r20;
        this.A0A = r15;
        this.A0M = r27;
        this.A0P = r30;
        this.A04 = r9;
        this.A0K = r25;
        this.A0R = r32;
        this.A0O = r29;
        this.A0U = r35;
        this.A0G = r21;
        this.A0N = r28;
        this.A0I = r23;
        this.A0B = r16;
        this.A0J = r24;
        this.A0H = r22;
        this.A0Q = r31;
        this.A0E = r19;
        this.A0D = r18;
        this.A0L = r26;
        this.A0V = new ExecutorC39691qM(r8);
        this.A0W = new ExecutorC41461tZ(r33);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00ac, code lost:
        if (r73 != 2) goto L_0x00ae;
     */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x008f  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x009c  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x012c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A06(X.AbstractC28771Oy r62, X.C14370lK r63, X.AbstractC14590lg r64, X.AbstractC14590lg r65, java.lang.String r66, java.lang.String r67, java.lang.String r68, java.lang.String r69, java.lang.String r70, java.lang.String r71, byte[] r72, int r73, int r74, int r75, int r76, long r77) {
        /*
        // Method dump skipped, instructions count: 504
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20330va.A06(X.1Oy, X.0lK, X.0lg, X.0lg, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, byte[], int, int, int, int, long):void");
    }
}
