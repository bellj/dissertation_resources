package X;

import android.os.IBinder;
import com.google.android.gms.common.internal.IAccountAccessor;

/* renamed from: X.3ql  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79543ql extends C98394ic implements IAccountAccessor {
    public C79543ql(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.IAccountAccessor");
    }
}
