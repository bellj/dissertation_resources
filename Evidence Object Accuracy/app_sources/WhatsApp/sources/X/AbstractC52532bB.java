package X;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.ViewGroup;

/* renamed from: X.2bB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC52532bB extends ViewGroup {
    public boolean A00 = true;

    public abstract void A02(C91194Qu v);

    public abstract void A03(C91194Qu v, int i);

    public abstract void A04(C91194Qu v, int i, int i2);

    public abstract String getDescriptionOfMountedItems();

    public abstract int getMountItemCount();

    public AbstractC52532bB(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override // android.view.ViewGroup
    public boolean getClipChildren() {
        if (Build.VERSION.SDK_INT < 18) {
            return this.A00;
        }
        return super.getClipChildren();
    }

    @Override // android.view.ViewGroup
    public void setClipChildren(boolean z) {
        if (Build.VERSION.SDK_INT < 18) {
            this.A00 = z;
        }
        super.setClipChildren(z);
    }
}
