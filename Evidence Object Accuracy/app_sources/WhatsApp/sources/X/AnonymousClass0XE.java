package X;

import android.view.MenuItem;

/* renamed from: X.0XE  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0XE implements AbstractC011605p {
    public final /* synthetic */ AnonymousClass08Z A00;

    @Override // X.AbstractC011605p
    public boolean ASh(MenuItem menuItem, AnonymousClass07H r3) {
        return false;
    }

    public AnonymousClass0XE(AnonymousClass08Z r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0017, code lost:
        if (r1 == false) goto L_0x0019;
     */
    @Override // X.AbstractC011605p
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void ASi(X.AnonymousClass07H r5) {
        /*
            r4 = this;
            X.08Z r0 = r4.A00
            android.view.Window$Callback r3 = r0.A00
            X.08e r0 = r0.A01
            X.08d r0 = (X.C017408d) r0
            androidx.appcompat.widget.Toolbar r0 = r0.A09
            androidx.appcompat.widget.ActionMenuView r0 = r0.A0O
            if (r0 == 0) goto L_0x0019
            X.0XQ r0 = r0.A08
            if (r0 == 0) goto L_0x0019
            boolean r1 = r0.A02()
            r0 = 1
            if (r1 != 0) goto L_0x001a
        L_0x0019:
            r0 = 0
        L_0x001a:
            r2 = 108(0x6c, float:1.51E-43)
            if (r0 == 0) goto L_0x0022
            r3.onPanelClosed(r2, r5)
        L_0x0021:
            return
        L_0x0022:
            r1 = 0
            r0 = 0
            boolean r0 = r3.onPreparePanel(r1, r0, r5)
            if (r0 == 0) goto L_0x0021
            r3.onMenuOpened(r2, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0XE.ASi(X.07H):void");
    }
}
