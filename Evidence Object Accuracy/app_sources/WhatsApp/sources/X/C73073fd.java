package X;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;

/* renamed from: X.3fd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73073fd extends Drawable {
    public final /* synthetic */ AnonymousClass1OY A00;

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
    }

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -3;
    }

    @Override // android.graphics.drawable.Drawable
    public boolean isStateful() {
        return true;
    }

    @Override // android.graphics.drawable.Drawable
    public boolean onLevelChange(int i) {
        return true;
    }

    @Override // android.graphics.drawable.Drawable
    public boolean onStateChange(int[] iArr) {
        return true;
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
    }

    public C73073fd(AnonymousClass1OY r1) {
        this.A00 = r1;
    }

    @Override // android.graphics.drawable.Drawable
    public boolean setState(int[] iArr) {
        invalidateSelf();
        return true;
    }
}
