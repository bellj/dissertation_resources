package X;

/* renamed from: X.1XG  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1XG extends AnonymousClass1X4 implements AnonymousClass1XH, AbstractC16400ox, AbstractC16420oz {
    public int A00 = 0;

    public AnonymousClass1XG(C16150oX r10, AnonymousClass1IS r11, AnonymousClass1XG r12, long j) {
        super(r10, r11, (AbstractC16130oV) r12, r12.A0y, j, true);
        this.A00 = r12.A00;
    }

    public AnonymousClass1XG(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 43, j);
    }

    public AnonymousClass1XG(C40851sR r2, AnonymousClass1IS r3, long j, boolean z, boolean z2) {
        super(r3, (byte) 43, j);
        A1E(r2, z, z2);
        A1D(r2);
    }

    @Override // X.AbstractC16420oz
    public void A6k(C39971qq r9) {
        AnonymousClass1G3 r4 = r9.A03;
        C56962mF r0 = ((C27081Fy) r4.A00).A0G;
        if (r0 == null) {
            r0 = C56962mF.A02;
        }
        AnonymousClass1G4 A0T = r0.A0T();
        C27081Fy r02 = ((C56962mF) A0T.A00).A01;
        if (r02 == null) {
            r02 = C27081Fy.A0i;
        }
        AnonymousClass1G4 A0T2 = r02.A0T();
        C40851sR r03 = ((C27081Fy) A0T2.A00).A0f;
        if (r03 == null) {
            r03 = C40851sR.A0O;
        }
        C82373vW r6 = (C82373vW) r03.A0T();
        A1C(r6, r9);
        r6.A03();
        C40851sR r3 = (C40851sR) r6.A00;
        r3.A00 |= C25981Bo.A0F;
        r3.A0N = true;
        A0T2.A03();
        C27081Fy r1 = (C27081Fy) A0T2.A00;
        r1.A0f = (C40851sR) r6.A02();
        r1.A00 |= 256;
        A0T.A03();
        C56962mF r12 = (C56962mF) A0T.A00;
        r12.A01 = (C27081Fy) A0T2.A02();
        r12.A00 |= 1;
        r4.A03();
        C27081Fy r2 = (C27081Fy) r4.A00;
        r2.A0G = (C56962mF) A0T.A02();
        r2.A00 |= 268435456;
    }

    @Override // X.AbstractC16400ox
    public /* bridge */ /* synthetic */ AbstractC15340mz A7M(AnonymousClass1IS r7) {
        long j = this.A0I;
        C16150oX r1 = ((AbstractC16130oV) this).A02;
        AnonymousClass009.A05(r1);
        return new AnonymousClass1XG(r1, r7, this, j);
    }

    @Override // X.AnonymousClass1XH
    public int AHc() {
        return this.A00;
    }

    @Override // X.AnonymousClass1XH
    public void Ad8(int i) {
        this.A00 = i;
    }
}
