package X;

/* renamed from: X.1Bu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26041Bu {
    public final C16510p9 A00;
    public final C19990v2 A01;
    public final C245215v A02;
    public final C18460sU A03;
    public final C16490p7 A04;
    public final C22810zg A05;
    public final C19770ue A06;
    public final C18470sV A07;

    public C26041Bu(C16510p9 r1, C19990v2 r2, C245215v r3, C18460sU r4, C16490p7 r5, C22810zg r6, C19770ue r7, C18470sV r8) {
        this.A03 = r4;
        this.A00 = r1;
        this.A01 = r2;
        this.A07 = r8;
        this.A05 = r6;
        this.A06 = r7;
        this.A04 = r5;
        this.A02 = r3;
    }

    public void A00() {
        C18460sU r1 = this.A03;
        r1.A04.clear();
        r1.A03.clear();
        C22810zg r3 = this.A05;
        r3.A00.A06(-1);
        C006202y r12 = r3.A06.A00;
        synchronized (r12) {
            r12.A06(-1);
        }
        C22830zi r13 = r3.A05;
        ((AbstractC20610w2) r13.A02).A00.A06(-1);
        ((AbstractC20610w2) r13.A01).A00.A06(-1);
        this.A02.A01.clear();
        this.A06.A01();
        C16490p7 r14 = this.A04;
        r14.A04();
        r14.A01 = false;
        C19990v2 r15 = this.A01;
        synchronized (r15) {
            if (r15.A00) {
                synchronized (r15) {
                    r15.A0B().clear();
                    r15.A00 = false;
                }
            }
        }
        C16510p9 r16 = this.A00;
        synchronized (r16) {
            r16.A06.clear();
            r16.A07.clear();
        }
        C18470sV r17 = this.A07;
        if (r17.A07 != null) {
            r17.A07.clear();
        }
    }
}
