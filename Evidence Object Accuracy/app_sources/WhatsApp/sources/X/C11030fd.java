package X;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/* renamed from: X.0fd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C11030fd extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ ClassLoader $classLoader;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C11030fd(ClassLoader classLoader) {
        super(0);
        this.$classLoader = classLoader;
    }

    /* renamed from: A00 */
    public final Boolean AJ3() {
        boolean z = false;
        Method method = this.$classLoader.loadClass("androidx.window.extensions.WindowExtensions").getMethod("getWindowLayoutComponent", new Class[0]);
        Class<?> loadClass = this.$classLoader.loadClass("androidx.window.extensions.layout.WindowLayoutComponent");
        C16700pc.A0B(method);
        if (Modifier.isPublic(method.getModifiers())) {
            C16700pc.A0B(loadClass);
            if (method.getReturnType().equals(loadClass)) {
                z = true;
            }
        }
        return Boolean.valueOf(z);
    }
}
