package X;

import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.Property;

/* renamed from: X.0Aq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C02260Aq extends Property {
    public Rect A00 = new Rect();

    public C02260Aq() {
        super(PointF.class, "boundsOrigin");
    }

    @Override // android.util.Property
    public /* bridge */ /* synthetic */ Object get(Object obj) {
        Rect rect = this.A00;
        ((Drawable) obj).copyBounds(rect);
        return new PointF((float) rect.left, (float) rect.top);
    }

    @Override // android.util.Property
    public /* bridge */ /* synthetic */ void set(Object obj, Object obj2) {
        Drawable drawable = (Drawable) obj;
        PointF pointF = (PointF) obj2;
        Rect rect = this.A00;
        drawable.copyBounds(rect);
        rect.offsetTo(Math.round(pointF.x), Math.round(pointF.y));
        drawable.setBounds(rect);
    }
}
