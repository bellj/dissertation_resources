package X;

import android.os.Handler;
import android.os.Looper;
import com.facebook.redex.RunnableBRunnable0Shape7S0200000_I0_7;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.util.Log;
import com.whatsapp.wamsys.JniBridge;

/* renamed from: X.13f  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C238413f {
    public final Handler A00 = new Handler(Looper.getMainLooper());
    public final C15570nT A01;
    public final C20870wS A02;
    public final C20670w8 A03;
    public final AnonymousClass13T A04;
    public final C237112s A05;
    public final C14830m7 A06;
    public final C15990oG A07;
    public final C18240s8 A08;
    public final C15650ng A09;
    public final C15600nX A0A;
    public final C22830zi A0B;
    public final C22810zg A0C;
    public final C21400xM A0D;
    public final C22130yZ A0E;
    public final C18770sz A0F;
    public final C14850m9 A0G;
    public final C14840m8 A0H;
    public final C20740wF A0I;
    public final C20660w7 A0J;
    public final C17230qT A0K;
    public final C239913u A0L;
    public final C20780wJ A0M;
    public final JniBridge A0N;

    public C238413f(C15570nT r3, C20870wS r4, C20670w8 r5, AnonymousClass13T r6, C237112s r7, C14830m7 r8, C15990oG r9, C18240s8 r10, C15650ng r11, C15600nX r12, C22830zi r13, C22810zg r14, C21400xM r15, C22130yZ r16, C18770sz r17, C14850m9 r18, C14840m8 r19, C20740wF r20, C20660w7 r21, C17230qT r22, C239913u r23, C20780wJ r24, JniBridge jniBridge) {
        this.A06 = r8;
        this.A0G = r18;
        this.A01 = r3;
        this.A0N = jniBridge;
        this.A0J = r21;
        this.A03 = r5;
        this.A02 = r4;
        this.A0C = r14;
        this.A08 = r10;
        this.A09 = r11;
        this.A0L = r23;
        this.A0H = r19;
        this.A07 = r9;
        this.A0F = r17;
        this.A0I = r20;
        this.A04 = r6;
        this.A0K = r22;
        this.A0D = r15;
        this.A0E = r16;
        this.A0B = r13;
        this.A0M = r24;
        this.A0A = r12;
        this.A05 = r7;
    }

    public byte[] A00(DeviceJid deviceJid, AnonymousClass1IS r13, C29211Rh r14, C29211Rh r15, byte[] bArr, byte[] bArr2, byte b, int i, int i2, boolean z) {
        StringBuilder sb;
        String str;
        this.A08.A00();
        C15950oC A02 = C15940oB.A02(deviceJid);
        StringBuilder sb2 = new StringBuilder("axolotl checking sessions for ");
        sb2.append(A02);
        sb2.append(" due to retry receipt for ");
        sb2.append(r13);
        Log.i(sb2.toString());
        if (!(bArr2 == null || bArr == null || r14 == null || r15 == null)) {
            if (z) {
                C15990oG r1 = this.A07;
                if (!r1.A0g(A02)) {
                    sb = new StringBuilder();
                    str = "axolotl Not processing keys from the receipt, missing session for ";
                } else if (r1.A0G(A02).A01.A00.A03 != i2) {
                    sb = new StringBuilder();
                    str = "Not processing keys from the receipt, registrationId does not match for ";
                }
                sb.append(str);
                sb.append(r13);
                Log.i(sb.toString());
            }
            StringBuilder sb3 = new StringBuilder("axolotl processing keys from the receipt for jid:");
            sb3.append(deviceJid);
            Log.i(sb3.toString());
            int A07 = this.A07.A07(C15940oB.A02(deviceJid), r15, r14, bArr2, bArr, b);
            if (A07 == 0) {
                this.A00.post(new RunnableBRunnable0Shape7S0200000_I0_7(this, 19, deviceJid));
            } else {
                StringBuilder sb4 = new StringBuilder("Error received from SignalCoordinator; status=");
                sb4.append(A07);
                Log.e(sb4.toString());
            }
        }
        C15990oG r5 = this.A07;
        if (!r5.A0g(A02)) {
            return null;
        }
        C31311aL r12 = r5.A0G(A02).A01;
        byte[] A04 = r12.A00.A05.A04();
        if (r12.A00.A03 != i2) {
            StringBuilder sb5 = new StringBuilder("axolotl deleting session due to registration id change for ");
            sb5.append(r13);
            Log.i(sb5.toString());
            r5.A0I.A00();
            r5.A0H(A02);
            r5.A0W(A02);
            return A04;
        } else if (i > 2 && r5.A0i(A02, r13)) {
            StringBuilder sb6 = new StringBuilder("axolotl will wait to send ");
            sb6.append(r13);
            sb6.append(" until a new prekey has been fetched");
            Log.i(sb6.toString());
            return A04;
        } else if (i != 2) {
            return null;
        } else {
            StringBuilder sb7 = new StringBuilder("axolotl will record the base key used to send ");
            sb7.append(r13);
            Log.i(sb7.toString());
            r5.A0Z(A02, r13, A04);
            return null;
        }
    }
}
