package X;

import android.view.ViewTreeObserver;
import androidx.fragment.app.ListFragment;
import com.facebook.redex.IDxLAdapterShape1S0100000_2_I1;
import com.whatsapp.status.StatusesFragment;

/* renamed from: X.3Nm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ViewTreeObserver$OnPreDrawListenerC66413Nm implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ StatusesFragment A00;

    public ViewTreeObserver$OnPreDrawListenerC66413Nm(StatusesFragment statusesFragment) {
        this.A00 = statusesFragment;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        StatusesFragment statusesFragment = this.A00;
        ListFragment.A00(statusesFragment);
        C12980iv.A1G(((ListFragment) statusesFragment).A04, this);
        statusesFragment.A1H(new IDxLAdapterShape1S0100000_2_I1(this, 17), true);
        return false;
    }
}
