package X;

/* renamed from: X.4Wm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92584Wm {
    public long A00;
    public final C44691zO A01;

    public C92584Wm(C44691zO r1, long j) {
        this.A01 = r1;
        this.A00 = j;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (!(obj instanceof C92584Wm)) {
                return false;
            }
            C92584Wm r7 = (C92584Wm) obj;
            if (this.A00 != r7.A00 || !this.A01.equals(r7.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] A1a = C12980iv.A1a();
        A1a[0] = this.A01;
        return C12960it.A06(Long.valueOf(this.A00), A1a);
    }
}
