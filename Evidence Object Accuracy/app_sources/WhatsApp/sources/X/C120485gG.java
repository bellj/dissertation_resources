package X;

import android.content.Context;
import com.whatsapp.util.Log;

/* renamed from: X.5gG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120485gG extends C126705tJ {
    public final Context A00;
    public final C14900mE A01;
    public final AnonymousClass102 A02;
    public final C17220qS A03;
    public final AnonymousClass68Z A04;
    public final C18650sn A05;
    public final C121265hX A06;
    public final C18590sh A07;

    public C120485gG(Context context, C14900mE r3, AnonymousClass102 r4, C17220qS r5, AnonymousClass68Z r6, C1308460e r7, C18650sn r8, C18610sj r9, C121265hX r10, C18590sh r11) {
        super(r7.A04, r9);
        this.A00 = context;
        this.A01 = r3;
        this.A03 = r5;
        this.A07 = r11;
        this.A02 = r4;
        this.A04 = r6;
        this.A05 = r8;
        this.A06 = r10;
    }

    public void A00(AnonymousClass1ZR r21, AnonymousClass1ZR r22, AnonymousClass6Ln r23) {
        String str;
        String str2;
        String str3;
        Log.i("PAY: verifyPaymentVpa called");
        String A01 = this.A07.A01();
        boolean z = !AnonymousClass1ZS.A02(r22);
        String str4 = null;
        if (z) {
            str = (String) C117295Zj.A0R(r22);
            if (((String) r22.A00).length() == 10) {
                str3 = "mobile_number";
            } else {
                str3 = "numeric_id";
            }
            str2 = "2";
        } else {
            str4 = (String) C117295Zj.A0R(r21);
            str = null;
            str2 = null;
            str3 = null;
        }
        C17220qS r1 = this.A03;
        String A012 = r1.A01();
        C130515zX r13 = new C130515zX(new C128665wT(A012), str4, A01, str, str2, str3);
        this.A06.A02(185474822, "in_upi_get_vpa_name_tag");
        r1.A0D(new C120855gr(this.A00, this.A01, r21, r22, r23, this.A05, super.A00, this, z), r13.A00, A012, 204, 0);
    }
}
