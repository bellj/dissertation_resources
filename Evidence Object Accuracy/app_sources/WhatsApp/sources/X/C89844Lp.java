package X;

import android.content.Context;
import android.os.Handler;

/* renamed from: X.4Lp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C89844Lp {
    public final Context A00;
    public final RunnableC72943fQ A01;

    public C89844Lp(Context context, Handler handler, AbstractC115045Pq r4) {
        this.A00 = context.getApplicationContext();
        this.A01 = new RunnableC72943fQ(handler, r4, this);
    }
}
