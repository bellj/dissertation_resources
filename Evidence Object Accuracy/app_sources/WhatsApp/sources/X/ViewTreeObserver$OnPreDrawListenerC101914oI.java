package X;

import android.view.ViewTreeObserver;
import com.whatsapp.registration.ChangeNumberNotifyContacts;

/* renamed from: X.4oI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnPreDrawListenerC101914oI implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ ChangeNumberNotifyContacts A00;

    public ViewTreeObserver$OnPreDrawListenerC101914oI(ChangeNumberNotifyContacts changeNumberNotifyContacts) {
        this.A00 = changeNumberNotifyContacts;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        ChangeNumberNotifyContacts changeNumberNotifyContacts = this.A00;
        C12980iv.A1G(changeNumberNotifyContacts.A08, this);
        changeNumberNotifyContacts.A2e();
        return false;
    }
}
