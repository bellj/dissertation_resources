package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.1YA  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1YA {
    public final long A00;
    public final long A01;
    public final UserJid A02;

    public AnonymousClass1YA(UserJid userJid, long j, long j2) {
        this.A02 = userJid;
        this.A00 = j;
        this.A01 = j2;
    }
}
