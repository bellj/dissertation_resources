package X;

/* renamed from: X.008  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass008 implements AnonymousClass005 {
    public final AnonymousClass007 A00;
    public final Object A01 = new Object();
    public volatile Object A02;

    public AnonymousClass008(AnonymousClass007 r2) {
        this.A00 = r2;
    }

    @Override // X.AnonymousClass005
    public Object generatedComponent() {
        if (this.A02 == null) {
            synchronized (this.A01) {
                if (this.A02 == null) {
                    AnonymousClass007 r0 = this.A00;
                    AnonymousClass01W r2 = new AnonymousClass01W();
                    r2.A0C = new AnonymousClass01X(r0.A00);
                    this.A02 = r2.A00();
                }
            }
        }
        return this.A02;
    }
}
