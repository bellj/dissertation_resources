package X;

import java.util.Iterator;
import java.util.NavigableMap;
import java.util.NavigableSet;

/* renamed from: X.3t0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C80813t0 extends AbstractC80933tC<K, V>.SortedKeySet implements NavigableSet<K> {
    public final /* synthetic */ AbstractC80933tC this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C80813t0(AbstractC80933tC r1, NavigableMap navigableMap) {
        super(r1, navigableMap);
        this.this$0 = r1;
    }

    @Override // java.util.NavigableSet
    public Object ceiling(Object obj) {
        return sortedMap().ceilingKey(obj);
    }

    @Override // java.util.NavigableSet
    public Iterator descendingIterator() {
        return descendingSet().iterator();
    }

    @Override // java.util.NavigableSet
    public NavigableSet descendingSet() {
        return new C80813t0(this.this$0, sortedMap().descendingMap());
    }

    @Override // java.util.NavigableSet
    public Object floor(Object obj) {
        return sortedMap().floorKey(obj);
    }

    @Override // java.util.NavigableSet, java.util.SortedSet
    public NavigableSet headSet(Object obj) {
        return headSet(obj, false);
    }

    @Override // java.util.NavigableSet
    public NavigableSet headSet(Object obj, boolean z) {
        return new C80813t0(this.this$0, sortedMap().headMap(obj, z));
    }

    @Override // java.util.NavigableSet
    public Object higher(Object obj) {
        return sortedMap().higherKey(obj);
    }

    @Override // java.util.NavigableSet
    public Object lower(Object obj) {
        return sortedMap().lowerKey(obj);
    }

    @Override // java.util.NavigableSet
    public Object pollFirst() {
        return AnonymousClass1I4.pollNext(iterator());
    }

    @Override // java.util.NavigableSet
    public Object pollLast() {
        return AnonymousClass1I4.pollNext(descendingIterator());
    }

    public NavigableMap sortedMap() {
        return (NavigableMap) super.sortedMap();
    }

    @Override // java.util.NavigableSet, java.util.SortedSet
    public NavigableSet subSet(Object obj, Object obj2) {
        return subSet(obj, true, obj2, false);
    }

    @Override // java.util.NavigableSet
    public NavigableSet subSet(Object obj, boolean z, Object obj2, boolean z2) {
        return new C80813t0(this.this$0, sortedMap().subMap(obj, z, obj2, z2));
    }

    @Override // java.util.NavigableSet, java.util.SortedSet
    public NavigableSet tailSet(Object obj) {
        return tailSet(obj, true);
    }

    @Override // java.util.NavigableSet
    public NavigableSet tailSet(Object obj, boolean z) {
        return new C80813t0(this.this$0, sortedMap().tailMap(obj, z));
    }
}
