package X;

import com.whatsapp.jid.Jid;

/* renamed from: X.2QD  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2QD extends AnonymousClass2PA {
    public final AnonymousClass2QE A00;

    public AnonymousClass2QD(Jid jid, AnonymousClass2QE r2, String str, long j) {
        super(jid, str, j);
        this.A00 = r2;
    }
}
