package X;

import java.util.Comparator;

/* renamed from: X.0eP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C10310eP implements Comparator {
    @Override // java.util.Comparator
    public int compare(Object obj, Object obj2) {
        C05220Os r5 = (C05220Os) obj;
        C05220Os r6 = (C05220Os) obj2;
        return ((((r6.A03 - r6.A06) + 1) * ((r6.A02 - r6.A05) + 1)) * ((r6.A01 - r6.A04) + 1)) - ((((r5.A03 - r5.A06) + 1) * ((r5.A02 - r5.A05) + 1)) * ((r5.A01 - r5.A04) + 1));
    }
}
