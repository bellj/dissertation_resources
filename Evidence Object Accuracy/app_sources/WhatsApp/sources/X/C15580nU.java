package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.Jid;

/* renamed from: X.0nU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15580nU extends GroupJid implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C100044lH();

    @Override // com.whatsapp.jid.Jid
    public String getServer() {
        return "g.us";
    }

    @Override // com.whatsapp.jid.Jid
    public int getType() {
        return 1;
    }

    public C15580nU(Parcel parcel) {
        super(parcel);
    }

    public C15580nU(String str) {
        super(str);
        String substring;
        int length;
        char charAt;
        char charAt2;
        int indexOf = str.indexOf(45);
        if (indexOf == -1) {
            int length2 = str.length();
            if (length2 >= 1 && length2 <= 20 && (charAt2 = str.charAt(0)) >= '1' && charAt2 <= '9') {
                int i = 1;
                while (i < length2) {
                    char charAt3 = str.charAt(i);
                    i = (charAt3 >= '0' && charAt3 <= '9') ? i + 1 : i;
                }
                return;
            }
        } else if (indexOf != 0 && indexOf != str.length() && C27631Ih.A03(str.substring(0, indexOf)) && (length = (substring = str.substring(indexOf + 1)).length()) == 10 && (charAt = substring.charAt(0)) >= '1' && charAt <= '9') {
            int i2 = 1;
            do {
                char charAt4 = substring.charAt(i2);
                if (charAt4 >= '0' && charAt4 <= '9') {
                    i2++;
                }
            } while (i2 < length);
            return;
        }
        throw new AnonymousClass1MW(str);
    }

    public static C15580nU A02(Jid jid) {
        if (jid instanceof C15580nU) {
            return (C15580nU) jid;
        }
        return null;
    }

    public static C15580nU A03(String str) {
        Jid jid = Jid.get(str);
        if (jid instanceof C15580nU) {
            return (C15580nU) jid;
        }
        throw new AnonymousClass1MW(str);
    }

    public static C15580nU A04(String str) {
        C15580nU r0 = null;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            r0 = A03(str);
            return r0;
        } catch (AnonymousClass1MW unused) {
            return r0;
        }
    }

    @Override // com.whatsapp.jid.Jid
    public String getObfuscatedString() {
        StringBuilder sb = new StringBuilder();
        sb.append(AnonymousClass1US.A02(15, this.user));
        sb.append('@');
        sb.append("g.us");
        return sb.toString();
    }
}
