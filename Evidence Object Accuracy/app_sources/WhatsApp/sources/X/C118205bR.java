package X;

import com.whatsapp.payments.ui.IndiaUpiSendPaymentToVpaFragment;

/* renamed from: X.5bR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118205bR extends AnonymousClass0Yo {
    public final /* synthetic */ IndiaUpiSendPaymentToVpaFragment A00;

    public C118205bR(IndiaUpiSendPaymentToVpaFragment indiaUpiSendPaymentToVpaFragment) {
        this.A00 = indiaUpiSendPaymentToVpaFragment;
    }

    @Override // X.AnonymousClass0Yo, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.isAssignableFrom(C118165bN.class)) {
            return new C118165bN(this.A00.A0O);
        }
        throw C12970iu.A0f("Invalid viewModel for IndiaUpiSendToVpaViewModel");
    }
}
