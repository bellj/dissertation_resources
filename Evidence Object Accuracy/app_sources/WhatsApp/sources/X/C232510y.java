package X;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import com.whatsapp.util.Log;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.10y  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C232510y {
    public final C21910yB A00;

    public C232510y(C21910yB r1) {
        this.A00 = r1;
    }

    public Long A00(String str) {
        Long l;
        C16310on A01 = this.A00.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT * FROM collection_versions WHERE collection_name = ?", new String[]{str});
            if (A09 == null || !A09.moveToFirst()) {
                l = null;
            } else {
                l = Long.valueOf(A09.getLong(A09.getColumnIndexOrThrow("version")));
            }
            if (A09 != null) {
                A09.close();
            }
            A01.close();
            return l;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public Map A01() {
        HashMap hashMap = new HashMap();
        C16310on A01 = this.A00.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT collection_name, version FROM collection_versions", null);
            while (A09.moveToNext()) {
                hashMap.put(A09.getString(A09.getColumnIndexOrThrow("collection_name")), Long.valueOf(A09.getLong(A09.getColumnIndexOrThrow("version"))));
            }
            A09.close();
            A01.close();
            return hashMap;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A02(String str) {
        C16310on A02 = this.A00.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            A02.A03.A0C("UPDATE collection_versions SET dirty_version = -1 WHERE collection_name = ? AND dirty_version = 0", new String[]{str});
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final void A03(String str) {
        C16310on A02 = this.A00.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            AnonymousClass1YE A0A = A02.A03.A0A("INSERT OR IGNORE INTO collection_versions (collection_name, version, lt_hash, dirty_version) VALUES (?, ?, ?, ?)");
            A0A.A02(1, str);
            A0A.A01(2, 0);
            SQLiteStatement sQLiteStatement = A0A.A00;
            sQLiteStatement.bindNull(3);
            A0A.A01(4, -1);
            sQLiteStatement.executeInsert();
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A04(String str, long j) {
        boolean z = false;
        if (j >= 0) {
            z = true;
        }
        AnonymousClass009.A0F(z);
        C16310on A02 = this.A00.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            A03(str);
            AnonymousClass1YE A0A = A02.A03.A0A("UPDATE collection_versions SET dirty_version = ?  WHERE collection_name = ? ");
            A0A.A01(1, j);
            A0A.A02(2, str);
            if (((long) A0A.A00.executeUpdateDelete()) <= 0) {
                StringBuilder sb = new StringBuilder();
                sb.append("SyncDbStore/updateCollectionDirtyVersion failed for collection: ");
                sb.append(str);
                sb.append(", dirtyVersion: ");
                sb.append(j);
                Log.e(sb.toString());
            }
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A05(String str, byte[] bArr, long j) {
        C16310on A02 = this.A00.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            A03(str);
            AnonymousClass1YE A0A = A02.A03.A0A("UPDATE collection_versions SET version = ?  , lt_hash = ?  WHERE collection_name = ? ");
            A0A.A01(1, j);
            if (bArr != null) {
                A0A.A00.bindBlob(2, bArr);
            }
            A0A.A02(3, str);
            if (((long) A0A.A00.executeUpdateDelete()) <= 0) {
                StringBuilder sb = new StringBuilder();
                sb.append("SyncDbStore/updateCollectionVersion failed for collection: ");
                sb.append(str);
                sb.append(", version: ");
                sb.append(j);
                sb.append(", ltHash: ");
                sb.append(Arrays.toString(bArr));
                Log.e(sb.toString());
            }
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
