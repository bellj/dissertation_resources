package X;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.ViewOnClickCListenerShape4S0200000_I0;

/* renamed from: X.2z8  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2z8 extends C53632el {
    public final Resources A00;
    public final LayoutInflater A01;
    public final C15610nY A02;

    public AnonymousClass2z8(Context context, C15570nT r12, C15550nR r13, C15610nY r14, AnonymousClass1J1 r15, AnonymousClass1OX r16, AbstractC13890kV r17, C15650ng r18, ViewOnClickCListenerShape4S0200000_I0 viewOnClickCListenerShape4S0200000_I0) {
        super(context, r12, r13, r15, r16, r17, r18, viewOnClickCListenerShape4S0200000_I0);
        this.A01 = LayoutInflater.from(context);
        this.A00 = context.getResources();
        this.A02 = r14;
    }

    @Override // X.C53632el, X.AnonymousClass0BR, android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        View inflate;
        String A0B;
        if (view != null) {
            inflate = view;
        } else {
            inflate = this.A01.inflate(R.layout.kept_by_bubble_container, viewGroup, false);
        }
        ViewGroup A0P = C12980iv.A0P(inflate, R.id.chat_bubble_container);
        TextView A0J = C12960it.A0J(inflate, R.id.kept_by_footer_tv);
        if (A0P == null || A0J == null) {
            return super.getView(i, view, viewGroup);
        }
        View view2 = super.getView(i, A0P.getChildAt(0), viewGroup);
        if (view == null) {
            A0P.addView(view2);
        }
        AbstractC15340mz A05 = getItem(i);
        AnonymousClass009.A05(A05);
        C30311Wx r9 = A05.A18;
        if (r9 != null && !r9.A0z.A02) {
            Resources resources = this.A00;
            Object[] A1b = C12970iu.A1b();
            C15550nR r1 = ((C53632el) this).A02;
            C15610nY r2 = this.A02;
            UserJid A0C = r9.A0C();
            if (A0C == null) {
                A0B = null;
            } else {
                A0B = r2.A0B(r1.A0B(A0C), C12990iw.A04(C15380n4.A0J(A05.A0z.A00) ? 1 : 0), false);
            }
            A0J.setText(C12990iw.A0o(resources, A0B, A1b, 0, R.string.kic_kept_message_info_keeper));
        }
        return inflate;
    }
}
