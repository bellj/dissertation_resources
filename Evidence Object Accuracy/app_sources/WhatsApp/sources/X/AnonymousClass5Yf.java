package X;

/* renamed from: X.5Yf  reason: invalid class name */
/* loaded from: classes3.dex */
public interface AnonymousClass5Yf extends AnonymousClass2BY {
    void A5r(int i);

    long AFf();

    long AFo();

    int AZ0(byte[] bArr, int i, int i2);

    void AZ4(byte[] bArr, int i, int i2);

    boolean AZ5(byte[] bArr, int i, int i2, boolean z);

    boolean AZu(byte[] bArr, int i, int i2, boolean z);

    void Aaj();

    int Ae1(int i);

    void Ae3(int i);

    long getLength();

    @Override // X.AnonymousClass2BY
    int read(byte[] bArr, int i, int i2);

    void readFully(byte[] bArr, int i, int i2);
}
