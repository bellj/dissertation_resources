package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.5pu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class EnumC124665pu extends Enum implements AnonymousClass5UX {
    public static final /* synthetic */ EnumC124665pu[] A00;
    public static final EnumC124665pu A01;
    public static final EnumC124665pu A02;
    public static final EnumC124665pu A03;
    public static final EnumC124665pu A04;
    public static final EnumC124665pu A05;
    public static final EnumC124665pu A06;
    public static final EnumC124665pu A07;
    public static final EnumC124665pu A08;
    public static final EnumC124665pu A09;
    public static final EnumC124665pu A0A;
    public static final EnumC124665pu A0B;
    public static final EnumC124665pu A0C;
    public static final EnumC124665pu A0D;
    public static final EnumC124665pu A0E;
    public static final EnumC124665pu A0F;
    public static final EnumC124665pu A0G;
    public final String fieldName;

    public static EnumC124665pu valueOf(String str) {
        return (EnumC124665pu) Enum.valueOf(EnumC124665pu.class, str);
    }

    public static EnumC124665pu[] values() {
        return (EnumC124665pu[]) A00.clone();
    }

    static {
        EnumC124665pu r15 = new EnumC124665pu("CREDENTIAL_ID", "credential_id", 0);
        A06 = r15;
        EnumC124665pu r21 = new EnumC124665pu("COUNTRY", "country", 1);
        A04 = r21;
        EnumC124665pu r20 = new EnumC124665pu("READABLE_NAME", "readable_name", 2);
        A0D = r20;
        EnumC124665pu r19 = new EnumC124665pu("ISSUER_NAME", "issuer_name", 3);
        A08 = r19;
        EnumC124665pu r18 = new EnumC124665pu("TYPE", "type", 4);
        A0E = r18;
        EnumC124665pu r13 = new EnumC124665pu("CARD_NETWORK", "card_network", 5);
        A03 = r13;
        EnumC124665pu r12 = new EnumC124665pu("CREATION_TIME_MILLIS", "creation_time_millis", 6);
        A05 = r12;
        EnumC124665pu r11 = new EnumC124665pu("UPDATED_TIME_MILLIS", "updated_time_millis", 7);
        A0F = r11;
        EnumC124665pu r10 = new EnumC124665pu("IS_DEFAULT_P2P_DEBIT", "is_default_p2p_debit", 8);
        A0C = r10;
        EnumC124665pu r9 = new EnumC124665pu("IS_DEFAULT_P2P_CREDIT", "is_default_p2p_credit", 9);
        A0B = r9;
        EnumC124665pu r8 = new EnumC124665pu("IS_DEFAULT_P2M_DEBIT", "is_default_p2m_debit", 10);
        A0A = r8;
        EnumC124665pu r7 = new EnumC124665pu("IS_DEFAULT_P2M_CREDIT", "is_default_p2m_credit", 11);
        A09 = r7;
        EnumC124665pu r6 = new EnumC124665pu("BALANCE", "balance", 12);
        A01 = r6;
        EnumC124665pu r5 = new EnumC124665pu("BALANCE_TIME_MILLIS", "balance_time_millis", 13);
        A02 = r5;
        EnumC124665pu r4 = new EnumC124665pu("ICON_BLOB", "icon_blob", 14);
        A07 = r4;
        EnumC124665pu r3 = new EnumC124665pu("WAS_PIN_EDUCATION_SHOWN", "was_pin_education_shown", 15);
        A0G = r3;
        EnumC124665pu r16 = new EnumC124665pu("INDIA_UPI_ADDITIONAL_DATA", "india_upi_additional_data", 16);
        EnumC124665pu[] r14 = new EnumC124665pu[17];
        C72453ed.A1F(r15, r21, r20, r19, r14);
        C117305Zk.A1M(r18, r13, r14);
        r14[6] = r12;
        r14[7] = r11;
        r14[8] = r10;
        r14[9] = r9;
        r14[10] = r8;
        r14[11] = r7;
        r14[12] = r6;
        r14[13] = r5;
        r14[14] = r4;
        r14[15] = r3;
        r14[16] = r16;
        A00 = r14;
    }

    public EnumC124665pu(String str, String str2, int i) {
        this.fieldName = str2;
    }

    @Override // X.AnonymousClass5UX
    public String ACu() {
        return this.fieldName;
    }
}
