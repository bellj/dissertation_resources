package X;

import android.content.Context;
import android.text.Html;
import android.util.AttributeSet;
import com.whatsapp.TextEmojiLabel;

/* renamed from: X.1Nn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC28491Nn extends C004602b implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;

    public AbstractC28491Nn(Context context) {
        super(context, null);
        A08();
    }

    public AbstractC28491Nn(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A08();
    }

    public AbstractC28491Nn(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A08();
    }

    public static void A03(TextEmojiLabel textEmojiLabel) {
        textEmojiLabel.A07 = new C52162aM();
    }

    public static void A04(TextEmojiLabel textEmojiLabel, AnonymousClass01d r2) {
        textEmojiLabel.setAccessibilityHelper(new AnonymousClass2eq(textEmojiLabel, r2));
    }

    public static void A05(TextEmojiLabel textEmojiLabel, AnonymousClass01d r2, CharSequence charSequence) {
        textEmojiLabel.setAccessibilityHelper(new AnonymousClass2eq(textEmojiLabel, r2));
        textEmojiLabel.A07 = new C52162aM();
        textEmojiLabel.setText(charSequence);
    }

    public static void A06(TextEmojiLabel textEmojiLabel, String str) {
        textEmojiLabel.A0G(null, Html.fromHtml(str));
    }

    public static boolean A07(TextEmojiLabel textEmojiLabel) {
        textEmojiLabel.A07 = new C52162aM();
        textEmojiLabel.setAutoLinkMask(0);
        textEmojiLabel.setLinksClickable(false);
        textEmojiLabel.setFocusable(false);
        textEmojiLabel.setClickable(false);
        return false;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v4, types: [com.whatsapp.text.AutoSizeTextView, X.1Nn] */
    /* JADX WARN: Type inference failed for: r1v6, types: [X.1Nn, com.whatsapp.storage.SizeTickerView] */
    /* JADX WARN: Type inference failed for: r1v12, types: [X.1Nn, com.whatsapp.status.playback.widget.TextStatusContentView] */
    /* JADX WARN: Type inference failed for: r1v13, types: [com.whatsapp.biz.catalog.view.EllipsizedTextEmojiLabel, X.1Nn] */
    /* JADX WARN: Type inference failed for: r1v15, types: [com.whatsapp.TextEmojiLabel] */
    /* JADX WARN: Type inference failed for: r1v18, types: [com.whatsapp.WaTextView] */
    /* JADX WARNING: Unknown variable types count: 4 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A08() {
        /*
        // Method dump skipped, instructions count: 393
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC28491Nn.A08():void");
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }
}
