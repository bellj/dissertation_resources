package X;

/* renamed from: X.51w  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1094851w implements AnonymousClass5T7 {
    @Override // X.AnonymousClass5T7
    public boolean A9i(AbstractC94534c0 r2, AbstractC94534c0 r3, AnonymousClass4RG r4) {
        if (r3 instanceof C83003wX) {
            r3 = C83003wX.A00(r3);
            if (r3 instanceof C82933wQ) {
                return false;
            }
        }
        return r3.A07().A00.contains(r2);
    }
}
