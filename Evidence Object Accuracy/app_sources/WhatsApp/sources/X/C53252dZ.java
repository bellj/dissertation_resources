package X;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import com.whatsapp.R;

/* renamed from: X.2dZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53252dZ extends PopupWindow {
    public View A00;
    public int[] A01 = new int[2];
    public final View A02;
    public final AnonymousClass5UB A03;
    public final int[][] A04;

    public C53252dZ(View view, AnonymousClass19M r11, AnonymousClass5UB r12, int[] iArr) {
        super(new LinearLayout(view.getContext()), -2, -2);
        this.A02 = view;
        this.A03 = r12;
        LinearLayout linearLayout = (LinearLayout) getContentView();
        linearLayout.setId(R.id.single_skin_tone_selector);
        linearLayout.setOrientation(0);
        linearLayout.setFocusableInTouchMode(true);
        linearLayout.setFocusable(true);
        int dimensionPixelSize = C12960it.A09(view).getDimensionPixelSize(R.dimen.emoji_picker_item);
        int dimensionPixelSize2 = (dimensionPixelSize - C12960it.A09(view).getDimensionPixelSize(R.dimen.emoji_picker_icon)) / 2;
        this.A04 = AnonymousClass3JU.A0A(iArr);
        int i = 0;
        while (true) {
            int[][] iArr2 = this.A04;
            if (i < iArr2.length) {
                int[] iArr3 = iArr2[i];
                ImageView imageView = new ImageView(view.getContext());
                imageView.setId(R.id.single_skin_tone_selector_item);
                imageView.setLayoutParams(new LinearLayout.LayoutParams(dimensionPixelSize, dimensionPixelSize));
                imageView.setPadding(dimensionPixelSize2, dimensionPixelSize2, dimensionPixelSize2, dimensionPixelSize2);
                AnonymousClass19M.A02(view.getResources(), imageView, r11, iArr3);
                imageView.setTag(Integer.valueOf(i));
                C12960it.A0x(imageView, this, 11);
                imageView.setBackgroundResource(R.drawable.selector_orange_gradient);
                imageView.setContentDescription(C37471mS.A00(iArr3));
                linearLayout.addView(imageView);
                i++;
            } else {
                C12970iu.A1F(linearLayout);
                setTouchable(true);
                setFocusable(true);
                setOutsideTouchable(true);
                setInputMethodMode(2);
                setBackgroundDrawable(AnonymousClass2GE.A01(view.getContext(), R.drawable.panel, R.color.skinTonePopupBackground));
                linearLayout.requestFocus();
                view.getParent().requestDisallowInterceptTouchEvent(true);
                return;
            }
        }
    }
}
