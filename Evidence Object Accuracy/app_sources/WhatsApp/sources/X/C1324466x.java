package X;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.widget.ImageView;
import com.whatsapp.util.Log;

/* renamed from: X.66x  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1324466x implements AnonymousClass6MK {
    public final /* synthetic */ ImageView A00;
    public final /* synthetic */ AbstractC11740gm A01;

    public C1324466x(ImageView imageView, AbstractC11740gm r2) {
        this.A01 = r2;
        this.A00 = imageView;
    }

    @Override // X.AnonymousClass6MK
    public void APk() {
        Log.e("ImageComponentBinder/bindView/bitmap read failed");
    }

    @Override // X.AnonymousClass6MK
    public void AWv(Bitmap bitmap) {
        Number number = (Number) this.A01.get();
        if (number != null) {
            Paint paint = new Paint();
            paint.setColorFilter(new PorterDuffColorFilter(number.intValue(), PorterDuff.Mode.SRC_IN));
            Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            new Canvas(createBitmap).drawBitmap(bitmap, 0.0f, 0.0f, paint);
            this.A00.setImageBitmap(createBitmap);
            return;
        }
        this.A00.setImageBitmap(bitmap);
    }
}
