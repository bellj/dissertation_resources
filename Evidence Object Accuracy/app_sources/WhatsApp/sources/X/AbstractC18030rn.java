package X;

import android.content.Context;

/* renamed from: X.0rn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC18030rn implements AbstractC17340qe {
    public static Context A00(AnonymousClass01X r1) {
        Context context = r1.A00;
        if (context != null) {
            return context;
        }
        throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }
}
