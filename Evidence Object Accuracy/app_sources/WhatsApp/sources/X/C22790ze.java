package X;

/* renamed from: X.0ze  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22790ze implements AbstractC22800zf {
    public final C18790t3 A00;
    public final C18640sm A01;
    public final C14830m7 A02;
    public final C16100oS A03;
    public final C16080oP A04;
    public final C18800t4 A05;
    public final C19930uu A06;

    public C22790ze(C18790t3 r1, C18640sm r2, C14830m7 r3, C16100oS r4, C16080oP r5, C18800t4 r6, C19930uu r7) {
        this.A02 = r3;
        this.A06 = r7;
        this.A00 = r1;
        this.A04 = r5;
        this.A05 = r6;
        this.A03 = r4;
        this.A01 = r2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0058, code lost:
        if ((r8 - r2.A05.A05[r2.A02].A04) > 600) goto L_0x005a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x0316  */
    /* JADX WARNING: Removed duplicated region for block: B:173:0x0539 A[LOOP:0: B:28:0x008a->B:173:0x0539, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:203:0x031c A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:208:0x026b A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0213  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x0239  */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x0257  */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x0291  */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x02a2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(X.AnonymousClass1N8 r40, int r41, boolean r42, boolean r43, boolean r44) {
        /*
        // Method dump skipped, instructions count: 1347
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22790ze.A00(X.1N8, int, boolean, boolean, boolean):void");
    }

    @Override // X.AbstractC22800zf
    public void AbY(AnonymousClass1N8 r2, boolean z) {
        AbZ(r2, 0, z, false);
    }

    @Override // X.AbstractC22800zf
    public void AbZ(AnonymousClass1N8 r7, int i, boolean z, boolean z2) {
        A00(r7, i, z, z2, false);
    }
}
