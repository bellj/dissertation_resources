package X;

import android.text.TextUtils;
import android.util.Pair;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.3J5  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3J5 {
    public static final Map A00;
    public static final Map A01;

    static {
        HashMap A11 = C12970iu.A11();
        A00 = A11;
        HashMap A112 = C12970iu.A11();
        A01 = A112;
        A112.put("payment_instruction", Integer.valueOf((int) R.string.order_payment_method_update_native_flow_message_outside_whatsapp));
        A112.put("confirm", Integer.valueOf((int) R.string.order_payment_method_update_native_flow_message_no_method));
        A11.put("pending", Integer.valueOf((int) R.string.order_status_update_native_flow_message_pending_text));
        A11.put("processing", Integer.valueOf((int) R.string.order_status_update_native_flow_message_processing_text));
        A11.put("completed", Integer.valueOf((int) R.string.order_status_update_native_flow_message_completed_text));
        A11.put("canceled", Integer.valueOf((int) R.string.order_status_update_native_flow_message_canceled_text));
        A11.put("partially_shipped", Integer.valueOf((int) R.string.order_status_update_native_flow_message_partially_shipped_text));
        A11.put("shipped", Integer.valueOf((int) R.string.order_status_update_native_flow_message_shipped_text));
    }

    public static Pair A00(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            JSONObject A05 = C13000ix.A05(str);
            return C12990iw.A0L(A05.getString("payment_method"), Long.valueOf(A05.getLong("payment_timestamp")));
        } catch (JSONException e) {
            Log.e("CheckoutInfoContentParser/getCustomPaymentMethodStr failed to parse parameters json", e);
            return null;
        }
    }

    public static AnonymousClass1ZD A01(AnonymousClass102 r15, String str, byte[] bArr, boolean z) {
        if (TextUtils.isEmpty(str)) {
            Log.e("CheckoutInfoContentParser/parseE2ECheckoutInfo/invalid native flow message does not have parameters json");
            return null;
        }
        try {
            JSONObject A05 = C13000ix.A05(str);
            String string = A05.getString("reference_id");
            String optString = A05.optString("type");
            AbstractC30791Yv A02 = r15.A02(A05.getString("currency"));
            AnonymousClass1ZI A012 = AnonymousClass1QC.A01(A05.optJSONObject("total_amount"));
            String string2 = A05.getString("payment_configuration");
            AnonymousClass1ZK A002 = AnonymousClass1QC.A00(A05.getJSONObject("order"));
            List A052 = AnonymousClass1QC.A05(A05.optJSONArray("external_payment_configurations"));
            return new AnonymousClass1ZD(A02, A002, A012, A002.A00(), string, optString, string2, null, A05.optString("payment_method"), A052, bArr, A05.optLong("payment_timestamp"), z);
        } catch (JSONException unused) {
            Log.e(C12960it.A0d(str, C12960it.A0k("CheckoutInfoContentParser/parseE2ECheckoutInfo/invalid paramsJson=")));
            return null;
        }
    }

    public static String A02(C27081Fy r3) {
        int i = r3.A01;
        if ((i & 1) == 1) {
            C57552nF r0 = r3.A03;
            if (r0 == null) {
                r0 = C57552nF.A08;
            }
            C57122mW r02 = ((C57442n3) r0.A03.get(0)).A03;
            if (r02 == null) {
                r02 = C57122mW.A03;
            }
            return r02.A02;
        } else if ((i & 8) != 8) {
            return null;
        } else {
            C57752nZ r2 = r3.A0K;
            if (r2 == null) {
                r2 = C57752nZ.A07;
            }
            if (r2.A01 == 6) {
                return ((C57162ma) r2.A0c().A02.get(0)).A02;
            }
            return null;
        }
    }

    public static String A03(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return C13000ix.A05(str).getJSONObject("order").getString("status");
        } catch (JSONException e) {
            Log.e("CheckoutInfoContentParser/getOrderStatusStr failed to parse parameters json", e);
            return null;
        }
    }
}
