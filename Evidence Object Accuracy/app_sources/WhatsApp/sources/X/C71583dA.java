package X;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.3dA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C71583dA implements AnonymousClass5ZX, AbstractC115515Rv, AbstractC115525Rw {
    public static final HashMap A01;
    public static final HashMap A02;
    public static final HashMap A03;
    public static final Map A04;
    public static final Map A05;
    public final Class A00;

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:52:0x013b */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r3v0, types: [java.util.Map] */
    /* JADX WARN: Type inference failed for: r3v5, types: [java.util.Map, java.lang.Object] */
    /* JADX WARN: Type inference failed for: r3v6, types: [X.3cp] */
    /* JADX WARN: Type inference failed for: r3v7, types: [java.util.LinkedHashMap, java.util.AbstractMap, java.util.Map] */
    /* JADX WARN: Type inference failed for: r3v8, types: [java.util.LinkedHashMap, java.util.AbstractMap] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x024a A[LOOP:3: B:36:0x0244->B:38:0x024a, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x028d A[LOOP:4: B:41:0x0287->B:43:0x028d, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x02ce A[LOOP:5: B:45:0x02c8->B:47:0x02ce, LOOP_END] */
    /* JADX WARNING: Unknown variable types count: 1 */
    static {
        /*
        // Method dump skipped, instructions count: 743
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C71583dA.<clinit>():void");
    }

    public C71583dA(Class cls) {
        C16700pc.A0E(cls, 1);
        this.A00 = cls;
    }

    public static final Class A00(C71583dA r2) {
        C16700pc.A0E(r2, 0);
        Class ADf = r2.ADf();
        if (ADf.isPrimitive()) {
            String name = ADf.getName();
            switch (name.hashCode()) {
                case -1325958191:
                    if (name.equals("double")) {
                        return Double.class;
                    }
                    break;
                case 104431:
                    if (name.equals("int")) {
                        return Integer.class;
                    }
                    break;
                case 3039496:
                    if (name.equals("byte")) {
                        return Byte.class;
                    }
                    break;
                case 3052374:
                    if (name.equals("char")) {
                        return Character.class;
                    }
                    break;
                case 3327612:
                    if (name.equals("long")) {
                        return Long.class;
                    }
                    break;
                case 3625364:
                    if (name.equals("void")) {
                        return Void.class;
                    }
                    break;
                case 64711720:
                    if (name.equals("boolean")) {
                        return Boolean.class;
                    }
                    break;
                case 97526364:
                    if (name.equals("float")) {
                        return Float.class;
                    }
                    break;
                case 109413500:
                    if (name.equals("short")) {
                        return Short.class;
                    }
                    break;
            }
        }
        return ADf;
    }

    public void A01() {
        String A0t;
        String str;
        Class cls = this.A00;
        C16700pc.A0E(cls, 0);
        if (cls.isAnonymousClass()) {
            return;
        }
        if (cls.isLocalClass()) {
            String simpleName = cls.getSimpleName();
            Method enclosingMethod = cls.getEnclosingMethod();
            if (enclosingMethod == null) {
                Constructor<?> enclosingConstructor = cls.getEnclosingConstructor();
                if (enclosingConstructor == null) {
                    C16700pc.A0B(simpleName);
                    int A012 = AnonymousClass03B.A01(simpleName, '$', 0, 6);
                    if (A012 != -1) {
                        C16700pc.A0B(simpleName.substring(A012 + 1, simpleName.length()));
                        return;
                    }
                    return;
                }
                C16700pc.A0B(simpleName);
                str = enclosingConstructor.getName();
            } else {
                C16700pc.A0B(simpleName);
                str = enclosingMethod.getName();
            }
            AnonymousClass03B.A07(simpleName, C16700pc.A08(str, "$"), simpleName);
        } else if (cls.isArray()) {
            Class<?> componentType = cls.getComponentType();
            if (componentType.isPrimitive() && (A0t = C12970iu.A0t(componentType.getName(), A05)) != null) {
                C16700pc.A08(A0t, "Array");
            }
        } else {
            A05.get(cls.getName());
        }
    }

    @Override // X.AnonymousClass5ZX
    public Class ADf() {
        return this.A00;
    }

    public boolean equals(Object obj) {
        return (obj instanceof C71583dA) && C16700pc.A0O(A00(this), A00((C71583dA) obj));
    }

    public int hashCode() {
        return A00(this).hashCode();
    }

    public String toString() {
        return C16700pc.A08(this.A00.toString(), " (Kotlin reflection is not available)");
    }
}
