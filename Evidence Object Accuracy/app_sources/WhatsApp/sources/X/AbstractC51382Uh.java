package X;

import android.content.Context;
import android.util.AttributeSet;
import com.whatsapp.status.ContactStatusThumbnail;

/* renamed from: X.2Uh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC51382Uh extends ContactStatusThumbnail {
    public boolean A00;

    public AbstractC51382Uh(Context context) {
        super(context);
        A00();
    }

    public AbstractC51382Uh(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
    }

    public AbstractC51382Uh(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
    }
}
