package X;

import android.content.Intent;
import com.whatsapp.payments.ui.IndiaUpiBankPickerActivity;

/* renamed from: X.5kA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C121855kA extends AnonymousClass6CT {
    public final /* synthetic */ AbstractActivityC121655j9 A00;

    public C121855kA(AbstractActivityC121655j9 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC1311861p
    public void ALx() {
        AbstractActivityC121655j9 r3 = this.A00;
        Intent A0D = C12990iw.A0D(r3, IndiaUpiBankPickerActivity.class);
        A0D.putExtra("extra_payments_entry_type", 6);
        A0D.putExtra("extra_is_first_payment_method", !AbstractActivityC119235dO.A1l(r3));
        A0D.putExtra("extra_skip_value_props_display", AbstractActivityC119235dO.A1l(r3));
        r3.A2D(A0D);
    }
}
