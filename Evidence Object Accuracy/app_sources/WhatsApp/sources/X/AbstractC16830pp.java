package X;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import com.whatsapp.jid.UserJid;
import java.util.List;
import java.util.regex.Pattern;

/* renamed from: X.0pp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public interface AbstractC16830pp extends AbstractC16840pq {
    boolean A70();

    boolean A71();

    void A9W(AnonymousClass1IR v, AnonymousClass1IR v2);

    Class AAV();

    Class AAW();

    Intent AAX(Context context);

    Class ABI();

    C38161nc ABU();

    Class ABa();

    Class ABc();

    Class ABd();

    AnonymousClass17Y ABn();

    AnonymousClass18N ABo();

    AnonymousClass18M ABq();

    AnonymousClass17U ABw();

    int AC0(String str);

    AbstractC38231nk ACH();

    String ACI();

    Intent ACS(Context context, boolean z);

    Intent ACT(Context context, Uri uri);

    int ACY();

    Intent ACd(Context context, String str, String str2);

    AbstractC16870pt ACx();

    Intent ADR(Context context);

    AnonymousClass17T AEE();

    AbstractC92724Xe AEF();

    AnonymousClass1V8 AEY(AnonymousClass20C v);

    Class AEb(Bundle bundle);

    AbstractC43531xB AEz();

    List AF1(AnonymousClass1IR v, AnonymousClass1IS v2);

    List AF2(AnonymousClass1IR v, AnonymousClass1IS v2);

    AnonymousClass18O AF4();

    AbstractC116085Ub AF5();

    AnonymousClass5Wu AF6(AnonymousClass018 v, C14850m9 v2, C22460z7 v3, AbstractC116085Ub v4);

    Class AF7();

    AbstractC450620a AF8();

    String AF9();

    AnonymousClass17W AFA();

    AnonymousClass5UU AFB(C16590pI v, C18600si v2);

    int AFC();

    Class AFD();

    AnonymousClass5X2 AFE();

    Class AFF();

    int AFH();

    Pattern AFI();

    AbstractC38191ng AFJ();

    AbstractC38141na AFL();

    Pattern AFN();

    String AFO(AnonymousClass18M v, AbstractC15340mz v2);

    AnonymousClass24Y AFQ();

    Class AFR();

    int AFS();

    Class AFT();

    AbstractC51302Tt AFU();

    Class AFV();

    Class AFa();

    AbstractC38181ne AFb();

    Class AFc();

    Class AFd();

    Intent AFe(Context context, String str, boolean z);

    Class AFg();

    Class AG9();

    String AGP(AnonymousClass1IR v);

    Class AGb();

    int AGi();

    String AH4(String str);

    Intent AHG(Context context, String str);

    int AHJ(AnonymousClass1IR v);

    String AHL(AnonymousClass1IR v);

    C47822Cw AHU(AnonymousClass1ZO v, UserJid userJid, String str);

    boolean AIG();

    boolean AJH();

    boolean AJs(Uri uri);

    boolean AKH(C89674Kw v);

    void AKc(Uri uri);

    void ALp(Context context, AbstractC13860kS v, AnonymousClass1IR v2);

    void AZN(C456522m v, List list);

    AnonymousClass1V8 AZQ(AnonymousClass1V8 v);

    void AdF(C17900ra v);

    boolean AdP();
}
