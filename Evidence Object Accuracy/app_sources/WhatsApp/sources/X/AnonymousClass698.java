package X;

import android.content.res.Resources;
import com.whatsapp.R;

/* renamed from: X.698  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass698 implements AnonymousClass17U {
    @Override // X.AnonymousClass17U
    public AnonymousClass1W9 AAI(String str) {
        return null;
    }

    @Override // X.AnonymousClass17U
    public boolean AK8() {
        return false;
    }

    @Override // X.AnonymousClass17U
    public String AHK(Resources resources, AnonymousClass1IR r5, String str) {
        if (r5.A02 == 420) {
            return C12990iw.A0o(resources, str, C12970iu.A1b(), 0, R.string.transaction_status_pending);
        }
        return null;
    }
}
