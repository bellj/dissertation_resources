package X;

import java.util.Arrays;

/* renamed from: X.0QC  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0QC {
    public static int A0C = 1;
    public float A00;
    public int A01 = -1;
    public int A02 = -1;
    public int A03 = 0;
    public int A04 = 0;
    public int A05 = 0;
    public AnonymousClass0JQ A06;
    public boolean A07;
    public boolean A08 = false;
    public float[] A09 = new float[9];
    public float[] A0A = new float[9];
    public C07240Xf[] A0B = new C07240Xf[16];

    public AnonymousClass0QC(AnonymousClass0JQ r4) {
        this.A06 = r4;
    }

    public void A00() {
        this.A06 = AnonymousClass0JQ.UNKNOWN;
        this.A04 = 0;
        this.A02 = -1;
        this.A01 = -1;
        this.A00 = 0.0f;
        this.A08 = false;
        int i = this.A03;
        for (int i2 = 0; i2 < i; i2++) {
            this.A0B[i2] = null;
        }
        this.A03 = 0;
        this.A05 = 0;
        this.A07 = false;
        Arrays.fill(this.A09, 0.0f);
    }

    public final void A01(C07240Xf r4) {
        int i = 0;
        while (true) {
            int i2 = this.A03;
            if (i >= i2) {
                C07240Xf[] r1 = this.A0B;
                int length = r1.length;
                if (i2 >= length) {
                    r1 = (C07240Xf[]) Arrays.copyOf(r1, length << 1);
                    this.A0B = r1;
                }
                int i3 = this.A03;
                r1[i3] = r4;
                this.A03 = i3 + 1;
                return;
            } else if (this.A0B[i] != r4) {
                i++;
            } else {
                return;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001a, code lost:
        r5.A03 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001c, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000e, code lost:
        r0 = r4 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0010, code lost:
        if (r3 >= r0) goto L_0x001a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0012, code lost:
        r1 = r3 + 1;
        r2[r3] = r2[r1];
        r3 = r1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A02(X.C07240Xf r6) {
        /*
            r5 = this;
            int r4 = r5.A03
            r3 = 0
        L_0x0003:
            if (r3 >= r4) goto L_0x001c
            X.0Xf[] r2 = r5.A0B
            r0 = r2[r3]
            if (r0 == r6) goto L_0x000e
            int r3 = r3 + 1
            goto L_0x0003
        L_0x000e:
            int r0 = r4 + -1
            if (r3 >= r0) goto L_0x001a
            int r1 = r3 + 1
            r0 = r2[r1]
            r2[r3] = r0
            r3 = r1
            goto L_0x000e
        L_0x001a:
            r5.A03 = r0
        L_0x001c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0QC.A02(X.0Xf):void");
    }

    public final void A03(C07240Xf r5) {
        int i = this.A03;
        for (int i2 = 0; i2 < i; i2++) {
            this.A0B[i2].A01(r5, false);
        }
        this.A03 = 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append(this.A02);
        return sb.toString();
    }
}
