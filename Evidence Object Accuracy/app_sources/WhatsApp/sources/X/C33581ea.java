package X;

import android.graphics.drawable.Drawable;
import com.facebook.redex.RunnableBRunnable0Shape12S0100000_I0_12;
import com.whatsapp.storage.StorageUsageDeleteCompleteDialogFragment;

/* renamed from: X.1ea  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C33581ea extends AnonymousClass03J {
    public final /* synthetic */ StorageUsageDeleteCompleteDialogFragment A00;

    public C33581ea(StorageUsageDeleteCompleteDialogFragment storageUsageDeleteCompleteDialogFragment) {
        this.A00 = storageUsageDeleteCompleteDialogFragment;
    }

    @Override // X.AnonymousClass03J
    public void A01(Drawable drawable) {
        this.A00.A00.A0J(new RunnableBRunnable0Shape12S0100000_I0_12(this, 33), 500);
    }
}
