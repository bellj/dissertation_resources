package X;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

/* renamed from: X.4bj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C94384bj {
    public static final /* synthetic */ AtomicIntegerFieldUpdater A01 = AtomicIntegerFieldUpdater.newUpdater(C94384bj.class, "blockingTasksInBuffer");
    public static final /* synthetic */ AtomicIntegerFieldUpdater A02 = AtomicIntegerFieldUpdater.newUpdater(C94384bj.class, "consumerIndex");
    public static final /* synthetic */ AtomicIntegerFieldUpdater A03 = AtomicIntegerFieldUpdater.newUpdater(C94384bj.class, "producerIndex");
    public static final /* synthetic */ AtomicReferenceFieldUpdater A04 = AtomicReferenceFieldUpdater.newUpdater(C94384bj.class, Object.class, "lastScheduledTask");
    public final AtomicReferenceArray A00 = new AtomicReferenceArray(128);
    public volatile /* synthetic */ int blockingTasksInBuffer = 0;
    public volatile /* synthetic */ int consumerIndex = 0;
    public volatile /* synthetic */ Object lastScheduledTask = null;
    public volatile /* synthetic */ int producerIndex = 0;

    public final long A00(C94384bj r7, boolean z) {
        AbstractRunnableC75993ks r5;
        do {
            r5 = (AbstractRunnableC75993ks) r7.lastScheduledTask;
            if (r5 == null || (z && r5.A01.A00 != 1)) {
                return -2;
            }
            long nanoTime = System.nanoTime() - r5.A00;
            long j = C88864Hl.A04;
            if (nanoTime < j) {
                return j - nanoTime;
            }
        } while (!AnonymousClass0KN.A00(r7, r5, null, A04));
        A02(r5);
        return -1;
    }

    public final AbstractRunnableC75993ks A01() {
        AbstractRunnableC75993ks r2;
        while (true) {
            int i = this.consumerIndex;
            if (i - this.producerIndex == 0) {
                return null;
            }
            int i2 = i & 127;
            if (A02.compareAndSet(this, i, i + 1) && (r2 = (AbstractRunnableC75993ks) this.A00.getAndSet(i2, null)) != null) {
                if (r2.A01.A00 == 1) {
                    A01.decrementAndGet(this);
                }
                return r2;
            }
        }
    }

    public final AbstractRunnableC75993ks A02(AbstractRunnableC75993ks r5) {
        AtomicReferenceArray atomicReferenceArray;
        AbstractRunnableC75993ks r3 = (AbstractRunnableC75993ks) A04.getAndSet(this, r5);
        if (r3 != null) {
            if (r3.A01.A00 == 1) {
                A01.incrementAndGet(this);
            }
            if (this.producerIndex - this.consumerIndex == 127) {
                return r3;
            }
            int i = this.producerIndex & 127;
            while (true) {
                atomicReferenceArray = this.A00;
                if (atomicReferenceArray.get(i) == null) {
                    break;
                }
                Thread.yield();
            }
            atomicReferenceArray.lazySet(i, r3);
            A03.incrementAndGet(this);
        }
        return null;
    }
}
