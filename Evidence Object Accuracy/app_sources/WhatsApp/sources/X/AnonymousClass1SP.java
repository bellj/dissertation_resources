package X;

import android.content.Context;
import android.os.Handler;
import com.facebook.redex.RunnableBRunnable0Shape1S0100000_I0_1;
import com.whatsapp.anr.SigquitBasedANRDetector;
import com.whatsapp.util.Log;

/* renamed from: X.1SP  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1SP {
    public int A00 = 0;
    public long A01;
    public AnonymousClass1SR A02;
    public final Context A03;
    public final AnonymousClass01d A04;

    public AnonymousClass1SP(Context context, AnonymousClass01d r3) {
        this.A03 = context;
        this.A04 = r3;
    }

    public final synchronized void A00(AnonymousClass1SQ r6, String str, String str2, int i) {
        Handler handler;
        RunnableBRunnable0Shape1S0100000_I0_1 runnableBRunnable0Shape1S0100000_I0_1;
        if (this.A02.A02 == this.A01) {
            if (i == 0) {
                Log.i("SigquitBasedANRDetector/Started monitoring");
            } else if (i != 1) {
                if (i == 2 || i == 3) {
                    this.A00 = 0;
                } else if (i == 4) {
                    this.A00 = 0;
                    Log.e("SigquitBasedANRDetector/onCheckFailed");
                } else {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Unexpected state change reason: ");
                    sb.append(i);
                    throw new IllegalArgumentException(sb.toString());
                }
                SigquitBasedANRDetector sigquitBasedANRDetector = r6.A01;
                if (sigquitBasedANRDetector.A09 != null) {
                    handler = sigquitBasedANRDetector.A09;
                    runnableBRunnable0Shape1S0100000_I0_1 = new RunnableBRunnable0Shape1S0100000_I0_1(r6, 49);
                    handler.post(runnableBRunnable0Shape1S0100000_I0_1);
                }
            } else {
                this.A00 = 2;
                StringBuilder sb2 = new StringBuilder("SigquitBasedANRDetector/On error detected ");
                sb2.append(str);
                sb2.append(" ");
                sb2.append(str2);
                Log.i(sb2.toString());
                SigquitBasedANRDetector sigquitBasedANRDetector2 = r6.A01;
                if (sigquitBasedANRDetector2.A09 != null) {
                    handler = sigquitBasedANRDetector2.A09;
                    runnableBRunnable0Shape1S0100000_I0_1 = new RunnableBRunnable0Shape1S0100000_I0_1(r6, 48);
                    handler.post(runnableBRunnable0Shape1S0100000_I0_1);
                }
            }
        }
    }
}
