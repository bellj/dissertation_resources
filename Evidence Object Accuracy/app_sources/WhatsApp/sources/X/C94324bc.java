package X;

/* renamed from: X.4bc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C94324bc {
    public static final C94324bc A02 = new C94324bc(0, 0);
    public final long A00;
    public final long A01;

    public C94324bc(long j, long j2) {
        this.A01 = j;
        this.A00 = j2;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C94324bc.class != obj.getClass()) {
                return false;
            }
            C94324bc r7 = (C94324bc) obj;
            if (!(this.A01 == r7.A01 && this.A00 == r7.A00)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return (((int) this.A01) * 31) + ((int) this.A00);
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("[timeUs=");
        A0k.append(this.A01);
        A0k.append(", position=");
        A0k.append(this.A00);
        return C12960it.A0d("]", A0k);
    }
}
