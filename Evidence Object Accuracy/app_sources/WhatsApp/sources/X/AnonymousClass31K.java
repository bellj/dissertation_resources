package X;

/* renamed from: X.31K  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass31K extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Boolean A02;
    public Boolean A03;
    public Boolean A04;
    public Boolean A05;
    public Boolean A06;
    public Boolean A07;
    public Boolean A08;
    public Boolean A09;
    public Boolean A0A;
    public Boolean A0B;
    public Boolean A0C;

    public AnonymousClass31K() {
        super(3126, new AnonymousClass00E(1, 1, 20), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(2, this.A00);
        r3.Abe(3, this.A01);
        r3.Abe(4, this.A02);
        r3.Abe(6, this.A03);
        r3.Abe(7, this.A04);
        r3.Abe(8, this.A05);
        r3.Abe(1, this.A06);
        r3.Abe(12, this.A07);
        r3.Abe(15, this.A08);
        r3.Abe(17, this.A09);
        r3.Abe(18, this.A0A);
        r3.Abe(20, this.A0B);
        r3.Abe(21, this.A0C);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamGroupInfo {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "exitGroup", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "groupAddParticipants", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "groupAudioCall", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "groupDescription", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "groupDisappearingMessages", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "groupEncryption", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "groupInfoVisit", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "groupMuteClick", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "groupSearch", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "groupStarredMessages", this.A09);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "groupVideoCall", this.A0A);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "inviteToGroupViaLink", this.A0B);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "reportGroup", this.A0C);
        return C12960it.A0d("}", A0k);
    }
}
