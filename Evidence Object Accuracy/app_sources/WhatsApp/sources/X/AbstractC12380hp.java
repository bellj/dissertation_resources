package X;

import android.graphics.Canvas;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.0hp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public interface AbstractC12380hp {
    void A7F(View view);

    void APW(Canvas canvas, View view, RecyclerView recyclerView, float f, float f2, int i, boolean z);
}
