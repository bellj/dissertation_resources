package X;

import java.util.Map;

/* renamed from: X.14O  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass14O {
    public final ThreadLocal A00 = new AnonymousClass1NS(this);

    public synchronized String A00(String str) {
        Map map;
        map = (Map) this.A00.get();
        AnonymousClass009.A05(map);
        return (String) map.get(str);
    }
}
