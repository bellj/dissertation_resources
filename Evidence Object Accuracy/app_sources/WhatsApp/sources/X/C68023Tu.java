package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape3S0110000_I1;
import java.util.Map;

/* renamed from: X.3Tu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68023Tu implements AbstractC116635Wf {
    public View A00;
    public final C53042cM A01;
    public final C14850m9 A02;
    public final AnonymousClass1CT A03;
    public final AnonymousClass12S A04;
    public final AnonymousClass12O A05;
    public final AnonymousClass01N A06;

    public C68023Tu(C53042cM r1, C14850m9 r2, AnonymousClass1CT r3, AnonymousClass12S r4, AnonymousClass12O r5, AnonymousClass01N r6) {
        this.A02 = r2;
        this.A04 = r4;
        this.A05 = r5;
        this.A01 = r1;
        this.A03 = r3;
        this.A06 = r6;
    }

    @Override // X.AbstractC116635Wf
    public void AIR() {
        C12970iu.A1G(this.A00);
    }

    @Override // X.AbstractC116635Wf
    public boolean AdK() {
        return C12960it.A1W(this.A05.A01());
    }

    @Override // X.AbstractC116635Wf
    public void AfF() {
        if (this.A00 == null) {
            C53042cM r2 = this.A01;
            View A0F = C12960it.A0F(C12960it.A0E(r2), r2, R.layout.conversations_user_notice_banner);
            this.A00 = A0F;
            r2.addView(A0F);
            this.A04.A01(C12960it.A0V());
        }
        AnonymousClass12O r3 = this.A05;
        C43911xp A01 = r3.A01();
        AnonymousClass009.A05(A01);
        View view = this.A00;
        AnonymousClass009.A03(view);
        TextView A0I = C12960it.A0I(view, R.id.user_notice_banner_text);
        C53042cM r6 = this.A01;
        A0I.setText(AnonymousClass3I2.A00(r6.getContext(), null, A01.A04));
        ((AnonymousClass36P) AnonymousClass028.A0D(this.A00, R.id.user_notice_banner_icon)).A02(A01);
        String str = A01.A01;
        String A012 = AnonymousClass3I2.A01(str);
        C14850m9 r1 = this.A02;
        C43831xf A013 = r3.A08.A01();
        AnonymousClass009.A05(A013);
        boolean A014 = C43901xo.A01(r1, A013);
        Map A02 = AnonymousClass3I2.A02(str);
        if (A014 && r6.getContext() != null) {
            C12960it.A0r(r6.getContext(), A0I, R.string.green_alert_banner_content_description);
        }
        this.A00.setOnClickListener(new AnonymousClass36c(this, A012, A02, A014));
        AnonymousClass028.A0D(this.A00, R.id.cancel).setOnClickListener(new ViewOnClickCListenerShape3S0110000_I1(this, 0, A014));
        Log.i("UserNoticeBanner/update/banner shown");
        this.A00.setVisibility(0);
    }
}
