package X;

import android.os.Build;
import com.whatsapp.voipcalling.GlVideoRenderer;
import org.chromium.net.UrlRequest;

/* renamed from: X.3d9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C71573d9 implements AnonymousClass01N {
    public final int A00;
    public final AnonymousClass2FL A01;
    public final C48722Hj A02;
    public final C51112Sw A03;
    public final AnonymousClass01J A04;

    public C71573d9(AnonymousClass2FL r1, C48722Hj r2, C51112Sw r3, AnonymousClass01J r4, int i) {
        this.A04 = r4;
        this.A02 = r2;
        this.A01 = r1;
        this.A03 = r3;
        this.A00 = i;
    }

    @Override // X.AnonymousClass01N, X.AnonymousClass01H
    public Object get() {
        int i = this.A00;
        switch (i) {
            case 0:
                return new AnonymousClass4J6(this);
            case 1:
                return new AnonymousClass4JH(this);
            case 2:
                return new AnonymousClass4JI(this);
            case 3:
                return new AnonymousClass4JJ(this);
            case 4:
                return new AnonymousClass4JK(this);
            case 5:
                return new AnonymousClass3W3(this);
            case 6:
                return new AnonymousClass3W1(this);
            case 7:
                return new AnonymousClass3W5(this);
            case 8:
                return new C68583Vy(this);
            case 9:
                return new C89154Iw(this);
            case 10:
                return new C89164Ix(this);
            case 11:
                return new C89174Iy(this);
            case 12:
                return new C89184Iz(this);
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                return new AnonymousClass4J0(this);
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                return new AnonymousClass4J1(this);
            case 15:
                return new AnonymousClass4J2(this);
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                return new AnonymousClass4J3(this);
            case 17:
                return new AnonymousClass4J4(this);
            case 18:
                return new AnonymousClass4J5(this);
            case 19:
                return new AnonymousClass4J7(this);
            case C43951xu.A01:
                return new AnonymousClass4J8(this);
            case 21:
                return new AnonymousClass4J9(this);
            case 22:
                return new AnonymousClass4JA(this);
            case 23:
                if (Build.VERSION.SDK_INT >= 24) {
                    return new C1105455y();
                }
                return new C1105555z();
            case 24:
                return new AnonymousClass4JB(this);
            case 25:
                return new AnonymousClass4JC(this);
            case 26:
                return new AnonymousClass4JD(this);
            case 27:
                return new AnonymousClass4JE(this);
            case 28:
                return new AnonymousClass4JF(this);
            case 29:
                return new AnonymousClass4JG(this);
            case C25991Bp.A0S:
                AnonymousClass01J r1 = this.A04;
                return new C127205u7(C18000rk.A00(r1.ABt), C18000rk.A00(r1.AIo));
            default:
                throw new AssertionError(i);
        }
    }
}
