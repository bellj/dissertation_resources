package X;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.util.Log;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/* renamed from: X.2kn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56382kn extends AbstractC77963o9 {
    public final AnonymousClass3CG A00;
    public final AnonymousClass3CG A01;
    public final AnonymousClass3CG A02;
    public final AnonymousClass3CG A03;
    public final AnonymousClass3CG A04;
    public final AnonymousClass3CG A05;
    public final AnonymousClass3CG A06;
    public final AnonymousClass3CG A07;
    public final AnonymousClass3CG A08;
    public final AnonymousClass3IK A09;
    public final ExecutorService A0A;

    @Override // X.AbstractC95064d1
    public final String A05() {
        return "com.google.android.gms.wearable.internal.IWearableService";
    }

    @Override // X.AbstractC95064d1
    public final String A06() {
        return "com.google.android.gms.wearable.BIND";
    }

    @Override // X.AbstractC95064d1
    public final boolean A0A() {
        return true;
    }

    @Override // X.AbstractC95064d1, X.AbstractC72443eb
    public final int AET() {
        return 8600000;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C56382kn(Context context, Looper looper, AbstractC14980mM r13, AbstractC15000mO r14, AnonymousClass3BW r15) {
        super(context, looper, r13, r14, r15, 14);
        ExecutorService unconfigurableExecutorService = Executors.unconfigurableExecutorService(Executors.newCachedThreadPool());
        C13020j0.A01(context);
        synchronized (AnonymousClass3IK.class) {
            if (AnonymousClass3IK.A01 == null) {
                AnonymousClass3IK.A01 = new AnonymousClass3IK(context);
            }
        }
        AnonymousClass3IK r1 = AnonymousClass3IK.A01;
        this.A00 = new AnonymousClass3CG();
        this.A01 = new AnonymousClass3CG();
        this.A02 = new AnonymousClass3CG();
        this.A03 = new AnonymousClass3CG();
        this.A04 = new AnonymousClass3CG();
        this.A05 = new AnonymousClass3CG();
        this.A06 = new AnonymousClass3CG();
        this.A07 = new AnonymousClass3CG();
        this.A08 = new AnonymousClass3CG();
        C13020j0.A01(unconfigurableExecutorService);
        this.A0A = unconfigurableExecutorService;
        this.A09 = r1;
    }

    @Override // X.AbstractC95064d1
    public final /* bridge */ /* synthetic */ IInterface A04(IBinder iBinder) {
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.wearable.internal.IWearableService");
        return !(queryLocalInterface instanceof C80523sO) ? new C80523sO(iBinder) : queryLocalInterface;
    }

    @Override // X.AbstractC95064d1
    public final String A07() {
        return this.A09.A01() ? "com.google.android.wearable.app.cn" : "com.google.android.gms";
    }

    @Override // X.AbstractC95064d1
    public final void A08(Bundle bundle, IBinder iBinder, int i, int i2) {
        if (Log.isLoggable("WearableClient", 2)) {
            Log.v("WearableClient", C12960it.A0e("onPostInitHandler: statusCode ", C12980iv.A0t(41), i));
        }
        if (i == 0) {
            this.A00.A00(iBinder);
            this.A01.A00(iBinder);
            this.A02.A00(iBinder);
            this.A03.A00(iBinder);
            this.A04.A00(iBinder);
            this.A05.A00(iBinder);
            this.A06.A00(iBinder);
            this.A07.A00(iBinder);
            this.A08.A00(iBinder);
            i = 0;
        }
        super.A08(bundle, iBinder, i, i2);
    }

    @Override // X.AbstractC95064d1
    public final C78603pB[] A0B() {
        return C88774Hb.A04;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0022, code lost:
        if (r1 < 8600000) goto L_0x0024;
     */
    @Override // X.AbstractC95064d1, X.AbstractC72443eb
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A7X(X.AnonymousClass5Sa r7) {
        /*
            r6 = this;
            java.lang.String r5 = "com.google.android.wearable.app.cn"
            boolean r0 = r6.Aad()
            if (r0 != 0) goto L_0x00a6
            android.content.Context r4 = r6.A0F     // Catch: NameNotFoundException -> 0x008b
            android.content.pm.PackageManager r1 = r4.getPackageManager()     // Catch: NameNotFoundException -> 0x008b
            r0 = 128(0x80, float:1.794E-43)
            android.content.pm.ApplicationInfo r0 = r1.getApplicationInfo(r5, r0)     // Catch: NameNotFoundException -> 0x008b
            android.os.Bundle r1 = r0.metaData     // Catch: NameNotFoundException -> 0x008b
            r3 = 0
            if (r1 == 0) goto L_0x0088
            java.lang.String r0 = "com.google.android.wearable.api.version"
            int r1 = r1.getInt(r0, r3)     // Catch: NameNotFoundException -> 0x008b
            r0 = 8600000(0x8339c0, float:1.2051167E-38)
            if (r1 >= r0) goto L_0x00a6
        L_0x0024:
            r0 = 82
            java.lang.StringBuilder r2 = X.C12980iv.A0t(r0)     // Catch: NameNotFoundException -> 0x008b
            java.lang.String r0 = "The Wear OS app is out of date. Requires API version 8600000 but found "
            r2.append(r0)     // Catch: NameNotFoundException -> 0x008b
            r2.append(r1)     // Catch: NameNotFoundException -> 0x008b
            java.lang.String r1 = "WearableClient"
            java.lang.String r0 = r2.toString()     // Catch: NameNotFoundException -> 0x008b
            android.util.Log.w(r1, r0)     // Catch: NameNotFoundException -> 0x008b
            java.lang.String r0 = "com.google.android.wearable.app.cn.UPDATE_ANDROID_WEAR"
            android.content.Intent r0 = X.C12990iw.A0E(r0)     // Catch: NameNotFoundException -> 0x008b
            android.content.Intent r2 = r0.setPackage(r5)     // Catch: NameNotFoundException -> 0x008b
            android.content.pm.PackageManager r1 = r4.getPackageManager()     // Catch: NameNotFoundException -> 0x008b
            r0 = 65536(0x10000, float:9.18355E-41)
            android.content.pm.ResolveInfo r0 = r1.resolveActivity(r2, r0)     // Catch: NameNotFoundException -> 0x008b
            if (r0 != 0) goto L_0x0069
            java.lang.String r0 = "market://details"
            android.net.Uri r0 = android.net.Uri.parse(r0)     // Catch: NameNotFoundException -> 0x008b
            android.net.Uri$Builder r1 = r0.buildUpon()     // Catch: NameNotFoundException -> 0x008b
            java.lang.String r0 = "id"
            android.net.Uri$Builder r0 = r1.appendQueryParameter(r0, r5)     // Catch: NameNotFoundException -> 0x008b
            android.net.Uri r0 = r0.build()     // Catch: NameNotFoundException -> 0x008b
            android.content.Intent r2 = X.C12970iu.A0B(r0)     // Catch: NameNotFoundException -> 0x008b
        L_0x0069:
            int r0 = X.C88424Fp.A00     // Catch: NameNotFoundException -> 0x008b
            android.app.PendingIntent r4 = android.app.PendingIntent.getActivity(r4, r3, r2, r0)     // Catch: NameNotFoundException -> 0x008b
            r3 = 6
            java.lang.String r0 = "Connection progress callbacks cannot be null."
            X.C13020j0.A02(r7, r0)     // Catch: NameNotFoundException -> 0x008b
            r6.A08 = r7     // Catch: NameNotFoundException -> 0x008b
            android.os.Handler r2 = r6.A0G     // Catch: NameNotFoundException -> 0x008b
            java.util.concurrent.atomic.AtomicInteger r0 = r6.A0C     // Catch: NameNotFoundException -> 0x008b
            int r1 = r0.get()     // Catch: NameNotFoundException -> 0x008b
            r0 = 3
            android.os.Message r0 = r2.obtainMessage(r0, r1, r3, r4)     // Catch: NameNotFoundException -> 0x008b
            r2.sendMessage(r0)     // Catch: NameNotFoundException -> 0x008b
            goto L_0x008a
        L_0x0088:
            r1 = 0
            goto L_0x0024
        L_0x008a:
            return
        L_0x008b:
            r4 = 0
            r3 = 16
            java.lang.String r0 = "Connection progress callbacks cannot be null."
            X.C13020j0.A02(r7, r0)
            r6.A08 = r7
            android.os.Handler r2 = r6.A0G
            java.util.concurrent.atomic.AtomicInteger r0 = r6.A0C
            int r1 = r0.get()
            r0 = 3
            android.os.Message r0 = r2.obtainMessage(r0, r1, r3, r4)
            r2.sendMessage(r0)
            return
        L_0x00a6:
            super.A7X(r7)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C56382kn.A7X(X.5Sa):void");
    }

    @Override // X.AbstractC95064d1, X.AbstractC72443eb
    public final boolean Aad() {
        return C12960it.A1T(this.A09.A01() ? 1 : 0);
    }
}
