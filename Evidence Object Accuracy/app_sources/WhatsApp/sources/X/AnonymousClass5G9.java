package X;

import java.util.Arrays;

/* renamed from: X.5G9  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5G9 implements AbstractC117285Zg {
    public int A00;
    public int A01;
    public int A02;
    public AnonymousClass20J A03;
    public AnonymousClass5O8 A04;
    public boolean A05;
    public boolean A06;
    public byte[] A07;
    public byte[] A08;
    public byte[] A09;
    public byte[] A0A;
    public byte[] A0B;

    public final void A01() {
        if (!this.A05) {
            this.A05 = true;
            AnonymousClass20J r5 = this.A03;
            r5.A97(this.A07, 0);
            int i = this.A00;
            byte[] bArr = new byte[i];
            bArr[i - 1] = 2;
            r5.update(bArr, 0, i);
        }
    }

    @Override // X.AnonymousClass5XQ
    public byte[] AE1() {
        int i = this.A02;
        byte[] bArr = new byte[i];
        System.arraycopy(this.A0A, 0, bArr, 0, i);
        return bArr;
    }

    @Override // X.AnonymousClass5XQ
    public int AEp(int i) {
        int i2 = i + this.A01;
        boolean z = this.A06;
        int i3 = this.A02;
        if (z) {
            return i2 + i3;
        }
        int i4 = i2 - i3;
        if (i2 < i3) {
            return 0;
        }
        return i4;
    }

    @Override // X.AnonymousClass5XQ
    public int AHQ(int i) {
        int i2 = i + this.A01;
        if (!this.A06) {
            int i3 = this.A02;
            i2 -= i3;
            if (i2 < i3) {
                return 0;
            }
        }
        return i2 - (i2 % this.A00);
    }

    public AnonymousClass5G9(AnonymousClass5XE r3) {
        int AAt = r3.AAt();
        this.A00 = AAt;
        AnonymousClass5G3 r1 = new AnonymousClass5G3(r3);
        this.A03 = r1;
        this.A0A = new byte[AAt];
        int i = r1.A01;
        this.A07 = new byte[i];
        this.A0B = new byte[i];
        this.A04 = new AnonymousClass5O8(r3);
    }

    public final void A00() {
        byte[] bArr = new byte[this.A00];
        int i = 0;
        this.A03.A97(bArr, 0);
        while (true) {
            byte[] bArr2 = this.A0A;
            if (i < bArr2.length) {
                i = C72453ed.A0Q(bArr, bArr2, i, this.A0B[i] ^ this.A07[i]);
            } else {
                return;
            }
        }
    }

    public final void A02(boolean z) {
        this.A04.reset();
        AnonymousClass20J r5 = this.A03;
        r5.reset();
        this.A01 = 0;
        Arrays.fill(this.A08, (byte) 0);
        if (z) {
            Arrays.fill(this.A0A, (byte) 0);
        }
        int i = this.A00;
        byte[] bArr = new byte[i];
        bArr[i - 1] = 1;
        r5.update(bArr, 0, i);
        this.A05 = false;
        byte[] bArr2 = this.A09;
        if (bArr2 != null) {
            AZX(bArr2, 0, bArr2.length);
        }
    }

    @Override // X.AnonymousClass5XQ
    public int A97(byte[] bArr, int i) {
        A01();
        int i2 = this.A01;
        byte[] bArr2 = this.A08;
        byte[] bArr3 = new byte[bArr2.length];
        this.A01 = 0;
        if (this.A06) {
            int i3 = i + i2;
            if (bArr.length >= this.A02 + i3) {
                this.A04.AZY(bArr2, bArr3, 0, 0);
                System.arraycopy(bArr3, 0, bArr, i, i2);
                this.A03.update(bArr3, 0, i2);
                A00();
                System.arraycopy(this.A0A, 0, bArr, i3, this.A02);
                A02(false);
                return i2 + this.A02;
            }
            throw new C114975Nu("Output buffer too short");
        }
        int i4 = this.A02;
        if (i2 < i4) {
            throw new C114965Nt("data too short");
        } else if (bArr.length >= (i + i2) - i4) {
            if (i2 > i4) {
                this.A03.update(bArr2, 0, i2 - i4);
                this.A04.AZY(this.A08, bArr3, 0, 0);
                System.arraycopy(bArr3, 0, bArr, i, i2 - this.A02);
            }
            A00();
            byte[] bArr4 = this.A08;
            int i5 = this.A02;
            int i6 = i2 - i5;
            int i7 = 0;
            for (int i8 = 0; i8 < i5; i8++) {
                i7 |= this.A0A[i8] ^ bArr4[i6 + i8];
            }
            if (i7 == 0) {
                A02(false);
                return i2 - this.A02;
            }
            throw new C114965Nt("mac check in EAX failed");
        } else {
            throw new C114975Nu("Output buffer too short");
        }
    }

    @Override // X.AnonymousClass5XQ
    public String AAf() {
        StringBuilder A0h = C12960it.A0h();
        C72463ee.A0U(A0h, ((AbstractC112995Fp) this.A04).A00);
        return C12960it.A0d("/EAX", A0h);
    }

    @Override // X.AbstractC117285Zg
    public AnonymousClass5XE AHO() {
        return ((AbstractC112995Fp) this.A04).A00;
    }

    @Override // X.AnonymousClass5XQ
    public void AIf(AnonymousClass20L r8, boolean z) {
        byte[] bArr;
        int AE2;
        AnonymousClass20L r1;
        this.A06 = z;
        if (r8 instanceof C113035Ft) {
            C113035Ft r82 = (C113035Ft) r8;
            bArr = AnonymousClass1TT.A02(r82.A03);
            this.A09 = AnonymousClass1TT.A02(r82.A02);
            AE2 = r82.A00 >> 3;
            this.A02 = AE2;
            r1 = r82.A01;
        } else if (r8 instanceof C113075Fx) {
            C113075Fx r83 = (C113075Fx) r8;
            bArr = r83.A01;
            this.A09 = null;
            AE2 = this.A03.AE2() >> 1;
            this.A02 = AE2;
            r1 = r83.A00;
        } else {
            throw C12970iu.A0f("invalid parameters passed to EAX");
        }
        int i = this.A00;
        if (!z) {
            i += AE2;
        }
        this.A08 = new byte[i];
        byte[] bArr2 = new byte[i];
        AnonymousClass20J r2 = this.A03;
        r2.AIc(r1);
        bArr2[i - 1] = 0;
        r2.update(bArr2, 0, i);
        r2.update(bArr, 0, bArr.length);
        byte[] bArr3 = this.A0B;
        r2.A97(bArr3, 0);
        this.A04.AIf(new C113075Fx(null, bArr3), true);
        A02(true);
    }

    @Override // X.AnonymousClass5XQ
    public void AZX(byte[] bArr, int i, int i2) {
        if (!this.A05) {
            this.A03.update(bArr, i, i2);
            return;
        }
        throw C12960it.A0U("AAD data cannot be added after encryption/decryption processing has begun.");
    }

    @Override // X.AnonymousClass5XQ
    public int AZZ(byte[] bArr, int i, int i2, byte[] bArr2, int i3) {
        int i4;
        A01();
        if (bArr.length >= i + i2) {
            int i5 = 0;
            for (int i6 = 0; i6 != i2; i6++) {
                byte b = bArr[i + i6];
                int i7 = i3 + i5;
                byte[] bArr3 = this.A08;
                int i8 = this.A01;
                int i9 = i8 + 1;
                this.A01 = i9;
                bArr3[i8] = b;
                if (i9 == bArr3.length) {
                    int length = bArr2.length;
                    int i10 = this.A00;
                    if (length >= i7 + i10) {
                        if (this.A06) {
                            i4 = this.A04.AZY(bArr3, bArr2, 0, i7);
                            this.A03.update(bArr2, i7, i10);
                        } else {
                            this.A03.update(bArr3, 0, i10);
                            i4 = this.A04.AZY(this.A08, bArr2, 0, i7);
                        }
                        this.A01 = 0;
                        if (!this.A06) {
                            byte[] bArr4 = this.A08;
                            System.arraycopy(bArr4, i10, bArr4, 0, this.A02);
                            this.A01 = this.A02;
                        }
                    } else {
                        throw new C114975Nu("Output buffer is too short");
                    }
                } else {
                    i4 = 0;
                }
                i5 += i4;
            }
            return i5;
        }
        throw new AnonymousClass5O2("Input buffer too short");
    }
}
