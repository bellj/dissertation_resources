package X;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/* renamed from: X.3tX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C81143tX extends AbstractC71723dP<K, Collection<V>> {
    public final transient Map submap;
    public final /* synthetic */ AbstractC80933tC this$0;

    public C81143tX(AbstractC80933tC r1, Map map) {
        this.this$0 = r1;
        this.submap = map;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public void clear() {
        Map map = this.submap;
        AbstractC80933tC r1 = this.this$0;
        if (map == r1.map) {
            r1.clear();
        } else {
            AnonymousClass1I4.clear(new AnonymousClass5DG(this));
        }
    }

    @Override // java.util.AbstractMap, java.util.Map
    public boolean containsKey(Object obj) {
        return C28271Mk.safeContainsKey(this.submap, obj);
    }

    @Override // X.AbstractC71723dP
    public Set createEntrySet() {
        return new C81273tk(this);
    }

    @Override // java.util.AbstractMap, java.util.Map, java.lang.Object
    public boolean equals(Object obj) {
        return this == obj || this.submap.equals(obj);
    }

    @Override // java.util.AbstractMap, java.util.Map
    public Collection get(Object obj) {
        Collection collection = (Collection) C28271Mk.safeGet(this.submap, obj);
        if (collection == null) {
            return null;
        }
        return this.this$0.wrapCollection(obj, collection);
    }

    @Override // java.util.AbstractMap, java.util.Map, java.lang.Object
    public int hashCode() {
        return this.submap.hashCode();
    }

    @Override // java.util.AbstractMap, java.util.Map
    public Set keySet() {
        return this.this$0.keySet();
    }

    @Override // java.util.AbstractMap, java.util.Map
    public Collection remove(Object obj) {
        Collection collection = (Collection) this.submap.remove(obj);
        if (collection == null) {
            return null;
        }
        Collection createCollection = this.this$0.createCollection();
        createCollection.addAll(collection);
        AbstractC80933tC.access$220(this.this$0, collection.size());
        collection.clear();
        return createCollection;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public int size() {
        return this.submap.size();
    }

    @Override // java.util.AbstractMap, java.lang.Object
    public String toString() {
        return this.submap.toString();
    }

    public Map.Entry wrapEntry(Map.Entry entry) {
        Object key = entry.getKey();
        return C28271Mk.immutableEntry(key, this.this$0.wrapCollection(key, (Collection) entry.getValue()));
    }
}
