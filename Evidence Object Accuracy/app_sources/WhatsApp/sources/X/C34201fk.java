package X;

/* renamed from: X.1fk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C34201fk extends AbstractC28131Kt {
    public final AbstractC14640lm A00;
    public final AbstractC14640lm A01;
    public final String A02;
    public final boolean A03;

    public C34201fk(AbstractC14640lm r2, AbstractC14640lm r3, String str, String str2, boolean z) {
        super(null, str);
        this.A02 = str2;
        this.A03 = z;
        this.A00 = r2;
        this.A01 = r3;
    }
}
