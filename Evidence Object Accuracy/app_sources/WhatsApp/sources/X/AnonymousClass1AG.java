package X;

/* renamed from: X.1AG  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1AG {
    public final AnonymousClass1AF A00;
    public final AnonymousClass1AE A01;
    public final C18580sg A02;
    public final AbstractC16850pr A03;

    public AnonymousClass1AG(AnonymousClass1AF r2, AnonymousClass1AE r3, C18580sg r4, AbstractC16850pr r5) {
        C16700pc.A0E(r5, 2);
        C16700pc.A0E(r3, 3);
        C16700pc.A0E(r2, 4);
        this.A02 = r4;
        this.A03 = r5;
        this.A01 = r3;
        this.A00 = r2;
    }
}
