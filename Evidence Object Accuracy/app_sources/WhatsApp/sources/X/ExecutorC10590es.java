package X;

import java.util.concurrent.Executor;

/* renamed from: X.0es  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class ExecutorC10590es implements Executor {
    public final /* synthetic */ C07760a2 A00;

    public ExecutorC10590es(C07760a2 r1) {
        this.A00 = r1;
    }

    @Override // java.util.concurrent.Executor
    public void execute(Runnable runnable) {
        this.A00.A00.post(runnable);
    }
}
