package X;

import com.whatsapp.contact.picker.ContactPickerFragment;
import java.util.ArrayList;

/* renamed from: X.4rG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103754rG implements AnonymousClass07L {
    public final /* synthetic */ ContactPickerFragment A00;

    @Override // X.AnonymousClass07L
    public boolean AUY(String str) {
        return false;
    }

    public C103754rG(ContactPickerFragment contactPickerFragment) {
        this.A00 = contactPickerFragment;
    }

    @Override // X.AnonymousClass07L
    public boolean AUX(String str) {
        ContactPickerFragment contactPickerFragment = this.A00;
        contactPickerFragment.A1p = str;
        ArrayList A02 = C32751cg.A02(contactPickerFragment.A16, str);
        contactPickerFragment.A1z = A02;
        if (A02.isEmpty()) {
            contactPickerFragment.A1z = null;
        }
        contactPickerFragment.A1K();
        return false;
    }
}
