package X;

import android.view.MenuItem;
import com.whatsapp.contact.picker.invite.InviteNonWhatsAppContactPickerActivity;

/* renamed from: X.4mV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class MenuItem$OnActionExpandListenerC100804mV implements MenuItem.OnActionExpandListener {
    public final /* synthetic */ AbstractC36931kt A00;
    public final /* synthetic */ C48232Fc A01;

    public MenuItem$OnActionExpandListenerC100804mV(AbstractC36931kt r1, C48232Fc r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // android.view.MenuItem.OnActionExpandListener
    public boolean onMenuItemActionCollapse(MenuItem menuItem) {
        AbstractC36931kt r0 = this.A00;
        if (r0 == null) {
            return true;
        }
        ((InviteNonWhatsAppContactPickerActivity) r0).A0K.A04(null);
        return true;
    }

    @Override // android.view.MenuItem.OnActionExpandListener
    public boolean onMenuItemActionExpand(MenuItem menuItem) {
        return true;
    }
}
