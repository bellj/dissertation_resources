package X;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;

/* renamed from: X.0UI  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0UI {
    public static int A00(Object obj) {
        if (Build.VERSION.SDK_INT >= 28) {
            return C06510Tx.A00(obj);
        }
        try {
            return ((Number) obj.getClass().getMethod("getResId", new Class[0]).invoke(obj, new Object[0])).intValue();
        } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            Log.e("IconCompat", "Unable to get icon resource", e);
            return 0;
        }
    }

    public static int A01(Object obj) {
        if (Build.VERSION.SDK_INT >= 28) {
            return C06510Tx.A01(obj);
        }
        try {
            return ((Number) obj.getClass().getMethod("getType", new Class[0]).invoke(obj, new Object[0])).intValue();
        } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to get icon type ");
            sb.append(obj);
            Log.e("IconCompat", sb.toString(), e);
            return -1;
        }
    }

    public static Drawable A02(Context context, Icon icon) {
        return icon.loadDrawable(context);
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0078  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.drawable.Icon A03(android.content.Context r4, androidx.core.graphics.drawable.IconCompat r5) {
        /*
            int r0 = r5.A02
            r3 = 0
            r2 = 26
            switch(r0) {
                case -1: goto L_0x0010;
                case 0: goto L_0x0008;
                case 1: goto L_0x0015;
                case 2: goto L_0x001e;
                case 3: goto L_0x0029;
                case 4: goto L_0x0036;
                case 5: goto L_0x003f;
                case 6: goto L_0x0046;
                default: goto L_0x0008;
            }
        L_0x0008:
            java.lang.String r1 = "Unknown type"
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>(r1)
            throw r0
        L_0x0010:
            java.lang.Object r0 = r5.A06
            android.graphics.drawable.Icon r0 = (android.graphics.drawable.Icon) r0
            return r0
        L_0x0015:
            java.lang.Object r0 = r5.A06
            android.graphics.Bitmap r0 = (android.graphics.Bitmap) r0
            android.graphics.drawable.Icon r2 = android.graphics.drawable.Icon.createWithBitmap(r0)
            goto L_0x0054
        L_0x001e:
            java.lang.String r1 = r5.A0C()
            int r0 = r5.A00
            android.graphics.drawable.Icon r2 = android.graphics.drawable.Icon.createWithResource(r1, r0)
            goto L_0x0054
        L_0x0029:
            java.lang.Object r2 = r5.A06
            byte[] r2 = (byte[]) r2
            int r1 = r5.A00
            int r0 = r5.A01
            android.graphics.drawable.Icon r2 = android.graphics.drawable.Icon.createWithData(r2, r1, r0)
            goto L_0x0054
        L_0x0036:
            java.lang.Object r0 = r5.A06
            java.lang.String r0 = (java.lang.String) r0
            android.graphics.drawable.Icon r2 = android.graphics.drawable.Icon.createWithContentUri(r0)
            goto L_0x0054
        L_0x003f:
            int r1 = android.os.Build.VERSION.SDK_INT
            java.lang.Object r0 = r5.A06
            android.graphics.Bitmap r0 = (android.graphics.Bitmap) r0
            goto L_0x0071
        L_0x0046:
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 30
            if (r1 < r0) goto L_0x0065
            android.net.Uri r0 = r5.A09()
            android.graphics.drawable.Icon r2 = X.C04060Kf.A00(r0)
        L_0x0054:
            android.content.res.ColorStateList r0 = r5.A03
            if (r0 == 0) goto L_0x005b
            r2.setTintList(r0)
        L_0x005b:
            android.graphics.PorterDuff$Mode r1 = r5.A04
            android.graphics.PorterDuff$Mode r0 = androidx.core.graphics.drawable.IconCompat.A0A
            if (r1 == r0) goto L_0x0064
            r2.setTintMode(r1)
        L_0x0064:
            return r2
        L_0x0065:
            if (r4 == 0) goto L_0x0099
            java.io.InputStream r0 = r5.A0B(r4)
            if (r0 == 0) goto L_0x0081
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r0)
        L_0x0071:
            if (r1 < r2) goto L_0x0078
            android.graphics.drawable.Icon r2 = X.C05650Qk.A01(r0)
            goto L_0x0054
        L_0x0078:
            android.graphics.Bitmap r0 = androidx.core.graphics.drawable.IconCompat.A01(r0, r3)
            android.graphics.drawable.Icon r2 = android.graphics.drawable.Icon.createWithBitmap(r0)
            goto L_0x0054
        L_0x0081:
            java.lang.String r0 = "Cannot load adaptive icon from uri: "
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            android.net.Uri r0 = r5.A09()
            r1.append(r0)
            java.lang.String r1 = r1.toString()
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>(r1)
            throw r0
        L_0x0099:
            java.lang.String r0 = "Context is required to resolve the file uri of the icon: "
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            android.net.Uri r0 = r5.A09()
            r1.append(r0)
            java.lang.String r1 = r1.toString()
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0UI.A03(android.content.Context, androidx.core.graphics.drawable.IconCompat):android.graphics.drawable.Icon");
    }

    public static Uri A04(Object obj) {
        if (Build.VERSION.SDK_INT >= 28) {
            return C06510Tx.A02(obj);
        }
        try {
            return (Uri) obj.getClass().getMethod("getUri", new Class[0]).invoke(obj, new Object[0]);
        } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            Log.e("IconCompat", "Unable to get icon uri", e);
            return null;
        }
    }

    public static String A05(Object obj) {
        if (Build.VERSION.SDK_INT >= 28) {
            return C06510Tx.A03(obj);
        }
        try {
            return (String) obj.getClass().getMethod("getResPackage", new Class[0]).invoke(obj, new Object[0]);
        } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            Log.e("IconCompat", "Unable to get icon package", e);
            return null;
        }
    }
}
