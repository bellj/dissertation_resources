package X;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.view.View;
import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.jid.UserJid;
import com.whatsapp.location.LocationPicker;
import com.whatsapp.location.LocationPicker2;
import com.whatsapp.mentions.MentionableEntry;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1vM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C42531vM extends C28791Pa implements AbstractC42541vN {
    public int A00;
    public C42561vP A01;
    public final ActivityC000800j A02;
    public final AbstractC13860kS A03;
    public final C14330lG A04;
    public final C14900mE A05;
    public final C15570nT A06;
    public final C239613r A07;
    public final C15450nH A08;
    public final C16170oZ A09;
    public final C254719n A0A;
    public final C14650lo A0B;
    public final AnonymousClass19N A0C;
    public final AnonymousClass19X A0D;
    public final AnonymousClass1AA A0E;
    public final C253619c A0F;
    public final C238013b A0G;
    public final AnonymousClass116 A0H;
    public final AbstractC13910kX A0I;
    public final AnonymousClass4U0 A0J;
    public final AnonymousClass11P A0K;
    public final AbstractC32851cq A0L;
    public final C17050qB A0M;
    public final AnonymousClass01d A0N;
    public final C15890o4 A0O;
    public final C14820m6 A0P;
    public final C14950mJ A0Q;
    public final C14850m9 A0R;
    public final C16120oU A0S;
    public final AbstractC14640lm A0T;
    public final C244415n A0U;
    public final MentionableEntry A0V;
    public final AnonymousClass1IZ A0W;
    public final C21200x2 A0X;
    public final C16630pM A0Y;
    public final AbstractC44971zr A0Z;
    public final C21820y2 A0a;
    public final AnonymousClass5WG A0b = new C69613Zx(this);
    public final C252718t A0c;
    public final C22190yg A0d;
    public final boolean A0e;

    public C42531vM(ActivityC000800j r15, AbstractC13860kS r16, C14330lG r17, C14900mE r18, C15570nT r19, C239613r r20, C15450nH r21, C16170oZ r22, C254719n r23, C14650lo r24, AnonymousClass19N r25, AnonymousClass19X r26, AnonymousClass1AA r27, C253619c r28, C238013b r29, AnonymousClass116 r30, AbstractC13910kX r31, AnonymousClass4U0 r32, AnonymousClass11P r33, AbstractC32851cq r34, C17050qB r35, AnonymousClass01d r36, C15890o4 r37, C14820m6 r38, C14950mJ r39, C14850m9 r40, C16120oU r41, AbstractC14640lm r42, C244415n r43, MentionableEntry mentionableEntry, C74503iB r45, AnonymousClass1A2 r46, C21200x2 r47, C16630pM r48, C21820y2 r49, C252718t r50, C22190yg r51, boolean z) {
        AnonymousClass598 r0 = new AbstractC44971zr() { // from class: X.598
            @Override // X.AbstractC44971zr
            public final void AVX(boolean z2) {
                C42561vP r02;
                C42531vM r1 = C42531vM.this;
                if (z2 && (r02 = r1.A01) != null && r02.isShowing()) {
                    r1.A01.A04();
                }
            }
        };
        this.A0Z = r0;
        this.A02 = r15;
        this.A03 = r16;
        this.A0R = r40;
        this.A05 = r18;
        this.A0c = r50;
        this.A06 = r19;
        this.A07 = r20;
        this.A04 = r17;
        this.A0S = r41;
        this.A08 = r21;
        this.A0D = r26;
        this.A09 = r22;
        this.A0C = r25;
        this.A0Q = r39;
        this.A0U = r43;
        this.A0F = r28;
        this.A0d = r51;
        this.A0N = r36;
        this.A0G = r29;
        this.A0X = r47;
        this.A0M = r35;
        this.A0H = r30;
        this.A0O = r37;
        this.A0P = r38;
        this.A0A = r23;
        this.A0B = r24;
        this.A0a = r49;
        this.A0Y = r48;
        this.A0K = r33;
        this.A0I = r31;
        this.A0L = r34;
        this.A0T = r42;
        this.A0e = z;
        this.A0V = mentionableEntry;
        this.A0J = r32;
        r49.A03(r0);
        this.A0E = r27;
        RunnableBRunnable0Shape4S0100000_I0_4 runnableBRunnable0Shape4S0100000_I0_4 = new RunnableBRunnable0Shape4S0100000_I0_4(this, 43);
        C14900mE r5 = r46.A00;
        C15570nT r6 = r46.A01;
        C17070qD r9 = r46.A04;
        this.A0W = new AnonymousClass1IZ(r15, r16, r5, r6, r46.A02, r46.A03, r9, r45, runnableBRunnable0Shape4S0100000_I0_4, null, true);
    }

    public void A00() {
        ActivityC000800j r2 = this.A02;
        if (RequestPermissionActivity.A0T(r2, this.A0O, 31) && this.A0M.A04(this.A0L)) {
            C22190yg.A01(r2, this.A04, this.A05, null, this.A0Y, 23);
        }
    }

    public void A01() {
        ActivityC000800j r2 = this.A02;
        if (RequestPermissionActivity.A0T(r2, this.A0O, 32) && this.A0M.A04(this.A0L)) {
            C22190yg.A01(r2, this.A04, this.A05, null, this.A0Y, 4);
        }
    }

    public final void A02() {
        if (A0D(this.A0J.A05) && this.A0M.A04(this.A0L)) {
            if (this.A0R.A07(931)) {
                C40691s6.A08(this.A0K, this.A0P, this.A0T);
            }
            C14900mE r2 = this.A05;
            C22190yg.A01(this.A02, this.A04, r2, this.A0T, this.A0Y, 5);
        }
    }

    public final void A03() {
        AbstractC15340mz AG1 = this.A0I.AG1();
        this.A06.A08();
        Bundle bundle = null;
        ActivityC000800j r6 = this.A02;
        AbstractC14640lm r5 = this.A0T;
        if (AG1 != null) {
            bundle = C30041Vv.A03(AG1.A0z);
        }
        C15580nU A05 = C30041Vv.A05(AG1);
        boolean z = this.A0e;
        Intent intent = new Intent();
        intent.setClassName(r6.getPackageName(), "com.whatsapp.contact.picker.PhoneContactsSelector");
        intent.putExtra("jid", C15380n4.A03(r5));
        intent.putExtra("quoted_message", bundle);
        intent.putExtra("quoted_group_jid", C15380n4.A03(A05));
        intent.putExtra("has_number_from_url", z);
        r6.startActivityForResult(intent, 9);
    }

    public final void A04() {
        if (A0D(this.A0J.A06) && this.A0M.A04(this.A0L)) {
            ActivityC000800j r4 = this.A02;
            AbstractC14640lm r3 = this.A0T;
            Intent intent = new Intent();
            intent.setClassName(r4.getPackageName(), "com.whatsapp.documentpicker.DocumentPickerActivity");
            intent.putExtra("jid", r3.getRawString());
            r4.startActivityForResult(intent, 6);
        }
    }

    public final void A05() {
        Class cls;
        long j;
        ActivityC000800j r4 = this.A02;
        View currentFocus = r4.getCurrentFocus();
        if (currentFocus != null) {
            this.A0c.A01(currentFocus);
        }
        AbstractC15340mz AG1 = this.A0I.AG1();
        if (this.A0U.A06(r4)) {
            cls = LocationPicker2.class;
        } else {
            cls = LocationPicker.class;
        }
        Intent intent = new Intent(r4, cls);
        intent.putExtra("jid", C15380n4.A03(this.A0T));
        if (AG1 == null) {
            j = 0;
        } else {
            j = AG1.A11;
        }
        intent.putExtra("quoted_message_row_id", j);
        intent.putExtra("quoted_group_jid", C15380n4.A03(C30041Vv.A05(AG1)));
        intent.putExtra("has_number_from_url", this.A0e);
        r4.startActivityForResult(intent, this.A0J.A09);
    }

    public final void A06() {
        long j;
        AbstractC15340mz AG1 = this.A0I.AG1();
        if (A0D(this.A0J.A07) && this.A0M.A04(this.A0L)) {
            ActivityC000800j r4 = this.A02;
            AbstractC14640lm r10 = this.A0T;
            if (AG1 == null) {
                j = 0;
            } else {
                j = AG1.A11;
            }
            C15580nU A05 = C30041Vv.A05(AG1);
            boolean z = this.A0e;
            MentionableEntry mentionableEntry = this.A0V;
            String A04 = AbstractC32741cf.A04(mentionableEntry.getStringText());
            List mentions = mentionableEntry.getMentions();
            int i = 1;
            if (this.A00 == 5) {
                i = 20;
            }
            Intent intent = new Intent();
            intent.setClassName(r4.getPackageName(), "com.whatsapp.gallerypicker.GalleryPicker");
            intent.setAction("android.intent.action.PICK");
            intent.setData(MediaStore.Images.Media.INTERNAL_CONTENT_URI);
            intent.putExtra("max_items", 30);
            intent.putExtra("jid", r10.getRawString());
            intent.putExtra("quoted_message_row_id", j);
            intent.putExtra("quoted_group_jid", C15380n4.A03(A05));
            intent.putExtra("number_from_url", z);
            intent.putExtra("send", true);
            intent.putExtra("picker_open_time", SystemClock.elapsedRealtime());
            intent.putExtra("origin", i);
            intent.putExtra("android.intent.extra.TEXT", A04);
            intent.putExtra("mentions", C15380n4.A06(mentions));
            r4.startActivityForResult(intent, 22);
        }
    }

    public final void A07() {
        if (A0D(this.A0J.A08) && this.A0M.A04(this.A0L)) {
            C14900mE r2 = this.A05;
            C22190yg.A01(this.A02, this.A04, r2, null, this.A0Y, 21);
        }
    }

    public void A08(int i) {
        int i2;
        long j;
        C21200x2 r5 = this.A0X;
        int i3 = 2;
        if (i != 1) {
            int i4 = 3;
            if (i != 2) {
                i3 = 4;
                if (i != 3) {
                    i4 = 5;
                    if (i != 4) {
                        i3 = 6;
                        if (i != 5) {
                            i4 = 7;
                            if (i != 7) {
                                if (i == 6) {
                                    i4 = 8;
                                } else {
                                    i3 = 1;
                                }
                            }
                        }
                    }
                }
            }
            i3 = i4;
        }
        if (r5.A01 != 0 && Math.random() * ((double) 650) < 1.0d) {
            r5.A01 = 1;
            r5.A02 = SystemClock.elapsedRealtime();
            r5.A00 = i3;
        }
        ActivityC000800j r2 = this.A02;
        Intent A09 = RequestPermissionActivity.A09(r2, this.A0O, 30);
        if (A09 != null) {
            if (i == 2) {
                i2 = this.A0J.A00;
            } else if (i == 3) {
                i2 = this.A0J.A01;
            } else if (i != 5) {
                i2 = 30;
            } else {
                i2 = this.A0J.A02;
            }
            r2.startActivityForResult(A09, i2);
        } else if (this.A0M.A04(this.A0L)) {
            if (this.A0Q.A01() < ((long) ((this.A08.A02(AbstractC15460nI.A1p) << 10) << 10))) {
                C33471e8.A04(r2, this.A03, this.A0S, 5);
            } else {
                C238013b r1 = this.A0G;
                AbstractC14640lm r10 = this.A0T;
                if (r1.A0I(UserJid.of(r10))) {
                    C36021jC.A01(r2, 106);
                } else {
                    AbstractC15340mz AG1 = this.A0I.AG1();
                    if (AG1 == null) {
                        j = 0;
                    } else {
                        j = AG1.A11;
                    }
                    String A03 = C15380n4.A03(C30041Vv.A05(AG1));
                    boolean z = this.A0e;
                    MentionableEntry mentionableEntry = this.A0V;
                    String A04 = AbstractC32741cf.A04(mentionableEntry.getStringText());
                    List mentions = mentionableEntry.getMentions();
                    Intent intent = new Intent();
                    intent.setClassName(r2.getPackageName(), "com.whatsapp.camera.CameraActivity");
                    intent.putExtra("jid", C15380n4.A03(r10));
                    intent.putExtra("quoted_message_row_id", j);
                    intent.putExtra("quoted_group_jid", A03);
                    intent.putExtra("chat_opened_from_url", z);
                    intent.putExtra("origin", i);
                    intent.putExtra("android.intent.extra.TEXT", A04);
                    intent.putStringArrayListExtra("mentions", C15380n4.A06(mentions));
                    r2.startActivityForResult(intent, this.A0J.A03);
                    this.A0K.A04();
                    return;
                }
            }
        }
        r5.A00();
    }

    public void A09(int i, String str) {
        C42561vP r0 = this.A01;
        if (r0 != null) {
            r0.dismiss();
        }
        this.A0W.A00(this.A0T, this.A0I.AG1(), AbstractC32741cf.A04(this.A0V.getStringText()), str, i, false);
    }

    public final void A0A(Uri uri, Byte b, int i) {
        if (uri != null) {
            ArrayList arrayList = new ArrayList();
            arrayList.add(uri);
            ActivityC000800j r3 = this.A02;
            AnonymousClass24W r4 = new AnonymousClass24W(r3);
            r4.A0C = arrayList;
            r4.A08 = C15380n4.A03(this.A0T);
            r4.A01 = i;
            r4.A0G = true;
            C39341ph r2 = new C39341ph(uri);
            MentionableEntry mentionableEntry = this.A0V;
            r2.A0D(AbstractC32741cf.A04(mentionableEntry.getStringText()));
            r2.A0E(AnonymousClass1Y6.A00(mentionableEntry.getMentions()));
            r2.A0C(b);
            C453421e r1 = new C453421e(r2);
            Bundle bundle = new Bundle();
            r1.A02(bundle);
            r4.A06 = bundle;
            AbstractC15340mz AG1 = this.A0I.AG1();
            if (AG1 != null) {
                r4.A04 = AG1.A11;
                r4.A09 = C15380n4.A03(C30041Vv.A05(AG1));
            }
            r3.startActivityForResult(r4.A00(), 22);
            return;
        }
        Log.e("conversation/setuppreview/share-failed");
        this.A05.A07(R.string.share_failed, 0);
    }

    public void A0B(View view, int i) {
        ActivityC000800j r1 = this.A02;
        C14900mE r3 = this.A05;
        C252718t r7 = this.A0c;
        C42561vP r0 = new C42561vP(r1, view, r3, this, this.A0N, this.A0T, r7);
        this.A01 = r0;
        this.A00 = i;
        r0.A07(r1);
    }

    /* JADX WARNING: Removed duplicated region for block: B:62:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0C(java.lang.String r8, boolean r9) {
        /*
        // Method dump skipped, instructions count: 322
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C42531vM.A0C(java.lang.String, boolean):void");
    }

    public final boolean A0D(int i) {
        if (this.A0O.A07()) {
            return true;
        }
        ActivityC000800j r4 = this.A02;
        int i2 = Build.VERSION.SDK_INT;
        int i3 = R.string.permission_storage_need_write_access_on_sending_media_v30;
        if (i2 < 30) {
            i3 = R.string.permission_storage_need_write_access_on_sending_media;
        }
        RequestPermissionActivity.A0L(r4, R.string.permission_storage_need_write_access_on_sending_media_request, i3, i);
        return false;
    }

    @Override // X.AbstractC42541vN
    public boolean ALt(Intent intent, int i, int i2) {
        int i3;
        String str;
        Uri fromFile;
        AnonymousClass4U0 r5 = this.A0J;
        if (i == r5.A04) {
            A05();
        } else if (i == 23 && i2 == 0) {
            C16630pM r2 = this.A0Y;
            synchronized (C22200yh.class) {
                if (C22200yh.A00 > 0) {
                    SharedPreferences.Editor edit = r2.A01(AnonymousClass01V.A07).edit();
                    int i4 = C22200yh.A00 - 1;
                    C22200yh.A00 = i4;
                    edit.putInt("file_index", i4);
                    edit.apply();
                }
            }
            return true;
        } else {
            if (i2 == -1) {
                if (i == 30 || i == r5.A00) {
                    i3 = 2;
                } else if (i == r5.A01) {
                    i3 = 3;
                } else {
                    i3 = 5;
                    if (i != r5.A02) {
                        if (i == 31) {
                            A00();
                            return true;
                        } else if (i == 32) {
                            A01();
                            return true;
                        } else if (i == 150) {
                            A03();
                            return true;
                        } else if (i == r5.A07) {
                            A06();
                            return true;
                        } else if (i == r5.A08) {
                            A07();
                            return true;
                        } else if (i == r5.A05) {
                            A02();
                            return true;
                        } else if (i == r5.A06) {
                            A04();
                            return true;
                        } else if (i != 44) {
                            if (i == 6) {
                                AnonymousClass009.A05(intent);
                                Uri uri = (Uri) intent.getParcelableExtra("uri");
                                Uri data = intent.getData();
                                List A07 = C15380n4.A07(AbstractC14640lm.class, intent.getStringArrayListExtra("jids"));
                                if (uri != null) {
                                    String stringExtra = intent.getStringExtra("caption");
                                    List A072 = C15380n4.A07(UserJid.class, intent.getStringArrayListExtra("mentions"));
                                    for (Object obj : A07) {
                                        C239613r r4 = this.A07;
                                        String A0N = C22200yh.A0N(uri, this.A0N);
                                        r4.A01(uri, this.A03, this.A0I.AG1(), A0N, stringExtra, Collections.singletonList(obj), A072, this.A0e);
                                    }
                                } else if (data != null) {
                                    C239613r r3 = this.A07;
                                    AbstractC14640lm r22 = this.A0T;
                                    String A0N2 = C22200yh.A0N(data, this.A0N);
                                    r3.A01(data, this.A03, this.A0I.AG1(), A0N2, null, Collections.singletonList(r22), null, this.A0e);
                                } else {
                                    ArrayList parcelableArrayListExtra = intent.getParcelableArrayListExtra("android.intent.extra.STREAM");
                                    if (parcelableArrayListExtra != null) {
                                        Iterator it = parcelableArrayListExtra.iterator();
                                        while (it.hasNext()) {
                                            Uri uri2 = (Uri) it.next();
                                            C239613r r42 = this.A07;
                                            AbstractC14640lm r23 = this.A0T;
                                            String A0N3 = C22200yh.A0N(uri2, this.A0N);
                                            r42.A01(uri2, this.A03, this.A0I.AG1(), A0N3, null, Collections.singletonList(r23), null, this.A0e);
                                        }
                                    }
                                }
                                this.A0I.A6H(5);
                                return true;
                            } else if (i != 30) {
                                if (i == 5) {
                                    AnonymousClass009.A05(intent);
                                    ArrayList parcelableArrayListExtra2 = intent.getParcelableArrayListExtra("result_uris");
                                    if (parcelableArrayListExtra2 == null) {
                                        if (intent.getData() != null) {
                                            parcelableArrayListExtra2 = new ArrayList();
                                            parcelableArrayListExtra2.add(intent.getData());
                                        } else {
                                            str = "(conversation|messagereply)/audio/share/failed";
                                            Log.w(str);
                                            this.A05.A07(R.string.share_failed, 0);
                                            return true;
                                        }
                                    }
                                    Iterator it2 = parcelableArrayListExtra2.iterator();
                                    while (it2.hasNext()) {
                                        this.A0d.A0D((Uri) it2.next(), this.A03, new AbstractC39321pf(intent, this) { // from class: X.3aq
                                            public final /* synthetic */ Intent A00;
                                            public final /* synthetic */ C42531vM A01;

                                            {
                                                this.A01 = r2;
                                                this.A00 = r1;
                                            }

                                            @Override // X.AbstractC39321pf
                                            public final void AQW(File file) {
                                                C42531vM r43 = this.A01;
                                                Intent intent2 = this.A00;
                                                try {
                                                    C239613r r52 = r43.A07;
                                                    AbstractC14640lm r24 = r43.A0T;
                                                    C14370lK r8 = C14370lK.A05;
                                                    boolean booleanExtra = intent2.getBooleanExtra("has_preview", true);
                                                    AbstractC13910kX r1 = r43.A0I;
                                                    r52.A06(null, r1.AG1(), r8, file, null, Collections.singletonList(r24), false, booleanExtra, r43.A0e);
                                                    r1.A6H(6);
                                                } catch (IOException e) {
                                                    r43.A05.A07(R.string.share_failed, 0);
                                                    Log.e(e);
                                                }
                                            }
                                        });
                                        this.A0I.A6G();
                                    }
                                } else if (i == 23) {
                                    Uri fromFile2 = Uri.fromFile(C39311pe.A01(this.A0Y));
                                    C22200yh.A0P(this.A02, fromFile2);
                                    A0A(fromFile2, null, 8);
                                    return true;
                                } else if (i == 4) {
                                    if (intent == null || intent.getData() == null) {
                                        File A01 = C39311pe.A01(this.A0Y);
                                        if (A01.exists()) {
                                            fromFile = Uri.fromFile(A01);
                                            C22200yh.A0P(this.A02, fromFile);
                                        } else {
                                            StringBuilder sb = new StringBuilder("conversation/video/share/nocapturefile ");
                                            sb.append(A01);
                                            Log.e(sb.toString());
                                            str = "conversation/video/share/failed";
                                            Log.w(str);
                                            this.A05.A07(R.string.share_failed, 0);
                                            return true;
                                        }
                                    } else {
                                        fromFile = intent.getData();
                                    }
                                    if (fromFile != null) {
                                        A0A(fromFile, null, 8);
                                        return true;
                                    }
                                    str = "conversation/video/share/failed";
                                    Log.w(str);
                                    this.A05.A07(R.string.share_failed, 0);
                                    return true;
                                } else if (i == 21) {
                                    if (intent != null) {
                                        A0A(intent.getData(), null, 1);
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
                A08(i3);
                return true;
            }
            return false;
        }
        return true;
    }

    @Override // X.C28791Pa, android.app.Application.ActivityLifecycleCallbacks
    public void onActivityDestroyed(Activity activity) {
        C42561vP r0 = this.A01;
        if (r0 != null && r0.isShowing()) {
            this.A01.A04();
        }
        this.A0a.A04(this.A0Z);
    }
}
