package X;

import java.io.IOException;
import java.util.Arrays;

/* renamed from: X.5NR  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5NR extends AnonymousClass1TL implements AnonymousClass5VP {
    public static final char[] A01 = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    public final byte[] A00;

    public AnonymousClass5NR(byte[] bArr) {
        this.A00 = AnonymousClass1TT.A02(bArr);
    }

    @Override // X.AnonymousClass1TL
    public void A08(AnonymousClass1TP r3, boolean z) {
        r3.A06(this.A00, 28, z);
    }

    @Override // X.AnonymousClass1TL
    public boolean A09() {
        return false;
    }

    @Override // X.AnonymousClass5VP
    public String AGy() {
        StringBuffer stringBuffer = new StringBuffer("#");
        try {
            byte[] A012 = A01();
            for (int i = 0; i != A012.length; i++) {
                char[] cArr = A01;
                stringBuffer.append(cArr[(A012[i] >>> 4) & 15]);
                stringBuffer.append(cArr[A012[i] & 15]);
            }
            return stringBuffer.toString();
        } catch (IOException unused) {
            throw new AnonymousClass4CU("internal error encoding UniversalString");
        }
    }

    @Override // X.AnonymousClass1TL, X.AnonymousClass1TM
    public int hashCode() {
        return AnonymousClass1TT.A00(this.A00);
    }

    public String toString() {
        return AGy();
    }

    @Override // X.AnonymousClass1TL
    public int A05() {
        return C72453ed.A0H(this.A00);
    }

    @Override // X.AnonymousClass1TL
    public boolean A0A(AnonymousClass1TL r3) {
        if (!(r3 instanceof AnonymousClass5NR)) {
            return false;
        }
        return Arrays.equals(this.A00, ((AnonymousClass5NR) r3).A00);
    }
}
