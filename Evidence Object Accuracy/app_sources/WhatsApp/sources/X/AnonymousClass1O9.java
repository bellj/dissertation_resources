package X;

import com.facebook.redex.RunnableBRunnable0Shape6S0100000_I0_6;

/* renamed from: X.1O9  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1O9 extends C006202y {
    public C89464Kb A00;

    public AnonymousClass1O9(int i) {
        super(i);
    }

    @Override // X.C006202y
    public void A09(Object obj, Object obj2, Object obj3, boolean z) {
        C89464Kb r0;
        synchronized (this) {
            r0 = this.A00;
        }
        if (r0 != null) {
            r0.A00.A02().execute(new RunnableBRunnable0Shape6S0100000_I0_6(obj2, 36));
        }
    }
}
