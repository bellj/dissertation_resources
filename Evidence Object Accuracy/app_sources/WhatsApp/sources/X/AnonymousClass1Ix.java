package X;

import com.whatsapp.R;

/* renamed from: X.1Ix  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1Ix {
    public static String A00(C15550nR r5, C15610nY r6, C16590pI r7, AbstractC15340mz r8) {
        AnonymousClass1IS r1 = r8.A0z;
        if (r1.A02) {
            return r7.A02(R.string.you);
        }
        AbstractC14640lm r0 = r1.A00;
        boolean A0J = C15380n4.A0J(r0);
        if (A0J) {
            r0 = r8.A0B();
        }
        if (r0 != null) {
            C15370n3 A0B = r5.A0B(r0);
            String A06 = r6.A06(A0B);
            if (A06 == null) {
                int i = 2;
                if (A0J) {
                    i = 1;
                }
                A06 = r6.A0B(A0B, i, false);
                if (A06 != null) {
                }
            }
            return A06;
        }
        return "";
    }
}
