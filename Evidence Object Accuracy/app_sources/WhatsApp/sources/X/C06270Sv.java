package X;

import android.content.res.Resources;
import android.graphics.Outline;
import android.graphics.drawable.Drawable;

/* renamed from: X.0Sv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06270Sv {
    public static Resources A00(Resources.Theme theme) {
        return theme.getResources();
    }

    public static void A01(Outline outline, Drawable drawable) {
        drawable.getOutline(outline);
    }

    public static boolean A02(Drawable.ConstantState constantState) {
        return constantState.canApplyTheme();
    }
}
