package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

/* renamed from: X.1ZR  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1ZR implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C100114lO();
    public Object A00;
    public final AnonymousClass2SN A01;
    public final Class A02;
    public final String A03;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass1ZR(AnonymousClass2SN r1, Class cls, Object obj, String str) {
        this.A03 = str;
        this.A01 = r1;
        this.A02 = cls;
        this.A00 = obj;
    }

    public AnonymousClass1ZR(Parcel parcel) {
        AnonymousClass2SN r0;
        String readString = parcel.readString();
        AnonymousClass009.A05(readString);
        this.A03 = readString;
        int readInt = parcel.readInt();
        if (readInt == 1) {
            r0 = new AnonymousClass2SM();
        } else if (readInt == 2) {
            r0 = new AnonymousClass57X();
        } else if (readInt == 3) {
            r0 = new C69053Xt(parcel);
        } else {
            throw new AssertionError("[PrivacyPolicy] Unknown executor read from parcel");
        }
        this.A01 = r0;
        Object readValue = parcel.readValue(Class.class.getClassLoader());
        AnonymousClass009.A05(readValue);
        Class cls = (Class) readValue;
        this.A02 = cls;
        this.A00 = parcel.readValue(cls.getClassLoader());
    }

    public boolean A00() {
        boolean isEmpty;
        Object obj = this.A00;
        if (obj == null) {
            return true;
        }
        Class cls = this.A02;
        if (cls == String.class) {
            isEmpty = ((String) obj).isEmpty();
        } else if (cls.isArray()) {
            if (Array.getLength(obj) == 0) {
                return true;
            }
            return false;
        } else if (obj instanceof Collection) {
            isEmpty = ((Collection) obj).isEmpty();
        } else if (obj instanceof Map) {
            isEmpty = ((Map) obj).isEmpty();
        } else {
            throw new UnsupportedOperationException("empty check not implemented for class type");
        }
        if (isEmpty) {
            return true;
        }
        return false;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass1ZR r5 = (AnonymousClass1ZR) obj;
            if (!this.A03.equals(r5.A03) || !this.A01.equals(r5.A01) || !this.A02.equals(r5.A02) || !C29941Vi.A00(this.A00, r5.A00)) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A03, this.A01, this.A02, this.A00});
    }

    @Override // java.lang.Object
    public String toString() {
        String A64 = this.A01.A64(this.A03, this.A00);
        return A64 == null ? "null" : A64;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        int i2;
        parcel.writeString(this.A03);
        AnonymousClass2SN r1 = this.A01;
        if (r1 instanceof AnonymousClass2SM) {
            i2 = 1;
        } else if (r1 instanceof AnonymousClass57X) {
            i2 = 2;
        } else if (r1 instanceof C69053Xt) {
            parcel.writeInt(3);
            throw new NullPointerException("getClass");
        } else {
            throw new AssertionError("[PrivacyPolicy] Unknown executor written to parcel");
        }
        parcel.writeInt(i2);
        parcel.writeValue(this.A02);
        parcel.writeValue(this.A00);
    }
}
