package X;

import org.wawebrtc.MediaCodecVideoEncoder;

/* renamed from: X.4cQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C94714cQ {
    public static final int[] A00 = {32000, 64000, 96000, 128000, 160000, 192000, 224000, 256000, 288000, 320000, 352000, 384000, 416000, 448000};
    public static final int[] A01 = {32000, 48000, 56000, 64000, 80000, 96000, 112000, 128000, 160000, 192000, 224000, 256000, 320000, 384000};
    public static final int[] A02 = {32000, 40000, 48000, 56000, 64000, 80000, 96000, 112000, 128000, 160000, 192000, 224000, 256000, 320000};
    public static final int[] A03 = {8000, 16000, 24000, 32000, 40000, 48000, 56000, 64000, 80000, 96000, 112000, 128000, 144000, 160000};
    public static final int[] A04 = {32000, 48000, 56000, 64000, 80000, 96000, 112000, 128000, 144000, 160000, 176000, 192000, 224000, 256000};
    public static final int[] A05 = {44100, 48000, 32000};
    public static final String[] A06 = {"audio/mpeg-L1", "audio/mpeg-L2", "audio/mpeg"};

    public static int A00(int i) {
        int i2;
        int i3;
        int i4;
        int i5;
        int[] iArr;
        int i6;
        int[] iArr2;
        if (!C12960it.A1V(i & -2097152, -2097152) || (i2 = (i >>> 19) & 3) == 1 || (i3 = (i >>> 17) & 3) == 0 || (i4 = (i >>> 12) & 15) == 0 || i4 == 15 || (i5 = (i >>> 10) & 3) == 3) {
            return -1;
        }
        int i7 = A05[i5];
        if (i2 == 2) {
            i7 >>= 1;
        } else if (i2 == 0) {
            i7 >>= 2;
        }
        int i8 = (i >>> 9) & 1;
        if (i3 == 3) {
            if (i2 == 3) {
                iArr2 = A00;
            } else {
                iArr2 = A04;
            }
            return (((iArr2[i4 - 1] * 12) / i7) + i8) << 2;
        }
        if (i2 != 3) {
            iArr = A03;
        } else if (i3 == 2) {
            iArr = A01;
        } else {
            iArr = A02;
        }
        int i9 = iArr[i4 - 1];
        int i10 = MediaCodecVideoEncoder.MIN_ENCODER_HEIGHT;
        if (i2 == 3) {
            i6 = i9 * MediaCodecVideoEncoder.MIN_ENCODER_HEIGHT;
        } else {
            if (i3 == 1) {
                i10 = 72;
            }
            i6 = i10 * i9;
        }
        return (i6 / i7) + i8;
    }

    public static int A01(int i) {
        int i2;
        int i3;
        if (!(!C12960it.A1V(i & -2097152, -2097152) || (i2 = (i >>> 19) & 3) == 1 || (i3 = (i >>> 17) & 3) == 0)) {
            int i4 = (i >>> 12) & 15;
            int i5 = (i >>> 10) & 3;
            if (!(i4 == 0 || i4 == 15 || i5 == 3)) {
                if (i3 != 1) {
                    if (i3 == 2) {
                        return 1152;
                    }
                    if (i3 == 3) {
                        return 384;
                    }
                    throw C72453ed.A0h();
                } else if (i2 != 3) {
                    return 576;
                } else {
                    return 1152;
                }
            }
        }
        return -1;
    }
}
