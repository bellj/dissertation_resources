package X;

import android.animation.Animator;

/* renamed from: X.0Uf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06580Uf implements Animator.AnimatorListener {
    public final /* synthetic */ C08800bs A00;
    public final /* synthetic */ C14230l4 A01;
    public final /* synthetic */ C14220l3 A02;
    public final /* synthetic */ AbstractC14200l1 A03;

    @Override // android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
    }

    @Override // android.animation.Animator.AnimatorListener
    public void onAnimationRepeat(Animator animator) {
    }

    @Override // android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
    }

    public C06580Uf(C08800bs r1, C14230l4 r2, C14220l3 r3, AbstractC14200l1 r4) {
        this.A00 = r1;
        this.A03 = r4;
        this.A02 = r3;
        this.A01 = r2;
    }

    @Override // android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        AbstractC14200l1 r2 = this.A03;
        C14250l6.A00(this.A01, this.A02, r2);
    }
}
