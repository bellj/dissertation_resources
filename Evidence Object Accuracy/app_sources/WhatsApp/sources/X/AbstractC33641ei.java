package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.SystemClock;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.mediacomposer.doodle.DoodleView;
import com.whatsapp.util.Log;
import java.io.IOException;

/* renamed from: X.1ei  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC33641ei {
    public final AnonymousClass12Q A00;
    public final C14900mE A01;
    public final AnonymousClass01d A02;
    public final AnonymousClass018 A03;
    public final AnonymousClass1CH A04;
    public final C48242Fd A05;

    public abstract long A01();

    public abstract void A07();

    public abstract void A08();

    public AbstractC33641ei(AnonymousClass12Q r1, C14900mE r2, AnonymousClass01d r3, AnonymousClass018 r4, AnonymousClass1CH r5, C48242Fd r6) {
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = r4;
        this.A04 = r5;
        this.A00 = r1;
        this.A05 = r6;
    }

    public static void A00(C92784Xk r2, AbstractC33641ei r3) {
        r2.A00 = 0;
        r2.A01 = SystemClock.elapsedRealtime();
        r2.A01();
        r3.A05.A01();
    }

    public Context A02() {
        View view = ((AbstractC33631eh) this.A05.A00).A00;
        AnonymousClass009.A03(view);
        return view.getContext();
    }

    public View A03() {
        if (this instanceof AnonymousClass35T) {
            return ((AnonymousClass35T) this).A06;
        }
        if (this instanceof AnonymousClass35X) {
            return ((AnonymousClass35X) this).A09;
        }
        if (this instanceof AnonymousClass35S) {
            return ((AnonymousClass35S) this).A00;
        }
        if (this instanceof AnonymousClass35V) {
            return ((AnonymousClass35V) this).A0E;
        }
        if (!(this instanceof AnonymousClass35U)) {
            return ((AnonymousClass35R) this).A00;
        }
        return ((AnonymousClass35U) this).A05;
    }

    public void A04() {
        C92784Xk r0;
        if (this instanceof AnonymousClass35T) {
            AbstractC28651Ol r02 = ((AnonymousClass35T) this).A00;
            if (r02 != null) {
                r02.A04();
            }
        } else if (!(this instanceof AnonymousClass35X)) {
            if (this instanceof AnonymousClass35S) {
                r0 = ((AnonymousClass35S) this).A05;
            } else if (this instanceof AnonymousClass35V) {
                r0 = ((AnonymousClass35V) this).A0D;
            } else if (!(this instanceof AnonymousClass35U)) {
                r0 = ((AnonymousClass35R) this).A01;
            } else {
                AnonymousClass35U r1 = (AnonymousClass35U) this;
                r1.A07.A02();
                r1.A02 = false;
                return;
            }
            r0.A02();
        } else {
            AnonymousClass35X r3 = (AnonymousClass35X) this;
            r3.A0G();
            if (r3.A02 != null) {
                r3.A0G();
                r3.A02.A05();
            }
            DoodleView doodleView = r3.A0E;
            if (doodleView != null) {
                doodleView.A0E.A0A = false;
                doodleView.invalidate();
            }
            r3.A0D();
            AnonymousClass2VE r03 = r3.A01;
            if (r03 != null) {
                r03.A02.dismiss();
            }
        }
    }

    public void A05() {
        C92784Xk r0;
        if (this instanceof AnonymousClass35T) {
            AnonymousClass35T r1 = (AnonymousClass35T) this;
            AbstractC28651Ol A0C = r1.A0C();
            if (A0C != null) {
                try {
                    A0C.A07();
                    return;
                } catch (IOException e) {
                    Log.e(e);
                }
            }
            ((AbstractC33641ei) r1).A01.A07(R.string.gallery_audio_cannot_load, 0);
        } else if (!(this instanceof AnonymousClass35X)) {
            if (this instanceof AnonymousClass35S) {
                r0 = ((AnonymousClass35S) this).A05;
            } else if (this instanceof AnonymousClass35V) {
                r0 = ((AnonymousClass35V) this).A0D;
            } else if (!(this instanceof AnonymousClass35U)) {
                r0 = ((AnonymousClass35R) this).A01;
            } else {
                r0 = ((AnonymousClass35U) this).A07;
            }
            r0.A01();
        } else {
            AnonymousClass35X r3 = (AnonymousClass35X) this;
            r3.A0G();
            if (!r3.A04) {
                r3.A0F();
                if (r3.A02 != null) {
                    r3.A0G();
                    r3.A02.A07();
                } else {
                    StringBuilder sb = new StringBuilder("statusplaybackvideo/no player for ");
                    sb.append(r3.A0G.A0z);
                    Log.w(sb.toString());
                }
                DoodleView doodleView = r3.A0E;
                if (doodleView != null) {
                    doodleView.A0E.A0A = true;
                    SystemClock.elapsedRealtime();
                    doodleView.invalidate();
                }
                r3.A0I();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00e3, code lost:
        if (r0.A0P == false) goto L_0x00e5;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A06() {
        /*
            r16 = this;
            r2 = r16
            boolean r0 = r2 instanceof X.AnonymousClass35T
            if (r0 != 0) goto L_0x00d8
            boolean r0 = r2 instanceof X.AnonymousClass35X
            if (r0 != 0) goto L_0x00c3
            boolean r0 = r2 instanceof X.AnonymousClass35S
            if (r0 != 0) goto L_0x0054
            boolean r0 = r2 instanceof X.AnonymousClass35V
            if (r0 != 0) goto L_0x0049
            boolean r0 = r2 instanceof X.AnonymousClass35U
            if (r0 == 0) goto L_0x0049
            X.35U r2 = (X.AnonymousClass35U) r2
            com.whatsapp.mediaview.PhotoView r4 = r2.A05
            android.content.Context r0 = r4.getContext()
            android.app.Activity r0 = X.AnonymousClass12P.A00(r0)
            android.view.Window r0 = r0.getWindow()
            android.view.View r0 = r0.getDecorView()
            int r1 = r0.getWidth()
            int r0 = r0.getHeight()
            int r0 = java.lang.Math.max(r1, r0)
            X.3b8 r6 = new X.3b8
            r6.<init>(r2, r0)
            boolean r0 = r2.A02
            X.19O r3 = r2.A08
            X.1X7 r5 = r2.A06
            if (r0 == 0) goto L_0x004a
            X.1IS r7 = r5.A0z
            r8 = 1
            r3.A0B(r4, r5, r6, r7, r8)
        L_0x0049:
            return
        L_0x004a:
            r9 = 1
            X.1IS r7 = r5.A0z
            r10 = 0
            r8 = 100
            r3.A0A(r4, r5, r6, r7, r8, r9, r10)
            return
        L_0x0054:
            X.35S r2 = (X.AnonymousClass35S) r2
            X.0xu r0 = r2.A02
            android.net.Uri r6 = X.AnonymousClass1FO.A00(r0)
            X.0mz r0 = r2.A04
            X.1IS r0 = r0.A0z
            boolean r0 = r0.A02
            r1 = 2131888345(0x7f1208d9, float:1.9411323E38)
            if (r0 == 0) goto L_0x006a
            r1 = 2131888344(0x7f1208d8, float:1.941132E38)
        L_0x006a:
            android.content.Context r0 = r2.A02()
            java.lang.String r1 = X.AnonymousClass1FO.A01(r0, r6, r1)
            android.text.Spanned r0 = android.text.Html.fromHtml(r1)
            android.text.SpannableString r8 = android.text.SpannableString.valueOf(r0)
            int r1 = r1.length()
            java.lang.Class<android.text.style.URLSpan> r0 = android.text.style.URLSpan.class
            r9 = 0
            java.lang.Object[] r7 = r8.getSpans(r9, r1, r0)
            android.text.style.URLSpan[] r7 = (android.text.style.URLSpan[]) r7
            int r5 = r7.length
            r4 = 0
        L_0x0089:
            if (r4 >= r5) goto L_0x00b3
            r10 = r7[r4]
            int r3 = r8.getSpanStart(r10)
            int r1 = r8.getSpanEnd(r10)
            r8.removeSpan(r10)
            com.whatsapp.TextEmojiLabel r0 = r2.A01
            android.content.Context r11 = r0.getContext()
            X.0mE r13 = r2.A01
            X.01d r14 = r2.A02
            X.12Q r12 = r2.A00
            java.lang.String r15 = r10.getURL()
            X.2oQ r10 = new X.2oQ
            r10.<init>(r11, r12, r13, r14, r15)
            r8.setSpan(r10, r3, r1, r9)
            int r4 = r4 + 1
            goto L_0x0089
        L_0x00b3:
            com.whatsapp.TextEmojiLabel r3 = r2.A01
            r3.setText(r8)
            r1 = 49
            com.whatsapp.util.ViewOnClickCListenerShape5S0200000_I1 r0 = new com.whatsapp.util.ViewOnClickCListenerShape5S0200000_I1
            r0.<init>(r2, r1, r6)
            r3.setOnClickListener(r0)
            return
        L_0x00c3:
            X.35X r2 = (X.AnonymousClass35X) r2
            boolean r0 = r2.A0A()
            if (r0 != 0) goto L_0x00d4
            r2.A0H()
            r2.A0J()
            r2.A0G()
        L_0x00d4:
            r2.A0E()
            return
        L_0x00d8:
            X.35T r2 = (X.AnonymousClass35T) r2
            X.1Xi r0 = r2.A05
            X.0oX r0 = r0.A02
            if (r0 == 0) goto L_0x00e5
            boolean r0 = r0.A0P
            r1 = 0
            if (r0 != 0) goto L_0x00e6
        L_0x00e5:
            r1 = 1
        L_0x00e6:
            X.2cE r0 = r2.A06
            r0.setBlurEnabled(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC33641ei.A06():void");
    }

    public void A09(boolean z) {
        if (this instanceof AnonymousClass35X) {
            AnonymousClass35X r2 = (AnonymousClass35X) this;
            r2.A04 = z;
            AbstractC33621eg r1 = ((AbstractC33641ei) r2).A05.A00;
            if (r1.A05) {
                AnonymousClass21T r0 = r2.A02;
                if (z) {
                    if (r0 != null) {
                        r0.A05();
                        Bitmap A03 = r2.A02.A03();
                        if (A03 != null) {
                            r2.A0F.A05(A03);
                            r2.A06 = true;
                        }
                    }
                    r2.A08.setVisibility(0);
                    r2.A0J();
                } else if (r0 == null) {
                    r2.A0G();
                    r2.A07();
                } else {
                    r2.A0G();
                    r1.A0E();
                }
            }
        }
    }

    public boolean A0A() {
        if (this instanceof AnonymousClass35T) {
            AnonymousClass35T r0 = (AnonymousClass35T) this;
            return AnonymousClass3I8.A01(((AbstractC33641ei) r0).A04, r0.A05);
        } else if (this instanceof AnonymousClass35X) {
            AnonymousClass35X r02 = (AnonymousClass35X) this;
            return AnonymousClass3I8.A01(((AbstractC33641ei) r02).A04, r02.A0G);
        } else if (this instanceof AnonymousClass35S) {
            AnonymousClass35S r03 = (AnonymousClass35S) this;
            return AnonymousClass3I8.A01(((AbstractC33641ei) r03).A04, r03.A04);
        } else if (this instanceof AnonymousClass35V) {
            AnonymousClass35V r04 = (AnonymousClass35V) this;
            return AnonymousClass3I8.A01(((AbstractC33641ei) r04).A04, r04.A0B);
        } else if (!(this instanceof AnonymousClass35U)) {
            return true;
        } else {
            AnonymousClass35U r05 = (AnonymousClass35U) this;
            return AnonymousClass3I8.A01(((AbstractC33641ei) r05).A04, r05.A06);
        }
    }

    public boolean A0B() {
        View view;
        if (!(this instanceof AnonymousClass35V)) {
            return false;
        }
        AnonymousClass35V r2 = (AnonymousClass35V) this;
        if (r2.A0G || (view = r2.A00) == null || view.getVisibility() != 0) {
            return false;
        }
        return true;
    }
}
