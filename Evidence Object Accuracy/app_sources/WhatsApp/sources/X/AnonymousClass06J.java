package X;

import java.util.Locale;

/* renamed from: X.06J  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass06J extends AnonymousClass06F {
    public static final AnonymousClass06J A00 = new AnonymousClass06J();

    public AnonymousClass06J() {
        super(null);
    }

    @Override // X.AnonymousClass06F
    public boolean A00() {
        return AnonymousClass069.A00(Locale.getDefault()) == 1;
    }
}
