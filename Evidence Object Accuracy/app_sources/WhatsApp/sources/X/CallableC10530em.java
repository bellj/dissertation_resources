package X;

import android.content.Context;
import java.util.concurrent.Callable;

/* renamed from: X.0em  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class CallableC10530em implements Callable {
    public final /* synthetic */ int A00;
    public final /* synthetic */ Context A01;
    public final /* synthetic */ C05060Oc A02;
    public final /* synthetic */ String A03;

    public CallableC10530em(Context context, C05060Oc r2, String str, int i) {
        this.A03 = str;
        this.A01 = context;
        this.A02 = r2;
        this.A00 = i;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        return AnonymousClass0RT.A00(this.A01, this.A02, this.A03, this.A00);
    }
}
