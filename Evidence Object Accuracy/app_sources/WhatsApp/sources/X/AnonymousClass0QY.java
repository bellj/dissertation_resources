package X;

import android.hardware.biometrics.BiometricPrompt;

/* renamed from: X.0QY  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0QY {
    public static void A00(BiometricPrompt.Builder builder, boolean z) {
        builder.setConfirmationRequired(z);
    }

    public static void A01(BiometricPrompt.Builder builder, boolean z) {
        builder.setDeviceCredentialAllowed(z);
    }
}
