package X;

import java.util.Collection;
import java.util.List;
import java.util.ListIterator;

/* renamed from: X.3t4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C80853t4 extends AbstractC80933tC<K, V>.WrappedCollection implements List<V> {
    public final /* synthetic */ AbstractC80933tC this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C80853t4(AbstractC80933tC r1, Object obj, List list, C113515Hw r4) {
        super(r1, obj, list, r4);
        this.this$0 = r1;
    }

    @Override // java.util.List
    public void add(int i, Object obj) {
        refreshIfEmpty();
        boolean isEmpty = getDelegate().isEmpty();
        getListDelegate().add(i, obj);
        AbstractC80933tC.access$208(this.this$0);
        if (isEmpty) {
            addToMap();
        }
    }

    @Override // java.util.List
    public boolean addAll(int i, Collection collection) {
        if (collection.isEmpty()) {
            return false;
        }
        int size = size();
        boolean addAll = getListDelegate().addAll(i, collection);
        if (!addAll) {
            return addAll;
        }
        AbstractC80933tC.access$212(this.this$0, getDelegate().size() - size);
        if (size != 0) {
            return addAll;
        }
        addToMap();
        return addAll;
    }

    @Override // java.util.List
    public Object get(int i) {
        refreshIfEmpty();
        return getListDelegate().get(i);
    }

    public List getListDelegate() {
        return (List) getDelegate();
    }

    @Override // java.util.List
    public int indexOf(Object obj) {
        refreshIfEmpty();
        return getListDelegate().indexOf(obj);
    }

    @Override // java.util.List
    public int lastIndexOf(Object obj) {
        refreshIfEmpty();
        return getListDelegate().lastIndexOf(obj);
    }

    @Override // java.util.List
    public ListIterator listIterator() {
        refreshIfEmpty();
        return new C80833t2(this);
    }

    @Override // java.util.List
    public ListIterator listIterator(int i) {
        refreshIfEmpty();
        return new C80833t2(this, i);
    }

    @Override // java.util.List
    public Object remove(int i) {
        refreshIfEmpty();
        Object remove = getListDelegate().remove(i);
        AbstractC80933tC.access$210(this.this$0);
        removeIfEmpty();
        return remove;
    }

    @Override // java.util.List
    public Object set(int i, Object obj) {
        refreshIfEmpty();
        return getListDelegate().set(i, obj);
    }

    @Override // java.util.List
    public List subList(int i, int i2) {
        refreshIfEmpty();
        AbstractC80933tC r3 = this.this$0;
        Object key = getKey();
        List subList = getListDelegate().subList(i, i2);
        C113515Hw ancestor = getAncestor();
        if (ancestor == null) {
            ancestor = this;
        }
        return r3.wrapList(key, subList, ancestor);
    }
}
