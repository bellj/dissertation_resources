package X;

import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.google.protobuf.CodedOutputStream;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.io.IOException;

/* renamed from: X.2nN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57632nN extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57632nN A0B;
    public static volatile AnonymousClass255 A0C;
    public int A00;
    public int A01;
    public int A02;
    public long A03;
    public C462525d A04;
    public C462525d A05;
    public C462525d A06;
    public C462425c A07;
    public C462425c A08;
    public boolean A09;
    public boolean A0A;

    static {
        C57632nN r0 = new C57632nN();
        A0B = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r15, Object obj, Object obj2) {
        C463325l r1;
        C463325l r12;
        C463325l r13;
        C463525n r14;
        C463525n r16;
        switch (r15.ordinal()) {
            case 0:
                return A0B;
            case 1:
                AbstractC462925h r7 = (AbstractC462925h) obj;
                C57632nN r3 = (C57632nN) obj2;
                this.A08 = (C462425c) r7.Aft(this.A08, r3.A08);
                this.A02 = r7.Afp(this.A02, r3.A02, C12960it.A1V(this.A00 & 2, 2), C12960it.A1V(r3.A00 & 2, 2));
                this.A07 = (C462425c) r7.Aft(this.A07, r3.A07);
                this.A06 = (C462525d) r7.Aft(this.A06, r3.A06);
                this.A04 = (C462525d) r7.Aft(this.A04, r3.A04);
                this.A05 = (C462525d) r7.Aft(this.A05, r3.A05);
                int i = this.A00;
                boolean A1V = C12960it.A1V(i & 64, 64);
                boolean z = this.A0A;
                int i2 = r3.A00;
                this.A0A = r7.Afl(A1V, z, C12960it.A1V(i2 & 64, 64), r3.A0A);
                this.A09 = r7.Afl(C12960it.A1V(i & 128, 128), this.A09, C12960it.A1V(i2 & 128, 128), r3.A09);
                this.A01 = r7.Afp(this.A01, r3.A01, C12960it.A1V(i & 256, 256), C12960it.A1V(i2 & 256, 256));
                this.A03 = r7.Afs(this.A03, r3.A03, C12960it.A1V(i & 512, 512), C12960it.A1V(i2 & 512, 512));
                if (r7 == C463025i.A00) {
                    this.A00 = i | i2;
                }
                return this;
            case 2:
                AnonymousClass253 r72 = (AnonymousClass253) obj;
                AnonymousClass254 r32 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        int A03 = r72.A03();
                        switch (A03) {
                            case 0:
                                break;
                            case 10:
                                if ((this.A00 & 1) == 1) {
                                    r16 = (C463525n) this.A08.A0T();
                                } else {
                                    r16 = null;
                                }
                                C462425c r0 = (C462425c) AbstractC27091Fz.A0H(r72, r32, C462425c.A03);
                                this.A08 = r0;
                                if (r16 != null) {
                                    this.A08 = (C462425c) AbstractC27091Fz.A0C(r16, r0);
                                }
                                this.A00 |= 1;
                                break;
                            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                                int A02 = r72.A02();
                                if (AnonymousClass4BT.A00(A02) != null) {
                                    this.A00 |= 2;
                                    this.A02 = A02;
                                    break;
                                } else {
                                    super.A0X(2, A02);
                                    break;
                                }
                            case 26:
                                if ((this.A00 & 4) == 4) {
                                    r14 = (C463525n) this.A07.A0T();
                                } else {
                                    r14 = null;
                                }
                                C462425c r02 = (C462425c) AbstractC27091Fz.A0H(r72, r32, C462425c.A03);
                                this.A07 = r02;
                                if (r14 != null) {
                                    this.A07 = (C462425c) AbstractC27091Fz.A0C(r14, r02);
                                }
                                this.A00 |= 4;
                                break;
                            case 34:
                                if ((this.A00 & 8) == 8) {
                                    r13 = (C463325l) this.A06.A0T();
                                } else {
                                    r13 = null;
                                }
                                C462525d r03 = (C462525d) AbstractC27091Fz.A0H(r72, r32, C462525d.A05);
                                this.A06 = r03;
                                if (r13 != null) {
                                    this.A06 = (C462525d) AbstractC27091Fz.A0C(r13, r03);
                                }
                                this.A00 |= 8;
                                break;
                            case 42:
                                if ((this.A00 & 16) == 16) {
                                    r12 = (C463325l) this.A04.A0T();
                                } else {
                                    r12 = null;
                                }
                                C462525d r04 = (C462525d) AbstractC27091Fz.A0H(r72, r32, C462525d.A05);
                                this.A04 = r04;
                                if (r12 != null) {
                                    this.A04 = (C462525d) AbstractC27091Fz.A0C(r12, r04);
                                }
                                this.A00 |= 16;
                                break;
                            case SearchActionVerificationClientService.TIME_TO_SLEEP_IN_MS /* 50 */:
                                if ((this.A00 & 32) == 32) {
                                    r1 = (C463325l) this.A05.A0T();
                                } else {
                                    r1 = null;
                                }
                                C462525d r05 = (C462525d) AbstractC27091Fz.A0H(r72, r32, C462525d.A05);
                                this.A05 = r05;
                                if (r1 != null) {
                                    this.A05 = (C462525d) AbstractC27091Fz.A0C(r1, r05);
                                }
                                this.A00 |= 32;
                                break;
                            case 56:
                                this.A00 |= 64;
                                this.A0A = r72.A0F();
                                break;
                            case 64:
                                this.A00 |= 128;
                                this.A09 = r72.A0F();
                                break;
                            case C43951xu.A02:
                                this.A00 |= 256;
                                this.A01 = r72.A02();
                                break;
                            case 80:
                                this.A00 |= 512;
                                this.A03 = r72.A06();
                                break;
                            default:
                                if (A0a(r72, A03)) {
                                    break;
                                } else {
                                    break;
                                }
                        }
                    } catch (C28971Pt e) {
                        throw AbstractC27091Fz.A0J(e, this);
                    } catch (IOException e2) {
                        throw AbstractC27091Fz.A0K(this, e2);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C57632nN();
            case 5:
                return new C82433vc();
            case 6:
                break;
            case 7:
                if (A0C == null) {
                    synchronized (C57632nN.class) {
                        if (A0C == null) {
                            A0C = AbstractC27091Fz.A09(A0B);
                        }
                    }
                }
                return A0C;
            default:
                throw C12970iu.A0z();
        }
        return A0B;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            C462425c r0 = this.A08;
            if (r0 == null) {
                r0 = C462425c.A03;
            }
            i2 = AbstractC27091Fz.A08(r0, 1, 0);
        }
        int i3 = this.A00;
        if ((i3 & 2) == 2) {
            i2 = AbstractC27091Fz.A03(2, this.A02, i2);
        }
        if ((i3 & 4) == 4) {
            C462425c r02 = this.A07;
            if (r02 == null) {
                r02 = C462425c.A03;
            }
            i2 = AbstractC27091Fz.A08(r02, 3, i2);
        }
        if ((this.A00 & 8) == 8) {
            C462525d r03 = this.A06;
            if (r03 == null) {
                r03 = C462525d.A05;
            }
            i2 = AbstractC27091Fz.A08(r03, 4, i2);
        }
        if ((this.A00 & 16) == 16) {
            C462525d r04 = this.A04;
            if (r04 == null) {
                r04 = C462525d.A05;
            }
            i2 = AbstractC27091Fz.A08(r04, 5, i2);
        }
        if ((this.A00 & 32) == 32) {
            C462525d r05 = this.A05;
            if (r05 == null) {
                r05 = C462525d.A05;
            }
            i2 = AbstractC27091Fz.A08(r05, 6, i2);
        }
        int i4 = this.A00;
        if ((i4 & 64) == 64) {
            i2 += CodedOutputStream.A00(7);
        }
        if ((i4 & 128) == 128) {
            i2 += CodedOutputStream.A00(8);
        }
        if ((i4 & 256) == 256) {
            i2 += CodedOutputStream.A03(9, this.A01);
        }
        if ((i4 & 512) == 512) {
            i2 += CodedOutputStream.A05(10, this.A03);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            C462425c r0 = this.A08;
            if (r0 == null) {
                r0 = C462425c.A03;
            }
            codedOutputStream.A0L(r0, 1);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0E(2, this.A02);
        }
        if ((this.A00 & 4) == 4) {
            C462425c r02 = this.A07;
            if (r02 == null) {
                r02 = C462425c.A03;
            }
            codedOutputStream.A0L(r02, 3);
        }
        if ((this.A00 & 8) == 8) {
            C462525d r03 = this.A06;
            if (r03 == null) {
                r03 = C462525d.A05;
            }
            codedOutputStream.A0L(r03, 4);
        }
        if ((this.A00 & 16) == 16) {
            C462525d r04 = this.A04;
            if (r04 == null) {
                r04 = C462525d.A05;
            }
            codedOutputStream.A0L(r04, 5);
        }
        if ((this.A00 & 32) == 32) {
            C462525d r05 = this.A05;
            if (r05 == null) {
                r05 = C462525d.A05;
            }
            codedOutputStream.A0L(r05, 6);
        }
        if ((this.A00 & 64) == 64) {
            codedOutputStream.A0J(7, this.A0A);
        }
        if ((this.A00 & 128) == 128) {
            codedOutputStream.A0J(8, this.A09);
        }
        if ((this.A00 & 256) == 256) {
            codedOutputStream.A0E(9, this.A01);
        }
        if ((this.A00 & 512) == 512) {
            codedOutputStream.A0H(10, this.A03);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
