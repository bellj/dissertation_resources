package X;

import com.whatsapp.search.SearchViewModel;

/* renamed from: X.2Lb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C49522Lb extends AnonymousClass07A {
    public final AbstractC15710nm A00;
    public final C14900mE A01;
    public final C251118d A02;
    public final C25601Aa A03;
    public final C15550nR A04;
    public final C15610nY A05;
    public final AnonymousClass1J1 A06;
    public final C16590pI A07;
    public final AnonymousClass018 A08;
    public final C20830wO A09;
    public final C15680nj A0A;
    public final C15240mn A0B;
    public final C20040v7 A0C;
    public final AnonymousClass12H A0D;
    public final C20030v6 A0E;
    public final C14850m9 A0F;
    public final C22050yP A0G;
    public final C21230x5 A0H;
    public final C49512La A0I;
    public final AnonymousClass1AY A0J;
    public final AnonymousClass1AM A0K;
    public final C240514a A0L;
    public final AnonymousClass12F A0M;
    public final AnonymousClass1AW A0N;
    public final AbstractC14440lR A0O;

    public C49522Lb(AbstractC001500q r2, AbstractC15710nm r3, C14900mE r4, C251118d r5, C25601Aa r6, C15550nR r7, C15610nY r8, AnonymousClass1J1 r9, C16590pI r10, AnonymousClass018 r11, C20830wO r12, C15680nj r13, C15240mn r14, C20040v7 r15, AnonymousClass12H r16, C20030v6 r17, C14850m9 r18, C22050yP r19, C21230x5 r20, C49512La r21, AnonymousClass1AY r22, AnonymousClass1AM r23, C240514a r24, AnonymousClass12F r25, AnonymousClass1AW r26, AbstractC14440lR r27) {
        super(null, r2);
        this.A07 = r10;
        this.A06 = r9;
        this.A0F = r18;
        this.A01 = r4;
        this.A0O = r27;
        this.A00 = r3;
        this.A0C = r15;
        this.A04 = r7;
        this.A0N = r26;
        this.A05 = r8;
        this.A08 = r11;
        this.A0B = r14;
        this.A0D = r16;
        this.A0M = r25;
        this.A0G = r19;
        this.A0L = r24;
        this.A0J = r22;
        this.A02 = r5;
        this.A0A = r13;
        this.A0K = r23;
        this.A0I = r21;
        this.A0E = r17;
        this.A09 = r12;
        this.A0H = r20;
        this.A03 = r6;
    }

    @Override // X.AnonymousClass07A
    public AnonymousClass015 A02(AnonymousClass07E r45, Class cls, String str) {
        AnonymousClass009.A0B("Invalid viewModel", cls.isAssignableFrom(SearchViewModel.class));
        C16590pI r1 = this.A07;
        C14850m9 r12 = this.A0F;
        C14900mE r13 = this.A01;
        AbstractC14440lR r14 = this.A0O;
        AbstractC15710nm r15 = this.A00;
        C20040v7 r16 = this.A0C;
        C15550nR r17 = this.A04;
        AnonymousClass1AW r18 = this.A0N;
        C15610nY r19 = this.A05;
        AnonymousClass018 r152 = this.A08;
        C15240mn r142 = this.A0B;
        AnonymousClass12H r132 = this.A0D;
        AnonymousClass12F r122 = this.A0M;
        C22050yP r11 = this.A0G;
        C240514a r10 = this.A0L;
        AnonymousClass1AY r9 = this.A0J;
        AnonymousClass1J1 r8 = this.A06;
        C251118d r7 = this.A02;
        C15680nj r6 = this.A0A;
        AnonymousClass1AM r5 = this.A0K;
        C49512La r4 = this.A0I;
        C20030v6 r3 = this.A0E;
        return new SearchViewModel(r45, r15, r13, r7, this.A03, r17, r19, r8, r1, r152, this.A09, r6, r142, r16, r132, r3, r12, r11, this.A0H, r4, r9, r5, r10, r122, r18, r14);
    }
}
