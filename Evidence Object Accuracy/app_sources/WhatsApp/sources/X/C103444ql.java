package X;

import android.content.Context;
import com.whatsapp.settings.SettingsDataUsageActivity;

/* renamed from: X.4ql  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103444ql implements AbstractC009204q {
    public final /* synthetic */ SettingsDataUsageActivity A00;

    public C103444ql(SettingsDataUsageActivity settingsDataUsageActivity) {
        this.A00 = settingsDataUsageActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
