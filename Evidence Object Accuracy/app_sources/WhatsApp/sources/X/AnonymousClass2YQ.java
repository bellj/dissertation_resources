package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.widget.FrameLayout;
import com.whatsapp.mentions.MentionPickerView;

/* renamed from: X.2YQ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2YQ extends AnimatorListenerAdapter {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AnonymousClass2xg A01;

    public AnonymousClass2YQ(AnonymousClass2xg r1, int i) {
        this.A01 = r1;
        this.A00 = i;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        AnonymousClass2xg r2 = this.A01;
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) r2.getLayoutParams();
        int i = this.A00;
        layoutParams.height = i;
        r2.setLayoutParams(layoutParams);
        if (i == 0) {
            r2.setVisibility(8);
            AnonymousClass1lB r0 = ((MentionPickerView) r2).A0A;
            if (r0 != null) {
                r0.ANo(false);
            }
        }
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        AnonymousClass2xg r2 = this.A01;
        if (r2.getVisibility() != 0) {
            r2.A03();
            r2.setVisibility(0);
            AnonymousClass1lB r0 = ((MentionPickerView) r2).A0A;
            if (r0 != null) {
                r0.ANo(true);
            }
        }
    }
}
