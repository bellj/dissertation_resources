package X;

import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;

/* renamed from: X.2vd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59942vd extends AbstractC37191le {
    public final AnonymousClass02H A00;
    public final RecyclerView A01;
    public final C251118d A02;
    public final C54312gW A03;

    public C59942vd(View view, C251118d r5, C54312gW r6, AnonymousClass018 r7) {
        super(view);
        AnonymousClass02H gridLayoutManager;
        this.A02 = r5;
        this.A01 = C12990iw.A0R(view, R.id.popular_categories_recycler_view);
        if (r5.A01()) {
            gridLayoutManager = C12990iw.A0Q(view);
        } else {
            view.getContext();
            gridLayoutManager = new GridLayoutManager(AbstractC37191le.A00(this));
        }
        this.A00 = gridLayoutManager;
        RecyclerView recyclerView = this.A01;
        recyclerView.setLayoutManager(gridLayoutManager);
        if (!this.A02.A01()) {
            recyclerView.A0l(new C54612h0(r7, view.getResources().getDimensionPixelSize(R.dimen.product_margin_8dp)));
            recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() { // from class: X.4nZ
                @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
                public final void onGlobalLayout() {
                    C59942vd r1 = C59942vd.this;
                    if (!r1.A02.A01()) {
                        GridLayoutManager gridLayoutManager2 = (GridLayoutManager) r1.A00;
                        int A0A = r1.A0A();
                        if (A0A != gridLayoutManager2.A00) {
                            gridLayoutManager2.A1h(A0A);
                        }
                    }
                }
            });
        } else if (view instanceof LinearLayout) {
            ((LinearLayout) view).setGravity(3);
        }
        this.A03 = r6;
    }

    public final int A0A() {
        return AbstractC37191le.A00(this);
    }
}
