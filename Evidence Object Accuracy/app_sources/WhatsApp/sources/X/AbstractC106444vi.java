package X;

import android.media.AudioTrack;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.Surface;
import com.facebook.redex.RunnableBRunnable0Shape0S0101100_I1;

/* renamed from: X.4vi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC106444vi implements AbstractC117055Yb, AnonymousClass5WX {
    public int A00;
    public int A01;
    public long A02 = Long.MIN_VALUE;
    public long A03;
    public C94214bR A04;
    public AbstractC116795Wx A05;
    public boolean A06;
    public boolean A07;
    public C100614mC[] A08;
    public final int A09;
    public final C89864Lr A0A = new C89864Lr();

    public void A07() {
    }

    public abstract void A08();

    public abstract void A09(long j, boolean z);

    public void A0A(boolean z, boolean z2) {
    }

    @Override // X.AbstractC117055Yb
    public /* synthetic */ void AcY(float f, float f2) {
    }

    public AbstractC106444vi(int i) {
        this.A09 = i;
    }

    public final int A00(C89864Lr r8, C76763mA r9, boolean z) {
        int AZs = this.A05.AZs(r8, r9, z);
        if (AZs == -4) {
            if (AnonymousClass4YO.A00(r9)) {
                this.A02 = Long.MIN_VALUE;
                if (!this.A06) {
                    return -3;
                }
                return -4;
            }
            long j = r9.A00 + this.A03;
            r9.A00 = j;
            this.A02 = Math.max(this.A02, j);
        } else if (AZs == -5) {
            C100614mC r5 = r8.A00;
            long j2 = r5.A0J;
            if (j2 != Long.MAX_VALUE) {
                C93844ap r2 = new C93844ap(r5);
                r2.A0H = j2 + this.A03;
                r8.A00 = new C100614mC(r2);
                return AZs;
            }
        }
        return AZs;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.AnonymousClass3A1 A01(X.C100614mC r10, java.lang.Throwable r11, boolean r12) {
        /*
            r9 = this;
            r2 = r10
            if (r10 == 0) goto L_0x001a
            boolean r0 = r9.A07
            if (r0 != 0) goto L_0x001a
            r0 = 1
            r9.A07 = r0
            r1 = 0
            int r0 = r9.Aeb(r10)     // Catch: 3A1 -> 0x0018, all -> 0x0014
            r7 = r0 & 7
            r9.A07 = r1
            goto L_0x001b
        L_0x0014:
            r0 = move-exception
            r9.A07 = r1
            throw r0
        L_0x0018:
            r9.A07 = r1
        L_0x001a:
            r7 = 4
        L_0x001b:
            java.lang.String r3 = r9.getName()
            int r6 = r9.A00
            if (r2 != 0) goto L_0x0024
            r7 = 4
        L_0x0024:
            r5 = 1
            r4 = r11
            r8 = r12
            X.3A1 r1 = new X.3A1
            r1.<init>(r2, r3, r4, r5, r6, r7, r8)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC106444vi.A01(X.4mC, java.lang.Throwable, boolean):X.3A1");
    }

    public void A02() {
        AnonymousClass5XY r2;
        if (this instanceof C76563lq) {
            r2 = ((C76563lq) this).A0I;
        } else if (this instanceof C76933mT) {
            C76933mT r6 = (C76933mT) this;
            r6.A07 = 0;
            r6.A0E = SystemClock.elapsedRealtime();
            r6.A0I = SystemClock.elapsedRealtime() * 1000;
            r6.A0J = 0;
            r6.A0D = 0;
            C94494bu r22 = r6.A0Y;
            r22.A0C = true;
            r22.A04 = 0;
            r22.A05 = -1;
            r22.A07 = -1;
            r22.A06(false);
            return;
        } else if (this instanceof C76943mU) {
            r2 = ((C76943mU) this).A0A;
        } else {
            return;
        }
        C106534vr r23 = (C106534vr) r2;
        r23.A0T = true;
        AudioTrack audioTrack = r23.A0D;
        if (audioTrack != null) {
            r23.A0d.A0N.A02();
            audioTrack.play();
        }
    }

    public void A03() {
        AnonymousClass5XY r0;
        Surface surface;
        if (this instanceof C76563lq) {
            C76563lq r02 = (C76563lq) this;
            r02.A0D();
            r0 = r02.A0I;
        } else if (this instanceof C76933mT) {
            C76933mT r2 = (C76933mT) this;
            r2.A0G = -9223372036854775807L;
            r2.A0Y();
            int i = r2.A0D;
            if (i != 0) {
                AnonymousClass4ME r4 = r2.A0Z;
                long j = r2.A0J;
                Handler handler = r4.A00;
                if (handler != null) {
                    handler.post(new RunnableBRunnable0Shape0S0101100_I1(r4, i, 1, j));
                }
                r2.A0J = 0;
                r2.A0D = 0;
            }
            C94494bu r3 = r2.A0Y;
            r3.A0C = false;
            if (AnonymousClass3JZ.A01 >= 30 && (surface = r3.A0B) != null && r3.A03 != 0.0f) {
                r3.A03 = 0.0f;
                C94494bu.A00(surface, 0.0f);
                return;
            }
            return;
        } else if (this instanceof C76943mU) {
            C76943mU r03 = (C76943mU) this;
            r03.A0X();
            r0 = r03.A0A;
        } else {
            return;
        }
        C106534vr r04 = (C106534vr) r0;
        r04.A0T = false;
        AudioTrack audioTrack = r04.A0D;
        if (audioTrack != null) {
            AnonymousClass4XV r5 = r04.A0d;
            r5.A0J = 0;
            r5.A05 = 0;
            r5.A02 = 0;
            r5.A0A = 0;
            r5.A0D = 0;
            r5.A0H = 0;
            r5.A0T = false;
            if (r5.A0L == -9223372036854775807L) {
                r5.A0N.A02();
                audioTrack.pause();
            }
        }
    }

    public void A04(C100614mC[] r10, long j, long j2) {
        if (this instanceof C76553lp) {
            C76553lp r1 = (C76553lp) this;
            r1.A03 = r10[0];
            if (r1.A04 != null) {
                r1.A00 = 1;
            } else {
                r1.A0D();
            }
        } else if (this instanceof C76543lo) {
            C76543lo r2 = (C76543lo) this;
            r2.A03 = r2.A07.A8E(r10[0]);
        } else if (this instanceof AbstractC76533ln) {
            AbstractC76533ln r4 = (AbstractC76533ln) this;
            boolean z = true;
            if (r4.A0D == -9223372036854775807L) {
                if (r4.A0E != -9223372036854775807L) {
                    z = false;
                }
                C95314dV.A04(z);
                r4.A0E = j;
                r4.A0D = j2;
                return;
            }
            int i = r4.A09;
            long[] jArr = r4.A12;
            if (i == jArr.length) {
                Log.w("MediaCodecRenderer", C12970iu.A0w(C12960it.A0k("Too many stream changes, so dropping offset: "), jArr[i - 1]));
            } else {
                r4.A09 = i + 1;
            }
            long[] jArr2 = r4.A13;
            int i2 = r4.A09;
            int i3 = i2 - 1;
            jArr2[i3] = j;
            jArr[i3] = j2;
            r4.A14[i2 - 1] = r4.A0B;
        }
    }

    @Override // X.AbstractC117055Yb
    public final boolean AIJ() {
        return C12960it.A1T((this.A02 > Long.MIN_VALUE ? 1 : (this.A02 == Long.MIN_VALUE ? 0 : -1)));
    }

    @Override // X.AbstractC117055Yb
    public final void reset() {
        C95314dV.A04(C12960it.A1T(this.A01));
        C89864Lr r1 = this.A0A;
        r1.A01 = null;
        r1.A00 = null;
        A07();
    }
}
