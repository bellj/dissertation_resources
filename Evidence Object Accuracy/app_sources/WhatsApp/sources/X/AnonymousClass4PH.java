package X;

import android.os.Looper;
import java.util.concurrent.Executor;

/* renamed from: X.4PH  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4PH {
    public final Executor A00;
    public volatile C92544Wi A01;
    public volatile Object A02;

    public AnonymousClass4PH(Looper looper, Object obj) {
        this.A00 = new AnonymousClass5E3(looper);
        C13020j0.A02(obj, "Listener must not be null");
        this.A02 = obj;
        C13020j0.A05("LocationListener");
        this.A01 = new C92544Wi(obj);
    }
}
