package X;

import com.whatsapp.payments.ui.IndiaUpiStepUpActivity;

/* renamed from: X.5bt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118485bt extends AnonymousClass0Yo {
    public final /* synthetic */ C30861Zc A00;
    public final /* synthetic */ C18600si A01;
    public final /* synthetic */ C120525gK A02;
    public final /* synthetic */ C129145xF A03;
    public final /* synthetic */ IndiaUpiStepUpActivity A04;
    public final /* synthetic */ C128355vy A05;
    public final /* synthetic */ C18590sh A06;
    public final /* synthetic */ String A07;

    public C118485bt(C30861Zc r1, C18600si r2, C120525gK r3, C129145xF r4, IndiaUpiStepUpActivity indiaUpiStepUpActivity, C128355vy r6, C18590sh r7, String str) {
        this.A05 = r6;
        this.A07 = str;
        this.A06 = r7;
        this.A02 = r3;
        this.A00 = r1;
        this.A01 = r2;
        this.A04 = indiaUpiStepUpActivity;
        this.A03 = r4;
    }

    @Override // X.AnonymousClass0Yo, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.isAssignableFrom(C117985b5.class)) {
            String str = this.A07;
            C16590pI r2 = this.A05.A0A;
            C18590sh r7 = this.A06;
            C120525gK r5 = this.A02;
            return new C117985b5(this.A04, r2, this.A00, this.A01, r5, this.A03, r7, str);
        }
        throw C12970iu.A0f("Invalid viewModel");
    }
}
