package X;

import android.content.Context;
import com.whatsapp.backup.google.RestoreFromBackupActivity;

/* renamed from: X.4pl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C102824pl implements AbstractC009204q {
    public final /* synthetic */ RestoreFromBackupActivity A00;

    public C102824pl(RestoreFromBackupActivity restoreFromBackupActivity) {
        this.A00 = restoreFromBackupActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
