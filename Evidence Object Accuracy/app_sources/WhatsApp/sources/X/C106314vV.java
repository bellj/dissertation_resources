package X;

import android.content.Context;
import android.view.View;
import org.wawebrtc.MediaCodecVideoEncoder;

/* renamed from: X.4vV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106314vV implements AnonymousClass5WW {
    public final /* synthetic */ AnonymousClass28D A00;

    public C106314vV(AnonymousClass28D r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5WW
    public /* bridge */ /* synthetic */ void A6O(Context context, Object obj, Object obj2, Object obj3) {
        View view = (View) obj;
        AnonymousClass28D r3 = this.A00;
        view.setScaleX(r3.A09(136, 1.0f));
        view.setScaleY(r3.A09(137, 1.0f));
        view.setTranslationX(r3.A09(MediaCodecVideoEncoder.MIN_ENCODER_HEIGHT, 0.0f));
        view.setTranslationY(r3.A09(145, 0.0f));
        view.setRotation(r3.A09(138, 0.0f));
        view.setAlpha(r3.A09(141, 1.0f));
    }

    @Override // X.AnonymousClass5WW
    public boolean Adc(Object obj, Object obj2, Object obj3, Object obj4) {
        return true;
    }

    @Override // X.AnonymousClass5WW
    public void Af8(Context context, Object obj, Object obj2, Object obj3) {
        View view = (View) obj;
        view.setScaleX(1.0f);
        view.setScaleY(1.0f);
        view.setTranslationX(0.0f);
        view.setTranslationY(0.0f);
        view.setRotation(0.0f);
        view.setAlpha(1.0f);
    }
}
