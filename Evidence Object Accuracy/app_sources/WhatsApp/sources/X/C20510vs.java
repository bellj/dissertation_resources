package X;

import android.content.ContentValues;
import android.database.Cursor;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0vs */
/* loaded from: classes2.dex */
public class C20510vs {
    public final C15570nT A00;
    public final C14650lo A01;
    public final C15550nR A02;
    public final C16590pI A03;
    public final AnonymousClass018 A04;
    public final C22320yt A05;
    public final C16510p9 A06;
    public final C19990v2 A07;
    public final C18460sU A08;
    public final C20850wQ A09;
    public final C16490p7 A0A;
    public final C21390xL A0B;

    public C20510vs(C15570nT r1, C14650lo r2, C15550nR r3, C16590pI r4, AnonymousClass018 r5, C22320yt r6, C16510p9 r7, C19990v2 r8, C18460sU r9, C20850wQ r10, C16490p7 r11, C21390xL r12) {
        this.A03 = r4;
        this.A08 = r9;
        this.A06 = r7;
        this.A00 = r1;
        this.A07 = r8;
        this.A02 = r3;
        this.A04 = r5;
        this.A0B = r12;
        this.A05 = r6;
        this.A0A = r11;
        this.A01 = r2;
        this.A09 = r10;
    }

    public static /* synthetic */ void A00(C20510vs r5, List list, long j) {
        C16590pI r3 = r5.A03;
        List<C30731Yp> A02 = C30721Yo.A02(r5.A01, r5.A02, r3, r5.A04, list);
        C16310on A022 = r5.A0A.A02();
        try {
            AnonymousClass1Lx A00 = A022.A00();
            for (C30731Yp r1 : A02) {
                r5.A07(r1.A00, j);
                r5.A08(r1, j);
            }
            A00.A00();
            A00.close();
            A022.close();
        } catch (Throwable th) {
            try {
                A022.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final List A01(long j, boolean z) {
        String str;
        if (z) {
            str = "SELECT vcard FROM message_quoted_vcard WHERE message_row_id = ? AND vcard IS NOT NULL AND vcard != \"\"";
        } else {
            str = "SELECT vcard FROM message_vcard WHERE message_row_id = ? AND vcard IS NOT NULL AND vcard != \"\"";
        }
        ArrayList arrayList = new ArrayList();
        C16310on A01 = this.A0A.get();
        try {
            Cursor A09 = A01.A03.A09(str, new String[]{Long.toString(j)});
            int columnIndexOrThrow = A09.getColumnIndexOrThrow("vcard");
            while (A09.moveToNext()) {
                arrayList.add(A09.getString(columnIndexOrThrow));
            }
            A09.close();
            A01.close();
            return arrayList;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A02(C30411Xh r7) {
        if ((A09() || (r7.A11 > 0 && r7.A11 <= this.A0B.A01("migration_vcard_index", 0))) && r7.A0I() != null) {
            A07(r7.A0I(), r7.A11);
        }
    }

    public void A03(C30351Xb r7) {
        if ((A09() || (r7.A11 > 0 && r7.A11 <= this.A0B.A01("migration_vcard_index", 0))) && !r7.A14().isEmpty()) {
            List<String> A14 = r7.A14();
            C16310on A02 = this.A0A.A02();
            try {
                AnonymousClass1Lx A00 = A02.A00();
                for (String str : A14) {
                    A07(str, r7.A11);
                }
                A00.A00();
                A00.close();
                A02.close();
            } catch (Throwable th) {
                try {
                    A02.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
    }

    public void A04(C30351Xb r5, long j) {
        List<String> A14 = r5.A14();
        if (!A14.isEmpty()) {
            C16310on A02 = this.A0A.A02();
            try {
                AnonymousClass1Lx A00 = A02.A00();
                for (String str : A14) {
                    A06(str, j);
                }
                A00.A00();
                A00.close();
                A02.close();
            } catch (Throwable th) {
                try {
                    A02.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
    }

    public void A05(C16440p1 r6) {
        if (((AbstractC15340mz) r6).A08 == 7 && ((AbstractC16130oV) r6).A02 != null) {
            C16310on A01 = this.A0A.get();
            try {
                ContentValues contentValues = new ContentValues(2);
                contentValues.put("message_row_id", Long.valueOf(r6.A11));
                contentValues.put("count", Integer.valueOf(((AbstractC16130oV) r6).A02.A01));
                A01.A03.A04(contentValues, "message_media_vcard_count");
                A01.close();
            } catch (Throwable th) {
                try {
                    A01.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
    }

    public final void A06(String str, long j) {
        C16310on A02 = this.A0A.A02();
        try {
            ContentValues contentValues = new ContentValues(2);
            contentValues.put("message_row_id", Long.valueOf(j));
            contentValues.put("vcard", str);
            A02.A03.A06(contentValues, "message_quoted_vcard", 4);
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final void A07(String str, long j) {
        C16310on A02 = this.A0A.A02();
        try {
            ContentValues contentValues = new ContentValues(2);
            contentValues.put("message_row_id", Long.valueOf(j));
            contentValues.put("vcard", str);
            A02.A03.A06(contentValues, "message_vcard", 4);
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final void A08(C30731Yp r11, long j) {
        long j2;
        String str = r11.A00;
        C16490p7 r4 = this.A0A;
        C16310on A01 = r4.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT _id FROM message_vcard WHERE message_row_id = ? AND vcard = ?", new String[]{Long.toString(j), str});
            if (A09.moveToFirst()) {
                j2 = A09.getLong(A09.getColumnIndexOrThrow("_id"));
                A09.close();
                A01.close();
            } else {
                A09.close();
                A01.close();
                j2 = -1;
            }
            List<C30741Yq> list = r11.A01.A05;
            if (list != null) {
                A01 = r4.A02();
                try {
                    AnonymousClass1Lx A00 = A01.A00();
                    for (C30741Yq r5 : list) {
                        if (r5.A01 != null) {
                            ContentValues contentValues = new ContentValues(3);
                            contentValues.put("vcard_jid_row_id", Long.valueOf(this.A08.A01(r5.A01)));
                            contentValues.put("vcard_row_id", Long.valueOf(j2));
                            contentValues.put("message_row_id", Long.valueOf(j));
                            A01.A03.A02(contentValues, "message_vcard_jid");
                        }
                    }
                    A00.A00();
                    A00.close();
                    A01.close();
                } finally {
                }
            }
        } finally {
        }
    }

    public boolean A09() {
        if (!this.A08.A0C() || this.A0B.A00("new_vcards_ready", 0) != 1) {
            return false;
        }
        return true;
    }
}
