package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

/* renamed from: X.0SS  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0SS {
    public final C04520Ma A00;

    public AnonymousClass0SS(C04520Ma r1) {
        this.A00 = r1;
    }

    public static String A00(EnumC03860Jk r3, String str, boolean z) {
        String str2;
        StringBuilder sb = new StringBuilder("lottie_cache_");
        sb.append(str.replaceAll("\\W+", ""));
        if (z) {
            StringBuilder sb2 = new StringBuilder(".temp");
            sb2.append(r3.extension);
            str2 = sb2.toString();
        } else {
            str2 = r3.extension;
        }
        sb.append(str2);
        return sb.toString();
    }

    public final File A01() {
        File file = new File(this.A00.A00.getCacheDir(), "lottie_network_cache");
        if (file.isFile()) {
            file.delete();
        }
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    public File A02(EnumC03860Jk r6, InputStream inputStream, String str) {
        File file = new File(A01(), A00(r6, str, true));
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            byte[] bArr = new byte[EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH];
            while (true) {
                int read = inputStream.read(bArr);
                if (read != -1) {
                    fileOutputStream.write(bArr, 0, read);
                } else {
                    fileOutputStream.flush();
                    fileOutputStream.close();
                    return file;
                }
            }
        } finally {
            inputStream.close();
        }
    }
}
