package X;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.Map;

/* renamed from: X.2L0  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2L0 extends AbstractC49422Kr {
    public AnonymousClass2L0(AbstractC15710nm r1, C14850m9 r2, C16120oU r3, C450720b r4, Map map) {
        super(r1, r2, r3, r4, map);
    }

    @Override // X.AbstractC49422Kr
    public void A01(AnonymousClass1V8 r9) {
        AbstractC450820c r5;
        Bundle bundle;
        Handler handler;
        int i;
        int i2;
        AnonymousClass1V8 A0D = r9.A0D(0);
        AbstractC15710nm r1 = this.A01;
        AbstractC14640lm A00 = C15380n4.A00(r9.A0A(r1, Jid.class, "from"));
        AbstractC14640lm A002 = C15380n4.A00(r9.A0A(r1, Jid.class, "participant"));
        if (AnonymousClass1V8.A02(A0D, "composing")) {
            String A0I = A0D.A0I("media", null);
            C450720b r3 = this.A04;
            UserJid of = UserJid.of(A002);
            StringBuilder sb = new StringBuilder("xmpp/reader/read/compose/composing ");
            sb.append(A00);
            sb.append(" ");
            sb.append(of);
            sb.append(" ");
            sb.append(A0I);
            Log.i(sb.toString());
            r5 = r3.A00;
            boolean equals = "audio".equals(A0I);
            bundle = new Bundle();
            bundle.putParcelable("jid", A00);
            bundle.putParcelable("author", of);
            bundle.putInt("media", equals ? 1 : 0);
            handler = null;
            i = 0;
            i2 = 20;
        } else if (AnonymousClass1V8.A02(A0D, "paused")) {
            C450720b r32 = this.A04;
            UserJid of2 = UserJid.of(A002);
            StringBuilder sb2 = new StringBuilder("xmpp/reader/read/compose/paused ");
            sb2.append(A00);
            sb2.append(" ");
            sb2.append(of2);
            Log.i(sb2.toString());
            r5 = r32.A00;
            bundle = new Bundle();
            bundle.putParcelable("jid", A00);
            bundle.putParcelable("author", of2);
            handler = null;
            i = 0;
            i2 = 21;
        } else {
            return;
        }
        r5.AYY(Message.obtain(handler, i, i2, i, bundle));
    }
}
