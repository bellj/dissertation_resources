package X;

import android.net.Uri;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.os.Process;
import com.whatsapp.util.Log;
import com.whatsapp.util.NativeUtils;
import com.whatsapp.util.StatResult;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.0nk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15690nk {
    public final AnonymousClass01H A00 = new C002601e(null, new C29581Tq(this));
    public final Set A01 = new HashSet();
    public volatile C14330lG A02;
    public volatile Set A03 = new HashSet();

    public static Collection A00(File[] fileArr, int i) {
        if (fileArr == null || i == 0) {
            return Collections.emptyList();
        }
        int length = fileArr.length;
        ArrayList arrayList = new ArrayList(length);
        for (File file : fileArr) {
            try {
                try {
                    StatResult lstatOpenFile = StatResult.lstatOpenFile(file.getPath());
                    if (!lstatOpenFile.A05) {
                        if (file.isDirectory()) {
                            arrayList.addAll(A00(file.listFiles(), i - 1));
                        } else if (lstatOpenFile.A01 == 1) {
                            arrayList.add(Long.valueOf(lstatOpenFile.A04));
                        }
                    }
                } catch (Exception e) {
                    throw new IOException(e);
                    break;
                }
            } catch (IOException e2) {
                StringBuilder sb = new StringBuilder("externalfilevalidator/file read error: ");
                sb.append(file);
                Log.e(sb.toString(), e2);
            }
        }
        return arrayList;
    }

    public void A01(Uri uri) {
        if (this.A01.contains(uri.getAuthority())) {
            StringBuilder sb = new StringBuilder("externalfilevalidator/don't allow sharing ");
            sb.append(uri);
            throw new IOException(sb.toString());
        }
    }

    public void A02(ParcelFileDescriptor parcelFileDescriptor) {
        StatResult statOpenFile = StatResult.statOpenFile(NativeUtils.getFileDescriptorForFileDescriptor(parcelFileDescriptor.getFileDescriptor()));
        StringBuilder sb = new StringBuilder("parcelFileDescriptor=");
        sb.append(parcelFileDescriptor);
        A03(statOpenFile, sb.toString());
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:8:0x0039 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:69:0x000c */
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.lang.String */
    /* JADX DEBUG: Multi-variable search result rejected for r0v34, resolved type: java.lang.StringBuilder */
    /* JADX DEBUG: Multi-variable search result rejected for r0v43, resolved type: java.lang.StringBuilder */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r3v1, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r3v2 */
    /* JADX WARN: Type inference failed for: r3v5, types: [long] */
    public final void A03(StatResult statResult, String str) {
        ParcelFileDescriptor parcelFileDescriptor;
        String str2;
        Collection A00;
        int intValue;
        try {
            str2 = "externalfilevalidator/getProcDeviceId/close failed: ";
            parcelFileDescriptor = null;
            try {
                try {
                    parcelFileDescriptor = ParcelFileDescriptor.open(new File("/proc/self"), 268435456);
                    Long valueOf = Long.valueOf(StatResult.statOpenFile(NativeUtils.getFileDescriptorForFileDescriptor(parcelFileDescriptor.getFileDescriptor())).A03);
                    try {
                        parcelFileDescriptor.close();
                    } catch (IOException e) {
                        StringBuilder sb = new StringBuilder();
                        sb.append((String) str2);
                        sb.append(e);
                        Log.e(sb.toString());
                    }
                    if (valueOf != null) {
                        str2 = valueOf.longValue();
                        if (str2 == statResult.A03) {
                            StringBuilder sb2 = new StringBuilder("file is on the proc filesystem; not permitting nefarious file share operation; ");
                            sb2.append(str);
                            throw new IOException(sb2.toString());
                        }
                    }
                } catch (FileNotFoundException e2) {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("externalfilevalidator/getProcDeviceId/proc file not found: ");
                    sb3.append(e2);
                    Log.e(sb3.toString());
                    if (0 != 0) {
                        try {
                            parcelFileDescriptor.close();
                        } catch (IOException e3) {
                            StringBuilder sb4 = new StringBuilder();
                            sb4.append((String) str2);
                            sb4.append(e3);
                            Log.e(sb4.toString());
                        }
                    }
                }
                int myUid = Process.myUid();
                int i = statResult.A02;
                if (myUid == i) {
                    try {
                        StringBuilder sb5 = new StringBuilder();
                        sb5.append(Environment.getExternalStorageDirectory().getCanonicalPath());
                        sb5.append("/.");
                        try {
                            Integer valueOf2 = Integer.valueOf(StatResult.lstatOpenFile(sb5.toString()).A00);
                            if (valueOf2 != null && (intValue = valueOf2.intValue()) != i && intValue == statResult.A00 && !statResult.A05) {
                                return;
                            }
                        } catch (Exception e4) {
                            throw new IOException(e4);
                        }
                    } catch (IOException e5) {
                        Log.e("externalfilevalidator/getExternalStorageGid/unable to read external storage dir", e5);
                    }
                    Set set = this.A03;
                    Long valueOf3 = Long.valueOf(statResult.A04);
                    if (!set.contains(valueOf3)) {
                        C28181Ma r6 = new C28181Ma("externalfilevalidator/update whitelist");
                        HashSet hashSet = new HashSet();
                        for (File file : (Set) this.A00.get()) {
                            if (file.isFile()) {
                                A00 = A00(new File[]{file}, 3);
                            } else {
                                A00 = A00(file.listFiles(), 3);
                            }
                            hashSet.addAll(A00);
                        }
                        this.A03 = hashSet;
                        r6.A01();
                        if (!hashSet.contains(valueOf3)) {
                            StringBuilder sb6 = new StringBuilder("file is owned by our application; not permitting nefarious file share operation; ");
                            sb6.append(str);
                            throw new IOException(sb6.toString());
                        }
                    }
                }
            } catch (Throwable th) {
                throw th;
            }
        } catch (Throwable th2) {
            if (parcelFileDescriptor != null) {
                try {
                    parcelFileDescriptor.close();
                    throw th2;
                } catch (IOException e6) {
                    StringBuilder sb7 = new StringBuilder();
                    sb7.append(str2);
                    sb7.append(e6);
                    Log.e(sb7.toString());
                    throw th2;
                }
            }
        }
    }

    public void A04(File file) {
        String canonicalPath = file.getCanonicalPath();
        try {
            StatResult lstatOpenFile = StatResult.lstatOpenFile(canonicalPath);
            StringBuilder sb = new StringBuilder("canonicalFilePath=");
            sb.append(canonicalPath);
            A03(lstatOpenFile, sb.toString());
        } catch (Exception e) {
            throw new IOException(e);
        }
    }

    public void A05(FileInputStream fileInputStream) {
        StatResult statOpenFile = StatResult.statOpenFile(NativeUtils.getFileDescriptorForFileDescriptor(fileInputStream.getFD()));
        StringBuilder sb = new StringBuilder("fileInputStream=");
        sb.append(fileInputStream);
        A03(statOpenFile, sb.toString());
    }
}
