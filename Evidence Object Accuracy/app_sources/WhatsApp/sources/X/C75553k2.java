package X;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.3k2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75553k2 extends AnonymousClass03U {
    public final Context A00;
    public final ImageView A01;
    public final TextView A02;
    public final AbstractC37291lx A03;

    public C75553k2(Context context, View view, AbstractC37291lx r4) {
        super(view);
        this.A00 = context;
        this.A03 = r4;
        this.A01 = C12970iu.A0K(view, R.id.chevron);
        this.A02 = C12960it.A0I(view, R.id.view_more);
    }
}
