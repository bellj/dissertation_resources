package X;

import com.whatsapp.util.Log;
import java.util.ArrayList;

/* renamed from: X.69Y  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass69Y implements AbstractC43531xB {
    @Override // X.AbstractC43531xB
    public /* synthetic */ int AH0() {
        return 0;
    }

    @Override // X.AbstractC43531xB
    public ArrayList AYv(AnonymousClass102 r5, AnonymousClass1V8 r6) {
        String str;
        ArrayList A0l = C12960it.A0l();
        String str2 = r6.A00;
        if (str2.equals("card-update")) {
            try {
                AnonymousClass1V8 A0F = r6.A0F("card");
                C119775f5 r0 = new C119775f5();
                r0.A01(r5, A0F, 0);
                A0l.add(r0);
                return A0l;
            } catch (AnonymousClass1V9 unused) {
                str = "PAY: BrazilProtoParser/parse: no card node for card-update notification";
            }
        } else {
            if (str2.equals("merchant-update")) {
                try {
                    AnonymousClass1V8 A0F2 = r6.A0F("merchant");
                    C119785f6 r02 = new C119785f6();
                    r02.A01(r5, A0F2, 0);
                    A0l.add(r02);
                    return A0l;
                } catch (AnonymousClass1V9 unused2) {
                    str = "PAY: BrazilProtoParser/parse: no merchant node for merchant-update notification";
                }
            }
            return A0l;
        }
        Log.w(str);
        return A0l;
    }

    @Override // X.AbstractC43531xB
    public /* synthetic */ C14580lf AYw(AnonymousClass1V8 r2) {
        throw C12980iv.A0u("Asynchronous parsing is not supported in Sync Mode");
    }
}
