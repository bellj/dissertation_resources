package X;

import android.os.Parcel;
import android.os.RemoteException;
import android.os.SystemClock;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/* renamed from: X.0mZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C15100mZ extends AbstractC15040mS {
    public C79663qy A00;
    public final AnonymousClass3LI A01 = new AnonymousClass3LI(this);
    public final AbstractC14150kw A02;
    public final C93924ay A03;

    public C15100mZ(C14160kx r3) {
        super(r3);
        this.A03 = new C93924ay(r3.A04);
        this.A02 = new C79693r1(r3, this);
    }

    public static final void A00(C15100mZ r3) {
        r3.A03.A00 = SystemClock.elapsedRealtime();
        r3.A02.A02(((Number) C88904Hw.A0A.A00()).longValue());
    }

    public final void A0H() {
        C14170ky.A00();
        A0G();
        try {
            AnonymousClass3IW.A00().A01(((C15050mT) this).A00.A00, this.A01);
        } catch (IllegalArgumentException | IllegalStateException unused) {
        }
        if (this.A00 != null) {
            this.A00 = null;
            C56572lE r0 = ((C15050mT) this).A00.A06;
            C14160kx.A01(r0);
            r0.A0G();
            C14170ky.A00();
            C15030mR r1 = r0.A00;
            C14170ky.A00();
            r1.A0G();
            r1.A09("Service disconnected");
        }
    }

    public final boolean A0I() {
        C14170ky.A00();
        A0G();
        return this.A00 != null;
    }

    public final boolean A0J(AnonymousClass3H2 r10) {
        C92324Vl r0;
        C13020j0.A01(r10);
        C14170ky.A00();
        A0G();
        C79663qy r8 = this.A00;
        if (r8 == null) {
            return false;
        }
        if (r10.A05) {
            r0 = C88904Hw.A0Z;
        } else {
            r0 = C88904Hw.A0Y;
        }
        String str = (String) r0.A00();
        List emptyList = Collections.emptyList();
        try {
            Map map = r10.A04;
            long j = r10.A02;
            Parcel obtain = Parcel.obtain();
            obtain.writeInterfaceToken(r8.A01);
            obtain.writeMap(map);
            obtain.writeLong(j);
            obtain.writeString(str);
            obtain.writeTypedList(emptyList);
            Parcel obtain2 = Parcel.obtain();
            r8.A00.transact(1, obtain, obtain2, 0);
            obtain2.readException();
            obtain.recycle();
            obtain2.recycle();
            A00(this);
            return true;
        } catch (RemoteException unused) {
            A09("Failed to send hits to AnalyticsService");
            return false;
        }
    }
}
