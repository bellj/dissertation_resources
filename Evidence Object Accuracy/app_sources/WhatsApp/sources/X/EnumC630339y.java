package X;

import com.facebook.msys.mci.DefaultCrypto;
import java.util.Map;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.39y  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class EnumC630339y extends Enum {
    public static final Map A00 = C12970iu.A11();
    public static final Map A01 = C12970iu.A11();
    public static final /* synthetic */ EnumC630339y[] A02;
    public final String[] otherEncodingNames;
    public final int[] values;

    static {
        EnumC630339y r33 = new EnumC630339y("Cp437", new int[]{0, 2}, new String[0], 0);
        EnumC630339y r32 = new EnumC630339y("ISO8859_1", new int[]{1, 3}, new String[]{"ISO-8859-1"}, 1);
        EnumC630339y r31 = new EnumC630339y("ISO8859_2", new String[]{"ISO-8859-2"}, 2, 4);
        EnumC630339y r30 = new EnumC630339y("ISO8859_3", new String[]{"ISO-8859-3"}, 3, 5);
        EnumC630339y r29 = new EnumC630339y("ISO8859_4", new String[]{"ISO-8859-4"}, 4, 6);
        EnumC630339y r28 = new EnumC630339y("ISO8859_5", new String[]{"ISO-8859-5"}, 5, 7);
        EnumC630339y r27 = new EnumC630339y("ISO8859_6", new String[]{"ISO-8859-6"}, 6, 8);
        EnumC630339y r26 = new EnumC630339y("ISO8859_7", new String[]{"ISO-8859-7"}, 7, 9);
        EnumC630339y r25 = new EnumC630339y("ISO8859_8", new String[]{"ISO-8859-8"}, 8, 10);
        EnumC630339y r24 = new EnumC630339y("ISO8859_9", new String[]{"ISO-8859-9"}, 9, 11);
        EnumC630339y r23 = new EnumC630339y("ISO8859_10", new String[]{"ISO-8859-10"}, 10, 12);
        EnumC630339y r22 = new EnumC630339y("ISO8859_11", new String[]{"ISO-8859-11"}, 11, 13);
        String[] A08 = C13000ix.A08();
        A08[0] = "ISO-8859-13";
        EnumC630339y r21 = new EnumC630339y("ISO8859_13", A08, 12, 15);
        String[] A082 = C13000ix.A08();
        A082[0] = "ISO-8859-14";
        EnumC630339y r20 = new EnumC630339y("ISO8859_14", A082, 13, 16);
        String[] A083 = C13000ix.A08();
        A083[0] = "ISO-8859-15";
        EnumC630339y r19 = new EnumC630339y("ISO8859_15", A083, 14, 17);
        String[] A084 = C13000ix.A08();
        A084[0] = "ISO-8859-16";
        EnumC630339y r18 = new EnumC630339y("ISO8859_16", A084, 15, 18);
        String[] A085 = C13000ix.A08();
        A085[0] = "Shift_JIS";
        EnumC630339y r17 = new EnumC630339y("SJIS", A085, 16, 20);
        String[] A086 = C13000ix.A08();
        A086[0] = "windows-1250";
        EnumC630339y r16 = new EnumC630339y("Cp1250", A086, 17, 21);
        String[] A087 = C13000ix.A08();
        A087[0] = "windows-1251";
        EnumC630339y r15 = new EnumC630339y("Cp1251", A087, 18, 22);
        String[] A088 = C13000ix.A08();
        A088[0] = "windows-1252";
        EnumC630339y r14 = new EnumC630339y("Cp1252", A088, 19, 23);
        String[] A089 = C13000ix.A08();
        A089[0] = "windows-1256";
        EnumC630339y r13 = new EnumC630339y("Cp1256", A089, 20, 24);
        EnumC630339y r12 = new EnumC630339y("UnicodeBigUnmarked", new String[]{"UTF-16BE", "UnicodeBig"}, 21, 25);
        String[] A0810 = C13000ix.A08();
        A0810[0] = DefaultCrypto.UTF_8;
        EnumC630339y r11 = new EnumC630339y("UTF8", A0810, 22, 26);
        int[] A07 = C13000ix.A07();
        // fill-array-data instruction
        A07[0] = 27;
        A07[1] = 170;
        String[] A0811 = C13000ix.A08();
        A0811[0] = "US-ASCII";
        EnumC630339y r10 = new EnumC630339y("ASCII", A07, A0811, 23);
        EnumC630339y r8 = new EnumC630339y("Big5", new int[]{28}, new String[0], 24);
        EnumC630339y r7 = new EnumC630339y("GB18030", new String[]{"GB2312", "EUC_CN", "GBK"}, 25, 29);
        String[] A0812 = C13000ix.A08();
        A0812[0] = "EUC-KR";
        EnumC630339y r5 = new EnumC630339y("EUC_KR", A0812, 26, 30);
        EnumC630339y[] r4 = new EnumC630339y[27];
        C12990iw.A1P(r33, r32, r4);
        C12980iv.A1P(r31, r30, r29, r4);
        C12970iu.A1R(r28, r27, r26, r25, r4);
        r4[9] = r24;
        r4[10] = r23;
        r4[11] = r22;
        r4[12] = r21;
        r4[13] = r20;
        r4[14] = r19;
        r4[15] = r18;
        r4[16] = r17;
        r4[17] = r16;
        r4[18] = r15;
        r4[19] = r14;
        C12960it.A1G(r13, r12, r11, r10, r4);
        r4[24] = r8;
        r4[25] = r7;
        r4[26] = r5;
        A02 = r4;
        EnumC630339y[] values = values();
        for (EnumC630339y r52 : values) {
            for (int i : r52.values) {
                A01.put(Integer.valueOf(i), r52);
            }
            A00.put(r52.name(), r52);
            for (String str : r52.otherEncodingNames) {
                A00.put(str, r52);
            }
        }
    }

    public EnumC630339y(String str, int[] iArr, String[] strArr, int i) {
        this.values = iArr;
        this.otherEncodingNames = strArr;
    }

    public EnumC630339y(String str, String[] strArr, int i, int i2) {
        this.values = new int[]{i2};
        this.otherEncodingNames = strArr;
    }

    public static EnumC630339y valueOf(String str) {
        return (EnumC630339y) Enum.valueOf(EnumC630339y.class, str);
    }

    public static EnumC630339y[] values() {
        return (EnumC630339y[]) A02.clone();
    }
}
