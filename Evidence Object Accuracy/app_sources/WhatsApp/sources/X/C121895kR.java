package X;

import android.content.Context;
import android.content.Intent;
import com.whatsapp.payments.ui.NoviPayHubAddPaymentMethodActivity;
import com.whatsapp.payments.ui.NoviTransactionMethodDetailsFragment;

/* renamed from: X.5kR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C121895kR extends AbstractC121845k9 {
    public final /* synthetic */ NoviTransactionMethodDetailsFragment A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C121895kR(Context context, AnonymousClass018 r2, C17070qD r3, AnonymousClass61F r4, NoviTransactionMethodDetailsFragment noviTransactionMethodDetailsFragment) {
        super(context, r2, r3, r4);
        this.A00 = noviTransactionMethodDetailsFragment;
    }

    @Override // X.AbstractC1311861p
    public void ALx() {
        NoviTransactionMethodDetailsFragment noviTransactionMethodDetailsFragment = this.A00;
        Intent A0D = C12990iw.A0D(noviTransactionMethodDetailsFragment.A0B(), NoviPayHubAddPaymentMethodActivity.class);
        A0D.putExtra("extra_funding_category", "balance_top_up");
        noviTransactionMethodDetailsFragment.A0v(A0D);
    }
}
