package X;

import android.database.Cursor;
import android.text.TextUtils;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape0S0410000_I0;
import com.facebook.redex.RunnableBRunnable0Shape1S0300000_I0_1;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;

/* renamed from: X.0z7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22460z7 {
    public final C14330lG A00;
    public final C14900mE A01;
    public final C14820m6 A02;
    public final AnonymousClass018 A03;
    public final C20340vb A04;
    public final C14850m9 A05;
    public final C22560zH A06;
    public final C22550zG A07;
    public final C22540zF A08;
    public final C22570zI A09;
    public final C22530zE A0A;
    public final C22580zJ A0B;
    public final C20350vc A0C;
    public final AbstractC14440lR A0D;

    public C22460z7(C14330lG r1, C14900mE r2, C14820m6 r3, AnonymousClass018 r4, C20340vb r5, C14850m9 r6, C22560zH r7, C22550zG r8, C22540zF r9, C22570zI r10, C22530zE r11, C22580zJ r12, C20350vc r13, AbstractC14440lR r14) {
        this.A05 = r6;
        this.A01 = r2;
        this.A0D = r14;
        this.A00 = r1;
        this.A03 = r4;
        this.A02 = r3;
        this.A04 = r5;
        this.A0A = r11;
        this.A0C = r13;
        this.A08 = r9;
        this.A07 = r8;
        this.A06 = r7;
        this.A09 = r10;
        this.A0B = r12;
    }

    public void A00(ImageView imageView, TextView textView, C30921Zi r9) {
        AnonymousClass04D.A09(textView, 4, 20, 2, 2);
        ViewGroup.LayoutParams layoutParams = textView.getLayoutParams();
        layoutParams.width = textView.getResources().getDimensionPixelSize(R.dimen.quoted_payment_amount_expressive_background_width);
        textView.setLayoutParams(layoutParams);
        textView.setTextColor(r9.A0C);
        textView.requestLayout();
        imageView.setBackgroundColor(r9.A0A);
        if (!TextUtils.isEmpty(r9.A01)) {
            imageView.setContentDescription(r9.A01);
        }
        A01(imageView, r9, imageView.getLayoutParams().width, imageView.getLayoutParams().height, false);
    }

    public void A01(ImageView imageView, C30921Zi r11, int i, int i2, boolean z) {
        if (r11.A02(this.A00.A09()).exists()) {
            this.A08.A00(imageView, r11, i, i2);
        } else if (!z) {
            this.A02.A19(true);
        } else if (this.A05.A07(1084)) {
            this.A0C.A03(r11, this.A0A);
        } else {
            this.A0D.Aaz(new C455422b(new C455522c(imageView, r11, this, i, i2), this), new Void[0]);
        }
    }

    public void A02(C30921Zi r4) {
        if (r4.A02(this.A00.A09()).exists()) {
            return;
        }
        if (this.A05.A07(1084)) {
            this.A0C.A03(r4, this.A0A);
        } else {
            this.A0D.Aaz(new C455422b(null, this), new Void[0]);
        }
    }

    public final void A03(C30921Zi r28, AnonymousClass22Y r29) {
        AnonymousClass1RN r1;
        StringBuilder sb;
        C14330lG r12 = this.A00;
        boolean exists = r28.A02(r12.A09()).exists();
        String str = r28.A0G;
        File file = null;
        if (!"image/webp".equals(str)) {
            sb = new StringBuilder("PAY: PaymentBackgroundRepository/downloadPaymentBackground/unsupported mimetype=");
            sb.append(str);
        } else {
            File A02 = r28.A02(r12.A09());
            if (!A02.exists()) {
                if (TextUtils.isEmpty(r28.A05)) {
                    sb = new StringBuilder("PAY: PaymentBackgroundRepository/downloadPaymentBackground/missing url for background id=");
                    sb.append(r28.A0F);
                } else {
                    C22560zH r0 = this.A06;
                    C14850m9 r9 = r0.A04;
                    C14900mE r4 = r0.A00;
                    C18790t3 r6 = r0.A02;
                    AnonymousClass22Z r3 = new AnonymousClass22Z(r4, r0.A01, r6, r0.A03, r28, r9, r0.A05, r0.A06, r0.A07, A02);
                    C22370yy r2 = r3.A01;
                    AbstractC28771Oy r13 = ((AbstractRunnableC38601oN) r3).A01;
                    String str2 = r3.A00.A05;
                    AnonymousClass009.A05(str2);
                    if (r2.A0D(r13, r3, null, null, str2, false)) {
                        try {
                            r1 = (AnonymousClass1RN) r3.A02.get();
                        } catch (InterruptedException | ExecutionException e) {
                            Log.e("DuplicatePaymentBackgroundDownloadListener/waitForResult ", e);
                            r1 = new AnonymousClass1RN(1);
                        }
                    } else {
                        r3.A5g(r3);
                        r1 = r3.A01().A00;
                    }
                    if (r1.A00 != 0) {
                        A02 = null;
                    }
                }
            }
            file = A02;
            this.A01.A0H(new RunnableBRunnable0Shape0S0410000_I0(this, r29, r28, file, 3, exists));
        }
        Log.e(sb.toString());
        this.A01.A0H(new RunnableBRunnable0Shape0S0410000_I0(this, r29, r28, file, 3, exists));
    }

    public final void A04(AnonymousClass22W r7) {
        C20340vb r5 = this.A04;
        Log.i("PAY: PaymentBackgroundStore/getPaymentBackgroundsForPicker");
        ArrayList arrayList = new ArrayList();
        C16310on A01 = r5.A00.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT payment_background.background_id, file_size, width, height, mime_type, placeholder_color, text_color, subtext_color, media_key, media_key_timestamp, file_sha256, file_enc_sha256, direct_path, fullsize_url, description, lg FROM payment_background INNER JOIN payment_background_order ON payment_background_order.background_id=payment_background.background_id ORDER BY payment_background_order.background_order ASC", null);
            while (A09.moveToNext()) {
                arrayList.add(r5.A00(A09, null));
            }
            A09.close();
            A01.close();
            StringBuilder sb = new StringBuilder("PAY: PaymentBackgroundStore/getPaymentBackgroundsForPicker/result size=");
            sb.append(arrayList.size());
            Log.i(sb.toString());
            this.A01.A0H(new RunnableBRunnable0Shape1S0300000_I0_1(this, arrayList, r7, 22));
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                A03((C30921Zi) it.next(), r7);
            }
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
