package X;

import android.content.Context;
import android.text.TextUtils;
import com.whatsapp.payments.IDxRCallbackShape2S0100000_3_I1;
import com.whatsapp.util.Log;
import com.whatsapp.wamsys.JniBridge;
import java.io.UnsupportedEncodingException;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5xZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129345xZ {
    public final Context A00;
    public final C14900mE A01;
    public final C15570nT A02;
    public final C18640sm A03;
    public final C14830m7 A04;
    public final C17220qS A05;
    public final AnonymousClass60Z A06;
    public final C18650sn A07;
    public final C18600si A08;
    public final C18610sj A09;
    public final AnonymousClass60T A0A;
    public final C128535wG A0B;
    public final C129095xA A0C;

    public C129345xZ(Context context, C14900mE r2, C15570nT r3, C18640sm r4, C14830m7 r5, C17220qS r6, AnonymousClass60Z r7, C18650sn r8, C18600si r9, C18610sj r10, AnonymousClass60T r11, C128535wG r12, C129095xA r13) {
        this.A04 = r5;
        this.A00 = context;
        this.A01 = r2;
        this.A02 = r3;
        this.A05 = r6;
        this.A08 = r9;
        this.A09 = r10;
        this.A0C = r13;
        this.A06 = r7;
        this.A07 = r8;
        this.A03 = r4;
        this.A0A = r11;
        this.A0B = r12;
    }

    public void A00(C452120p r12, AnonymousClass6B7 r13) {
        if (r12 != null || r13 == null) {
            Log.i("PAY: BrazilDeviceRegistrationAction missing key");
            C128535wG r2 = this.A0B;
            new C452120p();
            r2.A00(null);
            return;
        }
        Log.i("PAY: BrazilDeviceRegistrationAction starts to bind device");
        AnonymousClass6B5 r4 = (AnonymousClass6B5) r13.A00;
        String A00 = this.A0C.A00(5);
        String A06 = this.A08.A06();
        AnonymousClass60Z r3 = this.A06;
        String A01 = r3.A01(5);
        String str = null;
        if (!TextUtils.isEmpty(A01)) {
            byte[] bArr = new byte[0];
            try {
                byte[] bArr2 = (byte[]) JniBridge.jvidispatchOO(28, C117315Zl.A0a(A01));
                bArr = bArr2;
                if (bArr2 == null) {
                }
            } catch (UnsupportedEncodingException e) {
                Log.e("PAY: BrazilTokenizationHelper/generateDevicePublicKeyPem failed generating public pem key: ", e);
            }
            str = new String(bArr).split("\u0000")[0];
        }
        JSONObject A0a = C117295Zj.A0a();
        try {
            A0a.put("deviceId", A00).put("devicePublicKey", str).put("walletId", A06);
        } catch (JSONException e2) {
            Log.i(C12960it.A0b("PAY: BrazilDeviceRegistrationAction payload generation failed :", e2));
            e2.printStackTrace();
        }
        String A02 = r3.A02(r4, A0a.toString());
        String A0U = C117295Zj.A0U(this.A02, this.A04);
        C17220qS r32 = this.A05;
        String A012 = r32.A01();
        C117325Zm.A06(r32, new IDxRCallbackShape2S0100000_3_I1(this.A00, this.A01, this.A07, this, 2), new C126225sX(new AnonymousClass3CT(A012), A0U, A02, A00).A00, A012);
    }
}
