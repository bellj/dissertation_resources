package X;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4jM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98854jM implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        PendingIntent pendingIntent = null;
        String str = null;
        int i = 0;
        int i2 = 0;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                i = C95664e9.A02(parcel, readInt);
            } else if (c == 2) {
                i2 = C95664e9.A02(parcel, readInt);
            } else if (c != 3) {
                str = C95664e9.A09(parcel, str, c, 4, readInt);
            } else {
                pendingIntent = (PendingIntent) C95664e9.A07(parcel, PendingIntent.CREATOR, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C56492ky(pendingIntent, str, i, i2);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new C56492ky[i];
    }
}
