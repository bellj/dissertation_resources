package X;

import android.content.DialogInterface;

/* renamed from: X.4hL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class DialogInterface$OnDismissListenerC97604hL implements DialogInterface.OnDismissListener, DialogInterface.OnCancelListener {
    public AnonymousClass016 A00 = C12980iv.A0T();
    public AnonymousClass016 A01 = C12980iv.A0T();

    @Override // android.content.DialogInterface.OnCancelListener
    public void onCancel(DialogInterface dialogInterface) {
        this.A00.A0A(dialogInterface);
    }

    @Override // android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        this.A01.A0A(dialogInterface);
    }
}
