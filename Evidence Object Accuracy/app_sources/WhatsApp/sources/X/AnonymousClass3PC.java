package X;

import android.content.SharedPreferences;

/* renamed from: X.3PC  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3PC implements AnonymousClass024 {
    public final /* synthetic */ C49052Iy A00;

    public /* synthetic */ AnonymousClass3PC(C49052Iy r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass024
    public /* bridge */ /* synthetic */ void accept(Object obj) {
        AnonymousClass0S8 r1;
        for (AbstractC11390gD r3 : ((AnonymousClass0PZ) obj).A00) {
            if (r3 instanceof AbstractC12840ie) {
                C49052Iy r4 = this.A00;
                SharedPreferences sharedPreferences = r4.A04.A00;
                C12960it.A0t(sharedPreferences.edit(), "detect_device_foldable", true);
                C05400Pk r32 = ((AnonymousClass0ZZ) ((AbstractC12840ie) r3)).A00;
                if (r32.A02 - r32.A01 > r32.A00 - r32.A03) {
                    r1 = AnonymousClass0S8.A01;
                } else {
                    r1 = AnonymousClass0S8.A02;
                }
                if (r1 == AnonymousClass0S8.A02) {
                    C12960it.A0t(sharedPreferences.edit(), "detect_device_foldable_bookmode", true);
                }
                r4.A01 = true;
                r4.AWn();
                return;
            }
        }
    }
}
