package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;

/* renamed from: X.5aA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117565aA extends View.BaseSavedState {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(0);
    public float A00;
    public float A01;

    public /* synthetic */ C117565aA(Parcel parcel) {
        super(parcel);
        this.A01 = parcel.readFloat();
        this.A00 = parcel.readFloat();
    }

    public C117565aA(Parcelable parcelable) {
        super(parcelable);
    }

    @Override // android.view.View.BaseSavedState, android.os.Parcelable, android.view.AbsSavedState
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeFloat(this.A01);
        parcel.writeFloat(this.A00);
    }
}
