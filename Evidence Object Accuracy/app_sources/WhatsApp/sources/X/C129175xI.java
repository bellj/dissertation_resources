package X;

/* renamed from: X.5xI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129175xI {
    public final C14900mE A00;
    public final C16170oZ A01;
    public final C14410lO A02;
    public final C18610sj A03;
    public final C20350vc A04;
    public final C20360vd A05;

    public C129175xI(C14900mE r1, C16170oZ r2, C14410lO r3, C18610sj r4, C20350vc r5, C20360vd r6) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = r4;
        this.A05 = r6;
        this.A04 = r5;
    }

    public AnonymousClass1IR A00(AbstractC30791Yv r14, C30821Yy r15, AbstractC28901Pl r16, AbstractC30891Zf r17, C30921Zi r18, AnonymousClass1KC r19, C30061Vy r20, AnonymousClass1KS r21, String str, String str2, String str3, boolean z) {
        AnonymousClass1KC r2 = r19;
        boolean A0H = this.A03.A0H(r14, r15, r16, r17, r18, r20, str2, str3, z);
        if (r19 == null) {
            C14410lO r22 = this.A02;
            AnonymousClass1K9 A00 = C20350vc.A00(r21, str);
            AnonymousClass009.A05(A00);
            r2 = r22.A02(A00, true);
        }
        this.A00.A0H(new Runnable(r2, this, r20) { // from class: X.6Ja
            public final /* synthetic */ AnonymousClass1KC A00;
            public final /* synthetic */ C129175xI A01;
            public final /* synthetic */ C30061Vy A02;

            {
                this.A01 = r2;
                this.A02 = r3;
                this.A00 = r1;
            }

            @Override // java.lang.Runnable
            public final void run() {
                C129175xI r0 = this.A01;
                C30061Vy r23 = this.A02;
                r0.A01.A0L(this.A00, r23);
            }
        });
        this.A05.A00(r18, r20);
        if (A0H) {
            return r20.A0L;
        }
        return null;
    }
}
