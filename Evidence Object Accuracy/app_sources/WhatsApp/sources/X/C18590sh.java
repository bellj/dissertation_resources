package X;

/* renamed from: X.0sh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18590sh {
    public AnonymousClass5UU A00 = null;
    public final C16590pI A01;
    public final C18600si A02;
    public final C17900ra A03;
    public final C17070qD A04;

    public C18590sh(C16590pI r2, C18600si r3, C17900ra r4, C17070qD r5) {
        this.A01 = r2;
        this.A04 = r5;
        this.A02 = r3;
        this.A03 = r4;
    }

    public final AnonymousClass5UU A00() {
        AbstractC38041nQ r1;
        String str;
        AbstractC16830pp AFX;
        C17900ra r2 = this.A03;
        if (r2.A01() != null) {
            r1 = this.A04.A01(r2.A01().A03);
        } else {
            r1 = null;
        }
        AbstractC30791Yv A00 = r2.A00();
        if (A00 != null) {
            str = ((AbstractC30781Yu) A00).A04;
        } else {
            str = null;
        }
        if (r1 == null || (AFX = r1.AFX(str)) == null) {
            return null;
        }
        return AFX.AFB(this.A01, this.A02);
    }

    public String A01() {
        AnonymousClass5UU r0 = this.A00;
        if (r0 == null) {
            r0 = A00();
            this.A00 = r0;
            if (r0 == null) {
                return null;
            }
        }
        return r0.getId();
    }
}
