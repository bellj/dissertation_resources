package X;

import android.content.SharedPreferences;
import android.net.Uri;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.io.File;

/* renamed from: X.60U  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass60U {
    public C120035fV A00;
    public C127315uI A01;
    public final C14330lG A02;
    public final C15450nH A03;
    public final C18790t3 A04;
    public final C14830m7 A05;
    public final C16590pI A06;
    public final C14950mJ A07;
    public final C14850m9 A08;
    public final C20110vE A09;
    public final C18600si A0A;
    public final C22600zL A0B;

    public AnonymousClass60U(C14330lG r1, C15450nH r2, C18790t3 r3, C14830m7 r4, C16590pI r5, C14950mJ r6, C14850m9 r7, C20110vE r8, C18600si r9, C22600zL r10) {
        this.A05 = r4;
        this.A08 = r7;
        this.A06 = r5;
        this.A04 = r3;
        this.A02 = r1;
        this.A03 = r2;
        this.A07 = r6;
        this.A0B = r10;
        this.A0A = r9;
        this.A09 = r8;
    }

    public static C127315uI A00(byte[] bArr, long j) {
        String str;
        long j2;
        try {
            C27081Fy A01 = C27081Fy.A01(bArr);
            if (!A01.A0b()) {
                Log.e("dyiReportManager/create-report-info failed : invalid e2eMessage -> no document message found");
                return null;
            }
            C40831sP r2 = A01.A0C;
            if (r2 == null) {
                r2 = C40831sP.A0L;
            }
            if ((r2.A00 & 1) == 1) {
                str = r2.A0J;
                if (TextUtils.isEmpty(str)) {
                    Log.e("dyiReportManager/create-report-info failed : url is empty");
                    return null;
                } else if (!"https".equalsIgnoreCase(Uri.parse(str).getScheme())) {
                    StringBuilder A0h = C12960it.A0h();
                    A0h.append("dyiReportManager/create-report-info failed : invalid scheme; url =");
                    Log.e(C12960it.A0d(str, A0h));
                    return null;
                }
            } else {
                str = null;
            }
            if ((r2.A00 & 16) == 16) {
                j2 = r2.A04;
            } else {
                j2 = 0;
            }
            return new C127315uI(str, j2, j);
        } catch (C28971Pt e) {
            Log.e("dyiReportManager/create-report-info", e);
            return null;
        }
    }

    public static void A01(AnonymousClass017 r1, AnonymousClass60U r2, String str) {
        r1.A0A(Integer.valueOf(r2.A02(str)));
    }

    public synchronized int A02(String str) {
        SharedPreferences A01;
        String str2;
        A01 = this.A0A.A01();
        if ("personal".equals(str)) {
            str2 = "payment_dyi_report_state";
        } else {
            str2 = "business_payment_dyi_report_state";
        }
        return A01.getInt(str2, -1);
    }

    public synchronized C127315uI A03(String str) {
        byte[] A0G;
        String str2;
        String str3;
        if (this.A01 == null && (A0G = C003501n.A0G(A04(str))) != null) {
            C18600si r7 = this.A0A;
            SharedPreferences A01 = r7.A01();
            boolean equals = "personal".equals(str);
            if (equals) {
                str2 = "payment_dyi_report_timestamp";
            } else {
                str2 = "business_payment_dyi_report_timestamp";
            }
            long j = A01.getLong(str2, -1);
            SharedPreferences A012 = r7.A01();
            if (equals) {
                str3 = "payment_dyi_report_expiration_timestamp";
            } else {
                str3 = "business_payment_dyi_report_expiration_timestamp";
            }
            A012.getLong(str3, -1);
            this.A01 = A00(A0G, j);
        }
        return this.A01;
    }

    public final File A04(String str) {
        String str2;
        File filesDir = this.A06.A00.getFilesDir();
        if ("personal".equals(str)) {
            str2 = "dyi.info";
        } else {
            str2 = "business_dyi.info";
        }
        return new File(filesDir, str2);
    }

    public synchronized void A05(String str) {
        Log.i("dyiReportManager/reset");
        this.A01 = null;
        File A04 = A04(str);
        if (A04.exists() && !A04.delete()) {
            Log.e("dyiReportManager/reset/failed-delete-report-info");
        }
        C14330lG r2 = this.A02;
        File A0G = r2.A0G(str);
        if (A0G.exists() && !A0G.delete()) {
            Log.e("dyiReportManager/reset/failed-delete-report-file");
        }
        C14350lI.A0D(r2.A0J(str), 0);
        this.A0A.A0E(str);
    }
}
