package X;

/* renamed from: X.4cz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C95044cz {
    public int A00;
    public byte[] A01;

    public C95044cz() {
        this.A01 = new byte[64];
    }

    public C95044cz(int i) {
        this.A01 = new byte[i];
    }

    public C95044cz(byte[] bArr) {
        this.A01 = bArr;
        this.A00 = bArr.length;
    }

    public static void A00(C95044cz r3, C95044cz r4) {
        r4.A0A(r3.A01, 0, r3.A00);
    }

    public static void A01(C95044cz r3, byte[] bArr, int i, int i2, int i3) {
        int i4 = i + 1;
        bArr[i] = (byte) i2;
        int i5 = i4 + 1;
        bArr[i4] = (byte) (i3 >>> 8);
        bArr[i5] = (byte) i3;
        r3.A00 = i5 + 1;
    }

    public void A02(int i) {
        int i2 = this.A00;
        int i3 = i2 + 1;
        if (i3 > this.A01.length) {
            A05(1);
        }
        this.A01[i2] = (byte) i;
        this.A00 = i3;
    }

    public void A03(int i) {
        int i2 = this.A00;
        if (i2 + 4 > this.A01.length) {
            A05(4);
        }
        byte[] bArr = this.A01;
        int A0O = C72453ed.A0O(bArr, i2, i);
        bArr[A0O] = (byte) i;
        this.A00 = A0O + 1;
    }

    public void A04(int i) {
        int i2 = this.A00;
        if (i2 + 2 > this.A01.length) {
            A05(2);
        }
        byte[] bArr = this.A01;
        int i3 = i2 + 1;
        C72463ee.A0W(bArr, i, i2);
        bArr[i3] = (byte) i;
        this.A00 = i3 + 1;
    }

    public final void A05(int i) {
        byte[] bArr = this.A01;
        int length = bArr.length << 1;
        int i2 = this.A00;
        int i3 = i + i2;
        if (length <= i3) {
            length = i3;
        }
        byte[] bArr2 = new byte[length];
        System.arraycopy(bArr, 0, bArr2, 0, i2);
        this.A01 = bArr2;
    }

    public final void A06(int i, int i2) {
        int i3 = this.A00;
        if (i3 + 2 > this.A01.length) {
            A05(2);
        }
        byte[] bArr = this.A01;
        int i4 = i3 + 1;
        bArr[i3] = (byte) i;
        bArr[i4] = (byte) i2;
        this.A00 = i4 + 1;
    }

    public final void A07(int i, int i2) {
        int i3 = this.A00;
        if (i3 + 3 > this.A01.length) {
            A05(3);
        }
        A01(this, this.A01, i3, i, i2);
    }

    public final void A08(int i, int i2, int i3) {
        int i4 = this.A00;
        if (i4 + 5 > this.A01.length) {
            A05(5);
        }
        byte[] bArr = this.A01;
        int i5 = i4 + 1;
        bArr[i4] = (byte) i;
        C72463ee.A0W(bArr, i2, i5);
        A01(this, bArr, i5 + 1, i2, i3);
    }

    public final void A09(int i, String str, int i2) {
        byte[] bArr;
        int i3;
        int length = str.length();
        int i4 = i;
        for (int i5 = i; i5 < length; i5++) {
            char charAt = str.charAt(i5);
            i4 = (charAt < 1 || charAt > 127) ? charAt <= 2047 ? i4 + 2 : i4 + 3 : i4 + 1;
        }
        if (i4 <= i2) {
            int i6 = this.A00;
            int i7 = (i6 - i) - 2;
            if (i7 >= 0) {
                byte[] bArr2 = this.A01;
                C72463ee.A0W(bArr2, i4, i7);
                bArr2[i7 + 1] = (byte) i4;
            }
            if ((i6 + i4) - i > this.A01.length) {
                A05(i4 - i);
            }
            int i8 = this.A00;
            while (i < length) {
                int charAt2 = str.charAt(i);
                if (charAt2 < 1 || charAt2 > 127) {
                    bArr = this.A01;
                    int i9 = i8 + 1;
                    if (charAt2 <= 2047) {
                        bArr[i8] = (byte) (((charAt2 >> 6) & 31) | 192);
                        i8 = i9 + 1;
                        bArr[i9] = (byte) ((charAt2 & 63) | 128);
                        i++;
                    } else {
                        bArr[i8] = (byte) (((charAt2 >> 12) & 15) | 224);
                        i8 = i9 + 1;
                        bArr[i9] = (byte) (((charAt2 >> 6) & 63) | 128);
                        i3 = i8 + 1;
                        charAt2 = (charAt2 & 63) | 128;
                    }
                } else {
                    bArr = this.A01;
                    i3 = i8 + 1;
                }
                bArr[i8] = (byte) charAt2;
                i8 = i3;
                i++;
            }
            this.A00 = i8;
            return;
        }
        throw C12970iu.A0f("UTF8 string too large");
    }

    public void A0A(byte[] bArr, int i, int i2) {
        if (this.A00 + i2 > this.A01.length) {
            A05(i2);
        }
        if (bArr != null) {
            System.arraycopy(bArr, i, this.A01, this.A00, i2);
        }
        this.A00 += i2;
    }
}
