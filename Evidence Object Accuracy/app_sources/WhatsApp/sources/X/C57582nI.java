package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2nI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57582nI extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57582nI A08;
    public static volatile AnonymousClass255 A09;
    public int A00;
    public int A01;
    public AbstractC27881Jp A02;
    public AbstractC27881Jp A03;
    public AbstractC27881Jp A04;
    public AbstractC27881Jp A05;
    public AbstractC27881Jp A06;
    public AbstractC27881Jp A07;

    static {
        C57582nI r0 = new C57582nI();
        A08 = r0;
        r0.A0W();
    }

    public C57582nI() {
        AbstractC27881Jp r0 = AbstractC27881Jp.A01;
        this.A03 = r0;
        this.A02 = r0;
        this.A07 = r0;
        this.A06 = r0;
        this.A05 = r0;
        this.A04 = r0;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r7, Object obj, Object obj2) {
        switch (r7.ordinal()) {
            case 0:
                return A08;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                C57582nI r9 = (C57582nI) obj2;
                int i = this.A00;
                boolean A1R = C12960it.A1R(i);
                int i2 = this.A01;
                int i3 = r9.A00;
                this.A01 = r8.Afp(i2, r9.A01, A1R, C12960it.A1R(i3));
                this.A03 = r8.Afm(this.A03, r9.A03, C12960it.A1V(i & 2, 2), C12960it.A1V(i3 & 2, 2));
                this.A02 = r8.Afm(this.A02, r9.A02, C12960it.A1V(this.A00 & 4, 4), C12960it.A1V(r9.A00 & 4, 4));
                this.A07 = r8.Afm(this.A07, r9.A07, C12960it.A1V(this.A00 & 8, 8), C12960it.A1V(r9.A00 & 8, 8));
                this.A06 = r8.Afm(this.A06, r9.A06, C12960it.A1V(this.A00 & 16, 16), C12960it.A1V(r9.A00 & 16, 16));
                this.A05 = r8.Afm(this.A05, r9.A05, C12960it.A1V(this.A00 & 32, 32), C12960it.A1V(r9.A00 & 32, 32));
                this.A04 = r8.Afm(this.A04, r9.A04, C12960it.A1V(this.A00 & 64, 64), C12960it.A1V(r9.A00 & 64, 64));
                if (r8 == C463025i.A00) {
                    this.A00 |= r9.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        int A03 = r82.A03();
                        if (A03 == 0) {
                            break;
                        } else if (A03 == 8) {
                            this.A00 |= 1;
                            this.A01 = r82.A02();
                        } else if (A03 == 18) {
                            this.A00 |= 2;
                            this.A03 = r82.A08();
                        } else if (A03 == 26) {
                            this.A00 |= 4;
                            this.A02 = r82.A08();
                        } else if (A03 == 34) {
                            this.A00 |= 8;
                            this.A07 = r82.A08();
                        } else if (A03 == 42) {
                            this.A00 |= 16;
                            this.A06 = r82.A08();
                        } else if (A03 == 58) {
                            this.A00 |= 32;
                            this.A05 = r82.A08();
                        } else if (A03 == 66) {
                            this.A00 |= 64;
                            this.A04 = r82.A08();
                        } else if (!A0a(r82, A03)) {
                            break;
                        }
                    } catch (C28971Pt e) {
                        throw AbstractC27091Fz.A0J(e, this);
                    } catch (IOException e2) {
                        throw AbstractC27091Fz.A0K(this, e2);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C57582nI();
            case 5:
                return new C82493vi();
            case 6:
                break;
            case 7:
                if (A09 == null) {
                    synchronized (C57582nI.class) {
                        if (A09 == null) {
                            A09 = AbstractC27091Fz.A09(A08);
                        }
                    }
                }
                return A09;
            default:
                throw C12970iu.A0z();
        }
        return A08;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i2 = AbstractC27091Fz.A02(1, this.A01, 0);
        }
        if ((i3 & 2) == 2) {
            i2 = AbstractC27091Fz.A05(this.A03, 2, i2);
        }
        if ((i3 & 4) == 4) {
            i2 = AbstractC27091Fz.A05(this.A02, 3, i2);
        }
        if ((i3 & 8) == 8) {
            i2 = AbstractC27091Fz.A05(this.A07, 4, i2);
        }
        if ((i3 & 16) == 16) {
            i2 = AbstractC27091Fz.A05(this.A06, 5, i2);
        }
        if ((i3 & 32) == 32) {
            i2 = AbstractC27091Fz.A05(this.A05, 7, i2);
        }
        if ((i3 & 64) == 64) {
            i2 = AbstractC27091Fz.A05(this.A04, 8, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0F(1, this.A01);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0K(this.A03, 2);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0K(this.A02, 3);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0K(this.A07, 4);
        }
        if ((this.A00 & 16) == 16) {
            codedOutputStream.A0K(this.A06, 5);
        }
        if ((this.A00 & 32) == 32) {
            codedOutputStream.A0K(this.A05, 7);
        }
        if ((this.A00 & 64) == 64) {
            codedOutputStream.A0K(this.A04, 8);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
