package X;

import android.view.View;
import android.view.ViewTreeObserver;

/* renamed from: X.4o0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC101734o0 implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ View A00;
    public final /* synthetic */ Runnable A01;

    public ViewTreeObserver$OnGlobalLayoutListenerC101734o0(View view, Runnable runnable) {
        this.A00 = view;
        this.A01 = runnable;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        C12980iv.A1F(this.A00, this);
        this.A01.run();
    }
}
