package X;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;

/* renamed from: X.3au  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70203au implements AbstractC41521tf {
    public final /* synthetic */ AnonymousClass2y2 A00;

    @Override // X.AbstractC41521tf
    public /* synthetic */ void AQV() {
    }

    public C70203au(AnonymousClass2y2 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC41521tf
    public int AGm() {
        return AnonymousClass3GD.A01(this.A00.getContext(), 36);
    }

    @Override // X.AbstractC41521tf
    public void Adg(Bitmap bitmap, View view, AbstractC15340mz r4) {
        ImageView imageView = (ImageView) view;
        if (bitmap != null) {
            C12990iw.A1E(imageView);
            imageView.setImageBitmap(bitmap);
            return;
        }
        imageView.setScaleType(ImageView.ScaleType.CENTER);
        AnonymousClass2GE.A06(this.A00, imageView);
    }

    @Override // X.AbstractC41521tf
    public void Adu(View view) {
        ImageView imageView = (ImageView) view;
        imageView.setImageDrawable(null);
        imageView.setBackgroundColor(-7829368);
    }
}
