package X;

/* renamed from: X.3wN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C82903wN extends AbstractC1093851m {
    public final AnonymousClass4BP A00;
    public final AbstractC94534c0 A01;
    public final AbstractC94534c0 A02;

    public C82903wN(AnonymousClass4BP r1, AbstractC94534c0 r2, AbstractC94534c0 r3) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
        toString();
    }

    @Override // X.AnonymousClass5T6
    public boolean A65(AnonymousClass4RG r5) {
        AbstractC94534c0 r3 = this.A01;
        AbstractC94534c0 r2 = this.A02;
        if (r3 instanceof C82963wT) {
            r3 = ((C82963wT) r3).A08(r5);
        }
        if (r2 instanceof C82963wT) {
            r2 = ((C82963wT) r2).A08(r5);
        }
        AnonymousClass5T7 r0 = (AnonymousClass5T7) AnonymousClass4G0.A00.get(this.A00);
        if (r0 != null) {
            return r0.A9i(r3, r2, r5);
        }
        return false;
    }

    public String toString() {
        AnonymousClass4BP r2 = this.A00;
        if (r2 == AnonymousClass4BP.A06) {
            return this.A01.toString();
        }
        StringBuilder A0h = C12960it.A0h();
        C12970iu.A1V(this.A01, A0h);
        A0h.append(" ");
        C12970iu.A1V(r2, A0h);
        A0h.append(" ");
        return C12960it.A0d(this.A02.toString(), A0h);
    }
}
