package X;

/* renamed from: X.58L  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass58L implements AnonymousClass5UW {
    public static final AnonymousClass58V A02 = new AnonymousClass58V();
    public final int A00;
    public final String A01;

    public AnonymousClass58L(String str, int i) {
        this.A01 = str;
        this.A00 = i;
    }

    @Override // X.AnonymousClass5UW
    public boolean A9g(AnonymousClass4V4 r4) {
        C16700pc.A0E(r4, 0);
        Number number = (Number) r4.A00(this.A01);
        if (number == null || number.intValue() <= this.A00) {
            return false;
        }
        return true;
    }
}
