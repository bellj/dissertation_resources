package X;

import android.os.CountDownTimer;
import com.whatsapp.R;
import com.whatsapp.payments.pin.ui.PinBottomSheetDialogFragment;

/* renamed from: X.5Zy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class CountDownTimerC117445Zy extends CountDownTimer {
    public final /* synthetic */ PinBottomSheetDialogFragment A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public CountDownTimerC117445Zy(PinBottomSheetDialogFragment pinBottomSheetDialogFragment, long j) {
        super(j, 1000);
        this.A00 = pinBottomSheetDialogFragment;
    }

    @Override // android.os.CountDownTimer
    public void onFinish() {
        PinBottomSheetDialogFragment pinBottomSheetDialogFragment = this.A00;
        pinBottomSheetDialogFragment.A01 = null;
        pinBottomSheetDialogFragment.A04.setVisibility(4);
        pinBottomSheetDialogFragment.A05.setErrorState(false);
        pinBottomSheetDialogFragment.A05.setEnabled(true);
    }

    @Override // android.os.CountDownTimer
    public void onTick(long j) {
        PinBottomSheetDialogFragment pinBottomSheetDialogFragment = this.A00;
        pinBottomSheetDialogFragment.A04.setText(C12970iu.A0q(pinBottomSheetDialogFragment, C38131nZ.A04(pinBottomSheetDialogFragment.A08, j / 1000), C12970iu.A1b(), 0, R.string.payment_pin_timeout));
    }
}
