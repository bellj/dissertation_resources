package X;

import java.util.Map;

/* renamed from: X.3AE  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3AE {
    public static Object A00(C14240l5 r2, Map map) {
        if (!map.containsKey("initial_lispy")) {
            return map.get("initial");
        }
        try {
            return AnonymousClass3AG.A00(C14220l3.A01, C12980iv.A0V(C12970iu.A0t("initial_lispy", map)), r2);
        } catch (C87444Bn e) {
            C28691Op.A01("StateModule", "Exception gettin initial_lispy value", e);
            return null;
        }
    }
}
