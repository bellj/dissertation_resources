package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0100100_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0101000_I0;
import com.facebook.redex.RunnableBRunnable0Shape11S0100000_I0_11;
import com.facebook.redex.RunnableBRunnable0Shape7S0200000_I0_7;
import com.whatsapp.util.Log;

/* renamed from: X.1yE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C44101yE implements AbstractC21730xt {
    public C44111yF A00;
    public final AbstractC15710nm A01;
    public final C14900mE A02;
    public final C15570nT A03;
    public final C18640sm A04;
    public final C14830m7 A05;
    public final C17220qS A06;

    public C44101yE(AbstractC15710nm r1, C14900mE r2, C15570nT r3, C18640sm r4, C14830m7 r5, C17220qS r6) {
        this.A05 = r5;
        this.A02 = r2;
        this.A01 = r1;
        this.A03 = r3;
        this.A06 = r6;
        this.A04 = r4;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        Log.e("GetBusinessActivityReportProtocolHelper/delivery-error");
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r6, String str) {
        Log.e("GetBusinessActivityReportProtocolHelper/onError");
        int A00 = C41151sz.A00(r6);
        if (this.A00 != null) {
            this.A02.A0I(new RunnableBRunnable0Shape0S0101000_I0(this, A00, 22));
        }
        AbstractC15710nm r3 = this.A01;
        StringBuilder sb = new StringBuilder("error_code=");
        sb.append(A00);
        r3.AaV("GetBusinessActivityReportProtocolHelper/get business activity error", sb.toString(), true);
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r28, String str) {
        C14900mE r3;
        int i;
        AnonymousClass1V8 A0E = r28.A0E("p2b");
        if (A0E != null) {
            AnonymousClass1V8 A0E2 = A0E.A0E("report");
            if (A0E2 != null && this.A00 != null) {
                AnonymousClass1V8 A0E3 = A0E2.A0E("url");
                AnonymousClass1V8 A0E4 = A0E2.A0E("direct_path");
                AnonymousClass1V8 A0E5 = A0E2.A0E("file_name");
                AnonymousClass1V8 A0E6 = A0E2.A0E("file_length");
                AnonymousClass1V8 A0E7 = A0E2.A0E("media_key");
                AnonymousClass1V8 A0E8 = A0E2.A0E("file_sha256");
                AnonymousClass1V8 A0E9 = A0E2.A0E("file_enc_sha256");
                if (A0E3 == null || A0E5 == null || A0E6 == null || A0E8 == null || A0E9 == null || A0E7 == null || A0E4 == null) {
                    r3 = this.A02;
                    i = 18;
                } else {
                    long A01 = C28421Nd.A01(A0E6.A0G(), 0);
                    String A0G = A0E3.A0G();
                    String A0G2 = A0E4.A0G();
                    AnonymousClass009.A05(A0G2);
                    String A0G3 = A0E5.A0G();
                    AnonymousClass009.A05(A0G3);
                    String A0G4 = A0E7.A0G();
                    AnonymousClass009.A05(A0G4);
                    String A0G5 = A0E8.A0G();
                    AnonymousClass009.A05(A0G5);
                    String A0G6 = A0E9.A0G();
                    AnonymousClass009.A05(A0G6);
                    this.A02.A0I(new RunnableBRunnable0Shape7S0200000_I0_7(this, 38, new C44281ye(A0G, A0G2, A0G3, A0G4, A0G5, A0G6, A01, A0E2.A08("creation", 0) * 1000, A0E2.A08("expiration", (this.A05.A00() + 2592000000L) / 1000) * 1000)));
                    return;
                }
            } else if (this.A00 != null) {
                this.A02.A0I(new RunnableBRunnable0Shape0S0100100_I0(this, A0E.A08("timestamp", 0) * 1000, 7));
                return;
            } else {
                return;
            }
        } else if (this.A00 != null) {
            r3 = this.A02;
            i = 17;
        } else {
            return;
        }
        r3.A0I(new RunnableBRunnable0Shape11S0100000_I0_11(this, i));
    }
}
