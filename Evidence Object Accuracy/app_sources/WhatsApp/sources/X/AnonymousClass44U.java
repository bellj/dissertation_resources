package X;

import com.whatsapp.calling.callhistory.CallLogActivity;
import java.util.Set;

/* renamed from: X.44U  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass44U extends AbstractC33331dp {
    public final /* synthetic */ CallLogActivity A00;

    public AnonymousClass44U(CallLogActivity callLogActivity) {
        this.A00 = callLogActivity;
    }

    @Override // X.AbstractC33331dp
    public void A00(Set set) {
        this.A00.A2e();
    }
}
