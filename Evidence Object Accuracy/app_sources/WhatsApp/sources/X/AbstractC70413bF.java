package X;

import android.graphics.Bitmap;
import android.util.Pair;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.webpagepreview.WebPagePreviewView;

/* renamed from: X.3bF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC70413bF implements AbstractC41521tf {
    public final /* synthetic */ C64343Fe A00;

    @Override // X.AbstractC41521tf
    public int AGm() {
        return 0;
    }

    @Override // X.AbstractC41521tf
    public /* synthetic */ void AQV() {
    }

    @Override // X.AbstractC41521tf
    public /* synthetic */ void Adu(View view) {
    }

    public /* synthetic */ AbstractC70413bF(C64343Fe r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC41521tf
    public void Adg(Bitmap bitmap, View view, AbstractC15340mz r9) {
        C14360lJ r0 = r9.A0T;
        AnonymousClass009.A05(r0);
        C64343Fe r4 = this.A00;
        int i = r0.A01;
        int i2 = r0.A00;
        C61102zP r1 = r4.A02;
        r1.A09(i, i2);
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        Pair A07 = r1.A07(makeMeasureSpec, makeMeasureSpec);
        int A05 = C12960it.A05(A07.first);
        int i3 = (A05 - r4.A00) - r4.A01;
        int A052 = (C12960it.A05(A07.second) * i3) / A05;
        int i4 = C12970iu.A1a(i3, A052)[0];
        if (!(this instanceof C60732yV)) {
            C64343Fe r02 = ((C60722yU) this).A00;
            WebPagePreviewView webPagePreviewView = r02.A0A;
            webPagePreviewView.A07(i4, A052);
            if (bitmap == null) {
                webPagePreviewView.setImageLargeThumbWithBackground(AnonymousClass00T.A00(r02.A03, R.color.primary_surface));
            } else {
                webPagePreviewView.setImageLargeThumbWithBitmap(bitmap);
            }
        } else {
            C60732yV r3 = (C60732yV) this;
            C64343Fe r12 = r3.A00;
            WebPagePreviewView webPagePreviewView2 = r12.A0A;
            webPagePreviewView2.A09.getLayoutParams().width = i4;
            webPagePreviewView2.A09.getLayoutParams().height = A052;
            webPagePreviewView2.A09.requestLayout();
            if (bitmap == null) {
                webPagePreviewView2.setVideoLargeThumbWithBackground(AnonymousClass00T.A00(r12.A03, R.color.primary_surface));
                return;
            }
            webPagePreviewView2.setVideoLargeThumbWithBitmap(bitmap);
            r3.A01[0] = bitmap;
        }
    }
}
