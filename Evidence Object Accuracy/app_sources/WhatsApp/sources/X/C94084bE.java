package X;

/* renamed from: X.4bE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C94084bE {
    public static final C94084bE A04 = new C94084bE(-1, -1, -1);
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;

    public C94084bE(int i, int i2, int i3) {
        int i4;
        this.A03 = i;
        this.A01 = i2;
        this.A02 = i3;
        if (AnonymousClass3JZ.A0G(i3)) {
            i4 = AnonymousClass3JZ.A03(i3, i2);
        } else {
            i4 = -1;
        }
        this.A00 = i4;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("AudioFormat[sampleRate=");
        A0k.append(this.A03);
        A0k.append(", channelCount=");
        A0k.append(this.A01);
        A0k.append(", encoding=");
        A0k.append(this.A02);
        return C72453ed.A0t(A0k);
    }
}
