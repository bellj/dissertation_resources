package X;

import java.util.Comparator;

/* renamed from: X.0mB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14870mB implements Comparator {
    @Override // java.util.Comparator
    public int compare(Object obj, Object obj2) {
        return (((C14880mC) obj2).A04 > ((C14880mC) obj).A04 ? 1 : (((C14880mC) obj2).A04 == ((C14880mC) obj).A04 ? 0 : -1));
    }
}
