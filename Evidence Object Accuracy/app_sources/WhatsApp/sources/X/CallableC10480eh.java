package X;

import android.content.Context;
import com.airbnb.lottie.LottieAnimationView;
import java.util.concurrent.Callable;

/* renamed from: X.0eh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class CallableC10480eh implements Callable {
    public final /* synthetic */ int A00;
    public final /* synthetic */ LottieAnimationView A01;

    public CallableC10480eh(LottieAnimationView lottieAnimationView, int i) {
        this.A01 = lottieAnimationView;
        this.A00 = i;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        String str;
        LottieAnimationView lottieAnimationView = this.A01;
        boolean z = lottieAnimationView.A09;
        Context context = lottieAnimationView.getContext();
        int i = this.A00;
        if (z) {
            str = C06550Ub.A08(context, i);
        } else {
            str = null;
        }
        return C06550Ub.A00(context, str, i);
    }
}
