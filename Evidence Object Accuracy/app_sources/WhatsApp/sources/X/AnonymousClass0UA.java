package X;

import android.content.Context;
import com.facebook.common.time.AwakeTimeSinceBootClock;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.0UA  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0UA {
    public C06420Tn A00 = null;
    public AbstractC11560gU A01 = null;
    public AnonymousClass5T4 A02 = null;
    public final C08770bp A03;
    public final AnonymousClass0PT A04;
    public final AnonymousClass01c A05 = C65093Ic.A00().A04();
    public final AnonymousClass4E1 A06;
    public final AtomicLong A07 = new AtomicLong();
    public final AtomicReference A08 = new AtomicReference(C94004b6.A02);
    public final AtomicReference A09 = new AtomicReference();

    public AnonymousClass0UA(C08770bp r3, AnonymousClass0PT r4, AnonymousClass4E1 r5) {
        this.A03 = r3;
        this.A04 = r4;
        this.A06 = r5;
        AwakeTimeSinceBootClock.INSTANCE.now();
    }

    public static AnonymousClass0UA A00(C06020Rw r2, AnonymousClass0PT r3, AnonymousClass3JI r4, AnonymousClass4E1 r5) {
        return new AnonymousClass0UA(new C08770bp(r2, r4), r3, r5);
    }

    public static C06420Tn A01(Context context, String str, Map map) {
        AnonymousClass4MZ r3 = C65093Ic.A00().A05;
        if (r3 != null) {
            if (r3.A01 == null) {
                synchronized (r3) {
                    if (r3.A01 == null) {
                        AnonymousClass4N1 r1 = r3.A00;
                        r3.A01 = new C04580Mg(new C105774ub(r1.A00, r1.A01.A01.A00.A01.A1j));
                    }
                }
            }
            C16700pc.A0E(context, 0);
            C16700pc.A0E(map, 2);
            C06420Tn r32 = new C06420Tn(null, null, 1);
            AbstractC11520gQ r2 = ((C04580Mg) r3.A01).A00;
            C04570Mf r12 = new C04570Mf(r32);
            C105774ub r22 = (C105774ub) r2;
            AbstractC16850pr r4 = (AbstractC16850pr) r22.A01.get();
            r4.A02(null, new AnonymousClass5B4(context, r12, r22), null, str, (String) map.get("params"), null, r4.A00.contains(str));
            return r32;
        }
        throw new IllegalStateException("Unexpectedly attempting to perform a fetch/prefetch. Please ensure that your app provides a fetch action");
    }

    /* JADX INFO: finally extract failed */
    public AnonymousClass01T A02(Context context) {
        try {
            C08770bp r1 = this.A03;
            AnonymousClass01T A04 = r1.A04(context);
            C06420Tn r0 = this.A00;
            if (r0 != null) {
                r0.A04(r1);
                AnonymousClass5T4 A03 = A03(r1);
                this.A02 = A03;
                this.A00.A03(A03);
            }
            return A04;
        } catch (Throwable th) {
            C06420Tn r12 = this.A00;
            if (r12 != null) {
                AnonymousClass5T4 r02 = this.A03;
                r12.A04(r02);
                AnonymousClass5T4 A032 = A03(r02);
                this.A02 = A032;
                this.A00.A03(A032);
            }
            throw th;
        }
    }

    public final AnonymousClass5T4 A03(AnonymousClass5T4 r2) {
        return new C08760bo(this, r2);
    }

    public Throwable A04() {
        AbstractC92144Us r1 = (AbstractC92144Us) this.A09.get();
        if (r1 instanceof C03570Ih) {
            return ((C03570Ih) r1).A01();
        }
        return null;
    }

    public void A05() {
        C06420Tn r0 = this.A00;
        if (r0 != null) {
            r0.A02();
        }
        this.A00 = null;
        this.A03.A06();
        this.A09.set(null);
    }

    public void A06() {
        AnonymousClass5T4 r1 = this.A02;
        if (r1 != null) {
            C06420Tn r0 = this.A00;
            if (r0 != null) {
                r0.A04(r1);
            }
            this.A02 = null;
        }
    }

    public void A07(Context context, AbstractC12090hM r12) {
        AnonymousClass0PT r0;
        String str;
        AbstractC11560gU r2 = this.A01;
        if (r2 == null) {
            AnonymousClass01c r4 = this.A05;
            AnonymousClass0PT r3 = this.A04;
            r2 = r4.AeH(r3.A07, r3.A02, r3.A01, r3.A04);
            Map map = r3.A0A;
            if (map != null) {
                for (Map.Entry entry : map.entrySet()) {
                    entry.getKey();
                    Object value = entry.getValue();
                    if (value instanceof Number) {
                        ((Number) value).longValue();
                    } else if (!(value instanceof Boolean)) {
                        value.toString();
                    }
                }
            }
            Iterator it = r3.A09.iterator();
            if (it.hasNext()) {
                it.next();
                throw new NullPointerException("onStart");
            }
            this.A01 = r2;
        }
        if (this.A00 == null && (str = (r0 = this.A04).A06) != null) {
            this.A00 = A01(context, str, r0.A08);
        }
        this.A03.A0B(new C08400b5(this, r12, r2));
    }
}
