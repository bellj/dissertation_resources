package X;

import android.opengl.GLES20;
import java.nio.Buffer;
import java.nio.FloatBuffer;

/* renamed from: X.0ON  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0ON {
    public AnonymousClass0O8 A00;
    public final AnonymousClass0SM A01 = new AnonymousClass0SM(EnumC03740Iy.FULL_RECTANGLE);

    public AnonymousClass0ON(AnonymousClass0O8 r3) {
        this.A00 = r3;
    }

    public void A00(int i, float[] fArr) {
        AnonymousClass0O8 r3 = this.A00;
        float[] fArr2 = AnonymousClass0UM.A00;
        AnonymousClass0SM r0 = this.A01;
        FloatBuffer floatBuffer = r0.A06;
        int i2 = r0.A02;
        int i3 = r0.A00;
        int i4 = r0.A03;
        FloatBuffer floatBuffer2 = r0.A05;
        int i5 = r0.A01;
        AnonymousClass0UM.A04("draw start");
        GLES20.glUseProgram(r3.A00);
        AnonymousClass0UM.A04("glUseProgram");
        GLES20.glActiveTexture(33984);
        int i6 = r3.A01;
        GLES20.glBindTexture(i6, i);
        GLES20.glUniformMatrix4fv(r3.A06, 1, false, fArr2, 0);
        AnonymousClass0UM.A04("glUniformMatrix4fv");
        GLES20.glUniformMatrix4fv(r3.A07, 1, false, fArr, 0);
        AnonymousClass0UM.A04("glUniformMatrix4fv");
        int i7 = r3.A02;
        GLES20.glEnableVertexAttribArray(i7);
        AnonymousClass0UM.A04("glEnableVertexAttribArray");
        GLES20.glVertexAttribPointer(i7, i3, 5126, false, i4, (Buffer) floatBuffer);
        AnonymousClass0UM.A04("glVertexAttribPointer");
        int i8 = r3.A03;
        GLES20.glEnableVertexAttribArray(i8);
        AnonymousClass0UM.A04("glEnableVertexAttribArray");
        GLES20.glVertexAttribPointer(i8, 2, 5126, false, i5, (Buffer) floatBuffer2);
        AnonymousClass0UM.A04("glVertexAttribPointer");
        int i9 = r3.A05;
        if (i9 >= 0) {
            GLES20.glUniform1fv(i9, 9, r3.A09, 0);
            GLES20.glUniform2fv(r3.A08, 9, r3.A0A, 0);
            GLES20.glUniform1f(r3.A04, 0.0f);
        }
        GLES20.glDrawArrays(5, 0, i2);
        AnonymousClass0UM.A04("glDrawArrays");
        GLES20.glDisableVertexAttribArray(i7);
        GLES20.glDisableVertexAttribArray(i8);
        GLES20.glBindTexture(i6, 0);
        GLES20.glUseProgram(0);
    }
}
