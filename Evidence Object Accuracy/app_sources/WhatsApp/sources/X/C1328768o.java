package X;

import android.text.TextUtils;

/* renamed from: X.68o  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1328768o implements AnonymousClass1FK {
    public final /* synthetic */ C123505nG A00;
    public final /* synthetic */ Integer A01;
    public final /* synthetic */ Integer A02;

    public C1328768o(C123505nG r1, Integer num, Integer num2) {
        this.A00 = r1;
        this.A02 = num;
        this.A01 = num2;
    }

    @Override // X.AnonymousClass1FK
    public void AV3(C452120p r5) {
        C123505nG r3 = this.A00;
        C18600si r2 = ((AbstractC118085bF) r3).A06;
        r2.A0C(((AbstractC118085bF) r3).A04.A00());
        r2.A0A(0);
        r3.A08.A05(C12960it.A0b("accountRecovery/getPaymentTransactions/onRequestError. paymentNetworkError: ", r5));
    }

    @Override // X.AnonymousClass1FK
    public void AVA(C452120p r5) {
        C123505nG r3 = this.A00;
        C18600si r2 = ((AbstractC118085bF) r3).A06;
        r2.A0C(((AbstractC118085bF) r3).A04.A00());
        r2.A0A(0);
        r3.A08.A05(C12960it.A0b("accountRecovery/getPaymentTransactions/onResponseError. paymentNetworkError: ", r5));
    }

    @Override // X.AnonymousClass1FK
    public void AVB(C452220q r5) {
        C18600si r2;
        int i;
        boolean z = r5 instanceof AnonymousClass46P;
        C123505nG r3 = this.A00;
        if (z) {
            r3.A08.A04("accountRecovery/getTransactions/onResponseSuccess");
            AnonymousClass46P r52 = (AnonymousClass46P) r5;
            AnonymousClass20A r1 = r52.A00;
            if (r1 == null) {
                return;
            }
            if (r1.A02 || TextUtils.isEmpty(r1.A00)) {
                r2 = ((AbstractC118085bF) r3).A06;
                r2.A0C(((AbstractC118085bF) r3).A04.A00());
                i = 2;
            } else {
                r3.A07.A00(this, this.A02, this.A01, r52.A00.A00);
                return;
            }
        } else {
            r2 = ((AbstractC118085bF) r3).A06;
            r2.A0C(((AbstractC118085bF) r3).A04.A00());
            r3.A08.A04("unexpected payment transaction result type.");
            i = 0;
        }
        r2.A0A(i);
    }
}
