package X;

import java.util.List;

/* renamed from: X.0PO  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0PO {
    public int A00;
    public C006503b A01;
    public EnumC03840Ji A02;
    public String A03;
    public List A04;
    public List A05;

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001a, code lost:
        if (r1.equals(r0) == false) goto L_0x001c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r5) {
        /*
            r4 = this;
            r3 = 1
            if (r4 == r5) goto L_0x0054
            boolean r0 = r5 instanceof X.AnonymousClass0PO
            r2 = 0
            if (r0 == 0) goto L_0x001c
            X.0PO r5 = (X.AnonymousClass0PO) r5
            int r1 = r4.A00
            int r0 = r5.A00
            if (r1 != r0) goto L_0x001c
            java.lang.String r1 = r4.A03
            java.lang.String r0 = r5.A03
            if (r1 == 0) goto L_0x001d
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0020
        L_0x001c:
            return r2
        L_0x001d:
            if (r0 == 0) goto L_0x0020
            return r2
        L_0x0020:
            X.0Ji r1 = r4.A02
            X.0Ji r0 = r5.A02
            if (r1 != r0) goto L_0x001c
            X.03b r1 = r4.A01
            X.03b r0 = r5.A01
            if (r1 == 0) goto L_0x0033
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0036
            return r2
        L_0x0033:
            if (r0 == 0) goto L_0x0036
            return r2
        L_0x0036:
            java.util.List r1 = r4.A05
            java.util.List r0 = r5.A05
            if (r1 == 0) goto L_0x0043
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0046
            return r2
        L_0x0043:
            if (r0 == 0) goto L_0x0046
            return r2
        L_0x0046:
            java.util.List r1 = r4.A04
            java.util.List r0 = r5.A04
            if (r1 == 0) goto L_0x0051
            boolean r3 = r1.equals(r0)
            return r3
        L_0x0051:
            if (r0 == 0) goto L_0x0054
            r3 = 0
        L_0x0054:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0PO.equals(java.lang.Object):boolean");
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        String str = this.A03;
        int i5 = 0;
        if (str != null) {
            i = str.hashCode();
        } else {
            i = 0;
        }
        int i6 = i * 31;
        EnumC03840Ji r0 = this.A02;
        if (r0 != null) {
            i2 = r0.hashCode();
        } else {
            i2 = 0;
        }
        int i7 = (i6 + i2) * 31;
        C006503b r02 = this.A01;
        if (r02 != null) {
            i3 = r02.hashCode();
        } else {
            i3 = 0;
        }
        int i8 = (((i7 + i3) * 31) + this.A00) * 31;
        List list = this.A05;
        if (list != null) {
            i4 = list.hashCode();
        } else {
            i4 = 0;
        }
        int i9 = (i8 + i4) * 31;
        List list2 = this.A04;
        if (list2 != null) {
            i5 = list2.hashCode();
        }
        return i9 + i5;
    }
}
