package X;

import java.io.File;

/* renamed from: X.1qS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC39731qS {
    public String A00;
    public final File A01;
    public final boolean A02;
    public final byte[] A03;

    public AbstractC39731qS(File file, String str, byte[] bArr, boolean z) {
        this.A01 = file;
        this.A03 = bArr;
        this.A02 = z;
        this.A00 = str;
    }
}
