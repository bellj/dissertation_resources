package X;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import com.whatsapp.R;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.text.ReadMoreTextView;

/* renamed from: X.2cF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53032cF extends FrameLayout implements AnonymousClass004 {
    public C15550nR A00;
    public AnonymousClass01d A01;
    public C15370n3 A02;
    public AnonymousClass19M A03;
    public AnonymousClass11A A04;
    public GroupJid A05;
    public C16630pM A06;
    public C253118x A07;
    public AnonymousClass2P7 A08;
    public CharSequence A09;
    public boolean A0A;
    public final View A0B;
    public final AnonymousClass5UJ A0C;
    public final ReadMoreTextView A0D;

    public C53032cF(Context context) {
        super(context);
        if (!this.A0A) {
            this.A0A = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A07 = (C253118x) A00.AAW.get();
            this.A03 = (AnonymousClass19M) A00.A6R.get();
            this.A00 = C12960it.A0O(A00);
            this.A01 = C12960it.A0Q(A00);
            this.A04 = (AnonymousClass11A) A00.A8o.get();
            this.A06 = C12990iw.A0e(A00);
        }
        FrameLayout.inflate(getContext(), R.layout.community_description, this);
        ReadMoreTextView readMoreTextView = (ReadMoreTextView) AnonymousClass028.A0D(this, R.id.community_description_text);
        this.A0D = readMoreTextView;
        this.A0B = AnonymousClass028.A0D(this, R.id.community_home_top_divider);
        AbstractC28491Nn.A04(readMoreTextView, this.A01);
        this.A0C = new AnonymousClass5UJ() { // from class: X.57F
            @Override // X.AnonymousClass5UJ
            public final void ALi(AbstractC14640lm r3) {
                C53032cF r1 = C53032cF.this;
                if (r3 != null && r3.equals(r1.A05)) {
                    r1.A00();
                }
            }
        };
    }

    public final void A00() {
        AnonymousClass1PD r0;
        C15370n3 r02 = this.A02;
        if (r02 == null || (r0 = r02.A0G) == null || TextUtils.isEmpty(r0.A02)) {
            this.A0D.setVisibility(8);
            this.A0B.setVisibility(8);
            return;
        }
        String str = this.A02.A0G.A02;
        this.A0D.setVisibility(0);
        this.A0B.setVisibility(0);
        setDescription(str);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A08;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A08 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View, android.view.ViewGroup
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        AnonymousClass11A r0 = this.A04;
        r0.A00.add(this.A0C);
        this.A0D.requestLayout();
    }

    @Override // android.view.View, android.view.ViewGroup
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        AnonymousClass11A r0 = this.A04;
        r0.A00.remove(this.A0C);
    }

    private void setDescription(CharSequence charSequence) {
        if (!charSequence.equals(this.A09)) {
            this.A09 = charSequence;
            AnonymousClass01d r5 = this.A01;
            C16630pM r4 = this.A06;
            Context context = getContext();
            ReadMoreTextView readMoreTextView = this.A0D;
            SpannableStringBuilder A0J = C12990iw.A0J(C42971wC.A03(r5, r4, AbstractC36671kL.A03(context, readMoreTextView.getPaint(), this.A03, charSequence)));
            this.A07.A04(getContext(), A0J);
            readMoreTextView.A0G(null, A0J);
        }
    }
}
