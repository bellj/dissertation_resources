package X;

import android.graphics.Canvas;
import android.os.Build;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;

/* renamed from: X.0Z1  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Z1 implements AbstractC12380hp {
    public static final AbstractC12380hp A00 = new AnonymousClass0Z1();

    @Override // X.AbstractC12380hp
    public void A7F(View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            Object tag = view.getTag(R.id.item_touch_helper_previous_elevation);
            if (tag != null && (tag instanceof Float)) {
                AnonymousClass028.A0V(view, ((Number) tag).floatValue());
            }
            view.setTag(R.id.item_touch_helper_previous_elevation, null);
        }
        view.setTranslationX(0.0f);
        view.setTranslationY(0.0f);
    }

    @Override // X.AbstractC12380hp
    public void APW(Canvas canvas, View view, RecyclerView recyclerView, float f, float f2, int i, boolean z) {
        if (Build.VERSION.SDK_INT >= 21 && z && view.getTag(R.id.item_touch_helper_previous_elevation) == null) {
            Float valueOf = Float.valueOf(AnonymousClass028.A00(view));
            int childCount = recyclerView.getChildCount();
            float f3 = 0.0f;
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = recyclerView.getChildAt(i2);
                if (childAt != view) {
                    float A002 = AnonymousClass028.A00(childAt);
                    if (A002 > f3) {
                        f3 = A002;
                    }
                }
            }
            AnonymousClass028.A0V(view, f3 + 1.0f);
            view.setTag(R.id.item_touch_helper_previous_elevation, valueOf);
        }
        view.setTranslationX(f);
        view.setTranslationY(f2);
    }
}
