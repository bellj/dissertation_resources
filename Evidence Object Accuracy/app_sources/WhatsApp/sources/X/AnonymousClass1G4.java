package X;

/* renamed from: X.1G4  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1G4 extends AnonymousClass1G5 {
    public AbstractC27091Fz A00;
    public boolean A01 = false;
    public final AbstractC27091Fz A02;

    public AnonymousClass1G4(AbstractC27091Fz r3) {
        this.A02 = r3;
        this.A00 = (AbstractC27091Fz) r3.A0V(AnonymousClass25B.NEW_MUTABLE_INSTANCE, null, null);
    }

    public static AbstractC27091Fz A00(AnonymousClass1G4 r0) {
        r0.A03();
        return r0.A00;
    }

    public AbstractC27091Fz A01() {
        boolean z = this.A01;
        AbstractC27091Fz r0 = this.A00;
        if (z) {
            return r0;
        }
        r0.A0W();
        this.A01 = true;
        return this.A00;
    }

    public final AbstractC27091Fz A02() {
        AbstractC27091Fz A01 = A01();
        if (A01.A0Z()) {
            return A01;
        }
        throw new AnonymousClass257();
    }

    public void A03() {
        if (this.A01) {
            AbstractC27091Fz r2 = (AbstractC27091Fz) this.A00.A0V(AnonymousClass25B.NEW_MUTABLE_INSTANCE, null, null);
            r2.A0Y(C463025i.A00, this.A00);
            this.A00 = r2;
            this.A01 = false;
        }
    }

    public void A04(AbstractC27091Fz r3) {
        A03();
        this.A00.A0Y(C463025i.A00, r3);
    }

    @Override // X.AnonymousClass1G2
    public /* bridge */ /* synthetic */ AnonymousClass1G1 ACU() {
        return this.A02;
    }

    @Override // X.AnonymousClass1G5, java.lang.Object
    public /* bridge */ /* synthetic */ Object clone() {
        AnonymousClass1G4 r1 = (AnonymousClass1G4) this.A02.A0V(AnonymousClass25B.NEW_BUILDER, null, null);
        r1.A04(A01());
        return r1;
    }
}
