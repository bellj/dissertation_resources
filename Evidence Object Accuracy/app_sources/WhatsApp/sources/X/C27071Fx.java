package X;

/* renamed from: X.1Fx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C27071Fx extends AbstractC16110oT {
    public Long A00;
    public String A01;

    public C27071Fx() {
        super(2126, new AnonymousClass00E(1, 20, 200), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A01);
        r3.Abe(2, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("WamSuperpackDecompressionSuccess {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "assetName", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "decompressionT", this.A00);
        sb.append("}");
        return sb.toString();
    }
}
