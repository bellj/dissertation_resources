package X;

import android.view.Choreographer;

/* renamed from: X.0Vy  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Vy implements Choreographer.FrameCallback {
    public final /* synthetic */ AnonymousClass04G A00;

    public AnonymousClass0Vy(AnonymousClass04G r1) {
        this.A00 = r1;
    }

    @Override // android.view.Choreographer.FrameCallback
    public void doFrame(long j) {
        AnonymousClass04G r6 = this.A00;
        if (!r6.A03) {
            r6.A05.removeFrameCallback(this);
            return;
        }
        if (r6.A00 == -1) {
            r6.A00 = j;
            r6.A01 = j;
        } else {
            r6.A01 = j;
            AnonymousClass04F r7 = r6.A02.A00;
            double d = r7.A04;
            long max = Math.max(Math.round(((double) (j - r6.A01)) / d), 1L);
            long min = Math.min(max - 1, 100L);
            double d2 = (double) min;
            r7.A01 += d2;
            if (min > 4) {
                r7.A00 += d2 / 4.0d;
            }
            r7.A02 = (long) (((double) r7.A02) + (d * ((double) max)));
        }
        r6.A05.postFrameCallback(this);
    }
}
