package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import com.whatsapp.usernotice.UserNoticeBottomSheetDialogFragment;

/* renamed from: X.3fB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C72793fB extends AnimatorListenerAdapter {
    public final /* synthetic */ UserNoticeBottomSheetDialogFragment A00;
    public final /* synthetic */ boolean A01;

    public C72793fB(UserNoticeBottomSheetDialogFragment userNoticeBottomSheetDialogFragment, boolean z) {
        this.A00 = userNoticeBottomSheetDialogFragment;
        this.A01 = z;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        this.A00.A03.setVisibility(C12960it.A02(this.A01 ? 1 : 0));
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        this.A00.A03.setVisibility(0);
    }
}
