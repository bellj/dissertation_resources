package X;

/* renamed from: X.0mF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14910mF {
    public int A00 = 3;

    public String toString() {
        int i = this.A00;
        if (i == 1) {
            return "available";
        }
        return i == 3 ? "un-available" : "available-waiting-timeout";
    }
}
