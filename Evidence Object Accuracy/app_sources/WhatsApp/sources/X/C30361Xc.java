package X;

import com.whatsapp.util.Log;

/* renamed from: X.1Xc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30361Xc extends AbstractC15340mz {
    public int A00;
    public int A01;

    public C30361Xc(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 12, j);
    }

    public C30361Xc(AnonymousClass1IS r3, byte[] bArr, int i, long j) {
        this(r3, j);
        StringBuilder sb = new StringBuilder("FMessageFuture/futureproof/size=");
        sb.append(Integer.valueOf(bArr.length));
        Log.w(sb.toString());
        A0w(bArr);
        this.A01 = i;
    }
}
