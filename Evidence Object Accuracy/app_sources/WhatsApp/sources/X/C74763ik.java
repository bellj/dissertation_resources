package X;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;

/* renamed from: X.3ik  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74763ik extends AnonymousClass0FE {
    public final /* synthetic */ AbstractC64423Fm A00;

    @Override // X.AnonymousClass0FE
    public int A05() {
        return -1;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C74763ik(Context context, AbstractC64423Fm r2) {
        super(context);
        this.A00 = r2;
    }

    @Override // X.AnonymousClass0FE
    public float A04(DisplayMetrics displayMetrics) {
        return 40.0f / TypedValue.applyDimension(1, 40.0f, displayMetrics);
    }
}
