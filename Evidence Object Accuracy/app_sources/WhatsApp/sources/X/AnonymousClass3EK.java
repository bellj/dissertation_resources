package X;

import com.facebook.redex.IDxNFunctionShape17S0100000_2_I1;
import java.util.List;

/* renamed from: X.3EK  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3EK {
    public final AnonymousClass1V8 A00;
    public final String A01;
    public final List A02;

    public AnonymousClass3EK(AbstractC15710nm r12, AnonymousClass1V8 r13) {
        AnonymousClass1V8.A01(r13, "states");
        this.A01 = AnonymousClass3JT.A07(r13, "start_at", new String[1]);
        this.A02 = AnonymousClass3JT.A0B(r13, new IDxNFunctionShape17S0100000_2_I1(r12, 22), new String[]{"state"}, 1, Long.MAX_VALUE);
        this.A00 = r13;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || AnonymousClass3EK.class != obj.getClass()) {
                return false;
            }
            AnonymousClass3EK r5 = (AnonymousClass3EK) obj;
            if (!this.A01.equals(r5.A01) || !this.A02.equals(r5.A02)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] A1a = C12980iv.A1a();
        A1a[0] = this.A01;
        return C12960it.A06(this.A02, A1a);
    }
}
