package X;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.atomic.AtomicLong;
import java.util.zip.InflaterInputStream;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/* renamed from: X.15D  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass15D {
    public Cipher A00;
    public Cipher A01;
    public Cipher A02;
    public Cipher A03;
    public Cipher A04;
    public Cipher A05;
    public boolean A06;

    public static final InputStream A00(InputStream inputStream, AtomicLong atomicLong, Cipher cipher, byte[] bArr, byte[] bArr2) {
        cipher.init(2, new SecretKeySpec(bArr, "AES"), new IvParameterSpec(bArr2));
        return new InflaterInputStream(new AnonymousClass5IU(inputStream, atomicLong, cipher));
    }

    public static final ZipInputStream A01(InputStream inputStream, AtomicLong atomicLong, Cipher cipher, byte[] bArr, byte[] bArr2) {
        cipher.init(2, new SecretKeySpec(bArr, "AES"), new IvParameterSpec(bArr2));
        return new ZipInputStream(new AnonymousClass5IU(inputStream, atomicLong, cipher));
    }

    public static final CipherOutputStream A02(OutputStream outputStream, Cipher cipher, byte[] bArr, byte[] bArr2) {
        cipher.init(1, new SecretKeySpec(bArr, "AES"), new IvParameterSpec(bArr2));
        return new CipherOutputStream(outputStream, cipher);
    }

    public synchronized ZipInputStream A03(EnumC16570pG r3, InputStream inputStream, AtomicLong atomicLong, byte[] bArr, byte[] bArr2) {
        ZipInputStream zipInputStream;
        int i = AnonymousClass4GU.A00[r3.ordinal()];
        if (i != 1) {
            if (i != 2) {
                if (i != 3) {
                    if (i != 4) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("unsupported key selector ");
                        sb.append(r3);
                        throw new IllegalArgumentException(sb.toString());
                    }
                    zipInputStream = new ZipInputStream(inputStream);
                } else if (!(bArr == null || bArr2 == null)) {
                    A05();
                    zipInputStream = A01(inputStream, atomicLong, this.A02, bArr, bArr2);
                }
            }
            if (!(bArr == null || bArr2 == null)) {
                A05();
                zipInputStream = A01(inputStream, atomicLong, this.A01, bArr, bArr2);
            }
        }
        if (!(bArr == null || bArr2 == null)) {
            A05();
            zipInputStream = A01(inputStream, atomicLong, this.A00, bArr, bArr2);
        }
        zipInputStream = new ZipInputStream(inputStream);
        return zipInputStream;
    }

    public synchronized ZipOutputStream A04(EnumC16570pG r3, OutputStream outputStream, byte[] bArr, byte[] bArr2) {
        ZipOutputStream zipOutputStream;
        int i = AnonymousClass4GU.A00[r3.ordinal()];
        if (i == 1) {
            A05();
            zipOutputStream = new ZipOutputStream(A02(outputStream, this.A03, bArr, bArr2));
        } else if (i == 2) {
            A05();
            zipOutputStream = new ZipOutputStream(A02(outputStream, this.A04, bArr, bArr2));
        } else if (i == 3) {
            A05();
            zipOutputStream = new ZipOutputStream(A02(outputStream, this.A05, bArr, bArr2));
        } else if (i == 4) {
            zipOutputStream = new ZipOutputStream(outputStream);
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("unsupported key selector ");
            sb.append(r3);
            throw new IllegalArgumentException(sb.toString());
        }
        return zipOutputStream;
    }

    public final synchronized void A05() {
        if (!this.A06) {
            this.A03 = Cipher.getInstance("AES/GCM/NoPadding");
            this.A00 = Cipher.getInstance("AES/GCM/NoPadding");
            this.A04 = Cipher.getInstance("AES/GCM/NoPadding");
            this.A01 = Cipher.getInstance("AES/GCM/NoPadding");
            this.A05 = Cipher.getInstance("AES/GCM/NoPadding");
            this.A02 = Cipher.getInstance("AES/GCM/NoPadding");
            this.A06 = true;
        }
    }
}
