package X;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.logging.Level;

/* renamed from: X.3cN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C71093cN implements Closeable {
    public C13600jz A00;
    public final URL A01;
    public volatile InputStream A02;

    public C71093cN(URL url) {
        this.A01 = url;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public final void close() {
        InputStream inputStream = this.A02;
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e) {
                try {
                    AnonymousClass4Z2.A00.logp(Level.WARNING, "com.google.common.io.Closeables", "close", "IOException thrown while closing Closeable.", (Throwable) e);
                } catch (IOException e2) {
                    throw new AssertionError(e2);
                }
            }
        }
    }
}
