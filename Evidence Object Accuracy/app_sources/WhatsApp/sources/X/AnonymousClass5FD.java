package X;

import java.io.IOException;

/* renamed from: X.5FD  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5FD implements AnonymousClass5VN {
    @Override // X.AnonymousClass5VN
    public void AgH(Appendable appendable, Object obj, C94884ch r5) {
        try {
            AbstractC95364da.A00(obj.getClass());
            throw C12990iw.A0m("Redex: Unreachable code after no-return invoke");
        } catch (IOException e) {
            throw e;
        }
    }
}
