package X;

import android.view.View;
import android.view.ViewTreeObserver;
import com.whatsapp.R;
import com.whatsapp.inappsupport.ui.FaqItemActivityV2;

/* renamed from: X.3Np  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ViewTreeObserver$OnPreDrawListenerC66443Np implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ View A00;
    public final /* synthetic */ FaqItemActivityV2 A01;

    public ViewTreeObserver$OnPreDrawListenerC66443Np(View view, FaqItemActivityV2 faqItemActivityV2) {
        this.A01 = faqItemActivityV2;
        this.A00 = view;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        View view = this.A00;
        int height = view.getHeight();
        View findViewById = this.A01.findViewById(R.id.faq_screen_content);
        findViewById.setPadding(findViewById.getPaddingLeft(), findViewById.getPaddingTop(), findViewById.getPaddingRight(), findViewById.getPaddingBottom() + height);
        C12980iv.A1G(view, this);
        return false;
    }
}
