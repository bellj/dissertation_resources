package X;

import android.content.Context;
import com.whatsapp.util.Log;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/* renamed from: X.5gN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120555gN extends C126705tJ {
    public static final Long A06 = 2L;
    public final Context A00;
    public final C14900mE A01;
    public final C14850m9 A02;
    public final C17220qS A03;
    public final C18650sn A04;
    public final C18590sh A05;

    public C120555gN(Context context, C14900mE r3, C14850m9 r4, C17220qS r5, C1308460e r6, C18650sn r7, C18610sj r8, C18590sh r9) {
        super(r6.A04, r8);
        this.A00 = context;
        this.A02 = r4;
        this.A01 = r3;
        this.A03 = r5;
        this.A05 = r9;
        this.A04 = r7;
    }

    public static C128675wU A00(AnonymousClass20C r4) {
        return new C128675wU(String.valueOf(r4.A01()), String.valueOf(r4.A00), ((AbstractC30781Yu) r4.A01).A04);
    }

    public void A01(C30821Yy r19, AnonymousClass1ZR r20, AbstractC136226Ls r21, C120475gF r22, String str, String str2, String str3, String str4, String str5, String str6, String str7, HashMap hashMap) {
        String str8;
        AnonymousClass1W9[] A0L;
        Log.i("PAY: acceptCollect called");
        C17220qS r2 = this.A03;
        String A01 = r2.A01();
        Long l = null;
        if (hashMap != null) {
            str8 = C1308460e.A00("MPIN", hashMap);
        } else {
            str8 = null;
        }
        AnonymousClass009.A05(str8);
        if (this.A02.A07(747)) {
            l = A06;
        }
        C126395so r4 = new C126395so(new AnonymousClass3CT(A01), new C126405sp(A00(super.A01.A00(C30771Yt.A05, r19))), l, str8, this.A05.A01(), str6, str, str2, str3, (String) C117295Zj.A0R(r20), str4, str5, str7);
        C64513Fv r6 = super.A00;
        if (r6 != null) {
            r6.A04("upi-accept-collect");
        }
        List A0l = C12960it.A0l();
        AnonymousClass1V8 r42 = r4.A00;
        AnonymousClass1V8 A0D = r42.A0D(0);
        if (!(A0D == null || (A0L = A0D.A0L()) == null)) {
            AnonymousClass009.A05(A0L);
            A0l = Arrays.asList(A0L);
        }
        r22.A00("U66", A0l);
        C117325Zm.A05(r2, new C120665gY(this.A00, this.A01, this.A04, r6, r21, this), r42, A01);
    }
}
