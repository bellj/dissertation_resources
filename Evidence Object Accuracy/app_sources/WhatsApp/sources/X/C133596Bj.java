package X;

import java.util.Map;

/* renamed from: X.6Bj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C133596Bj implements AbstractC16960q2 {
    @Override // X.AbstractC16960q2
    public Class A9u() {
        return EnumC124595pn.class;
    }

    @Override // X.AbstractC16960q2
    public Object Aam(Enum r3, Object obj, Map map) {
        C129785yI r4 = (C129785yI) obj;
        EnumC124595pn r32 = (EnumC124595pn) r3;
        if (C117315Zl.A01(r32, C125215qt.A00) == C16700pc.A0N(r4, r32)) {
            return r4.A00;
        }
        throw new C113285Gx();
    }
}
