package X;

/* renamed from: X.4UO  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass4UO {
    public static int A04(int i) {
        return AbstractC79223qF.A00(i << 3);
    }

    public static int A05(int i, int i2) {
        return i2 + AbstractC79223qF.A00(i) + i;
    }

    public static void A06(AbstractC79223qF r1, int i, int i2) {
        r1.A05((i << 3) | i2);
    }
}
