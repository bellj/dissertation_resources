package X;

/* renamed from: X.3W4  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3W4 implements AbstractC115915Tk {
    public final /* synthetic */ C44341yl A00;

    public AnonymousClass3W4(C44341yl r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC115915Tk
    public AnonymousClass2K2 A7v(AbstractC115895Ti r5) {
        C44341yl r0 = this.A00;
        AnonymousClass01J r1 = r0.A03;
        AnonymousClass018 A0R = C12960it.A0R(r1);
        return new AnonymousClass2K2(C12970iu.A0V(r1), r5, (AbstractC115905Tj) r0.A02.A0d.get(), A0R);
    }
}
