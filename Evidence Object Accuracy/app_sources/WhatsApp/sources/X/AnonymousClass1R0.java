package X;

/* renamed from: X.1R0  reason: invalid class name */
/* loaded from: classes2.dex */
public enum AnonymousClass1R0 {
    INCORRECT,
    BLOCKED,
    LENGTH_LONG,
    LENGTH_SHORT,
    FORMAT_WRONG,
    TEMPORARILY_UNAVAILABLE,
    OLD_VERSION,
    ERROR_BAD_TOKEN,
    INVALID_SKEY_SIGNATURE,
    SECURITY_CODE,
    LIMITED_RELEASE,
    BIZ_NOT_ALLOWED,
    DEVICE_CONFIRM_OR_SECOND_CODE,
    SECOND_CODE
}
