package X;

import java.util.concurrent.CancellationException;

/* renamed from: X.5LF  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass5LF extends C94524by implements AnonymousClass1J7 {
    public void A0A(Throwable th) {
        AnonymousClass1J7 r0;
        if (this instanceof AnonymousClass5L0) {
            r0 = ((AnonymousClass5L0) this).A00;
        } else if (this instanceof C114285Kx) {
            C114285Kx r3 = (C114285Kx) this;
            if (C114285Kx.A01.compareAndSet(r3, 0, 1)) {
                r0 = r3.A00;
            } else {
                return;
            }
        } else if (!(this instanceof C114295Ky)) {
            C114275Kw r02 = (C114275Kw) this;
            C114215Kq r5 = r02.A00;
            C10710f4 r03 = ((AbstractC114145Kj) r02).A00;
            if (r03 != null) {
                CancellationException ABF = r03.ABF();
                if (((AnonymousClass5LK) r5).A00 == 2) {
                    C114205Kp r32 = (C114205Kp) r5.A01;
                    if (r32._reusableCancellableContinuation != null) {
                        while (true) {
                            Object obj = r32._reusableCancellableContinuation;
                            AnonymousClass4VA r1 = AnonymousClass4HG.A00;
                            if (C16700pc.A0O(obj, r1)) {
                                if (AnonymousClass0KN.A00(r32, r1, ABF, C114205Kp.A04)) {
                                    return;
                                }
                            } else if (obj instanceof Throwable) {
                                return;
                            } else {
                                if (AnonymousClass0KN.A00(r32, obj, null, C114205Kp.A04)) {
                                    break;
                                }
                            }
                        }
                    }
                }
                r5.A0A(ABF);
                r5.A05();
                return;
            }
            throw C16700pc.A06("job");
        } else {
            C114295Ky r04 = (C114295Ky) this;
            AbstractC02780Dy r12 = r04.A00;
            C10710f4 r05 = ((AbstractC114145Kj) r04).A00;
            if (r05 != null) {
                r12.AYr(r05);
                return;
            }
            throw C16700pc.A06("job");
        }
        r0.AJ4(th);
    }
}
