package X;

import com.whatsapp.R;

/* renamed from: X.5fi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120155fi extends C69973aX {
    public C120155fi(C16590pI r1, C22590zK r2, AbstractC14440lR r3) {
        super(r1, r2, r3);
    }

    @Override // X.C69973aX
    public int A00() {
        return (int) C16590pI.A00(this.A01).getDimension(R.dimen.novi_pay_bubble_icon_height);
    }

    @Override // X.C69973aX, X.AnonymousClass5Wu
    public int ADq() {
        return R.layout.novi_conversation_invite_image_view;
    }
}
