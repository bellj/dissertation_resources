package X;

/* renamed from: X.2Nx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C50062Nx {
    public final String A00;
    public final String A01;
    public final String A02;
    public final String A03;
    public final String A04;
    public final String A05 = "image/png";
    public final String A06;
    public final String A07;

    public C50062Nx(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        AnonymousClass009.A05(str);
        AnonymousClass009.A05(str2);
        AnonymousClass009.A05("image/png");
        this.A03 = str;
        this.A06 = str2;
        this.A07 = str3;
        this.A02 = str4;
        this.A04 = str5;
        this.A01 = str6;
        this.A00 = str7;
    }
}
