package X;

/* renamed from: X.42T  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass42T extends AnonymousClass4WS {
    public final String A00;

    public AnonymousClass42T(String str) {
        super(1);
        this.A00 = str;
    }

    @Override // X.AnonymousClass4WS
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
            return false;
        }
        return this.A00.equals(((AnonymousClass42T) obj).A00);
    }

    @Override // X.AnonymousClass4WS
    public int hashCode() {
        Object[] A1a = C12980iv.A1a();
        C12960it.A1O(A1a, super.hashCode());
        return C12960it.A06(this.A00, A1a);
    }
}
