package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.49x  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class EnumC870249x extends Enum {
    public static final /* synthetic */ EnumC870249x[] A00;
    public static final EnumC870249x A01;
    public static final EnumC870249x A02;

    public static EnumC870249x valueOf(String str) {
        return (EnumC870249x) Enum.valueOf(EnumC870249x.class, str);
    }

    public static EnumC870249x[] values() {
        return (EnumC870249x[]) A00.clone();
    }

    static {
        EnumC870249x r3 = new EnumC870249x("SELECTED", 0);
        A02 = r3;
        EnumC870249x r1 = new EnumC870249x("IDLE", 1);
        A01 = r1;
        EnumC870249x[] r0 = new EnumC870249x[2];
        C72453ed.A1J(r3, r1, r0);
        A00 = r0;
    }

    public EnumC870249x(String str, int i) {
    }
}
