package X;

import android.util.Log;
import java.lang.Thread;
import java.util.concurrent.FutureTask;

/* renamed from: X.3dU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C71773dU extends FutureTask {
    public final /* synthetic */ C14180kz A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C71773dU(C14180kz r1, Object obj, Runnable runnable) {
        super(runnable, obj);
        this.A00 = r1;
    }

    @Override // java.util.concurrent.FutureTask
    public final void setException(Throwable th) {
        Thread.UncaughtExceptionHandler uncaughtExceptionHandler = this.A00.A00.A00;
        if (uncaughtExceptionHandler != null) {
            uncaughtExceptionHandler.uncaughtException(Thread.currentThread(), th);
        } else if (Log.isLoggable("GAv4", 6)) {
            String valueOf = String.valueOf(th);
            StringBuilder A0t = C12980iv.A0t(valueOf.length() + 37);
            A0t.append("MeasurementExecutor: job failed with ");
            Log.e("GAv4", C12960it.A0d(valueOf, A0t));
        }
        super.setException(th);
    }
}
