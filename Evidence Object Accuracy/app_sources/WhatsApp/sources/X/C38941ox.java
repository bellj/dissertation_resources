package X;

import android.media.MediaMetadataRetriever;
import java.io.Closeable;

/* renamed from: X.1ox  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38941ox extends MediaMetadataRetriever implements Closeable {
    @Override // android.media.MediaMetadataRetriever, java.lang.AutoCloseable, java.io.Closeable
    public void close() {
        release();
    }
}
