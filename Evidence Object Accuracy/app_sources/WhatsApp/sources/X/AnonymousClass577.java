package X;

/* renamed from: X.577  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass577 implements AbstractC38871oq {
    public float A00;
    public AbstractC470728v A01;

    public AnonymousClass577(AbstractC470728v r2, Float f) {
        this.A01 = r2;
        this.A00 = f.floatValue();
    }

    @Override // X.AbstractC38871oq
    public /* bridge */ /* synthetic */ boolean A7R(Object obj) {
        return this.A01.AH5().equals(((AbstractC470728v) obj).AH5());
    }

    @Override // X.AbstractC38871oq
    public /* bridge */ /* synthetic */ Object ADD() {
        return this.A01;
    }

    @Override // X.AbstractC38871oq
    public float AHi() {
        return this.A00;
    }

    @Override // X.AbstractC38871oq
    public void AdB(float f) {
        this.A00 = f;
    }
}
