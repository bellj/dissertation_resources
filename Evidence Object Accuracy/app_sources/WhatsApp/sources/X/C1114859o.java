package X;

import com.facebook.redex.RunnableBRunnable0Shape1S0110000_I1;
import com.whatsapp.settings.SettingsChatHistoryFragment;

/* renamed from: X.59o  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1114859o implements AnonymousClass5WL {
    public final /* synthetic */ SettingsChatHistoryFragment A00;

    @Override // X.AnonymousClass5WL
    public void ASv() {
    }

    public C1114859o(SettingsChatHistoryFragment settingsChatHistoryFragment) {
        this.A00 = settingsChatHistoryFragment;
    }

    @Override // X.AnonymousClass5WL
    public void ATy(boolean z) {
        SettingsChatHistoryFragment settingsChatHistoryFragment = this.A00;
        settingsChatHistoryFragment.A1B();
        settingsChatHistoryFragment.A0B.Ab2(new RunnableBRunnable0Shape1S0110000_I1(this, 9, z));
    }
}
