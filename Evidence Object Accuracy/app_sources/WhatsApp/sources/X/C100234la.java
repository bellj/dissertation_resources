package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4la  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C100234la implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new C16470p4(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C16470p4[i];
    }
}
