package X;

import android.graphics.Rect;
import android.view.View;

/* renamed from: X.0Qn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05680Qn {
    public static Rect A00(View view) {
        return view.getClipBounds();
    }

    public static void A01(View view, Rect rect) {
        view.setClipBounds(rect);
    }
}
