package X;

/* renamed from: X.3Vn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68473Vn implements AnonymousClass2K1 {
    public final C48122Ek A00;
    public final /* synthetic */ AnonymousClass2K0 A01;

    public C68473Vn(C48122Ek r1, AnonymousClass2K0 r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass2K1
    public void APl(int i) {
        AbstractC116565Vy r0 = this.A01.A05;
        if (r0 != null) {
            r0.ANR(i);
        }
    }

    @Override // X.AnonymousClass2K1
    public /* bridge */ /* synthetic */ void AX4(Object obj) {
        AnonymousClass4T8 r5 = (AnonymousClass4T8) obj;
        AnonymousClass2K0 r3 = this.A01;
        if (r3.A05 != null) {
            AnonymousClass3AQ.A00(this.A00, r5.A00, r5.A03);
            r3.A05.ANS(r5);
        }
    }
}
