package X;

import android.net.Uri;
import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.net.ssl.HttpsURLConnection;

/* renamed from: X.0xu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21740xu {
    public static URL A0B;
    public static URL A0C;
    public static URL A0D;
    public int A00;
    public boolean A01;
    public final C18790t3 A02;
    public final C18640sm A03;
    public final C15810nw A04;
    public final C14830m7 A05;
    public final C16590pI A06;
    public final C14820m6 A07;
    public final C18800t4 A08;
    public final C19930uu A09;
    public final AbstractC14440lR A0A;

    public C21740xu(C18790t3 r1, C18640sm r2, C15810nw r3, C14830m7 r4, C16590pI r5, C14820m6 r6, C18800t4 r7, C19930uu r8, AbstractC14440lR r9) {
        this.A06 = r5;
        this.A05 = r4;
        this.A09 = r8;
        this.A0A = r9;
        this.A02 = r1;
        this.A04 = r3;
        this.A08 = r7;
        this.A07 = r6;
        this.A03 = r2;
    }

    public static byte[] A00(File file) {
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-256");
            try {
                FileInputStream fileInputStream = new FileInputStream(file);
                byte[] bArr = new byte[DefaultCrypto.BUFFER_SIZE];
                while (true) {
                    int read = fileInputStream.read(bArr);
                    if (read >= 0) {
                        instance.update(bArr, 0, read);
                    } else {
                        byte[] digest = instance.digest();
                        fileInputStream.close();
                        return digest;
                    }
                }
            } catch (IOException e) {
                Log.w(e);
                return null;
            }
        } catch (NoSuchAlgorithmException e2) {
            throw new AssertionError(e2);
        }
    }

    public Uri A01() {
        URL url = A0B;
        return Uri.parse(url != null ? url.toString() : "https://www.whatsapp.com/android/current/WhatsApp.apk");
    }

    public final String A02(URL url) {
        try {
            HttpsURLConnection A03 = A03(url);
            if (A03 == null) {
                return null;
            }
            StringBuilder sb = new StringBuilder();
            char[] cArr = new char[4096];
            InputStreamReader inputStreamReader = new InputStreamReader(new AnonymousClass1oJ(this.A02, A03.getInputStream(), null, 18), AnonymousClass01V.A08);
            while (true) {
                int read = inputStreamReader.read(cArr);
                if (read >= 0) {
                    sb.append(cArr, 0, read);
                } else {
                    inputStreamReader.close();
                    return sb.toString().trim();
                }
            }
        } catch (IOException e) {
            StringBuilder sb2 = new StringBuilder("IO exception during upgrade url fetch; url=");
            sb2.append(url);
            Log.w(sb2.toString(), e);
            return null;
        }
    }

    public final HttpsURLConnection A03(URL url) {
        HttpsURLConnection httpsURLConnection;
        URLConnection openConnection = url.openConnection();
        if (!(openConnection instanceof HttpsURLConnection) || (httpsURLConnection = (HttpsURLConnection) openConnection) == null) {
            StringBuilder sb = new StringBuilder("non https url provided to upgrade url fetch; url=");
            sb.append(url);
            Log.w(sb.toString());
            return null;
        }
        httpsURLConnection.setSSLSocketFactory(this.A08.A02());
        httpsURLConnection.setConnectTimeout(15000);
        httpsURLConnection.setReadTimeout(30000);
        httpsURLConnection.setRequestProperty("User-Agent", this.A09.A00());
        httpsURLConnection.setRequestProperty("Accept-Charset", AnonymousClass01V.A08);
        int responseCode = httpsURLConnection.getResponseCode();
        if (responseCode == 200) {
            return httpsURLConnection;
        }
        StringBuilder sb2 = new StringBuilder("unexpected response code during upgrade url fetch; url=");
        sb2.append(url);
        sb2.append("; responseCode=");
        sb2.append(responseCode);
        Log.w(sb2.toString());
        httpsURLConnection.disconnect();
        return null;
    }

    public void A04() {
        try {
            boolean createNewFile = this.A04.A07("WhatsApp.upgrade").createNewFile();
            StringBuilder sb = new StringBuilder();
            sb.append("upgrade sentinel file created; success=");
            sb.append(createNewFile);
            Log.i(sb.toString());
        } catch (IOException e) {
            Log.e("upgrade/sentinel/fail", e);
        }
    }

    public void A05() {
        C15810nw r2 = this.A04;
        File A07 = r2.A07("WhatsApp.download");
        if (A07.exists()) {
            Log.a(A07.delete());
        }
        if (!r2.A07("WhatsApp.upgrade").exists()) {
            File A072 = r2.A07("WhatsApp.apk");
            if (A072.exists()) {
                Log.a(A072.delete());
            }
            this.A07.A00.edit().remove("last_upgrade_remote_sha256").apply();
        }
    }
}
