package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.4iR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98284iR implements Handler.Callback {
    public boolean A00 = false;
    public final Handler A01;
    public final AbstractC115565Sb A02;
    public final Object A03 = C12970iu.A0l();
    public final ArrayList A04 = C12960it.A0l();
    public final ArrayList A05 = C12960it.A0l();
    public final ArrayList A06 = C12960it.A0l();
    public final AtomicInteger A07 = new AtomicInteger(0);
    public volatile boolean A08 = false;

    public C98284iR(Looper looper, AbstractC115565Sb r4) {
        this.A02 = r4;
        this.A01 = new HandlerC472529t(looper, this);
    }

    @Override // android.os.Handler.Callback
    public final boolean handleMessage(Message message) {
        int i = message.what;
        if (i == 1) {
            AbstractC14990mN r2 = (AbstractC14990mN) message.obj;
            synchronized (this.A03) {
                if (this.A08 && this.A02.isConnected() && this.A05.contains(r2)) {
                    r2.onConnected(null);
                }
            }
            return true;
        }
        StringBuilder A0t = C12980iv.A0t(45);
        A0t.append("Don't know how to handle message: ");
        A0t.append(i);
        Log.wtf("GmsClientEvents", A0t.toString(), new Exception());
        return false;
    }
}
