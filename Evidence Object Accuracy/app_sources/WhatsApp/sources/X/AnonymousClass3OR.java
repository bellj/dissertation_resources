package X;

import android.widget.SeekBar;
import com.whatsapp.R;
import com.whatsapp.mediaview.MediaViewFragment;
import com.whatsapp.util.Log;
import java.io.IOException;

/* renamed from: X.3OR  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3OR implements SeekBar.OnSeekBarChangeListener {
    public final /* synthetic */ MediaViewFragment A00;

    public /* synthetic */ AnonymousClass3OR(MediaViewFragment mediaViewFragment) {
        this.A00 = mediaViewFragment;
    }

    @Override // android.widget.SeekBar.OnSeekBarChangeListener
    public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
        if (seekBar != null) {
            MediaViewFragment mediaViewFragment = this.A00;
            mediaViewFragment.A1V.setContentDescription(C12970iu.A0q(mediaViewFragment, C38131nZ.A06(mediaViewFragment.A0j, (long) seekBar.getProgress()), C12970iu.A1b(), 0, R.string.voice_message_time_elapsed));
        }
    }

    @Override // android.widget.SeekBar.OnSeekBarChangeListener
    public void onStartTrackingTouch(SeekBar seekBar) {
        MediaViewFragment mediaViewFragment = this.A00;
        AbstractC28651Ol r0 = mediaViewFragment.A1P;
        if (r0 != null && r0.A0D()) {
            mediaViewFragment.A1P.A04();
        }
        mediaViewFragment.A06.removeMessages(0);
    }

    @Override // android.widget.SeekBar.OnSeekBarChangeListener
    public void onStopTrackingTouch(SeekBar seekBar) {
        MediaViewFragment mediaViewFragment = this.A00;
        AbstractC28651Ol r5 = mediaViewFragment.A1P;
        if (r5 == null) {
            mediaViewFragment.A1V.setProgress(0);
        } else if (mediaViewFragment.A01 == 1) {
            try {
                r5.A0A((int) (((float) r5.A03()) * (((float) mediaViewFragment.A1V.getProgress()) / ((float) mediaViewFragment.A1V.getMax()))));
                mediaViewFragment.A1P.A08();
                mediaViewFragment.A06.sendEmptyMessage(0);
                mediaViewFragment.A1S();
            } catch (IOException e) {
                Log.e("mediaview/fail onStopTracking", e);
                ((ActivityC13810kN) mediaViewFragment.A0C()).Ado(R.string.gallery_audio_cannot_load);
            }
        } else {
            int A03 = (int) (((float) r5.A03()) * (((float) mediaViewFragment.A1V.getProgress()) / ((float) mediaViewFragment.A1V.getMax())));
            AbstractC16130oV A1Q = mediaViewFragment.A1Q(mediaViewFragment.A03);
            if (A1Q != null) {
                mediaViewFragment.A1X(A1Q, A03, false);
            }
        }
    }
}
