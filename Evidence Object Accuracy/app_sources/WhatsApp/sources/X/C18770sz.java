package X;

import android.content.SharedPreferences;
import android.os.SystemClock;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0sz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18770sz {
    public final AbstractC15710nm A00;
    public final C15570nT A01;
    public final C14830m7 A02;
    public final C14820m6 A03;
    public final C237312u A04;
    public final C245916c A05;
    public final C14840m8 A06;

    public C18770sz(AbstractC15710nm r1, C15570nT r2, C14830m7 r3, C14820m6 r4, C237312u r5, C245916c r6, C14840m8 r7) {
        this.A02 = r3;
        this.A00 = r1;
        this.A01 = r2;
        this.A05 = r6;
        this.A06 = r7;
        this.A03 = r4;
        this.A04 = r5;
    }

    public static long A00(AnonymousClass1Mg r3, C18770sz r4, UserJid userJid) {
        String str = "";
        if (userJid != null) {
            HashSet hashSet = new HashSet(r4.A0E(userJid));
            if (!hashSet.isEmpty()) {
                str = AnonymousClass1YK.A00(hashSet);
            }
        }
        r3.A06 = str;
        r3.A02 = r4.A04(userJid);
        AnonymousClass1YF A09 = r4.A09(userJid);
        if (A09 != null) {
            return A09.A01;
        }
        return 0;
    }

    public static AnonymousClass1JO A01(C28601Of r5, C28601Of r6) {
        AnonymousClass1YJ r4 = new AnonymousClass1YJ();
        Iterator it = r5.A02().iterator();
        while (it.hasNext()) {
            Object next = it.next();
            Map map = r6.A00;
            if (!map.containsKey(next) || map.get(next) != r5.A00.get(next)) {
                r4.A02(next);
            }
        }
        return r4.A00();
    }

    public static AnonymousClass1JO A02(C28601Of r6, C28601Of r7) {
        AnonymousClass1YJ r5 = new AnonymousClass1YJ();
        Iterator it = r7.A02().iterator();
        while (it.hasNext()) {
            Object next = it.next();
            Map map = r6.A00;
            if (!map.containsKey(next) || map.get(next) != r7.A00.get(next)) {
                map.get(next);
                r7.A00.get(next);
                r5.A02(next);
            }
        }
        return r5.A00();
    }

    public long A03() {
        C14830m7 r3 = this.A02;
        if (r3.A01 != 0) {
            long elapsedRealtime = r3.A01 + SystemClock.elapsedRealtime();
            if (elapsedRealtime != 0) {
                return elapsedRealtime;
            }
        }
        return System.currentTimeMillis();
    }

    public long A04(UserJid userJid) {
        AnonymousClass1YF A09 = A09(userJid);
        if (A09 == null) {
            return 0;
        }
        return A09.A04;
    }

    public C28601Of A05() {
        C28601Of A00 = this.A05.A04.A00();
        AnonymousClass1YB r4 = new AnonymousClass1YB();
        Iterator it = A00.A01().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            r4.A01(entry.getKey(), Long.valueOf((long) ((AnonymousClass1JU) entry.getValue()).A03));
        }
        return r4.A00();
    }

    public AnonymousClass1JO A06() {
        C15570nT r0 = this.A01;
        r0.A08();
        if (r0.A05 == null) {
            return AnonymousClass1JO.A01;
        }
        return this.A05.A00().A02();
    }

    public AnonymousClass1YF A07() {
        SharedPreferences sharedPreferences = this.A03.A00;
        return new AnonymousClass1YF(sharedPreferences.getInt("adv_raw_id", -1), sharedPreferences.getLong("adv_timestamp_sec", -1), sharedPreferences.getLong("adv_expected_timestamp_sec_in_companion_mode", 0), sharedPreferences.getLong("adv_expected_ts_last_device_job_ts_in_companion_mode", 0), sharedPreferences.getLong("adv_expected_ts_update_ts_in_companion_mode", 0));
    }

    public AnonymousClass1YF A08(AnonymousClass1YF r15, long j) {
        long j2 = r15.A04;
        if (j2 < j) {
            long j3 = r15.A01;
            if (j3 < j) {
                long j4 = this.A03.A00.getLong("adv_last_device_job_ts", 0);
                long j5 = r15.A02;
                if (j2 >= j3) {
                    j5 = A03();
                }
                return new AnonymousClass1YF(r15.A00, j2, j, j4, j5);
            }
        }
        return r15;
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0067 A[Catch: all -> 0x007c, TRY_ENTER, TRY_LEAVE, TryCatch #1 {, blocks: (B:10:0x0025, B:12:0x002d, B:13:0x0033, B:15:0x0035, B:26:0x006a, B:16:0x004b, B:25:0x0067, B:31:0x007b), top: B:36:0x0025 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass1YF A09(com.whatsapp.jid.UserJid r8) {
        /*
            r7 = this;
            if (r8 != 0) goto L_0x0004
            r0 = 0
            return r0
        L_0x0004:
            X.0nT r0 = r7.A01
            boolean r0 = r0.A0F(r8)
            if (r0 == 0) goto L_0x0011
            X.1YF r0 = r7.A07()
            return r0
        L_0x0011:
            X.16c r2 = r7.A05
            X.0nT r0 = r2.A01
            boolean r0 = r0.A0F(r8)
            r1 = r0 ^ 1
            java.lang.String r0 = "only query info for others"
            X.AnonymousClass009.A0C(r0, r1)
            X.16e r5 = r2.A03
            java.lang.Object r3 = r5.A03
            monitor-enter(r3)
            java.util.Map r4 = r5.A04     // Catch: all -> 0x0081
            boolean r0 = r4.containsKey(r8)     // Catch: all -> 0x0081
            if (r0 == 0) goto L_0x0035
            java.lang.Object r0 = r4.get(r8)     // Catch: all -> 0x0081
            X.1YF r0 = (X.AnonymousClass1YF) r0     // Catch: all -> 0x0081
        L_0x0033:
            monitor-exit(r3)     // Catch: all -> 0x0081
            goto L_0x0074
        L_0x0035:
            r0 = 1
            java.lang.String[] r6 = new java.lang.String[r0]     // Catch: all -> 0x0081
            X.0sU r0 = r5.A00     // Catch: all -> 0x0081
            long r0 = r0.A01(r8)     // Catch: all -> 0x0081
            java.lang.String r1 = java.lang.Long.toString(r0)     // Catch: all -> 0x0081
            r0 = 0
            r6[r0] = r1     // Catch: all -> 0x0081
            X.0p7 r0 = r5.A01     // Catch: all -> 0x0081
            X.0on r2 = r0.get()     // Catch: all -> 0x0081
            X.0op r1 = r2.A03     // Catch: all -> 0x007c
            java.lang.String r0 = "SELECT raw_id, timestamp, expected_timestamp, expected_ts_last_device_job_ts, expected_timestamp_update_ts FROM user_device_info WHERE user_jid_row_id = ?"
            android.database.Cursor r1 = r1.A09(r0, r6)     // Catch: all -> 0x007c
            if (r1 == 0) goto L_0x0056
            goto L_0x0058
        L_0x0056:
            r0 = 0
            goto L_0x0062
        L_0x0058:
            boolean r0 = r1.moveToNext()     // Catch: all -> 0x0075
            if (r0 == 0) goto L_0x0056
            X.1YF r0 = r5.A00(r1)     // Catch: all -> 0x0075
        L_0x0062:
            r4.put(r8, r0)     // Catch: all -> 0x0075
            if (r1 == 0) goto L_0x006a
            r1.close()     // Catch: all -> 0x007c
        L_0x006a:
            r2.close()     // Catch: all -> 0x0081
            java.lang.Object r0 = r4.get(r8)     // Catch: all -> 0x0081
            X.1YF r0 = (X.AnonymousClass1YF) r0     // Catch: all -> 0x0081
            goto L_0x0033
        L_0x0074:
            return r0
        L_0x0075:
            r0 = move-exception
            if (r1 == 0) goto L_0x007b
            r1.close()     // Catch: all -> 0x007b
        L_0x007b:
            throw r0     // Catch: all -> 0x007c
        L_0x007c:
            r0 = move-exception
            r2.close()     // Catch: all -> 0x0080
        L_0x0080:
            throw r0     // Catch: all -> 0x0081
        L_0x0081:
            r0 = move-exception
            monitor-exit(r3)     // Catch: all -> 0x0081
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C18770sz.A09(com.whatsapp.jid.UserJid):X.1YF");
    }

    public Map A0A(UserJid userJid) {
        AnonymousClass009.A0F(!this.A01.A0F(userJid));
        HashMap hashMap = new HashMap(this.A05.A01(userJid).A00);
        DeviceJid of = DeviceJid.of(userJid);
        AnonymousClass009.A05(of);
        hashMap.put(of, 0L);
        return hashMap;
    }

    public Set A0B() {
        HashSet hashSet = new HashSet();
        AnonymousClass1MU A02 = this.A01.A02();
        if (A02 != null) {
            Iterator it = A06().iterator();
            while (it.hasNext()) {
                DeviceJid deviceJid = (DeviceJid) it.next();
                if (C15380n4.A0L(deviceJid.getUserJid())) {
                    try {
                        hashSet.add(new AnonymousClass1MX(A02, deviceJid.device));
                    } catch (AnonymousClass1MW e) {
                        Log.w("Failed to map to LID companion", e);
                    }
                }
            }
        }
        return hashSet;
    }

    public Set A0C() {
        HashSet hashSet = new HashSet();
        Iterator it = A06().iterator();
        while (it.hasNext()) {
            DeviceJid deviceJid = (DeviceJid) it.next();
            if (C15380n4.A0L(deviceJid.getUserJid())) {
                hashSet.add(deviceJid);
            }
        }
        return hashSet;
    }

    public Set A0D(UserJid userJid) {
        if (this.A06.A05()) {
            return A0E(userJid);
        }
        HashSet hashSet = new HashSet();
        hashSet.add(userJid.getPrimaryDevice());
        return hashSet;
    }

    public Set A0E(UserJid userJid) {
        Set A0B;
        Object obj;
        C15570nT r2 = this.A01;
        r2.A08();
        if (userJid.equals(r2.A05)) {
            A0B = A0C();
            r2.A08();
            obj = r2.A04;
        } else if (userJid.equals(r2.A02())) {
            A0B = A0B();
            synchronized (r2) {
                r2.A09();
                obj = r2.A02;
            }
        } else {
            HashSet hashSet = new HashSet(this.A05.A01(userJid).A02().A00);
            DeviceJid of = DeviceJid.of(userJid);
            AnonymousClass009.A05(of);
            hashSet.add(of);
            return hashSet;
        }
        AnonymousClass009.A05(obj);
        A0B.add(obj);
        return A0B;
    }

    public void A0F(C28601Of r21, UserJid userJid) {
        HashMap hashMap = new HashMap(r21.A00);
        C245916c r6 = this.A05;
        HashMap hashMap2 = new HashMap(r6.A01(userJid).A00);
        for (Object obj : hashMap2.keySet()) {
            hashMap.remove(obj);
        }
        A0I(userJid, hashMap);
        if (!hashMap2.containsKey(userJid.getPrimaryDevice())) {
            StringBuilder sb = new StringBuilder("UserDeviceManager/addDevicesForUser/no primary device for this user, jid=");
            sb.append(userJid);
            Log.w(sb.toString());
            hashMap.put(userJid.getPrimaryDevice(), 0L);
        }
        if (!hashMap.isEmpty()) {
            C28601Of A00 = C28601Of.A00(hashMap);
            AnonymousClass009.A0C("only add new device for others", !r6.A01.A0F(userJid));
            if (!A00.A00.isEmpty()) {
                C16310on A02 = r6.A02.A02();
                try {
                    AnonymousClass1Lx A002 = A02.A00();
                    AnonymousClass1JO A022 = r6.A01(userJid).A02();
                    C246016d r12 = r6.A05;
                    C16310on A023 = r12.A01.A02();
                    AnonymousClass1Lx A003 = A023.A00();
                    Iterator it = A00.A01().iterator();
                    while (it.hasNext()) {
                        Map.Entry entry = (Map.Entry) it.next();
                        DeviceJid deviceJid = (DeviceJid) entry.getKey();
                        long longValue = ((Long) entry.getValue()).longValue();
                        boolean z = false;
                        if (deviceJid.device == 0) {
                            z = true;
                        }
                        if ((!z || longValue != 0) && (!(!z) || longValue <= 0)) {
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("invalid devices jid=");
                            sb2.append(deviceJid);
                            sb2.append("; keyIndex=");
                            sb2.append(longValue);
                            AnonymousClass009.A07(sb2.toString());
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append("DeviceStore/addDevicesForUser/invalid devices jid=");
                            sb3.append(deviceJid);
                            sb3.append("; keyIndex=");
                            sb3.append(longValue);
                            Log.e(sb3.toString());
                        }
                        r12.A03(deviceJid, userJid, longValue);
                    }
                    A003.A00();
                    r12.A02(A023, userJid);
                    A003.close();
                    A023.close();
                    AnonymousClass1JO A024 = A00.A02();
                    AnonymousClass1JO r1 = AnonymousClass1JO.A01;
                    r6.A05(A022, A024, r1, userJid, false);
                    A002.A00();
                    A002.close();
                    A02.close();
                    r6.A04(A022, A00.A02(), r1, userJid);
                } catch (Throwable th) {
                    try {
                        A02.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            }
            this.A04.A05(userJid, hashMap.keySet(), Collections.emptySet());
        }
    }

    public void A0G(AnonymousClass1JO r11, UserJid userJid, boolean z) {
        HashSet hashSet = new HashSet(r11.A00);
        C245916c r4 = this.A05;
        hashSet.retainAll(new HashSet(r4.A01(userJid).A02().A00));
        if (!hashSet.isEmpty() || z) {
            AnonymousClass1JO A00 = AnonymousClass1JO.A00(hashSet);
            AnonymousClass009.A0C("only remove device for others", !r4.A01.A0F(userJid));
            DeviceJid primaryDevice = userJid.getPrimaryDevice();
            Set set = A00.A00;
            AnonymousClass009.A0C("never remove primary device.", !set.contains(primaryDevice));
            if (!set.isEmpty()) {
                C16310on A02 = r4.A02.A02();
                try {
                    AnonymousClass1Lx A002 = A02.A00();
                    AnonymousClass1JO A022 = r4.A01(userJid).A02();
                    r4.A05.A01(A00, userJid);
                    if (z) {
                        r4.A03.A02(userJid);
                    }
                    AnonymousClass1JO r6 = AnonymousClass1JO.A01;
                    r4.A05(A022, r6, A00, userJid, false);
                    A002.A00();
                    A002.close();
                    A02.close();
                    r4.A04(A022, r6, A00, userJid);
                } catch (Throwable th) {
                    try {
                        A02.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            } else if (z) {
                r4.A03.A02(userJid);
            }
            if (!hashSet.isEmpty()) {
                this.A04.A05(userJid, Collections.emptySet(), hashSet);
            }
        }
    }

    public void A0H(AnonymousClass1YF r6, UserJid userJid) {
        C15570nT r1 = this.A01;
        if (r1.A0F(userJid)) {
            r1.A08();
            AnonymousClass009.A0E(false);
            C14820m6 r0 = this.A03;
            int i = r6.A00;
            SharedPreferences sharedPreferences = r0.A00;
            sharedPreferences.edit().putInt("adv_raw_id", i).apply();
            sharedPreferences.edit().putLong("adv_timestamp_sec", r6.A04).apply();
            sharedPreferences.edit().putLong("adv_expected_timestamp_sec_in_companion_mode", r6.A01).apply();
            sharedPreferences.edit().putLong("adv_expected_ts_last_device_job_ts_in_companion_mode", r6.A03).apply();
            sharedPreferences.edit().putLong("adv_expected_ts_update_ts_in_companion_mode", r6.A02).apply();
            return;
        }
        this.A05.A03.A01(r6, userJid);
    }

    public void A0I(UserJid userJid, HashMap hashMap) {
        String str;
        HashMap hashMap2 = new HashMap();
        for (Map.Entry entry : hashMap.entrySet()) {
            if (!((DeviceJid) entry.getKey()).getUserJid().equals(userJid)) {
                hashMap2.put(entry.getKey(), entry.getValue());
            }
        }
        if (hashMap2.size() > 0) {
            AbstractC15710nm r3 = this.A00;
            StringBuilder sb = new StringBuilder("userJid=");
            sb.append(userJid);
            sb.append("; deviceJids=");
            StringBuilder sb2 = new StringBuilder();
            for (Map.Entry entry2 : hashMap2.entrySet()) {
                sb2.append(",");
                sb2.append(entry2.getKey());
                sb2.append(":");
                sb2.append(entry2.getValue());
            }
            if (sb2.length() > 0) {
                str = sb2.substring(1);
            } else {
                str = "no-data-found";
            }
            sb.append(str);
            r3.AaV("userdevicemanager/invalid_devices", sb.toString(), false);
            for (Object obj : hashMap2.keySet()) {
                hashMap.remove(obj);
            }
        }
    }

    public void A0J(UserJid userJid, boolean z) {
        AnonymousClass009.A0F(!this.A01.A0F(userJid));
        HashSet hashSet = new HashSet(this.A05.A01(userJid).A02().A00);
        hashSet.remove(userJid.getPrimaryDevice());
        A0G(AnonymousClass1JO.A00(hashSet), userJid, z);
    }

    public boolean A0K(C28601Of r30, AnonymousClass1YF r31, UserJid userJid, boolean z) {
        AnonymousClass009.A0C("cannot refresh yourself device", !this.A01.A0F(userJid));
        HashMap hashMap = new HashMap(r30.A00);
        A0I(userJid, hashMap);
        C28601Of A00 = C28601Of.A00(hashMap);
        C245916c r7 = this.A05;
        C28601Of A01 = r7.A01(userJid);
        AnonymousClass009.A0C("only refresh devices for others", !r7.A01.A0F(userJid));
        AnonymousClass009.A0C("device list should always include primary.", A00.A02().A00.contains(userJid.getPrimaryDevice()));
        C28601Of A012 = r7.A01(userJid);
        AnonymousClass1JO A013 = A01(A00, A012);
        AnonymousClass1JO A02 = A02(A00, A012);
        if (!A013.A00.isEmpty() || !A02.A00.isEmpty()) {
            C16310on A022 = r7.A02.A02();
            try {
                AnonymousClass1Lx A002 = A022.A00();
                C246016d r12 = r7.A05;
                long A014 = r12.A00.A01(userJid);
                C16310on A023 = r12.A01.A02();
                AnonymousClass1Lx A003 = A023.A00();
                A023.A03.A01("user_device", "user_jid_row_id = ?", new String[]{String.valueOf(A014)});
                if (!A00.A00.isEmpty()) {
                    Iterator it = A00.A01().iterator();
                    while (it.hasNext()) {
                        Map.Entry entry = (Map.Entry) it.next();
                        r12.A03((DeviceJid) entry.getKey(), userJid, ((Long) entry.getValue()).longValue());
                    }
                }
                A003.A00();
                r12.A02(A023, userJid);
                A003.close();
                A023.close();
                if (r31 != null) {
                    r7.A03.A01(r31, userJid);
                }
                r7.A05(A012.A02(), A013, A02, userJid, z);
                A002.A00();
                A002.close();
                A022.close();
            } catch (Throwable th) {
                try {
                    A022.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } else {
            if (z) {
                r7.A05(A012.A02(), A013, A02, userJid, z);
            }
            if (r31 != null) {
                r7.A03.A01(r31, userJid);
            }
        }
        r7.A04(A012.A02(), A013, A02, userJid);
        HashSet hashSet = new HashSet(A01(A00, A01).A00);
        HashSet hashSet2 = new HashSet(A02(A00, A01).A00);
        this.A04.A05(userJid, hashSet, hashSet2);
        return !hashSet.isEmpty() || !hashSet2.isEmpty();
    }
}
