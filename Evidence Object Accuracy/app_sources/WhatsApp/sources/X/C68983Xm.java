package X;

import com.whatsapp.registration.EULA;
import com.whatsapp.util.ViewOnClickCListenerShape5S0200000_I1;

/* renamed from: X.3Xm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68983Xm implements AnonymousClass5W3 {
    public final /* synthetic */ ViewOnClickCListenerShape5S0200000_I1 A00;

    public C68983Xm(ViewOnClickCListenerShape5S0200000_I1 viewOnClickCListenerShape5S0200000_I1) {
        this.A00 = viewOnClickCListenerShape5S0200000_I1;
    }

    @Override // X.AnonymousClass5W3
    public void ARi() {
        ((EULA) this.A00.A00).A0B.A00();
    }

    @Override // X.AnonymousClass5W3
    public void ARk() {
        AnonymousClass1C8 r2 = ((EULA) this.A00.A00).A0B;
        r2.A03 = true;
        r2.A00 = System.currentTimeMillis();
    }
}
