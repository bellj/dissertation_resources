package X;

import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.preference.Preference;
import androidx.preference.PreferenceGroup;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0F1  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0F1 extends AnonymousClass02M implements AbstractC11330g7 {
    public Handler A00;
    public PreferenceGroup A01;
    public Runnable A02 = new RunnableC09270cd(this);
    public List A03;
    public List A04;
    public List A05;

    public AnonymousClass0F1(PreferenceGroup preferenceGroup) {
        this.A01 = preferenceGroup;
        this.A00 = new Handler();
        this.A01.A09 = this;
        this.A04 = new ArrayList();
        this.A05 = new ArrayList();
        this.A03 = new ArrayList();
        A07(true);
        A0G();
    }

    @Override // X.AnonymousClass02M
    public long A00(int i) {
        if (!super.A00) {
            return -1;
        }
        return A0E(i).A00();
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A05.size();
    }

    public Preference A0E(int i) {
        if (i < 0 || i >= this.A05.size()) {
            return null;
        }
        return (Preference) this.A05.get(i);
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0035  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List A0F(androidx.preference.PreferenceGroup r11) {
        /*
            r10 = this;
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            java.util.List r9 = r11.A02
            int r8 = r9.size()
            r7 = 0
            r6 = 0
        L_0x0012:
            if (r7 >= r8) goto L_0x0077
            java.lang.Object r3 = r9.get(r7)
            androidx.preference.Preference r3 = (androidx.preference.Preference) r3
            boolean r0 = r3.A0c
            if (r0 == 0) goto L_0x0032
            int r2 = r11.A01
            r0 = 2147483647(0x7fffffff, float:NaN)
            r1 = 0
            if (r2 == r0) goto L_0x0073
            r1 = 1
            if (r6 < r2) goto L_0x0073
            r5.add(r3)
        L_0x002c:
            boolean r0 = r3 instanceof androidx.preference.PreferenceGroup
            if (r0 != 0) goto L_0x0035
            int r6 = r6 + 1
        L_0x0032:
            int r7 = r7 + 1
            goto L_0x0012
        L_0x0035:
            androidx.preference.PreferenceGroup r3 = (androidx.preference.PreferenceGroup) r3
            boolean r0 = r3.A0W()
            if (r0 == 0) goto L_0x0032
            if (r1 == 0) goto L_0x004e
            int r1 = r3.A01
            r0 = 2147483647(0x7fffffff, float:NaN)
            if (r1 == r0) goto L_0x004e
            java.lang.String r1 = "Nesting an expandable group inside of another expandable group is not supported!"
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>(r1)
            throw r0
        L_0x004e:
            java.util.List r0 = r10.A0F(r3)
            java.util.Iterator r3 = r0.iterator()
        L_0x0056:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x0032
            java.lang.Object r2 = r3.next()
            int r1 = r11.A01
            r0 = 2147483647(0x7fffffff, float:NaN)
            if (r1 == r0) goto L_0x006f
            if (r6 < r1) goto L_0x006f
            r5.add(r2)
        L_0x006c:
            int r6 = r6 + 1
            goto L_0x0056
        L_0x006f:
            r4.add(r2)
            goto L_0x006c
        L_0x0073:
            r4.add(r3)
            goto L_0x002c
        L_0x0077:
            int r1 = r11.A01
            r0 = 2147483647(0x7fffffff, float:NaN)
            if (r1 == r0) goto L_0x0095
            if (r6 <= r1) goto L_0x0095
            android.content.Context r0 = r11.A05
            long r2 = r11.A00()
            androidx.preference.ExpandButton r1 = new androidx.preference.ExpandButton
            r1.<init>(r0, r5, r2)
            X.0Yw r0 = new X.0Yw
            r0.<init>(r11, r10)
            r1.A0B = r0
            r4.add(r1)
        L_0x0095:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0F1.A0F(androidx.preference.PreferenceGroup):java.util.List");
    }

    public void A0G() {
        for (Preference preference : this.A04) {
            preference.A09 = null;
        }
        ArrayList arrayList = new ArrayList(this.A04.size());
        this.A04 = arrayList;
        PreferenceGroup preferenceGroup = this.A01;
        A0H(preferenceGroup, arrayList);
        this.A05 = A0F(preferenceGroup);
        A02();
        Iterator it = this.A04.iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public final void A0H(PreferenceGroup preferenceGroup, List list) {
        List list2;
        synchronized (preferenceGroup) {
            list2 = preferenceGroup.A02;
            Collections.sort(list2);
        }
        int size = list2.size();
        for (int i = 0; i < size; i++) {
            Preference preference = (Preference) list2.get(i);
            list.add(preference);
            AnonymousClass0PF r2 = new AnonymousClass0PF(preference);
            List list3 = this.A03;
            if (!list3.contains(r2)) {
                list3.add(r2);
            }
            if (preference instanceof PreferenceGroup) {
                PreferenceGroup preferenceGroup2 = (PreferenceGroup) preference;
                if (preferenceGroup2.A0W()) {
                    A0H(preferenceGroup2, list);
                }
            }
            preference.A09 = this;
        }
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r2, int i) {
        A0E(i).A0R((AnonymousClass0FF) r2);
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        AnonymousClass0PF r6 = (AnonymousClass0PF) this.A03.get(i);
        LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
        TypedArray obtainStyledAttributes = viewGroup.getContext().obtainStyledAttributes((AttributeSet) null, AnonymousClass0MO.A00);
        Drawable drawable = obtainStyledAttributes.getDrawable(0);
        if (drawable == null) {
            drawable = C012005t.A01().A04(viewGroup.getContext(), 17301602);
        }
        obtainStyledAttributes.recycle();
        View inflate = from.inflate(r6.A00, viewGroup, false);
        if (inflate.getBackground() == null) {
            inflate.setBackground(drawable);
        }
        ViewGroup viewGroup2 = (ViewGroup) inflate.findViewById(16908312);
        if (viewGroup2 != null) {
            int i2 = r6.A01;
            if (i2 != 0) {
                from.inflate(i2, viewGroup2);
            } else {
                viewGroup2.setVisibility(8);
            }
        }
        return new AnonymousClass0FF(inflate);
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        AnonymousClass0PF r3 = new AnonymousClass0PF(A0E(i));
        List list = this.A03;
        int indexOf = list.indexOf(r3);
        if (indexOf != -1) {
            return indexOf;
        }
        int size = list.size();
        list.add(r3);
        return size;
    }
}
