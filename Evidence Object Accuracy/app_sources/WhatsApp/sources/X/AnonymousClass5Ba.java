package X;

/* renamed from: X.5Ba  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5Ba implements Comparable {
    public final boolean A00;
    public final boolean A01;

    public AnonymousClass5Ba(C100614mC r4, int i) {
        this.A00 = (r4.A0G & 1) == 0 ? false : true;
        this.A01 = (i & 7) != 4 ? false : true;
    }

    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(Object obj) {
        AnonymousClass5Ba r4 = (AnonymousClass5Ba) obj;
        return AbstractC95284dR.start().compareFalseFirst(this.A01, r4.A01).compareFalseFirst(this.A00, r4.A00).result();
    }
}
