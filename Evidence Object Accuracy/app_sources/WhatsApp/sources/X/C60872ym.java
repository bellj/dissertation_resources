package X;

import android.content.Context;
import android.view.View;
import android.view.accessibility.AccessibilityManager;
import android.widget.ImageView;
import android.widget.TextView;
import com.facebook.redex.ViewOnClickCListenerShape8S0100000_I1_2;
import com.whatsapp.Conversation;
import com.whatsapp.R;
import com.whatsapp.search.views.itemviews.AudioPlayerView;
import com.whatsapp.search.views.itemviews.VoiceNoteProfileAvatarView;

/* renamed from: X.2ym  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60872ym extends AnonymousClass2y3 {
    public TextView A00;
    public C50532Px A01;
    public final View.OnClickListener A02 = new ViewOnClickCListenerShape8S0100000_I1_2(this, 38);
    public final ImageView A03;
    public final ImageView A04;
    public final AnonymousClass1J1 A05;
    public final AnonymousClass11P A06;
    public final C239913u A07;
    public final C63663Cl A08;
    public final AudioPlayerView A09;
    public final VoiceNoteProfileAvatarView A0A;
    public final boolean A0B;
    public final boolean A0C;

    public C60872ym(Context context, AnonymousClass1J1 r12, AbstractC13890kV r13, AnonymousClass19D r14, AnonymousClass11P r15, C239913u r16, C30421Xi r17) {
        super(context, r13, r14, r15, r17);
        this.A05 = r12;
        this.A07 = r16;
        this.A06 = r15;
        this.A0C = AbstractC28651Ol.A01(((AbstractC28551Oa) this).A0L);
        this.A09 = (AudioPlayerView) AnonymousClass028.A0D(this, R.id.conversation_row_audio_player_view);
        VoiceNoteProfileAvatarView voiceNoteProfileAvatarView = (VoiceNoteProfileAvatarView) AnonymousClass028.A0D(this, R.id.conversation_row_voice_note_profile_avatar);
        this.A0A = voiceNoteProfileAvatarView;
        this.A04 = voiceNoteProfileAvatarView.A05;
        this.A03 = voiceNoteProfileAvatarView.A03;
        this.A00 = C12960it.A0J(this, R.id.description);
        this.A0B = AnonymousClass23N.A05(((AbstractC28551Oa) this).A0J.A0P());
        AnonymousClass01J r0 = this.A01.A00.A04;
        this.A08 = new C63663Cl((AnonymousClass19D) r0.ABo.get(), C12970iu.A0Z(r0), voiceNoteProfileAvatarView);
        A1S(false, false);
    }

    @Override // X.C60752yZ, X.AnonymousClass1OY
    public void A0s() {
        super.A0s();
        A1S(false, false);
    }

    @Override // X.C60752yZ, X.AbstractC42671vd, X.AnonymousClass1OY
    public void A0y() {
        C30421Xi A02;
        AccessibilityManager A0P;
        if (((AbstractC28551Oa) this).A0L.A07(931)) {
            C40691s6.A04(((C60752yZ) this).A04.getRootView(), this.A06, ((C60752yZ) this).A00);
        }
        C30421Xi r11 = (C30421Xi) ((AbstractC16130oV) ((AbstractC28551Oa) this).A0O);
        AbstractC13890kV r6 = ((AbstractC28551Oa) this).A0a;
        if (r6 instanceof AbstractC13930kZ) {
            AbstractC13930kZ r62 = (AbstractC13930kZ) r6;
            Conversation conversation = (Conversation) r62;
            boolean z = true;
            if (!conversation.A4U && (A0P = ((ActivityC13810kN) conversation).A08.A0P()) != null && A0P.isTouchExplorationEnabled()) {
                z = false;
            }
            if (((AbstractC42671vd) this).A01 == null || AnonymousClass1OY.A0U(this)) {
                Context context = getContext();
                AnonymousClass59B r12 = new AnonymousClass59B(this);
                AnonymousClass1CY r13 = ((AbstractC28551Oa) this).A0P;
                AnonymousClass009.A05(r13);
                if (AnonymousClass3JG.A03(context, ((AnonymousClass1OY) this).A0J, r11, r12, r13, this.A1O)) {
                    if (!(!this.A0C || (A02 = this.A06.A02()) == null || A02.A11 == r11.A11)) {
                        r62.Afc(A02, 0, false);
                    }
                    C35191hP A00 = ((C60752yZ) this).A07.A00(AnonymousClass12P.A02(this), r11, true);
                    A00.A0C(r11);
                    A00.A0L = new AnonymousClass55F(this);
                    if (z) {
                        conversation.A4K = true;
                        A00.A0J = new AbstractC116055Ty(A00, r11) { // from class: X.3WM
                            public final /* synthetic */ C35191hP A01;
                            public final /* synthetic */ C30421Xi A02;

                            {
                                this.A02 = r3;
                                this.A01 = r2;
                            }

                            @Override // X.AbstractC116055Ty
                            public final void ATn(int i) {
                                C60872ym r5 = C60872ym.this;
                                C30421Xi r4 = this.A02;
                                C35191hP r2 = this.A01;
                                ((AnonymousClass19F) ((C60752yZ) r5).A01.get()).AaO(r4.A11);
                                AbstractC13890kV r63 = ((AbstractC28551Oa) r5).A0a;
                                AbstractC13930kZ r3 = (AbstractC13930kZ) r63;
                                boolean z2 = r2.A0T;
                                Conversation conversation2 = (Conversation) r3;
                                if (AnonymousClass3GJ.A01(((ActivityC13810kN) conversation2).A0C, AnonymousClass3GJ.A00(conversation2.A1g.getConversationCursorAdapter(), r4), r4, z2) && r3.A7g(r4, i, r2.A0T, r2.A0Y)) {
                                    r2.A0S = true;
                                } else if (r5.A0C && (r63 instanceof AbstractC13930kZ)) {
                                    r3.Afc(r4, (long) ((AnonymousClass1OY) r5).A0N.A02(AbstractC15460nI.A24), true);
                                }
                            }
                        };
                    } else {
                        A00.A0J = new AbstractC116055Ty(r11) { // from class: X.3WL
                            public final /* synthetic */ C30421Xi A01;

                            {
                                this.A01 = r2;
                            }

                            @Override // X.AbstractC116055Ty
                            public final void ATn(int i) {
                                C60872ym r5 = C60872ym.this;
                                C30421Xi r4 = this.A01;
                                ((AnonymousClass19F) ((C60752yZ) r5).A01.get()).AaO(r4.A11);
                                if (r5.A0C) {
                                    AbstractC13890kV r3 = ((AbstractC28551Oa) r5).A0a;
                                    if (r3 instanceof AbstractC13930kZ) {
                                        ((AbstractC13930kZ) r3).Afc(r4, (long) ((AnonymousClass1OY) r5).A0N.A02(AbstractC15460nI.A24), true);
                                    }
                                }
                            }
                        };
                    }
                    A00.A0F(true);
                    super.A0s();
                    A1S(true, true);
                    return;
                }
                return;
            }
            return;
        }
        super.A0y();
    }

    @Override // X.C60752yZ, X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r3, boolean z) {
        boolean A1X = C12960it.A1X(r3, (AbstractC16130oV) ((AbstractC28551Oa) this).A0O);
        super.A1D(r3, z);
        if (z || A1X) {
            A1S(false, false);
        }
    }

    @Override // X.AnonymousClass1OY
    public void A1E(AbstractC15340mz r3, boolean z) {
        super.A1D(r3, true);
        A1S(false, true);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0125, code lost:
        if (r0 != null) goto L_0x0127;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0151, code lost:
        if (X.C15380n4.A0N(r2) != false) goto L_0x0141;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A1S(boolean r12, boolean r13) {
        /*
        // Method dump skipped, instructions count: 351
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C60872ym.A1S(boolean, boolean):void");
    }

    @Override // X.C60752yZ, X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_voice_note_left;
    }

    @Override // X.C60752yZ, X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_voice_note_left;
    }

    @Override // X.C60752yZ, X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_voice_note_right;
    }

    @Override // X.C60752yZ
    public void setDuration(String str) {
        TextView textView = this.A00;
        if (textView == null) {
            textView = C12960it.A0J(this, R.id.description);
            this.A00 = textView;
        }
        textView.setText(str);
    }
}
