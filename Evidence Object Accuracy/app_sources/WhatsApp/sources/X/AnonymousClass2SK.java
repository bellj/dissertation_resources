package X;

import java.util.Arrays;

/* renamed from: X.2SK  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2SK {
    public int A00;
    public String A01;
    public String A02;
    public boolean A03;

    public AnonymousClass2SK(String str, String str2, int i, boolean z) {
        this.A00 = i;
        this.A03 = z;
        this.A02 = str;
        this.A01 = str2;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof AnonymousClass2SK) {
                AnonymousClass2SK r5 = (AnonymousClass2SK) obj;
                if (this.A00 != r5.A00 || this.A03 != r5.A03 || !C29941Vi.A00(this.A02, r5.A02) || !C29941Vi.A00(this.A01, r5.A01)) {
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Arrays.deepHashCode(new Object[]{Integer.valueOf(this.A00), Boolean.valueOf(this.A03), this.A02, this.A01});
    }
}
