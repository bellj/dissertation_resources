package X;

import java.util.LinkedHashMap;
import java.util.Map;

/* renamed from: X.16i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C246516i<K, V> extends LinkedHashMap<K, V> {
    public int cacheLimit;

    public C246516i(int i) {
        super(((int) Math.ceil((double) (((float) i) / 0.75f))) + 1, 0.75f, true);
        this.cacheLimit = i;
    }

    @Override // java.util.LinkedHashMap
    public boolean removeEldestEntry(Map.Entry entry) {
        return size() > this.cacheLimit;
    }
}
