package X;

import android.content.DialogInterface;
import com.facebook.redex.IDxDListenerShape14S0100000_3_I1;
import com.whatsapp.R;
import com.whatsapp.payments.pin.ui.PinBottomSheetDialogFragment;
import com.whatsapp.payments.ui.BrazilPaymentActivity;
import com.whatsapp.payments.ui.BrazilPaymentDPOActivity;
import com.whatsapp.payments.ui.widget.PaymentView;

/* renamed from: X.6Ah  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133316Ah implements AbstractC1311361k {
    public final /* synthetic */ AnonymousClass6CG A00;

    public C133316Ah(AnonymousClass6CG r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC136506Mu
    public void AKZ(C452120p r4) {
        this.A00.A04.A0N.A06("br_pay_precheck_tag", 3);
        APo(r4);
    }

    @Override // X.AbstractC136506Mu
    public void AKb() {
        this.A00.A04.A0N.A02(185481152, "br_pay_precheck_tag");
    }

    @Override // X.AbstractC136506Mu
    public void AKk() {
        this.A00.A04.A0N.A06("br_get_provider_key_tag", 3);
    }

    @Override // X.AbstractC136506Mu
    public void AKl() {
        this.A00.A04.A0N.A06("br_get_provider_key_tag", 2);
    }

    @Override // X.AbstractC1311361k
    public void APo(C452120p r13) {
        C32631cT A00;
        AnonymousClass04S A002;
        AnonymousClass6CG r0 = this.A00;
        PinBottomSheetDialogFragment pinBottomSheetDialogFragment = r0.A03;
        pinBottomSheetDialogFragment.A1M();
        BrazilPaymentActivity brazilPaymentActivity = r0.A04;
        brazilPaymentActivity.A0N.A03(brazilPaymentActivity.A00, "error_code", (long) r13.A00);
        brazilPaymentActivity.A0N.A05(r13, "br_get_provider_key_tag");
        int i = r13.A00;
        switch (i) {
            case 454:
                AnonymousClass1V8 r02 = r13.A05;
                if (!(r02 == null || (A00 = C32631cT.A00(r02)) == null)) {
                    ((AbstractActivityC121685jC) brazilPaymentActivity).A0D.A04(AnonymousClass4EV.A00(((AbstractActivityC121685jC) brazilPaymentActivity).A07, A00));
                }
                BrazilPaymentActivity.A02(pinBottomSheetDialogFragment, brazilPaymentActivity);
                return;
            case 1440:
                pinBottomSheetDialogFragment.A1O(r13.A01);
                return;
            case 1441:
                pinBottomSheetDialogFragment.A1P(r13.A02 * 1000, true);
                return;
            case 2826048:
                AnonymousClass62Q r5 = new DialogInterface.OnClickListener(pinBottomSheetDialogFragment, this) { // from class: X.62Q
                    public final /* synthetic */ PinBottomSheetDialogFragment A00;
                    public final /* synthetic */ C133316Ah A01;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                    }

                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i2) {
                        C133316Ah r1 = this.A01;
                        this.A00.A1C();
                        BrazilPaymentActivity brazilPaymentActivity2 = r1.A00.A04;
                        brazilPaymentActivity2.startActivity(C12990iw.A0D(brazilPaymentActivity2, BrazilPaymentDPOActivity.class));
                        AbstractC16870pt r12 = brazilPaymentActivity2.A0K;
                        AnonymousClass3FW A0S = C117305Zk.A0S();
                        C117325Zm.A07(A0S);
                        AnonymousClass61I.A01(A0S, r12, 120, "payment_disabled_alert", null, 1);
                    }
                };
                IDxDListenerShape14S0100000_3_I1 iDxDListenerShape14S0100000_3_I1 = new IDxDListenerShape14S0100000_3_I1(pinBottomSheetDialogFragment, 6);
                String string = brazilPaymentActivity.getString(R.string.brazil_payments_dpo_error_title);
                String string2 = brazilPaymentActivity.getString(R.string.brazil_payments_dpo_error_message);
                C004802e A0S = C12980iv.A0S(brazilPaymentActivity);
                A0S.setPositiveButton(R.string.payments_get_help, r5);
                A0S.A04(iDxDListenerShape14S0100000_3_I1);
                if (string2 == null) {
                    A0S.A0A(string);
                } else {
                    A0S.setTitle(string);
                    A0S.A0A(string2);
                }
                C12970iu.A1J(A0S);
                String string3 = brazilPaymentActivity.A01.getString(R.string.brazil_payments_dpo_error_message);
                AbstractC16870pt r3 = brazilPaymentActivity.A0K;
                AnonymousClass3FW A0S2 = C117305Zk.A0S();
                C117325Zm.A07(A0S2);
                A0S2.A01("dialog_text", string3);
                AnonymousClass61I.A01(A0S2, r3, null, "payment_disabled_alert", null, 0);
                return;
            case 2896003:
            case 2896004:
                AnonymousClass61I.A03(AnonymousClass61I.A00(((ActivityC13790kL) brazilPaymentActivity).A05, null, ((AbstractActivityC121685jC) brazilPaymentActivity).A0U, null, false), brazilPaymentActivity.A0K, "incentive_unavailable", "payment_confirm_prompt");
                A002 = brazilPaymentActivity.A08.A00(brazilPaymentActivity, null, new DialogInterface.OnDismissListener(pinBottomSheetDialogFragment, this) { // from class: X.62u
                    public final /* synthetic */ PinBottomSheetDialogFragment A00;
                    public final /* synthetic */ C133316Ah A01;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                    }

                    @Override // android.content.DialogInterface.OnDismissListener
                    public final void onDismiss(DialogInterface dialogInterface) {
                        C133316Ah r03 = this.A01;
                        PinBottomSheetDialogFragment pinBottomSheetDialogFragment2 = this.A00;
                        BrazilPaymentActivity brazilPaymentActivity2 = r03.A00.A04;
                        ((AbstractActivityC121685jC) brazilPaymentActivity2).A01 = 7;
                        brazilPaymentActivity2.A2m(null);
                        pinBottomSheetDialogFragment2.A1C();
                    }
                }, null, null, r13.A00);
                break;
            default:
                if (i == 444 || i == 478) {
                    brazilPaymentActivity.A0J.A01.A01("FB", "PIN");
                }
                A002 = brazilPaymentActivity.A08.A00(brazilPaymentActivity, new DialogInterface.OnDismissListener(pinBottomSheetDialogFragment, this) { // from class: X.62v
                    public final /* synthetic */ PinBottomSheetDialogFragment A00;
                    public final /* synthetic */ C133316Ah A01;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                    }

                    @Override // android.content.DialogInterface.OnDismissListener
                    public final void onDismiss(DialogInterface dialogInterface) {
                        C133316Ah r03 = this.A01;
                        PinBottomSheetDialogFragment pinBottomSheetDialogFragment2 = this.A00;
                        PaymentView paymentView = r03.A00.A04.A0V;
                        if (paymentView != null) {
                            paymentView.A0t.setText((CharSequence) null);
                        }
                        pinBottomSheetDialogFragment2.A1C();
                    }
                }, new IDxDListenerShape14S0100000_3_I1(pinBottomSheetDialogFragment, 4), new IDxDListenerShape14S0100000_3_I1(pinBottomSheetDialogFragment, 5), brazilPaymentActivity.A03.A04(((AbstractActivityC121685jC) brazilPaymentActivity).A08.A01(((AbstractActivityC121685jC) brazilPaymentActivity).A0G)), r13.A00);
                break;
        }
        A002.show();
    }

    @Override // X.AbstractC1311361k
    public void AVJ(String str) {
        AnonymousClass6CG r3 = this.A00;
        r3.A03.A1M();
        BrazilPaymentActivity brazilPaymentActivity = r3.A04;
        brazilPaymentActivity.A0N.A06("br_pay_precheck_tag", 2);
        String str2 = r3.A05;
        C30821Yy r5 = r3.A00;
        AbstractC28901Pl r6 = r3.A01;
        String str3 = r3.A06;
        int i = 1;
        if (brazilPaymentActivity.A2q(r5, ((AbstractActivityC121685jC) brazilPaymentActivity).A01) == null) {
            i = 0;
        }
        brazilPaymentActivity.A2u(r5, r6, r3.A02, str2, str, str3, i);
    }
}
