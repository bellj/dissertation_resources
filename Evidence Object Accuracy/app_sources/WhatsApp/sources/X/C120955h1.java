package X;

import java.util.Map;

/* renamed from: X.5h1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120955h1 extends C128885wp {
    public final String A00;

    public C120955h1(AnonymousClass1V8 r2) {
        super(r2);
        this.A00 = r2.A0H("postal_code_value_type");
    }

    @Override // X.C128885wp
    public Map A00() {
        Map A00 = super.A00();
        A00.put("postal_code_value_type", this.A00);
        return A00;
    }
}
