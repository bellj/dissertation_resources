package X;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;

/* renamed from: X.3FZ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3FZ {
    public final Paint A00;
    public final AnonymousClass3HI A01;
    public final AnonymousClass5S6 A02;

    public AnonymousClass3FZ(AnonymousClass3HI r3, AnonymousClass5S6 r4) {
        this.A01 = r3;
        this.A02 = r4;
        Paint A0F = C12990iw.A0F();
        this.A00 = A0F;
        C12970iu.A16(0, A0F);
        C12990iw.A14(A0F, PorterDuff.Mode.SRC);
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0078 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0079 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0036 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(int r9, android.graphics.Bitmap r10) {
        /*
            r8 = this;
            android.graphics.Canvas r4 = new android.graphics.Canvas
            r4.<init>(r10)
            android.graphics.PorterDuff$Mode r1 = android.graphics.PorterDuff.Mode.SRC
            r0 = 0
            r4.drawColor(r0, r1)
            boolean r0 = r8.A02(r9)
            r6 = r9
            if (r0 != 0) goto L_0x0079
            int r6 = r9 + -1
        L_0x0014:
            if (r6 < 0) goto L_0x0076
            X.3HI r0 = r8.A01
            X.4Sy[] r3 = r0.A09
            r2 = r3[r6]
            X.4A8 r1 = r2.A05
            X.4A8 r0 = X.AnonymousClass4A8.DISPOSE_DO_NOT
            if (r1 == r0) goto L_0x0055
            X.4A8 r0 = X.AnonymousClass4A8.DISPOSE_TO_BACKGROUND
            if (r1 != r0) goto L_0x004b
            boolean r0 = r8.A03(r2)
            if (r0 == 0) goto L_0x0055
            X.49f r0 = X.EnumC868549f.NOT_REQUIRED
        L_0x002e:
            int r0 = r0.ordinal()
            r7 = 1
            switch(r0) {
                case 0: goto L_0x0039;
                case 1: goto L_0x0078;
                case 2: goto L_0x0036;
                case 3: goto L_0x0079;
                default: goto L_0x0036;
            }
        L_0x0036:
            int r6 = r6 + -1
            goto L_0x0014
        L_0x0039:
            r5 = r3[r6]
            X.5S6 r0 = r8.A02
            X.0bz r3 = r0.AB5(r6)
            if (r3 == 0) goto L_0x0044
            goto L_0x0058
        L_0x0044:
            boolean r0 = r8.A02(r6)
            if (r0 == 0) goto L_0x0036
            goto L_0x0079
        L_0x004b:
            X.4A8 r0 = X.AnonymousClass4A8.DISPOSE_TO_PREVIOUS
            if (r1 != r0) goto L_0x0052
            X.49f r0 = X.EnumC868549f.SKIP
            goto L_0x002e
        L_0x0052:
            X.49f r0 = X.EnumC868549f.ABORT
            goto L_0x002e
        L_0x0055:
            X.49f r0 = X.EnumC868549f.REQUIRED
            goto L_0x002e
        L_0x0058:
            java.lang.Object r2 = r3.A04()     // Catch: all -> 0x0071
            android.graphics.Bitmap r2 = (android.graphics.Bitmap) r2     // Catch: all -> 0x0071
            r1 = 0
            r0 = 0
            r4.drawBitmap(r2, r0, r0, r1)     // Catch: all -> 0x0071
            X.4A8 r1 = r5.A05     // Catch: all -> 0x0071
            X.4A8 r0 = X.AnonymousClass4A8.DISPOSE_TO_BACKGROUND     // Catch: all -> 0x0071
            if (r1 != r0) goto L_0x006c
            r8.A01(r4, r5)     // Catch: all -> 0x0071
        L_0x006c:
            int r6 = r6 + r7
            r3.close()
            goto L_0x0079
        L_0x0071:
            r0 = move-exception
            r3.close()
            throw r0
        L_0x0076:
            r6 = 0
            goto L_0x0079
        L_0x0078:
            int r6 = r6 + r7
        L_0x0079:
            if (r6 >= r9) goto L_0x009d
            X.3HI r5 = r8.A01
            X.4Sy[] r0 = r5.A09
            r3 = r0[r6]
            X.4A8 r2 = r3.A05
            X.4A8 r0 = X.AnonymousClass4A8.DISPOSE_TO_PREVIOUS
            if (r2 == r0) goto L_0x009a
            X.49s r1 = r3.A04
            X.49s r0 = X.EnumC869749s.NO_BLEND
            if (r1 != r0) goto L_0x0090
            r8.A01(r4, r3)
        L_0x0090:
            r5.A02(r4, r6)
            X.4A8 r0 = X.AnonymousClass4A8.DISPOSE_TO_BACKGROUND
            if (r2 != r0) goto L_0x009a
            r8.A01(r4, r3)
        L_0x009a:
            int r6 = r6 + 1
            goto L_0x0079
        L_0x009d:
            X.3HI r3 = r8.A01
            X.4Sy[] r0 = r3.A09
            r2 = r0[r9]
            X.49s r1 = r2.A04
            X.49s r0 = X.EnumC869749s.NO_BLEND
            if (r1 != r0) goto L_0x00ac
            r8.A01(r4, r2)
        L_0x00ac:
            r3.A02(r4, r9)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3FZ.A00(int, android.graphics.Bitmap):void");
    }

    public final void A01(Canvas canvas, C91744Sy r10) {
        int i = r10.A02;
        int i2 = r10.A03;
        canvas.drawRect((float) i, (float) i2, (float) (i + r10.A01), (float) (i2 + r10.A00), this.A00);
    }

    public final boolean A02(int i) {
        if (i != 0) {
            C91744Sy[] r0 = this.A01.A09;
            C91744Sy r3 = r0[i];
            C91744Sy r2 = r0[i - 1];
            if (r3.A04 != EnumC869749s.NO_BLEND || !A03(r3)) {
                if (r2.A05 != AnonymousClass4A8.DISPOSE_TO_BACKGROUND || !A03(r2)) {
                    return false;
                }
                return true;
            }
        }
        return true;
    }

    public final boolean A03(C91744Sy r4) {
        if (r4.A02 == 0 && r4.A03 == 0) {
            int i = r4.A01;
            Rect rect = this.A01.A03;
            if (i == rect.width() && r4.A00 == rect.height()) {
                return true;
            }
        }
        return false;
    }
}
