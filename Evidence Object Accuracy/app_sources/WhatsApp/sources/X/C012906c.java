package X;

import android.content.Context;
import android.content.Intent;
import java.util.Collections;
import java.util.HashMap;

/* renamed from: X.06c  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C012906c extends AnonymousClass05K {
    @Override // X.AnonymousClass05K
    public Intent A00(Context context, Object obj) {
        return new Intent("androidx.activity.result.contract.action.REQUEST_PERMISSIONS").putExtra("androidx.activity.result.contract.extra.PERMISSIONS", (String[]) obj);
    }

    @Override // X.AnonymousClass05K
    public AnonymousClass0MP A01(Context context, Object obj) {
        int length;
        String[] strArr = (String[]) obj;
        if (strArr == null || (length = strArr.length) == 0) {
            return new AnonymousClass0MP(Collections.emptyMap());
        }
        AnonymousClass00N r5 = new AnonymousClass00N();
        int i = 0;
        boolean z = true;
        do {
            String str = strArr[i];
            boolean z2 = false;
            if (AnonymousClass00T.A01(context, str) == 0) {
                z2 = true;
            }
            r5.put(str, Boolean.valueOf(z2));
            if (!z2) {
                z = false;
            }
            i++;
        } while (i < length);
        if (z) {
            return new AnonymousClass0MP(r5);
        }
        return null;
    }

    @Override // X.AnonymousClass05K
    public Object A02(Intent intent, int i) {
        if (i == -1 && intent != null) {
            String[] stringArrayExtra = intent.getStringArrayExtra("androidx.activity.result.contract.extra.PERMISSIONS");
            int[] intArrayExtra = intent.getIntArrayExtra("androidx.activity.result.contract.extra.PERMISSION_GRANT_RESULTS");
            if (!(intArrayExtra == null || stringArrayExtra == null)) {
                HashMap hashMap = new HashMap();
                int length = stringArrayExtra.length;
                for (int i2 = 0; i2 < length; i2++) {
                    String str = stringArrayExtra[i2];
                    boolean z = false;
                    if (intArrayExtra[i2] == 0) {
                        z = true;
                    }
                    hashMap.put(str, Boolean.valueOf(z));
                }
                return hashMap;
            }
        }
        return Collections.emptyMap();
    }
}
