package X;

import android.text.Spannable;
import android.text.SpannableString;

/* renamed from: X.3gG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73453gG extends Spannable.Factory {
    @Override // android.text.Spannable.Factory
    public Spannable newSpannable(CharSequence charSequence) {
        return new C71153cT(new SpannableString(charSequence));
    }
}
