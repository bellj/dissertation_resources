package X;

import android.view.View;
import android.widget.TextView;
import com.facebook.redex.IDxObserverShape3S0100000_1_I1;
import com.whatsapp.R;
import com.whatsapp.calling.callgrid.viewmodel.CallGridViewModel;

/* renamed from: X.2vy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60122vy extends C60132vz {
    public final TextView A00;
    public final AnonymousClass02B A01 = new IDxObserverShape3S0100000_1_I1(this, 45);
    public final AnonymousClass018 A02;

    public C60122vy(View view, C18720su r4, C89374Js r5, CallGridViewModel callGridViewModel, AnonymousClass130 r7, C15610nY r8, AnonymousClass018 r9) {
        super(view, r4, r5, callGridViewModel, r7, r8);
        this.A02 = r9;
        this.A00 = C12960it.A0I(view, R.id.audio_call_duration);
    }

    @Override // X.C60132vz, X.C60142w0, X.AbstractC55202hx
    public void A0G(C64363Fg r3) {
        CallGridViewModel callGridViewModel = ((AbstractC55202hx) this).A04;
        if (callGridViewModel != null) {
            callGridViewModel.A05.A08(this.A01);
        }
        A0J(r3);
        A0M(r3);
        this.A00.setVisibility(C12960it.A02(r3.A0M ? 1 : 0));
        A0L(r3, false);
        ((AbstractC55202hx) this).A05 = r3;
    }
}
