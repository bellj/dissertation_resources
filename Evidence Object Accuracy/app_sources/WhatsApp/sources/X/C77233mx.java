package X;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* renamed from: X.3mx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77233mx extends AbstractC76783mE {
    public static final Pattern A02 = Pattern.compile("\\{\\\\.*?\\}");
    public static final Pattern A03 = Pattern.compile("\\s*((?:(\\d+):)?(\\d+):(\\d+)(?:,(\\d+))?)\\s*-->\\s*((?:(\\d+):)?(\\d+):(\\d+)(?:,(\\d+))?)\\s*");
    public final StringBuilder A00 = C12960it.A0h();
    public final ArrayList A01 = C12960it.A0l();

    public C77233mx() {
        super("SubripDecoder");
    }

    public static long A01(Matcher matcher, int i) {
        long j;
        String group = matcher.group(i + 1);
        if (group != null) {
            j = Long.parseLong(group) * 60 * 60 * 1000;
        } else {
            j = 0;
        }
        long A00 = j + (AbstractC76783mE.A00(matcher, i + 2) * 60 * 1000) + (AbstractC76783mE.A00(matcher, i + 3) * 1000);
        String group2 = matcher.group(i + 4);
        if (group2 != null) {
            A00 += Long.parseLong(group2);
        }
        return A00 * 1000;
    }
}
