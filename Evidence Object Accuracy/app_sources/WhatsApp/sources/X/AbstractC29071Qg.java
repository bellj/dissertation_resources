package X;

import java.lang.ref.PhantomReference;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.1Qg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC29071Qg extends PhantomReference {
    public AbstractC29071Qg next;
    public AbstractC29071Qg previous;

    public abstract void destruct();

    public AbstractC29071Qg() {
        super(null, C47842Cy.A03);
    }

    public /* synthetic */ AbstractC29071Qg(AnonymousClass2D5 r1) {
        this();
    }

    public AbstractC29071Qg(Object obj) {
        super(obj, C47842Cy.A03);
        AtomicReference atomicReference;
        AbstractC29071Qg r0;
        C47852Cz r2 = C47842Cy.A01;
        do {
            atomicReference = r2.A00;
            r0 = (AbstractC29071Qg) atomicReference.get();
            this.next = r0;
        } while (!atomicReference.compareAndSet(r0, this));
    }
}
