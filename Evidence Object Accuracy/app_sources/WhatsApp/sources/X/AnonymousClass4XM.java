package X;

import android.media.AudioTimestamp;
import android.media.AudioTrack;

/* renamed from: X.4XM  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4XM {
    public long A00;
    public long A01;
    public long A02;
    public final AudioTimestamp A03 = new AudioTimestamp();
    public final AudioTrack A04;

    public AnonymousClass4XM(AudioTrack audioTrack) {
        this.A04 = audioTrack;
    }

    public long A00() {
        return C12980iv.A0D(this.A03.nanoTime);
    }

    public boolean A01() {
        AudioTrack audioTrack = this.A04;
        AudioTimestamp audioTimestamp = this.A03;
        boolean timestamp = audioTrack.getTimestamp(audioTimestamp);
        if (timestamp) {
            long j = audioTimestamp.framePosition;
            if (this.A01 > j) {
                this.A02++;
            }
            this.A01 = j;
            this.A00 = j + (this.A02 << 32);
        }
        return timestamp;
    }
}
