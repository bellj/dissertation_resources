package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2mf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57212mf extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57212mf A03;
    public static volatile AnonymousClass255 A04;
    public int A00;
    public int A01;
    public long A02;

    static {
        C57212mf r0 = new C57212mf();
        A03 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r15, Object obj, Object obj2) {
        switch (r15.ordinal()) {
            case 0:
                return A03;
            case 1:
                AbstractC462925h r7 = (AbstractC462925h) obj;
                C57212mf r5 = (C57212mf) obj2;
                int i = this.A00;
                boolean A1R = C12960it.A1R(i);
                int i2 = this.A01;
                int i3 = r5.A00;
                this.A01 = r7.Afp(i2, r5.A01, A1R, C12960it.A1R(i3));
                this.A02 = r7.Afs(this.A02, r5.A02, C12960it.A1V(i & 2, 2), C12960it.A1V(i3 & 2, 2));
                if (r7 == C463025i.A00) {
                    this.A00 = i | i3;
                }
                return this;
            case 2:
                AnonymousClass253 r72 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        int A032 = r72.A03();
                        if (A032 == 0) {
                            break;
                        } else if (A032 == 8) {
                            int A02 = r72.A02();
                            if (AnonymousClass4BV.A00(A02) == null) {
                                super.A0X(1, A02);
                            } else {
                                this.A00 = 1 | this.A00;
                                this.A01 = A02;
                            }
                        } else if (A032 == 16) {
                            this.A00 |= 2;
                            this.A02 = r72.A06();
                        } else if (!A0a(r72, A032)) {
                            break;
                        }
                    } catch (C28971Pt e) {
                        throw AbstractC27091Fz.A0J(e, this);
                    } catch (IOException e2) {
                        throw AbstractC27091Fz.A0K(this, e2);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C57212mf();
            case 5:
                return new C82193vE();
            case 6:
                break;
            case 7:
                if (A04 == null) {
                    synchronized (C57212mf.class) {
                        if (A04 == null) {
                            A04 = AbstractC27091Fz.A09(A03);
                        }
                    }
                }
                return A04;
            default:
                throw C12970iu.A0z();
        }
        return A03;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i2 = AbstractC27091Fz.A03(1, this.A01, 0);
        }
        if ((i3 & 2) == 2) {
            i2 += CodedOutputStream.A05(2, this.A02);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0E(1, this.A01);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0H(2, this.A02);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
