package X;

import java.util.Arrays;

/* renamed from: X.4XD  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4XD {
    public final int A00;
    public final int A01;
    public final int A02;
    public final byte[] A03;

    public AnonymousClass4XD(int i, byte[] bArr, int i2, int i3) {
        this.A01 = i;
        this.A03 = bArr;
        this.A02 = i2;
        this.A00 = i3;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || AnonymousClass4XD.class != obj.getClass()) {
                return false;
            }
            AnonymousClass4XD r5 = (AnonymousClass4XD) obj;
            if (!(this.A01 == r5.A01 && this.A02 == r5.A02 && this.A00 == r5.A00 && Arrays.equals(this.A03, r5.A03))) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return (((((this.A01 * 31) + Arrays.hashCode(this.A03)) * 31) + this.A02) * 31) + this.A00;
    }
}
