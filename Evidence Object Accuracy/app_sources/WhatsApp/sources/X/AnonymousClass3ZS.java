package X;

import com.facebook.redex.RunnableBRunnable0Shape1S0101000_I1;

/* renamed from: X.3ZS  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3ZS implements AnonymousClass2N4 {
    public final /* synthetic */ AnonymousClass384 A00;

    @Override // X.AnonymousClass2N4
    public boolean AK7() {
        return false;
    }

    public AnonymousClass3ZS(AnonymousClass384 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass2N4
    public void APl(int i) {
        this.A00.A00.A0H(new RunnableBRunnable0Shape1S0101000_I1(this, i, 8));
    }

    @Override // X.AnonymousClass2N4
    public void AWu() {
        AnonymousClass384 r0 = this.A00;
        r0.A03.A09(C15370n3.A02(r0.A04), false);
    }
}
