package X;

import android.content.Context;
import android.net.Uri;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.whatsapp.R;
import java.util.List;
import java.util.Set;

/* renamed from: X.2UD  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2UD extends AnonymousClass02M {
    public AbstractC35581iK A00;
    public final Context A01;
    public final AnonymousClass1s8 A02;
    public final C457522x A03;
    public final AnonymousClass01H A04;
    public final List A05;
    public final Set A06;

    public AnonymousClass2UD(Context context, AnonymousClass1s8 r3, C457522x r4, AnonymousClass01H r5, List list, Set set) {
        this.A01 = context;
        this.A06 = set;
        this.A05 = list;
        this.A03 = r4;
        this.A04 = r5;
        this.A02 = r3;
        A07(true);
    }

    @Override // X.AnonymousClass02M
    public long A00(int i) {
        AbstractC35611iN A0E = A0E(i);
        if (A0E == null) {
            return 0;
        }
        Uri AAE = A0E.AAE();
        StringBuilder A0h = C12960it.A0h();
        A0h.append(AAE);
        return (long) C12960it.A0d("-gallery_thumb", A0h).hashCode();
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void A0A(AnonymousClass03U r3) {
        AnonymousClass2T1 r1 = ((View$OnClickListenerC55232i0) r3).A03;
        r1.setImageDrawable(null);
        ((AnonymousClass2T3) r1).A00 = null;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        int count;
        AbstractC35581iK r0 = this.A00;
        int i = 0;
        if (r0 == null) {
            count = 0;
        } else {
            count = r0.getCount();
        }
        if (((Boolean) this.A04.get()).booleanValue()) {
            i = this.A05.size();
        }
        return count + i;
    }

    public final AbstractC35611iN A0E(int i) {
        AbstractC35581iK r1;
        if (this.A00 == null) {
            return null;
        }
        if (((Boolean) this.A04.get()).booleanValue()) {
            List list = this.A05;
            if (i < list.size()) {
                return (AbstractC35611iN) list.get(i);
            }
            r1 = this.A00;
            i -= list.size();
        } else {
            r1 = this.A00;
        }
        return r1.AEC(i);
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r6, int i) {
        boolean z;
        View$OnClickListenerC55232i0 r62 = (View$OnClickListenerC55232i0) r6;
        AbstractC35611iN A0E = A0E(i);
        AnonymousClass2T1 r3 = r62.A03;
        r3.setMediaItem(A0E);
        ((AnonymousClass2T3) r3).A00 = null;
        r3.setId(R.id.thumb);
        C457522x r2 = r62.A04;
        r2.A01((AnonymousClass23D) r3.getTag());
        if (A0E != null) {
            r3.setScaleType(ImageView.ScaleType.CENTER_CROP);
            AnonymousClass028.A0k(r3, A0E.AAE().toString());
            AnonymousClass3X2 r1 = new AnonymousClass3X2(r62, A0E);
            r3.setTag(r1);
            r2.A02(r1, new AnonymousClass3XA(r62, A0E, r1));
            z = r62.A05.contains(r3.getUri());
        } else {
            r3.setScaleType(ImageView.ScaleType.CENTER);
            AnonymousClass028.A0k(r3, null);
            r3.setBackgroundColor(r62.A00);
            r3.setImageDrawable(null);
            z = false;
        }
        r3.setChecked(z);
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        AnonymousClass442 r4 = new AnonymousClass442(this.A01, this);
        if (C28391Mz.A02()) {
            r4.setSelector(null);
        }
        Set set = this.A06;
        return new View$OnClickListenerC55232i0(this.A02, r4, this.A03, set);
    }
}
