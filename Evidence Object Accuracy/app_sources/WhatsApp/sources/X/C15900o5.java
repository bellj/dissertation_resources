package X;

import android.content.Context;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/* renamed from: X.0o5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15900o5 {
    public final C16590pI A00;

    public C15900o5(C16590pI r1) {
        this.A00 = r1;
    }

    public C16560pF A00() {
        byte[] bArr;
        byte[] bArr2;
        Context context = this.A00.A00;
        File file = new File(context.getFilesDir(), "password_data.key");
        if (!file.exists()) {
            byte[] A0G = C003501n.A0G(new File(context.getFilesDir(), "password_hash.key"));
            if (A0G != null) {
                boolean z = false;
                if (A0G.length == 64) {
                    z = true;
                }
                AnonymousClass009.A0E(z);
            } else {
                A0G = null;
            }
            byte[] A0G2 = C003501n.A0G(new File(context.getFilesDir(), "password_hash_salt.key"));
            if (A0G2 != null) {
                boolean z2 = false;
                if (A0G2.length == 64) {
                    z2 = true;
                }
                AnonymousClass009.A0E(z2);
            } else {
                A0G2 = null;
            }
            if (A0G == null || A0G2 == null) {
                return null;
            }
            return new C16560pF(A0G, A0G2, 100000);
        }
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file));
            try {
                if (1 == objectInputStream.readInt() && (bArr = (byte[]) objectInputStream.readObject()) != null && 64 == bArr.length && (bArr2 = (byte[]) objectInputStream.readObject()) != null && 64 == bArr2.length) {
                    C16560pF r0 = new C16560pF(bArr, bArr2, objectInputStream.readInt());
                    objectInputStream.close();
                    return r0;
                }
                objectInputStream.close();
                return null;
            } catch (Throwable th) {
                try {
                    objectInputStream.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } catch (IOException | ClassNotFoundException e) {
            Log.e("EncBackupLocalStorage/failed to load password data", e);
            return null;
        }
    }

    public void A01(C16560pF r7) {
        byte[] bArr = r7.A01;
        boolean z = false;
        boolean z2 = false;
        if (bArr.length == 64) {
            z2 = true;
        }
        AnonymousClass009.A0E(z2);
        byte[] bArr2 = r7.A02;
        if (bArr2.length == 64) {
            z = true;
        }
        AnonymousClass009.A0E(z);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(new File(this.A00.A00.getFilesDir(), "password_data.key")));
        try {
            objectOutputStream.writeInt(1);
            objectOutputStream.writeObject(bArr);
            objectOutputStream.writeObject(bArr2);
            objectOutputStream.writeInt(r7.A00);
            objectOutputStream.close();
        } catch (Throwable th) {
            try {
                objectOutputStream.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A02(byte[] bArr) {
        boolean z = false;
        if (bArr.length == 32) {
            z = true;
        }
        AnonymousClass009.A0E(z);
        C003501n.A08(new File(this.A00.A00.getFilesDir(), "encrypted_backup.key"), bArr);
    }

    public byte[] A03() {
        byte[] A0G = C003501n.A0G(new File(this.A00.A00.getFilesDir(), "encrypted_backup.key"));
        if (A0G == null) {
            return null;
        }
        boolean z = false;
        if (A0G.length == 32) {
            z = true;
        }
        AnonymousClass009.A0E(z);
        return A0G;
    }
}
