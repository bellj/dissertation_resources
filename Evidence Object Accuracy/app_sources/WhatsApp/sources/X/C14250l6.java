package X;

/* renamed from: X.0l6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14250l6 {
    public final C14230l4 A00;

    public C14250l6(C14230l4 r1) {
        this.A00 = r1;
    }

    public static Object A00(C14230l4 r4, C14220l3 r5, AbstractC14200l1 r6) {
        Object obj;
        C14270l8 A03;
        C94614cC.A01("LispyEvaluation");
        C88824Hg.A04.incrementAndGet();
        C14260l7 r3 = r4.A00;
        if (!(r3 == null || (A03 = AnonymousClass3JV.A03(r3)) == null)) {
            AnonymousClass3J3.A02("Tree operations are only supported from the UI Thread");
            A03.A00++;
        }
        try {
            try {
                obj = AnonymousClass3AG.A00(r5, r6, r4);
            } catch (C87444Bn e) {
                C28691Op.A01("BloksInterpreter", "Exception while evaluating Lispy Script", e);
                obj = null;
            }
            return obj;
        } finally {
            C94614cC.A00();
            if (r3 != null) {
                AnonymousClass3JV.A06(r3);
            }
        }
    }

    public void A01(C14220l3 r2, AbstractC14200l1 r3) {
        A00(this.A00, r2, r3);
    }
}
