package X;

import java.util.Iterator;
import java.util.List;

/* renamed from: X.2un  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59472un extends C37171lc {
    public final List A00 = C12960it.A0l();

    public C59472un(C68553Vv r5, List list) {
        super(AnonymousClass39o.A0S);
        Iterator it = list.iterator();
        while (it.hasNext()) {
            this.A00.add(new AnonymousClass40I((AnonymousClass2x6) it.next(), r5));
        }
        this.A00.add(new AnonymousClass40H(r5));
    }
}
