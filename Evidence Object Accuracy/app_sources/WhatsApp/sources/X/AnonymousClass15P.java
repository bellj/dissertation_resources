package X;

import android.content.SharedPreferences;

/* renamed from: X.15P  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass15P {
    public SharedPreferences A00;
    public final C16630pM A01;

    public AnonymousClass15P(C16630pM r1) {
        this.A01 = r1;
    }

    public final synchronized SharedPreferences A00() {
        SharedPreferences sharedPreferences;
        sharedPreferences = this.A00;
        if (sharedPreferences == null) {
            sharedPreferences = this.A01.A01("disappearing_mode_prefs");
            this.A00 = sharedPreferences;
        }
        return sharedPreferences;
    }
}
