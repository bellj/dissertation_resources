package X;

import java.util.List;
import java.util.UUID;

/* renamed from: X.6Jp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class RunnableC135736Jp implements Runnable {
    public final /* synthetic */ AnonymousClass662 A00;
    public final /* synthetic */ List A01;
    public final /* synthetic */ UUID A02;
    public final /* synthetic */ boolean A03 = true;

    public RunnableC135736Jp(AnonymousClass662 r2, List list, UUID uuid) {
        this.A00 = r2;
        this.A01 = list;
        this.A02 = uuid;
    }

    @Override // java.lang.Runnable
    public void run() {
        List list = this.A01;
        if (0 < list.size()) {
            list.get(0);
            throw C12980iv.A0n("onError");
        } else if (this.A03) {
            AnonymousClass662 r2 = this.A00;
            r2.A0c.A02(this.A02);
            r2.A8z(null);
        }
    }
}
