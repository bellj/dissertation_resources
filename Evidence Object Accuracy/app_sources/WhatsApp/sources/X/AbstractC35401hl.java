package X;

import android.graphics.Bitmap;

/* renamed from: X.1hl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public interface AbstractC35401hl {
    void A7N();

    void A9X();

    void A9m(boolean z);

    void A9r(AnonymousClass4NS v, AnonymousClass1IS v2, String str, String str2, Bitmap[] bitmapArr, int i);

    int AC4();

    AnonymousClass1IS AC5();

    boolean ADU();

    boolean ADc();

    void AYz();

    void Ac0(int i);

    void AcB(AnonymousClass4NS v);

    void AcP(int i);

    void AeJ(C89114Is v, ScaleGestureDetector$OnScaleGestureListenerC52942c4 v2);
}
