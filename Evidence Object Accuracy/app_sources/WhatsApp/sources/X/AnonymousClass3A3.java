package X;

import android.content.Context;
import android.view.View;
import android.view.accessibility.AccessibilityNodeInfo;

/* renamed from: X.3A3  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3A3 {
    public static void A00(Context context, View view, AnonymousClass04Z r6, String str) {
        if (str != null) {
            char c = 65535;
            switch (str.hashCode()) {
                case -2137403731:
                    if (str.equals("Header")) {
                        c = 0;
                        break;
                    }
                    break;
                case -565577257:
                    if (str.equals("Image Button")) {
                        c = 1;
                        break;
                    }
                    break;
                case 2368538:
                    if (str.equals("Link")) {
                        c = 2;
                        break;
                    }
                    break;
                case 70760763:
                    if (str.equals("Image")) {
                        c = 3;
                        break;
                    }
                    break;
                case 109450440:
                    if (str.equals("Tab Bar")) {
                        c = 4;
                        break;
                    }
                    break;
                case 1404906583:
                    if (str.equals("Selected Button")) {
                        c = 5;
                        break;
                    }
                    break;
                case 2001146706:
                    if (str.equals("Button")) {
                        c = 6;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    r6.A0L(true);
                    if (view != null) {
                        AnonymousClass028.A0l(view, true);
                        return;
                    }
                    return;
                case 1:
                case 6:
                    r6.A02.setClassName("android.widget.Button");
                    return;
                case 2:
                    r6.A02.setClassName("android.widget.Button");
                    r6.A0F(context.getString(AnonymousClass0K4.accessibility_link_role));
                    return;
                case 3:
                    r6.A02.setClassName("android.widget.ImageView");
                    r6.A0A(C007804a.A0Y);
                    return;
                case 4:
                    r6.A02.setClassName("android.widget.TabWidget");
                    return;
                case 5:
                    AccessibilityNodeInfo accessibilityNodeInfo = r6.A02;
                    accessibilityNodeInfo.setClassName("android.widget.Button");
                    accessibilityNodeInfo.setSelected(true);
                    return;
                default:
                    return;
            }
        }
    }
}
