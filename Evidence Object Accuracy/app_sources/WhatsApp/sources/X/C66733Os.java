package X;

import com.whatsapp.audiopicker.AudioPickerActivity;

/* renamed from: X.3Os  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C66733Os implements AnonymousClass07L {
    public final /* synthetic */ AudioPickerActivity A00;

    @Override // X.AnonymousClass07L
    public boolean AUY(String str) {
        return false;
    }

    public C66733Os(AudioPickerActivity audioPickerActivity) {
        this.A00 = audioPickerActivity;
    }

    @Override // X.AnonymousClass07L
    public boolean AUX(String str) {
        AudioPickerActivity audioPickerActivity = this.A00;
        audioPickerActivity.A0M = str;
        audioPickerActivity.A0N = C32751cg.A02(((ActivityC13830kP) audioPickerActivity).A01, str);
        audioPickerActivity.A0W().A00(null, audioPickerActivity);
        return false;
    }
}
