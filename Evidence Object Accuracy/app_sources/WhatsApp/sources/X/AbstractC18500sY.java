package X;

import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteBlobTooBigException;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.database.sqlite.SQLiteOutOfMemoryException;
import android.text.TextUtils;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.whatsapp.util.Log;
import java.util.Collections;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.0sY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC18500sY {
    public final int A00;
    public final AbstractC15710nm A01;
    public final C15450nH A02;
    public final C14950mJ A03;
    public final C20820wN A04;
    public final C16490p7 A05;
    public final C21390xL A06;
    public final C21670xn A07;
    public final C21660xm A08;
    public final C21630xj A09;
    public final C21640xk A0A;
    public final C16120oU A0B;
    public final String A0C;

    public abstract AnonymousClass2Ez A09(Cursor cursor);

    public void A0I() {
    }

    public void A0J() {
    }

    public AbstractC18500sY(C18480sW r2, String str, int i) {
        this.A0C = str;
        this.A00 = i;
        this.A01 = r2.A00;
        this.A0B = r2.A0E;
        this.A02 = r2.A01;
        this.A03 = r2.A04;
        this.A09 = r2.A0C;
        this.A06 = r2.A08;
        this.A0A = r2.A0D;
        this.A05 = r2.A07;
        this.A08 = r2.A0B;
        this.A07 = r2.A09;
        this.A04 = r2.A06;
    }

    public static final int A00(C29001Pw r2) {
        for (AbstractC28981Pu r1 : r2.A00) {
            if (!r1.isValid()) {
                Integer A8s = r1.A8s();
                if (A8s != null) {
                    return A8s.intValue();
                }
                return 0;
            }
        }
        return 0;
    }

    public static void A01(AbstractC15710nm r3, String str, String str2, Throwable th) {
        StringBuilder sb = new StringBuilder("migration-failed-");
        sb.append(str2);
        sb.append("-");
        sb.append(str);
        String obj = sb.toString();
        Log.e(obj, th);
        r3.AaV(obj, th.toString(), true);
    }

    public int A02() {
        if ((this instanceof C19300tt) || (this instanceof C19230tm) || (this instanceof C19310tu)) {
            return 0;
        }
        if (this instanceof AbstractC19250to) {
            return 50;
        }
        if (this instanceof AnonymousClass0t2) {
            return 0;
        }
        if (this instanceof C18490sX) {
            return 10;
        }
        if (this instanceof C19070tW) {
            return 256;
        }
        if (this instanceof C19210tk) {
            return 128;
        }
        if (this instanceof C19060tV) {
            return EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
        }
        if (this instanceof C19120tb) {
            return 32;
        }
        if (this instanceof C19200tj) {
            return EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
        }
        if (this instanceof C19140td) {
            return EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
        }
        if (this instanceof C19050tU) {
            return 25;
        }
        if (this instanceof C19100tZ) {
            return 32;
        }
        if (this instanceof C19220tl) {
            return EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
        }
        if ((this instanceof C19170tg) || (this instanceof C18690sr)) {
            return 200;
        }
        if (this instanceof C19280tr) {
            return 512;
        }
        if (this instanceof C19260tp) {
            return 1;
        }
        if (this instanceof C19270tq) {
            return 512;
        }
        if (this instanceof C19090tY) {
            return 25;
        }
        if (this instanceof C19110ta) {
            return 10;
        }
        if (this instanceof C19130tc) {
            return EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
        }
        if (!(this instanceof C19180th)) {
            if ((this instanceof C19040tT) || (this instanceof C19160tf) || (this instanceof C19030tS)) {
                return EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
            }
            if (this instanceof C19000tP) {
                return EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
            }
            if (this instanceof C19290ts) {
                return 512;
            }
            if (this instanceof C19080tX) {
                return 128;
            }
            if (this instanceof C19020tR) {
                return 32;
            }
            if (!(this instanceof C19150te)) {
                return 0;
            }
            return EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
        } else if (!(((C19180th) this) instanceof C19190ti)) {
            return EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
        } else {
            return 256;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:65:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:80:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A03() {
        /*
            r6 = this;
            boolean r0 = r6.A0P()
            if (r0 != 0) goto L_0x00c3
            boolean r0 = r6.A0Q()
            if (r0 != 0) goto L_0x00c3
            r5 = r6
            boolean r0 = r6 instanceof X.C19300tt
            if (r0 != 0) goto L_0x009e
            boolean r0 = r6 instanceof X.C19230tm
            if (r0 != 0) goto L_0x00c3
            boolean r0 = r6 instanceof X.C19310tu
            if (r0 != 0) goto L_0x00a5
            boolean r0 = r6 instanceof X.AbstractC19250to
            if (r0 != 0) goto L_0x006c
            boolean r0 = r6 instanceof X.AnonymousClass0t2
            if (r0 != 0) goto L_0x00c3
            boolean r0 = r6 instanceof X.C18490sX
            if (r0 != 0) goto L_0x00c3
            boolean r0 = r6 instanceof X.C19050tU
            if (r0 != 0) goto L_0x00c3
            boolean r0 = r6 instanceof X.C19170tg
            if (r0 != 0) goto L_0x00c3
            boolean r0 = r6 instanceof X.C18690sr
            if (r0 != 0) goto L_0x00c3
            boolean r0 = r6 instanceof X.C19280tr
            if (r0 != 0) goto L_0x0065
            boolean r0 = r6 instanceof X.C19260tp
            if (r0 != 0) goto L_0x009c
            boolean r0 = r6 instanceof X.C19270tq
            if (r0 != 0) goto L_0x007d
            boolean r0 = r6 instanceof X.C19110ta
            if (r0 != 0) goto L_0x00c3
            boolean r0 = r6 instanceof X.C19180th
            if (r0 != 0) goto L_0x005a
            boolean r0 = r6 instanceof X.C19290ts
            if (r0 != 0) goto L_0x00b6
            boolean r0 = r6 instanceof X.C19020tR
            if (r0 != 0) goto L_0x00c3
            boolean r0 = r6 instanceof X.C18760sy
            if (r0 != 0) goto L_0x00c3
            X.0xk r1 = r6.A0A
            java.lang.String r0 = r6.A0C
        L_0x0055:
            int r1 = r1.A01(r0)
        L_0x0059:
            return r1
        L_0x005a:
            X.0th r5 = (X.C19180th) r5
            boolean r0 = r5 instanceof X.C19190ti
            if (r0 == 0) goto L_0x00c3
            X.0xk r1 = r5.A0A
            java.lang.String r0 = r5.A0C
            goto L_0x0055
        L_0x0065:
            X.0tr r5 = (X.C19280tr) r5
            X.0m9 r1 = r5.A00
            r0 = 438(0x1b6, float:6.14E-43)
            goto L_0x00bc
        L_0x006c:
            X.0xk r1 = r6.A0A
            java.lang.String r0 = r6.A0C
            org.json.JSONObject r1 = r1.A02(r0)
            if (r1 == 0) goto L_0x009c
            java.lang.String r0 = "enabled"
            boolean r0 = r1.optBoolean(r0)
            goto L_0x0099
        L_0x007d:
            X.0tq r5 = (X.C19270tq) r5
            X.0xL r3 = r5.A06
            java.lang.String r2 = r5.A0F()
            r0 = -1
            long r3 = r3.A01(r2, r0)
            r1 = 0
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x009c
            X.0m9 r1 = r5.A00
            r0 = 438(0x1b6, float:6.14E-43)
            boolean r0 = r1.A07(r0)
        L_0x0099:
            r1 = 3
            if (r0 != 0) goto L_0x0059
        L_0x009c:
            r1 = 2
            return r1
        L_0x009e:
            X.0tt r5 = (X.C19300tt) r5
            X.0m9 r1 = r5.A00
            r0 = 626(0x272, float:8.77E-43)
            goto L_0x00ab
        L_0x00a5:
            X.0tu r5 = (X.C19310tu) r5
            X.0m9 r1 = r5.A00
            r0 = 627(0x273, float:8.79E-43)
        L_0x00ab:
            java.lang.String r1 = r1.A03(r0)
            java.lang.String r0 = "migrate"
            boolean r0 = r0.equals(r1)
            goto L_0x00c0
        L_0x00b6:
            X.0ts r5 = (X.C19290ts) r5
            X.0m9 r1 = r5.A00
            r0 = 473(0x1d9, float:6.63E-43)
        L_0x00bc:
            boolean r0 = r1.A07(r0)
        L_0x00c0:
            r1 = 2
            if (r0 == 0) goto L_0x0059
        L_0x00c3:
            r1 = 3
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC18500sY.A03():int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:53:0x009e A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int A04() {
        /*
            r7 = this;
            boolean r0 = r7.A0R()
            if (r0 == 0) goto L_0x0011
            boolean r1 = r7.A0S()
            r0 = 11
            if (r1 != 0) goto L_0x0010
            r0 = 13
        L_0x0010:
            return r0
        L_0x0011:
            int r0 = r7.A03()
            r2 = 4
            r6 = 2
            r1 = 1
            if (r0 != r6) goto L_0x0028
            boolean r0 = r7.A0O()
            if (r0 != 0) goto L_0x0089
            boolean r0 = r7.A0N()
            if (r0 == 0) goto L_0x009e
            r0 = 0
            return r0
        L_0x0028:
            int r0 = r7.A03()
            r5 = 3
            if (r0 != r5) goto L_0x008a
            boolean r0 = r7.A0O()
            if (r0 != 0) goto L_0x0089
            boolean r0 = r7 instanceof X.C19230tm
            if (r0 != 0) goto L_0x0048
            boolean r0 = r7 instanceof X.AnonymousClass0t2
            if (r0 != 0) goto L_0x0048
            long r3 = r7.A07()
            r1 = 3
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x0048
            return r6
        L_0x0048:
            boolean r0 = r7.A0S()
            if (r0 != 0) goto L_0x0051
            r0 = 12
            return r0
        L_0x0051:
            boolean r0 = r7.A0M()
            if (r0 == 0) goto L_0x0086
            boolean r0 = r7.A0N()
            if (r0 != 0) goto L_0x0077
            long r3 = r7.A05()
            r1 = 0
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x0077
            X.0wN r0 = r7.A04
            java.lang.Long r0 = r0.A00()
            if (r0 == 0) goto L_0x0077
            long r1 = r0.longValue()
            int r0 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r0 > 0) goto L_0x0086
        L_0x0077:
            boolean r0 = r7.A0T()
            if (r0 == 0) goto L_0x0086
            boolean r0 = r7.A0N()
            if (r0 == 0) goto L_0x0084
            return r5
        L_0x0084:
            r0 = 5
            return r0
        L_0x0086:
            r0 = 10
            return r0
        L_0x0089:
            return r1
        L_0x008a:
            int r0 = r7.A03()
            if (r0 != r1) goto L_0x009f
            boolean r0 = r7.A0O()
            if (r0 != 0) goto L_0x009c
            boolean r0 = r7.A0N()
            if (r0 == 0) goto L_0x009e
        L_0x009c:
            r0 = 7
            return r0
        L_0x009e:
            return r2
        L_0x009f:
            r0 = 9
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC18500sY.A04():int");
    }

    public long A05() {
        if ((this instanceof C19300tt) || (this instanceof C19310tu) || (this instanceof C19070tW) || (this instanceof C19210tk) || (this instanceof C19060tV) || (this instanceof C19120tb) || (this instanceof C19200tj) || (this instanceof C19140td) || (this instanceof C19100tZ) || (this instanceof C19220tl) || (this instanceof C19090tY) || (this instanceof C19130tc) || (this instanceof C19040tT) || (this instanceof C19160tf) || (this instanceof C19030tS) || (this instanceof C19000tP) || (this instanceof C19080tX) || (this instanceof C19150te)) {
            return (long) this.A0A.A00.A02(261);
        }
        return 0;
    }

    public long A06() {
        C21640xk r0 = this.A0A;
        String str = this.A0C;
        String trim = r0.A00.A03(402).toLowerCase(Locale.US).trim();
        if (TextUtils.isEmpty(trim)) {
            return 0;
        }
        for (String str2 : trim.split(";")) {
            String trim2 = str2.trim();
            if (trim2.startsWith(str)) {
                String[] split = trim2.split(":");
                if (split.length == 2) {
                    String str3 = split[0];
                    long A01 = C28421Nd.A01(split[1], -1);
                    if (str.equals(str3) && A01 >= 0) {
                        if (A01 != -1) {
                            return A01;
                        }
                        return 10485760;
                    }
                } else {
                    continue;
                }
            }
        }
        return 10485760;
    }

    public long A07() {
        long j;
        if (!(this instanceof C19180th)) {
            j = 0;
        } else {
            j = 1;
        }
        C21390xL r5 = this.A06;
        if (j == r5.A01(A0E(), 0)) {
            return r5.A01(A0D(), 0);
        }
        return 0;
    }

    public Cursor A08(C16310on r13, int i, long j) {
        long j2;
        if (!(this instanceof AbstractC19250to)) {
            C16330op r4 = r13.A03;
            String A0C = A0C();
            String[] strArr = {String.valueOf(j), String.valueOf(i)};
            StringBuilder sb = new StringBuilder("MIGRATION_GET_QUERY_FOR_");
            sb.append(this.A0C);
            sb.toString();
            return r4.A09(A0C, strArr);
        }
        AbstractC19250to r8 = (AbstractC19250to) this;
        C16330op r7 = r13.A03;
        StringBuilder sb2 = new StringBuilder();
        sb2.append(r8.A0C());
        sb2.append(" OFFSET ?");
        String obj = sb2.toString();
        String[] strArr2 = new String[3];
        strArr2[0] = String.valueOf(j);
        strArr2[1] = String.valueOf(i);
        long j3 = (long) 50;
        AnonymousClass2FD r10 = r8.A03;
        int i2 = r10.A00;
        if (i2 < 1) {
            j2 = 0;
        } else {
            int nextInt = r10.A02.nextInt(i2);
            int i3 = r10.A01;
            int i4 = ((i2 - i3) - 1) + nextInt;
            if (i3 <= -1) {
                i4 = nextInt;
            }
            r10.A01 = nextInt;
            j2 = (long) i4;
        }
        strArr2[2] = String.valueOf(j3 * j2);
        StringBuilder sb3 = new StringBuilder("MIGRATION_GET_QUERY_FOR_");
        sb3.append(r8.A0C);
        sb3.toString();
        return r7.A09(obj, strArr2);
    }

    public String A0A() {
        if (this instanceof C19070tW) {
            return "new_vcards_ready";
        }
        if (this instanceof C19210tk) {
            return "thumbnail_ready";
        }
        if (this instanceof C19060tV) {
            return "text_ready";
        }
        if (this instanceof C19120tb) {
            return "system_message_ready";
        }
        if (this instanceof C19200tj) {
            return "send_count_ready";
        }
        if (this instanceof C19140td) {
            return "revoked_ready";
        }
        if (this instanceof C19100tZ) {
            return "quoted_message_ready";
        }
        if (this instanceof C19220tl) {
            return "new_pay_transaction_ready";
        }
        if (this instanceof C19280tr) {
            return "quoted_order_message_v2_migration_ready";
        }
        if (this instanceof C19260tp) {
            return "legacy_quoted_order_message_ready";
        }
        if (this instanceof C19270tq) {
            return "quoted_order_message_ready";
        }
        if (this instanceof C19130tc) {
            return "mention_message_ready";
        }
        if (this instanceof C19040tT) {
            return "main_message_ready";
        }
        if (this instanceof C19160tf) {
            return "location_ready";
        }
        if (this instanceof C19030tS) {
            return "links_ready";
        }
        if (this instanceof C19000tP) {
            return "labeled_jids_ready";
        }
        if (this instanceof C19290ts) {
            return "quoted_ui_elements_reply_message_migration_ready";
        }
        if (this instanceof C19080tX) {
            return "future_ready";
        }
        if (this instanceof C19150te) {
            return "frequent_ready";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(this.A0C);
        sb.append("_complete");
        return sb.toString();
    }

    public String A0B() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.A0C);
        sb.append("_in_progress");
        return sb.toString();
    }

    public String A0C() {
        if (this instanceof C19240tn) {
            return C38411o3.A00;
        }
        if ((this instanceof C19300tt) || (this instanceof C19230tm) || (this instanceof C19310tu) || (this instanceof AnonymousClass0t2)) {
            return "";
        }
        if (this instanceof C18490sX) {
            return "SELECT _id, raw_string, type FROM jid WHERE _id > ?  ORDER BY _id ASC LIMIT ? ";
        }
        if (this instanceof C19070tW) {
            return "SELECT _id, media_wa_type, data, raw_data FROM messages WHERE _id > ?  AND media_wa_type IN (4, 14) ORDER BY _id ASC LIMIT ?";
        }
        if (this instanceof C19210tk) {
            return "SELECT messages._id, message_thumbnails.thumbnail, message_thumbnails.key_remote_jid, message_thumbnails.key_from_me, message_thumbnails.key_id  FROM messages, message_thumbnails  WHERE messages._id > ? AND message_thumbnails.key_remote_jid = messages.key_remote_jid AND message_thumbnails.key_from_me = messages.key_from_me AND message_thumbnails.key_id = messages.key_id ORDER BY messages._id ASC LIMIT ?";
        }
        if (this instanceof C19060tV) {
            return "SELECT _id, key_remote_jid, key_from_me, key_id, media_name, media_caption, media_url, thumb_image, preview_type, status, media_duration  FROM messages WHERE _id > ? AND media_wa_type IN (0, 27) ORDER BY _id ASC LIMIT ?";
        }
        if (this instanceof C19120tb) {
            return AnonymousClass2F2.A04;
        }
        if (this instanceof C19200tj) {
            return "SELECT _id, send_count FROM messages WHERE _id > ? ORDER BY _id ASC LIMIT ?";
        }
        if (this instanceof C19140td) {
            return "SELECT _id, media_name, media_caption FROM messages WHERE _id > ? AND media_wa_type IN (15,64) ORDER BY _id ASC LIMIT ?";
        }
        if (this instanceof C19050tU) {
            return "SELECT _id, key_id, key_remote_jid, key_from_me, remote_resource, receipt_device_timestamp, read_device_timestamp, played_device_timestamp FROM messages WHERE _id > ?  AND (status IS NULL OR status!=6) ORDER BY _id ASC LIMIT ?";
        }
        if (this instanceof C19100tZ) {
            return AnonymousClass2F2.A03;
        }
        if (this instanceof C19220tl) {
            return "SELECT pay_transactions.rowid AS _id, pay_transactions.key_remote_jid, (CASE WHEN pay_transactions.key_remote_jid IS NOT NULL THEN pay_transactions.key_id else NULL END) AS key_id,messages.rowid AS message_row_id, (CASE WHEN pay_transactions.key_remote_jid IS NULL THEN pay_transactions.key_id else NULL END) AS interop_id, id, pay_transactions.timestamp AS timestamp, pay_transactions.status AS status,error_code, sender, receiver, type, currency, amount_1000, credential_id, methods, bank_transaction_id, metadata, init_timestamp, request_key_id, country, version, future_data, service_id, background_id, purchase_initiator FROM pay_transactions LEFT JOIN messages ON pay_transactions.key_id = messages.key_id WHERE pay_transactions.rowid>? LIMIT ?";
        }
        if (this instanceof C19170tg) {
            return "SELECT _id, gjid, jid, admin, pending, sent_sender_key FROM group_participants WHERE _id > ?  ORDER BY _id ASC LIMIT ?";
        }
        if (this instanceof C18690sr) {
            return "SELECT group_participant_user._id, group_jid_row_id FROM group_participant_user LEFT JOIN jid ON group_jid_row_id = jid._id WHERE group_participant_user._id > ?  AND type = 3 GROUP BY group_jid_row_id ORDER BY group_participant_user._id LIMIT ? ";
        }
        if (this instanceof C19280tr) {
            return "SELECT message_row_id, order_id, thumbnail, order_title, item_count, status, surface, message, seller_jid, token, currency_code,total_amount_1000 FROM message_quoted_order WHERE message_row_id > ? ORDER BY message_row_id LIMIT ?";
        }
        if (this instanceof C19260tp) {
            return "SELECT 1";
        }
        if (this instanceof C19270tq) {
            return "SELECT messages._id AS _id, quoted_message_order.order_id AS order_id, quoted_message_order.thumbnail AS thumbnail, quoted_message_order.order_title AS order_title, quoted_message_order.item_count AS item_count, quoted_message_order.status AS status, quoted_message_order.surface AS surface, quoted_message_order.message AS message, quoted_message_order.seller_jid AS seller_jid, quoted_message_order.token AS token, quoted_message_order.currency_code AS currency_code, quoted_message_order.total_amount_1000 AS total_amount_1000 FROM (SELECT messages._id, messages.quoted_row_id FROM messages WHERE messages.quoted_row_id != 0 AND messages.media_wa_type = 0 AND messages.timestamp > 1598925600000) messages INNER JOIN (SELECT messages_quotes._id FROM messages_quotes WHERE messages_quotes.media_wa_type = 44) messages_quotes ON messages_quotes._id = messages.quoted_row_id INNER JOIN quoted_message_order ON quoted_message_order.message_row_id = messages.quoted_row_id WHERE messages._id > ? ORDER BY messages._id LIMIT ?";
        }
        if (this instanceof C19090tY) {
            return AnonymousClass2F2.A02;
        }
        if (this instanceof C19110ta) {
            return "SELECT _id, key_remote_jid, key_from_me, key_id, remote_resource, status, receipt_device_timestamp, read_device_timestamp, played_device_timestamp, participant_hash FROM messages WHERE _id > ? ORDER BY _id ASC LIMIT ?";
        }
        if (this instanceof C19130tc) {
            return "SELECT _id, mentioned_jids FROM messages WHERE _id > ?  ORDER BY _id ASC LIMIT ?";
        }
        if (this instanceof C19180th) {
            return "SELECT _id, thumb_image, media_wa_type, key_remote_jid, multicast_id, media_url, media_mime_type, media_size, media_name, media_hash, media_duration, media_enc_hash, timestamp  FROM messages WHERE _id > ?  ORDER BY _id ASC LIMIT ?";
        }
        if (this instanceof C19040tT) {
            return AnonymousClass2F2.A01;
        }
        if (this instanceof C19160tf) {
            return "SELECT _id, key_remote_jid, media_wa_type, media_name, media_url, media_duration, media_size, latitude, longitude, thumb_image  FROM messages WHERE _id > ? AND media_wa_type IN (16, 5, 30) ORDER BY _id ASC LIMIT ?";
        }
        if (this instanceof C19030tS) {
            return "SELECT _id, chat_row_id, data, media_caption, message_type FROM available_message_view WHERE _id > ? ORDER BY _id ASC LIMIT ?";
        }
        if (this instanceof C19000tP) {
            return "SELECT _id, label_id, jid FROM labeled_jids WHERE _id > ? ORDER BY _id ASC LIMIT ?";
        }
        if (this instanceof C19290ts) {
            return "SELECT message_row_id, element_type, reply_values, reply_description FROM message_quoted_ui_elements_reply WHERE message_row_id > ? ORDER BY message_row_id LIMIT ?";
        }
        if (this instanceof C19080tX) {
            return "SELECT _id, media_duration, raw_data, future_message_type  FROM messages WHERE _id > ?   AND media_wa_type IN (12) ORDER BY _id ASC LIMIT ?";
        }
        if (this instanceof C19020tR) {
            return AnonymousClass2F2.A00;
        }
        if (this instanceof C19150te) {
            return "SELECT _id, jid, type, message_count FROM frequents WHERE _id > ? ORDER BY _id ASC LIMIT ?";
        }
        throw new UnsupportedOperationException();
    }

    public String A0D() {
        if (this instanceof C19240tn) {
            return "message_main_verification_retry_count";
        }
        if (this instanceof C19300tt) {
            return "rename_deprecated_tables_retry_count";
        }
        if (this instanceof C19230tm) {
            return "migration_jid_store_retry_count";
        }
        if (this instanceof C19310tu) {
            return "drop_deprecated_tables_retry_count";
        }
        if (this instanceof AnonymousClass0t2) {
            return "migration_chat_store_retry_count";
        }
        if (this instanceof C18490sX) {
            return "migration_blank_me_jid_retry";
        }
        if (this instanceof C19070tW) {
            return "migration_vcard_retry";
        }
        if (this instanceof C19210tk) {
            return "migration_message_thumbnail_retry";
        }
        if (this instanceof C19060tV) {
            return "migration_message_text_retry";
        }
        if (this instanceof C19120tb) {
            return "migration_message_system_retry";
        }
        if (this instanceof C19200tj) {
            return "migration_message_send_count_retry";
        }
        if (this instanceof C19140td) {
            return "migration_message_revoked_retry";
        }
        if (this instanceof C19050tU) {
            return "migration_receipt_retry";
        }
        if (this instanceof C19100tZ) {
            return "migration_message_quoted_retry";
        }
        if (this instanceof C19220tl) {
            return "migration_pay_transaction_retry";
        }
        if (this instanceof C19170tg) {
            return "migration_participant_user_retry";
        }
        if (this instanceof C18690sr) {
            return "migration_broadcast_me_jid_retry";
        }
        if (this instanceof C19280tr) {
            return "quoted_order_message_v2_retry_count";
        }
        if (this instanceof C19260tp) {
            return "legacy_quoted_order_message_retry_count";
        }
        if (this instanceof C19270tq) {
            return "quoted_order_message_retry_count";
        }
        if (this instanceof C19090tY) {
            return "migration_missed_calls_log_retry";
        }
        if (this instanceof C19110ta) {
            return "migration_receipt_device_retry";
        }
        if (this instanceof C19130tc) {
            return "migration_message_mention_retry";
        }
        if (this instanceof C19180th) {
            return !(((C19180th) this) instanceof C19190ti) ? "migration_message_media_retry" : "migration_message_media_fixer_retry";
        }
        if (this instanceof C19040tT) {
            return "migration_message_main_retry";
        }
        if (this instanceof C19160tf) {
            return "migration_message_location_retry";
        }
        if (this instanceof C19030tS) {
            return "migration_link_retry";
        }
        if (this instanceof C19000tP) {
            return "migration_labeled_jid_retry";
        }
        if (this instanceof C19290ts) {
            return "quoted_ui_elements_reply_message_retry_count";
        }
        if (this instanceof C19080tX) {
            return "migration_message_future_retry";
        }
        if (this instanceof C19020tR) {
            return "migration_fts_retry";
        }
        if (!(this instanceof C19150te)) {
            return "call_log_retry_count";
        }
        return "migration_frequent_retry";
    }

    public String A0E() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.A0C);
        sb.append("_retry_revision");
        return sb.toString();
    }

    public String A0F() {
        if (this instanceof C19240tn) {
            return "message_main_verification_start_index";
        }
        if (this instanceof C19300tt) {
            return "rename_deprecated_tables_start_index";
        }
        if (this instanceof C19230tm) {
            return "migration_jid_store_start_index";
        }
        if (this instanceof C19310tu) {
            return "drop_deprecated_tables_start_index";
        }
        if (this instanceof AnonymousClass0t2) {
            return "migration_chat_store_start_index";
        }
        if (this instanceof C18490sX) {
            return "migration_blank_me_jid_index";
        }
        if (this instanceof C19070tW) {
            return "migration_vcard_index";
        }
        if (this instanceof C19210tk) {
            return "migration_message_thumbnail_index";
        }
        if (this instanceof C19060tV) {
            return "migration_message_text_index";
        }
        if (this instanceof C19120tb) {
            return "migration_message_system_index";
        }
        if (this instanceof C19200tj) {
            return "migration_message_send_count_index";
        }
        if (this instanceof C19140td) {
            return "migration_message_revoked_index";
        }
        if (this instanceof C19050tU) {
            return "migration_receipt_index";
        }
        if (this instanceof C19100tZ) {
            return "migration_message_quoted_index";
        }
        if (this instanceof C19220tl) {
            return "migration_pay_transaction_index";
        }
        if (this instanceof C19170tg) {
            return "migration_participant_user_index";
        }
        if (this instanceof C18690sr) {
            return "migration_broadcast_me_jid_index";
        }
        if (this instanceof C19280tr) {
            return "quoted_order_message_v2_start_index";
        }
        if (this instanceof C19260tp) {
            return "legacy_quoted_order_message_start_index";
        }
        if (this instanceof C19270tq) {
            return "quoted_order_message_start_index";
        }
        if (this instanceof C19090tY) {
            return "migration_missed_calls_log_index";
        }
        if (this instanceof C19110ta) {
            return "migration_receipt_device_index";
        }
        if (this instanceof C19130tc) {
            return "migration_message_mention_index";
        }
        if (this instanceof C19180th) {
            return !(((C19180th) this) instanceof C19190ti) ? "migration_message_media_index" : "migration_message_media_fixer_index";
        }
        if (this instanceof C19040tT) {
            return "migration_message_main_index";
        }
        if (this instanceof C19160tf) {
            return "migration_message_location_index";
        }
        if (this instanceof C19030tS) {
            return "migration_link_index";
        }
        if (this instanceof C19000tP) {
            return "migration_labeled_jid_index";
        }
        if (this instanceof C19290ts) {
            return "quoted_ui_elements_reply_message_start_index";
        }
        if (this instanceof C19080tX) {
            return "migration_message_future_index";
        }
        if (this instanceof C19020tR) {
            return "migration_fts_index";
        }
        if (!(this instanceof C19150te)) {
            return "call_log_start_index";
        }
        return "migration_frequent_index";
    }

    public Set A0G() {
        HashSet hashSet;
        String str;
        if (this instanceof C19240tn) {
            hashSet = new HashSet();
            str = "message_main";
        } else if (this instanceof C19300tt) {
            hashSet = new HashSet();
            hashSet.add("message_main");
            hashSet.add("message_fts");
            hashSet.add("quoted_order_message_v2");
            hashSet.add("receipt_user");
            str = "receipt_device";
        } else if (this instanceof C19310tu) {
            hashSet = new HashSet();
            str = "rename_deprecated_tables";
        } else if (this instanceof AnonymousClass0t2) {
            return Collections.singleton("migration_jid_store");
        } else {
            if (!(this instanceof C18490sX) && !(this instanceof C19070tW) && !(this instanceof C19210tk)) {
                if (this instanceof C19060tV) {
                    hashSet = new HashSet();
                    hashSet.add("migration_jid_store");
                    hashSet.add("migration_chat_store");
                    str = "message_thumbnail";
                } else if (this instanceof C19120tb) {
                    hashSet = new HashSet();
                    hashSet.add("migration_jid_store");
                    hashSet.add("migration_chat_store");
                    str = "message_quoted";
                } else if (!(this instanceof C19200tj) && !(this instanceof C19140td) && !(this instanceof C19050tU) && !(this instanceof C19100tZ) && !(this instanceof C19220tl)) {
                    if (this instanceof C19170tg) {
                        hashSet = new HashSet();
                        hashSet.add("migration_jid_store");
                        hashSet.add("migration_chat_store");
                        str = "blank_me_jid";
                    } else if (this instanceof C18690sr) {
                        hashSet = new HashSet();
                        hashSet.add("migration_jid_store");
                        hashSet.add("migration_chat_store");
                        str = "participant_user";
                    } else if (this instanceof C19280tr) {
                        hashSet = new HashSet();
                        str = "quoted_order_message";
                    } else if (!(this instanceof C19090tY) && !(this instanceof C19110ta) && !(this instanceof C19130tc) && !(this instanceof C19180th)) {
                        if (this instanceof C19040tT) {
                            hashSet = new HashSet();
                            hashSet.add("migration_jid_store");
                            hashSet.add("migration_chat_store");
                            hashSet.add("message_frequent");
                            hashSet.add("message_future");
                            hashSet.add("labeled_jid");
                            hashSet.add("message_link");
                            hashSet.add("message_location");
                            hashSet.add("message_media");
                            hashSet.add("media_migration_fixer");
                            hashSet.add("message_mention");
                            hashSet.add("missed_calls");
                            hashSet.add("payment_transaction");
                            hashSet.add("message_quoted");
                            hashSet.add("message_revoked");
                            hashSet.add("message_send_count");
                            hashSet.add("message_system");
                            hashSet.add("message_text");
                            hashSet.add("message_thumbnail");
                            hashSet.add("message_vcard");
                            str = "call_log";
                        } else if (!(this instanceof C19160tf) && !(this instanceof C19030tS) && !(this instanceof C19000tP) && !(this instanceof C19080tX) && !(this instanceof C19150te)) {
                            return new HashSet();
                        }
                    }
                }
            }
            hashSet = new HashSet();
            hashSet.add("migration_jid_store");
            str = "migration_chat_store";
        }
        hashSet.add(str);
        return hashSet;
    }

    public void A0H() {
        C21390xL r2 = this.A06;
        r2.A03(A0F());
        r2.A03(A0D());
        r2.A03(A0E());
        int i = this.A00;
        if (Integer.MIN_VALUE != i) {
            r2.A03(A0B());
            r2.A04(A0A(), i);
        }
    }

    /* JADX INFO: finally extract failed */
    public final void A0K() {
        if (!(this instanceof AbstractC19010tQ)) {
            this.A01.AaV("db-rollbacks-not-supported", this.A0C, false);
            return;
        }
        AbstractC19010tQ r5 = (AbstractC19010tQ) this;
        C28181Ma r3 = new C28181Ma(false);
        r3.A03();
        try {
            r5.AND();
            C16310on A02 = this.A05.A02();
            AnonymousClass1Lx A00 = A02.A00();
            try {
                if (this.A00 != Integer.MIN_VALUE) {
                    C21390xL r4 = this.A06;
                    r4.A03(A0B());
                    int i = !(this instanceof C19030tS) ? -1 : 1;
                    String A0A = A0A();
                    if (i != -1) {
                        r4.A04(A0A, i);
                    } else {
                        r4.A03(A0A);
                    }
                }
                C21390xL r2 = this.A06;
                r2.A03(A0D());
                r2.A03(A0E());
                r2.A03(A0F());
                StringBuilder sb = new StringBuilder("migration_stats_");
                String str = this.A0C;
                sb.append(str);
                r2.A03(sb.toString());
                try {
                    r5.onRollback();
                    A00.A00();
                    A00.close();
                    A02.close();
                    r5.AM5();
                    long A01 = r3.A01();
                    AnonymousClass2FM r1 = new AnonymousClass2FM();
                    r1.A01 = str;
                    r1.A00 = Long.valueOf(A01);
                    this.A0B.A07(r1);
                } catch (Exception e) {
                    A01(this.A01, str, "rollback", e);
                    throw e;
                }
            } catch (Exception e2) {
                A01(this.A01, this.A0C, "before-rollback", e2);
                throw e2;
            }
        } catch (Throwable th) {
            r5.AM5();
            throw th;
        }
    }

    public final void A0L(AnonymousClass2F0 r7) {
        String str;
        C21390xL r4 = this.A06;
        StringBuilder sb = new StringBuilder("migration_stats_");
        sb.append(this.A0C);
        String obj = sb.toString();
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("row_processed", r7.A03);
            jSONObject.put("row_skipped", r7.A04);
            jSONObject.put("db_size_change", r7.A00);
            jSONObject.put("migration_time_spent", r7.A01);
            jSONObject.put("retry_count", r7.A02);
            str = jSONObject.toString();
        } catch (JSONException e) {
            Log.e("Failed to save migration statistics to JSON object.", e);
            str = null;
        }
        r4.A06(obj, str);
    }

    public boolean A0M() {
        long A02 = this.A03.A02();
        long A06 = A06();
        if (A02 > A06) {
            return true;
        }
        StringBuilder sb = new StringBuilder("DatabaseMigration/hasEnoughStorageForMigration/insufficient storage for migration; name=");
        sb.append(this.A0C);
        sb.append("; availableInternalPhoneStorage=");
        sb.append(A02);
        sb.append("; minInternalStorageRequired=");
        sb.append(A06);
        Log.w(sb.toString());
        return false;
    }

    public boolean A0N() {
        return this.A06.A01(A0F(), -1) >= 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:199:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:201:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0091 A[ORIG_RETURN, RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0O() {
        /*
        // Method dump skipped, instructions count: 681
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC18500sY.A0O():boolean");
    }

    public boolean A0P() {
        if ((this instanceof C19230tm) || (this instanceof AnonymousClass0t2) || (this instanceof C18490sX) || (this instanceof C19170tg)) {
            return true;
        }
        C21640xk r1 = this.A0A;
        String str = this.A0C;
        if (!r1.A02.contains(str)) {
            return C21640xk.A00(r1.A00.A03(242), str);
        }
        return true;
    }

    public boolean A0Q() {
        C21670xn r1 = this.A07;
        String str = this.A0C;
        return r1.A01.contains(str) || C21640xk.A00(r1.A00.A00.A03(404), str);
    }

    public boolean A0R() {
        if ((A0O() || A0N()) && !A0S()) {
            return true;
        }
        int i = this.A00;
        if (Integer.MIN_VALUE != i) {
            C21390xL r3 = this.A06;
            int A00 = r3.A00(A0B(), -1);
            if (A0N() && A00 == -1) {
                return true;
            }
            if (A00 != -1 && A00 != i) {
                return true;
            }
            int A002 = r3.A00(A0A(), -1);
            if (A002 != -1 && ((!(this instanceof C19030tS) || 1 < A002) && A002 != i)) {
                return true;
            }
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:5:0x000e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0S() {
        /*
            r5 = this;
            java.util.Set r0 = r5.A0G()
            java.util.Iterator r2 = r0.iterator()
        L_0x0008:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x004d
            java.lang.Object r4 = r2.next()
            java.lang.String r4 = (java.lang.String) r4
            X.0xj r0 = r5.A09
            X.0sY r1 = r0.A01(r4)
            r3 = 0
            if (r1 != 0) goto L_0x0040
            X.0nm r2 = r5.A01
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r0 = r5.A0C
            r1.append(r0)
            java.lang.String r0 = " depends on "
            r1.append(r0)
            r1.append(r4)
            java.lang.String r0 = " (missing)"
            r1.append(r0)
            java.lang.String r1 = r1.toString()
            java.lang.String r0 = "db-migration-missing-dep"
            r2.AaV(r0, r1, r3)
        L_0x003f:
            return r3
        L_0x0040:
            boolean r0 = r1.A0O()
            if (r0 == 0) goto L_0x003f
            boolean r0 = r1.A0R()
            if (r0 == 0) goto L_0x0008
            return r3
        L_0x004d:
            r0 = 1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC18500sY.A0S():boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0072, code lost:
        if (r0 == false) goto L_0x0074;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0T() {
        /*
            r3 = this;
            boolean r0 = r3 instanceof X.C19240tn
            if (r0 != 0) goto L_0x0056
            boolean r0 = r3 instanceof X.C18490sX
            if (r0 != 0) goto L_0x0041
            boolean r0 = r3 instanceof X.C19050tU
            if (r0 != 0) goto L_0x003b
            boolean r0 = r3 instanceof X.C19170tg
            if (r0 != 0) goto L_0x0035
            boolean r0 = r3 instanceof X.C18690sr
            if (r0 != 0) goto L_0x002f
            boolean r0 = r3 instanceof X.C19110ta
            if (r0 != 0) goto L_0x0047
            boolean r0 = r3 instanceof X.C19040tT
            if (r0 == 0) goto L_0x0054
            r0 = r3
            X.0tT r0 = (X.C19040tT) r0
            X.0sw r0 = r0.A01
            X.0xL r1 = r0.A04
            java.lang.String r0 = "call_log_ready"
            r2 = 0
            int r1 = r1.A00(r0, r2)
            r0 = 1
            if (r1 != r0) goto L_0x002e
            r2 = 1
        L_0x002e:
            return r2
        L_0x002f:
            r0 = r3
            X.0sr r0 = (X.C18690sr) r0
            X.0nT r0 = r0.A00
            goto L_0x004c
        L_0x0035:
            r0 = r3
            X.0tg r0 = (X.C19170tg) r0
            X.0nT r0 = r0.A00
            goto L_0x004c
        L_0x003b:
            r0 = r3
            X.0tU r0 = (X.C19050tU) r0
            X.0nT r0 = r0.A01
            goto L_0x004c
        L_0x0041:
            r0 = r3
            X.0sX r0 = (X.C18490sX) r0
            X.0nT r0 = r0.A00
            goto L_0x004c
        L_0x0047:
            r0 = r3
            X.0ta r0 = (X.C19110ta) r0
            X.0nT r0 = r0.A00
        L_0x004c:
            r0.A08()
            X.1Ih r0 = r0.A05
            r1 = 0
            if (r0 == 0) goto L_0x0055
        L_0x0054:
            r1 = 1
        L_0x0055:
            return r1
        L_0x0056:
            X.0p7 r0 = r3.A05
            X.0on r2 = r0.get()
            r0.A04()     // Catch: all -> 0x0079
            X.1To r1 = r0.A05     // Catch: all -> 0x0079
            X.0op r0 = r2.A03     // Catch: all -> 0x0079
            java.lang.Boolean r0 = r1.A06(r0)     // Catch: all -> 0x0079
            boolean r0 = r0.booleanValue()     // Catch: all -> 0x0079
            if (r0 == 0) goto L_0x0074
            boolean r0 = r1.A0E(r2)     // Catch: all -> 0x0079
            r1 = 1
            if (r0 != 0) goto L_0x0075
        L_0x0074:
            r1 = 0
        L_0x0075:
            r2.close()
            return r1
        L_0x0079:
            r0 = move-exception
            r2.close()     // Catch: all -> 0x007d
        L_0x007d:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC18500sY.A0T():boolean");
    }

    public boolean A0U(SQLException sQLException, int i) {
        if (sQLException instanceof SQLiteDatabaseCorruptException) {
            return false;
        }
        boolean z = sQLException instanceof SQLiteBlobTooBigException;
        if (i != 1) {
            if (z || (sQLException instanceof SQLiteOutOfMemoryException)) {
                return true;
            }
            return false;
        } else if (!z) {
            return false;
        } else {
            StringBuilder sb = new StringBuilder("DatabaseMigration/shouldRetryWithSmallerBatch/error; name=");
            sb.append(this.A0C);
            sb.append("; BlobTooBigException - skipping row");
            Log.e(sb.toString());
            return true;
        }
    }

    /* JADX INFO: finally extract failed */
    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    /* JADX WARNING: Removed duplicated region for block: B:224:0x072e  */
    /* JADX WARNING: Removed duplicated region for block: B:227:0x0795  */
    /* JADX WARNING: Removed duplicated region for block: B:230:0x07a9  */
    /* JADX WARNING: Removed duplicated region for block: B:232:0x07ae  */
    /* JADX WARNING: Removed duplicated region for block: B:238:0x07d5  */
    /* JADX WARNING: Removed duplicated region for block: B:239:0x07d9  */
    /* JADX WARNING: Removed duplicated region for block: B:243:0x07e7  */
    /* JADX WARNING: Removed duplicated region for block: B:342:0x01b7 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x01b9 A[Catch: Exception -> 0x0659, TryCatch #31 {Exception -> 0x0659, blocks: (B:55:0x01b1, B:58:0x01b9, B:60:0x01bf, B:61:0x01d5, B:63:0x01f4, B:65:0x01f8, B:69:0x0224, B:70:0x0229, B:71:0x0231, B:74:0x0249, B:84:0x02a1, B:85:0x02a6, B:101:0x033c, B:102:0x034c, B:103:0x0358, B:105:0x0364, B:107:0x036a, B:108:0x037b, B:111:0x039b, B:112:0x03b3, B:140:0x044e, B:143:0x0456, B:145:0x0458, B:146:0x047e, B:155:0x04da, B:184:0x05ad, B:186:0x05b3, B:188:0x05c4, B:190:0x05d6, B:192:0x05db, B:194:0x05f1, B:195:0x05ff, B:75:0x0259, B:83:0x029e, B:201:0x0638, B:147:0x0482, B:148:0x04b3, B:154:0x04d7, B:160:0x04fa, B:163:0x0509, B:165:0x050b, B:167:0x0511, B:168:0x051a, B:170:0x0520, B:173:0x054d, B:177:0x056f, B:181:0x0579, B:182:0x057a, B:183:0x05ab, B:203:0x0648, B:204:0x0649, B:86:0x02b5, B:100:0x0339, B:207:0x0653), top: B:307:0x01b1 }] */
    public boolean A0V(X.C29001Pw r36) {
        /*
        // Method dump skipped, instructions count: 2115
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC18500sY.A0V(X.1Pw):boolean");
    }
}
