package X;

import java.math.BigDecimal;
import java.math.BigInteger;

/* renamed from: X.1Ym  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30701Ym {
    public static final BigDecimal A00 = new BigDecimal("1000");

    public static BigDecimal A00(C30711Yn r3, long j) {
        int i = 0;
        while (true) {
            String str = r3.A00;
            if (i >= 3 - C30711Yn.A00(str)) {
                return new BigDecimal(new BigInteger(Long.toString(j)), C30711Yn.A00(str));
            }
            j /= 10;
            i++;
        }
    }
}
