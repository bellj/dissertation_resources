package X;

import java.text.Collator;
import java.util.Comparator;

/* renamed from: X.5Cc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C112115Cc implements Comparator {
    public final Collator A00;

    public C112115Cc(AnonymousClass018 r3) {
        Collator instance = Collator.getInstance(AnonymousClass018.A00(r3.A00));
        this.A00 = instance;
        instance.setDecomposition(1);
    }

    @Override // java.util.Comparator
    public /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
        return this.A00.compare(((AnonymousClass3WI) obj).A00, ((AnonymousClass3WI) obj2).A00);
    }
}
