package X;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.2zU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC61152zU extends AbstractC74133hN {
    public ViewGroup A00 = C12980iv.A0P(this, R.id.content);
    public ViewGroup A01 = C12980iv.A0P(this, R.id.negative_btn);
    public ViewGroup A02 = C12980iv.A0P(this, R.id.positive_btn);
    public TextView A03 = C12960it.A0J(this, R.id.header);
    public AnonymousClass018 A04;

    public abstract int getNegativeButtonTextResId();

    public abstract int getPositiveButtonIconResId();

    public abstract int getPositiveButtonTextResId();

    public AbstractC61152zU(Context context) {
        super(context);
        FrameLayout.inflate(context, R.layout.conversation_footer, this);
        C12970iu.A0L(this, R.id.positive_btn_icon).setImageResource(getPositiveButtonIconResId());
        TextView A0J = C12960it.A0J(this, R.id.positive_btn_text);
        C27531Hw.A06(A0J);
        A0J.setText(getPositiveButtonTextResId());
        TextView A0J2 = C12960it.A0J(this, R.id.negative_btn_text);
        C27531Hw.A06(A0J2);
        A0J2.setText(getNegativeButtonTextResId());
    }
}
