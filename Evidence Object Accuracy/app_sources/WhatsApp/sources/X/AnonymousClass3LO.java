package X;

import android.hardware.Camera;

/* renamed from: X.3LO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3LO implements Camera.PreviewCallback {
    public Camera.Size A00;
    public boolean A01 = true;
    public final /* synthetic */ AnonymousClass27X A02;

    public AnonymousClass3LO(AnonymousClass27X r2) {
        this.A02 = r2;
    }

    @Override // android.hardware.Camera.PreviewCallback
    public void onPreviewFrame(byte[] bArr, Camera camera) {
        boolean z;
        Camera camera2;
        if (this.A01) {
            this.A01 = false;
            this.A02.A0I.A01("cameraView1");
        }
        if (this.A00 == null) {
            try {
                this.A00 = camera.getParameters().getPreviewSize();
            } catch (RuntimeException unused) {
            }
        }
        Camera.Size size = this.A00;
        if (size != null) {
            AnonymousClass27X r5 = this.A02;
            AnonymousClass3WB r4 = r5.A0W;
            int i = size.width;
            int i2 = size.height;
            synchronized (r4) {
                AnonymousClass4Q1 r1 = r4.A00;
                if (r1.A02 == null) {
                    r1.A02 = bArr;
                    r1.A01 = i;
                    r1.A00 = i2;
                    r4.notify();
                    z = true;
                } else {
                    z = false;
                }
            }
            if (!z && (camera2 = r5.A07) != null && !r5.A0R && bArr == r5.A0S) {
                camera2.addCallbackBuffer(bArr);
            }
        }
    }
}
