package X;

import java.util.Map;

/* renamed from: X.3FB  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3FB {
    public final String A00;
    public final String A01;
    public final Map A02;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof AnonymousClass3FB) {
                AnonymousClass3FB r5 = (AnonymousClass3FB) obj;
                if (!C16700pc.A0O(this.A00, r5.A00) || !C16700pc.A0O(this.A01, r5.A01) || !C16700pc.A0O(this.A02, r5.A02)) {
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        int hashCode = this.A00.hashCode() * 31;
        String str = this.A01;
        int i = 0;
        int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
        Map map = this.A02;
        if (map != null) {
            i = map.hashCode();
        }
        return hashCode2 + i;
    }

    public AnonymousClass3FB(String str, String str2, Map map) {
        this.A00 = str;
        this.A01 = str2;
        this.A02 = map;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("FdsNode(config=");
        A0k.append(this.A00);
        A0k.append(", state=");
        A0k.append((Object) this.A01);
        A0k.append(", params=");
        return C12960it.A0a(this.A02, A0k);
    }
}
