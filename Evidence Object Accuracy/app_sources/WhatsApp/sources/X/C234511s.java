package X;

/* renamed from: X.11s  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C234511s {
    public final C15450nH A00;
    public final C18790t3 A01;
    public final C14950mJ A02;
    public final C14850m9 A03;
    public final C21780xy A04;
    public final C20110vE A05;
    public final C22600zL A06;

    public C234511s(C15450nH r1, C18790t3 r2, C14950mJ r3, C14850m9 r4, C21780xy r5, C20110vE r6, C22600zL r7) {
        this.A03 = r4;
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
        this.A06 = r7;
        this.A05 = r6;
        this.A04 = r5;
    }

    public static final void A00(AnonymousClass1t2 r4, String str, boolean z) {
        int i = r4.A01().A00.A00;
        if (i != 0) {
            if (r4.A01.delete()) {
                r4.A00.delete();
            }
            boolean z2 = true;
            if (AnonymousClass1RN.A01(i)) {
                if (i != 16) {
                    z2 = false;
                }
                StringBuilder sb = new StringBuilder("Transient error during downloading external mutations, status: ");
                sb.append(i);
                throw new AnonymousClass1t4(sb.toString(), z2);
            } else if (i != 5) {
                StringBuilder sb2 = new StringBuilder("Failed to download external mutation with status: ");
                sb2.append(i);
                throw new AnonymousClass1t4(sb2.toString(), true);
            } else if (z) {
                throw new AnonymousClass1K1(58, str);
            } else {
                throw new AnonymousClass1K1(4, str);
            }
        }
    }
}
