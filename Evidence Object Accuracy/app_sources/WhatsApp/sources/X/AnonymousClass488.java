package X;

import com.whatsapp.util.Log;
import com.whatsapp.wamsys.JniBridge;

/* renamed from: X.488  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass488 extends AbstractC29111Qx {
    public final /* synthetic */ AnonymousClass4WB A00;
    public final /* synthetic */ AnonymousClass1RJ A01;
    public final /* synthetic */ AnonymousClass1RJ A02;

    public AnonymousClass488(AnonymousClass4WB r1, AnonymousClass1RJ r2, AnonymousClass1RJ r3) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    @Override // X.AbstractC29111Qx
    public void A01() {
        try {
            AnonymousClass1RJ r11 = this.A02;
            AnonymousClass4WB r2 = this.A00;
            int i = r2.A01;
            int i2 = r2.A00;
            int i3 = r2.A02;
            long j = (long) i;
            long j2 = (long) i2;
            long j3 = (long) i3;
            JniBridge.jvidispatchIIIIOOOOOOOOO(j, j2, j3, r2.A07, r2.A06, r2.A04, r2.A03, r2.A05, r11, r2.A09, r2.A08, r2.A0A);
        } catch (Exception unused) {
            Log.e("Error: Could not enqueue download request in wa-msys");
            A02(new AnonymousClass1RN(17, null, false));
        }
    }
}
