package X;

import android.database.Cursor;
import android.text.TextUtils;
import android.util.Base64;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.whatsapp.util.Log;
import java.io.File;

/* renamed from: X.1Vy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30061Vy extends AbstractC16130oV implements AbstractC16400ox, AbstractC16420oz {
    public boolean A00;
    public transient AnonymousClass1KB A01;
    public transient Integer A02;

    public C30061Vy(C16150oX r10, AnonymousClass1IS r11, C30061Vy r12, long j, boolean z) {
        super(r10, r11, r12, r12.A0y, j, z);
        this.A01 = r12.A01;
        this.A00 = r12.A00;
    }

    public C30061Vy(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 20, j);
    }

    public C30061Vy(C57702nU r10, AnonymousClass1IS r11, long j, boolean z) {
        this(r11, j);
        AbstractC27881Jp r0;
        int i;
        C16150oX r3 = new C16150oX();
        ((AbstractC16130oV) this).A02 = r3;
        if ((r10.A00 & 8) == 8) {
            AnonymousClass3AI.A00(r3, this, r10.A09.A04());
        }
        int i2 = r10.A00;
        if ((i2 & 512) == 512) {
            r3.A0B = r10.A05 * 1000;
        }
        if (!z || (i2 & 2) == 2) {
            byte[] A04 = r10.A07.A04();
            int length = A04.length;
            if (length == 32) {
                ((AbstractC16130oV) this).A05 = Base64.encodeToString(A04, 2);
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append("FMessageSticker/bogus sha-256 hash received; length=");
                sb.append(length);
                sb.append("; message.key=");
                sb.append(r11);
                Log.w(sb.toString());
                throw new C43271wi(14);
            }
        }
        if ((r10.A00 & 4) == 4) {
            byte[] A042 = r10.A06.A04();
            int length2 = A042.length;
            if (length2 == 32) {
                ((AbstractC16130oV) this).A04 = Base64.encodeToString(A042, 2);
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("FMessageSticker/bogus sha-256 hash received; length=");
                sb2.append(length2);
                sb2.append("; message.key=");
                sb2.append(r11);
                Log.w(sb2.toString());
                throw new C43271wi(14);
            }
        }
        if (!z || (r10.A00 & 16) == 16) {
            String str = r10.A0D;
            if ("image/webp".equalsIgnoreCase(str)) {
                ((AbstractC16130oV) this).A06 = str;
            } else {
                StringBuilder sb3 = new StringBuilder("FMessageSticker/invalid sticker mime type; mimetype=");
                sb3.append(str);
                sb3.append("; message.key=");
                sb3.append(r11);
                Log.w(sb3.toString());
                throw new C43271wi(17);
            }
        }
        if ((r10.A00 & 1) == 1) {
            A19(r10.A0E);
        }
        int i3 = r10.A00;
        if ((i3 & 64) == 64 && (i3 & 32) == 32) {
            r3.A06 = r10.A02;
            r3.A08 = r10.A03;
        }
        if (!z || (i3 & 128) == 128) {
            r3.A0G = r10.A0C;
        }
        if ((i3 & 256) == 256) {
            long j2 = r10.A04;
            if (j2 >= 0) {
                ((AbstractC16130oV) this).A01 = j2;
            } else {
                StringBuilder sb4 = new StringBuilder("FMessageSticker/bogus media size received; fileLength=");
                sb4.append(j2);
                sb4.append("; message.key=");
                sb4.append(r11);
                Log.w(sb4.toString());
                throw new C43271wi(13);
            }
        }
        if ((i3 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024 && (i = r10.A01) > 0) {
            r3.A04 = i;
        }
        if ((i3 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048 && (r0 = r10.A08) != null) {
            r3.A0R = r0.A04();
        }
        this.A00 = r10.A0F;
    }

    @Override // X.AbstractC16130oV
    public void A17(Cursor cursor, C16150oX r8) {
        super.A17(cursor, r8);
        boolean z = false;
        if (cursor.getLong(cursor.getColumnIndexOrThrow("is_animated_sticker")) == 1) {
            z = true;
        }
        this.A00 = z;
    }

    public final C82343vT A1B(C39971qq r18) {
        C16150oX r5 = ((AbstractC16130oV) this).A02;
        if (r5 == null || (!r18.A08 && r5.A0U == null)) {
            StringBuilder sb = new StringBuilder("FMessageSticker/unable to send encrypted media message due to missing mediaKey; message.key=");
            sb.append(this.A0z);
            sb.append("; media_wa_type=");
            sb.append((int) this.A0y);
            Log.w(sb.toString());
            return null;
        }
        C57702nU r0 = ((C27081Fy) r18.A03.A00).A0c;
        if (r0 == null) {
            r0 = C57702nU.A0G;
        }
        C82343vT r3 = (C82343vT) r0.A0T();
        byte[] bArr = r5.A0U;
        if (bArr != null) {
            AbstractC27881Jp A01 = AbstractC27881Jp.A01(bArr, 0, bArr.length);
            r3.A03();
            C57702nU r1 = (C57702nU) r3.A00;
            r1.A00 |= 8;
            r1.A09 = A01;
        } else {
            Log.w("FMessageSticker/buildE2eMessage/sticker media key missing");
        }
        long j = r5.A0B;
        if (j > 0) {
            r3.A03();
            C57702nU r4 = (C57702nU) r3.A00;
            r4.A00 |= 512;
            r4.A05 = j / 1000;
        }
        if (!TextUtils.isEmpty(((AbstractC16130oV) this).A04)) {
            byte[] decode = Base64.decode(((AbstractC16130oV) this).A04, 0);
            AbstractC27881Jp A012 = AbstractC27881Jp.A01(decode, 0, decode.length);
            r3.A03();
            C57702nU r12 = (C57702nU) r3.A00;
            r12.A00 |= 4;
            r12.A06 = A012;
        }
        if (!TextUtils.isEmpty(((AbstractC16130oV) this).A05)) {
            byte[] decode2 = Base64.decode(((AbstractC16130oV) this).A05, 0);
            AbstractC27881Jp A013 = AbstractC27881Jp.A01(decode2, 0, decode2.length);
            r3.A03();
            C57702nU r13 = (C57702nU) r3.A00;
            r13.A00 |= 2;
            r13.A07 = A013;
        }
        int i = r5.A06;
        if (i > 0 && r5.A08 > 0) {
            r3.A03();
            C57702nU r14 = (C57702nU) r3.A00;
            r14.A00 |= 32;
            r14.A02 = i;
            int i2 = r5.A08;
            r3.A03();
            C57702nU r15 = (C57702nU) r3.A00;
            r15.A00 |= 64;
            r15.A03 = i2;
        }
        AnonymousClass1PG r132 = r18.A04;
        byte[] bArr2 = r18.A09;
        if (C32411c7.A0U(r132, this, bArr2)) {
            C43261wh A0P = C32411c7.A0P(r18.A00, r18.A02, r132, this, bArr2, r18.A06);
            r3.A03();
            C57702nU r16 = (C57702nU) r3.A00;
            r16.A0B = A0P;
            r16.A00 |= 16384;
        }
        String str = ((AbstractC16130oV) this).A08;
        if (str != null) {
            r3.A03();
            C57702nU r17 = (C57702nU) r3.A00;
            r17.A00 |= 1;
            r17.A0E = str;
        }
        String str2 = ((AbstractC16130oV) this).A06;
        if (str2 != null) {
            r3.A03();
            C57702nU r19 = (C57702nU) r3.A00;
            r19.A00 |= 16;
            r19.A0D = str2;
        }
        if (!TextUtils.isEmpty(r5.A0G)) {
            String str3 = r5.A0G;
            r3.A03();
            C57702nU r110 = (C57702nU) r3.A00;
            r110.A00 |= 128;
            r110.A0C = str3;
        }
        long j2 = ((AbstractC16130oV) this).A01;
        if (j2 > 0) {
            r3.A03();
            C57702nU r6 = (C57702nU) r3.A00;
            r6.A00 |= 256;
            r6.A04 = j2;
        }
        int i3 = r5.A04;
        if (i3 > 0) {
            r3.A03();
            C57702nU r111 = (C57702nU) r3.A00;
            r111.A00 |= EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
            r111.A01 = i3;
        }
        byte[] bArr3 = r5.A0R;
        if (bArr3 != null) {
            AbstractC27881Jp A014 = AbstractC27881Jp.A01(bArr3, 0, bArr3.length);
            r3.A03();
            C57702nU r112 = (C57702nU) r3.A00;
            r112.A00 |= EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
            r112.A08 = A014;
        }
        boolean z = this.A00;
        r3.A03();
        C57702nU r113 = (C57702nU) r3.A00;
        r113.A00 |= 4096;
        r113.A0F = z;
        return r3;
    }

    public AnonymousClass1KS A1C() {
        AnonymousClass1KS r2 = new AnonymousClass1KS();
        C16150oX r3 = ((AbstractC16130oV) this).A02;
        if (r3 != null) {
            File file = r3.A0F;
            if (file != null && file.exists()) {
                r2.A08 = r3.A0F.getAbsolutePath();
                r2.A01 = 1;
            } else if (A16() != null) {
                r2.A08 = A16();
                r2.A01 = 3;
            }
            r2.A0C = ((AbstractC16130oV) this).A05;
            r2.A07 = ((AbstractC16130oV) this).A04;
            r2.A03 = r3.A08;
            r2.A02 = r3.A06;
            r2.A0B = ((AbstractC16130oV) this).A06;
            byte[] bArr = r3.A0U;
            if (bArr != null) {
                r2.A0A = Base64.encodeToString(bArr, 1);
            }
            String str = r3.A0G;
            if (str != null) {
                r2.A05 = str;
            }
        }
        C37431mO.A00(r2);
        return r2;
    }

    @Override // X.AbstractC16420oz
    public void A6k(C39971qq r4) {
        AnonymousClass1IR r0 = this.A0L;
        if (r0 != null) {
            A0d(r0, r4);
            return;
        }
        C82343vT A1B = A1B(r4);
        if (A1B != null) {
            AnonymousClass1G3 r02 = r4.A03;
            r02.A03();
            C27081Fy r2 = (C27081Fy) r02.A00;
            r2.A0c = (C57702nU) A1B.A02();
            r2.A00 |= 2097152;
        }
    }

    @Override // X.AbstractC16400ox
    public /* bridge */ /* synthetic */ AbstractC15340mz A7M(AnonymousClass1IS r8) {
        return new C30061Vy(((AbstractC16130oV) this).A02, r8, this, this.A0I, true);
    }
}
