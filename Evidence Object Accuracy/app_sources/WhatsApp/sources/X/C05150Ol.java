package X;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Handler;
import android.os.Looper;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.0Ol  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C05150Ol {
    public FrameLayout A00;
    public final Context A01;
    public final Handler A02 = new Handler(Looper.getMainLooper());
    public final C04540Mc A03;
    public final C04550Md A04;
    public final C14260l7 A05;

    public C05150Ol(Context context, C04540Mc r4, C04550Md r5, C14260l7 r6) {
        this.A01 = context;
        this.A05 = r6;
        this.A03 = r4;
        this.A04 = r5;
    }

    public final void A00(FrameLayout frameLayout, C04550Md r17) {
        FrameLayout frameLayout2;
        FrameLayout frameLayout3 = this.A00;
        if (frameLayout3 == null) {
            this.A00 = new FrameLayout(this.A01);
        } else if (!(frameLayout3.getParent() == null || (frameLayout2 = this.A00) == null)) {
            ViewParent parent = frameLayout2.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(this.A00);
            }
            this.A00.removeAllViews();
        }
        Context context = this.A01;
        ImageView imageView = new ImageView(context);
        imageView.setImageResource(AnonymousClass0K3.company_layer_icons_close_outline_24);
        C14260l7 r2 = this.A05;
        imageView.setColorFilter(AnonymousClass0TG.A00(context, EnumC03700Iu.A05, r2));
        imageView.setOnClickListener(new AnonymousClass0WH(this));
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams((int) AnonymousClass0LQ.A00(context, 24.0f), (int) AnonymousClass0LQ.A00(context, 24.0f));
        layoutParams.setMargins((int) AnonymousClass0LQ.A00(context, 20.0f), (int) AnonymousClass0LQ.A00(context, 30.0f), 0, 0);
        layoutParams.gravity = 51;
        C013606j A01 = C013606j.A01(null, context.getResources(), R.drawable.ic_error_outline_96_rev);
        ImageView imageView2 = new ImageView(context);
        imageView2.setImageDrawable(A01);
        imageView2.setPadding(0, 0, 0, (int) AnonymousClass0LQ.A00(context, 32.0f));
        imageView2.setColorFilter(AnonymousClass0TG.A00(context, EnumC03700Iu.A0B, r2));
        FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(-2, -2);
        layoutParams2.gravity = 3;
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setGravity(16);
        linearLayout.setOrientation(1);
        linearLayout.setPadding((int) AnonymousClass0LQ.A00(context, 32.0f), 0, (int) AnonymousClass0LQ.A00(context, 32.0f), 0);
        linearLayout.addView(imageView2, layoutParams2);
        TextView textView = new TextView(context);
        textView.setText(AnonymousClass0K4.failed_loading_title);
        EnumC03700Iu r8 = EnumC03700Iu.A06;
        textView.setTextColor(AnonymousClass0TG.A00(context, r8, r2));
        textView.setTypeface(Typeface.defaultFromStyle(1));
        textView.setTextSize(24.0f);
        textView.setMaxLines(3);
        textView.setPadding(0, 0, 0, (int) AnonymousClass0LQ.A00(context, 20.0f));
        TextView textView2 = new TextView(context);
        textView2.setText(AnonymousClass0K4.failed_loading_message);
        textView.setTextColor(AnonymousClass0TG.A00(context, r8, r2));
        textView2.setTextSize(15.0f);
        textView2.setMaxLines(7);
        linearLayout.addView(textView);
        linearLayout.addView(textView2);
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setCornerRadius(AnonymousClass0LQ.A00(context, 4.0f));
        gradientDrawable.setColor(AnonymousClass0TG.A00(context, EnumC03700Iu.A03, r2));
        Button button = new Button(context);
        button.setBackgroundDrawable(gradientDrawable);
        button.setText(AnonymousClass0K4.failed_loading_refresh);
        button.setTextSize(17.0f);
        button.setTextColor(AnonymousClass0TG.A00(context, EnumC03700Iu.A04, r2));
        button.setHeight((int) AnonymousClass0LQ.A00(context, 52.0f));
        button.setOnClickListener(new AnonymousClass0WL(r17, this));
        LinearLayout linearLayout2 = new LinearLayout(context);
        linearLayout2.setGravity(80);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams3.setMargins((int) AnonymousClass0LQ.A00(context, 20.0f), 0, (int) AnonymousClass0LQ.A00(context, 20.0f), (int) AnonymousClass0LQ.A00(context, 20.0f));
        layoutParams3.weight = 1.0f;
        linearLayout2.addView(button, layoutParams3);
        this.A00.addView(imageView, layoutParams);
        this.A00.addView(linearLayout);
        this.A00.addView(linearLayout2);
        frameLayout.addView(this.A00);
    }
}
