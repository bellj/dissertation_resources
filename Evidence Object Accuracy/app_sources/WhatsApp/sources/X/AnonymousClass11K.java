package X;

import android.content.Context;

/* renamed from: X.11K  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass11K {
    public final C15570nT A00;
    public final C15550nR A01;
    public final C15610nY A02;
    public final C16590pI A03;
    public final AnonymousClass018 A04;

    public AnonymousClass11K(C15570nT r1, C15550nR r2, C15610nY r3, C16590pI r4, AnonymousClass018 r5) {
        this.A03 = r4;
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
        this.A04 = r5;
    }

    public final String A00(AbstractC14640lm r6, int i) {
        Context context = this.A03.A00;
        Object[] objArr = new Object[1];
        C15550nR r0 = this.A01;
        AnonymousClass009.A05(r6);
        String str = null;
        String A04 = this.A02.A04(r0.A0B(r6));
        if (A04 != null) {
            str = this.A04.A0F(A04);
        }
        objArr[0] = str;
        return context.getString(i, objArr);
    }
}
