package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.63g  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1315663g implements Parcelable {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(42);
    public final int A00;
    public final String A01;
    public final String A02;
    public final String A03;
    public final boolean A04;
    public final boolean A05;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C1315663g(Parcel parcel) {
        this.A02 = parcel.readString();
        this.A00 = parcel.readInt();
        this.A03 = parcel.readString();
        this.A01 = parcel.readString();
        boolean z = false;
        this.A05 = C12960it.A1V(parcel.readInt(), 1);
        this.A04 = parcel.readInt() == 1 ? true : z;
    }

    public C1315663g(String str, String str2, String str3, int i, boolean z, boolean z2) {
        this.A02 = str;
        this.A00 = i;
        this.A03 = str2;
        this.A01 = str3;
        this.A05 = z;
        this.A04 = z2;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A02);
        parcel.writeInt(this.A00);
        parcel.writeString(this.A03);
        parcel.writeString(this.A01);
        parcel.writeInt(this.A05 ? 1 : 0);
        parcel.writeInt(this.A04 ? 1 : 0);
    }
}
