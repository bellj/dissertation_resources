package X;

/* renamed from: X.1u8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C41771u8 extends AbstractC16110oT {
    public Double A00;
    public Integer A01;
    public Integer A02;
    public Long A03;

    public C41771u8() {
        super(848, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A01);
        r3.Abe(4, this.A00);
        r3.Abe(3, this.A03);
        r3.Abe(2, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        StringBuilder sb = new StringBuilder("WamProfilePicDownload {");
        Integer num = this.A01;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "profilePicDownloadResult", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "profilePicDownloadSize", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "profilePicDownloadT", this.A03);
        Integer num2 = this.A02;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "profilePicType", obj2);
        sb.append("}");
        return sb.toString();
    }
}
