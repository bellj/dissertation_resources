package X;

import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.util.Log;

/* renamed from: X.4W9  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4W9 {
    public Object A00;
    public boolean A01 = false;
    public boolean A02 = false;
    public final Context A03;
    public final C78583p9 A04;
    public final Object A05 = C12970iu.A0l();
    public final String A06;
    public final String A07;
    public final String A08;

    public AnonymousClass4W9(Context context, C78583p9 r5) {
        this.A03 = context;
        this.A06 = "FaceNativeHandle";
        this.A07 = C72453ed.A0r("com.google.android.gms.vision.dynamite.", "face");
        this.A08 = "face";
        this.A04 = r5;
        A00();
    }

    public final Object A00() {
        Object obj;
        String str;
        Object obj2;
        Object obj3;
        synchronized (this.A05) {
            obj = this.A00;
            if (obj == null) {
                C95564dy r6 = null;
                try {
                    r6 = C95564dy.A06(this.A03, C95564dy.A08, this.A07);
                } catch (AnonymousClass4CB unused) {
                    Object[] A1a = C12980iv.A1a();
                    A1a[0] = "com.google.android.gms.vision";
                    String str2 = this.A08;
                    A1a[1] = str2;
                    String format = String.format("%s.%s", A1a);
                    Object[] objArr = {format};
                    if (Log.isLoggable("Vision", 3)) {
                        Log.d("Vision", String.format("Cannot load thick client module, fall back to load optional module %s", objArr));
                    }
                    try {
                        r6 = C95564dy.A06(this.A03, C95564dy.A09, format);
                    } catch (AnonymousClass4CB e) {
                        AnonymousClass4DY.A00("Error loading optional module %s", e, format);
                        if (!this.A01) {
                            Object[] objArr2 = {str2};
                            if (Log.isLoggable("Vision", 3)) {
                                Log.d("Vision", String.format("Broadcasting download intent for dependency %s", objArr2));
                            }
                            Intent intent = new Intent();
                            intent.setClassName("com.google.android.gms", "com.google.android.gms.vision.DependencyBroadcastReceiverProxy");
                            intent.putExtra("com.google.android.gms.vision.DEPENDENCIES", str2);
                            intent.setAction("com.google.android.gms.vision.DEPENDENCY");
                            this.A03.sendBroadcast(intent);
                            this.A01 = true;
                        }
                    }
                }
                if (r6 != null) {
                    try {
                        Context context = this.A03;
                        if (C95564dy.A00(context, "com.google.android.gms.vision.dynamite.face") > C95564dy.A01(context, "com.google.android.gms.vision.dynamite", false)) {
                            str = "com.google.android.gms.vision.face.NativeFaceDetectorV2Creator";
                        } else {
                            str = "com.google.android.gms.vision.face.ChimeraNativeFaceDetectorCreator";
                        }
                        try {
                            IBinder iBinder = (IBinder) r6.A00.getClassLoader().loadClass(str).newInstance();
                            if (iBinder != null) {
                                IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.vision.face.internal.client.INativeFaceDetectorCreator");
                                if (queryLocalInterface instanceof AnonymousClass5YM) {
                                    obj3 = (AnonymousClass5YM) queryLocalInterface;
                                } else {
                                    obj3 = new C79973rV(iBinder);
                                }
                                if (obj3 != null) {
                                    BinderC56502l7 r5 = new BinderC56502l7(context);
                                    C78583p9 r2 = this.A04;
                                    C13020j0.A01(r2);
                                    C79973rV r62 = (C79973rV) obj3;
                                    Parcel obtain = Parcel.obtain();
                                    obtain.writeInterfaceToken(r62.A01);
                                    obtain.writeStrongBinder(r5.asBinder());
                                    if (r2 == null) {
                                        obtain.writeInt(0);
                                    } else {
                                        obtain.writeInt(1);
                                        r2.writeToParcel(obtain, 0);
                                    }
                                    Parcel A00 = r62.A00(obtain);
                                    IBinder readStrongBinder = A00.readStrongBinder();
                                    if (readStrongBinder == null) {
                                        obj2 = null;
                                    } else {
                                        IInterface queryLocalInterface2 = readStrongBinder.queryLocalInterface("com.google.android.gms.vision.face.internal.client.INativeFaceDetector");
                                        if (queryLocalInterface2 instanceof AnonymousClass5YL) {
                                            obj2 = (AnonymousClass5YL) queryLocalInterface2;
                                        } else {
                                            obj2 = new C79963rU(readStrongBinder);
                                        }
                                    }
                                    A00.recycle();
                                    this.A00 = obj2;
                                }
                            }
                            obj2 = null;
                            this.A00 = obj2;
                        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e2) {
                            throw new AnonymousClass4CB(C12960it.A0c(String.valueOf(str), "Failed to instantiate module class: "), e2);
                        }
                    } catch (AnonymousClass4CB | RemoteException e3) {
                        Log.e(this.A06, "Error creating remote native handle", e3);
                    }
                }
                boolean z = this.A02;
                if (!z && this.A00 == null) {
                    Log.w(this.A06, "Native handle not yet available. Reverting to no-op handle.");
                    this.A02 = true;
                } else if (z && this.A00 != null) {
                    Log.w(this.A06, "Native handle is now available.");
                }
                obj = this.A00;
            }
        }
        return obj;
    }
}
