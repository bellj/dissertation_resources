package X;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;

/* renamed from: X.1ia  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC35731ia {
    @Deprecated
    public static Activity A00(Context context) {
        while (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                return (Activity) context;
            }
            context = ((ContextWrapper) context).getBaseContext();
        }
        return null;
    }

    @Deprecated
    public static Activity A01(Context context, Class cls) {
        Activity A00 = A00(context);
        if (A00 == null || !cls.isAssignableFrom(A00.getClass())) {
            return null;
        }
        return A00;
    }
}
