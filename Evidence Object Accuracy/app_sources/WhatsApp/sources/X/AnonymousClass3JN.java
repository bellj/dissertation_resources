package X;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.SecureRandom;

/* renamed from: X.3JN  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3JN {
    public static final SecureRandom A04 = new SecureRandom();
    public byte[] A00;
    public final C63503Bv A01;
    public final AnonymousClass4TB[] A02;
    public final C91324Rh[] A03;

    public AnonymousClass3JN(C63503Bv r1, AnonymousClass4TB[] r2, C91324Rh[] r3) {
        this.A01 = r1;
        this.A03 = r3;
        this.A02 = r2;
    }

    public static AnonymousClass3JN A00(byte[] bArr) {
        if (bArr != null) {
            int length = bArr.length;
            if (length >= 12) {
                short A02 = A02(bArr, 0);
                byte b = bArr[2];
                boolean A1S = C12960it.A1S(b & 128);
                boolean A1S2 = C12960it.A1S(b & 4);
                boolean A1S3 = C12960it.A1S(b & 2);
                boolean A1S4 = C12960it.A1S(b & 1);
                byte b2 = bArr[3];
                boolean A1S5 = C12960it.A1S(b2 & 128);
                C63503Bv r2 = new C63503Bv((byte) ((b >> 3) & 15), A02, (short) ((byte) (b2 & 15)), A02(bArr, 4), A02(bArr, 6), A02(bArr, 8), A02(bArr, 10), A1S, A1S2, A1S3, A1S4, A1S5);
                int i = 12;
                int i2 = r2.A05;
                C91324Rh[] r10 = new C91324Rh[i2];
                for (int i3 = 0; i3 < i2; i3++) {
                    AnonymousClass3IM A00 = AnonymousClass3IM.A00(bArr, i);
                    int i4 = A00.A00;
                    int i5 = i + i4;
                    if (length >= i5 + 4) {
                        r10[i3] = new C91324Rh(A00, i4 + 4, A02(bArr, i5), A02(bArr, i5 + 2));
                        i += r10[i3].A00;
                    } else {
                        throw C12970iu.A0f("insufficient data");
                    }
                }
                int i6 = r2.A03;
                AnonymousClass4TB[] r7 = new AnonymousClass4TB[i6];
                for (int i7 = 0; i7 < i6; i7++) {
                    AnonymousClass3IM A002 = AnonymousClass3IM.A00(bArr, i);
                    int i8 = A002.A00;
                    int i9 = i + i8;
                    int i10 = i9 + 10;
                    if (length >= i10) {
                        short A022 = A02(bArr, i9);
                        short A023 = A02(bArr, i9 + 2);
                        int i11 = i9 + 4;
                        int i12 = ((bArr[i11] << 24) & -16777216) | ((bArr[i11 + 1] << 16) & 16711680) | ((bArr[i11 + 2] << 8) & 65280) | (bArr[i11 + 3] & 255);
                        int A024 = A02(bArr, i9 + 8);
                        if (length >= i10 + A024) {
                            byte[] bArr2 = new byte[A024];
                            System.arraycopy(bArr, i10, bArr2, 0, A024);
                            r7[i7] = new AnonymousClass4TB(A002, bArr2, i12, A024 + i8 + 10, A022, A023);
                            i += r7[i7].A00;
                        } else {
                            throw C12970iu.A0f("insufficient data");
                        }
                    } else {
                        throw C12970iu.A0f("insufficient data");
                    }
                }
                AnonymousClass3JN r0 = new AnonymousClass3JN(r2, r7, r10);
                r0.A00 = bArr;
                return r0;
            }
            throw C12970iu.A0f("bytes does not contain enough data");
        }
        throw C12980iv.A0n("bytes may not be null");
    }

    public static AnonymousClass3JN A01(C91324Rh[] r15) {
        return new AnonymousClass3JN(new C63503Bv((byte) 0, (short) A04.nextInt(65536), 0, 1, 0, 0, 0, false, false, false, true, false), new AnonymousClass4TB[0], r15);
    }

    public static short A02(byte[] bArr, int i) {
        return (short) (((bArr[i] << 8) & 65280) | (bArr[i + 1] & 255));
    }

    public static void A03(OutputStream outputStream, short s) {
        outputStream.write((byte) ((65280 & s) >>> 8));
        outputStream.write((byte) (s & 255));
    }

    public static void A04(byte[] bArr, int i, short s) {
        bArr[i] = (byte) ((65280 & s) >>> 8);
        bArr[i + 1] = (byte) (s & 255);
    }

    public byte[] A05() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            C63503Bv r3 = this.A01;
            byte[] bArr = new byte[12];
            A04(bArr, 0, r3.A01);
            int i = 0;
            bArr[2] = 0;
            int i2 = 0;
            if (r3.A0A) {
                i2 = 128;
            }
            byte b = (byte) (i2 | 0);
            bArr[2] = b;
            byte b2 = (byte) (b | ((byte) ((r3.A00 & 15) << 3)));
            bArr[2] = b2;
            byte b3 = 0;
            if (r3.A07) {
                b3 = 4;
            }
            byte b4 = (byte) (b2 | b3);
            bArr[2] = b4;
            byte b5 = 0;
            if (r3.A0B) {
                b5 = 2;
            }
            byte b6 = (byte) (b4 | b5);
            bArr[2] = b6;
            bArr[2] = (byte) (b6 | (r3.A09 ? 1 : 0));
            bArr[3] = 0;
            if (r3.A08) {
                i = 128;
            }
            byte b7 = (byte) (i | 0);
            bArr[3] = b7;
            bArr[3] = (byte) (b7 | ((byte) (r3.A06 & 15)));
            A04(bArr, 4, r3.A05);
            A04(bArr, 6, r3.A03);
            A04(bArr, 8, r3.A04);
            A04(bArr, 10, r3.A02);
            byteArrayOutputStream.write(bArr);
            C91324Rh[] r6 = this.A03;
            for (C91324Rh r1 : r6) {
                r1.A01.A02(byteArrayOutputStream);
                A03(byteArrayOutputStream, r1.A03);
                A03(byteArrayOutputStream, r1.A02);
            }
            AnonymousClass4TB[] r32 = this.A02;
            for (AnonymousClass4TB r62 : r32) {
                r62.A02.A02(byteArrayOutputStream);
                A03(byteArrayOutputStream, r62.A04);
                A03(byteArrayOutputStream, r62.A03);
                int i3 = r62.A01;
                byteArrayOutputStream.write((i3 >>> 24) & 255);
                byteArrayOutputStream.write((i3 >>> 16) & 255);
                byteArrayOutputStream.write((i3 >>> 8) & 255);
                byteArrayOutputStream.write(i3 & 255);
                byte[] bArr2 = r62.A05;
                A03(byteArrayOutputStream, (short) bArr2.length);
                byteArrayOutputStream.write(bArr2);
            }
            return byteArrayOutputStream.toByteArray();
        } catch (IOException unused) {
            return null;
        }
    }
}
