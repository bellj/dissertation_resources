package X;

import android.app.Activity;

/* renamed from: X.0LI  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0LI {
    public static final boolean A00(Activity activity) {
        return activity.isInMultiWindowMode();
    }
}
