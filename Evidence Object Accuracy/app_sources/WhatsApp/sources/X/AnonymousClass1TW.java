package X;

/* renamed from: X.1TW  reason: invalid class name */
/* loaded from: classes2.dex */
public interface AnonymousClass1TW {
    public static final AnonymousClass1TK A00 = new AnonymousClass1TK("1.3.14.3.2.7");
    public static final AnonymousClass1TK A01 = new AnonymousClass1TK("1.3.14.3.2.9");
    public static final AnonymousClass1TK A02 = new AnonymousClass1TK("1.3.14.3.2.6");
    public static final AnonymousClass1TK A03 = new AnonymousClass1TK("1.3.14.3.2.17");
    public static final AnonymousClass1TK A04 = new AnonymousClass1TK("1.3.14.3.2.8");
    public static final AnonymousClass1TK A05 = new AnonymousClass1TK("1.3.14.3.2.27");
    public static final AnonymousClass1TK A06 = new AnonymousClass1TK("1.3.14.7.2.1.1");
    public static final AnonymousClass1TK A07 = new AnonymousClass1TK("1.3.14.3.2.26");
    public static final AnonymousClass1TK A08 = new AnonymousClass1TK("1.3.14.3.2.2");
    public static final AnonymousClass1TK A09 = new AnonymousClass1TK("1.3.14.3.2.4");
    public static final AnonymousClass1TK A0A = new AnonymousClass1TK("1.3.14.3.2.3");
    public static final AnonymousClass1TK A0B = new AnonymousClass1TK("1.3.14.3.2.29");
}
