package X;

import android.app.Activity;
import com.whatsapp.jid.UserJid;

/* renamed from: X.543  reason: invalid class name */
/* loaded from: classes3.dex */
public final /* synthetic */ class AnonymousClass543 implements AnonymousClass5TY {
    public final /* synthetic */ Activity A00;
    public final /* synthetic */ C238013b A01;
    public final /* synthetic */ UserJid A02;

    public /* synthetic */ AnonymousClass543(Activity activity, C238013b r2, UserJid userJid) {
        this.A01 = r2;
        this.A00 = activity;
        this.A02 = userJid;
    }

    @Override // X.AnonymousClass5TY
    public final void AfB() {
        this.A01.A0C(this.A00, this.A02);
    }
}
