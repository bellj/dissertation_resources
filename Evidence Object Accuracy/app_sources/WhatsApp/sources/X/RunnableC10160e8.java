package X;

import android.os.Build;
import android.view.animation.Interpolator;
import android.widget.OverScroller;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.0e8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC10160e8 implements Runnable {
    public int A00;
    public int A01;
    public Interpolator A02;
    public OverScroller A03;
    public boolean A04 = false;
    public boolean A05 = false;
    public final /* synthetic */ RecyclerView A06;

    public RunnableC10160e8(RecyclerView recyclerView) {
        this.A06 = recyclerView;
        Interpolator interpolator = RecyclerView.A1B;
        this.A02 = interpolator;
        this.A03 = new OverScroller(recyclerView.getContext(), interpolator);
    }

    public final int A00(int i, int i2) {
        int height;
        int i3;
        int abs = Math.abs(i);
        int abs2 = Math.abs(i2);
        boolean z = false;
        if (abs > abs2) {
            z = true;
        }
        int sqrt = (int) Math.sqrt((double) 0);
        int sqrt2 = (int) Math.sqrt((double) ((i * i) + (i2 * i2)));
        RecyclerView recyclerView = this.A06;
        if (z) {
            height = recyclerView.getWidth();
        } else {
            height = recyclerView.getHeight();
        }
        float f = (float) height;
        float f2 = (float) (height >> 1);
        float sin = f2 + (((float) Math.sin((double) ((Math.min(1.0f, (((float) sqrt2) * 1.0f) / f) - 0.5f) * 0.47123894f))) * f2);
        if (sqrt > 0) {
            i3 = Math.round(Math.abs(sin / ((float) sqrt)) * 1000.0f) << 2;
        } else {
            if (!z) {
                abs = abs2;
            }
            i3 = (int) (((((float) abs) / f) + 1.0f) * 300.0f);
        }
        return Math.min(i3, 2000);
    }

    public void A01() {
        if (this.A04) {
            this.A05 = true;
            return;
        }
        RecyclerView recyclerView = this.A06;
        recyclerView.removeCallbacks(this);
        recyclerView.postOnAnimation(this);
    }

    public void A02(Interpolator interpolator, int i, int i2, int i3) {
        if (this.A02 != interpolator) {
            this.A02 = interpolator;
            this.A03 = new OverScroller(this.A06.getContext(), interpolator);
        }
        this.A06.setScrollState(2);
        this.A01 = 0;
        this.A00 = 0;
        this.A03.startScroll(0, 0, i, i2, i3);
        if (Build.VERSION.SDK_INT < 23) {
            this.A03.computeScrollOffset();
        }
        A01();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:107:0x0180, code lost:
        if (r2 != false) goto L_0x0118;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x019b, code lost:
        if (r2 > 0) goto L_0x00b4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x01a3, code lost:
        if (r9 <= 0) goto L_0x01a5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00d7, code lost:
        if (r1 == 0) goto L_0x00d9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00df, code lost:
        if (r0.getFinalX() != 0) goto L_0x00ee;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0109, code lost:
        if (r3 != r5) goto L_0x010b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00b1  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00ba  */
    @Override // java.lang.Runnable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
        // Method dump skipped, instructions count: 446
        */
        throw new UnsupportedOperationException("Method not decompiled: X.RunnableC10160e8.run():void");
    }
}
