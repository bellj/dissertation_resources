package X;

import com.whatsapp.util.Log;

/* renamed from: X.5hh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C121345hh extends AbstractC130285z6 {
    public AnonymousClass619 A00;
    public String A01;

    public C121345hh(AnonymousClass1V8 r2, String str) {
        try {
            this.A01 = str;
            this.A00 = AnonymousClass619.A01(r2, "rekyc_description");
        } catch (AnonymousClass1V9 unused) {
            Log.e("PAY: ReKycChallenge parse description challenge failed.");
        }
    }
}
