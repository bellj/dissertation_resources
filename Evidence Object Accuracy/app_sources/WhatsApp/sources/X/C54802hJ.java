package X;

import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.2hJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54802hJ extends AbstractC05270Ox {
    public final C14260l7 A00;
    public final AnonymousClass28D A01;
    public final AbstractC14200l1 A02;

    public C54802hJ(C14260l7 r1, AnonymousClass28D r2, AbstractC14200l1 r3) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    @Override // X.AbstractC05270Ox
    public void A01(RecyclerView recyclerView, int i, int i2) {
        String str;
        AnonymousClass28D r3 = this.A01;
        int A0B = r3.A0B(53, 0);
        if (A0B != 0) {
            if (recyclerView.canScrollVertically(A0B)) {
                str = "can_scroll";
            } else {
                str = "cannot_scroll";
            }
            C14210l2 r1 = new C14210l2();
            r1.A05(str, 0);
            C14260l7 r2 = this.A00;
            C28701Oq.A01(r2, r3, C14210l2.A01(r1, r2, 1), this.A02);
        }
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C54802hJ)) {
            return false;
        }
        C54802hJ r6 = (C54802hJ) obj;
        if (r6.A02 == this.A02 && r6.A01.A0B(53, 0) == this.A01.A0B(53, 0)) {
            return true;
        }
        return false;
    }
}
