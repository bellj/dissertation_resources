package X;

import android.content.Context;
import android.util.AttributeSet;
import com.whatsapp.chatinfo.view.custom.ChatInfoMediaCardV2;

/* renamed from: X.36E  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass36E extends AnonymousClass36H {
    public boolean A00;

    public AnonymousClass36E(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A01();
    }

    @Override // X.AbstractC53212dH
    public void A01() {
        if (this instanceof ChatInfoMediaCardV2) {
            ChatInfoMediaCardV2 chatInfoMediaCardV2 = (ChatInfoMediaCardV2) this;
            if (!chatInfoMediaCardV2.A00) {
                chatInfoMediaCardV2.A00 = true;
                chatInfoMediaCardV2.A0B = C12960it.A0R(AnonymousClass2P6.A00(chatInfoMediaCardV2.generatedComponent()));
            }
        } else if (!this.A00) {
            this.A00 = true;
            this.A0B = C12960it.A0R(AnonymousClass2P6.A00(generatedComponent()));
        }
    }
}
