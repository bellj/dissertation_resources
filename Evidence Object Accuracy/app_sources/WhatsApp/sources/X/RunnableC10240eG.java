package X;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.0eG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class RunnableC10240eG implements Runnable {
    public static AnonymousClass0AN A07;
    public static final BlockingQueue A08;
    public static final Executor A09;
    public static final ThreadFactory A0A;
    public static volatile Executor A0B;
    public final CallableC10420eb A00;
    public final CountDownLatch A01;
    public final FutureTask A02;
    public final AtomicBoolean A03 = new AtomicBoolean();
    public final AtomicBoolean A04 = new AtomicBoolean();
    public volatile AnonymousClass0JG A05 = AnonymousClass0JG.PENDING;
    public final /* synthetic */ AnonymousClass0ER A06;

    static {
        ThreadFactoryC10640ex r7 = new ThreadFactoryC10640ex();
        A0A = r7;
        LinkedBlockingQueue linkedBlockingQueue = new LinkedBlockingQueue(10);
        A08 = linkedBlockingQueue;
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(5, 128, 1, TimeUnit.SECONDS, linkedBlockingQueue, r7);
        A09 = threadPoolExecutor;
        A0B = threadPoolExecutor;
    }

    public RunnableC10240eG(AnonymousClass0ER r3) {
        this.A06 = r3;
        CallableC10420eb r1 = new CallableC10420eb(this);
        this.A00 = r1;
        this.A02 = new C10950fT(this, r1);
        this.A01 = new CountDownLatch(1);
    }

    public void A00(Object obj) {
        AnonymousClass0AN r3;
        synchronized (RunnableC10240eG.class) {
            r3 = A07;
            if (r3 == null) {
                r3 = new AnonymousClass0AN();
                A07 = r3;
            }
        }
        r3.obtainMessage(1, new C04750Mx(this, obj)).sendToTarget();
    }

    @Override // java.lang.Runnable
    public void run() {
        this.A06.A08();
    }
}
