package X;

import android.view.View;

/* renamed from: X.0G1  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0G1 extends AnonymousClass08s {
    public final /* synthetic */ View A00;
    public final /* synthetic */ AnonymousClass0GB A01;

    public AnonymousClass0G1(View view, AnonymousClass0GB r2) {
        this.A01 = r2;
        this.A00 = view;
    }

    @Override // X.AnonymousClass08s, X.AbstractC018608t
    public void AXq(AnonymousClass072 r4) {
        View view = this.A00;
        AnonymousClass0QJ r0 = AnonymousClass0U3.A04;
        r0.A05(view, 1.0f);
        r0.A03(view);
        r4.A09(this);
    }
}
