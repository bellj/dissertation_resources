package X;

import android.content.Context;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaTextView;

/* renamed from: X.3Va  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C68343Va implements AnonymousClass5TW {
    public final WaTextView A00;

    public C68343Va(View view) {
        WaTextView waTextView = (WaTextView) view.findViewById(R.id.title);
        this.A00 = waTextView;
        AnonymousClass23N.A04(view, true);
        C27531Hw.A06(waTextView);
    }

    @Override // X.AnonymousClass5TW
    public void ANG(AnonymousClass5TX r6) {
        String string;
        WaTextView waTextView = this.A00;
        Context context = waTextView.getContext();
        int i = ((AnonymousClass541) r6).A00;
        int i2 = R.string.block_list_contacts_header;
        if (i != 0) {
            i2 = R.string.block_list_businesses_header;
            if (i != 1) {
                i2 = R.string.block_list_payments_header;
                if (i != 2) {
                    string = null;
                    waTextView.setText(string);
                }
            }
        }
        string = context.getString(i2);
        waTextView.setText(string);
    }
}
