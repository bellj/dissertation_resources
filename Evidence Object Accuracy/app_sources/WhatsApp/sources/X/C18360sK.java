package X;

import android.app.Notification;
import android.os.DeadObjectException;
import android.util.Base64;
import com.whatsapp.util.Log;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* renamed from: X.0sK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18360sK {
    public final AnonymousClass02r A00;

    public C18360sK(C16590pI r3) {
        this.A00 = new AnonymousClass02r(r3.A00);
    }

    public static String A00(AbstractC14640lm r2) {
        if (r2 == null) {
            return null;
        }
        try {
            return Base64.encodeToString(MessageDigest.getInstance("SHA-256").digest(r2.getRawString().getBytes()), 0);
        } catch (NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }

    public static void A01(C005602s r0, int i) {
        r0.A07.icon = i;
    }

    public void A02(int i) {
        A04(i, null);
    }

    public void A03(int i, Notification notification) {
        A05(null, i, notification);
    }

    public void A04(int i, String str) {
        AnonymousClass01I.A01();
        try {
            this.A00.A01(str, i);
        } catch (RuntimeException e) {
            if (e.getCause() instanceof DeadObjectException) {
                StringBuilder sb = new StringBuilder("wanotificationmanager/cancelfailed/");
                sb.append(e);
                Log.e(sb.toString());
                return;
            }
            throw e;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000f, code lost:
        if (android.text.TextUtils.isEmpty(r6.getChannelId()) == false) goto L_0x0011;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A05(java.lang.String r4, int r5, android.app.Notification r6) {
        /*
            r3 = this;
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 26
            if (r1 < r0) goto L_0x0011
            java.lang.String r0 = r6.getChannelId()
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            r0 = 0
            if (r1 != 0) goto L_0x0012
        L_0x0011:
            r0 = 1
        L_0x0012:
            X.AnonymousClass009.A0F(r0)
            X.02r r0 = r3.A00     // Catch: RuntimeException -> 0x001b
            r0.A02(r4, r5, r6)     // Catch: RuntimeException -> 0x001b
            return
        L_0x001b:
            r2 = move-exception
            java.lang.Throwable r0 = r2.getCause()
            boolean r0 = r0 instanceof android.os.DeadObjectException
            if (r0 == 0) goto L_0x0037
            java.lang.String r1 = "wanotificationmanager/notifyfailed/"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            com.whatsapp.util.Log.e(r0)
            return
        L_0x0037:
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C18360sK.A05(java.lang.String, int, android.app.Notification):void");
    }
}
