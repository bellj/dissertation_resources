package X;

/* renamed from: X.4Xp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C92834Xp {
    public String A00;
    public boolean A01;
    public boolean A02;
    public final String A03;
    public final boolean A04;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C92834Xp) {
                C92834Xp r5 = (C92834Xp) obj;
                if (!(this.A04 == r5.A04 && C16700pc.A0O(this.A03, r5.A03) && this.A01 == r5.A01 && this.A02 == r5.A02 && C16700pc.A0O(this.A00, r5.A00))) {
                }
            }
            return false;
        }
        return true;
    }

    public /* synthetic */ C92834Xp(int i, String str, boolean z) {
        str = (i & 2) != 0 ? null : str;
        this.A04 = z;
        this.A03 = str;
        this.A01 = false;
        this.A02 = false;
        this.A00 = null;
    }

    public int hashCode() {
        boolean z = this.A04;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = 0;
        int A04 = ((i2 * 31) + C72463ee.A04(this.A03)) * 31;
        boolean z2 = this.A01;
        if (z2) {
            z2 = true;
        }
        int i6 = z2 ? 1 : 0;
        int i7 = z2 ? 1 : 0;
        int i8 = z2 ? 1 : 0;
        int i9 = (A04 + i6) * 31;
        if (!this.A02) {
            i = 0;
        }
        int i10 = (i9 + i) * 31;
        String str = this.A00;
        if (str != null) {
            i5 = str.hashCode();
        }
        return i10 + i5;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("CtwaUserJourneyLogger(isBusinessAccount=");
        A0k.append(this.A04);
        A0k.append(", phoneNumber=");
        A0k.append((Object) this.A03);
        A0k.append(", hasAdContext=");
        A0k.append(this.A01);
        A0k.append(", icebreakersShown=");
        A0k.append(this.A02);
        A0k.append(", adId=");
        A0k.append((Object) this.A00);
        return C12970iu.A0u(A0k);
    }
}
