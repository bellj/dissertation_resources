package X;

import android.content.Context;
import android.widget.TextView;
import com.whatsapp.R;
import java.util.List;

/* renamed from: X.2ze  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C61242ze extends AbstractCallableC112595Dz {
    public final int A00;
    public final Context A01;
    public final TextView A02;
    public final AnonymousClass018 A03;
    public final AnonymousClass1BK A04;
    public final AnonymousClass19M A05;
    public final AbstractC15340mz A06;
    public final List A07;

    public C61242ze(Context context, TextView textView, AnonymousClass018 r3, AnonymousClass1BK r4, AnonymousClass19M r5, AbstractC15340mz r6, List list, int i) {
        this.A01 = context;
        this.A05 = r5;
        this.A03 = r3;
        this.A04 = r4;
        this.A00 = i;
        this.A06 = r6;
        this.A02 = textView;
        this.A07 = list;
    }

    @Override // X.AbstractCallableC112595Dz
    public /* bridge */ /* synthetic */ Object A01() {
        CharSequence A02;
        Context context = this.A01;
        C64643Gi A00 = C64643Gi.A00(context, this.A04, this.A06, this.A00);
        AnonymousClass02N r6 = super.A00;
        r6.A02();
        String str = A00.A01;
        if (str == null) {
            A02 = context.getString(R.string.view_message);
        } else {
            A02 = AnonymousClass3J9.A02(context, this.A03, AbstractC36671kL.A03(context, this.A02.getPaint(), this.A05, str), this.A07);
        }
        r6.A02();
        String str2 = A00.A03;
        List list = this.A07;
        AnonymousClass018 r1 = this.A03;
        CharSequence A022 = AnonymousClass3J9.A02(context, r1, str2, list);
        CharSequence A023 = AnonymousClass3J9.A02(context, r1, A00.A02, list);
        r6.A02();
        return new C91354Rk(A00, A022, A023, A02);
    }
}
