package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import java.util.Arrays;

/* renamed from: X.5qY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C125025qY {
    public static String[] A00(byte[] bArr) {
        int i;
        int length = bArr.length;
        if (length <= 3072) {
            String[] strArr = new String[3];
            int i2 = 0;
            while (true) {
                i = i2 << 10;
                if (length <= 1024) {
                    break;
                }
                strArr[i2] = new String(Arrays.copyOfRange(bArr, i, i + EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH));
                length -= 1024;
                i2++;
            }
            if (length > 0) {
                strArr[i2] = new String(Arrays.copyOfRange(bArr, i, length + i));
            }
            return strArr;
        }
        throw C12960it.A0U("payload size exceeded limit");
    }
}
