package X;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.2hO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54852hO extends AbstractC05270Ox {
    public int A00 = -1;
    public boolean A01;
    public final AnonymousClass0FB A02;
    public final C14260l7 A03;
    public final AnonymousClass28D A04;
    public final AbstractC14200l1 A05;

    public C54852hO(AnonymousClass0FB r2, C14260l7 r3, AnonymousClass28D r4, AbstractC14200l1 r5) {
        this.A02 = r2;
        this.A05 = r5;
        this.A03 = r3;
        this.A04 = r4;
    }

    @Override // X.AbstractC05270Ox
    public void A00(RecyclerView recyclerView, int i) {
        AnonymousClass02H layoutManager;
        View A01;
        boolean z = true;
        if (i != 1) {
            if (i == 0 && (layoutManager = recyclerView.getLayoutManager()) != null && (A01 = this.A02.A01(layoutManager)) != null) {
                int A00 = RecyclerView.A00(A01);
                if (A00 != this.A00 || this.A01) {
                    C14210l2 r2 = new C14210l2();
                    r2.A05(Integer.valueOf(A00), 0);
                    C14260l7 r3 = this.A03;
                    C28701Oq.A01(r3, this.A04, C14210l2.A01(r2, r3, 1), this.A05);
                    this.A00 = A00;
                    z = false;
                } else {
                    return;
                }
            } else {
                return;
            }
        }
        this.A01 = z;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            C54852hO r5 = (C54852hO) obj;
            if (this.A02 == r5.A02 && this.A05 == r5.A05 && this.A04.A00 == r5.A04.A00) {
                return true;
            }
            return false;
        }
        return false;
    }

    public int hashCode() {
        return (C12990iw.A08(this.A05, this.A02.hashCode() * 31) * 31) + this.A04.A00;
    }
}
