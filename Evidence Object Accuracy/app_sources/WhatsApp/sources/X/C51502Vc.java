package X;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.wds.components.profilephoto.WDSProfilePhoto;

/* renamed from: X.2Vc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C51502Vc implements AnonymousClass28F {
    public final AnonymousClass130 A00;
    public final C15370n3 A01;
    public final AnonymousClass11F A02;
    public final C20710wC A03;

    public C51502Vc(AnonymousClass130 r1, C15370n3 r2, AnonymousClass11F r3, C20710wC r4) {
        this.A00 = r1;
        this.A03 = r4;
        this.A02 = r3;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass28F
    public void Adh(Bitmap bitmap, ImageView imageView, boolean z) {
        if (bitmap != null) {
            Drawable drawable = imageView.getDrawable();
            if (drawable == null || z) {
                imageView.setImageBitmap(bitmap);
                return;
            }
            if (drawable instanceof LayerDrawable) {
                LayerDrawable layerDrawable = (LayerDrawable) drawable;
                if (layerDrawable.getNumberOfLayers() > 0) {
                    drawable = layerDrawable.getDrawable(layerDrawable.getNumberOfLayers() - 1);
                }
            }
            TransitionDrawable transitionDrawable = new TransitionDrawable(new Drawable[]{drawable, new BitmapDrawable(bitmap)});
            transitionDrawable.setCrossFadeEnabled(false);
            transitionDrawable.startTransition(300);
            imageView.setImageDrawable(transitionDrawable);
            return;
        }
        Adv(imageView);
    }

    @Override // X.AnonymousClass28F
    public void Adv(ImageView imageView) {
        Drawable A00;
        int i = R.drawable.avatar_contact;
        AbstractC469028d r4 = AnonymousClass51L.A00;
        C15370n3 r2 = this.A01;
        if (r2 != null) {
            i = this.A00.A01(r2);
            if (this.A03.A0b(C15580nU.A02(r2.A0D))) {
                r4 = AnonymousClass51K.A00;
            }
        }
        if (imageView instanceof WDSProfilePhoto) {
            A00 = AnonymousClass00X.A04(imageView.getContext().getTheme(), imageView.getResources(), i);
        } else {
            A00 = this.A02.A00(imageView.getContext().getTheme(), imageView.getResources(), r4, i);
        }
        imageView.setImageDrawable(A00);
    }
}
