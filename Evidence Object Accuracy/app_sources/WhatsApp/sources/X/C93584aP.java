package X;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/* renamed from: X.4aP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C93584aP {
    public static final AnonymousClass4MB A03 = new AnonymousClass4MB(2, -9223372036854775807L);
    public static final AnonymousClass4MB A04 = new AnonymousClass4MB(3, -9223372036854775807L);
    public HandlerC52112aG A00;
    public IOException A01;
    public final ExecutorService A02 = Executors.newSingleThreadExecutor(new ThreadFactory() { // from class: X.5EB
        public final /* synthetic */ String A00 = "Loader:ProgressiveMediaPeriod";

        @Override // java.util.concurrent.ThreadFactory
        public final Thread newThread(Runnable runnable) {
            return new Thread(runnable, this.A00);
        }
    });
}
