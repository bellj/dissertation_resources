package X;

import java.security.KeyPair;

/* renamed from: X.5fK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119925fK extends AbstractC128495wC {
    public final /* synthetic */ C64063Ec A00;
    public final /* synthetic */ AnonymousClass3EB A01;
    public final /* synthetic */ AnonymousClass68O A02;
    public final /* synthetic */ KeyPair A03;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C119925fK(C64063Ec r1, AnonymousClass3EB r2, AnonymousClass3EB r3, AnonymousClass68O r4, KeyPair keyPair) {
        super(r2);
        this.A02 = r4;
        this.A03 = keyPair;
        this.A01 = r3;
        this.A00 = r1;
    }
}
