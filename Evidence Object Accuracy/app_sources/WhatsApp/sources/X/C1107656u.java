package X;

import android.graphics.Bitmap;
import com.whatsapp.gallery.MediaGalleryFragmentBase;

/* renamed from: X.56u  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1107656u implements AnonymousClass23D {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AbstractC35611iN A01;
    public final /* synthetic */ AnonymousClass35H A02;

    @Override // X.AnonymousClass23D
    public String AH5() {
        return "MY_PHOTOS_VIEW_HOLDER_TAG";
    }

    public C1107656u(AbstractC35611iN r1, AnonymousClass35H r2, int i) {
        this.A02 = r2;
        this.A01 = r1;
        this.A00 = i;
    }

    @Override // X.AnonymousClass23D
    public Bitmap AKU() {
        if (this.A02.A05.getTag() != this) {
            return null;
        }
        Bitmap Aem = this.A01.Aem(this.A00);
        if (Aem == null) {
            return MediaGalleryFragmentBase.A0U;
        }
        return Aem;
    }
}
