package X;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.3Wk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68703Wk implements AbstractC28051Kk {
    public final /* synthetic */ long A00 = 10000000;
    public final /* synthetic */ AnonymousClass31M A01;
    public final /* synthetic */ C17090qF A02;
    public final /* synthetic */ AtomicBoolean A03;

    @Override // X.AbstractC28051Kk
    public void ANt(C89454Ka r1) {
    }

    @Override // X.AbstractC28051Kk
    public void AOw(C28011Kc r1, AbstractC14640lm r2) {
    }

    public C68703Wk(AnonymousClass31M r3, C17090qF r4, AtomicBoolean atomicBoolean) {
        this.A02 = r4;
        this.A01 = r3;
        this.A03 = atomicBoolean;
    }

    @Override // X.AbstractC28051Kk
    public void ANs(AnonymousClass4KZ r6) {
        Iterator it = r6.A00.iterator();
        long j = 0;
        while (it.hasNext()) {
            j += ((C28021Kd) it.next()).A00.A0G;
        }
        AnonymousClass31M r2 = this.A01;
        long j2 = this.A00;
        r2.A01 = Long.valueOf((j / j2) * j2);
        this.A03.set(true);
        this.A02.A07.A08.remove(this);
    }
}
