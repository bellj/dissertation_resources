package X;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.text.SpannableString;
import android.text.util.Linkify;
import com.whatsapp.R;

/* renamed from: X.1Bh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C25911Bh {
    public final AnonymousClass12P A00;
    public final C16590pI A01;

    public C25911Bh(AnonymousClass12P r1, C16590pI r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public static String A00(String str) {
        int length = str.length();
        if (length <= 96) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(str.substring(0, 64));
        sb.append("…");
        sb.append(str.substring(length - 32));
        return sb.toString();
    }

    public void A01(Context context, Uri uri) {
        SpannableString spannableString = new SpannableString(this.A01.A00.getString(R.string.link_taking_to, A00(uri.toString())));
        Linkify.addLinks(spannableString, 1);
        C004802e r2 = new C004802e(context, R.style.AlertDialogExternalLink);
        r2.A0A(spannableString);
        r2.setNegativeButton(R.string.cancel, null);
        r2.setPositiveButton(R.string.btn_continue, new DialogInterface.OnClickListener(context, uri, this) { // from class: X.4h1
            public final /* synthetic */ Context A00;
            public final /* synthetic */ Uri A01;
            public final /* synthetic */ C25911Bh A02;

            {
                this.A02 = r3;
                this.A00 = r1;
                this.A01 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                C25911Bh r4 = this.A02;
                r4.A00.A06(this.A00, new Intent("android.intent.action.VIEW", this.A01));
            }
        });
        r2.A05();
    }
}
