package X;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.IAccountAccessor;

/* renamed from: X.3ox  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78463ox extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C98734jA();
    public final int A00;
    public final IBinder A01;
    public final C56492ky A02;
    public final boolean A03;
    public final boolean A04;

    public C78463ox(IBinder iBinder, C56492ky r2, int i, boolean z, boolean z2) {
        this.A00 = i;
        this.A01 = iBinder;
        this.A02 = r2;
        this.A03 = z;
        this.A04 = z2;
    }

    public final IAccountAccessor A00() {
        IBinder iBinder = this.A01;
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IAccountAccessor");
        return queryLocalInterface instanceof IAccountAccessor ? (IAccountAccessor) queryLocalInterface : new C79543ql(iBinder);
    }

    @Override // java.lang.Object
    public final boolean equals(Object obj) {
        if (obj != null) {
            if (this != obj) {
                if (obj instanceof C78463ox) {
                    C78463ox r5 = (C78463ox) obj;
                    if (!this.A02.equals(r5.A02) || !C13300jT.A00(A00(), r5.A00())) {
                    }
                }
            }
            return true;
        }
        return false;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 1, this.A00);
        C95654e8.A04(this.A01, parcel, 2);
        C95654e8.A0B(parcel, this.A02, 3, i, false);
        C95654e8.A09(parcel, 4, this.A03);
        C95654e8.A09(parcel, 5, this.A04);
        C95654e8.A06(parcel, A00);
    }
}
