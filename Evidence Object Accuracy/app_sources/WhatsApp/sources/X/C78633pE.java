package X;

import android.os.Parcel;

/* renamed from: X.3pE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C78633pE extends AnonymousClass1U5 {
    public static final C98804jH CREATOR = new C98804jH();
    public AbstractC115085Qd A00;
    public C78443ov A01;
    public final int A02;
    public final int A03;
    public final int A04;
    public final int A05;
    public final Class A06;
    public final String A07;
    public final String A08;
    public final boolean A09;
    public final boolean A0A;

    public C78633pE(C78513p2 r3, String str, String str2, int i, int i2, int i3, int i4, boolean z, boolean z2) {
        this.A05 = i;
        this.A02 = i2;
        this.A09 = z;
        this.A03 = i3;
        this.A0A = z2;
        this.A07 = str;
        this.A04 = i4;
        C78693pN r1 = null;
        if (str2 == null) {
            this.A06 = null;
            this.A08 = null;
        } else {
            this.A06 = C78763pV.class;
            this.A08 = str2;
        }
        if (r3 == null || (r1 = r3.A01) != null) {
            this.A00 = r1;
            return;
        }
        throw C12960it.A0U("There was no converter wrapped in this ConverterWrapper.");
    }

    public C78633pE(Class cls, String str, int i, int i2, int i3, boolean z, boolean z2) {
        String canonicalName;
        this.A05 = 1;
        this.A02 = i;
        this.A09 = z;
        this.A03 = i2;
        this.A0A = z2;
        this.A07 = str;
        this.A04 = i3;
        this.A06 = cls;
        if (cls == null) {
            canonicalName = null;
        } else {
            canonicalName = cls.getCanonicalName();
        }
        this.A08 = canonicalName;
        this.A00 = null;
    }

    public static C78633pE A00(String str, int i) {
        return new C78633pE(null, str, 7, 7, i, true, true);
    }

    @Override // java.lang.Object
    public final String toString() {
        C13290jS r2 = new C13290jS(this);
        r2.A00(Integer.valueOf(this.A05), "versionCode");
        r2.A00(Integer.valueOf(this.A02), "typeIn");
        r2.A00(Boolean.valueOf(this.A09), "typeInArray");
        r2.A00(Integer.valueOf(this.A03), "typeOut");
        r2.A00(Boolean.valueOf(this.A0A), "typeOutArray");
        r2.A00(this.A07, "outputFieldName");
        r2.A00(Integer.valueOf(this.A04), "safeParcelFieldId");
        String str = this.A08;
        if (str == null) {
            str = null;
        }
        r2.A00(str, "concreteTypeName");
        Class cls = this.A06;
        if (cls != null) {
            r2.A00(cls.getCanonicalName(), "concreteType.class");
        }
        AbstractC115085Qd r0 = this.A00;
        if (r0 != null) {
            r2.A00(r0.getClass().getCanonicalName(), "converterName");
        }
        return r2.toString();
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        C78513p2 r1;
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 1, this.A05);
        C95654e8.A07(parcel, 2, this.A02);
        C95654e8.A09(parcel, 3, this.A09);
        C95654e8.A07(parcel, 4, this.A03);
        C95654e8.A09(parcel, 5, this.A0A);
        C95654e8.A0D(parcel, this.A07, 6, false);
        C95654e8.A07(parcel, 7, this.A04);
        String str = this.A08;
        if (str == null) {
            str = null;
        }
        C95654e8.A0D(parcel, str, 8, false);
        AbstractC115085Qd r2 = this.A00;
        if (r2 == null) {
            r1 = null;
        } else if (r2 instanceof C78693pN) {
            r1 = new C78513p2((C78693pN) r2);
        } else {
            throw C12970iu.A0f("Unsupported safe parcelable field converter class.");
        }
        C95654e8.A0B(parcel, r1, 9, i, false);
        C95654e8.A06(parcel, A00);
    }
}
