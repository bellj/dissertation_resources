package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0101000_I0;

/* renamed from: X.2QF  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2QF extends AbstractC32541cK implements AnonymousClass2N4 {
    public final C14900mE A00;
    public final C15450nH A01;
    public final AnonymousClass018 A02;
    public final C21320xE A03;
    public final C15580nU A04;
    public final C20660w7 A05;
    public final C22230yk A06;
    public final C14860mA A07;
    public final String A08;

    public /* synthetic */ AnonymousClass2QF(C14900mE r1, C15450nH r2, AnonymousClass018 r3, C21320xE r4, C15580nU r5, C20660w7 r6, C22230yk r7, C14860mA r8, String str) {
        this.A00 = r1;
        this.A07 = r8;
        this.A05 = r6;
        this.A01 = r2;
        this.A06 = r7;
        this.A02 = r3;
        this.A03 = r4;
        this.A04 = r5;
        this.A08 = str;
    }

    @Override // X.AnonymousClass2N4
    public boolean AK7() {
        return super.A00;
    }

    @Override // X.AnonymousClass2N4
    public void APl(int i) {
        super.A01.cancel();
        this.A00.A0H(new RunnableBRunnable0Shape0S0101000_I0(this, i, 11));
        C14860mA r0 = this.A07;
        String str = this.A08;
        r0.A0H(str, i);
        this.A06.A0F(str, i);
        this.A03.A09(this.A04, false);
    }

    @Override // X.AnonymousClass2N4
    public void AWu() {
        super.A01.cancel();
        C14860mA r0 = this.A07;
        String str = this.A08;
        r0.A0H(str, 200);
        this.A06.A0F(str, 200);
        this.A03.A09(this.A04, false);
    }
}
