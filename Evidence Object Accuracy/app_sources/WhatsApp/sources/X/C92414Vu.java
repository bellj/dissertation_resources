package X;

/* renamed from: X.4Vu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92414Vu {
    public int A00 = -1;
    public long A01;
    public AbstractC35611iN A02;
    public final int A03;
    public final AbstractC35581iK A04;

    public C92414Vu(AbstractC35581iK r2, int i) {
        this.A04 = r2;
        this.A03 = i;
    }

    public boolean A00() {
        int i = this.A00;
        AbstractC35581iK r1 = this.A04;
        if (i >= r1.getCount() - 1) {
            return false;
        }
        int i2 = this.A00 + 1;
        this.A00 = i2;
        AbstractC35611iN AEC = r1.AEC(i2);
        this.A02 = AEC;
        this.A01 = AEC.ACQ();
        return true;
    }
}
