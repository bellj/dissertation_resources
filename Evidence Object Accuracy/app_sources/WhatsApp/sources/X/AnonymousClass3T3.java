package X;

import android.os.RemoteException;

/* renamed from: X.3T3  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3T3 implements AbstractC116355Vc {
    public final /* synthetic */ C64163Em A00;

    public AnonymousClass3T3(C64163Em r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC116355Vc
    public final int AgL() {
        return 5;
    }

    @Override // X.AbstractC116355Vc
    public final void AgQ(AbstractC115105Qf r4) {
        try {
            C65873Li r2 = (C65873Li) ((AnonymousClass3T2) this.A00.A01).A02;
            r2.A03(3, r2.A01());
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }
}
