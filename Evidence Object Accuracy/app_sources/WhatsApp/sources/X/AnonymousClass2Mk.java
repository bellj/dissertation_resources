package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import com.whatsapp.HomeActivity;

/* renamed from: X.2Mk  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Mk extends AnimatorListenerAdapter {
    public final /* synthetic */ HomeActivity A00;

    public AnonymousClass2Mk(HomeActivity homeActivity) {
        this.A00 = homeActivity;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        super.onAnimationEnd(animator);
        HomeActivity homeActivity = this.A00;
        homeActivity.A0H.setIconified(true);
        homeActivity.A0D.setVisibility(4);
    }
}
