package X;

/* renamed from: X.5Mr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114685Mr extends AnonymousClass1TM {
    public AnonymousClass5N0 A00;
    public C114705Mt A01;
    public C114845Nh A02;

    public C114685Mr(AnonymousClass5N0 r2) {
        this.A00 = r2;
        this.A02 = null;
        this.A01 = null;
    }

    public C114685Mr(AbstractC114775Na r6) {
        AnonymousClass5N0 r0;
        for (int i = 0; i != r6.A0B(); i++) {
            AnonymousClass5NU A01 = AnonymousClass5NU.A01(r6.A0D(i));
            int i2 = A01.A00;
            if (i2 == 0) {
                AnonymousClass5NU A012 = AnonymousClass5NU.A01(AnonymousClass5NU.A00(A01));
                if (A012 != null) {
                    r0 = new AnonymousClass5N0(A012);
                } else {
                    r0 = null;
                }
                this.A00 = r0;
            } else if (i2 == 1) {
                this.A02 = new C114845Nh(AnonymousClass5MA.A01(A01));
            } else if (i2 == 2) {
                this.A01 = new C114705Mt(AbstractC114775Na.A05(A01, false));
            } else {
                throw C12970iu.A0f(C12960it.A0f(C12960it.A0k("Unknown tag encountered in structure: "), A01.A00));
            }
        }
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        C94954co r3 = new C94954co(3);
        AnonymousClass5N0 r1 = this.A00;
        if (r1 != null) {
            r3.A06(new C114835Ng(r1));
        }
        C114845Nh r12 = this.A02;
        if (r12 != null) {
            C94954co.A02(r12, r3, 1, false);
        }
        C114705Mt r13 = this.A01;
        if (r13 != null) {
            C94954co.A02(r13, r3, 2, false);
        }
        return new AnonymousClass5NZ(r3);
    }

    public String toString() {
        String str = AnonymousClass1T7.A00;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("DistributionPoint: [");
        stringBuffer.append(str);
        AnonymousClass5N0 r0 = this.A00;
        if (r0 != null) {
            C72453ed.A1N("distributionPoint", str, r0.toString(), stringBuffer);
        }
        C114845Nh r02 = this.A02;
        if (r02 != null) {
            C72453ed.A1N("reasons", str, r02.toString(), stringBuffer);
        }
        C114705Mt r03 = this.A01;
        if (r03 != null) {
            C72453ed.A1N("cRLIssuer", str, r03.toString(), stringBuffer);
        }
        stringBuffer.append("]");
        stringBuffer.append(str);
        return stringBuffer.toString();
    }
}
