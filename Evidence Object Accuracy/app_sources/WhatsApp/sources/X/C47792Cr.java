package X;

/* renamed from: X.2Cr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C47792Cr {
    public int A00;
    public String A01;
    public String A02;
    public String A03;

    public C47792Cr(String str, String str2, String str3, int i) {
        this.A01 = str;
        this.A02 = str2;
        this.A00 = i;
        this.A03 = str3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0018, code lost:
        if (r0 != null) goto L_0x001a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r5) {
        /*
            r4 = this;
            r3 = 1
            if (r4 == r5) goto L_0x0048
            r2 = 0
            if (r5 == 0) goto L_0x001a
            java.lang.Class r1 = r4.getClass()
            java.lang.Class r0 = r5.getClass()
            if (r1 != r0) goto L_0x001a
            X.2Cr r5 = (X.C47792Cr) r5
            java.lang.String r1 = r4.A01
            java.lang.String r0 = r5.A01
            if (r1 != 0) goto L_0x001b
            if (r0 == 0) goto L_0x0022
        L_0x001a:
            return r2
        L_0x001b:
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0022
            return r2
        L_0x0022:
            java.lang.String r1 = r4.A02
            java.lang.String r0 = r5.A02
            if (r1 != 0) goto L_0x002b
            if (r0 == 0) goto L_0x0032
            return r2
        L_0x002b:
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0032
            return r2
        L_0x0032:
            int r1 = r4.A00
            int r0 = r5.A00
            if (r1 != r0) goto L_0x001a
            java.lang.String r1 = r4.A03
            java.lang.String r0 = r5.A03
            if (r1 != 0) goto L_0x0041
            if (r0 == 0) goto L_0x0048
            return r2
        L_0x0041:
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0048
            return r2
        L_0x0048:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C47792Cr.equals(java.lang.Object):boolean");
    }

    public int hashCode() {
        int hashCode;
        int hashCode2;
        String str = this.A01;
        int i = 0;
        if (str == null) {
            hashCode = 0;
        } else {
            hashCode = str.hashCode();
        }
        int i2 = (hashCode + 31) * 31;
        String str2 = this.A02;
        if (str2 == null) {
            hashCode2 = 0;
        } else {
            hashCode2 = str2.hashCode();
        }
        int i3 = (((i2 + hashCode2) * 31) + this.A00) * 31;
        String str3 = this.A03;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return i3 + i;
    }
}
