package X;

import android.content.Context;
import java.util.List;

/* renamed from: X.286  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass286 extends AbstractC16350or {
    public int A00;
    public int A01;
    public final int A02;
    public final Context A03;
    public final C14900mE A04;
    public final AnonymousClass018 A05;
    public final AnonymousClass287 A06;
    public final AnonymousClass2TK A07;
    public final AnonymousClass2WM A08;
    public final List A09;
    public final boolean A0A;

    public AnonymousClass286(Context context, AbstractC001200n r2, C14900mE r3, AnonymousClass018 r4, AnonymousClass287 r5, AnonymousClass2TK r6, AnonymousClass2WM r7, List list, int i, boolean z) {
        super(r2);
        this.A06 = r5;
        this.A03 = context;
        this.A04 = r3;
        this.A0A = z;
        this.A09 = list;
        this.A05 = r4;
        this.A07 = r6;
        this.A08 = r7;
        this.A02 = i;
    }
}
