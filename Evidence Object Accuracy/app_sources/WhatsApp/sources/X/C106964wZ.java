package X;

import com.google.android.search.verification.client.SearchActionVerificationClientService;

/* renamed from: X.4wZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106964wZ implements AnonymousClass5WY {
    public final int A00;
    public final int A01;
    public final long A02;
    public final long A03;
    public final long A04;
    public final long A05;

    public C106964wZ(int i, int i2, long j, long j2) {
        long max;
        this.A05 = j;
        this.A04 = j2;
        this.A01 = i2 == -1 ? 1 : i2;
        this.A00 = i;
        if (j == -1) {
            this.A02 = -1;
            max = -9223372036854775807L;
        } else {
            long j3 = j - j2;
            this.A02 = j3;
            max = ((Math.max(0L, j3) * 8) * SearchActionVerificationClientService.MS_TO_NS) / ((long) i);
        }
        this.A03 = max;
    }

    public long A00(long j) {
        return ((Math.max(0L, j - this.A04) * 8) * SearchActionVerificationClientService.MS_TO_NS) / ((long) this.A00);
    }

    @Override // X.AnonymousClass5WY
    public long ACc() {
        return this.A03;
    }

    @Override // X.AnonymousClass5WY
    public C92684Xa AGX(long j) {
        long j2 = this.A02;
        if (j2 == -1) {
            C94324bc r1 = new C94324bc(0, this.A04);
            return new C92684Xa(r1, r1);
        }
        long j3 = (long) this.A01;
        long A0X = this.A04 + C72453ed.A0X((((j * ((long) this.A00)) / 8000000) / j3) * j3, j2 - j3);
        long A00 = A00(A0X);
        C94324bc r7 = new C94324bc(A00, A0X);
        if (A00 < j) {
            long j4 = j3 + A0X;
            if (j4 < this.A05) {
                return C92684Xa.A00(r7, A00(j4), j4);
            }
        }
        return new C92684Xa(r7, r7);
    }

    @Override // X.AnonymousClass5WY
    public boolean AK2() {
        return C12960it.A1S((this.A02 > -1 ? 1 : (this.A02 == -1 ? 0 : -1)));
    }
}
