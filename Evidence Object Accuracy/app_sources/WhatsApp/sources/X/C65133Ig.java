package X;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Stack;

/* renamed from: X.3Ig  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C65133Ig {
    public AnonymousClass4AJ A00;
    public boolean A01;
    public boolean A02;
    public boolean A03;
    public final AnonymousClass17M A04;
    public final C89684Kx A05;
    public final C64373Fh A06;
    public final C50132Og A07;
    public final C17120qI A08;
    public final String A09;

    public C65133Ig(AnonymousClass17M r4, C89684Kx r5, C64373Fh r6, C17120qI r7, String str) {
        C16700pc.A0E(r7, 1);
        C16700pc.A0E(r4, 3);
        C16700pc.A0E(str, 4);
        this.A08 = r7;
        this.A06 = r6;
        this.A04 = r4;
        this.A09 = str;
        this.A05 = r5;
        C50132Og A02 = r7.A02(str);
        this.A07 = A02;
        A02.A00(new AbstractC50172Ok() { // from class: X.59y
            @Override // X.AbstractC50172Ok
            public final void APz(Object obj) {
                C65133Ig.A00(C65133Ig.this, (AnonymousClass6EA) obj);
            }
        }, AnonymousClass6EA.class, this);
        A02.A00(new AbstractC50172Ok() { // from class: X.59x
            @Override // X.AbstractC50172Ok
            public final void APz(Object obj) {
                C65133Ig r1 = C65133Ig.this;
                C16700pc.A0E(obj, 1);
                r1.A02 = true;
            }
        }, AnonymousClass5A2.class, this);
    }

    public static /* synthetic */ void A00(C65133Ig r4, AnonymousClass6EA r5) {
        C16700pc.A0E(r5, 1);
        C27661Io r3 = r4.A05.A00;
        C27661Io.A00(r3, "actionPerformed");
        r3.A04("action_performed", "cancel");
        C19630uQ r0 = r3.A0E;
        r0.A02.A05(r3.A00, 4);
        r4.A05(r5.A00, null, r5.A01);
    }

    public static void A01(C65133Ig r3, Map map, C17520qw r5) {
        C89684Kx r32 = r3.A05;
        String str = ((AnonymousClass4VP) r5.first).A01;
        C16700pc.A0E(str, 0);
        r32.A00.A05(str, map, (Map) r5.second);
    }

    public static boolean A02(Object obj, Object obj2) {
        return C16700pc.A0O(((AnonymousClass4VP) ((C17520qw) obj).first).A01, obj2);
    }

    public final ArrayList A03() {
        boolean z = this.A02;
        C64373Fh r2 = this.A06;
        ArrayList A01 = r2.A01();
        if (!z) {
            return A01;
        }
        if (A01.size() <= 1) {
            return r2.A00();
        }
        ArrayList A012 = r2.A01();
        ArrayList A00 = r2.A00();
        if (A00 instanceof Collection) {
            ArrayList A0w = C12980iv.A0w(A012.size() + A00.size());
            A0w.addAll(A012);
            A0w.addAll(A00);
            return A0w;
        }
        ArrayList A0x = C12980iv.A0x(A012);
        AnonymousClass01Z.A0B(A00, A0x);
        return A0x;
    }

    public final void A04(String str, Map map) {
        ArrayList A03 = A03();
        if (A03 instanceof Collection) {
            if (!A03.contains(str)) {
                return;
            }
        } else if (AnonymousClass01Y.A00(A03, str) < 0) {
            return;
        }
        C64373Fh r4 = this.A06;
        Stack stack = r4.A00;
        while (true) {
            Object lastElement = stack.lastElement();
            C16700pc.A0B(lastElement);
            if (A02(lastElement, str)) {
                break;
            }
            r4.A02();
        }
        ArrayList A01 = r4.A01();
        if (!A01.isEmpty()) {
            A01.remove(A01.size() - 1);
            this.A07.A01(new AnonymousClass6E7(A01));
            Object peek = stack.peek();
            C16700pc.A0B(peek);
            r4.A02();
            A01(this, map, (C17520qw) peek);
            return;
        }
        throw new NoSuchElementException("List is empty.");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00cd, code lost:
        if (r9 == null) goto L_0x00f7;
     */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0102  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A05(java.lang.String r9, java.util.Map r10, boolean r11) {
        /*
        // Method dump skipped, instructions count: 398
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C65133Ig.A05(java.lang.String, java.util.Map, boolean):void");
    }
}
