package X;

import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import java.util.List;
import java.util.Map;

/* renamed from: X.2E1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2E1 {
    public final int A00;
    public final int A01;
    public final int A02;
    public final long A03;
    public final AnonymousClass1IR A04;
    public final DeviceJid A05;
    public final Jid A06;
    public final C27631Ih A07;
    public final UserJid A08;
    public final C15930o9 A09;
    public final EnumC35661iT A0A;
    public final AnonymousClass1IS A0B;
    public final C32141bg A0C;
    public final AnonymousClass1V8 A0D;
    public final AnonymousClass1OT A0E;
    public final Integer A0F;
    public final Integer A0G;
    public final String A0H;
    public final String A0I;
    public final String A0J;
    public final String A0K;
    public final String A0L;
    public final String A0M;
    public final List A0N;
    public final List A0O;
    public final Map A0P;
    public final Map A0Q;
    public final Map A0R;
    public final boolean A0S;
    public final boolean A0T;

    public AnonymousClass2E1(AnonymousClass1IR r3, DeviceJid deviceJid, Jid jid, C27631Ih r6, UserJid userJid, C15930o9 r8, EnumC35661iT r9, AnonymousClass1IS r10, C32141bg r11, AnonymousClass1V8 r12, AnonymousClass1OT r13, Integer num, Integer num2, String str, String str2, String str3, String str4, String str5, String str6, List list, List list2, Map map, Map map2, Map map3, int i, int i2, int i3, long j, boolean z, boolean z2) {
        this.A0E = r13;
        this.A0B = r10;
        this.A06 = jid;
        this.A03 = j;
        this.A02 = i;
        this.A07 = r6;
        this.A05 = deviceJid;
        this.A08 = userJid;
        this.A0I = str;
        this.A0K = str2;
        this.A0J = str3;
        this.A0A = r9;
        this.A09 = r8;
        this.A0P = map;
        this.A0R = map2;
        this.A0N = list;
        this.A01 = i2;
        this.A0O = list2;
        this.A00 = i3;
        this.A0F = num;
        this.A04 = r3;
        this.A0Q = map3;
        this.A0G = num2;
        this.A0H = str4;
        this.A0M = str5;
        this.A0T = z;
        this.A0D = r12;
        this.A0C = r11;
        this.A0S = z2;
        this.A0L = str6;
    }
}
