package X;

/* renamed from: X.6K3  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6K3 implements Runnable {
    public final /* synthetic */ AbstractC129405xf A00;
    public final /* synthetic */ AnonymousClass6LA A01;
    public final /* synthetic */ Exception A02;
    public final /* synthetic */ Object A03;
    public final /* synthetic */ boolean A04;

    public AnonymousClass6K3(AbstractC129405xf r1, AnonymousClass6LA r2, Exception exc, Object obj, boolean z) {
        this.A01 = r2;
        this.A04 = z;
        this.A00 = r1;
        this.A03 = obj;
        this.A02 = exc;
    }

    @Override // java.lang.Runnable
    public void run() {
        if (this.A04) {
            this.A00.A01(this.A03);
            return;
        }
        this.A00.A00(this.A02);
    }
}
