package X;

/* renamed from: X.228  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass228 {
    public static C27081Fy A00(C27081Fy r2) {
        if ((r2.A00 & 33554432) != 33554432) {
            return r2;
        }
        C57312mp r0 = r2.A0B;
        if (r0 == null) {
            r0 = C57312mp.A04;
        }
        C27081Fy r22 = r0.A01;
        if (r22 == null) {
            return C27081Fy.A0i;
        }
        return r22;
    }
}
