package X;

import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.net.http.SslError;
import android.text.TextUtils;
import android.webkit.SafeBrowsingResponse;
import android.webkit.SslErrorHandler;
import android.webkit.URLUtil;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.whatsapp.R;
import com.whatsapp.WaInAppBrowsingActivity;
import com.whatsapp.util.Log;
import java.io.ByteArrayInputStream;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.2bU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52672bU extends WebViewClient {
    public final /* synthetic */ WaInAppBrowsingActivity A00;

    public /* synthetic */ C52672bU(WaInAppBrowsingActivity waInAppBrowsingActivity) {
        this.A00 = waInAppBrowsingActivity;
    }

    public static C93664aX A00(Uri uri, C90474Oa r9) {
        String str;
        C91034Qe r1;
        String str2;
        String scheme = uri.getScheme();
        String authority = uri.getAuthority();
        if (scheme == null || "".equals(scheme) || authority == null || "".equals(authority)) {
            r1 = new C91034Qe();
            r1.A01 = uri.getPath();
            r1.A02 = scheme;
            r1.A00 = authority;
            str = uri.getQuery();
        } else {
            if (TextUtils.isEmpty(uri.getPath())) {
                str2 = null;
            } else {
                str2 = "/--sanitized--";
            }
            str = null;
            if (!TextUtils.isEmpty(uri.getQuery())) {
                try {
                    Set<String> queryParameterNames = uri.getQueryParameterNames();
                    if (queryParameterNames != null && !queryParameterNames.isEmpty()) {
                        StringBuilder A0h = C12960it.A0h();
                        Collections.unmodifiableList(r9.A00);
                        Iterator<String> it = queryParameterNames.iterator();
                        while (it.hasNext()) {
                            String A0x = C12970iu.A0x(it);
                            if (A0h.length() > 0) {
                                A0h.append('&');
                            }
                            A0h.append(A0x);
                            A0h.append("=--sanitized--");
                        }
                        str = A0h.toString();
                    }
                } catch (UnsupportedOperationException unused) {
                }
            }
            r1 = new C91034Qe();
            r1.A02 = scheme;
            r1.A00 = authority;
            r1.A01 = str2;
        }
        return new C93664aX(r1.A02, r1.A00, r1.A01, str);
    }

    public static String A01(Uri uri) {
        C93664aX A00 = A00(uri, C93664aX.A04);
        StringBuilder A0h = C12960it.A0h();
        String str = A00.A03;
        if (!TextUtils.isEmpty(str)) {
            A0h.append(str);
            A0h.append(':');
        }
        String str2 = A00.A00;
        if (!TextUtils.isEmpty(str2)) {
            A0h.append("//");
            A0h.append(str2);
        }
        String str3 = A00.A01;
        if (!TextUtils.isEmpty(str3)) {
            A0h.append(str3);
        }
        String str4 = A00.A02;
        if (!TextUtils.isEmpty(str4)) {
            A0h.append('?');
            A0h.append(str4);
        }
        return A0h.toString();
    }

    @Override // android.webkit.WebViewClient
    public void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        if (webView != null) {
            WaInAppBrowsingActivity waInAppBrowsingActivity = this.A00;
            WaInAppBrowsingActivity.A02(waInAppBrowsingActivity, webView.getUrl());
            if (!"about:blank".equals(webView.getTitle())) {
                waInAppBrowsingActivity.A2e(webView.getTitle());
            }
        }
    }

    @Override // android.webkit.WebViewClient
    public void onReceivedError(WebView webView, int i, String str, String str2) {
        StringBuilder A0k = C12960it.A0k("WaInappBrowsingActivity/onReceivedError: Error loading the page ");
        String str3 = A00(Uri.parse(str2), C93664aX.A04).A01;
        if (str3 == null) {
            str3 = "";
        }
        C12990iw.A1T(A0k, str3);
        Log.e(C12960it.A0d(str, A0k));
        webView.loadUrl("about:blank");
        WaInAppBrowsingActivity waInAppBrowsingActivity = this.A00;
        waInAppBrowsingActivity.A2f(waInAppBrowsingActivity.getString(R.string.webview_error_not_available), true);
    }

    @Override // android.webkit.WebViewClient
    public void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
        onReceivedError(webView, webResourceError.getErrorCode(), webResourceError.getDescription().toString(), webResourceRequest.getUrl().toString());
    }

    @Override // android.webkit.WebViewClient
    public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
        StringBuilder A0k = C12960it.A0k("WaInappBrowsingActivity/onReceivedSslError: SSL Error while loading the page: ");
        A0k.append(sslError.getUrl());
        A0k.append(": Code ");
        Log.e(C12960it.A0f(A0k, sslError.getPrimaryError()));
        sslErrorHandler.cancel();
        webView.stopLoading();
        WaInAppBrowsingActivity waInAppBrowsingActivity = this.A00;
        waInAppBrowsingActivity.A2f(waInAppBrowsingActivity.getString(R.string.webview_error_not_trusted), true);
    }

    @Override // android.webkit.WebViewClient
    public void onSafeBrowsingHit(WebView webView, WebResourceRequest webResourceRequest, int i, SafeBrowsingResponse safeBrowsingResponse) {
        super.onSafeBrowsingHit(webView, webResourceRequest, i, safeBrowsingResponse);
        Log.e(C12960it.A0d(webView.getUrl(), C12960it.A0k("WaInappBrowsingActivity/onSafeBrowsingHit: Unsafe page hit: ")));
        WaInAppBrowsingActivity waInAppBrowsingActivity = this.A00;
        Intent A0A = C12970iu.A0A();
        String stringExtra = waInAppBrowsingActivity.getIntent().getStringExtra("webview_callback");
        if (stringExtra != null) {
            A0A.putExtra("webview_callback", stringExtra);
        }
        waInAppBrowsingActivity.setResult(0, A0A);
        waInAppBrowsingActivity.finish();
    }

    @Override // android.webkit.WebViewClient
    public WebResourceResponse shouldInterceptRequest(WebView webView, WebResourceRequest webResourceRequest) {
        return shouldInterceptRequest(webView, webResourceRequest.getUrl().toString());
    }

    @Override // android.webkit.WebViewClient
    public WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
        if (URLUtil.isHttpsUrl(str)) {
            return super.shouldInterceptRequest(webView, str);
        }
        Log.e(C12960it.A0d(str, C12960it.A0k("WaInappBrowsingActivity/shouldInterceptRequest: Cannot open resource trough a not encrypted channel: ")));
        return new WebResourceResponse("application/octet-stream", WaInAppBrowsingActivity.A06, new ByteArrayInputStream("".getBytes()));
    }

    @Override // android.webkit.WebViewClient
    public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
        return shouldOverrideUrlLoading(webView, webResourceRequest.getUrl().toString());
    }

    @Override // android.webkit.WebViewClient
    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        WaInAppBrowsingActivity waInAppBrowsingActivity = this.A00;
        if (!waInAppBrowsingActivity.A2g(str)) {
            Uri parse = Uri.parse(str);
            int A05 = waInAppBrowsingActivity.A04.A05(parse);
            if (A05 == 1 || A05 == 10) {
                try {
                    String url = waInAppBrowsingActivity.A00.getUrl();
                    boolean booleanExtra = waInAppBrowsingActivity.getIntent().getBooleanExtra("webview_avoid_external", false);
                    Resources resources = waInAppBrowsingActivity.getResources();
                    if (URLUtil.isHttpsUrl(str)) {
                        Uri parse2 = Uri.parse(url);
                        Uri parse3 = Uri.parse(str);
                        if (parse2 != null && booleanExtra) {
                            Log.e(C12960it.A0d(A01(Uri.parse(str)), C12960it.A0k("SecuredWebViewUtil/checkUrl: Tried to open external link when blocked: ")));
                            AnonymousClass009.A0C(resources.getString(R.string.webview_error_external_browsing_blocked), parse2.getHost().equals(parse3.getHost()));
                        }
                        waInAppBrowsingActivity.A2e(waInAppBrowsingActivity.getString(R.string.webview_loading));
                        WaInAppBrowsingActivity.A02(waInAppBrowsingActivity, "");
                        return false;
                    }
                    Log.e(C12960it.A0d(A01(Uri.parse(str)), C12960it.A0k("SecuredWebViewUtil/checkUrl: Tried to open non-HTTPS content on ")));
                    throw C12970iu.A0f(resources.getString(R.string.webview_error_not_https));
                } catch (IllegalArgumentException | IllegalStateException e) {
                    waInAppBrowsingActivity.A2f(e.getMessage(), false);
                    return true;
                }
            } else {
                waInAppBrowsingActivity.A03.Ab9(webView.getContext(), parse);
            }
        }
        return true;
    }
}
