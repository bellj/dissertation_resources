package X;

import java.util.Map;

/* renamed from: X.0tN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18980tN {
    public final Map A00;

    public C18980tN(Map map) {
        this.A00 = map;
    }

    public AnonymousClass110 A00(Class cls) {
        AnonymousClass01H r0 = (AnonymousClass01H) this.A00.get(cls);
        if (r0 != null) {
            return (AnonymousClass110) r0.get();
        }
        StringBuilder sb = new StringBuilder("No bridge with type ");
        sb.append(cls);
        sb.append(" was registered.");
        throw new IllegalStateException(sb.toString());
    }
}
