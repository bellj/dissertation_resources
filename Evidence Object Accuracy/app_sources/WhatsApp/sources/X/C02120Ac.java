package X;

import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.style.MetricAffectingSpan;

/* renamed from: X.0Ac  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02120Ac extends MetricAffectingSpan {
    public final Typeface A00;

    public C02120Ac(Typeface typeface) {
        this.A00 = typeface;
    }

    @Override // android.text.style.CharacterStyle
    public void updateDrawState(TextPaint textPaint) {
        textPaint.setTypeface(this.A00);
    }

    @Override // android.text.style.MetricAffectingSpan
    public void updateMeasureState(TextPaint textPaint) {
        textPaint.setTypeface(this.A00);
    }
}
