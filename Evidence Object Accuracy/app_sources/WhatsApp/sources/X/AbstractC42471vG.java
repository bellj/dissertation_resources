package X;

import android.content.Context;

/* renamed from: X.1vG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC42471vG extends AnonymousClass1OY {
    public boolean A00;

    public AbstractC42471vG(Context context, AbstractC13890kV r2, AbstractC15340mz r3) {
        super(context, r2, r3);
        A0Z();
    }

    @Override // X.AnonymousClass1OZ, X.AbstractC28561Ob
    public void A0Z() {
        if (!this.A00) {
            this.A00 = true;
            C42461vF r1 = (C42461vF) this;
            AnonymousClass2P6 r3 = (AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent());
            AnonymousClass01J r2 = r3.A06;
            ((AbstractC28551Oa) r1).A0L = (C14850m9) r2.A04.get();
            ((AbstractC28551Oa) r1).A0P = (AnonymousClass1CY) r2.ABO.get();
            ((AbstractC28551Oa) r1).A0F = (AbstractC15710nm) r2.A4o.get();
            ((AbstractC28551Oa) r1).A0N = (C244415n) r2.AAg.get();
            ((AbstractC28551Oa) r1).A0J = (AnonymousClass01d) r2.ALI.get();
            ((AbstractC28551Oa) r1).A0K = (AnonymousClass018) r2.ANb.get();
            ((AbstractC28551Oa) r1).A0M = (C22050yP) r2.A7v.get();
            ((AbstractC28551Oa) r1).A0G = (AnonymousClass19I) r2.A4a.get();
            r1.A0k = (C14830m7) r2.ALb.get();
            ((AnonymousClass1OY) r1).A0J = (C14900mE) r2.A8X.get();
            r1.A13 = (AnonymousClass13H) r2.ABY.get();
            r1.A1P = (AbstractC14440lR) r2.ANe.get();
            ((AnonymousClass1OY) r1).A0L = (C15570nT) r2.AAr.get();
            r1.A0h = (AnonymousClass19P) r2.ACQ.get();
            ((AnonymousClass1OY) r1).A0M = (C239613r) r2.AI9.get();
            ((AnonymousClass1OY) r1).A0O = (C18790t3) r2.AJw.get();
            r1.A0n = (C19990v2) r2.A3M.get();
            r1.A10 = (AnonymousClass19M) r2.A6R.get();
            ((AnonymousClass1OY) r1).A0N = (C15450nH) r2.AII.get();
            r1.A0v = (C21250x7) r2.AJh.get();
            r1.A0w = (C18470sV) r2.AK8.get();
            ((AnonymousClass1OY) r1).A0R = (C16170oZ) r2.AM4.get();
            r1.A1Q = (AnonymousClass19Z) r2.A2o.get();
            ((AnonymousClass1OY) r1).A0K = (AnonymousClass18U) r2.AAU.get();
            r1.A12 = (C14410lO) r2.AB3.get();
            ((AnonymousClass1OY) r1).A0I = (AnonymousClass12P) r2.A0H.get();
            ((AnonymousClass1OY) r1).A0a = (C21270x9) r2.A4A.get();
            r1.A0s = (C20040v7) r2.AAK.get();
            r1.A15 = (C17220qS) r2.ABt.get();
            ((AnonymousClass1OY) r1).A0X = (C15550nR) r2.A45.get();
            ((AnonymousClass1OY) r1).A0U = (C253619c) r2.AId.get();
            ((AnonymousClass1OY) r1).A0Z = (C15610nY) r2.AMe.get();
            r1.A1M = (C252018m) r2.A7g.get();
            r1.A1A = (C17070qD) r2.AFC.get();
            r1.A0t = (AnonymousClass1BK) r2.AFZ.get();
            ((AnonymousClass1OY) r1).A0b = (C253318z) r2.A4B.get();
            r1.A0p = (C15650ng) r2.A4m.get();
            ((AnonymousClass1OY) r1).A0V = (C238013b) r2.A1Z.get();
            r1.A11 = (C20710wC) r2.A8m.get();
            r1.A14 = (C22910zq) r2.A9O.get();
            r1.A1J = (AnonymousClass12F) r2.AJM.get();
            r1.A1F = r2.A41();
            r1.A1E = (AnonymousClass12V) r2.A0p.get();
            r1.A1I = (C240514a) r2.AJL.get();
            r1.A1O = (AnonymousClass19O) r2.ACO.get();
            r1.A17 = (C26151Cf) r2.ADi.get();
            r1.A1H = (C26701Em) r2.ABc.get();
            r1.A0x = (AnonymousClass132) r2.ALx.get();
            ((AnonymousClass1OY) r1).A0S = (C19850um) r2.A2v.get();
            r1.A0y = (C21400xM) r2.ABd.get();
            r1.A0z = (C15670ni) r2.AIb.get();
            r1.A1N = (C23000zz) r2.ALg.get();
            ((AnonymousClass1OY) r1).A0Y = (C22700zV) r2.AMN.get();
            r1.A0m = (C14820m6) r2.AN3.get();
            ((AnonymousClass1OY) r1).A0W = (C22640zP) r2.A3Z.get();
            r1.A19 = (C22710zW) r2.AF7.get();
            ((AnonymousClass1OY) r1).A0T = (AnonymousClass19Q) r2.A2u.get();
            r1.A1K = (AnonymousClass1AB) r2.AKI.get();
            r1.A18 = (AnonymousClass18T) r2.AE9.get();
            r1.A0r = (C15600nX) r2.A8x.get();
            r1.A0u = (C22440z5) r2.AG6.get();
            r1.A1D = (C16630pM) r2.AIc.get();
            r1.A0j = (C18640sm) r2.A3u.get();
            r1.A1L = (C26671Ej) r2.AKR.get();
            r1.A1G = r2.A42();
            r1.A0o = (C20830wO) r2.A4W.get();
            r1.A0q = (C242814x) r2.A71.get();
            r1.A0d = (AnonymousClass19K) r2.AFh.get();
            r1.A16 = (AnonymousClass19J) r2.ACA.get();
            r1.A1R = (C237512w) r2.AAD.get();
            r1.A0c = (AnonymousClass1AO) r2.AFg.get();
            r1.A0l = (C17170qN) r2.AMt.get();
            r1.A0i = (C17000q6) r2.ACv.get();
            r1.A1B = (C21190x1) r2.A3G.get();
            r1.A0f = r3.A02();
            r1.A00 = (C21740xu) r2.AM1.get();
            r1.A09 = (AnonymousClass14X) r2.AFM.get();
            r1.A04 = (C21860y6) r2.AE6.get();
            r1.A01 = (C15890o4) r2.AN1.get();
            r1.A03 = (C22370yy) r2.AB0.get();
            r1.A06 = (C17900ra) r2.AF5.get();
            r1.A02 = (AnonymousClass109) r2.AI8.get();
            r1.A05 = (AnonymousClass18P) r2.AEn.get();
            r1.A07 = (C22460z7) r2.AEF.get();
            r1.A08 = (AnonymousClass1A8) r2.AER.get();
        }
    }
}
