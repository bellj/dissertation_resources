package X;

/* renamed from: X.617  reason: invalid class name */
/* loaded from: classes4.dex */
public final class AnonymousClass617 {
    public final int A00;
    public final Object A01;
    public final Throwable A02;

    public AnonymousClass617(Object obj, Throwable th, int i) {
        this.A00 = i;
        this.A01 = obj;
        this.A02 = th;
    }

    public static AnonymousClass617 A00(Object obj) {
        return new AnonymousClass617(obj, null, 2);
    }

    public static AnonymousClass617 A01(Object obj) {
        return new AnonymousClass617(obj, null, 0);
    }

    public static AnonymousClass617 A02(Object obj, Throwable th) {
        return new AnonymousClass617(obj, th, 1);
    }
}
