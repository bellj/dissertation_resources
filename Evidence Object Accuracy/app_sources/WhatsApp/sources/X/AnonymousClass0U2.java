package X;

import android.graphics.PointF;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import java.lang.ref.WeakReference;

/* renamed from: X.0U2  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0U2 {
    public static C013506i A00;
    public static C05850Rf A01 = C05850Rf.A00("x", "y");
    public static C05850Rf A02 = C05850Rf.A00("t", "s", "e", "o", "i", "h", "to", "ti");
    public static final Interpolator A03 = new LinearInterpolator();

    public static Interpolator A00(PointF pointF, PointF pointF2) {
        int i;
        WeakReference weakReference;
        pointF.x = Math.max(-1.0f, Math.min(1.0f, pointF.x));
        pointF.y = Math.max(-100.0f, Math.min(100.0f, pointF.y));
        float max = Math.max(-1.0f, Math.min(1.0f, pointF2.x));
        pointF2.x = max;
        float max2 = Math.max(-100.0f, Math.min(100.0f, pointF2.y));
        pointF2.y = max2;
        float f = pointF.x;
        float f2 = pointF.y;
        if (f != 0.0f) {
            i = (int) (((float) 527) * f);
        } else {
            i = 17;
        }
        if (f2 != 0.0f) {
            i = (int) (((float) (i * 31)) * f2);
        }
        if (max != 0.0f) {
            i = (int) (((float) (i * 31)) * max);
        }
        if (max2 != 0.0f) {
            i = (int) (((float) (i * 31)) * max2);
        }
        synchronized (AnonymousClass0U2.class) {
            C013506i r0 = A00;
            if (r0 == null) {
                r0 = new C013506i();
                A00 = r0;
            }
            weakReference = (WeakReference) r0.A01(i);
        }
        Interpolator interpolator = weakReference != null ? (Interpolator) weakReference.get() : null;
        if (weakReference == null || interpolator == null) {
            try {
                interpolator = AnonymousClass0L1.A00(pointF.x, pointF.y, pointF2.x, pointF2.y);
            } catch (IllegalArgumentException e) {
                if ("The Path cannot loop back on itself.".equals(e.getMessage())) {
                    interpolator = AnonymousClass0L1.A00(Math.min(pointF.x, 1.0f), pointF.y, Math.max(pointF2.x, 0.0f), pointF2.y);
                } else {
                    interpolator = new LinearInterpolator();
                }
            }
            try {
                WeakReference weakReference2 = new WeakReference(interpolator);
                synchronized (AnonymousClass0U2.class) {
                    A00.A03(i, weakReference2);
                }
                return interpolator;
            } catch (ArrayIndexOutOfBoundsException unused) {
            }
        }
        return interpolator;
    }

    public static AnonymousClass0U8 A01(C05540Py r17, AbstractC12050hI r18, AbstractC08850bx r19, float f, boolean z, boolean z2) {
        Interpolator interpolator;
        Interpolator interpolator2;
        AnonymousClass0U8 r1;
        if (!z) {
            return new AnonymousClass0U8(r18.AYt(r19, f));
        }
        if (z2) {
            r19.A0A();
            PointF pointF = null;
            boolean z3 = false;
            PointF pointF2 = null;
            PointF pointF3 = null;
            PointF pointF4 = null;
            Object obj = null;
            PointF pointF5 = null;
            PointF pointF6 = null;
            PointF pointF7 = null;
            float f2 = 0.0f;
            PointF pointF8 = null;
            Object obj2 = null;
            while (r19.A0H()) {
                switch (r19.A04(A02)) {
                    case 0:
                        f2 = (float) r19.A02();
                        continue;
                    case 1:
                        obj = r18.AYt(r19, f);
                        continue;
                    case 2:
                        obj2 = r18.AYt(r19, f);
                        continue;
                    case 3:
                        if (r19.A05() == EnumC03770Jb.BEGIN_OBJECT) {
                            r19.A0A();
                            float f3 = 0.0f;
                            float f4 = 0.0f;
                            float f5 = 0.0f;
                            float f6 = 0.0f;
                            while (r19.A0H()) {
                                int A04 = r19.A04(A01);
                                if (A04 == 0) {
                                    EnumC03770Jb A05 = r19.A05();
                                    EnumC03770Jb r6 = EnumC03770Jb.NUMBER;
                                    if (A05 == r6) {
                                        f5 = (float) r19.A02();
                                        f3 = f5;
                                    } else {
                                        r19.A09();
                                        f3 = (float) r19.A02();
                                        if (r19.A05() == r6) {
                                            f5 = (float) r19.A02();
                                        } else {
                                            f5 = f3;
                                        }
                                        r19.A0B();
                                    }
                                } else if (A04 != 1) {
                                    r19.A0E();
                                } else {
                                    EnumC03770Jb A052 = r19.A05();
                                    EnumC03770Jb r5 = EnumC03770Jb.NUMBER;
                                    if (A052 == r5) {
                                        f6 = (float) r19.A02();
                                        f4 = f6;
                                    } else {
                                        r19.A09();
                                        f4 = (float) r19.A02();
                                        f6 = r19.A05() == r5 ? (float) r19.A02() : f4;
                                        r19.A0B();
                                    }
                                }
                            }
                            pointF4 = new PointF(f3, f4);
                            pointF5 = new PointF(f5, f6);
                            break;
                        } else {
                            pointF2 = AnonymousClass0UD.A02(r19, f);
                            continue;
                        }
                    case 4:
                        if (r19.A05() == EnumC03770Jb.BEGIN_OBJECT) {
                            r19.A0A();
                            float f7 = 0.0f;
                            float f8 = 0.0f;
                            float f9 = 0.0f;
                            float f10 = 0.0f;
                            while (r19.A0H()) {
                                int A042 = r19.A04(A01);
                                if (A042 == 0) {
                                    EnumC03770Jb A053 = r19.A05();
                                    EnumC03770Jb r4 = EnumC03770Jb.NUMBER;
                                    if (A053 == r4) {
                                        f9 = (float) r19.A02();
                                        f7 = f9;
                                    } else {
                                        r19.A09();
                                        f7 = (float) r19.A02();
                                        if (r19.A05() == r4) {
                                            f9 = (float) r19.A02();
                                        } else {
                                            f9 = f7;
                                        }
                                        r19.A0B();
                                    }
                                } else if (A042 != 1) {
                                    r19.A0E();
                                } else {
                                    EnumC03770Jb A054 = r19.A05();
                                    EnumC03770Jb r42 = EnumC03770Jb.NUMBER;
                                    if (A054 == r42) {
                                        f10 = (float) r19.A02();
                                        f8 = f10;
                                    } else {
                                        r19.A09();
                                        f8 = (float) r19.A02();
                                        f10 = r19.A05() == r42 ? (float) r19.A02() : f8;
                                        r19.A0B();
                                    }
                                }
                            }
                            pointF6 = new PointF(f7, f8);
                            pointF7 = new PointF(f9, f10);
                            break;
                        } else {
                            pointF3 = AnonymousClass0UD.A02(r19, f);
                            continue;
                        }
                    case 5:
                        z3 = false;
                        if (r19.A03() == 1) {
                            z3 = true;
                        } else {
                            continue;
                        }
                    case 6:
                        pointF8 = AnonymousClass0UD.A02(r19, f);
                        continue;
                    case 7:
                        pointF = AnonymousClass0UD.A02(r19, f);
                        continue;
                    default:
                        r19.A0E();
                        continue;
                }
                r19.A0C();
            }
            r19.A0C();
            if (z3) {
                interpolator2 = A03;
                obj2 = obj;
            } else if (pointF2 != null && pointF3 != null) {
                interpolator2 = A00(pointF2, pointF3);
            } else if (pointF4 == null || pointF5 == null || pointF6 == null || pointF7 == null) {
                interpolator2 = A03;
            } else {
                r1 = new AnonymousClass0U8(A00(pointF4, pointF6), A00(pointF5, pointF7), r17, obj, obj2, f2);
                r1.A06 = pointF8;
                r1.A07 = pointF;
                return r1;
            }
            r1 = new AnonymousClass0U8(interpolator2, r17, (Float) null, obj, obj2, f2);
            r1.A06 = pointF8;
            r1.A07 = pointF;
            return r1;
        }
        r19.A0A();
        PointF pointF9 = null;
        PointF pointF10 = null;
        Object obj3 = null;
        Object obj4 = null;
        PointF pointF11 = null;
        PointF pointF12 = null;
        boolean z4 = false;
        float f11 = 0.0f;
        while (r19.A0H()) {
            switch (r19.A04(A02)) {
                case 0:
                    f11 = (float) r19.A02();
                    break;
                case 1:
                    obj4 = r18.AYt(r19, f);
                    break;
                case 2:
                    obj3 = r18.AYt(r19, f);
                    break;
                case 3:
                    pointF9 = AnonymousClass0UD.A02(r19, 1.0f);
                    break;
                case 4:
                    pointF10 = AnonymousClass0UD.A02(r19, 1.0f);
                    break;
                case 5:
                    z4 = false;
                    if (r19.A03() != 1) {
                        break;
                    } else {
                        z4 = true;
                        break;
                    }
                case 6:
                    pointF11 = AnonymousClass0UD.A02(r19, f);
                    break;
                case 7:
                    pointF12 = AnonymousClass0UD.A02(r19, f);
                    break;
                default:
                    r19.A0E();
                    break;
            }
        }
        r19.A0C();
        if (z4) {
            interpolator = A03;
            obj3 = obj4;
        } else if (pointF9 == null || pointF10 == null) {
            interpolator = A03;
        } else {
            interpolator = A00(pointF9, pointF10);
        }
        AnonymousClass0U8 r12 = new AnonymousClass0U8(interpolator, r17, (Float) null, obj4, obj3, f11);
        r12.A06 = pointF11;
        r12.A07 = pointF12;
        return r12;
    }
}
