package X;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.payments.ui.AddPaymentMethodBottomSheet;
import com.whatsapp.payments.ui.BrazilDyiReportActivity;
import com.whatsapp.payments.ui.BrazilFbPayHubActivity;
import com.whatsapp.payments.ui.BrazilOrderDetailsActivity;
import com.whatsapp.payments.ui.BrazilPayBloksActivity;
import com.whatsapp.payments.ui.BrazilPaymentActivity;
import com.whatsapp.payments.ui.BrazilPaymentCardDetailsActivity;
import com.whatsapp.payments.ui.BrazilPaymentContactOmbudsmanActivity;
import com.whatsapp.payments.ui.BrazilPaymentContactSupportActivity;
import com.whatsapp.payments.ui.BrazilPaymentContactSupportP2pActivity;
import com.whatsapp.payments.ui.BrazilPaymentReportPaymentActivity;
import com.whatsapp.payments.ui.BrazilPaymentSettingsActivity;
import com.whatsapp.payments.ui.BrazilPaymentTransactionDetailActivity;
import com.whatsapp.payments.ui.BrazilViralityLinkVerifierActivity;
import com.whatsapp.payments.ui.IncentiveValuePropsActivity;
import com.whatsapp.payments.ui.IndiaPaymentTransactionHistoryActivity;
import com.whatsapp.payments.ui.IndiaUpiBankAccountDetailsActivity;
import com.whatsapp.payments.ui.IndiaUpiCheckBalanceActivity;
import com.whatsapp.payments.ui.IndiaUpiCheckOrderDetailsActivity;
import com.whatsapp.payments.ui.IndiaUpiIncentivesValuePropsActivity;
import com.whatsapp.payments.ui.IndiaUpiPaymentSettingsActivity;
import com.whatsapp.payments.ui.IndiaUpiPaymentTransactionDetailsActivity;
import com.whatsapp.payments.ui.IndiaUpiPaymentsAccountSetupActivity;
import com.whatsapp.payments.ui.IndiaUpiPinPrimerFullSheetActivity;
import com.whatsapp.payments.ui.IndiaUpiProfileDetailsActivity;
import com.whatsapp.payments.ui.IndiaUpiQuickBuyActivity;
import com.whatsapp.payments.ui.IndiaUpiSendPaymentActivity;
import com.whatsapp.payments.ui.IndiaUpiVpaContactInfoActivity;
import com.whatsapp.payments.ui.NoviPaymentTransactionDetailsActivity;
import com.whatsapp.payments.ui.NoviPaymentTransactionHistoryActivity;
import com.whatsapp.payments.ui.NoviSharedPaymentActivity;
import com.whatsapp.payments.ui.NoviSharedPaymentSettingsActivity;
import com.whatsapp.payments.ui.PaymentTransactionHistoryActivity;
import com.whatsapp.util.Log;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;
import org.json.JSONObject;

/* renamed from: X.6BG  reason: invalid class name */
/* loaded from: classes4.dex */
public abstract class AnonymousClass6BG implements AbstractC16830pp {
    public C14830m7 A00;
    public final C15550nR A01;
    public final C15610nY A02;
    public final C16590pI A03;
    public final C17070qD A04;
    public final AnonymousClass14X A05;
    public final String A06;

    @Override // X.AbstractC16830pp
    public boolean A71() {
        return true;
    }

    @Override // X.AbstractC16830pp
    public int AC0(String str) {
        return 1000;
    }

    @Override // X.AbstractC16830pp
    public String AF9() {
        return null;
    }

    @Override // X.AbstractC16830pp
    public boolean AJH() {
        return true;
    }

    public AnonymousClass6BG(C15550nR r1, C15610nY r2, C16590pI r3, C17070qD r4, AnonymousClass14X r5, String str) {
        this.A06 = str;
        this.A03 = r3;
        this.A05 = r5;
        this.A02 = r2;
        this.A01 = r1;
        this.A04 = r4;
    }

    @Override // X.AbstractC16830pp
    public boolean A70() {
        return this instanceof C121105hG;
    }

    @Override // X.AbstractC16830pp
    public void A9W(AnonymousClass1IR r3, AnonymousClass1IR r4) {
        AnonymousClass60R r0;
        String str;
        if ((this instanceof C121105hG) && r4 != null) {
            AbstractC30891Zf r02 = r3.A0A;
            AnonymousClass009.A05(r02);
            AnonymousClass60R r1 = ((C119835fB) r02).A0B;
            AbstractC30891Zf r03 = r4.A0A;
            AnonymousClass009.A05(r03);
            C119835fB r04 = (C119835fB) r03;
            if (r1 != null && (r0 = r04.A0B) != null && (str = r0.A0D) != null) {
                r1.A0H = str;
            }
        }
    }

    @Override // X.AbstractC16830pp
    public Class AAV() {
        if (this instanceof C121105hG) {
            return IndiaUpiBankAccountDetailsActivity.class;
        }
        if (!(this instanceof C121095hF)) {
            return null;
        }
        return BrazilPaymentCardDetailsActivity.class;
    }

    @Override // X.AbstractC16830pp
    public Class AAW() {
        if (this instanceof C121105hG) {
            return IndiaUpiPaymentsAccountSetupActivity.class;
        }
        if (!(this instanceof C121095hF)) {
            return null;
        }
        return BrazilPayBloksActivity.class;
    }

    @Override // X.AbstractC16830pp
    public Intent AAX(Context context) {
        if (!(this instanceof C121095hF)) {
            return null;
        }
        Intent A0D = C12990iw.A0D(context, BrazilPayBloksActivity.class);
        A0D.putExtra("screen_name", ((C121095hF) this).A0Q.A01());
        AbstractActivityC119645em.A0O(A0D, "referral_screen", "wa_payment_settings");
        return A0D;
    }

    @Override // X.AbstractC16830pp
    public Class ABI() {
        if (!(this instanceof C121105hG)) {
            return null;
        }
        return IndiaUpiCheckBalanceActivity.class;
    }

    @Override // X.AbstractC16830pp
    public C38161nc ABU() {
        boolean z = this instanceof C121105hG;
        C16590pI r3 = this.A03;
        C15610nY r2 = this.A02;
        C15550nR r1 = this.A01;
        if (!z) {
            return new C38161nc(r1, r2, r3);
        }
        return new C120055fY(r1, r2, r3);
    }

    @Override // X.AbstractC16830pp
    public Class ABa() {
        if (!(this instanceof C121095hF)) {
            return null;
        }
        return BrazilPaymentContactOmbudsmanActivity.class;
    }

    @Override // X.AbstractC16830pp
    public Class ABc() {
        if (!(this instanceof C121095hF)) {
            return null;
        }
        return BrazilPaymentContactSupportActivity.class;
    }

    @Override // X.AbstractC16830pp
    public Class ABd() {
        if (!(this instanceof C121095hF) || !((C121095hF) this).A0J.A03.A07(1615)) {
            return null;
        }
        return BrazilPaymentContactSupportP2pActivity.class;
    }

    @Override // X.AbstractC16830pp
    public AnonymousClass17Y ABn() {
        if (this instanceof C121085hE) {
            return ((C121085hE) this).A0C;
        }
        if (!(this instanceof C121105hG)) {
            return ((C121095hF) this).A0A;
        }
        return ((C121105hG) this).A0E;
    }

    @Override // X.AbstractC16830pp
    public AnonymousClass18N ABo() {
        if (!(this instanceof C121105hG)) {
            return null;
        }
        return ((C121105hG) this).A0C;
    }

    @Override // X.AbstractC16830pp
    public AnonymousClass18M ABq() {
        if (this instanceof C121105hG) {
            return ((C121105hG) this).A0S;
        }
        if (!(this instanceof C121095hF)) {
            return null;
        }
        C121095hF r0 = (C121095hF) this;
        C16590pI r1 = ((AnonymousClass6BG) r0).A03;
        C14850m9 r3 = r0.A09;
        AnonymousClass018 r2 = r0.A08;
        C22710zW r6 = r0.A0J;
        return new AnonymousClass69D(r1, r2, r3, r0.A0F, r0.A0I, r6);
    }

    @Override // X.AbstractC16840pq
    public AnonymousClass5W2 ABr() {
        if (this instanceof C121085hE) {
            C121085hE r0 = (C121085hE) this;
            return new AnonymousClass687(r0.A00, r0.A04);
        } else if (!(this instanceof C121105hG)) {
            C121095hF r02 = (C121095hF) this;
            C14830m7 r3 = r02.A06;
            C14900mE r1 = r02.A01;
            C18790t3 r2 = r02.A04;
            C17070qD r7 = ((AnonymousClass6BG) r02).A04;
            return new AnonymousClass689(r1, r2, r3, r02.A0D, r02.A0E, r02.A0F, r7, r02.A0O);
        } else {
            C121105hG r03 = (C121105hG) this;
            C16590pI r22 = ((AnonymousClass6BG) r03).A03;
            C18790t3 r12 = r03.A03;
            C17070qD r6 = ((AnonymousClass6BG) r03).A04;
            return new AnonymousClass688(r12, r22, r03.A0E, r03.A0G, r03.A0I, r6);
        }
    }

    @Override // X.AbstractC16830pp
    public AnonymousClass17U ABw() {
        if (this instanceof C121105hG) {
            return ((C121105hG) this).A0F;
        }
        if (!(this instanceof C121095hF)) {
            return null;
        }
        return ((C121095hF) this).A0C;
    }

    @Override // X.AbstractC16830pp
    public AbstractC38231nk ACH() {
        if (!(this instanceof C121105hG)) {
            return null;
        }
        C121105hG r0 = (C121105hG) this;
        C14830m7 r1 = r0.A06;
        C14900mE r12 = r0.A01;
        AbstractC14440lR r15 = r0.A0X;
        C16590pI r14 = ((AnonymousClass6BG) r0).A03;
        C15450nH r13 = r0.A02;
        AnonymousClass14X r122 = ((AnonymousClass6BG) r0).A05;
        AnonymousClass018 r11 = r0.A07;
        C18590sh r10 = r0.A0W;
        C17070qD r9 = ((AnonymousClass6BG) r0).A04;
        C1310060v r8 = r0.A0V;
        C21860y6 r7 = r0.A0G;
        C18610sj r6 = r0.A0N;
        AnonymousClass6BE r5 = r0.A0P;
        return new C120065fZ(r12, r13, r0.A05, r1, r14, r11, r0.A0A, r7, r0.A0H, r0.A0J, r0.A0M, r6, r9, r5, r8, r10, r122, r15);
    }

    @Override // X.AbstractC16830pp
    public /* synthetic */ String ACI() {
        if (!(this instanceof C121085hE)) {
            return null;
        }
        return C1310561a.A01(C12980iv.A0p(((C121085hE) this).A0B.A02(), "env_tier"));
    }

    @Override // X.AbstractC16830pp
    public Intent ACS(Context context, boolean z) {
        if (!(this instanceof C121105hG)) {
            return C12990iw.A0D(context, AFa());
        }
        StringBuilder A0k = C12960it.A0k("PAY: DeepLinkActivity handle DEEP_LINK_PAYMENT_SIGNUP ");
        A0k.append(IndiaUpiPaymentSettingsActivity.class);
        C12960it.A1F(A0k);
        Intent A0D = C12990iw.A0D(context, IndiaUpiPaymentSettingsActivity.class);
        A0D.putExtra("extra_is_invalid_deep_link_url", z);
        A0D.putExtra("referral_screen", "deeplink");
        return A0D;
    }

    @Override // X.AbstractC16830pp
    public Intent ACT(Context context, Uri uri) {
        int length;
        if (this instanceof C121105hG) {
            C121105hG r2 = (C121105hG) this;
            boolean A00 = C124955qR.A00(uri, r2.A0R);
            if (r2.A0G.A0A() || A00) {
                return r2.ACS(context, A00);
            }
            Log.i(C12960it.A0b("PAY: DeepLinkActivity handle DEEP_LINK_PAYMENT_SIGNUP for new user", ((AnonymousClass6BG) r2).A04.A02().AAW()));
            Intent A0D = C12990iw.A0D(context, IndiaUpiPaymentsAccountSetupActivity.class);
            A0D.putExtra("extra_skip_value_props_display", false);
            A0D.putExtra("extra_payments_entry_type", 9);
            C35741ib.A00(A0D, "deepLink");
            return A0D;
        } else if (!(this instanceof C121095hF)) {
            StringBuilder A0k = C12960it.A0k("PAY: DeepLinkActivity handle DEEP_LINK_PAYMENT_SIGNUP for new user");
            Class AAW = AAW();
            A0k.append(AAW);
            C12960it.A1F(A0k);
            Intent A0D2 = C12990iw.A0D(context, AAW);
            C35741ib.A00(A0D2, "deepLink");
            return A0D2;
        } else {
            C121095hF r22 = (C121095hF) this;
            if (C124955qR.A00(uri, r22.A0P)) {
                Intent A0D3 = C12990iw.A0D(context, BrazilPaymentSettingsActivity.class);
                A0D3.putExtra("referral_screen", "deeplink");
                return A0D3;
            }
            Intent AFe = r22.AFe(context, "deeplink", true);
            AFe.putExtra("extra_deep_link_url", uri);
            C130065yk r23 = r22.A0Q;
            String A01 = r23.A01();
            if ("brpay_p_pin_nux_create".equals(A01) || "brpay_p_compliance_kyc_next_screen_router".equals(A01)) {
                AbstractActivityC119645em.A0O(AFe, "deep_link_continue_setup", "1");
            }
            if (r23.A03.A0E("tos_no_wallet")) {
                return AFe;
            }
            String queryParameter = uri.getQueryParameter("c");
            if (queryParameter != null && (length = queryParameter.length()) >= 5 && !(!queryParameter.substring(length - 5, length).equals("9Y6XA"))) {
                return AFe;
            }
            AbstractActivityC119645em.A0O(AFe, "campaign_id", uri.getQueryParameter("c"));
            return AFe;
        }
    }

    @Override // X.AbstractC16830pp
    public int ACY() {
        if (!(this instanceof C121095hF)) {
            return 0;
        }
        return R.style.FbPayDialogTheme;
    }

    @Override // X.AbstractC16830pp
    public Intent ACd(Context context, String str, String str2) {
        if (!(this instanceof C121095hF)) {
            return null;
        }
        Intent A0D = C12990iw.A0D(context, BrazilDyiReportActivity.class);
        A0D.putExtra("extra_paymentProvider", str2);
        A0D.putExtra("extra_paymentAccountType", str);
        return A0D;
    }

    @Override // X.AbstractC16830pp
    public AbstractC16870pt ACx() {
        if (this instanceof C121105hG) {
            return ((C121105hG) this).A0P;
        }
        if (!(this instanceof C121095hF)) {
            return null;
        }
        return ((C121095hF) this).A0K;
    }

    @Override // X.AbstractC16830pp
    public Intent ADR(Context context) {
        Intent intent;
        if (this instanceof C121105hG) {
            intent = C12990iw.A0D(context, IndiaUpiIncentivesValuePropsActivity.class);
            intent.putExtra("extra_payments_entry_type", 1);
            intent.putExtra("extra_banner_type", 20);
        } else if (!(this instanceof C121095hF)) {
            return null;
        } else {
            intent = C12990iw.A0D(context, IncentiveValuePropsActivity.class);
        }
        intent.putExtra("referral_screen", "in_app_banner");
        return intent;
    }

    @Override // X.AbstractC16830pp
    public AnonymousClass17T AEE() {
        if (!(this instanceof C121095hF)) {
            return null;
        }
        return ((C121095hF) this).A0B;
    }

    @Override // X.AbstractC16830pp
    public AbstractC92724Xe AEF() {
        if (!(this instanceof C121095hF)) {
            return null;
        }
        C121095hF r0 = (C121095hF) this;
        C14830m7 r1 = r0.A06;
        C18600si r5 = r0.A0H;
        return new C120345g2(r1, r0.A07, r0.A0F, r0.A0B, r5, r0.A0K);
    }

    @Override // X.AbstractC16830pp
    public AnonymousClass1V8 AEY(AnonymousClass20C r5) {
        AnonymousClass1W9[] r3 = new AnonymousClass1W9[3];
        r3[0] = new AnonymousClass1W9("value", r5.A01());
        r3[1] = new AnonymousClass1W9("offset", r5.A00);
        C117295Zj.A1P("currency", ((AbstractC30781Yu) r5.A01).A04, r3);
        return new AnonymousClass1V8("money", r3);
    }

    @Override // X.AbstractC16830pp
    public Class AEb(Bundle bundle) {
        if (this instanceof C121085hE) {
            return ((C121085hE) this).A0D.A00(bundle);
        }
        if (!(this instanceof C121095hF)) {
            return null;
        }
        return C130425zO.A00(bundle);
    }

    @Override // X.AbstractC16830pp
    public AbstractC43531xB AEz() {
        if (this instanceof C121085hE) {
            C121085hE r0 = (C121085hE) this;
            AbstractC14440lR r7 = r0.A0I;
            C17070qD r2 = ((AnonymousClass6BG) r0).A04;
            C130155yt r3 = r0.A07;
            AnonymousClass61F r5 = r0.A0A;
            C22980zx r6 = r0.A0H;
            return new AnonymousClass69Z(r0.A02, r2, r3, r0.A09, r5, r6, r7);
        } else if (!(this instanceof C121105hG)) {
            return new AnonymousClass69Y();
        } else {
            return new C1329869a(((C121105hG) this).A0L);
        }
    }

    @Override // X.AbstractC16830pp
    public List AF1(AnonymousClass1IR r6, AnonymousClass1IS r7) {
        AnonymousClass20C r1;
        AbstractC30891Zf r12 = r6.A0A;
        if (r6.A0F() || r12 == null || (r1 = r12.A01) == null) {
            return null;
        }
        ArrayList A0l = C12960it.A0l();
        A0l.add(new AnonymousClass1V8(AEY(r1), "amount", new AnonymousClass1W9[0]));
        return A0l;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0132, code lost:
        if (r0 != null) goto L_0x005d;
     */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0203  */
    @Override // X.AbstractC16830pp
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List AF2(X.AnonymousClass1IR r10, X.AnonymousClass1IS r11) {
        /*
        // Method dump skipped, instructions count: 519
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass6BG.AF2(X.1IR, X.1IS):java.util.List");
    }

    @Override // X.AbstractC16830pp
    public AnonymousClass18O AF4() {
        if (!(this instanceof C121105hG)) {
            return null;
        }
        return ((C121105hG) this).A0T;
    }

    @Override // X.AbstractC16830pp
    public AbstractC116085Ub AF5() {
        if (!(this instanceof C121085hE)) {
            return new C1111158d();
        }
        return new AnonymousClass6CS(((C121085hE) this).A0G);
    }

    @Override // X.AbstractC16830pp
    public AnonymousClass5Wu AF6(AnonymousClass018 r3, C14850m9 r4, C22460z7 r5, AbstractC116085Ub r6) {
        if (!(this instanceof C121085hE)) {
            return new C69963aW(r3, r4, r5, r6);
        }
        return new C134056Dd(((C121085hE) this).A01, r3, r6);
    }

    @Override // X.AbstractC16830pp
    public Class AF7() {
        if (this instanceof C121105hG) {
            return IndiaUpiCheckOrderDetailsActivity.class;
        }
        if (!(this instanceof C121095hF)) {
            return null;
        }
        return BrazilOrderDetailsActivity.class;
    }

    @Override // X.AbstractC16830pp
    public AbstractC450620a AF8() {
        if (this instanceof C121105hG) {
            C121105hG r0 = (C121105hG) this;
            C14850m9 r5 = r0.A0A;
            C14900mE r1 = r0.A01;
            C16590pI r2 = ((AnonymousClass6BG) r0).A03;
            AbstractC14440lR r14 = r0.A0X;
            C17220qS r6 = r0.A0B;
            C18590sh r13 = r0.A0W;
            C17070qD r11 = ((AnonymousClass6BG) r0).A04;
            C1308460e r8 = r0.A0D;
            C18610sj r10 = r0.A0N;
            return new AnonymousClass697(r1, r2, r0.A08, r0.A09, r5, r6, r0.A0C, r8, r0.A0H, r10, r11, r0.A0U, r13, r14);
        } else if (!(this instanceof C121095hF)) {
            return null;
        } else {
            return new AnonymousClass696();
        }
    }

    @Override // X.AbstractC16830pp
    public AnonymousClass17W AFA() {
        if (this instanceof C121105hG) {
            return ((C121105hG) this).A0R;
        }
        if (!(this instanceof C121095hF)) {
            return null;
        }
        return ((C121095hF) this).A0P;
    }

    @Override // X.AbstractC16830pp
    public AnonymousClass5UU AFB(C16590pI r2, C18600si r3) {
        if (this instanceof C121105hG) {
            return new C121145hK(r2, r3);
        }
        if (!(this instanceof C121095hF)) {
            return new AnonymousClass69C(r2, r3);
        }
        return new C121135hJ(r2, r3);
    }

    @Override // X.AbstractC16830pp
    public int AFC() {
        if (this instanceof C121085hE) {
            return R.string.novi_title;
        }
        if (!(this instanceof C121105hG)) {
            return R.string.brazil_ecosystem_name;
        }
        return R.string.india_upi_short_name;
    }

    @Override // X.AbstractC16830pp
    public Class AFD() {
        if (!(this instanceof C121095hF)) {
            return null;
        }
        return BrazilFbPayHubActivity.class;
    }

    @Override // X.AbstractC16830pp
    public AnonymousClass5X2 AFE() {
        if (this instanceof C121105hG) {
            return new C121165hM();
        }
        if (!(this instanceof C121095hF)) {
            return null;
        }
        return new C121155hL();
    }

    @Override // X.AbstractC16830pp
    public Class AFF() {
        if (this instanceof C121085hE) {
            return NoviPaymentTransactionHistoryActivity.class;
        }
        if (!(this instanceof C121105hG)) {
            return PaymentTransactionHistoryActivity.class;
        }
        return IndiaPaymentTransactionHistoryActivity.class;
    }

    @Override // X.AbstractC16830pp
    public int AFH() {
        if (!(this instanceof C121105hG)) {
            return 0;
        }
        return R.string.india_upi_payment_id_name;
    }

    @Override // X.AbstractC16830pp
    public Pattern AFI() {
        if (!(this instanceof C121105hG)) {
            return null;
        }
        return C1309360o.A03;
    }

    @Override // X.AbstractC16830pp
    public AbstractC38191ng AFJ() {
        if (this instanceof C121105hG) {
            C121105hG r0 = (C121105hG) this;
            C14830m7 r5 = r0.A06;
            C14850m9 r7 = r0.A0A;
            C21740xu r2 = r0.A04;
            AnonymousClass14X r9 = ((AnonymousClass6BG) r0).A05;
            return new C120145fh(r0.A00, r2, ((AnonymousClass6BG) r0).A01, ((AnonymousClass6BG) r0).A02, r5, r0.A07, r7, r0.A0G, r9);
        } else if (!(this instanceof C121095hF)) {
            return null;
        } else {
            C121095hF r02 = (C121095hF) this;
            C14830m7 r52 = r02.A06;
            C14850m9 r72 = r02.A09;
            C21740xu r22 = r02.A05;
            AnonymousClass14X r92 = r02.A0R;
            return new C120135fg(r02.A00, r22, ((AnonymousClass6BG) r02).A01, ((AnonymousClass6BG) r02).A02, r52, r02.A08, r72, r02.A0Q, r92);
        }
    }

    @Override // X.AbstractC16830pp
    public AbstractC38141na AFL() {
        if (this instanceof C121085hE) {
            C121085hE r0 = (C121085hE) this;
            C14850m9 r3 = r0.A03;
            C16590pI r2 = ((AnonymousClass6BG) r0).A03;
            return new AnonymousClass69I(((AnonymousClass6BG) r0).A01, r2, r3, r0.A06, r0.A0A, r0.A0B);
        } else if (!(this instanceof C121105hG)) {
            return null;
        } else {
            C121105hG r02 = (C121105hG) this;
            return new AnonymousClass69H(r02.A06, ((AnonymousClass6BG) r02).A03, r02.A0A, r02.A0G);
        }
    }

    @Override // X.AbstractC16830pp
    public /* synthetic */ Pattern AFN() {
        if (!(this instanceof C121105hG)) {
            return null;
        }
        return C1309360o.A04;
    }

    @Override // X.AbstractC16830pp
    public String AFO(AnonymousClass18M r5, AbstractC15340mz r6) {
        if (!(this instanceof C121085hE)) {
            return this.A05.A0T(r5, r6);
        }
        C126105sL r0 = ((C121085hE) this).A0G;
        AnonymousClass1IR r2 = r6.A0L;
        if (r2 == null) {
            return null;
        }
        AbstractC130195yx A00 = r0.A00.A00(r2.A03);
        A00.A06(r2);
        if (!(A00 instanceof C123755no) || (!C31001Zq.A08(r6.A0L) && r6.A0L.A02 != 420)) {
            return A00.A07.A0T(r5, r6);
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public AnonymousClass24Y AFQ() {
        if (!(this instanceof C121095hF)) {
            return null;
        }
        C121095hF r1 = (C121095hF) this;
        return new C120295fw(((AnonymousClass6BG) r1).A03.A00, r1.A02, ((AnonymousClass6BG) r1).A04);
    }

    @Override // X.AbstractC16830pp
    public Class AFR() {
        if (!(this instanceof C121105hG)) {
            return null;
        }
        return IndiaUpiVpaContactInfoActivity.class;
    }

    @Override // X.AbstractC16830pp
    public int AFS() {
        if (!(this instanceof C121105hG)) {
            return 0;
        }
        return R.string.india_upi_payment_pin_name;
    }

    @Override // X.AbstractC16830pp
    public Class AFT() {
        if (!(this instanceof C121105hG)) {
            return null;
        }
        return IndiaUpiProfileDetailsActivity.class;
    }

    @Override // X.AbstractC16830pp
    public AbstractC51302Tt AFU() {
        if (!(this instanceof C121105hG)) {
            return null;
        }
        C121105hG r0 = (C121105hG) this;
        return new AnonymousClass69M(r0.A02, r0.A0E, r0.A0P);
    }

    @Override // X.AbstractC16830pp
    public Class AFV() {
        if (!(this instanceof C121105hG)) {
            return null;
        }
        return IndiaUpiQuickBuyActivity.class;
    }

    @Override // X.AbstractC16830pp
    public Class AFa() {
        if (this instanceof C121085hE) {
            return NoviSharedPaymentSettingsActivity.class;
        }
        if (!(this instanceof C121105hG)) {
            return BrazilPaymentSettingsActivity.class;
        }
        return IndiaUpiPaymentSettingsActivity.class;
    }

    @Override // X.AbstractC16830pp
    public AbstractC38181ne AFb() {
        if (!(this instanceof C121095hF)) {
            return null;
        }
        C121095hF r0 = (C121095hF) this;
        return new AnonymousClass69O(((AnonymousClass6BG) r0).A01, ((AnonymousClass6BG) r0).A02, r0.A06, r0.A0H, r0.A0R, r0.A0S);
    }

    @Override // X.AbstractC16830pp
    public Class AFc() {
        if (this instanceof C121085hE) {
            return NoviPaymentTransactionDetailsActivity.class;
        }
        if (!(this instanceof C121105hG)) {
            return BrazilPaymentTransactionDetailActivity.class;
        }
        return IndiaUpiPaymentTransactionDetailsActivity.class;
    }

    @Override // X.AbstractC16830pp
    public Class AFd() {
        if (!(this instanceof C121095hF)) {
            return null;
        }
        return BrazilViralityLinkVerifierActivity.class;
    }

    @Override // X.AbstractC16830pp
    public Intent AFe(Context context, String str, boolean z) {
        boolean A1Z;
        C14850m9 r1;
        int i;
        Intent A0D;
        if (this instanceof C121105hG) {
            Intent A0D2 = C12990iw.A0D(context, IndiaUpiPaymentsAccountSetupActivity.class);
            A0D2.putExtra("extra_payments_entry_type", 1);
            A0D2.putExtra("extra_skip_value_props_display", false);
            C35741ib.A00(A0D2, "inAppBanner");
            return A0D2;
        } else if (!(this instanceof C121095hF)) {
            return null;
        } else {
            C121095hF r2 = (C121095hF) this;
            if (str == "in_app_banner") {
                r1 = r2.A09;
                i = 567;
            } else if (str == "alt_virality") {
                r1 = r2.A09;
                i = 570;
            } else {
                A1Z = C12970iu.A1Z(str, "deeplink");
                C130065yk r3 = r2.A0Q;
                String A01 = r3.A01();
                if (A1Z || A01 == null) {
                    A0D = C12990iw.A0D(context, BrazilPaymentSettingsActivity.class);
                    A0D.putExtra("referral_screen", str);
                } else {
                    A0D = C12990iw.A0D(context, BrazilPayBloksActivity.class);
                    A0D.putExtra("screen_name", A01);
                    if (str != null) {
                        AbstractActivityC119645em.A0O(A0D, "referral_screen", str);
                    }
                }
                r3.A03(A0D, "generic_context");
                return A0D;
            }
            A1Z = r1.A07(i);
            C130065yk r3 = r2.A0Q;
            String A01 = r3.A01();
            if (A1Z) {
            }
            A0D = C12990iw.A0D(context, BrazilPaymentSettingsActivity.class);
            A0D.putExtra("referral_screen", str);
            r3.A03(A0D, "generic_context");
            return A0D;
        }
    }

    @Override // X.AbstractC16830pp
    public Class AFg() {
        if (!(this instanceof C121105hG)) {
            return null;
        }
        return IndiaUpiPinPrimerFullSheetActivity.class;
    }

    @Override // X.AbstractC16830pp
    public Class AG9() {
        if (!(this instanceof C121095hF)) {
            return null;
        }
        return BrazilPaymentReportPaymentActivity.class;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001d, code lost:
        if (r1 != 6) goto L_0x001f;
     */
    @Override // X.AbstractC16830pp
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String AGP(X.AnonymousClass1IR r3) {
        /*
            r2 = this;
            boolean r0 = r2 instanceof X.C121105hG
            if (r0 == 0) goto L_0x001f
            X.1Zf r0 = r3.A0A
            X.AnonymousClass009.A05(r0)
            X.5fB r0 = (X.C119835fB) r0
            X.60R r0 = r0.A0B
            if (r0 == 0) goto L_0x001f
            int r1 = r0.A00()
            r0 = 1
            if (r1 == r0) goto L_0x0033
            r0 = 2
            if (r1 == r0) goto L_0x002b
            r0 = 4
            if (r1 == r0) goto L_0x0033
            r0 = 6
            if (r1 == r0) goto L_0x002b
        L_0x001f:
            X.0pI r0 = r2.A03
            android.content.Context r1 = r0.A00
            r0 = 2131890573(0x7f12118d, float:1.9415842E38)
        L_0x0026:
            java.lang.String r0 = r1.getString(r0)
            return r0
        L_0x002b:
            X.0pI r0 = r2.A03
            android.content.Context r1 = r0.A00
            r0 = 2131890560(0x7f121180, float:1.9415815E38)
            goto L_0x0026
        L_0x0033:
            X.0pI r0 = r2.A03
            android.content.Context r1 = r0.A00
            r0 = 2131890685(0x7f1211fd, float:1.9416069E38)
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass6BG.AGP(X.1IR):java.lang.String");
    }

    @Override // X.AbstractC16830pp
    public Class AGb() {
        if (this instanceof C121085hE) {
            return NoviSharedPaymentActivity.class;
        }
        if (!(this instanceof C121105hG)) {
            return BrazilPaymentActivity.class;
        }
        return IndiaUpiSendPaymentActivity.class;
    }

    @Override // X.AbstractC16830pp
    public String AH4(String str) {
        if ((this instanceof C121085hE) && str.startsWith("https://novi.com/r/settings/security/tpp?access_code=")) {
            String queryParameter = Uri.parse(str).getQueryParameter("access_code");
            if (!TextUtils.isEmpty(queryParameter)) {
                return queryParameter;
            }
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public Intent AHG(Context context, String str) {
        if (!(this instanceof C121085hE)) {
            return null;
        }
        return ((C121085hE) this).A0E.A00(context, "xplatform_tpp_account_link", str);
    }

    @Override // X.AbstractC16830pp
    public int AHJ(AnonymousClass1IR r3) {
        if (!(this instanceof C121085hE)) {
            return AnonymousClass14X.A01(r3);
        }
        AbstractC130195yx A00 = ((C121085hE) this).A0G.A00.A00(r3.A03);
        A00.A06(r3);
        return A00.A01();
    }

    @Override // X.AbstractC16830pp
    public String AHL(AnonymousClass1IR r3) {
        AnonymousClass14X r0;
        if (!(this instanceof C121085hE)) {
            if (!(this instanceof C121105hG)) {
                r0 = ((C121095hF) this).A0R;
            } else {
                r0 = this.A05;
            }
            return r0.A0J(r3);
        }
        AbstractC130195yx A00 = ((C121085hE) this).A0G.A00.A00(r3.A03);
        A00.A06(r3);
        return A00.A04();
    }

    @Override // X.AbstractC16830pp
    public boolean AIG() {
        if (!(this instanceof C121095hF)) {
            return false;
        }
        return ((C121095hF) this).A0Q.A06.A03();
    }

    @Override // X.AbstractC16840pq
    public AbstractC30851Zb AIg() {
        if (this instanceof C121085hE) {
            return new C119735f1();
        }
        if (!(this instanceof C121105hG)) {
            return new C119745f2();
        }
        return new C119755f3();
    }

    @Override // X.AbstractC16840pq
    public AbstractC30871Zd AIh() {
        if (this instanceof C121085hE) {
            return new C119765f4();
        }
        if (!(this instanceof C121095hF)) {
            return null;
        }
        return new C119775f5();
    }

    @Override // X.AbstractC16840pq
    public AnonymousClass1ZO AIi() {
        if (this instanceof C121085hE) {
            return new AnonymousClass1ZO();
        }
        if (!(this instanceof C121105hG)) {
            return new C119695ex();
        }
        return new C119705ey();
    }

    @Override // X.AbstractC16840pq
    public AnonymousClass1ZX AIj() {
        if (!(this instanceof C121095hF)) {
            return null;
        }
        return new C119785f6();
    }

    @Override // X.AbstractC16840pq
    public AbstractC30891Zf AIk() {
        if (this instanceof C121085hE) {
            return new C119825fA();
        }
        if (!(this instanceof C121105hG)) {
            return new C119815f9();
        }
        return new C119835fB();
    }

    @Override // X.AbstractC16840pq
    public AnonymousClass1ZZ AIl() {
        if (!(this instanceof C121085hE)) {
            return null;
        }
        return new C119805f8();
    }

    @Override // X.AbstractC16830pp
    public boolean AJs(Uri uri) {
        if (this instanceof C121105hG) {
            return C124955qR.A00(uri, ((C121105hG) this).A0R);
        }
        if (!(this instanceof C121095hF)) {
            return false;
        }
        return C124955qR.A00(uri, ((C121095hF) this).A0P);
    }

    @Override // X.AbstractC16830pp
    public boolean AKH(C89674Kw r2) {
        if (!(this instanceof C121085hE)) {
            return true;
        }
        return r2.A00;
    }

    @Override // X.AbstractC16830pp
    public void AKc(Uri uri) {
        String str;
        String queryParameter;
        int length;
        if (this instanceof C121105hG) {
            AnonymousClass69A r5 = ((C121105hG) this).A0R;
            if (!uri.getQueryParameterNames().isEmpty()) {
                String queryParameter2 = uri.getQueryParameter("campaignID");
                String str2 = null;
                if (queryParameter2 == null) {
                    str = "Unknown signup url";
                } else {
                    str = C124955qR.A00(uri, r5) ? "Blocked signup url" : null;
                    try {
                        JSONObject A0a = C117295Zj.A0a();
                        A0a.put("campaign_id", queryParameter2);
                        str2 = A0a.toString();
                    } catch (Exception e) {
                        Log.e("IN PAY: error logging campaign id", e);
                    }
                }
                AnonymousClass2SP r1 = new AnonymousClass2SP();
                r1.A0Z = "deeplink";
                r1.A09 = C12980iv.A0i();
                r1.A0X = str2;
                r1.A0T = str;
                r5.A01.AKf(r1);
            }
        } else if (this instanceof C121095hF) {
            C121095hF r0 = (C121095hF) this;
            AnonymousClass69B r4 = r0.A0P;
            boolean A0E = r0.A0Q.A03.A0E("tos_no_wallet");
            String queryParameter3 = uri.getQueryParameter("c");
            if ("br".equals(r4.A00.A09(uri)) && queryParameter3 != null) {
                if (A0E || ((queryParameter = uri.getQueryParameter("c")) != null && (length = queryParameter.length()) >= 5 && !(!queryParameter.substring(length - 5, length).equals("9Y6XA")))) {
                    AnonymousClass3FW r52 = new AnonymousClass3FW(null, new AnonymousClass3FW[0]);
                    r52.A01("campaign_id", queryParameter3);
                    r4.A02.AKi(r52, 0, null, "deeplink", null);
                }
            }
        }
    }

    @Override // X.AbstractC16830pp
    public void ALp(Context context, AbstractC13860kS r8, AnonymousClass1IR r9) {
        if (!(this instanceof C121095hF)) {
            AnonymousClass009.A05(r9);
            Intent A0D = C12990iw.A0D(context, AAW());
            A0D.putExtra("extra_setup_mode", 2);
            A0D.putExtra("extra_receive_nux", true);
            if (r9.A0A != null && !TextUtils.isEmpty(null)) {
                A0D.putExtra("extra_onboarding_provider", (String) null);
            }
            C35741ib.A00(A0D, "acceptPayment");
            context.startActivity(A0D);
            return;
        }
        C121095hF r4 = (C121095hF) this;
        C130065yk r5 = r4.A0Q;
        String A01 = r5.A01();
        if (A01 != null) {
            Intent A0D2 = C12990iw.A0D(context, BrazilPayBloksActivity.class);
            A0D2.putExtra("screen_name", A01);
            A0D2.putExtra("hide_send_payment_cta", true);
            r5.A03(A0D2, "p2p_context");
            AbstractActivityC119645em.A0O(A0D2, "referral_screen", "get_started");
            C127715uw r2 = new C127715uw(A0D2, null, r4.A08.A09(R.string.accept_payment_add_debit_bottom_sheet_desc), null);
            AddPaymentMethodBottomSheet addPaymentMethodBottomSheet = new AddPaymentMethodBottomSheet();
            addPaymentMethodBottomSheet.A0U(C12970iu.A0D());
            addPaymentMethodBottomSheet.A04 = r2;
            addPaymentMethodBottomSheet.A05 = new Runnable() { // from class: X.6Fb
                @Override // java.lang.Runnable
                public final void run() {
                    AddPaymentMethodBottomSheet.this.A1B();
                }
            };
            r8.Adm(addPaymentMethodBottomSheet);
            return;
        }
        C117305Zk.A0C(((AnonymousClass6BG) r4).A04).A00(new AbstractC14590lg(r8, r4) { // from class: X.6EQ
            public final /* synthetic */ AbstractC13860kS A00;
            public final /* synthetic */ C121095hF A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC14590lg
            public final void accept(Object obj) {
                C121095hF r1 = this.A01;
                AbstractC13860kS r3 = this.A00;
                List list = (List) obj;
                if (list.size() == 0) {
                    Log.e("PAY: BrazilPaymentService/onAcceptPayment: Can't launch the 'ConfirmReceiveFragment'.");
                } else {
                    r1.A01.A0H(
                    /*  JADX ERROR: Method code generation error
                        jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0023: INVOKE  
                          (wrap: X.0mE : 0x001c: IGET  (r1v1 X.0mE A[REMOVE]) = (r1v0 'r1' X.5hF) X.5hF.A01 X.0mE)
                          (wrap: X.6I0 : 0x0020: CONSTRUCTOR  (r0v2 X.6I0 A[REMOVE]) = 
                          (r3v0 'r3' X.0kS)
                          (wrap: X.1Ze : 0x001a: CHECK_CAST (r2v1 X.1Ze A[REMOVE]) = (X.1Ze) (wrap: java.lang.Object : 0x0016: INVOKE  (r2v0 java.lang.Object A[REMOVE]) = 
                          (r5v1 'list' java.util.List)
                          (wrap: int : 0x0012: INVOKE  (r0v1 int A[REMOVE]) = (r5v1 'list' java.util.List) type: STATIC call: X.61i.A01(java.util.List):int)
                         type: INTERFACE call: java.util.List.get(int):java.lang.Object))
                         call: X.6I0.<init>(X.0kS, X.1Ze):void type: CONSTRUCTOR)
                         type: VIRTUAL call: X.0mE.A0H(java.lang.Runnable):void in method: X.6EQ.accept(java.lang.Object):void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                        	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                        	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:151)
                        	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                        	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                        	at java.util.ArrayList.forEach(ArrayList.java:1259)
                        	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                        	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                        Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0020: CONSTRUCTOR  (r0v2 X.6I0 A[REMOVE]) = 
                          (r3v0 'r3' X.0kS)
                          (wrap: X.1Ze : 0x001a: CHECK_CAST (r2v1 X.1Ze A[REMOVE]) = (X.1Ze) (wrap: java.lang.Object : 0x0016: INVOKE  (r2v0 java.lang.Object A[REMOVE]) = 
                          (r5v1 'list' java.util.List)
                          (wrap: int : 0x0012: INVOKE  (r0v1 int A[REMOVE]) = (r5v1 'list' java.util.List) type: STATIC call: X.61i.A01(java.util.List):int)
                         type: INTERFACE call: java.util.List.get(int):java.lang.Object))
                         call: X.6I0.<init>(X.0kS, X.1Ze):void type: CONSTRUCTOR in method: X.6EQ.accept(java.lang.Object):void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                        	... 21 more
                        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6I0, state: NOT_LOADED
                        	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                        	... 27 more
                        */
                    /*
                        this = this;
                        X.5hF r1 = r4.A01
                        X.0kS r3 = r4.A00
                        java.util.List r5 = (java.util.List) r5
                        int r0 = r5.size()
                        if (r0 != 0) goto L_0x0012
                        java.lang.String r0 = "PAY: BrazilPaymentService/onAcceptPayment: Can't launch the 'ConfirmReceiveFragment'."
                        com.whatsapp.util.Log.e(r0)
                        return
                    L_0x0012:
                        int r0 = X.C1311161i.A01(r5)
                        java.lang.Object r2 = r5.get(r0)
                        X.1Ze r2 = (X.C30881Ze) r2
                        X.0mE r1 = r1.A01
                        X.6I0 r0 = new X.6I0
                        r0.<init>(r3, r2)
                        r1.A0H(r0)
                        return
                    */
                    throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass6EQ.accept(java.lang.Object):void");
                }
            });
        }

        @Override // X.AbstractC16830pp
        public void AZN(C456522m r6, List list) {
            if (this instanceof C121105hG) {
                r6.A02 = 0;
                r6.A03 = 0;
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    AbstractC30891Zf r0 = C117315Zl.A09(it).A0A;
                    AnonymousClass009.A05(r0);
                    AnonymousClass60R r02 = ((C119835fB) r0).A0B;
                    if (r02 != null) {
                        if (C1310060v.A02(r02.A0E)) {
                            r6.A03++;
                        } else {
                            r6.A02++;
                        }
                    }
                }
            }
        }

        @Override // X.AbstractC16830pp
        public /* synthetic */ AnonymousClass1V8 AZQ(AnonymousClass1V8 r2) {
            if (!(this instanceof C121085hE)) {
                return r2;
            }
            try {
                return C1308960j.A00(((C121085hE) this).A09, r2);
            } catch (C124735q1 unused) {
                Log.e("PAY: NoviPaymentService/preProcessTransactionNotification - Failed to decrypt the transaction notification");
                return null;
            }
        }

        @Override // X.AbstractC16830pp
        public void AdF(C17900ra r5) {
            AbstractC30791Yv r3;
            C15450nH r1;
            C16140oW r0;
            if (this instanceof C121105hG) {
                C121105hG r2 = (C121105hG) this;
                C17930rd A01 = r5.A01();
                if (A01 == C17930rd.A0E) {
                    r3 = A01.A02;
                    r1 = r2.A02;
                    r0 = AbstractC15460nI.A20;
                } else {
                    return;
                }
            } else if (this instanceof C121095hF) {
                C121095hF r22 = (C121095hF) this;
                C17930rd A012 = r5.A01();
                if (A012 == C17930rd.A0D) {
                    r3 = A012.A02;
                    r1 = r22.A03;
                    r0 = AbstractC15460nI.A1w;
                } else {
                    return;
                }
            } else {
                return;
            }
            r3.AcK(C117295Zj.A0D(r3, new BigDecimal(r1.A02(r0))));
        }

        @Override // X.AbstractC16830pp
        public boolean AdP() {
            return (this instanceof C121085hE) || (this instanceof C121095hF);
        }
    }
