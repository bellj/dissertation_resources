package X;

/* renamed from: X.3lm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C76523lm extends AbstractC106444vi {
    public long A00;
    public final C76763mA A01 = new C76763mA(1);
    public final C95304dT A02 = new C95304dT();

    @Override // X.AbstractC117055Yb
    public boolean AJx() {
        return true;
    }

    @Override // X.AbstractC117055Yb, X.AnonymousClass5WX
    public String getName() {
        return "CameraMotionRenderer";
    }

    public C76523lm() {
        super(6);
    }

    @Override // X.AbstractC106444vi
    public void A08() {
    }

    @Override // X.AbstractC106444vi
    public void A09(long j, boolean z) {
        this.A00 = Long.MIN_VALUE;
    }

    @Override // X.AbstractC117055Yb
    public boolean AJN() {
        return AIJ();
    }

    @Override // X.AbstractC117055Yb
    public void AaS(long j, long j2) {
        while (!AIJ() && this.A00 < 100000 + j) {
            C76763mA r2 = this.A01;
            r2.clear();
            C89864Lr r1 = this.A0A;
            r1.A01 = null;
            r1.A00 = null;
            if (A00(r1, r2, false) == -4 && !AnonymousClass4YO.A00(r2)) {
                this.A00 = r2.A00;
            } else {
                return;
            }
        }
    }

    @Override // X.AnonymousClass5WX
    public int Aeb(C100614mC r3) {
        int i = 0;
        if ("application/x-camera-motion".equals(r3.A0T)) {
            i = 4;
        }
        return i | 0 | 0;
    }
}
