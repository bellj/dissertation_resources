package X;

/* renamed from: X.3T5  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3T5 implements AnonymousClass1UA {
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0042 A[Catch: Exception -> 0x00c5, TryCatch #2 {Exception -> 0x00c5, blocks: (B:10:0x003c, B:13:0x0042, B:14:0x0044, B:16:0x0048, B:18:0x004c, B:21:0x0055, B:23:0x005a, B:24:0x007c, B:25:0x0099, B:27:0x00a2, B:30:0x00af, B:31:0x00b5, B:26:0x009d, B:34:0x00bc, B:35:0x00bf), top: B:40:0x003c }] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0048 A[Catch: Exception -> 0x00c5, TryCatch #2 {Exception -> 0x00c5, blocks: (B:10:0x003c, B:13:0x0042, B:14:0x0044, B:16:0x0048, B:18:0x004c, B:21:0x0055, B:23:0x005a, B:24:0x007c, B:25:0x0099, B:27:0x00a2, B:30:0x00af, B:31:0x00b5, B:26:0x009d, B:34:0x00bc, B:35:0x00bf), top: B:40:0x003c }] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00ad  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00af A[Catch: Exception -> 0x00c5, TryCatch #2 {Exception -> 0x00c5, blocks: (B:10:0x003c, B:13:0x0042, B:14:0x0044, B:16:0x0048, B:18:0x004c, B:21:0x0055, B:23:0x005a, B:24:0x007c, B:25:0x0099, B:27:0x00a2, B:30:0x00af, B:31:0x00b5, B:26:0x009d, B:34:0x00bc, B:35:0x00bf), top: B:40:0x003c }] */
    @Override // X.AnonymousClass1UA
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.location.Location ADn(X.AnonymousClass1U8 r9) {
        /*
            r8 = this;
            r2 = 1
            boolean r1 = X.C12960it.A1W(r9)
            java.lang.String r0 = "GoogleApiClient parameter is required."
            X.C13020j0.A03(r0, r1)
            X.4DN r0 = X.AnonymousClass1U9.A01
            X.3eb r6 = r9.A04(r0)
            X.2ko r6 = (X.C56392ko) r6
            if (r6 != 0) goto L_0x0015
            r2 = 0
        L_0x0015:
            java.lang.String r0 = "GoogleApiClient is not configured to use the LocationServices.API Api. Pass thisinto GoogleApiClient.Builder#addApi() to use this feature."
            X.C13020j0.A04(r0, r2)
            android.content.Context r4 = r9.A02()
            int r1 = android.os.Build.VERSION.SDK_INT
            r7 = 0
            r0 = 30
            if (r1 < r0) goto L_0x003b
            if (r4 == 0) goto L_0x003b
            java.lang.Class<android.content.Context> r3 = android.content.Context.class
            java.lang.String r1 = "getAttributionTag"
            r2 = 0
            java.lang.Class[] r0 = new java.lang.Class[r2]     // Catch: NoSuchMethodException | IllegalAccessException | InvocationTargetException -> 0x003b
            java.lang.reflect.Method r1 = r3.getMethod(r1, r0)     // Catch: NoSuchMethodException | IllegalAccessException | InvocationTargetException -> 0x003b
            java.lang.Object[] r0 = new java.lang.Object[r2]     // Catch: NoSuchMethodException | IllegalAccessException | InvocationTargetException -> 0x003b
            java.lang.Object r5 = r1.invoke(r4, r0)     // Catch: NoSuchMethodException | IllegalAccessException | InvocationTargetException -> 0x003b
            java.lang.String r5 = (java.lang.String) r5     // Catch: NoSuchMethodException | IllegalAccessException | InvocationTargetException -> 0x003b
            goto L_0x003c
        L_0x003b:
            r5 = r7
        L_0x003c:
            X.2kw r0 = r6.A0Q     // Catch: Exception -> 0x00c5
            if (r0 != 0) goto L_0x0042
            r4 = 0
            goto L_0x0044
        L_0x0042:
            X.3pB[] r4 = r0.A03     // Catch: Exception -> 0x00c5
        L_0x0044:
            X.3pB r3 = X.C88794Hd.A02     // Catch: Exception -> 0x00c5
            if (r4 == 0) goto L_0x007c
            int r2 = r4.length     // Catch: Exception -> 0x00c5
            r1 = 0
        L_0x004a:
            if (r1 >= r2) goto L_0x007c
            r0 = r4[r1]     // Catch: Exception -> 0x00c5
            boolean r0 = X.C13300jT.A00(r0, r3)     // Catch: Exception -> 0x00c5
            if (r0 == 0) goto L_0x0055
            goto L_0x0058
        L_0x0055:
            int r1 = r1 + 1
            goto L_0x004a
        L_0x0058:
            if (r1 < 0) goto L_0x007c
            X.4ST r0 = r6.A00     // Catch: Exception -> 0x00c5
            X.5Qr r0 = r0.A01     // Catch: Exception -> 0x00c5
            X.50G r0 = (X.AnonymousClass50G) r0     // Catch: Exception -> 0x00c5
            X.2ko r0 = r0.A00     // Catch: Exception -> 0x00c5
            X.C56392ko.A00(r0)     // Catch: Exception -> 0x00c5
            android.os.IInterface r1 = r0.A03()     // Catch: Exception -> 0x00c5
            X.5YQ r1 = (X.AnonymousClass5YQ) r1     // Catch: Exception -> 0x00c5
            X.2lL r1 = (X.C56642lL) r1     // Catch: Exception -> 0x00c5
            android.os.Parcel r4 = android.os.Parcel.obtain()     // Catch: Exception -> 0x00c5
            java.lang.String r0 = r1.A01     // Catch: Exception -> 0x00c5
            r4.writeInterfaceToken(r0)     // Catch: Exception -> 0x00c5
            r4.writeString(r5)     // Catch: Exception -> 0x00c5
            r3 = 80
            goto L_0x0099
        L_0x007c:
            X.4ST r0 = r6.A00     // Catch: Exception -> 0x00c5
            X.5Qr r0 = r0.A01     // Catch: Exception -> 0x00c5
            X.50G r0 = (X.AnonymousClass50G) r0     // Catch: Exception -> 0x00c5
            X.2ko r0 = r0.A00     // Catch: Exception -> 0x00c5
            X.C56392ko.A00(r0)     // Catch: Exception -> 0x00c5
            android.os.IInterface r1 = r0.A03()     // Catch: Exception -> 0x00c5
            X.5YQ r1 = (X.AnonymousClass5YQ) r1     // Catch: Exception -> 0x00c5
            X.2lL r1 = (X.C56642lL) r1     // Catch: Exception -> 0x00c5
            android.os.Parcel r4 = android.os.Parcel.obtain()     // Catch: Exception -> 0x00c5
            java.lang.String r0 = r1.A01     // Catch: Exception -> 0x00c5
            r4.writeInterfaceToken(r0)     // Catch: Exception -> 0x00c5
            r3 = 7
        L_0x0099:
            android.os.Parcel r2 = android.os.Parcel.obtain()     // Catch: Exception -> 0x00c5
            android.os.IBinder r0 = r1.A00     // Catch: RuntimeException -> 0x00bb, all -> 0x00c0
            X.C12990iw.A18(r0, r4, r2, r3)     // Catch: RuntimeException -> 0x00bb, all -> 0x00c0
            r4.recycle()     // Catch: Exception -> 0x00c5
            android.os.Parcelable$Creator r1 = android.location.Location.CREATOR     // Catch: Exception -> 0x00c5
            int r0 = r2.readInt()     // Catch: Exception -> 0x00c5
            if (r0 != 0) goto L_0x00af
            r0 = 0
            goto L_0x00b5
        L_0x00af:
            java.lang.Object r0 = r1.createFromParcel(r2)     // Catch: Exception -> 0x00c5
            android.os.Parcelable r0 = (android.os.Parcelable) r0     // Catch: Exception -> 0x00c5
        L_0x00b5:
            android.location.Location r0 = (android.location.Location) r0     // Catch: Exception -> 0x00c5
            r2.recycle()     // Catch: Exception -> 0x00c5
            return r0
        L_0x00bb:
            r0 = move-exception
            r2.recycle()     // Catch: all -> 0x00c0
            throw r0     // Catch: all -> 0x00c0
        L_0x00c0:
            r0 = move-exception
            r4.recycle()     // Catch: Exception -> 0x00c5
            throw r0     // Catch: Exception -> 0x00c5
        L_0x00c5:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3T5.ADn(X.1U8):android.location.Location");
    }
}
