package X;

/* renamed from: X.43k  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C855943k extends AbstractC16110oT {
    public Integer A00;
    public Long A01;
    public Long A02;
    public Long A03;
    public Long A04;

    public C855943k() {
        super(2246, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(5, this.A01);
        r3.Abe(1, this.A00);
        r3.Abe(2, this.A02);
        r3.Abe(3, this.A03);
        r3.Abe(4, this.A04);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamExtDirMigration {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "attemptN", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "extDirMigrationResult", C12960it.A0Y(this.A00));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "extDirMigrationTotalT", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "moveT", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "rescanT", this.A04);
        return C12960it.A0d("}", A0k);
    }
}
