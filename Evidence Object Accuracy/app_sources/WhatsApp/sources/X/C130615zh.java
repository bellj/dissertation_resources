package X;

/* renamed from: X.5zh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130615zh {
    public final C126765tP A00;
    public final AnonymousClass6F2 A01;
    public final AnonymousClass6F2 A02;
    public final AbstractC121025h8 A03;

    public C130615zh(C126765tP r1, AnonymousClass6F2 r2, AnonymousClass6F2 r3, AbstractC121025h8 r4) {
        this.A00 = r1;
        this.A02 = r2;
        this.A01 = r3;
        this.A03 = r4;
    }

    public static C130615zh A00(AnonymousClass102 r7, AnonymousClass1V8 r8) {
        AnonymousClass6F2 r4;
        AnonymousClass6F2 r5;
        AbstractC121025h8 r3;
        AnonymousClass1V8 A0E = r8.A0E("balance");
        C126765tP r6 = null;
        if (A0E != null) {
            r5 = AnonymousClass6F2.A00(r7, A0E.A0F("primary"));
            r4 = AnonymousClass6F2.A00(r7, A0E.A0F("local"));
        } else {
            r4 = null;
            r5 = null;
        }
        AnonymousClass1V8 A0E2 = r8.A0E("transaction");
        if (A0E2 != null) {
            r3 = AbstractC121025h8.A00(r7, A0E2);
        } else {
            r3 = null;
        }
        AnonymousClass1V8 A0E3 = r8.A0E("cash_withdrawal_code");
        if (A0E3 != null) {
            r6 = new C126765tP(C1309260n.A01(A0E3.A0E("instructions")), A0E3.A0I("code", null));
        }
        return new C130615zh(r6, r4, r5, r3);
    }
}
