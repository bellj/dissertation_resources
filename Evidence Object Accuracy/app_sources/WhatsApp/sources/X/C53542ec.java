package X;

import android.view.View;
import com.whatsapp.R;

/* renamed from: X.2ec  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53542ec extends AnonymousClass04v {
    public final /* synthetic */ C36771kY A00;
    public final /* synthetic */ AnonymousClass5W1 A01;

    public C53542ec(C36771kY r1, AnonymousClass5W1 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r6) {
        super.A06(view, r6);
        int ADd = this.A01.ADd();
        ActivityC000900k A0B = this.A00.A01.A0B();
        int i = R.string.calls_row_action_click;
        if (ADd == 1) {
            i = R.string.contacts_row_action_click;
        }
        C12970iu.A1O(r6, A0B.getString(i));
    }
}
