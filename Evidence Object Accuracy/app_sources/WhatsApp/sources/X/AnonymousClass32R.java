package X;

import com.whatsapp.group.GroupChatInfo;
import java.util.List;

/* renamed from: X.32R  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass32R extends RunnableC32531cJ {
    public final /* synthetic */ GroupChatInfo A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass32R(C21320xE r10, GroupChatInfo groupChatInfo, C20710wC r12, C15580nU r13, C14860mA r14, List list) {
        super(r10, r12, r13, null, r14, null, list, 30);
        this.A00 = groupChatInfo;
    }
}
