package X;

import java.util.List;

/* renamed from: X.6Hh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class RunnableC135136Hh implements Runnable {
    public final /* synthetic */ C128965wx A00;
    public final /* synthetic */ List A01;

    public RunnableC135136Hh(C128965wx r1, List list) {
        this.A00 = r1;
        this.A01 = list;
    }

    @Override // java.lang.Runnable
    public void run() {
        List list = this.A01;
        int size = list.size();
        for (int i = 0; i < size; i++) {
            ((C128425w5) list.get(i)).A00();
        }
    }
}
