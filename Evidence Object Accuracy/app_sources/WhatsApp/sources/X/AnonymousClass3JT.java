package X;

import android.text.TextUtils;
import com.facebook.redex.IDxNFunctionShape17S0100000_2_I1;
import com.whatsapp.jid.Jid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.3JT  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3JT {
    public static AnonymousClass1V9 A00(String str, Object[] objArr) {
        return new AnonymousClass1V9(String.format(str, objArr));
    }

    public static Object A01(AbstractC15710nm r2, AnonymousClass1V8 r3, int i) {
        return A05(r3, new IDxNFunctionShape17S0100000_2_I1(r2, i), new String[0]);
    }

    public static Object A02(AbstractC15710nm r2, AnonymousClass1V8 r3, int i) {
        return A05(r3, new IDxNFunctionShape17S0100000_2_I1(r2, i), new String[0]);
    }

    public static Object A03(AbstractC15710nm r9, AnonymousClass1V8 r10, Class cls, Long l, Long l2, Object obj, String[] strArr, boolean z) {
        String A0I;
        String A0I2;
        int length = strArr.length;
        if (length != 0) {
            int i = length - 1;
            String str = strArr[i];
            boolean A1V = C12960it.A1V(str.charAt(0), 35);
            for (int i2 = 0; i2 < i; i2++) {
                r10 = r10.A0F(strArr[i2]);
            }
            Long l3 = null;
            if (cls == String.class) {
                if (A1V) {
                    A0I2 = r10.A0G();
                } else {
                    A0I2 = r10.A0I(str, null);
                }
                if (z) {
                    if (!C29941Vi.A00(A0I2, obj)) {
                        throw A00("Error while parsing attribute '%s' in tag <%s/>.", A0F(r10, str, 2));
                    }
                } else if (A0I2 != null) {
                    if (l != null && ((long) A0I2.length()) < l.longValue()) {
                        Object[] A0F = A0F(r10, str, 3);
                        A0F[2] = l;
                        throw A00("Length of attribute %s for tag %s is less than the specified lower bound value of %s.", A0F);
                    } else if (l2 != null && ((long) A0I2.length()) > l2.longValue()) {
                        Object[] A0F2 = A0F(r10, str, 3);
                        A0F2[2] = l2;
                        throw A00("Length of attribute %s for tag %s is more than the specified upper bound value of %s.", A0F2);
                    } else if (obj != null && !A0I2.equals(obj)) {
                        throw A00("Error while parsing attribute '%s' in tag <%s/>.", A0F(r10, str, 2));
                    }
                }
                return A0I2;
            } else if (cls == byte[].class) {
                if (A1V) {
                    byte[] bArr = r10.A01;
                    if (bArr != null) {
                        if (l != null && ((long) bArr.length) < l.longValue()) {
                            throw A00("Length of element value for tag %s is less than the specified lower bound value of %s", new Object[]{r10.A00, l});
                        } else if (l2 == null || ((long) bArr.length) <= l2.longValue()) {
                            return bArr;
                        } else {
                            throw A00("Length of element value for tag %s is more than the specified upper bound value of %s", new Object[]{r10.A00, l2});
                        }
                    }
                } else {
                    throw new AnonymousClass1V9("Cannot have binary not contained in an element value in tag %s", r10.A00);
                }
            } else if (cls == Long.class || cls == Long.TYPE) {
                if (A1V) {
                    A0I = r10.A0G();
                } else {
                    A0I = r10.A0I(str, null);
                }
                if (z) {
                    if (A0I != null) {
                        l3 = Long.valueOf(r10.A09(A0I, str));
                    }
                    if (!C29941Vi.A00(l3, obj)) {
                        throw A00("Error while parsing attribute '%s' in tag <%s/>.", A0F(r10, str, 2));
                    }
                } else if (A0I != null) {
                    Long valueOf = Long.valueOf(r10.A09(A0I, str));
                    if (l != null && valueOf.longValue() < l.longValue()) {
                        Object[] A0F3 = A0F(r10, str, 3);
                        A0F3[2] = l;
                        throw A00("Value of attribute '%s' for tag %s is less than the specified lower bound value of %s", A0F3);
                    } else if (l2 != null && valueOf.longValue() > l2.longValue()) {
                        Object[] A0F4 = A0F(r10, str, 3);
                        A0F4[2] = l2;
                        throw A00("Value of attribute '%s' for tag %s is more than the specified upper bound value of %s", A0F4);
                    } else if (obj == null || valueOf.equals(obj)) {
                        return valueOf;
                    } else {
                        throw A00("Error while parsing attribute '%s' in tag <%s/>.", A0F(r10, str, 2));
                    }
                }
            } else if (Jid.class.isAssignableFrom(cls)) {
                Jid A0A = r10.A0A(r9, cls, str);
                if (z) {
                    if (C29941Vi.A00(A0A, obj)) {
                        return A0A;
                    }
                    throw A00("Error while parsing attribute '%s' in tag <%s/>.", A0F(r10, str, 2));
                } else if (A0A != null) {
                    if (obj == null || A0A.equals(obj)) {
                        return A0A;
                    }
                    throw A00("Error while parsing attribute '%s' in tag <%s/>.", A0F(r10, str, 2));
                }
            } else {
                throw A00("Invalid type '%s' passed to function", new Object[]{cls.getName()});
            }
            return l3;
        }
        throw new AnonymousClass1V9("Empty path");
    }

    public static Object A04(AbstractC15710nm r5, AnonymousClass1V8 r6, Class cls, Long l, Long l2, Object obj, String[] strArr, boolean z) {
        Object A03 = A03(r5, r6, cls, l, l2, obj, strArr, z);
        if (A03 != null) {
            return A03;
        }
        int length = strArr.length - 1;
        String str = strArr[length];
        for (int i = 0; i < length; i++) {
            r6 = r6.A0F(strArr[i]);
        }
        Object[] A1a = C12980iv.A1a();
        A1a[0] = str;
        A1a[1] = r6.A00;
        throw A00("Required attribute '%s' missing for tag '%s'", A1a);
    }

    public static Object A05(AnonymousClass1V8 r2, AbstractC116095Uc r3, String[] strArr) {
        for (String str : strArr) {
            r2 = r2.A0F(str);
        }
        Object A63 = r3.A63(r2);
        if (A63 != null) {
            return A63;
        }
        throw new AnonymousClass1V9("Required mixin was null.");
    }

    public static Object A06(AnonymousClass1V8 r5, String str, List list, String[] strArr) {
        Object obj;
        Iterator it = list.iterator();
        while (it.hasNext()) {
            AbstractC116095Uc r3 = (AbstractC116095Uc) it.next();
            AnonymousClass1V8 r2 = r5;
            for (String str2 : strArr) {
                r2 = r2.A0F(str2);
            }
            try {
                obj = r3.A63(r2);
                continue;
            } catch (AnonymousClass1V9 unused) {
                obj = null;
                continue;
            }
            if (obj != null) {
                return obj;
            }
        }
        throw A00("Required mixin group '%s' was null.", new Object[]{str});
    }

    /* JADX DEBUG: Multi-variable search result rejected for r10v0, resolved type: java.lang.String[] */
    /* JADX WARN: Multi-variable type inference failed */
    public static String A07(AnonymousClass1V8 r8, Object obj, String[] strArr) {
        strArr[0] = obj;
        return (String) A04(null, r8, String.class, -9007199254740991L, 9007199254740991L, null, strArr, false);
    }

    public static String A08(AnonymousClass1V8 r8, List list, String[] strArr) {
        String str = (String) A03(null, r8, String.class, null, null, null, strArr, false);
        if (str == null) {
            return null;
        }
        if (list.contains(str)) {
            return str;
        }
        Object[] A1b = C12970iu.A1b();
        A1b[0] = TextUtils.join(", ", list);
        throw A00("String was expected to be one of '%s'.", A1b);
    }

    public static String A09(AnonymousClass1V8 r8, List list, String[] strArr) {
        String str = (String) A04(null, r8, String.class, null, null, null, strArr, false);
        if (list.contains(str)) {
            return str;
        }
        Object[] A1b = C12970iu.A1b();
        A1b[0] = TextUtils.join(", ", list);
        throw A00("String was expected to be one of '%s'.", A1b);
    }

    public static List A0A(AbstractC15710nm r6, AnonymousClass1V8 r7, String[] strArr, int i) {
        return A0B(r7, new IDxNFunctionShape17S0100000_2_I1(r6, i), strArr, 0, Long.MAX_VALUE);
    }

    public static List A0B(AnonymousClass1V8 r9, AbstractC116095Uc r10, String[] strArr, long j, long j2) {
        int length = strArr.length;
        if (length != 0) {
            int i = length - 1;
            for (int i2 = 0; i2 < i; i2++) {
                r9 = r9.A0F(strArr[i2]);
            }
            AnonymousClass1V8[] r3 = r9.A03;
            String str = strArr[i];
            if (r3 == null) {
                if (j != 0) {
                    throw A00("Null value received for non-optional children of type '%s'.", new Object[]{str});
                } else if (j == 0) {
                    return C12960it.A0l();
                }
            }
            List<AnonymousClass1V8> asList = Arrays.asList(r3);
            ArrayList A0l = C12960it.A0l();
            for (AnonymousClass1V8 r1 : asList) {
                if (str.equals(r1.A00)) {
                    A0l.add(r1);
                }
            }
            ArrayList A0l2 = C12960it.A0l();
            Iterator it = A0l.iterator();
            while (it.hasNext()) {
                A0l2.add(r10.A63((AnonymousClass1V8) it.next()));
            }
            if (((long) A0l2.size()) < j) {
                Object[] objArr = new Object[3];
                objArr[0] = str;
                C12960it.A1P(objArr, A0l2.size(), 1);
                C12980iv.A1U(objArr, 2, j);
                throw A00("Invalid number of children '%s'. Received %d children but the minimum value specified in the spec is %d.", objArr);
            } else if (((long) A0l2.size()) <= j2) {
                return A0l2;
            } else {
                Object[] objArr2 = new Object[3];
                objArr2[0] = str;
                C12960it.A1P(objArr2, A0l2.size(), 1);
                C12980iv.A1U(objArr2, 2, j2);
                throw A00("Invalid number of children '%s'. Received %d children but the maximum value specified in the spec is %d.", objArr2);
            }
        } else {
            throw new AnonymousClass1V9("Empty path");
        }
    }

    public static void A0C(byte[] bArr, long j, long j2) {
        Object[] objArr;
        String str;
        long length = (long) bArr.length;
        if (length < j) {
            objArr = new Object[1];
            C12980iv.A1U(objArr, 0, j);
            str = "Length of binary byte array is less than the specified lower bound value of %d";
        } else if (length > j2) {
            objArr = new Object[1];
            C12980iv.A1U(objArr, 0, j2);
            str = "Length of binary byte array is greater than the specified lower bound value of %d";
        } else {
            return;
        }
        AnonymousClass009.A07(String.format(str, objArr));
    }

    public static boolean A0D(Long l, long j, boolean z) {
        Object[] objArr;
        String str;
        if (l != null || z) {
            if (l != null) {
                long longValue = l.longValue();
                if (longValue < j) {
                    objArr = new Object[1];
                    C12980iv.A1U(objArr, 0, j);
                    str = "Value is less than the specified lower bound value of %d";
                } else if (longValue > 9007199254740991L) {
                    objArr = new Object[1];
                    C12980iv.A1U(objArr, 0, 9007199254740991L);
                    str = "Value is greater than the specified lower bound value of %d";
                }
                AnonymousClass009.A07(String.format(str, objArr));
            }
            return true;
        }
        AnonymousClass009.A07("Received null value for non-optional ':int'.");
        return false;
    }

    public static boolean A0E(String str, long j, long j2, boolean z) {
        Object[] objArr;
        String str2;
        if (str != null || z) {
            if (str != null) {
                long length = (long) str.length();
                if (length < j) {
                    objArr = new Object[1];
                    C12980iv.A1U(objArr, 0, j);
                    str2 = "Length of string is less than the specified lower bound value of %s";
                } else if (length > j2) {
                    objArr = new Object[1];
                    C12980iv.A1U(objArr, 0, j2);
                    str2 = "Length of string is greater than the specified upper bound value of %s";
                }
                AnonymousClass009.A07(String.format(str2, objArr));
            }
            return true;
        }
        AnonymousClass009.A07("Received null value for non-optional ':string'.");
        return false;
    }

    public static Object[] A0F(AnonymousClass1V8 r3, Object obj, int i) {
        Object[] objArr = new Object[i];
        objArr[0] = obj;
        objArr[1] = r3.A00;
        return objArr;
    }
}
