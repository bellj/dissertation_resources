package X;

/* renamed from: X.28L  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass28L extends AnonymousClass28M {
    public final C44721zR A00;

    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof AnonymousClass28L) && C16700pc.A0O(this.A00, ((AnonymousClass28L) obj).A00));
    }

    public int hashCode() {
        return this.A00.hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("CatalogSearchPageSuccessResult(catalogPage=");
        sb.append(this.A00);
        sb.append(')');
        return sb.toString();
    }

    public AnonymousClass28L(C44721zR r1) {
        this.A00 = r1;
    }
}
