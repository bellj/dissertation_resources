package X;

import com.whatsapp.util.Log;
import java.util.Map;

/* renamed from: X.5hN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C121175hN extends AbstractC19650uS {
    public final C17900ra A00;
    public final C133646Bo A01;
    public final C133596Bj A02 = new C133596Bj();
    public final C133616Bl A03 = new C133616Bl();
    public final C133666Bq A04;
    public final C133636Bn A05;
    public final C133656Bp A06;
    public final Map A07;
    public final Map A08;

    public C121175hN(C15550nR r10, C15610nY r11, AnonymousClass131 r12, C16590pI r13, AnonymousClass018 r14, C20300vX r15, C18600si r16, C17900ra r17, C17070qD r18, C18590sh r19, Map map, Map map2) {
        C16700pc.A0E(r13, 1);
        C117295Zj.A1K(r10, r11, r14, r19);
        C16700pc.A0E(r18, 6);
        C16700pc.A0E(r16, 7);
        C16700pc.A0E(r17, 9);
        C16700pc.A0E(r12, 10);
        C16700pc.A0E(map, 11);
        C16700pc.A0E(map2, 12);
        this.A00 = r17;
        this.A08 = map;
        this.A07 = map2;
        this.A06 = new C133656Bp(r10, r15, r18, r19);
        this.A04 = new C133666Bq(r13, r14, r16, r17, r18);
        this.A05 = new C133636Bn(r14);
        this.A01 = new C133646Bo(r11, r12, r13, r18);
    }

    @Override // X.AbstractC19650uS
    public /* bridge */ /* synthetic */ AbstractC16960q2 A00() {
        return this.A06;
    }

    @Override // X.AbstractC19650uS
    public Object A01(AnonymousClass3II r4, Object obj, Map map) {
        AnonymousClass3II[] r0;
        String str;
        AbstractC16960q2 r02;
        String str2;
        String str3;
        if (r4 == null || (r0 = r4.A02) == null || r0.length == 0) {
            return obj;
        }
        if (obj instanceof AbstractC28901Pl) {
            r02 = this.A04;
        } else if (obj instanceof AnonymousClass20C) {
            r02 = this.A05;
        } else if (obj instanceof AnonymousClass1ZY) {
            Map map2 = this.A08;
            C17930rd A01 = this.A00.A01();
            if (A01 == null) {
                str3 = null;
            } else {
                str3 = A01.A03;
            }
            r02 = (AbstractC16960q2) map2.get(str3);
            if (r02 == null) {
                str = "PaymentClientDaslQueryResolverRegistry/resolveObject PaymentMethodCountryData country not supported";
                Log.e(str);
                return null;
            }
        } else if (obj instanceof C15370n3) {
            r02 = this.A01;
        } else if (obj instanceof C16380ov) {
            r02 = this.A03;
        } else if (obj instanceof AnonymousClass1ZO) {
            Map map3 = this.A07;
            C17930rd A012 = this.A00.A01();
            if (A012 == null) {
                str2 = null;
            } else {
                str2 = A012.A03;
            }
            r02 = (AbstractC16960q2) map3.get(str2);
            if (r02 == null) {
                str = "PaymentClientDaslQueryResolverRegistry/resolveObject PaymentContactInfoCountryData country not supported";
                Log.e(str);
                return null;
            }
        } else if (obj instanceof C129785yI) {
            r02 = this.A02;
        } else {
            str = "PaymentClientDaslQueryResolverRegistry/resolveObject Object type not supported";
            Log.e(str);
            return null;
        }
        return A02(r4, r02, obj, map);
    }
}
