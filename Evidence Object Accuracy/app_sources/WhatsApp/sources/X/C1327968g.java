package X;

import com.whatsapp.util.Log;

/* renamed from: X.68g  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1327968g implements AnonymousClass1FK {
    public final /* synthetic */ C123595nP A00;

    public C1327968g(C123595nP r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass1FK
    public void AV3(C452120p r4) {
        Log.i("DyiViewModel/delete-report/on-error");
        int i = r4.A00;
        C123595nP r0 = this.A00;
        if (i == 404) {
            AnonymousClass60U r2 = r0.A08;
            String str = r0.A0A;
            synchronized (r2) {
                r2.A05(str);
            }
            AnonymousClass60U.A01(r0.A02, r2, str);
            return;
        }
        C117305Zk.A1B(r0.A03, C12960it.A0V(), r4);
    }

    @Override // X.AnonymousClass1FK
    public void AVA(C452120p r3) {
        Log.i("DyiViewModel/delete-report/on-error");
        C117305Zk.A1B(this.A00.A03, C12960it.A0V(), r3);
    }

    @Override // X.AnonymousClass1FK
    public void AVB(C452220q r4) {
        Log.i("DyiViewModel/delete-report/on-success");
        C123595nP r0 = this.A00;
        AnonymousClass60U r2 = r0.A08;
        String str = r0.A0A;
        synchronized (r2) {
            r2.A05(str);
        }
        AnonymousClass60U.A01(r0.A02, r2, str);
    }
}
