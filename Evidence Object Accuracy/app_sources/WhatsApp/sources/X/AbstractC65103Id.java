package X;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.conversationslist.ViewHolder;

/* renamed from: X.3Id  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC65103Id {
    public final Activity A00;
    public final Context A01;
    public final C15570nT A02;
    public final C15450nH A03;
    public final AnonymousClass11L A04;
    public final C14650lo A05;
    public final C238013b A06;
    public final C15550nR A07;
    public final C15610nY A08;
    public final AbstractC33091dK A09;
    public final C63543Bz A0A;
    public final ViewHolder A0B;
    public final C14830m7 A0C;
    public final C16590pI A0D;
    public final AnonymousClass018 A0E;
    public final C19990v2 A0F;
    public final C241714m A0G;
    public final C14850m9 A0H;
    public final C20710wC A0I;
    public final AnonymousClass13H A0J;
    public final C22710zW A0K;
    public final C17070qD A0L;
    public final AnonymousClass14X A0M;
    public final AnonymousClass3J9 A0N;

    public AbstractC65103Id(Activity activity, Context context, C15570nT r4, C15450nH r5, AnonymousClass11L r6, C14650lo r7, C238013b r8, C15550nR r9, C15610nY r10, AbstractC33091dK r11, C63543Bz r12, ViewHolder viewHolder, C14830m7 r14, C16590pI r15, AnonymousClass018 r16, C19990v2 r17, C241714m r18, C14850m9 r19, C20710wC r20, AnonymousClass13H r21, C22710zW r22, C17070qD r23, AnonymousClass14X r24, AnonymousClass3J9 r25) {
        this.A0B = viewHolder;
        this.A01 = context;
        this.A00 = activity;
        this.A0A = r12;
        this.A0C = r14;
        this.A0H = r19;
        this.A0J = r21;
        this.A02 = r4;
        this.A0D = r15;
        this.A0F = r17;
        this.A03 = r5;
        this.A0M = r24;
        this.A07 = r9;
        this.A0G = r18;
        this.A08 = r10;
        this.A0E = r16;
        this.A0L = r23;
        this.A0N = r25;
        this.A06 = r8;
        this.A0I = r20;
        this.A0K = r22;
        this.A05 = r7;
        this.A04 = r6;
        this.A09 = r11;
    }

    public static Drawable A01(Context context, ImageView imageView, AnonymousClass1XH r5) {
        Drawable drawable = null;
        if (r5.AHc() != 1) {
            drawable = AnonymousClass2GE.A01(context, R.drawable.msg_status_viewonce_one, R.color.msgStatusTint);
        }
        imageView.setImageDrawable(drawable);
        Drawable A01 = AnonymousClass2GE.A01(context, R.drawable.msg_status_ephemeral_ring, R.color.msgStatusTint);
        imageView.setBackground(A01);
        imageView.setVisibility(0);
        return A01;
    }

    public static Drawable A02(Context context, AbstractC65103Id r4, int i) {
        Drawable A01 = AnonymousClass2GE.A01(context, i, R.color.msgStatusTint);
        ImageView imageView = r4.A0B.A0B;
        imageView.setVisibility(0);
        imageView.setImageDrawable(A01);
        return A01;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:27:0x007d */
    /* JADX DEBUG: Multi-variable search result rejected for r4v2, resolved type: java.lang.String */
    /* JADX DEBUG: Multi-variable search result rejected for r4v3, resolved type: java.lang.String */
    /* JADX DEBUG: Multi-variable search result rejected for r4v4, resolved type: java.lang.String */
    /* JADX DEBUG: Multi-variable search result rejected for r4v14, resolved type: java.lang.String */
    /* JADX DEBUG: Multi-variable search result rejected for r4v29, resolved type: java.lang.String */
    /* JADX DEBUG: Multi-variable search result rejected for r4v32, resolved type: java.lang.String */
    /* JADX DEBUG: Multi-variable search result rejected for r4v77, resolved type: java.lang.Object */
    /* JADX DEBUG: Multi-variable search result rejected for r2v45, resolved type: X.13H */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r4v72 */
    /* JADX WARN: Type inference failed for: r4v78, types: [java.lang.CharSequence] */
    /* JADX WARN: Type inference failed for: r4v79, types: [android.text.SpannableStringBuilder] */
    /* JADX WARN: Type inference failed for: r4v80, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r4v81, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r4v82, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r4v83 */
    /* JADX WARN: Type inference failed for: r4v84 */
    /* JADX WARN: Type inference failed for: r4v88 */
    /* JADX WARN: Type inference failed for: r4v89 */
    /* JADX WARN: Type inference failed for: r4v90 */
    /* JADX WARN: Type inference failed for: r4v91 */
    /* JADX WARN: Type inference failed for: r4v92 */
    /* JADX WARN: Type inference failed for: r4v93 */
    /* JADX WARN: Type inference failed for: r4v94 */
    /* JADX WARN: Type inference failed for: r4v95 */
    /* JADX WARN: Type inference failed for: r4v96 */
    /* JADX WARN: Type inference failed for: r4v97 */
    /* JADX WARN: Type inference failed for: r4v98 */
    /* JADX WARN: Type inference failed for: r4v99 */
    /* JADX WARN: Type inference failed for: r4v100 */
    /* JADX WARN: Type inference failed for: r4v101 */
    /* JADX WARN: Type inference failed for: r4v102 */
    /* JADX WARN: Type inference failed for: r4v103 */
    /* JADX WARN: Type inference failed for: r4v104 */
    /* JADX WARN: Type inference failed for: r4v105 */
    /* JADX WARN: Type inference failed for: r4v106 */
    /* JADX WARN: Type inference failed for: r4v107 */
    /* JADX WARN: Type inference failed for: r4v108 */
    /* JADX WARN: Type inference failed for: r4v109 */
    /* JADX WARN: Type inference failed for: r4v110 */
    /* JADX WARN: Type inference failed for: r4v111 */
    /* JADX WARN: Type inference failed for: r4v112 */
    /* JADX WARN: Type inference failed for: r4v113 */
    /* JADX WARNING: Code restructure failed: missing block: B:159:0x0348, code lost:
        if (r1.A03 != 5) goto L_0x034a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x007d, code lost:
        if (r10 != null) goto L_0x003a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:149:0x0323  */
    /* JADX WARNING: Removed duplicated region for block: B:162:0x034e  */
    /* JADX WARNING: Removed duplicated region for block: B:175:0x0398  */
    /* JADX WARNING: Removed duplicated region for block: B:205:0x0449  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.util.Pair A03(X.AbstractC15340mz r10) {
        /*
        // Method dump skipped, instructions count: 1615
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC65103Id.A03(X.0mz):android.util.Pair");
    }

    public void A04() {
        if (this instanceof C61182zY) {
            C61232zd r0 = ((C61182zY) this).A00;
            if (r0 != null) {
                r0.A00();
            }
        } else if (this instanceof C61202za) {
            C61202za r1 = (C61202za) this;
            C61222zc r02 = r1.A01;
            if (r02 != null) {
                r02.A00();
            }
            AnonymousClass42Q r03 = r1.A00;
            if (r03 != null) {
                r03.A00();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0088, code lost:
        if (r11.A00() == false) goto L_0x008a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A05(X.AbstractC51662Vw r15, X.AnonymousClass5U4 r16, int r17, boolean r18) {
        /*
        // Method dump skipped, instructions count: 478
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC65103Id.A05(X.2Vw, X.5U4, int, boolean):void");
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v3, types: [java.lang.CharSequence] */
    /* JADX WARN: Type inference failed for: r2v10, types: [android.text.SpannableString] */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x0249, code lost:
        if (r2 != false) goto L_0x024b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00ce, code lost:
        if (r24.A0C == 6) goto L_0x00d0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x01d2, code lost:
        if (X.C37381mH.A00(r24.A0C, 4) >= 0) goto L_0x01d4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x01f3, code lost:
        if (r1 != 6) goto L_0x01f5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x01f5, code lost:
        r1 = com.whatsapp.R.drawable.msg_status_waiting;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x0220, code lost:
        if ((r24.A0I + 86400000) < r12.A00()) goto L_0x0222;
     */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0068 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00c9  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00e8  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x014c  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0162  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x016b  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0181  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x0201 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x022a  */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A06(X.C15370n3 r21, X.C15370n3 r22, X.C15370n3 r23, X.AbstractC15340mz r24, X.AnonymousClass1YY r25) {
        /*
        // Method dump skipped, instructions count: 633
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC65103Id.A06(X.0n3, X.0n3, X.0n3, X.0mz, X.1YY):void");
    }
}
