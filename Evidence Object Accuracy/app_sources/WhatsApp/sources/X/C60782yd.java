package X;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.CircularProgressBar;
import com.whatsapp.Conversation;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.conversation.conversationrow.ConversationRowImage$RowImageView;
import java.io.File;

/* renamed from: X.2yd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60782yd extends AbstractC60592yH {
    public View A00;
    public View A01;
    public ImageView A02;
    public TextView A03 = C12960it.A0J(this, R.id.control_btn);
    public CircularProgressBar A04;
    public TextEmojiLabel A05;
    public ConversationRowImage$RowImageView A06 = ((ConversationRowImage$RowImageView) findViewById(R.id.image));
    public boolean A07;
    public final AbstractC41521tf A08 = new C70193at(this);

    public C60782yd(Context context, AbstractC13890kV r4, AnonymousClass1X7 r5) {
        super(context, r4, r5);
        CircularProgressBar circularProgressBar = (CircularProgressBar) findViewById(R.id.progress_bar);
        this.A04 = circularProgressBar;
        circularProgressBar.A0B = 0;
        this.A02 = C12970iu.A0L(this, R.id.cancel_download);
        this.A00 = findViewById(R.id.control_frame);
        this.A01 = findViewById(R.id.text_and_date);
        TextEmojiLabel A0U = C12970iu.A0U(this, R.id.caption);
        this.A05 = A0U;
        if (A0U != null) {
            AbstractC28491Nn.A03(A0U);
            this.A05.setAutoLinkMask(0);
            this.A05.setLinksClickable(false);
            this.A05.setFocusable(false);
            this.A05.setClickable(false);
            this.A05.setLongClickable(false);
        }
        A0X(true);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0019, code lost:
        if (r5 <= 500) goto L_0x001b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A0X(boolean r22) {
        /*
        // Method dump skipped, instructions count: 372
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C60782yd.A0X(boolean):void");
    }

    @Override // X.AnonymousClass1OY
    public int A0n(int i) {
        if (!AnonymousClass1OY.A0T(this)) {
            return super.A0n(i);
        }
        return 0;
    }

    @Override // X.AnonymousClass1OY
    public void A0s() {
        A1H(false);
        A0X(false);
    }

    @Override // X.AbstractC42671vd, X.AnonymousClass1OY
    public void A0y() {
        boolean z;
        if (((AbstractC42671vd) this).A01 == null || AnonymousClass1OY.A0U(this)) {
            AbstractC16130oV r6 = (AbstractC16130oV) ((AbstractC28551Oa) this).A0O;
            C16150oX A00 = AbstractC15340mz.A00(r6);
            AnonymousClass1IS r3 = r6.A0z;
            boolean z2 = r3.A02;
            if (z2 || A00.A0P) {
                File file = A00.A0F;
                boolean z3 = false;
                if (file != null) {
                    z = C12980iv.A0h(file).exists();
                } else if (!z2 || A00.A0O) {
                    z = false;
                } else {
                    ((AnonymousClass1OY) this).A0J.A07(R.string.cannot_open_image_wait_until_processed, 0);
                    return;
                }
                AnonymousClass1OY.A0P(A00, r6, C12960it.A0k("viewmessage/ from_me:"), z2);
                if (!z) {
                    AnonymousClass1OY.A0S(this, r3);
                    return;
                }
                C64533Fx r0 = ((AbstractC28551Oa) this).A0b;
                if (r0 != null && r0.A08()) {
                    z3 = true;
                }
                AnonymousClass2TS r2 = new AnonymousClass2TS(getContext());
                r2.A07 = z3;
                AbstractC14640lm r02 = r3.A00;
                AnonymousClass009.A05(r02);
                r2.A03 = r02;
                r2.A04 = r3;
                r2.A06 = C12960it.A1W(AbstractC35731ia.A01(getContext(), Conversation.class));
                r2.A00 = 33;
                Intent A002 = r2.A00();
                ConversationRowImage$RowImageView conversationRowImage$RowImageView = this.A06;
                if (conversationRowImage$RowImageView != null) {
                    AbstractC454421p.A07(getContext(), A002, conversationRowImage$RowImageView);
                }
                AnonymousClass1OY.A0E(A002, this, conversationRowImage$RowImageView, r3);
            }
        }
    }

    @Override // X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r2, boolean z) {
        boolean A1X = C12960it.A1X(r2, ((AbstractC28551Oa) this).A0O);
        super.A1D(r2, z);
        if (z || A1X) {
            A0X(A1X);
        }
    }

    @Override // X.AnonymousClass1OY, android.view.ViewGroup, android.view.View
    public void dispatchSetPressed(boolean z) {
        boolean isPressed;
        super.dispatchSetPressed(z);
        ConversationRowImage$RowImageView conversationRowImage$RowImageView = this.A06;
        if (conversationRowImage$RowImageView != null && conversationRowImage$RowImageView.A0A != (isPressed = isPressed())) {
            conversationRowImage$RowImageView.A0A = isPressed;
            conversationRowImage$RowImageView.A02();
            conversationRowImage$RowImageView.invalidate();
        }
    }

    @Override // X.AnonymousClass1OY
    public int getBroadcastDrawableId() {
        return AnonymousClass1OY.A03(this);
    }

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_image_left;
    }

    @Override // X.AbstractC42671vd, X.AbstractC28551Oa
    public AnonymousClass1X7 getFMessage() {
        return (AnonymousClass1X7) ((AbstractC16130oV) ((AbstractC28551Oa) this).A0O);
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_image_left;
    }

    @Override // X.AnonymousClass1OY
    public Drawable getKeepDrawable() {
        if (AnonymousClass1OY.A0T(this)) {
            return AnonymousClass2GE.A01(getContext(), R.drawable.keep, R.color.white);
        }
        return super.getKeepDrawable();
    }

    @Override // X.AbstractC28551Oa
    public int getMainChildMaxWidth() {
        return this.A06.A02.A03();
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_image_right;
    }

    @Override // X.AbstractC28551Oa
    public int getReactionsViewVerticalOverlap() {
        Resources resources;
        int i;
        if (((AbstractC28551Oa) this).A0R) {
            resources = getResources();
            i = R.dimen.space_base_halfStep;
        } else if (!AnonymousClass1OY.A0T(this)) {
            return super.getReactionsViewVerticalOverlap();
        } else {
            resources = getResources();
            i = R.dimen.space_tight_halfStep;
        }
        return resources.getDimensionPixelOffset(i);
    }

    @Override // X.AnonymousClass1OY
    public Drawable getStarDrawable() {
        if (AnonymousClass1OY.A0T(this)) {
            return AnonymousClass00T.A04(getContext(), R.drawable.message_star_media);
        }
        return super.getStarDrawable();
    }

    @Override // X.AbstractC42671vd, X.AbstractC28551Oa
    public void setFMessage(AbstractC15340mz r2) {
        AnonymousClass009.A0F(r2 instanceof AnonymousClass1X7);
        super.setFMessage(r2);
    }

    public final void setImageViewDimensions(AnonymousClass1X7 r4, C16150oX r5) {
        ConversationRowImage$RowImageView conversationRowImage$RowImageView;
        ImageView.ScaleType scaleType;
        int i;
        int i2 = r5.A08;
        if (i2 == 0 || (i = r5.A06) == 0) {
            int i3 = 100;
            int A00 = AnonymousClass19O.A00(r4, 100);
            if (A00 > 0) {
                conversationRowImage$RowImageView = this.A06;
            } else {
                i3 = C27531Hw.A01(getContext());
                conversationRowImage$RowImageView = this.A06;
                A00 = (i3 * 9) >> 4;
            }
            conversationRowImage$RowImageView.A04(i3, A00);
        } else {
            conversationRowImage$RowImageView = this.A06;
            conversationRowImage$RowImageView.A04(i2, i);
            if (!((AbstractC28551Oa) this).A0R && !(this instanceof C60772yc)) {
                scaleType = ImageView.ScaleType.MATRIX;
                conversationRowImage$RowImageView.setScaleType(scaleType);
            }
        }
        scaleType = ImageView.ScaleType.CENTER_CROP;
        conversationRowImage$RowImageView.setScaleType(scaleType);
    }
}
