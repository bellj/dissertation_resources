package X;

import android.content.Context;
import android.text.Spannable;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import com.whatsapp.R;

/* renamed from: X.47d  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass47d extends AnonymousClass3J9 {
    @Override // X.AnonymousClass3J9
    public void A03(Context context, Spannable spannable, int i, int i2) {
        spannable.setSpan(new BackgroundColorSpan(AnonymousClass00T.A00(context, R.color.conversation_search_highlight)), i, i2, 33);
        spannable.setSpan(new ForegroundColorSpan(AnonymousClass00T.A00(context, R.color.conversation_search_highlight_text)), i, i2, 33);
    }
}
