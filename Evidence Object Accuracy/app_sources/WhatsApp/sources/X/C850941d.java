package X;

import com.whatsapp.status.StatusesFragment;

/* renamed from: X.41d  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C850941d extends AnonymousClass2Dn {
    public final /* synthetic */ StatusesFragment A00;

    public C850941d(StatusesFragment statusesFragment) {
        this.A00 = statusesFragment;
    }

    @Override // X.AnonymousClass2Dn
    public void A00(AbstractC14640lm r4) {
        StatusesFragment statusesFragment = this.A00;
        statusesFragment.A0g.getFilter().filter(statusesFragment.A0t);
    }
}
