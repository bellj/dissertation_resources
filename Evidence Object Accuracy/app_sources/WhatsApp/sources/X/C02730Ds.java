package X;

import android.view.accessibility.AccessibilityNodeInfo;

/* renamed from: X.0Ds  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02730Ds extends AnonymousClass0BE {
    public C02730Ds(C06070Sb r1) {
        super(r1);
    }

    @Override // android.view.accessibility.AccessibilityNodeProvider
    public AccessibilityNodeInfo findFocus(int i) {
        AnonymousClass04Z A01 = this.A00.A01(i);
        if (A01 == null) {
            return null;
        }
        return A01.A02;
    }
}
