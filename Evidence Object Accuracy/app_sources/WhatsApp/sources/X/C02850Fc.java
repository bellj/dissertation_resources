package X;

/* renamed from: X.0Fc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02850Fc extends AbstractC05330Pd {
    public final /* synthetic */ C07740a0 A00;

    @Override // X.AbstractC05330Pd
    public String A01() {
        return "UPDATE workspec SET schedule_requested_at=? WHERE id=?";
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C02850Fc(AnonymousClass0QN r1, C07740a0 r2) {
        super(r1);
        this.A00 = r2;
    }
}
