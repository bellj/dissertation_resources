package X;

import com.whatsapp.jid.Jid;

/* renamed from: X.2Q9  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Q9 extends AnonymousClass2PA {
    public final AnonymousClass2QA A00;

    public AnonymousClass2Q9(Jid jid, AnonymousClass2QA r2, String str, long j) {
        super(jid, str, j);
        this.A00 = r2;
    }
}
