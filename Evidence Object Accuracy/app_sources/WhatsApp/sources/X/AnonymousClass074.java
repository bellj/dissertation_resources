package X;

/* renamed from: X.074  reason: invalid class name */
/* loaded from: classes.dex */
public enum AnonymousClass074 {
    ON_CREATE,
    ON_START,
    ON_RESUME,
    ON_PAUSE,
    ON_STOP,
    ON_DESTROY,
    ON_ANY;

    public static AnonymousClass074 A00(AnonymousClass05I r0) {
        switch (r0.ordinal()) {
            case 1:
                return ON_CREATE;
            case 2:
                return ON_START;
            case 3:
                return ON_RESUME;
            default:
                return null;
        }
    }

    public AnonymousClass05I A01() {
        switch (ordinal()) {
            case 0:
            case 4:
                return AnonymousClass05I.CREATED;
            case 1:
            case 3:
                return AnonymousClass05I.STARTED;
            case 2:
                return AnonymousClass05I.RESUMED;
            case 5:
                return AnonymousClass05I.DESTROYED;
            default:
                StringBuilder sb = new StringBuilder();
                sb.append(this);
                sb.append(" has no target state");
                throw new IllegalArgumentException(sb.toString());
        }
    }
}
