package X;

import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;

/* renamed from: X.09u  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass09u extends PorterDuffColorFilter {
    public AnonymousClass09u(int i) {
        super(i, PorterDuff.Mode.SRC_ATOP);
    }
}
