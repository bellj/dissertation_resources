package X;

/* renamed from: X.5Xk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public interface AbstractC116915Xk {
    public static final AnonymousClass1TK A00;
    public static final AnonymousClass1TK A01;
    public static final AnonymousClass1TK A02;
    public static final AnonymousClass1TK A03;
    public static final AnonymousClass1TK A04;
    public static final AnonymousClass1TK A05;
    public static final AnonymousClass1TK A06;
    public static final AnonymousClass1TK A07;

    static {
        AnonymousClass1TK A12 = C72453ed.A12("1.0.10118");
        A04 = A12;
        AnonymousClass1TK A022 = AnonymousClass1TL.A02("3.0", A12);
        A00 = A022;
        A06 = AnonymousClass1TL.A02("49", A022);
        A05 = AnonymousClass1TL.A02("50", A022);
        A07 = AnonymousClass1TL.A02("55", A022);
        AnonymousClass1TK A122 = C72453ed.A12("1.0.18033.2");
        A03 = A122;
        A01 = AnonymousClass1TL.A02("1.2", A122);
        A02 = AnonymousClass1TL.A02("2.4", A122);
    }
}
