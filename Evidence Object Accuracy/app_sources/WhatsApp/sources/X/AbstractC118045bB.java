package X;

import android.content.Context;
import com.facebook.redex.IDxAListenerShape19S0100000_3_I1;
import com.facebook.redex.IDxAListenerShape1S0200000_3_I1;
import com.facebook.redex.IDxCListenerShape11S0100000_3_I1;
import java.util.AbstractCollection;
import java.util.List;
import org.wawebrtc.MediaCodecVideoEncoder;

/* renamed from: X.5bB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC118045bB extends AnonymousClass015 {
    public AnonymousClass016 A00 = new AnonymousClass016(C12960it.A0l());
    public C27691It A01 = C13000ix.A03();
    public boolean A02 = false;
    public final AnonymousClass61F A03;

    public AbstractC118045bB(AnonymousClass61F r3) {
        this.A03 = r3;
    }

    public static C27691It A00(Context context, AnonymousClass60Y r2, AnonymousClass610 r3, AbstractC118045bB r4, int i) {
        String string = context.getString(i);
        C128365vz r0 = r3.A00;
        r0.A0L = string;
        r2.A05(r0);
        return r4.A01;
    }

    public static void A01(Object obj, String str, String str2, AbstractCollection abstractCollection, int i) {
        IDxCListenerShape11S0100000_3_I1 iDxCListenerShape11S0100000_3_I1 = new IDxCListenerShape11S0100000_3_I1(obj, i);
        C122865mE r0 = new C122865mE();
        r0.A01 = str;
        r0.A02 = str2;
        r0.A00 = iDxCListenerShape11S0100000_3_I1;
        abstractCollection.add(r0);
    }

    public static void A02(Object obj, String str, AbstractCollection abstractCollection, int i, int i2) {
        abstractCollection.add(AnonymousClass612.A00(new IDxCListenerShape11S0100000_3_I1(obj, i), str, i2));
    }

    public static boolean A03(AbstractCollection abstractCollection) {
        C123235mp r1 = new C123235mp();
        r1.A03 = false;
        r1.A00 = 0;
        abstractCollection.add(r1);
        return false;
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [boolean, int] */
    /* JADX WARN: Type inference failed for: r12v0, types: [boolean] */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A04() {
        /*
        // Method dump skipped, instructions count: 1026
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC118045bB.A04():void");
    }

    public void A05(AbstractC001200n r13, ActivityC13790kL r14) {
        if (this instanceof C123395n5) {
            C123395n5 r2 = (C123395n5) this;
            C14830m7 r7 = r2.A0A;
            C14850m9 r8 = r2.A0E;
            new AnonymousClass61D(r2.A08, r7, r8, r2.A0G, r2.A0H, r2.A0N).A03(r2.A0D, new IDxAListenerShape19S0100000_3_I1(r2, 12), r2.A0J);
            if (r8.A07(822)) {
                C129685y8 r3 = r2.A0L;
                r3.A05.Ab6(new Runnable() { // from class: X.6Fa
                    @Override // java.lang.Runnable
                    public final void run() {
                        C129685y8 r4 = C129685y8.this;
                        C1310460z A01 = AnonymousClass61S.A01("novi-get-preferred-two-factor-method-auth");
                        C130155yt.A00(new IDxAListenerShape19S0100000_3_I1(r4, 6), r4.A02, A01);
                    }
                });
            }
            r2.A08(r14, r13);
            r2.A09(r13);
            AnonymousClass017 A00 = r2.A0K.A00();
            r2.A01 = A00;
            C117295Zj.A0s(r13, A00, r2, MediaCodecVideoEncoder.MIN_ENCODER_HEIGHT);
        } else if (this instanceof C123365n2) {
        } else {
            if (this instanceof C123385n4) {
                C123385n4 r4 = (C123385n4) this;
                C129235xO r32 = r4.A05;
                AnonymousClass016 A0T = C12980iv.A0T();
                C14580lf A0C = C117305Zk.A0C(r32.A02);
                A0C.A00(new AbstractC14590lg(A0T, A0C, r32) { // from class: X.6Ee
                    public final /* synthetic */ AnonymousClass016 A00;
                    public final /* synthetic */ C14580lf A01;
                    public final /* synthetic */ C129235xO A02;

                    {
                        this.A02 = r3;
                        this.A00 = r1;
                        this.A01 = r2;
                    }

                    @Override // X.AbstractC14590lg
                    public final void accept(Object obj) {
                        C129235xO r1 = this.A02;
                        AnonymousClass016 r33 = this.A00;
                        C14580lf r22 = this.A01;
                        List list = (List) obj;
                        if (list.size() > 0) {
                            AnonymousClass61F r12 = r1.A05;
                            AbstractC28901Pl A002 = AnonymousClass61F.A00(list);
                            AnonymousClass009.A05(A002);
                            C1315463e A02 = AnonymousClass61F.A02(A002);
                            AnonymousClass009.A05(A02);
                            r33.A0A(r12.A05(A02, list));
                            r22.A04();
                            return;
                        }
                        r22.A04();
                        r33.A0A(null);
                    }
                });
                A0T.A05(r13, new AnonymousClass02B(r13, r14, r4) { // from class: X.65g
                    public final /* synthetic */ AbstractC001200n A00;
                    public final /* synthetic */ ActivityC13790kL A01;
                    public final /* synthetic */ C123385n4 A02;

                    {
                        this.A02 = r3;
                        this.A01 = r2;
                        this.A00 = r1;
                    }

                    @Override // X.AnonymousClass02B
                    public final void ANq(Object obj) {
                        C123385n4 r33 = this.A02;
                        ActivityC13790kL r22 = this.A01;
                        AbstractC001200n r1 = this.A00;
                        r33.A08(false);
                        List A01 = AnonymousClass612.A01((List) obj);
                        r33.A02 = A01;
                        r33.A07(r22, r1, A01);
                        r33.A04();
                    }
                });
            } else if (!(this instanceof C123375n3)) {
                C123355n1 r33 = (C123355n1) this;
                C127055ts.A00(((AbstractC118045bB) r33).A01, 500);
                new AnonymousClass61D(r33.A01, r33.A04, r33.A07, r33.A09, r33.A0A, r33.A0C).A03(r33.A06, C117305Zk.A09(r14, r33, 51), r33.A0B);
            } else {
                C123375n3 r22 = (C123375n3) this;
                AnonymousClass602 A002 = r22.A0A.A00();
                if (A002 != null) {
                    r22.A07(A002);
                    r22.A04();
                } else {
                    r22.A0D.Ab2(new Runnable(r14, r22) { // from class: X.6Il
                        public final /* synthetic */ ActivityC13790kL A00;
                        public final /* synthetic */ C123375n3 A01;

                        {
                            this.A01 = r2;
                            this.A00 = r1;
                        }

                        @Override // java.lang.Runnable
                        public final void run() {
                            C123375n3 r34 = this.A01;
                            ActivityC13790kL r1 = this.A00;
                            AnonymousClass61D r42 = new AnonymousClass61D(r34.A02, r34.A03, r34.A05, r34.A07, r34.A08, r34.A0C);
                            IDxAListenerShape1S0200000_3_I1 A09 = C117305Zk.A09(r1, r34, 52);
                            C130155yt.A00(new IDxAListenerShape19S0100000_3_I1(A09, 3), r42.A03, C117295Zj.A0F(AnonymousClass61S.A00("action", "novi-get-funding-source-view-config"), new AnonymousClass61S[1], 0));
                            r34.A04();
                        }
                    });
                }
                if (r22.A0E.equals("withdrawal")) {
                    C127055ts.A00(((AbstractC118045bB) r22).A01, 500);
                    C117295Zj.A0s(r13, r22.A0B.A00(), r22, 142);
                }
            }
        }
    }

    public void A06(AbstractC001200n r4, ActivityC13790kL r5, C125985s8 r6) {
        if (r6.A00 == 0) {
            AnonymousClass61F r2 = this.A03;
            C117305Zk.A19(r4, r2.A0G, this, 5);
            r2.A0H.A05(r4, new AnonymousClass02B(r4, r5, this) { // from class: X.65e
                public final /* synthetic */ AbstractC001200n A00;
                public final /* synthetic */ ActivityC13790kL A01;
                public final /* synthetic */ AbstractC118045bB A02;

                {
                    this.A02 = r3;
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // X.AnonymousClass02B
                public final void ANq(Object obj) {
                    AbstractC118045bB r3 = this.A02;
                    ActivityC13790kL r22 = this.A01;
                    AbstractC001200n r1 = this.A00;
                    if (r3.A02) {
                        r3.A02 = false;
                        r3.A05(r1, r22);
                        r3.A04();
                    }
                }
            });
            A05(r4, r5);
            A04();
        }
    }
}
