package X;

import java.security.cert.CRLSelector;
import java.security.cert.CertStore;
import java.security.cert.CertStoreException;
import java.security.cert.X509CRL;
import java.security.cert.X509CRLSelector;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* renamed from: X.4Yt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC93044Yt {
    public static Set A00(Date date, List list, List list2, C113105Ga r8) {
        HashSet A12 = C12970iu.A12();
        try {
            A01(A12, list2, r8);
            A01(A12, list, r8);
            HashSet A122 = C12970iu.A12();
            Iterator it = A12.iterator();
            while (it.hasNext()) {
                X509CRL x509crl = (X509CRL) it.next();
                if (x509crl.getNextUpdate().after(date)) {
                    CRLSelector cRLSelector = r8.A01;
                    X509Certificate certificateChecking = cRLSelector instanceof X509CRLSelector ? ((X509CRLSelector) cRLSelector).getCertificateChecking() : null;
                    if (certificateChecking == null || x509crl.getThisUpdate().before(certificateChecking.getNotAfter())) {
                        A122.add(x509crl);
                    }
                }
            }
            return A122;
        } catch (AnonymousClass4C6 e) {
            throw AnonymousClass4C6.A00("Exception obtaining complete CRLs.", e);
        }
    }

    public static void A01(HashSet hashSet, List list, C113105Ga r8) {
        AnonymousClass4C6 r4 = null;
        boolean z = false;
        for (Object obj : list) {
            if (obj instanceof AnonymousClass5VS) {
                try {
                    hashSet.addAll(((AnonymousClass5VS) obj).AE7(r8));
                } catch (C113175Gl | CertStoreException e) {
                    r4 = AnonymousClass4C6.A00("Exception searching in X.509 CRL store.", e);
                }
            } else {
                hashSet.addAll(((CertStore) obj).getCRLs(new C113455Hq(r8)));
            }
            z = true;
        }
        if (!(z || r4 == null)) {
            throw r4;
        }
    }
}
