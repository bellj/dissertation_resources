package X;

/* renamed from: X.39q  reason: invalid class name */
/* loaded from: classes2.dex */
public enum AnonymousClass39q {
    A05(1),
    A01(2),
    A03(3),
    A06(4),
    A04(5),
    A02(0);
    
    public final int value;

    AnonymousClass39q(int i) {
        this.value = i;
    }
}
