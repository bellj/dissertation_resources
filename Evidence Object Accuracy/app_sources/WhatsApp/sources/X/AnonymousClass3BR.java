package X;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.3BR  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3BR {
    public int A00;
    public int A01;
    public final List A02 = C12960it.A0l();
    public final List A03 = C12960it.A0l();
    public final Map A04 = new LinkedHashMap();

    public AnonymousClass3BR(AnonymousClass1ZC r6, int i) {
        this.A00 = i;
        for (AnonymousClass1ZF r3 : r6.A02) {
            ArrayList A0l = C12960it.A0l();
            for (AnonymousClass1ZG r0 : r3.A01) {
                A0l.add(r0.A00);
                this.A01++;
            }
            this.A03.add(r3.A00);
            this.A02.add(A0l);
        }
    }
}
