package X;

import com.facebook.redex.RunnableBRunnable0Shape6S0100000_I0_6;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/* renamed from: X.1Hi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C27411Hi {
    public boolean A00;
    public final C27421Hj A01;
    public final AbstractC14440lR A02;
    public final Lock A03 = new ReentrantLock();

    public C27411Hi(C27421Hj r2, AbstractC14440lR r3) {
        this.A01 = r2;
        this.A02 = r3;
    }

    public void A00() {
        Lock lock = this.A03;
        lock.lock();
        if (!this.A00) {
            lock.unlock();
            this.A02.Ab2(new RunnableBRunnable0Shape6S0100000_I0_6(this, 32));
            return;
        }
        lock.unlock();
    }
}
