package X;

import java.util.HashSet;
import java.util.Set;

/* renamed from: X.0P0  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0P0 {
    public final Set A00 = new HashSet();

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || AnonymousClass0P0.class != obj.getClass()) {
            return false;
        }
        return this.A00.equals(((AnonymousClass0P0) obj).A00);
    }

    public int hashCode() {
        return this.A00.hashCode();
    }
}
