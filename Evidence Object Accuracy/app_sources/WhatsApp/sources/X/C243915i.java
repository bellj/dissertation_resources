package X;

/* renamed from: X.15i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C243915i {
    public int A00;
    public int A01;
    public int A02;
    public long A03;
    public final C16120oU A04;

    public C243915i(C16120oU r1) {
        this.A04 = r1;
    }

    public void A00(AbstractC14640lm r4, int i) {
        this.A00 = i;
        int i2 = 1;
        if (C15380n4.A0J(r4)) {
            i2 = 2;
        }
        this.A02 = i2;
        C32421c8 r2 = new C32421c8();
        int i3 = this.A01;
        if (i3 > 0) {
            r2.A01 = Integer.valueOf(i3);
        }
        r2.A02 = Integer.valueOf(i);
        r2.A00 = Integer.valueOf(i2);
        if (i == 1) {
            r2.A03 = Long.valueOf(this.A03);
        }
        this.A04.A07(r2);
    }
}
