package X;

/* renamed from: X.30G  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass30G extends AbstractC16110oT {
    public Boolean A00;
    public String A01;

    public AnonymousClass30G() {
        super(1846, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(2, this.A00);
        r3.Abe(3, this.A01);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamStickerPackDelete {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "stickerPackIsAvatar", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "waAvatarSessionId", this.A01);
        return C12960it.A0d("}", A0k);
    }
}
