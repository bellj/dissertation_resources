package X;

import android.graphics.Bitmap;
import android.net.Uri;
import java.lang.ref.WeakReference;

/* renamed from: X.391  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass391 extends AbstractC16350or {
    public String A00;
    public final Uri A01;
    public final C22190yg A02;
    public final WeakReference A03;

    public AnonymousClass391(Uri uri, AbstractC116105Ud r3, C22190yg r4) {
        this.A02 = r4;
        this.A03 = C12970iu.A10(r3);
        this.A01 = uri;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:8:0x0035 */
    /* JADX DEBUG: Multi-variable search result rejected for r0v3, resolved type: X.2Kb */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v2, types: [X.2KX, X.2KY] */
    /* JADX WARN: Type inference failed for: r0v6, types: [X.2Kb] */
    public static final C49262Kb A00(Bitmap bitmap, AnonymousClass2Ka r11) {
        int[] iArr = new int[bitmap.getWidth() * bitmap.getHeight()];
        bitmap.getPixels(iArr, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
        C49262Kb r0 = new AnonymousClass2KX(new C82543vn(bitmap.getWidth(), iArr, bitmap.getHeight()));
        try {
            r0 = r11.A00(new AnonymousClass2KZ(r0), null);
            return r0;
        } catch (AbstractC49392Ko unused) {
            return r0;
        }
    }
}
