package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4lT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C100164lT implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        C16700pc.A0E(parcel, 0);
        return new C65913Lo(C12960it.A1S(parcel.readByte()));
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C65913Lo[i];
    }
}
