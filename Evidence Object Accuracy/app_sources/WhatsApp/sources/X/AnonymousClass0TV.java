package X;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.view.Choreographer;

/* renamed from: X.0TV  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0TV {
    public static final AbstractC11140fo A00;
    public static volatile Choreographer choreographer;

    static {
        Object obj;
        AbstractC11140fo r2 = null;
        try {
            obj = new AnonymousClass5L5(A00(Looper.getMainLooper()));
        } catch (Throwable th) {
            obj = A01(th);
        }
        if (!AnonymousClass5BU.A01(obj)) {
            r2 = obj;
        }
        A00 = r2;
    }

    public static final Handler A00(Looper looper) {
        Object newInstance;
        if (Build.VERSION.SDK_INT >= 28) {
            newInstance = Handler.class.getDeclaredMethod("createAsync", Looper.class).invoke(null, looper);
            if (newInstance == null) {
                throw new NullPointerException("null cannot be cast to non-null type android.os.Handler");
            }
        } else {
            try {
                newInstance = Handler.class.getDeclaredConstructor(Looper.class, Handler.Callback.class, Boolean.TYPE).newInstance(looper, null, Boolean.TRUE);
            } catch (NoSuchMethodException unused) {
                return new Handler(looper);
            }
        }
        return (Handler) newInstance;
    }

    public static final Object A01(Throwable th) {
        return new AnonymousClass5BR(th);
    }
}
