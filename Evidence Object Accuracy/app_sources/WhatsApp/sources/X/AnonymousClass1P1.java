package X;

import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.util.Log;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import org.json.JSONObject;

/* renamed from: X.1P1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1P1 {
    public static String A00(InputStream inputStream) {
        if (inputStream == null) {
            return null;
        }
        char[] cArr = new char[DefaultCrypto.BUFFER_SIZE];
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        try {
            StringWriter stringWriter = new StringWriter();
            while (true) {
                int read = bufferedReader.read(cArr);
                if (read < 0) {
                    String obj = stringWriter.toString();
                    bufferedReader.close();
                    return obj;
                } else if (!Thread.currentThread().isInterrupted()) {
                    stringWriter.write(cArr, 0, read);
                } else {
                    throw new InterruptedIOException();
                }
            }
        } catch (Throwable th) {
            try {
                bufferedReader.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public static String A01(HttpURLConnection httpURLConnection) {
        return A00(httpURLConnection.getErrorStream());
    }

    public static JSONObject A02(InputStream inputStream) {
        String A00 = A00(inputStream);
        if (A00 != null) {
            return new JSONObject(A00);
        }
        return null;
    }

    public static void A03(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Throwable th) {
                Log.e(th);
            }
        }
    }

    public static byte[] A04(InputStream inputStream) {
        byte[] bArr = new byte[DefaultCrypto.BUFFER_SIZE];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(inputStream.available());
        while (true) {
            try {
                int read = inputStream.read(bArr, 0, DefaultCrypto.BUFFER_SIZE);
                if (read < 0) {
                    byteArrayOutputStream.flush();
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    byteArrayOutputStream.close();
                    return byteArray;
                }
                byteArrayOutputStream.write(bArr, 0, read);
            } catch (Throwable th) {
                try {
                    byteArrayOutputStream.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
    }
}
