package X;

import android.os.IBinder;
import android.os.IInterface;

/* renamed from: X.3py  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79053py extends C98384ib implements IInterface {
    public C79053py(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.signin.internal.ISignInService");
    }
}
