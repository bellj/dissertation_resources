package X;

import android.os.Parcel;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1Zf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC30891Zf extends AnonymousClass1ZP {
    public C30971Zn A00;
    public AnonymousClass20C A01;
    public C38171nd A02;
    public boolean A03;

    public abstract int A05();

    public abstract int A06();

    public abstract long A07();

    public abstract long A08();

    public abstract long A09();

    public AnonymousClass5YY A0A() {
        return null;
    }

    public abstract AnonymousClass1ZR A0B();

    public abstract AnonymousClass1ZR A0C();

    public abstract AnonymousClass1ZR A0D();

    public abstract String A0E();

    public abstract String A0F();

    public abstract String A0G();

    public abstract String A0H();

    public abstract String A0I();

    public abstract void A0K(int i);

    public abstract void A0L(int i);

    public abstract void A0M(int i);

    public abstract void A0N(long j);

    public abstract void A0O(long j);

    public abstract void A0S(String str);

    public abstract void A0T(String str);

    public abstract void A0U(String str);

    public abstract void A0V(String str);

    public boolean A0X() {
        return false;
    }

    public boolean A0Y(AnonymousClass1IR r2) {
        return false;
    }

    @Override // X.AnonymousClass1ZP
    public void A04(String str) {
        C38171nd r0;
        try {
            JSONObject jSONObject = new JSONObject(str);
            this.A03 = jSONObject.optBoolean("messageDeleted", false);
            JSONObject optJSONObject = jSONObject.optJSONObject("money");
            if (optJSONObject != null) {
                new AnonymousClass20C(C30771Yt.A06, 0);
                this.A01 = AnonymousClass20C.A00(optJSONObject);
            }
            JSONObject optJSONObject2 = jSONObject.optJSONObject("incentive");
            if (optJSONObject2 != null) {
                this.A00 = new C30971Zn(optJSONObject2);
            }
            JSONObject optJSONObject3 = jSONObject.optJSONObject("order");
            if (optJSONObject3 != null) {
                r0 = new C38171nd(optJSONObject3);
            } else {
                String optString = jSONObject.optString("orderId");
                long optLong = jSONObject.optLong("orderExpiryTsInSec");
                String optString2 = jSONObject.optString("orderMessageId");
                if (!TextUtils.isEmpty(optString) && !TextUtils.isEmpty(optString2)) {
                    r0 = new C38171nd(optString, optString2, optLong);
                } else {
                    return;
                }
            }
            this.A02 = r0;
        } catch (JSONException e) {
            Log.w("PAY: PaymentTransactionCountryData fromDBString threw: ", e);
        }
    }

    public JSONObject A0J() {
        JSONObject jSONObject = new JSONObject();
        boolean z = this.A03;
        if (z) {
            jSONObject.put("messageDeleted", z);
        }
        AnonymousClass20C r0 = this.A01;
        if (r0 != null) {
            jSONObject.put("money", r0.A02());
        }
        C30971Zn r3 = this.A00;
        if (r3 != null) {
            JSONObject jSONObject2 = new JSONObject();
            try {
                jSONObject2.put("offer-id", r3.A02);
                String str = r3.A01;
                if (str != null) {
                    jSONObject2.put("offer-claim-id", str);
                }
                String str2 = r3.A03;
                if (str2 != null) {
                    jSONObject2.put("parent-transaction-id", str2);
                }
                String str3 = r3.A00;
                if (str3 != null) {
                    jSONObject2.put("incentive-payment-id", str3);
                }
            } catch (JSONException e) {
                Log.w("PAY: PaymentIncentiveData toJson threw: ", e);
            }
            jSONObject.put("incentive", jSONObject2);
        }
        C38171nd r2 = this.A02;
        if (r2 != null) {
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("id", r2.A01);
            jSONObject3.put("message_id", r2.A02);
            jSONObject3.put("expiry_ts", r2.A00);
            jSONObject.put("order", jSONObject3);
        }
        return jSONObject;
    }

    public void A0P(Parcel parcel) {
        boolean z = true;
        if (parcel.readByte() != 1) {
            z = false;
        }
        this.A03 = z;
        this.A01 = (AnonymousClass20C) parcel.readParcelable(AnonymousClass20C.class.getClassLoader());
        this.A02 = (C38171nd) parcel.readParcelable(C38171nd.class.getClassLoader());
    }

    public void A0Q(AnonymousClass102 r7, AnonymousClass1IR r8, AnonymousClass1V8 r9, int i) {
        String A0I;
        String A0I2;
        A01(r7, r9, i);
        AnonymousClass20C r0 = r8.A09;
        if (r0 != null) {
            this.A01 = r0;
        }
        AnonymousClass1V8 A0E = r9.A0E("offer_claim");
        if (A0E != null) {
            String A0I3 = A0E.A0I("offer_id", null);
            String A0I4 = A0E.A0I("id", null);
            String A0I5 = A0E.A0I("parent_transaction_id", null);
            String A0I6 = A0E.A0I("incentive_payment_id", null);
            if (!(A0I3 == null || (A0I4 == null && A0I5 == null))) {
                this.A00 = new C30971Zn(A0I3, A0I4, A0I5, A0I6);
            }
        }
        AnonymousClass1V8 A0E2 = r9.A0E("order");
        if (A0E2 != null && (A0I = A0E2.A0I("id", null)) != null && (A0I2 = A0E2.A0I("message_id", null)) != null) {
            long j = 0;
            try {
                j = A0E2.A08("expiry_ts", 0);
            } catch (AnonymousClass1V9 unused) {
                Log.e("PAY: PaymentTransactionCountryData/parseOrderData : invalid expiry timestamp format");
            }
            this.A02 = new C38171nd(A0I, A0I2, j);
        }
    }

    public void A0R(AbstractC30891Zf r2) {
        this.A03 = r2.A03;
        AnonymousClass20C r0 = r2.A01;
        if (r0 != null) {
            this.A01 = r0;
        }
        C30971Zn r02 = r2.A00;
        if (r02 != null) {
            this.A00 = r02;
        }
        C38171nd r03 = r2.A02;
        if (r03 != null) {
            this.A02 = r03;
        }
    }

    public void A0W(String str, int i) {
        A04(str);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte(this.A03 ? (byte) 1 : 0);
        parcel.writeParcelable(this.A01, 0);
        parcel.writeParcelable(this.A02, 0);
    }
}
