package X;

/* renamed from: X.4aK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C93534aK {
    public static final C93534aK A03 = new C93534aK(-3, -9223372036854775807L, -1);
    public final int A00;
    public final long A01;
    public final long A02;

    public C93534aK(int i, long j, long j2) {
        this.A00 = i;
        this.A02 = j;
        this.A01 = j2;
    }
}
