package X;

/* renamed from: X.2zc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C61222zc extends AbstractCallableC112595Dz {
    public final AbstractC14640lm A00;
    public final /* synthetic */ C61202za A01;

    public C61222zc(C61202za r1, AbstractC14640lm r2) {
        this.A01 = r1;
        this.A00 = r2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005c  */
    @Override // X.AbstractCallableC112595Dz
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ java.lang.Object A01() {
        /*
            r19 = this;
            r6 = r19
            X.2za r5 = r6.A01
            X.0nR r7 = r5.A06
            X.0lm r9 = r6.A00
            X.0n3 r12 = r7.A0B(r9)
            X.10Y r0 = r5.A0E
            X.0mz r15 = r0.A01(r9)
            X.0xM r8 = r5.A0F
            X.0m9 r1 = r8.A0J
            r0 = 1605(0x645, float:2.249E-42)
            boolean r0 = r1.A07(r0)
            r1 = 0
            if (r0 == 0) goto L_0x0026
            if (r9 != 0) goto L_0x0063
            java.lang.String r0 = "MessageAddOnManager/getLastChatsListDisplayedMessageAddOn/jid is null"
            com.whatsapp.util.Log.e(r0)
        L_0x0026:
            X.0o1 r0 = r5.A0G
            X.1da r17 = X.C15860o1.A00(r9, r0)
            if (r1 == 0) goto L_0x0061
            X.1Iv r2 = r1.A02
            X.0lm r0 = r2.A0B()
            if (r0 == 0) goto L_0x0061
            X.0nT r0 = r5.A02
            X.0lm r0 = X.AnonymousClass3J0.A00(r0, r12, r2)
            if (r0 == 0) goto L_0x0061
            X.0n3 r13 = r7.A0B(r0)
        L_0x0042:
            X.0nT r0 = r5.A02
            X.0lm r0 = X.AnonymousClass3J0.A00(r0, r12, r15)
            if (r0 != 0) goto L_0x005c
            r14 = 0
        L_0x004b:
            X.02N r0 = r6.A00
            r0.A02()
            X.4Vw r0 = r5.A02
            r16 = r1
            r18 = r0
            X.3BZ r11 = new X.3BZ
            r11.<init>(r12, r13, r14, r15, r16, r17, r18)
            return r11
        L_0x005c:
            X.0n3 r14 = r7.A0B(r0)
            goto L_0x004b
        L_0x0061:
            r13 = 0
            goto L_0x0042
        L_0x0063:
            X.0v2 r0 = r8.A08
            X.1PE r4 = r0.A06(r9)
            if (r4 != 0) goto L_0x0075
            java.lang.String r0 = "MessageAddOnManager/getLastChatsListDisplayedMessageAddOn/no chat for "
            java.lang.String r0 = X.C12960it.A0b(r0, r9)
            com.whatsapp.util.Log.w(r0)
            goto L_0x0026
        L_0x0075:
            X.1YY r1 = r4.A0c
            if (r1 != 0) goto L_0x0026
            X.0p7 r0 = r8.A0D
            X.0on r3 = r0.get()
            long r10 = r4.A00()     // Catch: all -> 0x009a
            r1 = 0
            int r0 = (r10 > r1 ? 1 : (r10 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x0094
            long r1 = r4.A00()     // Catch: all -> 0x009a
            r0 = 1
            X.1YY r0 = r8.A04(r3, r0, r1)     // Catch: all -> 0x009a
            r4.A0c = r0     // Catch: all -> 0x009a
        L_0x0094:
            r3.close()
            X.1YY r1 = r4.A0c
            goto L_0x0026
        L_0x009a:
            r0 = move-exception
            r3.close()     // Catch: all -> 0x009e
        L_0x009e:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C61222zc.A01():java.lang.Object");
    }
}
