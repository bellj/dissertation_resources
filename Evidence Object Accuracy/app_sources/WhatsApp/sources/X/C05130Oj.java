package X;

import android.os.Build;
import android.text.TextUtils;

/* renamed from: X.0Oj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05130Oj {
    public int A00 = 0;
    public CharSequence A01 = null;
    public CharSequence A02 = null;
    public CharSequence A03 = null;
    public boolean A04 = true;
    public boolean A05 = false;

    public C05000Nw A00() {
        String str;
        boolean z;
        if (!TextUtils.isEmpty(this.A03)) {
            int i = this.A00;
            if (AnonymousClass0QX.A01(i)) {
                if (i != 0) {
                    z = false;
                    if ((i & 32768) != 0) {
                        z = true;
                    }
                } else {
                    z = this.A05;
                }
                if (TextUtils.isEmpty(this.A01) && !z) {
                    throw new IllegalArgumentException("Negative text must be set and non-empty.");
                } else if (TextUtils.isEmpty(this.A01) || !z) {
                    return new C05000Nw(this.A03, this.A02, this.A01, this.A00, this.A04, this.A05);
                } else {
                    throw new IllegalArgumentException("Negative text must not be set if device credential authentication is allowed.");
                }
            } else {
                StringBuilder sb = new StringBuilder("Authenticator combination is unsupported on API ");
                sb.append(Build.VERSION.SDK_INT);
                sb.append(": ");
                if (i != 15) {
                    str = i != 255 ? i != 32768 ? i != 32783 ? i != 33023 ? String.valueOf(i) : "BIOMETRIC_WEAK | DEVICE_CREDENTIAL" : "BIOMETRIC_STRONG | DEVICE_CREDENTIAL" : "DEVICE_CREDENTIAL" : "BIOMETRIC_WEAK";
                } else {
                    str = "BIOMETRIC_STRONG";
                }
                sb.append(str);
                throw new IllegalArgumentException(sb.toString());
            }
        } else {
            throw new IllegalArgumentException("Title must be set and non-empty.");
        }
    }
}
