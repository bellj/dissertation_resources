package X;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;

/* renamed from: X.5xR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129265xR {
    public C17050qB A00;
    public final Context A01;
    public final C14900mE A02;
    public final C18640sm A03;
    public final C14300lD A04;
    public final C18650sn A05;
    public final C18610sj A06;
    public final AnonymousClass60T A07;
    public final AbstractC14440lR A08;

    public C129265xR(Context context, C14900mE r2, C18640sm r3, C14300lD r4, C18650sn r5, C18610sj r6, AnonymousClass60T r7, AbstractC14440lR r8) {
        this.A01 = context;
        this.A02 = r2;
        this.A08 = r8;
        this.A04 = r4;
        this.A06 = r6;
        this.A03 = r3;
        this.A05 = r5;
        this.A07 = r7;
    }

    public void A00(AnonymousClass6B7 r19, C128015vQ r20, C14370lK r21, File file) {
        if (Build.VERSION.SDK_INT < 26) {
            this.A02.A06.execute(new Runnable() { // from class: X.6FW
                @Override // java.lang.Runnable
                public final void run() {
                    C128015vQ r0 = C128015vQ.this;
                    r0.A02.A2n(r0.A01);
                }
            });
            return;
        }
        try {
            File A00 = ((C32861cr) this.A00.A05.get()).A02.A00("enc");
            new FileOutputStream(A00).write(r19.A00.A9N(Files.readAllBytes(file.toPath()), C003501n.A0D(16)));
            AnonymousClass1KC A04 = this.A04.A04(AnonymousClass1K9.A00(Uri.fromFile(A00), null, new AnonymousClass1KA(SearchActionVerificationClientService.NOTIFICATION_ID, 100, 1600, 1600), new C14480lV(true, false, true), r21, null, null, 0, false, false, false, false), true);
            A04.A0U = "mms";
            A04.A03(new AbstractC14590lg(A04, this, r20) { // from class: X.6Eb
                public final /* synthetic */ AnonymousClass1KC A00;
                public final /* synthetic */ C129265xR A01;
                public final /* synthetic */ C128015vQ A02;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                    this.A02 = r3;
                }

                @Override // X.AbstractC14590lg
                public final void accept(Object obj) {
                    C129265xR r4 = this.A01;
                    AnonymousClass1KC r2 = this.A00;
                    C128015vQ r3 = this.A02;
                    C39871qg r1 = (C39871qg) r2.A08.A00();
                    if (r1 != null && !r1.A02.get()) {
                        r1.A01.delete();
                    }
                    C39721qR r22 = (C39721qR) r2.A0H.A00();
                    if (r22 == null || r22.A00 != 0) {
                        r4.A02.A06.execute(
                        /*  JADX ERROR: Method code generation error
                            jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0041: INVOKE  
                              (wrap: java.util.concurrent.Executor : 0x003a: IGET  (r1v2 java.util.concurrent.Executor A[REMOVE]) = (wrap: X.0mE : 0x0038: IGET  (r0v2 X.0mE A[REMOVE]) = (r4v0 'r4' X.5xR) X.5xR.A02 X.0mE) X.0mE.A06 java.util.concurrent.Executor)
                              (wrap: X.6FX : 0x003e: CONSTRUCTOR  (r0v3 X.6FX A[REMOVE]) = (r3v0 'r3' X.5vQ) call: X.6FX.<init>(X.5vQ):void type: CONSTRUCTOR)
                             type: INTERFACE call: java.util.concurrent.Executor.execute(java.lang.Runnable):void in method: X.6Eb.accept(java.lang.Object):void, file: classes4.dex
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                            	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                            	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                            	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                            	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                            	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                            	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                            	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                            	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                            	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                            	at java.util.ArrayList.forEach(ArrayList.java:1259)
                            	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                            	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                            Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x003e: CONSTRUCTOR  (r0v3 X.6FX A[REMOVE]) = (r3v0 'r3' X.5vQ) call: X.6FX.<init>(X.5vQ):void type: CONSTRUCTOR in method: X.6Eb.accept(java.lang.Object):void, file: classes4.dex
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                            	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                            	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                            	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                            	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                            	... 21 more
                            Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6FX, state: NOT_LOADED
                            	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                            	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                            	... 27 more
                            */
                        /*
                            this = this;
                            X.5xR r4 = r5.A01
                            X.1KC r2 = r5.A00
                            X.5vQ r3 = r5.A02
                            X.0lj r0 = r2.A08
                            java.lang.Object r1 = r0.A00()
                            X.1qg r1 = (X.C39871qg) r1
                            if (r1 == 0) goto L_0x001d
                            java.util.concurrent.atomic.AtomicBoolean r0 = r1.A02
                            boolean r0 = r0.get()
                            if (r0 != 0) goto L_0x001d
                            java.io.File r0 = r1.A01
                            r0.delete()
                        L_0x001d:
                            X.0lj r0 = r2.A0H
                            java.lang.Object r2 = r0.A00()
                            X.1qR r2 = (X.C39721qR) r2
                            if (r2 == 0) goto L_0x0038
                            int r0 = r2.A00
                            if (r0 != 0) goto L_0x0038
                            X.0mE r0 = r4.A02
                            java.util.concurrent.Executor r1 = r0.A06
                            X.6JA r0 = new X.6JA
                            r0.<init>(r2, r3)
                            r1.execute(r0)
                            return
                        L_0x0038:
                            X.0mE r0 = r4.A02
                            java.util.concurrent.Executor r1 = r0.A06
                            X.6FX r0 = new X.6FX
                            r0.<init>(r3)
                            r1.execute(r0)
                            return
                        */
                        throw new UnsupportedOperationException("Method not decompiled: X.C134296Eb.accept(java.lang.Object):void");
                    }
                }, null);
                this.A08.Ab2(new Runnable(A04, this) { // from class: X.6Ho
                    public final /* synthetic */ AnonymousClass1KC A00;
                    public final /* synthetic */ C129265xR A01;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        C129265xR r0 = this.A01;
                        r0.A04.A0D(this.A00, null);
                    }
                });
            } catch (Exception unused) {
                Log.e("PAY: PaymentsComplianceMediaUploadManager encryption failure");
                this.A02.A06.execute(new Runnable() { // from class: X.6FY
                    @Override // java.lang.Runnable
                    public final void run() {
                        C128015vQ r0 = C128015vQ.this;
                        r0.A02.A2n(r0.A01);
                    }
                });
            }
        }
    }
