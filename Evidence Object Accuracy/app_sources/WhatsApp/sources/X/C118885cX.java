package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiQrTabActivity;

/* renamed from: X.5cX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118885cX extends AnonymousClass077 {
    public final /* synthetic */ IndiaUpiQrTabActivity A00;

    public C118885cX(IndiaUpiQrTabActivity indiaUpiQrTabActivity) {
        this.A00 = indiaUpiQrTabActivity;
    }

    @Override // X.AnonymousClass077, X.AnonymousClass070
    public void ATP(int i, float f, int i2) {
        IndiaUpiQrTabActivity indiaUpiQrTabActivity = this.A00;
        boolean z = false;
        if (i == (!C28141Kv.A01(((ActivityC13830kP) indiaUpiQrTabActivity).A01)) || f != 0.0f) {
            z = true;
        }
        if (indiaUpiQrTabActivity.A0D != z) {
            indiaUpiQrTabActivity.A0D = z;
            if (z) {
                View currentFocus = indiaUpiQrTabActivity.getCurrentFocus();
                if (currentFocus != null) {
                    ((ActivityC13790kL) indiaUpiQrTabActivity).A0D.A01(currentFocus);
                }
                indiaUpiQrTabActivity.A2e();
            }
        }
    }

    @Override // X.AnonymousClass077, X.AnonymousClass070
    public void ATQ(int i) {
        IndiaUpiQrTabActivity indiaUpiQrTabActivity = this.A00;
        indiaUpiQrTabActivity.A0b();
        C117875at r3 = indiaUpiQrTabActivity.A08;
        int i2 = 0;
        while (true) {
            AnonymousClass4OE[] r1 = r3.A00;
            if (i2 >= r1.length) {
                break;
            }
            AnonymousClass4OE r0 = r1[i2];
            r0.A00.setSelected(C12960it.A1V(i2, i));
            i2++;
        }
        if (i == 0) {
            View currentFocus = indiaUpiQrTabActivity.getCurrentFocus();
            if (currentFocus != null) {
                ((ActivityC13790kL) indiaUpiQrTabActivity).A0D.A01(currentFocus);
            }
            if (!indiaUpiQrTabActivity.A0D) {
                indiaUpiQrTabActivity.A0D = true;
                indiaUpiQrTabActivity.A2e();
            }
            if (!((ActivityC13810kN) indiaUpiQrTabActivity).A07.A0B()) {
                ((ActivityC13810kN) indiaUpiQrTabActivity).A05.A07(R.string.no_internet_message, 1);
            }
        }
    }
}
