package X;

import android.os.SystemClock;
import android.view.View;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.2Gp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C48522Gp {
    public long A00 = SystemClock.elapsedRealtime();
    public boolean A01 = false;
    public final AbstractC14780m2 A02 = new AbstractC14780m2() { // from class: X.58f
        @Override // X.AbstractC14780m2
        public final boolean AM3() {
            C48522Gp r5 = C48522Gp.this;
            AnonymousClass009.A01();
            if (r5.A01) {
                return true;
            }
            r5.A01 = true;
            List<AbstractC48542Gr> list = r5.A03;
            for (AbstractC48542Gr r2 : list) {
                r2.AM4(r5.A00);
            }
            list.clear();
            return true;
        }
    };
    public final List A03 = new ArrayList();

    public C48522Gp(View view) {
        view.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver$OnPreDrawListenerC101974oO(view, this));
    }
}
