package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.3HJ  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3HJ {
    public static final ArrayList A0D;
    public static final ArrayList A0E;
    public static final ArrayList A0F;
    public static final ArrayList A0G;
    public final AnonymousClass1V8 A00;
    public final AnonymousClass3HF A01;
    public final String A02;
    public final String A03;
    public final String A04;
    public final String A05;
    public final String A06;
    public final String A07;
    public final String A08;
    public final String A09;
    public final String A0A;
    public final String A0B;
    public final List A0C;

    static {
        String[] strArr = new String[2];
        strArr[0] = "0";
        A0F = C12960it.A0m("1", strArr, 1);
        String[] strArr2 = new String[2];
        strArr2[0] = "0";
        A0E = C12960it.A0m("1", strArr2, 1);
        String[] strArr3 = new String[2];
        strArr3[0] = "0";
        A0D = C12960it.A0m("1", strArr3, 1);
        String[] strArr4 = new String[7];
        strArr4[0] = "ACTIVE";
        strArr4[1] = "EXTERNALLY_DISABLED";
        strArr4[2] = "HARD_BLOCKED";
        strArr4[3] = "INACTIVE";
        strArr4[4] = "INTEGRITY_BLOCKED";
        strArr4[5] = "PENDING";
        A0G = C12960it.A0m("SOFT_BLOCKED", strArr4, 6);
    }

    public AnonymousClass3HJ(AbstractC15710nm r12, AnonymousClass1V8 r13) {
        AnonymousClass1V8.A01(r13, "merchant");
        this.A0A = (String) AnonymousClass3JT.A04(null, r13, String.class, 1L, 100L, null, new String[]{"merchant-id"}, false);
        this.A02 = (String) AnonymousClass3JT.A03(null, r13, String.class, 1L, 200L, null, new String[]{"business-name"}, false);
        this.A08 = (String) AnonymousClass3JT.A03(null, r13, String.class, 1L, 200L, null, new String[]{"gateway-name"}, false);
        this.A0B = (String) AnonymousClass3JT.A03(null, r13, String.class, 1L, 20L, null, new String[]{"support-phone-number"}, false);
        this.A06 = (String) AnonymousClass3JT.A03(null, r13, String.class, 1L, 3000L, null, new String[]{"dashboard-url"}, false);
        this.A09 = (String) AnonymousClass3JT.A03(null, r13, String.class, 1L, 3000L, null, new String[]{"logo-uri"}, false);
        this.A05 = AnonymousClass3JT.A09(r13, A0F, new String[]{"can-sell"});
        this.A04 = AnonymousClass3JT.A09(r13, A0E, new String[]{"can-payout"});
        this.A03 = AnonymousClass3JT.A09(r13, A0D, new String[]{"can-add-payout"});
        this.A07 = AnonymousClass3JT.A09(r13, A0G, new String[]{"display-state"});
        this.A01 = (AnonymousClass3HF) AnonymousClass3JT.A02(r12, r13, 29);
        this.A0C = AnonymousClass3JT.A0A(r12, r13, new String[]{"payout"}, 28);
        this.A00 = r13;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || AnonymousClass3HJ.class != obj.getClass()) {
                return false;
            }
            AnonymousClass3HJ r5 = (AnonymousClass3HJ) obj;
            if (!this.A05.equals(r5.A05) || !this.A04.equals(r5.A04) || !this.A03.equals(r5.A03) || !this.A07.equals(r5.A07) || !this.A0A.equals(r5.A0A) || !C29941Vi.A00(this.A02, r5.A02) || !C29941Vi.A00(this.A08, r5.A08) || !C29941Vi.A00(this.A0B, r5.A0B) || !C29941Vi.A00(this.A06, r5.A06) || !C29941Vi.A00(this.A09, r5.A09) || !this.A0C.equals(r5.A0C) || !this.A01.equals(r5.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] objArr = new Object[12];
        objArr[0] = this.A05;
        objArr[1] = this.A04;
        objArr[2] = this.A03;
        objArr[3] = this.A07;
        objArr[4] = this.A0A;
        objArr[5] = this.A02;
        objArr[6] = this.A08;
        objArr[7] = this.A0B;
        objArr[8] = this.A06;
        objArr[9] = this.A09;
        objArr[10] = this.A0C;
        return C12980iv.A0B(this.A01, objArr, 11);
    }
}
