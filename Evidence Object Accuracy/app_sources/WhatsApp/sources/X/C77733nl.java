package X;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;

/* renamed from: X.3nl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77733nl extends AnonymousClass1U8 implements AbstractC116625We {
    public long A00 = 120000;
    public C472229q A01;
    public AnonymousClass5XO A02 = null;
    public Integer A03 = null;
    public Set A04 = C12970iu.A12();
    public final int A05;
    public final Context A06;
    public final Looper A07;
    public final C471729i A08;
    public final AbstractC77683ng A09;
    public final AnonymousClass4IP A0A = new AnonymousClass4IP();
    public final HandlerC79073q0 A0B;
    public final C93414a8 A0C;
    public final AnonymousClass3BW A0D;
    public final AbstractC115565Sb A0E;
    public final C98284iR A0F;
    public final ArrayList A0G;
    public final Map A0H;
    public final Map A0I;
    public final Queue A0J = new LinkedList();
    public final Lock A0K;
    public volatile boolean A0L;

    @Override // X.AnonymousClass1U8
    public final Context A02() {
        return this.A06;
    }

    @Override // X.AnonymousClass1U8
    public final Looper A03() {
        return this.A07;
    }

    public C77733nl(Context context, Looper looper, C471729i r11, AbstractC77683ng r12, AnonymousClass3BW r13, ArrayList arrayList, List list, List list2, Map map, Map map2, Lock lock) {
        C108434z3 r1 = new C108434z3(this);
        this.A0E = r1;
        this.A06 = context;
        this.A0K = lock;
        this.A0F = new C98284iR(looper, r1);
        this.A07 = looper;
        this.A0B = new HandlerC79073q0(looper, this);
        this.A08 = r11;
        this.A05 = -1;
        this.A0I = map;
        this.A0H = map2;
        this.A0G = arrayList;
        this.A0C = new C93414a8();
        for (Object obj : list) {
            C98284iR r5 = this.A0F;
            C13020j0.A01(obj);
            synchronized (r5.A03) {
                ArrayList arrayList2 = r5.A05;
                if (arrayList2.contains(obj)) {
                    String valueOf = String.valueOf(obj);
                    StringBuilder A0t = C12980iv.A0t(valueOf.length() + 62);
                    A0t.append("registerConnectionCallbacks(): listener ");
                    A0t.append(valueOf);
                    Log.w("GmsClientEvents", C12960it.A0d(" is already registered", A0t));
                } else {
                    arrayList2.add(obj);
                }
            }
            if (r5.A02.isConnected()) {
                C72463ee.A0R(r5.A01, obj);
            }
        }
        for (Object obj2 : list2) {
            C98284iR r0 = this.A0F;
            C13020j0.A01(obj2);
            synchronized (r0.A03) {
                ArrayList arrayList3 = r0.A06;
                if (arrayList3.contains(obj2)) {
                    String valueOf2 = String.valueOf(obj2);
                    StringBuilder A0t2 = C12980iv.A0t(valueOf2.length() + 67);
                    A0t2.append("registerConnectionFailedListener(): listener ");
                    A0t2.append(valueOf2);
                    Log.w("GmsClientEvents", C12960it.A0d(" is already registered", A0t2));
                } else {
                    arrayList3.add(obj2);
                }
            }
        }
        this.A0D = r13;
        this.A09 = r12;
    }

    public static int A00(Iterable iterable, boolean z) {
        Iterator it = iterable.iterator();
        boolean z2 = false;
        boolean z3 = false;
        while (it.hasNext()) {
            AbstractC72443eb r1 = (AbstractC72443eb) it.next();
            z2 |= r1.Aae();
            z3 |= r1.AZd();
        }
        if (z2) {
            return (!z3 || !z) ? 1 : 2;
        }
        return 3;
    }

    public static /* bridge */ /* synthetic */ void A01(C77733nl r2) {
        Lock lock = r2.A0K;
        lock.lock();
        try {
            if (r2.A0L) {
                r2.A0D();
            }
        } finally {
            lock.unlock();
        }
    }

    @Override // X.AnonymousClass1U8
    public final AbstractC72443eb A04(AnonymousClass4DN r3) {
        AbstractC72443eb r1 = (AbstractC72443eb) this.A0H.get(r3);
        C13020j0.A02(r1, "Appropriate Api was not requested.");
        return r1;
    }

    @Override // X.AnonymousClass1U8
    public final AnonymousClass1UI A05(AnonymousClass1UI r5) {
        AnonymousClass1UE r2 = r5.A01;
        boolean containsKey = this.A0H.containsKey(r5.A00);
        String str = r2.A02;
        StringBuilder A0t = C12980iv.A0t(C12970iu.A07(str) + 65);
        A0t.append("GoogleApiClient is not configured to use ");
        A0t.append(str);
        C13020j0.A03(C12960it.A0d(" required for this call.", A0t), containsKey);
        Lock lock = this.A0K;
        lock.lock();
        try {
            AnonymousClass5XO r0 = this.A02;
            if (r0 == null) {
                this.A0J.add(r5);
            } else {
                r0.AgV(r5);
            }
            return r5;
        } finally {
            lock.unlock();
        }
    }

    @Override // X.AnonymousClass1U8
    public final AnonymousClass1UI A06(AnonymousClass1UI r6) {
        AnonymousClass1UE r2 = r6.A01;
        boolean containsKey = this.A0H.containsKey(r6.A00);
        String str = r2.A02;
        StringBuilder A0t = C12980iv.A0t(C12970iu.A07(str) + 65);
        A0t.append("GoogleApiClient is not configured to use ");
        A0t.append(str);
        C13020j0.A03(C12960it.A0d(" required for this call.", A0t), containsKey);
        Lock lock = this.A0K;
        lock.lock();
        try {
            AnonymousClass5XO r1 = this.A02;
            if (r1 != null) {
                if (this.A0L) {
                    Queue queue = this.A0J;
                    queue.add(r6);
                    while (!queue.isEmpty()) {
                        AnonymousClass1UI r22 = (AnonymousClass1UI) queue.remove();
                        C93414a8 r12 = this.A0C;
                        r12.A01.add(r22);
                        r22.A0A.set(r12.A00);
                        r22.A09(Status.A07);
                    }
                } else {
                    r6 = r1.AgY(r6);
                }
                return r6;
            }
            throw C12960it.A0U("GoogleApiClient is not connected yet.");
        } finally {
            lock.unlock();
        }
    }

    @Override // X.AnonymousClass1U8
    public final void A07() {
        AnonymousClass5XO r0 = this.A02;
        if (r0 != null) {
            r0.Agg();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0068 A[Catch: all -> 0x0217, TryCatch #1 {all -> 0x021c, blocks: (B:3:0x0009, B:5:0x0010, B:7:0x0017, B:9:0x001b, B:10:0x002c, B:12:0x0032, B:13:0x0038, B:14:0x0039, B:15:0x003e, B:80:0x01c6, B:22:0x0055, B:24:0x0068, B:25:0x006e, B:27:0x0072, B:28:0x007a, B:30:0x0080, B:31:0x0091, B:34:0x0099, B:39:0x00a7, B:40:0x00c7, B:42:0x00ce, B:45:0x00e0, B:47:0x00ea, B:48:0x00ee, B:49:0x00f2, B:50:0x010a, B:52:0x0110, B:54:0x011e, B:55:0x0126, B:57:0x012c, B:58:0x0134, B:59:0x013a, B:60:0x013b, B:62:0x014a, B:64:0x0158, B:65:0x015c, B:67:0x0164, B:68:0x0167, B:69:0x016a, B:70:0x0170, B:73:0x0175, B:74:0x017b, B:75:0x017c, B:76:0x019e, B:77:0x01a4, B:78:0x01a5, B:79:0x01c3, B:100:0x01f2, B:101:0x0216), top: B:105:0x0009 }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0072 A[Catch: all -> 0x0217, TryCatch #1 {all -> 0x021c, blocks: (B:3:0x0009, B:5:0x0010, B:7:0x0017, B:9:0x001b, B:10:0x002c, B:12:0x0032, B:13:0x0038, B:14:0x0039, B:15:0x003e, B:80:0x01c6, B:22:0x0055, B:24:0x0068, B:25:0x006e, B:27:0x0072, B:28:0x007a, B:30:0x0080, B:31:0x0091, B:34:0x0099, B:39:0x00a7, B:40:0x00c7, B:42:0x00ce, B:45:0x00e0, B:47:0x00ea, B:48:0x00ee, B:49:0x00f2, B:50:0x010a, B:52:0x0110, B:54:0x011e, B:55:0x0126, B:57:0x012c, B:58:0x0134, B:59:0x013a, B:60:0x013b, B:62:0x014a, B:64:0x0158, B:65:0x015c, B:67:0x0164, B:68:0x0167, B:69:0x016a, B:70:0x0170, B:73:0x0175, B:74:0x017b, B:75:0x017c, B:76:0x019e, B:77:0x01a4, B:78:0x01a5, B:79:0x01c3, B:100:0x01f2, B:101:0x0216), top: B:105:0x0009 }] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0091 A[Catch: all -> 0x0217, TryCatch #1 {all -> 0x021c, blocks: (B:3:0x0009, B:5:0x0010, B:7:0x0017, B:9:0x001b, B:10:0x002c, B:12:0x0032, B:13:0x0038, B:14:0x0039, B:15:0x003e, B:80:0x01c6, B:22:0x0055, B:24:0x0068, B:25:0x006e, B:27:0x0072, B:28:0x007a, B:30:0x0080, B:31:0x0091, B:34:0x0099, B:39:0x00a7, B:40:0x00c7, B:42:0x00ce, B:45:0x00e0, B:47:0x00ea, B:48:0x00ee, B:49:0x00f2, B:50:0x010a, B:52:0x0110, B:54:0x011e, B:55:0x0126, B:57:0x012c, B:58:0x0134, B:59:0x013a, B:60:0x013b, B:62:0x014a, B:64:0x0158, B:65:0x015c, B:67:0x0164, B:68:0x0167, B:69:0x016a, B:70:0x0170, B:73:0x0175, B:74:0x017b, B:75:0x017c, B:76:0x019e, B:77:0x01a4, B:78:0x01a5, B:79:0x01c3, B:100:0x01f2, B:101:0x0216), top: B:105:0x0009 }] */
    @Override // X.AnonymousClass1U8
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A08() {
        /*
        // Method dump skipped, instructions count: 545
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C77733nl.A08():void");
    }

    @Override // X.AnonymousClass1U8
    public final void A09() {
        boolean z;
        Lock lock = this.A0K;
        lock.lock();
        try {
            Set set = this.A0C.A01;
            BasePendingResult[] basePendingResultArr = (BasePendingResult[]) set.toArray(new BasePendingResult[0]);
            for (BasePendingResult basePendingResult : basePendingResultArr) {
                basePendingResult.A0A.set(null);
                synchronized (basePendingResult.A06) {
                    if (((AnonymousClass1U8) basePendingResult.A07.get()) == null || !basePendingResult.A03) {
                        basePendingResult.A03();
                    }
                    z = basePendingResult.A02;
                }
                if (z) {
                    set.remove(basePendingResult);
                }
            }
            AnonymousClass5XO r0 = this.A02;
            if (r0 != null) {
                r0.Age();
            }
            Set<AnonymousClass4PH> set2 = this.A0A.A00;
            for (AnonymousClass4PH r1 : set2) {
                r1.A02 = null;
                r1.A01 = null;
            }
            set2.clear();
            Queue<AnonymousClass1UI> queue = this.A0J;
            for (AnonymousClass1UI r2 : queue) {
                r2.A0A.set(null);
                r2.A03();
            }
            queue.clear();
            if (this.A02 != null) {
                A0E();
                C98284iR r12 = this.A0F;
                r12.A08 = false;
                r12.A07.incrementAndGet();
            }
        } finally {
            lock.unlock();
        }
    }

    @Override // X.AnonymousClass1U8
    public final boolean A0A() {
        AnonymousClass5XO r0 = this.A02;
        return r0 != null && r0.Agh();
    }

    @Override // X.AnonymousClass1U8
    public final boolean A0B(AnonymousClass5QY r3) {
        AnonymousClass5XO r0 = this.A02;
        return r0 != null && r0.Agi(r3);
    }

    public final String A0C() {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        printWriter.append((CharSequence) "").append((CharSequence) "mContext=").println(this.A06);
        printWriter.append((CharSequence) "").append((CharSequence) "mResuming=").print(this.A0L);
        printWriter.append((CharSequence) " mWorkQueue.size()=").print(this.A0J.size());
        printWriter.append((CharSequence) " mUnconsumedApiCalls.size()=").println(this.A0C.A01.size());
        AnonymousClass5XO r0 = this.A02;
        if (r0 != null) {
            r0.Agf("", null, printWriter, null);
        }
        return stringWriter.toString();
    }

    public final void A0D() {
        this.A0F.A08 = true;
        AnonymousClass5XO r0 = this.A02;
        C13020j0.A01(r0);
        r0.Agd();
    }

    public final boolean A0E() {
        boolean z = false;
        if (this.A0L) {
            this.A0L = false;
            HandlerC79073q0 r1 = this.A0B;
            r1.removeMessages(2);
            z = true;
            r1.removeMessages(1);
            C472229q r0 = this.A01;
            if (r0 != null) {
                r0.A00();
                this.A01 = null;
            }
        }
        return z;
    }

    @Override // X.AbstractC116625We
    public final void AgN(C56492ky r9) {
        AtomicInteger atomicInteger;
        Context context = this.A06;
        int i = r9.A01;
        if (i != 18 && (i != 1 || !C472329r.A03(context))) {
            A0E();
        }
        if (!this.A0L) {
            C98284iR r7 = this.A0F;
            Handler handler = r7.A01;
            if (Looper.myLooper() == handler.getLooper()) {
                handler.removeMessages(1);
                synchronized (r7.A03) {
                    ArrayList arrayList = r7.A06;
                    ArrayList A0x = C12980iv.A0x(arrayList);
                    atomicInteger = r7.A07;
                    int i2 = atomicInteger.get();
                    Iterator it = A0x.iterator();
                    while (it.hasNext()) {
                        AbstractC15000mO r1 = (AbstractC15000mO) it.next();
                        if (!r7.A08 || atomicInteger.get() != i2) {
                            break;
                        } else if (arrayList.contains(r1)) {
                            r1.onConnectionFailed(r9);
                        }
                    }
                }
                r7.A08 = false;
                atomicInteger.incrementAndGet();
                return;
            }
            throw C12960it.A0U("onConnectionFailure must only be called on the Handler thread");
        }
    }

    @Override // X.AbstractC116625We
    public final void AgP(Bundle bundle) {
        while (true) {
            Queue queue = this.A0J;
            if (queue.isEmpty()) {
                break;
            }
            A06((AnonymousClass1UI) queue.remove());
        }
        C98284iR r6 = this.A0F;
        Handler handler = r6.A01;
        if (Looper.myLooper() == handler.getLooper()) {
            synchronized (r6.A03) {
                if (!r6.A00) {
                    handler.removeMessages(1);
                    r6.A00 = true;
                    ArrayList arrayList = r6.A04;
                    if (arrayList.isEmpty()) {
                        ArrayList A0x = C12980iv.A0x(r6.A05);
                        AtomicInteger atomicInteger = r6.A07;
                        int i = atomicInteger.get();
                        Iterator it = A0x.iterator();
                        while (it.hasNext()) {
                            AbstractC14980mM r1 = (AbstractC14980mM) it.next();
                            if (!r6.A08 || !r6.A02.isConnected() || atomicInteger.get() != i) {
                                break;
                            } else if (!arrayList.contains(r1)) {
                                r1.onConnected(bundle);
                            }
                        }
                        arrayList.clear();
                        r6.A00 = false;
                    } else {
                        throw C72463ee.A0D();
                    }
                } else {
                    throw C72463ee.A0D();
                }
            }
            return;
        }
        throw C12960it.A0U("onConnectionSuccess must only be called on the Handler thread");
    }

    @Override // X.AbstractC116625We
    public final void AgS(int i, boolean z) {
        AtomicInteger atomicInteger;
        if (i == 1) {
            if (!this.A0L) {
                this.A0L = true;
                if (this.A01 == null) {
                    try {
                        this.A01 = this.A08.A02(this.A06.getApplicationContext(), new C77863nz(this));
                    } catch (SecurityException unused) {
                    }
                }
                HandlerC79073q0 r3 = this.A0B;
                r3.sendMessageDelayed(r3.obtainMessage(1), this.A00);
                r3.sendMessageDelayed(r3.obtainMessage(2), 5000);
            }
            i = 1;
        }
        for (BasePendingResult basePendingResult : (BasePendingResult[]) this.A0C.A01.toArray(new BasePendingResult[0])) {
            basePendingResult.A06(C93414a8.A02);
        }
        C98284iR r6 = this.A0F;
        Handler handler = r6.A01;
        if (Looper.myLooper() == handler.getLooper()) {
            handler.removeMessages(1);
            synchronized (r6.A03) {
                r6.A00 = true;
                ArrayList arrayList = r6.A05;
                ArrayList A0x = C12980iv.A0x(arrayList);
                atomicInteger = r6.A07;
                int i2 = atomicInteger.get();
                Iterator it = A0x.iterator();
                while (it.hasNext()) {
                    AbstractC14980mM r1 = (AbstractC14980mM) it.next();
                    if (!r6.A08 || atomicInteger.get() != i2) {
                        break;
                    } else if (arrayList.contains(r1)) {
                        r1.onConnectionSuspended(i);
                    }
                }
                r6.A04.clear();
                r6.A00 = false;
            }
            r6.A08 = false;
            atomicInteger.incrementAndGet();
            if (i == 2) {
                A0D();
                return;
            }
            return;
        }
        throw C12960it.A0U("onUnintentionalDisconnection must only be called on the Handler thread");
    }
}
