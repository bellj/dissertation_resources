package X;

import android.content.SharedPreferences;

/* renamed from: X.0yQ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass0yQ {
    public SharedPreferences A00;
    public final C16630pM A01;

    public AnonymousClass0yQ(C16630pM r1) {
        this.A01 = r1;
    }

    public final synchronized SharedPreferences A00() {
        SharedPreferences sharedPreferences;
        sharedPreferences = this.A00;
        if (sharedPreferences == null) {
            sharedPreferences = this.A01.A01("time_spent_prefs");
            this.A00 = sharedPreferences;
        }
        return sharedPreferences;
    }
}
