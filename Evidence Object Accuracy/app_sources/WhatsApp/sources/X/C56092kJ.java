package X;

import android.net.Uri;
import android.text.TextUtils;
import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/* renamed from: X.2kJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56092kJ extends AbstractC67743Ss {
    public long A00;
    public Uri A01;
    public RandomAccessFile A02;
    public boolean A03;

    public C56092kJ() {
        super(false);
    }

    @Override // X.AnonymousClass2BW
    public Uri AHS() {
        return this.A01;
    }

    @Override // X.AnonymousClass2BW
    public long AYZ(AnonymousClass3H3 r8) {
        try {
            Uri uri = r8.A04;
            this.A01 = uri;
            A01();
            try {
                RandomAccessFile randomAccessFile = new RandomAccessFile(uri.getPath(), "r");
                this.A02 = randomAccessFile;
                long j = r8.A03;
                randomAccessFile.seek(j);
                long j2 = r8.A02;
                if (j2 == -1) {
                    j2 = this.A02.length() - j;
                }
                this.A00 = j2;
                if (j2 >= 0) {
                    this.A03 = true;
                    A03(r8);
                    return this.A00;
                }
                throw new EOFException();
            } catch (FileNotFoundException e) {
                if (!TextUtils.isEmpty(uri.getQuery()) || !TextUtils.isEmpty(uri.getFragment())) {
                    throw new C868248z(e, String.format("uri has query and/or fragment, which are not supported. Did you call Uri.parse() on a string containing '?' or '#'? Use Uri.fromFile(new File(path)) to avoid this. path=%s,query=%s,fragment=%s", uri.getPath(), uri.getQuery(), uri.getFragment()));
                }
                throw new C868248z(e);
            }
        } catch (IOException e2) {
            throw new C868248z(e2);
        }
    }

    @Override // X.AnonymousClass2BW
    public void close() {
        this.A01 = null;
        try {
            try {
                RandomAccessFile randomAccessFile = this.A02;
                if (randomAccessFile != null) {
                    randomAccessFile.close();
                }
            } catch (IOException e) {
                throw new C868248z(e);
            }
        } finally {
            this.A02 = null;
            if (this.A03) {
                this.A03 = false;
                A00();
            }
        }
    }

    @Override // X.AnonymousClass2BY
    public int read(byte[] bArr, int i, int i2) {
        if (i2 == 0) {
            return 0;
        }
        long j = this.A00;
        if (j == 0) {
            return -1;
        }
        try {
            int read = this.A02.read(bArr, i, (int) Math.min(j, (long) i2));
            if (read <= 0) {
                return read;
            }
            this.A00 -= (long) read;
            A02(read);
            return read;
        } catch (IOException e) {
            throw new C868248z(e);
        }
    }
}
