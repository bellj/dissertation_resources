package X;

import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5wy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128975wy {
    public long A00;
    public long A01;
    public String A02;
    public String A03;

    public C128975wy(String str, String str2, long j, long j2) {
        this.A03 = str;
        this.A02 = str2;
        this.A01 = j;
        this.A00 = j2;
    }

    public String toString() {
        JSONObject A0a = C117295Zj.A0a();
        try {
            A0a.put("shard-key", this.A03);
            A0a.put("entry-key", this.A02);
            A0a.put("expiration-time", this.A01);
            A0a.put("create-time", this.A00);
        } catch (JSONException unused) {
            Log.e("BkCacheSaveOnDiskHelper:BkCacheValueHelper/toJson threw exception");
        }
        return A0a.toString();
    }
}
