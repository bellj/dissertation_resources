package X;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.whatsapp.status.playback.MyStatusesActivity;
import java.util.Collections;

/* renamed from: X.36T  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass36T extends AbstractC102384p3 {
    public final /* synthetic */ MyStatusesActivity A00;

    public AnonymousClass36T(MyStatusesActivity myStatusesActivity) {
        this.A00 = myStatusesActivity;
    }

    @Override // X.AbstractC102384p3
    public void A00(AdapterView adapterView, View view, int i, long j) {
        MyStatusesActivity myStatusesActivity = this.A00;
        if (myStatusesActivity.A16.isEmpty()) {
            AbstractC15340mz r2 = (AbstractC15340mz) myStatusesActivity.A0l.A00.get(i);
            AbstractC009504t r0 = myStatusesActivity.A01;
            if (r0 != null) {
                r0.A05();
            }
            Intent A0F = C14960mK.A0F(myStatusesActivity, r2.A0B(), Boolean.FALSE);
            C38211ni.A00(A0F, r2.A0z);
            myStatusesActivity.startActivity(A0F);
            C18470sV r02 = myStatusesActivity.A0N;
            r02.A09();
            Object obj = r02.A07.get(C29831Uv.A00);
            C15570nT r03 = ((ActivityC13790kL) myStatusesActivity).A01;
            r03.A08();
            C27631Ih r22 = r03.A05;
            if (obj != null && r22 != null) {
                myStatusesActivity.A0j.A04(r22, null, null, null, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyMap());
            }
        }
    }

    @Override // X.AbstractC102384p3, android.widget.AdapterView.OnItemClickListener
    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        MyStatusesActivity myStatusesActivity = this.A00;
        if (myStatusesActivity.A16.isEmpty()) {
            super.onItemClick(adapterView, view, i, j);
        } else {
            myStatusesActivity.A2i(view, (AbstractC15340mz) myStatusesActivity.A0l.A00.get(i));
        }
    }
}
