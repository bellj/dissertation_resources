package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4l1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C99884l1 implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new C30841Za(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C30841Za[i];
    }
}
