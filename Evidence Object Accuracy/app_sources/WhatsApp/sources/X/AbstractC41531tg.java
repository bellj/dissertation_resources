package X;

import android.graphics.Bitmap;
import android.view.View;

/* renamed from: X.1tg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC41531tg {
    public Bitmap A00() {
        if (this instanceof C41551ti) {
            return AnonymousClass19O.A01(((C41551ti) this).A00, 100, false, false);
        }
        C41541th r0 = (C41541th) this;
        return AnonymousClass19O.A01(r0.A02, r0.A00, true, r0.A07);
    }

    public void A01() {
        if (this instanceof C41541th) {
            C41541th r0 = (C41541th) this;
            AnonymousClass19O r11 = r0.A04;
            AbstractC15340mz r3 = r0.A02;
            View view = r0.A01;
            AbstractC41521tf r5 = r0.A03;
            Object obj = r0.A05;
            r11.A04.A01(view, r3, new C41561tj(view, r3, r5, r11, obj), r5, obj, r0.A06);
        }
    }
}
