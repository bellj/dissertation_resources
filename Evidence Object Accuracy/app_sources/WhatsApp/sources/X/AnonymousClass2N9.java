package X;

import android.os.Bundle;
import android.os.Message;
import com.whatsapp.util.Log;

/* renamed from: X.2N9  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2N9 extends AbstractC35941j2 {
    public final /* synthetic */ AnonymousClass2L8 A00;

    public AnonymousClass2N9(AnonymousClass2L8 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC35941j2
    public void A01(AnonymousClass1V8 r7) {
        String A0I;
        AnonymousClass1V8 A0E = r7.A0E("error");
        if (A0E != null) {
            String A0I2 = A0E.A0I("code", null);
            AnonymousClass1V8 A0E2 = A0E.A0E("violation");
            if (A0E2 != null) {
                A0I = A0E2.A0I("reason", null);
            } else {
                A0I = A0E.A0I("text", null);
            }
            C450720b r2 = this.A00.A0H;
            int parseInt = Integer.parseInt(A0I2);
            StringBuilder sb = new StringBuilder("xmpp/reader/on-set-biz-vname-cert-error code: ");
            sb.append(parseInt);
            sb.append(", reason: ");
            sb.append(A0I);
            Log.w(sb.toString());
            AbstractC450820c r3 = r2.A00;
            Message obtain = Message.obtain(null, 0, 112, 0);
            Bundle data = obtain.getData();
            data.putInt("errorCode", parseInt);
            data.putString("errorReason", A0I);
            r3.AYY(obtain);
        }
    }
}
