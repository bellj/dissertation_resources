package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;

/* renamed from: X.3oz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78483oz extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C99194ju();
    public final String A00;
    public final List A01;

    public C78483oz(String str, List list) {
        this.A00 = str;
        this.A01 = list;
        C13020j0.A01(str);
        C13020j0.A01(list);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001a, code lost:
        if (r1.equals(r0) != false) goto L_0x001c;
     */
    @Override // java.lang.Object
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean equals(java.lang.Object r5) {
        /*
            r4 = this;
            r3 = 1
            if (r4 == r5) goto L_0x002f
            r2 = 0
            if (r5 == 0) goto L_0x0028
            java.lang.Class<X.3oz> r1 = X.C78483oz.class
            java.lang.Class r0 = r5.getClass()
            if (r1 != r0) goto L_0x0028
            X.3oz r5 = (X.C78483oz) r5
            java.lang.String r1 = r4.A00
            java.lang.String r0 = r5.A00
            if (r1 == 0) goto L_0x0029
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0028
        L_0x001c:
            java.util.List r1 = r4.A01
            java.util.List r0 = r5.A01
            if (r1 == 0) goto L_0x002c
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x002f
        L_0x0028:
            return r2
        L_0x0029:
            if (r0 == 0) goto L_0x001c
            return r2
        L_0x002c:
            if (r0 == 0) goto L_0x002f
            return r2
        L_0x002f:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C78483oz.equals(java.lang.Object):boolean");
    }

    @Override // java.lang.Object
    public final int hashCode() {
        int i = 0;
        int A0E = (C72453ed.A0E(this.A00) + 31) * 31;
        List list = this.A01;
        if (list != null) {
            i = list.hashCode();
        }
        return A0E + i;
    }

    @Override // java.lang.Object
    public final String toString() {
        String str = this.A00;
        String valueOf = String.valueOf(this.A01);
        StringBuilder A0t = C12980iv.A0t(C12970iu.A07(str) + 18 + valueOf.length());
        A0t.append("CapabilityInfo{");
        A0t.append(str);
        A0t.append(", ");
        A0t.append(valueOf);
        return C12960it.A0d("}", A0t);
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A0F(parcel, this.A01, 3, C95654e8.A0K(parcel, this.A00));
        C95654e8.A06(parcel, A00);
    }
}
