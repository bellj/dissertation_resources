package X;

import android.content.Context;
import com.whatsapp.acceptinvitelink.AcceptInviteLinkActivity;

/* renamed from: X.4pf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C102764pf implements AbstractC009204q {
    public final /* synthetic */ AcceptInviteLinkActivity A00;

    public C102764pf(AcceptInviteLinkActivity acceptInviteLinkActivity) {
        this.A00 = acceptInviteLinkActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
