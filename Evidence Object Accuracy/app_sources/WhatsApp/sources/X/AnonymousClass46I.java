package X;

import android.view.KeyEvent;
import android.widget.EditText;
import com.whatsapp.R;

/* renamed from: X.46I  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass46I extends AbstractC69143Yc {
    public AnonymousClass46I() {
        super(R.drawable.ic_key_comma);
    }

    @Override // X.AnonymousClass5W6
    public void AUC(EditText editText) {
        editText.dispatchKeyEvent(new KeyEvent(0, 0, 0, 159, 0));
        editText.dispatchKeyEvent(new KeyEvent(0, 0, 1, 159, 0));
    }
}
