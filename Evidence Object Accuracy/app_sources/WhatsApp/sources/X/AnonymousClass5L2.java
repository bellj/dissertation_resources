package X;

/* renamed from: X.5L2  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass5L2 extends C10710f4 implements AnonymousClass5WO, AbstractC02760Dw, AnonymousClass5VF {
    public final AnonymousClass5X4 A00;

    public AnonymousClass5L2(AnonymousClass5X4 r2) {
        A0Y((AbstractC02760Dw) r2.get(AbstractC02760Dw.A00));
        this.A00 = r2.plus(this);
    }

    @Override // X.C10710f4
    public String A0F() {
        return C16700pc.A08(C12980iv.A0r(this), " was cancelled");
    }

    @Override // X.C10710f4
    public String A0G() {
        return super.A0G();
    }

    @Override // X.C10710f4
    public final void A0T(Throwable th) {
        C88234Eu.A00(this.A00, th);
    }

    public final void A0m(Object obj, AnonymousClass5ZQ r14) {
        AnonymousClass5WO r0;
        try {
            if (r14 instanceof AbstractC112665Eg) {
                r0 = ((AbstractC112665Eg) r14).A04(obj, this);
            } else {
                AnonymousClass5X4 ABi = ABi();
                if (ABi == C112775Er.A00) {
                    r0 = new C113695Ip(obj, this, r14);
                } else {
                    r0 = new C113655Il(obj, this, ABi, r14);
                }
            }
            AnonymousClass5WO A00 = C93024Yr.A00(r0);
            AnonymousClass1WZ r7 = AnonymousClass1WZ.A00;
            if (A00 instanceof C114205Kp) {
                C114205Kp r5 = (C114205Kp) A00;
                Object obj2 = r7;
                Throwable A002 = AnonymousClass5BU.A00(r7);
                if (A002 != null) {
                    obj2 = new C94894ci(A002, false);
                }
                AbstractC10990fX r1 = r5.A03;
                AnonymousClass5WO r6 = r5.A02;
                if (r1.A03(r6.ABi())) {
                    r5.A00 = obj2;
                    ((AnonymousClass5LK) r5).A00 = 1;
                    r1.A04(r5, r6.ABi());
                    return;
                }
                AbstractC114165Kl A11 = C72453ed.A11();
                long j = A11.A00;
                if (j >= 4294967296L) {
                    r5.A00 = obj2;
                    ((AnonymousClass5LK) r5).A00 = 1;
                    A11.A08(r5);
                    return;
                }
                A11.A00 = j + 4294967296L;
                AbstractC02760Dw r12 = (AbstractC02760Dw) r6.ABi().get(AbstractC02760Dw.A00);
                if (r12 == null || r12.AJD()) {
                    Object obj3 = r5.A01;
                    AnonymousClass5X4 ABi2 = r6.ABi();
                    Object A003 = C94704cP.A00(obj3, ABi2);
                    if (A003 != C94704cP.A03) {
                        C88224Et.A00(r6, ABi2);
                    }
                    r6.Aas(r7);
                    C94704cP.A01(A003, ABi2);
                } else {
                    r5.Aas(new AnonymousClass5BR(r12.ABF()));
                }
                do {
                } while (A11.A0A());
                A11.A06();
                return;
            }
            A00.Aas(r7);
        } catch (Throwable th) {
            Aas(new AnonymousClass5BR(th));
            throw th;
        }
    }

    @Override // X.AnonymousClass5WO
    public final AnonymousClass5X4 ABi() {
        return this.A00;
    }

    @Override // X.AnonymousClass5WO
    public final void Aas(Object obj) {
        Throwable A00 = AnonymousClass5BU.A00(obj);
        if (A00 != null) {
            obj = new C94894ci(A00, false);
        }
        A0P(obj);
    }
}
