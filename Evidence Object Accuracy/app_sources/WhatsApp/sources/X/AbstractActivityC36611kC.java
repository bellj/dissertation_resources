package X;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.URLSpan;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.IDxObserverShape4S0100000_2_I1;
import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;
import com.facebook.redex.ViewOnClickCListenerShape1S0100000_I0_1;
import com.whatsapp.EmptyTellAFriendView;
import com.whatsapp.Me;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaImageButton;
import com.whatsapp.blocklist.UnblockDialogFragment;
import com.whatsapp.calling.callhistory.group.GroupCallParticipantPicker;
import com.whatsapp.calling.callhistory.group.GroupCallParticipantPickerSheet;
import com.whatsapp.community.LinkExistingGroups;
import com.whatsapp.components.FloatingActionButton;
import com.whatsapp.contact.picker.AddGroupParticipantsSelector;
import com.whatsapp.contact.picker.ContactsAttachmentSelector;
import com.whatsapp.contact.picker.ListMembersSelector;
import com.whatsapp.conversation.EditBroadcastRecipientsSelector;
import com.whatsapp.filter.SmoothScrollLinearLayoutManager;
import com.whatsapp.group.EditGroupAdminsSelector;
import com.whatsapp.group.GroupMembersSelector;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.registration.NotifyContactsSelector;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape14S0100000_I0_1;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.1kC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC36611kC extends AbstractActivityC36621kD {
    public int A00;
    public int A01;
    public AnimatorSet A02;
    public ValueAnimator A03;
    public MenuItem A04;
    public View A05;
    public View A06;
    public ViewGroup A07;
    public ListView A08;
    public RecyclerView A09;
    public C48232Fc A0A;
    public AnonymousClass1AR A0B;
    public C16170oZ A0C;
    public WaImageButton A0D;
    public WaImageButton A0E;
    public C238013b A0F;
    public C22330yu A0G;
    public FloatingActionButton A0H;
    public AnonymousClass116 A0I;
    public C15550nR A0J;
    public AnonymousClass10S A0K;
    public C15610nY A0L;
    public AnonymousClass1J1 A0M;
    public C21270x9 A0N;
    public AnonymousClass36m A0O;
    public C36631kE A0P;
    public C624737j A0Q;
    public AnonymousClass118 A0R;
    public AnonymousClass018 A0S;
    public C244215l A0T;
    public AnonymousClass12F A0U;
    public String A0V;
    public ArrayList A0W;
    public List A0X = new ArrayList();
    public List A0Y;
    public boolean A0Z = true;
    public boolean A0a;
    public final AnonymousClass2Dn A0b = new AnonymousClass41T(this);
    public final C27131Gd A0c = new C36491jy(this);
    public final AnonymousClass1lP A0d = new AnonymousClass1lP(this);
    public final AbstractC33331dp A0e = new AnonymousClass44Z(this);
    public final ArrayList A0f = new ArrayList();
    public final List A0g = new ArrayList();

    public int A2h() {
        return 0;
    }

    public boolean A37() {
        return false;
    }

    public int A2g() {
        if (this instanceof NotifyContactsSelector) {
            return R.string.change_number_notification;
        }
        if (this instanceof GroupMembersSelector) {
            return R.string.new_group;
        }
        if (this instanceof EditGroupAdminsSelector) {
            return R.string.edit_group_admins;
        }
        if (this instanceof EditBroadcastRecipientsSelector) {
            return R.string.edit_broadcast_recipients;
        }
        if (this instanceof ListMembersSelector) {
            return R.string.new_list;
        }
        if (this instanceof ContactsAttachmentSelector) {
            return R.string.contacts_to_send;
        }
        if (this instanceof AddGroupParticipantsSelector) {
            return R.string.add_paticipants;
        }
        if (this instanceof LinkExistingGroups) {
            return R.string.link_existing_groups;
        }
        if (!(((GroupCallParticipantPicker) this) instanceof GroupCallParticipantPickerSheet)) {
            return R.string.new_group_call;
        }
        return R.string.menuitem_new_call;
    }

    public int A2i() {
        if (this instanceof NotifyContactsSelector) {
            return R.plurals.notify_contacts_change_number_reach_limit;
        }
        if ((this instanceof GroupMembersSelector) || (this instanceof EditGroupAdminsSelector)) {
            return R.plurals.groupchat_reach_limit;
        }
        if ((this instanceof EditBroadcastRecipientsSelector) || (this instanceof ListMembersSelector)) {
            return R.plurals.broadcast_reach_limit;
        }
        if (this instanceof ContactsAttachmentSelector) {
            return R.plurals.contact_array_message_reach_limit;
        }
        if (this instanceof AddGroupParticipantsSelector) {
            return R.plurals.groupchat_reach_limit;
        }
        if (!(this instanceof LinkExistingGroups)) {
            return R.plurals.groupcall_reach_limit;
        }
        return R.plurals.link_group_max_limit;
    }

    public int A2j() {
        C14850m9 r1;
        int i;
        if (this instanceof NotifyContactsSelector) {
            return Integer.MAX_VALUE;
        }
        if (this instanceof GroupMembersSelector) {
            r1 = ((GroupMembersSelector) this).A01.A0B;
            i = 1304;
        } else if (this instanceof EditGroupAdminsSelector) {
            EditGroupAdminsSelector editGroupAdminsSelector = (EditGroupAdminsSelector) this;
            int size = editGroupAdminsSelector.A0X.size();
            if (size != 0) {
                return Math.min(editGroupAdminsSelector.A00.A0B.A02(1304) - 1, size);
            }
            return Integer.MAX_VALUE;
        } else if ((this instanceof EditBroadcastRecipientsSelector) || (this instanceof ListMembersSelector)) {
            int A02 = ((ActivityC13810kN) this).A06.A02(AbstractC15460nI.A1I);
            if (A02 == 0) {
                return Integer.MAX_VALUE;
            }
            return A02;
        } else if (this instanceof ContactsAttachmentSelector) {
            return 257;
        } else {
            if (this instanceof AddGroupParticipantsSelector) {
                AddGroupParticipantsSelector addGroupParticipantsSelector = (AddGroupParticipantsSelector) this;
                return (addGroupParticipantsSelector.A00.A0B.A02(1304) - 1) - addGroupParticipantsSelector.A06.size();
            } else if (this instanceof LinkExistingGroups) {
                return getIntent().getIntExtra("max_groups_allowed_to_link", Integer.MAX_VALUE);
            } else {
                r1 = ((ActivityC13810kN) this).A0C;
                i = 862;
            }
        }
        return r1.A02(i) - 1;
    }

    public int A2k() {
        if (this instanceof NotifyContactsSelector) {
            return 0;
        }
        if (this instanceof GroupMembersSelector) {
            return 1;
        }
        if (this instanceof EditGroupAdminsSelector) {
            return 0;
        }
        if ((this instanceof EditBroadcastRecipientsSelector) || (this instanceof ListMembersSelector)) {
            return 2;
        }
        return ((this instanceof ContactsAttachmentSelector) || (this instanceof AddGroupParticipantsSelector) || !(this instanceof LinkExistingGroups)) ? 1 : 0;
    }

    public int A2l() {
        if (this instanceof NotifyContactsSelector) {
            return R.string.done;
        }
        if (this instanceof GroupMembersSelector) {
            return R.string.next;
        }
        if ((this instanceof EditGroupAdminsSelector) || (this instanceof EditBroadcastRecipientsSelector)) {
            return R.string.done;
        }
        if (this instanceof ListMembersSelector) {
            return R.string.create;
        }
        if (this instanceof ContactsAttachmentSelector) {
            return R.string.next;
        }
        if (this instanceof AddGroupParticipantsSelector) {
            return R.string.done;
        }
        if (!(this instanceof LinkExistingGroups)) {
            return 0;
        }
        return R.string.next;
    }

    public Drawable A2m() {
        if (!(this instanceof NotifyContactsSelector)) {
            if (!(this instanceof GroupMembersSelector)) {
                if (!(this instanceof EditGroupAdminsSelector) && !(this instanceof EditBroadcastRecipientsSelector) && !(this instanceof ListMembersSelector)) {
                    if (!(this instanceof ContactsAttachmentSelector)) {
                        if (!(this instanceof AddGroupParticipantsSelector)) {
                            if (!(this instanceof LinkExistingGroups)) {
                                return null;
                            }
                        }
                    }
                }
            }
            return new AnonymousClass2GF(AnonymousClass00T.A04(this, R.drawable.ic_fab_next), this.A0S);
        }
        return AnonymousClass00T.A04(this, R.drawable.ic_fab_check);
    }

    public View A2n() {
        if (!(this instanceof LinkExistingGroups)) {
            return null;
        }
        View inflate = getLayoutInflater().inflate(R.layout.link_existing_groups_picker_header, (ViewGroup) A2e(), false);
        TextView textView = (TextView) AnonymousClass028.A0D(inflate, R.id.link_existing_groups_picker_header_title);
        C27531Hw.A06(textView);
        textView.setText(R.string.link_group_title);
        return inflate;
    }

    public View A2o() {
        if (this instanceof AddGroupParticipantsSelector) {
            AddGroupParticipantsSelector addGroupParticipantsSelector = (AddGroupParticipantsSelector) this;
            C15580nU r1 = addGroupParticipantsSelector.A02;
            if (r1 == null || !addGroupParticipantsSelector.A00.A0D(r1) || !((ActivityC13810kN) addGroupParticipantsSelector).A0C.A07(1863)) {
                return null;
            }
            View inflate = addGroupParticipantsSelector.getLayoutInflater().inflate(R.layout.multiple_contact_picker_warning_layout, (ViewGroup) null);
            TextEmojiLabel textEmojiLabel = (TextEmojiLabel) AnonymousClass028.A0D(inflate, R.id.multiple_contact_picker_warning_text);
            addGroupParticipantsSelector.A39(textEmojiLabel, addGroupParticipantsSelector.A02);
            textEmojiLabel.setMovementMethod(new C52162aM());
            return inflate;
        } else if (this instanceof LinkExistingGroups) {
            LinkExistingGroups linkExistingGroups = (LinkExistingGroups) this;
            View view = linkExistingGroups.A00;
            if (view != null) {
                return view;
            }
            View inflate2 = linkExistingGroups.getLayoutInflater().inflate(R.layout.multiple_contact_picker_warning_layout, (ViewGroup) null);
            linkExistingGroups.A00 = inflate2;
            ((TextView) AnonymousClass028.A0D(inflate2, R.id.multiple_contact_picker_warning_text)).setText(R.string.link_existing_groups_details);
            return linkExistingGroups.A00;
        } else if (!(this instanceof GroupCallParticipantPicker)) {
            return null;
        } else {
            GroupCallParticipantPicker groupCallParticipantPicker = (GroupCallParticipantPicker) this;
            if (groupCallParticipantPicker instanceof GroupCallParticipantPickerSheet) {
                return null;
            }
            View inflate3 = groupCallParticipantPicker.getLayoutInflater().inflate(R.layout.multiple_contact_picker_warning_layout, (ViewGroup) null);
            TextView textView = (TextView) inflate3.findViewById(R.id.multiple_contact_picker_warning_text);
            Spanned fromHtml = Html.fromHtml(groupCallParticipantPicker.A0S.A0I(new Object[]{Integer.valueOf(groupCallParticipantPicker.A2j())}, R.plurals.voip_joinable_new_group_call_warning, (long) groupCallParticipantPicker.A2j()));
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(fromHtml);
            URLSpan[] uRLSpanArr = (URLSpan[]) fromHtml.getSpans(0, fromHtml.length(), URLSpan.class);
            if (uRLSpanArr != null) {
                for (URLSpan uRLSpan : uRLSpanArr) {
                    if ("learn_more_link".equals(uRLSpan.getURL())) {
                        Log.i("GroupCallParticipantPicker/getCustomWarningLayout/learn_more_link link found");
                        int spanStart = spannableStringBuilder.getSpanStart(uRLSpan);
                        int spanEnd = spannableStringBuilder.getSpanEnd(uRLSpan);
                        int spanFlags = spannableStringBuilder.getSpanFlags(uRLSpan);
                        spannableStringBuilder.removeSpan(uRLSpan);
                        spannableStringBuilder.setSpan(new C58202oJ(groupCallParticipantPicker, groupCallParticipantPicker), spanStart, spanEnd, spanFlags);
                    }
                }
            }
            textView.setText(spannableStringBuilder);
            textView.setMovementMethod(new C52162aM());
            return inflate3;
        }
    }

    public final List A2p() {
        List<C15370n3> list = this.A0g;
        ArrayList arrayList = new ArrayList(list.size());
        for (C15370n3 r1 : list) {
            arrayList.add(r1.A0B(UserJid.class));
        }
        return arrayList;
    }

    public void A2q() {
        AnonymousClass02H layoutManager;
        RecyclerView recyclerView = this.A09;
        if (recyclerView != null && (layoutManager = recyclerView.getLayoutManager()) != null) {
            int A06 = layoutManager.A06();
            int i = 0;
            View A0D = this.A09.getLayoutManager().A0D(0);
            if (A06 != 0 && A0D != null) {
                int width = this.A09.getWidth();
                ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) A0D.getLayoutParams();
                int width2 = A0D.getWidth() + marginLayoutParams.leftMargin + marginLayoutParams.rightMargin;
                int paddingRight = this.A09.getPaddingRight();
                if (A06 < this.A0g.size()) {
                    int i2 = A06 * width2;
                    if (paddingRight == 0 && width >= i2 - marginLayoutParams.leftMargin && width <= i2 + marginLayoutParams.rightMargin) {
                        i = width2 >> 1;
                    } else {
                        return;
                    }
                } else if (paddingRight <= 0) {
                    return;
                }
                RecyclerView recyclerView2 = this.A09;
                recyclerView2.setPadding(recyclerView2.getPaddingLeft(), this.A09.getPaddingTop(), i, this.A09.getPaddingBottom());
            }
        }
    }

    public void A2r() {
        Intent intent;
        ArrayList<String> arrayList;
        String str;
        String rawString;
        ArrayList arrayList2;
        String str2;
        if (this instanceof NotifyContactsSelector) {
            intent = new Intent();
            arrayList = C15380n4.A06(A2p());
            str = "jids";
        } else if (!(this instanceof GroupMembersSelector)) {
            if (!(this instanceof EditGroupAdminsSelector)) {
                if (!(this instanceof EditBroadcastRecipientsSelector)) {
                    if (this instanceof ListMembersSelector) {
                        ListMembersSelector listMembersSelector = (ListMembersSelector) this;
                        C15680nj r6 = listMembersSelector.A00;
                        r6.A00.A0B();
                        long currentTimeMillis = System.currentTimeMillis() / 1000;
                        synchronized (r6.A01) {
                            while (true) {
                                StringBuilder sb = new StringBuilder();
                                sb.append(currentTimeMillis);
                                sb.append("@broadcast");
                                if (r6.A0D(C29901Ve.A02(sb.toString()))) {
                                    currentTimeMillis++;
                                }
                            }
                        }
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(currentTimeMillis);
                        sb2.append("@broadcast");
                        C29901Ve A02 = C29901Ve.A02(sb2.toString());
                        AnonymousClass009.A05(A02);
                        C20710wC r2 = listMembersSelector.A01;
                        r2.A0S.A0S(r2.A08(A02, listMembersSelector.A2p()));
                        listMembersSelector.A02.A03(A02, false);
                        ((ActivityC13790kL) listMembersSelector).A00.A07(listMembersSelector, new C14960mK().A0g(listMembersSelector, ((AbstractActivityC36611kC) listMembersSelector).A0J.A07(A02, "", System.currentTimeMillis())));
                        listMembersSelector.finish();
                        return;
                    } else if (this instanceof ContactsAttachmentSelector) {
                        ContactsAttachmentSelector contactsAttachmentSelector = (ContactsAttachmentSelector) this;
                        C53812fG r4 = contactsAttachmentSelector.A02;
                        List A2p = contactsAttachmentSelector.A2p();
                        AnonymousClass016 r22 = r4.A02;
                        r22.A0B(A2p);
                        r4.A03.A0B(Boolean.TRUE);
                        AnonymousClass1AW r1 = r4.A09;
                        AnonymousClass02P r3 = r4.A01;
                        r1.A00(new AnonymousClass02O() { // from class: X.3P2
                            @Override // X.AnonymousClass02O
                            public final Object apply(Object obj) {
                                String str3;
                                String str4;
                                C53812fG r5 = C53812fG.this;
                                AnonymousClass1KL r0 = (AnonymousClass1KL) obj;
                                Collection collection = (Collection) r0.A01;
                                AnonymousClass02N r42 = r0.A00;
                                try {
                                    r42.A02();
                                    ArrayList A0l = C12960it.A0l();
                                    C16590pI r9 = r5.A07;
                                    C15550nR r8 = r5.A06;
                                    AnonymousClass018 r23 = r5.A08;
                                    C14650lo r7 = r5.A05;
                                    ArrayList A0l2 = C12960it.A0l();
                                    if (collection != null) {
                                        Iterator A0n = C12960it.A0n(r8.A0H(new HashSet(collection)));
                                        while (A0n.hasNext()) {
                                            Map.Entry A15 = C12970iu.A15(A0n);
                                            if (!(A15.getKey() instanceof UserJid)) {
                                                str4 = "ContactStruct/constructContactsFromUserJid chat JID not an instance of user JID";
                                            } else if (A15.getValue() == null) {
                                                str4 = "ContactStruct/constructContactsFromUserJid null WaContact";
                                            } else {
                                                UserJid userJid = (UserJid) A15.getKey();
                                                C30721Yo r11 = new C30721Yo(r7, r8, r9, r23);
                                                r11.A08.A01 = ((C15370n3) A15.getValue()).A0K;
                                                r11.A0A(userJid, C248917h.A04(userJid), null, 2, true);
                                                A0l2.add(r11);
                                            }
                                            Log.w(str4);
                                        }
                                        Iterator it = A0l2.iterator();
                                        while (it.hasNext()) {
                                            C30721Yo r62 = (C30721Yo) it.next();
                                            r42.A02();
                                            try {
                                                str3 = new C41421tV(r5.A04, r23).A01(r62);
                                            } catch (C41341tN e) {
                                                Log.e("ContactsAttachmentSelectorViewModel/ Could not create VCard", new C41351tO(e));
                                                str3 = null;
                                            }
                                            if (str3 != null) {
                                                A0l.add(str3);
                                            }
                                        }
                                        return A0l;
                                    }
                                    throw new NullPointerException("iterator");
                                } catch (AnonymousClass04U unused) {
                                    return C12960it.A0l();
                                }
                            }
                        }, r22, r3);
                        r4.A00.A0D(r3, new IDxObserverShape4S0100000_2_I1(r4, 38));
                        return;
                    } else if (this instanceof AddGroupParticipantsSelector) {
                        ((ActivityC13790kL) this).A0D.A01(A2e());
                    } else if (this instanceof LinkExistingGroups) {
                        List<C15370n3> unmodifiableList = Collections.unmodifiableList(this.A0g);
                        ArrayList arrayList3 = new ArrayList();
                        for (C15370n3 r0 : unmodifiableList) {
                            Jid jid = r0.A0D;
                            if (jid != null) {
                                arrayList3.add(jid.getRawString());
                            }
                        }
                        intent = new Intent();
                        arrayList = new ArrayList<>(arrayList3);
                        str = "selected_jids";
                    } else {
                        return;
                    }
                }
                intent = new Intent();
                arrayList2 = C15380n4.A06(A2p());
                str2 = "contacts";
            } else {
                intent = new Intent();
                arrayList2 = C15380n4.A06(A2p());
                str2 = "jids";
            }
            intent.putExtra(str2, arrayList2);
            setResult(-1, intent);
            finish();
        } else {
            GroupMembersSelector groupMembersSelector = (GroupMembersSelector) this;
            List A2p2 = groupMembersSelector.A2p();
            if (A2p2.isEmpty()) {
                ((ActivityC13810kN) groupMembersSelector).A05.A07(R.string.no_valid_participant, 0);
                return;
            }
            Intent intent2 = new Intent();
            intent2.setClassName(groupMembersSelector.getPackageName(), "com.whatsapp.group.NewGroup");
            intent2.putExtra("create_group_for_community", false);
            Intent putExtra = intent2.putExtra("selected", C15380n4.A06(A2p2)).putExtra("entry_point", groupMembersSelector.getIntent().getIntExtra("entry_point", -1));
            C15580nU r02 = groupMembersSelector.A02;
            if (r02 == null) {
                rawString = null;
            } else {
                rawString = r02.getRawString();
            }
            groupMembersSelector.startActivityForResult(putExtra.putExtra("parent_group_jid_to_link", rawString), 1);
            return;
        }
        intent.putStringArrayListExtra(str, arrayList);
        setResult(-1, intent);
        finish();
    }

    public final void A2s() {
        C624737j r1 = this.A0Q;
        if (r1 != null) {
            r1.A03(true);
            this.A0Q = null;
        }
        C624737j r2 = new C624737j(this.A0L, this, this.A0R, this.A0W, this.A0X);
        this.A0Q = r2;
        ((ActivityC13830kP) this).A05.Aaz(r2, new Void[0]);
    }

    public final void A2t() {
        AnonymousClass36m r2;
        AnonymousClass36m r0 = this.A0O;
        if (r0 != null) {
            r0.A03(true);
        }
        C624737j r02 = this.A0Q;
        if (r02 != null) {
            r02.A03(true);
            this.A0Q = null;
        }
        if (!(this instanceof LinkExistingGroups)) {
            r2 = new AnonymousClass2x9(((ActivityC13810kN) this).A06, this.A0J, this.A0L, this, this.A0S, this.A0g);
        } else {
            LinkExistingGroups linkExistingGroups = (LinkExistingGroups) this;
            r2 = new AnonymousClass2xA(((ActivityC13810kN) linkExistingGroups).A06, ((AbstractActivityC36611kC) linkExistingGroups).A0L, linkExistingGroups, linkExistingGroups.A0S, linkExistingGroups.A01, linkExistingGroups.A02, linkExistingGroups.A0g);
        }
        this.A0O = r2;
        ((ActivityC13830kP) this).A05.Aaz(r2, new Void[0]);
    }

    public final void A2u() {
        boolean z;
        AnimatorSet animatorSet = new AnimatorSet();
        this.A02 = animatorSet;
        if (this.A0a) {
            if (this instanceof GroupCallParticipantPicker) {
                A2z(null, animatorSet);
                this.A02.start();
            }
            A2w();
            return;
        }
        ValueAnimator valueAnimator = this.A03;
        if (valueAnimator != null && valueAnimator.isRunning()) {
            this.A03.end();
        }
        ValueAnimator ofInt = ValueAnimator.ofInt(0, this.A01);
        ofInt.addUpdateListener(new C65353Jg(this));
        ofInt.addListener(new C95704eD(this));
        ofInt.setDuration(240L);
        if (!(this instanceof GroupCallParticipantPicker)) {
            z = false;
        } else {
            z = true;
        }
        AnimatorSet animatorSet2 = this.A02;
        if (z) {
            A2z(ofInt, animatorSet2);
        } else {
            animatorSet2.play(ofInt);
        }
        this.A02.start();
    }

    public final void A2v() {
        TextView textView;
        int A2h;
        View findViewById = findViewById(R.id.contacts_empty_permission_denied);
        View findViewById2 = findViewById(R.id.contacts_empty);
        View findViewById3 = findViewById(R.id.search_no_matches);
        View findViewById4 = findViewById(R.id.init_contacts_progress);
        if (!this.A0I.A00()) {
            findViewById4.setVisibility(8);
            findViewById.setVisibility(0);
        } else if (this.A0O != null) {
            findViewById4.setVisibility(0);
            findViewById.setVisibility(8);
        } else {
            if (!TextUtils.isEmpty(this.A0V)) {
                findViewById4.setVisibility(8);
                findViewById.setVisibility(8);
                findViewById2.setVisibility(8);
                findViewById3.setVisibility(0);
                textView = (TextView) findViewById3;
                A2h = R.string.search_no_results;
            } else {
                boolean A37 = A37();
                findViewById4.setVisibility(8);
                if (A37) {
                    findViewById.setVisibility(8);
                    findViewById2.setVisibility(8);
                    findViewById3.setVisibility(0);
                    textView = (TextView) findViewById3;
                    A2h = A2h();
                } else {
                    findViewById.setVisibility(8);
                    findViewById2.setVisibility(0);
                    findViewById3.setVisibility(8);
                    int size = this.A0g.size();
                    A2y(size);
                    A2x(size);
                }
            }
            textView.setText(getString(A2h, this.A0V));
            int size = this.A0g.size();
            A2y(size);
            A2x(size);
        }
        findViewById2.setVisibility(8);
        findViewById3.setVisibility(8);
        int size = this.A0g.size();
        A2y(size);
        A2x(size);
    }

    public final void A2w() {
        ViewGroup viewGroup;
        int i;
        if (!this.A0a || !this.A0g.isEmpty()) {
            viewGroup = this.A07;
            i = 8;
        } else {
            viewGroup = this.A07;
            i = 0;
        }
        viewGroup.setVisibility(i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001d, code lost:
        if (r3 == 0) goto L_0x0016;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A2x(int r3) {
        /*
            r2 = this;
            boolean r0 = r2 instanceof com.whatsapp.contact.picker.AddGroupParticipantsSelector
            if (r0 != 0) goto L_0x001b
            java.util.List r0 = r2.A0X
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x0020
            java.util.List r0 = r2.A0g
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x0020
            com.whatsapp.components.FloatingActionButton r1 = r2.A0H
        L_0x0016:
            r0 = 1
            r1.A03(r0)
        L_0x001a:
            return
        L_0x001b:
            com.whatsapp.components.FloatingActionButton r1 = r2.A0H
            if (r3 != 0) goto L_0x0026
            goto L_0x0016
        L_0x0020:
            boolean r0 = r2 instanceof com.whatsapp.calling.callhistory.group.GroupCallParticipantPicker
            if (r0 != 0) goto L_0x001a
            com.whatsapp.components.FloatingActionButton r1 = r2.A0H
        L_0x0026:
            r0 = 1
            r1.A04(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractActivityC36611kC.A2x(int):void");
    }

    public void A2y(int i) {
        String A0I;
        AbstractC005102i A1U = A1U();
        AnonymousClass009.A05(A1U);
        int A2j = A2j();
        boolean z = false;
        if (A2j > 0) {
            z = true;
        }
        AnonymousClass009.A0A("Max contacts must be positive", z);
        AnonymousClass018 r6 = this.A0S;
        if (A2j == Integer.MAX_VALUE) {
            A0I = r6.A0I(new Object[]{Integer.valueOf(i)}, R.plurals.n_contacts_selected, (long) i);
        } else {
            A0I = r6.A0I(new Object[]{Integer.valueOf(i), Integer.valueOf(A2j)}, R.plurals.n_of_m_contacts_selected, (long) i);
        }
        A1U.A0H(A0I);
    }

    public final void A2z(Animator animator, AnimatorSet animatorSet) {
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.selected_list_action_fab_1_margin_right);
        int dimensionPixelSize2 = getResources().getDimensionPixelSize(R.dimen.selected_list_action_fab_2_margin_right);
        int i = dimensionPixelSize + this.A00;
        int i2 = -1;
        int i3 = 1;
        if (!this.A0S.A04().A06) {
            i3 = -1;
        }
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.A0D, "translationX", (float) (i * i3));
        ofFloat.setDuration(240L).setInterpolator(new DecelerateInterpolator());
        ofFloat.setStartDelay(50);
        int i4 = dimensionPixelSize2 + this.A00;
        if (!(!this.A0S.A04().A06)) {
            i2 = 1;
        }
        ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(this.A0E, "translationX", (float) (i4 * i2));
        ofFloat2.setDuration(240L).setInterpolator(new DecelerateInterpolator());
        AnimatorSet.Builder play = animatorSet.play(ofFloat);
        if (animator == null) {
            play.with(ofFloat2);
        } else {
            play.with(ofFloat2).after(animator);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x002d, code lost:
        if (r5.A0d != false) goto L_0x002f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A30(X.C64043Ea r4, X.C15370n3 r5) {
        /*
            r3 = this;
            X.1J1 r1 = r3.A0M
            if (r1 == 0) goto L_0x0009
            android.widget.ImageView r0 = r4.A01
            r1.A06(r0, r5)
        L_0x0009:
            X.1Pb r2 = r4.A03
            java.util.ArrayList r1 = r3.A0W
            r0 = -1
            r2.A07(r5, r1, r0)
            android.view.View r2 = r4.A00
            r1 = 1
            com.facebook.redex.ViewOnClickCListenerShape0S0300000_I0 r0 = new com.facebook.redex.ViewOnClickCListenerShape0S0300000_I0
            r0.<init>(r3, r5, r4, r1)
            r2.setOnClickListener(r0)
            int r1 = r3.A2j()
            java.util.List r0 = r3.A0g
            int r0 = r0.size()
            if (r1 != r0) goto L_0x002f
            boolean r1 = r5.A0d
            r0 = 1052938076(0x3ec28f5c, float:0.38)
            if (r1 == 0) goto L_0x0031
        L_0x002f:
            r0 = 1065353216(0x3f800000, float:1.0)
        L_0x0031:
            r2.setAlpha(r0)
            com.whatsapp.components.SelectionCheckView r0 = r4.A04
            r0.setTag(r5)
            r3.A31(r4, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractActivityC36611kC.A30(X.3Ea, X.0n3):void");
    }

    public void A31(C64043Ea r4, C15370n3 r5) {
        if (!A38(r5) || r5.A0d) {
            if (r5.A0R == null || (this instanceof GroupCallParticipantPicker)) {
                r4.A02.setVisibility(8);
            } else {
                TextEmojiLabel textEmojiLabel = r4.A02;
                textEmojiLabel.setVisibility(0);
                textEmojiLabel.A0G(null, r5.A0R);
                String str = r5.A0R;
                if (str == null) {
                    str = "";
                }
                textEmojiLabel.A0G(null, str);
            }
            r4.A01(r5.A0d);
            return;
        }
        r4.A00(getString(R.string.tap_unblock), true);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003b, code lost:
        if (r7 >= 0) goto L_0x003d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A32(X.C15370n3 r10) {
        /*
        // Method dump skipped, instructions count: 567
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractActivityC36611kC.A32(X.0n3):void");
    }

    public void A33(C15370n3 r7) {
        int i;
        int i2;
        if (this instanceof GroupMembersSelector) {
            i = R.string.unblock_before_add_group;
        } else if ((this instanceof EditBroadcastRecipientsSelector) || (this instanceof ListMembersSelector)) {
            i = R.string.unblock_before_add_broadcast;
        } else {
            if (this instanceof AddGroupParticipantsSelector) {
                i2 = R.string.unblock_before_add_group;
            } else if (this instanceof GroupCallParticipantPicker) {
                i2 = R.string.unblock_before_add_group_call;
            } else {
                return;
            }
            String string = getString(i2, this.A0L.A04(r7));
            C238013b r2 = this.A0F;
            Jid A0B = r7.A0B(UserJid.class);
            AnonymousClass009.A05(A0B);
            UnblockDialogFragment.A00(new AnonymousClass543(this, r2, (UserJid) A0B), string, R.string.blocked_title, false).A1F(A0V(), null);
            return;
        }
        String string2 = getString(i, this.A0L.A04(r7));
        C238013b r22 = this.A0F;
        Jid A0B2 = r7.A0B(UserJid.class);
        AnonymousClass009.A05(A0B2);
        Adm(UnblockDialogFragment.A00(new AnonymousClass543(this, r22, (UserJid) A0B2), string2, R.string.blocked_title, false));
    }

    public void A34(String str) {
        this.A0V = str;
        ArrayList A02 = C32751cg.A02(this.A0S, str);
        this.A0W = A02;
        if (A02.isEmpty()) {
            this.A0W = null;
        }
        A2s();
    }

    public void A35(ArrayList arrayList) {
        this.A0J.A0T(arrayList);
    }

    public void A36(List list) {
        this.A0O = null;
        this.A0X = list;
        A2s();
        int i = 0;
        if (this.A0Z) {
            HashSet hashSet = new HashSet();
            List list2 = this.A0Y;
            if (list2 != null && !list2.isEmpty()) {
                for (C15370n3 r5 : this.A0X) {
                    if (this.A0Y.contains(r5.A0B(AbstractC14640lm.class))) {
                        r5.A0d = true;
                        if (!hashSet.contains(r5.A0B(AbstractC14640lm.class))) {
                            List list3 = this.A0g;
                            list3.add(r5);
                            hashSet.add(r5.A0B(AbstractC14640lm.class));
                            if (list3.size() >= A2j()) {
                                break;
                            }
                        } else {
                            continue;
                        }
                    }
                }
                this.A0d.A02();
            }
            this.A0Z = false;
        }
        List list4 = this.A0g;
        int size = list4.size();
        A2y(size);
        A2x(size);
        View view = this.A05;
        if (list4.isEmpty()) {
            i = 4;
        }
        view.setVisibility(i);
        if (!list4.isEmpty()) {
            int i2 = this.A01;
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.A08.getLayoutParams();
            layoutParams.topMargin = i2;
            this.A08.setLayoutParams(layoutParams);
            A2u();
        }
        MenuItem menuItem = this.A04;
        if (menuItem != null) {
            menuItem.setVisible(true ^ this.A0X.isEmpty());
        }
    }

    public final boolean A38(C15370n3 r4) {
        return r4.A0B(UserJid.class) != null && this.A0F.A0I((UserJid) r4.A0B(UserJid.class));
    }

    @Override // X.ActivityC13810kN, android.app.Activity, android.view.Window.Callback
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        try {
            return super.dispatchTouchEvent(motionEvent);
        } catch (IllegalArgumentException unused) {
            return false;
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        if (this.A0A.A05()) {
            this.A0A.A04(true);
        } else {
            finish();
        }
    }

    @Override // android.app.Activity
    public boolean onContextItemSelected(MenuItem menuItem) {
        C15370n3 r3 = (C15370n3) A2e().getItemAtPosition(((AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo()).position);
        if (menuItem.getItemId() == 0) {
            if (r3.A0B(UserJid.class) != null) {
                C238013b r1 = this.A0F;
                Jid A0B = r3.A0B(UserJid.class);
                AnonymousClass009.A05(A0B);
                r1.A0C(this, (UserJid) A0B);
                return true;
            }
            AnonymousClass009.A07("only user jid should be unblocked");
        }
        return super.onContextItemSelected(menuItem);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        int i;
        boolean z;
        int dimensionPixelSize;
        boolean z2;
        ListView listView;
        int dimensionPixelSize2;
        Resources resources;
        int i2;
        int i3;
        Drawable A01;
        Drawable A012;
        int i4;
        int i5;
        String string;
        super.onCreate(bundle);
        LayoutInflater layoutInflater = getLayoutInflater();
        if (!(this instanceof GroupCallParticipantPickerSheet)) {
            i = R.layout.multiple_contact_picker;
        } else {
            i = R.layout.group_call_participant_picker_sheet;
        }
        setContentView(layoutInflater.inflate(i, (ViewGroup) null));
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        A1e(toolbar);
        AbstractC005102i A1U = A1U();
        AnonymousClass009.A05(A1U);
        A1U.A0M(true);
        A1U.A0N(true);
        A1U.A0A(A2g());
        this.A0M = this.A0N.A04(this, "multiple-contact-picker");
        this.A0A = new C48232Fc(this, findViewById(R.id.search_holder), new C103764rH(this), toolbar, this.A0S);
        ListView A2e = A2e();
        this.A08 = A2e;
        boolean z3 = this instanceof GroupCallParticipantPicker;
        if (!z3) {
            z = true;
        } else {
            z = false;
        }
        A2e.setFastScrollAlwaysVisible(z);
        this.A08.setScrollBarStyle(33554432);
        if (A2n() != null) {
            this.A08.addHeaderView(A2n(), null, false);
        }
        List list = this.A0g;
        list.clear();
        if (bundle != null) {
            List<AbstractC14640lm> A07 = C15380n4.A07(AbstractC14640lm.class, bundle.getStringArrayList("selected_jids"));
            if (!A07.isEmpty()) {
                for (AbstractC14640lm r2 : A07) {
                    C15370n3 A0A = this.A0J.A0A(r2);
                    if (A0A != null) {
                        A0A.A0d = true;
                        list.add(A0A);
                    }
                }
            }
        } else {
            this.A0Y = C15380n4.A07(AbstractC14640lm.class, getIntent().getStringArrayListExtra("selected"));
        }
        A2t();
        this.A06 = findViewById(R.id.selected_items_divider);
        this.A09 = (RecyclerView) findViewById(R.id.selected_items);
        this.A00 = getResources().getDimensionPixelSize(R.dimen.selected_contacts_list_action_fab_size);
        RecyclerView recyclerView = this.A09;
        if (!z3) {
            dimensionPixelSize = 0;
        } else {
            dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.selected_contacts_list_left_padding);
        }
        recyclerView.setPadding(dimensionPixelSize, this.A09.getPaddingTop(), this.A09.getPaddingRight(), this.A09.getPaddingBottom());
        if (!z3) {
            z2 = false;
        } else {
            z2 = true;
        }
        if (z2) {
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.A09.getLayoutParams();
            int dimensionPixelSize3 = getResources().getDimensionPixelSize(R.dimen.selected_contact_list_with_fab_margin_right);
            if (!this.A0S.A04().A06) {
                layoutParams.rightMargin = dimensionPixelSize3;
            } else {
                layoutParams.leftMargin = dimensionPixelSize3;
            }
            this.A09.setLayoutParams(layoutParams);
        }
        int dimensionPixelSize4 = getResources().getDimensionPixelSize(R.dimen.selected_contacts_top_offset);
        this.A09.A0l(new C74893ix(this, dimensionPixelSize4));
        SmoothScrollLinearLayoutManager smoothScrollLinearLayoutManager = new SmoothScrollLinearLayoutManager();
        smoothScrollLinearLayoutManager.A1Q(0);
        this.A09.setLayoutManager(smoothScrollLinearLayoutManager);
        this.A09.setAdapter(this.A0d);
        this.A09.setItemAnimator(new C55262i6());
        this.A08.setOnScrollListener(new C102254oq(this));
        this.A08.setFastScrollEnabled(true);
        this.A08.setScrollbarFadingEnabled(true);
        boolean z4 = this.A0S.A04().A06;
        ListView listView2 = this.A08;
        if (z4) {
            listView2.setVerticalScrollbarPosition(1);
            listView = this.A08;
            dimensionPixelSize2 = getResources().getDimensionPixelSize(R.dimen.contact_list_padding_right);
            resources = getResources();
            i2 = R.dimen.contact_list_padding_left;
        } else {
            listView2.setVerticalScrollbarPosition(2);
            listView = this.A08;
            dimensionPixelSize2 = getResources().getDimensionPixelSize(R.dimen.contact_list_padding_left);
            resources = getResources();
            i2 = R.dimen.contact_list_padding_right;
        }
        listView.setPadding(dimensionPixelSize2, 0, resources.getDimensionPixelSize(i2), 0);
        this.A08.setOnItemClickListener(new AdapterView.OnItemClickListener() { // from class: X.4oy
            @Override // android.widget.AdapterView.OnItemClickListener
            public final void onItemClick(AdapterView adapterView, View view, int i6, long j) {
                AbstractActivityC36611kC r22 = AbstractActivityC36611kC.this;
                View findViewById = view.findViewById(R.id.selection_check);
                if (findViewById != null) {
                    C15370n3 r1 = (C15370n3) findViewById.getTag();
                    if (r22.A38(r1)) {
                        r22.A33(r1);
                    } else {
                        r22.A32(r1);
                    }
                }
            }
        });
        Resources resources2 = getResources();
        if (!z3) {
            i3 = R.dimen.selected_contacts_layout_height;
        } else {
            i3 = R.dimen.selected_contacts_layout_height_big;
        }
        this.A01 = resources2.getDimensionPixelSize(i3);
        View findViewById = findViewById(R.id.selected_list);
        this.A05 = findViewById;
        FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) findViewById.getLayoutParams();
        layoutParams2.height = this.A01;
        this.A05.setLayoutParams(layoutParams2);
        this.A05.setVisibility(4);
        this.A0D = (WaImageButton) findViewById(R.id.selected_list_action_fab_1);
        this.A0E = (WaImageButton) findViewById(R.id.selected_list_action_fab_2);
        WaImageButton waImageButton = this.A0D;
        if (!z2) {
            waImageButton.setVisibility(8);
            this.A0E.setVisibility(8);
        } else {
            waImageButton.setVisibility(0);
            this.A0E.setVisibility(0);
            WaImageButton waImageButton2 = this.A0D;
            if (!z3) {
                A01 = null;
            } else {
                A01 = AnonymousClass2GE.A01(this, R.drawable.ic_groupcall_voice, R.color.voipGroupCallPickerButtonTint);
            }
            waImageButton2.setImageDrawable(A01);
            WaImageButton waImageButton3 = this.A0E;
            if (!z3) {
                A012 = null;
            } else {
                A012 = AnonymousClass2GE.A01(this, R.drawable.ic_groupcall_video, R.color.voipGroupCallPickerButtonTint);
            }
            waImageButton3.setImageDrawable(A012);
            WaImageButton waImageButton4 = this.A0D;
            if (!z3) {
                i4 = 0;
            } else {
                i4 = R.string.audio_call;
            }
            waImageButton4.setContentDescription(getString(i4));
            WaImageButton waImageButton5 = this.A0E;
            if (!z3) {
                i5 = 0;
            } else {
                i5 = R.string.video_call;
            }
            waImageButton5.setContentDescription(getString(i5));
            this.A0D.setOnClickListener(new ViewOnClickCListenerShape1S0100000_I0_1(this, 32));
            this.A0E.setOnClickListener(new ViewOnClickCListenerShape1S0100000_I0_1(this, 31));
            C42941w9.A09(this.A0D, this.A0S, 0, dimensionPixelSize4, -this.A00, dimensionPixelSize4);
            C42941w9.A09(this.A0E, this.A0S, 0, dimensionPixelSize4, -this.A00, dimensionPixelSize4);
            this.A09.postDelayed(new RunnableBRunnable0Shape4S0100000_I0_4(this, 29), 200);
        }
        this.A07 = (ViewGroup) findViewById(R.id.warning);
        View A2o = A2o();
        if (A2o != null) {
            this.A0a = true;
            this.A07.removeAllViews();
            this.A07.addView(A2o);
        } else {
            if (this instanceof ListMembersSelector) {
                C15570nT r0 = ((ActivityC13790kL) this).A01;
                r0.A08();
                Me me = r0.A00;
                AnonymousClass018 r5 = this.A0S;
                String str = me.cc;
                string = getString(R.string.broadcast_to_recipients_note, r5.A0G(AnonymousClass23M.A0E(str, me.jabber_id.substring(str.length()))).replace(' ', (char) 160));
            } else if (!(this instanceof LinkExistingGroups)) {
                string = "";
            } else {
                string = getString(R.string.link_existing_groups_details);
            }
            this.A0a = !TextUtils.isEmpty(string);
            ((TextView) findViewById(R.id.warning_text)).setText(string);
        }
        A2w();
        C36631kE r02 = new C36631kE(this, this, this.A0f);
        this.A0P = r02;
        A2f(r02);
        FloatingActionButton floatingActionButton = (FloatingActionButton) AnonymousClass00T.A05(this, R.id.next_btn);
        this.A0H = floatingActionButton;
        if (!z3) {
            floatingActionButton.setImageDrawable(A2m());
            this.A0H.setContentDescription(getString(A2l()));
            this.A0H.setOnClickListener(new ViewOnClickCListenerShape14S0100000_I0_1(this, 0));
        }
        ((EmptyTellAFriendView) findViewById(R.id.contacts_empty)).setInviteButtonClickListener(new ViewOnClickCListenerShape1S0100000_I0_1(this, 30));
        findViewById(R.id.button_open_permission_settings).setOnClickListener(new ViewOnClickCListenerShape14S0100000_I0_1(this, 1));
        registerForContextMenu(this.A08);
        A2v();
    }

    @Override // X.ActivityC13790kL, android.app.Activity, android.view.View.OnCreateContextMenuListener
    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        C15370n3 r1 = (C15370n3) A2e().getItemAtPosition(((AdapterView.AdapterContextMenuInfo) contextMenuInfo).position);
        if (A38(r1)) {
            super.onCreateContextMenu(contextMenu, view, contextMenuInfo);
            contextMenu.add(0, 0, 0, getString(R.string.block_list_menu_unblock, this.A0L.A04(r1)));
        }
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem icon = menu.add(0, R.id.menuitem_search, 0, R.string.search).setIcon(R.drawable.ic_action_search);
        this.A04 = icon;
        icon.setShowAsAction(2);
        this.A04.setVisible(!this.A0X.isEmpty());
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC13770kJ, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A0X.clear();
        this.A0f.clear();
        AnonymousClass1J1 r0 = this.A0M;
        if (r0 != null) {
            r0.A00();
            this.A0M = null;
        }
        AnonymousClass36m r02 = this.A0O;
        if (r02 != null) {
            r02.A03(true);
            this.A0O = null;
        }
        C624737j r03 = this.A0Q;
        if (r03 != null) {
            r03.A03(true);
            this.A0Q = null;
        }
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId == R.id.menuitem_search) {
            onSearchRequested();
            return true;
        } else if (itemId != 16908332) {
            return true;
        } else {
            finish();
            return true;
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        this.A0K.A04(this.A0c);
        this.A0G.A04(this.A0b);
        this.A0T.A04(this.A0e);
    }

    @Override // X.ActivityC13770kJ, android.app.Activity
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        this.A0A.A02(bundle);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        this.A0K.A03(this.A0c);
        this.A0G.A03(this.A0b);
        this.A0T.A03(this.A0e);
        this.A0P.notifyDataSetChanged();
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        List<C15370n3> list = this.A0g;
        if (!list.isEmpty()) {
            ArrayList arrayList = new ArrayList(list.size());
            for (C15370n3 r1 : list) {
                arrayList.add(r1.A0B(AbstractC14640lm.class));
            }
            bundle.putStringArrayList("selected_jids", C15380n4.A06(arrayList));
        }
        this.A0A.A03(bundle);
    }

    @Override // android.app.Activity, android.view.Window.Callback
    public boolean onSearchRequested() {
        this.A0A.A01();
        return false;
    }
}
