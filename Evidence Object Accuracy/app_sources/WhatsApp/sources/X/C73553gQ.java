package X;

import android.text.TextPaint;
import android.text.style.MetricAffectingSpan;

/* renamed from: X.3gQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73553gQ extends MetricAffectingSpan {
    public final float A00;
    public final float A01;

    public C73553gQ(float f, float f2) {
        this.A01 = f;
        this.A00 = f2;
    }

    public final void A00(TextPaint textPaint) {
        textPaint.setLetterSpacing(this.A01 / (textPaint.getTextSize() / this.A00));
    }

    @Override // android.text.style.CharacterStyle
    public void updateDrawState(TextPaint textPaint) {
        A00(textPaint);
    }

    @Override // android.text.style.MetricAffectingSpan
    public void updateMeasureState(TextPaint textPaint) {
        A00(textPaint);
    }
}
