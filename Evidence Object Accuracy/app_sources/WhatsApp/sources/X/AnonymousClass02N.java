package X;

import android.os.CancellationSignal;

/* renamed from: X.02N  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass02N {
    public AbstractC11730gl A00;
    public Object A01;
    public boolean A02;
    public boolean A03;

    public Object A00() {
        Object obj;
        synchronized (this) {
            if (this.A01 == null) {
                CancellationSignal cancellationSignal = new CancellationSignal();
                this.A01 = cancellationSignal;
                if (this.A03) {
                    C04080Kh.A00(cancellationSignal);
                }
            }
            obj = this.A01;
        }
        return obj;
    }

    public void A01() {
        synchronized (this) {
            try {
                if (!this.A03) {
                    this.A03 = true;
                    this.A02 = true;
                    AbstractC11730gl r2 = this.A00;
                    Object obj = this.A01;
                    if (r2 != null) {
                        try {
                            r2.ANe();
                        } catch (Throwable th) {
                            synchronized (this) {
                                try {
                                    this.A02 = false;
                                    notifyAll();
                                    throw th;
                                } catch (Throwable th2) {
                                    throw th2;
                                }
                            }
                        }
                    }
                    if (obj != null) {
                        C04080Kh.A00(obj);
                    }
                    synchronized (this) {
                        try {
                            this.A02 = false;
                            notifyAll();
                        } catch (Throwable th3) {
                            throw th3;
                        }
                    }
                }
            } catch (Throwable th4) {
                throw th4;
            }
        }
    }

    public void A02() {
        if (A04()) {
            throw new AnonymousClass04U(null);
        }
    }

    public void A03(AbstractC11730gl r2) {
        synchronized (this) {
            while (this.A02) {
                try {
                    wait();
                } catch (InterruptedException unused) {
                }
            }
            if (this.A00 != r2) {
                this.A00 = r2;
                if (this.A03) {
                    r2.ANe();
                }
            }
        }
    }

    public boolean A04() {
        boolean z;
        synchronized (this) {
            z = this.A03;
        }
        return z;
    }
}
