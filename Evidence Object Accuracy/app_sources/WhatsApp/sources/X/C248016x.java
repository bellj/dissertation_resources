package X;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import com.facebook.redex.RunnableBRunnable0Shape0S0400000_I0;
import com.whatsapp.RequestPermissionActivity;
import java.util.List;

/* renamed from: X.16x  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C248016x {
    public final C15890o4 A00;
    public final C14820m6 A01;
    public final C69683a4 A02;
    public final C69693a5 A03;
    public final C27691It A04 = new C27691It();
    public final AbstractC14440lR A05;
    public final AnonymousClass01H A06;

    public C248016x(C14330lG r3, C16590pI r4, C15890o4 r5, C14820m6 r6, AbstractC14440lR r7, AnonymousClass01H r8) {
        AnonymousClass4OP r1 = new AnonymousClass4OP(r3, r4);
        this.A05 = r7;
        this.A00 = r5;
        this.A02 = new C69683a4(r4, r1);
        this.A03 = new C69693a5(r4, r1);
        this.A01 = r6;
        this.A06 = r8;
    }

    public void A00(Intent intent) {
        SharedPreferences sharedPreferences;
        boolean z;
        boolean booleanExtra;
        if (intent != null && (booleanExtra = intent.getBooleanExtra("feature_disabled", (z = (sharedPreferences = this.A01.A00).getBoolean("is_status_sharing_with_fb_disabled", false)))) != z) {
            sharedPreferences.edit().putBoolean("is_status_sharing_with_fb_disabled", booleanExtra).apply();
        }
    }

    public boolean A01() {
        C69683a4 r3 = this.A02;
        int i = 0;
        while (true) {
            AnonymousClass5WH[] r1 = r3.A00;
            if (i >= r1.length) {
                break;
            } else if (!r1[i].AKG()) {
                i++;
            } else if (this.A01.A00.getBoolean("is_status_sharing_with_fb_disabled", false)) {
                break;
            } else {
                return true;
            }
        }
        return false;
    }

    public final boolean A02(Activity activity, AnonymousClass01E r9, AbstractC28951Pq r10, List list) {
        boolean A0S;
        C15890o4 r1 = this.A00;
        if (!r1.A07()) {
            if (r9 != null) {
                A0S = RequestPermissionActivity.A0X(r9, r1);
            } else {
                A0S = RequestPermissionActivity.A0S(activity, r1);
            }
            if (!A0S) {
                return false;
            }
        }
        this.A05.Ab2(new RunnableBRunnable0Shape0S0400000_I0(this, r10, list, activity, 30));
        return true;
    }
}
