package X;

/* renamed from: X.5l6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122215l6 extends C1329368u {
    public final AbstractC001200n A00;
    public final C130105yo A01;
    public final C129235xO A02;
    public final C130095yn A03;
    public final boolean A04;
    public final boolean A05;

    public C122215l6(AbstractC001200n r19, ActivityC13790kL r20, C20380vf r21, C21860y6 r22, C18660so r23, C20390vg r24, C18600si r25, C243515e r26, C18610sj r27, C17070qD r28, AnonymousClass1A7 r29, C130105yo r30, C129235xO r31, C130095yn r32, C30941Zk r33, AbstractC136336Md r34, AnonymousClass6M5 r35, AbstractC136346Me r36, AbstractC14440lR r37, boolean z, boolean z2) {
        super(r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r33, r34, r35, r36, r37, true);
        this.A00 = r19;
        this.A02 = r31;
        this.A03 = r32;
        this.A04 = z;
        this.A05 = z2;
        this.A01 = r30;
    }

    @Override // X.C1329368u
    public void A02(boolean z, boolean z2) {
        super.A02(z, z2);
        if (this.A05) {
            C130095yn r3 = this.A03;
            AnonymousClass016 A0T = C12980iv.A0T();
            r3.A0A.Ab2(new AnonymousClass6JO(A0T, r3, null));
            C117295Zj.A0s(this.A00, A0T, this, 127);
        }
        if (this.A04) {
            C117295Zj.A0s(this.A00, this.A02.A00(), this, 128);
        }
    }
}
