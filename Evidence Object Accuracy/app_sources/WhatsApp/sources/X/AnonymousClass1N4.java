package X;

/* renamed from: X.1N4  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1N4 extends AbstractC16110oT {
    public Integer A00;
    public Long A01;
    public Long A02;

    public AnonymousClass1N4() {
        super(2862, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(2, this.A00);
        r3.Abe(1, this.A01);
        r3.Abe(3, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        StringBuilder sb = new StringBuilder("WamPsIdUpdate {");
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "psIdAction", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "psIdKey", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "psIdRotationFrequence", this.A02);
        sb.append("}");
        return sb.toString();
    }
}
