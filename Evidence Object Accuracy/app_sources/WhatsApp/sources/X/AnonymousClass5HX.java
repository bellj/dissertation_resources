package X;

import java.security.InvalidKeyException;

/* renamed from: X.5HX  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5HX extends InvalidKeyException {
    public final Throwable cause;

    public AnonymousClass5HX(String str, Throwable th) {
        super(str);
        this.cause = th;
    }

    @Override // java.lang.Throwable
    public Throwable getCause() {
        return this.cause;
    }
}
