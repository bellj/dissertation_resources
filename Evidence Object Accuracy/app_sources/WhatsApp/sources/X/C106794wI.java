package X;

import android.util.Log;

/* renamed from: X.4wI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C106794wI implements AbstractC116785Ww {
    public int A00 = -1;
    public long A01 = -1;
    public AbstractC14070ko A02;
    public AnonymousClass5X6 A03;
    public AbstractC116605Wc A04;

    public static AnonymousClass4T4 A00(AnonymousClass5Yf r13) {
        C93324Zz A00;
        byte[] bArr;
        C95304dT A05 = C95304dT.A05(16);
        if (C93324Zz.A00(r13, A05).A00 == 1380533830) {
            r13.AZ4(A05.A02, 0, 4);
            int A03 = C95304dT.A03(A05, 0);
            if (A03 != 1463899717) {
                Log.e("WavHeaderReader", C12960it.A0W(A03, "Unsupported RIFF format: "));
            } else {
                while (true) {
                    A00 = C93324Zz.A00(r13, A05);
                    if (A00.A00 == 1718449184) {
                        break;
                    }
                    r13.A5r((int) A00.A01);
                }
                long j = A00.A01;
                C95314dV.A04(C12990iw.A1W((j > 16 ? 1 : (j == 16 ? 0 : -1))));
                C95304dT.A06(r13, A05, 16);
                A05.A0S(0);
                int A0A = A05.A0A();
                int A0A2 = A05.A0A();
                int A09 = A05.A09();
                A05.A09();
                int A0A3 = A05.A0A();
                int A0A4 = A05.A0A();
                int i = ((int) j) - 16;
                if (i > 0) {
                    bArr = new byte[i];
                    r13.AZ4(bArr, 0, i);
                } else {
                    bArr = AnonymousClass3JZ.A0A;
                }
                return new AnonymousClass4T4(bArr, A0A, A0A2, A09, A0A3, A0A4);
            }
        }
        return null;
    }

    @Override // X.AbstractC116785Ww
    public void AIa(AbstractC14070ko r3) {
        this.A02 = r3;
        this.A03 = r3.Af4(0, 1);
        r3.A9V();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0096, code lost:
        if (r2 != 65534) goto L_0x0098;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00ad, code lost:
        if (r5 != 0) goto L_0x00af;
     */
    @Override // X.AbstractC116785Ww
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int AZn(X.AnonymousClass5Yf r12, X.AnonymousClass4IG r13) {
        /*
        // Method dump skipped, instructions count: 310
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C106794wI.AZn(X.5Yf, X.4IG):int");
    }

    @Override // X.AbstractC116785Ww
    public void AbQ(long j, long j2) {
        AbstractC116605Wc r0 = this.A04;
        if (r0 != null) {
            r0.Aaf(j2);
        }
    }

    @Override // X.AbstractC116785Ww
    public boolean Ae5(AnonymousClass5Yf r2) {
        return C12960it.A1W(A00(r2));
    }
}
