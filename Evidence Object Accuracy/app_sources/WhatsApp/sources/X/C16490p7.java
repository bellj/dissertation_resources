package X;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteFullException;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.Collections;
import java.util.concurrent.Callable;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* renamed from: X.0p7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16490p7 implements AnonymousClass01N {
    public boolean A00;
    public boolean A01;
    public final AbstractC15710nm A02;
    public final C15570nT A03;
    public final AnonymousClass1HL A04;
    public final C29561To A05;
    public final AnonymousClass16I A06;
    public final File A07;
    public final ReentrantReadWriteLock.WriteLock A08;
    public final ReentrantReadWriteLock A09;

    public C16490p7(AbstractC15710nm r11, C15570nT r12, C16590pI r13, C14820m6 r14, AnonymousClass12I r15, C231410n r16, C14850m9 r17, C15510nN r18, AnonymousClass16I r19) {
        Context context = r13.A00;
        File databasePath = context.getDatabasePath("msgstore.db");
        this.A07 = databasePath;
        boolean A07 = r17.A07(1350);
        C29681Uf r3 = new C29681Uf();
        r3.A01 = true;
        r3.A02 = A07;
        r3.A00 = r14.A00.getBoolean("force_db_check", false);
        this.A05 = new C29561To(context, new C29621Ty(), new C29691Ug(r3), r16, databasePath, Collections.singleton(new C29701Uh(r14, r15, this, r18)));
        this.A02 = r11;
        this.A03 = r12;
        ReentrantReadWriteLock reentrantReadWriteLock = new ReentrantReadWriteLock();
        this.A09 = reentrantReadWriteLock;
        this.A08 = reentrantReadWriteLock.writeLock();
        this.A06 = r19;
        this.A04 = new AnonymousClass1HL();
    }

    public int A00() {
        int i;
        String str;
        A04();
        C29561To r4 = this.A05;
        A04();
        ReentrantReadWriteLock.ReadLock readLock = this.A09.readLock();
        try {
            readLock.lock();
            synchronized (r4.A08) {
                Integer num = r4.A01;
                if (num == null) {
                    C28181Ma r6 = new C28181Ma("databasehelper/getInitialMessageCount");
                    int i2 = 0;
                    try {
                    } catch (SQLiteDatabaseCorruptException e) {
                        Log.w("databasehelper/getInitialMessageCount/dbcorrupt", e);
                    } catch (SQLiteException e2) {
                        if (e2.toString().contains("file is encrypted")) {
                            Log.w("databasehelper/getInitialMessageCount/encrypted-file-error", e2);
                        } else {
                            throw e2;
                        }
                    }
                    synchronized (r4) {
                        C16330op r2 = r4.A00;
                        if (r2 == null) {
                            i = 0;
                        } else {
                            try {
                                StringBuilder sb = new StringBuilder();
                                sb.append("SELECT COUNT(*) FROM ");
                                if (r4.A06(r4.A00).booleanValue()) {
                                    str = "message";
                                } else {
                                    str = "messages";
                                }
                                sb.append(str);
                                Cursor A09 = r2.A09(sb.toString(), null);
                                if (A09 != null) {
                                    try {
                                        if (A09.moveToNext()) {
                                            i2 = A09.getInt(0);
                                            if (i2 > 0) {
                                                i2--;
                                            }
                                            StringBuilder sb2 = new StringBuilder();
                                            sb2.append("databasehelper/getInitialMessageCount ");
                                            sb2.append(i2);
                                            sb2.append(" | time spent:");
                                            sb2.append(r6.A01());
                                            Log.i(sb2.toString());
                                        }
                                        A09.close();
                                    } catch (Throwable th) {
                                        try {
                                            A09.close();
                                        } catch (Throwable unused) {
                                        }
                                        throw th;
                                    }
                                }
                            } catch (SQLiteFullException e3) {
                                r4.A08(e3);
                                throw e3;
                            } catch (SQLiteException e4) {
                                if (e4.toString().contains("file is encrypted")) {
                                    Log.w("databasehelper/getInitialMessageCount/cursor/encrypted-file-error");
                                } else {
                                    throw e4;
                                }
                            }
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append("databasehelper/getInitialMessageCount/nocursor | time spent:");
                            sb3.append(r6.A01());
                            Log.i(sb3.toString());
                            Integer valueOf = Integer.valueOf(i2);
                            r4.A01 = valueOf;
                            i = valueOf.intValue();
                        }
                    }
                } else {
                    i = num.intValue();
                }
            }
            return i;
        } finally {
            readLock.unlock();
        }
    }

    /* renamed from: A01 */
    public C16310on get() {
        A04();
        ReentrantReadWriteLock.ReadLock readLock = this.A09.readLock();
        A04();
        return new C16310on(this.A02, this.A05, readLock, false);
    }

    public C16310on A02() {
        A04();
        ReentrantReadWriteLock.ReadLock readLock = this.A09.readLock();
        A04();
        return new C16310on(this.A02, this.A05, readLock, true);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        if (r1.getReadHoldCount() == 0) goto L_0x000f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A03() {
        /*
            r2 = this;
            java.util.concurrent.locks.ReentrantReadWriteLock r1 = r2.A09
            int r0 = r1.getWriteHoldCount()
            if (r0 > 0) goto L_0x000f
            int r1 = r1.getReadHoldCount()
            r0 = 0
            if (r1 != 0) goto L_0x0010
        L_0x000f:
            r0 = 1
        L_0x0010:
            X.AnonymousClass009.A0F(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C16490p7.A03():void");
    }

    public void A04() {
        AnonymousClass1HL r2 = this.A04;
        if (r2.A06()) {
            r2.A04(new Callable() { // from class: X.1Ui
                @Override // java.util.concurrent.Callable
                public final Object call() {
                    C16490p7 r1 = C16490p7.this;
                    C15570nT r0 = r1.A03;
                    r0.A08();
                    if (r0.A00 != null) {
                        return null;
                    }
                    r1.A04.A01();
                    return null;
                }
            });
            if (r2.A05()) {
                A07(null, true);
            }
            r2.A00();
        }
    }

    public final void A05() {
        C29561To r0 = this.A05;
        r0.close();
        File file = r0.A07;
        boolean delete = file.delete() & AnonymousClass1Tx.A04(file, "databasehelper");
        StringBuilder sb = new StringBuilder("msgstore-manager/deletedb/result/");
        sb.append(delete);
        Log.i(sb.toString());
    }

    public final void A06() {
        Log.i("msgstore-manager/setup");
        AnonymousClass009.A0F(this.A08.isHeldByCurrentThread());
        synchronized (this) {
            C29561To r5 = this.A05;
            File file = r5.A07;
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            file.delete();
            AnonymousClass1Tx.A04(file, "databasehelper");
            C14350lI.A0E(file, "msgstore/create-db/list ");
            SQLiteDatabase openDatabase = SQLiteDatabase.openDatabase(file.getAbsolutePath(), null, 805306384);
            AnonymousClass009.A05(openDatabase);
            r5.onCreate(openDatabase);
            C14350lI.A0E(file, "msgstore/create-db/done/list ");
            r5.AHr();
            this.A01 = true;
        }
    }

    public boolean A07(C249317l r6, boolean z) {
        boolean z2;
        Log.i("msgstore-manager/checkhealth");
        ReentrantReadWriteLock.WriteLock writeLock = this.A08;
        writeLock.lock();
        synchronized (this) {
            if (z) {
                AnonymousClass1HL r4 = this.A04;
                synchronized (r4) {
                    z2 = false;
                    if (r4.A00 > 3) {
                        z2 = true;
                    }
                }
                if (z2) {
                    boolean z3 = this.A01;
                    writeLock.unlock();
                    r4.A01();
                    if (this.A01 && r6 != null) {
                        r6.A01();
                    }
                    return z3;
                }
            }
            if (!this.A01) {
                this.A04.A03();
                File file = this.A07;
                String parent = file.getParent();
                StringBuilder sb = new StringBuilder();
                sb.append(file.getName());
                sb.append("-journal");
                boolean delete = new File(parent, sb.toString()).delete();
                StringBuilder sb2 = new StringBuilder();
                sb2.append("msgstore-manager/checkhealth/journal/delete ");
                sb2.append(delete);
                Log.i(sb2.toString());
                String parent2 = file.getParent();
                StringBuilder sb3 = new StringBuilder();
                sb3.append(file.getName());
                sb3.append(".back");
                boolean delete2 = new File(parent2, sb3.toString()).delete();
                StringBuilder sb4 = new StringBuilder();
                sb4.append("msgstore-manager/checkhealth/back/delete ");
                sb4.append(delete2);
                Log.i(sb4.toString());
                try {
                    this.A05.AHr();
                    this.A01 = true;
                    this.A00 = true;
                } catch (SQLiteDatabaseCorruptException e) {
                    Log.e("msgstore-manager/checkhealth ", e);
                    A05();
                } catch (SQLiteException e2) {
                    Log.w("msgstore-manager/checkhealth no db", e2);
                }
            }
            writeLock.unlock();
            this.A04.A01();
            if (this.A01 && r6 != null) {
                r6.A01();
            }
            A04();
            return this.A01;
        }
    }
}
