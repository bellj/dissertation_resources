package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5lf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122565lf extends AbstractC118835cS {
    public final TextView A00;

    public C122565lf(View view) {
        super(view);
        this.A00 = C12960it.A0I(view, R.id.text_view);
    }

    @Override // X.AbstractC118835cS
    public void A08(AbstractC125975s7 r3, int i) {
        C123135mf r32 = (C123135mf) r3;
        if (r32 != null) {
            TextView textView = this.A00;
            textView.setText(r32.A01);
            textView.setVisibility(r32.A00);
        }
    }
}
