package X;

import java.io.IOException;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

/* renamed from: X.123  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass123 {
    public static String A00(Map map) {
        AnonymousClass1NG r1 = new AnonymousClass1NG(map);
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-256");
            do {
            } while (new DigestInputStream(r1, instance).read() != -1);
            return C003501n.A04(instance.digest());
        } catch (IOException | NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }
}
