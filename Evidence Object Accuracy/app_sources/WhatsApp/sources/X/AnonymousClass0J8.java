package X;

/* renamed from: X.0J8  reason: invalid class name */
/* loaded from: classes.dex */
public enum AnonymousClass0J8 {
    all,
    /* Fake field, exist only in values array */
    aural,
    /* Fake field, exist only in values array */
    braille,
    /* Fake field, exist only in values array */
    embossed,
    /* Fake field, exist only in values array */
    handheld,
    /* Fake field, exist only in values array */
    print,
    /* Fake field, exist only in values array */
    projection,
    screen,
    /* Fake field, exist only in values array */
    speech,
    /* Fake field, exist only in values array */
    tty,
    /* Fake field, exist only in values array */
    tv
}
