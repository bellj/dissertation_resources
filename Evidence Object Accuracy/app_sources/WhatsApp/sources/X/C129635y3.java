package X;

/* renamed from: X.5y3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C129635y3 {
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final String A04;

    public C129635y3(String str, int i, int i2, int i3, int i4) {
        this.A02 = i;
        this.A01 = i2;
        this.A04 = str;
        this.A03 = i3;
        this.A00 = i4;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof C129635y3) {
            C129635y3 r4 = (C129635y3) obj;
            if (this.A02 == r4.A02 && this.A01 == r4.A01 && this.A03 == r4.A03 && this.A00 == r4.A00) {
                return this.A04.equals(r4.A04);
            }
        }
        return false;
    }

    public int hashCode() {
        return (((((((this.A02 * 31) + this.A01) * 31) + this.A04.hashCode()) * 31) + this.A03) * 31) + this.A00;
    }
}
