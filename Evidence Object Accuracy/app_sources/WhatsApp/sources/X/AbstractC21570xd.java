package X;

import android.content.ContentValues;
import android.database.Cursor;
import java.util.Locale;

/* renamed from: X.0xd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC21570xd {
    public final C232010t A00;

    public AbstractC21570xd(C232010t r3) {
        this.A00 = r3;
        r3.A02.A03(new AnonymousClass1VT(this));
    }

    public static long A00(ContentValues contentValues, C16310on r3, String str) {
        C16330op r1 = r3.A03;
        A05("ContactProvider/insertOrReplace/INSERT_", str);
        return r1.A03(contentValues, str);
    }

    public static long A01(ContentValues contentValues, C16310on r3, String str, String str2, String[] strArr) {
        C16330op r1 = r3.A03;
        A05("ContactProvider/delete/UPDATE_", str);
        return (long) r1.A00(str, contentValues, str2, strArr);
    }

    public static long A02(C16310on r1, String str, String str2, String[] strArr) {
        C16330op r12 = r1.A03;
        A05("ContactProvider/delete/DELETE_", str);
        return (long) r12.A01(str, str2, strArr);
    }

    public static Cursor A03(C16310on r2, String str, String str2, String str3, String str4, String[] strArr, String[] strArr2) {
        C16330op r22 = r2.A03;
        StringBuilder sb = new StringBuilder("ContactProvider/query/QUERY_");
        sb.append(str4);
        sb.toString();
        return r22.A08(str, str2, str3, null, strArr, strArr2);
    }

    public static void A04(ContentValues contentValues, C16310on r3, String str) {
        C16330op r1 = r3.A03;
        A05("ContactProvider/insertOrReplace/REPLACE_", str);
        r1.A05(contentValues, str);
    }

    public static void A05(String str, String str2) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(str2.toUpperCase(Locale.ROOT));
        sb.toString();
    }
}
