package X;

import android.content.Context;

/* renamed from: X.0Rt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05990Rt {
    public static C05990Rt A04;
    public C03140Gg A00;
    public C03150Gh A01;
    public C03160Gk A02;
    public AnonymousClass0Gi A03;

    public C05990Rt(Context context, AbstractC11500gO r4) {
        Context applicationContext = context.getApplicationContext();
        this.A00 = new C03140Gg(applicationContext, r4);
        this.A01 = new C03150Gh(applicationContext, r4);
        this.A02 = new C03160Gk(applicationContext, r4);
        this.A03 = new AnonymousClass0Gi(applicationContext, r4);
    }

    public static synchronized C05990Rt A00(Context context, AbstractC11500gO r3) {
        C05990Rt r0;
        synchronized (C05990Rt.class) {
            r0 = A04;
            if (r0 == null) {
                r0 = new C05990Rt(context, r3);
                A04 = r0;
            }
        }
        return r0;
    }
}
