package X;

import android.graphics.drawable.ColorDrawable;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaMediaThumbnailView;

/* renamed from: X.2ha  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C54972ha extends AnonymousClass03U {
    public final int A00;
    public final int A01;
    public final ColorDrawable A02;
    public final WaMediaThumbnailView A03;
    public final C457522x A04;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C54972ha(View view, C457522x r4) {
        super(view);
        C16700pc.A0E(r4, 2);
        this.A04 = r4;
        this.A00 = view.getResources().getDimensionPixelSize(R.dimen.gallery_selected_media_image_size);
        int A00 = AnonymousClass00T.A00(view.getContext(), R.color.camera_thumb);
        this.A01 = A00;
        this.A02 = new ColorDrawable(A00);
        this.A03 = (WaMediaThumbnailView) C16700pc.A03(view, R.id.selected_media_item_thumbnail);
    }
}
