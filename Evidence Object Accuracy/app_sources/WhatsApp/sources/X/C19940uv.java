package X;

import com.whatsapp.util.Log;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.0uv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19940uv {
    public AtomicInteger A00 = new AtomicInteger();
    public boolean A01 = false;
    public final AbstractC15710nm A02;
    public final C14850m9 A03;

    public C19940uv(AbstractC15710nm r2, C14850m9 r3) {
        this.A03 = r3;
        this.A02 = r2;
    }

    public synchronized void A00() {
        if (this.A00.addAndGet(1) > 15) {
            Log.i("Disable WATLS stack.");
            this.A01 = true;
        }
    }

    public synchronized boolean A01() {
        boolean z;
        if (this.A01) {
            z = false;
        } else {
            z = this.A03.A07(48);
        }
        return z;
    }

    public boolean A02(Throwable th) {
        return th.getMessage() != null && th.getMessage().contains("WATLS Exception");
    }
}
