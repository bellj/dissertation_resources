package X;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.IntentSender;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1ni  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38211ni {
    public static Intent A00(Intent intent, AnonymousClass1IS r5) {
        if (!intent.hasExtra("fMessageKeyJid") && !intent.hasExtra("fMessageKeyFromMe") && !intent.hasExtra("fMessageKeyId")) {
            return intent.putExtra("fMessageKeyId", r5.A01).putExtra("fMessageKeyFromMe", r5.A02).putExtra("fMessageKeyJid", C15380n4.A03(r5.A00));
        }
        throw new IllegalArgumentException("Intent already contains key.");
    }

    public static Intent A01(IntentSender intentSender, CharSequence charSequence, List list) {
        Intent intent;
        int i;
        int i2 = 0;
        Intent intent2 = (Intent) list.get(0);
        if (Build.VERSION.SDK_INT < 22 || intentSender == null) {
            intent = Intent.createChooser(intent2, charSequence);
        } else {
            intent = Intent.createChooser(intent2, charSequence, intentSender);
        }
        int size = list.size() - 1;
        if (size > 0) {
            Parcelable[] parcelableArr = new Intent[size];
            do {
                i = i2 + 1;
                parcelableArr[i2] = list.get(i);
                i2 = i;
            } while (i < size);
            intent.putExtra("android.intent.extra.INITIAL_INTENTS", parcelableArr);
        }
        return intent;
    }

    public static AnonymousClass1IS A02(Intent intent) {
        if (!intent.hasExtra("fMessageKeyJid") || !intent.hasExtra("fMessageKeyFromMe") || !intent.hasExtra("fMessageKeyId")) {
            return null;
        }
        return new AnonymousClass1IS(AbstractC14640lm.A01(intent.getStringExtra("fMessageKeyJid")), intent.getStringExtra("fMessageKeyId"), intent.getBooleanExtra("fMessageKeyFromMe", false));
    }

    public static AnonymousClass1IS A03(Bundle bundle, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("fMessageKeyJid");
        String obj = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append(str);
        sb2.append("fMessageKeyFromMe");
        String obj2 = sb2.toString();
        StringBuilder sb3 = new StringBuilder();
        sb3.append(str);
        sb3.append("fMessageKeyId");
        String obj3 = sb3.toString();
        if (!bundle.containsKey(obj) || !bundle.containsKey(obj2) || !bundle.containsKey(obj3)) {
            return null;
        }
        return new AnonymousClass1IS(AbstractC14640lm.A01(bundle.getString(obj)), bundle.getString(obj3), bundle.getBoolean(obj2, false));
    }

    public static List A04(Bundle bundle) {
        if (!bundle.containsKey("fMessageKeyJidArray") || !bundle.containsKey("fMessageKeyFromMeArray") || !bundle.containsKey("fMessageKeyIdArray")) {
            return null;
        }
        String[] stringArray = bundle.getStringArray("fMessageKeyIdArray");
        AnonymousClass009.A05(stringArray);
        boolean[] booleanArray = bundle.getBooleanArray("fMessageKeyFromMeArray");
        AnonymousClass009.A05(booleanArray);
        String[] stringArray2 = bundle.getStringArray("fMessageKeyJidArray");
        AnonymousClass009.A05(stringArray2);
        int length = stringArray.length;
        ArrayList arrayList = new ArrayList(length);
        for (int i = 0; i < length; i++) {
            arrayList.add(new AnonymousClass1IS(AbstractC14640lm.A01(stringArray2[i]), stringArray[i], booleanArray[i]));
        }
        return arrayList;
    }

    public static void A05(Activity activity) {
        A06(activity, "com.whatsapp");
    }

    public static void A06(Activity activity, String str) {
        try {
            Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
            intent.setData(Uri.fromParts("package", str, null));
            activity.startActivity(intent);
        } catch (ActivityNotFoundException unused) {
            activity.startActivity(new Intent("android.settings.SETTINGS"));
        }
    }

    public static void A07(Intent intent) {
        String action = intent.getAction();
        if (action != null) {
            StringBuilder sb = new StringBuilder("com.whatsapp");
            sb.append(".intent.action.");
            if (action.startsWith(sb.toString()) && !"gigaset".equalsIgnoreCase(Build.MANUFACTURER)) {
                intent.setPackage("com.whatsapp");
            }
        }
    }

    public static void A08(Bundle bundle, AnonymousClass1IS r5, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("fMessageKeyJid");
        String obj = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append(str);
        sb2.append("fMessageKeyFromMe");
        String obj2 = sb2.toString();
        StringBuilder sb3 = new StringBuilder();
        sb3.append(str);
        sb3.append("fMessageKeyId");
        String obj3 = sb3.toString();
        if (bundle.containsKey(obj3) || bundle.containsKey(obj2) || bundle.containsKey(obj3)) {
            throw new IllegalArgumentException("Bundle already contains key.");
        }
        bundle.putString(obj3, r5.A01);
        bundle.putBoolean(obj2, r5.A02);
        bundle.putString(obj, C15380n4.A03(r5.A00));
    }

    public static void A09(Bundle bundle, Collection collection) {
        if (bundle.containsKey("fMessageKeyJidArray") || bundle.containsKey("fMessageKeyFromMeArray") || bundle.containsKey("fMessageKeyIdArray")) {
            throw new IllegalArgumentException("Bundle already contains list of keys.");
        }
        String[] strArr = new String[collection.size()];
        boolean[] zArr = new boolean[collection.size()];
        String[] strArr2 = new String[collection.size()];
        int i = 0;
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            AnonymousClass1IS r1 = (AnonymousClass1IS) it.next();
            strArr[i] = r1.A01;
            zArr[i] = r1.A02;
            strArr2[i] = C15380n4.A03(r1.A00);
            i++;
        }
        bundle.putStringArray("fMessageKeyIdArray", strArr);
        bundle.putBooleanArray("fMessageKeyFromMeArray", zArr);
        bundle.putStringArray("fMessageKeyJidArray", strArr2);
    }
}
