package X;

import android.content.Context;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/* renamed from: X.0BC  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0BC extends ViewGroup implements AbstractC016507u {
    public int A00 = -1;
    public int A01;
    public View A02;
    public AnonymousClass0LS A03;
    public AnonymousClass0P3 A04;
    public C04560Me A05;
    public AbstractC12080hL A06;
    public C06210Sp A07;
    public List A08;
    public boolean A09 = true;
    public boolean A0A = true;
    public boolean A0B;
    public boolean A0C = true;
    public boolean A0D;
    public boolean A0E;
    public boolean A0F = true;
    public AbstractC12080hL[] A0G;
    public final C04740Mw A0H = new C04740Mw();
    public final boolean A0I = true;

    public AnonymousClass0BC(Context context) {
        super(context);
        AnonymousClass0HK r2 = new AnonymousClass0HK(this);
        if (this.A07 == null) {
            this.A07 = new C06210Sp(getContext(), this, r2);
        }
    }

    public final AbstractC12080hL A00(View view, List list, int i, int i2) {
        int abs;
        AbstractC12080hL[] r6 = this.A0G;
        AbstractC12080hL r5 = null;
        if (!(r6 == null || view == null)) {
            int i3 = Integer.MAX_VALUE;
            for (AbstractC12080hL r1 : r6) {
                if (!list.contains(r1) && (abs = Math.abs(r1.AFn(view, i2) - i)) < i3) {
                    r5 = r1;
                    i3 = abs;
                }
            }
        }
        return r5;
    }

    public void A01(AbstractC12080hL r4, int i) {
        View view = this.A02;
        if (view != null) {
            this.A06 = r4;
            RunnableC09990dq r2 = new RunnableC09990dq(view, this, i);
            if (AnonymousClass028.A0r(this)) {
                r2.run();
                return;
            }
            ViewTreeObserver viewTreeObserver = getViewTreeObserver();
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver$OnGlobalLayoutListenerC06960Wd(this, viewTreeObserver, r2));
        }
    }

    public void A02(AbstractC12080hL[] r5, boolean z) {
        this.A0G = (AbstractC12080hL[]) Arrays.copyOf(r5, r5.length);
        AbstractC12080hL r1 = this.A06;
        if (r1 != null) {
            if (this.A02 == null) {
                this.A06 = null;
                r1 = null;
            } else {
                int height = getHeight();
                List emptyList = Collections.emptyList();
                View view = this.A02;
                r1 = A00(view, emptyList, this.A06.AFn(view, height), height);
                this.A06 = r1;
            }
        }
        if (z) {
            if (r1 == null) {
                List emptyList2 = Collections.emptyList();
                View view2 = this.A02;
                if (view2 != null && AnonymousClass028.A0r(view2)) {
                    int height2 = getHeight();
                    r1 = A00(view2, emptyList2, height2 - view2.getTop(), height2);
                    if (r1 == null) {
                        return;
                    }
                } else {
                    return;
                }
            }
            A01(r1, this.A00);
        }
    }

    public final boolean A03(int i, int i2) {
        int i3;
        View view = this.A02;
        if (getNestedScrollAxes() == 1) {
            if (i2 == 0 || ((float) (Math.abs(i) / Math.abs(i2))) > 0.7f) {
                return false;
            }
        } else if (view == null) {
            return false;
        } else {
            int height = getHeight();
            AbstractC12080hL[] r1 = this.A0G;
            if (r1 == null || r1.length <= 0) {
                i3 = 0;
            } else {
                i3 = height - r1[0].AFn(view, height);
                int length = this.A0G.length;
                for (int i4 = 1; i4 < length; i4++) {
                    i3 = Math.min(i3, height - this.A0G[i4].AFn(view, height));
                }
            }
            if (view.getBottom() <= height || view.getTop() <= i3) {
                return false;
            }
        }
        return true;
    }

    @Override // android.view.ViewGroup
    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        if (getChildCount() <= 0) {
            if (this.A01 > 0) {
                view.setTop(getHeight() - this.A01);
            }
            this.A01 = 0;
            super.addView(view, i, layoutParams);
            this.A02 = view;
            return;
        }
        StringBuilder sb = new StringBuilder("BloksSlidingViewGroup");
        sb.append(" only supports a single child");
        throw new IllegalStateException(sb.toString());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0050, code lost:
        if (r3.A0A.isFinished() == false) goto L_0x0052;
     */
    @Override // android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void computeScroll() {
        /*
            r10 = this;
            X.0Sp r3 = r10.A07
            int r0 = r3.A03
            r2 = 2
            r6 = 0
            if (r0 != r2) goto L_0x0059
            android.view.View r0 = r3.A08
            if (r0 == 0) goto L_0x0060
            android.widget.Scroller r0 = r3.A0A
            boolean r1 = r0.computeScrollOffset()
            android.widget.Scroller r0 = r3.A0A
            int r7 = r0.getCurrY()
            android.view.View r0 = r3.A08
            int r0 = r0.getTop()
            int r0 = r7 - r0
            if (r0 <= 0) goto L_0x0061
            int r0 = r3.A04
            int r7 = java.lang.Math.min(r7, r0)
        L_0x0028:
            android.view.View r0 = r3.A08
            int r0 = r0.getTop()
            int r9 = r7 - r0
            if (r9 == 0) goto L_0x003f
            android.view.View r0 = r3.A08
            r0.offsetTopAndBottom(r9)
            com.bloks.foa.components.bottomsheet.ViewDragHelper$Callback r4 = r3.A0H
            android.view.View r5 = r3.A08
            r8 = 0
            r4.A05(r5, r6, r7, r8, r9)
        L_0x003f:
            if (r1 == 0) goto L_0x0052
            int r0 = r3.A04
            if (r7 != r0) goto L_0x0059
            android.widget.Scroller r0 = r3.A0A
            r0.abortAnimation()
            android.widget.Scroller r0 = r3.A0A
            boolean r0 = r0.isFinished()
            if (r0 != 0) goto L_0x0059
        L_0x0052:
            android.view.ViewGroup r1 = r3.A0G
            java.lang.Runnable r0 = r3.A0I
            r1.post(r0)
        L_0x0059:
            int r0 = r3.A03
            if (r0 != r2) goto L_0x0060
            r10.postInvalidateOnAnimation()
        L_0x0060:
            return
        L_0x0061:
            if (r0 >= 0) goto L_0x0028
            int r0 = r3.A04
            int r7 = java.lang.Math.max(r7, r0)
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0BC.computeScroll():void");
    }

    @Override // android.view.ViewGroup
    public int getNestedScrollAxes() {
        C04740Mw r0 = this.A0H;
        return r0.A01 | r0.A00;
    }

    @Override // android.view.ViewGroup
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        View A01;
        if (this.A0C) {
            if (this.A0E || !this.A09) {
                return false;
            }
            C06210Sp r3 = this.A07;
            int actionMasked = motionEvent.getActionMasked();
            int actionIndex = motionEvent.getActionIndex();
            if (actionMasked == 0) {
                r3.A02();
            }
            VelocityTracker velocityTracker = r3.A07;
            if (velocityTracker == null) {
                velocityTracker = VelocityTracker.obtain();
                r3.A07 = velocityTracker;
            }
            velocityTracker.addMovement(motionEvent);
            if (actionMasked != 0) {
                if (actionMasked != 1) {
                    if (actionMasked == 2) {
                        if (r3.A0C == null || r3.A0D == null) {
                            r3.A07(motionEvent.getPointerId(0), motionEvent.getX(), motionEvent.getY());
                        }
                        int pointerCount = motionEvent.getPointerCount();
                        for (int i = 0; i < pointerCount; i++) {
                            int pointerId = motionEvent.getPointerId(i);
                            if (((1 << pointerId) & r3.A05) != 0) {
                                motionEvent.getX(i);
                                float y = motionEvent.getY(i);
                                float[] fArr = r3.A0C;
                                float[] fArr2 = r3.A0D;
                                float f = y - fArr2[pointerId];
                                if (r3.A03 == 1) {
                                    break;
                                }
                                View A012 = r3.A01((int) fArr[pointerId], (int) fArr2[pointerId]);
                                if (A012 != null && r3.A0H.A00(A012) > 0 && Math.abs(f) > ((float) r3.A06) && r3.A0B(A012, pointerId)) {
                                    break;
                                }
                            }
                        }
                        r3.A09(motionEvent);
                    } else if (actionMasked != 3) {
                        if (actionMasked == 5) {
                            int pointerId2 = motionEvent.getPointerId(actionIndex);
                            float x = motionEvent.getX(actionIndex);
                            float y2 = motionEvent.getY(actionIndex);
                            r3.A07(pointerId2, x, y2);
                            if (r3.A03 == 2 && (A01 = r3.A01((int) x, (int) y2)) == r3.A08) {
                                r3.A0B(A01, pointerId2);
                            }
                        } else if (actionMasked == 6) {
                            r3.A06(motionEvent.getPointerId(actionIndex));
                        }
                    }
                }
                r3.A02();
            } else {
                float x2 = motionEvent.getX();
                float y3 = motionEvent.getY();
                int pointerId3 = motionEvent.getPointerId(0);
                r3.A07(pointerId3, x2, y3);
                View A013 = r3.A01((int) x2, (int) y3);
                if (A013 == r3.A08 && r3.A03 == 2) {
                    r3.A0B(A013, pointerId3);
                }
            }
            if (r3.A03 != 1) {
                return false;
            }
        }
        return true;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int height;
        AbstractC12080hL r1;
        View view = this.A02;
        if (view != null) {
            if (!this.A0F) {
                height = view.getTop();
            } else {
                height = getHeight();
            }
            int measuredHeight = view.getMeasuredHeight() + height;
            if (this.A0I) {
                measuredHeight = Math.max(getHeight(), measuredHeight);
            }
            view.layout(0, height, getWidth(), measuredHeight);
            boolean z2 = false;
            if (view.getMeasuredHeight() != this.A01) {
                z2 = true;
            }
            this.A01 = view.getMeasuredHeight();
            if ((z || z2) && (r1 = this.A06) != null) {
                A01(r1, this.A00);
            }
            AnonymousClass0P3 r12 = this.A04;
            if (r12 != null) {
                r12.A00(view, getHeight());
            }
            this.A0F = false;
        }
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        int i3;
        super.onMeasure(i, i2);
        measureChildren(i, i2);
        if (this.A0B) {
            View view = this.A02;
            if (this.A0G == null || view == null) {
                i3 = 0;
            } else {
                int measuredHeight = getMeasuredHeight();
                i3 = 0;
                for (AbstractC12080hL r0 : this.A0G) {
                    i3 = Math.max(i3, r0.AFn(view, measuredHeight));
                }
            }
            measureChildren(i, View.MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE));
        }
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, X.AbstractC016507u
    public boolean onNestedFling(View view, float f, float f2, boolean z) {
        if (!this.A09 || z) {
            return false;
        }
        C06210Sp r5 = this.A07;
        View view2 = this.A02;
        r5.A08 = view2;
        r5.A0B = true;
        r5.A0H.A04(view2, 0.0f, -f2);
        r5.A0B = false;
        if (r5.A03 != 1) {
            return true;
        }
        r5.A05(0);
        return true;
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, X.AbstractC016507u
    public boolean onNestedPreFling(View view, float f, float f2) {
        if (!this.A09 || !A03((int) f, (int) f2)) {
            return false;
        }
        C06210Sp r5 = this.A07;
        View view2 = this.A02;
        r5.A08 = view2;
        r5.A0B = true;
        r5.A0H.A04(view2, 0.0f, -f2);
        r5.A0B = false;
        if (r5.A03 != 1) {
            return true;
        }
        r5.A05(0);
        return true;
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, X.AbstractC016507u
    public void onNestedPreScroll(View view, int i, int i2, int[] iArr) {
        if (this.A09 && A03(i, i2)) {
            C06210Sp r5 = this.A07;
            int i3 = -i;
            int i4 = -i2;
            View view2 = r5.A08;
            if (view2 != null) {
                int left = view2.getLeft() + i3;
                int top = r5.A08.getTop() + i4;
                r5.A08(i3, i4);
                View view3 = r5.A08;
                if (!(view3 == null || iArr == null)) {
                    iArr[0] = (left - view3.getLeft()) - i3;
                    iArr[1] = (top - r5.A08.getTop()) - i4;
                }
            }
            if (getNestedScrollAxes() == 1 && iArr[1] != 0) {
                iArr[0] = i;
            }
        }
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, X.AbstractC016507u
    public void onNestedScroll(View view, int i, int i2, int i3, int i4) {
        if (this.A09) {
            C06210Sp r3 = this.A07;
            int i5 = -i3;
            int i6 = -i4;
            View view2 = r3.A08;
            if (view2 != null) {
                view2.getLeft();
                r3.A08.getTop();
                r3.A08(i5, i6);
            }
        }
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, X.AbstractC016507u
    public void onNestedScrollAccepted(View view, View view2, int i) {
        this.A0H.A01 = i;
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, X.AbstractC016507u
    public boolean onStartNestedScroll(View view, View view2, int i) {
        boolean z = false;
        if (!this.A0E) {
            z = true;
            this.A0E = true;
            C06210Sp r2 = this.A07;
            View view3 = this.A02;
            if (r2.A07 == null) {
                r2.A07 = VelocityTracker.obtain();
            }
            r2.A05(1);
            r2.A08 = view3;
        }
        return z;
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, X.AbstractC016507u
    public void onStopNestedScroll(View view) {
        this.A0H.A01 = 0;
        this.A0E = false;
        C06210Sp r2 = this.A07;
        r2.A08 = this.A02;
        if (r2.A03 != 2) {
            r2.A04(0.0f, 0.0f);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0074, code lost:
        if (r5.A02 == -1) goto L_0x0076;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0076, code lost:
        r0 = r5.A07;
        r8 = r5.A00;
        r0.computeCurrentVelocity(1000, r8);
        r0 = r5.A07.getXVelocity(r5.A02);
        r3 = r5.A01;
        r5.A04(X.C06210Sp.A00(r0, r3, r8), X.C06210Sp.A00(r5.A07.getYVelocity(r5.A02), r3, r8));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00d6, code lost:
        if (r2 > ((float) r6.getBottom())) goto L_0x00d8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00df, code lost:
        if (r1 != 3) goto L_0x00e1;
     */
    @Override // android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r12) {
        /*
        // Method dump skipped, instructions count: 556
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0BC.onTouchEvent(android.view.MotionEvent):boolean");
    }

    public void setInteractable(boolean z) {
        this.A0C = z;
        if (!z) {
            this.A07.A02();
        }
    }
}
