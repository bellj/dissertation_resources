package X;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.22V  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass22V extends AbstractC44461z0 {
    public final File A00;
    public final Set A01 = new HashSet();

    public AnonymousClass22V(File file) {
        super(file, 100, 524288000);
        this.A00 = file;
    }
}
