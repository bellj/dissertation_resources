package X;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.text.TextUtils;
import android.util.Patterns;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.whatsapp.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* renamed from: X.0lF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14320lF {
    public static final Set A0L = new HashSet(Arrays.asList("og:image", "og:image:type", "og:video", "og:video:type", "og:video:url", "og:video:secure_url", "og:video:width", "og:video:height", "image", "og:thumbnail", "thumbnail", "twitter:image", "og:title", "title", "twitter:title", "og:description", "description", "twitter:description", "og:url", "og:site_name", "twitter:url", "invite_link_type_v2", "parent_group_subject", "wa:artist", "wa:song"));
    public static final Pattern A0M = Pattern.compile("(?i)\\bcharset=\\s*\"?([^\\s;\"]*)", 34);
    public static final Pattern A0N = Pattern.compile("<head[^>]*>(.*)</head>", 34);
    public static final Pattern A0O = Pattern.compile("\\s*([^=]+)\\s*=\\s*(?:\"([^\"]+)\"|\\'([^\\']+)\\')", 34);
    public static final Pattern A0P = Pattern.compile("<link([^>]+?)/?>", 34);
    public static final Pattern A0Q = Pattern.compile("<meta([^>]+?)/?>", 34);
    public static final Pattern A0R = Pattern.compile("<title[^>]*>(.*)</title>", 34);
    public static final Pattern A0S = Pattern.compile("https://(www\\.)?pbs\\.twimg\\.com/profile_images", 2);
    public static final Pattern A0T = Pattern.compile("https://(www\\.)?pbs\\.twimg\\.com", 2);
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public long A04;
    public Bitmap A05;
    public AnonymousClass2V4 A06;
    public AnonymousClass3JH A07;
    public Integer A08 = null;
    public String A09;
    public String A0A = null;
    public String A0B;
    public String A0C;
    public String A0D;
    public String A0E;
    public LinkedHashSet A0F = new LinkedHashSet();
    public boolean A0G;
    public byte[] A0H;
    public byte[] A0I;
    public final C18790t3 A0J;
    public final String A0K;

    public C14320lF(C18790t3 r2, String str) {
        this.A0K = str;
        this.A0J = r2;
    }

    public static String A00(HashMap hashMap, String... strArr) {
        for (String str : strArr) {
            String str2 = (String) hashMap.get(str);
            if (str2 != null) {
                return str2.trim();
            }
        }
        return null;
    }

    public static HashMap A01(String str) {
        HashMap hashMap = new HashMap();
        Matcher matcher = A0O.matcher(str);
        while (matcher.find()) {
            String group = matcher.group(1);
            String group2 = matcher.group(2);
            if (group2 == null) {
                group2 = matcher.group(3);
            }
            hashMap.put(group, group2);
        }
        return hashMap;
    }

    public static final byte[] A02(Bitmap bitmap, Rect rect, int i, int i2, int i3) {
        Bitmap.Config config = bitmap.getConfig();
        if (config == null) {
            config = Bitmap.Config.ARGB_8888;
        }
        Canvas canvas = new Canvas(Bitmap.createBitmap(i, i2, config));
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        canvas.drawColor(-1);
        canvas.drawBitmap(bitmap, rect, new Rect(0, 0, i, i2), paint);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, i3, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public final AnonymousClass2V4 A03(String str, String str2, int i) {
        HttpURLConnection A06;
        if (!"image/gif".equals(str2) || i == -1) {
            return null;
        }
        if (str.endsWith("giphy.gif")) {
            try {
                if (new URI(str).getHost().contains("giphy.com")) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(str.substring(0, str.length() - 9));
                    sb.append("200.mp4");
                    str = sb.toString();
                    try {
                        A06 = A06(null, new URL(str));
                        if (A06 != null) {
                            String headerField = A06.getHeaderField("Content-Type");
                            if (headerField.equals("video/mp4")) {
                                AnonymousClass2V4 r3 = new AnonymousClass2V4(str, headerField, A06.getContentLength());
                                A0A(A06);
                                return r3;
                            }
                        }
                    } catch (IOException e) {
                        Log.w("WebPageInfo/checkForMp4 Cannot connect.", e);
                    }
                    A0A(A06);
                }
            } catch (URISyntaxException e2) {
                Log.w("WebPageInfo/getGifInfo Cannot connect.", e2);
            }
        }
        return new AnonymousClass2V4(str, str2, i);
    }

    public final AnonymousClass2V4 A04(Map map, String... strArr) {
        Throwable th;
        HttpURLConnection httpURLConnection;
        IOException e;
        HttpURLConnection httpURLConnection2 = null;
        for (String str : strArr) {
            String str2 = (String) map.get(str);
            if (str2 != null && Patterns.WEB_URL.matcher(str2).matches()) {
                try {
                    httpURLConnection = A06(null, new URL(str2));
                    if (httpURLConnection != null) {
                        try {
                            try {
                                AnonymousClass2V4 A03 = A03(str2, httpURLConnection.getHeaderField("Content-Type"), httpURLConnection.getContentLength());
                                if (A03 != null) {
                                    A0A(httpURLConnection);
                                    return A03;
                                }
                            } catch (IOException e2) {
                                e = e2;
                                Log.w("WebPageInfo/getGifOnPage Cannot connect.", e);
                                A0A(httpURLConnection);
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            httpURLConnection2 = httpURLConnection;
                            A0A(httpURLConnection2);
                            throw th;
                        }
                    }
                } catch (IOException e3) {
                    e = e3;
                    httpURLConnection = null;
                } catch (Throwable th3) {
                    th = th3;
                }
                A0A(httpURLConnection);
            }
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:149:0x03cb, code lost:
        if (android.text.TextUtils.isEmpty(com.whatsapp.acceptinvitelink.AcceptInviteLinkActivity.A02(android.net.Uri.parse(r4))) != false) goto L_0x03cd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0144, code lost:
        if ("video/mp4".equalsIgnoreCase((java.lang.String) r3.get("og:video:type")) == false) goto L_0x0146;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String A05(java.lang.String r18, java.lang.String r19, java.lang.String r20, java.net.URL r21, int r22) {
        /*
        // Method dump skipped, instructions count: 1180
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14320lF.A05(java.lang.String, java.lang.String, java.lang.String, java.net.URL, int):java.lang.String");
    }

    public final HttpURLConnection A06(String str, URL url) {
        URL url2 = null;
        try {
            url2 = new URL(new URI(url.toString()).toASCIIString());
        } catch (MalformedURLException | URISyntaxException unused) {
        }
        if (url2 == null) {
            return null;
        }
        URLConnection openConnection = url2.openConnection();
        if (!(openConnection instanceof HttpURLConnection)) {
            return null;
        }
        HttpURLConnection httpURLConnection = (HttpURLConnection) openConnection;
        httpURLConnection.setConnectTimeout(SearchActionVerificationClientService.NOTIFICATION_ID);
        httpURLConnection.setReadTimeout(SearchActionVerificationClientService.NOTIFICATION_ID);
        if (str != null) {
            httpURLConnection.setRequestProperty("Accept-Language", str);
        }
        StringBuilder sb = new StringBuilder("WhatsApp/");
        sb.append("2.22.17.70".replace(' ', '_'));
        sb.append(" A");
        httpURLConnection.setRequestProperty("User-Agent", sb.toString());
        return httpURLConnection;
    }

    public void A07() {
        this.A0E = null;
        this.A0B = null;
        this.A09 = null;
        this.A0F.clear();
        this.A0G = false;
        this.A02 = 0;
    }

    public void A08(String str) {
        int min;
        int i;
        this.A04 = 0;
        long currentTimeMillis = System.currentTimeMillis();
        if (!TextUtils.isEmpty(str)) {
            URL url = new URL(str);
            if (str.equals(URLDecoder.decode(str, AnonymousClass01V.A08))) {
                url = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef()).toURL();
            }
            HttpURLConnection A06 = A06(null, url);
            if (A06 != null) {
                A06.setConnectTimeout(SearchActionVerificationClientService.NOTIFICATION_ID);
                A06.setReadTimeout(20000);
                AnonymousClass1oJ r9 = new AnonymousClass1oJ(this.A0J, A06.getInputStream(), null, 23);
                try {
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    byte[] bArr = new byte[EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH];
                    int i2 = 0;
                    do {
                        int read = r9.read(bArr, 0, EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH);
                        if (read == -1) {
                            byte[] byteArray = byteArrayOutputStream.toByteArray();
                            byteArrayOutputStream.close();
                            r9.close();
                            this.A0H = A0D(byteArray, 100, 100, 140, 140, false);
                            if (TextUtils.isEmpty(str) || !A0S.matcher(C33771f3.A03(str, C33771f3.A03)).find() || this.A03 != 0) {
                                int i3 = this.A01;
                                int i4 = this.A00;
                                if (i3 > i4) {
                                    i = Math.min(i3, (int) EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH);
                                    min = (i4 * i) / i3;
                                } else {
                                    min = Math.min(i4, (int) EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH);
                                    i = (min * i3) / i4;
                                    if (i < 300) {
                                        min = (i4 * 300) / i3;
                                        i = 300;
                                    }
                                }
                                this.A0I = A0D(byteArray, 300, 75, i, min, true);
                            }
                            this.A04 = System.currentTimeMillis() - currentTimeMillis;
                            return;
                        }
                        byteArrayOutputStream.write(bArr, 0, read);
                        i2 += read;
                    } while (i2 <= 307200);
                    throw new C867648s();
                } catch (Throwable th) {
                    try {
                        r9.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            }
        }
    }

    public final void A09(String str, String str2, String... strArr) {
        String substring = str.substring(0, str.length() - str2.length());
        for (String str3 : strArr) {
            LinkedHashSet linkedHashSet = this.A0F;
            StringBuilder sb = new StringBuilder();
            sb.append(substring);
            sb.append(str3);
            linkedHashSet.add(sb.toString());
        }
    }

    public final void A0A(HttpURLConnection httpURLConnection) {
        if (httpURLConnection != null) {
            try {
                AnonymousClass1P1.A03(new AnonymousClass1oJ(this.A0J, httpURLConnection.getInputStream(), null, 23));
            } catch (IOException unused) {
            }
            httpURLConnection.disconnect();
        }
    }

    public boolean A0B() {
        boolean z;
        if (this instanceof AnonymousClass2KT) {
            return true;
        }
        boolean z2 = !TextUtils.isEmpty(this.A0E);
        boolean z3 = !TextUtils.isEmpty(this.A0B);
        AnonymousClass2V4 r0 = this.A06;
        if (r0 != null) {
            String str = r0.A01;
            if ("image/gif".equals(str) || "video/mp4".equals(str)) {
                z = true;
                if (!z2 || z3 || z) {
                    return true;
                }
                return false;
            }
        }
        z = false;
        return !z2 ? true : true;
    }

    public byte[] A0C(Bitmap bitmap) {
        int min = Math.min(140, Math.min(bitmap.getWidth(), bitmap.getHeight()));
        int min2 = Math.min(bitmap.getWidth(), bitmap.getHeight());
        return A02(bitmap, new Rect((bitmap.getWidth() - min2) >> 1, (bitmap.getHeight() - min2) >> 1, (bitmap.getWidth() + min2) >> 1, (bitmap.getHeight() + min2) >> 1), min, min, 80);
    }

    public byte[] A0D(byte[] bArr, int i, int i2, int i3, int i4, boolean z) {
        byte[] A0C;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
        int i5 = options.outWidth;
        this.A01 = i5;
        int i6 = options.outHeight;
        this.A00 = i6;
        if (i5 < i || i6 < i2) {
            return null;
        }
        options.inDither = true;
        options.inScaled = false;
        options.inPreferQualityOverSpeed = true;
        Bitmap bitmap = C37501mV.A06(new C41591tm(options, null, i3, i4, !z), bArr).A02;
        if (bitmap == null) {
            return null;
        }
        if (z) {
            A0C = A02(bitmap, new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight()), i3, i4, 50);
        } else {
            A0C = A0C(bitmap);
        }
        bitmap.recycle();
        return A0C;
    }
}
