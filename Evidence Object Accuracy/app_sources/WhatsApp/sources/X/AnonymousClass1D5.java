package X;

import android.content.SharedPreferences;

/* renamed from: X.1D5  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1D5 implements AbstractC16990q5 {
    public final AnonymousClass15P A00;

    @Override // X.AbstractC16990q5
    public /* synthetic */ void AOp() {
    }

    public AnonymousClass1D5(AnonymousClass15P r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC16990q5
    public void AOo() {
        SharedPreferences.Editor edit = this.A00.A00().edit();
        edit.remove("disappearing_mode_duration");
        edit.apply();
    }
}
