package X;

/* renamed from: X.3Xp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69013Xp implements AnonymousClass13Q {
    public final /* synthetic */ AbstractView$OnCreateContextMenuListenerC35851ir A00;

    public C69013Xp(AbstractView$OnCreateContextMenuListenerC35851ir r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass13Q
    public void AWN(AbstractC14640lm r3) {
        AbstractView$OnCreateContextMenuListenerC35851ir r1 = this.A00;
        if (r3.equals(r1.A0c)) {
            r1.A0K();
            r1.A0E.invalidateOptionsMenu();
        }
    }

    @Override // X.AnonymousClass13Q
    public void AWr(AbstractC14640lm r4) {
        AbstractView$OnCreateContextMenuListenerC35851ir r2 = this.A00;
        if (r4.equals(r2.A0c)) {
            C30751Yr r0 = r2.A0o;
            if (r0 != null && r2.A0z.A0F(r0.A06)) {
                r2.A0o = null;
            }
            r2.A0K();
            r2.A0E.invalidateOptionsMenu();
        }
    }
}
