package X;

import android.database.Cursor;
import com.whatsapp.util.Log;

/* renamed from: X.0tk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19210tk extends AbstractC18500sY {
    public final C20950wa A00;

    public C19210tk(C20950wa r3, C18480sW r4) {
        super(r4, "message_thumbnail", 2);
        this.A00 = r3;
    }

    @Override // X.AbstractC18500sY
    public AnonymousClass2Ez A09(Cursor cursor) {
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow("_id");
        int columnIndexOrThrow2 = cursor.getColumnIndexOrThrow("thumbnail");
        int i = 0;
        long j = -1;
        while (cursor.moveToNext()) {
            if (cursor.isNull(columnIndexOrThrow)) {
                Log.e("DatabaseUtils/safeGetLong/the value in the cursor is null");
                j = -1;
            } else {
                try {
                    j = cursor.getLong(columnIndexOrThrow);
                } catch (IllegalStateException e) {
                    Log.e("DatabaseUtils/safeGetLong/failed once", e);
                    try {
                        j = cursor.getLong(columnIndexOrThrow);
                    } catch (IllegalStateException e2) {
                        Log.e("DatabaseUtils/safeGetLong/failed twice, returning default value", e2);
                        j = -1;
                    }
                }
                if (j >= 1) {
                    byte[] blob = cursor.getBlob(columnIndexOrThrow2);
                    if (blob != null && blob.length > 0) {
                        C20950wa r9 = this.A00;
                        r9.A07(blob, j);
                        int columnIndexOrThrow3 = cursor.getColumnIndexOrThrow("key_remote_jid");
                        int columnIndexOrThrow4 = cursor.getColumnIndexOrThrow("key_from_me");
                        int columnIndexOrThrow5 = cursor.getColumnIndexOrThrow("key_id");
                        AbstractC14640lm A01 = AbstractC14640lm.A01(cursor.getString(columnIndexOrThrow3));
                        int i2 = cursor.getInt(columnIndexOrThrow4);
                        boolean z = true;
                        if (i2 != 1) {
                            z = false;
                        }
                        r9.A03(new AnonymousClass1IS(A01, cursor.getString(columnIndexOrThrow5), z));
                    }
                    i++;
                }
            }
            StringBuilder sb = new StringBuilder("ThumbnailMessageStore/processBatch/invalid row id, id=");
            sb.append(j);
            Log.e(sb.toString());
        }
        return new AnonymousClass2Ez(j, i);
    }
}
