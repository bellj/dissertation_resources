package X;

/* renamed from: X.4VX  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4VX {
    public final C14260l7 A00;
    public final AnonymousClass28D A01;
    public final AbstractC14200l1 A02;

    public AnonymousClass4VX(C14260l7 r1, AnonymousClass28D r2, AbstractC14200l1 r3) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof AnonymousClass4VX) || ((AnonymousClass4VX) obj).A02 != this.A02) {
            return false;
        }
        return true;
    }
}
