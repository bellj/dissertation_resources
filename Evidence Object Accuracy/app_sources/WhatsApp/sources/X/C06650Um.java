package X;

import android.animation.ValueAnimator;

/* renamed from: X.0Um  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06650Um implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ AnonymousClass0F7 A00;

    public C06650Um(AnonymousClass0F7 r1) {
        this.A00 = r1;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        int floatValue = (int) (((Number) valueAnimator.getAnimatedValue()).floatValue() * 255.0f);
        AnonymousClass0F7 r1 = this.A00;
        r1.A0O.setAlpha(floatValue);
        r1.A0M.setAlpha(floatValue);
        r1.A0B.invalidate();
    }
}
