package X;

import java.util.Comparator;

/* renamed from: X.1j7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C35981j7 {
    public final float A00;
    public final float A01;
    public final C244415n A02;
    public final Comparator A03;

    public C35981j7(C244415n r1, Comparator comparator, float f, float f2) {
        this.A00 = f;
        this.A01 = f2;
        this.A03 = comparator;
        this.A02 = r1;
    }
}
