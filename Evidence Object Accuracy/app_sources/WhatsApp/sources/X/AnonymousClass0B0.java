package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import com.facebook.redex.IDxCreatorShape0S0000000_I1;

/* renamed from: X.0B0  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0B0 extends View.BaseSavedState {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape0S0000000_I1(29);
    public float A00;
    public int A01;
    public int A02;
    public int A03;
    public String A04;
    public String A05;
    public boolean A06;

    public /* synthetic */ AnonymousClass0B0(Parcel parcel) {
        super(parcel);
        this.A04 = parcel.readString();
        this.A00 = parcel.readFloat();
        this.A06 = parcel.readInt() != 1 ? false : true;
        this.A05 = parcel.readString();
        this.A03 = parcel.readInt();
        this.A02 = parcel.readInt();
    }

    public AnonymousClass0B0(Parcelable parcelable) {
        super(parcelable);
    }

    @Override // android.view.View.BaseSavedState, android.os.Parcelable, android.view.AbsSavedState
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.A04);
        parcel.writeFloat(this.A00);
        parcel.writeInt(this.A06 ? 1 : 0);
        parcel.writeString(this.A05);
        parcel.writeInt(this.A03);
        parcel.writeInt(this.A02);
    }
}
