package X;

import com.whatsapp.R;
import com.whatsapp.community.AddGroupsToCommunityActivity;

/* renamed from: X.4Uy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92204Uy {
    public final /* synthetic */ AddGroupsToCommunityActivity A00;

    public C92204Uy(AddGroupsToCommunityActivity addGroupsToCommunityActivity) {
        this.A00 = addGroupsToCommunityActivity;
    }

    public void A00() {
        AddGroupsToCommunityActivity addGroupsToCommunityActivity = this.A00;
        addGroupsToCommunityActivity.AaN();
        addGroupsToCommunityActivity.A2K(new C1095252a(addGroupsToCommunityActivity), 0, R.string.create_community_failed_to_be_created, R.string.create_community_failed_try_again, R.string.create_community_failed_cancel);
    }
}
