package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

/* renamed from: X.1HY  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1HY extends BroadcastReceiver {
    public final /* synthetic */ C15550nR A00;

    public AnonymousClass1HY(C15550nR r1) {
        this.A00 = r1;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        Locale locale;
        C15550nR r1 = this.A00;
        Locale A00 = AnonymousClass018.A00(r1.A0C.A00);
        Map map = r1.A04.A01;
        synchronized (map) {
            HashSet hashSet = null;
            for (Map.Entry entry : map.entrySet()) {
                AbstractC14640lm r12 = (AbstractC14640lm) entry.getKey();
                C15370n3 r0 = (C15370n3) entry.getValue();
                if (!(r12 == null || r0 == null || (locale = r0.A0V) == null || A00.equals(locale))) {
                    if (hashSet == null) {
                        hashSet = new HashSet();
                    }
                    hashSet.add(r12);
                }
            }
            if (hashSet != null) {
                Iterator it = hashSet.iterator();
                while (it.hasNext()) {
                    map.remove((AbstractC14640lm) it.next());
                }
                hashSet.size();
            }
        }
    }
}
