package X;

import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.1I8  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1I8 implements ThreadFactory {
    public final int A00;
    public final String A01;
    public final AtomicInteger A02 = new AtomicInteger(1);

    public /* synthetic */ AnonymousClass1I8(String str, int i) {
        this.A00 = i;
        this.A01 = str;
    }

    @Override // java.util.concurrent.ThreadFactory
    public Thread newThread(Runnable runnable) {
        RunnableBRunnable0Shape8S0200000_I0_8 runnableBRunnable0Shape8S0200000_I0_8 = new RunnableBRunnable0Shape8S0200000_I0_8(this, 37, runnable);
        StringBuilder sb = new StringBuilder();
        sb.append(this.A01);
        sb.append(" #");
        sb.append(this.A02.getAndIncrement());
        return new AnonymousClass1MS(runnableBRunnable0Shape8S0200000_I0_8, sb.toString());
    }
}
