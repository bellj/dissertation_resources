package X;

import java.security.spec.AlgorithmParameterSpec;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.5C2  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass5C2 implements AlgorithmParameterSpec {
    public static Map A00;

    static {
        HashMap A11 = C12970iu.A11();
        A00 = A11;
        A11.put(AbstractC116985Xr.A0P, "E-A");
        A00.put(AbstractC116985Xr.A0Q, "E-B");
        A00.put(AbstractC116985Xr.A0R, "E-C");
        A00.put(AbstractC116985Xr.A0S, "E-D");
        A00.put(AbstractC116935Xm.A04, "Param-Z");
    }
}
