package X;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/* renamed from: X.50T  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass50T implements AbstractC117135Yq {
    public int zza = 0;

    public static C80013rZ A06(AbstractC80173rp r2) {
        return (C80013rZ) r2.A06(5);
    }

    public static void A07(Iterable iterable, List list) {
        if (iterable instanceof AnonymousClass5Z4) {
            List AhI = ((AnonymousClass5Z4) iterable).AhI();
            AnonymousClass5Z4 r6 = (AnonymousClass5Z4) list;
            int size = list.size();
            for (Object obj : AhI) {
                if (obj == null) {
                    StringBuilder A0t = C12980iv.A0t(37);
                    A0t.append("Element at index ");
                    A0t.append(r6.size() - size);
                    String A0d = C12960it.A0d(" is null.", A0t);
                    int size2 = r6.size();
                    while (true) {
                        size2--;
                        if (size2 < size) {
                            break;
                        }
                        r6.remove(size2);
                    }
                    throw C12980iv.A0n(A0d);
                } else if (obj instanceof AbstractC111925Bi) {
                    r6.Agp((AbstractC111925Bi) obj);
                } else {
                    r6.add(obj);
                }
            }
        } else if (iterable instanceof AbstractC115265Qv) {
            list.addAll((Collection) iterable);
        } else {
            if ((list instanceof ArrayList) && (iterable instanceof Collection)) {
                ((ArrayList) list).ensureCapacity(list.size() + ((Collection) iterable).size());
            }
            int size3 = list.size();
            for (Object obj2 : iterable) {
                if (obj2 == null) {
                    StringBuilder A0t2 = C12980iv.A0t(37);
                    A0t2.append("Element at index ");
                    A0t2.append(list.size() - size3);
                    String A0d2 = C12960it.A0d(" is null.", A0t2);
                    int size4 = list.size();
                    while (true) {
                        size4--;
                        if (size4 < size3) {
                            break;
                        }
                        list.remove(size4);
                    }
                    throw C12980iv.A0n(A0d2);
                }
                list.add(obj2);
            }
        }
    }

    public static void A08(Object obj, Object obj2) {
        AbstractC80173rp.zzd.put(obj, obj2);
    }
}
