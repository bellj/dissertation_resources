package X;

import android.content.Context;
import android.content.Intent;
import androidx.fragment.app.DialogFragment;
import com.whatsapp.R;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.payments.service.NoviPaymentInviteFragment;
import com.whatsapp.payments.ui.NoviPayBloksActivity;
import com.whatsapp.payments.ui.PaymentBottomSheet;
import com.whatsapp.payments.ui.invites.PaymentInviteFragment;
import java.util.ArrayList;

/* renamed from: X.69I  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass69I implements AbstractC38141na {
    public final C15550nR A00;
    public final C16590pI A01;
    public final C14850m9 A02;
    public final C22710zW A03;
    public final AnonymousClass61F A04;
    public final C130105yo A05;

    @Override // X.AbstractC38141na
    public int AFW() {
        return 2;
    }

    public AnonymousClass69I(C15550nR r1, C16590pI r2, C14850m9 r3, C22710zW r4, AnonymousClass61F r5, C130105yo r6) {
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = r1;
        this.A04 = r5;
        this.A05 = r6;
        this.A03 = r4;
    }

    @Override // X.AbstractC38141na
    public boolean A6y() {
        return this.A03.A04() && this.A02.A07(544) && AIF();
    }

    @Override // X.AbstractC38141na
    public boolean A6z(UserJid userJid) {
        if (this.A03.A04() && AIF() && !this.A00.A0a(userJid) && !this.A05.A05()) {
            C14850m9 r1 = this.A02;
            if (r1.A07(860) && r1.A07(900)) {
                return true;
            }
        }
        return false;
    }

    @Override // X.AbstractC38141na
    public Intent AAY(AbstractC15340mz r4) {
        if (AIF()) {
            return null;
        }
        AbstractC14640lm r1 = r4.A0z.A00;
        if (r1 instanceof GroupJid) {
            r1 = r4.A0B();
        }
        String A03 = C15380n4.A03(r1);
        Intent A0D = C12990iw.A0D(this.A01.A00, NoviPayBloksActivity.class);
        A0D.putExtra("extra_inviter_jid", A03);
        return A0D;
    }

    @Override // X.AbstractC38141na
    public int ADZ() {
        return R.drawable.novi_logo;
    }

    @Override // X.AbstractC38141na
    public AnonymousClass4S0 ADa() {
        return new AnonymousClass4S0("001_invite_bubble.webp", "", -1, true);
    }

    @Override // X.AbstractC38141na
    public C69973aX ADb(C16590pI r2, C22590zK r3, AbstractC14440lR r4) {
        return new C120155fi(r2, r3, r4);
    }

    @Override // X.AbstractC38141na
    public DialogFragment AFK(String str, ArrayList arrayList, boolean z, boolean z2) {
        PaymentBottomSheet paymentBottomSheet = new PaymentBottomSheet();
        NoviPaymentInviteFragment noviPaymentInviteFragment = new NoviPaymentInviteFragment();
        noviPaymentInviteFragment.A0U(PaymentInviteFragment.A01(str, arrayList, 2, z, z2));
        paymentBottomSheet.A01 = noviPaymentInviteFragment;
        return paymentBottomSheet;
    }

    @Override // X.AbstractC38141na
    public String AFM(Context context, String str, boolean z) {
        int i = R.string.novi_payment_invite_status_text_inbound;
        if (z) {
            i = R.string.novi_payment_invite_status_text_outbound;
        }
        return C12960it.A0X(context, str, C12970iu.A1b(), 0, i);
    }

    @Override // X.AbstractC38141na
    public boolean AIF() {
        AnonymousClass61F r1 = this.A04;
        return r1.A0E() && r1.A0F();
    }
}
