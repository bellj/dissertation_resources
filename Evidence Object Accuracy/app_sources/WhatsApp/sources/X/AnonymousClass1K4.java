package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.1K4  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1K4 extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass1K4 A09;
    public static volatile AnonymousClass255 A0A;
    public int A00;
    public int A01;
    public AbstractC27881Jp A02;
    public AbstractC27881Jp A03;
    public AnonymousClass1K6 A04 = AnonymousClass277.A01;
    public C462325b A05;
    public AnonymousClass1K3 A06;
    public C27921Jt A07;
    public AnonymousClass1tB A08;

    static {
        AnonymousClass1K4 r0 = new AnonymousClass1K4();
        A09 = r0;
        r0.A0W();
    }

    public AnonymousClass1K4() {
        AbstractC27881Jp r0 = AbstractC27881Jp.A01;
        this.A03 = r0;
        this.A02 = r0;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r8, Object obj, Object obj2) {
        C463625o r1;
        C463825q r12;
        C463725p r13;
        C464425w r14;
        switch (r8.ordinal()) {
            case 0:
                return A09;
            case 1:
                AbstractC462925h r9 = (AbstractC462925h) obj;
                AnonymousClass1K4 r10 = (AnonymousClass1K4) obj2;
                this.A08 = (AnonymousClass1tB) r9.Aft(this.A08, r10.A08);
                this.A04 = r9.Afr(this.A04, r10.A04);
                this.A06 = (AnonymousClass1K3) r9.Aft(this.A06, r10.A06);
                boolean z = false;
                if ((this.A00 & 4) == 4) {
                    z = true;
                }
                AbstractC27881Jp r3 = this.A03;
                boolean z2 = false;
                if ((r10.A00 & 4) == 4) {
                    z2 = true;
                }
                this.A03 = r9.Afm(r3, r10.A03, z, z2);
                boolean z3 = false;
                if ((this.A00 & 8) == 8) {
                    z3 = true;
                }
                AbstractC27881Jp r32 = this.A02;
                boolean z4 = false;
                if ((r10.A00 & 8) == 8) {
                    z4 = true;
                }
                this.A02 = r9.Afm(r32, r10.A02, z3, z4);
                this.A07 = (C27921Jt) r9.Aft(this.A07, r10.A07);
                this.A05 = (C462325b) r9.Aft(this.A05, r10.A05);
                int i = this.A00;
                boolean z5 = false;
                if ((i & 64) == 64) {
                    z5 = true;
                }
                int i2 = this.A01;
                int i3 = r10.A00;
                boolean z6 = false;
                if ((i3 & 64) == 64) {
                    z6 = true;
                }
                this.A01 = r9.Afp(i2, r10.A01, z5, z6);
                if (r9 == C463025i.A00) {
                    this.A00 = i | i3;
                }
                return this;
            case 2:
                AnonymousClass253 r92 = (AnonymousClass253) obj;
                AnonymousClass254 r102 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        try {
                            int A03 = r92.A03();
                            if (A03 == 0) {
                                break;
                            } else if (A03 == 10) {
                                if ((this.A00 & 1) == 1) {
                                    r14 = (C464425w) this.A08.A0T();
                                } else {
                                    r14 = null;
                                }
                                AnonymousClass1tB r0 = (AnonymousClass1tB) r92.A09(r102, AnonymousClass1tB.A02.A0U());
                                this.A08 = r0;
                                if (r14 != null) {
                                    r14.A04(r0);
                                    this.A08 = (AnonymousClass1tB) r14.A01();
                                }
                                this.A00 |= 1;
                            } else if (A03 == 18) {
                                AnonymousClass1K6 r15 = this.A04;
                                if (!((AnonymousClass1K7) r15).A00) {
                                    r15 = AbstractC27091Fz.A0G(r15);
                                    this.A04 = r15;
                                }
                                r15.add((C27941Jv) r92.A09(r102, C27941Jv.A03.A0U()));
                            } else if (A03 == 26) {
                                if ((this.A00 & 2) == 2) {
                                    r13 = (C463725p) this.A06.A0T();
                                } else {
                                    r13 = null;
                                }
                                AnonymousClass1K3 r02 = (AnonymousClass1K3) r92.A09(r102, AnonymousClass1K3.A07.A0U());
                                this.A06 = r02;
                                if (r13 != null) {
                                    r13.A04(r02);
                                    this.A06 = (AnonymousClass1K3) r13.A01();
                                }
                                this.A00 |= 2;
                            } else if (A03 == 34) {
                                this.A00 |= 4;
                                this.A03 = r92.A08();
                            } else if (A03 == 42) {
                                this.A00 |= 8;
                                this.A02 = r92.A08();
                            } else if (A03 == 50) {
                                if ((this.A00 & 16) == 16) {
                                    r12 = (C463825q) this.A07.A0T();
                                } else {
                                    r12 = null;
                                }
                                C27921Jt r03 = (C27921Jt) r92.A09(r102, C27921Jt.A02.A0U());
                                this.A07 = r03;
                                if (r12 != null) {
                                    r12.A04(r03);
                                    this.A07 = (C27921Jt) r12.A01();
                                }
                                this.A00 |= 16;
                            } else if (A03 == 58) {
                                if ((this.A00 & 32) == 32) {
                                    r1 = (C463625o) this.A05.A0T();
                                } else {
                                    r1 = null;
                                }
                                C462325b r04 = (C462325b) r92.A09(r102, C462325b.A03.A0U());
                                this.A05 = r04;
                                if (r1 != null) {
                                    r1.A04(r04);
                                    this.A05 = (C462325b) r1.A01();
                                }
                                this.A00 |= 32;
                            } else if (A03 == 64) {
                                this.A00 |= 64;
                                this.A01 = r92.A02();
                            } else if (!A0a(r92, A03)) {
                                break;
                            }
                        } catch (C28971Pt e) {
                            e.unfinishedMessage = this;
                            throw new RuntimeException(e);
                        }
                    } catch (IOException e2) {
                        C28971Pt r16 = new C28971Pt(e2.getMessage());
                        r16.unfinishedMessage = this;
                        throw new RuntimeException(r16);
                    }
                }
            case 3:
                ((AnonymousClass1K7) this.A04).A00 = false;
                return null;
            case 4:
                return new AnonymousClass1K4();
            case 5:
                return new C82463vf();
            case 6:
                break;
            case 7:
                if (A0A == null) {
                    synchronized (AnonymousClass1K4.class) {
                        if (A0A == null) {
                            A0A = new AnonymousClass255(A09);
                        }
                    }
                }
                return A0A;
            default:
                throw new UnsupportedOperationException();
        }
        return A09;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i;
        int i2 = ((AbstractC27091Fz) this).A00;
        if (i2 != -1) {
            return i2;
        }
        if ((this.A00 & 1) == 1) {
            AnonymousClass1tB r0 = this.A08;
            if (r0 == null) {
                r0 = AnonymousClass1tB.A02;
            }
            i = CodedOutputStream.A0A(r0, 1) + 0;
        } else {
            i = 0;
        }
        for (int i3 = 0; i3 < this.A04.size(); i3++) {
            i += CodedOutputStream.A0A((AnonymousClass1G1) this.A04.get(i3), 2);
        }
        if ((this.A00 & 2) == 2) {
            AnonymousClass1K3 r02 = this.A06;
            if (r02 == null) {
                r02 = AnonymousClass1K3.A07;
            }
            i += CodedOutputStream.A0A(r02, 3);
        }
        int i4 = this.A00;
        if ((i4 & 4) == 4) {
            i += CodedOutputStream.A09(this.A03, 4);
        }
        if ((i4 & 8) == 8) {
            i += CodedOutputStream.A09(this.A02, 5);
        }
        if ((i4 & 16) == 16) {
            C27921Jt r03 = this.A07;
            if (r03 == null) {
                r03 = C27921Jt.A02;
            }
            i += CodedOutputStream.A0A(r03, 6);
        }
        if ((this.A00 & 32) == 32) {
            C462325b r04 = this.A05;
            if (r04 == null) {
                r04 = C462325b.A03;
            }
            i += CodedOutputStream.A0A(r04, 7);
        }
        if ((this.A00 & 64) == 64) {
            i += CodedOutputStream.A04(8, this.A01);
        }
        int A00 = i + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            AnonymousClass1tB r0 = this.A08;
            if (r0 == null) {
                r0 = AnonymousClass1tB.A02;
            }
            codedOutputStream.A0L(r0, 1);
        }
        for (int i = 0; i < this.A04.size(); i++) {
            codedOutputStream.A0L((AnonymousClass1G1) this.A04.get(i), 2);
        }
        if ((this.A00 & 2) == 2) {
            AnonymousClass1K3 r02 = this.A06;
            if (r02 == null) {
                r02 = AnonymousClass1K3.A07;
            }
            codedOutputStream.A0L(r02, 3);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0K(this.A03, 4);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0K(this.A02, 5);
        }
        if ((this.A00 & 16) == 16) {
            C27921Jt r03 = this.A07;
            if (r03 == null) {
                r03 = C27921Jt.A02;
            }
            codedOutputStream.A0L(r03, 6);
        }
        if ((this.A00 & 32) == 32) {
            C462325b r04 = this.A05;
            if (r04 == null) {
                r04 = C462325b.A03;
            }
            codedOutputStream.A0L(r04, 7);
        }
        if ((this.A00 & 64) == 64) {
            codedOutputStream.A0F(8, this.A01);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
