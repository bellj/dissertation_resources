package X;

import android.app.Activity;
import android.view.View;
import com.whatsapp.conversationslist.ViewHolder;

/* renamed from: X.2ws  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60382ws extends AbstractC75673kE {
    public final Activity A00;
    public final C63543Bz A01;
    public final ViewHolder A02;

    public C60382ws(Activity activity, View view, C253519b r40, C15570nT r41, C15450nH r42, AnonymousClass11L r43, C14650lo r44, C238013b r45, AnonymousClass130 r46, C15550nR r47, C15610nY r48, AnonymousClass1J1 r49, C63563Cb r50, AbstractC33091dK r51, C63543Bz r52, C14830m7 r53, C16590pI r54, C14820m6 r55, AnonymousClass018 r56, C19990v2 r57, C241714m r58, C15600nX r59, C236812p r60, AnonymousClass10Y r61, C21400xM r62, C14850m9 r63, C20710wC r64, AnonymousClass13H r65, C22710zW r66, C17070qD r67, AnonymousClass14X r68, C15860o1 r69, AnonymousClass12F r70, AnonymousClass3J9 r71, AbstractC14440lR r72) {
        super(view);
        this.A00 = activity;
        this.A01 = r52;
        this.A02 = new ViewHolder(activity, view, r40, r41, r42, r43, r44, r45, r46, r47, r48, r49, r50, r51, r53, r54, r55, r56, r57, r58, r59, r60, r61, r62, r63, r64, r65, r66, r67, r68, r69, r70, r71, r72);
    }

    @Override // X.AbstractC75673kE
    public /* bridge */ /* synthetic */ void A08(Object obj, int i) {
        ViewHolder viewHolder = this.A02;
        AnonymousClass2WJ r4 = new AnonymousClass2WJ(((AnonymousClass1OU) obj).A02);
        Activity activity = this.A00;
        viewHolder.A0G(activity, activity, r4, new AnonymousClass5U4() { // from class: X.55f
            @Override // X.AnonymousClass5U4
            public final void AVW(int i2) {
            }
        }, this.A01, null, 5, i, false);
    }
}
