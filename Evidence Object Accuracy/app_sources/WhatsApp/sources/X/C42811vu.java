package X;

import android.net.Uri;

/* renamed from: X.1vu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C42811vu extends AbstractC16350or {
    public ActivityC13810kN A00;
    public AbstractC13870kT A01;
    public final int A02;
    public final Uri A03;
    public final AnonymousClass10T A04;
    public final AnonymousClass01d A05;
    public final AnonymousClass018 A06;
    public final C15370n3 A07;
    public final String A08;

    public C42811vu(Uri uri, ActivityC13810kN r2, AbstractC13870kT r3, AnonymousClass10T r4, AnonymousClass01d r5, AnonymousClass018 r6, C15370n3 r7, String str, int i) {
        super(r2);
        this.A01 = r3;
        this.A05 = r5;
        this.A06 = r6;
        this.A03 = uri;
        this.A07 = r7;
        this.A00 = r2;
        this.A04 = r4;
        this.A08 = str;
        this.A02 = i;
    }
}
