package X;

import android.animation.ValueAnimator;

/* renamed from: X.2RA  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2RA {
    public final ValueAnimator A00;
    public final int[] A01;

    public AnonymousClass2RA(ValueAnimator valueAnimator, int[] iArr) {
        this.A01 = iArr;
        this.A00 = valueAnimator;
    }
}
