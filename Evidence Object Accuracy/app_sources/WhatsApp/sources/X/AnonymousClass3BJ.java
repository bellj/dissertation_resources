package X;

import android.os.IBinder;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

/* renamed from: X.3BJ  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3BJ {
    public final Messenger A00;
    public final C13560jv A01;

    public AnonymousClass3BJ(IBinder iBinder) {
        String interfaceDescriptor = iBinder.getInterfaceDescriptor();
        if ("android.os.IMessenger".equals(interfaceDescriptor)) {
            this.A00 = new Messenger(iBinder);
        } else if ("com.google.android.gms.iid.IMessengerCompat".equals(interfaceDescriptor)) {
            this.A01 = new C13560jv(iBinder);
        } else {
            Log.w("MessengerIpcClient", C12960it.A0c(String.valueOf(interfaceDescriptor), "Invalid interface descriptor: "));
            throw new RemoteException();
        }
    }
}
