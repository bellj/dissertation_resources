package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2TC  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2TC extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass2TC A05;
    public static volatile AnonymousClass255 A06;
    public int A00;
    public int A01;
    public AnonymousClass2TG A02;
    public String A03 = "";
    public String A04 = "";

    static {
        AnonymousClass2TC r0 = new AnonymousClass2TC();
        A05 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r8, Object obj, Object obj2) {
        AnonymousClass2TI r1;
        switch (r8.ordinal()) {
            case 0:
                return A05;
            case 1:
                AbstractC462925h r9 = (AbstractC462925h) obj;
                AnonymousClass2TC r10 = (AnonymousClass2TC) obj2;
                int i = this.A00;
                boolean z = true;
                if ((i & 1) != 1) {
                    z = false;
                }
                String str = this.A03;
                int i2 = r10.A00;
                boolean z2 = true;
                if ((i2 & 1) != 1) {
                    z2 = false;
                }
                this.A03 = r9.Afy(str, r10.A03, z, z2);
                boolean z3 = false;
                if ((i & 2) == 2) {
                    z3 = true;
                }
                String str2 = this.A04;
                boolean z4 = false;
                if ((i2 & 2) == 2) {
                    z4 = true;
                }
                this.A04 = r9.Afy(str2, r10.A04, z3, z4);
                this.A02 = (AnonymousClass2TG) r9.Aft(this.A02, r10.A02);
                int i3 = this.A00;
                boolean z5 = false;
                if ((i3 & 8) == 8) {
                    z5 = true;
                }
                int i4 = this.A01;
                int i5 = r10.A00;
                boolean z6 = false;
                if ((i5 & 8) == 8) {
                    z6 = true;
                }
                this.A01 = r9.Afp(i4, r10.A01, z5, z6);
                if (r9 == C463025i.A00) {
                    this.A00 = i3 | i5;
                }
                return this;
            case 2:
                AnonymousClass253 r92 = (AnonymousClass253) obj;
                AnonymousClass254 r102 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        int A03 = r92.A03();
                        if (A03 == 0) {
                            break;
                        } else if (A03 == 10) {
                            String A0A = r92.A0A();
                            this.A00 = 1 | this.A00;
                            this.A03 = A0A;
                        } else if (A03 == 18) {
                            String A0A2 = r92.A0A();
                            this.A00 |= 2;
                            this.A04 = A0A2;
                        } else if (A03 == 26) {
                            if ((this.A00 & 4) == 4) {
                                r1 = (AnonymousClass2TI) this.A02.A0T();
                            } else {
                                r1 = null;
                            }
                            AnonymousClass2TG r0 = (AnonymousClass2TG) r92.A09(r102, AnonymousClass2TG.A0C.A0U());
                            this.A02 = r0;
                            if (r1 != null) {
                                r1.A04(r0);
                                this.A02 = (AnonymousClass2TG) r1.A01();
                            }
                            this.A00 |= 4;
                        } else if (A03 == 32) {
                            int A02 = r92.A02();
                            if (A02 == 0 || A02 == 1 || A02 == 2 || A02 == 3 || A02 == 4) {
                                this.A00 |= 8;
                                this.A01 = A02;
                            } else {
                                super.A0X(4, A02);
                            }
                        } else if (!A0a(r92, A03)) {
                            break;
                        }
                    } catch (C28971Pt e) {
                        e.unfinishedMessage = this;
                        throw new RuntimeException(e);
                    } catch (IOException e2) {
                        C28971Pt r12 = new C28971Pt(e2.getMessage());
                        r12.unfinishedMessage = this;
                        throw new RuntimeException(r12);
                    }
                }
                break;
            case 3:
                return null;
            case 4:
                return new AnonymousClass2TC();
            case 5:
                return new AnonymousClass2TE();
            case 6:
                break;
            case 7:
                if (A06 == null) {
                    synchronized (AnonymousClass2TC.class) {
                        if (A06 == null) {
                            A06 = new AnonymousClass255(A05);
                        }
                    }
                }
                return A06;
            default:
                throw new UnsupportedOperationException();
        }
        return A05;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A07(1, this.A03);
        }
        if ((this.A00 & 2) == 2) {
            i2 += CodedOutputStream.A07(2, this.A04);
        }
        if ((this.A00 & 4) == 4) {
            AnonymousClass2TG r0 = this.A02;
            if (r0 == null) {
                r0 = AnonymousClass2TG.A0C;
            }
            i2 += CodedOutputStream.A0A(r0, 3);
        }
        if ((this.A00 & 8) == 8) {
            i2 += CodedOutputStream.A02(4, this.A01);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A03);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0I(2, this.A04);
        }
        if ((this.A00 & 4) == 4) {
            AnonymousClass2TG r0 = this.A02;
            if (r0 == null) {
                r0 = AnonymousClass2TG.A0C;
            }
            codedOutputStream.A0L(r0, 3);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0E(4, this.A01);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
