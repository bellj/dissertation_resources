package X;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.view.animation.LinearInterpolator;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/* renamed from: X.0A2  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0A2 extends Drawable {
    public float A00;
    public float A01;
    public int A02;
    public ValueAnimator A03;
    public C04530Mb A04;
    public C04530Mb A05;
    public boolean A06;
    public final float A07;
    public final float A08;
    public final int A09;
    public final ValueAnimator.AnimatorUpdateListener A0A = new C06690Uq(this);
    public final ValueAnimator.AnimatorUpdateListener A0B = new C06680Up(this);
    public final Path A0C;
    public final List A0D;
    public final C04530Mb[] A0E;

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -1;
    }

    public AnonymousClass0A2(Context context, float f, int i) {
        C04530Mb[] r4 = new C04530Mb[4];
        this.A0E = r4;
        this.A0C = new Path();
        this.A02 = 255;
        this.A09 = i;
        this.A08 = f;
        this.A07 = AnonymousClass0LQ.A00(context, 375.0f);
        this.A0D = Arrays.asList(new AnonymousClass0S2(0.0f, AnonymousClass0LQ.A00(context, 116.0f), 0, AnonymousClass0LQ.A00(context, 350.0f), -15173646, AnonymousClass0LQ.A00(context, 232.0f)), new AnonymousClass0S2(0.0f, AnonymousClass0LQ.A00(context, -219.0f), 1, AnonymousClass0LQ.A00(context, 226.0f), -14298266, AnonymousClass0LQ.A00(context, 153.0f)), new AnonymousClass0S2(AnonymousClass0LQ.A00(context, 124.0f), AnonymousClass0LQ.A00(context, -438.0f), 2, AnonymousClass0LQ.A00(context, 156.0f), -668109, AnonymousClass0LQ.A00(context, 100.0f)), new AnonymousClass0S2(AnonymousClass0LQ.A00(context, 238.0f), AnonymousClass0LQ.A00(context, -196.0f), 3, AnonymousClass0LQ.A00(context, 206.0f), -37796, AnonymousClass0LQ.A00(context, 132.0f)), new AnonymousClass0S2(AnonymousClass0LQ.A00(context, -175.0f), AnonymousClass0LQ.A00(context, 373.0f), 4, AnonymousClass0LQ.A00(context, 272.0f), -15173646, AnonymousClass0LQ.A00(context, 175.0f)), new AnonymousClass0S2(AnonymousClass0LQ.A00(context, 308.0f), AnonymousClass0LQ.A00(context, -71.0f), 5, AnonymousClass0LQ.A00(context, 176.0f), -6278145, AnonymousClass0LQ.A00(context, 119.0f)));
        HashMap hashMap = new HashMap();
        C04990Nv r2 = new C04990Nv();
        r2.A04 = true;
        r2.A01 = 90.0f;
        hashMap.put(-1, r2);
        C04990Nv r0 = new C04990Nv();
        r0.A03 = true;
        hashMap.put(4, r0);
        C04990Nv r22 = new C04990Nv();
        r22.A02 = true;
        r22.A00 = 0.5f;
        hashMap.put(5, r22);
        r4[1] = new C04530Mb(hashMap);
        HashMap hashMap2 = new HashMap();
        C04990Nv r23 = new C04990Nv();
        r23.A04 = true;
        r23.A01 = 180.0f;
        hashMap2.put(-1, r23);
        C04990Nv r24 = new C04990Nv();
        r24.A02 = true;
        r24.A00 = 0.5f;
        r24.A03 = true;
        hashMap2.put(5, r24);
        r4[2] = new C04530Mb(hashMap2);
        HashMap hashMap3 = new HashMap();
        C04990Nv r25 = new C04990Nv();
        r25.A04 = true;
        r25.A01 = 270.0f;
        hashMap3.put(-1, r25);
        C04990Nv r26 = new C04990Nv();
        r26.A02 = true;
        r26.A00 = 0.5f;
        hashMap3.put(5, r26);
        C04990Nv r02 = new C04990Nv();
        r02.A03 = true;
        hashMap3.put(1, r02);
        r4[3] = new C04530Mb(hashMap3);
    }

    public final void A00(Canvas canvas, Paint paint, C04990Nv r7, C04990Nv r8, float f) {
        float f2;
        int alpha = paint.getAlpha();
        float f3 = 1.0f;
        if (r7 == null || !r7.A02) {
            f2 = 1.0f;
        } else {
            f2 = r7.A00;
        }
        if (r8 != null && r8.A02) {
            f3 = r8.A00;
        }
        float alpha2 = (float) paint.getAlpha();
        float f4 = f2 * alpha2;
        paint.setAlpha((int) (f4 + (this.A00 * ((f3 * alpha2) - f4))));
        canvas.drawCircle(0.0f, 0.0f, f, paint);
        paint.setAlpha(alpha);
    }

    public void A01(boolean z) {
        if (this.A06 != z) {
            ValueAnimator valueAnimator = this.A03;
            if (valueAnimator != null) {
                valueAnimator.cancel();
                this.A03 = null;
            }
            if (z) {
                ValueAnimator valueAnimator2 = new ValueAnimator();
                this.A03 = valueAnimator2;
                valueAnimator2.addUpdateListener(this.A0B);
                this.A03.setRepeatCount(-1);
                this.A03.setDuration(((long) this.A0E.length) * 3000);
                this.A03.setFloatValues(0.0f, 1.0f);
                this.A03.setInterpolator(new LinearInterpolator());
                this.A03.start();
            }
            this.A06 = z;
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        C04990Nv r2;
        C04990Nv r1;
        float f;
        float f2;
        C04990Nv r22;
        C04990Nv r12;
        float f3;
        float f4;
        int save = canvas.save();
        if (this.A08 != 0.0f) {
            canvas.clipPath(this.A0C);
        }
        if (this.A01 > 1.0f) {
            Rect bounds = getBounds();
            float f5 = this.A01;
            canvas.scale(f5, f5, bounds.exactCenterX(), bounds.exactCenterY());
        }
        canvas.drawColor(this.A09);
        C04530Mb r13 = this.A04;
        if (r13 != null) {
            r2 = (C04990Nv) r13.A00.get(-1);
        } else {
            r2 = null;
        }
        C04530Mb r14 = this.A05;
        if (r14 != null) {
            r1 = (C04990Nv) r14.A00.get(-1);
        } else {
            r1 = null;
        }
        if (r2 == null || !r2.A04) {
            f = 0.0f;
        } else {
            f = r2.A01;
        }
        if (r1 == null || !r1.A04) {
            f2 = 0.0f;
        } else {
            f2 = r1.A01;
        }
        if (f == 270.0f && f2 == 0.0f) {
            f2 = 360.0f;
        }
        float f6 = f + (this.A00 * (f2 - f));
        Rect bounds2 = getBounds();
        if (f6 != 0.0f) {
            canvas.rotate(f6, bounds2.exactCenterX(), bounds2.exactCenterY());
        }
        for (AnonymousClass0S2 r6 : this.A0D) {
            int save2 = canvas.save();
            C04530Mb r15 = this.A04;
            if (r15 != null) {
                r22 = (C04990Nv) r15.A00.get(Integer.valueOf(r6.A06));
            } else {
                r22 = null;
            }
            C04530Mb r16 = this.A05;
            if (r16 != null) {
                r12 = (C04990Nv) r16.A00.get(Integer.valueOf(r6.A06));
            } else {
                r12 = null;
            }
            float f7 = r6.A00;
            float exactCenterX = bounds2.exactCenterX();
            if (r22 == null || !r22.A03) {
                f3 = f7;
            } else {
                f3 = 0.0f + exactCenterX;
            }
            if (r12 != null && r12.A03) {
                f7 = exactCenterX + 0.0f;
            }
            float f8 = f3 + (this.A00 * (f7 - f3));
            float f9 = r6.A01;
            float exactCenterY = bounds2.exactCenterY();
            if (r22 == null || !r22.A03) {
                f4 = f9;
            } else {
                f4 = 0.0f + exactCenterY;
            }
            if (r12 != null && r12.A03) {
                f9 = exactCenterY + 0.0f;
            }
            canvas.translate(f8, f4 + (this.A00 * (f9 - f4)));
            float f10 = r6.A04;
            float f11 = r6.A05;
            float max = Math.max(f10, f11);
            canvas.scale(1.0f, Math.min(f10, f11) / max);
            A00(canvas, r6.A09, r22, r12, max * 2.0f);
            A00(canvas, r6.A08, r22, r12, max * 1.5f);
            A00(canvas, r6.A07, r22, r12, max);
            canvas.restoreToCount(save2);
        }
        canvas.restoreToCount(save);
    }

    @Override // android.graphics.drawable.Drawable
    public int getAlpha() {
        return this.A02;
    }

    @Override // android.graphics.drawable.Drawable
    public void onBoundsChange(Rect rect) {
        float f;
        super.onBoundsChange(rect);
        for (AnonymousClass0S2 r2 : this.A0D) {
            r2.A00 = ((float) rect.centerX()) + r2.A02;
            r2.A01 = ((float) rect.centerY()) + r2.A03;
        }
        Path path = this.A0C;
        path.reset();
        RectF rectF = new RectF(rect);
        float f2 = this.A08;
        path.addRoundRect(rectF, f2, f2, Path.Direction.CW);
        path.close();
        float width = (float) rect.width();
        float f3 = this.A07;
        if (width > f3) {
            f = width / f3;
        } else {
            f = 0.0f;
        }
        this.A01 = f;
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        this.A02 = i;
        for (AnonymousClass0S2 r1 : this.A0D) {
            r1.A07.setAlpha(i);
            r1.A08.setAlpha(i);
            r1.A09.setAlpha(i);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
        for (AnonymousClass0S2 r1 : this.A0D) {
            r1.A07.setColorFilter(colorFilter);
            r1.A08.setColorFilter(colorFilter);
            r1.A09.setColorFilter(colorFilter);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public boolean setVisible(boolean z, boolean z2) {
        ValueAnimator valueAnimator;
        ValueAnimator valueAnimator2;
        boolean z3 = this.A06;
        if (!z) {
            if (z3 && (valueAnimator2 = this.A03) != null && valueAnimator2.isStarted()) {
                this.A03.cancel();
            }
        } else if (z3 && (valueAnimator = this.A03) != null && !valueAnimator.isStarted()) {
            this.A03.start();
        }
        return super.setVisible(z, z2);
    }
}
