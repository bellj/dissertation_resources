package X;

import com.facebook.redex.IDxComparatorShape3S0000000_2_I1;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/* renamed from: X.4xt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107754xt implements AbstractC116805Wy {
    public final List A00;
    public final long[] A01;
    public final long[] A02;

    public C107754xt(List list) {
        this.A00 = Collections.unmodifiableList(C12980iv.A0x(list));
        this.A01 = new long[list.size() << 1];
        for (int i = 0; i < list.size(); i++) {
            AnonymousClass4PC r4 = (AnonymousClass4PC) list.get(i);
            int i2 = i << 1;
            long[] jArr = this.A01;
            jArr[i2] = r4.A01;
            jArr[i2 + 1] = r4.A00;
        }
        long[] jArr2 = this.A01;
        long[] copyOf = Arrays.copyOf(jArr2, jArr2.length);
        this.A02 = copyOf;
        Arrays.sort(copyOf);
    }

    @Override // X.AbstractC116805Wy
    public List ABx(long j) {
        ArrayList A0l = C12960it.A0l();
        ArrayList A0l2 = C12960it.A0l();
        int i = 0;
        while (true) {
            List list = this.A00;
            if (i >= list.size()) {
                break;
            }
            long[] jArr = this.A01;
            int i2 = i << 1;
            if (jArr[i2] <= j && j < jArr[i2 + 1]) {
                AnonymousClass4PC r6 = (AnonymousClass4PC) list.get(i);
                C93834ao r2 = r6.A02;
                if (r2.A01 == -3.4028235E38f) {
                    A0l2.add(r6);
                } else {
                    A0l.add(r2);
                }
            }
            i++;
        }
        Collections.sort(A0l2, new IDxComparatorShape3S0000000_2_I1(2));
        for (int i3 = 0; i3 < A0l2.size(); i3++) {
            C94144bK r22 = new C94144bK(((AnonymousClass4PC) A0l2.get(i3)).A02);
            r22.A01 = (float) (-1 - i3);
            r22.A07 = 1;
            A0l.add(r22.A00());
        }
        return A0l;
    }

    @Override // X.AbstractC116805Wy
    public long ACn(int i) {
        boolean z = true;
        C95314dV.A03(C12990iw.A1W(i));
        long[] jArr = this.A02;
        if (i >= jArr.length) {
            z = false;
        }
        C95314dV.A03(z);
        return jArr[i];
    }

    @Override // X.AbstractC116805Wy
    public int ACo() {
        return this.A02.length;
    }

    @Override // X.AbstractC116805Wy
    public int AEd(long j) {
        long[] jArr = this.A02;
        int A05 = AnonymousClass3JZ.A05(jArr, j, false);
        if (A05 >= jArr.length) {
            return -1;
        }
        return A05;
    }
}
