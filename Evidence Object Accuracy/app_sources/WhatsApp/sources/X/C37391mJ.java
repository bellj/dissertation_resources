package X;

/* renamed from: X.1mJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37391mJ extends AnonymousClass1JW {
    public final long A00;
    public final long A01;
    public final byte[] A02;

    public C37391mJ(AbstractC15710nm r2, C15450nH r3, AbstractC14640lm r4, byte[] bArr, long j, long j2) {
        super(r2, r3, false);
        this.A0C = r4;
        this.A01 = j;
        this.A00 = j2;
        this.A02 = bArr;
    }
}
