package X;

import android.text.TextUtils;
import com.facebook.redex.RunnableBRunnable0Shape0S1300000_I0;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.HashMap;

/* renamed from: X.2Jk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C49132Jk {
    public static C246516i A00 = new C246516i(16);
    public static HashMap A01 = new HashMap();

    public static void A00(C14900mE r6, C18790t3 r7, AbstractC49122Jj r8, AnonymousClass018 r9, AbstractC14440lR r10, String str) {
        C14320lF r0;
        if (!TextUtils.isEmpty(str)) {
            if (str == null || (r0 = (C14320lF) A00.get(str)) == null) {
                HashMap hashMap = A01;
                AbstractCollection abstractCollection = (AbstractCollection) hashMap.get(str);
                if (abstractCollection != null) {
                    abstractCollection.add(r8);
                    return;
                }
                ArrayList arrayList = new ArrayList(1);
                arrayList.add(r8);
                hashMap.put(str, arrayList);
                r10.Ab2(new RunnableBRunnable0Shape0S1300000_I0(new C14320lF(r7, str), r9, r6, str, 0));
                return;
            } else if (r8 == null) {
                return;
            }
        } else if (r8 != null) {
            r0 = null;
        } else {
            return;
        }
        r8.ATN(r0, true);
    }
}
