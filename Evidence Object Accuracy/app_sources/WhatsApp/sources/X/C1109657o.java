package X;

/* renamed from: X.57o  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1109657o implements AnonymousClass5W5 {
    public final /* synthetic */ AnonymousClass3FS A00;

    public C1109657o(AnonymousClass3FS r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5W5
    public void APL() {
        this.A00.A03 = true;
    }

    @Override // X.AnonymousClass5W5
    public void APM() {
        AnonymousClass3FS r1 = this.A00;
        r1.A03 = false;
        r1.A02(true);
    }
}
