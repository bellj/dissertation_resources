package X;

/* renamed from: X.0ai  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08180ai implements AbstractC12040hH {
    public final AnonymousClass0H9 A00;
    public final AnonymousClass0H9 A01;
    public final AnonymousClass0H9 A02;
    public final AnonymousClass0H9 A03;
    public final AnonymousClass0H9 A04;
    public final AnonymousClass0H9 A05;
    public final AbstractC12590iA A06;
    public final EnumC03790Jd A07;
    public final String A08;
    public final boolean A09;

    public C08180ai(AnonymousClass0H9 r1, AnonymousClass0H9 r2, AnonymousClass0H9 r3, AnonymousClass0H9 r4, AnonymousClass0H9 r5, AnonymousClass0H9 r6, AbstractC12590iA r7, EnumC03790Jd r8, String str, boolean z) {
        this.A08 = str;
        this.A07 = r8;
        this.A04 = r1;
        this.A06 = r7;
        this.A05 = r2;
        this.A00 = r3;
        this.A02 = r4;
        this.A01 = r5;
        this.A03 = r6;
        this.A09 = z;
    }

    @Override // X.AbstractC12040hH
    public AbstractC12470hy Aes(AnonymousClass0AA r2, AbstractC08070aX r3) {
        return new C08010aR(r2, this, r3);
    }
}
