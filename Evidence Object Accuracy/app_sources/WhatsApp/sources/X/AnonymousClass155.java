package X;

import android.content.SharedPreferences;
import android.os.Build;
import java.util.Iterator;
import java.util.List;
import org.chromium.net.UrlRequest;

/* renamed from: X.155  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass155 {
    public final C18640sm A00;
    public final C14830m7 A01;
    public final C14850m9 A02;
    public final AnonymousClass154 A03;

    public AnonymousClass155(C18640sm r1, C14830m7 r2, C14850m9 r3, AnonymousClass154 r4) {
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = r4;
        this.A00 = r1;
    }

    public static int A00(AnonymousClass1I0 r4) {
        if (r4 != null) {
            int i = r4.A00;
            if (r4.A04) {
                if (Build.VERSION.SDK_INT >= 29 && i == 20) {
                    return 3;
                }
                switch (i) {
                    case 1:
                    case 2:
                    case 4:
                    case 7:
                    case 11:
                        return 0;
                    case 3:
                    case 5:
                    case 6:
                    case 8:
                    case 9:
                    case 10:
                    case 12:
                    case UrlRequest.Status.READING_RESPONSE /* 14 */:
                        return 1;
                    case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                    case 15:
                        return 2;
                    default:
                        return 5;
                }
            } else if (r4.A06) {
                return 4;
            }
        }
        return 5;
    }

    public synchronized Float A01(int i) {
        Float f;
        float f2;
        float f3;
        int i2;
        int A00 = A00(this.A00.A07());
        int A002 = (int) ((((this.A01.A00() / 1000) / 60) / 60) % 24);
        AnonymousClass154 r2 = this.A03;
        C37821n4 r6 = new C37821n4(r2.A01(i, A002, A00));
        List list = r6.A00;
        boolean z = false;
        if (list.size() >= 10) {
            z = true;
        }
        if (z || !this.A02.A07(154)) {
            if (list.size() >= 10) {
                f2 = (float) C37821n4.A00(list);
            } else {
                f2 = -1.0f;
            }
            f = Float.valueOf(f2);
        } else {
            List A01 = r2.A01(i, (A002 + 23) % 24, A00);
            List A012 = r2.A01(i, (A002 + 1) % 24, A00);
            Iterator it = A01.iterator();
            Iterator it2 = A012.iterator();
            boolean z2 = false;
            while (true) {
                boolean z3 = false;
                if (list.size() >= 10) {
                    z3 = true;
                }
                if (!z3) {
                    if (z2 || !it.hasNext()) {
                        if (!it2.hasNext()) {
                            if (!it.hasNext()) {
                                f = null;
                                break;
                            }
                            i2 = ((Integer) it.next()).intValue();
                        } else {
                            i2 = ((Integer) it2.next()).intValue();
                            z2 = false;
                        }
                    } else {
                        i2 = ((Integer) it.next()).intValue();
                        z2 = true;
                    }
                    r6.A02(i2);
                } else {
                    if (list.size() >= 10) {
                        f3 = (float) C37821n4.A00(list);
                    } else {
                        f3 = -1.0f;
                    }
                    f = Float.valueOf(f3);
                }
            }
        }
        return f;
    }

    public synchronized void A02(C14370lK r7, int i, long j, long j2) {
        SharedPreferences sharedPreferences;
        int A00 = A00(this.A00.A07());
        int A002 = (int) ((((this.A01.A00() / 1000) / 60) / 60) % 24);
        if (!(A00 == 0 || A00 == 5 || (!(r7 == C14370lK.A0B || r7 == C14370lK.A0G || r7 == C14370lK.A0Z) || j < 51200 || j2 < 100))) {
            AnonymousClass154 r2 = this.A03;
            C37821n4 r1 = new C37821n4(r2.A01(i, A002, A00));
            r1.A02((int) (((float) j) / ((float) j2)));
            List<Integer> list = r1.A00;
            StringBuilder sb = new StringBuilder();
            for (Integer num : list) {
                sb.append(num.intValue());
                sb.append(",");
            }
            synchronized (r2) {
                sharedPreferences = r2.A00;
                if (sharedPreferences == null) {
                    sharedPreferences = r2.A01.A01("media_bandwidth_shared_preferences_v2");
                    r2.A00 = sharedPreferences;
                }
            }
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putString(AnonymousClass154.A00(i, A002, A00), sb.toString());
            edit.apply();
        }
    }
}
