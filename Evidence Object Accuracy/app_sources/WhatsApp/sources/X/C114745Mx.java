package X;

/* renamed from: X.5Mx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114745Mx extends AnonymousClass1TM implements AbstractC115545Ry {
    public AnonymousClass1TN A00;

    public C114745Mx(AnonymousClass5N2 r1) {
        this.A00 = r1;
    }

    public C114745Mx(AnonymousClass5NH r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        AnonymousClass1TN r3 = this.A00;
        return r3 instanceof AnonymousClass5NH ? new C114835Ng(r3, 2, true) : new C114835Ng(r3, 1, true);
    }
}
