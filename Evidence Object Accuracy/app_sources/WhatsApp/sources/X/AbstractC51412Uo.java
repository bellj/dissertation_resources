package X;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.widget.ImageView;
import android.widget.TextView;
import com.facebook.redex.ViewOnClickCListenerShape3S0100000_I0_3;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.quickcontact.QuickContactActivity;
import com.whatsapp.util.ViewOnClickCListenerShape2S0110000_I0;

/* renamed from: X.2Uo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC51412Uo {
    public final /* synthetic */ QuickContactActivity A00;

    public /* synthetic */ AbstractC51412Uo(QuickContactActivity quickContactActivity) {
        this.A00 = quickContactActivity;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:109:0x01e6, code lost:
        if (X.AnonymousClass1SF.A0J(((X.ActivityC13810kN) r1).A06, r1.A0J, r1.A0K, r7, com.whatsapp.jid.GroupJid.of(r7.A0D)) != false) goto L_0x0183;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x020b, code lost:
        if (X.AnonymousClass1SF.A0I(((X.ActivityC13790kL) r1).A01, ((X.ActivityC13810kN) r1).A06, r1.A0B, r1.A0K, r1.A0M, r1.A0P, r10) != false) goto L_0x0166;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0082, code lost:
        if (r0 == false) goto L_0x0084;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0100, code lost:
        if (r4.A0k == null) goto L_0x0102;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0164, code lost:
        if (r4.A0F(r5) == false) goto L_0x0166;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00() {
        /*
        // Method dump skipped, instructions count: 552
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC51412Uo.A00():void");
    }

    public void A01() {
        QuickContactActivity quickContactActivity = this.A00;
        if (quickContactActivity.A0M.A0K()) {
            Intent A0O = C14960mK.A0O(quickContactActivity, quickContactActivity.A0M.A0D, true, false, true);
            C35741ib.A00(A0O, "QuickContactActivity");
            quickContactActivity.startActivity(A0O);
        } else {
            C15370n3 r1 = quickContactActivity.A0M;
            Integer num = null;
            if (C15380n4.A0F(r1.A0D)) {
                quickContactActivity.startActivity(C14960mK.A0K(quickContactActivity, r1.A0D), null);
            } else {
                Integer valueOf = Integer.valueOf(quickContactActivity.getIntent().getIntExtra("profile_entry_point", -1));
                if (valueOf.intValue() != -1) {
                    num = valueOf;
                }
                quickContactActivity.startActivity(new C14960mK().A0h(quickContactActivity, quickContactActivity.A0M, num));
            }
        }
        quickContactActivity.A2g(false);
    }

    public void A02() {
        QuickContactActivity quickContactActivity = this.A00;
        quickContactActivity.A04.setOnClickListener(new ViewOnClickCListenerShape3S0100000_I0_3(this, 25));
        quickContactActivity.A00.setOnTouchListener(new AnonymousClass2E0(0.2f, 0.0f, 0.2f, 0.0f));
        quickContactActivity.A06.setOnTouchListener(new AnonymousClass2E0(0.2f, 0.0f, 0.2f, 0.0f));
        quickContactActivity.A01.setOnTouchListener(new AnonymousClass2E0(0.2f, 0.0f, 0.2f, 0.0f));
        quickContactActivity.A00.setOnClickListener(new ViewOnClickCListenerShape2S0110000_I0(quickContactActivity, 2, false));
        quickContactActivity.A06.setOnClickListener(new ViewOnClickCListenerShape2S0110000_I0(quickContactActivity, 2, true));
        quickContactActivity.A01.setOnClickListener(new ViewOnClickCListenerShape3S0100000_I0_3(this, 27));
        quickContactActivity.A03.setOnClickListener(new ViewOnClickCListenerShape3S0100000_I0_3(this, 26));
        quickContactActivity.A05.setOnClickListener(new ViewOnClickCListenerShape3S0100000_I0_3(this, 24));
    }

    public void A03() {
        TextView textView;
        String A09;
        if (!(this instanceof AnonymousClass34S)) {
            C51402Un r6 = (C51402Un) this;
            QuickContactActivity quickContactActivity = r6.A04;
            int i = 0;
            if (quickContactActivity.A0M.A0I()) {
                textView = r6.A02;
                A09 = ((ActivityC13830kP) quickContactActivity).A01.A0G(C248917h.A01(quickContactActivity.A0M));
            } else if (quickContactActivity.A0M.A0K()) {
                boolean A0b = quickContactActivity.A0P.A0b(C15580nU.A02(quickContactActivity.A0M.A0D));
                int i2 = R.plurals.quick_contact_group_participants_info;
                if (A0b) {
                    i2 = R.plurals.quick_contact_parent_group_participants_info;
                }
                int A00 = quickContactActivity.A0K.A00((AbstractC15590nW) quickContactActivity.A0M.A0B(GroupJid.class));
                r6.A02.setText(quickContactActivity.getResources().getQuantityString(i2, A00, Integer.valueOf(A00)));
                textView = r6.A02;
                textView.setVisibility(i);
                return;
            } else {
                boolean A03 = C15610nY.A03(quickContactActivity.A0M);
                textView = r6.A02;
                if (A03) {
                    A09 = quickContactActivity.A0D.A09(quickContactActivity.A0M);
                } else {
                    i = 8;
                    textView.setVisibility(i);
                    return;
                }
            }
            textView.setText(A09);
            textView = r6.A02;
            textView.setVisibility(i);
            return;
        }
        QuickContactActivity quickContactActivity2 = ((AnonymousClass34S) this).A00;
        new C28801Pb(quickContactActivity2, (TextEmojiLabel) quickContactActivity2.findViewById(R.id.name), quickContactActivity2.A0D, quickContactActivity2.A0Z).A06(quickContactActivity2.A0M);
    }

    public void A04() {
        AbstractC14640lm r1;
        ImageView imageView;
        AbstractC14640lm r12;
        QuickContactActivity quickContactActivity = this.A00;
        Jid A0B = quickContactActivity.A0M.A0B(GroupJid.class);
        float f = 0.0f;
        if (((ActivityC13810kN) quickContactActivity).A0C.A07(604)) {
            f = -1.0f;
        }
        if (quickContactActivity.A0P.A0b(C15580nU.A02(A0B))) {
            f = -2.14748365E9f;
        }
        Bitmap A00 = quickContactActivity.A0G.A00(quickContactActivity, quickContactActivity.A0M, f, quickContactActivity.getResources().getDimensionPixelSize(R.dimen.quick_contact_profile_photo_size));
        if (A00 != null) {
            imageView = quickContactActivity.A07;
        } else if (!(this instanceof AnonymousClass34S)) {
            QuickContactActivity quickContactActivity2 = ((C51402Un) this).A04;
            AnonymousClass130 r2 = quickContactActivity2.A0A;
            Jid jid = quickContactActivity2.A0M.A0D;
            if (jid instanceof AbstractC14640lm) {
                r12 = (AbstractC14640lm) jid;
            } else {
                r12 = null;
            }
            int A02 = r2.A02(r12);
            AbstractC469028d r4 = AnonymousClass51L.A00;
            if (quickContactActivity2.A0P.A0b(C15580nU.A02(quickContactActivity2.A0M.A0D))) {
                r4 = AnonymousClass51K.A00;
            }
            quickContactActivity2.A07.setImageDrawable(quickContactActivity2.A0N.A00(quickContactActivity2.getTheme(), quickContactActivity2.getResources(), r4, A02));
            return;
        } else {
            QuickContactActivity quickContactActivity3 = ((AnonymousClass34S) this).A00;
            AnonymousClass130 r22 = quickContactActivity3.A0A;
            Jid jid2 = quickContactActivity3.A0M.A0D;
            if (jid2 instanceof AbstractC14640lm) {
                r1 = (AbstractC14640lm) jid2;
            } else {
                r1 = null;
            }
            A00 = quickContactActivity3.A0A.A03(quickContactActivity3.A07.getContext(), r22.A02(r1));
            if (!(quickContactActivity3.A07.getDrawable() instanceof BitmapDrawable) || ((BitmapDrawable) quickContactActivity3.A07.getDrawable()).getBitmap() != A00) {
                imageView = quickContactActivity3.A07;
            } else {
                return;
            }
        }
        imageView.setImageBitmap(A00);
    }

    public boolean A05() {
        QuickContactActivity quickContactActivity = this.A00;
        if (quickContactActivity.A0M.A0K() || C15380n4.A0F(quickContactActivity.A0M.A0D)) {
            if (!quickContactActivity.A0M.A0K() || !AnonymousClass1SF.A0P(((ActivityC13810kN) quickContactActivity).A0C)) {
                return false;
            }
            C15580nU r2 = quickContactActivity.A0R;
            C15370n3 r0 = quickContactActivity.A0M;
            C19990v2 r1 = quickContactActivity.A0J;
            if (r2 == null || r0.A0Y || r1.A02(r2) == 3) {
                return false;
            }
        }
        return true;
    }
}
