package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.1JI  reason: invalid class name */
/* loaded from: classes2.dex */
public interface AnonymousClass1JI {
    void AOv(UserJid userJid);

    void APn(UserJid userJid, int i);

    void AT4(UserJid userJid);

    void AWV(UserJid userJid, String str, long j);
}
