package X;

import android.content.Context;
import com.airbnb.lottie.LottieAnimationView;
import java.util.concurrent.Callable;

/* renamed from: X.0ei  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class CallableC10490ei implements Callable {
    public final /* synthetic */ LottieAnimationView A00;
    public final /* synthetic */ String A01;

    public CallableC10490ei(LottieAnimationView lottieAnimationView, String str) {
        this.A00 = lottieAnimationView;
        this.A01 = str;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        String str;
        LottieAnimationView lottieAnimationView = this.A00;
        boolean z = lottieAnimationView.A09;
        Context context = lottieAnimationView.getContext();
        String str2 = this.A01;
        if (z) {
            StringBuilder sb = new StringBuilder("asset_");
            sb.append(str2);
            str = sb.toString();
        } else {
            str = null;
        }
        return C06550Ub.A01(context, str2, str);
    }
}
