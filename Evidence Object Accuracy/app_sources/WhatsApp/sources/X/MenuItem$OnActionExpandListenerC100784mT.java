package X;

import android.view.MenuItem;

/* renamed from: X.4mT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class MenuItem$OnActionExpandListenerC100784mT implements MenuItem.OnActionExpandListener {
    public final /* synthetic */ AbstractActivityC35431hr A00;

    @Override // android.view.MenuItem.OnActionExpandListener
    public boolean onMenuItemActionExpand(MenuItem menuItem) {
        return true;
    }

    public MenuItem$OnActionExpandListenerC100784mT(AbstractActivityC35431hr r1) {
        this.A00 = r1;
    }

    @Override // android.view.MenuItem.OnActionExpandListener
    public boolean onMenuItemActionCollapse(MenuItem menuItem) {
        this.A00.A0P = null;
        return true;
    }
}
