package X;

import com.whatsapp.notification.PopupNotification;
import java.util.Set;

/* renamed from: X.44l  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C858644l extends AbstractC33331dp {
    public final /* synthetic */ PopupNotification A00;

    public C858644l(PopupNotification popupNotification) {
        this.A00 = popupNotification;
    }

    @Override // X.AbstractC33331dp
    public void A00(Set set) {
        PopupNotification.A03(this.A00);
    }
}
