package X;

import java.util.Collection;
import java.util.List;

/* renamed from: X.58G  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass58G implements AnonymousClass5UW {
    public static final AnonymousClass58Y A01 = new AnonymousClass58Y();
    public final List A00;

    public AnonymousClass58G(List list) {
        this.A00 = list;
    }

    @Override // X.AnonymousClass5UW
    public boolean A9g(AnonymousClass4V4 r4) {
        C16700pc.A0E(r4, 0);
        List<AnonymousClass5UW> list = this.A00;
        if (!(!list.isEmpty())) {
            return false;
        }
        if ((list instanceof Collection) && list.isEmpty()) {
            return false;
        }
        for (AnonymousClass5UW r0 : list) {
            if (r0.A9g(r4)) {
                return true;
            }
        }
        return false;
    }
}
