package X;

/* renamed from: X.4Ag  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public enum EnumC87114Ag {
    FLEX_START,
    CENTER,
    FLEX_END,
    STRETCH,
    BASELINE,
    SPACE_BETWEEN,
    SPACE_AROUND
}
