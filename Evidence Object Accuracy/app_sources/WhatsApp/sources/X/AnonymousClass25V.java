package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.25V  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass25V extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass25V A01;
    public static volatile AnonymousClass255 A02;
    public AnonymousClass1K6 A00 = AnonymousClass277.A01;

    static {
        AnonymousClass25V r0 = new AnonymousClass25V();
        A01 = r0;
        r0.A0W();
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < this.A00.size(); i3++) {
            i2 += CodedOutputStream.A0A((AnonymousClass1G1) this.A00.get(i3), 1);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        for (int i = 0; i < this.A00.size(); i++) {
            codedOutputStream.A0L((AnonymousClass1G1) this.A00.get(i), 1);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
