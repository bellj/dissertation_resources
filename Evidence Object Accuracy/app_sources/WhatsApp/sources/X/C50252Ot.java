package X;

/* renamed from: X.2Ot  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C50252Ot extends AbstractC16110oT {
    public Integer A00;
    public Long A01;
    public Long A02;
    public Long A03;

    public C50252Ot() {
        super(3256, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A01);
        r3.Abe(3, this.A00);
        r3.Abe(5, this.A02);
        r3.Abe(4, this.A03);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        StringBuilder sb = new StringBuilder("WamPrivacyNoticeBadging {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "noticeBadgingId", this.A01);
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "noticeBadgingStage", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "noticeBadgingStarttime", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "noticeBadgingVersion", this.A03);
        sb.append("}");
        return sb.toString();
    }
}
