package X;

import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Handler;

/* renamed from: X.09n  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C020009n extends ContentObserver {
    public final /* synthetic */ AnonymousClass0BR A00;

    @Override // android.database.ContentObserver
    public boolean deliverSelfNotifications() {
        return true;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C020009n(AnonymousClass0BR r2) {
        super(new Handler());
        this.A00 = r2;
    }

    @Override // android.database.ContentObserver
    public void onChange(boolean z) {
        Cursor cursor;
        AnonymousClass0BR r1 = this.A00;
        if (r1.A06 && (cursor = r1.A02) != null && !cursor.isClosed()) {
            r1.A07 = r1.A02.requery();
        }
    }
}
