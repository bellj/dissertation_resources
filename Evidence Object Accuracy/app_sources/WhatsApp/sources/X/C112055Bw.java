package X;

import java.lang.reflect.Method;
import java.security.PrivilegedExceptionAction;
import java.security.spec.AlgorithmParameterSpec;

/* renamed from: X.5Bw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C112055Bw implements PrivilegedExceptionAction {
    public final /* synthetic */ AlgorithmParameterSpec A00;

    public C112055Bw(AlgorithmParameterSpec algorithmParameterSpec) {
        this.A00 = algorithmParameterSpec;
    }

    @Override // java.security.PrivilegedExceptionAction
    public Object run() {
        Method method = C94694cN.A01;
        AlgorithmParameterSpec algorithmParameterSpec = this.A00;
        return new C114635Mm((byte[]) method.invoke(algorithmParameterSpec, new Object[0]), C12960it.A05(C94694cN.A02.invoke(algorithmParameterSpec, new Object[0])) >> 3);
    }
}
