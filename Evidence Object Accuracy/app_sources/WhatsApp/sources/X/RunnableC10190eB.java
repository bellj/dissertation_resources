package X;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.util.Xml;
import androidx.sharetarget.ShortcutInfoCompatSaverImpl;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;

/* renamed from: X.0eB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC10190eB implements Runnable {
    public final /* synthetic */ ShortcutInfoCompatSaverImpl A00;
    public final /* synthetic */ File A01;

    public RunnableC10190eB(ShortcutInfoCompatSaverImpl shortcutInfoCompatSaverImpl, File file) {
        this.A00 = shortcutInfoCompatSaverImpl;
        this.A01 = file;
    }

    public static String A00(String str, XmlPullParser xmlPullParser) {
        String attributeValue = xmlPullParser.getAttributeValue("http://schemas.android.com/apk/res/android", str);
        return attributeValue == null ? xmlPullParser.getAttributeValue(null, str) : attributeValue;
    }

    @Override // java.lang.Runnable
    public void run() {
        try {
            File file = this.A01;
            if ((!file.exists() || file.isDirectory() || file.delete()) && !file.exists()) {
                file.mkdirs();
            }
            ShortcutInfoCompatSaverImpl shortcutInfoCompatSaverImpl = this.A00;
            File file2 = shortcutInfoCompatSaverImpl.A01;
            if ((!file2.exists() || file2.isDirectory() || file2.delete()) && !file2.exists()) {
                file2.mkdirs();
            }
            Map map = shortcutInfoCompatSaverImpl.A04;
            File file3 = shortcutInfoCompatSaverImpl.A02;
            Context context = shortcutInfoCompatSaverImpl.A00;
            AnonymousClass00N r3 = new AnonymousClass00N();
            try {
                FileInputStream fileInputStream = new FileInputStream(file3);
                try {
                    if (file3.exists()) {
                        XmlPullParser newPullParser = Xml.newPullParser();
                        newPullParser.setInput(fileInputStream, "UTF_8");
                        while (true) {
                            int next = newPullParser.next();
                            if (next == 1) {
                                break;
                            } else if (next == 2 && newPullParser.getName().equals("target") && newPullParser.getName().equals("target")) {
                                String A00 = A00("id", newPullParser);
                                String A002 = A00("short_label", newPullParser);
                                if (!TextUtils.isEmpty(A00) && !TextUtils.isEmpty(A002)) {
                                    int parseInt = Integer.parseInt(A00("rank", newPullParser));
                                    String A003 = A00("long_label", newPullParser);
                                    String A004 = A00("disabled_message", newPullParser);
                                    String A005 = A00("component", newPullParser);
                                    ComponentName unflattenFromString = TextUtils.isEmpty(A005) ? null : ComponentName.unflattenFromString(A005);
                                    String A006 = A00("icon_resource_name", newPullParser);
                                    String A007 = A00("icon_bitmap_path", newPullParser);
                                    ArrayList arrayList = new ArrayList();
                                    HashSet hashSet = new HashSet();
                                    while (true) {
                                        int next2 = newPullParser.next();
                                        if (next2 != 1) {
                                            if (next2 != 2) {
                                                if (next2 == 3 && newPullParser.getName().equals("target")) {
                                                    break;
                                                }
                                            } else {
                                                String name = newPullParser.getName();
                                                if (name.equals("intent")) {
                                                    String A008 = A00("action", newPullParser);
                                                    String A009 = A00("targetPackage", newPullParser);
                                                    String A0010 = A00("targetClass", newPullParser);
                                                    if (A008 != null) {
                                                        Intent intent = new Intent(A008);
                                                        if (!TextUtils.isEmpty(A009) && !TextUtils.isEmpty(A0010)) {
                                                            intent.setClassName(A009, A0010);
                                                        }
                                                        arrayList.add(intent);
                                                    }
                                                } else if (name.equals("categories")) {
                                                    String A0011 = A00("name", newPullParser);
                                                    if (!TextUtils.isEmpty(A0011)) {
                                                        hashSet.add(A0011);
                                                    }
                                                }
                                            }
                                        } else {
                                            break;
                                        }
                                    }
                                    AnonymousClass03w r7 = new AnonymousClass03w(context, A00);
                                    C007603x r1 = r7.A00;
                                    r1.A0B = A002;
                                    r1.A02 = parseInt;
                                    if (!TextUtils.isEmpty(A003)) {
                                        r1.A0C = A003;
                                    }
                                    if (!TextUtils.isEmpty(A004)) {
                                        r1.A0A = A004;
                                    }
                                    if (unflattenFromString != null) {
                                        r1.A04 = unflattenFromString;
                                    }
                                    if (!arrayList.isEmpty()) {
                                        r1.A0P = (Intent[]) arrayList.toArray(new Intent[0]);
                                    }
                                    if (!hashSet.isEmpty()) {
                                        r1.A0F = hashSet;
                                    }
                                    AnonymousClass0NL r12 = new AnonymousClass0NL(r7.A00(), A006, A007);
                                    r3.put(r12.A00.A0D, r12);
                                }
                            }
                        }
                    }
                    fileInputStream.close();
                } catch (Throwable th) {
                    try {
                        fileInputStream.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            } catch (Exception e) {
                file3.delete();
                StringBuilder sb = new StringBuilder("Failed to load saved values from file ");
                sb.append(file3.getAbsolutePath());
                sb.append(". Old state removed, new added");
                Log.e("ShortcutInfoCompatSaver", sb.toString(), e);
            }
            map.putAll(r3);
            shortcutInfoCompatSaverImpl.A05(new ArrayList(map.values()));
        } catch (Exception e2) {
            Log.w("ShortcutInfoCompatSaver", "ShortcutInfoCompatSaver started with an exceptions ", e2);
        }
    }
}
