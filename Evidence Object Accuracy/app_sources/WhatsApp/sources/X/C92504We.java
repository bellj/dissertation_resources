package X;

/* renamed from: X.4We  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C92504We {
    public final int A00;
    public final boolean A01;

    public C92504We(int i, boolean z) {
        this.A00 = i;
        this.A01 = z;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C92504We.class != obj.getClass()) {
                return false;
            }
            C92504We r5 = (C92504We) obj;
            if (!(this.A00 == r5.A00 && this.A01 == r5.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return (this.A00 * 31) + (this.A01 ? 1 : 0);
    }
}
