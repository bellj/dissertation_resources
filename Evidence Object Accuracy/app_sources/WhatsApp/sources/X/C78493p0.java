package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3p0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78493p0 extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C98644j1();
    public final long A00;
    public final long A01;
    public final boolean A02;

    public C78493p0(long j, long j2, boolean z) {
        this.A02 = z;
        this.A00 = j;
        this.A01 = j2;
    }

    @Override // java.lang.Object
    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C78493p0) {
                C78493p0 r8 = (C78493p0) obj;
                if (!(this.A02 == r8.A02 && this.A00 == r8.A00 && this.A01 == r8.A01)) {
                }
            }
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public final int hashCode() {
        Object[] objArr = new Object[3];
        objArr[0] = Boolean.valueOf(this.A02);
        objArr[1] = Long.valueOf(this.A00);
        return C12980iv.A0B(Long.valueOf(this.A01), objArr, 2);
    }

    @Override // java.lang.Object
    public final String toString() {
        StringBuilder A0k = C12960it.A0k("CollectForDebugParcelable[skipPersistentStorage: ");
        A0k.append(this.A02);
        A0k.append(",collectForDebugStartTimeMillis: ");
        A0k.append(this.A00);
        A0k.append(",collectForDebugExpiryTimeMillis: ");
        A0k.append(this.A01);
        return C12960it.A0d("]", A0k);
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A09(parcel, 1, this.A02);
        C95654e8.A08(parcel, 2, this.A01);
        C95654e8.A08(parcel, 3, this.A00);
        C95654e8.A06(parcel, A00);
    }
}
