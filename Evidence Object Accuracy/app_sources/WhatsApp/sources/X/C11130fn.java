package X;

import java.util.concurrent.CancellationException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;

/* renamed from: X.0fn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C11130fn extends AbstractC114195Ko implements AbstractC11610gZ {
    public final Executor A00;

    public C11130fn(Executor executor) {
        this.A00 = executor;
        AnonymousClass4ZK.A00(executor);
    }

    public static final void A02(RejectedExecutionException rejectedExecutionException, AnonymousClass5X4 r3) {
        CancellationException cancellationException = new CancellationException("The task was rejected");
        cancellationException.initCause(rejectedExecutionException);
        AnonymousClass0LZ.A00(cancellationException, r3);
    }

    @Override // X.AbstractC10990fX
    public void A04(Runnable runnable, AnonymousClass5X4 r3) {
        try {
            this.A00.execute(runnable);
        } catch (RejectedExecutionException e) {
            A02(e, r3);
            AnonymousClass4ZY.A00().A04(runnable, r3);
        }
    }

    @Override // X.AbstractC114195Ko, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        ExecutorService executorService;
        Executor executor = this.A00;
        if ((executor instanceof ExecutorService) && (executorService = (ExecutorService) executor) != null) {
            executorService.shutdown();
        }
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        return (obj instanceof C11130fn) && ((C11130fn) obj).A00 == this.A00;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return System.identityHashCode(this.A00);
    }

    @Override // X.AbstractC10990fX, java.lang.Object
    public String toString() {
        return this.A00.toString();
    }
}
