package X;

import android.graphics.PointF;

/* renamed from: X.0aw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08320aw implements AbstractC12050hI {
    public static final C08320aw A00 = new C08320aw();

    @Override // X.AbstractC12050hI
    public Object AYt(AbstractC08850bx r5, float f) {
        EnumC03770Jb A05 = r5.A05();
        if (A05 == EnumC03770Jb.BEGIN_ARRAY || A05 == EnumC03770Jb.BEGIN_OBJECT) {
            return AnonymousClass0UD.A02(r5, f);
        }
        if (A05 == EnumC03770Jb.NUMBER) {
            PointF pointF = new PointF(((float) r5.A02()) * f, ((float) r5.A02()) * f);
            while (r5.A0H()) {
                r5.A0E();
            }
            return pointF;
        }
        StringBuilder sb = new StringBuilder("Cannot convert json to point. Next token is ");
        sb.append(A05);
        throw new IllegalArgumentException(sb.toString());
    }
}
