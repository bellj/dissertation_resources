package X;

import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.2tu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C59062tu extends AbstractC59082tw {
    public final C63913Dk A00;
    public final AnonymousClass4Tm A01;
    public final AnonymousClass2SJ A02;
    public final C18640sm A03;
    public final C15680nj A04;
    public final C19870uo A05;
    public final C17220qS A06;
    public final C19840ul A07;

    public C59062tu(C14650lo r2, C63913Dk r3, AnonymousClass4Tm r4, AnonymousClass2SJ r5, C18640sm r6, C15680nj r7, C19870uo r8, C17220qS r9, C19840ul r10) {
        super(r2, r4.A05);
        this.A00 = r3;
        this.A01 = r4;
        this.A07 = r10;
        this.A06 = r9;
        this.A02 = r5;
        this.A04 = r7;
        this.A03 = r6;
        this.A05 = r8;
    }

    public void A06() {
        if (!this.A03.A0B()) {
            this.A00.A00(-1);
        } else if (super.A01.A08()) {
            A02();
        } else {
            A03();
        }
    }

    public final void A07() {
        AnonymousClass4Tm r1 = this.A01;
        if (r1.A06 == null) {
            int i = r1.A02;
            C19840ul r12 = this.A07;
            if (i == 0) {
                r12.A02("collection_management_view_tag");
                return;
            }
            AnonymousClass1Q5 A00 = C19840ul.A00(r12);
            if (A00 != null) {
                A00.A07("datasource_collections");
            }
        }
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        A07();
        Log.e("GetCollectionsProtocol/onDeliveryFailure/delivery-error");
        this.A00.A00(-1);
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r7, String str) {
        A07();
        AnonymousClass2SJ r5 = this.A02;
        AnonymousClass1V8 A0E = r7.A0E("collections");
        if (A0E != null) {
            List<AnonymousClass1V8> A0J = A0E.A0J("collection");
            ArrayList A0l = C12960it.A0l();
            for (AnonymousClass1V8 r1 : A0J) {
                if (r5.A01(r1) != null) {
                    A0l.add(r5.A01(r1));
                }
            }
            this.A00.A01(new C44651zK(AnonymousClass3HN.A00(A0E.A0E("paging")), A0l), this.A01);
            return;
        }
        this.A00.A00(0);
    }
}
