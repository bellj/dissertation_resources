package X;

import android.view.animation.Animation;

/* renamed from: X.3wp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83183wp extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ C48232Fc A00;

    public C83183wp(C48232Fc r1) {
        this.A00 = r1;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        this.A00.A02.setIconified(false);
    }
}
