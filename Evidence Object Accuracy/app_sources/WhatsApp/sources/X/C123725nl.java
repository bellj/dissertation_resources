package X;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

/* renamed from: X.5nl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123725nl extends C128955ww {
    public C123725nl(Context context) {
        super(context);
    }

    @Override // X.C128955ww
    public View A00(ViewGroup viewGroup, AnonymousClass1IR r5) {
        int i;
        if (r5.A01 == 3 && ((i = r5.A03) == 1 || i == 2 || i == 3 || i == 4 || i == 6 || i == 7 || i == 8 || i == 10 || i == 20 || i == 30 || i == 40 || i == 100 || i == 200 || i == 300)) {
            return new C123685nf(this.A01);
        }
        return super.A00(viewGroup, r5);
    }
}
