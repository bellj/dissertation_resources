package X;

import com.google.android.search.verification.client.SearchActionVerificationClientService;
import java.util.LinkedList;

/* renamed from: X.1ut  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C42241ut {
    public static final int[] A01 = {0, 0, 0, 2000, 5000, SearchActionVerificationClientService.NOTIFICATION_ID, 20000, 40000, 80000, 160000};
    public LinkedList A00;

    public C42241ut() {
        synchronized (this) {
            this.A00 = new LinkedList();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003f, code lost:
        return (((java.lang.Long) r6.get(r3)).longValue() + r4) - android.os.SystemClock.elapsedRealtime();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x002c, code lost:
        if (r3 <= 0) goto L_0x0040;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized long A00() {
        /*
            r9 = this;
            monitor-enter(r9)
            java.util.LinkedList r6 = r9.A00     // Catch: all -> 0x0044
            int r3 = r6.size()     // Catch: all -> 0x0044
        L_0x0007:
            int r3 = r3 + -1
            if (r3 < 0) goto L_0x0040
            long r7 = android.os.SystemClock.elapsedRealtime()     // Catch: all -> 0x0044
            java.lang.Object r0 = r6.get(r3)     // Catch: all -> 0x0044
            java.lang.Long r0 = (java.lang.Long) r0     // Catch: all -> 0x0044
            long r0 = r0.longValue()     // Catch: all -> 0x0044
            long r7 = r7 - r0
            int[] r2 = X.C42241ut.A01     // Catch: all -> 0x0044
            int r1 = r3 + 1
            int r0 = r2.length     // Catch: all -> 0x0044
            int r0 = r0 + -1
            int r0 = java.lang.Math.min(r1, r0)     // Catch: all -> 0x0044
            r0 = r2[r0]     // Catch: all -> 0x0044
            long r4 = (long) r0     // Catch: all -> 0x0044
            int r0 = (r4 > r7 ? 1 : (r4 == r7 ? 0 : -1))
            if (r0 <= 0) goto L_0x0007
            if (r3 <= 0) goto L_0x0040
            java.lang.Object r0 = r6.get(r3)     // Catch: all -> 0x0044
            java.lang.Long r0 = (java.lang.Long) r0     // Catch: all -> 0x0044
            long r2 = r0.longValue()     // Catch: all -> 0x0044
            long r2 = r2 + r4
            long r0 = android.os.SystemClock.elapsedRealtime()     // Catch: all -> 0x0044
            long r2 = r2 - r0
            monitor-exit(r9)
            return r2
        L_0x0040:
            r0 = 0
            monitor-exit(r9)
            return r0
        L_0x0044:
            r0 = move-exception
            monitor-exit(r9)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C42241ut.A00():long");
    }
}
