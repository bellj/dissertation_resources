package X;

/* renamed from: X.4C9  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4C9 extends Exception {
    public final String message;
    public final /* synthetic */ AnonymousClass393 this$0;

    public AnonymousClass4C9(AnonymousClass393 r1, String str) {
        this.this$0 = r1;
        this.message = str;
    }

    @Override // java.lang.Throwable, java.lang.Object
    public String toString() {
        return this.message;
    }
}
