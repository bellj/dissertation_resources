package X;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.ComposeShader;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.os.Build;
import java.util.Arrays;

/* renamed from: X.0A5  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0A5 extends Drawable {
    public static final float[] A0L = {0.0f, 0.317f, 0.453f, 0.542f, 0.85f, 1.0f};
    public static final int[] A0M = {-15173646, -14298266, -668109, -37796, -6278145, -15173646};
    public Path A00;
    public final float A01;
    public final float A02;
    public final float A03;
    public final int A04;
    public final Bitmap A05;
    public final Bitmap A06;
    public final Paint A07;
    public final Paint A08;
    public final RectF A09;
    public final RectF A0A;
    public final EnumC03810Jf A0B;
    public final boolean A0C;
    public final boolean A0D;
    public final boolean A0E;
    public final boolean A0F;
    public final boolean A0G;
    public final boolean A0H;
    public final Paint[] A0I;
    public final Paint[] A0J;
    public final Path[] A0K;

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0093, code lost:
        if (r1 != false) goto L_0x0096;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0018, code lost:
        if (android.os.Build.VERSION.SDK_INT >= 29) goto L_0x001a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0064  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00bc  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00d0  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0119  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x011c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass0A5(android.content.Context r11, X.EnumC03810Jf r12, X.C14260l7 r13, int r14, int r15) {
        /*
        // Method dump skipped, instructions count: 288
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0A5.<init>(android.content.Context, X.0Jf, X.0l7, int, int):void");
    }

    public static void A00(int[] iArr) {
        int A00 = AnonymousClass0LP.A00(-14931149, 0.2f);
        for (int i = 0; i < iArr.length; i++) {
            int i2 = iArr[i];
            int alpha = Color.alpha(i2);
            int alpha2 = Color.alpha(A00);
            int i3 = (255 - alpha2) * alpha;
            int i4 = i3 + alpha2;
            iArr[i] = Color.argb(Math.min(255, i4), ((Color.red(i2) * i3) + (Color.red(A00) * alpha2)) / i4, ((Color.green(i2) * i3) + (Color.green(A00) * alpha2)) / i4, ((i3 * Color.blue(i2)) + (Color.blue(A00) * alpha2)) / i4);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0123, code lost:
        if (r19.A0E != false) goto L_0x00d3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0128, code lost:
        if (r19.A0F != false) goto L_0x005c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0228, code lost:
        if (r19.A0E != false) goto L_0x01e9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x022d, code lost:
        if (r19.A0F != false) goto L_0x017e;
     */
    @Override // android.graphics.drawable.Drawable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void draw(android.graphics.Canvas r20) {
        /*
        // Method dump skipped, instructions count: 604
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0A5.draw(android.graphics.Canvas):void");
    }

    @Override // android.graphics.drawable.Drawable
    public boolean getPadding(Rect rect) {
        float f;
        float f2;
        float f3;
        float f4;
        float f5 = 0.0f;
        if (this.A0E) {
            f = this.A02;
        } else {
            f = 0.0f;
        }
        if (this.A0F) {
            f2 = this.A02;
        } else {
            f2 = 0.0f;
        }
        boolean z = this.A0G;
        if (z) {
            float f6 = this.A02;
            if (this.A0C) {
                f4 = 0.0f;
            } else {
                f4 = this.A01;
            }
            f3 = f6 + f4;
        } else {
            f3 = 0.0f;
        }
        if (this.A0D) {
            float f7 = this.A02;
            if (!this.A0C) {
                f5 = this.A01;
            }
            f5 += f7;
            if (z) {
                float f8 = this.A03;
                f3 -= f8;
                f5 += f8;
            }
        }
        rect.set((int) f, (int) f3, (int) f2, (int) f5);
        return true;
    }

    @Override // android.graphics.drawable.Drawable
    public void onBoundsChange(Rect rect) {
        float f;
        float f2;
        int[] iArr;
        float[] fArr;
        float f3;
        LinearGradient linearGradient;
        Shader linearGradient2;
        float f4;
        float f5;
        Bitmap bitmap;
        Paint paint;
        float f6;
        Canvas canvas;
        super.onBoundsChange(rect);
        RectF rectF = this.A09;
        float f7 = this.A02;
        float f8 = ((float) rect.left) + f7;
        boolean z = this.A0G;
        float f9 = (float) rect.top;
        if (z) {
            f9 += f7;
        }
        float f10 = ((float) rect.right) - f7;
        boolean z2 = this.A0D;
        float f11 = (float) rect.bottom;
        if (z2) {
            f11 -= f7;
        }
        rectF.set(f8, f9, f10, f11);
        RectF rectF2 = this.A0A;
        float f12 = ((float) rect.left) + f7;
        float f13 = (float) rect.top;
        if (z) {
            f13 = (f13 + f7) - this.A03;
        }
        float f14 = ((float) rect.right) - f7;
        float f15 = (float) rect.bottom;
        if (z2) {
            f15 = (f15 - f7) - this.A03;
        }
        rectF2.set(f12, f13, f14, f15);
        if (!this.A0E || !z || !this.A0F || !z2) {
            Path path = this.A00;
            if (path == null) {
                path = new Path();
                this.A00 = path;
            }
            path.reset();
            float f16 = 0.0f;
            if (z) {
                f = this.A01;
            } else {
                f = 0.0f;
            }
            if (z2) {
                f16 = this.A01;
            }
            this.A00.addRoundRect(rectF, new float[]{f, f, f, f, f16, f16, f16, f16}, Path.Direction.CW);
            this.A00.close();
        } else {
            this.A00 = null;
        }
        float f17 = this.A01;
        float f18 = -f17;
        RectF rectF3 = new RectF(f18, f18, f17, f17);
        RectF rectF4 = new RectF(rectF3);
        float f19 = -f7;
        rectF4.inset(f19, f19);
        EnumC03810Jf r5 = this.A0B;
        boolean z3 = this.A0H;
        if (z3) {
            f2 = r5.colorAlphaMultiplierDark;
        } else {
            f2 = r5.colorAlphaMultiplierLight;
        }
        int[] iArr2 = A0M;
        int[] copyOf = Arrays.copyOf(iArr2, iArr2.length);
        int[] iArr3 = {-15173646, -15173646, -15173646};
        if (z3 && r5.hasFoaStroke) {
            A00(iArr3);
            A00(copyOf);
        }
        float f20 = 0.3f * f2;
        boolean z4 = true;
        float f21 = 0.1f * f2;
        int[] iArr4 = {AnonymousClass0LP.A00(iArr3[0], f20), AnonymousClass0LP.A00(iArr3[1], f21), AnonymousClass0LP.A00(iArr3[2], 0.0f)};
        if (r5.hasFoaStroke) {
            int i = this.A04;
            iArr = new int[]{AnonymousClass0LP.A00(i, f20), AnonymousClass0LP.A00(i, f21), AnonymousClass0LP.A00(i, 0.0f)};
            float[] fArr2 = new float[3];
            fArr = fArr2;
            // fill-array-data instruction
            fArr2[0] = 0.0f;
            fArr2[1] = 0.5f;
            fArr2[2] = 1.0f;
            f3 = 0.0f;
            linearGradient = new LinearGradient(0.0f, 0.0f, ((float) getBounds().width()) - ((f7 + f17) * 2.0f), 0.0f, copyOf, A0L, Shader.TileMode.REPEAT);
        } else {
            int i2 = this.A04;
            iArr = new int[]{AnonymousClass0LP.A00(i2, f2 * 0.5f), AnonymousClass0LP.A00(i2, f20), AnonymousClass0LP.A00(i2, f21), AnonymousClass0LP.A00(i2, 0.0f)};
            float[] fArr3 = new float[4];
            fArr = fArr3;
            // fill-array-data instruction
            fArr3[0] = 0.0f;
            fArr3[1] = 0.25f;
            fArr3[2] = 0.75f;
            fArr3[3] = 1.0f;
            f3 = 0.0f;
            linearGradient = null;
        }
        float f22 = f17 + f7;
        int[] iArr5 = iArr;
        if (r5.hasFoaStroke) {
            iArr5 = iArr4;
        }
        Shader.TileMode tileMode = Shader.TileMode.CLAMP;
        RadialGradient radialGradient = new RadialGradient(0.0f, 0.0f, f22, iArr5, fArr, tileMode);
        if (!r5.hasFoaStroke || Build.VERSION.SDK_INT >= 29) {
            z4 = false;
        }
        int i3 = 0;
        int i4 = 0;
        while (true) {
            Path[] pathArr = this.A0K;
            if (i3 < pathArr.length) {
                Path path2 = pathArr[i3];
                path2.reset();
                path2.setFillType(Path.FillType.EVEN_ODD);
                this.A0I[i3].setShader(radialGradient);
                if (i4 == 90) {
                    path2.moveTo(f3, f18);
                    path2.rLineTo(f3, f19);
                    linearGradient2 = new LinearGradient(0.0f, 0.0f, f22, 0.0f, iArr4, fArr, tileMode);
                } else if (i4 == 180) {
                    path2.moveTo(f17, f3);
                    path2.rLineTo(f7, f3);
                    f4 = 0.0f;
                    f5 = f22;
                    linearGradient2 = new LinearGradient(0.0f, 0.0f, 0.0f, f5, iArr, fArr, tileMode);
                    if (r5.hasFoaStroke && z4 && (bitmap = this.A05) != null) {
                        bitmap.eraseColor(0);
                        paint = new Paint();
                        paint.setStyle(Paint.Style.FILL);
                        paint.setDither(true);
                        paint.setShader(linearGradient2);
                        f6 = 1.0f;
                        canvas = new Canvas(bitmap);
                        canvas.drawRect(0.0f, f4, f6, f5, paint);
                        Shader.TileMode tileMode2 = Shader.TileMode.REPEAT;
                        linearGradient2 = new BitmapShader(bitmap, tileMode2, tileMode2);
                    }
                } else if (i4 != 270) {
                    path2.moveTo(f18, f3);
                    path2.rLineTo(f19, f3);
                    linearGradient2 = new LinearGradient(0.0f, 0.0f, 0.0f, f19 - f17, iArr, fArr, tileMode);
                    if (r5.hasFoaStroke && z4 && (bitmap = this.A06) != null) {
                        bitmap.eraseColor(0);
                        paint = new Paint();
                        paint.setStyle(Paint.Style.FILL);
                        paint.setDither(true);
                        paint.setShader(linearGradient2);
                        Canvas canvas2 = new Canvas(bitmap);
                        canvas2.translate(0.0f, f22);
                        f6 = 1.0f;
                        f5 = 0.0f;
                        canvas = canvas2;
                        f4 = -f22;
                        canvas.drawRect(0.0f, f4, f6, f5, paint);
                        Shader.TileMode tileMode2 = Shader.TileMode.REPEAT;
                        linearGradient2 = new BitmapShader(bitmap, tileMode2, tileMode2);
                    }
                } else {
                    path2.moveTo(f3, f17);
                    path2.rLineTo(f3, f7);
                    linearGradient2 = new LinearGradient(0.0f, 0.0f, f19 - f17, 0.0f, iArr4, fArr, tileMode);
                }
                float f23 = (float) i4;
                path2.arcTo(rectF4, 180.0f + f23, 90.0f, false);
                path2.arcTo(rectF3, f23 + 270.0f, -90.0f, false);
                path2.close();
                if (r5.hasFoaStroke && linearGradient != null && (i4 == 180 || i4 == 0)) {
                    linearGradient2 = new ComposeShader(linearGradient2, linearGradient, PorterDuff.Mode.SRC_IN);
                }
                this.A0J[i3].setShader(linearGradient2);
                i4 += 90;
                i3++;
                f3 = 0.0f;
            } else {
                return;
            }
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        for (Paint paint : this.A0I) {
            paint.setAlpha(i);
        }
        for (Paint paint2 : this.A0J) {
            paint2.setAlpha(i);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
        for (Paint paint : this.A0I) {
            paint.setColorFilter(colorFilter);
        }
        for (Paint paint2 : this.A0J) {
            paint2.setColorFilter(colorFilter);
        }
    }
}
