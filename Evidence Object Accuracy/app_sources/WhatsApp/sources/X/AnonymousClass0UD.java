package X;

import android.graphics.Color;
import android.graphics.PointF;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0UD  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0UD {
    public static final C05850Rf A00 = C05850Rf.A00("x", "y");

    public static float A00(AbstractC08850bx r4) {
        EnumC03770Jb A05 = r4.A05();
        switch (A05.ordinal()) {
            case 0:
                r4.A09();
                float A02 = (float) r4.A02();
                while (r4.A0H()) {
                    r4.A0E();
                }
                r4.A0B();
                return A02;
            case 6:
                return (float) r4.A02();
            default:
                StringBuilder sb = new StringBuilder("Unknown value for token of type ");
                sb.append(A05);
                throw new IllegalArgumentException(sb.toString());
        }
    }

    public static int A01(AbstractC08850bx r8) {
        r8.A09();
        int A02 = (int) (r8.A02() * 255.0d);
        int A022 = (int) (r8.A02() * 255.0d);
        int A023 = (int) (r8.A02() * 255.0d);
        while (r8.A0H()) {
            r8.A0E();
        }
        r8.A0B();
        return Color.argb(255, A02, A022, A023);
    }

    public static PointF A02(AbstractC08850bx r5, float f) {
        float f2;
        float f3;
        switch (r5.A05().ordinal()) {
            case 0:
                r5.A09();
                f2 = (float) r5.A02();
                f3 = (float) r5.A02();
                while (r5.A05() != EnumC03770Jb.END_ARRAY) {
                    r5.A0E();
                }
                r5.A0B();
                break;
            case 1:
            case 3:
            case 4:
            case 5:
            default:
                StringBuilder sb = new StringBuilder("Unknown point starts with ");
                sb.append(r5.A05());
                throw new IllegalArgumentException(sb.toString());
            case 2:
                r5.A0A();
                f2 = 0.0f;
                f3 = 0.0f;
                while (r5.A0H()) {
                    int A04 = r5.A04(A00);
                    if (A04 == 0) {
                        f2 = A00(r5);
                    } else if (A04 != 1) {
                        r5.A0D();
                        r5.A0E();
                    } else {
                        f3 = A00(r5);
                    }
                }
                r5.A0C();
                break;
            case 6:
                float A02 = (float) r5.A02();
                float A022 = (float) r5.A02();
                while (r5.A0H()) {
                    r5.A0E();
                }
                return new PointF(A02 * f, A022 * f);
        }
        return new PointF(f2 * f, f3 * f);
    }

    public static List A03(AbstractC08850bx r3, float f) {
        ArrayList arrayList = new ArrayList();
        r3.A09();
        while (r3.A05() == EnumC03770Jb.BEGIN_ARRAY) {
            r3.A09();
            arrayList.add(A02(r3, f));
            r3.A0B();
        }
        r3.A0B();
        return arrayList;
    }
}
