package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4kc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C99634kc implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new C48152En(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C48152En[i];
    }
}
