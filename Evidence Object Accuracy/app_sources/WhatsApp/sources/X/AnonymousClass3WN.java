package X;

import android.app.Activity;
import android.view.View;
import java.lang.ref.WeakReference;

/* renamed from: X.3WN  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3WN implements AbstractC116055Ty {
    public final View A00;
    public final C14900mE A01;
    public final C15570nT A02;
    public final C15550nR A03;
    public final C15610nY A04;
    public final AnonymousClass1J1 A05;
    public final AnonymousClass19D A06;
    public final AnonymousClass11P A07;
    public final C14820m6 A08;
    public final AnonymousClass018 A09;
    public final C14850m9 A0A;
    public final AbstractC14440lR A0B;
    public final AnonymousClass01H A0C;
    public final WeakReference A0D;

    public AnonymousClass3WN(Activity activity, View view, C14900mE r4, C15570nT r5, C15550nR r6, C15610nY r7, AnonymousClass1J1 r8, AnonymousClass19D r9, AnonymousClass11P r10, C14820m6 r11, AnonymousClass018 r12, C14850m9 r13, AbstractC14440lR r14, AnonymousClass01H r15) {
        this.A0A = r13;
        this.A00 = view;
        this.A06 = r9;
        this.A07 = r10;
        this.A0C = r15;
        this.A01 = r4;
        this.A0D = C12970iu.A10(activity);
        this.A09 = r12;
        this.A05 = r8;
        this.A02 = r5;
        this.A03 = r6;
        this.A04 = r7;
        this.A0B = r14;
        this.A08 = r11;
    }

    @Override // X.AbstractC116055Ty
    public void ATn(int i) {
        AnonymousClass11P r0 = this.A07;
        C30421Xi A01 = r0.A01();
        C35191hP A00 = r0.A00();
        if (A00 != null && A01 != null) {
            this.A0B.Ab2(new RunnableC55602ix(this, A01, 0, i, A00.A0T));
        }
    }
}
