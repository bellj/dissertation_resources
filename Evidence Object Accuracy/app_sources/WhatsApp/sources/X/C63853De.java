package X;

import android.graphics.Matrix;
import com.whatsapp.mediacomposer.ImageComposerFragment;
import com.whatsapp.mediacomposer.doodle.ImagePreviewContentLayout;

/* renamed from: X.3De  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63853De {
    public final /* synthetic */ ImagePreviewContentLayout A00;

    public C63853De(ImagePreviewContentLayout imagePreviewContentLayout) {
        this.A00 = imagePreviewContentLayout;
    }

    public void A00(Matrix matrix) {
        ImagePreviewContentLayout imagePreviewContentLayout = this.A00;
        AnonymousClass2Ab r1 = imagePreviewContentLayout.A03;
        r1.A0I.A0A.set(matrix);
        r1.A0H.invalidate();
        imagePreviewContentLayout.invalidate();
    }

    public void A01(boolean z) {
        AbstractC115315Ra r0 = this.A00.A04;
        if (r0 != null) {
            ImageComposerFragment imageComposerFragment = ((C1109757p) r0).A00;
            if (z) {
                imageComposerFragment.A1K(true, false);
            } else {
                imageComposerFragment.A1K(false, true);
            }
        }
    }
}
