package X;

import android.graphics.Camera;
import android.graphics.Matrix;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/* renamed from: X.2UN  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2UN extends Animation {
    public Camera A00;
    public final float A01;
    public final float A02;
    public final float A03;
    public final float A04;

    public AnonymousClass2UN(float f, float f2, float f3, float f4) {
        this.A04 = f;
        this.A01 = f2;
        this.A02 = f3;
        this.A03 = f4;
    }

    @Override // android.view.animation.Animation
    public void applyTransformation(float f, Transformation transformation) {
        float f2 = 0.0f + ((this.A04 - 0.0f) * f);
        Matrix matrix = transformation.getMatrix();
        this.A00.save();
        this.A00.translate(0.0f, 0.0f, (float) (((double) this.A03) * Math.sin(((double) f) * 3.141592653589793d)));
        this.A00.rotateY(f2);
        this.A00.getMatrix(matrix);
        this.A00.restore();
        float f3 = this.A01;
        float f4 = this.A02;
        matrix.preTranslate(-f3, -f4);
        matrix.postTranslate(f3, f4);
    }

    @Override // android.view.animation.Animation
    public void initialize(int i, int i2, int i3, int i4) {
        super.initialize(i, i2, i3, i4);
        this.A00 = new Camera();
    }
}
