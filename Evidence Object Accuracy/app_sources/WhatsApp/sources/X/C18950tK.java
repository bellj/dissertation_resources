package X;

import com.whatsapp.jobqueue.job.SendMediaErrorReceiptJob;
import com.whatsapp.util.Log;

/* renamed from: X.0tK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18950tK {
    public final C15570nT A00;
    public final C20670w8 A01;
    public final C240313y A02;
    public final AnonymousClass2QY A03 = new AnonymousClass2QY();
    public final C230910i A04;
    public final C14830m7 A05;
    public final C16590pI A06;
    public final C21420xO A07;
    public final C22170ye A08;
    public final C21710xr A09;

    public C18950tK(C15570nT r2, C20670w8 r3, C240313y r4, C230910i r5, C14830m7 r6, C16590pI r7, C21420xO r8, C22170ye r9, C21710xr r10) {
        this.A05 = r6;
        this.A00 = r2;
        this.A06 = r7;
        this.A01 = r3;
        this.A08 = r9;
        this.A09 = r10;
        this.A04 = r5;
        this.A02 = r4;
        this.A07 = r8;
    }

    public void A00(AnonymousClass1PW r6, boolean z) {
        if (z) {
            String str = r6.A08;
            C15570nT r0 = this.A00;
            r0.A08();
            C34771gg r4 = new C34771gg(new AnonymousClass1IS(r0.A05, str, true), 1);
            byte[] bArr = r6.A09;
            r4.A0H = bArr;
            if (bArr == null) {
                Log.e("ReceiveHistorySyncManager/ missing media key");
            } else {
                this.A01.A00(new SendMediaErrorReceiptJob(r4, "peer", bArr));
            }
        }
        C240313y r3 = this.A02;
        int i = r6.A01;
        C50562Qa r2 = new C50562Qa();
        r2.A02 = 2;
        r2.A0B = r3.A00();
        r2.A00 = C233411h.A01(i);
        r2.A01 = C233411h.A00(i);
        r2.A0A = Long.valueOf(r3.A03.A00());
        r3.A06.A07(r2);
        this.A07.A00(r6.A08);
    }
}
