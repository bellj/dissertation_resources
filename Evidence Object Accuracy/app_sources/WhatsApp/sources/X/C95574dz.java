package X;

import java.lang.reflect.Method;

/* renamed from: X.4dz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C95574dz {
    public static final C95574dz A04 = new C95574dz("VZCBSIFJD", 1, 1, 2);
    public static final C95574dz A05 = new C95574dz("VZCBSIFJD", 3, 3, 4);
    public static final C95574dz A06 = new C95574dz("VZCBSIFJD", 2, 2, 3);
    public static final C95574dz A07 = new C95574dz("VZCBSIFJD", 8, 8, 9);
    public static final C95574dz A08 = new C95574dz("VZCBSIFJD", 6, 6, 7);
    public static final C95574dz A09 = new C95574dz("VZCBSIFJD", 5, 5, 6);
    public static final C95574dz A0A = new C95574dz("VZCBSIFJD", 7, 7, 8);
    public static final C95574dz A0B = new C95574dz("VZCBSIFJD", 4, 4, 5);
    public static final C95574dz A0C = new C95574dz("VZCBSIFJD", 0, 0, 1);
    public final int A00;
    public final int A01;
    public final int A02;
    public final String A03;

    public C95574dz(String str, int i, int i2, int i3) {
        this.A00 = i;
        this.A03 = str;
        this.A01 = i2;
        this.A02 = i3;
    }

    public static int A00(String str) {
        int i = 1;
        char charAt = str.charAt(1);
        int i2 = 1;
        int i3 = 1;
        while (charAt != ')') {
            if (charAt == 'J' || charAt == 'D') {
                i2++;
                i3 += 2;
            } else {
                while (str.charAt(i2) == '[') {
                    i2++;
                }
                int i4 = i2 + 1;
                if (str.charAt(i2) == 'L') {
                    i4 = Math.max(i4, str.indexOf(59, i4) + 1);
                }
                i3++;
                i2 = i4;
            }
            charAt = str.charAt(i2);
        }
        char charAt2 = str.charAt(i2 + 1);
        if (charAt2 == 'V') {
            return i3 << 2;
        }
        if (charAt2 == 'J' || charAt2 == 'D') {
            i = 2;
        }
        return (i3 << 2) | i;
    }

    public static String A01(Method method) {
        StringBuilder A0k = C12960it.A0k("(");
        for (Class<?> cls : method.getParameterTypes()) {
            A04(cls, A0k);
        }
        A0k.append(')');
        A04(method.getReturnType(), A0k);
        return A0k.toString();
    }

    public static C95574dz A02(Class cls) {
        if (!cls.isPrimitive()) {
            StringBuilder A0h = C12960it.A0h();
            A04(cls, A0h);
            String obj = A0h.toString();
            return A03(obj, 0, obj.length());
        } else if (cls == Integer.TYPE) {
            return A09;
        } else {
            if (cls == Void.TYPE) {
                return A0C;
            }
            if (cls == Boolean.TYPE) {
                return A04;
            }
            if (cls == Byte.TYPE) {
                return A05;
            }
            if (cls == Character.TYPE) {
                return A06;
            }
            if (cls == Short.TYPE) {
                return A0B;
            }
            if (cls == Double.TYPE) {
                return A07;
            }
            if (cls == Float.TYPE) {
                return A08;
            }
            if (cls == Long.TYPE) {
                return A0A;
            }
            throw new AssertionError();
        }
    }

    public static C95574dz A03(String str, int i, int i2) {
        char charAt = str.charAt(i);
        int i3 = 11;
        if (charAt != '(') {
            if (charAt == 'F') {
                return A08;
            }
            if (charAt == 'L') {
                i3 = 10;
                i++;
                i2--;
            } else if (charAt == 'S') {
                return A0B;
            } else {
                if (charAt == 'V') {
                    return A0C;
                }
                if (charAt == 'I') {
                    return A09;
                }
                if (charAt == 'J') {
                    return A0A;
                }
                if (charAt == 'Z') {
                    return A04;
                }
                if (charAt != '[') {
                    switch (charAt) {
                        case 'B':
                            return A05;
                        case 'C':
                            return A06;
                        case 'D':
                            return A07;
                        default:
                            throw C72453ed.A0h();
                    }
                } else {
                    i3 = 9;
                }
            }
        }
        return new C95574dz(str, i3, i, i2);
    }

    public static void A04(Class cls, StringBuilder sb) {
        char c;
        while (cls.isArray()) {
            sb.append('[');
            cls = cls.getComponentType();
        }
        if (!cls.isPrimitive()) {
            sb.append('L');
            sb.append(cls.getName().replace('.', '/'));
            c = ';';
        } else if (cls == Integer.TYPE) {
            c = 'I';
        } else if (cls == Void.TYPE) {
            c = 'V';
        } else if (cls == Boolean.TYPE) {
            c = 'Z';
        } else if (cls == Byte.TYPE) {
            c = 'B';
        } else if (cls == Character.TYPE) {
            c = 'C';
        } else if (cls == Short.TYPE) {
            c = 'S';
        } else if (cls == Double.TYPE) {
            c = 'D';
        } else if (cls == Float.TYPE) {
            c = 'F';
        } else if (cls == Long.TYPE) {
            c = 'J';
        } else {
            throw new AssertionError();
        }
        sb.append(c);
    }

    public static C95574dz[] A05(String str) {
        int i = 0;
        int i2 = 1;
        int i3 = 0;
        while (str.charAt(i2) != ')') {
            while (str.charAt(i2) == '[') {
                i2++;
            }
            int i4 = i2 + 1;
            i2 = str.charAt(i2) == 'L' ? Math.max(i4, str.indexOf(59, i4) + 1) : i4;
            i3++;
        }
        C95574dz[] r4 = new C95574dz[i3];
        int i5 = 1;
        while (str.charAt(i5) != ')') {
            int i6 = i5;
            while (str.charAt(i6) == '[') {
                i6++;
            }
            int i7 = i6 + 1;
            if (str.charAt(i6) == 'L') {
                i7 = Math.max(i7, str.indexOf(59, i7) + 1);
            }
            r4[i] = A03(str, i5, i7);
            i++;
            i5 = i7;
        }
        return r4;
    }

    public String A06() {
        String str;
        int i;
        int i2;
        int i3 = this.A00;
        if (i3 == 10) {
            str = this.A03;
            i = this.A01 - 1;
            i2 = this.A02 + 1;
        } else if (i3 == 12) {
            StringBuilder A0k = C12960it.A0k("L");
            C72453ed.A1O(this.A03, A0k, this.A01, this.A02);
            return C72463ee.A0H(A0k, ';');
        } else {
            str = this.A03;
            i = this.A01;
            i2 = this.A02;
        }
        return str.substring(i, i2);
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C95574dz) {
                C95574dz r8 = (C95574dz) obj;
                int i = this.A00;
                int i2 = 10;
                if (i == 12) {
                    i = 10;
                }
                int i3 = r8.A00;
                if (i3 != 12) {
                    i2 = i3;
                }
                if (i == i2) {
                    int i4 = this.A01;
                    int i5 = this.A02;
                    int i6 = r8.A01;
                    if (i5 - i4 == r8.A02 - i6) {
                        while (i4 < i5) {
                            if (this.A03.charAt(i4) == r8.A03.charAt(i6)) {
                                i4++;
                                i6++;
                            }
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i = this.A00;
        int i2 = i;
        if (i == 12) {
            i2 = 10;
        }
        int i3 = i2 * 13;
        if (i >= 9) {
            int i4 = this.A02;
            for (int i5 = this.A01; i5 < i4; i5++) {
                i3 = (i3 + this.A03.charAt(i5)) * 17;
            }
        }
        return i3;
    }

    public String toString() {
        return A06();
    }
}
