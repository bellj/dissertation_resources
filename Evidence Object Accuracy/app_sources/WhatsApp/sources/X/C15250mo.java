package X;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* renamed from: X.0mo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15250mo {
    public int A00;
    public int A01;
    public int A02;
    public AnonymousClass01T A03;
    public AbstractC14640lm A04;
    public C30941Zk A05;
    public C49662Lr A06;
    public Boolean A07 = null;
    public Boolean A08 = null;
    public CharSequence A09;
    public List A0A;
    public List A0B = null;
    public List A0C;
    public List A0D;
    public Map A0E;
    public boolean A0F = true;
    public boolean A0G = true;
    public final AnonymousClass018 A0H;
    public final Object A0I = new Object();

    public C15250mo(AnonymousClass018 r2) {
        this.A0H = r2;
    }

    public final AnonymousClass01T A00() {
        AnonymousClass01T A00;
        if (TextUtils.isEmpty(this.A09)) {
            A00 = new AnonymousClass01T(new ArrayList(), new ArrayList());
        } else {
            A00 = C32751cg.A00(this.A0H, A01(), false);
        }
        this.A03 = A00;
        return A00;
    }

    public String A01() {
        if (this.A09 == null) {
            AnonymousClass01T r0 = this.A03;
            if (r0 == null) {
                return "";
            }
            Object obj = r0.A01;
            AnonymousClass009.A05(obj);
            if (((List) obj).isEmpty()) {
                return "";
            }
        }
        CharSequence charSequence = this.A09;
        if (charSequence == null) {
            return TextUtils.join(" ", A02());
        }
        return charSequence.toString();
    }

    public List A02() {
        List list;
        AnonymousClass01T r0 = this.A03;
        if (r0 != null && (list = (List) r0.A01) != null && !list.isEmpty()) {
            return list;
        }
        Object obj = A00().A01;
        AnonymousClass009.A05(obj);
        return (List) obj;
    }

    public void A03(CharSequence charSequence) {
        this.A09 = charSequence;
        this.A03 = null;
    }

    public void A04(List list) {
        if (list == null) {
            list = new ArrayList();
        }
        if (this.A03 == null) {
            this.A03 = new AnonymousClass01T(new ArrayList(), list);
        }
        this.A09 = null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0092, code lost:
        if (r0 != false) goto L_0x0094;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x004d, code lost:
        if (r1 == false) goto L_0x004f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String toString() {
        /*
            r3 = this;
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r0 = super.toString()
            r2.append(r0)
            java.lang.String r0 = "\n\tnormal: \""
            r2.append(r0)
            java.lang.String r0 = r3.A01()
            r2.append(r0)
            java.lang.String r0 = "\";\ttokens: "
            r2.append(r0)
            java.util.List r0 = r3.A02()
            r2.append(r0)
            java.lang.String r0 = "\n\tlabel: \""
            r2.append(r0)
            java.util.List r0 = r3.A0D
            if (r0 != 0) goto L_0x0032
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
        L_0x0032:
            r2.append(r0)
            java.lang.String r0 = "\";\tjid: "
            r2.append(r0)
            X.0lm r0 = r3.A04
            r2.append(r0)
            java.lang.String r0 = "\";\tstarred: "
            r2.append(r0)
            java.lang.Boolean r0 = r3.A08
            if (r0 == 0) goto L_0x004f
            boolean r1 = r0.booleanValue()
            r0 = 1
            if (r1 != 0) goto L_0x0050
        L_0x004f:
            r0 = 0
        L_0x0050:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r2.append(r0)
            java.lang.String r0 = "\n\tpage: "
            r2.append(r0)
            int r0 = r3.A00
            r2.append(r0)
            java.lang.String r0 = "; pageSize: "
            r2.append(r0)
            int r0 = r3.A01
            r2.append(r0)
            java.lang.String r0 = "\n\tcontact filters:"
            r2.append(r0)
            java.util.List r0 = r3.A0A
            r2.append(r0)
            java.lang.String r0 = "\n\tcontact prefilters:"
            r2.append(r0)
            java.util.List r0 = r3.A0B
            r2.append(r0)
            java.lang.String r0 = "\n\tmapping: "
            r2.append(r0)
            X.01T r0 = r3.A03
            if (r0 == 0) goto L_0x0094
            java.lang.Object r1 = r0.A00
            java.util.List r1 = (java.util.List) r1
            if (r1 == 0) goto L_0x0094
            boolean r0 = r1.isEmpty()
            if (r0 == 0) goto L_0x009d
        L_0x0094:
            X.01T r0 = r3.A00()
            java.lang.Object r1 = r0.A00
            X.AnonymousClass009.A05(r1)
        L_0x009d:
            r2.append(r1)
            java.lang.String r0 = r2.toString()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15250mo.toString():java.lang.String");
    }
}
