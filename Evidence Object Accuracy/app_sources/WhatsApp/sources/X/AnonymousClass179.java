package X;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/* renamed from: X.179  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass179 {
    public C91334Ri A00(String str, X509Certificate x509Certificate) {
        KeyGenerator instance = KeyGenerator.getInstance("AES");
        instance.init(256);
        SecretKey generateKey = instance.generateKey();
        byte[] bArr = new byte[12];
        C002901h.A00().nextBytes(bArr);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(bArr);
        byte[] bytes = str.getBytes();
        Cipher instance2 = Cipher.getInstance("AES/GCM/NoPadding");
        instance2.init(1, generateKey, ivParameterSpec);
        byte[] iv = instance2.getIV();
        byte[] doFinal = instance2.doFinal(bytes);
        int length = doFinal.length;
        int i = length - 16;
        C91344Rj r5 = new C91344Rj(generateKey, Arrays.copyOfRange(doFinal, 0, i), Arrays.copyOfRange(doFinal, i, length), iv);
        PublicKey publicKey = x509Certificate.getPublicKey();
        SecretKey secretKey = r5.A00;
        Cipher instance3 = Cipher.getInstance("RSA/ECB/OAEPPadding");
        instance3.init(1, publicKey);
        return new C91334Ri(instance3.doFinal(secretKey.getEncoded()), r5.A01, r5.A03, r5.A02);
    }

    public String A01(C91334Ri r7, PrivateKey privateKey) {
        byte[] bArr = r7.A01;
        Cipher instance = Cipher.getInstance("RSA/ECB/OAEPPadding");
        instance.init(2, privateKey);
        byte[] doFinal = instance.doFinal(bArr);
        byte[] bArr2 = r7.A00;
        byte[] bArr3 = r7.A03;
        int length = bArr2.length;
        int length2 = bArr3.length;
        byte[] copyOf = Arrays.copyOf(bArr2, length + length2);
        System.arraycopy(bArr3, 0, copyOf, length, length2);
        SecretKeySpec secretKeySpec = new SecretKeySpec(doFinal, "AES");
        Cipher instance2 = Cipher.getInstance("AES/GCM/NoPadding");
        instance2.init(2, secretKeySpec, new IvParameterSpec(r7.A02));
        return new String(instance2.doFinal(copyOf), AnonymousClass01V.A08);
    }
}
