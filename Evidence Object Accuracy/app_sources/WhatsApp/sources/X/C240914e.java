package X;

import android.content.ContentValues;
import android.database.sqlite.SQLiteConstraintException;
import com.whatsapp.util.Log;

/* renamed from: X.14e  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C240914e {
    public final C16490p7 A00;

    public C240914e(C16490p7 r1) {
        this.A00 = r1;
    }

    public void A00(C38111nX r6, long j) {
        try {
            C16310on A02 = this.A00.A02();
            ContentValues contentValues = new ContentValues();
            contentValues.put("message_row_id", Long.valueOf(j));
            contentValues.put("waveform", r6.A01);
            A02.A03.A06(contentValues, "audio_data", 5);
            A02.close();
        } catch (SQLiteConstraintException e) {
            Log.e("WaveformMessageStore/insertWaveform/", e);
            throw e;
        }
    }
}
