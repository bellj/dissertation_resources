package X;

import java.io.File;

/* renamed from: X.17X  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass17X {
    public final C14900mE A00;
    public final C18790t3 A01;
    public final C16590pI A02;
    public final C18810t5 A03;
    public final C38721ob A04;

    public AnonymousClass17X(C14900mE r9, C18790t3 r10, C16590pI r11, C18810t5 r12) {
        this.A00 = r9;
        this.A02 = r11;
        this.A01 = r10;
        this.A03 = r12;
        C38771og r2 = new C38771og(r9, r10, r12, new File(r11.A00.getCacheDir(), A00()), "payments-image");
        r2.A01 = 16777216;
        r2.A05 = true;
        this.A04 = r2.A00();
    }

    public String A00() {
        return "payment_merchant_image_cache";
    }
}
