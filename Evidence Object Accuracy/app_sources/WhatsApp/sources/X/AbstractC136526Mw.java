package X;

import android.os.Handler;
import android.view.Surface;

/* renamed from: X.6Mw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public interface AbstractC136526Mw {
    int ADQ();

    void AIY(int i, int i2, int i3);

    void AZT(Handler handler, C128475wA v);

    Surface getSurface();

    void release();
}
