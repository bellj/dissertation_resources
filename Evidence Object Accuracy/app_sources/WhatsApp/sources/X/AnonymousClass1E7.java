package X;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/* renamed from: X.1E7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1E7 {
    public final C14830m7 A00;
    public final Map A01 = new HashMap();
    public final Set A02 = new HashSet();
    public final Set A03 = new HashSet();

    public AnonymousClass1E7(C14830m7 r2) {
        this.A00 = r2;
    }

    public void A00(String[] strArr) {
        Set set = this.A03;
        synchronized (set) {
            set.removeAll(C15380n4.A08(strArr));
        }
    }

    public boolean A01(AnonymousClass1IS r3) {
        boolean add;
        Set set = this.A02;
        synchronized (set) {
            add = set.add(r3);
        }
        return add;
    }
}
