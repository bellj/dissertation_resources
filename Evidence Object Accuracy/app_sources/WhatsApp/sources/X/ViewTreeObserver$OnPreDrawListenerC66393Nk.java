package X;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;

/* renamed from: X.3Nk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ViewTreeObserver$OnPreDrawListenerC66393Nk implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ C64633Gh A00;

    public ViewTreeObserver$OnPreDrawListenerC66393Nk(C64633Gh r1) {
        this.A00 = r1;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        C64633Gh r4 = this.A00;
        AnonymousClass2AS r3 = r4.A04;
        View view = r3.A05;
        C12980iv.A1G(view, this);
        r4.A00 = view.getMeasuredHeight();
        int A00 = C64633Gh.A00((View) r3.A07.getParent());
        r4.A01 = A00;
        ViewGroup viewGroup = r3.A06;
        int A002 = A00 + C64633Gh.A00(viewGroup.getChildAt(0));
        r4.A01 = A002;
        int i = 0;
        if (viewGroup.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            i = 0 + C12970iu.A0H(viewGroup).topMargin + C12970iu.A0H(viewGroup).bottomMargin;
        }
        if (viewGroup.getParent() instanceof View) {
            ViewParent parent = viewGroup.getParent();
            do {
                View view2 = (View) parent;
                if (view2 == view) {
                    break;
                }
                i = i + view2.getPaddingTop() + view2.getPaddingTop();
                if (view2.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
                    i = i + C12970iu.A0H(view2).topMargin + C12970iu.A0H(view2).bottomMargin;
                }
                parent = view2.getParent();
            } while (parent instanceof View);
            i = i + view.getPaddingTop() + view.getPaddingTop();
        }
        r4.A01 = A002 + i;
        r3.requestLayout();
        return false;
    }
}
