package X;

/* renamed from: X.0FK  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass0FK extends AnonymousClass0Z4 {
    public final AnonymousClass02M A00;

    public AnonymousClass0FK(AnonymousClass02M r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass0Z4, X.AbstractC12640iF
    public void ANr(Object obj, int i, int i2) {
        this.A00.A01.A04(obj, i, i2);
    }

    @Override // X.AbstractC12640iF
    public void ARP(int i, int i2) {
        this.A00.A01.A02(i, i2);
    }

    @Override // X.AbstractC12640iF
    public void ASr(int i, int i2) {
        this.A00.A01.A01(i, i2);
    }

    @Override // X.AbstractC12640iF
    public void AUs(int i, int i2) {
        this.A00.A01.A03(i, i2);
    }
}
