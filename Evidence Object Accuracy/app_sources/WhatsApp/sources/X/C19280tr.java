package X;

import android.content.ContentValues;
import android.database.Cursor;

/* renamed from: X.0tr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19280tr extends AbstractC18500sY {
    public final C14850m9 A00;

    public C19280tr(C18480sW r3, C14850m9 r4) {
        super(r3, "quoted_order_message_v2", 1);
        this.A00 = r4;
    }

    @Override // X.AbstractC18500sY
    public AnonymousClass2Ez A09(Cursor cursor) {
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow("message_row_id");
        int columnIndexOrThrow2 = cursor.getColumnIndexOrThrow("order_id");
        int columnIndexOrThrow3 = cursor.getColumnIndexOrThrow("thumbnail");
        int columnIndexOrThrow4 = cursor.getColumnIndexOrThrow("order_title");
        int columnIndexOrThrow5 = cursor.getColumnIndexOrThrow("item_count");
        int columnIndexOrThrow6 = cursor.getColumnIndexOrThrow("status");
        int columnIndexOrThrow7 = cursor.getColumnIndexOrThrow("surface");
        int columnIndexOrThrow8 = cursor.getColumnIndexOrThrow("message");
        int columnIndexOrThrow9 = cursor.getColumnIndexOrThrow("seller_jid");
        int columnIndexOrThrow10 = cursor.getColumnIndexOrThrow("token");
        int columnIndexOrThrow11 = cursor.getColumnIndexOrThrow("currency_code");
        int columnIndexOrThrow12 = cursor.getColumnIndexOrThrow("total_amount_1000");
        C16490p7 r0 = this.A05;
        C16310on A02 = r0.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            long j = -1;
            int i = 0;
            while (cursor.moveToNext()) {
                j = cursor.getLong(columnIndexOrThrow);
                C16310on A01 = r0.get();
                try {
                    C16330op r15 = A01.A03;
                    String l = Long.toString(j);
                    Cursor A09 = r15.A09("SELECT _id, quoted_row_id FROM messages WHERE _id = ?", new String[]{l});
                    if (A09.moveToNext()) {
                        long j2 = A09.getLong(A09.getColumnIndexOrThrow("quoted_row_id"));
                        A09.close();
                        A01.close();
                        if (j2 != 0) {
                            A02 = r0.get();
                            try {
                                boolean z = true;
                                Cursor A092 = A02.A03.A09("SELECT media_wa_type FROM messages_quotes WHERE _id = ?", new String[]{Long.toString(j2)});
                                if (A092.moveToNext()) {
                                    if (A092.getInt(A092.getColumnIndexOrThrow("media_wa_type")) != 44) {
                                        z = false;
                                    }
                                    A092.close();
                                    A02.close();
                                    if (z) {
                                        ContentValues contentValues = new ContentValues(12);
                                        contentValues.put("message_row_id", Long.valueOf(j2));
                                        contentValues.put("order_id", cursor.getString(columnIndexOrThrow2));
                                        contentValues.put("thumbnail", cursor.getBlob(columnIndexOrThrow3));
                                        contentValues.put("order_title", cursor.getString(columnIndexOrThrow4));
                                        contentValues.put("item_count", Long.valueOf(cursor.getLong(columnIndexOrThrow5)));
                                        contentValues.put("status", Long.valueOf(cursor.getLong(columnIndexOrThrow6)));
                                        contentValues.put("surface", Long.valueOf(cursor.getLong(columnIndexOrThrow7)));
                                        contentValues.put("message", cursor.getString(columnIndexOrThrow8));
                                        contentValues.put("seller_jid", Long.valueOf(cursor.getLong(columnIndexOrThrow9)));
                                        contentValues.put("token", cursor.getString(columnIndexOrThrow10));
                                        contentValues.put("currency_code", cursor.getString(columnIndexOrThrow11));
                                        contentValues.put("total_amount_1000", Long.valueOf(cursor.getLong(columnIndexOrThrow12)));
                                        A02.A03.A02(contentValues, "quoted_message_order");
                                        i++;
                                    }
                                } else {
                                    A092.close();
                                    A02.close();
                                }
                            } finally {
                            }
                        }
                    } else {
                        A09.close();
                        A01.close();
                    }
                    A02.A03.A01("message_quoted_order", "message_row_id = ?", new String[]{l});
                } finally {
                }
            }
            A00.A00();
            A00.close();
            A02.close();
            return new AnonymousClass2Ez(j, i);
        } finally {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
        }
    }
}
