package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1Pw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C29001Pw {
    public List A00;

    public C29001Pw(AbstractC28981Pu... r5) {
        int length = r5.length;
        this.A00 = new ArrayList(length);
        for (AbstractC28981Pu r1 : r5) {
            if (r1 != null) {
                this.A00.add(r1);
            }
        }
    }

    public boolean A00() {
        for (AbstractC28981Pu r0 : this.A00) {
            if (!r0.isValid()) {
                return false;
            }
        }
        return true;
    }
}
