package X;

/* renamed from: X.0jn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C13480jn extends Exception {
    public C13480jn() {
        super("Invalid properties file");
    }

    public C13480jn(Exception exc) {
        super(exc);
    }
}
