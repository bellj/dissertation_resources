package X;

import java.io.IOException;
import java.security.cert.CertSelector;
import java.security.cert.CertStore;
import java.security.cert.PKIXParameters;
import java.security.cert.X509CertSelector;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* renamed from: X.5Hm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C113415Hm extends PKIXParameters {
    public int A00 = 0;
    public List A01 = C12960it.A0l();
    public List A02 = C12960it.A0l();
    public Set A03 = C12970iu.A12();
    public Set A04 = C12970iu.A12();
    public Set A05 = C12970iu.A12();
    public Set A06 = C12970iu.A12();
    public AbstractC117205Yy A07;
    public boolean A08;
    public boolean A09 = false;

    @Override // java.security.cert.PKIXParameters
    public void setCertStores(List list) {
        if (list != null) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                addCertStore((CertStore) it.next());
            }
        }
    }

    public C113415Hm(Set set) {
        super(set);
    }

    public void A00(PKIXParameters pKIXParameters) {
        AbstractC117205Yy r0;
        setDate(pKIXParameters.getDate());
        setCertPathCheckers(pKIXParameters.getCertPathCheckers());
        setCertStores(pKIXParameters.getCertStores());
        setAnyPolicyInhibited(pKIXParameters.isAnyPolicyInhibited());
        setExplicitPolicyRequired(pKIXParameters.isExplicitPolicyRequired());
        setPolicyMappingInhibited(pKIXParameters.isPolicyMappingInhibited());
        setRevocationEnabled(pKIXParameters.isRevocationEnabled());
        setInitialPolicies(pKIXParameters.getInitialPolicies());
        setPolicyQualifiersRejected(pKIXParameters.getPolicyQualifiersRejected());
        setSigProvider(pKIXParameters.getSigProvider());
        setTargetCertConstraints(pKIXParameters.getTargetCertConstraints());
        try {
            setTrustAnchors(pKIXParameters.getTrustAnchors());
            if (pKIXParameters instanceof C113415Hm) {
                C113415Hm r3 = (C113415Hm) pKIXParameters;
                this.A00 = r3.A00;
                this.A09 = r3.A09;
                this.A08 = r3.A08;
                AbstractC117205Yy r02 = r3.A07;
                if (r02 == null) {
                    r0 = null;
                } else {
                    r0 = (AbstractC117205Yy) r02.clone();
                }
                this.A07 = r0;
                this.A02 = C12980iv.A0x(r3.A02);
                this.A01 = C12980iv.A0x(r3.A01);
                this.A06 = new HashSet(r3.A06);
                this.A05 = new HashSet(r3.A05);
                this.A04 = new HashSet(r3.A04);
                this.A03 = new HashSet(r3.A03);
            }
        } catch (Exception e) {
            throw C12990iw.A0m(e.getMessage());
        }
    }

    @Override // java.security.cert.PKIXParameters, java.security.cert.CertPathParameters, java.lang.Object
    public Object clone() {
        try {
            C113415Hm r0 = new C113415Hm(getTrustAnchors());
            r0.A00(this);
            return r0;
        } catch (Exception e) {
            throw C12990iw.A0m(e.getMessage());
        }
    }

    @Override // java.security.cert.PKIXParameters
    public void setTargetCertConstraints(CertSelector certSelector) {
        C113475Hs r1;
        super.setTargetCertConstraints(certSelector);
        if (certSelector != null) {
            X509CertSelector x509CertSelector = (X509CertSelector) certSelector;
            if (x509CertSelector != null) {
                r1 = new C113475Hs();
                r1.setAuthorityKeyIdentifier(x509CertSelector.getAuthorityKeyIdentifier());
                r1.setBasicConstraints(x509CertSelector.getBasicConstraints());
                r1.setCertificate(x509CertSelector.getCertificate());
                r1.setCertificateValid(x509CertSelector.getCertificateValid());
                r1.setMatchAllSubjectAltNames(x509CertSelector.getMatchAllSubjectAltNames());
                try {
                    r1.setPathToNames(x509CertSelector.getPathToNames());
                    r1.setExtendedKeyUsage(x509CertSelector.getExtendedKeyUsage());
                    r1.setNameConstraints(x509CertSelector.getNameConstraints());
                    r1.setPolicy(x509CertSelector.getPolicy());
                    r1.setSubjectPublicKeyAlgID(x509CertSelector.getSubjectPublicKeyAlgID());
                    r1.setSubjectAlternativeNames(x509CertSelector.getSubjectAlternativeNames());
                    r1.setIssuer(x509CertSelector.getIssuer());
                    r1.setKeyUsage(x509CertSelector.getKeyUsage());
                    r1.setPrivateKeyValid(x509CertSelector.getPrivateKeyValid());
                    r1.setSerialNumber(x509CertSelector.getSerialNumber());
                    r1.setSubject(x509CertSelector.getSubject());
                    r1.setSubjectKeyIdentifier(x509CertSelector.getSubjectKeyIdentifier());
                    r1.setSubjectPublicKey(x509CertSelector.getSubjectPublicKey());
                } catch (IOException e) {
                    throw C12970iu.A0f(C12960it.A0b("error in passed in selector: ", e));
                }
            } else {
                throw C12970iu.A0f("cannot create from null selector");
            }
        } else {
            r1 = null;
        }
        this.A07 = r1;
    }
}
