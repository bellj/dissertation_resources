package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.3VK  reason: invalid class name */
/* loaded from: classes2.dex */
public final /* synthetic */ class AnonymousClass3VK implements AnonymousClass5TP {
    public final /* synthetic */ AnonymousClass1CR A00;
    public final /* synthetic */ UserJid A01;
    public final /* synthetic */ Integer A02;
    public final /* synthetic */ Integer A03;
    public final /* synthetic */ Integer A04;
    public final /* synthetic */ String A05;

    public /* synthetic */ AnonymousClass3VK(AnonymousClass1CR r1, UserJid userJid, Integer num, Integer num2, Integer num3, String str) {
        this.A00 = r1;
        this.A01 = userJid;
        this.A02 = num;
        this.A03 = num2;
        this.A04 = num3;
        this.A05 = str;
    }

    @Override // X.AnonymousClass5TP
    public final void AQG(UserJid userJid) {
        Long l;
        AnonymousClass1CR r1 = this.A00;
        UserJid userJid2 = this.A01;
        Integer num = this.A02;
        Integer num2 = this.A03;
        Integer num3 = this.A04;
        String str = this.A05;
        AnonymousClass19Q r3 = r1.A00;
        if (num3 != null) {
            l = C12980iv.A0l(num3.intValue() + 1);
        } else {
            l = null;
        }
        C616030z r12 = new C616030z();
        r12.A04 = userJid2.getRawString();
        r12.A05 = r3.A00;
        r12.A06 = str;
        r12.A03 = C12980iv.A0l(r3.A08.getAndIncrement());
        r12.A01 = num;
        r12.A00 = num2;
        r12.A02 = l;
        r3.A04.A07(r12);
    }
}
