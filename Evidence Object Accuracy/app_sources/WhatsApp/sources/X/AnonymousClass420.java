package X;

import com.whatsapp.jid.UserJid;
import java.util.Collection;

/* renamed from: X.420  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass420 extends C27131Gd {
    public final /* synthetic */ AbstractActivityC35431hr A00;

    public AnonymousClass420(AbstractActivityC35431hr r1) {
        this.A00 = r1;
    }

    @Override // X.C27131Gd
    public void A00(AbstractC14640lm r2) {
        this.A00.A07.notifyDataSetChanged();
    }

    @Override // X.C27131Gd
    public void A03(UserJid userJid) {
        this.A00.A07.notifyDataSetChanged();
    }

    @Override // X.C27131Gd
    public void A06(Collection collection) {
        this.A00.A07.notifyDataSetChanged();
    }
}
