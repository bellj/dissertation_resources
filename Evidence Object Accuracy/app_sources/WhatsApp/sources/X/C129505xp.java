package X;

/* renamed from: X.5xp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129505xp {
    public final String A00;
    public final String A01;

    public /* synthetic */ C129505xp(String str, String str2) {
        this.A01 = str;
        this.A00 = str2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C129505xp)) {
            return false;
        }
        C129505xp r4 = (C129505xp) obj;
        if (!this.A01.equals(r4.A01) || !this.A00.equals(r4.A00)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (this.A01.hashCode() * 31) + (this.A00.hashCode() * 31);
    }
}
