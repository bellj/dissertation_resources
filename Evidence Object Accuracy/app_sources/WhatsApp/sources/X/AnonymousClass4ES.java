package X;

import android.text.TextUtils;
import android.util.Base64;
import java.nio.ByteBuffer;
import java.security.SecureRandom;

/* renamed from: X.4ES  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4ES {
    public static String A00(C91664Sp r16, String str) {
        if (TextUtils.isEmpty(str) || r16 == null) {
            return null;
        }
        byte[] bArr = new byte[24];
        new SecureRandom().nextBytes(bArr);
        ByteBuffer allocate = ByteBuffer.allocate(4);
        allocate.putInt(1);
        byte[] array = allocate.array();
        byte[] bytes = "cd7962b7".getBytes();
        ByteBuffer allocate2 = ByteBuffer.allocate(4);
        allocate2.putInt(r16.A02);
        byte[] array2 = allocate2.array();
        ByteBuffer allocate3 = ByteBuffer.allocate(8);
        allocate3.putLong(r16.A03);
        byte[] A05 = C16050oM.A05(bytes, allocate3.array(), array2, array, r16.A01, bArr);
        byte[] A01 = AnonymousClass20E.A01(r16.A04, r16.A00);
        byte[] bytes2 = str.getBytes();
        AnonymousClass20G A00 = AnonymousClass20F.A00(A01, bArr);
        AnonymousClass20I r3 = new AnonymousClass20I();
        byte[] bArr2 = new byte[32];
        A00.A01(bArr2, bArr2, 0, 32, 0);
        int length = bytes2.length;
        byte[] bArr3 = new byte[length + 16];
        A00.A01(bytes2, bArr3, 0, length, 16);
        r3.AIc(new AnonymousClass20K(bArr2));
        r3.update(bArr3, 16, length);
        r3.A97(bArr3, 0);
        return Base64.encodeToString(C16050oM.A05(A05, bArr3), 0);
    }
}
