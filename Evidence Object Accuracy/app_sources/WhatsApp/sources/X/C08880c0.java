package X;

import java.io.File;
import java.io.FileFilter;

/* renamed from: X.0c0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08880c0 implements FileFilter {
    public final /* synthetic */ AnonymousClass00I A00;

    public C08880c0(AnonymousClass00I r1) {
        this.A00 = r1;
    }

    @Override // java.io.FileFilter
    public boolean accept(File file) {
        return !file.getName().equals("MultiDex.lock");
    }
}
