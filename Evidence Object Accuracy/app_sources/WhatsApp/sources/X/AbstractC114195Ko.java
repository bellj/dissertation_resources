package X;

import java.io.Closeable;

/* renamed from: X.5Ko  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC114195Ko extends AbstractC10990fX implements Closeable {
    public static final C113615Ih A00 = new C113615Ih();

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        if (!(this instanceof C114255Ku)) {
            throw C12960it.A0U("Cannot be invoked on Dispatchers.IO");
        }
        throw C12980iv.A0u("Dispatchers.Default cannot be closed");
    }
}
