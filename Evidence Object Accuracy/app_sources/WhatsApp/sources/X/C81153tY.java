package X;

/* renamed from: X.3tY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C81153tY extends AbstractC81183tb {
    public final /* synthetic */ AnonymousClass4XX this$0;
    public final /* synthetic */ int val$expectedValuesPerKey = 2;

    public C81153tY(AnonymousClass4XX r2, int i) {
        this.this$0 = r2;
    }

    @Override // X.AbstractC81183tb
    public AbstractC117165Yt build() {
        return AnonymousClass4Yf.newListMultimap(this.this$0.createMap(), new AnonymousClass51R(2));
    }
}
