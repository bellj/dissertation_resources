package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.graphics.drawable.TransitionDrawable;
import org.npci.commonlibrary.GetCredential;

/* renamed from: X.5Zo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117345Zo extends AnimatorListenerAdapter {
    public final /* synthetic */ int A00;
    public final /* synthetic */ GetCredential A01;
    public final /* synthetic */ boolean A02;

    public C117345Zo(GetCredential getCredential, int i, boolean z) {
        this.A01 = getCredential;
        this.A02 = z;
        this.A00 = i;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        super.onAnimationEnd(animator);
        if (!this.A02) {
            GetCredential getCredential = this.A01;
            getCredential.A02.setVisibility(8);
            getCredential.A03.setVisibility(8);
            getCredential.A01.resetTransition();
        }
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        super.onAnimationStart(animator);
        boolean z = this.A02;
        GetCredential getCredential = this.A01;
        TransitionDrawable transitionDrawable = getCredential.A01;
        if (z) {
            transitionDrawable.startTransition(300);
            getCredential.A02.setVisibility(0);
            getCredential.A03.setVisibility(0);
            if (getCredential.A02.getY() == 0.0f) {
                getCredential.A02.setY((float) (-this.A00));
                return;
            }
            return;
        }
        transitionDrawable.reverseTransition(300);
    }
}
