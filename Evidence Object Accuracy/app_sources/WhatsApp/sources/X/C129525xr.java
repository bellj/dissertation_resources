package X;

import androidx.fragment.app.DialogFragment;
import java.lang.ref.WeakReference;

/* renamed from: X.5xr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129525xr {
    public DialogFragment A00;
    public WeakReference A01;

    public C129525xr(AnonymousClass6MC r2) {
        this.A01 = C12970iu.A10(r2);
    }

    public void A00() {
        DialogFragment dialogFragment = this.A00;
        if (dialogFragment != null) {
            if (dialogFragment.A0c()) {
                dialogFragment.A1B();
            }
            this.A00 = null;
        }
    }
}
