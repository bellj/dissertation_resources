package X;

import com.whatsapp.voipcalling.GlVideoRenderer;
import org.chromium.net.UrlRequest;

/* renamed from: X.4Ya  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92924Ya {
    public final int A00;
    public final int A01;
    @Deprecated
    public final byte[] A02;
    public final byte[] A03;
    public final int[] A04;
    public final int[] A05;
    public final String[] A06;
    public final C92804Xm[] A07;

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public C92924Ya(byte[] bArr) {
        C92804Xm[] r0;
        this.A03 = bArr;
        this.A02 = bArr;
        int A0L = C72453ed.A0L(bArr, 8);
        int[] iArr = new int[A0L];
        this.A05 = iArr;
        this.A06 = new String[A0L];
        int i = 10;
        int i2 = 0;
        boolean z = false;
        boolean z2 = false;
        int i3 = 1;
        while (i3 < A0L) {
            int i4 = i3 + 1;
            int i5 = i + 1;
            iArr[i3] = i5;
            int i6 = 3;
            switch (bArr[i]) {
                case 1:
                    i6 = 3 + C72453ed.A0L(this.A03, i5);
                    if (i6 > i2) {
                        i2 = i6;
                    }
                    i3 = i4;
                    break;
                case 2:
                case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                case UrlRequest.Status.READING_RESPONSE /* 14 */:
                default:
                    throw C72453ed.A0h();
                case 3:
                case 4:
                case 9:
                case 10:
                case 11:
                case 12:
                    i3 = i4;
                    i6 = 5;
                    break;
                case 5:
                case 6:
                    i6 = 9;
                    i4++;
                    i3 = i4;
                    break;
                case 7:
                case 8:
                case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                case 19:
                case C43951xu.A01:
                    i3 = i4;
                    break;
                case 15:
                    i6 = 4;
                    i3 = i4;
                    break;
                case 17:
                    i3 = i4;
                    z = true;
                    z2 = true;
                    i6 = 5;
                    break;
                case 18:
                    i3 = i4;
                    z2 = true;
                    i6 = 5;
                    break;
            }
            i += i6;
        }
        this.A01 = i2;
        this.A00 = i;
        int[] iArr2 = null;
        if (z) {
            r0 = new C92804Xm[A0L];
        } else {
            r0 = null;
        }
        this.A07 = r0;
        if (z2) {
            char[] cArr = new char[i2];
            int A03 = A03();
            for (int A0L2 = C72453ed.A0L(this.A03, A03 - 2); A0L2 > 0; A0L2--) {
                String A0D = A0D(cArr, A03);
                int A0J = C72453ed.A0J(this.A03, A03 + 2);
                int i7 = A03 + 6;
                if ("BootstrapMethods".equals(A0D)) {
                    int A0L3 = C72453ed.A0L(this.A03, i7);
                    iArr2 = new int[A0L3];
                    int i8 = i7 + 2;
                    for (int i9 = 0; i9 < A0L3; i9++) {
                        iArr2[i9] = i8;
                        i8 += (A00(this, i8) << 1) + 4;
                    }
                } else {
                    A03 = i7 + A0J;
                }
            }
            throw C72453ed.A0h();
        }
        this.A04 = iArr2;
    }

    public static int A00(C92924Ya r1, int i) {
        return r1.A05(i + 2);
    }

    public static int A01(C92924Ya r2, int i) {
        return r2.A04(r2.A05[r2.A05(i + 1)]);
    }

    public static String A02(C92924Ya r1, char[] cArr, int[] iArr, int i) {
        return r1.A0D(cArr, iArr[r1.A05(i)]);
    }

    public final int A03() {
        int i = this.A00;
        byte[] bArr = this.A03;
        int A0L = i + 8 + (C72453ed.A0L(bArr, i + 6) << 1);
        int A0L2 = C72453ed.A0L(bArr, A0L);
        int i2 = A0L + 2;
        while (true) {
            int i3 = A0L2 - 1;
            if (A0L2 <= 0) {
                break;
            }
            int A0L3 = C72453ed.A0L(bArr, i2 + 6);
            i2 += 8;
            while (true) {
                int i4 = A0L3 - 1;
                A0L2 = i3;
                if (A0L3 > 0) {
                    i2 += C72453ed.A0J(bArr, i2 + 2) + 6;
                    A0L3 = i4;
                }
            }
        }
        int A0L4 = C72453ed.A0L(bArr, i2);
        int i5 = i2 + 2;
        while (true) {
            int i6 = A0L4 - 1;
            if (A0L4 <= 0) {
                return i5 + 2;
            }
            int A0L5 = C72453ed.A0L(bArr, i5 + 6);
            i5 += 8;
            while (true) {
                int i7 = A0L5 - 1;
                A0L4 = i6;
                if (A0L5 > 0) {
                    i5 += C72453ed.A0J(bArr, i5 + 2) + 6;
                    A0L5 = i7;
                }
            }
        }
    }

    public int A04(int i) {
        return C72453ed.A0J(this.A03, i);
    }

    public int A05(int i) {
        return C72453ed.A0L(this.A03, i);
    }

    public final int A06(String str, C95474dn r13, char[] cArr, int i) {
        Object obj;
        int i2 = 0;
        if (r13 == null) {
            int i3 = this.A03[i] & 255;
            if (i3 == 64) {
                return A07(null, cArr, i + 3, true);
            }
            if (i3 == 91) {
                return A07(null, cArr, i + 1, false);
            }
            return i3 != 101 ? i + 3 : i + 5;
        }
        byte[] bArr = this.A03;
        int i4 = i + 1;
        int i5 = bArr[i] & 255;
        if (i5 != 64) {
            if (i5 != 70) {
                if (i5 == 83) {
                    r13.A09(str, Short.valueOf((short) C72453ed.A0J(bArr, this.A05[C72453ed.A0L(bArr, i4)])));
                } else if (i5 == 99) {
                    String A0D = A0D(cArr, i4);
                    r13.A09(str, C95574dz.A03(A0D, 0, A0D.length()));
                } else if (i5 == 101) {
                    String A0D2 = A0D(cArr, i4);
                    String A0D3 = A0D(cArr, i4 + 2);
                    r13.A00++;
                    if (r13.A06) {
                        AnonymousClass4YW.A01(str, r13.A04, r13.A05);
                    }
                    C95044cz r2 = r13.A04;
                    AnonymousClass4YW r1 = r13.A05;
                    r2.A07(101, r1.A02(A0D2));
                    AnonymousClass4YW.A01(A0D3, r2, r1);
                    return i4 + 4;
                } else if (i5 == 115) {
                    r13.A09(str, A0D(cArr, i4));
                } else if (!(i5 == 73 || i5 == 74)) {
                    if (i5 == 90) {
                        if (C72453ed.A0J(bArr, this.A05[C72453ed.A0L(bArr, i4)]) == 0) {
                            obj = Boolean.FALSE;
                        } else {
                            obj = Boolean.TRUE;
                        }
                        r13.A09(str, obj);
                    } else if (i5 != 91) {
                        switch (i5) {
                            case 66:
                                r13.A09(str, Byte.valueOf((byte) C72453ed.A0J(bArr, this.A05[C72453ed.A0L(bArr, i4)])));
                                break;
                            case 67:
                                r13.A09(str, Character.valueOf((char) C72453ed.A0J(bArr, this.A05[C72453ed.A0L(bArr, i4)])));
                                break;
                            case 68:
                                break;
                            default:
                                throw C72453ed.A0h();
                        }
                    } else {
                        int A0L = C72453ed.A0L(bArr, i4);
                        int i6 = i4 + 2;
                        if (A0L == 0) {
                            return A07(r13.A07(str), cArr, i6 - 2, false);
                        }
                        int i7 = bArr[i6] & 255;
                        if (i7 == 70) {
                            float[] fArr = new float[A0L];
                            while (i2 < A0L) {
                                fArr[i2] = Float.intBitsToFloat(A01(this, i6));
                                i6 += 3;
                                i2++;
                            }
                            r13.A09(str, fArr);
                            return i6;
                        } else if (i7 == 83) {
                            short[] sArr = new short[A0L];
                            while (i2 < A0L) {
                                sArr[i2] = (short) A01(this, i6);
                                i6 += 3;
                                i2++;
                            }
                            r13.A09(str, sArr);
                            return i6;
                        } else if (i7 == 90) {
                            boolean[] zArr = new boolean[A0L];
                            for (int i8 = 0; i8 < A0L; i8++) {
                                zArr[i8] = C12960it.A1S(A01(this, i6));
                                i6 += 3;
                            }
                            r13.A09(str, zArr);
                            return i6;
                        } else if (i7 == 73) {
                            int[] iArr = new int[A0L];
                            while (i2 < A0L) {
                                iArr[i2] = A01(this, i6);
                                i6 += 3;
                                i2++;
                            }
                            r13.A09(str, iArr);
                            return i6;
                        } else if (i7 != 74) {
                            switch (i7) {
                                case 66:
                                    byte[] bArr2 = new byte[A0L];
                                    while (i2 < A0L) {
                                        bArr2[i2] = (byte) A01(this, i6);
                                        i6 += 3;
                                        i2++;
                                    }
                                    r13.A09(str, bArr2);
                                    return i6;
                                case 67:
                                    char[] cArr2 = new char[A0L];
                                    while (i2 < A0L) {
                                        cArr2[i2] = (char) A01(this, i6);
                                        i6 += 3;
                                        i2++;
                                    }
                                    r13.A09(str, cArr2);
                                    return i6;
                                case 68:
                                    double[] dArr = new double[A0L];
                                    while (i2 < A0L) {
                                        dArr[i2] = Double.longBitsToDouble(A0B(this.A05[C72453ed.A0L(bArr, i6 + 1)]));
                                        i6 += 3;
                                        i2++;
                                    }
                                    r13.A09(str, dArr);
                                    return i6;
                                default:
                                    return A07(r13.A07(str), cArr, i6 - 2, false);
                            }
                        } else {
                            long[] jArr = new long[A0L];
                            while (i2 < A0L) {
                                jArr[i2] = A0B(this.A05[C72453ed.A0L(bArr, i6 + 1)]);
                                i6 += 3;
                                i2++;
                            }
                            r13.A09(str, jArr);
                            return i6;
                        }
                    }
                }
                return i4 + 2;
            }
            r13.A09(str, A0C(cArr, C72453ed.A0L(bArr, i4)));
            return i4 + 2;
        }
        String A0D4 = A0D(cArr, i4);
        r13.A00++;
        if (r13.A06) {
            AnonymousClass4YW.A01(str, r13.A04, r13.A05);
        }
        C95044cz r4 = r13.A04;
        AnonymousClass4YW r22 = r13.A05;
        r4.A07(64, r22.A02(A0D4));
        r4.A04(0);
        return A07(new C95474dn(null, r4, r22, true), cArr, i4 + 2, true);
    }

    public final int A07(C95474dn r5, char[] cArr, int i, boolean z) {
        int A0L = C72453ed.A0L(this.A03, i);
        int i2 = i + 2;
        if (!z) {
            while (true) {
                int i3 = A0L - 1;
                if (A0L <= 0) {
                    break;
                }
                i2 = A06(null, r5, cArr, i2);
                A0L = i3;
            }
        } else {
            while (true) {
                int i4 = A0L - 1;
                if (A0L <= 0) {
                    break;
                }
                i2 = A06(A0D(cArr, i2), r5, cArr, i2 + 2);
                A0L = i4;
            }
        }
        if (r5 != null) {
            r5.A08();
        }
        return i2;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0068  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x007c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int A08(X.AnonymousClass4UJ r14, int r15) {
        /*
            r13 = this;
            byte[] r6 = r13.A03
            int r9 = X.C72453ed.A0J(r6, r15)
            int r1 = r9 >>> 24
            r5 = 1
            if (r1 == 0) goto L_0x0062
            if (r1 == r5) goto L_0x0062
            r0 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            switch(r1) {
                case 16: goto L_0x0068;
                case 17: goto L_0x0068;
                case 18: goto L_0x0068;
                case 19: goto L_0x005f;
                case 20: goto L_0x005f;
                case 21: goto L_0x005f;
                case 22: goto L_0x0062;
                case 23: goto L_0x0068;
                default: goto L_0x0012;
            }
        L_0x0012:
            switch(r1) {
                case 64: goto L_0x0023;
                case 65: goto L_0x0023;
                case 66: goto L_0x0068;
                case 67: goto L_0x0021;
                case 68: goto L_0x0021;
                case 69: goto L_0x0021;
                case 70: goto L_0x0021;
                case 71: goto L_0x001a;
                case 72: goto L_0x001a;
                case 73: goto L_0x001a;
                case 74: goto L_0x001a;
                case 75: goto L_0x001a;
                default: goto L_0x0015;
            }
        L_0x0015:
            java.lang.IllegalArgumentException r0 = X.C72453ed.A0h()
            throw r0
        L_0x001a:
            r0 = -16776961(0xffffffffff0000ff, float:-1.7014636E38)
            r9 = r9 & r0
            int r15 = r15 + 4
            goto L_0x006c
        L_0x0021:
            r9 = r9 & r0
            goto L_0x006a
        L_0x0023:
            r9 = r9 & r0
            int r0 = r15 + 1
            int r10 = X.C72453ed.A0L(r6, r0)
            int r15 = r15 + 3
            X.4br[] r8 = new X.C94464br[r10]
            r14.A0H = r8
            X.4br[] r7 = new X.C94464br[r10]
            r14.A0G = r7
            int[] r4 = new int[r10]
            r14.A0C = r4
            r3 = 0
        L_0x0039:
            if (r3 >= r10) goto L_0x006c
            int r11 = X.C72453ed.A0L(r6, r15)
            int r12 = A00(r13, r15)
            int r0 = r15 + 4
            int r2 = X.C72453ed.A0L(r6, r0)
            int r15 = r15 + 6
            X.4br[] r1 = r14.A0I
            X.4br r0 = r13.A0G(r1, r11)
            r8[r3] = r0
            int r11 = r11 + r12
            X.4br r0 = r13.A0G(r1, r11)
            r7[r3] = r0
            r4[r3] = r2
            int r3 = r3 + 1
            goto L_0x0039
        L_0x005f:
            r9 = r9 & r0
            int r15 = r15 + r5
            goto L_0x006c
        L_0x0062:
            r0 = -65536(0xffffffffffff0000, float:NaN)
            r9 = r9 & r0
            int r15 = r15 + 2
            goto L_0x006c
        L_0x0068:
            r9 = r9 & -256(0xffffffffffffff00, float:NaN)
        L_0x006a:
            int r15 = r15 + 3
        L_0x006c:
            r14.A06 = r9
            byte r0 = r6[r15]
            r1 = r0 & 255(0xff, float:3.57E-43)
            if (r1 != 0) goto L_0x007c
            r0 = 0
        L_0x0075:
            r14.A0A = r0
            int r15 = r15 + r5
            int r0 = r1 << 1
            int r15 = r15 + r0
            return r15
        L_0x007c:
            X.4VT r0 = new X.4VT
            r0.<init>(r6, r15)
            goto L_0x0075
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C92924Ya.A08(X.4UJ, int):int");
    }

    public final int A09(char[] cArr, Object[] objArr, C94464br[] r6, int i, int i2) {
        Integer num;
        byte[] bArr = this.A03;
        int i3 = i + 1;
        switch (bArr[i] & 255) {
            case 0:
                num = AbstractC116905Xj.A05;
                objArr[i2] = num;
                return i3;
            case 1:
                num = AbstractC116905Xj.A02;
                objArr[i2] = num;
                return i3;
            case 2:
                num = AbstractC116905Xj.A01;
                objArr[i2] = num;
                return i3;
            case 3:
                num = AbstractC116905Xj.A00;
                objArr[i2] = num;
                return i3;
            case 4:
                num = AbstractC116905Xj.A03;
                objArr[i2] = num;
                return i3;
            case 5:
                num = AbstractC116905Xj.A04;
                objArr[i2] = num;
                return i3;
            case 6:
                num = AbstractC116905Xj.A06;
                objArr[i2] = num;
                return i3;
            case 7:
                objArr[i2] = A02(this, cArr, this.A05, i3);
                return i3 + 2;
            case 8:
                objArr[i2] = A0G(r6, C72453ed.A0L(bArr, i3));
                return i3 + 2;
            default:
                throw C72453ed.A0h();
        }
    }

    public final int A0A(int[] iArr, int i) {
        if (iArr == null || i >= iArr.length) {
            return -1;
        }
        int i2 = iArr[i];
        byte[] bArr = this.A03;
        if ((bArr[i2] & 255) >= 67) {
            return C72453ed.A0L(bArr, i2 + 1);
        }
        return -1;
    }

    public long A0B(int i) {
        byte[] bArr = this.A03;
        return (((long) C72453ed.A0J(bArr, i)) << 32) | (((long) C72453ed.A0J(bArr, i + 4)) & 4294967295L);
    }

    public Object A0C(char[] cArr, int i) {
        int[] iArr = this.A05;
        int i2 = iArr[i];
        byte[] bArr = this.A03;
        byte b = bArr[i2 - 1];
        switch (b) {
            case 3:
                return Integer.valueOf(C72453ed.A0J(bArr, i2));
            case 4:
                return Float.valueOf(Float.intBitsToFloat(C72453ed.A0J(bArr, i2)));
            case 5:
                return Long.valueOf(A0B(i2));
            case 6:
                return Double.valueOf(Double.longBitsToDouble(A0B(i2)));
            case 7:
                String A0D = A0D(cArr, i2);
                int i3 = 12;
                if (A0D.charAt(0) == '[') {
                    i3 = 9;
                }
                return new C95574dz(A0D, i3, 0, A0D.length());
            case 8:
                return A0D(cArr, i2);
            default:
                switch (b) {
                    case 15:
                        int i4 = bArr[i2] & 255;
                        int i5 = iArr[C72453ed.A0L(bArr, i2 + 1)];
                        int i6 = iArr[A00(this, i5)];
                        return new C92854Xr(A02(this, cArr, iArr, i5), A0D(cArr, i6), A0D(cArr, i6 + 2), i4, C12960it.A1V(bArr[i5 - 1], 11));
                    case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                        String A0D2 = A0D(cArr, i2);
                        return new C95574dz(A0D2, 11, 0, A0D2.length());
                    case 17:
                        C92804Xm[] r9 = this.A07;
                        C92804Xm r0 = r9[i];
                        if (r0 != null) {
                            return r0;
                        }
                        int i7 = iArr[A00(this, i2)];
                        String A0D3 = A0D(cArr, i7);
                        String A0D4 = A0D(cArr, i7 + 2);
                        int i8 = this.A04[C72453ed.A0L(bArr, i2)];
                        C92854Xr r5 = (C92854Xr) A0C(cArr, C72453ed.A0L(bArr, i8));
                        int A00 = A00(this, i8);
                        Object[] objArr = new Object[A00];
                        int i9 = i8 + 4;
                        for (int i10 = 0; i10 < A00; i10++) {
                            objArr[i10] = A0C(cArr, C72453ed.A0L(bArr, i9));
                            i9 += 2;
                        }
                        C92804Xm r02 = new C92804Xm(A0D3, A0D4, r5, objArr);
                        r9[i] = r02;
                        return r02;
                    default:
                        throw C72453ed.A0h();
                }
        }
    }

    public String A0D(char[] cArr, int i) {
        byte[] bArr = this.A03;
        int A0L = C72453ed.A0L(bArr, i);
        if (i == 0 || A0L == 0) {
            return null;
        }
        String[] strArr = this.A06;
        String str = strArr[A0L];
        if (str != null) {
            return str;
        }
        int i2 = this.A05[A0L];
        String A0E = A0E(cArr, i2 + 2, C72453ed.A0L(bArr, i2));
        strArr[A0L] = A0E;
        return A0E;
    }

    public final String A0E(char[] cArr, int i, int i2) {
        int i3;
        int i4;
        int i5 = i2 + i;
        byte[] bArr = this.A03;
        int i6 = 0;
        while (i < i5) {
            int i7 = i + 1;
            byte b = bArr[i];
            if ((b & 128) == 0) {
                i3 = i6 + 1;
                i4 = b & Byte.MAX_VALUE;
            } else {
                i3 = i6 + 1;
                if ((b & 224) == 192) {
                    i = i7 + 1;
                    cArr[i6] = (char) (((b & 31) << 6) + (bArr[i7] & 63));
                    i6 = i3;
                } else {
                    int i8 = i7 + 1;
                    i7 = i8 + 1;
                    i4 = ((b & 15) << 12) + ((bArr[i7] & 63) << 6) + (bArr[i8] & 63);
                }
            }
            cArr[i6] = (char) i4;
            i = i7;
            i6 = i3;
        }
        return new String(cArr, 0, i6);
    }

    public final C94914ck A0F(String str, C94914ck[] r6, int i, int i2) {
        String str2;
        int length = r6.length;
        int i3 = 0;
        while (true) {
            if (i3 >= length) {
                str2 = new C94914ck(str).A02;
                break;
            }
            str2 = r6[i3].A02;
            if (str2.equals(str)) {
                break;
            }
            i3++;
        }
        C94914ck r3 = new C94914ck(str2);
        byte[] bArr = new byte[i2];
        r3.A01 = bArr;
        System.arraycopy(this.A03, i, bArr, 0, i2);
        return r3;
    }

    public final C94464br A0G(C94464br[] r3, int i) {
        if (r3[i] == null) {
            r3[i] = new C94464br();
        }
        C94464br r1 = r3[i];
        r1.A05 = (short) (r1.A05 & -2);
        return r1;
    }

    public final void A0H(AnonymousClass4UJ r11, AnonymousClass4YT r12, int i, boolean z) {
        C95474dn[] r8;
        byte[] bArr = this.A03;
        int i2 = i + 1;
        int i3 = bArr[i] & 255;
        AnonymousClass5M6 r122 = (AnonymousClass5M6) r12;
        if (z) {
            r122.A0D = i3;
        } else {
            r122.A01 = i3;
        }
        char[] cArr = r11.A0B;
        for (int i4 = 0; i4 < i3; i4++) {
            int A0L = C72453ed.A0L(bArr, i2);
            i2 += 2;
            while (true) {
                int i5 = A0L - 1;
                if (A0L > 0) {
                    String A0D = A0D(cArr, i2);
                    int i6 = i2 + 2;
                    if (z) {
                        r8 = r122.A0b;
                        if (r8 == null) {
                            r8 = new C95474dn[C95574dz.A05(r122.A0i).length];
                            r122.A0b = r8;
                        }
                    } else {
                        r8 = r122.A0a;
                        if (r8 == null) {
                            r8 = new C95474dn[C95574dz.A05(r122.A0i).length];
                            r122.A0a = r8;
                        }
                    }
                    C95474dn A01 = C95474dn.A01(A0D, r8[i4], r122.A0l);
                    r8[i4] = A01;
                    i2 = A07(A01, cArr, i6, true);
                    A0L = i5;
                }
            }
        }
    }

    public final void A0I(C94464br[] r3, int i) {
        if (r3[i] == null) {
            r3[i] = new C94464br();
            C94464br r1 = r3[i];
            r1.A05 = (short) (r1.A05 | 1);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x008a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int[] A0J(X.AnonymousClass4UJ r14, X.AnonymousClass4YT r15, int r16, boolean r17) {
        /*
            r13 = this;
            char[] r11 = r14.A0B
            byte[] r10 = r13.A03
            r0 = r16
            int r9 = X.C72453ed.A0L(r10, r0)
            int[] r8 = new int[r9]
            int r1 = r16 + 2
            r7 = 0
        L_0x000f:
            if (r7 >= r9) goto L_0x0094
            r8[r7] = r1
            int r2 = X.C72453ed.A0J(r10, r1)
            int r4 = r2 >>> 24
            r0 = 23
            if (r4 == r0) goto L_0x0049
            switch(r4) {
                case 16: goto L_0x0049;
                case 17: goto L_0x0049;
                case 18: goto L_0x0049;
                default: goto L_0x0020;
            }
        L_0x0020:
            switch(r4) {
                case 64: goto L_0x0028;
                case 65: goto L_0x0028;
                case 66: goto L_0x0049;
                case 67: goto L_0x0049;
                case 68: goto L_0x0049;
                case 69: goto L_0x0049;
                case 70: goto L_0x0049;
                case 71: goto L_0x004c;
                case 72: goto L_0x004c;
                case 73: goto L_0x004c;
                case 74: goto L_0x004c;
                case 75: goto L_0x004c;
                default: goto L_0x0023;
            }
        L_0x0023:
            java.lang.IllegalArgumentException r0 = X.C72453ed.A0h()
            throw r0
        L_0x0028:
            int r0 = r1 + 1
            int r0 = X.C72453ed.A0L(r10, r0)
            int r1 = r1 + 3
        L_0x0030:
            int r6 = r0 + -1
            if (r0 <= 0) goto L_0x004e
            int r5 = X.C72453ed.A0L(r10, r1)
            int r3 = A00(r13, r1)
            int r1 = r1 + 6
            X.4br[] r0 = r14.A0I
            r13.A0G(r0, r5)
            int r5 = r5 + r3
            r13.A0G(r0, r5)
            r0 = r6
            goto L_0x0030
        L_0x0049:
            int r1 = r1 + 3
            goto L_0x004e
        L_0x004c:
            int r1 = r1 + 4
        L_0x004e:
            byte r0 = r10[r1]
            r3 = r0 & 255(0xff, float:3.57E-43)
            r0 = 66
            r6 = 0
            r5 = 1
            if (r4 != r0) goto L_0x008a
            if (r3 == 0) goto L_0x005f
            X.4VT r6 = new X.4VT
            r6.<init>(r10, r1)
        L_0x005f:
            int r0 = r3 << 1
            int r0 = r0 + r5
            int r1 = r1 + r0
            java.lang.String r4 = r13.A0D(r11, r1)
            int r12 = r1 + 2
            r3 = r2 & -256(0xffffffffffffff00, float:NaN)
            r2 = r15
            X.5M6 r2 = (X.AnonymousClass5M6) r2
            X.4YW r1 = r2.A0l
            if (r17 == 0) goto L_0x0081
            X.4dn r0 = r2.A0F
            X.4dn r0 = X.C95474dn.A02(r4, r0, r1, r6, r3)
            r2.A0F = r0
        L_0x007a:
            int r1 = r13.A07(r0, r11, r12, r5)
        L_0x007e:
            int r7 = r7 + 1
            goto L_0x000f
        L_0x0081:
            X.4dn r0 = r2.A0E
            X.4dn r0 = X.C95474dn.A02(r4, r0, r1, r6, r3)
            r2.A0E = r0
            goto L_0x007a
        L_0x008a:
            int r0 = r3 << 1
            int r0 = r0 + 3
            int r1 = r1 + r0
            int r1 = r13.A07(r6, r11, r1, r5)
            goto L_0x007e
        L_0x0094:
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C92924Ya.A0J(X.4UJ, X.4YT, int, boolean):int[]");
    }
}
