package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5lP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122405lP extends AbstractC118825cR {
    public TextView A00;
    public TextView A01;

    public C122405lP(View view) {
        super(view);
        this.A01 = C12960it.A0I(view, R.id.title_text);
        this.A00 = C12960it.A0I(view, R.id.subtitle_text);
    }
}
