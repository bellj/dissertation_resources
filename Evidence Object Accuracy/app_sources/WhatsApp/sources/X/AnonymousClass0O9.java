package X;

import android.view.View;
import java.util.ArrayList;

/* renamed from: X.0O9  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0O9 {
    public float A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public View A06;
    public AbstractC000600h A07;
    public Object A08 = null;
    public Object A09;
    public Object A0A;
    public Object A0B;
    public Object A0C;
    public ArrayList A0D;
    public ArrayList A0E;
    public boolean A0F;
    public boolean A0G;

    public AnonymousClass0O9() {
        Object obj = AnonymousClass01E.A0m;
        this.A0A = obj;
        this.A09 = obj;
        this.A0B = null;
        this.A0C = obj;
        this.A07 = null;
        this.A00 = 1.0f;
        this.A06 = null;
    }
}
