package X;

import android.content.Intent;
import com.whatsapp.R;
import com.whatsapp.payments.ui.BrazilPaymentActivity;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;

/* renamed from: X.6AI  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6AI implements AnonymousClass6MO {
    public final /* synthetic */ BrazilPaymentActivity A00;
    public final /* synthetic */ String A01;

    public AnonymousClass6AI(BrazilPaymentActivity brazilPaymentActivity, String str) {
        this.A00 = brazilPaymentActivity;
        this.A01 = str;
    }

    @Override // X.AnonymousClass6MO
    public void ANl(C30881Ze r3) {
        Log.i("PAY: BrazilPaymentActivity BrazilGetVerificationMethods - onCardVerified");
        ((AbstractActivityC121685jC) this.A00).A0P.A00().A03(new AbstractC451720l() { // from class: X.67d
            @Override // X.AbstractC451720l
            public final void AM7(List list) {
                BrazilPaymentActivity brazilPaymentActivity = AnonymousClass6AI.this.A00;
                brazilPaymentActivity.AaN();
                if (brazilPaymentActivity.A0T != null && list.size() > 0) {
                    brazilPaymentActivity.A0T.ATY((AbstractC28901Pl) C12980iv.A0o(list));
                }
            }
        }, r3);
    }

    @Override // X.AnonymousClass6MO
    public void AVP(C452120p r11, ArrayList arrayList) {
        int i;
        C14850m9 r6;
        AnonymousClass69D r4;
        BrazilPaymentActivity brazilPaymentActivity = this.A00;
        brazilPaymentActivity.AaN();
        if (r11 == null) {
            if (arrayList == null || arrayList.isEmpty()) {
                brazilPaymentActivity.A0N.A03(brazilPaymentActivity.A00, "error_code", (long) 0);
                r4 = brazilPaymentActivity.A08;
                r6 = ((ActivityC13810kN) brazilPaymentActivity).A0C;
                i = R.string.payment_card_cannot_verified_error;
                r4.A01(brazilPaymentActivity, r6, brazilPaymentActivity.A0B, 0, i).show();
            }
            JSONArray A02 = brazilPaymentActivity.A07.A02(arrayList);
            if (A02 != null && !C1309660r.A01(arrayList)) {
                String str = this.A01;
                String obj = A02.toString();
                C17070qD r0 = ((AbstractActivityC121685jC) brazilPaymentActivity).A0P;
                r0.A03();
                C30881Ze r1 = (C30881Ze) r0.A09.A08(str);
                if (r1 != null) {
                    Intent A00 = brazilPaymentActivity.A0S.A00(brazilPaymentActivity, r1, obj);
                    AbstractActivityC119645em.A0O(A00, "referral_screen", "verify_to_pay");
                    brazilPaymentActivity.A2E(A00, 1);
                    return;
                }
                return;
            }
        }
        Log.i(C12960it.A0W(0, "PAY: BrazilGetVerificationMethods Error: "));
        brazilPaymentActivity.A0N.A03(brazilPaymentActivity.A00, "error_code", (long) 0);
        r4 = brazilPaymentActivity.A08;
        r6 = ((ActivityC13810kN) brazilPaymentActivity).A0C;
        i = R.string.payment_verify_card_error;
        r4.A01(brazilPaymentActivity, r6, brazilPaymentActivity.A0B, 0, i).show();
    }
}
