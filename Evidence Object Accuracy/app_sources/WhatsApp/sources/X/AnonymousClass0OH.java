package X;

import android.content.Context;
import android.view.MenuItem;

/* renamed from: X.0OH  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass0OH {
    public AnonymousClass00O A00;
    public final Context A01;

    public AnonymousClass0OH(Context context) {
        this.A01 = context;
    }

    public final MenuItem A00(MenuItem menuItem) {
        if (!(menuItem instanceof AbstractMenuItemC017808h)) {
            return menuItem;
        }
        AbstractMenuItemC017808h r1 = (AbstractMenuItemC017808h) menuItem;
        AnonymousClass00O r0 = this.A00;
        if (r0 == null) {
            r0 = new AnonymousClass00O();
            this.A00 = r0;
        }
        MenuItem menuItem2 = (MenuItem) r0.get(menuItem);
        if (menuItem2 != null) {
            return menuItem2;
        }
        AnonymousClass0CJ r3 = new AnonymousClass0CJ(this.A01, r1);
        this.A00.put(r1, r3);
        return r3;
    }
}
