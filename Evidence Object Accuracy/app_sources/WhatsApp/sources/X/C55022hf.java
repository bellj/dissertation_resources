package X;

import android.view.View;
import android.widget.TextView;
import androidx.constraintlayout.widget.Group;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;

/* renamed from: X.2hf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C55022hf extends AnonymousClass03U {
    public final View A00;
    public final TextView A01;
    public final Group A02;
    public final Group A03;
    public final Group A04;
    public final TextEmojiLabel A05;
    public final AnonymousClass3CN A06;

    public C55022hf(View view, AnonymousClass3CN r6) {
        super(view);
        Group group = (Group) AnonymousClass028.A0D(view, R.id.web_beta_group);
        this.A03 = group;
        Group group2 = (Group) AnonymousClass028.A0D(view, R.id.web_update_group);
        this.A04 = group2;
        this.A00 = AnonymousClass028.A0D(view, R.id.header_separator);
        this.A01 = C12960it.A0I(view, R.id.web_beta_card_text);
        this.A05 = C12970iu.A0T(view, R.id.e2ee_description_text);
        this.A02 = (Group) AnonymousClass028.A0D(view, R.id.privacy_narrative_e2ee_group);
        this.A06 = r6;
        C12960it.A0y(AnonymousClass028.A0D(view, R.id.link_device_button), r6, 24);
        C12960it.A0y(group, r6, 26);
        C12960it.A0y(group2, r6, 25);
    }
}
