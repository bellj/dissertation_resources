package X;

/* renamed from: X.3rZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C80013rZ extends AnonymousClass50X {
    public AbstractC80173rp A00;
    public boolean A01 = false;
    public final AbstractC80173rp A02;

    public C80013rZ(AbstractC80173rp r3) {
        this.A02 = r3;
        this.A00 = (AbstractC80173rp) r3.A06(4);
    }

    public static void A00(C80013rZ r1) {
        if (r1.A01) {
            r1.A02();
            r1.A01 = false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0022, code lost:
        if (r1 != false) goto L_0x0024;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ X.AbstractC117135Yq A01() {
        /*
            r4 = this;
            X.5Yq r3 = r4.AhL()
            X.3rp r3 = (X.AbstractC80173rp) r3
            r0 = 1
            java.lang.Object r0 = r3.A06(r0)
            java.lang.Number r0 = (java.lang.Number) r0
            byte r1 = r0.byteValue()
            r0 = 1
            if (r1 == r0) goto L_0x0024
            if (r1 == 0) goto L_0x0025
            X.5XU r0 = X.C72463ee.A0C(r3)
            boolean r1 = r0.AhJ(r3)
            r0 = 2
            r3.A06(r0)
            if (r1 == 0) goto L_0x0025
        L_0x0024:
            return r3
        L_0x0025:
            X.5H0 r0 = new X.5H0
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C80013rZ.A01():X.5Yq");
    }

    public void A02() {
        AbstractC80173rp r2 = (AbstractC80173rp) this.A00.A06(4);
        C72463ee.A0C(r2).AhA(r2, this.A00);
        this.A00 = r2;
    }

    @Override // X.AbstractC117145Yr
    public /* synthetic */ AbstractC117135Yq AhL() {
        boolean z = this.A01;
        AbstractC80173rp r1 = this.A00;
        if (z) {
            return r1;
        }
        C72463ee.A0C(r1).AhF(r1);
        this.A01 = true;
        return this.A00;
    }

    @Override // X.AbstractC115675Sm
    public final /* synthetic */ AbstractC117135Yq AhS() {
        return this.A02;
    }

    @Override // X.AnonymousClass50X, java.lang.Object
    public /* synthetic */ Object clone() {
        C80013rZ A06 = AnonymousClass50T.A06(this.A02);
        A00(A06);
        AbstractC80173rp r1 = A06.A00;
        C72463ee.A0C(r1).AhA(r1, (AbstractC80173rp) AhL());
        return A06;
    }
}
