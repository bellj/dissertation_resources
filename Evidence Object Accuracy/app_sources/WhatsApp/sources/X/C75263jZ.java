package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.3jZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75263jZ extends AnonymousClass03U {
    public final TextView A00;

    public C75263jZ(View view) {
        super(view);
        this.A00 = C12960it.A0J(view, R.id.media_section);
    }
}
