package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;

/* renamed from: X.0xc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21560xc extends AbstractC21570xd {
    public C21560xc(C232010t r1) {
        super(r1);
    }

    public Integer A00(String str) {
        String A02 = A02(str);
        Integer num = null;
        if (A02 == null) {
            return null;
        }
        try {
            num = Integer.valueOf(Integer.parseInt(A02));
            return num;
        } catch (NumberFormatException e) {
            StringBuilder sb = new StringBuilder("key-value-store/getIntProp/Invalid int value: ");
            sb.append(A02);
            Log.e(sb.toString(), e);
            return num;
        }
    }

    public Long A01(String str) {
        String A02 = A02(str);
        Long l = null;
        if (A02 == null) {
            return null;
        }
        try {
            l = Long.valueOf(Long.parseLong(A02));
            return l;
        } catch (NumberFormatException e) {
            StringBuilder sb = new StringBuilder("key-value-store/getLongProp/Invalid long value: ");
            sb.append(A02);
            Log.e(sb.toString(), e);
            return l;
        }
    }

    public String A02(String str) {
        C16310on A01 = this.A00.get();
        try {
            Cursor A03 = AbstractC21570xd.A03(A01, "wa_props", "prop_name=?", null, "CONTACT_PROPS", new String[]{"prop_value"}, new String[]{str});
            if (A03 == null || !A03.moveToFirst()) {
                if (A03 != null) {
                    A03.close();
                }
                A01.close();
                return null;
            }
            String string = A03.getString(0);
            A03.close();
            A01.close();
            return string;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public List A03(String str) {
        String A02 = A02(str);
        if (A02 == null) {
            return null;
        }
        String[] split = A02.split(",");
        ArrayList arrayList = new ArrayList();
        for (String str2 : split) {
            try {
                arrayList.add(Long.valueOf(Long.parseLong(str2)));
            } catch (NumberFormatException e) {
                StringBuilder sb = new StringBuilder("key-value-store/getLongListProp/Invalid long value: ");
                sb.append(str2);
                Log.e(sb.toString(), e);
                return null;
            }
        }
        return arrayList;
    }

    public synchronized Set A04(String str) {
        HashSet hashSet;
        String A02 = A02(str);
        hashSet = new HashSet();
        if (A02 != null) {
            try {
                JSONArray jSONArray = new JSONArray(A02);
                for (int i = 0; i < jSONArray.length(); i++) {
                    hashSet.add(jSONArray.getString(i));
                }
            } catch (JSONException e) {
                throw new IllegalStateException("key-value-store/getStringSetProp:", e);
            }
        }
        return hashSet;
    }

    public final void A05(String str, String str2) {
        try {
            C16310on A02 = this.A00.A02();
            if (TextUtils.isEmpty(str2)) {
                AbstractC21570xd.A02(A02, "wa_props", "prop_name=?", new String[]{str});
            } else {
                ContentValues contentValues = new ContentValues(2);
                contentValues.put("prop_name", str);
                contentValues.put("prop_value", str2);
                AbstractC21570xd.A04(contentValues, A02, "wa_props");
            }
            A02.close();
        } catch (IllegalArgumentException e) {
            StringBuilder sb = new StringBuilder("key-value-store/unable to set prop:");
            sb.append(str);
            AnonymousClass009.A08(sb.toString(), e);
        }
    }

    public synchronized void A06(String str, Set set) {
        JSONArray jSONArray = new JSONArray();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            jSONArray.put((String) it.next());
        }
        A05(str, jSONArray.toString());
    }
}
