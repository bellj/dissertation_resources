package X;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.util.Log;
import java.util.NoSuchElementException;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.3IW  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3IW {
    public static final Object A01 = C12970iu.A0l();
    public static volatile AnonymousClass3IW A02;
    public ConcurrentHashMap A00 = new ConcurrentHashMap();

    public static AnonymousClass3IW A00() {
        if (A02 == null) {
            synchronized (A01) {
                if (A02 == null) {
                    A02 = new AnonymousClass3IW();
                }
            }
        }
        AnonymousClass3IW r0 = A02;
        C13020j0.A01(r0);
        return r0;
    }

    public void A01(Context context, ServiceConnection serviceConnection) {
        if (!(serviceConnection instanceof AbstractC115075Qc)) {
            ConcurrentHashMap concurrentHashMap = this.A00;
            if (concurrentHashMap.containsKey(serviceConnection)) {
                try {
                    try {
                        context.unbindService((ServiceConnection) concurrentHashMap.get(serviceConnection));
                    } catch (IllegalArgumentException | IllegalStateException | NoSuchElementException unused) {
                    }
                    return;
                } finally {
                    concurrentHashMap.remove(serviceConnection);
                }
            }
        }
        try {
            context.unbindService(serviceConnection);
        } catch (IllegalArgumentException | IllegalStateException | NoSuchElementException unused2) {
        }
    }

    public final boolean A02(Context context, Intent intent, ServiceConnection serviceConnection, String str, int i) {
        ComponentName component = intent.getComponent();
        if (component != null) {
            try {
                if ((C15080mX.A00(context).A00.getPackageManager().getApplicationInfo(component.getPackageName(), 0).flags & 2097152) != 0) {
                    Log.w("ConnectionTracker", "Attempted to bind to a service in a STOPPED package.");
                    return false;
                }
            } catch (PackageManager.NameNotFoundException unused) {
            }
        }
        if (serviceConnection instanceof AbstractC115075Qc) {
            return context.bindService(intent, serviceConnection, i);
        }
        ConcurrentHashMap concurrentHashMap = this.A00;
        Object putIfAbsent = concurrentHashMap.putIfAbsent(serviceConnection, serviceConnection);
        if (!(putIfAbsent == null || serviceConnection == putIfAbsent)) {
            Object[] objArr = new Object[3];
            C12990iw.A1P(serviceConnection, str, objArr);
            objArr[2] = intent.getAction();
            Log.w("ConnectionTracker", String.format("Duplicate binding with the same ServiceConnection: %s, %s, %s.", objArr));
        }
        try {
            boolean bindService = context.bindService(intent, serviceConnection, i);
            if (!bindService) {
                return false;
            }
            return bindService;
        } finally {
            concurrentHashMap.remove(serviceConnection, serviceConnection);
        }
    }
}
