package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4jj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C99084jj implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        String str = null;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            str = C95664e9.A09(parcel, str, (char) readInt, 2, readInt);
        }
        C95664e9.A0C(parcel, A01);
        return new C77983oB(str);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new C77983oB[i];
    }
}
