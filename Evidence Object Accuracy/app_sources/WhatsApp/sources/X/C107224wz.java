package X;

import android.util.SparseArray;

/* renamed from: X.4wz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C107224wz implements AnonymousClass5VY {
    public final C95054d0 A00 = new C95054d0(new byte[4], 4);
    public final /* synthetic */ C106734wC A01;

    @Override // X.AnonymousClass5VY
    public void AIe(AbstractC14070ko r1, C92824Xo r2, AnonymousClass4YB r3) {
    }

    public C107224wz(C106734wC r4) {
        this.A01 = r4;
    }

    @Override // X.AnonymousClass5VY
    public void A7a(C95304dT r10) {
        if (r10.A0C() == 0 && (r10.A0C() & 128) != 0) {
            r10.A0T(6);
            int A00 = C95304dT.A00(r10) / 4;
            for (int i = 0; i < A00; i++) {
                C95054d0 r2 = this.A00;
                r10.A0V(r2.A03, 0, 4);
                r2.A07(0);
                int A04 = r2.A04(16);
                r2.A08(3);
                if (A04 == 0) {
                    r2.A08(13);
                } else {
                    int A042 = r2.A04(13);
                    C106734wC r3 = this.A01;
                    SparseArray sparseArray = r3.A0B;
                    if (sparseArray.get(A042) == null) {
                        sparseArray.put(A042, new C107264x3(new C107244x1(r3, A042)));
                        r3.A02++;
                    }
                }
            }
            C106734wC r22 = this.A01;
            if (r22.A09 != 2) {
                r22.A0B.remove(0);
            }
        }
    }
}
