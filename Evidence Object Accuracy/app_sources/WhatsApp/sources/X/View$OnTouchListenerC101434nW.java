package X;

import android.view.MotionEvent;
import android.view.View;
import com.whatsapp.qrcode.QrScannerViewV2;

/* renamed from: X.4nW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class View$OnTouchListenerC101434nW implements View.OnTouchListener {
    public final /* synthetic */ AnonymousClass0MU A00;
    public final /* synthetic */ QrScannerViewV2 A01;

    public View$OnTouchListenerC101434nW(AnonymousClass0MU r1, QrScannerViewV2 qrScannerViewV2) {
        this.A01 = qrScannerViewV2;
        this.A00 = r1;
    }

    @Override // android.view.View.OnTouchListener
    public boolean onTouch(View view, MotionEvent motionEvent) {
        this.A00.A00.AXZ(motionEvent);
        return true;
    }
}
