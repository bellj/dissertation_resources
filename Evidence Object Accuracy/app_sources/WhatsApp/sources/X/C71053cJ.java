package X;

/* renamed from: X.3cJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C71053cJ implements AbstractC115485Rs {
    public final AnonymousClass1V8 A00;
    public final AnonymousClass3HF A01;
    public final String A02;

    public C71053cJ(AbstractC15710nm r13, AnonymousClass1V8 r14) {
        AnonymousClass1V8.A01(r14, "payout");
        AnonymousClass3JT.A04(null, r14, String.class, C12970iu.A0j(), C12970iu.A0k(), "prepaid-card", new String[]{"type"}, false);
        this.A02 = (String) AnonymousClass3JT.A04(null, r14, String.class, 4L, 4L, null, new String[]{"last4"}, false);
        this.A01 = (AnonymousClass3HF) AnonymousClass3JT.A02(r13, r14, 33);
        this.A00 = r14;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C71053cJ.class != obj.getClass()) {
                return false;
            }
            C71053cJ r5 = (C71053cJ) obj;
            if (!this.A02.equals(r5.A02) || !this.A01.equals(r5.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] A1a = C12980iv.A1a();
        A1a[0] = this.A02;
        return C12960it.A06(this.A01, A1a);
    }
}
