package X;

/* renamed from: X.30T  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass30T extends AbstractC16110oT {
    public Integer A00;
    public Long A01;
    public Long A02;

    public AnonymousClass30T() {
        super(2794, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
        r3.Abe(2, this.A01);
        r3.Abe(3, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamStickerSendFromContextualSuggestion {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "stickerPickerTab", C12960it.A0Y(this.A00));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "stickerPosition", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "suggestedStickersCount", this.A02);
        return C12960it.A0d("}", A0k);
    }
}
