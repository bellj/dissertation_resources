package X;

import android.text.StaticLayout;
import android.text.TextDirectionHeuristic;
import android.text.TextDirectionHeuristics;
import android.widget.TextView;

/* renamed from: X.08M  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass08M extends AnonymousClass08N {
    @Override // X.AnonymousClass08N
    public void A00(StaticLayout.Builder builder, TextView textView) {
        builder.setTextDirection((TextDirectionHeuristic) AnonymousClass08C.A00(textView, TextDirectionHeuristics.FIRSTSTRONG_LTR, "getTextDirectionHeuristic"));
    }
}
