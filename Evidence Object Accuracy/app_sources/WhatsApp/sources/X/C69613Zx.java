package X;

import android.text.TextUtils;
import com.whatsapp.R;
import java.util.Collections;
import java.util.Map;

/* renamed from: X.3Zx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69613Zx implements AnonymousClass5WG {
    public final /* synthetic */ C42531vM A00;

    public C69613Zx(C42531vM r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5WG
    public void AQL(Map map) {
    }

    @Override // X.AnonymousClass5WG
    public void AQM(AnonymousClass3M3 r11) {
        String str = r11.A04;
        if (!TextUtils.isEmpty(str) || !TextUtils.isEmpty(r11.A02) || !TextUtils.isEmpty(r11.A03)) {
            C42531vM r1 = this.A00;
            r1.A09.A0P(null, r11.A00, str, r11.A02, r1.A02.getString(R.string.welcome_to_my_shop), Collections.singletonList(r1.A0T), r11.A00());
        }
    }
}
