package X;

/* renamed from: X.0kI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC13760kI extends ActivityC13770kJ {
    public boolean A00 = false;

    public AbstractActivityC13760kI() {
        A0R(new C103004q3(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            AbstractActivityC13750kH r1 = (AbstractActivityC13750kH) this;
            AnonymousClass2FL r3 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r2 = r3.A1E;
            ((ActivityC13830kP) r1).A05 = (AbstractC14440lR) r2.ANe.get();
            ((ActivityC13810kN) r1).A0C = (C14850m9) r2.A04.get();
            ((ActivityC13810kN) r1).A05 = (C14900mE) r2.A8X.get();
            ((ActivityC13810kN) r1).A03 = (AbstractC15710nm) r2.A4o.get();
            ((ActivityC13810kN) r1).A04 = (C14330lG) r2.A7B.get();
            ((ActivityC13810kN) r1).A0B = (AnonymousClass19M) r2.A6R.get();
            ((ActivityC13810kN) r1).A0A = (C18470sV) r2.AK8.get();
            ((ActivityC13810kN) r1).A06 = (C15450nH) r2.AII.get();
            ((ActivityC13810kN) r1).A08 = (AnonymousClass01d) r2.ALI.get();
            ((ActivityC13810kN) r1).A0D = (C18810t5) r2.AMu.get();
            ((ActivityC13810kN) r1).A09 = (C14820m6) r2.AN3.get();
            ((ActivityC13810kN) r1).A07 = (C18640sm) r2.A3u.get();
            ((ActivityC13790kL) r1).A05 = (C14830m7) r2.ALb.get();
            ((ActivityC13790kL) r1).A0D = (C252718t) r2.A9K.get();
            ((ActivityC13790kL) r1).A01 = (C15570nT) r2.AAr.get();
            ((ActivityC13790kL) r1).A04 = (C15810nw) r2.A73.get();
            ((ActivityC13790kL) r1).A09 = r3.A06();
            ((ActivityC13790kL) r1).A06 = (C14950mJ) r2.AKf.get();
            ((ActivityC13790kL) r1).A00 = (AnonymousClass12P) r2.A0H.get();
            ((ActivityC13790kL) r1).A02 = (C252818u) r2.AMy.get();
            ((ActivityC13790kL) r1).A03 = (C22670zS) r2.A0V.get();
            ((ActivityC13790kL) r1).A0A = (C21840y4) r2.ACr.get();
            ((ActivityC13790kL) r1).A07 = (C15880o3) r2.ACF.get();
            ((ActivityC13790kL) r1).A0C = (C21820y2) r2.AHx.get();
            ((ActivityC13790kL) r1).A0B = (C15510nN) r2.AHZ.get();
            ((ActivityC13790kL) r1).A08 = (C249317l) r2.A8B.get();
            r1.A0K = (C16590pI) r2.AMg.get();
            r1.A0Z = (AnonymousClass13H) r2.ABY.get();
            r1.A0j = (C253018w) r2.AJS.get();
            r1.A0o = (C253118x) r2.AAW.get();
            r1.A0U = (C16120oU) r2.ANE.get();
            r1.A0a = (C245115u) r2.A7s.get();
            r1.A05 = (C18850tA) r2.AKx.get();
            r1.A03 = (C16170oZ) r2.AM4.get();
            r1.A0p = (C26501Ds) r2.AML.get();
            r1.A0B = (C21270x9) r2.A4A.get();
            r1.A07 = (C15550nR) r2.A45.get();
            r1.A0Q = (C20040v7) r2.AAK.get();
            r1.A0m = (C252018m) r2.A7g.get();
            r1.A09 = (C15610nY) r2.AMe.get();
            r1.A0d = (C17070qD) r2.AFC.get();
            r1.A0f = (C239913u) r2.AC1.get();
            r1.A0O = (C253218y) r2.AAF.get();
            r1.A0M = (C15650ng) r2.A4m.get();
            r1.A0R = (AnonymousClass12H) r2.AC5.get();
            r1.A08 = (AnonymousClass190) r2.AIZ.get();
            r1.A0i = (AnonymousClass12F) r2.AJM.get();
            r1.A0P = (C20000v3) r2.AAG.get();
            r1.A06 = (AnonymousClass116) r2.A3z.get();
            r1.A0T = (AnonymousClass193) r2.A6S.get();
            r1.A0S = (C242114q) r2.AJq.get();
            r1.A0L = (C15890o4) r2.AN1.get();
            r1.A0V = (C20710wC) r2.A8m.get();
            r1.A0n = (AnonymousClass198) r2.A0J.get();
            r1.A0h = (C240514a) r2.AJL.get();
            r1.A0W = (AnonymousClass11G) r2.AKq.get();
            r1.A0A = (AnonymousClass10T) r2.A47.get();
            r1.A0Y = (C22370yy) r2.AB0.get();
            r1.A0c = (C22710zW) r2.AF7.get();
            r1.A04 = (C14650lo) r2.A2V.get();
            r1.A0l = (AnonymousClass1AB) r2.AKI.get();
            r1.A0X = (AnonymousClass109) r2.AI8.get();
            r1.A0N = (C15600nX) r2.A8x.get();
            r1.A02 = (C16210od) r2.A0Y.get();
            r1.A0H = (AnonymousClass19D) r2.ABo.get();
            r1.A0I = (AnonymousClass11P) r2.ABp.get();
            r1.A0E = (AnonymousClass19I) r2.A4a.get();
            r1.A0b = (AnonymousClass19J) r2.ACA.get();
            r1.A0C = (AnonymousClass19K) r2.AFh.get();
            r1.A0q = (AnonymousClass19L) r2.A6l.get();
        }
    }
}
