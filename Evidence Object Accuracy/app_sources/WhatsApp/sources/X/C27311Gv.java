package X;

/* renamed from: X.1Gv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C27311Gv extends AbstractC250017s {
    public final C14830m7 A00;
    public final C19990v2 A01;
    public final C15860o1 A02;

    public C27311Gv(C14830m7 r1, C19990v2 r2, C233511i r3, C15860o1 r4) {
        super(r3);
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r4;
    }

    public final void A08(C34611gQ r7) {
        if (r7.A02) {
            long j = r7.A00;
            if (j != -1) {
                j = this.A00.A02(j);
            }
            this.A02.A0T(r7.A01, j, false);
            return;
        }
        C15860o1 r2 = this.A02;
        AbstractC14640lm r1 = r7.A01;
        if (r2.A0S(r1)) {
            r2.A0R(r1);
        }
    }
}
