package X;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.CancellationException;

/* renamed from: X.5L5  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5L5 extends AbstractC11140fo implements AbstractC11610gZ {
    public final Handler A00;
    public final AnonymousClass5L5 A01;
    public final boolean A02;
    public volatile AnonymousClass5L5 _immediate;

    public /* synthetic */ AnonymousClass5L5(Handler handler) {
        this(handler, false);
    }

    public AnonymousClass5L5(Handler handler, boolean z) {
        AnonymousClass5L5 r0 = null;
        this.A00 = handler;
        this.A02 = z;
        this._immediate = z ? this : r0;
        AnonymousClass5L5 r1 = this._immediate;
        if (r1 == null) {
            r1 = new AnonymousClass5L5(handler, true);
            this._immediate = r1;
        }
        this.A01 = r1;
    }

    @Override // X.AbstractC10990fX
    public boolean A03(AnonymousClass5X4 r3) {
        return !this.A02 || !C16700pc.A0O(Looper.myLooper(), this.A00.getLooper());
    }

    @Override // X.AbstractC10990fX
    public void A04(Runnable runnable, AnonymousClass5X4 r4) {
        if (!this.A00.post(runnable)) {
            StringBuilder A0k = C12960it.A0k("The task was rejected, the handler underlying the dispatcher '");
            A0k.append(this);
            AnonymousClass0LZ.A00(new CancellationException(C12960it.A0d("' was closed", A0k)), r4);
            AnonymousClass4ZY.A01.A04(runnable, r4);
        }
    }

    @Override // X.AbstractC114155Kk
    public /* bridge */ /* synthetic */ AbstractC114155Kk A05() {
        return this.A01;
    }

    public boolean equals(Object obj) {
        return (obj instanceof AnonymousClass5L5) && ((AnonymousClass5L5) obj).A00 == this.A00;
    }

    public int hashCode() {
        return System.identityHashCode(this.A00);
    }

    @Override // X.AbstractC10990fX, java.lang.Object
    public String toString() {
        String str;
        AbstractC114155Kk r0;
        AbstractC114155Kk r02 = C88584Gf.A00;
        if (this == r02) {
            str = "Dispatchers.Main";
        } else {
            str = null;
            try {
                r0 = r02.A05();
            } catch (UnsupportedOperationException unused) {
                r0 = null;
            }
            if (this == r0) {
                str = "Dispatchers.Main.immediate";
            }
        }
        if (str != null) {
            return str;
        }
        String obj = this.A00.toString();
        return this.A02 ? C16700pc.A08(obj, ".immediate") : obj;
    }
}
