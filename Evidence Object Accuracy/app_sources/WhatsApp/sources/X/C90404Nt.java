package X;

import android.content.Context;
import com.whatsapp.mediacomposer.bottombar.filterswipe.FilterSwipeView;

/* renamed from: X.4Nt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C90404Nt {
    public final Context A00;
    public final FilterSwipeView A01;

    public C90404Nt(FilterSwipeView filterSwipeView) {
        this.A01 = filterSwipeView;
        this.A00 = filterSwipeView.getContext();
    }
}
