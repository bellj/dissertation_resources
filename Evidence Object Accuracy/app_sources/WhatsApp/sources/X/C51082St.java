package X;

import android.content.Context;

/* renamed from: X.2St  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C51082St {
    public static boolean A00(Context context) {
        AnonymousClass01M.A00(context, AnonymousClass01J.class);
        AbstractC17940re of = AbstractC17940re.of();
        boolean z = false;
        if (of.size() <= 1) {
            z = true;
        }
        AnonymousClass2PX.A00("Cannot bind the flag @DisableFragmentGetContextFix more than once.", new Object[0], z);
        if (of.isEmpty()) {
            return true;
        }
        return ((Boolean) of.iterator().next()).booleanValue();
    }
}
