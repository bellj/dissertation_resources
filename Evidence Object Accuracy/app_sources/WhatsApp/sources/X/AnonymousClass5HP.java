package X;

/* renamed from: X.5HP  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5HP extends Throwable {
    public AnonymousClass5HP() {
        super("Failure occurred while trying to finish a future.");
    }

    @Override // java.lang.Throwable
    public synchronized Throwable fillInStackTrace() {
        return this;
    }
}
