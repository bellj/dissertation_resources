package X;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Pair;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.location.LocationSharingService;
import com.whatsapp.protocol.VoipStanzaChildNode;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.JNIUtils;
import com.whatsapp.voipcalling.Voip;
import java.security.SecureRandom;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.1Eh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26651Eh implements AbstractC15920o8 {
    public SecureRandom A00;
    public final C22490zA A01;
    public final AbstractC15710nm A02;
    public final C14900mE A03;
    public final C22930zs A04;
    public final C15570nT A05;
    public final C237913a A06;
    public final C239613r A07;
    public final C15450nH A08;
    public final C18790t3 A09;
    public final C16170oZ A0A;
    public final C20670w8 A0B;
    public final C238013b A0C;
    public final AnonymousClass12T A0D;
    public final C18720su A0E;
    public final C20970wc A0F;
    public final C243915i A0G;
    public final AnonymousClass13R A0H;
    public final C15550nR A0I;
    public final C22700zV A0J;
    public final AnonymousClass10T A0K;
    public final C253318z A0L;
    public final C250417w A0M;
    public final C18640sm A0N;
    public final C14830m7 A0O;
    public final C16590pI A0P;
    public final C17170qN A0Q;
    public final C14820m6 A0R;
    public final AnonymousClass018 A0S;
    public final C18750sx A0T;
    public final AnonymousClass15L A0U;
    public final C20650w6 A0V;
    public final C19990v2 A0W;
    public final C20830wO A0X;
    public final C21320xE A0Y;
    public final C15680nj A0Z;
    public final C241714m A0a;
    public final C15650ng A0b;
    public final C238113c A0c;
    public final C15240mn A0d;
    public final AnonymousClass13M A0e;
    public final C15600nX A0f;
    public final C20040v7 A0g;
    public final AnonymousClass10Y A0h;
    public final AnonymousClass12H A0i;
    public final C22810zg A0j;
    public final C21250x7 A0k;
    public final C242114q A0l;
    public final AnonymousClass10U A0m;
    public final C18470sV A0n;
    public final C22290yq A0o;
    public final AnonymousClass150 A0p;
    public final C231510o A0q;
    public final C14850m9 A0r;
    public final C20710wC A0s;
    public final C244915s A0t;
    public final C16030oK A0u;
    public final AnonymousClass13O A0v;
    public final C14300lD A0w;
    public final AnonymousClass109 A0x;
    public final AnonymousClass148 A0y;
    public final C22310ys A0z;
    public final C17220qS A10;
    public final C22170ye A11;
    public final C20740wF A12;
    public final C20660w7 A13;
    public final C22410z2 A14;
    public final C22420z3 A15;
    public final C244715q A16;
    public final C244515o A17;
    public final C22230yk A18;
    public final C17230qT A19;
    public final C20220vP A1A;
    public final AnonymousClass13L A1B;
    public final C19390u2 A1C;
    public final C22280yp A1D;
    public final C21810y1 A1E;
    public final C20750wG A1F;
    public final AnonymousClass10Z A1G;
    public final C22140ya A1H;
    public final C20680w9 A1I;
    public final C18910tG A1J;
    public final C15860o1 A1K;
    public final AnonymousClass12F A1L;
    public final AnonymousClass1E9 A1M;
    public final C22210yi A1N;
    public final C235512c A1O;
    public final C235412b A1P;
    public final AnonymousClass12O A1Q;
    public final AnonymousClass12N A1R;
    public final AbstractC14440lR A1S;
    public final JNIUtils A1T;
    public final C21280xA A1U;
    public final C14890mD A1V;
    public final C14860mA A1W;
    public final Map A1X = new ConcurrentHashMap();

    public C26651Eh(C22490zA r2, AbstractC15710nm r3, C14900mE r4, C22930zs r5, C15570nT r6, C237913a r7, C239613r r8, C15450nH r9, C18790t3 r10, C16170oZ r11, C20670w8 r12, C238013b r13, AnonymousClass12T r14, C18720su r15, C20970wc r16, C243915i r17, AnonymousClass13R r18, C15550nR r19, C22700zV r20, AnonymousClass10T r21, C253318z r22, C250417w r23, C18640sm r24, C14830m7 r25, C16590pI r26, C17170qN r27, C14820m6 r28, AnonymousClass018 r29, C18750sx r30, AnonymousClass15L r31, C20650w6 r32, C19990v2 r33, C20830wO r34, C21320xE r35, C15680nj r36, C241714m r37, C15650ng r38, C238113c r39, C15240mn r40, AnonymousClass13M r41, C15600nX r42, C20040v7 r43, AnonymousClass10Y r44, AnonymousClass12H r45, C22810zg r46, C21250x7 r47, C242114q r48, AnonymousClass10U r49, C18470sV r50, C22290yq r51, AnonymousClass150 r52, C231510o r53, C14850m9 r54, C20710wC r55, C244915s r56, C16030oK r57, AnonymousClass13O r58, C14300lD r59, AnonymousClass109 r60, AnonymousClass148 r61, C22310ys r62, C17220qS r63, C22170ye r64, C20740wF r65, C20660w7 r66, C22410z2 r67, C22420z3 r68, C244715q r69, C244515o r70, C22230yk r71, C17230qT r72, C20220vP r73, AnonymousClass13L r74, C19390u2 r75, C22280yp r76, C21810y1 r77, C20750wG r78, AnonymousClass10Z r79, C22140ya r80, C20680w9 r81, C18910tG r82, C15860o1 r83, AnonymousClass12F r84, AnonymousClass1E9 r85, C22210yi r86, C235512c r87, C235412b r88, AnonymousClass12O r89, AnonymousClass12N r90, AbstractC14440lR r91, JNIUtils jNIUtils, C21280xA r93, C14890mD r94, C14860mA r95) {
        this.A0P = r26;
        this.A0O = r25;
        this.A0E = r15;
        this.A0r = r54;
        this.A03 = r4;
        this.A1T = jNIUtils;
        this.A06 = r7;
        this.A02 = r3;
        this.A05 = r6;
        this.A07 = r8;
        this.A1S = r91;
        this.A09 = r10;
        this.A0W = r33;
        this.A1V = r94;
        this.A1W = r95;
        this.A0V = r32;
        this.A13 = r66;
        this.A08 = r9;
        this.A0k = r47;
        this.A0n = r50;
        this.A0A = r11;
        this.A0q = r53;
        this.A11 = r64;
        this.A0B = r12;
        this.A0g = r43;
        this.A10 = r63;
        this.A1M = r85;
        this.A1U = r93;
        this.A0I = r19;
        this.A0a = r37;
        this.A0j = r46;
        this.A18 = r71;
        this.A0S = r29;
        this.A1N = r86;
        this.A0t = r56;
        this.A0d = r40;
        this.A0w = r59;
        this.A1D = r76;
        this.A1I = r81;
        this.A1J = r82;
        this.A1P = r88;
        this.A0o = r51;
        this.A16 = r69;
        this.A0C = r13;
        this.A0G = r17;
        this.A0b = r38;
        this.A0L = r22;
        this.A0c = r39;
        this.A0s = r55;
        this.A0h = r44;
        this.A0i = r45;
        this.A1C = r75;
        this.A1L = r84;
        this.A0T = r30;
        this.A1K = r83;
        this.A04 = r5;
        this.A0z = r62;
        this.A1O = r87;
        this.A1Q = r89;
        this.A17 = r70;
        this.A0K = r21;
        this.A12 = r65;
        this.A0D = r14;
        this.A0m = r49;
        this.A19 = r72;
        this.A1A = r73;
        this.A1E = r77;
        this.A1F = r78;
        this.A1G = r79;
        this.A0l = r48;
        this.A0J = r20;
        this.A0F = r16;
        this.A0R = r28;
        this.A0Z = r36;
        this.A1H = r80;
        this.A0Y = r35;
        this.A14 = r67;
        this.A15 = r68;
        this.A0u = r57;
        this.A0M = r23;
        this.A0p = r52;
        this.A0x = r60;
        this.A0y = r61;
        this.A0U = r31;
        this.A0f = r42;
        this.A1B = r74;
        this.A0N = r24;
        this.A0e = r41;
        this.A0X = r34;
        this.A1R = r90;
        this.A0v = r58;
        this.A0H = r18;
        this.A0Q = r27;
        this.A01 = r2;
    }

    public synchronized int A00(Jid jid, VoipStanzaChildNode voipStanzaChildNode, String str, int i) {
        String str2;
        boolean z;
        int i2;
        int nativeRegisterJNIUtils = Voip.nativeRegisterJNIUtils(this.A1T);
        if (nativeRegisterJNIUtils == 0) {
            z = true;
        } else if (nativeRegisterJNIUtils == 70015) {
            z = false;
        } else {
            str2 = "app/xmpp/recv/native_handle_web_message failed to register jni";
            Log.i(str2);
            return 409;
        }
        int nativeHandleWebClientMessage = Voip.nativeHandleWebClientMessage(str, jid.getRawString(), i, voipStanzaChildNode);
        if (z) {
            Voip.nativeUnregisterJNIUtils();
        }
        if (nativeHandleWebClientMessage == 0 || nativeHandleWebClientMessage == 70020) {
            i2 = 200;
        } else if (nativeHandleWebClientMessage == 670001) {
            i2 = 423;
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("app/xmpp/recv/native_handle_web_message failed with code:");
            sb.append(nativeHandleWebClientMessage);
            str2 = sb.toString();
            Log.i(str2);
            return 409;
        }
        return i2;
    }

    public final int A01(Jid jid, String str, int i) {
        AnonymousClass2O6 r2;
        boolean z = true;
        if (!(i == 1 || i == 2 || i == 6 || i == 7)) {
            z = false;
        }
        AnonymousClass009.A0A("invalid web client action in handleAcceptAndReject", z);
        AnonymousClass2KU r6 = Voip.A01;
        Pair A00 = r6.A00(str);
        if (A00 != null && (r2 = (AnonymousClass2O6) A00.second) != null) {
            AnonymousClass2O7 r3 = r2.A01;
            Jid jid2 = ((AnonymousClass2MM) r3).A00;
            AnonymousClass009.A05(jid2);
            int A002 = A00(((DeviceJid) jid2).getUserJid(), ((AnonymousClass2MM) r3).A01, str, i);
            if (!TextUtils.equals(Voip.getCurrentCallId(), str) && (i == 2 || i == 7)) {
                C18750sx r5 = this.A0T;
                String A0B = AnonymousClass1SF.A0B(str);
                Jid jid3 = ((AnonymousClass2MM) r3).A00;
                AnonymousClass009.A05(jid3);
                AnonymousClass1YT A04 = r5.A04(new AnonymousClass1YU(r2.A00, ((DeviceJid) jid3).getUserJid(), A0B, false));
                if (A04 != null) {
                    A04.A06(2);
                    r5.A08(A04);
                }
            }
            r6.A01(str);
            return A002;
        } else if (i != 6 && i != 7) {
            return A00(jid, null, str, i);
        } else {
            StringBuilder sb = new StringBuilder("app/xmpp/recv/call_web_query no cached offer in accept/reject pending, callId:");
            sb.append(str);
            sb.append(", type:");
            sb.append(i);
            Log.w(sb.toString());
            return 410;
        }
    }

    public final AnonymousClass1JX A02(AbstractC14640lm r5, int i) {
        int i2;
        String str;
        AnonymousClass1JW r2 = new AnonymousClass1JW(this.A02, this.A08);
        ((AnonymousClass1JX) r2).A00 = i;
        C19990v2 r3 = this.A0W;
        r2.A0A = r3.A05(r5);
        AnonymousClass1PE r0 = (AnonymousClass1PE) r3.A0B().get(r5);
        if (r0 == null) {
            i2 = 0;
        } else {
            i2 = r0.A0A;
        }
        r2.A04 = i2;
        r2.A0C = r5;
        r2.A01 = r3.A00(r5);
        C15370n3 A01 = this.A0X.A01(r5);
        if (TextUtils.isEmpty(A01.A0K)) {
            str = null;
        } else {
            str = A01.A0K;
        }
        r2.A0I = str;
        r2.A09 = this.A1K.A02(r5);
        return r2;
    }

    public final void A03(AbstractC14640lm r10, String str, int i) {
        long j = (long) i;
        if (!this.A16.A02(r10, str, j, false)) {
            Context context = this.A0P.A00;
            AnonymousClass1Tv.A00(context, new Intent(context, LocationSharingService.class).setAction("com.whatsapp.ShareLocationService.ACTION_SEND_LOCATION_WEB_RESPONSE").putExtra("id", str).putExtra("chatJid", r10.getRawString()).putExtra("duration", j));
        }
    }

    public final void A04(Jid jid, String str, int i, long j) {
        C14860mA r1 = this.A1W;
        r1.A0H(str, i);
        this.A18.A0F(str, i);
        this.A11.A02(jid, str, j);
        r1.A08();
        r1.A09();
    }

    public final void A05(Jid jid, String str, long j) {
        C14860mA r2 = this.A1W;
        r2.A0H(str, 200);
        this.A18.A0F(str, 200);
        this.A11.A02(jid, str, j);
        r2.A08();
        r2.A09();
    }

    public final boolean A06(AnonymousClass02N r3, Jid jid, String str, long j) {
        if (!r3.A04()) {
            return false;
        }
        StringBuilder sb = new StringBuilder("app/xmpp/recv/webquery/canceled: ");
        sb.append(str);
        Log.e(sb.toString());
        this.A11.A02(jid, str, j);
        this.A1W.A0H(str, 499);
        this.A18.A0F(str, 499);
        this.A1X.remove(str);
        return true;
    }

    @Override // X.AbstractC15920o8
    public int[] ADF() {
        return new int[]{28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 230, 231, 45, 89, 118, 119, 146, 147, 162, 171, 173, 207, 220, 232};
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:283:0x0663, code lost:
        if (r30.A0l.A03(r7, false) != false) goto L_0x0754;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:285:0x066e, code lost:
        if (r30.A0l.A02(r7, null) == false) goto L_0x0526;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:309:0x06e2, code lost:
        if (r5 != 0) goto L_0x06fb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:411:0x091c, code lost:
        if (r1 != false) goto L_0x0a5a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:419:0x093b, code lost:
        if (r2.A02() != false) goto L_0x095a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:455:0x0a56, code lost:
        if (r7.A05().containsKey(r12) != false) goto L_0x0a58;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:493:0x0b6f, code lost:
        if (r6 != 0) goto L_0x0b16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:494:0x0b71, code lost:
        r30.A0M.A01(r11, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:901:0x17c2, code lost:
        if (r5.equals(r30.A1V.A00().A03) == false) goto L_0x17c4;
     */
    /* JADX WARNING: Removed duplicated region for block: B:388:0x08a0  */
    /* JADX WARNING: Removed duplicated region for block: B:680:0x10c4  */
    /* JADX WARNING: Removed duplicated region for block: B:683:0x10ce  */
    /* JADX WARNING: Removed duplicated region for block: B:686:0x10d7  */
    /* JADX WARNING: Removed duplicated region for block: B:804:0x14c9  */
    /* JADX WARNING: Removed duplicated region for block: B:805:0x14d4  */
    @Override // X.AbstractC15920o8
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean AI8(android.os.Message r31, int r32) {
        /*
        // Method dump skipped, instructions count: 6332
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C26651Eh.AI8(android.os.Message, int):boolean");
    }
}
