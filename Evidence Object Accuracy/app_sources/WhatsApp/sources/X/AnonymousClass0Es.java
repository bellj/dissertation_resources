package X;

/* renamed from: X.0Es  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Es extends AnonymousClass0Q0 {
    public final /* synthetic */ RunnableC10040dv A00;

    public AnonymousClass0Es(RunnableC10040dv r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass0Q0
    public int A00() {
        return this.A00.A02.size();
    }

    @Override // X.AnonymousClass0Q0
    public int A01() {
        return this.A00.A03.size();
    }

    @Override // X.AnonymousClass0Q0
    public Object A02(int i, int i2) {
        RunnableC10040dv r2 = this.A00;
        Object obj = r2.A03.get(i);
        Object obj2 = r2.A02.get(i2);
        if (obj != null && obj2 != null) {
            return null;
        }
        throw new AssertionError();
    }

    @Override // X.AnonymousClass0Q0
    public boolean A03(int i, int i2) {
        RunnableC10040dv r3 = this.A00;
        Object obj = r3.A03.get(i);
        Object obj2 = r3.A02.get(i2);
        if (obj != null) {
            if (obj2 != null) {
                return r3.A01.A03.A00.A00(obj, obj2);
            }
        } else if (obj2 == null) {
            return true;
        }
        throw new AssertionError();
    }

    @Override // X.AnonymousClass0Q0
    public boolean A04(int i, int i2) {
        RunnableC10040dv r3 = this.A00;
        Object obj = r3.A03.get(i);
        Object obj2 = r3.A02.get(i2);
        if (obj == null) {
            return obj2 == null;
        }
        if (obj2 != null) {
            return r3.A01.A03.A00.A01(obj, obj2);
        }
        return false;
    }
}
