package X;

import java.security.Signature;

/* renamed from: X.66n  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1323466n implements AnonymousClass21K {
    public final /* synthetic */ long A00;
    public final /* synthetic */ AnonymousClass607 A01;
    public final /* synthetic */ C1323666p A02;
    public final /* synthetic */ C128545wH A03;
    public final /* synthetic */ String A04;

    @Override // X.AnonymousClass21K
    public /* synthetic */ void AMg(Signature signature) {
    }

    public C1323466n(AnonymousClass607 r1, C1323666p r2, C128545wH r3, String str, long j) {
        this.A01 = r1;
        this.A02 = r2;
        this.A04 = str;
        this.A03 = r3;
        this.A00 = j;
    }

    @Override // X.AnonymousClass21K
    public void AMb(int i, CharSequence charSequence) {
        this.A01.A0I.A05(C12960it.A0d(charSequence.toString(), C12960it.A0k("authenticateBiometric/onAuthenticationError/error: ")));
        this.A02.AMb(i, charSequence);
    }

    @Override // X.AnonymousClass21K
    public void AMc() {
        this.A01.A0I.A05("authenticateBiometric/onAuthenticationFailed");
        this.A02.AMc();
    }

    @Override // X.AnonymousClass21K
    public void AMe(int i, CharSequence charSequence) {
        this.A01.A0I.A05(C12960it.A0d(charSequence.toString(), C12960it.A0k("authenticateBiometric/onAuthenticationHelp/help: ")));
        this.A02.AMe(i, charSequence);
    }

    @Override // X.AnonymousClass21K
    public void AMf(byte[] bArr) {
        if (bArr != null) {
            AnonymousClass607 r4 = this.A01;
            r4.A0I.A06("authenticateBiometric/onAuthenticationSucceeded/success");
            C1323666p r3 = this.A02;
            r3.AMf(bArr);
            AnonymousClass607.A00(r4, r3, this.A03.A02(C130775zx.A00(Boolean.FALSE, bArr, "AUTH", null, null, new Object[0], this.A00)), this.A04);
            return;
        }
        this.A01.A0I.A05("authenticateBiometric/onAuthenticationSucceeded/null signature");
        this.A02.AMc();
    }
}
