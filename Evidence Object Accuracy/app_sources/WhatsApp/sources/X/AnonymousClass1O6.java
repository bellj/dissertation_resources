package X;

import android.util.JsonReader;
import com.facebook.redex.RunnableBRunnable0Shape6S0100000_I0_6;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.Executor;

/* renamed from: X.1O6  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1O6 {
    public File A00;
    public Executor A01;
    public boolean A02;
    public final AbstractC15710nm A03;
    public final C16590pI A04;
    public final AnonymousClass1O9 A05;
    public final AbstractC14440lR A06;
    public final String A07 = "gif/gif_cache_mem_store";
    public final String A08;

    public AnonymousClass1O6(AbstractC15710nm r3, C16590pI r4, AbstractC14440lR r5, String str, int i) {
        this.A04 = r4;
        this.A03 = r3;
        this.A06 = r5;
        this.A08 = str;
        AnonymousClass1O9 r1 = new AnonymousClass1O9(i);
        this.A05 = r1;
        C89464Kb r0 = new C89464Kb(this);
        synchronized (r1) {
            r1.A00 = r0;
        }
    }

    public C39391pp A00(String str) {
        A03();
        AnonymousClass1O9 r3 = this.A05;
        C39391pp r2 = (C39391pp) r3.A04(str);
        if (r2 != null) {
            String str2 = r2.A00;
            if (!new File(str2).exists()) {
                r3.A07(str);
                A02().execute(new RunnableBRunnable0Shape6S0100000_I0_6(this, 35));
                return null;
            } else if (r2.A02 == null && !AnonymousClass01I.A01()) {
                r2.A02 = C26521Du.A03(C26521Du.A01(new File(str2)));
            }
        }
        return r2;
    }

    public final File A01() {
        String str;
        File file = this.A00;
        if (file != null && file.exists()) {
            return this.A00;
        }
        File externalCacheDir = this.A04.A00.getExternalCacheDir();
        if (externalCacheDir == null || !externalCacheDir.exists()) {
            str = "diskbackedgifcache/getmappingfile/external cache dir doesn't exit";
        } else {
            File file2 = new File(externalCacheDir, this.A07);
            if (file2.exists() || file2.mkdirs()) {
                File file3 = new File(file2, this.A08);
                this.A00 = file3;
                return file3;
            }
            str = "diskbackedgifcache/getmappingfile/disk cache dir doesn't exit";
        }
        Log.e(str);
        return null;
    }

    public synchronized Executor A02() {
        Executor executor;
        executor = this.A01;
        if (executor == null) {
            executor = new ExecutorC27271Gr(this.A06, false);
            this.A01 = executor;
        }
        return executor;
    }

    public final synchronized void A03() {
        if (!AnonymousClass01I.A01() && !this.A02) {
            A02();
            File A01 = A01();
            if (A01 != null && A01.exists()) {
                try {
                    ArrayList arrayList = new ArrayList();
                    JsonReader jsonReader = new JsonReader(new FileReader(A01));
                    try {
                        jsonReader.beginObject();
                        while (jsonReader.hasNext()) {
                            if ("mappings".equals(jsonReader.nextName())) {
                                jsonReader.beginArray();
                                while (jsonReader.hasNext()) {
                                    jsonReader.beginObject();
                                    String str = null;
                                    String str2 = null;
                                    while (jsonReader.hasNext()) {
                                        String nextName = jsonReader.nextName();
                                        int hashCode = nextName.hashCode();
                                        if (hashCode != 116079) {
                                            if (hashCode == 3143036 && nextName.equals("file")) {
                                                str = jsonReader.nextString();
                                            }
                                        } else if (nextName.equals("url")) {
                                            str2 = jsonReader.nextString();
                                        }
                                    }
                                    jsonReader.endObject();
                                    if (str == null) {
                                        throw new IOException("field not found: file");
                                    } else if (str2 != null) {
                                        arrayList.add(new C39391pp(str, str2, null));
                                    } else {
                                        throw new IOException("field not found: url");
                                    }
                                }
                                jsonReader.endArray();
                            }
                        }
                        jsonReader.endObject();
                        jsonReader.close();
                        Iterator it = arrayList.iterator();
                        while (it.hasNext()) {
                            C39391pp r2 = (C39391pp) it.next();
                            if (new File(r2.A00).exists()) {
                                this.A05.A08(r2.A01, r2);
                            }
                        }
                        arrayList.size();
                        A01.getAbsolutePath();
                    } catch (Throwable th) {
                        try {
                            jsonReader.close();
                        } catch (Throwable unused) {
                        }
                        throw th;
                    }
                } catch (IOException e) {
                    Log.e("diskbackedgifcache/init/error", e);
                    this.A03.AaV("disk-backed-gif-cache/load-error", e.toString(), false);
                }
            }
            this.A02 = true;
        }
    }
}
