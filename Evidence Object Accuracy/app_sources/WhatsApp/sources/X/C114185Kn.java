package X;

/* renamed from: X.5Kn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C114185Kn extends AbstractC10990fX {
    public static final C114185Kn A00 = new C114185Kn();

    @Override // X.AbstractC10990fX
    public boolean A03(AnonymousClass5X4 r2) {
        return false;
    }

    @Override // X.AbstractC10990fX, java.lang.Object
    public String toString() {
        return "Dispatchers.Unconfined";
    }

    @Override // X.AbstractC10990fX
    public AbstractC10990fX A02(int i) {
        throw C12980iv.A0u("limitedParallelism is not supported for Dispatchers.Unconfined");
    }

    @Override // X.AbstractC10990fX
    public void A04(Runnable runnable, AnonymousClass5X4 r3) {
        if (r3.get(C113595Ie.A00) == null) {
            throw C12980iv.A0u("Dispatchers.Unconfined.dispatch function can only be used by the yield function. If you wrap Unconfined dispatcher in your code, make sure you properly delegate isDispatchNeeded and dispatch calls.");
        }
    }
}
