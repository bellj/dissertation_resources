package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.util.Log;
import java.math.BigDecimal;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.20C  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass20C implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C99844kx();
    public final int A00;
    public final AbstractC30791Yv A01;
    public final C30821Yy A02;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass20C(AbstractC30791Yv r6, int i, long j) {
        boolean z = true;
        AnonymousClass009.A0B("value must be a number greater or equal to zero", j >= 0);
        AnonymousClass009.A0B("offset must be a number greater or equal to zero", i < 0 ? false : z);
        this.A00 = i;
        this.A01 = r6;
        this.A02 = new C30821Yy(new BigDecimal(((double) j) / ((double) i)), ((AbstractC30781Yu) r6).A01);
    }

    public AnonymousClass20C(AbstractC30791Yv r2, long j) {
        this(r2, 1, j);
    }

    public AnonymousClass20C(Parcel parcel) {
        this.A02 = (C30821Yy) parcel.readParcelable(C30821Yy.class.getClassLoader());
        this.A00 = parcel.readInt();
        this.A01 = AnonymousClass102.A00(parcel);
    }

    public static AnonymousClass20C A00(JSONObject jSONObject) {
        long optLong = jSONObject.optLong("value", -1);
        int optInt = jSONObject.optInt("offset", -1);
        AbstractC30791Yv A01 = AnonymousClass102.A01(jSONObject.optJSONObject("currency"), jSONObject.optInt("currencyType", -1));
        if (optInt <= 0) {
            return new AnonymousClass20C(A01, optLong);
        }
        return new AnonymousClass20C(A01, optInt, optLong);
    }

    public int A01() {
        return (int) (this.A02.A00.doubleValue() * ((double) this.A00));
    }

    public JSONObject A02() {
        JSONObject jSONObject = new JSONObject();
        try {
            double doubleValue = this.A02.A00.doubleValue();
            int i = this.A00;
            jSONObject.put("value", (int) (doubleValue * ((double) i)));
            jSONObject.put("offset", i);
            AbstractC30791Yv r2 = this.A01;
            jSONObject.put("currencyType", ((AbstractC30781Yu) r2).A00);
            jSONObject.put("currency", r2.Aew());
            return jSONObject;
        } catch (JSONException e) {
            Log.w("PAY: PaymentMoney toJson threw: ", e);
            return jSONObject;
        }
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass20C r5 = (AnonymousClass20C) obj;
            if (this.A00 != r5.A00 || !this.A01.equals(r5.A01) || !this.A02.equals(r5.A02)) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return (31 * this.A02.hashCode()) + this.A00 + this.A01.hashCode();
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("PaymentMoney{amount=");
        sb.append(this.A02);
        sb.append(", offset=");
        sb.append(this.A00);
        sb.append(", currency=");
        sb.append(((AbstractC30781Yu) this.A01).A04);
        sb.append('}');
        return sb.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.A02, 1);
        parcel.writeInt(this.A00);
        this.A01.writeToParcel(parcel, i);
    }
}
