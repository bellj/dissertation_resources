package X;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.PopupWindow;

/* renamed from: X.0Bv  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Bv extends PopupWindow {
    public static final boolean A01;
    public boolean A00;

    static {
        boolean z = false;
        if (Build.VERSION.SDK_INT < 21) {
            z = true;
        }
        A01 = z;
    }

    public AnonymousClass0Bv(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        C013406h A00 = C013406h.A00(context, attributeSet, AnonymousClass07O.A0H, i, i2);
        TypedArray typedArray = A00.A02;
        if (typedArray.hasValue(2)) {
            boolean z = typedArray.getBoolean(2, false);
            if (A01) {
                this.A00 = z;
            } else {
                AnonymousClass0TW.A02(this, z);
            }
        }
        setBackgroundDrawable(A00.A02(0));
        A00.A04();
    }

    @Override // android.widget.PopupWindow
    public void showAsDropDown(View view, int i, int i2) {
        if (A01 && this.A00) {
            i2 -= view.getHeight();
        }
        super.showAsDropDown(view, i, i2);
    }

    @Override // android.widget.PopupWindow
    public void showAsDropDown(View view, int i, int i2, int i3) {
        if (A01 && this.A00) {
            i2 -= view.getHeight();
        }
        super.showAsDropDown(view, i, i2, i3);
    }

    @Override // android.widget.PopupWindow
    public void update(View view, int i, int i2, int i3, int i4) {
        if (A01 && this.A00) {
            i2 -= view.getHeight();
        }
        super.update(view, i, i2, i3, i4);
    }
}
