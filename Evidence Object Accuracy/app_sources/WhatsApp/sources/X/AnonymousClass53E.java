package X;

import java.util.concurrent.atomic.AtomicLong;

/* renamed from: X.53E  reason: invalid class name */
/* loaded from: classes3.dex */
public final /* synthetic */ class AnonymousClass53E implements AnonymousClass5TI {
    public final /* synthetic */ AtomicLong A00;

    public /* synthetic */ AnonymousClass53E(AtomicLong atomicLong) {
        this.A00 = atomicLong;
    }

    @Override // X.AnonymousClass5TI
    public final void AOs(long j) {
        this.A00.addAndGet(j);
    }
}
