package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.mediaview.MediaViewFragment;
import com.whatsapp.mediaview.PhotoView;
import java.util.ArrayList;

/* renamed from: X.2YJ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2YJ extends AnimatorListenerAdapter {
    public final /* synthetic */ MediaViewFragment A00;
    public final /* synthetic */ AbstractC16130oV A01;

    public AnonymousClass2YJ(MediaViewFragment mediaViewFragment, AbstractC16130oV r2) {
        this.A00 = mediaViewFragment;
        this.A01 = r2;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        Bitmap photo;
        MediaViewFragment mediaViewFragment = this.A00;
        AbstractC16130oV r6 = this.A01;
        Uri fromFile = Uri.fromFile(AbstractC15340mz.A00(r6).A0F);
        ArrayList A0l = C12960it.A0l();
        A0l.add(fromFile);
        AnonymousClass24W r5 = new AnonymousClass24W(mediaViewFragment.A01());
        r5.A0C = A0l;
        r5.A08 = C15380n4.A03(mediaViewFragment.A10);
        r5.A01 = 29;
        r5.A0G = true;
        byte b = r6.A0y;
        if ((b == 1 || b == 42) && AbstractC454421p.A00) {
            PhotoView A19 = mediaViewFragment.A19(r6.A0z);
            if (!(A19 == null || (photo = A19.getPhoto()) == null)) {
                AnonymousClass1O4 A02 = mediaViewFragment.A0P.A02();
                StringBuilder A0h = C12960it.A0h();
                A0h.append(fromFile);
                A02.A03(C12960it.A0d("-media_view", A0h), photo);
                r5.A05 = fromFile;
            }
            ArrayList A0l2 = C12960it.A0l();
            C12970iu.A1T(mediaViewFragment.A05().findViewById(R.id.pager_container), fromFile.toString(), A0l2);
            View findViewById = mediaViewFragment.A05().findViewById(R.id.media_preview_header);
            C12970iu.A1T(findViewById, AnonymousClass028.A0J(findViewById), A0l2);
            View findViewById2 = mediaViewFragment.A05().findViewById(R.id.media_preview_footer);
            C12970iu.A1T(findViewById2, AnonymousClass028.A0J(findViewById2), A0l2);
            View findViewById3 = mediaViewFragment.A05().findViewById(R.id.media_preview_send);
            C12970iu.A1T(findViewById3, AnonymousClass028.A0J(findViewById3), A0l2);
            mediaViewFragment.A0P(r5.A00(), 5, C018108l.A02(mediaViewFragment.A0C(), (AnonymousClass01T[]) A0l2.toArray(new AnonymousClass01T[0])).A03());
            return;
        }
        mediaViewFragment.startActivityForResult(r5.A00(), 5);
        mediaViewFragment.A0C().overridePendingTransition(17432576, 0);
    }
}
