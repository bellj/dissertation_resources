package X;

/* renamed from: X.1XD  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1XD extends C28861Ph implements AbstractC16400ox, AbstractC16410oy, AbstractC16420oz {
    public String A00;

    public AnonymousClass1XD(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 49, j);
    }

    public AnonymousClass1XD(AnonymousClass1IS r8, AnonymousClass1XD r9, long j) {
        super(r8, r9, j, true);
        this.A00 = r9.A00;
    }

    public AnonymousClass1XD(AnonymousClass1IS r2, String str, String str2, long j) {
        super(r2, (byte) 49, j);
        A0l(AbstractC32741cf.A04(str));
        this.A00 = str2;
    }
}
