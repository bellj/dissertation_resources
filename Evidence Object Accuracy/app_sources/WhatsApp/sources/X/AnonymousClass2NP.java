package X;

import com.whatsapp.voipcalling.camera.VoipPhysicalCamera;

/* renamed from: X.2NP  reason: invalid class name */
/* loaded from: classes2.dex */
public interface AnonymousClass2NP {
    void AOD(VoipPhysicalCamera voipPhysicalCamera);

    void AQ0(VoipPhysicalCamera voipPhysicalCamera);

    void AVt(VoipPhysicalCamera voipPhysicalCamera);

    void AXz(VoipPhysicalCamera voipPhysicalCamera);
}
