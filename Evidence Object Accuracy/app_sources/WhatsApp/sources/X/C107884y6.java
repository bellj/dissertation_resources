package X;

@Deprecated
/* renamed from: X.4y6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107884y6 implements AbstractC47452At {
    public final int A00;
    public final int A01;
    public final AnonymousClass4MA A02;
    public final String A03;

    public C107884y6() {
        this(null);
    }

    public C107884y6(String str) {
        this.A02 = new AnonymousClass4MA();
        this.A03 = str;
        this.A00 = 8000;
        this.A01 = 8000;
    }

    @Override // X.AbstractC47452At
    public /* bridge */ /* synthetic */ AnonymousClass2BW A8D() {
        return new C56132kO(this.A02, this.A03, this.A00, this.A01);
    }
}
