package X;

import com.whatsapp.catalogcategory.view.fragment.CatalogCategoryExpandableGroupsListFragment;
import com.whatsapp.catalogcategory.view.viewmodel.CatalogCategoryGroupsViewModel;

/* renamed from: X.5Ja  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C113795Ja extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ CatalogCategoryExpandableGroupsListFragment this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C113795Ja(CatalogCategoryExpandableGroupsListFragment catalogCategoryExpandableGroupsListFragment) {
        super(0);
        this.this$0 = catalogCategoryExpandableGroupsListFragment;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        return new AnonymousClass02A(this.this$0).A00(CatalogCategoryGroupsViewModel.class);
    }
}
