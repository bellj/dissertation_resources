package X;

import java.util.AbstractList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;

/* renamed from: X.5Hz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C113545Hz extends AbstractList<String> implements AnonymousClass5Z3, RandomAccess {
    public final AnonymousClass5Z3 A00;

    public C113545Hz(AnonymousClass5Z3 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5Z3
    public final Object AG5(int i) {
        return this.A00.AG5(i);
    }

    @Override // X.AnonymousClass5Z3
    public final List AhD() {
        return this.A00.AhD();
    }

    @Override // X.AnonymousClass5Z3
    public final AnonymousClass5Z3 AhE() {
        return this;
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object get(int i) {
        return this.A00.get(i);
    }

    @Override // java.util.AbstractList, java.util.AbstractCollection, java.util.List, java.util.Collection, java.lang.Iterable
    public final Iterator iterator() {
        return new AnonymousClass5D7(this);
    }

    @Override // java.util.AbstractList, java.util.List
    public final ListIterator listIterator(int i) {
        return new AnonymousClass5DP(this, i);
    }

    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection
    public final int size() {
        return this.A00.size();
    }
}
