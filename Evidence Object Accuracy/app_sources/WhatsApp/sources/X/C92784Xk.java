package X;

import android.os.SystemClock;

/* renamed from: X.4Xk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92784Xk {
    public long A00;
    public long A01;
    public boolean A02;
    public final long A03;

    public C92784Xk(long j) {
        this.A03 = j;
    }

    public long A00() {
        long j = this.A00;
        return this.A02 ? j + (SystemClock.elapsedRealtime() - this.A01) : j;
    }

    public void A01() {
        if (!this.A02) {
            this.A01 = SystemClock.elapsedRealtime();
        }
        this.A02 = true;
    }

    public void A02() {
        if (this.A02) {
            this.A00 += SystemClock.elapsedRealtime() - this.A01;
        }
        this.A02 = false;
    }
}
