package X;

import android.view.ViewGroup;
import com.whatsapp.payments.ui.widget.PaymentMethodRow;

/* renamed from: X.5qC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public interface AbstractC124835qC {
    void A6F(ViewGroup viewGroup);

    String ABX(AbstractC28901Pl v, int i);

    String ACJ(AbstractC28901Pl v);

    String ACK(AbstractC28901Pl v);

    String ACf(AbstractC28901Pl v, int i);

    String AEO(AbstractC28901Pl v);

    void AMM(ViewGroup viewGroup);

    void AMN(ViewGroup viewGroup);

    void AMP(ViewGroup viewGroup);

    void AQh(ViewGroup viewGroup, AbstractC28901Pl v);

    boolean AdO(AbstractC28901Pl v, int i);

    boolean AdU(AbstractC28901Pl v);

    boolean AdV();

    void Adj(AbstractC28901Pl v, PaymentMethodRow paymentMethodRow);

    boolean Adt();
}
