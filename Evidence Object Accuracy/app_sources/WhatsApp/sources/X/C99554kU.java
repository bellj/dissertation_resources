package X;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4kU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C99554kU implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        Bundle bundle = null;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            if (((char) readInt) != 2) {
                C95664e9.A0D(parcel, readInt);
            } else {
                bundle = C95664e9.A05(parcel, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C56432ks(bundle);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new C56432ks[i];
    }
}
