package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.03B  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass03B extends AnonymousClass03C {
    public static final int A00(CharSequence charSequence) {
        C16700pc.A0E(charSequence, 0);
        return charSequence.length() - 1;
    }

    public static /* synthetic */ int A01(CharSequence charSequence, char c, int i, int i2) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        if (charSequence instanceof String) {
            return ((String) charSequence).indexOf(c, i);
        }
        char[] cArr = {c};
        if (i < 0) {
            i = 0;
        }
        int A00 = A00(charSequence);
        if (i > A00) {
            return -1;
        }
        do {
            int i3 = i + 1;
            if (cArr[0] == charSequence.charAt(i)) {
                return i;
            }
            i = i3;
        } while (i != A00);
        return -1;
    }

    public static final int A02(CharSequence charSequence, CharSequence charSequence2, int i, int i2) {
        int i3;
        if (i < 0) {
            i = 0;
        }
        int length = charSequence.length();
        if (i2 > length) {
            i2 = length;
        }
        C114075Kc r1 = new C114075Kc(i, i2);
        if ((charSequence instanceof String) && (charSequence2 instanceof String)) {
            i3 = r1.A00();
            int A01 = r1.A01();
            int A02 = r1.A02();
            if (A02 > 0 && i3 <= A01) {
                while (true) {
                    int i4 = i3 + A02;
                    if (AnonymousClass03C.A0M((String) charSequence2, (String) charSequence, i3, charSequence2.length(), false)) {
                        break;
                    } else if (i3 == A01) {
                        return -1;
                    } else {
                        i3 = i4;
                    }
                }
            } else {
                return -1;
            }
        } else {
            i3 = r1.A00();
            int A012 = r1.A01();
            int A022 = r1.A02();
            if (A022 > 0 && i3 <= A012) {
                while (true) {
                    int i5 = i3 + A022;
                    if (A0H(charSequence2, charSequence, i3, charSequence2.length(), false)) {
                        break;
                    } else if (i3 == A012) {
                        return -1;
                    } else {
                        i3 = i5;
                    }
                }
            } else {
                return -1;
            }
        }
        return i3;
    }

    public static final int A03(CharSequence charSequence, String str, int i) {
        C16700pc.A0E(charSequence, 0);
        C16700pc.A0E(str, 1);
        if (!(charSequence instanceof String)) {
            return A02(charSequence, str, i, charSequence.length());
        }
        return ((String) charSequence).indexOf(str, i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001d, code lost:
        if (java.lang.Character.isSpaceChar(r1) != false) goto L_0x001f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.lang.CharSequence A04(java.lang.CharSequence r6) {
        /*
            int r5 = r6.length()
            r4 = 1
            int r5 = r5 - r4
            r3 = 0
            r2 = 0
        L_0x0008:
            if (r3 > r5) goto L_0x002e
            r0 = r5
            if (r2 != 0) goto L_0x000e
            r0 = r3
        L_0x000e:
            char r1 = r6.charAt(r0)
            boolean r0 = java.lang.Character.isWhitespace(r1)
            if (r0 != 0) goto L_0x001f
            boolean r1 = java.lang.Character.isSpaceChar(r1)
            r0 = 0
            if (r1 == 0) goto L_0x0020
        L_0x001f:
            r0 = 1
        L_0x0020:
            if (r2 != 0) goto L_0x0029
            if (r0 != 0) goto L_0x0026
            r2 = 1
            goto L_0x0008
        L_0x0026:
            int r3 = r3 + 1
            goto L_0x0008
        L_0x0029:
            if (r0 == 0) goto L_0x002e
            int r5 = r5 + -1
            goto L_0x0008
        L_0x002e:
            int r5 = r5 + r4
            java.lang.CharSequence r0 = r6.subSequence(r3, r5)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass03B.A04(java.lang.CharSequence):java.lang.CharSequence");
    }

    public static final String A05(CharSequence charSequence, C114075Kc r3) {
        C16700pc.A0E(charSequence, 0);
        C16700pc.A0E(r3, 1);
        return charSequence.subSequence(r3.A04().intValue(), r3.A03().intValue() + 1).toString();
    }

    public static final String A06(String str, String str2) {
        C16700pc.A0E(str, 0);
        C16700pc.A0E(str2, 2);
        int lastIndexOf = str.lastIndexOf(46, A00(str));
        if (lastIndexOf == -1) {
            return str2;
        }
        String substring = str.substring(lastIndexOf + 1, str.length());
        C16700pc.A0B(substring);
        return substring;
    }

    public static final String A07(String str, String str2, String str3) {
        C16700pc.A0E(str, 0);
        C16700pc.A0E(str2, 1);
        C16700pc.A0E(str3, 2);
        int A03 = A03(str, str2, 0);
        if (A03 == -1) {
            return str3;
        }
        String substring = str.substring(A03 + str2.length(), str.length());
        C16700pc.A0B(substring);
        return substring;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0013, code lost:
        if (r9 > 10) goto L_0x0015;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.util.List A08(java.lang.CharSequence r7, java.lang.String r8, int r9) {
        /*
            r0 = 0
            r2 = 0
            int r6 = A03(r7, r8, r0)
            r5 = -1
            if (r6 == r5) goto L_0x004d
            r0 = 1
            if (r9 == r0) goto L_0x004d
            r4 = 0
            if (r9 <= 0) goto L_0x0015
            r4 = 1
            r1 = r9
            r0 = 10
            if (r9 <= r0) goto L_0x0017
        L_0x0015:
            r1 = 10
        L_0x0017:
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>(r1)
        L_0x001c:
            java.lang.CharSequence r0 = r7.subSequence(r2, r6)
            java.lang.String r0 = r0.toString()
            r3.add(r0)
            int r2 = r8.length()
            int r2 = r2 + r6
            if (r4 == 0) goto L_0x0046
            int r1 = r3.size()
            int r0 = r9 + -1
            if (r1 != r0) goto L_0x0046
        L_0x0036:
            int r0 = r7.length()
            java.lang.CharSequence r0 = r7.subSequence(r2, r0)
            java.lang.String r0 = r0.toString()
            r3.add(r0)
            return r3
        L_0x0046:
            int r6 = A03(r7, r8, r2)
            if (r6 != r5) goto L_0x001c
            goto L_0x0036
        L_0x004d:
            java.lang.String r0 = r7.toString()
            java.util.List r0 = X.C16780pk.A0K(r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass03B.A08(java.lang.CharSequence, java.lang.String, int):java.util.List");
    }

    public static final List A09(CharSequence charSequence, String[] strArr, int i) {
        C16700pc.A0E(charSequence, 0);
        if (strArr.length == 1) {
            String str = strArr[0];
            if (str.length() != 0) {
                return A08(charSequence, str, i);
            }
        }
        Iterable<C114075Kc> A01 = C11100fk.A01(A0E(charSequence, strArr, i));
        ArrayList arrayList = new ArrayList(C16760pi.A0D(A01));
        for (C114075Kc r0 : A01) {
            arrayList.add(A05(charSequence, r0));
        }
        return arrayList;
    }

    public static final C114075Kc A0A(CharSequence charSequence) {
        return new C114075Kc(0, charSequence.length() - 1);
    }

    public static final AnonymousClass1WO A0B(CharSequence charSequence) {
        return A0C(charSequence, new String[]{"\r\n", "\n", "\r"});
    }

    public static final AnonymousClass1WO A0C(CharSequence charSequence, String[] strArr) {
        return C11100fk.A04(new AnonymousClass5KI(charSequence), A0E(charSequence, strArr, 0));
    }

    public static final AnonymousClass1WO A0E(CharSequence charSequence, String[] strArr, int i) {
        return new C112845Ey(charSequence, new C72313eM(C10980fW.A04(strArr)), i);
    }

    public static final boolean A0G(CharSequence charSequence, CharSequence charSequence2) {
        int A02;
        C16700pc.A0E(charSequence2, 1);
        if (charSequence2 instanceof String) {
            A02 = A03(charSequence, (String) charSequence2, 0);
        } else {
            A02 = A02(charSequence, charSequence2, 0, charSequence.length());
        }
        if (A02 < 0) {
            return false;
        }
        return true;
    }

    public static final boolean A0H(CharSequence charSequence, CharSequence charSequence2, int i, int i2, boolean z) {
        char upperCase;
        char upperCase2;
        if (i >= 0 && 0 <= charSequence.length() - i2 && i <= charSequence2.length() - i2) {
            int i3 = 0;
            while (i3 < i2) {
                int i4 = i3 + 1;
                char charAt = charSequence.charAt(0 + i3);
                char charAt2 = charSequence2.charAt(i3 + i);
                if (charAt == charAt2 || (z && ((upperCase = Character.toUpperCase(charAt)) == (upperCase2 = Character.toUpperCase(charAt2)) || Character.toLowerCase(upperCase) == Character.toLowerCase(upperCase2)))) {
                    i3 = i4;
                }
            }
            return true;
        }
        return false;
    }
}
