package X;

/* renamed from: X.30q  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C615130q extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Integer A02;
    public Integer A03;
    public String A04;
    public String A05;

    public C615130q() {
        super(3052, AbstractC16110oT.A00(), 2, 0);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
        r3.Abe(7, this.A04);
        r3.Abe(3, this.A01);
        r3.Abe(5, this.A05);
        r3.Abe(4, this.A02);
        r3.Abe(2, this.A03);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamPsStructuredMessageInteraction {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "bizPlatform", C12960it.A0Y(this.A00));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "businessOwnerJid", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "messageClass", C12960it.A0Y(this.A01));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "messageClassAttributes", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "messageInteraction", C12960it.A0Y(this.A02));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "messageMediaType", C12960it.A0Y(this.A03));
        return C12960it.A0d("}", A0k);
    }
}
