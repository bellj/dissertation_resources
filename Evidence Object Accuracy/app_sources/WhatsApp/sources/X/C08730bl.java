package X;

import android.content.Context;

/* renamed from: X.0bl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08730bl implements AnonymousClass5WW {
    public final /* synthetic */ AnonymousClass0OS A00;

    public C08730bl(AnonymousClass0OS r1) {
        this.A00 = r1;
    }

    public void A00(Object obj) {
        if (obj != null) {
            this.A00.A00().setRenderTree(((AnonymousClass3IP) obj).A01());
        }
    }

    @Override // X.AnonymousClass5WW
    public /* bridge */ /* synthetic */ void A6O(Context context, Object obj, Object obj2, Object obj3) {
        A00(obj3);
    }

    @Override // X.AnonymousClass5WW
    public boolean Adc(Object obj, Object obj2, Object obj3, Object obj4) {
        return ((AnonymousClass28D) obj).A0F(40) != ((AnonymousClass28D) obj2).A0F(40);
    }

    @Override // X.AnonymousClass5WW
    public /* bridge */ /* synthetic */ void Af8(Context context, Object obj, Object obj2, Object obj3) {
    }
}
