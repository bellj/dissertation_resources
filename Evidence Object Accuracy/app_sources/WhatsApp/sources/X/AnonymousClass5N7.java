package X;

import java.io.OutputStream;

/* renamed from: X.5N7  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5N7 extends AnonymousClass1TP {
    public AnonymousClass5N7(OutputStream outputStream) {
        super(outputStream);
    }

    @Override // X.AnonymousClass1TP
    public AnonymousClass1TP A00() {
        return this;
    }

    @Override // X.AnonymousClass1TP
    public void A04(AnonymousClass1TL r2, boolean z) {
        r2.A07().A08(this, z);
    }
}
