package X;

import android.database.Cursor;
import com.whatsapp.util.Log;
import java.util.HashSet;

/* renamed from: X.1C5  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1C5 {
    public final C20130vG A00;
    public final C20520vt A01;

    public AnonymousClass1C5(C20130vG r1, C20520vt r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public int A00(AbstractC14640lm r11, long j) {
        String str;
        HashSet hashSet;
        String[] strArr;
        String str2;
        C20520vt r1 = this.A01;
        if (r1.A06()) {
            str = "SELECT message._id FROM message_quoted JOIN message_view AS message ON message_quoted.message_row_id = message._id WHERE message.sort_id > ? AND message.chat_row_id = ? AND message.message_type <> 15 AND message_quoted.from_me = 1 LIMIT 100";
        } else {
            str = "SELECT message._id FROM message_view AS message LEFT JOIN messages_quotes ON message.quoted_row_id = messages_quotes._id WHERE message._id > ? AND message.chat_row_id = ? AND message.quoted_row_id IS NOT NULL AND message.message_type <> 15 AND messages_quotes.key_from_me = 1 LIMIT 100";
        }
        HashSet hashSet2 = new HashSet();
        try {
            C16310on A01 = r1.A0C.get();
            Cursor A09 = A01.A03.A09(str, new String[]{String.valueOf(j), String.valueOf(r1.A03.A02(r11))});
            while (A09.moveToNext()) {
                try {
                    hashSet2.add(Long.valueOf(A09.getLong(0)));
                } catch (Throwable th) {
                    if (A09 != null) {
                        try {
                            A09.close();
                        } catch (Throwable unused) {
                        }
                    }
                    throw th;
                }
            }
            A09.close();
            A01.close();
        } catch (Exception e) {
            Log.e("QuotedMessageStore/getQuotedImportantMessagesNewerThanCount", e);
        }
        C20130vG r3 = this.A00;
        C15570nT r0 = r3.A00;
        r0.A08();
        C27631Ih r2 = r0.A05;
        if (r2 == null) {
            hashSet = new HashSet();
        } else {
            hashSet = new HashSet();
            if (r3.A02()) {
                strArr = new String[]{String.valueOf(j), String.valueOf(r3.A01.A02(r11)), String.valueOf(r3.A02.A01(r2))};
                str2 = "SELECT message._id FROM message_mentions JOIN message_view AS message ON message_mentions.message_row_id == message._id WHERE message.sort_id > ? AND message.chat_row_id = ? AND message_mentions.jid_row_id = ? AND message.message_type <> 15 LIMIT 100";
            } else {
                strArr = new String[]{String.valueOf(j), String.valueOf(r3.A01.A02(r11))};
                String rawString = r2.getRawString();
                StringBuilder sb = new StringBuilder("SELECT _id FROM message_view WHERE _id > ? AND chat_row_id = ? AND ");
                sb.append("mentioned_jids LIKE '%");
                sb.append(rawString);
                sb.append("%'");
                sb.append(" LIMIT 100");
                str2 = sb.toString();
            }
            try {
                C16310on A012 = r3.A03.get();
                Cursor A092 = A012.A03.A09(str2, strArr);
                while (A092.moveToNext()) {
                    hashSet.add(Long.valueOf(A092.getLong(0)));
                }
                A092.close();
                A012.close();
            } catch (Exception e2) {
                Log.e("MentionMessageStore/getMentionImportantMessagesNewerThanCount", e2);
            }
        }
        hashSet2.addAll(hashSet);
        return hashSet2.size();
    }
}
