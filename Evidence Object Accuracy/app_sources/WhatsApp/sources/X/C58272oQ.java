package X;

import android.content.Context;
import android.net.Uri;
import android.text.TextPaint;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import com.facebook.redex.RunnableBRunnable0Shape1S1300000_I1;

/* renamed from: X.2oQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58272oQ extends AbstractC52172aN {
    public int A00 = 4;
    public int A01 = 0;
    public AnonymousClass5TE A02;
    public Runnable A03;
    public boolean A04;
    public boolean A05;
    public final AnonymousClass12Q A06;
    public final C14900mE A07;
    public final AnonymousClass01d A08;
    public final String A09;

    public C58272oQ(Context context, AnonymousClass12Q r3, C14900mE r4, AnonymousClass01d r5, String str) {
        super(context);
        this.A07 = r4;
        this.A08 = r5;
        this.A06 = r3;
        this.A09 = str;
    }

    @Override // X.AbstractC52172aN, X.AbstractC116465Vn
    public void AXY(MotionEvent motionEvent, View view) {
        super.AXY(motionEvent, view);
        if (super.A04) {
            Uri parse = Uri.parse(this.A09);
            String scheme = parse.getScheme();
            if ("http".equals(scheme) || "https".equals(scheme) || "rtsp".equals(scheme) || "ftp".equals(scheme) || "tel".equals(scheme) || "wapay".equals(scheme) || "upi".equals(scheme)) {
                Runnable runnable = this.A03;
                if (runnable == null) {
                    runnable = new RunnableBRunnable0Shape1S1300000_I1(this, parse, view, scheme, 1);
                    this.A03 = runnable;
                }
                this.A07.A0J(runnable, (long) ViewConfiguration.getLongPressTimeout());
                return;
            }
            return;
        }
        Runnable runnable2 = this.A03;
        if (runnable2 != null) {
            this.A07.A0G(runnable2);
        }
    }

    @Override // X.AbstractC116465Vn
    public void onClick(View view) {
        int i = this.A00;
        if (i == 2 || i == 3) {
            this.A06.AbB(view.getContext(), Uri.parse(this.A09), this.A01, this.A00);
        } else {
            boolean z = this.A04;
            AnonymousClass12Q r3 = this.A06;
            Context context = view.getContext();
            Uri parse = Uri.parse(this.A09);
            if (z) {
                r3.AbA(context, parse, this.A01);
            } else {
                r3.Ab9(context, parse);
            }
        }
        AnonymousClass5TE r0 = this.A02;
        if (r0 != null) {
            r0.A7H();
        }
    }

    @Override // X.AbstractC52172aN, android.text.style.CharacterStyle
    public void updateDrawState(TextPaint textPaint) {
        super.updateDrawState(textPaint);
        textPaint.setUnderlineText(this.A05);
    }
}
