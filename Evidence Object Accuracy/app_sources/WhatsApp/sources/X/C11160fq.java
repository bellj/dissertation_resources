package X;

import org.xml.sax.SAXException;

/* renamed from: X.0fq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C11160fq extends SAXException {
    public C11160fq(String str) {
        super(str);
    }

    public C11160fq(String str, Exception exc) {
        super(str, exc);
    }
}
