package X;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import com.facebook.redex.RunnableBRunnable0Shape0S0110000_I0;
import com.facebook.redex.RunnableBRunnable0Shape1S0300000_I0_1;
import com.facebook.redex.RunnableBRunnable0Shape6S0200000_I0_6;
import com.whatsapp.util.Log;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.0vP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20220vP {
    public long A00 = (System.currentTimeMillis() - 200);
    public Handler A01;
    public RunnableC43021wH A02;
    public boolean A03;
    public final AbstractC15710nm A04;
    public final C19430u6 A05;
    public final C15570nT A06;
    public final C15450nH A07;
    public final C15550nR A08;
    public final C15610nY A09;
    public final AnonymousClass01d A0A;
    public final C14830m7 A0B;
    public final C16590pI A0C;
    public final C18360sK A0D;
    public final C14820m6 A0E;
    public final AnonymousClass018 A0F;
    public final C19990v2 A0G;
    public final C15650ng A0H;
    public final C18460sU A0I;
    public final AnonymousClass10Y A0J;
    public final C21400xM A0K;
    public final C15670ni A0L;
    public final C14850m9 A0M;
    public final C42981wD A0N = new C42981wD(this);
    public final C22630zO A0O;
    public final AnonymousClass11N A0P;
    public final C238313e A0Q;
    public final C15860o1 A0R;
    public final AbstractC14440lR A0S;
    public final C14860mA A0T;
    public final Map A0U = new HashMap();
    public volatile long A0V;

    public C20220vP(AbstractC15710nm r5, C19430u6 r6, C15570nT r7, C15450nH r8, C15550nR r9, C15610nY r10, AnonymousClass01d r11, C14830m7 r12, C16590pI r13, C18360sK r14, C14820m6 r15, AnonymousClass018 r16, C19990v2 r17, C15650ng r18, C18460sU r19, AnonymousClass10Y r20, C21400xM r21, C15670ni r22, C14850m9 r23, C22630zO r24, AnonymousClass11N r25, C238313e r26, C15860o1 r27, AbstractC14440lR r28, C14860mA r29) {
        this.A0C = r13;
        this.A0B = r12;
        this.A0M = r23;
        this.A0I = r19;
        this.A04 = r5;
        this.A06 = r7;
        this.A0S = r28;
        this.A0G = r17;
        this.A0T = r29;
        this.A07 = r8;
        this.A08 = r9;
        this.A0A = r11;
        this.A09 = r10;
        this.A0F = r16;
        this.A0H = r18;
        this.A0J = r20;
        this.A0R = r27;
        this.A0O = r24;
        this.A0Q = r26;
        this.A0K = r21;
        this.A0L = r22;
        this.A0E = r15;
        this.A0P = r25;
        this.A0D = r14;
        this.A05 = r6;
    }

    public static final void A00(List list) {
        if (list != null) {
            StringBuilder sb = new StringBuilder();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                sb.append(((C42951wA) it.next()).A00.A0z.A01);
                sb.append(' ');
            }
            sb.toString();
        }
    }

    public synchronized Handler A01() {
        Handler handler;
        handler = this.A01;
        if (handler == null) {
            HandlerThread handlerThread = new HandlerThread("Notifications", 10);
            handlerThread.start();
            handler = new Handler(handlerThread.getLooper());
            this.A01 = handler;
        }
        return handler;
    }

    public C42951wA A02(AbstractC15340mz r12) {
        AbstractC15710nm r1 = this.A04;
        C15570nT r2 = this.A06;
        C15450nH r3 = this.A07;
        C15550nR r4 = this.A08;
        AnonymousClass01d r6 = this.A0A;
        return new C42951wA(r1, r2, r3, r4, this.A09, r6, this.A0F, this.A0L, this.A0O, r12);
    }

    public C42951wA A03(AnonymousClass1YY r13) {
        C16590pI r7 = this.A0C;
        AbstractC15710nm r1 = this.A04;
        C15570nT r2 = this.A06;
        C15450nH r3 = this.A07;
        C15550nR r4 = this.A08;
        AnonymousClass01d r6 = this.A0A;
        return new C43001wF(r1, r2, r3, r4, this.A09, r6, r7, this.A0F, this.A0L, this.A0O, r13);
    }

    public final RunnableC43021wH A04(AbstractC14640lm r86, AbstractC15340mz r87, int i, boolean z, boolean z2, boolean z3, boolean z4) {
        AnonymousClass01J r0 = this.A05.A00.A01;
        C14900mE r1 = (C14900mE) r0.A8X.get();
        C15570nT r12 = (C15570nT) r0.AAr.get();
        C19990v2 r13 = (C19990v2) r0.A3M.get();
        C22160yd r14 = (C22160yd) r0.AC4.get();
        C21270x9 r15 = (C21270x9) r0.A4A.get();
        C21290xB r16 = (C21290xB) r0.ANf.get();
        C22670zS r17 = (C22670zS) r0.A0V.get();
        AnonymousClass130 r18 = (AnonymousClass130) r0.A41.get();
        C15550nR r19 = (C15550nR) r0.A45.get();
        AnonymousClass01d r110 = (AnonymousClass01d) r0.ALI.get();
        C15610nY r111 = (C15610nY) r0.AMe.get();
        AnonymousClass018 r112 = (AnonymousClass018) r0.ANb.get();
        C243815h r113 = (C243815h) r0.AIq.get();
        C21300xC r114 = (C21300xC) r0.A0c.get();
        C15860o1 r115 = (C15860o1) r0.A3H.get();
        C22630zO r116 = (C22630zO) r0.AD7.get();
        C18330sH r117 = (C18330sH) r0.AN4.get();
        C21310xD r118 = (C21310xD) r0.AMS.get();
        C20220vP r119 = (C20220vP) r0.AC3.get();
        C23000zz r120 = (C23000zz) r0.ALg.get();
        C22700zV r152 = (C22700zV) r0.AMN.get();
        C15890o4 r142 = (C15890o4) r0.AN1.get();
        C14820m6 r132 = (C14820m6) r0.AN3.get();
        C15680nj r122 = (C15680nj) r0.A4e.get();
        C20710wC r11 = (C20710wC) r0.A8m.get();
        C18360sK r10 = (C18360sK) r0.AN0.get();
        C22450z6 r9 = (C22450z6) r0.A8s.get();
        C21820y2 r8 = (C21820y2) r0.AHx.get();
        C15600nX r7 = (C15600nX) r0.A8x.get();
        AnonymousClass13L r6 = (AnonymousClass13L) r0.AFp.get();
        C15510nN r5 = (C15510nN) r0.AHZ.get();
        C16210od r4 = (C16210od) r0.A0Y.get();
        AnonymousClass11P r3 = (AnonymousClass11P) r0.ABp.get();
        C237612x r2 = (C237612x) r0.AI3.get();
        AnonymousClass11U r121 = (AnonymousClass11U) r0.AGi.get();
        return new RunnableC43021wH(AbstractC18030rn.A00(r0.AO3), r4, (C22490zA) r0.A4c.get(), r1, r12, r117, r16, r17, r18, r19, r152, r111, r15, r3, r14, r110, r10, r142, r132, r112, r13, r122, r7, r118, (C14850m9) r0.A04.get(), r11, r86, r119, r121, r116, r6, r113, r87, r5, r8, r115, r120, r114, r9, r2, (C21280xA) r0.AMU.get(), i, z, z2, z3, z4);
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r8v4, types: [java.util.LinkedList] */
    /* JADX WARN: Type inference failed for: r5v6, types: [java.util.List] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List A05(X.AbstractC14640lm r24, int r25) {
        /*
        // Method dump skipped, instructions count: 603
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20220vP.A05(X.0lm, int):java.util.List");
    }

    public void A06() {
        PendingIntent A01;
        C14860mA r4 = this.A0T;
        if (r4.A0N.A02() && (A01 = AnonymousClass1UY.A01(r4.A0I.A00, 0, new Intent("com.whatsapp.alarm.WEB_RENOTIFY").setPackage("com.whatsapp"), 536870912)) != null) {
            AlarmManager A04 = r4.A0G.A04();
            if (A04 != null) {
                A04.cancel(A01);
            } else {
                Log.w("WebSession/cancelReNotify AlarmManager is null");
            }
            A01.cancel();
        }
        C238313e r1 = this.A0Q;
        synchronized (r1) {
            r1.A01 = null;
            r1.A00 = null;
        }
    }

    public void A07() {
        A0B(null, true, true, false, false, false);
    }

    public void A08(AbstractC14640lm r4) {
        A01().post(new RunnableBRunnable0Shape6S0200000_I0_6(this, 25, r4));
        A06();
    }

    public void A09(AbstractC14640lm r4, AbstractC15340mz r5) {
        A01().post(new RunnableBRunnable0Shape1S0300000_I0_1(this, r5, r4, 15));
    }

    public void A0A(AbstractC15340mz r8, boolean z, boolean z2) {
        A0B(r8, z, this.A03, false, false, z2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x006d, code lost:
        if (r23 == null) goto L_0x006f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0076, code lost:
        if (r0 != false) goto L_0x0078;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0B(X.AbstractC15340mz r23, boolean r24, boolean r25, boolean r26, boolean r27, boolean r28) {
        /*
        // Method dump skipped, instructions count: 244
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20220vP.A0B(X.0mz, boolean, boolean, boolean, boolean, boolean):void");
    }

    public void A0C(AnonymousClass1IS r9, boolean z, boolean z2) {
        if (r9 != null) {
            AbstractC15340mz A03 = this.A0H.A0K.A03(r9);
            if (A03 != null) {
                C30041Vv.A0C(A03);
                A0B(A03, false, z, z2, false, true);
                return;
            }
            Log.i("messagenotification/refreshStatusBarNotificationIfMessageExists/no-message");
            return;
        }
        Log.e("messagenotification/refreshStatusBarNotificationIfMessageExists/no-messag-key");
    }

    public void A0D(boolean z) {
        A01().post(new RunnableBRunnable0Shape0S0110000_I0(this, 13, z));
        A06();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0023, code lost:
        if (r0 == false) goto L_0x0025;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0E(X.C42951wA r6) {
        /*
            r5 = this;
            r3 = 0
            if (r6 == 0) goto L_0x005f
            X.0mz r4 = r6.A00
            byte r1 = r4.A0y
            r0 = 36
            if (r1 == r0) goto L_0x005f
            X.1IS r0 = r4.A0z
            X.0lm r2 = r0.A00
            X.AnonymousClass009.A05(r6)
            java.util.List r1 = r4.A0o
            r3 = 1
            if (r1 == 0) goto L_0x0025
            X.0nT r0 = r5.A06
            r0.A08()
            X.1Ih r0 = r0.A05
            boolean r0 = r1.contains(r0)
            r1 = 1
            if (r0 != 0) goto L_0x0026
        L_0x0025:
            r1 = 0
        L_0x0026:
            X.0mz r0 = r4.A0E()
            if (r0 == 0) goto L_0x0077
            X.0lm r0 = r0.A0B()
            if (r0 != 0) goto L_0x0077
        L_0x0032:
            X.1IS r0 = r4.A0z
            X.0lm r0 = r0.A00
            boolean r0 = X.C15380n4.A0J(r0)
            if (r0 == 0) goto L_0x0075
            if (r3 != 0) goto L_0x0040
            if (r1 == 0) goto L_0x0075
        L_0x0040:
            X.0o1 r1 = r5.A0R
            X.0lm r0 = r4.A0B()
            X.AnonymousClass009.A05(r0)
            java.lang.String r0 = r0.getRawString()
            X.1da r0 = r1.A08(r0)
            boolean r3 = r0.A0A()
        L_0x0055:
            if (r2 == 0) goto L_0x0060
            X.0v2 r0 = r5.A0G
            boolean r0 = r0.A0E(r2)
            if (r0 == 0) goto L_0x0060
        L_0x005f:
            return r3
        L_0x0060:
            X.0o1 r1 = r5.A0R
            X.AnonymousClass009.A05(r2)
            java.lang.String r0 = r2.getRawString()
            X.1da r0 = r1.A08(r0)
            boolean r0 = r0.A0A()
            if (r0 == 0) goto L_0x005f
            r3 = 1
            return r3
        L_0x0075:
            r3 = 0
            goto L_0x0055
        L_0x0077:
            r3 = 0
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20220vP.A0E(X.1wA):boolean");
    }
}
