package X;

import com.facebook.simplejni.NativeHolder;
import com.whatsapp.wamsys.JniBridge;

/* renamed from: X.2Du  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Du {
    public final NativeHolder A00;

    public AnonymousClass2Du(NativeHolder nativeHolder) {
        this.A00 = nativeHolder;
    }

    public AnonymousClass2Du(String str) {
        JniBridge.getInstance();
        this.A00 = new AnonymousClass2Du((NativeHolder) JniBridge.jvidispatchOO(3, str)).A00;
    }
}
