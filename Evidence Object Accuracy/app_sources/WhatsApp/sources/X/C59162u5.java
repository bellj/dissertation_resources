package X;

import android.os.Parcel;

/* renamed from: X.2u5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59162u5 extends C44691zO {
    public final boolean A00;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C59162u5(X.C44691zO r20, boolean r21) {
        /*
            r19 = this;
            r1 = r20
            java.lang.String r6 = r1.A0D
            java.lang.String r7 = r1.A04
            java.lang.String r8 = r1.A0A
            java.math.BigDecimal r12 = r1.A05
            X.1Yn r5 = r1.A03
            java.lang.String r9 = r1.A0C
            java.lang.String r10 = r1.A0E
            java.util.List r13 = r1.A06
            X.1zS r3 = r1.A01
            X.3M7 r4 = r1.A02
            java.lang.String r11 = r1.A0B
            int r14 = r1.A00
            boolean r0 = r1.A07
            long r15 = r1.A08
            r17 = 1
            r2 = 0
            r1 = r19
            r18 = r0
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r17, r18)
            r0 = r21
            r1.A00 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C59162u5.<init>(X.1zO, boolean):void");
    }

    @Override // X.C44691zO, java.lang.Object
    public boolean equals(Object obj) {
        if (!super.equals(obj) || !(obj instanceof C59162u5) || this.A00 != ((C59162u5) obj).A00) {
            return false;
        }
        return true;
    }

    @Override // X.C44691zO, java.lang.Object
    public int hashCode() {
        return (super.hashCode() * 31) + (this.A00 ? 1 : 0);
    }

    @Override // X.C44691zO, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeByte(this.A00 ? (byte) 1 : 0);
    }
}
