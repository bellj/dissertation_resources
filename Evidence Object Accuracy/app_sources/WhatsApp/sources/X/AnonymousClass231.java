package X;

/* renamed from: X.231  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass231 implements AbstractC21730xt {
    public final /* synthetic */ AnonymousClass016 A00;
    public final /* synthetic */ C17260qW A01;

    public AnonymousClass231(AnonymousClass016 r1, C17260qW r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        this.A00.A0A(new AnonymousClass4NX(null, false));
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r5, String str) {
        this.A00.A0A(new AnonymousClass4NX(null, false));
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r5, String str) {
        AnonymousClass016 r3;
        AnonymousClass4NX r2;
        String str2;
        try {
            if ("result".equals(r5.A0I("type", null))) {
                r3 = this.A00;
                AnonymousClass1V8 A0E = r5.A0E("accept");
                if (A0E != null) {
                    str2 = A0E.A0I("optout", null);
                } else {
                    str2 = null;
                }
                r2 = new AnonymousClass4NX(Boolean.valueOf("true".equals(str2)), true);
            } else {
                r3 = this.A00;
                r2 = new AnonymousClass4NX(null, false);
            }
            r3.A0A(r2);
        } catch (AnonymousClass1V9 unused) {
            this.A00.A0A(new AnonymousClass4NX(null, false));
        }
    }
}
