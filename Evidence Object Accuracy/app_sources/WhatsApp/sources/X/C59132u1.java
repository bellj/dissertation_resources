package X;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape0S1200000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S1200100_I0;
import com.whatsapp.R;
import com.whatsapp.biz.cart.view.fragment.CartFragment;
import com.whatsapp.biz.catalog.view.widgets.QuantitySelector;
import com.whatsapp.jid.UserJid;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/* renamed from: X.2u1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59132u1 extends AbstractC75653kC {
    public final ImageView A00;
    public final LinearLayout A01;
    public final TextView A02;
    public final TextView A03;
    public final TextView A04;
    public final AnonymousClass2EM A05;
    public final CartFragment A06;
    public final C37071lG A07;
    public final QuantitySelector A08;
    public final AnonymousClass018 A09;

    public C59132u1(View view, AnonymousClass2EM r4, AbstractC37061lF r5, CartFragment cartFragment, CartFragment cartFragment2, C37071lG r8, AnonymousClass018 r9) {
        super(view);
        this.A09 = r9;
        this.A05 = r4;
        this.A07 = r8;
        this.A06 = cartFragment2;
        this.A04 = C12960it.A0I(view, R.id.cart_item_title);
        this.A02 = C12960it.A0I(view, R.id.cart_item_price);
        this.A03 = C12960it.A0I(view, R.id.cart_item_original_price);
        this.A01 = (LinearLayout) AnonymousClass028.A0D(view, R.id.cart_item_price_container);
        QuantitySelector quantitySelector = (QuantitySelector) AnonymousClass028.A0D(view, R.id.cart_item_quantity_selector);
        this.A08 = quantitySelector;
        quantitySelector.A04 = new AnonymousClass5TT(r5, this, cartFragment2) { // from class: X.3VR
            public final /* synthetic */ AbstractC37061lF A00;
            public final /* synthetic */ C59132u1 A01;
            public final /* synthetic */ CartFragment A02;

            {
                this.A01 = r2;
                this.A00 = r1;
                this.A02 = r3;
            }

            @Override // X.AnonymousClass5TT
            public final void ARm(long j) {
                C59132u1 r0 = this.A01;
                AbstractC37061lF r1 = this.A00;
                CartFragment cartFragment3 = this.A02;
                r1.ACa(r0.A00());
                C12960it.A0w(cartFragment3.A05(), cartFragment3.A0g, j);
            }
        };
        quantitySelector.A05 = new AnonymousClass5TU(r5, this, cartFragment2) { // from class: X.3VS
            public final /* synthetic */ AbstractC37061lF A00;
            public final /* synthetic */ C59132u1 A01;
            public final /* synthetic */ CartFragment A02;

            {
                this.A01 = r2;
                this.A00 = r1;
                this.A02 = r3;
            }

            @Override // X.AnonymousClass5TU
            public final void AUU(long j) {
                C59132u1 r2 = this.A01;
                AbstractC37061lF r3 = this.A00;
                CartFragment cartFragment3 = this.A02;
                String str = ((C84493zO) r3.ACa(r2.A00())).A00.A01.A0D;
                C37041lD r1 = cartFragment3.A0R;
                if (j == 0) {
                    AnonymousClass2EM r52 = r1.A0I;
                    UserJid userJid = r1.A0O;
                    r52.A0G.A03(userJid, 54, str, 30);
                    r52.A0L.Ab2(new RunnableBRunnable0Shape0S1200000_I0(r52, userJid, str, 9));
                } else {
                    AnonymousClass2EM r92 = r1.A0I;
                    UserJid userJid2 = r1.A0O;
                    r92.A0G.A02(userJid2, 53, Long.valueOf(j), str, 29);
                    r92.A0L.Ab2(new RunnableBRunnable0Shape0S1200100_I0(r92, userJid2, str, 1, j));
                }
                int i = 0;
                if (j == 0) {
                    i = 4;
                }
                r2.A08.setVisibility(i);
            }
        };
        this.A00 = C12970iu.A0K(view, R.id.cart_item_thumbnail);
        AbstractView$OnClickListenerC34281fs.A03(view, this, cartFragment, r5, 1);
    }

    @Override // X.AbstractC75653kC
    public void A08(AnonymousClass4JV r13) {
        AnonymousClass01T r1;
        C84493zO r132 = (C84493zO) r13;
        C92584Wm r7 = r132.A00;
        TextView textView = this.A04;
        C44691zO r2 = r7.A01;
        textView.setText(r2.A04);
        QuantitySelector quantitySelector = this.A08;
        quantitySelector.A04(r7.A00, r2.A08);
        quantitySelector.setVisibility(0);
        long j = r7.A00;
        BigDecimal bigDecimal = r2.A05;
        C30711Yn r9 = r2.A03;
        AnonymousClass3M7 r12 = r2.A02;
        AnonymousClass018 r8 = this.A09;
        Date date = r132.A01;
        this.A0H.getContext();
        String str = null;
        if (bigDecimal == null || r9 == null) {
            r1 = new AnonymousClass01T(null, null);
        } else {
            String A03 = r9.A03(r8, bigDecimal.multiply(BigDecimal.valueOf(j)), true);
            if (r12 != null && r12.A00(date)) {
                str = r9.A03(r8, r12.A01.multiply(BigDecimal.valueOf(j)), true);
            }
            r1 = new AnonymousClass01T(A03, str);
        }
        Object obj = r1.A00;
        if (obj == null) {
            this.A02.setText("$000.00");
            this.A01.setVisibility(4);
        } else {
            this.A01.setVisibility(0);
            Object obj2 = r1.A01;
            TextView textView2 = this.A02;
            if (obj2 == null) {
                textView2.setText((CharSequence) obj);
                this.A03.setVisibility(8);
            } else {
                textView2.setText((CharSequence) obj2);
                TextView textView3 = this.A03;
                textView3.setText((CharSequence) obj);
                textView3.setVisibility(0);
                textView3.setPaintFlags(textView3.getPaintFlags() | 16);
            }
        }
        ImageView imageView = this.A00;
        if (!A09(imageView, r2)) {
            AnonymousClass2EM r0 = this.A05;
            C44691zO A05 = r0.A0F.A05(null, r2.A0D);
            if (A05 == null || !A09(imageView, A05)) {
                AnonymousClass4Dz.A00(imageView);
            }
        }
    }

    public final boolean A09(ImageView imageView, C44691zO r6) {
        List<C44741zT> list = r6.A06;
        if (!list.isEmpty() && !r6.A02()) {
            for (C44741zT r1 : list) {
                if (!(r1 == null || TextUtils.isEmpty(r1.A01))) {
                    AnonymousClass3AK.A00(imageView, this.A07, new AnonymousClass3M0(r1.A04, r1.A01));
                    return true;
                }
            }
        }
        return false;
    }
}
