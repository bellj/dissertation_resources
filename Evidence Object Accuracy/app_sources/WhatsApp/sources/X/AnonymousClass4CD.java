package X;

/* renamed from: X.4CD  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4CD extends Exception {
    public AnonymousClass4CD(String str) {
        super(str);
    }

    public AnonymousClass4CD(String str, Throwable th) {
        super(str, th);
    }
}
