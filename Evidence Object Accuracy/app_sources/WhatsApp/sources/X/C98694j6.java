package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4j6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98694j6 implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        int i = 0;
        String str = null;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c != 1) {
                str = C95664e9.A09(parcel, str, c, 2, readInt);
            } else {
                i = C95664e9.A02(parcel, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78473oy(i, str);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new C78473oy[i];
    }
}
