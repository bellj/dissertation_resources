package X;

import java.math.BigInteger;
import java.util.Arrays;

/* renamed from: X.5ND  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5ND extends AnonymousClass1TL {
    public static AnonymousClass5ND[] A02 = new AnonymousClass5ND[12];
    public final int A00;
    public final byte[] A01;

    @Override // X.AnonymousClass1TL
    public void A08(AnonymousClass1TP r3, boolean z) {
        r3.A06(this.A01, 10, z);
    }

    @Override // X.AnonymousClass1TL
    public boolean A09() {
        return false;
    }

    @Override // X.AnonymousClass1TL, X.AnonymousClass1TM
    public int hashCode() {
        return AnonymousClass1TT.A00(this.A01);
    }

    public AnonymousClass5ND(int i) {
        if (i >= 0) {
            this.A01 = BigInteger.valueOf((long) i).toByteArray();
            this.A00 = 0;
            return;
        }
        throw C12970iu.A0f("enumerated must be non-negative");
    }

    public AnonymousClass5ND(byte[] bArr) {
        int length = bArr.length;
        if (length == 0 || (length != 1 && bArr[0] == (bArr[1] >> 7) && !C94664cJ.A01("org.spongycastle.asn1.allow_unsafe_integer"))) {
            throw C12970iu.A0f("malformed enumerated");
        } else if ((bArr[0] & 128) == 0) {
            this.A01 = AnonymousClass1TT.A02(bArr);
            int i = length - 1;
            int i2 = 0;
            while (i2 < i) {
                int i3 = i2 + 1;
                if (bArr[i2] != (bArr[i3] >> 7)) {
                    break;
                }
                i2 = i3;
            }
            this.A00 = i2;
        } else {
            throw C12970iu.A0f("enumerated must be non-negative");
        }
    }

    public static AnonymousClass5ND A00(Object obj) {
        if (obj == null || (obj instanceof AnonymousClass5ND)) {
            return (AnonymousClass5ND) obj;
        }
        if (obj instanceof byte[]) {
            try {
                return (AnonymousClass5ND) AnonymousClass1TL.A03((byte[]) obj);
            } catch (Exception e) {
                throw C12970iu.A0f(C12960it.A0d(e.toString(), C12960it.A0k("encoding error in getInstance: ")));
            }
        } else {
            throw C12970iu.A0f(C12960it.A0d(C12980iv.A0s(obj), C12960it.A0k("illegal object in getInstance: ")));
        }
    }

    @Override // X.AnonymousClass1TL
    public int A05() {
        return C72453ed.A0H(this.A01);
    }

    @Override // X.AnonymousClass1TL
    public boolean A0A(AnonymousClass1TL r3) {
        if (!(r3 instanceof AnonymousClass5ND)) {
            return false;
        }
        return Arrays.equals(this.A01, ((AnonymousClass5ND) r3).A01);
    }

    public int A0B() {
        byte[] bArr = this.A01;
        int length = bArr.length;
        int i = this.A00;
        if (length - i <= 4) {
            int max = Math.max(i, length - 4);
            int i2 = -1 & bArr[max];
            while (true) {
                max++;
                if (max >= length) {
                    return i2;
                }
                i2 = (i2 << 8) | (bArr[max] & 255);
            }
        } else {
            throw new ArithmeticException("ASN.1 Enumerated out of int range");
        }
    }
}
