package X;

import com.whatsapp.util.Log;

/* renamed from: X.2S8  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2S8 implements AnonymousClass2S5 {
    public final /* synthetic */ long A00;
    public final /* synthetic */ AnonymousClass2S1 A01;
    public final /* synthetic */ AnonymousClass17Z A02;

    public AnonymousClass2S8(AnonymousClass2S1 r1, AnonymousClass17Z r2, long j) {
        this.A02 = r2;
        this.A00 = j;
        this.A01 = r1;
    }

    @Override // X.AnonymousClass2S5
    public void APo(C452120p r3) {
        StringBuilder sb = new StringBuilder("getOfferDetails: failed with error: ");
        sb.append(r3);
        Log.e(sb.toString());
        AnonymousClass2S1 r0 = this.A01;
        if (r0 != null) {
            r0.APk();
        }
    }

    @Override // X.AnonymousClass2S5
    public void AX2(AnonymousClass1V8 r6) {
        try {
            AnonymousClass1V8 A0F = r6.A0F("account").A0F("offer_eligibility");
            this.A02.A08(new C69173Yf(this), A0F, this.A00);
        } catch (AnonymousClass1V9 e) {
            AnonymousClass2S1 r0 = this.A01;
            if (r0 != null) {
                r0.APk();
            }
            e.printStackTrace();
        }
    }
}
