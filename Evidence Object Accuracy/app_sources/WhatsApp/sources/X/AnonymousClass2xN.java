package X;

import android.view.View;

/* renamed from: X.2xN  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2xN extends AbstractC104124rr {
    public final /* synthetic */ AnonymousClass0QQ A00;
    public final /* synthetic */ AnonymousClass03U A01;
    public final /* synthetic */ C55262i6 A02;

    public AnonymousClass2xN(AnonymousClass0QQ r1, AnonymousClass03U r2, C55262i6 r3) {
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC104124rr, X.AbstractC12530i4
    public void AMC(View view) {
        this.A00.A09(null);
        view.setScaleX(1.0f);
        view.setScaleY(1.0f);
        C55262i6 r2 = this.A02;
        AnonymousClass03U r1 = this.A01;
        r2.A03(r1);
        C12980iv.A1K(r2, r1, r2.A0A);
    }

    @Override // X.AbstractC104124rr, X.AbstractC12530i4
    public void AMD(View view) {
    }
}
