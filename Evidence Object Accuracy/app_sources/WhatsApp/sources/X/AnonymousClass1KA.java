package X;

/* renamed from: X.1KA  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1KA {
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;

    public AnonymousClass1KA(int i, int i2, int i3, int i4) {
        this.A02 = i;
        this.A03 = i2;
        this.A00 = i3;
        this.A01 = i4;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("{maxKb=");
        sb.append(this.A02);
        sb.append(", quality=");
        sb.append(this.A03);
        sb.append(", maxEdge=");
        sb.append(this.A00);
        sb.append('}');
        return sb.toString();
    }
}
