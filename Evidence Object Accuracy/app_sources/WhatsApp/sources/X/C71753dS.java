package X;

import android.graphics.Path;
import android.graphics.RectF;
import java.util.HashMap;

/* renamed from: X.3dS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C71753dS extends HashMap<Integer, AbstractC469028d<RectF, Path>> {
    public C71753dS() {
        put(C12960it.A0V(), AnonymousClass51L.A00);
        put(C12970iu.A0g(), AnonymousClass51K.A00);
    }
}
