package X;

import java.util.Map;

/* renamed from: X.4XX  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass4XX {
    public abstract Map createMap();

    public AbstractC81183tb arrayListValues() {
        return arrayListValues(2);
    }

    public AbstractC81183tb arrayListValues(int i) {
        C28251Mi.checkNonnegative(2, "expectedValuesPerKey");
        return new C81153tY(this, 2);
    }
}
