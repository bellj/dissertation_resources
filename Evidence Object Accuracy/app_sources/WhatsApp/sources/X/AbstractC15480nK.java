package X;

import android.content.SharedPreferences;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.Map;

/* renamed from: X.0nK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC15480nK {
    public final String A00;
    public final String A01;

    public AbstractC15480nK(String str, String str2) {
        this.A00 = str;
        this.A01 = str2;
    }

    public void A00(SharedPreferences.Editor editor, Map map) {
        String str;
        NumberFormatException e;
        String str2;
        StringBuilder sb;
        String str3;
        String str4;
        if (!(this instanceof AnonymousClass1ML)) {
            if (!(this instanceof C16140oW)) {
                boolean z = this instanceof AnonymousClass1NJ;
                str2 = (String) map.get(this.A01);
                boolean isEmpty = TextUtils.isEmpty(str2);
                if (!z) {
                    if (!isEmpty) {
                        try {
                            boolean z2 = false;
                            if (Integer.parseInt(str2) != 0) {
                                z2 = true;
                            }
                            editor.putBoolean(this.A00, z2);
                            return;
                        } catch (NumberFormatException e2) {
                            e = e2;
                            str4 = "ServerPropBoolean/invalid number format for property; prefKey=";
                            sb = new StringBuilder(str4);
                            str3 = this.A00;
                            sb.append(str3);
                            sb.append("; value=");
                            sb.append(str2);
                            Log.w(sb.toString(), e);
                            editor.remove(str3);
                            return;
                        }
                    }
                } else if (!isEmpty) {
                    try {
                        editor.putFloat(this.A00, Float.parseFloat(str2));
                        return;
                    } catch (NumberFormatException e3) {
                        e = e3;
                        str4 = "ServerPropFloat/invalid number format for property; prefKey=";
                        sb = new StringBuilder(str4);
                        str3 = this.A00;
                        sb.append(str3);
                        sb.append("; value=");
                        sb.append(str2);
                        Log.w(sb.toString(), e);
                        editor.remove(str3);
                        return;
                    }
                }
            } else {
                C16140oW r3 = (C16140oW) this;
                str2 = (String) map.get(((AbstractC15480nK) r3).A01);
                if (TextUtils.isEmpty(str2)) {
                    str = ((AbstractC15480nK) r3).A00;
                    editor.remove(str);
                }
                try {
                    int parseInt = Integer.parseInt(str2);
                    Integer num = r3.A02;
                    if (num != null) {
                        parseInt = Math.max(num.intValue(), parseInt);
                    }
                    Integer num2 = r3.A01;
                    if (num2 != null) {
                        parseInt = Math.min(num2.intValue(), parseInt);
                    }
                    editor.putInt(((AbstractC15480nK) r3).A00, parseInt);
                    return;
                } catch (NumberFormatException e4) {
                    e = e4;
                    sb = new StringBuilder("ServerPropInteger/invalid number format for property; prefKey=");
                    str3 = ((AbstractC15480nK) r3).A00;
                }
            }
            sb.append(str3);
            sb.append("; value=");
            sb.append(str2);
            Log.w(sb.toString(), e);
            editor.remove(str3);
            return;
        }
        String str5 = (String) map.get(this.A01);
        if (!TextUtils.isEmpty(str5)) {
            editor.putString(this.A00, str5);
            return;
        }
        str = this.A00;
        editor.remove(str);
    }
}
