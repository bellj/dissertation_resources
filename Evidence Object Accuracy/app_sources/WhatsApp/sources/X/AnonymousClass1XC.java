package X;

import java.util.Collections;
import java.util.List;

/* renamed from: X.1XC  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1XC extends AbstractC15340mz implements AbstractC16400ox, AbstractC27681Ir, AbstractC16420oz {
    public int A00;
    public long A01;

    public AnonymousClass1XC(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 51, j);
    }

    public AnonymousClass1XC(AnonymousClass1IS r9, AnonymousClass1XC r10, long j) {
        super(r10, r9, j, true);
        this.A00 = r10.A00;
        this.A01 = r10.A01;
    }

    public AnonymousClass1XC(C57212mf r5, AnonymousClass1IS r6, long j) {
        super(r6, (byte) 51, j);
        AnonymousClass4BV A00 = AnonymousClass4BV.A00(r5.A01);
        this.A00 = (A00 == null ? AnonymousClass4BV.A03 : A00).value;
        if ((r5.A00 & 2) == 2) {
            this.A01 = r5.A02 * 1000;
        }
    }

    @Override // X.AbstractC16420oz
    public void A6k(C39971qq r9) {
        AnonymousClass1G3 r7 = r9.A03;
        C57212mf r0 = ((C27081Fy) r7.A00).A0S;
        if (r0 == null) {
            r0 = C57212mf.A03;
        }
        AnonymousClass1G4 A0T = r0.A0T();
        AnonymousClass4BV A00 = AnonymousClass4BV.A00(this.A00);
        A0T.A03();
        C57212mf r1 = (C57212mf) A0T.A00;
        r1.A00 |= 1;
        r1.A01 = A00.value;
        long j = this.A01;
        if (j > 0) {
            A0T.A03();
            C57212mf r12 = (C57212mf) A0T.A00;
            r12.A00 |= 2;
            r12.A02 = j / 1000;
        }
        r7.A03();
        C27081Fy r13 = (C27081Fy) r7.A00;
        r13.A0S = (C57212mf) A0T.A02();
        r13.A01 |= 4;
    }

    @Override // X.AbstractC16400ox
    public AbstractC15340mz A7M(AnonymousClass1IS r4) {
        return new AnonymousClass1XC(r4, this, this.A0I);
    }

    @Override // X.AbstractC27681Ir
    public List AGt() {
        AnonymousClass1W9[] r3 = new AnonymousClass1W9[2];
        r3[0] = new AnonymousClass1W9("type", "invite");
        int i = this.A00;
        String str = i != 1 ? i != 2 ? i != 3 ? null : "UPI" : "NOVI" : "FBPAY";
        AnonymousClass009.A05(str);
        r3[1] = new AnonymousClass1W9("service", str);
        return Collections.singletonList(new AnonymousClass1V8("pay", r3));
    }
}
