package X;

/* renamed from: X.3Sk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C67673Sk implements AbstractC14080kp, AbstractC14130ku {
    public long A00;
    public long A01 = 0;
    public AbstractC14130ku A02;
    public C107614xf[] A03 = new C107614xf[0];
    public final AbstractC14080kp A04;

    public C67673Sk(AbstractC14080kp r3, long j) {
        this.A04 = r3;
        this.A00 = j;
    }

    public boolean A00() {
        return C12960it.A1S((this.A01 > -9223372036854775807L ? 1 : (this.A01 == -9223372036854775807L ? 0 : -1)));
    }

    @Override // X.AbstractC14080kp
    public boolean A7e(long j) {
        return this.A04.A7e(j);
    }

    @Override // X.AbstractC14080kp
    public void A8x(long j, boolean z) {
        this.A04.A8x(j, false);
    }

    @Override // X.AbstractC14080kp
    public long AAe(C94224bS r18, long j) {
        long j2;
        C94224bS r10 = r18;
        if (j == 0) {
            return 0;
        }
        long j3 = r10.A01;
        long max = Math.max(0L, Math.min(j3, j - 0));
        long j4 = r10.A00;
        long j5 = this.A00;
        if (j5 == Long.MIN_VALUE) {
            j2 = Long.MAX_VALUE;
        } else {
            j2 = j5 - j;
        }
        long max2 = Math.max(0L, Math.min(j4, j2));
        if (!(max == j3 && max2 == j4)) {
            r10 = new C94224bS(max, max2);
        }
        return this.A04.AAe(r10, j);
    }

    @Override // X.AbstractC14080kp
    public long AB2() {
        long AB2 = this.A04.AB2();
        if (AB2 != Long.MIN_VALUE) {
            long j = this.A00;
            if (j == Long.MIN_VALUE || AB2 < j) {
                return AB2;
            }
        }
        return Long.MIN_VALUE;
    }

    @Override // X.AbstractC14080kp
    public long AEf() {
        long AEf = this.A04.AEf();
        if (AEf != Long.MIN_VALUE) {
            long j = this.A00;
            if (j == Long.MIN_VALUE || AEf < j) {
                return AEf;
            }
        }
        return Long.MIN_VALUE;
    }

    @Override // X.AbstractC14080kp
    public C100564m7 AHH() {
        return this.A04.AHH();
    }

    @Override // X.AbstractC14080kp
    public boolean AJh() {
        return this.A04.AJh();
    }

    @Override // X.AbstractC14080kp
    public void ALS() {
        this.A04.ALS();
    }

    @Override // X.AbstractC14140kv
    public /* bridge */ /* synthetic */ void AOe(AbstractC14090kq r2) {
        this.A02.AOe(this);
    }

    @Override // X.AbstractC14130ku
    public void AUB(AbstractC14080kp r2) {
        this.A02.AUB(this);
    }

    @Override // X.AbstractC14080kp
    public void AZS(AbstractC14130ku r2, long j) {
        this.A02 = r2;
        this.A04.AZS(this, j);
    }

    @Override // X.AbstractC14080kp
    public long AZt() {
        if (A00()) {
            long j = this.A01;
            this.A01 = -9223372036854775807L;
            long AZt = AZt();
            return AZt != -9223372036854775807L ? AZt : j;
        }
        long AZt2 = this.A04.AZt();
        if (AZt2 == -9223372036854775807L) {
            return -9223372036854775807L;
        }
        boolean z = true;
        C95314dV.A04(C12990iw.A1W((AZt2 > 0 ? 1 : (AZt2 == 0 ? 0 : -1))));
        long j2 = this.A00;
        if (j2 != Long.MIN_VALUE && AZt2 > j2) {
            z = false;
        }
        C95314dV.A04(z);
        return AZt2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0031, code lost:
        if (r5 > r3) goto L_0x0034;
     */
    @Override // X.AbstractC14080kp
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long AbT(long r9) {
        /*
            r8 = this;
            r0 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            r8.A01 = r0
            X.4xf[] r3 = r8.A03
            int r2 = r3.length
            r7 = 0
            r1 = 0
        L_0x000c:
            if (r1 >= r2) goto L_0x0017
            r0 = r3[r1]
            if (r0 == 0) goto L_0x0014
            r0.A00 = r7
        L_0x0014:
            int r1 = r1 + 1
            goto L_0x000c
        L_0x0017:
            X.0kp r0 = r8.A04
            long r5 = r0.AbT(r9)
            int r0 = (r5 > r9 ? 1 : (r5 == r9 ? 0 : -1))
            if (r0 == 0) goto L_0x0033
            r1 = 0
            int r0 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x0034
            long r3 = r8.A00
            r1 = -9223372036854775808
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x0033
            int r0 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
            if (r0 > 0) goto L_0x0034
        L_0x0033:
            r7 = 1
        L_0x0034:
            X.C95314dV.A04(r7)
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C67673Sk.AbT(long):long");
    }

    /* JADX DEBUG: Multi-variable search result rejected for r7v0, resolved type: X.4xf[] */
    /* JADX DEBUG: Multi-variable search result rejected for r0v13, resolved type: java.lang.Object[] */
    /* JADX DEBUG: Multi-variable search result rejected for r0v14, resolved type: java.lang.Object[] */
    /* JADX DEBUG: Multi-variable search result rejected for r0v15, resolved type: X.4xf */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x004a, code lost:
        if (r10 > r0) goto L_0x0077;
     */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0052  */
    @Override // X.AbstractC14080kp
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long AbW(X.AbstractC116795Wx[] r19, X.AbstractC117085Ye[] r20, boolean[] r21, boolean[] r22, long r23) {
        /*
            r18 = this;
            r6 = r19
            int r5 = r6.length
            X.4xf[] r7 = new X.C107614xf[r5]
            r4 = r18
            r4.A03 = r7
            X.5Wx[] r12 = new X.AbstractC116795Wx[r5]
            r3 = 0
            r1 = 0
        L_0x000d:
            r2 = 0
            if (r1 >= r5) goto L_0x0021
            r0 = r19[r1]
            r7[r1] = r0
            r0 = r7[r1]
            if (r0 == 0) goto L_0x001c
            r0 = r7[r1]
            X.5Wx r2 = r0.A01
        L_0x001c:
            r12[r1] = r2
            int r1 = r1 + 1
            goto L_0x000d
        L_0x0021:
            X.0kp r11 = r4.A04
            r13 = r20
            r14 = r21
            r16 = r23
            r15 = r22
            long r10 = r11.AbW(r12, r13, r14, r15, r16)
            r0 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            r4.A01 = r0
            int r0 = (r10 > r23 ? 1 : (r10 == r23 ? 0 : -1))
            if (r0 == 0) goto L_0x004c
            r7 = 0
            int r0 = (r10 > r7 ? 1 : (r10 == r7 ? 0 : -1))
            if (r0 < 0) goto L_0x0077
            long r0 = r4.A00
            r8 = -9223372036854775808
            int r7 = (r0 > r8 ? 1 : (r0 == r8 ? 0 : -1))
            if (r7 == 0) goto L_0x004c
            int r7 = (r10 > r0 ? 1 : (r10 == r0 ? 0 : -1))
            if (r7 > 0) goto L_0x0077
        L_0x004c:
            r0 = 1
        L_0x004d:
            X.C95314dV.A04(r0)
        L_0x0050:
            if (r3 >= r5) goto L_0x0079
            r0 = r12[r3]
            X.4xf[] r7 = r4.A03
            if (r0 != 0) goto L_0x0061
            r7[r3] = r2
        L_0x005a:
            r0 = r7[r3]
            r19[r3] = r0
            int r3 = r3 + 1
            goto L_0x0050
        L_0x0061:
            r0 = r7[r3]
            if (r0 == 0) goto L_0x006d
            r0 = r7[r3]
            X.5Wx r1 = r0.A01
            r0 = r12[r3]
            if (r1 == r0) goto L_0x005a
        L_0x006d:
            r1 = r12[r3]
            X.4xf r0 = new X.4xf
            r0.<init>(r4, r1)
            r7[r3] = r0
            goto L_0x005a
        L_0x0077:
            r0 = 0
            goto L_0x004d
        L_0x0079:
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C67673Sk.AbW(X.5Wx[], X.5Ye[], boolean[], boolean[], long):long");
    }
}
