package X;

/* renamed from: X.23g  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C458123g extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Integer A02;
    public Integer A03;
    public Integer A04;
    public Integer A05;
    public Integer A06;
    public Long A07;
    public Long A08;

    public C458123g() {
        super(1176, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(6, this.A02);
        r3.Abe(2, this.A03);
        r3.Abe(7, this.A04);
        r3.Abe(5, this.A07);
        r3.Abe(8, this.A00);
        r3.Abe(9, this.A01);
        r3.Abe(4, this.A05);
        r3.Abe(3, this.A06);
        r3.Abe(1, this.A08);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        String obj3;
        String obj4;
        String obj5;
        StringBuilder sb = new StringBuilder("WamStatusPost {");
        Integer num = this.A02;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "defaultStatusPrivacySetting", obj);
        Integer num2 = this.A03;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mediaType", obj2);
        Integer num3 = this.A04;
        if (num3 == null) {
            obj3 = null;
        } else {
            obj3 = num3.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "perPostStatusPrivacySetting", obj3);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "retryCount", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "statusAudienceSelectorClicked", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "statusAudienceSelectorUpdated", this.A01);
        Integer num4 = this.A05;
        if (num4 == null) {
            obj4 = null;
        } else {
            obj4 = num4.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "statusPostOrigin", obj4);
        Integer num5 = this.A06;
        if (num5 == null) {
            obj5 = null;
        } else {
            obj5 = num5.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "statusPostResult", obj5);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "statusSessionId", this.A08);
        sb.append("}");
        return sb.toString();
    }
}
