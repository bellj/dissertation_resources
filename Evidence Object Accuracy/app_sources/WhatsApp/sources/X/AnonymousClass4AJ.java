package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.4AJ  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4AJ extends Enum {
    public static final /* synthetic */ AnonymousClass4AJ[] A00;
    public static final AnonymousClass4AJ A01;
    public static final AnonymousClass4AJ A02;
    public static final AnonymousClass4AJ A03;

    public static AnonymousClass4AJ valueOf(String str) {
        return (AnonymousClass4AJ) Enum.valueOf(AnonymousClass4AJ.class, str);
    }

    public static AnonymousClass4AJ[] values() {
        return (AnonymousClass4AJ[]) A00.clone();
    }

    static {
        AnonymousClass4AJ r4 = new AnonymousClass4AJ("BACK", 0);
        A01 = r4;
        AnonymousClass4AJ r3 = new AnonymousClass4AJ("CLOSE", 1);
        A02 = r3;
        AnonymousClass4AJ r1 = new AnonymousClass4AJ("NONE", 2);
        A03 = r1;
        AnonymousClass4AJ[] r0 = new AnonymousClass4AJ[3];
        C12970iu.A1U(r4, r3, r0);
        r0[2] = r1;
        A00 = r0;
    }

    public AnonymousClass4AJ(String str, int i) {
    }
}
