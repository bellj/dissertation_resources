package X;

import java.util.Arrays;

/* renamed from: X.3BG  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3BG {
    public final AnonymousClass1V8 A00;

    public AnonymousClass3BG(AnonymousClass4LW r5) {
        C41141sy r3 = new C41141sy("context");
        AnonymousClass1V8 r2 = r5.A00;
        r3.A07(r2, C12960it.A0l());
        r3.A09(r2, Arrays.asList(new String[0]), C12960it.A0l());
        this.A00 = r3.A03();
    }
}
