package X;

import java.io.InputStream;

/* renamed from: X.0Ip  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C03650Ip extends InputStream {
    public int A00 = 0;
    public int A01 = 0;
    public final AbstractC12930in A02;

    @Override // java.io.InputStream
    public boolean markSupported() {
        return true;
    }

    public C03650Ip(AbstractC12930in r2) {
        AnonymousClass0RA.A00(!r2.isClosed());
        this.A02 = r2;
    }

    @Override // java.io.InputStream
    public int available() {
        return this.A02.size() - this.A01;
    }

    @Override // java.io.InputStream
    public void mark(int i) {
        this.A00 = this.A01;
    }

    @Override // java.io.InputStream
    public int read() {
        if (available() <= 0) {
            return -1;
        }
        AbstractC12930in r2 = this.A02;
        int i = this.A01;
        this.A01 = i + 1;
        return r2.AZm(i) & 255;
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr) {
        return read(bArr, 0, bArr.length);
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr, int i, int i2) {
        if (i < 0 || i2 < 0 || i + i2 > bArr.length) {
            StringBuilder sb = new StringBuilder("length=");
            sb.append(bArr.length);
            sb.append("; regionStart=");
            sb.append(i);
            sb.append("; regionLength=");
            sb.append(i2);
            throw new ArrayIndexOutOfBoundsException(sb.toString());
        }
        int available = available();
        if (available <= 0) {
            return -1;
        }
        if (i2 <= 0) {
            return 0;
        }
        int min = Math.min(available, i2);
        this.A02.AZr(bArr, this.A01, i, min);
        this.A01 += min;
        return min;
    }

    @Override // java.io.InputStream
    public void reset() {
        this.A01 = this.A00;
    }

    @Override // java.io.InputStream
    public long skip(long j) {
        boolean z = false;
        if (j >= 0) {
            z = true;
        }
        AnonymousClass0RA.A00(z);
        int min = Math.min((int) j, available());
        this.A01 += min;
        return (long) min;
    }
}
