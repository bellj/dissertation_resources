package X;

import android.content.Intent;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiPaymentsAccountSetupActivity;
import com.whatsapp.payments.ui.widget.PaymentView;

/* renamed from: X.68d  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1327668d implements AnonymousClass1FK {
    public final /* synthetic */ AbstractActivityC121525iS A00;

    @Override // X.AnonymousClass1FK
    public void AV3(C452120p r1) {
    }

    public C1327668d(AbstractActivityC121525iS r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass1FK
    public void AVA(C452120p r2) {
        this.A00.AaN();
    }

    @Override // X.AnonymousClass1FK
    public void AVB(C452220q r6) {
        if (!r6.A02) {
            AbstractActivityC121525iS r2 = this.A00;
            C12970iu.A0M(r2, R.id.unlink_payment_accounts_title).setText(R.string.payment_account_not_unlinked);
            r2.findViewById(R.id.unlink_payment_accounts_desc).setVisibility(8);
            r2.Ado(R.string.payment_account_not_unlinked);
            return;
        }
        AbstractActivityC121525iS r3 = this.A00;
        C1329668y r1 = ((AbstractActivityC121665jA) r3).A0B;
        r1.A8l(r1.A07(), true);
        r3.A2q();
        Intent A0D = C12990iw.A0D(r3, IndiaUpiPaymentsAccountSetupActivity.class);
        PaymentView paymentView = r3.A0W;
        if (paymentView != null) {
            ((AbstractActivityC121685jC) r3).A0h = paymentView.getPaymentNote();
            ((AbstractActivityC121685jC) r3).A0C = r3.A0W.getPaymentBackground();
            PaymentView paymentView2 = r3.A0W;
            ((AbstractActivityC121685jC) r3).A0c = paymentView2.getStickerIfSelected();
            ((AbstractActivityC121685jC) r3).A0e = paymentView2.getStickerSendOrigin();
        }
        r3.A2v(A0D);
        int i = 3;
        if ("payment_composer_icon".equals(r3.A0d)) {
            i = 10;
        }
        A0D.putExtra("extra_payments_entry_type", i);
        A0D.putExtra("extra_is_first_payment_method", true);
        A0D.putExtra("extra_skip_value_props_display", true);
        A0D.putExtra("extra_receiver_jid", C15380n4.A03(r3.A0C));
        r3.startActivity(A0D);
        r3.finish();
        r3.AaN();
    }
}
