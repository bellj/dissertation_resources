package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.4A6  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4A6 extends Enum {
    public static final /* synthetic */ AnonymousClass4A6[] A00;
    public static final AnonymousClass4A6 A01;
    public static final AnonymousClass4A6 A02;

    public static AnonymousClass4A6 valueOf(String str) {
        return (AnonymousClass4A6) Enum.valueOf(AnonymousClass4A6.class, str);
    }

    public static AnonymousClass4A6[] values() {
        return (AnonymousClass4A6[]) A00.clone();
    }

    static {
        AnonymousClass4A6 r4 = new AnonymousClass4A6("COROUTINE_SUSPENDED", 0);
        A01 = r4;
        AnonymousClass4A6 r3 = new AnonymousClass4A6("UNDECIDED", 1);
        A02 = r3;
        AnonymousClass4A6 r1 = new AnonymousClass4A6("RESUMED", 2);
        AnonymousClass4A6[] r0 = new AnonymousClass4A6[3];
        C12970iu.A1U(r4, r3, r0);
        r0[2] = r1;
        A00 = r0;
    }

    public AnonymousClass4A6(String str, int i) {
    }
}
