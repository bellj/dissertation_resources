package X;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.media.MediaPlayer;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import com.whatsapp.R;
import com.whatsapp.WaTextView;
import com.whatsapp.search.views.MessageThumbView;
import com.whatsapp.search.views.itemviews.MessageGifVideoPlayer;

/* renamed from: X.34v  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C621534v extends AnonymousClass34g {
    public AnimatorSet A00;
    public AnonymousClass018 A01;
    public AbstractC115385Ri A02 = new AnonymousClass59G(this);
    public boolean A03;
    public final WaTextView A04;
    public final MessageThumbView A05;
    public final MessageGifVideoPlayer A06;

    @Override // X.AbstractC84543zT
    public boolean A04() {
        return true;
    }

    @Override // X.AnonymousClass34g
    public float getRatio() {
        return 1.5f;
    }

    public C621534v(Context context) {
        super(context);
        A01();
        MessageThumbView messageThumbView = (MessageThumbView) AnonymousClass028.A0D(this, R.id.thumb_view);
        this.A05 = messageThumbView;
        MessageGifVideoPlayer messageGifVideoPlayer = (MessageGifVideoPlayer) AnonymousClass028.A0D(this, R.id.video_player);
        this.A06 = messageGifVideoPlayer;
        this.A04 = C12960it.A0N(this, R.id.media_time);
        C12960it.A0r(context, messageThumbView, R.string.gif_preview_description);
        messageGifVideoPlayer.A06 = this.A02;
    }

    public static /* synthetic */ void A00(C621534v r10, boolean z) {
        AnimatorSet animatorSet = r10.A00;
        if (animatorSet != null) {
            animatorSet.cancel();
        }
        float f = 0.0f;
        if (z) {
            f = 1.0f;
        }
        r10.A00 = new AnimatorSet();
        FrameLayout frameLayout = ((AnonymousClass34g) r10).A00;
        r10.A00.playTogether(ObjectAnimator.ofFloat(frameLayout, "alpha", frameLayout.getAlpha(), f), ObjectAnimator.ofFloat(((AnonymousClass34g) r10).A01, "alpha", frameLayout.getAlpha(), f));
        r10.A00.setInterpolator(new DecelerateInterpolator());
        r10.A00.setDuration(100L);
        r10.A00.start();
    }

    @Override // X.AbstractC74163hQ
    public void A01() {
        if (!this.A03) {
            this.A03 = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            ((AbstractC84543zT) this).A01 = C12960it.A0S(A00);
            this.A01 = C12960it.A0R(A00);
        }
    }

    @Override // X.AbstractC84543zT
    public void A03() {
        MessageGifVideoPlayer messageGifVideoPlayer = this.A06;
        messageGifVideoPlayer.setVisibility(8);
        MediaPlayer mediaPlayer = messageGifVideoPlayer.A02;
        if (mediaPlayer != null) {
            mediaPlayer.release();
            messageGifVideoPlayer.A02 = null;
            messageGifVideoPlayer.A0C = false;
            messageGifVideoPlayer.A0D = false;
            messageGifVideoPlayer.A0F = false;
        }
    }

    @Override // X.AnonymousClass34g
    public int getMark() {
        return R.drawable.msg_status_gif;
    }

    @Override // X.AnonymousClass34g
    public int getMarkTintColor() {
        return R.color.white;
    }

    public void setMessage(AnonymousClass1XR r4) {
        super.setMessage((AbstractC16130oV) r4);
        ((AbstractC84543zT) this).A00 = 0;
        setId(R.id.gif_grid);
        MessageThumbView messageThumbView = this.A05;
        messageThumbView.setMessage(r4);
        this.A06.setMessage(r4);
        messageThumbView.setVisibility(0);
        WaTextView waTextView = this.A04;
        C12990iw.A1G(waTextView);
        waTextView.setVisibility(8);
    }

    @Override // X.AbstractC84543zT
    public void setScrolling(boolean z) {
        this.A06.setScrolling(z);
    }

    @Override // X.AbstractC84543zT
    public void setShouldPlay(boolean z) {
        this.A06.setShouldPlay(z);
    }
}
