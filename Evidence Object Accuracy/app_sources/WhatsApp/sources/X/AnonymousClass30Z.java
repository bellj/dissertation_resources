package X;

/* renamed from: X.30Z  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass30Z extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Integer A02;
    public Integer A03;

    public AnonymousClass30Z() {
        super(3176, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
        r3.Abe(2, this.A01);
        r3.Abe(3, this.A02);
        r3.Abe(4, this.A03);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamE2eeSystemMessage {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "chatCategory", C12960it.A0Y(this.A00));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "e2eeDialogInteraction", C12960it.A0Y(this.A01));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "e2eeSystemMessageGroupSizeBucket", C12960it.A0Y(this.A02));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "e2eeSystemMessageType", C12960it.A0Y(this.A03));
        return C12960it.A0d("}", A0k);
    }
}
