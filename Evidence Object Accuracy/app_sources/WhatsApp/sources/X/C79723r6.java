package X;

import android.os.IBinder;

/* renamed from: X.3r6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79723r6 extends C98334iW implements AnonymousClass5YA {
    public C79723r6(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.location.internal.IFusedLocationProviderCallback");
    }
}
