package X;

import android.graphics.Outline;
import android.view.View;
import android.view.ViewOutlineProvider;

/* renamed from: X.3gp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73803gp extends ViewOutlineProvider {
    public final /* synthetic */ C64103Eg A00;
    public final /* synthetic */ C52342ae A01;

    public C73803gp(C64103Eg r1, C52342ae r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // android.view.ViewOutlineProvider
    public void getOutline(View view, Outline outline) {
        C64103Eg r0 = this.A00;
        outline.setOval(0, 0, r0.A05, r0.A04);
    }
}
