package X;

import android.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.wawebrtc.MediaCodecVideoEncoder;

/* renamed from: X.3n4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77303n4 extends AbstractC107784xw {
    public static final int[] A0I = {32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 225, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 233, 93, 237, 243, 250, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, C43951xu.A03, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 231, 247, 209, 241, 9632};
    public static final int[] A0J = {0, 4, 8, 12, 16, 20, 24, 28};
    public static final int[] A0K = {11, 1, 3, 12, 14, 5, 7, 9};
    public static final int[] A0L = {174, MediaCodecVideoEncoder.MIN_ENCODER_WIDTH, 189, 191, 8482, 162, 163, 9834, 224, 32, 232, 226, 234, 238, 244, 251};
    public static final int[] A0M = {193, 201, 211, 218, 220, 252, 8216, 161, 42, 39, 8212, 169, 8480, 8226, 8220, 8221, 192, 194, 199, 200, 202, 203, 235, 206, 207, 239, 212, 217, 249, 219, 171, 187};
    public static final int[] A0N = {195, 227, 205, 204, 236, 210, 242, 213, 245, 123, 125, 92, 94, 95, 124, 126, 196, 228, 214, 246, 223, 165, 164, 9474, 197, 229, 216, 248, 9484, 9488, 9492, 9496};
    public static final int[] A0O = {-1, -16711936, -16776961, -16711681, -65536, -256, -65281};
    public static final boolean[] A0P = {false, true, true, false, true, false, false, true, true, false, false, true, false, true, true, false, true, false, false, true, false, true, true, false, false, true, true, false, true, false, false, true, true, false, false, true, false, true, true, false, false, true, true, false, true, false, false, true, false, true, true, false, true, false, false, true, true, false, false, true, false, true, true, false, true, false, false, true, false, true, true, false, false, true, true, false, true, false, false, true, false, true, true, false, true, false, false, true, true, false, false, true, false, true, true, false, false, true, true, false, true, false, false, true, true, false, false, true, false, true, true, false, true, false, false, true, false, true, true, false, false, true, true, false, true, false, false, true, true, false, false, true, false, true, true, false, false, true, true, false, true, false, false, true, false, true, true, false, true, false, false, true, true, false, false, true, false, true, true, false, false, true, true, false, true, false, false, true, true, false, false, true, false, true, true, false, true, false, false, true, false, true, true, false, false, true, true, false, true, false, false, true, false, true, true, false, true, false, false, true, true, false, false, true, false, true, true, false, true, false, false, true, false, true, true, false, false, true, true, false, true, false, false, true, true, false, false, true, false, true, true, false, false, true, true, false, true, false, false, true, false, true, true, false, true, false, false, true, true, false, false, true, false, true, true, false};
    public byte A00;
    public byte A01;
    public int A02;
    public int A03;
    public int A04 = 0;
    public long A05;
    public AnonymousClass4Y2 A06 = new AnonymousClass4Y2(0, 4);
    public List A07;
    public List A08;
    public boolean A09;
    public boolean A0A;
    public boolean A0B;
    public final int A0C;
    public final int A0D;
    public final int A0E;
    public final long A0F = 16000000;
    public final C95304dT A0G = new C95304dT();
    public final ArrayList A0H = C12960it.A0l();

    public C77303n4(int i, String str) {
        this.A0C = "application/x-mp4-cea-608".equals(str) ? 2 : 3;
        if (i != 1) {
            if (i != 2) {
                if (i == 3) {
                    this.A0D = 0;
                } else if (i != 4) {
                    Log.w("Cea608Decoder", "Invalid channel. Defaulting to CC1.");
                } else {
                    this.A0D = 1;
                }
                this.A0E = 1;
                A04(0);
                A03();
                this.A0A = true;
                this.A05 = -9223372036854775807L;
            }
            this.A0D = 1;
            this.A0E = 0;
            A04(0);
            A03();
            this.A0A = true;
            this.A05 = -9223372036854775807L;
        }
        this.A0D = 0;
        this.A0E = 0;
        A04(0);
        A03();
        this.A0A = true;
        this.A05 = -9223372036854775807L;
    }

    @Override // X.AbstractC107784xw
    public AbstractC76773mC A00() {
        AbstractC76773mC A00 = super.A8o();
        if (A00 == null) {
            long j = this.A0F;
            if (j == -9223372036854775807L) {
                return null;
            }
            long j2 = this.A05;
            if (j2 == -9223372036854775807L || super.A00 - j2 < j || (A00 = (AbstractC76773mC) super.A04.pollFirst()) == null) {
                return null;
            }
            this.A07 = Collections.emptyList();
            this.A05 = -9223372036854775807L;
            List list = this.A07;
            this.A08 = list;
            C107694xn r2 = new C107694xn(list);
            long j3 = super.A00;
            A00.timeUs = j3;
            A00.A01 = r2;
            A00.A00 = j3;
        }
        return A00;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:142:0x0206, code lost:
        if (r0 != 3) goto L_0x0070;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x004a, code lost:
        if (r1[r2] == false) goto L_0x004c;
     */
    /* JADX WARNING: Removed duplicated region for block: B:184:0x0084 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:194:0x0011 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00b5  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0101  */
    @Override // X.AbstractC107784xw
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(X.C76743m8 r12) {
        /*
        // Method dump skipped, instructions count: 714
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C77303n4.A01(X.3m8):void");
    }

    public final List A02() {
        ArrayList arrayList = this.A0H;
        int size = arrayList.size();
        ArrayList A0w = C12980iv.A0w(size);
        int i = 2;
        for (int i2 = 0; i2 < size; i2++) {
            C93834ao A01 = ((AnonymousClass4Y2) arrayList.get(i2)).A01(Integer.MIN_VALUE);
            A0w.add(A01);
            if (A01 != null) {
                i = Math.min(i, A01.A08);
            }
        }
        ArrayList A0w2 = C12980iv.A0w(size);
        for (int i3 = 0; i3 < size; i3++) {
            C93834ao r1 = (C93834ao) A0w.get(i3);
            if (r1 != null) {
                if (r1.A08 != i) {
                    r1 = ((AnonymousClass4Y2) arrayList.get(i3)).A01(i);
                }
                A0w2.add(r1);
            }
        }
        return A0w2;
    }

    public final void A03() {
        AnonymousClass4Y2 r2 = this.A06;
        r2.A00 = this.A02;
        r2.A06.clear();
        r2.A07.clear();
        r2.A05.setLength(0);
        r2.A03 = 15;
        r2.A02 = 0;
        r2.A04 = 0;
        ArrayList arrayList = this.A0H;
        arrayList.clear();
        arrayList.add(this.A06);
    }

    public final void A04(int i) {
        int i2 = this.A02;
        if (i2 != i) {
            this.A02 = i;
            if (i == 3) {
                int i3 = 0;
                while (true) {
                    ArrayList arrayList = this.A0H;
                    if (i3 < arrayList.size()) {
                        ((AnonymousClass4Y2) arrayList.get(i3)).A00 = 3;
                        i3++;
                    } else {
                        return;
                    }
                }
            } else {
                A03();
                if (i2 == 3 || i == 1 || i == 0) {
                    this.A07 = Collections.emptyList();
                }
            }
        }
    }

    @Override // X.AbstractC107784xw, X.AnonymousClass5XF
    public void flush() {
        super.flush();
        this.A07 = null;
        this.A08 = null;
        A04(0);
        this.A03 = 4;
        this.A06.A01 = 4;
        A03();
        this.A09 = false;
        this.A0B = false;
        this.A00 = 0;
        this.A01 = 0;
        this.A04 = 0;
        this.A0A = true;
        this.A05 = -9223372036854775807L;
    }
}
