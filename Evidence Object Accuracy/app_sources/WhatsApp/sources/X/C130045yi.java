package X;

import android.util.Pair;

/* renamed from: X.5yi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130045yi {
    public final int A00;
    public final int A01;
    public final Pair A02;
    public final byte[] A03;
    public final float[] A04;
    public final AnonymousClass6MI[] A05;

    public C130045yi(Pair pair, byte[] bArr, float[] fArr, AnonymousClass6MI[] r4, int i, int i2) {
        this.A03 = bArr;
        this.A05 = r4;
        this.A04 = fArr;
        this.A02 = pair;
        this.A01 = i;
        this.A00 = i2;
    }

    public int A00() {
        return this.A00;
    }

    public int A01() {
        return this.A01;
    }

    public byte[] A02() {
        return this.A03;
    }

    public AnonymousClass6MI[] A03() {
        return this.A05;
    }
}
