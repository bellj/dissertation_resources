package X;

import android.content.Context;
import androidx.work.impl.WorkDatabase;
import java.util.List;

/* renamed from: X.0O4  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0O4 {
    public Context A00;
    public C05180Oo A01;
    public AnonymousClass0NN A02 = new AnonymousClass0NN();
    public WorkDatabase A03;
    public AbstractC11450gJ A04;
    public AbstractC11500gO A05;
    public String A06;
    public List A07;

    public AnonymousClass0O4(Context context, C05180Oo r3, WorkDatabase workDatabase, AbstractC11450gJ r5, AbstractC11500gO r6, String str) {
        this.A00 = context.getApplicationContext();
        this.A05 = r6;
        this.A04 = r5;
        this.A01 = r3;
        this.A03 = workDatabase;
        this.A06 = str;
    }
}
