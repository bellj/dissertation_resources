package X;

import android.text.Layout;
import com.facebook.redex.IDxComparatorShape3S0000000_2_I1;
import java.util.Comparator;

/* renamed from: X.4a5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C93384a5 {
    public static final Comparator A02 = new IDxComparatorShape3S0000000_2_I1(0);
    public final int A00;
    public final C93834ao A01;

    public C93384a5(Layout.Alignment alignment, CharSequence charSequence, float f, float f2, int i, int i2, int i3, int i4, boolean z) {
        C94144bK r1 = new C94144bK();
        r1.A0E = charSequence;
        r1.A0D = alignment;
        r1.A01 = f;
        r1.A07 = 0;
        r1.A06 = i;
        r1.A02 = f2;
        r1.A08 = i2;
        r1.A04 = -3.4028235E38f;
        if (z) {
            r1.A0B = i3;
            r1.A0F = true;
        }
        this.A01 = r1.A00();
        this.A00 = i4;
    }
}
