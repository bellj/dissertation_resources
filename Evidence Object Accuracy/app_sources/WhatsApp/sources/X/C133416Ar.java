package X;

import com.whatsapp.util.Log;

/* renamed from: X.6Ar  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133416Ar implements AnonymousClass6MV {
    public final /* synthetic */ C119785f6 A00;
    public final /* synthetic */ C118035bA A01;

    public C133416Ar(C119785f6 r1, C118035bA r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass6MV
    public void APo(C452120p r2) {
        Log.e("PAY: BrazilPayBloksActivity/provider key iq returned null");
    }

    @Override // X.AnonymousClass6MV
    public void AVE(AnonymousClass6B7 r4) {
        this.A01.A04(this.A00, r4.A05);
    }
}
