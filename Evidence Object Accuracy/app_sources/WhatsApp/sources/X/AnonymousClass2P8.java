package X;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.camera.overlays.AutofocusOverlay;
import com.whatsapp.camera.overlays.ShutterOverlay;
import com.whatsapp.camera.overlays.ZoomOverlay;

/* renamed from: X.2P8  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2P8 {
    public final View A00;
    public final AnonymousClass1s9 A01;
    public final AutofocusOverlay A02;
    public final ShutterOverlay A03;
    public final ZoomOverlay A04;

    public AnonymousClass2P8(ViewGroup viewGroup, AnonymousClass1s9 r5) {
        this.A01 = r5;
        this.A00 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.camera_overlays, viewGroup, true);
        this.A02 = (AutofocusOverlay) AnonymousClass028.A0D(viewGroup, R.id.autofocus_overlay);
        this.A04 = (ZoomOverlay) AnonymousClass028.A0D(viewGroup, R.id.zoom_overlay);
        this.A03 = (ShutterOverlay) AnonymousClass028.A0D(viewGroup, R.id.shutter_overlay);
    }
}
