package X;

import android.view.View;
import com.whatsapp.conversation.conversationrow.NativeFlowButtonsRowContentLayout;

/* renamed from: X.3hs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74373hs extends AnonymousClass04v {
    public final /* synthetic */ NativeFlowButtonsRowContentLayout A00;
    public final /* synthetic */ String A01;

    public C74373hs(NativeFlowButtonsRowContentLayout nativeFlowButtonsRowContentLayout, String str) {
        this.A00 = nativeFlowButtonsRowContentLayout;
        this.A01 = str;
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r5) {
        super.A06(view, r5);
        r5.A09(new C007804a(16, this.A01));
    }
}
