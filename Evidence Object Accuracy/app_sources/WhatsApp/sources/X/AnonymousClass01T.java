package X;

/* renamed from: X.01T  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass01T {
    public final Object A00;
    public final Object A01;

    public AnonymousClass01T(Object obj, Object obj2) {
        this.A00 = obj;
        this.A01 = obj2;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof AnonymousClass01T)) {
            return false;
        }
        AnonymousClass01T r4 = (AnonymousClass01T) obj;
        if (!C015407h.A01(r4.A00, this.A00) || !C015407h.A01(r4.A01, this.A01)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        Object obj = this.A00;
        int i = 0;
        int hashCode = obj == null ? 0 : obj.hashCode();
        Object obj2 = this.A01;
        if (obj2 != null) {
            i = obj2.hashCode();
        }
        return hashCode ^ i;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Pair{");
        sb.append(this.A00);
        sb.append(" ");
        sb.append(this.A01);
        sb.append("}");
        return sb.toString();
    }
}
