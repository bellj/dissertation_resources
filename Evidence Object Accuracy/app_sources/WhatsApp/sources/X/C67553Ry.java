package X;

import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.3Ry  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67553Ry implements AbstractC12640iF {
    public int A00 = -1;
    public final RecyclerView A01;
    public final C54532gs A02;

    public C67553Ry(RecyclerView recyclerView, C54532gs r3) {
        this.A02 = r3;
        this.A01 = recyclerView;
    }

    @Override // X.AbstractC12640iF
    public void ANr(Object obj, int i, int i2) {
        ((AnonymousClass02M) this.A02).A01.A04(obj, i, i2);
    }

    @Override // X.AbstractC12640iF
    public void ARP(int i, int i2) {
        int i3 = this.A00;
        if (i3 == -1 || i3 > i) {
            this.A00 = i;
            this.A01.A0Y(0);
        }
        ((AnonymousClass02M) this.A02).A01.A02(i, i2);
    }

    @Override // X.AbstractC12640iF
    public void ASr(int i, int i2) {
        ((AnonymousClass02M) this.A02).A01.A01(i, i2);
    }

    @Override // X.AbstractC12640iF
    public void AUs(int i, int i2) {
        ((AnonymousClass02M) this.A02).A01.A03(i, i2);
    }
}
