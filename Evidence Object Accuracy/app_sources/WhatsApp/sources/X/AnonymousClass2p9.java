package X;

import android.content.Context;
import com.whatsapp.RollingCounterView;

/* renamed from: X.2p9  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2p9 extends RollingCounterView implements AnonymousClass004 {
    public AnonymousClass018 A00;
    public AnonymousClass2P7 A01;
    public boolean A02;

    public AnonymousClass2p9(Context context) {
        super(context);
        if (!this.A02) {
            this.A02 = true;
            this.A00 = C12960it.A0R(AnonymousClass2P6.A00(generatedComponent()));
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A01;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A01 = r0;
        }
        return r0.generatedComponent();
    }
}
