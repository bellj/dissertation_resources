package X;

import androidx.core.view.inputmethod.InputContentInfoCompat;
import com.whatsapp.util.Log;

/* renamed from: X.3Gt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64753Gt {
    public static final String[] A01 = {"image/gif", "video/x.looping_mp4", "image/jpeg", "image/jpg", "image/png", "image/webp.wasticker"};
    public InputContentInfoCompat A00;

    /* JADX INFO: finally extract failed */
    public C90044Mj A00(InputContentInfoCompat inputContentInfoCompat, int i) {
        try {
            try {
                InputContentInfoCompat inputContentInfoCompat2 = this.A00;
                if (inputContentInfoCompat2 != null) {
                    inputContentInfoCompat2.releasePermission();
                }
            } catch (Exception e) {
                Log.e("conversation/InputContentInfoCompat#releasePermission() failed.", e);
            }
            this.A00 = null;
            String[] strArr = A01;
            for (String str : strArr) {
                if (inputContentInfoCompat.getDescription().hasMimeType(str)) {
                    if ((i & 1) != 0) {
                        try {
                            inputContentInfoCompat.requestPermission();
                        } catch (Exception e2) {
                            Log.e("conversation/InputContentInfoCompat#requestPermission() failed.", e2);
                            return null;
                        }
                    }
                    Log.i(C12960it.A0d(inputContentInfoCompat.getContentUri().toString(), C12960it.A0k("conversation/onCommitContent: ")));
                    this.A00 = inputContentInfoCompat;
                    return new C90044Mj(str, inputContentInfoCompat.getContentUri());
                }
            }
            return null;
        } catch (Throwable th) {
            this.A00 = null;
            throw th;
        }
    }
}
