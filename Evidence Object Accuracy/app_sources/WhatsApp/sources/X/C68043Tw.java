package X;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import com.whatsapp.R;

/* renamed from: X.3Tw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68043Tw implements AbstractC116635Wf {
    public View A00;
    public final C53042cM A01;
    public final C18850tA A02;
    public final C14850m9 A03;

    public C68043Tw(C53042cM r1, C18850tA r2, C14850m9 r3) {
        this.A03 = r3;
        this.A02 = r2;
        this.A01 = r1;
    }

    public final View A00() {
        View view = this.A00;
        if (view != null) {
            return view;
        }
        C53042cM r3 = this.A01;
        Activity A01 = AnonymousClass12P.A01(r3.getContext(), ActivityC000900k.class);
        View A0F = C12960it.A0F(LayoutInflater.from(A01), r3, R.layout.app_state_sync_fatal_banner);
        this.A00 = A0F;
        C12960it.A10(A0F, A01, 28);
        r3.addView(this.A00);
        return this.A00;
    }

    @Override // X.AbstractC116635Wf
    public void AIR() {
        C12970iu.A1G(this.A00);
    }

    @Override // X.AbstractC116635Wf
    public boolean AdK() {
        if (!this.A03.A07(623)) {
            return false;
        }
        C18850tA r1 = this.A02;
        if (!r1.A0Z.A03()) {
            r1.A04.A08();
            return false;
        } else if (r1.A0R()) {
            return true;
        } else {
            return false;
        }
    }

    @Override // X.AbstractC116635Wf
    public void AfF() {
        View A00;
        int i;
        if (AdK()) {
            A00 = A00();
            i = 0;
        } else if (this.A00 != null) {
            A00 = A00();
            i = 8;
        } else {
            return;
        }
        A00.setVisibility(i);
    }
}
