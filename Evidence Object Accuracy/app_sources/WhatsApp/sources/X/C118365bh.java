package X;

import android.os.Bundle;

/* renamed from: X.5bh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118365bh extends AnonymousClass0Yo {
    public final /* synthetic */ Bundle A00;
    public final /* synthetic */ C128375w0 A01;

    public C118365bh(Bundle bundle, C128375w0 r2) {
        this.A01 = r2;
        this.A00 = bundle;
    }

    @Override // X.AnonymousClass0Yo, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.isAssignableFrom(C118125bJ.class)) {
            Bundle bundle = this.A00;
            C128375w0 r0 = this.A01;
            C130155yt r3 = r0.A0W;
            C130125yq r4 = r0.A0a;
            AnonymousClass61F r6 = r0.A0d;
            return new C118125bJ(bundle, r0.A03, r3, r4, r0.A0c, r6, r0.A0v);
        }
        throw C12970iu.A0f("Invalid viewModel for NoviTextInputStepUpViewModel");
    }
}
