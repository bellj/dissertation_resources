package X;

import android.content.Context;
import com.facebook.soloader.SoLoader;
import com.facebook.superpack.AssetDecompressor;
import com.whatsapp.AbstractAppShellDelegate;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.0wi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21030wi {
    public static final Integer[] A08 = {0, 1};
    public String A00;
    public String A01;
    public boolean A02 = false;
    public final AnonymousClass12A A03;
    public final AbstractC14440lR A04;
    public final HashMap A05 = new HashMap();
    public final Set A06 = new HashSet();
    public final AtomicBoolean A07 = new AtomicBoolean(true);

    public C21030wi(AnonymousClass12A r3, AbstractC14440lR r4) {
        this.A04 = r4;
        this.A03 = r3;
    }

    public final String A00(int i) {
        if (i == 0) {
            String str = this.A00;
            if (str != null) {
                return str;
            }
            String str2 = new String[]{"arm64-v8a", "armeabi-v7a", "x86", "x86_64"}[AssetDecompressor.get_architecture()];
            this.A00 = str2;
            return str2;
        } else if (i == 1) {
            return "strings";
        } else {
            StringBuilder sb = new StringBuilder("Compressed folder not explicitly specified for assetType: ");
            sb.append(i);
            throw new RuntimeException(sb.toString());
        }
    }

    public void A01(Context context) {
        boolean z = true;
        AnonymousClass009.A0F(!"2.22.17.70".isEmpty());
        int i = AssetDecompressor.get_architecture();
        StringBuilder sb = new StringBuilder();
        sb.append("2.22.17.70");
        sb.append(":");
        sb.append(new String[]{"arm64-v8a", "armeabi-v7a", "x86", "x86_64"}[i]);
        sb.append(":");
        sb.append(new File(context.getPackageCodePath()).lastModified() / 1000);
        this.A01 = sb.toString();
        this.A02 = true;
        AnonymousClass12A r3 = this.A03;
        if (!(i == 0 || i == 3)) {
            z = false;
        }
        r3.A01(new File(context.getFilesDir(), "decompressed/libs.spk.zst").getAbsolutePath(), z);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x007d, code lost:
        if (r0 == false) goto L_0x007f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A02(android.content.Context r8, java.lang.String r9) {
        /*
            r7 = this;
            java.lang.Boolean r2 = java.lang.Boolean.TRUE
            java.util.HashMap r4 = r7.A05
            java.lang.Object r0 = r4.get(r9)
            boolean r0 = r2.equals(r0)
            r1 = 1
            if (r0 != 0) goto L_0x0019
            boolean r0 = r7.A02
            if (r0 != 0) goto L_0x001a
            java.lang.Boolean r0 = java.lang.Boolean.FALSE
            r4.put(r9, r0)
            r1 = 0
        L_0x0019:
            return r1
        L_0x001a:
            java.util.Set r0 = r7.A06
            boolean r0 = r0.contains(r9)
            if (r0 == 0) goto L_0x0026
            r4.put(r9, r2)
            return r1
        L_0x0026:
            java.io.File r2 = r8.getFilesDir()     // Catch: IOException -> 0x006e
            java.lang.String r1 = "decompressed"
            java.io.File r0 = new java.io.File     // Catch: IOException -> 0x006e
            r0.<init>(r2, r1)     // Catch: IOException -> 0x006e
            java.io.File r1 = new java.io.File     // Catch: IOException -> 0x006e
            r1.<init>(r0, r9)     // Catch: IOException -> 0x006e
            java.lang.String r0 = ".superpack_version"
            java.io.File r6 = new java.io.File     // Catch: IOException -> 0x006e
            r6.<init>(r1, r0)     // Catch: IOException -> 0x006e
            long r1 = r6.length()     // Catch: IOException -> 0x006e
            int r0 = (int) r1     // Catch: IOException -> 0x006e
            java.io.ByteArrayOutputStream r5 = new java.io.ByteArrayOutputStream     // Catch: IOException -> 0x006e
            r5.<init>(r0)     // Catch: IOException -> 0x006e
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch: IOException -> 0x006e
            r3.<init>(r6)     // Catch: IOException -> 0x006e
            r0 = 1024(0x400, float:1.435E-42)
            byte[] r2 = new byte[r0]     // Catch: all -> 0x0069
        L_0x0050:
            int r1 = r3.read(r2)     // Catch: all -> 0x0069
            r0 = -1
            if (r1 == r0) goto L_0x005c
            r0 = 0
            r5.write(r2, r0, r1)     // Catch: all -> 0x0069
            goto L_0x0050
        L_0x005c:
            r3.close()     // Catch: IOException -> 0x006e
            byte[] r0 = r5.toByteArray()     // Catch: IOException -> 0x006e
            java.lang.String r1 = new java.lang.String     // Catch: IOException -> 0x006e
            r1.<init>(r0)     // Catch: IOException -> 0x006e
            goto L_0x0070
        L_0x0069:
            r0 = move-exception
            r3.close()     // Catch: all -> 0x006d
        L_0x006d:
            throw r0     // Catch: IOException -> 0x006e
        L_0x006e:
            java.lang.String r1 = ""
        L_0x0070:
            boolean r0 = r1.isEmpty()
            if (r0 != 0) goto L_0x007f
            java.lang.String r0 = r7.A01
            boolean r0 = r1.equals(r0)
            r1 = 1
            if (r0 != 0) goto L_0x0080
        L_0x007f:
            r1 = 0
        L_0x0080:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r1)
            r4.put(r9, r0)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C21030wi.A02(android.content.Context, java.lang.String):boolean");
    }

    public boolean A03(Context context, String str) {
        StringBuilder sb = new StringBuilder("lib");
        sb.append(str);
        sb.append(".so");
        if (!new File(new File(new File(context.getFilesDir(), "decompressed"), AbstractAppShellDelegate.COMPRESSED_LIBS_ARCHIVE_NAME), sb.toString()).exists()) {
            StringBuilder sb2 = new StringBuilder("whatsappassetdecompressor/load-library-from-archive File missing: ");
            sb2.append(str);
            Log.w(sb2.toString());
            return false;
        }
        try {
            return SoLoader.A04(str);
        } catch (UnsatisfiedLinkError e) {
            StringBuilder sb3 = new StringBuilder("whatsappassetdecompressor/load-library-from-archive error: ");
            sb3.append(str);
            Log.w(sb3.toString(), e);
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x008b, code lost:
        if (r7.contains("libvlc.so") != false) goto L_0x008d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A04(android.content.Context r10, java.lang.String r11, int r12, boolean r13, boolean r14) {
        /*
        // Method dump skipped, instructions count: 281
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C21030wi.A04(android.content.Context, java.lang.String, int, boolean, boolean):boolean");
    }
}
