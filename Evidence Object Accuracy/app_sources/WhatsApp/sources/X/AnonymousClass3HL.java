package X;

import android.content.Context;
import android.os.Looper;
import android.view.View;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.3HL  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3HL {
    public static final AtomicInteger A0E = new AtomicInteger(0);
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public AnonymousClass3IP A05;
    public AnonymousClass3D8 A06;
    public AnonymousClass5SD A07;
    public AnonymousClass3H1 A08;
    public AnonymousClass4SN A09;
    public final Context A0A;
    public final HandlerC52012a0 A0B = new HandlerC52012a0(Looper.getMainLooper(), this);
    public final AnonymousClass4MY A0C;
    public final Object A0D;

    public AnonymousClass3HL(Context context, AnonymousClass4MY r4, Object obj) {
        A0E.incrementAndGet();
        this.A01 = -1;
        this.A03 = 0;
        this.A00 = -1;
        this.A04 = -1;
        this.A02 = -1;
        this.A0A = context;
        this.A0C = r4;
        this.A0D = obj;
    }

    public final void A00() {
        AnonymousClass3H1 r2;
        String str;
        String str2;
        synchronized (this) {
            AnonymousClass4MY r1 = this.A0C;
            C89024Ij r0 = (C89024Ij) this.A05.A04;
            r1.A00 = r0;
            C14270l8 r12 = r1.A01;
            AnonymousClass4TT r22 = r0.A00;
            AnonymousClass3J3.A02("Committing Variables and Bound tree is only supported from the UI Thread");
            r12.A02 = r22;
            r12.A08 = r22.A06;
            C14260l7 r6 = (C14260l7) r12.A07.get();
            Iterator it = r12.A0G.iterator();
            if (it.hasNext()) {
                it.next();
                throw C12980iv.A0n("onCommit");
            }
            if (r6 != null) {
                for (AnonymousClass4PY r13 : r22.A03) {
                    AnonymousClass28D r02 = r13.A01;
                    Object A04 = AnonymousClass3JV.A04(r6, r02);
                    if (A04 == null) {
                        str = "BloksTreeManager";
                        str2 = "Binding was targeting a controller but the returned controller was null";
                    } else {
                        int i = r02.A01;
                        int i2 = r13.A00;
                        Object obj = r13.A02;
                        if (i == 13688) {
                            C67833Tb r4 = (C67833Tb) A04;
                            if (obj == null) {
                                str = "ViewTransformsBindControllerOverride";
                                str2 = "Trying to set null value for a view transform property";
                            } else {
                                r4.Abl(null, obj, i2);
                            }
                        } else {
                            Object[] A1b = C12970iu.A1b();
                            C12960it.A1P(A1b, i, 0);
                            throw C12970iu.A0f(String.format("No implementation bound to key: %s", A1b));
                        }
                    }
                    C28691Op.A00(str, str2);
                }
            }
            r2 = this.A05.A02;
            this.A08 = r2;
        }
        AnonymousClass4SN r14 = this.A09;
        if (!(r14 == null || r14.A01 == r2)) {
            if (r2 == null) {
                r14.A04.A09();
            }
            r14.A01 = r2;
            r14.A03.requestLayout();
        }
    }

    public void A01(int i, int[] iArr, int i2) {
        int i3;
        AnonymousClass3D8 r8;
        boolean z;
        int i4;
        int i5;
        AnonymousClass3H1 r5 = this.A08;
        if (r5 == null || !AnonymousClass4DA.A00(r5.A01, i, r5.A03.A04.width()) || !AnonymousClass4DA.A00(r5.A00, i2, r5.A03.A04.height())) {
            synchronized (this) {
                this.A04 = i;
                this.A02 = i2;
                AnonymousClass3IP r0 = this.A05;
                if (r0 != null) {
                    AnonymousClass3H1 r52 = r0.A02;
                    if (AnonymousClass4DA.A00(r52.A01, i, r52.A03.A04.width()) && AnonymousClass4DA.A00(r52.A00, i2, r52.A03.A04.height()) && iArr != null) {
                        iArr[0] = this.A05.A02.A03.A04.width();
                        iArr[1] = this.A05.A02.A03.A04.height();
                        return;
                    }
                }
                if (this.A07 == null) {
                    if (iArr != null) {
                        iArr[0] = 0;
                        iArr[1] = 0;
                    }
                    return;
                }
                AnonymousClass3D8 r2 = this.A06;
                if (r2 == null || !(((i4 = r2.A02) == i || (View.MeasureSpec.getMode(i4) == 0 && View.MeasureSpec.getMode(i) == 0)) && ((i5 = r2.A00) == i2 || (View.MeasureSpec.getMode(i5) == 0 && View.MeasureSpec.getMode(i2) == 0)))) {
                    i3 = this.A03;
                    this.A03 = i3 + 1;
                    r8 = new AnonymousClass3D8(this.A0A, this.A05, this.A07, this.A0D, i3, this.A04, this.A02);
                    this.A06 = r8;
                } else {
                    r8 = this.A06;
                    i3 = r8.A01;
                }
                AnonymousClass3IP A00 = r8.A00();
                synchronized (this) {
                    if (i3 > this.A00) {
                        this.A00 = i3;
                        this.A05 = A00;
                        z = true;
                    } else {
                        z = false;
                    }
                    if (this.A06 == r8) {
                        this.A06 = null;
                    }
                    if (iArr != null) {
                        iArr[0] = this.A05.A02.A03.A04.width();
                        iArr[1] = this.A05.A02.A03.A04.height();
                    }
                }
                if (!z) {
                    return;
                }
                if (AnonymousClass3J3.A03()) {
                    A00();
                    return;
                }
                HandlerC52012a0 r22 = this.A0B;
                if (!r22.hasMessages(99)) {
                    r22.sendEmptyMessage(99);
                }
            }
        } else if (iArr != null) {
            iArr[0] = this.A08.A03.A04.width();
            iArr[1] = this.A08.A03.A04.height();
        }
    }
}
