package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4l0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C99874l0 implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new AnonymousClass20A(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new AnonymousClass20A[i];
    }
}
