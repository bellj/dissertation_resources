package X;

import android.view.inputmethod.InputMethodManager;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.3j4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74963j4 extends AbstractC05270Ox {
    public final /* synthetic */ C14260l7 A00;

    public C74963j4(C14260l7 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC05270Ox
    public void A00(RecyclerView recyclerView, int i) {
        if (i == 1) {
            ((InputMethodManager) this.A00.A00.getSystemService("input_method")).hideSoftInputFromWindow(recyclerView.getWindowToken(), 0);
        }
    }
}
