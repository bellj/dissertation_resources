package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiDeviceBindStepActivity;
import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5Zs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117385Zs extends BroadcastReceiver {
    public final Object A00 = C12970iu.A0l();
    public volatile boolean A01 = false;
    public final /* synthetic */ IndiaUpiDeviceBindStepActivity A02;

    public C117385Zs(IndiaUpiDeviceBindStepActivity indiaUpiDeviceBindStepActivity) {
        this.A02 = indiaUpiDeviceBindStepActivity;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        AnonymousClass1ZR A0I;
        if (!this.A01) {
            synchronized (this.A00) {
                if (!this.A01) {
                    AnonymousClass22D.A00(context);
                    this.A01 = true;
                }
            }
        }
        int resultCode = getResultCode();
        IndiaUpiDeviceBindStepActivity indiaUpiDeviceBindStepActivity = this.A02;
        C30931Zj r4 = indiaUpiDeviceBindStepActivity.A0W;
        r4.A06(C12960it.A0W(resultCode, "SmsSentReceiver onReceive: "));
        if (resultCode == -1) {
            indiaUpiDeviceBindStepActivity.A3C(true);
            indiaUpiDeviceBindStepActivity.A0J.A00.A07("smsSend");
            indiaUpiDeviceBindStepActivity.A0J.A00.A08("deviceBind");
            String A06 = ((AbstractActivityC121665jA) indiaUpiDeviceBindStepActivity).A0A.A06(indiaUpiDeviceBindStepActivity.A0A);
            C1329668y r5 = ((AbstractActivityC121665jA) indiaUpiDeviceBindStepActivity).A0B;
            synchronized (r5) {
                String str = null;
                try {
                    String A04 = r5.A03.A04();
                    if (!TextUtils.isEmpty(A04)) {
                        str = C13000ix.A05(A04).optString("smsVerifDataGen", null);
                    }
                } catch (JSONException e) {
                    Log.w("PAY: IndiaUpiPaymentSharedPrefs readSmsVerificationDataGenerated threw: ", e);
                }
                A0I = C117305Zk.A0I(C117305Zk.A0J(), String.class, str, "smsVerificationDataGen");
            }
            String A2p = indiaUpiDeviceBindStepActivity.A2p(A06, (String) A0I.A00);
            String A05 = ((AbstractActivityC121665jA) indiaUpiDeviceBindStepActivity).A0A.A05(indiaUpiDeviceBindStepActivity.A0A);
            C1329668y r8 = ((AbstractActivityC121665jA) indiaUpiDeviceBindStepActivity).A0B;
            synchronized (r8) {
                AnonymousClass009.A05(A05);
                AnonymousClass009.A05(A2p);
                try {
                    C18600si r7 = r8.A03;
                    JSONObject A0b = C117295Zj.A0b(r7);
                    A0b.put("v", "2");
                    JSONObject A01 = C1329668y.A01(A05, "smsVerifDataSentToPsp", A0b);
                    if (A01 != null) {
                        A01.put("smsVerifData", A2p);
                    }
                    C117295Zj.A1E(r7, A0b);
                } catch (JSONException e2) {
                    Log.w("PAY: IndiaUpiPaymentSharedPrefs storeSmsVerificationDataSent threw: ", e2);
                }
            }
            C64513Fv r1 = indiaUpiDeviceBindStepActivity.A0C;
            if (r1 != null) {
                r1.A05("device-binding-sms");
            }
            StringBuilder A0k = C12960it.A0k("IndiaUpiPaymentBankSetupActivity: onSmsSent to psp: ");
            A0k.append(A05);
            A0k.append(" storing verification data sent: ");
            r4.A06(C12960it.A0d(C1309060l.A00(A2p), A0k));
            if (!TextUtils.isEmpty(A2p)) {
                ((ActivityC13810kN) indiaUpiDeviceBindStepActivity).A05.A0H(new Runnable() { // from class: X.6Fz
                    @Override // java.lang.Runnable
                    public final void run() {
                        IndiaUpiDeviceBindStepActivity indiaUpiDeviceBindStepActivity2 = IndiaUpiDeviceBindStepActivity.this;
                        indiaUpiDeviceBindStepActivity2.A0P = "2";
                        indiaUpiDeviceBindStepActivity2.markStepDone(indiaUpiDeviceBindStepActivity2.A05);
                        indiaUpiDeviceBindStepActivity2.markStepProcessing(indiaUpiDeviceBindStepActivity2.A03);
                        indiaUpiDeviceBindStepActivity2.markStepDisabled(indiaUpiDeviceBindStepActivity2.A04);
                        C12990iw.A0x(indiaUpiDeviceBindStepActivity2, indiaUpiDeviceBindStepActivity2.A06, R.drawable.ic_verify_bank);
                    }
                });
                indiaUpiDeviceBindStepActivity.A0T = true;
                indiaUpiDeviceBindStepActivity.A0E.A00();
                ((AbstractActivityC121665jA) indiaUpiDeviceBindStepActivity).A0D.AeG();
                return;
            }
            return;
        }
        C64513Fv r12 = indiaUpiDeviceBindStepActivity.A0C;
        if (r12 != null) {
            r12.A06("device-binding-sms", resultCode);
        }
        indiaUpiDeviceBindStepActivity.A3C(false);
        AbstractActivityC119235dO.A1h(indiaUpiDeviceBindStepActivity, R.string.payments_error_sms, true);
    }
}
