package X;

import java.util.Comparator;

/* renamed from: X.3cj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C71313cj implements Comparator {
    public final C15450nH A00;
    public final C15550nR A01;
    public final C15610nY A02;

    public C71313cj(C15450nH r1, C15550nR r2, C15610nY r3) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    /* renamed from: A00 */
    public int compare(AnonymousClass1YV r8, AnonymousClass1YV r9) {
        C15550nR r1 = this.A01;
        C15370n3 A0B = r1.A0B(r8.A02);
        C15370n3 A0B2 = r1.A0B(r9.A02);
        C28811Pc r4 = A0B.A0C;
        boolean z = false;
        boolean A1W = C12960it.A1W(r4);
        if (A0B2.A0C != null) {
            z = true;
        }
        if (A1W == z) {
            C15610nY r0 = this.A02;
            String A04 = r0.A04(A0B);
            String A042 = r0.A04(A0B2);
            if (A04 == null) {
                return -1;
            }
            if (A042 != null) {
                return A04.compareTo(A042);
            }
            return 1;
        } else if (r4 != null) {
            return -1;
        } else {
            return 1;
        }
    }
}
