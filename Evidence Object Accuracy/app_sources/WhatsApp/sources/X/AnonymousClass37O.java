package X;

import java.lang.ref.WeakReference;

/* renamed from: X.37O  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass37O extends AbstractC16350or {
    public final int A00;
    public final C43851xh A01;
    public final WeakReference A02;

    public /* synthetic */ AnonymousClass37O(C43851xh r2, AnonymousClass36P r3) {
        this.A01 = r2;
        this.A02 = C12970iu.A10(r3);
        this.A00 = r3.getTargetIconSize();
    }
}
