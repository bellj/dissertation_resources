package X;

/* renamed from: X.43Y  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass43Y extends AbstractC16110oT {
    public Boolean A00;
    public Long A01;
    public String A02;

    public AnonymousClass43Y() {
        super(2956, AbstractC16110oT.A00(), 2, 42196056);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(2, this.A00);
        r3.Abe(3, this.A02);
        r3.Abe(1, this.A01);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamTestAnonymousWeeklyId {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "psTestBooleanField", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "psTestStringField", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "psTimeSinceLastEventInMin", this.A01);
        return C12960it.A0d("}", A0k);
    }
}
