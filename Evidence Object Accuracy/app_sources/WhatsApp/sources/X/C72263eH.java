package X;

import com.whatsapp.catalogcategory.view.viewmodel.CatalogAllCategoryViewModel;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.3eH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C72263eH extends AnonymousClass1WI implements AnonymousClass1J7 {
    public final /* synthetic */ UserJid $bizJid;
    public final /* synthetic */ AnonymousClass4A0 $displayContext;
    public final /* synthetic */ CatalogAllCategoryViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C72263eH(AnonymousClass4A0 r2, CatalogAllCategoryViewModel catalogAllCategoryViewModel, UserJid userJid) {
        super(1);
        this.this$0 = catalogAllCategoryViewModel;
        this.$bizJid = userJid;
        this.$displayContext = r2;
    }

    @Override // X.AnonymousClass1J7
    public /* bridge */ /* synthetic */ Object AJ4(Object obj) {
        Object obj2;
        AnonymousClass4K6 r8 = (AnonymousClass4K6) obj;
        C16700pc.A0E(r8, 0);
        if (r8 instanceof C60252wM) {
            List<AnonymousClass4SX> list = ((C60252wM) r8).A01;
            C12990iw.A0P(this.this$0.A08).A0A(C12960it.A0V());
            UserJid userJid = this.$bizJid;
            AnonymousClass4A0 r4 = this.$displayContext;
            ArrayList A0E = C16760pi.A0E(list);
            for (AnonymousClass4SX r1 : list) {
                switch (r4.ordinal()) {
                    case 0:
                        obj2 = new AnonymousClass2wU(r1, userJid);
                        break;
                    case 1:
                        obj2 = new AnonymousClass2wT(r1, userJid);
                        break;
                    default:
                        throw C12990iw.A0v();
                }
                A0E.add(obj2);
            }
            if (this.$displayContext.ordinal() == 1) {
                A0E = C12980iv.A0x(A0E);
                A0E.add(0, new C849840r());
            }
            C12990iw.A0P(this.this$0.A07).A0A(A0E);
        } else {
            C12990iw.A0P(this.this$0.A08).A0A(C12970iu.A0g());
        }
        return AnonymousClass1WZ.A00;
    }
}
