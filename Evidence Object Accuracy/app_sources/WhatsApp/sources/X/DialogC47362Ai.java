package X;

import android.app.Dialog;
import android.os.Bundle;
import android.text.SpannedString;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.redex.RunnableBRunnable0Shape8S0100000_I0_8;
import com.whatsapp.KeyboardPopupLayout;
import com.whatsapp.R;
import com.whatsapp.WaImageButton;
import com.whatsapp.emoji.search.EmojiSearchContainer;
import com.whatsapp.jid.Jid;
import com.whatsapp.mediacomposer.bottombar.caption.CaptionView;
import com.whatsapp.mediacomposer.bottombar.recipients.RecipientsView;
import com.whatsapp.mentions.MentionableEntry;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape15S0100000_I0_2;
import com.whatsapp.util.ViewOnClickCListenerShape5S0200000_I1;
import java.util.List;

/* renamed from: X.2Ai  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class DialogC47362Ai extends Dialog implements AnonymousClass2Aj, AbstractC33441e5, AbstractC47372Ak {
    public int A00;
    public C15270mq A01;
    public C15330mx A02;
    public AnonymousClass3DD A03;
    public AnonymousClass4QP A04;
    public C63763Cv A05;
    public AnonymousClass3F8 A06;
    public CharSequence A07;
    public boolean A08;
    public boolean A09;
    public boolean A0A;
    public boolean A0B;
    public final AbstractC15710nm A0C;
    public final ActivityC13810kN A0D;
    public final AbstractC116455Vm A0E = new AnonymousClass3UG(this);
    public final C15610nY A0F;
    public final AnonymousClass01d A0G;
    public final C14820m6 A0H;
    public final AnonymousClass018 A0I;
    public final C18470sV A0J;
    public final AnonymousClass19M A0K;
    public final C231510o A0L;
    public final AnonymousClass193 A0M;
    public final C14850m9 A0N;
    public final C453321d A0O;
    public final C16630pM A0P;
    public final C252718t A0Q;
    public final AnonymousClass01H A0R;
    public final List A0S;
    public final boolean A0T;

    @Override // X.AnonymousClass2Aj
    public /* synthetic */ void ALw() {
    }

    public DialogC47362Ai(AbstractC15710nm r2, ActivityC13810kN r3, C15610nY r4, AnonymousClass01d r5, C14820m6 r6, AnonymousClass018 r7, C18470sV r8, AnonymousClass19M r9, C231510o r10, AnonymousClass193 r11, C14850m9 r12, C453321d r13, C16630pM r14, C252718t r15, AnonymousClass01H r16, CharSequence charSequence, List list, int i, boolean z, boolean z2) {
        super(r3, R.style.DoodleTextDialog);
        this.A0S = list;
        this.A07 = charSequence;
        this.A00 = i;
        this.A09 = z;
        this.A0D = r3;
        this.A0N = r12;
        this.A0Q = r15;
        this.A0C = r2;
        this.A0K = r9;
        this.A0J = r8;
        this.A0L = r10;
        this.A0G = r5;
        this.A0F = r4;
        this.A0I = r7;
        this.A0M = r11;
        this.A0H = r6;
        this.A0O = r13;
        this.A0P = r14;
        this.A0R = r16;
        this.A0T = z2;
    }

    @Override // X.AbstractC33441e5
    public void AUk(boolean z) {
        this.A08 = true;
        this.A0B = z;
        onDismiss();
    }

    @Override // X.AnonymousClass2Aj
    public void AYN() {
        C453321d r3 = this.A0O;
        int intValue = ((Number) r3.A05.A01()).intValue();
        if (intValue == 2) {
            r3.A06(3);
        } else if (intValue == 3) {
            r3.A06(2);
        }
    }

    @Override // android.app.Dialog
    public void onCreate(Bundle bundle) {
        Jid jid;
        super.onCreate(bundle);
        AnonymousClass018 r4 = this.A0I;
        C42941w9.A0B(getWindow(), r4);
        boolean z = this.A0T;
        int i = R.layout.capture_send_dialog;
        if (z) {
            i = R.layout.new_capture_send_dialog;
        }
        setContentView(i);
        View A00 = AnonymousClass0KS.A00(this, R.id.main);
        AnonymousClass19M r0 = this.A0K;
        AnonymousClass01d r02 = this.A0G;
        C16630pM r03 = this.A0P;
        AnonymousClass3DD r13 = new AnonymousClass3DD(r02, r4, r0, (CaptionView) AnonymousClass028.A0D(A00, R.id.input_container_inner), r03);
        this.A03 = r13;
        CharSequence charSequence = this.A07;
        List list = this.A0S;
        if (list.size() == 1) {
            jid = (Jid) list.get(0);
        } else {
            jid = null;
        }
        ViewGroup viewGroup = (ViewGroup) AnonymousClass028.A0D(A00, R.id.mention_attach);
        C453321d r7 = this.A0O;
        ActivityC13810kN r04 = this.A0D;
        CaptionView captionView = r13.A04;
        MentionableEntry mentionableEntry = captionView.A0B;
        mentionableEntry.setScrollBarStyle(EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING);
        mentionableEntry.setImeOptions(6);
        mentionableEntry.setInputType(147457);
        captionView.A07.setVisibility(0);
        captionView.setCaptionEditTextView(charSequence);
        C104954tH r15 = new AnonymousClass02B() { // from class: X.4tH
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                AnonymousClass3DD.this.A00((Integer) obj);
            }
        };
        AnonymousClass016 r14 = r7.A05;
        r14.A05(r04, r15);
        r13.A00(Integer.valueOf(((Number) r14.A01()).intValue()));
        if (C15380n4.A0J(jid)) {
            mentionableEntry.A04 = A00;
            mentionableEntry.A0D(viewGroup, C15580nU.A02(jid), true, false, false);
        }
        LinearLayout linearLayout = captionView.A08;
        linearLayout.setVisibility(0);
        captionView.A05.setVisibility(8);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(220);
        alphaAnimation.setInterpolator(new DecelerateInterpolator());
        linearLayout.startAnimation(alphaAnimation);
        mentionableEntry.startAnimation(alphaAnimation);
        this.A03.A04.setCaptionButtonsListener(this);
        AnonymousClass3DD r05 = this.A03;
        CaptionView captionView2 = r05.A04;
        AnonymousClass19M r152 = r05.A03;
        AnonymousClass01d r142 = r05.A01;
        C16630pM r132 = r05.A05;
        MentionableEntry mentionableEntry2 = captionView2.A0B;
        mentionableEntry2.addTextChangedListener(new AnonymousClass367(mentionableEntry2, (TextView) captionView2.findViewById(R.id.counter), r142, captionView2.A00, r152, r132, EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH, 30, true));
        mentionableEntry2.setOnEditorActionListener(new TextView.OnEditorActionListener() { // from class: X.4pS
            @Override // android.widget.TextView.OnEditorActionListener
            public final boolean onEditorAction(TextView textView, int i2, KeyEvent keyEvent) {
                AnonymousClass2Aj r1 = AnonymousClass2Aj.this;
                if (i2 != 6) {
                    return false;
                }
                Log.i("MediaCaptionDialog/dismiss/send");
                r1.onDismiss();
                return true;
            }
        });
        ((C37011l7) mentionableEntry2).A00 = new AbstractC116025Tv() { // from class: X.554
            @Override // X.AbstractC116025Tv
            public final boolean ARf(int i2, KeyEvent keyEvent) {
                AnonymousClass2Aj r2 = AnonymousClass2Aj.this;
                if (i2 != 4 || keyEvent.getAction() != 1) {
                    return false;
                }
                Log.i("MediaCaptionDialog/dismiss/send");
                r2.onDismiss();
                return false;
            }
        };
        AnonymousClass3F8 r10 = new AnonymousClass3F8((WaImageButton) AnonymousClass028.A0D(A00, R.id.send), r4);
        this.A06 = r10;
        r10.A00(this.A00);
        AnonymousClass3F8 r12 = this.A06;
        r12.A01.setOnClickListener(new ViewOnClickCListenerShape5S0200000_I1(r12, 47, this));
        if (z) {
            this.A05 = new C63763Cv(r4, (RecipientsView) AnonymousClass028.A0D(A00, R.id.media_recipients), true);
            View A0D = AnonymousClass028.A0D(A00, R.id.input_container);
            boolean z2 = this.A09;
            C63763Cv r06 = this.A05;
            if (z2) {
                r06.A02.setRecipientsListener(this);
            } else {
                RecipientsView recipientsView = r06.A02;
                recipientsView.A04 = false;
                recipientsView.A00 = R.color.audience_selector_disabled_chip;
            }
            C63763Cv r122 = this.A05;
            AnonymousClass016 r11 = r7.A00;
            r122.A00(this.A0F, (C32731ce) r7.A03.A01(), list, C15380n4.A0P((List) r11.A01()), true);
            boolean z3 = !((List) r11.A01()).isEmpty();
            getContext();
            if (z3) {
                C92974Yj.A00(A0D, r4);
            } else {
                C92974Yj.A01(A0D, r4);
            }
            this.A06.A01(z3);
        }
        getWindow().setLayout(-1, -1);
        if ((r04.getWindow().getAttributes().flags & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) != 0) {
            getWindow().setFlags(EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH, EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH);
            getWindow().clearFlags(256);
        }
        KeyboardPopupLayout keyboardPopupLayout = (KeyboardPopupLayout) A00.findViewById(R.id.main);
        keyboardPopupLayout.setOnClickListener(new ViewOnClickCListenerShape15S0100000_I0_2(this, 8));
        C252718t r112 = this.A0Q;
        AbstractC15710nm r102 = this.A0C;
        C231510o r72 = this.A0L;
        AnonymousClass193 r9 = this.A0M;
        C14820m6 r8 = this.A0H;
        CaptionView captionView3 = this.A03.A04;
        C15270mq r6 = new C15270mq(r04, captionView3.A07, r102, keyboardPopupLayout, captionView3.A0B, r02, r8, r4, r0, r72, r9, r03, r112);
        this.A01 = r6;
        r6.A0E = new RunnableBRunnable0Shape8S0100000_I0_8(this, 6);
        C15330mx r82 = new C15330mx(r04, r4, r0, this.A01, r72, (EmojiSearchContainer) A00.findViewById(R.id.emoji_search_container), r03);
        this.A02 = r82;
        r82.A00 = new AbstractC14020ki() { // from class: X.56k
            @Override // X.AbstractC14020ki
            public final void APd(C37471mS r3) {
                DialogC47362Ai.this.A0E.APc(r3.A00);
            }
        };
        C15270mq r3 = this.A01;
        r3.A0C(this.A0E);
        r3.A00 = R.drawable.ib_emoji;
        r3.A03 = R.drawable.ib_keyboard;
        getWindow().setSoftInputMode(5);
        this.A03.A04.A0B.A04(true);
    }

    @Override // X.AnonymousClass2Aj, X.AbstractC47372Ak
    public void onDismiss() {
        super.dismiss();
        if (this.A01.isShowing()) {
            this.A01.dismiss();
        }
        CaptionView captionView = this.A03.A04;
        this.A04 = new AnonymousClass4QP(new SpannedString(captionView.getCaptionText()), captionView.A0B.getStringText(), captionView.A0B.getMentions());
        this.A03.A04.A0B.A08();
    }
}
