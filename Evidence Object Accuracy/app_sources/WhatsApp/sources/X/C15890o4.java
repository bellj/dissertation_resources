package X;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0o4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15890o4 {
    public final C16590pI A00;
    public final C14820m6 A01;

    public C15890o4(C16590pI r1, C14820m6 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public static List A00() {
        ArrayList arrayList = new ArrayList();
        arrayList.add("android.permission.READ_PHONE_STATE");
        if (Build.VERSION.SDK_INT >= 30) {
            arrayList.add("android.permission.READ_PHONE_NUMBERS");
        }
        return arrayList;
    }

    public static boolean A01(Context context, String str) {
        if (Build.VERSION.SDK_INT < 23) {
            return true;
        }
        PackageManager packageManager = context.getPackageManager();
        if (packageManager == null || packageManager.checkPermission(str, "com.whatsapp.w4b") != 0) {
            return false;
        }
        return true;
    }

    public int A02(String str) {
        if (Build.VERSION.SDK_INT < 23) {
            return 0;
        }
        int A01 = AnonymousClass00T.A01(this.A00.A00, str);
        if (A01 == 0) {
            this.A01.A00.edit().remove(str).apply();
        }
        return A01;
    }

    public boolean A03() {
        return A02("android.permission.ACCESS_COARSE_LOCATION") == 0 || A02("android.permission.ACCESS_FINE_LOCATION") == 0;
    }

    public boolean A04() {
        return A02("android.permission.READ_CALL_LOG") == 0;
    }

    public boolean A05() {
        return A02("android.permission.ANSWER_PHONE_CALLS") == 0 && A08();
    }

    public boolean A06() {
        String str;
        if (Build.VERSION.SDK_INT < 28) {
            str = "android.permission.CALL_PHONE";
        } else if (A02("android.permission.ANSWER_PHONE_CALLS") != 0) {
            return false;
        } else {
            str = "android.permission.READ_CALL_LOG";
        }
        if (A02(str) != 0 || !A08()) {
            return false;
        }
        return true;
    }

    public boolean A07() {
        return A02("android.permission.WRITE_EXTERNAL_STORAGE") == 0 && A02("android.permission.READ_EXTERNAL_STORAGE") == 0;
    }

    public boolean A08() {
        boolean z = true;
        for (String str : A00()) {
            boolean z2 = false;
            if (A02(str) == 0) {
                z2 = true;
            }
            z &= z2;
        }
        return z;
    }

    public boolean A09() {
        return C28391Mz.A05() && A02("android.permission.READ_PHONE_STATE") == -1;
    }

    public boolean A0A(String str) {
        return "mounted".equals(str) && A02("android.permission.WRITE_EXTERNAL_STORAGE") == 0;
    }

    public boolean A0B(boolean z) {
        boolean z2;
        int i = Build.VERSION.SDK_INT;
        if ((i >= 23 || this.A00.A00.checkCallingOrSelfPermission("android.permission.RECORD_AUDIO") == 0) && (i < 23 || A02("android.permission.RECORD_AUDIO") == 0)) {
            z2 = false;
        } else {
            z2 = true;
        }
        boolean z3 = z && ((i < 23 && this.A00.A00.checkCallingOrSelfPermission("android.permission.CAMERA") != 0) || (i >= 23 && A02("android.permission.CAMERA") != 0));
        if (z2 || z3) {
            return false;
        }
        return true;
    }
}
