package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.2BI  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2BI extends Handler {
    public final AnonymousClass2BH A00;

    public AnonymousClass2BI(Looper looper, AnonymousClass2BH r2) {
        super(looper);
        this.A00 = r2;
    }

    public void A00(long j) {
        Long valueOf = Long.valueOf(j);
        Message obtain = Message.obtain();
        obtain.what = 1;
        if (valueOf != null) {
            obtain.getData().putLong("TimerDuration", valueOf.longValue());
        }
        sendMessage(obtain);
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        HashSet hashSet;
        HashSet hashSet2;
        Long l;
        boolean z;
        String str;
        boolean containsKey;
        int i = message.what;
        if (i != 1) {
            if (i == 2) {
                removeMessages(3);
            } else if (i == 3) {
                removeMessages(2);
            } else {
                return;
            }
            AnonymousClass2BH r0 = this.A00;
            if (r0 != null) {
                AnonymousClass1II r1 = r0.A00;
                synchronized (r1) {
                    r1.A00 = 1;
                }
                AnonymousClass1IJ r02 = r1.A03;
                if (r02 != null) {
                    AnonymousClass1HH r8 = r02.A00;
                    AnonymousClass1HE r2 = ((AnonymousClass1HG) r8).A01;
                    synchronized (r2) {
                        hashSet = new HashSet(r2.A02.keySet());
                    }
                    Iterator it = hashSet.iterator();
                    long j = -1;
                    while (it.hasNext()) {
                        AnonymousClass1IA r5 = (AnonymousClass1IA) it.next();
                        synchronized (r2) {
                            hashSet2 = new HashSet(r2.A03(r5).A03.keySet());
                        }
                        Iterator it2 = hashSet2.iterator();
                        while (true) {
                            while (it2.hasNext()) {
                                Runnable runnable = (Runnable) it2.next();
                                synchronized (r2) {
                                    l = (Long) r2.A03(r5).A03.get(runnable);
                                }
                                if (l != null) {
                                    String A00 = AnonymousClass1HE.A00(runnable);
                                    j = r2.A02(A00).A00 - (SystemClock.uptimeMillis() - l.longValue());
                                    if (j <= 0) {
                                        Set set = r8.A02;
                                        synchronized (set) {
                                            z = false;
                                            if (!set.contains(A00)) {
                                                z = true;
                                                synchronized (r2) {
                                                    containsKey = r2.A03(r5).A03.containsKey(runnable);
                                                }
                                                if (containsKey) {
                                                    set.add(A00);
                                                }
                                            }
                                        }
                                        if (z) {
                                            String str2 = r5.A00;
                                            HashMap hashMap = new HashMap();
                                            synchronized (r2) {
                                                str = (String) r2.A03(r5).A04.get(runnable);
                                            }
                                            hashMap.put("threadName", str);
                                            hashMap.put("expectedJobLimit", String.valueOf(r2.A02(A00).A00));
                                            hashMap.put("threadPoolExecutor", r5.toString());
                                            r8.A00(str2, hashMap);
                                        } else {
                                            continue;
                                        }
                                    } else if (j < 0 || j < j) {
                                    }
                                }
                            }
                        }
                        j = j;
                    }
                    if (r8.A01()) {
                        AnonymousClass1II r52 = r8.A00;
                        synchronized (r52) {
                            if (r8.A01()) {
                                if (j > 0) {
                                    r52.A00(r8.A01, j);
                                } else {
                                    r52.A03 = null;
                                    AnonymousClass2BI r12 = r52.A02;
                                    if (r12 != null) {
                                        r12.removeMessages(1);
                                        r12.removeMessages(3);
                                        r12.removeMessages(2);
                                        r52.A02 = null;
                                    }
                                }
                            }
                        }
                        return;
                    }
                    return;
                }
                return;
            }
            return;
        }
        long j2 = message.getData().getLong("TimerDuration");
        Message obtain = Message.obtain();
        obtain.what = 2;
        sendMessageDelayed(obtain, Math.max(j2, 0L));
        AnonymousClass2BH r03 = this.A00;
        if (r03 != null) {
            AnonymousClass1II r13 = r03.A00;
            synchronized (r13) {
                r13.A00 = 3;
            }
        }
    }
}
