package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: X.1ce  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C32731ce implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C99744kn();
    public final int A00;
    public final List A01;
    public final List A02;
    public final boolean A03;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C32731ce(Parcel parcel) {
        this.A00 = parcel.readInt();
        ArrayList arrayList = new ArrayList();
        this.A01 = arrayList;
        parcel.readList(arrayList, UserJid.class.getClassLoader());
        ArrayList arrayList2 = new ArrayList();
        this.A02 = arrayList2;
        parcel.readList(arrayList2, UserJid.class.getClassLoader());
        this.A03 = parcel.readByte() != 0;
    }

    public C32731ce(List list, List list2, int i, boolean z) {
        this.A00 = i;
        this.A01 = list == null ? Collections.emptyList() : list;
        this.A02 = list2 == null ? Collections.emptyList() : list2;
        this.A03 = z;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.A00);
        parcel.writeList(this.A01);
        parcel.writeList(this.A02);
        parcel.writeByte(this.A03 ? (byte) 1 : 0);
    }
}
