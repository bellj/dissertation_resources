package X;

/* renamed from: X.2FM  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2FM extends AbstractC16110oT {
    public Long A00;
    public String A01;

    public AnonymousClass2FM() {
        super(2578, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A01);
        r3.Abe(2, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("WamAndroidDatabaseRollbackEvent {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "migrationName", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "rollbackT", this.A00);
        sb.append("}");
        return sb.toString();
    }
}
