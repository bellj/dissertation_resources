package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

/* renamed from: X.3fA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C72783fA extends AnimatorListenerAdapter {
    public final /* synthetic */ AbstractC92674Wx A00;
    public final /* synthetic */ AnonymousClass3C8 A01;

    public C72783fA(AbstractC92674Wx r1, AnonymousClass3C8 r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        super.onAnimationEnd(animator);
        this.A00.A01();
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        super.onAnimationStart(animator);
        AbstractC92674Wx r1 = this.A00;
        r1.A01 = true;
        r1.A00 = -1.0f;
    }
}
