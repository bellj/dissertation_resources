package X;

import android.os.Build;
import android.view.accessibility.AccessibilityNodeInfo;

/* renamed from: X.0TY  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0TY {
    public final Object A00;

    public AnonymousClass0TY(Object obj) {
        this.A00 = obj;
    }

    public static AnonymousClass0TY A00(int i) {
        AccessibilityNodeInfo.CollectionInfo collectionInfo;
        if (Build.VERSION.SDK_INT >= 19) {
            collectionInfo = AccessibilityNodeInfo.CollectionInfo.obtain(1, i, false);
        } else {
            collectionInfo = null;
        }
        return new AnonymousClass0TY(collectionInfo);
    }

    public static AnonymousClass0TY A01(int i, int i2, int i3, boolean z) {
        AccessibilityNodeInfo.CollectionInfo collectionInfo;
        int i4 = Build.VERSION.SDK_INT;
        if (i4 >= 21) {
            collectionInfo = AccessibilityNodeInfo.CollectionInfo.obtain(i, i2, z, i3);
        } else if (i4 >= 19) {
            collectionInfo = AccessibilityNodeInfo.CollectionInfo.obtain(i, i2, z);
        } else {
            collectionInfo = null;
        }
        return new AnonymousClass0TY(collectionInfo);
    }
}
