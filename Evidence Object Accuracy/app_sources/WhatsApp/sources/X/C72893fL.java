package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.facebook.redex.RunnableBRunnable0Shape14S0100000_I1;

/* renamed from: X.3fL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C72893fL extends BroadcastReceiver {
    public final /* synthetic */ AnonymousClass3HW A00;

    public /* synthetic */ C72893fL(AnonymousClass3HW r1) {
        this.A00 = r1;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        AnonymousClass3HW r3 = this.A00;
        r3.A06.post(new RunnableBRunnable0Shape14S0100000_I1(r3, 3));
    }
}
