package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.413  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass413 extends AnonymousClass4EB {
    public final UserJid A00;
    public final String A01;

    public AnonymousClass413(UserJid userJid, String str) {
        C16700pc.A0E(userJid, 2);
        this.A01 = str;
        this.A00 = userJid;
    }
}
