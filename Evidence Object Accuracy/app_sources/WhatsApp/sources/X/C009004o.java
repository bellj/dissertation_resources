package X;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* renamed from: X.04o  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C009004o implements Set {
    public final /* synthetic */ AbstractC008904n A00;

    public C009004o(AbstractC008904n r1) {
        this.A00 = r1;
    }

    @Override // java.util.Set, java.util.Collection
    public boolean add(Object obj) {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.Set, java.util.Collection
    public boolean addAll(Collection collection) {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.Set, java.util.Collection
    public void clear() {
        this.A00.A06();
    }

    @Override // java.util.Set, java.util.Collection
    public boolean contains(Object obj) {
        return this.A00.A02(obj) >= 0;
    }

    @Override // java.util.Set, java.util.Collection
    public boolean containsAll(Collection collection) {
        Map A05 = this.A00.A05();
        for (Object obj : collection) {
            if (!A05.containsKey(obj)) {
                return false;
            }
        }
        return true;
    }

    @Override // java.util.Set, java.util.Collection, java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof Set) {
            Set set = (Set) obj;
            try {
                if (size() != set.size()) {
                    return false;
                }
                if (containsAll(set)) {
                    return true;
                }
                return false;
            } catch (ClassCastException | NullPointerException unused) {
            }
        }
        return false;
    }

    @Override // java.util.Set, java.util.Collection, java.lang.Object
    public int hashCode() {
        int hashCode;
        AbstractC008904n r4 = this.A00;
        int i = 0;
        for (int A01 = r4.A01() - 1; A01 >= 0; A01--) {
            Object A03 = r4.A03(A01, 0);
            if (A03 == null) {
                hashCode = 0;
            } else {
                hashCode = A03.hashCode();
            }
            i += hashCode;
        }
        return i;
    }

    @Override // java.util.Set, java.util.Collection
    public boolean isEmpty() {
        return this.A00.A01() == 0;
    }

    @Override // java.util.Set, java.util.Collection, java.lang.Iterable
    public Iterator iterator() {
        return new C009104p(this.A00, 0);
    }

    @Override // java.util.Set, java.util.Collection
    public boolean remove(Object obj) {
        AbstractC008904n r1 = this.A00;
        int A02 = r1.A02(obj);
        if (A02 < 0) {
            return false;
        }
        r1.A07(A02);
        return true;
    }

    @Override // java.util.Set, java.util.Collection
    public boolean removeAll(Collection collection) {
        Map A05 = this.A00.A05();
        int size = A05.size();
        for (Object obj : collection) {
            A05.remove(obj);
        }
        return size != A05.size();
    }

    @Override // java.util.Set, java.util.Collection
    public boolean retainAll(Collection collection) {
        return AbstractC008904n.A00(collection, this.A00.A05());
    }

    @Override // java.util.Set, java.util.Collection
    public int size() {
        return this.A00.A01();
    }

    @Override // java.util.Set, java.util.Collection
    public Object[] toArray() {
        AbstractC008904n r5 = this.A00;
        int A01 = r5.A01();
        Object[] objArr = new Object[A01];
        for (int i = 0; i < A01; i++) {
            objArr[i] = r5.A03(i, 0);
        }
        return objArr;
    }

    @Override // java.util.Set, java.util.Collection
    public Object[] toArray(Object[] objArr) {
        return this.A00.A08(objArr, 0);
    }
}
