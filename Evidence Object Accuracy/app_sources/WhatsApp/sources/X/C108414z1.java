package X;

import android.os.IBinder;
import com.google.android.gms.common.internal.IGmsServiceBroker;

/* renamed from: X.4z1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C108414z1 implements IGmsServiceBroker {
    public final IBinder A00;

    public C108414z1(IBinder iBinder) {
        this.A00 = iBinder;
    }

    @Override // android.os.IInterface
    public final IBinder asBinder() {
        return this.A00;
    }
}
