package X;

import android.os.Build;
import android.text.TextUtils;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodManager;
import android.view.inputmethod.InputMethodSubtype;
import com.whatsapp.util.Log;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Pattern;

/* renamed from: X.197  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass197 {
    public InputMethodSubtype A00;
    public Set A01;
    public final AnonymousClass01d A02;

    public AnonymousClass197(AnonymousClass01d r1) {
        this.A02 = r1;
    }

    public Locale A00() {
        InputMethodManager A0Q = this.A02.A0Q();
        AnonymousClass009.A05(A0Q);
        AnonymousClass009.A05(A0Q);
        InputMethodSubtype currentInputMethodSubtype = A0Q.getCurrentInputMethodSubtype();
        if (currentInputMethodSubtype == null || !currentInputMethodSubtype.getMode().equals("keyboard")) {
            return null;
        }
        InputMethodSubtype inputMethodSubtype = this.A00;
        if (!(inputMethodSubtype == null || inputMethodSubtype == currentInputMethodSubtype || inputMethodSubtype.equals(currentInputMethodSubtype))) {
            A02();
        }
        this.A00 = currentInputMethodSubtype;
        if (this.A01 == null) {
            A02();
        }
        Set set = this.A01;
        if (set == null || !set.contains(currentInputMethodSubtype)) {
            return null;
        }
        String locale = currentInputMethodSubtype.getLocale();
        if (TextUtils.isEmpty(locale)) {
            return null;
        }
        if (Pattern.matches("[a-z]{2}_[A-Z]{2}", locale)) {
            String replace = locale.replace("_", "-");
            if (Build.VERSION.SDK_INT >= 21) {
                return Locale.forLanguageTag(replace);
            }
            return new Locale(locale.substring(0, 2), locale.substring(3, 5));
        } else if (Pattern.matches("[a-z]{2}", locale)) {
            return new Locale(locale);
        } else {
            StringBuilder sb = new StringBuilder("keyboardLanguageExtractor/error/cannot parse locale ");
            sb.append(locale);
            Log.e(sb.toString());
            return null;
        }
    }

    public TreeSet A01() {
        String str;
        int length;
        TreeSet treeSet = new TreeSet(String.CASE_INSENSITIVE_ORDER);
        InputMethodManager A0Q = this.A02.A0Q();
        AnonymousClass009.A05(A0Q);
        for (InputMethodInfo inputMethodInfo : A0Q.getEnabledInputMethodList()) {
            for (InputMethodSubtype inputMethodSubtype : A0Q.getEnabledInputMethodSubtypeList(inputMethodInfo, true)) {
                if (inputMethodSubtype.getMode().equals("keyboard")) {
                    if (Build.VERSION.SDK_INT >= 24) {
                        str = inputMethodSubtype.getLanguageTag();
                    } else {
                        str = "";
                    }
                    if (str.isEmpty()) {
                        str = new Locale(inputMethodSubtype.getLocale()).getLanguage();
                    }
                    if (str.isEmpty() || (length = str.length()) < 2) {
                        StringBuilder sb = new StringBuilder("KeyboardLanguageExtractor/getTwoLetterLanguageCode/unexpected language result from input method, language: '");
                        sb.append(str);
                        sb.append("'");
                        Log.w(sb.toString());
                    } else if (length <= 2 || (str = str.substring(0, 2)) != null) {
                        treeSet.add(str);
                    }
                }
            }
        }
        return treeSet;
    }

    public final void A02() {
        this.A01 = new HashSet();
        InputMethodManager A0Q = this.A02.A0Q();
        AnonymousClass009.A05(A0Q);
        AnonymousClass009.A05(A0Q);
        for (InputMethodInfo inputMethodInfo : A0Q.getEnabledInputMethodList()) {
            if (inputMethodInfo.getId().startsWith("com.google.android.inputmethod")) {
                this.A01.addAll(A0Q.getEnabledInputMethodSubtypeList(inputMethodInfo, true));
            }
        }
    }
}
