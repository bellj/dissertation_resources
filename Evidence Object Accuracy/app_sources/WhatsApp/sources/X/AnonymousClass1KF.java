package X;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: X.1KF  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1KF extends C14580lf {
    public int A00;
    public boolean A01 = false;
    public final List A02;
    public final List A03;

    public AnonymousClass1KF(List list) {
        AnonymousClass009.A0B("AllOfAsyncFuture: futures list is empty", list.size() > 0);
        this.A02 = new ArrayList(Collections.nCopies(list.size(), null));
        this.A03 = new ArrayList(Collections.nCopies(list.size(), null));
        this.A00 = list.size();
        for (int i = 0; i < list.size(); i++) {
            C14580lf r2 = (C14580lf) list.get(i);
            r2.A00(new AbstractC14590lg(i) { // from class: X.5AK
                public final /* synthetic */ int A00;

                {
                    this.A00 = r2;
                }

                @Override // X.AbstractC14590lg
                public final void accept(Object obj) {
                    AnonymousClass1KF r22 = AnonymousClass1KF.this;
                    int i2 = this.A00;
                    synchronized (r22) {
                        r22.A02.set(i2, obj);
                        r22.A01 = true;
                        int i3 = r22.A00 - 1;
                        r22.A00 = i3;
                        if (i3 == 0) {
                            r22.A05();
                        }
                    }
                }
            });
            r2.A00.A03(new AbstractC14590lg(i) { // from class: X.5AJ
                public final /* synthetic */ int A00;

                {
                    this.A00 = r2;
                }

                @Override // X.AbstractC14590lg
                public final void accept(Object obj) {
                    AnonymousClass1KF r22 = AnonymousClass1KF.this;
                    int i2 = this.A00;
                    synchronized (r22) {
                        r22.A03.set(i2, obj);
                        int i3 = r22.A00 - 1;
                        r22.A00 = i3;
                        if (i3 == 0) {
                            r22.A05();
                        }
                    }
                }
            }, null);
        }
    }

    public final void A05() {
        ArrayList arrayList;
        AnonymousClass3A0 r1;
        synchronized (this) {
            arrayList = null;
            if (this.A01) {
                arrayList = new ArrayList(this.A02);
                r1 = null;
            } else {
                r1 = new AnonymousClass3A0(this.A03);
            }
        }
        if (arrayList != null) {
            A02(arrayList);
        } else {
            A03(r1);
        }
    }
}
