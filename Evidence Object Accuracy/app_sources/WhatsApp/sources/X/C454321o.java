package X;

import android.animation.TimeInterpolator;
import android.content.Intent;
import android.os.Bundle;
import android.transition.ChangeBounds;
import android.transition.ChangeImageTransform;
import android.transition.ChangeTransform;
import android.transition.Fade;
import android.transition.Transition;
import android.transition.TransitionSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.LinearInterpolator;
import com.whatsapp.R;
import com.whatsapp.mediaview.MediaViewBaseFragment;
import com.whatsapp.mediaview.PhotoView;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.21o  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C454321o extends AbstractC454421p {
    public final AnonymousClass2TT A00;
    public final MediaViewBaseFragment A01;

    @Override // X.AbstractC454421p
    public void A0A() {
    }

    public C454321o(AnonymousClass2TT r1, MediaViewBaseFragment mediaViewBaseFragment) {
        this.A00 = r1;
        this.A01 = mediaViewBaseFragment;
    }

    public static C004902f A00(View view, ActivityC000800j r2, AnonymousClass01F r3, AnonymousClass2TT r4, String str) {
        Object obj;
        ArrayList A01 = A01(view, r2, r4, str);
        C004902f r1 = new C004902f(r3);
        Iterator it = A01.iterator();
        while (it.hasNext()) {
            AnonymousClass01T r0 = (AnonymousClass01T) it.next();
            Object obj2 = r0.A00;
            if (!(obj2 == null || (obj = r0.A01) == null)) {
                View view2 = (View) obj2;
                String str2 = (String) obj;
                if (AnonymousClass0TQ.A00 != null || AnonymousClass0TQ.A01 != null) {
                    String A0J = AnonymousClass028.A0J(view2);
                    if (A0J != null) {
                        if (r1.A0C == null) {
                            r1.A0C = new ArrayList();
                            r1.A0D = new ArrayList();
                        } else if (r1.A0D.contains(str2)) {
                            StringBuilder sb = new StringBuilder("A shared element with the target name '");
                            sb.append(str2);
                            sb.append("' has already been added to the transaction.");
                            throw new IllegalArgumentException(sb.toString());
                        } else if (r1.A0C.contains(A0J)) {
                            StringBuilder sb2 = new StringBuilder("A shared element with the source name '");
                            sb2.append(A0J);
                            sb2.append("' has already been added to the transaction.");
                            throw new IllegalArgumentException(sb2.toString());
                        }
                        r1.A0C.add(A0J);
                        r1.A0D.add(str2);
                    } else {
                        throw new IllegalArgumentException("Unique transitionNames are required for all sharedElements");
                    }
                }
            }
        }
        return r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x00a7, code lost:
        if (r2 == 0) goto L_0x00a9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x00ad, code lost:
        if (r6 != r11.getHeight()) goto L_0x00af;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x00af, code lost:
        X.AnonymousClass028.A0f(r11, new android.graphics.Rect(0, r2, r11.getWidth(), r6));
        r11.postDelayed(new com.facebook.redex.RunnableBRunnable0Shape8S0100000_I0_8(r11, 18), (long) r12.getResources().getInteger(17694721));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x00d1, code lost:
        r3.addAll(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00d4, code lost:
        return r3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.ArrayList A01(android.view.View r11, X.ActivityC000800j r12, X.AnonymousClass2TT r13, java.lang.String r14) {
        /*
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            X.AnonymousClass028.A0k(r11, r14)
            java.lang.String r1 = r11.getTransitionName()
            X.01T r0 = new X.01T
            r0.<init>(r11, r1)
            r3.add(r0)
            r0 = 16908335(0x102002f, float:2.387736E-38)
            android.view.View r2 = r12.findViewById(r0)
            if (r2 == 0) goto L_0x002b
            java.lang.String r1 = "statusBar"
            X.AnonymousClass028.A0k(r2, r1)
            X.01T r0 = new X.01T
            r0.<init>(r2, r1)
            r3.add(r0)
        L_0x002b:
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            int r6 = r11.getHeight()
            r8 = 2
            int[] r7 = new int[r8]
            r11.getLocationOnScreen(r7)
            r0 = 2131366495(0x7f0a125f, float:1.8352885E38)
            android.view.View r5 = r12.findViewById(r0)
            r10 = 1
            if (r5 == 0) goto L_0x007f
            r0 = 2131892354(0x7f121882, float:1.9419454E38)
            java.lang.String r1 = r13.A00(r0)
            X.AnonymousClass028.A0k(r5, r1)
            X.01T r0 = new X.01T
            r0.<init>(r5, r1)
            r4.add(r0)
            int[] r9 = new int[r8]
            r5.getLocationOnScreen(r9)
            r2 = r7[r10]
            int r0 = r11.getHeight()
            int r2 = r2 + r0
            r1 = r9[r10]
            int r0 = r5.getHeight()
            int r1 = r1 + r0
            if (r2 <= r1) goto L_0x007f
            r2 = r7[r10]
            int r0 = r11.getHeight()
            int r2 = r2 + r0
            r1 = r9[r10]
            int r0 = r5.getHeight()
            int r1 = r1 + r0
            int r2 = r2 - r1
            int r6 = r11.getHeight()
            int r6 = r6 - r2
        L_0x007f:
            r0 = 2131366496(0x7f0a1260, float:1.8352887E38)
            android.view.View r2 = r12.findViewById(r0)
            r5 = 0
            if (r2 == 0) goto L_0x00d5
            r0 = 2131892355(0x7f121883, float:1.9419456E38)
            java.lang.String r1 = r13.A00(r0)
            X.AnonymousClass028.A0k(r2, r1)
            X.01T r0 = new X.01T
            r0.<init>(r2, r1)
            r4.add(r0)
            int[] r1 = new int[r8]
            r2.getLocationOnScreen(r1)
            r0 = r7[r10]
            r2 = r1[r10]
            if (r0 >= r2) goto L_0x00d5
            int r2 = r2 - r0
            if (r2 != 0) goto L_0x00af
        L_0x00a9:
            int r0 = r11.getHeight()
            if (r6 == r0) goto L_0x00d1
        L_0x00af:
            int r1 = r11.getWidth()
            android.graphics.Rect r0 = new android.graphics.Rect
            r0.<init>(r5, r2, r1, r6)
            X.AnonymousClass028.A0f(r11, r0)
            r0 = 18
            com.facebook.redex.RunnableBRunnable0Shape8S0100000_I0_8 r2 = new com.facebook.redex.RunnableBRunnable0Shape8S0100000_I0_8
            r2.<init>(r11, r0)
            android.content.res.Resources r1 = r12.getResources()
            r0 = 17694721(0x10e0001, float:2.6081284E-38)
            int r0 = r1.getInteger(r0)
            long r0 = (long) r0
            r11.postDelayed(r2, r0)
        L_0x00d1:
            r3.addAll(r4)
            return r3
        L_0x00d5:
            r2 = 0
            goto L_0x00a9
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C454321o.A01(android.view.View, X.00j, X.2TT, java.lang.String):java.util.ArrayList");
    }

    public static void A02(Intent intent, View view, ActivityC000800j r4, AnonymousClass2TT r5, String str) {
        C018108l A02 = C018108l.A02(r4, (AnonymousClass01T[]) A01(view, r4, r5, str).toArray(new AnonymousClass01T[0]));
        r4.setExitSharedElementCallback(new SharedElementCallbackC51742Ym(view, r4));
        r4.startActivity(intent, A02.A03());
    }

    @Override // X.AbstractC454421p
    public void A0B(Bundle bundle) {
        PhotoView A18;
        String str;
        MediaViewBaseFragment mediaViewBaseFragment = this.A01;
        Object A1C = mediaViewBaseFragment.A1C(mediaViewBaseFragment.A09.getCurrentItem());
        if (A1C != null) {
            int childCount = mediaViewBaseFragment.A09.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = mediaViewBaseFragment.A09.getChildAt(i);
                if (childAt instanceof ViewGroup) {
                    ViewGroup viewGroup = (ViewGroup) childAt;
                    if (viewGroup.getChildCount() > 0 && (A18 = mediaViewBaseFragment.A18(viewGroup)) != null) {
                        if (A1C.equals(viewGroup.getTag())) {
                            str = AbstractC42671vd.A0Z(A1C.toString());
                        } else {
                            str = null;
                        }
                        AnonymousClass028.A0k(A18, str);
                    }
                }
            }
            if (mediaViewBaseFragment.A1B() != null && !A1C.equals(mediaViewBaseFragment.A1B())) {
                mediaViewBaseFragment.A07().A07 = new C53422eO(bundle, this);
            }
            if (mediaViewBaseFragment.A1N()) {
                mediaViewBaseFragment.A0C().A0a();
                return;
            }
        }
        mediaViewBaseFragment.A1E();
    }

    @Override // X.AbstractC454421p
    public void A0C(Bundle bundle, AbstractC35501i8 r12) {
        MediaViewBaseFragment mediaViewBaseFragment = this.A01;
        mediaViewBaseFragment.A03.setVisibility(4);
        mediaViewBaseFragment.A1L(false, 0);
        mediaViewBaseFragment.A06.setVisibility(0);
        mediaViewBaseFragment.A0G = false;
        View decorView = mediaViewBaseFragment.A0C().getWindow().getDecorView();
        decorView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver$OnPreDrawListenerC101894oG(decorView));
        LinearInterpolator linearInterpolator = new LinearInterpolator();
        AnonymousClass2TT r5 = this.A00;
        ChangeBounds changeBounds = new ChangeBounds();
        changeBounds.setInterpolator(linearInterpolator);
        changeBounds.excludeTarget(r5.A00(R.string.transition_clipper_top), true);
        changeBounds.excludeTarget(r5.A00(R.string.transition_clipper_bottom), true);
        ChangeTransform changeTransform = new ChangeTransform();
        changeTransform.setInterpolator(linearInterpolator);
        ChangeImageTransform changeImageTransform = new ChangeImageTransform();
        changeImageTransform.setInterpolator(linearInterpolator);
        C52332ad r2 = new C52332ad(mediaViewBaseFragment.A01(), r5, true);
        r2.setInterpolator(linearInterpolator);
        TransitionSet transitionSet = new TransitionSet();
        transitionSet.setInterpolator((TimeInterpolator) linearInterpolator);
        transitionSet.setDuration(220L);
        transitionSet.addTransition(changeBounds);
        transitionSet.addTransition(changeTransform);
        transitionSet.addTransition(changeImageTransform);
        transitionSet.addTransition(r2);
        C52332ad r22 = new C52332ad(mediaViewBaseFragment.A01(), r5, false);
        r22.setInterpolator(linearInterpolator);
        TransitionSet transitionSet2 = new TransitionSet();
        transitionSet2.setInterpolator((TimeInterpolator) linearInterpolator);
        transitionSet2.setDuration(220L);
        transitionSet2.addTransition(changeBounds);
        transitionSet2.addTransition(changeTransform);
        transitionSet2.addTransition(changeImageTransform);
        transitionSet2.addTransition(r22);
        Fade fade = new Fade();
        fade.excludeTarget(16908335, true);
        fade.excludeTarget(16908336, true);
        Fade fade2 = new Fade();
        fade2.excludeTarget(16908335, true);
        fade2.excludeTarget(16908336, true);
        if (mediaViewBaseFragment.A1N()) {
            ActivityC000900k A0C = mediaViewBaseFragment.A0C();
            Window window = A0C.getWindow();
            A0C.A0c();
            window.setSharedElementEnterTransition(transitionSet);
            window.setSharedElementReturnTransition(transitionSet2);
            window.setEnterTransition(fade);
            window.setReturnTransition(fade2);
            transitionSet.addListener((Transition.TransitionListener) new C58632r6(mediaViewBaseFragment, r12));
            transitionSet2.addListener((Transition.TransitionListener) new C83793xu(r12));
            return;
        }
        mediaViewBaseFragment.A07().A0B = transitionSet;
        mediaViewBaseFragment.A07().A0C = transitionSet2;
        mediaViewBaseFragment.A07().A08 = fade;
        mediaViewBaseFragment.A07().A0A = fade2;
        fade.addListener(new C58632r6(mediaViewBaseFragment, r12));
        fade2.addListener(new C83793xu(r12));
    }
}
