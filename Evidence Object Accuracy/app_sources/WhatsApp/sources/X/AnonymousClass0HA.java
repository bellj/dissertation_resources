package X;

import java.util.Collections;
import java.util.List;

/* renamed from: X.0HA  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0HA extends AbstractC08110ab {
    public AnonymousClass0HA() {
        super(Collections.singletonList(new AnonymousClass0U8(100)));
    }

    public AnonymousClass0HA(List list) {
        super(list);
    }

    @Override // X.AbstractC12590iA
    public AnonymousClass0QR A88() {
        return new AnonymousClass0H0(this.A00);
    }
}
