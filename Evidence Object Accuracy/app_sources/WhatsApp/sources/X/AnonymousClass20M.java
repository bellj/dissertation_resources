package X;

/* renamed from: X.20M  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass20M extends RuntimeException {
    public AnonymousClass20M() {
        super("Presented MAC doesn't match calculated MAC (MAC prepended)");
    }
}
