package X;

import android.content.Context;

/* renamed from: X.1Nz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28541Nz {
    public final int A00;
    public final AbstractC18730sv A01;
    public final C20640w5 A02;
    public final AnonymousClass01d A03;
    public final AnonymousClass1O0 A04;

    public C28541Nz(Context context, AbstractC18730sv r3, C20640w5 r4, AnonymousClass01d r5, C14850m9 r6) {
        this.A04 = new AnonymousClass1O0(context);
        this.A02 = r4;
        this.A03 = r5;
        this.A01 = r3;
        this.A00 = r6.A02(1360);
    }
}
