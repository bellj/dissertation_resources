package X;

import android.content.ContentValues;
import android.database.Cursor;

/* renamed from: X.0tj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19200tj extends AbstractC18500sY implements AbstractC19010tQ {
    public final C20900wV A00;

    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AM5() {
    }

    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AND() {
    }

    public C19200tj(C20900wV r3, C18480sW r4) {
        super(r4, "message_send_count", 1);
        this.A00 = r3;
    }

    @Override // X.AbstractC18500sY
    public AnonymousClass2Ez A09(Cursor cursor) {
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow("_id");
        int columnIndexOrThrow2 = cursor.getColumnIndexOrThrow("send_count");
        C16310on A02 = this.A05.A02();
        long j = -1;
        int i = 0;
        while (cursor.moveToNext()) {
            try {
                j = cursor.getLong(columnIndexOrThrow);
                int i2 = cursor.getInt(columnIndexOrThrow2);
                if (i2 != 0) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("message_row_id", Long.valueOf(j));
                    contentValues.put("send_count", Integer.valueOf(i2));
                    A02.A03.A06(contentValues, "message_send_count", 5);
                    i++;
                }
            } catch (Throwable th) {
                try {
                    A02.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
        A02.close();
        return new AnonymousClass2Ez(j, i);
    }

    @Override // X.AbstractC19010tQ
    public void onRollback() {
        C16310on A02 = this.A05.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            A02.A03.A01("message_send_count", null, null);
            C21390xL r1 = this.A06;
            r1.A03("send_count_ready");
            r1.A03("migration_message_send_count_index");
            r1.A03("migration_message_send_count_retry");
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
