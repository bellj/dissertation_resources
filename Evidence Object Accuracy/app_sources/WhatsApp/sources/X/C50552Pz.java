package X;

import com.whatsapp.jid.Jid;

/* renamed from: X.2Pz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C50552Pz extends AnonymousClass2PA {
    public final AnonymousClass1IS A00;
    public final String A01;

    public C50552Pz(Jid jid, AnonymousClass1IS r2, String str, String str2, long j) {
        super(jid, str, j);
        this.A01 = str2;
        this.A00 = r2;
    }
}
