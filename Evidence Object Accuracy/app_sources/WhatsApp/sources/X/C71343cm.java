package X;

import java.util.Iterator;
import java.util.NoSuchElementException;

/* renamed from: X.3cm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C71343cm implements Iterator {
    public int A00 = 0;
    public final int A01;
    public final /* synthetic */ AbstractC27881Jp A02;

    public C71343cm(AbstractC27881Jp r2) {
        this.A02 = r2;
        this.A01 = r2.A03();
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        return C12990iw.A1Y(this.A00, this.A01);
    }

    @Override // java.util.Iterator
    public /* bridge */ /* synthetic */ Object next() {
        try {
            AbstractC27881Jp r2 = this.A02;
            int i = this.A00;
            this.A00 = i + 1;
            return Byte.valueOf(r2.A02(i));
        } catch (IndexOutOfBoundsException e) {
            throw new NoSuchElementException(e.getMessage());
        }
    }

    @Override // java.util.Iterator
    public void remove() {
        throw C12970iu.A0z();
    }
}
