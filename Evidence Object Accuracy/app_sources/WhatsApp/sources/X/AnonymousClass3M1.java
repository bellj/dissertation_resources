package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.redex.IDxCreatorShape1S0000000_2_I1;
import java.util.Arrays;

/* renamed from: X.3M1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3M1 implements Parcelable {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape1S0000000_2_I1(39);
    public final AnonymousClass3M4 A00;
    public final String A01;
    public final String A02;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass3M1(AnonymousClass3M4 r1, String str, String str2) {
        this.A01 = str;
        this.A02 = str2;
        this.A00 = r1;
    }

    public AnonymousClass3M1(Parcel parcel) {
        this.A01 = parcel.readString();
        this.A02 = parcel.readString();
        this.A00 = (AnonymousClass3M4) C12990iw.A0I(parcel, AnonymousClass3M4.class);
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AnonymousClass3M1)) {
            return false;
        }
        AnonymousClass3M1 r4 = (AnonymousClass3M1) obj;
        if (!C29941Vi.A00(this.A01, r4.A01) || !C29941Vi.A00(this.A02, r4.A02) || !C29941Vi.A00(this.A00, r4.A00)) {
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return Arrays.deepHashCode(new Object[]{this.A01, this.A02, this.A00});
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A01);
        parcel.writeString(this.A02);
        parcel.writeParcelable(this.A00, i);
    }
}
