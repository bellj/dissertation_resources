package X;

import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;

/* renamed from: X.641  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass641 implements ActionMode.Callback {
    public final /* synthetic */ AnonymousClass676 A00;

    @Override // android.view.ActionMode.Callback
    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
        return false;
    }

    @Override // android.view.ActionMode.Callback
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
        return false;
    }

    @Override // android.view.ActionMode.Callback
    public void onDestroyActionMode(ActionMode actionMode) {
    }

    @Override // android.view.ActionMode.Callback
    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
        return false;
    }

    public AnonymousClass641(AnonymousClass676 r1) {
        this.A00 = r1;
    }
}
