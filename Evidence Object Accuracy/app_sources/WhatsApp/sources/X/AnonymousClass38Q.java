package X;

import com.whatsapp.jid.UserJid;
import java.lang.ref.WeakReference;
import java.util.Set;

/* renamed from: X.38Q  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass38Q extends AbstractC16350or {
    public Set A00;
    public Set A01;
    public final UserJid A02;
    public final WeakReference A03;
    public final /* synthetic */ C244915s A04;

    public AnonymousClass38Q(C244915s r2, AnonymousClass4V2 r3, UserJid userJid) {
        this.A04 = r2;
        this.A02 = userJid;
        this.A03 = C12970iu.A10(r3);
    }
}
