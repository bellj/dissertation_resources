package X;

import android.view.animation.Animation;
import com.whatsapp.search.SearchFragment;

/* renamed from: X.3xB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83403xB extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ SearchFragment A00;
    public final /* synthetic */ Runnable A01;

    public C83403xB(SearchFragment searchFragment, Runnable runnable) {
        this.A00 = searchFragment;
        this.A01 = runnable;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        this.A01.run();
        super.onAnimationEnd(animation);
    }
}
