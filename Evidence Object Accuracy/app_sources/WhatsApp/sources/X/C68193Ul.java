package X;

import android.text.TextUtils;
import java.lang.ref.WeakReference;

/* renamed from: X.3Ul  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68193Ul implements AnonymousClass5XB {
    public int A00;
    public int A01;
    public final AnonymousClass4JU A02;
    public final C30181Wk A03;
    public final WeakReference A04 = C12970iu.A10(null);

    public C68193Ul(AnonymousClass4JU r2, C30181Wk r3, int i, int i2) {
        this.A03 = r3;
        this.A02 = r2;
        this.A01 = i;
        this.A00 = i2;
    }

    @Override // X.AnonymousClass5XB
    public boolean A9o() {
        return TextUtils.isEmpty(this.A03.A03);
    }

    @Override // X.AnonymousClass5XB
    public int AE9() {
        return this.A00;
    }

    @Override // X.AnonymousClass5XB
    public int AEB() {
        return this.A01;
    }

    @Override // X.AnonymousClass5XB
    public String AHT() {
        String str = this.A03.A03;
        return str == null ? "" : str;
    }

    @Override // X.AnonymousClass5XB
    public String getId() {
        return this.A03.A00;
    }
}
