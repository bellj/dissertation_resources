package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;

/* renamed from: X.0zM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22610zM {
    public static final long A03 = TimeUnit.DAYS.toMillis(7);
    public C40501rh A00;
    public C40501rh A01;
    public final C14830m7 A02;

    public C22610zM(C14830m7 r3, C16630pM r4) {
        this.A02 = r3;
        this.A01 = new C40501rh(r4, "entry_point_conversions_for_sending");
        this.A00 = new C40501rh(r4, "entry_point_conversions_for_logging");
    }

    public void A00(C40511ri r9) {
        String str;
        C40511ri A00;
        C40501rh r0 = this.A01;
        UserJid userJid = r9.A04;
        C16630pM r7 = r0.A00;
        String str2 = r0.A01;
        String string = r7.A01(str2).getString(userJid.getRawString(), null);
        if (string == null || (A00 = C40501rh.A00(string)) == null) {
            try {
                r7.A01(str2).edit().putString(userJid.getRawString(), r9.A00()).apply();
                return;
            } catch (JSONException e) {
                e = e;
                str = "CTWA: EntryPointConversionStore/storeConversion/json error";
            }
        } else if (System.currentTimeMillis() - A00.A03 > A03) {
            try {
                r7.A01(str2).edit().putString(userJid.getRawString(), r9.A00()).apply();
                return;
            } catch (JSONException e2) {
                e = e2;
                str = "CTWA: EntryPointConversionStore/updateConversion/json error";
            }
        } else {
            return;
        }
        StringBuilder sb = new StringBuilder(str);
        sb.append(e);
        Log.e(sb.toString());
    }

    public final void A01(C40501rh r10) {
        ArrayList arrayList = new ArrayList();
        C16630pM r6 = r10.A00;
        String str = r10.A01;
        Map<String, ?> all = r6.A01(str).getAll();
        for (Map.Entry<String, ?> entry : all.entrySet()) {
            Object obj = all.get(entry.getKey());
            if (obj != null) {
                C40511ri A00 = C40501rh.A00(obj.toString());
                if (A00 != null) {
                    arrayList.add(A00);
                }
            } else {
                StringBuilder sb = new StringBuilder("CTWA: EntryPointConversionStore/getAllConversions/ null pref value for key=");
                sb.append(entry);
                Log.e(sb.toString());
            }
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            C40511ri r5 = (C40511ri) it.next();
            if (System.currentTimeMillis() - r5.A03 > A03) {
                r6.A01(str).edit().remove(r5.A04.getRawString()).apply();
            }
        }
    }
}
