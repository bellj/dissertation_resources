package X;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import com.whatsapp.R;
import com.whatsapp.support.faq.SearchFAQ;
import java.util.List;

/* renamed from: X.2bW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52692bW extends ArrayAdapter {
    public final /* synthetic */ SearchFAQ A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C52692bW(Context context, SearchFAQ searchFAQ, List list) {
        super(context, (int) R.layout.search_faq_row, list);
        this.A00 = searchFAQ;
    }

    @Override // android.widget.ArrayAdapter, android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        AnonymousClass4OX r0;
        LinearLayout linearLayout;
        if (view == null) {
            LinearLayout linearLayout2 = new LinearLayout(getContext());
            LayoutInflater A01 = AnonymousClass01d.A01(getContext());
            AnonymousClass009.A05(A01);
            A01.inflate(R.layout.search_faq_row, (ViewGroup) linearLayout2, true);
            r0 = new AnonymousClass4OX();
            r0.A01 = C12960it.A0J(linearLayout2, R.id.search_faq_row_text);
            r0.A00 = linearLayout2.findViewById(R.id.divider);
            linearLayout2.setTag(r0);
            linearLayout = linearLayout2;
        } else {
            r0 = (AnonymousClass4OX) view.getTag();
            linearLayout = view;
        }
        Object item = getItem(i);
        AnonymousClass009.A05(item);
        AnonymousClass4S6 r3 = (AnonymousClass4S6) item;
        r0.A01.setText(r3.A02);
        View view2 = r0.A00;
        int i2 = 8;
        if (i < getCount() - 1) {
            i2 = 0;
        }
        view2.setVisibility(i2);
        C12960it.A14(linearLayout, this, r3, 46);
        return linearLayout;
    }
}
