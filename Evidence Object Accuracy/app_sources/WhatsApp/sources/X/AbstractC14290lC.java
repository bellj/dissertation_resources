package X;

import android.content.Context;
import android.util.AttributeSet;

/* renamed from: X.0lC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC14290lC extends AnonymousClass011 implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;

    public AbstractC14290lC(Context context) {
        super(context, null);
        A02();
    }

    public AbstractC14290lC(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A02();
    }

    public AbstractC14290lC(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A02();
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v5, types: [com.whatsapp.mentions.MentionableEntry] */
    /* JADX WARN: Type inference failed for: r2v5, types: [com.whatsapp.mediacomposer.doodle.textentry.DoodleEditText, X.0lC] */
    /* JADX WARN: Type inference failed for: r2v7, types: [X.1IW, X.0lC] */
    /* JADX WARN: Type inference failed for: r2v9, types: [com.whatsapp.CodeInputField, X.0lC] */
    /* JADX WARN: Type inference failed for: r2v11, types: [com.whatsapp.WaEditText] */
    /* JADX WARNING: Unknown variable types count: 3 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02() {
        /*
        // Method dump skipped, instructions count: 233
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC14290lC.A02():void");
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }
}
