package X;

import com.facebook.redex.RunnableBRunnable0Shape6S0200000_I0_6;

/* renamed from: X.2DO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2DO implements AnonymousClass1FK {
    public final /* synthetic */ AnonymousClass103 A00;
    public final /* synthetic */ AnonymousClass1V8 A01;

    public AnonymousClass2DO(AnonymousClass103 r1, AnonymousClass1V8 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass1FK
    public void AV3(C452120p r3) {
        this.A00.A0F.A05("handlePaymentMethodUpdate: sendGetPaymentMethods request error");
    }

    @Override // X.AnonymousClass1FK
    public void AVA(C452120p r3) {
        this.A00.A0F.A05("handlePaymentMethodUpdate: sendGetPaymentMethods response error");
    }

    @Override // X.AnonymousClass1FK
    public void AVB(C452220q r5) {
        this.A00.A01.A0H(new RunnableBRunnable0Shape6S0200000_I0_6(this, 38, this.A01));
    }
}
