package X;

import android.view.View;
import android.widget.RadioButton;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;

/* renamed from: X.2zX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C61172zX extends AbstractC75243jX {
    public final RadioButton A00;
    public final TextEmojiLabel A01;
    public final TextEmojiLabel A02;
    public final /* synthetic */ C54352ga A03;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C61172zX(View view, C54352ga r3) {
        super(view, r3);
        this.A03 = r3;
        this.A02 = C12970iu.A0U(view, R.id.select_list_item_title);
        this.A01 = C12970iu.A0U(view, R.id.select_list_item_description);
        this.A00 = (RadioButton) view.findViewById(R.id.select_list_item_radio_button);
        C12960it.A0y(view, this, 45);
    }
}
