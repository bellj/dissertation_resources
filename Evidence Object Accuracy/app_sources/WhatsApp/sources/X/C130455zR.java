package X;

import java.util.ArrayList;

/* renamed from: X.5zR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C130455zR {
    public static final ArrayList A01;
    public final AnonymousClass1V8 A00;

    static {
        String[] strArr = new String[2];
        strArr[0] = "0";
        A01 = C12960it.A0m("1", strArr, 1);
    }

    public C130455zR(C130465zS r13, AnonymousClass3CT r14, String str, String str2, String str3, String str4, long j) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy A0N = C117295Zj.A0N(A0M);
        C41141sy.A01(A0N, "action", "br-add-credential");
        if (C117295Zj.A1V(str, 1, false)) {
            C41141sy.A01(A0N, "device_id", str);
        }
        if (C117295Zj.A1W(str2, 1, false)) {
            C41141sy.A01(A0N, "nonce", str2);
        }
        if (AnonymousClass3JT.A0E(str3, 1, 100, false)) {
            C41141sy.A01(A0N, "provider", str3);
        }
        if (AnonymousClass3JT.A0D(Long.valueOf(j), -9007199254740991L, false)) {
            C117315Zl.A0X(A0N, "key_version", j);
        }
        A0N.A0A(str4, "is_first_card", A01);
        A0N.A05(r13.A00);
        this.A00 = C117295Zj.A0J(A0N, A0M, r14);
    }
}
