package X;

import android.graphics.Rect;

/* renamed from: X.3G6  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3G6 {
    public static int A00(String str) {
        float A01;
        if (str == null) {
            A01 = 0.0f;
        } else {
            try {
                A01 = AnonymousClass3JW.A01(str);
            } catch (AnonymousClass491 unused) {
                C28691Op.A00("InsetUtils", C12960it.A0d(str, C12960it.A0k("Error parsing inset value ")));
                return 0;
            }
        }
        return (int) A01;
    }

    public static Rect A01(AnonymousClass28D r5) {
        return new Rect(A00(r5.A0I(42)), A00(r5.A0I(40)), A00(r5.A0I(41)), A00(AnonymousClass28D.A06(r5)));
    }
}
