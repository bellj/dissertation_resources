package X;

import android.animation.ObjectAnimator;
import android.animation.TypeConverter;
import android.graphics.Path;
import android.graphics.Rect;
import android.os.Build;
import android.util.Property;
import android.view.View;
import java.util.Map;

/* renamed from: X.071  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass071 extends AnonymousClass072 {
    public static final Property A00 = new C02190Aj();
    public static final Property A01 = new C02180Ai();
    public static final Property A02 = new C02260Aq();
    public static final Property A03 = new C02210Al();
    public static final Property A04 = new C02200Ak();
    public static final Property A05 = new C02170Ah();
    public static final String[] A06 = {"android:changeBounds:bounds", "android:changeBounds:clip", "android:changeBounds:parent", "android:changeBounds:windowX", "android:changeBounds:windowY"};

    public static ObjectAnimator A00(Path path, Property property, Object obj) {
        if (Build.VERSION.SDK_INT >= 21) {
            return ObjectAnimator.ofObject(obj, property, (TypeConverter) null, path);
        }
        return ObjectAnimator.ofFloat(obj, new C02270Ar(path, property), 0.0f, 1.0f);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v13, resolved type: android.animation.AnimatorSet */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0057, code lost:
        if (r11 != r10) goto L_0x0059;
     */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x009d  */
    @Override // X.AnonymousClass072
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.animation.Animator A0Q(android.view.ViewGroup r21, X.C05350Pf r22, X.C05350Pf r23) {
        /*
        // Method dump skipped, instructions count: 300
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass071.A0Q(android.view.ViewGroup, X.0Pf, X.0Pf):android.animation.Animator");
    }

    @Override // X.AnonymousClass072
    public String[] A0S() {
        return A06;
    }

    @Override // X.AnonymousClass072
    public void A0T(C05350Pf r1) {
        A0V(r1);
    }

    @Override // X.AnonymousClass072
    public void A0U(C05350Pf r1) {
        A0V(r1);
    }

    public final void A0V(C05350Pf r7) {
        View view = r7.A00;
        if (AnonymousClass028.A0r(view) || view.getWidth() != 0 || view.getHeight() != 0) {
            Map map = r7.A02;
            map.put("android:changeBounds:bounds", new Rect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom()));
            map.put("android:changeBounds:parent", r7.A00.getParent());
        }
    }
}
