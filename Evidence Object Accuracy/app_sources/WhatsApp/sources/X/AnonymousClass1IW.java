package X;

import android.content.Context;
import android.util.AttributeSet;
import com.whatsapp.WaEditText;

/* renamed from: X.1IW  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1IW extends WaEditText {
    public boolean A00;

    public AnonymousClass1IW(Context context) {
        super(context);
        A02();
    }

    public AnonymousClass1IW(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A02();
    }

    public AnonymousClass1IW(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A02();
    }
}
