package X;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.WaTextView;
import com.whatsapp.jid.UserJid;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.2gs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54532gs extends AnonymousClass02M implements AnonymousClass1lM {
    public C67553Ry A00;
    public final C15570nT A01;
    public final AnonymousClass19Q A02;
    public final C37071lG A03;
    public final AbstractC116525Vu A04;
    public final AnonymousClass018 A05;
    public final C14850m9 A06;
    public final UserJid A07;
    public final List A08 = C12960it.A0l();
    public final List A09 = C12960it.A0l();

    public C54532gs(C15570nT r2, AnonymousClass19Q r3, C37071lG r4, AbstractC116525Vu r5, AnonymousClass018 r6, C14850m9 r7, UserJid userJid) {
        this.A07 = userJid;
        this.A01 = r2;
        this.A05 = r6;
        this.A03 = r4;
        this.A02 = r3;
        this.A06 = r7;
        this.A04 = r5;
    }

    @Override // X.AnonymousClass02M
    public void A0A(AnonymousClass03U r2) {
        if (r2 instanceof C59382ub) {
            ((AbstractC75723kJ) r2).A08();
        }
    }

    @Override // X.AnonymousClass02M
    public void A0B(RecyclerView recyclerView) {
        this.A00 = new C67553Ry(recyclerView, this);
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A09.size();
    }

    public boolean A0E() {
        List list = this.A09;
        return list.size() > 0 && (list.get(0) instanceof AnonymousClass53Y);
    }

    @Override // X.AnonymousClass1lM
    public C44691zO AFw(int i) {
        return ((C1097953b) this.A09.get(i)).A00;
    }

    @Override // X.AnonymousClass02M
    public void ANH(AnonymousClass03U r7, int i) {
        long j;
        int itemViewType = getItemViewType(i);
        if (itemViewType == 1) {
            WaTextView waTextView = ((C75213jU) r7).A00;
            String str = ((C1097853a) this.A09.get(i)).A00;
            waTextView.setText(str);
            waTextView.setContentDescription(C12960it.A0X(waTextView.getContext(), str, new Object[1], 0, R.string.product_list_accessibility_section_header));
        } else if (itemViewType == 0) {
            C44691zO AFw = AFw(i);
            C59302uQ r72 = (C59302uQ) r7;
            String str2 = AFw.A0D;
            Iterator it = this.A08.iterator();
            while (true) {
                if (!it.hasNext()) {
                    j = 0;
                    break;
                }
                C92584Wm r1 = (C92584Wm) it.next();
                if (r1.A01.A0D.equals(str2)) {
                    j = r1.A00;
                    break;
                }
            }
            r72.A09(new C84693zj(AFw, 0, j));
        } else if (itemViewType == 2) {
            new C84633zd(2);
            ((C59382ub) r7).A0A();
        } else if (itemViewType != 3) {
            throw C12960it.A0U(C12960it.A0W(itemViewType, "Unsupported view type - "));
        }
    }

    @Override // X.AnonymousClass02M
    public AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        if (i == 1) {
            return new C75213jU(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.product_list_row));
        }
        if (i == 0) {
            Context context = viewGroup.getContext();
            UserJid userJid = this.A07;
            C15570nT r2 = this.A01;
            AnonymousClass018 r9 = this.A05;
            C37071lG r4 = this.A03;
            AnonymousClass19Q r3 = this.A02;
            AbstractC116525Vu r8 = this.A04;
            View A0F = C12960it.A0F(LayoutInflater.from(context), viewGroup, R.layout.business_product_catalog_list_product);
            AnonymousClass23N.A01(A0F);
            return new C59302uQ(A0F, r2, r3, r4, this, null, null, r8, r9, userJid);
        } else if (i == 2) {
            return new C59382ub(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.product_catalog_placeholder));
        } else {
            if (i == 3) {
                return new C84713zl(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.business_product_catalog_list_loading));
            }
            throw C12960it.A0U(C12960it.A0W(i, "Unsupported view type - "));
        }
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        int type = ((AnonymousClass5TM) this.A09.get(i)).getType();
        if (type == 0) {
            return 0;
        }
        if (type != 1) {
            return type != 2 ? 3 : 2;
        }
        return 1;
    }
}
