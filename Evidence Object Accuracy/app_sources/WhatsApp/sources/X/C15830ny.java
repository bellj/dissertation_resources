package X;

import android.content.Context;
import com.whatsapp.stickers.WebpUtils;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* renamed from: X.0ny  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15830ny implements AbstractC15840nz {
    public final C15570nT A00;
    public final C002701f A01;
    public final C15820nx A02;
    public final C15810nw A03;
    public final C17050qB A04;
    public final C16590pI A05;
    public final C19490uC A06;
    public final C240413z A07;
    public final AnonymousClass15C A08;
    public final C21780xy A09;
    public final AnonymousClass15E A0A;
    public final AnonymousClass15A A0B;
    public final AnonymousClass15B A0C;
    public final C16600pJ A0D;
    public final AnonymousClass15D A0E;

    @Override // X.AbstractC15840nz
    public String AAp() {
        return "stickers-db";
    }

    public C15830ny(C15570nT r1, C002701f r2, C15820nx r3, C15810nw r4, C17050qB r5, C16590pI r6, C19490uC r7, C240413z r8, AnonymousClass15C r9, C21780xy r10, AnonymousClass15E r11, AnonymousClass15A r12, AnonymousClass15B r13, C16600pJ r14, AnonymousClass15D r15) {
        this.A05 = r6;
        this.A0E = r15;
        this.A00 = r1;
        this.A03 = r4;
        this.A06 = r7;
        this.A02 = r3;
        this.A07 = r8;
        this.A04 = r5;
        this.A08 = r9;
        this.A0D = r14;
        this.A0C = r13;
        this.A0A = r11;
        this.A01 = r2;
        this.A0B = r12;
        this.A09 = r10;
    }

    public AnonymousClass1KZ A00(String str) {
        String[] strArr;
        String str2;
        AnonymousClass009.A00();
        AnonymousClass15B r2 = this.A0C;
        if (str == null) {
            strArr = null;
            str2 = "SELECT * FROM downloadable_sticker_packs LEFT JOIN installed_sticker_packs ON (id = installed_id)";
        } else {
            strArr = new String[]{str};
            str2 = "SELECT * FROM downloadable_sticker_packs LEFT JOIN installed_sticker_packs ON (id = installed_id) WHERE id= ?";
        }
        List A00 = r2.A00(str2, strArr);
        if (A00.isEmpty()) {
            return null;
        }
        if (A00.size() < 2) {
            return (AnonymousClass1KZ) A00.get(0);
        }
        StringBuilder sb = new StringBuilder("StickerPack/getDownloadablePackById/there should only be one sticker that matches this id:");
        sb.append(str);
        throw new IllegalStateException(sb.toString());
    }

    public AnonymousClass1KZ A01(String str) {
        String[] strArr;
        String str2;
        String str3;
        AnonymousClass009.A00();
        AnonymousClass15B r2 = this.A0C;
        if (str == null) {
            strArr = null;
            str2 = "SELECT * FROM installed_sticker_packs LEFT JOIN downloadable_sticker_packs ON (installed_id = id)";
        } else {
            strArr = new String[]{str};
            str2 = "SELECT * FROM installed_sticker_packs LEFT JOIN downloadable_sticker_packs ON (installed_id = id) WHERE installed_id= ?";
        }
        List A00 = r2.A00(str2, strArr);
        if (A00.size() <= 0) {
            return null;
        }
        if (A00.size() < 2) {
            AnonymousClass1KZ r5 = (AnonymousClass1KZ) A00.get(0);
            if (r5 == null) {
                return r5;
            }
            List<AnonymousClass1KS> A002 = this.A08.A00(str);
            if (!A002.isEmpty()) {
                for (AnonymousClass1KS r22 : A002) {
                    if (!(r22.A04 != null || (str3 = r22.A08) == null || str3.length() == 0)) {
                        AnonymousClass009.A05(str3);
                        AnonymousClass1KB A003 = AnonymousClass1KB.A00(WebpUtils.fetchWebpMetadata(str3));
                        if (A003 != null) {
                            r22.A04 = A003;
                            r22.A0G = A003.A05;
                            C37471mS[] r0 = A003.A08;
                            if (r0 != null) {
                                r22.A06 = AnonymousClass1KS.A00(r0);
                            }
                        }
                    }
                }
            }
            r5.A04 = A002;
            return r5;
        }
        StringBuilder sb = new StringBuilder("StickerPack/getInstalledPackById/there should only be one sticker that matches this id:");
        sb.append(str);
        throw new IllegalStateException(sb.toString());
    }

    public File A02(EnumC16570pG r18) {
        File file;
        File file2;
        ReentrantReadWriteLock.WriteLock writeLock;
        String obj;
        C17050qB r11 = this.A04;
        if (r11.A02()) {
            Log.i("sticker-db-storage/backup/skip no media or read-only media");
        } else {
            if (r18 == EnumC16570pG.A08) {
                try {
                    file = this.A09.A00.A00("");
                } catch (IOException e) {
                    Log.e("sticker-db-storage/make temp file failed", e);
                    file = null;
                    file2 = null;
                }
            } else {
                C15810nw r1 = this.A03;
                EnumC16570pG r0 = EnumC16570pG.A05;
                File A02 = r1.A02();
                if (r18 == r0) {
                    obj = "stickers.db.crypt1";
                } else {
                    StringBuilder sb = new StringBuilder("stickers.db.crypt");
                    sb.append(r18.version);
                    obj = sb.toString();
                }
                file = new File(A02, obj);
            }
            file2 = file;
            if (file != null) {
                if (file.exists() && file.isDirectory()) {
                    file.delete();
                }
                File parentFile = file.getParentFile();
                AnonymousClass009.A05(parentFile);
                if (!parentFile.exists()) {
                    parentFile.mkdirs();
                }
                List A07 = C32781cj.A07(EnumC16570pG.A05, EnumC16570pG.A00());
                A07.add(".crypt1");
                File file3 = new File(this.A03.A02(), "stickers.db");
                ArrayList A06 = C32781cj.A06(file3, A07);
                C32781cj.A0C(file3, A06);
                Iterator it = A06.iterator();
                while (it.hasNext()) {
                    File file4 = (File) it.next();
                    if (!file4.equals(file2) && file4.exists()) {
                        file4.delete();
                    }
                }
                StringBuilder sb2 = new StringBuilder("sticker-db-storage/backup/to ");
                sb2.append(file2);
                Log.i(sb2.toString());
                ReentrantReadWriteLock reentrantReadWriteLock = this.A07.A04;
                if (reentrantReadWriteLock != null) {
                    writeLock = reentrantReadWriteLock.writeLock();
                } else {
                    writeLock = null;
                }
                try {
                    writeLock.lock();
                    try {
                        AnonymousClass15D r02 = this.A0E;
                        AbstractC33251dh A00 = C33231df.A00(this.A00, new C33211dd(file2), null, this.A02, r11, this.A06, this.A09, this.A0D, r18, r02);
                        Context context = this.A05.A00;
                        AbstractC37461mR A022 = A00.A02(context);
                        if (A022 != null) {
                            try {
                                A022.AgB(context.getDatabasePath("stickers.db"));
                                File[] listFiles = this.A01.A02().listFiles();
                                if (listFiles != null) {
                                    for (File file5 : listFiles) {
                                        A022.AgB(file5);
                                    }
                                }
                                A022.close();
                                return file2;
                            } catch (Throwable th) {
                                try {
                                    A022.close();
                                } catch (Throwable unused) {
                                }
                                throw th;
                            }
                        }
                    } catch (Exception e2) {
                        Log.e("sticker-db-storage/backup failed", e2);
                    }
                    return null;
                } finally {
                    writeLock.unlock();
                }
            }
        }
        return null;
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0041, code lost:
        if (r4 <= 0) goto L_0x0043;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A03(X.AnonymousClass1KZ r10, boolean r11) {
        /*
            r9 = this;
            X.AnonymousClass009.A00()
            X.15B r0 = r9.A0C
            java.lang.String r8 = r10.A0D
            X.13z r0 = r0.A01
            X.0on r5 = r0.A02()
            r0 = 1
            java.lang.String[] r3 = new java.lang.String[r0]     // Catch: all -> 0x007a
            r0 = 0
            r3[r0] = r8     // Catch: all -> 0x007a
            X.0op r2 = r5.A03     // Catch: all -> 0x007a
            java.lang.String r1 = "installed_sticker_packs"
            java.lang.String r0 = "installed_id LIKE ?"
            int r4 = r2.A01(r1, r0, r3)     // Catch: all -> 0x007a
            r5.close()
            X.15C r0 = r9.A08
            X.13z r0 = r0.A00
            X.0on r3 = r0.A02()
            java.lang.String r7 = "sticker_pack_id LIKE ?"
            r0 = 1
            java.lang.String[] r2 = new java.lang.String[r0]     // Catch: all -> 0x0075
            r0 = 0
            r2[r0] = r8     // Catch: all -> 0x0075
            X.0op r1 = r3.A03     // Catch: all -> 0x0075
            java.lang.String r0 = "stickers"
            int r0 = r1.A01(r0, r7, r2)     // Catch: all -> 0x0075
            r3.close()
            r6 = 1
            if (r0 <= 0) goto L_0x0043
            r5 = 1
            if (r4 > 0) goto L_0x0044
        L_0x0043:
            r5 = 0
        L_0x0044:
            if (r11 != 0) goto L_0x0074
            X.15A r4 = r9.A0B
            monitor-enter(r4)
            X.13z r0 = r4.A00     // Catch: all -> 0x0071
            X.0on r3 = r0.A02()     // Catch: all -> 0x0071
            java.lang.String[] r2 = new java.lang.String[r6]     // Catch: all -> 0x006c
            r0 = 0
            r2[r0] = r8     // Catch: all -> 0x006c
            X.0op r1 = r3.A03     // Catch: all -> 0x006c
            java.lang.String r0 = "sticker_pack_order"
            int r1 = r1.A01(r0, r7, r2)     // Catch: all -> 0x006c
            r3.close()     // Catch: all -> 0x0071
            monitor-exit(r4)
            r0 = 0
            if (r1 <= 0) goto L_0x0065
            r0 = 1
        L_0x0065:
            if (r5 == 0) goto L_0x006a
            if (r0 == 0) goto L_0x006a
            return r6
        L_0x006a:
            r6 = 0
            return r6
        L_0x006c:
            r0 = move-exception
            r3.close()     // Catch: all -> 0x0070
        L_0x0070:
            throw r0     // Catch: all -> 0x0071
        L_0x0071:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x0074:
            return r5
        L_0x0075:
            r0 = move-exception
            r3.close()     // Catch: all -> 0x0079
        L_0x0079:
            throw r0
        L_0x007a:
            r0 = move-exception
            r5.close()     // Catch: all -> 0x007e
        L_0x007e:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15830ny.A03(X.1KZ, boolean):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0030, code lost:
        if (r14 == null) goto L_0x0055;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean A04(java.io.File r17) {
        /*
            r16 = this;
            r2 = r16
            monitor-enter(r2)
            X.13z r0 = r2.A07     // Catch: all -> 0x0061
            r0.close()     // Catch: all -> 0x0061
            X.0pI r0 = r2.A05     // Catch: all -> 0x0061
            android.content.Context r0 = r0.A00     // Catch: all -> 0x0061
            java.lang.String r6 = "stickers.db"
            java.io.File r5 = r0.getDatabasePath(r6)     // Catch: all -> 0x0061
            X.01f r0 = r2.A01     // Catch: all -> 0x0061
            java.io.File r4 = r0.A02()     // Catch: all -> 0x0061
            r3 = 0
            r1 = r17
            java.lang.String r0 = r1.getName()     // Catch: Exception -> 0x0058, all -> 0x0061
            int r6 = X.C32781cj.A01(r0, r6)     // Catch: Exception -> 0x0058, all -> 0x0061
            if (r6 <= 0) goto L_0x0055
            X.0pG r14 = X.EnumC16570pG.A05     // Catch: Exception -> 0x0058, all -> 0x0061
            int r0 = r14.version     // Catch: Exception -> 0x0058, all -> 0x0061
            if (r6 < r0) goto L_0x0032
            X.0pG r14 = X.EnumC16570pG.A02(r6)     // Catch: Exception -> 0x0058, all -> 0x0061
            if (r14 == 0) goto L_0x0055
        L_0x0032:
            X.15D r15 = r2.A0E     // Catch: Exception -> 0x0058, all -> 0x0061
            X.0nT r6 = r2.A00     // Catch: Exception -> 0x0058, all -> 0x0061
            X.0uC r11 = r2.A06     // Catch: Exception -> 0x0058, all -> 0x0061
            X.0nx r9 = r2.A02     // Catch: Exception -> 0x0058, all -> 0x0061
            X.0qB r10 = r2.A04     // Catch: Exception -> 0x0058, all -> 0x0061
            X.0pJ r13 = r2.A0D     // Catch: Exception -> 0x0058, all -> 0x0061
            X.0xy r12 = r2.A09     // Catch: Exception -> 0x0058, all -> 0x0061
            r8 = 0
            X.1dd r7 = new X.1dd     // Catch: Exception -> 0x0058, all -> 0x0061
            r7.<init>(r1)     // Catch: Exception -> 0x0058, all -> 0x0061
            X.1dh r1 = X.C33231df.A00(r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)     // Catch: Exception -> 0x0058, all -> 0x0061
            X.1mT r0 = new X.1mT     // Catch: Exception -> 0x0058, all -> 0x0061
            r0.<init>(r5, r4)     // Catch: Exception -> 0x0058, all -> 0x0061
            boolean r0 = r1.A05(r0, r3)     // Catch: Exception -> 0x0058, all -> 0x0061
            monitor-exit(r2)
            return r0
        L_0x0055:
            X.0pG r14 = X.EnumC16570pG.A08     // Catch: Exception -> 0x0058, all -> 0x0061
            goto L_0x0032
        L_0x0058:
            r1 = move-exception
            java.lang.String r0 = "sticker-db-storage/restore failed"
            com.whatsapp.util.Log.e(r0, r1)     // Catch: all -> 0x0061
            monitor-exit(r2)
            return r3
        L_0x0061:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15830ny.A04(java.io.File):boolean");
    }

    @Override // X.AbstractC15840nz
    public boolean A6K() {
        EnumC16570pG r0;
        if (this.A02.A04()) {
            r0 = EnumC16570pG.A07;
        } else {
            r0 = EnumC16570pG.A06;
        }
        return A02(r0) != null;
    }
}
