package X;

/* renamed from: X.31M  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass31M extends AbstractC16110oT {
    public Long A00;
    public Long A01;
    public Long A02;
    public Long A03;
    public Long A04;
    public Long A05;
    public Long A06;
    public Long A07;
    public Long A08;
    public Long A09;
    public Long A0A;
    public Long A0B;
    public Long A0C;
    public Long A0D;

    public AnonymousClass31M() {
        super(2208, new AnonymousClass00E(1, 20, 1000), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(7, this.A00);
        r3.Abe(3, this.A01);
        r3.Abe(14, this.A02);
        r3.Abe(13, this.A03);
        r3.Abe(12, this.A04);
        r3.Abe(10, this.A05);
        r3.Abe(9, this.A06);
        r3.Abe(11, this.A07);
        r3.Abe(8, this.A08);
        r3.Abe(6, this.A09);
        r3.Abe(5, this.A0A);
        r3.Abe(4, this.A0B);
        r3.Abe(2, this.A0C);
        r3.Abe(1, this.A0D);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamAndroidDiskFootprintEvent {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "chatDatabaseSize", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "chatUsageSize", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "externalBackupsSize", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "externalDatabasesSize", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "externalMediaSize", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "externalStorageAvailSize", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "externalStorageTotalSize", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "externalWhatsappFolderSize", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "internalCachedirSize", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "internalDatabasesSize", this.A09);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "internalFilesdirSize", this.A0A);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "internalWhatsappFolderSize", this.A0B);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "storageAvailSize", this.A0C);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "storageTotalSize", this.A0D);
        return C12960it.A0d("}", A0k);
    }
}
