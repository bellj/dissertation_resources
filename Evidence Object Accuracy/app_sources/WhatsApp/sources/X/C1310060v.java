package X;

import android.content.Context;
import com.whatsapp.R;
import java.util.TimeZone;

/* renamed from: X.60v  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1310060v {
    public final C14830m7 A00;
    public final C16590pI A01;
    public final AnonymousClass018 A02;

    public C1310060v(C14830m7 r1, C16590pI r2, AnonymousClass018 r3) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
    }

    public static String A00(C14830m7 r2, AnonymousClass018 r3, long j) {
        return AnonymousClass1MY.A04(r3, r2.A02(j - ((long) TimeZone.getTimeZone("Asia/Kolkata").getRawOffset())));
    }

    public static String A01(C14830m7 r2, C1310060v r3, long j) {
        return AnonymousClass1MY.A04(r3.A02, r2.A02(j - ((long) TimeZone.getTimeZone("Asia/Kolkata").getRawOffset())));
    }

    public static boolean A02(String str) {
        return str != null && !str.equals("ONETIME") && !str.equals("UNKNOWN");
    }

    public String A03(long j) {
        return C12960it.A0X(this.A01.A00, A01(this.A00, this, j), C12970iu.A1b(), 0, R.string.upi_mandate_payment_transaction_detail_date_row_description_date_range);
    }

    public String A04(C30821Yy r6, String str) {
        String AAA = C30771Yt.A05.AAA(this.A02, r6, 0);
        if ("MAX".equals(str)) {
            return C12960it.A0X(this.A01.A00, AAA, C12970iu.A1b(), 0, R.string.upi_mandate_bottom_row_item_amount_upto);
        }
        return AAA;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public String A05(String str) {
        Context context;
        int i;
        if (str != null) {
            switch (str.hashCode()) {
                case -1738378111:
                    if (str.equals("WEEKLY")) {
                        context = this.A01.A00;
                        i = R.string.upi_mandate_bottom_row_item_frequency_weekly;
                        break;
                    }
                    break;
                case -1681232246:
                    if (str.equals("YEARLY")) {
                        context = this.A01.A00;
                        i = R.string.upi_mandate_bottom_row_item_frequency_yearly;
                        break;
                    }
                    break;
                case -602281453:
                    if (str.equals("ONETIME")) {
                        context = this.A01.A00;
                        i = R.string.upi_mandate_bottom_row_item_frequency_once;
                        break;
                    }
                    break;
                case 64808441:
                    if (str.equals("DAILY")) {
                        context = this.A01.A00;
                        i = R.string.upi_mandate_bottom_row_item_frequency_daily;
                        break;
                    }
                    break;
                case 1134556285:
                    if (str.equals("HALFYEARLY")) {
                        context = this.A01.A00;
                        i = R.string.upi_mandate_bottom_row_item_frequency_half_yearly;
                        break;
                    }
                    break;
                case 1271097434:
                    if (str.equals("FORTNIGHTLY")) {
                        context = this.A01.A00;
                        i = R.string.upi_mandate_bottom_row_item_frequency_fortnightly;
                        break;
                    }
                    break;
                case 1297843654:
                    if (str.equals("BIMONTHLY")) {
                        context = this.A01.A00;
                        i = R.string.upi_mandate_bottom_row_item_frequency_bimonthly;
                        break;
                    }
                    break;
                case 1720567065:
                    if (str.equals("QUARTERLY")) {
                        context = this.A01.A00;
                        i = R.string.upi_mandate_bottom_row_item_frequency_quarterly;
                        break;
                    }
                    break;
                case 1896178312:
                    if (str.equals("ASPRESENTED")) {
                        context = this.A01.A00;
                        i = R.string.upi_mandate_bottom_row_item_frequency_as_presented;
                        break;
                    }
                    break;
                case 1954618349:
                    if (str.equals("MONTHLY")) {
                        context = this.A01.A00;
                        i = R.string.upi_mandate_bottom_row_item_frequency_monthly;
                        break;
                    }
                    break;
            }
            return context.getString(i);
        }
        context = this.A01.A00;
        i = R.string.unknown;
        return context.getString(i);
    }
}
