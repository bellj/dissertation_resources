package X;

import android.app.Activity;
import android.content.Context;
import com.whatsapp.R;
import com.whatsapp.util.Log;

/* renamed from: X.5wm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128855wm {
    public final /* synthetic */ Activity A00;
    public final /* synthetic */ AnonymousClass68Z A01;
    public final /* synthetic */ AnonymousClass5US A02;

    public C128855wm(Activity activity, AnonymousClass68Z r2, AnonymousClass5US r3) {
        this.A01 = r2;
        this.A00 = activity;
        this.A02 = r3;
    }

    public void A00(C452120p r7, boolean z) {
        String A0X;
        StringBuilder A0k = C12960it.A0k("PAY: IndiaUpiBlockListManager/on-error blocked: ");
        A0k.append(z);
        Log.e(C12960it.A0Z(r7, " error: ", A0k));
        AnonymousClass68Z r2 = this.A01;
        C14900mE r5 = r2.A02;
        Activity activity = this.A00;
        r5.A0A((AbstractC13860kS) activity);
        if (!activity.isFinishing()) {
            Context context = r2.A05.A00;
            if (z) {
                A0X = context.getString(R.string.block_upi_id_error);
            } else {
                A0X = C12960it.A0X(context, context.getString(R.string.india_upi_payment_id_name), C12970iu.A1b(), 0, R.string.unblock_payment_id_error_default);
            }
            r5.A0L(A0X, 0);
        }
        AnonymousClass5US r0 = this.A02;
        if (r0 != null) {
            r0.AVD(r7);
        }
    }
}
