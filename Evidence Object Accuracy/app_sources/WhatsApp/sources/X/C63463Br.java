package X;

import android.app.Activity;
import com.whatsapp.gifsearch.GifSearchContainer;

/* renamed from: X.3Br  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63463Br {
    public final /* synthetic */ Activity A00;
    public final /* synthetic */ AnonymousClass01d A01;
    public final /* synthetic */ C14820m6 A02;
    public final /* synthetic */ C14850m9 A03;
    public final /* synthetic */ C16120oU A04;
    public final /* synthetic */ C15260mp A05;
    public final /* synthetic */ C15320mw A06;
    public final /* synthetic */ C253719d A07;
    public final /* synthetic */ GifSearchContainer A08;
    public final /* synthetic */ C16630pM A09;
    public final /* synthetic */ C252718t A0A;

    public C63463Br(Activity activity, AnonymousClass01d r2, C14820m6 r3, C14850m9 r4, C16120oU r5, C15260mp r6, C15320mw r7, C253719d r8, GifSearchContainer gifSearchContainer, C16630pM r10, C252718t r11) {
        this.A06 = r7;
        this.A05 = r6;
        this.A08 = gifSearchContainer;
        this.A03 = r4;
        this.A07 = r8;
        this.A0A = r11;
        this.A04 = r5;
        this.A01 = r2;
        this.A02 = r3;
        this.A09 = r10;
        this.A00 = activity;
    }
}
