package X;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.3Ia  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC65073Ia {
    public List A00;
    public List A01;
    public Map A02;
    public Map A03;
    public final EnumC869849t A04;

    public AbstractC65073Ia(EnumC869849t r5) {
        List<C93304Zx> emptyList = Collections.emptyList();
        List<C93304Zx> emptyList2 = Collections.emptyList();
        this.A04 = r5;
        for (C93304Zx r0 : emptyList) {
            A04(r0);
        }
        for (C93304Zx r2 : emptyList2) {
            List list = this.A00;
            if (list == null) {
                list = C12960it.A0l();
                this.A00 = list;
                if (this.A02 == null) {
                    this.A02 = C12970iu.A11();
                } else {
                    throw C12960it.A0U("Extension Map and Extension List out of sync!");
                }
            }
            A00(r2, list, this.A02);
        }
    }

    public static void A00(C93304Zx r3, List list, Map map) {
        Class<?> cls = r3.A00.getClass();
        if (map.put(cls, r3) != null) {
            for (int size = list.size() - 1; size >= 0; size--) {
                if (((C93304Zx) list.get(size)).A00.getClass() == cls) {
                    list.remove(size);
                }
            }
            throw C12960it.A0U("Extension Map and Extension List out of sync!");
        }
        list.add(r3);
    }

    public static void A01(Object obj, Object obj2, List list, List list2, List list3, List list4, Map map) {
        if (list == null || list.isEmpty()) {
            if (list2 != null) {
                list3.addAll(list2);
            }
        } else if (list2 == null || list2.isEmpty()) {
            list4.addAll(list);
        } else {
            HashMap hashMap = new HashMap(list2.size());
            Iterator it = list2.iterator();
            while (it.hasNext()) {
                C93304Zx r5 = (C93304Zx) it.next();
                AnonymousClass5WW r3 = r5.A00;
                Class<?> cls = r3.getClass();
                C93304Zx r0 = (C93304Zx) map.get(cls);
                if (r0 != null) {
                    boolean Adc = r3.Adc(r0.A01, r5.A01, obj, obj2);
                    hashMap.put(cls, Boolean.valueOf(Adc));
                    if (Adc) {
                    }
                }
                list3.add(r5);
            }
            Iterator it2 = list.iterator();
            while (it2.hasNext()) {
                C93304Zx r2 = (C93304Zx) it2.next();
                Class<?> cls2 = r2.A00.getClass();
                if (!hashMap.containsKey(cls2) || C12970iu.A1Y(hashMap.get(cls2))) {
                    list4.add(r2);
                }
            }
        }
    }

    public long A02() {
        if (this instanceof C55992k9) {
            return ((C55992k9) this).A03;
        }
        if (this instanceof AnonymousClass2k7) {
            return ((AnonymousClass2k7) this).A01;
        }
        if (!(this instanceof AnonymousClass2k8)) {
            return ((C56002kA) this).A0D;
        }
        return ((AnonymousClass2k8) this).A00;
    }

    public String A03() {
        Class<?> cls = getClass();
        String name = cls.getName();
        if (name.length() > 80) {
            return cls.getSimpleName();
        }
        StringBuilder A0k = C12960it.A0k("<cls>");
        A0k.append(name);
        return C12960it.A0d("</cls>", A0k);
    }

    public void A04(C93304Zx r3) {
        List list = this.A01;
        if (list == null) {
            list = C12960it.A0l();
            this.A01 = list;
            if (this.A03 == null) {
                this.A03 = C12970iu.A11();
            } else {
                throw C12960it.A0U("Extension Map and Extension List out of sync!");
            }
        }
        A00(r3, list, this.A03);
    }
}
