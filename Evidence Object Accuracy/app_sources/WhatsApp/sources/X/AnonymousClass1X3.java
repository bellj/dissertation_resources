package X;

/* renamed from: X.1X3  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1X3 extends AnonymousClass1X4 implements AbstractC16400ox, AbstractC16420oz {
    public AnonymousClass1X3(C16150oX r1, AnonymousClass1IS r2, AbstractC16130oV r3, byte b, long j, boolean z) {
        super(r1, r2, r3, b, j, z);
    }

    public AnonymousClass1X3(AnonymousClass1IS r1, byte b, long j) {
        super(r1, b, j);
    }

    public AnonymousClass1X3(C40851sR r1, AnonymousClass1IS r2, byte b, long j, boolean z, boolean z2) {
        super(r1, r2, b, j, z, z2);
    }
}
