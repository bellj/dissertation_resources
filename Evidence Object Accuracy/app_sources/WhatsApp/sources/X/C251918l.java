package X;

import com.whatsapp.voipcalling.GlVideoRenderer;
import org.chromium.net.UrlRequest;

/* renamed from: X.18l */
/* loaded from: classes2.dex */
public class C251918l {
    public static /* synthetic */ AnonymousClass1RC A00(int i, int i2) {
        if (i == 1) {
            return AnonymousClass1RC.OK;
        }
        if (i == 2) {
            return AnonymousClass1RC.YES;
        }
        if (i != 3) {
            if (i == 4) {
                return AnonymousClass1RC.YES_WITH_CODE;
            }
        } else if (i2 == 2) {
            return AnonymousClass1RC.ERROR_BLOCKED;
        } else {
            if (i2 == 26) {
                return AnonymousClass1RC.ERROR_LIMITED_RELEASE;
            }
            if (i2 == 23) {
                return AnonymousClass1RC.SECURITY_CODE;
            }
            if (i2 == 24) {
                return AnonymousClass1RC.ERROR_INVALID_SKEY_SIGNATURE;
            }
            switch (i2) {
                case 6:
                    return AnonymousClass1RC.ERROR_TEMPORARILY_UNAVAILABLE;
                case 7:
                    return AnonymousClass1RC.ERROR_OLD_VERSION;
                case 8:
                    return AnonymousClass1RC.ERROR_TOO_RECENT;
                case 9:
                    return AnonymousClass1RC.ERROR_TOO_MANY;
                case 10:
                    return AnonymousClass1RC.ERROR_NEXT_METHOD;
                case 11:
                    return AnonymousClass1RC.ERROR_TOO_MANY_GUESSES;
                case 12:
                    return AnonymousClass1RC.ERROR_BAD_PARAMETER;
                case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                    return AnonymousClass1RC.ERROR_MISSING_PARAMETER;
                case UrlRequest.Status.READING_RESPONSE /* 14 */:
                    return AnonymousClass1RC.ERROR_PROVIDER_TIMEOUT;
                case 15:
                    return AnonymousClass1RC.ERROR_PROVIDER_UNROUTABLE;
                case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                    return AnonymousClass1RC.ERROR_BAD_TOKEN;
                case 17:
                    return AnonymousClass1RC.ERROR_TOO_MANY_ALL_METHODS;
                case 18:
                    return AnonymousClass1RC.ERROR_NO_ROUTES;
                default:
                    switch (i2) {
                        case 29:
                            return AnonymousClass1RC.ERROR_FLASH_CALL_DISABLED;
                        case C25991Bp.A0S /* 30 */:
                            return AnonymousClass1RC.ERROR_DEVICE_CONFIRM_OR_SECOND_OTP;
                        case 31:
                            return AnonymousClass1RC.ERROR_SECOND_OTP;
                        case 32:
                            return AnonymousClass1RC.ERROR_NOT_ALLOWED;
                    }
            }
        }
        return AnonymousClass1RC.ERROR_UNSPECIFIED;
    }

    public static /* synthetic */ AnonymousClass1R8 A01(int i, int i2) {
        if (i == 1) {
            return AnonymousClass1R8.YES;
        }
        if (i != 3) {
            if (i == 5) {
                return AnonymousClass1R8.VERIFIED_STANDALONE;
            }
        } else if (i2 == 2) {
            return AnonymousClass1R8.FAIL_BLOCKED;
        } else {
            if (i2 == 6) {
                return AnonymousClass1R8.FAIL_TEMPORARILY_UNAVAILABLE;
            }
            if (i2 == 11) {
                return AnonymousClass1R8.FAIL_TOO_MANY_GUESSES;
            }
            if (i2 == 26) {
                return AnonymousClass1R8.ERROR_LIMITED_RELEASE;
            }
            switch (i2) {
                case 19:
                    return AnonymousClass1R8.FAIL_MISMATCH;
                case C43951xu.A01 /* 20 */:
                    return AnonymousClass1R8.FAIL_GUESSED_TOO_FAST;
                case 21:
                    return AnonymousClass1R8.FAIL_MISSING;
                case 22:
                    return AnonymousClass1R8.FAIL_STALE;
                case 23:
                    return AnonymousClass1R8.SECURITY_CODE;
                default:
                    switch (i2) {
                        case C25991Bp.A0S /* 30 */:
                            return AnonymousClass1R8.DEVICE_CONFIRM_OR_SECOND_OTP;
                        case 31:
                            return AnonymousClass1R8.SECOND_OTP;
                        case 32:
                            return AnonymousClass1R8.FAIL_NOT_ALLOWED;
                    }
            }
        }
        return AnonymousClass1R8.ERROR_UNSPECIFIED;
    }
}
