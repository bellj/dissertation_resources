package X;

import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.stickers.StickerStoreActivity;
import com.whatsapp.stickers.StickerStoreTabFragment;

/* renamed from: X.3S6  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3S6 implements AnonymousClass070 {
    public final /* synthetic */ StickerStoreActivity A00;

    @Override // X.AnonymousClass070
    public void ATO(int i) {
    }

    @Override // X.AnonymousClass070
    public void ATP(int i, float f, int i2) {
    }

    public AnonymousClass3S6(StickerStoreActivity stickerStoreActivity) {
        this.A00 = stickerStoreActivity;
    }

    @Override // X.AnonymousClass070
    public void ATQ(int i) {
        int i2 = 0;
        while (true) {
            StickerStoreActivity stickerStoreActivity = this.A00;
            if (i2 < stickerStoreActivity.A05.A00.size()) {
                boolean A1V = C12960it.A1V(i, i2);
                RecyclerView recyclerView = ((StickerStoreTabFragment) stickerStoreActivity.A05.A0G(i2)).A04;
                if (recyclerView != null) {
                    recyclerView.setNestedScrollingEnabled(A1V);
                }
                i2++;
            } else {
                stickerStoreActivity.A01.requestLayout();
                return;
            }
        }
    }
}
