package X;

import java.util.Collection;
import java.util.Iterator;

/* renamed from: X.1bP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C31971bP {
    public static String[] A00(Collection collection) {
        String[] strArr = new String[collection.size() * 3];
        Iterator it = collection.iterator();
        int i = 0;
        while (it.hasNext()) {
            C15950oC r3 = (C15950oC) it.next();
            int i2 = i * 3;
            strArr[i2] = r3.A02;
            strArr[i2 + 1] = String.valueOf(r3.A01);
            strArr[i2 + 2] = String.valueOf(r3.A00);
            i++;
        }
        return strArr;
    }
}
