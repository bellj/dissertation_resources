package X;

/* renamed from: X.5F8  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5F8 implements AnonymousClass5VL {
    @Override // X.AnonymousClass5VL
    public boolean ALX(String str) {
        char charAt;
        if (str != null) {
            int length = str.length();
            if (length != 0 && str.trim() == str && (((charAt = str.charAt(0)) < '0' || charAt > '9') && charAt != '-')) {
                int i = 0;
                while (true) {
                    if (i >= length) {
                        if (C95224dL.A02(str)) {
                            break;
                        }
                    } else {
                        char charAt2 = str.charAt(i);
                        if (charAt2 == '\r' || charAt2 == '\n' || charAt2 == '\t' || charAt2 == ' ' || C95224dL.A00(charAt2) || charAt2 == '\b' || charAt2 == '\f' || charAt2 == '\n' || C95224dL.A01(charAt2)) {
                            break;
                        }
                        i++;
                    }
                }
            }
            return true;
        }
        return false;
    }
}
