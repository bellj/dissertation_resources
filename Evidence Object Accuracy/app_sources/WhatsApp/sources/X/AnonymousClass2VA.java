package X;

import android.content.res.Resources;
import android.graphics.Rect;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import com.whatsapp.InfoCard;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaTextView;
import com.whatsapp.biz.BusinessProfileFieldView;
import com.whatsapp.biz.catalog.view.CatalogMediaCard;
import com.whatsapp.biz.profile.TrustSignalItem;
import com.whatsapp.chatinfo.ContactInfoActivity;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* renamed from: X.2VA  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2VA implements AbstractC13940ka {
    public View A00;
    public View A01;
    public View A02;
    public View A03;
    public TextView A04;
    public InfoCard A05;
    public InfoCard A06;
    public TextEmojiLabel A07;
    public TextEmojiLabel A08;
    public TextEmojiLabel A09;
    public WaTextView A0A;
    public AnonymousClass3IU A0B;
    public BusinessProfileFieldView A0C;
    public CatalogMediaCard A0D;
    public TrustSignalItem A0E;
    public TrustSignalItem A0F;
    public AnonymousClass2VB A0G;
    public C30141Wg A0H;
    public C15370n3 A0I;
    public C68193Ul A0J;
    public boolean A0K;
    public boolean A0L = true;
    public boolean A0M = true;
    public final View A0N;
    public final AnonymousClass12P A0O;
    public final C14900mE A0P;
    public final AnonymousClass18U A0Q;
    public final C15570nT A0R;
    public final C254719n A0S;
    public final C14650lo A0T;
    public final C246716k A0U;
    public final AnonymousClass19Q A0V;
    public final C25781Au A0W;
    public final AnonymousClass3Dn A0X;
    public final ContactInfoActivity A0Y;
    public final AnonymousClass2Ew A0Z;
    public final C22700zV A0a;
    public final C15610nY A0b;
    public final AnonymousClass01d A0c;
    public final C14830m7 A0d;
    public final C16590pI A0e;
    public final C17170qN A0f;
    public final AnonymousClass018 A0g;
    public final C14850m9 A0h;
    public final C16120oU A0i;
    public final C244415n A0j;
    public final C17220qS A0k;
    public final C63393Bk A0l;
    public final AnonymousClass12U A0m;
    public final AnonymousClass19Z A0n;
    public final AnonymousClass18W A0o;
    public final AnonymousClass18X A0p;
    public final Integer A0q;

    public AnonymousClass2VA(View view, AnonymousClass12P r15, C14900mE r16, AnonymousClass18U r17, C15570nT r18, C254719n r19, C14650lo r20, C246716k r21, AnonymousClass19Q r22, C25781Au r23, AnonymousClass2VB r24, AnonymousClass3Dn r25, ContactInfoActivity contactInfoActivity, AnonymousClass2Ew r27, C22700zV r28, C15610nY r29, AnonymousClass01d r30, C14830m7 r31, C16590pI r32, C17170qN r33, AnonymousClass018 r34, C15370n3 r35, C14850m9 r36, C16120oU r37, C244415n r38, C17220qS r39, C63393Bk r40, AnonymousClass12U r41, AnonymousClass19Z r42, AnonymousClass18W r43, AnonymousClass18X r44, Integer num) {
        this.A0d = r31;
        this.A0h = r36;
        this.A0o = r43;
        this.A0P = r16;
        this.A0R = r18;
        this.A0e = r32;
        this.A0m = r41;
        this.A0i = r37;
        this.A0n = r42;
        this.A0Q = r17;
        this.A0O = r15;
        this.A0j = r38;
        this.A0k = r39;
        this.A0b = r29;
        this.A0c = r30;
        this.A0g = r34;
        this.A0Z = r27;
        this.A0p = r44;
        this.A0l = r40;
        this.A0a = r28;
        this.A0U = r21;
        this.A0S = r19;
        this.A0T = r20;
        this.A0V = r22;
        this.A0f = r33;
        this.A0W = r23;
        this.A0X = r25;
        this.A0G = r24;
        AnonymousClass009.A03(view);
        this.A01 = view.findViewById(R.id.business_verification_status);
        this.A09 = (TextEmojiLabel) AnonymousClass028.A0D(view, R.id.business_verification_status_text);
        this.A0C = (BusinessProfileFieldView) AnonymousClass028.A0D(view, R.id.business_description);
        this.A0D = (CatalogMediaCard) AnonymousClass028.A0D(view, R.id.business_catalog_media_card);
        this.A06 = (InfoCard) AnonymousClass028.A0D(view, R.id.business_catalog_shop_info_card);
        this.A03 = AnonymousClass028.A0D(view, R.id.shops_container);
        this.A04 = (TextView) AnonymousClass028.A0D(view, R.id.blank_business_details_text);
        this.A00 = AnonymousClass028.A0D(view, R.id.add_business_to_contact);
        this.A05 = (InfoCard) AnonymousClass028.A0D(view, R.id.business_chaining_container);
        ((AbstractC58392on) AnonymousClass028.A0D(view, R.id.business_chaining_layout)).setTitle(contactInfoActivity.getString(R.string.biz_dir_see_more_like_this));
        this.A08 = (TextEmojiLabel) AnonymousClass028.A0D(view, R.id.business_title);
        this.A07 = (TextEmojiLabel) AnonymousClass028.A0D(view, R.id.business_subtitle);
        this.A0A = (WaTextView) AnonymousClass028.A0D(view, R.id.custom_url);
        this.A0q = num;
        this.A0Y = contactInfoActivity;
        this.A0N = view;
        this.A0I = r35;
        C14850m9 r1 = this.A0h;
        if (r1.A07(1483) || r1.A07(1849)) {
            UserJid A03 = A03();
            AnonymousClass009.A05(A03);
            WaTextView waTextView = this.A0A;
            waTextView.setVisibility(0);
            waTextView.setText(R.string.loading_spinner);
            AnonymousClass3ZN r7 = new AnonymousClass3ZN(this.A0P, new C90064Ml(this, A03), this.A0f, this.A0k);
            String rawString = A03.getRawString();
            C17220qS r6 = r7.A03;
            String A01 = r6.A01();
            C41141sy r2 = new C41141sy("user");
            r2.A04(new AnonymousClass1W9("jid", rawString));
            r6.A0A(r7, new AnonymousClass1V8(r2.A03(), "iq", new AnonymousClass1W9[]{new AnonymousClass1W9(AnonymousClass1VY.A00, "to"), new AnonymousClass1W9("id", A01), new AnonymousClass1W9("type", "get"), new AnonymousClass1W9("xmlns", "fb:thrift_iq"), new AnonymousClass1W9("smax_id", "78")}), A01, 316, 32000);
            StringBuilder sb = new StringBuilder("GetCustomUrlsByJidProtocol/sendRequest/jid=");
            sb.append(rawString);
            Log.i(sb.toString());
            return;
        }
        this.A0A.setVisibility(8);
    }

    public static void A00(AnonymousClass12P r7, TrustSignalItem trustSignalItem, C25781Au r9, AnonymousClass2VB r10, C30221Wo r11, Integer num, String str, boolean z, boolean z2) {
        String str2;
        trustSignalItem.setUpFromAccount(r11);
        if (r11 != null) {
            StringBuilder sb = new StringBuilder("https://");
            int i = trustSignalItem.A00;
            if (i == 0) {
                sb.append("www.facebook.com/");
                str2 = r11.A02;
            } else if (i == 1) {
                sb.append("www.instagram.com/");
                str2 = r11.A01;
            } else {
                return;
            }
            sb.append(str2);
            trustSignalItem.setOnClickListener(new View.OnClickListener(Uri.parse(sb.toString()), r7, trustSignalItem, r9, r10, r11, num, str, z2, z) { // from class: X.2jv
                public final /* synthetic */ Uri A00;
                public final /* synthetic */ AnonymousClass12P A01;
                public final /* synthetic */ TrustSignalItem A02;
                public final /* synthetic */ C25781Au A03;
                public final /* synthetic */ AnonymousClass2VB A04;
                public final /* synthetic */ C30221Wo A05;
                public final /* synthetic */ Integer A06;
                public final /* synthetic */ String A07;
                public final /* synthetic */ boolean A08;
                public final /* synthetic */ boolean A09;

                {
                    this.A03 = r4;
                    this.A07 = r8;
                    this.A06 = r7;
                    this.A05 = r6;
                    this.A02 = r3;
                    this.A08 = r9;
                    this.A09 = r10;
                    this.A04 = r5;
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    C25781Au r6 = this.A03;
                    String str3 = this.A07;
                    Integer num2 = this.A06;
                    C30221Wo r0 = this.A05;
                    TrustSignalItem trustSignalItem2 = this.A02;
                    boolean z3 = this.A08;
                    boolean z4 = this.A09;
                    AnonymousClass2VB r1 = this.A04;
                    AnonymousClass12P r3 = this.A01;
                    Uri uri = this.A00;
                    r6.A04(num2, Integer.valueOf(r0.A00), str3, 15, trustSignalItem2.A00, z3);
                    if (z4) {
                        r6.A01(r1, 11);
                    }
                    r3.A06(trustSignalItem2.getContext(), C12970iu.A0B(uri));
                }
            });
        }
    }

    public static /* synthetic */ void A01(AnonymousClass2VA r2, int i) {
        if (r2.A0I.A0J()) {
            r2.A0W.A01(r2.A0G, i);
        }
    }

    public static boolean A02(View view) {
        if (view == null || !view.isShown()) {
            return false;
        }
        Rect rect = new Rect();
        view.getGlobalVisibleRect(rect);
        return rect.intersects(0, 0, Resources.getSystem().getDisplayMetrics().widthPixels, Resources.getSystem().getDisplayMetrics().heightPixels);
    }

    public UserJid A03() {
        C15370n3 r1 = this.A0I;
        if (r1 == null) {
            return null;
        }
        return (UserJid) r1.A0B(UserJid.class);
    }

    public final void A04(int i) {
        if (this.A0I.A0J()) {
            this.A0W.A05(null, this.A0q, C15380n4.A03(A03()), i, A09(), A08());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:132:0x02a7, code lost:
        if (r2.A01 == null) goto L_0x026e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:137:0x030b, code lost:
        if (r33.A0F.getVisibility() != 0) goto L_0x030d;
     */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x0285  */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x0297  */
    /* JADX WARNING: Removed duplicated region for block: B:136:0x0304  */
    /* JADX WARNING: Removed duplicated region for block: B:144:0x0325  */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x0381  */
    /* JADX WARNING: Removed duplicated region for block: B:256:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00c9  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0144  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0169  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0185  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x018c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A05(X.AnonymousClass2VB r34, X.C15370n3 r35, X.AbstractView$OnClickListenerC34281fs r36, boolean r37, boolean r38, boolean r39) {
        /*
        // Method dump skipped, instructions count: 1553
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2VA.A05(X.2VB, X.0n3, X.1fs, boolean, boolean, boolean):void");
    }

    public final void A06(AnonymousClass2Ew r16) {
        C30221Wo r0;
        Integer num;
        C30221Wo r02;
        Integer num2 = null;
        if (this.A0M || this.A0L || this.A0K) {
            C30141Wg r03 = this.A0H;
            if (r03 != null) {
                C30231Wp r1 = r03.A02;
                if (this.A0L && A02(this.A0E)) {
                    if (r1 == null || (r02 = r1.A00) == null) {
                        num = null;
                    } else {
                        num = Integer.valueOf(r02.A00);
                    }
                    this.A0W.A04(this.A0q, num, C15380n4.A03(A03()), 16, 0, A08());
                    this.A0L = false;
                }
                if (this.A0M && A02(this.A0F)) {
                    if (!(r1 == null || (r0 = r1.A01) == null)) {
                        num2 = Integer.valueOf(r0.A00);
                    }
                    this.A0W.A04(this.A0q, num2, C15380n4.A03(A03()), 16, 1, A08());
                    this.A0M = false;
                }
                if (this.A0K && A02(this.A05)) {
                    C25781Au r3 = this.A0W;
                    boolean z = false;
                    if (this.A0I.A0C != null) {
                        z = true;
                    }
                    r3.A02(this.A0G, 14, z);
                    this.A0K = false;
                    return;
                }
                return;
            }
            return;
        }
        r16.A0K = null;
    }

    public void A07(UserJid userJid, boolean z) {
        C30141Wg r0 = this.A0H;
        if (r0 == null || (r0.A0K && this.A0h.A07(957))) {
            this.A0D.setVisibility(8);
            this.A03.setVisibility(8);
            this.A06.setVisibility(8);
            return;
        }
        CatalogMediaCard catalogMediaCard = this.A0D;
        catalogMediaCard.A0B = new C1099753t(this);
        C254719n r4 = this.A0S;
        C30141Wg r2 = this.A0H;
        C14850m9 r1 = r4.A01;
        if ((!r1.A07(355) || !r1.A07(636)) && r4.A00(r2)) {
            if (catalogMediaCard.getVisibility() == 0) {
                catalogMediaCard.setVisibility(8);
            }
            C30141Wg r22 = this.A0H;
            this.A03.setVisibility(0);
            this.A06.setVisibility(0);
            AnonymousClass01F A0V = this.A0Y.A0V();
            if (A0V.A0A("shops_product_frag") == null) {
                AnonymousClass18W r12 = this.A0o;
                String str = r22.A0B;
                AnonymousClass009.A05(str);
                AnonymousClass01E A8Y = r12.A8Y(str);
                C004902f r13 = new C004902f(A0V);
                r13.A0A(A8Y, "shops_product_frag", R.id.shop_product_container);
                r13.A02();
                return;
            }
            return;
        }
        View view = this.A03;
        if (view.getVisibility() == 0) {
            view.setVisibility(8);
        }
        C30141Wg r14 = this.A0H;
        if (r14.A0I || r4.A01(r14)) {
            catalogMediaCard.setVisibility(0);
            this.A06.setVisibility(0);
            catalogMediaCard.A03(this.A0H, userJid, null, z, false);
            return;
        }
        catalogMediaCard.setVisibility(8);
    }

    public boolean A08() {
        C30181Wk r0;
        C30141Wg r02 = this.A0H;
        return (r02 == null || (r0 = r02.A01) == null || TextUtils.isEmpty(r0.A00)) ? false : true;
    }

    public boolean A09() {
        C30231Wp r1;
        C30141Wg r0 = this.A0H;
        if (r0 == null || (r1 = r0.A02) == null) {
            return false;
        }
        return (r1.A00 == null && r1.A01 == null) ? false : true;
    }

    @Override // X.AbstractC13940ka
    public void AR0() {
        ContactInfoActivity contactInfoActivity = this.A0Y;
        if (contactInfoActivity != null) {
            contactInfoActivity.AaN();
            contactInfoActivity.A13.A06("profile_view_tag", false);
        }
    }

    @Override // X.AbstractC13940ka
    public void AR1() {
        ContactInfoActivity contactInfoActivity = this.A0Y;
        if (contactInfoActivity != null) {
            contactInfoActivity.AaN();
            contactInfoActivity.A13.A06("profile_view_tag", true);
        }
    }
}
