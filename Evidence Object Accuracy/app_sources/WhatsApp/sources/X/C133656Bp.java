package X;

import com.whatsapp.jid.UserJid;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.6Bp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C133656Bp implements AbstractC16960q2 {
    public final C15550nR A00;
    public final C20300vX A01;
    public final C17070qD A02;
    public final C18590sh A03;

    @Override // X.AbstractC16960q2
    public Class A9u() {
        return EnumC124645ps.class;
    }

    public C133656Bp(C15550nR r1, C20300vX r2, C17070qD r3, C18590sh r4) {
        this.A00 = r1;
        this.A03 = r4;
        this.A02 = r3;
        this.A01 = r2;
    }

    @Override // X.AbstractC16960q2
    public /* bridge */ /* synthetic */ Object Aam(Enum r8, Object obj, Map map) {
        Object obj2;
        Object obj3;
        Number number;
        Object obj4;
        long A0G;
        AbstractC30791Yv r0;
        Object obj5;
        String str;
        Object obj6;
        String str2;
        Object obj7;
        String str3;
        Object obj8;
        Object obj9;
        Boolean bool;
        AbstractC28901Pl r4;
        EnumC124645ps r82 = (EnumC124645ps) r8;
        C16700pc.A0E(r82, 1);
        switch (C117315Zl.A01(r82, C125275qz.A00)) {
            case 1:
                if (map == null) {
                    obj8 = null;
                } else {
                    obj8 = map.get("scenario");
                }
                if (!(obj8 instanceof String)) {
                    obj8 = null;
                }
                if (map == null) {
                    obj9 = null;
                } else {
                    obj9 = map.get("is_debit");
                }
                if (obj9 instanceof Boolean) {
                    bool = (Boolean) obj9;
                } else {
                    bool = null;
                }
                if (obj8 == null || bool == null) {
                    return null;
                }
                boolean equals = "P2P".equals(obj8);
                if (equals && bool.booleanValue()) {
                    C17070qD r02 = this.A02;
                    r02.A03();
                    return r02.A09.A06();
                } else if (!equals || bool.booleanValue()) {
                    boolean equals2 = "P2M".equals(obj8);
                    if (equals2 && bool.booleanValue()) {
                        Iterator it = C117295Zj.A0Z(this.A02).iterator();
                        while (it.hasNext()) {
                            AbstractC28901Pl A0H = C117305Zk.A0H(it);
                            if (A0H.A03 == 2) {
                                return A0H;
                            }
                        }
                        return null;
                    } else if (!equals2 || bool.booleanValue()) {
                        return null;
                    } else {
                        C17070qD r03 = this.A02;
                        r03.A03();
                        C241414j r3 = r03.A09;
                        synchronized (r3) {
                            Iterator it2 = r3.A0B().iterator();
                            while (true) {
                                if (it2.hasNext()) {
                                    r4 = C117305Zk.A0H(it2);
                                    if (r4.A02 == 2) {
                                    }
                                } else {
                                    r4 = null;
                                }
                            }
                        }
                        return r4;
                    }
                } else {
                    Iterator it3 = C117295Zj.A0Z(this.A02).iterator();
                    while (it3.hasNext()) {
                        AbstractC28901Pl A0H2 = C117305Zk.A0H(it3);
                        if (A0H2.A00 == 2) {
                            return A0H2;
                        }
                    }
                    return null;
                }
            case 2:
                if (map == null) {
                    obj7 = null;
                } else {
                    obj7 = map.get("credential_id");
                }
                if (obj7 instanceof String) {
                    str3 = (String) obj7;
                } else {
                    str3 = null;
                }
                if (str3 != null) {
                    return C241414j.A00(str3, C117295Zj.A0Z(this.A02));
                }
                return null;
            case 3:
                return C117295Zj.A0Z(this.A02);
            case 4:
                if (map == null) {
                    obj6 = null;
                } else {
                    obj6 = map.get("jid");
                }
                if (obj6 instanceof String) {
                    str2 = (String) obj6;
                } else {
                    str2 = null;
                }
                UserJid nullable = UserJid.getNullable(str2);
                if (nullable != null) {
                    return this.A00.A0B(nullable);
                }
                return null;
            case 5:
                if (map == null) {
                    obj5 = null;
                } else {
                    obj5 = map.get("message_id");
                }
                if (obj5 instanceof String) {
                    str = (String) obj5;
                } else {
                    str = null;
                }
                if (str != null) {
                    return this.A01.A00(str);
                }
                return null;
            case 6:
                String A01 = this.A03.A01();
                C16700pc.A0B(A01);
                return new C129785yI(A01);
            case 7:
                if (map == null) {
                    obj2 = null;
                } else {
                    obj2 = map.get("currency_code");
                }
                if (!(obj2 instanceof String)) {
                    obj2 = null;
                }
                if (map == null) {
                    obj3 = null;
                } else {
                    obj3 = map.get("offset");
                }
                if (obj3 instanceof Integer) {
                    number = (Number) obj3;
                } else {
                    number = null;
                }
                if (number == null) {
                    return null;
                }
                int intValue = number.intValue();
                if (map == null) {
                    obj4 = null;
                } else {
                    obj4 = map.get("value");
                }
                if (obj4 instanceof Integer) {
                    A0G = (long) C12960it.A05(obj4);
                } else if (!(obj4 instanceof Long)) {
                    return null;
                } else {
                    A0G = C12980iv.A0G(obj4);
                }
                C94074bD r1 = new C94074bD();
                r1.A02 = A0G;
                r1.A01 = intValue;
                if (C16700pc.A0O(obj2, "INR")) {
                    r0 = C30771Yt.A05;
                } else if (!C16700pc.A0O(obj2, "BRL")) {
                    return null;
                } else {
                    r0 = C30771Yt.A04;
                }
                r1.A03 = r0;
                return r1.A00();
            default:
                throw new C113285Gx();
        }
    }
}
