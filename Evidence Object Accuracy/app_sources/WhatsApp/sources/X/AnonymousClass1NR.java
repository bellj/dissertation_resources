package X;

import javax.net.ssl.SSLException;

/* renamed from: X.1NR  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1NR extends Exception {
    public final byte description;
    public final boolean errorTransient = false;
    public final SSLException ex;

    public AnonymousClass1NR(SSLException sSLException, byte b) {
        this.description = b;
        this.ex = sSLException;
    }

    public AnonymousClass1NR(SSLException sSLException, byte b, boolean z) {
        this.description = b;
        this.ex = sSLException;
    }

    public static AnonymousClass1NR A00(String str, byte b) {
        return new AnonymousClass1NR(new SSLException(str), b);
    }

    public static AnonymousClass1NR A01(String str, Throwable th, byte b) {
        return new AnonymousClass1NR(new SSLException(str, th), b);
    }
}
