package X;

/* renamed from: X.1EK  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1EK {
    public final C241114g A00;
    public final C14850m9 A01;

    public AnonymousClass1EK(C241114g r1, C14850m9 r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public synchronized void A00(C38101nW r5) {
        C38091nV A00;
        AbstractC16130oV r3 = r5.A04;
        if (!r5.A04() && (A00 = this.A00.A00(r3.A0z)) != null) {
            byte[] bArr = A00.A00;
            int[] iArr = A00.A01;
            synchronized (r5) {
                if (!r5.A00) {
                    r5.A02(bArr, iArr);
                }
            }
            r5.A05 = C30041Vv.A0U(this.A01, r3);
        }
    }

    public synchronized boolean A01(AbstractC15340mz r2) {
        boolean A01;
        C38101nW A14;
        if (r2 == null) {
            A01 = false;
        } else if (!(r2 instanceof AbstractC16130oV) || (A14 = ((AbstractC16130oV) r2).A14()) == null || A14.A04()) {
            A01 = A01(r2.A0E());
        } else {
            A01 = true;
        }
        return A01;
    }
}
