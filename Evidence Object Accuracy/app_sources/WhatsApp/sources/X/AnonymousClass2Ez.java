package X;

/* renamed from: X.2Ez  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Ez {
    public int A00 = 0;
    public int A01 = 0;
    public int A02 = 0;
    public int A03 = 0;
    public long A04 = -1;

    public AnonymousClass2Ez() {
    }

    public AnonymousClass2Ez(int i, long j, int i2) {
        this.A04 = j;
        this.A01 = i;
        this.A00 = i2;
    }

    public AnonymousClass2Ez(long j, int i) {
        this.A04 = j;
        this.A01 = i;
    }
}
