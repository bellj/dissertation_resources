package X;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Parcelable;
import android.provider.ContactsContract;
import android.text.TextUtils;
import com.facebook.redex.RunnableBRunnable0Shape2S0200000_I0_2;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.190  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass190 {
    public final C14900mE A00;
    public final C15570nT A01;
    public final C18850tA A02;
    public final AnonymousClass116 A03;
    public final C15550nR A04;
    public final AnonymousClass10S A05;
    public final C15610nY A06;
    public final AnonymousClass10W A07;
    public final C253318z A08;
    public final C20730wE A09;
    public final AnonymousClass01d A0A;
    public final C14830m7 A0B;
    public final C14850m9 A0C;
    public final AbstractC14440lR A0D;

    public AnonymousClass190(C14900mE r1, C15570nT r2, C18850tA r3, AnonymousClass116 r4, C15550nR r5, AnonymousClass10S r6, C15610nY r7, AnonymousClass10W r8, C253318z r9, C20730wE r10, AnonymousClass01d r11, C14830m7 r12, C14850m9 r13, AbstractC14440lR r14) {
        this.A0B = r12;
        this.A0C = r13;
        this.A00 = r1;
        this.A01 = r2;
        this.A0D = r14;
        this.A02 = r3;
        this.A04 = r5;
        this.A0A = r11;
        this.A06 = r7;
        this.A05 = r6;
        this.A08 = r9;
        this.A09 = r10;
        this.A03 = r4;
        this.A07 = r8;
    }

    public static Intent A00(Activity activity, Bitmap bitmap, C30721Yo r17, boolean z) {
        Intent intent;
        String str;
        String str2;
        ContentValues contentValues;
        CharSequence typeLabel;
        StringBuilder sb;
        String str3;
        if (z) {
            intent = new Intent("android.intent.action.INSERT", ContactsContract.Contacts.CONTENT_URI);
        } else {
            intent = new Intent("android.intent.action.INSERT_OR_EDIT");
            intent.setType("vnd.android.cursor.item/contact");
        }
        intent.putExtra("finishActivityOnSaveCompleted", true);
        intent.putExtra("name", r17.A08.A01);
        Resources resources = activity.getResources();
        ArrayList<? extends Parcelable> arrayList = new ArrayList<>();
        List<C30741Yq> list = r17.A05;
        if (list != null) {
            for (C30741Yq r4 : list) {
                ContentValues contentValues2 = new ContentValues();
                contentValues2.put("mimetype", "vnd.android.cursor.item/phone_v2");
                contentValues2.put("data1", r4.A02);
                contentValues2.put("data2", Integer.valueOf(r4.A00));
                contentValues2.put("data3", ContactsContract.CommonDataKinds.Phone.getTypeLabel(resources, r4.A00, r4.A03).toString());
                arrayList.add(contentValues2);
            }
        }
        List<AnonymousClass4TL> list2 = r17.A02;
        if (list2 != null) {
            for (AnonymousClass4TL r3 : list2) {
                Class cls = r3.A01;
                if (cls == ContactsContract.CommonDataKinds.Email.class) {
                    contentValues = new ContentValues();
                    contentValues.put("mimetype", "vnd.android.cursor.item/email_v2");
                    contentValues.put("data1", r3.A02);
                    contentValues.put("data2", Integer.valueOf(r3.A00));
                    typeLabel = ContactsContract.CommonDataKinds.Email.getTypeLabel(resources, r3.A00, r3.A03);
                } else if (cls == ContactsContract.CommonDataKinds.StructuredPostal.class) {
                    contentValues = new ContentValues();
                    contentValues.put("mimetype", "vnd.android.cursor.item/postal-address_v2");
                    contentValues.put("data4", AnonymousClass3HC.A00(r3.A04.A03));
                    contentValues.put("data7", r3.A04.A00);
                    contentValues.put("data8", r3.A04.A02);
                    contentValues.put("data9", r3.A04.A04);
                    contentValues.put("data10", r3.A04.A01);
                    contentValues.put("data2", Integer.valueOf(r3.A00));
                    typeLabel = ContactsContract.CommonDataKinds.StructuredPostal.getTypeLabel(resources, r3.A00, r3.A03);
                } else {
                    if (cls == ContactsContract.CommonDataKinds.Im.class) {
                        sb = new StringBuilder();
                        str3 = "sharecontactutil ";
                    } else {
                        sb = new StringBuilder();
                        str3 = "sharecontactutil/type/unknown ";
                    }
                    sb.append(str3);
                    sb.append(r3.toString());
                    Log.e(sb.toString());
                }
                contentValues.put("data3", typeLabel.toString());
                arrayList.add(contentValues);
            }
        }
        List list3 = r17.A04;
        if (list3 != null && list3.size() > 0) {
            C90624Op r13 = (C90624Op) r17.A04.get(0);
            String str4 = r13.A00;
            int lastIndexOf = str4.lastIndexOf(" ");
            if (lastIndexOf > 0) {
                str4 = str4.substring(0, lastIndexOf);
            }
            ContentValues contentValues3 = new ContentValues();
            contentValues3.put("mimetype", "vnd.android.cursor.item/organization");
            contentValues3.put("data1", str4);
            if (lastIndexOf > 0) {
                contentValues3.put("data5", r13.A00.substring(lastIndexOf + 1));
            }
            contentValues3.put("data4", r13.A01);
            arrayList.add(contentValues3);
        }
        List list4 = r17.A06;
        if (list4 != null && list4.size() > 0) {
            for (C90634Oq r2 : r17.A06) {
                ContentValues contentValues4 = new ContentValues();
                contentValues4.put("mimetype", "vnd.android.cursor.item/website");
                contentValues4.put("data2", Integer.valueOf(r2.A00));
                contentValues4.put("data1", r2.A01);
                arrayList.add(contentValues4);
            }
        }
        Map map = r17.A07;
        if (map != null) {
            for (String str5 : map.keySet()) {
                if (str5.equals("NICKNAME")) {
                    ContentValues contentValues5 = new ContentValues();
                    contentValues5.put("mimetype", "vnd.android.cursor.item/nickname");
                    contentValues5.put("data1", ((AnonymousClass3FO) ((List) r17.A07.get(str5)).get(0)).A02);
                    arrayList.add(contentValues5);
                }
                if (str5.equals("BDAY")) {
                    ContentValues contentValues6 = new ContentValues();
                    contentValues6.put("mimetype", "vnd.android.cursor.item/contact_event");
                    contentValues6.put("data2", (Integer) 3);
                    contentValues6.put("data1", ((AnonymousClass3FO) ((List) r17.A07.get(str5)).get(0)).A02);
                    arrayList.add(contentValues6);
                }
                HashMap hashMap = C30721Yo.A0G;
                if (hashMap.containsKey(str5)) {
                    ContentValues contentValues7 = new ContentValues();
                    contentValues7.put("mimetype", "vnd.android.cursor.item/im");
                    contentValues7.put("data5", (Integer) hashMap.get(str5));
                    contentValues7.put("data1", ((AnonymousClass3FO) ((List) r17.A07.get(str5)).get(0)).A02);
                    Set set = ((AnonymousClass3FO) ((List) r17.A07.get(str5)).get(0)).A04;
                    if (set.size() > 0) {
                        contentValues7.put("data2", (String) set.toArray()[0]);
                    }
                    arrayList.add(contentValues7);
                }
            }
        }
        if (bitmap != null) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            ContentValues contentValues8 = new ContentValues();
            contentValues8.put("mimetype", "vnd.android.cursor.item/photo");
            contentValues8.put("data15", byteArray);
            arrayList.add(contentValues8);
            try {
                byteArrayOutputStream.close();
            } catch (IOException unused) {
            }
        }
        if (!arrayList.isEmpty()) {
            ContentValues contentValues9 = (ContentValues) arrayList.get(0);
            String asString = contentValues9.getAsString("mimetype");
            boolean z2 = true;
            char c = 65535;
            switch (asString.hashCode()) {
                case -1569536764:
                    if (asString.equals("vnd.android.cursor.item/email_v2")) {
                        c = 0;
                        break;
                    }
                    break;
                case -1328682538:
                    if (asString.equals("vnd.android.cursor.item/contact_event")) {
                        c = 1;
                        break;
                    }
                    break;
                case -601229436:
                    if (asString.equals("vnd.android.cursor.item/postal-address_v2")) {
                        c = 2;
                        break;
                    }
                    break;
                case 684173810:
                    if (asString.equals("vnd.android.cursor.item/phone_v2")) {
                        c = 3;
                        break;
                    }
                    break;
                case 689862072:
                    if (asString.equals("vnd.android.cursor.item/organization")) {
                        c = 4;
                        break;
                    }
                    break;
                case 905843021:
                    if (asString.equals("vnd.android.cursor.item/photo")) {
                        c = 5;
                        break;
                    }
                    break;
                case 950831081:
                    if (asString.equals("vnd.android.cursor.item/im")) {
                        c = 6;
                        break;
                    }
                    break;
                case 2034973555:
                    if (asString.equals("vnd.android.cursor.item/nickname")) {
                        c = 7;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    intent.putExtra("email", contentValues9.getAsString("data1"));
                    str = contentValues9.getAsString("data3");
                    str2 = "email_type";
                    intent.putExtra(str2, str);
                    break;
                case 1:
                case 5:
                case 7:
                    z2 = false;
                    break;
                case 2:
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(contentValues9.getAsString("data4"));
                    sb2.append(", ");
                    sb2.append(contentValues9.getAsString("data7"));
                    sb2.append(", ");
                    sb2.append(contentValues9.getAsString("data8"));
                    sb2.append(" ");
                    sb2.append(contentValues9.getAsString("data9"));
                    sb2.append(", ");
                    sb2.append(contentValues9.getAsString("data10"));
                    intent.putExtra("postal", sb2.toString());
                    str = contentValues9.getAsString("data3");
                    str2 = "postal_type";
                    intent.putExtra(str2, str);
                    break;
                case 3:
                    intent.putExtra("phone", contentValues9.getAsString("data1"));
                    str = contentValues9.getAsString("data3");
                    str2 = "phone_type";
                    intent.putExtra(str2, str);
                    break;
                case 4:
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(contentValues9.getAsString("data1"));
                    String asString2 = contentValues9.getAsString("data5");
                    if (asString2 != null) {
                        sb3.append(", ");
                        sb3.append(asString2);
                    }
                    intent.putExtra("company", sb3.toString());
                    str = contentValues9.getAsString("data4");
                    str2 = "job_title";
                    intent.putExtra(str2, str);
                    break;
                case 6:
                    intent.putExtra("im_protocol", contentValues9.getAsString("data5"));
                    str = contentValues9.getAsString("data1");
                    str2 = "im_handle";
                    intent.putExtra(str2, str);
                    break;
            }
            if (z2) {
                arrayList.remove(0);
            }
        }
        intent.putParcelableArrayListExtra("data", arrayList);
        return intent;
    }

    public void A01(Context context, UserJid userJid, String str) {
        C15550nR r3 = this.A04;
        C15370n3 A0B = r3.A0B(userJid);
        if (A0B.A0b || TextUtils.isEmpty(str)) {
            if (!A0B.A0I() && !A0B.A0e && !A0B.A0b) {
                this.A0D.Ab2(new RunnableBRunnable0Shape2S0200000_I0_2(this, 30, userJid));
            }
            Intent A0g = new C14960mK().A0g(context, r3.A0B(userJid));
            C35741ib.A00(A0g, "ShareContactUtil");
            context.startActivity(A0g);
            return;
        }
        context.startActivity(new Intent().setClassName(context.getPackageName(), "com.whatsapp.conversation.conversationrow.ContactSyncActivity").putExtra("user_jid", C15380n4.A03(userJid)).addFlags(335544320));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00e6, code lost:
        if (r1 != null) goto L_0x00e8;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02(java.lang.String r23, java.lang.String r24, java.util.ArrayList r25, java.util.List r26) {
        /*
        // Method dump skipped, instructions count: 332
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass190.A02(java.lang.String, java.lang.String, java.util.ArrayList, java.util.List):void");
    }
}
