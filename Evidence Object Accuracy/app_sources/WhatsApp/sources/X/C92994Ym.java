package X;

import android.content.Context;
import android.graphics.drawable.Drawable;
import com.whatsapp.R;

/* renamed from: X.4Ym  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92994Ym {
    public static Drawable A00(Context context) {
        Drawable A04 = AnonymousClass00T.A04(context, R.drawable.balloon_incoming_frame);
        int A00 = AnonymousClass00T.A00(context, R.color.bubble_color_incoming);
        AnonymousClass009.A05(A04);
        return AnonymousClass2GE.A04(A04, A00);
    }

    public static Drawable A01(Context context) {
        Drawable A04 = AnonymousClass00T.A04(context, R.drawable.balloon_outgoing_frame);
        int A00 = AnonymousClass00T.A00(context, R.color.bubble_color_outgoing);
        AnonymousClass009.A05(A04);
        return AnonymousClass2GE.A04(A04, A00);
    }
}
