package X;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.cardview.widget.CardView;
import com.whatsapp.R;
import com.whatsapp.TextData;
import com.whatsapp.TextEmojiLabel;
import java.util.List;

/* renamed from: X.2cO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53052cO extends FrameLayout implements AnonymousClass004 {
    public static boolean A0B;
    public CardView A00;
    public TextEmojiLabel A01;
    public AnonymousClass01d A02;
    public AnonymousClass1BK A03;
    public AnonymousClass19M A04;
    public C16630pM A05;
    public C28861Ph A06;
    public C92784Xk A07;
    public AnonymousClass2P7 A08;
    public boolean A09;
    public final List A0A;

    public C53052cO(Context context) {
        super(context);
        if (!this.A09) {
            this.A09 = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A04 = (AnonymousClass19M) A00.A6R.get();
            this.A02 = C12960it.A0Q(A00);
            this.A05 = C12990iw.A0e(A00);
        }
        this.A0A = C12960it.A0l();
        View inflate = LayoutInflater.from(context).inflate(A0B ? R.layout.status_playback_text_v2 : R.layout.status_playback_text, (ViewGroup) this, true);
        this.A01 = C12970iu.A0U(inflate, R.id.message_text);
        if (A0B) {
            this.A00 = (CardView) inflate.findViewById(R.id.web_page_preview_container);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00c1, code lost:
        if (r5.length <= 0) goto L_0x00c3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C53052cO A00(android.content.Context r19, X.AnonymousClass1BK r20, X.C28861Ph r21, boolean r22, boolean r23) {
        /*
        // Method dump skipped, instructions count: 458
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C53052cO.A00(android.content.Context, X.1BK, X.1Ph, boolean, boolean):X.2cO");
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A08;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A08 = r0;
        }
        return r0.generatedComponent();
    }

    public List getDisplayedUrls() {
        return this.A0A;
    }

    public C92784Xk getStaticContentPlayer() {
        return this.A07;
    }

    public CardView getWebPagePreviewContainer() {
        return this.A00;
    }

    public void setMessage(C28861Ph r1) {
        this.A06 = r1;
    }

    public void setPhishingManager(AnonymousClass1BK r1) {
        this.A03 = r1;
    }

    private void setTextContentProperties(TextData textData) {
        int i = textData.textColor;
        if (i != 0) {
            this.A01.setTextColor(i);
        }
        int i2 = textData.backgroundColor;
        if (i2 != 0) {
            setBackgroundColor(i2);
        }
        this.A01.setTypeface(AnonymousClass24D.A03(getContext(), textData.fontStyle));
    }
}
