package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

/* renamed from: X.4jd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C99024jd implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        LatLngBounds latLngBounds = null;
        LatLng latLng = null;
        LatLng latLng2 = null;
        LatLng latLng3 = null;
        LatLng latLng4 = null;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 2) {
                latLng = (LatLng) C95664e9.A07(parcel, LatLng.CREATOR, readInt);
            } else if (c == 3) {
                latLng2 = (LatLng) C95664e9.A07(parcel, LatLng.CREATOR, readInt);
            } else if (c == 4) {
                latLng3 = (LatLng) C95664e9.A07(parcel, LatLng.CREATOR, readInt);
            } else if (c == 5) {
                latLng4 = (LatLng) C95664e9.A07(parcel, LatLng.CREATOR, readInt);
            } else if (c != 6) {
                C95664e9.A0D(parcel, readInt);
            } else {
                latLngBounds = (LatLngBounds) C95664e9.A07(parcel, LatLngBounds.CREATOR, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C56442kt(latLng, latLng2, latLng3, latLng4, latLngBounds);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new C56442kt[i];
    }
}
