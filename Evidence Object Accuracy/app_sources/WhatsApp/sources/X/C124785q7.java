package X;

/* renamed from: X.5q7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124785q7 extends Exception {
    public final Exception innerException;

    public C124785q7(Exception exc) {
        this.innerException = exc;
    }

    @Override // java.lang.Throwable, java.lang.Object
    public String toString() {
        return this.innerException.toString();
    }
}
