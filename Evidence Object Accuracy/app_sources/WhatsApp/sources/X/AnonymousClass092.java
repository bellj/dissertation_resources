package X;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.TimeInterpolator;

/* renamed from: X.092  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass092 extends Animator {
    public final int A00;
    public final AnimatorSet A01;

    public AnonymousClass092(int i, Animator animator) {
        this.A00 = i;
        AnimatorSet animatorSet = new AnimatorSet();
        this.A01 = animatorSet;
        if (i > 0 || i == -1) {
            animatorSet.play(animator);
            animatorSet.addListener(new AnonymousClass09M(this));
        }
    }

    @Override // android.animation.Animator
    public void cancel() {
        this.A01.cancel();
    }

    @Override // android.animation.Animator
    public void end() {
        this.A01.end();
    }

    @Override // android.animation.Animator
    public long getDuration() {
        return this.A01.getDuration();
    }

    @Override // android.animation.Animator
    public long getStartDelay() {
        return this.A01.getStartDelay();
    }

    @Override // android.animation.Animator
    public boolean isRunning() {
        return this.A01.isRunning();
    }

    @Override // android.animation.Animator
    public void pause() {
        this.A01.pause();
    }

    @Override // android.animation.Animator
    public void resume() {
        this.A01.resume();
    }

    @Override // android.animation.Animator
    public Animator setDuration(long j) {
        return this.A01.setDuration(j);
    }

    @Override // android.animation.Animator
    public void setInterpolator(TimeInterpolator timeInterpolator) {
        this.A01.setInterpolator(timeInterpolator);
    }

    @Override // android.animation.Animator
    public void setStartDelay(long j) {
        this.A01.setStartDelay(j);
    }

    @Override // android.animation.Animator
    public void start() {
        this.A01.start();
    }
}
