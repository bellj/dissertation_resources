package X;

import com.whatsapp.search.views.itemviews.VoiceNoteProfileAvatarView;

/* renamed from: X.3Cl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63663Cl {
    public final AnonymousClass19D A00;
    public final C14820m6 A01;
    public final VoiceNoteProfileAvatarView A02;

    public C63663Cl(AnonymousClass19D r1, C14820m6 r2, VoiceNoteProfileAvatarView voiceNoteProfileAvatarView) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = voiceNoteProfileAvatarView;
    }

    public void A00(C30421Xi r7) {
        C35191hP r2;
        AbstractC28651Ol r0;
        AnonymousClass11P r1 = this.A00.A05;
        if (r1.A0D(r7)) {
            r2 = r1.A00();
            if (r2 != null) {
                r2.A09++;
            }
        } else {
            r2 = null;
        }
        int i = C35191hP.A0x;
        int i2 = 1;
        if (i != 0) {
            i2 = 2;
            if (i != 1) {
                if (i == 2) {
                    i2 = 0;
                } else {
                    throw C12960it.A0U(C12960it.A0W(i, "fastPlaybackOnClick: Did not handle fastPlaybackPlayerState: "));
                }
            }
        }
        if (r2 == null || (r0 = r2.A0P) == null || !r0.A0D() || r2.A0J(i2)) {
            C35191hP.A0x = i2;
            C12970iu.A1B(C12960it.A08(this.A01), "ptt_fast_playback_player_state", i2);
            AnonymousClass1IS r02 = r7.A0z;
            boolean z = r02.A02;
            boolean A0J = C15380n4.A0J(r02.A00);
            int i3 = 1;
            if (i2 != 0) {
                i3 = 2;
                if (i2 != 1) {
                    if (i2 == 2) {
                        i3 = 3;
                    } else {
                        throw C12970iu.A0f(C12960it.A0W(i2, "Unsupported FastPlaybackPlayerState "));
                    }
                }
            }
            this.A02.A02(i3, false, z, A0J);
        }
    }
}
