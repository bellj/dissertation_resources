package X;

/* renamed from: X.42R  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass42R extends AnonymousClass3HA {
    @Override // X.AnonymousClass3HA
    public boolean A00(String str) {
        Object obj = AbstractC41281tH.A03.get(str);
        if (obj != null) {
            if (!AnonymousClass0RB.A00(this.A00, obj.toString())) {
                return false;
            }
        }
        return true;
    }
}
