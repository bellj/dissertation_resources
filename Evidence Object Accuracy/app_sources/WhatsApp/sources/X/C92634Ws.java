package X;

import java.util.Arrays;

/* renamed from: X.4Ws  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92634Ws {
    public final int A00;
    public final C63373Bi A01;

    public C92634Ws(C63373Bi r1, int i) {
        this.A01 = r1;
        this.A00 = i;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C92634Ws r5 = (C92634Ws) obj;
            if (this.A00 != r5.A00 || !AnonymousClass28V.A00(this.A01, r5.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] A1a = C12980iv.A1a();
        A1a[0] = this.A01;
        C12980iv.A1T(A1a, this.A00);
        return Arrays.hashCode(A1a);
    }
}
