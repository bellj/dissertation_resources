package X;

/* renamed from: X.027  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass027 {
    public static Object A00(Class cls, Object obj) {
        if (obj instanceof AnonymousClass01L) {
            return cls.cast(obj);
        }
        if (obj instanceof AnonymousClass005) {
            return A00(cls, ((AnonymousClass005) obj).generatedComponent());
        }
        throw new IllegalStateException(String.format("Given component holder %s does not implement %s or %s", obj.getClass(), AnonymousClass01L.class, AnonymousClass005.class));
    }
}
