package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import com.whatsapp.mediaview.MediaViewBaseFragment;

/* renamed from: X.2YK  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2YK extends AnimatorListenerAdapter {
    public final /* synthetic */ AbstractC35501i8 A00;
    public final /* synthetic */ AnonymousClass33Z A01;

    public AnonymousClass2YK(AbstractC35501i8 r1, AnonymousClass33Z r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        MediaViewBaseFragment mediaViewBaseFragment = this.A01.A06;
        if (mediaViewBaseFragment.A0B() != null) {
            mediaViewBaseFragment.A1M(true, true);
            AbstractC35501i8 r0 = this.A00;
            if (r0 != null) {
                r0.AXp(true);
            }
        }
    }
}
