package X;

import android.animation.AnimatorSet;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.facebook.redex.ViewOnClickCListenerShape10S0100000_I1_4;
import com.facebook.redex.ViewOnClickCListenerShape9S0100000_I1_3;
import com.whatsapp.R;
import com.whatsapp.WaButton;
import com.whatsapp.WaTextView;
import com.whatsapp.mediacomposer.doodle.DoodleView;
import com.whatsapp.mediacomposer.doodle.titlebar.TitleBarView;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.2Ag  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C47342Ag {
    public float A00 = 1.0f;
    public int A01 = -13381889;
    public AnimatorSet A02;
    public C64453Fp A03;
    public AnonymousClass2Ab A04;
    public List A05;
    public boolean A06;
    public boolean A07;
    public boolean A08;
    public boolean A09;
    public boolean A0A = true;
    public boolean A0B = true;
    public final ValueAnimator A0C = ValueAnimator.ofFloat(0.8f, 1.0f);
    public final Animation A0D;
    public final Animation A0E;
    public final C453321d A0F;
    public final C89504Kf A0G;
    public final TitleBarView A0H;
    public final boolean A0I;

    public C47342Ag(C453321d r5, C89504Kf r6, TitleBarView titleBarView, boolean z) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        this.A0D = alphaAnimation;
        AlphaAnimation alphaAnimation2 = new AlphaAnimation(0.0f, 1.0f);
        this.A0E = alphaAnimation2;
        this.A0I = z;
        this.A0F = r5;
        this.A0H = titleBarView;
        this.A0G = r6;
        alphaAnimation.setDuration(300);
        alphaAnimation2.setDuration(300);
        titleBarView.A02 = (ImageView) AnonymousClass028.A0D(titleBarView, R.id.back);
        titleBarView.A08 = (WaButton) AnonymousClass028.A0D(titleBarView, R.id.done);
        titleBarView.A07 = (RelativeLayout) AnonymousClass028.A0D(titleBarView, R.id.tool_bar_extra);
        titleBarView.A01 = AnonymousClass028.A0D(titleBarView, R.id.undo);
        titleBarView.A00 = AnonymousClass028.A0D(titleBarView, R.id.title_bar);
        titleBarView.A05 = (ImageView) AnonymousClass028.A0D(titleBarView, R.id.pen);
        titleBarView.A06 = (ImageView) AnonymousClass028.A0D(titleBarView, R.id.shape);
        titleBarView.A09 = (WaTextView) AnonymousClass028.A0D(titleBarView, R.id.text);
        titleBarView.A03 = (ImageView) AnonymousClass028.A0D(titleBarView, R.id.crop);
        titleBarView.A04 = (ImageView) AnonymousClass028.A0D(titleBarView, R.id.delete);
        titleBarView.A0B = new AnonymousClass2ZW(titleBarView.getContext(), R.drawable.ic_cam_draw);
        titleBarView.A0C = new AnonymousClass2ZW(titleBarView.getContext(), R.drawable.ic_cam_sticker);
        titleBarView.A0D = new AnonymousClass2ZW(titleBarView.getContext(), 0);
        DisplayMetrics displayMetrics = titleBarView.getContext().getResources().getDisplayMetrics();
        if (((int) (((float) displayMetrics.widthPixels) / displayMetrics.density)) < 360) {
            titleBarView.A02.setPadding(0, 0, 0, 0);
            titleBarView.A04.setPadding(0, 0, 0, 0);
            titleBarView.A03.setPadding(0, 0, 0, 0);
            titleBarView.A01.setPadding(0, 0, 0, 0);
        }
        titleBarView.A05.setImageDrawable(titleBarView.A0B);
        titleBarView.A06.setImageDrawable(titleBarView.A0C);
        titleBarView.A09.setBackground(titleBarView.A0D);
        ImageView imageView = titleBarView.A02;
        imageView.setImageDrawable(new AnonymousClass2GF(imageView.getDrawable(), titleBarView.A0A));
        titleBarView.A02.setOnClickListener(new ViewOnClickCListenerShape9S0100000_I1_3(this, 47));
        titleBarView.A08.setOnClickListener(new ViewOnClickCListenerShape9S0100000_I1_3(this, 46));
        titleBarView.A01.setOnClickListener(new ViewOnClickCListenerShape10S0100000_I1_4(this, 0));
        titleBarView.A01.setOnLongClickListener(new View.OnLongClickListener() { // from class: X.3Mr
            @Override // android.view.View.OnLongClickListener
            public final boolean onLongClick(View view) {
                C47342Ag r3 = C47342Ag.this;
                AnonymousClass2Ab r2 = r3.A04;
                if (r2 == null || r3.A03 == null) {
                    return true;
                }
                DoodleView doodleView = r2.A0H;
                doodleView.A0H.A02();
                C454921v r0 = doodleView.A0E;
                r0.A01();
                r0.A02();
                r2.A0J.A02 = false;
                r2.A0E.A05(false);
                r2.A03.A00();
                r2.A0Q.A09(0);
                r3.A0H.A01.setVisibility(4);
                r3.A03.A02();
                r3.A04.A04();
                return true;
            }
        });
        titleBarView.A05.setOnClickListener(new ViewOnClickCListenerShape9S0100000_I1_3(this, 49));
        titleBarView.A06.setOnClickListener(new ViewOnClickCListenerShape10S0100000_I1_4(this, 1));
        titleBarView.A03.setOnClickListener(new ViewOnClickCListenerShape10S0100000_I1_4(this, 2));
        titleBarView.A09.setOnClickListener(new ViewOnClickCListenerShape9S0100000_I1_3(this, 48));
        titleBarView.A04.setOnClickListener(new ViewOnClickCListenerShape10S0100000_I1_4(this, 3));
        if (!z) {
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) titleBarView.getLayoutParams();
            layoutParams.setMargins(0, 0, 0, 0);
            layoutParams.height = AnonymousClass3G9.A00(titleBarView.getContext());
            titleBarView.setLayoutParams(layoutParams);
            titleBarView.setPadding(titleBarView.getPaddingLeft(), titleBarView.getResources().getDimensionPixelSize(R.dimen.title_bar_top_padding), titleBarView.getPaddingRight(), titleBarView.getPaddingBottom());
        }
    }

    public int A00() {
        return ((Number) this.A0F.A04.A01()).intValue();
    }

    public AnimatorSet A01(boolean z) {
        float x;
        long j;
        TimeInterpolator r0;
        AnimatorSet animatorSet = new AnimatorSet();
        this.A05 = new ArrayList();
        TitleBarView titleBarView = this.A0H;
        float shapeToolOffsetX = titleBarView.getShapeToolOffsetX();
        float textToolOffsetX = titleBarView.getTextToolOffsetX();
        float cropToolOffsetX = titleBarView.getCropToolOffsetX();
        int A00 = A00();
        if (titleBarView.A01.getVisibility() == 8 || A00 != 1) {
            x = titleBarView.A05.getX();
        } else {
            x = titleBarView.A09.getX() - titleBarView.A09.getTranslationX();
        }
        float x2 = (x - titleBarView.A01.getX()) - titleBarView.A01.getTranslationX();
        float deleteToolOffsetX = titleBarView.getDeleteToolOffsetX();
        A02(shapeToolOffsetX, 40, z).addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: X.4eN
            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                C47342Ag.this.A0H.setShapeToolX(C12960it.A00(valueAnimator));
            }
        });
        A02(textToolOffsetX, 60, z).addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: X.4eQ
            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                C47342Ag.this.A0H.setTextToolX(C12960it.A00(valueAnimator));
            }
        });
        A02(cropToolOffsetX, 20, z).addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: X.4eP
            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                C47342Ag.this.A0H.setCropToolX(C12960it.A00(valueAnimator));
            }
        });
        A02(x2, 20, z).addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: X.4eR
            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                C47342Ag.this.A0H.setUndoToolX(C12960it.A00(valueAnimator));
            }
        });
        A02(deleteToolOffsetX, 20, z).addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: X.4eS
            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                C47342Ag.this.A0H.setDeleteToolX(C12960it.A00(valueAnimator));
            }
        });
        float[] fArr = new float[2];
        float f = 1.0f;
        float f2 = 0.0f;
        if (z) {
            f2 = 1.0f;
        }
        fArr[0] = f2;
        float f3 = 1.0f;
        if (z) {
            f3 = 0.0f;
        }
        fArr[1] = f3;
        ValueAnimator ofFloat = ValueAnimator.ofFloat(fArr);
        ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: X.3Jk
            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                C47342Ag r02 = C47342Ag.this;
                TitleBarView titleBarView2 = r02.A0H;
                int A002 = r02.A00();
                float A003 = C12960it.A00(valueAnimator);
                if (A002 != 1) {
                    if (A002 != 2) {
                        if (A002 == 3) {
                            titleBarView2.A05.setAlpha(A003);
                            titleBarView2.A06.setAlpha(A003);
                            titleBarView2.A03.setAlpha(A003);
                            return;
                        } else if (A002 != 5) {
                            return;
                        }
                    }
                    titleBarView2.A05.setAlpha(A003);
                    titleBarView2.A09.setAlpha(A003);
                    titleBarView2.A03.setAlpha(A003);
                    return;
                }
                titleBarView2.A06.setAlpha(A003);
                titleBarView2.A09.setAlpha(A003);
                titleBarView2.A03.setAlpha(A003);
            }
        });
        ValueAnimator valueAnimator = this.A0C;
        valueAnimator.setDuration(400L);
        if (z) {
            ofFloat.setDuration(300L);
            ofFloat.setInterpolator(new AnonymousClass078());
            this.A05.add(valueAnimator);
        } else {
            ofFloat.setDuration(500L);
        }
        this.A05.add(ofFloat);
        if (!(A00() == 5 || A00() == 2)) {
            float[] fArr2 = new float[2];
            float f4 = 0.0f;
            if (z) {
                f4 = 1.0f;
            }
            fArr2[0] = f4;
            if (z) {
                f = 0.0f;
            }
            fArr2[1] = f;
            ValueAnimator ofFloat2 = ValueAnimator.ofFloat(fArr2);
            ofFloat2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: X.4eO
                @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                public final void onAnimationUpdate(ValueAnimator valueAnimator2) {
                    C47342Ag.this.A0H.setCloseButtonAlpha(C12960it.A00(valueAnimator2));
                }
            });
            long j2 = 100;
            if (z) {
                j = 100;
            } else {
                j = 300;
            }
            ofFloat2.setDuration(j);
            if (z) {
                j2 = 0;
            }
            ofFloat2.setStartDelay(j2);
            if (z) {
                r0 = new C016007o();
            } else {
                r0 = new AnonymousClass078();
            }
            ofFloat2.setInterpolator(r0);
            this.A05.add(ofFloat2);
        }
        animatorSet.playTogether(this.A05);
        animatorSet.addListener(new AnonymousClass2YI(this, z));
        return animatorSet;
    }

    public ValueAnimator A02(float f, long j, boolean z) {
        float[] fArr = new float[2];
        float f2 = -f;
        if (z) {
            f2 = 0.0f;
        }
        fArr[0] = f2;
        if (!z) {
            f = 0.0f;
        }
        fArr[1] = f;
        ValueAnimator ofFloat = ValueAnimator.ofFloat(fArr);
        if (z) {
            ofFloat.setDuration(300L);
            ofFloat.setInterpolator(new AnonymousClass078());
        } else {
            ofFloat.setDuration(500L);
            ofFloat.setStartDelay(j);
            ofFloat.setInterpolator(AnonymousClass0L1.A00(0.5f, 1.35f, 0.4f, 1.0f));
        }
        this.A05.add(ofFloat);
        return ofFloat;
    }

    public void A03() {
        TitleBarView titleBarView = this.A0H;
        Animation animation = this.A0D;
        titleBarView.A00.setVisibility(4);
        titleBarView.A00.startAnimation(animation);
    }

    public void A04() {
        AnimatorSet animatorSet;
        AnimatorSet animatorSet2 = this.A02;
        if (animatorSet2 != null && animatorSet2.isRunning() && this.A07) {
            this.A02.end();
        }
        if (!this.A0I || !this.A09 || (animatorSet = this.A02) == null || animatorSet.isRunning()) {
            TitleBarView titleBarView = this.A0H;
            Animation animation = this.A0E;
            if (titleBarView.A00.getVisibility() != 0) {
                titleBarView.A00.setVisibility(0);
                titleBarView.A00.startAnimation(animation);
            }
        } else {
            this.A02.start();
            this.A07 = false;
        }
        this.A09 = false;
    }

    public void A05(float f) {
        if (this.A0I) {
            A04();
        }
        this.A0F.A05(0);
        TitleBarView titleBarView = this.A0H;
        titleBarView.setShapeToolDrawableStrokePreview(false);
        titleBarView.setPenToolDrawableStrokePreview(false);
        titleBarView.A05.setSelected(false);
        titleBarView.A09.setSelected(false);
        titleBarView.A06.setSelected(false);
        titleBarView.A0C.A00(f, 0);
        titleBarView.A0D.A00(f, 0);
        titleBarView.A0B.A00(f, 0);
        titleBarView.setBackButtonDrawable(R.drawable.ic_cam_close);
        this.A0A = true;
        this.A0B = true;
        titleBarView.A05.setVisibility(0);
        titleBarView.A09.setVisibility(0);
        titleBarView.A06.setVisibility(0);
        titleBarView.A05.setAlpha(1.0f);
        titleBarView.A09.setAlpha(1.0f);
        titleBarView.A03.setAlpha(1.0f);
        titleBarView.A06.setAlpha(1.0f);
        if (titleBarView.A03.getVisibility() != 8) {
            titleBarView.A03.setVisibility(0);
        }
    }

    public void A06(float f, int i) {
        if (i == 0 || !this.A0A) {
            this.A0H.A0B.A00(f, i);
            return;
        }
        ValueAnimator valueAnimator = this.A0C;
        valueAnimator.addUpdateListener(new C65493Ju(this, f, i));
        valueAnimator.addListener(new C72523ek(this));
        valueAnimator.start();
        this.A0A = false;
        this.A0B = true;
    }

    public void A07(float f, int i) {
        if (!this.A0B || i == 0) {
            this.A0H.A0C.A00(f, i);
            return;
        }
        ValueAnimator valueAnimator = this.A0C;
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(f, i) { // from class: X.3Jw
            public final /* synthetic */ float A00;
            public final /* synthetic */ int A01;

            {
                this.A00 = r2;
                this.A01 = r3;
            }

            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator2) {
                C47342Ag r0 = C47342Ag.this;
                float f2 = this.A00;
                int i2 = this.A01;
                TitleBarView titleBarView = r0.A0H;
                float A00 = C12960it.A00(valueAnimator2);
                AnonymousClass2ZW r02 = titleBarView.A0C;
                r02.A02 = f2;
                r02.A03 = i2;
                r02.A01 = A00;
                r02.invalidateSelf();
            }
        });
        valueAnimator.addListener(new C72523ek(this));
        if (this.A0I) {
            AnimatorSet animatorSet = this.A02;
            if (animatorSet != null) {
                animatorSet.end();
            }
            AnimatorSet A01 = A01(true);
            this.A02 = A01;
            A01.start();
            this.A07 = true;
            return;
        }
        valueAnimator.start();
        this.A0A = true;
        this.A0B = false;
    }

    public void A08(float f, int i, boolean z, boolean z2) {
        this.A06 = z;
        this.A01 = i;
        this.A00 = f;
        boolean z3 = false;
        this.A0A = false;
        this.A0B = false;
        int A00 = A00();
        if (A00 == 0) {
            A05(f);
        } else if (A00 == 1) {
            A06(f, i);
            this.A0H.setPenToolDrawableStrokePreview(z);
        } else if (A00 == 2) {
            A07(f, i);
            if (z && z2) {
                z3 = true;
            }
            this.A0H.setShapeToolDrawableStrokePreview(z3);
        }
    }

    public void A09(int i) {
        if (i == 2 && A00() == 2) {
            A07(this.A00, this.A01);
            return;
        }
        C453321d r1 = this.A0F;
        if (A00() == i) {
            i = 0;
        }
        r1.A05(i);
    }

    public void A0A(int i, float f) {
        ValueAnimator valueAnimator;
        ValueAnimator.AnimatorUpdateListener r0;
        int A00 = A00();
        if (A00 != 1) {
            if (A00 == 3) {
                valueAnimator = this.A0C;
                r0 = new ValueAnimator.AnimatorUpdateListener(f, i) { // from class: X.3Jv
                    public final /* synthetic */ float A00;
                    public final /* synthetic */ int A01;

                    {
                        this.A00 = r2;
                        this.A01 = r3;
                    }

                    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                    public final void onAnimationUpdate(ValueAnimator valueAnimator2) {
                        C47342Ag r02 = C47342Ag.this;
                        float f2 = this.A00;
                        int i2 = this.A01;
                        TitleBarView titleBarView = r02.A0H;
                        float A002 = C12960it.A00(valueAnimator2);
                        AnonymousClass2ZW r03 = titleBarView.A0D;
                        r03.A02 = f2;
                        r03.A03 = i2;
                        r03.A01 = A002;
                        r03.invalidateSelf();
                    }
                };
            }
            this.A0C.addListener(new C72523ek(this));
            AnimatorSet A01 = A01(true);
            this.A02 = A01;
            A01.addListener(new AnonymousClass2YF(this));
            this.A02.start();
            this.A07 = true;
        }
        valueAnimator = this.A0C;
        r0 = new C65493Ju(this, f, i);
        valueAnimator.addUpdateListener(r0);
        this.A0C.addListener(new C72523ek(this));
        AnimatorSet A01 = A01(true);
        this.A02 = A01;
        A01.addListener(new AnonymousClass2YF(this));
        this.A02.start();
        this.A07 = true;
    }
}
