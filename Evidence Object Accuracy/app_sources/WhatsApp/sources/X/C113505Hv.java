package X;

import java.util.AbstractCollection;
import java.util.Iterator;

/* renamed from: X.5Hv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C113505Hv extends AbstractCollection<V> {
    public final /* synthetic */ AnonymousClass51S this$0;

    public C113505Hv(AnonymousClass51S r1) {
        this.this$0 = r1;
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public void clear() {
        this.this$0.clear();
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public boolean contains(Object obj) {
        return this.this$0.containsValue(obj);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
    public Iterator iterator() {
        return this.this$0.valueIterator();
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public int size() {
        return this.this$0.size();
    }
}
