package X;

/* renamed from: X.4wE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C106754wE implements AbstractC116785Ww {
    public int A00;
    public int A01;
    public int A02;
    public long A03 = -1;
    public AnonymousClass5Yf A04;
    public AbstractC14070ko A05;
    public C106834wM A06;
    public C106974wa A07;
    public C107474xO A08;
    public final C95304dT A09 = C95304dT.A05(6);

    @Override // X.AbstractC116785Ww
    public void AIa(AbstractC14070ko r1) {
        this.A05 = r1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:158:0x01c6 A[SYNTHETIC] */
    @Override // X.AbstractC116785Ww
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int AZn(X.AnonymousClass5Yf r32, X.AnonymousClass4IG r33) {
        /*
        // Method dump skipped, instructions count: 807
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C106754wE.AZn(X.5Yf, X.4IG):int");
    }

    @Override // X.AbstractC116785Ww
    public void AbQ(long j, long j2) {
        if (j == 0) {
            this.A02 = 0;
            this.A07 = null;
        } else if (this.A02 == 5) {
            this.A07.AbQ(j, j2);
        }
    }

    @Override // X.AbstractC116785Ww
    public boolean Ae5(AnonymousClass5Yf r8) {
        C95304dT r6 = this.A09;
        r6.A0Q(2);
        r8.AZ4(r6.A02, 0, 2);
        if (r6.A0F() != 65496) {
            return false;
        }
        r6.A0Q(2);
        C95304dT.A06(r8, r6, 2);
        int A0F = r6.A0F();
        this.A00 = A0F;
        if (A0F == 65504) {
            r6.A0Q(2);
            C95304dT.A06(r8, r6, 2);
            r8.A5r(r6.A0F() - 2);
            r6.A0Q(2);
            C95304dT.A06(r8, r6, 2);
            A0F = r6.A0F();
            this.A00 = A0F;
        }
        if (A0F != 65505) {
            return false;
        }
        r8.A5r(2);
        r6.A0Q(6);
        C95304dT.A06(r8, r6, 6);
        if (r6.A0I() == 1165519206 && r6.A0F() == 0) {
            return true;
        }
        return false;
    }
}
