package X;

import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.3k9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75623k9 extends AnonymousClass03U {
    public final View A00;
    public final View A01;
    public final CheckBox A02;
    public final ImageView A03;
    public final ImageView A04;
    public final ImageView A05;
    public final TextView A06;
    public final TextView A07;

    public /* synthetic */ C75623k9(View view) {
        super(view);
        this.A07 = C12960it.A0J(view, R.id.title_tv);
        this.A06 = C12960it.A0J(view, R.id.subtitle_tv);
        this.A01 = view.findViewById(R.id.primary_action_btn);
        this.A03 = C12970iu.A0L(view, R.id.primary_action_icon);
        this.A04 = C12970iu.A0L(view, R.id.secondary_action_btn);
        this.A05 = C12970iu.A0L(view, R.id.third_action_btn);
        this.A02 = (CheckBox) view.findViewById(R.id.cbx);
        this.A00 = view;
    }
}
