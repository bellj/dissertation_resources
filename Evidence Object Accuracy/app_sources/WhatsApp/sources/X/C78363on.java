package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;

/* renamed from: X.3on  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78363on extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C99524kR();
    public final int A00;
    public final long A01;
    public final List A02;

    public C78363on(List list, int i, long j) {
        this.A00 = i;
        this.A01 = j;
        this.A02 = list;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 2, this.A00);
        C95654e8.A08(parcel, 3, this.A01);
        C95654e8.A0F(parcel, this.A02, 4, false);
        C95654e8.A06(parcel, A00);
    }
}
