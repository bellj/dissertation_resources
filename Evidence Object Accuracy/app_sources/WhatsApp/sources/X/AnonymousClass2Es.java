package X;

import com.facebook.redex.RunnableBRunnable0Shape1S0200000_I0_1;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* renamed from: X.2Es  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Es implements AbstractC21730xt {
    public final C14900mE A00;
    public final C17170qN A01;
    public final C14850m9 A02;
    public final C17220qS A03;
    public final C48192Eu A04;

    public AnonymousClass2Es(C14900mE r1, C17170qN r2, C14850m9 r3, C17220qS r4, C48192Eu r5) {
        this.A02 = r3;
        this.A00 = r1;
        this.A03 = r4;
        this.A01 = r2;
        this.A04 = r5;
    }

    public final void A00(int i) {
        this.A00.A0I(new RunnableBRunnable0Shape1S0200000_I0_1(this, 13, new C48182Et(null, null, null, -1, i)));
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        Log.e("GetUserByCustomUrlProtocol/onDeliveryFailure");
        A00(408);
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r2, String str) {
        Log.e("GetUserByCustomUrlProtocol/onError");
        A00(C41151sz.A00(r2));
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r10, String str) {
        UserJid nullable;
        Log.e("GetUserByCustomUrlProtocol/onSuccess");
        AnonymousClass1V8 A0E = r10.A0E("user");
        if (A0E == null || (nullable = UserJid.getNullable(A0E.A0I("jid", null))) == null) {
            A00(0);
            return;
        }
        int i = 2;
        if (this.A02.A07(1849)) {
            i = 3;
        }
        this.A00.A0I(new RunnableBRunnable0Shape1S0200000_I0_1(this, 13, new C48182Et(nullable, null, null, i, 0)));
    }
}
