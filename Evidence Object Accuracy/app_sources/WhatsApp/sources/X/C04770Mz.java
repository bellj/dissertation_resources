package X;

import java.util.concurrent.Executor;

/* renamed from: X.0Mz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C04770Mz {
    public final AnonymousClass02K A00;
    public final Executor A01;

    public C04770Mz(AnonymousClass02K r1, Executor executor) {
        this.A01 = executor;
        this.A00 = r1;
    }
}
