package X;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.os.Message;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.jobqueue.job.SendReadReceiptJob;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.util.AbstractCollection;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.0ye  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22170ye {
    public final C20670w8 A00;
    public final C20090vC A01;
    public final C243715g A02;
    public final C17220qS A03;
    public final C238213d A04;
    public final C17230qT A05;

    public C22170ye(C20670w8 r1, C20090vC r2, C243715g r3, C17220qS r4, C238213d r5, C17230qT r6) {
        this.A00 = r1;
        this.A03 = r4;
        this.A01 = r2;
        this.A04 = r5;
        this.A05 = r6;
        this.A02 = r3;
    }

    public static HashMap A00(Collection collection) {
        StringBuilder sb;
        String str;
        String str2;
        HashMap hashMap = new HashMap();
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            AbstractC15340mz r6 = (AbstractC15340mz) it.next();
            if (r6.A0C == 16) {
                sb = new StringBuilder();
                str = "skipping read receipt since its already sent; message.key=";
            } else {
                byte b = r6.A0y;
                if (b == 11) {
                    sb = new StringBuilder();
                    str = "skipping read receipt due to decryption failure; message.key=";
                } else if (b == 31) {
                    sb = new StringBuilder();
                    str = "skipping read receipt due to multi device placeholder; message.key=";
                } else {
                    if (C30041Vv.A0m(r6)) {
                        str2 = "skip read receipt for revoked message";
                    } else if (b == 19) {
                        sb = new StringBuilder();
                        str = "skip read receipt for hsm rejection message. key=";
                    } else if (b == 21) {
                        str2 = "skip sending read receipt for request declined message.";
                    } else {
                        AnonymousClass1IS r4 = r6.A0z;
                        C32381c4 r0 = new C32381c4(r4.A00, r6.A0B(), r6 instanceof AnonymousClass1Iv);
                        AbstractCollection abstractCollection = (AbstractCollection) hashMap.get(r0);
                        if (abstractCollection == null) {
                            abstractCollection = new ArrayList();
                            hashMap.put(r0, abstractCollection);
                        }
                        abstractCollection.add(r4.A01);
                    }
                    Log.i(str2);
                }
            }
            sb.append(str);
            sb.append(r6.A0z);
            str2 = sb.toString();
            Log.i(str2);
        }
        return hashMap;
    }

    public final void A01(Message message, long j) {
        this.A05.A03(j);
        this.A03.A08(message, false);
    }

    public void A02(Jid jid, String str, long j) {
        A03(jid, str, "web", j);
    }

    public void A03(Jid jid, String str, String str2, long j) {
        if (str != null) {
            AnonymousClass1VH r1 = new AnonymousClass1VH();
            r1.A01 = jid;
            r1.A02 = null;
            r1.A07 = str;
            r1.A00 = j;
            r1.A08 = str2;
            r1.A05 = "notification";
            this.A03.A0B(r1.A00());
        }
    }

    public void A04(AbstractC15340mz r7) {
        if (r7.A0y == 31) {
            A07(r7, null);
        } else if (!C15380n4.A0M(r7.A0B()) && !r7.A0u) {
            if (r7.A0C == 13) {
                C238213d r5 = this.A04;
                if (r5.A00(r7.A0z.A00) && r7.A0I >= 1415214000000L && !C30041Vv.A0m(r7) && !r7.A1B) {
                    if (r7.A1C) {
                        if (!r5.A02(r7)) {
                            A01(Message.obtain(null, 0, 9, 0, r7), r7.A14);
                        }
                        r7.A1C = false;
                    }
                    A05(r7);
                    return;
                }
            }
            A01(Message.obtain(null, 0, 9, 0, r7), r7.A14);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002b, code lost:
        if (X.C15380n4.A0M(r5) != false) goto L_0x002d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A05(X.AbstractC15340mz r14) {
        /*
            r13 = this;
            int r1 = r14.A0C
            r0 = 16
            if (r1 == r0) goto L_0x004b
            boolean r0 = X.C30041Vv.A0m(r14)
            if (r0 != 0) goto L_0x004b
            byte r1 = r14.A0y
            r0 = 19
            if (r1 == r0) goto L_0x004b
            r0 = 21
            if (r1 == r0) goto L_0x004b
            boolean r0 = X.C32401c6.A0M(r14)
            if (r0 != 0) goto L_0x004b
            X.0lm r5 = r14.A0B()
            boolean r0 = r14 instanceof X.AnonymousClass1Iv
            r3 = 0
            r2 = 1
            if (r0 != 0) goto L_0x002d
            boolean r0 = X.C15380n4.A0M(r5)
            r12 = 0
            if (r0 == 0) goto L_0x002e
        L_0x002d:
            r12 = 1
        L_0x002e:
            X.0w8 r1 = r13.A00
            X.1IS r0 = r14.A0z
            X.0lm r4 = r0.A00
            X.AnonymousClass009.A05(r4)
            com.whatsapp.jid.DeviceJid r6 = r14.A17
            java.lang.String[] r7 = new java.lang.String[r2]
            java.lang.String r0 = r0.A01
            r7[r3] = r0
            long r8 = r14.A0I
            long r10 = r14.A14
            com.whatsapp.jobqueue.job.SendReadReceiptJob r3 = new com.whatsapp.jobqueue.job.SendReadReceiptJob
            r3.<init>(r4, r5, r6, r7, r8, r10, r12)
            r1.A00(r3)
        L_0x004b:
            X.1IS r0 = r14.A0z
            X.0lm r2 = r0.A00
            boolean r0 = X.C15380n4.A0N(r2)
            if (r0 != 0) goto L_0x0066
            X.13d r0 = r13.A04
            boolean r0 = r0.A02(r14)
            if (r0 == 0) goto L_0x0066
            int r1 = r14.A0C
            r0 = 17
            if (r1 != r0) goto L_0x0067
            r13.A06(r14)
        L_0x0066:
            return
        L_0x0067:
            X.15g r1 = r13.A02
            X.AnonymousClass009.A05(r2)
            long r3 = r14.A11
            long r5 = r14.A12
            r1.A00(r2, r3, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22170ye.A05(X.0mz):void");
    }

    public final void A06(AbstractC15340mz r3) {
        if (!(r3 instanceof AnonymousClass1Iv)) {
            r3.A0Y(16);
            try {
                this.A01.A08(r3, false);
            } catch (IOException e) {
                Log.e("Failed to update msg status back to 16", e);
            }
        }
    }

    public void A07(AbstractC15340mz r11, String str) {
        long j = r11.A14;
        AnonymousClass1IS r0 = r11.A0z;
        AbstractC14640lm r4 = r0.A00;
        AnonymousClass009.A05(r4);
        AbstractC14640lm A0B = r11.A0B();
        String str2 = r0.A01;
        AbstractC14640lm r3 = A0B;
        if (!C15380n4.A0F(A0B)) {
            r3 = r4;
            r4 = A0B;
        }
        A01(Message.obtain(null, 0, 129, 0, new C32391c5(r3, r4, str2, str, null, j)), j);
    }

    public void A08(C28941Pp r7) {
        if (!r7.A0b) {
            AbstractC15340mz r0 = r7.A0B;
            if (r0 == null) {
                A01(Message.obtain(null, 0, 9, 0, r7.A02((byte) 0)), r7.A06);
                return;
            }
            A04(r0);
        }
    }

    public void A09(C28941Pp r12, String str, String str2) {
        long j = r12.A06;
        AbstractC14640lm A00 = C15380n4.A00(r12.A0g);
        AbstractC14640lm A002 = C15380n4.A00(r12.A08);
        String str3 = r12.A0k;
        AbstractC14640lm r4 = A002;
        if (!C15380n4.A0F(A002)) {
            r4 = A00;
            A00 = A002;
        }
        A01(Message.obtain(null, 0, 129, 0, new C32391c5(r4, A00, str3, str, str2, j)), j);
    }

    public void A0A(Collection collection) {
        AnonymousClass1V2 A05;
        long j;
        String str;
        String str2;
        long max;
        A0B(A00(collection));
        HashMap hashMap = new HashMap();
        HashMap hashMap2 = new HashMap();
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            AbstractC15340mz r8 = (AbstractC15340mz) it.next();
            if (this.A04.A02(r8)) {
                if (r8.A0C == 17) {
                    A06(r8);
                } else {
                    AbstractC14640lm r9 = r8.A0z.A00;
                    if (C15380n4.A0N(r9)) {
                        AbstractC14640lm A0B = r8.A0B();
                        if (C15380n4.A0L(A0B)) {
                            Number number = (Number) hashMap2.get(A0B);
                            if (number == null) {
                                max = r8.A12;
                            } else {
                                max = Math.max(number.longValue(), r8.A12);
                            }
                            hashMap2.put(A0B, Long.valueOf(max));
                        }
                    } else {
                        AbstractC15340mz r6 = (AbstractC15340mz) hashMap.get(r9);
                        if (r6 != null && r6.A12 > r8.A12) {
                            r8 = r6;
                        }
                        hashMap.put(r9, r8);
                    }
                }
            }
        }
        for (Map.Entry entry : hashMap.entrySet()) {
            this.A02.A00((AbstractC14640lm) entry.getKey(), ((AbstractC15340mz) entry.getValue()).A11, ((AbstractC15340mz) entry.getValue()).A12);
        }
        for (Map.Entry entry2 : hashMap2.entrySet()) {
            C243715g r5 = this.A02;
            UserJid userJid = (UserJid) entry2.getKey();
            long longValue = ((Number) entry2.getValue()).longValue();
            StringBuilder sb = new StringBuilder("msgstore/setstatusreadreceiptssent/");
            sb.append(userJid);
            sb.append(" ");
            sb.append(longValue);
            Log.i(sb.toString());
            C18470sV r10 = r5.A09;
            AnonymousClass1V2 A04 = r10.A04(userJid);
            if (A04 == null) {
                StringBuilder sb2 = new StringBuilder("msgstore/setstatusreadreceiptssent/no status for ");
                sb2.append(userJid);
                Log.w(sb2.toString());
            } else {
                synchronized (A04) {
                    if (longValue > A04.A07) {
                        A04.A07 = longValue;
                    }
                    A05 = A04.A05();
                }
                try {
                    try {
                        synchronized (A05) {
                            j = A05.A07;
                        }
                        boolean A0G = r10.A0G();
                        C16310on A02 = r10.A02.A02();
                        try {
                            ContentValues contentValues = new ContentValues();
                            contentValues.put("last_read_receipt_sent_message_table_id", Long.valueOf(j));
                            C16330op r4 = A02.A03;
                            if (A0G) {
                                str = "status_list";
                                str2 = "key_remote_jid=?";
                            } else {
                                str = "status";
                                str2 = "jid_row_id=?";
                            }
                            String[] A0J = r10.A0J(userJid, A0G);
                            C18470sV.A02("updateLastReadReceiptSentMessageRowId/UPDATE", A0G);
                            if (r4.A00(str, contentValues, str2, A0J) == 0) {
                                StringBuilder sb3 = new StringBuilder();
                                sb3.append("StatusStore/updateLastReadReceiptSentMessageTableId/no status saved for ");
                                sb3.append(userJid);
                                sb3.append("; shouldUseDeprecatedTable=");
                                sb3.append(A0G);
                                Log.e(sb3.toString());
                            }
                            A02.close();
                        } catch (Throwable th) {
                            try {
                                A02.close();
                            } catch (Throwable unused) {
                            }
                            throw th;
                        }
                    } catch (Error | RuntimeException e) {
                        Log.e(e);
                        throw e;
                    }
                } catch (SQLiteDatabaseCorruptException e2) {
                    Log.e(e2);
                    r5.A07.A02();
                }
            }
        }
    }

    public void A0B(HashMap hashMap) {
        for (Map.Entry entry : hashMap.entrySet()) {
            int size = ((AbstractCollection) entry.getValue()).size();
            int i = 0;
            while (i < size) {
                int min = Math.min(i + 256, size);
                this.A00.A00(new SendReadReceiptJob(((C32381c4) entry.getKey()).A00, ((C32381c4) entry.getKey()).A01, null, (String[]) ((AbstractList) entry.getValue()).subList(i, min).toArray(new String[0]), -1, 0, ((C32381c4) entry.getKey()).A02));
                i = min;
            }
        }
    }
}
