package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5h4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120985h4 extends AbstractC1316063k {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(30);
    public final C1316363n A00;
    public final String A01;
    public final String A02;

    public C120985h4(AnonymousClass102 r3, AnonymousClass1V8 r4) {
        super(r4);
        this.A01 = r4.A0I("claim_id", "");
        this.A02 = r4.A0H("orig_transaction_id");
        this.A00 = C1316363n.A00(r3, r4.A0F("transaction-amount"));
    }

    public C120985h4(Parcel parcel) {
        super(parcel);
        this.A01 = parcel.readString();
        this.A02 = C12990iw.A0p(parcel);
        Parcelable A0I = C12990iw.A0I(parcel, C1316363n.class);
        AnonymousClass009.A05(A0I);
        this.A00 = (C1316363n) A0I;
    }

    public C120985h4(String str) {
        super(str);
        JSONObject A05 = C13000ix.A05(str);
        this.A01 = A05.optString("claim_id", "");
        this.A02 = A05.getString("orig_transaction_id");
        C1316363n A01 = C1316363n.A01(A05.getJSONObject("transaction_amount").toString());
        AnonymousClass009.A05(A01);
        this.A00 = A01;
    }

    @Override // X.AbstractC1316063k
    public void A05(JSONObject jSONObject) {
        try {
            String str = this.A01;
            if (!TextUtils.isEmpty(str)) {
                jSONObject.put("claim_id", str);
            }
            jSONObject.put("orig_transaction_id", this.A02);
            jSONObject.put("transaction_amount", this.A00.A02());
        } catch (JSONException unused) {
            Log.w("PAY:NoviTransactionRefund/addTransactionDataToJson: Error while creating a JSON from a refund");
        }
    }

    @Override // X.AbstractC1316063k, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.A01);
        parcel.writeString(this.A02);
        parcel.writeParcelable(this.A00, i);
    }
}
