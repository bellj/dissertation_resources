package X;

/* renamed from: X.1gr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C34881gr extends AbstractC16110oT {
    public Integer A00;

    public C34881gr() {
        super(2518, new AnonymousClass00E(1, 20, 1000), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        StringBuilder sb = new StringBuilder("WamMdAppStateKeyRotation {");
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mdAppStateKeyRotationReason", obj);
        sb.append("}");
        return sb.toString();
    }
}
