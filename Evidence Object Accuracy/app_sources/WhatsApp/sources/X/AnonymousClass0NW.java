package X;

import android.content.Context;
import android.location.LocationManager;

/* renamed from: X.0NW  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0NW {
    public static AnonymousClass0NW A03;
    public final Context A00;
    public final LocationManager A01;
    public final C04690Mr A02 = new C04690Mr();

    public AnonymousClass0NW(Context context, LocationManager locationManager) {
        this.A00 = context;
        this.A01 = locationManager;
    }
}
