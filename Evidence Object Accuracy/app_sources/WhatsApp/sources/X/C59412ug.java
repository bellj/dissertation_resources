package X;

import com.whatsapp.jid.Jid;

/* renamed from: X.2ug  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59412ug extends AbstractC16320oo {
    public final Jid A00;

    public C59412ug(AbstractC15710nm r13, C14900mE r14, C16430p0 r15, AnonymousClass2K1 r16, AnonymousClass1B4 r17, C16340oq r18, C17170qN r19, AnonymousClass018 r20, C14850m9 r21, Jid jid, AbstractC14440lR r23) {
        super(r13, r14, r15, r16, r17, r18, r19, r20, r21, r23);
        this.A00 = jid;
    }
}
