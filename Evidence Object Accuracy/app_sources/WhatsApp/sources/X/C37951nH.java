package X;

import java.io.Closeable;

/* renamed from: X.1nH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C37951nH implements Closeable {
    public final C37961nI A00;
    public final C37961nI A01;
    public final C37961nI A02;
    public final /* synthetic */ AnonymousClass153 A03;

    public C37951nH(C37961nI r1, C37961nI r2, C37961nI r3, AnonymousClass153 r4) {
        this.A03 = r4;
        this.A02 = r1;
        this.A00 = r2;
        this.A01 = r3;
    }

    public C91444Rt A00() {
        String A00;
        AnonymousClass153 r4 = this.A03;
        String A002 = this.A02.A00();
        String A003 = this.A00.A00();
        C37961nI r0 = this.A01;
        if (r0 == null) {
            A00 = null;
        } else {
            A00 = r0.A00();
        }
        return new C91444Rt(r4, A002, A003, A00);
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        AnonymousClass1P1.A03(this.A00);
        AnonymousClass1P1.A03(this.A02);
        AnonymousClass1P1.A03(this.A01);
    }
}
