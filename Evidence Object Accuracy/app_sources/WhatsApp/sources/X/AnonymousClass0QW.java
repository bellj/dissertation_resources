package X;

import android.app.Activity;
import android.text.Selection;
import android.text.Spannable;
import android.view.DragEvent;
import android.view.View;
import android.widget.TextView;

/* renamed from: X.0QW  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0QW {
    public static boolean A00(Activity activity, DragEvent dragEvent, View view) {
        activity.requestDragAndDropPermissions(dragEvent);
        AnonymousClass028.A0E(view, new AnonymousClass0MT(dragEvent.getClipData(), 3).A00.A6j());
        return true;
    }

    /* JADX INFO: finally extract failed */
    public static boolean A01(Activity activity, DragEvent dragEvent, TextView textView) {
        activity.requestDragAndDropPermissions(dragEvent);
        int offsetForPosition = textView.getOffsetForPosition(dragEvent.getX(), dragEvent.getY());
        textView.beginBatchEdit();
        try {
            Selection.setSelection((Spannable) textView.getText(), offsetForPosition);
            AnonymousClass028.A0E(textView, new AnonymousClass0MT(dragEvent.getClipData(), 3).A00.A6j());
            textView.endBatchEdit();
            return true;
        } catch (Throwable th) {
            textView.endBatchEdit();
            throw th;
        }
    }
}
