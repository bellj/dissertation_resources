package X;

import android.view.MenuItem;

/* renamed from: X.0W0  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0W0 implements MenuItem.OnActionExpandListener {
    public final MenuItem.OnActionExpandListener A00;
    public final /* synthetic */ AnonymousClass0CJ A01;

    public AnonymousClass0W0(MenuItem.OnActionExpandListener onActionExpandListener, AnonymousClass0CJ r2) {
        this.A01 = r2;
        this.A00 = onActionExpandListener;
    }

    @Override // android.view.MenuItem.OnActionExpandListener
    public boolean onMenuItemActionCollapse(MenuItem menuItem) {
        return this.A00.onMenuItemActionCollapse(this.A01.A00(menuItem));
    }

    @Override // android.view.MenuItem.OnActionExpandListener
    public boolean onMenuItemActionExpand(MenuItem menuItem) {
        return this.A00.onMenuItemActionExpand(this.A01.A00(menuItem));
    }
}
