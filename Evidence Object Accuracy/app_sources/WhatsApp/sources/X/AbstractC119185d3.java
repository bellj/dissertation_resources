package X;

import android.content.Context;
import android.util.AttributeSet;
import com.whatsapp.BidiToolbar;

/* renamed from: X.5d3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC119185d3 extends BidiToolbar {
    public boolean A00;

    public AbstractC119185d3(Context context) {
        super(context);
        A0I();
    }

    public AbstractC119185d3(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A0I();
    }

    public AbstractC119185d3(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A0I();
    }

    @Override // X.AnonymousClass1MN
    public void A0I() {
        if (!this.A00) {
            this.A00 = true;
            ((BidiToolbar) this).A00 = C12960it.A0R(AnonymousClass2P6.A00(generatedComponent()));
        }
    }
}
