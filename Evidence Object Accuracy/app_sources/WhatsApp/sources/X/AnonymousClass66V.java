package X;

import android.util.Base64;
import android.util.Log;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.66V  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass66V implements AbstractC17390qj {
    public final C130125yq A00;

    public AnonymousClass66V(C130125yq r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC17390qj
    public C89994Me Ad6(C14260l7 r8, C14270l8 r9, AnonymousClass4ZD r10, C93484aF r11, C14240l5 r12) {
        Object obj = r11.A02.get("payload");
        if (obj instanceof String) {
            byte[] A04 = this.A00.A04(Base64.decode((String) obj, 0));
            if (A04 != null) {
                try {
                    JSONObject A05 = C13000ix.A05(new String(A04));
                    Iterator<String> keys = A05.keys();
                    HashMap A11 = C12970iu.A11();
                    while (keys.hasNext()) {
                        String A0x = C12970iu.A0x(keys);
                        A11.put(A0x, A05.get(A0x));
                    }
                    return new C89994Me(null, A11);
                } catch (JSONException e) {
                    Log.d("Whatsapp", "PAY: NoviBloksDataModule/setUp can't construct JSON from decrypted payload", e);
                }
            } else {
                Log.d("Whatsapp", "PAY: NoviBloksDataModule/setUp can't decrypt the payload");
            }
        }
        return new C89994Me(null, null);
    }
}
