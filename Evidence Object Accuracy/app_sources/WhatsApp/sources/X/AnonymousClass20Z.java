package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.util.Log;
import java.math.BigDecimal;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.20Z  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass20Z implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C99864kz();
    public int A00;
    public AbstractC28901Pl A01;
    public final C30821Yy A02;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass20Z(C30821Yy r2, AbstractC28901Pl r3, int i) {
        AnonymousClass009.A05(r2);
        AnonymousClass009.A0F(r2.A03());
        this.A02 = r2;
        this.A00 = i;
        this.A01 = r3;
    }

    public JSONObject A00(boolean z) {
        Object obj;
        JSONObject jSONObject = new JSONObject();
        try {
            AbstractC28901Pl r3 = this.A01;
            jSONObject.put("t", r3.A04());
            jSONObject.put("st", (Object) null);
            jSONObject.put("cc", r3.A07.A03);
            if (!z) {
                jSONObject.put("c", r3.A0A);
                AnonymousClass1ZR r0 = r3.A09;
                if (r0 == null) {
                    obj = null;
                } else {
                    obj = r0.A00;
                }
                jSONObject.put("n", obj);
                jSONObject.put("a", this.A02.toString());
            }
            if (r3 instanceof C30881Ze) {
                jSONObject.put("ci", ((C30881Ze) r3).A01);
            }
            jSONObject.put("sd", this.A00);
            return jSONObject;
        } catch (JSONException e) {
            if (z) {
                return null;
            }
            Log.w("PAY: PaymentTransaction:Source:toJsonString threw creating json string: ", e);
            return null;
        }
    }

    @Override // java.lang.Object
    public String toString() {
        JSONObject A00 = A00(true);
        if (A00 != null) {
            return A00.toString();
        }
        return null;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        Object obj;
        AbstractC28901Pl r1 = this.A01;
        parcel.writeInt(r1.A04());
        parcel.writeString(null);
        parcel.writeString(r1.A07.A03);
        parcel.writeString(r1.A0A);
        AnonymousClass1ZR r0 = r1.A09;
        if (r0 == null) {
            obj = null;
        } else {
            obj = r0.A00;
        }
        parcel.writeString((String) obj);
        if (r1 instanceof C30881Ze) {
            parcel.writeInt(((C30881Ze) r1).A01);
        }
        BigDecimal bigDecimal = this.A02.A00;
        parcel.writeInt(bigDecimal.scale());
        parcel.writeString(bigDecimal.toString());
        parcel.writeInt(this.A00);
    }
}
