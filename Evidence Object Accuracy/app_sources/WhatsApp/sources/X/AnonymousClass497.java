package X;

import java.io.IOException;

/* renamed from: X.497  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass497 extends IOException {
    public AnonymousClass497() {
        super("CodedOutputStream was writing to a flat byte array and ran out of space.");
    }

    public AnonymousClass497(Throwable th) {
        super("CodedOutputStream was writing to a flat byte array and ran out of space.", th);
    }

    public AnonymousClass497(String str) {
        super(C72453ed.A0r("CodedOutputStream was writing to a flat byte array and ran out of space.: ", str));
    }

    public AnonymousClass497(String str, Throwable th) {
        super(C72453ed.A0r("CodedOutputStream was writing to a flat byte array and ran out of space.: ", str), th);
    }
}
