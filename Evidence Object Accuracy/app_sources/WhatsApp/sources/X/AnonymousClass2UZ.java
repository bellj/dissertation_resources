package X;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.WaImageView;

/* renamed from: X.2UZ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2UZ extends WaImageView {
    public int A00;
    public int A01;
    public Bitmap A02;
    public BitmapShader A03;
    public Matrix A04 = new Matrix();
    public Paint A05 = new Paint(1);
    public Paint A06 = new Paint(1);
    public Paint A07 = new Paint(1);
    public Paint A08 = new Paint(1);
    public RectF A09 = new RectF();
    public RectF A0A = new RectF();
    public boolean A0B;
    public final float A0C;
    public final int A0D;
    public final int A0E;
    public final int A0F;
    public final int A0G;
    public final ImageView.ScaleType A0H = ImageView.ScaleType.CENTER;

    public AnonymousClass2UZ(Context context) {
        super(context);
        A00();
        this.A05.setColor(AnonymousClass00T.A00(context, R.color.settings_contact_us_screenshot_bg));
        this.A06.setColor(AnonymousClass00T.A00(context, R.color.settings_contact_us_screenshot_border));
        this.A07.setColor(AnonymousClass00T.A00(context, R.color.settings_contact_us_screenshot_circle));
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.settings_contact_us_screenshot_size);
        this.A0G = dimensionPixelSize;
        int dimensionPixelSize2 = getResources().getDimensionPixelSize(R.dimen.settings_contact_us_screenshot_corner_radius);
        this.A0E = dimensionPixelSize2;
        this.A0D = getResources().getDimensionPixelSize(R.dimen.settings_contact_us_screenshot_circle_size);
        int dimensionPixelSize3 = getResources().getDimensionPixelSize(R.dimen.settings_contact_us_screenshot_border_width);
        this.A0F = dimensionPixelSize3;
        float f = (float) dimensionPixelSize2;
        float f2 = (float) dimensionPixelSize;
        this.A0C = f * ((f2 - (((float) dimensionPixelSize3) * 2.0f)) / f2);
        A02();
    }

    public final void A02() {
        super.setScaleType(this.A0H);
        Resources resources = getResources();
        Drawable drawable = resources.getDrawable(R.drawable.ic_action_add);
        drawable.setColorFilter(resources.getColor(R.color.settings_contact_us_screenshot_plus), PorterDuff.Mode.SRC_IN);
        super.setImageDrawable(drawable);
        super.setContentDescription(getContext().getString(R.string.describe_problem_add_screenshot));
    }

    @Override // com.whatsapp.WaImageView, android.widget.ImageView, android.view.View
    public void onDraw(Canvas canvas) {
        Bitmap bitmap = this.A02;
        RectF rectF = this.A09;
        float f = (float) this.A0E;
        if (bitmap == null) {
            canvas.drawRoundRect(rectF, f, f, this.A05);
            canvas.drawCircle((float) (getMeasuredWidth() >> 1), (float) (getMeasuredHeight() >> 1), (float) (this.A0D >> 1), this.A07);
            super.onDraw(canvas);
            return;
        }
        canvas.drawRoundRect(rectF, f, f, this.A06);
        RectF rectF2 = this.A0A;
        float f2 = this.A0C;
        canvas.drawRoundRect(rectF2, f2, f2, this.A08);
    }

    @Override // android.widget.ImageView, android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        RectF rectF = this.A09;
        int i3 = this.A0G;
        float f = (float) i3;
        rectF.set(0.0f, 0.0f, f, f);
        RectF rectF2 = this.A0A;
        int i4 = this.A0F;
        float f2 = (float) i4;
        float f3 = (float) (i3 - i4);
        rectF2.set(f2, f2, f3, f3);
        setMeasuredDimension(i3, i3);
    }

    public void setScreenshot(Bitmap bitmap) {
        float width;
        float height;
        super.setImageBitmap(bitmap);
        this.A02 = bitmap;
        if (getWidth() != 0 || getHeight() != 0) {
            Bitmap bitmap2 = this.A02;
            if (bitmap2 == null) {
                A02();
            } else {
                Shader.TileMode tileMode = Shader.TileMode.CLAMP;
                this.A03 = new BitmapShader(bitmap2, tileMode, tileMode);
                Paint paint = this.A08;
                paint.setAntiAlias(true);
                paint.setShader(this.A03);
                this.A00 = this.A02.getHeight();
                this.A01 = this.A02.getWidth();
                Matrix matrix = this.A04;
                matrix.set(null);
                RectF rectF = this.A0A;
                float f = 0.0f;
                if (((float) this.A01) * rectF.height() > rectF.width() * ((float) this.A00)) {
                    width = rectF.height() / ((float) this.A00);
                    f = (rectF.width() - (((float) this.A01) * width)) * 0.5f;
                    height = 0.0f;
                } else {
                    width = rectF.width() / ((float) this.A01);
                    height = (rectF.height() - (((float) this.A00) * width)) * 0.5f;
                }
                matrix.setScale(width, width);
                matrix.postTranslate(((float) ((int) (f + 0.5f))) + rectF.left, ((float) ((int) (height + 0.5f))) + rectF.top);
                this.A03.setLocalMatrix(matrix);
            }
            invalidate();
        }
    }
}
