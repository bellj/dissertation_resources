package X;

/* renamed from: X.0af  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08150af implements AbstractC12040hH {
    public final AnonymousClass0H9 A00;
    public final AnonymousClass0H9 A01;
    public final C08170ah A02;
    public final String A03;
    public final boolean A04;

    public C08150af(AnonymousClass0H9 r1, AnonymousClass0H9 r2, C08170ah r3, String str, boolean z) {
        this.A03 = str;
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
        this.A04 = z;
    }

    @Override // X.AbstractC12040hH
    public AbstractC12470hy Aes(AnonymousClass0AA r2, AbstractC08070aX r3) {
        return new C07980aO(r2, this, r3);
    }
}
