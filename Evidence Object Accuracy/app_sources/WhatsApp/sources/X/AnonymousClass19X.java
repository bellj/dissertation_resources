package X;

import com.facebook.redex.RunnableBRunnable0Shape0S1200000_I0;
import com.whatsapp.util.Log;
import java.util.Collections;
import java.util.Map;
import java.util.regex.Matcher;

/* renamed from: X.19X  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass19X {
    public boolean A00 = false;
    public final C006202y A01 = new C006202y(5);
    public final AbstractC15710nm A02;
    public final C14900mE A03;
    public final AnonymousClass19S A04;
    public final C19850um A05;
    public final AnonymousClass19T A06;
    public final AnonymousClass19R A07;
    public final C14850m9 A08;
    public final AnonymousClass19U A09;
    public final AbstractC14440lR A0A;

    public AnonymousClass19X(AbstractC15710nm r3, C14900mE r4, AnonymousClass19S r5, C19850um r6, AnonymousClass19T r7, AnonymousClass19R r8, C14850m9 r9, AnonymousClass19U r10, AbstractC14440lR r11) {
        this.A08 = r9;
        this.A03 = r4;
        this.A02 = r3;
        this.A07 = r8;
        this.A0A = r11;
        this.A04 = r5;
        this.A06 = r7;
        this.A05 = r6;
        this.A09 = r10;
    }

    public static /* synthetic */ void A00(AnonymousClass19X r4, AnonymousClass5WG r5, String str, Map map, boolean z) {
        r5.AQL(map);
        if (map != null) {
            StringBuilder sb = new StringBuilder();
            for (Map.Entry entry : map.entrySet()) {
                sb.append(entry.getValue().toString());
                sb.append('\n');
            }
            r4.A02.AaV("ShopManager/requestShopMetadata", sb.toString(), z);
        }
        if (str != null) {
            r4.A01.A07(str);
        }
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:18:0x0005 */
    /* JADX DEBUG: Multi-variable search result rejected for r3v2, resolved type: boolean */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r3v0 */
    /* JADX WARN: Type inference failed for: r3v1, types: [X.0nm] */
    public void A01(AnonymousClass5WG r7, String str) {
        AnonymousClass19R r5 = this.A07;
        boolean z = 1;
        try {
            Matcher A00 = r5.A00(str);
            if (A00.matches()) {
                String group = A00.group(1);
                if (group != null) {
                    AnonymousClass3M3 r0 = (AnonymousClass3M3) this.A01.A04(group);
                    if (r0 != null) {
                        r7.AQM(r0);
                        return;
                    }
                    AnonymousClass19U r02 = this.A09;
                    r02.A01 = group;
                    r02.A00 = "STOREFRONT";
                    this.A0A.Ab2(new RunnableBRunnable0Shape0S1200000_I0(this, r7, group, 10));
                    return;
                }
            } else {
                AbstractC15710nm r2 = r5.A00;
                StringBuilder sb = new StringBuilder();
                sb.append("Matcher didn't match for: ");
                sb.append(str);
                r2.AaV("ShopUtils/parseIdFromUrl", sb.toString(), false);
            }
        } catch (AnonymousClass4CC e) {
            Log.e(e);
            r5.A00.AaV("ShopUtils/parseIdFromUrl", e.getMessage(), z);
        }
        z = this.A02;
        StringBuilder sb2 = new StringBuilder("Couldn't find shopId: ");
        sb2.append(str);
        z.AaV("ShopManager/requestShopMetadataByUrl", sb2.toString(), false);
        r7.AQL(Collections.EMPTY_MAP);
    }

    public boolean A02() {
        C14850m9 r1 = this.A08;
        return !r1.A07(854) && !r1.A07(832) && r1.A07(1062);
    }
}
