package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

/* renamed from: X.4xN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107464xN implements AnonymousClass5YX {
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(21);
    public final int A00;
    public final int A01;
    public final String A02;
    public final byte[] A03;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public /* synthetic */ C107464xN(Parcel parcel) {
        this.A02 = parcel.readString();
        this.A03 = parcel.createByteArray();
        this.A00 = parcel.readInt();
        this.A01 = parcel.readInt();
    }

    public C107464xN(byte[] bArr, int i, int i2, String str) {
        this.A02 = str;
        this.A03 = bArr;
        this.A00 = i;
        this.A01 = i2;
    }

    @Override // X.AnonymousClass5YX
    public /* synthetic */ byte[] AHp() {
        return null;
    }

    @Override // X.AnonymousClass5YX
    public /* synthetic */ C100614mC AHq() {
        return null;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C107464xN.class != obj.getClass()) {
                return false;
            }
            C107464xN r5 = (C107464xN) obj;
            if (!this.A02.equals(r5.A02) || !Arrays.equals(this.A03, r5.A03) || this.A00 != r5.A00 || this.A01 != r5.A01) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return ((((C72453ed.A05(this.A02.hashCode()) + Arrays.hashCode(this.A03)) * 31) + this.A00) * 31) + this.A01;
    }

    @Override // java.lang.Object
    public String toString() {
        return C12960it.A0d(this.A02, C12960it.A0k("mdta: key="));
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A02);
        parcel.writeByteArray(this.A03);
        parcel.writeInt(this.A00);
        parcel.writeInt(this.A01);
    }
}
