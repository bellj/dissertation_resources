package X;

/* renamed from: X.02E  reason: invalid class name */
/* loaded from: classes.dex */
public interface AnonymousClass02E {
    @Override // X.AnonymousClass02E
    boolean isNestedScrollingEnabled();

    @Override // X.AnonymousClass02E
    void setNestedScrollingEnabled(boolean z);

    @Override // X.AnonymousClass02E
    void stopNestedScroll();
}
