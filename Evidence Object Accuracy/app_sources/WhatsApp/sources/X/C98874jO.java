package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4jO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98874jO implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new C78293og[i];
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        String str = null;
        int i = 0;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                C95664e9.A0F(parcel, readInt, 4);
                parcel.readInt();
            } else if (c == 2) {
                str = C95664e9.A08(parcel, readInt);
            } else if (c != 3) {
                C95664e9.A0D(parcel, readInt);
            } else {
                i = C95664e9.A02(parcel, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78293og(str, i);
    }
}
