package X;

import android.graphics.BitmapFactory;
import android.util.Base64;
import java.util.Arrays;

/* renamed from: X.0p3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16460p3 {
    public Float A00;
    public boolean A01;
    public boolean A02;
    public byte[] A03;
    public final AbstractC15340mz A04;

    public static boolean A00(byte b) {
        return b == 1 || b == 3 || b == 13 || b == 9 || b == 5 || b == 16 || b == 23 || b == 37 || b == 24 || b == 44 || b == 20 || b == 25 || b == 26 || b == 28 || b == 29 || b == 30 || b == 42 || b == 43 || b == 45 || b == 52 || b == 54 || b == 55 || b == 57 || b == 62 || b == 63;
    }

    public C16460p3(AbstractC15340mz r3) {
        if (A00(r3.A0y)) {
            this.A04 = r3;
            return;
        }
        throw new IllegalStateException("this message should not have a thumbnail");
    }

    public synchronized void A01(byte[] bArr) {
        if (!this.A01) {
            this.A03 = bArr;
            this.A01 = true;
        }
    }

    public synchronized void A02(byte[] bArr) {
        A03(bArr, false);
    }

    public synchronized void A03(byte[] bArr, boolean z) {
        C16150oX r4;
        float f;
        int i;
        AbstractC15340mz r5 = this.A04;
        if ((r5 instanceof AbstractC16130oV) && (r4 = ((AbstractC16130oV) r5).A02) != null) {
            if (bArr != null) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
                int i2 = options.outWidth;
                if (i2 > 0 && (i = options.outHeight) > 0) {
                    f = ((float) i) / ((float) i2);
                    r4.A00 = f;
                }
            }
            f = -1.0f;
            r4.A00 = f;
        }
        if (z) {
            r5.A0w(bArr);
            this.A02 = false;
        } else {
            r5.A0w(null);
            this.A02 = true;
        }
        this.A00 = null;
        this.A01 = true;
        this.A03 = bArr;
    }

    public synchronized boolean A04() {
        boolean z = true;
        if (!this.A01 || this.A03 == null) {
            AbstractC15340mz r2 = this.A04;
            if ((r2.A03() != 0 || r2.A0I() == null || r2.A0I().length() <= 0) && (r2.A13() == null || r2.A13().length <= 0)) {
                if (r2 instanceof AnonymousClass1XP) {
                    if (((AnonymousClass1XP) r2).A02 == 2) {
                    }
                    z = false;
                } else if (!(r2 instanceof AbstractC16130oV)) {
                    return false;
                } else {
                    C16150oX r0 = ((AbstractC16130oV) r2).A02;
                    if (r0 != null && r0.A00 > 0.0f) {
                    }
                    z = false;
                }
            }
        }
        return z;
    }

    public synchronized boolean A05() {
        return this.A01;
    }

    public byte[] A06() {
        byte[] A13;
        String A0I;
        AbstractC15340mz r3 = this.A04;
        if (r3.A03() == 0 && (A0I = r3.A0I()) != null && A0I.length() > 0) {
            try {
                return Base64.decode(r3.A0I(), 0);
            } catch (IllegalArgumentException unused) {
                return null;
            }
        } else if (r3.A03() != 1 || (A13 = r3.A13()) == null || A13.length <= 0) {
            return null;
        } else {
            return A13;
        }
    }

    public synchronized byte[] A07() {
        if (this.A01) {
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("thumbnail not loaded, key=");
            sb.append(this.A04.A0z);
            throw new IllegalStateException(sb.toString());
        }
        return this.A03;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0027, code lost:
        if (r0 != null) goto L_0x0029;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r4) {
        /*
            r3 = this;
            if (r3 != r4) goto L_0x0004
            r0 = 1
            return r0
        L_0x0004:
            r2 = 0
            if (r4 == 0) goto L_0x0029
            java.lang.Class r1 = r3.getClass()
            java.lang.Class r0 = r4.getClass()
            if (r1 != r0) goto L_0x0029
            X.0p3 r4 = (X.C16460p3) r4
            X.0mz r0 = r3.A04
            X.1IS r1 = r0.A0z
            X.0mz r0 = r4.A04
            X.1IS r0 = r0.A0z
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0029
            byte[] r1 = r3.A03
            byte[] r0 = r4.A03
            if (r1 != 0) goto L_0x002a
            if (r0 == 0) goto L_0x0031
        L_0x0029:
            return r2
        L_0x002a:
            boolean r0 = java.util.Arrays.equals(r1, r0)
            if (r0 != 0) goto L_0x0031
            return r2
        L_0x0031:
            java.lang.Float r1 = r3.A00
            java.lang.Float r0 = r4.A00
            boolean r0 = X.C29941Vi.A00(r1, r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C16460p3.equals(java.lang.Object):boolean");
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A04.A0z, this.A03, this.A00});
    }
}
