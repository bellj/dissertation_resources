package X;

import java.util.Arrays;

/* renamed from: X.1bX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C32051bX {
    public final byte A00;
    public final byte[] A01;

    public C32051bX(byte[] bArr, byte b) {
        this.A01 = bArr;
        this.A00 = b;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof C32051bX)) {
            return false;
        }
        return Arrays.equals(this.A01, ((C32051bX) obj).A01);
    }

    public int hashCode() {
        return Arrays.hashCode(this.A01);
    }
}
