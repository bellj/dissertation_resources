package X;

import java.util.Set;

/* renamed from: X.0jE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class C13150jE implements AbstractC13110jA {
    public static final C13150jE A00 = new C13150jE();

    @Override // X.AbstractC13110jA
    public Object A80(AbstractC13170jG r5) {
        Set A03 = r5.A03();
        C13420jh r2 = C13420jh.A01;
        if (r2 == null) {
            synchronized (C13420jh.class) {
                r2 = C13420jh.A01;
                if (r2 == null) {
                    r2 = new C13420jh();
                    C13420jh.A01 = r2;
                }
            }
        }
        return new C13130jC(r2, A03);
    }
}
