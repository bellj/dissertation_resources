package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5aT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117705aT extends LinearLayout implements AnonymousClass004 {
    public ImageView A00;
    public TextView A01;
    public C18600si A02;
    public AnonymousClass2P7 A03;
    public boolean A04;

    public C117705aT(Context context) {
        super(context);
        if (!this.A04) {
            this.A04 = true;
            this.A02 = C117315Zl.A0A(AnonymousClass2P6.A00(generatedComponent()));
        }
        View inflate = C12960it.A0E(this).inflate(R.layout.payment_help_support_information_row, (ViewGroup) this, true);
        this.A00 = C12970iu.A0L(inflate, R.id.bank_logo);
        this.A01 = C12960it.A0J(inflate, R.id.contact_bank_details);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A03;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A03 = r0;
        }
        return r0.generatedComponent();
    }

    public final void setBankContactDetails(AbstractC28901Pl r8, String str, String str2) {
        TextView textView;
        boolean isEmpty = TextUtils.isEmpty(str2);
        Context context = getContext();
        if (!isEmpty) {
            Object[] objArr = new Object[3];
            objArr[0] = r8.A0B;
            objArr[1] = str2;
            String A0X = C12960it.A0X(context, str, objArr, 2, R.string.upi_contact_bank_with_name_and_phone_number);
            SpannableString spannableString = new SpannableString(A0X);
            C117295Zj.A0l(spannableString, C12960it.A0d(str2, C12960it.A0k("tel:")), A0X, str2);
            textView = this.A01;
            textView.setText(spannableString);
        } else {
            Object[] objArr2 = new Object[2];
            objArr2[0] = r8.A0B;
            String A0X2 = C12960it.A0X(context, str, objArr2, 1, R.string.upi_contact_support_for_payment);
            textView = this.A01;
            textView.setText(A0X2);
        }
        Bitmap A05 = r8.A05();
        if (A05 != null) {
            ImageView imageView = this.A00;
            imageView.setImageBitmap(A05);
            imageView.setVisibility(0);
        }
        textView.setVisibility(0);
    }

    public void setContactInformation(AbstractC28901Pl r4, String str, String str2, String str3) {
        String string = this.A02.A01().getString("payments_support_phone_number", null);
        if (!TextUtils.isEmpty(string) && C31001Zq.A09(str2)) {
            setWhatsAppContactDetails(string, str2);
        } else if (r4 != null && C31001Zq.A09(str3)) {
            setBankContactDetails(r4, str3, str);
        } else if (!TextUtils.isEmpty(string)) {
            setWhatsAppContactDetails(string, null);
        } else {
            setVisibility(8);
        }
    }

    public final void setWhatsAppContactDetails(String str, String str2) {
        int i;
        Object[] objArr;
        boolean A09 = C31001Zq.A09(str2);
        Context context = getContext();
        if (A09) {
            i = R.string.upi_contact_support_for_payment;
            objArr = C12980iv.A1a();
            objArr[0] = str;
            objArr[1] = str2;
        } else {
            i = R.string.upi_contact_support_for_payment_no_transaction;
            objArr = new Object[]{str};
        }
        String string = context.getString(i, objArr);
        SpannableString spannableString = new SpannableString(string);
        C117295Zj.A0l(spannableString, C12960it.A0d(str, C12960it.A0k("tel:")), string, str);
        TextView textView = this.A01;
        textView.setText(spannableString);
        textView.setVisibility(0);
    }
}
