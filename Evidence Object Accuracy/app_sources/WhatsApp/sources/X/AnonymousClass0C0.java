package X;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.widget.Switch;

/* renamed from: X.0C0  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0C0 extends Switch implements AbstractC12680iK {
    public /* synthetic */ AnonymousClass0C0(Context context) {
        super(context);
    }

    @Override // X.AbstractC12680iK
    public void Ad3(ColorStateList colorStateList, boolean z) {
        PorterDuff.Mode mode;
        if (Build.VERSION.SDK_INT >= 23) {
            super.setTrackTintList(colorStateList);
            if (z) {
                mode = PorterDuff.Mode.SRC_OVER;
            } else {
                mode = PorterDuff.Mode.SRC_IN;
            }
            setTrackTintMode(mode);
            return;
        }
        Drawable trackDrawable = getTrackDrawable();
        if (trackDrawable != null) {
            Drawable mutate = C015607k.A03(trackDrawable).mutate();
            C015607k.A04(colorStateList, mutate);
            if (mutate.isStateful()) {
                mutate.setState(getDrawableState());
            }
            setTrackDrawable(mutate);
        }
    }

    @Override // android.widget.Switch, X.AbstractC12680iK
    public void setThumbTintList(ColorStateList colorStateList) {
        if (Build.VERSION.SDK_INT >= 23) {
            super.setThumbTintList(colorStateList);
            return;
        }
        Drawable thumbDrawable = getThumbDrawable();
        if (thumbDrawable != null) {
            Drawable mutate = C015607k.A03(thumbDrawable).mutate();
            C015607k.A04(colorStateList, mutate);
            if (mutate.isStateful()) {
                mutate.setState(getDrawableState());
            }
            setThumbDrawable(mutate);
        }
    }
}
