package X;

/* renamed from: X.1pG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class RunnableC39081pG implements Runnable, Comparable, AbstractC14600lh {
    public final Object A00;
    public final Object A01;
    public final Runnable A02;
    public volatile boolean A03 = false;
    public final /* synthetic */ AbstractC14550lc A04;

    public RunnableC39081pG(AbstractC14550lc r2, Object obj, Object obj2, Runnable runnable) {
        this.A04 = r2;
        this.A01 = obj;
        this.A02 = runnable;
        this.A00 = obj2;
    }

    @Override // X.AbstractC14600lh
    public void cancel() {
        Runnable runnable = this.A02;
        if (runnable instanceof AbstractC14600lh) {
            ((AbstractC14600lh) runnable).cancel();
        }
    }

    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(Object obj) {
        RunnableC39081pG r3 = (RunnableC39081pG) obj;
        Runnable runnable = this.A02;
        if (runnable instanceof Comparable) {
            return ((Comparable) runnable).compareTo(r3.A02);
        }
        return 0;
    }

    @Override // java.lang.Runnable
    public void run() {
        try {
            this.A03 = true;
            this.A02.run();
        } finally {
            this.A04.A04(this, this.A01);
            this.A03 = false;
        }
    }
}
