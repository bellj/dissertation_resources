package X;

import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: X.1Wh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C30151Wh {
    public C30201Wm A00;
    public C30181Wk A01;
    public C30231Wp A02;
    public C30171Wj A03 = C30171Wj.A04;
    public UserJid A04;
    public String A05;
    public String A06;
    public String A07;
    public String A08;
    public String A09;
    public String A0A;
    public String A0B;
    public String A0C;
    public String A0D;
    public List A0E = new ArrayList();
    public List A0F = new ArrayList();
    public boolean A0G;
    public boolean A0H;
    public boolean A0I;
    public boolean A0J;
    public boolean A0K;

    public C30151Wh() {
    }

    public C30151Wh(C30141Wg r3) {
        this.A04 = r3.A04;
        this.A0C = r3.A0C;
        this.A0E = new ArrayList(r3.A0E);
        this.A0F = new ArrayList(r3.A0F);
        this.A0A = r3.A0A;
        this.A09 = r3.A09;
        this.A03 = r3.A03;
        this.A00 = r3.A00;
        this.A0G = r3.A0I;
        this.A05 = r3.A05;
        this.A0B = r3.A0B;
        this.A06 = r3.A06;
        this.A0H = r3.A0G;
        this.A0D = r3.A0D;
        this.A0I = r3.A0H;
        this.A0K = r3.A0K;
        this.A02 = r3.A02;
        this.A08 = r3.A08;
        this.A07 = r3.A07;
        this.A0J = r3.A0J;
        this.A01 = r3.A01;
    }

    public C30141Wg A00() {
        UserJid userJid = this.A04;
        String str = this.A0C;
        List unmodifiableList = Collections.unmodifiableList(new ArrayList(this.A0E));
        List unmodifiableList2 = Collections.unmodifiableList(new ArrayList(this.A0F));
        String str2 = this.A0A;
        String str3 = this.A09;
        C30171Wj r14 = this.A03;
        C30201Wm r13 = this.A00;
        boolean z = this.A0G;
        String str4 = this.A05;
        String str5 = this.A0B;
        String str6 = this.A06;
        boolean z2 = this.A0H;
        String str7 = this.A0D;
        boolean z3 = this.A0I;
        boolean z4 = this.A0K;
        return new C30141Wg(r13, this.A01, this.A02, r14, userJid, str, str2, str3, str4, str5, str6, str7, this.A08, this.A07, unmodifiableList, unmodifiableList2, z, z2, z3, z4, this.A0J);
    }
}
