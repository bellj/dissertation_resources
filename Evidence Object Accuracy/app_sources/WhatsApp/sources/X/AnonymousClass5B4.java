package X;

import android.content.Context;
import java.util.Collections;

/* renamed from: X.5B4  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5B4 implements AnonymousClass5WN {
    public final /* synthetic */ Context A00;
    public final /* synthetic */ C04570Mf A01;
    public final /* synthetic */ C105774ub A02;

    public AnonymousClass5B4(Context context, C04570Mf r2, C105774ub r3) {
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = context;
    }

    @Override // X.AnonymousClass5WN
    public void AVC(C28671On r5) {
        C28701Oq.A02(this.A00, r5, C14220l3.A01, this.A02.A00, Collections.emptyMap());
    }

    @Override // X.AnonymousClass5WN
    public void AVH(C91064Qh r5) {
        AbstractC92144Us r2;
        AnonymousClass5T4 A00;
        new AnonymousClass4VI(true);
        C94004b6 r3 = new C94004b6(new C87874Dj(), 1);
        if (r5.A00 == 5) {
            r2 = new C03580Ii(r5.A01, r3);
        } else {
            r2 = new C03570Ih(r3, r5.A02);
        }
        C06420Tn r1 = this.A01.A00;
        synchronized (r1) {
            A00 = r1.A00();
        }
        if (A00 != null) {
            A00.AV7(r2);
        }
    }
}
