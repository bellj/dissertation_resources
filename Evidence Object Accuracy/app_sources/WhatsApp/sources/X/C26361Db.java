package X;

import android.text.TextUtils;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.voipcalling.CallInfo;
import com.whatsapp.voipcalling.Voip;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

/* renamed from: X.1Db  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26361Db extends AnonymousClass1DY {
    public final C15570nT A00;
    public final C15550nR A01;
    public final C15610nY A02;
    public final C15620nZ A03;
    public final AnonymousClass13C A04;
    public final AnonymousClass01N A05;
    public final AnonymousClass01N A06;

    public C26361Db(C15570nT r1, C15550nR r2, C15610nY r3, C15620nZ r4, AnonymousClass13C r5, AnonymousClass01N r6, AnonymousClass01N r7) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
        this.A05 = r6;
        this.A06 = r7;
        this.A04 = r5;
        this.A03 = r4;
    }

    public static String A02(Voip.CallState callState) {
        switch (callState.ordinal()) {
            case 1:
            case 2:
                return "outgoing_ringing";
            case 3:
            case 8:
                return "incoming_ringing";
            case 4:
            case 5:
            case 6:
            case 11:
                return "active";
            case 7:
            case 9:
            case 10:
            default:
                return "idle";
        }
    }

    public static JSONObject A03(C15570nT r12, C15550nR r13, C15610nY r14, C15620nZ r15, AnonymousClass13C r16, C15430nF r17, CallInfo callInfo, Voip.CallState callState) {
        String A02 = A02(callState);
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("call_state", A02);
        if (!"idle".equals(A02) && callInfo != null) {
            if (!callInfo.isCaller) {
                AnonymousClass009.A05(callInfo.getPeerJid());
                jSONObject.put("caller_contact_id", r15.A01.A03(r17, callInfo.getPeerJid().getRawString()));
                jSONObject.put("caller_name", r14.A0D(r13.A0B(callInfo.getPeerJid()), false, false));
            }
            GroupJid groupJid = callInfo.groupJid;
            if (groupJid != null) {
                jSONObject.put("group_name", r14.A04(r13.A0B(groupJid)));
            }
            Set<AbstractC14640lm> keySet = callInfo.participants.keySet();
            if (!keySet.isEmpty()) {
                JSONArray jSONArray = new JSONArray();
                JSONArray jSONArray2 = new JSONArray();
                int i = 0;
                for (AbstractC14640lm r10 : keySet) {
                    if (!r12.A0F(r10)) {
                        String A0D = r14.A0D(r13.A0B(r10), false, false);
                        if (TextUtils.isEmpty(A0D)) {
                            i++;
                        } else {
                            jSONArray.put(r15.A01.A03(r17, r10.getRawString()));
                            jSONArray2.put(A0D);
                        }
                    }
                }
                jSONObject.put("call_participant_contact_ids", jSONArray);
                jSONObject.put("call_participant_names", jSONArray2);
                jSONObject.put("unnamed_call_participant_count", i);
            }
            jSONObject.put("call_id", r16.A03(r17, callInfo.callId));
            jSONObject.put("video_call", callInfo.videoEnabled);
        }
        return jSONObject;
    }
}
