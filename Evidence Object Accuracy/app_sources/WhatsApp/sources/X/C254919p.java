package X;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.19p  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C254919p {
    public final C14900mE A00;
    public final C14850m9 A01;
    public final AnonymousClass12U A02;
    public final AnonymousClass10D A03;

    public C254919p(C14900mE r1, C14850m9 r2, AnonymousClass12U r3, AnonymousClass10D r4) {
        this.A00 = r1;
        this.A02 = r3;
        this.A03 = r4;
        this.A01 = r2;
    }

    public boolean A00(Context context, Intent intent, AbstractC13860kS r11, String str, boolean z) {
        Intent A01;
        if (z) {
            ArrayList arrayList = new ArrayList();
            List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, 0);
            if (queryIntentActivities != null) {
                for (ResolveInfo resolveInfo : queryIntentActivities) {
                    ActivityInfo activityInfo = resolveInfo.activityInfo;
                    String str2 = activityInfo.name;
                    String str3 = activityInfo.applicationInfo.packageName;
                    Intent intent2 = new Intent(intent);
                    intent2.setClassName(str3, str2);
                    intent2.setPackage(str3);
                    if (str3.contains("gm") || str3.contains("email") || str3.contains("fsck.k9") || str3.contains("maildroid") || str3.contains("hotmail") || str3.contains("android.mail") || str3.contains("com.baydin.boomerang") || str3.contains("yandex.mail") || str3.contains("com.google.android.apps.inbox") || str3.contains("com.microsoft.office.outlook") || str3.contains("com.asus.email") || str3.equals("org.kman.AquaMail")) {
                        arrayList.add(intent2);
                    }
                }
            }
            int size = arrayList.size();
            if (size != 0) {
                if (size == 1) {
                    A01 = (Intent) arrayList.get(0);
                } else {
                    int i = size - 1;
                    Object obj = arrayList.get(i);
                    arrayList.remove(i);
                    arrayList.add(0, obj);
                    A01 = C38211ni.A01(null, str, arrayList);
                }
                context.startActivity(A01);
                return true;
            } else if (r11 != null) {
                r11.Ado(R.string.error_no_email_client);
                return false;
            } else {
                this.A00.A07(R.string.error_no_email_client, 0);
                return false;
            }
        } else {
            try {
                context.startActivity(Intent.createChooser(intent, str));
                return true;
            } catch (ActivityNotFoundException e) {
                Log.e("email-sender/start-activity ", e);
                this.A00.A07(R.string.error_no_email_client, 0);
                return false;
            }
        }
    }
}
