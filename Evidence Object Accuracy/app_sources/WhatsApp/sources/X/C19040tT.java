package X;

import android.database.Cursor;
import android.text.TextUtils;
import com.facebook.redex.RunnableBRunnable0Shape6S0100000_I0_6;
import com.whatsapp.util.Log;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* renamed from: X.0tT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19040tT extends AbstractC18500sY implements AbstractC19010tQ {
    public ReentrantReadWriteLock.WriteLock A00;
    public final C18740sw A01;
    public final C20090vC A02;
    public final C14850m9 A03;
    public final Object A04 = new Object();

    public C19040tT(C18740sw r3, C20090vC r4, C18480sW r5, C14850m9 r6) {
        super(r5, "message_main", 1);
        this.A03 = r6;
        this.A01 = r3;
        this.A02 = r4;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x006c, code lost:
        if (r16 != false) goto L_0x006e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0177, code lost:
        if (r8.A03.A00("message", r2, "_id = ?", new java.lang.String[]{java.lang.String.valueOf(r5.A11)}) == 0) goto L_0x0179;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0179, code lost:
        r2 = new android.content.ContentValues();
        r6.A05(r2, r5);
        r8.A03.A03(r2, "message");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0188, code lost:
        r7 = r7 + 1;
     */
    @Override // X.AbstractC18500sY
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass2Ez A09(android.database.Cursor r19) {
        /*
        // Method dump skipped, instructions count: 417
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C19040tT.A09(android.database.Cursor):X.2Ez");
    }

    @Override // X.AbstractC18500sY
    public void A0I() {
        C16490p7 r0 = this.A05;
        r0.A03();
        r0.A04();
        ReentrantReadWriteLock.WriteLock writeLock = r0.A08;
        try {
            writeLock.lock();
            C16310on A02 = r0.A02();
            r0.A04();
            C29561To r9 = r0.A05;
            C28181Ma r7 = new C28181Ma("databasehelper/finalizeMigration");
            AnonymousClass1Lx A00 = A02.A00();
            try {
                C16330op r6 = A02.A03;
                r9.A0B(r6, true);
                C29731Ul.A00(r6, "migration_completed", "DatabaseHelper", 1);
                r9.A0D(r6, C29561To.A05(r6), r9.A0F(r6));
                A00.A00();
                A02.A03(new RunnableBRunnable0Shape6S0100000_I0_6(r9, 13));
                A00.close();
                StringBuilder sb = new StringBuilder("databasehelper/finalizeMigration time spent:");
                sb.append(r7.A01());
                Log.i(sb.toString());
                A02.close();
            } catch (Throwable th) {
                try {
                    A00.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } finally {
            writeLock.unlock();
        }
    }

    @Override // X.AbstractC18500sY
    public void A0J() {
        if (this.A06.A01(A0F(), -1) <= 0 && this.A03.A07(1350)) {
            C16310on A02 = this.A05.A02();
            try {
                AnonymousClass4EM.A00(A02.A03);
                Log.i("MainMessageStore/MainMessageDatabaseMigration/onBeforeMigration/ Create chat sort_id index");
                A02.close();
            } catch (Throwable th) {
                try {
                    A02.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
    }

    @Override // X.AbstractC19010tQ
    public void AM5() {
        synchronized (this.A04) {
            ReentrantReadWriteLock.WriteLock writeLock = this.A00;
            if (writeLock == null) {
                super.A01.AaV("db-migration-lock-already-null", this.A0C, false);
            } else if (!writeLock.isHeldByCurrentThread()) {
                super.A01.AaV("db-migration-lock-not-held-by-thread", this.A0C, false);
            } else {
                this.A00.unlock();
                this.A00 = null;
            }
        }
    }

    @Override // X.AbstractC19010tQ
    public void AND() {
        C16490p7 r1 = this.A05;
        r1.A03();
        synchronized (this.A04) {
            if (this.A00 != null) {
                super.A01.AaV("db-migration-lock-already-created", this.A0C, false);
            } else {
                r1.A04();
                ReentrantReadWriteLock.WriteLock writeLock = r1.A08;
                this.A00 = writeLock;
                writeLock.lock();
            }
        }
    }

    @Override // X.AbstractC19010tQ
    public void onRollback() {
        Log.i("MainMessageStore/resetDatabaseMigration/starting");
        C16490p7 r6 = this.A05;
        C16310on A02 = r6.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            C16330op r5 = A02.A03;
            if (!TextUtils.isEmpty(AnonymousClass1Uj.A00(r5, "table", "messages"))) {
                Cursor A09 = r5.A09("SELECT COUNT (*) as rows_count FROM messages", null);
                if (!A09.moveToNext() || A09.getInt(A09.getColumnIndexOrThrow("rows_count")) <= 1) {
                    A09.close();
                } else {
                    A09.close();
                    r6.A04();
                    C29561To r4 = r6.A05;
                    C28181Ma r8 = new C28181Ma("databasehelper/rollbackMigration");
                    AnonymousClass1Lx A002 = A02.A00();
                    AnonymousClass009.A05(r5.A00);
                    r4.A0B(r5, false);
                    C29731Ul.A00(r5, "migration_completed", "DatabaseHelper", 0);
                    r4.A0D(r5, C29561To.A05(r5), r4.A0F(r5));
                    A002.A00();
                    A02.A03(new RunnableBRunnable0Shape6S0100000_I0_6(r4, 12));
                    A002.close();
                    StringBuilder sb = new StringBuilder("databasehelper/finalizeMigration time spent:");
                    sb.append(r8.A01());
                    Log.i(sb.toString());
                    r5.A01("message", null, null);
                    C21390xL r1 = this.A06;
                    r1.A03("main_message_ready");
                    r1.A03("migration_message_main_index");
                    r1.A03("migration_message_main_retry");
                    r6.A04();
                    r4.A0B(r5, false);
                    A00.A00();
                    A00.close();
                    A02.close();
                    Log.i("MainMessageStore/resetDatabaseMigration/done");
                    return;
                }
            }
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
