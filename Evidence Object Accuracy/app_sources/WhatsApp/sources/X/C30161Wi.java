package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.1Wi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30161Wi implements Parcelable {
    public static final C30161Wi A04 = new C30161Wi(null, null, null, null);
    public static final Parcelable.Creator CREATOR = new C99684kh();
    public final Double A00;
    public final Double A01;
    public final String A02;
    public final String A03;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C30161Wi(Parcel parcel) {
        String readString = parcel.readString();
        AnonymousClass009.A05(readString);
        this.A02 = readString;
        String readString2 = parcel.readString();
        AnonymousClass009.A05(readString2);
        this.A03 = readString2;
        if (parcel.readByte() == 0) {
            this.A00 = null;
        } else {
            this.A00 = Double.valueOf(parcel.readDouble());
        }
        if (parcel.readByte() == 0) {
            this.A01 = null;
        } else {
            this.A01 = Double.valueOf(parcel.readDouble());
        }
    }

    public C30161Wi(Double d, Double d2, String str, String str2) {
        this.A02 = str == null ? "" : str;
        this.A03 = str2 == null ? "" : str2;
        this.A00 = d;
        this.A01 = d2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0030, code lost:
        if (r1.equals(r0) == false) goto L_0x0032;
     */
    @Override // java.lang.Object
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r5) {
        /*
            r4 = this;
            r3 = 1
            if (r4 == r5) goto L_0x0044
            r2 = 0
            if (r5 == 0) goto L_0x0032
            java.lang.Class r1 = r4.getClass()
            java.lang.Class r0 = r5.getClass()
            if (r1 != r0) goto L_0x0032
            X.1Wi r5 = (X.C30161Wi) r5
            java.lang.String r1 = r4.A02
            java.lang.String r0 = r5.A02
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0032
            java.lang.String r1 = r4.A03
            java.lang.String r0 = r5.A03
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0032
            java.lang.Double r1 = r4.A00
            java.lang.Double r0 = r5.A00
            if (r1 == 0) goto L_0x0033
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0036
        L_0x0032:
            return r2
        L_0x0033:
            if (r0 == 0) goto L_0x0036
            return r2
        L_0x0036:
            java.lang.Double r1 = r4.A01
            java.lang.Double r0 = r5.A01
            if (r1 == 0) goto L_0x0041
            boolean r3 = r1.equals(r0)
            return r3
        L_0x0041:
            if (r0 == 0) goto L_0x0044
            r3 = 0
        L_0x0044:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C30161Wi.equals(java.lang.Object):boolean");
    }

    @Override // java.lang.Object
    public int hashCode() {
        int i;
        int hashCode = ((this.A02.hashCode() * 31) + this.A03.hashCode()) * 31;
        Double d = this.A00;
        int i2 = 0;
        if (d != null) {
            i = d.hashCode();
        } else {
            i = 0;
        }
        int i3 = (hashCode + i) * 31;
        Double d2 = this.A01;
        if (d2 != null) {
            i2 = d2.hashCode();
        }
        return i3 + i2;
    }

    @Override // java.lang.Object
    public String toString() {
        Object[] objArr = new Object[4];
        objArr[0] = this.A02;
        objArr[1] = this.A03;
        Object obj = this.A00;
        Object obj2 = "";
        if (obj == null) {
            obj = obj2;
        }
        objArr[2] = obj;
        Double d = this.A01;
        if (d != null) {
            obj2 = d;
        }
        objArr[3] = obj2;
        return String.format("%s, %s, %s, %s", objArr);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A02);
        parcel.writeString(this.A03);
        Double d = this.A00;
        if (d == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeDouble(d.doubleValue());
        }
        Double d2 = this.A01;
        if (d2 == null) {
            parcel.writeByte((byte) 0);
            return;
        }
        parcel.writeByte((byte) 1);
        parcel.writeDouble(d2.doubleValue());
    }
}
