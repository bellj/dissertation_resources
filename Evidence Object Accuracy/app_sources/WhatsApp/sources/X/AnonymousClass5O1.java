package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.5O1  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5O1 extends EnumC87374Bg {
    public AnonymousClass5O1() {
        super("PKCS12", 2);
    }

    @Override // X.AnonymousClass5WQ
    public byte[] A7j(char[] cArr) {
        return AbstractC94944cn.A00(cArr);
    }

    @Override // X.AnonymousClass5WQ
    public String AHM() {
        return "PKCS12";
    }
}
