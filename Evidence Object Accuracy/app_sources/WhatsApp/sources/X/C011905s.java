package X;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;

/* renamed from: X.05s  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C011905s {
    public static C011905s A01;
    public static final PorterDuff.Mode A02 = PorterDuff.Mode.SRC_IN;
    public C012005t A00;

    public static synchronized PorterDuffColorFilter A00(PorterDuff.Mode mode, int i) {
        PorterDuffColorFilter A00;
        synchronized (C011905s.class) {
            A00 = C012005t.A00(mode, i);
        }
        return A00;
    }

    public static synchronized C011905s A01() {
        C011905s r0;
        synchronized (C011905s.class) {
            if (A01 == null) {
                A02();
            }
            r0 = A01;
        }
        return r0;
    }

    public static synchronized void A02() {
        synchronized (C011905s.class) {
            if (A01 == null) {
                C011905s r1 = new C011905s();
                A01 = r1;
                r1.A00 = C012005t.A01();
                C012005t r12 = A01.A00;
                AnonymousClass060 r0 = new AnonymousClass060();
                synchronized (r12) {
                    r12.A01 = r0;
                }
            }
        }
    }

    public synchronized Drawable A03(Context context, int i) {
        return this.A00.A04(context, i);
    }
}
