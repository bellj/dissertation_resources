package X;

import android.graphics.PointF;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: X.0az  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08350az implements AbstractC12050hI {
    public static final C08350az A00 = new C08350az();
    public static final C05850Rf A01 = C05850Rf.A00("c", "v", "i", "o");

    @Override // X.AbstractC12050hI
    public Object AYt(AbstractC08850bx r17, float f) {
        if (r17.A05() == EnumC03770Jb.BEGIN_ARRAY) {
            r17.A09();
        }
        r17.A0A();
        List list = null;
        List list2 = null;
        List list3 = null;
        boolean z = false;
        while (r17.A0H()) {
            int A04 = r17.A04(A01);
            if (A04 == 0) {
                z = r17.A0I();
            } else if (A04 == 1) {
                list = AnonymousClass0UD.A03(r17, f);
            } else if (A04 == 2) {
                list2 = AnonymousClass0UD.A03(r17, f);
            } else if (A04 != 3) {
                r17.A0D();
                r17.A0E();
            } else {
                list3 = AnonymousClass0UD.A03(r17, f);
            }
        }
        r17.A0C();
        if (r17.A05() == EnumC03770Jb.END_ARRAY) {
            r17.A0B();
        }
        if (list == null || list2 == null || list3 == null) {
            throw new IllegalArgumentException("Shape data was missing information.");
        } else if (list.isEmpty()) {
            return new AnonymousClass0SE(new PointF(), Collections.emptyList(), false);
        } else {
            int size = list.size();
            PointF pointF = (PointF) list.get(0);
            ArrayList arrayList = new ArrayList(size);
            for (int i = 1; i < size; i++) {
                PointF pointF2 = (PointF) list.get(i);
                int i2 = i - 1;
                PointF pointF3 = (PointF) list.get(i2);
                PointF pointF4 = (PointF) list3.get(i2);
                PointF pointF5 = (PointF) list2.get(i);
                arrayList.add(new C05900Rk(new PointF(pointF3.x + pointF4.x, pointF3.y + pointF4.y), new PointF(pointF2.x + pointF5.x, pointF2.y + pointF5.y), pointF2));
            }
            if (z) {
                PointF pointF6 = (PointF) list.get(0);
                int i3 = size - 1;
                PointF pointF7 = (PointF) list.get(i3);
                PointF pointF8 = (PointF) list3.get(i3);
                PointF pointF9 = (PointF) list2.get(0);
                arrayList.add(new C05900Rk(new PointF(pointF7.x + pointF8.x, pointF7.y + pointF8.y), new PointF(pointF6.x + pointF9.x, pointF6.y + pointF9.y), pointF6));
            }
            return new AnonymousClass0SE(pointF, arrayList, z);
        }
    }
}
