package X;

import android.view.KeyEvent;
import android.widget.TextView;
import com.whatsapp.Conversation;

/* renamed from: X.3OV  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3OV implements TextView.OnEditorActionListener {
    public boolean A00;
    public final /* synthetic */ Conversation A01;

    public AnonymousClass3OV(Conversation conversation) {
        this.A01 = conversation;
    }

    @Override // android.widget.TextView.OnEditorActionListener
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (keyEvent != null) {
            keyEvent.getKeyCode();
        }
        if (i == 4) {
            this.A01.A3J(false);
            return true;
        } else if (keyEvent == null || keyEvent.getKeyCode() != 66) {
            return false;
        } else {
            if (this.A00) {
                this.A00 = false;
                return true;
            }
            Conversation conversation = this.A01;
            if (((ActivityC13810kN) conversation).A09.A00.getBoolean("input_enter_send", true)) {
                conversation.A3J(false);
            } else {
                int selectionStart = conversation.A2w.getSelectionStart();
                int selectionEnd = conversation.A2w.getSelectionEnd();
                if (selectionStart != conversation.A2w.length()) {
                    conversation.A2w.getText().replace(Math.min(selectionStart, selectionEnd), Math.max(selectionStart, selectionEnd), "\n", 0, 1);
                } else {
                    conversation.A2w.append("\n");
                }
            }
            this.A00 = true;
            return true;
        }
    }
}
