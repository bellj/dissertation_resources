package X;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.view.View;

/* renamed from: X.03R  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass03R extends AnonymousClass03S {
    public static final Matrix A0Y = new Matrix();
    public static final Paint A0Z = new Paint(1);
    public static final Path A0a = new Path();
    public static final C05050Ob A0b = new C05050Ob();
    public float A00;
    public float A01;
    public float A02;
    public float A03;
    public float A04;
    public float A05;
    public float A06;
    public float A07;
    public float A08;
    public float A09;
    public float A0A;
    public float A0B;
    public float A0C;
    public float A0D;
    public float A0E;
    public int A0F;
    public int A0G;
    public View A0H;
    public AnonymousClass04Q A0I;
    public C04640Mm A0J;
    public AnonymousClass03T A0K;
    public Object A0L;
    public String A0M;
    public String A0N;
    public boolean A0O;
    public boolean A0P;
    public boolean A0Q;
    public boolean A0R;
    public final float A0S;
    public final float A0T;
    public final int A0U;
    public final float[] A0V;
    public final float[] A0W;
    public final float[] A0X = new float[2];

    public AnonymousClass03R(AnonymousClass04Q r7, C06050Rz r8) {
        super(r7);
        float[] fArr = new float[2];
        this.A0W = fArr;
        float[] fArr2 = new float[2];
        this.A0V = fArr2;
        AnonymousClass03T r2 = r8.A01;
        this.A0K = r2;
        super.A00 = (double) AnonymousClass0U9.A02(r2.A01);
        super.A01 = (double) AnonymousClass0U9.A01(r2.A00);
        C04640Mm r0 = r8.A00;
        this.A0J = r0 == null ? AnonymousClass03Q.A00() : r0;
        this.A0N = r8.A03;
        this.A0M = r8.A02;
        this.A00 = 1.0f;
        super.A04 = true;
        super.A02 = 0.0f;
        float[] fArr3 = r8.A04;
        fArr2[0] = fArr3[0];
        fArr2[1] = fArr3[1];
        float[] fArr4 = r8.A05;
        fArr[0] = fArr4[0];
        fArr[1] = fArr4[1];
        float f = super.A05;
        this.A0T = 48.0f * f;
        this.A0U = (int) (8.0f * f);
        this.A0S = f * 5.0f;
        A0D();
    }

    @Override // X.AnonymousClass03S
    public int A00(float f, float f2) {
        if (this.A0Q) {
            Matrix matrix = A0Y;
            matrix.setRotate(this.A06);
            float[] fArr = this.A0X;
            fArr[0] = this.A02 - this.A04;
            fArr[1] = this.A0B - this.A07;
            matrix.mapPoints(fArr);
            fArr[0] = fArr[0] + this.A0D;
            float f3 = fArr[1] + (this.A0E - this.A0S);
            fArr[1] = f3;
            int i = this.A0G;
            float f4 = (float) this.A0F;
            float f5 = f3 - f4;
            float f6 = fArr[0];
            float f7 = (float) (i >> 1);
            float f8 = f6 - f7;
            float f9 = f6 + f7;
            float f10 = this.A0T;
            if (f4 <= f10) {
                float f11 = this.A0C;
                f5 -= f11;
                f3 -= f11;
            }
            if (((float) i) <= f10) {
                float f12 = this.A03;
                f8 -= f12;
                f9 += f12;
            }
            if (f >= f8 && f <= f9 && f2 >= f5 && f2 <= f3) {
                this.A0O = true;
                return 2;
            }
        }
        this.A0O = false;
        if (!A0G()) {
            return 0;
        }
        long nanoTime = System.nanoTime();
        try {
            float[] fArr2 = this.A0X;
            fArr2[0] = f;
            fArr2[1] = f2;
            Matrix matrix2 = A0Y;
            matrix2.setRotate(-this.A06, this.A0D, this.A0E);
            matrix2.mapPoints(fArr2);
            float f13 = fArr2[0];
            float f14 = this.A0D;
            float f15 = f14 - this.A04;
            if (f13 >= f15 && f13 <= this.A05 + f14) {
                float f16 = fArr2[1];
                float f17 = this.A0E;
                if (f16 >= f17 - this.A07 && f16 <= f17 + this.A01) {
                    return 2;
                }
            }
            float f18 = this.A03;
            if (f13 >= f15 - f18 && f13 <= f14 + this.A05 + f18) {
                float f19 = fArr2[1];
                float f20 = this.A0E;
                float f21 = this.A0C;
                if (f19 >= (f20 - this.A07) - f21) {
                    if (f19 <= f20 + this.A01 + f21) {
                        return 1;
                    }
                }
            }
            return 0;
        } finally {
            C06160Sk.A0H.A02(System.nanoTime() - nanoTime);
        }
    }

    @Override // X.AnonymousClass03S
    public void A02() {
        A0A();
    }

    @Override // X.AnonymousClass03S
    public void A03(float f, float f2) {
        if (this.A0O) {
            this.A0P = true;
            A01();
        }
    }

    @Override // X.AnonymousClass03S
    public void A04(float f, float f2) {
        if (this.A0O && this.A0P) {
            this.A0P = false;
            A01();
        }
    }

    @Override // X.AnonymousClass03S
    public boolean A05(float f, float f2) {
        AnonymousClass04Q r0;
        AbstractC12170hU r02;
        AnonymousClass04Q r4 = super.A09;
        if (!this.A0O || (r0 = this.A0I) == null || (r02 = r0.A0A) == null) {
            AnonymousClass04Q r03 = this.A0I;
            if (r03 != null) {
                AbstractC12190hW r04 = r03.A0C;
                if (r04 != null) {
                    r04.ASQ(this);
                    return true;
                }
                A0B();
                AnonymousClass03T r05 = this.A0K;
                C05200Oq r2 = new C05200Oq();
                r2.A06 = r05;
                r4.A0B(r2, null, 500);
                return true;
            }
        } else {
            r02.ARN(this);
        }
        return true;
    }

    @Override // X.AnonymousClass03S
    public void A08(Canvas canvas) {
        long nanoTime = System.nanoTime();
        try {
            Bitmap bitmap = this.A0J.A00;
            boolean z = this.A0Q && !(this.A0N == null && this.A0M == null);
            if (A0G()) {
                Paint paint = A0Z;
                paint.setAlpha((int) (this.A00 * 255.0f));
                Matrix matrix = A0Y;
                matrix.setTranslate(this.A0D - this.A04, this.A0E - this.A07);
                matrix.postRotate(this.A06, this.A0D, this.A0E);
                canvas.drawBitmap(bitmap, matrix, paint);
                paint.setAlpha(255);
                if (z) {
                    float[] fArr = this.A0X;
                    fArr[0] = this.A02;
                    fArr[1] = this.A0B;
                    matrix.mapPoints(fArr);
                    long nanoTime2 = System.nanoTime();
                    int i = this.A0G / 2;
                    if (this.A0R) {
                        Path path = A0a;
                        path.reset();
                        float f = (float) i;
                        float f2 = fArr[1] - ((float) (this.A0F * 1));
                        float f3 = this.A0S;
                        float f4 = f3 * ((float) 1);
                        path.moveTo(fArr[0] - f, f2 - f4);
                        path.lineTo(fArr[0] + f, (fArr[1] - ((float) (this.A0F * 1))) - f4);
                        path.lineTo(fArr[0] + f, fArr[1] - f4);
                        path.lineTo(fArr[0] + f3, fArr[1] - f4);
                        path.lineTo(fArr[0], fArr[1]);
                        path.lineTo(fArr[0] - f3, fArr[1] - f4);
                        path.lineTo(fArr[0] - f, fArr[1] - f4);
                        path.close();
                        paint.setColor(-16777216);
                        paint.setShadowLayer(12.0f, 0.0f, 0.0f, -16777216);
                        canvas.drawPath(path, paint);
                        int i2 = -1;
                        if (this.A0P) {
                            i2 = -2236963;
                        }
                        paint.setColor(i2);
                        canvas.drawPath(path, paint);
                    }
                    matrix.setTranslate(fArr[0] - ((float) i), (fArr[1] - ((float) this.A0F)) - this.A0S);
                    this.A0H.setDrawingCacheEnabled(true);
                    canvas.drawBitmap(this.A0H.getDrawingCache(), matrix, paint);
                    C06160Sk.A0D.A02(System.nanoTime() - nanoTime2);
                }
            }
        } finally {
            C06160Sk.A0G.A02(System.nanoTime() - nanoTime);
        }
    }

    public void A0A() {
        if (this.A0Q && super.A03 != 1) {
            super.A03 = 1;
            AnonymousClass04Q r0 = this.A0I;
            if (r0 != null) {
                r0.A0D(this);
                r0.A0C(this);
            }
        }
        this.A0Q = false;
    }

    public void A0B() {
        A0C();
        if (super.A03 != 4) {
            super.A03 = 4;
            AnonymousClass04Q r0 = this.A0I;
            if (r0 != null) {
                r0.A0D(this);
                r0.A0C(this);
            }
        }
        this.A0Q = true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00d2, code lost:
        if (r1 != null) goto L_0x007e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0C() {
        /*
            r11 = this;
            r1 = 0
            r11.A0H = r1
            X.04Q r6 = r11.A09
            X.0hS r0 = r6.A08
            r5 = 0
            r10 = 1
            if (r0 == 0) goto L_0x0013
            android.view.View r1 = r0.ADS(r11)
            r11.A0H = r1
            r11.A0R = r5
        L_0x0013:
            r2 = -2
            if (r1 == 0) goto L_0x005d
            android.view.ViewGroup$LayoutParams r0 = r1.getLayoutParams()
            if (r0 != 0) goto L_0x0026
            android.view.View r1 = r11.A0H
            android.view.ViewGroup$LayoutParams r0 = new android.view.ViewGroup$LayoutParams
            r0.<init>(r2, r2)
            r1.setLayoutParams(r0)
        L_0x0026:
            android.view.View r4 = r11.A0H
            X.04L r3 = r6.A0E
            int r0 = r3.getWidth()
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            int r1 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r2)
            int r0 = r3.getHeight()
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r2)
            r4.measure(r1, r0)
            android.view.View r0 = r11.A0H
            int r0 = r0.getMeasuredWidth()
            r11.A0G = r0
            android.view.View r0 = r11.A0H
            int r2 = r0.getMeasuredHeight()
            r11.A0F = r2
            android.view.View r1 = r11.A0H
            int r0 = r11.A0G
            r1.layout(r5, r5, r0, r2)
            r11.A0D()
            r11.A01()
            return
        L_0x005d:
            r11.A0R = r10
            android.widget.LinearLayout$LayoutParams r4 = new android.widget.LinearLayout$LayoutParams
            r4.<init>(r2, r2)
            android.content.Context r9 = r11.A08
            android.widget.LinearLayout r3 = new android.widget.LinearLayout
            r3.<init>(r9)
            r3.setLayoutParams(r4)
            r3.setOrientation(r10)
            java.lang.String r1 = r11.A0N
            r2 = 3
            if (r1 == 0) goto L_0x00cf
            java.lang.String r0 = r11.A0M
            if (r0 == 0) goto L_0x00cf
            int r8 = r11.A0U
            int r7 = r8 / r2
        L_0x007e:
            android.widget.TextView r1 = new android.widget.TextView
            r1.<init>(r9)
            r1.setPadding(r8, r8, r8, r7)
            java.lang.String r0 = r11.A0N
            r1.setText(r0)
            android.text.TextUtils$TruncateAt r0 = android.text.TextUtils.TruncateAt.END
            r1.setEllipsize(r0)
            r1.setMaxLines(r10)
            android.graphics.Typeface r0 = android.graphics.Typeface.DEFAULT_BOLD
            r1.setTypeface(r0)
            r0 = 17
            r1.setGravity(r0)
            r0 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r1.setTextColor(r0)
            r0 = -1
            r4.width = r0
            r3.addView(r1, r4)
        L_0x00a8:
            java.lang.String r0 = r11.A0M
            if (r0 == 0) goto L_0x00cb
            android.widget.TextView r1 = new android.widget.TextView
            r1.<init>(r9)
            r1.setPadding(r8, r7, r8, r8)
            r1.setText(r0)
            r0 = 5
            r1.setMaxLines(r0)
            android.text.TextUtils$TruncateAt r0 = android.text.TextUtils.TruncateAt.END
            r1.setEllipsize(r0)
            r0 = -12303292(0xffffffffff444444, float:-2.6088314E38)
            r1.setTextColor(r0)
            r4.gravity = r2
            r3.addView(r1, r4)
        L_0x00cb:
            r11.A0H = r3
            goto L_0x0026
        L_0x00cf:
            int r7 = r11.A0U
            r8 = r7
            if (r1 == 0) goto L_0x00a8
            goto L_0x007e
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass03R.A0C():void");
    }

    public final void A0D() {
        Bitmap bitmap = this.A0J.A00;
        float width = (float) bitmap.getWidth();
        float[] fArr = this.A0V;
        float f = fArr[0] * width;
        this.A04 = f;
        this.A05 = width - f;
        float height = (float) bitmap.getHeight();
        float f2 = fArr[1] * height;
        this.A07 = f2;
        this.A01 = height - f2;
        float[] fArr2 = this.A0W;
        this.A02 = fArr2[0] * width;
        this.A0B = fArr2[1] * height;
        float f3 = this.A0T;
        if (width < f3) {
            this.A03 = (f3 - width) / 2.0f;
        } else {
            this.A03 = 0.0f;
        }
        if (height < f3) {
            this.A0C = (f3 - height) / 2.0f;
        } else {
            this.A0C = 0.0f;
        }
        Matrix matrix = A0Y;
        matrix.setRotate(0.0f);
        float[] fArr3 = super.A0C;
        fArr3[0] = this.A04 - this.A02;
        fArr3[1] = this.A07 - this.A0B;
        matrix.mapPoints(fArr3);
        this.A0A = ((float) this.A0F) + this.A0S + fArr3[1];
        float f4 = (float) (this.A0G >> 1);
        float f5 = fArr3[0];
        this.A08 = f4 + f5;
        this.A09 = f4 - f5;
    }

    public void A0E(C04640Mm r1) {
        if (r1 == null) {
            r1 = AnonymousClass03Q.A00();
        }
        this.A0J = r1;
        A0D();
        A01();
    }

    public void A0F(AnonymousClass03T r3) {
        this.A0K = r3;
        super.A00 = (double) AnonymousClass0U9.A02(r3.A01);
        super.A01 = (double) AnonymousClass0U9.A01(r3.A00);
        A01();
    }

    public final boolean A0G() {
        boolean z;
        if (!this.A0Q || (this.A0N == null && this.A0M == null)) {
            z = false;
        } else {
            z = true;
        }
        this.A06 = 0.0f + 0.0f;
        C05050Ob r10 = A0b;
        double d = super.A00;
        AnonymousClass0U9 r12 = super.A0A;
        double A03 = d - r12.A03(this.A04);
        r10.A01 = A03;
        double A032 = d + r12.A03(this.A05);
        r10.A02 = A032;
        double d2 = super.A01;
        double A033 = d2 - r12.A03(this.A07);
        r10.A03 = A033;
        r10.A00 = d2 + r12.A03(this.A01);
        if (z) {
            double A034 = d2 - r12.A03(this.A0A);
            if (A034 < A033) {
                r10.A03 = A034;
            }
            double A035 = d - r12.A03(this.A08);
            if (A035 < A03) {
                r10.A01 = A035;
            }
            double A036 = d + r12.A03(this.A09);
            if (A032 < A036) {
                r10.A02 = A036;
            }
        }
        float[] fArr = super.A0C;
        C05050Ob r4 = super.A0B;
        r12.A07(r4);
        if (r10.A00 >= r4.A03 && r10.A03 <= r4.A00) {
            fArr[0] = (float) ((int) Math.ceil(r4.A01 - r10.A02));
            float floor = (float) ((int) Math.floor(r4.A02 - r10.A01));
            fArr[1] = floor;
            float f = fArr[0];
            if (f <= floor) {
                r12.A08(fArr, d + ((double) f), d2);
                this.A0D = fArr[0];
                this.A0E = fArr[1];
                return true;
            }
        }
        return false;
    }
}
