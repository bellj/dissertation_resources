package X;

import android.os.Build;
import com.whatsapp.util.Log;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* renamed from: X.12A  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass12A implements AnonymousClass12B {
    public static volatile AnonymousClass12A A02;
    public String A00 = null;
    public Method A01 = null;

    public static AnonymousClass12A A00() {
        if (A02 == null) {
            synchronized (AnonymousClass12B.class) {
                if (A02 == null) {
                    A02 = new AnonymousClass12A();
                }
            }
        }
        return A02;
    }

    public synchronized void A01(String str, boolean z) {
        Method method = null;
        if (Build.VERSION.SDK_INT < 23 && z) {
            try {
                Method declaredMethod = Runtime.class.getDeclaredMethod("nativeLoad", String.class, ClassLoader.class, String.class);
                declaredMethod.setAccessible(true);
                method = declaredMethod;
            } catch (NoSuchMethodException | SecurityException e) {
                Log.w("robustsofileloader/get-native-load-runtime-method: Could not get nativeLoad method", e);
            }
        }
        this.A01 = method;
        this.A00 = str;
    }

    @Override // X.AnonymousClass12B
    public synchronized void AKV(String str, int i) {
        Method method = this.A01;
        if (method == null) {
            System.load(str);
        } else {
            try {
                boolean z = false;
                if (this.A00 != null) {
                    z = true;
                }
                AnonymousClass009.A0F(z);
                String str2 = (String) method.invoke(Runtime.getRuntime(), str, AnonymousClass12A.class.getClassLoader(), this.A00);
                if (str2 != null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("robustsofileloader/load: nativeLoad returned error ");
                    sb.append(str2);
                    throw new UnsatisfiedLinkError(sb.toString());
                }
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("robustsofileloader/load: Cannot load ");
                sb2.append(str);
                String obj = sb2.toString();
                StringBuilder sb3 = new StringBuilder();
                sb3.append("robustsofileloader/load: Error when loading lib: ");
                sb3.append(obj);
                Log.e(sb3.toString(), e);
                throw new UnsatisfiedLinkError(obj);
            }
        }
    }
}
