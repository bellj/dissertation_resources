package X;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/* renamed from: X.0ZE  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0ZE implements AbstractC12920im {
    public static final String[] A01 = new String[0];
    public final SQLiteDatabase A00;

    public AnonymousClass0ZE(SQLiteDatabase sQLiteDatabase) {
        this.A00 = sQLiteDatabase;
    }

    @Override // X.AbstractC12920im
    public Cursor AZi(AbstractC12390hq r6) {
        return this.A00.rawQueryWithFactory(new AnonymousClass0VB(r6, this), r6.AGr(), A01, null);
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.A00.close();
    }
}
