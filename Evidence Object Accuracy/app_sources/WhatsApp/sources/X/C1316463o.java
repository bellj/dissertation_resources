package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.63o  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1316463o implements Parcelable {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(2);
    public int A00;
    public long A01;
    public String A02;
    public final String A03;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C1316463o(String str, String str2, int i, long j) {
        if (!TextUtils.isEmpty(str)) {
            this.A03 = str;
            this.A01 = j;
            this.A00 = i;
            this.A02 = str2;
            return;
        }
        throw C12970iu.A0f("Claim id cannot be empty");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0051, code lost:
        if (r0 == false) goto L_0x002e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C1316463o A00(X.AnonymousClass1V8 r8) {
        /*
            if (r8 == 0) goto L_0x0054
            java.lang.String r0 = "id"
            java.lang.String r3 = r8.A0H(r0)
            r1 = 0
            java.lang.String r0 = "ts"
            long r6 = r8.A08(r0, r1)
            r0 = 1000(0x3e8, double:4.94E-321)
            long r6 = r6 * r0
            r5 = 0
            java.lang.String r4 = ""
            X.63o r2 = new X.63o
            r2.<init>(r3, r4, r5, r6)
            java.lang.String r0 = "status"
            java.lang.String r1 = r8.A0H(r0)
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x002e
            int r0 = r1.hashCode()
            switch(r0) {
                case 527514546: goto L_0x004a;
                case 1350822958: goto L_0x0042;
                case 1967871671: goto L_0x003a;
                default: goto L_0x002e;
            }
        L_0x002e:
            r1 = 0
        L_0x002f:
            r2.A00 = r1
            java.lang.String r0 = "reason"
            java.lang.String r0 = r8.A0H(r0)
            r2.A02 = r0
            return r2
        L_0x003a:
            java.lang.String r0 = "APPROVED"
            boolean r0 = r1.equals(r0)
            r1 = 2
            goto L_0x0051
        L_0x0042:
            java.lang.String r0 = "DECLINED"
            boolean r0 = r1.equals(r0)
            r1 = 3
            goto L_0x0051
        L_0x004a:
            java.lang.String r0 = "IN_REVIEW"
            boolean r0 = r1.equals(r0)
            r1 = 1
        L_0x0051:
            if (r0 != 0) goto L_0x002f
            goto L_0x002e
        L_0x0054:
            r2 = 0
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C1316463o.A00(X.1V8):X.63o");
    }

    public static C1316463o A01(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            JSONObject A05 = C13000ix.A05(str);
            String string = A05.getString("id");
            long optLong = A05.optLong("ts", 0);
            return new C1316463o(string, A05.getString("reason"), A05.optInt("status", 0), optLong);
        } catch (JSONException unused) {
            Log.w("PAY: Claim/fromJsonString threw exception");
            return null;
        }
    }

    public JSONObject A02() {
        JSONObject A0a = C117295Zj.A0a();
        try {
            A0a.put("id", this.A03);
            A0a.put("ts", this.A01);
            A0a.put("status", this.A00);
            A0a.put("reason", this.A02);
            return A0a;
        } catch (JSONException unused) {
            Log.w("PAY: Claim/toJson threw exception");
            return A0a;
        }
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A03);
        parcel.writeLong(this.A01);
        parcel.writeInt(this.A00);
        parcel.writeString(this.A02);
    }
}
