package X;

import android.content.Context;
import android.text.TextUtils;
import android.view.animation.Interpolator;
import com.whatsapp.R;

/* renamed from: X.3J7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3J7 {
    public static final Interpolator A00 = AnonymousClass0L1.A00(0.85f, 0.0f, 0.15f, 1.0f);
    public static final Interpolator A01 = AnonymousClass0L1.A00(0.83f, 0.0f, 0.17f, 1.0f);
    public static final C15310mu A02;
    public static final C37471mS[] A03;

    static {
        C37471mS[] r3 = {new C37471mS("🏳‍⚧"), new C37471mS("🏳️‍⚧️"), new C37471mS("🏴󠁵󠁳󠁴󠁸󠁿"), new C37471mS("🇽🇹"), new C37471mS("🏳‍🟧‍⬛‍🟧")};
        A03 = r3;
        A02 = new C15300mt(r3, false, false);
    }

    public static int A00(C40481rf r2, String str) {
        C40491rg r0;
        if (r2 != null) {
            synchronized (r2) {
                r0 = (C40491rg) r2.A01.get(str);
            }
            if (r0 != null) {
                return r0.A04.size();
            }
        }
        return 0;
    }

    public static AnonymousClass2VH A01(AnonymousClass2VH r11) {
        String str = r11.A05;
        if (TextUtils.isEmpty(str)) {
            return r11;
        }
        AnonymousClass009.A05(str);
        if (!C38491oB.A02(str)) {
            return new AnonymousClass2VH(r11.A03, r11.A04, "□", r11.A01, r11.A02, r11.A00);
        }
        return r11;
    }

    public static String A02(Context context, AnonymousClass018 r2, int i) {
        if (i > 999) {
            return context.getString(R.string.max_reactions_count);
        }
        return r2.A0J().format((long) i);
    }
}
