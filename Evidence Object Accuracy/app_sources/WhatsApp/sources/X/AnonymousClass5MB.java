package X;

import java.io.ByteArrayOutputStream;

/* renamed from: X.5MB  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5MB extends AnonymousClass5NC {
    public AnonymousClass5MB(C94954co r1) {
        super(r1);
    }

    @Override // X.AnonymousClass1TL
    public int A05() {
        return A01().length;
    }

    @Override // X.AnonymousClass1TL
    public void A08(AnonymousClass1TP r7, boolean z) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        AnonymousClass5NC.A01(byteArrayOutputStream, "DL", this.A02);
        AnonymousClass5NC.A01(byteArrayOutputStream, "DL", this.A01);
        AnonymousClass5NC.A01(byteArrayOutputStream, "DL", this.A03);
        byteArrayOutputStream.write(new C114825Nf(this.A04, this.A00, true).A02("DL"));
        r7.A05(byteArrayOutputStream.toByteArray(), 32, 8, z);
    }
}
