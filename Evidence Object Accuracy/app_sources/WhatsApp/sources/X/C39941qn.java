package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.google.protobuf.CodedOutputStream;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.io.IOException;
import org.chromium.net.UrlRequest;

/* renamed from: X.1qn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C39941qn extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C39941qn A0E;
    public static volatile AnonymousClass255 A0F;
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public long A04;
    public long A05;
    public long A06;
    public AnonymousClass2n0 A07;
    public AnonymousClass2n0 A08;
    public AnonymousClass1G8 A09;
    public String A0A = "";
    public String A0B = "";
    public boolean A0C;
    public boolean A0D;

    static {
        C39941qn r0 = new C39941qn();
        A0E = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r16, Object obj, Object obj2) {
        C82393vY r2;
        C82393vY r22;
        AnonymousClass1G9 r23;
        switch (r16.ordinal()) {
            case 0:
                return A0E;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                C39941qn r0 = (C39941qn) obj2;
                int i = this.A00;
                boolean z = true;
                if ((i & 1) != 1) {
                    z = false;
                }
                int i2 = this.A01;
                int i3 = r0.A00;
                boolean z2 = true;
                if ((i3 & 1) != 1) {
                    z2 = false;
                }
                this.A01 = r8.Afp(i2, r0.A01, z, z2);
                boolean z3 = false;
                if ((i & 2) == 2) {
                    z3 = true;
                }
                long j = this.A04;
                boolean z4 = false;
                if ((i3 & 2) == 2) {
                    z4 = true;
                }
                this.A04 = r8.Afs(j, r0.A04, z3, z4);
                boolean z5 = false;
                if ((i & 4) == 4) {
                    z5 = true;
                }
                String str = this.A0B;
                boolean z6 = false;
                if ((i3 & 4) == 4) {
                    z6 = true;
                }
                this.A0B = r8.Afy(str, r0.A0B, z5, z6);
                boolean z7 = false;
                if ((i & 8) == 8) {
                    z7 = true;
                }
                int i4 = this.A02;
                boolean z8 = false;
                if ((i3 & 8) == 8) {
                    z8 = true;
                }
                this.A02 = r8.Afp(i4, r0.A02, z7, z8);
                boolean z9 = false;
                if ((i & 16) == 16) {
                    z9 = true;
                }
                long j2 = this.A06;
                boolean z10 = false;
                if ((i3 & 16) == 16) {
                    z10 = true;
                }
                this.A06 = r8.Afs(j2, r0.A06, z9, z10);
                this.A09 = (AnonymousClass1G8) r8.Aft(this.A09, r0.A09);
                int i5 = this.A00;
                boolean z11 = false;
                if ((i5 & 64) == 64) {
                    z11 = true;
                }
                long j3 = this.A05;
                int i6 = r0.A00;
                boolean z12 = false;
                if ((i6 & 64) == 64) {
                    z12 = true;
                }
                this.A05 = r8.Afs(j3, r0.A05, z11, z12);
                boolean z13 = false;
                if ((i5 & 128) == 128) {
                    z13 = true;
                }
                boolean z14 = this.A0C;
                boolean z15 = false;
                if ((i6 & 128) == 128) {
                    z15 = true;
                }
                this.A0C = r8.Afl(z13, z14, z15, r0.A0C);
                boolean z16 = false;
                if ((i5 & 256) == 256) {
                    z16 = true;
                }
                String str2 = this.A0A;
                boolean z17 = false;
                if ((i6 & 256) == 256) {
                    z17 = true;
                }
                this.A0A = r8.Afy(str2, r0.A0A, z16, z17);
                boolean z18 = false;
                if ((i5 & 512) == 512) {
                    z18 = true;
                }
                int i7 = this.A03;
                boolean z19 = false;
                if ((i6 & 512) == 512) {
                    z19 = true;
                }
                this.A03 = r8.Afp(i7, r0.A03, z18, z19);
                boolean z20 = false;
                if ((i5 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
                    z20 = true;
                }
                boolean z21 = this.A0D;
                boolean z22 = false;
                if ((i6 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
                    z22 = true;
                }
                this.A0D = r8.Afl(z20, z21, z22, r0.A0D);
                this.A08 = (AnonymousClass2n0) r8.Aft(this.A08, r0.A08);
                this.A07 = (AnonymousClass2n0) r8.Aft(this.A07, r0.A07);
                if (r8 == C463025i.A00) {
                    this.A00 |= r0.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                AnonymousClass254 r02 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        int A03 = r82.A03();
                        switch (A03) {
                            case 0:
                                break;
                            case 8:
                                int A02 = r82.A02();
                                if (A02 != 0 && A02 != 1) {
                                    super.A0X(1, A02);
                                    break;
                                } else {
                                    this.A00 |= 1;
                                    this.A01 = A02;
                                    break;
                                }
                                break;
                            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                                this.A00 |= 2;
                                this.A04 = r82.A06();
                                break;
                            case 26:
                                String A0A = r82.A0A();
                                this.A00 = 4 | this.A00;
                                this.A0B = A0A;
                                break;
                            case 32:
                                int A022 = r82.A02();
                                switch (A022) {
                                    case 0:
                                    case 1:
                                    case 2:
                                    case 3:
                                    case 4:
                                    case 5:
                                    case 6:
                                    case 7:
                                    case 8:
                                    case 9:
                                    case 10:
                                    case 11:
                                        this.A00 |= 8;
                                        this.A02 = A022;
                                        continue;
                                    default:
                                        super.A0X(4, A022);
                                        continue;
                                }
                            case 40:
                                this.A00 |= 16;
                                this.A06 = r82.A06();
                                break;
                            case SearchActionVerificationClientService.TIME_TO_SLEEP_IN_MS /* 50 */:
                                if ((this.A00 & 32) == 32) {
                                    r23 = (AnonymousClass1G9) this.A09.A0T();
                                } else {
                                    r23 = null;
                                }
                                AnonymousClass1G8 r1 = (AnonymousClass1G8) r82.A09(r02, AnonymousClass1G8.A05.A0U());
                                this.A09 = r1;
                                if (r23 != null) {
                                    r23.A04(r1);
                                    this.A09 = (AnonymousClass1G8) r23.A01();
                                }
                                this.A00 |= 32;
                                break;
                            case 56:
                                this.A00 |= 64;
                                this.A05 = r82.A06();
                                break;
                            case 64:
                                this.A00 |= 128;
                                this.A0C = r82.A0F();
                                break;
                            case 74:
                                String A0A2 = r82.A0A();
                                this.A00 |= 256;
                                this.A0A = A0A2;
                                break;
                            case 80:
                                int A023 = r82.A02();
                                switch (A023) {
                                    case 0:
                                    case 1:
                                    case 2:
                                    case 3:
                                    case 4:
                                    case 5:
                                    case 6:
                                    case 7:
                                    case 8:
                                    case 9:
                                    case 10:
                                    case 11:
                                    case 12:
                                    case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                                    case UrlRequest.Status.READING_RESPONSE /* 14 */:
                                    case 15:
                                    case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                                    case 17:
                                    case 18:
                                    case 19:
                                    case C43951xu.A01 /* 20 */:
                                    case 21:
                                    case 22:
                                    case 23:
                                    case 24:
                                    case 25:
                                    case 26:
                                    case 27:
                                    case 28:
                                    case 29:
                                    case C25991Bp.A0S:
                                    case 31:
                                        this.A00 |= 512;
                                        this.A03 = A023;
                                        continue;
                                    default:
                                        super.A0X(10, A023);
                                        continue;
                                }
                            case 88:
                                this.A00 |= EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
                                this.A0D = r82.A0F();
                                break;
                            case 98:
                                if ((this.A00 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
                                    r22 = (C82393vY) this.A08.A0T();
                                } else {
                                    r22 = null;
                                }
                                AnonymousClass2n0 r12 = (AnonymousClass2n0) r82.A09(r02, AnonymousClass2n0.A04.A0U());
                                this.A08 = r12;
                                if (r22 != null) {
                                    r22.A04(r12);
                                    this.A08 = (AnonymousClass2n0) r22.A01();
                                }
                                this.A00 |= EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
                                break;
                            case 106:
                                if ((this.A00 & 4096) == 4096) {
                                    r2 = (C82393vY) this.A07.A0T();
                                } else {
                                    r2 = null;
                                }
                                AnonymousClass2n0 r13 = (AnonymousClass2n0) r82.A09(r02, AnonymousClass2n0.A04.A0U());
                                this.A07 = r13;
                                if (r2 != null) {
                                    r2.A04(r13);
                                    this.A07 = (AnonymousClass2n0) r2.A01();
                                }
                                this.A00 |= 4096;
                                break;
                            default:
                                if (A0a(r82, A03)) {
                                    break;
                                } else {
                                    break;
                                }
                        }
                    } catch (C28971Pt e) {
                        e.unfinishedMessage = this;
                        throw new RuntimeException(e);
                    } catch (IOException e2) {
                        C28971Pt r14 = new C28971Pt(e2.getMessage());
                        r14.unfinishedMessage = this;
                        throw new RuntimeException(r14);
                    }
                }
                break;
            case 3:
                return null;
            case 4:
                return new C39941qn();
            case 5:
                return new AnonymousClass279();
            case 6:
                break;
            case 7:
                if (A0F == null) {
                    synchronized (C39941qn.class) {
                        if (A0F == null) {
                            A0F = new AnonymousClass255(A0E);
                        }
                    }
                }
                return A0F;
            default:
                throw new UnsupportedOperationException();
        }
        return A0E;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A02(1, this.A01);
        }
        if ((i3 & 2) == 2) {
            i2 += CodedOutputStream.A06(2, this.A04);
        }
        if ((i3 & 4) == 4) {
            i2 += CodedOutputStream.A07(3, this.A0B);
        }
        int i4 = this.A00;
        if ((i4 & 8) == 8) {
            i2 += CodedOutputStream.A02(4, this.A02);
        }
        if ((i4 & 16) == 16) {
            i2 += CodedOutputStream.A06(5, this.A06);
        }
        if ((i4 & 32) == 32) {
            AnonymousClass1G8 r0 = this.A09;
            if (r0 == null) {
                r0 = AnonymousClass1G8.A05;
            }
            i2 += CodedOutputStream.A0A(r0, 6);
        }
        int i5 = this.A00;
        if ((i5 & 64) == 64) {
            i2 += CodedOutputStream.A06(7, this.A05);
        }
        if ((i5 & 128) == 128) {
            i2 += CodedOutputStream.A00(8);
        }
        if ((i5 & 256) == 256) {
            i2 += CodedOutputStream.A07(9, this.A0A);
        }
        int i6 = this.A00;
        if ((i6 & 512) == 512) {
            i2 += CodedOutputStream.A02(10, this.A03);
        }
        if ((i6 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            i2 += CodedOutputStream.A00(11);
        }
        if ((i6 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            AnonymousClass2n0 r02 = this.A08;
            if (r02 == null) {
                r02 = AnonymousClass2n0.A04;
            }
            i2 += CodedOutputStream.A0A(r02, 12);
        }
        if ((this.A00 & 4096) == 4096) {
            AnonymousClass2n0 r03 = this.A07;
            if (r03 == null) {
                r03 = AnonymousClass2n0.A04;
            }
            i2 += CodedOutputStream.A0A(r03, 13);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0E(1, this.A01);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0H(2, this.A04);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0I(3, this.A0B);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0E(4, this.A02);
        }
        if ((this.A00 & 16) == 16) {
            codedOutputStream.A0H(5, this.A06);
        }
        if ((this.A00 & 32) == 32) {
            AnonymousClass1G8 r0 = this.A09;
            if (r0 == null) {
                r0 = AnonymousClass1G8.A05;
            }
            codedOutputStream.A0L(r0, 6);
        }
        if ((this.A00 & 64) == 64) {
            codedOutputStream.A0H(7, this.A05);
        }
        if ((this.A00 & 128) == 128) {
            codedOutputStream.A0J(8, this.A0C);
        }
        if ((this.A00 & 256) == 256) {
            codedOutputStream.A0I(9, this.A0A);
        }
        if ((this.A00 & 512) == 512) {
            codedOutputStream.A0E(10, this.A03);
        }
        if ((this.A00 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            codedOutputStream.A0J(11, this.A0D);
        }
        if ((this.A00 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            AnonymousClass2n0 r02 = this.A08;
            if (r02 == null) {
                r02 = AnonymousClass2n0.A04;
            }
            codedOutputStream.A0L(r02, 12);
        }
        if ((this.A00 & 4096) == 4096) {
            AnonymousClass2n0 r03 = this.A07;
            if (r03 == null) {
                r03 = AnonymousClass2n0.A04;
            }
            codedOutputStream.A0L(r03, 13);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
