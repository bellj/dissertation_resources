package X;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.appcompat.widget.Toolbar;
import com.facebook.redex.IDxAListenerShape1S0200000_3_I1;
import com.facebook.redex.IDxCListenerShape10S0100000_3_I1;
import com.facebook.redex.IDxCListenerShape11S0100000_3_I1;
import com.facebook.redex.IDxObserverShape5S0100000_3_I1;
import com.facebook.redex.IDxObserverShape5S0200000_3_I1;
import com.whatsapp.R;
import com.whatsapp.authentication.FingerprintBottomSheet;
import com.whatsapp.jid.UserJid;
import com.whatsapp.payments.ui.widget.PayToolbar;
import com.whatsapp.util.Log;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.AbstractCollection;
import java.util.AbstractList;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.npci.commonlibrary.widget.FormItemEditText;

/* renamed from: X.5Zk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C117305Zk {
    public static int A00(int i) {
        return i != 0 ? 2 : 0;
    }

    public static int A01(ActivityC13810kN r2, int i) {
        r2.setContentView(i);
        int A00 = AnonymousClass00T.A00(r2, R.color.fb_pay_hub_icon_tint);
        r2.A1e((Toolbar) r2.findViewById(R.id.pay_service_toolbar));
        return A00;
    }

    public static int A02(String str, int i, int i2) {
        return Integer.parseInt(str.substring(i, i2).trim());
    }

    public static Parcelable A03(Parcel parcel, ClassLoader classLoader) {
        Parcelable readParcelable = parcel.readParcelable(classLoader);
        AnonymousClass009.A05(readParcelable);
        return readParcelable;
    }

    public static View A04(Context context, LayoutInflater layoutInflater, ViewGroup viewGroup, int i) {
        View inflate = layoutInflater.inflate(i, viewGroup, false);
        inflate.setClickable(false);
        inflate.setBackgroundColor(AnonymousClass00T.A00(context, R.color.primary_surface));
        return inflate;
    }

    public static View A05(View view, AnonymousClass5Wu r2, int i, int i2) {
        C88094Eg.A00((ViewStub) AnonymousClass028.A0D(view, i), r2);
        return AnonymousClass028.A0D(view, i2);
    }

    public static ImageView A06(ActivityC000800j r0, int i) {
        return (ImageView) r0.findViewById(i);
    }

    public static LinearLayout A07(View view, int i) {
        return (LinearLayout) AnonymousClass028.A0D(view, i);
    }

    public static Toolbar A08(ActivityC000800j r1) {
        return (Toolbar) r1.findViewById(R.id.toolbar);
    }

    public static IDxAListenerShape1S0200000_3_I1 A09(Object obj, Object obj2, int i) {
        return new IDxAListenerShape1S0200000_3_I1(obj, i, obj2);
    }

    public static IDxCListenerShape11S0100000_3_I1 A0A(Object obj, int i) {
        return new IDxCListenerShape11S0100000_3_I1(obj, i);
    }

    public static IDxObserverShape5S0100000_3_I1 A0B(Object obj, int i) {
        return new IDxObserverShape5S0100000_3_I1(obj, i);
    }

    public static C14580lf A0C(C17070qD r0) {
        return r0.A00().A00();
    }

    public static FingerprintBottomSheet A0D() {
        return FingerprintBottomSheet.A00(R.string.payment_bio_bottom_sheet_title, R.string.cancel, R.string.use_payments_pin, R.layout.pay_header);
    }

    public static C30821Yy A0E(Object obj, String str) {
        return C30821Yy.A00(str, ((AbstractC30781Yu) obj).A01);
    }

    public static AnonymousClass1ZO A0F(UserJid userJid, C17070qD r2) {
        r2.A03();
        return r2.A09.A05(userJid);
    }

    public static AnonymousClass102 A0G(AnonymousClass01J r0) {
        return (AnonymousClass102) r0.AEL.get();
    }

    public static AbstractC28901Pl A0H(Iterator it) {
        return (AbstractC28901Pl) it.next();
    }

    public static AnonymousClass1ZR A0I(AnonymousClass2SN r1, Class cls, Object obj, String str) {
        return new AnonymousClass1ZR(r1, cls, obj, str);
    }

    public static AnonymousClass2SM A0J() {
        return new AnonymousClass2SM();
    }

    public static AbstractC38191ng A0K(C17070qD r0) {
        return r0.A02().AFJ();
    }

    public static C452120p A0L() {
        return new C452120p();
    }

    public static C18610sj A0M(AnonymousClass01J r0) {
        return (C18610sj) r0.AF0.get();
    }

    public static AnonymousClass61M A0N(AnonymousClass01J r0) {
        return (AnonymousClass61M) r0.AF1.get();
    }

    public static C22710zW A0O(AnonymousClass01J r0) {
        return (C22710zW) r0.AF7.get();
    }

    public static C17070qD A0P(AnonymousClass01J r0) {
        return (C17070qD) r0.AFC.get();
    }

    public static C1310460z A0Q(Collection collection) {
        return new C1310460z("account", new ArrayList(collection));
    }

    public static AnonymousClass6F2 A0R(AbstractC30791Yv r2, BigDecimal bigDecimal, int i) {
        return new AnonymousClass6F2(r2, new C30821Yy(bigDecimal, i));
    }

    public static AnonymousClass3FW A0S() {
        return new AnonymousClass3FW(null, new AnonymousClass3FW[0]);
    }

    public static AnonymousClass6BE A0T(AnonymousClass01J r0) {
        return (AnonymousClass6BE) r0.A9X.get();
    }

    public static AbstractC16870pt A0U(C17070qD r0) {
        return r0.A02().ACx();
    }

    public static C30931Zj A0V(String str, String str2) {
        return C30931Zj.A00(str, str2, "COMMON");
    }

    public static AnonymousClass60Y A0W(AnonymousClass01J r0) {
        return (AnonymousClass60Y) r0.ADK.get();
    }

    public static AnonymousClass61F A0X(AnonymousClass01J r0) {
        return (AnonymousClass61F) r0.AD9.get();
    }

    public static C126915te A0Y(C1316663q r2) {
        return (C126915te) r2.A05.get(r2.A00);
    }

    public static C74503iB A0Z(AbstractC001400p r2) {
        return (C74503iB) new AnonymousClass02A(r2).A00(C74503iB.class);
    }

    public static PayToolbar A0a(ActivityC13810kN r1) {
        return (PayToolbar) AnonymousClass028.A0D(r1.A00, R.id.pay_service_toolbar);
    }

    public static AnonymousClass1ZD A0b(AbstractC16390ow r0) {
        C16470p4 ABf = r0.ABf();
        AnonymousClass009.A05(ABf);
        AnonymousClass1ZD r02 = ABf.A01;
        AnonymousClass009.A05(r02);
        return r02;
    }

    public static AnonymousClass1V8 A0c(AnonymousClass1V8 r1) {
        return r1.A0E("account");
    }

    public static AnonymousClass1V8 A0d(Iterator it) {
        return (AnonymousClass1V8) it.next();
    }

    public static CharSequence A0e(Context context, AnonymousClass018 r2, AbstractC30791Yv r3, C30821Yy r4, int i) {
        return r3.AA7(context, r3.AAA(r2, r4, i));
    }

    public static Long A0f() {
        return 0L;
    }

    public static String A0g(C15570nT r0) {
        r0.A08();
        C27621Ig r02 = r0.A01;
        AnonymousClass009.A05(r02);
        return C248917h.A01(r02);
    }

    public static String A0h(C15570nT r1) {
        String A04 = r1.A04();
        AnonymousClass009.A05(A04);
        return C248917h.A00(C20920wX.A00(), A04);
    }

    public static String A0i(C15570nT r1, int i) {
        int length;
        r1.A08();
        String A02 = C248917h.A02((AbstractC14640lm) r1.A01.A0B(AbstractC14640lm.class));
        return (TextUtils.isEmpty(A02) || (length = A02.length()) <= i) ? A02 : A02.substring(length - i);
    }

    public static String A0j(C15570nT r1, C14830m7 r2) {
        byte[] A01 = AnonymousClass15O.A01(r1, r2, false);
        AnonymousClass009.A05(A01);
        return C003501n.A03(A01);
    }

    public static String A0k(AnonymousClass018 r2, AbstractC30791Yv r3, BigDecimal bigDecimal, int i) {
        return r3.AAB(r2, bigDecimal.setScale(i, RoundingMode.HALF_EVEN), 2);
    }

    public static String A0l(Object obj, String str, JSONObject jSONObject) {
        jSONObject.put(str, obj);
        return jSONObject.toString();
    }

    public static String A0m(Map.Entry entry) {
        return (String) entry.getValue();
    }

    public static String A0n(byte[] bArr) {
        return Base64.encodeToString(bArr, 2);
    }

    public static KeyPair A0o() {
        ECGenParameterSpec eCGenParameterSpec = new ECGenParameterSpec("secp256r1");
        KeyPairGenerator instance = KeyPairGenerator.getInstance("EC");
        instance.initialize(eCGenParameterSpec);
        return instance.generateKeyPair();
    }

    public static PublicKey A0p(byte[] bArr) {
        return KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(bArr));
    }

    public static List A0q(AnonymousClass1V8 r0, C41141sy r1, List list) {
        r1.A07(r0, list);
        return Arrays.asList(new String[0]);
    }

    public static List A0r(Collection collection) {
        return Collections.unmodifiableList(new ArrayList(collection));
    }

    public static C117795ag A0s(AbstractList abstractList, int i) {
        return (C117795ag) abstractList.get(i);
    }

    public static short A0t(Object obj, String str) {
        return !str.equals(obj) ? (short) -1 : 0;
    }

    public static short A0u(Object obj, String str) {
        return !str.equals(obj) ? (short) -1 : 1;
    }

    public static short A0v(Object obj, String str) {
        return !str.equals(obj) ? (short) -1 : 2;
    }

    public static short A0w(Object obj, String str) {
        return !str.equals(obj) ? (short) -1 : 3;
    }

    public static void A0x(Activity activity) {
        activity.setResult(-1);
        activity.finish();
    }

    public static void A0y(Context context, Intent intent) {
        C06380Tj.A00(context).A03(intent);
    }

    public static void A0z(Context context, AbstractC14640lm r3, long j) {
        Intent A0i = new C14960mK().A0i(context, r3);
        A0i.putExtra("extra_quoted_message_row_id", j);
        context.startActivity(A0i);
    }

    public static void A10(Intent intent, Intent intent2, String str) {
        intent2.putExtra(str, intent.getStringExtra(str));
    }

    public static void A11(Intent intent, AnonymousClass1IR r4, AbstractC14640lm r5) {
        C38211ni.A00(intent, new AnonymousClass1IS(r5, r4.A0L, r4.A0Q));
        intent.putExtra("extra_transaction_id", r4.A0K);
    }

    public static void A12(Intent intent, Object obj, Object obj2, HashMap hashMap) {
        hashMap.put(obj, obj2);
        Bundle bundle = new Bundle();
        bundle.putSerializable("screen_params", hashMap);
        intent.putExtras(bundle);
    }

    public static void A13(Resources resources, Drawable drawable, int i) {
        drawable.setColorFilter(resources.getColor(i), PorterDuff.Mode.SRC_ATOP);
    }

    public static void A14(LayoutInflater layoutInflater, LinearLayout linearLayout, int i) {
        layoutInflater.inflate(i, (ViewGroup) linearLayout, true);
        linearLayout.setOrientation(1);
    }

    public static void A15(View view, int i) {
        AnonymousClass028.A0D(view, i).setVisibility(8);
    }

    public static void A16(AbstractC005102i r1, int i) {
        r1.A0A(i);
        r1.A0M(true);
    }

    public static void A17(C004802e r1, CharSequence charSequence, Object obj, int i) {
        r1.A03(new IDxCListenerShape10S0100000_3_I1(obj, i), charSequence);
        r1.A05();
    }

    public static void A18(C004802e r1, Object obj, int i, int i2) {
        r1.setNegativeButton(i2, new IDxCListenerShape10S0100000_3_I1(obj, i));
    }

    public static void A19(AbstractC001200n r1, AnonymousClass017 r2, Object obj, int i) {
        r2.A05(r1, new IDxObserverShape5S0200000_3_I1(r1, i, obj));
    }

    public static void A1A(AnonymousClass017 r1, Object obj, int i) {
        r1.A0B(new AnonymousClass4OZ(i, obj));
    }

    public static void A1B(AnonymousClass017 r1, Object obj, Object obj2) {
        r1.A0A(new AnonymousClass01T(obj, obj2));
    }

    public static void A1C(AbstractC130685zo r1) {
        ((Number) r1.A03(AbstractC130685zo.A0d)).intValue();
        AnonymousClass616.A00();
    }

    public static void A1D(ActivityC13790kL r2, int i) {
        C36021jC.A00(r2, i);
        r2.A00.Ab9(r2, Uri.parse("https://faq.whatsapp.com/android/payments/how-to-change-or-set-up-new-upi-pin/?india=1"));
    }

    public static void A1E(AnonymousClass3FE r1) {
        r1.A00("on_failure_handled_natively");
    }

    public static void A1F(AnonymousClass3FE r1, Object obj, Object obj2, AbstractMap abstractMap) {
        abstractMap.put(obj, obj2);
        r1.A01("on_success", abstractMap);
    }

    public static void A1G(AnonymousClass3FE r1, Object obj, Object obj2, AbstractMap abstractMap) {
        abstractMap.put(obj, obj2);
        r1.A02("on_success", abstractMap);
    }

    public static void A1H(C18610sj r6, AbstractC21730xt r7, AnonymousClass1V8 r8) {
        r6.A0F(r7, r8, "set", 0);
    }

    public static void A1I(C18610sj r6, AbstractC21730xt r7, AnonymousClass1V8 r8) {
        r6.A0F(r7, r8, "get", 0);
    }

    public static void A1J(C18610sj r6, AbstractC21730xt r7, AnonymousClass1V8 r8) {
        r6.A0F(r7, r8, "set", C26061Bw.A0L);
    }

    public static void A1K(C1310460z r2, String str, ArrayList arrayList) {
        r2.A02.add(new C1310460z(str, arrayList));
    }

    public static void A1L(AnonymousClass17V r2) {
        r2.A02 = null;
        r2.A00 = 0;
    }

    public static void A1M(Object obj, Object obj2, Object[] objArr) {
        objArr[4] = obj;
        objArr[5] = obj2;
    }

    public static void A1N(String str, String str2) {
        Log.e(C30931Zj.A01(str, str2));
    }

    public static void A1O(String str, String str2, Object[] objArr) {
        objArr[0] = new AnonymousClass1W9(str, str2);
    }

    public static void A1P(String str, String str2, Object[] objArr) {
        objArr[1] = new AnonymousClass1W9(str, str2);
    }

    public static void A1Q(String str, String str2, Object[] objArr) {
        objArr[3] = new AnonymousClass1W9(str, str2);
    }

    public static void A1R(String str, AbstractCollection abstractCollection, ArrayList arrayList) {
        abstractCollection.add(new C1310460z(str, arrayList));
    }

    public static void A1S(FormItemEditText formItemEditText, int[] iArr, int[][] iArr2) {
        formItemEditText.A07 = new ColorStateList(iArr2, iArr);
        formItemEditText.A0N = new float[6];
        formItemEditText.A0L = new float[6];
    }

    public static boolean A1T(ActivityC13810kN r1) {
        return r1.A0C.A07(714);
    }

    public static boolean A1U(C14850m9 r1) {
        return r1.A07(842);
    }

    public static boolean A1V(Object obj, String str) {
        return str.equals(((AbstractC30781Yu) obj).A04);
    }

    public static boolean A1W(Object obj, String str, Map map) {
        return str.equals(map.get(obj));
    }

    public static boolean A1X(String str, long j, boolean z) {
        return AnonymousClass3JT.A0E(str, j, 10000, z);
    }

    public static boolean A1Y(String str, boolean z) {
        return AnonymousClass3JT.A0E(str, 1, 200, z);
    }

    public static boolean A1Z(List list, int i) {
        return list.contains(Integer.valueOf(i));
    }

    public static byte[] A1a(int i) {
        byte[] bArr = new byte[i];
        new SecureRandom().nextBytes(bArr);
        return bArr;
    }

    public static AnonymousClass1W9[] A1b(AbstractCollection abstractCollection) {
        return (AnonymousClass1W9[]) abstractCollection.toArray(new AnonymousClass1W9[0]);
    }
}
