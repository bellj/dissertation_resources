package X;

/* renamed from: X.0Hb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC03250Hb extends AbstractC03490Hz {
    @Override // X.AbstractC03490Hz, X.AbstractC12490i0
    public void A5f(AnonymousClass0OO r3) {
        if (r3 instanceof AbstractC12140hR) {
            ((AbstractC03490Hz) this).A01.add(r3);
            return;
        }
        StringBuilder sb = new StringBuilder("Text content elements cannot contain ");
        sb.append(r3);
        sb.append(" elements.");
        throw new C11160fq(sb.toString());
    }
}
