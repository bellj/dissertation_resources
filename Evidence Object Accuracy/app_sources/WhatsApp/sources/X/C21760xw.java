package X;

/* renamed from: X.0xw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21760xw {
    public C43791xb A00;
    public final AbstractC15710nm A01;
    public final C16590pI A02;
    public final C231410n A03;
    public final String A04 = "commerce.db";

    public C21760xw(AbstractC15710nm r2, C16590pI r3, C231410n r4) {
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = r4;
    }

    public synchronized C43791xb A00() {
        C43791xb r4;
        r4 = this.A00;
        if (r4 == null) {
            r4 = new C43791xb(this.A01, this.A02, this.A03, this.A04);
            this.A00 = r4;
        }
        return r4;
    }

    public synchronized void A01() {
        C43791xb r0 = this.A00;
        if (r0 != null) {
            r0.close();
            this.A00 = null;
        }
    }
}
