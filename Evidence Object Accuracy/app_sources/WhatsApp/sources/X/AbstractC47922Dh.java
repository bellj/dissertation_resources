package X;

import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.Jid;

/* renamed from: X.2Dh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public interface AbstractC47922Dh {
    boolean AJo();

    AnonymousClass1IS AKM(int i);

    DeviceJid AYy(int i);

    C32141bg AZx();

    Jid AaD();

    void AbL(C20670w8 v, int i);

    AnonymousClass1OT Ae6();

    int AeP();

    long Aeq(int i);

    int size();
}
