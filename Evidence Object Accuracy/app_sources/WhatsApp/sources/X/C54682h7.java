package X;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.2h7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54682h7 extends AbstractC018308n {
    public final int A00;
    public final int A01;
    public final Paint A02;
    public final C36911kq A03;

    public C54682h7(C36911kq r3, int i, int i2) {
        this.A03 = r3;
        this.A01 = i2;
        this.A00 = i;
        Paint A0F = C12990iw.A0F();
        this.A02 = A0F;
        A0F.setAntiAlias(true);
    }

    @Override // X.AbstractC018308n
    public void A00(Canvas canvas, C05480Ps r16, RecyclerView recyclerView) {
        int childCount = recyclerView.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = recyclerView.getChildAt(i);
            if (A03(childAt)) {
                int bottom = childAt.getBottom() + C12970iu.A0H(childAt).bottomMargin;
                int left = recyclerView.getLeft();
                int width = recyclerView.getWidth();
                Paint paint = this.A02;
                paint.setColor(this.A00);
                canvas.drawRect((float) left, (float) bottom, (float) width, (float) (this.A01 + bottom), paint);
            }
        }
    }

    @Override // X.AbstractC018308n
    public void A01(Rect rect, View view, C05480Ps r4, RecyclerView recyclerView) {
        if (A03(view)) {
            rect.bottom = this.A01;
        }
    }

    public final boolean A03(View view) {
        int i;
        if (view != null) {
            int A00 = RecyclerView.A00(view);
            C36911kq r1 = this.A03;
            if (A00 != -1) {
                C44091yC r2 = r1.A0q;
                if (Integer.valueOf(r2.A00(A00)) != null && (i = A00 + 1) > 0 && i < r2.size()) {
                    Integer valueOf = Integer.valueOf(r2.A00(i));
                    return (valueOf != null && valueOf.intValue() == 22) || valueOf.intValue() == 23;
                }
            }
        }
        return false;
    }
}
