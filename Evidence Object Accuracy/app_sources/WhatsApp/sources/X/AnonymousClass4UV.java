package X;

import com.facebook.redex.RunnableBRunnable0Shape14S0100000_I1;

/* renamed from: X.4UV  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass4UV {
    public void A00() {
        AnonymousClass3EX r0;
        if (this instanceof C84463zL) {
            r0 = ((AnonymousClass2fT) ((C84463zL) this).A00.A0X.getValue()).A04;
        } else if (this instanceof C84453zK) {
            r0 = ((C84453zK) this).A00.A0E.A03;
        } else if (this instanceof C84443zJ) {
            C68263Us r3 = ((C84443zJ) this).A00.A0a.A0D;
            r3.A0A.Ab2(new RunnableBRunnable0Shape14S0100000_I1(r3, 36));
            return;
        } else if (!(this instanceof C84433zI)) {
            r0 = ((C84423zH) this).A00.A0F.A0H;
        } else {
            r0 = ((C84433zI) this).A00.A0D.A06;
        }
        r0.A00();
    }
}
