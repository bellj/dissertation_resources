package X;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import com.facebook.redex.EmptyBaseRunnable0;
import com.facebook.redex.RunnableBRunnable0Shape10S0200000_I1;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.2jY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class RunnableC55852jY extends EmptyBaseRunnable0 implements Runnable {
    public final AppWidgetManager A00;
    public final Context A01;
    public final C14900mE A02;
    public final C22670zS A03;
    public final AnonymousClass018 A04;
    public final C19990v2 A05;
    public final C15680nj A06;
    public final AnonymousClass10Y A07;
    public final AtomicBoolean A08 = new AtomicBoolean();
    public final int[] A09;

    public RunnableC55852jY(AppWidgetManager appWidgetManager, Context context, C14900mE r4, C22670zS r5, AnonymousClass018 r6, C19990v2 r7, C15680nj r8, AnonymousClass10Y r9, int[] iArr) {
        this.A01 = context;
        this.A02 = r4;
        this.A05 = r7;
        this.A03 = r5;
        this.A04 = r6;
        this.A00 = appWidgetManager;
        this.A07 = r9;
        this.A06 = r8;
        this.A09 = iArr;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0020, code lost:
        if (r10 != 0) goto L_0x0028;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ void A00(X.RunnableC55852jY r11, java.util.ArrayList r12) {
        /*
            com.whatsapp.appwidget.WidgetProvider.A0A = r12
            int[] r4 = r11.A09
            int r3 = r4.length
            r2 = 0
        L_0x0006:
            if (r2 >= r3) goto L_0x0044
            r8 = r4[r2]
            android.appwidget.AppWidgetManager r1 = r11.A00
            android.os.Bundle r5 = r1.getAppWidgetOptions(r8)
            if (r5 == 0) goto L_0x0022
            java.lang.String r0 = "appWidgetMinWidth"
            int r9 = r5.getInt(r0)
            java.lang.String r0 = "appWidgetMinHeight"
            int r10 = r5.getInt(r0)
            if (r9 == 0) goto L_0x0022
            if (r10 != 0) goto L_0x0028
        L_0x0022:
            r9 = 2147483647(0x7fffffff, float:NaN)
            r10 = 2147483647(0x7fffffff, float:NaN)
        L_0x0028:
            android.content.Context r5 = r11.A01
            X.0zS r6 = r11.A03
            X.018 r7 = r11.A04
            android.widget.RemoteViews r0 = com.whatsapp.appwidget.WidgetProvider.A00(r5, r6, r7, r8, r9, r10)
            r1.updateAppWidget(r8, r0)
            r0 = 100
            if (r9 <= r0) goto L_0x0041
            if (r10 <= r0) goto L_0x0041
            r0 = 2131364168(0x7f0a0948, float:1.8348165E38)
            r1.notifyAppWidgetViewDataChanged(r8, r0)
        L_0x0041:
            int r2 = r2 + 1
            goto L_0x0006
        L_0x0044:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.RunnableC55852jY.A00(X.2jY, java.util.ArrayList):void");
    }

    @Override // java.lang.Runnable
    public void run() {
        AtomicBoolean atomicBoolean = this.A08;
        if (!atomicBoolean.get()) {
            List A07 = this.A06.A07();
            ArrayList A0l = C12960it.A0l();
            Iterator it = A07.iterator();
            while (it.hasNext()) {
                AbstractC14640lm A0b = C12990iw.A0b(it);
                if (!atomicBoolean.get()) {
                    int A00 = this.A05.A00(A0b);
                    if (A00 > 0) {
                        A0l.addAll(this.A07.A05(A0b, Math.min(A00, 100)));
                    }
                } else {
                    return;
                }
            }
            Collections.sort(A0l, new AnonymousClass5CQ());
            this.A02.A0H(new RunnableBRunnable0Shape10S0200000_I1(this, 36, A0l));
        }
    }
}
