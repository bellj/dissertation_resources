package X;

import java.util.HashMap;
import java.util.List;

/* renamed from: X.5vw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C128335vw {
    public final int A00;
    public final long A01;
    public final AnonymousClass018 A02;
    public final C15370n3 A03;
    public final C30821Yy A04;
    public final AbstractC14640lm A05;
    public final AbstractC136536Mx A06;
    public final AbstractC16390ow A07;
    public final String A08;
    public final String A09;
    public final String A0A;
    public final String A0B;
    public final String A0C;
    public final String A0D;
    public final String A0E;
    public final String A0F;
    public final HashMap A0G;
    public final List A0H;
    public final boolean A0I;
    public final boolean A0J;
    public final boolean A0K;
    public final boolean A0L;
    public final boolean A0M;
    public final boolean A0N;

    public C128335vw(AnonymousClass018 r3, C15370n3 r4, C30821Yy r5, AbstractC14640lm r6, AbstractC136536Mx r7, AbstractC16390ow r8, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, HashMap hashMap, List list, int i, long j, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6) {
        this.A04 = r5;
        this.A06 = r7;
        this.A0E = str3;
        this.A0B = str4;
        this.A05 = r6;
        this.A00 = i;
        this.A01 = j;
        this.A0F = str5;
        this.A02 = r3;
        this.A09 = str6;
        this.A0D = str;
        this.A03 = r4;
        this.A08 = str2;
        this.A0C = str7;
        this.A07 = r8;
        this.A0J = z;
        this.A0I = z2;
        this.A0H = list;
        this.A0K = z3;
        this.A0M = z4;
        this.A0N = z5;
        this.A0A = str8;
        this.A0L = z6;
        this.A0G = hashMap;
    }
}
