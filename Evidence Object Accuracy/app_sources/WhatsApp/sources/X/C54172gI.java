package X;

import android.view.ViewGroup;
import com.whatsapp.R;
import java.util.List;

/* renamed from: X.2gI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C54172gI extends AnonymousClass02M {
    public final List A00;

    public C54172gI(List list) {
        this.A00 = list;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A00.size();
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r1, int i) {
    }

    @Override // X.AnonymousClass02M
    public AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        C16700pc.A0E(viewGroup, 0);
        return new C75163jP(C16700pc.A01(C12960it.A0E(viewGroup), viewGroup, R.layout.name_view_holder));
    }
}
