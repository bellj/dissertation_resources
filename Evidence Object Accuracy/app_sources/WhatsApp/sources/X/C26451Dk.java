package X;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import com.whatsapp.R;
import com.whatsapp.migration.android.view.GoogleMigrateImporterActivity;
import com.whatsapp.registration.RegisterName;
import com.whatsapp.util.Log;

/* renamed from: X.1Dk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26451Dk {
    public final C16590pI A00;
    public final C18360sK A01;
    public final AnonymousClass018 A02;

    public C26451Dk(C16590pI r1, C18360sK r2, AnonymousClass018 r3) {
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
    }

    public final C005602s A00(boolean z) {
        Class cls;
        Context context = this.A00.A00;
        if (z) {
            cls = RegisterName.class;
        } else {
            cls = GoogleMigrateImporterActivity.class;
        }
        Intent intent = new Intent(context, cls);
        if (!z) {
            intent.setAction(GoogleMigrateImporterActivity.A0F);
        }
        C005602s A00 = C22630zO.A00(context);
        A00.A0J = "other_notifications@1";
        int i = Build.VERSION.SDK_INT;
        int i2 = -2;
        if (i >= 26) {
            i2 = -1;
        }
        A00.A03 = i2;
        A00.A09 = AnonymousClass1UY.A00(context, 0, intent, 134217728);
        C18360sK.A01(A00, R.drawable.notifybar);
        if (i >= 21) {
            A00.A06 = 1;
        }
        return A00;
    }

    public void A01(int i) {
        Context context = this.A00.A00;
        String string = context.getResources().getString(R.string.google_migrate_notification_importing);
        if (i >= 0) {
            StringBuilder sb = new StringBuilder("GoogleMigrateNotificationManager/onProgress (");
            sb.append(i);
            sb.append("%)");
            Log.i(sb.toString());
            A02(string, context.getResources().getString(R.string.google_migrate_notification_import_percentage, this.A02.A0K().format(((double) i) / 100.0d)), i, false, false);
        }
    }

    public final void A02(String str, String str2, int i, boolean z, boolean z2) {
        boolean z3 = false;
        if (i == -1) {
            z3 = true;
        }
        C005602s A00 = A00(z2);
        int i2 = 100;
        if (z3) {
            i2 = 0;
            i = 0;
        }
        A00.A03(i2, i, false);
        A00.A0D(z);
        A00.A0E(z3);
        A00.A0A(str);
        A00.A09(str2);
        this.A01.A03(31, A00.A01());
    }
}
