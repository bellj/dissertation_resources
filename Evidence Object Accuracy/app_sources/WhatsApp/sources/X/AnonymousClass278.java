package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.278  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass278 extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass278 A05;
    public static volatile AnonymousClass255 A06;
    public int A00;
    public int A01;
    public long A02;
    public AnonymousClass1G8 A03;
    public String A04 = "";

    static {
        AnonymousClass278 r0 = new AnonymousClass278();
        A05 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r16, Object obj, Object obj2) {
        AnonymousClass1G9 r1;
        switch (r16.ordinal()) {
            case 0:
                return A05;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                AnonymousClass278 r2 = (AnonymousClass278) obj2;
                int i = this.A00;
                boolean z = true;
                if ((i & 1) != 1) {
                    z = false;
                }
                int i2 = this.A01;
                int i3 = r2.A00;
                boolean z2 = true;
                if ((i3 & 1) != 1) {
                    z2 = false;
                }
                this.A01 = r8.Afp(i2, r2.A01, z, z2);
                boolean z3 = false;
                if ((i & 2) == 2) {
                    z3 = true;
                }
                long j = this.A02;
                boolean z4 = false;
                if ((i3 & 2) == 2) {
                    z4 = true;
                }
                this.A02 = r8.Afs(j, r2.A02, z3, z4);
                this.A03 = (AnonymousClass1G8) r8.Aft(this.A03, r2.A03);
                int i4 = this.A00;
                boolean z5 = false;
                if ((i4 & 8) == 8) {
                    z5 = true;
                }
                String str = this.A04;
                int i5 = r2.A00;
                boolean z6 = false;
                if ((i5 & 8) == 8) {
                    z6 = true;
                }
                this.A04 = r8.Afy(str, r2.A04, z5, z6);
                if (r8 == C463025i.A00) {
                    this.A00 = i4 | i5;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                AnonymousClass254 r22 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        try {
                            int A03 = r82.A03();
                            if (A03 == 0) {
                                break;
                            } else if (A03 == 8) {
                                int A02 = r82.A02();
                                if (AnonymousClass4BS.A00(A02) == null) {
                                    super.A0X(1, A02);
                                } else {
                                    this.A00 = 1 | this.A00;
                                    this.A01 = A02;
                                }
                            } else if (A03 == 16) {
                                this.A00 |= 2;
                                this.A02 = r82.A06();
                            } else if (A03 == 26) {
                                if ((this.A00 & 4) == 4) {
                                    r1 = (AnonymousClass1G9) this.A03.A0T();
                                } else {
                                    r1 = null;
                                }
                                AnonymousClass1G8 r0 = (AnonymousClass1G8) r82.A09(r22, AnonymousClass1G8.A05.A0U());
                                this.A03 = r0;
                                if (r1 != null) {
                                    r1.A04(r0);
                                    this.A03 = (AnonymousClass1G8) r1.A01();
                                }
                                this.A00 |= 4;
                            } else if (A03 == 34) {
                                String A0A = r82.A0A();
                                this.A00 |= 8;
                                this.A04 = A0A;
                            } else if (!A0a(r82, A03)) {
                                break;
                            }
                        } catch (C28971Pt e) {
                            e.unfinishedMessage = this;
                            throw new RuntimeException(e);
                        }
                    } catch (IOException e2) {
                        C28971Pt r12 = new C28971Pt(e2.getMessage());
                        r12.unfinishedMessage = this;
                        throw new RuntimeException(r12);
                    }
                }
            case 3:
                return null;
            case 4:
                return new AnonymousClass278();
            case 5:
                return new AnonymousClass27A();
            case 6:
                break;
            case 7:
                if (A06 == null) {
                    synchronized (AnonymousClass278.class) {
                        if (A06 == null) {
                            A06 = new AnonymousClass255(A05);
                        }
                    }
                }
                return A06;
            default:
                throw new UnsupportedOperationException();
        }
        return A05;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A02(1, this.A01);
        }
        if ((i3 & 2) == 2) {
            i2 += CodedOutputStream.A05(2, this.A02);
        }
        if ((i3 & 4) == 4) {
            AnonymousClass1G8 r0 = this.A03;
            if (r0 == null) {
                r0 = AnonymousClass1G8.A05;
            }
            i2 += CodedOutputStream.A0A(r0, 3);
        }
        if ((this.A00 & 8) == 8) {
            i2 += CodedOutputStream.A07(4, this.A04);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0E(1, this.A01);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0H(2, this.A02);
        }
        if ((this.A00 & 4) == 4) {
            AnonymousClass1G8 r0 = this.A03;
            if (r0 == null) {
                r0 = AnonymousClass1G8.A05;
            }
            codedOutputStream.A0L(r0, 3);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0I(4, this.A04);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
