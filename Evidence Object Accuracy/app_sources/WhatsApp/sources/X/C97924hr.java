package X;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import com.whatsapp.notification.PopupNotification;
import com.whatsapp.util.Log;

/* renamed from: X.4hr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C97924hr implements SensorEventListener {
    public final /* synthetic */ PopupNotification A00;

    @Override // android.hardware.SensorEventListener
    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    public C97924hr(PopupNotification popupNotification) {
        this.A00 = popupNotification;
    }

    @Override // android.hardware.SensorEventListener
    public void onSensorChanged(SensorEvent sensorEvent) {
        PopupNotification popupNotification = this.A00;
        float f = sensorEvent.values[0];
        popupNotification.A00 = f;
        StringBuilder A0k = C12960it.A0k("popupnotification/proximity:");
        A0k.append(f);
        Log.i(A0k.toString());
    }
}
