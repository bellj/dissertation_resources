package X;

import android.content.res.Resources;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;

/* renamed from: X.1le  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC37191le extends AnonymousClass03U {
    public AbstractC37191le(View view) {
        super(view);
    }

    public static int A00(AnonymousClass03U r4) {
        View view = r4.A0H;
        Resources resources = view.getResources();
        int A07 = resources.getDisplayMetrics().widthPixels - (AnonymousClass028.A07(view) + AnonymousClass028.A06(view));
        return (int) Math.floor((double) (((float) A07) / ((float) resources.getDimensionPixelSize(R.dimen.popular_category_cell_width))));
    }

    public void A08() {
        RecyclerView recyclerView;
        if (this instanceof C59922vb) {
            recyclerView = ((C59922vb) this).A00;
        } else if (this instanceof C59912va) {
            recyclerView = ((C59912va) this).A00;
        } else if (this instanceof C59942vd) {
            recyclerView = ((C59942vd) this).A01;
        } else if (this instanceof C37181ld) {
            C37181ld r2 = (C37181ld) this;
            r2.A01.A00();
            C27131Gd r1 = r2.A00;
            if (r1 != null) {
                r2.A07.A04(r1);
            }
            r2.A06.A00();
            return;
        } else if (this instanceof C59952ve) {
            C48842Hz r12 = ((C59952ve) this).A0H;
            r12.A09 = null;
            r12.A00();
            return;
        } else {
            return;
        }
        recyclerView.setAdapter(null);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v21, resolved type: X.2uv */
    /* JADX DEBUG: Multi-variable search result rejected for r0v31, resolved type: X.2v4 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:145:0x0415  */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x042b  */
    /* JADX WARNING: Removed duplicated region for block: B:156:0x043a  */
    /* JADX WARNING: Removed duplicated region for block: B:161:0x0463  */
    /* JADX WARNING: Removed duplicated region for block: B:166:0x048d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A09(java.lang.Object r18) {
        /*
        // Method dump skipped, instructions count: 2024
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC37191le.A09(java.lang.Object):void");
    }
}
