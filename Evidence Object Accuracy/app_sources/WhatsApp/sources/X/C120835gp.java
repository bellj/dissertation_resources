package X;

import android.content.Context;

/* renamed from: X.5gp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120835gp extends C120895gv {
    public final /* synthetic */ C120535gL A00;
    public final /* synthetic */ AnonymousClass3CQ A01;
    public final /* synthetic */ String A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120835gp(Context context, C14900mE r8, C18650sn r9, C64513Fv r10, C120535gL r11, AnonymousClass3CQ r12, String str) {
        super(context, r8, r9, r10, "register-alias");
        this.A00 = r11;
        this.A02 = str;
        this.A01 = r12;
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A02(C452120p r3) {
        C120535gL.A00(r3, this.A00, this.A02);
        super.A02(r3);
        this.A01.A00(null, r3);
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A03(C452120p r3) {
        C120535gL.A00(r3, this.A00, this.A02);
        super.A03(r3);
        this.A01.A00(null, r3);
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A04(AnonymousClass1V8 r6) {
        AnonymousClass1V8 A0E;
        C120535gL r4 = this.A00;
        C120535gL.A00(null, r4, this.A02);
        super.A04(r6);
        AnonymousClass1V8 A0c = C117305Zk.A0c(r6);
        if (A0c != null && (A0E = A0c.A0E("alias")) != null) {
            AnonymousClass3CQ r2 = this.A01;
            try {
                r2.A00(C120895gv.A01(A0E), null);
            } catch (AnonymousClass1V9 unused) {
                r4.A04.A05("onRegisterVpaAlias/onResponseSuccess/corrupt stream exception");
                r2.A00(null, new C452120p(500));
            }
        }
    }
}
