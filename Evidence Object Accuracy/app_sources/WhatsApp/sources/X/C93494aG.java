package X;

import com.whatsapp.R;

/* renamed from: X.4aG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C93494aG {
    public final int A00;
    public final int A01;
    public final int A02;

    public C93494aG(int i, int i2, int i3) {
        this.A02 = i;
        this.A01 = i2;
        this.A00 = i3;
    }

    public static C93494aG A00(AbstractC15340mz r3, boolean z) {
        int i;
        int i2;
        int i3;
        if (C30041Vv.A0I(r3.A0y)) {
            i = R.string.message_opened_by;
            if (z) {
                i = R.string.message_opened;
            }
            i2 = R.drawable.msg_status_ephemeral_ring;
            i3 = R.color.view_once_viewed;
        } else {
            i = R.string.message_played_by;
            if (z) {
                i = R.string.message_played;
            }
            i2 = R.drawable.msg_status_mic;
            i3 = R.color.msgStatusReadTint;
        }
        return new C93494aG(i, i2, i3);
    }
}
