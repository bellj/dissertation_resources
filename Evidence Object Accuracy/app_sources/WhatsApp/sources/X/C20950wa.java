package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.Collection;
import java.util.Iterator;

/* renamed from: X.0wa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20950wa {
    public final C16510p9 A00;
    public final C20120vF A01;
    public final C16490p7 A02;
    public final C21390xL A03;

    public C20950wa(C16510p9 r1, C20120vF r2, C16490p7 r3, C21390xL r4) {
        this.A00 = r1;
        this.A03 = r4;
        this.A02 = r3;
        this.A01 = r2;
    }

    public static final void A00(AbstractC15340mz r8) {
        boolean z = false;
        boolean z2 = false;
        if (r8.A11 > 0) {
            z2 = true;
        }
        StringBuilder sb = new StringBuilder("ThumbnailMessageStore/isThumbnailV2Ready/message must have row_id set; key=");
        AnonymousClass1IS r2 = r8.A0z;
        sb.append(r2);
        AnonymousClass009.A0B(sb.toString(), z2);
        if (r8.A08() == 1) {
            z = true;
        }
        StringBuilder sb2 = new StringBuilder("ThumbnailMessageStore/isThumbnailV2Ready/message in main storage; key=");
        sb2.append(r2);
        AnonymousClass009.A0B(sb2.toString(), z);
    }

    public void A01(AbstractC15340mz r10) {
        String str;
        if (r10 instanceof C30061Vy) {
            AbstractC16130oV r102 = (AbstractC16130oV) r10;
            String str2 = r102.A05;
            if (!TextUtils.isEmpty(str2)) {
                C20120vF r3 = this.A01;
                boolean z = false;
                String[] strArr = {r102.A05, String.valueOf(r102.A11)};
                if (r3.A0A()) {
                    str = "SELECT COUNT(1) FROM message_media WHERE file_hash = ? AND message_row_id != ?";
                } else {
                    str = "SELECT COUNT(1) FROM messages WHERE media_hash = ? AND _id != ?";
                }
                C16310on A01 = r3.A02.get();
                try {
                    Cursor A09 = A01.A03.A09(str, strArr);
                    if (A09.moveToNext()) {
                        if (A09.getLong(0) == 0) {
                            z = true;
                        }
                        A09.close();
                        A01.close();
                        if (!z) {
                            return;
                        }
                    } else {
                        A09.close();
                        A01.close();
                    }
                    A01 = this.A02.A02();
                    try {
                        A01.A03.A01("media_hash_thumbnail", "media_hash = ?", new String[]{str2});
                    } finally {
                        try {
                            A01.close();
                        } catch (Throwable unused) {
                        }
                    }
                } finally {
                }
            }
        } else if (A08(r10.A11)) {
            A00(r10);
            long j = r10.A11;
            C16310on A02 = this.A02.A02();
            try {
                A02.A03.A01("message_thumbnail", "message_row_id = ?", new String[]{String.valueOf(j)});
                A02.close();
            } finally {
                try {
                    A02.close();
                } catch (Throwable unused2) {
                }
            }
        } else {
            A03(r10.A0z);
        }
    }

    public void A02(AbstractC15340mz r5, byte[] bArr) {
        if (r5 instanceof C30061Vy) {
            String str = ((AbstractC16130oV) r5).A05;
            if (!TextUtils.isEmpty(str) && A0A(str) == null) {
                C16310on A02 = this.A02.A02();
                try {
                    ContentValues contentValues = new ContentValues(2);
                    contentValues.put("media_hash", str);
                    contentValues.put("thumbnail", bArr);
                    A02.A03.A02(contentValues, "media_hash_thumbnail");
                    A02.close();
                } catch (Throwable th) {
                    try {
                        A02.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            }
        } else if (A08(r5.A11)) {
            A00(r5);
            if (bArr != null) {
                A07(bArr, r5.A11);
            }
        } else {
            A04(r5.A0z, bArr);
        }
    }

    public final void A03(AnonymousClass1IS r10) {
        try {
            C16310on A02 = this.A02.A02();
            AbstractC14640lm r0 = r10.A00;
            AnonymousClass009.A05(r0);
            String rawString = r0.getRawString();
            C16330op r8 = A02.A03;
            String[] strArr = new String[3];
            int i = 0;
            strArr[0] = rawString;
            if (r10.A02) {
                i = 1;
            }
            strArr[1] = String.valueOf(i);
            String str = r10.A01;
            strArr[2] = str;
            int A01 = r8.A01("message_thumbnails", "key_remote_jid = ? AND key_from_me = ? AND key_id = ?", strArr);
            StringBuilder sb = new StringBuilder();
            sb.append("thumbnailmsgstore/deleteMessageThumbnail/");
            sb.append(str);
            sb.append("/");
            sb.append(A01);
            Log.i(sb.toString());
            A02.close();
        } catch (SQLiteDatabaseCorruptException e) {
            Log.e("thumbnailmsgstore/deleteMessageThumbnail", e);
        }
    }

    public final void A04(AnonymousClass1IS r8, byte[] bArr) {
        try {
            try {
                C16310on A02 = this.A02.A02();
                try {
                    AbstractC14640lm r0 = r8.A00;
                    AnonymousClass009.A05(r0);
                    String rawString = r0.getRawString();
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("thumbnail", bArr);
                    contentValues.put("key_remote_jid", rawString);
                    int i = 0;
                    if (r8.A02) {
                        i = 1;
                    }
                    contentValues.put("key_from_me", String.valueOf(i));
                    contentValues.put("key_id", r8.A01);
                    contentValues.put("timestamp", Long.valueOf(System.currentTimeMillis() / 1000));
                    A02.A03.A04(contentValues, "message_thumbnails");
                    A02.close();
                } catch (Throwable th) {
                    try {
                        A02.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            } catch (SQLiteDatabaseCorruptException e) {
                Log.e("thumbnailmsgstore/insertOrUpdateMessageThumbnail", e);
            }
        } catch (Error | RuntimeException e2) {
            Log.e(e2);
            throw e2;
        }
    }

    public void A05(AnonymousClass1IS r4, byte[] bArr, long j) {
        if (A08(j)) {
            boolean z = false;
            if (j > 0) {
                z = true;
            }
            StringBuilder sb = new StringBuilder("ThumbnailMessageStore/insertOrUpdateMessageThumbnail/message must have row_id set; key=");
            sb.append(r4);
            AnonymousClass009.A0B(sb.toString(), z);
            if (bArr != null) {
                A07(bArr, j);
                return;
            }
            return;
        }
        A04(r4, bArr);
    }

    public void A06(Collection collection) {
        String str;
        C16310on A02 = this.A02.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            Iterator it = collection.iterator();
            while (it.hasNext()) {
                String str2 = (String) it.next();
                C20120vF r4 = this.A01;
                boolean z = true;
                String[] strArr = {str2};
                if (r4.A0A()) {
                    str = "SELECT COUNT(1) FROM message_media WHERE file_hash=?";
                } else {
                    str = "SELECT COUNT(1) FROM messages WHERE media_hash = ?";
                }
                C16310on A01 = r4.A02.get();
                Cursor A09 = A01.A03.A09(str, strArr);
                try {
                    if (A09.moveToNext() && A09.getLong(0) != 0) {
                        z = false;
                    }
                    A09.close();
                    A01.close();
                    if (!z) {
                        A02.A03.A01("media_hash_thumbnail", "media_hash = ?", new String[]{str2});
                    }
                } catch (Throwable th) {
                    if (A09 != null) {
                        try {
                            A09.close();
                        } catch (Throwable unused) {
                        }
                    }
                    throw th;
                }
            }
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th2) {
            try {
                A02.close();
            } catch (Throwable unused2) {
            }
            throw th2;
        }
    }

    public final void A07(byte[] bArr, long j) {
        C16310on A02 = this.A02.A02();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("message_row_id", Long.valueOf(j));
            contentValues.put("thumbnail", bArr);
            boolean z = false;
            if (A02.A03.A06(contentValues, "message_thumbnail", 5) == j) {
                z = true;
            }
            AnonymousClass009.A0C("ThumbnailMessageStore/insertOrUpdateThumbnailV2/inserted row should has same row_id", z);
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final boolean A08(long j) {
        C21390xL r3 = this.A03;
        if (r3.A00("thumbnail_ready", 0) != 2) {
            return j > 0 && r3.A01("migration_message_thumbnail_index", 0) >= j;
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x00c0 A[Catch: all -> 0x00ce, TRY_ENTER, TRY_LEAVE, TryCatch #7 {all -> 0x00ce, blocks: (B:25:0x0089, B:28:0x009a, B:36:0x00c0, B:30:0x00ab, B:31:0x00b2, B:33:0x00b8), top: B:48:0x0089 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public byte[] A09(X.AbstractC15340mz r10) {
        /*
            r9 = this;
            if (r10 != 0) goto L_0x0004
            r4 = 0
            return r4
        L_0x0004:
            boolean r0 = r10 instanceof X.C30061Vy
            if (r0 == 0) goto L_0x0011
            X.0oV r10 = (X.AbstractC16130oV) r10
            java.lang.String r0 = r10.A05
            byte[] r4 = r9.A0A(r0)
            return r4
        L_0x0011:
            long r0 = r10.A11
            boolean r0 = r9.A08(r0)
            if (r0 == 0) goto L_0x0078
            A00(r10)
            long r5 = r10.A11
            r3 = 1
            r4 = 0
            r1 = 0
            int r0 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            r2 = 0
            if (r0 <= 0) goto L_0x0028
            r2 = 1
        L_0x0028:
            java.lang.String r0 = "ThumbnailMessageStore/getThumbnailV2/message must have row_id set; key="
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            X.1IS r0 = r10.A0z
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            X.AnonymousClass009.A0B(r0, r2)
            java.lang.String[] r3 = new java.lang.String[r3]
            long r0 = r10.A11
            java.lang.String r0 = java.lang.Long.toString(r0)
            r3[r4] = r0
            r4 = 0
            X.0p7 r0 = r9.A02
            X.0on r2 = r0.get()
            X.0op r1 = r2.A03     // Catch: all -> 0x0073
            java.lang.String r0 = "SELECT thumbnail FROM message_thumbnail WHERE message_row_id = ?"
            android.database.Cursor r1 = r1.A09(r0, r3)     // Catch: all -> 0x0073
            boolean r0 = r1.moveToNext()     // Catch: all -> 0x006c
            if (r0 == 0) goto L_0x0065
            java.lang.String r0 = "thumbnail"
            int r0 = r1.getColumnIndexOrThrow(r0)     // Catch: all -> 0x006c
            byte[] r4 = r1.getBlob(r0)     // Catch: all -> 0x006c
        L_0x0065:
            r1.close()     // Catch: all -> 0x0073
            r2.close()
            return r4
        L_0x006c:
            r0 = move-exception
            if (r1 == 0) goto L_0x0072
            r1.close()     // Catch: all -> 0x0072
        L_0x0072:
            throw r0     // Catch: all -> 0x0073
        L_0x0073:
            r0 = move-exception
            r2.close()     // Catch: all -> 0x0077
        L_0x0077:
            throw r0
        L_0x0078:
            X.1IS r8 = r10.A0z
            X.0lm r0 = r8.A00
            X.AnonymousClass009.A05(r0)
            java.lang.String r1 = r0.getRawString()
            X.0p7 r0 = r9.A02
            X.0on r5 = r0.get()
            X.0op r7 = r5.A03     // Catch: all -> 0x00ce
            java.lang.String r6 = "SELECT thumbnail FROM message_thumbnails WHERE key_remote_jid = ? AND key_from_me = ? AND key_id = ?"
            r0 = 3
            java.lang.String[] r4 = new java.lang.String[r0]     // Catch: all -> 0x00ce
            r3 = 0
            r4[r3] = r1     // Catch: all -> 0x00ce
            boolean r2 = r8.A02     // Catch: all -> 0x00ce
            r1 = 1
            r0 = 0
            if (r2 == 0) goto L_0x009a
            r0 = 1
        L_0x009a:
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch: all -> 0x00ce
            r4[r1] = r0     // Catch: all -> 0x00ce
            r1 = 2
            java.lang.String r0 = r8.A01     // Catch: all -> 0x00ce
            r4[r1] = r0     // Catch: all -> 0x00ce
            android.database.Cursor r1 = r7.A09(r6, r4)     // Catch: all -> 0x00ce
            if (r1 != 0) goto L_0x00b2
            java.lang.String r0 = "thumbnailmsgstore/getMessageThumbnail no cursor"
            com.whatsapp.util.Log.e(r0)     // Catch: all -> 0x00c7
            goto L_0x00bd
        L_0x00b2:
            boolean r0 = r1.moveToLast()     // Catch: all -> 0x00c7
            if (r0 == 0) goto L_0x00bd
            byte[] r4 = r1.getBlob(r3)     // Catch: all -> 0x00c7
            goto L_0x00be
        L_0x00bd:
            r4 = 0
        L_0x00be:
            if (r1 == 0) goto L_0x00c3
            r1.close()     // Catch: all -> 0x00ce
        L_0x00c3:
            r5.close()
            return r4
        L_0x00c7:
            r0 = move-exception
            if (r1 == 0) goto L_0x00cd
            r1.close()     // Catch: all -> 0x00cd
        L_0x00cd:
            throw r0     // Catch: all -> 0x00ce
        L_0x00ce:
            r0 = move-exception
            r5.close()     // Catch: all -> 0x00d2
        L_0x00d2:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20950wa.A09(X.0mz):byte[]");
    }

    public final byte[] A0A(String str) {
        byte[] bArr = null;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        String[] strArr = {str};
        C16310on A01 = this.A02.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT thumbnail FROM media_hash_thumbnail WHERE media_hash = ?", strArr);
            if (A09.moveToNext()) {
                bArr = A09.getBlob(A09.getColumnIndexOrThrow("thumbnail"));
            }
            A09.close();
            A01.close();
            return bArr;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
