package X;

import com.facebook.redex.RunnableBRunnable0Shape6S0200000_I0_6;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/* renamed from: X.22P  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass22P {
    public final HashSet A00 = new HashSet();
    public final LinkedHashMap A01 = new LinkedHashMap();
    public final /* synthetic */ C22390z0 A02;

    public /* synthetic */ AnonymousClass22P(C22390z0 r2) {
        this.A02 = r2;
    }

    public synchronized void A00() {
        Iterator it = this.A01.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            HashSet hashSet = this.A00;
            if (!hashSet.contains(entry.getKey())) {
                break;
            }
            AbstractC15340mz r3 = (AbstractC15340mz) entry.getValue();
            it.remove();
            hashSet.remove(entry.getKey());
            C22390z0 r1 = this.A02;
            r1.A00.A01(r3);
            r1.A02.Ab2(new RunnableBRunnable0Shape6S0200000_I0_6(this, 11, r3));
        }
    }

    public synchronized String toString() {
        StringBuilder sb;
        sb = new StringBuilder();
        sb.append("[pending:");
        sb.append(this.A01.size());
        sb.append(" ready:");
        sb.append(this.A00.size());
        sb.append("]");
        return sb.toString();
    }
}
