package X;

import com.google.android.search.verification.client.SearchActionVerificationClientService;
import java.util.Arrays;
import java.util.Collections;

/* renamed from: X.4wF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C106764wF implements AbstractC116785Ww {
    public int A00;
    public int A01;
    public int A02;
    public int A03 = 0;
    public long A04;
    public AbstractC14070ko A05;
    public C95424dg A06;
    public AnonymousClass5X6 A07;
    public C76793mF A08;
    public C100624mD A09;
    public final AnonymousClass4IE A0A = new AnonymousClass4IE();
    public final C95304dT A0B = new C95304dT(new byte[32768], 0);
    public final byte[] A0C = new byte[42];

    public final void A00() {
        this.A07.AbG(null, 1, this.A00, 0, (this.A04 * SearchActionVerificationClientService.MS_TO_NS) / ((long) this.A06.A07));
    }

    @Override // X.AbstractC116785Ww
    public void AIa(AbstractC14070ko r3) {
        this.A05 = r3;
        this.A07 = r3.Af4(0, 1);
        r3.A9V();
    }

    @Override // X.AbstractC116785Ww
    public int AZn(AnonymousClass5Yf r30, AnonymousClass4IG r31) {
        int i;
        boolean A0C;
        C100624mD r14;
        C95424dg r7;
        C95424dg r5;
        AnonymousClass5WY r4;
        boolean z;
        boolean z2;
        long j;
        int i2 = this.A03;
        if (i2 != 0) {
            if (i2 == 1) {
                byte[] bArr = this.A0C;
                r30.AZ4(bArr, 0, bArr.length);
                r30.Aaj();
                i = 2;
            } else if (i2 == 2) {
                C95304dT A05 = C95304dT.A05(4);
                r30.readFully(A05.A02, 0, 4);
                if (A05.A0I() == 1716281667) {
                    i = 3;
                } else {
                    throw AnonymousClass496.A00("Failed to read FLAC stream marker.");
                }
            } else if (i2 == 3) {
                AnonymousClass4IF r8 = new AnonymousClass4IF(this.A06);
                do {
                    r30.Aaj();
                    C95054d0 r52 = new C95054d0(new byte[4], 4);
                    r30.AZ4(r52.A03, 0, 4);
                    A0C = r52.A0C();
                    int A04 = r52.A04(7);
                    int A042 = r52.A04(24) + 4;
                    if (A04 == 0) {
                        byte[] bArr2 = new byte[38];
                        r30.readFully(bArr2, 0, 38);
                        r7 = new C95424dg(bArr2, 4);
                    } else {
                        C95424dg r72 = r8.A00;
                        if (r72 == null) {
                            throw C72453ed.A0h();
                        } else if (A04 == 3) {
                            C95304dT A052 = C95304dT.A05(A042);
                            r30.readFully(A052.A02, 0, A042);
                            r7 = r72.A04(AnonymousClass4DF.A00(A052));
                        } else {
                            if (A04 == 4) {
                                C95304dT A053 = C95304dT.A05(A042);
                                r30.readFully(A053.A02, 0, A042);
                                A053.A0T(4);
                                A053.A0O((int) A053.A0G());
                                long A0G = A053.A0G();
                                String[] strArr = new String[(int) A0G];
                                for (int i3 = 0; ((long) i3) < A0G; i3++) {
                                    strArr[i3] = A053.A0O((int) A053.A0G());
                                }
                                r14 = C95424dg.A01(Arrays.asList(new AnonymousClass4IH(strArr).A00), Collections.emptyList());
                            } else if (A04 == 6) {
                                C95304dT A054 = C95304dT.A05(A042);
                                r30.readFully(A054.A02, 0, A042);
                                A054.A0T(4);
                                int A07 = A054.A07();
                                int A072 = A054.A07();
                                String str = new String(A054.A02, A054.A01, A072, C88814Hf.A01);
                                A054.A01 += A072;
                                String A0O = A054.A0O(A054.A07());
                                int A073 = A054.A07();
                                int A074 = A054.A07();
                                int A075 = A054.A07();
                                int A076 = A054.A07();
                                int A077 = A054.A07();
                                byte[] bArr3 = new byte[A077];
                                A054.A0V(bArr3, 0, A077);
                                r14 = C95424dg.A01(Collections.emptyList(), Collections.singletonList(new C107504xR(str, A0O, bArr3, A07, A073, A074, A075, A076)));
                            } else {
                                r30.Ae3(A042);
                                r5 = r8.A00;
                                this.A06 = r5;
                            }
                            C100624mD r2 = r72.A0B;
                            if (r2 != null) {
                                r14 = r2.A00(r14);
                            }
                            r7 = new C95424dg(r72.A0A, r14, r72.A05, r72.A03, r72.A06, r72.A04, r72.A07, r72.A02, r72.A00, r72.A09);
                        }
                    }
                    r8.A00 = r7;
                    r5 = r8.A00;
                    this.A06 = r5;
                } while (!A0C);
                this.A02 = Math.max(r5.A06, 6);
                this.A07.AA6(r5.A03(this.A09, this.A0C));
                i = 4;
            } else if (i2 == 4) {
                r30.Aaj();
                C95304dT A055 = C95304dT.A05(2);
                C95304dT.A06(r30, A055, 2);
                int A0F = A055.A0F();
                if ((A0F >> 2) == 16382) {
                    r30.Aaj();
                    this.A01 = A0F;
                    AbstractC14070ko r53 = this.A05;
                    long AFo = r30.AFo();
                    long length = r30.getLength();
                    C95424dg r82 = this.A06;
                    if (r82.A0A != null) {
                        r4 = new C106894wS(r82, AFo);
                    } else if (length == -1 || r82.A09 <= 0) {
                        r4 = new C106904wT(r82.A02(), 0);
                    } else {
                        C76793mF r73 = new C76793mF(r82, A0F, AFo, length);
                        this.A08 = r73;
                        r4 = r73.A02;
                    }
                    r53.AbR(r4);
                    i = 5;
                } else {
                    r30.Aaj();
                    throw AnonymousClass496.A00("First frame does not start with sync code.");
                }
            } else if (i2 == 5) {
                C95424dg r10 = this.A06;
                C76793mF r22 = this.A08;
                if (!(r22 == null || r22.A00 == null)) {
                    return r22.A00(r30, r31);
                }
                if (this.A04 == -1) {
                    r30.Aaj();
                    boolean z3 = true;
                    r30.A5r(1);
                    byte[] bArr4 = new byte[1];
                    r30.AZ4(bArr4, 0, 1);
                    if ((bArr4[0] & 1) != 1) {
                        z3 = false;
                    }
                    r30.A5r(2);
                    int i4 = 6;
                    if (z3) {
                        i4 = 7;
                    }
                    C95304dT A056 = C95304dT.A05(i4);
                    byte[] bArr5 = A056.A02;
                    int i5 = 0;
                    do {
                        int AZ0 = r30.AZ0(bArr5, 0 + i5, i4 - i5);
                        if (AZ0 == -1) {
                            break;
                        }
                        i5 += AZ0;
                    } while (i5 < i4);
                    A056.A0R(i5);
                    r30.Aaj();
                    try {
                        long A0K = A056.A0K();
                        if (!z3) {
                            A0K *= (long) r10.A03;
                        }
                        this.A04 = A0K;
                        return 0;
                    } catch (NumberFormatException unused) {
                        throw new AnonymousClass496();
                    }
                } else {
                    C95304dT r54 = this.A0B;
                    int i6 = r54.A00;
                    if (i6 < 32768) {
                        int read = r30.read(r54.A02, i6, 32768 - i6);
                        if (read == -1) {
                            z = true;
                            if (C95304dT.A00(r54) == 0) {
                                A00();
                                return -1;
                            }
                        } else {
                            z = false;
                            r54.A0R(i6 + read);
                        }
                    } else {
                        z = false;
                    }
                    int i7 = r54.A01;
                    int i8 = this.A00;
                    int i9 = this.A02;
                    if (i8 < i9) {
                        r54.A0T(C72463ee.A02(r54.A00, i7, i9 - i8));
                    }
                    int i10 = r54.A01;
                    while (true) {
                        int i11 = r54.A00;
                        if (i10 <= i11 - 16) {
                            r54.A0S(i10);
                            C95424dg r3 = this.A06;
                            int i12 = this.A01;
                            AnonymousClass4IE r23 = this.A0A;
                            if (C92944Yc.A01(r23, r3, r54, i12)) {
                                r54.A0S(i10);
                                j = r23.A00;
                                break;
                            }
                            i10++;
                        } else {
                            if (!z) {
                                r54.A0S(i10);
                            } else {
                                while (i10 <= i11 - this.A02) {
                                    r54.A0S(i10);
                                    try {
                                        z2 = C92944Yc.A01(this.A0A, this.A06, r54, this.A01);
                                    } catch (IndexOutOfBoundsException unused2) {
                                        z2 = false;
                                    }
                                    int i13 = r54.A01;
                                    i11 = r54.A00;
                                    if (i13 <= i11 && z2) {
                                        r54.A0S(i10);
                                        j = this.A0A.A00;
                                        break;
                                    }
                                    i10++;
                                }
                                r54.A0S(i11);
                            }
                            j = -1;
                        }
                    }
                    int i14 = r54.A01 - i7;
                    r54.A0S(i7);
                    this.A07.AbC(r54, i14);
                    this.A00 += i14;
                    if (j != -1) {
                        A00();
                        this.A00 = 0;
                        this.A04 = j;
                    }
                    int i15 = r54.A00;
                    int i16 = r54.A01;
                    int i17 = i15 - i16;
                    if (i17 >= 16) {
                        return 0;
                    }
                    byte[] bArr6 = r54.A02;
                    System.arraycopy(bArr6, i16, bArr6, 0, i17);
                    r54.A0S(0);
                    r54.A0R(i17);
                    return 0;
                }
            } else {
                throw C72463ee.A0D();
            }
            this.A03 = i;
            return 0;
        }
        r30.Aaj();
        long AFf = r30.AFf();
        C100624mD r74 = null;
        C100624mD A00 = new C92084Ul().A00(r30, null);
        if (!(A00 == null || A00.A00.length == 0)) {
            r74 = A00;
        }
        r30.Ae3((int) (r30.AFf() - AFf));
        this.A09 = r74;
        this.A03 = 1;
        return 0;
    }

    @Override // X.AbstractC116785Ww
    public void AbQ(long j, long j2) {
        long j3 = 0;
        if (j == 0) {
            this.A03 = 0;
        } else {
            C76793mF r0 = this.A08;
            if (r0 != null) {
                r0.A01(j2);
            }
        }
        if (j2 != 0) {
            j3 = -1;
        }
        this.A04 = j3;
        this.A00 = 0;
        this.A0B.A0Q(0);
    }

    @Override // X.AbstractC116785Ww
    public boolean Ae5(AnonymousClass5Yf r7) {
        new C92084Ul().A00(r7, C76993mZ.A01);
        C95304dT A05 = C95304dT.A05(4);
        r7.AZ4(A05.A02, 0, 4);
        if (A05.A0I() == 1716281667) {
            return true;
        }
        return false;
    }
}
