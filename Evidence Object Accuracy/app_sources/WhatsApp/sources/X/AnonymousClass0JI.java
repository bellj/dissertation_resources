package X;

/* renamed from: X.0JI  reason: invalid class name */
/* loaded from: classes.dex */
public enum AnonymousClass0JI {
    PRE_COMP,
    /* Fake field, exist only in values array */
    SOLID,
    IMAGE,
    /* Fake field, exist only in values array */
    NULL,
    /* Fake field, exist only in values array */
    SHAPE,
    /* Fake field, exist only in values array */
    TEXT,
    UNKNOWN
}
