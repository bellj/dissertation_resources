package X;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaTextView;

/* renamed from: X.2hk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C55072hk extends AnonymousClass03U {
    public final ViewGroup A00;
    public final ImageView A01;
    public final ImageView A02;
    public final C15570nT A03;
    public final C237913a A04;
    public final TextEmojiLabel A05;
    public final C28801Pb A06;
    public final WaTextView A07;
    public final AnonymousClass1J1 A08;

    public /* synthetic */ C55072hk(View view, C15570nT r5, C237913a r6, C15610nY r7, AnonymousClass1J1 r8, AnonymousClass12F r9) {
        super(view);
        this.A04 = r6;
        this.A03 = r5;
        this.A08 = r8;
        this.A00 = (ViewGroup) AnonymousClass028.A0D(view, R.id.group_chat_info_row_container);
        AnonymousClass028.A0D(view, R.id.group_chat_info_layout).setBackground(null);
        this.A01 = C12970iu.A0K(view, R.id.avatar);
        this.A02 = C12970iu.A0K(view, R.id.community_member_error_icon);
        TextEmojiLabel A0T = C12970iu.A0T(view, R.id.name);
        this.A07 = C12960it.A0N(view, R.id.owner);
        this.A05 = C12970iu.A0T(view, R.id.status);
        this.A06 = new C28801Pb(view.getContext(), A0T, r7, r9);
    }
}
