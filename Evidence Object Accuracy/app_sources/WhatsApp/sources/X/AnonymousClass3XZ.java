package X;

import android.app.Activity;
import android.util.Base64;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Map;
import java.util.Set;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/* renamed from: X.3XZ  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3XZ implements AbstractC44401yr {
    public final /* synthetic */ UserJid A00;
    public final /* synthetic */ AnonymousClass1WA A01;

    public AnonymousClass3XZ(UserJid userJid, AnonymousClass1WA r2) {
        this.A01 = r2;
        this.A00 = userJid;
    }

    @Override // X.AbstractC44401yr
    public void A6t(AnonymousClass23Z r20) {
        Set keySet;
        String str;
        C16700pc.A0E(r20, 0);
        if (r20.A00 == 0) {
            C25761As r3 = (C25761As) r20.A02;
            AnonymousClass1WA r8 = this.A01;
            Object obj = ((AnonymousClass12Z) r3).A00;
            C16700pc.A0C(obj);
            C16700pc.A0B(obj);
            String str2 = (String) obj;
            String str3 = r8.A06;
            String str4 = r8.A09;
            C16700pc.A0I(str2, str3);
            C16700pc.A0E(str4, 2);
            byte[] decode = Base64.decode(str3, 2);
            C30071Vz r4 = new C30071Vz(new SecretKeySpec(decode, 0, decode.length, "AES"), Base64.decode(str2, 2), Base64.decode(str4, 2));
            try {
                IvParameterSpec ivParameterSpec = new IvParameterSpec(r4.A02);
                Cipher instance = Cipher.getInstance("AES/GCM/NoPadding");
                instance.init(2, r4.A00, ivParameterSpec);
                str = new String(instance.doFinal(r4.A01));
            } catch (GeneralSecurityException e) {
                Log.w(C12960it.A0d(e.getMessage(), C12960it.A0k("GalaxyConnectionManagerdecryptGalaxyFlowData/issue while decrypting data/")));
                str = null;
            }
            int i = r3.A00;
            Activity activity = r8.A00;
            C90904Pr r7 = r8.A03;
            if (i == 0) {
                r8.A02.A0I(new RunnableC55722jC(activity, r7, r8, null, str, null, C12980iv.A1X(str)));
                return;
            }
            r8.A02.A0I(new RunnableC55722jC(activity, r7, r8, null, null, str, true));
            return;
        }
        AnonymousClass1WA r5 = this.A01;
        UserJid userJid = this.A00;
        C25751Ap r6 = (C25751Ap) r20.A03;
        if (r5.A0A) {
            Map map = r6.A00;
            if (map == null) {
                keySet = null;
            } else {
                keySet = map.keySet();
            }
            C16700pc.A0C(keySet);
            if (keySet.contains(2498061)) {
                Activity activity2 = r5.A00;
                C90904Pr r42 = r5.A03;
                String str5 = r5.A08;
                String str6 = r5.A07;
                String str7 = r5.A06;
                String str8 = r5.A09;
                C19820uj r62 = r5.A05;
                r62.A00(userJid, new AnonymousClass1WA(activity2, r5.A01, r5.A02, r42, r5.A04, r62, str5, str6, str7, str8, false), str6, str7, str8, true);
                return;
            }
        }
        r5.A02.A0I(new RunnableC55722jC(r5.A00, r5.A03, r5, r6, null, null, true));
    }

    @Override // X.AbstractC44401yr
    public void AOz(IOException iOException) {
        AnonymousClass1WA r4 = this.A01;
        r4.A02.A0I(new RunnableC55722jC(r4.A00, r4.A03, r4, null, null, null, true));
    }

    @Override // X.AbstractC44401yr
    public void APp(Exception exc) {
        AnonymousClass1WA r4 = this.A01;
        r4.A02.A0I(new RunnableC55722jC(r4.A00, r4.A03, r4, null, null, null, true));
    }
}
