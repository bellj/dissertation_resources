package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3og  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78293og extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C98874jO();
    public final int A00;
    public final int A01 = 1;
    public final String A02;

    public C78293og(String str, int i) {
        C13020j0.A01(str);
        this.A02 = str;
        this.A00 = i;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 1, this.A01);
        C95654e8.A0D(parcel, this.A02, 2, false);
        C95654e8.A07(parcel, 3, this.A00);
        C95654e8.A06(parcel, A00);
    }
}
