package X;

/* renamed from: X.4S0  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4S0 {
    public final int A00;
    public final String A01;
    public final String A02;
    public final boolean A03;

    public AnonymousClass4S0(String str, String str2, int i, boolean z) {
        this.A02 = str;
        this.A01 = str2;
        this.A00 = i;
        this.A03 = z;
    }
}
