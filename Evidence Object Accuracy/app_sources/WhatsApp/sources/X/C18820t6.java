package X;

import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipInputStream;

/* renamed from: X.0t6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C18820t6 extends AbstractC18830t7 {
    public String A00;
    public String A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C18820t6(C18790t3 r8, C16590pI r9, C18810t5 r10, C18800t4 r11, AbstractC14440lR r12) {
        super(r8, r9, r10, r11, r12, 13);
        C16700pc.A0E(r9, 1);
        C16700pc.A0E(r12, 2);
        C16700pc.A0E(r8, 3);
        C16700pc.A0E(r11, 4);
        C16700pc.A0E(r10, 5);
    }

    @Override // X.AbstractC18830t7
    public /* bridge */ /* synthetic */ void A03(AnonymousClass2DH r3, AnonymousClass2K5 r4, Object obj, String str) {
        throw new UnsupportedOperationException("Current fetch method is not supported for the bloks commerce flow");
    }

    @Override // X.AbstractC18830t7
    public /* bridge */ /* synthetic */ boolean A07(InputStream inputStream, Object obj) {
        File A00 = A00(C16700pc.A08("commerce_flow_", this.A00));
        if (A00 != null) {
            C14350lI.A0M(A00);
        }
        File A002 = super.A00(C16700pc.A08("commerce_flow_", this.A00));
        File A003 = A00(C25841Ba.A0E);
        if (A002 == null || A003 == null) {
            Log.e("CommerceBloksAssetDownloader/storeAssets:: Could not prepare resource directory");
            return false;
        }
        try {
            ZipInputStream zipInputStream = new ZipInputStream(inputStream);
            try {
                new AnonymousClass31X(A002, A003).A02(zipInputStream);
                zipInputStream.close();
                return true;
            } catch (Throwable th) {
                try {
                    throw th;
                } catch (Throwable th2) {
                    C88184Ep.A00(zipInputStream, th);
                    throw th2;
                }
            }
        } catch (FileNotFoundException | IOException e) {
            Log.e("CommerceBloksAssetDownloader/unzipBatchBackgrounds", e);
            return false;
        }
    }

    public final void A0A(AnonymousClass2DH r2, String str, String str2) {
        C16700pc.A0E(str, 0);
        C16700pc.A0E(str2, 1);
        this.A00 = str2;
        this.A01 = str;
        super.A03(r2, null, null, str);
    }
}
