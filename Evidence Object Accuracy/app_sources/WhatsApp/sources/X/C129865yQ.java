package X;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import com.facebook.redex.IDxCListenerShape10S0100000_3_I1;
import com.facebook.redex.IDxDListenerShape14S0100000_3_I1;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.lang.ref.WeakReference;

/* renamed from: X.5yQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129865yQ {
    public final AnonymousClass12P A00;
    public final AnonymousClass61M A01;
    public final WeakReference A02;

    public C129865yQ(AnonymousClass12P r2, ActivityC13790kL r3, AnonymousClass61M r4) {
        this.A02 = C12970iu.A10(r3);
        this.A00 = r2;
        this.A01 = r4;
    }

    public static void A00(C129865yQ r2, C130785zy r3) {
        r2.A02(r3.A00, null, null);
    }

    public void A01(AnonymousClass018 r12, C452120p r13, Runnable runnable, Runnable runnable2, Runnable runnable3) {
        ActivityC13790kL r2 = (ActivityC13790kL) this.A02.get();
        if (r2 != null && !(!((ActivityC13810kN) r2).A0E)) {
            String str = r13.A08;
            String str2 = r13.A07;
            if (str2 == null) {
                str2 = "";
            }
            AnonymousClass04S A00 = C130295zB.A00(r2, C126995tm.A00(null, R.string.ok), C126995tm.A00(null, R.string.learn_more), str, str2, false);
            A00.setOnShowListener(new DialogInterface.OnShowListener(A00, r2, r12, this, runnable, runnable2) { // from class: X.636
                public final /* synthetic */ AnonymousClass04S A00;
                public final /* synthetic */ ActivityC13790kL A01;
                public final /* synthetic */ AnonymousClass018 A02;
                public final /* synthetic */ C129865yQ A03;
                public final /* synthetic */ Runnable A04;
                public final /* synthetic */ Runnable A05;

                {
                    this.A03 = r4;
                    this.A00 = r1;
                    this.A04 = r5;
                    this.A05 = r6;
                    this.A02 = r3;
                    this.A01 = r2;
                }

                @Override // android.content.DialogInterface.OnShowListener
                public final void onShow(DialogInterface dialogInterface) {
                    C129865yQ r8 = this.A03;
                    AnonymousClass04S r7 = this.A00;
                    Runnable runnable4 = this.A04;
                    Runnable runnable5 = this.A05;
                    AnonymousClass018 r4 = this.A02;
                    ActivityC13790kL r3 = this.A01;
                    AnonymousClass0U5 r22 = r7.A00;
                    C117295Zj.A0o(r22.A0G, runnable4, r7, 0);
                    r22.A0E.setOnClickListener(
                    /*  JADX ERROR: Method code generation error
                        jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x001b: INVOKE  
                          (wrap: android.widget.Button : 0x0014: IGET  (r1v1 android.widget.Button A[REMOVE]) = (r2v0 'r22' X.0U5) X.0U5.A0E android.widget.Button)
                          (wrap: X.64T : 0x0018: CONSTRUCTOR  (r0v1 X.64T A[REMOVE]) = (r3v0 'r3' X.0kL), (r4v0 'r4' X.018), (r8v0 'r8' X.5yQ), (r5v0 'runnable5' java.lang.Runnable) call: X.64T.<init>(X.0kL, X.018, X.5yQ, java.lang.Runnable):void type: CONSTRUCTOR)
                         type: VIRTUAL call: android.view.View.setOnClickListener(android.view.View$OnClickListener):void in method: X.636.onShow(android.content.DialogInterface):void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                        	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                        	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                        	at java.util.ArrayList.forEach(ArrayList.java:1259)
                        	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                        	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                        Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0018: CONSTRUCTOR  (r0v1 X.64T A[REMOVE]) = (r3v0 'r3' X.0kL), (r4v0 'r4' X.018), (r8v0 'r8' X.5yQ), (r5v0 'runnable5' java.lang.Runnable) call: X.64T.<init>(X.0kL, X.018, X.5yQ, java.lang.Runnable):void type: CONSTRUCTOR in method: X.636.onShow(android.content.DialogInterface):void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                        	... 15 more
                        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.64T, state: NOT_LOADED
                        	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                        	... 21 more
                        */
                    /*
                        this = this;
                        X.5yQ r8 = r9.A03
                        X.04S r7 = r9.A00
                        java.lang.Runnable r6 = r9.A04
                        java.lang.Runnable r5 = r9.A05
                        X.018 r4 = r9.A02
                        X.0kL r3 = r9.A01
                        X.0U5 r2 = r7.A00
                        android.widget.Button r1 = r2.A0G
                        r0 = 0
                        X.C117295Zj.A0o(r1, r6, r7, r0)
                        android.widget.Button r1 = r2.A0E
                        X.64T r0 = new X.64T
                        r0.<init>(r3, r4, r8, r5)
                        r1.setOnClickListener(r0)
                        return
                    */
                    throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass636.onShow(android.content.DialogInterface):void");
                }
            });
            if (runnable3 != null) {
                runnable3.run();
            }
            A00.show();
        }
    }

    public void A02(C452120p r11, Runnable runnable, Runnable runnable2) {
        AnonymousClass04S A02;
        C126995tm A00;
        C126995tm r6;
        String string;
        String string2;
        int i;
        if (r11 == null || !((i = r11.A00) == 542720109 || i == 542720003)) {
            WeakReference weakReference = this.A02;
            ActivityC13810kN r3 = (ActivityC13810kN) weakReference.get();
            ActivityC13810kN r4 = (ActivityC13810kN) weakReference.get();
            boolean z = false;
            if (!(r11 == null || r4 == null || (!r4.A0E))) {
                int i2 = r11.A00;
                if (i2 == -2) {
                    string = null;
                    string2 = r4.getString(R.string.novi_airplane_mode_message);
                    A00 = C126995tm.A00(runnable, R.string.ok);
                    r6 = C126995tm.A00(new Runnable(r4, this) { // from class: X.6Hm
                        public final /* synthetic */ Context A00;
                        public final /* synthetic */ C129865yQ A01;

                        {
                            this.A01 = r2;
                            this.A00 = r1;
                        }

                        @Override // java.lang.Runnable
                        public final void run() {
                            C129865yQ r0 = this.A01;
                            r0.A00.A06(this.A00, new Intent("android.settings.SETTINGS"));
                        }
                    }, R.string.permission_settings_open);
                } else {
                    A00 = C126995tm.A00(runnable, R.string.close);
                    if (runnable2 != null) {
                        r6 = C126995tm.A00(runnable2, R.string.payments_try_again);
                    } else {
                        r6 = null;
                    }
                    int i3 = 0;
                    if (i2 != -2) {
                        if (i2 == 6) {
                            i3 = R.string.check_for_internet_connection;
                        }
                    }
                    string = r4.getString(i3);
                    int i4 = 0;
                    if (i2 == 6) {
                        i4 = R.string.network_required;
                    }
                    string2 = r4.getString(i4);
                    z = true;
                }
                A02 = C130295zB.A00(r4, A00, r6, string, string2, z);
                A02.show();
            }
            if (r3 != null && !(!r3.A0E)) {
                IDxDListenerShape14S0100000_3_I1 iDxDListenerShape14S0100000_3_I1 = new IDxDListenerShape14S0100000_3_I1(runnable, 1);
                if (r11 != null) {
                    A02 = AnonymousClass61M.A01(r3, iDxDListenerShape14S0100000_3_I1, iDxDListenerShape14S0100000_3_I1, null, r11.A00);
                    if (A02 == null) {
                        String str = r11.A08;
                        String str2 = r11.A07;
                        if (runnable2 != null) {
                            IDxCListenerShape10S0100000_3_I1 iDxCListenerShape10S0100000_3_I1 = new IDxCListenerShape10S0100000_3_I1(runnable2, 1);
                            C004802e A0S = C12980iv.A0S(r3);
                            A0S.setNegativeButton(R.string.close, null);
                            A0S.setPositiveButton(R.string.payments_try_again, iDxCListenerShape10S0100000_3_I1);
                            A0S.A04(iDxDListenerShape14S0100000_3_I1);
                            if (str2 == null) {
                                A0S.A0A(str);
                            } else {
                                A0S.setTitle(str);
                                A0S.A0A(str2);
                            }
                            A02 = A0S.create();
                        } else if (str != null) {
                            if (str2 != null) {
                                A02 = AnonymousClass61M.A03(r3, iDxDListenerShape14S0100000_3_I1, str, str2);
                            } else {
                                A02 = AnonymousClass61M.A02(r3, iDxDListenerShape14S0100000_3_I1, str);
                            }
                        } else if (str2 != null) {
                            A02 = AnonymousClass61M.A02(r3, iDxDListenerShape14S0100000_3_I1, str2);
                        }
                    }
                    A02.show();
                }
                A02 = AnonymousClass61M.A02(r3, iDxDListenerShape14S0100000_3_I1, r3.getString(R.string.payments_generic_error));
                A02.show();
            }
        }
    }

    public void A03(C452120p r10, Runnable runnable, Runnable runnable2) {
        ActivityC13790kL r3;
        if (r10 == null || r10.A00 != 443 || (r3 = (ActivityC13790kL) this.A02.get()) == null) {
            A02(r10, runnable, null);
        } else {
            C130295zB.A00(r3, C126995tm.A00(new Runnable() { // from class: X.6FR
                @Override // java.lang.Runnable
                public final void run() {
                    ActivityC13790kL r1 = ActivityC13790kL.this;
                    try {
                        r1.startActivity(C117295Zj.A04("market://details?id=com.whatsapp"));
                    } catch (ActivityNotFoundException unused) {
                        Log.e("[PAY] : GP store is not available");
                    }
                    r1.finish();
                }
            }, R.string.upgrade), C126995tm.A00(runnable2, R.string.not_now), r3.getString(R.string.payments_upgrade_error), r3.getString(R.string.payments_upgrade_error_message), false).show();
        }
    }
}
