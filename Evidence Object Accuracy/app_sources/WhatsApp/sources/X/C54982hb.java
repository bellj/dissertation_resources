package X;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.calling.callhistory.group.GroupCallLogActivity;

/* renamed from: X.2hb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54982hb extends AnonymousClass03U {
    public ImageButton A00;
    public ImageButton A01;
    public ImageView A02;
    public TextView A03;
    public C28801Pb A04;
    public final /* synthetic */ GroupCallLogActivity A05;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C54982hb(View view, GroupCallLogActivity groupCallLogActivity) {
        super(view);
        this.A05 = groupCallLogActivity;
        this.A02 = C12970iu.A0L(view, R.id.contact_photo);
        this.A04 = new C28801Pb(view, groupCallLogActivity.A03, groupCallLogActivity.A08, (int) R.id.contact_name);
        this.A01 = (ImageButton) view.findViewById(R.id.call_btn);
        this.A00 = (ImageButton) view.findViewById(R.id.video_call_btn);
        this.A03 = C12960it.A0J(view, R.id.participant_call_log_result);
    }
}
