package X;

import java.util.Map;

/* renamed from: X.0dM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09690dM implements Runnable {
    public final AnonymousClass0SJ A00;
    public final String A01;

    public RunnableC09690dM(AnonymousClass0SJ r1, String str) {
        this.A00 = r1;
        this.A01 = str;
    }

    @Override // java.lang.Runnable
    public void run() {
        AnonymousClass0SJ r1 = this.A00;
        synchronized (r1.A00) {
            Map map = r1.A02;
            String str = this.A01;
            if (((RunnableC09690dM) map.remove(str)) != null) {
                AbstractC11490gN r6 = (AbstractC11490gN) r1.A01.remove(str);
                if (r6 != null) {
                    C06390Tk.A00().A02(C07640Zo.A09, String.format("Exceeded time limits on execution for %s", str), new Throwable[0]);
                    ((C07640Zo) r6).A01();
                }
            } else {
                C06390Tk.A00().A02("WrkTimerRunnable", String.format("Timer with %s is already marked as complete.", str), new Throwable[0]);
            }
        }
    }
}
