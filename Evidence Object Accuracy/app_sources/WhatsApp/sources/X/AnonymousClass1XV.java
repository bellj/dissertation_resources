package X;

import android.text.TextUtils;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.math.BigDecimal;

/* renamed from: X.1XV  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1XV extends AnonymousClass1X7 implements AbstractC16400ox, AbstractC16420oz {
    public int A00;
    public UserJid A01;
    public String A02;
    public String A03;
    public String A04;
    public String A05;
    public String A06;
    public String A07;
    public String A08;
    public String A09;
    public BigDecimal A0A;
    public BigDecimal A0B;

    public AnonymousClass1XV(C16150oX r2, AnonymousClass1IS r3, AnonymousClass1XV r4, long j, boolean z) {
        super(r2, r3, r4, j, z);
        this.A01 = r4.A01;
        this.A06 = r4.A06;
        this.A09 = r4.A09;
        this.A04 = r4.A04;
        this.A03 = r4.A03;
        this.A0A = r4.A0A;
        this.A0B = r4.A0B;
        this.A08 = r4.A08;
        this.A07 = r4.A07;
        this.A00 = r4.A00;
        this.A02 = r4.A02;
        this.A05 = r4.A05;
    }

    public AnonymousClass1XV(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 23, j);
    }

    public String A1E() {
        StringBuilder sb = new StringBuilder();
        if (!TextUtils.isEmpty(this.A09)) {
            sb.append(this.A09);
        }
        if (!TextUtils.isEmpty(this.A02)) {
            if (!TextUtils.isEmpty(sb)) {
                sb.append("\n");
            }
            sb.append(this.A02);
        }
        if (!TextUtils.isEmpty(this.A05)) {
            if (!TextUtils.isEmpty(sb)) {
                sb.append("\n");
            }
            sb.append(this.A05);
        }
        return sb.toString();
    }

    @Override // X.AnonymousClass1X7, X.AbstractC16420oz
    public void A6k(C39971qq r13) {
        UserJid userJid;
        AnonymousClass1G3 r2 = r13.A03;
        C57542nE r0 = ((C27081Fy) r2.A00).A0V;
        if (r0 == null) {
            r0 = C57542nE.A07;
        }
        AnonymousClass1G4 A0T = r0.A0T();
        C57542nE r02 = ((C27081Fy) r2.A00).A0V;
        if (r02 == null) {
            r02 = C57542nE.A07;
        }
        C57652nP r03 = r02.A03;
        if (r03 == null) {
            r03 = C57652nP.A0C;
        }
        AnonymousClass1G4 A0T2 = r03.A0T();
        C40821sO r04 = ((C57652nP) A0T2.A00).A04;
        if (r04 == null) {
            r04 = C40821sO.A0R;
        }
        boolean z = r13.A08;
        boolean z2 = r13.A06;
        C81963ur A1C = A1C((C81963ur) r04.A0T(), z, z2);
        if (A1C == null || (userJid = this.A01) == null) {
            StringBuilder sb = new StringBuilder("FMessageProduct/buildE2eMessage/unable to send encrypted media message due to missing mediaKey or businessOwnerJid; message.key=");
            sb.append(this.A0z);
            sb.append("; media_wa_type=");
            sb.append((int) this.A0y);
            sb.append("; business_owner_jid=");
            sb.append(this.A01);
            Log.w(sb.toString());
            return;
        }
        String rawString = userJid.getRawString();
        A0T.A03();
        C57542nE r1 = (C57542nE) A0T.A00;
        r1.A00 |= 2;
        r1.A05 = rawString;
        if (!TextUtils.isEmpty(this.A06)) {
            String str = this.A06;
            A0T2.A03();
            C57652nP r12 = (C57652nP) A0T2.A00;
            r12.A00 |= 2;
            r12.A08 = str;
        }
        if (!TextUtils.isEmpty(this.A04)) {
            String str2 = this.A04;
            A0T2.A03();
            C57652nP r14 = (C57652nP) A0T2.A00;
            r14.A00 |= 8;
            r14.A06 = str2;
        }
        if (!TextUtils.isEmpty(this.A09)) {
            String str3 = this.A09;
            A0T2.A03();
            C57652nP r15 = (C57652nP) A0T2.A00;
            r15.A00 |= 4;
            r15.A0A = str3;
        }
        if (!TextUtils.isEmpty(this.A02)) {
            String str4 = this.A02;
            A0T.A03();
            C57542nE r16 = (C57542nE) A0T.A00;
            r16.A00 |= 8;
            r16.A04 = str4;
        }
        if (!TextUtils.isEmpty(this.A05)) {
            String str5 = this.A05;
            A0T.A03();
            C57542nE r17 = (C57542nE) A0T.A00;
            r17.A00 |= 16;
            r17.A06 = str5;
        }
        if (!TextUtils.isEmpty(this.A03) && this.A0A != null) {
            String str6 = this.A03;
            A0T2.A03();
            C57652nP r18 = (C57652nP) A0T2.A00;
            r18.A00 |= 16;
            r18.A05 = str6;
            BigDecimal bigDecimal = this.A0A;
            BigDecimal bigDecimal2 = C30701Ym.A00;
            long longValue = bigDecimal.multiply(bigDecimal2).longValue();
            A0T2.A03();
            C57652nP r6 = (C57652nP) A0T2.A00;
            r6.A00 |= 32;
            r6.A02 = longValue;
            BigDecimal bigDecimal3 = this.A0B;
            if (!(bigDecimal3 == null || BigDecimal.ZERO.compareTo(bigDecimal3) == 0)) {
                long longValue2 = this.A0B.multiply(bigDecimal2).longValue();
                A0T2.A03();
                C57652nP r62 = (C57652nP) A0T2.A00;
                r62.A00 |= EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
                r62.A03 = longValue2;
            }
        }
        if (!TextUtils.isEmpty(this.A08)) {
            String str7 = this.A08;
            A0T2.A03();
            C57652nP r19 = (C57652nP) A0T2.A00;
            r19.A00 |= 64;
            r19.A09 = str7;
        }
        if (!TextUtils.isEmpty(this.A07)) {
            String str8 = this.A07;
            A0T2.A03();
            C57652nP r110 = (C57652nP) A0T2.A00;
            r110.A00 |= 128;
            r110.A0B = str8;
        }
        int i = this.A00;
        A0T2.A03();
        C57652nP r111 = (C57652nP) A0T2.A00;
        r111.A00 |= 256;
        r111.A01 = i;
        A0T2.A03();
        C57652nP r112 = (C57652nP) A0T2.A00;
        r112.A04 = (C40821sO) A1C.A02();
        r112.A00 |= 1;
        A0T.A03();
        C57542nE r113 = (C57542nE) A0T.A00;
        r113.A03 = (C57652nP) A0T2.A02();
        r113.A00 |= 1;
        AnonymousClass1PG r8 = r13.A04;
        byte[] bArr = r13.A09;
        if (C32411c7.A0U(r8, this, bArr)) {
            C43261wh A0P = C32411c7.A0P(r13.A00, r13.A02, r8, this, bArr, z2);
            A0T.A03();
            C57542nE r114 = (C57542nE) A0T.A00;
            r114.A01 = A0P;
            r114.A00 |= 32;
        }
        r2.A03();
        C27081Fy r22 = (C27081Fy) r2.A00;
        r22.A0V = (C57542nE) A0T.A02();
        r22.A00 |= EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING;
    }
}
