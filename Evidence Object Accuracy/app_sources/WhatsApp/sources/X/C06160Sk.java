package X;

import java.util.Random;

/* renamed from: X.0Sk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06160Sk {
    public static final C06160Sk A06 = new C06160Sk(true);
    public static final C06160Sk A07 = new C06160Sk(true);
    public static final C06160Sk A08 = new C06160Sk(true);
    public static final C06160Sk A09 = new C06160Sk(true);
    public static final C06160Sk A0A = new C06160Sk(true);
    public static final C06160Sk A0B = new C06160Sk(true);
    public static final C06160Sk A0C;
    public static final C06160Sk A0D;
    public static final C06160Sk A0E;
    public static final C06160Sk A0F = new C06160Sk(false);
    public static final C06160Sk A0G;
    public static final C06160Sk A0H;
    public static final C06160Sk A0I = new C06160Sk(true);
    public static final C06160Sk A0J = new C06160Sk(true);
    public static final C06160Sk A0K = new C06160Sk(true);
    public static final C06160Sk A0L;
    public static final C06160Sk A0M;
    public static final C06160Sk A0N = new C06160Sk(false);
    public static final C06160Sk A0O;
    public static final Random A0P = new Random();
    public static final C06160Sk[] A0Q;
    public int A00 = 0;
    public long A01 = Long.MIN_VALUE;
    public long A02 = Long.MAX_VALUE;
    public long A03 = 0;
    public long[] A04;
    public final boolean A05;

    static {
        C06160Sk r10 = new C06160Sk(false);
        A0C = r10;
        C06160Sk r9 = new C06160Sk(false);
        A0E = r9;
        C06160Sk r8 = new C06160Sk(true);
        A0M = r8;
        C06160Sk r7 = new C06160Sk(true);
        A0L = r7;
        C06160Sk r6 = new C06160Sk(true);
        A0O = r6;
        C06160Sk r5 = new C06160Sk(false);
        A0G = r5;
        C06160Sk r4 = new C06160Sk(false);
        A0H = r4;
        C06160Sk r3 = new C06160Sk(false);
        A0D = r3;
        A0Q = new C06160Sk[]{r10, r9, r8, r7, r6, r5, r4, r3};
    }

    public C06160Sk(boolean z) {
        this.A05 = z;
    }

    public void A00() {
        synchronized (this) {
        }
    }

    public void A01() {
        if (this.A05) {
            synchronized (this) {
                this.A01 = Long.MIN_VALUE;
                this.A02 = Long.MAX_VALUE;
                this.A03 = 0;
                this.A00 = 0;
            }
            return;
        }
        this.A01 = Long.MIN_VALUE;
        this.A02 = Long.MAX_VALUE;
        this.A03 = 0;
        this.A00 = 0;
    }

    public void A02(long j) {
        if (this.A05) {
            synchronized (this) {
                A03(j);
            }
            return;
        }
        A03(j);
    }

    public final void A03(long j) {
        long[] jArr = this.A04;
        if (jArr == null) {
            jArr = new long[256];
            this.A04 = jArr;
        }
        long j2 = this.A01;
        if (j2 <= j) {
            j2 = j;
        }
        this.A01 = j2;
        long j3 = this.A02;
        if (j3 >= j) {
            j3 = j;
        }
        this.A02 = j3;
        this.A03 += j;
        int i = this.A00;
        if (i < 256 || (i = A0P.nextInt(i + 1)) < 256) {
            jArr[i] = j;
        }
        int i2 = i + 1;
        this.A00 = i2;
        if (i2 == 256) {
            A01();
        }
    }
}
