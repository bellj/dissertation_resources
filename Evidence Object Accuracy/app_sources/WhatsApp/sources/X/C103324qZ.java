package X;

import android.content.Context;
import com.whatsapp.quickcontact.QuickContactActivity;

/* renamed from: X.4qZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103324qZ implements AbstractC009204q {
    public final /* synthetic */ QuickContactActivity A00;

    public C103324qZ(QuickContactActivity quickContactActivity) {
        this.A00 = quickContactActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
