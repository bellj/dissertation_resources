package X;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.URLSpan;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.text.ReadMoreTextView;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.5m4  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass5m4 extends AbstractC118835cS {
    public final C14900mE A00;
    public final AnonymousClass18U A01;
    public final AnonymousClass3DI A02;
    public final AnonymousClass01d A03;
    public final AnonymousClass13H A04;
    public final ReadMoreTextView A05;

    public AnonymousClass5m4(View view, C14900mE r3, AnonymousClass18U r4, AnonymousClass3DI r5, AnonymousClass01d r6, AnonymousClass13H r7) {
        super(view);
        this.A00 = r3;
        this.A04 = r7;
        this.A01 = r4;
        this.A02 = r5;
        this.A03 = r6;
        this.A05 = (ReadMoreTextView) AnonymousClass028.A0D(view, R.id.payment_note_text);
    }

    @Override // X.AbstractC118835cS
    public void A08(AbstractC125975s7 r7, int i) {
        AbstractC15340mz r5 = ((C123045mW) r7).A00;
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(r5.A0I());
        this.A04.A02(this.A0H.getContext(), spannableStringBuilder, r5.A0o);
        TextEmojiLabel textEmojiLabel = this.A05;
        A09(spannableStringBuilder, textEmojiLabel, true);
        this.A02.A00(textEmojiLabel, new AbstractC116035Tw() { // from class: X.67V
            @Override // X.AbstractC116035Tw
            public final void AaT(Spannable spannable) {
                AnonymousClass5m4 r2 = AnonymousClass5m4.this;
                r2.A09(spannable, r2.A05, false);
            }
        }, spannableStringBuilder, r5.A0z);
    }

    public final void A09(Spannable spannable, TextEmojiLabel textEmojiLabel, boolean z) {
        int i;
        AnonymousClass2eq r0;
        Context context = textEmojiLabel.getContext();
        ArrayList A05 = C42971wC.A05(spannable);
        if (A05 == null || A05.isEmpty()) {
            i = 0;
        } else {
            Iterator it = A05.iterator();
            i = 0;
            while (it.hasNext()) {
                URLSpan uRLSpan = (URLSpan) it.next();
                String url = uRLSpan.getURL();
                spannable.setSpan(new C58272oQ(context, this.A01, this.A00, this.A03, url), spannable.getSpanStart(uRLSpan), spannable.getSpanEnd(uRLSpan), spannable.getSpanFlags(uRLSpan));
                i++;
            }
            Iterator it2 = A05.iterator();
            while (it2.hasNext()) {
                spannable.removeSpan(it2.next());
            }
        }
        boolean A1W = C12960it.A1W(textEmojiLabel.A06);
        if (i > 0) {
            if (!A1W) {
                r0 = new AnonymousClass2eq(textEmojiLabel, this.A03);
            }
            textEmojiLabel.A0E(spannable);
        }
        if (A1W) {
            textEmojiLabel.setFocusable(false);
            AnonymousClass028.A0a(textEmojiLabel, 0);
        }
        r0 = null;
        textEmojiLabel.setAccessibilityHelper(r0);
        if (i <= 0 && !z) {
            return;
        }
        textEmojiLabel.A0E(spannable);
    }
}
