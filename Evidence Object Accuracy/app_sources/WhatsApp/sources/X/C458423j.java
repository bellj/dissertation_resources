package X;

import com.whatsapp.jid.UserJid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.23j  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C458423j {
    public UserJid A00;
    public final int A01;
    public final long A02 = 60000;
    public final long A03;
    public final long A04;
    public final C14830m7 A05;
    public final C22180yf A06;
    public final UserJid A07;
    public final Integer A08;
    public final String A09;
    public final List A0A;
    public final List A0B;
    public final List A0C;
    public final Map A0D;
    public final Map A0E;
    public final Map A0F = new HashMap();

    public C458423j(C14830m7 r5, C22180yf r6, UserJid userJid, Integer num, String str, List list, List list2, List list3, Map map, Map map2, int i, long j, long j2) {
        this.A07 = userJid;
        this.A05 = r5;
        this.A06 = r6;
        this.A04 = j2;
        this.A03 = j;
        this.A08 = num;
        this.A01 = i;
        this.A0C = list;
        this.A0B = list2;
        this.A0A = list3;
        this.A0E = map;
        this.A0D = map2;
        this.A09 = str;
    }

    public final boolean A00(AbstractC15340mz r10) {
        long A00 = this.A05.A00();
        Map map = this.A0D;
        AnonymousClass1IS r5 = r10.A0z;
        Number number = (Number) map.get(r5);
        if (number != null && A00 - number.longValue() <= this.A02) {
            return false;
        }
        map.put(r5, Long.valueOf(A00));
        return true;
    }
}
