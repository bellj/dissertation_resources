package X;

import android.animation.ObjectAnimator;
import android.text.Editable;
import android.view.animation.Animation;
import com.whatsapp.Conversation;

/* renamed from: X.368  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass368 extends C469928m {
    public final /* synthetic */ Conversation A00;

    public AnonymousClass368(Conversation conversation) {
        this.A00 = conversation;
    }

    @Override // X.C469928m, android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        AnonymousClass1Ly r1;
        String obj = editable.toString();
        boolean z = !AnonymousClass1US.A0C(obj);
        Conversation conversation = this.A00;
        conversation.A0X.setEnabled(z);
        conversation.A30();
        if (conversation.A0Y.getVisibility() == 8 && !z) {
            C469928m.A00(conversation.A0Y, true);
            conversation.A0Y.setVisibility(0);
            conversation.A0W.startAnimation(Conversation.A03(C28141Kv.A01(conversation.A26), true, false));
            conversation.A0V.startAnimation(Conversation.A03(C28141Kv.A01(conversation.A26), true, true));
            conversation.A0V.setVisibility(0);
            C469928m.A00(conversation.A0X, false);
            conversation.A0X.setVisibility(8);
            int A08 = conversation.A3D.A08(conversation.A2s);
            if (conversation.A3O(A08) && conversation.A0C.getVisibility() != 0) {
                conversation.A0C.startAnimation(Conversation.A03(C28141Kv.A01(conversation.A26), true, true));
                conversation.A0C.setVisibility(0);
                conversation.A38(A08);
            }
        } else if (conversation.A0Y.getVisibility() == 0 && z) {
            C469928m.A00(conversation.A0Y, false);
            conversation.A0Y.setVisibility(8);
            conversation.A0W.startAnimation(Conversation.A03(C28141Kv.A01(conversation.A26), false, false));
            Animation A03 = Conversation.A03(C28141Kv.A01(conversation.A26), false, true);
            A03.setAnimationListener(new C83173wo(conversation.A0V));
            conversation.A0V.startAnimation(A03);
            if (conversation.A3O(conversation.A3D.A08(conversation.A2s)) && conversation.A0C.getVisibility() != 8) {
                Animation A032 = Conversation.A03(C28141Kv.A01(conversation.A26), false, true);
                A032.setAnimationListener(new C83173wo(conversation.A0C));
                conversation.A0C.startAnimation(A032);
                ObjectAnimator objectAnimator = conversation.A05;
                if (objectAnimator != null) {
                    objectAnimator.cancel();
                }
            }
            C469928m.A00(conversation.A0X, true);
            conversation.A0X.setVisibility(0);
        }
        C14310lE r2 = conversation.A21;
        C37071lG r12 = conversation.A1L;
        if (r12 == null) {
            r12 = new C37071lG(conversation.A1K);
            conversation.A1L = r12;
        }
        r2.A04(editable, r12);
        C42971wC.A06(conversation, conversation.A2w.getPaint(), editable, ((ActivityC13810kN) conversation).A08, ((ActivityC13810kN) conversation).A0B, conversation.A3J);
        if (conversation.A3Z.A00 && (r1 = conversation.A3c) != null && ((AbstractActivityC13750kH) conversation).A0T.A02) {
            r1.A00(obj, 200);
        }
    }
}
