package X;

import android.os.Build;

/* renamed from: X.4GR  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4GR {
    public static final AbstractC116215Uo A00;

    static {
        AbstractC116215Uo r0;
        int i = Build.VERSION.SDK_INT;
        if (i >= 28) {
            r0 = new C69713a7();
        } else if (i >= 23) {
            r0 = new C69703a6();
        } else {
            r0 = new AnonymousClass59P();
        }
        A00 = r0;
    }
}
