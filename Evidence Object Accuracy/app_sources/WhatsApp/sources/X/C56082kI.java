package X;

import android.net.Uri;
import android.util.Base64;
import java.net.URLDecoder;

/* renamed from: X.2kI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56082kI extends AbstractC67743Ss {
    public int A00;
    public int A01;
    public AnonymousClass3H3 A02;
    public byte[] A03;

    public C56082kI() {
        super(false);
    }

    @Override // X.AnonymousClass2BW
    public Uri AHS() {
        AnonymousClass3H3 r0 = this.A02;
        if (r0 != null) {
            return r0.A04;
        }
        return null;
    }

    @Override // X.AnonymousClass2BW
    public long AYZ(AnonymousClass3H3 r7) {
        byte[] decode;
        int length;
        A01();
        this.A02 = r7;
        this.A01 = (int) r7.A03;
        Uri uri = r7.A04;
        String scheme = uri.getScheme();
        if ("data".equals(scheme)) {
            String[] split = uri.getSchemeSpecificPart().split(",", -1);
            if (split.length == 2) {
                String str = split[1];
                if (split[0].contains(";base64")) {
                    try {
                        decode = Base64.decode(str, 0);
                        this.A03 = decode;
                    } catch (IllegalArgumentException e) {
                        throw new AnonymousClass496(C12960it.A0d(str, C12960it.A0k("Error while parsing Base64 encoded string: ")), e);
                    }
                } else {
                    decode = URLDecoder.decode(str, C88814Hf.A01.name()).getBytes(C88814Hf.A05);
                    this.A03 = decode;
                }
                long j = r7.A02;
                if (j != -1) {
                    length = ((int) j) + this.A01;
                } else {
                    length = decode.length;
                }
                this.A00 = length;
                if (length > decode.length || this.A01 > length) {
                    this.A03 = null;
                    throw new C867948w();
                }
                A03(r7);
                return ((long) this.A00) - ((long) this.A01);
            }
            throw new AnonymousClass496(C12960it.A0b("Unexpected URI format: ", uri));
        }
        throw new AnonymousClass496(C12960it.A0d(scheme, C12960it.A0k("Unsupported scheme: ")));
    }

    @Override // X.AnonymousClass2BW
    public void close() {
        if (this.A03 != null) {
            this.A03 = null;
            A00();
        }
        this.A02 = null;
    }

    @Override // X.AnonymousClass2BY
    public int read(byte[] bArr, int i, int i2) {
        if (i2 == 0) {
            return 0;
        }
        int i3 = this.A00;
        int i4 = this.A01;
        int i5 = i3 - i4;
        if (i5 == 0) {
            return -1;
        }
        int min = Math.min(i2, i5);
        System.arraycopy(this.A03, i4, bArr, i, min);
        this.A01 += min;
        A02(min);
        return min;
    }
}
