package X;

import com.whatsapp.voipcalling.GlVideoRenderer;
import java.io.EOFException;
import org.chromium.net.UrlRequest;

/* renamed from: X.0HI  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0HI extends AbstractC08850bx {
    public static final C08960c8 A06 = A00("*/");
    public static final C08960c8 A07 = A00("\"\\");
    public static final C08960c8 A08 = A00("\n\r");
    public static final C08960c8 A09 = A00("'\\");
    public static final C08960c8 A0A = A00("{}[]:, \n\t\r\f/\\;#=");
    public int A00 = 0;
    public int A01;
    public long A02;
    public String A03;
    public final C10730f6 A04;
    public final AbstractC02750Dv A05;

    public AnonymousClass0HI(AbstractC02750Dv r2) {
        this.A05 = r2;
        this.A04 = r2.A6i();
        A0F(6);
    }

    public static final C08960c8 A00(String str) {
        return C95384dc.A03(str);
    }

    @Override // X.AbstractC08850bx
    public double A02() {
        String A0M;
        C08960c8 r0;
        double parseDouble;
        int i = this.A00;
        if (i == 0) {
            i = A0K();
        }
        if (i == 16) {
            this.A00 = 0;
            int[] iArr = super.A01;
            int i2 = super.A00 - 1;
            iArr[i2] = iArr[i2] + 1;
            return (double) this.A02;
        }
        try {
            if (i == 17) {
                A0M = this.A04.A05((long) this.A01);
            } else {
                if (i == 9) {
                    r0 = A07;
                } else if (i == 8) {
                    r0 = A09;
                } else if (i == 10) {
                    A0M = A0M();
                } else {
                    if (i != 11) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Expected a double but was ");
                        sb.append(A05());
                        sb.append(" at path ");
                        sb.append(A06());
                        throw new C10750f9(sb.toString());
                    }
                    this.A00 = 11;
                    parseDouble = Double.parseDouble(this.A03);
                    if (!Double.isNaN(parseDouble) || Double.isInfinite(parseDouble)) {
                        StringBuilder sb2 = new StringBuilder("JSON forbids NaN and infinities: ");
                        sb2.append(parseDouble);
                        sb2.append(" at path ");
                        sb2.append(A06());
                        throw new C03640Io(sb2.toString());
                    }
                    this.A03 = null;
                    this.A00 = 0;
                    int[] iArr2 = super.A01;
                    int i3 = super.A00 - 1;
                    iArr2[i3] = iArr2[i3] + 1;
                    return parseDouble;
                }
                A0M = A0N(r0);
            }
            parseDouble = Double.parseDouble(this.A03);
            if (!Double.isNaN(parseDouble)) {
            }
            StringBuilder sb2 = new StringBuilder("JSON forbids NaN and infinities: ");
            sb2.append(parseDouble);
            sb2.append(" at path ");
            sb2.append(A06());
            throw new C03640Io(sb2.toString());
        } catch (NumberFormatException unused) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("Expected a double but was ");
            sb3.append(this.A03);
            sb3.append(" at path ");
            sb3.append(A06());
            throw new C10750f9(sb3.toString());
        }
        this.A03 = A0M;
        this.A00 = 11;
    }

    @Override // X.AbstractC08850bx
    public int A03() {
        C08960c8 r0;
        int i = this.A00;
        if (i == 0) {
            i = A0K();
        }
        if (i == 16) {
            long j = this.A02;
            int i2 = (int) j;
            if (j == ((long) i2)) {
                this.A00 = 0;
                int[] iArr = super.A01;
                int i3 = super.A00 - 1;
                iArr[i3] = iArr[i3] + 1;
                return i2;
            }
            StringBuilder sb = new StringBuilder();
            sb.append("Expected an int but was ");
            sb.append(j);
            sb.append(" at path ");
            sb.append(A06());
            throw new C10750f9(sb.toString());
        }
        if (i == 17) {
            this.A03 = this.A04.A05((long) this.A01);
        } else {
            if (i == 9) {
                r0 = A07;
            } else if (i == 8) {
                r0 = A09;
            } else if (i != 11) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Expected an int but was ");
                sb2.append(A05());
                sb2.append(" at path ");
                sb2.append(A06());
                throw new C10750f9(sb2.toString());
            }
            String A0N = A0N(r0);
            this.A03 = A0N;
            try {
                int parseInt = Integer.parseInt(A0N);
                this.A00 = 0;
                int[] iArr2 = super.A01;
                int i4 = super.A00 - 1;
                iArr2[i4] = iArr2[i4] + 1;
                return parseInt;
            } catch (NumberFormatException unused) {
            }
        }
        this.A00 = 11;
        try {
            String str = this.A03;
            double parseDouble = Double.parseDouble(str);
            int i5 = (int) parseDouble;
            if (((double) i5) == parseDouble) {
                this.A03 = null;
                this.A00 = 0;
                int[] iArr3 = super.A01;
                int i6 = super.A00 - 1;
                iArr3[i6] = iArr3[i6] + 1;
                return i5;
            }
            StringBuilder sb3 = new StringBuilder();
            sb3.append("Expected an int but was ");
            sb3.append(str);
            sb3.append(" at path ");
            sb3.append(A06());
            throw new C10750f9(sb3.toString());
        } catch (NumberFormatException unused2) {
            StringBuilder sb4 = new StringBuilder();
            sb4.append("Expected an int but was ");
            sb4.append(this.A03);
            sb4.append(" at path ");
            sb4.append(A06());
            throw new C10750f9(sb4.toString());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0071, code lost:
        if (r6 == -1) goto L_0x0073;
     */
    @Override // X.AbstractC08850bx
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A04(X.C05850Rf r10) {
        /*
            r9 = this;
            int r1 = r9.A00
            if (r1 != 0) goto L_0x0008
            int r1 = r9.A0K()
        L_0x0008:
            r0 = 12
            r8 = -1
            if (r1 < r0) goto L_0x0087
            r7 = 15
            if (r1 > r7) goto L_0x0087
            if (r1 != r7) goto L_0x0032
            java.lang.String r4 = r9.A03
            java.lang.String[] r3 = r10.A01
            int r2 = r3.length
            r1 = 0
            r6 = 0
        L_0x001a:
            if (r6 >= r2) goto L_0x0085
            r0 = r3[r6]
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x002f
            r9.A00 = r1
            java.lang.String[] r1 = r9.A03
            int r0 = r9.A00
            int r0 = r0 + -1
            r1[r0] = r4
        L_0x002e:
            return r6
        L_0x002f:
            int r6 = r6 + 1
            goto L_0x001a
        L_0x0032:
            X.0Dv r1 = r9.A05
            X.5I3 r0 = r10.A00
            int r3 = r1.AbU(r0)
            if (r3 == r8) goto L_0x004c
            r0 = 0
            r9.A00 = r0
            java.lang.String[] r2 = r9.A03
            int r0 = r9.A00
            int r1 = r0 + -1
            java.lang.String[] r0 = r10.A01
            r0 = r0[r3]
            r2[r1] = r0
            return r3
        L_0x004c:
            java.lang.String[] r1 = r9.A03
            int r0 = r9.A00
            int r0 = r0 + -1
            r5 = r1[r0]
            java.lang.String r4 = r9.A07()
            java.lang.String[] r3 = r10.A01
            int r2 = r3.length
            r1 = 0
            r6 = 0
        L_0x005d:
            if (r6 >= r2) goto L_0x0083
            r0 = r3[r6]
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0080
            r9.A00 = r1
            java.lang.String[] r1 = r9.A03
            int r0 = r9.A00
            int r0 = r0 + -1
            r1[r0] = r4
            if (r6 != r8) goto L_0x002e
        L_0x0073:
            r9.A00 = r7
            r9.A03 = r4
            java.lang.String[] r1 = r9.A03
            int r0 = r9.A00
            int r0 = r0 + -1
            r1[r0] = r5
            return r6
        L_0x0080:
            int r6 = r6 + 1
            goto L_0x005d
        L_0x0083:
            r6 = -1
            goto L_0x0073
        L_0x0085:
            r6 = -1
            return r6
        L_0x0087:
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0HI.A04(X.0Rf):int");
    }

    @Override // X.AbstractC08850bx
    public EnumC03770Jb A05() {
        int i = this.A00;
        if (i == 0) {
            i = A0K();
        }
        switch (i) {
            case 1:
                return EnumC03770Jb.BEGIN_OBJECT;
            case 2:
                return EnumC03770Jb.END_OBJECT;
            case 3:
                return EnumC03770Jb.BEGIN_ARRAY;
            case 4:
                return EnumC03770Jb.END_ARRAY;
            case 5:
            case 6:
                return EnumC03770Jb.BOOLEAN;
            case 7:
                return EnumC03770Jb.NULL;
            case 8:
            case 9:
            case 10:
            case 11:
                return EnumC03770Jb.STRING;
            case 12:
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
            case 15:
                return EnumC03770Jb.NAME;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
            case 17:
                return EnumC03770Jb.NUMBER;
            case 18:
                return EnumC03770Jb.END_DOCUMENT;
            default:
                throw new AssertionError();
        }
    }

    @Override // X.AbstractC08850bx
    public String A07() {
        String str;
        C08960c8 r0;
        int i = this.A00;
        if (i == 0) {
            i = A0K();
        }
        if (i == 14) {
            str = A0M();
        } else {
            if (i == 13) {
                r0 = A07;
            } else if (i == 12) {
                r0 = A09;
            } else if (i == 15) {
                str = this.A03;
            } else {
                StringBuilder sb = new StringBuilder("Expected a name but was ");
                sb.append(A05());
                sb.append(" at path ");
                sb.append(A06());
                throw new C10750f9(sb.toString());
            }
            str = A0N(r0);
        }
        this.A00 = 0;
        super.A03[super.A00 - 1] = str;
        return str;
    }

    @Override // X.AbstractC08850bx
    public String A08() {
        String A05;
        int i = this.A00;
        if (i == 0) {
            i = A0K();
        }
        if (i == 10) {
            A05 = A0M();
        } else if (i == 9) {
            A05 = A0N(A07);
        } else if (i == 8) {
            A05 = A0N(A09);
        } else if (i == 11) {
            A05 = this.A03;
            this.A03 = null;
        } else if (i == 16) {
            A05 = Long.toString(this.A02);
        } else if (i == 17) {
            A05 = this.A04.A05((long) this.A01);
        } else {
            StringBuilder sb = new StringBuilder("Expected a string but was ");
            sb.append(A05());
            sb.append(" at path ");
            sb.append(A06());
            throw new C10750f9(sb.toString());
        }
        this.A00 = 0;
        int[] iArr = super.A01;
        int i2 = super.A00 - 1;
        iArr[i2] = iArr[i2] + 1;
        return A05;
    }

    @Override // X.AbstractC08850bx
    public void A09() {
        int i = this.A00;
        if (i == 0) {
            i = A0K();
        }
        if (i == 3) {
            A0F(1);
            super.A01[super.A00 - 1] = 0;
            this.A00 = 0;
            return;
        }
        StringBuilder sb = new StringBuilder("Expected BEGIN_ARRAY but was ");
        sb.append(A05());
        sb.append(" at path ");
        sb.append(A06());
        throw new C10750f9(sb.toString());
    }

    @Override // X.AbstractC08850bx
    public void A0A() {
        int i = this.A00;
        if (i == 0) {
            i = A0K();
        }
        if (i == 1) {
            A0F(3);
            this.A00 = 0;
            return;
        }
        StringBuilder sb = new StringBuilder("Expected BEGIN_OBJECT but was ");
        sb.append(A05());
        sb.append(" at path ");
        sb.append(A06());
        throw new C10750f9(sb.toString());
    }

    @Override // X.AbstractC08850bx
    public void A0B() {
        int i = this.A00;
        if (i == 0) {
            i = A0K();
        }
        if (i == 4) {
            int i2 = super.A00 - 1;
            super.A00 = i2;
            int[] iArr = super.A01;
            int i3 = i2 - 1;
            iArr[i3] = iArr[i3] + 1;
            this.A00 = 0;
            return;
        }
        StringBuilder sb = new StringBuilder("Expected END_ARRAY but was ");
        sb.append(A05());
        sb.append(" at path ");
        sb.append(A06());
        throw new C10750f9(sb.toString());
    }

    @Override // X.AbstractC08850bx
    public void A0C() {
        int i = this.A00;
        if (i == 0) {
            i = A0K();
        }
        if (i == 2) {
            int i2 = super.A00 - 1;
            super.A00 = i2;
            super.A03[i2] = null;
            int[] iArr = super.A01;
            int i3 = i2 - 1;
            iArr[i3] = iArr[i3] + 1;
            this.A00 = 0;
            return;
        }
        StringBuilder sb = new StringBuilder("Expected END_OBJECT but was ");
        sb.append(A05());
        sb.append(" at path ");
        sb.append(A06());
        throw new C10750f9(sb.toString());
    }

    @Override // X.AbstractC08850bx
    public void A0D() {
        C08960c8 r0;
        int i = this.A00;
        if (i == 0) {
            i = A0K();
        }
        if (i == 14) {
            long AIW = this.A05.AIW(A0A);
            C10730f6 r3 = this.A04;
            if (AIW == -1) {
                AIW = r3.A03();
            }
            r3.A0G(AIW);
        } else {
            if (i == 13) {
                r0 = A07;
            } else if (i == 12) {
                r0 = A09;
            } else if (i != 15) {
                StringBuilder sb = new StringBuilder("Expected a name but was ");
                sb.append(A05());
                sb.append(" at path ");
                sb.append(A06());
                throw new C10750f9(sb.toString());
            }
            A0P(r0);
        }
        this.A00 = 0;
        super.A03[super.A00 - 1] = "null";
    }

    @Override // X.AbstractC08850bx
    public void A0E() {
        C08960c8 r0;
        int i = 0;
        do {
            int i2 = this.A00;
            if (i2 == 0) {
                i2 = A0K();
            }
            if (i2 == 3) {
                A0F(1);
            } else if (i2 == 1) {
                A0F(3);
            } else {
                if (i2 == 4) {
                    i--;
                    if (i < 0) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Expected a value but was ");
                        sb.append(A05());
                        sb.append(" at path ");
                        sb.append(A06());
                        throw new C10750f9(sb.toString());
                    }
                } else if (i2 == 2) {
                    i--;
                    if (i < 0) {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("Expected a value but was ");
                        sb2.append(A05());
                        sb2.append(" at path ");
                        sb2.append(A06());
                        throw new C10750f9(sb2.toString());
                    }
                } else {
                    if (i2 == 14 || i2 == 10) {
                        long AIW = this.A05.AIW(A0A);
                        C10730f6 r7 = this.A04;
                        if (AIW == -1) {
                            AIW = r7.A03();
                        }
                        r7.A0G(AIW);
                    } else {
                        if (i2 == 9 || i2 == 13) {
                            r0 = A07;
                        } else if (i2 == 8 || i2 == 12) {
                            r0 = A09;
                        } else if (i2 == 17) {
                            this.A04.A0G((long) this.A01);
                        } else if (i2 == 18) {
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append("Expected a value but was ");
                            sb3.append(A05());
                            sb3.append(" at path ");
                            sb3.append(A06());
                            throw new C10750f9(sb3.toString());
                        }
                        A0P(r0);
                    }
                    this.A00 = 0;
                }
                super.A00--;
                this.A00 = 0;
            }
            i++;
            this.A00 = 0;
        } while (i != 0);
        int[] iArr = super.A01;
        int i3 = super.A00;
        int i4 = i3 - 1;
        iArr[i4] = iArr[i4] + 1;
        super.A03[i3 - 1] = "null";
    }

    @Override // X.AbstractC08850bx
    public boolean A0H() {
        int i = this.A00;
        if (i == 0) {
            i = A0K();
        }
        return (i == 2 || i == 4 || i == 18) ? false : true;
    }

    @Override // X.AbstractC08850bx
    public boolean A0I() {
        int i = this.A00;
        if (i == 0) {
            i = A0K();
        }
        if (i == 5) {
            this.A00 = 0;
            int[] iArr = super.A01;
            int i2 = super.A00 - 1;
            iArr[i2] = iArr[i2] + 1;
            return true;
        } else if (i == 6) {
            this.A00 = 0;
            int[] iArr2 = super.A01;
            int i3 = super.A00 - 1;
            iArr2[i3] = iArr2[i3] + 1;
            return false;
        } else {
            StringBuilder sb = new StringBuilder("Expected a boolean but was ");
            sb.append(A05());
            sb.append(" at path ");
            sb.append(A06());
            throw new C10750f9(sb.toString());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0076, code lost:
        if (r9 > 57) goto L_0x0078;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x007e, code lost:
        if (r9 > 102) goto L_0x0080;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final char A0J() {
        /*
        // Method dump skipped, instructions count: 222
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0HI.A0J():char");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:147:0x01fb, code lost:
        if (A0Q(r1) != false) goto L_0x022a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:148:0x01fd, code lost:
        if (r11 != 2) goto L_0x021e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:149:0x01ff, code lost:
        if (r18 == false) goto L_0x0225;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:0x0205, code lost:
        if (r4 != Long.MIN_VALUE) goto L_0x0209;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:152:0x0207, code lost:
        if (r17 == false) goto L_0x0225;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:0x020b, code lost:
        if (r4 != 0) goto L_0x021b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x020d, code lost:
        if (r17 != false) goto L_0x0225;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:156:0x020f, code lost:
        r4 = -r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:157:0x0210, code lost:
        r19.A02 = r4;
        r6.A0G((long) r13);
        r5 = 16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x0218, code lost:
        r19.A00 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:159:0x021a, code lost:
        return r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:160:0x021b, code lost:
        if (r17 == false) goto L_0x020f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:161:0x021e, code lost:
        if (r11 == 2) goto L_0x0225;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:163:0x0221, code lost:
        if (r11 == 4) goto L_0x0225;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:164:0x0223, code lost:
        if (r11 != 7) goto L_0x022a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:165:0x0225, code lost:
        r19.A01 = r13;
        r5 = 17;
     */
    /* JADX WARNING: Removed duplicated region for block: B:168:0x0234  */
    /* JADX WARNING: Removed duplicated region for block: B:170:0x023f  */
    /* JADX WARNING: Removed duplicated region for block: B:201:0x01fd A[EDGE_INSN: B:201:0x01fd->B:148:0x01fd ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0092  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int A0K() {
        /*
        // Method dump skipped, instructions count: 706
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0HI.A0K():int");
    }

    public final int A0L(boolean z) {
        int i = 0;
        while (true) {
            AbstractC02750Dv r3 = this.A05;
            int i2 = i + 1;
            if (r3.AaY((long) i2)) {
                C10730f6 r4 = this.A04;
                byte A01 = r4.A01((long) i);
                if (A01 == 10 || A01 == 32 || A01 == 13 || A01 == 9) {
                    i = i2;
                } else {
                    r4.A0G((long) (i2 - 1));
                    if (A01 == 47) {
                        if (r3.AaY(2)) {
                            A0O();
                            throw new RuntimeException("Redex: Unreachable code after no-return invoke");
                        }
                    } else if (A01 == 35) {
                        A0O();
                        throw new RuntimeException("Redex: Unreachable code after no-return invoke");
                    }
                    return A01;
                }
            } else if (!z) {
                return -1;
            } else {
                throw new EOFException("End of input");
            }
        }
    }

    public final String A0M() {
        long AIW = this.A05.AIW(A0A);
        return AIW != -1 ? this.A04.A05(AIW) : this.A04.A04();
    }

    public final String A0N(C08960c8 r8) {
        StringBuilder sb = null;
        while (true) {
            long AIW = this.A05.AIW(r8);
            if (AIW != -1) {
                C10730f6 r4 = this.A04;
                if (r4.A01(AIW) == 92) {
                    if (sb == null) {
                        sb = new StringBuilder();
                    }
                    sb.append(r4.A05(AIW));
                    r4.readByte();
                    sb.append(A0J());
                } else {
                    String A05 = r4.A05(AIW);
                    if (sb == null) {
                        r4.readByte();
                        return A05;
                    }
                    sb.append(A05);
                    r4.readByte();
                    return sb.toString();
                }
            } else {
                A0G("Unterminated string");
                throw new RuntimeException("Redex: Unreachable code after no-return invoke");
            }
        }
    }

    public final void A0O() {
        A0G("Use JsonReader.setLenient(true) to accept malformed JSON");
        throw new RuntimeException("Redex: Unreachable code after no-return invoke");
    }

    public final void A0P(C08960c8 r8) {
        while (true) {
            long AIW = this.A05.AIW(r8);
            if (AIW != -1) {
                C10730f6 r6 = this.A04;
                byte A01 = r6.A01(AIW);
                long j = AIW + 1;
                if (A01 == 92) {
                    r6.A0G(j);
                    A0J();
                } else {
                    r6.A0G(j);
                    return;
                }
            } else {
                A0G("Unterminated string");
                throw new RuntimeException("Redex: Unreachable code after no-return invoke");
            }
        }
    }

    public final boolean A0Q(int i) {
        if (i == 9 || i == 10 || i == 12 || i == 13 || i == 32) {
            return false;
        }
        if (i != 35) {
            if (i == 44) {
                return false;
            }
            if (!(i == 47 || i == 61)) {
                if (i == 123 || i == 125 || i == 58) {
                    return false;
                }
                if (i != 59) {
                    switch (i) {
                        case 91:
                        case 93:
                            return false;
                        case 92:
                            break;
                        default:
                            return true;
                    }
                }
            }
        }
        A0O();
        throw new RuntimeException("Redex: Unreachable code after no-return invoke");
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.A00 = 0;
        super.A02[0] = 8;
        super.A00 = 1;
        this.A04.A0A();
        this.A05.close();
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("JsonReader(");
        sb.append(this.A05);
        sb.append(")");
        return sb.toString();
    }
}
