package X;

import java.util.Iterator;
import java.util.NoSuchElementException;

/* renamed from: X.04p  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C009104p implements Iterator {
    public int A00;
    public int A01;
    public boolean A02 = false;
    public final int A03;
    public final /* synthetic */ AbstractC008904n A04;

    public C009104p(AbstractC008904n r2, int i) {
        this.A04 = r2;
        this.A03 = i;
        this.A01 = r2.A01();
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        return this.A00 < this.A01;
    }

    @Override // java.util.Iterator
    public Object next() {
        if (hasNext()) {
            AbstractC008904n r1 = this.A04;
            int i = this.A00;
            Object A03 = r1.A03(i, this.A03);
            this.A00 = i + 1;
            this.A02 = true;
            return A03;
        }
        throw new NoSuchElementException();
    }

    @Override // java.util.Iterator
    public void remove() {
        if (this.A02) {
            int i = this.A00 - 1;
            this.A00 = i;
            this.A01--;
            this.A02 = false;
            this.A04.A07(i);
            return;
        }
        throw new IllegalStateException();
    }
}
