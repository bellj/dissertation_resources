package X;

/* renamed from: X.0FO  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0FO extends AbstractC02880Ff {
    public final /* synthetic */ C07710Zx A00;

    @Override // X.AbstractC05330Pd
    public String A01() {
        return "INSERT OR REPLACE INTO `SystemIdInfo` (`work_spec_id`,`system_id`) VALUES (?,?)";
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass0FO(AnonymousClass0QN r1, C07710Zx r2) {
        super(r1);
        this.A00 = r2;
    }

    @Override // X.AbstractC02880Ff
    public void A03(AbstractC12830ic r4, Object obj) {
        AnonymousClass0PC r5 = (AnonymousClass0PC) obj;
        String str = r5.A01;
        if (str == null) {
            r4.A6T(1);
        } else {
            r4.A6U(1, str);
        }
        r4.A6S(2, (long) r5.A00);
    }
}
