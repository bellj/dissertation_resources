package X;

import android.graphics.PointF;
import android.graphics.RectF;

/* renamed from: X.4O0  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4O0 {
    public PointF A00;
    public RectF A01;

    public AnonymousClass4O0(PointF pointF, RectF rectF) {
        this.A01 = rectF;
        this.A00 = pointF;
    }
}
