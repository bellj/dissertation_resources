package X;

import android.util.Base64;
import java.util.List;

/* renamed from: X.0Oc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C05060Oc {
    public final String A00;
    public final String A01;
    public final String A02;
    public final String A03;
    public final List A04;

    public C05060Oc(String str, String str2, String str3, List list) {
        this.A01 = str;
        this.A02 = str2;
        this.A03 = str3;
        this.A04 = list;
        StringBuilder sb = new StringBuilder(str);
        sb.append("-");
        sb.append(str2);
        sb.append("-");
        sb.append(str3);
        this.A00 = sb.toString();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder("FontRequest {mProviderAuthority: ");
        sb2.append(this.A01);
        sb2.append(", mProviderPackage: ");
        sb2.append(this.A02);
        sb2.append(", mQuery: ");
        sb2.append(this.A03);
        sb2.append(", mCertificates:");
        sb.append(sb2.toString());
        int i = 0;
        while (true) {
            List list = this.A04;
            if (i < list.size()) {
                sb.append(" [");
                List list2 = (List) list.get(i);
                for (int i2 = 0; i2 < list2.size(); i2++) {
                    sb.append(" \"");
                    sb.append(Base64.encodeToString((byte[]) list2.get(i2), 0));
                    sb.append("\"");
                }
                sb.append(" ]");
                i++;
            } else {
                sb.append("}");
                StringBuilder sb3 = new StringBuilder("mCertificatesArray: ");
                sb3.append(0);
                sb.append(sb3.toString());
                return sb.toString();
            }
        }
    }
}
