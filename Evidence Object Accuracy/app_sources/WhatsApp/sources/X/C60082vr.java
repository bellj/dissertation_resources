package X;

import com.google.android.material.chip.Chip;

/* renamed from: X.2vr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60082vr extends AbstractC75733kK {
    public Chip A00;
    public final C48842Hz A01;

    public C60082vr(Chip chip, C48842Hz r2) {
        super(chip);
        this.A00 = chip;
        this.A01 = r2;
    }

    @Override // X.AbstractC75733kK
    public void A08() {
        this.A01.A00();
    }

    @Override // X.AbstractC75733kK
    public void A09(AnonymousClass3E9 r4) {
        String str;
        C48142Em r2 = r4.A00;
        AnonymousClass009.A05(r2);
        if (r2 instanceof C48152En) {
            str = ((C48152En) r2).A00;
        } else if (r2 instanceof C48172Ep) {
            str = ((C48172Ep) r2).A00;
        } else {
            throw C12990iw.A0m("SearchHistoryBarItemChipViewHolder/bind Recent search type not supported");
        }
        Chip chip = this.A00;
        chip.setText(str);
        AbstractView$OnClickListenerC34281fs.A03(chip, this, r4, r2, 4);
    }
}
