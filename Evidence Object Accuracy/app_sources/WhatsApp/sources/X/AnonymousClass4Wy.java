package X;

import com.facebook.redex.IDxNFunctionShape17S0100000_2_I1;
import java.util.Arrays;

/* renamed from: X.4Wy  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4Wy {
    public final AnonymousClass1V8 A00;
    public final AbstractC115475Rr A01;

    public AnonymousClass4Wy(AbstractC15710nm r5, AnonymousClass1V8 r6) {
        AnonymousClass1V8.A01(r6, "state");
        this.A01 = (AbstractC115475Rr) AnonymousClass3JT.A06(r6, "FDSResourceState|FDSChoiceState|FDSSucceedState|FDSPassState", C12980iv.A0x(Arrays.asList(new IDxNFunctionShape17S0100000_2_I1(r5, 25), new IDxNFunctionShape17S0100000_2_I1(r5, 24), new IDxNFunctionShape17S0100000_2_I1(r5, 23), new IDxNFunctionShape17S0100000_2_I1(r5, 26))), new String[0]);
        this.A00 = r6;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || AnonymousClass4Wy.class != obj.getClass()) {
            return false;
        }
        return this.A01.equals(((AnonymousClass4Wy) obj).A01);
    }

    public int hashCode() {
        return C12980iv.A0B(this.A01, new Object[1], 0);
    }
}
