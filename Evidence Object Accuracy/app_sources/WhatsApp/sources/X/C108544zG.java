package X;

/* renamed from: X.4zG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C108544zG implements AbstractC115595Se {
    @Override // X.AbstractC115595Se
    public final byte[] AhH(byte[] bArr, int i, int i2) {
        byte[] bArr2 = new byte[i2];
        System.arraycopy(bArr, i, bArr2, 0, i2);
        return bArr2;
    }
}
