package X;

import android.app.Activity;
import com.whatsapp.R;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.3Tn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67953Tn implements AbstractC17450qp {
    public final AbstractC17450qp A00;
    public final C64873Hg A01;
    public final AbstractC72403eX A02;

    public C67953Tn(AbstractC17450qp r1, C64873Hg r2, AbstractC72403eX r3) {
        this.A02 = r3;
        this.A00 = r1;
        this.A01 = r2;
    }

    public static Activity A00(C14230l4 r1) {
        return (Activity) r1.A00.A02.A00().get(R.id.bloks_host_activity);
    }

    public static C90184Mx A01(List list, int i) {
        C1093751l r1 = ((C94724cR) list.get(i)).A00;
        C90184Mx r0 = new C90184Mx();
        r0.A01 = r1;
        return r0;
    }

    public static final HashMap A02(Map map) {
        String str;
        HashMap A11 = C12970iu.A11();
        Iterator A0n = C12960it.A0n(map);
        while (A0n.hasNext()) {
            Map.Entry A15 = C12970iu.A15(A0n);
            boolean z = A15.getValue() instanceof Number;
            Object key = A15.getKey();
            if (!z) {
                str = null;
                if (A15.getValue() == null) {
                    A11.put(key, str);
                }
            }
            str = A15.getValue().toString();
            A11.put(key, str);
        }
        return A11;
    }

    public final HashMap A03(Map map) {
        HashMap hashMap;
        HashMap A11 = C12970iu.A11();
        Iterator A0n = C12960it.A0n(map);
        while (A0n.hasNext()) {
            Map.Entry A15 = C12970iu.A15(A0n);
            Object key = A15.getKey();
            String str = null;
            try {
                Map map2 = (Map) A15.getValue();
                if (A15.getValue() != null) {
                    hashMap = A03(map2);
                } else {
                    hashMap = null;
                }
                A11.put(key, hashMap);
            } catch (ClassCastException unused) {
                if (A15.getValue() != null) {
                    str = A15.getValue().toString();
                }
                A11.put(key, str);
            }
        }
        return A11;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r2v1, resolved type: java.lang.String[] */
    /* JADX DEBUG: Multi-variable search result rejected for r0v242, resolved type: boolean */
    /* JADX DEBUG: Multi-variable search result rejected for r8v51, resolved type: boolean */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:176:0x05e0, code lost:
        if (r0 == false) goto L_0x001d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0213, code lost:
        if (r0 != false) goto L_0x01c8;
     */
    @Override // X.AbstractC17450qp
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ java.lang.Object A9j(X.C14220l3 r11, X.C1093651k r12, X.C14240l5 r13) {
        /*
        // Method dump skipped, instructions count: 2090
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C67953Tn.A9j(X.0l3, X.51k, X.0l5):java.lang.Object");
    }
}
