package X;

import android.util.Pair;
import java.io.IOException;
import java.util.List;

/* renamed from: X.3So  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C67713So implements AbstractC28711Os, AnonymousClass5Q3 {
    public AnonymousClass4P0 A00;
    public AnonymousClass1Or A01;
    public final C67613Se A02;
    public final /* synthetic */ C64463Fq A03;

    public C67713So(C67613Se r2, C64463Fq r3) {
        this.A03 = r3;
        this.A01 = r3.A05;
        this.A00 = r3.A04;
        this.A02 = r2;
    }

    public final boolean A00(C28741Ov r8, int i) {
        C28741Ov r2;
        if (r8 != null) {
            C67613Se r6 = this.A02;
            int i2 = 0;
            while (true) {
                List list = r6.A04;
                if (i2 >= list.size()) {
                    return false;
                }
                if (((C28751Ow) list.get(i2)).A03 == r8.A03) {
                    r2 = r8.A01(Pair.create(r6.A03, r8.A04));
                    break;
                }
                i2++;
            }
        } else {
            r2 = null;
        }
        int i3 = i + this.A02.A00;
        AnonymousClass1Or r1 = this.A01;
        if (r1.A00 != i3 || !AnonymousClass3JZ.A0H(r1.A01, r2)) {
            this.A01 = new AnonymousClass1Or(r2, this.A03.A05.A02, i3);
        }
        AnonymousClass4P0 r12 = this.A00;
        if (r12.A00 == i3 && AnonymousClass3JZ.A0H(r12.A01, r2)) {
            return true;
        }
        this.A00 = new AnonymousClass4P0(r2, this.A03.A04.A02, i3);
        return true;
    }

    @Override // X.AbstractC28711Os
    public void APT(C28731Ou r2, C28741Ov r3, int i) {
        if (A00(r3, i)) {
            this.A01.A05(r2);
        }
    }

    @Override // X.AbstractC28711Os
    public void ARu(C28721Ot r2, C28731Ou r3, C28741Ov r4, int i) {
        if (A00(r4, i)) {
            this.A01.A01(r2, r3);
        }
    }

    @Override // X.AbstractC28711Os
    public void ARv(C28721Ot r2, C28731Ou r3, C28741Ov r4, int i) {
        if (A00(r4, i)) {
            this.A01.A02(r2, r3);
        }
    }

    @Override // X.AbstractC28711Os
    public void ARy(C28721Ot r2, C28731Ou r3, C28741Ov r4, IOException iOException, int i, boolean z) {
        if (A00(r4, i)) {
            this.A01.A04(r2, r3, iOException, z);
        }
    }

    @Override // X.AbstractC28711Os
    public void AS4(C28721Ot r2, C28731Ou r3, C28741Ov r4, int i) {
        if (A00(r4, i)) {
            this.A01.A03(r2, r3);
        }
    }
}
