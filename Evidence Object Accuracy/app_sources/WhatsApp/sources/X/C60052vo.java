package X;

import android.content.Context;
import com.google.android.material.chip.Chip;
import com.whatsapp.R;

/* renamed from: X.2vo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60052vo extends AbstractC75703kH {
    public final Chip A00;

    public C60052vo(Chip chip, AnonymousClass2Jw r2) {
        super(chip, r2);
        this.A00 = chip;
    }

    @Override // X.AbstractC75703kH
    public void A08(AnonymousClass4UW r5) {
        Chip chip = this.A00;
        Context context = chip.getContext();
        boolean A00 = r5.A00();
        int i = R.color.search_token_text;
        if (A00) {
            i = R.color.white;
        }
        C12960it.A0s(context, chip, i);
        int i2 = R.color.searchChipBackground;
        if (A00) {
            i2 = R.color.search_input_token;
        }
        chip.setChipBackgroundColorResource(i2);
        int i3 = R.color.search_token_text;
        if (A00) {
            i3 = R.color.white;
        }
        chip.setCloseIconTintResource(i3);
        chip.setCloseIconVisible(false);
        chip.A04.A0C(null);
        chip.setChipIconTintResource(i3);
        chip.setChipIconSize((float) C12960it.A09(chip).getDimensionPixelSize(R.dimen.filter_bar_filter_chip_icon_size));
    }
}
