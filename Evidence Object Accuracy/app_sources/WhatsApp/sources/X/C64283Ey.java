package X;

import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.whatsapp.R;
import java.util.ArrayList;

/* renamed from: X.3Ey  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C64283Ey {
    public View A00;
    public C52992cB A01;
    public C53112cv A02;
    public AnonymousClass5X0 A03;
    public C61122zR A04;
    public C61132zS A05;
    public ArrayList A06;
    public boolean A07;
    public boolean A08;
    public final ViewGroup A09;
    public final ViewGroup A0A;
    public final ListView A0B;
    public final ActivityC000800j A0C;
    public final C15400n6 A0D;
    public final AbstractC13860kS A0E;
    public final C15570nT A0F;
    public final C15450nH A0G;
    public final C238013b A0H;
    public final C64433Fn A0I;
    public final C19990v2 A0J;
    public final C21250x7 A0K;
    public final C20510vs A0L;
    public final C14850m9 A0M;
    public final C20710wC A0N;
    public final AbstractC14640lm A0O;
    public final AnonymousClass3D0 A0P;

    public C64283Ey(ViewGroup viewGroup, ListView listView, ActivityC000800j r5, C15400n6 r6, AbstractC13860kS r7, C15570nT r8, C15450nH r9, C238013b r10, C15550nR r11, C64433Fn r12, C16370ot r13, C19990v2 r14, C21250x7 r15, C20510vs r16, C14850m9 r17, C20710wC r18, AbstractC14640lm r19) {
        this.A0M = r17;
        this.A0C = r5;
        this.A0F = r8;
        this.A0J = r14;
        this.A0G = r9;
        this.A0K = r15;
        this.A0H = r10;
        this.A0N = r18;
        this.A0L = r16;
        this.A0I = r12;
        this.A0E = r7;
        this.A0D = r6;
        this.A0O = r19;
        this.A0B = listView;
        this.A0A = viewGroup;
        this.A0P = new AnonymousClass3D0(r8, r11, r13, r16);
        ViewGroup viewGroup2 = (ViewGroup) C12960it.A0F(r5.getLayoutInflater(), listView, R.layout.conversation_header);
        this.A09 = viewGroup2;
        this.A00 = viewGroup2.findViewById(R.id.progress);
        listView.addHeaderView(viewGroup2);
    }

    public final void A00(C15370n3 r4, boolean z) {
        AnonymousClass5X0 r0;
        C64433Fn r2 = this.A0I;
        r2.A00 = r4;
        r2.A01 = z;
        if (this.A03 == null) {
            boolean A07 = this.A0M.A07(412);
            ActivityC000800j r1 = this.A0C;
            if (A07) {
                r0 = new C61162zV(r1);
            } else {
                r0 = new C61142zT(r1);
            }
            this.A03 = r0;
            r0.setup(r2);
            AnonymousClass5X0 r22 = this.A03;
            if (r22 instanceof C61142zT) {
                this.A0B.addFooterView((View) r22);
            } else if (r22 instanceof C61162zV) {
                ViewGroup viewGroup = this.A0A;
                viewGroup.setVisibility(0);
                viewGroup.addView((View) r22);
            }
        }
    }

    public void A01(boolean z, int i) {
        TextView textView;
        int i2;
        if (this.A02 == null) {
            this.A02 = new C53112cv(this.A0C);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
            layoutParams.gravity = 17;
            this.A09.addView(this.A02, layoutParams);
        }
        this.A00.setVisibility(C12960it.A02(z ? 1 : 0));
        if (z || i == 0) {
            this.A02.setVisibility(8);
            return;
        }
        this.A02.setVisibility(0);
        C53112cv r1 = this.A02;
        if (i == 1) {
            r1.A00.setVisibility(0);
            textView = r1.A01;
            i2 = R.string.chat_history_sync_in_progress;
        } else if (i == 2) {
            r1.A00.setVisibility(8);
            textView = r1.A01;
            i2 = R.string.chat_history_sync_complete_but_more_messages_on_primary;
        } else {
            return;
        }
        textView.setText(i2);
    }
}
