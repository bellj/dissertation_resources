package X;

import java.security.Signature;
import javax.crypto.Cipher;
import javax.crypto.Mac;

/* renamed from: X.04B  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass04B {
    public final Signature A00;
    public final Cipher A01;
    public final Mac A02;

    public AnonymousClass04B(Signature signature) {
        this.A00 = signature;
        this.A01 = null;
        this.A02 = null;
    }

    public AnonymousClass04B(Cipher cipher) {
        this.A01 = cipher;
        this.A00 = null;
        this.A02 = null;
    }

    public AnonymousClass04B(Mac mac) {
        this.A02 = mac;
        this.A01 = null;
        this.A00 = null;
    }
}
