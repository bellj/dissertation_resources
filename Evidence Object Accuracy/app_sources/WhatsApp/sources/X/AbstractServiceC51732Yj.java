package X;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import com.facebook.redex.RunnableBRunnable0Shape3S0300000_I1;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* renamed from: X.2Yj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractServiceC51732Yj extends Service {
    public int A00;
    public int A01;
    public Binder A02;
    public final Object A03;
    public final ExecutorService A04;

    public abstract void A02(Intent intent);

    public AbstractServiceC51732Yj() {
        String str;
        String A0r = C12980iv.A0r(this);
        if (A0r.length() != 0) {
            str = "Firebase-".concat(A0r);
        } else {
            str = new String("Firebase-");
        }
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(1, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), new ThreadFactoryC16540pC(str));
        threadPoolExecutor.allowCoreThreadTimeOut(true);
        this.A04 = Executors.unconfigurableExecutorService(threadPoolExecutor);
        this.A03 = C12970iu.A0l();
        this.A01 = 0;
    }

    public final C13600jz A00(Intent intent) {
        if ("com.google.firebase.messaging.NOTIFICATION_OPEN".equals(intent.getAction())) {
            PendingIntent pendingIntent = (PendingIntent) intent.getParcelableExtra("pending_intent");
            if (pendingIntent != null) {
                try {
                    pendingIntent.send();
                } catch (PendingIntent.CanceledException unused) {
                    Log.e("FirebaseMessaging", "Notification pending intent canceled");
                }
            }
            if (AnonymousClass3G4.A01(intent)) {
                if ("1".equals(intent.getStringExtra("google.c.a.tc"))) {
                    C13030j1 A00 = C13030j1.A00();
                    A00.A02();
                    A00.A02.A02(AnonymousClass5R9.class);
                    if (Log.isLoggable("FirebaseMessaging", 3)) {
                        Log.d("FirebaseMessaging", "Received event with track-conversion=true. Setting user property and reengagement event");
                    }
                    Log.w("FirebaseMessaging", "Unable to set user property for conversion tracking:  analytics library is missing");
                } else if (Log.isLoggable("FirebaseMessaging", 3)) {
                    Log.d("FirebaseMessaging", "Received event with track-conversion=false. Do not set user property");
                }
                AnonymousClass3G4.A00(intent, "_no");
            }
            C13600jz r0 = new C13600jz();
            r0.A08(null);
            return r0;
        }
        C13690kA r3 = new C13690kA();
        this.A04.execute(new RunnableBRunnable0Shape3S0300000_I1(this, intent, r3, 4));
        return r3.A00;
    }

    public final void A01(Intent intent) {
        if (intent != null) {
            synchronized (AnonymousClass4HQ.A02) {
                if (AnonymousClass4HQ.A00 != null && intent.getBooleanExtra("com.google.firebase.iid.WakeLockHolder.wakefulintent", false)) {
                    intent.putExtra("com.google.firebase.iid.WakeLockHolder.wakefulintent", false);
                    AnonymousClass4HQ.A00.A00();
                }
            }
        }
        synchronized (this.A03) {
            int i = this.A01 - 1;
            this.A01 = i;
            if (i == 0) {
                stopSelfResult(this.A00);
            }
        }
    }

    @Override // android.app.Service
    public final synchronized IBinder onBind(Intent intent) {
        Binder binder;
        if (Log.isLoggable("EnhancedIntentService", 3)) {
            Log.d("EnhancedIntentService", "Service received bind request");
        }
        binder = this.A02;
        if (binder == null) {
            binder = new BinderC73183fo(new C88994Ig(this));
            this.A02 = binder;
        }
        return binder;
    }

    @Override // android.app.Service
    public void onDestroy() {
        this.A04.shutdown();
        super.onDestroy();
    }

    @Override // android.app.Service
    public final int onStartCommand(Intent intent, int i, int i2) {
        synchronized (this.A03) {
            this.A00 = i2;
            this.A01++;
        }
        Intent intent2 = (Intent) C16060oN.A00().A03.poll();
        if (intent2 != null) {
            C13600jz A00 = A00(intent2);
            if (!A00.A09()) {
                A00.A03.A00(new AnonymousClass514(new AbstractC115775Sw(intent, this) { // from class: X.50t
                    public final Intent A00;
                    public final AbstractServiceC51732Yj A01;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                    }

                    @Override // X.AbstractC115775Sw
                    public final void AOR(C13600jz r3) {
                        this.A01.A01(this.A00);
                    }
                }, AnonymousClass5E7.A00));
                A00.A04();
                return 3;
            }
        }
        A01(intent);
        return 2;
    }
}
