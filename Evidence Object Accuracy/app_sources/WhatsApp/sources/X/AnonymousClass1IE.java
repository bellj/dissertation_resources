package X;

import java.util.Set;

/* renamed from: X.1IE  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1IE extends AnonymousClass1IF {
    public final /* synthetic */ AnonymousClass168 A00;
    public final /* synthetic */ Runnable A01;
    public final /* synthetic */ String A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass1IE(AnonymousClass168 r1, Runnable runnable, String str, String str2) {
        super(str);
        this.A00 = r1;
        this.A01 = runnable;
        this.A02 = str2;
    }

    @Override // java.lang.Runnable
    public void run() {
        try {
            this.A01.run();
            Set set = this.A00.A02;
            synchronized (set) {
                try {
                    set.remove(this.A02);
                } catch (Throwable th) {
                    throw th;
                }
            }
        } catch (Throwable th2) {
            Set set2 = this.A00.A02;
            synchronized (set2) {
                try {
                    set2.remove(this.A02);
                    throw th2;
                } catch (Throwable th3) {
                    throw th3;
                }
            }
        }
    }
}
