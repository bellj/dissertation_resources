package X;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.redex.RunnableBRunnable0Shape15S0100000_I1_1;
import com.facebook.redex.RunnableBRunnable0Shape8S0100000_I0_8;
import com.whatsapp.R;
import com.whatsapp.mediacomposer.MediaComposerActivity;
import com.whatsapp.mediacomposer.MediaComposerFragment;
import com.whatsapp.mediacomposer.bottombar.BottomBarView;
import com.whatsapp.mediacomposer.bottombar.caption.CaptionView;
import com.whatsapp.mentions.MentionableEntry;
import java.util.Collection;

/* renamed from: X.3YE  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3YE implements AnonymousClass2Aj, AnonymousClass21x, AbstractC33441e5, AnonymousClass2GO {
    public AbstractC453021a A00;
    public AbstractC453121b A01;
    public final Context A02;
    public final C453321d A03;
    public final BottomBarView A04;
    public final AnonymousClass3DD A05;
    public final C90404Nt A06;
    public final C63763Cv A07;
    public final AnonymousClass3F8 A08;
    public final C455021w A09;
    public final boolean A0A;

    @Override // X.AnonymousClass2Aj, X.AbstractC47372Ak
    public /* synthetic */ void onDismiss() {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x006c, code lost:
        if (X.C12980iv.A0z(r3).isEmpty() == false) goto L_0x006e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass3YE(X.C15610nY r13, X.C453321d r14, com.whatsapp.mediacomposer.bottombar.BottomBarView r15, X.AnonymousClass3DD r16, X.C90404Nt r17, X.C63763Cv r18, X.AnonymousClass3F8 r19, X.C455021w r20, boolean r21) {
        /*
            r12 = this;
            r12.<init>()
            android.content.Context r0 = r15.getContext()
            r12.A02 = r0
            r12.A04 = r15
            r12.A03 = r14
            r1 = r16
            r12.A05 = r1
            r6 = r18
            r12.A07 = r6
            r0 = r17
            r12.A06 = r0
            r5 = r20
            r12.A09 = r5
            r2 = r19
            r12.A08 = r2
            r4 = r21
            r12.A0A = r4
            X.016 r3 = r14.A00
            java.util.List r9 = X.C12980iv.A0z(r3)
            java.util.List r0 = X.C12980iv.A0z(r3)
            boolean r10 = X.C15380n4.A0P(r0)
            X.016 r0 = r14.A03
            java.lang.Object r8 = r0.A01()
            X.1ce r8 = (X.C32731ce) r8
            r11 = 1
            r7 = r13
            r6.A00(r7, r8, r9, r10, r11)
            com.whatsapp.mediacomposer.bottombar.caption.CaptionView r7 = r1.A04
            com.whatsapp.mentions.MentionableEntry r1 = r7.A0B
            r0 = 33554432(0x2000000, float:9.403955E-38)
            r1.setScrollBarStyle(r0)
            r6 = 0
            r1.setClickable(r6)
            r1.setCursorVisible(r6)
            r1.setFocusable(r6)
            r1.setFocusableInTouchMode(r6)
            r0 = 2
            r1.setImportantForAccessibility(r0)
            com.whatsapp.WaImageButton r0 = r7.A09
            r0.setVisibility(r6)
            boolean r0 = r14.A0B
            if (r0 == 0) goto L_0x006e
            java.util.List r0 = X.C12980iv.A0z(r3)
            boolean r1 = r0.isEmpty()
            r0 = 1
            if (r1 != 0) goto L_0x006f
        L_0x006e:
            r0 = 0
        L_0x006f:
            r2.A00(r0)
            androidx.recyclerview.widget.RecyclerView r2 = r5.A06
            X.018 r1 = r5.A07
            X.2gy r0 = new X.2gy
            r0.<init>(r1)
            r2.A0l(r0)
            androidx.recyclerview.widget.LinearLayoutManager r0 = new androidx.recyclerview.widget.LinearLayoutManager
            r0.<init>()
            r0.A1Q(r6)
            r2.setLayoutManager(r0)
            if (r21 == 0) goto L_0x00a8
            java.util.List r0 = X.C12980iv.A0z(r3)
            boolean r0 = r0.isEmpty()
            r2 = r0 ^ 1
            X.3DD r0 = r12.A05
            com.whatsapp.mediacomposer.bottombar.caption.CaptionView r1 = r0.A04
            r1.getContext()
            X.018 r0 = r1.A00
            if (r2 == 0) goto L_0x00a9
            X.C92974Yj.A00(r1, r0)
        L_0x00a3:
            X.3F8 r0 = r12.A08
            r0.A01(r2)
        L_0x00a8:
            return
        L_0x00a9:
            X.C92974Yj.A01(r1, r0)
            goto L_0x00a3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3YE.<init>(X.0nY, X.21d, com.whatsapp.mediacomposer.bottombar.BottomBarView, X.3DD, X.4Nt, X.3Cv, X.3F8, X.21w, boolean):void");
    }

    public void A00(CharSequence charSequence, boolean z) {
        CharSequence charSequence2;
        AnonymousClass3DD r5 = this.A05;
        if (TextUtils.isEmpty(charSequence)) {
            CaptionView captionView = r5.A04;
            captionView.setCaptionText(null);
            C12960it.A0r(r5.A00, captionView, R.string.add_caption);
            return;
        }
        if (z) {
            AnonymousClass01d r4 = r5.A01;
            C16630pM r3 = r5.A05;
            MentionableEntry mentionableEntry = r5.A04.A0B;
            charSequence2 = AbstractC36671kL.A03(r5.A00, mentionableEntry.getPaint(), r5.A03, C42971wC.A04(r4, r3, charSequence, mentionableEntry.getCurrentTextColor(), true));
        } else {
            charSequence2 = charSequence;
        }
        CaptionView captionView2 = r5.A04;
        captionView2.setCaptionText(charSequence2);
        captionView2.setContentDescription(charSequence);
    }

    public void A01(boolean z) {
        if (z) {
            C455021w r3 = this.A09;
            r3.A06.animate().alpha(1.0f).withStartAction(new RunnableBRunnable0Shape8S0100000_I0_8(r3, 8));
        }
        BottomBarView bottomBarView = this.A04;
        bottomBarView.animate().alpha(1.0f).withStartAction(new RunnableBRunnable0Shape15S0100000_I1_1(bottomBarView, 45));
    }

    public void A02(boolean z) {
        if (z) {
            C455021w r3 = this.A09;
            r3.A06.animate().alpha(0.0f).withEndAction(new RunnableBRunnable0Shape8S0100000_I0_8(r3, 7));
        }
        BottomBarView bottomBarView = this.A04;
        bottomBarView.animate().alpha(0.0f).withEndAction(new RunnableBRunnable0Shape15S0100000_I1_1(bottomBarView, 46));
    }

    public void A03(boolean z) {
        this.A08.A01.setClickable(z);
        CaptionView captionView = this.A05.A04;
        captionView.setClickable(z);
        captionView.setAddButtonClickable(z);
        captionView.setViewOnceButtonClickable(z);
    }

    public void A04(boolean z) {
        this.A04.setVisibility(0);
        C455021w r0 = this.A09;
        r0.A06.setVisibility(C12960it.A02(z ? 1 : 0));
    }

    @Override // X.AnonymousClass2Aj
    public void ALw() {
        MediaComposerActivity mediaComposerActivity = (MediaComposerActivity) this.A00;
        MediaComposerFragment A2g = mediaComposerActivity.A2g();
        if (A2g == null || !A2g.A1G()) {
            mediaComposerActivity.A2k();
            mediaComposerActivity.A2l();
            Intent A0A = C12970iu.A0A();
            A0A.putParcelableArrayListExtra("android.intent.extra.STREAM", C12980iv.A0x((Collection) mediaComposerActivity.A0b.A02.A01()));
            if (mediaComposerActivity.A11) {
                A0A.putStringArrayListExtra("jids", C15380n4.A06((Collection) mediaComposerActivity.A0b.A00.A01()));
            }
            C453421e r0 = mediaComposerActivity.A1B;
            Bundle A0D = C12970iu.A0D();
            r0.A02(A0D);
            A0A.putExtra("media_preview_params", A0D);
            mediaComposerActivity.A00 = 1;
            mediaComposerActivity.setResult(1, A0A);
            mediaComposerActivity.finish();
        }
    }

    @Override // X.AbstractC33441e5
    public void AUk(boolean z) {
        AbstractC453021a r1 = this.A00;
        if (r1 != null) {
            MediaComposerActivity mediaComposerActivity = (MediaComposerActivity) r1;
            mediaComposerActivity.A10 = true;
            mediaComposerActivity.A2u(z);
        }
    }

    @Override // X.AnonymousClass2GO
    public void AVo() {
        MediaComposerActivity mediaComposerActivity = (MediaComposerActivity) this.A00;
        mediaComposerActivity.A0v.get();
        mediaComposerActivity.A2n();
    }

    @Override // X.AnonymousClass21x
    public void AXU(int i) {
        MediaComposerActivity mediaComposerActivity = (MediaComposerActivity) this.A01;
        mediaComposerActivity.A0W.setCurrentItem(mediaComposerActivity.A0d.A0L(i));
    }

    @Override // X.AnonymousClass2Aj
    public void AYN() {
        C453321d r3 = this.A03;
        int A05 = C12960it.A05(r3.A05.A01());
        if (A05 == 2) {
            r3.A06(3);
        } else if (A05 == 3) {
            r3.A06(2);
        }
    }
}
