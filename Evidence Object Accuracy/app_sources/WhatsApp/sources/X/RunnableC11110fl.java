package X;

/* renamed from: X.0fl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class RunnableC11110fl extends AbstractC10990fX implements Runnable, AbstractC11610gZ {
    public final int A00;
    public final AbstractC10990fX A01;
    public final C94304ba A02;
    public final /* synthetic */ AbstractC11610gZ A03;
    public volatile int runningWorkers;

    public RunnableC11110fl(AbstractC10990fX r2, int i) {
        AbstractC11610gZ A00;
        this.A01 = r2;
        this.A00 = i;
        this.A03 = (!(r2 instanceof AbstractC11610gZ) || (A00 = (AbstractC11610gZ) r2) == null) ? AnonymousClass4ZI.A00() : A00;
        this.A02 = new C94304ba();
    }

    @Override // X.AbstractC10990fX
    public AbstractC10990fX A02(int i) {
        C88264Ex.A00(i);
        if (i >= this.A00) {
            return this;
        }
        return super.A02(i);
    }

    @Override // X.AbstractC10990fX
    public void A04(Runnable runnable, AnonymousClass5X4 r5) {
        boolean z;
        if (!A05(runnable)) {
            synchronized (this) {
                if (this.runningWorkers >= this.A00) {
                    z = false;
                } else {
                    z = true;
                    this.runningWorkers++;
                }
            }
            if (z) {
                this.A01.A04(this, this);
            }
        }
    }

    public final boolean A05(Runnable runnable) {
        this.A02.A02(runnable);
        return this.runningWorkers >= this.A00;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0027, code lost:
        monitor-enter(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0028, code lost:
        r3.runningWorkers--;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0032, code lost:
        if (r1.A00() != 0) goto L_0x0035;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0035, code lost:
        r3.runningWorkers++;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003d, code lost:
        monitor-exit(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x003e, code lost:
        return;
     */
    @Override // java.lang.Runnable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r3 = this;
        L_0x0000:
            r2 = 0
        L_0x0001:
            X.4ba r1 = r3.A02
            java.lang.Object r0 = r1.A01()
            java.lang.Runnable r0 = (java.lang.Runnable) r0
            if (r0 == 0) goto L_0x0027
            r0.run()     // Catch: all -> 0x000f
            goto L_0x0015
        L_0x000f:
            r1 = move-exception
            X.5Er r0 = X.C112775Er.A00
            X.C88234Eu.A00(r0, r1)
        L_0x0015:
            int r2 = r2 + 1
            r0 = 16
            if (r2 < r0) goto L_0x0001
            X.0fX r1 = r3.A01
            boolean r0 = r1.A03(r3)
            if (r0 == 0) goto L_0x0001
            r1.A04(r3, r3)
            return
        L_0x0027:
            monitor-enter(r3)
            int r0 = r3.runningWorkers     // Catch: all -> 0x003f
            int r0 = r0 + -1
            r3.runningWorkers = r0     // Catch: all -> 0x003f
            int r0 = r1.A00()     // Catch: all -> 0x003f
            if (r0 != 0) goto L_0x0035
            goto L_0x003d
        L_0x0035:
            int r0 = r3.runningWorkers     // Catch: all -> 0x003f
            int r0 = r0 + 1
            r3.runningWorkers = r0     // Catch: all -> 0x003f
            monitor-exit(r3)
            goto L_0x0000
        L_0x003d:
            monitor-exit(r3)
            return
        L_0x003f:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.RunnableC11110fl.run():void");
    }
}
