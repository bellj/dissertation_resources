package X;

import com.facebook.simplejni.NativeHolder;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.jobqueue.job.E2eMessageEncryptor$EncryptionFailException;
import com.whatsapp.jobqueue.job.E2eMessageEncryptor$UnrecoverableErrorException;
import com.whatsapp.util.Log;
import com.whatsapp.wamsys.JniBridge;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.3He  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64863He {
    public final int A00;
    public final int A01;
    public final AbstractC15710nm A02;
    public final C15570nT A03;
    public final C20870wS A04;
    public final C15990oG A05;
    public final C18240s8 A06;
    public final C15600nX A07;
    public final C22830zi A08;
    public final C22130yZ A09;
    public final C27061Fw A0A;
    public final C14850m9 A0B;
    public final Jid A0C;
    public final C63843Dd A0D;
    public final AnonymousClass3IO A0E;
    public final AnonymousClass2Ci A0F;
    public final C27081Fy A0G;
    public final AbstractC15340mz A0H;
    public final AnonymousClass1IS A0I;
    public final String A0J;
    public final String A0K;
    public final String A0L;
    public final Map A0M;
    public final byte[] A0N;

    public C64863He(AbstractC15710nm r3, C15570nT r4, C20870wS r5, C15990oG r6, C18240s8 r7, C15600nX r8, C22830zi r9, C22130yZ r10, C27061Fw r11, C14850m9 r12, C63843Dd r13, AnonymousClass3IO r14, AnonymousClass2Ci r15, C27081Fy r16, AbstractC15340mz r17, AnonymousClass1IS r18, String str, String str2, String str3, Map map, byte[] bArr, int i, int i2) {
        this.A0B = r12;
        this.A02 = r3;
        this.A03 = r4;
        this.A04 = r5;
        this.A06 = r7;
        this.A0E = r14;
        this.A05 = r6;
        this.A08 = r9;
        this.A09 = r10;
        this.A0A = r11;
        this.A07 = r8;
        this.A0F = r15;
        this.A0C = Jid.getNullable(str);
        this.A0L = str;
        this.A0K = str2;
        this.A0I = r18;
        this.A0H = r17;
        this.A0G = r16;
        this.A0N = bArr;
        this.A0M = map;
        this.A0J = str3;
        this.A0D = r13;
        this.A01 = i;
        this.A00 = i2;
    }

    public static C15930o9 A00(C31801b8 r4) {
        if (r4.A01 != 0) {
            return null;
        }
        return new C15930o9(r4.A02, 2, AnonymousClass4EJ.A00(r4.A00));
    }

    public final C31801b8 A01(DeviceJid deviceJid, C27081Fy r11, AbstractC15340mz r12) {
        int A01;
        C31801b8 A0B = this.A05.A0B(C15940oB.A02(deviceJid), r11.A02());
        C20870wS r1 = this.A04;
        Jid jid = this.A0C;
        int i = this.A01;
        if (A0B.A01 == 0) {
            A01 = 0;
        } else {
            A01 = this.A0D.A01(deviceJid) + 1;
        }
        r1.A09(A0B, deviceJid, jid, r12, i, A01, this.A00);
        return A0B;
    }

    public C27081Fy A02(C31381aS r5, C41961uR r6, String str) {
        byte[] bArr = r5.A01;
        C57242mi r0 = ((C27081Fy) C27081Fy.A00().A00).A0b;
        if (r0 == null) {
            r0 = C57242mi.A03;
        }
        C56892m7 r2 = (C56892m7) r0.A0T();
        r2.A06(this.A0L);
        r2.A05(AbstractC27881Jp.A01(bArr, 0, bArr.length));
        AnonymousClass1G3 A00 = C27081Fy.A00();
        A00.A0A(r2);
        C32401c6.A0C(r6, A00);
        C27081Fy r1 = (C27081Fy) A00.A02();
        return str != null ? AnonymousClass3IO.A01(r1, null, str) : r1;
    }

    public final String A03(Collection collection) {
        HashSet A12 = C12970iu.A12();
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            UserJid userJid = ((DeviceJid) it.next()).getUserJid();
            if (!this.A03.A0F(userJid)) {
                A12.add(userJid.getPrimaryDevice());
            }
        }
        return AnonymousClass1YK.A00(A12);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0053, code lost:
        if (r6 != false) goto L_0x0055;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.Map A04(java.util.Map r19) {
        /*
            r18 = this;
            java.util.TreeMap r3 = new java.util.TreeMap
            r3.<init>()
            java.util.HashMap r6 = X.C12970iu.A11()
            r4 = r18
            X.0s8 r2 = r4.A06
            r0 = 26
            r5 = r19
            com.facebook.redex.RunnableBRunnable0Shape3S0300000_I1 r1 = new com.facebook.redex.RunnableBRunnable0Shape3S0300000_I1
            r1.<init>(r4, r5, r6, r0)
            java.util.concurrent.ThreadPoolExecutor r0 = r2.A00
            java.util.concurrent.Future r0 = r0.submit(r1)
            r0.get()
            java.util.Iterator r9 = X.C12990iw.A0s(r6)
            r2 = 0
            r1 = r2
        L_0x0025:
            boolean r0 = r9.hasNext()
            if (r0 == 0) goto L_0x009a
            java.util.Map$Entry r6 = X.C12970iu.A15(r9)
            java.lang.Object r0 = r6.getKey()
            X.0oC r0 = (X.C15950oC) r0
            com.whatsapp.jid.DeviceJid r12 = X.C15940oB.A03(r0)
            X.AnonymousClass009.A05(r12)
            java.lang.Object r11 = r6.getValue()
            X.1b8 r11 = (X.C31801b8) r11
            int r8 = r11.A01
            r7 = 1
            if (r8 == 0) goto L_0x0098
            if (r1 != 0) goto L_0x004f
            X.2Ci r0 = r4.A0F
            java.util.Collection r1 = r0.A00()
        L_0x004f:
            boolean r6 = r1.contains(r12)
            if (r6 == 0) goto L_0x0068
        L_0x0055:
            X.0wS r10 = r4.A04
            X.0mz r14 = r4.A0H
            com.whatsapp.jid.Jid r13 = r4.A0C
            int r15 = r4.A01
            if (r8 != 0) goto L_0x008f
            r16 = 0
        L_0x0061:
            int r0 = r4.A00
            r17 = r0
            r10.A09(r11, r12, r13, r14, r15, r16, r17)
        L_0x0068:
            X.0o9 r0 = A00(r11)
            if (r0 == 0) goto L_0x0072
            r3.put(r12, r0)
            goto L_0x0025
        L_0x0072:
            byte r0 = r12.device
            if (r0 != 0) goto L_0x0084
            if (r6 == 0) goto L_0x0084
            X.3Dd r0 = r4.A0D
            int r1 = r0.A00(r12)
            com.whatsapp.jobqueue.job.E2eMessageEncryptor$EncryptionFailException r0 = new com.whatsapp.jobqueue.job.E2eMessageEncryptor$EncryptionFailException
            r0.<init>(r12, r1)
            throw r0
        L_0x0084:
            java.lang.String r0 = "sende2emessagejob/encryptMessages/dropping message due to encryption failure for "
            java.lang.String r0 = X.C12960it.A0b(r0, r12)
            com.whatsapp.util.Log.e(r0)
            goto L_0x0025
        L_0x008f:
            X.3Dd r0 = r4.A0D
            int r16 = r0.A01(r12)
            int r16 = r16 + r7
            goto L_0x0061
        L_0x0098:
            r6 = 1
            goto L_0x0055
        L_0x009a:
            boolean r0 = r5.isEmpty()
            if (r0 != 0) goto L_0x00b3
            boolean r0 = r3.isEmpty()
            if (r0 == 0) goto L_0x00b3
            java.lang.String r0 = "sende2emessagejob/encryptMessages/no encrypted messages due to encryption failures"
            com.whatsapp.util.Log.e(r0)
            r1 = 4
            com.whatsapp.jobqueue.job.E2eMessageEncryptor$EncryptionFailException r0 = new com.whatsapp.jobqueue.job.E2eMessageEncryptor$EncryptionFailException
            r0.<init>(r2, r1)
            throw r0
        L_0x00b3:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C64863He.A04(java.util.Map):java.util.Map");
    }

    public final void A05(C29901Ve r21, Collection collection, Map map) {
        C15570nT r6 = this.A03;
        r6.A08();
        C27631Ih r9 = r6.A05;
        AnonymousClass009.A05(r9);
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            DeviceJid deviceJid = (DeviceJid) it.next();
            if (!map.containsKey(deviceJid)) {
                Object obj = null;
                if (!r6.A0E(deviceJid)) {
                    UserJid userJid = deviceJid.getUserJid();
                    AnonymousClass1PG r0 = (AnonymousClass1PG) this.A0M.get(userJid.getPrimaryDevice().getRawString());
                    if (r0 != null) {
                        byte[] bArr = this.A0N;
                        int i = r0.expiration;
                        obj = JniBridge.jvidispatchOOOOOOO(0, userJid.getRawString(), r9.getRawString(), null, new AnonymousClass4I6((NativeHolder) JniBridge.jvidispatchOII((long) i, C12980iv.A0D(r0.ephemeralSettingTimestamp))).A00, bArr, r21.getRawString());
                        if (obj == null) {
                            Log.e("sende2emessagejob/failed to encrypt broadcast setting");
                            throw new E2eMessageEncryptor$EncryptionFailException(deviceJid, this.A0D.A00(deviceJid));
                        }
                    } else {
                        Log.e("sende2emessagejob/missing broadcast setting");
                        throw new E2eMessageEncryptor$UnrecoverableErrorException(deviceJid);
                    }
                }
                map.put(deviceJid, obj);
            }
        }
    }
}
