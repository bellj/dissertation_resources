package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RectF;
import android.text.TextUtils;
import android.util.Base64;
import com.whatsapp.InteractiveAnnotation;
import com.whatsapp.SerializablePoint;
import com.whatsapp.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.3JD  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3JD {
    public int A00;
    public int A01;
    public int A02;
    public RectF A03;
    public RectF A04;
    public String A05;
    public List A06;

    public AnonymousClass3JD(RectF rectF, RectF rectF2, List list, int i) {
        this.A04 = rectF;
        this.A03 = rectF2;
        this.A06 = list;
        this.A02 = i;
    }

    public static Matrix A00(float f, float f2, float f3, float f4, float f5, boolean z, boolean z2) {
        float f6;
        float f7;
        float f8;
        Matrix A01 = C13000ix.A01();
        float f9 = f3 % 180.0f;
        if (f9 == 90.0f) {
            f6 = f2 / f4;
            f7 = f / f5;
        } else {
            f6 = f / f4;
            f7 = f2 / f5;
        }
        A01.preScale(f6, f7);
        float f10 = 0.0f;
        if (z) {
            Matrix A012 = C13000ix.A01();
            A012.setValues(new float[]{-1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f});
            A01.preConcat(A012);
            float f11 = f4;
            if (f9 == 90.0f) {
                f11 = f5;
            }
            A01.preTranslate(-f11, 0.0f);
        }
        if (z2) {
            Matrix A013 = C13000ix.A01();
            A013.setValues(new float[]{1.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f});
            A01.preConcat(A013);
            float f12 = f5;
            if (f9 == 90.0f) {
                f12 = f4;
            }
            A01.preTranslate(-f12, 0.0f);
        }
        A01.preRotate(f3);
        if (f3 == 90.0f) {
            A01.preTranslate(0.0f, -f5);
        } else {
            if (f3 == 180.0f) {
                f8 = -f4;
                f10 = -f5;
            } else {
                f8 = -f4;
                if (f3 != 270.0f) {
                    if (f3 != 0.0f) {
                        throw new IllegalArgumentException();
                    }
                }
            }
            A01.preTranslate(f8, f10);
            return A01;
        }
        return A01;
    }

    public static AnonymousClass3JD A01(Context context, AnonymousClass018 r4, AnonymousClass19M r5, AnonymousClass1AB r6, File file) {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        try {
            StringBuilder A0h = C12960it.A0h();
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    A0h.append(readLine);
                } else {
                    AnonymousClass3JD A03 = A03(context, r4, r5, r6, A0h.toString());
                    bufferedReader.close();
                    return A03;
                }
            }
        } catch (Throwable th) {
            try {
                bufferedReader.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public static AnonymousClass3JD A02(Context context, AnonymousClass018 r1, AnonymousClass19M r2, AnonymousClass1AB r3, File file) {
        try {
            return A01(context, r1, r2, r3, file);
        } catch (IOException e) {
            Log.e("Doodle/safeLoad could not load doodle from file", e);
            return null;
        }
    }

    public static AnonymousClass3JD A03(Context context, AnonymousClass018 r13, AnonymousClass19M r14, AnonymousClass1AB r15, String str) {
        JSONObject jSONObject;
        AbstractC454821u r10;
        if (!TextUtils.isEmpty(str)) {
            try {
                JSONObject A05 = C13000ix.A05(str);
                RectF A0K = C12980iv.A0K();
                A0K.left = ((float) A05.getInt("l")) / 100.0f;
                A0K.top = ((float) A05.getInt("t")) / 100.0f;
                A0K.right = ((float) A05.getInt("r")) / 100.0f;
                A0K.bottom = ((float) A05.getInt("b")) / 100.0f;
                RectF A0K2 = C12980iv.A0K();
                A0K2.left = ((float) A05.getInt("crop-l")) / 100.0f;
                A0K2.top = ((float) A05.getInt("crop-t")) / 100.0f;
                A0K2.right = ((float) A05.getInt("crop-r")) / 100.0f;
                A0K2.bottom = ((float) A05.getInt("crop-b")) / 100.0f;
                int i = A05.getInt("rotate");
                JSONArray jSONArray = A05.getJSONArray("shapes");
                ArrayList A0l = C12960it.A0l();
                for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                    JSONObject jSONObject2 = jSONArray.getJSONObject(i2);
                    String string = jSONObject2.getString("type");
                    switch (string.hashCode()) {
                        case -2069773495:
                            if (string.equals("thinking-bubble")) {
                                r10 = new AnonymousClass33K(jSONObject2);
                                break;
                            } else {
                                continue;
                            }
                        case -1890252483:
                            if (string.equals("sticker")) {
                                r10 = new AnonymousClass33D(context, r15, jSONObject2);
                                break;
                            } else {
                                continue;
                            }
                        case -1676415975:
                            if (string.equals("digital-clock")) {
                                r10 = new AnonymousClass33C(context, r13, jSONObject2);
                                break;
                            } else {
                                continue;
                            }
                        case -1493474063:
                            if (string.equals("analog-clock")) {
                                r10 = new AnonymousClass33M(context, r13, jSONObject2);
                                break;
                            } else {
                                continue;
                            }
                        case -841189240:
                            if (string.equals("speech-bubble-oval")) {
                                r10 = new AnonymousClass33H(jSONObject2);
                                break;
                            } else {
                                continue;
                            }
                        case -841116134:
                            if (string.equals("speech-bubble-rect")) {
                                r10 = new AnonymousClass33I(jSONObject2);
                                break;
                            } else {
                                continue;
                            }
                        case 110873:
                            if (string.equals("pen")) {
                                r10 = new AnonymousClass33J(jSONObject2, C12960it.A01(context));
                                break;
                            } else {
                                continue;
                            }
                        case 3423314:
                            if (string.equals("oval")) {
                                r10 = new AnonymousClass33F(jSONObject2);
                                break;
                            } else {
                                continue;
                            }
                        case 3496420:
                            if (string.equals("rect")) {
                                r10 = new AnonymousClass33G(jSONObject2);
                                break;
                            } else {
                                continue;
                            }
                        case 3556653:
                            if (string.equals("text")) {
                                r10 = new AnonymousClass33L(context, r13, r14, jSONObject2);
                                break;
                            } else {
                                continue;
                            }
                        case 93090825:
                            if (string.equals("arrow")) {
                                r10 = new AnonymousClass33E(jSONObject2);
                                break;
                            } else {
                                continue;
                            }
                        case 96632902:
                            if (string.equals("emoji")) {
                                r10 = new AnonymousClass33B(context, r14, jSONObject2);
                                break;
                            } else {
                                continue;
                            }
                        case 1901043637:
                            if (string.equals("location")) {
                                r10 = new AnonymousClass33N(context, r13, jSONObject2);
                                break;
                            } else {
                                continue;
                            }
                        default:
                            continue;
                    }
                    r10.A05();
                    A0l.add(r10);
                }
                AnonymousClass3JD r4 = new AnonymousClass3JD(A0K, A0K2, A0l, i);
                if (A05.has("blurred-bitmap-provider") && (jSONObject = A05.getJSONObject("blurred-bitmap-provider")) != null) {
                    byte[] decode = Base64.decode(jSONObject.getString("small-bitmap"), 0);
                    C93694aa r3 = new C93694aa(jSONObject.getInt("origin-width"), jSONObject.getInt("origin-height"), BitmapFactory.decodeByteArray(decode, 0, decode.length));
                    for (AbstractC454821u r1 : r4.A06) {
                        if (r1 instanceof AnonymousClass33J) {
                            ((AnonymousClass33J) r1).A0R(r3);
                        }
                    }
                }
                return r4;
            } catch (IllegalArgumentException e) {
                Log.e("Doodle/Drawable or picture unable to load from JSON", e);
                return null;
            } catch (JSONException e2) {
                Log.e("Doodle/load unable to load from JSON", e2);
            }
        }
        return null;
    }

    public String A04() {
        C93694aa r5;
        JSONObject jSONObject;
        try {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("version", 1);
            RectF rectF = this.A04;
            jSONObject2.put("l", (int) (rectF.left * 100.0f));
            jSONObject2.put("t", (int) (rectF.top * 100.0f));
            jSONObject2.put("r", (int) (rectF.right * 100.0f));
            jSONObject2.put("b", (int) (rectF.bottom * 100.0f));
            RectF rectF2 = this.A03;
            jSONObject2.put("crop-l", (int) (rectF2.left * 100.0f));
            jSONObject2.put("crop-t", (int) (rectF2.top * 100.0f));
            jSONObject2.put("crop-r", (int) (rectF2.right * 100.0f));
            jSONObject2.put("crop-b", (int) (rectF2.bottom * 100.0f));
            jSONObject2.put("rotate", this.A02);
            List<AbstractC454821u> list = this.A06;
            Iterator it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    r5 = null;
                    break;
                }
                AbstractC454821u r1 = (AbstractC454821u) it.next();
                if ((r1 instanceof AnonymousClass33J) && (r5 = ((AnonymousClass33J) r1).A04) != null) {
                    break;
                }
            }
            if (r5 != null) {
                if (r5.A03 == null) {
                    jSONObject = null;
                } else {
                    jSONObject = new JSONObject();
                    jSONObject.put("origin-width", r5.A01);
                    jSONObject.put("origin-height", r5.A00);
                    jSONObject.put("small-bitmap", C37501mV.A07(r5.A03));
                }
                jSONObject2.put("blurred-bitmap-provider", jSONObject);
            }
            JSONArray jSONArray = new JSONArray();
            for (AbstractC454821u r12 : list) {
                JSONObject jSONObject3 = new JSONObject();
                r12.A0N(jSONObject3);
                jSONArray.put(jSONObject3);
            }
            jSONObject2.put("shapes", jSONArray);
            return jSONObject2.toString();
        } catch (JSONException e) {
            Log.e("Doodle/toJson error while constructing JSON", e);
            return null;
        }
    }

    public List A05(float f, float f2) {
        ArrayList A0l = C12960it.A0l();
        for (Object obj : this.A06) {
            if (obj instanceof AnonymousClass33N) {
                A0l.add(obj);
            }
        }
        if (A0l.isEmpty()) {
            return null;
        }
        RectF rectF = this.A03;
        Matrix A00 = A00(f, f2, (float) this.A02, rectF.width(), rectF.height(), false, false);
        ArrayList A0l2 = C12960it.A0l();
        Iterator it = A0l.iterator();
        while (it.hasNext()) {
            AnonymousClass33N r7 = (AnonymousClass33N) it.next();
            PointF pointF = new PointF(f, f2);
            RectF rectF2 = ((AbstractC454821u) r7).A02;
            rectF2.sort();
            Matrix A01 = C13000ix.A01();
            A01.preConcat(A00);
            A01.preTranslate(-rectF.left, -rectF.top);
            A01.preRotate(((AbstractC454821u) r7).A00, rectF2.centerX(), rectF2.centerY());
            float f3 = rectF2.left;
            float f4 = rectF2.top;
            float f5 = rectF2.right;
            float f6 = rectF2.bottom;
            float[] fArr = {f3, f4, f5, f4, f5, f6, f3, f6};
            A01.mapPoints(fArr);
            float f7 = fArr[0];
            float f8 = pointF.x;
            double d = (double) (f7 / f8);
            float f9 = fArr[1];
            float f10 = pointF.y;
            A0l2.add(new InteractiveAnnotation(r7.A06, new SerializablePoint[]{new SerializablePoint(d, (double) (f9 / f10)), new SerializablePoint((double) (fArr[2] / f8), (double) (fArr[3] / f10)), new SerializablePoint((double) (fArr[4] / f8), (double) (fArr[5] / f10)), new SerializablePoint((double) (fArr[6] / f8), (double) (fArr[7] / f10))}, r7.A00, r7.A01));
        }
        return A0l2;
    }

    public void A06(int i, Bitmap bitmap) {
        Canvas canvas = new Canvas(bitmap);
        A08(bitmap, canvas, i, false, false);
        for (AbstractC454821u r0 : this.A06) {
            r0.A0I(canvas);
        }
    }

    public void A07(Bitmap bitmap, int i, boolean z, boolean z2) {
        Canvas canvas = new Canvas(bitmap);
        A08(bitmap, canvas, i, z, z2);
        for (AbstractC454821u r0 : this.A06) {
            r0.A0P(canvas);
        }
    }

    public void A08(Bitmap bitmap, Canvas canvas, int i, boolean z, boolean z2) {
        RectF rectF = this.A04;
        canvas.concat(A00((float) bitmap.getWidth(), (float) bitmap.getHeight(), (float) i, rectF.width(), rectF.height(), z, z2));
    }

    public void A09(AnonymousClass31R r5) {
        String[] A08 = C13000ix.A08();
        A08[0] = "pen";
        if (A0B(A08)) {
            r5.A0A = C12980iv.A0m(r5.A0A);
        }
        if (A0B(new String[]{"arrow", "oval", "thinking-bubble", "speech-bubble-oval", "speech-bubble-rect", "digital-clock", "analog-clock", "location", "sticker", "emoji"})) {
            r5.A0E = C12980iv.A0m(r5.A0E);
        }
        String[] A082 = C13000ix.A08();
        A082[0] = "text";
        if (A0B(A082)) {
            r5.A0G = C12980iv.A0m(r5.A0G);
        }
        String[] A083 = C13000ix.A08();
        A083[0] = "location";
        r5.A03 = Boolean.valueOf(A0B(A083));
    }

    public boolean A0A(File file) {
        String A04 = A04();
        if (A04 == null) {
            return false;
        }
        try {
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(A04);
            fileWriter.close();
            return true;
        } catch (IOException e) {
            Log.e("Doodle/save failed to save doodle string to file", e);
            return false;
        }
    }

    public final boolean A0B(String[] strArr) {
        List<AbstractC454821u> list = this.A06;
        if (list != null) {
            for (AbstractC454821u r0 : list) {
                if (C37871n9.A01(r0.A0G(), strArr)) {
                    return true;
                }
            }
        }
        return false;
    }
}
