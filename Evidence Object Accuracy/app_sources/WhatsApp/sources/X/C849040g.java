package X;

import android.view.View;

/* renamed from: X.40g  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C849040g extends AbstractC55202hx {
    @Override // X.AbstractC55202hx
    public void A08() {
    }

    @Override // X.AbstractC55202hx
    public void A0A(int i) {
    }

    @Override // X.AbstractC55202hx
    public void A0B(int i) {
    }

    @Override // X.AbstractC55202hx
    public void A0F(AnonymousClass3CK r1) {
    }

    public C849040g(View view, C18720su r9, AnonymousClass130 r10, C15610nY r11) {
        super(view, r9, null, null, r10, r11);
    }

    @Override // X.AbstractC55202hx
    public void A09() {
        ((AbstractC55202hx) this).A05 = null;
    }

    @Override // X.AbstractC55202hx
    public void A0G(C64363Fg r1) {
        ((AbstractC55202hx) this).A05 = r1;
    }
}
