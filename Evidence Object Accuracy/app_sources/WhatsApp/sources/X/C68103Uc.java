package X;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/* renamed from: X.3Uc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68103Uc implements AbstractC37461mR {
    public boolean A00;
    public final OutputStream A01;
    public final ZipOutputStream A02;
    public final /* synthetic */ AnonymousClass2AU A03;

    public C68103Uc(AnonymousClass2AU r5) {
        this.A03 = r5;
        OutputStream AEq = r5.A00.AEq();
        this.A01 = AEq;
        this.A02 = r5.A04.A04(EnumC16570pG.A08, AEq, null, null);
    }

    @Override // X.AbstractC37461mR
    public void AgB(File file) {
        if (file != null && file.isFile() && file.exists()) {
            FileInputStream fileInputStream = new FileInputStream(file);
            try {
                ZipOutputStream zipOutputStream = this.A02;
                zipOutputStream.putNextEntry(new ZipEntry(file.getName()));
                C14350lI.A0G(fileInputStream, zipOutputStream);
                zipOutputStream.closeEntry();
                fileInputStream.close();
            } catch (Throwable th) {
                try {
                    fileInputStream.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
    }

    @Override // X.AbstractC37461mR
    public void AgD(AnonymousClass03D r7, AbstractC14590lg r8, File file, String str, long j) {
        if (file.isFile() && file.exists() && str != null) {
            FileInputStream fileInputStream = new FileInputStream(file);
            try {
                ZipOutputStream zipOutputStream = this.A02;
                zipOutputStream.putNextEntry(new ZipEntry(str));
                C32781cj.A0A(r7, r8, fileInputStream, zipOutputStream, j);
                zipOutputStream.closeEntry();
                fileInputStream.close();
            } catch (Throwable th) {
                try {
                    fileInputStream.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        if (!this.A00) {
            this.A02.close();
            this.A01.close();
            this.A00 = true;
        }
    }
}
