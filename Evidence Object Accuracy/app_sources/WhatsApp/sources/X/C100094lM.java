package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.location.PlaceInfo;

/* renamed from: X.4lM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C100094lM implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new PlaceInfo(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new PlaceInfo[i];
    }
}
