package X;

/* renamed from: X.2KM  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2KM extends AbstractC16110oT {
    public Integer A00;
    public Long A01;
    public Long A02;
    public Long A03;
    public String A04;

    public AnonymousClass2KM() {
        super(3092, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A01);
        r3.Abe(2, this.A04);
        r3.Abe(3, this.A02);
        r3.Abe(4, this.A03);
        r3.Abe(5, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        StringBuilder sb = new StringBuilder("WamSeamlessMigrationEvent {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mdDurationS", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mdSessionId", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mdTimestampS", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "seamlessMigrationEventErrorCode", this.A03);
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "seamlessMigrationEventStage", obj);
        sb.append("}");
        return sb.toString();
    }
}
