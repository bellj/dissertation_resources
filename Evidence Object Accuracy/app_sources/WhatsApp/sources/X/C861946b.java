package X;

import java.util.Arrays;

/* renamed from: X.46b  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C861946b extends AbstractC89694Ky {
    public String A00 = "";

    public C861946b() {
        super(-1);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C861946b)) {
            return false;
        }
        return C29941Vi.A00(this.A00, ((C861946b) obj).A00);
    }

    public int hashCode() {
        Object[] A1a = C12980iv.A1a();
        C12960it.A1O(A1a, super.A00);
        C12980iv.A1T(A1a, this.A00.hashCode());
        return Arrays.hashCode(A1a);
    }
}
