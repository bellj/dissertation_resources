package X;

/* renamed from: X.4Bv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C87524Bv extends Exception {
    public final int timeoutOperation;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C87524Bv(int r2) {
        /*
            r1 = this;
            r0 = 1
            if (r2 == r0) goto L_0x0017
            r0 = 2
            if (r2 == r0) goto L_0x0014
            r0 = 3
            if (r2 == r0) goto L_0x0011
            java.lang.String r0 = "Undefined timeout."
        L_0x000b:
            r1.<init>(r0)
            r1.timeoutOperation = r2
            return
        L_0x0011:
            java.lang.String r0 = "Detaching surface timed out."
            goto L_0x000b
        L_0x0014:
            java.lang.String r0 = "Setting foreground mode timed out."
            goto L_0x000b
        L_0x0017:
            java.lang.String r0 = "Player release timed out."
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C87524Bv.<init>(int):void");
    }
}
