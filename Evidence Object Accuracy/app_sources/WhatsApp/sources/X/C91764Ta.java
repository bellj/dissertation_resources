package X;

/* renamed from: X.4Ta  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C91764Ta {
    public final C15450nH A00;
    public final AnonymousClass01d A01;
    public final C14850m9 A02;
    public final C453421e A03;
    public final C16630pM A04;
    public final C22190yg A05;
    public final AbstractC14440lR A06;

    public C91764Ta(C15450nH r1, AnonymousClass01d r2, C14850m9 r3, C453421e r4, C16630pM r5, C22190yg r6, AbstractC14440lR r7) {
        this.A06 = r7;
        this.A00 = r1;
        this.A02 = r3;
        this.A05 = r6;
        this.A03 = r4;
        this.A04 = r5;
        this.A01 = r2;
    }
}
