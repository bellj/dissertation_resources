package X;

import java.util.Map;

/* renamed from: X.6Mz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public interface AbstractC136556Mz {
    void A5v(Object obj, Map map, int i, int i2);

    void A9R(Object obj, String str, int i, int i2);

    void A9S(Object obj, String str, String str2, int i, int i2);

    void A9U(Object obj, int i, int i2);

    void AKq(Object obj, String str, String str2, int i, int i2);

    void AKr(Object obj, String str, Map map, int i, int i2);

    void AeI(Object obj, String str, int i, int i2, boolean z);
}
