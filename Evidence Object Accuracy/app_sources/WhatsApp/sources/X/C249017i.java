package X;

/* renamed from: X.17i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C249017i {
    public final C15450nH A00;
    public final C21360xI A01;
    public final C16590pI A02;
    public final C18360sK A03;
    public final AnonymousClass018 A04;
    public final C245716a A05;
    public final C14840m8 A06;
    public final AnonymousClass17R A07;

    public C249017i(C15450nH r1, C21360xI r2, C16590pI r3, C18360sK r4, AnonymousClass018 r5, C245716a r6, C14840m8 r7, AnonymousClass17R r8) {
        this.A02 = r3;
        this.A00 = r1;
        this.A07 = r8;
        this.A04 = r5;
        this.A06 = r7;
        this.A05 = r6;
        this.A03 = r4;
        this.A01 = r2;
    }
}
