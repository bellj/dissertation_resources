package X;

import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/* renamed from: X.0QS  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0QS {
    public int A00;
    public int A01;
    public AnonymousClass0OK A02;
    public AbstractC03990Jy A03;
    public ArrayList A04 = null;
    public final ArrayList A05;
    public final ArrayList A06 = new ArrayList();
    public final List A07;
    public final /* synthetic */ RecyclerView A08;

    public AnonymousClass0QS(RecyclerView recyclerView) {
        this.A08 = recyclerView;
        ArrayList arrayList = new ArrayList();
        this.A05 = arrayList;
        this.A07 = Collections.unmodifiableList(arrayList);
        this.A00 = 2;
        this.A01 = 2;
    }

    public int A00(int i) {
        if (i >= 0) {
            RecyclerView recyclerView = this.A08;
            C05480Ps r1 = recyclerView.A0y;
            if (i < r1.A00()) {
                if (r1.A08) {
                    return recyclerView.A0J.A00(i, 0);
                }
                return i;
            }
        }
        StringBuilder sb = new StringBuilder("invalid position ");
        sb.append(i);
        sb.append(". State ");
        sb.append("item count is ");
        RecyclerView recyclerView2 = this.A08;
        sb.append(recyclerView2.A0y.A00());
        sb.append(recyclerView2.A0F());
        throw new IndexOutOfBoundsException(sb.toString());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:149:0x0263, code lost:
        r8.A00 = r1 | r8.A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:153:0x0270, code lost:
        if (r4.A08 == false) goto L_0x0272;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:178:0x02dc, code lost:
        r8.A05 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:202:0x0352, code lost:
        if (r8.A08 != r10.A00(r8.A05)) goto L_0x0272;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00aa, code lost:
        if ((r1 & 4) == 0) goto L_0x0071;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00d5, code lost:
        if ((r12 + r0) >= r20) goto L_0x0071;
     */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0087 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00a2  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00c3  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x00df  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x010d  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0129  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0138  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0144  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x014d  */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x0161  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass03U A01(int r19, long r20) {
        /*
        // Method dump skipped, instructions count: 1208
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0QS.A01(int, long):X.03U");
    }

    public void A02() {
        ArrayList arrayList = this.A06;
        int size = arrayList.size();
        while (true) {
            size--;
            if (size < 0) {
                break;
            }
            A04(size);
        }
        arrayList.clear();
        if (RecyclerView.A1D) {
            AnonymousClass0Z8 r2 = this.A08.A0L;
            int[] iArr = r2.A03;
            if (iArr != null) {
                Arrays.fill(iArr, -1);
            }
            r2.A00 = 0;
        }
    }

    public void A03() {
        int i;
        AnonymousClass02H r0 = this.A08.A0S;
        if (r0 != null) {
            i = r0.A02;
        } else {
            i = 0;
        }
        this.A01 = this.A00 + i;
        ArrayList arrayList = this.A06;
        int size = arrayList.size();
        while (true) {
            size--;
            if (size >= 0 && arrayList.size() > this.A01) {
                A04(size);
            } else {
                return;
            }
        }
    }

    public void A04(int i) {
        ArrayList arrayList = this.A06;
        A0A((AnonymousClass03U) arrayList.get(i), true);
        arrayList.remove(i);
    }

    public void A05(View view) {
        AnonymousClass03U A01 = RecyclerView.A01(view);
        if ((A01.A00 & 256) != 0) {
            this.A08.removeDetachedView(view, false);
        }
        AnonymousClass0QS r0 = A01.A09;
        if (r0 != null) {
            r0.A09(A01);
        } else {
            int i = A01.A00;
            if ((i & 32) != 0) {
                A01.A00 = i & -33;
            }
        }
        A08(A01);
    }

    public void A06(View view) {
        AnonymousClass04Y r1;
        AnonymousClass03U A01 = RecyclerView.A01(view);
        int i = A01.A00;
        if ((12 & i) != 0 || (i & 2) == 0 || (r1 = this.A08.A0R) == null || r1.A0C(A01, A01.A01())) {
            int i2 = A01.A00;
            if ((i2 & 4) != 0 && (i2 & 8) == 0) {
                RecyclerView recyclerView = this.A08;
                if (!recyclerView.A0N.A00) {
                    StringBuilder sb = new StringBuilder("Called scrap view with an invalid view. Invalid views cannot be reused from scrap, they should rebound from recycler pool.");
                    sb.append(recyclerView.A0F());
                    throw new IllegalArgumentException(sb.toString());
                }
            }
            A01.A09 = this;
            A01.A0G = false;
            this.A05.add(A01);
            return;
        }
        ArrayList arrayList = this.A04;
        if (arrayList == null) {
            arrayList = new ArrayList();
            this.A04 = arrayList;
        }
        A01.A09 = this;
        A01.A0G = true;
        arrayList.add(A01);
    }

    public final void A07(ViewGroup viewGroup, boolean z) {
        for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
            View childAt = viewGroup.getChildAt(childCount);
            if (childAt instanceof ViewGroup) {
                A07((ViewGroup) childAt, true);
            }
        }
        if (!z) {
            return;
        }
        if (viewGroup.getVisibility() == 4) {
            viewGroup.setVisibility(0);
            viewGroup.setVisibility(4);
            return;
        }
        int visibility = viewGroup.getVisibility();
        viewGroup.setVisibility(4);
        viewGroup.setVisibility(visibility);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0044, code lost:
        if (r2.hasTransientState() == false) goto L_0x0046;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A08(X.AnonymousClass03U r12) {
        /*
        // Method dump skipped, instructions count: 272
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0QS.A08(X.03U):void");
    }

    public void A09(AnonymousClass03U r2) {
        ArrayList arrayList;
        if (r2.A0G) {
            arrayList = this.A04;
        } else {
            arrayList = this.A05;
        }
        arrayList.remove(r2);
        r2.A09 = null;
        r2.A0G = false;
        r2.A00 &= -33;
    }

    public void A0A(AnonymousClass03U r5, boolean z) {
        RecyclerView.A05(r5);
        boolean z2 = false;
        if ((16384 & r5.A00) != 0) {
            z2 = true;
        }
        if (z2) {
            r5.A03(0, 16384);
            AnonymousClass028.A0g(r5.A0H, null);
        }
        if (z) {
            RecyclerView recyclerView = this.A08;
            AbstractC11880h1 r0 = recyclerView.A0W;
            if (r0 != null) {
                r0.AYO(r5);
            }
            AnonymousClass02M r02 = recyclerView.A0N;
            if (r02 != null) {
                r02.A0A(r5);
            }
            if (recyclerView.A0y != null) {
                recyclerView.A11.A05(r5);
            }
        }
        r5.A0C = null;
        AnonymousClass0OK r3 = this.A02;
        if (r3 == null) {
            r3 = new AnonymousClass0OK();
            this.A02 = r3;
        }
        int i = r5.A02;
        ArrayList arrayList = r3.A00(i).A03;
        if (((C04810Nd) r3.A01.get(i)).A00 > arrayList.size()) {
            r5.A02();
            arrayList.add(r5);
        }
    }
}
