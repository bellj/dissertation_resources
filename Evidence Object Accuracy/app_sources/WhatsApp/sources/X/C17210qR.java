package X;

import android.content.SharedPreferences;

/* renamed from: X.0qR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17210qR {
    public C29361Rw A00;
    public final C17200qQ A01;
    public final C17220qS A02;

    public C17210qR(C17200qQ r1, C17220qS r2) {
        this.A01 = r1;
        this.A02 = r2;
    }

    public void A00() {
        C17200qQ r5 = this.A01;
        if (r5.A0A.A07(1689)) {
            SharedPreferences A01 = r5.A0C.A01("keystore");
            if (A01.getInt("remaining_auth_key_rotation_attempts", 0) > 0) {
                long j = A01.getLong("last_failed_auth_key_rotation_attempt", -1);
                if (j == -1 || r5.A06.A00() - j > 1800000) {
                    if (this.A00 == null) {
                        this.A00 = C29361Rw.A00();
                    }
                    C17220qS r6 = this.A02;
                    String A012 = r6.A01();
                    r6.A0D(new AnonymousClass3ZL(r5, this, this.A00), new AnonymousClass3BD(new AnonymousClass3CT(A012), this.A00.A02.A01).A00, A012, 331, 32000);
                }
            }
        }
    }
}
