package X;

/* renamed from: X.4C2  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4C2 extends Exception {
    public final int errorCode;
    public final String message;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass4C2(int r3, java.lang.String r4) {
        /*
            r2 = this;
            java.lang.String r0 = "error="
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            r1.append(r3)
            java.lang.String r0 = ": "
            r1.append(r0)
            java.lang.String r0 = X.C12960it.A0d(r4, r1)
            r2.<init>(r0)
            r2.errorCode = r3
            r2.message = r4
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass4C2.<init>(int, java.lang.String):void");
    }
}
