package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;

/* renamed from: X.2YH  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2YH extends AnimatorListenerAdapter {
    public final /* synthetic */ C53232dO A00;
    public final /* synthetic */ boolean A01;

    public AnonymousClass2YH(C53232dO r1, boolean z) {
        this.A00 = r1;
        this.A01 = z;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        C53232dO r8;
        int i;
        int i2;
        int i3 = 0;
        while (true) {
            r8 = this.A00;
            if (i3 >= r8.getChildCount()) {
                break;
            }
            View childAt = r8.getChildAt(i3);
            ViewGroup.LayoutParams layoutParams = childAt.getLayoutParams();
            layoutParams.width = r8.A0A[i3];
            childAt.setLayoutParams(layoutParams);
            i3++;
        }
        C53232dO.A01(r8, 0);
        boolean z = this.A01;
        int childCount = r8.getChildCount();
        int i4 = -1;
        if (z == C28141Kv.A01(r8.A02)) {
            i = r8.getChildCount() - 1;
            i2 = -1;
        } else {
            i4 = childCount;
            i = 0;
            i2 = 1;
        }
        int i5 = 0;
        while (i != i4) {
            if (!(r8.getChildAt(i) instanceof AbstractC116775Wt)) {
                AnonymousClass009.A07("Given view is not ReactionTrayItem.");
            }
            AbstractC116775Wt r11 = (AbstractC116775Wt) r8.getChildAt(i);
            r11.setForegroundScale(0.0f);
            AnimatorSet animatorSet = new AnimatorSet();
            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(r11, "foregroundScale", 0.0f, 1.1f);
            ofFloat.setDuration(160L);
            Interpolator interpolator = C53232dO.A0D;
            ofFloat.setInterpolator(interpolator);
            ObjectAnimator duration = ObjectAnimator.ofFloat(r11, "foregroundAlpha", 0.0f, 1.0f).setDuration(120L);
            ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(r11, "foregroundScale", 1.1f, 1.0f);
            ofFloat2.setDuration(160L);
            ofFloat2.setInterpolator(interpolator);
            animatorSet.playSequentially(ofFloat, ofFloat2);
            r11.setBackgroundAlpha(0.0f);
            ObjectAnimator ofFloat3 = ObjectAnimator.ofFloat(r11, "backgroundAlpha", 0.0f, 1.0f);
            ofFloat3.setInterpolator(C53232dO.A0B);
            ofFloat3.setDuration(320L);
            animatorSet.playTogether(ofFloat, duration, ofFloat3);
            animatorSet.setStartDelay(((long) i5) * 35);
            animatorSet.start();
            i += i2;
            i5++;
        }
    }
}
