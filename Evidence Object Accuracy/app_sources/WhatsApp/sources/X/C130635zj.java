package X;

/* renamed from: X.5zj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130635zj {
    public final int A00;
    public final C129975yb A01;
    public final Integer A02;
    public final Integer A03;
    public final byte[] A04;

    public C130635zj(C129975yb r3) {
        Integer num;
        Integer num2 = null;
        this.A04 = null;
        this.A01 = r3;
        this.A00 = 35;
        if (r3 != null) {
            num = Integer.valueOf(r3.A02);
        } else {
            num = null;
        }
        this.A03 = num;
        this.A02 = r3 != null ? Integer.valueOf(r3.A00) : num2;
    }

    public C130635zj(byte[] bArr) {
        this.A04 = bArr;
        this.A01 = null;
        this.A00 = 256;
        this.A03 = null;
        this.A02 = null;
    }
}
