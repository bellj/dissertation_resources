package X;

import java.util.List;

/* renamed from: X.0Cz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02590Cz extends AbstractC07280Xj {
    @Override // X.AbstractC07280Xj
    public boolean A0B() {
        return false;
    }

    public C02590Cz(AnonymousClass0QV r2) {
        super(r2);
        r2.A0c.A08();
        r2.A0d.A08();
        this.A01 = ((C02540Cs) r2).A01;
    }

    @Override // X.AbstractC07280Xj
    public void A06() {
        C07270Xi r0;
        AnonymousClass0QV r6 = this.A03;
        C02540Cs r02 = (C02540Cs) r6;
        int i = r02.A02;
        int i2 = r02.A03;
        int i3 = r02.A01;
        C07270Xi r2 = this.A05;
        if (i3 == 1) {
            if (i != -1) {
                r2.A08.add(r6.A0Z.A0c.A05);
                this.A03.A0Z.A0c.A05.A07.add(r2);
            } else if (i2 != -1) {
                r2.A08.add(r6.A0Z.A0c.A04);
                this.A03.A0Z.A0c.A04.A07.add(r2);
                i = -i2;
            } else {
                r2.A09 = true;
                r2.A08.add(r6.A0Z.A0c.A04);
                this.A03.A0Z.A0c.A04.A07.add(r2);
                C07270Xi r03 = this.A03.A0c.A05;
                List list = r2.A07;
                list.add(r03);
                r03.A08.add(r2);
                r0 = this.A03.A0c.A04;
                list.add(r0);
            }
            r2.A00 = i;
            C07270Xi r03 = this.A03.A0c.A05;
            List list = r2.A07;
            list.add(r03);
            r03.A08.add(r2);
            r0 = this.A03.A0c.A04;
            list.add(r0);
        } else {
            if (i != -1) {
                r2.A08.add(r6.A0Z.A0d.A05);
                this.A03.A0Z.A0d.A05.A07.add(r2);
            } else if (i2 != -1) {
                r2.A08.add(r6.A0Z.A0d.A04);
                this.A03.A0Z.A0d.A04.A07.add(r2);
                i = -i2;
            } else {
                r2.A09 = true;
                r2.A08.add(r6.A0Z.A0d.A04);
                this.A03.A0Z.A0d.A04.A07.add(r2);
                C07270Xi r04 = this.A03.A0d.A05;
                List list2 = r2.A07;
                list2.add(r04);
                r04.A08.add(r2);
                r0 = this.A03.A0d.A04;
                list2.add(r0);
            }
            r2.A00 = i;
            C07270Xi r04 = this.A03.A0d.A05;
            List list2 = r2.A07;
            list2.add(r04);
            r04.A08.add(r2);
            r0 = this.A03.A0d.A04;
            list2.add(r0);
        }
        r0.A08.add(r2);
    }

    @Override // X.AbstractC07280Xj
    public void A07() {
        AnonymousClass0QV r3 = this.A03;
        int i = ((C02540Cs) r3).A01;
        int i2 = this.A05.A02;
        if (i == 1) {
            r3.A0P = i2;
        } else {
            r3.A0Q = i2;
        }
    }

    @Override // X.AbstractC07280Xj
    public void A08() {
        this.A05.A00();
    }

    @Override // X.AbstractC07280Xj, X.AbstractC11720gk
    public void AfH(AbstractC11720gk r6) {
        C07270Xi r4 = this.A05;
        if (r4.A0A && !r4.A0B) {
            r4.A01((int) ((((float) ((C07270Xi) r4.A08.get(0)).A02) * ((C02540Cs) this.A03).A00) + 0.5f));
        }
    }
}
