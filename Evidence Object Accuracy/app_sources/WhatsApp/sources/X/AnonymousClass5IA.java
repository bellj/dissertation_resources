package X;

import java.util.ArrayList;

/* renamed from: X.5IA  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5IA extends ArrayList<AnonymousClass4QF> {
    public AnonymousClass5IA() {
        add(new AnonymousClass4QF(EnumC87174Am.INTEGER_PRIMARY_KEY_AUTOINCREMENT, "_id", false));
        EnumC87174Am r3 = EnumC87174Am.INTEGER;
        add(new AnonymousClass4QF(r3, "label_id", true));
        add(new AnonymousClass4QF(r3, "message_row_id", true));
    }
}
