package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;
import com.whatsapp.R;

/* renamed from: X.54z  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1102954z implements AnonymousClass28F {
    public final /* synthetic */ AnonymousClass130 A00;
    public final /* synthetic */ boolean A01;

    public C1102954z(AnonymousClass130 r1, boolean z) {
        this.A00 = r1;
        this.A01 = z;
    }

    @Override // X.AnonymousClass28F
    public void Adh(Bitmap bitmap, ImageView imageView, boolean z) {
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        } else {
            Adv(imageView);
        }
    }

    @Override // X.AnonymousClass28F
    public void Adv(ImageView imageView) {
        AnonymousClass130 r3 = this.A00;
        Context context = imageView.getContext();
        boolean z = this.A01;
        int i = R.drawable.avatar_contact;
        if (z) {
            i = R.drawable.avatar_contact_voip;
        }
        imageView.setImageBitmap(r3.A03(context, i));
    }
}
