package X;

/* renamed from: X.4XF  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4XF {
    public final int A00;
    public final AbstractC116855Xe A01;
    public final AnonymousClass1UE A02;
    public final String A03;

    public final int hashCode() {
        return this.A00;
    }

    public AnonymousClass4XF(AbstractC116855Xe r3, AnonymousClass1UE r4, String str) {
        this.A02 = r4;
        this.A01 = r3;
        this.A03 = str;
        Object[] objArr = new Object[3];
        C12970iu.A1U(r4, r3, objArr);
        this.A00 = C12980iv.A0B(str, objArr, 2);
    }

    public final boolean equals(Object obj) {
        if (obj != null) {
            if (obj != this) {
                if (obj instanceof AnonymousClass4XF) {
                    AnonymousClass4XF r5 = (AnonymousClass4XF) obj;
                    if (!C13300jT.A00(this.A02, r5.A02) || !C13300jT.A00(this.A01, r5.A01) || !C13300jT.A00(this.A03, r5.A03)) {
                    }
                }
            }
            return true;
        }
        return false;
    }
}
