package X;

import android.content.Context;
import android.view.PointerIcon;

/* renamed from: X.0Kr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C04180Kr {
    public static PointerIcon A00(Context context, int i) {
        return PointerIcon.getSystemIcon(context, i);
    }
}
