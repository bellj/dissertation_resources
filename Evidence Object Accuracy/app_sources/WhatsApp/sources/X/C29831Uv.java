package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.UserJid;

/* renamed from: X.1Uv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C29831Uv extends UserJid {
    public static final C29831Uv A00;
    public static final Parcelable.Creator CREATOR = new AnonymousClass27G();

    @Override // com.whatsapp.jid.UserJid, com.whatsapp.jid.Jid
    public String getServer() {
        return "status_me";
    }

    @Override // com.whatsapp.jid.UserJid, com.whatsapp.jid.Jid
    public int getType() {
        return 11;
    }

    static {
        try {
            A00 = new C29831Uv();
        } catch (AnonymousClass1MW e) {
            throw new IllegalStateException(e);
        }
    }

    public C29831Uv() {
        super("");
    }

    public /* synthetic */ C29831Uv(Parcel parcel) {
        super(parcel);
    }

    @Override // com.whatsapp.jid.UserJid, com.whatsapp.jid.Jid
    public String getObfuscatedString() {
        return getRawString();
    }

    @Override // com.whatsapp.jid.UserJid
    public DeviceJid getPrimaryDevice() {
        throw new UnsupportedOperationException("getPrimaryDevice() must not be called for MeJid");
    }
}
