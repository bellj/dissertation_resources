package X;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;

/* renamed from: X.2va  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59912va extends AbstractC37191le {
    public final RecyclerView A00;
    public final C54492go A01;

    public C59912va(View view, C54492go r4) {
        super(view);
        RecyclerView A0R = C12990iw.A0R(view, R.id.search_filter_recycler_view);
        this.A00 = A0R;
        A0R.setLayoutManager(C12990iw.A0Q(view));
        this.A01 = r4;
    }
}
