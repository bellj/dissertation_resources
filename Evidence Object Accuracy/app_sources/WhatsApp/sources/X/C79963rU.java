package X;

import android.os.IBinder;

/* renamed from: X.3rU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79963rU extends C98404id implements AnonymousClass5YL {
    public C79963rU(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.vision.face.internal.client.INativeFaceDetector");
    }
}
