package X;

import java.util.Arrays;

/* renamed from: X.4Wg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C92524Wg {
    public int A00;
    public final AnonymousClass5QI[] A01;

    public C92524Wg(AnonymousClass5QI... r1) {
        this.A01 = r1;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || C92524Wg.class != obj.getClass()) {
            return false;
        }
        return Arrays.equals(this.A01, ((C92524Wg) obj).A01);
    }

    public int hashCode() {
        int i = this.A00;
        if (i != 0) {
            return i;
        }
        int hashCode = 527 + Arrays.hashCode(this.A01);
        this.A00 = hashCode;
        return hashCode;
    }
}
