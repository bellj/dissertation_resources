package X;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.biz.cart.view.fragment.CartFragment;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/* renamed from: X.1lE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37051lE extends AnonymousClass02M implements AbstractC37061lF {
    public Date A00 = new Date();
    public final AnonymousClass4J6 A01;
    public final AnonymousClass2EM A02;
    public final CartFragment A03;
    public final CartFragment A04;
    public final C37071lG A05;
    public final C14850m9 A06;
    public final List A07 = new ArrayList();

    public C37051lE(AnonymousClass4J6 r2, AnonymousClass2EM r3, CartFragment cartFragment, CartFragment cartFragment2, C37071lG r6, C14850m9 r7) {
        this.A06 = r7;
        this.A05 = r6;
        this.A03 = cartFragment;
        this.A02 = r3;
        this.A04 = cartFragment2;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A07.size();
    }

    public int A0E() {
        int i = 0;
        for (AnonymousClass4JV r1 : this.A07) {
            if (r1 instanceof C84493zO) {
                i = (int) (((long) i) + ((C84493zO) r1).A00.A00);
            }
        }
        return i;
    }

    public List A0F() {
        ArrayList arrayList = new ArrayList();
        for (AnonymousClass4JV r1 : this.A07) {
            if (r1 instanceof C84493zO) {
                arrayList.add(((C84493zO) r1).A00);
            }
        }
        return arrayList;
    }

    @Override // X.AbstractC37061lF
    public AnonymousClass4JV ACa(int i) {
        return (AnonymousClass4JV) this.A07.get(i);
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r2, int i) {
        ((AbstractC75653kC) r2).A08((AnonymousClass4JV) this.A07.get(i));
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        if (i == 0) {
            return new C59102ty(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_cart_header_item, viewGroup, false), this.A03);
        } else if (i == 1) {
            AnonymousClass4J6 r3 = this.A01;
            AnonymousClass2EM r6 = this.A02;
            C37071lG r10 = this.A05;
            return new C59132u1(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_cart_item_new_selector, viewGroup, false), r6, this, this.A03, this.A04, r10, (AnonymousClass018) r3.A00.A04.ANb.get());
        } else if (i == 2) {
            return new C59112tz(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_cart_footer_item, viewGroup, false));
        } else {
            throw new IllegalStateException("CartItemsAdapter/onCreateViewHolder/unhandled view type");
        }
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        return ((AnonymousClass4JV) this.A07.get(i)).A00;
    }
}
