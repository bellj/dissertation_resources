package X;

/* renamed from: X.4RX  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass4RX {
    public final AnonymousClass4NJ A00;
    public final boolean A01;
    public final boolean A02;
    public final boolean A03;

    public /* synthetic */ AnonymousClass4RX(AnonymousClass4NJ r2, int i, boolean z, boolean z2, boolean z3) {
        z = (i & 1) != 0 ? false : z;
        z2 = (i & 2) != 0 ? false : z2;
        r2 = (i & 4) != 0 ? AnonymousClass41F.A00 : r2;
        z3 = (i & 8) != 0 ? false : z3;
        this.A01 = z;
        this.A02 = z2;
        this.A00 = r2;
        this.A03 = z3;
    }
}
