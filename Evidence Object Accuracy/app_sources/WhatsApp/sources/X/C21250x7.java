package X;

import com.facebook.redex.RunnableBRunnable0Shape4S0200000_I0_4;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.Arrays;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.0x7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21250x7 {
    public final C15570nT A00;
    public final C15550nR A01;
    public final C14830m7 A02;
    public final C19990v2 A03;
    public final C21390xL A04;
    public final C244815r A05;
    public final C16120oU A06;
    public final AnonymousClass11G A07;
    public final C17110qH A08;

    public C21250x7(C15570nT r1, C15550nR r2, C14830m7 r3, C19990v2 r4, C21390xL r5, C244815r r6, C16120oU r7, AnonymousClass11G r8, C17110qH r9) {
        this.A02 = r3;
        this.A00 = r1;
        this.A05 = r6;
        this.A03 = r4;
        this.A06 = r7;
        this.A01 = r2;
        this.A04 = r5;
        this.A07 = r8;
        this.A08 = r9;
    }

    public final int A00(AbstractC14640lm r4) {
        AnonymousClass1PE A06 = this.A03.A06(r4);
        int i = 0;
        if (r4 == null) {
            Log.e("spamManager/isCallNotSpamProp/null jid");
        } else {
            List A01 = A01();
            if (A01 != null && A01.contains(r4)) {
                i = 1;
            }
        }
        int i2 = i ^ 1;
        if (A06 == null) {
            return i2 ^ 1;
        }
        if (i2 != 0) {
            return A06.A03;
        }
        return 1;
    }

    public final List A01() {
        String A02 = this.A04.A02("call_not_spam_jids");
        if (A02 == null || A02.length() <= 0) {
            return null;
        }
        return C15380n4.A07(AbstractC14640lm.class, Arrays.asList(A02.split(",")));
    }

    public void A02(AbstractC14640lm r6, Integer num, boolean z) {
        C17110qH r4 = this.A08;
        long A00 = this.A02.A00();
        try {
            JSONObject A01 = r4.A01(r6);
            if (A01 == null) {
                A01 = new JSONObject();
            }
            A01.put("tb_last_action_ts", A00);
            r4.A02(r6, A01);
        } catch (JSONException unused) {
        }
        if (z) {
            C35721iZ r1 = new C35721iZ();
            r1.A00 = num;
            r1.A01 = 1;
            r1.A02 = r6.getRawString();
            C16120oU r0 = this.A06;
            r0.A07(r1);
            r0.A01();
        }
    }

    public boolean A03(C20710wC r5, C15580nU r6) {
        C15370n3 r3;
        C15550nR r1 = this.A01;
        UserJid A0D = r1.A0D(r6);
        if (A0D != null) {
            r3 = r1.A0B(A0D);
        } else {
            r3 = null;
        }
        if ((r3 == null || (!this.A00.A0F(r3.A0D) && r3.A0C == null)) && !this.A07.A01(r6) && !r5.A0X.A0F(r6) && A00(r6) == -1) {
            return true;
        }
        return false;
    }

    public boolean A04(AbstractC14640lm r3) {
        int A00 = A00(r3);
        return A00 == -1 || A00 == -2 || A00 == 0;
    }

    public boolean A05(AbstractC14640lm r9) {
        C17110qH r2 = this.A08;
        long A00 = this.A02.A00();
        try {
            JSONObject A01 = r2.A01(r9);
            if (A01 == null || !A01.has("tb_expired_ts") || !A01.has("tb_cooldown") || A01.getLong("tb_expired_ts") < A00) {
                return false;
            }
            if (!A01.has("tb_last_action_ts")) {
                return true;
            }
            if (A01.getLong("tb_last_action_ts") + A01.getLong("tb_cooldown") > A00) {
                return false;
            }
            return true;
        } catch (JSONException unused) {
            return false;
        }
    }

    public boolean A06(AbstractC14640lm r6, int i) {
        C244815r r4 = this.A05;
        AnonymousClass1PE A06 = r4.A02.A06(r6);
        if (A06 == null || A06.A03 == i) {
            return false;
        }
        A06.A03 = i;
        r4.A00.A01(new RunnableBRunnable0Shape4S0200000_I0_4(r4, 25, A06), 34);
        return true;
    }
}
