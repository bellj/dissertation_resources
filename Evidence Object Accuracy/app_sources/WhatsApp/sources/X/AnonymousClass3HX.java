package X;

import com.whatsapp.VideoFrameConverter;
import java.nio.ByteBuffer;

/* renamed from: X.3HX  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3HX {
    public static final int[] A0A = {2, 3, 4, 6, 8};
    public ByteBuffer A00;
    public final C89444Jz A01;
    public final C94064bC A02 = new C94064bC();
    public final C64113Eh A03;
    public final AnonymousClass4Q1 A04 = new AnonymousClass4Q1();
    public final AbstractC115975Tq A05;
    public final Object A06 = C12970iu.A0l();
    public volatile boolean A07;
    public volatile boolean A08;
    public volatile boolean A09;

    public AnonymousClass3HX(AnonymousClass4K0 r5) {
        C1101954p r3 = new C1101954p(this);
        this.A05 = r3;
        this.A01 = new C89444Jz(this);
        this.A03 = new C64113Eh(new AbstractC115965Tp() { // from class: X.54o
            @Override // X.AbstractC115965Tp
            public final void AT1(C49262Kb r2) {
                AbstractC467527b r0 = AnonymousClass4K0.this.A00.A00;
                if (r0 != null) {
                    r0.AUS(r2);
                }
            }
        }, r3, 3);
    }

    public final AnonymousClass4Q1 A00() {
        if (!this.A08) {
            return null;
        }
        Object obj = this.A06;
        synchronized (obj) {
            this.A07 = true;
            while (!this.A09 && this.A08) {
                try {
                    obj.wait();
                } catch (InterruptedException unused) {
                }
            }
            if (this.A08) {
                C94064bC r4 = this.A02;
                byte[] bArr = r4.A02;
                if (bArr != null) {
                    AnonymousClass4Q1 r1 = this.A04;
                    r1.A02 = bArr;
                    r1.A01 = r4.A01;
                    r1.A00 = r4.A00;
                } else if (r4.A03 != null) {
                    int[] iArr = A0A;
                    int i = 640;
                    int i2 = 480;
                    for (int i3 : iArr) {
                        i = (r4.A01 * i3) / 8;
                        i2 = (i3 * r4.A00) / 8;
                        if (i >= 640 && i2 >= 480) {
                            break;
                        }
                    }
                    if (this.A08) {
                        int i4 = i * i2;
                        ByteBuffer byteBuffer = this.A00;
                        if (byteBuffer == null || byteBuffer.capacity() != i4) {
                            ByteBuffer allocateDirect = ByteBuffer.allocateDirect(i4);
                            this.A00 = allocateDirect;
                            AnonymousClass4Q1 r12 = this.A04;
                            r12.A02 = allocateDirect.array();
                            r12.A01 = i;
                            r12.A00 = i2;
                        }
                        AnonymousClass6MI r0 = r4.A03[0];
                        VideoFrameConverter.scalePlane(r0.ACN(), r0.AGK(), r4.A01, r4.A00, this.A00, i, i, i2);
                    }
                }
                this.A07 = false;
                obj.notify();
                return this.A04;
            }
            this.A07 = false;
            obj.notify();
            return null;
        }
    }

    public void A01() {
        if (this.A08) {
            this.A08 = false;
            this.A03.A00();
        }
    }

    public final void A02(C130045yi r7) {
        if (this.A08) {
            Object obj = this.A06;
            synchronized (obj) {
                if (this.A07) {
                    this.A02.A01(r7.A02(), r7.A03(), r7.A01(), r7.A00());
                    this.A09 = true;
                    obj.notify();
                    while (this.A07 && this.A08) {
                        try {
                            obj.wait();
                        } catch (InterruptedException unused) {
                        }
                    }
                    this.A09 = false;
                }
            }
        }
    }
}
