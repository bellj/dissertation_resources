package X;

import java.util.List;

/* renamed from: X.2vv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60102vv extends C37261lu {
    public C60102vv(AnonymousClass2JB r1, AnonymousClass2JC r2, AnonymousClass2JD r3, AnonymousClass2JE r4, AnonymousClass2JF r5, C14850m9 r6) {
        super(r1, r2, r3, r4, r5, r6);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0008, code lost:
        if (r3 == 11) goto L_0x000a;
     */
    @Override // X.C37261lu
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View A0E(int r3, android.view.ViewGroup r4) {
        /*
            r2 = this;
            if (r3 == 0) goto L_0x000a
            r0 = 3
            if (r3 == r0) goto L_0x000a
            r0 = 11
            r1 = 0
            if (r3 != r0) goto L_0x000b
        L_0x000a:
            r1 = 1
        L_0x000b:
            java.lang.String r0 = "Unknown view holder type in HScroll"
            X.AnonymousClass009.A0A(r0, r1)
            android.view.View r0 = super.A0E(r3, r4)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C60102vv.A0E(int, android.view.ViewGroup):android.view.View");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0008, code lost:
        if (r4 == 11) goto L_0x000a;
     */
    @Override // X.C37261lu
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AbstractC55202hx A0F(android.view.View r3, int r4) {
        /*
            r2 = this;
            if (r4 == 0) goto L_0x000a
            r0 = 3
            if (r4 == r0) goto L_0x000a
            r0 = 11
            r1 = 0
            if (r4 != r0) goto L_0x000b
        L_0x000a:
            r1 = 1
        L_0x000b:
            java.lang.String r0 = "Unknown view holder type"
            X.AnonymousClass009.A0A(r0, r1)
            X.2hx r0 = super.A0F(r3, r4)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C60102vv.A0F(android.view.View, int):X.2hx");
    }

    @Override // X.C37261lu, X.AnonymousClass02M
    public int getItemViewType(int i) {
        C64363Fg r1;
        if (i < 0) {
            return 11;
        }
        List list = this.A0D;
        if (i >= list.size() || (r1 = (C64363Fg) list.get(i)) == null || r1.A0A) {
            return 11;
        }
        return r1.A09 ? 3 : 0;
    }
}
