package X;

import java.util.List;

/* renamed from: X.1eS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C33551eS implements AbstractC21730xt {
    public final C18640sm A00;
    public final C17220qS A01;
    public final C33541eR A02;
    public final List A03;

    public C33551eS(C18640sm r1, C17220qS r2, C33541eR r3, List list) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
        this.A03 = list;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        this.A02.A00(-1);
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r3, String str) {
        int A00 = C41151sz.A00(r3);
        if (A00 > 0) {
            this.A02.A00(A00);
        } else {
            this.A02.A00(0);
        }
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r4, String str) {
        C33541eR r2 = this.A02;
        C33361ds A00 = AnonymousClass2SR.A00(r4);
        C23000zz r1 = r2.A01;
        r1.A01(A00);
        if (r2.A03) {
            AnonymousClass164 r0 = r1.A08;
            r0.A00().edit().putInt("tos_fetch_iteration", r2.A00).apply();
        }
    }
}
