package X;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import java.lang.reflect.Method;

/* renamed from: X.0XR  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0XR implements AbstractC12600iB {
    public static Method A0Q;
    public static Method A0R;
    public static Method A0S;
    public int A00 = 0;
    public int A01 = -2;
    public int A02;
    public int A03;
    public int A04 = -2;
    public int A05 = 1002;
    public int A06 = Integer.MAX_VALUE;
    public Context A07;
    public DataSetObserver A08;
    public Rect A09;
    public View A0A;
    public AdapterView.OnItemClickListener A0B;
    public ListAdapter A0C;
    public PopupWindow A0D;
    public C02360Bs A0E;
    public Runnable A0F;
    public boolean A0G;
    public boolean A0H;
    public boolean A0I;
    public boolean A0J;
    public final Rect A0K = new Rect();
    public final Handler A0L;
    public final RunnableC09070cJ A0M = new RunnableC09070cJ(this);
    public final C07090Wq A0N = new C07090Wq(this);
    public final AnonymousClass0WS A0O = new AnonymousClass0WS(this);
    public final RunnableC09080cK A0P = new RunnableC09080cK(this);

    static {
        if (Build.VERSION.SDK_INT <= 28) {
            try {
                A0R = PopupWindow.class.getDeclaredMethod("setClipToScreenEnabled", Boolean.TYPE);
            } catch (NoSuchMethodException unused) {
                Log.i("ListPopupWindow", "Could not find method setClipToScreenEnabled() on PopupWindow. Oh well.");
            }
            try {
                A0S = PopupWindow.class.getDeclaredMethod("setEpicenterBounds", Rect.class);
            } catch (NoSuchMethodException unused2) {
                Log.i("ListPopupWindow", "Could not find method setEpicenterBounds(Rect) on PopupWindow. Oh well.");
            }
        }
        if (Build.VERSION.SDK_INT <= 23) {
            try {
                A0Q = PopupWindow.class.getDeclaredMethod("getMaxAvailableHeight", View.class, Integer.TYPE, Boolean.TYPE);
            } catch (NoSuchMethodException unused3) {
                Log.i("ListPopupWindow", "Could not find method getMaxAvailableHeight(View, int, boolean) on PopupWindow. Oh well.");
            }
        }
    }

    public AnonymousClass0XR(Context context, AttributeSet attributeSet, int i, int i2) {
        this.A07 = context;
        this.A0L = new Handler(context.getMainLooper());
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass07O.A0D, i, i2);
        this.A02 = obtainStyledAttributes.getDimensionPixelOffset(0, 0);
        int dimensionPixelOffset = obtainStyledAttributes.getDimensionPixelOffset(1, 0);
        this.A03 = dimensionPixelOffset;
        if (dimensionPixelOffset != 0) {
            this.A0G = true;
        }
        obtainStyledAttributes.recycle();
        AnonymousClass0Bv r0 = new AnonymousClass0Bv(context, attributeSet, i, i2);
        this.A0D = r0;
        r0.setInputMethodMode(1);
    }

    public C02360Bs A00(Context context, boolean z) {
        return new C02360Bs(context, z);
    }

    public void A01(int i) {
        Drawable background = this.A0D.getBackground();
        if (background != null) {
            Rect rect = this.A0K;
            background.getPadding(rect);
            this.A04 = rect.left + rect.right + i;
            return;
        }
        this.A04 = i;
    }

    public Drawable AAm() {
        return this.A0D.getBackground();
    }

    public int ADN() {
        return this.A02;
    }

    @Override // X.AbstractC12600iB
    public ListView ADv() {
        return this.A0E;
    }

    public int AHX() {
        if (!this.A0G) {
            return 0;
        }
        return this.A03;
    }

    @Override // X.AbstractC12600iB
    public boolean AK4() {
        return this.A0D.isShowing();
    }

    @Override // X.AbstractC12740iQ
    public void Abi(ListAdapter listAdapter) {
        DataSetObserver dataSetObserver = this.A08;
        if (dataSetObserver == null) {
            this.A08 = new C020109o(this);
        } else {
            ListAdapter listAdapter2 = this.A0C;
            if (listAdapter2 != null) {
                listAdapter2.unregisterDataSetObserver(dataSetObserver);
            }
        }
        this.A0C = listAdapter;
        if (listAdapter != null) {
            listAdapter.registerDataSetObserver(this.A08);
        }
        C02360Bs r1 = this.A0E;
        if (r1 != null) {
            r1.setAdapter(this.A0C);
        }
    }

    public void Abn(Drawable drawable) {
        this.A0D.setBackgroundDrawable(drawable);
    }

    public void AcC(int i) {
        this.A02 = i;
    }

    public void Ad7(int i) {
        this.A03 = i;
        this.A0G = true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:103:0x01d7  */
    @Override // X.AbstractC12600iB
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void Ade() {
        /*
        // Method dump skipped, instructions count: 512
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0XR.Ade():void");
    }

    @Override // X.AbstractC12600iB
    public void dismiss() {
        PopupWindow popupWindow = this.A0D;
        popupWindow.dismiss();
        popupWindow.setContentView(null);
        this.A0E = null;
        this.A0L.removeCallbacks(this.A0P);
    }
}
