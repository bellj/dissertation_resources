package X;

import android.animation.ValueAnimator;
import com.whatsapp.qrcode.QrEducationView;

/* renamed from: X.3Jl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C65403Jl implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ QrEducationView A00;

    public C65403Jl(QrEducationView qrEducationView) {
        this.A00 = qrEducationView;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        QrEducationView qrEducationView = this.A00;
        float A00 = C12960it.A00(valueAnimator);
        if (A00 < qrEducationView.A00) {
            qrEducationView.A0D = true;
        }
        if (qrEducationView.A0D) {
            A00 += 1.0f;
        }
        qrEducationView.A00 = A00;
        qrEducationView.invalidate();
    }
}
