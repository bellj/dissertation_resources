package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.redex.IDxCreatorShape0S0000000_I1;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.0Ec  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Ec extends C02280As {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape0S0000000_I1(20);
    public Set A00;

    public AnonymousClass0Ec(Parcel parcel) {
        super(parcel);
        int readInt = parcel.readInt();
        this.A00 = new HashSet();
        String[] strArr = new String[readInt];
        parcel.readStringArray(strArr);
        Collections.addAll(this.A00, strArr);
    }

    public AnonymousClass0Ec(Parcelable parcelable) {
        super(parcelable);
    }

    @Override // android.view.AbsSavedState, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.A00.size());
        Set set = this.A00;
        parcel.writeStringArray((String[]) set.toArray(new String[set.size()]));
    }
}
