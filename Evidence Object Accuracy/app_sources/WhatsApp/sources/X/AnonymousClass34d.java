package X;

import android.content.Context;
import java.util.List;

/* renamed from: X.34d  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass34d extends AbstractC51472Uz {
    public C44061y8 A00;
    public boolean A01;

    public AnonymousClass34d(Context context, C15570nT r2, C15550nR r3, C15610nY r4, C63563Cb r5, C63543Bz r6, AnonymousClass01d r7, C14830m7 r8, AnonymousClass018 r9, AnonymousClass19M r10, C16630pM r11, AnonymousClass12F r12) {
        super(context, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12);
        A00();
    }

    @Override // X.AnonymousClass2V0
    public /* bridge */ /* synthetic */ void A04(C15370n3 r7, C15370n3 r8, AbstractC15340mz r9, List list) {
        C30421Xi r5 = (C30421Xi) r9;
        super.A04(r7, r8, r5, list);
        setContentDescription(C65023Hv.A01(getContext(), this.A09, this.A0A, this.A0E, this.A0F, r5));
    }

    /* renamed from: A07 */
    public void A05(C30421Xi r2, List list) {
        super.A05(r2, list);
        this.A00.setAudioMessage(r2);
    }

    public void setVoiceNoteAttachmentView(C44061y8 r1) {
        this.A00 = r1;
    }
}
