package X;

import X.AnonymousClass2NW;
import android.graphics.Point;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.media.Image;
import android.media.ImageReader;
import android.os.Handler;
import android.view.Surface;
import com.facebook.redex.RunnableBRunnable0Shape13S0100000_I0_13;
import com.whatsapp.VideoFrameConverter;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.VideoPort;
import com.whatsapp.voipcalling.camera.VoipCamera;
import com.whatsapp.voipcalling.camera.VoipPhysicalCamera;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.2NW  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2NW extends VoipPhysicalCamera {
    public static final AtomicBoolean A0H = new AtomicBoolean(false);
    public static final int[] A0I = {2, 0, 1, 3};
    public int A00 = 0;
    public Point A01;
    public CameraDevice A02;
    public Image A03;
    public Surface A04;
    public ByteBuffer A05;
    public boolean A06 = false;
    public boolean A07 = false;
    public final int A08;
    public final CameraCharacteristics A09;
    public final CameraDevice.StateCallback A0A = new C51862Zk(this);
    public final CameraManager A0B;
    public final ImageReader A0C;
    public final AnonymousClass01d A0D;
    public final AbstractC14440lR A0E;
    public final AnonymousClass2NT A0F;
    public final Object A0G = new Object();

    @Override // com.whatsapp.voipcalling.camera.VoipPhysicalCamera
    public int getCameraStartMode() {
        return 0;
    }

    public AnonymousClass2NW(AnonymousClass01d r13, C14850m9 r14, AbstractC14440lR r15, int i, int i2, int i3, int i4, int i5) {
        super(r14);
        this.A0D = r13;
        this.A0E = r15;
        StringBuilder sb = new StringBuilder("voip/video/VoipCamera/create idx: ");
        sb.append(i);
        sb.append(", size:");
        sb.append(i2);
        sb.append("x");
        sb.append(i3);
        sb.append(", format: 0x");
        sb.append(Integer.toHexString(i4));
        sb.append(", fps * 1000: ");
        sb.append(i5);
        sb.append(", api 2, this ");
        sb.append(this);
        Log.i(sb.toString());
        CameraManager A0E = r13.A0E();
        AnonymousClass009.A05(A0E);
        this.A0B = A0E;
        this.A08 = i;
        try {
            CameraCharacteristics cameraCharacteristics = A0E.getCameraCharacteristics(Integer.toString(i));
            this.A09 = cameraCharacteristics;
            Object obj = cameraCharacteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
            AnonymousClass009.A05(obj);
            Number number = (Number) obj;
            Object obj2 = cameraCharacteristics.get(CameraCharacteristics.LENS_FACING);
            AnonymousClass009.A05(obj2);
            AnonymousClass2NT r4 = new AnonymousClass2NT(i2, i3, i4, i5, number.intValue(), i, ((Number) obj2).intValue() == 0);
            this.A0F = r4;
            ImageReader newInstance = ImageReader.newInstance(r4.A05, r4.A02, r4.A00, 3);
            this.A0C = newInstance;
            if (newInstance != null) {
                C98004hz r2 = new ImageReader.OnImageAvailableListener() { // from class: X.4hz
                    @Override // android.media.ImageReader.OnImageAvailableListener
                    public final void onImageAvailable(ImageReader imageReader) {
                        AnonymousClass2NW.A04(imageReader, AnonymousClass2NW.this);
                    }
                };
                AbstractC1311461l r1 = this.cameraProcessor;
                Handler handler = this.cameraThreadHandler;
                if (r1 != null) {
                    r1.AcE(r2, handler);
                    newInstance.setOnImageAvailableListener(this.cameraProcessor, this.cameraThreadHandler);
                    return;
                }
                newInstance.setOnImageAvailableListener(r2, handler);
                return;
            }
            throw new RuntimeException("Unable to create image reader");
        } catch (CameraAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public static int A00(AnonymousClass01d r12) {
        CameraManager A0E = r12.A0E();
        if (A0E == null) {
            Log.e("voip/video/VoipCamera/getLowestCam2HardwareLevel CameraManager is null");
        } else {
            int[] iArr = A0I;
            int length = iArr.length;
            try {
                if (A0E.getCameraIdList().length > 0) {
                    for (String str : A0E.getCameraIdList()) {
                        Integer num = (Integer) A0E.getCameraCharacteristics(str).get(CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL);
                        if (num != null) {
                            int i = 0;
                            while (true) {
                                if (i >= length) {
                                    break;
                                } else if (num.intValue() == iArr[i]) {
                                    length = i;
                                    break;
                                } else {
                                    i++;
                                }
                            }
                        }
                    }
                }
                if (length < length) {
                    return iArr[length];
                }
            } catch (AssertionError | Exception e) {
                Log.e("voip/video/VoipCamera/getLowestCam2HardwareLevel unable to acquire camera info", e);
                return -1;
            }
        }
        return -1;
    }

    public static /* synthetic */ void A04(ImageReader imageReader, AnonymousClass2NW r16) {
        Image acquireLatestImage = imageReader.acquireLatestImage();
        if (acquireLatestImage != null) {
            for (Map.Entry entry : r16.virtualCameras.entrySet()) {
                if (((VoipCamera) entry.getValue()).started) {
                    r16.updateCameraCallbackCheck();
                    Image.Plane plane = acquireLatestImage.getPlanes()[0];
                    Image.Plane plane2 = acquireLatestImage.getPlanes()[1];
                    Image.Plane plane3 = acquireLatestImage.getPlanes()[2];
                    ((VoipCamera) entry.getValue()).framePlaneCallback(acquireLatestImage.getWidth(), acquireLatestImage.getHeight(), plane.getBuffer(), plane.getRowStride(), plane2.getBuffer(), plane2.getRowStride(), plane3.getBuffer(), plane3.getRowStride(), plane2.getPixelStride());
                }
            }
            synchronized (r16.A0G) {
                Image image = r16.A03;
                if (image != null) {
                    image.close();
                }
                r16.A03 = acquireLatestImage;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004a, code lost:
        if (r0 == false) goto L_0x000d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A06(java.lang.String r5, int r6) {
        /*
            java.util.Locale r0 = java.util.Locale.US
            java.lang.String r1 = r5.toUpperCase(r0)
            int r0 = r1.hashCode()
            switch(r0) {
                case -2053249079: goto L_0x0043;
                case -1038134325: goto L_0x003b;
                case 2169487: goto L_0x0033;
                case 787768856: goto L_0x002b;
                case 894099834: goto L_0x0023;
                default: goto L_0x000d;
            }
        L_0x000d:
            r5 = -1
        L_0x000e:
            if (r5 != r6) goto L_0x0012
            r4 = 1
        L_0x0011:
            return r4
        L_0x0012:
            int[] r3 = X.AnonymousClass2NW.A0I
            int r2 = r3.length
            r1 = 0
            r4 = 0
        L_0x0017:
            if (r1 >= r2) goto L_0x004d
            r0 = r3[r1]
            if (r0 != r5) goto L_0x001e
            r4 = 1
        L_0x001e:
            if (r0 == r6) goto L_0x0011
            int r1 = r1 + 1
            goto L_0x0017
        L_0x0023:
            java.lang.String r0 = "LIMITED"
            boolean r0 = r1.equals(r0)
            r5 = 0
            goto L_0x004a
        L_0x002b:
            java.lang.String r0 = "LEVEL_3"
            boolean r0 = r1.equals(r0)
            r5 = 3
            goto L_0x004a
        L_0x0033:
            java.lang.String r0 = "FULL"
            boolean r0 = r1.equals(r0)
            r5 = 1
            goto L_0x004a
        L_0x003b:
            java.lang.String r0 = "EXTERNAL"
            boolean r0 = r1.equals(r0)
            r5 = 4
            goto L_0x004a
        L_0x0043:
            java.lang.String r0 = "LEGACY"
            boolean r0 = r1.equals(r0)
            r5 = 2
        L_0x004a:
            if (r0 != 0) goto L_0x000e
            goto L_0x000d
        L_0x004d:
            r4 = 0
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2NW.A06(java.lang.String, int):boolean");
    }

    public final int A07() {
        Surface surface;
        Log.i("voip/video/VoipCamera/ starting camera");
        if (this.A02 == null || this.videoPort == null) {
            return -5;
        }
        AnonymousClass2NT r0 = this.A0F;
        int i = r0.A05;
        int i2 = r0.A02;
        createTexture(i, i2);
        C92444Vx r1 = this.textureHolder;
        if (r1 == null) {
            return -1;
        }
        AbstractC1311461l r02 = this.cameraProcessor;
        if (r02 != null) {
            SurfaceTexture ABC = r02.ABC();
            boolean z = false;
            if (ABC != null) {
                z = true;
            }
            AnonymousClass009.A0A("Camera processor should output non null texture", z);
            ABC.setDefaultBufferSize(i, i2);
            surface = new Surface(ABC);
        } else {
            surface = new Surface(r1.A01);
        }
        this.A04 = surface;
        A08();
        try {
            CaptureRequest.Builder createCaptureRequest = this.A02.createCaptureRequest(1);
            createCaptureRequest.addTarget(this.A04);
            ImageReader imageReader = this.A0C;
            createCaptureRequest.addTarget(imageReader.getSurface());
            this.A07 = true;
            this.A02.createCaptureSession(Arrays.asList(this.A04, imageReader.getSurface()), new C51852Zj(createCaptureRequest, this), this.cameraThreadHandler);
            return 0;
        } catch (CameraAccessException e) {
            Log.e("startCaptureSessionOnCameraThread", e);
            return -2;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001c, code lost:
        if (r4 == 2) goto L_0x001e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A08() {
        /*
            r5 = this;
            X.01d r0 = r5.A0D
            android.view.WindowManager r0 = r0.A0O()
            android.view.Display r0 = r0.getDefaultDisplay()
            int r4 = r0.getRotation()
            X.2NT r3 = r5.A0F
            int r0 = r3.A04
            int r0 = r0 % 180
            r2 = 0
            if (r0 != 0) goto L_0x0018
            r2 = 1
        L_0x0018:
            if (r4 == 0) goto L_0x001e
            r1 = 2
            r0 = 0
            if (r4 != r1) goto L_0x001f
        L_0x001e:
            r0 = 1
        L_0x001f:
            if (r2 != r0) goto L_0x0035
            int r2 = r3.A05
            int r1 = r3.A02
        L_0x0025:
            android.graphics.Point r0 = new android.graphics.Point
            r0.<init>(r2, r1)
            r5.A01 = r0
            X.4Vx r1 = r5.textureHolder
            if (r1 == 0) goto L_0x0034
            int r0 = 4 - r4
            r1.A04 = r0
        L_0x0034:
            return
        L_0x0035:
            int r2 = r3.A02
            int r1 = r3.A05
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2NW.A08():void");
    }

    @Override // com.whatsapp.voipcalling.camera.VoipPhysicalCamera
    public void closeOnCameraThread() {
        int i = this.A00;
        boolean z = true;
        if (!(i == 0 || i == 1)) {
            z = false;
        }
        StringBuilder sb = new StringBuilder("closing camera while still open: ");
        sb.append(i);
        AnonymousClass009.A0A(sb.toString(), z);
        this.cameraEventsDispatcher.A00();
        this.A0C.close();
        synchronized (this.A0G) {
            Image image = this.A03;
            if (image != null) {
                image.close();
                this.A03 = null;
            }
        }
    }

    @Override // com.whatsapp.voipcalling.camera.VoipPhysicalCamera
    public Point getAdjustedPreviewSize() {
        return this.A01;
    }

    @Override // com.whatsapp.voipcalling.camera.VoipPhysicalCamera
    public AnonymousClass2NT getCameraInfo() {
        return this.A0F;
    }

    @Override // com.whatsapp.voipcalling.camera.VoipPhysicalCamera
    public AnonymousClass2NY getLastCachedFrame() {
        synchronized (this.A0G) {
            Image image = this.A03;
            if (image == null) {
                return null;
            }
            int width = ((image.getWidth() * 3) * this.A03.getHeight()) / 2;
            ByteBuffer byteBuffer = this.A05;
            if (byteBuffer == null || byteBuffer.capacity() != width) {
                this.A05 = ByteBuffer.allocateDirect(width);
            }
            Image.Plane plane = this.A03.getPlanes()[0];
            Image.Plane plane2 = this.A03.getPlanes()[1];
            Image.Plane plane3 = this.A03.getPlanes()[2];
            VideoFrameConverter.convertAndroid420toI420(plane.getBuffer(), plane.getRowStride(), plane2.getBuffer(), plane2.getRowStride(), plane3.getBuffer(), plane3.getRowStride(), plane2.getPixelStride(), this.A03.getWidth(), this.A03.getHeight(), this.A05);
            byte[] bArr = new byte[width];
            this.A05.rewind();
            this.A05.get(bArr);
            AnonymousClass2NY r2 = new AnonymousClass2NY();
            r2.A05 = bArr;
            AnonymousClass2NT r1 = this.A0F;
            r2.A03 = r1.A05;
            r2.A01 = r1.A02;
            r2.A00 = r1.A00;
            r2.A02 = r1.A04;
            r2.A04 = r1.A06;
            return r2;
        }
    }

    @Override // com.whatsapp.voipcalling.camera.VoipPhysicalCamera
    public int getLatestFrame(ByteBuffer byteBuffer) {
        throw new UnsupportedOperationException("VoipCameraApi2 does not support this operation ATM");
    }

    @Override // com.whatsapp.voipcalling.camera.VoipPhysicalCamera
    public void onFrameAvailableOnCameraThread() {
        VideoPort videoPort = this.videoPort;
        if (videoPort == null) {
            Log.e("voip/video/VoipCamera/videoport null while receiving frames");
            return;
        }
        C92444Vx r2 = this.textureHolder;
        if (r2 != null) {
            AnonymousClass2NT r0 = this.A0F;
            videoPort.renderTexture(r2, r0.A05, r0.A02);
        }
    }

    @Override // com.whatsapp.voipcalling.camera.VoipPhysicalCamera
    public int setVideoPortOnCameraThread(VideoPort videoPort) {
        int i;
        int i2;
        VideoPort videoPort2 = this.videoPort;
        if (videoPort2 != videoPort) {
            StringBuilder sb = new StringBuilder("voip/video/VoipCamera/setVideoPortOnCameraThread to ");
            if (videoPort != null) {
                i = videoPort.hashCode();
            } else {
                i = 0;
            }
            sb.append(i);
            sb.append(" from ");
            if (videoPort2 != null) {
                i2 = videoPort2.hashCode();
            } else {
                i2 = 0;
            }
            sb.append(i2);
            sb.append(", status: ");
            sb.append(this.A00);
            Log.i(sb.toString());
            if (videoPort == null) {
                stopOnCameraThread();
                this.videoPort = null;
            } else if (this.A07) {
                return -10;
            } else {
                this.videoPort = videoPort;
                videoPort.setScaleType(0);
                int i3 = this.A00;
                if (i3 == 2) {
                    if (videoPort2 != null) {
                        releaseTexture();
                    }
                    return A07();
                } else if (i3 == 0 || i3 == 1) {
                    return startOnCameraThread();
                }
            }
        }
        return 0;
    }

    @Override // com.whatsapp.voipcalling.camera.VoipPhysicalCamera
    public int startOnCameraThread() {
        int i = this.A00;
        if (!(i == 2 || i == 3 || this.videoPort == null)) {
            if (i == 1) {
                Log.i("voip/video/VoipCamera/ Camera device is not fully closed, create camera device on close");
                this.A06 = true;
            } else if (A0H.get()) {
                Log.i("voip/video/VoipCamera/retryOpen");
                this.cameraThreadHandler.postDelayed(new RunnableBRunnable0Shape13S0100000_I0_13(this, 45), 250);
                return 0;
            } else {
                try {
                    Log.i("voip/video/VoipCamera/ opening camera");
                    this.A0B.openCamera(Integer.toString(this.A08), this.A0A, this.cameraThreadHandler);
                    this.A00 = 3;
                    return 0;
                } catch (CameraAccessException e) {
                    Log.e("voip/video/VoipCamera/ failed to open camera ", e);
                    return -4;
                } catch (IllegalArgumentException e2) {
                    Log.e("voip/video/VoipCamera/ failed to open camera due to crashed HAL ", e2);
                    return -4;
                }
            }
        }
        return 0;
    }

    @Override // com.whatsapp.voipcalling.camera.VoipPhysicalCamera
    public int stopOnCameraThread() {
        stopPeriodicCameraCallbackCheck();
        CameraDevice cameraDevice = this.A02;
        if (cameraDevice != null) {
            this.A02 = null;
            this.A00 = 1;
            A0H.set(true);
            this.A0E.Ab6(new Runnable(cameraDevice) { // from class: com.whatsapp.voipcalling.camera.VoipCameraApi2$StopCameraRunnable
                public final CameraDevice closingCameraDevice;

                {
                    this.closingCameraDevice = r2;
                }

                @Override // java.lang.Runnable
                public void run() {
                    this.closingCameraDevice.close();
                    AnonymousClass2NW.A0H.set(false);
                }
            });
        }
        Surface surface = this.A04;
        if (surface != null) {
            surface.release();
        }
        releaseTexture();
        this.A07 = false;
        return 0;
    }

    @Override // com.whatsapp.voipcalling.camera.VoipPhysicalCamera
    public void updatePreviewOrientation() {
        if (this.videoPort != null) {
            syncRunOnCameraThread(new Callable() { // from class: X.5Df
                @Override // java.util.concurrent.Callable
                public final Object call() {
                    return AnonymousClass2NW.A03(AnonymousClass2NW.this);
                }
            }, -100);
        }
    }
}
