package X;

import android.content.Context;

/* renamed from: X.5xW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129315xW {
    public static C16590pI A0B;
    public static C124215on A0C;
    public static C124165oi A0D;
    public final AbstractC15710nm A00;
    public final C17220qS A01;
    public final C120445gC A02;
    public final AbstractC136436Mn A03;
    public final AbstractC17860rW A04;
    public final C18600si A05;
    public final AnonymousClass162 A06;
    public final C18590sh A07;
    public final AbstractC14440lR A08;
    public final C130165yu A09;
    public final C126615tA A0A;

    public C129315xW(AbstractC15710nm r18, C14900mE r19, C15570nT r20, C16590pI r21, C17220qS r22, C1329668y r23, AbstractC136436Mn r24, C18650sn r25, C64513Fv r26, AbstractC17860rW r27, C18600si r28, C18610sj r29, AnonymousClass162 r30, AnonymousClass6BE r31, C18590sh r32, AbstractC14440lR r33, C130165yu r34) {
        this.A00 = r18;
        this.A08 = r33;
        this.A01 = r22;
        this.A07 = r32;
        this.A05 = r28;
        this.A09 = r34;
        this.A06 = r30;
        this.A04 = r27;
        this.A03 = r24;
        Context context = r21.A00;
        A0B = r21;
        if (C126615tA.A02 == null) {
            synchronized (C126615tA.class) {
                if (C126615tA.A02 == null) {
                    C126615tA.A02 = new C126615tA();
                    C126615tA.A01 = r34;
                }
            }
        }
        C126615tA r0 = C126615tA.A02;
        this.A0A = r0;
        String A04 = r20.A04();
        AnonymousClass009.A05(A04);
        AnonymousClass009.A05(r0);
        this.A02 = new C120445gC(context, r19, r22, r23, r24, r25, r26, r29, r31, r32, r33, A04, r0);
    }

    public synchronized void A00() {
        C18590sh r8 = this.A07;
        C126615tA r10 = this.A0A;
        AbstractC15710nm r3 = this.A00;
        boolean A1T = C12960it.A1T(this.A05.A01().getBoolean("payments_sandbox", false) ? 1 : 0);
        AbstractC136436Mn r5 = this.A03;
        C120445gC r4 = this.A02;
        C130165yu r9 = this.A09;
        C124215on r2 = new C124215on(r3, r4, r5, this.A04, this.A06, r8, r9, r10, A1T);
        A0C = r2;
        C12990iw.A1N(r2, this.A08);
    }
}
