package X;

/* renamed from: X.520  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass520 implements AnonymousClass5T7 {
    @Override // X.AnonymousClass5T7
    public boolean A9i(AbstractC94534c0 r3, AbstractC94534c0 r4, AnonymousClass4RG r5) {
        return !((AnonymousClass5T7) AnonymousClass4G0.A00.get(AnonymousClass4BP.A05)).A9i(r3, r4, r5);
    }
}
