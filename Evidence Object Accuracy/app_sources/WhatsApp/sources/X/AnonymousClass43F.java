package X;

/* renamed from: X.43F  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass43F extends AbstractC16110oT {
    public Integer A00;

    public AnonymousClass43F() {
        super(2908, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(2, null);
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamWaShopsManagement {");
        String str = null;
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "isShopsProductPreviewVisible", null);
        Integer num = this.A00;
        if (num != null) {
            str = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "shopsManagementAction", str);
        return C12960it.A0d("}", A0k);
    }
}
