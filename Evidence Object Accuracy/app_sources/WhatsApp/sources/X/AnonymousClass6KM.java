package X;

import android.hardware.camera2.CameraAccessException;
import java.util.concurrent.Callable;

/* renamed from: X.6KM  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6KM implements Callable {
    public final /* synthetic */ AnonymousClass662 A00;

    public AnonymousClass6KM(AnonymousClass662 r1) {
        this.A00 = r1;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        AnonymousClass61Q r2 = this.A00.A0Y;
        C129805yK r1 = r2.A0H;
        r1.A01("Can only check if is retrieving preview frames from the Optic thread");
        r1.A01("Can only check if the prepared on the Optic thread");
        if (r1.A00 && r2.A0R) {
            return null;
        }
        try {
            r2.A0F(true, false);
            return null;
        } catch (CameraAccessException | IllegalArgumentException unused) {
            return null;
        } catch (Exception e) {
            throw new AnonymousClass6L0(C12960it.A0d(e.getMessage(), C12960it.A0k("Could not start preview: ")));
        }
    }
}
