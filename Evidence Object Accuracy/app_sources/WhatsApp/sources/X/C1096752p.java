package X;

import androidx.core.view.inputmethod.EditorInfoCompat;

/* renamed from: X.52p  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1096752p implements AbstractC116455Vm {
    public final /* synthetic */ C36271jb A00;

    public C1096752p(C36271jb r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC116455Vm
    public void AMr() {
        C12960it.A0v(this.A00.A07);
    }

    @Override // X.AbstractC116455Vm
    public void APc(int[] iArr) {
        AbstractC36671kL.A08(this.A00.A07, iArr, EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH);
    }
}
