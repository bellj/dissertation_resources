package X;

/* renamed from: X.4TO  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4TO {
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final int A04;
    public final int A05;
    public final byte[] A06;

    public AnonymousClass4TO(byte[] bArr, int i, int i2, int i3, int i4, int i5, int i6) {
        this.A04 = i;
        this.A05 = i2;
        this.A00 = i3;
        this.A01 = i4;
        this.A02 = i5;
        this.A03 = i6;
        this.A06 = bArr;
    }
}
