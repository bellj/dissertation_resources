package X;

import android.view.animation.Animation;
import android.widget.LinearLayout;

/* renamed from: X.2AY  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2AY extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ AnonymousClass2AS A00;
    public final /* synthetic */ Runnable A01;

    public AnonymousClass2AY(AnonymousClass2AS r1, Runnable runnable) {
        this.A00 = r1;
        this.A01 = runnable;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        AnonymousClass2AS r3 = this.A00;
        r3.clearAnimation();
        r3.setVisibility(8);
        r3.A0E = false;
        r3.setEnabled(true);
        r3.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.A01.run();
    }
}
