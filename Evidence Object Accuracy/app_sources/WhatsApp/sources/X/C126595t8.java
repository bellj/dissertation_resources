package X;

import android.content.Context;
import android.content.res.AssetManager;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import org.json.JSONArray;
import org.json.JSONException;

/* renamed from: X.5t8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C126595t8 {
    public JSONArray A00 = C117315Zl.A0L();

    public C126595t8(Context context) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            AssetManager assets = context.getAssets();
            StringBuilder A0h = C12960it.A0h();
            A0h.append("npci/");
            InputStream open = assets.open(C12960it.A0d("npci_otp_rules.json", A0h));
            byte[] bArr = new byte[4096];
            while (true) {
                int read = open.read(bArr);
                if (read == -1) {
                    break;
                }
                byteArrayOutputStream.write(bArr, 0, read);
            }
            byteArrayOutputStream.close();
            open.close();
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            if (byteArray != null) {
                try {
                    this.A00 = new JSONArray(new String(byteArray));
                } catch (JSONException e) {
                    throw C117315Zl.A0J(e);
                }
            }
        } catch (Exception e2) {
            throw C117315Zl.A0J(e2);
        }
    }
}
