package X;

import android.content.Context;
import android.widget.Button;
import com.whatsapp.R;
import java.util.List;

/* renamed from: X.5ke  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C121945ke extends C133766Ca {
    public C121945ke(C15550nR r1, C15610nY r2, C21270x9 r3, String str, boolean z) {
        super(r1, r2, r3, str, z);
    }

    @Override // X.C133766Ca
    public void A00(Context context, List list) {
        Button button;
        int i;
        super.A00(context, list);
        String str = this.A0E;
        if ("chat".equals(str) || "payment_composer_icon".equals(str)) {
            this.A05.setText(R.string.send_money_to_a_upi_id);
            button = this.A05;
            i = 0;
        } else {
            button = this.A05;
            i = 8;
        }
        button.setVisibility(i);
    }
}
