package X;

import com.whatsapp.jid.Jid;

/* renamed from: X.2PG  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2PG extends AnonymousClass2PA {
    public final AbstractC14640lm A00;

    public AnonymousClass2PG(AbstractC14640lm r1, Jid jid, String str, long j) {
        super(jid, str, j);
        this.A00 = r1;
    }
}
