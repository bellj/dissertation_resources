package X;

import android.content.Context;
import android.content.res.Resources;
import com.whatsapp.settings.chat.wallpaper.WallpaperImagePreview;

/* renamed from: X.38V  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass38V extends AbstractC16350or {
    public final int A00;
    public final int A01;
    public final Context A02;
    public final Resources A03;
    public final WallpaperImagePreview A04;
    public final WallpaperImagePreview A05;

    public /* synthetic */ AnonymousClass38V(Context context, Resources resources, WallpaperImagePreview wallpaperImagePreview, WallpaperImagePreview wallpaperImagePreview2, int i, int i2) {
        this.A02 = context;
        this.A05 = wallpaperImagePreview;
        this.A04 = wallpaperImagePreview2;
        this.A03 = resources;
        this.A00 = i;
        this.A01 = i2;
    }
}
