package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.40R  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass40R extends AbstractC37191le {
    public AnonymousClass40R(View view) {
        super(view);
    }

    @Override // X.AbstractC37191le
    public /* bridge */ /* synthetic */ void A09(Object obj) {
        AnonymousClass402 r6 = (AnonymousClass402) obj;
        View view = this.A0H;
        TextView textView = (TextView) view;
        int i = r6.A00;
        int i2 = R.string.biz_directory_see_all_results;
        if (i != 1) {
            i2 = R.string.biz_directory_show_more;
        }
        textView.setText(i2);
        view.setOnClickListener(r6.A01);
    }
}
