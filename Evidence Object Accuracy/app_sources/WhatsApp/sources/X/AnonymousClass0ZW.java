package X;

import android.app.Activity;
import java.util.Iterator;

/* renamed from: X.0ZW  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0ZW implements AbstractC11940h7 {
    public final /* synthetic */ C07530Zb A00;

    public AnonymousClass0ZW(C07530Zb r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC11940h7
    public void AYW(Activity activity, AnonymousClass0PZ r6) {
        C16700pc.A0E(activity, 0);
        Iterator it = this.A00.A01.iterator();
        while (it.hasNext()) {
            C05970Rr r2 = (C05970Rr) it.next();
            if (C16700pc.A0O(r2.A01, activity)) {
                r2.A00 = r6;
                r2.A03.execute(new RunnableC09620dF(r2, r6));
            }
        }
    }
}
