package X;

import java.util.List;

/* renamed from: X.0Gx  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Gx extends AnonymousClass0H3 {
    public final AnonymousClass0OM A00 = new AnonymousClass0OM(1.0f, 1.0f);

    public AnonymousClass0Gx(List list) {
        super(list);
    }

    @Override // X.AnonymousClass0QR
    public /* bridge */ /* synthetic */ Object A04(AnonymousClass0U8 r7, float f) {
        Object obj;
        Object obj2 = r7.A0F;
        if (obj2 == null || (obj = r7.A09) == null) {
            throw new IllegalStateException("Missing values for keyframe.");
        }
        AnonymousClass0OM r5 = (AnonymousClass0OM) obj2;
        AnonymousClass0OM r4 = (AnonymousClass0OM) obj;
        AnonymousClass0SF r1 = this.A03;
        if (r1 != null) {
            r7.A08.floatValue();
            A02();
            AnonymousClass0NB r0 = r1.A02;
            r0.A01 = r5;
            r0.A00 = r4;
            Object obj3 = r1.A01;
            if (obj3 != null) {
                return obj3;
            }
        }
        AnonymousClass0OM r3 = this.A00;
        float f2 = r5.A00;
        float f3 = f2 + ((r4.A00 - f2) * f);
        float f4 = r5.A01;
        r3.A00 = f3;
        r3.A01 = f4 + (f * (r4.A01 - f4));
        return r3;
    }
}
