package X;

import android.content.res.Resources;
import android.view.View;

/* renamed from: X.4Vd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92254Vd {
    public final Resources A00;
    public final View A01;
    public final AnonymousClass5UM A02;

    public C92254Vd(Resources resources, View view, AnonymousClass5UM r3) {
        this.A00 = resources;
        this.A01 = view;
        this.A02 = r3;
    }

    public void A00(int i, boolean z) {
        if (z) {
            View view = this.A01;
            C73943h3 r2 = new C73943h3(view, this, i);
            r2.setDuration((long) ((int) (((float) i) / this.A00.getDisplayMetrics().density)));
            view.startAnimation(r2);
            return;
        }
        View view2 = this.A01;
        view2.getLayoutParams().height = i;
        view2.requestLayout();
        this.A02.ATC((float) i);
    }
}
