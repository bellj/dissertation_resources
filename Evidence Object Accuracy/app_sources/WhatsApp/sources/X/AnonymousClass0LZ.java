package X;

import java.util.concurrent.CancellationException;

/* renamed from: X.0LZ  reason: invalid class name */
/* loaded from: classes.dex */
public final /* synthetic */ class AnonymousClass0LZ {
    public static final void A00(CancellationException cancellationException, AnonymousClass5X4 r2) {
        AbstractC02760Dw r0 = (AbstractC02760Dw) r2.get(AbstractC02760Dw.A00);
        if (r0 != null) {
            r0.A74(cancellationException);
        }
    }
}
