package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

/* renamed from: X.4jK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98834jK implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        String str = null;
        ArrayList arrayList = null;
        int i = 0;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                i = C95664e9.A02(parcel, readInt);
            } else if (c == 2) {
                str = C95664e9.A08(parcel, readInt);
            } else if (c != 3) {
                C95664e9.A0D(parcel, readInt);
            } else {
                arrayList = C95664e9.A0B(parcel, C78543p5.CREATOR, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78533p4(str, arrayList, i);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new C78533p4[i];
    }
}
