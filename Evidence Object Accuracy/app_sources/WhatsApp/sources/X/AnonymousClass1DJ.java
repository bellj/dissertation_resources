package X;

import java.util.List;

/* renamed from: X.1DJ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1DJ implements AbstractC16990q5 {
    public final C18470sV A00;
    public final AnonymousClass1BD A01;

    @Override // X.AbstractC16990q5
    public /* synthetic */ void AOo() {
    }

    public AnonymousClass1DJ(C18470sV r1, AnonymousClass1BD r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC16990q5
    public void AOp() {
        AnonymousClass1BD r5 = this.A01;
        List<AnonymousClass1V2> A05 = this.A00.A05();
        C855243d r4 = new C855243d();
        r4.A01 = 0L;
        r4.A03 = 0L;
        r4.A00 = 0L;
        r4.A02 = 0L;
        for (AnonymousClass1V2 r6 : A05) {
            if (!r6.A0C()) {
                r4.A01 = Long.valueOf(r4.A01.longValue() + 1);
                if (r6.A01() != r6.A02()) {
                    r4.A03 = Long.valueOf(r4.A03.longValue() + 1);
                }
                r4.A00 = Long.valueOf(r4.A00.longValue() + ((long) r6.A01()));
                r4.A02 = Long.valueOf(r4.A02.longValue() + ((long) (r6.A01() - r6.A02())));
            }
        }
        r5.A0A.A06(r4);
    }
}
