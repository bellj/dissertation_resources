package X;

import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.1H1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1H1 extends AbstractC250017s {
    public List A00 = new ArrayList();
    public final C15570nT A01;
    public final C15550nR A02;
    public final C15610nY A03;
    public final C20730wE A04;
    public final C230910i A05;
    public final C14830m7 A06;
    public final C15680nj A07;
    public final C15600nX A08;
    public final C233511i A09;
    public final AbstractC14440lR A0A;

    public AnonymousClass1H1(C15570nT r2, C15550nR r3, C15610nY r4, C20730wE r5, C230910i r6, C14830m7 r7, C15680nj r8, C15600nX r9, C233511i r10, AbstractC14440lR r11) {
        super(r10);
        this.A06 = r7;
        this.A01 = r2;
        this.A0A = r11;
        this.A02 = r3;
        this.A03 = r4;
        this.A04 = r5;
        this.A09 = r10;
        this.A05 = r6;
        this.A07 = r8;
        this.A08 = r9;
    }

    public static final void A00(Jid jid, UserJid userJid, List list, Map map) {
        UserJid of = UserJid.of(jid);
        if (of != null && !userJid.equals(jid) && map.containsKey(of)) {
            StringBuilder sb = new StringBuilder("contact-mutation-handler/populateJidList adding jid: ");
            sb.append(of);
            Log.i(sb.toString());
            list.add(of);
        }
    }

    public List A08(C27791Jf r18, Collection collection) {
        String str;
        String str2;
        C15370n3 A09;
        HashSet hashSet = new HashSet();
        ArrayList arrayList = new ArrayList(collection.size());
        long A00 = this.A06.A00();
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            UserJid userJid = (UserJid) it.next();
            if (!hashSet.contains(userJid)) {
                C27791Jf r6 = C27791Jf.A03;
                if (r18.equals(r6)) {
                    C15550nR r5 = this.A02;
                    C15570nT r4 = r5.A01;
                    if (r4.A0F(userJid)) {
                        r4.A08();
                        A09 = r4.A01;
                    } else {
                        r5.A04.A01.remove(userJid);
                        A09 = r5.A09(userJid);
                    }
                    if (A09 != null) {
                        str = A09.A0M;
                        str2 = this.A03.A04(A09);
                        arrayList.add(new C34501gF(r18, null, userJid, null, str, str2, A00));
                        hashSet.add(userJid);
                    }
                }
                if (r18.equals(r6)) {
                    StringBuilder sb = new StringBuilder("contact-sync-handler/create-contact-mutations given contact ");
                    sb.append(userJid);
                    sb.append(" doesn't exist in DB but should");
                    Log.e(sb.toString());
                } else {
                    str = null;
                    str2 = null;
                    arrayList.add(new C34501gF(r18, null, userJid, null, str, str2, A00));
                    hashSet.add(userJid);
                }
            }
        }
        return arrayList;
    }

    public List A09(List list) {
        C15570nT r0 = this.A01;
        r0.A08();
        C27631Ih r7 = r0.A05;
        if (r7 == null) {
            Log.e("contact-mutation-handler/createBootstrapMutations me is null");
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList();
        this.A02.A0T(arrayList);
        HashMap hashMap = new HashMap();
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            C15370n3 r1 = (C15370n3) it.next();
            UserJid of = UserJid.of(r1.A0D);
            if (of != null) {
                hashMap.put(of, r1);
            }
        }
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        ArrayList arrayList4 = new ArrayList();
        Iterator it2 = list.iterator();
        while (it2.hasNext()) {
            AnonymousClass1JY r2 = (AnonymousClass1JY) it2.next();
            AbstractC14640lm r8 = r2.A00.A0C;
            if (C15380n4.A0L(r8)) {
                Log.i("contact-mutation-handler/createBootstrapMutations attempting to add jid from 1:1 chat");
                A00(r8, r7, arrayList2, hashMap);
            } else if (C15380n4.A0J(r8)) {
                StringBuilder sb = new StringBuilder("contact-mutation-handler/createBootstrapMutations attempting to add jids that messaged in group or were mentioned: ");
                sb.append(r8);
                Log.i(sb.toString());
                for (AbstractC15340mz r12 : r2.A01) {
                    A00(r12.A0B(), r7, arrayList3, hashMap);
                    List<Jid> list2 = r12.A0o;
                    if (list2 != null) {
                        for (Jid jid : list2) {
                            A00(jid, r7, arrayList3, hashMap);
                        }
                    }
                }
                C15600nX r13 = this.A08;
                GroupJid of2 = GroupJid.of(r8);
                AnonymousClass009.A05(of2);
                AnonymousClass1JO A06 = r13.A02(of2).A06();
                StringBuilder sb2 = new StringBuilder("contact-mutation-handler/createBootstrapMutations attempting to add all participant jids for group: ");
                sb2.append(r8);
                Log.i(sb2.toString());
                Iterator it3 = A06.iterator();
                while (it3.hasNext()) {
                    A00((Jid) it3.next(), r7, arrayList4, hashMap);
                }
            }
        }
        List<Collection> asList = Arrays.asList(arrayList2, arrayList3, arrayList4, hashMap.keySet());
        ArrayList arrayList5 = new ArrayList();
        for (Collection collection : asList) {
            arrayList5.addAll(collection);
        }
        return A08(C27791Jf.A03, arrayList5);
    }
}
