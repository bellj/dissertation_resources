package X;

import android.content.Context;

/* renamed from: X.5fr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120245fr extends AbstractC451020e {
    public final /* synthetic */ AnonymousClass6MT A00;
    public final /* synthetic */ C128065vV A01;
    public final /* synthetic */ String A02;
    public final /* synthetic */ String A03;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120245fr(Context context, C14900mE r2, C18650sn r3, AnonymousClass6MT r4, C128065vV r5, String str, String str2) {
        super(context, r2, r3);
        this.A01 = r5;
        this.A02 = str;
        this.A00 = r4;
        this.A03 = str2;
    }

    @Override // X.AbstractC451020e
    public void A02(C452120p r4) {
        C30931Zj r2 = this.A01.A05;
        StringBuilder A0k = C12960it.A0k("PaymentKycAction ");
        A0k.append(this.A02);
        r2.A05(C12960it.A0Z(r4, ": onRequestError: ", A0k));
        this.A00.ARg(r4);
    }

    @Override // X.AbstractC451020e
    public void A03(C452120p r4) {
        C30931Zj r2 = this.A01.A05;
        StringBuilder A0k = C12960it.A0k("PaymentKycAction ");
        A0k.append(this.A02);
        r2.A05(C12960it.A0Z(r4, ": onResponseError: ", A0k));
        this.A00.ARg(r4);
    }

    @Override // X.AbstractC451020e
    public void A04(AnonymousClass1V8 r6) {
        C30931Zj r2;
        String str;
        AnonymousClass1V8 A0c = C117305Zk.A0c(r6);
        if (A0c == null) {
            r2 = this.A01.A05;
            StringBuilder A0j = C12960it.A0j("PaymentKycAction ");
            A0j.append(this.A02);
            str = C12960it.A0d(": onResponseSuccess: missing account node", A0j);
        } else {
            C452120p A00 = C452120p.A00(A0c);
            if (A00 != null) {
                C128065vV r3 = this.A01;
                C30931Zj r22 = r3.A05;
                StringBuilder A0j2 = C12960it.A0j("PaymentKycAction ");
                A0j2.append(this.A02);
                r22.A05(C12960it.A0Z(A00, ": onResponseSuccess: account-node error: ", A0j2));
                if (A00.A00 == 1448) {
                    r3.A04.A02(A00, this.A03, "KYC");
                }
                this.A00.ARg(A00);
                return;
            }
            C460524g A002 = C460524g.A00(A0c);
            if (A002 != null) {
                this.A00.ARh(A002);
                return;
            } else {
                r2 = this.A01.A05;
                str = "PaymentKycAction/createCallback PaymentKycInfo is null";
            }
        }
        r2.A05(str);
        this.A00.ARg(C117305Zk.A0L());
    }
}
