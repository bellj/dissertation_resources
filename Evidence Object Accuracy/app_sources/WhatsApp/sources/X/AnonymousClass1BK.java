package X;

import android.net.Uri;
import android.os.Build;
import android.os.LocaleList;
import com.whatsapp.Me;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;

/* renamed from: X.1BK  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1BK {
    public final C20920wX A00;
    public final C15570nT A01;
    public final C15450nH A02;
    public final AnonymousClass018 A03;

    public AnonymousClass1BK(C20920wX r1, C15570nT r2, C15450nH r3, AnonymousClass018 r4) {
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = r4;
        this.A00 = r1;
    }

    public Set A00(AbstractC14640lm r3, AbstractC15340mz r4, String str) {
        if (str == null || r4.A0z.A02) {
            return null;
        }
        return A01(r3, str);
    }

    public Set A01(AbstractC14640lm r14, String str) {
        String str2;
        String str3 = "ZZ";
        String host = Uri.parse(str).getHost();
        if (host == null) {
            return null;
        }
        try {
            C71133cR A0E = this.A00.A0E(C248917h.A04(r14), null);
            str2 = C22650zQ.A01(Integer.toString(A0E.countryCode_), C20920wX.A01(A0E));
        } catch (AnonymousClass4C8 unused) {
            str2 = str3;
        }
        C15570nT r0 = this.A01;
        r0.A08();
        Me me = r0.A00;
        if (me != null) {
            str3 = C22650zQ.A01(me.cc, me.number);
        }
        HashSet hashSet = new HashSet();
        Locale A00 = AnonymousClass018.A00(this.A03.A00);
        if (Build.VERSION.SDK_INT >= 24) {
            LocaleList localeList = LocaleList.getDefault();
            for (int i = 0; i < localeList.size(); i++) {
                hashSet.add(localeList.get(i));
            }
        }
        hashSet.add(A00);
        String[] split = host.split("\\.");
        int length = split.length;
        int i2 = 0;
        HashSet hashSet2 = null;
        String str4 = null;
        int i3 = 0;
        boolean z = false;
        while (true) {
            boolean z2 = true;
            if (i3 < length) {
                String str5 = split[i3];
                boolean matches = true ^ AnonymousClass3B8.A00.matcher(str5).matches();
                if (matches) {
                    if (str4 != null) {
                        break;
                    }
                    str4 = str5;
                }
                i3++;
                z = matches;
            } else if (str4 != null && !z) {
                int length2 = str4.length();
                ArrayList arrayList = new ArrayList(2);
                int i4 = 0;
                while (true) {
                    if (i4 < length2) {
                        int codePointAt = str4.codePointAt(i4);
                        if (z2) {
                            z2 = true;
                            if (AnonymousClass3B8.A03.contains(Integer.valueOf(codePointAt))) {
                                continue;
                                i4 += Character.charCount(codePointAt);
                            }
                        }
                        z2 = false;
                        if (AnonymousClass3B8.A01.get(codePointAt) != null) {
                            if (arrayList.size() >= 2) {
                                break;
                            }
                            arrayList.add(Integer.valueOf(codePointAt));
                        } else {
                            continue;
                        }
                        i4 += Character.charCount(codePointAt);
                    } else if (z2) {
                        HashSet hashSet3 = AnonymousClass3B8.A04;
                        if (!hashSet3.contains(str3) && !hashSet3.contains(str2)) {
                            Iterator it = hashSet.iterator();
                            while (it.hasNext()) {
                                if (AnonymousClass3B8.A05.contains(((Locale) it.next()).getLanguage())) {
                                    return null;
                                }
                            }
                            hashSet2 = new HashSet(length2);
                            while (i2 < length2) {
                                int codePointAt2 = str4.codePointAt(i2);
                                hashSet2.add(Integer.valueOf(codePointAt2));
                                i2 += Character.charCount(codePointAt2);
                            }
                        }
                    } else if (!arrayList.isEmpty()) {
                        AnonymousClass01b r4 = new AnonymousClass01b(arrayList.size());
                        Iterator it2 = arrayList.iterator();
                        while (it2.hasNext()) {
                            int intValue = ((Number) it2.next()).intValue();
                            AnonymousClass3BK r3 = (AnonymousClass3BK) AnonymousClass3B8.A01.get(intValue);
                            Collection collection = r3.A01;
                            if (!collection.contains(str3) && !collection.contains(str2)) {
                                Iterator it3 = hashSet.iterator();
                                while (true) {
                                    if (!it3.hasNext()) {
                                        r4.add(Integer.valueOf(intValue));
                                        break;
                                    }
                                    if (r3.A00.contains(((Locale) it3.next()).getLanguage())) {
                                        break;
                                    }
                                }
                            }
                        }
                        if (!r4.isEmpty()) {
                            return r4;
                        }
                    }
                }
            }
        }
        return hashSet2;
    }
}
