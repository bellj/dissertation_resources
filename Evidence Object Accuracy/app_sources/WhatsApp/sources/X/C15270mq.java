package X;

import android.app.Activity;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.accessibility.AccessibilityManager;
import android.view.animation.Animation;
import android.widget.AbsListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.facebook.redex.RunnableBRunnable0Shape0S0110000_I0;
import com.facebook.redex.RunnableBRunnable0Shape5S0200000_I0_5;
import com.facebook.redex.ViewOnClickCListenerShape1S0100000_I0_1;
import com.whatsapp.R;
import com.whatsapp.WaEditText;
import com.whatsapp.emoji.EmojiPopupFooter;
import com.whatsapp.util.ViewOnClickCListenerShape14S0100000_I0_1;
import java.util.Arrays;

/* renamed from: X.0mq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15270mq extends AbstractC15280mr {
    public int A00 = R.drawable.ib_emoji;
    public int A01;
    public int A02;
    public int A03 = R.drawable.ib_keyboard;
    public int A04;
    public ViewGroup A05;
    public AbstractC116455Vm A06;
    public AnonymousClass3IZ A07;
    public EmojiPopupFooter A08;
    public C15310mu A09 = new C15310mu(null, true);
    public AnonymousClass5UA A0A;
    public C91894Tq A0B;
    public C15330mx A0C;
    public C255819y A0D;
    public Runnable A0E;
    public final View.OnClickListener A0F;
    public final View A0G;
    public final AbsListView.OnScrollListener A0H = new AnonymousClass3O0(this);
    public final ImageButton A0I;
    public final AbstractC116455Vm A0J = new C1096652o(this);
    public final WaEditText A0K;
    public final AnonymousClass018 A0L;
    public final AnonymousClass19M A0M;
    public final C231510o A0N;
    public final AnonymousClass193 A0O;
    public final C16630pM A0P;

    public C15270mq(Activity activity, ImageButton imageButton, AbstractC15710nm r14, AbstractC49822Mw r15, WaEditText waEditText, AnonymousClass01d r17, C14820m6 r18, AnonymousClass018 r19, AnonymousClass19M r20, C231510o r21, AnonymousClass193 r22, C16630pM r23, C252718t r24) {
        super(activity, r14, r15, r17, r18, r24);
        ViewOnClickCListenerShape1S0100000_I0_1 viewOnClickCListenerShape1S0100000_I0_1 = new ViewOnClickCListenerShape1S0100000_I0_1(this, 49);
        this.A0F = viewOnClickCListenerShape1S0100000_I0_1;
        View view = (View) r15;
        this.A0G = view;
        this.A0M = r20;
        this.A0N = r21;
        this.A0L = r19;
        this.A0O = r22;
        this.A0P = r23;
        this.A0I = imageButton;
        this.A0K = waEditText;
        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() { // from class: X.4nj
            @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
            public final void onGlobalLayout() {
                C15270mq.A01(C15270mq.this);
            }
        });
        if (imageButton != null) {
            imageButton.setImageDrawable(AnonymousClass2GE.A01(activity, R.drawable.ib_emoji, R.color.ibEmojiIconTint));
            imageButton.setOnClickListener(viewOnClickCListenerShape1S0100000_I0_1);
        }
    }

    public static final void A00(RelativeLayout relativeLayout) {
        if (C42941w9.A01) {
            relativeLayout.setLayoutDirection(3);
        }
    }

    public static /* synthetic */ void A01(C15270mq r2) {
        boolean A00;
        if (((AbstractC15280mr) r2).A03.getResources().getConfiguration().keyboard != 1) {
            A00 = false;
        } else {
            A00 = C252718t.A00(r2.A0G);
        }
        int i = A00 ? 1 : 0;
        int i2 = A00 ? 1 : 0;
        int i3 = A00 ? 1 : 0;
        ((AbstractC15280mr) r2).A00 = i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0057, code lost:
        if (r1 == false) goto L_0x0059;
     */
    @Override // X.AbstractC15280mr
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A04() {
        /*
            r4 = this;
            super.A04()
            X.193 r1 = r4.A0O
            r0 = 0
            r1.A00 = r0
            r2 = r4
            boolean r0 = r4 instanceof X.C15260mp
            if (r0 != 0) goto L_0x004c
            int r0 = r4.A01
            if (r0 == 0) goto L_0x0035
            android.widget.ImageButton r3 = r4.A0I
            if (r3 == 0) goto L_0x0035
            android.app.Activity r2 = r4.A03
            int r1 = r4.A00
            r0 = 2131100315(0x7f06029b, float:1.7813008E38)
            android.graphics.drawable.Drawable r0 = X.AnonymousClass2GE.A01(r2, r1, r0)
            r3.setImageDrawable(r0)
            r0 = 2131887884(0x7f12070c, float:1.9410388E38)
            java.lang.String r0 = r2.getString(r0)
            r3.setContentDescription(r0)
            android.view.View$OnClickListener r0 = r4.A0F
            r3.setOnClickListener(r0)
            r0 = 0
            r4.A01 = r0
        L_0x0035:
            X.3IZ r2 = r4.A07
            if (r2 == 0) goto L_0x004b
            android.view.View r1 = r2.A0E
            r0 = 8
            r1.setVisibility(r0)
            androidx.viewpager.widget.ViewPager r0 = r2.A0L
            android.view.ViewTreeObserver r1 = r0.getViewTreeObserver()
            android.view.ViewTreeObserver$OnGlobalLayoutListener r0 = r2.A0H
            r1.removeGlobalOnLayoutListener(r0)
        L_0x004b:
            return
        L_0x004c:
            X.0mp r2 = (X.C15260mp) r2
            X.2B7 r0 = r2.A0A
            if (r0 == 0) goto L_0x0059
            boolean r1 = r0.A01()
            r0 = 2
            if (r1 != 0) goto L_0x005a
        L_0x0059:
            r0 = 0
        L_0x005a:
            r2.A0J(r0)
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15270mq.A04():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x009f, code lost:
        if (r1.A00 != 0) goto L_0x00a1;
     */
    @Override // X.AbstractC15280mr
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A06() {
        /*
        // Method dump skipped, instructions count: 234
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15270mq.A06():void");
    }

    public void A0A() {
        Activity activity = super.A03;
        RelativeLayout relativeLayout = new RelativeLayout(activity);
        boolean z = true;
        activity.getLayoutInflater().inflate(R.layout.emoji_picker_horizontal, (ViewGroup) relativeLayout, true);
        ViewGroup viewGroup = (ViewGroup) relativeLayout.findViewById(R.id.emoji_view);
        this.A05 = viewGroup;
        viewGroup.getLayoutParams().height = -1;
        this.A05.setVisibility(0);
        setContentView(relativeLayout);
        A00(relativeLayout);
        setWidth(-1);
        setHeight(-2);
        setBackgroundDrawable(new BitmapDrawable());
        setTouchable(true);
        setFocusable(true);
        setOutsideTouchable(true);
        setInputMethodMode(2);
        setAnimationStyle(0);
        View findViewById = this.A05.findViewById(R.id.fallback_divider);
        int i = 8;
        if (Build.VERSION.SDK_INT < 21) {
            findViewById.setVisibility(0);
        } else {
            findViewById.setVisibility(8);
        }
        AbstractC15710nm r11 = super.A04;
        AnonymousClass19M r14 = this.A0M;
        C231510o r15 = this.A0N;
        AnonymousClass018 r13 = this.A0L;
        C14820m6 r12 = super.A07;
        C16630pM r0 = this.A0P;
        AnonymousClass3IZ r6 = new AnonymousClass3IZ(activity, this.A0G, this.A05, this.A0H, r11, r12, r13, r14, r15, r0);
        this.A07 = r6;
        r6.A02 = this.A0J;
        EmojiPopupFooter emojiPopupFooter = (EmojiPopupFooter) this.A05.findViewById(R.id.footer_toolbar);
        this.A08 = emojiPopupFooter;
        emojiPopupFooter.setClickable(true);
        A0D(true);
        View findViewById2 = this.A05.findViewById(R.id.gif_tab);
        View findViewById3 = this.A05.findViewById(R.id.emoji_tab);
        View findViewById4 = this.A05.findViewById(R.id.search_button);
        findViewById2.setVisibility(8);
        findViewById3.setVisibility(8);
        if (!(this instanceof C15260mp)) {
            this.A0O.A00 = new AnonymousClass5UG(findViewById4) { // from class: X.572
                public final /* synthetic */ View A00;

                {
                    this.A00 = r1;
                }

                @Override // X.AnonymousClass5UG
                public final void AVg(boolean z2) {
                    View view = this.A00;
                    view.post(new RunnableBRunnable0Shape0S0110000_I0(view, 8, z2));
                }
            };
        } else {
            C15260mp r7 = (C15260mp) this;
            r7.A0O.A00 = new AnonymousClass5UG(findViewById4, r7) { // from class: X.574
                public final /* synthetic */ View A00;
                public final /* synthetic */ C15260mp A01;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // X.AnonymousClass5UG
                public final void AVg(boolean z2) {
                    C15260mp r3 = this.A01;
                    View view = this.A00;
                    view.post(new RunnableBRunnable0Shape5S0200000_I0_5(r3, 2, view));
                }
            };
        }
        AnonymousClass193 r62 = this.A0O;
        if (r62.A02) {
            i = 0;
        }
        findViewById4.setVisibility(i);
        r62.A01();
        findViewById4.setOnClickListener(new ViewOnClickCListenerShape14S0100000_I0_1(this, 28));
        this.A05.findViewById(R.id.delete_symbol_tb).setVisibility(0);
        setTouchInterceptor(new View.OnTouchListener() { // from class: X.3N3
            @Override // android.view.View.OnTouchListener
            public final boolean onTouch(View view, MotionEvent motionEvent) {
                int i2;
                Rect rect;
                int i3;
                int i4;
                C15270mq r4 = C15270mq.this;
                if (!(motionEvent.getActionMasked() == 2 || motionEvent.getActionMasked() == 1)) {
                    float x = motionEvent.getX();
                    float y = motionEvent.getY();
                    int[] A07 = C13000ix.A07();
                    view.getLocationOnScreen(A07);
                    Point point = new Point(((int) x) + A07[0], ((int) y) + A07[1]);
                    WaEditText waEditText = r4.A0K;
                    if (waEditText != null && waEditText.isShown() && AbstractC15280mr.A02(point, waEditText) && ((rect = waEditText.A00) == null || ((i3 = point.x) >= rect.left && i3 <= rect.right && (i4 = point.y) >= rect.top && i4 <= rect.bottom))) {
                        r4.A08(waEditText);
                        return true;
                    } else if (motionEvent.getY() < 0.0f) {
                        return true;
                    }
                }
                AnonymousClass3IZ r1 = r4.A07;
                C53252dZ r02 = r1.A04;
                if (r02 == null || !r02.isShowing()) {
                    return false;
                }
                C53252dZ r5 = r1.A04;
                int x2 = (int) motionEvent.getX();
                int y2 = (int) motionEvent.getY();
                ViewGroup viewGroup2 = (ViewGroup) r5.getContentView();
                int[] iArr = r5.A01;
                view.getLocationOnScreen(iArr);
                int i5 = x2 + iArr[0];
                int i6 = y2 + iArr[1];
                int childCount = viewGroup2.getChildCount();
                View view2 = r5.A00;
                r5.A00 = null;
                int i7 = 0;
                while (true) {
                    if (i7 >= childCount) {
                        i7 = 0;
                        break;
                    }
                    View childAt = viewGroup2.getChildAt(i7);
                    childAt.getLocationOnScreen(iArr);
                    int i8 = iArr[0];
                    if (i5 > i8 && i5 < i8 + childAt.getWidth() && i6 > (i2 = iArr[1]) && i6 < i2 + childAt.getHeight()) {
                        childAt.setPressed(true);
                        r5.A00 = childAt;
                        break;
                    }
                    i7++;
                }
                if (!(view2 == null || view2 == r5.A00)) {
                    view2.setPressed(false);
                }
                if (motionEvent.getAction() != 1 || r5.A00 == null) {
                    return false;
                }
                r5.A03.AW8(r5.A04[i7]);
                r5.dismiss();
                return false;
            }
        });
        AccessibilityManager A0P = super.A06.A0P();
        if (A0P == null || !A0P.isTouchExplorationEnabled()) {
            z = false;
        }
        setFocusable(z);
    }

    public void A0B() {
        ImageButton imageButton;
        if (this.A01 != 1 && (imageButton = this.A0I) != null) {
            Activity activity = super.A03;
            imageButton.setImageDrawable(AnonymousClass2GE.A01(activity, this.A03, R.color.ibEmojiIconTint));
            imageButton.setContentDescription(activity.getString(R.string.keyboard_button_description));
            imageButton.setOnClickListener(this.A0F);
            this.A01 = 1;
        }
    }

    public void A0C(AbstractC116455Vm r3) {
        this.A06 = r3;
        AnonymousClass3IZ r1 = this.A07;
        if (r1 != null) {
            r1.A02 = this.A0J;
        }
    }

    public void A0D(boolean z) {
        ImageView imageView = this.A07.A0K;
        if (imageView != null) {
            int i = 8;
            if (z) {
                i = 0;
            }
            imageView.setVisibility(i);
        }
    }

    public void A0E(C37471mS[] r7) {
        AnonymousClass3IZ r5 = this.A07;
        C37471mS[] r0 = r5.A05;
        if (r0 != null ? !Arrays.equals(r0, r7) : r7 != null) {
            r5.A05 = r7;
            AnonymousClass3HU[] r4 = r5.A0T;
            for (AnonymousClass3HU r02 : r4) {
                r02.A01(r7);
                AnonymousClass2bf r03 = r5.A0S[r02.A04];
                if (r03 != null) {
                    r03.notifyDataSetChanged();
                }
            }
        }
        C15330mx r04 = this.A0C;
        if (r04 != null) {
            r04.A02.A0G = r7;
        }
    }

    public void Aak() {
        if (this.A08.getVisibility() != 0) {
            this.A08.setVisibility(0);
        }
        Animation animation = this.A08.getAnimation();
        if (animation instanceof C52592bM) {
            animation.cancel();
        }
        this.A08.setTopOffset(0);
    }
}
