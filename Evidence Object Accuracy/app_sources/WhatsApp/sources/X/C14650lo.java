package X;

import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Handler;
import android.os.Looper;
import com.facebook.redex.RunnableBRunnable0Shape0S0210000_I0;
import com.whatsapp.jid.UserJid;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0lo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14650lo {
    public final Handler A00 = new Handler(Looper.getMainLooper());
    public final AbstractC15710nm A01;
    public final C15570nT A02;
    public final AnonymousClass10A A03;
    public final C246716k A04;
    public final AnonymousClass0t9 A05;
    public final C247116o A06;
    public final C246816l A07;
    public final C247016n A08;
    public final C30091Wb A09;
    public final C14830m7 A0A;
    public final C14820m6 A0B;
    public final C17220qS A0C;
    public final C19860un A0D;
    public final AbstractC14440lR A0E;
    public final Set A0F = new HashSet();

    static {
        TimeUnit.HOURS.toMillis(24);
    }

    public C14650lo(AbstractC15710nm r3, C15570nT r4, AnonymousClass10A r5, C246716k r6, AnonymousClass0t9 r7, C247116o r8, C246816l r9, C247016n r10, C14830m7 r11, C14820m6 r12, C17220qS r13, C19860un r14, AbstractC14440lR r15, C232010t r16) {
        this.A0A = r11;
        this.A02 = r4;
        this.A0E = r15;
        this.A01 = r3;
        this.A0C = r13;
        this.A0B = r12;
        this.A04 = r6;
        this.A03 = r5;
        this.A0D = r14;
        this.A07 = r9;
        this.A08 = r10;
        this.A05 = r7;
        this.A09 = new C30091Wb(r16);
        this.A06 = r8;
    }

    public C30101Wc A00(AbstractC15710nm r10, UserJid userJid, String str) {
        int A00 = this.A04.A00();
        C17220qS r5 = this.A0C;
        return new C30101Wc(r10, this, this.A03, userJid, r5, (C19840ul) ((AnonymousClass01J) AnonymousClass027.A00(AnonymousClass01J.class, this.A0D.A00.A00)).A1Q.get(), str, A00);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0043, code lost:
        if (r1 != null) goto L_0x0045;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A01(com.whatsapp.jid.UserJid r12) {
        /*
            r11 = this;
            X.1Wb r0 = r11.A09
            java.lang.String r3 = "business_description"
            r7 = 0
            if (r12 != 0) goto L_0x000d
            java.lang.String r0 = "contact-mgr-db/cannot get business description details by null jid"
            com.whatsapp.util.Log.w(r0)
            return r7
        L_0x000d:
            java.lang.String r2 = r12.getRawString()
            X.10t r0 = r0.A00
            X.0on r4 = r0.get()
            java.lang.String r5 = "wa_biz_profiles LEFT JOIN wa_biz_profiles_websites ON (wa_biz_profiles._id = wa_biz_profiles_websites.wa_biz_profile_id)"
            r1 = 1
            java.lang.String[] r9 = new java.lang.String[r1]     // Catch: all -> 0x004c
            r0 = 0
            r9[r0] = r3     // Catch: all -> 0x004c
            java.lang.String r6 = "wa_biz_profiles.jid = ?"
            java.lang.String[] r10 = new java.lang.String[r1]     // Catch: all -> 0x004c
            r10[r0] = r2     // Catch: all -> 0x004c
            java.lang.String r8 = "CONTACT_BIZ_PROFILES"
            android.database.Cursor r1 = X.AbstractC21570xd.A03(r4, r5, r6, r7, r8, r9, r10)     // Catch: all -> 0x004c
            if (r1 == 0) goto L_0x0043
            boolean r0 = r1.moveToFirst()     // Catch: all -> 0x003e
            if (r0 == 0) goto L_0x0043
            int r0 = r1.getColumnIndexOrThrow(r3)     // Catch: all -> 0x003e
            java.lang.String r7 = r1.getString(r0)     // Catch: all -> 0x003e
            goto L_0x0045
        L_0x003e:
            r0 = move-exception
            r1.close()     // Catch: all -> 0x0042
        L_0x0042:
            throw r0     // Catch: all -> 0x004c
        L_0x0043:
            if (r1 == 0) goto L_0x0048
        L_0x0045:
            r1.close()     // Catch: all -> 0x004c
        L_0x0048:
            r4.close()
            return r7
        L_0x004c:
            r0 = move-exception
            r4.close()     // Catch: all -> 0x0050
        L_0x0050:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14650lo.A01(com.whatsapp.jid.UserJid):java.lang.String");
    }

    public Map A02() {
        C30091Wb r0 = this.A09;
        HashMap hashMap = new HashMap();
        C16310on A01 = r0.A00.get();
        try {
            Cursor A03 = AbstractC21570xd.A03(A01, "wa_biz_profiles LEFT JOIN wa_biz_profiles_websites ON (wa_biz_profiles._id = wa_biz_profiles_websites.wa_biz_profile_id)", null, null, "CONTACT_BIZ_PROFILES", new String[]{"jid", "tag"}, null);
            if (A03 != null) {
                while (A03.moveToNext()) {
                    UserJid nullable = UserJid.getNullable(A03.getString(0));
                    if (nullable != null) {
                        hashMap.put(nullable, A03.getString(1));
                    }
                }
                A03.close();
            }
            A01.close();
            return hashMap;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x006b, code lost:
        if (r0.booleanValue() == false) goto L_0x0070;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A03(X.AbstractC30111Wd r6, com.whatsapp.jid.UserJid r7) {
        /*
            r5 = this;
            X.0lR r2 = r5.A0E
            X.1We r1 = new X.1We
            r1.<init>(r6, r5, r7)
            r0 = 0
            java.lang.Void[] r0 = new java.lang.Void[r0]
            r2.Aaz(r1, r0)
            X.0t9 r3 = r5.A05
            X.0m9 r1 = r3.A05
            r0 = 1693(0x69d, float:2.372E-42)
            java.lang.String r1 = r1.A03(r0)
            r4 = 0
            if (r7 == 0) goto L_0x0028
            if (r1 == 0) goto L_0x0028
            java.lang.String r0 = r7.user
            X.C16700pc.A0B(r0)
            boolean r0 = X.AnonymousClass03B.A0G(r1, r0)
            if (r0 == 0) goto L_0x0028
            r4 = 1
        L_0x0028:
            X.0m6 r0 = r3.A03
            android.content.SharedPreferences r2 = r0.A00
            java.lang.String r1 = "pref_commerce_metadata_cache_enable"
            r0 = 1
            boolean r0 = r2.getBoolean(r1, r0)
            if (r0 == 0) goto L_0x006d
            if (r4 == 0) goto L_0x0070
            X.1WG r0 = r3.A02()
            if (r0 == 0) goto L_0x006d
            java.util.List r1 = r0.A00
            if (r1 == 0) goto L_0x006d
            boolean r0 = r1.isEmpty()
            r2 = 0
            if (r0 != 0) goto L_0x0061
            java.util.Iterator r1 = r1.iterator()
        L_0x004c:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0061
            java.lang.Object r0 = r1.next()
            X.1WE r0 = (X.AnonymousClass1WE) r0
            java.lang.String r0 = r0.A03
            X.1WE r0 = r3.A00(r0)
            if (r0 != 0) goto L_0x004c
            r2 = 1
        L_0x0061:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r2)
            if (r0 == 0) goto L_0x006d
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0070
        L_0x006d:
            r3.A03()
        L_0x0070:
            X.16o r4 = r5.A06
            X.0m9 r1 = r4.A06
            r0 = 1763(0x6e3, float:2.47E-42)
            java.lang.String r1 = r1.A03(r0)
            if (r7 == 0) goto L_0x00b5
            java.lang.String r0 = r7.user
            if (r0 == 0) goto L_0x00b5
            if (r1 == 0) goto L_0x00b5
            boolean r0 = X.AnonymousClass03B.A0G(r1, r0)
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            if (r0 == 0) goto L_0x00b5
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x00b5
            boolean r0 = r4.A01
            if (r0 != 0) goto L_0x00ac
            r0 = 1
            r4.A01 = r0
            X.0pI r0 = r4.A03
            android.content.Context r3 = r0.A00
            X.1Wf r2 = new X.1Wf
            r2.<init>(r4)
            java.lang.String r1 = "android.intent.action.LOCALE_CHANGED"
            android.content.IntentFilter r0 = new android.content.IntentFilter
            r0.<init>(r1)
            r3.registerReceiver(r2, r0)
        L_0x00ac:
            boolean r0 = r4.A02()
            if (r0 == 0) goto L_0x00b5
            r4.A01()
        L_0x00b5:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14650lo.A03(X.1Wd, com.whatsapp.jid.UserJid):void");
    }

    public void A04(AbstractC13940ka r2, UserJid userJid, String str) {
        A00(this.A01, userJid, str).A01(r2);
    }

    public void A05(C30141Wg r11, UserJid userJid) {
        if (r11 != null) {
            if (this.A02.A0F(userJid)) {
                C14820m6 r3 = this.A0B;
                r3.A00.edit().putLong("smb_last_my_business_profile_sync_time", this.A0A.A00()).apply();
            }
            C30091Wb r1 = this.A09;
            HashMap hashMap = new HashMap();
            hashMap.put(userJid, r11);
            r1.A03(hashMap);
            if ((this.A04.A00() & 512) > 0 && r11.A0J) {
                this.A08.A01(userJid, null, null, null, null, false);
            }
        }
    }

    public void A06(UserJid userJid, String str) {
        C14820m6 r0 = this.A0B;
        String rawString = userJid.getRawString();
        SharedPreferences.Editor edit = r0.A00.edit();
        StringBuilder sb = new StringBuilder("dc_default_postcode_");
        sb.append(rawString);
        edit.putString(sb.toString(), str).apply();
    }

    public void A07(UserJid userJid, boolean z) {
        this.A0E.Ab2(new RunnableBRunnable0Shape0S0210000_I0(this, userJid, 3, z));
    }

    public boolean A08() {
        return (this.A04.A00() & 128) > 0;
    }
}
