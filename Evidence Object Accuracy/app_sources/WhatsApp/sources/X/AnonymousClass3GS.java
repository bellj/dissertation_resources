package X;

import android.content.Context;

/* renamed from: X.3GS  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3GS {
    public static Context A00;
    public static Boolean A01;

    public static synchronized boolean A00(Context context) {
        boolean z;
        Boolean bool;
        Boolean bool2;
        synchronized (AnonymousClass3GS.class) {
            Context applicationContext = context.getApplicationContext();
            Context context2 = A00;
            if (context2 == null || (bool2 = A01) == null || context2 != applicationContext) {
                A01 = null;
                if (C472729v.A03()) {
                    bool = Boolean.valueOf(applicationContext.getPackageManager().isInstantApp());
                } else {
                    try {
                        context.getClassLoader().loadClass("com.google.android.instantapps.supervisor.InstantAppsRuntime");
                        bool = Boolean.TRUE;
                        A01 = bool;
                    } catch (ClassNotFoundException unused) {
                        bool = Boolean.FALSE;
                    }
                    A00 = applicationContext;
                    z = bool.booleanValue();
                }
                A01 = bool;
                A00 = applicationContext;
                z = bool.booleanValue();
            } else {
                z = bool2.booleanValue();
            }
        }
        return z;
    }
}
