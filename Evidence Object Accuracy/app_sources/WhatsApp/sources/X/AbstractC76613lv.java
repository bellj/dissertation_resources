package X;

import com.google.android.exoplayer2.Timeline;

/* renamed from: X.3lv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC76613lv extends Timeline {
    public final Timeline A00;

    public AbstractC76613lv(Timeline timeline) {
        this.A00 = timeline;
    }

    @Override // com.google.android.exoplayer2.Timeline
    public C94404bl A0B(C94404bl r2, int i, long j) {
        return this.A00.A0B(r2, i, j);
    }
}
