package X;

import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.16F  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass16F extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Boolean A02;
    public Boolean A03;
    public Boolean A04;
    public Boolean A05;
    public Boolean A06;
    public Boolean A07;
    public Boolean A08;
    public Boolean A09;
    public Boolean A0A;
    public Boolean A0B;
    public Boolean A0C;
    public Boolean A0D;
    public Boolean A0E;
    public Boolean A0F;
    public Boolean A0G;
    public Boolean A0H;
    public Long A0I;
    public Long A0J;
    public Long A0K;
    public Long A0L;
    public Long A0M;
    public Long A0N;
    public final C14820m6 A0O;

    public AnonymousClass16F(C14820m6 r5) {
        super(1144, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
        this.A0O = r5;
        String string = r5.A00.getString("wam_client_errors", null);
        if (string != null) {
            try {
                JSONObject jSONObject = new JSONObject(string);
                this.A0I = A02(jSONObject, 2);
                this.A0J = A02(jSONObject, 3);
                this.A00 = A01(jSONObject, 1);
                this.A0G = A01(jSONObject, 4);
                this.A0H = A01(jSONObject, 5);
                this.A0F = A01(jSONObject, 6);
                this.A0D = A01(jSONObject, 7);
                this.A04 = A01(jSONObject, 8);
                this.A09 = A01(jSONObject, 9);
                this.A0A = A01(jSONObject, 10);
                this.A07 = A01(jSONObject, 11);
                this.A0E = A01(jSONObject, 12);
                this.A0B = A01(jSONObject, 13);
                this.A08 = A01(jSONObject, 14);
                this.A03 = A01(jSONObject, 15);
                this.A02 = A01(jSONObject, 16);
                this.A05 = A01(jSONObject, 17);
                this.A01 = A01(jSONObject, 18);
                this.A06 = A01(jSONObject, 19);
                this.A0C = A01(jSONObject, 20);
                this.A0M = A02(jSONObject, 22);
                this.A0N = A02(jSONObject, 23);
                this.A0K = A02(jSONObject, 24);
                this.A0L = A02(jSONObject, 25);
            } catch (JSONException e) {
                Log.e(e);
            }
        }
    }

    public static Boolean A01(JSONObject jSONObject, int i) {
        String valueOf = String.valueOf(i);
        if (!jSONObject.has(valueOf)) {
            return null;
        }
        try {
            return Boolean.valueOf(jSONObject.getBoolean(valueOf));
        } catch (JSONException e) {
            Log.e(e);
            return null;
        }
    }

    public static Long A02(JSONObject jSONObject, int i) {
        String valueOf = String.valueOf(i);
        if (!jSONObject.has(valueOf)) {
            return null;
        }
        try {
            return Long.valueOf(jSONObject.getLong(valueOf));
        } catch (JSONException e) {
            Log.e(e);
            return null;
        }
    }

    public static void A03(Boolean bool, JSONObject jSONObject, int i) {
        if (bool != null) {
            try {
                jSONObject.put(String.valueOf(i), bool.booleanValue());
            } catch (JSONException e) {
                Log.e(e);
            }
        }
    }

    public static void A04(Long l, JSONObject jSONObject, int i) {
        if (l != null) {
            try {
                jSONObject.put(String.valueOf(i), l.longValue());
            } catch (JSONException e) {
                Log.e(e);
            }
        }
    }

    public void A05() {
        String str;
        if (!A0A()) {
            JSONObject jSONObject = new JSONObject();
            A04(this.A0I, jSONObject, 2);
            A04(this.A0J, jSONObject, 3);
            A03(this.A00, jSONObject, 1);
            A03(this.A0G, jSONObject, 4);
            A03(this.A0H, jSONObject, 5);
            A03(this.A0F, jSONObject, 6);
            A03(this.A0D, jSONObject, 7);
            A03(this.A04, jSONObject, 8);
            A03(this.A09, jSONObject, 9);
            A03(this.A0A, jSONObject, 10);
            A03(this.A07, jSONObject, 11);
            A03(this.A0E, jSONObject, 12);
            A03(this.A0B, jSONObject, 13);
            A03(this.A08, jSONObject, 14);
            A03(this.A03, jSONObject, 15);
            A03(this.A02, jSONObject, 16);
            A03(this.A05, jSONObject, 17);
            A03(this.A01, jSONObject, 18);
            A03(this.A06, jSONObject, 19);
            A03(this.A0C, jSONObject, 20);
            A04(this.A0M, jSONObject, 22);
            A04(this.A0N, jSONObject, 23);
            A04(this.A0K, jSONObject, 24);
            A04(this.A0L, jSONObject, 25);
            str = jSONObject.toString();
        } else {
            str = null;
        }
        this.A0O.A00.edit().putString("wam_client_errors", str).apply();
    }

    public final void A06() {
        this.A04 = Boolean.TRUE;
    }

    public final void A07() {
        this.A09 = Boolean.TRUE;
    }

    public final void A08() {
        this.A0D = Boolean.TRUE;
    }

    public final void A09() {
        this.A0G = Boolean.TRUE;
    }

    public final boolean A0A() {
        boolean z = false;
        boolean z2 = false;
        if (this.A0I == null) {
            z2 = true;
        }
        boolean z3 = z2 & true;
        boolean z4 = false;
        if (this.A0J == null) {
            z4 = true;
        }
        boolean z5 = z3 & z4;
        boolean z6 = false;
        if (this.A00 == null) {
            z6 = true;
        }
        boolean z7 = z5 & z6;
        boolean z8 = false;
        if (this.A0K == null) {
            z8 = true;
        }
        boolean z9 = z7 & z8;
        boolean z10 = false;
        if (this.A0L == null) {
            z10 = true;
        }
        boolean z11 = z9 & z10;
        boolean z12 = false;
        if (this.A0M == null) {
            z12 = true;
        }
        boolean z13 = z11 & z12;
        boolean z14 = false;
        if (this.A0N == null) {
            z14 = true;
        }
        boolean z15 = z13 & z14;
        boolean z16 = false;
        if (this.A01 == null) {
            z16 = true;
        }
        boolean z17 = z15 & z16;
        boolean z18 = false;
        if (this.A02 == null) {
            z18 = true;
        }
        boolean z19 = z17 & z18;
        boolean z20 = false;
        if (this.A03 == null) {
            z20 = true;
        }
        boolean z21 = z19 & z20;
        boolean z22 = false;
        if (this.A04 == null) {
            z22 = true;
        }
        boolean z23 = z21 & z22;
        boolean z24 = false;
        if (this.A05 == null) {
            z24 = true;
        }
        boolean z25 = z23 & z24;
        boolean z26 = false;
        if (this.A06 == null) {
            z26 = true;
        }
        boolean z27 = z25 & z26;
        boolean z28 = false;
        if (this.A07 == null) {
            z28 = true;
        }
        boolean z29 = z27 & z28;
        boolean z30 = false;
        if (this.A08 == null) {
            z30 = true;
        }
        boolean z31 = z29 & z30;
        boolean z32 = false;
        if (this.A09 == null) {
            z32 = true;
        }
        boolean z33 = z31 & z32;
        boolean z34 = false;
        if (this.A0A == null) {
            z34 = true;
        }
        boolean z35 = z33 & z34;
        boolean z36 = false;
        if (this.A0B == null) {
            z36 = true;
        }
        boolean z37 = z35 & z36;
        boolean z38 = false;
        if (this.A0C == null) {
            z38 = true;
        }
        boolean z39 = z37 & z38;
        boolean z40 = false;
        if (this.A0D == null) {
            z40 = true;
        }
        boolean z41 = z39 & z40;
        boolean z42 = false;
        if (this.A0E == null) {
            z42 = true;
        }
        boolean z43 = z41 & z42;
        boolean z44 = false;
        if (this.A0F == null) {
            z44 = true;
        }
        boolean z45 = z43 & z44;
        boolean z46 = false;
        if (this.A0G == null) {
            z46 = true;
        }
        boolean z47 = z45 & z46;
        if (this.A0H == null) {
            z = true;
        }
        return z47 & z;
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(2, this.A0I);
        r3.Abe(3, this.A0J);
        r3.Abe(1, this.A00);
        r3.Abe(24, this.A0K);
        r3.Abe(25, this.A0L);
        r3.Abe(22, this.A0M);
        r3.Abe(23, this.A0N);
        r3.Abe(18, this.A01);
        r3.Abe(16, this.A02);
        r3.Abe(15, this.A03);
        r3.Abe(8, this.A04);
        r3.Abe(17, this.A05);
        r3.Abe(19, this.A06);
        r3.Abe(11, this.A07);
        r3.Abe(14, this.A08);
        r3.Abe(9, this.A09);
        r3.Abe(10, this.A0A);
        r3.Abe(13, this.A0B);
        r3.Abe(20, this.A0C);
        r3.Abe(7, this.A0D);
        r3.Abe(12, this.A0E);
        r3.Abe(6, this.A0F);
        r3.Abe(4, this.A0G);
        r3.Abe(5, this.A0H);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("WamWamClientErrors {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "wamClientDroppedEventCount", this.A0I);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "wamClientDroppedEventSize", this.A0J);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "wamClientErrorFlags", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "wamClientPrivateDroppedEventCount", this.A0K);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "wamClientPrivateDroppedEventSize", this.A0L);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "wamClientRealtimeDroppedEventCount", this.A0M);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "wamClientRealtimeDroppedEventSize", this.A0N);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "wamErrorBadCurrentEventBufferChecksum", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "wamErrorBadEventBuffer", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "wamErrorBadFileHeader", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "wamErrorBadFileSize", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "wamErrorBadHeaderChecksum", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "wamErrorBadRotatedEventBufferChecksum", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "wamErrorCloseFile", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "wamErrorCreateWamFile", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "wamErrorFseekFile", this.A09);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "wamErrorOpenFile", this.A0A);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "wamErrorOpenWamFile", this.A0B);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "wamErrorPersistence", this.A0C);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "wamErrorReadFile", this.A0D);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "wamErrorRemoveFile", this.A0E);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "wamErrorWriteEventBuffer", this.A0F);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "wamErrorWriteFile", this.A0G);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "wamErrorWriteHeader", this.A0H);
        sb.append("}");
        return sb.toString();
    }
}
