package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.whatsapp.util.Log;
import java.util.LinkedList;

/* renamed from: X.1x6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class HandlerC43481x6 extends Handler implements AbstractC43471x5 {
    public final /* synthetic */ HandlerThreadC43491x7 A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public HandlerC43481x6(Looper looper, HandlerThreadC43491x7 r2) {
        super(looper);
        this.A00 = r2;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        int i = message.what;
        if (i == 0) {
            Log.i("xmpp/writer/recv/connected");
            HandlerThreadC43491x7 r2 = this.A00;
            r2.A00 = (AnonymousClass2L8) message.obj;
            r2.A01 = false;
            while (!r2.A01) {
                LinkedList linkedList = r2.A09;
                if (!linkedList.isEmpty()) {
                    r2.A00((Message) linkedList.remove());
                } else {
                    return;
                }
            }
        } else if (i == 1) {
            this.A00.A01 = true;
        } else if (i == 2) {
            this.A00.A00(message);
        }
    }
}
