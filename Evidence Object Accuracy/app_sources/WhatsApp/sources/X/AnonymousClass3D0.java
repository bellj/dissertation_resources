package X;

import android.database.Cursor;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

/* renamed from: X.3D0  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3D0 {
    public final C15570nT A00;
    public final C15550nR A01;
    public final C16370ot A02;
    public final C20510vs A03;

    public AnonymousClass3D0(C15570nT r1, C15550nR r2, C16370ot r3, C20510vs r4) {
        this.A00 = r1;
        this.A01 = r2;
        this.A03 = r4;
        this.A02 = r3;
    }

    public ArrayList A00(UserJid userJid) {
        ArrayList arrayList;
        boolean z;
        C16310on A01;
        List<Object> A0x;
        UserJid A0C;
        if (userJid == null) {
            return C12960it.A0l();
        }
        C20510vs r1 = this.A03;
        if (r1.A09()) {
            if (!r1.A09()) {
                A0x = Collections.emptyList();
            } else {
                TreeSet treeSet = new TreeSet(Collections.reverseOrder());
                long A012 = r1.A08.A01(userJid);
                A01 = r1.A0A.get();
                try {
                    C16330op r6 = A01.A03;
                    String l = Long.toString(A012);
                    Cursor A09 = r6.A09("SELECT message_row_id FROM message_vcard WHERE _id IN (SELECT vcard_row_id FROM message_vcard_jid WHERE vcard_jid_row_id = ?)", new String[]{l});
                    int columnIndexOrThrow = A09.getColumnIndexOrThrow("message_row_id");
                    while (A09.moveToNext()) {
                        treeSet.add(Long.valueOf(A09.getLong(columnIndexOrThrow)));
                    }
                    A09.close();
                    Cursor A092 = r6.A09("SELECT message_row_id FROM message_vcard_jid WHERE vcard_jid_row_id = ?", new String[]{l});
                    int columnIndexOrThrow2 = A092.getColumnIndexOrThrow("message_row_id");
                    while (A092.moveToNext()) {
                        long j = A092.getLong(columnIndexOrThrow2);
                        if (j != 0) {
                            treeSet.add(Long.valueOf(j));
                        }
                    }
                    A092.close();
                    A01.close();
                    A0x = C12980iv.A0x(treeSet);
                } finally {
                }
            }
            arrayList = C12960it.A0l();
            for (Object obj : A0x) {
                long A0G = C12980iv.A0G(obj);
                AbstractC15340mz A00 = this.A02.A00(A0G);
                C65953Ls r4 = null;
                if (!(A00 == null || (A0C = A00.A0C()) == null)) {
                    r4 = new C65953Ls(userJid.getRawString(), A0C.getRawString(), A0G);
                }
                if (r4 != null) {
                    arrayList.add(r4);
                }
            }
        } else {
            arrayList = C12960it.A0l();
            A01 = r1.A0A.get();
            try {
                C16330op r42 = A01.A03;
                String[] A08 = C13000ix.A08();
                A08[0] = userJid.getRawString();
                Cursor A093 = r42.A09("SELECT sender_jid, message_row_id FROM messages_vcards WHERE _id IN (SELECT vcard_row_id FROM messages_vcards_jids WHERE vcard_jid = ?) ORDER BY message_row_id DESC", A08);
                int columnIndexOrThrow3 = A093.getColumnIndexOrThrow("sender_jid");
                int columnIndexOrThrow4 = A093.getColumnIndexOrThrow("message_row_id");
                while (A093.moveToNext()) {
                    arrayList.add(new C65953Ls(userJid.getRawString(), A093.getString(columnIndexOrThrow3), A093.getLong(columnIndexOrThrow4)));
                }
                A093.close();
            } finally {
                try {
                    A01.close();
                } catch (Throwable unused) {
                }
            }
        }
        if (arrayList.isEmpty()) {
            return arrayList;
        }
        ArrayList A0w = C12980iv.A0w(arrayList.size());
        HashMap A11 = C12970iu.A11();
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            C65953Ls r43 = (C65953Ls) it.next();
            UserJid nullable = UserJid.getNullable(r43.A02);
            if (nullable != null) {
                if (A11.containsKey(nullable)) {
                    Object obj2 = A11.get(nullable);
                    AnonymousClass009.A05(obj2);
                    z = C12970iu.A1Y(obj2);
                } else {
                    C15370n3 A0A = this.A01.A0A(nullable);
                    if (A0A == null || (!this.A00.A0F(A0A.A0D) && A0A.A0C == null)) {
                        z = false;
                    } else {
                        z = true;
                    }
                    A11.put(nullable, Boolean.valueOf(z));
                }
                if (z) {
                    A0w.add(r43);
                }
            }
        }
        return A0w;
    }
}
