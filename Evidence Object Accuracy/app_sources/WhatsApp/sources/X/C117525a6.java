package X;

import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import com.whatsapp.R;

/* renamed from: X.5a6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117525a6 extends ClickableSpan {
    public final /* synthetic */ AnonymousClass5m3 A00;
    public final /* synthetic */ C123315mx A01;

    public C117525a6(AnonymousClass5m3 r1, C123315mx r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // android.text.style.ClickableSpan
    public void onClick(View view) {
        this.A01.A03.onClick(this.A00.A08);
    }

    @Override // android.text.style.ClickableSpan, android.text.style.CharacterStyle
    public void updateDrawState(TextPaint textPaint) {
        super.updateDrawState(textPaint);
        textPaint.setColor(C12960it.A09(this.A00.A01).getColor(R.color.link_color));
        textPaint.setUnderlineText(false);
    }
}
