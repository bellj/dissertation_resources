package X;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.net.Uri;
import java.io.File;

/* renamed from: X.441  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass441 extends AbstractC68833Wx {
    @Override // X.AbstractC35611iN
    public int getType() {
        return 2;
    }

    public AnonymousClass441(ContentResolver contentResolver, Uri uri, String str, String str2, long j, long j2, long j3) {
        super(contentResolver, uri, str, str2, j, j2, j3);
    }

    @Override // X.AbstractC35611iN
    public Bitmap Aem(int i) {
        File file;
        String str = this.A05;
        if (str == null) {
            file = null;
        } else {
            file = new File(str);
        }
        return C26521Du.A01(file);
    }
}
