package X;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/* renamed from: X.3Tm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67943Tm implements AbstractC17450qp {
    public final AbstractC17450qp A00;

    public C67943Tm(AbstractC17450qp r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC17450qp
    public Object A9j(C14220l3 r9, C1093651k r10, C14240l5 r11) {
        String str = r10.A00;
        char c = 65535;
        switch (str.hashCode()) {
            case -1882328740:
                if (str.equals("bk.action.f32.Sqrt")) {
                    c = 0;
                    break;
                }
                break;
            case -1666855539:
                if (str.equals("bk.action.string.ToUpperCase")) {
                    c = 1;
                    break;
                }
                break;
            case -1225336055:
                if (str.equals("bk.action.string.Contains")) {
                    c = 2;
                    break;
                }
                break;
            case -965327084:
                if (str.equals("bk.action.string.Join")) {
                    c = 3;
                    break;
                }
                break;
            case 944474301:
                if (str.equals("bk.action.map.Filter")) {
                    c = 4;
                    break;
                }
                break;
            case 975567453:
                if (str.equals("bk.action.string.ValueOfNumberInBase")) {
                    c = 5;
                    break;
                }
                break;
            case 1377663097:
                if (str.equals("bk.action.map.Keys")) {
                    c = 6;
                    break;
                }
                break;
            case 1395153511:
                if (str.equals("bk.action.map.Values")) {
                    c = 7;
                    break;
                }
                break;
            case 1740388232:
                if (str.equals("bk.action.f32.Log")) {
                    c = '\b';
                    break;
                }
                break;
            case 1740392092:
                if (str.equals("bk.action.f32.Pow")) {
                    c = '\t';
                    break;
                }
                break;
            case 1867263777:
                if (str.equals("bk.action.string.StartsWith")) {
                    c = '\n';
                    break;
                }
                break;
            case 1890080876:
                if (str.equals("bk.action.string.ToLowerCase")) {
                    c = 11;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                return C64983Hr.A00(Math.sqrt(((Number) r9.A00.get(0)).doubleValue()));
            case 1:
                return C12960it.A0g(r9.A00, 0).toUpperCase(Locale.ROOT);
            case 2:
                List list = r9.A00;
                return Boolean.valueOf(C12960it.A0g(list, 0).contains(C12960it.A0g(list, 1)));
            case 3:
                List list2 = r9.A00;
                String A0g = C12960it.A0g(list2, 0);
                r9.A00(1);
                List A10 = C12980iv.A10(list2, 1);
                if (A0g != null) {
                    StringBuilder A0h = C12960it.A0h();
                    for (int i = 0; i < A10.size(); i++) {
                        String A0g2 = C12960it.A0g(A10, i);
                        if (A0g2 != null) {
                            A0h.append(A0g2);
                            if (i < C12990iw.A09(A10, 1)) {
                                A0h.append(A0g);
                            }
                        } else {
                            throw C12970iu.A0f("element must not be null");
                        }
                    }
                    return A0h.toString();
                }
                throw C12970iu.A0f("delimeter must not be null");
            case 4:
                List list3 = r9.A00;
                Map A0u = C12990iw.A0u(list3, 0);
                C94724cR r6 = (C94724cR) list3.get(1);
                HashMap A11 = C12970iu.A11();
                Iterator A0n = C12960it.A0n(A0u);
                while (A0n.hasNext()) {
                    Map.Entry A15 = C12970iu.A15(A0n);
                    C14210l2 r1 = new C14210l2();
                    r1.A05(A15.getKey(), 0);
                    try {
                        if (C64983Hr.A02(AnonymousClass3AG.A00(C14210l2.A01(r1, A15.getValue(), 1), r6.A00, r11))) {
                            C12990iw.A1Q(A15.getKey(), A11, A15);
                        }
                    } catch (C87444Bn e) {
                        throw new RuntimeException(e);
                    }
                }
                return A11;
            case 5:
                List list4 = r9.A00;
                long A0G = C12980iv.A0G(list4.get(0));
                long A05 = (long) C12960it.A05(list4.get(1));
                if (A05 >= 2 && A05 <= 36) {
                    return Long.toString(A0G, (int) A05);
                }
                throw C12970iu.A0f("radix parameter of string.ValueOfNumberInBase must be between 2 and 36");
            case 6:
                return C12980iv.A0x(C12990iw.A0u(r9.A00, 0).keySet());
            case 7:
                return C12980iv.A0x(C12990iw.A0u(r9.A00, 0).values());
            case '\b':
                return C64983Hr.A00(Math.log(((Number) r9.A00.get(0)).doubleValue()));
            case '\t':
                List list5 = r9.A00;
                return C64983Hr.A00(Math.pow(((Number) list5.get(0)).doubleValue(), ((Number) list5.get(1)).doubleValue()));
            case '\n':
                List list6 = r9.A00;
                return Boolean.valueOf(C12960it.A0g(list6, 0).startsWith(C12960it.A0g(list6, 1)));
            case 11:
                return C12960it.A0g(r9.A00, 0).toLowerCase(Locale.ROOT);
            default:
                AbstractC17450qp r0 = this.A00;
                if (r0 != null) {
                    return r0.A9j(r9, r10, r11);
                }
                throw new AnonymousClass5H5(C12960it.A0d(str, C12960it.A0k("unknown function ")));
        }
    }
}
