package X;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import com.whatsapp.avatar.profilephoto.AvatarProfilePhotoActivity;
import com.whatsapp.avatar.profilephoto.AvatarProfilePhotoImageView;

/* renamed from: X.3NU  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3NU implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ View A00;
    public final /* synthetic */ AvatarProfilePhotoActivity A01;

    public AnonymousClass3NU(View view, AvatarProfilePhotoActivity avatarProfilePhotoActivity) {
        this.A00 = view;
        this.A01 = avatarProfilePhotoActivity;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        Integer valueOf;
        Integer valueOf2;
        C12980iv.A1E(this.A00, this);
        AvatarProfilePhotoActivity avatarProfilePhotoActivity = this.A01;
        View view = avatarProfilePhotoActivity.A01;
        ViewGroup.LayoutParams layoutParams = null;
        if (view != null && (valueOf = Integer.valueOf(view.getWidth())) != null) {
            int intValue = valueOf.intValue();
            View view2 = avatarProfilePhotoActivity.A01;
            if (view2 != null && (valueOf2 = Integer.valueOf(view2.getHeight())) != null) {
                int intValue2 = valueOf2.intValue();
                if (intValue >= intValue2) {
                    intValue = intValue2;
                }
                int i = (int) (((double) intValue) * 0.65d);
                AvatarProfilePhotoImageView avatarProfilePhotoImageView = avatarProfilePhotoActivity.A06;
                if (avatarProfilePhotoImageView != null) {
                    ViewGroup.LayoutParams layoutParams2 = avatarProfilePhotoImageView.getLayoutParams();
                    if (layoutParams2 != null) {
                        layoutParams2.width = i;
                        layoutParams2.height = i;
                        layoutParams = layoutParams2;
                    }
                    avatarProfilePhotoImageView.setLayoutParams(layoutParams);
                }
            }
        }
    }
}
