package X;

import android.content.SharedPreferences;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.3Fr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64473Fr {
    public int A00;
    public int A01;
    public int A02;
    public long A03;
    public long A04;
    public long A05;
    public long A06;
    public long A07;
    public long A08;
    public long A09;
    public long A0A;
    public long A0B;
    public long A0C;
    public boolean A0D;
    public final AbstractC15710nm A0E;
    public final C14330lG A0F;
    public final C33241dg A0G;
    public final C15820nx A0H;
    public final C22730zY A0I;
    public final AnonymousClass10N A0J;
    public final AnonymousClass28Q A0K;
    public final C27051Fv A0L;
    public final AbstractC44761zV A0M;
    public final AnonymousClass1D6 A0N;
    public final C33491eA A0O;
    public final C44791zY A0P;
    public final AnonymousClass5TK A0Q;
    public final C15810nw A0R;
    public final C17050qB A0S;
    public final C14830m7 A0T;
    public final C16590pI A0U;
    public final C15890o4 A0V;
    public final C14820m6 A0W;
    public final C15880o3 A0X;
    public final C16490p7 A0Y;
    public final C14850m9 A0Z;
    public final C16120oU A0a;
    public final C44931zn A0b;
    public final C17220qS A0c;
    public final String A0d;
    public final List A0e;
    public final Map A0f = new ConcurrentHashMap();
    public final AtomicInteger A0g = new AtomicInteger(0);
    public final AtomicLong A0h = new AtomicLong(0);
    public final AtomicLong A0i;
    public final AtomicLong A0j = new AtomicLong(0);
    public final AtomicLong A0k;
    public final boolean A0l;

    public C64473Fr(AbstractC15710nm r5, C14330lG r6, C33241dg r7, C15820nx r8, C22730zY r9, AnonymousClass10N r10, AnonymousClass28Q r11, C27051Fv r12, AbstractC44761zV r13, AnonymousClass1D6 r14, C44791zY r15, AnonymousClass5TK r16, C15810nw r17, C17050qB r18, C14830m7 r19, C16590pI r20, C15890o4 r21, C14820m6 r22, C15880o3 r23, C16490p7 r24, C14850m9 r25, C16120oU r26, C44931zn r27, C17220qS r28, String str, List list, AtomicLong atomicLong, AtomicLong atomicLong2, boolean z) {
        this.A0U = r20;
        this.A0d = str;
        this.A0T = r19;
        this.A0Z = r25;
        this.A0E = r5;
        this.A0e = list;
        this.A0F = r6;
        this.A0J = r10;
        this.A0a = r26;
        this.A0R = r17;
        this.A0k = atomicLong;
        this.A0c = r28;
        this.A0M = r13;
        this.A0H = r8;
        this.A0K = r11;
        this.A0S = r18;
        this.A0P = r15;
        this.A0l = z;
        this.A0i = atomicLong2;
        this.A0N = r14;
        this.A0X = r23;
        this.A0Y = r24;
        this.A0L = r12;
        this.A0V = r21;
        this.A0W = r22;
        this.A0I = r9;
        this.A0b = r27;
        this.A0G = r7;
        this.A0Q = r16;
        this.A0O = new C33491eA(r11);
    }

    public final void A00() {
        C44931zn r3;
        long j;
        double d = 0.0d;
        try {
            File A09 = this.A0X.A09();
            r3 = this.A0b;
            r3.A04 = Double.valueOf(A09 != null ? (double) A09.length() : 0.0d);
        } catch (IOException e) {
            Log.e("gdrive/backup", e);
            r3 = this.A0b;
            r3.A04 = Double.valueOf(0.0d);
        }
        AtomicLong atomicLong = this.A0i;
        if (((double) atomicLong.get()) > r3.A04.doubleValue()) {
            d = ((double) atomicLong.get()) - r3.A04.doubleValue();
        }
        r3.A06 = Double.valueOf(d);
        r3.A08 = Double.valueOf((double) this.A07);
        long j2 = 0;
        for (File file : this.A0e) {
            long j3 = 0;
            if (file.exists()) {
                if (file.isDirectory()) {
                    LinkedList linkedList = new LinkedList();
                    linkedList.add(file);
                    long j4 = 0;
                    while (linkedList.peek() != null) {
                        Object poll = linkedList.poll();
                        AnonymousClass009.A05(poll);
                        File[] listFiles = ((File) poll).listFiles();
                        if (listFiles != null) {
                            for (File file2 : listFiles) {
                                if (file2.exists()) {
                                    if (file2.isDirectory()) {
                                        linkedList.add(file2);
                                    } else {
                                        j4 += file2.length() > 0 ? 1 : 0;
                                    }
                                }
                            }
                        }
                    }
                    j3 = j4;
                } else if (file.length() > 0) {
                    j3 = 1;
                }
            }
            j2 += j3;
        }
        r3.A05 = Double.valueOf((double) j2);
        SharedPreferences sharedPreferences = this.A0W.A00;
        r3.A0N = C12980iv.A0l(sharedPreferences.getInt("gdrive_successive_backup_failed_count", 0));
        r3.A01 = Boolean.valueOf(sharedPreferences.getBoolean("gdrive_include_videos_in_backup", false));
        if (this.A0I.A00 == 1) {
            j = 1;
        } else {
            j = 0;
        }
        r3.A0J = Long.valueOf(j);
        if (r3.A0E == null) {
            r3.A0E = 1;
        }
        C44791zY r2 = this.A0P;
        r3.A07 = Double.valueOf((double) (r2.A07.A00 + r2.A0H.get()));
        Integer num = r3.A0D;
        if (num == null || num.intValue() == 1) {
            r3.A0D = C12970iu.A0g();
        }
        this.A0a.A06(r3);
        Log.i(C12960it.A0d(C44771zW.A0A(r3), C12960it.A0k("gdrive/backup ")));
    }

    public void A01(File file, String str) {
        if (this.A0f.get(C44771zW.A08(this.A0U.A00, this.A0R, file)) == null) {
            StringBuilder A0k = C12960it.A0k("gdrive/backup/files/");
            A0k.append(str);
            Log.e(C12960it.A0d("/cancel-backup", A0k));
            StringBuilder A0k2 = C12960it.A0k("File ");
            A0k2.append(file);
            throw new C84123yS(C12960it.A0d(" not backed up", A0k2));
        }
    }

    public void A02(boolean z) {
        this.A0J.A0A(false);
        if (z) {
            SharedPreferences sharedPreferences = this.A0W.A00;
            int i = sharedPreferences.getInt("gdrive_successive_backup_failed_count", 0) + 1;
            Log.i(C12960it.A0W(i, "wa-shared-preferences/increment-backup-failed-count/updated-count/"));
            C12960it.A0u(sharedPreferences, "gdrive_successive_backup_failed_count", i);
            if (sharedPreferences.getInt("gdrive_successive_backup_failed_count", 0) >= 4) {
                StringBuilder A0k = C12960it.A0k("gdrive/backup ");
                A0k.append(sharedPreferences.getInt("gdrive_successive_backup_failed_count", 0));
                Log.e(C12960it.A0d(" successive backups have failed, this is probably unusual.", A0k));
            }
        }
        A00();
        this.A07 = 0;
        this.A0W.A0V(0);
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:174:0x05ac, code lost:
        r29 = r33.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:280:0x089c, code lost:
        com.whatsapp.util.Log.i("gdrive/backup/files waiting for backup to finish...");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:281:0x08a1, code lost:
        r28 = r28 & r11.await(86400000, java.util.concurrent.TimeUnit.MILLISECONDS);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:282:0x08ad, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:283:0x08ae, code lost:
        com.whatsapp.util.Log.e("gdrive/backup/files upload interrupted", r3);
        r0.A0D = 31;
        r28 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:452:0x0d96, code lost:
        if (r6 == false) goto L_0x0f40;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0035, code lost:
        if (r3.A01() != false) goto L_0x0037;
     */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x03d1 A[Catch: 3yK -> 0x06f2, 2AR -> 0x0cec, 1VO -> 0x0d88, LOOP:5: B:103:0x03cb->B:105:0x03d1, LOOP_END, TryCatch #8 {3yK -> 0x06f2, blocks: (B:92:0x0357, B:94:0x0395, B:96:0x03a6, B:97:0x03ab, B:99:0x03ba, B:101:0x03c2, B:102:0x03c5, B:103:0x03cb, B:105:0x03d1, B:107:0x03e2, B:108:0x03f9, B:128:0x04c6, B:131:0x04d3, B:170:0x059d, B:172:0x05a4, B:174:0x05ac, B:175:0x05b0, B:177:0x05b6, B:179:0x05c2, B:181:0x05d4, B:183:0x05dc, B:184:0x05e5, B:186:0x05eb, B:188:0x05f5, B:191:0x060a, B:193:0x0610, B:194:0x0614, B:196:0x061a, B:198:0x0626, B:199:0x063a, B:200:0x0644, B:202:0x064a, B:204:0x0650, B:206:0x0660, B:211:0x0672, B:212:0x069b, B:213:0x06a1, B:215:0x06a7, B:217:0x06b1, B:218:0x06b4, B:219:0x06b6, B:220:0x06b8, B:227:0x06ec, B:230:0x06f1), top: B:539:0x0357, outer: #17 }] */
    /* JADX WARNING: Removed duplicated region for block: B:215:0x06a7 A[Catch: 3yK -> 0x06f2, 2AR -> 0x0cec, 1VO -> 0x0d88, TryCatch #8 {3yK -> 0x06f2, blocks: (B:92:0x0357, B:94:0x0395, B:96:0x03a6, B:97:0x03ab, B:99:0x03ba, B:101:0x03c2, B:102:0x03c5, B:103:0x03cb, B:105:0x03d1, B:107:0x03e2, B:108:0x03f9, B:128:0x04c6, B:131:0x04d3, B:170:0x059d, B:172:0x05a4, B:174:0x05ac, B:175:0x05b0, B:177:0x05b6, B:179:0x05c2, B:181:0x05d4, B:183:0x05dc, B:184:0x05e5, B:186:0x05eb, B:188:0x05f5, B:191:0x060a, B:193:0x0610, B:194:0x0614, B:196:0x061a, B:198:0x0626, B:199:0x063a, B:200:0x0644, B:202:0x064a, B:204:0x0650, B:206:0x0660, B:211:0x0672, B:212:0x069b, B:213:0x06a1, B:215:0x06a7, B:217:0x06b1, B:218:0x06b4, B:219:0x06b6, B:220:0x06b8, B:227:0x06ec, B:230:0x06f1), top: B:539:0x0357, outer: #17 }] */
    /* JADX WARNING: Removed duplicated region for block: B:245:0x0772 A[Catch: 2AR -> 0x0cec, 1VO -> 0x0d88, LOOP:14: B:243:0x076c->B:245:0x0772, LOOP_END, TryCatch #17 {1VO -> 0x0d88, 2AR -> 0x0cec, blocks: (B:16:0x00a9, B:18:0x00be, B:19:0x00cb, B:21:0x00d1, B:22:0x00db, B:23:0x00ea, B:25:0x0105, B:27:0x010b, B:29:0x011d, B:31:0x0123, B:33:0x0131, B:35:0x0147, B:37:0x0160, B:38:0x0169, B:39:0x016e, B:40:0x0174, B:42:0x017a, B:44:0x019f, B:46:0x01aa, B:48:0x01cb, B:49:0x01d0, B:50:0x01d8, B:52:0x01ec, B:54:0x0221, B:56:0x022b, B:57:0x025d, B:59:0x0263, B:61:0x026b, B:62:0x0277, B:63:0x027b, B:65:0x0281, B:67:0x0289, B:69:0x028f, B:71:0x02a0, B:73:0x02a8, B:75:0x02b2, B:77:0x02ba, B:78:0x02bf, B:79:0x02fa, B:81:0x0300, B:82:0x030c, B:84:0x031e, B:86:0x0324, B:87:0x0331, B:88:0x033f, B:90:0x034d, B:91:0x0352, B:92:0x0357, B:94:0x0395, B:96:0x03a6, B:97:0x03ab, B:99:0x03ba, B:101:0x03c2, B:102:0x03c5, B:103:0x03cb, B:105:0x03d1, B:107:0x03e2, B:108:0x03f9, B:128:0x04c6, B:131:0x04d3, B:170:0x059d, B:172:0x05a4, B:174:0x05ac, B:175:0x05b0, B:177:0x05b6, B:179:0x05c2, B:181:0x05d4, B:183:0x05dc, B:184:0x05e5, B:186:0x05eb, B:188:0x05f5, B:191:0x060a, B:193:0x0610, B:194:0x0614, B:196:0x061a, B:198:0x0626, B:199:0x063a, B:200:0x0644, B:202:0x064a, B:204:0x0650, B:206:0x0660, B:211:0x0672, B:212:0x069b, B:213:0x06a1, B:215:0x06a7, B:217:0x06b1, B:218:0x06b4, B:219:0x06b6, B:220:0x06b8, B:227:0x06ec, B:230:0x06f1, B:231:0x06f2, B:233:0x0700, B:235:0x0711, B:236:0x071c, B:237:0x071f, B:238:0x072e, B:240:0x074c, B:242:0x0752, B:243:0x076c, B:245:0x0772, B:246:0x0782, B:248:0x0788, B:250:0x0791, B:251:0x0799, B:254:0x07a7, B:257:0x07b4, B:258:0x07bd, B:259:0x07f6, B:261:0x07fc, B:263:0x0808, B:265:0x0812, B:266:0x082b, B:268:0x0831, B:270:0x0837, B:271:0x0840, B:273:0x0848, B:275:0x0856, B:276:0x087b, B:279:0x0898, B:280:0x089c, B:281:0x08a1, B:283:0x08ae, B:284:0x08bb, B:286:0x08c1, B:288:0x08c9, B:289:0x08e4, B:292:0x08f1, B:293:0x08fb, B:294:0x0900, B:295:0x0901, B:296:0x0915, B:297:0x0916, B:298:0x0947, B:300:0x094d, B:302:0x0959, B:303:0x095b, B:305:0x0967, B:307:0x0972, B:309:0x097f, B:311:0x0989, B:313:0x098f, B:314:0x0992, B:317:0x09a1, B:318:0x09a4, B:321:0x09aa, B:322:0x09b4, B:324:0x09ba, B:326:0x09be, B:328:0x09cb, B:330:0x09d3, B:331:0x09d8, B:333:0x09e0, B:334:0x09e6, B:336:0x09ec, B:337:0x09f1, B:338:0x09f8, B:339:0x0a0b, B:340:0x0a0f, B:342:0x0a8b, B:346:0x0a9d, B:347:0x0a9e, B:348:0x0ab3, B:350:0x0aed, B:351:0x0af6, B:352:0x0b05, B:354:0x0b0b, B:356:0x0b1f, B:357:0x0b24, B:358:0x0b2d, B:360:0x0b37, B:363:0x0b47, B:364:0x0b53, B:366:0x0b78, B:367:0x0b7d, B:369:0x0b82, B:370:0x0b8f, B:372:0x0b95, B:374:0x0bad, B:375:0x0bb8, B:376:0x0bfa, B:378:0x0c00, B:379:0x0c04, B:381:0x0c0a, B:382:0x0c1e, B:384:0x0c4f, B:386:0x0c63, B:389:0x0c93, B:390:0x0c97, B:391:0x0cbd, B:393:0x0cbf, B:394:0x0cc6, B:395:0x0cc7), top: B:550:0x00a9, inners: #0, #2, #8, #13 }] */
    /* JADX WARNING: Removed duplicated region for block: B:248:0x0788 A[Catch: 2AR -> 0x0cec, 1VO -> 0x0d88, TryCatch #17 {1VO -> 0x0d88, 2AR -> 0x0cec, blocks: (B:16:0x00a9, B:18:0x00be, B:19:0x00cb, B:21:0x00d1, B:22:0x00db, B:23:0x00ea, B:25:0x0105, B:27:0x010b, B:29:0x011d, B:31:0x0123, B:33:0x0131, B:35:0x0147, B:37:0x0160, B:38:0x0169, B:39:0x016e, B:40:0x0174, B:42:0x017a, B:44:0x019f, B:46:0x01aa, B:48:0x01cb, B:49:0x01d0, B:50:0x01d8, B:52:0x01ec, B:54:0x0221, B:56:0x022b, B:57:0x025d, B:59:0x0263, B:61:0x026b, B:62:0x0277, B:63:0x027b, B:65:0x0281, B:67:0x0289, B:69:0x028f, B:71:0x02a0, B:73:0x02a8, B:75:0x02b2, B:77:0x02ba, B:78:0x02bf, B:79:0x02fa, B:81:0x0300, B:82:0x030c, B:84:0x031e, B:86:0x0324, B:87:0x0331, B:88:0x033f, B:90:0x034d, B:91:0x0352, B:92:0x0357, B:94:0x0395, B:96:0x03a6, B:97:0x03ab, B:99:0x03ba, B:101:0x03c2, B:102:0x03c5, B:103:0x03cb, B:105:0x03d1, B:107:0x03e2, B:108:0x03f9, B:128:0x04c6, B:131:0x04d3, B:170:0x059d, B:172:0x05a4, B:174:0x05ac, B:175:0x05b0, B:177:0x05b6, B:179:0x05c2, B:181:0x05d4, B:183:0x05dc, B:184:0x05e5, B:186:0x05eb, B:188:0x05f5, B:191:0x060a, B:193:0x0610, B:194:0x0614, B:196:0x061a, B:198:0x0626, B:199:0x063a, B:200:0x0644, B:202:0x064a, B:204:0x0650, B:206:0x0660, B:211:0x0672, B:212:0x069b, B:213:0x06a1, B:215:0x06a7, B:217:0x06b1, B:218:0x06b4, B:219:0x06b6, B:220:0x06b8, B:227:0x06ec, B:230:0x06f1, B:231:0x06f2, B:233:0x0700, B:235:0x0711, B:236:0x071c, B:237:0x071f, B:238:0x072e, B:240:0x074c, B:242:0x0752, B:243:0x076c, B:245:0x0772, B:246:0x0782, B:248:0x0788, B:250:0x0791, B:251:0x0799, B:254:0x07a7, B:257:0x07b4, B:258:0x07bd, B:259:0x07f6, B:261:0x07fc, B:263:0x0808, B:265:0x0812, B:266:0x082b, B:268:0x0831, B:270:0x0837, B:271:0x0840, B:273:0x0848, B:275:0x0856, B:276:0x087b, B:279:0x0898, B:280:0x089c, B:281:0x08a1, B:283:0x08ae, B:284:0x08bb, B:286:0x08c1, B:288:0x08c9, B:289:0x08e4, B:292:0x08f1, B:293:0x08fb, B:294:0x0900, B:295:0x0901, B:296:0x0915, B:297:0x0916, B:298:0x0947, B:300:0x094d, B:302:0x0959, B:303:0x095b, B:305:0x0967, B:307:0x0972, B:309:0x097f, B:311:0x0989, B:313:0x098f, B:314:0x0992, B:317:0x09a1, B:318:0x09a4, B:321:0x09aa, B:322:0x09b4, B:324:0x09ba, B:326:0x09be, B:328:0x09cb, B:330:0x09d3, B:331:0x09d8, B:333:0x09e0, B:334:0x09e6, B:336:0x09ec, B:337:0x09f1, B:338:0x09f8, B:339:0x0a0b, B:340:0x0a0f, B:342:0x0a8b, B:346:0x0a9d, B:347:0x0a9e, B:348:0x0ab3, B:350:0x0aed, B:351:0x0af6, B:352:0x0b05, B:354:0x0b0b, B:356:0x0b1f, B:357:0x0b24, B:358:0x0b2d, B:360:0x0b37, B:363:0x0b47, B:364:0x0b53, B:366:0x0b78, B:367:0x0b7d, B:369:0x0b82, B:370:0x0b8f, B:372:0x0b95, B:374:0x0bad, B:375:0x0bb8, B:376:0x0bfa, B:378:0x0c00, B:379:0x0c04, B:381:0x0c0a, B:382:0x0c1e, B:384:0x0c4f, B:386:0x0c63, B:389:0x0c93, B:390:0x0c97, B:391:0x0cbd, B:393:0x0cbf, B:394:0x0cc6, B:395:0x0cc7), top: B:550:0x00a9, inners: #0, #2, #8, #13 }] */
    /* JADX WARNING: Removed duplicated region for block: B:317:0x09a1 A[Catch: IOException -> 0x0b77, 2AR -> 0x0cec, 1VO -> 0x0d88, TryCatch #0 {IOException -> 0x0b77, blocks: (B:297:0x0916, B:298:0x0947, B:300:0x094d, B:302:0x0959, B:303:0x095b, B:305:0x0967, B:307:0x0972, B:309:0x097f, B:311:0x0989, B:313:0x098f, B:314:0x0992, B:317:0x09a1, B:318:0x09a4, B:321:0x09aa, B:322:0x09b4, B:324:0x09ba, B:326:0x09be, B:328:0x09cb, B:330:0x09d3, B:331:0x09d8, B:333:0x09e0, B:334:0x09e6, B:336:0x09ec, B:337:0x09f1, B:338:0x09f8, B:339:0x0a0b, B:340:0x0a0f, B:342:0x0a8b, B:346:0x0a9d, B:347:0x0a9e, B:348:0x0ab3, B:350:0x0aed, B:351:0x0af6, B:352:0x0b05, B:354:0x0b0b, B:356:0x0b1f, B:357:0x0b24, B:358:0x0b2d, B:360:0x0b37, B:363:0x0b47, B:364:0x0b53), top: B:527:0x0916, outer: #17 }] */
    /* JADX WARNING: Removed duplicated region for block: B:318:0x09a4 A[Catch: IOException -> 0x0b77, 2AR -> 0x0cec, 1VO -> 0x0d88, TryCatch #0 {IOException -> 0x0b77, blocks: (B:297:0x0916, B:298:0x0947, B:300:0x094d, B:302:0x0959, B:303:0x095b, B:305:0x0967, B:307:0x0972, B:309:0x097f, B:311:0x0989, B:313:0x098f, B:314:0x0992, B:317:0x09a1, B:318:0x09a4, B:321:0x09aa, B:322:0x09b4, B:324:0x09ba, B:326:0x09be, B:328:0x09cb, B:330:0x09d3, B:331:0x09d8, B:333:0x09e0, B:334:0x09e6, B:336:0x09ec, B:337:0x09f1, B:338:0x09f8, B:339:0x0a0b, B:340:0x0a0f, B:342:0x0a8b, B:346:0x0a9d, B:347:0x0a9e, B:348:0x0ab3, B:350:0x0aed, B:351:0x0af6, B:352:0x0b05, B:354:0x0b0b, B:356:0x0b1f, B:357:0x0b24, B:358:0x0b2d, B:360:0x0b37, B:363:0x0b47, B:364:0x0b53), top: B:527:0x0916, outer: #17 }] */
    /* JADX WARNING: Removed duplicated region for block: B:369:0x0b82 A[Catch: 2AR -> 0x0cec, 1VO -> 0x0d88, TryCatch #17 {1VO -> 0x0d88, 2AR -> 0x0cec, blocks: (B:16:0x00a9, B:18:0x00be, B:19:0x00cb, B:21:0x00d1, B:22:0x00db, B:23:0x00ea, B:25:0x0105, B:27:0x010b, B:29:0x011d, B:31:0x0123, B:33:0x0131, B:35:0x0147, B:37:0x0160, B:38:0x0169, B:39:0x016e, B:40:0x0174, B:42:0x017a, B:44:0x019f, B:46:0x01aa, B:48:0x01cb, B:49:0x01d0, B:50:0x01d8, B:52:0x01ec, B:54:0x0221, B:56:0x022b, B:57:0x025d, B:59:0x0263, B:61:0x026b, B:62:0x0277, B:63:0x027b, B:65:0x0281, B:67:0x0289, B:69:0x028f, B:71:0x02a0, B:73:0x02a8, B:75:0x02b2, B:77:0x02ba, B:78:0x02bf, B:79:0x02fa, B:81:0x0300, B:82:0x030c, B:84:0x031e, B:86:0x0324, B:87:0x0331, B:88:0x033f, B:90:0x034d, B:91:0x0352, B:92:0x0357, B:94:0x0395, B:96:0x03a6, B:97:0x03ab, B:99:0x03ba, B:101:0x03c2, B:102:0x03c5, B:103:0x03cb, B:105:0x03d1, B:107:0x03e2, B:108:0x03f9, B:128:0x04c6, B:131:0x04d3, B:170:0x059d, B:172:0x05a4, B:174:0x05ac, B:175:0x05b0, B:177:0x05b6, B:179:0x05c2, B:181:0x05d4, B:183:0x05dc, B:184:0x05e5, B:186:0x05eb, B:188:0x05f5, B:191:0x060a, B:193:0x0610, B:194:0x0614, B:196:0x061a, B:198:0x0626, B:199:0x063a, B:200:0x0644, B:202:0x064a, B:204:0x0650, B:206:0x0660, B:211:0x0672, B:212:0x069b, B:213:0x06a1, B:215:0x06a7, B:217:0x06b1, B:218:0x06b4, B:219:0x06b6, B:220:0x06b8, B:227:0x06ec, B:230:0x06f1, B:231:0x06f2, B:233:0x0700, B:235:0x0711, B:236:0x071c, B:237:0x071f, B:238:0x072e, B:240:0x074c, B:242:0x0752, B:243:0x076c, B:245:0x0772, B:246:0x0782, B:248:0x0788, B:250:0x0791, B:251:0x0799, B:254:0x07a7, B:257:0x07b4, B:258:0x07bd, B:259:0x07f6, B:261:0x07fc, B:263:0x0808, B:265:0x0812, B:266:0x082b, B:268:0x0831, B:270:0x0837, B:271:0x0840, B:273:0x0848, B:275:0x0856, B:276:0x087b, B:279:0x0898, B:280:0x089c, B:281:0x08a1, B:283:0x08ae, B:284:0x08bb, B:286:0x08c1, B:288:0x08c9, B:289:0x08e4, B:292:0x08f1, B:293:0x08fb, B:294:0x0900, B:295:0x0901, B:296:0x0915, B:297:0x0916, B:298:0x0947, B:300:0x094d, B:302:0x0959, B:303:0x095b, B:305:0x0967, B:307:0x0972, B:309:0x097f, B:311:0x0989, B:313:0x098f, B:314:0x0992, B:317:0x09a1, B:318:0x09a4, B:321:0x09aa, B:322:0x09b4, B:324:0x09ba, B:326:0x09be, B:328:0x09cb, B:330:0x09d3, B:331:0x09d8, B:333:0x09e0, B:334:0x09e6, B:336:0x09ec, B:337:0x09f1, B:338:0x09f8, B:339:0x0a0b, B:340:0x0a0f, B:342:0x0a8b, B:346:0x0a9d, B:347:0x0a9e, B:348:0x0ab3, B:350:0x0aed, B:351:0x0af6, B:352:0x0b05, B:354:0x0b0b, B:356:0x0b1f, B:357:0x0b24, B:358:0x0b2d, B:360:0x0b37, B:363:0x0b47, B:364:0x0b53, B:366:0x0b78, B:367:0x0b7d, B:369:0x0b82, B:370:0x0b8f, B:372:0x0b95, B:374:0x0bad, B:375:0x0bb8, B:376:0x0bfa, B:378:0x0c00, B:379:0x0c04, B:381:0x0c0a, B:382:0x0c1e, B:384:0x0c4f, B:386:0x0c63, B:389:0x0c93, B:390:0x0c97, B:391:0x0cbd, B:393:0x0cbf, B:394:0x0cc6, B:395:0x0cc7), top: B:550:0x00a9, inners: #0, #2, #8, #13 }] */
    /* JADX WARNING: Removed duplicated region for block: B:370:0x0b8f A[Catch: 2AR -> 0x0cec, 1VO -> 0x0d88, TryCatch #17 {1VO -> 0x0d88, 2AR -> 0x0cec, blocks: (B:16:0x00a9, B:18:0x00be, B:19:0x00cb, B:21:0x00d1, B:22:0x00db, B:23:0x00ea, B:25:0x0105, B:27:0x010b, B:29:0x011d, B:31:0x0123, B:33:0x0131, B:35:0x0147, B:37:0x0160, B:38:0x0169, B:39:0x016e, B:40:0x0174, B:42:0x017a, B:44:0x019f, B:46:0x01aa, B:48:0x01cb, B:49:0x01d0, B:50:0x01d8, B:52:0x01ec, B:54:0x0221, B:56:0x022b, B:57:0x025d, B:59:0x0263, B:61:0x026b, B:62:0x0277, B:63:0x027b, B:65:0x0281, B:67:0x0289, B:69:0x028f, B:71:0x02a0, B:73:0x02a8, B:75:0x02b2, B:77:0x02ba, B:78:0x02bf, B:79:0x02fa, B:81:0x0300, B:82:0x030c, B:84:0x031e, B:86:0x0324, B:87:0x0331, B:88:0x033f, B:90:0x034d, B:91:0x0352, B:92:0x0357, B:94:0x0395, B:96:0x03a6, B:97:0x03ab, B:99:0x03ba, B:101:0x03c2, B:102:0x03c5, B:103:0x03cb, B:105:0x03d1, B:107:0x03e2, B:108:0x03f9, B:128:0x04c6, B:131:0x04d3, B:170:0x059d, B:172:0x05a4, B:174:0x05ac, B:175:0x05b0, B:177:0x05b6, B:179:0x05c2, B:181:0x05d4, B:183:0x05dc, B:184:0x05e5, B:186:0x05eb, B:188:0x05f5, B:191:0x060a, B:193:0x0610, B:194:0x0614, B:196:0x061a, B:198:0x0626, B:199:0x063a, B:200:0x0644, B:202:0x064a, B:204:0x0650, B:206:0x0660, B:211:0x0672, B:212:0x069b, B:213:0x06a1, B:215:0x06a7, B:217:0x06b1, B:218:0x06b4, B:219:0x06b6, B:220:0x06b8, B:227:0x06ec, B:230:0x06f1, B:231:0x06f2, B:233:0x0700, B:235:0x0711, B:236:0x071c, B:237:0x071f, B:238:0x072e, B:240:0x074c, B:242:0x0752, B:243:0x076c, B:245:0x0772, B:246:0x0782, B:248:0x0788, B:250:0x0791, B:251:0x0799, B:254:0x07a7, B:257:0x07b4, B:258:0x07bd, B:259:0x07f6, B:261:0x07fc, B:263:0x0808, B:265:0x0812, B:266:0x082b, B:268:0x0831, B:270:0x0837, B:271:0x0840, B:273:0x0848, B:275:0x0856, B:276:0x087b, B:279:0x0898, B:280:0x089c, B:281:0x08a1, B:283:0x08ae, B:284:0x08bb, B:286:0x08c1, B:288:0x08c9, B:289:0x08e4, B:292:0x08f1, B:293:0x08fb, B:294:0x0900, B:295:0x0901, B:296:0x0915, B:297:0x0916, B:298:0x0947, B:300:0x094d, B:302:0x0959, B:303:0x095b, B:305:0x0967, B:307:0x0972, B:309:0x097f, B:311:0x0989, B:313:0x098f, B:314:0x0992, B:317:0x09a1, B:318:0x09a4, B:321:0x09aa, B:322:0x09b4, B:324:0x09ba, B:326:0x09be, B:328:0x09cb, B:330:0x09d3, B:331:0x09d8, B:333:0x09e0, B:334:0x09e6, B:336:0x09ec, B:337:0x09f1, B:338:0x09f8, B:339:0x0a0b, B:340:0x0a0f, B:342:0x0a8b, B:346:0x0a9d, B:347:0x0a9e, B:348:0x0ab3, B:350:0x0aed, B:351:0x0af6, B:352:0x0b05, B:354:0x0b0b, B:356:0x0b1f, B:357:0x0b24, B:358:0x0b2d, B:360:0x0b37, B:363:0x0b47, B:364:0x0b53, B:366:0x0b78, B:367:0x0b7d, B:369:0x0b82, B:370:0x0b8f, B:372:0x0b95, B:374:0x0bad, B:375:0x0bb8, B:376:0x0bfa, B:378:0x0c00, B:379:0x0c04, B:381:0x0c0a, B:382:0x0c1e, B:384:0x0c4f, B:386:0x0c63, B:389:0x0c93, B:390:0x0c97, B:391:0x0cbd, B:393:0x0cbf, B:394:0x0cc6, B:395:0x0cc7), top: B:550:0x00a9, inners: #0, #2, #8, #13 }] */
    /* JADX WARNING: Removed duplicated region for block: B:388:0x0c85 A[Catch: 2AR -> 0x0cea, 1VO -> 0x0ce7, TRY_ENTER, TRY_LEAVE, TryCatch #16 {1VO -> 0x0ce7, 2AR -> 0x0cea, blocks: (B:388:0x0c85, B:397:0x0cd4, B:399:0x0cde), top: B:551:0x00bc }] */
    /* JADX WARNING: Removed duplicated region for block: B:399:0x0cde A[Catch: 2AR -> 0x0cea, 1VO -> 0x0ce7, TRY_LEAVE, TryCatch #16 {1VO -> 0x0ce7, 2AR -> 0x0cea, blocks: (B:388:0x0c85, B:397:0x0cd4, B:399:0x0cde), top: B:551:0x00bc }] */
    /* JADX WARNING: Removed duplicated region for block: B:406:0x0cf9  */
    /* JADX WARNING: Removed duplicated region for block: B:420:0x0d39  */
    /* JADX WARNING: Removed duplicated region for block: B:455:0x0e31  */
    /* JADX WARNING: Removed duplicated region for block: B:458:0x0e3f  */
    /* JADX WARNING: Removed duplicated region for block: B:461:0x0e99  */
    /* JADX WARNING: Removed duplicated region for block: B:467:0x0f10  */
    /* JADX WARNING: Removed duplicated region for block: B:471:0x0f1d  */
    /* JADX WARNING: Removed duplicated region for block: B:472:0x0f3c  */
    /* JADX WARNING: Removed duplicated region for block: B:540:0x0f4e A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:619:0x09b4 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:621:0x09aa A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x03ba A[Catch: 3yK -> 0x06f2, 2AR -> 0x0cec, 1VO -> 0x0d88, TryCatch #8 {3yK -> 0x06f2, blocks: (B:92:0x0357, B:94:0x0395, B:96:0x03a6, B:97:0x03ab, B:99:0x03ba, B:101:0x03c2, B:102:0x03c5, B:103:0x03cb, B:105:0x03d1, B:107:0x03e2, B:108:0x03f9, B:128:0x04c6, B:131:0x04d3, B:170:0x059d, B:172:0x05a4, B:174:0x05ac, B:175:0x05b0, B:177:0x05b6, B:179:0x05c2, B:181:0x05d4, B:183:0x05dc, B:184:0x05e5, B:186:0x05eb, B:188:0x05f5, B:191:0x060a, B:193:0x0610, B:194:0x0614, B:196:0x061a, B:198:0x0626, B:199:0x063a, B:200:0x0644, B:202:0x064a, B:204:0x0650, B:206:0x0660, B:211:0x0672, B:212:0x069b, B:213:0x06a1, B:215:0x06a7, B:217:0x06b1, B:218:0x06b4, B:219:0x06b6, B:220:0x06b8, B:227:0x06ec, B:230:0x06f1), top: B:539:0x0357, outer: #17 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A03() {
        /*
        // Method dump skipped, instructions count: 4148
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C64473Fr.A03():boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0056, code lost:
        if (r2.length() <= 0) goto L_0x0058;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A04(java.util.List r26, java.util.List r27, java.util.List r28) {
        /*
            r25 = this;
            r7 = r26
            r7.size()
            r0 = 0
            java.util.concurrent.atomic.AtomicReference r5 = new java.util.concurrent.atomic.AtomicReference
            r5.<init>(r0)
            int r0 = r7.size()
            java.util.concurrent.CountDownLatch r6 = new java.util.concurrent.CountDownLatch
            r6.<init>(r0)
            r0 = 1
            java.util.concurrent.atomic.AtomicBoolean r4 = new java.util.concurrent.atomic.AtomicBoolean
            r4.<init>(r0)
            int r1 = r7.size()
            r0 = 100
            if (r1 <= r0) goto L_0x00a3
            int r15 = r7.size()
            int r15 = r15 / r0
        L_0x0027:
            r14 = 0
            r3 = 0
        L_0x0029:
            int r0 = r7.size()
            if (r3 >= r0) goto L_0x00a5
            r8 = r25
            X.1zV r0 = r8.A0M
            boolean r0 = r0.A00()
            if (r0 != 0) goto L_0x003a
            return r14
        L_0x003a:
            int r0 = r3 % r15
            boolean r24 = X.C12960it.A1T(r0)
            java.lang.Object r2 = r7.get(r3)
            java.io.File r2 = (java.io.File) r2
            boolean r0 = r2.exists()
            r13 = 1
            if (r0 == 0) goto L_0x0058
            long r11 = r2.length()
            r9 = 0
            int r0 = (r11 > r9 ? 1 : (r11 == r9 ? 0 : -1))
            r10 = 1
            if (r0 > 0) goto L_0x0059
        L_0x0058:
            r10 = 0
        L_0x0059:
            java.lang.String r9 = r2.getCanonicalPath()     // Catch: IOException -> 0x005e
            goto L_0x0069
        L_0x005e:
            r1 = move-exception
            java.lang.String r0 = "gdrive/backup/failed to get canonical path for "
            java.lang.String r0 = X.C12960it.A0b(r0, r2)
            com.whatsapp.util.Log.e(r0, r1)
            r9 = 0
        L_0x0069:
            X.28Q r1 = r8.A0K
            if (r9 == 0) goto L_0x007c
            java.lang.String r0 = r1.A05
            boolean r0 = r9.startsWith(r0)
            if (r0 == 0) goto L_0x007c
            boolean r0 = r1.A00(r2, r9)
            if (r0 != 0) goto L_0x007c
            r13 = 0
        L_0x007c:
            r10 = r10 & r13
            if (r10 == 0) goto L_0x009c
            X.2jE r1 = new X.2jE
            r20 = r27
            r21 = r28
            r22 = r6
            r23 = r5
            r18 = r2
            r19 = r9
            r17 = r8
            r16 = r1
            r16.<init>(r18, r19, r20, r21, r22, r23, r24)
            java.util.concurrent.ThreadPoolExecutor r0 = X.AnonymousClass3B5.A02
            r0.execute(r1)
        L_0x0099:
            int r3 = r3 + 1
            goto L_0x0029
        L_0x009c:
            r2.length()
            r6.countDown()
            goto L_0x0099
        L_0x00a3:
            r15 = 1
            goto L_0x0027
        L_0x00a5:
            r6.await()     // Catch: InterruptedException -> 0x00a9
            goto L_0x00af
        L_0x00a9:
            r1 = move-exception
            java.lang.String r0 = "gdrive/backup/collect-files-to-be-uploaded/waiting for files to be processed was interrupted"
            com.whatsapp.util.Log.e(r0, r1)
        L_0x00af:
            java.lang.Object r1 = r5.get()
            java.lang.Throwable r1 = (java.lang.Throwable) r1
            if (r1 == 0) goto L_0x00c2
            boolean r0 = r1 instanceof X.AnonymousClass2AP
            if (r0 == 0) goto L_0x00bc
            throw r1
        L_0x00bc:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>(r1)
            throw r0
        L_0x00c2:
            boolean r0 = r4.get()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C64473Fr.A04(java.util.List, java.util.List, java.util.List):boolean");
    }

    public final boolean A05(AtomicReference atomicReference, int i) {
        if (!this.A0I.A0c.get()) {
            Log.i("gdrive/backup/cancelled.");
            return true;
        }
        Throwable th = (Throwable) atomicReference.get();
        if (th != null) {
            Log.i("gdrive/backup", th);
            if (th instanceof C84073yN) {
                throw th;
            } else if (th instanceof C84183yY) {
                throw th;
            } else if (th instanceof C84103yQ) {
                throw th;
            } else if (th instanceof C84083yO) {
                throw th;
            } else if (th instanceof C84113yR) {
                throw th;
            } else if (th instanceof AnonymousClass2AP) {
                throw th;
            } else if (th instanceof C84093yP) {
                throw th;
            } else if (th instanceof C84063yM) {
                throw th;
            }
        }
        AtomicLong atomicLong = this.A0i;
        if (atomicLong.get() > 0) {
            AtomicLong atomicLong2 = this.A0j;
            if (C12970iu.A00(atomicLong2, atomicLong) > 1.0d) {
                StringBuilder A0j = C12960it.A0j("gdrive/backup/too-many-failures/");
                A0j.append(C12970iu.A00(atomicLong2, atomicLong));
                Log.i(C12960it.A0d("% bytes", A0j));
                C12990iw.A1M(this.A0b, 38);
                return true;
            }
        }
        if (i <= 0) {
            return false;
        }
        AtomicInteger atomicInteger = this.A0g;
        double d = (double) i;
        if ((((double) atomicInteger.get()) * 100.0d) / d <= 1.0d) {
            return false;
        }
        StringBuilder A0j2 = C12960it.A0j("gdrive/backup/too-many-failures/");
        A0j2.append((((double) atomicInteger.get()) * 100.0d) / d);
        Log.i(C12960it.A0d("% files", A0j2));
        C12990iw.A1M(this.A0b, 38);
        return true;
    }
}
