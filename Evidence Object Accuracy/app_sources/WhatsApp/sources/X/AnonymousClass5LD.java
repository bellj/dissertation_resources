package X;

/* renamed from: X.5LD  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass5LD extends C94524by {
    public AnonymousClass4VA A0A() {
        Object obj;
        Object obj2;
        C114215Kq r3 = ((AnonymousClass5L7) this).A00;
        Boolean bool = Boolean.TRUE;
        do {
            obj = r3._state;
            if (!(obj instanceof AbstractC115535Rx)) {
                return null;
            }
            AbstractC115535Rx r9 = (AbstractC115535Rx) obj;
            int i = ((AnonymousClass5LK) r3).A00;
            obj2 = bool;
            if (!(bool instanceof C94894ci) && ((i == 1 || i == 2) && (r9 instanceof AbstractC114125Kh) && !(r9 instanceof AbstractC114115Kg))) {
                obj2 = new C92844Xq(bool, null, null, null, (AbstractC114125Kh) r9);
            }
        } while (!AnonymousClass0KN.A00(r3, obj, obj2, C114215Kq.A04));
        r3.A05();
        return AnonymousClass4GY.A00;
    }
}
