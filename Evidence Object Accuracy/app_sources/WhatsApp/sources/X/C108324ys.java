package X;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.util.Log;
import com.facebook.redex.RunnableBRunnable0Shape14S0100000_I1;
import com.google.android.gms.common.api.internal.RunnablezaavShape12S0200000_I1;
import com.google.android.gms.common.internal.IAccountAccessor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Lock;

/* renamed from: X.4ys  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C108324ys implements AnonymousClass5XN {
    public int A00;
    public int A01 = 0;
    public int A02;
    public C56492ky A03;
    public IAccountAccessor A04;
    public AnonymousClass5Yh A05;
    public boolean A06;
    public boolean A07;
    public boolean A08;
    public boolean A09;
    public boolean A0A;
    public final Context A0B;
    public final Bundle A0C = C12970iu.A0D();
    public final C471929k A0D;
    public final AbstractC77683ng A0E;
    public final C108364yw A0F;
    public final AnonymousClass3BW A0G;
    public final ArrayList A0H = C12960it.A0l();
    public final Map A0I;
    public final Set A0J = C12970iu.A12();
    public final Lock A0K;

    @Override // X.AnonymousClass5XN
    public final void AgW() {
    }

    public C108324ys(Context context, C471929k r3, AbstractC77683ng r4, C108364yw r5, AnonymousClass3BW r6, Map map, Lock lock) {
        this.A0F = r5;
        this.A0G = r6;
        this.A0I = map;
        this.A0D = r3;
        this.A0E = r4;
        this.A0K = lock;
        this.A0B = context;
    }

    public final void A00() {
        this.A07 = false;
        C108364yw r6 = this.A0F;
        r6.A05.A04 = Collections.emptySet();
        for (Object obj : this.A0J) {
            Map map = r6.A0A;
            if (!map.containsKey(obj)) {
                map.put(obj, new C56492ky(17, null));
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public final void A01() {
        IBinder iBinder;
        C108364yw r3 = this.A0F;
        Lock lock = r3.A0D;
        lock.lock();
        try {
            r3.A05.A0E();
            r3.A0E = new C108304yq(r3);
            r3.A0E.AgT();
            r3.A0C.signalAll();
            lock.unlock();
            C63283Az.A00.execute(new RunnableBRunnable0Shape14S0100000_I1(this, 5));
            AnonymousClass5Yh r1 = this.A05;
            if (r1 != null) {
                if (this.A09) {
                    IAccountAccessor iAccountAccessor = this.A04;
                    C13020j0.A01(iAccountAccessor);
                    boolean z = this.A0A;
                    C77953o8 r12 = (C77953o8) r1;
                    try {
                        C98384ib r4 = (C98384ib) r12.A03();
                        Integer num = r12.A02;
                        C13020j0.A01(num);
                        int intValue = num.intValue();
                        Parcel obtain = Parcel.obtain();
                        obtain.writeInterfaceToken(r4.A01);
                        if (iAccountAccessor == null) {
                            iBinder = null;
                        } else {
                            iBinder = iAccountAccessor.asBinder();
                        }
                        obtain.writeStrongBinder(iBinder);
                        obtain.writeInt(intValue);
                        obtain.writeInt(z ? 1 : 0);
                        r4.A00(9, obtain);
                    } catch (RemoteException unused) {
                        Log.w("SignInClientImpl", "Remote service probably died when saveDefaultAccount is called");
                    }
                }
                A05(false);
            }
            Iterator A10 = C72453ed.A10(r3.A0A);
            while (A10.hasNext()) {
                Object obj = r3.A09.get(A10.next());
                C13020j0.A01(obj);
                ((AbstractC72443eb) obj).A8y();
            }
            Bundle bundle = this.A0C;
            if (bundle.isEmpty()) {
                bundle = null;
            }
            r3.A07.AgP(bundle);
        } catch (Throwable th) {
            lock.unlock();
            throw th;
        }
    }

    public final void A02() {
        if (this.A02 != 0) {
            return;
        }
        if (!this.A07 || this.A08) {
            ArrayList A0l = C12960it.A0l();
            this.A01 = 1;
            C108364yw r4 = this.A0F;
            Map map = r4.A09;
            this.A02 = map.size();
            Iterator A10 = C72453ed.A10(map);
            while (A10.hasNext()) {
                Object next = A10.next();
                if (!r4.A0A.containsKey(next)) {
                    A0l.add(map.get(next));
                } else if (A06()) {
                    A01();
                }
            }
            if (!A0l.isEmpty()) {
                this.A0H.add(C63283Az.A00.submit(new RunnablezaavShape12S0200000_I1(this, A0l)));
            }
        }
    }

    public final void A03(C56492ky r6) {
        ArrayList arrayList = this.A0H;
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            ((Future) arrayList.get(i)).cancel(true);
        }
        arrayList.clear();
        A05(!r6.A00());
        C108364yw r0 = this.A0F;
        r0.A00(r6);
        r0.A07.AgN(r6);
    }

    public final void A04(C56492ky r5, AnonymousClass1UE r6, boolean z) {
        if ((!z || r5.A00() || this.A0D.A01(null, null, r5.A01) != null) && (this.A03 == null || Integer.MAX_VALUE < this.A00)) {
            this.A03 = r5;
            this.A00 = Integer.MAX_VALUE;
        }
        this.A0F.A0A.put(r6.A01, r5);
    }

    public final void A05(boolean z) {
        AnonymousClass5Yh r4 = this.A05;
        if (r4 != null) {
            if (r4.isConnected() && z) {
                C77953o8 r0 = (C77953o8) r4;
                try {
                    C98384ib r3 = (C98384ib) r0.A03();
                    Integer num = r0.A02;
                    C13020j0.A01(num);
                    int intValue = num.intValue();
                    Parcel obtain = Parcel.obtain();
                    obtain.writeInterfaceToken(r3.A01);
                    obtain.writeInt(intValue);
                    r3.A00(7, obtain);
                } catch (RemoteException unused) {
                    Log.w("SignInClientImpl", "Remote service probably died when clearAccountFromSessionStore is called");
                }
            }
            r4.A8y();
            C13020j0.A01(this.A0G);
            this.A04 = null;
        }
    }

    public final boolean A06() {
        C56492ky r2;
        int i = this.A02 - 1;
        this.A02 = i;
        if (i <= 0) {
            if (i < 0) {
                Log.w("GACConnecting", this.A0F.A05.A0C());
                Log.wtf("GACConnecting", "GoogleApiClient received too many callbacks for the given step. Clients may be in an unexpected state; GoogleApiClient will now disconnect.", new Exception());
                r2 = new C56492ky(8, null);
            } else {
                r2 = this.A03;
                if (r2 == null) {
                    return true;
                }
                this.A0F.A00 = this.A00;
            }
            A03(r2);
        }
        return false;
    }

    public final boolean A07(int i) {
        String str;
        String str2;
        if (this.A01 == i) {
            return true;
        }
        Log.w("GACConnecting", this.A0F.A05.A0C());
        Log.w("GACConnecting", "Unexpected callback in ".concat(toString()));
        Log.w("GACConnecting", C12960it.A0e("mRemainingConnections=", C12980iv.A0t(33), this.A02));
        if (this.A01 != 0) {
            str = "STEP_GETTING_REMOTE_SERVICE";
        } else {
            str = "STEP_SERVICE_BINDINGS_AND_SIGN_IN";
        }
        if (i != 0) {
            str2 = "STEP_GETTING_REMOTE_SERVICE";
        } else {
            str2 = "STEP_SERVICE_BINDINGS_AND_SIGN_IN";
        }
        StringBuilder A0t = C12980iv.A0t(str.length() + 70 + str2.length());
        A0t.append("GoogleApiClient connecting is in step ");
        A0t.append(str);
        A0t.append(" but received callback for step ");
        A0t.append(str2);
        Log.e("GACConnecting", A0t.toString(), new Exception());
        A03(new C56492ky(8, null));
        return false;
    }

    @Override // X.AnonymousClass5XN
    public final AnonymousClass1UI AgM(AnonymousClass1UI r2) {
        this.A0F.A05.A0J.add(r2);
        return r2;
    }

    @Override // X.AnonymousClass5XN
    public final AnonymousClass1UI AgO(AnonymousClass1UI r2) {
        throw C12960it.A0U("GoogleApiClient is not connected yet.");
    }

    /* JADX WARN: Type inference failed for: r0v10, types: [X.5Yh, X.3eb] */
    @Override // X.AnonymousClass5XN
    public final void AgT() {
        C108364yw r2 = this.A0F;
        r2.A0A.clear();
        this.A07 = false;
        this.A03 = null;
        this.A01 = 0;
        this.A06 = true;
        this.A08 = false;
        this.A09 = false;
        HashMap A11 = C12970iu.A11();
        Map map = this.A0I;
        Iterator A10 = C72453ed.A10(map);
        while (A10.hasNext()) {
            AnonymousClass1UE r6 = (AnonymousClass1UE) A10.next();
            Map map2 = r2.A09;
            AnonymousClass4DN r5 = r6.A01;
            Object obj = map2.get(r5);
            C13020j0.A01(obj);
            AbstractC72443eb r4 = (AbstractC72443eb) obj;
            boolean A1Y = C12970iu.A1Y(map.get(r6));
            if (r4.Aae()) {
                this.A07 = true;
                if (A1Y) {
                    this.A0J.add(r5);
                } else {
                    this.A06 = false;
                }
            }
            A11.put(r4, new C108404z0(r6, this, A1Y));
        }
        if (this.A07) {
            AnonymousClass3BW r9 = this.A0G;
            C13020j0.A01(r9);
            AbstractC77683ng r42 = this.A0E;
            C13020j0.A01(r42);
            C77733nl r1 = r2.A05;
            r9.A00 = Integer.valueOf(System.identityHashCode(r1));
            C108204yg r7 = new C108204yg(this);
            this.A05 = r42.A00(this.A0B, r1.A07, r7, r7, r9, r9.A01);
        }
        this.A02 = r2.A09.size();
        this.A0H.add(C63283Az.A00.submit(new RunnablezaavShape12S0200000_I1(this, A11)));
    }

    @Override // X.AnonymousClass5XN
    public final void AgZ(Bundle bundle) {
        if (A07(1)) {
            if (bundle != null) {
                this.A0C.putAll(bundle);
            }
            if (A06()) {
                A01();
            }
        }
    }

    @Override // X.AnonymousClass5XN
    public final void Aga(C56492ky r2, AnonymousClass1UE r3, boolean z) {
        if (A07(1)) {
            A04(r2, r3, z);
            if (A06()) {
                A01();
            }
        }
    }

    @Override // X.AnonymousClass5XN
    public final void Agb(int i) {
        A03(new C56492ky(8, null));
    }

    @Override // X.AnonymousClass5XN
    public final boolean Agc() {
        ArrayList arrayList = this.A0H;
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            ((Future) arrayList.get(i)).cancel(true);
        }
        arrayList.clear();
        A05(true);
        this.A0F.A00(null);
        return true;
    }
}
