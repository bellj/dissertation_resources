package X;

import java.io.IOException;
import java.util.Enumeration;
import java.util.NoSuchElementException;

/* renamed from: X.5D4  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5D4 implements Enumeration {
    public Object A00;
    public AnonymousClass1TO A01;

    public AnonymousClass5D4(byte[] bArr) {
        AnonymousClass1TO r0 = new AnonymousClass1TO(bArr, true);
        this.A01 = r0;
        try {
            this.A00 = r0.A05();
        } catch (IOException e) {
            throw new AnonymousClass4CU(C12960it.A0b("malformed DER construction: ", e), e);
        }
    }

    @Override // java.util.Enumeration
    public boolean hasMoreElements() {
        return C12960it.A1W(this.A00);
    }

    @Override // java.util.Enumeration
    public Object nextElement() {
        Object obj = this.A00;
        if (obj != null) {
            try {
                this.A00 = this.A01.A05();
                return obj;
            } catch (IOException e) {
                throw new AnonymousClass4CU(C12960it.A0b("malformed DER construction: ", e), e);
            }
        } else {
            throw new NoSuchElementException();
        }
    }
}
