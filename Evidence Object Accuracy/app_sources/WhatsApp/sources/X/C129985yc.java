package X;

import android.content.Context;
import android.view.GestureDetector;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;

/* renamed from: X.5yc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129985yc {
    public float A00 = -1.0f;
    public float A01 = -1.0f;
    public float A02 = 0.0f;
    public float A03 = 0.0f;
    public int A04;
    public int A05 = 0;
    public int A06;
    public int A07;
    public int A08;
    public Context A09;
    public GestureDetector A0A;
    public VelocityTracker A0B = null;
    public EnumC124515pf A0C = EnumC124515pf.VERTICAL;
    public AnonymousClass6LS A0D = null;
    public AnonymousClass6Ll A0E = null;
    public EnumC124555pj A0F = EnumC124555pj.AT_REST;
    public AnonymousClass6Lm A0G = null;
    public EnumC124585pm A0H = null;
    public boolean A0I;

    public C129985yc(Context context) {
        this.A0A = new GestureDetector(context, new AnonymousClass642(this));
        this.A09 = context;
    }

    public void A00() {
        this.A00 = -1.0f;
        this.A01 = -1.0f;
        this.A0F = EnumC124555pj.CANCELED;
        VelocityTracker velocityTracker = this.A0B;
        this.A0B = null;
        if (velocityTracker != null) {
            velocityTracker.recycle();
        }
    }

    public final void A01() {
        if (!this.A0I) {
            Context context = this.A09;
            if (context != null) {
                ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
                int scaledPagingTouchSlop = viewConfiguration.getScaledPagingTouchSlop();
                int scaledTouchSlop = viewConfiguration.getScaledTouchSlop();
                int scaledMinimumFlingVelocity = viewConfiguration.getScaledMinimumFlingVelocity();
                int scaledMaximumFlingVelocity = viewConfiguration.getScaledMaximumFlingVelocity();
                this.A04 = scaledPagingTouchSlop;
                this.A08 = scaledTouchSlop;
                this.A07 = scaledMinimumFlingVelocity;
                this.A06 = scaledMaximumFlingVelocity;
                this.A0I = true;
                this.A0I = true;
                this.A09 = null;
                return;
            }
            throw C12960it.A0U("Init Context must not be null");
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001e, code lost:
        if (r1 != 3) goto L_0x0020;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0081, code lost:
        if (r0 != r2) goto L_0x013a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0138, code lost:
        if (r1 != r0) goto L_0x013a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x013a, code lost:
        r12.A0E.APU(r5, r4, r3);
     */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0077  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A02(android.view.MotionEvent r13) {
        /*
        // Method dump skipped, instructions count: 321
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C129985yc.A02(android.view.MotionEvent):boolean");
    }
}
