package X;

import android.database.Cursor;
import android.util.Base64;
import com.facebook.simplejni.NativeHolder;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import com.whatsapp.wamsys.JniBridge;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/* renamed from: X.1Iw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C27711Iw extends AnonymousClass1Iv implements AbstractC27681Ir, AbstractC16420oz {
    public long A00;
    public List A01;
    public byte[] A02;
    public byte[] A03;
    public byte[] A04;
    public final List A05;

    public C27711Iw(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 67, j);
        this.A05 = new ArrayList();
    }

    public C27711Iw(AnonymousClass1IS r3, C40711sC r4, List list, long j, long j2, long j3) {
        super(r3, (byte) 67, j);
        ((AnonymousClass1Iv) this).A02 = r4;
        ((AnonymousClass1Iv) this).A01 = null;
        ((AnonymousClass1Iv) this).A00 = j2;
        ArrayList arrayList = new ArrayList();
        this.A05 = arrayList;
        arrayList.addAll(list);
        this.A00 = j3;
    }

    public C27711Iw(AnonymousClass2RK r6, AnonymousClass1IS r7, long j) {
        super(r7, (byte) 67, j);
        this.A05 = new ArrayList();
        if ((r6.A00 & 1) != 1 ? false : true) {
            AnonymousClass1G8 r3 = r6.A04;
            r3 = r3 == null ? AnonymousClass1G8.A05 : r3;
            AbstractC14640lm A01 = AbstractC14640lm.A01(r3.A03);
            AnonymousClass009.A05(A01);
            AnonymousClass1IS r32 = new AnonymousClass1IS(A01, r3.A01, r3.A04);
            AnonymousClass1G8 r0 = r6.A04;
            ((AnonymousClass1Iv) this).A02 = new C40711sC(UserJid.getNullable((r0 == null ? AnonymousClass1G8.A05 : r0).A02), r32);
            int i = r6.A00;
            if ((i & 8) == 8) {
                this.A00 = r6.A01;
                if ((i & 2) == 2) {
                    C57222mg r4 = r6.A02;
                    r4 = r4 == null ? C57222mg.A03 : r4;
                    if ((r4.A00 & 2) == 2) {
                        byte[] A04 = r4.A01.A04();
                        if (A04.length == 12) {
                            this.A02 = A04;
                            if ((r4.A00 & 1) == 1) {
                                this.A03 = r4.A02.A04();
                                return;
                            }
                            throw new C43271wi("poll_update_missing_vote_enc_payload", 34);
                        }
                        throw new C43271wi("poll_update_invalid_vote_enc_iv", 34);
                    }
                    throw new C43271wi("poll_update_missing_vote_enc_iv", 34);
                }
                throw new C43271wi("poll_update_missing_update", 34);
            }
            throw new C43271wi("poll_update_missing_sender_timestamp", 34);
        }
        throw new C43271wi("poll_update_missing_poll_message_key", 34);
    }

    @Override // X.AnonymousClass1Iv
    public void A15(Cursor cursor, C18460sU r4, HashMap hashMap) {
        super.A15(cursor, r4, hashMap);
        this.A00 = cursor.getLong(AnonymousClass1Tx.A00("sender_timestamp", hashMap));
    }

    @Override // X.AbstractC16420oz
    public void A6k(C39971qq r19) {
        AbstractC14640lm r8;
        Jid of;
        UserJid A0C;
        String str;
        int length;
        int length2;
        AbstractC27881Jp A01;
        byte[] bArr;
        JniBridge jniBridge = r19.A05;
        AnonymousClass009.A05(jniBridge);
        AnonymousClass1G3 r7 = r19.A03;
        AnonymousClass2RK r0 = ((C27081Fy) r7.A00).A0U;
        if (r0 == null) {
            r0 = AnonymousClass2RK.A05;
        }
        AnonymousClass1G4 A0T = r0.A0T();
        AnonymousClass1G8 r02 = ((AnonymousClass2RK) A0T.A00).A04;
        if (r02 == null) {
            r02 = AnonymousClass1G8.A05;
        }
        AnonymousClass1G9 r2 = (AnonymousClass1G9) r02.A0T();
        AnonymousClass1IS A14 = A14();
        C40711sC r03 = ((AnonymousClass1Iv) this).A02;
        if (r03 == null) {
            r8 = null;
        } else {
            r8 = r03.A00;
        }
        AnonymousClass009.A05(A14);
        C40721sD.A02(r8, r2, A14);
        A0T.A03();
        AnonymousClass2RK r1 = (AnonymousClass2RK) A0T.A00;
        r1.A04 = (AnonymousClass1G8) r2.A02();
        r1.A00 |= 1;
        long j = this.A00;
        A0T.A03();
        AnonymousClass2RK r4 = (AnonymousClass2RK) A0T.A00;
        r4.A00 |= 8;
        r4.A01 = j;
        C57222mg r04 = r4.A02;
        if (r04 == null) {
            r04 = C57222mg.A03;
        }
        AnonymousClass1G4 A0T2 = r04.A0T();
        byte[] bArr2 = this.A02;
        if (bArr2 == null || (bArr = this.A03) == null) {
            List<String> list = this.A01;
            AnonymousClass009.A05(list);
            byte[] bArr3 = this.A04;
            AnonymousClass009.A05(bArr3);
            String str2 = A14.A01;
            if (A14.A02) {
                C15570nT r05 = r19.A00;
                r05.A08();
                of = r05.A05;
            } else {
                of = UserJid.of(r8);
            }
            AnonymousClass009.A05(of);
            if (this.A0z.A02) {
                C15570nT r06 = r19.A00;
                r06.A08();
                A0C = r06.A05;
            } else {
                A0C = A0C();
            }
            AnonymousClass009.A05(A0C);
            ArrayList arrayList = new ArrayList();
            for (String str3 : list) {
                arrayList.add(Base64.decode(str3, 2));
            }
            NativeHolder nativeHolder = (NativeHolder) JniBridge.jvidispatchOOOOOOO(2, str2, of.getRawString(), A0C.getRawString(), jniBridge.wajContext.get(), new C40741sF((NativeHolder) JniBridge.jvidispatchOO(4, arrayList)).A00, new C40731sE((NativeHolder) JniBridge.jvidispatchOO(5, bArr3)).A00);
            if (nativeHolder != null) {
                AnonymousClass4I8 r07 = new AnonymousClass4I8(nativeHolder);
                JniBridge.getInstance();
                NativeHolder nativeHolder2 = r07.A00;
                byte[] bArr4 = (byte[]) JniBridge.jvidispatchOIO(0, (long) 62, nativeHolder2);
                JniBridge.getInstance();
                byte[] bArr5 = (byte[]) JniBridge.jvidispatchOIO(0, (long) 61, nativeHolder2);
                if (bArr4 == null || (length = bArr4.length) == 0 || bArr5 == null || (length2 = bArr5.length) == 0) {
                    str = "MessageAddOnPollVoteUtils/encryptPollVotePayload encryption values are invalid";
                } else {
                    AbstractC27881Jp A012 = AbstractC27881Jp.A01(bArr4, 0, length);
                    A0T2.A03();
                    C57222mg r12 = (C57222mg) A0T2.A00;
                    r12.A00 |= 2;
                    r12.A01 = A012;
                    A01 = AbstractC27881Jp.A01(bArr5, 0, length2);
                }
            } else {
                str = "MessageAddOnPollVoteUtils/encryptPollVotePayload encryptionResult is null";
            }
            Log.e(str);
            return;
        }
        AbstractC27881Jp A013 = AbstractC27881Jp.A01(bArr2, 0, bArr2.length);
        A0T2.A03();
        C57222mg r13 = (C57222mg) A0T2.A00;
        r13.A00 |= 2;
        r13.A01 = A013;
        A01 = AbstractC27881Jp.A01(bArr, 0, bArr.length);
        A0T2.A03();
        C57222mg r14 = (C57222mg) A0T2.A00;
        r14.A00 |= 1;
        r14.A02 = A01;
        A0T.A03();
        AnonymousClass2RK r15 = (AnonymousClass2RK) A0T.A00;
        r15.A02 = (C57222mg) A0T2.A02();
        r15.A00 |= 2;
        r7.A03();
        C27081Fy r16 = (C27081Fy) r7.A00;
        r16.A0U = (AnonymousClass2RK) A0T.A02();
        r16.A01 |= 128;
    }

    @Override // X.AbstractC27681Ir
    public List AGt() {
        return Collections.singletonList(new AnonymousClass1V8("meta", new AnonymousClass1W9[]{new AnonymousClass1W9("polltype", "vote")}));
    }
}
