package X;

import java.util.List;
import java.util.Locale;

/* renamed from: X.0PU  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0PU {
    public final float A00;
    public final float A01;
    public final int A02;
    public final int A03;
    public final int A04;
    public final int A05;
    public final int A06;
    public final long A07;
    public final long A08;
    public final C05540Py A09;
    public final AnonymousClass0H9 A0A;
    public final AnonymousClass0H8 A0B;
    public final C04850Nh A0C;
    public final C08170ah A0D;
    public final AnonymousClass0JI A0E;
    public final AnonymousClass0J6 A0F;
    public final String A0G;
    public final String A0H;
    public final List A0I;
    public final List A0J;
    public final List A0K;
    public final boolean A0L;

    public AnonymousClass0PU(C05540Py r3, AnonymousClass0H9 r4, AnonymousClass0H8 r5, C04850Nh r6, C08170ah r7, AnonymousClass0JI r8, AnonymousClass0J6 r9, String str, String str2, List list, List list2, List list3, float f, float f2, int i, int i2, int i3, int i4, int i5, long j, long j2, boolean z) {
        this.A0K = list;
        this.A09 = r3;
        this.A0G = str;
        this.A07 = j;
        this.A0E = r8;
        this.A08 = j2;
        this.A0H = str2;
        this.A0J = list2;
        this.A0D = r7;
        this.A06 = i;
        this.A05 = i2;
        this.A04 = i3;
        this.A01 = f;
        this.A00 = f2;
        this.A03 = i4;
        this.A02 = i5;
        this.A0B = r5;
        this.A0C = r6;
        this.A0I = list3;
        this.A0F = r9;
        this.A0A = r4;
        this.A0L = z;
    }

    public String A00(String str) {
        int i;
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(this.A0G);
        sb.append("\n");
        C05540Py r2 = this.A09;
        long j = this.A08;
        AnonymousClass036 r5 = r2.A05;
        AnonymousClass0PU r1 = (AnonymousClass0PU) r5.A04(j, null);
        if (r1 != null) {
            String str2 = "\t\tParents: ";
            while (true) {
                sb.append(str2);
                sb.append(r1.A0G);
                r1 = (AnonymousClass0PU) r5.A04(r1.A08, null);
                if (r1 == null) {
                    break;
                }
                str2 = "->";
            }
            sb.append(str);
            sb.append("\n");
        }
        List list = this.A0J;
        if (!list.isEmpty()) {
            sb.append(str);
            sb.append("\tMasks: ");
            sb.append(list.size());
            sb.append("\n");
        }
        int i2 = this.A06;
        if (!(i2 == 0 || (i = this.A05) == 0)) {
            sb.append(str);
            sb.append("\tBackground: ");
            sb.append(String.format(Locale.US, "%dx%d %X\n", Integer.valueOf(i2), Integer.valueOf(i), Integer.valueOf(this.A04)));
        }
        List list2 = this.A0K;
        if (!list2.isEmpty()) {
            sb.append(str);
            sb.append("\tShapes:\n");
            for (Object obj : list2) {
                sb.append(str);
                sb.append("\t\t");
                sb.append(obj);
                sb.append("\n");
            }
        }
        return sb.toString();
    }

    public String toString() {
        return A00("");
    }
}
