package X;

import android.view.View;

/* renamed from: X.4QW  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4QW {
    public View A00;
    public final AnonymousClass11P A01;
    public final AnonymousClass01H A02;

    public AnonymousClass4QW(View view, AnonymousClass11P r2, AnonymousClass01H r3) {
        this.A00 = view;
        this.A01 = r2;
        this.A02 = r3;
    }
}
