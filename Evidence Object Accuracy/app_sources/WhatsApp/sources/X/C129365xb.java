package X;

import android.content.Context;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.5xb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129365xb {
    public final Context A00;
    public final C14900mE A01;
    public final C18640sm A02;
    public final C18650sn A03;
    public final C18610sj A04;
    public final C129135xE A05;
    public final C129215xM A06;
    public final AnonymousClass60T A07;
    public final C130015yf A08;
    public final C130775zx A09;
    public final C18590sh A0A;
    public final String A0B;
    public final String A0C;
    public final String A0D;
    public final List A0E;

    public C129365xb(Context context, C14900mE r14, C15570nT r15, C18640sm r16, C14830m7 r17, C18650sn r18, C18610sj r19, C129135xE r20, AnonymousClass60T r21, C130015yf r22, C18590sh r23, String str, String str2, String str3, List list) {
        AnonymousClass009.A0E((TextUtils.isEmpty(str2) ^ true) != (TextUtils.isEmpty(str3) ^ true) ? false : true);
        this.A00 = context;
        this.A01 = r14;
        this.A05 = r20;
        this.A0A = r23;
        this.A08 = r22;
        this.A04 = r19;
        this.A02 = r16;
        this.A03 = r18;
        this.A07 = r21;
        this.A09 = new C130775zx(r15, r17, r19);
        this.A06 = new C129215xM(context, r14, r18, r19, r21, "PIN");
        this.A0B = str;
        this.A0E = list;
        this.A0C = str2;
        this.A0D = str3;
    }

    public final void A00(C128545wH r13, C128925wt r14, String str) {
        C129135xE r2 = this.A05;
        List list = this.A0E;
        C128995x0 r6 = new C128995x0(new C1331169n(this, r13, r14));
        if ("token".equals(r13.A00.A03)) {
            AbstractC14440lR r1 = r2.A05;
            AnonymousClass1BY r8 = r2.A03;
            C18600si r4 = r2.A01;
            C22120yY r9 = r2.A04;
            C18610sj r5 = r2.A02;
            C18640sm r3 = r2.A00;
            C12990iw.A1N(new C120305fx(r3, r4, r5, r6, null, r8, r9, list, 0), r1);
            ArrayList A0l = C12960it.A0l();
            C12970iu.A1T("fbpay_pin", str, A0l);
            C12990iw.A1N(new C120305fx(r3, r4, r5, r6, null, r8, r9, A0l, 1), r1);
            return;
        }
        r6.A00(1, str);
        AbstractC14440lR r0 = r2.A05;
        AnonymousClass1BY r82 = r2.A03;
        C18600si r42 = r2.A01;
        C22120yY r92 = r2.A04;
        C12990iw.A1N(new C120305fx(r2.A00, r42, r2.A02, null, new C1330269e(r6, r2), r82, r92, list, -1), r0);
    }
}
