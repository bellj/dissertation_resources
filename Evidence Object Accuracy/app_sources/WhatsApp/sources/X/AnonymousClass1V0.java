package X;

import com.whatsapp.util.Log;

/* renamed from: X.1V0  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1V0 {
    public final int A00;
    public final long A01;

    public AnonymousClass1V0(int i, long j) {
        this.A00 = i;
        if (i != 0 || j == 0) {
            this.A01 = j;
        } else {
            Log.e("GrowthLock Nonzero expiration for unlocked GrowthLock");
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            AnonymousClass1V0 r7 = (AnonymousClass1V0) obj;
            if (this.A00 == r7.A00 && this.A01 == r7.A01) {
                return true;
            }
            return false;
        }
        return false;
    }

    public int hashCode() {
        long j = this.A01;
        return (this.A00 * 31) + ((int) (j ^ (j >>> 32)));
    }
}
