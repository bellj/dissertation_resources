package X;

import android.content.Context;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;

/* renamed from: X.2yM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60642yM extends C60812yg {
    public boolean A00;

    public C60642yM(Context context, AbstractC13890kV r2, C30321Wy r3) {
        super(context, r2, r3);
        A0Z();
    }

    @Override // X.AnonymousClass2xj, X.AnonymousClass1OZ, X.AbstractC28561Ob
    public void A0Z() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2P6 A07 = AnonymousClass1OY.A07(this);
            AnonymousClass01J A08 = AnonymousClass1OY.A08(A07, this);
            AnonymousClass1OY.A0L(A08, this);
            AnonymousClass1OY.A0M(A08, this);
            AnonymousClass1OY.A0K(A08, this);
            AnonymousClass1OY.A0I(A07, A08, this, AnonymousClass1OY.A09(A08, this, AnonymousClass1OY.A0B(A08, this)));
        }
    }

    @Override // X.C60812yg
    public String getMessageString() {
        UserJid userJid = ((C30321Wy) getFMessage()).A00;
        AnonymousClass009.A05(userJid);
        if (((AnonymousClass1OY) this).A0L.A0F(userJid)) {
            return getContext().getString(R.string.admin_revoked_msg_by_you);
        }
        return C12960it.A0X(getContext(), ((AnonymousClass1OY) this).A0Z.A0F(AnonymousClass29B.newArrayList(userJid), -1), new Object[1], 0, R.string.admin_revoked_msg_by_name);
    }
}
