package X;

import android.content.Context;
import com.whatsapp.util.Log;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5fn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120205fn extends AbstractC451020e {
    public final /* synthetic */ C128115va A00;
    public final /* synthetic */ C126275sc A01;
    public final /* synthetic */ HashMap A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120205fn(Context context, C14900mE r2, C18650sn r3, C128115va r4, C126275sc r5, HashMap hashMap) {
        super(context, r2, r3);
        this.A00 = r4;
        this.A02 = hashMap;
        this.A01 = r5;
    }

    @Override // X.AbstractC451020e
    public void A02(C452120p r4) {
        Log.e(C12960it.A0b("PAY: BrazilMerchantGetStatusAction request error: ", r4));
        HashMap hashMap = this.A02;
        C117295Zj.A1D(r4, hashMap);
        this.A00.A05.A00.A01("on_failure", hashMap);
    }

    @Override // X.AbstractC451020e
    public void A03(C452120p r4) {
        Log.e(C12960it.A0b("PAY: BrazilMerchantGetStatusAction response error: ", r4));
        HashMap hashMap = this.A02;
        C117295Zj.A1D(r4, hashMap);
        this.A00.A05.A00.A01("on_failure", hashMap);
    }

    @Override // X.AbstractC451020e
    public void A04(AnonymousClass1V8 r14) {
        AnonymousClass1V8 A0c = C117305Zk.A0c(r14);
        if (A0c != null) {
            C452120p A00 = C452120p.A00(A0c);
            if (A00 != null) {
                HashMap hashMap = this.A02;
                hashMap.put("error_code", String.valueOf(A00.A00));
                this.A00.A05.A00.A01("on_success", hashMap);
                return;
            }
            int A002 = C28421Nd.A00(C117295Zj.A0W(A0c, "status"), 0);
            HashMap hashMap2 = this.A02;
            hashMap2.put("status", String.valueOf(A002));
            try {
                if (A002 == 1) {
                    C126275sc r1 = this.A01;
                    C128115va r6 = this.A00;
                    AnonymousClass60F r9 = new AnonymousClass60F(r6.A01, r14, r1);
                    Log.i("PAY: BrazilVerifyTaxId: Linked account, try to link merchant");
                    hashMap2.put("verify_type", r9.A08);
                    hashMap2.put("verify_id", String.valueOf(r9.A00));
                    hashMap2.put("bank_code", String.valueOf(r9.A03));
                    hashMap2.put("bank_name", r9.A04);
                    hashMap2.put("masked_account_number", r9.A06);
                    hashMap2.put("last4", r9.A05);
                    hashMap2.put("support_phone_number", r9.A07);
                    r6.A05.A00.A01("on_success", hashMap2);
                    return;
                }
                C126275sc r92 = this.A01;
                C128115va r62 = this.A00;
                List<AnonymousClass60E> list = new C129605xz(r62.A01, r14, r92).A02;
                try {
                    JSONArray A0L = C117315Zl.A0L();
                    for (AnonymousClass60E r11 : list) {
                        JSONObject A0a = C117295Zj.A0a();
                        A0a.put("bank_code", r11.A00);
                        A0a.put("bank_name", r11.A03);
                        A0a.put("short_name", r11.A05);
                        A0a.put("accept_savings", r11.A02);
                        A0L.put(A0a);
                    }
                    hashMap2.put("banks", A0L.toString());
                    r62.A05.A00.A01("on_success", hashMap2);
                } catch (JSONException e) {
                    Log.e(C12960it.A0Z(e, "PAY: BrazilPayBloksActivity payoutBanksToJsonArrayException: ", C12960it.A0h()), e);
                    hashMap2.put("error_code", e.getMessage());
                    r62.A05.A00.A01("on_failure", hashMap2);
                }
            } catch (AnonymousClass1V9 e2) {
                C117305Zk.A1N("GetMerchantStatus", e2.getMessage());
                hashMap2.put("error_code", e2.getMessage());
                this.A00.A05.A00.A01("on_failure", hashMap2);
            }
        }
    }
}
