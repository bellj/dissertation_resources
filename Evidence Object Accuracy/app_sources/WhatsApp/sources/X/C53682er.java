package X;

import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Bundle;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import com.facebook.rendercore.text.RCTextView;
import java.util.List;

/* renamed from: X.2er  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53682er extends AnonymousClass0DW {
    public AnonymousClass04v A00;
    public final /* synthetic */ RCTextView A01;

    @Override // X.AnonymousClass0DW
    public boolean A0G(int i, int i2, Bundle bundle) {
        return false;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C53682er(RCTextView rCTextView) {
        super(rCTextView);
        this.A01 = rCTextView;
        rCTextView.setFocusable(false);
        rCTextView.setImportantForAccessibility(1);
    }

    @Override // X.AnonymousClass04v
    public void A02(View view, AccessibilityEvent accessibilityEvent) {
        super.A02(view, accessibilityEvent);
        RCTextView rCTextView = this.A01;
        if (!TextUtils.isEmpty(rCTextView.A0B)) {
            accessibilityEvent.getText().add(rCTextView.getTextForAccessibility());
        }
    }

    @Override // X.AnonymousClass0DW, X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r5) {
        super.A06(view, r5);
        CharSequence charSequence = ((RCTextView) view).getTextForAccessibility();
        if (!TextUtils.isEmpty(charSequence)) {
            AccessibilityNodeInfo accessibilityNodeInfo = r5.A02;
            accessibilityNodeInfo.setText(charSequence);
            accessibilityNodeInfo.addAction(256);
            accessibilityNodeInfo.addAction(512);
            accessibilityNodeInfo.setMovementGranularities(31);
            accessibilityNodeInfo.addAction(C25981Bo.A0F);
        }
        AnonymousClass04v r0 = this.A00;
        if (r0 != null) {
            r0.A06(view, r5);
        }
    }

    @Override // X.AnonymousClass0DW
    public int A07(float f, float f2) {
        RCTextView rCTextView = this.A01;
        CharSequence charSequence = rCTextView.A0B;
        if (charSequence instanceof Spanned) {
            Spanned spanned = (Spanned) charSequence;
            int i = 0;
            while (true) {
                ClickableSpan[] clickableSpanArr = rCTextView.A0D;
                if (i >= clickableSpanArr.length) {
                    break;
                }
                ClickableSpan clickableSpan = clickableSpanArr[i];
                int spanStart = spanned.getSpanStart(clickableSpan);
                int spanEnd = spanned.getSpanEnd(clickableSpan);
                int A01 = rCTextView.A01((int) f, (int) f2);
                if (A01 >= spanStart && A01 <= spanEnd) {
                    return i;
                }
                i++;
            }
        }
        return Integer.MIN_VALUE;
    }

    @Override // X.AnonymousClass0DW
    public void A0C(AnonymousClass04Z r12, int i) {
        int lineVisibleEnd;
        RCTextView rCTextView = this.A01;
        Spanned spanned = (Spanned) rCTextView.A0B;
        Rect A0J = C12980iv.A0J();
        ClickableSpan[] clickableSpanArr = rCTextView.A0D;
        if (clickableSpanArr == null || i >= clickableSpanArr.length) {
            AccessibilityNodeInfo accessibilityNodeInfo = r12.A02;
            accessibilityNodeInfo.setText("");
            accessibilityNodeInfo.setBoundsInParent(A0J);
            return;
        }
        ClickableSpan clickableSpan = clickableSpanArr[i];
        int spanStart = spanned.getSpanStart(clickableSpan);
        int spanEnd = spanned.getSpanEnd(clickableSpan);
        int lineForOffset = rCTextView.A0A.getLineForOffset(spanStart);
        int lineForOffset2 = rCTextView.A0A.getLineForOffset(spanEnd);
        Path path = new Path();
        RectF A0K = C12980iv.A0K();
        if (lineForOffset == lineForOffset2) {
            lineVisibleEnd = spanEnd;
        } else {
            lineVisibleEnd = rCTextView.A0A.getLineVisibleEnd(lineForOffset);
        }
        rCTextView.A0A.getSelectionPath(spanStart, lineVisibleEnd, path);
        path.computeBounds(A0K, true);
        A0K.offset(rCTextView.A00, rCTextView.A01);
        A0K.round(A0J);
        AccessibilityNodeInfo accessibilityNodeInfo2 = r12.A02;
        accessibilityNodeInfo2.setBoundsInParent(A0J);
        accessibilityNodeInfo2.setClickable(true);
        accessibilityNodeInfo2.setFocusable(true);
        accessibilityNodeInfo2.setEnabled(true);
        accessibilityNodeInfo2.setVisibleToUser(true);
        accessibilityNodeInfo2.setText(spanned.subSequence(spanStart, spanEnd));
        accessibilityNodeInfo2.setClassName("android.widget.Button");
        if (clickableSpan instanceof AbstractC73473gI) {
            AbstractC73473gI r5 = (AbstractC73473gI) clickableSpan;
            String str = r5.A00;
            String str2 = r5.A01;
            if (str != null) {
                accessibilityNodeInfo2.setContentDescription(str);
            }
            AnonymousClass3A3.A00(rCTextView.getContext(), null, r12, str2);
        }
    }

    @Override // X.AnonymousClass0DW
    public void A0D(List list) {
        ClickableSpan[] clickableSpanArr = this.A01.A0D;
        if (clickableSpanArr != null) {
            int length = clickableSpanArr.length;
            for (int i = 0; i < length; i++) {
                list.add(Integer.valueOf(i));
            }
        }
    }
}
