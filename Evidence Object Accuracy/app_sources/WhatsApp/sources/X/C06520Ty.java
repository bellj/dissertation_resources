package X;

import android.view.DisplayCutout;

/* renamed from: X.0Ty  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06520Ty {
    public static int A00(DisplayCutout displayCutout) {
        return displayCutout.getSafeInsetBottom();
    }

    public static int A01(DisplayCutout displayCutout) {
        return displayCutout.getSafeInsetLeft();
    }

    public static int A02(DisplayCutout displayCutout) {
        return displayCutout.getSafeInsetRight();
    }

    public static int A03(DisplayCutout displayCutout) {
        return displayCutout.getSafeInsetTop();
    }
}
