package X;

import android.util.Base64;
import com.whatsapp.Mp4Ops;
import com.whatsapp.util.Log;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* renamed from: X.3Fo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64443Fo {
    public AnonymousClass4QM A00;
    public AnonymousClass3IS A01;
    public Exception A02;
    public String A03;
    public String A04;
    public final AbstractC15710nm A05;
    public final C14330lG A06;
    public final C20870wS A07;
    public final Mp4Ops A08;
    public final C18790t3 A09;
    public final C16590pI A0A;
    public final C14950mJ A0B;
    public final C37891nB A0C;
    public final C14850m9 A0D;
    public final C20110vE A0E;
    public final AnonymousClass12J A0F;
    public final C28921Pn A0G;
    public final C28781Oz A0H;
    public final C41471ta A0I;
    public final C28481Nj A0J;
    public final AnonymousClass2Ba A0K;
    public final C26481Dq A0L;
    public final File A0M;
    public final File A0N;
    public final File A0O;
    public final URL A0P;
    public final byte[] A0Q;
    public final int[] A0R;

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x005d, code lost:
        if (r2 == 2) goto L_0x005f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C64443Fo(X.AbstractC15710nm r6, X.C14330lG r7, X.C20870wS r8, com.whatsapp.Mp4Ops r9, X.C18790t3 r10, X.C16590pI r11, X.C14950mJ r12, X.C14850m9 r13, X.C20110vE r14, X.AnonymousClass12J r15, X.C28921Pn r16, X.C28781Oz r17, X.C41471ta r18, X.C28481Nj r19, X.AnonymousClass2Ba r20, X.C26481Dq r21, java.io.File r22, java.io.File r23, java.io.File r24, java.net.URL r25, byte[] r26, int[] r27) {
        /*
            r5 = this;
            r5.<init>()
            r5.A0D = r13
            r5.A0A = r11
            r5.A08 = r9
            r5.A05 = r6
            r5.A06 = r7
            r5.A09 = r10
            r5.A0B = r12
            r5.A07 = r8
            r5.A0F = r15
            r0 = r21
            r5.A0L = r0
            r0 = r26
            r5.A0Q = r0
            r4 = r18
            r5.A0I = r4
            r3 = r17
            r5.A0H = r3
            r0 = r27
            r5.A0R = r0
            r5.A0E = r14
            X.0lK r0 = r4.A09
            X.2QH r1 = new X.2QH
            r1.<init>(r0)
            byte[] r0 = r4.A0Y
            X.1nB r0 = r1.A8r(r0)
            r5.A0C = r0
            r0 = r16
            r5.A0G = r0
            r0 = r20
            r5.A0K = r0
            r0 = r25
            r5.A0P = r0
            r0 = r22
            r5.A0O = r0
            r0 = r23
            r5.A0N = r0
            r0 = r24
            r5.A0M = r0
            r0 = r19
            r5.A0J = r0
            int r2 = r4.A02
            r0 = 3
            if (r2 == r0) goto L_0x005f
            r1 = 2
            r0 = 0
            if (r2 != r1) goto L_0x0060
        L_0x005f:
            r0 = 1
        L_0x0060:
            X.AnonymousClass009.A0F(r0)
            r0 = 3
            if (r2 != r0) goto L_0x0072
            java.lang.String r1 = r4.A0K
            r0 = 6
            if (r1 != 0) goto L_0x006c
            r0 = 4
        L_0x006c:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r3.A08 = r0
        L_0x0072:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C64443Fo.<init>(X.0nm, X.0lG, X.0wS, com.whatsapp.Mp4Ops, X.0t3, X.0pI, X.0mJ, X.0m9, X.0vE, X.12J, X.1Pn, X.1Oz, X.1ta, X.1Nj, X.2Ba, X.1Dq, java.io.File, java.io.File, java.io.File, java.net.URL, byte[], int[]):void");
    }

    public final AbstractC37631mk A00(URL url, long j, long j2) {
        AbstractC37631mk A00 = this.A0E.A00(this.A0J, url, j, j2);
        if (this.A00 == null) {
            this.A00 = new AnonymousClass4QM(A00.AIP("X-WA-Metadata"), ((C37621mj) A00).A00);
        }
        return A00;
    }

    public final void A01() {
        int[] iArr;
        byte[] bArr;
        C41471ta r2 = this.A0I;
        C14370lK r0 = r2.A09;
        boolean z = r2.A0U;
        if (AnonymousClass14P.A01(r0) && z && !r2.A0N && (iArr = this.A0R) != null) {
            C28781Oz r5 = this.A0H;
            if (r5.A0G() == null) {
                try {
                    AnonymousClass03k r1 = new AnonymousClass03k(new C37601mh(C12990iw.A0h(this.A0N), (long) iArr[0]), C22190yg.A09);
                    bArr = AnonymousClass3A5.A00(r1);
                    r1.close();
                } catch (IOException e) {
                    Log.e("ProgressiveJpegUtils/generateThumbnailFromFirstScan/errorGeneratingThumbnail", e);
                    bArr = null;
                }
                r5.A0F(bArr);
            }
        }
    }

    public final boolean A02(long j) {
        long j2;
        int i = this.A0G.A0s;
        if (i == 2 || i == 3) {
            C41471ta r1 = this.A0I;
            if (!r1.A0U || !AnonymousClass14P.A02(r1.A09) || i != 3) {
                int[] iArr = this.A0R;
                j2 = iArr != null ? (long) iArr[0] : 262144;
            } else {
                long j3 = r1.A07;
                long j4 = r1.A06;
                if (j4 == 0) {
                    j2 = 0;
                } else {
                    j2 = (long) Math.ceil((double) (5.0f * (((float) j3) / ((float) j4))));
                }
            }
            if (j >= j2) {
                return true;
            }
        }
        return false;
    }

    public final boolean A03(AnonymousClass3G0 r9) {
        int[] iArr;
        String str;
        C41471ta r7 = this.A0I;
        if (r7.A02 == 3 && r9 != null && (iArr = this.A0R) != null && iArr.length == 4 && (str = r7.A0K) != null && r9.A08(0) && r9.A08(1) && r9.A08(2)) {
            try {
                if (this.A0L.A00(this.A0N, iArr[0], r7.A0U) && A04(str)) {
                    return A05(iArr);
                }
            } catch (IOException e) {
                Log.e("StreamMediaDownloadHandler/attemptSetPartialProgressiveJpegOnDownloadFailure", e);
            }
        }
        return false;
    }

    public final boolean A04(String str) {
        long j;
        try {
            File file = this.A0N;
            MessageDigest instance = MessageDigest.getInstance("SHA-256");
            int[] iArr = this.A0R;
            if (iArr == null || iArr.length != 4) {
                j = -1;
            } else {
                j = (long) (iArr[0] + iArr[1] + iArr[2]);
            }
            BufferedInputStream bufferedInputStream = new BufferedInputStream(new C37601mh(C22200yh.A0K(file), j));
            try {
                C38531oF.A02(bufferedInputStream, instance);
                String encodeToString = Base64.encodeToString(instance.digest(), 2);
                bufferedInputStream.close();
                if (str.equals(encodeToString)) {
                    return true;
                }
                Log.e("ProgressiveJpegUtils/setPartialImageFailed plaintextHashes did not match");
                return false;
            } catch (Throwable th) {
                try {
                    bufferedInputStream.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } catch (IOException | NoSuchAlgorithmException e) {
            Log.e("ProgressiveJpegUtils/validatePartialHashesFailed", e);
            return false;
        }
    }

    public final boolean A05(int[] iArr) {
        boolean z;
        try {
            RandomAccessFile randomAccessFile = new RandomAccessFile(this.A0N, "rw");
            long j = (long) (iArr[0] + iArr[1] + iArr[2]);
            randomAccessFile.seek(j);
            byte[] bArr = C22190yg.A09;
            randomAccessFile.write(bArr);
            randomAccessFile.setLength(j + ((long) bArr.length));
            randomAccessFile.getFD().sync();
            randomAccessFile.close();
            z = true;
        } catch (IOException unused) {
            Log.e("ProgressiveJpegUtils/setPartialImageToReadableFile/failed to set file");
            z = false;
        }
        C28781Oz r1 = this.A0H;
        if (z) {
            synchronized (r1) {
                r1.A0H = true;
            }
            this.A0K.A04(3);
            return true;
        }
        r1.A06();
        return false;
    }
}
