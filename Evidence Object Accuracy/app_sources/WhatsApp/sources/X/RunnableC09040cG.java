package X;

import android.view.ViewParent;

/* renamed from: X.0cG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09040cG implements Runnable {
    public final /* synthetic */ AnonymousClass0WW A00;

    public RunnableC09040cG(AnonymousClass0WW r1) {
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        ViewParent parent = this.A00.A07.getParent();
        if (parent != null) {
            parent.requestDisallowInterceptTouchEvent(true);
        }
    }
}
