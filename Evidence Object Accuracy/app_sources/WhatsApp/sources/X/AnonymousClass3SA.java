package X;

import android.graphics.Rect;
import android.view.View;
import androidx.viewpager.widget.ViewPager;
import com.whatsapp.jid.UserJid;
import com.whatsapp.status.playback.StatusPlaybackActivity;
import com.whatsapp.status.playback.fragment.StatusPlaybackBaseFragment;
import com.whatsapp.status.playback.fragment.StatusPlaybackContactFragment;
import com.whatsapp.status.playback.fragment.StatusPlaybackFragment;
import java.util.Iterator;

/* renamed from: X.3SA  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3SA implements AnonymousClass070 {
    public int A00 = -1;
    public StatusPlaybackFragment A01 = null;
    public boolean A02 = false;
    public final Rect A03 = C12980iv.A0J();
    public final Rect A04 = C12980iv.A0J();
    public final /* synthetic */ StatusPlaybackActivity A05;

    public /* synthetic */ AnonymousClass3SA(StatusPlaybackActivity statusPlaybackActivity) {
        this.A05 = statusPlaybackActivity;
    }

    @Override // X.AnonymousClass070
    public void ATO(int i) {
        char c;
        AbstractC33631eh A1I;
        StatusPlaybackActivity statusPlaybackActivity = this.A05;
        if (i != 0) {
            if (!statusPlaybackActivity.A0K) {
                statusPlaybackActivity.A0K = true;
                this.A00 = statusPlaybackActivity.A07.getCurrentItem();
            }
            c = 1;
            if (i != 1) {
                c = 2;
                if (i != 2) {
                    throw C12960it.A0U("Invalid scrollState value from ViewPager");
                }
            }
        } else {
            c = 0;
            statusPlaybackActivity.A0K = false;
            this.A00 = -1;
            this.A02 = false;
            this.A01 = null;
            Iterator A0o = ActivityC13810kN.A0o(statusPlaybackActivity);
            while (A0o.hasNext()) {
                AnonymousClass01E r1 = (AnonymousClass01E) A0o.next();
                if (r1 instanceof StatusPlaybackFragment) {
                    StatusPlaybackFragment statusPlaybackFragment = (StatusPlaybackFragment) r1;
                    if (!statusPlaybackFragment.A00 && (A1I = ((StatusPlaybackContactFragment) statusPlaybackFragment).A1I()) != null && A1I.A05) {
                        A1I.A06(0);
                    }
                }
            }
            this.A02 = false;
            Runnable runnable = statusPlaybackActivity.A0H;
            if (runnable != null) {
                runnable.run();
                statusPlaybackActivity.A0H = null;
            }
        }
        Iterator A0o2 = ActivityC13810kN.A0o(statusPlaybackActivity);
        while (A0o2.hasNext()) {
            AnonymousClass01E r2 = (AnonymousClass01E) A0o2.next();
            if (r2 instanceof StatusPlaybackFragment) {
                StatusPlaybackFragment statusPlaybackFragment2 = (StatusPlaybackFragment) r2;
                if (statusPlaybackFragment2 instanceof StatusPlaybackBaseFragment) {
                    StatusPlaybackBaseFragment statusPlaybackBaseFragment = (StatusPlaybackBaseFragment) statusPlaybackFragment2;
                    if (c == 0) {
                        statusPlaybackBaseFragment.A1H(false);
                    } else if (c == 1 || c == 2) {
                        statusPlaybackBaseFragment.A1H(true);
                    }
                }
            }
        }
    }

    @Override // X.AnonymousClass070
    public void ATP(int i, float f, int i2) {
        View view;
        StatusPlaybackActivity statusPlaybackActivity;
        ViewPager viewPager;
        if (!Float.isNaN(f) && f != 0.0f && f != 1.0f) {
            boolean A1V = C12960it.A1V(i, this.A00);
            if (!this.A02) {
                StatusPlaybackFragment statusPlaybackFragment = this.A01;
                if (statusPlaybackFragment == null) {
                    StatusPlaybackActivity statusPlaybackActivity2 = this.A05;
                    if (A1V) {
                        i++;
                    }
                    AnonymousClass3EE r0 = statusPlaybackActivity2.A0D;
                    statusPlaybackFragment = null;
                    if (r0 != null && i >= 0 && i < r0.A00.size()) {
                        statusPlaybackFragment = statusPlaybackActivity2.A2f((AnonymousClass4LD) statusPlaybackActivity2.A0D.A00.get(i));
                    }
                    this.A01 = statusPlaybackFragment;
                    if (statusPlaybackFragment == null) {
                        return;
                    }
                }
                if (!statusPlaybackFragment.A00 && (view = statusPlaybackFragment.A0A) != null && (viewPager = (statusPlaybackActivity = this.A05).A07) != null && viewPager.isShown() && view.isShown()) {
                    Rect rect = this.A04;
                    viewPager.getGlobalVisibleRect(rect);
                    Rect rect2 = this.A03;
                    view.getGlobalVisibleRect(rect2);
                    if (rect.intersect(rect2)) {
                        StatusPlaybackFragment statusPlaybackFragment2 = this.A01;
                        int i3 = statusPlaybackActivity.A02;
                        if (i3 != 0) {
                            statusPlaybackActivity.A02 = 0;
                        } else {
                            i3 = 2;
                            if (A1V) {
                                i3 = 3;
                            }
                        }
                        statusPlaybackFragment2.A1C(i3);
                        this.A02 = true;
                    }
                }
            }
        }
    }

    @Override // X.AnonymousClass070
    public void ATQ(int i) {
        boolean z;
        StatusPlaybackActivity statusPlaybackActivity = this.A05;
        if (i != statusPlaybackActivity.A01) {
            statusPlaybackActivity.A0I = false;
            AnonymousClass4LD r5 = (AnonymousClass4LD) statusPlaybackActivity.A0D.A00.get(i);
            if (r5 != null) {
                Iterator A0o = ActivityC13810kN.A0o(statusPlaybackActivity);
                while (true) {
                    z = true;
                    if (!A0o.hasNext()) {
                        break;
                    }
                    AnonymousClass01E r4 = (AnonymousClass01E) A0o.next();
                    if (r4 instanceof StatusPlaybackFragment) {
                        StatusPlaybackFragment statusPlaybackFragment = (StatusPlaybackFragment) r4;
                        String rawString = r5.A00.A0A.getRawString();
                        StatusPlaybackContactFragment statusPlaybackContactFragment = (StatusPlaybackContactFragment) statusPlaybackFragment;
                        UserJid userJid = statusPlaybackContactFragment.A0P;
                        AnonymousClass009.A05(userJid);
                        if (!rawString.equals(userJid.getRawString()) && statusPlaybackFragment.A00) {
                            if (i <= statusPlaybackActivity.A01) {
                                z = false;
                            }
                            int i2 = statusPlaybackActivity.A03;
                            if (i2 != 0) {
                                statusPlaybackActivity.A03 = 0;
                            } else {
                                i2 = 6;
                                if (z) {
                                    i2 = 7;
                                }
                            }
                            AbstractC33631eh A1I = statusPlaybackContactFragment.A1I();
                            if (A1I != null && A1I.A05) {
                                A1I.A06(i2);
                            }
                            statusPlaybackFragment.A1B();
                        }
                    }
                }
                StatusPlaybackFragment A2f = statusPlaybackActivity.A2f(r5);
                if (A2f != null && !A2f.A00) {
                    A2f.A1A();
                    if (i <= statusPlaybackActivity.A01) {
                        z = false;
                    }
                    int i3 = statusPlaybackActivity.A02;
                    if (i3 != 0) {
                        statusPlaybackActivity.A02 = 0;
                    } else {
                        i3 = 2;
                        if (z) {
                            i3 = 3;
                        }
                    }
                    A2f.A1C(i3);
                }
            }
            statusPlaybackActivity.A01 = i;
        }
    }
}
