package X;

import android.content.Context;
import android.text.TextUtils;
import android.util.Pair;
import com.whatsapp.R;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* renamed from: X.11L  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass11L {
    public static final int[] A0F = {R.plurals.community_activity_unknown_add, R.plurals.community_activity_one_or_many_add, R.string.community_activity_two_add, R.string.community_activity_three_add};
    public static final int[] A0G = {R.string.existing_group_linked_to_community_system_message_created_by_you, R.string.existing_group_linked_to_community_system_message, R.string.existing_group_linked_to_community_system_message_created_by_creator};
    public static final int[] A0H = {R.string.existing_group_linked_to_community_system_message_created_by_you_unknown_community, R.string.existing_group_linked_to_community_system_message_unknown_community, R.string.existing_group_linked_to_community_system_message_created_by_creator_unknown_community};
    public static final int[][] A0I = {new int[]{R.string.system_message_deleted_parent_group_by_you, R.string.system_message_deleted_parent_group_by_you_unknown_community}, new int[]{R.string.system_message_deleted_parent_group_by_someone, R.string.system_message_deleted_parent_group_by_someone_unknown_community_name}, new int[]{R.string.system_message_deleted_parent_group_by_unknown, R.string.system_message_deleted_parent_group_by_unknown_unknown_community}};
    public static final int[][] A0J = {new int[]{R.string.system_message_group_removed_from_parent_group_by_you, R.string.system_message_group_removed_from_parent_group_by_you_unknown_community}, new int[]{R.string.system_message_group_removed_from_parent_group, R.string.system_message_group_removed_from_parent_group_unknown_community}, new int[]{R.string.system_message_group_removed_from_parent_group_unknown_author, R.string.system_message_group_removed_from_parent_group_unknown_author_and_community}};
    public static final int[][] A0K = {new int[]{R.string.group_join_auto_add_you_added_by_unknown_known_community, R.string.group_join_auto_add_you_were_added_by_unknown_unknown_community}, new int[]{R.string.group_join_auto_add_you_were_added_by_someone_known_community, R.string.group_join_auto_add_you_were_added_by_someone_unknown_community}};
    public static final int[][] A0L = {new int[]{R.string.parent_group_description_deleted_by_name, R.string.parent_group_description_changed_by_name, R.string.parent_group_description_changed_by_name_in_chats}, new int[]{R.string.group_description_deleted_by_name, R.string.group_description_changed_by_name, R.string.group_description_changed_by_name_in_chats}};
    public static final int[][] A0M = {new int[]{R.string.parent_group_description_deleted_by_you, R.string.parent_group_description_changed_by_you, R.string.parent_group_description_changed_by_you_in_chats}, new int[]{R.string.group_description_deleted_by_you, R.string.group_description_changed_by_you, R.string.group_description_changed_by_you_in_chats}};
    public static final int[][] A0N = {new int[]{R.plurals.system_message_sibling_link_unknown_name_by_you, R.plurals.system_message_sibling_link_unknown_name_by_author, R.plurals.system_message_sibling_link_unknown_name_by_unknown}, new int[]{R.plurals.system_message_sibling_link_many_known_name_by_you, R.plurals.system_message_sibling_link_many_known_name_by_author, R.plurals.system_message_sibling_link_many_known_name_by_unknown}, new int[]{R.string.system_message_sibling_link_two_known_name_by_you, R.string.system_message_sibling_link_two_known_name_by_author, R.string.system_message_sibling_link_two_known_name_by_unknown}, new int[]{R.string.system_message_sibling_link_three_known_name_by_you, R.string.system_message_sibling_link_three_known_name_by_author, R.string.system_message_sibling_link_three_known_name_by_unknown}};
    public static final int[][] A0O = {new int[]{R.plurals.system_message_sibling_unlink_unknown_name_by_you, R.plurals.system_message_sibling_unlink_unknown_name_by_author, R.plurals.system_message_sibling_unlink_unknown_name_by_unknown}, new int[]{R.string.system_message_sibling_unlink_one_known_name_by_you, R.string.system_message_sibling_unlink_one_known_name_by_author, R.string.system_message_sibling_unlink_one_known_name_by_unknown}, new int[]{R.plurals.system_message_sibling_unlink_many_known_name_by_you, R.plurals.system_message_sibling_unlink_many_known_name_by_author, R.plurals.system_message_sibling_unlink_many_known_name_by_unknown}};
    public static final int[][] A0P = {new int[]{R.string.parent_group_description_deleted_by_participant, R.string.parent_group_description_changed_by_participant, R.string.parent_group_description_changed_by_participant_in_chats}, new int[]{R.string.group_description_deleted_by_participant, R.string.group_description_changed_by_participant, R.string.group_description_changed_by_participant_in_chats}};
    public final AbstractC15710nm A00;
    public final C15570nT A01;
    public final AnonymousClass11I A02;
    public final C15550nR A03;
    public final C15610nY A04;
    public final AnonymousClass11J A05;
    public final C16590pI A06;
    public final AnonymousClass018 A07;
    public final C15600nX A08;
    public final C14850m9 A09;
    public final C20710wC A0A;
    public final AnonymousClass11G A0B;
    public final C14840m8 A0C;
    public final AnonymousClass11H A0D;
    public final AnonymousClass11K A0E;

    public AnonymousClass11L(AbstractC15710nm r1, C15570nT r2, AnonymousClass11I r3, C15550nR r4, C15610nY r5, AnonymousClass11J r6, C16590pI r7, AnonymousClass018 r8, C15600nX r9, C14850m9 r10, C20710wC r11, AnonymousClass11G r12, C14840m8 r13, AnonymousClass11H r14, AnonymousClass11K r15) {
        this.A09 = r10;
        this.A00 = r1;
        this.A01 = r2;
        this.A06 = r7;
        this.A03 = r4;
        this.A04 = r5;
        this.A07 = r8;
        this.A0A = r11;
        this.A0C = r13;
        this.A0B = r12;
        this.A0D = r14;
        this.A08 = r9;
        this.A02 = r3;
        this.A05 = r6;
        this.A0E = r15;
    }

    public static Pair A00(List list, int i) {
        int i2;
        int valueOf;
        int i3;
        int size = list.size();
        while (true) {
            i2 = 3;
            if (list.size() >= 3) {
                break;
            }
            list.add(null);
        }
        if (i >= 1 && (size >= 3 || size == i)) {
            if (size == 3) {
                if (i > 3) {
                    i3 = i - 2;
                    valueOf = 1;
                }
                valueOf = Integer.valueOf(i2);
                i3 = i - 2;
            } else if (size == 1) {
                i3 = 1;
                valueOf = 1;
            } else {
                if (size == 2) {
                    i2 = 2;
                } else if (size != 3) {
                    AnonymousClass009.A07("Unexpected number of display names");
                }
                valueOf = Integer.valueOf(i2);
                i3 = i - 2;
            }
            return new Pair(valueOf, Integer.valueOf(i3));
        }
        return new Pair(0, 0);
    }

    public static List A01(Set set, int i) {
        ArrayList arrayList = new ArrayList();
        Iterator it = set.iterator();
        while (it.hasNext() && arrayList.size() < i) {
            String str = ((AnonymousClass1OU) it.next()).A03;
            if (!TextUtils.isEmpty(str)) {
                arrayList.add(str);
            }
        }
        return arrayList;
    }

    public final String A02(C15370n3 r4, int i) {
        String A0B = this.A04.A0B(r4, i, false);
        if (A0B == null) {
            return null;
        }
        return this.A07.A0F(A0B);
    }

    public String A03(AbstractC14640lm r7, int i) {
        int i2;
        int i3;
        int i4;
        int i5;
        if (this.A01.A0F(r7) || r7 == C29831Uv.A00) {
            if (i <= 86400) {
                i2 = i / 3600;
                i3 = R.plurals.dm_system_message_bubble_hours_you;
            } else {
                i2 = i / 86400;
                i3 = R.plurals.dm_system_message_bubble_days_you;
            }
            return this.A06.A00.getResources().getQuantityString(i3, i2, Integer.valueOf(i2));
        }
        if (i <= 86400) {
            i4 = i / 3600;
            i5 = R.plurals.dm_system_message_bubble_hours_other;
        } else {
            i4 = i / 86400;
            i5 = R.plurals.dm_system_message_bubble_days_other;
        }
        C15370n3 A0B = this.A03.A0B(r7);
        int i6 = 2;
        if (C15380n4.A0J(r7)) {
            i6 = 1;
        }
        return this.A06.A00.getResources().getQuantityString(i5, i4, A02(A0B, i6), Integer.valueOf(i4));
    }

    public String A04(AbstractC14640lm r8, int i, boolean z) {
        int i2;
        Context context;
        int i3;
        if (r8 != null) {
            if (!this.A01.A0F(r8) && r8 != C29831Uv.A00) {
                C15370n3 A0B = this.A03.A0B(r8);
                int i4 = 2;
                if (C15380n4.A0J(r8)) {
                    i4 = 1;
                }
                String A02 = A02(A0B, i4);
                if (A02 != null) {
                    if (i <= 0) {
                        Context context2 = this.A06.A00;
                        int i5 = R.string.ephemeral_setting_disabled_by_name;
                        if (z) {
                            i5 = R.string.ephemeral_setting_disabled_by_name_tap_to_change;
                        }
                        return context2.getString(i5, A02);
                    }
                    int i6 = R.plurals.ephemeral_setting_enabled_by_name_in_seconds;
                    if (z) {
                        i6 = R.plurals.ephemeral_setting_enabled_by_name_in_seconds_tap_to_change;
                    }
                    if (i > 86400) {
                        i /= 86400;
                        i6 = R.plurals.ephemeral_setting_enabled_by_name_in_days;
                        if (z) {
                            i6 = R.plurals.ephemeral_setting_enabled_by_name_in_days_tap_to_change;
                        }
                    } else if (i >= 3600) {
                        i /= 3600;
                        i6 = R.plurals.ephemeral_setting_enabled_by_name_in_hours;
                        if (z) {
                            i6 = R.plurals.ephemeral_setting_enabled_by_name_in_hours_tap_to_change;
                        }
                    } else if (i >= 60) {
                        i /= 60;
                        i6 = R.plurals.ephemeral_setting_enabled_by_name_in_minutes;
                        if (z) {
                            i6 = R.plurals.ephemeral_setting_enabled_by_name_in_minutes_tap_to_change;
                        }
                    }
                    return this.A06.A00.getResources().getQuantityString(i6, i, A02, Integer.valueOf(i));
                }
            } else if (i <= 0) {
                context = this.A06.A00;
                i3 = R.string.ephemeral_setting_disabled_by_you;
                if (z) {
                    i3 = R.string.ephemeral_setting_disabled_by_you_tap_to_change;
                }
                return context.getString(i3);
            } else {
                i2 = R.plurals.ephemeral_setting_enabled_by_you_in_seconds;
                if (z) {
                    i2 = R.plurals.ephemeral_setting_enabled_by_you_in_seconds_tap_to_change;
                }
                if (i > 86400) {
                    i /= 86400;
                    i2 = R.plurals.ephemeral_setting_enabled_by_you_in_days;
                    if (z) {
                        i2 = R.plurals.ephemeral_setting_enabled_by_you_in_days_tap_to_change;
                    }
                } else if (i >= 3600) {
                    i /= 3600;
                    i2 = R.plurals.ephemeral_setting_enabled_by_you_in_hours;
                    if (z) {
                        i2 = R.plurals.ephemeral_setting_enabled_by_you_in_hours_tap_to_change;
                    }
                } else if (i >= 60) {
                    i /= 60;
                    i2 = R.plurals.ephemeral_setting_enabled_by_you_in_minutes;
                    if (z) {
                        i2 = R.plurals.ephemeral_setting_enabled_by_you_in_minutes_tap_to_change;
                    }
                }
                return this.A06.A00.getResources().getQuantityString(i2, i, Integer.valueOf(i));
            }
        }
        if (i <= 0) {
            context = this.A06.A00;
            i3 = R.string.ephemeral_setting_disabled;
            if (z) {
                i3 = R.string.ephemeral_setting_disabled_tap_to_change;
            }
            return context.getString(i3);
        }
        i2 = R.plurals.ephemeral_setting_enabled_in_seconds;
        if (z) {
            i2 = R.plurals.ephemeral_setting_enabled_in_seconds_tap_to_change;
        }
        if (i > 86400) {
            i /= 86400;
            i2 = R.plurals.ephemeral_setting_enabled_in_days;
            if (z) {
                i2 = R.plurals.ephemeral_setting_enabled_in_days_tap_to_change;
            }
        } else if (i >= 3600) {
            i /= 3600;
            i2 = R.plurals.ephemeral_setting_enabled_in_hours;
            if (z) {
                i2 = R.plurals.ephemeral_setting_enabled_in_hours_tap_to_change;
            }
        } else if (i >= 60) {
            i /= 60;
            i2 = R.plurals.ephemeral_setting_enabled_in_minutes;
            if (z) {
                i2 = R.plurals.ephemeral_setting_enabled_in_minutes_tap_to_change;
            }
        }
        return this.A06.A00.getResources().getQuantityString(i2, i, Integer.valueOf(i));
    }

    public final String A05(AnonymousClass1XB r5, int i, int i2, int i3) {
        UserJid of = UserJid.of(r5.A0B());
        AnonymousClass009.A05(of);
        if (this.A01.A0F(of)) {
            return this.A06.A00.getString(i);
        }
        String A04 = this.A04.A04(this.A03.A0B(of));
        if (!"admin".equals(((C30481Xo) r5).A00)) {
            i2 = i3;
        }
        return this.A06.A00.getString(i2, A04);
    }

    public final String A06(AnonymousClass1XB r10, int i, int i2, int i3) {
        List<Jid> list = ((C30461Xm) r10).A01;
        if (list.size() == 1 && this.A01.A0F((Jid) list.get(0))) {
            return this.A06.A00.getString(i);
        }
        C15570nT r4 = this.A01;
        r4.A08();
        if (!(list.contains(r4.A05) || list.contains(r4.A02()))) {
            return this.A06.A00.getResources().getQuantityString(i3, list.size(), this.A07.A0F(this.A04.A0F(list, -1)));
        }
        ArrayList arrayList = new ArrayList();
        for (Jid jid : list) {
            if (!r4.A0F(jid)) {
                arrayList.add(jid);
            }
        }
        return this.A06.A00.getResources().getQuantityString(i2, arrayList.size(), this.A07.A0F(this.A04.A0F(arrayList, -1)));
    }

    public final String A07(AnonymousClass1XB r5, int i, int i2, int i3, int i4) {
        Context context;
        UserJid of = UserJid.of(r5.A0B());
        C15580nU A02 = C15580nU.A02(r5.A0z.A00);
        if (of == null) {
            C15600nX r0 = this.A08;
            AnonymousClass009.A05(A02);
            boolean A0D = r0.A0D(A02);
            context = this.A06.A00;
            i = R.string.group_member_add_mode_setting_disabled_by_server_member_system_message;
            if (A0D) {
                i = R.string.group_member_add_mode_setting_disabled_by_server_admin_soak_system_message;
            }
        } else if (this.A01.A0F(of)) {
            context = this.A06.A00;
        } else {
            String A04 = this.A04.A04(this.A03.A0B(of));
            boolean A0D2 = this.A08.A0D(A02);
            boolean A07 = this.A09.A07(1863);
            if (!A0D2) {
                i2 = i3;
            }
            if (A07) {
                i2 = i4;
            }
            return this.A06.A00.getString(i2, A04);
        }
        return context.getString(i);
    }

    public final String A08(AnonymousClass1XB r5, String str, int i, int i2, int i3) {
        if (C15380n4.A0F(r5.A0z.A00)) {
            return this.A06.A00.getString(i, str);
        }
        boolean A0F2 = this.A01.A0F(r5.A0B());
        Context context = this.A06.A00;
        if (A0F2) {
            return context.getString(i2);
        }
        return context.getResources().getQuantityString(i3, 1, str);
    }

    public final String A09(AnonymousClass1XB r10, String str, int i, int i2, int i3, int i4, int i5, int i6) {
        List list = ((C30461Xm) r10).A01;
        AbstractC14640lm r2 = r10.A0z.A00;
        if (C15380n4.A0F(r2)) {
            return this.A06.A00.getResources().getQuantityString(i6, list.size(), this.A07.A0F(this.A04.A0F(list, -1)));
        }
        C15570nT r1 = this.A01;
        if (r1.A0F(r10.A0B())) {
            return this.A06.A00.getString(i, this.A07.A0F(this.A04.A0F(list, -1)));
        }
        int size = list.size();
        if (str == null) {
            if (size != 1 || !r1.A0F((Jid) list.get(0))) {
                return this.A06.A00.getResources().getQuantityString(i3, list.size(), this.A07.A0F(this.A04.A0F(list, -1)));
            }
            return this.A06.A00.getString(i2);
        } else if (size == 1 && r1.A0F((Jid) list.get(0))) {
            return this.A06.A00.getString(i4, str);
        } else {
            int i7 = 2;
            if (C15380n4.A0J(r2)) {
                i7 = 1;
            }
            return this.A06.A00.getString(i5, str, this.A07.A0F(this.A04.A0F(list, i7)));
        }
    }

    /* JADX WARN: Type inference failed for: r9v2, types: [boolean] */
    /* JADX WARNING: Code restructure failed: missing block: B:133:0x02a3, code lost:
        if (r6.lastIndexOf("\"") > r6.indexOf("\"")) goto L_0x02a5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:194:0x0417, code lost:
        if (r24.A01.A0F(r5) == false) goto L_0x0419;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:685:0x1155, code lost:
        if (r24.A01.A0F((com.whatsapp.jid.Jid) r8.get(0)) == false) goto L_0x1157;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x01d2, code lost:
        if (r8 == 3) goto L_0x01d4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x01de, code lost:
        if (r8 != 0) goto L_0x01e0;
     */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x026a  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003c A[PHI: r15 
      PHI: (r15v133 java.lang.String) = (r15v0 java.lang.String), (r15v0 java.lang.String), (r15v0 java.lang.String), (r15v0 java.lang.String), (r15v0 java.lang.String), (r15v0 java.lang.String), (r15v0 java.lang.String), (r15v0 java.lang.String), (r15v123 java.lang.String), (r15v0 java.lang.String) binds: [B:11:0x0039, B:101:0x021c, B:66:0x019c, B:68:0x01a2, B:62:0x0180, B:44:0x012d, B:46:0x013c, B:40:0x0107, B:33:0x00d7, B:14:0x0044] A[DONT_GENERATE, DONT_INLINE], RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:166:0x03b1  */
    /* JADX WARNING: Removed duplicated region for block: B:189:0x0404  */
    /* JADX WARNING: Removed duplicated region for block: B:208:0x0459  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0064  */
    /* JADX WARNING: Removed duplicated region for block: B:224:0x04b4  */
    /* JADX WARNING: Removed duplicated region for block: B:226:0x04e5  */
    /* JADX WARNING: Removed duplicated region for block: B:228:0x04f3  */
    /* JADX WARNING: Removed duplicated region for block: B:230:0x0501  */
    /* JADX WARNING: Removed duplicated region for block: B:232:0x0513  */
    /* JADX WARNING: Removed duplicated region for block: B:234:0x0525  */
    /* JADX WARNING: Removed duplicated region for block: B:236:0x0535  */
    /* JADX WARNING: Removed duplicated region for block: B:242:0x0559  */
    /* JADX WARNING: Removed duplicated region for block: B:244:0x0569  */
    /* JADX WARNING: Removed duplicated region for block: B:252:0x0588  */
    /* JADX WARNING: Removed duplicated region for block: B:258:0x05d9  */
    /* JADX WARNING: Removed duplicated region for block: B:267:0x062a  */
    /* JADX WARNING: Removed duplicated region for block: B:284:0x0685  */
    /* JADX WARNING: Removed duplicated region for block: B:286:0x069e  */
    /* JADX WARNING: Removed duplicated region for block: B:288:0x06bf  */
    /* JADX WARNING: Removed duplicated region for block: B:290:0x06d8  */
    /* JADX WARNING: Removed duplicated region for block: B:292:0x06e6  */
    /* JADX WARNING: Removed duplicated region for block: B:294:0x06f4  */
    /* JADX WARNING: Removed duplicated region for block: B:303:0x071d  */
    /* JADX WARNING: Removed duplicated region for block: B:312:0x0758  */
    /* JADX WARNING: Removed duplicated region for block: B:324:0x07a0  */
    /* JADX WARNING: Removed duplicated region for block: B:337:0x07d4  */
    /* JADX WARNING: Removed duplicated region for block: B:339:0x07e0  */
    /* JADX WARNING: Removed duplicated region for block: B:341:0x07f4  */
    /* JADX WARNING: Removed duplicated region for block: B:343:0x0808  */
    /* JADX WARNING: Removed duplicated region for block: B:345:0x081c  */
    /* JADX WARNING: Removed duplicated region for block: B:347:0x0828  */
    /* JADX WARNING: Removed duplicated region for block: B:363:0x089a  */
    /* JADX WARNING: Removed duplicated region for block: B:371:0x08be  */
    /* JADX WARNING: Removed duplicated region for block: B:379:0x08e2  */
    /* JADX WARNING: Removed duplicated region for block: B:385:0x0906  */
    /* JADX WARNING: Removed duplicated region for block: B:391:0x092a  */
    /* JADX WARNING: Removed duplicated region for block: B:394:0x0938  */
    /* JADX WARNING: Removed duplicated region for block: B:396:0x0944  */
    /* JADX WARNING: Removed duplicated region for block: B:398:0x0958  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00f5  */
    /* JADX WARNING: Removed duplicated region for block: B:400:0x096c  */
    /* JADX WARNING: Removed duplicated region for block: B:422:0x09f8  */
    /* JADX WARNING: Removed duplicated region for block: B:424:0x0a1a  */
    /* JADX WARNING: Removed duplicated region for block: B:426:0x0a3c  */
    /* JADX WARNING: Removed duplicated region for block: B:428:0x0a5f  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x012b  */
    /* JADX WARNING: Removed duplicated region for block: B:443:0x0ac3  */
    /* JADX WARNING: Removed duplicated region for block: B:445:0x0acf  */
    /* JADX WARNING: Removed duplicated region for block: B:447:0x0ae3  */
    /* JADX WARNING: Removed duplicated region for block: B:449:0x0af7  */
    /* JADX WARNING: Removed duplicated region for block: B:451:0x0b0b  */
    /* JADX WARNING: Removed duplicated region for block: B:453:0x0b17  */
    /* JADX WARNING: Removed duplicated region for block: B:464:0x0b92  */
    /* JADX WARNING: Removed duplicated region for block: B:472:0x0bb4  */
    /* JADX WARNING: Removed duplicated region for block: B:484:0x0be2  */
    /* JADX WARNING: Removed duplicated region for block: B:486:0x0bf6  */
    /* JADX WARNING: Removed duplicated region for block: B:488:0x0c0c  */
    /* JADX WARNING: Removed duplicated region for block: B:502:0x0c5e  */
    /* JADX WARNING: Removed duplicated region for block: B:510:0x0c7d  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x015c  */
    /* JADX WARNING: Removed duplicated region for block: B:566:0x0e37  */
    /* JADX WARNING: Removed duplicated region for block: B:568:0x0e47  */
    /* JADX WARNING: Removed duplicated region for block: B:570:0x0e57  */
    /* JADX WARNING: Removed duplicated region for block: B:576:0x0e87  */
    /* JADX WARNING: Removed duplicated region for block: B:578:0x0e93  */
    /* JADX WARNING: Removed duplicated region for block: B:580:0x0e9f  */
    /* JADX WARNING: Removed duplicated region for block: B:592:0x0ee9  */
    /* JADX WARNING: Removed duplicated region for block: B:598:0x0f08  */
    /* JADX WARNING: Removed duplicated region for block: B:616:0x0f47  */
    /* JADX WARNING: Removed duplicated region for block: B:622:0x0f67  */
    /* JADX WARNING: Removed duplicated region for block: B:638:0x1016  */
    /* JADX WARNING: Removed duplicated region for block: B:653:0x108d  */
    /* JADX WARNING: Removed duplicated region for block: B:655:0x109b  */
    /* JADX WARNING: Removed duplicated region for block: B:657:0x10a9  */
    /* JADX WARNING: Removed duplicated region for block: B:659:0x10b7  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x019a  */
    /* JADX WARNING: Removed duplicated region for block: B:661:0x10c7  */
    /* JADX WARNING: Removed duplicated region for block: B:667:0x10eb  */
    /* JADX WARNING: Removed duplicated region for block: B:672:0x10fd  */
    /* JADX WARNING: Removed duplicated region for block: B:679:0x1113  */
    /* JADX WARNING: Removed duplicated region for block: B:711:0x1200  */
    /* JADX WARNING: Removed duplicated region for block: B:713:0x120a  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x01c3  */
    /* JADX WARNING: Removed duplicated region for block: B:751:0x12cd  */
    /* JADX WARNING: Removed duplicated region for block: B:754:0x12de  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A0A(X.AnonymousClass1XB r25, boolean r26) {
        /*
        // Method dump skipped, instructions count: 5194
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass11L.A0A(X.1XB, boolean):java.lang.String");
    }
}
