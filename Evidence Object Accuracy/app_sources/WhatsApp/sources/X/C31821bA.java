package X;

/* renamed from: X.1bA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C31821bA implements AbstractC31371aR {
    public final int A00;
    public final int A01;
    public final C31491ad A02;
    public final byte[] A03;
    public final byte[][] A04;

    @Override // X.AbstractC31371aR
    public int getType() {
        return 6;
    }

    public C31821bA(C31491ad r11, byte[][] bArr, int i, int i2) {
        byte[] bArr2 = {(byte) 51};
        AnonymousClass1G4 A0T = AnonymousClass25I.A05.A0T();
        A0T.A03();
        AnonymousClass25I r1 = (AnonymousClass25I) A0T.A00;
        r1.A00 |= 1;
        r1.A01 = i;
        A0T.A03();
        AnonymousClass25I r12 = (AnonymousClass25I) A0T.A00;
        r12.A00 |= 2;
        r12.A02 = i2;
        byte[] A00 = r11.A00();
        AbstractC27881Jp A01 = AbstractC27881Jp.A01(A00, 0, A00.length);
        A0T.A03();
        AnonymousClass25I r13 = (AnonymousClass25I) A0T.A00;
        r13.A00 |= 4;
        r13.A03 = A01;
        for (byte[] bArr3 : bArr) {
            AbstractC27881Jp A012 = AbstractC27881Jp.A01(bArr3, 0, bArr3.length);
            A0T.A03();
            AnonymousClass25I r2 = (AnonymousClass25I) A0T.A00;
            AnonymousClass1K6 r14 = r2.A04;
            if (!((AnonymousClass1K7) r14).A00) {
                r14 = AbstractC27091Fz.A0G(r14);
                r2.A04 = r14;
            }
            r14.add(A012);
        }
        byte[] A02 = A0T.A02().A02();
        this.A00 = i;
        this.A01 = i2;
        this.A04 = bArr;
        this.A02 = r11;
        this.A03 = C31241aE.A00(bArr2, A02);
    }

    public C31821bA(byte[] bArr) {
        try {
            byte[][] A01 = C31241aE.A01(bArr, 1, bArr.length - 1);
            int i = 0;
            byte b = A01[0][0];
            byte[] bArr2 = A01[1];
            int i2 = (b & 255) >> 4;
            if (i2 < 3) {
                StringBuilder sb = new StringBuilder();
                sb.append("Legacy message: ");
                sb.append(i2);
                throw new C31571al(sb.toString());
            } else if (i2 <= 3) {
                AnonymousClass25I r2 = (AnonymousClass25I) AbstractC27091Fz.A0E(AnonymousClass25I.A05, bArr2);
                int i3 = r2.A00;
                if ((i3 & 1) == 1 && (i3 & 2) == 2 && r2.A04.size() > 0 && (r2.A00 & 4) == 4) {
                    this.A03 = bArr;
                    this.A00 = r2.A01;
                    this.A01 = r2.A02;
                    this.A02 = C31481ac.A00(r2.A03.A04());
                    AnonymousClass1K6 r22 = r2.A04;
                    this.A04 = new byte[r22.size()];
                    while (true) {
                        byte[][] bArr3 = this.A04;
                        if (i < bArr3.length) {
                            bArr3[i] = ((AbstractC27881Jp) r22.get(i)).A04();
                            i++;
                        } else {
                            return;
                        }
                    }
                } else {
                    throw new C31521ag("Incomplete message.");
                }
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Unknown version: ");
                sb2.append(i2);
                throw new C31521ag(sb2.toString());
            }
        } catch (C28971Pt | C31561ak e) {
            throw new C31521ag(e);
        }
    }

    @Override // X.AbstractC31371aR
    public byte[] Abf() {
        return this.A03;
    }
}
