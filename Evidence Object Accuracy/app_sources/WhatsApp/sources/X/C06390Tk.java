package X;

import android.util.Log;

/* renamed from: X.0Tk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06390Tk {
    public static C06390Tk A01;
    public int A00;

    public C06390Tk(int i) {
        this.A00 = i;
    }

    public static synchronized C06390Tk A00() {
        C06390Tk r1;
        synchronized (C06390Tk.class) {
            r1 = A01;
            if (r1 == null) {
                r1 = new C06390Tk(3);
                A01 = r1;
            }
        }
        return r1;
    }

    public static String A01(String str) {
        int length = str.length();
        StringBuilder sb = new StringBuilder(23);
        sb.append("WM-");
        if (length >= 20) {
            str = str.substring(0, 20);
        }
        sb.append(str);
        return sb.toString();
    }

    public void A02(String str, String str2, Throwable... thArr) {
        if (this.A00 > 3) {
            return;
        }
        if (thArr.length >= 1) {
            Log.d(str, str2, thArr[0]);
        } else {
            Log.d(str, str2);
        }
    }

    public void A03(String str, String str2, Throwable... thArr) {
        if (this.A00 > 6) {
            return;
        }
        if (thArr.length >= 1) {
            Log.e(str, str2, thArr[0]);
        } else {
            Log.e(str, str2);
        }
    }

    public void A04(String str, String str2, Throwable... thArr) {
        if (this.A00 > 4) {
            return;
        }
        if (thArr.length >= 1) {
            Log.i(str, str2, thArr[0]);
        } else {
            Log.i(str, str2);
        }
    }

    public void A05(String str, String str2, Throwable... thArr) {
        if (this.A00 > 5) {
            return;
        }
        if (thArr.length >= 1) {
            Log.w(str, str2, thArr[0]);
        } else {
            Log.w(str, str2);
        }
    }
}
