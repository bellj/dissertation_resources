package X;

import java.util.List;

/* renamed from: X.2wM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C60252wM extends AnonymousClass4K6 {
    public boolean A00;
    public final List A01;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C60252wM) {
                C60252wM r5 = (C60252wM) obj;
                if (!C16700pc.A0O(this.A01, r5.A01) || this.A00 != r5.A00) {
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        int hashCode = this.A01.hashCode() * 31;
        boolean z = this.A00;
        if (z) {
            z = true;
        }
        int i = z ? 1 : 0;
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        return hashCode + i;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C60252wM(List list, boolean z) {
        super(true);
        C16700pc.A0E(list, 1);
        this.A01 = list;
        this.A00 = z;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("FetchCategoriesSuccess(categories=");
        A0k.append(this.A01);
        A0k.append(", cached=");
        A0k.append(this.A00);
        return C12970iu.A0u(A0k);
    }
}
