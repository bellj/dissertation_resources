package X;

import android.content.Context;
import com.whatsapp.ephemeral.ChangeEphemeralSettingActivity;

/* renamed from: X.4qA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103074qA implements AbstractC009204q {
    public final /* synthetic */ ChangeEphemeralSettingActivity A00;

    public C103074qA(ChangeEphemeralSettingActivity changeEphemeralSettingActivity) {
        this.A00 = changeEphemeralSettingActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
