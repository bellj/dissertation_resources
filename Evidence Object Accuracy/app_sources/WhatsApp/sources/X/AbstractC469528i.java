package X;

import com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3;
import com.whatsapp.jid.GroupJid;

/* renamed from: X.28i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC469528i {
    public void A00(GroupJid groupJid) {
        if (this instanceof AnonymousClass32Y) {
            C37271lv r3 = ((AnonymousClass32Y) this).A00;
            if (groupJid.equals(r3.A0O)) {
                r3.A0U.execute(new RunnableBRunnable0Shape3S0100000_I0_3(r3, 47));
            }
        }
    }
}
