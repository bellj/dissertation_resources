package X;

import android.content.SharedPreferences;
import com.whatsapp.util.Log;

/* renamed from: X.11b  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C232811b {
    public final C18640sm A00;
    public final C14820m6 A01;

    public C232811b(C18640sm r1, C14820m6 r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public void A00() {
        SharedPreferences sharedPreferences = this.A01.A00;
        sharedPreferences.edit().putInt("sticker_store_backoff_attempt", 0).apply();
        sharedPreferences.edit().putLong("sticker_store_backoff_time", 0).apply();
        sharedPreferences.edit().putLong("sticker_store_last_fetch_time", System.currentTimeMillis()).apply();
        Log.i("StickerRequestBackoffManager/clearAttempts");
    }

    public void A01() {
        SharedPreferences sharedPreferences = this.A01.A00;
        int i = sharedPreferences.getInt("sticker_store_backoff_attempt", 0) + 1;
        AnonymousClass1VN r4 = new AnonymousClass1VN(1, 720);
        r4.A03((long) i);
        long A01 = r4.A01();
        long currentTimeMillis = (60 * A01 * 1000) + System.currentTimeMillis();
        sharedPreferences.edit().putInt("sticker_store_backoff_attempt", i).apply();
        sharedPreferences.edit().putLong("sticker_store_backoff_time", currentTimeMillis).apply();
        StringBuilder sb = new StringBuilder("StickerRequestBackoffManager/incrementAttempt/Backing off for ");
        sb.append(A01);
        sb.append(" minutes.");
        Log.e(sb.toString());
    }
}
