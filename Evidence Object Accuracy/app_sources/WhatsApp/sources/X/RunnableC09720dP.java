package X;

import android.widget.FrameLayout;

/* renamed from: X.0dP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09720dP implements Runnable {
    public final /* synthetic */ FrameLayout A00;
    public final /* synthetic */ C05150Ol A01;

    public RunnableC09720dP(FrameLayout frameLayout, C05150Ol r2) {
        this.A01 = r2;
        this.A00 = frameLayout;
    }

    @Override // java.lang.Runnable
    public void run() {
        C05150Ol r2 = this.A01;
        r2.A00(this.A00, r2.A04);
    }
}
