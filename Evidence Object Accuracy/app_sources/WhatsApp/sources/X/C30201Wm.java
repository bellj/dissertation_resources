package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1Wm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30201Wm implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C99654ke();
    public final String A00;
    public final String A01;
    public final List A02;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C30201Wm(Parcel parcel) {
        this.A01 = parcel.readString();
        this.A00 = parcel.readString();
        ArrayList createTypedArrayList = parcel.createTypedArrayList(C30191Wl.CREATOR);
        AnonymousClass009.A05(createTypedArrayList);
        this.A02 = createTypedArrayList;
    }

    public C30201Wm(String str, String str2, List list) {
        this.A01 = str;
        this.A00 = str2;
        this.A02 = list;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (!(obj instanceof C30201Wm)) {
            return false;
        }
        C30201Wm r4 = (C30201Wm) obj;
        if (!TextUtils.equals(this.A01, r4.A01) || !TextUtils.equals(this.A00, r4.A00) || !this.A02.equals(r4.A02)) {
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        int i;
        String str = this.A01;
        int i2 = 0;
        if (str != null) {
            i = str.hashCode();
        } else {
            i = 0;
        }
        int i3 = i * 31;
        String str2 = this.A00;
        if (str2 != null) {
            i2 = str2.hashCode();
        }
        return ((i3 + i2) * 31) + this.A02.hashCode();
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("timezone: ");
        sb.append(this.A01);
        sb.append(", note: ");
        sb.append(this.A00);
        for (Object obj : this.A02) {
            sb.append(obj.toString());
            sb.append(";");
        }
        return sb.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A01);
        parcel.writeString(this.A00);
        parcel.writeTypedList(this.A02);
    }
}
