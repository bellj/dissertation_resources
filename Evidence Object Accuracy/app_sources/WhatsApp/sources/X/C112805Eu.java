package X;

/* renamed from: X.5Eu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C112805Eu implements AnonymousClass5ZX {
    public final Class A00;

    public C112805Eu(Class cls) {
        this.A00 = cls;
    }

    @Override // X.AnonymousClass5ZX
    public Class ADf() {
        return this.A00;
    }

    public boolean equals(Object obj) {
        return (obj instanceof C112805Eu) && C16700pc.A0O(this.A00, ((C112805Eu) obj).A00);
    }

    public int hashCode() {
        return this.A00.hashCode();
    }

    public String toString() {
        return C16700pc.A08(this.A00.toString(), " (Kotlin reflection is not available)");
    }
}
