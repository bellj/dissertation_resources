package X;

import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.media.ImageReader;
import android.os.Handler;

/* renamed from: X.61l  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public interface AbstractC1311461l extends Camera.PreviewCallback, ImageReader.OnImageAvailableListener {
    SurfaceTexture ABC();

    void AcE(ImageReader.OnImageAvailableListener onImageAvailableListener, Handler handler);

    void Acb(SurfaceTexture surfaceTexture, int i, int i2);

    void Acc(Camera.PreviewCallback previewCallback, Handler handler);
}
