package X;

import android.os.Build;
import android.util.Base64;
import android.util.JsonReader;
import android.util.JsonToken;
import android.util.JsonWriter;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: X.60Y  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass60Y {
    public long A00;
    public C127395uQ A01;
    public String A02;
    public String A03;
    public String A04 = "PAYMENTS";
    public Random A05;
    public final C14830m7 A06;
    public final C16590pI A07;
    public final AnonymousClass018 A08;
    public final C16120oU A09;
    public final C130155yt A0A;
    public final AnonymousClass600 A0B;
    public final C130125yq A0C;
    public final AnonymousClass61F A0D;
    public final C130105yo A0E;
    public final AnonymousClass18L A0F;
    public final C19930uu A0G;
    public final AtomicLong A0H;

    public AnonymousClass60Y(C14830m7 r4, C16590pI r5, AnonymousClass018 r6, C16120oU r7, C130155yt r8, AnonymousClass600 r9, C130125yq r10, AnonymousClass61F r11, C130105yo r12, AnonymousClass18L r13, C19930uu r14) {
        this.A06 = r4;
        this.A0G = r14;
        this.A07 = r5;
        this.A09 = r7;
        this.A0B = r9;
        this.A08 = r6;
        this.A0A = r8;
        this.A0D = r11;
        this.A0C = r10;
        this.A0E = r12;
        this.A0F = r13;
        Random random = new Random();
        this.A05 = random;
        this.A00 = random.nextLong();
        this.A0H = new AtomicLong(0);
    }

    public static void A00(JsonWriter jsonWriter, Map map) {
        Iterator A0n = C12960it.A0n(map);
        while (A0n.hasNext()) {
            Map.Entry A15 = C12970iu.A15(A0n);
            jsonWriter.name(C12990iw.A0r(A15));
            Object value = A15.getValue();
            if (value instanceof String) {
                jsonWriter.value((String) value);
            } else if (value instanceof Boolean) {
                jsonWriter.value(C12970iu.A1Y(value));
            } else if ((value instanceof Integer) || (value instanceof Long) || (value instanceof Number)) {
                jsonWriter.value((Number) value);
            } else if (value instanceof Map) {
                A00(jsonWriter, (Map) value);
            } else {
                Log.e("Pay: NoviEventLogger/writeMap cannot serialize value");
            }
        }
    }

    public static void A01(AnonymousClass60Y r1, String str, String str2, String str3, String str4) {
        r1.A06(new AnonymousClass610(str, str2, str3, str4).A00);
    }

    public static void A02(AnonymousClass60Y r1, String str, String str2, String str3, String str4) {
        r1.A05(new AnonymousClass610(str, str2, str3, str4).A00);
    }

    public String A03(String str) {
        HashMap A11 = C12970iu.A11();
        JsonReader jsonReader = new JsonReader(new StringReader(str));
        try {
            jsonReader.beginObject();
            while (jsonReader.hasNext()) {
                String nextName = jsonReader.nextName();
                if (jsonReader.peek() == JsonToken.NULL) {
                    jsonReader.nextNull();
                } else if (jsonReader.peek() == JsonToken.NUMBER) {
                    A11.put(nextName, Long.valueOf(jsonReader.nextLong()));
                } else if (jsonReader.peek() == JsonToken.BOOLEAN) {
                    A11.put(nextName, Boolean.valueOf(jsonReader.nextBoolean()));
                } else {
                    A11.put(nextName, jsonReader.nextString());
                }
            }
            jsonReader.endObject();
            jsonReader.close();
            A07(A11);
            return A04(A11);
        } catch (Throwable th) {
            try {
                jsonReader.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final String A04(Map map) {
        StringWriter stringWriter = new StringWriter();
        JsonWriter jsonWriter = new JsonWriter(stringWriter);
        try {
            jsonWriter.beginObject();
            A00(jsonWriter, map);
            jsonWriter.endObject();
            jsonWriter.close();
            String obj = stringWriter.toString();
            C127395uQ r5 = this.A01;
            if (r5 == null) {
                C130105yo r3 = this.A0E;
                r5 = null;
                String string = r3.A02().getString("wavi_event_log_key", null);
                String string2 = r3.A02().getString("wavi_event_log_key_seed", null);
                String string3 = r3.A02().getString("wavi_event_log_root_key_id", null);
                if (!(string == null || string2 == null || string3 == null)) {
                    r5 = new C127395uQ(Base64.decode(string, 0), Base64.decode(string2, 0), Base64.decode(string3, 0));
                }
                this.A01 = r5;
                if (r5 == null) {
                    Log.e("Pay: NoviEventLogger/encrypt logging encryption keys not found");
                    throw new IllegalStateException();
                }
            }
            byte[] bytes = obj.getBytes();
            byte[] bArr = r5.A00;
            byte[] bArr2 = r5.A01;
            byte[] bArr3 = r5.A02;
            byte[] A1a = C117305Zk.A1a(12);
            return Base64.encodeToString(C16050oM.A05(C16050oM.A05(new byte[]{1}, bArr3, bArr2, A1a), C1309560q.A01(bArr, bytes, A1a, null, true)), 2);
        } catch (Throwable th) {
            try {
                jsonWriter.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A05(C128365vz r5) {
        HashMap hashMap = new HashMap(50);
        String str = r5.A0X;
        if (str != null) {
            hashMap.put("novi_wallet_event_name", str);
        }
        String str2 = r5.A0D;
        if (str2 != null) {
            hashMap.put("acct_restriction_type", str2);
        }
        String str3 = r5.A0F;
        if (str3 != null) {
            hashMap.put("app_flow_type", str3);
        }
        String str4 = r5.A0L;
        if (str4 != null) {
            hashMap.put("cta_text", str4);
        }
        String str5 = r5.A0N;
        if (str5 != null) {
            hashMap.put("error_code", str5);
        }
        String str6 = r5.A0O;
        if (str6 != null) {
            hashMap.put("error_desc", str6);
        }
        String str7 = r5.A0P;
        if (str7 != null) {
            hashMap.put("error_title", str7);
        }
        String str8 = r5.A0Q;
        if (str8 != null) {
            hashMap.put("event_status", str8);
        }
        String str9 = r5.A0R;
        if (str9 != null) {
            hashMap.put("event_target", str9);
        }
        String str10 = r5.A0T;
        if (str10 != null) {
            hashMap.put("fi_type", str10);
        }
        Boolean bool = r5.A00;
        if (bool != null) {
            hashMap.put("has_message", bool);
        }
        String str11 = r5.A0V;
        if (str11 != null) {
            hashMap.put("media_upload_handle", str11);
        }
        String str12 = r5.A0W;
        if (str12 != null) {
            hashMap.put("model_name", str12);
        }
        String str13 = r5.A0Y;
        if (str13 != null) {
            hashMap.put("object_type", str13);
        }
        String str14 = r5.A0Z;
        if (str14 != null) {
            hashMap.put("quote_id", str14);
        }
        Long l = r5.A06;
        if (l != null) {
            hashMap.put("rank_nbr", l);
        }
        String str15 = r5.A0c;
        if (str15 != null) {
            hashMap.put("search_text", str15);
        }
        String str16 = r5.A0g;
        if (str16 != null) {
            hashMap.put("step_up_type", str16);
        }
        String str17 = r5.A0j;
        if (str17 != null) {
            hashMap.put("surface", str17);
        }
        String str18 = r5.A0n;
        if (str18 != null) {
            hashMap.put("transaction_type", str18);
        }
        String str19 = r5.A0J;
        if (str19 != null) {
            hashMap.put("biometrics_type", str19);
        }
        String str20 = r5.A0I;
        if (str20 != null) {
            hashMap.put("biometrics_prior_state", str20);
        }
        String str21 = r5.A0k;
        if (str21 != null) {
            hashMap.put("text_input_type", str21);
        }
        Boolean bool2 = r5.A02;
        if (bool2 != null) {
            hashMap.put("is_enabled", bool2);
        }
        Long l2 = r5.A09;
        if (l2 != null) {
            hashMap.put("sender_input_box", l2);
        }
        String str22 = r5.A0e;
        if (str22 != null) {
            hashMap.put("sender_trading_cur", str22);
        }
        Long l3 = r5.A0B;
        if (l3 != null) {
            hashMap.put("sender_trading_amt", l3);
        }
        String str23 = r5.A0b;
        if (str23 != null) {
            hashMap.put("receiver_trading_cur", str23);
        }
        Long l4 = r5.A08;
        if (l4 != null) {
            hashMap.put("receiver_trading_amt", l4);
        }
        String str24 = r5.A0d;
        if (str24 != null) {
            hashMap.put("sender_local_cur", str24);
        }
        Long l5 = r5.A0A;
        if (l5 != null) {
            hashMap.put("sender_local_amt", l5);
        }
        String str25 = r5.A0a;
        if (str25 != null) {
            hashMap.put("receiver_local_cur", str25);
        }
        Long l6 = r5.A07;
        if (l6 != null) {
            hashMap.put("receiver_local_amt", l6);
        }
        Long l7 = r5.A0C;
        if (l7 != null) {
            hashMap.put("trading_amt", l7);
        }
        String str26 = r5.A0l;
        if (str26 != null) {
            hashMap.put("trading_cur", str26);
        }
        String str27 = r5.A0U;
        if (str27 != null) {
            hashMap.put("local_cur", str27);
        }
        Long l8 = r5.A05;
        if (l8 != null) {
            hashMap.put("local_amt", l8);
        }
        String str28 = r5.A0E;
        if (str28 != null) {
            hashMap.put("action_model_id", str28);
        }
        String str29 = r5.A0S;
        if (str29 != null) {
            hashMap.put("fi_model_id", str29);
        }
        String str30 = r5.A0h;
        if (str30 != null) {
            hashMap.put("store_id", str30);
        }
        String str31 = r5.A0m;
        if (str31 != null) {
            hashMap.put("transaction_model_id", str31);
        }
        String str32 = r5.A0K;
        if (str32 != null) {
            hashMap.put("challenge_model_id", str32);
        }
        Boolean bool3 = r5.A01;
        if (bool3 != null) {
            hashMap.put("is_abtu", bool3);
        }
        String str33 = r5.A0M;
        if (str33 != null) {
            hashMap.put("deposit_txn_model_id", str33);
        }
        Long l9 = r5.A04;
        if (l9 != null) {
            hashMap.put("balance_top_up_trading_amt", l9);
        }
        String str34 = r5.A0H;
        if (str34 != null) {
            hashMap.put("balance_top_up_trading_cur", str34);
        }
        Long l10 = r5.A03;
        if (l10 != null) {
            hashMap.put("balance_top_up_local_amt", l10);
        }
        String str35 = r5.A0G;
        if (str35 != null) {
            hashMap.put("balance_top_up_local_cur", str35);
        }
        String str36 = r5.A0f;
        if (str36 != null) {
            hashMap.put("step_up_entry_point", str36);
        }
        String str37 = r5.A0i;
        if (str37 != null) {
            hashMap.put("sub_surface", str37);
        }
        HashMap hashMap2 = new HashMap(hashMap);
        try {
            A07(hashMap2);
            String[] A00 = C125025qY.A00(C117315Zl.A0a(A04(hashMap2)));
            C119975fP r1 = new C119975fP();
            r1.A00 = this.A0A.A00;
            r1.A01 = A00[0];
            r1.A02 = A00[1];
            r1.A03 = A00[2];
            this.A09.A07(r1);
        } catch (IOException | IllegalStateException unused) {
            Log.e("Pay: NoviEventLogger/logEvent cannot log event");
        }
    }

    public void A06(C128365vz r2) {
        if (this.A0D.A0H()) {
            A05(r2);
        } else {
            Log.e("Pay: NoviEventLogger/logNoviSessionEvent not logged in");
        }
    }

    public final void A07(Map map) {
        long nextLong;
        long j;
        if (!AnonymousClass600.A03.equals(this.A03)) {
            this.A03 = AnonymousClass600.A03;
            String A0t = C12970iu.A0t("app_flow_type", map);
            String A0t2 = C12970iu.A0t("surface", map);
            C128365vz r0 = new AnonymousClass610("RISK_PERIOD_STARTED", A0t).A00;
            r0.A0j = A0t2;
            A05(r0);
            this.A0H.set(0);
        }
        String A0t3 = C12970iu.A0t("app_flow_type", map);
        Object obj = map.get("novi_wallet_event_name");
        if ((A0t3 != null && !A0t3.equals(this.A02)) || "FLOW_SESSION_START".equals(obj)) {
            this.A02 = A0t3;
            Random random = this.A05;
            do {
                nextLong = (random.nextLong() << 1) >>> 1;
                j = nextLong % Long.MAX_VALUE;
            } while ((nextLong - j) + 9223372036854775806L < 0);
            this.A00 = j;
        }
        map.put("app_flow_session_id", Long.valueOf(this.A00));
        map.put("risk_period_uuid", AnonymousClass600.A03);
        map.put("session_sequence_nbr", Long.valueOf(this.A0H.getAndIncrement()));
        C130155yt r2 = this.A0A;
        map.put("enc_app_install_uuid", r2.A05());
        map.put("device_locale", C12970iu.A14(this.A08).toString());
        map.put("client_event_time", Long.valueOf(System.currentTimeMillis()));
        map.put("app_build", AnonymousClass00C.A00());
        map.put("app_id", Long.valueOf(Long.parseLong(AnonymousClass029.A0B)));
        map.put("device_brand", Build.BRAND);
        map.put("device_model", Build.MODEL);
        map.put("device_os", "Android");
        map.put("device_os_version", Build.VERSION.RELEASE);
        map.put("mobile_app_version", "2.22.17.70");
        map.put("user_agent", this.A0G.A00());
        String str = this.A02;
        if (str != null) {
            map.put("app_flow_type", str);
        }
        String str2 = r2.A00;
        if (str2 != null) {
            String str3 = "TEST";
            switch (str2.hashCode()) {
                case -1185824521:
                    if (str2.equals("novi.wallet_core.prod_intern")) {
                        str3 = "INTERNAL_PROD";
                        break;
                    }
                    break;
                case 79002694:
                    if (str2.equals("novi.wallet_core.rc_stable")) {
                        str3 = "INTEGRATION";
                        break;
                    }
                    break;
                case 899898772:
                    if (str2.equals("novi.wallet_core.rc")) {
                        str3 = "VALIDATION";
                        break;
                    }
                    break;
                case 1514251770:
                    if (str2.equals("novi.wallet_core.prod")) {
                        str3 = "PROD";
                        break;
                    }
                    break;
            }
            map.put("env_type", str3);
        }
        String str4 = this.A04;
        if (str4 != null) {
            map.put("novi_entry_point", str4);
        }
        String string = this.A0E.A02().getString("novi_account_id", null);
        if (string != null) {
            map.put("enc_account_model_id", string);
        }
        if ("FLOW_SESSION_END".equals(obj)) {
            this.A02 = null;
        }
    }
}
