package X;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.accessibility.AccessibilityManager;
import android.widget.FrameLayout;
import com.facebook.redex.RunnableBRunnable0Shape0S0100000_I0;
import java.util.List;

/* renamed from: X.1fu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C34291fu extends FrameLayout {
    public AbstractC15150me A00;
    public AnonymousClass5R6 A01;
    public final AccessibilityManager A02;
    public final AbstractC11760go A03;

    public C34291fu(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C50572Qb.A0D);
        if (obtainStyledAttributes.hasValue(1)) {
            AnonymousClass028.A0V(this, (float) obtainStyledAttributes.getDimensionPixelSize(1, 0));
        }
        obtainStyledAttributes.recycle();
        AccessibilityManager accessibilityManager = (AccessibilityManager) context.getSystemService("accessibility");
        this.A02 = accessibilityManager;
        C104134rs r2 = new C104134rs(this);
        this.A03 = r2;
        if (Build.VERSION.SDK_INT >= 19) {
            C05770Qw.A00(accessibilityManager, r2);
        }
        setClickableOrFocusableBasedOnAccessibility(accessibilityManager.isTouchExplorationEnabled());
    }

    @Override // android.view.View, android.view.ViewGroup
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        AnonymousClass028.A0R(this);
    }

    @Override // android.view.View, android.view.ViewGroup
    public void onDetachedFromWindow() {
        boolean z;
        AnonymousClass4PT r0;
        super.onDetachedFromWindow();
        AbstractC15150me r4 = this.A00;
        if (r4 != null) {
            C15140md r42 = (C15140md) r4;
            AbstractC15160mf r02 = r42.A00;
            C64883Hh A00 = C64883Hh.A00();
            AnonymousClass5R8 r2 = r02.A07;
            synchronized (A00.A03) {
                if (!A00.A05(r2) && ((r0 = A00.A01) == null || r2 == null || r0.A02.get() != r2)) {
                    z = false;
                }
                z = true;
            }
            if (z) {
                AbstractC15160mf.A08.post(new RunnableBRunnable0Shape0S0100000_I0(r42, 10));
            }
        }
        AccessibilityManager accessibilityManager = this.A02;
        AbstractC11760go r22 = this.A03;
        if (Build.VERSION.SDK_INT >= 19) {
            C05770Qw.A01(accessibilityManager, r22);
        }
    }

    @Override // android.widget.FrameLayout, android.view.View, android.view.ViewGroup
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        AnonymousClass5R6 r0 = this.A01;
        if (r0 != null) {
            AbstractC15160mf r2 = ((AnonymousClass51B) r0).A00;
            r2.A05.A01 = null;
            List<AccessibilityServiceInfo> enabledAccessibilityServiceList = r2.A04.getEnabledAccessibilityServiceList(1);
            if (enabledAccessibilityServiceList == null || !enabledAccessibilityServiceList.isEmpty()) {
                r2.A02();
            } else {
                r2.A00();
            }
        }
    }

    /* access modifiers changed from: private */
    public void setClickableOrFocusableBasedOnAccessibility(boolean z) {
        setClickable(!z);
        setFocusable(z);
    }

    public void setOnAttachStateChangeListener(AbstractC15150me r1) {
        this.A00 = r1;
    }

    public void setOnLayoutChangeListener(AnonymousClass5R6 r1) {
        this.A01 = r1;
    }
}
