package X;

import androidx.work.impl.WorkDatabase;
import java.util.UUID;

/* renamed from: X.0Gl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C03170Gl extends AbstractRunnableC10150e7 {
    public final /* synthetic */ AnonymousClass022 A00;
    public final /* synthetic */ UUID A01;

    public C03170Gl(AnonymousClass022 r1, UUID uuid) {
        this.A00 = r1;
        this.A01 = uuid;
    }

    /* JADX INFO: finally extract failed */
    @Override // X.AbstractRunnableC10150e7
    public void A00() {
        AnonymousClass022 r3 = this.A00;
        WorkDatabase workDatabase = r3.A04;
        workDatabase.A03();
        try {
            A01(r3, this.A01.toString());
            workDatabase.A05();
            workDatabase.A04();
            AnonymousClass0TI.A01(r3.A02, workDatabase, r3.A07);
        } catch (Throwable th) {
            workDatabase.A04();
            throw th;
        }
    }
}
