package X;

/* renamed from: X.0ao  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08240ao implements AbstractC12040hH {
    public final AnonymousClass0H9 A00;
    public final AbstractC12590iA A01;
    public final AbstractC12590iA A02;
    public final String A03;
    public final boolean A04;

    public C08240ao(AnonymousClass0H9 r1, AbstractC12590iA r2, AbstractC12590iA r3, String str, boolean z) {
        this.A03 = str;
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
        this.A04 = z;
    }

    @Override // X.AbstractC12040hH
    public AbstractC12470hy Aes(AnonymousClass0AA r2, AbstractC08070aX r3) {
        return new C08000aQ(r2, this, r3);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("RectangleShape{position=");
        sb.append(this.A01);
        sb.append(", size=");
        sb.append(this.A02);
        sb.append('}');
        return sb.toString();
    }
}
