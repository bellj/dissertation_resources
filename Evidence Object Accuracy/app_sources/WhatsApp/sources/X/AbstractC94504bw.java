package X;

import android.graphics.SurfaceTexture;
import android.view.Surface;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;

/* renamed from: X.4bw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC94504bw {
    public static final int[] A00 = {12324, 8, 12323, 8, 12322, 8, 12321, 8, 12352, 4, 12339, 1, 12344};
    public static final int[] A01 = {12324, 8, 12323, 8, 12322, 8, 12352, 4, 12344};
    public static final int[] A02 = {12324, 8, 12323, 8, 12322, 8, 12352, 4, 12610, 1, 12344};

    public int A01() {
        AnonymousClass5Pe r1 = (AnonymousClass5Pe) this;
        int[] iArr = new int[1];
        r1.A04.eglQuerySurface(r1.A02, r1.A03, 12374, iArr);
        return iArr[0];
    }

    public int A02() {
        AnonymousClass5Pe r1 = (AnonymousClass5Pe) this;
        int[] iArr = new int[1];
        r1.A04.eglQuerySurface(r1.A02, r1.A03, 12375, iArr);
        return iArr[0];
    }

    public void A03() {
        AnonymousClass5Pe r0 = (AnonymousClass5Pe) this;
        EGL10 egl10 = r0.A04;
        EGLDisplay eGLDisplay = r0.A02;
        EGLSurface eGLSurface = EGL10.EGL_NO_SURFACE;
        if (!egl10.eglMakeCurrent(eGLDisplay, eGLSurface, eGLSurface, EGL10.EGL_NO_CONTEXT)) {
            C93054Yu.A00("detachCurrent");
            throw C12990iw.A0m("eglMakeCurrent failed");
        }
    }

    public void A04() {
        AnonymousClass5Pe r4 = (AnonymousClass5Pe) this;
        r4.A0B();
        EGLSurface eGLSurface = r4.A03;
        if (eGLSurface == EGL10.EGL_NO_SURFACE) {
            throw C12990iw.A0m("No EGLSurface - can't make current");
        } else if (!r4.A04.eglMakeCurrent(r4.A02, eGLSurface, eGLSurface, r4.A01)) {
            C93054Yu.A00("makeCurrent");
            throw C12990iw.A0m("eglMakeCurrent failed");
        }
    }

    public void A05() {
        AnonymousClass5Pe r3 = (AnonymousClass5Pe) this;
        r3.A0B();
        r3.A06();
        r3.A03();
        EGL10 egl10 = r3.A04;
        egl10.eglDestroyContext(r3.A02, r3.A01);
        egl10.eglTerminate(r3.A02);
        r3.A01 = EGL10.EGL_NO_CONTEXT;
        r3.A02 = EGL10.EGL_NO_DISPLAY;
        r3.A00 = null;
    }

    public void A06() {
        AnonymousClass5Pe r3 = (AnonymousClass5Pe) this;
        EGLSurface eGLSurface = r3.A03;
        if (eGLSurface != EGL10.EGL_NO_SURFACE) {
            r3.A04.eglDestroySurface(r3.A02, eGLSurface);
            r3.A03 = EGL10.EGL_NO_SURFACE;
        }
    }

    public void A07(SurfaceTexture surfaceTexture) {
        ((AnonymousClass5Pe) this).A0C(surfaceTexture);
    }

    public void A08(Surface surface) {
        AnonymousClass5Pe r1 = (AnonymousClass5Pe) this;
        r1.A0C(new SurfaceHolderC100904mf(surface, r1));
    }

    public boolean A09() {
        return C12960it.A1X(((AnonymousClass5Pe) this).A03, EGL10.EGL_NO_SURFACE);
    }

    public boolean A0A() {
        AnonymousClass5Pe r3 = (AnonymousClass5Pe) this;
        r3.A0B();
        EGLSurface eGLSurface = r3.A03;
        if (eGLSurface != EGL10.EGL_NO_SURFACE) {
            return r3.A04.eglSwapBuffers(r3.A02, eGLSurface);
        }
        throw C12990iw.A0m("No EGLSurface - can't swap buffers");
    }
}
