package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.0VL  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0VL implements Parcelable.ClassLoaderCreator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new C06780Vb(parcel, null);
    }

    @Override // android.os.Parcelable.ClassLoaderCreator
    public Object createFromParcel(Parcel parcel, ClassLoader classLoader) {
        return new C06780Vb(parcel, classLoader);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C06780Vb[i];
    }
}
