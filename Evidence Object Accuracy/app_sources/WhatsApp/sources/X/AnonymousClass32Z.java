package X;

import com.whatsapp.group.GroupChatInfo;
import java.lang.ref.WeakReference;

/* renamed from: X.32Z  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass32Z extends AnonymousClass384 {
    public final WeakReference A00;

    public AnonymousClass32Z(C14900mE r10, C15570nT r11, C14830m7 r12, C21320xE r13, C15370n3 r14, GroupChatInfo groupChatInfo, C20660w7 r16, String str) {
        super(r10, r11, r12, r13, r14, r16, str);
        this.A00 = C12970iu.A10(groupChatInfo);
    }
}
