package X;

import com.whatsapp.R;
import com.whatsapp.authentication.AppAuthSettingsActivity;
import com.whatsapp.util.Log;

/* renamed from: X.2eA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53372eA extends AnonymousClass0PW {
    public final /* synthetic */ AppAuthSettingsActivity A00;

    public C53372eA(AppAuthSettingsActivity appAuthSettingsActivity) {
        this.A00 = appAuthSettingsActivity;
    }

    @Override // X.AnonymousClass0PW
    public void A01(int i, CharSequence charSequence) {
        if (i == 7) {
            AppAuthSettingsActivity appAuthSettingsActivity = this.A00;
            C14900mE r5 = ((ActivityC13810kN) appAuthSettingsActivity).A05;
            Object[] objArr = new Object[1];
            C12960it.A1P(objArr, 30, 0);
            r5.A0E(appAuthSettingsActivity.getString(R.string.app_auth_lockout_error_short, objArr), 1);
        }
        Log.i("AppAuthSettingsActivity/error");
        this.A00.A2e();
    }

    @Override // X.AnonymousClass0PW
    public void A02(C04700Ms r4) {
        Log.i("AppAuthSettingsActivity/success");
        AppAuthSettingsActivity appAuthSettingsActivity = this.A00;
        ((ActivityC13790kL) appAuthSettingsActivity).A03.A01(false);
        ((ActivityC13810kN) appAuthSettingsActivity).A09.A17(true);
        appAuthSettingsActivity.A0C.A07();
        appAuthSettingsActivity.A2f(true);
        appAuthSettingsActivity.A05.setChecked(true);
        appAuthSettingsActivity.A09.A01();
        ((ActivityC13790kL) appAuthSettingsActivity).A03.A00(appAuthSettingsActivity);
    }
}
