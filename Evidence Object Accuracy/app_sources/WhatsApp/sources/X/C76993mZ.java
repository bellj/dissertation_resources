package X;

import com.facebook.msys.mci.DefaultCrypto;
import java.util.Locale;

/* renamed from: X.3mZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C76993mZ extends AbstractC107514xS {
    public static final AnonymousClass5SP A01 = new AnonymousClass5SP() { // from class: X.4xU
    };
    public final AnonymousClass5SP A00;

    public static String A02(int i) {
        return i != 1 ? i != 2 ? i != 3 ? "ISO-8859-1" : DefaultCrypto.UTF_8 : "UTF-16BE" : "UTF-16";
    }

    public C76993mZ(AnonymousClass5SP r1) {
        this.A00 = r1;
    }

    public static int A00(byte[] bArr, int i, int i2) {
        int length;
        while (true) {
            length = bArr.length;
            if (i < length) {
                if (bArr[i] == 0) {
                    break;
                }
                i++;
            } else {
                i = length;
                break;
            }
        }
        if (i2 == 0 || i2 == 3) {
            return i;
        }
        while (i < length - 1) {
            if (i % 2 != 0 || bArr[i + 1] != 0) {
                while (true) {
                    i++;
                    if (i < length) {
                        if (bArr[i] == 0) {
                            break;
                        }
                    } else {
                        i = length;
                        break;
                    }
                }
            } else {
                return i;
            }
        }
        return length;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:120:0x01e6, code lost:
        if (r12 == 3) goto L_0x01e8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:136:0x0216, code lost:
        if (r10 == 67) goto L_0x0218;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x028b, code lost:
        if (r10 == 3) goto L_0x028d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:174:0x02d7, code lost:
        if (r11 == 3) goto L_0x02d9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0109, code lost:
        if (r10 == 3) goto L_0x010b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x014a, code lost:
        if (r10 == 3) goto L_0x014c;
     */
    /* JADX WARNING: Removed duplicated region for block: B:165:0x02a6  */
    /* JADX WARNING: Removed duplicated region for block: B:178:0x02ed  */
    /* JADX WARNING: Removed duplicated region for block: B:191:0x0352  */
    /* JADX WARNING: Removed duplicated region for block: B:219:0x03ca A[Catch: UnsupportedEncodingException -> 0x04c7, all -> 0x04e6, TryCatch #0 {UnsupportedEncodingException -> 0x04c7, blocks: (B:56:0x00ee, B:61:0x010c, B:71:0x012f, B:76:0x014d, B:78:0x0151, B:80:0x0155, B:82:0x0159, B:91:0x017a, B:93:0x0183, B:95:0x0187, B:97:0x018b, B:100:0x0197, B:101:0x019c, B:102:0x019e, B:111:0x01b7, B:113:0x01ca, B:115:0x01ce, B:117:0x01d2, B:122:0x01e9, B:124:0x01f5, B:125:0x01f7, B:126:0x01fe, B:137:0x0218, B:139:0x022e, B:145:0x0251, B:147:0x0255, B:149:0x0259, B:151:0x026d, B:152:0x0275, B:157:0x028e, B:160:0x0292, B:161:0x0297, B:162:0x0299, B:171:0x02b1, B:176:0x02da, B:185:0x02fb, B:187:0x0336, B:188:0x0345, B:193:0x0356, B:194:0x035b, B:196:0x035e, B:198:0x0362, B:200:0x0366, B:203:0x0388, B:206:0x0392, B:207:0x0397, B:209:0x039b, B:211:0x03a7, B:212:0x03ab, B:216:0x03c2, B:217:0x03c7, B:219:0x03ca, B:221:0x03ce, B:223:0x03d2, B:225:0x03f5, B:227:0x03fe, B:228:0x0403, B:230:0x0406, B:232:0x040a, B:234:0x040e, B:235:0x041f, B:236:0x0424, B:238:0x0428, B:240:0x0434, B:241:0x0438, B:242:0x044f, B:243:0x0460, B:245:0x046d, B:247:0x0471, B:249:0x0475, B:250:0x0484, B:253:0x048d, B:254:0x04a7), top: B:267:0x00e1, outer: #1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:227:0x03fe A[Catch: UnsupportedEncodingException -> 0x04c7, all -> 0x04e6, TryCatch #0 {UnsupportedEncodingException -> 0x04c7, blocks: (B:56:0x00ee, B:61:0x010c, B:71:0x012f, B:76:0x014d, B:78:0x0151, B:80:0x0155, B:82:0x0159, B:91:0x017a, B:93:0x0183, B:95:0x0187, B:97:0x018b, B:100:0x0197, B:101:0x019c, B:102:0x019e, B:111:0x01b7, B:113:0x01ca, B:115:0x01ce, B:117:0x01d2, B:122:0x01e9, B:124:0x01f5, B:125:0x01f7, B:126:0x01fe, B:137:0x0218, B:139:0x022e, B:145:0x0251, B:147:0x0255, B:149:0x0259, B:151:0x026d, B:152:0x0275, B:157:0x028e, B:160:0x0292, B:161:0x0297, B:162:0x0299, B:171:0x02b1, B:176:0x02da, B:185:0x02fb, B:187:0x0336, B:188:0x0345, B:193:0x0356, B:194:0x035b, B:196:0x035e, B:198:0x0362, B:200:0x0366, B:203:0x0388, B:206:0x0392, B:207:0x0397, B:209:0x039b, B:211:0x03a7, B:212:0x03ab, B:216:0x03c2, B:217:0x03c7, B:219:0x03ca, B:221:0x03ce, B:223:0x03d2, B:225:0x03f5, B:227:0x03fe, B:228:0x0403, B:230:0x0406, B:232:0x040a, B:234:0x040e, B:235:0x041f, B:236:0x0424, B:238:0x0428, B:240:0x0434, B:241:0x0438, B:242:0x044f, B:243:0x0460, B:245:0x046d, B:247:0x0471, B:249:0x0475, B:250:0x0484, B:253:0x048d, B:254:0x04a7), top: B:267:0x00e1, outer: #1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:238:0x0428 A[Catch: UnsupportedEncodingException -> 0x04c7, all -> 0x04e6, TryCatch #0 {UnsupportedEncodingException -> 0x04c7, blocks: (B:56:0x00ee, B:61:0x010c, B:71:0x012f, B:76:0x014d, B:78:0x0151, B:80:0x0155, B:82:0x0159, B:91:0x017a, B:93:0x0183, B:95:0x0187, B:97:0x018b, B:100:0x0197, B:101:0x019c, B:102:0x019e, B:111:0x01b7, B:113:0x01ca, B:115:0x01ce, B:117:0x01d2, B:122:0x01e9, B:124:0x01f5, B:125:0x01f7, B:126:0x01fe, B:137:0x0218, B:139:0x022e, B:145:0x0251, B:147:0x0255, B:149:0x0259, B:151:0x026d, B:152:0x0275, B:157:0x028e, B:160:0x0292, B:161:0x0297, B:162:0x0299, B:171:0x02b1, B:176:0x02da, B:185:0x02fb, B:187:0x0336, B:188:0x0345, B:193:0x0356, B:194:0x035b, B:196:0x035e, B:198:0x0362, B:200:0x0366, B:203:0x0388, B:206:0x0392, B:207:0x0397, B:209:0x039b, B:211:0x03a7, B:212:0x03ab, B:216:0x03c2, B:217:0x03c7, B:219:0x03ca, B:221:0x03ce, B:223:0x03d2, B:225:0x03f5, B:227:0x03fe, B:228:0x0403, B:230:0x0406, B:232:0x040a, B:234:0x040e, B:235:0x041f, B:236:0x0424, B:238:0x0428, B:240:0x0434, B:241:0x0438, B:242:0x044f, B:243:0x0460, B:245:0x046d, B:247:0x0471, B:249:0x0475, B:250:0x0484, B:253:0x048d, B:254:0x04a7), top: B:267:0x00e1, outer: #1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:245:0x046d A[Catch: UnsupportedEncodingException -> 0x04c7, all -> 0x04e6, TryCatch #0 {UnsupportedEncodingException -> 0x04c7, blocks: (B:56:0x00ee, B:61:0x010c, B:71:0x012f, B:76:0x014d, B:78:0x0151, B:80:0x0155, B:82:0x0159, B:91:0x017a, B:93:0x0183, B:95:0x0187, B:97:0x018b, B:100:0x0197, B:101:0x019c, B:102:0x019e, B:111:0x01b7, B:113:0x01ca, B:115:0x01ce, B:117:0x01d2, B:122:0x01e9, B:124:0x01f5, B:125:0x01f7, B:126:0x01fe, B:137:0x0218, B:139:0x022e, B:145:0x0251, B:147:0x0255, B:149:0x0259, B:151:0x026d, B:152:0x0275, B:157:0x028e, B:160:0x0292, B:161:0x0297, B:162:0x0299, B:171:0x02b1, B:176:0x02da, B:185:0x02fb, B:187:0x0336, B:188:0x0345, B:193:0x0356, B:194:0x035b, B:196:0x035e, B:198:0x0362, B:200:0x0366, B:203:0x0388, B:206:0x0392, B:207:0x0397, B:209:0x039b, B:211:0x03a7, B:212:0x03ab, B:216:0x03c2, B:217:0x03c7, B:219:0x03ca, B:221:0x03ce, B:223:0x03d2, B:225:0x03f5, B:227:0x03fe, B:228:0x0403, B:230:0x0406, B:232:0x040a, B:234:0x040e, B:235:0x041f, B:236:0x0424, B:238:0x0428, B:240:0x0434, B:241:0x0438, B:242:0x044f, B:243:0x0460, B:245:0x046d, B:247:0x0471, B:249:0x0475, B:250:0x0484, B:253:0x048d, B:254:0x04a7), top: B:267:0x00e1, outer: #1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:252:0x048b  */
    /* JADX WARNING: Removed duplicated region for block: B:254:0x04a7 A[Catch: UnsupportedEncodingException -> 0x04c7, all -> 0x04e6, TRY_LEAVE, TryCatch #0 {UnsupportedEncodingException -> 0x04c7, blocks: (B:56:0x00ee, B:61:0x010c, B:71:0x012f, B:76:0x014d, B:78:0x0151, B:80:0x0155, B:82:0x0159, B:91:0x017a, B:93:0x0183, B:95:0x0187, B:97:0x018b, B:100:0x0197, B:101:0x019c, B:102:0x019e, B:111:0x01b7, B:113:0x01ca, B:115:0x01ce, B:117:0x01d2, B:122:0x01e9, B:124:0x01f5, B:125:0x01f7, B:126:0x01fe, B:137:0x0218, B:139:0x022e, B:145:0x0251, B:147:0x0255, B:149:0x0259, B:151:0x026d, B:152:0x0275, B:157:0x028e, B:160:0x0292, B:161:0x0297, B:162:0x0299, B:171:0x02b1, B:176:0x02da, B:185:0x02fb, B:187:0x0336, B:188:0x0345, B:193:0x0356, B:194:0x035b, B:196:0x035e, B:198:0x0362, B:200:0x0366, B:203:0x0388, B:206:0x0392, B:207:0x0397, B:209:0x039b, B:211:0x03a7, B:212:0x03ab, B:216:0x03c2, B:217:0x03c7, B:219:0x03ca, B:221:0x03ce, B:223:0x03d2, B:225:0x03f5, B:227:0x03fe, B:228:0x0403, B:230:0x0406, B:232:0x040a, B:234:0x040e, B:235:0x041f, B:236:0x0424, B:238:0x0428, B:240:0x0434, B:241:0x0438, B:242:0x044f, B:243:0x0460, B:245:0x046d, B:247:0x0471, B:249:0x0475, B:250:0x0484, B:253:0x048d, B:254:0x04a7), top: B:267:0x00e1, outer: #1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:288:0x03d1 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:299:0x0474 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00e3  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x011e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AbstractC107404xH A01(X.AnonymousClass5SP r20, X.C95304dT r21, int r22, int r23, boolean r24) {
        /*
        // Method dump skipped, instructions count: 1259
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C76993mZ.A01(X.5SP, X.4dT, int, int, boolean):X.4xH");
    }

    public static String A03(int i, int i2, int i3, int i4, int i5) {
        Object[] objArr;
        String str;
        Locale locale = Locale.US;
        if (i == 2) {
            objArr = new Object[3];
            C12960it.A1P(objArr, i2, 0);
            C12960it.A1P(objArr, i3, 1);
            C12960it.A1P(objArr, i4, 2);
            str = "%c%c%c";
        } else {
            objArr = new Object[4];
            C12960it.A1P(objArr, i2, 0);
            C12960it.A1P(objArr, i3, 1);
            C12960it.A1P(objArr, i4, 2);
            C12960it.A1P(objArr, i5, 3);
            str = "%c%c%c%c";
        }
        return String.format(locale, str, objArr);
    }

    public static String A04(String str, byte[] bArr, int i, int i2) {
        return (i2 <= i || i2 > bArr.length) ? "" : new String(bArr, i, i2 - i, str);
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:24:0x006b */
    /* JADX DEBUG: Multi-variable search result rejected for r1v2, resolved type: boolean */
    /* JADX DEBUG: Multi-variable search result rejected for r1v8, resolved type: boolean */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0091, code lost:
        return false;
     */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0084 A[Catch: all -> 0x0092, TRY_ENTER, TryCatch #0 {all -> 0x0092, blocks: (B:3:0x0003, B:4:0x0007, B:8:0x0014, B:9:0x0021, B:32:0x0084), top: B:40:0x0003 }] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x008e A[ADDED_TO_REGION, EDGE_INSN: B:45:0x008e->B:35:0x008e ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A05(X.C95304dT r13, int r14, int r15, boolean r16) {
        /*
            int r5 = r13.A01
            goto L_0x0007
        L_0x0003:
            int r0 = (int) r3     // Catch: all -> 0x0092
            r13.A0T(r0)     // Catch: all -> 0x0092
        L_0x0007:
            int r6 = r13.A00     // Catch: all -> 0x0092
            int r0 = r13.A01     // Catch: all -> 0x0092
            int r0 = r6 - r0
            r10 = 1
            if (r0 < r15) goto L_0x0035
            r2 = 3
            r12 = 0
            if (r14 < r2) goto L_0x0021
            int r1 = r13.A07()     // Catch: all -> 0x0092
            long r3 = r13.A0I()     // Catch: all -> 0x0092
            int r7 = r13.A0F()     // Catch: all -> 0x0092
            goto L_0x002b
        L_0x0021:
            int r1 = r13.A0D()     // Catch: all -> 0x0092
            int r0 = r13.A0D()     // Catch: all -> 0x0092
            long r3 = (long) r0     // Catch: all -> 0x0092
            r7 = 0
        L_0x002b:
            r8 = 0
            if (r1 != 0) goto L_0x0039
            int r0 = (r3 > r8 ? 1 : (r3 == r8 ? 0 : -1))
            if (r0 != 0) goto L_0x0039
            if (r7 != 0) goto L_0x0039
        L_0x0035:
            r13.A0S(r5)
            return r10
        L_0x0039:
            r0 = 4
            if (r14 != r0) goto L_0x0075
            if (r16 != 0) goto L_0x0063
            r1 = 8421504(0x808080, double:4.160776E-317)
            long r1 = r1 & r3
            int r0 = (r1 > r8 ? 1 : (r1 == r8 ? 0 : -1))
            if (r0 != 0) goto L_0x008e
            r10 = 255(0xff, double:1.26E-321)
            long r8 = r3 & r10
            r0 = 8
            long r1 = r3 >> r0
            long r1 = r1 & r10
            r0 = 7
            long r1 = r1 << r0
            long r8 = r8 | r1
            r0 = 16
            long r1 = r3 >> r0
            long r1 = r1 & r10
            r0 = 14
            long r1 = r1 << r0
            long r8 = r8 | r1
            r0 = 24
            long r3 = r3 >> r0
            long r3 = r3 & r10
            r0 = 21
            long r3 = r3 << r0
            long r3 = r3 | r8
        L_0x0063:
            r0 = r7 & 64
            boolean r1 = X.C12960it.A1S(r0)
            r0 = r7 & 1
        L_0x006b:
            if (r0 == 0) goto L_0x006f
            int r1 = r1 + 4
        L_0x006f:
            long r1 = (long) r1
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x008e
            goto L_0x0084
        L_0x0075:
            if (r14 == r0) goto L_0x0063
            if (r14 != r2) goto L_0x0082
            r0 = r7 & 32
            boolean r1 = X.C12960it.A1S(r0)
            r0 = r7 & 128(0x80, float:1.794E-43)
            goto L_0x006b
        L_0x0082:
            r1 = 0
            goto L_0x006f
        L_0x0084:
            int r0 = r13.A01     // Catch: all -> 0x0092
            int r6 = r6 - r0
            long r1 = (long) r6     // Catch: all -> 0x0092
            int r0 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r0 < 0) goto L_0x008e
            goto L_0x0003
        L_0x008e:
            r13.A0S(r5)
            return r12
        L_0x0092:
            r0 = move-exception
            r13.A0S(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C76993mZ.A05(X.4dT, int, int, boolean):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x009b  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C100624mD A06(byte[] r14, int r15) {
        /*
        // Method dump skipped, instructions count: 250
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C76993mZ.A06(byte[], int):X.4mD");
    }
}
