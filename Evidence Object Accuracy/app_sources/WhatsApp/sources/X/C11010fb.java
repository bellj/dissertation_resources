package X;

import java.math.BigInteger;

/* renamed from: X.0fb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C11010fb extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ C08950c7 this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C11010fb(C08950c7 r2) {
        super(0);
        this.this$0 = r2;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        return BigInteger.valueOf((long) this.this$0.A00).shiftLeft(32).or(BigInteger.valueOf((long) this.this$0.A01)).shiftLeft(32).or(BigInteger.valueOf((long) this.this$0.A02));
    }
}
