package X;

import android.database.sqlite.SQLiteConstraintException;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0xP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21430xP {
    public final C15570nT A00;
    public final C15550nR A01;
    public final C18780t0 A02;
    public final C21370xJ A03;
    public final C14830m7 A04;
    public final C15990oG A05;
    public final C18240s8 A06;
    public final C20650w6 A07;
    public final C16510p9 A08;
    public final C19990v2 A09;
    public final C15650ng A0A;
    public final C21410xN A0B;
    public final C15600nX A0C;
    public final C21380xK A0D;
    public final C21390xL A0E;
    public final C18470sV A0F;
    public final C21400xM A0G;
    public final C21420xO A0H;
    public final C14850m9 A0I;
    public final C20330va A0J;
    public final C20160vJ A0K;
    public final AbstractC14440lR A0L;

    public C21430xP(C15570nT r2, C15550nR r3, C18780t0 r4, C21370xJ r5, C14830m7 r6, C15990oG r7, C18240s8 r8, C20650w6 r9, C16510p9 r10, C19990v2 r11, C15650ng r12, C21410xN r13, C15600nX r14, C21380xK r15, C21390xL r16, C18470sV r17, C21400xM r18, C21420xO r19, C14850m9 r20, C20330va r21, C20160vJ r22, AbstractC14440lR r23) {
        this.A04 = r6;
        this.A0I = r20;
        this.A08 = r10;
        this.A0L = r23;
        this.A00 = r2;
        this.A09 = r11;
        this.A0F = r17;
        this.A07 = r9;
        this.A03 = r5;
        this.A0D = r15;
        this.A06 = r8;
        this.A01 = r3;
        this.A0A = r12;
        this.A0K = r22;
        this.A0E = r16;
        this.A05 = r7;
        this.A0G = r18;
        this.A0J = r21;
        this.A0B = r13;
        this.A02 = r4;
        this.A0C = r14;
        this.A0H = r19;
    }

    public static final int A00(AnonymousClass1PB r2) {
        AnonymousClass1PK r0;
        int i = r2.A03;
        if (i == 0 || i != 1) {
            r0 = AnonymousClass1PK.A02;
        } else {
            r0 = AnonymousClass1PK.A01;
        }
        switch (r0.ordinal()) {
            case 0:
                return 2;
            case 1:
                return 0;
            default:
                return 1;
        }
    }

    public List A01(List list) {
        AbstractC15340mz r0;
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            AnonymousClass1G6 r1 = ((C40761sH) it.next()).A03;
            if (r1 == null) {
                r1 = AnonymousClass1G6.A0k;
            }
            AbstractC15340mz r2 = null;
            try {
                AnonymousClass1sN A01 = this.A0K.A01(r1);
                if (!(A01 == null || (r0 = A01.A00) == null)) {
                    r2 = r0;
                }
            } catch (Exception e) {
                Log.e("HistorySyncChunkProcessor/ failed to parse wmi of history sync message", e);
            }
            if (r2 != null && A03(r2)) {
                arrayList.add(r2);
            }
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:296:? */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:267:0x05dd */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX DEBUG: Type inference failed for r18v0. Raw type applied. Possible types: java.util.Iterator<E>, java.util.Iterator */
    /* JADX DEBUG: Type inference failed for r6v2. Raw type applied. Possible types: java.util.Iterator<E>, java.util.Iterator */
    /* JADX DEBUG: Type inference failed for r14v3. Raw type applied. Possible types: E */
    /* JADX DEBUG: Type inference failed for r11v15. Raw type applied. Possible types: java.util.Iterator<E>, java.util.Iterator */
    /* JADX WARN: Type inference failed for: r3v1, types: [X.1gg] */
    /* JADX WARN: Type inference failed for: r3v2, types: [X.0mz] */
    /* JADX WARN: Type inference failed for: r0v26, types: [X.0ye] */
    /* JADX WARN: Type inference failed for: r3v3, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r3v4 */
    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    public void A02(X.AnonymousClass1PX r45, X.AnonymousClass1PW r46, java.io.File r47) {
        /*
        // Method dump skipped, instructions count: 1890
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C21430xP.A02(X.1PX, X.1PW, java.io.File):void");
    }

    public final boolean A03(AbstractC15340mz r10) {
        C40481rf r0;
        try {
            r10.A0T(65536);
            C21390xL r8 = this.A0E;
            long A01 = r8.A01("next_sort_id_for_companion_history_sync", -2);
            r10.A12 = A01;
            long j = A01 - 1;
            boolean z = false;
            if (j < r8.A01("next_sort_id_for_companion_history_sync", -2)) {
                z = true;
            }
            AnonymousClass009.A0B("The next sort_id for a historical message should be negative and smaller than our last used sort_id.", z);
            r8.A05("next_sort_id_for_companion_history_sync", j);
            if (r10.A04 > 0) {
                this.A0B.A01(r10, r10.A0I);
            }
            this.A0A.A0T(r10);
            if ((r10.A07 & 1) == 1 && (r0 = r10.A0V) != null) {
                for (AnonymousClass1Iv r1 : r0.A02()) {
                    r1.A0Y(17);
                    this.A0G.A00(r1, false);
                }
            }
            return true;
        } catch (SQLiteConstraintException unused) {
            Log.w("HistorySyncChunkProcessor/ tried to insert duplicate message");
            return false;
        } catch (Exception e) {
            Log.e("HistorySyncChunkProcessor/ could not insert message into db", e);
            return false;
        }
    }
}
