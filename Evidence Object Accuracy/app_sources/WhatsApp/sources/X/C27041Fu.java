package X;

import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import javax.crypto.NoSuchPaddingException;
import org.json.JSONObject;

/* renamed from: X.1Fu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C27041Fu implements AbstractC15840nz {
    public final C15570nT A00;
    public final C15820nx A01;
    public final AnonymousClass1D6 A02;
    public final C15810nw A03;
    public final C17050qB A04;
    public final C16590pI A05;
    public final C14820m6 A06;
    public final C19490uC A07;
    public final C21780xy A08;
    public final C16600pJ A09;
    public final AnonymousClass15D A0A;

    @Override // X.AbstractC15840nz
    public String AAp() {
        return "backup-settings";
    }

    public C27041Fu(C15570nT r1, C15820nx r2, AnonymousClass1D6 r3, C15810nw r4, C17050qB r5, C16590pI r6, C14820m6 r7, C19490uC r8, C21780xy r9, C16600pJ r10, AnonymousClass15D r11) {
        this.A05 = r6;
        this.A0A = r11;
        this.A00 = r1;
        this.A03 = r4;
        this.A07 = r8;
        this.A01 = r2;
        this.A04 = r5;
        this.A02 = r3;
        this.A06 = r7;
        this.A09 = r10;
        this.A08 = r9;
    }

    @Override // X.AbstractC15840nz
    public boolean A6K() {
        EnumC16570pG r5;
        Throwable e;
        String str;
        int i;
        AbstractC33251dh A00;
        C15820nx r10 = this.A01;
        if (r10.A04()) {
            r5 = EnumC16570pG.A07;
        } else {
            r5 = EnumC16570pG.A06;
        }
        synchronized (this) {
            C21780xy r13 = this.A08;
            C27421Hj r0 = r13.A00;
            r0.A01();
            File file = new File(r0.A03, "backup_settings.json");
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                try {
                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream, AnonymousClass01V.A08);
                    HashMap hashMap = new HashMap();
                    C14820m6 r7 = this.A06;
                    hashMap.put("backupFrequency", Integer.valueOf(r7.A01()));
                    hashMap.put("backupNetworkSettings", Integer.valueOf(r7.A02()));
                    hashMap.put("includeVideosInBackup", Boolean.valueOf(r7.A00.getBoolean("gdrive_include_videos_in_backup", false)));
                    hashMap.put("localSettings", r7.A0E());
                    AnonymousClass1D6 r72 = this.A02;
                    if (!r72.A08.A07(932) || r72.A05.A00.getInt("gdrive_backup_quota_warning_visibility", 0) != 0) {
                        i = r72.A05.A00.getInt("gdrive_backup_quota_warning_visibility", 0);
                    } else {
                        i = 2;
                    }
                    long A01 = r72.A01();
                    hashMap.put("backupQuotaWarningVisibility", Integer.valueOf(i));
                    hashMap.put("backupQuotaUserNoticePeriodEndDate", Long.valueOf(A01));
                    outputStreamWriter.write(new JSONObject(hashMap).toString(2));
                    outputStreamWriter.close();
                    fileOutputStream.close();
                    try {
                        File A02 = this.A03.A02();
                        StringBuilder sb = new StringBuilder("backup_settings.json.crypt");
                        sb.append(r5.version);
                        File file2 = new File(A02, sb.toString());
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("backup_settings/backup/to ");
                        sb2.append(file2);
                        Log.i(sb2.toString());
                        AnonymousClass15D r1 = this.A0A;
                        C15570nT r73 = this.A00;
                        C19490uC r12 = this.A07;
                        A00 = C33231df.A00(r73, new C33211dd(file2), null, r10, this.A04, r12, r13, this.A09, r5, r1);
                    } catch (IOException | InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException e2) {
                        e = e2;
                        str = "backup_settings/backup failed";
                        Log.e(str, e);
                        return false;
                    }
                } catch (Throwable th) {
                    try {
                        fileOutputStream.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            } catch (Exception e3) {
                e = e3;
                StringBuilder sb3 = new StringBuilder();
                sb3.append("backup_settings/backup/exception while writing to temp file");
                sb3.append(file);
                str = sb3.toString();
            }
            if (!A00.A04(this.A05.A00)) {
                Log.w("backup_settings/backup/prepare for backup failed");
                return false;
            }
            A00.A03(null, file);
            return true;
        }
    }
}
