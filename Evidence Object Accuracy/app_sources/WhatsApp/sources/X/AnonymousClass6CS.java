package X;

/* renamed from: X.6CS  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6CS implements AbstractC116085Ub {
    public final C126105sL A00;

    public AnonymousClass6CS(C126105sL r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC116085Ub
    public boolean Adb(AnonymousClass1IR r3) {
        AbstractC130195yx A00 = this.A00.A00.A00(r3.A03);
        A00.A06(r3);
        return A00.A03;
    }
}
