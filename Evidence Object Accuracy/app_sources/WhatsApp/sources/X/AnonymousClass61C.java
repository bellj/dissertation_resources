package X;

import com.whatsapp.util.Log;
import java.security.KeyPair;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.61C  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass61C {
    public final C20920wX A00;
    public final C15570nT A01;
    public final C14830m7 A02;
    public final C130155yt A03;
    public final C130125yq A04;
    public final AnonymousClass61E A05;

    public AnonymousClass61C(C20920wX r1, C15570nT r2, C14830m7 r3, C130155yt r4, C130125yq r5, AnonymousClass61E r6) {
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = r1;
        this.A03 = r4;
        this.A04 = r5;
        this.A05 = r6;
    }

    public static JSONObject A00(AnonymousClass6F2 r3, AnonymousClass6F2 r4, String str, String str2, String str3, String str4, String str5, long j) {
        JSONObject A0a = C117295Zj.A0a();
        try {
            C117295Zj.A1L(str4, str5, A0a, j);
            A0a.put("client_idempotency_key", str);
            A0a.put("financial_instrument_id", str2);
            A0a.put("trading_currency", ((AbstractC30781Yu) C130325zE.A01(r3, "trading_amount", A0a)).A04);
            A0a.put("local_currency", ((AbstractC30781Yu) C130325zE.A01(r4, "local_amount", A0a)).A04);
            A0a.put("quote_id", str3);
            return A0a;
        } catch (JSONException unused) {
            Log.e("PAY: IntentPayloadHelper/getDepositIntentPayload/toJson can't construct json");
            return A0a;
        }
    }

    public static final void A01(String str, JSONObject jSONObject) {
        try {
            jSONObject.put("client_idempotency_key", str);
        } catch (JSONException unused) {
            Log.e("PAY: SignedIntentPayloadManager/addClientIdempotencyKey/toJson can't construct json");
        }
    }

    public C129585xx A02(long j) {
        C130125yq r3 = this.A04;
        C127385uP A01 = r3.A01();
        AnonymousClass009.A05(A01);
        JSONObject A04 = A04(j);
        try {
            JSONObject A0a = C117295Zj.A0a();
            int i = A01.A02 & 65535;
            A04.put("encryption_key_registration", A0a.put("key_id", new String(new byte[]{(byte) (i >> 8), (byte) i})).put("key_type", "X25519").put("pub_key_b64", C117305Zk.A0n(A01.A01.A02.A01)));
        } catch (JSONException unused) {
            Log.e("PAY: SignedIntentPayloadManager/getEncryptionKeyRegistrationSessionSignedIntent/toJson can't construct json");
        }
        return new C129585xx(r3, "REGISTER_ENCRYPTION_KEY", A04);
    }

    public C129585xx A03(String str, String str2, String str3, long j) {
        JSONObject A04 = A04(j);
        A05(A04);
        C130125yq r3 = this.A04;
        KeyPair A02 = r3.A02();
        AnonymousClass009.A05(A02);
        try {
            A04.put("signing_key_registration", C117295Zj.A0a().put("key_type", "ECDSA_SECP256R1").put("pub_key_b64", C117305Zk.A0n(A02.getPublic().getEncoded())));
        } catch (JSONException unused) {
            Log.e("PAY: SignedIntentPayloadManager/addSigningKeyRegistration/toJson can't construct json");
        }
        try {
            A04.put("encrypted_password_b64", str2);
        } catch (JSONException unused2) {
            Log.e("PAY: SignedIntentPayloadManager/addEncryptedPassword/toJson can't construct json");
        }
        A01(str3, A04);
        return new C129585xx(r3, str, A04);
    }

    public final JSONObject A04(long j) {
        JSONObject A0a = C117295Zj.A0a();
        try {
            A0a.put("risk_period_uuid", AnonymousClass600.A03);
            A0a.put("app_install_uuid", this.A03.A05());
            A0a.put("client_timestamp_ms", j);
            return A0a;
        } catch (JSONException unused) {
            Log.e("PAY: SignedIntentPayloadManager/getBaseSignIntentPayload/toJson can't construct json");
            return A0a;
        }
    }

    public final void A05(JSONObject jSONObject) {
        JSONObject jSONObject2;
        C126905td r4 = new C126905td(this.A00, this.A01.A03());
        try {
            try {
                jSONObject2 = C117295Zj.A0a().put("country_code", r4.A00).put("national_number", r4.A01);
            } catch (JSONException unused) {
                Log.e("PAY: PhoneNumberPayload/toJson can't construct json");
                jSONObject2 = null;
            }
            jSONObject.put("phone", jSONObject2);
        } catch (JSONException unused2) {
            Log.e("PAY: SignedIntentPayloadManager/addPhonePayload/toJson can't construct json");
        }
    }
}
