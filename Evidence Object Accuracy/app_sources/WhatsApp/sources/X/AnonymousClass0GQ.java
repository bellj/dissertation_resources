package X;

import android.os.Build;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0GQ  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0GQ extends AbstractC004301y {
    public AnonymousClass0GQ(Class cls, TimeUnit timeUnit, TimeUnit timeUnit2) {
        super(cls);
        C004401z r6 = this.A00;
        long millis = timeUnit.toMillis(43200000);
        long millis2 = timeUnit2.toMillis(21600000);
        if (millis < 900000) {
            C06390Tk.A00().A05(C004401z.A0J, String.format("Interval duration lesser than minimum allowed value; Changed to %s", 900000L), new Throwable[0]);
            millis = 900000;
        }
        if (millis2 < 300000) {
            C06390Tk.A00().A05(C004401z.A0J, String.format("Flex duration lesser than minimum allowed value; Changed to %s", 300000L), new Throwable[0]);
            millis2 = 300000;
        }
        if (millis2 > millis) {
            C06390Tk.A00().A05(C004401z.A0J, String.format("Flex duration greater than interval duration; Changed to %s", Long.valueOf(millis)), new Throwable[0]);
            millis2 = millis;
        }
        r6.A04 = millis;
        r6.A02 = millis2;
    }

    @Override // X.AbstractC004301y
    public /* bridge */ /* synthetic */ AnonymousClass020 A01() {
        if (!this.A03 || Build.VERSION.SDK_INT < 23 || !this.A00.A09.A04()) {
            return new AnonymousClass0GR(this);
        }
        throw new IllegalArgumentException("Cannot set backoff criteria on an idle mode job");
    }
}
