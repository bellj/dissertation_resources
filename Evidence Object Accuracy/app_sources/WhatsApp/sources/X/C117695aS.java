package X;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import com.whatsapp.CircleWaImageView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaTextView;
import com.whatsapp.components.Button;

/* renamed from: X.5aS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117695aS extends LinearLayout implements AbstractC136376Mh, AnonymousClass004 {
    public int A00;
    public CircleWaImageView A01;
    public TextEmojiLabel A02;
    public TextEmojiLabel A03;
    public TextEmojiLabel A04;
    public WaTextView A05;
    public Button A06;
    public C21270x9 A07;
    public AnonymousClass1IR A08;
    public C128205vj A09;
    public AbstractC130195yx A0A;
    public AnonymousClass2P7 A0B;
    public boolean A0C;

    public C117695aS(Context context) {
        super(context);
        if (!this.A0C) {
            this.A0C = true;
            this.A07 = C12970iu.A0W(AnonymousClass2P6.A00(generatedComponent()));
        }
        A00();
    }

    public C117695aS(Context context, int i) {
        super(context);
        if (!this.A0C) {
            this.A0C = true;
            this.A07 = C12970iu.A0W(AnonymousClass2P6.A00(generatedComponent()));
        }
        this.A00 = i;
        A00();
    }

    public void A00() {
        C117305Zk.A14(C12960it.A0E(this), this, R.layout.novi_hub_payment_row);
        setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        setBackground(AnonymousClass00T.A04(getContext(), R.drawable.selector_orange_gradient));
        this.A01 = (CircleWaImageView) AnonymousClass028.A0D(this, R.id.transaction_icon);
        this.A04 = C12970iu.A0T(this, R.id.transaction_title);
        this.A05 = C12960it.A0N(this, R.id.transaction_subtitle);
        this.A02 = C12970iu.A0T(this, R.id.transaction_amount);
        this.A03 = C12970iu.A0T(this, R.id.transaction_amount_secondary);
        this.A06 = (Button) AnonymousClass028.A0D(this, R.id.view_code_button);
        C42941w9.A04(this.A04);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0036, code lost:
        if ((r2 instanceof X.C123755no) == false) goto L_0x0038;
     */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00aa  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00ce  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00ed  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0108  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0113  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x012d  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x018d  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x01a9  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x01b3  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x01bd  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x01d0  */
    /* JADX WARNING: Removed duplicated region for block: B:58:? A[RETURN, SYNTHETIC] */
    /* renamed from: A01 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A6W(X.C128205vj r12) {
        /*
        // Method dump skipped, instructions count: 483
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C117695aS.A6W(X.5vj):void");
    }

    public void A02(boolean z) {
        AnonymousClass028.A0D(this, R.id.divider).setVisibility(C12960it.A02(z ? 1 : 0));
    }

    @Override // X.AbstractC136376Mh
    public void AaB() {
        C128205vj r0 = this.A09;
        if (r0 != null) {
            A6W(r0);
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0B;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A0B = r0;
        }
        return r0.generatedComponent();
    }

    public int getLayoutId() {
        return R.layout.novi_hub_payment_row;
    }

    public void setTransactionRowListener(View.OnClickListener onClickListener) {
        setOnClickListener(onClickListener);
    }
}
