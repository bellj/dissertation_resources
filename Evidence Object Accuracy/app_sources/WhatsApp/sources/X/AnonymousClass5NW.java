package X;

import java.util.Iterator;

/* renamed from: X.5NW  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5NW extends AbstractC114775Na {
    public byte[] A00;

    public AnonymousClass5NW(byte[] bArr) {
        this.A00 = bArr;
    }

    @Override // X.AbstractC114775Na, X.AnonymousClass1TL
    public synchronized AnonymousClass1TL A06() {
        A0E();
        return super.A06();
    }

    @Override // X.AbstractC114775Na, X.AnonymousClass1TL, X.AnonymousClass1TM
    public synchronized int hashCode() {
        A0E();
        return super.hashCode();
    }

    @Override // X.AbstractC114775Na, java.lang.Iterable
    public synchronized Iterator iterator() {
        A0E();
        return super.iterator();
    }

    @Override // X.AnonymousClass1TL
    public synchronized int A05() {
        int i;
        byte[] bArr = this.A00;
        if (bArr != null) {
            int length = bArr.length;
            i = AnonymousClass1TQ.A00(length) + 1 + length;
        } else {
            i = new AnonymousClass5NY(super.A00).A05();
        }
        return i;
    }

    public final void A0E() {
        byte[] bArr = this.A00;
        if (bArr != null) {
            C94954co r2 = new C94954co(10);
            AnonymousClass5D4 r1 = new AnonymousClass5D4(bArr);
            while (r1.hasMoreElements()) {
                r2.A06((AnonymousClass1TM) r1.nextElement());
            }
            super.A00 = r2.A07();
            this.A00 = null;
        }
    }
}
