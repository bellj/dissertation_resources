package X;

import android.content.Context;

/* renamed from: X.0Fn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02960Fn extends AnonymousClass0OL {
    public final Context A00;

    public C02960Fn(Context context, int i, int i2) {
        super(i, i2);
        this.A00 = context;
    }

    @Override // X.AnonymousClass0OL
    public void A00(AbstractC12920im r7) {
        if (super.A00 >= 10) {
            ((AnonymousClass0ZE) r7).A00.execSQL("INSERT OR REPLACE INTO `Preference` (`key`, `long_value`) VALUES (@key, @long_value)", new Object[]{"reschedule_needed", 1});
        } else {
            this.A00.getSharedPreferences("androidx.work.util.preferences", 0).edit().putBoolean("reschedule_needed", true).apply();
        }
    }
}
