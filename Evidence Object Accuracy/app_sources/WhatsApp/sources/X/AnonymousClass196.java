package X;

/* renamed from: X.196  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass196 {
    public final C15450nH A00;
    public final C18790t3 A01;
    public final C17170qN A02;
    public final C231710q A03;
    public final C231610p A04;
    public final C18810t5 A05;
    public final C18800t4 A06;

    public AnonymousClass196(C15450nH r1, C18790t3 r2, C17170qN r3, C231710q r4, C231610p r5, C18810t5 r6, C18800t4 r7) {
        this.A01 = r2;
        this.A00 = r1;
        this.A04 = r5;
        this.A06 = r7;
        this.A05 = r6;
        this.A03 = r4;
        this.A02 = r3;
    }
}
