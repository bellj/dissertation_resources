package X;

import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.4V6  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4V6 {
    public int A00;

    public AnonymousClass4V6(int i) {
        this.A00 = i;
    }

    public JSONObject A00() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("attempts", this.A00);
        } catch (JSONException unused) {
        }
        return jSONObject;
    }
}
