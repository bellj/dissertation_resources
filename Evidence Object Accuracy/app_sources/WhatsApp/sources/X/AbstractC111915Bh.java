package X;

import java.io.Serializable;
import java.util.Iterator;

/* renamed from: X.5Bh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC111915Bh implements Iterable, Serializable {
    public static final AbstractC111915Bh A00 = new C79243qH(C93094Zb.A04);
    public static final AbstractC115595Se A01 = ((AnonymousClass4ZU.A00 == null || AnonymousClass4ZU.A01) ? new C108534zF() : new C108544zG());
    public int zzfk = 0;

    @Override // java.lang.Object
    public abstract boolean equals(Object obj);

    @Override // java.lang.Iterable
    public /* synthetic */ Iterator iterator() {
        return new AnonymousClass5DC(this);
    }

    public static int A00(int i, int i2, int i3) {
        int i4 = i2 - i;
        if ((i | i2 | i4 | (i3 - i2)) >= 0) {
            return i4;
        }
        if (i < 0) {
            StringBuilder A0t = C12980iv.A0t(32);
            A0t.append("Beginning index: ");
            A0t.append(i);
            throw new IndexOutOfBoundsException(C12960it.A0d(" < 0", A0t));
        } else if (i2 < i) {
            StringBuilder A0t2 = C12980iv.A0t(66);
            A0t2.append("Beginning index larger than ending index: ");
            A0t2.append(i);
            throw new IndexOutOfBoundsException(C12960it.A0e(", ", A0t2, i2));
        } else {
            StringBuilder A0t3 = C12980iv.A0t(37);
            A0t3.append("End index: ");
            A0t3.append(i2);
            throw new IndexOutOfBoundsException(C12960it.A0e(" >= ", A0t3, i3));
        }
    }

    public byte A01(int i) {
        C79243qH r3 = (C79243qH) this;
        if (!(r3 instanceof C79233qG)) {
            return r3.zzfp[i];
        }
        C79233qG r32 = (C79233qG) r3;
        int i2 = r32.zzfn;
        if (((i2 - (i + 1)) | i) >= 0) {
            return r32.zzfp[r32.zzfm + i];
        }
        if (i < 0) {
            throw new ArrayIndexOutOfBoundsException(C12960it.A0e("Index < 0: ", C12980iv.A0t(22), i));
        }
        StringBuilder A0t = C12980iv.A0t(40);
        A0t.append("Index > length: ");
        A0t.append(i);
        throw new ArrayIndexOutOfBoundsException(C12960it.A0e(", ", A0t, i2));
    }

    public int A02() {
        C79243qH r1 = (C79243qH) this;
        if (!(r1 instanceof C79233qG)) {
            return r1.zzfp.length;
        }
        return ((C79233qG) r1).zzfn;
    }

    public void A03(AnonymousClass4UO r4) {
        C79243qH r0 = (C79243qH) this;
        ((AbstractC79223qF) r4).A0D(r0.zzfp, r0.A04(), r0.A02());
    }

    @Override // java.lang.Object
    public final int hashCode() {
        int i = this.zzfk;
        if (i == 0) {
            int A02 = A02();
            C79243qH r0 = (C79243qH) this;
            byte[] bArr = r0.zzfp;
            int A04 = r0.A04();
            i = A02;
            for (int i2 = A04; i2 < A04 + A02; i2++) {
                i = (i * 31) + bArr[i2];
            }
            if (i == 0) {
                i = 1;
            }
            this.zzfk = i;
        }
        return i;
    }

    @Override // java.lang.Object
    public final String toString() {
        Object[] A1a = C12980iv.A1a();
        A1a[0] = C72453ed.A0o(this);
        C12980iv.A1T(A1a, A02());
        return String.format("<ByteString@%s size=%d>", A1a);
    }
}
