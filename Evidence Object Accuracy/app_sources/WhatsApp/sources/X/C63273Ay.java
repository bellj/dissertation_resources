package X;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.res.Resources;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.Map;

/* renamed from: X.3Ay  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63273Ay {
    public static AnonymousClass04S A00(Activity activity, DialogInterface.OnCancelListener onCancelListener, DialogInterface.OnClickListener onClickListener, DialogInterface.OnClickListener onClickListener2, C15610nY r16, C15370n3 r17, AnonymousClass19M r18, ArrayList arrayList, Map map) {
        Resources resources;
        int i;
        String A0o;
        Resources resources2;
        int i2;
        C15370n3 r4 = r17;
        if (map != null && map.size() == 1) {
            r4 = C12970iu.A0a(C12960it.A0o(map));
            map.clear();
        }
        if (map != null && !map.isEmpty()) {
            String A0G = r16.A0G(map.values(), 3, -1, false, true);
            if (arrayList == null || arrayList.size() <= 1) {
                A0o = C12990iw.A0o(activity.getResources(), A0G, new Object[1], 0, R.string.confirm_sharing_title);
            } else {
                Resources resources3 = activity.getResources();
                int size = arrayList.size();
                Object[] objArr = new Object[2];
                C12960it.A1P(objArr, arrayList.size(), 0);
                objArr[1] = A0G;
                A0o = resources3.getQuantityString(R.plurals.confirm_sharing_multiple_title, size, objArr);
            }
        } else if (r4 == null || !r4.A0K()) {
            if (arrayList == null || arrayList.size() <= 1) {
                resources = activity.getResources();
                i = R.string.confirm_sharing_title;
                A0o = C12990iw.A0o(resources, r16.A04(r4), new Object[1], 0, i);
            } else {
                resources2 = activity.getResources();
                i2 = R.plurals.confirm_sharing_multiple_title;
                int size2 = arrayList.size();
                Object[] objArr2 = new Object[2];
                C12960it.A1P(objArr2, arrayList.size(), 0);
                objArr2[1] = r16.A04(r4);
                A0o = resources2.getQuantityString(i2, size2, objArr2);
            }
        } else if (arrayList == null || arrayList.size() <= 1) {
            resources = activity.getResources();
            i = R.string.group_confirm_sharing_title;
            A0o = C12990iw.A0o(resources, r16.A04(r4), new Object[1], 0, i);
        } else {
            resources2 = activity.getResources();
            i2 = R.plurals.group_confirm_sharing_multiple_title;
            int size2 = arrayList.size();
            Object[] objArr2 = new Object[2];
            C12960it.A1P(objArr2, arrayList.size(), 0);
            objArr2[1] = r16.A04(r4);
            A0o = resources2.getQuantityString(i2, size2, objArr2);
        }
        C004802e A0S = C12980iv.A0S(activity);
        A0S.A0A(AbstractC36671kL.A05(activity, r18, A0o));
        A0S.A0B(true);
        A0S.setNegativeButton(R.string.cancel, onClickListener2);
        A0S.setPositiveButton(R.string.ok, onClickListener);
        A0S.A08(onCancelListener);
        return A0S.create();
    }
}
