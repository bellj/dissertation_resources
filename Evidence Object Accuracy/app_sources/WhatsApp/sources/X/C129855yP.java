package X;

/* renamed from: X.5yP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C129855yP {
    public final int A00;
    public final AnonymousClass6ML A01;
    public final C460124c A02;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C129855yP) {
                C129855yP r5 = (C129855yP) obj;
                if (this.A00 != r5.A00 || !C16700pc.A0O(this.A02, r5.A02) || !C16700pc.A0O(this.A01, r5.A01)) {
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (((this.A00 * 31) + this.A02.hashCode()) * 31) + this.A01.hashCode();
    }

    public C129855yP(AnonymousClass6ML r2, C460124c r3, int i) {
        C16700pc.A0E(r3, 2);
        this.A00 = i;
        this.A02 = r3;
        this.A01 = r2;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("AlertBannerConfiguration(count=");
        A0k.append(this.A00);
        A0k.append(", alert=");
        A0k.append(this.A02);
        A0k.append(", onAlertClickListener=");
        A0k.append(this.A01);
        return C12970iu.A0u(A0k);
    }
}
