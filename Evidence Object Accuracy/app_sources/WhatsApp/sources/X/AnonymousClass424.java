package X;

import com.whatsapp.blocklist.BlockList;
import com.whatsapp.jid.UserJid;
import java.util.Collection;

/* renamed from: X.424  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass424 extends C27131Gd {
    public final /* synthetic */ BlockList A00;

    public AnonymousClass424(BlockList blockList) {
        this.A00 = blockList;
    }

    @Override // X.C27131Gd
    public void A00(AbstractC14640lm r2) {
        BlockList.A02(this.A00);
    }

    @Override // X.C27131Gd
    public void A02(UserJid userJid) {
        BlockList.A02(this.A00);
    }

    @Override // X.C27131Gd
    public void A03(UserJid userJid) {
        BlockList.A02(this.A00);
    }

    @Override // X.C27131Gd
    public void A04(Collection collection) {
        BlockList blockList = this.A00;
        BlockList.A02(blockList);
        blockList.A2h();
    }

    @Override // X.C27131Gd
    public void A06(Collection collection) {
        BlockList.A02(this.A00);
    }
}
