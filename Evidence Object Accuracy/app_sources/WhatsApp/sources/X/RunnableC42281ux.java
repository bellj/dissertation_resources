package X;

import com.facebook.redex.EmptyBaseRunnable0;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1ux  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class RunnableC42281ux extends EmptyBaseRunnable0 implements Runnable {
    public final C42251uu A00;
    public final /* synthetic */ AnonymousClass1DP A01;

    public RunnableC42281ux(AnonymousClass1DP r1, C42251uu r2) {
        this.A01 = r1;
        this.A00 = r2;
    }

    public final void A00(C42351v4 r7) {
        ArrayList arrayList = new ArrayList();
        C42251uu r4 = this.A00;
        List<C42701vg> list = r4.A03;
        for (C42701vg r1 : list) {
            if (!r1.A00) {
                arrayList.add(r1);
                r1.A01(r7);
            }
        }
        list.removeAll(arrayList);
        if (!list.isEmpty()) {
            r4.A01 = false;
            this.A01.A01(r4);
        }
    }

    public final void A01(C42351v4 r3) {
        for (AnonymousClass1VC r0 : this.A00.A03) {
            r0.A01(r3);
        }
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:166:0x049e */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:840:0x05f1 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:827:0x0611 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:925:0x1731 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r12v2 */
    /* JADX WARN: Type inference failed for: r12v3, types: [java.util.Collection] */
    /* JADX WARN: Type inference failed for: r12v4, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r0v105, types: [X.0nR] */
    /* JADX WARN: Type inference failed for: r10v3, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r10v6, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r10v20, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r10v21, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r10v22, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r10v23, types: [java.util.List] */
    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    @Override // java.lang.Runnable
    public void run() {
        /*
        // Method dump skipped, instructions count: 6587
        */
        throw new UnsupportedOperationException("Method not decompiled: X.RunnableC42281ux.run():void");
    }
}
