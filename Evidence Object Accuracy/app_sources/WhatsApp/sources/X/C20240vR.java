package X;

/* renamed from: X.0vR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20240vR implements AbstractC20200vN {
    public final C20220vP A00;
    public final C20230vQ A01;

    public C20240vR(C20220vP r1, C20230vQ r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC20200vN
    public void ANK() {
        this.A00.A07();
        this.A01.A04(true);
    }
}
