package X;

import android.graphics.Point;
import com.whatsapp.calling.callgrid.view.CallGrid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.CallInfo;
import com.whatsapp.voipcalling.VideoPort;
import com.whatsapp.voipcalling.Voip;

/* renamed from: X.3W9  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3W9 implements AbstractC116665Wi {
    public final /* synthetic */ CallGrid A00;

    public AnonymousClass3W9(CallGrid callGrid) {
        this.A00 = callGrid;
    }

    @Override // X.AbstractC116665Wi
    public void AWM(C64363Fg r6, VideoPort videoPort) {
        CallInfo A06;
        C48782Ht r4 = this.A00.A05.A0C;
        UserJid userJid = r6.A0S;
        boolean z = r6.A0G;
        CallInfo callInfo = r4.A04;
        if (callInfo != null && !callInfo.isGroupCall() && callInfo.videoEnabled && (A06 = r4.A06(null)) != null && !A06.videoEnabled) {
            Log.w("voip/CallDatasource/setVideoPort: we are not in the video call");
        } else if (z) {
            r4.A0A(videoPort);
            r4.A05 = videoPort;
        } else {
            if (Voip.setVideoDisplayPort(userJid.getRawString(), videoPort) == 0) {
                Voip.startVideoRenderStream(userJid.getRawString());
            } else {
                C29631Ua r2 = r4.A03;
                if (r2 != null) {
                    r2.A0m(null, null, 22);
                }
            }
            r4.A0D.put(userJid, Integer.valueOf(videoPort.hashCode()));
        }
    }

    @Override // X.AbstractC116665Wi
    public void AWq(C64363Fg r6, VideoPort videoPort) {
        C48782Ht r4 = this.A00.A05.A0C;
        UserJid userJid = r6.A0S;
        if (r6.A0G) {
            r4.A0B.removeCameraErrorListener(r4.A0A);
            r4.A0A(null);
            r4.A05 = null;
        } else if (C29941Vi.A00(Integer.valueOf(videoPort.hashCode()), r4.A0D.get(userJid))) {
            Voip.stopVideoRenderStream(userJid.getRawString());
            Voip.setVideoDisplayPort(userJid.getRawString(), null);
        }
    }

    @Override // X.AbstractC116665Wi
    public void AYX(C64363Fg r4, VideoPort videoPort) {
        AnonymousClass1S6 infoByJid;
        C48782Ht r2 = this.A00.A05.A0C;
        UserJid userJid = r4.A0S;
        CallInfo A06 = r2.A06(null);
        if (A06 != null && (infoByJid = A06.getInfoByJid(userJid)) != null) {
            if (infoByJid.A0F) {
                Point windowSize = videoPort.getWindowSize();
                Voip.setVideoPreviewSize(windowSize.x, windowSize.y);
                return;
            }
            Voip.setVideoDisplayPort(userJid.getRawString(), videoPort);
        }
    }
}
