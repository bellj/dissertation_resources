package X;

import java.io.IOException;

/* renamed from: X.1TL  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1TL extends AnonymousClass1TM {
    public static AnonymousClass1TL A03(byte[] bArr) {
        AnonymousClass1TO r0 = new AnonymousClass1TO(bArr);
        try {
            AnonymousClass1TL A05 = r0.A05();
            if (r0.available() == 0) {
                return A05;
            }
            throw new IOException("Extra data detected in stream");
        } catch (ClassCastException unused) {
            throw new IOException("cannot recognise object in stream");
        }
    }

    public final boolean A04(AnonymousClass1TL r3) {
        return this == r3 || A0A(r3);
    }

    public AnonymousClass1TL A06() {
        return this;
    }

    public AnonymousClass1TL A07() {
        return this;
    }

    @Override // X.AnonymousClass1TM
    public final boolean equals(Object obj) {
        return this == obj || ((obj instanceof AnonymousClass1TN) && A0A(((AnonymousClass1TN) obj).Aer()));
    }

    @Override // X.AnonymousClass1TM
    public abstract int hashCode();

    public static AnonymousClass1TK A02(String str, AnonymousClass1TK r2) {
        return new AnonymousClass1TK(str, r2);
    }

    public int A05() {
        int length = ((AnonymousClass1TK) this).A0C().length;
        return AnonymousClass1TQ.A00(length) + 1 + length;
    }

    public void A08(AnonymousClass1TP r3, boolean z) {
        r3.A06(((AnonymousClass1TK) this).A0C(), 6, z);
    }

    public boolean A09() {
        return false;
    }

    public boolean A0A(AnonymousClass1TL r3) {
        AnonymousClass1TK r1 = (AnonymousClass1TK) this;
        if (r3 == r1) {
            return true;
        }
        if (!(r3 instanceof AnonymousClass1TK)) {
            return false;
        }
        return r1.A01.equals(((AnonymousClass1TK) r3).A01);
    }
}
