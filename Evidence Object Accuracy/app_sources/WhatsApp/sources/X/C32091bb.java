package X;

/* renamed from: X.1bb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C32091bb {
    public C32101bc A00;

    public C32091bb(C31491ad r6, AbstractC31681aw r7, byte[] bArr, int i, int i2) {
        C466726t r1 = (C466726t) AnonymousClass25E.A03.A0T();
        r1.A05(i2);
        r1.A06(AbstractC27881Jp.A01(bArr, 0, bArr.length));
        AnonymousClass25E r4 = (AnonymousClass25E) r1.A02();
        AnonymousClass1G4 A0T = C31471ab.A03.A0T();
        byte[] A00 = r6.A00();
        AbstractC27881Jp A01 = AbstractC27881Jp.A01(A00, 0, A00.length);
        A0T.A03();
        C31471ab r12 = (C31471ab) A0T.A00;
        r12.A00 |= 1;
        r12.A02 = A01;
        if (r7 instanceof C31691ax) {
            byte[] bArr2 = ((C31721b0) r7.A00()).A00;
            AbstractC27881Jp A012 = AbstractC27881Jp.A01(bArr2, 0, bArr2.length);
            A0T.A03();
            C31471ab r13 = (C31471ab) A0T.A00;
            r13.A00 |= 2;
            r13.A01 = A012;
        }
        AnonymousClass1G4 A0T2 = C32101bc.A05.A0T();
        A0T2.A03();
        C32101bc r14 = (C32101bc) A0T2.A00;
        r14.A00 |= 1;
        r14.A01 = i;
        A0T2.A03();
        C32101bc r15 = (C32101bc) A0T2.A00;
        r15.A03 = r4;
        r15.A00 |= 2;
        A0T2.A03();
        C32101bc r16 = (C32101bc) A0T2.A00;
        r16.A04 = (C31471ab) A0T.A02();
        r16.A00 |= 4;
        this.A00 = (C32101bc) A0T2.A02();
    }

    public C32091bb(C32101bc r1) {
        this.A00 = r1;
    }

    public AnonymousClass3H8 A00() {
        AnonymousClass25E r0 = this.A00.A03;
        if (r0 == null) {
            r0 = AnonymousClass25E.A03;
        }
        return new AnonymousClass3H8(r0.A01, r0.A02.A04());
    }

    public void A01(AnonymousClass3H8 r5) {
        C466726t r3 = (C466726t) AnonymousClass25E.A03.A0T();
        r3.A05(r5.A00);
        byte[] bArr = r5.A01;
        r3.A06(AbstractC27881Jp.A01(bArr, 0, bArr.length));
        AnonymousClass1G4 A0T = this.A00.A0T();
        A0T.A03();
        C32101bc r1 = (C32101bc) A0T.A00;
        r1.A03 = (AnonymousClass25E) r3.A02();
        r1.A00 |= 2;
        this.A00 = (C32101bc) A0T.A02();
    }
}
