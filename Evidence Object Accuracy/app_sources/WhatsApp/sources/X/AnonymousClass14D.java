package X;

import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Base64;
import java.security.SecureRandom;

/* renamed from: X.14D  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass14D {
    public final byte[] A00;

    public AnonymousClass14D(C14820m6 r6) {
        byte[] bArr;
        SharedPreferences sharedPreferences = r6.A00;
        String string = sharedPreferences.getString("upload_token_random_bytes", null);
        if (TextUtils.isEmpty(string) || (bArr = Base64.decode(string, 3)) == null) {
            bArr = new byte[32];
            new SecureRandom().nextBytes(bArr);
            sharedPreferences.edit().putString("upload_token_random_bytes", Base64.encodeToString(bArr, 3)).apply();
        }
        this.A00 = bArr;
    }
}
