package X;

import android.content.Context;
import android.view.View;
import com.whatsapp.R;

/* renamed from: X.2LL  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2LL extends View {
    public AnonymousClass2LL(Context context) {
        super(context);
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, View.MeasureSpec.makeMeasureSpec(getContext().getResources().getDimensionPixelSize(R.dimen.tab_height), 1073741824));
    }
}
