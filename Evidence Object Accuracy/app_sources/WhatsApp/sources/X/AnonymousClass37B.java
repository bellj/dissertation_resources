package X;

import com.whatsapp.biz.catalog.view.CatalogHeader;
import java.lang.ref.WeakReference;

/* renamed from: X.37B  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass37B extends AbstractC16350or {
    public final AnonymousClass131 A00;
    public final C15370n3 A01;
    public final WeakReference A02;

    public /* synthetic */ AnonymousClass37B(CatalogHeader catalogHeader, AnonymousClass131 r3, C15370n3 r4) {
        this.A01 = r4;
        this.A00 = r3;
        this.A02 = C12970iu.A10(catalogHeader);
    }
}
