package X;

import com.whatsapp.util.Log;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.6DK  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6DK implements AbstractC21730xt {
    public final /* synthetic */ AbstractC128705wX A00;
    public final /* synthetic */ AbstractC128495wC A01;

    public AnonymousClass6DK(AbstractC128705wX r1, AbstractC128495wC r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        Log.e("Bloks : Reply node not found");
        AnonymousClass3EB r1 = this.A01.A00;
        Log.e("FBUserEntityManagement : Network failed while sending the payload");
        r1.A00.AOy();
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r5, String str) {
        Log.e("Bloks : Reply node not found");
        this.A00.A00.AaV("EncryptionProtocolHelper failed with a server error", null, false);
        AbstractC128495wC r0 = this.A01;
        r0.A00.A01(new C47732Ce(r5, str));
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r15, String str) {
        byte[] bArr;
        byte[] bArr2;
        byte[] bArr3;
        Exception e;
        AnonymousClass3EB r0;
        Long l;
        String A0G;
        AnonymousClass1V8 A0F = r15.A0F("encryption_metadata");
        AnonymousClass1V8 A0F2 = A0F.A0F("encrypted_key");
        AnonymousClass1V8 A0F3 = A0F.A0F("nonce");
        AnonymousClass1V8 A0F4 = A0F.A0F("encrypted_data");
        AnonymousClass1V8 A0F5 = A0F.A0F("auth_tag");
        byte[] bArr4 = A0F2.A01;
        if (bArr4 == null || (bArr = A0F3.A01) == null || (bArr2 = A0F4.A01) == null || (bArr3 = A0F5.A01) == null) {
            throw new AnonymousClass1V9("encryption_metadata inner node data missing");
        }
        C91334Ri r4 = new C91334Ri(bArr4, bArr2, bArr3, bArr);
        AnonymousClass1V8 A0E = r15.A0E("tos_id");
        if (!(A0E == null || (A0G = A0E.A0G()) == null)) {
            Integer.parseInt(A0G);
        }
        AbstractC128495wC r02 = this.A01;
        if (r02 instanceof C119925fK) {
            C119925fK r03 = (C119925fK) r02;
            try {
                JSONObject A05 = C13000ix.A05(r03.A02.A01.A01(r4, r03.A03.getPrivate()));
                AnonymousClass3EB r3 = r03.A01;
                C64063Ec r42 = r03.A00;
                Object obj = r42.A03.A00;
                AnonymousClass009.A05(obj);
                r3.A00(new C64063Ec(r42.A01, r42.A05, C117315Zl.A0K(r42.A04), C13000ix.A05(A05.getString("data")).getString("access_token"), r42.A06, C12980iv.A0G(obj), r42.A00));
                return;
            } catch (UnsupportedEncodingException | GeneralSecurityException | JSONException e2) {
                e = e2;
                r0 = r03.A01;
            }
        } else if (!(r02 instanceof C119905fI)) {
            C119915fJ r04 = (C119915fJ) r02;
            try {
                AnonymousClass68O r32 = r04.A01;
                JSONObject A052 = C13000ix.A05(r32.A01.A01(r4, r04.A03.getPrivate()));
                String str2 = r04.A02;
                C16820po r43 = r32.A03;
                JSONObject A053 = C13000ix.A05(A052.getString("data"));
                long j = A053.getLong("fbid");
                String string = A053.getString("access_token");
                long j2 = A053.getLong("timestamp");
                String str3 = null;
                if (A053.has("ttl")) {
                    l = Long.valueOf(A053.optLong("ttl"));
                } else {
                    l = null;
                }
                if (A053.has("analytics_claim")) {
                    str3 = A053.optString("analytics_claim");
                }
                r04.A00.A00(new C64063Ec(r43, l, str2, string, str3, j, j2));
                return;
            } catch (UnsupportedEncodingException | GeneralSecurityException | JSONException e3) {
                e = e3;
                r0 = r04.A00;
            }
        } else {
            ((C119905fI) r02).A00.A00(null);
            return;
        }
        r0.A01(e);
    }
}
