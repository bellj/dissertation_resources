package X;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiBankAccountPickerActivity;
import java.util.List;

/* renamed from: X.5by  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118535by extends AnonymousClass02M {
    public final C125835rt A00;
    public final List A01;
    public final /* synthetic */ IndiaUpiBankAccountPickerActivity A02;

    public C118535by(C125835rt r1, IndiaUpiBankAccountPickerActivity indiaUpiBankAccountPickerActivity, List list) {
        this.A02 = indiaUpiBankAccountPickerActivity;
        this.A01 = list;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A01.size();
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r13, int i) {
        Drawable drawable;
        View$OnClickListenerC118875cW r132 = (View$OnClickListenerC118875cW) r13;
        List list = this.A01;
        C128025vR r5 = (C128025vR) list.get(i);
        IndiaUpiBankAccountPickerActivity indiaUpiBankAccountPickerActivity = this.A02;
        if (!TextUtils.isEmpty(indiaUpiBankAccountPickerActivity.A0R)) {
            indiaUpiBankAccountPickerActivity.A0Q.A00(indiaUpiBankAccountPickerActivity.getResources().getDrawable(R.drawable.bank_logo_placeholder_with_circle_bg), null, r132.A00, null, indiaUpiBankAccountPickerActivity.A0R);
        } else {
            r132.A00.setImageResource(R.drawable.bank_logo_placeholder_with_circle_bg);
        }
        int size = list.size();
        RadioButton radioButton = r132.A01;
        if (size == 1) {
            radioButton.setVisibility(8);
        } else {
            radioButton.setVisibility(0);
        }
        TextView textView = r132.A03;
        String str = r5.A02;
        String str2 = r5.A03;
        StringBuilder A0j = C12960it.A0j(str);
        C117325Zm.A09(A0j);
        textView.setText(C12960it.A0d(str2, A0j));
        radioButton.setChecked(r5.A00);
        r132.A04.setText(r5.A04);
        boolean z = !r5.A05;
        View view = r132.A0H;
        Context context = view.getContext();
        if (z) {
            C12960it.A0s(context, textView, R.color.list_item_title);
            r132.A02.setText(r5.A01);
            radioButton.setEnabled(true);
        } else {
            C12960it.A0s(context, textView, R.color.text_disabled);
            r132.A02.setText(R.string.payments_add_bank_account_already_added);
            radioButton.setEnabled(false);
        }
        if (indiaUpiBankAccountPickerActivity.A0V || !z) {
            drawable = null;
        } else {
            drawable = AnonymousClass00T.A04(view.getContext(), R.drawable.selector_orange_gradient);
        }
        view.setBackground(drawable);
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        return new View$OnClickListenerC118875cW(C12960it.A0F(this.A02.getLayoutInflater(), viewGroup, R.layout.india_upi_account_picker_list_row), this.A00);
    }
}
