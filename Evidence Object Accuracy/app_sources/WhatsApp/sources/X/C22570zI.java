package X;

/* renamed from: X.0zI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22570zI {
    public final C15570nT A00;
    public final C18790t3 A01;
    public final C16590pI A02;
    public final C14820m6 A03;
    public final AnonymousClass018 A04;
    public final C20340vb A05;
    public final C18810t5 A06;
    public final AbstractC17860rW A07;
    public final C18800t4 A08;
    public final AbstractC14440lR A09;

    public C22570zI(C15570nT r1, C18790t3 r2, C16590pI r3, C14820m6 r4, AnonymousClass018 r5, C20340vb r6, C18810t5 r7, AbstractC17860rW r8, C18800t4 r9, AbstractC14440lR r10) {
        this.A02 = r3;
        this.A09 = r10;
        this.A00 = r1;
        this.A01 = r2;
        this.A04 = r5;
        this.A08 = r9;
        this.A06 = r7;
        this.A03 = r4;
        this.A05 = r6;
        this.A07 = r8;
    }

    public void A00(AnonymousClass2DH r12, String str) {
        C16590pI r3 = this.A02;
        C15570nT r1 = this.A00;
        AbstractC14440lR r10 = this.A09;
        C18790t3 r2 = this.A01;
        AnonymousClass018 r5 = this.A04;
        C18800t4 r9 = this.A08;
        new C61342zv(r1, r2, r3, this.A03, r5, this.A05, this.A06, this.A07, r9, r10).A0A(r12, str);
    }
}
