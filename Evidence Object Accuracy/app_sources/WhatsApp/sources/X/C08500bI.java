package X;

import android.graphics.Path;

/* renamed from: X.0bI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08500bI implements AbstractC12670iJ {
    public float A00;
    public float A01;
    public Path A02 = new Path();
    public final /* synthetic */ C06540Ua A03;

    public C08500bI(C08520bK r2, C06540Ua r3) {
        this.A03 = r3;
        if (r2 != null) {
            r2.A02(this);
        }
    }

    @Override // X.AbstractC12670iJ
    public void A69(float f, float f2, float f3, float f4, float f5, boolean z, boolean z2) {
        C06540Ua.A04(this, this.A00, this.A01, f, f2, f3, f4, f5, z, z2);
        this.A00 = f4;
        this.A01 = f5;
    }

    @Override // X.AbstractC12670iJ
    public void A8d(float f, float f2, float f3, float f4, float f5, float f6) {
        this.A02.cubicTo(f, f2, f3, f4, f5, f6);
        this.A00 = f5;
        this.A01 = f6;
    }

    @Override // X.AbstractC12670iJ
    public void AKS(float f, float f2) {
        this.A02.lineTo(f, f2);
        this.A00 = f;
        this.A01 = f2;
    }

    @Override // X.AbstractC12670iJ
    public void ALW(float f, float f2) {
        this.A02.moveTo(f, f2);
        this.A00 = f;
        this.A01 = f2;
    }

    @Override // X.AbstractC12670iJ
    public void AZh(float f, float f2, float f3, float f4) {
        this.A02.quadTo(f, f2, f3, f4);
        this.A00 = f3;
        this.A01 = f4;
    }

    @Override // X.AbstractC12670iJ
    public void close() {
        this.A02.close();
    }
}
