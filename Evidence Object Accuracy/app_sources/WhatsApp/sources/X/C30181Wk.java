package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.1Wk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30181Wk implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C99694ki();
    public final String A00;
    public final String A01;
    public final String A02;
    public final String A03;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C30181Wk(Parcel parcel) {
        String readString = parcel.readString();
        AnonymousClass009.A05(readString);
        this.A00 = readString;
        this.A02 = parcel.readString();
        this.A01 = parcel.readString();
        this.A03 = parcel.readString();
    }

    public C30181Wk(String str, String str2) {
        this.A03 = str;
        this.A00 = str2;
        this.A02 = null;
        this.A01 = null;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (!(obj instanceof C30181Wk)) {
            return false;
        }
        C30181Wk r4 = (C30181Wk) obj;
        if (!C29941Vi.A00(this.A00, r4.A00) || !C29941Vi.A00(this.A02, r4.A02) || !C29941Vi.A00(this.A01, r4.A01) || !C29941Vi.A00(this.A03, r4.A03)) {
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        int i;
        int i2;
        int i3;
        String str = this.A00;
        int i4 = 0;
        if (str != null) {
            i = str.hashCode();
        } else {
            i = 0;
        }
        int i5 = i * 31;
        String str2 = this.A02;
        if (str2 != null) {
            i2 = str2.hashCode();
        } else {
            i2 = 0;
        }
        int i6 = (i5 + i2) * 31;
        String str3 = this.A01;
        if (str3 != null) {
            i3 = str3.hashCode();
        } else {
            i3 = 0;
        }
        int i7 = (i6 + i3) * 31;
        String str4 = this.A03;
        if (str4 != null) {
            i4 = str4.hashCode();
        }
        return i7 + i4;
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("CoverPhoto:{'id'='");
        sb.append(this.A00);
        sb.append("', 'ts'='");
        sb.append(this.A02);
        sb.append("', 'token'='");
        sb.append(this.A01);
        sb.append("', 'url'='");
        sb.append(this.A03);
        sb.append("'}");
        return sb.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A00);
        parcel.writeString(this.A02);
        parcel.writeString(this.A01);
        parcel.writeString(this.A03);
    }
}
