package X;

import X.C14830m7;
import X.C15570nT;
import X.C236412l;
import X.C29631Ua;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Point;
import android.hardware.SensorEventListener;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.SoundPool;
import android.media.audiofx.AcousticEchoCanceler;
import android.media.audiofx.AutomaticGainControl;
import android.media.audiofx.NoiseSuppressor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.os.SystemClock;
import android.os.Vibrator;
import android.service.notification.StatusBarNotification;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.redex.RunnableBRunnable0Shape0S0000000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0110000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S1110000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S2000000_I0;
import com.facebook.redex.RunnableBRunnable0Shape1S0200000_I0_1;
import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;
import com.whatsapp.R;
import com.whatsapp.calling.service.VoiceService$DefaultSignalingXmppCallback;
import com.whatsapp.calling.service.VoiceService$VoiceServiceEventCallback;
import com.whatsapp.fieldstats.events.WamCall;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.protocol.VoipStanzaChildNode;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.CallInfo;
import com.whatsapp.voipcalling.DefaultCryptoCallback;
import com.whatsapp.voipcalling.GlVideoRenderer;
import com.whatsapp.voipcalling.JNIUtils;
import com.whatsapp.voipcalling.SignalingXmppCallback;
import com.whatsapp.voipcalling.VoiceFGService;
import com.whatsapp.voipcalling.Voip;
import com.whatsapp.voipcalling.VoipActivityV2;
import com.whatsapp.voipcalling.VoipErrorDialogFragment;
import com.whatsapp.voipcalling.camera.VoipCameraManager;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import org.chromium.net.UrlRequest;

/* renamed from: X.1Ua */
/* loaded from: classes2.dex */
public class C29631Ua {
    public static String A2k;
    public static final ThreadPoolExecutor A2l = new ThreadPoolExecutor(0, 1, 10, TimeUnit.SECONDS, new LinkedBlockingQueue());
    public static final AtomicInteger A2m = new AtomicInteger();
    public double A00 = Double.NaN;
    public double A01 = Double.NaN;
    public int A02;
    public int A03 = 0;
    public int A04 = 0;
    public int A05;
    public int A06 = 30;
    public int A07 = 0;
    public int A08;
    public long A09;
    public long A0A;
    public long A0B;
    public long A0C = -1;
    public long A0D;
    public long A0E;
    public BroadcastReceiver A0F;
    public BroadcastReceiver A0G;
    public Ringtone A0H;
    public SoundPool A0I;
    public Uri A0J;
    public Handler A0K;
    public Handler A0L;
    public Handler A0M;
    public Handler A0N;
    public PowerManager.WakeLock A0O;
    public PowerManager.WakeLock A0P;
    public PhoneStateListener A0Q;
    public AnonymousClass108 A0R;
    public C48782Ht A0S;
    public C89414Jw A0T;
    public C90984Pz A0U;
    public C90984Pz A0V;
    public C90984Pz A0W;
    public WamCall A0X;
    public C91044Qf A0Y;
    public AnonymousClass379 A0Z;
    public AnonymousClass2CS A0a;
    public AnonymousClass2ON A0b;
    public AnonymousClass1L4 A0c;
    public Boolean A0d = null;
    public Integer A0e;
    public Integer A0f;
    public Integer A0g;
    public Integer A0h;
    public Integer A0i = 1500;
    public Integer A0j;
    public Integer A0k;
    public Integer A0l;
    public Integer A0m;
    public Long A0n;
    public Long A0o;
    public Long A0p;
    public Long A0q;
    public Object A0r;
    public Object A0s;
    public Object A0t;
    public String A0u;
    public String A0v;
    public String A0w;
    public ScheduledExecutorService A0x;
    public short A0y;
    public boolean A0z;
    public boolean A10;
    public boolean A11;
    public boolean A12;
    public boolean A13;
    public boolean A14 = false;
    public boolean A15;
    public boolean A16;
    public boolean A17;
    public boolean A18;
    public boolean A19 = false;
    public boolean A1A;
    public boolean A1B;
    public boolean A1C;
    public boolean A1D;
    public boolean A1E;
    public boolean A1F;
    public boolean A1G;
    public boolean A1H = false;
    public boolean A1I;
    public boolean A1J;
    public boolean A1K;
    public boolean A1L = true;
    public boolean A1M;
    public long[] A1N;
    public final int A1O;
    public final Context A1P;
    public final TelephonyManager A1Q;
    public final C16210od A1R;
    public final C236912q A1S;
    public final AbstractC15710nm A1T;
    public final C14330lG A1U;
    public final C14900mE A1V;
    public final C15570nT A1W;
    public final C15450nH A1X;
    public final C18790t3 A1Y;
    public final C16240og A1Z;
    public final C22880zn A1a;
    public final C19380u1 A1b;
    public final C236212j A1c;
    public final C236512m A1d;
    public final C89424Jx A1e = new C89424Jx(this);
    public final VoiceService$VoiceServiceEventCallback A1f;
    public final C20970wc A1g;
    public final C237412v A1h;
    public final C15550nR A1i;
    public final C15610nY A1j;
    public final C20720wD A1k;
    public final C20020v5 A1l;
    public final C18640sm A1m;
    public final AnonymousClass01d A1n;
    public final C14830m7 A1o;
    public final C18360sK A1p;
    public final C15890o4 A1q;
    public final C14820m6 A1r;
    public final AnonymousClass018 A1s;
    public final C15990oG A1t;
    public final C18240s8 A1u;
    public final C236612n A1v;
    public final C18750sx A1w;
    public final C236812p A1x;
    public final C21250x7 A1y;
    public final C18770sz A1z;
    public final C14850m9 A20;
    public final C22050yP A21;
    public final C20710wC A22;
    public final AnonymousClass107 A23;
    public final C20660w7 A24;
    public final C22230yk A25;
    public final C17230qT A26;
    public final C22630zO A27;
    public final C22280yp A28;
    public final C22850zk A29;
    public final C22270yo A2A;
    public final AbstractC44971zr A2B = new C69563Zs(this);
    public final C21820y2 A2C;
    public final C15860o1 A2D;
    public final AbstractC14440lR A2E;
    public final AnonymousClass3D1 A2F;
    public final C237012r A2G;
    public final C21260x8 A2H;
    public final AnonymousClass0t1 A2I;
    public final C236412l A2J;
    public final C64263Ew A2K;
    public final JNIUtils A2L;
    public final C237512w A2M;
    public final C237612x A2N;
    public final C237212t A2O;
    public final AnonymousClass2Nu A2P;
    public final C21280xA A2Q;
    public final C17140qK A2R;
    public final C17150qL A2S;
    public final VoipCameraManager A2T;
    public final C236312k A2U;
    public final AnonymousClass01H A2V;
    public final List A2W = new ArrayList();
    public final Map A2X = new HashMap();
    public final Map A2Y = new HashMap();
    public final Map A2Z = new ConcurrentHashMap();
    public final Map A2a = new ConcurrentHashMap();
    public final Map A2b = new HashMap();
    public final Set A2c = new HashSet();
    public final Set A2d = Collections.synchronizedSet(new HashSet());
    public volatile int A2e = 0;
    public volatile AnonymousClass3DL A2f;
    public volatile Boolean A2g;
    public volatile Integer A2h;
    public volatile String A2i;
    public volatile boolean A2j;

    public C29631Ua(Context context, C16210od r34, C236912q r35, AbstractC15710nm r36, C14330lG r37, C14900mE r38, C22820zh r39, C15570nT r40, C22920zr r41, AnonymousClass101 r42, C15450nH r43, C18790t3 r44, C16240og r45, C22880zn r46, C19380u1 r47, C18280sC r48, C236212j r49, C236512m r50, C20970wc r51, C237412v r52, C15550nR r53, C15610nY r54, C20720wD r55, C20020v5 r56, C237112s r57, C18640sm r58, AnonymousClass01d r59, C14830m7 r60, C18360sK r61, C15890o4 r62, C14820m6 r63, AnonymousClass018 r64, C15990oG r65, C18240s8 r66, C236612n r67, C21540xa r68, C17650rA r69, C18750sx r70, C236812p r71, C21250x7 r72, C237312u r73, C22130yZ r74, C18770sz r75, C14850m9 r76, C22050yP r77, C16120oU r78, C20710wC r79, C19950uw r80, AnonymousClass107 r81, C21510xX r82, C20660w7 r83, C22230yk r84, C17230qT r85, C22630zO r86, C22280yp r87, C22850zk r88, C22270yo r89, C21820y2 r90, C15860o1 r91, AbstractC14440lR r92, C237012r r93, C21260x8 r94, AnonymousClass0t1 r95, C236412l r96, JNIUtils jNIUtils, C237512w r98, C237612x r99, C237212t r100, C21280xA r101, C17140qK r102, C17150qL r103, VoipCameraManager voipCameraManager, C236312k r105, AnonymousClass01H r106) {
        this.A1P = context;
        this.A1g = r51;
        this.A1o = r60;
        this.A20 = r76;
        this.A1V = r38;
        this.A2L = jNIUtils;
        this.A1T = r36;
        this.A1W = r40;
        this.A2E = r92;
        this.A1U = r37;
        this.A1Y = r44;
        this.A24 = r83;
        this.A1X = r43;
        this.A1y = r72;
        this.A2H = r94;
        this.A1a = r46;
        this.A2Q = r101;
        this.A2U = r105;
        this.A1b = r47;
        this.A1i = r53;
        this.A1u = r66;
        this.A1n = r59;
        this.A1j = r54;
        this.A25 = r84;
        this.A1s = r64;
        this.A2J = r96;
        this.A2A = r89;
        this.A28 = r87;
        this.A22 = r79;
        this.A1Z = r45;
        this.A1w = r70;
        this.A1l = r56;
        this.A2D = r91;
        this.A1k = r55;
        this.A21 = r77;
        this.A27 = r86;
        this.A1t = r65;
        this.A1z = r75;
        this.A2T = voipCameraManager;
        this.A1v = r67;
        this.A26 = r85;
        this.A1q = r62;
        this.A1r = r63;
        this.A1x = r71;
        this.A2S = r103;
        this.A2R = r102;
        this.A1p = r61;
        this.A1S = r35;
        this.A2G = r93;
        this.A23 = r81;
        this.A2C = r90;
        this.A2I = r95;
        this.A2O = r100;
        this.A1R = r34;
        this.A1m = r58;
        this.A1h = r52;
        this.A2N = r99;
        this.A1f = new VoiceService$VoiceServiceEventCallback(this, r80);
        this.A29 = r88;
        this.A2F = new AnonymousClass3D1(r48);
        this.A1Q = r59.A0N();
        this.A2K = new C64263Ew(r39, r41, r42, new AnonymousClass3CL(this), r55, r57, r60, r65, r66, (C21490xV) ((AnonymousClass01J) AnonymousClass027.A00(AnonymousClass01J.class, r69.A00.A00)).A56.get(), r68, r69, r73, r74, r76, r78, r82);
        this.A2P = new AnonymousClass2Nu(context, r35, this, r59, r63, r76, r99);
        this.A2M = r98;
        this.A1O = r76.A02(870);
        this.A1d = r50;
        this.A2V = r106;
        this.A1c = r49;
    }

    public static final int A00(CallInfo callInfo) {
        switch (callInfo.callResult) {
            case 1:
                return callInfo.isEndedByMe ? 2 : 3;
            case 2:
                return 6;
            case 3:
            case 7:
            case 11:
            case 12:
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
            case 15:
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
            case 17:
                return 8;
            case 4:
            case 8:
            case 9:
                return 5;
            case 5:
                if (callInfo.isCaller) {
                    return 7;
                }
                return 5;
            case 6:
            case 10:
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
            case 19:
                return 1;
            case 18:
                return 4;
            case C43951xu.A01 /* 20 */:
                return 11;
            default:
                return 9;
        }
    }

    public static void A01(Bundle bundle, DeviceJid deviceJid, Jid jid, String str, String str2, long j) {
        bundle.putString("id", str);
        bundle.putParcelable("jid", jid);
        bundle.putParcelable("callCreatorJid", deviceJid);
        bundle.putString("callId", str2);
        bundle.putLong("loggableStanzaId", j);
    }

    public static /* synthetic */ void A03(C29631Ua r14, DeviceJid deviceJid, boolean z) {
        VoipStanzaChildNode[] voipStanzaChildNodeArr;
        String currentCallId = Voip.getCurrentCallId();
        AnonymousClass3DL r5 = r14.A2f;
        if (r5 != null) {
            String str = r5.A04;
            if (str.equals(currentCallId) && r5.A06.containsKey(deviceJid)) {
                r14.A2f = null;
                VoipStanzaChildNode voipStanzaChildNode = r5.A02;
                if (voipStanzaChildNode != null) {
                    ArrayList arrayList = new ArrayList();
                    VoipStanzaChildNode[] childrenCopy = voipStanzaChildNode.getChildrenCopy();
                    AnonymousClass009.A05(childrenCopy);
                    for (VoipStanzaChildNode voipStanzaChildNode2 : childrenCopy) {
                        AnonymousClass1W9[] attributesCopy = voipStanzaChildNode2.getAttributesCopy();
                        AnonymousClass009.A05(attributesCopy);
                        int length = attributesCopy.length;
                        int i = 0;
                        while (true) {
                            if (i < length) {
                                AnonymousClass1W9 r1 = attributesCopy[i];
                                if ("jid".equals(r1.A02)) {
                                    DeviceJid of = DeviceJid.of(r1.A01);
                                    if (of != null && !of.equals(deviceJid)) {
                                        arrayList.add(voipStanzaChildNode2);
                                    }
                                } else {
                                    i++;
                                }
                            }
                        }
                    }
                    if (!arrayList.isEmpty() && (voipStanzaChildNodeArr = (VoipStanzaChildNode[]) arrayList.toArray(new VoipStanzaChildNode[0])) != null) {
                        r14.A0q(new AnonymousClass3DL(r5.A01, AnonymousClass1SF.A06(r5.A03, voipStanzaChildNodeArr), str, null));
                    }
                }
            }
        }
        ScheduledExecutorService scheduledExecutorService = r14.A0x;
        if (scheduledExecutorService != null) {
            try {
                scheduledExecutorService.execute(new RunnableBRunnable0Shape0S0110000_I0(deviceJid, 2, z));
            } catch (RejectedExecutionException unused) {
                Log.w("voip/handleDeviceBecomesInvalid: executor shutdown");
            }
        }
    }

    public static /* synthetic */ void A05(C29631Ua r3, List list, int i) {
        VoipErrorDialogFragment A02;
        CallInfo callInfo = Voip.getCallInfo();
        if (i == 1) {
            if (callInfo != null) {
                r3.A0P();
                A02 = VoipErrorDialogFragment.A02(list, callInfo.participants.size(), false);
            }
            A02 = null;
        } else if (i == 2) {
            if (callInfo != null) {
                r3.A0P();
                A02 = VoipErrorDialogFragment.A02(list, callInfo.participants.size(), true);
            }
            A02 = null;
        } else if (i == 6) {
            r3.A0P();
            A02 = VoipErrorDialogFragment.A01(new C50102Ob(), 6);
        } else if (i != 18) {
            if (i != 10 && i != 11) {
                switch (i) {
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                        Bundle bundle = new Bundle();
                        bundle.putBoolean("finish", true);
                        r3.A0P();
                        A02 = VoipErrorDialogFragment.A00(bundle, new C50102Ob(), i);
                        break;
                    default:
                        A02 = null;
                        break;
                }
            } else {
                r3.A0P();
                A02 = VoipErrorDialogFragment.A01(new C50102Ob(), i);
            }
        } else {
            Bundle bundle2 = new Bundle();
            bundle2.putParcelableArrayList("user_jids", new ArrayList<>(list));
            r3.A0P();
            A02 = VoipErrorDialogFragment.A00(bundle2, new C50102Ob(), 18);
        }
        AbstractC13860kS r0 = r3.A1V.A00;
        if (r0 != null && A02 != null) {
            r0.Adm(A02);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x00a9 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00d4  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00f1  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x010e  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0125  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01e3  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x01f3  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0203  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0220  */
    /* JADX WARNING: Removed duplicated region for block: B:70:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ void A06(X.C29631Ua r4, boolean r5) {
        /*
        // Method dump skipped, instructions count: 651
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C29631Ua.A06(X.1Ua, boolean):void");
    }

    public static void A07(String str) {
        if (4 <= Log.level) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append("/total threads count = ");
            sb.append(Thread.activeCount());
            Log.log(4, sb.toString());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:387:0x0740, code lost:
        if (r0 != null) goto L_0x0742;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:391:0x074e, code lost:
        if (com.whatsapp.voipcalling.Voip.A09(r3) != false) goto L_0x0973;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:457:0x0857, code lost:
        if (r4 == 9) goto L_0x0859;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:475:0x08a5, code lost:
        if (r4 != 8) goto L_0x08a7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:476:0x08a7, code lost:
        r11 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:585:0x0b05, code lost:
        if (r4.isJoinableGroupCall == false) goto L_0x0b07;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:669:0x0cac, code lost:
        if (r13.A04((X.AbstractC14640lm) r11) == false) goto L_0x0cae;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:765:0x0f3d, code lost:
        if (r4.callResult == 5) goto L_0x0f3f;
     */
    /* JADX WARNING: Removed duplicated region for block: B:185:0x0440  */
    /* JADX WARNING: Removed duplicated region for block: B:255:0x054d  */
    /* JADX WARNING: Removed duplicated region for block: B:554:0x0a21  */
    /* JADX WARNING: Removed duplicated region for block: B:558:0x0a31  */
    /* JADX WARNING: Removed duplicated region for block: B:563:0x0a3e  */
    /* JADX WARNING: Removed duplicated region for block: B:566:0x0a49  */
    /* JADX WARNING: Removed duplicated region for block: B:569:0x0a57  */
    /* JADX WARNING: Removed duplicated region for block: B:572:0x0a66  */
    /* JADX WARNING: Removed duplicated region for block: B:575:0x0aca  */
    /* JADX WARNING: Removed duplicated region for block: B:580:0x0af9  */
    /* JADX WARNING: Removed duplicated region for block: B:591:0x0b19  */
    /* JADX WARNING: Removed duplicated region for block: B:606:0x0b79  */
    /* JADX WARNING: Removed duplicated region for block: B:607:0x0b7d  */
    /* JADX WARNING: Removed duplicated region for block: B:689:0x0cf7  */
    /* JADX WARNING: Removed duplicated region for block: B:693:0x0d03  */
    /* JADX WARNING: Removed duplicated region for block: B:698:0x0d2f  */
    /* JADX WARNING: Removed duplicated region for block: B:716:0x0dd4  */
    /* JADX WARNING: Removed duplicated region for block: B:733:0x0e4d  */
    /* JADX WARNING: Removed duplicated region for block: B:768:0x0f45  */
    /* JADX WARNING: Removed duplicated region for block: B:806:0x103c A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:811:0x1051 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:812:0x1052  */
    /* JADX WARNING: Removed duplicated region for block: B:813:0x1055  */
    /* JADX WARNING: Removed duplicated region for block: B:816:0x1060  */
    /* JADX WARNING: Removed duplicated region for block: B:818:0x1064  */
    /* JADX WARNING: Removed duplicated region for block: B:819:0x1068  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ boolean A08(android.os.Message r25, X.C29631Ua r26) {
        /*
        // Method dump skipped, instructions count: 4426
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C29631Ua.A08(android.os.Message, X.1Ua):boolean");
    }

    public static /* synthetic */ boolean A09(C29631Ua r2) {
        PowerManager A0I = r2.A1n.A0I();
        if (Build.VERSION.SDK_INT < 21) {
            return false;
        }
        if (A0I != null) {
            return A0I.isPowerSaveMode();
        }
        Log.w("voip/service/start pm=null");
        return false;
    }

    public int A0A() {
        TelephonyManager telephonyManager = this.A1Q;
        if (telephonyManager == null || this.A1q.A09()) {
            return 0;
        }
        return telephonyManager.getCallState();
    }

    public final long A0B(boolean z) {
        int A02;
        if (!z) {
            return 45000;
        }
        C15450nH r2 = this.A1X;
        synchronized (C15450nH.class) {
            A02 = r2.A02(AbstractC15460nI.A1N) * 1000;
        }
        return (long) A02;
    }

    public final AnonymousClass1YW A0C(UserJid userJid, String str) {
        C246416h r5;
        long j;
        long A03;
        AnonymousClass1YW r0;
        C236212j r4 = this.A1c;
        synchronized (r4) {
            r5 = r4.A00;
            if (r5.containsKey(str)) {
                r0 = r5.get(str);
            } else {
                C16310on A01 = r4.A02.get();
                Cursor A09 = A01.A03.A09("SELECT _id, token, creator_jid_row_id FROM call_link WHERE token = ?", new String[]{str});
                UserJid userJid2 = null;
                if (A09.moveToNext()) {
                    long j2 = (long) A09.getInt(A09.getColumnIndexOrThrow("_id"));
                    int i = A09.getInt(A09.getColumnIndexOrThrow("creator_jid_row_id"));
                    if (i != 0) {
                        userJid2 = UserJid.of(r4.A01.A03((long) i));
                    }
                    r0 = new AnonymousClass1YW(userJid2, str, j2);
                    r5.put(str, r0);
                    A01.close();
                } else {
                    A01.close();
                }
            }
            if (r0 != null) {
                return r0;
            }
        }
        synchronized (r4) {
            AnonymousClass009.A00();
            C16310on A02 = r4.A02.A02();
            AnonymousClass1Lx A00 = A02.A00();
            ContentValues contentValues = new ContentValues();
            contentValues.put("token", str);
            if (userJid != null) {
                j = r4.A01.A01(userJid);
            } else {
                j = 0;
            }
            contentValues.put("creator_jid_row_id", Long.valueOf(j));
            A03 = A02.A03.A03(contentValues, "call_link");
            r5.put(str, new AnonymousClass1YW(userJid, str, A03));
            A00.A00();
            A00.close();
            A02.close();
        }
        return new AnonymousClass1YW(userJid, str, A03);
    }

    public final AnonymousClass4NH A0D(String str) {
        Map map = this.A2X;
        AnonymousClass4NH r0 = (AnonymousClass4NH) map.get(str);
        if (r0 != null) {
            return r0;
        }
        AnonymousClass4NH r02 = new AnonymousClass4NH();
        map.put(str, r02);
        return r02;
    }

    public final AnonymousClass1YT A0E(String str) {
        AnonymousClass1YS A04 = this.A1x.A04(AnonymousClass1SF.A0B(str));
        if (A04 != null) {
            return this.A1w.A02(A04.A00);
        }
        return null;
    }

    public C52142aJ A0F(String str) {
        if (str == null || Build.VERSION.SDK_INT < 28) {
            return null;
        }
        return this.A2N.A03(str);
    }

    public final AnonymousClass2ON A0G() {
        return new AnonymousClass39T(this);
    }

    public String A0H(String str, String str2) {
        AnonymousClass1YT A0E = A0E(str);
        if (A0E != null) {
            String str3 = A0E.A07;
            if (str3 != null || str2 == null) {
                return str3;
            }
            synchronized (A0E) {
                if (A0E.A07 != null) {
                    AnonymousClass009.A07("CallRandomId cannot be set twice!");
                } else {
                    A0E.A0I = true;
                    A0E.A07 = str2;
                }
            }
            this.A1w.A08(A0E);
        }
        return str2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x00b9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.Map A0I(java.util.Map r14, int r15, boolean r16) {
        /*
        // Method dump skipped, instructions count: 602
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C29631Ua.A0I(java.util.Map, int, boolean):java.util.Map");
    }

    public void A0J() {
        StringBuilder sb = new StringBuilder("voip/service/create ");
        sb.append(this);
        Log.i(sb.toString());
        this.A0M = new Handler(new Handler.Callback() { // from class: X.3Lc
            /* JADX WARNING: Code restructure failed: missing block: B:73:0x0139, code lost:
                if (r3 == Integer.MIN_VALUE) goto L_0x013b;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:7:0x0017, code lost:
                if (r2 == com.whatsapp.voipcalling.Voip.CallState.CONNECTED_LONELY) goto L_0x0019;
             */
            @Override // android.os.Handler.Callback
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final boolean handleMessage(android.os.Message r18) {
                /*
                // Method dump skipped, instructions count: 392
                */
                throw new UnsupportedOperationException("Method not decompiled: X.C65813Lc.handleMessage(android.os.Message):boolean");
            }
        });
        this.A0K = new Handler(new Handler.Callback() { // from class: X.3Lb
            @Override // android.os.Handler.Callback
            public final boolean handleMessage(Message message) {
                int i;
                Voip.CallState callState;
                C29631Ua r4 = C29631Ua.this;
                CallInfo callInfo = Voip.getCallInfo();
                if (!Voip.A08(callInfo)) {
                    Log.e("voip/callTimeoutHandler we are not in an active call");
                    return false;
                }
                int i2 = message.what;
                if (i2 == 0) {
                    Log.i("voip/call/not-accept-timeout");
                    if (callInfo.isGroupCall() && ((callState = callInfo.callState) == Voip.CallState.CALLING || callState == Voip.CallState.PRE_ACCEPT_RECEIVED)) {
                        return true;
                    }
                    i = 7;
                } else if (i2 == 1) {
                    Log.i("voip/call/accepted-but-not-active-timeout");
                    i = 8;
                } else if (i2 == 2) {
                    Log.i("voip/call/send-call-offer-timeout");
                    if (r4.A1m.A05(true) != 0) {
                        return true;
                    }
                    r4.A0m(callInfo, r4.A1P.getString(R.string.voip_call_failed_no_network), 9);
                    return true;
                } else if (i2 == 3) {
                    Log.i("voip/call/busy-tone-timeout");
                    i = 10;
                } else if (i2 != 4) {
                    return false;
                } else {
                    Log.i("voip/call/ringtone-timeout");
                    r4.A0N();
                    return true;
                }
                r4.A0m(callInfo, null, i);
                return true;
            }
        });
        this.A0N = new Handler(new Handler.Callback() { // from class: X.3Ld
            @Override // android.os.Handler.Callback
            public final boolean handleMessage(Message message) {
                C29631Ua r2 = C29631Ua.this;
                int i = message.what;
                if (i != 0) {
                    if (i != 1) {
                        return false;
                    }
                    if (C21280xA.A00()) {
                        return true;
                    }
                    r2.A2P.A02();
                    return true;
                } else if (C21280xA.A00()) {
                    return true;
                } else {
                    if (C29631Ua.A2m.get() > 0) {
                        r2.A0O();
                        return true;
                    }
                    Log.i("voip/service/stopSelfHandler stopSelf now");
                    r2.A1J = true;
                    r2.A1g.A00.obtainMessage(2).sendToTarget();
                    return true;
                }
            }
        });
        this.A0L = new Handler(new Handler.Callback() { // from class: X.4iO
            @Override // android.os.Handler.Callback
            public final boolean handleMessage(Message message) {
                return C29631Ua.A08(message, C29631Ua.this);
            }
        });
        this.A0x = new C71783dV(this, new ThreadFactory() { // from class: X.5EA
            @Override // java.util.concurrent.ThreadFactory
            public final Thread newThread(Runnable runnable) {
                return new AnonymousClass1MS(runnable, "VoIP Signaling Thread");
            }
        });
        AnonymousClass01d r4 = this.A1n;
        this.A0Y = new C91044Qf(r4);
        this.A0Q = new C52152aL(this);
        this.A0F = new C72903fM(this);
        this.A0G = new C51782Yu(this);
        this.A0T = new C89414Jw(this);
        AnonymousClass2Nu r2 = this.A2P;
        Log.i("voip/audio_route/init");
        r2.A0A.A04 = new WeakReference(r2);
        if (AnonymousClass2Nu.A0K) {
            r2.A0H.A08(r2.A0G);
        }
        C64263Ew r22 = this.A2K;
        r22.A07.A03(r22.A06);
        r22.A01.A03(r22.A00);
        r22.A0F.A03(r22.A0E);
        this.A2C.A03(this.A2B);
        AnonymousClass53A r1 = new AnonymousClass53A(this);
        this.A0R = r1;
        this.A1a.A0s.add(r1);
        if (Build.VERSION.SDK_INT >= 28) {
            AnonymousClass2ON A0G = A0G();
            this.A0b = A0G;
            this.A2N.A08(A0G);
        } else {
            this.A0b = null;
        }
        Voip.nativeRegisterJNIUtils(this.A2L);
        Voip.nativeRegisterEventCallback(this.A1f);
        if (Voip.A00 == null) {
            DefaultCryptoCallback defaultCryptoCallback = new DefaultCryptoCallback(this.A1W);
            Voip.nativeRegisterCryptoCallback(defaultCryptoCallback);
            Voip.A00 = defaultCryptoCallback;
        }
        VoiceService$DefaultSignalingXmppCallback voiceService$DefaultSignalingXmppCallback = new SignalingXmppCallback(this, this.A1o, this.A1W, this.A2J) { // from class: com.whatsapp.calling.service.VoiceService$DefaultSignalingXmppCallback
            public final C236412l callSendMethods;
            public final C15570nT meManager;
            public final C29631Ua service;
            public final C14830m7 time;

            {
                this.service = r1;
                this.time = r2;
                this.meManager = r3;
                this.callSendMethods = r4;
            }

            /* JADX WARNING: Removed duplicated region for block: B:79:0x017a  */
            @Override // com.whatsapp.voipcalling.SignalingXmppCallback
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void sendCallStanza(com.whatsapp.jid.Jid r18, java.lang.String r19, com.whatsapp.protocol.VoipStanzaChildNode r20) {
                /*
                // Method dump skipped, instructions count: 470
                */
                throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.calling.service.VoiceService$DefaultSignalingXmppCallback.sendCallStanza(com.whatsapp.jid.Jid, java.lang.String, com.whatsapp.protocol.VoipStanzaChildNode):void");
            }
        };
        Voip.nativeRegisterSignalingXmppCallback(voiceService$DefaultSignalingXmppCallback);
        Voip.A03 = voiceService$DefaultSignalingXmppCallback;
        if (Build.VERSION.SDK_INT >= 22) {
            AnonymousClass2CS r0 = new AnonymousClass2CS(r4.A0H());
            this.A0a = r0;
            r0.A08();
        } else {
            this.A0a = null;
        }
        Log.i("voip/service/created");
    }

    public void A0K() {
        SharedPreferences.Editor remove;
        int intValue;
        AnonymousClass2CS r0;
        StringBuilder sb = new StringBuilder("voip/service/destroy ");
        sb.append(this);
        Log.i(sb.toString());
        try {
            A0N();
        } catch (Exception e) {
            Log.e(e);
        }
        A0U();
        SoundPool soundPool = this.A0I;
        if (soundPool != null) {
            this.A0m = null;
            soundPool.release();
            this.A0I = null;
        }
        AnonymousClass2Nu r2 = this.A2P;
        Log.i("voip/audio_route/deinit");
        r2.A03 = true;
        if (AnonymousClass2Nu.A0K) {
            r2.A0H.A09(r2.A0G);
        }
        r2.A0A.A04 = null;
        A0W();
        if (Build.VERSION.SDK_INT >= 22 && (r0 = this.A0a) != null) {
            r0.A07();
            this.A0a = null;
        }
        Voip.nativeUnregisterJNIUtils();
        Voip.nativeUnregisterEventCallback();
        Voip.nativeUnregisterSignalingXmppCallback();
        Voip.A03 = null;
        C64263Ew r22 = this.A2K;
        r22.A07.A04(r22.A06);
        r22.A01.A04(r22.A00);
        r22.A0F.A04(r22.A0E);
        this.A2C.A04(this.A2B);
        ScheduledExecutorService scheduledExecutorService = this.A0x;
        if (scheduledExecutorService != null) {
            scheduledExecutorService.shutdown();
            this.A0x = null;
        }
        Integer num = this.A0g;
        if (num == null || (intValue = num.intValue()) == 0) {
            remove = this.A1r.A00.edit().remove("call_offer_ack_timeout");
        } else {
            remove = this.A1r.A00.edit().putInt("call_offer_ack_timeout", intValue * 1000);
        }
        remove.apply();
        StringBuilder sb2 = new StringBuilder("voip/service/destroyed pendingCommands: ");
        List<C26391De> list = this.A2W;
        sb2.append(list.size());
        Log.i(sb2.toString());
        for (C26391De r1 : list) {
            this.A1g.A00(r1);
        }
        list.clear();
        int i = Build.VERSION.SDK_INT;
        if (i >= 28 && this.A0b != null) {
            C237612x r23 = this.A2N;
            boolean z = false;
            if (r23.A00() == 0) {
                z = true;
            }
            AnonymousClass009.A0A("Self managed connections are not disconnected when VoiceService is being destroyed", z);
            r23.A05();
            r23.A09(this.A0b);
            this.A0b = null;
        }
        this.A1a.A0s.remove(this.A0R);
        if (this.A1A && i >= 28) {
            for (StatusBarNotification statusBarNotification : this.A27.A0L()) {
                if (statusBarNotification.getId() == 23) {
                    this.A1T.AaV("VoiceService/onDestroy", "voip/orphannotification", true);
                }
            }
        }
    }

    public void A0L() {
        long j = this.A0C;
        if (j != -1) {
            this.A0E += System.currentTimeMillis() - j;
            this.A0C = -1;
            Log.i("VoiceService:onExitPictureInPicture");
        }
    }

    public void A0M() {
        A0X();
        this.A2T.setRequestedCamera2SupportLevel(this.A2R.A02());
        this.A0L.removeMessages(23);
        this.A0L.sendEmptyMessageDelayed(23, 45000);
        this.A0x.execute(new RunnableBRunnable0Shape0S0000000_I0(5));
    }

    public void A0N() {
        if (this.A1N != null) {
            Log.i("voip/vibrate/stop");
            Vibrator A0K = this.A1n.A0K();
            AnonymousClass009.A05(A0K);
            A0K.cancel();
            this.A1N = null;
        }
        this.A0J = null;
        AnonymousClass379 r1 = this.A0Z;
        if (r1 != null) {
            r1.A03(true);
            this.A0Z = null;
        }
        if (this.A0H != null) {
            Log.i("voip/ringtone/stop");
            this.A0H.stop();
            this.A0H = null;
        }
    }

    public final void A0O() {
        this.A0N.removeMessages(0);
        this.A0N.sendEmptyMessageDelayed(0, 15000);
    }

    public final void A0P() {
        ((C18980tN) this.A2V.get()).A00(AnonymousClass115.class);
    }

    public final void A0Q() {
        if (this.A0J != null) {
            Ringtone ringtone = this.A0H;
            if (ringtone != null) {
                try {
                    if (!ringtone.isPlaying()) {
                        A0T();
                    }
                } catch (NullPointerException e) {
                    if (Build.VERSION.SDK_INT != 22 || !"oppo".equalsIgnoreCase(Build.MANUFACTURER)) {
                        throw e;
                    }
                    Log.e("voip/loadAndPlayRingtone error while playing existing ringtone", e);
                }
            } else if (this.A0Z == null) {
                AnonymousClass379 r4 = new AnonymousClass379(this.A1P, this.A1e);
                this.A0Z = r4;
                this.A2E.Aaz(r4, this.A0J);
            }
        }
    }

    public final void A0R() {
        AnonymousClass1L4 r6;
        Log.i("voip/ear-far");
        AnonymousClass009.A01();
        this.A13 = false;
        CallInfo callInfo = Voip.getCallInfo();
        if (callInfo != null && callInfo.isEitherSideRequestingUpgrade()) {
            AnonymousClass1L4 r0 = this.A0c;
            if (r0 != null) {
                r0.Afi(callInfo);
            }
            this.A2P.A07(callInfo, null);
        }
        if ((this.A0P == null || Build.VERSION.SDK_INT < 21) && (r6 = this.A0c) != null) {
            VoipActivityV2 voipActivityV2 = (VoipActivityV2) r6;
            AnonymousClass009.A01();
            Window window = voipActivityV2.getWindow();
            View childAt = ((ViewGroup) window.getDecorView().findViewById(16908290)).getChildAt(0);
            WindowManager.LayoutParams attributes = window.getAttributes();
            Log.i("voip/voipactivity/ear-far. changing visibility of the window.");
            if (childAt.getVisibility() == 4) {
                attributes.flags &= -1025;
                attributes.screenBrightness = -1.0f;
                window.getDecorView().setSystemUiVisibility(window.getDecorView().getSystemUiVisibility() & -3);
                childAt.setVisibility(0);
                window.setAttributes(attributes);
            }
            voipActivityV2.A0E.removeMessages(2);
        }
    }

    public final void A0S() {
        AnonymousClass1L4 r6;
        Log.i("voip/ear-near");
        AnonymousClass009.A01();
        this.A13 = true;
        CallInfo callInfo = Voip.getCallInfo();
        if (this.A1K || (callInfo != null && callInfo.isEitherSideRequestingUpgrade())) {
            AnonymousClass2Nu r2 = this.A2P;
            boolean z = true;
            if (r2.A00 != 1) {
                z = false;
            }
            if (z) {
                r2.A0A(callInfo, false);
            }
            this.A1K = false;
        }
        if ((this.A0P == null || Build.VERSION.SDK_INT < 21) && (r6 = this.A0c) != null) {
            VoipActivityV2 voipActivityV2 = (VoipActivityV2) r6;
            AnonymousClass009.A01();
            Window window = voipActivityV2.getWindow();
            View childAt = ((ViewGroup) window.getDecorView().findViewById(16908290)).getChildAt(0);
            WindowManager.LayoutParams attributes = window.getAttributes();
            Log.i("voip/voipactivity/ear-near. changing visibility of the window.");
            if (childAt.getVisibility() == 0) {
                attributes.flags |= EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
                attributes.screenBrightness = 0.1f;
                window.getDecorView().setSystemUiVisibility(window.getDecorView().getSystemUiVisibility() | 2);
                childAt.setVisibility(4);
                window.setAttributes(attributes);
            }
            voipActivityV2.A0E.removeMessages(2);
            voipActivityV2.A0E.sendEmptyMessageDelayed(2, 3000);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0035, code lost:
        if (r0.getRingerMode() != 2) goto L_0x0037;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0T() {
        /*
            r7 = this;
            android.media.Ringtone r0 = r7.A0H
            if (r0 == 0) goto L_0x003e
            boolean r0 = r0.isPlaying()
            if (r0 != 0) goto L_0x003e
            android.media.Ringtone r0 = r7.A0H
            r0.play()
            int r0 = r7.A05
            r4 = 1
            int r0 = r0 + r4
            r7.A05 = r0
            com.whatsapp.voipcalling.CallInfo r6 = com.whatsapp.voipcalling.Voip.getCallInfo()
            if (r6 == 0) goto L_0x003e
            boolean r0 = r6.isGroupCall()
            if (r0 == 0) goto L_0x003e
            com.whatsapp.voipcalling.Voip$CallState r1 = r6.callState
            com.whatsapp.voipcalling.Voip$CallState r0 = com.whatsapp.voipcalling.Voip.CallState.RECEIVED_CALL
            if (r1 != r0) goto L_0x003e
            X.01d r5 = r7.A1n
            android.media.AudioManager r0 = r5.A0G()
            if (r0 == 0) goto L_0x0037
            int r1 = r0.getRingerMode()
            r0 = 2
            r3 = 1
            if (r1 == r0) goto L_0x0038
        L_0x0037:
            r3 = 0
        L_0x0038:
            long[] r0 = r7.A1N
            if (r0 == 0) goto L_0x003f
            if (r3 != 0) goto L_0x003f
        L_0x003e:
            return
        L_0x003f:
            boolean r2 = r7.A13(r6)
            long[] r0 = r7.A1N
            if (r0 == 0) goto L_0x005a
            java.lang.String r0 = "voip/vibrate/stop"
            com.whatsapp.util.Log.i(r0)
            android.os.Vibrator r0 = r5.A0K()
            X.AnonymousClass009.A05(r0)
            r0.cancel()
            r0 = 0
            r7.A1N = r0
        L_0x005a:
            com.whatsapp.jid.UserJid r0 = r6.getPeerJid()
            X.AnonymousClass009.A05(r0)
            com.whatsapp.jid.UserJid r1 = r6.getPeerJid()
            com.whatsapp.jid.UserJid r0 = r6.getPeerJid()
            long[] r0 = r7.A15(r0, r2, r3)
            if (r2 != 0) goto L_0x0075
            if (r3 != 0) goto L_0x0075
        L_0x0071:
            r7.A0g(r1, r0, r4)
            return
        L_0x0075:
            r4 = 0
            goto L_0x0071
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C29631Ua.A0T():void");
    }

    public final void A0U() {
        Object obj = this.A0r;
        if (obj != null) {
            try {
                ((AcousticEchoCanceler) obj).release();
            } catch (Throwable th) {
                Log.e(th);
            }
        }
        this.A0r = null;
        Object obj2 = this.A0s;
        if (obj2 != null) {
            try {
                ((AutomaticGainControl) obj2).release();
            } catch (Throwable th2) {
                Log.e(th2);
            }
        }
        this.A0s = null;
        Object obj3 = this.A0t;
        if (obj3 != null) {
            try {
                ((NoiseSuppressor) obj3).release();
            } catch (Throwable th3) {
                Log.e(th3);
            }
        }
        this.A0t = null;
    }

    public final void A0V() {
        AnonymousClass009.A01();
        try {
            if (this.A0P != null) {
                Log.i("voip/service/releaseProximityWakeLock");
                this.A0P.release();
                this.A0P = null;
            }
        } catch (Exception e) {
            Log.e(e);
            this.A0P = null;
        }
        C91044Qf r3 = this.A0Y;
        SensorEventListener sensorEventListener = r3.A00;
        if (sensorEventListener != null) {
            r3.A02.unregisterListener(sensorEventListener, r3.A01);
            r3.A00 = null;
        }
        this.A13 = false;
    }

    public final void A0W() {
        A0V();
        AnonymousClass009.A01();
        try {
            if (this.A0O != null) {
                Log.i("voip/service/releasePartialWakeLock");
                this.A0O.release();
                this.A0O = null;
            }
        } catch (Exception e) {
            Log.e(e);
            this.A0O = null;
        }
    }

    public final void A0X() {
        Point point = new Point();
        this.A1n.A0O().getDefaultDisplay().getSize(point);
        Voip.setScreenSize(point.x, point.y);
    }

    public void A0Y(int i, String str) {
        Bundle bundle = new Bundle();
        bundle.putInt("end_call_reason", i);
        if (str != null) {
            bundle.putString("end_call_string", str);
        }
        this.A1g.A00(new C26391De("hangup_call", bundle));
    }

    public void A0Z(long j) {
        StringBuilder sb = new StringBuilder("VoiceService/delayShowingIncomingCall delay = ");
        sb.append(j);
        Log.i(sb.toString());
        this.A0L.removeMessages(1);
        this.A0L.sendEmptyMessageDelayed(1, j);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x035f, code lost:
        if (r1.equals("refresh_notification") == false) goto L_0x00a8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x0378, code lost:
        if (r1.equals("reject_call") == false) goto L_0x00a8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x039b, code lost:
        if (r1.equals("show_voip_activity") == false) goto L_0x00a8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x03c8, code lost:
        if (r1.equals("receive_message") == false) goto L_0x00a8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:134:0x03e8, code lost:
        if (r1.equals("create_call_link") == false) goto L_0x00a8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:0x0406, code lost:
        if (r1.equals("start_web_relay") == false) goto L_0x00a8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:143:0x041c, code lost:
        if (r1.equals("hangup_call") == false) goto L_0x00a8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00ec, code lost:
        if (r1.equals("start_foreground_service_from_push") == false) goto L_0x00a8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00f3, code lost:
        if (r1.equals("check_ongoing_calls") == false) goto L_0x00a8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00fa, code lost:
        if (r1.equals("preview_call_link") == false) goto L_0x00a8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0030, code lost:
        if (r3 == 4) goto L_0x0032;
     */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x02fa  */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x0363  */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x037c  */
    /* JADX WARNING: Removed duplicated region for block: B:124:0x039f  */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x03cc  */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x03ec  */
    /* JADX WARNING: Removed duplicated region for block: B:141:0x040a  */
    /* JADX WARNING: Removed duplicated region for block: B:144:0x0420  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x010a  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0252  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x025f  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x0277  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0a(X.C26391De r29) {
        /*
        // Method dump skipped, instructions count: 1152
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C29631Ua.A0a(X.1De):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:42:0x00a6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0b(com.whatsapp.fieldstats.events.WamCall r8, com.whatsapp.jid.UserJid r9, java.lang.Integer r10, java.lang.Integer r11, java.lang.Integer r12, java.lang.Long r13, java.lang.String r14, java.lang.String r15) {
        /*
            r7 = this;
            X.0sm r2 = r7.A1m
            r1 = 1
            int r0 = r2.A05(r1)
            if (r0 != 0) goto L_0x0033
            r1 = 3
        L_0x000a:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r1)
            r8.callNetwork = r0
            X.1I0 r0 = r2.A07()
            if (r0 == 0) goto L_0x001f
            int r0 = r0.A00
            long r0 = (long) r0
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            r8.callNetworkSubtype = r0
        L_0x001f:
            X.0og r3 = r7.A1Z
            int r2 = r3.A04
            r1 = 2
            r0 = 0
            if (r2 != r1) goto L_0x0028
            r0 = 1
        L_0x0028:
            r5 = 1
            r6 = 3
            if (r0 == 0) goto L_0x0037
            java.lang.Integer r0 = java.lang.Integer.valueOf(r6)
            r8.xmppStatus = r0
            goto L_0x0048
        L_0x0033:
            if (r0 != r1) goto L_0x000a
            r1 = 2
            goto L_0x000a
        L_0x0037:
            int r0 = r3.A04
            if (r0 != r5) goto L_0x0042
            java.lang.Integer r0 = java.lang.Integer.valueOf(r1)
            r8.xmppStatus = r0
            goto L_0x0048
        L_0x0042:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r5)
            r8.xmppStatus = r0
        L_0x0048:
            boolean r0 = android.media.audiofx.AcousticEchoCanceler.isAvailable()     // Catch: all -> 0x004d
            goto L_0x0052
        L_0x004d:
            r0 = move-exception
            com.whatsapp.util.Log.e(r0)
            r0 = 0
        L_0x0052:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r8.builtinAecAvailable = r0
            boolean r0 = android.media.audiofx.AutomaticGainControl.isAvailable()     // Catch: all -> 0x005d
            goto L_0x0062
        L_0x005d:
            r0 = move-exception
            com.whatsapp.util.Log.e(r0)
            r0 = 0
        L_0x0062:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r8.builtinAgcAvailable = r0
            boolean r0 = android.media.audiofx.NoiseSuppressor.isAvailable()     // Catch: all -> 0x006d
            goto L_0x0072
        L_0x006d:
            r0 = move-exception
            com.whatsapp.util.Log.e(r0)
            r0 = 0
        L_0x0072:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r8.builtinNsAvailable = r0
            r8.callOfferElapsedT = r13
            r8.callFromUi = r10
            r8.callWakeupSource = r12
            r8.callPeerPlatform = r14
            r8.callPeerAppVersion = r15
            r1 = 0
            if (r9 == 0) goto L_0x00bf
            X.0yp r0 = r7.A28
            java.util.HashMap r0 = r0.A06
            java.lang.Object r0 = r0.get(r9)
            X.1fH r0 = (X.C33911fH) r0
            if (r0 == 0) goto L_0x00bf
            long r3 = r0.A04
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x00bf
            r1 = 1
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x00ba
            java.lang.Integer r0 = java.lang.Integer.valueOf(r6)
        L_0x00a2:
            r8.peerXmppStatus = r0
            if (r11 == 0) goto L_0x00b0
            long r0 = r11.longValue()
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            r8.callAndroidAudioMode = r0
        L_0x00b0:
            int r0 = android.os.Build.VERSION.SDK_INT
            long r0 = (long) r0
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            r8.androidApiLevel = r0
            return
        L_0x00ba:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r5)
            goto L_0x00a2
        L_0x00bf:
            r0 = 4
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            goto L_0x00a2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C29631Ua.A0b(com.whatsapp.fieldstats.events.WamCall, com.whatsapp.jid.UserJid, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Long, java.lang.String, java.lang.String):void");
    }

    public final void A0c(DeviceJid deviceJid, VoipStanzaChildNode voipStanzaChildNode, String str, String str2, boolean z) {
        C15930o9 r1;
        String str3;
        VoipStanzaChildNode A04 = AnonymousClass1SF.A04(voipStanzaChildNode);
        if (A04 == null) {
            str3 = "invalid enc node!";
        } else {
            Byte A08 = AnonymousClass1SF.A08(A04);
            if (A08 == null) {
                str3 = "invalid retry count!";
            } else {
                byte[] dataCopy = A04.getDataCopy();
                if (dataCopy == null) {
                    Log.e("VoiceService:sendReKeyStanza, e2e key is empty");
                    str3 = "e2e key is empty!";
                } else {
                    byte byteValue = A08.byteValue();
                    Map map = this.A2a;
                    map.put(deviceJid, Byte.valueOf(byteValue));
                    HashMap hashMap = new HashMap();
                    hashMap.put(deviceJid, dataCopy);
                    Map A0I = A0I(hashMap, 1, false);
                    if (A0I != null && (r1 = (C15930o9) A0I.get(deviceJid)) != null) {
                        if (!str2.equals(Voip.getCurrentCallId())) {
                            StringBuilder sb = new StringBuilder("VoiceService:rekeyEncryptionTask(");
                            sb.append(str2);
                            sb.append(", ");
                            sb.append(deviceJid);
                            sb.append(", the call has ended, do nothing)");
                            Log.w(sb.toString());
                            return;
                        }
                        map.remove(deviceJid);
                        this.A2J.A00(new AnonymousClass2MM(deviceJid, AnonymousClass1SF.A03(r1, voipStanzaChildNode, byteValue), str, str2, z));
                        return;
                    }
                    return;
                }
            }
        }
        AnonymousClass009.A07(str3);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(37:7|(2:9|(2:13|(1:17))(1:12))|18|(1:(1:21)(33:38|23|(1:25)|27|28|(2:30|(1:32))|33|(1:35)|36|(3:145|39|(1:41)(2:42|(1:44)))|(3:48|(1:50)|51)|(3:53|(2:57|(3:70|(1:72)(4:75|(3:77|(1:(2:80|(1:82))(1:87))(1:88)|83)|84|(1:86)(5:89|(1:91)|93|94|(3:(3:98|(1:100)|101)(1:108)|102|(2:(1:106)|107))))|(1:74))(2:(1:62)(1:69)|63))|64)(1:109)|65|(1:67)|68|143|110|(1:112)|113|(1:115)|116|(1:118)|119|122|(1:124)|126|(6:130|131|(1:133)|135|136|(2:138|139)(2:141|142))|140|131|(0)|135|136|(0)(0)))|22|23|(0)|27|28|(0)|33|(0)|36|(0)|(0)|(0)(0)|65|(0)|68|143|110|(0)|113|(0)|116|(0)|119|122|(0)|126|(7:128|130|131|(0)|135|136|(0)(0))|140|131|(0)|135|136|(0)(0)) */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x0359, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x035a, code lost:
        com.whatsapp.util.Log.w("voip/service/start failed to load call sound set", r1);
        r18.A0m = null;
        r18.A2b.clear();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x0376, code lost:
        if (r8 != null) goto L_0x0378;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:134:0x03c6, code lost:
        if (r2.A03(r1, (X.C15580nU) r19) == false) goto L_0x03c8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x011c, code lost:
        if (r13 == false) goto L_0x011e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x02a2, code lost:
        if (r12.equals(r9.getString(com.whatsapp.R.string.settings_sound_silent)) != false) goto L_0x02a4;
     */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x02f6  */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x0303  */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x031d  */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x0336  */
    /* JADX WARNING: Removed duplicated region for block: B:124:0x036b  */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x03bf  */
    /* JADX WARNING: Removed duplicated region for block: B:138:0x03f6  */
    /* JADX WARNING: Removed duplicated region for block: B:141:0x03ff  */
    /* JADX WARNING: Removed duplicated region for block: B:145:0x0183 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x011b  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0139  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0169  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x01ac  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x01e6  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0213  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0d(com.whatsapp.jid.GroupJid r19, com.whatsapp.jid.UserJid r20, java.lang.String r21, boolean r22, boolean r23, boolean r24) {
        /*
        // Method dump skipped, instructions count: 1030
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C29631Ua.A0d(com.whatsapp.jid.GroupJid, com.whatsapp.jid.UserJid, java.lang.String, boolean, boolean, boolean):void");
    }

    public final void A0e(GroupJid groupJid, AnonymousClass1YT r13) {
        AbstractC14640lm r0;
        AnonymousClass1YS r02 = r13.A06;
        if (r02 != null && !C29941Vi.A00(r02.A01, groupJid)) {
            Log.w("VoiceService/setGroupJidInCallLog: mismatched groupJid in joinableCallLog and callLog");
        } else if (r13.A04 == null && groupJid != null) {
            synchronized (r13) {
                if (r13.A04 != groupJid) {
                    r13.A0I = true;
                }
                r13.A04 = groupJid;
            }
            C18750sx r2 = this.A1w;
            GroupJid groupJid2 = r13.A04;
            if (groupJid2 != null && r2.A09.A06(groupJid2) != null) {
                C22140ya r4 = r2.A0J;
                GroupJid groupJid3 = r13.A04;
                long j = r13.A09;
                AnonymousClass1Y2 r3 = (AnonymousClass1Y2) C22140ya.A00(r4.A00, r4.A03.A02(groupJid3, true), null, 70, j);
                AnonymousClass1YU r1 = r13.A0B;
                if (r1.A03) {
                    C15570nT r03 = r4.A01;
                    r03.A08();
                    r0 = r03.A05;
                } else {
                    r0 = r1.A01;
                }
                r3.A0e(r0);
                r3.A01 = r13.A0H;
                r3.A00 = AnonymousClass1SF.A0A(r1.A02);
                r2.A0A.A0S(r3);
            }
        }
    }

    public final void A0f(Jid jid, String str, boolean z) {
        VoipStanzaChildNode A03;
        if (str != null) {
            StringBuilder sb = new StringBuilder("voip/sendPendingCallOfferStanza jid=");
            sb.append(jid);
            sb.append(" callId=");
            sb.append(str);
            sb.append(" callTerminated=");
            sb.append(z);
            sb.append(" pendingCallOfferStanza=(");
            sb.append(this.A2f);
            sb.append("), this = ");
            sb.append(this);
            Log.i(sb.toString());
        }
        AnonymousClass3DL r5 = this.A2f;
        if (r5 != null) {
            String str2 = r5.A04;
            if (str2.equals(str)) {
                Jid jid2 = r5.A01;
                if (jid2 instanceof DeviceJid) {
                    jid2 = ((DeviceJid) jid2).getUserJid();
                }
                if (jid instanceof DeviceJid) {
                    jid = ((DeviceJid) jid).getUserJid();
                }
                if (jid2.equals(jid)) {
                    if (z) {
                        if (r5.A02 != null) {
                            A03 = AnonymousClass1SF.A05(r5.A03, null, r5.A06.keySet());
                        } else {
                            A03 = AnonymousClass1SF.A03(null, r5.A03, r5.A00);
                        }
                        r5 = new AnonymousClass3DL(jid2, A03, str2, null);
                    }
                    this.A2f = null;
                    A0q(r5);
                }
            }
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00b1, code lost:
        if (r4.equals("2") == false) goto L_0x0088;
     */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00c2  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00d8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0g(com.whatsapp.jid.UserJid r6, long[] r7, boolean r8) {
        /*
        // Method dump skipped, instructions count: 308
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C29631Ua.A0g(com.whatsapp.jid.UserJid, long[], boolean):void");
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0382, code lost:
        if (r0.A0C() == false) goto L_0x0384;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:157:0x04d6, code lost:
        if (r53.A2N.A0C() == false) goto L_0x04d8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:233:0x0646, code lost:
        if (android.text.TextUtils.isEmpty(r11.A01) != false) goto L_0x0648;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:246:0x0678, code lost:
        if (r8 == 70004) goto L_0x067a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:258:0x06b0, code lost:
        if (r3.equals(r11) == false) goto L_0x06b2;
     */
    /* JADX WARNING: Removed duplicated region for block: B:199:0x05a0  */
    /* JADX WARNING: Removed duplicated region for block: B:245:0x0675  */
    /* JADX WARNING: Removed duplicated region for block: B:255:0x069e  */
    /* JADX WARNING: Removed duplicated region for block: B:374:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0h(X.AnonymousClass2O7 r54) {
        /*
        // Method dump skipped, instructions count: 2430
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C29631Ua.A0h(X.2O7):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0073, code lost:
        if (r2.A03(r1, (X.C15580nU) r6) == false) goto L_0x0075;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0i(com.whatsapp.voipcalling.CallGroupInfo r23, X.AnonymousClass1YT r24, java.lang.Integer r25, java.lang.String r26, java.lang.String r27, java.lang.String r28, int r29, long r30, boolean r32, boolean r33, boolean r34, boolean r35, boolean r36) {
        /*
        // Method dump skipped, instructions count: 360
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C29631Ua.A0i(com.whatsapp.voipcalling.CallGroupInfo, X.1YT, java.lang.Integer, java.lang.String, java.lang.String, java.lang.String, int, long, boolean, boolean, boolean, boolean, boolean):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0008, code lost:
        if (r6.A13 != false) goto L_0x000a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x006f A[Catch: Exception -> 0x0091, TryCatch #2 {Exception -> 0x0091, blocks: (B:25:0x0041, B:28:0x004a, B:30:0x0059, B:32:0x0061, B:34:0x0067, B:36:0x006f, B:37:0x0076, B:41:0x007d, B:43:0x0087), top: B:60:0x0041, inners: #3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0076 A[Catch: Exception -> 0x0091, TryCatch #2 {Exception -> 0x0091, blocks: (B:25:0x0041, B:28:0x004a, B:30:0x0059, B:32:0x0061, B:34:0x0067, B:36:0x006f, B:37:0x0076, B:41:0x007d, B:43:0x0087), top: B:60:0x0041, inners: #3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00ac  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0j(com.whatsapp.voipcalling.CallInfo r7) {
        /*
            r6 = this;
            boolean r0 = r6.A12
            r3 = 0
            if (r0 == 0) goto L_0x000a
            boolean r0 = r6.A13
            r2 = 1
            if (r0 == 0) goto L_0x000b
        L_0x000a:
            r2 = 0
        L_0x000b:
            r6.A12 = r2
            X.2Nu r0 = r6.A2P
            int r1 = r0.A00
            r0 = 2
            if (r1 != r0) goto L_0x0024
            boolean r0 = com.whatsapp.voipcalling.Voip.A09(r7)
            if (r0 == 0) goto L_0x00c0
            boolean r0 = r7.videoEnabled
            if (r0 != 0) goto L_0x0024
            boolean r0 = X.C38241nl.A07()
            if (r0 == 0) goto L_0x00c0
        L_0x0024:
            boolean r0 = r7.isPeerRequestingUpgrade()
            if (r0 != 0) goto L_0x003a
            boolean r0 = r6.A1K
            if (r0 != 0) goto L_0x003a
            if (r3 != 0) goto L_0x003a
            r6.A0V()
            java.lang.String r0 = "voip/adjustProximitySensor: off"
        L_0x0036:
            com.whatsapp.util.Log.i(r0)
            return
        L_0x003a:
            X.AnonymousClass009.A01()
            android.os.PowerManager$WakeLock r0 = r6.A0P
            if (r0 != 0) goto L_0x00bb
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch: Exception -> 0x0091
            r0 = 21
            if (r1 < r0) goto L_0x004a
            r2 = 32
            goto L_0x0067
        L_0x004a:
            java.lang.Class<android.os.PowerManager> r1 = android.os.PowerManager.class
            java.lang.String r0 = "PROXIMITY_SCREEN_OFF_WAKE_LOCK"
            java.lang.reflect.Field r1 = r1.getDeclaredField(r0)     // Catch: NoSuchFieldException -> 0x0060, IllegalAccessException -> 0x0058, Exception -> 0x0091
            r0 = 0
            int r2 = r1.getInt(r0)     // Catch: NoSuchFieldException -> 0x0060, IllegalAccessException -> 0x0058, Exception -> 0x0091
            goto L_0x0067
        L_0x0058:
            r1 = move-exception
            java.lang.String r0 = "unable to access PROXIMITY_SCREEN_OFF_WAKE_LOCK field in PowerManager"
            com.whatsapp.util.Log.w(r0, r1)     // Catch: Exception -> 0x0091
            goto L_0x0066
        L_0x0060:
            r1 = move-exception
            java.lang.String r0 = "no PROXIMITY_SCREEN_OFF_WAKE_LOCK field in PowerManager"
            com.whatsapp.util.Log.w(r0, r1)     // Catch: Exception -> 0x0091
        L_0x0066:
            r2 = -1
        L_0x0067:
            X.01d r0 = r6.A1n     // Catch: Exception -> 0x0091
            android.os.PowerManager r1 = r0.A0I()     // Catch: Exception -> 0x0091
            if (r1 != 0) goto L_0x0076
            java.lang.String r0 = "voip/service/acquireProximityWakeLock pm=null"
            com.whatsapp.util.Log.w(r0)     // Catch: Exception -> 0x0091
            goto L_0x0098
        L_0x0076:
            android.os.PowerManager$WakeLock r0 = r6.A0P     // Catch: Exception -> 0x0091
            if (r0 != 0) goto L_0x0098
            r0 = -1
            if (r2 == r0) goto L_0x0098
            java.lang.String r0 = "VoiceService Proximity"
            android.os.PowerManager$WakeLock r0 = X.C39151pN.A00(r1, r0, r2)     // Catch: Exception -> 0x0091
            r6.A0P = r0     // Catch: Exception -> 0x0091
            if (r0 == 0) goto L_0x0098
            r0.acquire()     // Catch: Exception -> 0x0091
            java.lang.String r0 = "voip/service/acquireProximityWakeLock acquired"
            com.whatsapp.util.Log.i(r0)     // Catch: Exception -> 0x0091
            goto L_0x0098
        L_0x0091:
            r0 = move-exception
            com.whatsapp.util.Log.e(r0)
            r0 = 0
            r6.A0P = r0
        L_0x0098:
            X.4Qf r5 = r6.A0Y
            X.4Jw r4 = r6.A0T
            android.hardware.SensorEventListener r2 = r5.A00
            if (r2 == 0) goto L_0x00aa
            android.hardware.SensorManager r1 = r5.A02
            android.hardware.Sensor r0 = r5.A01
            r1.unregisterListener(r2, r0)
            r0 = 0
            r5.A00 = r0
        L_0x00aa:
            if (r4 == 0) goto L_0x00bb
            X.3LR r3 = new X.3LR
            r3.<init>(r4, r5)
            r5.A00 = r3
            android.hardware.SensorManager r2 = r5.A02
            android.hardware.Sensor r1 = r5.A01
            r0 = 2
            r2.registerListener(r3, r1, r0)
        L_0x00bb:
            java.lang.String r0 = "voip/adjustProximitySensor: on"
            goto L_0x0036
        L_0x00c0:
            if (r2 != 0) goto L_0x0024
            boolean r0 = r7.isCaller
            if (r0 != 0) goto L_0x00ce
            com.whatsapp.voipcalling.Voip$CallState r0 = r7.callState
            boolean r0 = com.whatsapp.voipcalling.Voip.A0A(r0)
            if (r0 != 0) goto L_0x0024
        L_0x00ce:
            boolean r0 = r7.isSelfRequestingUpgrade()
            if (r0 != 0) goto L_0x0024
            r3 = 1
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C29631Ua.A0j(com.whatsapp.voipcalling.CallInfo):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x006d, code lost:
        if (r2.A04((X.AbstractC14640lm) r0) != false) goto L_0x006f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00a0, code lost:
        if (r19.A1R.A00 != false) goto L_0x006f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00e9, code lost:
        if (r19.A0z == false) goto L_0x00eb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00ff, code lost:
        if (r7 != false) goto L_0x0089;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0k(com.whatsapp.voipcalling.CallInfo r20) {
        /*
        // Method dump skipped, instructions count: 259
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C29631Ua.A0k(com.whatsapp.voipcalling.CallInfo):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001c, code lost:
        if (r15.A19 == false) goto L_0x001e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0061  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0l(com.whatsapp.voipcalling.CallInfo r16, int r17, boolean r18) {
        /*
            r15 = this;
            X.AnonymousClass009.A01()
            r6 = r16
            if (r16 == 0) goto L_0x00ae
            boolean r0 = r6.callEnding
            if (r0 != 0) goto L_0x00ae
            X.1S7 r1 = r6.callWaitingInfo
            int r0 = r1.A01
            r5 = 0
            r2 = 1
            r7 = r15
            r4 = 1
            if (r0 == r2) goto L_0x001e
            r4 = 0
            boolean r0 = r15.A14
            if (r0 != 0) goto L_0x002a
            boolean r0 = r15.A19
            if (r0 != 0) goto L_0x002a
        L_0x001e:
            r5 = 1
            X.12k r3 = r15.A2U
            if (r4 == 0) goto L_0x00a7
            java.lang.String r1 = r1.A04
        L_0x0025:
            java.lang.String r0 = "build_voip_notification"
            r3.A02(r1, r0)
        L_0x002a:
            boolean r0 = r15.A1E
            X.2Nz r12 = X.C50082Nz.A00(r6, r0)
            X.12r r9 = r15.A2G
            android.content.Context r10 = r15.A1P
            X.0wc r11 = r15.A1g
            r14 = 0
            r13 = r17
            android.app.Notification r6 = r9.A00(r10, r11, r12, r13, r14)
            if (r5 == 0) goto L_0x004e
            X.12k r3 = r15.A2U
            java.lang.String r1 = r12.A04
            java.lang.String r0 = "start_foreground_service"
            r3.A02(r1, r0)
            if (r4 == 0) goto L_0x004e
            r3.A00(r1)
        L_0x004e:
            boolean r0 = r15.A1A
            r3 = 23
            if (r0 == 0) goto L_0x0061
            java.lang.String r0 = "VoiceService/startForegroundService Background restrictions on"
            com.whatsapp.util.Log.i(r0)
            X.0sK r0 = r15.A1p
            r0.A03(r3, r6)
        L_0x005e:
            r15.A19 = r2
        L_0x0060:
            return
        L_0x0061:
            monitor-enter(r7)
            X.0pI r0 = r11.A0R     // Catch: all -> 0x00ab
            android.content.Context r5 = r0.A00     // Catch: all -> 0x00ab
            X.0zp r4 = r11.A0w     // Catch: all -> 0x00ab
            java.lang.String r1 = "voicefgservice/start-service notifcation:"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch: all -> 0x00ab
            r0.<init>(r1)     // Catch: all -> 0x00ab
            r0.append(r6)     // Catch: all -> 0x00ab
            java.lang.String r0 = r0.toString()     // Catch: all -> 0x00ab
            com.whatsapp.util.Log.i(r0)     // Catch: all -> 0x00ab
            java.lang.String r0 = "com.whatsapp.service.VoiceFgService.START"
            android.content.Intent r1 = new android.content.Intent     // Catch: all -> 0x00ab
            r1.<init>(r0)     // Catch: all -> 0x00ab
            java.lang.String r0 = "com.whatsapp.service.VoiceFgService.EXTRA_NOTIFICATION_ID"
            android.content.Intent r1 = r1.putExtra(r0, r3)     // Catch: all -> 0x00ab
            java.lang.String r0 = "com.whatsapp.service.VoiceFgService.EXTRA_STOP_FOREGROUND_STATE"
            r8 = r18
            android.content.Intent r1 = r1.putExtra(r0, r8)     // Catch: all -> 0x00ab
            com.whatsapp.voipcalling.VoiceFGService.A02 = r6     // Catch: all -> 0x00ab
            java.lang.Class<com.whatsapp.voipcalling.VoiceFGService> r0 = com.whatsapp.voipcalling.VoiceFGService.class
            boolean r0 = r4.A03(r5, r1, r0)     // Catch: all -> 0x00ab
            r15.A14 = r0     // Catch: all -> 0x00ab
            monitor-exit(r7)
            if (r0 != 0) goto L_0x0060
            r14 = 1
            android.app.Notification r1 = r9.A00(r10, r11, r12, r13, r14)
            X.0sK r0 = r15.A1p
            r0.A03(r3, r1)
            goto L_0x005e
        L_0x00a7:
            java.lang.String r1 = r6.callId
            goto L_0x0025
        L_0x00ab:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        L_0x00ae:
            java.lang.String r0 = "do not create notification, we are not in a active call"
            com.whatsapp.util.Log.e(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C29631Ua.A0l(com.whatsapp.voipcalling.CallInfo, int, boolean):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0052 A[Catch: SecurityException -> 0x0077, TRY_LEAVE, TryCatch #1 {SecurityException -> 0x0077, blocks: (B:10:0x002a, B:11:0x0038, B:21:0x0052, B:22:0x005a, B:23:0x0073), top: B:75:0x002a }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0m(com.whatsapp.voipcalling.CallInfo r9, java.lang.String r10, int r11) {
        /*
        // Method dump skipped, instructions count: 400
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C29631Ua.A0m(com.whatsapp.voipcalling.CallInfo, java.lang.String, int):void");
    }

    public final void A0n(CallInfo callInfo, boolean z, boolean z2) {
        String obj;
        Voip.CallState callState;
        AnonymousClass009.A01();
        StringBuilder sb = new StringBuilder("voip/phone-call-in-progress-changed: ");
        sb.append(z);
        if (callInfo == null) {
            obj = null;
        } else {
            StringBuilder sb2 = new StringBuilder(", call state: ");
            sb2.append(callInfo.callState);
            obj = sb2.toString();
        }
        sb.append(obj);
        Log.i(sb.toString());
        if (callInfo != null && (callState = callInfo.callState) != Voip.CallState.NONE) {
            if (!z) {
                if (!z2) {
                    Message obtainMessage = this.A0L.obtainMessage(38);
                    this.A0L.removeMessages(38);
                    this.A0L.sendMessageDelayed(obtainMessage, 2000);
                } else {
                    boolean z3 = callInfo.videoEnabled;
                    AnonymousClass2Nu r1 = this.A2P;
                    Boolean bool = Boolean.TRUE;
                    if (z3) {
                        r1.A07(callInfo, bool);
                    } else {
                        r1.A08(callInfo, bool);
                    }
                }
                this.A2P.A04();
            } else if (callState == Voip.CallState.CALLING || callState == Voip.CallState.PRE_ACCEPT_RECEIVED || callState == Voip.CallState.RECEIVED_CALL || callState == Voip.CallState.REJOINING) {
                A0Y(4, null);
                return;
            } else {
                AnonymousClass2Nu r0 = this.A2P;
                r0.A06(callInfo);
                r0.A01();
                if (z2) {
                    r0.A03();
                }
                this.A16 = true;
            }
            Voip.onCallInterrupted(z, !A14(callInfo.callId));
        }
    }

    public final void A0o(AnonymousClass1YT r34, boolean z) {
        C18750sx r3 = this.A1w;
        if (r34.A04 == null) {
            C19990v2 r1 = r3.A09;
            UserJid userJid = r34.A0B.A01;
            if (r1.A06(userJid) != null) {
                C30381Xe r2 = new C30381Xe(new AnonymousClass1IS(userJid, AnonymousClass15O.A00(r3.A02, r3.A03, false), false), r34.A09);
                ArrayList arrayList = new ArrayList();
                AnonymousClass1IS r5 = r2.A0z;
                AbstractC14640lm r6 = r5.A00;
                UserJid of = UserJid.of(r6);
                if (!C15380n4.A0L(of)) {
                    StringBuilder sb = new StringBuilder("CallLog/fromFMessageMissedCall bad UserJid: ");
                    sb.append(r6);
                    Log.e(sb.toString());
                } else {
                    AnonymousClass1YU r7 = new AnonymousClass1YU(0, of, r5.A01, r5.A02);
                    long j = r2.A0I;
                    boolean z2 = r34.A0H;
                    GroupJid groupJid = r34.A04;
                    boolean z3 = r34.A0G;
                    List emptyList = Collections.emptyList();
                    AnonymousClass1YT r0 = new AnonymousClass1YT(r34.A0F, r34.A0A, groupJid, r2, r7, r34.A06, null, emptyList, 0, 2, -1, j, 0, z2, false, true, z3);
                    for (AnonymousClass1YV r72 : r34.A0C.values()) {
                        Map map = r0.A0C;
                        UserJid userJid2 = r72.A02;
                        map.put(userJid2, new AnonymousClass1YV(userJid2, r72.A00, -1));
                    }
                    arrayList.add(r0);
                }
                r2.A16(arrayList);
                r3.A0A.A0S(r2);
            }
        }
        C21260x8 r12 = this.A2H;
        Log.i("voip/notifyCallMissed");
        for (C236712o r02 : r12.A01()) {
            r02.A03(r34, z);
        }
    }

    public final void A0p(AnonymousClass1YT r7, boolean z) {
        AnonymousClass1YS r0;
        AnonymousClass1YS A03;
        synchronized (r7) {
            if (r7.A0G != z) {
                r7.A0I = true;
            }
            r7.A0G = z;
        }
        String A0A = AnonymousClass1SF.A0A(r7.A03().A02);
        StringBuilder sb = new StringBuilder("voip/setCallLogIsJoinableGroupCall callId:");
        sb.append(A0A);
        sb.append(" joinable:");
        sb.append(z);
        Log.i(sb.toString());
        if (z) {
            boolean z2 = false;
            if (r7.A0A != null) {
                z2 = true;
            }
            AnonymousClass009.A0A("Can't rejoin from call logs missing call creator", z2);
            C237212t r4 = this.A2O;
            r4.A00();
            if (r4.A00.isEmpty()) {
                r4.A02.A00.edit().putLong("first_unseen_joinable_call", r7.A02()).apply();
            }
            String A0A2 = AnonymousClass1SF.A0A(r7.A0B.A02);
            synchronized (r4) {
                r4.A00.add(A0A2);
            }
            r4.A01.A0H(new RunnableBRunnable0Shape4S0100000_I0_4(r4.A03, 41));
            if (r7.A06 == null) {
                GroupJid groupJid = r7.A04;
                if (!(groupJid == null || (A03 = this.A1x.A03(groupJid)) == null)) {
                    this.A1T.AaV("linked-group-call/downgrade-ongoing-call", null, false);
                    C18750sx r2 = this.A1w;
                    AnonymousClass1YT A02 = r2.A02(A03.A00);
                    if (A02 != null) {
                        A03.A02 = true;
                        A03.A01 = null;
                        synchronized (A02) {
                            if (A02.A04 != null) {
                                A02.A0I = true;
                            }
                            A02.A04 = null;
                        }
                        A02.A09(A03);
                        r2.A08(A02);
                        StringBuilder sb2 = new StringBuilder("VoiceService/maybeClearCallLogWithSameGroupJid Cleaning up zombie call: ");
                        String str = A03.A03;
                        sb2.append(str);
                        Log.i(sb2.toString());
                        ArrayList arrayList = new ArrayList();
                        arrayList.add(AnonymousClass1SF.A0A(str));
                        this.A0x.execute(new RunnableBRunnable0Shape1S0200000_I0_1(this, 39, arrayList));
                    }
                }
                r0 = new AnonymousClass1YS(r7.A04, r7.A03().A02, r7.A02(), r7.A0H);
            } else {
                return;
            }
        } else {
            C237212t r22 = this.A2O;
            r22.A00();
            String A0A3 = AnonymousClass1SF.A0A(r7.A0B.A02);
            synchronized (r22) {
                r22.A00.remove(A0A3);
            }
            r22.A01.A0H(new RunnableBRunnable0Shape4S0100000_I0_4(r22.A03, 41));
            C17140qK r3 = this.A2R;
            SharedPreferences.Editor edit = r3.A01().edit();
            StringBuilder sb3 = new StringBuilder("active_joinable_call");
            sb3.append(A0A);
            edit.remove(sb3.toString()).apply();
            if (r7.A06 != null) {
                r0 = null;
            } else {
                SharedPreferences.Editor edit2 = r3.A01().edit();
                StringBuilder sb4 = new StringBuilder("joinable_");
                sb4.append(A0A);
                edit2.remove(sb4.toString()).apply();
                return;
            }
        }
        r7.A09(r0);
    }

    public final void A0q(AnonymousClass3DL r15) {
        Jid jid = r15.A01;
        String str = r15.A04;
        VoipStanzaChildNode voipStanzaChildNode = r15.A03;
        String str2 = r15.A05;
        boolean z = !TextUtils.isEmpty(str2);
        if (!z) {
            str2 = AnonymousClass15O.A00(this.A1W, this.A1o, false);
        }
        this.A0u = str2;
        HashMap hashMap = new HashMap();
        for (Map.Entry entry : r15.A06.entrySet()) {
            Object key = entry.getKey();
            if (entry.getValue() != null) {
                hashMap.put(key, entry.getValue());
            }
        }
        Set keySet = hashMap.keySet();
        boolean z2 = !keySet.isEmpty();
        if (z2) {
            for (Object obj : keySet) {
                if (this.A2K.A0K.contains(obj)) {
                    StringBuilder sb = new StringBuilder("VoiceService:sendOfferStanza waiting for PreKey job finishes with ");
                    sb.append(obj);
                    Log.i(sb.toString());
                    this.A2f = r15;
                    return;
                }
            }
        }
        RunnableC55792jL r4 = new Runnable(jid, voipStanzaChildNode, r15, str, str2, hashMap, z2, z) { // from class: X.2jL
            public final /* synthetic */ Jid A01;
            public final /* synthetic */ VoipStanzaChildNode A02;
            public final /* synthetic */ AnonymousClass3DL A03;
            public final /* synthetic */ String A04;
            public final /* synthetic */ String A05;
            public final /* synthetic */ Map A06;
            public final /* synthetic */ boolean A07;
            public final /* synthetic */ boolean A08;

            {
                this.A07 = r8;
                this.A01 = r2;
                this.A04 = r5;
                this.A06 = r7;
                this.A03 = r4;
                this.A02 = r3;
                this.A05 = r6;
                this.A08 = r9;
            }

            @Override // java.lang.Runnable
            public final void run() {
                C29631Ua r3 = C29631Ua.this;
                boolean z3 = this.A07;
                Jid jid2 = this.A01;
                String str3 = this.A04;
                Map map = this.A06;
                AnonymousClass3DL r42 = this.A03;
                VoipStanzaChildNode voipStanzaChildNode2 = this.A02;
                String str4 = this.A05;
                boolean z4 = this.A08;
                if (r3.A2e != 14) {
                    r3.A2j = false;
                    if (z3) {
                        UserJid convertToUserJid = Voip.JidHelper.convertToUserJid(jid2);
                        AnonymousClass009.A05(convertToUserJid);
                        StringBuilder A0k = C12960it.A0k("VoiceService:sendOfferEcryptionTask, Call ID = ");
                        A0k.append(str3);
                        A0k.append(", peer = ");
                        A0k.append(convertToUserJid);
                        C12960it.A1F(A0k);
                        Map A0I = r3.A0I(map, 0, false);
                        if (A0I != null) {
                            CallInfo callInfo = Voip.getCallInfo();
                            if (callInfo != null && ((callInfo.isCaller || callInfo.callLinkToken != null) && callInfo.callId.equals(str3))) {
                                UserJid peerJid = callInfo.getPeerJid();
                                AnonymousClass009.A05(peerJid);
                                if (peerJid.equals(convertToUserJid)) {
                                    if (r42.A02 != null) {
                                        voipStanzaChildNode2 = AnonymousClass1SF.A05(r42.A03, A0I, r42.A06.keySet());
                                    } else {
                                        C15930o9 r2 = null;
                                        AnonymousClass009.A0A("cannot have multiple encrypted messages in old format!", C12960it.A1V(A0I.size(), 1));
                                        if (A0I.size() == 1) {
                                            DeviceJid of = DeviceJid.of(r42.A01);
                                            AnonymousClass009.A05(of);
                                            Object obj2 = A0I.get(of);
                                            AnonymousClass009.A05(obj2);
                                            r2 = (C15930o9) obj2;
                                        }
                                        voipStanzaChildNode2 = AnonymousClass1SF.A03(r2, r42.A03, r42.A00);
                                    }
                                }
                            }
                            StringBuilder A0k2 = C12960it.A0k("VoiceService:sendOfferEcryptionTask(");
                            A0k2.append(str3);
                            A0k2.append(", ");
                            A0k2.append(convertToUserJid);
                            Log.w(C12960it.A0d(", call state does not match, do nothing)", A0k2));
                        }
                        Log.i("VoiceService:sendOfferStanza sendOfferEcryptionTask skipped or failed");
                        r3.A2f = r42;
                    }
                    StringBuilder A0k3 = C12960it.A0k("VoiceService:sendOfferStanza without enc (Call ID = ");
                    A0k3.append(str3);
                    A0k3.append(", peer = ");
                    A0k3.append(jid2);
                    Log.i(C12960it.A0d(")", A0k3));
                    if (voipStanzaChildNode2 != null) {
                        if (r3.A0W == null) {
                            r3.A0W = new C90984Pz(str4, SystemClock.elapsedRealtime());
                        }
                        r3.A2J.A00(new AnonymousClass2MM(jid2, voipStanzaChildNode2, str4, str3, z4));
                        return;
                    }
                    r3.A2f = r42;
                }
            }
        };
        if (z2) {
            this.A02 = this.A2R.A01().getInt("call_start_delay", 0);
            CallInfo callInfo = Voip.getCallInfo();
            int i = this.A02;
            if (i <= 0 || i >= 3000 || callInfo == null || callInfo.callState != Voip.CallState.CALLING || r15.A00 != 0) {
                Log.i("VoiceService:sendOfferStanza without delay");
                this.A0x.execute(r4);
                return;
            }
            StringBuilder sb2 = new StringBuilder("VoiceService:sendOfferStanza with ");
            sb2.append(i);
            sb2.append(" ms delay");
            Log.i(sb2.toString());
            this.A2j = true;
            this.A0x.schedule(r4, (long) this.A02, TimeUnit.MILLISECONDS);
            return;
        }
        r4.run();
    }

    public final void A0r(Voip.CallState callState, CallInfo callInfo) {
        int duration;
        PowerManager.WakeLock A00;
        StringBuilder sb = new StringBuilder("voip/service/stop ");
        sb.append(this);
        Log.i(sb.toString());
        AnonymousClass009.A01();
        synchronized (this) {
            if (this.A14) {
                C20970wc r1 = this.A1g;
                Context context = r1.A0R.A00;
                C22900zp r12 = r1.A0w;
                Log.i("voicefgservice/stop-service");
                r12.A01(context, VoiceFGService.class);
                this.A14 = false;
            }
            if (this.A19) {
                Log.i("VoiceService/stopForegroundService cancel via waNotificationManager");
                this.A1p.A04(23, null);
                this.A19 = false;
            }
        }
        if (this.A1I) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            this.A1I = false;
            C87704Co.A00 = false;
            this.A0D = 0;
            this.A0o = null;
            this.A0z = false;
            this.A13 = false;
            this.A1E = false;
            this.A2h = null;
            this.A2i = null;
            this.A0X = null;
            this.A0e = null;
            this.A0i = 1500;
            this.A0J = null;
            this.A0h = null;
            this.A0f = null;
            this.A0n = null;
            this.A11 = false;
            this.A0A = 0;
            this.A0W = null;
            this.A0U = null;
            this.A0V = null;
            this.A0w = null;
            this.A0v = null;
            this.A1L = true;
            this.A0l = null;
            this.A0j = null;
            this.A10 = false;
            this.A09 = 0;
            this.A18 = false;
            this.A0y = 0;
            this.A08 = 0;
            this.A2e = 0;
            this.A2f = null;
            this.A2Z.clear();
            this.A2a.clear();
            this.A2K.A0K.clear();
            this.A2Y.clear();
            this.A1G = false;
            this.A12 = false;
            this.A06 = 30;
            this.A02 = 0;
            this.A2j = false;
            AnonymousClass3D1 r3 = this.A2F;
            r3.A02 = 0;
            r3.A01 = Double.NaN;
            r3.A00 = Double.NaN;
            this.A0E = 0;
            this.A0C = -1;
            this.A03 = 0;
            this.A0p = null;
            this.A17 = false;
            this.A0k = null;
            this.A0B = 0;
            this.A16 = false;
            TelephonyManager telephonyManager = this.A1Q;
            if (telephonyManager == null) {
                Log.w("voip/service/stop telephonyManager=null");
            } else if (!this.A1q.A09()) {
                telephonyManager.listen(this.A0Q, 0);
            }
            Context context2 = this.A1P;
            context2.unregisterReceiver(this.A0F);
            AnonymousClass2Nu r32 = this.A2P;
            StringBuilder sb2 = new StringBuilder("voip/audio_route/onCallStop using telecom:");
            sb2.append(r32.A06);
            Log.i(sb2.toString());
            if (!r32.A06) {
                r32.A0F.A02();
                r32.A0A.A07(r32);
                r32.A09.unregisterReceiver(r32.A0I);
            } else if (callInfo != null && !callInfo.self.A09) {
                r32.A03();
            }
            r32.A02 = null;
            r32.A04 = false;
            if (Build.VERSION.SDK_INT >= 21) {
                context2.unregisterReceiver(this.A0G);
            }
            A0R();
            A0W();
            try {
                PowerManager A0I = this.A1n.A0I();
                if (A0I == null) {
                    Log.w("voice/service/turn-on-screen pm=null");
                } else if (!A0I.isScreenOn() && (A00 = C39151pN.A00(A0I, "VoiceService end call", 268435466)) != null) {
                    A00.acquire(1);
                    A00.release();
                }
            } catch (Exception e) {
                Log.e(e);
            }
            A0N();
            if (this.A0I == null || callState != Voip.CallState.ACTIVE || callInfo == null || callInfo.callWaitingInfo.A01 != 0) {
                r32.A02();
            } else {
                MediaPlayer create = MediaPlayer.create(context2, (int) R.raw.end_call);
                if (create == null) {
                    duration = 500;
                } else {
                    duration = create.getDuration();
                }
                StringBuilder sb3 = new StringBuilder("voip/service/playEndCallTone duration: ");
                sb3.append(duration);
                Log.i(sb3.toString());
                float f = 0.5f;
                if (r32.A00 == 2) {
                    f = 1.0f;
                }
                Integer num = this.A0m;
                if (num != null) {
                    this.A0I.play(num.intValue(), f, f, 0, 0, 1.0f);
                } else {
                    Log.e("VoiceService:playEndCallTone sound pool has not been loaded successfully");
                }
                this.A0N.removeMessages(1);
                this.A0N.sendEmptyMessageDelayed(1, (long) (duration + 100));
            }
            this.A0M.removeCallbacksAndMessages(null);
            SharedPreferences sharedPreferences = this.A1r.A00;
            sharedPreferences.edit().remove("voip_call_id").apply();
            sharedPreferences.edit().remove("voip_call_ab_test_bucket").apply();
            A0O();
            StringBuilder sb4 = new StringBuilder("voip/service/stop elapsed ");
            sb4.append(SystemClock.elapsedRealtime() - elapsedRealtime);
            sb4.append(" ms");
            Log.i(sb4.toString());
            A07("voip/service/stop");
            if (this.A1D) {
                this.A1b.A00(4, false);
            } else {
                this.A1b.A00(3, false);
            }
        }
    }

    public void A0s(Voip.CallState callState, String str) {
        int i;
        AudioManager A0G = this.A1n.A0G();
        switch (callState.ordinal()) {
            case 1:
            case 2:
            case 4:
            case 5:
            case 6:
            case 11:
                if (A0G != null && !A14(str)) {
                    int mode = A0G.getMode();
                    Integer num = this.A0e;
                    if (num != null) {
                        i = num.intValue();
                    } else {
                        i = 3;
                    }
                    if (mode != i) {
                        A0G.setMode(i);
                    }
                    StringBuilder sb = new StringBuilder("voip/updateAudioModeForCallState ");
                    sb.append(callState);
                    sb.append(" to ");
                    sb.append(i);
                    Log.i(sb.toString());
                    return;
                }
                return;
            case 3:
            case 7:
            case 8:
            case 9:
            case 10:
            default:
                return;
        }
    }

    public void A0t(AnonymousClass1L4 r3) {
        StringBuilder sb = new StringBuilder("voip/service/resetVoipUiIfEquals ");
        sb.append(this);
        Log.i(sb.toString());
        if (this.A0c == r3) {
            this.A0c = null;
        }
    }

    public void A0u(AnonymousClass1L4 r8) {
        this.A0c = r8;
        if (Build.VERSION.SDK_INT >= 23) {
            StatusBarNotification[] A0L = this.A27.A0L();
            for (StatusBarNotification statusBarNotification : A0L) {
                if (statusBarNotification.getId() == 27) {
                    this.A1p.A04(27, statusBarNotification.getTag());
                }
            }
            return;
        }
        for (String str : this.A2R.A03()) {
            this.A1p.A04(27, str);
        }
    }

    public void A0v(String str) {
        Log.i("voip/call/reject");
        this.A0x.execute(new RunnableBRunnable0Shape0S2000000_I0(str, "close_call_link_lobby", 1));
    }

    public void A0w(String str) {
        AnonymousClass009.A0A("must be called for self managed connection", A14(str));
        C52142aJ A0F = A0F(str);
        if (Build.VERSION.SDK_INT >= 28 && A0F != null && A0F.getState() == 5) {
            A0F.onUnhold();
        }
    }

    public final void A0x(String str) {
        if (str == null || str.equals(Voip.getCurrentCallId())) {
            A0Y(13, this.A1P.getString(R.string.voip_call_failed_no_network));
        } else {
            Voip.clearVoipParam(str);
        }
    }

    public void A0y(String str, int i) {
        CallInfo callInfo = Voip.getCallInfo();
        if (!Voip.A09(callInfo)) {
            Log.w("voip/service/acceptCall No active call");
            return;
        }
        boolean equals = str.equals(callInfo.callWaitingInfo.A04);
        if (i != 0) {
            A0D(str).A00 = Integer.valueOf(i);
        }
        if (!equals) {
            this.A0z = true;
            A0N();
            AnonymousClass2Nu r1 = this.A2P;
            if (r1.A00 == 1) {
                r1.A0A(callInfo, false);
            }
            A0s(Voip.CallState.ACCEPT_SENT, str);
        }
        this.A0x.schedule(new RunnableBRunnable0Shape0S1110000_I0(this, str, 2, equals), 100, TimeUnit.MILLISECONDS);
    }

    public void A0z(String str, String str2, int i) {
        Log.i("voip/call/reject");
        if (i != 0) {
            A0D(str).A00 = Integer.valueOf(i);
        }
        this.A0x.execute(new RunnableBRunnable0Shape0S2000000_I0(str, str2, 0));
    }

    public void A10(List list) {
        DeviceJid deviceJid;
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            String str = (String) it.next();
            AnonymousClass1YT A0E = A0E(str);
            if (!(A0E == null || (deviceJid = A0E.A0A) == null)) {
                arrayList.add(str);
                arrayList2.add(deviceJid);
            }
        }
        if (!arrayList.isEmpty()) {
            StringBuilder sb = new StringBuilder("VoiceService/actionCheckOngoingCalls: ongoing calls count=");
            sb.append(arrayList.size());
            Log.i(sb.toString());
            Voip.checkOngoingCalls((String[]) arrayList.toArray(new String[0]), (DeviceJid[]) arrayList2.toArray(new DeviceJid[0]));
        }
    }

    public final boolean A11(AudioManager audioManager) {
        boolean isSpeakerphoneOn = audioManager.isSpeakerphoneOn();
        AnonymousClass2Nu r3 = this.A2P;
        boolean z = true;
        if (r3.A00 != 1) {
            z = false;
        }
        if (z == isSpeakerphoneOn) {
            return false;
        }
        StringBuilder sb = new StringBuilder("VoiceService:callEnding audio route mismatch detectecd. current = ");
        sb.append(audioManager.isSpeakerphoneOn());
        sb.append(", Expected = ");
        boolean z2 = true;
        if (r3.A00 != 1) {
            z2 = false;
        }
        sb.append(z2);
        Log.w(sb.toString());
        return true;
    }

    public final boolean A12(UserJid userJid, String str, boolean z) {
        C15370n3 A0B = this.A1i.A0B(userJid);
        if (Build.VERSION.SDK_INT >= 28) {
            C237612x r2 = this.A2N;
            Context context = this.A1P;
            C15570nT r0 = this.A1W;
            r0.A08();
            C27631Ih r02 = r0.A05;
            AnonymousClass009.A05(r02);
            if (r2.A0E(context, r02) && r2.A0F(userJid, str, this.A1j.A04(A0B), z)) {
                return true;
            }
        }
        return false;
    }

    public final boolean A13(CallInfo callInfo) {
        if (callInfo == null) {
            callInfo = Voip.getCallInfo();
        }
        return this.A1O > 0 && callInfo != null && callInfo.groupJid != null && callInfo.isJoinableGroupCall && !A0D(callInfo.callId).A01;
    }

    public boolean A14(String str) {
        return A0F(str) != null;
    }

    public final long[] A15(UserJid userJid, boolean z, boolean z2) {
        int i;
        if ("0".equals(this.A2D.A08(userJid.getRawString()).A04())) {
            return new long[]{0, 0, 0};
        }
        if (!z) {
            return new long[]{0, 50, 50, 70, 30, 70, 30, 50, 50, 70, 30, 70, 30, 50, 50, 70, 30, 70, 4000};
        }
        long[] jArr = {0, 150, 83, 50, 67, 20, 97, 20, 96, 50, 67, 35, 195, 50, 78, 300, 4000};
        if (z2 || (i = this.A1O) <= 1) {
            return jArr;
        }
        long[] jArr2 = new long[(i << 4) + 1];
        for (int i2 = 0; i2 < i; i2++) {
            System.arraycopy(jArr, 1, jArr2, (i2 << 4) + 1, 16);
        }
        return jArr2;
    }
}
