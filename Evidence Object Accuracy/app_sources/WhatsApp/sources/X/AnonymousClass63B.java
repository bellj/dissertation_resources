package X;

import android.hardware.Camera;
import android.os.Handler;
import android.os.Looper;
import com.facebook.redex.IDxCallableShape15S0100000_3_I1;
import java.util.List;
import java.util.concurrent.Callable;

/* renamed from: X.63B  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass63B implements Camera.OnZoomChangeListener {
    public int A00;
    public int A01;
    public int A02;
    public Camera A03;
    public final Handler A04 = new Handler(Looper.getMainLooper(), new AnonymousClass63S(this));
    public final C129775yH A05 = new C129775yH();
    public final C129885yS A06;
    public final C1308560f A07;
    public final Callable A08 = new IDxCallableShape15S0100000_3_I1(this, 7);
    public volatile int A09;
    public volatile List A0A;
    public volatile boolean A0B;
    public volatile boolean A0C;
    public volatile boolean A0D;
    public volatile boolean A0E;

    public AnonymousClass63B(C129885yS r4, C1308560f r5) {
        this.A06 = r4;
        this.A07 = r5;
    }

    public void A00(int i) {
        if (this.A0B && i != this.A09 && i <= this.A01 && i >= 0) {
            if (!AnonymousClass61K.A02()) {
                if (this.A0E) {
                    synchronized (this) {
                        this.A02 = i;
                        if (!this.A0C) {
                            this.A0C = true;
                            this.A03.startSmoothZoom(i);
                        } else if (!this.A0D) {
                            this.A0D = true;
                            this.A03.stopSmoothZoom();
                        }
                    }
                    return;
                }
                try {
                    C119105ct A00 = this.A06.A00(this.A00);
                    AbstractC125485rK.A02(AbstractC130685zo.A0v, A00, i);
                    A00.A02();
                    onZoomChange(i, true, this.A03);
                    return;
                } catch (Exception e) {
                    RuntimeException runtimeException = new RuntimeException(C12960it.A0W(i, "Failed to set zoom level to: "), e);
                    synchronized (this) {
                        Handler handler = this.A04;
                        handler.sendMessage(handler.obtainMessage(2, runtimeException));
                    }
                }
                return;
            }
            throw C12990iw.A0m("Attempting to zoom on the UI thread!");
        }
    }

    @Override // android.hardware.Camera.OnZoomChangeListener
    public synchronized void onZoomChange(int i, boolean z, Camera camera) {
        this.A09 = i;
        int i2 = 0;
        if (this.A0E) {
            this.A0C = C12960it.A1T(z ? 1 : 0);
            if (z) {
                this.A0D = false;
                if (this.A0B && this.A02 != i) {
                    this.A07.A07("update_zoom_level", this.A08);
                }
            }
        }
        Handler handler = this.A04;
        if (z) {
            i2 = 1;
        }
        handler.sendMessage(handler.obtainMessage(1, i, i2));
    }
}
