package X;

import android.view.View;
import com.whatsapp.businessdirectory.viewmodel.BusinessDirectorySearchQueryViewModel;
import java.util.List;

/* renamed from: X.36e  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C622936e extends AbstractView$OnClickListenerC34281fs {
    public final /* synthetic */ int A00;
    public final /* synthetic */ BusinessDirectorySearchQueryViewModel A01;
    public final /* synthetic */ C30211Wn A02;
    public final /* synthetic */ String A03;
    public final /* synthetic */ List A04;

    public C622936e(BusinessDirectorySearchQueryViewModel businessDirectorySearchQueryViewModel, C30211Wn r2, String str, List list, int i) {
        this.A01 = businessDirectorySearchQueryViewModel;
        this.A02 = r2;
        this.A04 = list;
        this.A00 = i;
        this.A03 = str;
    }

    @Override // X.AbstractView$OnClickListenerC34281fs
    public void A04(View view) {
        BusinessDirectorySearchQueryViewModel businessDirectorySearchQueryViewModel = this.A01;
        C30211Wn r6 = this.A02;
        businessDirectorySearchQueryViewModel.A05 = r6;
        businessDirectorySearchQueryViewModel.A00 = 1;
        businessDirectorySearchQueryViewModel.A0Q.A00(new C48152En(r6.A01, System.currentTimeMillis()));
        C12970iu.A1Q(businessDirectorySearchQueryViewModel.A0Z, 0);
        C16430p0 r7 = businessDirectorySearchQueryViewModel.A0L;
        Long A0l = C12980iv.A0l(businessDirectorySearchQueryViewModel.A02);
        Long A0l2 = C12980iv.A0l(businessDirectorySearchQueryViewModel.A01);
        List list = this.A04;
        r7.A08(businessDirectorySearchQueryViewModel.A0N.A01(), A0l, A0l2, C12980iv.A0l(list.size()), Long.valueOf(((long) list.indexOf(r6)) + 1), C12980iv.A0l(this.A00), null, C12980iv.A0l(businessDirectorySearchQueryViewModel.A03), this.A03, 45);
        businessDirectorySearchQueryViewModel.A0A();
    }
}
