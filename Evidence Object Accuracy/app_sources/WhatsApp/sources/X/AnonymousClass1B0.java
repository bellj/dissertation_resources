package X;

import android.content.SharedPreferences;
import org.json.JSONObject;

/* renamed from: X.1B0  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1B0 {
    public AnonymousClass124 A00;
    public final AbstractC15710nm A01;
    public final C21330xF A02;
    public final AnonymousClass122 A03;

    public AnonymousClass1B0(AbstractC15710nm r1, C21330xF r2, AnonymousClass122 r3) {
        this.A01 = r1;
        this.A03 = r3;
        this.A02 = r2;
    }

    public void A00() {
        this.A02.A00().edit().remove("current_search_location").apply();
    }

    public void A01(C48122Ek r5) {
        SharedPreferences.Editor edit = this.A02.A00().edit();
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("radius", r5.A05);
        jSONObject.put("latitude", r5.A03);
        jSONObject.put("longitude", r5.A04);
        jSONObject.put("imprecise_latitude", r5.A01);
        jSONObject.put("imprecise_longitude", r5.A02);
        jSONObject.put("location_description", r5.A06);
        jSONObject.put("provider", r5.A07);
        jSONObject.put("accuracy", r5.A00);
        String obj = jSONObject.toString();
        edit.putString("current_search_location", C48162Eo.A01(this.A01, this.A00, obj)).apply();
    }

    public void A02(boolean z) {
        this.A02.A00().edit().putBoolean("location_access_granted", z).apply();
    }
}
