package X;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.ConditionVariable;
import android.os.IBinder;

/* renamed from: X.215  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass215 implements ServiceConnection {
    public final /* synthetic */ ConditionVariable A00;
    public final /* synthetic */ C22880zn A01;
    public final /* synthetic */ AnonymousClass10L A02;

    public AnonymousClass215(ConditionVariable conditionVariable, C22880zn r2, AnonymousClass10L r3) {
        this.A01 = r2;
        this.A00 = conditionVariable;
        this.A02 = r3;
    }

    @Override // android.content.ServiceConnection
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        this.A00.open();
        C22880zn r2 = this.A01;
        r2.A07.A02(this.A02);
        r2.A06.A03();
    }

    @Override // android.content.ServiceConnection
    public void onServiceDisconnected(ComponentName componentName) {
        this.A00.close();
    }
}
