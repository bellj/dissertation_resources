package X;

import com.whatsapp.jid.Jid;

/* renamed from: X.22n  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C456622n extends AbstractC28131Kt {
    public final int A00;
    public final long A01;
    public final Jid A02;
    public final boolean A03;

    public C456622n(Jid jid, String str, int i, long j, boolean z) {
        super(null, str);
        this.A02 = jid;
        this.A01 = j;
        this.A03 = z;
        this.A00 = i;
    }
}
