package X;

import android.view.ViewTreeObserver;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.4o5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC101784o5 implements ViewTreeObserver.OnGlobalLayoutListener {
    public RecyclerView A00;
    public C54812hK A01;
    public boolean A02;

    public /* synthetic */ ViewTreeObserver$OnGlobalLayoutListenerC101784o5(RecyclerView recyclerView, C54812hK r2) {
        this.A00 = recyclerView;
        this.A01 = r2;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        if (this.A02) {
            this.A01.A02(this.A00);
            this.A02 = false;
        }
    }
}
