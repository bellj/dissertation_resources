package X;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.chatinfo.ContactInfoActivity;
import com.whatsapp.chatinfo.view.custom.BusinessChatInfoLayout;

/* renamed from: X.3Uj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C68173Uj implements AbstractC38741od {
    @Override // X.AbstractC38741od
    public /* bridge */ /* synthetic */ void AMK(AnonymousClass5XB r1) {
    }

    @Override // X.AbstractC38741od
    public /* bridge */ /* synthetic */ void ARx(AnonymousClass5XB r1) {
    }

    @Override // X.AbstractC38741od
    public /* bridge */ /* synthetic */ void AS3(AnonymousClass5XB r1) {
    }

    @Override // X.AbstractC38741od
    public void AS7(Bitmap bitmap, AnonymousClass5XB r9, boolean z) {
        AnonymousClass2VA r6 = ((C68193Ul) r9).A02.A00;
        ContactInfoActivity contactInfoActivity = r6.A0Y;
        AnonymousClass2Ew r1 = (AnonymousClass2Ew) contactInfoActivity.findViewById(R.id.content);
        if (r1 instanceof BusinessChatInfoLayout) {
            ((ImageView) AnonymousClass028.A0D(r1, R.id.picture)).setImageBitmap(bitmap);
            r1.A06();
        }
        AnonymousClass0PI r5 = new AnonymousClass0PI(bitmap);
        new AnonymousClass0AI(r5, new C67453Ro(contactInfoActivity)).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, r5.A01);
        r6.A0J = null;
    }
}
