package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2mn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57292mn extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57292mn A04;
    public static volatile AnonymousClass255 A05;
    public int A00;
    public C43261wh A01;
    public String A02 = "";
    public String A03 = "";

    static {
        C57292mn r0 = new C57292mn();
        A04 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r7, Object obj, Object obj2) {
        C81603uH r1;
        switch (r7.ordinal()) {
            case 0:
                return A04;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                C57292mn r9 = (C57292mn) obj2;
                int i = this.A00;
                boolean A1R = C12960it.A1R(i);
                String str = this.A02;
                int i2 = r9.A00;
                this.A02 = r8.Afy(str, r9.A02, A1R, C12960it.A1R(i2));
                this.A03 = r8.Afy(this.A03, r9.A03, C12960it.A1V(i & 2, 2), C12960it.A1V(i2 & 2, 2));
                this.A01 = (C43261wh) r8.Aft(this.A01, r9.A01);
                if (r8 == C463025i.A00) {
                    this.A00 |= r9.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                AnonymousClass254 r92 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        int A03 = r82.A03();
                        if (A03 == 0) {
                            break;
                        } else if (A03 == 10) {
                            String A0A = r82.A0A();
                            this.A00 = 1 | this.A00;
                            this.A02 = A0A;
                        } else if (A03 == 130) {
                            String A0A2 = r82.A0A();
                            this.A00 |= 2;
                            this.A03 = A0A2;
                        } else if (A03 == 138) {
                            if ((this.A00 & 4) == 4) {
                                r1 = (C81603uH) this.A01.A0T();
                            } else {
                                r1 = null;
                            }
                            C43261wh r0 = (C43261wh) AbstractC27091Fz.A0H(r82, r92, C43261wh.A0O);
                            this.A01 = r0;
                            if (r1 != null) {
                                this.A01 = (C43261wh) AbstractC27091Fz.A0C(r1, r0);
                            }
                            this.A00 |= 4;
                        } else if (!A0a(r82, A03)) {
                            break;
                        }
                    } catch (C28971Pt e) {
                        throw AbstractC27091Fz.A0J(e, this);
                    } catch (IOException e2) {
                        throw AbstractC27091Fz.A0K(this, e2);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C57292mn();
            case 5:
                return new C81833ue();
            case 6:
                break;
            case 7:
                if (A05 == null) {
                    synchronized (C57292mn.class) {
                        if (A05 == null) {
                            A05 = AbstractC27091Fz.A09(A04);
                        }
                    }
                }
                return A05;
            default:
                throw C12970iu.A0z();
        }
        return A04;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            i2 = AbstractC27091Fz.A04(1, this.A02, 0);
        }
        if ((this.A00 & 2) == 2) {
            i2 = AbstractC27091Fz.A04(16, this.A03, i2);
        }
        if ((this.A00 & 4) == 4) {
            C43261wh r0 = this.A01;
            if (r0 == null) {
                r0 = C43261wh.A0O;
            }
            i2 = AbstractC27091Fz.A08(r0, 17, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A02);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0I(16, this.A03);
        }
        if ((this.A00 & 4) == 4) {
            C43261wh r0 = this.A01;
            if (r0 == null) {
                r0 = C43261wh.A0O;
            }
            codedOutputStream.A0L(r0, 17);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
