package X;

import com.whatsapp.R;

/* renamed from: X.2xl  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2xl extends AnonymousClass1OY {
    @Override // X.AbstractC28551Oa
    public int getBubbleAlpha() {
        return 153;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: com.whatsapp.TextEmojiLabel */
    /* JADX DEBUG: Multi-variable search result rejected for r0v5, resolved type: android.content.SharedPreferences */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r4v0, types: [boolean, int] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass2xl(android.content.Context r7, X.AbstractC13890kV r8, X.C30371Xd r9) {
        /*
            r6 = this;
            r6.<init>(r7, r8, r9)
            r0 = 2131364478(0x7f0a0a7e, float:1.8348794E38)
            com.whatsapp.TextEmojiLabel r5 = X.C12970iu.A0U(r6, r0)
            boolean r4 = X.AbstractC28491Nn.A07(r5)
            r5.setLongClickable(r4)
            X.18m r1 = r6.A1M
            java.lang.String r0 = "26000015"
            java.lang.String r3 = X.C252018m.A00(r1, r0)
            android.content.Context r2 = r6.getContext()
            r1 = 2131887615(0x7f1205ff, float:1.9409842E38)
            java.lang.Object[] r0 = X.C12970iu.A1b()
            java.lang.String r0 = X.C12960it.A0X(r2, r3, r0, r4, r1)
            X.AnonymousClass1OY.A0Q(r5, r6, r0)
            X.0m6 r0 = r6.A0m
            android.content.SharedPreferences r0 = r0.A00
            java.lang.String r2 = "decryption_failure_views"
            int r1 = r0.getInt(r2, r4)
            X.0m6 r0 = r6.A0m
            int r1 = r1 + 1
            android.content.SharedPreferences r0 = r0.A00
            X.C12960it.A0u(r0, r2, r1)
            X.0yP r1 = r6.A0M
            r0 = 2
            r1.A09(r9, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2xl.<init>(android.content.Context, X.0kV, X.1Xd):void");
    }

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_decryption_failure_left;
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_decryption_failure_left;
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_decryption_failure_right;
    }
}
