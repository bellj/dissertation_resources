package X;

/* renamed from: X.1P9  reason: invalid class name */
/* loaded from: classes2.dex */
public enum AnonymousClass1P9 {
    A02(0),
    A03(1),
    A01(2),
    A06(3),
    A05(4),
    A04(5);
    
    public final int value;

    AnonymousClass1P9(int i) {
        this.value = i;
    }

    public static AnonymousClass1P9 A00(int i) {
        if (i == 0) {
            return A02;
        }
        if (i == 1) {
            return A03;
        }
        if (i == 2) {
            return A01;
        }
        if (i == 3) {
            return A06;
        }
        if (i == 4) {
            return A05;
        }
        if (i != 5) {
            return null;
        }
        return A04;
    }
}
