package X;

/* renamed from: X.1Jn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C27861Jn extends AbstractC27871Jo {
    public static final long serialVersionUID = 1;
    public final byte[] bytes;

    public C27861Jn(byte[] bArr) {
        this.bytes = bArr;
    }

    public int A05() {
        if (!(this instanceof C56832m1)) {
            return 0;
        }
        return ((C56832m1) this).bytesOffset;
    }

    @Override // X.AbstractC27881Jp, java.lang.Object
    public final boolean equals(Object obj) {
        int A03;
        AbstractC27881Jp r8;
        int A032;
        Object r4;
        Object r2;
        if (obj != this) {
            if ((obj instanceof AbstractC27881Jp) && (A03 = A03()) == (A032 = (r8 = (AbstractC27881Jp) obj).A03())) {
                if (A03 != 0) {
                    if (!(obj instanceof C27861Jn)) {
                        return obj.equals(this);
                    }
                    int i = this.hash;
                    int i2 = r8.hash;
                    if (i == 0 || i2 == 0 || i == i2) {
                        if (A03 <= A032) {
                            int i3 = 0 + A03;
                            if (i3 <= A032) {
                                boolean z = r8 instanceof C27861Jn;
                                C27861Jn r82 = (C27861Jn) r8;
                                if (z) {
                                    byte[] bArr = this.bytes;
                                    byte[] bArr2 = r82.bytes;
                                    int A05 = A05();
                                    int i4 = A03 + A05;
                                    int A052 = r82.A05() + 0;
                                    while (A05 < i4) {
                                        if (bArr[A05] != bArr2[A052]) {
                                            return false;
                                        }
                                        A05++;
                                        A052++;
                                    }
                                    return true;
                                }
                                int A00 = AbstractC27881Jp.A00(0, i3, r82.A03());
                                if (A00 == 0) {
                                    r4 = AbstractC27881Jp.A01;
                                } else {
                                    r4 = new C56832m1(r82.bytes, r82.A05() + 0, A00);
                                }
                                int A002 = AbstractC27881Jp.A00(0, A03, A03());
                                if (A002 == 0) {
                                    r2 = AbstractC27881Jp.A01;
                                } else {
                                    r2 = new C56832m1(this.bytes, A05() + 0, A002);
                                }
                                return r4.equals(r2);
                            }
                            StringBuilder sb = new StringBuilder("Ran off end of other: ");
                            sb.append(0);
                            sb.append(", ");
                            sb.append(A03);
                            sb.append(", ");
                            sb.append(A032);
                            throw new IllegalArgumentException(sb.toString());
                        }
                        StringBuilder sb2 = new StringBuilder("Length too large: ");
                        sb2.append(A03);
                        sb2.append(A03);
                        throw new IllegalArgumentException(sb2.toString());
                    }
                }
            }
            return false;
        }
        return true;
    }
}
