package X;

import android.app.Activity;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.TextView;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.redex.RunnableBRunnable0Shape13S0100000_I0_13;
import com.whatsapp.R;
import com.whatsapp.emoji.search.EmojiSearchContainer;
import com.whatsapp.mentions.MentionableEntry;
import java.util.List;

/* renamed from: X.1e0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C33421e0 {
    public final View A00;
    public final ViewTreeObserver.OnGlobalLayoutListener A01;
    public final AbstractC116455Vm A02;
    public final C15270mq A03;
    public final C15330mx A04;
    public final MentionableEntry A05;
    public final C252718t A06;

    public C33421e0(Activity activity, View view, AbstractC15710nm r34, AnonymousClass01d r35, C14820m6 r36, AnonymousClass018 r37, C15370n3 r38, AnonymousClass19M r39, C231510o r40, AnonymousClass193 r41, C14850m9 r42, C16630pM r43, C252718t r44, String str, List list) {
        C1097152t r2 = new C1097152t(this);
        this.A02 = r2;
        ViewTreeObserver$OnGlobalLayoutListenerC101664nt r1 = new ViewTreeObserver$OnGlobalLayoutListenerC101664nt(this);
        this.A01 = r1;
        this.A00 = view;
        this.A06 = r44;
        MentionableEntry mentionableEntry = (MentionableEntry) AnonymousClass028.A0D(view, R.id.caption);
        this.A05 = mentionableEntry;
        mentionableEntry.setInputEnterAction(6);
        mentionableEntry.setFilters(new InputFilter[]{new C100654mG(EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH)});
        mentionableEntry.setOnEditorActionListener(new TextView.OnEditorActionListener() { // from class: X.4pX
            @Override // android.widget.TextView.OnEditorActionListener
            public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                C33421e0 r22 = C33421e0.this;
                if (keyEvent == null || keyEvent.getKeyCode() != 66) {
                    return false;
                }
                r22.A05.A03();
                return true;
            }
        });
        mentionableEntry.addTextChangedListener(new AnonymousClass367(mentionableEntry, (TextView) view.findViewById(R.id.counter), r35, r37, r39, r43, EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH, 30, true));
        if (r38 != null && r38.A0K()) {
            mentionableEntry.A04 = view;
            mentionableEntry.A0D((ViewGroup) AnonymousClass028.A0D(view, R.id.mention_attach), (C15580nU) r38.A0B(C15580nU.class), r42.A07(815), false, false);
        }
        if (!TextUtils.isEmpty(str)) {
            mentionableEntry.setMentionableText(str, list);
        }
        ImageButton imageButton = (ImageButton) view.findViewById(R.id.emoji_picker_btn);
        C15270mq r8 = new C15270mq(activity, imageButton, r34, (AbstractC49822Mw) activity.findViewById(R.id.main), mentionableEntry, r35, r36, r37, r39, r40, r41, r43, r44);
        this.A03 = r8;
        r8.A00 = R.drawable.ib_emoji;
        r8.A03 = R.drawable.ib_keyboard;
        imageButton.setImageDrawable(AnonymousClass2GE.A01(imageButton.getContext(), R.drawable.ib_emoji, R.color.ibEmojiIconTint));
        C15330mx r6 = new C15330mx(activity, r37, r39, r8, r40, (EmojiSearchContainer) view.findViewById(R.id.emoji_search_container), r43);
        this.A04 = r6;
        r6.A00 = new AbstractC14020ki() { // from class: X.56q
            @Override // X.AbstractC14020ki
            public final void APd(C37471mS r3) {
                C33421e0.this.A02.APc(r3.A00);
            }
        };
        r8.A0C(r2);
        r8.A0E = new RunnableBRunnable0Shape13S0100000_I0_13(this, 6);
        view.getViewTreeObserver().addOnGlobalLayoutListener(r1);
    }
}
