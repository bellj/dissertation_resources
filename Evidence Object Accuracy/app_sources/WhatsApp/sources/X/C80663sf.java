package X;

import android.view.View;
import com.whatsapp.storage.StorageUsageGallerySortBottomSheet;

/* renamed from: X.3sf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C80663sf extends AnonymousClass2UH {
    public final /* synthetic */ StorageUsageGallerySortBottomSheet A00;

    @Override // X.AnonymousClass2UH
    public void A00(View view, float f) {
    }

    public C80663sf(StorageUsageGallerySortBottomSheet storageUsageGallerySortBottomSheet) {
        this.A00 = storageUsageGallerySortBottomSheet;
    }

    @Override // X.AnonymousClass2UH
    public void A01(View view, int i) {
        if (i == 4 || i == 5) {
            this.A00.A1B();
        }
    }
}
