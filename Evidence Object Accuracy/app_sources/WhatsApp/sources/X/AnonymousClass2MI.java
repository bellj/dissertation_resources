package X;

/* renamed from: X.2MI  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2MI extends AbstractC28131Kt {
    public final Boolean A00;
    public final Integer A01;
    public final Long A02;

    public AnonymousClass2MI(Boolean bool, Integer num, Long l) {
        super(null, null);
        this.A00 = bool;
        this.A01 = num;
        this.A02 = l;
    }
}
