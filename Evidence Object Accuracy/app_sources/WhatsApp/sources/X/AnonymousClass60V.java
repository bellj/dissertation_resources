package X;

import android.content.Context;
import com.whatsapp.R;

/* renamed from: X.60V  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass60V {
    public int A00;
    public final String A01;

    public AnonymousClass60V(int i) {
        this.A00 = i;
        this.A01 = null;
    }

    public AnonymousClass60V(int i, String str) {
        this.A00 = i;
        this.A01 = str;
    }

    public int A00() {
        return this.A00;
    }

    public String A01(Context context) {
        String str = this.A01;
        return str == null ? context.getString(this.A00) : str;
    }

    public void A02() {
        this.A00 = R.string.payments_generic_error;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AnonymousClass60V)) {
            return false;
        }
        AnonymousClass60V r4 = (AnonymousClass60V) obj;
        if (this.A00 != r4.A00 || !C29941Vi.A00(this.A01, r4.A01)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        Object[] A1a = C12980iv.A1a();
        C12960it.A1O(A1a, this.A00);
        return C12960it.A06(this.A01, A1a);
    }
}
