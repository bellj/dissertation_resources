package X;

import android.widget.CompoundButton;
import com.facebook.redex.RunnableBRunnable0Shape6S0100000_I0_6;
import com.whatsapp.MuteDialogFragment;
import com.whatsapp.group.GroupChatInfo;

/* renamed from: X.1jD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C36031jD implements CompoundButton.OnCheckedChangeListener {
    public final /* synthetic */ GroupChatInfo A00;

    public C36031jD(GroupChatInfo groupChatInfo) {
        this.A00 = groupChatInfo;
    }

    @Override // android.widget.CompoundButton.OnCheckedChangeListener
    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        GroupChatInfo groupChatInfo = this.A00;
        groupChatInfo.A2i();
        if (z) {
            groupChatInfo.Adl(MuteDialogFragment.A00(groupChatInfo.A1C), null);
        } else {
            ((ActivityC13830kP) groupChatInfo).A05.Ab2(new RunnableBRunnable0Shape6S0100000_I0_6(this, 41));
        }
    }
}
