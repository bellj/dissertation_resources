package X;

import java.util.Collection;

/* renamed from: X.194  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass194 {
    public AnonymousClass5UG A00;
    public boolean A01;
    public boolean A02 = false;
    public final AnonymousClass192 A03;

    public AnonymousClass194(AnonymousClass192 r2) {
        this.A03 = r2;
    }

    public AnonymousClass4VM A00(String str, Object[] objArr, boolean z) {
        AnonymousClass4VM r3 = new AnonymousClass4VM();
        if (this.A02) {
            AnonymousClass192 r5 = this.A03;
            AnonymousClass575 r6 = new AnonymousClass5UH() { // from class: X.575
                @Override // X.AnonymousClass5UH
                public final void AVL(Collection collection) {
                    AnonymousClass4VM r1 = AnonymousClass4VM.this;
                    AnonymousClass009.A01();
                    if (collection != null) {
                        r1.A01.addAll(collection);
                    }
                    AnonymousClass1KY r0 = r1.A00;
                    if (r0 != null) {
                        r0.AVQ(r1);
                    }
                }
            };
            AnonymousClass009.A01();
            C626037w r1 = r5.A00;
            if (r1 != null) {
                r1.A03(true);
            }
            C626037w r4 = new C626037w(r5, r6, r5.A09, objArr, z);
            r5.A00 = r4;
            r5.A0A.Aaz(r4, str);
        }
        return r3;
    }

    public synchronized void A01() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass192 r1 = this.A03;
            AnonymousClass4V0 r0 = new AnonymousClass4V0(this);
            AnonymousClass009.A01();
            r1.A0A.Aaz(new C627738n(r1, r0), new String[0]);
        }
    }
}
