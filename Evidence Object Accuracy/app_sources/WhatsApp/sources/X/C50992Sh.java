package X;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.WaMediaThumbnailView;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.2Sh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C50992Sh extends AnonymousClass02M {
    public final LayoutInflater A00;
    public final C457522x A01;
    public final List A02 = new ArrayList();

    public C50992Sh(LayoutInflater layoutInflater, C457522x r3) {
        C16700pc.A0E(r3, 2);
        this.A00 = layoutInflater;
        this.A01 = r3;
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void A0A(AnonymousClass03U r3) {
        C54972ha r32 = (C54972ha) r3;
        C16700pc.A0E(r32, 0);
        WaMediaThumbnailView waMediaThumbnailView = r32.A03;
        waMediaThumbnailView.setImageDrawable(null);
        waMediaThumbnailView.A01 = null;
        waMediaThumbnailView.setThumbnail(null);
        waMediaThumbnailView.setTag(null);
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A02.size();
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r6, int i) {
        AnonymousClass23D r2;
        C54972ha r62 = (C54972ha) r6;
        C16700pc.A0E(r62, 0);
        AbstractC35611iN r4 = (AbstractC35611iN) this.A02.get(i);
        WaMediaThumbnailView waMediaThumbnailView = r62.A03;
        waMediaThumbnailView.A01 = r4;
        Object tag = waMediaThumbnailView.getTag();
        if ((tag instanceof AnonymousClass23D) && (r2 = (AnonymousClass23D) tag) != null) {
            r62.A04.A01(r2);
        }
        if (r4 != null) {
            waMediaThumbnailView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            AnonymousClass3X3 r22 = new AnonymousClass3X3(r4, r62);
            waMediaThumbnailView.setTag(r22);
            r62.A04.A02(r22, new AnonymousClass3XB(r4, r62, r22));
            return;
        }
        waMediaThumbnailView.setScaleType(ImageView.ScaleType.CENTER);
        waMediaThumbnailView.setBackgroundColor(r62.A01);
        waMediaThumbnailView.setImageDrawable(null);
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        C16700pc.A0E(viewGroup, 0);
        View inflate = this.A00.inflate(R.layout.selected_media_item_view, viewGroup, false);
        C16700pc.A0B(inflate);
        return new C54972ha(inflate, this.A01);
    }
}
