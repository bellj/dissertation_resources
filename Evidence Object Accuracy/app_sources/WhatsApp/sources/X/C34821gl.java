package X;

import android.util.Pair;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.1gl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C34821gl extends AbstractC30271Wt {
    public C34871gq A00;
    public boolean A01 = false;

    public C34821gl(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 38, j);
    }

    public HashMap A14() {
        C34871gq r0 = this.A00;
        AnonymousClass009.A05(r0);
        HashMap hashMap = new HashMap();
        for (C34861gp r02 : r0.A00) {
            Pair A00 = AnonymousClass1JZ.A00(r02);
            hashMap.put(A00.first, A00.second);
        }
        return hashMap;
    }

    public void A15(HashMap hashMap) {
        C27821Ji r7;
        AnonymousClass1G4 A0T = C34871gq.A01.A0T();
        for (Map.Entry entry : hashMap.entrySet()) {
            AnonymousClass1JR r1 = (AnonymousClass1JR) entry.getKey();
            AnonymousClass1JZ r0 = (AnonymousClass1JZ) entry.getValue();
            if (r0 != null) {
                r7 = r0.A00;
            } else {
                r7 = null;
            }
            AnonymousClass1G4 A0T2 = C34861gp.A03.A0T();
            C34851go A02 = r1.A02();
            A0T2.A03();
            C34861gp r12 = (C34861gp) A0T2.A00;
            r12.A02 = A02;
            r12.A00 |= 1;
            if (r7 != null) {
                AnonymousClass1G4 A0T3 = C57282mm.A04.A0T();
                byte[] bArr = r7.A02;
                AbstractC27881Jp A01 = AbstractC27881Jp.A01(bArr, 0, bArr.length);
                A0T3.A03();
                C57282mm r13 = (C57282mm) A0T3.A00;
                r13.A00 |= 1;
                r13.A02 = A01;
                long j = r7.A00;
                A0T3.A03();
                C57282mm r5 = (C57282mm) A0T3.A00;
                r5.A00 |= 4;
                r5.A01 = j;
                C34311fw A012 = r7.A01.A01();
                A0T3.A03();
                C57282mm r14 = (C57282mm) A0T3.A00;
                r14.A03 = A012;
                r14.A00 |= 2;
                A0T2.A03();
                C34861gp r15 = (C34861gp) A0T2.A00;
                r15.A01 = (C57282mm) A0T3.A02();
                r15.A00 |= 2;
            }
            AbstractC27091Fz A022 = A0T2.A02();
            A0T.A03();
            C34871gq r2 = (C34871gq) A0T.A00;
            AnonymousClass1K6 r16 = r2.A00;
            if (!((AnonymousClass1K7) r16).A00) {
                r16 = AbstractC27091Fz.A0G(r16);
                r2.A00 = r16;
            }
            r16.add(A022);
        }
        this.A00 = (C34871gq) A0T.A02();
    }

    @Override // X.AbstractC16420oz
    public void A6k(C39971qq r4) {
        C56882m6 r2 = (C56882m6) C57692nT.A0D.A0T();
        r2.A05(EnumC87324Bb.A03);
        C34871gq r0 = this.A00;
        r2.A03();
        C57692nT r1 = (C57692nT) r2.A00;
        r1.A07 = r0;
        r1.A00 |= 32;
        r4.A03.A09((C57692nT) r2.A02());
    }
}
