package X;

import com.whatsapp.mediaview.MediaViewFragment;
import com.whatsapp.mediaview.PhotoView;

/* renamed from: X.33W  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass33W extends AnonymousClass3NJ {
    public final /* synthetic */ MediaViewFragment A00;
    public final /* synthetic */ PhotoView A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass33W(MediaViewFragment mediaViewFragment, PhotoView photoView, PhotoView photoView2, AbstractC16130oV r4) {
        super(photoView, r4);
        this.A00 = mediaViewFragment;
        this.A01 = photoView2;
    }
}
