package X;

import android.content.Context;
import android.view.View;
import com.facebook.redex.RunnableBRunnable0Shape0S0102000_I0;
import com.whatsapp.text.ReadMoreTextView;

/* renamed from: X.3xW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83613xW extends AbstractC52172aN {
    public final /* synthetic */ RunnableBRunnable0Shape0S0102000_I0 A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C83613xW(Context context, RunnableBRunnable0Shape0S0102000_I0 runnableBRunnable0Shape0S0102000_I0, int i) {
        super(context, i);
        this.A00 = runnableBRunnable0Shape0S0102000_I0;
    }

    @Override // X.AbstractC116465Vn
    public void onClick(View view) {
        RunnableBRunnable0Shape0S0102000_I0 runnableBRunnable0Shape0S0102000_I0 = this.A00;
        ReadMoreTextView readMoreTextView = (ReadMoreTextView) runnableBRunnable0Shape0S0102000_I0.A02;
        AbstractC116255Us r0 = readMoreTextView.A02;
        if (r0 == null || !r0.AO3()) {
            readMoreTextView.setExpanded(true);
            runnableBRunnable0Shape0S0102000_I0.A01 = 0;
            runnableBRunnable0Shape0S0102000_I0.A00 = 0;
            readMoreTextView.A08.removeCallbacks(readMoreTextView.A0A);
        }
    }
}
