package X;

import android.os.SystemClock;
import com.whatsapp.R;
import java.util.HashMap;
import java.util.Timer;

/* renamed from: X.1If  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C27611If {
    public static HashMap A0O = new HashMap();
    public long A00 = SystemClock.elapsedRealtime();
    public AnonymousClass1J9 A01 = new AnonymousClass1J9(this);
    public Long A02;
    public boolean A03;
    public boolean A04;
    public byte[] A05;
    public byte[] A06;
    public final C14900mE A07;
    public final C15570nT A08;
    public final C15550nR A09;
    public final AnonymousClass10S A0A;
    public final AnonymousClass10T A0B;
    public final AnonymousClass10V A0C;
    public final C14830m7 A0D;
    public final C15650ng A0E;
    public final C15600nX A0F;
    public final AnonymousClass10Y A0G;
    public final C16120oU A0H;
    public final AbstractC14640lm A0I;
    public final AnonymousClass10Z A0J;
    public final AnonymousClass1P4 A0K;
    public final C22140ya A0L;
    public final C14860mA A0M;
    public final boolean A0N;

    public C27611If(C14900mE r5, C15570nT r6, C15550nR r7, AnonymousClass10S r8, AnonymousClass10T r9, AnonymousClass10V r10, C14830m7 r11, C15650ng r12, C15600nX r13, AnonymousClass10Y r14, C16120oU r15, AbstractC14640lm r16, AnonymousClass10Z r17, AnonymousClass1P4 r18, C22140ya r19, C14860mA r20, byte[] bArr, byte[] bArr2, boolean z) {
        this.A0D = r11;
        this.A07 = r5;
        this.A08 = r6;
        this.A0H = r15;
        this.A0M = r20;
        this.A09 = r7;
        this.A0A = r8;
        this.A0E = r12;
        this.A0G = r14;
        this.A0B = r9;
        this.A0C = r10;
        this.A0J = r17;
        this.A0L = r19;
        this.A0F = r13;
        this.A0I = r16;
        this.A05 = bArr;
        this.A06 = bArr2;
        this.A0K = r18;
        this.A0N = z;
        Long valueOf = Long.valueOf(System.currentTimeMillis());
        this.A02 = valueOf;
        A0O.put(valueOf.toString(), this);
        new Timer().schedule(this.A01, 32000);
    }

    public static boolean A00(AbstractC14640lm r3) {
        if (r3 != null) {
            for (C27611If r0 : A0O.values()) {
                if (r3.equals(r0.A0I)) {
                    return true;
                }
            }
        }
        return false;
    }

    public final void A01(int i) {
        int length;
        C613530a r4 = new C613530a();
        byte[] bArr = this.A05;
        int i2 = 0;
        if (bArr == null) {
            length = 0;
        } else {
            length = bArr.length;
        }
        byte[] bArr2 = this.A06;
        if (bArr2 != null) {
            i2 = bArr2.length;
        }
        r4.A01 = Double.valueOf((double) (length + i2));
        r4.A03 = Long.valueOf(SystemClock.elapsedRealtime() - this.A00);
        r4.A02 = Integer.valueOf(i);
        r4.A00 = Boolean.valueOf(this.A0N);
        this.A0H.A06(r4);
    }

    public final void A02(AbstractC14640lm r3) {
        this.A0C.A05.A04(this.A09.A0B(r3));
        this.A0A.A05(r3);
    }

    public final void A03(AbstractC14640lm r4, int i) {
        A02(r4);
        this.A0J.A0K.A00(r4, i);
        C14900mE r2 = this.A07;
        boolean A0J = C15380n4.A0J(r4);
        int i2 = R.string.failed_update_profile_photo;
        if (A0J) {
            i2 = R.string.failed_update_photo;
        }
        r2.A07(i2, 0);
    }
}
