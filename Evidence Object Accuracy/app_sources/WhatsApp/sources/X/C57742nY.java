package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2nY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57742nY extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57742nY A09;
    public static volatile AnonymousClass255 A0A;
    public int A00;
    public int A01;
    public AnonymousClass1K6 A02 = AnonymousClass277.A01;
    public C43261wh A03;
    public C57362mu A04;
    public String A05 = "";
    public String A06 = "";
    public String A07 = "";
    public String A08 = "";

    static {
        C57742nY r0 = new C57742nY();
        A09 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r7, Object obj, Object obj2) {
        C81603uH r1;
        C82113v6 r12;
        switch (r7.ordinal()) {
            case 0:
                return A09;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                C57742nY r9 = (C57742nY) obj2;
                int i = this.A00;
                boolean A1R = C12960it.A1R(i);
                String str = this.A08;
                int i2 = r9.A00;
                this.A08 = r8.Afy(str, r9.A08, A1R, C12960it.A1R(i2));
                this.A06 = r8.Afy(this.A06, r9.A06, C12960it.A1V(i & 2, 2), C12960it.A1V(i2 & 2, 2));
                this.A05 = r8.Afy(this.A05, r9.A05, C12960it.A1V(i & 4, 4), C12960it.A1V(i2 & 4, 4));
                this.A01 = r8.Afp(this.A01, r9.A01, C12960it.A1V(i & 8, 8), C12960it.A1V(i2 & 8, 8));
                this.A02 = r8.Afr(this.A02, r9.A02);
                this.A04 = (C57362mu) r8.Aft(this.A04, r9.A04);
                this.A07 = r8.Afy(this.A07, r9.A07, C12960it.A1V(this.A00 & 32, 32), C12960it.A1V(r9.A00 & 32, 32));
                this.A03 = (C43261wh) r8.Aft(this.A03, r9.A03);
                if (r8 == C463025i.A00) {
                    this.A00 |= r9.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                AnonymousClass254 r92 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        int A03 = r82.A03();
                        if (A03 == 0) {
                            break;
                        } else if (A03 == 10) {
                            String A0A2 = r82.A0A();
                            this.A00 = 1 | this.A00;
                            this.A08 = A0A2;
                        } else if (A03 == 18) {
                            String A0A3 = r82.A0A();
                            this.A00 |= 2;
                            this.A06 = A0A3;
                        } else if (A03 == 26) {
                            String A0A4 = r82.A0A();
                            this.A00 |= 4;
                            this.A05 = A0A4;
                        } else if (A03 == 32) {
                            int A02 = r82.A02();
                            if (A02 == 0 || A02 == 1 || A02 == 2) {
                                this.A00 |= 8;
                                this.A01 = A02;
                            } else {
                                super.A0X(4, A02);
                            }
                        } else if (A03 == 42) {
                            AnonymousClass1K6 r13 = this.A02;
                            if (!((AnonymousClass1K7) r13).A00) {
                                r13 = AbstractC27091Fz.A0G(r13);
                                this.A02 = r13;
                            }
                            r13.add((C57202me) AbstractC27091Fz.A0H(r82, r92, C57202me.A03));
                        } else if (A03 == 50) {
                            if ((this.A00 & 16) == 16) {
                                r12 = (C82113v6) this.A04.A0T();
                            } else {
                                r12 = null;
                            }
                            C57362mu r0 = (C57362mu) AbstractC27091Fz.A0H(r82, r92, C57362mu.A04);
                            this.A04 = r0;
                            if (r12 != null) {
                                this.A04 = (C57362mu) AbstractC27091Fz.A0C(r12, r0);
                            }
                            this.A00 |= 16;
                        } else if (A03 == 58) {
                            String A0A5 = r82.A0A();
                            this.A00 |= 32;
                            this.A07 = A0A5;
                        } else if (A03 == 66) {
                            if ((this.A00 & 64) == 64) {
                                r1 = (C81603uH) this.A03.A0T();
                            } else {
                                r1 = null;
                            }
                            C43261wh r02 = (C43261wh) AbstractC27091Fz.A0H(r82, r92, C43261wh.A0O);
                            this.A03 = r02;
                            if (r1 != null) {
                                this.A03 = (C43261wh) AbstractC27091Fz.A0C(r1, r02);
                            }
                            this.A00 |= 64;
                        } else if (!A0a(r82, A03)) {
                            break;
                        }
                    } catch (C28971Pt e) {
                        throw AbstractC27091Fz.A0J(e, this);
                    } catch (IOException e2) {
                        throw AbstractC27091Fz.A0K(this, e2);
                    }
                }
                break;
            case 3:
                AbstractC27091Fz.A0R(this.A02);
                return null;
            case 4:
                return new C57742nY();
            case 5:
                return new C82083v3();
            case 6:
                break;
            case 7:
                if (A0A == null) {
                    synchronized (C57742nY.class) {
                        if (A0A == null) {
                            A0A = AbstractC27091Fz.A09(A09);
                        }
                    }
                }
                return A0A;
            default:
                throw C12970iu.A0z();
        }
        return A09;
    }

    public AnonymousClass4B6 A0b() {
        int i = this.A01;
        if (i != 0) {
            if (i == 1) {
                return AnonymousClass4B6.A02;
            }
            if (i == 2) {
                return AnonymousClass4B6.A01;
            }
        }
        return AnonymousClass4B6.A03;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i;
        int i2 = ((AbstractC27091Fz) this).A00;
        if (i2 != -1) {
            return i2;
        }
        if ((this.A00 & 1) == 1) {
            i = CodedOutputStream.A07(1, this.A08) + 0;
        } else {
            i = 0;
        }
        if ((this.A00 & 2) == 2) {
            i = AbstractC27091Fz.A04(2, this.A06, i);
        }
        if ((this.A00 & 4) == 4) {
            i = AbstractC27091Fz.A04(3, this.A05, i);
        }
        if ((this.A00 & 8) == 8) {
            i = AbstractC27091Fz.A03(4, this.A01, i);
        }
        for (int i3 = 0; i3 < this.A02.size(); i3++) {
            i = AbstractC27091Fz.A08((AnonymousClass1G1) this.A02.get(i3), 5, i);
        }
        if ((this.A00 & 16) == 16) {
            C57362mu r0 = this.A04;
            if (r0 == null) {
                r0 = C57362mu.A04;
            }
            i = AbstractC27091Fz.A08(r0, 6, i);
        }
        if ((this.A00 & 32) == 32) {
            i = AbstractC27091Fz.A04(7, this.A07, i);
        }
        if ((this.A00 & 64) == 64) {
            C43261wh r02 = this.A03;
            if (r02 == null) {
                r02 = C43261wh.A0O;
            }
            i = AbstractC27091Fz.A08(r02, 8, i);
        }
        return AbstractC27091Fz.A07(this, i);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A08);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0I(2, this.A06);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0I(3, this.A05);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0E(4, this.A01);
        }
        int i = 0;
        while (i < this.A02.size()) {
            i = AbstractC27091Fz.A06(codedOutputStream, this.A02, i, 5);
        }
        if ((this.A00 & 16) == 16) {
            C57362mu r0 = this.A04;
            if (r0 == null) {
                r0 = C57362mu.A04;
            }
            codedOutputStream.A0L(r0, 6);
        }
        if ((this.A00 & 32) == 32) {
            codedOutputStream.A0I(7, this.A07);
        }
        if ((this.A00 & 64) == 64) {
            C43261wh r02 = this.A03;
            if (r02 == null) {
                r02 = C43261wh.A0O;
            }
            codedOutputStream.A0L(r02, 8);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
