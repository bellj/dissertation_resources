package X;

import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

/* renamed from: X.0f4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C10710f4 implements AbstractC02760Dw, AbstractC02780Dy, AbstractC02770Dx {
    public static final /* synthetic */ AtomicReferenceFieldUpdater A00 = AtomicReferenceFieldUpdater.newUpdater(C10710f4.class, Object.class, "_state");
    public volatile /* synthetic */ Object _parentHandle = null;
    public volatile /* synthetic */ Object _state = C93134Zf.A00;

    public String A0F() {
        return "Job was cancelled";
    }

    public boolean A0b() {
        return true;
    }

    public boolean A0c() {
        return false;
    }

    public boolean A0j(Throwable th) {
        return false;
    }

    public static final String A00(Object obj) {
        if (obj instanceof AnonymousClass5F3) {
            AnonymousClass5F3 r2 = (AnonymousClass5F3) obj;
            if (r2.A08()) {
                return "Cancelling";
            }
            if (r2.A09()) {
                return "Completing";
            }
            return "Active";
        } else if (!(obj instanceof AnonymousClass5WP)) {
            return obj instanceof C94894ci ? "Cancelled" : "Completed";
        } else {
            if (!((AnonymousClass5WP) obj).AJD()) {
                return "New";
            }
            return "Active";
        }
    }

    public static final Throwable A02(Object obj) {
        C94894ci r2;
        if (!(obj instanceof C94894ci) || (r2 = (C94894ci) obj) == null) {
            return null;
        }
        return r2.A00;
    }

    public static final C114295Ky A04(C94524by r1) {
        while (r1.A09()) {
            r1 = r1.A04();
        }
        while (true) {
            r1 = r1.A03();
            if (!r1.A09()) {
                if (r1 instanceof C114295Ky) {
                    return (C114295Ky) r1;
                }
                if (r1 instanceof AnonymousClass5L9) {
                    return null;
                }
            }
        }
    }

    public static /* synthetic */ AnonymousClass5VG A05(AnonymousClass1J7 r2, AbstractC02760Dw r3, int i, boolean z) {
        boolean z2 = false;
        if ((i & 1) != 0) {
            z = false;
        }
        if ((i & 2) != 0) {
            z2 = true;
        }
        return r3.AJB(r2, z, z2);
    }

    public static final void A07(Throwable th, List list) {
        if (list.size() > 1) {
            Set newSetFromMap = Collections.newSetFromMap(new IdentityHashMap(list.size()));
            Iterator it = list.iterator();
            while (it.hasNext()) {
                Throwable th2 = (Throwable) it.next();
                if (th2 != th && th2 != th && !(th2 instanceof CancellationException) && newSetFromMap.add(th2)) {
                    C88164En.A00(th, th2);
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x002f A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int A08(java.lang.Object r5) {
        /*
            r4 = this;
            boolean r0 = r5 instanceof X.AnonymousClass5F1
            r3 = -1
            r2 = 1
            r1 = 0
            if (r0 == 0) goto L_0x001b
            r0 = r5
            X.5F1 r0 = (X.AnonymousClass5F1) r0
            boolean r0 = r0.AJD()
            if (r0 != 0) goto L_0x0030
            java.util.concurrent.atomic.AtomicReferenceFieldUpdater r1 = X.C10710f4.A00
            X.5F1 r0 = X.C93134Zf.A00
            boolean r0 = X.AnonymousClass0KN.A00(r4, r5, r0, r1)
            if (r0 != 0) goto L_0x002f
            return r3
        L_0x001b:
            boolean r0 = r5 instanceof X.AnonymousClass5F2
            if (r0 == 0) goto L_0x0030
            java.util.concurrent.atomic.AtomicReferenceFieldUpdater r1 = X.C10710f4.A00
            r0 = r5
            X.5F2 r0 = (X.AnonymousClass5F2) r0
            X.5L9 r0 = r0.ADu()
            boolean r0 = X.AnonymousClass0KN.A00(r4, r5, r0, r1)
            if (r0 != 0) goto L_0x002f
            return r3
        L_0x002f:
            return r2
        L_0x0030:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C10710f4.A08(java.lang.Object):int");
    }

    public final Object A09() {
        while (true) {
            Object obj = this._state;
            if (!(obj instanceof AnonymousClass4WM)) {
                return obj;
            }
            ((AnonymousClass4WM) obj).A00(this);
        }
    }

    public final Object A0A(Object obj) {
        Object A0C;
        do {
            Object A09 = A09();
            if (!(A09 instanceof AnonymousClass5WP) || ((A09 instanceof AnonymousClass5F3) && ((AnonymousClass5F3) A09).A09())) {
                return C93134Zf.A01;
            }
            A0C = A0C(A09, new C94894ci(A0I(obj)));
        } while (A0C == C93134Zf.A02);
        return A0C;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0053, code lost:
        return X.C93134Zf.A01;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object A0B(java.lang.Object r7) {
        /*
            r6 = this;
            r3 = 0
            r5 = r3
        L_0x0002:
            java.lang.Object r2 = r6.A09()
            boolean r0 = r2 instanceof X.AnonymousClass5F3
            if (r0 == 0) goto L_0x0038
            monitor-enter(r2)
            r4 = r2
            X.5F3 r4 = (X.AnonymousClass5F3) r4     // Catch: all -> 0x0068
            boolean r0 = r4.A0A()     // Catch: all -> 0x0068
            if (r0 == 0) goto L_0x0017
            X.4VA r0 = X.C93134Zf.A05     // Catch: all -> 0x0068
            goto L_0x0066
        L_0x0017:
            boolean r0 = r4.A08()     // Catch: all -> 0x0068
            if (r5 != 0) goto L_0x0021
            java.lang.Throwable r5 = r6.A0I(r7)     // Catch: all -> 0x0068
        L_0x0021:
            r4.A06(r5)     // Catch: all -> 0x0068
            java.lang.Throwable r1 = r4.A02()     // Catch: all -> 0x0068
            r0 = r0 ^ 1
            if (r0 == 0) goto L_0x002d
            r3 = r1
        L_0x002d:
            monitor-exit(r2)
            if (r3 == 0) goto L_0x0051
            X.5L9 r0 = r4.ADu()
            r6.A0U(r3, r0)
            goto L_0x0051
        L_0x0038:
            boolean r0 = r2 instanceof X.AnonymousClass5WP
            if (r0 == 0) goto L_0x0077
            if (r5 != 0) goto L_0x0042
            java.lang.Throwable r5 = r6.A0I(r7)
        L_0x0042:
            r1 = r2
            X.5WP r1 = (X.AnonymousClass5WP) r1
            boolean r0 = r1.AJD()
            if (r0 == 0) goto L_0x0054
            boolean r0 = r6.A0l(r5, r1)
            if (r0 == 0) goto L_0x0002
        L_0x0051:
            X.4VA r0 = X.C93134Zf.A01
            return r0
        L_0x0054:
            X.4ci r0 = new X.4ci
            r0.<init>(r5)
            java.lang.Object r1 = r6.A0C(r2, r0)
            X.4VA r0 = X.C93134Zf.A01
            if (r1 == r0) goto L_0x006b
            X.4VA r0 = X.C93134Zf.A02
            if (r1 == r0) goto L_0x0002
            return r1
        L_0x0066:
            monitor-exit(r2)
            return r0
        L_0x0068:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x006b:
            java.lang.String r0 = "Cannot happen in "
            java.lang.String r1 = X.C16700pc.A08(r0, r2)
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>(r1)
            throw r0
        L_0x0077:
            X.4VA r0 = X.C93134Zf.A05
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C10710f4.A0B(java.lang.Object):java.lang.Object");
    }

    public final Object A0C(Object obj, Object obj2) {
        if (!(obj instanceof AnonymousClass5WP)) {
            return C93134Zf.A01;
        }
        if ((!(obj instanceof AnonymousClass5F1) && !(obj instanceof AbstractC114145Kj)) || (obj instanceof C114295Ky) || (obj2 instanceof C94894ci)) {
            return A0D(obj2, (AnonymousClass5WP) obj);
        }
        if (A0g(obj2, (AnonymousClass5WP) obj)) {
            return obj2;
        }
        return C93134Zf.A02;
    }

    public final Object A0D(Object obj, AnonymousClass5WP r8) {
        AnonymousClass5F3 r3;
        AnonymousClass4VA r0;
        AnonymousClass5L9 A0O = A0O(r8);
        if (A0O == null) {
            return C93134Zf.A02;
        }
        Throwable th = null;
        if (r8 instanceof AnonymousClass5F3) {
            r3 = (AnonymousClass5F3) r8;
        } else {
            r3 = null;
        }
        if (r3 == null) {
            r3 = new AnonymousClass5F3(null, A0O);
        }
        synchronized (r3) {
            if (r3.A09()) {
                r0 = C93134Zf.A01;
            } else {
                r3.A04();
                if (r3 == r8 || AnonymousClass0KN.A00(this, r8, r3, A00)) {
                    boolean A08 = r3.A08();
                    C94894ci r02 = obj instanceof C94894ci ? (C94894ci) obj : null;
                    if (r02 != null) {
                        r3.A06(r02.A00);
                    }
                    th = r3.A02();
                    if (true ^ A08) {
                    }
                    if (th != null) {
                        A0U(th, A0O);
                    }
                    C114295Ky A0M = A0M(r8);
                    if (A0M == null || !A0f(obj, A0M, r3)) {
                        return A0E(obj, r3);
                    }
                    return C93134Zf.A03;
                }
                r0 = C93134Zf.A02;
            }
            return r0;
        }
    }

    public final Object A0E(Object obj, AnonymousClass5F3 r6) {
        C94894ci r0;
        Throwable A0J;
        boolean z = true;
        Throwable th = null;
        if (obj instanceof C94894ci) {
            r0 = (C94894ci) obj;
        } else {
            r0 = null;
        }
        if (r0 != null) {
            th = r0.A00;
        }
        synchronized (r6) {
            List A03 = r6.A03(th);
            A0J = A0J(A03, r6);
            if (A0J != null) {
                A07(A0J, A03);
            }
        }
        if (!(A0J == null || A0J == th)) {
            obj = new C94894ci(A0J);
        }
        if (A0J != null) {
            if (!A0k(A0J) && !A0j(A0J)) {
                z = false;
            }
            if (z) {
                if (obj != null) {
                    ((C94894ci) obj).A00();
                } else {
                    throw new NullPointerException("null cannot be cast to non-null type kotlinx.coroutines.CompletedExceptionally");
                }
            }
        }
        AnonymousClass0KN.A00(this, r6, C93134Zf.A00(obj), A00);
        A0R(obj, r6);
        return obj;
    }

    public String A0G() {
        return getClass().getSimpleName();
    }

    public final String A0H() {
        StringBuilder sb = new StringBuilder();
        sb.append(A0G());
        sb.append('{');
        sb.append(A00(A09()));
        sb.append('}');
        return sb.toString();
    }

    public final Throwable A0I(Object obj) {
        if (!(obj instanceof Throwable)) {
            return ((AbstractC02770Dx) obj).ABM();
        }
        Throwable th = (Throwable) obj;
        if (th == null) {
            return new C71763dT(A0F(), null, this);
        }
        return th;
    }

    public final Throwable A0J(List list, AnonymousClass5F3 r7) {
        Object obj;
        if (!list.isEmpty()) {
            Iterator it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (!(obj instanceof CancellationException)) {
                    break;
                }
            }
            Throwable th = (Throwable) obj;
            if (th == null) {
                return (Throwable) list.get(0);
            }
            return th;
        } else if (r7.A08()) {
            return new C71763dT(A0F(), null, this);
        } else {
            return null;
        }
    }

    public final CancellationException A0K(String str, Throwable th) {
        CancellationException cancellationException;
        if ((th instanceof CancellationException) && (cancellationException = (CancellationException) th) != null) {
            return cancellationException;
        }
        if (str == null) {
            str = A0F();
        }
        return new C71763dT(str, th, this);
    }

    public final AbstractC02790Dz A0L() {
        return (AbstractC02790Dz) this._parentHandle;
    }

    public final C114295Ky A0M(AnonymousClass5WP r3) {
        C114295Ky r0;
        if ((r3 instanceof C114295Ky) && (r0 = (C114295Ky) r3) != null) {
            return r0;
        }
        AnonymousClass5L9 ADu = r3.ADu();
        if (ADu != null) {
            return A04(ADu);
        }
        return null;
    }

    public final AbstractC114145Kj A0N(AnonymousClass1J7 r2, boolean z) {
        AbstractC114145Kj r0;
        if (z) {
            if (!(r2 instanceof AbstractC114305Kz) || (r0 = (AbstractC114145Kj) r2) == null) {
                r0 = new C114285Kx(r2);
            }
        } else if (!(r2 instanceof AbstractC114145Kj) || (r0 = (AbstractC114145Kj) r2) == null) {
            r0 = new AnonymousClass5L0(r2);
        }
        r0.A0B(this);
        return r0;
    }

    public final AnonymousClass5L9 A0O(AnonymousClass5WP r3) {
        AnonymousClass5L9 ADu = r3.ADu();
        if (ADu != null) {
            return ADu;
        }
        if (r3 instanceof AnonymousClass5F1) {
            return new AnonymousClass5L9();
        }
        if (r3 instanceof AbstractC114145Kj) {
            A0Z((AbstractC114145Kj) r3);
            return null;
        }
        throw new IllegalStateException(C16700pc.A08("State should have list: ", r3));
    }

    public final void A0P(Object obj) {
        Object A0C;
        do {
            A0C = A0C(A09(), obj);
            if (A0C == C93134Zf.A01) {
                StringBuilder sb = new StringBuilder("Job ");
                sb.append(this);
                sb.append(" is already complete or completing, but is being completed with ");
                sb.append(obj);
                throw new IllegalStateException(sb.toString(), A02(obj));
            }
        } while (A0C == C93134Zf.A02);
    }

    public final void A0Q(Object obj, C114295Ky r3, AnonymousClass5F3 r4) {
        C114295Ky A04 = A04(r3);
        if (A04 == null || !A0f(obj, A04, r4)) {
            A0E(obj, r4);
        }
    }

    public final void A0R(Object obj, AnonymousClass5WP r5) {
        C94894ci r4;
        AbstractC02790Dz A0L = A0L();
        if (A0L != null) {
            A0L.dispose();
            A0W(AnonymousClass5F0.A00);
        }
        Throwable th = null;
        if ((obj instanceof C94894ci) && (r4 = (C94894ci) obj) != null) {
            th = r4.A00;
        }
        if (r5 instanceof AbstractC114145Kj) {
            try {
                ((AbstractC114145Kj) r5).A0A(th);
            } catch (Throwable th2) {
                StringBuilder sb = new StringBuilder("Exception in completion handler ");
                sb.append(r5);
                sb.append(" for ");
                sb.append(this);
                A0T(new C113295Gy(sb.toString(), th2));
            }
        } else {
            AnonymousClass5L9 ADu = r5.ADu();
            if (ADu != null) {
                A0V(th, ADu);
            }
        }
    }

    public void A0S(Throwable th) {
        A0e(th);
    }

    public void A0T(Throwable th) {
        throw th;
    }

    public final void A0U(Throwable th, AnonymousClass5L9 r7) {
        C113295Gy r1 = null;
        for (C94524by r4 = (C94524by) r7.A01(); !C16700pc.A0O(r4, r7); r4 = r4.A03()) {
            if (r4 instanceof AbstractC114305Kz) {
                AnonymousClass5LF r3 = (AnonymousClass5LF) r4;
                try {
                    r3.A0A(th);
                } catch (Throwable th2) {
                    if (r1 == null) {
                        StringBuilder sb = new StringBuilder("Exception in completion handler ");
                        sb.append(r3);
                        sb.append(" for ");
                        sb.append(this);
                        r1 = new C113295Gy(sb.toString(), th2);
                    } else {
                        C88164En.A00(r1, th2);
                    }
                }
            }
        }
        if (r1 != null) {
            A0T(r1);
        }
        A0k(th);
    }

    public final void A0V(Throwable th, AnonymousClass5L9 r7) {
        C113295Gy r1 = null;
        for (C94524by r4 = (C94524by) r7.A01(); !C16700pc.A0O(r4, r7); r4 = r4.A03()) {
            if (r4 instanceof AbstractC114145Kj) {
                AnonymousClass5LF r3 = (AnonymousClass5LF) r4;
                try {
                    r3.A0A(th);
                } catch (Throwable th2) {
                    if (r1 == null) {
                        StringBuilder sb = new StringBuilder("Exception in completion handler ");
                        sb.append(r3);
                        sb.append(" for ");
                        sb.append(this);
                        r1 = new C113295Gy(sb.toString(), th2);
                    } else {
                        C88164En.A00(r1, th2);
                    }
                }
            }
        }
        if (r1 != null) {
            A0T(r1);
        }
    }

    public final void A0W(AbstractC02790Dz r1) {
        this._parentHandle = r1;
    }

    public final void A0X(AnonymousClass5F1 r3) {
        AnonymousClass5L9 r1 = new AnonymousClass5L9();
        AnonymousClass5WP r12 = r1;
        if (!r3.AJD()) {
            r12 = new AnonymousClass5F2(r1);
        }
        AnonymousClass0KN.A00(this, r3, r12, A00);
    }

    public final void A0Y(AbstractC02760Dw r3) {
        if (r3 != null) {
            r3.Ae7();
            AbstractC02790Dz A6E = r3.A6E(this);
            A0W(A6E);
            if (A0d()) {
                A6E.dispose();
            } else {
                return;
            }
        }
        A0W(AnonymousClass5F0.A00);
    }

    public final void A0Z(AbstractC114145Kj r3) {
        r3.A07(new AnonymousClass5L9());
        AnonymousClass0KN.A00(this, r3, r3.A03(), A00);
    }

    public final void A0a(AbstractC114145Kj r4) {
        Object A09;
        do {
            A09 = A09();
            if (A09 instanceof AbstractC114145Kj) {
                if (A09 == r4) {
                } else {
                    return;
                }
            } else if ((A09 instanceof AnonymousClass5WP) && ((AnonymousClass5WP) A09).ADu() != null) {
                r4.A06();
                return;
            } else {
                return;
            }
        } while (!AnonymousClass0KN.A00(this, A09, C93134Zf.A00, A00));
    }

    public final boolean A0d() {
        return !(A09() instanceof AnonymousClass5WP);
    }

    public final boolean A0e(Object obj) {
        Object obj2 = C93134Zf.A01;
        if (!A0c() || (obj2 = A0A(obj)) != C93134Zf.A03) {
            if (obj2 == obj2) {
                obj2 = A0B(obj);
            }
            if (obj2 != obj2 && obj2 != C93134Zf.A03 && obj2 == C93134Zf.A05) {
                return false;
            }
        }
        return true;
    }

    public final boolean A0f(Object obj, C114295Ky r6, AnonymousClass5F3 r7) {
        do {
            if (A05(new AnonymousClass5L1(obj, r6, r7, this), r6.A00, 1, false) != AnonymousClass5F0.A00) {
                return true;
            }
            r6 = A04(r6);
        } while (r6 != null);
        return false;
    }

    public final boolean A0g(Object obj, AnonymousClass5WP r6) {
        if (!AnonymousClass0KN.A00(this, r6, C93134Zf.A00(obj), A00)) {
            return false;
        }
        A0R(obj, r6);
        return true;
    }

    public final boolean A0h(Object obj, AbstractC114145Kj r5, AnonymousClass5L9 r6) {
        int A002;
        AnonymousClass5LB r2 = new AnonymousClass5LB(obj, this, r5);
        do {
            A002 = r6.A04().A00(r2, r5, r6);
            if (A002 == 1) {
                return true;
            }
        } while (A002 != 2);
        return false;
    }

    public boolean A0i(Throwable th) {
        if ((th instanceof CancellationException) || (A0e(th) && A0b())) {
            return true;
        }
        return false;
    }

    public final boolean A0k(Throwable th) {
        boolean z = th instanceof CancellationException;
        AbstractC02790Dz A0L = A0L();
        if (A0L == null || A0L == AnonymousClass5F0.A00) {
            return z;
        }
        if (A0L.A79(th) || z) {
            return true;
        }
        return false;
    }

    public final boolean A0l(Throwable th, AnonymousClass5WP r7) {
        AnonymousClass5L9 A0O = A0O(r7);
        if (A0O == null || !AnonymousClass0KN.A00(this, r7, new AnonymousClass5F3(th, A0O), A00)) {
            return false;
        }
        A0U(th, A0O);
        return true;
    }

    @Override // X.AbstractC02760Dw
    public final AbstractC02790Dz A6E(AbstractC02780Dy r4) {
        return (AbstractC02790Dz) A05(new C114295Ky(r4), this, 2, true);
    }

    @Override // X.AbstractC02760Dw
    public void A74(CancellationException cancellationException) {
        if (cancellationException == null) {
            cancellationException = new C71763dT(A0F(), null, this);
        }
        A0S(cancellationException);
    }

    @Override // X.AbstractC02760Dw
    public final CancellationException ABF() {
        Object A09 = A09();
        if (A09 instanceof AnonymousClass5F3) {
            Throwable A02 = ((AnonymousClass5F3) A09).A02();
            if (A02 != null) {
                return A0K(C16700pc.A08(getClass().getSimpleName(), " is cancelling"), A02);
            }
            throw new IllegalStateException(C16700pc.A08("Job is still new or active: ", this));
        } else if (A09 instanceof AnonymousClass5WP) {
            throw new IllegalStateException(C16700pc.A08("Job is still new or active: ", this));
        } else if (A09 instanceof C94894ci) {
            return A0K(null, ((C94894ci) A09).A00);
        } else {
            return new C71763dT(C16700pc.A08(getClass().getSimpleName(), " has completed normally"), null, this);
        }
    }

    @Override // X.AbstractC02770Dx
    public CancellationException ABM() {
        CancellationException cancellationException;
        Object A09 = A09();
        Throwable th = null;
        if (A09 instanceof AnonymousClass5F3) {
            th = ((AnonymousClass5F3) A09).A02();
        } else if (A09 instanceof C94894ci) {
            th = ((C94894ci) A09).A00;
        } else if (A09 instanceof AnonymousClass5WP) {
            throw new IllegalStateException(C16700pc.A08("Cannot be cancelling child in this state: ", A09));
        }
        return (!(th instanceof CancellationException) || (cancellationException = (CancellationException) th) == null) ? new C71763dT(C16700pc.A08("Parent job is ", A00(A09)), th, this) : cancellationException;
    }

    /* JADX WARNING: Removed duplicated region for block: B:59:0x001d A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0004 A[SYNTHETIC] */
    @Override // X.AbstractC02760Dw
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.AnonymousClass5VG AJB(X.AnonymousClass1J7 r8, boolean r9, boolean r10) {
        /*
            r7 = this;
            X.5Kj r6 = r7.A0N(r8, r9)
        L_0x0004:
            java.lang.Object r5 = r7.A09()
            boolean r0 = r5 instanceof X.AnonymousClass5F1
            if (r0 == 0) goto L_0x0022
            r1 = r5
            X.5F1 r1 = (X.AnonymousClass5F1) r1
            boolean r0 = r1.AJD()
            if (r0 == 0) goto L_0x001e
            java.util.concurrent.atomic.AtomicReferenceFieldUpdater r0 = X.C10710f4.A00
            boolean r0 = X.AnonymousClass0KN.A00(r7, r5, r6, r0)
        L_0x001b:
            if (r0 == 0) goto L_0x0004
            return r6
        L_0x001e:
            r7.A0X(r1)
            goto L_0x0004
        L_0x0022:
            boolean r0 = r5 instanceof X.AnonymousClass5WP
            r4 = 0
            if (r0 == 0) goto L_0x007a
            r0 = r5
            X.5WP r0 = (X.AnonymousClass5WP) r0
            X.5L9 r3 = r0.ADu()
            if (r3 != 0) goto L_0x0038
            if (r5 == 0) goto L_0x006f
            X.5Kj r5 = (X.AbstractC114145Kj) r5
            r7.A0Z(r5)
            goto L_0x0004
        L_0x0038:
            X.5F0 r2 = X.AnonymousClass5F0.A00
            if (r9 == 0) goto L_0x0062
            boolean r0 = r5 instanceof X.AnonymousClass5F3
            if (r0 == 0) goto L_0x0062
            monitor-enter(r5)
            r1 = r5
            X.5F3 r1 = (X.AnonymousClass5F3) r1     // Catch: all -> 0x0077
            java.lang.Throwable r4 = r1.A02()     // Catch: all -> 0x0077
            if (r4 == 0) goto L_0x0054
            boolean r0 = r8 instanceof X.C114295Ky     // Catch: all -> 0x0077
            if (r0 == 0) goto L_0x0061
            boolean r0 = r1.A09()     // Catch: all -> 0x0077
            if (r0 != 0) goto L_0x0061
        L_0x0054:
            boolean r0 = r7.A0h(r5, r6, r3)     // Catch: all -> 0x0077
            if (r0 != 0) goto L_0x005c
            monitor-exit(r5)
            goto L_0x0004
        L_0x005c:
            if (r4 != 0) goto L_0x0060
            monitor-exit(r5)
            return r6
        L_0x0060:
            r2 = r6
        L_0x0061:
            monitor-exit(r5)
        L_0x0062:
            if (r4 == 0) goto L_0x006a
            if (r10 == 0) goto L_0x0069
            r8.AJ4(r4)
        L_0x0069:
            return r2
        L_0x006a:
            boolean r0 = r7.A0h(r5, r6, r3)
            goto L_0x001b
        L_0x006f:
            java.lang.String r1 = "null cannot be cast to non-null type kotlinx.coroutines.JobNode"
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            r0.<init>(r1)
            throw r0
        L_0x0077:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        L_0x007a:
            if (r10 == 0) goto L_0x0089
            boolean r0 = r5 instanceof X.C94894ci
            if (r0 == 0) goto L_0x008c
            X.4ci r5 = (X.C94894ci) r5
        L_0x0082:
            if (r5 == 0) goto L_0x0086
            java.lang.Throwable r4 = r5.A00
        L_0x0086:
            r8.AJ4(r4)
        L_0x0089:
            X.5F0 r0 = X.AnonymousClass5F0.A00
            return r0
        L_0x008c:
            r5 = r4
            goto L_0x0082
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C10710f4.AJB(X.1J7, boolean, boolean):X.5VG");
    }

    @Override // X.AbstractC02760Dw
    public boolean AJD() {
        Object A09 = A09();
        return (A09 instanceof AnonymousClass5WP) && ((AnonymousClass5WP) A09).AJD();
    }

    @Override // X.AbstractC02780Dy
    public final void AYr(AbstractC02770Dx r1) {
        A0e(r1);
    }

    @Override // X.AbstractC02760Dw
    public final boolean Ae7() {
        int A08;
        do {
            A08 = A08(A09());
            if (A08 == 0) {
                return false;
            }
        } while (A08 != 1);
        return true;
    }

    @Override // X.AnonymousClass5X4
    public Object fold(Object obj, AnonymousClass5ZQ r3) {
        return C95104d9.A00(obj, this, r3);
    }

    @Override // X.AnonymousClass5ZU, X.AnonymousClass5X4
    public AnonymousClass5ZU get(AbstractC115495Rt r2) {
        return C95104d9.A01(this, r2);
    }

    @Override // X.AnonymousClass5ZU
    public final AbstractC115495Rt getKey() {
        return AbstractC02760Dw.A00;
    }

    @Override // X.AnonymousClass5X4
    public AnonymousClass5X4 minusKey(AbstractC115495Rt r2) {
        return C95104d9.A02(this, r2);
    }

    @Override // X.AnonymousClass5X4
    public AnonymousClass5X4 plus(AnonymousClass5X4 r2) {
        return C95104d9.A03(this, r2);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(A0H());
        sb.append('@');
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        return sb.toString();
    }
}
