package X;

import android.os.Bundle;
import java.util.concurrent.locks.Lock;

/* renamed from: X.4yh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C108214yh implements AbstractC14980mM, AbstractC15000mO {
    public AbstractC15020mQ A00;
    public final AnonymousClass1UE A01;
    public final boolean A02;

    public C108214yh(AnonymousClass1UE r1, boolean z) {
        this.A01 = r1;
        this.A02 = z;
    }

    @Override // X.AbstractC14990mN
    public final void onConnected(Bundle bundle) {
        C13020j0.A02(this.A00, "Callbacks must be attached to a ClientConnectionHelper instance before connecting the client.");
        this.A00.onConnected(bundle);
    }

    @Override // X.AbstractC15010mP
    public final void onConnectionFailed(C56492ky r5) {
        C13020j0.A02(this.A00, "Callbacks must be attached to a ClientConnectionHelper instance before connecting the client.");
        AbstractC15020mQ r0 = this.A00;
        AnonymousClass1UE r3 = this.A01;
        boolean z = this.A02;
        C108364yw r02 = (C108364yw) r0;
        Lock lock = r02.A0D;
        lock.lock();
        try {
            r02.A0E.Aga(r5, r3, z);
        } finally {
            lock.unlock();
        }
    }

    @Override // X.AbstractC14990mN
    public final void onConnectionSuspended(int i) {
        C13020j0.A02(this.A00, "Callbacks must be attached to a ClientConnectionHelper instance before connecting the client.");
        this.A00.onConnectionSuspended(i);
    }
}
