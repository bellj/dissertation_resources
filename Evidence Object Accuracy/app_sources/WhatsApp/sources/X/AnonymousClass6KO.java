package X;

import java.util.concurrent.Callable;

/* renamed from: X.6KO  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6KO implements Callable {
    public final /* synthetic */ AnonymousClass661 A00;
    public final /* synthetic */ C128385w1 A01;

    public AnonymousClass6KO(AnonymousClass661 r1, C128385w1 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        AnonymousClass661 r5 = this.A00;
        if (r5.isConnected()) {
            C129885yS r4 = r5.A0R;
            ((C119105ct) r4.A02.get(r4.A03.A02(r5.A00))).A04(this.A01);
            return r4.A02(r5.A00);
        }
        throw new C136076Kx("Cannot modify settings");
    }
}
