package X;

/* renamed from: X.21j  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C453821j {
    public final AbstractC15710nm A00;
    public final C15570nT A01;
    public final C20670w8 A02;
    public final C14650lo A03;
    public final C15550nR A04;
    public final C14830m7 A05;
    public final C16590pI A06;
    public final AnonymousClass018 A07;
    public final C17650rA A08;
    public final C15650ng A09;
    public final C16490p7 A0A;
    public final C21400xM A0B;
    public final AnonymousClass102 A0C;
    public final C14850m9 A0D;
    public final C17070qD A0E;
    public final C22940zt A0F;
    public final C20320vZ A0G;

    public C453821j(AbstractC15710nm r2, C15570nT r3, C20670w8 r4, C14650lo r5, C15550nR r6, C14830m7 r7, C16590pI r8, AnonymousClass018 r9, C17650rA r10, C15650ng r11, C16490p7 r12, C21400xM r13, AnonymousClass102 r14, C14850m9 r15, C17070qD r16, C22940zt r17, C20320vZ r18) {
        this.A05 = r7;
        this.A0D = r15;
        this.A00 = r2;
        this.A01 = r3;
        this.A06 = r8;
        this.A02 = r4;
        this.A04 = r6;
        this.A07 = r9;
        this.A0G = r18;
        this.A0E = r16;
        this.A09 = r11;
        this.A0F = r17;
        this.A0A = r12;
        this.A0B = r13;
        this.A03 = r5;
        this.A0C = r14;
        this.A08 = r10;
    }
}
