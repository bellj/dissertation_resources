package X;

import android.text.InputFilter;
import android.text.Spanned;

/* renamed from: X.4mE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C100634mE implements InputFilter {
    @Override // android.text.InputFilter
    public CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
        while (i < i2) {
            if (Character.isWhitespace(charSequence.charAt(i))) {
                return "";
            }
            i++;
        }
        return null;
    }
}
