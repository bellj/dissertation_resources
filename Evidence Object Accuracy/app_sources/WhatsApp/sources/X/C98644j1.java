package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4j1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98644j1 implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new C78493p0[i];
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        long j = 0;
        long j2 = 0;
        boolean z = false;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                z = C12960it.A1S(C95664e9.A02(parcel, readInt));
            } else if (c == 2) {
                j2 = C95664e9.A04(parcel, readInt);
            } else if (c != 3) {
                C95664e9.A0D(parcel, readInt);
            } else {
                j = C95664e9.A04(parcel, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78493p0(j, j2, z);
    }
}
