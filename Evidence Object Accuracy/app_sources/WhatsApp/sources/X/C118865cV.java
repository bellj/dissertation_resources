package X;

import android.text.TextUtils;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaTextView;

/* renamed from: X.5cV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118865cV extends AnonymousClass03U {
    public final View A00;
    public final WaTextView A01;
    public final WaTextView A02;
    public final WaTextView A03;
    public final WaTextView A04;
    public final WaTextView A05;
    public final WaTextView A06;
    public final WaTextView A07;
    public final WaTextView A08;
    public final WaTextView A09;

    public C118865cV(View view) {
        super(view);
        this.A05 = C12960it.A0N(view, R.id.subtotal_key);
        this.A06 = C12960it.A0N(view, R.id.subtotal_amount);
        this.A07 = C12960it.A0N(view, R.id.taxes_key);
        this.A08 = C12960it.A0N(view, R.id.taxes_amount);
        this.A01 = C12960it.A0N(view, R.id.discount_key);
        this.A02 = C12960it.A0N(view, R.id.discount_amount);
        this.A03 = C12960it.A0N(view, R.id.shipping_key);
        this.A04 = C12960it.A0N(view, R.id.shipping_amount);
        this.A09 = C12960it.A0N(view, R.id.total_charge_amount);
        this.A00 = AnonymousClass028.A0D(view, R.id.dashed_underline2);
    }

    public final void A08(int i) {
        this.A00.setVisibility(i);
        WaTextView waTextView = this.A05;
        waTextView.setVisibility(i);
        waTextView.setVisibility(i);
        this.A07.setVisibility(i);
        this.A08.setVisibility(i);
        this.A01.setVisibility(i);
        this.A02.setVisibility(i);
        this.A03.setVisibility(i);
        this.A04.setVisibility(i);
    }

    public final void A09(WaTextView waTextView, WaTextView waTextView2, AnonymousClass018 r7, String str, String str2, int i) {
        StringBuilder A0j;
        if (TextUtils.isEmpty(str2)) {
            waTextView.setVisibility(8);
            waTextView2.setVisibility(8);
            return;
        }
        String string = this.A0H.getContext().getString(i);
        if (!TextUtils.isEmpty(str)) {
            if (C28141Kv.A01(r7)) {
                A0j = C12960it.A0j(string);
                C117325Zm.A08(" (", str, ") ", A0j);
            } else {
                A0j = C12960it.A0j(" (");
                C117325Zm.A08(str, ") ", string, A0j);
            }
            string = A0j.toString();
        }
        waTextView.setText(string);
        waTextView.setVisibility(0);
        waTextView2.setText(str2);
        waTextView2.setVisibility(0);
        int i2 = 5;
        int i3 = 3;
        if (r7.A04().A06) {
            i3 = 5;
        }
        waTextView.setGravity(i3);
        if (r7.A04().A06) {
            i2 = 3;
        }
        waTextView2.setGravity(i2);
    }
}
