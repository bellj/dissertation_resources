package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4l2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C99894l2 implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new C30921Zi(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C30921Zi[i];
    }
}
