package X;

import java.io.Serializable;
import java.util.Comparator;

/* renamed from: X.5BQ  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5BQ implements Serializable {
    public static final long serialVersionUID = 0;
    public final Comparator comparator;
    public final Object[] elements;

    public AnonymousClass5BQ(Comparator comparator, Object[] objArr) {
        this.comparator = comparator;
        this.elements = objArr;
    }

    public Object readResolve() {
        C81083tR r1 = new C81083tR(this.comparator);
        r1.add(this.elements);
        return r1.build();
    }
}
