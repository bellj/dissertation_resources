package X;

import androidx.biometric.BiometricFragment;
import com.whatsapp.R;

/* renamed from: X.0Yf  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Yf implements AnonymousClass02B {
    public final /* synthetic */ BiometricFragment A00;

    public AnonymousClass0Yf(BiometricFragment biometricFragment) {
        this.A00 = biometricFragment;
    }

    @Override // X.AnonymousClass02B
    public /* bridge */ /* synthetic */ void ANq(Object obj) {
        if (((Boolean) obj).booleanValue()) {
            BiometricFragment biometricFragment = this.A00;
            if (biometricFragment.A1J()) {
                biometricFragment.A19();
            } else {
                AnonymousClass0EP r0 = biometricFragment.A01;
                CharSequence charSequence = r0.A0G;
                if (charSequence == null) {
                    C05000Nw r02 = r0.A06;
                    if (r02 != null) {
                        charSequence = r02.A01;
                        if (charSequence == null) {
                            charSequence = "";
                        }
                    } else {
                        charSequence = biometricFragment.A0I(R.string.default_error_msg);
                    }
                }
                biometricFragment.A1G(13, charSequence);
                biometricFragment.A18();
                biometricFragment.A1E(2);
            }
            biometricFragment.A01.A06(false);
        }
    }
}
