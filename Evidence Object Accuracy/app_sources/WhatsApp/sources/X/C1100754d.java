package X;

import com.whatsapp.voipcalling.VideoPort;

/* renamed from: X.54d  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1100754d implements AbstractC116665Wi {
    public final /* synthetic */ C37261lu A00;

    public C1100754d(C37261lu r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC116665Wi
    public void AWM(C64363Fg r2, VideoPort videoPort) {
        AbstractC116665Wi r0 = this.A00.A03;
        if (r0 != null) {
            r0.AWM(r2, videoPort);
        }
    }

    @Override // X.AbstractC116665Wi
    public void AWq(C64363Fg r2, VideoPort videoPort) {
        AbstractC116665Wi r0 = this.A00.A03;
        if (r0 != null) {
            r0.AWq(r2, videoPort);
        }
    }

    @Override // X.AbstractC116665Wi
    public void AYX(C64363Fg r2, VideoPort videoPort) {
        AbstractC116665Wi r0 = this.A00.A03;
        if (r0 != null) {
            r0.AYX(r2, videoPort);
        }
    }
}
