package X;

import android.content.ContentValues;
import android.database.Cursor;
import com.facebook.redex.RunnableBRunnable0Shape4S0200000_I0_4;
import com.whatsapp.jid.UserJid;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.16e  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C246116e {
    public final C18460sU A00;
    public final C16490p7 A01;
    public final C19770ue A02;
    public final Object A03 = new Object();
    public final Map A04;

    public C246116e(C18460sU r2, C16490p7 r3, C19770ue r4) {
        this.A00 = r2;
        this.A02 = r4;
        this.A01 = r3;
        this.A04 = Collections.synchronizedMap(new HashMap());
    }

    public final AnonymousClass1YF A00(Cursor cursor) {
        return new AnonymousClass1YF(cursor.getInt(cursor.getColumnIndexOrThrow("raw_id")), cursor.getLong(cursor.getColumnIndexOrThrow("timestamp")), cursor.getLong(cursor.getColumnIndexOrThrow("expected_timestamp")), cursor.getLong(cursor.getColumnIndexOrThrow("expected_ts_last_device_job_ts")), cursor.getLong(cursor.getColumnIndexOrThrow("expected_timestamp_update_ts")));
    }

    public void A01(AnonymousClass1YF r13, UserJid userJid) {
        long A01 = this.A00.A01(userJid);
        C16310on A02 = this.A01.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            ContentValues contentValues = new ContentValues();
            contentValues.put("raw_id", Integer.valueOf(r13.A00));
            contentValues.put("timestamp", Long.valueOf(r13.A04));
            contentValues.put("expected_timestamp", Long.valueOf(r13.A01));
            contentValues.put("expected_ts_last_device_job_ts", Long.valueOf(r13.A03));
            contentValues.put("expected_timestamp_update_ts", Long.valueOf(r13.A02));
            C16330op r7 = A02.A03;
            if (r7.A00("user_device_info", contentValues, "user_jid_row_id = ?", new String[]{String.valueOf(A01)}) != 1) {
                contentValues.put("user_jid_row_id", Long.valueOf(A01));
                r7.A03(contentValues, "user_device_info");
            }
            A00.A00();
            A02.A03(new RunnableBRunnable0Shape4S0200000_I0_4(this, 30, userJid));
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A02(UserJid userJid) {
        String[] strArr = {Long.toString(this.A00.A01(userJid))};
        C16310on A02 = this.A01.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            A02.A03.A01("user_device_info", "user_jid_row_id=?", strArr);
            A00.A00();
            A02.A03(new RunnableBRunnable0Shape4S0200000_I0_4(this, 31, userJid));
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
