package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.49T  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass49T extends Enum {
    public static final AnonymousClass49T A00 = new AnonymousClass49T("FAILED_ICON", "2", 1);
    public static final AnonymousClass49T A01 = new AnonymousClass49T("PENDING_ICON", "1", 0);
    public static final AnonymousClass49T A02 = new AnonymousClass49T("SUCCESS_ICON", "3", 2);
    public String iconType;

    public AnonymousClass49T(String str, String str2, int i) {
        this.iconType = str2;
    }
}
