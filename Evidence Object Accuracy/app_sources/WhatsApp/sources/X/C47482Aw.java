package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.SubtitleView;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import com.whatsapp.videoplayback.ExoPlaybackControlView;

/* renamed from: X.2Aw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C47482Aw extends FrameLayout implements AnonymousClass004 {
    public int A00;
    public C47492Ax A01;
    public ExoPlaybackControlView A02;
    public AnonymousClass3FK A03;
    public AnonymousClass5AW A04;
    public AnonymousClass2P7 A05;
    public String A06;
    public boolean A07;
    public boolean A08;
    public final View A09;
    public final View A0A;
    public final AspectRatioFrameLayout A0B;
    public final SubtitleView A0C;
    public final C67653Si A0D;
    public final boolean A0E;

    public C47482Aw(Context context, boolean z) {
        super(context);
        View r1;
        if (!this.A08) {
            this.A08 = true;
            generatedComponent();
        }
        this.A00 = -1;
        this.A07 = false;
        LayoutInflater.from(context).inflate(R.layout.wa_exoplayer_video_view, this);
        this.A0D = new C67653Si(this);
        AspectRatioFrameLayout aspectRatioFrameLayout = (AspectRatioFrameLayout) findViewById(R.id.video_frame);
        this.A0B = aspectRatioFrameLayout;
        this.A09 = findViewById(R.id.shutter);
        SubtitleView subtitleView = (SubtitleView) findViewById(R.id.subtitles);
        this.A0C = subtitleView;
        subtitleView.A00();
        subtitleView.A01();
        this.A0E = z;
        if (z) {
            r1 = new SurfaceView(context);
        } else {
            r1 = new C52392aj(context);
        }
        this.A0A = r1;
        r1.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        aspectRatioFrameLayout.addView(r1, 0);
        this.A04 = new AnonymousClass5AW(this);
    }

    @Override // android.view.View, android.view.ViewGroup
    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        ExoPlaybackControlView exoPlaybackControlView = this.A02;
        return exoPlaybackControlView != null ? exoPlaybackControlView.dispatchKeyEvent(keyEvent) : super.dispatchKeyEvent(keyEvent);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A05;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A05 = r0;
        }
        return r0.generatedComponent();
    }

    public Bitmap getCurrentFrame() {
        try {
            View view = this.A0A;
            int width = view.getWidth() / 4;
            int height = view.getHeight() / 4;
            if (!this.A0E) {
                return ((TextureView) view).getBitmap(width, height);
            }
            boolean isDrawingCacheEnabled = view.isDrawingCacheEnabled();
            if (!isDrawingCacheEnabled) {
                view.setDrawingCacheEnabled(true);
            }
            view.buildDrawingCache(true);
            Bitmap createScaledBitmap = Bitmap.createScaledBitmap(view.getDrawingCache(), width, height, true);
            if (!isDrawingCacheEnabled) {
                view.setDrawingCacheEnabled(false);
            }
            view.destroyDrawingCache();
            return createScaledBitmap;
        } catch (OutOfMemoryError e) {
            Log.e("ExoPlayerView/getCurrentFrame/", e);
            return null;
        }
    }

    @Override // android.view.View
    public boolean onTrackballEvent(MotionEvent motionEvent) {
        ExoPlaybackControlView exoPlaybackControlView = this.A02;
        if (exoPlaybackControlView == null) {
            return false;
        }
        exoPlaybackControlView.A01();
        return true;
    }

    public void setController(ExoPlaybackControlView exoPlaybackControlView) {
        this.A02 = exoPlaybackControlView;
        if (exoPlaybackControlView != null) {
            exoPlaybackControlView.A05 = this.A04;
            C47492Ax r0 = this.A01;
            if (r0 != null) {
                exoPlaybackControlView.setPlayer(r0);
            }
        }
    }

    public void setExoPlayerErrorActionsController(AnonymousClass3FK r1) {
        this.A03 = r1;
    }

    public void setLayoutResizingEnabled(boolean z) {
        AspectRatioFrameLayout aspectRatioFrameLayout = this.A0B;
        int i = 3;
        if (z) {
            i = 0;
        }
        aspectRatioFrameLayout.setResizeMode(i);
    }

    public void setPlayer(C47492Ax r7) {
        SurfaceTexture surfaceTexture;
        int width;
        int height;
        SurfaceHolder holder;
        C47492Ax r0 = this.A01;
        if (r0 != null) {
            C67653Si r1 = this.A0D;
            r0.A0Y.remove(r1);
            this.A01.A0Z.remove(r1);
            this.A01.AaK(r1);
            C47492Ax r2 = this.A01;
            r2.A03();
            r2.A02();
            r2.A07(null, false);
            r2.A05(0, 0);
        }
        this.A01 = r7;
        if (r7 != null) {
            boolean z = this.A0E;
            View view = this.A0A;
            if (z) {
                SurfaceView surfaceView = (SurfaceView) view;
                r7.A03();
                if (surfaceView == null) {
                    holder = null;
                } else {
                    holder = surfaceView.getHolder();
                }
                r7.A03();
                r7.A02();
                if (holder != null) {
                    r7.A09(null, 2, 8);
                }
                r7.A07 = holder;
                if (holder != null) {
                    holder.addCallback(r7.A0Q);
                    Surface surface = holder.getSurface();
                    if (surface != null && surface.isValid()) {
                        r7.A07(surface, false);
                        Rect surfaceFrame = holder.getSurfaceFrame();
                        width = surfaceFrame.width();
                        height = surfaceFrame.height();
                        r7.A05(width, height);
                    }
                }
                r7.A07(null, false);
                r7.A05(0, 0);
            } else {
                TextureView textureView = (TextureView) view;
                r7.A03();
                r7.A02();
                if (textureView != null) {
                    r7.A09(null, 2, 8);
                }
                r7.A08 = textureView;
                if (textureView != null) {
                    if (textureView.getSurfaceTextureListener() != null) {
                        android.util.Log.w("SimpleExoPlayer", "Replacing existing SurfaceTextureListener.");
                    }
                    textureView.setSurfaceTextureListener(r7.A0Q);
                    if (textureView.isAvailable() && (surfaceTexture = textureView.getSurfaceTexture()) != null) {
                        r7.A07(new Surface(surfaceTexture), true);
                        width = textureView.getWidth();
                        height = textureView.getHeight();
                        r7.A05(width, height);
                    }
                }
                r7.A07(null, true);
                r7.A05(0, 0);
            }
            C67653Si r12 = this.A0D;
            r7.A0Z.add(r12);
            r7.A5h(r12);
            r7.A0Y.add(r12);
            ExoPlaybackControlView exoPlaybackControlView = this.A02;
            if (exoPlaybackControlView != null) {
                exoPlaybackControlView.setPlayer(r7);
            }
        } else {
            this.A09.setVisibility(0);
        }
        this.A07 = false;
    }
}
