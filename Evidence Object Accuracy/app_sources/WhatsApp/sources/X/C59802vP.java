package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;

/* renamed from: X.2vP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C59802vP extends AbstractC37191le {
    public final View A00;
    public final TextEmojiLabel A01;

    public C59802vP(View view) {
        super(view);
        this.A00 = view;
        this.A01 = (TextEmojiLabel) C16700pc.A02(view, R.id.search_location_address);
    }
}
