package X;

import android.view.KeyEvent;
import android.widget.TextView;
import com.whatsapp.search.SearchViewModel;
import com.whatsapp.search.views.TokenizedSearchInput;

/* renamed from: X.4pU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C102654pU implements TextView.OnEditorActionListener {
    public final /* synthetic */ TokenizedSearchInput A00;

    public C102654pU(TokenizedSearchInput tokenizedSearchInput) {
        this.A00 = tokenizedSearchInput;
    }

    @Override // android.widget.TextView.OnEditorActionListener
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        SearchViewModel searchViewModel = this.A00.A0C;
        if (searchViewModel == null || i != 3) {
            return false;
        }
        searchViewModel.A0X(false);
        return true;
    }
}
