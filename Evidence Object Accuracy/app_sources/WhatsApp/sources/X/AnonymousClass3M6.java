package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.redex.IDxCreatorShape1S0000000_2_I1;
import java.util.ArrayList;

/* renamed from: X.3M6  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3M6 implements Parcelable {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape1S0000000_2_I1(57);
    public final C100514m2 A00;
    public final String A01;
    public final String A02;
    public final String A03;
    public final String A04;
    public final ArrayList A05;
    public final boolean A06;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass3M6(C100514m2 r1, String str, String str2, String str3, String str4, ArrayList arrayList, boolean z) {
        this.A02 = str;
        this.A03 = str2;
        this.A05 = arrayList;
        this.A06 = z;
        this.A01 = str3;
        this.A04 = str4;
        this.A00 = r1;
    }

    public AnonymousClass3M6(Parcel parcel) {
        this.A02 = parcel.readString();
        this.A03 = parcel.readString();
        this.A05 = parcel.createTypedArrayList(CREATOR);
        this.A06 = C12960it.A1S(parcel.readByte());
        this.A01 = parcel.readString();
        this.A04 = parcel.readString();
        this.A00 = (C100514m2) C12990iw.A0I(parcel, C100514m2.class);
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (obj instanceof AnonymousClass3M6) {
            AnonymousClass3M6 r8 = (AnonymousClass3M6) obj;
            if (r8.A02.equals(this.A02) && r8.A03.equals(this.A03) && r8.A06 == this.A06) {
                ArrayList arrayList = this.A05;
                ArrayList arrayList2 = r8.A05;
                if (arrayList == null) {
                    if (arrayList2 == null) {
                        return true;
                    }
                } else if (arrayList2 != null && arrayList.size() == arrayList2.size()) {
                    for (int i = 0; i < arrayList.size(); i++) {
                        if (arrayList.get(i).equals(arrayList2.get(i))) {
                        }
                    }
                    return true;
                }
            }
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        Object[] objArr = new Object[4];
        objArr[0] = this.A02;
        objArr[1] = this.A03;
        objArr[2] = Boolean.valueOf(this.A06);
        return C12980iv.A0B(this.A05, objArr, 3);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A02);
        parcel.writeString(this.A03);
        parcel.writeTypedList(this.A05);
        parcel.writeByte(this.A06 ? (byte) 1 : 0);
        parcel.writeString(this.A01);
        parcel.writeString(this.A04);
        parcel.writeParcelable(this.A00, i);
    }
}
