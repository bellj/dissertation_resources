package X;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

/* renamed from: X.5DH  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass5DH implements Iterator {
    public int entryIndex;
    public int expectedModCount;
    public final /* synthetic */ AbstractC80943tD this$0;
    public int toRemove = -1;

    public abstract Object result(int i);

    public AnonymousClass5DH(AbstractC80943tD r3) {
        this.this$0 = r3;
        C95614e4 r1 = r3.backingMap;
        this.entryIndex = r1.firstIndex();
        this.expectedModCount = r1.modCount;
    }

    private void checkForConcurrentModification() {
        if (this.this$0.backingMap.modCount != this.expectedModCount) {
            throw new ConcurrentModificationException();
        }
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        checkForConcurrentModification();
        return C12990iw.A1W(this.entryIndex);
    }

    @Override // java.util.Iterator
    public Object next() {
        if (hasNext()) {
            Object result = result(this.entryIndex);
            int i = this.entryIndex;
            this.toRemove = i;
            this.entryIndex = this.this$0.backingMap.nextIndex(i);
            return result;
        }
        throw new NoSuchElementException();
    }

    @Override // java.util.Iterator
    public void remove() {
        checkForConcurrentModification();
        C28251Mi.checkRemove(C12980iv.A1V(this.toRemove, -1));
        AbstractC80943tD r4 = this.this$0;
        r4.size -= (long) r4.backingMap.removeEntry(this.toRemove);
        C95614e4 r2 = this.this$0.backingMap;
        this.entryIndex = r2.nextIndexAfterRemove(this.entryIndex, this.toRemove);
        this.toRemove = -1;
        this.expectedModCount = r2.modCount;
    }
}
