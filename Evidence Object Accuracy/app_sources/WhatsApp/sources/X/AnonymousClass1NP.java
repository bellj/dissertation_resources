package X;

import java.util.Arrays;

/* renamed from: X.1NP  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1NP {
    public final int A00;
    public final byte[] A01;
    public final /* synthetic */ AnonymousClass14K A02;

    public AnonymousClass1NP(AnonymousClass14K r2, byte[] bArr) {
        this.A02 = r2;
        this.A01 = bArr;
        this.A00 = Arrays.hashCode(bArr);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof AnonymousClass1NP)) {
            return false;
        }
        return Arrays.equals(this.A01, ((AnonymousClass1NP) obj).A01);
    }

    public int hashCode() {
        return this.A00;
    }
}
