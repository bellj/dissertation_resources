package X;

import com.whatsapp.util.Log;

/* renamed from: X.0wI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20770wI {
    public final C14900mE A00;
    public final C14820m6 A01;
    public final C17220qS A02;
    public final C22280yp A03;

    public C20770wI(C14900mE r1, C14820m6 r2, C17220qS r3, C22280yp r4) {
        this.A00 = r1;
        this.A02 = r3;
        this.A03 = r4;
        this.A01 = r2;
    }

    public void A00(C37241lo r14) {
        C41731u0 r7 = new C41731u0(new C41721tz(this, r14), this.A02);
        Log.i("PrivacySettingsProtocolHelper/sendGetPrivacySettingsRequest");
        C17220qS r6 = r7.A01;
        String A01 = r6.A01();
        r6.A0D(r7, new AnonymousClass1V8(new AnonymousClass1V8("privacy", null), "iq", new AnonymousClass1W9[]{new AnonymousClass1W9(AnonymousClass1VY.A00, "to"), new AnonymousClass1W9("id", A01), new AnonymousClass1W9("xmlns", "privacy"), new AnonymousClass1W9("type", "get")}), A01, 70, 0);
    }

    public void A01(String str, String str2) {
        C41731u0 r10 = new C41731u0(new C41721tz(this, null), this.A02);
        Log.i("PrivacySettingsProtocolHelper/sendSetPrivacySettingsRequest");
        C17220qS r9 = r10.A01;
        String A01 = r9.A01();
        r9.A0D(r10, new AnonymousClass1V8(new AnonymousClass1V8("privacy", (AnonymousClass1W9[]) null, new AnonymousClass1V8[]{new AnonymousClass1V8("category", new AnonymousClass1W9[]{new AnonymousClass1W9("name", str), new AnonymousClass1W9("value", str2)})}), "iq", new AnonymousClass1W9[]{new AnonymousClass1W9(AnonymousClass1VY.A00, "to"), new AnonymousClass1W9("type", "set"), new AnonymousClass1W9("id", A01), new AnonymousClass1W9("xmlns", "privacy")}), A01, 69, 0);
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0068 A[ADDED_TO_REGION, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0089 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0073 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0010 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02(java.util.Map r14) {
        /*
        // Method dump skipped, instructions count: 339
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20770wI.A02(java.util.Map):void");
    }
}
