package X;

import java.util.ArrayList;

/* renamed from: X.1D1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1D1 implements AbstractC16990q5 {
    public final C18290sD A00;

    @Override // X.AbstractC16990q5
    public /* synthetic */ void AOo() {
    }

    public AnonymousClass1D1(C18290sD r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC16990q5
    public void AOp() {
        C18290sD r8 = this.A00;
        C15570nT r0 = r8.A01;
        r0.A08();
        if (r0.A00 != null) {
            AnonymousClass31U r7 = new AnonymousClass31U();
            for (AbstractC18500sY r2 : new ArrayList(r8.A0A.A00().A00.values())) {
                if (r2 instanceof C19300tt) {
                    r7.A0R = Integer.valueOf(r2.A04());
                } else if (r2 instanceof C19230tm) {
                    r7.A0B = Integer.valueOf(r2.A04());
                } else if (r2 instanceof C19310tu) {
                    r7.A04 = Integer.valueOf(r2.A04());
                } else if (r2 instanceof AnonymousClass0t2) {
                    r7.A03 = Integer.valueOf(r2.A04());
                } else if (r2 instanceof C18490sX) {
                    r7.A00 = Integer.valueOf(r2.A04());
                } else if (r2 instanceof C19070tW) {
                    r7.A0X = Integer.valueOf(r2.A04());
                } else if (r2 instanceof C19210tk) {
                    r7.A0W = Integer.valueOf(r2.A04());
                } else if (r2 instanceof C19060tV) {
                    r7.A0V = Integer.valueOf(r2.A04());
                } else if (r2 instanceof C19120tb) {
                    r7.A0U = Integer.valueOf(r2.A04());
                } else if (r2 instanceof C19200tj) {
                    r7.A0T = Integer.valueOf(r2.A04());
                } else if (r2 instanceof C19140td) {
                    r7.A0S = Integer.valueOf(r2.A04());
                } else if (r2 instanceof C19050tU) {
                    r7.A0Q = Integer.valueOf(r2.A04());
                } else if (r2 instanceof C19100tZ) {
                    r7.A0M = Integer.valueOf(r2.A04());
                } else if (r2 instanceof C19220tl) {
                    r7.A0L = Integer.valueOf(r2.A04());
                } else if (r2 instanceof C19170tg) {
                    r7.A0A = Integer.valueOf(r2.A04());
                } else if (r2 instanceof C18690sr) {
                    r7.A01 = Integer.valueOf(r2.A04());
                } else if (r2 instanceof C19280tr) {
                    r7.A0O = Integer.valueOf(r2.A04());
                } else if (r2 instanceof C19260tp) {
                    r7.A0D = Integer.valueOf(r2.A04());
                } else if (r2 instanceof C19270tq) {
                    r7.A0N = Integer.valueOf(r2.A04());
                } else if (r2 instanceof C19090tY) {
                    r7.A0K = Integer.valueOf(r2.A04());
                } else if (r2 instanceof C19110ta) {
                    r7.A0P = Integer.valueOf(r2.A04());
                } else if (r2 instanceof C19130tc) {
                    r7.A0H = Integer.valueOf(r2.A04());
                } else if (r2 instanceof C19180th) {
                    C19180th r22 = (C19180th) r2;
                    boolean z = r22 instanceof C19190ti;
                    Integer valueOf = Integer.valueOf(r22.A04());
                    if (!z) {
                        r7.A0I = valueOf;
                    } else {
                        r7.A0J = valueOf;
                    }
                } else if (r2 instanceof C19040tT) {
                    r7.A0G = Integer.valueOf(r2.A04());
                } else if (r2 instanceof C19160tf) {
                    r7.A0F = Integer.valueOf(r2.A04());
                } else if (r2 instanceof C19030tS) {
                    r7.A0E = Integer.valueOf(r2.A04());
                } else if (r2 instanceof C19000tP) {
                    r7.A0C = Integer.valueOf(r2.A04());
                } else if (r2 instanceof C19080tX) {
                    r7.A09 = Integer.valueOf(r2.A04());
                } else if (r2 instanceof C19020tR) {
                    r7.A08 = Integer.valueOf(r2.A04());
                } else if (r2 instanceof C19150te) {
                    r7.A07 = Integer.valueOf(r2.A04());
                } else if (r2 instanceof C18760sy) {
                    r7.A02 = Integer.valueOf(r2.A04());
                }
            }
            r7.A05 = 1;
            r7.A06 = 1;
            long j = -1;
            long j2 = r8.A04.A00.getLong("db_last_all_migrations_attempt_timestamp", -1);
            if (j2 >= 0) {
                j = r8.A03.A00() - j2;
            }
            r7.A0Y = Long.valueOf(j);
            r8.A0B.A05(r7);
        }
    }
}
