package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.38t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC628338t extends AbstractC16350or {
    public int A00;
    public C28601Of A01;
    public AnonymousClass1JO A02;
    public C15580nU A03;
    public UserJid A04;
    public final C20660w7 A05;

    public AbstractC628338t(C15580nU r1, UserJid userJid, C20660w7 r3) {
        this.A05 = r3;
        this.A03 = r1;
        this.A04 = userJid;
    }
}
