package X;

import android.os.Bundle;
import com.google.android.gms.auth.api.signin.internal.SignInHubActivity;
import java.util.Set;

/* renamed from: X.3Rn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C67443Rn implements AnonymousClass03M {
    public final /* synthetic */ SignInHubActivity A00;

    public /* synthetic */ C67443Rn(SignInHubActivity signInHubActivity) {
        this.A00 = signInHubActivity;
    }

    @Override // X.AnonymousClass03M
    public final void AS8(AnonymousClass0QL r1) {
    }

    @Override // X.AnonymousClass03M
    public final AnonymousClass0QL AOi(Bundle bundle, int i) {
        SignInHubActivity signInHubActivity = this.A00;
        Set set = AnonymousClass1U8.A00;
        synchronized (set) {
        }
        return new C54022fy(signInHubActivity, set);
    }

    @Override // X.AnonymousClass03M
    public final /* bridge */ /* synthetic */ void AS2(AnonymousClass0QL r4, Object obj) {
        SignInHubActivity signInHubActivity = this.A00;
        signInHubActivity.setResult(signInHubActivity.A00, signInHubActivity.A01);
        signInHubActivity.finish();
    }
}
