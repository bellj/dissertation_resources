package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.3Dk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63913Dk {
    public final /* synthetic */ AnonymousClass4RO A00;
    public final /* synthetic */ AnonymousClass19T A01;

    public C63913Dk(AnonymousClass4RO r1, AnonymousClass19T r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public void A00(int i) {
        AnonymousClass4RO r2 = this.A00;
        AnonymousClass19T r6 = r2.A00;
        r6.A00 = false;
        if (i == 404) {
            r6.A0C.A0F(r2.A01, false);
        }
        C19850um r4 = r6.A0C;
        UserJid userJid = r2.A01;
        r4.A0A(new C44651zK(new C44711zQ(null, false), C12960it.A0l()), userJid, true);
        r6.A03.A0A(new C84583zY(userJid));
    }

    public void A01(C44651zK r7, AnonymousClass4Tm r8) {
        boolean z;
        AnonymousClass4RO r3 = this.A00;
        AnonymousClass19T r2 = r3.A00;
        r2.A00 = false;
        String str = r8.A06;
        if (str == null) {
            z = false;
        } else if (str.equals(r3.A02)) {
            z = true;
        } else {
            return;
        }
        C19850um r0 = r2.A0C;
        UserJid userJid = r3.A01;
        r0.A0A(r7, userJid, z);
        boolean z2 = r3.A03;
        if (!z2 || !r7.A01.isEmpty()) {
            r2.A03.A0A(new C84593zZ(new C90854Pm(r7.A01, z2, false), userJid));
        } else {
            r2.A03.A0A(new C84583zY(userJid));
        }
    }
}
