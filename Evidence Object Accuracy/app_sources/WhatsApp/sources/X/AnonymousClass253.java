package X;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.253  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass253 {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public final InputStream A09;
    public final byte[] A0A;

    public AnonymousClass253(InputStream inputStream) {
        this.A03 = Integer.MAX_VALUE;
        this.A06 = 100;
        this.A07 = 67108864;
        this.A0A = new byte[4096];
        this.A00 = 0;
        this.A08 = 0;
        this.A09 = inputStream;
    }

    public AnonymousClass253(byte[] bArr, int i, int i2) {
        this.A03 = Integer.MAX_VALUE;
        this.A06 = 100;
        this.A07 = 67108864;
        this.A0A = bArr;
        this.A01 = i2 + i;
        this.A00 = i;
        this.A08 = -i;
        this.A09 = null;
    }

    public int A00() {
        int i = this.A03;
        if (i == Integer.MAX_VALUE) {
            return -1;
        }
        return i - (this.A08 + this.A00);
    }

    public int A01() {
        int i = this.A00;
        if (this.A01 - i < 4) {
            A0E(4);
            i = this.A00;
        }
        byte[] bArr = this.A0A;
        this.A00 = i + 4;
        return ((bArr[i + 3] & 255) << 24) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x003a, code lost:
        if (r4[r2] < 0) goto L_0x003c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A02() {
        /*
            r5 = this;
            int r0 = r5.A00
            int r1 = r5.A01
            if (r1 == r0) goto L_0x003c
            byte[] r4 = r5.A0A
            int r2 = r0 + 1
            byte r3 = r4[r0]
            if (r3 < 0) goto L_0x005d
            r5.A00 = r2
            return r3
        L_0x0011:
            int r2 = r1 + 1
            byte r1 = r4[r1]
            int r0 = r1 << 28
            r3 = r3 ^ r0
            r0 = 266354560(0xfe03f80, float:2.2112565E-29)
            r3 = r3 ^ r0
            if (r1 >= 0) goto L_0x004d
            int r1 = r2 + 1
            byte r0 = r4[r2]
            if (r0 >= 0) goto L_0x006d
            int r2 = r1 + 1
            byte r0 = r4[r1]
            if (r0 >= 0) goto L_0x004d
            int r1 = r2 + 1
            byte r0 = r4[r2]
            if (r0 >= 0) goto L_0x006d
            int r2 = r1 + 1
            byte r0 = r4[r1]
            if (r0 >= 0) goto L_0x004d
            int r1 = r2 + 1
            byte r0 = r4[r2]
            if (r0 >= 0) goto L_0x006d
        L_0x003c:
            long r1 = r5.A07()
            int r0 = (int) r1
            return r0
        L_0x0042:
            int r2 = r1 + 1
            byte r0 = r4[r1]
            int r0 = r0 << 14
            r3 = r3 ^ r0
            if (r3 < 0) goto L_0x004f
            r3 = r3 ^ 16256(0x3f80, float:2.278E-41)
        L_0x004d:
            r1 = r2
            goto L_0x006d
        L_0x004f:
            int r1 = r2 + 1
            byte r0 = r4[r2]
            int r0 = r0 << 21
            r3 = r3 ^ r0
            if (r3 >= 0) goto L_0x0011
            r0 = -2080896(0xffffffffffe03f80, float:NaN)
            r3 = r3 ^ r0
            goto L_0x006d
        L_0x005d:
            int r1 = r1 - r2
            r0 = 9
            if (r1 < r0) goto L_0x003c
            int r1 = r2 + 1
            byte r0 = r4[r2]
            int r0 = r0 << 7
            r3 = r3 ^ r0
            if (r3 >= 0) goto L_0x0042
            r3 = r3 ^ -128(0xffffffffffffff80, float:NaN)
        L_0x006d:
            r5.A00 = r1
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass253.A02():int");
    }

    public int A03() {
        if (this.A00 != this.A01 || A0G(1)) {
            int A02 = A02();
            this.A04 = A02;
            if ((A02 >>> 3) != 0) {
                return A02;
            }
            throw new C28971Pt("Protocol message contained an invalid tag (zero).");
        }
        this.A04 = 0;
        return 0;
    }

    public int A04(int i) {
        if (i >= 0) {
            int i2 = i + this.A08 + this.A00;
            int i3 = this.A03;
            if (i2 <= i3) {
                this.A03 = i2;
                A0B();
                return i3;
            }
            throw new C28971Pt("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either that the input has been truncated or that an embedded message misreported its own length.");
        }
        throw new C28971Pt("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
    }

    public long A05() {
        int i = this.A00;
        if (this.A01 - i < 8) {
            A0E(8);
            i = this.A00;
        }
        byte[] bArr = this.A0A;
        this.A00 = i + 8;
        return ((((long) bArr[i + 7]) & 255) << 56) | (((long) bArr[i]) & 255) | ((((long) bArr[i + 1]) & 255) << 8) | ((((long) bArr[i + 2]) & 255) << 16) | ((((long) bArr[i + 3]) & 255) << 24) | ((((long) bArr[i + 4]) & 255) << 32) | ((((long) bArr[i + 5]) & 255) << 40) | ((((long) bArr[i + 6]) & 255) << 48);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002d, code lost:
        if (((long) r6[r5]) < 0) goto L_0x002f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long A06() {
        /*
        // Method dump skipped, instructions count: 181
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass253.A06():long");
    }

    public long A07() {
        long j = 0;
        int i = 0;
        do {
            if (this.A00 == this.A01) {
                A0E(1);
            }
            byte[] bArr = this.A0A;
            int i2 = this.A00;
            this.A00 = i2 + 1;
            byte b = bArr[i2];
            j |= ((long) (b & Byte.MAX_VALUE)) << i;
            if ((b & 128) == 0) {
                return j;
            }
            i += 7;
        } while (i < 64);
        throw new C28971Pt("CodedInputStream encountered a malformed varint.");
    }

    public AbstractC27881Jp A08() {
        int A02 = A02();
        int i = this.A01;
        int i2 = this.A00;
        if (A02 <= i - i2 && A02 > 0) {
            AbstractC27881Jp A01 = AbstractC27881Jp.A01(this.A0A, i2, A02);
            this.A00 += A02;
            return A01;
        } else if (A02 == 0) {
            return AbstractC27881Jp.A01;
        } else {
            return new C27861Jn(A0H(A02));
        }
    }

    public AnonymousClass1G1 A09(AnonymousClass254 r4, AnonymousClass255 r5) {
        int A02 = A02();
        int i = this.A05;
        if (i < this.A06) {
            int A04 = A04(A02);
            this.A05 = i + 1;
            AbstractC27091Fz A0B = AbstractC27091Fz.A0B(this, r4, r5.A00);
            A0C(0);
            this.A05--;
            this.A03 = A04;
            A0B();
            return A0B;
        }
        throw new C28971Pt("Protocol message had too many levels of nesting.  May be malicious.  Use CodedInputStream.setRecursionLimit() to increase the depth limit.");
    }

    public String A0A() {
        String str;
        int A02 = A02();
        int i = this.A01;
        int i2 = this.A00;
        if (A02 <= i - i2 && A02 > 0) {
            str = new String(this.A0A, i2, A02, C27851Jm.A03);
        } else if (A02 == 0) {
            return "";
        } else {
            if (A02 > i) {
                return new String(A0H(A02), C27851Jm.A03);
            }
            A0E(A02);
            str = new String(this.A0A, this.A00, A02, C27851Jm.A03);
        }
        this.A00 += A02;
        return str;
    }

    public final void A0B() {
        int i = this.A01 + this.A02;
        this.A01 = i;
        int i2 = this.A08 + i;
        int i3 = this.A03;
        if (i2 > i3) {
            int i4 = i2 - i3;
            this.A02 = i4;
            this.A01 = i - i4;
            return;
        }
        this.A02 = 0;
    }

    public void A0C(int i) {
        if (this.A04 != i) {
            throw new C28971Pt("Protocol message end-group tag did not match expected tag.");
        }
    }

    public void A0D(int i) {
        int i2 = this.A01;
        int i3 = this.A00;
        int i4 = i2 - i3;
        if (i <= i4) {
            if (i >= 0) {
                this.A00 = i3 + i;
                return;
            }
        } else if (i >= 0) {
            int i5 = this.A08;
            int i6 = i5 + i3 + i;
            int i7 = this.A03;
            if (i6 <= i7) {
                this.A00 = i2;
                while (true) {
                    A0E(1);
                    int i8 = i - i4;
                    int i9 = this.A01;
                    if (i8 > i9) {
                        i4 += i9;
                        this.A00 = i9;
                    } else {
                        this.A00 = i8;
                        return;
                    }
                }
            } else {
                A0D((i7 - i5) - i3);
                throw new C28971Pt("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either that the input has been truncated or that an embedded message misreported its own length.");
            }
        }
        throw new C28971Pt("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
    }

    public final void A0E(int i) {
        if (!A0G(i)) {
            throw new C28971Pt("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either that the input has been truncated or that an embedded message misreported its own length.");
        }
    }

    public boolean A0F() {
        return A06() != 0;
    }

    public final boolean A0G(int i) {
        InputStream inputStream;
        int i2 = this.A00;
        int i3 = i2 + i;
        int i4 = this.A01;
        if (i3 > i4) {
            if (this.A08 + i2 + i <= this.A03 && (inputStream = this.A09) != null) {
                if (i2 > 0) {
                    if (i4 > i2) {
                        byte[] bArr = this.A0A;
                        System.arraycopy(bArr, i2, bArr, 0, i4 - i2);
                    }
                    this.A08 += i2;
                    i4 = this.A01 - i2;
                    this.A01 = i4;
                    this.A00 = 0;
                }
                byte[] bArr2 = this.A0A;
                int length = bArr2.length;
                int read = inputStream.read(bArr2, i4, length - i4);
                if (read == 0 || read < -1 || read > length) {
                    StringBuilder sb = new StringBuilder("InputStream#read(byte[]) returned invalid result: ");
                    sb.append(read);
                    sb.append("\nThe InputStream implementation is buggy.");
                    throw new IllegalStateException(sb.toString());
                } else if (read > 0) {
                    this.A01 += read;
                    if ((this.A08 + i) - this.A07 <= 0) {
                        A0B();
                        if (this.A01 >= i) {
                            return true;
                        }
                        return A0G(i);
                    }
                    throw new C28971Pt("Protocol message was too large.  May be malicious.  Use CodedInputStream.setSizeLimit() to increase the size limit.");
                }
            }
            return false;
        }
        StringBuilder sb2 = new StringBuilder("refillBuffer() called when ");
        sb2.append(i);
        sb2.append(" bytes were already available in buffer");
        throw new IllegalStateException(sb2.toString());
    }

    public final byte[] A0H(int i) {
        if (i <= 0) {
            throw new C28971Pt("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
        }
        int i2 = this.A08;
        int i3 = this.A00;
        int i4 = i2 + i3 + i;
        if (i4 <= this.A07) {
            int i5 = this.A03;
            if (i4 <= i5) {
                InputStream inputStream = this.A09;
                if (inputStream != null) {
                    int i6 = this.A01;
                    int i7 = i6 - i3;
                    this.A08 = i2 + i6;
                    this.A00 = 0;
                    this.A01 = 0;
                    int i8 = i - i7;
                    if (i8 < 4096 || i8 <= inputStream.available()) {
                        byte[] bArr = new byte[i];
                        System.arraycopy(this.A0A, i3, bArr, 0, i7);
                        while (i7 < i) {
                            int read = inputStream.read(bArr, i7, i - i7);
                            if (read != -1) {
                                this.A08 += read;
                                i7 += read;
                            } else {
                                throw new C28971Pt("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either that the input has been truncated or that an embedded message misreported its own length.");
                            }
                        }
                        return bArr;
                    }
                    ArrayList arrayList = new ArrayList();
                    while (i8 > 0) {
                        int min = Math.min(i8, 4096);
                        byte[] bArr2 = new byte[min];
                        int i9 = 0;
                        while (i9 < min) {
                            int read2 = inputStream.read(bArr2, i9, min - i9);
                            if (read2 != -1) {
                                this.A08 += read2;
                                i9 += read2;
                            } else {
                                throw new C28971Pt("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either that the input has been truncated or that an embedded message misreported its own length.");
                            }
                        }
                        i8 -= min;
                        arrayList.add(bArr2);
                    }
                    byte[] bArr3 = new byte[i];
                    System.arraycopy(this.A0A, i3, bArr3, 0, i7);
                    Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        byte[] bArr4 = (byte[]) it.next();
                        int length = bArr4.length;
                        System.arraycopy(bArr4, 0, bArr3, i7, length);
                        i7 += length;
                    }
                    return bArr3;
                }
                throw new C28971Pt("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either that the input has been truncated or that an embedded message misreported its own length.");
            }
            A0D((i5 - i2) - i3);
            throw new C28971Pt("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either that the input has been truncated or that an embedded message misreported its own length.");
        }
        throw new C28971Pt("Protocol message was too large.  May be malicious.  Use CodedInputStream.setSizeLimit() to increase the size limit.");
    }
}
