package X;

import android.util.Pair;
import com.facebook.redex.RunnableBRunnable0Shape0S0200000_I0;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executor;

/* renamed from: X.0lj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14620lj implements AbstractC14590lg {
    public Object A00;
    public final Object A01 = new Object();
    public final List A02 = new ArrayList();

    public Object A00() {
        Object obj;
        synchronized (this.A01) {
            obj = this.A00;
        }
        return obj;
    }

    public void A01() {
        synchronized (this.A01) {
            this.A02.clear();
        }
    }

    public void A02(AbstractC14590lg r4) {
        synchronized (this.A01) {
            Iterator it = this.A02.iterator();
            while (it.hasNext()) {
                if (((AbstractC14590lg) ((Pair) it.next()).first).equals(r4)) {
                    it.remove();
                }
            }
        }
    }

    public void A03(AbstractC14590lg r5, Executor executor) {
        Object obj;
        synchronized (this.A01) {
            obj = this.A00;
            this.A02.add(Pair.create(r5, executor));
        }
        if (obj == null) {
            return;
        }
        if (executor == null) {
            r5.accept(obj);
        } else {
            executor.execute(new RunnableBRunnable0Shape0S0200000_I0(r5, 36, obj));
        }
    }

    public void A04(Object obj) {
        ArrayList arrayList;
        synchronized (this.A01) {
            this.A00 = obj;
            arrayList = new ArrayList(this.A02);
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            Pair pair = (Pair) it.next();
            AbstractC14590lg r3 = (AbstractC14590lg) pair.first;
            Executor executor = (Executor) pair.second;
            if (executor == null) {
                r3.accept(obj);
            } else {
                executor.execute(new RunnableBRunnable0Shape0S0200000_I0(r3, 36, obj));
            }
        }
    }

    @Override // X.AbstractC14590lg
    public void accept(Object obj) {
        A04(obj);
    }
}
