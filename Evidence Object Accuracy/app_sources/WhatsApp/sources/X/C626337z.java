package X;

import android.os.SystemClock;
import com.whatsapp.status.StatusPrivacyActivity;
import java.lang.ref.WeakReference;

/* renamed from: X.37z  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C626337z extends AbstractC16350or {
    public final long A00 = SystemClock.elapsedRealtime();
    public final C14900mE A01;
    public final C20670w8 A02;
    public final C18470sV A03;
    public final AnonymousClass1BD A04;
    public final WeakReference A05;

    public C626337z(C14900mE r3, C20670w8 r4, C18470sV r5, StatusPrivacyActivity statusPrivacyActivity, AnonymousClass1BD r7) {
        this.A05 = C12970iu.A10(statusPrivacyActivity);
        this.A01 = r3;
        this.A03 = r5;
        this.A02 = r4;
        this.A04 = r7;
    }
}
