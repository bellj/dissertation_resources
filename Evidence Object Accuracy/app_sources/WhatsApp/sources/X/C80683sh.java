package X;

import android.view.View;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.whatsapp.usernotice.UserNoticeBottomSheetDialogFragment;

/* renamed from: X.3sh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C80683sh extends AnonymousClass2UH {
    public final /* synthetic */ BottomSheetBehavior A00;
    public final /* synthetic */ UserNoticeBottomSheetDialogFragment A01;

    @Override // X.AnonymousClass2UH
    public void A00(View view, float f) {
    }

    public C80683sh(BottomSheetBehavior bottomSheetBehavior, UserNoticeBottomSheetDialogFragment userNoticeBottomSheetDialogFragment) {
        this.A01 = userNoticeBottomSheetDialogFragment;
        this.A00 = bottomSheetBehavior;
    }

    @Override // X.AnonymousClass2UH
    public void A01(View view, int i) {
        if (i != 3) {
            this.A00.A0M(3);
        }
    }
}
