package X;

import android.util.SparseIntArray;

/* renamed from: X.4aR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C93604aR {
    public final int A00;
    public final int A01;
    public final int A02;
    public final SparseIntArray A03;

    public C93604aR(SparseIntArray sparseIntArray, int i, int i2, int i3) {
        AnonymousClass0RA.A01(C12990iw.A1X(i2, i));
        this.A02 = i;
        this.A01 = i2;
        this.A03 = sparseIntArray;
        this.A00 = i3;
    }
}
