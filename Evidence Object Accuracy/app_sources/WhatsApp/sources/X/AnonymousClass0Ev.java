package X;

import android.animation.Animator;
import android.view.View;
import java.util.List;

/* renamed from: X.0Ev  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Ev extends C06590Ug {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AnonymousClass0F6 A01;
    public final /* synthetic */ AnonymousClass03U A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass0Ev(AnonymousClass0F6 r9, AnonymousClass03U r10, AnonymousClass03U r11, float f, float f2, float f3, float f4, int i, int i2) {
        super(r10, f, f2, f3, f4, i);
        this.A01 = r9;
        this.A00 = i2;
        this.A02 = r11;
    }

    @Override // X.C06590Ug, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        View view;
        super.onAnimationEnd(animator);
        if (!this.A05) {
            int i = this.A00;
            AnonymousClass0F6 r3 = this.A01;
            if (i <= 0) {
                AnonymousClass03U r1 = this.A02;
                AbstractC12380hp r0 = AnonymousClass0Z1.A00;
                view = r1.A0H;
                r0.A7F(view);
            } else {
                List list = r3.A0S;
                view = this.A02.A0H;
                list.add(view);
                this.A04 = true;
                r3.A0M.post(new RunnableC09590dC(this, r3));
            }
            if (r3.A0G == view) {
                r3.A08(view);
            }
        }
    }
}
