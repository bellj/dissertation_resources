package X;

import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.0zG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22550zG {
    public AnonymousClass1VC A00;
    public final C14330lG A01;
    public final C18790t3 A02;
    public final C14820m6 A03;
    public final C18810t5 A04;
    public final AbstractC17860rW A05;
    public final C18800t4 A06;
    public final AtomicBoolean A07 = new AtomicBoolean(false);

    public C22550zG(C14330lG r3, C18790t3 r4, C14820m6 r5, C18810t5 r6, AbstractC17860rW r7, C18800t4 r8) {
        this.A01 = r3;
        this.A02 = r4;
        this.A06 = r8;
        this.A04 = r6;
        this.A03 = r5;
        this.A05 = r7;
    }

    /* JADX WARNING: Removed duplicated region for block: B:51:0x014c  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x015a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.Set A00() {
        /*
        // Method dump skipped, instructions count: 368
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22550zG.A00():java.util.Set");
    }
}
