package X;

import com.whatsapp.jid.UserJid;
import java.util.Iterator;

/* renamed from: X.3Uz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68333Uz implements AbstractC116475Vp {
    public final /* synthetic */ AnonymousClass19T A00;
    public final /* synthetic */ UserJid A01;
    public final /* synthetic */ String A02;
    public final /* synthetic */ boolean A03;

    public C68333Uz(AnonymousClass19T r1, UserJid userJid, String str, boolean z) {
        this.A00 = r1;
        this.A03 = z;
        this.A02 = str;
        this.A01 = userJid;
    }

    @Override // X.AbstractC116475Vp
    public void AQN(AnonymousClass28H r6, int i) {
        AnonymousClass19T r4 = this.A00;
        r4.A01 = false;
        boolean z = this.A03;
        if (z) {
            r4.A04.A0A(Boolean.FALSE);
        }
        if (i == 406 || i == 404) {
            r4.A0C.A0D(this.A01);
        }
        C25821Ay r0 = r4.A0B;
        UserJid userJid = this.A01;
        Iterator A00 = AbstractC16230of.A00(r0);
        while (A00.hasNext()) {
            ((AbstractC13960kc) A00.next()).AQH(userJid, i);
        }
        if (z) {
            r4.A02.A0A(new C84603za(userJid, "catalog_products_all_items_collection_id", i));
        }
    }

    @Override // X.AbstractC116475Vp
    public void AQO(C44721zR r10, AnonymousClass28H r11) {
        AnonymousClass19T r8 = this.A00;
        r8.A01 = false;
        boolean z = this.A03;
        if (z) {
            r8.A04.A0A(Boolean.FALSE);
        }
        String str = r11.A06;
        if (str == null || str.equals(this.A02)) {
            boolean z2 = true;
            boolean A1W = C12960it.A1W(str);
            C19850um r0 = r8.A0C;
            UserJid userJid = this.A01;
            r0.A0B(r10, userJid, A1W);
            C25821Ay r02 = r8.A0B;
            String str2 = this.A02;
            boolean A1X = C12980iv.A1X(str2);
            Iterator A00 = AbstractC16230of.A00(r02);
            while (A00.hasNext()) {
                ((AbstractC13960kc) A00.next()).AQI(userJid, false, A1X);
            }
            if (z) {
                AnonymousClass016 r2 = r8.A02;
                if (str2 != null) {
                    z2 = false;
                }
                r2.A0A(new C84613zb(userJid, "catalog_products_all_items_collection_id", false, z2));
            }
        }
    }
}
