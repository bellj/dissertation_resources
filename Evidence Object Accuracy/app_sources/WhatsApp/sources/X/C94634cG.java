package X;

/* renamed from: X.4cG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C94634cG {
    public static final AnonymousClass4XW A00;

    static {
        AnonymousClass4XW r0;
        if (!C95634e6.A06 || !C95634e6.A05 || AnonymousClass4ZV.A00()) {
            r0 = new C80503sM();
        } else {
            r0 = new C80513sN();
        }
        A00 = r0;
    }

    public static int A00(CharSequence charSequence) {
        int length = charSequence.length();
        int i = 0;
        int i2 = 0;
        while (i2 < length && charSequence.charAt(i2) < 128) {
            i2++;
        }
        int i3 = length;
        while (true) {
            if (i2 >= length) {
                break;
            }
            char charAt = charSequence.charAt(i2);
            if (charAt < 2048) {
                i3 += (127 - charAt) >>> 31;
                i2++;
            } else {
                int length2 = charSequence.length();
                while (i2 < length2) {
                    char charAt2 = charSequence.charAt(i2);
                    if (charAt2 < 2048) {
                        i += (127 - charAt2) >>> 31;
                    } else {
                        i += 2;
                        if (55296 <= charAt2 && charAt2 <= 57343) {
                            if (Character.codePointAt(charSequence, i2) >= 65536) {
                                i2++;
                            } else {
                                throw new AnonymousClass4CN(i2, length2);
                            }
                        }
                    }
                    i2++;
                }
                i3 += i;
            }
        }
        if (i3 >= length) {
            return i3;
        }
        throw C12970iu.A0f(C72453ed.A0n(i3));
    }
}
