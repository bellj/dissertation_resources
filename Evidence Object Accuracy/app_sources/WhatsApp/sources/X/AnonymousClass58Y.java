package X;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

/* renamed from: X.58Y  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass58Y implements AnonymousClass5W9 {
    @Override // X.AnonymousClass5W9
    public AnonymousClass5UW A8V(JSONObject jSONObject) {
        C16700pc.A0E(jSONObject, 0);
        JSONArray jSONArray = jSONObject.getJSONArray("or");
        int length = jSONArray.length();
        AnonymousClass5UW[] r4 = new AnonymousClass5UW[length];
        boolean z = false;
        for (int i = 0; i < length; i++) {
            JSONObject jSONObject2 = jSONArray.getJSONObject(i);
            C16700pc.A0B(jSONObject2);
            r4[i] = AnonymousClass4ET.A00(jSONObject2);
        }
        ArrayList A0l = C12960it.A0l();
        int i2 = 0;
        while (i2 < length) {
            AnonymousClass5UW r0 = r4[i2];
            i2++;
            if (r0 != null) {
                A0l.add(r0);
            }
        }
        if (A0l.size() > 1) {
            z = true;
        }
        AnonymousClass009.A0A("expected 2 or more rules in input", z);
        return new AnonymousClass58G(A0l);
    }

    @Override // X.AnonymousClass5W9
    public String ADP() {
        return "or";
    }
}
