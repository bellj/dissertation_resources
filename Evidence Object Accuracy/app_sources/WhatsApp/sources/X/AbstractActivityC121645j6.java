package X;

import com.whatsapp.payments.phoenix.flowconfigurationservice.npci.IndiaUpiFcsPinHandlerActivity;

/* renamed from: X.5j6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC121645j6 extends AbstractActivityC121545iU {
    public boolean A00 = false;

    public AbstractActivityC121645j6() {
        C117295Zj.A0p(this, 4);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            IndiaUpiFcsPinHandlerActivity indiaUpiFcsPinHandlerActivity = (IndiaUpiFcsPinHandlerActivity) this;
            AnonymousClass2FL r3 = (AnonymousClass2FL) ((AnonymousClass2FJ) generatedComponent());
            AnonymousClass01J A1M = ActivityC13830kP.A1M(r3, indiaUpiFcsPinHandlerActivity);
            ActivityC13810kN.A10(A1M, indiaUpiFcsPinHandlerActivity);
            AbstractActivityC119235dO.A1S(r3, A1M, indiaUpiFcsPinHandlerActivity, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(r3, A1M, indiaUpiFcsPinHandlerActivity, ActivityC13790kL.A0Y(A1M, indiaUpiFcsPinHandlerActivity)), indiaUpiFcsPinHandlerActivity));
            AbstractActivityC119235dO.A1Y(A1M, indiaUpiFcsPinHandlerActivity);
            AbstractActivityC119235dO.A1Z(A1M, indiaUpiFcsPinHandlerActivity);
            indiaUpiFcsPinHandlerActivity.A05 = C18000rk.A00(A1M.A7n);
            indiaUpiFcsPinHandlerActivity.A04 = (C19750uc) A1M.A7o.get();
            indiaUpiFcsPinHandlerActivity.A00 = (C49032Iw) r3.A0f.get();
        }
    }
}
