package X;

/* renamed from: X.3su  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C80753su extends AbstractC80773sw {
    public final char A00;

    public C80753su(char c) {
        this.A00 = c;
    }

    public String toString() {
        char c = this.A00;
        char[] cArr = {'\\', 'u', 0, 0, 0, 0};
        for (int i = 0; i < 4; i++) {
            cArr[5 - i] = "0123456789ABCDEF".charAt(c & 15);
            c = (char) (c >> 4);
        }
        String copyValueOf = String.copyValueOf(cArr);
        StringBuilder A0t = C12980iv.A0t(C12970iu.A07(copyValueOf) + 18);
        A0t.append("CharMatcher.is('");
        A0t.append(copyValueOf);
        return C12960it.A0d("')", A0t);
    }
}
