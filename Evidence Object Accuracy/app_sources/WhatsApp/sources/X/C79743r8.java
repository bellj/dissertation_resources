package X;

import android.os.IBinder;

/* renamed from: X.3r8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79743r8 extends C98334iW implements AnonymousClass5YE {
    public C79743r8(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.location.ILocationListener");
    }
}
