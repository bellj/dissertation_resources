package X;

import android.view.Choreographer;

/* renamed from: X.4SM  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4SM {
    public long A00;
    public C94254bV A01;
    public boolean A02;
    public final Choreographer.FrameCallback A03 = new Choreographer$FrameCallbackC100724mN(this);
    public final Choreographer A04;

    public AnonymousClass4SM(Choreographer choreographer) {
        this.A04 = choreographer;
    }
}
