package X;

import android.os.IBinder;
import android.os.IInterface;

/* renamed from: X.3qj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79523qj extends C98394ic implements IInterface {
    public C79523qj(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.dynamite.IDynamiteLoader");
    }
}
