package X;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.google.android.gms.common.api.Status;

/* renamed from: X.3fw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractBinderC73263fw extends Binder implements IInterface {
    @Override // android.os.IInterface
    public final IBinder asBinder() {
        return this;
    }

    public AbstractBinderC73263fw(String str) {
        attachInterface(this, str);
    }

    @Override // android.os.Binder
    public final boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        if (i <= 16777215) {
            C72463ee.A0Q(this, parcel);
        } else if (super.onTransact(i, parcel, parcel2, i2)) {
            return true;
        }
        if (this instanceof BinderC79033pw) {
            BinderC79033pw r3 = (BinderC79033pw) this;
            if (i != 1) {
                return false;
            }
            AnonymousClass4DO.A00((Status) C12970iu.A0F(parcel, Status.CREATOR), r3.A00, Boolean.valueOf(C12960it.A1S(parcel.readInt())));
            return true;
        } else if (!(this instanceof BinderC79023pv)) {
            BinderC79013pu r32 = (BinderC79013pu) this;
            if (i != 1) {
                return false;
            }
            AnonymousClass4DO.A00((Status) C12970iu.A0F(parcel, Status.CREATOR), r32.A00, Integer.valueOf(parcel.readInt()));
            return true;
        } else {
            BinderC79023pv r33 = (BinderC79023pv) this;
            if (i != 1) {
                return false;
            }
            AnonymousClass4DO.A00((Status) C12970iu.A0F(parcel, Status.CREATOR), r33.A00, parcel.createByteArray());
            return true;
        }
    }
}
