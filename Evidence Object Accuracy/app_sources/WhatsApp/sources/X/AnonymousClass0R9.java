package X;

/* renamed from: X.0R9  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0R9 {
    public static C05200Oq A00(C06860Vj r4) {
        C05200Oq r3 = new C05200Oq();
        r3.A06 = r4.A03;
        float f = r4.A02;
        if (f != Float.MIN_VALUE) {
            r3.A01 = f;
        }
        float f2 = r4.A00;
        if (f2 != Float.MIN_VALUE) {
            r3.A00 = f2;
        }
        return r3;
    }

    public static C05200Oq A01(AnonymousClass03T r1, float f) {
        C05200Oq r0 = new C05200Oq();
        r0.A06 = r1;
        r0.A01 = f;
        return r0;
    }
}
