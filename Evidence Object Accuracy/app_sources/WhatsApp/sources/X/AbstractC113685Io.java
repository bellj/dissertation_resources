package X;

/* renamed from: X.5Io  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC113685Io extends AbstractC112665Eg {
    public transient AnonymousClass5WO A00;
    public final AnonymousClass5X4 _context;

    public AbstractC113685Io(AnonymousClass5WO r1, AnonymousClass5X4 r2) {
        super(r1);
        this._context = r2;
    }

    @Override // X.AnonymousClass5WO
    public AnonymousClass5X4 ABi() {
        AnonymousClass5X4 ABi;
        if (!(this instanceof C113675In)) {
            AnonymousClass5X4 r0 = this._context;
            C16700pc.A0C(r0);
            return r0;
        }
        AnonymousClass5WO r02 = ((C113675In) this).completion;
        if (r02 == null || (ABi = r02.ABi()) == null) {
            return C112775Er.A00;
        }
        return ABi;
    }
}
