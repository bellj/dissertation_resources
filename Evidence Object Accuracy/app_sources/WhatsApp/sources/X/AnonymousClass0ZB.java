package X;

import android.view.View;
import android.view.ViewGroup;

/* renamed from: X.0ZB  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0ZB implements AbstractC12650iG {
    public final /* synthetic */ AnonymousClass02H A00;

    public AnonymousClass0ZB(AnonymousClass02H r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC12650iG
    public View ABJ(int i) {
        return this.A00.A0D(i);
    }

    @Override // X.AbstractC12650iG
    public int ABL(View view) {
        return view.getBottom() + ((AnonymousClass0B6) view.getLayoutParams()).A03.bottom + ((ViewGroup.MarginLayoutParams) view.getLayoutParams()).bottomMargin;
    }

    @Override // X.AbstractC12650iG
    public int ABN(View view) {
        return (view.getTop() - ((AnonymousClass0B6) view.getLayoutParams()).A03.top) - ((ViewGroup.MarginLayoutParams) view.getLayoutParams()).topMargin;
    }

    @Override // X.AbstractC12650iG
    public int AEw() {
        AnonymousClass02H r0 = this.A00;
        return r0.A00 - r0.A08();
    }

    @Override // X.AbstractC12650iG
    public int AEy() {
        return this.A00.A0B();
    }
}
