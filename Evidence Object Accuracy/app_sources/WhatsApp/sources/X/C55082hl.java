package X;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.stickers.StickerView;

/* renamed from: X.2hl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C55082hl extends AnonymousClass03U {
    public View.OnLongClickListener A00;
    public AnonymousClass1KS A01;
    public boolean A02;
    public boolean A03;
    public final View.OnLongClickListener A04 = new View$OnLongClickListenerC101144n3(this);
    public final AnonymousClass1AB A05;
    public final AbstractC116245Ur A06;
    public final StickerView A07;
    public final Integer A08;

    public C55082hl(LayoutInflater layoutInflater, ViewGroup viewGroup, AnonymousClass1AB r5, AbstractC116245Ur r6, Integer num) {
        super(C12960it.A0F(layoutInflater, viewGroup, R.layout.sticker_picker_item));
        this.A05 = r5;
        this.A06 = r6;
        this.A08 = num;
        StickerView stickerView = (StickerView) this.A0H.findViewById(R.id.sticker_view);
        this.A07 = stickerView;
        stickerView.A03 = true;
    }
}
