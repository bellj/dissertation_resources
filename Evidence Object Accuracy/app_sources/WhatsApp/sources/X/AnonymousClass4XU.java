package X;

/* renamed from: X.4XU  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4XU {
    public byte A00;
    public long A01;
    public long A02;
    public long A03;
    public long A04;
    public long A05;
    public long A06;
    public long A07;
    public long A08;
    public Long A09;
    public Long A0A;
    public Long A0B;
    public Long A0C;
    public final int A0D = 3;

    public static long A00(long j, int i) {
        if (i == 0) {
            return 0;
        }
        return (((j - 1) / 1024) + 1) * 1024;
    }

    public C856643r A01() {
        Long l;
        Long l2;
        Long l3;
        Long l4;
        C856643r r5 = new C856643r();
        r5.A01 = Integer.valueOf(this.A0D);
        r5.A00 = Integer.valueOf(C33761f2.A00(this.A00, 0, false));
        Long l5 = this.A0C;
        if (l5 == null || (l4 = this.A0B) == null) {
            l = null;
        } else {
            l = Long.valueOf(l4.longValue() - l5.longValue());
        }
        r5.A0B = l;
        Long l6 = this.A0A;
        if (l6 == null || (l3 = this.A09) == null) {
            l2 = null;
        } else {
            l2 = Long.valueOf(l3.longValue() - l6.longValue());
        }
        r5.A02 = l2;
        r5.A09 = Long.valueOf(this.A07);
        r5.A0A = Long.valueOf(this.A08);
        r5.A06 = Long.valueOf(this.A04);
        long j = this.A01;
        r5.A03 = Long.valueOf(A00(j, (j > 0 ? 1 : (j == 0 ? 0 : -1))));
        r5.A07 = Long.valueOf(this.A05);
        long j2 = this.A02;
        r5.A04 = Long.valueOf(A00(j2, (j2 > 0 ? 1 : (j2 == 0 ? 0 : -1))));
        r5.A08 = Long.valueOf(this.A06);
        long j3 = this.A03;
        r5.A05 = Long.valueOf(A00(j3, (j3 > 0 ? 1 : (j3 == 0 ? 0 : -1))));
        return r5;
    }

    public String toString() {
        return A01().toString();
    }
}
