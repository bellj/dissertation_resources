package X;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

/* renamed from: X.29o  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass29o extends DialogFragment {
    public Dialog A00;
    public Dialog A01;
    public DialogInterface.OnCancelListener A02;

    @Override // android.app.DialogFragment, android.content.DialogInterface.OnCancelListener
    public void onCancel(DialogInterface dialogInterface) {
        DialogInterface.OnCancelListener onCancelListener = this.A02;
        if (onCancelListener != null) {
            onCancelListener.onCancel(dialogInterface);
        }
    }

    @Override // android.app.DialogFragment
    public Dialog onCreateDialog(Bundle bundle) {
        Dialog dialog = this.A00;
        if (dialog != null) {
            return dialog;
        }
        setShowsDialog(false);
        Dialog dialog2 = this.A01;
        if (dialog2 != null) {
            return dialog2;
        }
        Activity activity = getActivity();
        C13020j0.A01(activity);
        AlertDialog create = new AlertDialog.Builder(activity).create();
        this.A01 = create;
        return create;
    }
}
