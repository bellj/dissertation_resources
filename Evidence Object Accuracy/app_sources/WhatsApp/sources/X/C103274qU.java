package X;

import android.content.Context;
import com.whatsapp.profile.ProfilePhotoReminder;

/* renamed from: X.4qU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103274qU implements AbstractC009204q {
    public final /* synthetic */ ProfilePhotoReminder A00;

    public C103274qU(ProfilePhotoReminder profilePhotoReminder) {
        this.A00 = profilePhotoReminder;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
