package X;

import android.content.SharedPreferences;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.16M  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass16M {
    public static final Map A02 = new HashMap();
    public final SharedPreferences A00;
    public final Object A01 = new Object();

    public AnonymousClass16M(C16630pM r2) {
        this.A00 = r2.A02(AnonymousClass01V.A07);
    }
}
