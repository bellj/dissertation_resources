package X;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/* renamed from: X.1BQ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1BQ extends AnonymousClass015 {
    public AnonymousClass1KY A00;
    public String A01 = "";
    public boolean A02;
    public final AnonymousClass016 A03 = new AnonymousClass016();
    public final AnonymousClass1A4 A04;

    public AnonymousClass1BQ(AnonymousClass1A4 r2) {
        this.A04 = r2;
    }

    public void A04() {
        AnonymousClass016 r1 = this.A03;
        if (r1.A01() != null && !((List) r1.A01()).isEmpty()) {
            A05(this.A01, true);
        }
    }

    public final void A05(String str, boolean z) {
        this.A02 = z;
        if (str.length() <= 20) {
            String replaceAll = str.replaceAll("[\\p{Punct}¥%，。？；：（）【】「」《》—¡!«»¿?{}]", "");
            if (replaceAll.equals(this.A01) && !z) {
                return;
            }
            if (!replaceAll.isEmpty()) {
                this.A01 = replaceAll;
                AnonymousClass1A4 r2 = this.A04;
                AnonymousClass009.A01();
                AnonymousClass4VM r4 = new AnonymousClass4VM();
                if (r2.A01.A02) {
                    C624337f r0 = r2.A00;
                    if (r0 != null) {
                        r0.A03(true);
                        r2.A00 = null;
                    }
                    C624337f r3 = new C624337f(new AnonymousClass5UH() { // from class: X.576
                        @Override // X.AnonymousClass5UH
                        public final void AVL(Collection collection) {
                            AnonymousClass4VM r1 = AnonymousClass4VM.this;
                            AnonymousClass009.A01();
                            if (collection != null) {
                                r1.A01.addAll(collection);
                            }
                            AnonymousClass1KY r02 = r1.A00;
                            if (r02 != null) {
                                r02.AVQ(r1);
                            }
                        }
                    }, r2, r2.A02);
                    r2.A00 = r3;
                    r2.A03.Aaz(r3, replaceAll);
                }
                AnonymousClass1KY r02 = this.A00;
                if (r02 == null) {
                    r02 = new AnonymousClass1KY() { // from class: X.3XS
                        @Override // X.AnonymousClass1KY
                        public final void AVQ(AnonymousClass4VM r6) {
                            AnonymousClass1BQ r42 = AnonymousClass1BQ.this;
                            List list = r6.A01;
                            ArrayList A0y = C12980iv.A0y(list);
                            for (int i = 0; i < list.size(); i++) {
                                A0y.add(list.get(i));
                            }
                            AnonymousClass016 r1 = r42.A03;
                            if (!((r1.A01() == null || C12980iv.A0z(r1).isEmpty()) && A0y.isEmpty())) {
                                r1.A0B(A0y);
                            }
                        }
                    };
                    this.A00 = r02;
                }
                r4.A00(r02);
                return;
            }
        }
        this.A03.A0B(new ArrayList(0));
        this.A01 = "";
        AnonymousClass1A4 r22 = this.A04;
        C624337f r1 = r22.A00;
        if (r1 != null) {
            r1.A03(true);
            r22.A00 = null;
        }
    }
}
