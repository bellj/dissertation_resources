package X;

import android.app.ProgressDialog;
import android.content.Context;

/* renamed from: X.3fK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ProgressDialogC72883fK extends ProgressDialog {
    public CharSequence A00;

    public ProgressDialogC72883fK(Context context) {
        super(context);
    }

    @Override // android.app.ProgressDialog, android.app.AlertDialog
    public void setMessage(CharSequence charSequence) {
        super.setMessage(charSequence);
        this.A00 = charSequence;
    }
}
