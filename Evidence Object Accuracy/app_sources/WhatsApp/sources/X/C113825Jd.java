package X;

import com.whatsapp.catalogsearch.view.viewmodel.CatalogSearchViewModel;

/* renamed from: X.5Jd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C113825Jd extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ CatalogSearchViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C113825Jd(CatalogSearchViewModel catalogSearchViewModel) {
        super(0);
        this.this$0 = catalogSearchViewModel;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        return AnonymousClass0R2.A01((AnonymousClass017) this.this$0.A06.getValue());
    }
}
