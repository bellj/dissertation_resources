package X;

/* renamed from: X.4BV  reason: invalid class name */
/* loaded from: classes3.dex */
public enum AnonymousClass4BV {
    A03(0),
    A01(1),
    A02(2),
    A04(3);
    
    public final int value;

    AnonymousClass4BV(int i) {
        this.value = i;
    }

    public static AnonymousClass4BV A00(int i) {
        if (i == 0) {
            return A03;
        }
        if (i == 1) {
            return A01;
        }
        if (i == 2) {
            return A02;
        }
        if (i != 3) {
            return null;
        }
        return A04;
    }
}
