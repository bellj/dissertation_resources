package X;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.graphics.Color;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import org.chromium.net.UrlRequest;

/* renamed from: X.0br  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08790br implements AbstractC17450qp {
    public static final long A01 = TimeUnit.SECONDS.toMillis(1);
    public final AbstractC17450qp A00;

    public C08790br(AbstractC17450qp r1) {
        this.A00 = r1;
    }

    public static void A00(AnimatorSet animatorSet, List list) {
        int size = list.size();
        long j = 0;
        for (int i = 0; i < size; i++) {
            Animator animator = (Animator) list.get(i);
            j = Math.max(j, animator.getStartDelay() + animator.getDuration());
        }
        ValueAnimator ofInt = ValueAnimator.ofInt(0, 0);
        ofInt.setDuration(j);
        list.add(0, ofInt);
        animatorSet.playTogether(list);
    }

    /* renamed from: A01 */
    public Object A9j(C14230l4 r19, C14220l3 r20, C1093651k r21) {
        String str;
        float f;
        float f2;
        int i;
        String str2 = r21.A00;
        char c = 65535;
        switch (str2.hashCode()) {
            case -914746283:
                if (str2.equals("bk.action.animated.Stagger")) {
                    c = 0;
                    break;
                }
                break;
            case -573790654:
                if (str2.equals("bk.action.animated.easing.CreateCubicBezier")) {
                    c = 1;
                    break;
                }
                break;
            case -387628292:
                if (str2.equals("bk.action.animated.Loop")) {
                    c = 2;
                    break;
                }
                break;
            case -214348689:
                if (str2.equals("bk.action.animated.CreateColor")) {
                    c = 3;
                    break;
                }
                break;
            case 177085473:
                if (str2.equals("bk.action.animated.CancelWithToken")) {
                    c = 4;
                    break;
                }
                break;
            case 445536294:
                if (str2.equals("bk.action.animated.GetCurrentDimensionValue")) {
                    c = 5;
                    break;
                }
                break;
            case 511230409:
                if (str2.equals("bk.action.animated.GetCurrentColorValue")) {
                    c = 6;
                    break;
                }
                break;
            case 748692594:
                if (str2.equals("bk.action.animated.CreateDimension")) {
                    c = 7;
                    break;
                }
                break;
            case 875025162:
                if (str2.equals("bk.action.animated.Start")) {
                    c = '\b';
                    break;
                }
                break;
            case 880735442:
                if (str2.equals("bk.action.animated.Cancel")) {
                    c = '\t';
                    break;
                }
                break;
            case 896165716:
                if (str2.equals("bk.action.animated.Create")) {
                    c = '\n';
                    break;
                }
                break;
            case 1208990953:
                if (str2.equals("bk.action.animated.StartWithToken")) {
                    c = 11;
                    break;
                }
                break;
            case 1572781663:
                if (str2.equals("bk.action.animated.Parallel")) {
                    c = '\f';
                    break;
                }
                break;
            case 1750927385:
                if (str2.equals("bk.action.animated.Sequence")) {
                    c = '\r';
                    break;
                }
                break;
            case 2100418198:
                if (str2.equals("bk.action.animated.GetCurrentValue")) {
                    c = 14;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                float floatValue = ((Number) r20.A01(0)).floatValue();
                List list = (List) r20.A00(1);
                long j = (long) (floatValue * ((float) A01));
                for (int i2 = 0; i2 < list.size(); i2++) {
                    ((Animator) list.get(i2)).setStartDelay(((long) i2) * j);
                }
                AnimatorSet animatorSet = new AnimatorSet();
                A00(animatorSet, list);
                return animatorSet;
            case 1:
                return AnonymousClass0L1.A00(((Number) r20.A01(0)).floatValue(), ((Number) r20.A01(1)).floatValue(), ((Number) r20.A01(2)).floatValue(), ((Number) r20.A01(3)).floatValue());
            case 2:
                return new AnonymousClass092(((Number) r20.A01(0)).intValue(), (Animator) r20.A00(1));
            case 3:
                C1093751l A00 = C94724cR.A00(r20.A01(0));
                AbstractC14200l1 A012 = C94724cR.A01(r20.A00(1));
                int parseColor = Color.parseColor((String) r20.A00(2));
                int parseColor2 = Color.parseColor((String) r20.A00(3));
                float floatValue2 = ((Number) r20.A01(4)).floatValue();
                C14260l7 A02 = C67973Tp.A02(r19, r20, 6);
                ValueAnimator ofInt = ValueAnimator.ofInt(parseColor, parseColor2);
                ofInt.setEvaluator(new ArgbEvaluator());
                ofInt.setDuration((long) (floatValue2 * ((float) A01)));
                ofInt.setInterpolator((TimeInterpolator) r20.A00(5));
                C14210l2 r0 = new C14210l2();
                r0.A05(ofInt, 0);
                ofInt.addUpdateListener(new AnonymousClass0Uw(this, r19, r0.A03(), A00));
                A02(ofInt, A02, r19, A012);
                return ofInt;
            case 4:
                C14260l7 r02 = r19.A00;
                AnonymousClass4D3.A00(r02);
                Animator A002 = AnonymousClass3JV.A00(r02, (String) r20.A00(0));
                if (A002 != null) {
                    A002.cancel();
                }
                return null;
            case 5:
                AnonymousClass09S r3 = (AnonymousClass09S) r20.A00(0);
                float floatValue3 = ((Number) r3.getAnimatedValue()).floatValue();
                Object[] objArr = new Object[1];
                if (r3.A00 == 0) {
                    objArr[0] = Float.valueOf(floatValue3);
                    str = "%.2f%%";
                } else {
                    objArr[0] = Integer.valueOf(Math.round(floatValue3));
                    str = "%dpx";
                }
                return String.format(str, objArr);
            case 6:
                return String.format("#%08x", Integer.valueOf(((Number) ((ValueAnimator) r20.A00(0)).getAnimatedValue()).intValue()));
            case 7:
                C1093751l A003 = C94724cR.A00(r20.A01(0));
                AbstractC14200l1 A013 = C94724cR.A01(r20.A00(1));
                String str3 = (String) r20.A00(2);
                String str4 = (String) r20.A00(3);
                float floatValue4 = ((Number) r20.A01(4)).floatValue();
                TimeInterpolator timeInterpolator = (TimeInterpolator) r20.A00(5);
                C14260l7 A022 = C67973Tp.A02(r19, r20, 6);
                try {
                    if (str3.endsWith("%")) {
                        f = AnonymousClass3JW.A00(str3);
                        f2 = AnonymousClass3JW.A00(str4);
                        i = 0;
                    } else {
                        f = AnonymousClass3JW.A01(str3);
                        f2 = AnonymousClass3JW.A01(str4);
                        i = 1;
                    }
                    AnonymousClass09S r4 = new AnonymousClass09S(i);
                    r4.setFloatValues(f, f2);
                    r4.setDuration((long) (floatValue4 * ((float) A01)));
                    r4.setInterpolator(timeInterpolator);
                    C14210l2 r03 = new C14210l2();
                    r03.A05(r4, 0);
                    r4.addUpdateListener(new C06750Ux(this, r19, r03.A03(), A003));
                    A02(r4, A022, r19, A013);
                    return r4;
                } catch (AnonymousClass491 e) {
                    StringBuilder sb = new StringBuilder("Could not parse start and end values. ");
                    sb.append(e);
                    throw new IllegalArgumentException(sb.toString());
                }
            case '\b':
                A03((Animator) r20.A00(0), r19, null);
                return null;
            case '\t':
                ((Animator) r20.A00(0)).cancel();
                return null;
            case '\n':
                C1093751l A004 = C94724cR.A00(r20.A01(0));
                AbstractC14200l1 A014 = C94724cR.A01(r20.A00(1));
                float floatValue5 = ((Number) r20.A01(2)).floatValue();
                float floatValue6 = ((Number) r20.A01(3)).floatValue();
                float floatValue7 = ((Number) r20.A01(4)).floatValue();
                C14260l7 A023 = C67973Tp.A02(r19, r20, 6);
                ValueAnimator valueAnimator = new ValueAnimator();
                C14210l2 r04 = new C14210l2();
                r04.A05(valueAnimator, 0);
                valueAnimator.addUpdateListener(new C06740Uv(this, r19, r04.A03(), A004));
                A02(valueAnimator, A023, r19, A014);
                valueAnimator.setFloatValues(floatValue5, floatValue6);
                valueAnimator.setDuration((long) (floatValue7 * ((float) A01)));
                valueAnimator.setInterpolator((TimeInterpolator) r20.A00(5));
                return valueAnimator;
            case 11:
                A03((Animator) r20.A00(0), r19, (String) r20.A00(1));
                return null;
            case '\f':
                AbstractC14200l1 A015 = C94724cR.A01(r20.A00(0));
                float floatValue8 = ((Number) r20.A01(1)).floatValue();
                C14260l7 A024 = C67973Tp.A02(r19, r20, 3);
                AnimatorSet animatorSet2 = new AnimatorSet();
                A00(animatorSet2, (List) r20.A00(2));
                A02(animatorSet2, A024, r19, A015);
                animatorSet2.setStartDelay((long) (floatValue8 * ((float) A01)));
                return animatorSet2;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                AnimatorSet animatorSet3 = new AnimatorSet();
                animatorSet3.playSequentially((List) r20.A00(0));
                return animatorSet3;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                return C64983Hr.A01(((Number) ((ValueAnimator) r20.A00(0)).getAnimatedValue()).floatValue());
            default:
                return this.A00.A9j(r20, r21, r19);
        }
    }

    public final void A02(Animator animator, C14260l7 r8, C14230l4 r9, AbstractC14200l1 r10) {
        animator.addListener(new AnonymousClass09J(animator, this, r8, r9, r10));
    }

    public final void A03(Animator animator, C14230l4 r5, String str) {
        C14260l7 r2 = r5.A00;
        AnonymousClass4D3.A00(r2);
        if (str == null) {
            StringBuilder sb = new StringBuilder("NO_ID");
            sb.append(UUID.randomUUID().toString());
            str = sb.toString();
        }
        animator.addListener(new AnonymousClass09L(this, r2, str));
        animator.start();
        AnonymousClass3JV.A05(animator, r2, str);
    }
}
