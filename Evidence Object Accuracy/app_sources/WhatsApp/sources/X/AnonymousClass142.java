package X;

import java.io.ByteArrayInputStream;
import java.io.File;

/* renamed from: X.142  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass142 {
    public final C002701f A00;

    public AnonymousClass142(C002701f r1) {
        this.A00 = r1;
    }

    public File A00(AnonymousClass1KZ r3, byte[] bArr) {
        File A01 = A01(r3.A0D);
        if (A01 == null || !C14350lI.A0P(A01, new ByteArrayInputStream(bArr))) {
            return null;
        }
        return A01;
    }

    public File A01(String str) {
        File A02 = this.A00.A02();
        if (!A02.exists() && !A02.mkdirs()) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(".png");
        return new File(A02, sb.toString());
    }
}
