package X;

/* renamed from: X.0FQ  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0FQ extends AbstractC02880Ff {
    public final /* synthetic */ C07730Zz A00;

    @Override // X.AbstractC05330Pd
    public String A01() {
        return "INSERT OR REPLACE INTO `WorkProgress` (`work_spec_id`,`progress`) VALUES (?,?)";
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass0FQ(AnonymousClass0QN r1, C07730Zz r2) {
        super(r1);
        this.A00 = r2;
    }

    @Override // X.AbstractC02880Ff
    public /* bridge */ /* synthetic */ void A03(AbstractC12830ic r3, Object obj) {
        throw new NullPointerException("mWorkSpecId");
    }
}
