package X;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import com.whatsapp.filter.FilterUtils;
import com.whatsapp.mediacomposer.ImageComposerFragment;
import com.whatsapp.mediacomposer.MediaComposerFragment;
import org.json.JSONObject;

/* renamed from: X.33P  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass33P extends AbstractC64833Hb {
    public C93694aa A00;
    public final float A01;
    public final Rect A02 = C12980iv.A0J();
    public final RectF A03 = C12980iv.A0K();

    public AnonymousClass33P(Bitmap bitmap, Paint paint, PointF pointF, PointF pointF2, C93694aa r18, float f, int i) {
        super(bitmap, paint, pointF2, f, i);
        this.A00 = r18;
        super.A03.add(pointF);
        Canvas canvas = super.A00;
        if (canvas != null) {
            A06(canvas, super.A02, pointF.x, pointF.y, (int) this.A01);
        }
        this.A01 = f;
    }

    @Override // X.AbstractC64833Hb
    public void A05(JSONObject jSONObject) {
        super.A05(jSONObject);
        jSONObject.put("brush_blur", true);
    }

    public void A06(Canvas canvas, Paint paint, float f, float f2, int i) {
        Bitmap bitmap;
        C93694aa r4 = this.A00;
        Bitmap bitmap2 = r4.A02;
        if (bitmap2 == null) {
            Bitmap bitmap3 = r4.A03;
            if (bitmap3 == null) {
                C89534Ki r0 = r4.A04;
                if (r0 != null) {
                    MediaComposerFragment mediaComposerFragment = r0.A00.A0D;
                    if (!(mediaComposerFragment instanceof ImageComposerFragment)) {
                        bitmap = null;
                    } else {
                        ImageComposerFragment imageComposerFragment = (ImageComposerFragment) mediaComposerFragment;
                        int i2 = imageComposerFragment.A07.A01;
                        if (i2 != 0) {
                            bitmap = FilterUtils.A00(imageComposerFragment.A00, imageComposerFragment.A03, i2, true);
                        } else {
                            bitmap = imageComposerFragment.A00;
                        }
                    }
                    r4.A01 = bitmap.getWidth();
                    int height = bitmap.getHeight();
                    r4.A00 = height;
                    bitmap3 = Bitmap.createScaledBitmap(bitmap, (int) (((float) r4.A01) * 0.05f), (int) (((float) height) * 0.05f), false);
                    r4.A03 = bitmap3;
                } else {
                    throw C12960it.A0U("Origin bitmap loader is required to get origin bitmap");
                }
            }
            bitmap2 = Bitmap.createScaledBitmap(bitmap3, Math.round(((float) r4.A01) / 3.0f), Math.round(((float) r4.A00) / 3.0f), false);
            r4.A02 = bitmap2;
        }
        float f3 = (float) (i >> 1);
        int i3 = (int) (f - f3);
        int i4 = (int) (f2 - f3);
        int i5 = i3 + i;
        int i6 = i + i4;
        Rect rect = this.A02;
        float f4 = (float) i3;
        rect.left = Math.round(f4 / 3.0f);
        float f5 = (float) i4;
        rect.top = Math.round(f5 / 3.0f);
        float f6 = (float) i5;
        rect.right = Math.round(f6 / 3.0f);
        float f7 = (float) i6;
        rect.bottom = Math.round(f7 / 3.0f);
        RectF rectF = this.A03;
        rectF.left = f4;
        rectF.top = f5;
        rectF.right = f6;
        rectF.bottom = f7;
        canvas.drawBitmap(bitmap2, rect, rectF, paint);
    }
}
