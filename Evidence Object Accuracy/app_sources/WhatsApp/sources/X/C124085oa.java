package X;

import android.text.TextUtils;
import com.whatsapp.R;

/* renamed from: X.5oa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124085oa extends AbstractC16350or {
    public final String A00;
    public final /* synthetic */ C118005b7 A01;

    public C124085oa(C118005b7 r1, String str) {
        this.A01 = r1;
        this.A00 = str;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        String str = this.A00;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        C17070qD r0 = this.A01.A0E;
        r0.A03();
        return r0.A08.A0N(null, str);
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        AnonymousClass1IR r0;
        AnonymousClass1IR r6 = (AnonymousClass1IR) obj;
        C118005b7 r4 = this.A01;
        C30931Zj r2 = r4.A0I;
        StringBuilder A0k = C12960it.A0k("onTransactionDetailData loaded: ");
        A0k.append(C12960it.A1W(r6));
        C117295Zj.A1F(r2, A0k);
        if (r6 != null) {
            r4.A06 = r6;
        }
        AnonymousClass1IR r22 = r4.A07;
        String str = r22.A0H;
        if ((str == null || str.equals("0")) && (r0 = r4.A06) != null) {
            r22.A0H = r0.A0H;
        }
        r4.A0J.Ab2(new RunnableC135796Jv(r4, R.string.upi_mandate_missing_payment_method_message, 11));
    }
}
