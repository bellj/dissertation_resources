package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;

/* renamed from: X.097  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass097 extends AnimatorListenerAdapter {
    public final /* synthetic */ View A00;
    public final /* synthetic */ AbstractC12420ht A01;
    public final /* synthetic */ AnonymousClass0G0 A02;

    public AnonymousClass097(View view, AbstractC12420ht r2, AnonymousClass0G0 r3) {
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = view;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        this.A01.AaF(this.A00);
    }
}
