package X;

import com.whatsapp.conversation.conversationrow.album.MediaAlbumActivity;
import com.whatsapp.reactions.ReactionsTrayViewModel;

/* renamed from: X.52n  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1096552n implements AbstractC116455Vm {
    public final /* synthetic */ MediaAlbumActivity A00;

    @Override // X.AbstractC116455Vm
    public void AMr() {
    }

    public C1096552n(MediaAlbumActivity mediaAlbumActivity) {
        this.A00 = mediaAlbumActivity;
    }

    @Override // X.AbstractC116455Vm
    public void APc(int[] iArr) {
        MediaAlbumActivity mediaAlbumActivity = this.A00;
        if (mediaAlbumActivity.A2n()) {
            ReactionsTrayViewModel reactionsTrayViewModel = mediaAlbumActivity.A0g;
            AnonymousClass009.A05(reactionsTrayViewModel);
            reactionsTrayViewModel.A05(AbstractC36671kL.A06(iArr));
        }
    }
}
