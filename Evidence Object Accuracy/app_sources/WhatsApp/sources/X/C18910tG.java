package X;

import android.content.SharedPreferences;
import com.facebook.redex.RunnableBRunnable0Shape7S0200000_I0_7;
import com.whatsapp.util.Log;

/* renamed from: X.0tG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18910tG {
    public String A00;
    public boolean A01;
    public final C41121sw A02;
    public final C21220x4 A03;
    public final C14830m7 A04;
    public final C14820m6 A05;
    public final C22100yW A06;
    public final C16120oU A07;
    public final C14840m8 A08;
    public final C22230yk A09;
    public final C41081sr A0A;
    public final AbstractC14440lR A0B;
    public final Runnable A0C;

    public C18910tG(AbstractC15710nm r19, C14900mE r20, AnonymousClass16S r21, AnonymousClass16T r22, C21220x4 r23, C233411h r24, C14830m7 r25, C14820m6 r26, C15990oG r27, C18240s8 r28, C22100yW r29, C16120oU r30, C17220qS r31, C14840m8 r32, C22230yk r33, AnonymousClass1F2 r34, AbstractC14440lR r35) {
        this.A04 = r25;
        this.A0B = r35;
        this.A07 = r30;
        this.A09 = r33;
        this.A08 = r32;
        this.A05 = r26;
        this.A06 = r29;
        this.A03 = r23;
        C41121sw r3 = new C41121sw(r19, r20, r21, r22, r24, r25, r26, r27, r28, r29, r31, new AnonymousClass2KK(this), r34, r35);
        this.A02 = r3;
        this.A0A = new C41081sr(r3, r26, this);
        this.A0C = new RunnableBRunnable0Shape7S0200000_I0_7(this, 39, r29);
    }

    public synchronized void A00() {
        SharedPreferences sharedPreferences = this.A05.A00;
        if (!sharedPreferences.getBoolean("seamless_migration_in_progress", false)) {
            Log.e("SeamlessManager_abortSeamlessMigration/Abort called when no seamless migration is in progress.");
        } else {
            A01();
            sharedPreferences.edit().putInt("md_seamless_status", 2).apply();
        }
    }

    public final synchronized void A01() {
        this.A03.A00();
        this.A05.A18(false);
        this.A00 = null;
        this.A06.A04(this.A0A);
        this.A0B.AaP(this.A0C);
        this.A01 = false;
    }

    public void A02(AnonymousClass2KL r6) {
        long longValue;
        AnonymousClass2KM r4 = new AnonymousClass2KM();
        r4.A00 = r6.A00;
        synchronized (this) {
            r4.A04 = this.A00;
        }
        Long l = r6.A03;
        if (l == null) {
            longValue = System.currentTimeMillis() / 1000;
        } else {
            longValue = l.longValue();
        }
        r4.A02 = Long.valueOf(longValue);
        r4.A01 = r6.A01;
        r4.A03 = r6.A02;
        this.A07.A0B(r4, new AnonymousClass00E(1, 1), false);
    }
}
