package X;

import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.api.Status;

/* renamed from: X.3HY  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass3HY {
    public final int A00;

    public AnonymousClass3HY(int i) {
        this.A00 = i;
    }

    public static /* bridge */ /* synthetic */ Status A00(RemoteException remoteException) {
        StringBuilder A0h = C12960it.A0h();
        C12990iw.A1T(A0h, C12980iv.A0r(remoteException));
        return new Status(19, C12960it.A0d(remoteException.getLocalizedMessage(), A0h));
    }

    public void A01(Status status) {
        try {
            ((C56352kk) this).A00.A09(status);
        } catch (IllegalStateException e) {
            Log.w("ApiCallRunner", "Exception reporting failure", e);
        }
    }

    public void A02(AnonymousClass3CU r4, boolean z) {
        AnonymousClass1UI r2 = ((C56352kk) this).A00;
        r4.A00.put(r2, Boolean.valueOf(z));
        r2.A00(new C108224yi(r2, r4));
    }

    public void A03(C14970mL r4) {
        C56352kk r2 = (C56352kk) this;
        try {
            r2.A00.A08(r4.A04);
        } catch (RuntimeException e) {
            r2.A04(e);
        }
    }

    public void A04(Exception exc) {
        C56352kk r4 = (C56352kk) this;
        String A0r = C12980iv.A0r(exc);
        String localizedMessage = exc.getLocalizedMessage();
        StringBuilder A0t = C12980iv.A0t(A0r.length() + 2 + C12970iu.A07(localizedMessage));
        C12990iw.A1T(A0t, A0r);
        try {
            r4.A00.A09(new Status(10, C12960it.A0d(localizedMessage, A0t)));
        } catch (IllegalStateException e) {
            Log.w("ApiCallRunner", "Exception reporting failure", e);
        }
    }
}
