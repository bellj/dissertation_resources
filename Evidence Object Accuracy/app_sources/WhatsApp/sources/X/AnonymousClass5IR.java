package X;

import com.facebook.redex.RunnableBRunnable0Shape16S0100000_I1_2;
import com.whatsapp.settings.SettingsNetworkUsage;
import java.util.TimerTask;

/* renamed from: X.5IR  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5IR extends TimerTask {
    public final /* synthetic */ SettingsNetworkUsage A00;

    public AnonymousClass5IR(SettingsNetworkUsage settingsNetworkUsage) {
        this.A00 = settingsNetworkUsage;
    }

    @Override // java.util.TimerTask, java.lang.Runnable
    public void run() {
        SettingsNetworkUsage settingsNetworkUsage = this.A00;
        settingsNetworkUsage.A00.post(new RunnableBRunnable0Shape16S0100000_I1_2(settingsNetworkUsage, 12));
    }
}
