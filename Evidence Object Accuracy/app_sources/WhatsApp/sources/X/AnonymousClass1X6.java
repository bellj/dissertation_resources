package X;

/* renamed from: X.1X6  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1X6 extends AnonymousClass1X7 implements AbstractC16390ow, AbstractC16400ox, AbstractC16420oz {
    public C16470p4 A00;

    public AnonymousClass1X6(C16150oX r2, AnonymousClass1IS r3, AnonymousClass1X6 r4, long j, boolean z) {
        super(r2, r3, r4, j, z);
        this.A00 = r4.A00;
    }

    public AnonymousClass1X6(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 57, j);
    }

    public AnonymousClass1X6(C40821sO r2, AnonymousClass1IS r3, C16470p4 r4, long j, boolean z, boolean z2) {
        super(r3, (byte) 57, j);
        this.A00 = r4;
        A1D(r2, z, z2);
    }

    @Override // X.AbstractC16390ow
    public C16470p4 ABf() {
        return this.A00;
    }

    @Override // X.AbstractC16390ow
    public C33711ex ACL() {
        C16470p4 r1 = this.A00;
        if (r1 == null) {
            return null;
        }
        return AnonymousClass4EY.A00(r1, r1.A00);
    }

    @Override // X.AbstractC16390ow
    public void Abv(C16470p4 r1) {
        this.A00 = r1;
    }
}
