package X;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

/* renamed from: X.4YL  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass4YL {
    public final C92284Vg A00;

    public AnonymousClass4YL(C92284Vg r1) {
        this.A00 = r1;
    }

    public static String A00(Class cls, Object obj, StringBuilder sb) {
        sb.append(obj);
        sb.append(" to ");
        return cls.getName();
    }

    public Object A01() {
        if (!(this instanceof C114475Lr)) {
            if (this instanceof C114485Ls) {
                try {
                    return ((C114485Ls) this).A00.getConstructor(new Class[0]).newInstance(new Object[0]);
                } catch (Exception unused) {
                    return null;
                }
            } else if (!(this instanceof C114465Lq)) {
                if (this instanceof C114445Lo) {
                    throw C12980iv.A0n("newInstance");
                } else if (this instanceof C114495Lt) {
                    return C12960it.A0l();
                } else {
                    StringBuilder A0k = C12960it.A0k("Invalid or non Implemented status");
                    A0k.append(" createArray() in ");
                    throw C12990iw.A0m(C12970iu.A0s(getClass(), A0k));
                }
            }
        }
        return new AnonymousClass5IB();
    }

    public Object A02() {
        if (this instanceof C114475Lr) {
            return new LinkedHashMap();
        }
        if (this instanceof C114485Ls) {
            try {
                return ((C114485Ls) this).A00.getConstructor(new Class[0]).newInstance(new Object[0]);
            } catch (Exception unused) {
                return null;
            }
        } else if (this instanceof C114465Lq) {
            return new AnonymousClass5IH();
        } else {
            if (this instanceof C114455Lp) {
                throw C12980iv.A0n("newInstance");
            } else if (!(this instanceof C114435Ln)) {
                StringBuilder A0k = C12960it.A0k("Invalid or non Implemented status");
                A0k.append(" createObject() in ");
                throw C12990iw.A0m(C12970iu.A0s(getClass(), A0k));
            } else {
                throw C12980iv.A0n("newInstance");
            }
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v5, resolved type: java.lang.Byte[] */
    /* JADX DEBUG: Multi-variable search result rejected for r4v7, resolved type: java.lang.Short[] */
    /* JADX DEBUG: Multi-variable search result rejected for r4v9, resolved type: java.lang.Integer[] */
    /* JADX DEBUG: Multi-variable search result rejected for r4v13, resolved type: java.lang.Double[] */
    /* JADX DEBUG: Multi-variable search result rejected for r4v15, resolved type: java.lang.Float[] */
    /* JADX DEBUG: Multi-variable search result rejected for r4v17, resolved type: java.lang.Long[] */
    /* JADX WARN: Multi-variable type inference failed */
    public Object A03(Object obj) {
        boolean A1S;
        if (this instanceof C114335Lc) {
            List<Object> list = (List) obj;
            Object[] objArr = (Object[]) Array.newInstance(((C114335Lc) this).A01, list.size());
            int i = 0;
            for (Object obj2 : list) {
                objArr[i] = obj2;
                i++;
            }
            return objArr;
        } else if (this instanceof C114325Lb) {
            List<Object> list2 = (List) obj;
            long[] jArr = new long[list2.size()];
            int i2 = 0;
            for (Object obj3 : list2) {
                jArr[i2] = (long) C12960it.A05(obj3);
                i2++;
            }
            return jArr;
        } else if (this instanceof C114315La) {
            List list3 = (List) obj;
            Character[] chArr = new Character[list3.size()];
            int i3 = 0;
            for (Object obj4 : list3) {
                if (obj4 != null) {
                    chArr[i3] = Character.valueOf(obj4.toString().charAt(0));
                    i3++;
                }
            }
            return chArr;
        } else if (this instanceof AnonymousClass5LZ) {
            List<Object> list4 = (List) obj;
            char[] cArr = new char[list4.size()];
            int i4 = 0;
            for (Object obj5 : list4) {
                cArr[i4] = obj5.toString().charAt(0);
                i4++;
            }
            return cArr;
        } else if (this instanceof AnonymousClass5LY) {
            List list5 = (List) obj;
            Byte[] bArr = new Byte[list5.size()];
            int i5 = 0;
            for (Object obj6 : list5) {
                if (obj6 != null) {
                    if (!(obj6 instanceof Byte)) {
                        obj6 = Byte.valueOf(((Number) obj6).byteValue());
                    }
                    bArr[i5] = obj6;
                    i5++;
                }
            }
            return bArr;
        } else if (this instanceof AnonymousClass5LX) {
            List<Number> list6 = (List) obj;
            byte[] bArr2 = new byte[list6.size()];
            int i6 = 0;
            for (Number number : list6) {
                bArr2[i6] = number.byteValue();
                i6++;
            }
            return bArr2;
        } else if (this instanceof AnonymousClass5LW) {
            List list7 = (List) obj;
            Short[] shArr = new Short[list7.size()];
            int i7 = 0;
            for (Object obj7 : list7) {
                if (obj7 != null) {
                    if (!(obj7 instanceof Short)) {
                        obj7 = Short.valueOf(((Number) obj7).shortValue());
                    }
                    shArr[i7] = obj7;
                    i7++;
                }
            }
            return shArr;
        } else if (this instanceof AnonymousClass5LV) {
            List<Number> list8 = (List) obj;
            short[] sArr = new short[list8.size()];
            int i8 = 0;
            for (Number number2 : list8) {
                sArr[i8] = number2.shortValue();
                i8++;
            }
            return sArr;
        } else if (this instanceof AnonymousClass5LU) {
            List list9 = (List) obj;
            Integer[] numArr = new Integer[list9.size()];
            int i9 = 0;
            for (Object obj8 : list9) {
                if (obj8 != null) {
                    if (!(obj8 instanceof Integer)) {
                        obj8 = Integer.valueOf(C12960it.A05(obj8));
                    }
                    numArr[i9] = obj8;
                    i9++;
                }
            }
            return numArr;
        } else if (this instanceof AnonymousClass5LT) {
            List<Object> list10 = (List) obj;
            int[] iArr = new int[list10.size()];
            int i10 = 0;
            for (Object obj9 : list10) {
                iArr[i10] = C12960it.A05(obj9);
                i10++;
            }
            return iArr;
        } else if (this instanceof AnonymousClass5LS) {
            List list11 = (List) obj;
            Boolean[] boolArr = new Boolean[list11.size()];
            int i11 = 0;
            for (Object obj10 : list11) {
                if (obj10 != null) {
                    if (obj10 instanceof Boolean) {
                        A1S = C12970iu.A1Y(obj10);
                    } else if (obj10 instanceof Number) {
                        A1S = C12960it.A1S(C12960it.A05(obj10));
                    } else {
                        StringBuilder A0k = C12960it.A0k("can not convert ");
                        A0k.append(obj10);
                        throw C12990iw.A0m(C12960it.A0d(" toBoolean", A0k));
                    }
                    boolArr[i11] = Boolean.valueOf(A1S);
                    i11++;
                }
            }
            return boolArr;
        } else if (this instanceof AnonymousClass5LR) {
            List<Object> list12 = (List) obj;
            boolean[] zArr = new boolean[list12.size()];
            int i12 = 0;
            for (Object obj11 : list12) {
                zArr[i12] = C12970iu.A1Y(obj11);
                i12++;
            }
            return zArr;
        } else if (this instanceof AnonymousClass5LQ) {
            List list13 = (List) obj;
            Double[] dArr = new Double[list13.size()];
            int i13 = 0;
            for (Object obj12 : list13) {
                if (obj12 != null) {
                    if (!(obj12 instanceof Double)) {
                        obj12 = Double.valueOf(C72453ed.A00(obj12));
                    }
                    dArr[i13] = obj12;
                    i13++;
                }
            }
            return dArr;
        } else if (this instanceof AnonymousClass5LP) {
            List<Object> list14 = (List) obj;
            double[] dArr2 = new double[list14.size()];
            int i14 = 0;
            for (Object obj13 : list14) {
                dArr2[i14] = C72453ed.A00(obj13);
                i14++;
            }
            return dArr2;
        } else if (this instanceof AnonymousClass5LO) {
            List list15 = (List) obj;
            Float[] fArr = new Float[list15.size()];
            int i15 = 0;
            for (Object obj14 : list15) {
                if (obj14 != null) {
                    if (!(obj14 instanceof Float)) {
                        obj14 = Float.valueOf(C72453ed.A02(obj14));
                    }
                    fArr[i15] = obj14;
                    i15++;
                }
            }
            return fArr;
        } else if (this instanceof AnonymousClass5LN) {
            List<Object> list16 = (List) obj;
            float[] fArr2 = new float[list16.size()];
            int i16 = 0;
            for (Object obj15 : list16) {
                fArr2[i16] = C72453ed.A02(obj15);
                i16++;
            }
            return fArr2;
        } else if (this instanceof AnonymousClass5LM) {
            List list17 = (List) obj;
            Long[] lArr = new Long[list17.size()];
            int i17 = 0;
            for (Object obj16 : list17) {
                if (obj16 != null) {
                    if (!(obj16 instanceof Float)) {
                        obj16 = Long.valueOf(C12980iv.A0G(obj16));
                    }
                    lArr[i17] = obj16;
                    i17++;
                }
            }
            return lArr;
        } else if (this instanceof C114425Lm) {
            return C12960it.A0Y(obj);
        } else {
            if (!(this instanceof C114415Ll)) {
                if (!(this instanceof C114405Lk)) {
                    if (!(this instanceof C114395Lj)) {
                        if (!(this instanceof C114385Li)) {
                            if (!(this instanceof AnonymousClass5Lh)) {
                                if (!(this instanceof C114375Lg)) {
                                    if (!(this instanceof C114365Lf)) {
                                        if (!(this instanceof C114355Le)) {
                                            return obj;
                                        }
                                        if (obj == null) {
                                            return null;
                                        }
                                        return new BigDecimal(obj.toString());
                                    } else if (obj == null) {
                                        return null;
                                    } else {
                                        return new BigInteger(obj.toString());
                                    }
                                } else if (obj != null) {
                                    Class<?> cls = obj.getClass();
                                    if (Boolean.class.isAssignableFrom(cls)) {
                                        return obj;
                                    }
                                    StringBuilder A0k2 = C12960it.A0k("can not map a ");
                                    throw new C82823wF(C12960it.A0d(A00(Boolean.class, cls, A0k2), A0k2));
                                }
                            } else if (obj != null) {
                                Class<?> cls2 = obj.getClass();
                                if (Date.class.isAssignableFrom(cls2)) {
                                    return obj;
                                }
                                if (Long.class.isAssignableFrom(cls2)) {
                                    return new Date(C12980iv.A0G(obj));
                                }
                                if (String.class.isAssignableFrom(cls2)) {
                                    try {
                                        return DateFormat.getInstance().parse(obj.toString());
                                    } catch (ParseException e) {
                                        throw new C82823wF(e);
                                    }
                                } else {
                                    StringBuilder A0k3 = C12960it.A0k("can not map a ");
                                    throw new C82823wF(C12960it.A0d(A00(Date.class, cls2, A0k3), A0k3));
                                }
                            }
                        } else if (obj != null) {
                            Class<?> cls3 = obj.getClass();
                            if (Double.class.isAssignableFrom(cls3)) {
                                return obj;
                            }
                            if (Integer.class.isAssignableFrom(cls3) || Long.class.isAssignableFrom(cls3) || BigDecimal.class.isAssignableFrom(cls3) || Float.class.isAssignableFrom(cls3)) {
                                return Double.valueOf(C72453ed.A00(obj));
                            }
                            if (String.class.isAssignableFrom(cls3)) {
                                return Double.valueOf(obj.toString());
                            }
                            StringBuilder A0k4 = C12960it.A0k("can not map a ");
                            throw new C82823wF(C12960it.A0d(A00(Double.class, cls3, A0k4), A0k4));
                        }
                    } else if (obj != null) {
                        Class<?> cls4 = obj.getClass();
                        if (Float.class.isAssignableFrom(cls4)) {
                            return obj;
                        }
                        if (Integer.class.isAssignableFrom(cls4) || Long.class.isAssignableFrom(cls4) || BigDecimal.class.isAssignableFrom(cls4) || Double.class.isAssignableFrom(cls4)) {
                            return Float.valueOf(C72453ed.A02(obj));
                        }
                        if (String.class.isAssignableFrom(cls4)) {
                            return Float.valueOf(obj.toString());
                        }
                        StringBuilder A0k5 = C12960it.A0k("can not map a ");
                        throw new C82823wF(C12960it.A0d(A00(Float.class, cls4, A0k5), A0k5));
                    }
                } else if (obj != null) {
                    Class<?> cls5 = obj.getClass();
                    if (Integer.class.isAssignableFrom(cls5)) {
                        return obj;
                    }
                    if (Long.class.isAssignableFrom(cls5) || Double.class.isAssignableFrom(cls5) || BigDecimal.class.isAssignableFrom(cls5) || Float.class.isAssignableFrom(cls5)) {
                        return Integer.valueOf(C12960it.A05(obj));
                    }
                    if (String.class.isAssignableFrom(cls5)) {
                        return Integer.valueOf(obj.toString());
                    }
                    StringBuilder A0k6 = C12960it.A0k("can not map a ");
                    throw new C82823wF(C12960it.A0d(A00(Integer.class, cls5, A0k6), A0k6));
                }
            } else if (obj != null) {
                Class<?> cls6 = obj.getClass();
                if (Long.class.isAssignableFrom(cls6)) {
                    return obj;
                }
                if (Integer.class.isAssignableFrom(cls6) || Double.class.isAssignableFrom(cls6) || BigDecimal.class.isAssignableFrom(cls6) || Float.class.isAssignableFrom(cls6)) {
                    return Long.valueOf(C12980iv.A0G(obj));
                }
                if (String.class.isAssignableFrom(cls6)) {
                    return Long.valueOf(obj.toString());
                }
                StringBuilder A0k7 = C12960it.A0k("can not map a ");
                throw new C82823wF(C12960it.A0d(A00(Long.class, cls6, A0k7), A0k7));
            }
            return null;
        }
    }
}
