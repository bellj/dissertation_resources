package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.storage.StorageUsageMediaPreviewView;

/* renamed from: X.3kA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75633kA extends AnonymousClass03U {
    public final View A00;
    public final View A01;
    public final TextView A02;
    public final TextView A03;
    public final TextView A04;
    public final AnonymousClass018 A05;
    public final C16120oU A06;
    public final StorageUsageMediaPreviewView A07;
    public final StorageUsageMediaPreviewView A08;

    public C75633kA(View view, AnonymousClass018 r5, C16120oU r6) {
        super(view);
        this.A06 = r6;
        this.A05 = r5;
        View A0D = AnonymousClass028.A0D(view, R.id.forwarded_files_container);
        this.A00 = A0D;
        this.A02 = C12960it.A0I(view, R.id.forwarded_files_size_text_view);
        this.A07 = (StorageUsageMediaPreviewView) AnonymousClass028.A0D(view, R.id.forwarded_files_preview_view);
        View A0D2 = AnonymousClass028.A0D(view, R.id.large_files_container);
        this.A01 = A0D2;
        this.A04 = C12960it.A0I(view, R.id.large_files_title_text_view);
        this.A03 = C12960it.A0I(view, R.id.large_files_size_text_view);
        this.A08 = (StorageUsageMediaPreviewView) AnonymousClass028.A0D(view, R.id.large_files_preview_view);
        AnonymousClass23N.A01(A0D);
        AnonymousClass23N.A01(A0D2);
    }
}
