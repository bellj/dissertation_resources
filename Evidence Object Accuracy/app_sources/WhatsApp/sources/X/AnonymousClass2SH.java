package X;

import com.whatsapp.biz.product.view.activity.ProductDetailActivity;

/* renamed from: X.2SH  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2SH {
    public void A00(String str) {
        if (!(this instanceof C59142u3)) {
            ProductDetailActivity productDetailActivity = ((C59152u4) this).A00;
            C44691zO A05 = productDetailActivity.A0P.A05(null, str);
            C44691zO r1 = productDetailActivity.A0Q;
            if (r1 == null || (r1.A0D.equals(str) && !r1.equals(A05))) {
                ((AnonymousClass283) productDetailActivity).A00 = 0;
                productDetailActivity.A0Q = productDetailActivity.A0P.A05(productDetailActivity.A0h, str);
                productDetailActivity.A2e();
                return;
            }
            return;
        }
        AbstractActivityC59392ue r2 = ((C59142u3) this).A00;
        C44691zO A052 = r2.A09.A05(null, str);
        if (A052 != null) {
            r2.A04.A0K(A052);
        }
    }

    public void A01(String str) {
        if (!(this instanceof C59142u3)) {
            ProductDetailActivity productDetailActivity = ((C59152u4) this).A00;
            if (str.equals(productDetailActivity.A0l)) {
                productDetailActivity.A0Q = productDetailActivity.A0P.A05(productDetailActivity.A0h, str);
                productDetailActivity.A2e();
                return;
            }
            return;
        }
        AbstractActivityC59392ue r2 = ((C59142u3) this).A00;
        C44691zO A05 = r2.A09.A05(null, str);
        if (A05 != null) {
            r2.A04.A0K(A05);
        }
    }
}
