package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.25O  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass25O extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass25O A04;
    public static volatile AnonymousClass255 A05;
    public int A00;
    public AbstractC27881Jp A01;
    public AbstractC27881Jp A02;
    public AbstractC27881Jp A03;

    static {
        AnonymousClass25O r0 = new AnonymousClass25O();
        A04 = r0;
        r0.A0W();
    }

    public AnonymousClass25O() {
        AbstractC27881Jp r0 = AbstractC27881Jp.A01;
        this.A01 = r0;
        this.A03 = r0;
        this.A02 = r0;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A09(this.A01, 1);
        }
        if ((i3 & 2) == 2) {
            i2 += CodedOutputStream.A09(this.A03, 2);
        }
        if ((i3 & 4) == 4) {
            i2 += CodedOutputStream.A09(this.A02, 3);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0K(this.A01, 1);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0K(this.A03, 2);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0K(this.A02, 3);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
