package X;

import android.content.Intent;

/* renamed from: X.66i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1322966i implements AbstractC42541vN {
    public final /* synthetic */ ActivityC13790kL A00;
    public final /* synthetic */ AbstractC28681Oo A01;
    public final /* synthetic */ C128275vq A02;
    public final /* synthetic */ Object A03;

    public C1322966i(ActivityC13790kL r1, AbstractC28681Oo r2, C128275vq r3, Object obj) {
        this.A02 = r3;
        this.A03 = obj;
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC42541vN
    public boolean ALt(Intent intent, int i, int i2) {
        C128275vq r1 = this.A02;
        if (i != 1001) {
            return false;
        }
        r1.A00.A0I(new Runnable(this.A01, this.A03) { // from class: X.6Hp
            public final /* synthetic */ AbstractC28681Oo A00;
            public final /* synthetic */ Object A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // java.lang.Runnable
            public final void run() {
                C119625ek.A00(this.A00.AAN(), this.A01);
            }
        });
        this.A00.A2Z(this);
        return true;
    }
}
