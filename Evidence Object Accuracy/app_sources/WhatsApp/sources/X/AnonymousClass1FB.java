package X;

import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.1FB  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1FB {
    public final AbstractC15710nm A00;
    public final C14900mE A01;
    public final C18430sR A02;
    public final C14650lo A03;
    public final C19850um A04;
    public final C90844Pl A05;
    public final C20100vD A06;
    public final C15680nj A07;
    public final C14850m9 A08;
    public final C19870uo A09;
    public final C17220qS A0A;
    public final C19860un A0B;
    public final Map A0C = new HashMap();

    public AnonymousClass1FB(AbstractC15710nm r2, C14900mE r3, C18430sR r4, C14650lo r5, C19850um r6, C20100vD r7, C16590pI r8, C15680nj r9, C14850m9 r10, C19870uo r11, C17220qS r12, C19860un r13) {
        this.A01 = r3;
        this.A00 = r2;
        this.A0A = r12;
        this.A04 = r6;
        this.A07 = r9;
        this.A03 = r5;
        this.A0B = r13;
        this.A05 = new C90844Pl(r8, r10);
        this.A06 = r7;
        this.A08 = r10;
        this.A09 = r11;
        this.A02 = r4;
    }

    public void A00(AnonymousClass016 r9, AnonymousClass3EQ r10) {
        Map map = this.A0C;
        AnonymousClass2EK r2 = (AnonymousClass2EK) map.get(r10);
        if (r2 == null) {
            r2 = new AnonymousClass2EK(this.A01, this.A04, this.A05, r10.A00, r10.A03);
            map.put(r10, r2);
        }
        r2.A02 = r9;
        C90834Pk r1 = new C90834Pk(r2.A01);
        r1.A01 = r2.A01();
        r2.A02.A0B(r1);
    }

    public void A01(AnonymousClass3EQ r32) {
        C90834Pk r2;
        int ceil;
        Map map = this.A0C;
        AnonymousClass2EK r7 = (AnonymousClass2EK) map.get(r32);
        if (r7 == null) {
            r7 = new AnonymousClass2EK(this.A01, this.A04, this.A05, r32.A00, r32.A03);
            map.put(r32, r7);
        }
        int i = r7.A01;
        if (i == 0) {
            r2 = new C90834Pk(0);
        } else {
            if (i != 5) {
                int i2 = r7.A00;
                List list = r7.A07;
                if (i2 < list.size()) {
                    C90844Pl r8 = r7.A05;
                    int size = list.size();
                    boolean z = false;
                    if (r7.A00 == 0) {
                        z = true;
                    }
                    float f = (float) r8.A01.A00.getResources().getDisplayMetrics().heightPixels;
                    float f2 = r8.A00;
                    if (f < f2) {
                        ceil = 1;
                    } else {
                        ceil = (int) Math.ceil((double) (f / f2));
                    }
                    if (!z || size >= ceil * 3) {
                        size = ceil << 1;
                    }
                    int min = Math.min(r8.A02.A02(464), size);
                    List<String> subList = list.subList(r7.A00, Math.min(list.size(), r7.A00 + min));
                    r7.A00 += min;
                    ArrayList arrayList = new ArrayList();
                    for (String str : subList) {
                        if (r7.A04.A05(null, str) == null) {
                            arrayList.add(str);
                        }
                    }
                    if (arrayList.isEmpty()) {
                        if (r7.A00 < list.size()) {
                            ArrayList arrayList2 = new ArrayList();
                            r2 = new C90834Pk(1);
                            r2.A01 = arrayList2;
                        }
                    } else if (this.A08.A07(1096)) {
                        C20100vD r13 = this.A06;
                        C19880up r14 = r13.A01;
                        C19840ul r0 = r14.A0D;
                        C14650lo r02 = r14.A02;
                        AbstractC15710nm r15 = r14.A00;
                        C17560r0 r12 = r14.A05;
                        C15680nj r11 = r14.A0A;
                        C19830uk r9 = r14.A08;
                        C17220qS r82 = r14.A0C;
                        C19860un r6 = r14.A0E;
                        AbstractC14440lR r5 = r14.A0F;
                        C19810ui r4 = r14.A04;
                        r13.A00(new C59272uH(r15, r02, r7, r4, r12, new AnonymousClass4RQ(r4, r12), r14.A07, r9, r32, r14.A09, r11, r14.A0B, r82, r0, r6, r5));
                        return;
                    } else {
                        C18430sR r1 = this.A02;
                        AnonymousClass3HN r83 = new AnonymousClass3HN(1);
                        UserJid userJid = r32.A00;
                        String str2 = r32.A02;
                        String str3 = r32.A01;
                        AnonymousClass01J r10 = r1.A02.A00.A01;
                        C59032tr r102 = new C59032tr((AbstractC15710nm) r10.A4o.get(), (C14650lo) r10.A2V.get(), r7, r83, (C15680nj) r10.A4e.get(), userJid, (C19870uo) r10.A8U.get(), (C17220qS) r10.ABt.get(), (C19860un) r10.A1R.get(), str2, str3, arrayList);
                        if (r102.A01.A08()) {
                            r102.A02();
                            return;
                        } else {
                            r102.A03();
                            return;
                        }
                    }
                }
            }
            r2 = new C90834Pk(5);
        }
        r7.A02(r2);
    }
}
