package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2mg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57222mg extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57222mg A03;
    public static volatile AnonymousClass255 A04;
    public int A00;
    public AbstractC27881Jp A01;
    public AbstractC27881Jp A02;

    static {
        C57222mg r0 = new C57222mg();
        A03 = r0;
        r0.A0W();
    }

    public C57222mg() {
        AbstractC27881Jp r0 = AbstractC27881Jp.A01;
        this.A02 = r0;
        this.A01 = r0;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r5, Object obj, Object obj2) {
        switch (r5.ordinal()) {
            case 0:
                return A03;
            case 1:
                AbstractC462925h r6 = (AbstractC462925h) obj;
                C57222mg r7 = (C57222mg) obj2;
                this.A02 = r6.Afm(this.A02, r7.A02, C12960it.A1R(this.A00), C12960it.A1R(r7.A00));
                this.A01 = r6.Afm(this.A01, r7.A01, C12960it.A1V(this.A00 & 2, 2), C12960it.A1V(r7.A00 & 2, 2));
                if (r6 == C463025i.A00) {
                    this.A00 |= r7.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r62 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        int A032 = r62.A03();
                        if (A032 == 0) {
                            break;
                        } else if (A032 == 10) {
                            this.A00 |= 1;
                            this.A02 = r62.A08();
                        } else if (A032 == 18) {
                            this.A00 |= 2;
                            this.A01 = r62.A08();
                        } else if (!A0a(r62, A032)) {
                            break;
                        }
                    } catch (C28971Pt e) {
                        throw AbstractC27091Fz.A0J(e, this);
                    } catch (IOException e2) {
                        throw AbstractC27091Fz.A0K(this, e2);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C57222mg();
            case 5:
                return new C82223vH();
            case 6:
                break;
            case 7:
                if (A04 == null) {
                    synchronized (C57222mg.class) {
                        if (A04 == null) {
                            A04 = AbstractC27091Fz.A09(A03);
                        }
                    }
                }
                return A04;
            default:
                throw C12970iu.A0z();
        }
        return A03;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i2 = AbstractC27091Fz.A05(this.A02, 1, 0);
        }
        if ((i3 & 2) == 2) {
            i2 = AbstractC27091Fz.A05(this.A01, 2, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0K(this.A02, 1);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0K(this.A01, 2);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
