package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.4an  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C93824an {
    public final int A00;
    public final int A01;
    public final int A02;
    public final long A03;
    public final long A04;
    public final long A05;
    public final AbstractC14640lm A06;
    public final UserJid A07;
    public final UserJid A08;
    public final AnonymousClass1IS A09;
    public final boolean A0A;
    public final boolean A0B;
    public final boolean A0C;
    public final boolean A0D;

    public C93824an(AbstractC14640lm r3, UserJid userJid, UserJid userJid2, int i, int i2, long j, long j2, long j3, boolean z, boolean z2, boolean z3, boolean z4) {
        this.A09 = null;
        this.A06 = r3;
        this.A08 = userJid;
        this.A07 = userJid2;
        this.A0B = z;
        this.A04 = j;
        this.A0A = z2;
        this.A0D = z3;
        this.A02 = i;
        this.A00 = 0;
        this.A0C = z4;
        this.A05 = j2;
        this.A01 = i2;
        this.A03 = j3;
    }

    public C93824an(UserJid userJid, UserJid userJid2, AnonymousClass1IS r7, int i, int i2, int i3, long j, long j2, long j3, boolean z, boolean z2, boolean z3, boolean z4) {
        AbstractC14640lm r2 = r7.A00;
        AnonymousClass009.A05(r2);
        this.A09 = r7;
        AnonymousClass009.A05(r2);
        boolean equals = r2.equals(r2);
        StringBuilder A0k = C12960it.A0k("key=");
        A0k.append(r7);
        AnonymousClass009.A0C(C12960it.A0Z(r2, "; jid=", A0k), equals);
        this.A06 = r2;
        this.A08 = userJid;
        this.A07 = userJid2;
        this.A0B = z;
        this.A04 = j;
        this.A0A = z2;
        this.A0D = z3;
        this.A02 = i;
        this.A00 = i2;
        this.A0C = z4;
        this.A05 = j2;
        this.A01 = i3;
        this.A03 = j3;
    }
}
