package X;

/* renamed from: X.51h  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C1093351h implements AnonymousClass5T5 {
    public final String A00;

    public C1093351h(String str) {
        this.A00 = str;
    }

    @Override // X.AnonymousClass5T5
    public boolean Aej(AnonymousClass28D r3) {
        return r3.A0H() != null && r3.A0H().equals(this.A00);
    }
}
