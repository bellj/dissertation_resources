package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.2M1  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2M1 extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass2M1 A04;
    public static volatile AnonymousClass255 A05;
    public int A00;
    public int A01 = 0;
    public int A02;
    public Object A03;

    static {
        AnonymousClass2M1 r0 = new AnonymousClass2M1();
        A04 = r0;
        r0.A0W();
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.A01 == 1) {
            i2 = 0 + CodedOutputStream.A0A((AnonymousClass1G0) this.A03, 1);
        }
        if (this.A01 == 2) {
            i2 += CodedOutputStream.A0A((AnonymousClass1G0) this.A03, 2);
        }
        if (this.A01 == 3) {
            i2 += CodedOutputStream.A0A((AnonymousClass1G0) this.A03, 3);
        }
        if ((this.A00 & 8) == 8) {
            i2 += CodedOutputStream.A04(4, this.A02);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if (this.A01 == 1) {
            codedOutputStream.A0L((AnonymousClass1G0) this.A03, 1);
        }
        if (this.A01 == 2) {
            codedOutputStream.A0L((AnonymousClass1G0) this.A03, 2);
        }
        if (this.A01 == 3) {
            codedOutputStream.A0L((AnonymousClass1G0) this.A03, 3);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0F(4, this.A02);
        }
        this.unknownFields.A02(codedOutputStream);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0066, code lost:
        if (r9.A01 == 2) goto L_0x0072;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x006b, code lost:
        if (r9.A01 == 1) goto L_0x0072;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0070, code lost:
        if (r9.A01 == 3) goto L_0x0072;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0072, code lost:
        r5 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0073, code lost:
        r9.A03 = r11.Afv(r9.A03, r12.A03, r5);
     */
    @Override // X.AbstractC27091Fz
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object A0V(X.AnonymousClass25B r10, java.lang.Object r11, java.lang.Object r12) {
        /*
        // Method dump skipped, instructions count: 424
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2M1.A0V(X.25B, java.lang.Object, java.lang.Object):java.lang.Object");
    }
}
