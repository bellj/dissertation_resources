package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5f8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119805f8 extends AnonymousClass1ZZ {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(22);
    public AbstractC30791Yv A00;
    public AbstractC30791Yv A01;
    public C1315463e A02;
    public LinkedHashSet A03;

    @Override // X.AnonymousClass1ZP
    public void A01(AnonymousClass102 r1, AnonymousClass1V8 r2, int i) {
    }

    @Override // X.AnonymousClass1ZP
    public void A02(List list, int i) {
    }

    @Override // X.AnonymousClass1ZY
    public AnonymousClass1ZR A06() {
        return null;
    }

    @Override // X.AnonymousClass1ZY
    public AnonymousClass1ZR A07() {
        return null;
    }

    @Override // X.AnonymousClass1ZY
    public String A08() {
        return null;
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C119805f8() {
    }

    public C119805f8(Parcel parcel) {
        super(parcel);
        Parcelable A0I = C12990iw.A0I(parcel, C119805f8.class);
        AnonymousClass009.A05(A0I);
        A0C((C1315463e) A0I);
        this.A01 = AnonymousClass102.A00(parcel);
        this.A00 = AnonymousClass102.A00(parcel);
        int readInt = parcel.readInt();
        this.A03 = new LinkedHashSet();
        for (int i = 0; i < readInt; i++) {
            this.A03.add(AnonymousClass102.A00(parcel));
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r8v0, resolved type: java.lang.Object */
    /* JADX DEBUG: Multi-variable search result rejected for r8v2, resolved type: org.json.JSONObject */
    /* JADX DEBUG: Multi-variable search result rejected for r8v3, resolved type: org.json.JSONObject */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // X.AnonymousClass1ZP
    public String A03() {
        JSONObject jSONObject;
        try {
            JSONObject A0a = C117295Zj.A0a();
            BigDecimal bigDecimal = this.A06;
            if (bigDecimal != null) {
                A0a.put("balance", bigDecimal.longValue());
            }
            long j = super.A00;
            if (j > 0) {
                A0a.put("balanceTs", j);
            }
            if (!TextUtils.isEmpty(super.A02)) {
                A0a.put("credentialId", super.A02);
            }
            long j2 = super.A01;
            if (j2 > 0) {
                A0a.put("createTs", j2);
            }
            C1315463e r4 = this.A02;
            JSONObject A0a2 = C117295Zj.A0a();
            try {
                A0a2.put("id", r4.A02);
                C1316263m r10 = r4.A01;
                Object obj = "";
                if (r10 != null) {
                    jSONObject = C117295Zj.A0a();
                    try {
                        C117315Zl.A0W(r10.A02, "primary", jSONObject);
                        C117315Zl.A0W(r10.A01, "local", jSONObject);
                        jSONObject.put("updateTsInMicroSeconds", r10.A00);
                    } catch (JSONException unused) {
                        Log.e("PAY: NoviBalance toJson threw exception");
                    }
                } else {
                    jSONObject = obj;
                }
                A0a2.put("balance", jSONObject);
                AnonymousClass63Y r6 = r4.A00;
                JSONObject jSONObject2 = obj;
                if (r6 != null) {
                    JSONObject A0a3 = C117295Zj.A0a();
                    try {
                        AbstractC30791Yv r9 = r6.A02;
                        A0a3.put("primary_iso_code", ((AbstractC30781Yu) r9).A04);
                        AbstractC30791Yv r62 = r6.A01;
                        A0a3.put("local_iso_code", ((AbstractC30781Yu) r62).A04);
                        A0a3.put("primary-currency", r9.Aew());
                        A0a3.put("local-currency", r62.Aew());
                        jSONObject2 = A0a3;
                    } catch (JSONException unused2) {
                        Log.e("PAY: CurrencyPreference toJson threw exception");
                        jSONObject2 = A0a3;
                    }
                }
                A0a2.put("currency", jSONObject2);
                A0a2.put("kycStatus", r4.A03);
                A0a2.put("kycTier", r4.A04);
            } catch (JSONException unused3) {
                Log.e("PAY: NoviAccount toJson threw exception");
            }
            A0a.put("Novi", A0a2);
            A0a.put("currencyType", ((AbstractC30781Yu) this.A01).A00);
            A0a.put("currency", this.A01.Aew());
            A0a.put("defaultCurrencyType", ((AbstractC30781Yu) this.A00).A00);
            A0a.put("defaultCurrency", this.A00.Aew());
            A0a.put("supportedCurrenciesCount", this.A03.size());
            int i = 0;
            Iterator it = this.A03.iterator();
            while (it.hasNext()) {
                AbstractC30791Yv r2 = (AbstractC30791Yv) it.next();
                StringBuilder A0h = C12960it.A0h();
                A0h.append("supportedCurrencyType_");
                A0a.put(C12960it.A0f(A0h, i), ((AbstractC30781Yu) r2).A00);
                StringBuilder A0h2 = C12960it.A0h();
                A0h2.append("supportedCurrency_");
                A0a.put(C12960it.A0f(A0h2, i), r2.Aew());
                i++;
            }
            return A0a.toString();
        } catch (JSONException unused4) {
            Log.e("PAY: NoviPaymentMethodCountryData toDBString threw exception");
            return null;
        }
    }

    @Override // X.AnonymousClass1ZP
    public void A04(String str) {
        AnonymousClass63Y r7;
        if (!TextUtils.isEmpty(str)) {
            try {
                JSONObject A05 = C13000ix.A05(str);
                this.A06 = BigDecimal.valueOf(A05.optLong("balance", 0));
                super.A00 = A05.optLong("balanceTs", -1);
                super.A02 = A05.optString("credentialId", null);
                super.A01 = A05.optLong("createTs", -1);
                String optString = A05.optString("Novi", "");
                C1315463e r2 = null;
                if (!TextUtils.isEmpty(optString)) {
                    try {
                        JSONObject A052 = C13000ix.A05(optString);
                        String optString2 = A052.optString("id", "");
                        C1316263m A01 = C1316263m.A01(A052.optString("balance", ""));
                        String optString3 = A052.optString("currency", "");
                        if (TextUtils.isEmpty(optString3)) {
                            r7 = null;
                        } else {
                            if (!TextUtils.isEmpty(optString3)) {
                                try {
                                    JSONObject A053 = C13000ix.A05(optString3);
                                    A053.optString("local_iso_code", A053.optString("fiat-iso-code", ""));
                                    String optString4 = A053.optString("primary_iso_code", A053.optString("crypto-iso-code", ""));
                                    JSONObject optJSONObject = A053.optJSONObject("local-currency");
                                    if (optJSONObject == null) {
                                        optJSONObject = A053.optJSONObject("fiat-currency");
                                    }
                                    C30771Yt r5 = new C30771Yt(optJSONObject);
                                    JSONObject optJSONObject2 = A053.optJSONObject("primary-currency");
                                    if (optJSONObject2 == null) {
                                        optJSONObject2 = A053.optJSONObject("crypto-currency");
                                    }
                                    r7 = new AnonymousClass63Y(r5, new C30801Yw(optJSONObject2), optString4);
                                } catch (JSONException unused) {
                                    Log.e("PAY: CurrencyPreference fromJsonString threw exception");
                                }
                            }
                            r7 = null;
                        }
                        r2 = new C1315463e(r7, A01, optString2, A052.optString("kycStatus", "NOT_READY_FOR_ASSESSMENT"), A052.optString("kycTier", "NONE"));
                    } catch (JSONException unused2) {
                        Log.e("PAY: NoviAccount fromJsonString threw exception");
                    }
                }
                AnonymousClass009.A05(r2);
                this.A02 = r2;
                this.A01 = AnonymousClass102.A01(A05.optJSONObject("currency"), A05.optInt("currencyType"));
                this.A00 = AnonymousClass102.A01(A05.optJSONObject("defaultCurrency"), A05.optInt("defaultCurrencyType"));
                int optInt = A05.optInt("supportedCurrenciesCount");
                this.A03 = new LinkedHashSet();
                for (int i = 0; i < optInt; i++) {
                    StringBuilder A0h = C12960it.A0h();
                    A0h.append("supportedCurrencyType_");
                    int optInt2 = A05.optInt(C12960it.A0f(A0h, i));
                    StringBuilder A0h2 = C12960it.A0h();
                    A0h2.append("supportedCurrency_");
                    this.A03.add(AnonymousClass102.A01(A05.optJSONObject(C12960it.A0f(A0h2, i)), optInt2));
                }
            } catch (JSONException unused3) {
                Log.e("PAY: NoviPaymentMethodCountryData fromDBString threw exception");
            }
        }
    }

    @Override // X.AnonymousClass1ZY
    public AbstractC28901Pl A05() {
        C30841Za r2 = new C30841Za(C17930rd.A00("US"), this.A02.A02, "Novi", this.A06, this.A03, 2, 2);
        r2.A08 = this;
        r2.A00 = super.A00;
        r2.A0B = "Novi";
        return r2;
    }

    @Override // X.AnonymousClass1ZY
    public LinkedHashSet A09() {
        return this.A03;
    }

    public void A0B(AnonymousClass102 r3) {
        this.A01 = r3.A02("USDP");
        this.A00 = r3.A02("USDP");
        this.A03 = new LinkedHashSet(Collections.singleton(r3.A02("USDP")));
    }

    public void A0C(C1315463e r2) {
        this.A02 = r2;
        super.A02 = r2.A02;
        C1316263m r0 = r2.A01;
        if (r0 != null) {
            this.A06 = r0.A02.A01.A00;
        }
    }

    @Override // X.AnonymousClass1ZZ, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        this.A01.writeToParcel(parcel, i);
        this.A00.writeToParcel(parcel, i);
        parcel.writeInt(this.A03.size());
        Iterator it = this.A03.iterator();
        while (it.hasNext()) {
            ((AbstractC30791Yv) it.next()).writeToParcel(parcel, i);
        }
    }
}
