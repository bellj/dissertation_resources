package X;

import android.os.Build;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.60h  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1308760h {
    public static HashSet A00(Object... objArr) {
        HashSet A12 = C12970iu.A12();
        for (Object obj : objArr) {
            A12.add(obj);
        }
        return A12;
    }

    public static boolean A01(char c, char c2, char c3) {
        String str;
        int indexOf;
        if (!Build.MANUFACTURER.equals("samsung") || (indexOf = (str = Build.FINGERPRINT).indexOf(":user/")) < 3) {
            return false;
        }
        char charAt = str.charAt(indexOf - 3);
        char charAt2 = str.charAt(indexOf - 2);
        char charAt3 = str.charAt(indexOf - 1);
        if (charAt <= c) {
            if (charAt != c) {
                return false;
            }
            if (charAt2 <= c2) {
                if (charAt2 != c2 || charAt3 < c3) {
                    return false;
                }
                return true;
            }
        }
        return true;
    }

    public static boolean A02(Set set) {
        return set.contains(C1310160w.A04) || set.contains(C1310160w.A05) || set.contains(C1310160w.A06) || set.contains(C1310160w.A07);
    }
}
