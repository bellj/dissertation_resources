package X;

import android.graphics.drawable.Drawable;

/* renamed from: X.2JQ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2JQ extends AnonymousClass2JR {
    public final Integer A00;

    public AnonymousClass2JQ(Drawable drawable, Integer num, String str) {
        super(drawable, 0, str, true);
        this.A00 = num;
    }
}
