package X;

/* renamed from: X.59a  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1113459a implements AbstractC116245Ur {
    public final /* synthetic */ AnonymousClass241 A00;

    public C1113459a(AnonymousClass241 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC116245Ur
    public void AWl(AnonymousClass1KS r4, Integer num, int i) {
        AnonymousClass241 r2 = this.A00;
        r2.A0C.A0C(r4, false);
        AbstractC116245Ur r0 = r2.A01;
        if (r0 != null) {
            r0.AWl(r4, num, i);
        }
    }
}
