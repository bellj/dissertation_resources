package X;

/* renamed from: X.0rk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C18000rk implements AnonymousClass01N, AnonymousClass01H {
    public static final Object A02 = new Object();
    public volatile Object A00 = A02;
    public volatile AnonymousClass01N A01;

    public C18000rk(AnonymousClass01N r2) {
        this.A01 = r2;
    }

    public static AnonymousClass01H A00(AnonymousClass01N r1) {
        if (r1 instanceof AnonymousClass01H) {
            return (AnonymousClass01H) r1;
        }
        return new C18000rk(r1);
    }

    public static AnonymousClass01N A01(AnonymousClass01N r1) {
        if (r1 instanceof C18000rk) {
            return r1;
        }
        return new C18000rk(r1);
    }

    @Override // X.AnonymousClass01N, X.AnonymousClass01H
    public Object get() {
        Object obj;
        Object obj2 = this.A00;
        Object obj3 = A02;
        if (obj2 != obj3) {
            return obj2;
        }
        synchronized (this) {
            obj = this.A00;
            if (obj == obj3) {
                obj = this.A01.get();
                Object obj4 = this.A00;
                if (obj4 == obj3 || obj4 == obj) {
                    this.A00 = obj;
                    this.A01 = null;
                } else {
                    StringBuilder sb = new StringBuilder("Scoped provider was invoked recursively returning different results: ");
                    sb.append(obj4);
                    sb.append(" & ");
                    sb.append(obj);
                    sb.append(". This is likely due to a circular dependency.");
                    throw new IllegalStateException(sb.toString());
                }
            }
        }
        return obj;
    }
}
