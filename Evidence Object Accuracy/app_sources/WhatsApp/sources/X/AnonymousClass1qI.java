package X;

import android.content.Context;

/* renamed from: X.1qI  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1qI extends AnonymousClass1qE {
    public final Context A00;
    public final AbstractC116225Up A01;

    public AnonymousClass1qI(Context context, AnonymousClass1KS r10, AbstractC116225Up r11, String str, int i, int i2) {
        super(r10, str, i, i2, 2, true, false);
        this.A00 = context;
        this.A01 = r11;
    }
}
