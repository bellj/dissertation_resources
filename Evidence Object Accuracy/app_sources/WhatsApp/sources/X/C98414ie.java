package X;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

/* renamed from: X.4ie  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C98414ie implements IInterface {
    public final IBinder A00;
    public final String A01;

    public C98414ie(IBinder iBinder, String str) {
        this.A00 = iBinder;
        this.A01 = str;
    }

    @Override // android.os.IInterface
    public IBinder asBinder() {
        return this.A00;
    }

    public final Parcel A00(int i, Parcel parcel) {
        try {
            parcel = Parcel.obtain();
            C12990iw.A18(this.A00, parcel, parcel, i);
            return parcel;
        } catch (RuntimeException e) {
            throw e;
        } finally {
            parcel.recycle();
        }
    }

    public final void A01(int i, Parcel parcel) {
        Parcel obtain = Parcel.obtain();
        try {
            C12990iw.A18(this.A00, parcel, obtain, i);
        } finally {
            parcel.recycle();
            obtain.recycle();
        }
    }
}
