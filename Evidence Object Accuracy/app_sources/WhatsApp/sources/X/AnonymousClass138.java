package X;

/* renamed from: X.138  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass138 {
    public final C15520nO A00;

    public AnonymousClass138(C15520nO r1) {
        this.A00 = r1;
    }

    public synchronized boolean A00(String str, String str2) {
        boolean z;
        C15520nO r1 = this.A00;
        String string = r1.A01().getString(C15520nO.A00(str, "auth/token"), null);
        if (string == null) {
            z = false;
        } else {
            z = string.equals(str2);
        }
        return z;
    }

    public synchronized boolean A01(String str, String str2) {
        if (str2 != null) {
            C15520nO r1 = this.A00;
            String string = r1.A01().getString(C15520nO.A00(str, "request/token"), null);
            if (string != null) {
                return str2.equals(string);
            }
        }
        return false;
    }
}
