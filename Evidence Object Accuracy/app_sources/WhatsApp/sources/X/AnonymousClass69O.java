package X;

import android.content.Context;
import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import org.json.JSONObject;

/* renamed from: X.69O  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass69O implements AbstractC38181ne {
    public JSONObject A00;
    public final C15550nR A01;
    public final C15610nY A02;
    public final C14830m7 A03;
    public final C18600si A04;
    public final AnonymousClass14X A05;
    public final C22140ya A06;

    public AnonymousClass69O(C15550nR r1, C15610nY r2, C14830m7 r3, C18600si r4, AnonymousClass14X r5, C22140ya r6) {
        this.A03 = r3;
        this.A05 = r5;
        this.A01 = r1;
        this.A02 = r2;
        this.A04 = r4;
        this.A06 = r6;
    }

    @Override // X.AbstractC38181ne
    public List A6m(List list) {
        String str;
        Comparable valueOf;
        String A0X;
        Context context;
        int i;
        int i2;
        ArrayList A0l = C12960it.A0l();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            AnonymousClass1IR A09 = C117315Zl.A09(it);
            AbstractC30891Zf r0 = A09.A0A;
            if (r0 != null) {
                str = String.valueOf(r0.A07());
            } else {
                str = "EMPTY";
            }
            StringBuilder A0k = C12960it.A0k("PAY: BrazilPaymentStatusNotificationHelper/buildPaymentReminders - transaction transferred at: ");
            A0k.append(A09.A05);
            A0k.append(", expired at: ");
            Log.i(C12960it.A0d(str, A0k));
            AnonymousClass14X r7 = this.A05;
            Long A0E = r7.A0E(A09);
            if (A0E != null) {
                String str2 = A09.A0L;
                boolean z = false;
                long longValue = A0E.longValue();
                if (longValue > 0 && longValue <= 86400000) {
                    JSONObject jSONObject = this.A00;
                    if (jSONObject == null) {
                        try {
                            jSONObject = C13000ix.A05(this.A04.A01().getString("payments_nagged_transactions", ""));
                            this.A00 = jSONObject;
                        } catch (Exception unused) {
                            jSONObject = C117295Zj.A0a();
                            this.A00 = jSONObject;
                        }
                    }
                    if (!jSONObject.optBoolean(str2) && (i2 = Calendar.getInstance(TimeZone.getDefault()).get(11)) >= 9 && i2 < 21) {
                        z = true;
                    }
                }
                if (!z) {
                    Log.i(C12960it.A0d(A09.A0L, C12960it.A0k("PAY: BrazilPaymentStatusNotificationHelper/buildPaymentReminders - transaction skipped: ")));
                }
            }
            UserJid userJid = A09.A0E;
            if (userJid != null) {
                String A04 = this.A02.A04(this.A01.A0B(userJid));
                C30551Xw r3 = (C30551Xw) this.A06.A0A(A09.A0C, 39, this.A03.A00());
                Comparable[] comparableArr = new Comparable[3];
                comparableArr[0] = A09.A0E;
                comparableArr[1] = A09.A0I;
                C30821Yy r02 = A09.A08;
                String str3 = "";
                if (r02 == null) {
                    valueOf = str3;
                } else {
                    valueOf = Long.valueOf(r02.A00.scaleByPowerOfTen(3).longValue());
                }
                comparableArr[2] = valueOf;
                r3.A02 = TextUtils.join(";", Arrays.asList(comparableArr));
                ((C30541Xv) r3).A03 = AnonymousClass14X.A06(A09.A08, A09.A0I);
                C30821Yy r03 = A09.A08;
                if (r03 != null) {
                    str3 = String.valueOf(r03.A00.intValue());
                }
                r3.A01 = str3;
                long j = A09.A05;
                int A00 = C38121nY.A00(r7.A04.A00(), j);
                if (A00 == 0) {
                    A0X = r7.A06.A08(270);
                } else if (A00 == 1) {
                    A0X = r7.A06.A08(294);
                } else {
                    if (A00 < 7) {
                        Calendar instance = Calendar.getInstance();
                        instance.setTimeInMillis(j);
                        switch (instance.get(7)) {
                            case 1:
                                context = r7.A05.A00;
                                i = R.string.transaction_timestamp_format_sunday;
                                A0X = context.getString(i);
                                break;
                            case 2:
                                context = r7.A05.A00;
                                i = R.string.transaction_timestamp_format_monday;
                                A0X = context.getString(i);
                                break;
                            case 3:
                                context = r7.A05.A00;
                                i = R.string.transaction_timestamp_format_tuesday;
                                A0X = context.getString(i);
                                break;
                            case 4:
                                context = r7.A05.A00;
                                i = R.string.transaction_timestamp_format_wednesday;
                                A0X = context.getString(i);
                                break;
                            case 5:
                                context = r7.A05.A00;
                                i = R.string.transaction_timestamp_format_thursday;
                                A0X = context.getString(i);
                                break;
                            case 6:
                                context = r7.A05.A00;
                                i = R.string.transaction_timestamp_format_friday;
                                A0X = context.getString(i);
                                break;
                            case 7:
                                context = r7.A05.A00;
                                i = R.string.transaction_timestamp_format_saturday;
                                A0X = context.getString(i);
                                break;
                        }
                    }
                    A0X = C12960it.A0X(r7.A05.A00, AnonymousClass1MY.A00(r7.A06, j), new Object[1], 0, R.string.transaction_timestamp_format);
                }
                r3.A04 = A0X;
                r3.A03 = A04;
                AbstractC14640lm r2 = A09.A0C;
                boolean z2 = A09.A0Q;
                String str4 = A09.A0L;
                ((C30541Xv) r3).A02 = new AnonymousClass1IS(r2, str4, z2);
                if (A0E != null) {
                    r3.A00 = A0E.intValue();
                    JSONObject jSONObject2 = this.A00;
                    if (jSONObject2 == null) {
                        try {
                            jSONObject2 = C13000ix.A05(this.A04.A01().getString("payments_nagged_transactions", str3));
                            this.A00 = jSONObject2;
                        } catch (Exception unused2) {
                            jSONObject2 = C117295Zj.A0a();
                            this.A00 = jSONObject2;
                        }
                    }
                    try {
                        jSONObject2.put(str4, true);
                    } catch (Exception e) {
                        Log.e("BrazilPaymentStatusNotificationHelper/setNaggedTransaction/error", e);
                    }
                    C12970iu.A1D(C117295Zj.A05(this.A04), "payments_nagged_transactions", jSONObject2.toString());
                }
                A0l.add(r3);
            }
        }
        return A0l;
    }
}
