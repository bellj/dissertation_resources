package X;

import java.security.PrivilegedAction;
import java.security.Security;

/* renamed from: X.5Bp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C111985Bp implements PrivilegedAction {
    public final /* synthetic */ String A00;

    public C111985Bp(String str) {
        this.A00 = str;
    }

    @Override // java.security.PrivilegedAction
    public Object run() {
        return Security.getProperty(this.A00);
    }
}
