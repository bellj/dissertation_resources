package X;

import android.view.View;
import android.widget.PopupWindow;
import com.whatsapp.R;
import com.whatsapp.emoji.EmojiDescriptor;
import java.util.ArrayList;

/* renamed from: X.1oB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38491oB {
    public static C37471mS A00(String str) {
        int length = str.length();
        int[] iArr = new int[length];
        int i = 0;
        int i2 = 0;
        while (i < length) {
            int codePointAt = Character.codePointAt(str, i);
            i += Character.charCount(codePointAt);
            iArr[i2] = codePointAt;
            i2++;
        }
        int[] iArr2 = new int[i2];
        System.arraycopy(iArr, 0, iArr2, 0, i2);
        return new C37471mS(iArr2);
    }

    public static void A01(View view, View view2, PopupWindow popupWindow) {
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        int[] iArr2 = new int[2];
        view2.getRootView().getLocationOnScreen(iArr2);
        popupWindow.showAtLocation(view2, 51, ((iArr[0] - Math.max(0, iArr2[0])) + (view.getMeasuredWidth() / 2)) - (popupWindow.getContentView().getMeasuredWidth() / 2), ((iArr[1] - Math.max(0, iArr2[1])) - popupWindow.getContentView().getMeasuredHeight()) - view.getContext().getResources().getDimensionPixelSize(R.dimen.skin_emoji_popup_offset));
    }

    public static boolean A02(CharSequence charSequence) {
        C32681cY r7 = new C32681cY(charSequence);
        boolean z = false;
        long A00 = EmojiDescriptor.A00(r7, false);
        if (A00 != -1) {
            z = true;
        }
        if (!z) {
            return false;
        }
        int A01 = r7.A01(0, A00);
        int i = r7.A01;
        int[] iArr = new int[i];
        for (int i2 = 0; i2 < i; i2++) {
            iArr[i2] = Character.codePointAt(r7.A02, i2);
        }
        if (A01 == iArr.length) {
            return true;
        }
        return false;
    }

    public static boolean A03(String str, String[] strArr) {
        for (String str2 : strArr) {
            if (str.contains(str2)) {
                return true;
            }
        }
        return false;
    }

    public static int[] A04(int[] iArr) {
        int length = iArr.length;
        ArrayList arrayList = new ArrayList(length);
        for (int i : iArr) {
            if (i != 65039) {
                arrayList.add(Integer.valueOf(i));
            }
        }
        int size = arrayList.size();
        int[] iArr2 = new int[size];
        for (int i2 = 0; i2 < size; i2++) {
            iArr2[i2] = ((Number) arrayList.get(i2)).intValue();
        }
        return iArr2;
    }
}
