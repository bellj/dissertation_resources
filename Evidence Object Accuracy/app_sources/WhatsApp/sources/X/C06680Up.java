package X;

import android.animation.ValueAnimator;

/* renamed from: X.0Up  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06680Up implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ AnonymousClass0A2 A00;

    public C06680Up(AnonymousClass0A2 r1) {
        this.A00 = r1;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        long animatedFraction = (long) (valueAnimator.getAnimatedFraction() * ((float) valueAnimator.getDuration()));
        AnonymousClass0A2 r6 = this.A00;
        C04530Mb[] r10 = r6.A0E;
        int length = r10.length - 1;
        int i = 0;
        int max = Math.max(Math.min((int) (animatedFraction / 3000), length), 0);
        if (max != length) {
            i = max + 1;
        }
        r6.A04 = r10[max];
        r6.A05 = r10[i];
        r6.A00 = ((float) (animatedFraction - (((long) max) * 3000))) / 3000.0f;
        r6.invalidateSelf();
    }
}
