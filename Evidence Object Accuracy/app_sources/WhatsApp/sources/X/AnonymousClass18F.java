package X;

import java.util.Map;

/* renamed from: X.18F  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass18F implements AbstractC18150rz {
    public final /* synthetic */ C19960ux A00;

    public AnonymousClass18F(C19960ux r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass6MF
    public /* bridge */ /* synthetic */ AbstractC120015fT A8U(String str, String str2, String str3, Map map, long j) {
        AnonymousClass01J r1 = this.A00.A01;
        C18790t3 r2 = (C18790t3) r1.AJw.get();
        C14820m6 r3 = (C14820m6) r1.AN3.get();
        AnonymousClass01H A00 = C18000rk.A00(r1.AMu);
        AnonymousClass01N r12 = r1.A11;
        AnonymousClass01N r13 = r1.A12;
        return new AnonymousClass39Y(r2, r3, (C14850m9) r1.A04.get(), (C18160s0) r1.A14.get(), (AnonymousClass18L) r1.A89.get(), A00, str2, str3, str, map, r12, r13, j);
    }
}
