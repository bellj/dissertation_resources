package X;

import android.os.Bundle;

/* renamed from: X.5be  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118335be extends AnonymousClass0Yo {
    public final /* synthetic */ Bundle A00;
    public final /* synthetic */ C128375w0 A01;

    public C118335be(Bundle bundle, C128375w0 r2) {
        this.A01 = r2;
        this.A00 = bundle;
    }

    @Override // X.AnonymousClass0Yo, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.isAssignableFrom(C117975b4.class)) {
            return new C117975b4(this.A00);
        }
        throw C12970iu.A0f("Invalid viewModel for NoviCaptureVideoSelfieViewModel");
    }
}
