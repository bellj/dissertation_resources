package X;

/* renamed from: X.2FO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2FO {
    public final AbstractC15710nm A00;
    public final C16590pI A01;
    public final C15890o4 A02;
    public final C19390u2 A03;

    public AnonymousClass2FO(AbstractC15710nm r1, C16590pI r2, C15890o4 r3, C19390u2 r4) {
        this.A01 = r2;
        this.A00 = r1;
        this.A03 = r4;
        this.A02 = r3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00b1, code lost:
        if (r3 != 3) goto L_0x00d3;
     */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00b7  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00c7  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00dd  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00f6  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00fd  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AbstractC35581iK A00(X.AnonymousClass2JH r13) {
        /*
        // Method dump skipped, instructions count: 273
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2FO.A00(X.2JH):X.1iK");
    }
}
