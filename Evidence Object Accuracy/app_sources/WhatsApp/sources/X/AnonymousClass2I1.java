package X;

import android.content.pm.PackageManager;

/* renamed from: X.2I1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2I1 {
    public final PackageManager A00;
    public final AnonymousClass146 A01;
    public final C235512c A02;
    public final AnonymousClass147 A03;

    public AnonymousClass2I1(C16590pI r2, AnonymousClass146 r3, C235512c r4, AnonymousClass147 r5) {
        this.A00 = r2.A00.getPackageManager();
        this.A01 = r3;
        this.A02 = r4;
        this.A03 = r5;
    }
}
