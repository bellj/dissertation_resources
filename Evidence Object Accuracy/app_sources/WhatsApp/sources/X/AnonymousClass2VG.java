package X;

import android.util.Pair;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/* renamed from: X.2VG  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2VG extends AnonymousClass015 {
    public int A00 = -1;
    public final C16170oZ A01;
    public final C16370ot A02;
    public final AnonymousClass3D7 A03;
    public final AnonymousClass19O A04;
    public final C36161jQ A05;
    public final C27691It A06 = new C27691It();
    public final C27691It A07 = new C27691It();
    public final boolean A08;
    public final boolean A09;

    public AnonymousClass2VG(C16170oZ r10, C16370ot r11, AbstractC14640lm r12, C40481rf r13, AnonymousClass19O r14, boolean z) {
        String str;
        this.A01 = r10;
        this.A04 = r14;
        this.A02 = r11;
        Iterator A03 = r13.A03();
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        while (A03.hasNext()) {
            ArrayList arrayList3 = new ArrayList();
            ArrayList arrayList4 = new ArrayList();
            Iterator descendingIterator = ((C40491rg) A03.next()).A04.descendingIterator();
            while (descendingIterator.hasNext()) {
                AnonymousClass2VH A01 = AnonymousClass3J7.A01((AnonymousClass2VH) descendingIterator.next());
                if (A01.A03.A0F(A01.A04)) {
                    arrayList3.add(A01);
                } else {
                    arrayList4.add(A01);
                }
            }
            Pair pair = new Pair(arrayList3, arrayList4);
            arrayList.addAll((Collection) pair.first);
            arrayList2.addAll((Collection) pair.second);
        }
        arrayList.addAll(arrayList2);
        this.A03 = new AnonymousClass3D7(this, "", arrayList, 0);
        ArrayList arrayList5 = new ArrayList();
        Iterator A032 = r13.A03();
        int i = 1;
        while (A032.hasNext()) {
            ArrayList arrayList6 = new ArrayList();
            Iterator descendingIterator2 = ((C40491rg) A032.next()).A04.descendingIterator();
            while (descendingIterator2.hasNext()) {
                arrayList6.add(AnonymousClass3J7.A01((AnonymousClass2VH) descendingIterator2.next()));
            }
            if (arrayList6.isEmpty() || (str = ((AnonymousClass2VH) arrayList6.get(0)).A05) == null) {
                str = "";
            }
            arrayList5.add(new AnonymousClass3D7(this, str, arrayList6, i));
            i++;
        }
        this.A05 = new C36161jQ(arrayList5);
        this.A09 = r12 instanceof UserJid;
        this.A08 = z;
    }
}
