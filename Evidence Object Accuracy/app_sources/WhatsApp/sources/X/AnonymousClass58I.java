package X;

/* renamed from: X.58I  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass58I implements AnonymousClass5UW {
    public static final AnonymousClass58R A02 = new AnonymousClass58R();
    public final String A00;
    public final boolean A01;

    public AnonymousClass58I(String str, boolean z) {
        this.A00 = str;
        this.A01 = z;
    }

    @Override // X.AnonymousClass5UW
    public boolean A9g(AnonymousClass4V4 r4) {
        C16700pc.A0E(r4, 0);
        if (this.A01 != C12960it.A1W(r4.A00(this.A00))) {
            return false;
        }
        return true;
    }
}
