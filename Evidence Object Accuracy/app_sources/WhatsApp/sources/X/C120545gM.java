package X;

import android.content.Context;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.ArrayList;

/* renamed from: X.5gM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120545gM extends C126705tJ {
    public AnonymousClass6MP A00;
    public final Context A01;
    public final C14900mE A02;
    public final AnonymousClass102 A03;
    public final C17220qS A04;
    public final C1308460e A05;
    public final C1329668y A06;
    public final C18650sn A07;
    public final C17070qD A08;
    public final C118025b9 A09;

    public C120545gM(Context context, C14900mE r3, AnonymousClass102 r4, C17220qS r5, C1308460e r6, C1329668y r7, C18650sn r8, C18610sj r9, C17070qD r10, AnonymousClass6MP r11, C118025b9 r12) {
        super(r6.A04, r9);
        this.A01 = context;
        this.A02 = r3;
        this.A04 = r5;
        this.A08 = r10;
        this.A05 = r6;
        this.A03 = r4;
        this.A07 = r8;
        this.A06 = r7;
        this.A09 = r12;
        this.A00 = r11;
    }

    public static boolean A00(C119715ez r3, C1329668y r4, ArrayList arrayList, ArrayList arrayList2) {
        if (arrayList == null || arrayList.size() <= 0) {
            return false;
        }
        if (((C119755f3) arrayList.get(0)).A00 <= 1 || !TextUtils.isEmpty(r4.A07()) || (arrayList2 != null && arrayList2.size() > 0 && r3 != null)) {
            return true;
        }
        return false;
    }

    public void A01() {
        Log.i("PAY: IndiaUpiPaymentSetup sendGetBanksList called");
        C64513Fv r8 = super.A00;
        r8.A04("upi-get-banks");
        C17220qS r3 = this.A04;
        String A01 = r3.A01();
        C117295Zj.A1B(r3, new C120605gS(this.A01, this.A02, this.A07, r8, this), new C126515t0(new AnonymousClass3CS(A01)).A00, A01);
    }
}
