package X;

import android.graphics.Bitmap;

/* renamed from: X.3kX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75833kX extends AnonymousClass4UM {
    public final AnonymousClass4I1 A00;
    public final AnonymousClass5YZ A01;

    public C75833kX(AnonymousClass4I1 r1, AnonymousClass5YZ r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass4UM
    public C08870bz A00(Bitmap.Config config, int i, int i2) {
        int i3 = i * i2;
        AnonymousClass5YZ r3 = this.A01;
        Bitmap bitmap = (Bitmap) r3.get(C94594c9.A00(config) * i3);
        AnonymousClass0RA.A00(C12990iw.A1X(bitmap.getAllocationByteCount(), i3 * C94594c9.A00(config)));
        bitmap.reconfigure(i, i2, config);
        return new C08870bz(this.A00.A00, r3, bitmap);
    }
}
