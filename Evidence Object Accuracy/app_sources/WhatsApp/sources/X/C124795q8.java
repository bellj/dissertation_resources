package X;

/* renamed from: X.5q8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124795q8 extends Exception {
    public final int errorCode;
    public final String errorMessage;

    public C124795q8(EnumC124485pc r2) {
        super(r2.description);
        this.errorCode = r2.code;
        this.errorMessage = r2.description;
    }

    @Override // java.lang.Throwable, java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("Error ");
        A0k.append(this.errorCode);
        A0k.append(" : ");
        return C12960it.A0d(this.errorMessage, A0k);
    }
}
