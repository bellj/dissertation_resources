package X;

import android.graphics.Rect;
import android.hardware.Camera;
import android.util.Log;
import java.util.Collections;
import java.util.List;
import org.chromium.net.UrlRequest;

/* renamed from: X.5cq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C119075cq extends AbstractC130685zo implements Cloneable {
    public Rect A00;
    public Rect A01;
    public C129845yO A02;
    public C129845yO A03;
    public C129845yO A04;
    public C129845yO A05;
    public Boolean A06;
    public Boolean A07;
    public Boolean A08;
    public Boolean A09;
    public Boolean A0A;
    public Boolean A0B;
    public Double A0C;
    public Double A0D;
    public Double A0E;
    public Float A0F;
    public Float A0G;
    public Integer A0H;
    public Integer A0I;
    public Integer A0J;
    public Integer A0K;
    public Integer A0L;
    public Integer A0M;
    public Integer A0N;
    public Integer A0O;
    public Integer A0P;
    public Integer A0Q;
    public Integer A0R;
    public Integer A0S;
    public Integer A0T;
    public Integer A0U;
    public Integer A0V;
    public Integer A0W;
    public Long A0X;
    public String A0Y;
    public String A0Z;
    public List A0a;
    public List A0b;
    public final C119055co A0c;
    public final int[] A0d = new int[2];

    public C119075cq(Camera.Parameters parameters, C119055co r7) {
        List emptyList;
        int i;
        List emptyList2;
        Camera.Size size;
        C129845yO r0;
        Camera.Size size2;
        Boolean bool = Boolean.FALSE;
        this.A07 = bool;
        this.A08 = bool;
        Integer A0i = C12980iv.A0i();
        this.A0J = A0i;
        this.A0S = A0i;
        this.A0B = bool;
        Double valueOf = Double.valueOf(0.0d);
        this.A0C = valueOf;
        this.A0E = valueOf;
        this.A0D = valueOf;
        this.A0X = C117305Zk.A0f();
        this.A0P = A0i;
        this.A0M = A0i;
        this.A09 = bool;
        this.A0U = A0i;
        this.A06 = Boolean.TRUE;
        this.A0Z = parameters.flatten();
        this.A0c = r7;
        this.A0L = Integer.valueOf(AnonymousClass61R.A03(parameters.getFocusMode()));
        this.A0H = Integer.valueOf(AnonymousClass61R.A00(parameters.getAntibanding()));
        this.A0I = Integer.valueOf(AnonymousClass61R.A01(parameters.getColorEffect()));
        if (C117295Zj.A1S(AbstractC130695zp.A02, r7)) {
            this.A07 = Boolean.valueOf(parameters.getAutoExposureLock());
        }
        if (C117295Zj.A1S(AbstractC130695zp.A05, r7)) {
            this.A08 = Boolean.valueOf(parameters.getAutoWhiteBalanceLock());
        }
        this.A0K = Integer.valueOf(AnonymousClass61R.A02(parameters.getFlashMode()));
        if (C117295Zj.A1S(AbstractC130695zp.A0C, r7)) {
            this.A0J = Integer.valueOf(parameters.getExposureCompensation());
        }
        if (C117295Zj.A1S(AbstractC130695zp.A0O, r7)) {
            emptyList = C130365zI.A00(AnonymousClass61X.A03(parameters.get("focus-areas")));
        } else {
            emptyList = Collections.emptyList();
        }
        this.A0a = emptyList;
        this.A0F = Float.valueOf(parameters.getHorizontalViewAngle());
        this.A0G = Float.valueOf(parameters.getVerticalViewAngle());
        this.A0N = Integer.valueOf(parameters.getJpegQuality());
        try {
            i = parameters.getJpegThumbnailQuality();
        } catch (NumberFormatException e) {
            Log.e("ParametersHelper", C12960it.A0d(parameters.get("jpeg-thumbnail-quality"), C12960it.A0k("Invalid jpeg thumbnail quality parameter string=")), e);
            i = 85;
        }
        this.A0O = Integer.valueOf(i);
        try {
            Camera.Size jpegThumbnailSize = parameters.getJpegThumbnailSize();
            this.A02 = new C129845yO(jpegThumbnailSize.width, jpegThumbnailSize.height);
        } catch (NumberFormatException unused) {
        }
        if (C117295Zj.A1S(AbstractC130695zp.A0P, r7)) {
            emptyList2 = C130365zI.A00(AnonymousClass61X.A03(parameters.get("metering-areas")));
        } else {
            emptyList2 = Collections.emptyList();
        }
        this.A0b = emptyList2;
        this.A0Q = Integer.valueOf(parameters.getPictureFormat());
        C129845yO r3 = null;
        try {
            size = parameters.getPictureSize();
        } catch (NumberFormatException unused2) {
            size = null;
        }
        C125475rJ r4 = AbstractC130685zo.A0g;
        if (size != null) {
            r0 = new C129845yO(size.width, size.height);
        } else {
            r0 = null;
        }
        A05(r4, r0);
        this.A0R = Integer.valueOf(parameters.getPreviewFormat());
        parameters.getPreviewFpsRange(this.A0d);
        if (!r7.A0Y.isEmpty()) {
            this.A0S = Integer.valueOf(parameters.getPreviewFrameRate());
        }
        this.A0T = Integer.valueOf(AnonymousClass61R.A04(parameters.getSceneMode()));
        this.A0A = Boolean.valueOf(parameters.getVideoStabilization());
        this.A0V = Integer.valueOf(AnonymousClass61R.A05(parameters.getWhiteBalance()));
        this.A0W = Integer.valueOf(parameters.getZoom());
        try {
            size2 = parameters.getPreviewSize();
        } catch (NumberFormatException unused3) {
            size2 = null;
        }
        A05(AbstractC130685zo.A0m, size2 != null ? new C129845yO(size2.width, size2.height) : r3);
    }

    public String A04() {
        String str;
        StringBuilder sb = new StringBuilder(1000);
        sb.append("mFocusMode");
        sb.append('=');
        sb.append(this.A0L);
        sb.append(",mAntibanding");
        sb.append('=');
        sb.append(this.A0H);
        sb.append(",mColorEffect");
        sb.append('=');
        sb.append(this.A0I);
        sb.append(",mIsAutoExposureLock");
        sb.append('=');
        sb.append(this.A07);
        sb.append(",mIsAutoWhiteBalanceLock");
        sb.append('=');
        sb.append(this.A08);
        sb.append(",mFlashMode");
        sb.append('=');
        sb.append(this.A0K);
        sb.append(",mExposureCompensation");
        sb.append('=');
        sb.append(this.A0J);
        sb.append(",mFocusAreas");
        sb.append('=');
        sb.append(AnonymousClass61X.A01(this.A0a));
        sb.append(",mHorizontalViewAngle");
        sb.append('=');
        sb.append(this.A0F);
        sb.append(",mVerticalViewAngle");
        sb.append('=');
        sb.append(this.A0G);
        sb.append(",mJpegQuality");
        sb.append('=');
        sb.append(this.A0N);
        sb.append(",mJpegThumbnailQuality");
        sb.append('=');
        sb.append(this.A0O);
        sb.append(",mJpegThumbnailSize");
        sb.append('=');
        C129845yO r3 = this.A02;
        String str2 = "null";
        if (r3 != null) {
            C129845yO.A01(r3, sb);
        } else {
            sb.append(str2);
        }
        sb.append(",mMeteringAreas");
        sb.append('=');
        sb.append(AnonymousClass61X.A01(this.A0b));
        sb.append(",mPictureFormat");
        sb.append('=');
        sb.append(this.A0Q);
        sb.append(",mPictureSize");
        sb.append('=');
        C129845yO r0 = this.A03;
        if (r0 != null) {
            C129845yO.A01(r0, sb);
        } else {
            sb.append(str2);
        }
        sb.append(",mYuvPictureSize");
        sb.append('=');
        sb.append(str2);
        sb.append(",mPreviewFormat");
        sb.append('=');
        sb.append(this.A0R);
        sb.append(",mPreviewFpsRange");
        sb.append('=');
        int[] iArr = this.A0d;
        sb.append(iArr[0]);
        sb.append('-');
        sb.append(iArr[1]);
        sb.append(",mPreviewSize");
        sb.append('=');
        C129845yO r02 = this.A04;
        if (r02 != null) {
            C129845yO.A01(r02, sb);
        } else {
            sb.append(str2);
        }
        sb.append(",mIsoSensitivity");
        sb.append('=');
        sb.append(this.A0M);
        sb.append(",mSceneMode");
        sb.append('=');
        sb.append(this.A0T);
        sb.append(",mIsVideoStabilizationEnabled");
        sb.append('=');
        sb.append(this.A0A);
        sb.append(",mIsPreviewStabilizationEnabled");
        sb.append('=');
        sb.append(false);
        sb.append(",mVideoSize");
        sb.append('=');
        C129845yO r03 = this.A05;
        if (r03 != null) {
            C129845yO.A01(r03, sb);
        } else {
            sb.append(str2);
        }
        sb.append(",mWhiteBalance");
        sb.append('=');
        sb.append(this.A0V);
        sb.append(",mZoom");
        sb.append('=');
        sb.append(this.A0W);
        sb.append(",mPreviewRect");
        sb.append("=(");
        Rect rect = this.A01;
        if (rect != null) {
            str = rect.flattenToString();
        } else {
            str = str2;
        }
        sb.append(str);
        sb.append(')');
        sb.append(",mPictureRect");
        sb.append("=(");
        Rect rect2 = this.A00;
        if (rect2 != null) {
            str2 = rect2.flattenToString();
        }
        sb.append(str2);
        sb.append(')');
        sb.append(",mRecordingHint");
        sb.append('=');
        sb.append(this.A0B);
        sb.append(",mGpsAltitude");
        sb.append('=');
        sb.append(this.A0C);
        sb.append(",mGpsLongitude");
        sb.append('=');
        sb.append(this.A0E);
        sb.append(",mGpsLatitude");
        sb.append('=');
        sb.append(this.A0D);
        sb.append(",mGpsProcessingMethod");
        sb.append('=');
        sb.append(this.A0Y);
        sb.append(",mGpsTimestamp");
        sb.append('=');
        sb.append(this.A0X);
        sb.append(",mPhotoRotation");
        sb.append('=');
        sb.append(this.A0P);
        sb.append(",mVideoRotation");
        sb.append('=');
        sb.append(this.A0U);
        sb.append(",mIsoSensitivity");
        sb.append('=');
        sb.append(this.A0M);
        sb.append(",mSourceConfig");
        sb.append('=');
        return C12960it.A0d(this.A0Z, sb);
    }

    public void A05(C125475rJ r7, Object obj) {
        int i = r7.A00;
        if (i != 1) {
            int i2 = 0;
            if (i == 2) {
                if (C117295Zj.A1S(AbstractC130695zp.A0G, this.A0c)) {
                    boolean A1Y = C12970iu.A1Y(obj);
                    if (A1Y) {
                        i2 = 17;
                    }
                    A05(AbstractC130685zo.A0o, Integer.valueOf(i2));
                    if (A1Y) {
                        A05(AbstractC130685zo.A0n, Boolean.FALSE);
                    }
                }
            } else if (i == 3) {
                this.A0A = (Boolean) obj;
            } else if (i == 27) {
                this.A0X = (Long) obj;
            } else if (i == 42) {
                this.A0Y = (String) obj;
            } else if (i == 45) {
                this.A06 = (Boolean) obj;
            } else if (i == 52) {
            } else {
                if (i == 57) {
                    this.A09 = (Boolean) obj;
                } else if (i != 59) {
                    switch (i) {
                        case 8:
                            this.A0B = (Boolean) obj;
                            return;
                        case 9:
                            this.A0L = (Integer) obj;
                            return;
                        case 10:
                            this.A0K = (Integer) obj;
                            return;
                        case 11:
                            this.A0H = (Integer) obj;
                            return;
                        case 12:
                            this.A0I = (Integer) obj;
                            return;
                        case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                            this.A0J = (Integer) obj;
                            return;
                        case UrlRequest.Status.READING_RESPONSE /* 14 */:
                            this.A0N = (Integer) obj;
                            return;
                        case 15:
                            this.A0O = (Integer) obj;
                            return;
                        default:
                            switch (i) {
                                case 17:
                                    this.A0Q = (Integer) obj;
                                    return;
                                case 18:
                                    this.A0R = (Integer) obj;
                                    return;
                                case 19:
                                    this.A0S = (Integer) obj;
                                    return;
                                default:
                                    switch (i) {
                                        case 21:
                                            this.A0P = (Integer) obj;
                                            return;
                                        case 22:
                                            this.A0M = (Integer) obj;
                                            return;
                                        case 23:
                                            this.A0T = (Integer) obj;
                                            return;
                                        case 24:
                                            this.A0V = (Integer) obj;
                                            return;
                                        case 25:
                                            this.A0W = (Integer) obj;
                                            return;
                                        default:
                                            Rect rect = null;
                                            switch (i) {
                                                case C25991Bp.A0S:
                                                    this.A0C = (Double) obj;
                                                    return;
                                                case 31:
                                                    this.A0E = (Double) obj;
                                                    return;
                                                case 32:
                                                    this.A0D = (Double) obj;
                                                    return;
                                                case 33:
                                                    C129845yO r8 = (C129845yO) obj;
                                                    this.A04 = r8;
                                                    if (r8 != null) {
                                                        rect = new Rect(0, 0, r8.A02, r8.A01);
                                                    }
                                                    this.A01 = rect;
                                                    return;
                                                case 34:
                                                    C129845yO r82 = (C129845yO) obj;
                                                    this.A03 = r82;
                                                    if (r82 != null) {
                                                        rect = new Rect(0, 0, r82.A02, r82.A01);
                                                    }
                                                    this.A00 = rect;
                                                    return;
                                                case 35:
                                                    this.A05 = (C129845yO) obj;
                                                    return;
                                                case 36:
                                                    this.A02 = (C129845yO) obj;
                                                    return;
                                                case 37:
                                                    this.A0a = C130365zI.A00((List) obj);
                                                    return;
                                                case 38:
                                                    this.A0b = C130365zI.A00((List) obj);
                                                    return;
                                                case 39:
                                                    int[] iArr = (int[]) obj;
                                                    if (iArr != null && iArr.length == 2) {
                                                        int[] iArr2 = this.A0d;
                                                        iArr2[0] = iArr[0];
                                                        iArr2[1] = iArr[1];
                                                        return;
                                                    }
                                                    return;
                                                default:
                                                    throw C12990iw.A0m(C12960it.A0W(i, "Cannot directly set: "));
                                            }
                                    }
                            }
                    }
                } else {
                    this.A0U = (Integer) obj;
                }
            }
        } else {
            this.A08 = (Boolean) obj;
        }
    }

    @Override // java.lang.Object
    public Object clone() {
        return super.clone();
    }
}
