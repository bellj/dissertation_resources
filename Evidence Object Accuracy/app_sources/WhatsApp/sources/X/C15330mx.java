package X;

import android.app.Activity;
import com.facebook.redex.RunnableBRunnable0Shape6S0100000_I0_6;
import com.whatsapp.emoji.search.EmojiSearchContainer;
import com.whatsapp.gifsearch.GifSearchContainer;

/* renamed from: X.0mx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15330mx {
    public AbstractC14020ki A00;
    public final C15270mq A01;
    public final EmojiSearchContainer A02;

    public C15330mx(Activity activity, AnonymousClass018 r11, AnonymousClass19M r12, C15270mq r13, C231510o r14, EmojiSearchContainer emojiSearchContainer, C16630pM r16) {
        this.A02 = emojiSearchContainer;
        emojiSearchContainer.setVisibility(8);
        this.A01 = r13;
        r13.A0B = new C91894Tq(activity, r11, r12, r13, r14, emojiSearchContainer, this, r16);
    }

    public void A00(boolean z) {
        int i;
        if (!(this instanceof C15320mw)) {
            EmojiSearchContainer emojiSearchContainer = this.A02;
            if (emojiSearchContainer.getVisibility() == 0) {
                emojiSearchContainer.setVisibility(8);
                emojiSearchContainer.A04.A03();
                emojiSearchContainer.A09 = null;
            }
            this.A01.A0C = null;
            return;
        }
        C15320mw r3 = (C15320mw) this;
        GifSearchContainer gifSearchContainer = r3.A02;
        if (gifSearchContainer.getVisibility() == 0) {
            gifSearchContainer.A00();
        } else {
            EmojiSearchContainer emojiSearchContainer2 = ((C15330mx) r3).A02;
            if (emojiSearchContainer2.getVisibility() == 0) {
                emojiSearchContainer2.setVisibility(8);
                emojiSearchContainer2.A04.A03();
                emojiSearchContainer2.A09 = null;
            }
        }
        ((C15330mx) r3).A01.A0C = null;
        if (z) {
            C16120oU r2 = r3.A01;
            AbstractC253919f r0 = r3.A03;
            AnonymousClass434 r1 = new AnonymousClass434();
            if (!(r0 instanceof C254019g)) {
                i = 0;
            } else {
                i = 1;
            }
            r1.A00 = Integer.valueOf(i);
            r2.A07(r1);
        }
    }

    public boolean A01() {
        EmojiSearchContainer emojiSearchContainer;
        if (!(this instanceof C15320mw)) {
            emojiSearchContainer = this.A02;
        } else {
            C15320mw r1 = (C15320mw) this;
            if (r1.A02.getVisibility() != 0) {
                emojiSearchContainer = ((C15330mx) r1).A02;
            }
        }
        return emojiSearchContainer.getVisibility() == 0;
    }

    public boolean A02() {
        if (!A01()) {
            return false;
        }
        A00(true);
        this.A02.post(new RunnableBRunnable0Shape6S0100000_I0_6(this, 22));
        return true;
    }
}
