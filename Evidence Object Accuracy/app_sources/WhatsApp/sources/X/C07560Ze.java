package X;

import android.app.Activity;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build;
import android.util.Log;
import android.view.Display;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* renamed from: X.0Ze  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C07560Ze implements AbstractC12770iT {
    public static final C07560Ze A00 = new C07560Ze();

    public static final Point A00(Display display) {
        Point point = new Point();
        if (Build.VERSION.SDK_INT >= 17) {
            AnonymousClass0LK.A00(point, display);
            return point;
        }
        try {
            Method declaredMethod = Display.class.getDeclaredMethod("getRealSize", Point.class);
            declaredMethod.setAccessible(true);
            declaredMethod.invoke(display, point);
            return point;
        } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            Log.w("WindowMetricsCalculatorCompat", e);
            return point;
        }
    }

    public final Rect A01(Activity activity) {
        int i;
        Display defaultDisplay = activity.getWindowManager().getDefaultDisplay();
        C16700pc.A0B(defaultDisplay);
        Point A002 = A00(defaultDisplay);
        Rect rect = new Rect();
        int i2 = A002.x;
        if (i2 == 0 || (i = A002.y) == 0) {
            defaultDisplay.getRectSize(rect);
            return rect;
        }
        rect.right = i2;
        rect.bottom = i;
        return rect;
    }

    public final Rect A02(Activity activity) {
        int i;
        Rect rect = new Rect();
        Display defaultDisplay = activity.getWindowManager().getDefaultDisplay();
        defaultDisplay.getRectSize(rect);
        if (!AnonymousClass0LI.A00(activity)) {
            Point A002 = A00(defaultDisplay);
            Resources resources = activity.getResources();
            int identifier = resources.getIdentifier("navigation_bar_height", "dimen", "android");
            if (identifier > 0) {
                i = resources.getDimensionPixelSize(identifier);
            } else {
                i = 0;
            }
            int i2 = rect.bottom + i;
            if (i2 == A002.y) {
                rect.bottom = i2;
            } else {
                int i3 = rect.right + i;
                if (i3 == A002.x) {
                    rect.right = i3;
                    return rect;
                }
            }
        }
        return rect;
    }

    /* JADX WARNING: Removed duplicated region for block: B:46:0x0126  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.graphics.Rect A03(android.app.Activity r11) {
        /*
        // Method dump skipped, instructions count: 355
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C07560Ze.A03(android.app.Activity):android.graphics.Rect");
    }

    public final Rect A04(Activity activity) {
        Configuration configuration = activity.getResources().getConfiguration();
        try {
            Field declaredField = Configuration.class.getDeclaredField("windowConfiguration");
            declaredField.setAccessible(true);
            Object obj = declaredField.get(configuration);
            Object invoke = obj.getClass().getDeclaredMethod("getBounds", new Class[0]).invoke(obj, new Object[0]);
            if (invoke != null) {
                return new Rect((Rect) invoke);
            }
            throw new NullPointerException("null cannot be cast to non-null type android.graphics.Rect");
        } catch (IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException e) {
            Log.w("WindowMetricsCalculatorCompat", e);
            return A03(activity);
        }
    }

    public C05300Pa A05(Activity activity) {
        Rect A01;
        int i = Build.VERSION.SDK_INT;
        if (i >= 30) {
            A01 = AnonymousClass0LJ.A00(activity);
        } else if (i >= 29) {
            A01 = A04(activity);
        } else if (i >= 28) {
            A01 = A03(activity);
        } else if (i >= 24) {
            A01 = A02(activity);
        } else {
            A01 = A01(activity);
        }
        return new C05300Pa(A01);
    }
}
