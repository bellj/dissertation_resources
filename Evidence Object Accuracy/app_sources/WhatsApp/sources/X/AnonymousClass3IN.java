package X;

import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.3IN  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3IN {
    public final AnonymousClass017 A00;
    public final AnonymousClass28G A01;
    public final AnonymousClass4KA A02;
    public final AbstractC16710pd A03;

    public AnonymousClass3IN(AnonymousClass28G r2, AnonymousClass4KA r3) {
        this.A01 = r2;
        this.A02 = r3;
        AbstractC16710pd A00 = AnonymousClass4Yq.A00(new AnonymousClass5J2());
        this.A03 = A00;
        this.A00 = C12990iw.A0P(A00);
    }

    public static final AnonymousClass4KB A00(AnonymousClass4A1 r0, AnonymousClass4KB r1, AnonymousClass4KB r2) {
        switch (r0.ordinal()) {
            case 0:
                return r1;
            case 1:
                return r2;
            default:
                throw C12990iw.A0v();
        }
    }

    public final void A01(AnonymousClass4A1 r7, UserJid userJid, String str) {
        AnonymousClass28K r3 = new AnonymousClass28K(userJid, str);
        C72173e8 r4 = new C72173e8(r7, this);
        ArrayList A0l = C12960it.A0l();
        int i = 0;
        do {
            i++;
            A0l.add(AnonymousClass41A.A00);
        } while (i < 3);
        C12990iw.A0P(this.A03).A0B(A00(r7, new C60282wa(A0l), new AnonymousClass2wZ(A0l)));
        switch (r7.ordinal()) {
            case 0:
                AnonymousClass28G r2 = this.A01;
                AnonymousClass28I r1 = r2.A02;
                r1.A00 = null;
                ((AtomicReference) r1.A01.getValue()).set(r3);
                r2.A00(r3, r4);
                return;
            case 1:
                this.A01.A00(r3, r4);
                return;
            default:
                return;
        }
    }
}
