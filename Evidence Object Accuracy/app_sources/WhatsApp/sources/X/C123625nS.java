package X;

/* renamed from: X.5nS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123625nS extends AbstractC130145ys {
    public final int A00;
    public final int A01;
    public final C126765tP A02;

    public C123625nS(C16590pI r10, AnonymousClass018 r11, AnonymousClass102 r12, AnonymousClass1IR r13, C126765tP r14, C130205yy r15, C129835yN r16, C123765np r17, int i, int i2) {
        super(r10, r11, r12, r13, r15, r16, r17);
        this.A00 = i;
        this.A01 = i2;
        this.A02 = r14;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002f, code lost:
        if (r1 != 12) goto L_0x0031;
     */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x002d  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0044  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00f2  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0138  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x015c  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0162  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x016d  */
    @Override // X.AbstractC130145ys
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List A01() {
        /*
        // Method dump skipped, instructions count: 520
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C123625nS.A01():java.util.List");
    }
}
