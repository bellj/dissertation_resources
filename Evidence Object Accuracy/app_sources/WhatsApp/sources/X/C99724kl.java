package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4kl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C99724kl implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new C30171Wj(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C30171Wj[i];
    }
}
