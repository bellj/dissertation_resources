package X;

import java.math.BigDecimal;
import org.json.JSONObject;

/* renamed from: X.5zE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130325zE {
    public static long A00(AbstractC30791Yv r0, C30821Yy r1) {
        return r1.A00.movePointRight(C117315Zl.A00((AbstractC30781Yu) r0)).longValue();
    }

    public static AbstractC30791Yv A01(AnonymousClass6F2 r2, String str, JSONObject jSONObject) {
        C30821Yy r0 = r2.A01;
        AbstractC30791Yv r22 = r2.A00;
        jSONObject.put(str, A00(r22, r0));
        return r22;
    }

    public static C1310460z A02(AnonymousClass6F2 r5, String str) {
        C1310460z r2 = new C1310460z(str);
        AnonymousClass61S[] r3 = new AnonymousClass61S[3];
        C30821Yy r0 = r5.A01;
        AbstractC30791Yv r6 = r5.A00;
        r3[0] = new AnonymousClass61S("value", A00(r6, r0));
        AbstractC30781Yu r62 = (AbstractC30781Yu) r6;
        r3[1] = AnonymousClass61S.A00("offset", BigDecimal.ONE.movePointRight(C117315Zl.A00(r62)).toString());
        C117305Zk.A1K(r2, "money", C12960it.A0m(AnonymousClass61S.A00("currency", r62.A04), r3, 2));
        return r2;
    }
}
