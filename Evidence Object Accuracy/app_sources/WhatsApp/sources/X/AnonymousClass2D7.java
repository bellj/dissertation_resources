package X;

import com.whatsapp.jobqueue.job.DeleteAccountFromHsmServerJob;
import com.whatsapp.util.Log;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.2D7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2D7 implements AnonymousClass2D8 {
    public final /* synthetic */ DeleteAccountFromHsmServerJob A00;
    public final /* synthetic */ AtomicInteger A01;

    public AnonymousClass2D7(DeleteAccountFromHsmServerJob deleteAccountFromHsmServerJob, AtomicInteger atomicInteger) {
        this.A00 = deleteAccountFromHsmServerJob;
        this.A01 = atomicInteger;
    }

    @Override // X.AnonymousClass1OG
    public void APr(String str, int i) {
        StringBuilder sb = new StringBuilder("DeleteAccountFromHsmServerJob/job unsuccessful with error code: ");
        sb.append(i);
        Log.e(sb.toString());
        this.A01.set(i);
    }

    @Override // X.AnonymousClass2D8
    public void AWu() {
        Log.i("DeleteAccountFromHsmServerJob/job successful");
    }
}
