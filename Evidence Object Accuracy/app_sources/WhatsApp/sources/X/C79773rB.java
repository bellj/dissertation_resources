package X;

import android.os.IBinder;

/* renamed from: X.3rB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79773rB extends C65873Li implements AnonymousClass5YB {
    public C79773rB(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.model.internal.IBitmapDescriptorFactoryDelegate");
    }
}
