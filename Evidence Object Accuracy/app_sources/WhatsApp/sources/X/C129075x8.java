package X;

import android.content.Context;
import com.whatsapp.jid.UserJid;
import com.whatsapp.wabloks.ui.WaBloksActivity;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5x8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129075x8 {
    public C15550nR A00;
    public AnonymousClass018 A01;
    public AnonymousClass1IR A02;
    public AnonymousClass14X A03;
    public String A04;

    public void A00(Context context) {
        C15550nR r0;
        try {
            AnonymousClass009.A05(this.A04);
            AnonymousClass009.A05(this.A01);
            JSONObject A0a = C117295Zj.A0a();
            A0a.put("ref", this.A04);
            A0a.put("locale", this.A01.A07());
            AnonymousClass1IR r02 = this.A02;
            if (r02 != null) {
                A0a.put("transaction_id", r02.A0K);
                AnonymousClass1IR r03 = this.A02;
                C30821Yy r5 = r03.A08;
                if (r5 != null) {
                    A0a.put("transaction_amount", AnonymousClass14X.A05(this.A01, r03.A00(), r5, 0, true));
                }
                AnonymousClass14X r1 = this.A03;
                if (r1 != null) {
                    A0a.put("transaction_status", r1.A0J(this.A02));
                }
                AnonymousClass1IR r04 = this.A02;
                A0a.put("transaction_status_enum", C31001Zq.A05(r04.A03, r04.A02));
                Boolean A02 = this.A02.A02();
                if (A02 != null) {
                    A0a.put("is_transaction_sender", A02);
                }
                UserJid userJid = this.A02.A0D;
                if (!(userJid == null || (r0 = this.A00) == null)) {
                    A0a.put("receiver_name", r0.A0B(userJid).A0D());
                }
            }
            context.startActivity(WaBloksActivity.A09(context, "com.bloks.www.payments.whatsapp.f2care", C117295Zj.A0a().put("params", C117295Zj.A0a().put("server_params", A0a)).toString()));
        } catch (JSONException unused) {
        }
    }
}
