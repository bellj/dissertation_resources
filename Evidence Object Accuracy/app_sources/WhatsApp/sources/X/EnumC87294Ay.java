package X;

/* renamed from: X.4Ay  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public enum EnumC87294Ay {
    A01(0),
    A02(1),
    A03(2);
    
    public final int mIntValue;

    EnumC87294Ay(int i) {
        this.mIntValue = i;
    }
}
