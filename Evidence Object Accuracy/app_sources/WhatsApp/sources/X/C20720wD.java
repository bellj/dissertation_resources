package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0201000_I0;
import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.0wD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20720wD implements AbstractC18870tC {
    public final C20670w8 A00;
    public final C15550nR A01;
    public final AnonymousClass1E7 A02;
    public final C14830m7 A03;
    public final C14820m6 A04;
    public final C15680nj A05;
    public final C14850m9 A06;
    public final C14840m8 A07;
    public final AbstractC14440lR A08;

    @Override // X.AbstractC18870tC
    public /* synthetic */ void AR9() {
    }

    @Override // X.AbstractC18870tC
    public /* synthetic */ void ARA() {
    }

    @Override // X.AbstractC18870tC
    public /* synthetic */ void ARB() {
    }

    public C20720wD(C20670w8 r1, C15550nR r2, AnonymousClass1E7 r3, C14830m7 r4, C14820m6 r5, C15680nj r6, C14850m9 r7, C14840m8 r8, AbstractC14440lR r9) {
        this.A03 = r4;
        this.A06 = r7;
        this.A08 = r9;
        this.A00 = r1;
        this.A01 = r2;
        this.A07 = r8;
        this.A04 = r5;
        this.A05 = r6;
        this.A02 = r3;
    }

    public final Set A00() {
        HashSet hashSet = new HashSet();
        Set<String> stringSet = this.A04.A00.getStringSet("pending_users_to_sync_device", new HashSet());
        AnonymousClass009.A05(stringSet);
        hashSet.addAll(C15380n4.A08((String[]) stringSet.toArray(new String[0])));
        return hashSet;
    }

    public void A01(UserJid[] userJidArr, int i) {
        boolean z;
        String[] A0Q = C15380n4.A0Q(Arrays.asList(userJidArr));
        if (A0Q == null || A0Q.length == 0) {
            throw new IllegalArgumentException("invalid jid list");
        }
        ArrayList arrayList = new ArrayList();
        for (UserJid userJid : userJidArr) {
            AnonymousClass1E7 r1 = this.A02;
            Set set = r1.A03;
            synchronized (set) {
                if (!set.contains(userJid)) {
                    r1.A01.put(userJid, Long.valueOf(r1.A00.A00()));
                    set.add(userJid);
                    z = true;
                } else {
                    z = false;
                }
            }
            if (z) {
                arrayList.add(userJid);
            }
        }
        if (!arrayList.isEmpty()) {
            new RunnableBRunnable0Shape0S0201000_I0(this, arrayList, i, 9).run();
        }
    }

    @Override // X.AbstractC18870tC
    public void ARC() {
        if (this.A06.A07(560)) {
            this.A08.Ab2(new RunnableBRunnable0Shape4S0100000_I0_4(this, 38));
        }
    }
}
