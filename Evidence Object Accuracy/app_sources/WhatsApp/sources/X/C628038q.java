package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.38q  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C628038q extends AbstractC16350or {
    public final C253318z A00;
    public final AbstractC116675Wj A01;
    public final UserJid A02;
    public final C17220qS A03;

    public C628038q(C253318z r1, AbstractC116675Wj r2, UserJid userJid, C17220qS r4) {
        this.A03 = r4;
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = userJid;
    }
}
