package X;

/* renamed from: X.4WS  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4WS {
    public final int A00;

    public AnonymousClass4WS(int i) {
        this.A00 = i;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass() && this.A00 == ((AnonymousClass4WS) obj).A00) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return this.A00;
    }
}
