package X;

/* renamed from: X.1u3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C41751u3 extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Integer A02;
    public Integer A03;
    public Integer A04;
    public Integer A05;
    public Integer A06;
    public Long A07;
    public Long A08;
    public Long A09;

    public C41751u3() {
        super(1980, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(9, this.A07);
        r3.Abe(8, this.A00);
        r3.Abe(6, this.A01);
        r3.Abe(5, this.A02);
        r3.Abe(10, this.A08);
        r3.Abe(2, this.A03);
        r3.Abe(11, this.A04);
        r3.Abe(3, this.A05);
        r3.Abe(4, this.A09);
        r3.Abe(1, this.A06);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        String obj3;
        String obj4;
        String obj5;
        String obj6;
        String obj7;
        StringBuilder sb = new StringBuilder("WamPlaceholderActivity {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "deviceCount", this.A07);
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "deviceSizeBucket", obj);
        Integer num2 = this.A01;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "messageMediaType", obj2);
        Integer num3 = this.A02;
        if (num3 == null) {
            obj3 = null;
        } else {
            obj3 = num3.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "messageType", obj3);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "participantCount", this.A08);
        Integer num4 = this.A03;
        if (num4 == null) {
            obj4 = null;
        } else {
            obj4 = num4.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "placeholderActionInd", obj4);
        Integer num5 = this.A04;
        if (num5 == null) {
            obj5 = null;
        } else {
            obj5 = num5.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "placeholderAddReason", obj5);
        Integer num6 = this.A05;
        if (num6 == null) {
            obj6 = null;
        } else {
            obj6 = num6.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "placeholderChatTypeInd", obj6);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "placeholderTimePeriod", this.A09);
        Integer num7 = this.A06;
        if (num7 == null) {
            obj7 = null;
        } else {
            obj7 = num7.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "placeholderTypeInd", obj7);
        sb.append("}");
        return sb.toString();
    }
}
