package X;

import java.util.ArrayList;
import java.util.HashSet;

/* renamed from: X.0QV  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0QV {
    public float A00 = 0.0f;
    public float A01;
    public float A02;
    public float A03 = 1.0f;
    public float A04 = 1.0f;
    public float A05 = 1.0f;
    public float A06;
    public int A07;
    public int A08;
    public int A09;
    public int A0A;
    public int A0B = -1;
    public int A0C = 0;
    public int A0D = 0;
    public int A0E = 0;
    public int A0F = 0;
    public int A0G = 0;
    public int A0H = 0;
    public int A0I;
    public int A0J;
    public int A0K = -1;
    public int A0L;
    public int A0M = -1;
    public int A0N;
    public int A0O;
    public int A0P;
    public int A0Q;
    public AnonymousClass0Q7 A0R;
    public AnonymousClass0Q7 A0S;
    public AnonymousClass0Q7 A0T;
    public AnonymousClass0Q7 A0U;
    public AnonymousClass0Q7 A0V;
    public AnonymousClass0Q7 A0W;
    public AnonymousClass0Q7 A0X;
    public AnonymousClass0Q7 A0Y;
    public AnonymousClass0QV A0Z;
    public AnonymousClass0D1 A0a;
    public AnonymousClass0D1 A0b;
    public AnonymousClass0D3 A0c = new AnonymousClass0D3(this);
    public AnonymousClass0D2 A0d = new AnonymousClass0D2(this);
    public Object A0e;
    public String A0f;
    public ArrayList A0g;
    public boolean A0h = false;
    public boolean A0i = false;
    public float[] A0j;
    public int[] A0k = {Integer.MAX_VALUE, Integer.MAX_VALUE};
    public int[] A0l = new int[2];
    public int[] A0m = {0, 0, 0, 0};
    public AnonymousClass0Q7[] A0n;
    public AnonymousClass0JR[] A0o;
    public AnonymousClass0QV[] A0p;
    public AnonymousClass0QV[] A0q;
    public boolean[] A0r = {true, true};
    public boolean[] A0s;

    public AnonymousClass0QV() {
        AnonymousClass0Q7 r2 = new AnonymousClass0Q7(AnonymousClass0JZ.LEFT, this);
        this.A0W = r2;
        AnonymousClass0Q7 r12 = new AnonymousClass0Q7(AnonymousClass0JZ.TOP, this);
        this.A0Y = r12;
        AnonymousClass0Q7 r10 = new AnonymousClass0Q7(AnonymousClass0JZ.RIGHT, this);
        this.A0X = r10;
        AnonymousClass0Q7 r9 = new AnonymousClass0Q7(AnonymousClass0JZ.BOTTOM, this);
        this.A0S = r9;
        AnonymousClass0Q7 r8 = new AnonymousClass0Q7(AnonymousClass0JZ.BASELINE, this);
        this.A0R = r8;
        this.A0U = new AnonymousClass0Q7(AnonymousClass0JZ.CENTER_X, this);
        this.A0V = new AnonymousClass0Q7(AnonymousClass0JZ.CENTER_Y, this);
        AnonymousClass0Q7 r5 = new AnonymousClass0Q7(AnonymousClass0JZ.CENTER, this);
        this.A0T = r5;
        this.A0n = new AnonymousClass0Q7[]{r2, r10, r12, r9, r8, r5};
        this.A0g = new ArrayList();
        this.A0s = new boolean[2];
        AnonymousClass0JR r0 = AnonymousClass0JR.FIXED;
        this.A0o = new AnonymousClass0JR[]{r0, r0};
        this.A0Z = null;
        this.A0O = 0;
        this.A09 = 0;
        this.A01 = 0.0f;
        this.A08 = -1;
        this.A0P = 0;
        this.A0Q = 0;
        this.A07 = 0;
        this.A02 = 0.5f;
        this.A06 = 0.5f;
        this.A0N = 0;
        this.A0f = null;
        this.A0A = 0;
        this.A0L = 0;
        this.A0j = new float[]{-1.0f, -1.0f};
        this.A0p = new AnonymousClass0QV[]{null, null};
        this.A0q = new AnonymousClass0QV[]{null, null};
        ArrayList arrayList = this.A0g;
        arrayList.add(this.A0W);
        arrayList.add(this.A0Y);
        arrayList.add(this.A0X);
        arrayList.add(this.A0S);
        arrayList.add(this.A0U);
        arrayList.add(this.A0V);
        arrayList.add(this.A0T);
        arrayList.add(this.A0R);
    }

    public int A00() {
        if (this.A0N == 8) {
            return 0;
        }
        return this.A09;
    }

    public int A01() {
        if (this.A0N == 8) {
            return 0;
        }
        return this.A0O;
    }

    public int A02() {
        AnonymousClass0QV r1 = this.A0Z;
        if (r1 == null || !(r1 instanceof C02560Cw)) {
            return this.A0P;
        }
        return ((C02560Cw) r1).A02 + this.A0P;
    }

    public int A03() {
        AnonymousClass0QV r1 = this.A0Z;
        if (r1 == null || !(r1 instanceof C02560Cw)) {
            return this.A0Q;
        }
        return ((C02560Cw) r1).A03 + this.A0Q;
    }

    public AnonymousClass0Q7 A04(AnonymousClass0JZ r3) {
        switch (r3.ordinal()) {
            case 1:
                return this.A0W;
            case 2:
                return this.A0Y;
            case 3:
                return this.A0X;
            case 4:
                return this.A0S;
            case 5:
                return this.A0R;
            case 6:
                return this.A0T;
            case 7:
                return this.A0U;
            case 8:
                return this.A0V;
            default:
                throw new AssertionError(r3.name());
        }
    }

    public void A05(int i) {
        this.A09 = i;
        int i2 = this.A0I;
        if (i < i2) {
            this.A09 = i2;
        }
    }

    public void A06(int i) {
        this.A0O = i;
        int i2 = this.A0J;
        if (i < i2) {
            this.A0O = i2;
        }
    }

    public void A07(AnonymousClass0NX r2) {
        this.A0W.A02();
        this.A0Y.A02();
        this.A0X.A02();
        this.A0S.A02();
        this.A0R.A02();
        this.A0T.A02();
        this.A0U.A02();
        this.A0V.A02();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:107:0x01ed, code lost:
        if (r2.A03 != null) goto L_0x01ef;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:214:0x04b8, code lost:
        if (r8 == -1) goto L_0x04ba;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:218:0x04c0, code lost:
        if (r81.A0G == 0) goto L_0x04c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:222:0x04cb, code lost:
        if (r81.A0G > 0) goto L_0x0503;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:228:0x04de, code lost:
        if (r12.A03 != null) goto L_0x01ef;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:236:0x04f6, code lost:
        if (r12.A03 != null) goto L_0x04ba;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:240:0x0501, code lost:
        if (r12.A03 != null) goto L_0x0503;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:241:0x0503, code lost:
        r81.A05 = 1.0f / r5;
     */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x01e9  */
    /* JADX WARNING: Removed duplicated region for block: B:166:0x02c1  */
    /* JADX WARNING: Removed duplicated region for block: B:169:0x02cb  */
    /* JADX WARNING: Removed duplicated region for block: B:176:0x02e6  */
    /* JADX WARNING: Removed duplicated region for block: B:182:0x033c  */
    /* JADX WARNING: Removed duplicated region for block: B:188:0x036e  */
    /* JADX WARNING: Removed duplicated region for block: B:191:0x0432  */
    /* JADX WARNING: Removed duplicated region for block: B:194:0x043d  */
    /* JADX WARNING: Removed duplicated region for block: B:217:0x04be  */
    /* JADX WARNING: Removed duplicated region for block: B:220:0x04c7  */
    /* JADX WARNING: Removed duplicated region for block: B:227:0x04dc  */
    /* JADX WARNING: Removed duplicated region for block: B:239:0x04ff  */
    /* JADX WARNING: Removed duplicated region for block: B:266:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A08(X.C06240Ss r82) {
        /*
        // Method dump skipped, instructions count: 1372
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0QV.A08(X.0Ss):void");
    }

    public void A09(C06240Ss r2) {
        r2.A05(this.A0W);
        r2.A05(this.A0Y);
        r2.A05(this.A0X);
        r2.A05(this.A0S);
        if (this.A07 > 0) {
            r2.A05(this.A0R);
        }
    }

    public void A0A(C06240Ss r8) {
        int i;
        int i2;
        int A00 = C06240Ss.A00(this.A0W);
        int A002 = C06240Ss.A00(this.A0Y);
        int A003 = C06240Ss.A00(this.A0X);
        int A004 = C06240Ss.A00(this.A0S);
        AnonymousClass0D3 r1 = this.A0c;
        C07270Xi r2 = r1.A05;
        if (r2.A0B) {
            C07270Xi r12 = r1.A04;
            if (r12.A0B) {
                A00 = r2.A02;
                A003 = r12.A02;
            }
        }
        AnonymousClass0D2 r13 = this.A0d;
        C07270Xi r22 = r13.A05;
        if (r22.A0B) {
            C07270Xi r14 = r13.A04;
            if (r14.A0B) {
                A002 = r22.A02;
                A004 = r14.A02;
            }
        }
        int i3 = A004 - A002;
        if (A003 - A00 < 0 || i3 < 0 || A00 == Integer.MIN_VALUE || A00 == Integer.MAX_VALUE || A002 == Integer.MIN_VALUE || A002 == Integer.MAX_VALUE || A003 == Integer.MIN_VALUE || A003 == Integer.MAX_VALUE || A004 == Integer.MIN_VALUE || A004 == Integer.MAX_VALUE) {
            A004 = 0;
            A00 = 0;
            A002 = 0;
            A003 = 0;
        }
        int i4 = A003 - A00;
        int i5 = A004 - A002;
        this.A0P = A00;
        this.A0Q = A002;
        if (this.A0N == 8) {
            this.A0O = 0;
            this.A09 = 0;
            return;
        }
        AnonymousClass0JR[] r23 = this.A0o;
        AnonymousClass0JR r0 = r23[0];
        AnonymousClass0JR r15 = AnonymousClass0JR.FIXED;
        if (r0 == r15 && i4 < (i2 = this.A0O)) {
            i4 = i2;
        }
        if (r23[1] == r15 && i5 < (i = this.A09)) {
            i5 = i;
        }
        this.A0O = i4;
        this.A09 = i5;
        int i6 = this.A0I;
        if (i5 < i6) {
            this.A09 = i6;
        }
        int i7 = this.A0J;
        if (i4 < i7) {
            this.A0O = i7;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:132:0x01a3, code lost:
        if (r16 != false) goto L_0x0140;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:0x01d6, code lost:
        if (r48 == 1) goto L_0x01d8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:164:0x01fe, code lost:
        if (r9 == r11) goto L_0x0200;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:171:0x0216, code lost:
        if (r8 == false) goto L_0x0104;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0060, code lost:
        if (r3 == 4) goto L_0x0062;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00ec, code lost:
        if (r15 != 0) goto L_0x00ee;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x00fd, code lost:
        if ((r9 instanceof X.AnonymousClass0Cu) != false) goto L_0x00ff;
     */
    /* JADX WARNING: Removed duplicated region for block: B:178:0x0228  */
    /* JADX WARNING: Removed duplicated region for block: B:246:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:248:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0108  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x010e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0B(X.C06240Ss r35, X.AnonymousClass0QC r36, X.AnonymousClass0QC r37, X.AnonymousClass0Q7 r38, X.AnonymousClass0Q7 r39, X.AnonymousClass0JR r40, float r41, float r42, int r43, int r44, int r45, int r46, int r47, int r48, int r49, int r50, boolean r51, boolean r52, boolean r53, boolean r54, boolean r55, boolean r56, boolean r57, boolean r58, boolean r59, boolean r60) {
        /*
        // Method dump skipped, instructions count: 830
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0QV.A0B(X.0Ss, X.0QC, X.0QC, X.0Q7, X.0Q7, X.0JR, float, float, int, int, int, int, int, int, int, int, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean):void");
    }

    public void A0C(AnonymousClass0JZ r5, AnonymousClass0JZ r6, AnonymousClass0QV r7, int i, int i2) {
        AnonymousClass0Q7 A04 = A04(r5);
        AnonymousClass0Q7 A042 = r7.A04(r6);
        if (A042 == null) {
            A04.A01();
            return;
        }
        A04.A03 = A042;
        HashSet hashSet = A042.A04;
        if (hashSet == null) {
            hashSet = new HashSet();
            A042.A04 = hashSet;
        }
        hashSet.add(A04);
        if (i > 0) {
            A04.A01 = i;
        } else {
            A04.A01 = 0;
        }
        A04.A00 = i2;
    }

    public boolean A0D() {
        return this.A0N != 8;
    }

    public boolean A0E() {
        AnonymousClass0Q7 r1 = this.A0W;
        AnonymousClass0Q7 r0 = r1.A03;
        if (r0 != null && r0.A03 == r1) {
            return true;
        }
        AnonymousClass0Q7 r12 = this.A0X;
        AnonymousClass0Q7 r02 = r12.A03;
        return r02 != null && r02.A03 == r12;
    }

    public boolean A0F() {
        AnonymousClass0Q7 r1 = this.A0Y;
        AnonymousClass0Q7 r0 = r1.A03;
        if (r0 != null && r0.A03 == r1) {
            return true;
        }
        AnonymousClass0Q7 r12 = this.A0S;
        AnonymousClass0Q7 r02 = r12.A03;
        return r02 != null && r02.A03 == r12;
    }

    public void A0G() {
        this.A0W.A01();
        this.A0Y.A01();
        this.A0X.A01();
        this.A0S.A01();
        this.A0R.A01();
        this.A0U.A01();
        this.A0V.A01();
        this.A0T.A01();
        this.A0Z = null;
        this.A00 = 0.0f;
        this.A0O = 0;
        this.A09 = 0;
        this.A01 = 0.0f;
        this.A08 = -1;
        this.A0P = 0;
        this.A0Q = 0;
        this.A07 = 0;
        this.A0J = 0;
        this.A0I = 0;
        this.A02 = 0.5f;
        this.A06 = 0.5f;
        AnonymousClass0JR[] r1 = this.A0o;
        AnonymousClass0JR r0 = AnonymousClass0JR.FIXED;
        r1[0] = r0;
        r1[1] = r0;
        this.A0e = null;
        this.A0N = 0;
        this.A0A = 0;
        this.A0L = 0;
        float[] fArr = this.A0j;
        fArr[0] = -1.0f;
        fArr[1] = -1.0f;
        this.A0B = -1;
        this.A0M = -1;
        int[] iArr = this.A0k;
        iArr[0] = Integer.MAX_VALUE;
        iArr[1] = Integer.MAX_VALUE;
        this.A0D = 0;
        this.A0C = 0;
        this.A04 = 1.0f;
        this.A03 = 1.0f;
        this.A0F = Integer.MAX_VALUE;
        this.A0E = Integer.MAX_VALUE;
        this.A0H = 0;
        this.A0G = 0;
        this.A0K = -1;
        this.A05 = 1.0f;
        boolean[] zArr = this.A0r;
        zArr[0] = true;
        zArr[1] = true;
        boolean[] zArr2 = this.A0s;
        zArr2[0] = false;
        zArr2[1] = false;
    }

    public void A0H(boolean z, boolean z2) {
        int i;
        int i2;
        AnonymousClass0D3 r2 = this.A0c;
        boolean z3 = z & r2.A09;
        AnonymousClass0D2 r1 = this.A0d;
        boolean z4 = z2 & r1.A09;
        int i3 = r2.A05.A02;
        int i4 = r1.A05.A02;
        int i5 = r2.A04.A02;
        int i6 = r1.A04.A02;
        int i7 = i6 - i4;
        if (i5 - i3 < 0 || i7 < 0 || i3 == Integer.MIN_VALUE || i3 == Integer.MAX_VALUE || i4 == Integer.MIN_VALUE || i4 == Integer.MAX_VALUE || i5 == Integer.MIN_VALUE || i5 == Integer.MAX_VALUE || i6 == Integer.MIN_VALUE || i6 == Integer.MAX_VALUE) {
            i5 = 0;
            i3 = 0;
            i6 = 0;
            i4 = 0;
        }
        int i8 = i5 - i3;
        int i9 = i6 - i4;
        if (z3) {
            this.A0P = i3;
        }
        if (z4) {
            this.A0Q = i4;
        }
        if (this.A0N == 8) {
            this.A0O = 0;
            this.A09 = 0;
            return;
        }
        if (z3) {
            if (this.A0o[0] == AnonymousClass0JR.FIXED && i8 < (i2 = this.A0O)) {
                i8 = i2;
            }
            this.A0O = i8;
            int i10 = this.A0J;
            if (i8 < i10) {
                this.A0O = i10;
            }
        }
        if (z4) {
            if (this.A0o[1] == AnonymousClass0JR.FIXED && i9 < (i = this.A09)) {
                i9 = i;
            }
            this.A09 = i9;
            int i11 = this.A0I;
            if (i9 < i11) {
                this.A09 = i11;
            }
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        String str = "";
        String str2 = this.A0f;
        if (str2 != null) {
            StringBuilder sb2 = new StringBuilder("id: ");
            sb2.append(str2);
            sb2.append(" ");
            str = sb2.toString();
        }
        sb.append(str);
        sb.append("(");
        sb.append(this.A0P);
        sb.append(", ");
        sb.append(this.A0Q);
        sb.append(") - (");
        sb.append(this.A0O);
        sb.append(" x ");
        sb.append(this.A09);
        sb.append(")");
        return sb.toString();
    }
}
