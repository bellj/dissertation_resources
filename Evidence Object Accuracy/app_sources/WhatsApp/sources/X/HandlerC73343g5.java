package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.whatsapp.registration.RegisterName;

/* renamed from: X.3g5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class HandlerC73343g5 extends Handler {
    public final /* synthetic */ RegisterName A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public HandlerC73343g5(Looper looper, RegisterName registerName) {
        super(looper);
        this.A00 = registerName;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        RunnableC49072Ja r1 = RegisterName.A1S;
        if (r1 != null && r1.A03) {
            boolean z = r1.A04;
            int i = 1;
            RegisterName registerName = this.A00;
            if (z) {
                AnonymousClass27T r0 = registerName.A0r;
                if (r0 != null) {
                    r0.A00(1);
                    return;
                }
                return;
            }
            C36021jC.A00(registerName, 0);
            int i2 = RegisterName.A1S.A00;
            if (i2 != 1) {
                if (i2 == 3) {
                    i = 109;
                } else {
                    return;
                }
            }
            C36021jC.A01(registerName, i);
        }
    }
}
