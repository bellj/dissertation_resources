package X;

import com.google.android.exoplayer2.Timeline;

/* renamed from: X.2kH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56072kH extends AbstractC76613lv {
    public static final Object A02 = C12970iu.A0l();
    public final Object A00;
    public final Object A01;

    public C56072kH(Timeline timeline, Object obj, Object obj2) {
        super(timeline);
        this.A01 = obj;
        this.A00 = obj2;
    }

    @Override // com.google.android.exoplayer2.Timeline
    public int A04(Object obj) {
        Object obj2;
        Timeline timeline = super.A00;
        if (A02.equals(obj) && (obj2 = this.A00) != null) {
            obj = obj2;
        }
        return timeline.A04(obj);
    }

    @Override // com.google.android.exoplayer2.Timeline
    public AnonymousClass4YJ A09(AnonymousClass4YJ r3, int i, boolean z) {
        super.A00.A09(r3, i, z);
        if (AnonymousClass3JZ.A0H(r3.A05, this.A00) && z) {
            r3.A05 = A02;
        }
        return r3;
    }

    @Override // X.AbstractC76613lv, com.google.android.exoplayer2.Timeline
    public C94404bl A0B(C94404bl r3, int i, long j) {
        super.A00.A0B(r3, i, j);
        if (AnonymousClass3JZ.A0H(r3.A09, this.A01)) {
            r3.A09 = C94404bl.A0F;
        }
        return r3;
    }

    @Override // com.google.android.exoplayer2.Timeline
    public Object A0C(int i) {
        Object A0C = super.A00.A0C(i);
        return AnonymousClass3JZ.A0H(A0C, this.A00) ? A02 : A0C;
    }
}
