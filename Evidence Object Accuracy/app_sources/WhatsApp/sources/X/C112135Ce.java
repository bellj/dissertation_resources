package X;

import java.text.Collator;
import java.util.Comparator;

/* renamed from: X.5Ce  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final /* synthetic */ class C112135Ce implements Comparator {
    public final /* synthetic */ Collator A00;

    public /* synthetic */ C112135Ce(Collator collator) {
        this.A00 = collator;
    }

    @Override // java.util.Comparator
    public final int compare(Object obj, Object obj2) {
        return this.A00.compare(((AnonymousClass4XI) obj).A03, ((AnonymousClass4XI) obj2).A03);
    }
}
