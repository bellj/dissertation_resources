package X;

import com.whatsapp.util.Log;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/* renamed from: X.0p0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16430p0 {
    public int A00;
    public String A01;
    public Random A02;
    public final C251118d A03;
    public final C16120oU A04;

    public C16430p0(C251118d r1, C16120oU r2) {
        this.A04 = r2;
        this.A03 = r1;
    }

    public void A00(int i, int i2) {
        long j;
        AnonymousClass2P0 r2 = new AnonymousClass2P0();
        this.A00 = i;
        if (i2 == 3 || i2 == 5 || i2 == 7) {
            Random random = this.A02;
            if (random == null) {
                random = new Random();
                this.A02 = random;
            }
            this.A01 = Long.toHexString(random.nextLong());
        }
        if (i == 1 || i == 2 || i == 3 || i == 4) {
            j = 0;
        } else {
            Log.e("DirectorySearchAnalyticsManager/getEntryPointVersion: Entrypoint not recognised");
            j = -1;
        }
        r2.A01 = Long.valueOf(j);
        r2.A00 = Integer.valueOf(i2);
        r2.A02 = this.A01;
        this.A04.A07(r2);
    }

    public void A01(int i, int i2) {
        C28531Ny r1 = new C28531Ny();
        r1.A06 = Integer.valueOf(i);
        r1.A09 = Integer.valueOf(i2);
        A03(r1);
    }

    public void A02(int i, long j, int i2) {
        C28531Ny r1 = new C28531Ny();
        r1.A06 = 7;
        r1.A0M = Long.valueOf(j);
        r1.A03 = Integer.valueOf(i);
        if (i2 == 0) {
            i2 = 2;
        }
        r1.A07 = Integer.valueOf(i2);
        A03(r1);
    }

    public final void A03(C28531Ny r3) {
        r3.A0W = this.A01;
        r3.A04 = Integer.valueOf(this.A00);
        r3.A0J = 0L;
        this.A04.A07(r3);
    }

    public final void A04(C28531Ny r3) {
        r3.A0W = this.A01;
        r3.A04 = Integer.valueOf(this.A00);
        r3.A0J = 1L;
        this.A04.A07(r3);
    }

    public void A05(Integer num, int i, int i2) {
        C28531Ny r1 = new C28531Ny();
        r1.A06 = Integer.valueOf(i);
        r1.A05 = Integer.valueOf(i2);
        r1.A03 = num;
        A03(r1);
    }

    public void A06(Integer num, int i, int i2) {
        C28531Ny r1 = new C28531Ny();
        r1.A06 = Integer.valueOf(i);
        r1.A07 = Integer.valueOf(i2);
        r1.A03 = num;
        A03(r1);
    }

    public void A07(Integer num, Long l, Long l2, int i) {
        C28531Ny r2 = new C28531Ny();
        r2.A06 = Integer.valueOf(i);
        r2.A0L = l;
        r2.A0Q = l2;
        r2.A0I = 1L;
        r2.A03 = num;
        A03(r2);
    }

    public void A08(Integer num, Long l, Long l2, Long l3, Long l4, Long l5, Long l6, Long l7, String str, int i) {
        C28531Ny r1 = new C28531Ny();
        r1.A06 = Integer.valueOf(i);
        r1.A0F = l;
        r1.A0D = l2;
        r1.A0E = l3;
        r1.A0H = l4;
        r1.A0I = 1L;
        r1.A03 = num;
        C14850m9 r2 = this.A03.A00;
        if (r2.A07(450) && r2.A07(1310)) {
            r1.A0K = l5;
            r1.A0O = l6;
            r1.A0C = l7;
            if (str != null) {
                r1.A0B = Long.valueOf((long) str.length());
                r1.A0G = Long.valueOf((long) str.trim().split("\\s+").length);
            }
        } else if (i == 54 || i == 57 || i == 56 || i == 55) {
            return;
        }
        A03(r1);
    }

    public void A09(Integer num, String str, String str2, double d, double d2, int i, int i2) {
        if (str2 != null && num != null) {
            C14850m9 r1 = this.A03.A00;
            if (r1.A07(450) && r1.A07(1951)) {
                C50292Oz r2 = new C50292Oz();
                r2.A05 = str2;
                r2.A04 = Long.valueOf((long) i);
                r2.A02 = num;
                r2.A03 = Integer.valueOf(i2);
                r2.A06 = str;
                r2.A01 = Double.valueOf(d);
                r2.A00 = Double.valueOf(d2);
                this.A04.A07(r2);
            }
        }
    }

    public void A0A(String str, int i, int i2, long j) {
        C28531Ny r1 = new C28531Ny();
        r1.A06 = 8;
        r1.A0X = str;
        r1.A0R = Long.valueOf(j);
        r1.A03 = Integer.valueOf(i);
        if (i2 == 0) {
            i2 = 2;
        }
        r1.A07 = Integer.valueOf(i2);
        A03(r1);
    }

    public void A0B(String str, String str2, int i, int i2, int i3, int i4, long j, long j2, long j3) {
        C28531Ny r1 = new C28531Ny();
        r1.A06 = 12;
        r1.A0O = Long.valueOf(j);
        r1.A03 = Integer.valueOf(i);
        r1.A0X = str;
        r1.A0Z = str2;
        r1.A0K = Long.valueOf(j2);
        r1.A0N = Long.valueOf(j3);
        r1.A01 = Integer.valueOf(i2);
        r1.A02 = Integer.valueOf(i4);
        if (i3 == 0) {
            i3 = 2;
        }
        r1.A07 = Integer.valueOf(i3);
        A03(r1);
    }

    public void A0C(String str, String str2, int i, int i2, int i3, int i4, long j, long j2, long j3) {
        C28531Ny r1 = new C28531Ny();
        r1.A06 = 38;
        r1.A0O = Long.valueOf(j);
        r1.A0X = str;
        r1.A0Z = str2;
        r1.A0K = Long.valueOf(j2);
        r1.A0N = Long.valueOf(j3);
        r1.A03 = Integer.valueOf(i);
        r1.A01 = Integer.valueOf(i2);
        r1.A02 = Integer.valueOf(i4);
        if (i3 == 0) {
            i3 = 2;
        }
        r1.A07 = Integer.valueOf(i3);
        A03(r1);
    }

    public void A0D(List list) {
        C14850m9 r1 = this.A03.A00;
        if (r1.A07(450) && r1.A07(1951) && list != null) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                this.A04.A07((AbstractC16110oT) it.next());
            }
        }
    }
}
