package X;

import com.facebook.redex.RunnableBRunnable0Shape7S0200000_I0_7;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* renamed from: X.2Ki  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C49332Ki implements AbstractC21730xt {
    public final C14900mE A00;
    public final C17220qS A01;
    public final C48192Eu A02;

    public C49332Ki(C14900mE r1, C17220qS r2, C48192Eu r3) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
    }

    public final void A00(int i) {
        this.A00.A0I(new RunnableBRunnable0Shape7S0200000_I0_7(this, 30, new C48182Et(null, null, null, -1, i)));
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        Log.e("sendScanContactQr/delivery-error");
        A00(408);
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r2, String str) {
        Log.e("sendScanContactQr/response-error");
        A00(C41151sz.A00(r2));
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r10, String str) {
        String str2;
        int i;
        AnonymousClass1V8 A0E = r10.A0E("qr");
        if (A0E != null) {
            String A0I = A0E.A0I("jid", null);
            String A0I2 = A0E.A0I("notify", null);
            UserJid nullable = UserJid.getNullable(A0I);
            if (nullable == null) {
                str2 = "sendScanContactQr/error: invalid jid";
            } else {
                String A0I3 = A0E.A0I("type", null);
                String str3 = null;
                if ("contact".equals(A0I3)) {
                    i = 0;
                } else if ("subscribe".equals(A0I3)) {
                    i = 1;
                } else if ("message".equals(A0I3)) {
                    AnonymousClass1V8 A0E2 = A0E.A0E("message");
                    if (A0E2 != null) {
                        str3 = A0E2.A0G();
                    }
                    i = 2;
                } else {
                    StringBuilder sb = new StringBuilder("sendScanContactQr/error: invalid type ");
                    sb.append(A0I3);
                    str2 = sb.toString();
                }
                Log.e("sendScanContactQr/success");
                this.A00.A0I(new RunnableBRunnable0Shape7S0200000_I0_7(this, 30, new C48182Et(nullable, A0I2, str3, i, 0)));
                return;
            }
        } else {
            str2 = "sendScanContactQr/error: missing node";
        }
        Log.e(str2);
        A00(0);
    }
}
