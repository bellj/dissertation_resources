package X;

/* renamed from: X.24c  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C460124c implements Comparable {
    public boolean A00 = true;
    public final int A01;
    public final C460024b A02;
    public final C32631cT A03;
    public final String A04;
    public final String A05;
    public final String A06;
    public final String A07;
    public final String A08;
    public final boolean A09;

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C460124c) {
                C460124c r5 = (C460124c) obj;
                if (!C16700pc.A0O(this.A06, r5.A06) || !C16700pc.A0O(this.A08, r5.A08) || !C16700pc.A0O(this.A05, r5.A05) || !C16700pc.A0O(this.A04, r5.A04) || !C16700pc.A0O(this.A07, r5.A07) || this.A01 != r5.A01 || this.A09 != r5.A09 || !C16700pc.A0O(this.A02, r5.A02) || !C16700pc.A0O(this.A03, r5.A03) || this.A00 != r5.A00) {
                }
            }
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        int hashCode = ((((((((((this.A06.hashCode() * 31) + this.A08.hashCode()) * 31) + this.A05.hashCode()) * 31) + this.A04.hashCode()) * 31) + this.A07.hashCode()) * 31) + this.A01) * 31;
        boolean z = this.A09;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = (hashCode + i2) * 31;
        C460024b r0 = this.A02;
        int i6 = 0;
        int hashCode2 = (i5 + (r0 == null ? 0 : r0.hashCode())) * 31;
        C32631cT r02 = this.A03;
        if (r02 != null) {
            i6 = r02.hashCode();
        }
        int i7 = (hashCode2 + i6) * 31;
        if (!this.A00) {
            i = 0;
        }
        return i7 + i;
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("AlertInfo(id=");
        sb.append(this.A06);
        sb.append(", title=");
        sb.append(this.A08);
        sb.append(", description=");
        sb.append(this.A05);
        sb.append(", ctaText=");
        sb.append(this.A04);
        sb.append(", scope=");
        sb.append(this.A07);
        sb.append(", type=");
        sb.append(this.A01);
        sb.append(", isCancelable=");
        sb.append(this.A09);
        sb.append(", phoenix=");
        sb.append(this.A02);
        sb.append(", legacyPaymentStepUpInfo=");
        sb.append(this.A03);
        sb.append(", shouldShowNotification=");
        sb.append(this.A00);
        sb.append(')');
        return sb.toString();
    }

    public C460124c(C460024b r3, C32631cT r4, String str, String str2, String str3, String str4, String str5, int i, boolean z) {
        C16700pc.A0E(str, 1);
        C16700pc.A0E(str2, 2);
        C16700pc.A0E(str3, 3);
        C16700pc.A0E(str4, 4);
        C16700pc.A0E(str5, 5);
        this.A06 = str;
        this.A08 = str2;
        this.A05 = str3;
        this.A04 = str4;
        this.A07 = str5;
        this.A01 = i;
        this.A09 = z;
        this.A02 = r3;
        this.A03 = r4;
    }

    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(Object obj) {
        C460124c r3 = (C460124c) obj;
        C16700pc.A0E(r3, 0);
        return this.A01 - r3.A01;
    }
}
