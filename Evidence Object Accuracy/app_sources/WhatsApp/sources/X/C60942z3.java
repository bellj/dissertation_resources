package X;

import com.whatsapp.conversation.conversationrow.message.StarredMessagesActivity;
import com.whatsapp.util.Log;

/* renamed from: X.2z3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60942z3 extends AnonymousClass1MD {
    public final /* synthetic */ AbstractActivityC35431hr A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C60942z3(AnonymousClass12P r44, AbstractC15710nm r45, C14900mE r46, C15570nT r47, C15450nH r48, C16170oZ r49, ActivityC13790kL r50, AnonymousClass12T r51, C18850tA r52, C15550nR r53, C22700zV r54, C15610nY r55, AbstractActivityC35431hr r56, AnonymousClass1A6 r57, C255419u r58, AnonymousClass01d r59, C14830m7 r60, C14820m6 r61, AnonymousClass018 r62, C15600nX r63, C253218y r64, C242114q r65, C22100yW r66, C22180yf r67, AnonymousClass19M r68, C231510o r69, AnonymousClass193 r70, C14850m9 r71, C16120oU r72, C20710wC r73, AnonymousClass109 r74, C22370yy r75, AnonymousClass13H r76, C16630pM r77, C88054Ec r78, AnonymousClass12F r79, C253018w r80, AnonymousClass12U r81, C23000zz r82, C252718t r83, AbstractC14440lR r84, AnonymousClass01H r85) {
        super(r44, r45, r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r57, r58, r59, r60, r61, r62, r63, r64, r65, r66, r67, r68, r69, r70, r71, r72, r73, r74, r75, r76, r77, r78, r79, r80, r81, r82, r83, r84, r85);
        this.A00 = r56;
    }

    @Override // X.AnonymousClass1MD, X.AnonymousClass02Q
    public void AP3(AbstractC009504t r4) {
        String str;
        StringBuilder A0h = C12960it.A0h();
        AbstractActivityC35431hr r2 = this.A00;
        if (!(r2 instanceof StarredMessagesActivity)) {
            str = "kept";
        } else {
            str = "starred";
        }
        A0h.append(str);
        Log.i(C12960it.A0d("/selectionended", A0h));
        super.AP3(r4);
        C35451ht r0 = ((AbstractActivityC13750kH) r2).A0J;
        if (r0 != null) {
            r0.A00();
            ((AbstractActivityC13750kH) r2).A0J = null;
        }
        r2.A07.notifyDataSetChanged();
        ((AbstractActivityC13750kH) r2).A01 = null;
    }
}
