package X;

import android.os.Bundle;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.03n  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C007003n {
    public CharSequence A00;
    public CharSequence[] A01;
    public final Bundle A02 = new Bundle();
    public final Set A03 = new HashSet();

    public C007003n(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Result key can't be null");
        }
    }
}
