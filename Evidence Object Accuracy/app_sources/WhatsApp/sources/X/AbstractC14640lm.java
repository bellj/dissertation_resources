package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.whatsapp.jid.Jid;

/* renamed from: X.0lm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC14640lm extends Jid implements Parcelable {
    public AbstractC14640lm(Parcel parcel) {
        super(parcel);
    }

    public AbstractC14640lm(String str) {
        super(str);
    }

    public static AbstractC14640lm A00(String str) {
        Jid jid = Jid.get(str);
        if (jid instanceof AbstractC14640lm) {
            return (AbstractC14640lm) jid;
        }
        throw new AnonymousClass1MW(str);
    }

    public static AbstractC14640lm A01(String str) {
        AbstractC14640lm r0 = null;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            r0 = A00(str);
            return r0;
        } catch (AnonymousClass1MW unused) {
            return r0;
        }
    }
}
