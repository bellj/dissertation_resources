package X;

import android.content.Context;
import java.io.File;

/* renamed from: X.1DE  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1DE implements AbstractC16990q5 {
    public final Context A00;

    @Override // X.AbstractC16990q5
    public /* synthetic */ void AOp() {
    }

    public AnonymousClass1DE(Context context) {
        this.A00 = context;
    }

    @Override // X.AbstractC16990q5
    public void AOo() {
        Context context = this.A00;
        String str = new String[]{"vpx_1_8_1"}[0];
        StringBuilder sb = new StringBuilder("lib");
        sb.append(str);
        sb.append(".so");
        try {
            new File(context.getFilesDir(), sb.toString()).delete();
        } catch (SecurityException unused) {
        }
    }
}
