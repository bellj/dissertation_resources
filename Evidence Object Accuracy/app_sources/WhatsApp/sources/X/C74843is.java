package X;

import com.facebook.redex.RunnableBRunnable0Shape15S0100000_I1_1;
import com.whatsapp.companiondevice.LinkedDevicesActivity;

/* renamed from: X.3is  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74843is extends AnonymousClass0QE {
    public final /* synthetic */ LinkedDevicesActivity A00;

    public C74843is(LinkedDevicesActivity linkedDevicesActivity) {
        this.A00 = linkedDevicesActivity;
    }

    @Override // X.AnonymousClass0QE
    public void A00() {
        LinkedDevicesActivity linkedDevicesActivity = this.A00;
        ((ActivityC13810kN) linkedDevicesActivity).A05.A0H(new RunnableBRunnable0Shape15S0100000_I1_1(linkedDevicesActivity, 16));
    }
}
