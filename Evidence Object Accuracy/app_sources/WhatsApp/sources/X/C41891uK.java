package X;

import android.content.SharedPreferences;
import android.os.SystemClock;
import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.HashSet;

/* renamed from: X.1uK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C41891uK {
    public final C14830m7 A00;
    public final C14820m6 A01;
    public final C15990oG A02;
    public final C18240s8 A03;
    public final C22100yW A04;
    public final AbstractC14440lR A05;

    public C41891uK(C14830m7 r1, C14820m6 r2, C15990oG r3, C18240s8 r4, C22100yW r5, AbstractC14440lR r6) {
        this.A00 = r1;
        this.A05 = r6;
        this.A03 = r4;
        this.A02 = r3;
        this.A01 = r2;
        this.A04 = r5;
    }

    public static C41901uL A00(int i, int i2, long j) {
        AnonymousClass1G4 A0T = C41901uL.A04.A0T();
        A0T.A03();
        C41901uL r1 = (C41901uL) A0T.A00;
        r1.A00 |= 1;
        r1.A02 = i;
        A0T.A03();
        C41901uL r12 = (C41901uL) A0T.A00;
        r12.A00 |= 4;
        r12.A01 = i2;
        A0T.A03();
        C41901uL r13 = (C41901uL) A0T.A00;
        r13.A00 |= 2;
        r13.A03 = j;
        return (C41901uL) A0T.A02();
    }

    public long A01() {
        long j;
        long j2 = this.A01.A00.getLong("adv_timestamp_sec", -1);
        C14830m7 r9 = this.A00;
        if (r9.A01 != 0) {
            j = r9.A01 + SystemClock.elapsedRealtime();
        } else {
            j = 0;
        }
        long j3 = j / 1000;
        if (j3 == 0) {
            j3 = System.currentTimeMillis() / 1000;
        }
        long A01 = r9.A01() / 1000;
        long j4 = 1 + j2;
        if (j4 > j3 + 86400) {
            StringBuilder sb = new StringBuilder("CompanionDeviceAdvUtil/getTimestampSec invalid ts lastTs=");
            sb.append(j2);
            sb.append("; ntpTs=");
            sb.append(j3);
            sb.append("; serverTs=");
            sb.append(A01);
            Log.e(sb.toString());
            return -1;
        }
        if (Math.abs(A01 - j3) <= 86400) {
            j3 = A01;
        }
        return Math.max(j3, j4);
    }

    public C41911uM A02(C41901uL r7) {
        C22100yW r1 = this.A04;
        HashSet hashSet = new HashSet();
        r1.A07.A08();
        hashSet.add(0);
        for (AnonymousClass1JU r0 : new ArrayList(r1.A0I.A00().A00.values())) {
            hashSet.add(Integer.valueOf(r0.A03));
        }
        hashSet.add(Integer.valueOf(r7.A01));
        AnonymousClass1G4 A0T = C41911uM.A06.A0T();
        int i = r7.A02;
        A0T.A03();
        C41911uM r12 = (C41911uM) A0T.A00;
        r12.A00 |= 1;
        r12.A02 = i;
        int i2 = r7.A01;
        A0T.A03();
        C41911uM r13 = (C41911uM) A0T.A00;
        r13.A00 |= 4;
        r13.A01 = i2;
        long j = r7.A03;
        A0T.A03();
        C41911uM r14 = (C41911uM) A0T.A00;
        r14.A00 |= 2;
        r14.A04 = j;
        A0T.A03();
        C41911uM r2 = (C41911uM) A0T.A00;
        AbstractC41941uP r15 = r2.A05;
        if (!((AnonymousClass1K7) r15).A00) {
            r15 = AbstractC27091Fz.A0F(r15);
            r2.A05 = r15;
        }
        AnonymousClass1G5.A01(hashSet, r15);
        return (C41911uM) A0T.A02();
    }

    public C41921uN A03(C41911uM r6) {
        C31951bN r0 = (C31951bN) this.A03.A00.submit(new CallableC41951uQ(this)).get();
        AnonymousClass009.A05(r0);
        C32051bX r4 = r0.A00;
        byte[] A05 = C16050oM.A05(AnonymousClass01V.A0D, r6.A02());
        AnonymousClass1G4 A0T = C41921uN.A03.A0T();
        byte[] A052 = C15940oB.A05(r4, A05);
        AbstractC27881Jp A01 = AbstractC27881Jp.A01(A052, 0, A052.length);
        A0T.A03();
        C41921uN r1 = (C41921uN) A0T.A00;
        r1.A00 |= 2;
        r1.A01 = A01;
        AbstractC27881Jp A00 = r6.A00();
        A0T.A03();
        C41921uN r12 = (C41921uN) A0T.A00;
        r12.A00 |= 1;
        r12.A02 = A00;
        return (C41921uN) A0T.A02();
    }

    public void A04() {
        this.A01.A00.edit().putLong("adv_timestamp_sec", -1).apply();
        this.A05.Ab2(new RunnableBRunnable0Shape4S0100000_I0_4(this, 3));
    }

    public void A05(long j) {
        SharedPreferences sharedPreferences = this.A01.A00;
        sharedPreferences.edit().remove("adv_key_index_list_update_retry_count").apply();
        sharedPreferences.edit().remove("adv_key_index_list_last_failure_time").apply();
        sharedPreferences.edit().putLong("adv_key_index_list_last_update_time", this.A00.A00()).apply();
        sharedPreferences.edit().remove("adv_key_index_list_require_update").apply();
        if (j > 0) {
            sharedPreferences.edit().putLong("adv_timestamp_sec", j).apply();
        }
    }
}
