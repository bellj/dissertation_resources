package X;

import android.content.Context;
import androidx.work.ListenableWorker;
import java.util.concurrent.Executor;

/* renamed from: X.0eF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC10230eF implements Runnable {
    public static final String A06 = C06390Tk.A01("WorkForegroundRunnable");
    public final Context A00;
    public final AbstractC11950h8 A01;
    public final ListenableWorker A02;
    public final C004401z A03;
    public final AnonymousClass040 A04 = AnonymousClass040.A00();
    public final AbstractC11500gO A05;

    public RunnableC10230eF(Context context, AbstractC11950h8 r3, ListenableWorker listenableWorker, C004401z r5, AbstractC11500gO r6) {
        this.A00 = context;
        this.A03 = r5;
        this.A02 = listenableWorker;
        this.A01 = r3;
        this.A05 = r6;
    }

    public AbstractFutureC44231yX A00() {
        return this.A04;
    }

    @Override // java.lang.Runnable
    public void run() {
        if (!this.A03.A0H || C04070Kg.A00()) {
            this.A04.A09(null);
            return;
        }
        AnonymousClass040 A00 = AnonymousClass040.A00();
        Executor executor = ((C07760a2) this.A05).A02;
        executor.execute(new RunnableC09670dK(this, A00));
        A00.A5i(new RunnableC09680dL(this, A00), executor);
    }
}
