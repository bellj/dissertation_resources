package X;

import android.os.Handler;
import com.facebook.redex.RunnableBRunnable0Shape11S0100000_I0_11;
import com.whatsapp.service.UnsentMessagesNetworkAvailableJob;

/* renamed from: X.1yW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C44221yW extends AbstractC18860tB {
    public final /* synthetic */ UnsentMessagesNetworkAvailableJob A00;

    public C44221yW(UnsentMessagesNetworkAvailableJob unsentMessagesNetworkAvailableJob) {
        this.A00 = unsentMessagesNetworkAvailableJob;
    }

    public static /* synthetic */ void A00(C44221yW r1) {
        UnsentMessagesNetworkAvailableJob unsentMessagesNetworkAvailableJob = r1.A00;
        unsentMessagesNetworkAvailableJob.A01.A04(unsentMessagesNetworkAvailableJob.A08);
    }

    @Override // X.AbstractC18860tB
    public void A02(AbstractC15340mz r4, int i) {
        UnsentMessagesNetworkAvailableJob unsentMessagesNetworkAvailableJob = this.A00;
        if (!unsentMessagesNetworkAvailableJob.A03.A03()) {
            Handler handler = unsentMessagesNetworkAvailableJob.A07;
            handler.removeCallbacks(unsentMessagesNetworkAvailableJob.A0A);
            UnsentMessagesNetworkAvailableJob.A06(unsentMessagesNetworkAvailableJob);
            handler.post(new RunnableBRunnable0Shape11S0100000_I0_11(this, 37));
        }
    }
}
