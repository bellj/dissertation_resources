package X;

import android.content.Context;
import com.whatsapp.util.Log;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

/* renamed from: X.0sE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18300sE extends AbstractC18220s6 {
    public final Context A00;
    public final C16210od A01;
    public final AbstractC15710nm A02;
    public final C14330lG A03;
    public final C15570nT A04;
    public final C18250s9 A05;
    public final C18280sC A06;
    public final C18230s7 A07;
    public final C17050qB A08;
    public final AnonymousClass01d A09;
    public final C14830m7 A0A;
    public final C14820m6 A0B;
    public final C14950mJ A0C;
    public final C15990oG A0D;
    public final C18240s8 A0E;
    public final C18260sA A0F;
    public final C16490p7 A0G;
    public final C18290sD A0H;
    public final C14850m9 A0I;
    public final C16120oU A0J;
    public final AbstractC14440lR A0K;
    public final AnonymousClass01H A0L;

    public C18300sE(Context context, C16210od r3, AbstractC15710nm r4, C14330lG r5, C15570nT r6, C18250s9 r7, C18280sC r8, C18230s7 r9, C17050qB r10, AnonymousClass01d r11, C14830m7 r12, C14820m6 r13, C14950mJ r14, C15990oG r15, C18240s8 r16, C18260sA r17, C16490p7 r18, C18290sD r19, C14850m9 r20, C16120oU r21, AbstractC14440lR r22, AnonymousClass01H r23) {
        super(context);
        this.A00 = context;
        this.A0A = r12;
        this.A0I = r20;
        this.A07 = r9;
        this.A02 = r4;
        this.A04 = r6;
        this.A0K = r22;
        this.A03 = r5;
        this.A0J = r21;
        this.A0C = r14;
        this.A0E = r16;
        this.A09 = r11;
        this.A05 = r7;
        this.A0D = r15;
        this.A08 = r10;
        this.A0F = r17;
        this.A0G = r18;
        this.A0B = r13;
        this.A06 = r8;
        this.A0H = r19;
        this.A01 = r3;
        this.A0L = r23;
    }

    public final void A02() {
        long nextInt;
        Calendar instance = Calendar.getInstance();
        if (instance.get(11) >= 2) {
            instance.add(5, 1);
        }
        instance.set(14, 0);
        instance.set(13, 0);
        instance.set(12, 0);
        instance.set(11, 2);
        long timeInMillis = instance.getTimeInMillis();
        C18250s9 r0 = this.A05;
        C15450nH r2 = r0.A00;
        Random random = r0.A01;
        int A02 = r2.A02(AbstractC15460nI.A1t);
        if (A02 <= 0) {
            nextInt = 0;
        } else {
            nextInt = ((long) random.nextInt(A02 << 1)) * 1000;
        }
        long j = timeInMillis + nextInt;
        StringBuilder sb = new StringBuilder("BackupMessagesAction/setupBackupMessagesAlarm; alarmTimeMillis=");
        sb.append(new Date(j));
        Log.i(sb.toString());
        if (!this.A07.A02(super.A00("com.whatsapp.action.BACKUP_MESSAGES", 134217728), 0, j)) {
            Log.w("BackupMessagesAction/setupBackupMessagesAlarm AlarmManager is null");
        }
    }
}
