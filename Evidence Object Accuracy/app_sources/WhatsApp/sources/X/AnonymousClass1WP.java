package X;

import org.json.JSONObject;

/* renamed from: X.1WP  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1WP extends AnonymousClass1WI implements AnonymousClass1J7 {
    public final /* synthetic */ JSONObject $jsonObject;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass1WP(JSONObject jSONObject) {
        super(1);
        this.$jsonObject = jSONObject;
    }

    @Override // X.AnonymousClass1J7
    public /* bridge */ /* synthetic */ Object AJ4(Object obj) {
        String str = (String) obj;
        C16700pc.A0B(str);
        boolean z = true;
        if (str.length() <= 0 || !this.$jsonObject.has(str)) {
            z = false;
        }
        return Boolean.valueOf(z);
    }
}
