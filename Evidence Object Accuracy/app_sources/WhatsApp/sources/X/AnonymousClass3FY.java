package X;

import java.net.DatagramPacket;

/* renamed from: X.3FY  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3FY {
    public final byte[] A00 = new byte[48];
    public volatile DatagramPacket A01;

    public final int A00(int i) {
        byte[] bArr = this.A00;
        return (bArr[i + 3] & 255) | ((bArr[i] & 255) << 24) | ((bArr[i + 1] & 255) << 16) | ((bArr[i + 2] & 255) << 8);
    }

    public synchronized DatagramPacket A01() {
        if (this.A01 == null) {
            byte[] bArr = this.A00;
            this.A01 = new DatagramPacket(bArr, bArr.length);
            this.A01.setPort(123);
        }
        return this.A01;
    }

    public final C71193cX A02(int i) {
        byte[] bArr = this.A00;
        return new C71193cX((((long) (bArr[i] & 255)) << 56) | (((long) (bArr[i + 1] & 255)) << 48) | (((long) (bArr[i + 2] & 255)) << 40) | (((long) (bArr[i + 3] & 255)) << 32) | (((long) (bArr[i + 4] & 255)) << 24) | (((long) (bArr[i + 5] & 255)) << 16) | (((long) (bArr[i + 6] & 255)) << 8) | ((long) (bArr[i + 7] & 255)));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0073, code lost:
        if (r6 == 4) goto L_0x0075;
     */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0091  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String toString() {
        /*
        // Method dump skipped, instructions count: 271
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3FY.toString():java.lang.String");
    }
}
