package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.1fw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C34311fw extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C34311fw A05;
    public static volatile AnonymousClass255 A06;
    public int A00;
    public int A01;
    public int A02 = -1;
    public int A03;
    public AbstractC41941uP A04 = C56822m0.A02;

    static {
        C34311fw r0 = new C34311fw();
        A05 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r7, Object obj, Object obj2) {
        switch (r7.ordinal()) {
            case 0:
                return A05;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                C34311fw r9 = (C34311fw) obj2;
                int i = this.A00;
                boolean z = true;
                if ((i & 1) != 1) {
                    z = false;
                }
                int i2 = this.A03;
                int i3 = r9.A00;
                boolean z2 = true;
                if ((i3 & 1) != 1) {
                    z2 = false;
                }
                this.A03 = r8.Afp(i2, r9.A03, z, z2);
                boolean z3 = false;
                if ((i & 2) == 2) {
                    z3 = true;
                }
                int i4 = this.A01;
                boolean z4 = false;
                if ((i3 & 2) == 2) {
                    z4 = true;
                }
                this.A01 = r8.Afp(i4, r9.A01, z3, z4);
                this.A04 = r8.Afq(this.A04, r9.A04);
                if (r8 == C463025i.A00) {
                    this.A00 |= r9.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        try {
                            int A03 = r82.A03();
                            if (A03 == 0) {
                                break;
                            } else if (A03 == 8) {
                                this.A00 |= 1;
                                this.A03 = r82.A02();
                            } else if (A03 == 16) {
                                this.A00 |= 2;
                                this.A01 = r82.A02();
                            } else if (A03 == 24) {
                                AbstractC41941uP r2 = this.A04;
                                if (!((AnonymousClass1K7) r2).A00) {
                                    r2 = AbstractC27091Fz.A0F(r2);
                                    this.A04 = r2;
                                }
                                C56822m0 r22 = (C56822m0) r2;
                                r22.A02(r22.A00, r82.A02());
                            } else if (A03 == 26) {
                                int A04 = r82.A04(r82.A02());
                                AbstractC41941uP r1 = this.A04;
                                if (!((AnonymousClass1K7) r1).A00 && r82.A00() > 0) {
                                    this.A04 = AbstractC27091Fz.A0F(r1);
                                }
                                while (r82.A00() > 0) {
                                    C56822m0 r23 = (C56822m0) this.A04;
                                    r23.A02(r23.A00, r82.A02());
                                }
                                r82.A03 = A04;
                                r82.A0B();
                            } else if (!A0a(r82, A03)) {
                                break;
                            }
                        } catch (C28971Pt e) {
                            e.unfinishedMessage = this;
                            throw new RuntimeException(e);
                        }
                    } catch (IOException e2) {
                        C28971Pt r12 = new C28971Pt(e2.getMessage());
                        r12.unfinishedMessage = this;
                        throw new RuntimeException(r12);
                    }
                }
                break;
            case 3:
                ((AnonymousClass1K7) this.A04).A00 = false;
                return null;
            case 4:
                return new C34311fw();
            case 5:
                return new C81723uT();
            case 6:
                break;
            case 7:
                if (A06 == null) {
                    synchronized (C34311fw.class) {
                        if (A06 == null) {
                            A06 = new AnonymousClass255(A05);
                        }
                    }
                }
                return A06;
            default:
                throw new UnsupportedOperationException();
        }
        return A05;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i;
        int i2;
        int i3 = ((AbstractC27091Fz) this).A00;
        if (i3 != -1) {
            return i3;
        }
        int i4 = this.A00;
        if ((i4 & 1) == 1) {
            i = CodedOutputStream.A04(1, this.A03) + 0;
        } else {
            i = 0;
        }
        if ((i4 & 2) == 2) {
            i += CodedOutputStream.A04(2, this.A01);
        }
        int i5 = 0;
        for (int i6 = 0; i6 < this.A04.size(); i6++) {
            C56822m0 r0 = (C56822m0) this.A04;
            r0.A01(i6);
            i5 += CodedOutputStream.A01(r0.A01[i6]);
        }
        int i7 = i + i5;
        if (!this.A04.isEmpty()) {
            int i8 = i7 + 1;
            if (i5 >= 0) {
                i2 = CodedOutputStream.A01(i5);
            } else {
                i2 = 10;
            }
            i7 = i8 + i2;
        }
        this.A02 = i5;
        int A00 = i7 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        AGd();
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0F(1, this.A03);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0F(2, this.A01);
        }
        if (this.A04.size() > 0) {
            codedOutputStream.A0C(26);
            codedOutputStream.A0C(this.A02);
        }
        for (int i = 0; i < this.A04.size(); i++) {
            C56822m0 r0 = (C56822m0) this.A04;
            r0.A01(i);
            codedOutputStream.A0C(r0.A01[i]);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
