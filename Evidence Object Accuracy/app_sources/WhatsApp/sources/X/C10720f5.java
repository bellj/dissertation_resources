package X;

/* renamed from: X.0f5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C10720f5 implements AnonymousClass5VI {
    public final /* synthetic */ AnonymousClass024 A00;

    public C10720f5(AnonymousClass024 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5VI
    public Object A9I(Object obj, AnonymousClass5WO r3) {
        this.A00.accept(obj);
        return AnonymousClass1WZ.A00;
    }
}
