package X;

import android.content.Context;
import android.database.Cursor;
import androidx.work.ListenableWorker;
import androidx.work.WorkerParameters;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemalarm.RescheduleReceiver;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Executor;

/* renamed from: X.0eH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC10250eH implements Runnable {
    public static final String A0J = C06390Tk.A01("WorkerWrapper");
    public Context A00;
    public C05180Oo A01;
    public AnonymousClass043 A02 = new AnonymousClass0GK();
    public ListenableWorker A03;
    public AnonymousClass0NN A04;
    public WorkDatabase A05;
    public AbstractC11450gJ A06;
    public AbstractC11980hB A07;
    public C004401z A08;
    public AbstractC12700iM A09;
    public AbstractC11990hC A0A;
    public AnonymousClass040 A0B = AnonymousClass040.A00();
    public AbstractC11500gO A0C;
    public AbstractFutureC44231yX A0D = null;
    public String A0E;
    public String A0F;
    public List A0G;
    public List A0H;
    public volatile boolean A0I;

    public RunnableC10250eH(AnonymousClass0O4 r2) {
        this.A00 = r2.A00;
        this.A0C = r2.A05;
        this.A06 = r2.A04;
        this.A0F = r2.A06;
        this.A0G = r2.A07;
        this.A04 = r2.A02;
        this.A03 = null;
        this.A01 = r2.A01;
        WorkDatabase workDatabase = r2.A03;
        this.A05 = workDatabase;
        this.A09 = workDatabase.A0B();
        this.A07 = this.A05.A06();
        this.A0A = this.A05.A0C();
    }

    public AbstractFutureC44231yX A00() {
        return this.A0B;
    }

    public void A01() {
        boolean z;
        this.A0I = true;
        A07();
        AbstractFutureC44231yX r0 = this.A0D;
        if (r0 != null) {
            z = r0.isDone();
            this.A0D.cancel(true);
        } else {
            z = false;
        }
        ListenableWorker listenableWorker = this.A03;
        if (listenableWorker == null || z) {
            C06390Tk.A00().A02(A0J, String.format("WorkSpec %s is already done. Not interrupting.", this.A08), new Throwable[0]);
            return;
        }
        listenableWorker.A04 = true;
        listenableWorker.A03();
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0215, code lost:
        if (r4.A00() == false) goto L_0x0217;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02() {
        /*
        // Method dump skipped, instructions count: 689
        */
        throw new UnsupportedOperationException("Method not decompiled: X.RunnableC10250eH.A02():void");
    }

    public void A03() {
        WorkDatabase workDatabase = this.A05;
        workDatabase.A03();
        try {
            String str = this.A0F;
            LinkedList linkedList = new LinkedList();
            linkedList.add(str);
            while (!linkedList.isEmpty()) {
                String str2 = (String) linkedList.remove();
                AbstractC12700iM r2 = this.A09;
                if (r2.AGv(str2) != EnumC03840Ji.CANCELLED) {
                    r2.Act(EnumC03840Ji.FAILED, str2);
                }
                linkedList.addAll(this.A07.ACW(str2));
            }
            this.A09.AcQ(((AnonymousClass0GK) this.A02).A00, str);
            workDatabase.A05();
        } finally {
            workDatabase.A04();
            A06(false);
        }
    }

    public final void A04() {
        AbstractC12700iM r0 = this.A09;
        String str = this.A0F;
        EnumC03840Ji AGv = r0.AGv(str);
        EnumC03840Ji r02 = EnumC03840Ji.RUNNING;
        C06390Tk A00 = C06390Tk.A00();
        String str2 = A0J;
        if (AGv == r02) {
            A00.A02(str2, String.format("Status for %s is RUNNING;not doing any work and rescheduling for later execution", str), new Throwable[0]);
            A06(true);
            return;
        }
        A00.A02(str2, String.format("Status for %s is %s; not doing any work", str, AGv), new Throwable[0]);
        A06(false);
    }

    /* JADX INFO: finally extract failed */
    public final void A05() {
        C06390Tk A00;
        String str;
        Object[] objArr;
        String str2;
        C006503b A002;
        if (!A07()) {
            WorkDatabase workDatabase = this.A05;
            workDatabase.A03();
            try {
                AbstractC12700iM r6 = this.A09;
                String str3 = this.A0F;
                C004401z AHn = r6.AHn(str3);
                this.A08 = AHn;
                int i = 0;
                if (AHn == null) {
                    C06390Tk.A00().A03(A0J, String.format("Didn't find WorkSpec for id %s", str3), new Throwable[0]);
                    A06(false);
                } else {
                    EnumC03840Ji r11 = AHn.A0D;
                    EnumC03840Ji r3 = EnumC03840Ji.ENQUEUED;
                    if (r11 != r3) {
                        A04();
                        workDatabase.A05();
                        C06390Tk.A00().A02(A0J, String.format("%s is not in ENQUEUED state. Nothing more to do.", this.A08.A0G), new Throwable[0]);
                    }
                    if (AHn.A04 != 0 || (r11 == r3 && AHn.A00 > 0)) {
                        long currentTimeMillis = System.currentTimeMillis();
                        if (AHn.A06 != 0 && currentTimeMillis < AHn.A00()) {
                            C06390Tk.A00().A02(A0J, String.format("Delaying execution for %s because it is being executed before schedule.", this.A08.A0G), new Throwable[0]);
                            A06(true);
                        }
                    }
                    workDatabase.A05();
                    workDatabase.A04();
                    C004401z r10 = this.A08;
                    if (r10.A04 != 0) {
                        A002 = r10.A0A;
                    } else {
                        String str4 = r10.A0F;
                        try {
                            AnonymousClass0S5 r12 = (AnonymousClass0S5) Class.forName(str4).newInstance();
                            if (r12 != null) {
                                ArrayList arrayList = new ArrayList();
                                arrayList.add(this.A08.A0A);
                                C07740a0 r1 = (C07740a0) r6;
                                AnonymousClass0ZJ A003 = AnonymousClass0ZJ.A00("SELECT output FROM workspec WHERE id IN (SELECT prerequisite_id FROM dependency WHERE work_spec_id=?)", 1);
                                if (str3 == null) {
                                    A003.A6T(1);
                                } else {
                                    A003.A6U(1, str3);
                                }
                                AnonymousClass0QN r0 = r1.A01;
                                r0.A02();
                                Cursor A004 = AnonymousClass0LC.A00(r0, A003, false);
                                try {
                                    ArrayList arrayList2 = new ArrayList(A004.getCount());
                                    while (A004.moveToNext()) {
                                        arrayList2.add(C006503b.A00(A004.getBlob(0)));
                                    }
                                    A004.close();
                                    A003.A01();
                                    arrayList.addAll(arrayList2);
                                    A002 = r12.A00(arrayList);
                                } catch (Throwable th) {
                                    A004.close();
                                    A003.A01();
                                    throw th;
                                }
                            }
                        } catch (Exception e) {
                            C06390Tk A005 = C06390Tk.A00();
                            String str5 = AnonymousClass0S5.A00;
                            StringBuilder sb = new StringBuilder("Trouble instantiating + ");
                            sb.append(str4);
                            A005.A03(str5, sb.toString(), e);
                        }
                        A00 = C06390Tk.A00();
                        str = A0J;
                        objArr = new Object[]{this.A08.A0F};
                        str2 = "Could not create Input Merger %s";
                        A00.A03(str, String.format(str2, objArr), new Throwable[i]);
                        A03();
                        return;
                    }
                    UUID fromString = UUID.fromString(str3);
                    List list = this.A0H;
                    AnonymousClass0NN r122 = this.A04;
                    int i2 = this.A08.A00;
                    C05180Oo r02 = this.A01;
                    Executor executor = r02.A05;
                    AbstractC11500gO r8 = this.A0C;
                    AnonymousClass0S6 r9 = r02.A04;
                    WorkerParameters workerParameters = new WorkerParameters(A002, new C07570Zh(workDatabase, this.A06, r8), new C07590Zj(workDatabase, r8), r9, r122, r8, list, fromString, executor, i2);
                    ListenableWorker listenableWorker = this.A03;
                    if (listenableWorker == null) {
                        listenableWorker = r9.A00(this.A00, workerParameters, this.A08.A0G);
                        this.A03 = listenableWorker;
                        if (listenableWorker == null) {
                            A00 = C06390Tk.A00();
                            str = A0J;
                            i = 0;
                            objArr = new Object[]{this.A08.A0G};
                            str2 = "Could not create Worker %s";
                            A00.A03(str, String.format(str2, objArr), new Throwable[i]);
                            A03();
                            return;
                        }
                    }
                    i = 0;
                    if (listenableWorker.A03) {
                        A00 = C06390Tk.A00();
                        str = A0J;
                        objArr = new Object[]{this.A08.A0G};
                        str2 = "Received an already-used Worker %s; WorkerFactory should return new instances";
                        A00.A03(str, String.format(str2, objArr), new Throwable[i]);
                        A03();
                        return;
                    }
                    listenableWorker.A03 = true;
                    workDatabase.A03();
                    try {
                        boolean z = true;
                        if (r6.AGv(str3) == r3) {
                            r6.Act(EnumC03840Ji.RUNNING, str3);
                            C07740a0 r62 = (C07740a0) r6;
                            AnonymousClass0QN r92 = r62.A01;
                            r92.A02();
                            AbstractC05330Pd r32 = r62.A03;
                            AbstractC12830ic A006 = r32.A00();
                            if (str3 == null) {
                                A006.A6T(1);
                            } else {
                                A006.A6U(1, str3);
                            }
                            r92.A03();
                            ((C02980Fp) A006).A00.executeUpdateDelete();
                            r92.A05();
                            r92.A04();
                            r32.A02(A006);
                        } else {
                            z = false;
                        }
                        workDatabase.A05();
                        if (!z) {
                            A04();
                            return;
                        } else if (!A07()) {
                            AnonymousClass040 A007 = AnonymousClass040.A00();
                            RunnableC10230eF r93 = new RunnableC10230eF(this.A00, workerParameters.A02, this.A03, this.A08, r8);
                            C07760a2 r82 = (C07760a2) r8;
                            Executor executor2 = r82.A02;
                            executor2.execute(r93);
                            AbstractFutureC44231yX A008 = r93.A00();
                            A008.A5i(new RunnableC09930dk(this, A007, A008), executor2);
                            A007.A5i(new RunnableC09940dl(this, A007, this.A0E), r82.A01);
                            return;
                        } else {
                            return;
                        }
                    } finally {
                    }
                }
                workDatabase.A05();
            } finally {
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public final void A06(boolean z) {
        ListenableWorker listenableWorker;
        WorkDatabase workDatabase = this.A05;
        workDatabase.A03();
        try {
            boolean z2 = false;
            AnonymousClass0ZJ A00 = AnonymousClass0ZJ.A00("SELECT COUNT(*) > 0 FROM workspec WHERE state NOT IN (2, 3, 5) LIMIT 1", 0);
            AnonymousClass0QN r0 = ((C07740a0) workDatabase.A0B()).A01;
            r0.A02();
            Cursor A002 = AnonymousClass0LC.A00(r0, A00, false);
            if (A002.moveToFirst() && A002.getInt(0) != 0) {
                z2 = true;
            }
            A002.close();
            A00.A01();
            if (!z2) {
                AnonymousClass0RF.A00(this.A00, RescheduleReceiver.class, false);
            }
            if (z) {
                AbstractC12700iM r3 = this.A09;
                EnumC03840Ji r1 = EnumC03840Ji.ENQUEUED;
                String str = this.A0F;
                r3.Act(r1, str);
                r3.AKt(str, -1);
            }
            if (!(this.A08 == null || (listenableWorker = this.A03) == null || !listenableWorker.A02())) {
                AbstractC11450gJ r32 = this.A06;
                String str2 = this.A0F;
                C07650Zp r33 = (C07650Zp) r32;
                synchronized (r33.A09) {
                    r33.A07.remove(str2);
                    r33.A01();
                }
            }
            workDatabase.A05();
            workDatabase.A04();
            this.A0B.A09(Boolean.valueOf(z));
        } catch (Throwable th) {
            workDatabase.A04();
            throw th;
        }
    }

    public final boolean A07() {
        if (!this.A0I) {
            return false;
        }
        C06390Tk.A00().A02(A0J, String.format("Work interrupted for %s", this.A0E), new Throwable[0]);
        EnumC03840Ji AGv = this.A09.AGv(this.A0F);
        if (AGv == null) {
            A06(false);
            return true;
        }
        A06(!AGv.A00());
        return true;
    }

    @Override // java.lang.Runnable
    public void run() {
        AbstractC11990hC r0 = this.A0A;
        String str = this.A0F;
        List<String> AH6 = r0.AH6(str);
        this.A0H = AH6;
        StringBuilder sb = new StringBuilder("Work [ id=");
        sb.append(str);
        sb.append(", tags={ ");
        boolean z = true;
        for (String str2 : AH6) {
            if (z) {
                z = false;
            } else {
                sb.append(", ");
            }
            sb.append(str2);
        }
        sb.append(" } ]");
        this.A0E = sb.toString();
        A05();
    }
}
