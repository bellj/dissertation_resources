package X;

/* renamed from: X.51d  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1092951d implements AnonymousClass5T3 {
    public AnonymousClass28D A00;
    public final AnonymousClass5T5 A01;

    public /* synthetic */ C1092951d(AnonymousClass5T5 r1) {
        this.A01 = r1;
    }

    @Override // X.AnonymousClass5T3
    public boolean Afk(AnonymousClass28D r2) {
        if (!this.A01.Aej(r2)) {
            return false;
        }
        this.A00 = r2;
        return true;
    }
}
