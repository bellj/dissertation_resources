package X;

import java.lang.reflect.Field;
import java.util.Arrays;

/* renamed from: X.4bL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C94154bL {
    public int A00;
    public int A01;
    public int A02 = Integer.MAX_VALUE;
    public int A03 = Integer.MIN_VALUE;
    public int A04 = 0;
    public int A05 = 0;
    public int A06 = 0;
    public int A07 = 0;
    public int A08;
    public int A09;
    public int A0A;
    public int A0B;
    public int A0C;
    public Class A0D;
    public Object A0E;
    public Object A0F;
    public Object A0G;
    public Field A0H;
    public final int A0I;
    public final int A0J;
    public final int A0K;
    public final int A0L;
    public final int A0M;
    public final int A0N;
    public final int A0O;
    public final int A0P;
    public final int A0Q;
    public final AnonymousClass4VG A0R;
    public final int[] A0S;
    public final Object[] A0T;

    public C94154bL(Class cls, String str, Object[] objArr) {
        this.A0D = cls;
        AnonymousClass4VG r4 = new AnonymousClass4VG(str);
        this.A0R = r4;
        this.A0T = objArr;
        this.A0I = r4.A00();
        int A00 = r4.A00();
        this.A0M = A00;
        int[] iArr = null;
        if (A00 != 0) {
            int A002 = r4.A00();
            this.A0N = A002;
            int A003 = r4.A00();
            this.A0J = r4.A00();
            this.A0K = r4.A00();
            this.A0P = r4.A00();
            this.A0L = r4.A00();
            this.A0O = r4.A00();
            this.A0Q = r4.A00();
            int A004 = r4.A00();
            this.A0S = A004 != 0 ? new int[A004] : iArr;
            this.A00 = (A002 << 1) + A003;
        }
    }

    public static Field A00(Class cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (NoSuchFieldException unused) {
            Field[] declaredFields = cls.getDeclaredFields();
            for (Field field : declaredFields) {
                if (str.equals(field.getName())) {
                    return field;
                }
            }
            String name = cls.getName();
            String arrays = Arrays.toString(declaredFields);
            StringBuilder A0t = C12980iv.A0t(C12970iu.A07(str) + 40 + name.length() + C12970iu.A07(arrays));
            A0t.append("Field ");
            A0t.append(str);
            A0t.append(" for ");
            A0t.append(name);
            A0t.append(" not found. Known fields are ");
            throw C12990iw.A0m(C12960it.A0d(arrays, A0t));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0096, code lost:
        if (r1 == (X.EnumC869349n.A02.id + 51)) goto L_0x0098;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x009b, code lost:
        if ((r19.A0I & 1) == 1) goto L_0x009d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x009d, code lost:
        r2 = r19.A0T;
        r1 = r19.A00;
        r19.A00 = r1 + 1;
        r19.A0F = r2[r1];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x010f, code lost:
        if ((r19.A09 & androidx.core.view.inputmethod.EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) != 0) goto L_0x009d;
     */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00aa  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A01() {
        /*
        // Method dump skipped, instructions count: 320
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C94154bL.A01():boolean");
    }
}
