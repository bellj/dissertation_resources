package X;

import android.graphics.Bitmap;
import android.util.SparseArray;
import java.util.concurrent.ExecutorService;

/* renamed from: X.4SI  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4SI {
    public final Bitmap.Config A00;
    public final SparseArray A01 = new SparseArray();
    public final C63693Co A02;
    public final AnonymousClass4UM A03;
    public final ExecutorService A04;

    public AnonymousClass4SI(Bitmap.Config config, C63693Co r3, AnonymousClass4UM r4, ExecutorService executorService) {
        this.A03 = r4;
        this.A02 = r3;
        this.A00 = config;
        this.A04 = executorService;
    }
}
