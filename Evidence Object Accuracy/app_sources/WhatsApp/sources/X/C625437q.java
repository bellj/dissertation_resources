package X;

import android.net.Uri;
import java.lang.ref.WeakReference;

/* renamed from: X.37q  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C625437q extends AbstractC16350or {
    public final int A00;
    public final int A01;
    public final Uri A02;
    public final C22190yg A03;
    public final WeakReference A04;

    public C625437q(Uri uri, AnonymousClass34P r3, C22190yg r4, int i, int i2) {
        this.A03 = r4;
        this.A02 = uri;
        this.A01 = i;
        this.A00 = i2;
        this.A04 = C12970iu.A10(r3);
    }
}
