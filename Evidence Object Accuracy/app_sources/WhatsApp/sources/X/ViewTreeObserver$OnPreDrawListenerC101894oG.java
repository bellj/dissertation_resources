package X;

import android.view.View;
import android.view.ViewTreeObserver;

/* renamed from: X.4oG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnPreDrawListenerC101894oG implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ View A00;

    public ViewTreeObserver$OnPreDrawListenerC101894oG(View view) {
        this.A00 = view;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        View view = this.A00;
        C12980iv.A1G(view, this);
        View findViewById = view.findViewById(16908335);
        if (findViewById != null) {
            AnonymousClass028.A0k(findViewById, "statusBar");
        }
        View findViewById2 = view.findViewById(16908336);
        if (findViewById2 == null) {
            return true;
        }
        AnonymousClass028.A0k(findViewById2, "navigationBar");
        return true;
    }
}
