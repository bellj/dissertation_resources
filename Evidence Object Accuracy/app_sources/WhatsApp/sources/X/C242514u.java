package X;

import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.UserJid;
import java.util.Map;

/* renamed from: X.14u  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C242514u {
    public final C21410xN A00;
    public final C18460sU A01;
    public final C16490p7 A02;

    public C242514u(C21410xN r1, C18460sU r2, C16490p7 r3) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    public AbstractC15340mz A00(UserJid userJid, AbstractC15340mz r6, Map map, long j) {
        AnonymousClass1IS r3 = r6.A0z;
        AnonymousClass1IS r1 = new AnonymousClass1IS(userJid, r3.A01, true);
        AnonymousClass009.A0F(r6 instanceof AbstractC16400ox);
        AnonymousClass009.A0F(r6.A0r);
        AbstractC15340mz A7M = ((AbstractC16400ox) r6).A7M(r1);
        A7M.A0l = null;
        A7M.A0e(r3.A00);
        map.put(DeviceJid.of(userJid), this.A00.A01(A7M, j));
        return A7M;
    }
}
