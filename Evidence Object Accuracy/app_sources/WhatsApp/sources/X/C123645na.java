package X;

import android.content.Context;
import com.whatsapp.R;
import com.whatsapp.payments.ui.widget.PaymentMethodRow;

/* renamed from: X.5na  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123645na extends PaymentMethodRow {
    public boolean A00;

    public C123645na(Context context) {
        super(context);
        A00();
    }

    @Override // com.whatsapp.payments.ui.widget.PaymentMethodRow
    public int getLayoutRes() {
        return R.layout.novi_payment_method_row;
    }
}
