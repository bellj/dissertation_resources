package X;

import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import org.json.JSONArray;
import org.json.JSONObject;

/* renamed from: X.0rP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17800rP implements AbstractC16940q0 {
    public final AbstractC16940q0 A00;

    public C17800rP(AbstractC16940q0 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC16940q0
    public /* bridge */ /* synthetic */ Object A7i(JSONObject jSONObject, long j) {
        JSONObject optJSONObject;
        JSONObject optJSONObject2;
        C44711zQ r6;
        Object A7i;
        try {
            HashSet hashSet = new HashSet();
            Collections.addAll(hashSet, "xwa_product_catalog_get_product_catalog");
            if (AnonymousClass3GH.A01(hashSet, jSONObject) && (optJSONObject = jSONObject.optJSONObject("xwa_product_catalog_get_product_catalog")) != null) {
                HashSet hashSet2 = new HashSet();
                Collections.addAll(hashSet2, "product_catalog");
                if (AnonymousClass3GH.A01(hashSet2, optJSONObject) && (optJSONObject2 = optJSONObject.optJSONObject("product_catalog")) != null) {
                    boolean z = true;
                    HashSet hashSet3 = new HashSet();
                    Collections.addAll(hashSet3, "products");
                    if (AnonymousClass3GH.A01(hashSet3, optJSONObject2)) {
                        JSONArray optJSONArray = optJSONObject2.optJSONArray("products");
                        if (optJSONArray == null) {
                            return new C90114Mq(null, false);
                        }
                        ArrayList arrayList = new ArrayList();
                        for (int i = 0; i < optJSONArray.length(); i++) {
                            JSONObject optJSONObject3 = optJSONArray.optJSONObject(i);
                            if (!(optJSONObject3 == null || (A7i = this.A00.A7i(optJSONObject3, j)) == null)) {
                                arrayList.add(A7i);
                            }
                        }
                        HashSet hashSet4 = new HashSet();
                        Collections.addAll(hashSet4, "paging");
                        if (!AnonymousClass3GH.A01(hashSet4, optJSONObject2)) {
                            r6 = new C44711zQ(null, false);
                        } else {
                            String A00 = AnonymousClass3GH.A00("after", optJSONObject2.optJSONObject("paging"));
                            r6 = new C44711zQ(A00, !TextUtils.isEmpty(A00));
                        }
                        String A002 = AnonymousClass3GH.A00("cart_enabled", optJSONObject2);
                        if (TextUtils.isEmpty(A002) || !A002.equals("CARTENABLED_TRUE")) {
                            z = false;
                        }
                        return new C90114Mq(new C44721zR(r6, arrayList), z);
                    }
                }
            }
            return new C90114Mq(null, false);
        } catch (Exception e) {
            Log.e("GetProductCatalogGraphQLResponseConverter/convert/Could not create GetProductCatalogPageResult from GetProductCatalog GraphQL response", e);
            return new C90114Mq(null, false);
        }
    }
}
