package X;

import android.text.TextUtils;

/* renamed from: X.1Pg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28851Pg extends C28861Ph implements AbstractC28871Pi, AbstractC16400ox, AbstractC16410oy, AbstractC16420oz {
    public C28891Pk A00;

    public C28851Pg(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 27, j);
    }

    public C28851Pg(AnonymousClass1IS r8, C28851Pg r9, long j) {
        super(r8, r9, j, true);
        this.A00 = r9.A00.A00();
    }

    public String A18() {
        if (TextUtils.isEmpty(A0I())) {
            return this.A00.A01;
        }
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder("*");
        sb2.append(A0I());
        sb2.append("*");
        sb.append(sb2.toString());
        sb.append("\n");
        sb.append(this.A00.A01);
        return sb.toString();
    }

    @Override // X.AbstractC28871Pi
    public String ADA() {
        C28891Pk r1;
        String str;
        if (!TextUtils.isEmpty(A0I())) {
            StringBuilder sb = new StringBuilder();
            sb.append(A0I());
            sb.append(" ");
            r1 = this.A00;
            sb.append(r1.A01);
            str = sb.toString();
        } else {
            r1 = this.A00;
            str = r1.A01;
        }
        if (TextUtils.isEmpty(r1.A02)) {
            return str;
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append(str);
        sb2.append(" ");
        sb2.append(this.A00.A02);
        return sb2.toString();
    }

    @Override // X.AbstractC28871Pi
    public String AEh(AnonymousClass018 r2) {
        return !TextUtils.isEmpty(A0I()) ? A0I() : this.A00.A01;
    }

    @Override // X.AbstractC28871Pi
    public String AFs() {
        return !TextUtils.isEmpty(A0I()) ? A0I() : this.A00.A01;
    }

    @Override // X.AbstractC28871Pi
    public String AG2() {
        return A18();
    }

    @Override // X.AbstractC28871Pi
    public C28891Pk AH7() {
        return this.A00;
    }

    @Override // X.AbstractC28871Pi
    public void Acz(C28891Pk r1) {
        this.A00 = r1;
    }
}
