package X;

/* renamed from: X.1iZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C35721iZ extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public String A02;

    public C35721iZ() {
        super(2314, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(2, this.A00);
        r3.Abe(1, this.A02);
        r3.Abe(3, this.A01);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        StringBuilder sb = new StringBuilder("WamSpamBlockAction {");
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "spamBlockActionType", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "spamBlockBusinessJid", this.A02);
        Integer num2 = this.A01;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "spamBlockEntryPoint", obj2);
        sb.append("}");
        return sb.toString();
    }
}
