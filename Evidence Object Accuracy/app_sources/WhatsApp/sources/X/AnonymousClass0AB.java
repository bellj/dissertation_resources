package X;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Outline;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;

/* renamed from: X.0AB  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass0AB extends Drawable implements Drawable.Callback {
    public int A00 = 255;
    public int A01 = -1;
    public long A02;
    public long A03;
    public Rect A04;
    public Drawable A05;
    public Drawable A06;
    public AnonymousClass0VC A07;
    public AnonymousClass09z A08;
    public Runnable A09;
    public boolean A0A;
    public boolean A0B;

    public abstract AnonymousClass09z A03();

    public final void A00(Drawable drawable) {
        AnonymousClass0VC r1 = this.A07;
        if (r1 == null) {
            r1 = new AnonymousClass0VC();
            this.A07 = r1;
        }
        r1.A00 = drawable.getCallback();
        drawable.setCallback(r1);
        try {
            if (this.A08.A07 <= 0 && this.A0A) {
                drawable.setAlpha(this.A00);
            }
            AnonymousClass09z r12 = this.A08;
            if (r12.A0R) {
                drawable.setColorFilter(r12.A0E);
            } else {
                if (r12.A0S) {
                    C015607k.A04(r12.A0C, drawable);
                }
                AnonymousClass09z r13 = this.A08;
                if (r13.A0T) {
                    C015607k.A07(r13.A0F, drawable);
                }
            }
            drawable.setVisible(isVisible(), true);
            drawable.setDither(this.A08.A0Q);
            drawable.setState(getState());
            drawable.setLevel(getLevel());
            drawable.setBounds(getBounds());
            int i = Build.VERSION.SDK_INT;
            if (i >= 23) {
                C015607k.A0D(C015607k.A01(this), drawable);
            }
            if (i >= 19) {
                C015607k.A0C(drawable, this.A08.A0I);
            }
            Rect rect = this.A04;
            if (i >= 21 && rect != null) {
                C015607k.A0B(drawable, rect.left, rect.top, rect.right, rect.bottom);
            }
        } finally {
            AnonymousClass0VC r2 = this.A07;
            Drawable.Callback callback = r2.A00;
            r2.A00 = null;
            drawable.setCallback(callback);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0026  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x003b A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:26:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(boolean r14) {
        /*
            r13 = this;
            r8 = 1
            r13.A0A = r8
            long r1 = android.os.SystemClock.uptimeMillis()
            android.graphics.drawable.Drawable r9 = r13.A05
            r11 = 255(0xff, double:1.26E-321)
            r7 = 0
            r3 = 0
            if (r9 == 0) goto L_0x001f
            long r5 = r13.A02
            int r0 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
            if (r0 == 0) goto L_0x0021
            int r0 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            if (r0 > 0) goto L_0x0057
            int r0 = r13.A00
            r9.setAlpha(r0)
        L_0x001f:
            r13.A02 = r3
        L_0x0021:
            r10 = 0
        L_0x0022:
            android.graphics.drawable.Drawable r9 = r13.A06
            if (r9 == 0) goto L_0x0036
            long r5 = r13.A03
            int r0 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
            if (r0 == 0) goto L_0x0038
            int r0 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            if (r0 > 0) goto L_0x0046
            r9.setVisible(r7, r7)
            r0 = 0
            r13.A06 = r0
        L_0x0036:
            r13.A03 = r3
        L_0x0038:
            r8 = r10
        L_0x0039:
            if (r14 == 0) goto L_0x0045
            if (r8 == 0) goto L_0x0045
            java.lang.Runnable r0 = r13.A09
            r3 = 16
            long r1 = r1 + r3
            r13.scheduleSelf(r0, r1)
        L_0x0045:
            return
        L_0x0046:
            long r5 = r5 - r1
            long r5 = r5 * r11
            int r3 = (int) r5
            X.09z r0 = r13.A08
            int r0 = r0.A08
            int r3 = r3 / r0
            int r0 = r13.A00
            int r3 = r3 * r0
            int r0 = r3 / 255
            r9.setAlpha(r0)
            goto L_0x0039
        L_0x0057:
            long r5 = r5 - r1
            long r5 = r5 * r11
            int r10 = (int) r5
            X.09z r0 = r13.A08
            int r0 = r0.A07
            int r10 = r10 / r0
            int r5 = 255 - r10
            int r0 = r13.A00
            int r5 = r5 * r0
            int r0 = r5 / 255
            r9.setAlpha(r0)
            r10 = 1
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0AB.A01(boolean):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0066  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A02(int r9) {
        /*
            r8 = this;
            int r0 = r8.A01
            r3 = 0
            if (r9 != r0) goto L_0x0006
            return r3
        L_0x0006:
            long r5 = android.os.SystemClock.uptimeMillis()
            X.09z r0 = r8.A08
            int r0 = r0.A08
            r7 = 0
            r1 = 0
            if (r0 <= 0) goto L_0x0075
            android.graphics.drawable.Drawable r0 = r8.A06
            if (r0 == 0) goto L_0x001a
            r0.setVisible(r3, r3)
        L_0x001a:
            android.graphics.drawable.Drawable r0 = r8.A05
            if (r0 == 0) goto L_0x0070
            r8.A06 = r0
            X.09z r0 = r8.A08
            int r0 = r0.A08
            long r3 = (long) r0
            long r3 = r3 + r5
            r8.A03 = r3
        L_0x0028:
            if (r9 < 0) goto L_0x006a
            X.09z r3 = r8.A08
            int r0 = r3.A0A
            if (r9 >= r0) goto L_0x006a
            android.graphics.drawable.Drawable r7 = r3.A01(r9)
            r8.A05 = r7
            r8.A01 = r9
            if (r7 == 0) goto L_0x0047
            X.09z r0 = r8.A08
            int r0 = r0.A07
            if (r0 <= 0) goto L_0x0044
            long r3 = (long) r0
            long r5 = r5 + r3
            r8.A02 = r5
        L_0x0044:
            r8.A00(r7)
        L_0x0047:
            long r3 = r8.A02
            r5 = 1
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x0054
            long r3 = r8.A03
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x0062
        L_0x0054:
            java.lang.Runnable r0 = r8.A09
            if (r0 != 0) goto L_0x0066
            X.0cC r0 = new X.0cC
            r0.<init>(r8)
            r8.A09 = r0
        L_0x005f:
            r8.A01(r5)
        L_0x0062:
            r8.invalidateSelf()
            return r5
        L_0x0066:
            r8.unscheduleSelf(r0)
            goto L_0x005f
        L_0x006a:
            r8.A05 = r7
            r0 = -1
            r8.A01 = r0
            goto L_0x0047
        L_0x0070:
            r8.A06 = r7
            r8.A03 = r1
            goto L_0x0028
        L_0x0075:
            android.graphics.drawable.Drawable r0 = r8.A05
            if (r0 == 0) goto L_0x0028
            r0.setVisible(r3, r3)
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0AB.A02(int):boolean");
    }

    public void A04(AnonymousClass09z r2) {
        this.A08 = r2;
        int i = this.A01;
        if (i >= 0) {
            Drawable A01 = r2.A01(i);
            this.A05 = A01;
            if (A01 != null) {
                A00(A01);
            }
        }
        this.A06 = null;
    }

    @Override // android.graphics.drawable.Drawable
    public void applyTheme(Resources.Theme theme) {
        AnonymousClass09z r5 = this.A08;
        if (theme != null) {
            r5.A03();
            int i = r5.A0A;
            Drawable[] drawableArr = r5.A0X;
            for (int i2 = 0; i2 < i; i2++) {
                if (drawableArr[i2] != null && C015607k.A0E(drawableArr[i2])) {
                    C015607k.A06(theme, drawableArr[i2]);
                    r5.A01 |= drawableArr[i2].getChangingConfigurations();
                }
            }
            r5.A06(C06270Sv.A00(theme));
        }
    }

    @Override // android.graphics.drawable.Drawable
    public boolean canApplyTheme() {
        return this.A08.canApplyTheme();
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        Drawable drawable = this.A05;
        if (drawable != null) {
            drawable.draw(canvas);
        }
        Drawable drawable2 = this.A06;
        if (drawable2 != null) {
            drawable2.draw(canvas);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public int getAlpha() {
        return this.A00;
    }

    @Override // android.graphics.drawable.Drawable
    public int getChangingConfigurations() {
        int changingConfigurations = super.getChangingConfigurations();
        AnonymousClass09z r0 = this.A08;
        return changingConfigurations | r0.A00 | r0.A01;
    }

    @Override // android.graphics.drawable.Drawable
    public final Drawable.ConstantState getConstantState() {
        AnonymousClass09z r6 = this.A08;
        if (!r6.A0L) {
            r6.A03();
            r6.A0L = true;
            int i = r6.A0A;
            Drawable[] drawableArr = r6.A0X;
            for (int i2 = 0; i2 < i; i2++) {
                if (drawableArr[i2].getConstantState() == null) {
                    r6.A0J = false;
                    return null;
                }
            }
            r6.A0J = true;
        } else if (!r6.A0J) {
            return null;
        }
        this.A08.A00 = getChangingConfigurations();
        return this.A08;
    }

    @Override // android.graphics.drawable.Drawable
    public Drawable getCurrent() {
        return this.A05;
    }

    @Override // android.graphics.drawable.Drawable
    public void getHotspotBounds(Rect rect) {
        Rect rect2 = this.A04;
        if (rect2 != null) {
            rect.set(rect2);
        } else {
            super.getHotspotBounds(rect);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicHeight() {
        AnonymousClass09z r1 = this.A08;
        if (r1.A0P) {
            if (!r1.A0K) {
                r1.A02();
            }
            return r1.A02;
        }
        Drawable drawable = this.A05;
        if (drawable != null) {
            return drawable.getIntrinsicHeight();
        }
        return -1;
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicWidth() {
        AnonymousClass09z r1 = this.A08;
        if (r1.A0P) {
            if (!r1.A0K) {
                r1.A02();
            }
            return r1.A05;
        }
        Drawable drawable = this.A05;
        if (drawable != null) {
            return drawable.getIntrinsicWidth();
        }
        return -1;
    }

    @Override // android.graphics.drawable.Drawable
    public int getMinimumHeight() {
        AnonymousClass09z r1 = this.A08;
        if (r1.A0P) {
            if (!r1.A0K) {
                r1.A02();
            }
            return r1.A03;
        }
        Drawable drawable = this.A05;
        if (drawable != null) {
            return drawable.getMinimumHeight();
        }
        return 0;
    }

    @Override // android.graphics.drawable.Drawable
    public int getMinimumWidth() {
        AnonymousClass09z r1 = this.A08;
        if (r1.A0P) {
            if (!r1.A0K) {
                r1.A02();
            }
            return r1.A04;
        }
        Drawable drawable = this.A05;
        if (drawable != null) {
            return drawable.getMinimumWidth();
        }
        return 0;
    }

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        int i;
        Drawable drawable = this.A05;
        if (drawable == null || !drawable.isVisible()) {
            return -2;
        }
        AnonymousClass09z r5 = this.A08;
        if (r5.A0M) {
            return r5.A0B;
        }
        r5.A03();
        int i2 = r5.A0A;
        Drawable[] drawableArr = r5.A0X;
        if (i2 > 0) {
            i = drawableArr[0].getOpacity();
        } else {
            i = -2;
        }
        for (int i3 = 1; i3 < i2; i3++) {
            i = Drawable.resolveOpacity(i, drawableArr[i3].getOpacity());
        }
        r5.A0B = i;
        r5.A0M = true;
        return i;
    }

    @Override // android.graphics.drawable.Drawable
    public void getOutline(Outline outline) {
        Drawable drawable = this.A05;
        if (drawable != null) {
            C06270Sv.A01(outline, drawable);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0065, code lost:
        if (r6 != null) goto L_0x0067;
     */
    @Override // android.graphics.drawable.Drawable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean getPadding(android.graphics.Rect r10) {
        /*
            r9 = this;
            X.09z r7 = r9.A08
            boolean r0 = r7.A0W
            r6 = 0
            if (r0 != 0) goto L_0x0051
            android.graphics.Rect r2 = r7.A0G
            if (r2 != 0) goto L_0x0067
            boolean r0 = r7.A0N
            if (r0 != 0) goto L_0x0051
            r7.A03()
            android.graphics.Rect r8 = new android.graphics.Rect
            r8.<init>()
            int r5 = r7.A0A
            android.graphics.drawable.Drawable[] r4 = r7.A0X
            r3 = 0
            r2 = 0
        L_0x001d:
            if (r2 >= r5) goto L_0x005f
            r0 = r4[r2]
            boolean r0 = r0.getPadding(r8)
            if (r0 == 0) goto L_0x004e
            if (r6 != 0) goto L_0x002e
            android.graphics.Rect r6 = new android.graphics.Rect
            r6.<init>(r3, r3, r3, r3)
        L_0x002e:
            int r1 = r8.left
            int r0 = r6.left
            if (r1 <= r0) goto L_0x0036
            r6.left = r1
        L_0x0036:
            int r1 = r8.top
            int r0 = r6.top
            if (r1 <= r0) goto L_0x003e
            r6.top = r1
        L_0x003e:
            int r1 = r8.right
            int r0 = r6.right
            if (r1 <= r0) goto L_0x0046
            r6.right = r1
        L_0x0046:
            int r1 = r8.bottom
            int r0 = r6.bottom
            if (r1 <= r0) goto L_0x004e
            r6.bottom = r1
        L_0x004e:
            int r2 = r2 + 1
            goto L_0x001d
        L_0x0051:
            android.graphics.drawable.Drawable r0 = r9.A05
            if (r0 == 0) goto L_0x005a
            boolean r2 = r0.getPadding(r10)
            goto L_0x0079
        L_0x005a:
            boolean r2 = super.getPadding(r10)
            goto L_0x0079
        L_0x005f:
            r0 = 1
            r7.A0N = r0
            r7.A0G = r6
            r2 = r6
            if (r6 == 0) goto L_0x0051
        L_0x0067:
            r10.set(r2)
            int r1 = r2.left
            int r0 = r2.top
            r1 = r1 | r0
            int r0 = r2.bottom
            r1 = r1 | r0
            int r0 = r2.right
            r0 = r0 | r1
            r2 = 0
            if (r0 == 0) goto L_0x0079
            r2 = 1
        L_0x0079:
            X.09z r0 = r9.A08
            boolean r0 = r0.A0I
            r1 = 1
            if (r0 == 0) goto L_0x008e
            int r0 = X.C015607k.A01(r9)
            if (r0 != r1) goto L_0x008e
            int r1 = r10.left
            int r0 = r10.right
            r10.left = r0
            r10.right = r1
        L_0x008e:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0AB.getPadding(android.graphics.Rect):boolean");
    }

    @Override // android.graphics.drawable.Drawable.Callback
    public void invalidateDrawable(Drawable drawable) {
        AnonymousClass09z r1 = this.A08;
        if (r1 != null) {
            r1.A0M = false;
            r1.A0O = false;
        }
        if (drawable == this.A05 && getCallback() != null) {
            getCallback().invalidateDrawable(this);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public boolean isAutoMirrored() {
        return this.A08.A0I;
    }

    @Override // android.graphics.drawable.Drawable
    public void jumpToCurrentState() {
        boolean z;
        Drawable drawable = this.A06;
        if (drawable != null) {
            drawable.jumpToCurrentState();
            this.A06 = null;
            z = true;
        } else {
            z = false;
        }
        Drawable drawable2 = this.A05;
        if (drawable2 != null) {
            drawable2.jumpToCurrentState();
            if (this.A0A) {
                this.A05.setAlpha(this.A00);
            }
        }
        if (this.A03 != 0) {
            this.A03 = 0;
            z = true;
        }
        if (this.A02 != 0) {
            this.A02 = 0;
        } else if (!z) {
            return;
        }
        invalidateSelf();
    }

    @Override // android.graphics.drawable.Drawable
    public Drawable mutate() {
        if (!this.A0B && super.mutate() == this) {
            AnonymousClass09z A03 = A03();
            A03.A04();
            A04(A03);
            this.A0B = true;
        }
        return this;
    }

    @Override // android.graphics.drawable.Drawable
    public void onBoundsChange(Rect rect) {
        Drawable drawable = this.A06;
        if (drawable != null) {
            drawable.setBounds(rect);
        }
        Drawable drawable2 = this.A05;
        if (drawable2 != null) {
            drawable2.setBounds(rect);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public boolean onLayoutDirectionChanged(int i) {
        return this.A08.A07(i, this.A01);
    }

    @Override // android.graphics.drawable.Drawable
    public boolean onLevelChange(int i) {
        Drawable drawable = this.A06;
        if (drawable == null && (drawable = this.A05) == null) {
            return false;
        }
        return drawable.setLevel(i);
    }

    @Override // android.graphics.drawable.Drawable
    public boolean onStateChange(int[] iArr) {
        Drawable drawable = this.A06;
        if (drawable == null && (drawable = this.A05) == null) {
            return false;
        }
        return drawable.setState(iArr);
    }

    @Override // android.graphics.drawable.Drawable.Callback
    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        if (drawable == this.A05 && getCallback() != null) {
            getCallback().scheduleDrawable(this, runnable, j);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        if (!this.A0A || this.A00 != i) {
            this.A0A = true;
            this.A00 = i;
            Drawable drawable = this.A05;
            if (drawable == null) {
                return;
            }
            if (this.A02 == 0) {
                drawable.setAlpha(i);
            } else {
                A01(false);
            }
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setAutoMirrored(boolean z) {
        AnonymousClass09z r1 = this.A08;
        if (r1.A0I != z) {
            r1.A0I = z;
            Drawable drawable = this.A05;
            if (drawable != null) {
                C015607k.A0C(drawable, z);
            }
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
        AnonymousClass09z r1 = this.A08;
        r1.A0R = true;
        if (r1.A0E != colorFilter) {
            r1.A0E = colorFilter;
            Drawable drawable = this.A05;
            if (drawable != null) {
                drawable.setColorFilter(colorFilter);
            }
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setDither(boolean z) {
        AnonymousClass09z r1 = this.A08;
        if (r1.A0Q != z) {
            r1.A0Q = z;
            Drawable drawable = this.A05;
            if (drawable != null) {
                drawable.setDither(z);
            }
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setHotspot(float f, float f2) {
        Drawable drawable = this.A05;
        if (drawable != null) {
            C015607k.A09(drawable, f, f2);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setHotspotBounds(int i, int i2, int i3, int i4) {
        Rect rect = this.A04;
        if (rect == null) {
            this.A04 = new Rect(i, i2, i3, i4);
        } else {
            rect.set(i, i2, i3, i4);
        }
        Drawable drawable = this.A05;
        if (drawable != null) {
            C015607k.A0B(drawable, i, i2, i3, i4);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setTintList(ColorStateList colorStateList) {
        AnonymousClass09z r1 = this.A08;
        r1.A0S = true;
        if (r1.A0C != colorStateList) {
            r1.A0C = colorStateList;
            C015607k.A04(colorStateList, this.A05);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setTintMode(PorterDuff.Mode mode) {
        AnonymousClass09z r1 = this.A08;
        r1.A0T = true;
        if (r1.A0F != mode) {
            r1.A0F = mode;
            C015607k.A07(mode, this.A05);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public boolean setVisible(boolean z, boolean z2) {
        boolean visible = super.setVisible(z, z2);
        Drawable drawable = this.A06;
        if (drawable != null) {
            drawable.setVisible(z, z2);
        }
        Drawable drawable2 = this.A05;
        if (drawable2 != null) {
            drawable2.setVisible(z, z2);
        }
        return visible;
    }

    @Override // android.graphics.drawable.Drawable.Callback
    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        if (drawable == this.A05 && getCallback() != null) {
            getCallback().unscheduleDrawable(this, runnable);
        }
    }
}
