package X;

import java.io.InputStream;

/* renamed from: X.0lW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14490lW implements AbstractC14500lX {
    public final C14370lK A00;

    public C14490lW(C14370lK r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC14500lX
    public AbstractC37941nG ACi(byte[] bArr) {
        return new AbstractC37941nG(bArr) { // from class: X.565
            public final /* synthetic */ byte[] A01;

            {
                this.A01 = r2;
            }

            @Override // X.AbstractC37941nG
            public final InputStream A9M(InputStream inputStream) {
                C14490lW r0 = C14490lW.this;
                return new C629439g(new AnonymousClass2QH(r0.A00).A8r(this.A01), inputStream);
            }
        };
    }

    @Override // X.AbstractC14500lX
    public AnonymousClass2QI ADk() {
        return new AnonymousClass2QH(this.A00);
    }
}
