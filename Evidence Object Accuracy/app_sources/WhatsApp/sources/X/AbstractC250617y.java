package X;

import android.app.Application;

/* renamed from: X.17y  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC250617y implements AbstractC17340qe {
    public static Application A00(AnonymousClass01X r1) {
        Application A00 = AnonymousClass1HD.A00(r1.A00);
        if (A00 != null) {
            return A00;
        }
        throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }
}
