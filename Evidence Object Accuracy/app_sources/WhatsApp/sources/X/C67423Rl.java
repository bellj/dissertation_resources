package X;

/* renamed from: X.3Rl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67423Rl implements AbstractC009404s {
    public final C14900mE A00;
    public final C21220x4 A01;
    public final C18640sm A02;
    public final C14820m6 A03;
    public final C14840m8 A04;
    public final AbstractC14440lR A05;
    public final boolean A06;
    public final boolean A07;

    public C67423Rl(C14900mE r1, C21220x4 r2, C18640sm r3, C14820m6 r4, C14840m8 r5, AbstractC14440lR r6, boolean z, boolean z2) {
        this.A00 = r1;
        this.A05 = r6;
        this.A04 = r5;
        this.A02 = r3;
        this.A03 = r4;
        this.A01 = r2;
        this.A06 = z;
        this.A07 = z2;
    }

    @Override // X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        C14900mE r1 = this.A00;
        AbstractC14440lR r6 = this.A05;
        C14840m8 r5 = this.A04;
        return new C53892fb(r1, this.A01, this.A02, this.A03, r5, r6, this.A06, this.A07);
    }
}
