package X;

/* renamed from: X.5L1  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5L1 extends AbstractC114145Kj {
    public final Object A00;
    public final C114295Ky A01;
    public final AnonymousClass5F3 A02;
    public final C10710f4 A03;

    public AnonymousClass5L1(Object obj, C114295Ky r2, AnonymousClass5F3 r3, C10710f4 r4) {
        this.A03 = r4;
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = obj;
    }

    @Override // X.AnonymousClass5LF
    public void A0A(Throwable th) {
        C10710f4 r3 = this.A03;
        AnonymousClass5F3 r2 = this.A02;
        C10710f4.A06(this.A00, this.A01, r2, r3);
    }

    @Override // X.AnonymousClass1J7
    public /* bridge */ /* synthetic */ Object AJ4(Object obj) {
        A0A((Throwable) obj);
        return AnonymousClass1WZ.A00;
    }
}
