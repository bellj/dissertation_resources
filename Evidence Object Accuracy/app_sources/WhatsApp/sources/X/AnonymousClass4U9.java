package X;

/* renamed from: X.4U9  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4U9 {
    public int A00;
    public int A01;
    public C95474dn A02;
    public C95474dn A03;
    public C95474dn A04;
    public C95474dn A05;
    public C94914ck A06;
    public AnonymousClass4U9 A07 = null;
    public final int A08;
    public final int A09;
    public final int A0A;
    public final AnonymousClass4YW A0B;

    public AnonymousClass4U9(Object obj, String str, String str2, String str3, AnonymousClass4YW r6, int i) {
        this.A0B = r6;
        this.A08 = i;
        this.A0A = r6.A02(str);
        this.A09 = r6.A02(str2);
        if (str3 != null) {
            this.A01 = r6.A02(str3);
        }
        if (obj != null) {
            this.A00 = r6.A09(obj).A03;
        }
    }
}
