package X;

/* renamed from: X.0xY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21520xY {
    public final AbstractC15710nm A00;
    public final C17690rE A01;
    public final C21540xa A02;
    public final C17180qO A03;
    public final C21510xX A04;
    public final C16630pM A05;
    public final String A06 = "ctwa_ads_customer_data_migrated";

    public C21520xY(AbstractC15710nm r2, C17690rE r3, C21540xa r4, C17180qO r5, C21510xX r6, C16630pM r7) {
        this.A03 = r5;
        this.A05 = r7;
        this.A00 = r2;
        this.A04 = r6;
        this.A01 = r3;
        this.A02 = r4;
    }

    public boolean A00() {
        return this.A05.A01("ctwa_logging_v2_migration").getBoolean(this.A06, false);
    }
}
