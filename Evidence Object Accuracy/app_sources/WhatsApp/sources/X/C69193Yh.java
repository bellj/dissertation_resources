package X;

import com.facebook.redex.RunnableBRunnable0Shape9S0100000_I0_9;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/* renamed from: X.3Yh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C69193Yh implements AnonymousClass5WA {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AnonymousClass5UY A01;
    public final /* synthetic */ AnonymousClass5UZ A02;
    public final /* synthetic */ AnonymousClass17Q A03;
    public final /* synthetic */ String A04;
    public final /* synthetic */ Map A05;

    public C69193Yh(AnonymousClass5UY r1, AnonymousClass5UZ r2, AnonymousClass17Q r3, String str, Map map, int i) {
        this.A03 = r3;
        this.A00 = i;
        this.A01 = r1;
        this.A02 = r2;
        this.A05 = map;
        this.A04 = str;
    }

    @Override // X.AnonymousClass5WA
    public void AQE(AnonymousClass3FA r5) {
        C16700pc.A0E(r5, 0);
        AnonymousClass17Q r3 = this.A03;
        int i = this.A00;
        Long valueOf = Long.valueOf(r5.A00);
        AnonymousClass17P r0 = r3.A07;
        AnonymousClass17Q.A00(r0, r3, valueOf, i);
        r0.A02.A05(i, 467);
        C19750uc r2 = r3.A09;
        String str = r3.A0B;
        synchronized (r2) {
            C16700pc.A0E(str, 0);
            r2.A00.remove(str);
        }
        AnonymousClass5UZ r22 = this.A02;
        if (r22 != null) {
            C17520qw r02 = new C17520qw(AnonymousClass49R.A00.key, r5);
            Map singletonMap = Collections.singletonMap(r02.first, r02.second);
            C16700pc.A0B(singletonMap);
            r22.AQd(singletonMap);
        }
    }

    @Override // X.AnonymousClass5WA
    public void AQF(AnonymousClass3EI r30) {
        AnonymousClass17Q r10 = this.A03;
        int i = this.A00;
        AnonymousClass17P r8 = r10.A07;
        AnonymousClass1Q5 r7 = r8.A02;
        r7.A03(i, "iqResponse");
        AnonymousClass5UY r11 = this.A01;
        AnonymousClass5UZ r14 = this.A02;
        Object obj = this.A05;
        String str = this.A04;
        AnonymousClass17K r1 = r10.A03;
        String A04 = r10.A04();
        AnonymousClass17N r0 = r10.A0A;
        String A00 = r0.A00();
        String str2 = r10.A0B;
        AnonymousClass17O r15 = r10.A08;
        AnonymousClass01J r02 = r1.A00.A01;
        AnonymousClass18J r3 = (AnonymousClass18J) r02.A7T.get();
        C16210od r2 = (C16210od) r02.A0Y.get();
        C19630uQ r12 = (C19630uQ) r02.AFf.get();
        r10.A00 = new C27661Io(r2, r3, (AnonymousClass18K) r02.A7U.get(), C12970iu.A0R(r02), (AnonymousClass17M) r02.AFd.get(), r12, r02.A3m(), r15, (C17120qI) r02.ALs.get(), A04, A00, str2);
        if (r11 != null) {
            r11.AQc(r10.A02().A0L);
        }
        r10.A07(r30);
        AbstractC17770rM AGE = r10.A02().A0F.AGE("open_bloks_screen_graphql");
        AnonymousClass3HE r32 = r30.A01;
        AnonymousClass3E4 r03 = r32.A05;
        if (r03 != null) {
            AnonymousClass3EK r5 = r03.A01;
            String str3 = r5.A01;
            C16700pc.A0B(str3);
            AnonymousClass49S r04 = AnonymousClass49S.A01;
            C17520qw r13 = new C17520qw(r04.key, str);
            Map singletonMap = Collections.singletonMap(r13.first, r13.second);
            C16700pc.A0B(singletonMap);
            if (obj == null) {
                obj = C71373cp.A00;
            }
            LinkedHashMap linkedHashMap = new LinkedHashMap(singletonMap);
            linkedHashMap.putAll(obj);
            C27661Io A02 = r10.A02();
            AnonymousClass4O6 r132 = new AnonymousClass4O6(r14, r10);
            A02.A0C.A0H(new RunnableBRunnable0Shape9S0100000_I0_9(A02, 19));
            A02.A04 = r132;
            AnonymousClass18J r16 = A02.A0A;
            String str4 = A02.A0L;
            C89684Kx r142 = A02.A0G;
            AnonymousClass01J r22 = r16.A00.A01;
            C64373Fh r19 = new C64373Fh();
            A02.A01 = new C65133Ig((AnonymousClass17M) r22.AFd.get(), r142, r19, (C17120qI) r22.ALs.get(), str4);
            A02.A02 = new C63963Dq((C17120qI) A02.A0B.A00.A01.ALs.get(), str4);
            AbstractC17760rL r17 = A02.A0F;
            r17.A6A(str4);
            r17.AWS();
            A02.A0D.A00(A02.A0J, A02.A0M, (String) linkedHashMap.get(r04.key));
            A02.A05(str3, linkedHashMap, null);
            Long A0l = C12980iv.A0l(r5.A02.size());
            r7.A03(i, "initializeStateMachine");
            r8.A01(i, "num_states", A0l.longValue());
            r8.A02(i, "session_id", r0.A00());
        }
        AnonymousClass3E1 r05 = r32.A02;
        if (r05 != null) {
            for (AnonymousClass3Dy r18 : r05.A01.A01) {
                if (AGE != null) {
                    C16700pc.A0B(r18);
                    ((C19710uY) AGE).A04(r18);
                }
            }
        }
        r7.A05(i, 467);
        AnonymousClass3E2 r06 = r32.A03;
        if (r06 != null) {
            for (AnonymousClass3EH r07 : r06.A01.A01) {
                C16700pc.A0B(r07);
                AnonymousClass17Q.A01(r10, r07);
            }
        }
    }
}
