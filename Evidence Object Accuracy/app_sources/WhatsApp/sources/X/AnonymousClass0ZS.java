package X;

import android.view.View;
import android.view.WindowId;

/* renamed from: X.0ZS  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0ZS implements AbstractC11380gC {
    public final WindowId A00;

    public AnonymousClass0ZS(View view) {
        this.A00 = view.getWindowId();
    }

    public boolean equals(Object obj) {
        return (obj instanceof AnonymousClass0ZS) && ((AnonymousClass0ZS) obj).A00.equals(this.A00);
    }

    public int hashCode() {
        return this.A00.hashCode();
    }
}
