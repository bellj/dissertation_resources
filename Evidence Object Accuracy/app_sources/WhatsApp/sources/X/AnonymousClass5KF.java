package X;

/* renamed from: X.5KF  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5KF extends AnonymousClass1WI implements AnonymousClass1J7 {
    public final /* synthetic */ AnonymousClass1J7 $onError;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass5KF(AnonymousClass1J7 r2) {
        super(1);
        this.$onError = r2;
    }

    @Override // X.AnonymousClass1J7
    public /* bridge */ /* synthetic */ Object AJ4(Object obj) {
        Throwable th = new Throwable("GraphQL Error caught.");
        AnonymousClass1J7 r0 = this.$onError;
        if (r0 != null) {
            r0.AJ4(th);
        }
        return AnonymousClass1WZ.A00;
    }
}
