package X;

/* renamed from: X.50S  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass50S implements AbstractC115245Qt {
    public final int A00;
    public final AbstractC117135Yq A01;
    public final String A02;
    public final Object[] A03;

    public AnonymousClass50S(AbstractC117135Yq r6, String str, Object[] objArr) {
        char charAt;
        this.A01 = r6;
        this.A02 = str;
        this.A03 = objArr;
        int charAt2 = str.charAt(0);
        if (charAt2 >= 55296) {
            int i = charAt2 & 8191;
            int i2 = 13;
            int i3 = 1;
            while (true) {
                int i4 = i3 + 1;
                charAt = str.charAt(i3);
                if (charAt < 55296) {
                    break;
                }
                i |= (charAt & 8191) << i2;
                i2 += 13;
                i3 = i4;
            }
            charAt2 = i | (charAt << i2);
        }
        this.A00 = charAt2;
    }
}
