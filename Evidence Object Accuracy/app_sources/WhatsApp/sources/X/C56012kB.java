package X;

import android.content.res.TypedArray;

/* renamed from: X.2kB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C56012kB extends AbstractC64543Fy {
    public C56012kB() {
        this.A00.A0F = false;
    }

    @Override // X.AbstractC64543Fy
    public /* bridge */ /* synthetic */ AbstractC64543Fy A00(TypedArray typedArray) {
        super.A00(typedArray);
        if (typedArray.hasValue(2)) {
            AnonymousClass3C3 r3 = this.A00;
            r3.A05 = (typedArray.getColor(2, r3.A05) & 16777215) | (r3.A05 & -16777216);
        }
        if (typedArray.hasValue(12)) {
            AnonymousClass3C3 r1 = this.A00;
            r1.A09 = typedArray.getColor(12, r1.A09);
        }
        return this;
    }
}
