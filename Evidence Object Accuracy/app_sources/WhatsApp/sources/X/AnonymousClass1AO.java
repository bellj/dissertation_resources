package X;

import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.1AO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1AO {
    public final C16120oU A00;
    public final AtomicInteger A01 = new AtomicInteger();

    public AnonymousClass1AO(C16120oU r2) {
        this.A00 = r2;
    }

    public void A00(Boolean bool, Boolean bool2, int i) {
        C613630b r2 = new C613630b();
        r2.A03 = Long.valueOf((long) this.A01.getAndIncrement());
        r2.A02 = Integer.valueOf(i);
        r2.A00 = bool;
        r2.A01 = bool2;
        this.A00.A07(r2);
    }
}
