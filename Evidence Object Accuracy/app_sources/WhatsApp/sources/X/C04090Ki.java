package X;

import android.content.res.Configuration;
import android.os.LocaleList;

/* renamed from: X.0Ki  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C04090Ki {
    public static LocaleList A00(Configuration configuration) {
        return configuration.getLocales();
    }
}
