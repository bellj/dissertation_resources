package X;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.maps.internal.ICameraUpdateFactoryDelegate;

/* renamed from: X.1ij  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C35781ij {
    public static boolean A00;

    public static synchronized int A00(Context context) {
        ICameraUpdateFactoryDelegate iCameraUpdateFactoryDelegate;
        AnonymousClass5YB r1;
        synchronized (C35781ij.class) {
            C13020j0.A02(context, "Context is null");
            if (!A00) {
                try {
                    try {
                        C79843rI r4 = (C79843rI) AnonymousClass3GU.A01(context);
                        Parcel A02 = r4.A02(4, r4.A01());
                        IBinder readStrongBinder = A02.readStrongBinder();
                        if (readStrongBinder == null) {
                            iCameraUpdateFactoryDelegate = null;
                        } else {
                            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.ICameraUpdateFactoryDelegate");
                            if (queryLocalInterface instanceof ICameraUpdateFactoryDelegate) {
                                iCameraUpdateFactoryDelegate = (ICameraUpdateFactoryDelegate) queryLocalInterface;
                            } else {
                                iCameraUpdateFactoryDelegate = new C79793rD(readStrongBinder);
                            }
                        }
                        A02.recycle();
                        C13020j0.A01(iCameraUpdateFactoryDelegate);
                        C65193Io.A00 = iCameraUpdateFactoryDelegate;
                        Parcel A022 = r4.A02(5, r4.A01());
                        IBinder readStrongBinder2 = A022.readStrongBinder();
                        if (readStrongBinder2 == null) {
                            r1 = null;
                        } else {
                            IInterface queryLocalInterface2 = readStrongBinder2.queryLocalInterface("com.google.android.gms.maps.model.internal.IBitmapDescriptorFactoryDelegate");
                            if (queryLocalInterface2 instanceof AnonymousClass5YB) {
                                r1 = (AnonymousClass5YB) queryLocalInterface2;
                            } else {
                                r1 = new C79773rB(readStrongBinder2);
                            }
                        }
                        A022.recycle();
                        if (C36321jh.A00 == null) {
                            C13020j0.A02(r1, "delegate must not be null");
                            C36321jh.A00 = r1;
                        }
                        A00 = true;
                    } catch (RemoteException e) {
                        throw new C113245Gt(e);
                    }
                } catch (AnonymousClass29w e2) {
                    return e2.errorCode;
                }
            }
        }
        return 0;
    }
}
