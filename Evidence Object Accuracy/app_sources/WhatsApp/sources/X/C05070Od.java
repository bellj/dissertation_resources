package X;

import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.0Od  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05070Od {
    public final /* synthetic */ AnonymousClass05J A00;
    public final /* synthetic */ AnonymousClass05K A01;
    public final /* synthetic */ AnonymousClass02O A02;
    public final /* synthetic */ AnonymousClass01E A03;
    public final /* synthetic */ AtomicReference A04;

    public C05070Od(AnonymousClass05J r1, AnonymousClass05K r2, AnonymousClass02O r3, AnonymousClass01E r4, AtomicReference atomicReference) {
        this.A03 = r4;
        this.A02 = r3;
        this.A04 = atomicReference;
        this.A01 = r2;
        this.A00 = r1;
    }

    public void A00() {
        AnonymousClass01E r5 = this.A03;
        StringBuilder sb = new StringBuilder("fragment_");
        sb.append(r5.A0T);
        sb.append("_rq#");
        sb.append(r5.A0l.getAndIncrement());
        String obj = sb.toString();
        this.A04.set(((AnonymousClass052) this.A02.apply(null)).A01(this.A00, this.A01, r5, obj));
    }
}
