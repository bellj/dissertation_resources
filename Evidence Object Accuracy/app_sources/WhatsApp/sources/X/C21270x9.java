package X;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import com.whatsapp.R;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0x9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21270x9 {
    public final C14900mE A00;
    public final C15570nT A01;
    public final AnonymousClass130 A02;
    public final C15550nR A03;
    public final C15610nY A04;
    public final AnonymousClass10T A05;
    public final AnonymousClass131 A06;
    public final C19990v2 A07;
    public final AnonymousClass11F A08;
    public final C20710wC A09;

    public C21270x9(C14900mE r1, C15570nT r2, AnonymousClass130 r3, C15550nR r4, C15610nY r5, AnonymousClass10T r6, AnonymousClass131 r7, C19990v2 r8, AnonymousClass11F r9, C20710wC r10) {
        this.A00 = r1;
        this.A01 = r2;
        this.A07 = r8;
        this.A02 = r3;
        this.A03 = r4;
        this.A04 = r5;
        this.A09 = r10;
        this.A05 = r6;
        this.A08 = r9;
        this.A06 = r7;
    }

    public static Bitmap A00(Bitmap bitmap, float f, int i) {
        Rect rect;
        Bitmap createBitmap = Bitmap.createBitmap(i, i, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        Paint paint = new Paint();
        float f2 = (float) i;
        RectF rectF = new RectF(0.0f, 0.0f, f2, f2);
        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setFilterBitmap(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(-1);
        if (f == 0.0f) {
            canvas.drawRect(rectF, paint);
        } else if (f > 0.0f) {
            canvas.drawRoundRect(rectF, f, f, paint);
        } else if (f == -2.14748365E9f) {
            canvas.drawPath(C37501mV.A03(rectF), paint);
        } else {
            canvas.drawArc(rectF, 0.0f, 360.0f, true, paint);
        }
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        RectF rectF2 = new RectF(0.0f, 0.0f, f2, f2);
        int width = (bitmap.getWidth() - bitmap.getHeight()) >> 1;
        if (width > 0) {
            rect = new Rect(width, 0, bitmap.getWidth() - width, bitmap.getHeight());
        } else {
            rect = new Rect(0, -width, bitmap.getWidth(), bitmap.getHeight() + width);
        }
        canvas.drawBitmap(bitmap, rect, rectF2, paint);
        bitmap.recycle();
        return createBitmap;
    }

    public static Bitmap A01(List list, float f) {
        Rect rect;
        RectF rectF;
        int i;
        boolean z = false;
        if (list.size() > 1) {
            z = true;
        }
        AnonymousClass009.A0A("Insufficient number of bitmaps to combine", z);
        if (list.size() == 1) {
            return (Bitmap) list.get(0);
        }
        Iterator it = list.iterator();
        int i2 = 0;
        int i3 = 0;
        while (it.hasNext()) {
            Bitmap bitmap = (Bitmap) it.next();
            if (i2 < bitmap.getWidth()) {
                i2 = bitmap.getWidth();
            }
            if (i3 < bitmap.getHeight()) {
                i3 = bitmap.getHeight();
            }
        }
        Bitmap createBitmap = Bitmap.createBitmap(i2, i3, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        float f2 = (float) i2;
        float f3 = (float) i3;
        RectF rectF2 = new RectF(0.0f, 0.0f, f2, f3);
        Paint paint = new Paint();
        paint.setColor(-1);
        if (f != 0.0f) {
            if (f > 0.0f) {
                canvas.drawRoundRect(rectF2, f, f, paint);
            } else {
                canvas.drawArc(rectF2, 0.0f, 360.0f, true, paint);
            }
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        }
        if (list.size() == 2) {
            Rect rect2 = new Rect(((int) (((double) i2) * 0.25d)) + 1, 0, ((int) (0.75f * f2)) - 1, i3);
            float f4 = 0.5f * f2;
            canvas.drawBitmap((Bitmap) list.get(0), rect2, new RectF(0.0f, 0.0f, f4 - 2.0f, f3), paint);
            canvas.drawBitmap((Bitmap) list.get(1), rect2, new RectF(f4 + 2.0f, 0.0f, f2, f3), paint);
        } else {
            if (list.size() == 3) {
                int i4 = ((int) (((double) i2) * 0.25d)) + 1;
                int i5 = ((int) (0.75f * f2)) - 1;
                float f5 = f2 * 0.5f;
                canvas.drawBitmap((Bitmap) list.get(0), new Rect(i4, 0, i5, i3), new RectF(0.0f, 0.0f, f5 - 2.0f, f3), paint);
                double d = (double) i3;
                rect = new Rect(i4, ((int) (d * 0.25d)) + 1, i5, ((int) (d * 0.75d)) - 1);
                float f6 = f5 + 2.0f;
                float f7 = f3 * 0.5f;
                canvas.drawBitmap((Bitmap) list.get(1), rect, new RectF(f6, 0.0f, f2, f7 - 2.0f), paint);
                rectF = new RectF(f6, f7 + 2.0f, f2, f3);
                i = 2;
            } else if (list.size() == 4) {
                double d2 = (double) i3;
                rect = new Rect(((int) (((double) i2) * 0.25d)) + 1, ((int) (d2 * 0.25d)) + 1, ((int) (0.75f * f2)) - 1, ((int) (d2 * 0.75d)) - 1);
                float f8 = f2 * 0.5f;
                float f9 = f8 - 2.0f;
                float f10 = 0.5f * f3;
                float f11 = f10 - 2.0f;
                canvas.drawBitmap((Bitmap) list.get(0), rect, new RectF(0.0f, 0.0f, f9, f11), paint);
                float f12 = f10 + 2.0f;
                canvas.drawBitmap((Bitmap) list.get(1), rect, new RectF(0.0f, f12, f9, f3), paint);
                float f13 = f8 + 2.0f;
                canvas.drawBitmap((Bitmap) list.get(2), rect, new RectF(f13, 0.0f, f2, f11), paint);
                rectF = new RectF(f13, f12, f2, f3);
                i = 3;
            }
            canvas.drawBitmap((Bitmap) list.get(i), rect, rectF, paint);
            return createBitmap;
        }
        return createBitmap;
    }

    public Bitmap A02(Context context, C15370n3 r6, int i, int i2) {
        int min = Math.min(i, i2);
        int i3 = 0;
        if (Build.VERSION.SDK_INT >= 21) {
            i3 = -1;
        }
        float f = (float) i3;
        Bitmap bitmap = (Bitmap) this.A05.A02.A01().A00(r6.A0E(f, min));
        return (bitmap != null || !r6.A0X || min <= 0) ? bitmap : this.A06.A01(context, r6, f, min);
    }

    public AnonymousClass1J1 A03(Context context, String str) {
        Resources resources = context.getResources();
        return new AnonymousClass1J1(this, str, resources.getDimension(R.dimen.small_avatar_radius), resources.getDimensionPixelSize(R.dimen.small_avatar_size), true);
    }

    public AnonymousClass1J1 A04(Context context, String str) {
        Resources resources = context.getResources();
        return new AnonymousClass1J1(this, str, resources.getDimension(R.dimen.small_avatar_radius), resources.getDimensionPixelSize(R.dimen.small_avatar_size), false);
    }

    public AnonymousClass1J1 A05(String str, float f, int i) {
        return new AnonymousClass1J1(this, str, f, i, false);
    }
}
