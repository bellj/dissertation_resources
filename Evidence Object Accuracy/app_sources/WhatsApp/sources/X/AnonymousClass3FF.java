package X;

/* renamed from: X.3FF  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3FF {
    public final int A00;
    public final int A01;
    public final C15370n3 A02;
    public final AbstractC14640lm A03;

    public AnonymousClass3FF(C15370n3 r1, AbstractC14640lm r2, int i, int i2) {
        this.A02 = r1;
        this.A03 = r2;
        this.A01 = i;
        this.A00 = i2;
    }

    public boolean A00() {
        int i = this.A01;
        return i == 1 || i == 2;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass3FF r5 = (AnonymousClass3FF) obj;
            if (!(this.A01 == r5.A01 && this.A00 == r5.A00 && C29941Vi.A00(this.A02, r5.A02))) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] objArr = new Object[3];
        objArr[0] = this.A02;
        C12980iv.A1T(objArr, this.A01);
        return C12980iv.A0B(Integer.valueOf(this.A00), objArr, 2);
    }
}
