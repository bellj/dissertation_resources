package X;

import android.content.Context;
import com.whatsapp.mediacomposer.doodle.shapepicker.ShapePickerRecyclerView;

/* renamed from: X.3ih  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74733ih extends AnonymousClass0FE {
    public final /* synthetic */ ShapePickerRecyclerView A00;

    @Override // X.AnonymousClass0FE
    public int A06() {
        return -1;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C74733ih(Context context, ShapePickerRecyclerView shapePickerRecyclerView) {
        super(context);
        this.A00 = shapePickerRecyclerView;
    }
}
