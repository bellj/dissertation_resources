package X;

import android.database.ContentObserver;
import java.util.HashMap;

/* renamed from: X.2JN  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2JN implements AbstractC35581iK {
    @Override // X.AbstractC35581iK
    public AbstractC35611iN AEC(int i) {
        return null;
    }

    @Override // X.AbstractC35581iK
    public void AaX() {
    }

    @Override // X.AbstractC35581iK
    public void close() {
    }

    @Override // X.AbstractC35581iK
    public int getCount() {
        return 0;
    }

    @Override // X.AbstractC35581iK
    public boolean isEmpty() {
        return true;
    }

    @Override // X.AbstractC35581iK
    public void registerContentObserver(ContentObserver contentObserver) {
    }

    @Override // X.AbstractC35581iK
    public void unregisterContentObserver(ContentObserver contentObserver) {
    }

    @Override // X.AbstractC35581iK
    public HashMap AAz() {
        return new HashMap();
    }
}
