package X;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.4vQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106264vQ implements AnonymousClass5WW {
    @Override // X.AnonymousClass5WW
    public void A6O(Context context, Object obj, Object obj2, Object obj3) {
        ((RecyclerView) obj).setItemAnimator(((C56002kA) obj2).A05);
    }

    @Override // X.AnonymousClass5WW
    public boolean Adc(Object obj, Object obj2, Object obj3, Object obj4) {
        return C12960it.A1X(((C56002kA) obj).A05, ((C56002kA) obj2).A05);
    }

    @Override // X.AnonymousClass5WW
    public void Af8(Context context, Object obj, Object obj2, Object obj3) {
        ((RecyclerView) obj).setItemAnimator(null);
    }
}
