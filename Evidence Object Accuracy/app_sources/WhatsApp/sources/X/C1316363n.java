package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.63n  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1316363n implements Parcelable {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(37);
    public final long A00;
    public final AnonymousClass6F2 A01;
    public final AnonymousClass6F2 A02;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C1316363n(AnonymousClass6F2 r1, AnonymousClass6F2 r2, long j) {
        this.A00 = j;
        this.A01 = r1;
        this.A02 = r2;
    }

    public static C1316363n A00(AnonymousClass102 r5, AnonymousClass1V8 r6) {
        return new C1316363n(AnonymousClass6F2.A00(r5, r6.A0F("local")), AnonymousClass6F2.A00(r5, r6.A0F("trading")), r6.A08("quote-id", -1));
    }

    public static C1316363n A01(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            JSONObject A05 = C13000ix.A05(str);
            AnonymousClass6F2 A01 = AnonymousClass6F2.A01(A05.optString("local", A05.optString("fiat", "")));
            AnonymousClass6F2 A012 = AnonymousClass6F2.A01(A05.optString("trading", A05.optString("crypto", "")));
            AnonymousClass009.A05(A01);
            AnonymousClass009.A05(A012);
            return new C1316363n(A01, A012, -1);
        } catch (JSONException unused) {
            Log.w("PAY: TransactionAmount fromJsonString threw exception");
            return null;
        }
    }

    public JSONObject A02() {
        JSONObject A0a = C117295Zj.A0a();
        try {
            C117315Zl.A0W(this.A01, "local", A0a);
            C117315Zl.A0W(this.A02, "trading", A0a);
            return A0a;
        } catch (JSONException unused) {
            Log.w("PAY: TransactionAmount toJson threw exception");
            return A0a;
        }
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.A00);
        parcel.writeParcelable(this.A01, i);
        parcel.writeParcelable(this.A02, i);
    }
}
