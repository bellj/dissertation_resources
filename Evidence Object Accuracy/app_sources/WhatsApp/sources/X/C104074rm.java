package X;

import android.view.View;
import com.google.android.material.appbar.CollapsingToolbarLayout;

/* renamed from: X.4rm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C104074rm implements AnonymousClass07F {
    public final /* synthetic */ CollapsingToolbarLayout A00;

    public C104074rm(CollapsingToolbarLayout collapsingToolbarLayout) {
        this.A00 = collapsingToolbarLayout;
    }

    @Override // X.AnonymousClass07F
    public C018408o AMH(View view, C018408o r5) {
        CollapsingToolbarLayout collapsingToolbarLayout = this.A00;
        C018408o r1 = null;
        if (collapsingToolbarLayout.getFitsSystemWindows()) {
            r1 = r5;
        }
        if (!C015407h.A01(collapsingToolbarLayout.A0F, r1)) {
            collapsingToolbarLayout.A0F = r1;
            collapsingToolbarLayout.requestLayout();
        }
        return r5.A00.A09();
    }
}
