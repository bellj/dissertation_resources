package X;

import android.content.Context;
import com.whatsapp.location.LocationPicker;

/* renamed from: X.32q  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C618132q extends AnonymousClass294 {
    public final /* synthetic */ LocationPicker A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C618132q(Context context, AnonymousClass0O0 r2, LocationPicker locationPicker) {
        super(context, r2);
        this.A00 = locationPicker;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002c, code lost:
        if (r1 != 3) goto L_0x0015;
     */
    @Override // X.AnonymousClass294, android.view.View, android.view.ViewGroup
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean dispatchTouchEvent(android.view.MotionEvent r11) {
        /*
        // Method dump skipped, instructions count: 292
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C618132q.dispatchTouchEvent(android.view.MotionEvent):boolean");
    }
}
