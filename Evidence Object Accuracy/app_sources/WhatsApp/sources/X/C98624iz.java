package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4iz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98624iz implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        byte[] bArr = null;
        boolean z = false;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c != 1) {
                z = C95664e9.A0H(parcel, c, 2, readInt, z);
            } else {
                bArr = C95664e9.A0I(parcel, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78043oH(bArr, z);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new C78043oH[i];
    }
}
