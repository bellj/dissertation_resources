package X;

import com.whatsapp.R;
import com.whatsapp.payments.ui.widget.PaymentView;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.5op  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124235op extends AbstractC16350or {
    public final /* synthetic */ AbstractActivityC121525iS A00;

    public C124235op(AbstractActivityC121525iS r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        return C117295Zj.A0Z(((AbstractActivityC121685jC) this.A00).A0P);
    }

    @Override // X.AbstractC16350or
    public void A06() {
        AbstractActivityC121525iS r1 = this.A00;
        r1.A0n.incrementAndGet();
        r1.A2C(R.string.register_wait_message);
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        int i;
        List list = (List) obj;
        AbstractActivityC121525iS r3 = this.A00;
        if (!((AbstractActivityC121545iU) r3).A0G && !r3.A0k && r3.A0n.decrementAndGet() == 0) {
            r3.AaN();
        }
        if (list == null || list.size() == 0) {
            r3.A0m.A06("PopulateMethodsForSend could not find methods;");
            r3.finish();
            return;
        }
        C30931Zj r4 = r3.A0m;
        StringBuilder A0k = C12960it.A0k("onPostExecute got methods: ");
        A0k.append(list.size());
        C117295Zj.A1F(r4, A0k);
        List A03 = AbstractC28901Pl.A03(((AbstractActivityC121685jC) r3).A0N.A01(), list);
        r3.A0g = A03;
        r4.A04(C12970iu.A0s(Integer.valueOf(A03.size()), C12960it.A0k("onPostExecute got paymentMethodList for store: ")));
        List list2 = r3.A0g;
        if (list2 != null && list2.size() > 0) {
            AbstractC28901Pl r1 = r3.A0B;
            boolean z = false;
            List list3 = r3.A0g;
            if (r1 != null) {
                Iterator it = list3.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    AbstractC28901Pl A0H = C117305Zk.A0H(it);
                    if (A0H.A0A.equals(r3.A0B.A0A)) {
                        r3.A0g.remove(A0H);
                        break;
                    }
                }
                r3.A0g.add(0, r3.A0B);
            } else {
                r3.A0B = C117315Zl.A08(list3, 0);
            }
            PaymentView paymentView = r3.A0W;
            if (paymentView != null) {
                paymentView.setBankLogo(r3.A0B.A05());
                PaymentView paymentView2 = r3.A0W;
                AnonymousClass018 r9 = ((AbstractActivityC121545iU) r3).A01;
                C17070qD r7 = ((AbstractActivityC121685jC) r3).A0P;
                List list4 = r3.A0g;
                AbstractC28901Pl r5 = r3.A0B;
                if (r5 != null) {
                    i = 0;
                    while (true) {
                        if (i < list4.size()) {
                            if (C117315Zl.A08(list4, i).A0A.equals(r5.A0A)) {
                                break;
                            }
                            i++;
                        } else {
                            i = 0;
                            break;
                        }
                    }
                } else {
                    i = C1311161i.A01(list4);
                }
                paymentView2.setPaymentMethodText(C1311161i.A02(r3, r9, C117315Zl.A08(list4, i), r7, true));
                PaymentView paymentView3 = r3.A0W;
                if (r3.A0B == null) {
                    z = true;
                }
                paymentView3.A0H(z);
                PaymentView paymentView4 = r3.A0W;
                if (paymentView4.A00 != 1) {
                    paymentView4.A0G(true);
                }
            }
        }
        r3.A0U = null;
    }
}
