package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.redex.IDxCreatorShape1S0000000_2_I1;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.3M8  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3M8 implements Parcelable {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape1S0000000_2_I1(46);
    public double A00;
    public double A01;
    public final int A02;
    public final Double A03;
    public final Double A04;
    public final Double A05;
    public final String A06;
    public final String A07;
    public final String A08;
    public final String A09;
    public final String A0A;
    public final String A0B;
    public final List A0C;
    public final List A0D;
    public final boolean A0E;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass3M8(Parcel parcel) {
        this.A07 = C12990iw.A0p(parcel);
        this.A06 = parcel.readString();
        this.A0B = C12990iw.A0p(parcel);
        Double valueOf = Double.valueOf(parcel.readDouble());
        AnonymousClass009.A05(valueOf);
        this.A04 = valueOf;
        Double valueOf2 = Double.valueOf(parcel.readDouble());
        AnonymousClass009.A05(valueOf2);
        this.A05 = valueOf2;
        ArrayList A0l = C12960it.A0l();
        this.A0C = A0l;
        parcel.readStringList(A0l);
        this.A0A = C12990iw.A0p(parcel);
        Integer valueOf3 = Integer.valueOf(parcel.readInt());
        AnonymousClass009.A05(valueOf3);
        this.A02 = valueOf3.intValue();
        this.A08 = parcel.readString();
        this.A03 = Double.valueOf(parcel.readDouble());
        this.A09 = parcel.readString();
        ArrayList A0l2 = C12960it.A0l();
        this.A0D = A0l2;
        parcel.readList(A0l2, C30221Wo.class.getClassLoader());
        this.A0E = C12970iu.A1W(parcel.readInt());
    }

    public AnonymousClass3M8(Double d, Double d2, Double d3, String str, String str2, String str3, String str4, String str5, String str6, List list, List list2, int i, boolean z) {
        this.A07 = str;
        this.A06 = str2;
        this.A0B = str3;
        this.A04 = d;
        this.A05 = d2;
        this.A0E = z;
        this.A0C = list;
        this.A0A = str4;
        this.A02 = i;
        this.A08 = str5;
        this.A03 = d3;
        this.A0D = list2;
        this.A09 = str6;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x006d, code lost:
        if (r0 != 2) goto L_0x006f;
     */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x008c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass3M8 A00(org.json.JSONObject r22) {
        /*
            java.lang.String r0 = "jid"
            r1 = r22
            java.lang.String r13 = r1.getString(r0)
            java.lang.String r0 = "verified_name"
            java.lang.String r16 = r1.getString(r0)
            java.lang.String r0 = "profile_pic_url"
            java.lang.String r17 = r1.optString(r0)
            java.lang.String r0 = "address"
            java.lang.String r14 = r1.optString(r0)
            java.lang.String r0 = "vertical"
            java.lang.String r15 = r1.getString(r0)
            java.lang.String r0 = "latitude"
            double r2 = r1.optDouble(r0)
            java.lang.Double r10 = java.lang.Double.valueOf(r2)
            java.lang.String r0 = "longitude"
            double r2 = r1.optDouble(r0)
            java.lang.Double r11 = java.lang.Double.valueOf(r2)
            java.lang.String r0 = "responsive"
            boolean r22 = r1.optBoolean(r0)
            java.lang.String r0 = "categories"
            org.json.JSONArray r3 = r1.getJSONArray(r0)
            X.AnonymousClass009.A05(r3)
            java.util.ArrayList r8 = X.C12960it.A0l()
            r2 = 0
        L_0x004a:
            int r0 = r3.length()
            if (r2 >= r0) goto L_0x005a
            java.lang.String r0 = r3.getString(r2)
            r8.add(r0)
            int r2 = r2 + 1
            goto L_0x004a
        L_0x005a:
            java.lang.String r2 = "business_operating"
            boolean r0 = r1.has(r2)
            if (r0 == 0) goto L_0x006f
            int r0 = r1.optInt(r2)
            r4 = 0
            if (r0 == 0) goto L_0x0070
            r4 = 1
            if (r0 == r4) goto L_0x0070
            r4 = 2
            if (r0 == r4) goto L_0x0070
        L_0x006f:
            r4 = 3
        L_0x0070:
            java.lang.String r0 = "biz_pre_rank_score"
            double r2 = r1.optDouble(r0)
            java.lang.Double r12 = java.lang.Double.valueOf(r2)
            java.lang.String r0 = "ranking_result_id"
            java.lang.String r18 = r1.optString(r0)
            java.util.ArrayList r3 = X.C12960it.A0l()
            java.lang.String r2 = "linked_accounts"
            boolean r0 = r1.has(r2)
            if (r0 == 0) goto L_0x00dc
            org.json.JSONArray r5 = r1.getJSONArray(r2)
            X.AnonymousClass009.A05(r5)
            java.util.ArrayList r3 = X.C12960it.A0l()
            if (r5 == 0) goto L_0x00dc
            r2 = 0
        L_0x009a:
            int r0 = r5.length()
            if (r2 >= r0) goto L_0x00dc
            org.json.JSONObject r1 = r5.getJSONObject(r2)     // Catch: JSONException -> 0x00d3
            java.lang.String r0 = "id"
            java.lang.String r7 = r1.getString(r0)     // Catch: JSONException -> 0x00d3
            X.AnonymousClass009.A04(r7)     // Catch: JSONException -> 0x00d3
            java.lang.String r0 = "type"
            boolean r6 = r1.has(r0)     // Catch: JSONException -> 0x00d3
            if (r6 == 0) goto L_0x00d9
            int r0 = r1.getInt(r0)     // Catch: JSONException -> 0x00d3
            java.lang.String r6 = "fanCount"
            int r6 = r1.optInt(r6)     // Catch: JSONException -> 0x00d3
            if (r0 == 0) goto L_0x00c8
            r1 = 1
            if (r0 != r1) goto L_0x00d9
            java.lang.String r1 = "instagram"
            goto L_0x00ca
        L_0x00c8:
            java.lang.String r1 = "facebook"
        L_0x00ca:
            X.1Wo r0 = new X.1Wo     // Catch: JSONException -> 0x00d3
            r0.<init>(r7, r1, r6)     // Catch: JSONException -> 0x00d3
            r3.add(r0)     // Catch: JSONException -> 0x00d3
            goto L_0x00d9
        L_0x00d3:
            r1 = move-exception
            java.lang.String r0 = "MinifiedBusinessProfile/readLinkedAccountsArray: could not parse one of the LinkedAccount json object"
            com.whatsapp.util.Log.e(r0, r1)
        L_0x00d9:
            int r2 = r2 + 1
            goto L_0x009a
        L_0x00dc:
            r20 = r3
            r21 = r4
            r19 = r8
            X.3M8 r9 = new X.3M8
            r9.<init>(r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22)
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3M8.A00(org.json.JSONObject):X.3M8");
    }

    public boolean A01() {
        Double d = this.A04;
        Double valueOf = Double.valueOf(Double.NaN);
        if (!d.equals(valueOf)) {
            Double d2 = this.A05;
            if (!d2.equals(valueOf) && d.doubleValue() != 0.0d && d2.doubleValue() != 0.0d) {
                return true;
            }
        }
        return false;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass3M8 r5 = (AnonymousClass3M8) obj;
            if (this.A02 != r5.A02 || !this.A07.equals(r5.A07) || !C29941Vi.A00(this.A06, r5.A06) || !this.A04.equals(r5.A04) || !this.A05.equals(r5.A05) || !this.A0A.equals(r5.A0A) || !this.A0C.equals(r5.A0C) || !C29941Vi.A00(this.A08, r5.A08)) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        Object[] objArr = new Object[6];
        objArr[0] = this.A07;
        objArr[1] = this.A04;
        objArr[2] = this.A05;
        objArr[3] = this.A0A;
        objArr[4] = this.A0C;
        return C12980iv.A0B(Integer.valueOf(this.A02), objArr, 5);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A07);
        parcel.writeString(this.A06);
        parcel.writeString(this.A0B);
        parcel.writeDouble(this.A04.doubleValue());
        parcel.writeDouble(this.A05.doubleValue());
        parcel.writeStringList(this.A0C);
        parcel.writeString(this.A0A);
        parcel.writeInt(this.A02);
        parcel.writeString(this.A08);
        parcel.writeDouble(this.A03.doubleValue());
        parcel.writeString(this.A09);
        parcel.writeList(this.A0D);
        parcel.writeInt(this.A0E ? 1 : 0);
    }
}
