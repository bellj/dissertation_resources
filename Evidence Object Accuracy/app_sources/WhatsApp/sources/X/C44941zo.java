package X;

/* renamed from: X.1zo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C44941zo extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Double A02;
    public Double A03;
    public Double A04;
    public Double A05;
    public Double A06;
    public Double A07;
    public Double A08;
    public Integer A09;
    public Long A0A;
    public Long A0B;
    public Long A0C;
    public Long A0D;

    public C44941zo() {
        super(486, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(8, this.A02);
        r3.Abe(19, this.A0A);
        r3.Abe(5, this.A00);
        r3.Abe(2, this.A01);
        r3.Abe(3, this.A0B);
        r3.Abe(12, this.A03);
        r3.Abe(9, this.A04);
        r3.Abe(13, this.A05);
        r3.Abe(1, this.A09);
        r3.Abe(6, this.A0C);
        r3.Abe(7, this.A06);
        r3.Abe(11, this.A07);
        r3.Abe(10, this.A08);
        r3.Abe(14, this.A0D);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        StringBuilder sb = new StringBuilder("WamRestore {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupRestoreChatdbSize", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupRestoreEncryptionVersion", this.A0A);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupRestoreIncludeVideos", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupRestoreIsFull", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupRestoreIsWifi", this.A0B);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupRestoreMediaFileCount", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupRestoreMediaSize", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupRestoreNetworkRequestCount", this.A05);
        Integer num = this.A09;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupRestoreResult", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupRestoreT", this.A0C);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupRestoreTotalSize", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupRestoreTransferFailedSize", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupRestoreTransferSize", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "restoreConcurrentReadsCount", this.A0D);
        sb.append("}");
        return sb.toString();
    }
}
