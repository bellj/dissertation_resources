package X;

import android.graphics.Rect;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;

/* renamed from: X.3Mb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class View$OnFocusChangeListenerC66043Mb implements View.OnFocusChangeListener {
    public final C14260l7 A00;
    public final AnonymousClass3C4 A01;
    public final AnonymousClass28D A02;
    public final AbstractC14200l1 A03;
    public final AbstractC14200l1 A04;

    public View$OnFocusChangeListenerC66043Mb(C14260l7 r2, AnonymousClass28D r3, AbstractC14200l1 r4, AbstractC14200l1 r5) {
        this.A02 = r3;
        this.A00 = r2;
        this.A03 = r4;
        this.A04 = r5;
        this.A01 = (AnonymousClass3C4) AnonymousClass3JV.A04(r2, r3);
    }

    @Override // android.view.View.OnFocusChangeListener
    public void onFocusChange(View view, boolean z) {
        AbstractC14200l1 r6;
        Editable text;
        C15190mi r8 = (C15190mi) view;
        TextUtils.TruncateAt ellipsize = r8.getEllipsize();
        if (z) {
            if (ellipsize != null) {
                C15200mj.A01(null, r8, this.A01);
                r8.getExtendedPaddingTop();
                r8.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver$OnPreDrawListenerC101954oM(r8, this));
            }
            r6 = this.A03;
        } else {
            if (ellipsize == null && C15200mj.A09(r8) && (text = r8.getText()) != null) {
                Rect A0J = C12980iv.A0J();
                r8.getPaint().getTextBounds(text.toString(), 0, text.length(), A0J);
                if (A0J.width() > C12960it.A04(r8, r8.getWidth())) {
                    C15200mj.A01(TextUtils.TruncateAt.END, r8, this.A01);
                }
            }
            r6 = this.A04;
        }
        if (r6 != null) {
            C14210l2 r0 = new C14210l2();
            AnonymousClass28D r3 = this.A02;
            r0.A04(r3, 0);
            C14260l7 r2 = this.A00;
            r0.A04(r2, 1);
            C28701Oq.A01(r2, r3, new C14220l3(r0.A00), r6);
        }
    }
}
