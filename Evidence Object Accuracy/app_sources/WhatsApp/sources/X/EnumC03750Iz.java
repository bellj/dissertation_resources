package X;

/* renamed from: X.0Iz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public enum EnumC03750Iz {
    /* Fake field, exist only in values array */
    TEXTURE_2D,
    TEXTURE_EXT,
    /* Fake field, exist only in values array */
    TEXTURE_EXT_BW,
    /* Fake field, exist only in values array */
    TEXTURE_EXT_FILT
}
