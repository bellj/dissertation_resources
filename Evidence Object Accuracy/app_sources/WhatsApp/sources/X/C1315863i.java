package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

/* renamed from: X.63i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1315863i implements Parcelable {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(17);
    public final AnonymousClass63Y A00;
    public final AnonymousClass63Y A01;
    public final C1316563p A02;
    public final C1316563p A03;
    public final C1316563p A04;
    public final C1316563p A05;
    public final AbstractC136446Mo A06;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0042  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C1315863i(X.AnonymousClass63Y r3, X.AnonymousClass63Y r4, X.C1316563p r5, X.C1316563p r6, X.C1316563p r7, X.C1316563p r8) {
        /*
            r2 = this;
            r2.<init>()
            r2.A05 = r5
            r2.A04 = r6
            r2.A03 = r7
            r2.A02 = r8
            r2.A01 = r3
            r2.A00 = r4
            X.1Yv r0 = r3.A00
            X.1Yu r0 = (X.AbstractC30781Yu) r0
            int r0 = r0.A00
            r1 = 1
            if (r0 != r1) goto L_0x0028
            X.1Yv r0 = r4.A00
            X.1Yu r0 = (X.AbstractC30781Yu) r0
            int r0 = r0.A00
            if (r0 != r1) goto L_0x003a
            X.6BB r0 = new X.6BB
            r0.<init>(r3, r4)
        L_0x0025:
            r2.A06 = r0
            return
        L_0x0028:
            if (r0 == r1) goto L_0x003a
            if (r0 != 0) goto L_0x0048
            X.1Yv r0 = r4.A00
            X.1Yu r0 = (X.AbstractC30781Yu) r0
            int r0 = r0.A00
            if (r0 != r1) goto L_0x0048
            X.6B9 r0 = new X.6B9
            r0.<init>(r3, r4)
            goto L_0x0025
        L_0x003a:
            X.1Yv r0 = r4.A00
            X.1Yu r0 = (X.AbstractC30781Yu) r0
            int r0 = r0.A00
            if (r0 != 0) goto L_0x0048
            X.6B8 r0 = new X.6B8
            r0.<init>(r3, r4)
            goto L_0x0025
        L_0x0048:
            X.6BA r0 = new X.6BA
            r0.<init>(r3, r4)
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C1315863i.<init>(X.63Y, X.63Y, X.63p, X.63p, X.63p, X.63p):void");
    }

    public static C1316563p A00(AnonymousClass102 r10, AnonymousClass1V8 r11) {
        AnonymousClass6F2 r2;
        AnonymousClass1V8 A0E = r11.A0E("fee");
        long A07 = r11.A07("id");
        long A072 = r11.A07("expiry-ts");
        if (A0E != null) {
            r2 = AnonymousClass6F2.A00(r10, A0E);
        } else {
            r2 = null;
        }
        String A0H = r11.A0H("exchange-rate");
        try {
            return new C1316563p(r2, r11.A0H("source-iso-code"), r11.A0H("target-iso-code"), new BigDecimal(A0H), A07, A072);
        } catch (NullPointerException | NumberFormatException unused) {
            throw new AnonymousClass1V9(C12960it.A0d(A0H, C12960it.A0k("attribute exchange-rate is not integral: ")));
        }
    }

    public C30821Yy A01(AbstractC30791Yv r3, C30821Yy r4, int i) {
        return this.A06.A9k(new AnonymousClass6F2(r3, r4), this, i).A01;
    }

    public boolean A02(long j) {
        return TimeUnit.MICROSECONDS.toMillis(Math.min(Math.min(this.A05.A00, this.A03.A00), this.A02.A00)) > j && this.A06.AKE(this);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.A05, i);
        parcel.writeParcelable(this.A04, i);
        parcel.writeParcelable(this.A03, i);
        parcel.writeParcelable(this.A02, i);
        parcel.writeParcelable(this.A01, i);
        parcel.writeParcelable(this.A00, i);
    }
}
