package X;

import java.util.Map;

/* renamed from: X.0ub  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C19740ub extends AbstractC19720uZ implements AbstractC19700uX, AbstractC19580uL, AbstractC19730ua {
    public final C19640uR A00;
    public final C17120qI A01;
    public final C19600uN A02;
    public final /* synthetic */ C19570uK A03;
    public final /* synthetic */ C19690uW A04;

    @Override // X.AbstractC19580uL
    public boolean A5Z(String str) {
        return this.A03.A5Z(str);
    }

    @Override // X.AbstractC19700uX
    public void A6h() {
        this.A04.A6h();
    }

    @Override // X.AbstractC19700uX
    public AnonymousClass01E AEX(String str, String str2, Map map, Map map2, int i) {
        C16700pc.A0E(str2, 3);
        return this.A04.AEX(str, str2, map, map2, i);
    }

    @Override // X.AbstractC19580uL
    public void AHw(String str, String str2) {
        this.A03.AHw(str, str2);
    }

    @Override // X.AbstractC19700uX
    public void AYb(String str, String str2, String str3, String str4, Map map, Map map2, int i) {
        C16700pc.A0E(str4, 5);
        this.A04.AYb(str, str2, str3, str4, map, map2, i);
    }

    @Override // X.AbstractC19700uX
    public void AYh(AnonymousClass4A3 r10, String str, String str2, String str3, String str4, Map map, Map map2, int i) {
        C16700pc.A0E(str4, 6);
        this.A04.AYh(r10, str, str2, str3, str4, map, map2, i);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C19740ub(C19680uV r2, C19630uQ r3, C19570uK r4, C19640uR r5, C19690uW r6, C17120qI r7, C19600uN r8) {
        super(r2, r3);
        C16700pc.A0E(r7, 1);
        C16700pc.A0E(r2, 4);
        C16700pc.A0E(r3, 7);
        this.A01 = r7;
        this.A02 = r8;
        this.A00 = r5;
        this.A03 = r4;
        this.A04 = r6;
    }

    public static final Map A00(Map map) {
        C17520qw[] r3 = new C17520qw[2];
        r3[0] = new C17520qw("should_load_bloks_through_cdn", Boolean.TRUE);
        Object obj = map.get("static_url");
        if (obj != null) {
            r3[1] = new C17520qw("static_url", obj);
            return C17530qx.A02(r3);
        }
        throw new NullPointerException("null cannot be cast to non-null type kotlin.String");
    }

    public static /* synthetic */ void A01(C19740ub r2, AnonymousClass6E8 r3) {
        C16700pc.A0E(r2, 0);
        C16700pc.A0E(r3, 1);
        r2.A00.A01(r3.A00);
    }

    @Override // X.AbstractC19730ua
    public void AZR(AnonymousClass3D5 r4, Map map, int i) {
        C16700pc.A0E(map, 0);
        C19600uN r2 = this.A02;
        Object obj = map.get("app_id");
        if (obj != null) {
            r2.A02(r4, (String) obj, A00(map), i);
            return;
        }
        throw new NullPointerException("null cannot be cast to non-null type kotlin.String");
    }
}
