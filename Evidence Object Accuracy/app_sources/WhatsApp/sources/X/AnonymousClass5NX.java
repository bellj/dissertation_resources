package X;

/* renamed from: X.5NX  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5NX extends AbstractC114775Na {
    public AnonymousClass5NX() {
    }

    public AnonymousClass5NX(AnonymousClass1TN r1) {
        super(r1);
    }

    public AnonymousClass5NX(C94954co r1) {
        super(r1);
    }

    @Override // X.AnonymousClass1TL
    public int A05() {
        int length = this.A00.length;
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            i += C72463ee.A0N(this.A00, i2).A05();
        }
        return i + 2 + 2;
    }
}
