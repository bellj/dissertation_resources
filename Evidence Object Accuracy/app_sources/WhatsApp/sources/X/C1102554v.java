package X;

import android.graphics.Bitmap;
import android.widget.ImageView;
import com.whatsapp.R;

/* renamed from: X.54v  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1102554v implements AnonymousClass28F {
    public final /* synthetic */ C60172w8 A00;

    public C1102554v(C60172w8 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass28F
    public void Adh(Bitmap bitmap, ImageView imageView, boolean z) {
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        } else {
            Adv(imageView);
        }
    }

    @Override // X.AnonymousClass28F
    public void Adv(ImageView imageView) {
        imageView.setImageResource(R.drawable.avatar_contact);
    }
}
