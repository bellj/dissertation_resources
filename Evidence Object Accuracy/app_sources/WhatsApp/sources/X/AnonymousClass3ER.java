package X;

import com.google.android.gms.maps.model.LatLng;
import java.util.ArrayList;

/* renamed from: X.3ER  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3ER {
    public C68493Vp A00;
    public C59422uh A01;
    public final AbstractC48942In A02;
    public final AnonymousClass1B5 A03;

    public AnonymousClass3ER(AbstractC48942In r1, AnonymousClass1B5 r2) {
        this.A03 = r2;
        this.A02 = r1;
    }

    public synchronized void A00() {
        C59422uh r0 = this.A01;
        if (!(r0 == null || r0.A0A.A00() == 2 || this.A01.A0A.A02.isCancelled())) {
            this.A01.A0A.A03(true);
            this.A01 = null;
        }
    }

    public void A01(LatLng latLng, AnonymousClass1P6 r13, String str, String str2, float f) {
        ArrayList A01 = C65013Hu.A01(latLng.A00, latLng.A01, 10);
        AnonymousClass009.A05(A01);
        LatLng A02 = AnonymousClass1U5.A02(C65013Hu.A02(10, C12980iv.A0G(A01.get(0)), C12980iv.A0G(A01.get(1))));
        synchronized (this) {
            A00();
            C68493Vp r4 = new C68493Vp(latLng, r13, this, str, str2, f);
            this.A00 = r4;
            C59422uh A82 = this.A02.A82(A02, r4, this.A03.A00, 256);
            A82.A03();
            this.A01 = A82;
        }
    }
}
