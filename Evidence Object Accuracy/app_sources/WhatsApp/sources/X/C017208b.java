package X;

import android.view.MenuItem;

/* renamed from: X.08b  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C017208b implements AbstractC017308c {
    public final /* synthetic */ AnonymousClass08Z A00;

    public C017208b(AnonymousClass08Z r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC017308c
    public boolean onMenuItemClick(MenuItem menuItem) {
        return this.A00.A00.onMenuItemSelected(0, menuItem);
    }
}
