package X;

/* renamed from: X.0Zi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07580Zi implements AbstractC12800iW {
    public final AnonymousClass016 A00 = new AnonymousClass016();
    public final AnonymousClass040 A01 = AnonymousClass040.A00();

    public C07580Zi() {
        A00(AbstractC12800iW.A00);
    }

    public void A00(AnonymousClass0LM r3) {
        this.A00.A0A(r3);
        if (r3 instanceof AnonymousClass0GO) {
            this.A01.A09(r3);
        } else if (r3 instanceof AnonymousClass0GP) {
            this.A01.A0A(((AnonymousClass0GP) r3).A00);
        }
    }
}
