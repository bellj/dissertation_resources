package X;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.util.ViewOnClickCListenerShape2S0201000_I1;
import java.util.ArrayList;

/* renamed from: X.3DB  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3DB {
    public ImageView A00;
    public final ViewGroup A01;
    public final ImageView A02;
    public final ImageView A03;
    public final TextView A04;
    public final /* synthetic */ AnonymousClass2y2 A05;

    public AnonymousClass3DB(View view, AnonymousClass2y2 r5, int i) {
        this.A05 = r5;
        this.A04 = C12960it.A0J(view, R.id.date);
        this.A03 = C12970iu.A0L(view, R.id.album_thumb_status);
        ImageView A0L = C12970iu.A0L(view, R.id.thumb);
        this.A02 = A0L;
        C12960it.A0r(view.getContext(), A0L, R.string.action_open_image);
        this.A01 = (ViewGroup) AnonymousClass028.A0D(view, R.id.album_thumbnail_date_wrapper);
        A0L.setOnClickListener(new ViewOnClickCListenerShape2S0201000_I1(this, r5, i, 2));
        A0L.setOnLongClickListener(r5.A1a);
    }

    public void A00(AbstractC16130oV r5, ArrayList arrayList) {
        ImageView imageView = this.A02;
        AnonymousClass1IS r3 = r5.A0z;
        C12970iu.A1T(imageView, AbstractC28561Ob.A0W(r3), arrayList);
        TextView textView = this.A04;
        if (textView.getVisibility() == 0) {
            C12970iu.A1T(textView, AbstractC42671vd.A0Y(r5), arrayList);
        }
        ImageView imageView2 = this.A03;
        if (imageView2 != null) {
            StringBuilder sb = new StringBuilder("status-transition-");
            sb.append(r3);
            C12970iu.A1T(imageView2, sb.toString(), arrayList);
        }
    }
}
