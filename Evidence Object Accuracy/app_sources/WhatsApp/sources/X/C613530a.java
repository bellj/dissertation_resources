package X;

/* renamed from: X.30a  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C613530a extends AbstractC16110oT {
    public Boolean A00;
    public Double A01;
    public Integer A02;
    public Long A03;

    public C613530a() {
        super(468, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(9, this.A00);
        r3.Abe(4, this.A01);
        r3.Abe(1, this.A02);
        r3.Abe(3, this.A03);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamProfilePicUpload {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "profilePicIsAvatar", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "profilePicSize", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "profilePicUploadResult", C12960it.A0Y(this.A02));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "profilePicUploadT", this.A03);
        return C12960it.A0d("}", A0k);
    }
}
