package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.1uq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C42211uq {
    public final long A00;
    public final UserJid A01;
    public final String A02;

    public C42211uq(UserJid userJid, String str, long j) {
        this.A00 = j;
        this.A01 = userJid;
        this.A02 = str;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("id=");
        sb.append(this.A00);
        sb.append(" jid=");
        sb.append(this.A01);
        sb.append(" display=");
        sb.append(this.A02);
        return sb.toString();
    }
}
