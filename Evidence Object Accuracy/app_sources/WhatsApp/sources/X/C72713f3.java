package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

/* renamed from: X.3f3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C72713f3 extends AnimatorListenerAdapter {
    public final /* synthetic */ AbstractC28551Oa A00;

    public C72713f3(AbstractC28551Oa r1) {
        this.A00 = r1;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
        C72453ed.A1D(this.A00);
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        C72453ed.A1D(this.A00);
    }
}
