package X;

import android.content.Context;
import android.util.AttributeSet;
import com.whatsapp.WaImageView;

/* renamed from: X.3xs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC83773xs extends WaImageView {
    public AbstractC83773xs(Context context) {
        super(context);
        A00();
    }

    public AbstractC83773xs(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
    }

    public AbstractC83773xs(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
    }
}
