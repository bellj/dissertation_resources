package X;

/* renamed from: X.311  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass311 extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Long A02;
    public Long A03;
    public String A04;
    public String A05;
    public String A06;
    public String A07;

    public AnonymousClass311() {
        super(1368, new AnonymousClass00E(1, 1, 5), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(4, this.A04);
        r3.Abe(6, this.A00);
        r3.Abe(2, this.A01);
        r3.Abe(1, this.A05);
        r3.Abe(9, this.A06);
        r3.Abe(7, this.A02);
        r3.Abe(8, this.A07);
        r3.Abe(3, this.A03);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamAndroidEmojiDictionaryFetch {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "currentLanguages", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "doNetworkFetch", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "isFirstAttempt", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "requestedLanguages", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "result", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "resultHttpCode", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "resultLanguages", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "timeSinceLastRequestMsT", this.A03);
        return C12960it.A0d("}", A0k);
    }
}
