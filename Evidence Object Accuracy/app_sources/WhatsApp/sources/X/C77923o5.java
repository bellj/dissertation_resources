package X;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.text.TextUtils;

/* renamed from: X.3o5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77923o5 extends AbstractC77963o9 {
    public final Bundle A00 = C12970iu.A0D();

    @Override // X.AbstractC95064d1, X.AbstractC72443eb
    public final int AET() {
        return 12451000;
    }

    public C77923o5(Context context, Looper looper, AbstractC14980mM r11, AbstractC15000mO r12, AnonymousClass3BW r13) {
        super(context, looper, r11, r12, r13, 16);
    }

    @Override // X.AbstractC95064d1, X.AbstractC72443eb
    public final boolean Aae() {
        AnonymousClass3BW r2 = ((AbstractC77963o9) this).A00;
        if (!TextUtils.isEmpty(null)) {
            r2.A04.get(AnonymousClass4HU.A03);
            if (!r2.A05.isEmpty()) {
                return true;
            }
        }
        return false;
    }
}
