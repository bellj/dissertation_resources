package X;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.DatePicker;
import com.whatsapp.R;

/* renamed from: X.5aj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class DialogInterface$OnClickListenerC117805aj extends AnonymousClass04S implements DialogInterface.OnClickListener, DatePicker.OnDateChangedListener {
    public DatePickerDialog.OnDateSetListener A00;
    public final DatePicker A01;

    public DialogInterface$OnClickListenerC117805aj(DatePickerDialog.OnDateSetListener onDateSetListener, Context context, int i, int i2, int i3) {
        super(context, R.style.DatePickerDialog);
        DatePicker datePicker = new DatePicker(getContext());
        this.A01 = datePicker;
        AnonymousClass0U5 r1 = ((AnonymousClass04S) this).A00;
        r1.A0D = datePicker;
        r1.A06 = 0;
        A03(-1, context.getString(R.string.ok), this);
        A03(-2, context.getString(R.string.cancel), this);
        datePicker.init(i, i2, i3, this);
        this.A00 = onDateSetListener;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public void onClick(DialogInterface dialogInterface, int i) {
        DatePickerDialog.OnDateSetListener onDateSetListener;
        if (i == -2) {
            cancel();
        } else if (i == -1 && (onDateSetListener = this.A00) != null) {
            DatePicker datePicker = this.A01;
            datePicker.clearFocus();
            onDateSetListener.onDateSet(datePicker, datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());
        }
    }

    @Override // android.widget.DatePicker.OnDateChangedListener
    public void onDateChanged(DatePicker datePicker, int i, int i2, int i3) {
        this.A01.init(i, i2, i3, this);
    }

    @Override // android.app.Dialog
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        this.A01.init(bundle.getInt("year"), bundle.getInt("month"), bundle.getInt("day"), this);
    }

    @Override // android.app.Dialog
    public Bundle onSaveInstanceState() {
        Bundle onSaveInstanceState = super.onSaveInstanceState();
        DatePicker datePicker = this.A01;
        onSaveInstanceState.putInt("year", datePicker.getYear());
        onSaveInstanceState.putInt("month", datePicker.getMonth());
        onSaveInstanceState.putInt("day", datePicker.getDayOfMonth());
        return onSaveInstanceState;
    }
}
