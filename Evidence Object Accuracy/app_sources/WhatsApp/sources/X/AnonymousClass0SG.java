package X;

/* renamed from: X.0SG  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0SG {
    public float A00;
    public float A01;
    public float A02;
    public float A03;

    public AnonymousClass0SG(float f, float f2, float f3, float f4) {
        this.A01 = f;
        this.A02 = f2;
        this.A03 = f3;
        this.A00 = f4;
    }

    public AnonymousClass0SG(AnonymousClass0SG r2) {
        this.A01 = r2.A01;
        this.A02 = r2.A02;
        this.A03 = r2.A03;
        this.A00 = r2.A00;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        sb.append(this.A01);
        sb.append(" ");
        sb.append(this.A02);
        sb.append(" ");
        sb.append(this.A03);
        sb.append(" ");
        sb.append(this.A00);
        sb.append("]");
        return sb.toString();
    }
}
