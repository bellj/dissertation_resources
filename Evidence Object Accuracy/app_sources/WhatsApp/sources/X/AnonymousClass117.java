package X;

import android.accounts.Account;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.117  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass117 {
    public final C15570nT A00;
    public final AnonymousClass116 A01;
    public final C14820m6 A02;
    public final AnonymousClass018 A03;
    public final C16630pM A04;
    public final AbstractC14440lR A05;

    public AnonymousClass117(C15570nT r1, AnonymousClass116 r2, C14820m6 r3, AnonymousClass018 r4, C16630pM r5, AbstractC14440lR r6) {
        this.A00 = r1;
        this.A05 = r6;
        this.A03 = r4;
        this.A01 = r2;
        this.A02 = r3;
        this.A04 = r5;
    }

    public static final byte A00(String str) {
        if (str.equals("vnd.android.cursor.item/name")) {
            return 0;
        }
        if (str.equals("vnd.android.cursor.item/phone_v2")) {
            return 1;
        }
        if (str.equals("vnd.android.cursor.item/vnd.com.whatsapp.profile")) {
            return 2;
        }
        if (str.equals("vnd.android.cursor.item/vnd.com.whatsapp.voip.call")) {
            return 3;
        }
        if (str.equals("vnd.android.cursor.item/vnd.com.whatsapp.video.call")) {
            return 4;
        }
        StringBuilder sb = new StringBuilder("no code found for ");
        sb.append(str);
        throw new IllegalStateException(sb.toString());
    }

    public static final boolean A01(ContentResolver contentResolver, String str, ArrayList arrayList) {
        boolean z;
        try {
            try {
                contentResolver.applyBatch("com.android.contacts", arrayList);
                z = true;
            } catch (Exception e) {
                StringBuilder sb = new StringBuilder();
                sb.append("androidcontactssync/");
                sb.append(str);
                Log.e(sb.toString(), e);
                z = false;
            }
            return z;
        } finally {
            arrayList.clear();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0057 A[Catch: NullPointerException -> 0x0066, TryCatch #0 {NullPointerException -> 0x0066, blocks: (B:17:0x0051, B:19:0x0057, B:20:0x005b), top: B:29:0x0051 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.accounts.Account A02(android.content.Context r8) {
        /*
            r7 = this;
            java.lang.String r4 = "com.android.contacts"
            android.accounts.AccountManager r6 = android.accounts.AccountManager.get(r8)
            java.lang.String r5 = "com.whatsapp"
            android.accounts.Account[] r1 = r6.getAccountsByType(r5)
            int r0 = r1.length
            r2 = 0
            if (r0 == 0) goto L_0x0025
            r0 = 0
            r3 = r1[r0]
            r0 = 2131886300(0x7f1200dc, float:1.9407175E38)
            java.lang.String r1 = r8.getString(r0)
            java.lang.String r0 = r3.name
            boolean r0 = android.text.TextUtils.equals(r1, r0)
            if (r0 != 0) goto L_0x0051
            r6.removeAccount(r3, r2, r2)
        L_0x0025:
            X.0nT r0 = r7.A00
            r0.A08()
            X.1Ih r0 = r0.A05
            if (r0 != 0) goto L_0x0037
            java.lang.String r0 = "androidcontactssync/get-or-create-account null jid"
        L_0x0030:
            com.whatsapp.util.Log.e(r0)
            r3 = r2
        L_0x0034:
            if (r3 != 0) goto L_0x0051
            return r2
        L_0x0037:
            r0 = 2131886300(0x7f1200dc, float:1.9407175E38)
            java.lang.String r0 = r8.getString(r0)
            android.accounts.Account r3 = new android.accounts.Account
            r3.<init>(r0, r5)
            boolean r0 = r6.addAccountExplicitly(r3, r2, r2)
            if (r0 == 0) goto L_0x004e
            r0 = 1
            android.content.ContentResolver.setIsSyncable(r3, r4, r0)
            goto L_0x0034
        L_0x004e:
            java.lang.String r0 = "androidcontactssync/get-or-create-account failed to add account"
            goto L_0x0030
        L_0x0051:
            boolean r0 = android.content.ContentResolver.getSyncAutomatically(r3, r4)     // Catch: NullPointerException -> 0x0066
            if (r0 != 0) goto L_0x005b
            r0 = 1
            android.content.ContentResolver.setSyncAutomatically(r3, r4, r0)     // Catch: NullPointerException -> 0x0066
        L_0x005b:
            android.os.Bundle r2 = new android.os.Bundle     // Catch: NullPointerException -> 0x0066
            r2.<init>()     // Catch: NullPointerException -> 0x0066
            r0 = 3600(0xe10, double:1.7786E-320)
            android.content.ContentResolver.addPeriodicSync(r3, r4, r2, r0)     // Catch: NullPointerException -> 0x0066
            return r3
        L_0x0066:
            r2 = move-exception
            java.lang.String r0 = r2.getMessage()
            if (r0 == 0) goto L_0x007a
            java.lang.String r1 = r2.getMessage()
            java.lang.String r0 = "Attempt to invoke virtual method 'com.prism.gaia"
            boolean r0 = r1.startsWith(r0)
            if (r0 == 0) goto L_0x007a
            return r3
        L_0x007a:
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass117.A02(android.content.Context):android.accounts.Account");
    }

    public final synchronized void A03(Account account, Context context) {
        Uri build = ContactsContract.RawContacts.CONTENT_URI.buildUpon().appendQueryParameter("account_name", account.name).appendQueryParameter("account_type", account.type).appendQueryParameter("caller_is_syncadapter", "true").build();
        Uri build2 = ContactsContract.Data.CONTENT_URI.buildUpon().appendQueryParameter("caller_is_syncadapter", "true").build();
        ArrayList arrayList = new ArrayList();
        Cursor query = context.getContentResolver().query(build, new String[]{"_id", "sync1", "sync2", "display_name"}, null, null, null);
        if (query != null) {
            while (query.moveToNext()) {
                UserJid nullable = UserJid.getNullable(query.getString(1));
                if (nullable != null) {
                    arrayList.add(new C42211uq(nullable, query.getString(3), query.getLong(0)));
                }
            }
            query.close();
        }
        ArrayList arrayList2 = new ArrayList();
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            C42211uq r2 = (C42211uq) it.next();
            if (arrayList2.size() >= 100) {
                A01(context.getContentResolver(), "error updating contact data action strings", arrayList2);
            }
            AnonymousClass018 r8 = this.A03;
            String A0G = r8.A0G(C248917h.A00(C20920wX.A00(), r2.A01.user));
            String valueOf = String.valueOf(r2.A00);
            arrayList2.add(ContentProviderOperation.newUpdate(build2).withSelection("raw_contact_id=? and mimetype=?", new String[]{valueOf, "vnd.android.cursor.item/vnd.com.whatsapp.profile"}).withValue("data3", r8.A0B(R.string.account_sync_message_detail_format, A0G)).withYieldAllowed(true).build());
            arrayList2.add(ContentProviderOperation.newUpdate(build2).withSelection("raw_contact_id=? and mimetype=?", new String[]{valueOf, "vnd.android.cursor.item/vnd.com.whatsapp.voip.call"}).withValue("data3", r8.A0B(R.string.account_sync_voip_call_detail_format, A0G)).build());
            arrayList2.add(ContentProviderOperation.newUpdate(build2).withSelection("raw_contact_id=? and mimetype=?", new String[]{valueOf, "vnd.android.cursor.item/vnd.com.whatsapp.video.call"}).withValue("data3", r8.A0B(R.string.account_sync_video_call_detail_format, A0G)).build());
        }
        if (!arrayList2.isEmpty()) {
            A01(context.getContentResolver(), "error updating contact data action strings", arrayList2);
        }
    }
}
