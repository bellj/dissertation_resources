package X;

/* renamed from: X.5Nw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114995Nw extends AbstractC94944cn {
    public AnonymousClass5XI A00;

    public C114995Nw(AnonymousClass5XI r1) {
        this.A00 = r1;
    }

    public final byte[] A05() {
        AnonymousClass5XI r5 = this.A00;
        int ACZ = r5.ACZ();
        byte[] bArr = new byte[ACZ];
        byte[] bArr2 = this.A01;
        r5.update(bArr2, 0, bArr2.length);
        byte[] bArr3 = this.A02;
        r5.update(bArr3, 0, bArr3.length);
        r5.A97(bArr, 0);
        for (int i = 1; i < super.A00; i++) {
            r5.update(bArr, 0, ACZ);
            r5.A97(bArr, 0);
        }
        return bArr;
    }
}
