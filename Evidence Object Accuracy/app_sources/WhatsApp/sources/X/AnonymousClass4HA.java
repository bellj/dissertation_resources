package X;

import android.text.TextUtils;

/* renamed from: X.4HA  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4HA {
    public static final String A00;
    public static final String[] A01;

    static {
        String[] strArr = {"sender_timestamp"};
        A01 = strArr;
        A00 = TextUtils.join(",", strArr);
    }
}
