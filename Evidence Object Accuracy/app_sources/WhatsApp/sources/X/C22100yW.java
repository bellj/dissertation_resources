package X;

import android.content.ContentValues;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import com.facebook.redex.RunnableBRunnable0Shape0S0200100_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0220000_I0;
import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;
import com.facebook.redex.RunnableBRunnable0Shape4S0200000_I0_4;
import com.facebook.redex.RunnableBRunnable0Shape7S0200000_I0_7;
import com.whatsapp.identity.IdentityVerificationActivity;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.util.Log;
import com.whatsapp.util.RunnableTRunnableShape13S0200000_I0;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0yW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22100yW extends AbstractC16230of implements AbstractC15920o8 {
    public AnonymousClass1JM A00;
    public DeviceJid A01;
    public Comparator A02 = new Comparator() { // from class: X.1se
        @Override // java.util.Comparator
        public final int compare(Object obj, Object obj2) {
            AnonymousClass1JU r8 = (AnonymousClass1JU) obj;
            AnonymousClass1JU r9 = (AnonymousClass1JU) obj2;
            long j = r8.A01;
            if ((j != 0 || r9.A01 != 0) && (j == 0 || r9.A01 == 0)) {
                return j != 0 ? 1 : -1;
            }
            return (r9.A00 > r8.A00 ? 1 : (r9.A00 == r8.A00 ? 0 : -1));
        }
    };
    public boolean A03;
    public final Handler A04 = new Handler(Looper.getMainLooper());
    public final AbstractC15710nm A05;
    public final C244615p A06;
    public final C15570nT A07;
    public final C233711k A08;
    public final C18230s7 A09;
    public final AnonymousClass01d A0A;
    public final C14830m7 A0B;
    public final C16590pI A0C;
    public final C14820m6 A0D;
    public final AnonymousClass018 A0E;
    public final C15990oG A0F;
    public final C18240s8 A0G;
    public final C234211p A0H;
    public final C245916c A0I;
    public final AnonymousClass1E2 A0J;
    public final C17220qS A0K;
    public final C14840m8 A0L;
    public final ExecutorC27271Gr A0M;
    public final AbstractC14440lR A0N;
    public final Object A0O = new Object();
    public final Set A0P = new HashSet();

    @Override // X.AbstractC15920o8
    public int[] ADF() {
        return new int[]{213};
    }

    public C22100yW(AbstractC15710nm r4, C244615p r5, C15570nT r6, C233711k r7, C18230s7 r8, AnonymousClass01d r9, C14830m7 r10, C16590pI r11, C14820m6 r12, AnonymousClass018 r13, C15990oG r14, C18240s8 r15, C234211p r16, C245916c r17, AnonymousClass1E2 r18, C17220qS r19, C14840m8 r20, AbstractC14440lR r21) {
        this.A0B = r10;
        this.A09 = r8;
        this.A05 = r4;
        this.A07 = r6;
        this.A0C = r11;
        this.A0N = r21;
        this.A0I = r17;
        this.A0K = r19;
        this.A0G = r15;
        this.A0A = r9;
        this.A0E = r13;
        this.A0L = r20;
        this.A0F = r14;
        this.A0J = r18;
        this.A0D = r12;
        this.A06 = r5;
        this.A08 = r7;
        this.A0H = r16;
        this.A0M = new ExecutorC27271Gr(r21, true);
    }

    public C14580lf A05() {
        C14580lf r3 = new C14580lf();
        if (!this.A0L.A03()) {
            r3.A02(Boolean.FALSE);
            return r3;
        }
        this.A0N.Aaz(new C40991si(r3, this), new Void[0]);
        return r3;
    }

    public AnonymousClass1JU A06(int i) {
        if (i > 0 && this.A0L.A03()) {
            Iterator it = this.A0I.A04.A00().A01().iterator();
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                if (((DeviceJid) entry.getKey()).device == i) {
                    return (AnonymousClass1JU) entry.getValue();
                }
            }
        }
        return null;
    }

    public List A07() {
        if (!this.A0L.A03()) {
            return new ArrayList();
        }
        return new ArrayList(new ArrayList(this.A0I.A04.A00().A00.values()));
    }

    public List A08() {
        if (!this.A0L.A03()) {
            return new ArrayList();
        }
        return new ArrayList(new ArrayList(this.A0I.A00().A00.values()));
    }

    public List A09() {
        List A07 = A07();
        Collections.sort(A07, this.A02);
        ArrayList arrayList = new ArrayList();
        Iterator it = A07.iterator();
        long A00 = this.A0B.A00();
        int i = 0;
        while (it.hasNext()) {
            AnonymousClass1JU r10 = (AnonymousClass1JU) it.next();
            long j = r10.A01;
            if (j > 0) {
                if (C38121nY.A00(A00, j) > 5) {
                    it.remove();
                    arrayList.add(r10.A05);
                } else {
                    i++;
                }
            }
        }
        if (i > 20) {
            int size = A07.size();
            ArrayList arrayList2 = new ArrayList(A07.subList(size - (i - 20), size));
            A07.removeAll(arrayList2);
            Iterator it2 = arrayList2.iterator();
            while (it2.hasNext()) {
                arrayList.add(((AnonymousClass1JU) it2.next()).A05);
            }
        }
        if (!arrayList.isEmpty()) {
            this.A0I.A03(AnonymousClass1JO.A00(arrayList));
        }
        return A07;
    }

    public final void A0A(Location location, AnonymousClass1JU r13) {
        AnonymousClass1JU r0;
        String str = null;
        try {
            List<Address> fromLocation = new Geocoder(this.A0C.A00, AnonymousClass018.A00(this.A0E.A00)).getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            if (fromLocation != null) {
                for (Address address : fromLocation) {
                    str = address.getLocality();
                    if (!TextUtils.isEmpty(str)) {
                        break;
                    }
                }
            }
        } catch (IOException | IllegalArgumentException unused) {
        }
        if (!TextUtils.isEmpty(str)) {
            C245916c r02 = this.A0I;
            DeviceJid deviceJid = r13.A05;
            C246216f r8 = r02.A04;
            ContentValues contentValues = new ContentValues();
            contentValues.put("place_name", str);
            C16310on A02 = r8.A02.A02();
            try {
                A02.A03.A00("devices", contentValues, "device_id = ?", new String[]{deviceJid.getRawString()});
                synchronized (r8) {
                    C28601Of r03 = r8.A00;
                    if (!(r03 == null || (r0 = (AnonymousClass1JU) r03.A00.get(deviceJid)) == null)) {
                        r0.A02 = str;
                    }
                }
                A02.close();
                for (AnonymousClass1GD r1 : A01()) {
                    if (r1 instanceof C40971sf) {
                        ((C40971sf) r1).A00.A0R.A0A(null);
                    }
                }
            } catch (Throwable th) {
                try {
                    A02.close();
                } catch (Throwable unused2) {
                }
                throw th;
            }
        }
    }

    public final void A0B(AnonymousClass1JO r8) {
        for (AnonymousClass1GD r3 : A01()) {
            if (r3 instanceof C41021sl) {
                IdentityVerificationActivity identityVerificationActivity = ((C41021sl) r3).A00;
                C15570nT r0 = ((ActivityC13790kL) identityVerificationActivity).A01;
                r0.A08();
                identityVerificationActivity.A2k(r0.A05);
            } else if (r3 instanceof C41031sm) {
                C41031sm r32 = (C41031sm) r3;
                Iterator it = r8.iterator();
                while (it.hasNext()) {
                    r32.A00.A03.A05((DeviceJid) it.next());
                }
            } else if (r3 instanceof AnonymousClass1JN) {
                AnonymousClass1JN r33 = (AnonymousClass1JN) r3;
                C18850tA r1 = r33.A00;
                r1.A04.A08();
                r1.A0e.Ab2(new RunnableTRunnableShape13S0200000_I0(r33, r8));
            } else if (r3 instanceof C41041sn) {
                C41041sn r34 = (C41041sn) r3;
                C21220x4 r5 = r34.A01;
                Log.i("MDOptInManager/Deleting_Sync_Data");
                r5.A00.A0J(2);
                SharedPreferences sharedPreferences = r5.A01.A00;
                sharedPreferences.edit().remove("delete_chat_clear_chat_nux_accepted").apply();
                sharedPreferences.edit().putBoolean("companion_reg_opt_in_enabled", false).apply();
                AnonymousClass02O r12 = r34.A00;
                if (r12 != null) {
                    r12.apply(Boolean.TRUE);
                }
                r5.A02.A04(r34);
            } else if (r3 instanceof C41051so) {
                AnonymousClass16S r2 = ((C41051so) r3).A00;
                r2.A01.A08();
                if (r2.A03()) {
                    Log.i("SyncdDeleteAllDataApiHandler/onDeviceRemoved");
                    if (!(!r2.A08.A08().isEmpty())) {
                        for (AbstractC18880tD r02 : r2.A03.A01()) {
                            r02.AUt();
                        }
                        C233411h r52 = r2.A04;
                        C41071sq r13 = new C41071sq();
                        r13.A00 = Long.valueOf((long) (r2.A05.A01().getInt("syncd_dirty", -1) - 1));
                        r52.A06.A07(r13);
                    }
                    r2.A05.A05(0);
                    r2.A00();
                } else if (r2.A02.A02(AbstractC15460nI.A1f) > 0 && !(!r2.A08.A08().isEmpty())) {
                    Log.i("SyncdDeleteAllDataApiHandler/onDeviceRemoved setLastCompanionDeregTime");
                    C233711k r14 = r2.A05;
                    r14.A01().edit().putLong("syncd_last_companion_dereg_time", r2.A06.A00()).apply();
                }
            } else if (r3 instanceof C40971sf) {
                C40971sf r35 = (C40971sf) r3;
                r35.A00.A05.A0H(new RunnableBRunnable0Shape4S0100000_I0_4(r35, 11));
            } else if (r3 instanceof C41061sp) {
                C41061sp r36 = (C41061sp) r3;
                if (!r8.A00.isEmpty()) {
                    AnonymousClass10B r15 = r36.A00;
                    if (r15.A03()) {
                        r15.A00();
                    }
                }
            }
        }
    }

    public void A0C(DeviceJid deviceJid, String str, boolean z, boolean z2) {
        StringBuilder sb = new StringBuilder("companion-device-manager/logoutDeviceAndNotify: ");
        sb.append(deviceJid);
        sb.append(", removalReason ");
        sb.append(str);
        sb.append(", remove on error: ");
        sb.append(z);
        Log.i(sb.toString());
        C41011sk r9 = new C41011sk(new C41001sj(this, z2, z), this.A0K);
        AnonymousClass1YJ r0 = new AnonymousClass1YJ();
        r0.A02(deviceJid);
        r9.A00 = r0.A00();
        C17220qS r8 = r9.A02;
        String A01 = r8.A01();
        AnonymousClass009.A05(deviceJid);
        boolean A0D = r8.A0D(r9, new AnonymousClass1V8(new AnonymousClass1V8("remove-companion-device", new AnonymousClass1W9[]{new AnonymousClass1W9(deviceJid, "jid"), new AnonymousClass1W9("reason", str)}), "iq", new AnonymousClass1W9[]{new AnonymousClass1W9(AnonymousClass1VY.A00, "to"), new AnonymousClass1W9("id", A01), new AnonymousClass1W9("xmlns", "md"), new AnonymousClass1W9("type", "set")}), A01, 237, 32000);
        StringBuilder sb2 = new StringBuilder("app/sendRemoveDeviceRequest success: ");
        sb2.append(A0D);
        Log.i(sb2.toString());
        if (!A0D) {
            r9.A01.A00(r9.A00, -1);
        }
    }

    public final void A0D(String str) {
        synchronized (this.A0O) {
            AnonymousClass1JM r2 = this.A00;
            if (r2 != null) {
                StringBuilder sb = new StringBuilder();
                sb.append("companion-device-manager/device login canceled: ");
                sb.append(r2.A01.A05);
                Log.i(sb.toString());
                A0C(this.A00.A01.A05, str, true, false);
                this.A00 = null;
                this.A03 = false;
            }
        }
    }

    public void A0E(String str, boolean z) {
        StringBuilder sb = new StringBuilder("companion-device-manager/logoutAllCompanionDevicesAndNotify/remove on error: ");
        sb.append(z);
        sb.append(", removalReason ");
        sb.append(str);
        Log.i(sb.toString());
        AnonymousClass1JO A02 = this.A0I.A04.A00().A02();
        if (A02.A00.isEmpty()) {
            A0B(A02);
            return;
        }
        C41011sk r10 = new C41011sk(new C41001sj(this, false, z), this.A0K);
        r10.A00 = A02;
        C17220qS r9 = r10.A02;
        String A01 = r9.A01();
        boolean A0D = r9.A0D(r10, new AnonymousClass1V8(new AnonymousClass1V8("remove-companion-device", new AnonymousClass1W9[]{new AnonymousClass1W9("all", "true"), new AnonymousClass1W9("reason", str)}), "iq", new AnonymousClass1W9[]{new AnonymousClass1W9(AnonymousClass1VY.A00, "to"), new AnonymousClass1W9("id", A01), new AnonymousClass1W9("xmlns", "md"), new AnonymousClass1W9("type", "set")}), A01, 237, 32000);
        StringBuilder sb2 = new StringBuilder("app/sendRemoveAllDevicesRequest success: ");
        sb2.append(A0D);
        Log.i(sb2.toString());
        if (!A0D) {
            r10.A01.A00(A02, -1);
        }
    }

    public boolean A0F(C28601Of r13, boolean z, boolean z2) {
        Log.i("companion-device-manager/refreshDevices");
        C28601Of A00 = this.A0I.A00();
        C28601Of r5 = A00;
        Object obj = this.A0O;
        synchronized (obj) {
            if (this.A00 != null) {
                HashMap hashMap = new HashMap(A00.A00);
                hashMap.remove(this.A00.A01.A05);
                r5 = C28601Of.A00(hashMap);
            }
        }
        if (r13 != null) {
            AnonymousClass1YJ r7 = new AnonymousClass1YJ();
            Iterator it = r5.A01().iterator();
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                Object key = entry.getKey();
                Map map = r13.A00;
                if (!map.containsKey(key) || (z && ((Number) map.get(entry.getKey())).intValue() != ((AnonymousClass1JU) entry.getValue()).A03)) {
                    r7.A02(entry.getKey());
                }
            }
            AnonymousClass1JO A002 = r7.A00();
            Set set = A002.A00;
            if (!set.isEmpty()) {
                int size = set.size();
                int size2 = r5.A00.size();
                boolean z3 = false;
                if (size == size2) {
                    z3 = true;
                }
                StringBuilder sb = new StringBuilder("companion-device-manager/onDeviceRemovedByServer/devices: ");
                sb.append(A002);
                Log.i(sb.toString());
                this.A0M.execute(new RunnableBRunnable0Shape0S0220000_I0(A002, this, !z3));
            }
            HashMap hashMap2 = new HashMap();
            Iterator it2 = r13.A01().iterator();
            while (it2.hasNext()) {
                Map.Entry entry2 = (Map.Entry) it2.next();
                Object key2 = entry2.getKey();
                C15570nT r0 = this.A07;
                r0.A08();
                if (!key2.equals(r0.A04)) {
                    Object key3 = entry2.getKey();
                    Map map2 = A00.A00;
                    if (!map2.containsKey(key3) || (z && ((AnonymousClass1JU) map2.get(entry2.getKey())).A03 != ((Number) entry2.getValue()).intValue())) {
                        hashMap2.put(entry2.getKey(), entry2.getValue());
                    }
                }
            }
            if (!hashMap2.isEmpty()) {
                for (Map.Entry entry3 : hashMap2.entrySet()) {
                    synchronized (obj) {
                        AnonymousClass1JM r02 = this.A00;
                        if (r02 == null || !r02.A01.A05.equals(entry3.getKey())) {
                            this.A07.A08();
                            A0C((DeviceJid) entry3.getKey(), "unknown_companion", true, false);
                            AbstractC15710nm r52 = this.A05;
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("toAdd=");
                            sb2.append(hashMap2.keySet());
                            r52.AaV("ContactSyncDevicesUpdater/update add unknown device of self", sb2.toString(), false);
                        } else if (z2) {
                            AnonymousClass1JM r53 = this.A00;
                            StringBuilder sb3 = new StringBuilder("companion-device-manager/device registered: ");
                            sb3.append(r53.A01.A05);
                            Log.i(sb3.toString());
                            for (AnonymousClass1GD r9 : A01()) {
                                if (r9 instanceof C41081sr) {
                                    C41081sr r92 = (C41081sr) r9;
                                    Log.i("SeamlessManager_CompanionDeviceObserver/onDeviceRegistered");
                                    AnonymousClass1JM r03 = r92.A00.A00;
                                    if (r03 != null && r03.equals(r53)) {
                                        C18910tG r2 = r92.A02;
                                        synchronized (r2) {
                                            r2.A0B.AaP(r2.A0C);
                                            r2.A01 = false;
                                        }
                                    }
                                } else if (r9 instanceof C41091ss) {
                                    C41091ss r93 = (C41091ss) r9;
                                    r93.A00.runOnUiThread(new RunnableBRunnable0Shape7S0200000_I0_7(r93, 21, r53));
                                }
                            }
                            this.A08.A01().edit().remove("syncd_last_fatal_error_time").apply();
                            this.A0M.execute(new RunnableBRunnable0Shape4S0200000_I0_4(this, 42, r53));
                        }
                    }
                }
            }
            return !set.isEmpty();
        } else if (r5.A00.isEmpty()) {
            return false;
        } else {
            AnonymousClass1JO A02 = r5.A02();
            StringBuilder sb4 = new StringBuilder("companion-device-manager/onDeviceRemovedByServer/devices: ");
            sb4.append(A02);
            Log.i(sb4.toString());
            this.A0M.execute(new RunnableBRunnable0Shape0S0220000_I0(A02, this, false));
            return true;
        }
    }

    public boolean A0G(DeviceJid deviceJid) {
        boolean z;
        AnonymousClass1JM r0;
        synchronized (this.A0O) {
            DeviceJid deviceJid2 = this.A01;
            if ((deviceJid2 == null || !deviceJid2.equals(deviceJid)) && ((r0 = this.A00) == null || !r0.A01.A05.equals(deviceJid) || !this.A03)) {
                z = false;
            }
            z = true;
        }
        return z;
    }

    @Override // X.AbstractC15920o8
    public boolean AI8(Message message, int i) {
        long A00;
        if (i != 213) {
            return false;
        }
        if (message == null) {
            return true;
        }
        AnonymousClass1V8 r4 = (AnonymousClass1V8) message.obj;
        DeviceJid deviceJid = (DeviceJid) r4.A0B(this.A05, DeviceJid.class, "from");
        if (deviceJid == null || !this.A07.A0E(deviceJid)) {
            return true;
        }
        String A0I = r4.A0I("type", null);
        if (A0I == null || "available".equals(A0I)) {
            A00 = this.A0B.A00();
            this.A0P.add(deviceJid);
        } else if (!"unavailable".equals(A0I)) {
            return true;
        } else {
            A00 = C40981sh.A00(r4);
            this.A0P.remove(deviceJid);
        }
        if (A00 == 0) {
            return true;
        }
        this.A0N.Ab2(new RunnableBRunnable0Shape0S0200100_I0(this, deviceJid, 5, A00));
        return true;
    }
}
