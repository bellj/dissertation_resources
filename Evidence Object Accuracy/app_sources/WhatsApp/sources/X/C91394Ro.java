package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.4Ro  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C91394Ro {
    public final long A00;
    public final C15580nU A01;
    public final UserJid A02;
    public final String A03;

    public C91394Ro(C15580nU r1, UserJid userJid, String str, long j) {
        this.A02 = userJid;
        this.A01 = r1;
        this.A03 = str;
        this.A00 = j;
    }
}
