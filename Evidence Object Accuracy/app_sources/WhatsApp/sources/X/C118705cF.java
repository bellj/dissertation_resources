package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5cF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118705cF extends AnonymousClass03U {
    public final TextView A00;

    public C118705cF(View view) {
        super(view);
        this.A00 = C12960it.A0J(view, R.id.header_text);
    }
}
