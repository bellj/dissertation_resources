package X;

import android.content.SharedPreferences;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.0qD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17070qD {
    public C38051nR A00;
    public AbstractC248317b A01;
    public C38031nP A02;
    public boolean A03;
    public final AbstractC15710nm A04;
    public final C15450nH A05;
    public final C16590pI A06;
    public final AnonymousClass018 A07;
    public final C20370ve A08;
    public final C241414j A09;
    public final C21860y6 A0A;
    public final C241314i A0B;
    public final C18660so A0C;
    public final C17900ra A0D;
    public final C22710zW A0E;
    public final C241514k A0F;
    public final C30931Zj A0G = C30931Zj.A00("PaymentsManager", "infra", "COMMON");
    public final AbstractC14440lR A0H;

    public C17070qD(AbstractC15710nm r4, C15450nH r5, C16590pI r6, AnonymousClass018 r7, C20370ve r8, C241414j r9, C21860y6 r10, C241314i r11, C18660so r12, C17900ra r13, C22710zW r14, C241514k r15, AbstractC14440lR r16) {
        this.A04 = r4;
        this.A06 = r6;
        this.A0H = r16;
        this.A09 = r9;
        this.A05 = r5;
        this.A07 = r7;
        this.A0A = r10;
        this.A0F = r15;
        this.A0E = r14;
        this.A0D = r13;
        this.A0B = r11;
        this.A08 = r8;
        this.A0C = r12;
    }

    public C38051nR A00() {
        A03();
        C38051nR r0 = this.A00;
        AnonymousClass009.A05(r0);
        return r0;
    }

    public synchronized AbstractC38041nQ A01(String str) {
        AbstractC38041nQ r0;
        A03();
        AbstractC248317b r02 = this.A01;
        if (r02 == null) {
            r0 = null;
        } else {
            r0 = r02.AIy(str);
        }
        return r0;
    }

    @Deprecated
    public synchronized AbstractC16830pp A02() {
        C38031nP r0;
        A03();
        r0 = this.A02;
        AnonymousClass009.A05(r0);
        return r0;
    }

    public final synchronized void A03() {
        if (!this.A03) {
            AbstractC248317b r0 = this.A01;
            if (r0 == null) {
                r0 = (AbstractC248317b) ((AnonymousClass01J) AnonymousClass01M.A00(this.A06.A00, AnonymousClass01J.class)).AEK.get();
                this.A01 = r0;
            }
            this.A01 = r0;
            if (r0 == null) {
                this.A0G.A05("initialize/paymentConfig is null");
            } else {
                this.A02 = new C38031nP(this.A05, this.A07, this.A0D, r0.AGf());
                C241414j r5 = this.A09;
                AbstractC248317b r02 = this.A01;
                synchronized (r5) {
                    r5.A01 = r02;
                    C38061nS r1 = new C38061nS(r5);
                    if (!r5.A07) {
                        r5.A00 = new AnonymousClass1IQ(r5.A04.A00, r5.A02, r5.A06, Collections.singleton(r1));
                        r5.A07 = true;
                    }
                }
                C20370ve r3 = this.A08;
                AbstractC248317b r2 = this.A01;
                r3.A00 = r2;
                this.A0F.A00 = r2;
                this.A00 = new C38051nR(r3, r5, r2, this.A0H);
                this.A03 = true;
                this.A0G.A06("initialized");
            }
        }
    }

    public void A04(AnonymousClass1FK r7) {
        Map map;
        boolean z;
        A03();
        C241314i r4 = this.A0B;
        if (r4 != null) {
            synchronized (r4) {
                map = r4.A00;
                z = false;
                if (map.size() > 0) {
                    z = true;
                }
            }
            if (z) {
                synchronized (r4) {
                    HashSet hashSet = new HashSet();
                    for (String str : map.keySet()) {
                        if (map.get(str) == r7) {
                            hashSet.add(str);
                        }
                    }
                    Iterator it = hashSet.iterator();
                    while (it.hasNext()) {
                        map.remove((String) it.next());
                    }
                }
            }
        }
    }

    public synchronized void A05(boolean z, boolean z2) {
        String str;
        String str2;
        String str3;
        this.A0G.A06("reset");
        A03();
        this.A03 = false;
        C17900ra r3 = this.A0D;
        synchronized (r3) {
            r3.A05.A03(null, "reset country");
            r3.A00 = null;
            r3.A01 = false;
        }
        if (this.A09.A07 && !z2) {
            C38051nR r0 = this.A00;
            r0.A03.Aaz(new C38071nT(r0), new Void[0]);
        }
        this.A0B.A00();
        if (z) {
            this.A0A.A04();
        } else {
            C21860y6 r32 = this.A0A;
            synchronized (r32) {
                C18600si r1 = r32.A00;
                boolean z3 = r32.A02;
                SharedPreferences A01 = r1.A01();
                if (z3) {
                    str = "payments_setup_completed_steps";
                } else {
                    str = "payments_merchant_setup_completed_steps";
                }
                String string = A01.getString(str, "");
                boolean z4 = r32.A00.A01().getBoolean("payments_sandbox", false);
                r32.A00.A0K(r32.A02);
                if (!TextUtils.isEmpty(string)) {
                    try {
                        JSONObject jSONObject = new JSONObject(string);
                        Iterator<String> keys = jSONObject.keys();
                        ArrayList arrayList = new ArrayList();
                        while (keys.hasNext()) {
                            String next = keys.next();
                            if (!"tos_no_wallet".equals(next)) {
                                arrayList.add(next);
                            } else if (z4) {
                                r32.A00.A0L(z4);
                            }
                        }
                        Iterator it = arrayList.iterator();
                        while (it.hasNext()) {
                            jSONObject.remove((String) it.next());
                        }
                        C18600si r02 = r32.A00;
                        boolean z5 = r32.A02;
                        String obj = jSONObject.toString();
                        SharedPreferences.Editor edit = r02.A01().edit();
                        if (z5) {
                            str3 = "payments_setup_completed_steps";
                        } else {
                            str3 = "payments_merchant_setup_completed_steps";
                        }
                        edit.putString(str3, obj).apply();
                    } catch (JSONException e) {
                        r32.A03.A0A("clearAllButTos threw: ", e);
                    }
                }
                C30931Zj r4 = r32.A03;
                StringBuilder sb = new StringBuilder();
                sb.append("clearAllButTos ended with steps: ");
                C18600si r12 = r32.A00;
                boolean z6 = r32.A02;
                SharedPreferences A012 = r12.A01();
                if (z6) {
                    str2 = "payments_setup_completed_steps";
                } else {
                    str2 = "payments_merchant_setup_completed_steps";
                }
                sb.append(A012.getString(str2, ""));
                sb.append(" sandbox: ");
                sb.append(r32.A00.A01().getBoolean("payments_sandbox", false));
                r4.A06(sb.toString());
            }
        }
        AnonymousClass17Y ABn = this.A02.ABn();
        if (ABn != null) {
            ABn.A8k();
        }
        AnonymousClass18N ABo = this.A02.ABo();
        if (ABo != null) {
            ABo.clear();
            ABo.Acr();
        }
    }
}
