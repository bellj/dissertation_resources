package X;

import android.hardware.camera2.CaptureRequest;

/* renamed from: X.5cm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119035cm extends AbstractC129405xf {
    public final /* synthetic */ CaptureRequest.Builder A00;
    public final /* synthetic */ AbstractC129405xf A01;
    public final /* synthetic */ C1308060a A02;
    public final /* synthetic */ AnonymousClass66I A03;
    public final /* synthetic */ boolean A04;

    public C119035cm(CaptureRequest.Builder builder, AbstractC129405xf r2, C1308060a r3, AnonymousClass66I r4, boolean z) {
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = builder;
        this.A03 = r4;
        this.A04 = z;
    }
}
