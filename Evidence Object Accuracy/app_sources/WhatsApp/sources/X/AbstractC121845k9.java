package X;

import android.content.Context;

/* renamed from: X.5k9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC121845k9 extends AnonymousClass6CT {
    public final Context A00;
    public final AnonymousClass018 A01;
    public final C17070qD A02;
    public final AnonymousClass61F A03;

    public AbstractC121845k9(Context context, AnonymousClass018 r2, C17070qD r3, AnonymousClass61F r4) {
        this.A00 = context;
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = r4;
    }
}
