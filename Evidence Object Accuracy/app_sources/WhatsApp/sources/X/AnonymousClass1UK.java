package X;

import com.google.android.gms.common.api.internal.BasePendingResult;

/* renamed from: X.1UK  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1UK {
    public void A00(AbstractC50792Ra r8) {
        BasePendingResult basePendingResult = (BasePendingResult) this;
        synchronized (basePendingResult.A06) {
            if (basePendingResult.A09.getCount() == 0) {
                r8.AON(basePendingResult.A01);
            } else {
                basePendingResult.A08.add(r8);
            }
        }
    }
}
