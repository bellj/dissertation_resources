package X;

import android.graphics.PointF;
import android.util.Property;
import android.view.View;

/* renamed from: X.0Al  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C02210Al extends Property {
    public C02210Al() {
        super(PointF.class, "position");
    }

    @Override // android.util.Property
    public Object get(Object obj) {
        return null;
    }

    @Override // android.util.Property
    public void set(Object obj, Object obj2) {
        View view = (View) obj;
        PointF pointF = (PointF) obj2;
        int round = Math.round(pointF.x);
        int round2 = Math.round(pointF.y);
        AnonymousClass0U3.A04.A06(view, round, round2, view.getWidth() + round, view.getHeight() + round2);
    }
}
