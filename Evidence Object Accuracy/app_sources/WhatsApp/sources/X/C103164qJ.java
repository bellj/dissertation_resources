package X;

import android.content.Context;
import com.whatsapp.location.GroupChatLiveLocationsActivity2;

/* renamed from: X.4qJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103164qJ implements AbstractC009204q {
    public final /* synthetic */ GroupChatLiveLocationsActivity2 A00;

    public C103164qJ(GroupChatLiveLocationsActivity2 groupChatLiveLocationsActivity2) {
        this.A00 = groupChatLiveLocationsActivity2;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
