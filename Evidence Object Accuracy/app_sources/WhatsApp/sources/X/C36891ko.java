package X;

import android.animation.ObjectAnimator;
import android.content.res.Resources;
import android.os.Build;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.conversationslist.ConversationsFragment;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape14S0100000_I0_1;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collections;

/* renamed from: X.1ko */
/* loaded from: classes2.dex */
public class C36891ko {
    public static final int[] A0E = {R.id.contact_icon1, R.id.contact_icon2, R.id.contact_icon3, R.id.contact_icon4, R.id.contact_icon5};
    public ObjectAnimator A00;
    public AnonymousClass37Z A01;
    public AbstractView$OnClickListenerC34281fs A02;
    public boolean A03;
    public boolean A04;
    public final C15550nR A05;
    public final C15610nY A06;
    public final AnonymousClass10T A07;
    public final AnonymousClass1J1 A08;
    public final ConversationsFragment A09;
    public final AnonymousClass018 A0A;
    public final C14850m9 A0B;
    public final C16120oU A0C;
    public final ExecutorC27271Gr A0D;

    public C36891ko(C15550nR r3, C15610nY r4, AnonymousClass10T r5, AnonymousClass1J1 r6, ConversationsFragment conversationsFragment, AnonymousClass018 r8, C14850m9 r9, C16120oU r10, AbstractC14440lR r11) {
        this.A09 = conversationsFragment;
        this.A0B = r9;
        this.A0C = r10;
        this.A05 = r3;
        this.A06 = r4;
        this.A0A = r8;
        this.A07 = r5;
        this.A08 = r6;
        this.A0D = new ExecutorC27271Gr(r11, false);
    }

    public static /* synthetic */ void A00(AnonymousClass01T r14, C36891ko r15) {
        AbstractList abstractList;
        int intValue;
        Resources resources;
        String quantityString;
        r15.A01 = null;
        ConversationsFragment conversationsFragment = r15.A09;
        View view = ((AnonymousClass01E) conversationsFragment).A0A;
        ActivityC000900k A0B = conversationsFragment.A0B();
        if (view == null || A0B == null || A0B.isFinishing() || r14 == null) {
            Log.w("conversations/updateNuxView: NUX view cannot be updated");
            return;
        }
        View findViewById = view.findViewById(R.id.conversations_empty_nux);
        Object obj = r14.A00;
        if (obj == null) {
            abstractList = new ArrayList();
        } else {
            abstractList = (AbstractList) obj;
        }
        Object obj2 = r14.A01;
        if (obj2 == null) {
            intValue = 0;
        } else {
            intValue = ((Number) obj2).intValue();
        }
        int size = abstractList.size();
        int[] iArr = A0E;
        int length = iArr.length;
        boolean z = false;
        if (size <= length) {
            z = true;
        }
        AnonymousClass009.A0F(z);
        if (Build.VERSION.SDK_INT <= 16) {
            Collections.reverse(abstractList);
        }
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < size; i++) {
            C15370n3 r142 = (C15370n3) abstractList.get(i);
            ImageView imageView = (ImageView) findViewById.findViewById(iArr[i]);
            r15.A08.A06(imageView, r142);
            imageView.setVisibility(0);
            imageView.setOnClickListener(r15.A02);
            String escapeHtml = Html.escapeHtml(r15.A06.A0A(r142, -1));
            arrayList.add(escapeHtml);
            imageView.setContentDescription(escapeHtml);
        }
        for (int i2 = size; i2 < length; i2++) {
            findViewById.findViewById(iArr[i2]).setVisibility(8);
        }
        if (!r15.A04) {
            intValue -= Math.min(size, 3);
            if (intValue > 0) {
                resources = A0B.getResources();
                if (size != 0) {
                    if (size == 1) {
                        quantityString = resources.getQuantityString(R.plurals.nux_one_more_contact_prompt, intValue, arrayList.get(0), Integer.valueOf(intValue));
                    } else if (size != 2) {
                        quantityString = resources.getQuantityString(R.plurals.nux_three_more_contact_prompt, intValue, arrayList.get(0), arrayList.get(1), arrayList.get(2), Integer.valueOf(intValue));
                    } else {
                        quantityString = resources.getQuantityString(R.plurals.nux_two_more_contact_prompt, intValue, arrayList.get(0), arrayList.get(1), Integer.valueOf(intValue));
                    }
                }
                quantityString = resources.getQuantityString(R.plurals.nux_abbreviated_prompt, intValue, Integer.valueOf(intValue));
            } else if (size != 0) {
                if (size == 1) {
                    quantityString = A0B.getString(R.string.nux_one_contact_prompt, arrayList.get(0));
                } else if (size != 2) {
                    quantityString = A0B.getString(R.string.nux_three_contact_prompt, arrayList.get(0), arrayList.get(1), arrayList.get(2));
                } else {
                    quantityString = A0B.getString(R.string.nux_two_contact_prompt, arrayList.get(0), arrayList.get(1));
                }
            }
            TextView textView = (TextView) AnonymousClass028.A0D(findViewById, R.id.prompt_text);
            textView.setText(Html.fromHtml(quantityString));
            textView.setVisibility(0);
            TextView textView2 = (TextView) AnonymousClass028.A0D(findViewById, R.id.instruction_text);
            C27531Hw.A06(textView2);
            textView2.setOnClickListener(new ViewOnClickCListenerShape14S0100000_I0_1(r15, 24));
            textView.setOnClickListener(new ViewOnClickCListenerShape14S0100000_I0_1(r15, 25));
        }
        resources = A0B.getResources();
        quantityString = resources.getQuantityString(R.plurals.nux_abbreviated_prompt, intValue, Integer.valueOf(intValue));
        TextView textView = (TextView) AnonymousClass028.A0D(findViewById, R.id.prompt_text);
        textView.setText(Html.fromHtml(quantityString));
        textView.setVisibility(0);
        TextView textView2 = (TextView) AnonymousClass028.A0D(findViewById, R.id.instruction_text);
        C27531Hw.A06(textView2);
        textView2.setOnClickListener(new ViewOnClickCListenerShape14S0100000_I0_1(r15, 24));
        textView.setOnClickListener(new ViewOnClickCListenerShape14S0100000_I0_1(r15, 25));
    }

    public void A01() {
        if (this.A03) {
            ConversationsFragment conversationsFragment = this.A09;
            View findViewById = conversationsFragment.A05().findViewById(R.id.conversations_empty_nux);
            if (this.A02 == null) {
                boolean z = false;
                if ((conversationsFragment.A02().getConfiguration().screenLayout & 15) == 1) {
                    z = true;
                }
                this.A04 = z;
                AnonymousClass018 r3 = this.A0A;
                if (r3.A04().A06) {
                    findViewById.findViewById(R.id.instruction_arrow).setRotationY(180.0f);
                }
                if (Build.VERSION.SDK_INT >= 17) {
                    View findViewById2 = findViewById.findViewById(R.id.linear_layout);
                    if (!r3.A04().A06) {
                        AnonymousClass028.A0c(findViewById2, 1);
                    } else {
                        AnonymousClass028.A0c(findViewById2, 0);
                    }
                }
                this.A02 = new ViewOnClickCListenerShape14S0100000_I0_1(this, 23);
            }
            AnonymousClass37Z r1 = this.A01;
            if (r1 != null) {
                r1.A03(true);
            }
            this.A03 = false;
            ExecutorC27271Gr r5 = this.A0D;
            r5.A00();
            C15550nR r32 = this.A05;
            AnonymousClass10T r12 = this.A07;
            C14850m9 r2 = this.A0B;
            AnonymousClass37Z r0 = new AnonymousClass37Z(r32, r12, this, r2);
            this.A01 = r0;
            ((AbstractC16350or) r0).A02.executeOnExecutor(r5, new Object[0]);
            this.A03 = false;
            if (r2.A07(1266)) {
                AnonymousClass028.A0D(findViewById, R.id.instruction_text).setVisibility(8);
                AnonymousClass028.A0D(findViewById, R.id.instruction_badge).setVisibility(8);
                AnonymousClass028.A0D(findViewById, R.id.instruction_arrow).setVisibility(8);
            }
        }
    }
}
