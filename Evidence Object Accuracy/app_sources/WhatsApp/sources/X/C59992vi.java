package X;

import android.content.Context;
import com.facebook.redex.ViewOnClickCListenerShape7S0100000_I1_1;
import com.google.android.material.chip.Chip;
import com.whatsapp.R;
import java.util.Set;

/* renamed from: X.2vi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59992vi extends C60052vo {
    public C59992vi(Chip chip, AnonymousClass2Jw r2) {
        super(chip, r2);
    }

    @Override // X.C60052vo, X.AbstractC75703kH
    public void A08(AnonymousClass4UW r9) {
        String string;
        Chip chip = ((C60052vo) this).A00;
        chip.setChipIconResource(R.drawable.ic_business_cat_restaurant);
        super.A08(r9);
        Set set = ((C59682vD) r9).A00;
        if (set.size() == 0) {
            string = chip.getContext().getString(R.string.biz_dir_categories);
        } else if (set.size() == 1) {
            string = ((C30211Wn) set.iterator().next()).A01;
        } else {
            Context context = chip.getContext();
            Object[] objArr = new Object[1];
            C12960it.A1P(objArr, set.size(), 0);
            string = context.getString(R.string.biz_dir_number_of_categories, objArr);
        }
        chip.setText(string);
        chip.setCloseIconVisible(true);
        C12960it.A0r(chip.getContext(), chip, R.string.biz_dir_categories);
        C12960it.A0z(chip, this, 19);
        chip.A02 = new ViewOnClickCListenerShape7S0100000_I1_1(this, 18);
    }
}
