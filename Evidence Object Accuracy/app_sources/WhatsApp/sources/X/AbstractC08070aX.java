package X;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.os.Build;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.0aX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC08070aX implements AbstractC12030hG, AbstractC12480hz, AbstractC12860ig {
    public Paint A00;
    public AnonymousClass0H1 A01;
    public AnonymousClass0NO A02;
    public AbstractC08070aX A03;
    public AbstractC08070aX A04;
    public List A05;
    public boolean A06;
    public boolean A07;
    public final Matrix A08;
    public final Matrix A09 = new Matrix();
    public final Paint A0A;
    public final Paint A0B = new C020609t(1);
    public final Paint A0C = new C020609t(1, PorterDuff.Mode.DST_IN);
    public final Paint A0D = new C020609t(1, PorterDuff.Mode.DST_OUT);
    public final Paint A0E;
    public final Path A0F = new Path();
    public final RectF A0G;
    public final RectF A0H;
    public final RectF A0I;
    public final RectF A0J;
    public final AnonymousClass0AA A0K;
    public final AnonymousClass0QD A0L;
    public final AnonymousClass0PU A0M;
    public final String A0N;
    public final List A0O;

    public void A04(C06430To r1, C06430To r2, List list, int i) {
    }

    public abstract void A06(Canvas canvas, Matrix matrix, int i);

    @Override // X.AbstractC12470hy
    public void Aby(List list, List list2) {
    }

    public AbstractC08070aX(AnonymousClass0AA r5, AnonymousClass0PU r6) {
        PorterDuff.Mode mode;
        C020609t r2 = new C020609t(1);
        this.A0E = r2;
        this.A0A = new C020609t(PorterDuff.Mode.CLEAR);
        this.A0I = new RectF();
        this.A0G = new RectF();
        this.A0H = new RectF();
        this.A0J = new RectF();
        this.A08 = new Matrix();
        this.A0O = new ArrayList();
        this.A07 = true;
        this.A0K = r5;
        this.A0M = r6;
        StringBuilder sb = new StringBuilder();
        sb.append(r6.A0G);
        sb.append("#draw");
        this.A0N = sb.toString();
        if (r6.A0F == AnonymousClass0J6.INVERT) {
            mode = PorterDuff.Mode.DST_OUT;
        } else {
            mode = PorterDuff.Mode.DST_IN;
        }
        r2.setXfermode(new PorterDuffXfermode(mode));
        AnonymousClass0QD r0 = new AnonymousClass0QD(r6.A0D);
        this.A0L = r0;
        r0.A02(this);
        List list = r6.A0J;
        if (list != null && !list.isEmpty()) {
            AnonymousClass0NO r02 = new AnonymousClass0NO(list);
            this.A02 = r02;
            for (AnonymousClass0QR r03 : r02.A00) {
                r03.A07.add(this);
            }
            for (AnonymousClass0QR r04 : this.A02.A02) {
                A03(r04);
                r04.A07.add(this);
            }
        }
        List list2 = this.A0M.A0I;
        boolean z = true;
        if (!list2.isEmpty()) {
            AnonymousClass0H1 r05 = new AnonymousClass0H1(list2);
            this.A01 = r05;
            r05.A05 = true;
            r05.A07.add(new C08030aT(this));
            z = ((Number) this.A01.A03()).floatValue() != 1.0f ? false : z;
            if (z != this.A07) {
                this.A07 = z;
                this.A0K.invalidateSelf();
            }
            A03(this.A01);
        } else if (true != this.A07) {
            this.A07 = true;
            this.A0K.invalidateSelf();
        }
    }

    public final void A00() {
        if (this.A05 == null) {
            AbstractC08070aX r1 = this.A04;
            if (r1 == null) {
                this.A05 = Collections.emptyList();
                return;
            }
            this.A05 = new ArrayList();
            do {
                this.A05.add(r1);
                r1 = r1.A04;
            } while (r1 != null);
        }
    }

    public void A01(float f) {
        AnonymousClass0QD r1 = this.A0L;
        AnonymousClass0QR r0 = r1.A02;
        if (r0 != null) {
            r0.A07(f);
        }
        AnonymousClass0QR r02 = r1.A06;
        if (r02 != null) {
            r02.A07(f);
        }
        AnonymousClass0QR r03 = r1.A01;
        if (r03 != null) {
            r03.A07(f);
        }
        AnonymousClass0QR r04 = r1.A00;
        if (r04 != null) {
            r04.A07(f);
        }
        AnonymousClass0QR r05 = r1.A03;
        if (r05 != null) {
            r05.A07(f);
        }
        AnonymousClass0QR r06 = r1.A05;
        if (r06 != null) {
            r06.A07(f);
        }
        AnonymousClass0QR r07 = r1.A04;
        if (r07 != null) {
            r07.A07(f);
        }
        AnonymousClass0H1 r08 = r1.A07;
        if (r08 != null) {
            r08.A07(f);
        }
        AnonymousClass0H1 r09 = r1.A08;
        if (r09 != null) {
            r09.A07(f);
        }
        AnonymousClass0NO r4 = this.A02;
        int i = 0;
        if (r4 != null) {
            int i2 = 0;
            while (true) {
                List list = r4.A00;
                if (i2 >= list.size()) {
                    break;
                }
                ((AnonymousClass0QR) list.get(i2)).A07(f);
                i2++;
            }
        }
        float f2 = this.A0M.A01;
        if (f2 != 0.0f) {
            f /= f2;
        }
        AnonymousClass0H1 r12 = this.A01;
        if (r12 != null) {
            r12.A07(f / f2);
        }
        AbstractC08070aX r13 = this.A03;
        if (r13 != null) {
            r13.A01(r13.A0M.A01 * f);
        }
        while (true) {
            List list2 = this.A0O;
            if (i < list2.size()) {
                ((AnonymousClass0QR) list2.get(i)).A07(f);
                i++;
            } else {
                return;
            }
        }
    }

    public final void A02(Canvas canvas) {
        RectF rectF = this.A0I;
        canvas.drawRect(rectF.left - 1.0f, rectF.top - 1.0f, rectF.right + 1.0f, rectF.bottom + 1.0f, this.A0A);
        AnonymousClass0MI.A00();
    }

    public void A03(AnonymousClass0QR r2) {
        if (r2 != null) {
            this.A0O.add(r2);
        }
    }

    public void A05(boolean z) {
        if (z && this.A00 == null) {
            this.A00 = new C020609t();
        }
        this.A06 = z;
    }

    @Override // X.AbstractC12480hz
    public void A5q(AnonymousClass0SF r2, Object obj) {
        this.A0L.A04(r2, obj);
    }

    @Override // X.AbstractC12860ig
    public void A9C(Canvas canvas, Matrix matrix, int i) {
        int intValue;
        Paint paint;
        Paint paint2;
        AnonymousClass0NO r0;
        if (this.A07) {
            AnonymousClass0PU r7 = this.A0M;
            if (!r7.A0L) {
                A00();
                Matrix matrix2 = this.A09;
                matrix2.reset();
                matrix2.set(matrix);
                int size = this.A05.size();
                while (true) {
                    size--;
                    if (size < 0) {
                        break;
                    }
                    matrix2.preConcat(((AbstractC08070aX) this.A05.get(size)).A0L.A00());
                }
                AnonymousClass0MI.A00();
                AnonymousClass0QD r3 = this.A0L;
                AnonymousClass0QR r02 = r3.A02;
                if (r02 == null) {
                    intValue = 100;
                } else {
                    intValue = ((Number) r02.A03()).intValue();
                }
                int i2 = (int) ((((((float) i) / 255.0f) * ((float) intValue)) / 100.0f) * 255.0f);
                if (this.A03 != null || ((r0 = this.A02) != null && !r0.A00.isEmpty())) {
                    RectF rectF = this.A0I;
                    int i3 = 0;
                    AAy(matrix2, rectF, false);
                    if (!(this.A03 == null || r7.A0F == AnonymousClass0J6.INVERT)) {
                        RectF rectF2 = this.A0H;
                        rectF2.set(0.0f, 0.0f, 0.0f, 0.0f);
                        this.A03.AAy(matrix, rectF2, true);
                        if (!rectF.intersect(rectF2)) {
                            rectF.set(0.0f, 0.0f, 0.0f, 0.0f);
                        }
                    }
                    matrix2.preConcat(r3.A00());
                    RectF rectF3 = this.A0G;
                    rectF3.set(0.0f, 0.0f, 0.0f, 0.0f);
                    AnonymousClass0NO r11 = this.A02;
                    if (r11 != null) {
                        List list = r11.A00;
                        if (!list.isEmpty()) {
                            List list2 = r11.A01;
                            int size2 = list2.size();
                            int i4 = 0;
                            while (true) {
                                if (i4 < size2) {
                                    C04860Ni r13 = (C04860Ni) list2.get(i4);
                                    Path path = this.A0F;
                                    path.set((Path) ((AnonymousClass0QR) list.get(i4)).A03());
                                    path.transform(matrix2);
                                    switch (r13.A02.ordinal()) {
                                        case 0:
                                        case 2:
                                            if (r13.A03) {
                                                break;
                                            }
                                            break;
                                    }
                                    RectF rectF4 = this.A0J;
                                    path.computeBounds(rectF4, false);
                                    if (i4 == 0) {
                                        rectF3.set(rectF4);
                                    } else {
                                        rectF3.set(Math.min(rectF3.left, rectF4.left), Math.min(rectF3.top, rectF4.top), Math.max(rectF3.right, rectF4.right), Math.max(rectF3.bottom, rectF4.bottom));
                                    }
                                    i4++;
                                } else if (!rectF.intersect(rectF3)) {
                                    rectF.set(0.0f, 0.0f, 0.0f, 0.0f);
                                }
                            }
                        }
                    }
                    if (!rectF.intersect(0.0f, 0.0f, (float) canvas.getWidth(), (float) canvas.getHeight())) {
                        rectF.set(0.0f, 0.0f, 0.0f, 0.0f);
                    }
                    AnonymousClass0MI.A00();
                    if (rectF.width() >= 1.0f && rectF.height() >= 1.0f) {
                        Paint paint3 = this.A0B;
                        paint3.setAlpha(255);
                        AnonymousClass0UV.A03(canvas, paint3, rectF, 31);
                        AnonymousClass0MI.A00();
                        A02(canvas);
                        A06(canvas, matrix2, i2);
                        AnonymousClass0MI.A00();
                        if (r11 != null) {
                            List list3 = r11.A00;
                            if (!list3.isEmpty()) {
                                Paint paint4 = this.A0C;
                                AnonymousClass0UV.A03(canvas, paint4, rectF, 19);
                                if (Build.VERSION.SDK_INT < 28) {
                                    A02(canvas);
                                }
                                AnonymousClass0MI.A00();
                                while (true) {
                                    List list4 = r11.A01;
                                    if (i3 < list4.size()) {
                                        C04860Ni r14 = (C04860Ni) list4.get(i3);
                                        AnonymousClass0QR r15 = (AnonymousClass0QR) list3.get(i3);
                                        AnonymousClass0QR r132 = (AnonymousClass0QR) r11.A02.get(i3);
                                        switch (r14.A02.ordinal()) {
                                            case 0:
                                                if (r14.A03) {
                                                    AnonymousClass0UV.A03(canvas, paint3, rectF, 31);
                                                    canvas.drawRect(rectF, paint3);
                                                    Path path2 = this.A0F;
                                                    path2.set((Path) r15.A03());
                                                    path2.transform(matrix2);
                                                    paint3.setAlpha((int) (((float) ((Number) r132.A03()).intValue()) * 2.55f));
                                                    canvas.drawPath(path2, this.A0D);
                                                    break;
                                                } else {
                                                    Path path3 = this.A0F;
                                                    path3.set((Path) r15.A03());
                                                    path3.transform(matrix2);
                                                    paint3.setAlpha((int) (((float) ((Number) r132.A03()).intValue()) * 2.55f));
                                                    canvas.drawPath(path3, paint3);
                                                    continue;
                                                    i3++;
                                                }
                                            case 1:
                                                if (i3 == 0) {
                                                    paint3.setColor(-16777216);
                                                    paint3.setAlpha(255);
                                                    canvas.drawRect(rectF, paint3);
                                                }
                                                if (r14.A03) {
                                                    paint2 = this.A0D;
                                                    AnonymousClass0UV.A03(canvas, paint2, rectF, 31);
                                                    canvas.drawRect(rectF, paint3);
                                                    paint2.setAlpha((int) (((float) ((Number) r132.A03()).intValue()) * 2.55f));
                                                    Path path4 = this.A0F;
                                                    path4.set((Path) r15.A03());
                                                    path4.transform(matrix2);
                                                    canvas.drawPath(path4, paint2);
                                                    break;
                                                } else {
                                                    Path path5 = this.A0F;
                                                    path5.set((Path) r15.A03());
                                                    path5.transform(matrix2);
                                                    canvas.drawPath(path5, this.A0D);
                                                    continue;
                                                    i3++;
                                                }
                                            case 2:
                                                if (!r14.A03) {
                                                    AnonymousClass0UV.A03(canvas, paint4, rectF, 31);
                                                    Path path6 = this.A0F;
                                                    path6.set((Path) r15.A03());
                                                    path6.transform(matrix2);
                                                    paint3.setAlpha((int) (((float) ((Number) r132.A03()).intValue()) * 2.55f));
                                                    canvas.drawPath(path6, paint3);
                                                    break;
                                                } else {
                                                    AnonymousClass0UV.A03(canvas, paint4, rectF, 31);
                                                    canvas.drawRect(rectF, paint3);
                                                    paint2 = this.A0D;
                                                    paint2.setAlpha((int) (((float) ((Number) r132.A03()).intValue()) * 2.55f));
                                                    Path path4 = this.A0F;
                                                    path4.set((Path) r15.A03());
                                                    path4.transform(matrix2);
                                                    canvas.drawPath(path4, paint2);
                                                    break;
                                                }
                                            case 3:
                                                if (!list3.isEmpty()) {
                                                    int i5 = 0;
                                                    while (true) {
                                                        if (i5 >= list4.size()) {
                                                            paint3.setAlpha(255);
                                                            canvas.drawRect(rectF, paint3);
                                                        } else if (((C04860Ni) list4.get(i5)).A02 == AnonymousClass0JS.MASK_MODE_NONE) {
                                                            i5++;
                                                        }
                                                    }
                                                } else {
                                                    continue;
                                                }
                                                i3++;
                                                break;
                                            default:
                                                i3++;
                                        }
                                        canvas.restore();
                                        i3++;
                                    } else {
                                        canvas.restore();
                                        AnonymousClass0MI.A00();
                                    }
                                }
                            }
                        }
                        if (this.A03 != null) {
                            AnonymousClass0UV.A03(canvas, this.A0E, rectF, 19);
                            AnonymousClass0MI.A00();
                            A02(canvas);
                            this.A03.A9C(canvas, matrix, i2);
                            canvas.restore();
                            AnonymousClass0MI.A00();
                            AnonymousClass0MI.A00();
                        }
                        canvas.restore();
                        AnonymousClass0MI.A00();
                    }
                    if (this.A06 && (paint = this.A00) != null) {
                        paint.setStyle(Paint.Style.STROKE);
                        this.A00.setColor(-251901);
                        this.A00.setStrokeWidth(4.0f);
                        canvas.drawRect(rectF, this.A00);
                        this.A00.setStyle(Paint.Style.FILL);
                        this.A00.setColor(1357638635);
                        canvas.drawRect(rectF, this.A00);
                    }
                } else {
                    matrix2.preConcat(r3.A00());
                    A06(canvas, matrix2, i2);
                    AnonymousClass0MI.A00();
                }
                AnonymousClass0MI.A00();
                C04840Ng r5 = this.A0K.A04.A0D;
                String str = r7.A0G;
                if (r5.A00) {
                    Map map = r5.A02;
                    AnonymousClass0NA r32 = (AnonymousClass0NA) map.get(str);
                    if (r32 == null) {
                        r32 = new AnonymousClass0NA();
                        map.put(str, r32);
                    }
                    float f = r32.A00 + 0.0f;
                    r32.A00 = f;
                    int i6 = r32.A01 + 1;
                    r32.A01 = i6;
                    if (i6 == Integer.MAX_VALUE) {
                        r32.A00 = f / 2.0f;
                        r32.A01 = 1073741823;
                    }
                    if (str.equals("__container")) {
                        Iterator it = r5.A03.iterator();
                        if (it.hasNext()) {
                            it.next();
                            throw new NullPointerException("onFrameRendered");
                        }
                        return;
                    }
                    return;
                }
                return;
            }
        }
        AnonymousClass0MI.A00();
    }

    @Override // X.AbstractC12860ig
    public void AAy(Matrix matrix, RectF rectF, boolean z) {
        this.A0I.set(0.0f, 0.0f, 0.0f, 0.0f);
        A00();
        Matrix matrix2 = this.A08;
        matrix2.set(matrix);
        if (z) {
            List list = this.A05;
            if (list != null) {
                int size = list.size();
                while (true) {
                    size--;
                    if (size < 0) {
                        break;
                    }
                    matrix2.preConcat(((AbstractC08070aX) this.A05.get(size)).A0L.A00());
                }
            } else {
                AbstractC08070aX r0 = this.A04;
                if (r0 != null) {
                    matrix2.preConcat(r0.A0L.A00());
                }
            }
        }
        matrix2.preConcat(this.A0L.A00());
    }

    @Override // X.AbstractC12030hG
    public void AYB() {
        this.A0K.invalidateSelf();
    }

    @Override // X.AbstractC12480hz
    public void Aan(C06430To r4, C06430To r5, List list, int i) {
        AbstractC08070aX r0 = this.A03;
        if (r0 != null) {
            String str = r0.A0M.A0G;
            C06430To r2 = new C06430To(r5);
            r2.A01.add(str);
            if (r4.A01(this.A03.A0M.A0G, i)) {
                AbstractC08070aX r1 = this.A03;
                C06430To r02 = new C06430To(r2);
                r02.A00 = r1;
                list.add(r02);
            }
            if (r4.A03(this.A0M.A0G, i)) {
                this.A03.A04(r4, r2, list, r4.A00(this.A03.A0M.A0G, i) + i);
            }
        }
        String str2 = this.A0M.A0G;
        if (r4.A02(str2, i)) {
            if (!"__container".equals(str2)) {
                C06430To r12 = new C06430To(r5);
                r12.A01.add(str2);
                r5 = r12;
                if (r4.A01(str2, i)) {
                    C06430To r03 = new C06430To(r12);
                    r03.A00 = this;
                    list.add(r03);
                }
            }
            if (r4.A03(str2, i)) {
                A04(r4, r5, list, i + r4.A00(str2, i));
            }
        }
    }

    @Override // X.AbstractC12470hy
    public String getName() {
        return this.A0M.A0G;
    }
}
