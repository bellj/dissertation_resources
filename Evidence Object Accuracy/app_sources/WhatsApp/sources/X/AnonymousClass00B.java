package X;

import android.os.Process;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.whatsapp.util.Log;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.FutureTask;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.00B  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass00B extends Thread {
    public static final Runnable A05 = new Runnable() { // from class: X.00D
        @Override // java.lang.Runnable
        public final void run() {
        }
    };
    public static final String A06;
    public static volatile boolean A07;
    public AnonymousClass01O A00;
    public final AnonymousClass00E A01 = new AnonymousClass00E(20, 20);
    public final BlockingQueue A02 = new ArrayBlockingQueue(EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH, true);
    public final AtomicInteger A03 = new AtomicInteger(0);
    public volatile boolean A04;

    static {
        StringBuilder sb = new StringBuilder("Logger (");
        sb.append(AnonymousClass00C.A00());
        sb.append(')');
        A06 = sb.toString();
    }

    public AnonymousClass00B() {
        super(A06);
    }

    public final void A00() {
        AnonymousClass01O r2;
        Object obj = null;
        do {
            try {
                obj = this.A02.take();
                continue;
            } catch (InterruptedException unused) {
            }
        } while (obj == null);
        if (obj instanceof String) {
            Log.doLogToFile((String) obj);
        } else if (obj instanceof FutureTask) {
            ((FutureTask) obj).run();
        } else {
            throw new IllegalStateException("Invalid log item type");
        }
        if (this.A04 && this.A02.isEmpty()) {
            Log.blockingLog(4, "log/emptyingqueue/end");
            StringBuilder sb = new StringBuilder("log/emptyingqueue/skipped ");
            AtomicInteger atomicInteger = this.A03;
            sb.append(atomicInteger);
            sb.append(" entries");
            Log.blockingLog(4, sb.toString());
            atomicInteger.set(0);
            A07 = true;
            this.A04 = false;
            if (this.A01.A00()) {
                synchronized (this) {
                    r2 = this.A00;
                    if (r2 == null) {
                        throw new NullPointerException();
                    }
                }
                r2.AaV("Log/doLogLoop", "Logging queue became full", true);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0061, code lost:
        if (r7 == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0063, code lost:
        r4.interrupt();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0066, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A01(java.lang.Object r12) {
        /*
            r11 = this;
            java.lang.Thread r4 = java.lang.Thread.currentThread()
            r3 = 1
            if (r4 == r11) goto L_0x0067
            boolean r0 = r11.A04
            if (r0 == 0) goto L_0x0011
            java.util.concurrent.atomic.AtomicInteger r0 = r11.A03
            r0.incrementAndGet()
        L_0x0010:
            return
        L_0x0011:
            long r9 = android.os.SystemClock.elapsedRealtime()
            r8 = 0
            r7 = 0
        L_0x0017:
            java.util.concurrent.BlockingQueue r5 = r11.A02     // Catch: InterruptedException -> 0x005f
            r0 = 1000(0x3e8, double:4.94E-321)
            java.util.concurrent.TimeUnit r2 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch: InterruptedException -> 0x005f
            boolean r0 = r5.offer(r12, r0, r2)     // Catch: InterruptedException -> 0x005f
            if (r0 != 0) goto L_0x0061
            long r5 = android.os.SystemClock.elapsedRealtime()     // Catch: InterruptedException -> 0x005f
            long r5 = r5 - r9
            r1 = 2000(0x7d0, double:9.88E-321)
            int r0 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x0017
            if (r8 != 0) goto L_0x0017
            r1 = r11
            monitor-enter(r1)     // Catch: InterruptedException -> 0x005e
            X.01O r2 = r11.A00     // Catch: all -> 0x005b
            if (r2 == 0) goto L_0x0055
            monitor-exit(r1)     // Catch: InterruptedException -> 0x005e
            java.lang.String r1 = "addLogItem waited for 2 seconds"
            r0 = 0
            r2.AaV(r1, r0, r3)     // Catch: InterruptedException -> 0x005e
            boolean r0 = r11.A04     // Catch: InterruptedException -> 0x005e
            if (r0 != 0) goto L_0x0053
            boolean r0 = X.AnonymousClass00B.A07     // Catch: InterruptedException -> 0x005e
            if (r0 != 0) goto L_0x0053
            r11.A04 = r3     // Catch: InterruptedException -> 0x005e
            java.util.concurrent.atomic.AtomicInteger r0 = r11.A03     // Catch: InterruptedException -> 0x005e
            r0.incrementAndGet()     // Catch: InterruptedException -> 0x005e
            r1 = 4
            java.lang.String r0 = "log/emptyingqueue/start"
            com.whatsapp.util.Log.blockingLog(r1, r0)     // Catch: InterruptedException -> 0x005e
            goto L_0x0061
        L_0x0053:
            r8 = 1
            goto L_0x0017
        L_0x0055:
            java.lang.NullPointerException r0 = new java.lang.NullPointerException     // Catch: all -> 0x005b
            r0.<init>()     // Catch: all -> 0x005b
            throw r0     // Catch: all -> 0x005b
        L_0x005b:
            r0 = move-exception
            monitor-exit(r1)     // Catch: InterruptedException -> 0x005e
            throw r0     // Catch: InterruptedException -> 0x005e
        L_0x005e:
            r8 = 1
        L_0x005f:
            r7 = 1
            goto L_0x0017
        L_0x0061:
            if (r7 == 0) goto L_0x0010
            r4.interrupt()
            return
        L_0x0067:
            java.lang.String r0 = "Cannot add a log item from the logging thread. Attempting to crash."
            com.whatsapp.util.Log.blockingLog(r3, r0)
            java.lang.String r1 = "Cannot add a log item from the logging thread."
            java.lang.AssertionError r0 = new java.lang.AssertionError
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass00B.A01(java.lang.Object):void");
    }

    @Override // java.lang.Thread, java.lang.Runnable
    public void run() {
        Process.setThreadPriority(10);
        while (true) {
            A00();
        }
    }
}
