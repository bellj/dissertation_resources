package X;

/* renamed from: X.0Ff  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC02880Ff extends AbstractC05330Pd {
    public abstract void A03(AbstractC12830ic v, Object obj);

    public AbstractC02880Ff(AnonymousClass0QN r1) {
        super(r1);
    }

    public final void A04(Object obj) {
        AbstractC12830ic A00 = A00();
        try {
            A03(A00, obj);
            ((C02980Fp) A00).A00.executeInsert();
        } finally {
            A02(A00);
        }
    }
}
