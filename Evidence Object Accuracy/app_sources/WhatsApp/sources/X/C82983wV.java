package X;

import java.time.OffsetDateTime;

/* renamed from: X.3wV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C82983wV extends AbstractC94534c0 {
    public final OffsetDateTime A00;

    public C82983wV(CharSequence charSequence) {
        this.A00 = OffsetDateTime.parse(charSequence);
    }

    @Override // X.AbstractC94534c0
    public C82973wU A06() {
        return new C82973wU(this.A00.toString(), false);
    }

    public OffsetDateTime A08() {
        return this.A00;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C82983wV) && !(obj instanceof C82973wU)) {
            return false;
        }
        if (this.A00.compareTo(((AbstractC94534c0) obj).A05().A00) != 0) {
            return false;
        }
        return true;
    }

    public String toString() {
        return this.A00.toString();
    }
}
