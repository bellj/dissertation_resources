package X;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import com.whatsapp.payments.ui.widget.PeerPaymentTransactionRow;

/* renamed from: X.5aU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC117715aU extends LinearLayout implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;

    public AbstractC117715aU(Context context) {
        super(context);
        A01();
    }

    public AbstractC117715aU(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A01();
    }

    public AbstractC117715aU(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A01();
    }

    public static void A00(AnonymousClass01J r1, PeerPaymentTransactionRow peerPaymentTransactionRow) {
        peerPaymentTransactionRow.A0N = (C21270x9) r1.A4A.get();
        peerPaymentTransactionRow.A0Y = (AnonymousClass14X) r1.AFM.get();
        peerPaymentTransactionRow.A0J = (AnonymousClass130) r1.A41.get();
        peerPaymentTransactionRow.A0K = (C15550nR) r1.A45.get();
        peerPaymentTransactionRow.A0L = (C15610nY) r1.AMe.get();
        peerPaymentTransactionRow.A0W = (C17070qD) r1.AFC.get();
        peerPaymentTransactionRow.A0P = (C15650ng) r1.A4m.get();
        peerPaymentTransactionRow.A0O = (AnonymousClass018) r1.ANb.get();
        peerPaymentTransactionRow.A0V = (C22710zW) r1.AF7.get();
        peerPaymentTransactionRow.A0U = (AnonymousClass18P) r1.AEn.get();
        peerPaymentTransactionRow.A0X = (AnonymousClass1A8) r1.AER.get();
    }

    public void A01() {
        if (this instanceof C123685nf) {
            C123685nf r2 = (C123685nf) this;
            if (!r2.A03) {
                r2.A03 = true;
                AnonymousClass01J A00 = AnonymousClass2P6.A00(r2.generatedComponent());
                r2.A0R = C12960it.A0S(A00);
                r2.A0S = (AnonymousClass13H) A00.ABY.get();
                A00(A00, r2);
                r2.A01 = C12980iv.A0b(A00);
            }
        } else if (!this.A01) {
            this.A01 = true;
            PeerPaymentTransactionRow peerPaymentTransactionRow = (PeerPaymentTransactionRow) this;
            AnonymousClass01J r1 = ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06;
            peerPaymentTransactionRow.A0R = C12960it.A0S(r1);
            peerPaymentTransactionRow.A0S = (AnonymousClass13H) r1.ABY.get();
            A00(r1, peerPaymentTransactionRow);
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }
}
