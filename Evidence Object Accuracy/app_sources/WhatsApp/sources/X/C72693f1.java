package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import com.whatsapp.calling.CallPictureGrid;

/* renamed from: X.3f1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C72693f1 extends AnimatorListenerAdapter {
    public final /* synthetic */ CallPictureGrid A00;

    public C72693f1(CallPictureGrid callPictureGrid) {
        this.A00 = callPictureGrid;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
        CallPictureGrid callPictureGrid = this.A00;
        callPictureGrid.clearAnimation();
        callPictureGrid.setAlpha(1.0f);
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        CallPictureGrid callPictureGrid = this.A00;
        callPictureGrid.clearAnimation();
        callPictureGrid.setAlpha(1.0f);
    }
}
