package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.View;
import com.whatsapp.Conversation;
import com.whatsapp.jid.UserJid;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;

/* renamed from: X.3bE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70403bE implements AbstractC41521tf {
    public boolean A00 = false;
    public final /* synthetic */ int A01;
    public final /* synthetic */ Context A02;
    public final /* synthetic */ View A03;
    public final /* synthetic */ C19850um A04;
    public final /* synthetic */ AnonymousClass1XV A05;
    public final /* synthetic */ AnonymousClass19O A06;
    public final /* synthetic */ boolean A07;

    @Override // X.AbstractC41521tf
    public /* synthetic */ void AQV() {
    }

    @Override // X.AbstractC41521tf
    public /* synthetic */ void Adu(View view) {
    }

    public C70403bE(Context context, View view, C19850um r4, AnonymousClass1XV r5, AnonymousClass19O r6, int i, boolean z) {
        this.A05 = r5;
        this.A02 = context;
        this.A04 = r4;
        this.A07 = z;
        this.A01 = i;
        this.A06 = r6;
        this.A03 = view;
    }

    @Override // X.AbstractC41521tf
    public int AGm() {
        return this.A06.A03(this.A03.getContext());
    }

    @Override // X.AbstractC41521tf
    public void Adg(Bitmap bitmap, View view, AbstractC15340mz r30) {
        C37071lG r7;
        C30711Yn r13;
        Bitmap bitmap2 = bitmap;
        if (!this.A00) {
            this.A00 = true;
            if (bitmap == null || bitmap2.getWidth() <= 0 || bitmap2.getHeight() <= 0) {
                bitmap2 = null;
            }
            AnonymousClass1XV r0 = this.A05;
            Context context = this.A02;
            String str = r0.A06;
            Conversation conversation = (Conversation) AbstractC35731ia.A01(context, Conversation.class);
            if (conversation != null) {
                r7 = conversation.A1L;
                if (r7 == null) {
                    r7 = new C37071lG(conversation.A1K);
                    conversation.A1L = r7;
                }
                if (bitmap2 != null) {
                    StringBuilder A0j = C12960it.A0j(str);
                    A0j.append('_');
                    String A0f = C12960it.A0f(A0j, 3);
                    C252918v r6 = r7.A01;
                    if (r6.A02 != null) {
                        try {
                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            bitmap2.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
                            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
                            C58962tj r2 = r6.A02;
                            String A01 = C003501n.A01(A0f);
                            AnonymousClass009.A05(A01);
                            ((AbstractC38761of) r2).A03.A02(byteArrayInputStream, A01);
                            byteArrayInputStream.close();
                        } catch (IOException unused) {
                        }
                    }
                }
            } else {
                r7 = null;
            }
            ArrayList A0l = C12960it.A0l();
            for (int i = 0; i < r0.A00; i++) {
                if (i != 0 || r7 == null || bitmap2 == null) {
                    A0l.add(null);
                } else {
                    A0l.add(new C44741zT(str, "", "", bitmap2.getWidth(), bitmap2.getHeight()));
                }
            }
            String str2 = r0.A09;
            if (str2 == null) {
                str2 = "";
            }
            String str3 = r0.A04;
            if (str3 == null) {
                str3 = "";
            }
            BigDecimal bigDecimal = r0.A0A;
            if (TextUtils.isEmpty(r0.A03)) {
                r13 = null;
            } else {
                r13 = new C30711Yn(r0.A03);
            }
            C44691zO r9 = new C44691zO(null, new C44731zS(null, null, null, 0, false), null, r13, str, str2, str3, r0.A07, r0.A08, null, bigDecimal, A0l, 0, 99, false, false);
            this.A04.A0C(r9, null);
            UserJid userJid = r0.A01;
            boolean z = this.A07;
            AnonymousClass283.A02(context, C14960mK.A0c(context, false), userJid, null, null, r9.A0D, this.A01, z);
        }
    }
}
