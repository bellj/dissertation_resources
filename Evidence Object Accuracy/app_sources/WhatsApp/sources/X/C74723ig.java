package X;

import android.content.Context;
import android.util.DisplayMetrics;
import com.whatsapp.filter.SmoothScrollLinearLayoutManager;

/* renamed from: X.3ig  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74723ig extends AnonymousClass0FE {
    public final /* synthetic */ SmoothScrollLinearLayoutManager A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C74723ig(Context context, SmoothScrollLinearLayoutManager smoothScrollLinearLayoutManager) {
        super(context);
        this.A00 = smoothScrollLinearLayoutManager;
    }

    @Override // X.AnonymousClass0FE
    public float A04(DisplayMetrics displayMetrics) {
        return 150.0f / ((float) displayMetrics.densityDpi);
    }
}
