package X;

import java.io.IOException;

/* renamed from: X.39f  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C629339f extends IOException {
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C629339f(java.lang.Throwable r3) {
        /*
            r2 = this;
            java.lang.String r0 = "Unexpected "
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            java.lang.String r0 = X.C12980iv.A0r(r3)
            X.C12990iw.A1T(r1, r0)
            java.lang.String r0 = r3.getMessage()
            java.lang.String r0 = X.C12960it.A0d(r0, r1)
            r2.<init>(r0, r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C629339f.<init>(java.lang.Throwable):void");
    }
}
