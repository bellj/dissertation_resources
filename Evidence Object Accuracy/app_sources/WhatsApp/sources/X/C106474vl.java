package X;

import android.content.Context;
import android.os.Handler;
import java.util.ArrayList;

/* renamed from: X.4vl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106474vl implements AnonymousClass5SH {
    public boolean A00;
    public final Context A01;

    public C106474vl(Context context, boolean z) {
        this.A01 = context;
        this.A00 = z;
    }

    @Override // X.AnonymousClass5SH
    public AbstractC117055Yb[] A8T(Handler handler, AbstractC72373eU r10, AnonymousClass5SO r11, AnonymousClass5SS r12, AnonymousClass5XS r13) {
        ArrayList A0l = C12960it.A0l();
        A0l.add(new C76933mT(this.A01, handler, AbstractC116825Xa.A00, new C107334xA(this), r13));
        return (AbstractC117055Yb[]) A0l.toArray(new AbstractC117055Yb[0]);
    }
}
