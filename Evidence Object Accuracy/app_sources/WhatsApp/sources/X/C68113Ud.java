package X;

import android.net.Uri;
import com.whatsapp.backup.google.viewmodel.RestoreFromBackupViewModel;
import com.whatsapp.util.Log;

/* renamed from: X.3Ud  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68113Ud implements AnonymousClass29Z {
    public final /* synthetic */ Uri A00;
    public final /* synthetic */ RestoreFromBackupViewModel A01;

    @Override // X.AnonymousClass29Z
    public void ANE() {
    }

    public C68113Ud(Uri uri, RestoreFromBackupViewModel restoreFromBackupViewModel) {
        this.A01 = restoreFromBackupViewModel;
        this.A00 = uri;
    }

    @Override // X.AnonymousClass29Z
    public void AM6(int i) {
        C84263yg r1;
        RestoreFromBackupViewModel restoreFromBackupViewModel = this.A01;
        restoreFromBackupViewModel.A04.A03.A04(this);
        int i2 = 0;
        if (i != 0) {
            i2 = 1;
            if (i != 1) {
                i2 = 2;
                if (i != 2) {
                    Log.e(C12960it.A0W(i, "restore-from-backup-view-model/incorrect-space-check-result/"));
                    r1 = null;
                    restoreFromBackupViewModel.A07.A0A(r1);
                }
            }
        }
        r1 = new C84263yg(this.A00, i2);
        restoreFromBackupViewModel.A07.A0A(r1);
    }
}
