package X;

import android.animation.AnimatorListenerAdapter;

/* renamed from: X.093  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass093 extends AnimatorListenerAdapter {
    public final /* synthetic */ AnonymousClass0O3 A00;
    public final /* synthetic */ AnonymousClass071 A01;
    public AnonymousClass0O3 mViewBounds;

    public AnonymousClass093(AnonymousClass0O3 r1, AnonymousClass071 r2) {
        this.A01 = r2;
        this.A00 = r1;
        this.mViewBounds = r1;
    }
}
