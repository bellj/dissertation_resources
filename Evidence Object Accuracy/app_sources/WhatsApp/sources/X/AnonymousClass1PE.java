package X;

import android.content.ContentValues;

/* renamed from: X.1PE  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1PE {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public int A09;
    public int A0A;
    public long A0B = 1;
    public long A0C;
    public long A0D;
    public long A0E;
    public long A0F;
    public long A0G = Long.MIN_VALUE;
    public long A0H;
    public long A0I;
    public long A0J;
    public long A0K = 1;
    public long A0L = 0;
    public long A0M = 1;
    public long A0N = Long.MIN_VALUE;
    public long A0O = 1;
    public long A0P = Long.MIN_VALUE;
    public long A0Q;
    public long A0R;
    public long A0S = 0;
    public long A0T = 1;
    public long A0U = Long.MIN_VALUE;
    public long A0V = -1;
    public long A0W;
    public long A0X;
    public AnonymousClass1PG A0Y;
    public AbstractC15340mz A0Z;
    public AbstractC15340mz A0a;
    public AnonymousClass1V0 A0b;
    public AnonymousClass1YY A0c;
    public String A0d;
    public String A0e;
    public boolean A0f;
    public boolean A0g;
    public boolean A0h;
    public final AbstractC14640lm A0i;

    public AnonymousClass1PE(AbstractC14640lm r7) {
        this.A0i = r7;
        this.A0Y = new AnonymousClass1PG(0, 0, 0);
        this.A0E = Long.MIN_VALUE;
        this.A0F = Long.MIN_VALUE;
        this.A0C = Long.MIN_VALUE;
        this.A0D = Long.MIN_VALUE;
    }

    public synchronized long A00() {
        return this.A0L;
    }

    public synchronized long A01() {
        return this.A0S;
    }

    public synchronized long A02() {
        return this.A0W;
    }

    public synchronized ContentValues A03() {
        ContentValues contentValues;
        contentValues = new ContentValues(4);
        contentValues.put("unseen_message_count", Integer.valueOf(this.A06));
        contentValues.put("unseen_missed_calls_count", Integer.valueOf(this.A07));
        contentValues.put("unseen_row_count", Integer.valueOf(this.A08));
        contentValues.put("unseen_earliest_message_received_time", Long.valueOf(this.A0X));
        return contentValues;
    }

    public synchronized ContentValues A04(Long l) {
        ContentValues contentValues;
        contentValues = new ContentValues();
        contentValues.put("mod_tag", Integer.valueOf(this.A0A));
        contentValues.put("display_message_row_id", Long.valueOf(this.A0T));
        contentValues.put("display_message_sort_id", Long.valueOf(this.A0U));
        contentValues.put("last_message_row_id", Long.valueOf(this.A0M));
        contentValues.put("last_message_sort_id", Long.valueOf(this.A0N));
        contentValues.put("last_read_message_row_id", Long.valueOf(this.A0O));
        contentValues.put("last_read_message_sort_id", Long.valueOf(this.A0P));
        contentValues.put("last_read_receipt_sent_message_row_id", Long.valueOf(this.A0Q));
        contentValues.put("last_read_receipt_sent_message_sort_id", Long.valueOf(this.A0R));
        contentValues.put("unseen_earliest_message_received_time", Long.valueOf(this.A0X));
        contentValues.put("unseen_message_count", Integer.valueOf(this.A06));
        contentValues.put("unseen_missed_calls_count", Integer.valueOf(this.A07));
        contentValues.put("unseen_row_count", Integer.valueOf(this.A08));
        contentValues.put("last_important_message_row_id", Long.valueOf(this.A0K));
        int i = 1;
        int i2 = 0;
        if (this.A0h) {
            i2 = 1;
        }
        contentValues.put("show_group_description", Integer.valueOf(i2));
        contentValues.put("ephemeral_expiration", Integer.valueOf(this.A0Y.expiration));
        contentValues.put("ephemeral_setting_timestamp", Long.valueOf(this.A0Y.ephemeralSettingTimestamp));
        contentValues.put("ephemeral_disappearing_messages_initiator", Integer.valueOf(this.A0Y.disappearingMessagesInitiator));
        contentValues.put("subject", this.A0e);
        if (!this.A0f) {
            i = 0;
        }
        contentValues.put("archived", Integer.valueOf(i));
        contentValues.put("sort_timestamp", Long.valueOf(this.A0W));
        contentValues.put("change_number_notified_message_row_id", Long.valueOf(this.A0B));
        contentValues.put("spam_detection", Integer.valueOf(this.A03));
        contentValues.put("plaintext_disabled", Integer.valueOf(this.A00));
        contentValues.put("vcard_ui_dismissed", Integer.valueOf(this.A09));
        if (l != null) {
            contentValues.put("created_timestamp", l);
        }
        contentValues.put("unseen_important_message_count", Integer.valueOf(this.A04));
        contentValues.put("group_type", Integer.valueOf(this.A01));
        contentValues.put("unseen_message_reaction_count", Integer.valueOf(this.A05));
        contentValues.put("last_message_reaction_row_id", Long.valueOf(this.A0L));
        contentValues.put("last_seen_message_reaction_row_id", Long.valueOf(this.A0S));
        contentValues.put("has_new_community_admin_dialog_been_acknowledged", Boolean.valueOf(this.A0g));
        contentValues.put("history_sync_progress", Integer.valueOf(this.A02));
        return contentValues;
    }

    public synchronized AbstractC14640lm A05() {
        return this.A0i;
    }

    public synchronized String A06() {
        return this.A0e;
    }

    public synchronized String A07() {
        StringBuilder sb;
        sb = new StringBuilder();
        sb.append(this.A08);
        sb.append("/");
        sb.append(this.A06);
        sb.append("/");
        sb.append(this.A07);
        sb.append("/");
        sb.append(this.A0X);
        sb.append("/");
        sb.append(this.A04);
        sb.append("/");
        sb.append(this.A0L - this.A0S);
        return sb.toString();
    }

    public synchronized void A08() {
        this.A0a = null;
        this.A0Z = null;
        this.A0c = null;
        this.A0T = 1;
        this.A0U = Long.MIN_VALUE;
        this.A0M = 1;
        this.A0N = Long.MIN_VALUE;
        this.A0K = 1;
        this.A0O = 1;
        this.A0P = Long.MIN_VALUE;
        this.A0Q = 1;
        this.A0R = Long.MIN_VALUE;
        this.A0G = Long.MIN_VALUE;
        A0C(0, 0, 0, 0);
    }

    public synchronized void A09(int i) {
        this.A03 = i;
    }

    public void A0A(int i, long j, int i2) {
        this.A0Y = new AnonymousClass1PG(i, j, i2);
    }

    public synchronized void A0B(long j) {
        this.A0W = j;
    }

    public synchronized boolean A0C(int i, int i2, int i3, int i4) {
        boolean z;
        if (this.A06 == i && this.A07 == i2 && this.A08 == i3 && this.A04 == i4) {
            z = false;
        } else {
            if (i <= 0) {
                this.A0X = 0;
            }
            this.A06 = i;
            this.A04 = i4;
            this.A07 = i2;
            this.A08 = i3;
            z = true;
        }
        return z;
    }
}
