package X;

/* renamed from: X.0GH  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0GH extends AnonymousClass0SP {
    public final AbstractC11930h6 A00;
    public final AnonymousClass0JH A01;
    public final C03950Ju A02;
    public final Object A03;
    public final String A04;
    public final String A05;

    @Override // X.AnonymousClass0SP
    public AnonymousClass0SP A01(String str, AnonymousClass1J7 r2) {
        return this;
    }

    public AnonymousClass0GH(AbstractC11930h6 r4, AnonymousClass0JH r5, Object obj, String str, String str2) {
        C16700pc.A0E(obj, 1);
        C16700pc.A0E(r4, 4);
        C16700pc.A0E(r5, 5);
        this.A03 = obj;
        this.A05 = str;
        this.A04 = str2;
        this.A00 = r4;
        this.A01 = r5;
        C03950Ju r2 = new C03950Ju(AnonymousClass0SP.A00(str2, obj));
        StackTraceElement[] stackTrace = r2.getStackTrace();
        C16700pc.A0B(stackTrace);
        Object[] array = C10970fV.A00(stackTrace).toArray(new StackTraceElement[0]);
        if (array != null) {
            r2.setStackTrace((StackTraceElement[]) array);
            this.A02 = r2;
            return;
        }
        throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
    }

    @Override // X.AnonymousClass0SP
    public Object A02() {
        switch (this.A01.ordinal()) {
            case 0:
                throw this.A02;
            case 1:
                this.A00.A8f(this.A05, AnonymousClass0SP.A00(this.A04, this.A03));
                return null;
            case 2:
                return null;
            default:
                throw new C113285Gx();
        }
    }
}
