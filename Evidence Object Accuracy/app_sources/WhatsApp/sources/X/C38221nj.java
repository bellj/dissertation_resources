package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.whatsapp.util.Log;

/* renamed from: X.1nj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38221nj extends BroadcastReceiver {
    public C20380vf A00;
    public final Object A01 = new Object();
    public volatile boolean A02 = false;

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (!this.A02) {
            synchronized (this.A01) {
                if (!this.A02) {
                    this.A00 = (C20380vf) ((AnonymousClass01J) AnonymousClass22D.A00(context)).ACR.get();
                    this.A02 = true;
                }
            }
        }
        Log.i("MessagelessPaymentNotification/dismiss");
        this.A00.A00();
    }
}
