package X;

import android.content.Context;
import com.whatsapp.payments.ui.IndiaUpiSendPaymentActivity;

/* renamed from: X.5ga  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120685ga extends C120895gv {
    public final /* synthetic */ C120555gN A00;
    public final /* synthetic */ C128595wM A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120685ga(Context context, C14900mE r8, C18650sn r9, C64513Fv r10, C120555gN r11, C128595wM r12) {
        super(context, r8, r9, r10, "upi-collect-from-vpa");
        this.A00 = r11;
        this.A01 = r12;
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A02(C452120p r5) {
        super.A02(r5);
        IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A01.A00;
        if (r5 == null) {
            ((AbstractActivityC121545iU) indiaUpiSendPaymentActivity).A0B.A04(123, 2);
        }
        indiaUpiSendPaymentActivity.A3U(r5, true);
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A03(C452120p r5) {
        super.A03(r5);
        IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A01.A00;
        if (r5 == null) {
            ((AbstractActivityC121545iU) indiaUpiSendPaymentActivity).A0B.A04(123, 2);
        }
        indiaUpiSendPaymentActivity.A3U(r5, true);
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A04(AnonymousClass1V8 r6) {
        super.A04(r6);
        IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A01.A00;
        ((AbstractActivityC121545iU) indiaUpiSendPaymentActivity).A0B.A04(123, 2);
        indiaUpiSendPaymentActivity.A3U(null, true);
    }
}
