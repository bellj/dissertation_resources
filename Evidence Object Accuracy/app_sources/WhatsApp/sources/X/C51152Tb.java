package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2Tb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C51152Tb extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C51152Tb A05;
    public static volatile AnonymousClass255 A06;
    public int A00;
    public int A01;
    public C51182Te A02;
    public String A03 = "";
    public boolean A04;

    static {
        C51152Tb r0 = new C51152Tb();
        A05 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r8, Object obj, Object obj2) {
        C51192Tf r1;
        switch (r8.ordinal()) {
            case 0:
                return A05;
            case 1:
                AbstractC462925h r9 = (AbstractC462925h) obj;
                C51152Tb r10 = (C51152Tb) obj2;
                boolean z = true;
                if ((this.A00 & 1) != 1) {
                    z = false;
                }
                String str = this.A03;
                boolean z2 = true;
                if ((r10.A00 & 1) != 1) {
                    z2 = false;
                }
                this.A03 = r9.Afy(str, r10.A03, z, z2);
                this.A02 = (C51182Te) r9.Aft(this.A02, r10.A02);
                int i = this.A00;
                boolean z3 = false;
                if ((i & 4) == 4) {
                    z3 = true;
                }
                int i2 = this.A01;
                int i3 = r10.A00;
                boolean z4 = false;
                if ((i3 & 4) == 4) {
                    z4 = true;
                }
                this.A01 = r9.Afp(i2, r10.A01, z3, z4);
                boolean z5 = false;
                if ((i & 8) == 8) {
                    z5 = true;
                }
                boolean z6 = this.A04;
                boolean z7 = false;
                if ((i3 & 8) == 8) {
                    z7 = true;
                }
                this.A04 = r9.Afl(z5, z6, z7, r10.A04);
                if (r9 == C463025i.A00) {
                    this.A00 = i | i3;
                }
                return this;
            case 2:
                AnonymousClass253 r92 = (AnonymousClass253) obj;
                AnonymousClass254 r102 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        try {
                            int A03 = r92.A03();
                            if (A03 == 0) {
                                break;
                            } else if (A03 == 10) {
                                String A0A = r92.A0A();
                                this.A00 = 1 | this.A00;
                                this.A03 = A0A;
                            } else if (A03 == 18) {
                                if ((this.A00 & 2) == 2) {
                                    r1 = (C51192Tf) this.A02.A0T();
                                } else {
                                    r1 = null;
                                }
                                C51182Te r0 = (C51182Te) r92.A09(r102, C51182Te.A06.A0U());
                                this.A02 = r0;
                                if (r1 != null) {
                                    r1.A04(r0);
                                    this.A02 = (C51182Te) r1.A01();
                                }
                                this.A00 |= 2;
                            } else if (A03 == 24) {
                                int A02 = r92.A02();
                                if (AnonymousClass1YI.A00(A02) == null) {
                                    super.A0X(3, A02);
                                } else {
                                    this.A00 |= 4;
                                    this.A01 = A02;
                                }
                            } else if (A03 == 32) {
                                this.A00 |= 8;
                                this.A04 = r92.A0F();
                            } else if (!A0a(r92, A03)) {
                                break;
                            }
                        } catch (C28971Pt e) {
                            e.unfinishedMessage = this;
                            throw new RuntimeException(e);
                        }
                    } catch (IOException e2) {
                        C28971Pt r12 = new C28971Pt(e2.getMessage());
                        r12.unfinishedMessage = this;
                        throw new RuntimeException(r12);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C51152Tb();
            case 5:
                return new C51172Td();
            case 6:
                break;
            case 7:
                if (A06 == null) {
                    synchronized (C51152Tb.class) {
                        if (A06 == null) {
                            A06 = new AnonymousClass255(A05);
                        }
                    }
                }
                return A06;
            default:
                throw new UnsupportedOperationException();
        }
        return A05;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A07(1, this.A03);
        }
        if ((this.A00 & 2) == 2) {
            C51182Te r0 = this.A02;
            if (r0 == null) {
                r0 = C51182Te.A06;
            }
            i2 += CodedOutputStream.A0A(r0, 2);
        }
        int i3 = this.A00;
        if ((i3 & 4) == 4) {
            i2 += CodedOutputStream.A02(3, this.A01);
        }
        if ((i3 & 8) == 8) {
            i2 += CodedOutputStream.A00(4);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A03);
        }
        if ((this.A00 & 2) == 2) {
            C51182Te r0 = this.A02;
            if (r0 == null) {
                r0 = C51182Te.A06;
            }
            codedOutputStream.A0L(r0, 2);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0E(3, this.A01);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0J(4, this.A04);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
