package X;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import com.whatsapp.CircularProgressBar;
import com.whatsapp.R;

/* renamed from: X.35s  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C622335s extends AbstractC69213Yj {
    public int A00;
    public View A01;
    public TextView A02;
    public CircularProgressBar A03;
    public AnonymousClass1KZ A04;
    public final AnonymousClass1AB A05;
    public final C235512c A06;
    public final AbstractC116245Ur A07;

    public C622335s(Context context, LayoutInflater layoutInflater, C14850m9 r3, AnonymousClass1AB r4, AnonymousClass1KZ r5, C235512c r6, AbstractC116245Ur r7, int i) {
        super(context, layoutInflater, r3, i);
        this.A06 = r6;
        this.A05 = r4;
        this.A04 = r5;
        this.A07 = r7;
    }

    @Override // X.AbstractC69213Yj
    public void A03(View view) {
        this.A03 = (CircularProgressBar) AnonymousClass028.A0D(view, R.id.pack_loading);
        this.A02 = C12960it.A0I(view, R.id.pack_loading_text);
        View A0D = AnonymousClass028.A0D(view, R.id.cancel_button);
        this.A01 = A0D;
        AbstractView$OnClickListenerC34281fs.A01(A0D, this, 44);
        A04();
    }

    public void A04() {
        View view;
        if (!(this instanceof AnonymousClass35n)) {
            CircularProgressBar circularProgressBar = this.A03;
            if (circularProgressBar != null && this.A02 != null && (view = this.A01) != null) {
                if (this.A04.A05) {
                    circularProgressBar.setVisibility(0);
                    this.A02.setVisibility(0);
                    this.A01.setVisibility(0);
                    boolean isEmpty = TextUtils.isEmpty(this.A04.A0F);
                    TextView textView = this.A02;
                    if (isEmpty) {
                        textView.setText(R.string.sticker_pack_downloading);
                    } else {
                        textView.setText(C12960it.A0X(this.A08, this.A04.A0F, new Object[1], 0, R.string.sticker_pack_downloading_with_name));
                    }
                    int i = this.A00;
                    CircularProgressBar circularProgressBar2 = this.A03;
                    if (i >= 0) {
                        circularProgressBar2.setIndeterminate(false);
                        this.A03.setProgress(this.A00);
                        return;
                    }
                    circularProgressBar2.setIndeterminate(true);
                    return;
                }
                view.setVisibility(8);
                this.A03.setVisibility(8);
                this.A02.setVisibility(8);
                return;
            }
            return;
        }
        AnonymousClass35n r4 = (AnonymousClass35n) this;
        CircularProgressBar circularProgressBar3 = ((C622335s) r4).A03;
        if (!(circularProgressBar3 == null || ((C622335s) r4).A02 == null)) {
            if (r4.A02) {
                circularProgressBar3.setVisibility(0);
                ((C622335s) r4).A02.setVisibility(0);
                boolean isEmpty2 = TextUtils.isEmpty(((C622335s) r4).A04.A0F);
                TextView textView2 = ((C622335s) r4).A02;
                if (isEmpty2) {
                    textView2.setText(R.string.sticker_pack_loading);
                } else {
                    textView2.setText(C12960it.A0X(r4.A08, ((C622335s) r4).A04.A0F, C12970iu.A1b(), 0, R.string.sticker_pack_loading_with_name));
                }
            } else {
                circularProgressBar3.setVisibility(8);
                ((C622335s) r4).A02.setVisibility(8);
            }
        }
        if (r4.A00 == null) {
            return;
        }
        if (((C622335s) r4).A04.A04.size() != 0 || r4.A02) {
            r4.A00.setVisibility(8);
            return;
        }
        r4.A00.setVisibility(0);
        C12960it.A11(r4.A01, r4, 29);
    }
}
