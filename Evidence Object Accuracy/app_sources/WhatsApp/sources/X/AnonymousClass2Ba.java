package X;

import com.whatsapp.util.Log;
import java.io.EOFException;
import java.io.File;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/* renamed from: X.2Ba  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Ba {
    public int A00;
    public int A01;
    public int A02;
    public long A03;
    public long A04;
    public long A05;
    public long A06;
    public AnonymousClass3G0 A07;
    public File A08;
    public Long A09;
    public boolean A0A;
    public boolean A0B = false;
    public boolean A0C;
    public boolean A0D;
    public final C91024Qd A0E = new C91024Qd();
    public final List A0F = new CopyOnWriteArrayList();
    public volatile File A0G;

    public synchronized int A00() {
        return this.A01;
    }

    public synchronized long A01() {
        long j;
        j = this.A04;
        if (j == 0) {
            j = this.A03;
        }
        return j;
    }

    public synchronized File A02() {
        return this.A0G;
    }

    public synchronized void A03() {
        File file = this.A08;
        if (file != null) {
            if (!file.delete()) {
                Log.w("DownloadContext/unable to delete chunkstore file");
            }
            this.A08 = null;
        }
    }

    public synchronized void A04(int i) {
        if (this.A00 != i) {
            this.A00 = i;
            for (AnonymousClass2BZ r0 : this.A0F) {
                r0.APR(i);
            }
        }
    }

    public synchronized void A05(int i) {
        if (this.A01 != i) {
            this.A01 = i;
            for (AnonymousClass2BZ r0 : this.A0F) {
                r0.APS(this);
            }
        }
    }

    public synchronized void A06(long j, long j2) {
        this.A06 = j;
        for (AnonymousClass2BZ r0 : this.A0F) {
            r0.ANU(this, j2);
        }
    }

    public synchronized void A07(File file) {
        this.A0G = file;
        for (AnonymousClass2BZ r0 : this.A0F) {
            r0.AQT(this);
        }
    }

    public synchronized boolean A08() {
        return this.A0B;
    }

    public synchronized boolean A09(long j) {
        boolean z;
        if (this.A01 == 3) {
            z = true;
        } else if (this.A07 == null) {
            z = false;
        } else if (j <= A01()) {
            z = this.A07.A08(this.A07.A01(j));
        } else {
            throw new EOFException();
        }
        return z;
    }
}
