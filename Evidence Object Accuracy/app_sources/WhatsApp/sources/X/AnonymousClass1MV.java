package X;

import com.whatsapp.Me;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;

/* renamed from: X.1MV  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1MV extends ObjectInputStream {
    public final /* synthetic */ C15570nT A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass1MV(C15570nT r1, InputStream inputStream) {
        super(inputStream);
        this.A00 = r1;
    }

    @Override // java.io.ObjectInputStream
    public ObjectStreamClass readClassDescriptor() {
        ObjectStreamClass readClassDescriptor = super.readClassDescriptor();
        return readClassDescriptor.getName().equals("com.whatsapp.App$Me") ? ObjectStreamClass.lookup(Me.class) : readClassDescriptor;
    }
}
