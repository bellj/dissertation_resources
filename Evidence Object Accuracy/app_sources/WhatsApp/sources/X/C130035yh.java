package X;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.WaTextView;
import com.whatsapp.payments.ui.BrazilPaymentCardDetailsActivity;
import com.whatsapp.payments.ui.NoviPaymentBankDetailsActivity;
import com.whatsapp.payments.ui.NoviPaymentCardDetailsActivity;

/* renamed from: X.5yh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130035yh {
    public View A00;
    public ImageView A01;
    public TextView A02;
    public final int A03;
    public final AbstractView$OnClickListenerC121765jx A04;

    public C130035yh(AbstractView$OnClickListenerC121765jx r2) {
        int i;
        this.A04 = r2;
        if ((r2 instanceof NoviPaymentBankDetailsActivity) || (r2 instanceof NoviPaymentCardDetailsActivity) || (r2 instanceof BrazilPaymentCardDetailsActivity)) {
            i = R.color.fb_pay_hub_icon_tint;
        } else {
            i = R.color.settings_icon;
        }
        this.A03 = AnonymousClass00T.A00(r2, i);
    }

    public void A00() {
        AbstractC30871Zd r1;
        if (!(this instanceof C121455i8)) {
            this.A00.setVisibility(8);
            return;
        }
        C121455i8 r3 = (C121455i8) this;
        AbstractC28901Pl r12 = ((C130035yh) r3).A04.A09;
        if (C1311161i.A0B(r12) && (r1 = (AbstractC30871Zd) r12.A08) != null) {
            if (!r1.A0X) {
                r3.A02.setVisibility(8);
            }
            if (!r1.A0T) {
                r3.A01.setVisibility(8);
            }
        }
        r3.A00.setVisibility(8);
    }

    public void A01(AbstractC28901Pl r6) {
        int i;
        int i2;
        int i3;
        int i4;
        AbstractC30871Zd r3;
        View.OnClickListener onClickListener;
        AbstractC30871Zd r0;
        if (!(this instanceof C121455i8)) {
            AnonymousClass2GE.A07(this.A01, this.A03);
            boolean A1V = C12960it.A1V(r6.A01, 2);
            ImageView imageView = this.A01;
            int i5 = R.drawable.ic_settings_unstarred;
            if (A1V) {
                i5 = R.drawable.ic_settings_starred;
            }
            imageView.setImageResource(i5);
            TextView textView = this.A02;
            int i6 = R.string.default_payment_method_unset;
            if (A1V) {
                i6 = R.string.default_payment_method_set;
            }
            textView.setText(i6);
            View view = this.A00;
            if (!A1V) {
                onClickListener = this.A04;
            } else {
                onClickListener = null;
            }
            view.setOnClickListener(onClickListener);
            if (C1311161i.A0B(r6) && (r0 = (AbstractC30871Zd) r6.A08) != null && !r0.A0X) {
                A00();
                return;
            }
            return;
        }
        C121455i8 r4 = (C121455i8) this;
        boolean A1V2 = C12960it.A1V(r6.A01, 2);
        WaTextView waTextView = r4.A06;
        int i7 = R.string.p2p_default_method_message_disabled;
        if (A1V2) {
            i7 = R.string.p2p_default_method_message_enabled;
        }
        waTextView.setText(i7);
        WaTextView waTextView2 = r4.A06;
        if (A1V2) {
            i = r4.A09;
        } else {
            i = r4.A08;
        }
        waTextView2.setTextColor(i);
        ImageView imageView2 = r4.A04;
        int i8 = R.drawable.ic_group;
        if (A1V2) {
            i8 = R.drawable.ic_check_circle_24dp;
        }
        imageView2.setImageResource(i8);
        if (A1V2) {
            i2 = r4.A07;
        } else {
            i2 = ((C130035yh) r4).A03;
        }
        AnonymousClass2GE.A07(imageView2, i2);
        ViewGroup viewGroup = r4.A02;
        AbstractView$OnClickListenerC34281fs r02 = r4.A0D;
        if (A1V2) {
            viewGroup.setOnClickListener(null);
            viewGroup.setBackground(null);
        } else {
            viewGroup.setOnClickListener(r02);
            C42631vX.A00(viewGroup);
        }
        boolean A1V3 = C12960it.A1V(r6.A03, 2);
        WaTextView waTextView3 = r4.A05;
        int i9 = R.string.p2m_default_method_message_disabled;
        if (A1V3) {
            i9 = R.string.p2m_default_method_message_enabled;
        }
        waTextView3.setText(i9);
        WaTextView waTextView4 = r4.A05;
        if (A1V3) {
            i3 = r4.A09;
        } else {
            i3 = r4.A08;
        }
        waTextView4.setTextColor(i3);
        ImageView imageView3 = r4.A03;
        int i10 = R.drawable.ic_shopping_cart;
        if (A1V3) {
            i10 = R.drawable.ic_check_circle_24dp;
        }
        imageView3.setImageResource(i10);
        if (A1V3) {
            i4 = r4.A07;
        } else {
            i4 = ((C130035yh) r4).A03;
        }
        AnonymousClass2GE.A07(imageView3, i4);
        ViewGroup viewGroup2 = r4.A01;
        AbstractView$OnClickListenerC34281fs r03 = r4.A0C;
        if (A1V3) {
            viewGroup2.setOnClickListener(null);
            viewGroup2.setBackground(null);
        } else {
            viewGroup2.setOnClickListener(r03);
            C42631vX.A00(viewGroup2);
        }
        if (C1311161i.A0B(r6) && (r3 = (AbstractC30871Zd) r6.A08) != null) {
            int i11 = 0;
            r4.A02.setVisibility(C12960it.A02(r3.A0X ? 1 : 0));
            ViewGroup viewGroup3 = r4.A01;
            if (!r3.A0T) {
                i11 = 8;
            }
            viewGroup3.setVisibility(i11);
        }
    }
}
