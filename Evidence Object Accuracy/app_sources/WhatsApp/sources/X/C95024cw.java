package X;

import android.text.SpannableStringBuilder;
import android.util.Pair;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

/* renamed from: X.4cw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C95024cw {
    public List A00;
    public final long A01;
    public final long A02;
    public final C95024cw A03;
    public final AnonymousClass4WC A04;
    public final String A05;
    public final String A06;
    public final String A07;
    public final String A08;
    public final HashMap A09 = C12970iu.A11();
    public final HashMap A0A = C12970iu.A11();
    public final boolean A0B;
    public final String[] A0C;

    public C95024cw(C95024cw r2, AnonymousClass4WC r3, String str, String str2, String str3, String str4, String[] strArr, long j, long j2) {
        this.A07 = str;
        this.A08 = str2;
        this.A05 = str4;
        this.A04 = r3;
        this.A0C = strArr;
        this.A0B = C12960it.A1W(str2);
        this.A02 = j;
        this.A01 = j2;
        this.A06 = str3;
        this.A03 = r2;
    }

    public static SpannableStringBuilder A00(String str, Map map) {
        if (!map.containsKey(str)) {
            C94144bK r1 = new C94144bK();
            r1.A0E = new SpannableStringBuilder();
            map.put(str, r1);
        }
        return (SpannableStringBuilder) ((C94144bK) map.get(str)).A0E;
    }

    public static AnonymousClass4WC A01(AnonymousClass4WC r3, Map map, String[] strArr) {
        int i = 0;
        if (r3 == null) {
            if (strArr == null) {
                return null;
            }
            int length = strArr.length;
            if (length == 1) {
                return (AnonymousClass4WC) map.get(strArr[0]);
            }
            if (length <= 1) {
                return r3;
            }
            AnonymousClass4WC r32 = new AnonymousClass4WC();
            while (i < length) {
                r32.A00((AnonymousClass4WC) map.get(strArr[i]));
                i++;
            }
            return r32;
        } else if (strArr == null) {
            return r3;
        } else {
            int length2 = strArr.length;
            if (length2 == 1) {
                r3.A00((AnonymousClass4WC) map.get(strArr[0]));
                return r3;
            } else if (length2 <= 1) {
                return r3;
            } else {
                while (i < length2) {
                    r3.A00((AnonymousClass4WC) map.get(strArr[i]));
                    i++;
                }
                return r3;
            }
        }
    }

    public C95024cw A02(int i) {
        List list = this.A00;
        if (list != null) {
            return (C95024cw) list.get(i);
        }
        throw new IndexOutOfBoundsException();
    }

    public final void A03(String str, List list, long j) {
        int size;
        String str2;
        String str3 = this.A06;
        if (!"".equals(str3)) {
            str = str3;
        }
        if (!A07(j) || !"div".equals(this.A07) || (str2 = this.A05) == null) {
            int i = 0;
            while (true) {
                List list2 = this.A00;
                if (list2 == null) {
                    size = 0;
                } else {
                    size = list2.size();
                }
                if (i < size) {
                    A02(i).A03(str, list, j);
                    i++;
                } else {
                    return;
                }
            }
        } else {
            list.add(new Pair(str, str2));
        }
    }

    public final void A04(String str, Map map, long j, boolean z) {
        int size;
        boolean z2;
        String str2 = str;
        HashMap hashMap = this.A0A;
        hashMap.clear();
        HashMap hashMap2 = this.A09;
        hashMap2.clear();
        String str3 = this.A07;
        if (!"metadata".equals(str3)) {
            String str4 = this.A06;
            if (!"".equals(str4)) {
                str2 = str4;
            }
            if (this.A0B && z) {
                A00(str2, map).append((CharSequence) this.A08);
            } else if ("br".equals(str3) && z) {
                A00(str2, map).append('\n');
            } else if (A07(j)) {
                Iterator A0n = C12960it.A0n(map);
                while (A0n.hasNext()) {
                    Map.Entry A15 = C12970iu.A15(A0n);
                    C12960it.A1K(A15.getKey(), hashMap, ((C94144bK) A15.getValue()).A0E.length());
                }
                boolean equals = "p".equals(str3);
                int i = 0;
                while (true) {
                    List list = this.A00;
                    if (list == null) {
                        size = 0;
                    } else {
                        size = list.size();
                    }
                    if (i >= size) {
                        break;
                    }
                    C95024cw A02 = A02(i);
                    if (!z) {
                        z2 = false;
                        if (!equals) {
                            A02.A04(str2, map, j, z2);
                            i++;
                        }
                    }
                    z2 = true;
                    A02.A04(str2, map, j, z2);
                    i++;
                }
                if (equals) {
                    SpannableStringBuilder A00 = A00(str2, map);
                    int length = A00.length();
                    while (true) {
                        length--;
                        if (length < 0) {
                            break;
                        } else if (A00.charAt(length) != ' ') {
                            if (A00.charAt(length) != '\n') {
                                A00.append('\n');
                            }
                        }
                    }
                }
                Iterator A0n2 = C12960it.A0n(map);
                while (A0n2.hasNext()) {
                    Map.Entry A152 = C12970iu.A15(A0n2);
                    C12960it.A1K(A152.getKey(), hashMap2, ((C94144bK) A152.getValue()).A0E.length());
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00df, code lost:
        if (r13 == 1) goto L_0x00e1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A05(java.lang.String r27, java.util.Map r28, java.util.Map r29, java.util.Map r30, long r31) {
        /*
        // Method dump skipped, instructions count: 520
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95024cw.A05(java.lang.String, java.util.Map, java.util.Map, java.util.Map, long):void");
    }

    public final void A06(TreeSet treeSet, boolean z) {
        boolean z2;
        String str = this.A07;
        boolean equals = "p".equals(str);
        boolean equals2 = "div".equals(str);
        if (z || equals || (equals2 && this.A05 != null)) {
            long j = this.A02;
            if (j != -9223372036854775807L) {
                treeSet.add(Long.valueOf(j));
            }
            long j2 = this.A01;
            if (j2 != -9223372036854775807L) {
                treeSet.add(Long.valueOf(j2));
            }
        }
        if (this.A00 != null) {
            for (int i = 0; i < this.A00.size(); i++) {
                C95024cw r1 = (C95024cw) this.A00.get(i);
                if (!z) {
                    z2 = false;
                    if (!equals) {
                        r1.A06(treeSet, z2);
                    }
                }
                z2 = true;
                r1.A06(treeSet, z2);
            }
        }
    }

    public boolean A07(long j) {
        long j2 = this.A02;
        if (j2 == -9223372036854775807L && this.A01 == -9223372036854775807L) {
            return true;
        }
        if (j2 <= j && this.A01 == -9223372036854775807L) {
            return true;
        }
        if (j2 != -9223372036854775807L || j >= this.A01) {
            return j2 <= j && j < this.A01;
        }
        return true;
    }
}
