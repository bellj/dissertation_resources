package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0400000_I0;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.15V  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass15V {
    public static final AtomicInteger A09 = new AtomicInteger(0);
    public final RunnableBRunnable0Shape0S0400000_I0 A00;
    public final AnonymousClass15S A01;
    public final AbstractC20460vn A02;
    public final AnonymousClass15U A03;
    public final C243115a A04;
    public final AbstractC14440lR A05;
    public final AnonymousClass01H A06;
    public final ArrayBlockingQueue A07;
    public final ConcurrentHashMap A08;

    public AnonymousClass15V(AnonymousClass15S r7, AbstractC20460vn r8, AnonymousClass15U r9, C243115a r10, AbstractC14440lR r11, AnonymousClass01H r12) {
        this.A06 = r12;
        this.A05 = r11;
        this.A02 = r8;
        this.A01 = r7;
        this.A03 = r9;
        this.A04 = r10;
        ArrayBlockingQueue arrayBlockingQueue = new ArrayBlockingQueue(100);
        this.A07 = arrayBlockingQueue;
        ConcurrentHashMap concurrentHashMap = new ConcurrentHashMap();
        this.A08 = concurrentHashMap;
        this.A00 = new RunnableBRunnable0Shape0S0400000_I0(arrayBlockingQueue, concurrentHashMap, r8, r12, 29);
    }

    public static void A00(AbstractC28621Oh r1, C28631Oi r2, int i) {
        if (i == 1) {
            r1.AWI(r2);
        } else if (i == 2) {
            r1.APh(r2);
        } else {
            StringBuilder sb = new StringBuilder("Unknown event type: ");
            sb.append(i);
            throw new IllegalStateException(sb.toString());
        }
    }

    public final void A01(int i) {
        ConcurrentHashMap concurrentHashMap = this.A08;
        Integer valueOf = Integer.valueOf(i);
        AtomicInteger atomicInteger = (AtomicInteger) concurrentHashMap.get(valueOf);
        if (atomicInteger == null) {
            atomicInteger = new AtomicInteger(0);
            AtomicInteger atomicInteger2 = (AtomicInteger) concurrentHashMap.putIfAbsent(valueOf, atomicInteger);
            if (atomicInteger2 != null) {
                atomicInteger = atomicInteger2;
            }
        }
        atomicInteger.incrementAndGet();
    }

    public final void A02(C28631Oi r7, int i) {
        AnonymousClass1Q1 r4 = r7.A05;
        if (r4 != AnonymousClass1Q1.A02) {
            for (AbstractC28621Oh r0 : r4.A00) {
                A00(r0, r7, i);
            }
            for (AbstractC28621Oh r2 : r4.A01) {
                if (this.A07.offer(new C28611Og(r2, r7, i))) {
                    A01(r7.A01);
                } else {
                    this.A02.A6J();
                }
            }
            this.A05.Ab4(this.A00, "qpl_bg_listeners");
        }
    }
}
