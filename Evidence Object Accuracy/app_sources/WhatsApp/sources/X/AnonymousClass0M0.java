package X;

import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0M0  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0M0 {
    public static final Map A00;

    static {
        HashMap hashMap = new HashMap(10);
        A00 = hashMap;
        hashMap.put("none", EnumC03780Jc.none);
        hashMap.put("xMinYMin", EnumC03780Jc.xMinYMin);
        hashMap.put("xMidYMin", EnumC03780Jc.xMidYMin);
        hashMap.put("xMaxYMin", EnumC03780Jc.xMaxYMin);
        hashMap.put("xMinYMid", EnumC03780Jc.xMinYMid);
        hashMap.put("xMidYMid", EnumC03780Jc.xMidYMid);
        hashMap.put("xMaxYMid", EnumC03780Jc.xMaxYMid);
        hashMap.put("xMinYMax", EnumC03780Jc.xMinYMax);
        hashMap.put("xMidYMax", EnumC03780Jc.xMidYMax);
        hashMap.put("xMaxYMax", EnumC03780Jc.xMaxYMax);
    }
}
