package X;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Parcelable;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.3a2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69663a2 implements AnonymousClass5WH {
    public final C16590pI A00;

    public C69663a2(C16590pI r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5WH
    public Intent AGj(List list, int i) {
        String str;
        if (!(i == 5 || i == 4)) {
            if (i == 1) {
                str = "image/png";
            } else {
                str = (i == 2 || i == 4) ? "video/mp4" : "*/*";
            }
            String str2 = "com.facebook.lite.composer.activities.ShareIntentMultiPhotoAlphabeticalAlias";
            if (!(i == 5 || i == 3 || i == 1)) {
                str2 = "com.facebook.lite.composer.activities.ShareIntentVideoAlphabeticalAlias";
            }
            ArrayList<? extends Parcelable> A0l = C12960it.A0l();
            JSONArray jSONArray = new JSONArray();
            for (int i2 = 0; i2 < list.size(); i2++) {
                JSONObject jSONObject = new JSONObject();
                AnonymousClass4TH r11 = (AnonymousClass4TH) list.get(i2);
                Uri uri = r11.A02;
                try {
                    jSONObject.put("story_media_caption", r11.A04);
                    jSONObject.put("story_media_uri", uri.toString());
                    jSONObject.put("story_media_video_length_sec", r11.A01);
                    jSONObject.put("story_media_aspect_ratio", r11.A00);
                    jSONObject.put("story_media_link_url", r11.A05);
                    jSONArray.put(jSONObject.toString());
                    A0l.add(uri);
                } catch (JSONException e) {
                    Log.w("liteposter/json", e);
                }
            }
            Intent putExtra = C12970iu.A0A().setType(str).setPackage("com.facebook.lite").setComponent(new ComponentName("com.facebook.lite", str2)).addFlags(268435456).putExtra("com.facebook.platform.extra.APPLICATION_ID", "994766073959253").putExtra("editing_disabled", true).putExtra("media_list", jSONArray.toString());
            if (list.size() == 1) {
                putExtra.setAction("android.intent.action.SEND").putExtra("android.intent.extra.STREAM", ((AnonymousClass4TH) list.get(0)).A02);
            } else if (list.size() > 1) {
                putExtra.setAction("android.intent.action.SEND_MULTIPLE").putParcelableArrayListExtra("android.intent.extra.STREAM", A0l);
            }
            List<ResolveInfo> queryIntentActivities = this.A00.A00.getPackageManager().queryIntentActivities(putExtra, 65536);
            if (!(queryIntentActivities == null || queryIntentActivities.size() == 0)) {
                return putExtra;
            }
        }
        return null;
    }

    @Override // X.AnonymousClass5WH
    public boolean AKG() {
        Intent A0A = C12970iu.A0A();
        A0A.setPackage("com.facebook.lite").setType("image/png").setComponent(new ComponentName("com.facebook.lite", "com.facebook.lite.composer.activities.ShareIntentMultiPhotoAlphabeticalAlias"));
        Context context = this.A00.A00;
        try {
            List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(A0A, 65536);
            if (queryIntentActivities == null || queryIntentActivities.size() <= 0) {
                return false;
            }
            try {
                if (Integer.parseInt(context.getPackageManager().getPackageInfo("com.facebook.lite", 0).versionName.split("\\.")[0]) >= 91) {
                    return true;
                }
                return false;
            } catch (Exception e) {
                Log.w("Cannot get FBLite version number", e);
                return false;
            }
        } catch (Exception unused) {
            return false;
        }
    }
}
