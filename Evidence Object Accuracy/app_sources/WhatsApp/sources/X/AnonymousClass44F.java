package X;

import com.whatsapp.search.SearchViewModel;

/* renamed from: X.44F  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass44F extends AbstractC51682Vy {
    public final SearchViewModel A00;
    public final AnonymousClass34e A01;

    public AnonymousClass44F(SearchViewModel searchViewModel, AnonymousClass34e r2) {
        super(r2);
        this.A01 = r2;
        this.A00 = searchViewModel;
    }
}
