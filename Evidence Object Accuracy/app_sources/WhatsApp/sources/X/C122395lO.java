package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5lO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122395lO extends AbstractC118825cR {
    public final TextView A00;
    public final TextView A01;

    public C122395lO(View view) {
        super(view);
        this.A00 = C12960it.A0I(view, R.id.personal_info_label);
        this.A01 = C12960it.A0I(view, R.id.personal_info_text);
    }
}
