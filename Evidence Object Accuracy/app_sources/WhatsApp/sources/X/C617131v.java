package X;

import android.app.Activity;
import android.view.View;
import com.facebook.redex.ViewOnClickCListenerShape1S0400000_I1;
import com.whatsapp.R;

/* renamed from: X.31v  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C617131v extends AbstractC51682Vy {
    public C617131v(Activity activity, View view, AnonymousClass1BF r11, C14820m6 r12, C22050yP r13) {
        super(view);
        View A0D = AnonymousClass028.A0D(view, R.id.empty_community_row_button);
        C27531Hw.A06(C12960it.A0I(view, R.id.empty_community_row_title));
        A0D.setOnClickListener(new ViewOnClickCListenerShape1S0400000_I1(r11, r13, r12, activity, 3));
    }
}
