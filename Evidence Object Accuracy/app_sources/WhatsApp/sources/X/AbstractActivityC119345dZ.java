package X;

import com.whatsapp.payments.ui.BusinessHubActivity;

/* renamed from: X.5dZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC119345dZ extends ActivityC13790kL {
    public boolean A00 = false;

    public AbstractActivityC119345dZ() {
        C117295Zj.A0p(this, 25);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            BusinessHubActivity businessHubActivity = (BusinessHubActivity) this;
            AnonymousClass2FL r3 = (AnonymousClass2FL) ((AnonymousClass2FJ) generatedComponent());
            AnonymousClass01J A1M = ActivityC13830kP.A1M(r3, businessHubActivity);
            ActivityC13810kN.A10(A1M, businessHubActivity);
            ((ActivityC13790kL) businessHubActivity).A08 = ActivityC13790kL.A0S(r3, A1M, businessHubActivity, ActivityC13790kL.A0Y(A1M, businessHubActivity));
            businessHubActivity.A0A = C117305Zk.A0P(A1M);
            businessHubActivity.A0B = (C25901Bg) A1M.AEg.get();
        }
    }
}
