package X;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.5qa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C125045qa {
    public static C1316663q A00(AnonymousClass1V8 r6) {
        List A0J = r6.A0J("choice");
        ArrayList A0l = C12960it.A0l();
        Iterator it = A0J.iterator();
        while (it.hasNext()) {
            AnonymousClass1V8 A0d = C117305Zk.A0d(it);
            A0l.add(new C126915te(A0d.A0H("primary_step_up"), C117295Zj.A0W(A0d, "alternative_step_up")));
        }
        String A0H = r6.A0H("metadata");
        String A0H2 = r6.A0H("entry_flow");
        AnonymousClass1V8 A0E = r6.A0E("message");
        C130625zi r1 = null;
        if (A0E != null) {
            r1 = new C130625zi(A0E);
        }
        return new C1316663q(r1, A0H, A0H2, C117295Zj.A0W(r6, "action_id"), A0l);
    }
}
