package X;

import android.database.Cursor;
import com.whatsapp.jid.UserJid;

/* renamed from: X.1ip  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C35831ip {
    public final C30751Yr A00;

    public C35831ip(Cursor cursor, UserJid userJid) {
        AnonymousClass009.A05(userJid);
        C30751Yr r2 = new C30751Yr(userJid);
        this.A00 = r2;
        r2.A00 = cursor.getDouble(1);
        r2.A01 = cursor.getDouble(2);
        r2.A03 = cursor.getInt(3);
        r2.A02 = cursor.getFloat(4);
        r2.A04 = cursor.getInt(5);
        r2.A05 = cursor.getLong(6);
    }
}
