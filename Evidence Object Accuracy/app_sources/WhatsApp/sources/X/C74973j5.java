package X;

import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.3j5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74973j5 extends AbstractC05270Ox {
    public final /* synthetic */ C63423Bn A00;

    public C74973j5(C63423Bn r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC05270Ox
    public void A01(RecyclerView recyclerView, int i, int i2) {
        if (i != 0) {
            this.A00.A03 += i;
        }
        if (i2 != 0) {
            this.A00.A04 += i2;
        }
    }
}
