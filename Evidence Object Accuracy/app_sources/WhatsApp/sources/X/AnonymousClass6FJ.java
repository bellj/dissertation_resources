package X;

import java.util.List;

/* renamed from: X.6FJ  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6FJ implements Runnable {
    public final /* synthetic */ AnonymousClass61Q A00;

    public AnonymousClass6FJ(AnonymousClass61Q r1) {
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        List list = this.A00.A0M.A00;
        if (0 < list.size()) {
            list.get(0);
            throw C12980iv.A0n("onPreviewStopped");
        }
    }
}
