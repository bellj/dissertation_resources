package X;

import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* renamed from: X.15t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C245015t {
    public final C15990oG A00;
    public final C18770sz A01;
    public final C20660w7 A02;

    public C245015t(C15990oG r1, C18770sz r2, C20660w7 r3) {
        this.A02 = r3;
        this.A00 = r1;
        this.A01 = r2;
    }

    public Map A00(Iterable iterable) {
        HashMap hashMap = new HashMap();
        Iterator it = iterable.iterator();
        while (it.hasNext()) {
            DeviceJid deviceJid = (DeviceJid) it.next();
            AnonymousClass1JS A0E = this.A00.A0E(C15940oB.A02(deviceJid));
            if (A0E != null) {
                hashMap.put(deviceJid, A0E);
            }
        }
        return hashMap;
    }

    public void A01(UserJid userJid) {
        Set A0E = this.A01.A0E(userJid);
        Map A00 = A00(A0E);
        ArrayList arrayList = new ArrayList(A0E);
        arrayList.removeAll(A00.keySet());
        if (!arrayList.isEmpty()) {
            this.A02.A0K(arrayList);
        }
    }
}
