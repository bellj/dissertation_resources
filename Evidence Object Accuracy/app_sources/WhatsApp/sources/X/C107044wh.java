package X;

import android.util.Pair;

/* renamed from: X.4wh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107044wh implements AbstractC117075Yd {
    public final long A00;
    public final long[] A01;
    public final long[] A02;

    @Override // X.AbstractC117075Yd
    public long ACO() {
        return -1;
    }

    @Override // X.AnonymousClass5WY
    public boolean AK2() {
        return true;
    }

    public C107044wh(long[] jArr, long[] jArr2, long j) {
        this.A01 = jArr;
        this.A02 = jArr2;
        this.A00 = j == -9223372036854775807L ? C95214dK.A01(jArr2[jArr2.length - 1]) : j;
    }

    public static Pair A00(long[] jArr, long[] jArr2, long j) {
        double d;
        Long valueOf;
        Long valueOf2;
        int A06 = AnonymousClass3JZ.A06(jArr, j, true);
        long j2 = jArr[A06];
        long j3 = jArr2[A06];
        int i = A06 + 1;
        if (i == jArr.length) {
            valueOf = Long.valueOf(j2);
            valueOf2 = Long.valueOf(j3);
        } else {
            long j4 = jArr[i];
            long j5 = jArr2[i];
            if (j4 == j2) {
                d = 0.0d;
            } else {
                d = (((double) j) - ((double) j2)) / ((double) (j4 - j2));
            }
            valueOf = Long.valueOf(j);
            valueOf2 = Long.valueOf(((long) (d * ((double) (j5 - j3)))) + j3);
        }
        return Pair.create(valueOf, valueOf2);
    }

    @Override // X.AnonymousClass5WY
    public long ACc() {
        return this.A00;
    }

    @Override // X.AnonymousClass5WY
    public C92684Xa AGX(long j) {
        Pair A00 = A00(this.A02, this.A01, C95214dK.A02(C72453ed.A0X(j, this.A00)));
        C94324bc r1 = new C94324bc(C95214dK.A01(C12980iv.A0G(A00.first)), C12980iv.A0G(A00.second));
        return new C92684Xa(r1, r1);
    }

    @Override // X.AbstractC117075Yd
    public long AHD(long j) {
        return C95214dK.A01(C12980iv.A0G(A00(this.A01, this.A02, j).second));
    }
}
