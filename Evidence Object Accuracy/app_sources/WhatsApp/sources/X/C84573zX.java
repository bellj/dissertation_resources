package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.3zX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C84573zX extends AbstractC88024Dy {
    public final UserJid A00;
    public final String A01;
    public final String A02;

    public C84573zX(UserJid userJid, String str, String str2) {
        this.A00 = userJid;
        this.A01 = str;
        this.A02 = str2;
    }
}
