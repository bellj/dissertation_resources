package X;

import com.whatsapp.jid.GroupJid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0nj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15680nj {
    public final C19990v2 A00;
    public final ArrayList A01 = new ArrayList();
    public final Comparator A02 = new Comparator() { // from class: X.1W1
        @Override // java.util.Comparator
        public final int compare(Object obj, Object obj2) {
            AnonymousClass1W2 r6 = (AnonymousClass1W2) obj;
            AnonymousClass1W2 r7 = (AnonymousClass1W2) obj2;
            long j = r6.A00;
            long j2 = r7.A00;
            if (j != j2) {
                return j < j2 ? 1 : -1;
            }
            return r6.A01.getRawString().compareTo(r7.A01.getRawString());
        }
    };

    public C15680nj(C19990v2 r2) {
        this.A00 = r2;
    }

    public int A00() {
        int i;
        C19990v2 r4 = this.A00;
        r4.A0B();
        ArrayList arrayList = this.A01;
        synchronized (arrayList) {
            Iterator it = arrayList.iterator();
            i = 0;
            while (it.hasNext()) {
                if (r4.A0E(((AnonymousClass1W2) it.next()).A01)) {
                    i++;
                }
            }
        }
        return i;
    }

    public int A01() {
        int size;
        this.A00.A0B();
        ArrayList arrayList = this.A01;
        synchronized (arrayList) {
            size = arrayList.size();
        }
        return size;
    }

    public int A02() {
        int i;
        C19990v2 r4 = this.A00;
        r4.A0B();
        ArrayList arrayList = this.A01;
        synchronized (arrayList) {
            Iterator it = arrayList.iterator();
            i = 0;
            while (it.hasNext()) {
                if (!r4.A0E(((AnonymousClass1W2) it.next()).A01)) {
                    i++;
                }
            }
        }
        return i;
    }

    public final int A03(AbstractC14640lm r4) {
        ArrayList arrayList = this.A01;
        synchronized (arrayList) {
            for (int i = 0; i < arrayList.size(); i++) {
                if (((AnonymousClass1W2) arrayList.get(i)).A01.equals(r4)) {
                    return i;
                }
            }
            return -1;
        }
    }

    public List A04() {
        ArrayList arrayList;
        this.A00.A0B();
        ArrayList arrayList2 = this.A01;
        synchronized (arrayList2) {
            arrayList = new ArrayList(arrayList2.size());
            Iterator it = arrayList2.iterator();
            while (it.hasNext()) {
                arrayList.add(((AnonymousClass1W2) it.next()).A01);
            }
        }
        return arrayList;
    }

    public List A05() {
        C19990v2 r5 = this.A00;
        r5.A0B();
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = this.A01;
        synchronized (arrayList2) {
            Iterator it = arrayList2.iterator();
            while (it.hasNext()) {
                AnonymousClass1W2 r1 = (AnonymousClass1W2) it.next();
                if (r5.A0E(r1.A01)) {
                    arrayList.add(r1.A01);
                }
            }
        }
        return arrayList;
    }

    public List A06() {
        ArrayList arrayList;
        this.A00.A0B();
        ArrayList arrayList2 = this.A01;
        synchronized (arrayList2) {
            arrayList = new ArrayList();
            Iterator it = arrayList2.iterator();
            while (it.hasNext()) {
                AbstractC14640lm r1 = ((AnonymousClass1W2) it.next()).A01;
                if (C15380n4.A0J(r1) || C15380n4.A0G(r1)) {
                    arrayList.add((AbstractC15590nW) r1);
                }
            }
        }
        return arrayList;
    }

    public List A07() {
        C19990v2 r5 = this.A00;
        r5.A0B();
        ArrayList arrayList = this.A01;
        ArrayList arrayList2 = new ArrayList(arrayList.size());
        synchronized (arrayList) {
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                AnonymousClass1W2 r1 = (AnonymousClass1W2) it.next();
                if (!r5.A0E(r1.A01)) {
                    arrayList2.add(r1.A01);
                }
            }
        }
        return arrayList2;
    }

    public List A08(C15860o1 r7) {
        this.A00.A0B();
        ArrayList arrayList = this.A01;
        ArrayList arrayList2 = new ArrayList(arrayList.size());
        Set A0D = r7.A0D();
        ArrayList arrayList3 = new ArrayList();
        synchronized (arrayList) {
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                arrayList3.add(((AnonymousClass1W2) it.next()).A01);
            }
        }
        Iterator it2 = arrayList3.iterator();
        while (it2.hasNext()) {
            Object next = it2.next();
            if (!A0D.contains(next)) {
                arrayList2.add(next);
            }
        }
        A0D.retainAll(arrayList3);
        arrayList2.addAll(0, A0D);
        return arrayList2;
    }

    public List A09(C15860o1 r8) {
        C19990v2 r6 = this.A00;
        r6.A0B();
        ArrayList arrayList = this.A01;
        ArrayList arrayList2 = new ArrayList(arrayList.size());
        Set A0D = r8.A0D();
        ArrayList arrayList3 = new ArrayList();
        synchronized (arrayList) {
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                arrayList3.add(((AnonymousClass1W2) it.next()).A01);
            }
        }
        A0D.retainAll(arrayList3);
        arrayList2.addAll(A0D);
        Iterator it2 = arrayList3.iterator();
        while (it2.hasNext()) {
            AbstractC14640lm r1 = (AbstractC14640lm) it2.next();
            if (!r6.A0E(r1) && !A0D.contains(r1)) {
                arrayList2.add(r1);
            }
        }
        return arrayList2;
    }

    public Map A0A(Collection collection) {
        Boolean bool;
        HashMap hashMap = new HashMap();
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            C15370n3 r1 = (C15370n3) it.next();
            if (A0D((AbstractC14640lm) r1.A0B(AbstractC14640lm.class))) {
                bool = Boolean.TRUE;
            } else {
                bool = Boolean.FALSE;
            }
            hashMap.put(r1, bool);
        }
        return hashMap;
    }

    public Set A0B() {
        HashSet hashSet;
        this.A00.A0B();
        ArrayList arrayList = this.A01;
        synchronized (arrayList) {
            hashSet = new HashSet(arrayList.size());
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                hashSet.add(((AnonymousClass1W2) it.next()).A01);
            }
        }
        return hashSet;
    }

    public void A0C(AbstractC14640lm r3) {
        this.A00.A0B();
        ArrayList arrayList = this.A01;
        synchronized (arrayList) {
            int A03 = A03(r3);
            if (A03 >= 0) {
                arrayList.remove(A03);
            }
        }
    }

    public boolean A0D(AbstractC14640lm r3) {
        this.A00.A0B();
        return A03(r3) >= 0;
    }

    public final boolean A0E(AbstractC14640lm r7, AbstractC14640lm r8, long j) {
        AnonymousClass1W2 r4;
        boolean z;
        ArrayList arrayList = this.A01;
        synchronized (arrayList) {
            int A03 = A03(r7);
            if (A03 >= 0) {
                r4 = (AnonymousClass1W2) arrayList.remove(A03);
            } else {
                r4 = new AnonymousClass1W2();
            }
            boolean z2 = true;
            if (this.A00.A03(GroupJid.of(r8)) != 1) {
                z2 = false;
            }
            z = true;
            if (z2) {
                if (A03 >= 0) {
                }
                z = false;
            } else {
                AnonymousClass009.A05(r8);
                r4.A01 = r8;
                r4.A00 = j;
                int binarySearch = Collections.binarySearch(arrayList, r4, this.A02);
                boolean z3 = false;
                if (binarySearch < 0) {
                    z3 = true;
                }
                AnonymousClass009.A0E(z3);
                int i = (-binarySearch) - 1;
                arrayList.add(i, r4);
                if (i != A03) {
                }
                z = false;
            }
        }
        return z;
    }
}
