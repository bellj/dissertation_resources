package X;

import com.facebook.redex.EmptyBaseRunnable0;

/* renamed from: X.2jF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class RunnableC55742jF extends EmptyBaseRunnable0 implements Runnable {
    public final /* synthetic */ float A00;
    public final /* synthetic */ float A01;
    public final /* synthetic */ float A02 = 300.0f;
    public final /* synthetic */ float A03;
    public final /* synthetic */ float A04;
    public final /* synthetic */ long A05;
    public final /* synthetic */ AnonymousClass289 A06;
    public final /* synthetic */ Runnable A07;

    public RunnableC55742jF(AnonymousClass289 r2, Runnable runnable, float f, float f2, float f3, float f4, long j) {
        this.A06 = r2;
        this.A05 = j;
        this.A04 = f;
        this.A03 = f2;
        this.A00 = f3;
        this.A01 = f4;
        this.A07 = runnable;
    }

    @Override // java.lang.Runnable
    public void run() {
        long currentTimeMillis = System.currentTimeMillis();
        float f = this.A02;
        float min = Math.min(f, (float) (currentTimeMillis - this.A05));
        AnonymousClass289 r2 = this.A06;
        r2.A02(this.A04 + (this.A03 * min), this.A00, this.A01);
        this.A07.run();
        if (min < f) {
            r2.A09.post(this);
        }
    }
}
