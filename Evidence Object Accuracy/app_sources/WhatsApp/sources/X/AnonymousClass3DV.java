package X;

import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.RectF;
import android.view.View;

/* renamed from: X.3DV  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3DV {
    public float A00;
    public float A01;
    public float A02;
    public int A03;
    public boolean A04;
    public final Paint A05 = C12990iw.A0G(1);
    public final Paint A06 = C12990iw.A0G(1);
    public final Path A07 = new Path();
    public final Path A08 = new Path();
    public final RectF A09 = C12980iv.A0K();
    public final RectF A0A = C12980iv.A0K();
    public final View A0B;
    public final float[] A0C = new float[8];

    public AnonymousClass3DV(View view) {
        this.A0B = view;
    }

    public void A00(float[] fArr, float f, float f2, float f3, int i, int i2, int i3) {
        this.A03 = i3;
        this.A02 = f2;
        Paint paint = this.A06;
        paint.setColor(i);
        if (i == 0) {
            C12990iw.A14(paint, PorterDuff.Mode.CLEAR);
        }
        int i4 = (f > 0.0f ? 1 : (f == 0.0f ? 0 : -1));
        Paint paint2 = this.A05;
        if (i4 != 0) {
            paint2.setColor(i2);
        } else {
            paint2.setColor(0);
        }
        C12990iw.A13(paint2);
        paint2.setStrokeWidth(f);
        if (f > 0.0f && fArr != null) {
            paint2.setPathEffect(new DashPathEffect(fArr, f3));
        }
        Path path = this.A08;
        path.setFillType(Path.FillType.EVEN_ODD);
        float f4 = f / 2.0f;
        this.A00 = f4;
        this.A01 = this.A02 - f4;
        path.reset();
    }
}
