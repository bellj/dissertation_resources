package X;

import android.text.TextUtils;
import com.whatsapp.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.60R  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass60R {
    public int A00;
    public long A01;
    public long A02;
    public long A03;
    public long A04;
    public AnonymousClass20C A05;
    public AnonymousClass1ZR A06;
    public AnonymousClass1ZR A07;
    public AnonymousClass1ZR A08;
    @Deprecated
    public AnonymousClass1ZR A09;
    public AnonymousClass1ZR A0A;
    public C1309760s A0B;
    public AnonymousClass60O A0C;
    public String A0D;
    public String A0E;
    public String A0F;
    public String A0G;
    public String A0H;
    public String A0I;
    public String A0J;
    public boolean A0K;
    public boolean A0L;
    public C130755zv[] A0M;

    /* JADX WARNING: Code restructure failed: missing block: B:68:0x01bf, code lost:
        if (r3.equals(r1) == false) goto L_0x01c1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass60R(X.AnonymousClass102 r14, X.AnonymousClass1V8 r15) {
        /*
        // Method dump skipped, instructions count: 600
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass60R.<init>(X.102, X.1V8):void");
    }

    public AnonymousClass60R(String str) {
        Object obj;
        Object obj2;
        Object obj3;
        Object obj4;
        Object obj5;
        JSONArray jSONArray;
        int length;
        if (!TextUtils.isEmpty(str)) {
            try {
                JSONObject A05 = C13000ix.A05(str);
                this.A0K = A05.optBoolean("isMandate", false);
                this.A0L = A05.optBoolean("isRevocable", false);
                this.A02 = A05.optLong("mandateStartTs", this.A02);
                this.A01 = A05.optLong("mandateEndTs", this.A01);
                this.A0D = A05.optString("debitTs", this.A0D);
                this.A0H = A05.optString("previousDebitTs", this.A0H);
                this.A04 = A05.optLong("nextPaymentTs", this.A04);
                this.A03 = A05.optLong("nextPaymentEndTs", this.A03);
                this.A00 = A05.optInt("totalRecurringTxnCount", this.A00);
                this.A0F = A05.optString("mandateAmountRule", this.A0F);
                AnonymousClass2SM A0J = C117305Zk.A0J();
                AnonymousClass1ZR r0 = this.A09;
                if (r0 == null) {
                    obj = null;
                } else {
                    obj = r0.A00;
                }
                this.A09 = C117305Zk.A0I(A0J, String.class, A05.optString("originalAmount", (String) obj), "moneyStringValue");
                if (A05.has("originalMoney")) {
                    this.A05 = new C94074bD(A05.optJSONObject("originalMoney")).A00();
                }
                AnonymousClass2SM A0J2 = C117305Zk.A0J();
                AnonymousClass1ZR r02 = this.A08;
                if (r02 == null) {
                    obj2 = null;
                } else {
                    obj2 = r02.A00;
                }
                this.A08 = C117305Zk.A0I(A0J2, String.class, A05.optString("mandateNo", (String) obj2), "mandateNo");
                AnonymousClass2SM A0J3 = C117305Zk.A0J();
                AnonymousClass1ZR r03 = this.A07;
                if (r03 == null) {
                    obj3 = null;
                } else {
                    obj3 = r03.A00;
                }
                this.A07 = C117305Zk.A0I(A0J3, String.class, A05.optString("mandateName", (String) obj3), "mandateName");
                AnonymousClass2SM A0J4 = C117305Zk.A0J();
                AnonymousClass1ZR r04 = this.A0A;
                if (r04 == null) {
                    obj4 = null;
                } else {
                    obj4 = r04.A00;
                }
                this.A0A = C117305Zk.A0I(A0J4, String.class, A05.optString("upiPurposeCode", (String) obj4), "upiPurposeCode");
                this.A0G = A05.optString("mandateErrorCode", this.A0G);
                AnonymousClass2SM A0J5 = C117305Zk.A0J();
                AnonymousClass1ZR r05 = this.A06;
                if (r05 == null) {
                    obj5 = null;
                } else {
                    obj5 = r05.A00;
                }
                this.A06 = C117305Zk.A0I(A0J5, String.class, A05.optString("mandateInfo", (String) obj5), "mandateInfo");
                this.A0E = A05.optString("frequencyRule", this.A0E);
                this.A0J = A05.optString("recurrenceRule", this.A0J);
                this.A0I = A05.optString("recurrenceDay", this.A0I);
                if (A05.has("pendingMandateUpdate")) {
                    this.A0C = new AnonymousClass60O(A05.optString("pendingMandateUpdate", null));
                }
                if (A05.has("pauseResumeStatusDetails")) {
                    this.A0B = new C1309760s(A05.optString("pauseResumeStatusDetails", null));
                }
                if (A05.has("instanceTransactions") && (length = (jSONArray = new JSONArray(A05.optString("instanceTransactions", "[]"))).length()) > 0) {
                    this.A0M = new C130755zv[length];
                    int i = 0;
                    while (true) {
                        this.A0M[i] = new C130755zv(jSONArray.get(i).toString());
                        i++;
                        if (i >= length) {
                            return;
                        }
                    }
                }
            } catch (JSONException e) {
                Log.w("PAY: IndiaUpiTransactionPendingUpdateMetadata threw: ", e);
            }
        }
    }

    public int A00() {
        C1309760s r7 = this.A0B;
        if (r7 != null) {
            String str = r7.A02;
            if (!str.equals("RESUME")) {
                if (str.equals("PAUSE")) {
                    String str2 = r7.A03;
                    switch (str2.hashCode()) {
                        case -1149187101:
                            if (str2.equals("SUCCESS")) {
                                return 2;
                            }
                            break;
                        case -368591510:
                            if (str2.equals("FAILURE")) {
                                return 3;
                            }
                            break;
                        case 35394935:
                            if (str2.equals("PENDING")) {
                                return 1;
                            }
                            break;
                    }
                }
            }
            String str3 = r7.A03;
            switch (str3.hashCode()) {
                case -1149187101:
                    if (str3.equals("SUCCESS")) {
                        return 5;
                    }
                    break;
                case -368591510:
                    if (str3.equals("FAILURE")) {
                        return 6;
                    }
                    break;
                case 35394935:
                    if (str3.equals("PENDING")) {
                        return 4;
                    }
                    break;
            }
        }
        return 0;
    }

    public String A01() {
        String str;
        Object obj;
        String str2;
        String str3;
        Object obj2;
        Object obj3;
        try {
            JSONObject A0a = C117295Zj.A0a();
            A0a.put("isMandate", this.A0K);
            A0a.put("isRevocable", this.A0L);
            long j = this.A02;
            if (j > 0) {
                A0a.put("mandateStartTs", j);
            }
            long j2 = this.A01;
            if (j2 > 0) {
                A0a.put("mandateEndTs", j2);
            }
            String str4 = this.A0F;
            if (str4 != null) {
                A0a.put("mandateAmountRule", str4);
            }
            AnonymousClass1ZR r1 = this.A08;
            if (r1 != null) {
                C117315Zl.A0U(r1, "mandateNo", A0a);
            }
            AnonymousClass1ZR r12 = this.A07;
            if (r12 != null) {
                C117315Zl.A0U(r12, "mandateName", A0a);
            }
            AnonymousClass1ZR r13 = this.A0A;
            if (r13 != null) {
                C117315Zl.A0U(r13, "upiPurposeCode", A0a);
            }
            String str5 = this.A0G;
            if (str5 != null) {
                A0a.put("mandateErrorCode", str5);
            }
            AnonymousClass1ZR r14 = this.A09;
            if (!AnonymousClass1ZS.A03(r14)) {
                C117315Zl.A0U(r14, "originalAmount", A0a);
            }
            AnonymousClass20C r0 = this.A05;
            if (r0 != null) {
                A0a.put("originalMoney", r0.A02());
            }
            AnonymousClass60O r2 = this.A0C;
            if (r2 != null) {
                try {
                    JSONObject A0a2 = C117295Zj.A0a();
                    AnonymousClass1ZR r15 = r2.A03;
                    if (r15 != null) {
                        C117315Zl.A0U(r15, "pendingAmount", A0a2);
                    }
                    AnonymousClass20C r02 = r2.A01;
                    if (r02 != null) {
                        A0a2.put("pendingMoney", r02.A02());
                    }
                    String str6 = r2.A06;
                    if (str6 != null) {
                        A0a2.put("isRevocable", str6);
                    }
                    long j3 = r2.A00;
                    if (j3 > 0) {
                        A0a2.put("mandateEndTs", j3);
                    }
                    String str7 = r2.A07;
                    if (str7 != null) {
                        A0a2.put("mandateAmountRule", str7);
                    }
                    AnonymousClass1ZR r5 = r2.A04;
                    if (!AnonymousClass1ZS.A03(r5)) {
                        if (r5 == null) {
                            obj3 = null;
                        } else {
                            obj3 = r5.A00;
                        }
                        A0a2.put("seqNum", obj3);
                    }
                    String str8 = r2.A05;
                    if (str8 != null) {
                        A0a2.put("errorCode", str8);
                    }
                    String str9 = r2.A09;
                    if (str9 != null) {
                        A0a2.put("mandateUpdateStatus", str9);
                    }
                    String str10 = r2.A08;
                    if (str10 != null) {
                        A0a2.put("mandateUpdateAction", str10);
                    }
                    AnonymousClass1ZR r22 = r2.A02;
                    if (!AnonymousClass1ZS.A03(r22)) {
                        if (r22 == null) {
                            obj2 = null;
                        } else {
                            obj2 = r22.A00;
                        }
                        A0a2.put("mandateUpdateInfo", obj2);
                    }
                    str3 = A0a2.toString();
                } catch (JSONException e) {
                    Log.w("PAY: IndiaUpiTransactionPendingUpdateMetadata toJsonString threw: ", e);
                    str3 = null;
                }
                A0a.put("pendingMandateUpdate", str3);
            }
            AnonymousClass1ZR r16 = this.A06;
            if (r16 != null) {
                C117315Zl.A0U(r16, "mandateInfo", A0a);
            }
            String str11 = this.A0E;
            if (str11 != null) {
                A0a.put("frequencyRule", str11);
            }
            String str12 = this.A0J;
            if (str12 != null) {
                A0a.put("recurrenceRule", str12);
            }
            String str13 = this.A0I;
            if (str13 != null) {
                A0a.put("recurrenceDay", str13);
            }
            int i = this.A00;
            if (i > 0) {
                A0a.put("totalRecurringTxnCount", i);
            }
            String str14 = this.A0D;
            if (str14 != null) {
                A0a.put("debitTs", str14);
            }
            String str15 = this.A0H;
            if (str15 != null) {
                A0a.put("previousDebitTs", str15);
            }
            long j4 = this.A04;
            if (j4 > 0) {
                A0a.put("nextPaymentTs", j4);
            }
            long j5 = this.A03;
            if (j5 > 0) {
                A0a.put("nextPaymentEndTs", j5);
            }
            C1309760s r52 = this.A0B;
            if (r52 != null) {
                JSONObject A0a3 = C117295Zj.A0a();
                try {
                    A0a3.put("action", r52.A02);
                    A0a3.put("status", r52.A03);
                    A0a3.put("pauseStartTs", r52.A01);
                    A0a3.put("pauseEndTs", r52.A00);
                    str2 = A0a3.toString();
                } catch (JSONException e2) {
                    Log.w("PAY: IndiaUpiMandateMetadata:PauseResumeStatusDetails toJsonString threw: ", e2);
                    str2 = null;
                }
                A0a.put("pauseResumeStatusDetails", str2);
            }
            C130755zv[] r03 = this.A0M;
            if (r03 != null && r03.length > 0) {
                JSONArray A0L = C117315Zl.A0L();
                C130755zv[] r8 = this.A0M;
                for (C130755zv r3 : r8) {
                    JSONObject A0a4 = C117295Zj.A0a();
                    try {
                        AnonymousClass1ZR r04 = r3.A00;
                        if (r04 == null) {
                            obj = null;
                        } else {
                            obj = r04.A00;
                        }
                        A0a4.put("id", obj);
                        str = C117305Zk.A0l(r3.A01, "status", A0a4);
                    } catch (JSONException e3) {
                        Log.w("PAY: IndiaUpiMandateMetadata:InstanceTransaction toJsonString threw: ", e3);
                        str = null;
                    }
                    A0L.put(str);
                }
                A0a.put("instanceTransactions", A0L);
            }
            return A0a.toString();
        } catch (JSONException e4) {
            Log.w("PAY: IndiaUpiTransactionMetadata toDBString threw: ", e4);
            return null;
        }
    }

    public String toString() {
        String obj;
        AnonymousClass60O r0 = this.A0C;
        String str = "null";
        if (r0 == null) {
            obj = str;
        } else {
            obj = r0.toString();
        }
        C1309760s r02 = this.A0B;
        if (r02 != null) {
            str = r02.toString();
        }
        StringBuilder A0k = C12960it.A0k("[ ");
        C130755zv[] r6 = this.A0M;
        if (r6 != null) {
            for (C130755zv r03 : r6) {
                A0k.append(r03.toString());
            }
        }
        A0k.append(" ]");
        StringBuilder A0k2 = C12960it.A0k("[ mandateNo: ");
        A0k2.append(this.A08);
        A0k2.append(" mandateErrorCode: ");
        A0k2.append(this.A0G);
        A0k2.append(" isMandate : ");
        A0k2.append(this.A0K);
        A0k2.append(" mandateName : ");
        A0k2.append(this.A07);
        A0k2.append(" upiPurposeCode : ");
        A0k2.append(this.A0A);
        A0k2.append(" mandateStartTs: ");
        StringBuilder A0h = C12960it.A0h();
        A0h.append(this.A02);
        C1309060l.A03(A0k2, A0h.toString());
        A0k2.append(" mandateEndTs: ");
        StringBuilder A0h2 = C12960it.A0h();
        A0h2.append(this.A01);
        C1309060l.A03(A0k2, C12960it.A0d("", A0h2));
        A0k2.append(" debitTs: ");
        StringBuilder A0h3 = C12960it.A0h();
        A0h3.append(this.A0D);
        C1309060l.A03(A0k2, C12960it.A0d("", A0h3));
        A0k2.append(" previousDebitTs: ");
        StringBuilder A0h4 = C12960it.A0h();
        A0h4.append(this.A0H);
        C1309060l.A03(A0k2, C12960it.A0d("", A0h4));
        A0k2.append(" nextPaymentTs: ");
        StringBuilder A0h5 = C12960it.A0h();
        A0h5.append(this.A04);
        C1309060l.A03(A0k2, C12960it.A0d("", A0h5));
        A0k2.append(" nextPaymentEndTs: ");
        StringBuilder A0h6 = C12960it.A0h();
        A0h6.append(this.A03);
        C1309060l.A03(A0k2, C12960it.A0d("", A0h6));
        A0k2.append(" totalRecurringTxnCount: ");
        StringBuilder A0h7 = C12960it.A0h();
        A0h7.append(this.A00);
        C1309060l.A03(A0k2, C12960it.A0d("", A0h7));
        A0k2.append(" mandateInfo: ");
        A0k2.append(this.A06);
        A0k2.append(" pendingMandateUpdate: {");
        A0k2.append(obj);
        A0k2.append("}  pauseResumeStatusDetails: {");
        A0k2.append(str);
        A0k2.append("}  instanceTransactions: {");
        A0k2.append(A0k.toString());
        return C12960it.A0d("} ]", A0k2);
    }
}
