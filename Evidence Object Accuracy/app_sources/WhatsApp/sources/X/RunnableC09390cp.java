package X;

import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;

/* renamed from: X.0cp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09390cp implements Runnable {
    public final /* synthetic */ AnonymousClass0PL A00;

    public RunnableC09390cp(AnonymousClass0PL r1) {
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        AnonymousClass0PL r2 = this.A00;
        FrameLayout frameLayout = r2.A00;
        if (frameLayout != null) {
            ViewParent parent = frameLayout.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(r2.A00);
                r2.A00.removeAllViews();
            }
        }
    }
}
