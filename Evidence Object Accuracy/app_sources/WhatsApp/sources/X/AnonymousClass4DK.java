package X;

import android.text.Spannable;

/* renamed from: X.4DK  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4DK {
    public static void A00(Spannable spannable, Object obj, int i, int i2) {
        Object[] spans = spannable.getSpans(i, i2, obj.getClass());
        for (Object obj2 : spans) {
            if (spannable.getSpanStart(obj2) == i && spannable.getSpanEnd(obj2) == i2 && spannable.getSpanFlags(obj2) == 33) {
                spannable.removeSpan(obj2);
            }
        }
        spannable.setSpan(obj, i, i2, 33);
    }
}
