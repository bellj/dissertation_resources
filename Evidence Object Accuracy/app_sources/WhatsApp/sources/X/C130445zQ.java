package X;

import java.util.ArrayList;

/* renamed from: X.5zQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C130445zQ {
    public static final ArrayList A01;
    public final AnonymousClass1V8 A00;

    static {
        String[] strArr = new String[2];
        strArr[0] = "0";
        A01 = C12960it.A0m("1", strArr, 1);
    }

    public C130445zQ(C126205sV r14, AnonymousClass3CT r15, String str, String str2, String str3, String str4, String str5, String str6) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy A0N = C117295Zj.A0N(A0M);
        C41141sy.A01(A0N, "action", "br-add-card");
        if (C117295Zj.A1V(str, 1, false)) {
            C41141sy.A01(A0N, "device-id", str);
        }
        if (AnonymousClass3JT.A0E(str2, 1, 1000, false)) {
            C41141sy.A01(A0N, "nonce", str2);
        }
        if (AnonymousClass3JT.A0E(str3, 2, 2, false)) {
            C41141sy.A01(A0N, "card-expiry-month", str3);
        }
        if (AnonymousClass3JT.A0E(str4, 4, 4, false)) {
            C41141sy.A01(A0N, "card-expiry-year", str4);
        }
        if (C117305Zk.A1X(str5, 1, false)) {
            C41141sy.A01(A0N, "token", str5);
        }
        ArrayList arrayList = A01;
        if (str6 != null) {
            A0N.A0A(str6, "is_first_card", arrayList);
        }
        if (r14 != null) {
            A0N.A05(r14.A00);
        }
        this.A00 = C117295Zj.A0J(A0N, A0M, r15);
    }
}
