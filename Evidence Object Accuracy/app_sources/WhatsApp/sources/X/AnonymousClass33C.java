package X;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import com.whatsapp.R;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONObject;

/* renamed from: X.33C  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass33C extends AnonymousClass45J {
    public float A00;
    public Boolean A01;
    public String A02;
    public String A03;
    public SimpleDateFormat A04;
    public SimpleDateFormat A05;
    public SimpleDateFormat A06;
    public boolean A07;
    public boolean A08;
    public boolean A09;
    public final Context A0A;
    public final Paint A0B;
    public final Paint A0C;
    public final Paint A0D;
    public final Paint A0E;
    public final Paint A0F;
    public final Paint A0G;
    public final Paint A0H;
    public final Paint A0I;
    public final Rect A0J;
    public final AnonymousClass018 A0K;
    public final AbstractC92674Wx A0L;
    public final AnonymousClass3C8 A0M;
    public final AnonymousClass3EU A0N;
    public final boolean A0O;
    public final C91474Rw[] A0P;
    public final C91474Rw[] A0Q;

    @Override // X.AbstractC454821u
    public boolean A0B() {
        return true;
    }

    @Override // X.AbstractC454821u
    public String A0G() {
        return "digital-clock";
    }

    @Override // X.AbstractC454821u
    public boolean A0J() {
        return false;
    }

    @Override // X.AbstractC454821u
    public boolean A0K() {
        return false;
    }

    public AnonymousClass33C(Context context, AnonymousClass018 r3, JSONObject jSONObject) {
        this(context, r3, false);
        this.A01 = Boolean.valueOf(jSONObject.getBoolean("theme"));
        this.A03 = jSONObject.getString("time");
        this.A02 = jSONObject.getString("period");
        A0S();
        super.A0A(jSONObject);
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x00a2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass33C(android.content.Context r11, X.AnonymousClass018 r12, boolean r13) {
        /*
        // Method dump skipped, instructions count: 426
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass33C.<init>(android.content.Context, X.018, boolean):void");
    }

    @Override // X.AbstractC454821u
    public void A04() {
        float f = (AbstractC454821u.A03 * this.A00) / 116.0f;
        RectF rectF = super.A02;
        if (rectF.height() < AbstractC454821u.A03 || rectF.width() < f) {
            float f2 = f / 2.0f;
            rectF.set(rectF.centerX() - f2, rectF.centerY() - (AbstractC454821u.A03 / 2.0f), rectF.centerX() + f2, rectF.centerY() + (AbstractC454821u.A03 / 2.0f));
        }
    }

    @Override // X.AbstractC454821u
    public void A05() {
        this.A0N.A00 = false;
    }

    @Override // X.AbstractC454821u
    public void A06(float f) {
        AbstractC454821u.A00(this, f);
    }

    @Override // X.AbstractC454821u
    public void A08(float f, int i) {
        A07(f, 2);
        this.A0N.A00(f);
    }

    @Override // X.AbstractC454821u
    public boolean A0C() {
        return this.A0L.A01;
    }

    @Override // X.AbstractC454821u
    public boolean A0D() {
        String str = this.A03;
        A0T();
        boolean z = !str.equals(this.A03);
        if (z) {
            A0S();
        }
        return z;
    }

    @Override // X.AbstractC454821u
    public boolean A0E() {
        this.A0M.A00(this.A0L);
        return true;
    }

    @Override // X.AbstractC454821u
    public String A0H(Context context) {
        return context.getString(R.string.doodle_item_digital_clock);
    }

    @Override // X.AbstractC454821u
    public void A0I(Canvas canvas) {
        A0P(canvas);
    }

    @Override // X.AbstractC454821u
    public void A0N(JSONObject jSONObject) {
        super.A0N(jSONObject);
        jSONObject.put("theme", this.A01);
        jSONObject.put("time", this.A03);
        jSONObject.put("period", this.A02);
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x00b6  */
    /* JADX WARNING: Removed duplicated region for block: B:29:? A[RETURN, SYNTHETIC] */
    @Override // X.AbstractC454821u
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0P(android.graphics.Canvas r10) {
        /*
            r9 = this;
            X.4Wx r1 = r9.A0L
            float r5 = r1.A00()
            java.lang.Boolean r0 = r9.A01
            boolean r8 = r0.booleanValue()
            boolean r0 = r1.A01
            if (r0 == 0) goto L_0x0019
            float r1 = r1.A00
            r0 = 0
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 < 0) goto L_0x0019
            r8 = r8 ^ 1
        L_0x0019:
            r10.save()
            android.graphics.RectF r2 = r9.A02
            r2.sort()
            float r0 = r9.A00
            X.C12990iw.A12(r10, r2, r0)
            float r4 = r2.width()
            float r0 = r9.A00
            float r4 = r4 / r0
            float r3 = r2.height()
            r0 = 1122500608(0x42e80000, float:116.0)
            float r3 = r3 / r0
            float r1 = r2.left
            float r0 = r2.top
            r10.scale(r4, r3, r1, r0)
            float r1 = r2.left
            float r0 = r2.top
            r10.translate(r1, r0)
            float r1 = r9.A00
            r0 = 1073741824(0x40000000, float:2.0)
            float r1 = r1 / r0
            r0 = 1114112000(0x42680000, float:58.0)
            r10.scale(r5, r5, r1, r0)
            if (r8 == 0) goto L_0x0064
            X.4Rw[] r7 = r9.A0P
        L_0x0050:
            int r6 = r7.length
            r5 = 0
        L_0x0052:
            if (r5 >= r6) goto L_0x0067
            r0 = r7[r5]
            android.graphics.RectF r4 = r0.A03
            float r3 = r0.A00
            float r1 = r0.A01
            android.graphics.Paint r0 = r0.A02
            r10.drawRoundRect(r4, r3, r1, r0)
            int r5 = r5 + 1
            goto L_0x0052
        L_0x0064:
            X.4Rw[] r7 = r9.A0Q
            goto L_0x0050
        L_0x0067:
            if (r8 == 0) goto L_0x00d0
            android.graphics.Paint r5 = r9.A0E
            android.graphics.Paint r4 = r9.A0C
        L_0x006d:
            java.lang.String r3 = r9.A03
            int r1 = r3.length()
            android.graphics.Rect r7 = r9.A0J
            r0 = 0
            r5.getTextBounds(r3, r0, r1, r7)
            float r3 = r9.A00
            r6 = 1073741824(0x40000000, float:2.0)
            float r3 = r3 / r6
            java.lang.String r0 = r9.A03
            float r1 = r5.measureText(r0)
            java.lang.String r0 = r9.A02
            float r0 = r4.measureText(r0)
            float r1 = r1 + r0
            float r1 = r1 / r6
            float r3 = r3 - r1
            int r0 = r7.height()
            int r0 = r0 >> 1
            int r0 = r0 + 58
            float r1 = (float) r0
            boolean r0 = r9.A07
            if (r0 != 0) goto L_0x00ca
            boolean r0 = r9.A09
            if (r0 == 0) goto L_0x00be
            java.lang.String r0 = r9.A03
            r10.drawText(r0, r3, r1, r5)
            java.lang.String r0 = r9.A03
            float r0 = r5.measureText(r0)
            float r3 = r3 + r0
            java.lang.String r0 = r9.A02
            r10.drawText(r0, r3, r1, r4)
        L_0x00af:
            r10.restore()
            boolean r0 = r9.A0O
            if (r0 != 0) goto L_0x00bd
            X.3EU r1 = r9.A0N
            float r0 = r9.A00
            r1.A01(r10, r2, r0)
        L_0x00bd:
            return
        L_0x00be:
            java.lang.String r0 = r9.A02
            r10.drawText(r0, r3, r1, r4)
            java.lang.String r0 = r9.A02
            float r0 = r4.measureText(r0)
            float r3 = r3 + r0
        L_0x00ca:
            java.lang.String r0 = r9.A03
            r10.drawText(r0, r3, r1, r5)
            goto L_0x00af
        L_0x00d0:
            android.graphics.Paint r5 = r9.A0I
            android.graphics.Paint r4 = r9.A0G
            goto L_0x006d
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass33C.A0P(android.graphics.Canvas):void");
    }

    @Override // X.AnonymousClass45J
    public float A0R() {
        return this.A00 / 116.0f;
    }

    public final void A0S() {
        float f;
        if (this.A08) {
            float measureText = this.A0I.measureText(this.A03);
            if (!this.A07) {
                f = this.A0G.measureText(this.A02);
            } else {
                f = 0.0f;
            }
            float f2 = measureText + f + 100.0f;
            this.A00 = f2;
            C91474Rw[] r0 = this.A0P;
            r0[0] = new C91474Rw(this.A0D, 0.0f, 0.0f, f2, 116.0f, 58.0f, 58.0f);
            r0[1] = new C91474Rw(this.A0B, 2.0f, 2.0f, this.A00 - 2.0f, 114.0f, 58.0f, 58.0f);
            C91474Rw[] r02 = this.A0Q;
            r02[0] = new C91474Rw(this.A0H, 0.0f, 0.0f, this.A00, 116.0f, 29.0f, 29.0f);
            r02[1] = new C91474Rw(this.A0F, 12.0f, 12.0f, this.A00 - 12.0f, 104.0f, 17.0f, 17.0f);
        }
    }

    public final void A0T() {
        String format;
        Date date = new Date();
        boolean z = this.A0K.A04().A00;
        this.A07 = z;
        if (z) {
            this.A03 = this.A06.format(date);
            format = "";
        } else {
            this.A03 = this.A05.format(date);
            format = this.A04.format(date);
        }
        this.A02 = format;
        A0S();
    }
}
