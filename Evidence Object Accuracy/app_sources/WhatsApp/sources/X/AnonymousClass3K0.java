package X;

import android.animation.ValueAnimator;
import android.view.ViewGroup;
import com.whatsapp.calling.videoparticipant.VideoCallParticipantView;
import com.whatsapp.calling.videoparticipant.VideoCallParticipantViewLayout;

/* renamed from: X.3K0  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3K0 implements ValueAnimator.AnimatorUpdateListener {
    public int A00;
    public int A01;
    public final /* synthetic */ int A02;
    public final /* synthetic */ int A03;
    public final /* synthetic */ VideoCallParticipantViewLayout A04;

    public AnonymousClass3K0(VideoCallParticipantViewLayout videoCallParticipantViewLayout, int i, int i2) {
        this.A04 = videoCallParticipantViewLayout;
        this.A02 = i;
        this.A03 = i2;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        int i;
        int i2;
        float animatedFraction = valueAnimator.getAnimatedFraction();
        VideoCallParticipantViewLayout videoCallParticipantViewLayout = this.A04;
        VideoCallParticipantView videoCallParticipantView = videoCallParticipantViewLayout.A0Q;
        ViewGroup.MarginLayoutParams A0H = C12970iu.A0H(videoCallParticipantView);
        AnonymousClass018 r5 = videoCallParticipantViewLayout.A0H;
        AnonymousClass009.A05(r5);
        if (animatedFraction == 0.0f) {
            this.A01 = A0H.topMargin;
            if (C28141Kv.A01(r5)) {
                i2 = A0H.leftMargin;
            } else {
                i2 = A0H.rightMargin;
            }
            this.A00 = i2;
            videoCallParticipantView.getWidth();
            videoCallParticipantView.getHeight();
        }
        int i3 = ((int) (((float) this.A02) * animatedFraction)) + this.A00;
        int i4 = A0H.topMargin;
        if (C28141Kv.A01(r5)) {
            i = A0H.rightMargin;
        } else {
            i = A0H.leftMargin;
        }
        C42941w9.A09(videoCallParticipantView, r5, i3, i4, i, A0H.bottomMargin);
        ViewGroup.MarginLayoutParams A0H2 = C12970iu.A0H(videoCallParticipantView);
        A0H2.topMargin = this.A01 + ((int) (((float) this.A03) * animatedFraction));
        videoCallParticipantView.setLayoutParams(A0H2);
    }
}
