package X;

import android.content.res.Resources;
import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;

/* renamed from: X.3iw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74883iw extends AbstractC018308n {
    public final /* synthetic */ Resources A00;
    public final /* synthetic */ C59932vc A01;

    public C74883iw(Resources resources, C59932vc r2) {
        this.A01 = r2;
        this.A00 = resources;
    }

    @Override // X.AbstractC018308n
    public void A01(Rect rect, View view, C05480Ps r5, RecyclerView recyclerView) {
        rect.set(0, 0, this.A00.getDimensionPixelSize(R.dimen.product_margin_8dp), 0);
    }
}
