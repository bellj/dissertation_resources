package X;

import android.content.ContentValues;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.net.Uri;
import android.util.Base64;
import android.util.Pair;
import com.whatsapp.util.Log;
import java.io.File;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.17s  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC250017s {
    public final C233511i A00;

    public AbstractC250017s(C233511i r1) {
        this.A00 = r1;
    }

    public String A00() {
        if ((this instanceof AnonymousClass1H7) || (this instanceof AnonymousClass1HA)) {
            return "regular_low";
        }
        if (this instanceof C27341Gy) {
            return "regular_high";
        }
        if (this instanceof AnonymousClass1H6) {
            return null;
        }
        if (this instanceof AnonymousClass1H3) {
            return "critical_block";
        }
        if (this instanceof C249917r) {
            return "regular";
        }
        if (this instanceof C27321Gw) {
            return "regular_low";
        }
        if (this instanceof C27311Gv) {
            return "regular_high";
        }
        if (this instanceof AnonymousClass1H2) {
            return "regular_low";
        }
        if (this instanceof C27351Gz) {
            return "critical_block";
        }
        if (this instanceof AnonymousClass1H8) {
            return "regular_low";
        }
        if ((this instanceof AnonymousClass1H4) || (this instanceof AnonymousClass1H0)) {
            return "regular_high";
        }
        if (!(this instanceof AnonymousClass1H1)) {
            return !(this instanceof AnonymousClass1H5) ? "regular_low" : "regular_high";
        }
        return "critical_unblock_low";
    }

    public List A01(boolean z) {
        C16310on A01;
        ArrayList arrayList;
        boolean z2;
        List<C462024y> subList;
        SecureRandom secureRandom;
        if (this instanceof AnonymousClass1H7) {
            AnonymousClass1H7 r0 = (AnonymousClass1H7) this;
            SharedPreferences sharedPreferences = r0.A06.A00;
            if (sharedPreferences.getBoolean("archive_v2_enabled", false)) {
                r0.A00.A08();
                Log.i("unarchive-chats-setting-handler/createBootstrapMutations");
                if (z) {
                    return r0.A08();
                }
                return Collections.singletonList(new C34481gD(null, null, r0.A05.A00(), sharedPreferences.getBoolean("notify_new_message_for_archived_chats", false)));
            }
            Log.i("unarchive-chats-setting-handler/createBootstrapMutations/empty");
            return Collections.emptyList();
        } else if (this instanceof AnonymousClass1HA) {
            AnonymousClass1HA r02 = (AnonymousClass1HA) this;
            r02.A00.A08();
            AnonymousClass009.A0F(true);
            return Collections.singletonList(new C34551gK(null, null, r02.A01.A00(), r02.A02.A04().A00));
        } else if (!(this instanceof C27341Gy)) {
            if (!(this instanceof AnonymousClass1H6)) {
                if (this instanceof AnonymousClass1H3) {
                    AnonymousClass1H3 r03 = (AnonymousClass1H3) this;
                    return Collections.singletonList(new C34571gM(null, null, r03.A00.A05(), r03.A01.A00()));
                } else if (!(this instanceof C249917r)) {
                    if (this instanceof C27321Gw) {
                        C27321Gw r04 = (C27321Gw) this;
                        ArrayList arrayList2 = new ArrayList();
                        for (Map.Entry entry : r04.A07.A0C().entrySet()) {
                            arrayList2.add(new C34651gU((AbstractC14640lm) entry.getKey(), ((Number) entry.getValue()).longValue(), true));
                        }
                        if (z) {
                            r04.A08(arrayList2);
                        }
                        arrayList = new ArrayList();
                        Iterator it = arrayList2.iterator();
                        while (it.hasNext()) {
                            C34651gU r3 = (C34651gU) it.next();
                            r04.A01.A06(1);
                            AbstractC14640lm r8 = r3.A00;
                            arrayList.add(new C34631gS(r04.A00.A04(r8, false), r8, r04.A03.A00(), false));
                            arrayList.add(r3);
                        }
                    } else if (this instanceof C27311Gv) {
                        C27311Gv r05 = (C27311Gv) this;
                        arrayList = new ArrayList();
                        C15860o1 r2 = r05.A02;
                        HashSet hashSet = new HashSet();
                        A01 = r2.A03().get();
                        try {
                            Cursor A09 = A01.A03.A09("SELECT jid, mute_end FROM settings WHERE mute_end IS NOT NULL", null);
                            int columnIndex = A09.getColumnIndex("jid");
                            int columnIndex2 = A09.getColumnIndex("mute_end");
                            while (A09.moveToNext()) {
                                AbstractC14640lm A012 = AbstractC14640lm.A01(A09.getString(columnIndex));
                                if (A012 != null) {
                                    hashSet.add(new Pair(A012, Long.valueOf(A09.getLong(columnIndex2))));
                                }
                            }
                            A09.close();
                            A01.close();
                            Iterator it2 = hashSet.iterator();
                            while (it2.hasNext()) {
                                Pair pair = (Pair) it2.next();
                                AbstractC14640lm r9 = (AbstractC14640lm) pair.first;
                                long longValue = ((Number) pair.second).longValue();
                                C14830m7 r5 = r05.A00;
                                if (longValue > System.currentTimeMillis() || longValue == -1) {
                                    arrayList.add(new C34611gQ(null, r9, null, longValue, r5.A00(), true, false));
                                }
                            }
                        } finally {
                        }
                    } else if (this instanceof AnonymousClass1H2) {
                        AnonymousClass1H2 r06 = (AnonymousClass1H2) this;
                        ArrayList arrayList3 = new ArrayList();
                        C19990v2 r7 = r06.A04;
                        ArrayList arrayList4 = new ArrayList();
                        ConcurrentHashMap A0B = r7.A0B();
                        synchronized (r7) {
                            z2 = r7.A00;
                        }
                        if (z2) {
                            for (Map.Entry entry2 : A0B.entrySet()) {
                                AbstractC14640lm r4 = (AbstractC14640lm) entry2.getKey();
                                if (((AnonymousClass1PE) entry2.getValue()).A06 == -1 && !r7.A0F(r4)) {
                                    arrayList4.add(r4);
                                }
                            }
                            Iterator it3 = arrayList4.iterator();
                            while (it3.hasNext()) {
                                AbstractC14640lm r72 = (AbstractC14640lm) it3.next();
                                arrayList3.add(new C34521gH(null, r06.A00.A04(r72, false), r72, null, r06.A02.A00(), false, false));
                            }
                            return arrayList3;
                        }
                        throw new IllegalStateException("ChatsCache/getMarkedAsUnreadChats: chat haven't initialized");
                    } else if (this instanceof C27351Gz) {
                        return Collections.singletonList(new AnonymousClass1JP(null, null, AbstractC27291Gt.A05(Locale.getDefault()), ((C27351Gz) this).A01.A00()));
                    } else {
                        if (this instanceof AnonymousClass1H8) {
                            Log.i("favorite-sticker-handler/createBootstrapMutations");
                            C249817q r52 = ((AnonymousClass1H8) this).A00;
                            AnonymousClass15F r6 = r52.A01;
                            ArrayList arrayList5 = new ArrayList();
                            try {
                                C16310on A013 = r6.A01.get();
                                Cursor A092 = A013.A03.A09("SELECT starred_stickers.plaintext_hash as plaintext_hash, timestamp, hash_of_image_part, last_uploaded_time, sticker_md_upload.direct_path as direct_path, sticker_md_upload.media_key as media_key, sticker_md_upload.handle as handle From starred_stickers LEFT JOIN sticker_md_upload ON starred_stickers.plaintext_hash = sticker_md_upload.plaintext_hash ORDER BY timestamp DESC LIMIT ? ", new String[]{String.valueOf(150)});
                                int columnIndexOrThrow = A092.getColumnIndexOrThrow("plaintext_hash");
                                int columnIndexOrThrow2 = A092.getColumnIndexOrThrow("hash_of_image_part");
                                int columnIndexOrThrow3 = A092.getColumnIndexOrThrow("timestamp");
                                int columnIndexOrThrow4 = A092.getColumnIndexOrThrow("direct_path");
                                int columnIndexOrThrow5 = A092.getColumnIndexOrThrow("media_key");
                                int columnIndexOrThrow6 = A092.getColumnIndexOrThrow("last_uploaded_time");
                                while (A092.moveToNext()) {
                                    String string = A092.getString(columnIndexOrThrow);
                                    A092.getString(columnIndexOrThrow4);
                                    if (string != null) {
                                        arrayList5.add(new C462024y(string, A092.getString(columnIndexOrThrow2), A092.getString(columnIndexOrThrow4), A092.getString(columnIndexOrThrow5), A092.getLong(columnIndexOrThrow3), A092.getLong(columnIndexOrThrow6)));
                                    }
                                }
                                A092.close();
                                A013.close();
                            } catch (SQLiteDatabaseCorruptException e) {
                                Log.e("StarredStickerDBTableHelper.getStarredStickersWithUploadInfo", e);
                                r6.A00.AaV("StarredStickerDBTableHelper.getStarredStickersWithUploadInfo", e.getMessage(), true);
                            }
                            if (arrayList5.size() <= 30) {
                                subList = new ArrayList();
                            } else {
                                subList = arrayList5.subList(30, Math.min(arrayList5.size(), 150));
                            }
                            List<C37421mM> A00 = r6.A00(30, 0);
                            A00.size();
                            for (C37421mM r1 : A00) {
                                C002701f r07 = r52.A00;
                                String str = r1.A0A;
                                File A04 = r07.A04(str);
                                if (A04.exists()) {
                                    C462024y r23 = new C462024y(str, r1.A01, null, null, r1.A07, 0);
                                    C249717p r42 = r52.A03;
                                    C462124z r62 = new C462124z(r52.A02);
                                    byte[] bArr = new byte[32];
                                    synchronized (r42) {
                                        secureRandom = r42.A00;
                                        if (secureRandom == null) {
                                            secureRandom = new SecureRandom();
                                            r42.A00 = secureRandom;
                                        }
                                    }
                                    secureRandom.nextBytes(bArr);
                                    AnonymousClass1K9 A002 = AnonymousClass1K9.A00(Uri.fromFile(A04), new C14390lM(bArr, r42.A01.A00()), null, new C14480lV(true, false, true), C14370lK.A0S, null, null, 0, false, false, true, false);
                                    C14300lD r73 = r42.A03;
                                    AnonymousClass1KC A042 = r73.A04(A002, false);
                                    A042.A0U = "mms";
                                    A042.A03(new AbstractC14590lg(r23, A042, r62, r42, bArr) { // from class: X.250
                                        public final /* synthetic */ C462024y A00;
                                        public final /* synthetic */ AnonymousClass1KC A01;
                                        public final /* synthetic */ C462124z A02;
                                        public final /* synthetic */ C249717p A03;
                                        public final /* synthetic */ byte[] A04;

                                        {
                                            this.A03 = r4;
                                            this.A01 = r2;
                                            this.A02 = r3;
                                            this.A00 = r1;
                                            this.A04 = r5;
                                        }

                                        @Override // X.AbstractC14590lg
                                        public final void accept(Object obj) {
                                            Set emptySet;
                                            C249717p r22 = this.A03;
                                            AnonymousClass1KC r12 = this.A01;
                                            C462124z r63 = this.A02;
                                            C462024y r32 = this.A00;
                                            byte[] bArr2 = this.A04;
                                            C39721qR r08 = (C39721qR) r12.A0H.A00();
                                            r12.A02();
                                            int intValue = ((Number) obj).intValue();
                                            if (intValue == 0 && r08 != null) {
                                                C29171Rd r13 = r08.A02;
                                                if (r13.A03() != null) {
                                                    String A03 = r13.A03();
                                                    String encodeToString = Base64.encodeToString(bArr2, 2);
                                                    long A003 = r22.A01.A00();
                                                    r32.A02 = A03;
                                                    r32.A04 = encodeToString;
                                                    r32.A01 = A003;
                                                    for (AnonymousClass1GH r09 : r63.A00.A01()) {
                                                        C18850tA r43 = r09.A00;
                                                        Log.i("sync-manager/onFavoriteStickerInfoChanged");
                                                        if (r43.A0M.A03("favoriteSticker") != null) {
                                                            C15570nT r010 = r43.A04;
                                                            r010.A08();
                                                            if (r010.A05 != null && r43.A0Q()) {
                                                                Log.i("favorite-sticker-handler/onFavoriteStickerChanged");
                                                                emptySet = r43.A0A(Collections.singletonList(new AnonymousClass251(r32)));
                                                                r43.A0O(emptySet);
                                                            }
                                                        }
                                                        Log.i("sync-manager/onFavoriteStickerInfoChanged/emptySet");
                                                        emptySet = Collections.emptySet();
                                                        r43.A0O(emptySet);
                                                    }
                                                    C249617o r24 = r22.A02;
                                                    ContentValues contentValues = new ContentValues();
                                                    contentValues.put("plaintext_hash", r32.A02);
                                                    contentValues.put("last_uploaded_time", Long.valueOf(r32.A01));
                                                    contentValues.put("media_key", r32.A04);
                                                    contentValues.put("direct_path", r32.A02);
                                                    contentValues.put("is_favorite", Boolean.valueOf(r32.A06));
                                                    try {
                                                        C16310on A02 = r24.A00.A02();
                                                        A02.A03.A06(contentValues, "sticker_md_upload", 5);
                                                        A02.close();
                                                        return;
                                                    } catch (SQLiteDatabaseCorruptException e2) {
                                                        Log.e("StickerMDUploadDBStorage/addUploadInfo", e2);
                                                        return;
                                                    }
                                                }
                                            }
                                            StringBuilder sb = new StringBuilder("StickerMDUploadStore/uploadSticker/failed to upload, error:");
                                            sb.append(AnonymousClass4EP.A00(intValue));
                                            Log.e(sb.toString());
                                        }
                                    }, null);
                                    r73.A0D(A042, null);
                                }
                            }
                            A00.size();
                            arrayList = new ArrayList(subList.size());
                            for (C462024y r22 : subList) {
                                arrayList.add(new AnonymousClass251(r22));
                            }
                        } else if (this instanceof AnonymousClass1H4) {
                            return new ArrayList();
                        } else {
                            if (!(this instanceof AnonymousClass1H0)) {
                                if (this instanceof AnonymousClass1H1) {
                                    AnonymousClass009.A07("Please use createBootstrapMutations(initialData) method instead");
                                    return ((AnonymousClass1H1) this).A09(Collections.emptyList());
                                } else if (!(this instanceof AnonymousClass1H5)) {
                                    if (!(this instanceof C27331Gx)) {
                                        return Collections.singletonList(new C34441g9(null, null, ((AnonymousClass1H9) this).A00.A00(), false));
                                    }
                                    C27331Gx r08 = (C27331Gx) this;
                                    arrayList = new ArrayList();
                                    for (AbstractC14640lm r74 : r08.A07.A05()) {
                                        arrayList.add(new C34631gS(r08.A00.A04(r74, false), r74, r08.A03.A00(), true));
                                    }
                                }
                            }
                        }
                    }
                    return arrayList;
                } else {
                    C249917r r09 = (C249917r) this;
                    C14850m9 r24 = r09.A03;
                    boolean A07 = r24.A07(808);
                    boolean A072 = r24.A07(1312);
                    ArrayList arrayList6 = new ArrayList();
                    if (A07) {
                        arrayList6.add("contact_except");
                    }
                    if (A072) {
                        arrayList6.add("ddm_settings");
                    }
                    arrayList6.add("reactions_send");
                    return Collections.singletonList(new C34691gY(null, null, arrayList6, r09.A02.A00()));
                }
            }
            return Collections.emptyList();
        } else {
            C27341Gy r010 = (C27341Gy) this;
            A01 = r010.A05.get();
            try {
                Cursor AEK = r010.A06.AEK(null, null);
                List A08 = r010.A08(AEK, A01, true);
                AEK.close();
                return A08;
            } finally {
                try {
                    A01.close();
                } catch (Throwable unused) {
                }
            }
        }
    }

    public void A02(AnonymousClass1JQ r5) {
        if (!(this instanceof AnonymousClass1H7)) {
            if (this instanceof AnonymousClass1HA) {
                AnonymousClass009.A07("Android shouldn't process TimeFormatMutation with dependencies missing");
            } else if (this instanceof C27341Gy) {
                C27341Gy r2 = (C27341Gy) this;
                C34591gO r52 = (C34591gO) r5;
                C15650ng r0 = r2.A04;
                AbstractC15340mz A03 = r0.A0K.A03(r52.A01);
                if (A03 != null) {
                    r2.A09(r52, A03);
                    r2.A03(r52);
                    return;
                }
                return;
            } else if (this instanceof AnonymousClass1H6) {
                ((AnonymousClass1H6) this).A08((C34461gB) r5, null);
                return;
            } else if (this instanceof AnonymousClass1H3) {
                return;
            } else {
                if (!(this instanceof C249917r)) {
                    if (this instanceof C27321Gw) {
                        C27321Gw r3 = (C27321Gw) this;
                        r3.A08(r3.A06.A09("pin_v1", true));
                        r3.A03(r5);
                        return;
                    } else if (this instanceof C27311Gv) {
                        C27311Gv r22 = (C27311Gv) this;
                        C34611gQ r53 = (C34611gQ) r5;
                        if (r22.A01.A0D(r53.A01)) {
                            r22.A08(r53);
                            r22.A03(r53);
                            return;
                        }
                        return;
                    } else if (this instanceof AnonymousClass1H2) {
                        ((AnonymousClass1H2) this).A08((C34521gH) r5);
                        return;
                    } else if (this instanceof C27351Gz) {
                        return;
                    } else {
                        if (this instanceof AnonymousClass1H8) {
                            Log.i("favorite-sticker-handler/handleMutationWithDependenciesMissing");
                            A05(r5);
                            return;
                        } else if (this instanceof AnonymousClass1H4) {
                            AnonymousClass1H4 r32 = (AnonymousClass1H4) this;
                            C34411g6 r54 = (C34411g6) r5;
                            C15650ng r23 = r32.A01;
                            AbstractC15340mz A032 = r23.A0K.A03(r54.A02);
                            if (A032 != null) {
                                r23.A0h(Collections.singleton(A032), (r54.A03 ? 1 : 0) | 2);
                                r32.A03(r54);
                                return;
                            }
                            return;
                        } else if (!(this instanceof AnonymousClass1H0) && !(this instanceof AnonymousClass1H1) && !(this instanceof AnonymousClass1H5)) {
                            if (!(this instanceof C27331Gx)) {
                                A07(r5, null);
                                return;
                            } else {
                                ((C27331Gx) this).A09((C34631gS) r5);
                                return;
                            }
                        } else {
                            return;
                        }
                    }
                }
            }
        }
        A03(r5);
    }

    public final void A03(AnonymousClass1JQ r8) {
        String str;
        C16310on A02 = this.A00.A02.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            C16330op r5 = A02.A03;
            String[] strArr = new String[3];
            strArr[0] = r8.A03();
            if (r8 instanceof AbstractC34381g3) {
                str = ((AbstractC34381g3) r8).ABH().getRawString();
            } else {
                str = null;
            }
            strArr[1] = str;
            strArr[2] = AnonymousClass1JQ.A00(r8.A06());
            r5.A0C("UPDATE syncd_mutations SET are_dependencies_missing = 0, mutation_name = ?, chat_jid = ?  WHERE mutation_index = ? ", strArr);
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final void A04(AnonymousClass1JQ r3) {
        this.A00.A0H(Collections.singleton(r3.A07));
    }

    public final void A05(AnonymousClass1JQ r3) {
        r3.A04(false);
        this.A00.A0G(Collections.singleton(r3));
    }

    public final void A06(AnonymousClass1JQ r3) {
        r3.A04(true);
        this.A00.A0G(Collections.singleton(r3));
    }

    public final void A07(AnonymousClass1JQ r3, AnonymousClass1JQ r4) {
        if (r4 != null) {
            this.A00.A0E(r4);
        }
        this.A00.A0G(Collections.singleton(r3));
    }
}
