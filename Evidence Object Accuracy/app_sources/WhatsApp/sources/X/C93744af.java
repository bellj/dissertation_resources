package X;

import com.whatsapp.protocol.VoipStanzaChildNode;

/* renamed from: X.4af  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C93744af {
    public static final C93744af A07 = new C93744af(null, null, null, null, null, (byte) 0, 1);
    public final byte A00;
    public final int A01;
    public final AbstractC455722e A02;
    public final VoipStanzaChildNode A03;
    public final String A04;
    public final byte[] A05;
    public final byte[] A06;

    public C93744af(AbstractC455722e r1, VoipStanzaChildNode voipStanzaChildNode, String str, byte[] bArr, byte[] bArr2, byte b, int i) {
        this.A01 = i;
        this.A03 = voipStanzaChildNode;
        this.A00 = b;
        this.A06 = bArr;
        this.A05 = bArr2;
        this.A04 = str;
        this.A02 = r1;
    }
}
