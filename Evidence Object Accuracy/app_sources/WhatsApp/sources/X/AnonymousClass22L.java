package X;

import java.util.Collection;

/* renamed from: X.22L  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass22L extends AbstractC34011fR {
    public final int A00;
    public final AbstractC14640lm A01;
    public final C22470z8 A02;
    public final Collection A03;
    public final boolean A04;

    public AnonymousClass22L(AbstractC14640lm r1, C22470z8 r2, Collection collection, int i, boolean z) {
        this.A02 = r2;
        this.A01 = r1;
        this.A04 = z;
        this.A03 = collection;
        this.A00 = i;
    }
}
