package X;

import android.widget.BaseAdapter;
import com.whatsapp.calling.callhistory.CallLogActivity;
import java.util.List;

/* renamed from: X.2bi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52802bi extends BaseAdapter {
    public List A00 = C12960it.A0l();
    public final /* synthetic */ CallLogActivity A01;

    @Override // android.widget.Adapter
    public long getItemId(int i) {
        return (long) i;
    }

    @Override // android.widget.BaseAdapter, android.widget.Adapter
    public int getViewTypeCount() {
        return 1;
    }

    public /* synthetic */ C52802bi(CallLogActivity callLogActivity) {
        this.A01 = callLogActivity;
    }

    @Override // android.widget.Adapter
    public int getCount() {
        return C12970iu.A09(this.A00);
    }

    @Override // android.widget.Adapter
    public /* bridge */ /* synthetic */ Object getItem(int i) {
        return this.A00.get(i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0113, code lost:
        if (r2 == 6) goto L_0x0115;
     */
    @Override // android.widget.Adapter
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View getView(int r16, android.view.View r17, android.view.ViewGroup r18) {
        /*
        // Method dump skipped, instructions count: 290
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C52802bi.getView(int, android.view.View, android.view.ViewGroup):android.view.View");
    }
}
