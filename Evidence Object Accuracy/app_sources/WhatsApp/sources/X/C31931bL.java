package X;

/* renamed from: X.1bL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C31931bL {
    public final String A00;
    public final String A01;

    public C31931bL(String str, String str2) {
        this.A00 = str;
        this.A01 = str2;
    }

    public String A00() {
        StringBuilder sb;
        String str = this.A00;
        String str2 = this.A01;
        if (str.compareTo(str2) <= 0) {
            sb = new StringBuilder();
            sb.append(str);
            sb.append(str2);
        } else {
            sb = new StringBuilder();
            sb.append(str2);
            sb.append(str);
        }
        return sb.toString();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            C31931bL r4 = (C31931bL) obj;
            if (this.A00.equals(r4.A00)) {
                return this.A01.equals(r4.A01);
            }
        }
        return false;
    }

    public int hashCode() {
        return (this.A00.hashCode() * 31) + this.A01.hashCode();
    }
}
