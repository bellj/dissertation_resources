package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2nB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57512nB extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57512nB A06;
    public static volatile AnonymousClass255 A07;
    public int A00;
    public int A01;
    public C43261wh A02;
    public C57032mM A03;
    public String A04 = "";
    public String A05 = "";

    static {
        C57512nB r0 = new C57512nB();
        A06 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r7, Object obj, Object obj2) {
        C81603uH r1;
        C82163vB r12;
        EnumC87264Av r0;
        switch (r7.ordinal()) {
            case 0:
                return A06;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                C57512nB r9 = (C57512nB) obj2;
                int i = this.A00;
                boolean A1R = C12960it.A1R(i);
                String str = this.A05;
                int i2 = r9.A00;
                this.A05 = r8.Afy(str, r9.A05, A1R, C12960it.A1R(i2));
                this.A01 = r8.Afp(this.A01, r9.A01, C12960it.A1V(i & 2, 2), C12960it.A1V(i2 & 2, 2));
                this.A03 = (C57032mM) r8.Aft(this.A03, r9.A03);
                this.A02 = (C43261wh) r8.Aft(this.A02, r9.A02);
                int i3 = this.A00;
                boolean A1V = C12960it.A1V(i3 & 16, 16);
                String str2 = this.A04;
                int i4 = r9.A00;
                this.A04 = r8.Afy(str2, r9.A04, A1V, C12960it.A1V(i4 & 16, 16));
                if (r8 == C463025i.A00) {
                    this.A00 = i3 | i4;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                AnonymousClass254 r92 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        try {
                            int A03 = r82.A03();
                            if (A03 == 0) {
                                break;
                            } else if (A03 == 10) {
                                String A0A = r82.A0A();
                                this.A00 = 1 | this.A00;
                                this.A05 = A0A;
                            } else if (A03 == 16) {
                                int A02 = r82.A02();
                                if (A02 == 0) {
                                    r0 = EnumC87264Av.A02;
                                } else if (A02 != 1) {
                                    r0 = null;
                                } else {
                                    r0 = EnumC87264Av.A01;
                                }
                                if (r0 == null) {
                                    super.A0X(2, A02);
                                } else {
                                    this.A00 |= 2;
                                    this.A01 = A02;
                                }
                            } else if (A03 == 26) {
                                if ((this.A00 & 4) == 4) {
                                    r12 = (C82163vB) this.A03.A0T();
                                } else {
                                    r12 = null;
                                }
                                C57032mM r02 = (C57032mM) AbstractC27091Fz.A0H(r82, r92, C57032mM.A02);
                                this.A03 = r02;
                                if (r12 != null) {
                                    this.A03 = (C57032mM) AbstractC27091Fz.A0C(r12, r02);
                                }
                                this.A00 |= 4;
                            } else if (A03 == 34) {
                                if ((this.A00 & 8) == 8) {
                                    r1 = (C81603uH) this.A02.A0T();
                                } else {
                                    r1 = null;
                                }
                                C43261wh r03 = (C43261wh) AbstractC27091Fz.A0H(r82, r92, C43261wh.A0O);
                                this.A02 = r03;
                                if (r1 != null) {
                                    this.A02 = (C43261wh) AbstractC27091Fz.A0C(r1, r03);
                                }
                                this.A00 |= 8;
                            } else if (A03 == 42) {
                                String A0A2 = r82.A0A();
                                this.A00 |= 16;
                                this.A04 = A0A2;
                            } else if (!A0a(r82, A03)) {
                                break;
                            }
                        } catch (IOException e) {
                            throw AbstractC27091Fz.A0K(this, e);
                        }
                    } catch (C28971Pt e2) {
                        throw AbstractC27091Fz.A0J(e2, this);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C57512nB();
            case 5:
                return new C82153vA();
            case 6:
                break;
            case 7:
                if (A07 == null) {
                    synchronized (C57512nB.class) {
                        if (A07 == null) {
                            A07 = AbstractC27091Fz.A09(A06);
                        }
                    }
                }
                return A07;
            default:
                throw C12970iu.A0z();
        }
        return A06;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            i2 = AbstractC27091Fz.A04(1, this.A05, 0);
        }
        int i3 = this.A00;
        if ((i3 & 2) == 2) {
            i2 = AbstractC27091Fz.A03(2, this.A01, i2);
        }
        if ((i3 & 4) == 4) {
            C57032mM r0 = this.A03;
            if (r0 == null) {
                r0 = C57032mM.A02;
            }
            i2 = AbstractC27091Fz.A08(r0, 3, i2);
        }
        if ((this.A00 & 8) == 8) {
            C43261wh r02 = this.A02;
            if (r02 == null) {
                r02 = C43261wh.A0O;
            }
            i2 = AbstractC27091Fz.A08(r02, 4, i2);
        }
        if ((this.A00 & 16) == 16) {
            i2 = AbstractC27091Fz.A04(5, this.A04, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A05);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0E(2, this.A01);
        }
        if ((this.A00 & 4) == 4) {
            C57032mM r0 = this.A03;
            if (r0 == null) {
                r0 = C57032mM.A02;
            }
            codedOutputStream.A0L(r0, 3);
        }
        if ((this.A00 & 8) == 8) {
            C43261wh r02 = this.A02;
            if (r02 == null) {
                r02 = C43261wh.A0O;
            }
            codedOutputStream.A0L(r02, 4);
        }
        if ((this.A00 & 16) == 16) {
            codedOutputStream.A0I(5, this.A04);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
