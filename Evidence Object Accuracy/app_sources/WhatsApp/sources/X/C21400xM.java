package X;

import android.content.ContentValues;
import android.database.Cursor;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0xM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21400xM {
    public static final List A0L = Arrays.asList((byte) 56, (byte) 68);
    public static final List A0M = Arrays.asList((byte) 56, (byte) 68, (byte) 67);
    public final int A00;
    public final C22490zA A01;
    public final C21370xJ A02;
    public final C16970q3 A03;
    public final C14830m7 A04;
    public final C14820m6 A05;
    public final C16370ot A06;
    public final C16510p9 A07;
    public final C19990v2 A08;
    public final C15650ng A09;
    public final C21380xK A0A;
    public final C18460sU A0B;
    public final C241914o A0C;
    public final C16490p7 A0D;
    public final AnonymousClass1EE A0E;
    public final AnonymousClass1EJ A0F;
    public final AnonymousClass1EI A0G;
    public final AnonymousClass1EG A0H;
    public final AnonymousClass1ED A0I;
    public final C14850m9 A0J;
    public final C22170ye A0K;

    public C21400xM(C22490zA r3, C21370xJ r4, C16970q3 r5, C14830m7 r6, C14820m6 r7, C16370ot r8, C16510p9 r9, C19990v2 r10, C15650ng r11, C21380xK r12, C18460sU r13, C241914o r14, C16490p7 r15, AnonymousClass1EE r16, AnonymousClass1EJ r17, AnonymousClass1EI r18, AnonymousClass1EG r19, AnonymousClass1ED r20, C14850m9 r21, C22170ye r22) {
        this.A04 = r6;
        this.A0J = r21;
        this.A0B = r13;
        this.A07 = r9;
        this.A08 = r10;
        this.A03 = r5;
        this.A02 = r4;
        this.A0K = r22;
        this.A0A = r12;
        this.A09 = r11;
        this.A0I = r20;
        this.A0C = r14;
        this.A06 = r8;
        this.A05 = r7;
        this.A0D = r15;
        this.A0F = r17;
        this.A0G = r18;
        this.A0H = r19;
        this.A0E = r16;
        this.A01 = r3;
        this.A00 = r21.A02(987);
    }

    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    public final int A00(X.AnonymousClass1Iv r34, boolean r35) {
        /*
        // Method dump skipped, instructions count: 2178
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C21400xM.A00(X.1Iv, boolean):int");
    }

    public final long A01(Set set) {
        long j = -1;
        if (!set.isEmpty()) {
            HashSet hashSet = new HashSet();
            Iterator it = set.iterator();
            while (it.hasNext()) {
                AbstractC15340mz r5 = (AbstractC15340mz) it.next();
                if (j < r5.A11) {
                    j = r5.A11;
                }
                hashSet.add(Long.valueOf(r5.A11));
            }
            if (!hashSet.isEmpty()) {
                AnonymousClass1ED r9 = this.A0I;
                if (!hashSet.isEmpty()) {
                    String[] strArr = new String[hashSet.size()];
                    Iterator it2 = hashSet.iterator();
                    int i = 0;
                    while (it2.hasNext()) {
                        strArr[i] = Long.toString(((Number) it2.next()).longValue());
                        i++;
                    }
                    C29841Uw r2 = new C29841Uw(strArr, 975);
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("status", (Integer) 17);
                    C16310on A02 = r9.A02.A02();
                    try {
                        AnonymousClass1Lx A00 = A02.A00();
                        Iterator it3 = r2.iterator();
                        while (it3.hasNext()) {
                            String[] strArr2 = (String[]) it3.next();
                            StringBuilder sb = new StringBuilder();
                            sb.append("_id IN ");
                            sb.append(AnonymousClass1Ux.A00(strArr2.length));
                            A02.A03.A00("message_add_on", contentValues, sb.toString(), strArr2);
                        }
                        A00.A00();
                        A00.close();
                        A02.close();
                    } catch (Throwable th) {
                        try {
                            A02.close();
                        } catch (Throwable unused) {
                        }
                        throw th;
                    }
                }
            }
            hashSet.size();
            A07(set);
        }
        return j;
    }

    public AnonymousClass1Iv A02(AnonymousClass1IS r9) {
        if (r9 == null) {
            return null;
        }
        C16310on A01 = this.A0D.get();
        try {
            AnonymousClass1ED r5 = this.A0I;
            C16510p9 r1 = r5.A00;
            AbstractC14640lm r0 = r9.A00;
            AnonymousClass009.A05(r0);
            Cursor A09 = A01.A03.A09(C40471re.A03, new String[]{String.valueOf(r1.A02(r0)), r9.A01, String.valueOf(r9.A02 ? 1 : 0)});
            int columnIndexOrThrow = A09.getColumnIndexOrThrow("_id");
            int columnIndexOrThrow2 = A09.getColumnIndexOrThrow("message_add_on_type");
            if (!A09.moveToNext()) {
                Log.w("MessageAddOnManager/getMessageAddOnForMessageKey message add on not found");
            } else {
                long j = A09.getLong(columnIndexOrThrow);
                byte b = (byte) A09.getInt(columnIndexOrThrow2);
                A09.close();
                A09 = AnonymousClass1ED.A00(A01, b, j);
                HashMap A00 = AnonymousClass1sB.A00(A09, b);
                if (A09.moveToNext()) {
                    AnonymousClass1Iv A04 = r5.A04(A09, A00);
                    if (A04 == null) {
                        Log.e("MessageAddOnManager/getMessageAddOnForMessageKey failed to read fmessage");
                    } else {
                        A04.A15(A09, this.A0B, A00);
                        if (A04 instanceof C27711Iw) {
                            C27711Iw r3 = (C27711Iw) A04;
                            List A002 = this.A0G.A06.A00(r3.A11);
                            List list = r3.A05;
                            list.clear();
                            list.addAll(A002);
                        }
                        A09.close();
                        A01.close();
                        return A04;
                    }
                }
            }
            A09.close();
            A01.close();
            return null;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public AnonymousClass1Iv A03(AnonymousClass1IS r9) {
        AnonymousClass1Iv A02 = A02(r9);
        if (A02 instanceof AnonymousClass1X9) {
            AbstractC15340mz A00 = this.A06.A00(A02.A00);
            if (A00 != null) {
                A02.A02 = new C40711sC(A00.A0B(), A00.A0z);
                A02.A01 = C40721sD.A01(A00);
                return A02;
            }
            Log.i("MessageAddOn/getMessageAddOnForMessageKeyForSend/missing parent message");
            return null;
        }
        if (A02 instanceof C27711Iw) {
            C27711Iw r4 = (C27711Iw) A02;
            AbstractC15340mz A002 = this.A06.A00(((AnonymousClass1Iv) r4).A00);
            if (A002 instanceof C27671Iq) {
                ((AnonymousClass1Iv) r4).A02 = new C40711sC(A002.A0B(), A002.A0z);
                AnonymousClass1EI.A00((C27671Iq) A002, r4);
                return A02;
            }
            Log.i("MessageAddOn/getMessageAddOnForMessageKeyForSend/missing parent message");
            return null;
        }
        return A02;
    }

    public final AnonymousClass1YY A04(C16310on r9, int i, long j) {
        AnonymousClass1ED r2 = this.A0I;
        Cursor A00 = AnonymousClass1ED.A00(r9, (byte) 56, j);
        try {
            HashMap A002 = AnonymousClass1sB.A00(A00, (byte) 56);
            if (!A00.moveToNext()) {
                Log.e("MessageAddOnManager/createMessageAddOnReactionPreview couldn't collect data for message add on");
            } else {
                AnonymousClass1Iv A04 = r2.A04(A00, A002);
                if (!(A04 instanceof AnonymousClass1X9)) {
                    Log.e("MessageAddOnManager/createMessageAddOnReactionPreview unexpected fmessage");
                } else {
                    AnonymousClass1X9 r4 = (AnonymousClass1X9) A04;
                    r4.A15(A00, this.A0B, A002);
                    AbstractC15340mz A003 = this.A06.A00(((AnonymousClass1Iv) r4).A00);
                    if (A003 == null) {
                        Log.e("MessageAddOnManager/createMessageAddOnReactionPreview parent message missing");
                    } else {
                        A04.A02 = new C40711sC(A003.A0B(), A003.A0z);
                        AnonymousClass1YY r0 = new AnonymousClass1YY(A003, r4, i);
                        A00.close();
                        return r0;
                    }
                }
            }
            A00.close();
            return null;
        } catch (Throwable th) {
            if (A00 != null) {
                try {
                    A00.close();
                } catch (Throwable unused) {
                }
            }
            throw th;
        }
    }

    public List A05(AbstractC14640lm r19) {
        AnonymousClass1PE A06 = this.A08.A06(r19);
        if (A06 == null || A06.A00() == A06.A01()) {
            return new ArrayList();
        }
        long A02 = this.A07.A02(r19);
        long A01 = A06.A01();
        LinkedList linkedList = new LinkedList();
        HashMap hashMap = new HashMap();
        C16310on A012 = this.A0D.get();
        try {
            C16330op r5 = A012.A03;
            String str = C40471re.A01;
            String valueOf = String.valueOf(A02);
            String valueOf2 = String.valueOf(0);
            String valueOf3 = String.valueOf(A01);
            Cursor A09 = r5.A09(str, new String[]{valueOf, valueOf2, valueOf3, String.valueOf(7L)});
            int columnIndex = A09.getColumnIndex("last_message_add_on_row_id");
            int columnIndex2 = A09.getColumnIndex("unread_count");
            int columnIndex3 = A09.getColumnIndex("parent_message_row_id");
            while (A09.moveToNext()) {
                long j = A09.getLong(columnIndex3);
                AnonymousClass1YY A04 = A04(A012, A09.getInt(columnIndex2), A09.getLong(columnIndex));
                if (A04 != null && A04.A01.A0z.A02) {
                    linkedList.addFirst(A04);
                    hashMap.put(Long.valueOf(j), A04);
                }
            }
            if (!hashMap.keySet().isEmpty()) {
                Set<Long> keySet = hashMap.keySet();
                boolean z = false;
                if (keySet.size() > 0) {
                    z = true;
                }
                AnonymousClass009.A0E(z);
                int i = 3;
                String[] strArr = new String[keySet.size() + 3];
                strArr[0] = valueOf;
                strArr[1] = valueOf2;
                strArr[2] = valueOf3;
                for (Long l : keySet) {
                    strArr[i] = String.valueOf(l.longValue());
                    i++;
                }
                Cursor A092 = r5.A09(C40471re.A02((byte) 56, keySet.size()), strArr);
                int columnIndex4 = A092.getColumnIndex("parent_message_row_id");
                int columnIndex5 = A092.getColumnIndex("sender_jid_row_id");
                while (A092.moveToNext()) {
                    long j2 = A092.getLong(columnIndex4);
                    long j3 = A092.getLong(columnIndex5);
                    AnonymousClass1YY r0 = (AnonymousClass1YY) hashMap.get(Long.valueOf(j2));
                    AnonymousClass009.A05(r0);
                    r0.A03.add(Long.valueOf(j3));
                }
                A092.close();
            }
            A09.close();
            A012.close();
            return linkedList;
        } catch (Throwable th) {
            try {
                A012.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A06(Map map, Set set) {
        this.A0I.A06(set, 17);
        for (Map.Entry entry : map.entrySet()) {
            this.A07.A0F((AbstractC14640lm) entry.getKey(), ((Number) entry.getValue()).longValue());
        }
    }

    public final void A07(Set set) {
        HashSet hashSet = new HashSet();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            AbstractC15340mz r1 = (AbstractC15340mz) it.next();
            AnonymousClass1IS r2 = r1.A0z;
            if (!r2.A02) {
                hashSet.add(r1);
            } else {
                StringBuilder sb = new StringBuilder("MessageAddOnManager/filterOutSelfReactionsAndSendReadSefReceipts this msg should not be part of the set ");
                sb.append(r2);
                Log.w(sb.toString());
            }
        }
        this.A0K.A0B(C22170ye.A00(hashSet));
    }
}
