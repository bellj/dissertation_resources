package X;

import java.lang.ref.WeakReference;
import java.security.Signature;
import javax.crypto.Cipher;
import javax.crypto.Mac;

/* renamed from: X.0DP  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0DP extends AnonymousClass04A {
    public final /* synthetic */ AnonymousClass0OP A00;

    public AnonymousClass0DP(AnonymousClass0OP r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass04A
    public void A00() {
        this.A00.A02.A00();
    }

    @Override // X.AnonymousClass04A
    public void A01(int i, CharSequence charSequence) {
        this.A00.A02.A01(i, charSequence);
    }

    @Override // X.AnonymousClass04A
    public void A02(int i, CharSequence charSequence) {
        WeakReference weakReference = ((C02440Ch) this.A00.A02).A00;
        if (weakReference.get() != null) {
            AnonymousClass0EP r1 = (AnonymousClass0EP) weakReference.get();
            AnonymousClass016 r0 = r1.A09;
            if (r0 == null) {
                r0 = new AnonymousClass016();
                r1.A09 = r0;
            }
            AnonymousClass0EP.A00(r0, charSequence);
        }
    }

    @Override // X.AnonymousClass04A
    public void A03(AnonymousClass0MS r4) {
        AnonymousClass04B r1 = r4.A00;
        AnonymousClass0U4 r2 = null;
        if (r1 != null) {
            Cipher cipher = r1.A01;
            if (cipher != null) {
                r2 = new AnonymousClass0U4(cipher);
            } else {
                Signature signature = r1.A00;
                if (signature != null) {
                    r2 = new AnonymousClass0U4(signature);
                } else {
                    Mac mac = r1.A02;
                    if (mac != null) {
                        r2 = new AnonymousClass0U4(mac);
                    }
                }
            }
        }
        this.A00.A02.A02(new C04700Ms(r2, 2));
    }
}
