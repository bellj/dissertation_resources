package X;

import android.graphics.Bitmap;
import android.widget.ImageView;
import com.whatsapp.R;

/* renamed from: X.59b  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1113559b implements AbstractC38791oi {
    public final /* synthetic */ ImageView A00;
    public final /* synthetic */ C622335s A01;
    public final /* synthetic */ String A02;

    public C1113559b(ImageView imageView, C622335s r2, String str) {
        this.A01 = r2;
        this.A02 = str;
        this.A00 = imageView;
    }

    @Override // X.AbstractC38791oi
    public void ARs(Bitmap bitmap) {
        String str = this.A02;
        ImageView imageView = this.A00;
        if (str.equals(imageView.getTag())) {
            imageView.setImageBitmap(bitmap);
        }
    }

    @Override // X.AbstractC38791oi
    public void AS0() {
        String str = this.A02;
        ImageView imageView = this.A00;
        if (str.equals(imageView.getTag())) {
            imageView.setImageResource(R.drawable.selector_sticker_pack_error);
        }
    }

    @Override // X.AbstractC38791oi
    public void AS5(Bitmap bitmap) {
        String str = this.A02;
        ImageView imageView = this.A00;
        if (str.equals(imageView.getTag())) {
            imageView.setImageBitmap(bitmap);
        }
    }
}
