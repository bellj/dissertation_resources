package X;

import android.text.InputFilter;

/* renamed from: X.3M9  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3M9 implements InputFilter {
    public final /* synthetic */ AnonymousClass2GS A00;

    public /* synthetic */ AnonymousClass3M9(AnonymousClass2GS r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x009c, code lost:
        if (r13 == false) goto L_0x00eb;
     */
    @Override // android.text.InputFilter
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.CharSequence filter(java.lang.CharSequence r18, int r19, int r20, android.text.Spanned r21, int r22, int r23) {
        /*
        // Method dump skipped, instructions count: 340
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3M9.filter(java.lang.CharSequence, int, int, android.text.Spanned, int, int):java.lang.CharSequence");
    }
}
