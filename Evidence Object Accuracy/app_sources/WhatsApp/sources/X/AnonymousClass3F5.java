package X;

/* renamed from: X.3F5  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3F5 {
    public final int A00;
    public final C71133cR A01;
    public final String A02;

    public AnonymousClass3F5(C71133cR r2, String str, int i) {
        if (i < 0) {
            throw C12970iu.A0f("Start index must be >= 0.");
        } else if (str != null) {
            this.A00 = i;
            this.A02 = str;
            this.A01 = r2;
        } else {
            throw null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AnonymousClass3F5)) {
            return false;
        }
        AnonymousClass3F5 r4 = (AnonymousClass3F5) obj;
        if (!this.A02.equals(r4.A02) || this.A00 != r4.A00 || !this.A01.equals(r4.A01)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        Object[] objArr = new Object[3];
        C12960it.A1O(objArr, this.A00);
        objArr[1] = this.A02;
        return C12980iv.A0B(this.A01, objArr, 2);
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("PhoneNumberMatch [");
        int i = this.A00;
        A0k.append(i);
        A0k.append(",");
        String str = this.A02;
        A0k.append(i + str.length());
        A0k.append(") ");
        return C12960it.A0d(str, A0k);
    }
}
