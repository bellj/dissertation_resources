package X;

import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;

/* renamed from: X.02M  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass02M {
    public boolean A00 = false;
    public final AnonymousClass03Z A01 = new AnonymousClass03Z();

    public long A00(int i) {
        return -1;
    }

    public void A08(AnonymousClass03U r1) {
    }

    public void A09(AnonymousClass03U r1) {
    }

    public void A0A(AnonymousClass03U r1) {
    }

    public void A0B(RecyclerView recyclerView) {
    }

    public void A0C(RecyclerView recyclerView) {
    }

    public abstract int A0D();

    public abstract void ANH(AnonymousClass03U v, int i);

    public abstract AnonymousClass03U AOl(ViewGroup viewGroup, int i);

    public int getItemViewType(int i) {
        return 0;
    }

    public final AnonymousClass03U A01(ViewGroup viewGroup, int i) {
        try {
            C003401m.A01("RV CreateView");
            AnonymousClass03U AOl = AOl(viewGroup, i);
            if (AOl.A0H.getParent() == null) {
                AOl.A02 = i;
                return AOl;
            }
            throw new IllegalStateException("ViewHolder views must not be attached when created. Ensure that you are not passing 'true' to the attachToRoot parameter of LayoutInflater.inflate(..., boolean attachToRoot)");
        } finally {
            C003401m.A00();
        }
    }

    public final void A02() {
        this.A01.A00();
    }

    public final void A03(int i) {
        this.A01.A04(null, i, 1);
    }

    public final void A04(int i) {
        this.A01.A02(i, 1);
    }

    public final void A05(int i) {
        this.A01.A03(i, 1);
    }

    public void A06(AnonymousClass03U r1, List list, int i) {
        ANH(r1, i);
    }

    public void A07(boolean z) {
        if (!this.A01.A05()) {
            this.A00 = z;
            return;
        }
        throw new IllegalStateException("Cannot change whether this adapter has stable IDs while the adapter has registered observers.");
    }
}
