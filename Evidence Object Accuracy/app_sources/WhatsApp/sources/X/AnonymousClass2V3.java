package X;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.whatsapp.R;
import com.whatsapp.stickers.StickerStoreFeaturedTabFragment;
import com.whatsapp.util.ViewOnClickCListenerShape4S0200000_I0;
import java.util.List;

/* renamed from: X.2V3  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2V3 extends C38671oW {
    public Drawable.ConstantState A00;
    public final /* synthetic */ StickerStoreFeaturedTabFragment A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2V3(StickerStoreFeaturedTabFragment stickerStoreFeaturedTabFragment, List list) {
        super(stickerStoreFeaturedTabFragment, list);
        this.A01 = stickerStoreFeaturedTabFragment;
    }

    @Override // X.C38671oW, X.AnonymousClass02M
    public void ANH(AnonymousClass03U r9, int i) {
        Drawable.ConstantState constantState;
        C55132hq r92 = (C55132hq) r9;
        super.ANH(r92, i);
        AnonymousClass1KZ r4 = (AnonymousClass1KZ) ((C38671oW) this).A00.get(i);
        boolean z = r4.A0K;
        int i2 = 8;
        ImageView imageView = r92.A05;
        if (z) {
            imageView.setVisibility(0);
        } else {
            imageView.setVisibility(8);
        }
        if (r4.A01()) {
            r92.A04.setVisibility(0);
            r92.A06.setVisibility(8);
            r92.A09.setVisibility(8);
            r92.A07.setVisibility(8);
            if (r4.A05) {
                r92.A0E.setVisibility(4);
                r92.A0A.setVisibility(0);
            } else {
                r92.A0A.setVisibility(4);
                r92.A0E.setVisibility(0);
            }
        } else if (r4.A05) {
            r92.A06.setVisibility(8);
            r92.A04.setVisibility(4);
            ProgressBar progressBar = r92.A09;
            progressBar.setVisibility(0);
            progressBar.setIndeterminate(true);
        } else if (r4.A02 != null) {
            ImageView imageView2 = r92.A06;
            imageView2.setVisibility(0);
            imageView2.setImageResource(R.drawable.countrypicker_checkmark);
            imageView2.setOnClickListener(null);
            imageView2.setContentDescription(this.A01.A0I(R.string.sticker_store_downloaded_content_description));
            imageView2.setClickable(false);
            imageView2.setFocusable(true);
            if (this.A00 == null) {
                this.A00 = imageView2.getBackground().mutate().getConstantState();
            }
            imageView2.setBackgroundResource(0);
            r92.A09.setVisibility(8);
            r92.A04.setVisibility(4);
        } else {
            r92.A04.setVisibility(4);
            ImageView imageView3 = r92.A06;
            imageView3.setVisibility(0);
            r92.A09.setVisibility(8);
            imageView3.setImageResource(R.drawable.sticker_store_download);
            if (imageView3.getBackground() == null && (constantState = this.A00) != null) {
                imageView3.setBackground(constantState.newDrawable(this.A01.A02()));
            }
            imageView3.setContentDescription(this.A01.A0I(R.string.sticker_store_download_pack_content_description));
            imageView3.setOnClickListener(new ViewOnClickCListenerShape4S0200000_I0(r4, 31, this));
        }
        View view = r92.A03;
        if (!r4.A05 && r4.A02 == null && r4.A06) {
            i2 = 0;
        }
        view.setVisibility(i2);
    }
}
