package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* renamed from: X.2Rh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C50862Rh {
    public static int A00(AnonymousClass1V8 r4) {
        int i = 0;
        try {
            AnonymousClass1V8 A0E = r4.A0E("ephemeral");
            if (A0E != null) {
                i = A0E.A05("expiration", 0);
                return i;
            }
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder("GroupProtocolTreeNodeHelper/getEphemeralDuration ");
            sb.append(e.getMessage());
            Log.e(sb.toString(), e);
        }
        return i;
    }

    public static int A01(AnonymousClass1V8 r5) {
        boolean z = false;
        if (r5.A0E("default_sub_group") != null) {
            z = true;
        }
        boolean z2 = false;
        if (r5.A0E("linked_parent") != null) {
            z2 = true;
        }
        if (r5.A0E("parent") != null) {
            return 1;
        }
        if (z) {
            return 3;
        }
        if (z2) {
            return 2;
        }
        return 0;
    }

    public static C15580nU A02(AbstractC15710nm r3, AnonymousClass1V8 r4) {
        AnonymousClass1V8 A0E = r4.A0E("linked_parent");
        if (A0E == null) {
            return null;
        }
        return (C15580nU) A0E.A0A(r3, C15580nU.class, "jid");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x003c, code lost:
        if (r3 == null) goto L_0x003e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x007f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass1PD A03(X.AbstractC15710nm r8, X.AnonymousClass1V8 r9, X.AnonymousClass1V8 r10) {
        /*
            java.lang.String r0 = "description"
            X.1V8 r4 = r9.A0E(r0)
            if (r4 == 0) goto L_0x003e
            java.lang.String r0 = "body"
            X.1V8 r3 = r4.A0E(r0)
            java.lang.String r0 = "delete"
            X.1V8 r2 = r4.A0E(r0)
            if (r3 == 0) goto L_0x004c
            if (r2 == 0) goto L_0x004c
            java.lang.String r0 = "Node: "
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            r1.append(r4)
            java.lang.String r0 = " contains both a body and delete child: "
            r1.append(r0)
            r1.append(r3)
            java.lang.String r0 = "; "
            r1.append(r0)
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            X.1V9 r0 = new X.1V9
            r0.<init>(r1)
            throw r0
        L_0x003c:
            if (r3 != 0) goto L_0x0041
        L_0x003e:
            X.1PD r0 = X.AnonymousClass1PD.A04
            return r0
        L_0x0041:
            java.lang.String r0 = r3.A0G()
            if (r0 == 0) goto L_0x0087
            java.lang.String r5 = r3.A0G()
            goto L_0x0059
        L_0x004c:
            X.1W9[] r0 = r4.A0L()
            if (r0 == 0) goto L_0x003c
            int r0 = r0.length
            if (r0 == 0) goto L_0x003c
            if (r3 != 0) goto L_0x0041
            java.lang.String r5 = ""
        L_0x0059:
            boolean r0 = android.text.TextUtils.isEmpty(r5)
            if (r0 == 0) goto L_0x007f
            r4 = 0
        L_0x0060:
            X.AnonymousClass009.A05(r10)
            r0 = 0
            java.lang.String r2 = "t"
            long r6 = r10.A08(r2, r0)
            X.AnonymousClass009.A05(r10)
            java.lang.Class<com.whatsapp.jid.UserJid> r1 = com.whatsapp.jid.UserJid.class
            java.lang.String r0 = "participant"
            com.whatsapp.jid.Jid r3 = r10.A0A(r8, r1, r0)
            com.whatsapp.jid.UserJid r3 = (com.whatsapp.jid.UserJid) r3
            X.1PD r2 = new X.1PD
            r2.<init>(r3, r4, r5, r6)
            return r2
        L_0x007f:
            java.lang.String r1 = "id"
            r0 = 0
            java.lang.String r4 = r4.A0I(r1, r0)
            goto L_0x0060
        L_0x0087:
            java.lang.String r1 = "Non-empy description tag with no body"
            X.1V9 r0 = new X.1V9
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C50862Rh.A03(X.0nm, X.1V8, X.1V8):X.1PD");
    }

    public static AnonymousClass1V0 A04(AnonymousClass1V8 r6) {
        if (A01(r6) == 1) {
            return null;
        }
        AnonymousClass1V8 A0E = r6.A0E("growth_locked");
        if (A0E == null) {
            return new AnonymousClass1V0(0, 0);
        }
        String A0I = A0E.A0I("type", "");
        if ("invite".equals(A0I)) {
            return new AnonymousClass1V0(1, A0E.A09(A0E.A0H("expiration"), "expiration"));
        }
        StringBuilder sb = new StringBuilder("GroupProtocolTreeNodeHelper/getGrowthLock unexpected type: ");
        sb.append(A0I);
        Log.i(sb.toString());
        return null;
    }

    public static List A05(AbstractC15710nm r11, AnonymousClass1V8 r12, Map map) {
        AnonymousClass1MU r1;
        AnonymousClass1MU r7 = null;
        String str = null;
        ArrayList arrayList = new ArrayList();
        for (AnonymousClass1V8 r9 : r12.A0J("participant")) {
            UserJid userJid = (UserJid) r9.A0A(r11, UserJid.class, "jid");
            if (userJid != null) {
                String A0I = r9.A0I("type", "");
                AnonymousClass009.A05(A0I);
                UserJid userJid2 = (UserJid) r9.A0A(r11, UserJid.class, "lid");
                if (userJid2 != null) {
                    r7 = userJid2;
                }
                String A0I2 = r9.A0I("display_name", null);
                if (!AnonymousClass1US.A0C(A0I2)) {
                    str = A0I2;
                }
                AnonymousClass009.A05(userJid);
                AnonymousClass009.A05(A0I);
                if (r7 == null || !(r7 instanceof AnonymousClass1MU)) {
                    r1 = null;
                } else {
                    r1 = r7;
                }
                arrayList.add(new AnonymousClass4XJ(r1, userJid, A0I, str));
                String A0I3 = r9.A0I("type", "");
                AnonymousClass009.A05(A0I3);
                map.put(userJid, A0I3);
            }
        }
        return arrayList;
    }
}
