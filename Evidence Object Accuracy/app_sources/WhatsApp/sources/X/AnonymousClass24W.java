package X;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.SystemClock;
import java.util.ArrayList;

/* renamed from: X.24W  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass24W {
    public int A00;
    public int A01;
    public long A02 = -1;
    public long A03;
    public long A04;
    public Uri A05;
    public Bundle A06;
    public C32731ce A07;
    public String A08;
    public String A09;
    public String A0A;
    public ArrayList A0B;
    public ArrayList A0C;
    public boolean A0D;
    public boolean A0E;
    public boolean A0F;
    public boolean A0G;
    public boolean A0H;
    public final Context A0I;

    public AnonymousClass24W(Context context) {
        this.A0I = context;
    }

    public Intent A00() {
        long j;
        Intent intent = new Intent();
        intent.setClassName(this.A0I.getPackageName(), "com.whatsapp.mediacomposer.MediaComposerActivity");
        intent.putExtra("android.intent.extra.STREAM", this.A0C);
        intent.putExtra("android.intent.extra.TEXT", this.A0A);
        intent.putExtra("jid", this.A08);
        intent.putExtra("jids", this.A0B);
        intent.putExtra("max_items", this.A00);
        intent.putExtra("origin", this.A01);
        long j2 = this.A03;
        if (j2 <= 0) {
            j2 = SystemClock.elapsedRealtime();
        }
        intent.putExtra("picker_open_time", j2);
        intent.putExtra("send", this.A0G);
        intent.putExtra("quoted_message_row_id", this.A04);
        intent.putExtra("quoted_group_jid", this.A09);
        intent.putExtra("number_from_url", this.A0D);
        intent.putExtra("media_preview_params", this.A06);
        intent.putExtra("smb_quick_reply", false);
        intent.putExtra("start_home", this.A0H);
        intent.putExtra("animate_uri", this.A05);
        intent.putExtra("preselected_image_uri", (Parcelable) null);
        intent.putExtra("scan_for_qr", this.A0F);
        intent.putExtra("is_new_content", this.A0E);
        intent.putExtra("status_distribution", this.A07);
        int i = this.A01;
        if (i == 1 || i == 11 || i == 14 || i == 17 || i == 20 || i == 21) {
            j = this.A02;
        } else {
            j = -1;
        }
        intent.putExtra("gallery_duration_ms", j);
        return intent;
    }
}
