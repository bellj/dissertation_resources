package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.payments.ui.BrazilPaymentCardDetailsActivity;

/* renamed from: X.646  reason: invalid class name */
/* loaded from: classes4.dex */
public final /* synthetic */ class AnonymousClass646 implements View.OnClickListener {
    public final /* synthetic */ BrazilPaymentCardDetailsActivity A00;
    public final /* synthetic */ String A01;

    public /* synthetic */ AnonymousClass646(BrazilPaymentCardDetailsActivity brazilPaymentCardDetailsActivity, String str) {
        this.A00 = brazilPaymentCardDetailsActivity;
        this.A01 = str;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        BrazilPaymentCardDetailsActivity brazilPaymentCardDetailsActivity = this.A00;
        String str = this.A01;
        brazilPaymentCardDetailsActivity.A2C(R.string.payment_get_verify_card_data);
        C14830m7 r6 = ((ActivityC13790kL) brazilPaymentCardDetailsActivity).A05;
        C14900mE r3 = ((AbstractView$OnClickListenerC121765jx) brazilPaymentCardDetailsActivity).A05;
        C15570nT r4 = ((ActivityC13790kL) brazilPaymentCardDetailsActivity).A01;
        C17220qS r8 = brazilPaymentCardDetailsActivity.A01;
        C18590sh r14 = ((AbstractActivityC121755jw) brazilPaymentCardDetailsActivity).A0C;
        C17070qD r12 = ((AbstractView$OnClickListenerC121765jx) brazilPaymentCardDetailsActivity).A0D;
        C18610sj r11 = ((AbstractActivityC121755jw) brazilPaymentCardDetailsActivity).A06;
        C129095xA r13 = ((AbstractActivityC121755jw) brazilPaymentCardDetailsActivity).A09;
        new C129355xa(brazilPaymentCardDetailsActivity, r3, r4, ((ActivityC13810kN) brazilPaymentCardDetailsActivity).A07, r6, brazilPaymentCardDetailsActivity.A00, r8, brazilPaymentCardDetailsActivity.A05, ((AbstractActivityC121755jw) brazilPaymentCardDetailsActivity).A03, r11, r12, r13, r14, str).A00(new AnonymousClass6AH(brazilPaymentCardDetailsActivity));
    }
}
