package X;

/* renamed from: X.5H3  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5H3 extends RuntimeException {
    public AnonymousClass5H3(String str) {
        super(str);
    }

    public AnonymousClass5H3(String str, Throwable th) {
        super(str, th);
    }
}
