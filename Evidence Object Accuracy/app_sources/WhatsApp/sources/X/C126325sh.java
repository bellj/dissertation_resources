package X;

/* renamed from: X.5sh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C126325sh {
    public final AnonymousClass1V8 A00;

    public C126325sh(AnonymousClass3CT r7, String str, String str2, String str3) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy A0N = C117295Zj.A0N(A0M);
        C41141sy.A01(A0N, "action", "br-verify-send-auth-code");
        if (C117305Zk.A1Y(str, false)) {
            C41141sy.A01(A0N, "credential-id", str);
        }
        if (C117295Zj.A1W(str2, 1, false)) {
            C41141sy.A01(A0N, "nonce", str2);
        }
        if (C117305Zk.A1X(str3, 1, false)) {
            C41141sy.A01(A0N, "auth-code", str3);
        }
        this.A00 = C117295Zj.A0J(A0N, A0M, r7);
    }
}
