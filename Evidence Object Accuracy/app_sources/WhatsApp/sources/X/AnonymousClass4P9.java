package X;

import android.util.SparseArray;

/* renamed from: X.4P9  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4P9 {
    public final int A00;
    public final int A01;
    public final SparseArray A02;

    public AnonymousClass4P9(SparseArray sparseArray, int i, int i2) {
        this.A01 = i;
        this.A00 = i2;
        this.A02 = sparseArray;
    }
}
