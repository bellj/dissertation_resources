package X;

import java.util.LinkedHashSet;

/* renamed from: X.4Vh  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4Vh {
    public final AbstractC12520i3 A00;
    public final AnonymousClass4I0 A01 = new AnonymousClass4I0(this);
    public final C105914up A02;
    public final LinkedHashSet A03 = new LinkedHashSet();

    public AnonymousClass4Vh(AbstractC12520i3 r2, C105914up r3) {
        this.A00 = r2;
        this.A02 = r3;
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0065 A[Catch: all -> 0x0080, TRY_ENTER, TryCatch #1 {, blocks: (B:4:0x000f, B:6:0x0022, B:7:0x0029, B:24:0x0065, B:25:0x0071, B:9:0x0030, B:12:0x0046, B:15:0x0056, B:19:0x005f, B:21:0x0061), top: B:33:0x000f }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C08870bz A00(X.C08870bz r13, int r14) {
        /*
            r12 = this;
            X.4up r2 = r12.A02
            X.0i3 r0 = r12.A00
            X.4uk r5 = new X.4uk
            r5.<init>(r0, r14)
            X.4I0 r6 = r12.A01
            r2.A04()
            monitor-enter(r2)
            X.4YA r9 = r2.A05     // Catch: all -> 0x0080
            java.lang.Object r4 = r9.A02(r5)     // Catch: all -> 0x0080
            X.4SJ r4 = (X.AnonymousClass4SJ) r4     // Catch: all -> 0x0080
            X.4YA r3 = r2.A04     // Catch: all -> 0x0080
            java.lang.Object r0 = r3.A02(r5)     // Catch: all -> 0x0080
            X.4SJ r0 = (X.AnonymousClass4SJ) r0     // Catch: all -> 0x0080
            r11 = 0
            if (r0 == 0) goto L_0x002e
            r2.A05(r0)     // Catch: all -> 0x0080
            X.0bz r10 = r2.A02(r0)     // Catch: all -> 0x0080
        L_0x0029:
            java.lang.Object r1 = r13.A04()     // Catch: all -> 0x0080
            goto L_0x0030
        L_0x002e:
            r10 = r11
            goto L_0x0029
        L_0x0030:
            X.5S7 r0 = r2.A06     // Catch: all -> 0x007e
            int r7 = r0.AGn(r1)     // Catch: all -> 0x007e
            X.4Sz r0 = r2.A01     // Catch: all -> 0x007e
            int r0 = r0.A01     // Catch: all -> 0x007e
            r8 = 1
            if (r7 > r0) goto L_0x0062
            int r1 = r3.A00()     // Catch: all -> 0x0060
            int r0 = r9.A00()     // Catch: all -> 0x0060
            int r1 = r1 - r0
            X.4Sz r0 = r2.A01     // Catch: all -> 0x007e
            int r0 = r0.A00     // Catch: all -> 0x007e
            int r0 = r0 - r8
            if (r1 > r0) goto L_0x0062
            int r1 = r3.A01()     // Catch: all -> 0x005e
            int r0 = r9.A01()     // Catch: all -> 0x005e
            int r1 = r1 - r0
            X.4Sz r0 = r2.A01     // Catch: all -> 0x007e
            int r0 = r0.A02     // Catch: all -> 0x007e
            int r0 = r0 - r7
            if (r1 > r0) goto L_0x0062
            goto L_0x0063
        L_0x005e:
            r0 = move-exception
            throw r0     // Catch: all -> 0x007e
        L_0x0060:
            r0 = move-exception
            throw r0     // Catch: all -> 0x007e
        L_0x0062:
            r8 = 0
        L_0x0063:
            if (r8 == 0) goto L_0x0071
            X.4SJ r0 = new X.4SJ     // Catch: all -> 0x0080
            r0.<init>(r13, r6, r5)     // Catch: all -> 0x0080
            r3.A03(r5, r0)     // Catch: all -> 0x0080
            X.0bz r11 = r2.A01(r0)     // Catch: all -> 0x0080
        L_0x0071:
            monitor-exit(r2)     // Catch: all -> 0x0080
            if (r10 == 0) goto L_0x0077
            r10.close()
        L_0x0077:
            X.C105914up.A00(r4)
            r2.A03()
            return r11
        L_0x007e:
            r0 = move-exception
            throw r0     // Catch: all -> 0x0080
        L_0x0080:
            r0 = move-exception
            monitor-exit(r2)     // Catch: all -> 0x0080
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass4Vh.A00(X.0bz, int):X.0bz");
    }
}
