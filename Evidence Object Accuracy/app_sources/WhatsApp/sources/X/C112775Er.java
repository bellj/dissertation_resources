package X;

import java.io.Serializable;

/* renamed from: X.5Er  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C112775Er implements AnonymousClass5X4, Serializable {
    public static final C112775Er A00 = new C112775Er();
    public static final long serialVersionUID = 0;

    @Override // X.AnonymousClass5X4
    public Object fold(Object obj, AnonymousClass5ZQ r2) {
        return obj;
    }

    @Override // X.AnonymousClass5X4
    public AnonymousClass5ZU get(AbstractC115495Rt r2) {
        return null;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return 0;
    }

    @Override // X.AnonymousClass5X4
    public AnonymousClass5X4 minusKey(AbstractC115495Rt r1) {
        return this;
    }

    @Override // X.AnonymousClass5X4
    public AnonymousClass5X4 plus(AnonymousClass5X4 r2) {
        C16700pc.A0E(r2, 0);
        return r2;
    }

    @Override // java.lang.Object
    public String toString() {
        return "EmptyCoroutineContext";
    }

    private final Object readResolve() {
        return A00;
    }
}
