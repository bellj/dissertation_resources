package X;

/* renamed from: X.2TF  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2TF extends AbstractC16350or {
    public final C51162Tc A00;
    public final AnonymousClass2TK A01;
    public final boolean A02;

    public AnonymousClass2TF(AbstractC001200n r1, C51162Tc r2, AnonymousClass2TK r3, boolean z) {
        super(r1);
        this.A00 = r2;
        this.A01 = r3;
        this.A02 = z;
    }
}
