package X;

import android.os.Build;

/* renamed from: X.1di  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C33261di {
    public static final boolean A00;

    static {
        boolean z = false;
        if (Build.VERSION.SDK_INT >= 26) {
            z = true;
        }
        A00 = z;
    }
}
