package X;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.gallery.DocumentsGalleryFragment;
import com.whatsapp.gallery.GalleryFragmentBase;

/* renamed from: X.2hn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C55102hn extends AnonymousClass03U {
    public C16440p1 A00;
    public final View A01;
    public final View A02;
    public final View A03;
    public final View A04;
    public final ImageView A05;
    public final TextView A06;
    public final TextView A07;
    public final TextView A08;
    public final TextView A09;
    public final TextView A0A;
    public final /* synthetic */ DocumentsGalleryFragment A0B;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C55102hn(View view, DocumentsGalleryFragment documentsGalleryFragment) {
        super(view);
        this.A0B = documentsGalleryFragment;
        this.A05 = C12970iu.A0L(view, R.id.icon);
        this.A09 = C12960it.A0J(view, R.id.title);
        this.A06 = C12960it.A0J(view, R.id.date);
        this.A08 = C12960it.A0J(view, R.id.size);
        this.A03 = view.findViewById(R.id.bullet_size);
        this.A07 = C12960it.A0J(view, R.id.info);
        this.A01 = view.findViewById(R.id.bullet_info);
        this.A0A = C12960it.A0J(view, R.id.type);
        this.A04 = view.findViewById(R.id.starred_status);
        this.A02 = view.findViewById(R.id.kept_status);
        C12960it.A0x(view, this, 17);
        view.setOnLongClickListener(new View.OnLongClickListener() { // from class: X.3Mn
            @Override // android.view.View.OnLongClickListener
            public final boolean onLongClick(View view2) {
                C55102hn r0 = C55102hn.this;
                C16440p1 r4 = r0.A00;
                if (r4 == null) {
                    return false;
                }
                DocumentsGalleryFragment documentsGalleryFragment2 = r0.A0B;
                ActivityC000900k A01 = C13010iy.A01(documentsGalleryFragment2);
                AbstractC13890kV r1 = (AbstractC13890kV) A01;
                boolean AIM = r1.AIM();
                AnonymousClass009.A05(A01);
                if (AIM) {
                    r1.Af1(r4);
                } else {
                    r1.AeE(r4);
                }
                ((GalleryFragmentBase) documentsGalleryFragment2).A0A.A02();
                return true;
            }
        });
    }
}
