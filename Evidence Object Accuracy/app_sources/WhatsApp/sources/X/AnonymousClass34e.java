package X;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.text.TextUtils;
import com.whatsapp.R;
import java.util.List;

/* renamed from: X.34e  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass34e extends AbstractC51472Uz {
    public AnonymousClass34Z A00;
    public boolean A01;
    public final AnonymousClass130 A02;
    public final AnonymousClass1J1 A03;
    public final C20830wO A04;
    public final C16030oK A05;
    public final C244415n A06;

    public AnonymousClass34e(Context context, C15570nT r16, AnonymousClass130 r17, C15550nR r18, C15610nY r19, AnonymousClass1J1 r20, C63563Cb r21, C63543Bz r22, AnonymousClass01d r23, C14830m7 r24, AnonymousClass018 r25, C20830wO r26, AnonymousClass19M r27, C16030oK r28, C244415n r29, C16630pM r30, AnonymousClass12F r31) {
        super(context, r16, r18, r19, r21, r22, r23, r24, r25, r27, r30, r31);
        A00();
        this.A06 = r29;
        this.A02 = r17;
        this.A03 = r20;
        this.A05 = r28;
        this.A04 = r26;
        A01();
    }

    @Override // X.AbstractC51472Uz, X.AnonymousClass2V0
    public /* bridge */ /* synthetic */ CharSequence A02(C15370n3 r10, AbstractC15340mz r11) {
        String str;
        AnonymousClass1XP r112 = (AnonymousClass1XP) r11;
        if (!(r112 instanceof C30341Xa) || (str = ((C30341Xa) r112).A03) == null) {
            str = "";
        }
        int i = R.drawable.ic_inline_live_location;
        if (r112 instanceof AnonymousClass1XO) {
            i = R.drawable.msg_status_location;
        }
        Drawable A01 = AnonymousClass2GE.A01(getContext(), i, R.color.msgStatusTint);
        TextPaint paint = ((AnonymousClass2V0) this).A01.getPaint();
        if (TextUtils.isEmpty(str)) {
            return super.A02(r10, r112);
        }
        CharSequence A012 = C52252aV.A01(paint, A01, "");
        CharSequence A013 = AnonymousClass3J0.A01(getContext(), this.A08, this.A0A, this.A0F, r10, r112.A0z.A02);
        if (TextUtils.isEmpty(A013)) {
            return A012;
        }
        boolean A0G = C42941w9.A0G(A012);
        CharSequence[] charSequenceArr = new CharSequence[4];
        charSequenceArr[0] = A013;
        char c = 8207;
        if (A0G) {
            c = 8206;
        }
        String valueOf = String.valueOf(c);
        charSequenceArr[1] = valueOf;
        charSequenceArr[2] = A012;
        charSequenceArr[3] = valueOf;
        return TextUtils.concat(charSequenceArr);
    }

    /* renamed from: A07 */
    public void A05(AnonymousClass1XP r2, List list) {
        super.A05(r2, list);
        this.A00.setMessage(r2, list);
    }
}
