package X;

import android.graphics.SurfaceTexture;
import android.opengl.EGL14;
import android.opengl.EGLConfig;
import android.opengl.EGLContext;
import android.opengl.EGLDisplay;
import android.opengl.EGLExt;
import android.opengl.EGLSurface;
import android.os.Build;
import android.view.Surface;

/* renamed from: X.5Pf  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5Pf extends AbstractC94504bw {
    public static final int A04 = Build.VERSION.SDK_INT;
    public EGLConfig A00;
    public EGLContext A01;
    public EGLDisplay A02;
    public EGLSurface A03 = EGL14.EGL_NO_SURFACE;

    public AnonymousClass5Pf(AnonymousClass5Pd r11, int[] iArr) {
        EGLDisplay eglGetDisplay = EGL14.eglGetDisplay(0);
        if (eglGetDisplay != EGL14.EGL_NO_DISPLAY) {
            int[] iArr2 = new int[2];
            if (EGL14.eglInitialize(eglGetDisplay, iArr2, 0, iArr2, 1)) {
                this.A02 = eglGetDisplay;
                EGLConfig[] eGLConfigArr = new EGLConfig[1];
                if (EGL14.eglChooseConfig(eglGetDisplay, iArr, 0, eGLConfigArr, 0, 1, new int[1], 0)) {
                    EGLConfig eGLConfig = eGLConfigArr[0];
                    this.A00 = eGLConfig;
                    EGLDisplay eGLDisplay = this.A02;
                    if (r11 != null) {
                        throw C12980iv.A0n("egl14Context");
                    }
                    EGLContext eglCreateContext = EGL14.eglCreateContext(eGLDisplay, eGLConfig, EGL14.EGL_NO_CONTEXT, new int[]{12440, 2, 12344}, 0);
                    if (eglCreateContext != EGL14.EGL_NO_CONTEXT) {
                        this.A01 = eglCreateContext;
                    } else {
                        C93054Yu.A00("eglCreateContext");
                        throw C12990iw.A0m("Failed to create EGL context");
                    }
                } else {
                    C93054Yu.A00("eglChooseConfig");
                    throw C12990iw.A0m("Unable to find any matching EGL config");
                }
            } else {
                C93054Yu.A00("eglInitialize");
                throw C12990iw.A0m("Unable to initialize EGL14");
            }
        } else {
            throw C12990iw.A0m("Unable to get EGL14 display");
        }
    }

    public static boolean A00() {
        return A04 >= 18;
    }

    @Override // X.AbstractC94504bw
    public int A01() {
        int[] iArr = new int[1];
        EGL14.eglQuerySurface(this.A02, this.A03, 12374, iArr, 0);
        return iArr[0];
    }

    @Override // X.AbstractC94504bw
    public int A02() {
        int[] iArr = new int[1];
        EGL14.eglQuerySurface(this.A02, this.A03, 12375, iArr, 0);
        return iArr[0];
    }

    @Override // X.AbstractC94504bw
    public void A03() {
        EGLDisplay eGLDisplay = this.A02;
        EGLSurface eGLSurface = EGL14.EGL_NO_SURFACE;
        if (!EGL14.eglMakeCurrent(eGLDisplay, eGLSurface, eGLSurface, EGL14.EGL_NO_CONTEXT)) {
            C93054Yu.A00("detachCurrent");
            throw C12990iw.A0m("eglMakeCurrent failed");
        }
    }

    @Override // X.AbstractC94504bw
    public void A04() {
        A0B();
        EGLSurface eGLSurface = this.A03;
        if (eGLSurface == EGL14.EGL_NO_SURFACE) {
            throw C12990iw.A0m("No EGLSurface - can't make current");
        } else if (!EGL14.eglMakeCurrent(this.A02, eGLSurface, eGLSurface, this.A01)) {
            C93054Yu.A00("makeCurrent");
            throw C12990iw.A0m("eglMakeCurrent failed");
        }
    }

    @Override // X.AbstractC94504bw
    public void A05() {
        A0B();
        A06();
        A03();
        EGL14.eglDestroyContext(this.A02, this.A01);
        EGL14.eglReleaseThread();
        EGL14.eglTerminate(this.A02);
        this.A01 = EGL14.EGL_NO_CONTEXT;
        this.A02 = EGL14.EGL_NO_DISPLAY;
        this.A00 = null;
    }

    @Override // X.AbstractC94504bw
    public void A06() {
        EGLSurface eGLSurface = this.A03;
        if (eGLSurface != EGL14.EGL_NO_SURFACE) {
            EGL14.eglDestroySurface(this.A02, eGLSurface);
            this.A03 = EGL14.EGL_NO_SURFACE;
        }
    }

    @Override // X.AbstractC94504bw
    public void A07(SurfaceTexture surfaceTexture) {
        A0D(surfaceTexture);
    }

    @Override // X.AbstractC94504bw
    public void A08(Surface surface) {
        A0D(surface);
    }

    @Override // X.AbstractC94504bw
    public boolean A09() {
        return C12960it.A1X(this.A03, EGL14.EGL_NO_SURFACE);
    }

    @Override // X.AbstractC94504bw
    public boolean A0A() {
        A0B();
        EGLSurface eGLSurface = this.A03;
        if (eGLSurface != EGL14.EGL_NO_SURFACE) {
            return EGL14.eglSwapBuffers(this.A02, eGLSurface);
        }
        throw C12990iw.A0m("No EGLSurface - can't swap buffers");
    }

    public final void A0B() {
        if (this.A02 == EGL14.EGL_NO_DISPLAY || this.A01 == EGL14.EGL_NO_CONTEXT || this.A00 == null) {
            throw C12990iw.A0m("This object has been released");
        }
    }

    public void A0C(long j) {
        A0B();
        EGLSurface eGLSurface = this.A03;
        if (eGLSurface != EGL14.EGL_NO_SURFACE) {
            EGLExt.eglPresentationTimeANDROID(this.A02, eGLSurface, j);
            EGL14.eglSwapBuffers(this.A02, this.A03);
            return;
        }
        throw C12990iw.A0m("No EGLSurface - can't swap buffers");
    }

    public final void A0D(Object obj) {
        if ((obj instanceof Surface) || (obj instanceof SurfaceTexture)) {
            A0B();
            if (this.A03 == EGL14.EGL_NO_SURFACE) {
                EGLSurface eglCreateWindowSurface = EGL14.eglCreateWindowSurface(this.A02, this.A00, obj, new int[]{12344}, 0);
                this.A03 = eglCreateWindowSurface;
                if (eglCreateWindowSurface == EGL14.EGL_NO_SURFACE) {
                    C93054Yu.A00("eglCreateWindowSurface");
                    throw C12990iw.A0m("Failed to create window surface");
                }
                return;
            }
            throw C12990iw.A0m("Already has an EGLSurface");
        }
        throw C12960it.A0U("Input must be either a Surface or SurfaceTexture");
    }
}
