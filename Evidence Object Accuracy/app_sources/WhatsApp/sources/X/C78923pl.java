package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;

/* renamed from: X.3pl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C78923pl extends AbstractC78753pU {
    public static final AnonymousClass00N A06;
    public static final Parcelable.Creator CREATOR = new C98534iq();
    public List A00;
    public List A01;
    public List A02;
    public List A03;
    public List A04;
    public final int A05;

    static {
        AnonymousClass00N r2 = new AnonymousClass00N();
        A06 = r2;
        r2.put("registered", C78633pE.A00("registered", 2));
        r2.put("in_progress", C78633pE.A00("in_progress", 3));
        r2.put("success", C78633pE.A00("success", 4));
        r2.put("failed", C78633pE.A00("failed", 5));
        r2.put("escrowed", C78633pE.A00("escrowed", 6));
    }

    public C78923pl() {
        this.A05 = 1;
    }

    public C78923pl(List list, List list2, List list3, List list4, List list5, int i) {
        this.A05 = i;
        this.A00 = list;
        this.A01 = list2;
        this.A02 = list3;
        this.A03 = list4;
        this.A04 = list5;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 1, this.A05);
        C95654e8.A0E(parcel, this.A00, 2);
        C95654e8.A0E(parcel, this.A01, 3);
        C95654e8.A0E(parcel, this.A02, 4);
        C95654e8.A0E(parcel, this.A03, 5);
        C95654e8.A0E(parcel, this.A04, 6);
        C95654e8.A06(parcel, A00);
    }
}
