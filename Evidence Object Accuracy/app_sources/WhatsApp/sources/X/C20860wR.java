package X;

import android.content.ContentValues;
import com.whatsapp.jid.UserJid;

/* renamed from: X.0wR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20860wR {
    public final C18460sU A00;
    public final C16490p7 A01;
    public final C21390xL A02;

    public C20860wR(C18460sU r1, C16490p7 r2, C21390xL r3) {
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
    }

    public void A00(C30331Wz r10) {
        Long l;
        UserJid userJid;
        C21390xL r5 = this.A02;
        if (r5.A01("revoked_ready", 0) == 1 || (r10.A11 > 0 && r10.A11 <= r5.A01("migration_message_revoked_index", 0))) {
            boolean z = false;
            boolean z2 = false;
            if (r10.A11 > 0) {
                z2 = true;
            }
            StringBuilder sb = new StringBuilder("RevokedMessageStore/insertOrUpdateRevokedMessage/message must have row_id set; key=");
            AnonymousClass1IS r3 = r10.A0z;
            sb.append(r3);
            AnonymousClass009.A0B(sb.toString(), z2);
            boolean z3 = false;
            if (r10.A08() == 1) {
                z3 = true;
            }
            StringBuilder sb2 = new StringBuilder("RevokedMessageStore/insertOrUpdateRevokedMessage/message in main storage; key=");
            sb2.append(r3);
            AnonymousClass009.A0B(sb2.toString(), z3);
            C16310on A02 = this.A01.A02();
            try {
                ContentValues contentValues = new ContentValues();
                contentValues.put("message_row_id", Long.valueOf(r10.A11));
                C30021Vq.A04(contentValues, "revoked_key_id", r10.A01);
                if (!(r10 instanceof C30321Wy) || (userJid = ((C30321Wy) r10).A00) == null) {
                    l = null;
                } else {
                    l = Long.valueOf(this.A00.A01(userJid));
                }
                if (l == null) {
                    contentValues.putNull("admin_jid_row_id");
                } else {
                    contentValues.put("admin_jid_row_id", l);
                }
                contentValues.put("revoke_timestamp", Long.valueOf(r10.A00));
                if (A02.A03.A06(contentValues, "message_revoked", 5) == r10.A11) {
                    z = true;
                }
                AnonymousClass009.A0C("RevokedMessageStore/insertOrUpdateRevokedMessage/inserted row should have same row_id", z);
                A02.close();
            } catch (Throwable th) {
                try {
                    A02.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
    }
}
