package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

/* renamed from: X.4j7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98704j7 implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        int i = 0;
        ArrayList arrayList = null;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                i = C95664e9.A02(parcel, readInt);
            } else if (c != 2) {
                C95664e9.A0D(parcel, readInt);
            } else {
                arrayList = C95664e9.A0B(parcel, C78423ot.CREATOR, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C56402kp(arrayList, i);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new C56402kp[i];
    }
}
