package X;

import android.app.Person;
import android.os.Bundle;
import android.os.PersistableBundle;
import androidx.core.graphics.drawable.IconCompat;

/* renamed from: X.03s  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C007303s {
    public IconCompat A00;
    public CharSequence A01;
    public String A02;
    public String A03;
    public boolean A04;
    public boolean A05;

    public C007303s(C007403t r2) {
        this.A01 = r2.A01;
        this.A00 = r2.A00;
        this.A03 = r2.A03;
        this.A02 = r2.A02;
        this.A04 = r2.A04;
        this.A05 = r2.A05;
    }

    public static C007303s A00(PersistableBundle persistableBundle) {
        return AnonymousClass0Qf.A01(persistableBundle);
    }

    public Person A01() {
        return AnonymousClass0KT.A00(this);
    }

    public Bundle A02() {
        Bundle bundle;
        Bundle bundle2 = new Bundle();
        bundle2.putCharSequence("name", this.A01);
        IconCompat iconCompat = this.A00;
        if (iconCompat != null) {
            bundle = iconCompat.A0A();
        } else {
            bundle = null;
        }
        bundle2.putBundle("icon", bundle);
        bundle2.putString("uri", this.A03);
        bundle2.putString("key", this.A02);
        bundle2.putBoolean("isBot", this.A04);
        bundle2.putBoolean("isImportant", this.A05);
        return bundle2;
    }

    public PersistableBundle A03() {
        return AnonymousClass0Qf.A00(this);
    }
}
