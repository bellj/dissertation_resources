package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;
import com.whatsapp.util.Log;

/* renamed from: X.2OV  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2OV extends Handler {
    public final /* synthetic */ AnonymousClass19Z A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2OV(Looper looper, AnonymousClass19Z r2) {
        super(looper);
        this.A00 = r2;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        AbstractC14440lR r2;
        int i;
        boolean z = true;
        if (message.what == 1) {
            AnonymousClass19Z r4 = this.A00;
            AnonymousClass1MP r5 = r4.A0S;
            StringBuilder sb = new StringBuilder("app/startOutgoingCall/WHAT_START_PENDING_INTENT ");
            sb.append(r5);
            Log.i(sb.toString());
            if (r5 != null) {
                long j = r4.A00;
                if (j > 0) {
                    r5.A00 = SystemClock.elapsedRealtime() - j;
                }
                Boolean bool = (Boolean) message.obj;
                if (bool == null) {
                    z = false;
                }
                AnonymousClass009.A0A("isRejoin is null", z);
                if (bool == null || !bool.booleanValue()) {
                    r2 = r4.A0N;
                    i = 43;
                } else {
                    if (r5.A04 != null) {
                        r2 = r4.A0N;
                        i = 42;
                    }
                    r4.A0S = null;
                }
                r2.Ab2(new RunnableBRunnable0Shape8S0200000_I0_8(r4, i, r5));
                r4.A0S = null;
            }
        }
    }
}
