package X;

import android.content.Context;
import android.os.Looper;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;

@Deprecated
/* renamed from: X.1UD  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1UD {
    public Looper A00;
    public C471729i A01 = C471729i.A00;
    public AbstractC77683ng A02 = C88804He.A00;
    public String A03;
    public String A04;
    public final Context A05;
    public final ArrayList A06 = new ArrayList();
    public final ArrayList A07 = new ArrayList();
    public final Map A08 = new AnonymousClass00N();
    public final Map A09 = new AnonymousClass00N();
    public final Set A0A = new HashSet();
    public final Set A0B = new HashSet();

    public AnonymousClass1UD(Context context) {
        this.A05 = context;
        this.A00 = context.getMainLooper();
        this.A03 = context.getPackageName();
        this.A04 = context.getClass().getName();
    }

    public AnonymousClass1U8 A00() {
        Map map = this.A09;
        C13020j0.A03("must call addApi() to add at least one API", !map.isEmpty());
        C108184yd r3 = C108184yd.A00;
        AnonymousClass1UE r1 = C88804He.A04;
        if (map.containsKey(r1)) {
            r3 = (C108184yd) map.get(r1);
        }
        AnonymousClass1UE r11 = null;
        Set set = this.A0A;
        AnonymousClass3BW r5 = new AnonymousClass3BW(r3, this.A03, this.A04, this.A08, set);
        Map map2 = r5.A04;
        AnonymousClass00N r4 = new AnonymousClass00N();
        AnonymousClass00N r32 = new AnonymousClass00N();
        ArrayList arrayList = new ArrayList();
        for (AnonymousClass1UE r6 : map.keySet()) {
            Object obj = map.get(r6);
            boolean z = false;
            if (map2.get(r6) != null) {
                z = true;
            }
            r4.put(r6, Boolean.valueOf(z));
            C108214yh r14 = new C108214yh(r6, z);
            arrayList.add(r14);
            AbstractC77683ng r13 = r6.A00;
            C13020j0.A01(r13);
            AbstractC72443eb A00 = r13.A00(this.A05, this.A00, r14, r14, r5, obj);
            r32.put(r6.A01, A00);
            if (A00.AZd()) {
                if (r11 == null) {
                    r11 = r6;
                } else {
                    String str = r6.A02;
                    String str2 = r11.A02;
                    StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 21 + String.valueOf(str2).length());
                    sb.append(str);
                    sb.append(" cannot be used with ");
                    sb.append(str2);
                    throw new IllegalStateException(sb.toString());
                }
            }
        }
        if (r11 != null) {
            Object[] objArr = {r11.A02};
            if (!set.equals(this.A0B)) {
                throw new IllegalStateException(String.format("Must not set scopes in GoogleApiClient.Builder when using %s. Set account in GoogleSignInOptions.Builder instead.", objArr));
            }
        }
        C77733nl.A00(r32.values(), true);
        C77733nl r7 = new C77733nl(this.A05, this.A00, this.A01, this.A02, r5, arrayList, this.A06, this.A07, r4, r32, new ReentrantLock());
        Set set2 = AnonymousClass1U8.A00;
        synchronized (set2) {
            set2.add(r7);
        }
        return r7;
    }

    public void A01(AnonymousClass1UE r3) {
        C13020j0.A02(r3, "Api must not be null");
        this.A09.put(r3, null);
        C13020j0.A02(r3.A00, "Base client builder must not be null");
        List emptyList = Collections.emptyList();
        this.A0B.addAll(emptyList);
        this.A0A.addAll(emptyList);
    }
}
