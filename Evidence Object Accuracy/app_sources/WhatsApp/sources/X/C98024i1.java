package X;

import android.media.MediaPlayer;
import com.whatsapp.videoplayback.VideoSurfaceView;

/* renamed from: X.4i1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C98024i1 implements MediaPlayer.OnBufferingUpdateListener {
    public final /* synthetic */ VideoSurfaceView A00;

    public C98024i1(VideoSurfaceView videoSurfaceView) {
        this.A00 = videoSurfaceView;
    }

    @Override // android.media.MediaPlayer.OnBufferingUpdateListener
    public void onBufferingUpdate(MediaPlayer mediaPlayer, int i) {
        this.A00.A01 = i;
    }
}
