package X;

import android.os.Build;
import android.view.accessibility.AccessibilityNodeInfo;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.msys.mci.DefaultCrypto;

/* renamed from: X.04a  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C007804a {
    public static final C007804a A01 = new C007804a(64, null);
    public static final C007804a A02 = new C007804a(128, null);
    public static final C007804a A03 = new C007804a(2, null);
    public static final C007804a A04 = new C007804a(8, null);
    public static final C007804a A05 = new C007804a(16, null);
    public static final C007804a A06 = new C007804a(524288, null);
    public static final C007804a A07;
    public static final C007804a A08 = new C007804a(16384, null);
    public static final C007804a A09 = new C007804a(65536, null);
    public static final C007804a A0A = new C007804a(1048576, null);
    public static final C007804a A0B = new C007804a(262144, null);
    public static final C007804a A0C = new C007804a(1, null);
    public static final C007804a A0D;
    public static final C007804a A0E;
    public static final C007804a A0F = new C007804a(32, null);
    public static final C007804a A0G;
    public static final C007804a A0H = new C007804a(null, null, 256);
    public static final C007804a A0I = new C007804a(null, null, EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH);
    public static final C007804a A0J;
    public static final C007804a A0K;
    public static final C007804a A0L;
    public static final C007804a A0M;
    public static final C007804a A0N = new C007804a(32768, null);
    public static final C007804a A0O;
    public static final C007804a A0P = new C007804a(null, null, 512);
    public static final C007804a A0Q = new C007804a(null, null, EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH);
    public static final C007804a A0R = new C007804a(DefaultCrypto.BUFFER_SIZE, null);
    public static final C007804a A0S;
    public static final C007804a A0T = new C007804a(4096, null);
    public static final C007804a A0U;
    public static final C007804a A0V;
    public static final C007804a A0W;
    public static final C007804a A0X;
    public static final C007804a A0Y = new C007804a(4, null);
    public static final C007804a A0Z;
    public static final C007804a A0a = new C007804a(null, null, C25981Bo.A0F);
    public static final C007804a A0b = new C007804a(null, null, 2097152);
    public static final C007804a A0c;
    public static final C007804a A0d;
    public final Object A00;

    static {
        AccessibilityNodeInfo.AccessibilityAction accessibilityAction;
        AccessibilityNodeInfo.AccessibilityAction accessibilityAction2;
        AccessibilityNodeInfo.AccessibilityAction accessibilityAction3;
        AccessibilityNodeInfo.AccessibilityAction accessibilityAction4;
        AccessibilityNodeInfo.AccessibilityAction accessibilityAction5;
        AccessibilityNodeInfo.AccessibilityAction accessibilityAction6;
        AccessibilityNodeInfo.AccessibilityAction accessibilityAction7;
        AccessibilityNodeInfo.AccessibilityAction accessibilityAction8;
        AccessibilityNodeInfo.AccessibilityAction accessibilityAction9;
        AccessibilityNodeInfo.AccessibilityAction accessibilityAction10;
        AccessibilityNodeInfo.AccessibilityAction accessibilityAction11;
        AccessibilityNodeInfo.AccessibilityAction accessibilityAction12;
        AccessibilityNodeInfo.AccessibilityAction accessibilityAction13;
        AccessibilityNodeInfo.AccessibilityAction accessibilityAction14;
        AccessibilityNodeInfo.AccessibilityAction accessibilityAction15;
        AccessibilityNodeInfo.AccessibilityAction accessibilityAction16 = null;
        int i = Build.VERSION.SDK_INT;
        if (i >= 23) {
            accessibilityAction = AccessibilityNodeInfo.AccessibilityAction.ACTION_SHOW_ON_SCREEN;
        } else {
            accessibilityAction = null;
        }
        A0c = new C007804a(null, accessibilityAction, 16908342);
        if (i >= 23) {
            accessibilityAction2 = AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_TO_POSITION;
        } else {
            accessibilityAction2 = null;
        }
        A0W = new C007804a(null, accessibilityAction2, 16908343);
        if (i >= 23) {
            accessibilityAction3 = AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_UP;
        } else {
            accessibilityAction3 = null;
        }
        AccessibilityNodeInfo.AccessibilityAction accessibilityAction17 = null;
        A0X = new C007804a(null, accessibilityAction3, 16908344);
        if (i >= 23) {
            accessibilityAction4 = AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_LEFT;
        } else {
            accessibilityAction4 = null;
        }
        A0U = new C007804a(null, accessibilityAction4, 16908345);
        if (i >= 23) {
            accessibilityAction5 = AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_DOWN;
        } else {
            accessibilityAction5 = null;
        }
        A0S = new C007804a(null, accessibilityAction5, 16908346);
        if (i >= 23) {
            accessibilityAction6 = AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_RIGHT;
        } else {
            accessibilityAction6 = null;
        }
        A0V = new C007804a(null, accessibilityAction6, 16908347);
        if (i >= 29) {
            accessibilityAction17 = AccessibilityNodeInfo.AccessibilityAction.ACTION_PAGE_UP;
        }
        A0M = new C007804a(null, accessibilityAction17, 16908358);
        if (i >= 29) {
            accessibilityAction7 = AccessibilityNodeInfo.AccessibilityAction.ACTION_PAGE_DOWN;
        } else {
            accessibilityAction7 = null;
        }
        A0J = new C007804a(null, accessibilityAction7, 16908359);
        if (i >= 29) {
            accessibilityAction8 = AccessibilityNodeInfo.AccessibilityAction.ACTION_PAGE_LEFT;
        } else {
            accessibilityAction8 = null;
        }
        A0K = new C007804a(null, accessibilityAction8, 16908360);
        if (i >= 29) {
            accessibilityAction9 = AccessibilityNodeInfo.AccessibilityAction.ACTION_PAGE_RIGHT;
        } else {
            accessibilityAction9 = null;
        }
        A0L = new C007804a(null, accessibilityAction9, 16908361);
        if (i >= 23) {
            accessibilityAction10 = AccessibilityNodeInfo.AccessibilityAction.ACTION_CONTEXT_CLICK;
        } else {
            accessibilityAction10 = null;
        }
        A07 = new C007804a(null, accessibilityAction10, 16908348);
        if (i >= 24) {
            accessibilityAction11 = AccessibilityNodeInfo.AccessibilityAction.ACTION_SET_PROGRESS;
        } else {
            accessibilityAction11 = null;
        }
        A0Z = new C007804a(null, accessibilityAction11, 16908349);
        if (i >= 26) {
            accessibilityAction12 = AccessibilityNodeInfo.AccessibilityAction.ACTION_MOVE_WINDOW;
        } else {
            accessibilityAction12 = null;
        }
        A0G = new C007804a(null, accessibilityAction12, 16908354);
        if (i >= 28) {
            accessibilityAction13 = AccessibilityNodeInfo.AccessibilityAction.ACTION_SHOW_TOOLTIP;
        } else {
            accessibilityAction13 = null;
        }
        A0d = new C007804a(null, accessibilityAction13, 16908356);
        if (i >= 28) {
            accessibilityAction14 = AccessibilityNodeInfo.AccessibilityAction.ACTION_HIDE_TOOLTIP;
        } else {
            accessibilityAction14 = null;
        }
        A0D = new C007804a(null, accessibilityAction14, 16908357);
        if (i >= 30) {
            accessibilityAction15 = AccessibilityNodeInfo.AccessibilityAction.ACTION_PRESS_AND_HOLD;
        } else {
            accessibilityAction15 = null;
        }
        A0O = new C007804a(null, accessibilityAction15, 16908362);
        if (i >= 30) {
            accessibilityAction16 = AccessibilityNodeInfo.AccessibilityAction.ACTION_IME_ENTER;
        }
        A0E = new C007804a(null, accessibilityAction16, 16908372);
    }

    public C007804a(int i, CharSequence charSequence) {
        this(charSequence, null, i);
    }

    public C007804a(CharSequence charSequence, Object obj, int i) {
        if (Build.VERSION.SDK_INT < 21 || obj != null) {
            this.A00 = obj;
        } else {
            this.A00 = new AccessibilityNodeInfo.AccessibilityAction(i, charSequence);
        }
    }

    public int A00() {
        if (Build.VERSION.SDK_INT >= 21) {
            return ((AccessibilityNodeInfo.AccessibilityAction) this.A00).getId();
        }
        return 0;
    }

    public CharSequence A01() {
        if (Build.VERSION.SDK_INT >= 21) {
            return ((AccessibilityNodeInfo.AccessibilityAction) this.A00).getLabel();
        }
        return null;
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof C007804a)) {
            Object obj2 = this.A00;
            Object obj3 = ((C007804a) obj).A00;
            if (obj2 == null) {
                if (obj3 == null) {
                    return true;
                }
            } else if (!obj2.equals(obj3)) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        Object obj = this.A00;
        if (obj != null) {
            return obj.hashCode();
        }
        return 0;
    }
}
