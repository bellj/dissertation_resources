package X;

import android.content.Context;
import java.util.List;

/* renamed from: X.4va  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106364va implements AnonymousClass5WW {
    public final C14260l7 A00;
    public final AnonymousClass28D A01;
    public final List A02;

    public /* synthetic */ C106364va(C14260l7 r1, AnonymousClass28D r2, List list) {
        this.A00 = r1;
        this.A02 = list;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass5WW
    public /* bridge */ /* synthetic */ void A6O(Context context, Object obj, Object obj2, Object obj3) {
        List list = this.A02;
        int size = list.size();
        for (int i = 0; i < size; i++) {
            C65093Ic.A00().A08.A02(this.A00, (AnonymousClass28D) list.get(i), this.A01, obj);
        }
    }

    @Override // X.AnonymousClass5WW
    public boolean Adc(Object obj, Object obj2, Object obj3, Object obj4) {
        return true;
    }

    @Override // X.AnonymousClass5WW
    public /* bridge */ /* synthetic */ void Af8(Context context, Object obj, Object obj2, Object obj3) {
        List list = this.A02;
        int size = list.size();
        for (int i = 0; i < size; i++) {
            C65093Ic.A00().A08.A03(this.A00, (AnonymousClass28D) list.get(i), this.A01, obj);
        }
    }
}
