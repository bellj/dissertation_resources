package X;

/* renamed from: X.04e  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass04e extends AnonymousClass032 {
    public double childSystemTimeS;
    public double childUserTimeS;
    public double systemTimeS;
    public double userTimeS;

    @Override // X.AnonymousClass032
    public /* bridge */ /* synthetic */ AnonymousClass032 A01(AnonymousClass032 r1) {
        A03((AnonymousClass04e) r1);
        return this;
    }

    @Override // X.AnonymousClass032
    public /* bridge */ /* synthetic */ AnonymousClass032 A02(AnonymousClass032 r5, AnonymousClass032 r6) {
        AnonymousClass04e r52 = (AnonymousClass04e) r5;
        AnonymousClass04e r62 = (AnonymousClass04e) r6;
        if (r62 == null) {
            r62 = new AnonymousClass04e();
        }
        if (r52 == null) {
            r62.A03(this);
            return r62;
        }
        r62.systemTimeS = this.systemTimeS - r52.systemTimeS;
        r62.userTimeS = this.userTimeS - r52.userTimeS;
        r62.childSystemTimeS = this.childSystemTimeS - r52.childSystemTimeS;
        r62.childUserTimeS = this.childUserTimeS - r52.childUserTimeS;
        return r62;
    }

    public void A03(AnonymousClass04e r3) {
        this.userTimeS = r3.userTimeS;
        this.systemTimeS = r3.systemTimeS;
        this.childUserTimeS = r3.childUserTimeS;
        this.childSystemTimeS = r3.childSystemTimeS;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass04e r6 = (AnonymousClass04e) obj;
            if (!(Double.compare(r6.systemTimeS, this.systemTimeS) == 0 && Double.compare(r6.userTimeS, this.userTimeS) == 0 && Double.compare(r6.childSystemTimeS, this.childSystemTimeS) == 0 && Double.compare(r6.childUserTimeS, this.childUserTimeS) == 0)) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        long doubleToLongBits = Double.doubleToLongBits(this.systemTimeS);
        int i = (int) (doubleToLongBits ^ (doubleToLongBits >>> 32));
        long doubleToLongBits2 = Double.doubleToLongBits(this.userTimeS);
        int i2 = (i * 31) + ((int) (doubleToLongBits2 ^ (doubleToLongBits2 >>> 32)));
        long doubleToLongBits3 = Double.doubleToLongBits(this.childSystemTimeS);
        int i3 = (i2 * 31) + ((int) (doubleToLongBits3 ^ (doubleToLongBits3 >>> 32)));
        long doubleToLongBits4 = Double.doubleToLongBits(this.childUserTimeS);
        return (i3 * 31) + ((int) (doubleToLongBits4 ^ (doubleToLongBits4 >>> 32)));
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("CpuMetrics{userTimeS=");
        sb.append(this.userTimeS);
        sb.append(", systemTimeS=");
        sb.append(this.systemTimeS);
        sb.append(", childUserTimeS=");
        sb.append(this.childUserTimeS);
        sb.append(", childSystemTimeS=");
        sb.append(this.childSystemTimeS);
        sb.append('}');
        return sb.toString();
    }
}
