package X;

import android.view.ViewTreeObserver;
import com.whatsapp.location.GroupChatLiveLocationsActivity2;

/* renamed from: X.4nn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC101604nn implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ GroupChatLiveLocationsActivity2 A00;

    public ViewTreeObserver$OnGlobalLayoutListenerC101604nn(GroupChatLiveLocationsActivity2 groupChatLiveLocationsActivity2) {
        this.A00 = groupChatLiveLocationsActivity2;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        GroupChatLiveLocationsActivity2 groupChatLiveLocationsActivity2 = this.A00;
        C12980iv.A1F(groupChatLiveLocationsActivity2.A0L, this);
        if (groupChatLiveLocationsActivity2.A0L.getWidth() > 0 && groupChatLiveLocationsActivity2.A0L.getHeight() > 0) {
            groupChatLiveLocationsActivity2.A2i(false);
        }
    }
}
