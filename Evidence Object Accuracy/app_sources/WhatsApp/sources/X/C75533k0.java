package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaTextView;

/* renamed from: X.3k0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75533k0 extends AnonymousClass03U {
    public final WaTextView A00;
    public final WaTextView A01;
    public final AnonymousClass018 A02;

    public C75533k0(View view, AnonymousClass018 r3) {
        super(view);
        this.A02 = r3;
        this.A01 = (WaTextView) AnonymousClass028.A0D(view, R.id.poll_results_option);
        this.A00 = (WaTextView) AnonymousClass028.A0D(view, R.id.poll_results_option_count);
    }
}
