package X;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.payments.ui.invites.PaymentInviteFragment;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.6CZ  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6CZ implements AbstractC1310761d {
    public View A00;
    public ViewGroup A01;
    public ViewGroup A02;
    public Button A03;
    public GridView A04;
    public ImageButton A05;
    public ImageView A06;
    public TextView A07;
    public TextView A08;
    public PaymentInviteFragment A09;
    public final C15550nR A0A;
    public final C15610nY A0B;
    public final C21270x9 A0C;
    public final C16590pI A0D;
    public final C22590zK A0E;
    public final AbstractC14440lR A0F;

    public AnonymousClass6CZ(C15550nR r1, C15610nY r2, C21270x9 r3, C16590pI r4, C22590zK r5, AbstractC14440lR r6) {
        this.A0F = r6;
        this.A0D = r4;
        this.A0C = r3;
        this.A0A = r1;
        this.A0B = r2;
        this.A0E = r5;
    }

    @Override // X.AnonymousClass5Wu
    public /* bridge */ /* synthetic */ void A6Q(Object obj) {
        AnonymousClass4OZ r14 = (AnonymousClass4OZ) obj;
        Context context = this.A00.getContext();
        AnonymousClass009.A05(r14);
        if (1 == r14.A00) {
            this.A02.setVisibility(0);
            this.A01.setVisibility(8);
            return;
        }
        this.A02.setVisibility(8);
        Object obj2 = r14.A01;
        AnonymousClass009.A05(obj2);
        List<AbstractC14640lm> list = (List) obj2;
        if (list.size() == 1) {
            String A08 = this.A0B.A08(this.A0A.A0B((AbstractC14640lm) list.get(0)));
            this.A03.setText(R.string.payments_invite_button_text);
            this.A07.setText(C12960it.A0X(context, A08, new Object[1], 0, R.string.novi_payment_invite_bottom_sheet_body));
            this.A08.setText(C12960it.A0X(context, A08, new Object[1], 0, R.string.novi_payment_invite_bottom_sheet_title));
            this.A05.setImageResource(R.drawable.ic_close);
        } else {
            ArrayList A0l = C12960it.A0l();
            for (AbstractC14640lm r1 : list) {
                A0l.add(this.A0A.A0B(r1));
            }
            this.A04.setAdapter((ListAdapter) new C117595aF(context, context, this.A0C.A04(context, "novi-invite-view-component"), this, A0l, A0l));
            this.A04.setVisibility(0);
        }
        C117295Zj.A0n(this.A03, this, 84);
        this.A05.setVisibility(0);
        C117295Zj.A0o(this.A05, this, list, 23);
        this.A01.setVisibility(0);
        ImageView imageView = this.A06;
        C12990iw.A1N(new AnonymousClass5oX(imageView, this), this.A0F);
    }

    @Override // X.AnonymousClass5Wu
    public int ADq() {
        return R.layout.novi_invite_view_component;
    }

    @Override // X.AnonymousClass5Wu
    public void AYL(View view) {
        this.A00 = view;
        this.A05 = (ImageButton) AnonymousClass028.A0D(view, R.id.back);
        this.A03 = (Button) AnonymousClass028.A0D(view, R.id.invite_button);
        this.A04 = (GridView) AnonymousClass028.A0D(view, R.id.selected_items);
        this.A01 = C117315Zl.A04(view, R.id.invite_ui_content);
        this.A02 = C117315Zl.A04(view, R.id.invite_ui_loader);
        this.A07 = C12960it.A0I(view, R.id.payment_invite_bottom_sheet_body);
        this.A08 = C12960it.A0I(view, R.id.payment_invite_bottom_sheet_title);
        this.A06 = C12970iu.A0K(view, R.id.payment_invite_bottom_sheet_image);
    }

    @Override // X.AbstractC1310761d
    public void AcV(PaymentInviteFragment paymentInviteFragment) {
        this.A09 = paymentInviteFragment;
    }
}
