package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import com.whatsapp.R;

/* renamed from: X.3bH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70433bH implements AbstractC41521tf {
    public final int A00;
    public final int A01;

    @Override // X.AbstractC41521tf
    public /* synthetic */ void AQV() {
    }

    public C70433bH(Context context) {
        this.A01 = context.getResources().getDimensionPixelSize(R.dimen.status_thumbnail_size);
        this.A00 = AnonymousClass00T.A00(context, C41691tw.A08(context) ? R.color.statusThumbnailLoadingPlaceholder_dark : R.color.statusThumbnailLoadingPlaceholder);
    }

    @Override // X.AbstractC41521tf
    public int AGm() {
        return this.A01;
    }

    @Override // X.AbstractC41521tf
    public void Adg(Bitmap bitmap, View view, AbstractC15340mz r5) {
        if (bitmap == null) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.RGB_565);
            bitmap.setPixel(0, 0, this.A00);
        }
        ((ImageView) view).setImageBitmap(bitmap);
    }

    @Override // X.AbstractC41521tf
    public void Adu(View view) {
        if (view instanceof ImageView) {
            ((ImageView) view).setImageResource(R.drawable.circle_shade_big);
        }
    }
}
