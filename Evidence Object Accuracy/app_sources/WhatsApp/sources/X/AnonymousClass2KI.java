package X;

/* renamed from: X.2KI  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2KI extends AbstractC44161yL {
    public final boolean A00;
    public final /* synthetic */ AnonymousClass2F9 A01;

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0005, code lost:
        if (r5 != false) goto L_0x0007;
     */
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass2KI(X.AnonymousClass2F9 r2, boolean r3, boolean r4, boolean r5) {
        /*
            r1 = this;
            r1.A01 = r2
            if (r4 == 0) goto L_0x0007
            r0 = 0
            if (r5 == 0) goto L_0x0008
        L_0x0007:
            r0 = 1
        L_0x0008:
            r1.<init>(r2, r3, r4, r0)
            r1.A00 = r5
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2KI.<init>(X.2F9, boolean, boolean, boolean):void");
    }

    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    @Override // X.AbstractC44161yL
    public void A08() {
        /*
        // Method dump skipped, instructions count: 1416
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2KI.A08():void");
    }
}
