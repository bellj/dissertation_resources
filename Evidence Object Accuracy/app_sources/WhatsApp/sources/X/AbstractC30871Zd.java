package X;

import android.text.TextUtils;
import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1Zd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC30871Zd extends AnonymousClass1ZY {
    public int A00;
    public int A01;
    public int A02 = 8;
    public int A03 = 0;
    public int A04 = -1;
    public long A05;
    public long A06 = -1;
    public AnonymousClass1ZR A07;
    public AnonymousClass1ZR A08;
    public Long A09;
    public String A0A;
    public String A0B;
    public String A0C;
    public String A0D;
    public String A0E;
    @Deprecated
    public String A0F = "ACTIVE";
    public String A0G;
    public String A0H;
    public String A0I = "ACTIVE";
    public String A0J;
    public String A0K;
    public String A0L;
    public String A0M;
    public String A0N;
    public String A0O;
    public boolean A0P;
    public boolean A0Q;
    public boolean A0R;
    public boolean A0S;
    public boolean A0T;
    @Deprecated
    public boolean A0U;
    public boolean A0V;
    public boolean A0W;
    public boolean A0X;
    @Deprecated
    public boolean A0Y;
    public boolean A0Z;
    public boolean A0a;

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0019 A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String A00(java.lang.String r2) {
        /*
            r1 = 0
            if (r2 == 0) goto L_0x000a
            int r0 = r2.hashCode()
            switch(r0) {
                case -891611359: goto L_0x0011;
                case 930084620: goto L_0x000e;
                case 1053567612: goto L_0x000b;
                default: goto L_0x000a;
            }
        L_0x000a:
            return r1
        L_0x000b:
            java.lang.String r0 = "DISABLED"
            goto L_0x0013
        L_0x000e:
            java.lang.String r0 = "REQUIRES_VERIFICATION"
            goto L_0x0013
        L_0x0011:
            java.lang.String r0 = "ENABLED"
        L_0x0013:
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x000a
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC30871Zd.A00(java.lang.String):java.lang.String");
    }

    public String A0B() {
        if (!this.A0Y || !"ACTIVE".equals(this.A0I)) {
            return "DISABLED";
        }
        String str = this.A0F;
        if (str == null || "ACTIVE".equals(str)) {
            return "ENABLED";
        }
        return "REQUIRES_VERIFICATION";
    }

    public JSONObject A0C() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("verified", this.A0a);
            AnonymousClass1ZR r2 = this.A08;
            if (!AnonymousClass1ZS.A03(r2)) {
                jSONObject.put("bankName", r2.A00);
            }
            AnonymousClass1ZR r22 = this.A07;
            if (!AnonymousClass1ZS.A03(r22)) {
                jSONObject.put("bankCode", r22.A00);
            }
            String str = this.A0B;
            if (str != null) {
                jSONObject.put("bankPhoneNumber", str);
            }
            String str2 = this.A0A;
            if (str2 != null) {
                jSONObject.put("bankLogoUrl", str2);
            }
            long j = this.A06;
            if (j >= 0) {
                jSONObject.put("timeLastAdded", j);
            }
            String str3 = this.A0O;
            if (str3 != null) {
                jSONObject.put("verificationType", str3);
            }
            if ("otp".equals(this.A0O)) {
                jSONObject.put("otpNumberMatch", this.A0Q);
            }
            int i = this.A02;
            if (i >= 0) {
                jSONObject.put("otpLength", i);
            }
            String str4 = this.A0I;
            if (str4 != null) {
                jSONObject.put("displayState", str4);
            }
            try {
                jSONObject.put("editable", this.A0P);
                jSONObject.put("verifiable", this.A0Z);
                jSONObject.put("p2pDefaultEligible", this.A0X);
                jSONObject.put("p2mDefaultEligible", this.A0T);
                jSONObject.put("p2pSend", this.A0N);
                jSONObject.put("p2pReceive", this.A0M);
                jSONObject.put("p2mSend", this.A0L);
                jSONObject.put("p2mReceive", this.A0K);
                return jSONObject;
            } catch (JSONException e) {
                StringBuilder sb = new StringBuilder("PAY: PaymentMethodCardCountryData/addCapabilitiesToJson threw: ");
                sb.append(e);
                Log.w(sb.toString());
                return jSONObject;
            }
        } catch (JSONException e2) {
            StringBuilder sb2 = new StringBuilder("PAY: PaymentMethodCardCountryData toJSONObject threw: ");
            sb2.append(e2);
            Log.w(sb2.toString());
            return jSONObject;
        }
    }

    public void A0D(JSONObject jSONObject) {
        String str;
        String str2;
        this.A0a = jSONObject.optBoolean("verified", false);
        this.A08 = AnonymousClass1ZS.A00(jSONObject.optString("bankName", null), "bankName");
        this.A07 = AnonymousClass1ZS.A00(jSONObject.optString("bankCode", null), "bankCode");
        this.A0B = jSONObject.optString("bankPhoneNumber", null);
        this.A0A = jSONObject.optString("bankLogoUrl", this.A0A);
        this.A06 = jSONObject.optLong("timeLastAdded", -1);
        this.A0O = jSONObject.optString("verificationType", null);
        this.A0Q = jSONObject.optBoolean("otpNumberMatch", false);
        this.A02 = jSONObject.optInt("otpLength", 8);
        String optString = jSONObject.optString("displayState", null);
        if (TextUtils.isEmpty(optString)) {
            optString = "ACTIVE";
        }
        this.A0I = optString;
        this.A0P = jSONObject.optBoolean("editable", false);
        this.A0Z = jSONObject.optBoolean("verifiable", false);
        String str3 = "p2pDefaultEligible";
        if (!jSONObject.has(str3)) {
            str3 = "defaultEligible";
        }
        this.A0X = jSONObject.optBoolean(str3, false);
        this.A0T = jSONObject.optBoolean("p2mDefaultEligible", false);
        if (!this.A0Y || !"ACTIVE".equals(this.A0I)) {
            str = "DISABLED";
        } else {
            str = this.A0a ? "ENABLED" : "REQUIRES_VERIFICATION";
        }
        this.A0N = jSONObject.optString("p2pSend", str);
        this.A0M = jSONObject.optString("p2pReceive", A0B());
        if (!this.A0U || !"ACTIVE".equals(this.A0I)) {
            str2 = "DISABLED";
        } else {
            str2 = this.A0a ? "ENABLED" : "REQUIRES_VERIFICATION";
        }
        this.A0L = jSONObject.optString("p2mSend", str2);
        this.A0K = jSONObject.optString("p2mReceive", "DISABLED");
    }
}
