package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.2NE  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2NE implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C100124lP();
    public final C29211Rh A00;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass2NE(C29211Rh r1) {
        this.A00 = r1;
    }

    public AnonymousClass2NE(Parcel parcel) {
        this.A00 = new C29211Rh(parcel.createByteArray(), parcel.createByteArray(), parcel.createByteArray());
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        C29211Rh r1 = this.A00;
        parcel.writeByteArray(r1.A01);
        parcel.writeByteArray(r1.A00);
        parcel.writeByteArray(r1.A02);
    }
}
