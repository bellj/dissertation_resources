package X;

import android.graphics.drawable.Icon;
import android.net.Uri;

/* renamed from: X.0Kf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C04060Kf {
    public static Icon A00(Uri uri) {
        return Icon.createWithAdaptiveBitmapContentUri(uri);
    }
}
