package X;

import com.facebook.redex.IDxComparatorShape3S0000000_2_I1;
import java.util.ArrayList;
import java.util.Comparator;

/* renamed from: X.3Gm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64683Gm {
    public static final Comparator A07 = new IDxComparatorShape3S0000000_2_I1(8);
    public static final Comparator A08 = new IDxComparatorShape3S0000000_2_I1(9);
    public int A00 = -1;
    public int A01;
    public int A02;
    public int A03;
    public final int A04 = 2000;
    public final ArrayList A05 = C12960it.A0l();
    public final AnonymousClass4PE[] A06 = new AnonymousClass4PE[5];
}
