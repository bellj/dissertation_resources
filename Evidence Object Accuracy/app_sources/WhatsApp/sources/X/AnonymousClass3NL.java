package X;

import android.graphics.Path;
import android.os.Build;
import android.view.ViewTreeObserver;
import com.whatsapp.WaRoundCornerImageView;

/* renamed from: X.3NL  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3NL implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ WaRoundCornerImageView A00;

    public /* synthetic */ AnonymousClass3NL(WaRoundCornerImageView waRoundCornerImageView) {
        this.A00 = waRoundCornerImageView;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        if (Build.VERSION.SDK_INT >= 21) {
            WaRoundCornerImageView waRoundCornerImageView = this.A00;
            Path path = new Path();
            waRoundCornerImageView.A01 = path;
            float f = waRoundCornerImageView.A00;
            path.addRoundRect((float) (waRoundCornerImageView.getLeft() + waRoundCornerImageView.getPaddingLeft()), (float) (waRoundCornerImageView.getTop() + waRoundCornerImageView.getPaddingTop()), (float) (waRoundCornerImageView.getRight() - waRoundCornerImageView.getPaddingRight()), (float) (waRoundCornerImageView.getBottom() - waRoundCornerImageView.getPaddingBottom()), f, f, Path.Direction.CCW);
        }
    }
}
