package X;

/* renamed from: X.2i9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C55292i9 extends AnonymousClass0FK {
    public final C15610nY A00;

    public C55292i9(AnonymousClass02M r1, C15610nY r2) {
        super(r1);
        this.A00 = r2;
    }

    @Override // X.AnonymousClass0Z4
    public boolean A01(Object obj, Object obj2) {
        return C29941Vi.A00(obj, obj2);
    }

    @Override // X.AnonymousClass0Z4
    public boolean A02(Object obj, Object obj2) {
        return C29941Vi.A00(obj, obj2);
    }

    @Override // X.AnonymousClass0Z4, java.util.Comparator
    public /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
        int i;
        int i2;
        C15370n3 r4 = (C15370n3) obj;
        C15370n3 r5 = (C15370n3) obj2;
        C29951Vj r0 = r4.A0F;
        if (r0 != null) {
            i = r0.A00;
        } else {
            i = 0;
        }
        C29951Vj r02 = r5.A0F;
        if (r02 != null) {
            i2 = r02.A00;
        } else {
            i2 = 0;
        }
        if (i != i2) {
            return i == 3 ? -1 : 1;
        }
        C15610nY r03 = this.A00;
        String A04 = r03.A04(r4);
        String A042 = r03.A04(r5);
        if (A04 == null || A042 == null) {
            return 0;
        }
        return A04.compareTo(A042);
    }
}
