package X;

/* renamed from: X.38S  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass38S extends AbstractC16350or {
    public final C14900mE A00;
    public final C253318z A01;
    public final AnonymousClass4KP A02;
    public final C17220qS A03;
    public final String A04;
    public final String A05;

    public AnonymousClass38S(C14900mE r1, C253318z r2, AnonymousClass4KP r3, C17220qS r4, String str, String str2) {
        this.A00 = r1;
        this.A03 = r4;
        this.A01 = r2;
        this.A05 = str;
        this.A04 = str2;
        this.A02 = r3;
    }
}
