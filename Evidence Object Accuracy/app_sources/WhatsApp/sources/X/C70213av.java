package X;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.conversation.conversationrow.ConversationRowVideo$RowVideoView;

/* renamed from: X.3av  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70213av implements AbstractC41521tf {
    public final /* synthetic */ C60622yK A00;

    public C70213av(C60622yK r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC41521tf
    public int AGm() {
        return AnonymousClass3GD.A02(this.A00);
    }

    @Override // X.AbstractC41521tf
    public void AQV() {
        this.A00.A1O();
    }

    @Override // X.AbstractC41521tf
    public void Adg(Bitmap bitmap, View view, AbstractC15340mz r7) {
        C60622yK r0 = this.A00;
        ConversationRowVideo$RowVideoView conversationRowVideo$RowVideoView = r0.A08;
        if (bitmap != null) {
            conversationRowVideo$RowVideoView.setImageDrawable(new BitmapDrawable(C12960it.A09(r0), bitmap));
            conversationRowVideo$RowVideoView.A02(bitmap.getWidth(), bitmap.getHeight(), false);
            return;
        }
        conversationRowVideo$RowVideoView.setImageDrawable(C12980iv.A0L(r0.getContext(), R.color.dark_gray));
    }

    @Override // X.AbstractC41521tf
    public void Adu(View view) {
        this.A00.A08.setBackgroundColor(-7829368);
    }
}
