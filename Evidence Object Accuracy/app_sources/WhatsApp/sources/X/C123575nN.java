package X;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.view.View;
import com.facebook.redex.IDxCListenerShape2S0200000_3_I1;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/* renamed from: X.5nN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123575nN extends C118185bP {
    public static final SimpleDateFormat A0G = new SimpleDateFormat("MMM d, yyyy", Locale.US);
    public String A00;
    public boolean A01;
    public final C15450nH A02;
    public final C14830m7 A03;
    public final C14850m9 A04;
    public final AnonymousClass68Z A05;
    public final C1308460e A06;
    public final C1329668y A07;
    public final C243515e A08;
    public final C120565gO A09;
    public final C120485gG A0A;
    public final C120425gA A0B;
    public final AnonymousClass1A8 A0C;
    public final AnonymousClass69E A0D;
    public final C1310060v A0E;
    public final C22980zx A0F;

    public C123575nN(Bundle bundle, C14900mE r46, C15570nT r47, C15450nH r48, C14650lo r49, AnonymousClass1AA r50, C238013b r51, C15550nR r52, AnonymousClass01d r53, C14830m7 r54, C16590pI r55, AnonymousClass018 r56, C15650ng r57, C20300vX r58, C20370ve r59, AnonymousClass102 r60, C241414j r61, C14850m9 r62, C17220qS r63, C129925yW r64, AnonymousClass68Z r65, C1308460e r66, C1329668y r67, C20380vf r68, C21860y6 r69, C18650sn r70, C243515e r71, C18610sj r72, C22710zW r73, C17070qD r74, AnonymousClass1A7 r75, AbstractC16870pt r76, AnonymousClass1A8 r77, AnonymousClass17Z r78, AnonymousClass69E r79, C121265hX r80, AnonymousClass604 r81, C1310060v r82, C18590sh r83, AnonymousClass14X r84, C22980zx r85, AbstractC14440lR r86) {
        super(bundle, r46, r47, r49, r50, r51, r52, r53, r54, r55, r56, r57, r58, r59, r60, r61, r64, r68, r69, r71, r73, r74, r75, r76, r78, r81, r84, r86);
        this.A03 = r54;
        this.A02 = r48;
        this.A04 = r62;
        this.A0E = r82;
        this.A0D = r79;
        this.A0F = r85;
        this.A06 = r66;
        this.A05 = r65;
        this.A07 = r67;
        this.A08 = r71;
        this.A0C = r77;
        Context context = r55.A00;
        this.A09 = new C120565gO(context, r46, r70, new C64513Fv(), r72, null, r83);
        this.A0A = new C120485gG(context, r46, r60, r63, r65, r66, r70, r72, r80, r83);
        this.A0B = new C120425gA(r46, r55, r66, r70, r72, r74, r85, r86);
        if (bundle != null) {
            this.A01 = bundle.getBoolean("extra_return_after_completion");
            this.A00 = bundle.getString("referral_screen", null);
        }
    }

    public static void A02(C118185bP r10, List list) {
        SpannableStringBuilder spannableStringBuilder;
        String str;
        Context context;
        int i;
        Object[] objArr;
        AnonymousClass1IR r7 = r10.A06.A01;
        C30821Yy r3 = r7.A08;
        if (r3 != null) {
            spannableStringBuilder = AnonymousClass14X.A02(r10.A0O.A00, r10.A0P, r7.A00(), r3);
        } else {
            spannableStringBuilder = new SpannableStringBuilder();
        }
        C30921Zi A01 = r7.A01();
        C17070qD r6 = r10.A0a;
        boolean Adb = r6.A02().AF5().Adb(r7);
        AnonymousClass14X r2 = r10.A0g;
        list.add(new C123245mq(A01, spannableStringBuilder, r2.A0D(r7, spannableStringBuilder), Adb));
        C123135mf r5 = new C123135mf();
        AnonymousClass1IR r4 = r10.A06.A01;
        if (r4.A0F()) {
            int i2 = r4.A02;
            if (i2 == 12 || i2 == 11) {
                str = (String) r2.A0A(r4).second;
            }
            str = null;
        } else {
            String A0M = r2.A0M(r4);
            String A0P = r2.A0P(r4);
            int i3 = r4.A02;
            if (i3 == 406 || i3 == 407) {
                context = r10.A0O.A00;
                i = R.string.transaction_short_status_sent_failed;
                objArr = new Object[]{A0M};
            } else {
                if (i3 == 102 && r10.A0H.A0F(r4.A0D)) {
                    context = r10.A0O.A00;
                    i = R.string.transaction_short_status_sent_pending_received_by_you;
                    objArr = new Object[]{A0P};
                }
                str = null;
            }
            str = context.getString(i, objArr);
        }
        if (!AnonymousClass14X.A07(r4) && !TextUtils.isEmpty(str)) {
            r5.A01 = str;
            r5.A00 = 0;
            list.add(r5);
        }
        C123205mm r1 = new C123205mm();
        r1.A02 = r10.A06;
        r1.A01 = r10;
        r1.A00 = r6.A02().ACH();
        list.add(r1);
        r10.A0I(list);
        r10.A0J(list);
    }

    public static void A03(List list) {
        list.add(new C122995mR());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0011, code lost:
        if (r0 == false) goto L_0x0013;
     */
    @Override // X.C118185bP
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A06(X.AnonymousClass1IR r3) {
        /*
            r2 = this;
            X.1Zf r1 = r3.A0A
            X.5fB r1 = (X.C119835fB) r1
            com.whatsapp.jid.UserJid r0 = r3.A0E
            if (r0 != 0) goto L_0x0013
            if (r1 == 0) goto L_0x0013
            java.lang.String r0 = r1.A0L
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            r1 = 1
            if (r0 != 0) goto L_0x0014
        L_0x0013:
            r1 = 0
        L_0x0014:
            boolean r0 = r3.A0F()
            if (r0 == 0) goto L_0x002a
            if (r1 == 0) goto L_0x002a
            java.lang.String r0 = r3.A0F
            boolean r0 = X.C31001Zq.A09(r0)
            if (r0 == 0) goto L_0x0027
            java.lang.String r0 = r3.A0F
            return r0
        L_0x0027:
            java.lang.String r0 = r3.A0K
            return r0
        L_0x002a:
            java.lang.String r0 = super.A06(r3)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C123575nN.A06(X.1IR):java.lang.String");
    }

    @Override // X.C118185bP
    public void A07() {
        String str;
        String str2;
        C30931Zj r2 = this.A0d;
        r2.A06("IN- HANDLE_SEND_AGAIN start");
        C127915vG r0 = super.A06;
        if (r0 != null) {
            AnonymousClass1IR r02 = r0.A01;
            if (r02 != null) {
                boolean z = r02.A0P;
                if (z) {
                    r2.A06("IN- HANDLE_SEND_AGAIN transaction is not null and it's interop");
                    AbstractC30891Zf r1 = super.A06.A01.A0A;
                    if (r1 instanceof C119835fB) {
                        str2 = ((C119835fB) r1).A0J;
                    } else {
                        str2 = "";
                    }
                    if (C1309360o.A00(str2)) {
                        AnonymousClass1ZR A0E = C117295Zj.A0E(str2);
                        if (!this.A05.AJE(A0E)) {
                            A0O(true);
                            this.A0A.A00(A0E, null, new AnonymousClass6Ln(A0E, this, str2) { // from class: X.693
                                public final /* synthetic */ AnonymousClass1ZR A00;
                                public final /* synthetic */ C123575nN A01;
                                public final /* synthetic */ String A02;

                                {
                                    this.A01 = r2;
                                    this.A00 = r1;
                                    this.A02 = r3;
                                }

                                @Override // X.AnonymousClass6Ln
                                public final void AVN(UserJid userJid, AnonymousClass1ZR r7, AnonymousClass1ZR r8, AnonymousClass1ZR r9, C452120p r10, String str3, String str4, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6) {
                                    C128315vu A00;
                                    C123575nN r3 = this.A01;
                                    AnonymousClass1ZR r4 = this.A00;
                                    String str5 = this.A02;
                                    C30931Zj r12 = r3.A0d;
                                    r12.A06("IN- HANDLE_SEND_AGAIN vpa valid check response");
                                    r3.A0O(false);
                                    if (!z2 || r10 != null) {
                                        if (!z4) {
                                            if (r10 != null) {
                                                r12.A06("IN- HANDLE_SEND_AGAIN error from server");
                                                A00 = C128315vu.A00(8);
                                                AnonymousClass60V A04 = r3.A0D.A04(r3.A06.A04, r10.A00);
                                                Context context = r3.A0O.A00;
                                                String A01 = A04.A01(context);
                                                if (TextUtils.isEmpty(A01)) {
                                                    A01 = context.getString(R.string.payment_invalid_vpa_error_text);
                                                }
                                                A00.A0B = A01;
                                                C118185bP.A01(r3, A00);
                                            }
                                            r12.A05("Unable to validate the receiver to send payment again");
                                            return;
                                        }
                                    } else if (!z4) {
                                        r12.A06("IN- HANDLE_SEND_AGAIN starting payment");
                                        C123535nJ r03 = new C123535nJ(105);
                                        r03.A00 = r4;
                                        r03.A0D = str3;
                                        r03.A07 = r7;
                                        C118185bP.A01(r3, r03);
                                        return;
                                    }
                                    r12.A06("IN- HANDLE_SEND_AGAIN server said user blocked");
                                    A00 = C128315vu.A00(13);
                                    A00.A06 = userJid;
                                    A00.A0E = str5;
                                    C118185bP.A01(r3, A00);
                                }
                            });
                            return;
                        }
                        r2.A06("IN- HANDLE_SEND_AGAIN user blocked checked locally");
                        C128315vu A00 = C128315vu.A00(13);
                        A00.A0E = str2;
                        C118185bP.A01(this, A00);
                        return;
                    }
                    r2.A06("IN- HANDLE_SEND_AGAIN vpa valid check locally, incorrect vpa");
                    C128315vu A002 = C128315vu.A00(8);
                    A002.A0B = this.A0O.A00.getString(R.string.payment_invalid_vpa_error_text);
                    C118185bP.A01(this, A002);
                    return;
                }
                StringBuilder A0k = C12960it.A0k("IN- HANDLE_SEND_AGAIN isInterop is ");
                A0k.append(z);
                str = A0k.toString();
            } else {
                str = "IN- HANDLE_SEND_AGAIN transactionInfo is null?";
            }
        } else {
            str = "IN- HANDLE_SEND_AGAIN transactionDetailData is null?";
        }
        r2.A06(str);
        r2.A06("IN- HANDLE_SEND_AGAIN calling super");
        super.A07();
    }

    @Override // X.C118185bP
    public void A0A() {
        AnonymousClass1IR r0;
        C119835fB r02;
        C127915vG r03 = super.A06;
        if (!(r03 == null || (r0 = r03.A01) == null || (r02 = (C119835fB) r0.A0A) == null || r02.A0B == null)) {
            ((C118185bP) this).A01 = R.string.upi_mandate_transaction_details_title;
        }
        super.A0A();
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0022  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0054  */
    @Override // X.C118185bP
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0D(X.C123325my r7) {
        /*
            r6 = this;
            boolean r0 = r6.A0V()
            if (r0 != 0) goto L_0x000a
            super.A0D(r7)
            return
        L_0x000a:
            X.5vG r0 = r6.A06
            X.1IR r5 = r0.A01
            int r0 = r5.A02
            switch(r0) {
                case 11: goto L_0x0022;
                case 12: goto L_0x0022;
                case 13: goto L_0x0054;
                case 14: goto L_0x0054;
                case 15: goto L_0x0022;
                case 16: goto L_0x0022;
                case 17: goto L_0x0051;
                case 18: goto L_0x0022;
                case 19: goto L_0x0022;
                case 20: goto L_0x0022;
                default: goto L_0x0013;
            }
        L_0x0013:
            switch(r0) {
                case 101: goto L_0x0022;
                case 102: goto L_0x0022;
                case 103: goto L_0x0022;
                case 104: goto L_0x0022;
                case 105: goto L_0x0054;
                case 106: goto L_0x0051;
                case 107: goto L_0x0054;
                case 108: goto L_0x0054;
                case 109: goto L_0x0022;
                case 110: goto L_0x0054;
                case 111: goto L_0x0054;
                case 112: goto L_0x0022;
                default: goto L_0x0016;
            }
        L_0x0016:
            switch(r0) {
                case 401: goto L_0x0022;
                case 402: goto L_0x0022;
                case 403: goto L_0x0022;
                case 404: goto L_0x0054;
                case 405: goto L_0x0051;
                case 406: goto L_0x0054;
                case 407: goto L_0x0054;
                case 408: goto L_0x0054;
                case 409: goto L_0x0054;
                case 410: goto L_0x0022;
                case 411: goto L_0x0054;
                case 412: goto L_0x0054;
                case 413: goto L_0x0054;
                case 414: goto L_0x0054;
                case 415: goto L_0x0022;
                case 416: goto L_0x0054;
                case 417: goto L_0x0022;
                case 418: goto L_0x0022;
                default: goto L_0x0019;
            }
        L_0x0019:
            switch(r0) {
                case 420: goto L_0x0022;
                case 421: goto L_0x0022;
                case 422: goto L_0x0054;
                case 423: goto L_0x0054;
                case 424: goto L_0x0054;
                default: goto L_0x001c;
            }
        L_0x001c:
            switch(r0) {
                case 601: goto L_0x0022;
                case 602: goto L_0x0022;
                case 603: goto L_0x0022;
                case 604: goto L_0x0051;
                case 605: goto L_0x0054;
                case 606: goto L_0x0022;
                case 607: goto L_0x0022;
                case 608: goto L_0x0022;
                default: goto L_0x001f;
            }
        L_0x001f:
            switch(r0) {
                case 703: goto L_0x0051;
                case 704: goto L_0x0054;
                default: goto L_0x0022;
            }
        L_0x0022:
            X.49T r4 = X.AnonymousClass49T.A01
        L_0x0024:
            X.0pI r1 = r6.A0O
            java.lang.String r0 = r4.iconType
            android.text.SpannableStringBuilder r3 = new android.text.SpannableStringBuilder
            r3.<init>(r0)
            android.content.Context r0 = r1.A00
            android.graphics.Typeface r0 = X.AnonymousClass00X.A02(r0)
            if (r0 == 0) goto L_0x0044
            X.1Z3 r2 = new X.1Z3
            r2.<init>(r0)
            java.lang.String r0 = r4.iconType
            int r1 = r0.length()
            r0 = 0
            r3.setSpan(r2, r0, r1, r0)
        L_0x0044:
            r7.A03 = r3
            int r0 = X.AnonymousClass14X.A01(r5)
            r7.A01 = r0
            r0 = 1106247680(0x41f00000, float:30.0)
            r7.A00 = r0
            return
        L_0x0051:
            X.49T r4 = X.AnonymousClass49T.A02
            goto L_0x0024
        L_0x0054:
            X.49T r4 = X.AnonymousClass49T.A00
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C123575nN.A0D(X.5my):void");
    }

    @Override // X.C118185bP
    public void A0E(C126075sI r3) {
        C127915vG r0;
        AnonymousClass1IR r02;
        C123535nJ r1;
        int i = r3.A00;
        if (i == 300) {
            A0P(false);
        } else if (i != 301) {
            super.A0E(r3);
        } else {
            if (this.A01) {
                C123535nJ r12 = new C123535nJ(101);
                r12.A02 = super.A07.A01;
                r12.A05 = super.A0C;
                r12.A04 = "SUBMITTED";
                r12.A03 = "00";
                r1 = r12;
            } else {
                String str = this.A00;
                if (("chat".equals(str) || "payment_composer_icon".equals(str)) && (r0 = super.A06) != null && (r02 = r0.A01) != null && r02.A0P) {
                    r1 = new C123535nJ(106);
                } else {
                    r1 = C128315vu.A00(19);
                }
            }
            C118185bP.A01(this, r1);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:131:0x03d5, code lost:
        if (r1 == 6) goto L_0x03d7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0178, code lost:
        if (r1 == 418) goto L_0x017a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x01f0, code lost:
        if (r1 != 418) goto L_0x01f2;
     */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x0321  */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x0381  */
    /* JADX WARNING: Removed duplicated region for block: B:186:0x0583  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x01e0  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0211  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x026e  */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x02b6  */
    @Override // X.C118185bP
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0G(java.util.List r22) {
        /*
        // Method dump skipped, instructions count: 1673
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C123575nN.A0G(java.util.List):void");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0194, code lost:
        if (r0 == false) goto L_0x0196;
     */
    /* JADX WARNING: Removed duplicated region for block: B:100:0x028b  */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x02f5  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x0241  */
    @Override // X.C118185bP
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0L(java.util.List r18) {
        /*
        // Method dump skipped, instructions count: 782
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C123575nN.A0L(java.util.List):void");
    }

    @Override // X.C118185bP
    public boolean A0Q(AnonymousClass1IR r3) {
        C119835fB r1 = (C119835fB) r3.A0A;
        if (r1 == null || (TextUtils.isEmpty(r1.A0G) && r1.A0B == null)) {
            return super.A0Q(r3);
        }
        return false;
    }

    public final void A0S() {
        A0O(true);
        AnonymousClass1A8 r3 = this.A0C;
        String str = super.A06.A01.A0K;
        AnonymousClass6BY r1 = new AnonymousClass5UV() { // from class: X.6BY
            @Override // X.AnonymousClass5UV
            public final void AOr(String str2) {
                C123575nN r32 = C123575nN.this;
                ArrayList A0l = C12960it.A0l();
                A0l.add(new C123025mU(C117305Zk.A0A(r32, 152)));
                ((C118185bP) r32).A02.A0B(A0l);
                r32.A0O(false);
            }
        };
        if (!TextUtils.isEmpty(str)) {
            ArrayList A0l = C12960it.A0l();
            A0l.add(str);
            r3.A02(r1, A0l);
        }
    }

    public final void A0T(C119835fB r4, List list, int i) {
        AnonymousClass60R r0;
        if (i != 401 && i != 20 && (r0 = r4.A0B) != null && !AnonymousClass1ZS.A02(r0.A08)) {
            C123275mt r2 = new C123275mt();
            r2.A04 = this.A0O.A00.getString(R.string.upi_mandate_payment_transaction_detail_umn_row_title);
            r2.A03 = (String) C117295Zj.A0R(r4.A0B.A08);
            r2.A02 = new View.OnLongClickListener(r4, this) { // from class: X.64q
                public final /* synthetic */ C119835fB A00;
                public final /* synthetic */ C123575nN A01;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // android.view.View.OnLongClickListener
                public final boolean onLongClick(View view) {
                    Object obj;
                    C123575nN r42 = this.A01;
                    C119835fB r3 = this.A00;
                    ClipboardManager A0B = r42.A0M.A0B();
                    if (A0B == null) {
                        r42.A0G.A07(R.string.view_contact_unsupport, 0);
                        return true;
                    }
                    try {
                        AnonymousClass1ZR r02 = r3.A0B.A08;
                        if (r02 == null) {
                            obj = null;
                        } else {
                            obj = r02.A00;
                        }
                        CharSequence charSequence = (CharSequence) obj;
                        A0B.setPrimaryClip(ClipData.newPlainText(charSequence, charSequence));
                        r42.A0G.A07(R.string.upi_mandate_number_copied, 0);
                        return true;
                    } catch (NullPointerException | SecurityException unused) {
                        r42.A0G.A07(R.string.view_contact_unsupport, 0);
                        return true;
                    }
                }
            };
            list.add(r2);
        }
    }

    public final void A0U(List list) {
        C119835fB r3 = (C119835fB) super.A06.A01.A0A;
        if (this.A02.A05(AbstractC15460nI.A0y) && !TextUtils.isEmpty(r3.A0Q)) {
            C123275mt r2 = new C123275mt();
            Context context = this.A0O.A00;
            r2.A04 = context.getString(R.string.upi_url_reference_section_title);
            r2.A03 = context.getString(R.string.upi_url_reference_section_description);
            r2.A01 = new IDxCListenerShape2S0200000_3_I1(this, 44, r3);
            list.add(r2);
        }
    }

    public final boolean A0V() {
        C127915vG r0;
        AnonymousClass1IR r2;
        AbstractC30891Zf r1;
        AnonymousClass60R r02;
        Boolean A02;
        if (!this.A0Z.A03.A07(1358) || (r0 = super.A06) == null || (r2 = r0.A01) == null || (r1 = r2.A0A) == null || !(r1 instanceof C119835fB) || (((r02 = ((C119835fB) r1).A0B) != null && r02.A0K) || (A02 = r2.A02()) == null || !A02.booleanValue())) {
            return false;
        }
        return true;
    }
}
