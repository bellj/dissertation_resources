package X;

import com.facebook.common.time.RealtimeSinceBootClock;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/* renamed from: X.4Zu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C93274Zu {
    public final AbstractC12250hc A00;
    public final ExecutorService A01 = Executors.newFixedThreadPool(3);

    public C93274Zu() {
        RealtimeSinceBootClock realtimeSinceBootClock = RealtimeSinceBootClock.A00;
        this.A00 = realtimeSinceBootClock;
    }

    public C93274Zu(int i) {
        RealtimeSinceBootClock realtimeSinceBootClock = RealtimeSinceBootClock.A00;
        this.A00 = realtimeSinceBootClock;
    }
}
