package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.3jv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75483jv extends AnonymousClass03U {
    public final TextView A00;
    public final AnonymousClass018 A01;

    public C75483jv(View view, AnonymousClass018 r3) {
        super(view);
        this.A01 = r3;
        this.A00 = C12960it.A0I(view, R.id.storage_usage_chat_footer_text);
    }
}
