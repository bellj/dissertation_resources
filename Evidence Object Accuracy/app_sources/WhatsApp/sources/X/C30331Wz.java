package X;

/* renamed from: X.1Wz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30331Wz extends AbstractC15340mz implements AbstractC16400ox, AbstractC16420oz {
    public long A00;
    public String A01;

    public C30331Wz(AnonymousClass1IS r3, byte b, int i, long j) {
        super(r3, b, j);
        super.A01 = i;
        this.A00 = j;
        synchronized (this.A10) {
            this.A02 = 0;
        }
    }

    public C30331Wz(AnonymousClass1IS r9, C30331Wz r10, long j) {
        super(r10, r9, j, true);
        this.A01 = r10.A01;
        this.A00 = r10.A00;
    }

    public void A14(AnonymousClass1G9 r3) {
        AnonymousClass1IS r1 = this.A0z;
        r3.A07(C15380n4.A03(r1.A00));
        r3.A08(r1.A02);
        String str = this.A01;
        if (str == null) {
            str = r1.A01;
        }
        r3.A05(str);
    }

    @Override // X.AbstractC16420oz
    public void A6k(C39971qq r5) {
        AnonymousClass1G3 r3 = r5.A03;
        C57692nT r0 = ((C27081Fy) r3.A00).A0W;
        if (r0 == null) {
            r0 = C57692nT.A0D;
        }
        C56882m6 r2 = (C56882m6) r0.A0T();
        AnonymousClass1G8 r02 = ((C57692nT) r2.A00).A0C;
        if (r02 == null) {
            r02 = AnonymousClass1G8.A05;
        }
        AnonymousClass1G9 r03 = (AnonymousClass1G9) r02.A0T();
        A14(r03);
        r2.A03();
        C57692nT r1 = (C57692nT) r2.A00;
        r1.A0C = (AnonymousClass1G8) r03.A02();
        r1.A00 |= 1;
        r2.A05(EnumC87324Bb.A0B);
        r3.A08(r2);
    }

    @Override // X.AbstractC16400ox
    public /* bridge */ /* synthetic */ AbstractC15340mz A7M(AnonymousClass1IS r5) {
        if (!(this instanceof C30321Wy)) {
            return new C30331Wz(r5, this, this.A0I);
        }
        C30321Wy r3 = (C30321Wy) this;
        return new C30321Wy(r5, r3, r3.A0I);
    }
}
