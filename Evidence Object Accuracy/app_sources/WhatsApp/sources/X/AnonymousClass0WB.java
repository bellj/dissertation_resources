package X;

import android.animation.Animator;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.0WB  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0WB implements View.OnAttachStateChangeListener, ViewTreeObserver.OnPreDrawListener {
    public ViewGroup A00;
    public AnonymousClass072 A01;

    @Override // android.view.View.OnAttachStateChangeListener
    public void onViewAttachedToWindow(View view) {
    }

    public AnonymousClass0WB(ViewGroup viewGroup, AnonymousClass072 r2) {
        this.A01 = r2;
        this.A00 = viewGroup;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        C04960Ns r9;
        C05350Pf r2;
        View view;
        View view2;
        View view3;
        View view4;
        ViewGroup viewGroup = this.A00;
        viewGroup.getViewTreeObserver().removeOnPreDrawListener(this);
        viewGroup.removeOnAttachStateChangeListener(this);
        if (AnonymousClass073.A02.remove(viewGroup)) {
            AnonymousClass00N A00 = AnonymousClass073.A00();
            AbstractCollection abstractCollection = (AbstractCollection) A00.get(viewGroup);
            ArrayList arrayList = null;
            if (abstractCollection == null) {
                abstractCollection = new ArrayList();
                A00.put(viewGroup, abstractCollection);
            } else if (abstractCollection.size() > 0) {
                arrayList = new ArrayList(abstractCollection);
            }
            AnonymousClass072 r4 = this.A01;
            abstractCollection.add(r4);
            r4.A08(new AnonymousClass0G2(A00, this));
            r4.A0K(viewGroup, false);
            if (arrayList != null) {
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    ((AnonymousClass072) it.next()).A0H(viewGroup);
                }
            }
            r4.A0F = new ArrayList();
            r4.A0D = new ArrayList();
            C04830Nf r10 = r4.A09;
            C04830Nf r92 = r4.A08;
            AnonymousClass00N r8 = new AnonymousClass00N(r10.A02);
            AnonymousClass00N r3 = new AnonymousClass00N(r92.A02);
            int i = 0;
            while (true) {
                int[] iArr = r4.A0K;
                if (i >= iArr.length) {
                    break;
                }
                int i2 = iArr[i];
                if (i2 == 1) {
                    int size = r8.size();
                    while (true) {
                        size--;
                        if (size >= 0) {
                            View view5 = (View) r8.A02[size << 1];
                            if (!(view5 == null || !r4.A0P(view5) || (r2 = (C05350Pf) r3.remove(view5)) == null || (view = r2.A00) == null || !r4.A0P(view))) {
                                r4.A0F.add(r8.A06(size));
                                r4.A0D.add(r2);
                            }
                        }
                    }
                } else if (i2 == 2) {
                    AnonymousClass00N r12 = r10.A01;
                    AnonymousClass00N r0 = r92.A01;
                    int size2 = r12.size();
                    for (int i3 = 0; i3 < size2; i3++) {
                        View view6 = (View) r12.A02[(i3 << 1) + 1];
                        if (view6 != null && r4.A0P(view6) && (view2 = (View) r0.get(r12.A02[i3 << 1])) != null && r4.A0P(view2)) {
                            Object obj = r8.get(view6);
                            Object obj2 = r3.get(view2);
                            if (!(obj == null || obj2 == null)) {
                                r4.A0F.add(obj);
                                r4.A0D.add(obj2);
                                r8.remove(view6);
                                r3.remove(view2);
                            }
                        }
                    }
                } else if (i2 == 3) {
                    SparseArray sparseArray = r10.A00;
                    SparseArray sparseArray2 = r92.A00;
                    int size3 = sparseArray.size();
                    for (int i4 = 0; i4 < size3; i4++) {
                        View view7 = (View) sparseArray.valueAt(i4);
                        if (view7 != null && r4.A0P(view7) && (view3 = (View) sparseArray2.get(sparseArray.keyAt(i4))) != null && r4.A0P(view3)) {
                            Object obj3 = r8.get(view7);
                            Object obj4 = r3.get(view3);
                            if (!(obj3 == null || obj4 == null)) {
                                r4.A0F.add(obj3);
                                r4.A0D.add(obj4);
                                r8.remove(view7);
                                r3.remove(view3);
                            }
                        }
                    }
                } else if (i2 == 4) {
                    AnonymousClass036 r13 = r10.A03;
                    AnonymousClass036 r02 = r92.A03;
                    int A002 = r13.A00();
                    for (int i5 = 0; i5 < A002; i5++) {
                        View view8 = (View) r13.A03(i5);
                        if (view8 != null && r4.A0P(view8) && (view4 = (View) r02.A04(r13.A01(i5), null)) != null && r4.A0P(view4)) {
                            Object obj5 = r8.get(view8);
                            Object obj6 = r3.get(view4);
                            if (!(obj5 == null || obj6 == null)) {
                                r4.A0F.add(obj5);
                                r4.A0D.add(obj6);
                                r8.remove(view8);
                                r3.remove(view4);
                            }
                        }
                    }
                }
                i++;
            }
            for (int i6 = 0; i6 < r8.size(); i6++) {
                C05350Pf r1 = (C05350Pf) r8.A02[(i6 << 1) + 1];
                if (r4.A0P(r1.A00)) {
                    r4.A0F.add(r1);
                    r4.A0D.add(null);
                }
            }
            for (int i7 = 0; i7 < r3.size(); i7++) {
                C05350Pf r14 = (C05350Pf) r3.A02[(i7 << 1) + 1];
                if (r4.A0P(r14.A00)) {
                    r4.A0D.add(r14);
                    r4.A0F.add(null);
                }
            }
            AnonymousClass00N A01 = AnonymousClass072.A01();
            int size4 = A01.size();
            AbstractC11380gC A003 = AnonymousClass0U3.A00(viewGroup);
            for (int i8 = size4 - 1; i8 >= 0; i8--) {
                Animator animator = (Animator) A01.A02[i8 << 1];
                if (!(animator == null || (r9 = (C04960Ns) A01.get(animator)) == null || r9.A00 == null || !A003.equals(r9.A03))) {
                    C05350Pf r22 = r9.A02;
                    View view9 = r9.A00;
                    C05350Pf A0B = r4.A0B(view9, true);
                    C05350Pf A0A = r4.A0A(view9, true);
                    if (!(A0B == null && A0A == null) && r9.A01.A0R(r22, A0A)) {
                        if (animator.isRunning() || animator.isStarted()) {
                            animator.cancel();
                        } else {
                            A01.remove(animator);
                        }
                    }
                }
            }
            r4.A0J(viewGroup, r4.A09, r4.A08, r4.A0F, r4.A0D);
            r4.A0E();
        }
        return true;
    }

    @Override // android.view.View.OnAttachStateChangeListener
    public void onViewDetachedFromWindow(View view) {
        ViewGroup viewGroup = this.A00;
        viewGroup.getViewTreeObserver().removeOnPreDrawListener(this);
        viewGroup.removeOnAttachStateChangeListener(this);
        AnonymousClass073.A02.remove(viewGroup);
        AbstractCollection abstractCollection = (AbstractCollection) AnonymousClass073.A00().get(viewGroup);
        if (abstractCollection != null && abstractCollection.size() > 0) {
            Iterator it = abstractCollection.iterator();
            while (it.hasNext()) {
                ((AnonymousClass072) it.next()).A0H(viewGroup);
            }
        }
        AnonymousClass072 r1 = this.A01;
        r1.A09.A02.clear();
        r1.A09.A00.clear();
        r1.A09.A03.A05();
    }
}
