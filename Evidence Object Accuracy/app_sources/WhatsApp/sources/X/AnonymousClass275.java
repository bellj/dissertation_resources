package X;

/* renamed from: X.275  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass275 implements AbstractC462925h {
    public static final C113275Gw A00 = new C113275Gw();
    public static final AnonymousClass275 A01 = new AnonymousClass275();

    @Override // X.AbstractC462925h
    public boolean Afl(boolean z, boolean z2, boolean z3, boolean z4) {
        if (z == z3 && z2 == z4) {
            return z2;
        }
        throw A00;
    }

    @Override // X.AbstractC462925h
    public AbstractC27881Jp Afm(AbstractC27881Jp r2, AbstractC27881Jp r3, boolean z, boolean z2) {
        if (z == z2 && r2.equals(r3)) {
            return r2;
        }
        throw A00;
    }

    @Override // X.AbstractC462925h
    public double Afn(double d, double d2, boolean z, boolean z2) {
        if (z == z2 && d == d2) {
            return d;
        }
        throw A00;
    }

    @Override // X.AbstractC462925h
    public float Afo(float f, float f2, boolean z, boolean z2) {
        if (z == z2 && f == f2) {
            return f;
        }
        throw A00;
    }

    @Override // X.AbstractC462925h
    public int Afp(int i, int i2, boolean z, boolean z2) {
        if (z == z2 && i == i2) {
            return i;
        }
        throw A00;
    }

    @Override // X.AbstractC462925h
    public AbstractC41941uP Afq(AbstractC41941uP r2, AbstractC41941uP r3) {
        if (r2.equals(r3)) {
            return r2;
        }
        throw A00;
    }

    @Override // X.AbstractC462925h
    public AnonymousClass1K6 Afr(AnonymousClass1K6 r2, AnonymousClass1K6 r3) {
        if (r2.equals(r3)) {
            return r2;
        }
        throw A00;
    }

    @Override // X.AbstractC462925h
    public long Afs(long j, long j2, boolean z, boolean z2) {
        if (z == z2 && j == j2) {
            return j;
        }
        throw A00;
    }

    @Override // X.AbstractC462925h
    public AnonymousClass1G1 Aft(AnonymousClass1G1 r4, AnonymousClass1G1 r5) {
        if (r4 == null) {
            if (r5 == null) {
                return null;
            }
        } else if (r5 != null) {
            AbstractC27091Fz r2 = (AbstractC27091Fz) r4;
            if (r2 == r5 || !r2.A0V(AnonymousClass25B.GET_DEFAULT_INSTANCE, null, null).getClass().isInstance(r5)) {
                return r4;
            }
            r2.A0Y(this, (AbstractC27091Fz) r5);
            return r4;
        }
        throw A00;
    }

    @Override // X.AbstractC462925h
    public Object Afu(Object obj, Object obj2, boolean z) {
        if (z && obj.equals(obj2)) {
            return obj;
        }
        throw A00;
    }

    @Override // X.AbstractC462925h
    public Object Afv(Object obj, Object obj2, boolean z) {
        if (z) {
            AbstractC27091Fz r2 = (AbstractC27091Fz) obj;
            AnonymousClass1G1 r5 = (AnonymousClass1G1) obj2;
            if (r2 != r5) {
                if (r2.A0V(AnonymousClass25B.GET_DEFAULT_INSTANCE, null, null).getClass().isInstance(r5)) {
                    r2.A0Y(this, (AbstractC27091Fz) r5);
                }
            }
            return obj;
        }
        throw A00;
    }

    @Override // X.AbstractC462925h
    public void Afw(boolean z) {
        if (z) {
            throw A00;
        }
    }

    @Override // X.AbstractC462925h
    public Object Afx(Object obj, Object obj2, boolean z) {
        if (z && obj.equals(obj2)) {
            return obj;
        }
        throw A00;
    }

    @Override // X.AbstractC462925h
    public String Afy(String str, String str2, boolean z, boolean z2) {
        if (z == z2 && str.equals(str2)) {
            return str;
        }
        throw A00;
    }

    @Override // X.AbstractC462925h
    public AnonymousClass256 Afz(AnonymousClass256 r2, AnonymousClass256 r3) {
        if (r2.equals(r3)) {
            return r2;
        }
        throw A00;
    }
}
