package X;

/* renamed from: X.3Dv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C64003Dv {
    public final AnonymousClass1V8 A00;
    public final C63993Du A01;

    public C64003Dv(AbstractC15710nm r2, AnonymousClass1V8 r3) {
        AnonymousClass1V8.A01(r3, "choice");
        this.A01 = (C63993Du) AnonymousClass3JT.A01(r2, r3, 3);
        this.A00 = r3;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || C64003Dv.class != obj.getClass()) {
            return false;
        }
        return this.A01.equals(((C64003Dv) obj).A01);
    }

    public int hashCode() {
        return C12970iu.A08(this.A01, C12970iu.A1b());
    }
}
