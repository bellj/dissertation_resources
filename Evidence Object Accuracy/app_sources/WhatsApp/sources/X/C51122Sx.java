package X;

import android.content.Intent;
import androidx.fragment.app.DialogFragment;

/* renamed from: X.2Sx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C51122Sx {
    public static void A00(Intent intent, AnonymousClass01E r2) {
        C35741ib.A00(intent, r2.getClass().getSimpleName());
        r2.A0v(intent);
    }

    public static void A01(DialogFragment dialogFragment, AnonymousClass01E r4) {
        if (!r4.A0X) {
            AnonymousClass01F A0E = r4.A0E();
            String name = dialogFragment.getClass().getName();
            if (A0E.A0A(name) == null) {
                dialogFragment.A1F(r4.A0E(), name);
            }
        }
    }

    public static void A02(AnonymousClass01E r1, AnonymousClass182 r2, AnonymousClass180 r3, boolean z, boolean z2) {
        int i;
        if (r3 != null && z != z2) {
            if (z2) {
                r3.A02(r1, "visible");
                i = 1;
            } else {
                r3.A02(r1, "invisible");
                i = 2;
            }
            r2.A00(r1, i);
        }
    }
}
