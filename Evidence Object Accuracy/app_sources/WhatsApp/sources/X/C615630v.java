package X;

/* renamed from: X.30v  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C615630v extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Integer A02;
    public Integer A03;
    public Long A04;
    public Long A05;
    public String A06;

    public C615630v() {
        super(3482, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A04);
        r3.Abe(2, this.A00);
        r3.Abe(3, this.A01);
        r3.Abe(5, this.A02);
        r3.Abe(7, this.A03);
        r3.Abe(11, this.A05);
        r3.Abe(13, this.A06);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamDisappearingMessageKeepInChat {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "chatEphemeralityDuration", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "isAGroup", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "isAdmin", this.A01);
        String str = null;
        Integer num = this.A02;
        if (num != null) {
            str = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "kicActionName", str);
        String str2 = null;
        Integer num2 = this.A03;
        if (num2 != null) {
            str2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "kicEntryPoint", str2);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "messagesInFolder", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "threadId", this.A06);
        return C12960it.A0d("}", A0k);
    }
}
