package X;

import android.content.Context;
import android.graphics.Rect;
import android.util.Pair;
import java.util.concurrent.Callable;

/* renamed from: X.3cz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class CallableC71473cz implements Callable {
    public final /* synthetic */ Context A00;
    public final /* synthetic */ AnonymousClass3D8 A01;
    public final /* synthetic */ AnonymousClass5SD A02;
    public final /* synthetic */ Object A03;

    public CallableC71473cz(Context context, AnonymousClass3D8 r2, AnonymousClass5SD r3, Object obj) {
        this.A01 = r2;
        this.A00 = context;
        this.A02 = r3;
        this.A03 = obj;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        AnonymousClass28D r2;
        Object obj;
        Pair Aal;
        C89824Ln r1;
        AnonymousClass3IP A00;
        AnonymousClass3H1 r10;
        Context context = this.A00;
        AnonymousClass5SD r9 = this.A02;
        Object obj2 = this.A03;
        AnonymousClass3D8 r0 = this.A01;
        AnonymousClass3IP r5 = r0.A05;
        int i = r0.A01;
        int i2 = r0.A02;
        int i3 = r0.A00;
        if (r5 != null) {
            r2 = r5.A03;
            obj = r5.A04;
        } else {
            r2 = null;
            obj = null;
        }
        C94614cC.A01("RC Create Tree");
        if (r5 == null || r9 != r5.A01) {
            Aal = r9.Aal();
        } else {
            Aal = C12990iw.A0L(r2, obj);
        }
        Object obj3 = Aal.first;
        if (!(r5 == null || (r10 = r5.A02) == null || obj3 != r5.A03)) {
            int i4 = r10.A01;
            Rect rect = r10.A03.A04;
            if (AnonymousClass4DA.A00(i4, i2, rect.width()) && AnonymousClass4DA.A00(r10.A00, i3, rect.height())) {
                A00 = new AnonymousClass3IP(r5.A00, r9, r10, (AnonymousClass28D) Aal.first, Aal.second);
                C94614cC.A00();
                return A00;
            }
        }
        C94614cC.A01("RC Layout");
        if (r5 == null || (r1 = r5.A00) == null) {
            r1 = null;
        }
        C92304Vj r8 = new C92304Vj(context, new C93294Zw(r1), obj2, i);
        AbstractC72393eW A0D = ((AnonymousClass28D) Aal.first).A0D(r8, i2, i3);
        C94614cC.A00();
        C94614cC.A01("RC Reduce");
        A00 = AnonymousClass3IP.A00(A0D, r8, r9, (AnonymousClass28D) Aal.first, Aal.second, i2, i3);
        C94614cC.A00();
        r8.A00 = null;
        C94614cC.A00();
        return A00;
    }
}
