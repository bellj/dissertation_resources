package X;

import android.net.Uri;
import java.io.File;

/* renamed from: X.399  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass399 extends AbstractC107844y2 {
    public final ActivityC13810kN A00;
    public final C14900mE A01;
    public final C16170oZ A02;
    public final C15610nY A03;
    public final C18640sm A04;
    public final C16590pI A05;
    public final C14950mJ A06;
    public final C20830wO A07;
    public final AnonymousClass1CH A08;
    public final C22370yy A09;
    public final AnonymousClass39D A0A;
    public final AnonymousClass1X4 A0B;

    public AnonymousClass399(ActivityC13810kN r1, C14900mE r2, C16170oZ r3, C15610nY r4, C18640sm r5, C16590pI r6, C14950mJ r7, C20830wO r8, AnonymousClass1CH r9, C22370yy r10, AnonymousClass39D r11, AnonymousClass1X4 r12) {
        this.A05 = r6;
        this.A01 = r2;
        this.A02 = r3;
        this.A06 = r7;
        this.A03 = r4;
        this.A08 = r9;
        this.A09 = r10;
        this.A04 = r5;
        this.A07 = r8;
        this.A0B = r12;
        this.A00 = r1;
        this.A0A = r11;
    }

    @Override // X.AbstractC47452At
    public AnonymousClass2BW A8D() {
        File file;
        AnonymousClass1X4 r12 = this.A0B;
        C16150oX A00 = AbstractC15340mz.A00(r12);
        if (A00.A0P && (file = A00.A0F) != null && file.exists()) {
            return new C67773Sv(this.A00, Uri.fromFile(A00.A0F));
        }
        C16590pI r6 = this.A05;
        C14900mE r2 = this.A01;
        C16170oZ r3 = this.A02;
        C14950mJ r7 = this.A06;
        C15610nY r4 = this.A03;
        AnonymousClass1CH r9 = this.A08;
        C22370yy r10 = this.A09;
        return new AnonymousClass2CZ(this.A00, r2, r3, r4, this.A04, r6, r7, this.A07, r9, r10, this.A0A, r12, super.A00);
    }
}
