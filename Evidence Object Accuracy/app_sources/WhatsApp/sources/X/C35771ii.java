package X;

import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.1ii  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C35771ii extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C35771ii A0B;
    public static volatile AnonymousClass255 A0C;
    public double A00;
    public double A01;
    public float A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public long A07;
    public AbstractC27881Jp A08 = AbstractC27881Jp.A01;
    public C43261wh A09;
    public String A0A = "";

    static {
        C35771ii r0 = new C35771ii();
        A0B = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r16, Object obj, Object obj2) {
        C81603uH r2;
        switch (r16.ordinal()) {
            case 0:
                return A0B;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                C35771ii r0 = (C35771ii) obj2;
                int i = this.A04;
                boolean z = true;
                if ((i & 1) != 1) {
                    z = false;
                }
                double d = this.A00;
                int i2 = r0.A04;
                boolean z2 = true;
                if ((i2 & 1) != 1) {
                    z2 = false;
                }
                this.A00 = r8.Afn(d, r0.A00, z, z2);
                boolean z3 = false;
                if ((i & 2) == 2) {
                    z3 = true;
                }
                double d2 = this.A01;
                boolean z4 = false;
                if ((i2 & 2) == 2) {
                    z4 = true;
                }
                this.A01 = r8.Afn(d2, r0.A01, z3, z4);
                boolean z5 = false;
                if ((i & 4) == 4) {
                    z5 = true;
                }
                int i3 = this.A03;
                boolean z6 = false;
                if ((i2 & 4) == 4) {
                    z6 = true;
                }
                this.A03 = r8.Afp(i3, r0.A03, z5, z6);
                boolean z7 = false;
                if ((i & 8) == 8) {
                    z7 = true;
                }
                float f = this.A02;
                boolean z8 = false;
                if ((i2 & 8) == 8) {
                    z8 = true;
                }
                this.A02 = r8.Afo(f, r0.A02, z7, z8);
                boolean z9 = false;
                if ((i & 16) == 16) {
                    z9 = true;
                }
                int i4 = this.A05;
                boolean z10 = false;
                if ((i2 & 16) == 16) {
                    z10 = true;
                }
                this.A05 = r8.Afp(i4, r0.A05, z9, z10);
                boolean z11 = false;
                if ((i & 32) == 32) {
                    z11 = true;
                }
                String str = this.A0A;
                boolean z12 = false;
                if ((i2 & 32) == 32) {
                    z12 = true;
                }
                this.A0A = r8.Afy(str, r0.A0A, z11, z12);
                boolean z13 = false;
                if ((i & 64) == 64) {
                    z13 = true;
                }
                long j = this.A07;
                boolean z14 = false;
                if ((i2 & 64) == 64) {
                    z14 = true;
                }
                this.A07 = r8.Afs(j, r0.A07, z13, z14);
                boolean z15 = false;
                if ((i & 128) == 128) {
                    z15 = true;
                }
                int i5 = this.A06;
                boolean z16 = false;
                if ((i2 & 128) == 128) {
                    z16 = true;
                }
                this.A06 = r8.Afp(i5, r0.A06, z15, z16);
                boolean z17 = false;
                if ((i & 256) == 256) {
                    z17 = true;
                }
                AbstractC27881Jp r4 = this.A08;
                boolean z18 = false;
                if ((i2 & 256) == 256) {
                    z18 = true;
                }
                this.A08 = r8.Afm(r4, r0.A08, z17, z18);
                this.A09 = (C43261wh) r8.Aft(this.A09, r0.A09);
                if (r8 == C463025i.A00) {
                    this.A04 |= r0.A04;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                AnonymousClass254 r02 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        int A03 = r82.A03();
                        switch (A03) {
                            case 0:
                                break;
                            case 9:
                                this.A04 |= 1;
                                this.A00 = Double.longBitsToDouble(r82.A05());
                                break;
                            case 17:
                                this.A04 |= 2;
                                this.A01 = Double.longBitsToDouble(r82.A05());
                                break;
                            case 24:
                                this.A04 |= 4;
                                this.A03 = r82.A02();
                                break;
                            case 37:
                                this.A04 |= 8;
                                this.A02 = Float.intBitsToFloat(r82.A01());
                                break;
                            case 40:
                                this.A04 |= 16;
                                this.A05 = r82.A02();
                                break;
                            case SearchActionVerificationClientService.TIME_TO_SLEEP_IN_MS /* 50 */:
                                String A0A = r82.A0A();
                                this.A04 |= 32;
                                this.A0A = A0A;
                                break;
                            case 56:
                                this.A04 |= 64;
                                this.A07 = r82.A06();
                                break;
                            case 64:
                                this.A04 |= 128;
                                this.A06 = r82.A02();
                                break;
                            case 130:
                                this.A04 |= 256;
                                this.A08 = r82.A08();
                                break;
                            case 138:
                                if ((this.A04 & 512) == 512) {
                                    r2 = (C81603uH) this.A09.A0T();
                                } else {
                                    r2 = null;
                                }
                                C43261wh r1 = (C43261wh) r82.A09(r02, C43261wh.A0O.A0U());
                                this.A09 = r1;
                                if (r2 != null) {
                                    r2.A04(r1);
                                    this.A09 = (C43261wh) r2.A01();
                                }
                                this.A04 |= 512;
                                break;
                            default:
                                if (A0a(r82, A03)) {
                                    break;
                                } else {
                                    break;
                                }
                        }
                    } catch (C28971Pt e) {
                        e.unfinishedMessage = this;
                        throw new RuntimeException(e);
                    } catch (IOException e2) {
                        C28971Pt r12 = new C28971Pt(e2.getMessage());
                        r12.unfinishedMessage = this;
                        throw new RuntimeException(r12);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C35771ii();
            case 5:
                return new C35761ih();
            case 6:
                break;
            case 7:
                if (A0C == null) {
                    synchronized (C35771ii.class) {
                        if (A0C == null) {
                            A0C = new AnonymousClass255(A0B);
                        }
                    }
                }
                return A0C;
            default:
                throw new UnsupportedOperationException();
        }
        return A0B;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A04;
        if ((i3 & 1) == 1) {
            i2 = 9;
        }
        if ((i3 & 2) == 2) {
            i2 += 9;
        }
        if ((i3 & 4) == 4) {
            i2 += CodedOutputStream.A04(3, this.A03);
        }
        if ((i3 & 8) == 8) {
            i2 += 5;
        }
        if ((i3 & 16) == 16) {
            i2 += CodedOutputStream.A04(5, this.A05);
        }
        if ((i3 & 32) == 32) {
            i2 += CodedOutputStream.A07(6, this.A0A);
        }
        int i4 = this.A04;
        if ((i4 & 64) == 64) {
            i2 += CodedOutputStream.A05(7, this.A07);
        }
        if ((i4 & 128) == 128) {
            i2 += CodedOutputStream.A04(8, this.A06);
        }
        if ((i4 & 256) == 256) {
            i2 += CodedOutputStream.A09(this.A08, 16);
        }
        if ((i4 & 512) == 512) {
            C43261wh r0 = this.A09;
            if (r0 == null) {
                r0 = C43261wh.A0O;
            }
            i2 += CodedOutputStream.A0A(r0, 17);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A04 & 1) == 1) {
            codedOutputStream.A0G(1, Double.doubleToRawLongBits(this.A00));
        }
        if ((this.A04 & 2) == 2) {
            codedOutputStream.A0G(2, Double.doubleToRawLongBits(this.A01));
        }
        if ((this.A04 & 4) == 4) {
            codedOutputStream.A0F(3, this.A03);
        }
        if ((this.A04 & 8) == 8) {
            codedOutputStream.A0D(4, Float.floatToRawIntBits(this.A02));
        }
        if ((this.A04 & 16) == 16) {
            codedOutputStream.A0F(5, this.A05);
        }
        if ((this.A04 & 32) == 32) {
            codedOutputStream.A0I(6, this.A0A);
        }
        if ((this.A04 & 64) == 64) {
            codedOutputStream.A0H(7, this.A07);
        }
        if ((this.A04 & 128) == 128) {
            codedOutputStream.A0F(8, this.A06);
        }
        if ((this.A04 & 256) == 256) {
            codedOutputStream.A0K(this.A08, 16);
        }
        if ((this.A04 & 512) == 512) {
            C43261wh r0 = this.A09;
            if (r0 == null) {
                r0 = C43261wh.A0O;
            }
            codedOutputStream.A0L(r0, 17);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
