package X;

import android.content.BroadcastReceiver;
import android.content.ComponentCallbacks;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.SystemClock;
import android.view.View;
import android.widget.FrameLayout;
import java.lang.ref.WeakReference;
import java.util.EnumSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.TimeUnit;

/* renamed from: X.04L  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass04L extends FrameLayout implements AnonymousClass04M, AnonymousClass04N, AnonymousClass04O, AnonymousClass04P {
    public static final double A0r = Math.log(2.0d);
    public double A00;
    public double A01;
    public double A02 = 0.5d;
    public double A03 = 0.5d;
    public float A04;
    public float A05;
    public float A06;
    public float A07;
    public float A08;
    public float A09;
    public float A0A;
    public float A0B;
    public float A0C;
    public int A0D;
    public int A0E;
    public int A0F;
    public int A0G;
    public long A0H = System.nanoTime();
    public long A0I = SystemClock.uptimeMillis();
    public long A0J;
    public long A0K;
    public Context A0L;
    public AnonymousClass04Q A0M;
    public AnonymousClass0O0 A0N;
    public AnonymousClass03S A0O;
    public AbstractC11540gS A0P;
    public AbstractC11550gT A0Q;
    public C05110Oh A0R;
    public RunnableC10120e4 A0S;
    public AnonymousClass0OD A0T;
    public AbstractC12780iU A0U = AbstractC12780iU.A00;
    public EnumSet A0V;
    public Queue A0W;
    public boolean A0X;
    public boolean A0Y;
    public boolean A0Z;
    public boolean A0a;
    public boolean A0b;
    public boolean A0c = false;
    public boolean A0d;
    public boolean A0e;
    public boolean A0f;
    public boolean A0g;
    public boolean A0h;
    public final BroadcastReceiver A0i = new C019609f(this);
    public final ComponentCallbacks A0j = new AnonymousClass0V1(this);
    public final Matrix A0k = new Matrix();
    public final Matrix A0l = new Matrix();
    public final Paint A0m = new Paint(2);
    public final RectF A0n = new RectF();
    public final C05250Ov A0o = new C05250Ov(this);
    public final float[] A0p = new float[2];
    public final float[] A0q = new float[4];

    public AnonymousClass04L(Context context, AnonymousClass0O0 r5) {
        super(context);
        setWillNotDraw(false);
        this.A0L = context;
        this.A0N = r5;
        AnonymousClass0OD r1 = new AnonymousClass0OD(context, this);
        this.A0T = r1;
        Matrix matrix = this.A0l;
        r1.A0K = matrix;
        r1.A09 = 0.87f;
        r1.A07 = 0.85f;
        this.A0a = this.A0L.getPackageManager().hasSystemFeature("android.hardware.touchscreen.multitouch.distinct");
        this.A0h = true;
        RunnableC10120e4 r0 = new RunnableC10120e4(this, this);
        this.A0S = r0;
        r0.A06 = matrix;
        AnonymousClass03N.A08.add(new WeakReference(this));
        AnonymousClass03N.A00();
    }

    public static double A00(double d) {
        int i;
        if (d < 0.0d) {
            i = 1;
        } else {
            i = 0;
            if (d > 1.0d) {
                i = -1;
            }
        }
        return d + ((double) i);
    }

    private void A01(Bundle bundle) {
        if (bundle != null && bundle.containsKey("zoom")) {
            AnonymousClass04Q r2 = this.A0M;
            A0D((int) Math.min(Math.max((float) bundle.getInt("zoom"), r2.A01), r2.A00), bundle.getFloat("scale"));
            this.A02 = bundle.getDouble("xVisibleCenter") - ((double) (((long) (0 - this.A0M.A05)) / (this.A0J << 1)));
            double d = bundle.getDouble("yVisibleCenter");
            AnonymousClass04Q r0 = this.A0M;
            this.A03 = d - ((double) (((long) (r0.A06 - r0.A04)) / (this.A0J << 1)));
            this.A0B = bundle.getFloat("rotation");
            Matrix matrix = this.A0k;
            float f = this.A0C;
            matrix.setScale(f, f);
            matrix.postRotate(this.A0B);
            matrix.invert(this.A0l);
            this.A0g = false;
        }
    }

    public double A02(double d, long j) {
        double d2 = this.A01 * (((double) this.A0J) / ((double) j));
        double d3 = 1.0d - d2;
        if (d < d2) {
            return d2;
        }
        return d > d3 ? d3 : d;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0009, code lost:
        if (r2.A0H == null) goto L_0x000b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A03() {
        /*
            r5 = this;
            X.04Q r2 = r5.A0M
            boolean r0 = r2.A0N
            if (r0 == 0) goto L_0x000b
            X.0IB r1 = r2.A0H
            r0 = 1
            if (r1 != 0) goto L_0x000c
        L_0x000b:
            r0 = 0
        L_0x000c:
            r4 = 1
            if (r0 == 0) goto L_0x0018
            X.0VE r1 = r2.A0U
            boolean r0 = r1.A03
            if (r0 != 0) goto L_0x0018
            r1.A01(r4)
        L_0x0018:
            boolean r0 = r5.A0Y
            if (r0 != 0) goto L_0x0031
            android.content.Context r3 = r5.A0L
            android.content.ComponentCallbacks r0 = r5.A0j
            r3.registerComponentCallbacks(r0)
            android.content.BroadcastReceiver r2 = r5.A0i
            java.lang.String r1 = "android.net.conn.CONNECTIVITY_CHANGE"
            android.content.IntentFilter r0 = new android.content.IntentFilter
            r0.<init>(r1)
            r3.registerReceiver(r2, r0)
            r5.A0Y = r4
        L_0x0031:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass04L.A03():void");
    }

    public final void A04() {
        AnonymousClass0VE r1 = this.A0M.A0U;
        if (r1.A03) {
            r1.A01(false);
        }
        if (this.A0Y) {
            Context context = this.A0L;
            context.unregisterComponentCallbacks(this.A0j);
            try {
                context.unregisterReceiver(this.A0i);
            } catch (IllegalArgumentException unused) {
            }
            this.A0Y = false;
        }
        this.A0M.A07();
        for (C06160Sk r0 : C06160Sk.A0Q) {
            r0.A01();
        }
    }

    public final void A05() {
        int size = this.A0M.A0W.size();
        for (int i = 0; i < size; i++) {
            this.A0M.A0W.get(i);
        }
        AnonymousClass0UE.A01(new AnonymousClass0IF());
    }

    public final void A06() {
        this.A0e = false;
        C05430Pn A06 = this.A0M.A0R.A06();
        C05250Ov r8 = this.A0o;
        AnonymousClass03T r0 = A06.A02;
        double d = r0.A00;
        double d2 = r0.A01;
        AnonymousClass03T r02 = A06.A01;
        double d3 = r02.A00;
        double d4 = r02.A01;
        String str = AnonymousClass03N.A0A.A02;
        int i = this.A0G;
        r8.A02 = d;
        r8.A03 = d2;
        r8.A00 = d3;
        r8.A01 = d4;
        r8.A07 = str;
        r8.A04 = i;
        if (!r8.A08) {
            r8.A08 = true;
            long nanoTime = System.nanoTime() - r8.A05;
            long j = r8.A09;
            if (nanoTime < j) {
                r8.A0A.postDelayed(new RunnableC09440cu(r8), TimeUnit.NANOSECONDS.toMillis(j - nanoTime));
            } else {
                r8.A00();
            }
        }
    }

    public final void A07() {
        AnonymousClass04Q r4 = this.A0M;
        AnonymousClass0Q8 r1 = ((AnonymousClass0IT) r4.A0T).A0A;
        if (r1.A03 == -1) {
            r1.A03 = 1;
        }
        this.A0e = true;
        r4.A07();
        RunnableC10120e4 r12 = this.A0S;
        r12.A0E.removeCallbacks(r12);
        r12.A0C = false;
        r12.A07 = false;
        r12.A08 = true;
        r12.A0F.forceFinished(true);
        r12.A01 = 0.0f;
        r12.A00 = 0.0f;
    }

    public final void A08() {
        AnonymousClass0Q8 r1 = ((AnonymousClass0IT) this.A0M.A0T).A0A;
        if (r1.A03 == -1) {
            r1.A03 = 1;
        }
        RunnableC10120e4 r2 = this.A0S;
        View view = r2.A0E;
        view.removeCallbacks(r2);
        r2.A08 = false;
        r2.A07 = true;
        view.postOnAnimation(r2);
    }

    public final void A09() {
        RectF rectF = this.A0n;
        rectF.left = 0.0f;
        rectF.right = (float) this.A0F;
        rectF.top = 0.0f;
        rectF.bottom = (float) this.A0D;
        Matrix matrix = this.A0l;
        matrix.mapRect(rectF);
        float[] fArr = this.A0q;
        float f = this.A04;
        fArr[0] = -f;
        float f2 = -this.A05;
        fArr[1] = f2;
        fArr[2] = f;
        fArr[3] = f2;
        matrix.mapVectors(fArr);
        float max = Math.max(Math.abs(fArr[0]), Math.abs(fArr[2]));
        float max2 = Math.max(Math.abs(fArr[1]), Math.abs(fArr[3]));
        float f3 = (float) this.A0J;
        this.A00 = (double) (max / f3);
        this.A01 = (double) (max2 / f3);
    }

    public void A0A(double d, double d2) {
        this.A02 = A00(d);
        this.A03 = A02(d2, this.A0J);
    }

    public void A0B(float f, float f2, float f3) {
        if (this.A0a) {
            AnonymousClass0U9 r0 = this.A0M.A0R;
            float[] fArr = this.A0p;
            r0.A0A(fArr, f2, f3);
            float f4 = fArr[0];
            float f5 = fArr[1];
            Matrix matrix = this.A0k;
            matrix.postRotate(f - this.A0B, f2, f3);
            matrix.invert(this.A0l);
            this.A0B = f % 360.0f;
            A09();
            A0C(f2, f3, f4, f5);
        }
    }

    public final void A0C(float f, float f2, float f3, float f4) {
        float[] fArr = this.A0p;
        fArr[0] = this.A04 - f;
        fArr[1] = this.A05 - f2;
        this.A0l.mapVectors(fArr);
        float f5 = fArr[0];
        long j = this.A0J;
        float f6 = (float) j;
        this.A02 = A00((double) (f3 + (f5 / f6)));
        this.A03 = A02((double) (f4 + (fArr[1] / f6)), j);
    }

    public final void A0D(int i, float f) {
        this.A0G = i;
        this.A0C = f;
        int i2 = 1 << i;
        this.A0E = i2;
        this.A0J = (long) (i2 * this.A0M.A0O);
    }

    public final void A0E(Bundle bundle) {
        AnonymousClass0O0 r0 = this.A0N;
        AnonymousClass04Q r3 = new AnonymousClass04Q(r0, this);
        this.A0M = r3;
        C06860Vj r4 = r0.A01;
        if (r4 == null) {
            float f = r3.A01;
            A0D((int) f, (f % 1.0f) + 1.0f);
        } else {
            float min = Math.min(Math.max(r4.A02, r3.A01), r3.A00);
            A0D((int) min, (min % 1.0f) + 1.0f);
            AnonymousClass03T r2 = r4.A03;
            if (r2 != null) {
                this.A02 = (double) AnonymousClass0U9.A02(r2.A01);
                this.A03 = (double) AnonymousClass0U9.A01(r2.A00);
            }
            this.A0B = r4.A00;
        }
        this.A0R = r3.A0S;
        Matrix matrix = this.A0k;
        float f2 = this.A0C;
        matrix.setScale(f2, f2);
        matrix.postRotate(this.A0B);
        matrix.invert(this.A0l);
        A01(bundle);
    }

    public final void A0F(Bundle bundle) {
        if (!this.A0g) {
            bundle.putDouble("xVisibleCenter", this.A02 + ((double) (((long) (0 - this.A0M.A05)) / (this.A0J << 1))));
            double d = this.A03;
            AnonymousClass04Q r0 = this.A0M;
            bundle.putDouble("yVisibleCenter", d + ((double) (((long) (r0.A06 - r0.A04)) / (this.A0J << 1))));
            bundle.putInt("zoom", this.A0G);
            bundle.putFloat("scale", this.A0C);
            bundle.putFloat("rotation", this.A0B);
            this.A0g = true;
        }
    }

    public void A0G(AbstractC12200hX r2) {
        Queue queue;
        if (!this.A0b || ((queue = this.A0W) != null && !queue.isEmpty())) {
            Queue queue2 = this.A0W;
            if (queue2 == null) {
                queue2 = new LinkedList();
                this.A0W = queue2;
            }
            queue2.add(r2);
            return;
        }
        r2.ASO(this.A0M);
    }

    public boolean A0H(float f, float f2, float f3) {
        AnonymousClass0U9 r0 = this.A0M.A0R;
        float[] fArr = this.A0p;
        r0.A0A(fArr, f2, f3);
        float f4 = fArr[0];
        float f5 = fArr[1];
        AnonymousClass04Q r2 = this.A0M;
        float min = Math.min(Math.max(f, r2.A01), r2.A00);
        float f6 = (min % 1.0f) + 1.0f;
        int i = this.A0G;
        float f7 = f6 / this.A0C;
        this.A06 = f7;
        A0D((int) min, f6);
        Matrix matrix = this.A0k;
        matrix.postScale(f7, f7, f2, f3);
        matrix.invert(this.A0l);
        A09();
        A0C(f2, f3, f4, f5);
        if (this.A0G != i) {
            return true;
        }
        return false;
    }

    public final boolean A0I(float f, float f2, float f3) {
        float f4 = this.A0C * f;
        int i = this.A0G;
        while (f4 > 2.0f) {
            f4 /= 2.0f;
            i++;
        }
        while (f4 < 1.0f) {
            f4 *= 2.0f;
            i--;
        }
        if (A0H((((float) i) + f4) - 1.0f, f2, f3)) {
            this.A0M.A05();
        }
        return this.A06 != 1.0f;
    }

    public EnumSet getCurrentAttribution() {
        return this.A0V;
    }

    @Deprecated
    public final AnonymousClass04Q getMap() {
        return this.A0M;
    }

    public AbstractC11540gS getOnAttributionChangeListener() {
        return this.A0P;
    }

    public float getZoom() {
        return (((float) this.A0G) + this.A0C) - 1.0f;
    }

    @Override // android.view.View, android.view.ViewGroup
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.A0M != null) {
            A03();
            this.A0K = System.nanoTime();
            if (!this.A0d) {
                AnonymousClass0IC r3 = this.A0M.A0T.A03;
                ((AbstractC08590bS) r3).A01.set(0);
                ((AnonymousClass0IU) r3).A00.set(0);
                ((AnonymousClass0IU) r3).A01.set(0);
                this.A0d = true;
                return;
            }
            return;
        }
        throw new RuntimeException("MapView.onCreate() must be called!");
    }

    @Override // android.view.View
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.A0M.A04();
    }

    @Override // android.view.View, android.view.ViewGroup
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        AnonymousClass04Q r2 = this.A0M;
        AnonymousClass0IB r1 = r2.A0H;
        if (r1 != null) {
            r1.A0E.A01();
            r1.A01();
        }
        r2.A04();
        AnonymousClass0UE.A01(new AnonymousClass0IF());
        A04();
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        long nanoTime = System.nanoTime();
        super.onDraw(canvas);
        canvas.drawColor(-987675);
        this.A0X = true;
        int size = this.A0M.A0W.size();
        for (int i = 0; i < size; i++) {
            AnonymousClass03S r1 = (AnonymousClass03S) this.A0M.A0W.get(i);
            if (r1.A04) {
                r1.A08(canvas);
                if (r1 instanceof AnonymousClass0IS) {
                    boolean z = this.A0X;
                    boolean z2 = false;
                    if (((AnonymousClass0IT) r1).A06 == 0) {
                        z2 = true;
                    }
                    this.A0X = z2 & z;
                }
            }
        }
        if (this.A0X) {
            AnonymousClass04Q r12 = this.A0M;
            if (this.A0d) {
                new C10900fO(r12.A0T.A03);
                this.A0d = false;
            }
        }
        long nanoTime2 = System.nanoTime();
        C06160Sk.A0C.A02(nanoTime2 - nanoTime);
        if (this.A0H > 0) {
            new C10910fP(this, nanoTime2);
            this.A0H = 0;
        }
        if (this.A0K > 0) {
            new C10920fQ(this, nanoTime2);
            this.A0K = 0;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x00bd, code lost:
        if (r1 != false) goto L_0x009e;
     */
    @Override // android.widget.FrameLayout, android.view.View, android.view.ViewGroup
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onLayout(boolean r10, int r11, int r12, int r13, int r14) {
        /*
        // Method dump skipped, instructions count: 233
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass04L.onLayout(boolean, int, int, int, int):void");
    }

    @Override // android.view.View
    public final void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            if (bundle.containsKey("zoom")) {
                if (bundle.containsKey("parentBundle")) {
                    super.onRestoreInstanceState(bundle.getParcelable("parentBundle"));
                }
                A01(bundle);
                return;
            }
        }
        super.onRestoreInstanceState(parcelable);
    }

    @Override // android.view.View
    public final Parcelable onSaveInstanceState() {
        boolean z = this.A0g;
        Parcelable onSaveInstanceState = super.onSaveInstanceState();
        if (z) {
            return onSaveInstanceState;
        }
        Bundle bundle = new Bundle();
        A0F(bundle);
        bundle.putParcelable("parentBundle", onSaveInstanceState);
        return bundle;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0044, code lost:
        if (java.lang.Math.abs(r4 - r3.A0B) <= r13) goto L_0x028d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:0x0316, code lost:
        if (r3.A0O != false) goto L_0x03b5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:0x031f, code lost:
        if (r14 == 0.0f) goto L_0x03b5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x007f, code lost:
        if (java.lang.Math.abs(r4 - r3.A01) <= r1) goto L_0x0089;
     */
    /* JADX WARNING: Removed duplicated region for block: B:140:0x0341 A[Catch: all -> 0x04cb, TryCatch #0 {all -> 0x04cb, blocks: (B:3:0x0004, B:7:0x002b, B:9:0x003a, B:11:0x0046, B:14:0x0051, B:16:0x005b, B:18:0x0066, B:20:0x0075, B:22:0x0081, B:23:0x0089, B:25:0x00a1, B:26:0x00a7, B:27:0x00c4, B:29:0x00ca, B:31:0x00d4, B:35:0x00de, B:37:0x00e2, B:39:0x00e6, B:41:0x00ed, B:43:0x00fc, B:45:0x0109, B:46:0x011e, B:48:0x0132, B:50:0x0138, B:51:0x013d, B:53:0x0141, B:54:0x0146, B:56:0x014c, B:58:0x0157, B:60:0x0165, B:62:0x0170, B:64:0x0186, B:65:0x01ae, B:67:0x01b2, B:69:0x01b6, B:70:0x01c7, B:72:0x01d3, B:74:0x01d7, B:76:0x01e1, B:78:0x01e5, B:79:0x01ef, B:81:0x01f3, B:83:0x01fd, B:84:0x0207, B:86:0x020b, B:88:0x0235, B:90:0x0243, B:93:0x0267, B:97:0x0270, B:99:0x0276, B:102:0x027f, B:104:0x0285, B:107:0x0293, B:108:0x02a1, B:110:0x02a9, B:111:0x02bf, B:113:0x02ca, B:114:0x02d7, B:116:0x02dd, B:119:0x02e5, B:121:0x02f1, B:123:0x0301, B:126:0x0310, B:128:0x0314, B:134:0x0321, B:136:0x0337, B:138:0x033d, B:140:0x0341, B:141:0x0344, B:142:0x0347, B:144:0x034f, B:146:0x0383, B:148:0x038b, B:150:0x0393, B:152:0x039b, B:154:0x03a9, B:155:0x03b0, B:157:0x03b7, B:159:0x03be, B:160:0x03c0, B:162:0x03d5, B:164:0x03e2, B:166:0x03e6, B:167:0x03f1, B:168:0x03f4, B:170:0x03fe, B:172:0x0417, B:173:0x0419, B:175:0x0425, B:178:0x042d, B:179:0x042e, B:183:0x043c, B:185:0x0449, B:187:0x044d, B:189:0x045e, B:190:0x0464, B:191:0x0467, B:192:0x0469, B:194:0x0474, B:195:0x0476, B:197:0x0484, B:198:0x0487, B:200:0x0496, B:201:0x0499, B:202:0x04b0, B:204:0x04b4, B:205:0x04b7), top: B:211:0x0004 }] */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x03a9 A[Catch: all -> 0x04cb, TryCatch #0 {all -> 0x04cb, blocks: (B:3:0x0004, B:7:0x002b, B:9:0x003a, B:11:0x0046, B:14:0x0051, B:16:0x005b, B:18:0x0066, B:20:0x0075, B:22:0x0081, B:23:0x0089, B:25:0x00a1, B:26:0x00a7, B:27:0x00c4, B:29:0x00ca, B:31:0x00d4, B:35:0x00de, B:37:0x00e2, B:39:0x00e6, B:41:0x00ed, B:43:0x00fc, B:45:0x0109, B:46:0x011e, B:48:0x0132, B:50:0x0138, B:51:0x013d, B:53:0x0141, B:54:0x0146, B:56:0x014c, B:58:0x0157, B:60:0x0165, B:62:0x0170, B:64:0x0186, B:65:0x01ae, B:67:0x01b2, B:69:0x01b6, B:70:0x01c7, B:72:0x01d3, B:74:0x01d7, B:76:0x01e1, B:78:0x01e5, B:79:0x01ef, B:81:0x01f3, B:83:0x01fd, B:84:0x0207, B:86:0x020b, B:88:0x0235, B:90:0x0243, B:93:0x0267, B:97:0x0270, B:99:0x0276, B:102:0x027f, B:104:0x0285, B:107:0x0293, B:108:0x02a1, B:110:0x02a9, B:111:0x02bf, B:113:0x02ca, B:114:0x02d7, B:116:0x02dd, B:119:0x02e5, B:121:0x02f1, B:123:0x0301, B:126:0x0310, B:128:0x0314, B:134:0x0321, B:136:0x0337, B:138:0x033d, B:140:0x0341, B:141:0x0344, B:142:0x0347, B:144:0x034f, B:146:0x0383, B:148:0x038b, B:150:0x0393, B:152:0x039b, B:154:0x03a9, B:155:0x03b0, B:157:0x03b7, B:159:0x03be, B:160:0x03c0, B:162:0x03d5, B:164:0x03e2, B:166:0x03e6, B:167:0x03f1, B:168:0x03f4, B:170:0x03fe, B:172:0x0417, B:173:0x0419, B:175:0x0425, B:178:0x042d, B:179:0x042e, B:183:0x043c, B:185:0x0449, B:187:0x044d, B:189:0x045e, B:190:0x0464, B:191:0x0467, B:192:0x0469, B:194:0x0474, B:195:0x0476, B:197:0x0484, B:198:0x0487, B:200:0x0496, B:201:0x0499, B:202:0x04b0, B:204:0x04b4, B:205:0x04b7), top: B:211:0x0004 }] */
    /* JADX WARNING: Removed duplicated region for block: B:157:0x03b7 A[Catch: all -> 0x04cb, TryCatch #0 {all -> 0x04cb, blocks: (B:3:0x0004, B:7:0x002b, B:9:0x003a, B:11:0x0046, B:14:0x0051, B:16:0x005b, B:18:0x0066, B:20:0x0075, B:22:0x0081, B:23:0x0089, B:25:0x00a1, B:26:0x00a7, B:27:0x00c4, B:29:0x00ca, B:31:0x00d4, B:35:0x00de, B:37:0x00e2, B:39:0x00e6, B:41:0x00ed, B:43:0x00fc, B:45:0x0109, B:46:0x011e, B:48:0x0132, B:50:0x0138, B:51:0x013d, B:53:0x0141, B:54:0x0146, B:56:0x014c, B:58:0x0157, B:60:0x0165, B:62:0x0170, B:64:0x0186, B:65:0x01ae, B:67:0x01b2, B:69:0x01b6, B:70:0x01c7, B:72:0x01d3, B:74:0x01d7, B:76:0x01e1, B:78:0x01e5, B:79:0x01ef, B:81:0x01f3, B:83:0x01fd, B:84:0x0207, B:86:0x020b, B:88:0x0235, B:90:0x0243, B:93:0x0267, B:97:0x0270, B:99:0x0276, B:102:0x027f, B:104:0x0285, B:107:0x0293, B:108:0x02a1, B:110:0x02a9, B:111:0x02bf, B:113:0x02ca, B:114:0x02d7, B:116:0x02dd, B:119:0x02e5, B:121:0x02f1, B:123:0x0301, B:126:0x0310, B:128:0x0314, B:134:0x0321, B:136:0x0337, B:138:0x033d, B:140:0x0341, B:141:0x0344, B:142:0x0347, B:144:0x034f, B:146:0x0383, B:148:0x038b, B:150:0x0393, B:152:0x039b, B:154:0x03a9, B:155:0x03b0, B:157:0x03b7, B:159:0x03be, B:160:0x03c0, B:162:0x03d5, B:164:0x03e2, B:166:0x03e6, B:167:0x03f1, B:168:0x03f4, B:170:0x03fe, B:172:0x0417, B:173:0x0419, B:175:0x0425, B:178:0x042d, B:179:0x042e, B:183:0x043c, B:185:0x0449, B:187:0x044d, B:189:0x045e, B:190:0x0464, B:191:0x0467, B:192:0x0469, B:194:0x0474, B:195:0x0476, B:197:0x0484, B:198:0x0487, B:200:0x0496, B:201:0x0499, B:202:0x04b0, B:204:0x04b4, B:205:0x04b7), top: B:211:0x0004 }] */
    @Override // android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r23) {
        /*
        // Method dump skipped, instructions count: 1240
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass04L.onTouchEvent(android.view.MotionEvent):boolean");
    }

    @Override // android.view.View
    public void onWindowVisibilityChanged(int i) {
        super.onWindowVisibilityChanged(i);
        if (i == 0) {
            A03();
        } else {
            A04();
        }
    }

    @Override // X.AnonymousClass04M
    public void setCurrentAttribution(EnumSet enumSet) {
        if (!enumSet.equals(this.A0V)) {
            this.A0V = enumSet;
            AbstractC11540gS r0 = this.A0P;
            if (r0 != null) {
                ((AnonymousClass0IR) r0).A02.A07 = enumSet;
            }
        }
    }

    public final void setMapEventHandler(AbstractC12780iU r1) {
        if (r1 == null) {
            r1 = AbstractC12780iU.A00;
        }
        this.A0U = r1;
    }

    public void setOnAttributionChangeListener(AbstractC11540gS r1) {
        this.A0P = r1;
    }

    public void setOnFirstTileLoadedCallback(AbstractC11550gT r1) {
        this.A0Q = r1;
    }
}
