package X;

/* renamed from: X.0Jk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public enum EnumC03860Jk {
    JSON(0),
    ZIP(1);
    
    public final String extension;

    EnumC03860Jk(int i) {
        this.extension = r2;
    }

    @Override // java.lang.Enum, java.lang.Object
    public String toString() {
        return this.extension;
    }
}
