package X;

import android.content.Context;
import android.content.res.Resources;
import com.whatsapp.R;

/* renamed from: X.0jL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C13220jL {
    public final Resources A00;
    public final String A01;

    public C13220jL(Context context) {
        C13020j0.A01(context);
        Resources resources = context.getResources();
        this.A00 = resources;
        this.A01 = resources.getResourcePackageName(R.string.common_google_play_services_unknown_issue);
    }

    public String A00(String str) {
        Resources resources = this.A00;
        int identifier = resources.getIdentifier(str, "string", this.A01);
        if (identifier == 0) {
            return null;
        }
        return resources.getString(identifier);
    }
}
