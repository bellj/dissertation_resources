package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/* renamed from: X.1zO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C44691zO implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C99574kW();
    public int A00;
    public C44731zS A01;
    public AnonymousClass3M7 A02;
    public C30711Yn A03;
    public String A04;
    public BigDecimal A05;
    public List A06;
    public boolean A07;
    public final long A08;
    public final AnonymousClass3M1 A09;
    public final String A0A;
    public final String A0B;
    public final String A0C;
    public final String A0D;
    public final String A0E;
    public final boolean A0F;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C44691zO(AnonymousClass3M1 r6, C44731zS r7, AnonymousClass3M7 r8, C30711Yn r9, String str, String str2, String str3, String str4, String str5, String str6, BigDecimal bigDecimal, List list, int i, long j, boolean z, boolean z2) {
        List list2 = list;
        this.A0D = str;
        this.A04 = str2;
        if (bigDecimal == null || r9 == null) {
            this.A05 = null;
            this.A03 = null;
        } else {
            this.A05 = bigDecimal;
            this.A03 = r9;
        }
        this.A0C = str4;
        this.A0E = str5;
        this.A0A = str3;
        this.A0B = str6;
        this.A06 = A02() ? new ArrayList() : list2;
        this.A01 = r7;
        this.A02 = r8;
        this.A0F = z;
        this.A07 = z2;
        this.A00 = i;
        this.A09 = r6;
        if (j < 0) {
            this.A08 = 99;
        } else {
            this.A08 = j;
        }
    }

    public C44691zO(Parcel parcel) {
        BigDecimal bigDecimal;
        this.A0D = parcel.readString();
        this.A04 = parcel.readString();
        this.A0A = parcel.readString();
        String readString = parcel.readString();
        C30711Yn r2 = null;
        if (TextUtils.isEmpty(readString)) {
            bigDecimal = null;
        } else {
            bigDecimal = new BigDecimal(readString);
        }
        this.A05 = bigDecimal;
        String readString2 = parcel.readString();
        this.A03 = !TextUtils.isEmpty(readString2) ? new C30711Yn(readString2) : r2;
        this.A0C = parcel.readString();
        this.A0E = parcel.readString();
        this.A06 = parcel.createTypedArrayList(C44741zT.CREATOR);
        this.A01 = (C44731zS) parcel.readParcelable(C44731zS.class.getClassLoader());
        this.A02 = (AnonymousClass3M7) parcel.readParcelable(AnonymousClass3M7.class.getClassLoader());
        this.A0B = parcel.readString();
        boolean z = true;
        this.A0F = parcel.readByte() != 0;
        this.A07 = parcel.readByte() == 0 ? false : z;
        this.A00 = parcel.readInt();
        this.A09 = (AnonymousClass3M1) parcel.readParcelable(AnonymousClass3M1.class.getClassLoader());
        this.A08 = parcel.readLong();
    }

    public void A00(AnonymousClass1XV r4) {
        r4.A06 = this.A0D;
        r4.A09 = this.A04;
        r4.A04 = this.A0A;
        C30711Yn r0 = this.A03;
        if (r0 != null) {
            r4.A03 = r0.A00;
            BigDecimal bigDecimal = this.A05;
            r4.A0A = bigDecimal;
            AnonymousClass3M7 r1 = this.A02;
            if (r1 != null) {
                Date date = new Date();
                if (bigDecimal == null) {
                    bigDecimal = null;
                } else if (r1.A00(date)) {
                    bigDecimal = r1.A01;
                }
                r4.A0B = bigDecimal;
            }
        }
        r4.A08 = this.A0E;
        r4.A07 = this.A0C;
        r4.A00 = this.A06.size();
    }

    public boolean A01() {
        C44731zS r0 = this.A01;
        return (r0 == null || r0.A00 == 0) && !A02() && !this.A07;
    }

    public boolean A02() {
        String str = this.A0B;
        return "FETCH_FAILED".equals(str) || "PARTIAL_FETCH".equals(str);
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C44691zO) {
                C44691zO r8 = (C44691zO) obj;
                if (AnonymousClass1US.A0D(this.A0D, r8.A0D) && AnonymousClass1US.A0D(this.A04, r8.A04) && AnonymousClass1US.A0D(this.A0A, r8.A0A) && C29941Vi.A00(this.A03, r8.A03) && C29941Vi.A00(this.A05, r8.A05) && this.A08 == r8.A08 && AnonymousClass1US.A0D(this.A0C, r8.A0C) && AnonymousClass1US.A0D(this.A0E, r8.A0E) && C29941Vi.A00(this.A01, r8.A01) && C29941Vi.A00(this.A02, r8.A02)) {
                    List list = this.A06;
                    int size = list.size();
                    List list2 = r8.A06;
                    if (size == list2.size()) {
                        if (list != list2) {
                            for (int i = 0; i < list.size(); i++) {
                                if (!list.get(i).equals(list2.get(i))) {
                                    break;
                                }
                            }
                        }
                        if (!(this.A0F == r8.A0F && this.A07 == r8.A07 && this.A00 == r8.A00 && C29941Vi.A00(this.A09, r8.A09))) {
                            break;
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A0D, this.A04, this.A0A, this.A05, this.A03, this.A0C, this.A0E, this.A06, this.A01, this.A02, Long.valueOf(this.A08), Boolean.valueOf(this.A07), Integer.valueOf(this.A00), this.A09});
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        String obj;
        parcel.writeString(this.A0D);
        parcel.writeString(this.A04);
        parcel.writeString(this.A0A);
        BigDecimal bigDecimal = this.A05;
        String str = null;
        if (bigDecimal == null) {
            obj = null;
        } else {
            obj = bigDecimal.toString();
        }
        parcel.writeString(obj);
        C30711Yn r0 = this.A03;
        if (r0 != null) {
            str = r0.A00;
        }
        parcel.writeString(str);
        parcel.writeString(this.A0C);
        parcel.writeString(this.A0E);
        parcel.writeTypedList(this.A06);
        parcel.writeParcelable(this.A01, i);
        parcel.writeParcelable(this.A02, i);
        parcel.writeString(this.A0B);
        parcel.writeByte(this.A0F ? (byte) 1 : 0);
        parcel.writeByte(this.A07 ? (byte) 1 : 0);
        parcel.writeInt(this.A00);
        parcel.writeParcelable(this.A09, i);
        parcel.writeLong(this.A08);
    }
}
