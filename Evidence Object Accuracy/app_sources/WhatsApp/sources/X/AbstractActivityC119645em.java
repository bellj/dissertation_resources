package X;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.appcompat.widget.Toolbar;
import com.whatsapp.R;
import com.whatsapp.bloks.ui.BloksDialogFragment;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/* renamed from: X.5em  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC119645em extends AbstractActivityC119225dN implements AbstractC136496Mt, AnonymousClass6Lk {
    public C48912Ik A00;
    public C128695wW A01;
    public AnonymousClass60K A02;
    public AnonymousClass5TZ A03;
    public C90204Mz A04;
    public BloksDialogFragment A05;
    public C18580sg A06;
    public C16120oU A07;
    public Map A08;
    public final C64873Hg A09 = new C64873Hg();

    public static void A0O(Intent intent, String str, String str2) {
        HashMap hashMap;
        Serializable serializableExtra = intent.getSerializableExtra("screen_params");
        if (serializableExtra == null) {
            hashMap = C12970iu.A11();
        } else {
            hashMap = (HashMap) serializableExtra;
        }
        hashMap.put(str, str2);
        intent.putExtra("screen_params", hashMap);
    }

    public AnonymousClass5TZ A2e() {
        C90204Mz r2 = this.A04;
        C64873Hg r1 = this.A09;
        C14830m7 r10 = ((ActivityC13790kL) this).A05;
        AbstractC15710nm r4 = ((ActivityC13810kN) this).A03;
        C14900mE r5 = ((ActivityC13810kN) this).A05;
        C15570nT r6 = ((ActivityC13790kL) this).A01;
        C16120oU r12 = this.A07;
        AnonymousClass01d r9 = ((ActivityC13810kN) this).A08;
        AnonymousClass018 r11 = ((ActivityC13830kP) this).A01;
        AnonymousClass549 r0 = new AnonymousClass5TZ(r1, new AnonymousClass670(r4, r5, r6, this.A01, this.A02, r9, r10, r11, r12), r2) { // from class: X.549
            public final /* synthetic */ C64873Hg A00;
            public final /* synthetic */ AbstractC72403eX A01;
            public final /* synthetic */ C90204Mz A02;

            {
                this.A02 = r3;
                this.A00 = r1;
                this.A01 = r2;
            }

            @Override // X.AnonymousClass5TZ
            public final AbstractC17450qp AAO() {
                C90204Mz r02 = this.A02;
                return new C67953Tn((AbstractC17450qp) r02.A01.get(), this.A00, this.A01);
            }
        };
        r2.A00 = r0;
        return r0;
    }

    public String A2f() {
        String str = C124815qA.A00;
        return str == null ? getIntent().getStringExtra("screen_name") : str;
    }

    public void A2g() {
        String A2f = A2f();
        if (!TextUtils.isEmpty(A2f)) {
            this.A05 = BloksDialogFragment.A00(A2f, C124815qA.A01);
            C004902f r2 = new C004902f(A0V());
            r2.A07(this.A05, R.id.bloks_fragment_container);
            r2.A01();
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        C64873Hg r2 = this.A09;
        HashMap hashMap = r2.A01;
        AnonymousClass3FE r0 = (AnonymousClass3FE) hashMap.get("backpress");
        if (r0 != null) {
            C117315Zl.A0T(r0);
            return;
        }
        AnonymousClass01F A0V = A0V();
        if (A0V.A03() <= 1) {
            setResult(0, C87934Dp.A00(getIntent()));
            C124815qA.A00 = null;
            C124815qA.A01 = null;
            finish();
            return;
        }
        A0V.A0H();
        A0V.A0k(true);
        A0V.A0J();
        C64873Hg.A00(hashMap);
        Stack stack = r2.A02;
        stack.pop();
        AnonymousClass01F A0V2 = A0V();
        this.A05 = BloksDialogFragment.A00(((C004902f) ((AbstractC005002h) A0V2.A0E.get(A0V2.A03() - 1))).A0A, (HashMap) stack.peek());
        C004902f r22 = new C004902f(A0V);
        r22.A07(this.A05, R.id.bloks_fragment_container);
        r22.A01();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        Drawable A03;
        Serializable serializableExtra = getIntent().getSerializableExtra("screen_params");
        C64873Hg r2 = this.A09;
        C64873Hg.A00(r2.A01);
        r2.A02.add(C12970iu.A11());
        if (serializableExtra != null) {
            r2.A05((Map) serializableExtra);
        }
        super.onCreate(bundle);
        try {
            AnonymousClass1Q8.A00(getApplicationContext());
        } catch (IOException unused) {
        }
        setContentView(R.layout.activity_blok_main);
        Toolbar A08 = C117305Zk.A08(this);
        A08.A07();
        A1e(A08);
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0I("");
            A1U.A0M(true);
        }
        AnonymousClass2GF A00 = AnonymousClass2GF.A00(this, ((ActivityC13830kP) this).A01, R.drawable.ic_back);
        C117305Zk.A13(getResources(), A00, R.color.lightActionBarItemDrawableTint);
        A08.setNavigationIcon(A00);
        A08.setNavigationOnClickListener(C117305Zk.A0A(this, 0));
        boolean z = this instanceof AbstractActivityC123635nW;
        if (z && (A03 = AnonymousClass2GE.A03(this, AnonymousClass00T.A04(this, R.drawable.novi_wordmark), R.color.novi_header)) != null) {
            A08.setLogo(A03);
        }
        if (z) {
            C12970iu.A1N(this, R.id.toolbar_bottom_divider, 0);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A09.A03();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        this.A09.A06(false);
    }

    @Override // android.app.Activity
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        C64873Hg r2 = this.A09;
        ArrayList arrayList = (ArrayList) bundle.getSerializable("screen_manager_saved_state");
        if (arrayList != null) {
            Stack stack = r2.A02;
            stack.clear();
            stack.addAll(arrayList);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        if (this.A03 == null) {
            this.A03 = A2e();
        }
        this.A06.A00(getApplicationContext(), this.A03.AAO(), this.A00.A00(this, A0V(), new C89264Jh(this.A08)));
        this.A09.A06(true);
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        this.A09.A04(bundle);
    }
}
