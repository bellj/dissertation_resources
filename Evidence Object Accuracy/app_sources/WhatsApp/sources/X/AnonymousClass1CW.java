package X;

import com.whatsapp.util.Log;
import java.util.ArrayList;

/* renamed from: X.1CW  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1CW {
    public AbstractC16130oV A00;
    public Integer A01;
    public final C22370yy A02;
    public final ArrayList A03 = new ArrayList();

    public AnonymousClass1CW(C22370yy r2) {
        this.A02 = r2;
    }

    public final void A00(AbstractC16130oV r3, int i) {
        Integer num;
        StringBuilder sb = new StringBuilder("statusdownload/queue-status-download ");
        sb.append(r3.A0z.A01);
        sb.append(" ");
        sb.append(r3.A0B());
        sb.append(", mode = ");
        sb.append(i);
        Log.i(sb.toString());
        if (!r3.equals(this.A00) || ((num = this.A01) != null && i < num.intValue())) {
            this.A00 = r3;
            this.A01 = Integer.valueOf(i);
            this.A02.A07(new AnonymousClass3Y7(this), r3, i);
        }
    }
}
