package X;

/* renamed from: X.4VN  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4VN {
    public C94354bf A00;
    public byte[] A01;

    public AnonymousClass4VN(C94354bf r1, byte[] bArr) {
        this.A01 = bArr;
        this.A00 = r1;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("WtCertificateEntry{certData=");
        A0k.append(AnonymousClass3JS.A03(this.A01));
        A0k.append(", wtExtensions=");
        A0k.append(this.A00);
        return C12970iu.A0v(A0k);
    }
}
