package X;

import android.database.ContentObserver;
import android.database.Cursor;
import com.whatsapp.util.Log;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.1iL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C35591iL implements AbstractC35581iK {
    public int A00 = 0;
    public C16520pA A01;
    public final C15650ng A02;
    public final C15660nh A03;
    public final AbstractC14640lm A04;
    public final AnonymousClass19O A05;
    public final Map A06 = new HashMap();

    public C35591iL(C15650ng r2, C15660nh r3, AbstractC14640lm r4, AnonymousClass19O r5) {
        this.A02 = r2;
        this.A03 = r3;
        this.A05 = r5;
        this.A04 = r4;
    }

    public Cursor A00() {
        C15660nh r8 = this.A03;
        AbstractC14640lm r7 = this.A04;
        AnonymousClass009.A05(r7);
        StringBuilder sb = new StringBuilder("mediamsgstore/getMediaMessagesCursor:");
        sb.append(r7);
        Log.i(sb.toString());
        C16310on A01 = r8.A0C.get();
        try {
            Cursor A09 = A01.A03.A09(C32331bz.A07, new String[]{String.valueOf(r8.A06.A02(r7))});
            A01.close();
            return A09;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    /* renamed from: A01 */
    public AbstractC35601iM AEC(int i) {
        AbstractC35601iM r1;
        Map map = this.A06;
        Integer valueOf = Integer.valueOf(i);
        AbstractC35601iM r12 = (AbstractC35601iM) map.get(valueOf);
        if (this.A01 == null || r12 != null) {
            return r12;
        }
        synchronized (this) {
            if (this.A01.moveToPosition(i)) {
                C16520pA r0 = this.A01;
                AnonymousClass19O r13 = this.A05;
                AbstractC16130oV A00 = r0.A00();
                AnonymousClass009.A05(A00);
                r1 = AnonymousClass3GG.A00(A00, r13);
                map.put(valueOf, r1);
            } else {
                r1 = null;
            }
        }
        return r1;
    }

    @Override // X.AbstractC35581iK
    public HashMap AAz() {
        return new HashMap();
    }

    @Override // X.AbstractC35581iK
    public void AaX() {
        C16520pA r2 = this.A01;
        if (r2 != null) {
            Cursor A00 = A00();
            r2.A01.close();
            r2.A01 = A00;
            r2.A00 = -1;
            r2.moveToPosition(-1);
        }
        this.A06.clear();
        this.A00 = 0;
    }

    @Override // X.AbstractC35581iK
    public void close() {
        C16520pA r0 = this.A01;
        if (r0 != null) {
            r0.close();
        }
    }

    @Override // X.AbstractC35581iK
    public int getCount() {
        C16520pA r0 = this.A01;
        if (r0 == null) {
            return 0;
        }
        return r0.getCount() - this.A00;
    }

    @Override // X.AbstractC35581iK
    public boolean isEmpty() {
        return getCount() == 0;
    }

    @Override // X.AbstractC35581iK
    public void registerContentObserver(ContentObserver contentObserver) {
        C16520pA r0 = this.A01;
        if (r0 != null) {
            r0.registerContentObserver(contentObserver);
        }
    }

    @Override // X.AbstractC35581iK
    public void unregisterContentObserver(ContentObserver contentObserver) {
        C16520pA r0 = this.A01;
        if (r0 != null) {
            r0.unregisterContentObserver(contentObserver);
        }
    }
}
