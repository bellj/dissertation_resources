package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0100100_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0101000_I0;
import com.facebook.redex.RunnableBRunnable0Shape11S0100000_I0_11;
import com.whatsapp.util.Log;

/* renamed from: X.1ya  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C44241ya implements AbstractC21730xt {
    public C44251yb A00;
    public final AbstractC15710nm A01;
    public final C14900mE A02;
    public final C15570nT A03;
    public final C18640sm A04;
    public final AnonymousClass018 A05;
    public final C17220qS A06;

    public C44241ya(AbstractC15710nm r1, C14900mE r2, C15570nT r3, C18640sm r4, AnonymousClass018 r5, C17220qS r6) {
        this.A02 = r2;
        this.A01 = r1;
        this.A03 = r3;
        this.A06 = r6;
        this.A05 = r5;
        this.A04 = r4;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        Log.e("RequestBusinessActivityReportProtocolHelper/delivery-error");
        if (this.A00 != null) {
            this.A02.A0I(new RunnableBRunnable0Shape11S0100000_I0_11(this, 20));
        }
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r6, String str) {
        Log.e("RequestBusinessActivityReportProtocolHelper/onError");
        int A00 = C41151sz.A00(r6);
        if (this.A00 != null) {
            this.A02.A0I(new RunnableBRunnable0Shape0S0101000_I0(this, A00, 23));
        }
        AbstractC15710nm r3 = this.A01;
        StringBuilder sb = new StringBuilder("error_code=");
        sb.append(A00);
        r3.AaV("RequestBusinessActivityReportProtocolHelper/get business activity error", sb.toString(), true);
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r6, String str) {
        AnonymousClass1V8 A0E = r6.A0E("p2b");
        if (A0E != null) {
            long A08 = A0E.A08("timestamp", 0) * 1000;
            if (this.A00 != null) {
                this.A02.A0I(new RunnableBRunnable0Shape0S0100100_I0(this, A08, 8));
            }
        } else if (this.A00 != null) {
            this.A02.A0I(new RunnableBRunnable0Shape11S0100000_I0_11(this, 19));
        }
    }
}
