package X;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONObject;

/* renamed from: X.1pv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C39451pv {
    public final String A00;
    public final String A01;
    public final String A02;
    public final Map A03;

    public C39451pv(String str, String str2, String str3, String str4, Map map) {
        this.A01 = str;
        this.A00 = str3;
        this.A02 = str4;
        map = map == null ? new HashMap() : map;
        this.A03 = map;
        map.put(Integer.toString(-1), str2);
    }

    public static C39451pv A00(String str) {
        Object obj;
        JSONObject jSONObject = new JSONObject(str);
        Map A01 = A01(jSONObject);
        if (A01 == null) {
            obj = "";
        } else {
            obj = A01.get(Integer.toString(-1));
        }
        String string = jSONObject.getString("name");
        AnonymousClass009.A05(obj);
        return new C39451pv(string, (String) obj, jSONObject.optString("locale_lang", null), jSONObject.optString("url", null), A01);
    }

    public static Map A01(JSONObject jSONObject) {
        JSONObject optJSONObject = jSONObject.optJSONObject("bundles");
        if (optJSONObject == null) {
            return null;
        }
        HashMap hashMap = new HashMap();
        Iterator<String> keys = optJSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            hashMap.put(next, optJSONObject.getString(next));
        }
        return hashMap;
    }

    public String A02() {
        Object obj = this.A03.get(Integer.toString(-1));
        AnonymousClass009.A05(obj);
        return (String) obj;
    }

    public String A03() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("name", this.A01);
        jSONObject.put("locale_lang", this.A00);
        jSONObject.put("url", this.A02);
        jSONObject.put("bundles", new JSONObject(this.A03));
        return jSONObject.toString();
    }

    public String A04(int i) {
        String str = (String) this.A03.get(Integer.toString(i));
        return str == null ? "" : str;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C39451pv)) {
            return false;
        }
        C39451pv r4 = (C39451pv) obj;
        if (!C29941Vi.A00(this.A01, r4.A01) || !C29941Vi.A00(this.A00, r4.A00) || !C29941Vi.A00(this.A02, r4.A02) || !C29941Vi.A00(this.A03, r4.A03)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A01, this.A00, this.A02, this.A03});
    }
}
