package X;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import com.whatsapp.AbstractAppShellDelegate;
import com.whatsapp.ApplicationLike;
import com.whatsapp.util.Log;
import java.io.File;

/* renamed from: X.002  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass002 extends Application implements AnonymousClass003 {
    public static final AnonymousClass006 appStartStat = AnonymousClass006.A03;
    public ApplicationLike delegate;
    public volatile AnonymousClass046 waResourcesWrapper;

    @Override // android.content.ContextWrapper
    public void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        AnonymousClass009.A01 = true;
        File file = new File(getFilesDir(), "Logs");
        if (!Log.logDirRef.compareAndSet(null, file)) {
            throw new IllegalStateException("log application context already assigned");
        }
        Log.logFile = new File(file, "whatsapp.log");
        Log.logTempFile = new File(file, "whatsapp.tmp");
        Log.logFileLatch.countDown();
        Log.level = 3;
        StringBuilder sb = new StringBuilder("==== logfile version=");
        sb.append("2.22.17.70");
        sb.append(" level=");
        sb.append(3);
        sb.append("====");
        Log.log("LL_I ", sb.toString());
        AnonymousClass00F.A00();
        AnonymousClass00G.A01(this);
        configureCrashLogging(this);
    }

    private void configureCrashLogging(Context context) {
        Thread.setDefaultUncaughtExceptionHandler(new AnonymousClass00M(context, this));
    }

    public ApplicationLike createDelegate() {
        return new AbstractAppShellDelegate(this, appStartStat);
    }

    @Override // android.content.Context, android.content.ContextWrapper
    public Resources getResources() {
        if (Boolean.TRUE.equals(AnonymousClass009.A01)) {
            return super.getResources();
        }
        if (this.waResourcesWrapper == null) {
            synchronized (this) {
                if (this.waResourcesWrapper == null) {
                    this.waResourcesWrapper = AnonymousClass046.A00(super.getResources(), ((AnonymousClass01J) AnonymousClass01M.A00(this, AnonymousClass01J.class)).Ag8());
                }
            }
        }
        return this.waResourcesWrapper;
    }

    @Override // X.AnonymousClass003
    public C05180Oo getWorkManagerConfiguration() {
        Log.i("work-manager/configuration/created");
        AnonymousClass0NM r2 = new AnonymousClass0NM();
        r2.A02 = 1000;
        r2.A01 = Integer.MAX_VALUE;
        r2.A00 = 2;
        return new C05180Oo(r2);
    }

    @Override // android.app.Application, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        ApplicationLike applicationLike = this.delegate;
        AnonymousClass009.A05(applicationLike);
        applicationLike.onConfigurationChanged(configuration);
    }

    @Override // android.app.Application
    public void onCreate() {
        super.onCreate();
        AbstractAppShellDelegate abstractAppShellDelegate = new AbstractAppShellDelegate(this, appStartStat);
        this.delegate = abstractAppShellDelegate;
        abstractAppShellDelegate.onCreate();
    }
}
