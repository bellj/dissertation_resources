package X;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.0ex  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class ThreadFactoryC10640ex implements ThreadFactory {
    public final AtomicInteger A00 = new AtomicInteger(1);

    @Override // java.util.concurrent.ThreadFactory
    public Thread newThread(Runnable runnable) {
        StringBuilder sb = new StringBuilder("ModernAsyncTask #");
        sb.append(this.A00.getAndIncrement());
        return new Thread(runnable, sb.toString());
    }
}
