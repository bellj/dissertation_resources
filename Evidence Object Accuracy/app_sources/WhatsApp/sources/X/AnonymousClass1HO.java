package X;

import android.os.SystemClock;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.1HO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1HO {
    public final long A00;
    public final C16120oU A01;
    public final String A02;
    public final AtomicBoolean A03 = new AtomicBoolean(false);

    public AnonymousClass1HO(C16120oU r3, String str) {
        this.A01 = r3;
        this.A02 = str;
        this.A00 = SystemClock.elapsedRealtime();
    }

    public void A00(String str) {
        if (this.A03.compareAndSet(false, true)) {
            String str2 = this.A02;
            AnonymousClass43S r1 = new AnonymousClass43S();
            r1.A00 = Long.valueOf(SystemClock.elapsedRealtime() - this.A00);
            r1.A02 = str2;
            r1.A01 = str;
            this.A01.A07(r1);
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("PerfTimer(");
        sb.append(this.A02);
        sb.append(") already stopped");
        AnonymousClass009.A0A(sb.toString(), false);
    }
}
