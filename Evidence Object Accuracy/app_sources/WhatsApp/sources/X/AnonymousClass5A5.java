package X;

import com.whatsapp.companiondevice.LinkedDevicesSharedViewModel;

/* renamed from: X.5A5  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5A5 implements AbstractC14590lg {
    public final /* synthetic */ LinkedDevicesSharedViewModel A00;

    public AnonymousClass5A5(LinkedDevicesSharedViewModel linkedDevicesSharedViewModel) {
        this.A00 = linkedDevicesSharedViewModel;
    }

    @Override // X.AbstractC14590lg
    public /* bridge */ /* synthetic */ void accept(Object obj) {
        this.A00.A0S.A0B(obj);
    }
}
