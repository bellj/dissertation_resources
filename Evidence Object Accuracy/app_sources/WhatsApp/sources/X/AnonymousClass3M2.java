package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.redex.IDxCreatorShape1S0000000_2_I1;

/* renamed from: X.3M2  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3M2 implements Parcelable {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape1S0000000_2_I1(66);
    public final int A00;
    public final String A01;
    public final String A02;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass3M2(Parcel parcel) {
        this.A00 = parcel.readInt();
        this.A02 = C12990iw.A0p(parcel);
        this.A01 = parcel.readString();
    }

    public AnonymousClass3M2(String str, String str2, int i) {
        this.A01 = str;
        this.A02 = str2;
        this.A00 = i;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass3M2 r5 = (AnonymousClass3M2) obj;
            if (!(this.A01 == r5.A01 && this.A02.equals(r5.A02) && this.A00 == r5.A00)) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        int i = 527 + this.A00;
        String str = this.A01;
        if (str != null) {
            i = (i * 31) + str.hashCode();
        }
        return (i * 31) + this.A02.hashCode();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.A00);
        parcel.writeString(this.A02);
        parcel.writeString(this.A01);
    }
}
