package X;

/* renamed from: X.5Xl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public interface AbstractC116925Xl {
    public static final AnonymousClass1TK A00;
    public static final AnonymousClass1TK A01 = C72453ed.A12("1.3.6.1.5.5.7.48.1.6");
    public static final AnonymousClass1TK A02 = C72453ed.A12("1.3.6.1.5.5.7.48.1.1");
    public static final AnonymousClass1TK A03 = C72453ed.A12("1.3.6.1.5.5.7.48.1.3");
    public static final AnonymousClass1TK A04;
    public static final AnonymousClass1TK A05 = C72453ed.A12("1.3.6.1.5.5.7.48.1.5");
    public static final AnonymousClass1TK A06 = C72453ed.A12("1.3.6.1.5.5.7.48.1.2");
    public static final AnonymousClass1TK A07;
    public static final AnonymousClass1TK A08 = C72453ed.A12("1.3.6.1.5.5.7.48.1.4");
    public static final AnonymousClass1TK A09 = C72453ed.A12("1.3.6.1.5.5.7.48.1.7");

    static {
        AnonymousClass1TK A12 = C72453ed.A12("1.3.6.1.5.5.7.48.1");
        A00 = A12;
        A07 = AnonymousClass1TL.A02("8", A12);
        A04 = AnonymousClass1TL.A02("9", A12);
    }
}
