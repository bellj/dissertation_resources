package X;

import java.util.Arrays;
import java.util.List;

/* renamed from: X.3tv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C81383tv extends C113265Gv {
    public final List componentsInCycle;

    public C81383tv(List list) {
        super(C12960it.A0d(Arrays.toString(list.toArray()), C12960it.A0k("Dependency cycle detected: ")));
        this.componentsInCycle = list;
    }
}
