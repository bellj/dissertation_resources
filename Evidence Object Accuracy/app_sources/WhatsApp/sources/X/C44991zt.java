package X;

import android.os.ConditionVariable;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.Calendar;
import java.util.Locale;

/* renamed from: X.1zt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C44991zt implements AbstractC45001zu {
    public final /* synthetic */ ConditionVariable A00;
    public final /* synthetic */ C18260sA A01;
    public final /* synthetic */ C44981zs A02;

    public C44991zt(ConditionVariable conditionVariable, C18260sA r2, C44981zs r3) {
        this.A01 = r2;
        this.A00 = conditionVariable;
        this.A02 = r3;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v20, resolved type: boolean */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0115, code lost:
        if (r0.intValue() != 0) goto L_0x0117;
     */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00f2  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00fd  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0110  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0130  */
    @Override // X.AbstractC45001zu
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AM2(int r13) {
        /*
        // Method dump skipped, instructions count: 422
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C44991zt.AM2(int):void");
    }

    @Override // X.AbstractC45001zu
    public void ANC() {
        C18260sA r4 = this.A01;
        r4.A00 = false;
        C14900mE r7 = r4.A03;
        r7.A06(R.string.msg_store_backup_db_title, R.string.settings_backup_db_now_message);
        Calendar instance = Calendar.getInstance();
        instance.set(14, 0);
        instance.set(13, 0);
        instance.set(12, 0);
        instance.set(11, 2);
        long timeInMillis = instance.getTimeInMillis();
        AnonymousClass018 r6 = r4.A0G;
        r7.A0K(AnonymousClass3JK.A01(r6, r4.A0E.A00.getString(R.string.msg_store_backup_db_message, AnonymousClass3JK.A00(r6, timeInMillis)), timeInMillis));
    }

    @Override // X.AbstractC45001zu
    public void AUO(int i) {
        String format = String.format(Locale.ENGLISH, "app/backup/progress/%d%%", Integer.valueOf(i));
        if (i % 10 == 0) {
            Log.i(format);
        }
        Calendar instance = Calendar.getInstance();
        instance.set(14, 0);
        instance.set(13, 0);
        instance.set(12, 0);
        instance.set(11, 2);
        long timeInMillis = instance.getTimeInMillis();
        C18260sA r0 = this.A01;
        C14900mE r10 = r0.A03;
        AnonymousClass018 r11 = r0.A0G;
        r10.A0K(AnonymousClass3JK.A01(r11, r0.A0E.A00.getString(R.string.msg_store_backup_db_message_with_percentage_placeholder, r11.A0K().format(((double) i) / 100.0d), AnonymousClass3JK.A00(r11, timeInMillis)), timeInMillis));
    }
}
