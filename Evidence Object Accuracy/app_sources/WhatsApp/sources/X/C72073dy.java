package X;

/* renamed from: X.3dy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C72073dy extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ int $position;
    public final /* synthetic */ C54382gd this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C72073dy(C54382gd r2, int i) {
        super(0);
        this.this$0 = r2;
        this.$position = i;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        C54382gd r3 = this.this$0;
        int i = this.$position;
        r3.A01 = "";
        r3.A00 = i;
        r3.A07.AJ4(r3.A06.get(i));
        r3.A02();
        return AnonymousClass1WZ.A00;
    }
}
