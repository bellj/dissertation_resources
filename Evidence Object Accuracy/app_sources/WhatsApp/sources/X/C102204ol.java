package X;

import android.widget.AbsListView;
import com.whatsapp.chatinfo.ListChatInfo;

/* renamed from: X.4ol  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C102204ol implements AbsListView.OnScrollListener {
    public final /* synthetic */ ListChatInfo A00;

    @Override // android.widget.AbsListView.OnScrollListener
    public void onScrollStateChanged(AbsListView absListView, int i) {
    }

    public C102204ol(ListChatInfo listChatInfo) {
        this.A00 = listChatInfo;
    }

    @Override // android.widget.AbsListView.OnScrollListener
    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        this.A00.A2y();
    }
}
