package X;

import com.facebook.redex.RunnableBRunnable0Shape0S1100000_I0;
import java.io.File;

/* renamed from: X.19k  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C254419k {
    public final C254619m A00;
    public final AbstractC14440lR A01;

    public C254419k(C254619m r1, AbstractC14440lR r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public void A00(File file) {
        if (file != null) {
            this.A01.Ab2(new RunnableBRunnable0Shape0S1100000_I0(38, file.getName().split("@")[0], this));
        }
    }
}
