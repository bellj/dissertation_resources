package X;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.math.BigDecimal;
import java.math.RoundingMode;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.63p  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1316563p implements Parcelable {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(18);
    public final long A00;
    public final long A01;
    public final AnonymousClass6F2 A02;
    public final String A03;
    public final String A04;
    public final BigDecimal A05;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C1316563p(AnonymousClass6F2 r1, String str, String str2, BigDecimal bigDecimal, long j, long j2) {
        this.A01 = j;
        this.A00 = j2;
        this.A02 = r1;
        this.A05 = bigDecimal;
        this.A03 = str;
        this.A04 = str2;
    }

    public static C1316563p A00(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            JSONObject A05 = C13000ix.A05(str);
            long optLong = A05.optLong("id", -1);
            long optLong2 = A05.optLong("expiry", -1);
            return new C1316563p(AnonymousClass6F2.A01(A05.optString("fee", "")), A05.optString("source-iso-code", ""), A05.optString("target-iso-code", ""), new BigDecimal(A05.optString("rate")), optLong, optLong2);
        } catch (JSONException e) {
            Log.w("PAY: ExchangeQuote.Quote fromJsonString threw: ", e);
            return null;
        }
    }

    public static String A01(Context context, AnonymousClass018 r6, AbstractC30791Yv r7, AbstractC30791Yv r8, BigDecimal bigDecimal) {
        int i = 4;
        if (BigDecimal.ONE.equals(bigDecimal)) {
            i = 0;
        }
        String AAB = r7.AAB(r6, bigDecimal.setScale(i, RoundingMode.HALF_EVEN), 2);
        Object[] objArr = new Object[2];
        objArr[0] = r8.AAB(r6, BigDecimal.ONE, 2);
        return C12960it.A0X(context, AAB, objArr, 1, R.string.novi_send_money_review_transaction_exchange_rate);
    }

    public JSONObject A02() {
        JSONObject A0a = C117295Zj.A0a();
        try {
            A0a.put("id", this.A01);
            A0a.put("expiry", this.A00);
            AnonymousClass6F2 r1 = this.A02;
            if (r1 != null) {
                C117315Zl.A0W(r1, "fee", A0a);
            }
            A0a.put("rate", this.A05.toString());
            A0a.put("source-iso-code", this.A03);
            A0a.put("target-iso-code", this.A04);
            return A0a;
        } catch (JSONException e) {
            Log.w("PAY: ExchangeQuote.Quote toJson threw: ", e);
            return A0a;
        }
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.A01);
        parcel.writeLong(this.A00);
        parcel.writeParcelable(this.A02, i);
        parcel.writeSerializable(this.A05);
        parcel.writeString(this.A03);
        parcel.writeString(this.A04);
    }
}
