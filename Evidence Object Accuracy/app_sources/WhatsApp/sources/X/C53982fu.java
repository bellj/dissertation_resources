package X;

import android.content.Context;
import java.util.ArrayList;

/* renamed from: X.2fu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53982fu extends AnonymousClass0ER {
    public final C15650ng A00;
    public final long[] A01;

    public C53982fu(Context context, C15650ng r2, long[] jArr) {
        super(context);
        this.A01 = jArr;
        this.A00 = r2;
    }

    @Override // X.AnonymousClass0QL
    public void A01() {
        A00();
    }

    @Override // X.AnonymousClass0QL
    public void A02() {
        A00();
    }

    @Override // X.AnonymousClass0QL
    public void A03() {
        boolean z = this.A03;
        this.A03 = false;
        this.A04 |= z;
        A09();
    }

    @Override // X.AnonymousClass0QL
    public /* bridge */ /* synthetic */ void A04(Object obj) {
        if (!this.A05 && this.A06) {
            super.A04(obj);
        }
    }

    @Override // X.AnonymousClass0ER
    public /* bridge */ /* synthetic */ Object A06() {
        ArrayList A0l = C12960it.A0l();
        long[] jArr = this.A01;
        for (long j : jArr) {
            synchronized (this) {
                if (C12960it.A1W(((AnonymousClass0ER) this).A01)) {
                    throw new AnonymousClass04U(null);
                }
            }
            AbstractC15340mz A00 = this.A00.A0K.A00(j);
            if (A00 instanceof AbstractC16130oV) {
                A0l.add(A00);
            }
        }
        return A0l;
    }
}
