package X;

import android.os.Build;
import android.os.Process;
import com.facebook.msys.mci.DefaultCrypto;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/* renamed from: X.00F  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass00F {
    public static final byte[] A00;

    static {
        StringBuilder sb = new StringBuilder();
        String str = Build.FINGERPRINT;
        if (str != null) {
            sb.append(str);
        }
        String str2 = Build.SERIAL;
        if (str2 != null) {
            sb.append(str2);
        }
        try {
            A00 = sb.toString().getBytes(DefaultCrypto.UTF_8);
        } catch (UnsupportedEncodingException unused) {
            throw new RuntimeException("UTF-8 encoding not supported");
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x00b1  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0119 A[Catch: all -> 0x0138, TryCatch #2 {, blocks: (B:15:0x007e, B:17:0x0087, B:19:0x008a, B:22:0x009b, B:25:0x00b2, B:26:0x00db, B:27:0x00e1, B:29:0x00ef, B:31:0x00f1, B:32:0x010f, B:34:0x0111, B:35:0x0118, B:36:0x0119, B:37:0x0137), top: B:46:0x007e, inners: #0 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A00() {
        /*
        // Method dump skipped, instructions count: 316
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass00F.A00():void");
    }

    public static byte[] A01() {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
            dataOutputStream.writeLong(System.currentTimeMillis());
            dataOutputStream.writeLong(System.nanoTime());
            dataOutputStream.writeInt(Process.myPid());
            dataOutputStream.writeInt(Process.myUid());
            dataOutputStream.write(A00);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            dataOutputStream.close();
            byteArrayOutputStream.close();
            return byteArray;
        } catch (IOException e) {
            throw new SecurityException("Failed to generate seed", e);
        }
    }
}
