package X;

import com.whatsapp.camera.CameraActivity;
import com.whatsapp.nativelibloader.WhatsAppLibLoader;

/* renamed from: X.2GI  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2GI extends ActivityC13790kL {
    public boolean A00 = false;

    public AnonymousClass2GI() {
        A0R(new C102884pr(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            CameraActivity cameraActivity = (CameraActivity) this;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r3 = r2.A1E;
            ((ActivityC13830kP) cameraActivity).A05 = (AbstractC14440lR) r3.ANe.get();
            ((ActivityC13810kN) cameraActivity).A0C = (C14850m9) r3.A04.get();
            ((ActivityC13810kN) cameraActivity).A05 = (C14900mE) r3.A8X.get();
            ((ActivityC13810kN) cameraActivity).A03 = (AbstractC15710nm) r3.A4o.get();
            ((ActivityC13810kN) cameraActivity).A04 = (C14330lG) r3.A7B.get();
            ((ActivityC13810kN) cameraActivity).A0B = (AnonymousClass19M) r3.A6R.get();
            ((ActivityC13810kN) cameraActivity).A0A = (C18470sV) r3.AK8.get();
            ((ActivityC13810kN) cameraActivity).A06 = (C15450nH) r3.AII.get();
            ((ActivityC13810kN) cameraActivity).A08 = (AnonymousClass01d) r3.ALI.get();
            ((ActivityC13810kN) cameraActivity).A0D = (C18810t5) r3.AMu.get();
            ((ActivityC13810kN) cameraActivity).A09 = (C14820m6) r3.AN3.get();
            ((ActivityC13810kN) cameraActivity).A07 = (C18640sm) r3.A3u.get();
            ((ActivityC13790kL) cameraActivity).A05 = (C14830m7) r3.ALb.get();
            ((ActivityC13790kL) cameraActivity).A0D = (C252718t) r3.A9K.get();
            ((ActivityC13790kL) cameraActivity).A01 = (C15570nT) r3.AAr.get();
            ((ActivityC13790kL) cameraActivity).A04 = (C15810nw) r3.A73.get();
            ((ActivityC13790kL) cameraActivity).A09 = r2.A06();
            ((ActivityC13790kL) cameraActivity).A06 = (C14950mJ) r3.AKf.get();
            ((ActivityC13790kL) cameraActivity).A00 = (AnonymousClass12P) r3.A0H.get();
            ((ActivityC13790kL) cameraActivity).A02 = (C252818u) r3.AMy.get();
            ((ActivityC13790kL) cameraActivity).A03 = (C22670zS) r3.A0V.get();
            ((ActivityC13790kL) cameraActivity).A0A = (C21840y4) r3.ACr.get();
            ((ActivityC13790kL) cameraActivity).A07 = (C15880o3) r3.ACF.get();
            ((ActivityC13790kL) cameraActivity).A0C = (C21820y2) r3.AHx.get();
            ((ActivityC13790kL) cameraActivity).A0B = (C15510nN) r3.AHZ.get();
            ((ActivityC13790kL) cameraActivity).A08 = (C249317l) r3.A8B.get();
            cameraActivity.A01 = (C18720su) r3.A2c.get();
            cameraActivity.A06 = (C245115u) r3.A7s.get();
            cameraActivity.A03 = (AnonymousClass2GJ) r2.A0C.get();
            cameraActivity.A09 = r2.A0J();
            cameraActivity.A08 = (C21200x2) r3.A2q.get();
            cameraActivity.A07 = (WhatsAppLibLoader) r3.ANa.get();
            cameraActivity.A05 = (C16490p7) r3.ACJ.get();
            cameraActivity.A04 = (C15890o4) r3.AN1.get();
            cameraActivity.A0A = C18000rk.A00(r2.A15);
        }
    }
}
