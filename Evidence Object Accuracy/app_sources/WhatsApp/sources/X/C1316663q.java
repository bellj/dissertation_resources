package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.63q  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1316663q implements Parcelable {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(38);
    public int A00 = 0;
    public C130625zi A01;
    public final String A02;
    public final String A03;
    public final String A04;
    public final List A05;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C1316663q(C130625zi r2, String str, String str2, String str3, List list) {
        this.A05 = list;
        this.A01 = r2;
        this.A04 = str;
        this.A03 = str2;
        this.A02 = str3;
    }

    public /* synthetic */ C1316663q(Parcel parcel) {
        C130625zi r0;
        this.A00 = parcel.readInt();
        this.A05 = C12960it.A0l();
        int readInt = parcel.readInt();
        for (int i = 0; i < readInt; i++) {
            this.A05.add(new C126915te(parcel.readString(), parcel.readString()));
        }
        this.A04 = C12990iw.A0p(parcel);
        this.A03 = C12990iw.A0p(parcel);
        String readString = parcel.readString();
        String readString2 = parcel.readString();
        String readString3 = parcel.readString();
        String readString4 = parcel.readString();
        if (TextUtils.isEmpty(readString) || TextUtils.isEmpty(readString2) || TextUtils.isEmpty(readString2)) {
            r0 = null;
        } else {
            r0 = new C130625zi(readString, readString2, readString3, readString4);
        }
        this.A01 = r0;
        this.A02 = parcel.readString();
    }

    public static C1316663q A00(String str) {
        C130625zi r6;
        String str2 = null;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        JSONObject A05 = C13000ix.A05(str);
        ArrayList A0l = C12960it.A0l();
        JSONArray optJSONArray = A05.optJSONArray("choices");
        int i = 0;
        while (true) {
            AnonymousClass009.A05(optJSONArray);
            if (i >= optJSONArray.length()) {
                break;
            }
            JSONObject jSONObject = optJSONArray.getJSONObject(i);
            A0l.add(new C126915te(jSONObject.optString("primary_step_up"), jSONObject.optString("alternative_step_up")));
            i++;
        }
        JSONObject optJSONObject = A05.optJSONObject("message");
        String optString = A05.optString("action_id");
        if (optJSONObject != null) {
            r6 = new C130625zi(optJSONObject.getString("title"), optJSONObject.getString("description"), optJSONObject.getString("primary_action"), optJSONObject.optString("secondary_action"));
        } else {
            r6 = null;
        }
        String optString2 = A05.optString("metadata");
        String optString3 = A05.optString("entry_flow");
        if (!TextUtils.isEmpty(optString)) {
            str2 = optString;
        }
        return new C1316663q(r6, optString2, optString3, str2, A0l);
    }

    public JSONObject A01() {
        JSONObject A0a = C117295Zj.A0a();
        try {
            A0a.put("entry_flow", this.A03);
            A0a.put("metadata", this.A04);
            JSONArray A0L = C117315Zl.A0L();
            for (C126915te r4 : this.A05) {
                JSONObject A0a2 = C117295Zj.A0a();
                A0a2.put("primary_step_up", r4.A01);
                String str = r4.A00;
                if (str != null) {
                    A0a2.put("alternative_step_up", str);
                }
                A0L.put(A0a2);
            }
            A0a.put("choices", A0L);
            C130625zi r42 = this.A01;
            if (r42 != null) {
                JSONObject A0a3 = C117295Zj.A0a();
                A0a3.put("title", r42.A03);
                A0a3.put("description", r42.A00);
                A0a3.put("primary_action", r42.A01);
                String str2 = r42.A02;
                if (!TextUtils.isEmpty(str2)) {
                    A0a3.put("secondary_action", str2);
                }
                A0a.put("message", A0a3);
            }
            Object obj = this.A02;
            if (obj != null) {
                A0a.put("action_id", obj);
                return A0a;
            }
        } catch (JSONException unused) {
            Log.w("PAY: StepUp toJson threw exception");
        }
        return A0a;
    }

    public boolean A02() {
        List list = this.A05;
        if (list.size() <= 0 || !"MANUAL_REVIEW__AUTO_TRIGGERED".equals(((C126915te) list.get(0)).A01)) {
            return false;
        }
        return true;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        String str;
        String str2;
        String str3;
        parcel.writeInt(this.A00);
        List<C126915te> list = this.A05;
        parcel.writeInt(list.size());
        for (C126915te r1 : list) {
            parcel.writeString(r1.A01);
            parcel.writeString(r1.A00);
        }
        parcel.writeString(this.A04);
        parcel.writeString(this.A03);
        C130625zi r2 = this.A01;
        String str4 = null;
        if (r2 == null) {
            str = null;
        } else {
            str = r2.A03;
        }
        parcel.writeString(str);
        if (r2 == null) {
            str2 = null;
        } else {
            str2 = r2.A00;
        }
        parcel.writeString(str2);
        if (r2 == null) {
            str3 = null;
        } else {
            str3 = r2.A01;
        }
        parcel.writeString(str3);
        if (r2 != null) {
            str4 = r2.A02;
        }
        parcel.writeString(str4);
        parcel.writeString(this.A02);
    }
}
