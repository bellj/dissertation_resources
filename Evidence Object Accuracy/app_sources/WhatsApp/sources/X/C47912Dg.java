package X;

import android.util.Pair;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jobqueue.job.ReceiptMultiTargetProcessingJob;
import java.util.List;

/* renamed from: X.2Dg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C47912Dg implements AbstractC47922Dh {
    public final int A00;
    public final Jid A01;
    public final AnonymousClass1IS A02;
    public final C32141bg A03;
    public final AnonymousClass1OT A04;
    public final List A05;
    public final boolean A06;

    public C47912Dg(Jid jid, AnonymousClass1IS r2, C32141bg r3, AnonymousClass1OT r4, List list, int i, boolean z) {
        this.A02 = r2;
        this.A01 = jid;
        this.A00 = i;
        this.A05 = list;
        this.A04 = r4;
        this.A06 = z;
        this.A03 = r3;
    }

    @Override // X.AbstractC47922Dh
    public boolean AJo() {
        return this.A06;
    }

    @Override // X.AbstractC47922Dh
    public AnonymousClass1IS AKM(int i) {
        return this.A02;
    }

    @Override // X.AbstractC47922Dh
    public DeviceJid AYy(int i) {
        return (DeviceJid) ((Pair) this.A05.get(i)).first;
    }

    @Override // X.AbstractC47922Dh
    public C32141bg AZx() {
        return this.A03;
    }

    @Override // X.AbstractC47922Dh
    public Jid AaD() {
        return this.A01;
    }

    @Override // X.AbstractC47922Dh
    public void AbL(C20670w8 r7, int i) {
        List list = this.A05;
        List subList = list.subList(i, list.size());
        AnonymousClass1IS r2 = this.A02;
        r7.A00(new ReceiptMultiTargetProcessingJob(this.A01, r2, this.A03, subList, this.A00));
    }

    @Override // X.AbstractC47922Dh
    public AnonymousClass1OT Ae6() {
        return this.A04;
    }

    @Override // X.AbstractC47922Dh
    public int AeP() {
        return this.A00;
    }

    @Override // X.AbstractC47922Dh
    public long Aeq(int i) {
        return ((Number) ((Pair) this.A05.get(i)).second).longValue();
    }

    @Override // X.AbstractC47922Dh
    public int size() {
        return this.A05.size();
    }
}
