package X;

import android.hardware.Camera;
import com.facebook.redex.IDxCallableShape15S0100000_3_I1;
import java.util.concurrent.ExecutionException;

/* renamed from: X.60S  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass60S {
    public static int A02 = -1;
    public static volatile Camera.CameraInfo[] A03;
    public final C130055yj A00;
    public final C1308560f A01;

    public AnonymousClass60S(C130055yj r1, C1308560f r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public static void A00() {
        if (A03 == null) {
            int i = A02;
            if (i == -1) {
                i = Camera.getNumberOfCameras();
                A02 = i;
            }
            Camera.CameraInfo[] cameraInfoArr = new Camera.CameraInfo[i];
            for (int i2 = 0; i2 < A02; i2++) {
                Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
                Camera.getCameraInfo(i2, cameraInfo);
                cameraInfoArr[i2] = cameraInfo;
            }
            A03 = cameraInfoArr;
        }
    }

    public int A01(int i) {
        int A022 = A02(i);
        if (A022 != -1) {
            Camera.CameraInfo cameraInfo = A03[A022];
            if (cameraInfo != null) {
                return cameraInfo.orientation;
            }
            return 0;
        }
        throw C12990iw.A0m(C12960it.A0W(i, "Could not load CameraInfo for CameraFacing: "));
    }

    public final int A02(int i) {
        if (A03 == null) {
            A04();
        }
        if (A03 != null) {
            boolean A1W = C12970iu.A1W(i);
            for (int i2 = 0; i2 < A02; i2++) {
                if (A03[i2].facing == A1W) {
                    return i2;
                }
            }
            AnonymousClass616.A01("CameraInventory", C12960it.A0W(A1W ? 1 : 0, "Could not get CameraInfo for CameraFacing id: "));
        }
        return -1;
    }

    public int A03(int i, int i2) {
        int i3;
        if (A03 == null) {
            if (!AnonymousClass61K.A02()) {
                AnonymousClass616.A01("CameraInventory", "Loading camera info on the UI thread");
            }
            A04();
        }
        if (i2 != -1) {
            int A022 = A02(i);
            if (A022 >= A03.length) {
                AnonymousClass616.A01("CameraInventory", C12960it.A0W(A022, "No CameraInfo found for camera id: "));
            } else {
                Camera.CameraInfo cameraInfo = A03[A022];
                int i4 = ((i2 + 45) / 90) * 90;
                int i5 = cameraInfo.facing;
                int i6 = cameraInfo.orientation;
                if (i5 == 1) {
                    i3 = (i6 - i4) + 360;
                } else {
                    i3 = i6 + i4;
                }
                return i3 % 360;
            }
        }
        return 0;
    }

    public final void A04() {
        if (A03 == null) {
            C1308560f r2 = this.A01;
            if (r2.A09()) {
                A00();
                return;
            }
            try {
                r2.A01(new C118905cZ(), new IDxCallableShape15S0100000_3_I1(this, 6)).get();
            } catch (InterruptedException | ExecutionException e) {
                AnonymousClass616.A01("CameraInventory", C12960it.A0d(e.getMessage(), C12960it.A0k("failed to load camera infos: ")));
            }
        }
    }
}
