package X;

/* renamed from: X.6As  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133426As implements AnonymousClass6MV {
    public final /* synthetic */ AnonymousClass607 A00;
    public final /* synthetic */ AbstractC1311361k A01;
    public final /* synthetic */ String A02;

    public C133426As(AnonymousClass607 r1, AbstractC1311361k r2, String str) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = str;
    }

    @Override // X.AnonymousClass6MV
    public void APo(C452120p r2) {
        AbstractC1311361k r0 = this.A01;
        r0.AKk();
        r0.APo(r2);
    }

    @Override // X.AnonymousClass6MV
    public void AVE(AnonymousClass6B7 r7) {
        AbstractC1311361k r5 = this.A01;
        r5.AKl();
        AnonymousClass607 r4 = this.A00;
        C128545wH r3 = new C128545wH(r7);
        r4.A0F.A00(new C1330469g(r4, r5, r3), r3, this.A02);
    }
}
