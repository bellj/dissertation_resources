package X;

import android.content.SharedPreferences;

/* renamed from: X.19E  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass19E implements AnonymousClass19F {
    public final AnonymousClass11P A00;
    public final C14820m6 A01;

    public AnonymousClass19E(AnonymousClass11P r1, C14820m6 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public final void A00(long j, int i, boolean z) {
        SharedPreferences.Editor edit = this.A01.A00.edit();
        StringBuilder sb = new StringBuilder("ptt_saved_playback_position_");
        sb.append(j);
        SharedPreferences.Editor putInt = edit.putInt(sb.toString(), i);
        if (z) {
            putInt.commit();
        } else {
            putInt.apply();
        }
    }

    @Override // X.AnonymousClass19F
    public int AFz(long j) {
        SharedPreferences sharedPreferences = this.A01.A00;
        StringBuilder sb = new StringBuilder("ptt_saved_playback_position_");
        sb.append(j);
        return sharedPreferences.getInt(sb.toString(), -1);
    }

    @Override // X.AnonymousClass19F
    public void AaO(long j) {
        SharedPreferences.Editor edit = this.A01.A00.edit();
        StringBuilder sb = new StringBuilder("ptt_saved_playback_position_");
        sb.append(j);
        edit.remove(sb.toString()).apply();
    }

    @Override // X.AnonymousClass19F
    public void Ac1() {
        C30421Xi r0;
        C35191hP A00 = this.A00.A00();
        if (A00 != null && (r0 = A00.A0O) != null) {
            A00(r0.A11, A00.A02(), false);
        }
    }

    @Override // X.AnonymousClass19F
    public void Ac2() {
        C30421Xi r0;
        C35191hP A00 = this.A00.A00();
        if (A00 != null && (r0 = A00.A0O) != null) {
            A00(r0.A11, A00.A02(), true);
        }
    }

    @Override // X.AnonymousClass19F
    public void Acg(long j, int i) {
        A00(j, i, false);
    }
}
