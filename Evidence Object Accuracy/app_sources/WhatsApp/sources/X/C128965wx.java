package X;

import android.hardware.Camera;
import java.util.concurrent.locks.ReentrantLock;

/* renamed from: X.5wx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128965wx {
    public final C129465xl A00 = new C129465xl();
    public final C129775yH A01 = new C129775yH();
    public final C129775yH A02 = new C129775yH();
    public volatile C124805q9 A03;

    public void A00(boolean z, Camera camera) {
        C129465xl r1 = this.A00;
        ReentrantLock reentrantLock = r1.A01;
        reentrantLock.lock();
        if (camera != null) {
            try {
                if (!r1.A01()) {
                    camera.stopPreview();
                    reentrantLock.lock();
                    r1.A00 = 0;
                    reentrantLock.unlock();
                    if (z) {
                        C129775yH r12 = this.A02;
                        if (!r12.A00.isEmpty()) {
                            AnonymousClass61K.A00(new RunnableC135146Hi(this, r12.A00));
                        }
                    }
                }
            } finally {
                reentrantLock.unlock();
            }
        }
    }
}
