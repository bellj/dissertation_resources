package X;

/* renamed from: X.3Wu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68803Wu implements AbstractC116695Wl {
    public final /* synthetic */ C68883Xc A00;
    public final /* synthetic */ AbstractC44401yr A01;

    public C68803Wu(C68883Xc r1, AbstractC44401yr r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC116695Wl
    public void AOy() {
        this.A01.AOz(C12990iw.A0i("network error while refreshing token"));
    }

    @Override // X.AbstractC116695Wl
    public void APp(Exception exc) {
        this.A01.APp(exc);
    }

    @Override // X.AbstractC116695Wl
    public void AWx(C64063Ec r3) {
        AnonymousClass5UI r1 = this.A00.A03;
        AnonymousClass009.A05(r3);
        Object obj = r3.A02.A00;
        AnonymousClass009.A05(obj);
        r1.A7x((String) obj).AZO(this.A01);
    }
}
