package X;

import android.content.Context;
import android.text.Spannable;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.3J9  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass3J9 {
    public static Map A00 = new ConcurrentHashMap();
    public static final AnonymousClass3J9 A01 = new AnonymousClass47d();
    public static final AnonymousClass3J9 A02 = new C864047e();
    public static final AnonymousClass3J9 A03 = new C864147f();
    public static final AnonymousClass3J9 A04 = new C863947c();

    public abstract void A03(Context context, Spannable spannable, int i, int i2);

    /* JADX DEBUG: Multi-variable search result rejected for r23v0, resolved type: java.lang.CharSequence */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0104, code lost:
        r3 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0105, code lost:
        if (r11 != null) goto L_0x010b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0107, code lost:
        r11 = android.text.SpannableString.valueOf(r23);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x010b, code lost:
        r22.A03(r20, r11, r9, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0128, code lost:
        if (r11 == null) goto L_0x012a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass01T A00(android.content.Context r20, X.AnonymousClass018 r21, X.AnonymousClass3J9 r22, java.lang.CharSequence r23, java.util.List r24, boolean r25) {
        /*
        // Method dump skipped, instructions count: 306
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3J9.A00(android.content.Context, X.018, X.3J9, java.lang.CharSequence, java.util.List, boolean):X.01T");
    }

    public static CharSequence A01(Context context, AnonymousClass018 r7, CharSequence charSequence, List list) {
        Object obj = A00(context, r7, A02, charSequence, list, true).A00;
        AnonymousClass009.A05(obj);
        return (CharSequence) obj;
    }

    public static CharSequence A02(Context context, AnonymousClass018 r7, CharSequence charSequence, List list) {
        return (CharSequence) A00(context, r7, A04, charSequence, list, false).A00;
    }
}
