package X;

import android.view.View;
import com.whatsapp.status.playback.MessageReplyActivity;

/* renamed from: X.3sd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C80643sd extends AnonymousClass2UH {
    public final /* synthetic */ MessageReplyActivity A00;

    public C80643sd(MessageReplyActivity messageReplyActivity) {
        this.A00 = messageReplyActivity;
    }

    @Override // X.AnonymousClass2UH
    public void A00(View view, float f) {
        if (f < 0.5f) {
            MessageReplyActivity messageReplyActivity = this.A00;
            if (!messageReplyActivity.AJN()) {
                messageReplyActivity.A2g();
                messageReplyActivity.A2f();
            }
        }
    }

    @Override // X.AnonymousClass2UH
    public void A01(View view, int i) {
        if (i == 4) {
            MessageReplyActivity messageReplyActivity = this.A00;
            messageReplyActivity.A2e();
            if (!messageReplyActivity.AJN()) {
                messageReplyActivity.A2g();
                messageReplyActivity.A2f();
            }
        }
    }
}
