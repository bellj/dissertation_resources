package X;

import android.view.View;

/* renamed from: X.0Qr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05720Qr {
    public static CharSequence A00(View view) {
        return view.getStateDescription();
    }

    public static void A01(View view, CharSequence charSequence) {
        view.setStateDescription(charSequence);
    }
}
