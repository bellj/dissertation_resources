package X;

import android.util.SparseArray;

/* renamed from: X.4PX  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4PX {
    public final SparseArray A00;
    public final SparseArray A01;
    public final SparseArray A02;

    public AnonymousClass4PX(AnonymousClass4PX r4) {
        int i;
        int i2;
        SparseArray sparseArray;
        SparseArray sparseArray2;
        SparseArray sparseArray3;
        int i3 = 0;
        if (r4 == null || (sparseArray3 = r4.A02) == null) {
            i = 0;
        } else {
            i = sparseArray3.size();
        }
        this.A02 = new SparseArray(i);
        if (r4 == null || (sparseArray2 = r4.A00) == null) {
            i2 = 0;
        } else {
            i2 = sparseArray2.size();
        }
        this.A00 = new SparseArray(i2);
        if (!(r4 == null || (sparseArray = r4.A01) == null)) {
            i3 = sparseArray.size();
        }
        this.A01 = new SparseArray(i3);
    }
}
