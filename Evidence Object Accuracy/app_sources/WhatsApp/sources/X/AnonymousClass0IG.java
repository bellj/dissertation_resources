package X;

/* renamed from: X.0IG  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0IG extends AnonymousClass0eL {
    public final /* synthetic */ AnonymousClass04L A00;

    public AnonymousClass0IG(AnonymousClass04L r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass0eL, java.lang.Runnable
    public void run() {
        AnonymousClass04L r2 = this.A00;
        if (r2.A0W != null) {
            while (true) {
                AbstractC12200hX r1 = (AbstractC12200hX) r2.A0W.poll();
                if (r1 != null) {
                    r1.ASO(r2.A0M);
                } else {
                    r2.A0W = null;
                    return;
                }
            }
        }
    }
}
