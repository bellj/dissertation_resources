package X;

/* renamed from: X.0S8  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0S8 {
    public static final AnonymousClass0S8 A01 = new AnonymousClass0S8("HORIZONTAL");
    public static final AnonymousClass0S8 A02 = new AnonymousClass0S8("VERTICAL");
    public final String A00;

    public AnonymousClass0S8(String str) {
        this.A00 = str;
    }

    public String toString() {
        return this.A00;
    }
}
