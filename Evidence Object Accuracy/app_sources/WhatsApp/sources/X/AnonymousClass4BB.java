package X;

import java.util.List;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.4BB  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4BB extends Enum {
    public static final /* synthetic */ AnonymousClass4BB[] A00;
    public static final AnonymousClass4BB A01;
    public static final AnonymousClass4BB A02;
    public static final AnonymousClass4BB A03;
    public static final AnonymousClass4BB A04;
    public final Class valueType;

    static {
        AnonymousClass4BB r14 = new AnonymousClass4BB(Object.class, "OTHER", 0);
        AnonymousClass4BB r12 = new AnonymousClass4BB(Void.class, "PURE_BARCODE", 1);
        A03 = r12;
        AnonymousClass4BB r11 = new AnonymousClass4BB(List.class, "POSSIBLE_FORMATS", 2);
        AnonymousClass4BB r10 = new AnonymousClass4BB(Void.class, "TRY_HARDER", 3);
        A04 = r10;
        AnonymousClass4BB r9 = new AnonymousClass4BB(String.class, "CHARACTER_SET", 4);
        A01 = r9;
        AnonymousClass4BB r8 = new AnonymousClass4BB(int[].class, "ALLOWED_LENGTHS", 5);
        AnonymousClass4BB r7 = new AnonymousClass4BB(Void.class, "ASSUME_CODE_39_CHECK_DIGIT", 6);
        AnonymousClass4BB r6 = new AnonymousClass4BB(Void.class, "ASSUME_GS1", 7);
        AnonymousClass4BB r5 = new AnonymousClass4BB(Void.class, "RETURN_CODABAR_START_END", 8);
        AnonymousClass4BB r4 = new AnonymousClass4BB(AnonymousClass5RA.class, "NEED_RESULT_POINT_CALLBACK", 9);
        A02 = r4;
        AnonymousClass4BB r2 = new AnonymousClass4BB(int[].class, "ALLOWED_EAN_EXTENSIONS", 10);
        AnonymousClass4BB[] r1 = new AnonymousClass4BB[11];
        r1[0] = r14;
        r1[1] = r12;
        C12980iv.A1P(r11, r10, r9, r1);
        C12970iu.A1R(r8, r7, r6, r5, r1);
        r1[9] = r4;
        r1[10] = r2;
        A00 = r1;
    }

    public AnonymousClass4BB(Class cls, String str, int i) {
        this.valueType = cls;
    }

    public static AnonymousClass4BB valueOf(String str) {
        return (AnonymousClass4BB) Enum.valueOf(AnonymousClass4BB.class, str);
    }

    public static AnonymousClass4BB[] values() {
        return (AnonymousClass4BB[]) A00.clone();
    }
}
