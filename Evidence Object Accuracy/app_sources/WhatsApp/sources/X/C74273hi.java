package X;

import android.graphics.Typeface;
import com.google.android.material.chip.Chip;

/* renamed from: X.3hi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74273hi extends AnonymousClass08K {
    public final /* synthetic */ Chip A00;

    @Override // X.AnonymousClass08K
    public void A01(int i) {
    }

    public C74273hi(Chip chip) {
        this.A00 = chip;
    }

    @Override // X.AnonymousClass08K
    public void A02(Typeface typeface) {
        Chip chip = this.A00;
        chip.setText(chip.getText());
        chip.requestLayout();
        chip.invalidate();
    }
}
