package X;

import android.view.View;
import android.view.accessibility.AccessibilityNodeInfo;

/* renamed from: X.2eS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53442eS extends AnonymousClass04v {
    public final AnonymousClass28D A00;

    public C53442eS(AnonymousClass28D r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r6) {
        super.A06(view, r6);
        AnonymousClass28D r3 = this.A00;
        String A0I = r3.A0I(36);
        String A0I2 = r3.A0I(38);
        if (A0I != null) {
            r6.A02.setContentDescription(A0I);
        }
        AnonymousClass3A3.A00(view.getContext(), view, r6, A0I2);
        String A0I3 = r3.A0I(40);
        if (A0I3 != null) {
            C12970iu.A1O(r6, A0I3);
        }
        boolean A0O = r3.A0O(42, false);
        boolean A0O2 = r3.A0O(43, false);
        AccessibilityNodeInfo accessibilityNodeInfo = r6.A02;
        accessibilityNodeInfo.setSelected(A0O);
        accessibilityNodeInfo.setEnabled(!A0O2);
    }
}
