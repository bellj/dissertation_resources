package X;

import com.whatsapp.ui.media.MediaCard;
import java.lang.ref.WeakReference;

/* renamed from: X.38W  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass38W extends AbstractC16350or {
    public final AnonymousClass02N A00 = new AnonymousClass02N();
    public final AnonymousClass018 A01;
    public final AnonymousClass1BB A02;
    public final C20050v8 A03;
    public final C15660nh A04;
    public final AbstractC14640lm A05;
    public final WeakReference A06;

    public AnonymousClass38W(AnonymousClass018 r2, AnonymousClass1BB r3, C20050v8 r4, C15660nh r5, AbstractC14640lm r6, MediaCard mediaCard) {
        this.A01 = r2;
        this.A03 = r4;
        this.A04 = r5;
        this.A02 = r3;
        this.A06 = C12970iu.A10(mediaCard);
        this.A05 = r6;
    }
}
