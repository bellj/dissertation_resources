package X;

import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;

/* renamed from: X.3Mh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class View$OnLayoutChangeListenerC66103Mh implements View.OnLayoutChangeListener {
    public final /* synthetic */ C470928x A00;

    public View$OnLayoutChangeListenerC66103Mh(C470928x r1) {
        this.A00 = r1;
    }

    @Override // android.view.View.OnLayoutChangeListener
    public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        C470928x r0 = this.A00;
        View view2 = r0.A03;
        ViewGroup.MarginLayoutParams A0H = C12970iu.A0H(view2);
        int width = r0.A06.getWidth();
        A0H.setMargins(0, 0, 0, 0);
        A0H.width = C12990iw.A07(view2, R.dimen.shape_picker_search_collapsed_width) + C12990iw.A07(view2, R.dimen.shape_picker_landscape_button_spacing) + width;
        view2.setLayoutParams(A0H);
        view2.removeOnLayoutChangeListener(this);
    }
}
