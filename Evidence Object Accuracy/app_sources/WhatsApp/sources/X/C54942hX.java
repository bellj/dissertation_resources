package X;

import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.2hX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54942hX extends AnonymousClass03U {
    public AnonymousClass4QG A00;
    public final RadioButton A01;
    public final TextView A02;

    public C54942hX(View view) {
        super(view);
        this.A02 = C12960it.A0I(view, R.id.question);
        RadioButton radioButton = (RadioButton) AnonymousClass028.A0D(view, R.id.radio);
        this.A01 = radioButton;
        radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: X.4p9
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                AnonymousClass4QG r1 = C54942hX.this.A00;
                if (r1.A00 != z) {
                    r1.A00 = z;
                    if (z) {
                        r1.A01.A0A(r1);
                    }
                }
            }
        });
        C12960it.A0x(this.A02, this, 6);
    }
}
