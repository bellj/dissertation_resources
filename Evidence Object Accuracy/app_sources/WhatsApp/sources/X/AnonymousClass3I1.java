package X;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.3I1  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3I1 {
    public static final Map A00(AnonymousClass3FA r8) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        linkedHashMap.put(EnumC629739j.A05.key, Boolean.FALSE);
        if (r8 != null) {
            C17520qw[] r5 = new C17520qw[2];
            C17520qw.A00(EnumC629739j.A01.key, Integer.valueOf((int) r8.A00), r5, 0);
            C17520qw.A00(EnumC629739j.A02.key, r8.A01, r5, 1);
            LinkedHashMap linkedHashMap2 = new LinkedHashMap(3);
            int i = 0;
            do {
                C17520qw r0 = r5[i];
                i++;
                linkedHashMap2.put(r0.first, r0.second);
            } while (i < 2);
            Map map = r8.A02;
            if (map != null) {
                linkedHashMap2.put(EnumC629739j.A03.key, map);
            }
            linkedHashMap.put(EnumC629739j.A00.key, linkedHashMap2);
        }
        return linkedHashMap;
    }

    public static final Map A01(AnonymousClass3EI r6) {
        HashMap A11 = C12970iu.A11();
        A11.put(EnumC629739j.A05.key, Boolean.TRUE);
        AnonymousClass3E3 r1 = r6.A01.A04;
        if (r1 != null) {
            String str = EnumC629739j.A04.key;
            Object A01 = C94734cS.A00(r1.A01.A01).A01("$", new AnonymousClass5T6[0]);
            C16700pc.A0B(A01);
            A11.put(str, A01);
        }
        return A11;
    }

    public static final void A02(AnonymousClass17Q r7, AnonymousClass3EI r8) {
        AnonymousClass3E1 r0;
        C16700pc.A0E(r7, 0);
        AnonymousClass3HE r5 = r8.A01;
        AnonymousClass3E4 r02 = r5.A05;
        if (r02 != null) {
            AnonymousClass3EK r2 = r02.A01;
            List list = r2.A02;
            if (!list.isEmpty()) {
                r7.A07(r8);
                C27661Io A02 = r7.A02();
                String str = r2.A01;
                C16700pc.A0B(str);
                int size = list.size();
                A02.A06 = str;
                if (A02.A03 != null) {
                    C19630uQ r4 = A02.A0E;
                    r4.A02.A03(A02.A00, "queueStates");
                    r4.A01(A02.A00, "num_states_queued", (long) size);
                    r4.A02(A02.A00, "session_id", A02.A0M);
                }
            }
        }
        AbstractC17770rM AGE = r7.A02().A0F.AGE("open_bloks_screen_graphql");
        if (!(AGE == null || (r0 = r5.A02) == null)) {
            for (AnonymousClass3Dy r1 : r0.A01.A01) {
                C16700pc.A0B(r1);
                ((C19710uY) AGE).A04(r1);
            }
        }
    }
}
