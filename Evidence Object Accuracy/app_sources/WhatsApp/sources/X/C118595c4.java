package X;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiBankPickerActivity;
import java.util.List;

/* renamed from: X.5c4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118595c4 extends AnonymousClass02M {
    public List A00 = C12960it.A0l();
    public final Drawable A01;
    public final boolean A02;
    public final /* synthetic */ IndiaUpiBankPickerActivity A03;

    public C118595c4(IndiaUpiBankPickerActivity indiaUpiBankPickerActivity, boolean z) {
        this.A03 = indiaUpiBankPickerActivity;
        this.A02 = z;
        this.A01 = indiaUpiBankPickerActivity.getResources().getDrawable(z ? R.drawable.bank_logo_placeholder_with_circle_bg_popular_bank : R.drawable.bank_logo_placeholder_with_circle_bg);
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A00.size();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x008f, code lost:
        if (getItemViewType(r13 + 1) == 1) goto L_0x0091;
     */
    @Override // X.AnonymousClass02M
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void ANH(X.AnonymousClass03U r12, int r13) {
        /*
            r11 = this;
            int r0 = r11.getItemViewType(r13)
            r3 = 1
            if (r0 != r3) goto L_0x0015
            X.5cF r12 = (X.C118705cF) r12
            android.widget.TextView r1 = r12.A00
            java.util.List r0 = r11.A00
            java.lang.String r0 = X.C12960it.A0g(r0, r13)
            r1.setText(r0)
        L_0x0014:
            return
        L_0x0015:
            r2 = r12
            X.5cL r2 = (X.C118765cL) r2
            java.util.List r0 = r11.A00
            java.lang.Object r4 = r0.get(r13)
            X.1Zb r4 = (X.AbstractC30851Zb) r4
            android.graphics.drawable.Drawable r6 = r11.A01
            java.lang.String r0 = r4.A03
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x0063
            com.whatsapp.payments.ui.IndiaUpiBankPickerActivity r0 = r2.A03
            X.1ob r5 = r0.A0E
            java.lang.String r10 = r4.A03
            X.AnonymousClass009.A05(r10)
            android.widget.ImageView r8 = r2.A01
            r9 = 0
            r7 = r6
            r5.A00(r6, r7, r8, r9, r10)
        L_0x003a:
            java.util.ArrayList r7 = X.C12960it.A0l()
            com.whatsapp.payments.ui.IndiaUpiBankPickerActivity r8 = r2.A03
            java.util.ArrayList r0 = r8.A0G
            if (r0 == 0) goto L_0x0047
            r7.addAll(r0)
        L_0x0047:
            java.lang.String r0 = r8.A0F
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x0069
            r6 = 0
        L_0x0050:
            java.lang.String r5 = r8.A0F
            int r0 = r5.length()
            if (r6 >= r0) goto L_0x0069
            int r1 = r6 + 1
            java.lang.String r0 = r5.substring(r6, r1)
            r7.add(r0)
            r6 = r1
            goto L_0x0050
        L_0x0063:
            android.widget.ImageView r0 = r2.A01
            r0.setImageDrawable(r6)
            goto L_0x003a
        L_0x0069:
            com.whatsapp.TextEmojiLabel r1 = r2.A02
            java.lang.String r0 = r4.A0B()
            r1.A0G(r7, r0)
            android.view.View r1 = r12.A0H
            X.64G r0 = new X.64G
            r0.<init>(r12, r11, r13)
            r1.setOnClickListener(r0)
            android.view.View r2 = r2.A00
            if (r2 == 0) goto L_0x0014
            java.util.List r0 = r11.A00
            int r0 = r0.size()
            int r0 = r0 - r3
            if (r13 == r0) goto L_0x0091
            int r13 = r13 + r3
            int r1 = r11.getItemViewType(r13)
            r0 = 0
            if (r1 != r3) goto L_0x0092
        L_0x0091:
            r0 = 4
        L_0x0092:
            r2.setVisibility(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C118595c4.ANH(X.03U, int):void");
    }

    @Override // X.AnonymousClass02M
    public AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        LayoutInflater A0E = C12960it.A0E(viewGroup);
        if (i != 2) {
            return new C118705cF(A0E.inflate(R.layout.india_upi_payment_bank_picker_header_row, viewGroup, false));
        }
        IndiaUpiBankPickerActivity indiaUpiBankPickerActivity = this.A03;
        boolean z = this.A02;
        int i2 = R.layout.india_upi_payment_bank_picker_list_row;
        if (z) {
            i2 = R.layout.india_upi_payment_bank_picker_grid_item;
        }
        return new C118765cL(A0E.inflate(i2, viewGroup, false), indiaUpiBankPickerActivity);
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        return this.A00.get(i) instanceof String ? 1 : 2;
    }
}
