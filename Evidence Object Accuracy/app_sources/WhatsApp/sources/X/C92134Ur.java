package X;

import java.util.LinkedHashMap;
import java.util.regex.Pattern;

/* renamed from: X.4Ur  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92134Ur {
    public AnonymousClass4MO A00;

    public C92134Ur(int i) {
        this.A00 = new AnonymousClass4MO(i);
    }

    public Pattern A00(String str) {
        LinkedHashMap linkedHashMap;
        Object obj;
        AnonymousClass4MO r2 = this.A00;
        synchronized (r2) {
            linkedHashMap = r2.A01;
            obj = linkedHashMap.get(str);
        }
        Pattern pattern = (Pattern) obj;
        if (pattern == null) {
            pattern = Pattern.compile(str);
            synchronized (r2) {
                linkedHashMap.put(str, pattern);
            }
        }
        return pattern;
    }
}
