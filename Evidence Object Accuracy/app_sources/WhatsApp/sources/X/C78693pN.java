package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/* renamed from: X.3pN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78693pN extends AnonymousClass1U5 implements AbstractC115085Qd {
    public static final Parcelable.Creator CREATOR = new C98784jF();
    public final int A00;
    public final SparseArray A01;
    public final HashMap A02;

    public C78693pN() {
        this.A00 = 1;
        this.A02 = C12970iu.A11();
        this.A01 = new SparseArray();
    }

    public C78693pN(ArrayList arrayList, int i) {
        this.A00 = i;
        this.A02 = C12970iu.A11();
        this.A01 = new SparseArray();
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            C78523p3 r0 = (C78523p3) arrayList.get(i2);
            String str = r0.A02;
            int i3 = r0.A01;
            C12960it.A1K(str, this.A02, i3);
            this.A01.put(i3, str);
        }
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 1, this.A00);
        ArrayList A0l = C12960it.A0l();
        HashMap hashMap = this.A02;
        Iterator A0L = C72463ee.A0L(hashMap);
        while (A0L.hasNext()) {
            String A0x = C12970iu.A0x(A0L);
            A0l.add(new C78523p3(A0x, C12960it.A05(hashMap.get(A0x))));
        }
        C95654e8.A0F(parcel, A0l, 2, false);
        C95654e8.A06(parcel, A00);
    }
}
