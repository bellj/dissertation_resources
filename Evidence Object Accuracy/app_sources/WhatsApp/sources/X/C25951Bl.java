package X;

import com.whatsapp.jid.UserJid;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.1Bl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C25951Bl {
    public final C14850m9 A00;
    public final C16120oU A01;
    public final AtomicInteger A02;

    public C25951Bl(C14850m9 r3, C16120oU r4) {
        AtomicInteger atomicInteger = new AtomicInteger();
        this.A02 = atomicInteger;
        this.A00 = r3;
        this.A01 = r4;
        atomicInteger.set(1);
    }

    public final void A00(UserJid userJid, Integer num, Integer num2, Integer num3) {
        if (this.A00.A07(1669)) {
            C615430t r2 = new C615430t();
            r2.A00 = 1;
            r2.A01 = 1;
            if (userJid != null) {
                r2.A06 = userJid.getRawString();
            }
            r2.A02 = num;
            r2.A03 = num2;
            r2.A04 = num3;
            r2.A05 = Long.valueOf((long) this.A02.getAndIncrement());
            this.A01.A07(r2);
        }
    }
}
