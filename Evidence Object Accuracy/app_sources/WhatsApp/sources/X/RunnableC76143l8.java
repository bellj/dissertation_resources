package X;

import com.facebook.redex.EmptyBaseRunnable0;
import com.whatsapp.mediaview.PhotoView;

/* renamed from: X.3l8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class RunnableC76143l8 extends EmptyBaseRunnable0 implements Runnable {
    public long A00;
    public boolean A01;
    public final PhotoView A02;

    public RunnableC76143l8(PhotoView photoView) {
        this.A02 = photoView;
    }

    @Override // java.lang.Runnable
    public void run() {
        if (!this.A01) {
            long j = this.A00;
            if (j == 0) {
                j = System.currentTimeMillis();
                this.A00 = j;
            }
            if (((float) (System.currentTimeMillis() - j)) / ((float) 0) >= 1.0f) {
                this.A01 = true;
                PhotoView photoView = this.A02;
                photoView.invalidate();
                if (!this.A01) {
                    photoView.post(this);
                    return;
                }
                return;
            }
            throw C12980iv.A0n("left");
        }
    }
}
