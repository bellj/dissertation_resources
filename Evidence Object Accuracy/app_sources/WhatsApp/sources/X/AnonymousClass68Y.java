package X;

import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiVpaContactInfoActivity;

/* renamed from: X.68Y  reason: invalid class name */
/* loaded from: classes4.dex */
public final /* synthetic */ class AnonymousClass68Y implements AnonymousClass5US {
    public final /* synthetic */ IndiaUpiVpaContactInfoActivity A00;
    public final /* synthetic */ boolean A01;

    public /* synthetic */ AnonymousClass68Y(IndiaUpiVpaContactInfoActivity indiaUpiVpaContactInfoActivity, boolean z) {
        this.A00 = indiaUpiVpaContactInfoActivity;
        this.A01 = z;
    }

    @Override // X.AnonymousClass5US
    public final void AVD(C452120p r6) {
        IndiaUpiVpaContactInfoActivity indiaUpiVpaContactInfoActivity = this.A00;
        boolean z = this.A01;
        if (r6 == null) {
            indiaUpiVpaContactInfoActivity.A2T(z);
        } else if (z) {
            indiaUpiVpaContactInfoActivity.Ado(R.string.block_upi_id_error);
        } else {
            Object[] A1b = C12970iu.A1b();
            A1b[0] = indiaUpiVpaContactInfoActivity.getString(R.string.india_upi_payment_id_name);
            indiaUpiVpaContactInfoActivity.Adr(A1b, 0, R.string.unblock_payment_id_error_default);
        }
    }
}
