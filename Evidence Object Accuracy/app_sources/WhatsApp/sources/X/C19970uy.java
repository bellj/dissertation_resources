package X;

/* renamed from: X.0uy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C19970uy implements AnonymousClass01N {
    public static final Object A02 = new Object();
    public volatile Object A00 = A02;
    public volatile AnonymousClass01N A01;

    public C19970uy(AnonymousClass01N r2) {
        this.A01 = r2;
    }

    public static AnonymousClass01N A00(AnonymousClass01N r1) {
        return ((r1 instanceof C19970uy) || (r1 instanceof C18000rk)) ? r1 : new C19970uy(r1);
    }

    @Override // X.AnonymousClass01N, X.AnonymousClass01H
    public Object get() {
        Object obj = this.A00;
        if (obj != A02) {
            return obj;
        }
        AnonymousClass01N r0 = this.A01;
        if (r0 == null) {
            return this.A00;
        }
        Object obj2 = r0.get();
        this.A00 = obj2;
        this.A01 = null;
        return obj2;
    }
}
