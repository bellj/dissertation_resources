package X;

import android.view.View;

/* renamed from: X.07u  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public interface AbstractC016507u {
    @Override // android.view.ViewParent, X.AbstractC016507u
    boolean onNestedFling(View view, float f, float f2, boolean z);

    @Override // android.view.ViewParent, X.AbstractC016507u
    boolean onNestedPreFling(View view, float f, float f2);

    @Override // android.view.ViewParent, X.AbstractC016507u
    void onNestedPreScroll(View view, int i, int i2, int[] iArr);

    @Override // android.view.ViewParent, X.AbstractC016507u
    void onNestedScroll(View view, int i, int i2, int i3, int i4);

    @Override // android.view.ViewParent, X.AbstractC016507u
    void onNestedScrollAccepted(View view, View view2, int i);

    @Override // android.view.ViewParent, X.AbstractC016507u
    boolean onStartNestedScroll(View view, View view2, int i);

    @Override // android.view.ViewParent, X.AbstractC016507u
    void onStopNestedScroll(View view);
}
