package X;

import com.whatsapp.payments.ui.IndiaUpiQuickBuyActivity;

/* renamed from: X.68A  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass68A implements AnonymousClass1QA {
    public final /* synthetic */ IndiaUpiQuickBuyActivity A00;
    public final /* synthetic */ C128335vw A01;

    @Override // X.AnonymousClass1QA
    public void AWZ() {
    }

    public AnonymousClass68A(IndiaUpiQuickBuyActivity indiaUpiQuickBuyActivity, C128335vw r2) {
        this.A00 = indiaUpiQuickBuyActivity;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass1QA
    public void AWX() {
        C12960it.A1E(new C124035oL(this), ((ActivityC13830kP) this.A00).A05);
    }
}
