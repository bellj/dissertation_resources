package X;

/* renamed from: X.45L  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass45L extends AbstractC92674Wx {
    public final /* synthetic */ AnonymousClass33C A00;

    public AnonymousClass45L(AnonymousClass33C r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC92674Wx
    public void A01() {
        AnonymousClass33C r1 = this.A00;
        r1.A01 = Boolean.valueOf(!r1.A01.booleanValue());
        super.A01();
    }
}
