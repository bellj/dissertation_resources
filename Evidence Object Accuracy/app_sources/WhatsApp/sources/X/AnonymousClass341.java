package X;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import com.facebook.redex.IDxLListenerShape13S0100000_1_I1;
import com.whatsapp.R;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.341  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass341 extends AnonymousClass242 implements AnonymousClass1KU {
    public int A00;
    public int A01;
    public int A02;
    public View A03;
    public View A04;
    public C58372oi A05;
    public AbstractC116245Ur A06;
    public AnonymousClass35o A07;
    public AnonymousClass35p A08;
    public AnonymousClass35p A09;
    public AnonymousClass35p A0A;
    public AnonymousClass35p A0B;
    public C622235r A0C;
    public AnonymousClass35q A0D;
    public AbstractC69213Yj A0E;
    public String A0F;
    public HashMap A0G;
    public List A0H;
    public List A0I;
    public boolean A0J = false;
    public final int A0K;
    public final LayoutInflater A0L;
    public final ViewTreeObserver.OnGlobalLayoutListener A0M = new IDxLListenerShape13S0100000_1_I1(this, 12);
    public final AbstractC15710nm A0N;
    public final AnonymousClass01d A0O;
    public final C14820m6 A0P;
    public final C14850m9 A0Q;
    public final C16120oU A0R;
    public final C69893aP A0S;
    public final C22210yi A0T;
    public final AnonymousClass15H A0U;
    public final AnonymousClass1AB A0V;
    public final AnonymousClass146 A0W;
    public final C64763Gu A0X;
    public final C235512c A0Y;
    public final AbstractC116245Ur A0Z = new AnonymousClass59Z(this);
    public final C255519v A0a;
    public final AnonymousClass1BQ A0b;
    public final C69233Yl A0c;
    public final AnonymousClass1KT A0d;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass341(Activity activity, ViewGroup viewGroup, AbstractC05270Ox r19, AbstractC15710nm r20, AnonymousClass01d r21, C14820m6 r22, AnonymousClass018 r23, C14850m9 r24, C16120oU r25, C69893aP r26, C22210yi r27, AnonymousClass15H r28, AnonymousClass1AB r29, AnonymousClass146 r30, C235512c r31, C255519v r32, AnonymousClass1BQ r33, AnonymousClass1KT r34, AbstractC14440lR r35) {
        super(activity, viewGroup, r19, r23, R.id.sticker_pager);
        int i;
        AnonymousClass1KT r10 = r34;
        this.A0Q = r24;
        this.A0N = r20;
        this.A0R = r25;
        this.A0O = r21;
        this.A0W = r30;
        this.A0Y = r31;
        this.A0P = r22;
        this.A0U = r28;
        this.A0V = r29;
        this.A0S = r26;
        this.A0T = r27;
        this.A0L = LayoutInflater.from(activity);
        this.A0X = new C64763Gu();
        this.A0d = r34 == null ? new AnonymousClass1KT(r22, r27, r30, r31, r35) : r10;
        this.A0b = r33;
        this.A0a = r32;
        if (this.A0Q.A07(1396)) {
            viewGroup.findViewById(R.id.store_button_view_top).setVisibility(8);
            C12980iv.A1B(viewGroup, R.id.store_button_view, 0);
            this.A03 = viewGroup.findViewById(R.id.store_button);
            i = R.id.store_badge;
        } else {
            viewGroup.findViewById(R.id.store_button_view).setVisibility(8);
            C12980iv.A1B(viewGroup, R.id.store_button_view_top, 0);
            this.A03 = viewGroup.findViewById(R.id.sticker_store_button);
            i = R.id.sticker_store_badge;
        }
        this.A04 = viewGroup.findViewById(i);
        this.A0K = C12960it.A09(super.A0A).getDimensionPixelSize(R.dimen.sticker_picker_item);
        int dimensionPixelSize = activity.getResources().getDimensionPixelSize(R.dimen.sticker_picker_header_height);
        this.A01 = viewGroup.getWidth();
        this.A00 = viewGroup.getHeight() - dimensionPixelSize;
        this.A0I = C12960it.A0l();
        this.A0G = C12970iu.A11();
        C58372oi r0 = new C58372oi(r23, new AbstractC69213Yj[0]);
        this.A05 = r0;
        A03(r0);
        C69233Yl r2 = new C69233Yl(super.A07, viewGroup, r23);
        this.A0c = r2;
        if (r33 != null) {
            C12960it.A19((AbstractC001200n) activity, r33.A03, this, 93);
        }
        AbstractView$OnClickListenerC34281fs.A01(this.A03, this, 43);
        AbstractC116745Wq r1 = super.A04;
        if (r1 != null) {
            r1.Abx(null);
        }
        super.A04 = r2;
        r2.Abx(this);
    }

    public static AnonymousClass35p A00(AnonymousClass341 r7, int i) {
        return new AnonymousClass35p(((AnonymousClass242) r7).A07, r7.A0L, r7.A0Q, r7.A0V, r7.A0Z, i, r7.A0K);
    }

    public static final AbstractC69213Yj A01(String str, List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            AbstractC69213Yj r1 = (AbstractC69213Yj) it.next();
            if (str.equals(r1.getId())) {
                return r1;
            }
        }
        return null;
    }

    public void A04() {
        if (this.A04.getVisibility() == 0) {
            SharedPreferences sharedPreferences = this.A0P.A00;
            long A0F = C12980iv.A0F(sharedPreferences, "sticker_store_update_hidden_time");
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis - A0F >= 604800000) {
                C12970iu.A1C(sharedPreferences.edit(), "sticker_store_update_hidden_time", currentTimeMillis);
            }
        }
        Iterator A00 = AbstractC16230of.A00(this.A0W);
        while (A00.hasNext()) {
            ((AnonymousClass1KM) A00.next()).A00();
        }
        Context context = super.A07;
        Intent A0A = C12970iu.A0A();
        A0A.setClassName(context.getPackageName(), "com.whatsapp.stickers.StickerStoreActivity");
        context.startActivity(A0A);
        this.A0R.A07(new C854142s());
    }

    @Override // X.AnonymousClass1KU
    public void AQC() {
        AnonymousClass35q r0 = this.A0D;
        if (r0 != null) {
            r0.A01();
            if (this.A0J) {
                this.A0F = "starred";
                AbstractC69213Yj A01 = A01("starred", this.A0I);
                if (A01 != null) {
                    A02(this.A0I.indexOf(A01), true);
                    this.A0F = null;
                }
            }
        }
    }

    @Override // X.AnonymousClass1KU
    public void AUj() {
        C622235r r0 = this.A0C;
        if (r0 != null) {
            r0.A01();
        }
    }

    @Override // X.AnonymousClass1KU
    public void AWh(String str, HashMap hashMap, HashMap hashMap2, HashSet hashSet, List list, int i) {
        if (!this.A0G.containsKey(str)) {
            Acu(null, hashMap, hashMap2, hashSet, list);
            return;
        }
        C622335s r0 = (C622335s) this.A0G.get(str);
        r0.A00 = i;
        r0.A04();
    }

    @Override // X.AnonymousClass1KU
    public void AWk(AnonymousClass1KZ r4) {
        C622335s r2 = (C622335s) this.A0G.get(r4.A0D);
        if (r2 != null) {
            r2.A04 = r4;
            r2.A00().A0E(r4.A04);
            r2.A01();
        }
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:215:0x0293 */
    /* JADX WARN: Type inference failed for: r6v7, types: [X.3Yj, X.35s] */
    /* JADX WARN: Type inference failed for: r6v8 */
    /* JADX WARN: Type inference failed for: r6v9, types: [X.35s] */
    /* JADX WARN: Type inference failed for: r6v10, types: [X.35n] */
    /* JADX WARNING: Code restructure failed: missing block: B:150:0x0401, code lost:
        if (r28.A0d.A06 != false) goto L_0x0403;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:178:0x0477, code lost:
        if (r29 != null) goto L_0x0479;
     */
    /* JADX WARNING: Removed duplicated region for block: B:165:0x0443 A[LOOP:10: B:163:0x043d->B:165:0x0443, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:184:0x0489  */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // X.AnonymousClass1KU
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void Acu(java.lang.String r29, java.util.HashMap r30, java.util.HashMap r31, java.util.HashSet r32, java.util.List r33) {
        /*
        // Method dump skipped, instructions count: 1183
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass341.Acu(java.lang.String, java.util.HashMap, java.util.HashMap, java.util.HashSet, java.util.List):void");
    }
}
