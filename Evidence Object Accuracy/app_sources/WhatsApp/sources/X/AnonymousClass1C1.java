package X;

import android.content.ContentValues;
import android.database.Cursor;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/* renamed from: X.1C1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1C1 {
    public Map A00;
    public final AbstractC15710nm A01;
    public final AnonymousClass1C3 A02;
    public final C97814hg A03 = new C97814hg(this);
    public final C26081By A04;

    public AnonymousClass1C1(AbstractC15710nm r2, AnonymousClass1C3 r3, C26081By r4) {
        this.A01 = r2;
        this.A04 = r4;
        this.A02 = r3;
    }

    public synchronized String A00(String str) {
        String str2;
        Map map = this.A00;
        HashMap hashMap = map;
        if (map == null) {
            HashMap hashMap2 = new HashMap();
            C16310on A00 = this.A04.A00.A00();
            Cursor A09 = A00.A03.A09("SELECT key, value FROM properties", null);
            A00.close();
            if (A09.moveToFirst()) {
                int columnIndexOrThrow = A09.getColumnIndexOrThrow("key");
                int columnIndexOrThrow2 = A09.getColumnIndexOrThrow("value");
                do {
                    hashMap2.put(A09.getString(columnIndexOrThrow), A09.getString(columnIndexOrThrow2));
                } while (A09.moveToNext());
            }
            A09.close();
            this.A00 = hashMap2;
            hashMap = hashMap2;
        }
        str2 = (String) hashMap.get(str);
        if (str2 == null) {
            return null;
        }
        return str2;
    }

    public synchronized void A01(String str, String str2) {
        C29621Ty r6 = this.A02.A03;
        AnonymousClass1C0 r0 = this.A04.A00;
        C16310on A01 = r0.A01();
        AnonymousClass1Lx A00 = A01.A00();
        C16310on A012 = r0.A01();
        try {
            AnonymousClass1Lx A002 = A012.A00();
            C16330op r3 = A012.A03;
            r3.A01("properties", "key = ?", new String[]{str});
            if (str2 != null) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("key", str);
                contentValues.put("value", str2);
                r3.A03(contentValues, "properties");
            }
            A002.A00();
            A002.close();
            A012.close();
            C97814hg r2 = this.A03;
            Object obj = new Object();
            Object obj2 = r6.A01.get();
            AnonymousClass009.A05(obj2);
            ((HashMap) obj2).put(obj, r2);
            A00.A00();
            Map map = this.A00;
            if (map != null) {
                if (str2 != null) {
                    map.put(str, str2);
                } else {
                    map.remove(str);
                }
            }
            A00.close();
            A01.close();
        } catch (Throwable th) {
            try {
                A012.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public boolean A02(String str) {
        String A00 = A00(str);
        if (A00 != null) {
            String lowerCase = A00.toLowerCase(Locale.US);
            if (lowerCase.equals("true")) {
                return true;
            }
            if (!lowerCase.equals("false")) {
                AbstractC15710nm r2 = this.A01;
                StringBuilder sb = new StringBuilder("Malformed boolean: ");
                sb.append(str);
                sb.append("=");
                sb.append(A00);
                r2.AaV("xpm-file-prefetcher-properties", sb.toString(), false);
            }
        }
        return false;
    }
}
