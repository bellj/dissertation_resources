package X;

import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RectF;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.21t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C454721t {
    public C91484Rx A00;
    public AbstractC454821u A01;
    public AbstractC454821u A02;
    public final C63863Df A03 = new C63863Df();
    public final List A04;
    public final List A05;

    public C454721t() {
        ArrayList arrayList = new ArrayList();
        this.A04 = arrayList;
        this.A05 = Collections.unmodifiableList(arrayList);
    }

    public AbstractC454821u A00(PointF pointF) {
        List list = this.A04;
        int size = list.size();
        while (true) {
            size--;
            if (size < 0) {
                return null;
            }
            AbstractC454821u r4 = (AbstractC454821u) list.get(size);
            if (!(r4 instanceof AnonymousClass33J)) {
                float f = pointF.x;
                float f2 = pointF.y;
                if (!(r4 instanceof AnonymousClass33H)) {
                    boolean z = r4 instanceof AnonymousClass33E;
                    float f3 = -r4.A00;
                    if (!z) {
                        RectF rectF = r4.A02;
                        float centerX = rectF.centerX();
                        float centerY = rectF.centerY();
                        Matrix matrix = new Matrix();
                        float[] fArr = {f, f2};
                        matrix.setRotate(f3, centerX, centerY);
                        matrix.mapPoints(fArr);
                        if (rectF.contains(fArr[0], fArr[1])) {
                            return r4;
                        }
                    } else {
                        RectF rectF2 = r4.A02;
                        float centerX2 = rectF2.centerX();
                        float centerY2 = rectF2.centerY();
                        Matrix matrix2 = new Matrix();
                        float[] fArr2 = {f, f2};
                        matrix2.setRotate(f3, centerX2, centerY2);
                        matrix2.mapPoints(fArr2);
                        float f4 = fArr2[0];
                        float f5 = fArr2[1];
                        if (rectF2.contains(f4, f5)) {
                            float f6 = rectF2.left;
                            float f7 = rectF2.bottom;
                            float f8 = rectF2.right;
                            float f9 = rectF2.top;
                            float f10 = f9 - f7;
                            float f11 = f8 - f6;
                            if (((double) Math.abs((((f4 * f10) - (f5 * f11)) + (f8 * f7)) - (f9 * f6))) / Math.sqrt((double) ((f10 * f10) + (f11 * f11))) < ((double) AbstractC454821u.A06)) {
                                return r4;
                            }
                        } else {
                            continue;
                        }
                    }
                } else {
                    RectF rectF3 = r4.A02;
                    if (rectF3.contains(f, f2)) {
                        float centerX3 = f - rectF3.centerX();
                        float centerY3 = f2 - rectF3.centerY();
                        float width = rectF3.width() / 2.0f;
                        float height = rectF3.height() / 2.0f;
                        if (((centerX3 * centerX3) / (width * width)) + ((centerY3 * centerY3) / (height * height)) <= 1.0f) {
                            return r4;
                        }
                    } else {
                        continue;
                    }
                }
            }
        }
    }

    public List A01() {
        ArrayList arrayList = new ArrayList();
        for (Object obj : this.A04) {
            if (obj instanceof AnonymousClass33J) {
                arrayList.add(obj);
            }
        }
        return arrayList;
    }

    public void A02() {
        this.A01 = null;
        this.A02 = null;
        this.A00 = null;
        this.A04.clear();
        this.A03.A00.clear();
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x001f  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0045 A[LOOP:1: B:17:0x003f->B:19:0x0045, LOOP_END] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A03(X.AbstractC454821u r5) {
        /*
            r4 = this;
            boolean r0 = r5 instanceof X.AnonymousClass33J
            if (r0 == 0) goto L_0x0063
            java.util.List r3 = r4.A04
            boolean r0 = r3.isEmpty()
            if (r0 != 0) goto L_0x0063
            r0 = r5
            X.33J r0 = (X.AnonymousClass33J) r0
            X.3Hb r0 = r0.A03
            boolean r0 = r0 instanceof X.AnonymousClass33P
            java.util.Iterator r2 = r3.iterator()
            if (r0 == 0) goto L_0x004f
        L_0x0019:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x005e
            java.lang.Object r1 = r2.next()
            X.21u r1 = (X.AbstractC454821u) r1
            boolean r0 = r1 instanceof X.AnonymousClass33J
            if (r0 == 0) goto L_0x0032
            r0 = r1
            X.33J r0 = (X.AnonymousClass33J) r0
            X.3Hb r0 = r0.A03
            boolean r0 = r0 instanceof X.AnonymousClass33P
            if (r0 != 0) goto L_0x0019
        L_0x0032:
            int r0 = r3.indexOf(r1)
        L_0x0036:
            r3.add(r0, r5)
        L_0x0039:
            r4.A01 = r5
            java.util.Iterator r1 = r3.iterator()
        L_0x003f:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0069
            java.lang.Object r0 = r1.next()
            X.21u r0 = (X.AbstractC454821u) r0
            r0.A05()
            goto L_0x003f
        L_0x004f:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x005e
            java.lang.Object r1 = r2.next()
            boolean r0 = r1 instanceof X.AnonymousClass33J
            if (r0 != 0) goto L_0x004f
            goto L_0x0032
        L_0x005e:
            int r0 = r3.size()
            goto L_0x0036
        L_0x0063:
            java.util.List r3 = r4.A04
            r3.add(r5)
            goto L_0x0039
        L_0x0069:
            X.45N r1 = new X.45N
            r1.<init>(r5)
            X.3Df r0 = r4.A03
            java.util.LinkedList r0 = r0.A00
            r0.add(r1)
            r0 = 0
            r4.A02 = r0
            r4.A00 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C454721t.A03(X.21u):void");
    }

    public void A04(AbstractC454821u r5) {
        List list = this.A04;
        int indexOf = list.indexOf(r5);
        if (indexOf != -1) {
            this.A03.A00.add(new AnonymousClass45P(r5, indexOf));
            list.remove(r5);
            if (r5 == this.A01) {
                this.A01 = null;
            }
        }
    }

    public void A05(String str) {
        AbstractC94414bm r2;
        if (str != null) {
            try {
                C63863Df r22 = this.A03;
                List list = this.A04;
                JSONArray jSONArray = new JSONObject(str).getJSONArray("actions");
                LinkedList linkedList = r22.A00;
                linkedList.clear();
                for (int i = 0; i < jSONArray.length(); i++) {
                    JSONObject jSONObject = jSONArray.getJSONObject(i);
                    String string = jSONObject.getString("type");
                    switch (string.hashCode()) {
                        case 49116392:
                            if (string.equals("undo_add_shape")) {
                                r2 = new AnonymousClass45N();
                                break;
                            } else {
                                continue;
                            }
                        case 1021915016:
                            if (string.equals("undo_delete_shape")) {
                                r2 = new AnonymousClass45P();
                                break;
                            } else {
                                continue;
                            }
                        case 1953129077:
                            if (string.equals("undo_change_z_order")) {
                                r2 = new AnonymousClass45O();
                                break;
                            } else {
                                continue;
                            }
                        case 1971936087:
                            if (string.equals("undo_modify_shape")) {
                                r2 = new AnonymousClass33O();
                                break;
                            } else {
                                continue;
                            }
                        default:
                            continue;
                    }
                    r2.A02(jSONObject);
                    int i2 = jSONObject.getInt("shape_index");
                    if (i2 >= 0 && i2 < list.size()) {
                        r2.A00 = (AbstractC454821u) list.get(i2);
                        linkedList.add(r2);
                    }
                }
            } catch (JSONException e) {
                Log.e("ShapeRepository/loadUndoState", e);
            }
        }
    }

    public boolean A06() {
        for (AbstractC454821u r0 : this.A04) {
            if (r0.A0B()) {
                return true;
            }
        }
        return false;
    }
}
