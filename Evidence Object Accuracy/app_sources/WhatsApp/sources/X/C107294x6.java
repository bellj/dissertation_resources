package X;

import com.google.android.search.verification.client.SearchActionVerificationClientService;

/* renamed from: X.4x6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107294x6 implements AbstractC116605Wc {
    public static final int[] A0C = {-1, -1, -1, -1, 2, 4, 6, 8, -1, -1, -1, -1, 2, 4, 6, 8};
    public static final int[] A0D = {7, 8, 9, 10, 11, 12, 13, 14, 16, 17, 19, 21, 23, 25, 28, 31, 34, 37, 41, 45, 50, 55, 60, 66, 73, 80, 88, 97, 107, 118, 130, 143, 157, 173, 190, 209, 230, 253, 279, 307, 337, 371, 408, 449, 494, 544, 598, 658, 724, 796, 876, 963, 1060, 1166, 1282, 1411, 1552, 1707, 1878, 2066, 2272, 2499, 2749, 3024, 3327, 3660, 4026, 4428, 4871, 5358, 5894, 6484, 7132, 7845, 8630, 9493, 10442, 11487, 12635, 13899, 15289, 16818, 18500, 20350, 22385, 24623, 27086, 29794, 32767};
    public int A00;
    public int A01;
    public long A02;
    public long A03;
    public final int A04;
    public final int A05;
    public final C100614mC A06;
    public final AbstractC14070ko A07;
    public final AnonymousClass5X6 A08;
    public final AnonymousClass4T4 A09;
    public final C95304dT A0A;
    public final byte[] A0B;

    public C107294x6(AbstractC14070ko r10, AnonymousClass5X6 r11, AnonymousClass4T4 r12) {
        this.A07 = r10;
        this.A08 = r11;
        this.A09 = r12;
        int i = r12.A03;
        int max = Math.max(1, i / 10);
        this.A05 = max;
        C95304dT r0 = new C95304dT(r12.A05);
        r0.A0A();
        int A0A = r0.A0A();
        this.A04 = A0A;
        int i2 = r12.A04;
        int i3 = r12.A01;
        int i4 = (((i3 - (i2 << 2)) << 3) / (r12.A00 * i2)) + 1;
        if (A0A == i4) {
            int i5 = ((max + A0A) - 1) / A0A;
            this.A0B = new byte[i3 * i5];
            this.A0A = C95304dT.A05(i5 * (A0A << 1) * i2);
            int i6 = ((i * i3) << 3) / A0A;
            C93844ap A00 = C93844ap.A00();
            A00.A0R = "audio/raw";
            A00.A03 = i6;
            A00.A0A = i6;
            A00.A08 = (max << 1) * i2;
            A00.A04 = i2;
            A00.A0D = i;
            A00.A09 = 2;
            this.A06 = new C100614mC(A00);
            return;
        }
        StringBuilder A0k = C12960it.A0k("Expected frames per block: ");
        A0k.append(i4);
        throw AnonymousClass496.A00(C12960it.A0e("; got: ", A0k, A0A));
    }

    public final void A00(int i) {
        long j = this.A03;
        long j2 = this.A02;
        AnonymousClass4T4 r2 = this.A09;
        long A07 = j + AnonymousClass3JZ.A07(j2, SearchActionVerificationClientService.MS_TO_NS, (long) r2.A03);
        int i2 = (i << 1) * r2.A04;
        this.A08.AbG(null, 1, i2, this.A01 - i2, A07);
        this.A02 += (long) i;
        this.A01 -= i2;
    }

    @Override // X.AbstractC116605Wc
    public void AIZ(int i, long j) {
        this.A07.AbR(new C106934wW(this.A09, this.A04, (long) i, j));
        this.A08.AA6(this.A06);
    }

    @Override // X.AbstractC116605Wc
    public void Aaf(long j) {
        this.A00 = 0;
        this.A03 = j;
        this.A01 = 0;
        this.A02 = 0;
    }

    @Override // X.AbstractC116605Wc
    public boolean AbE(AnonymousClass5Yf r25, long j) {
        int i;
        int i2 = this.A05;
        int i3 = this.A01;
        AnonymousClass4T4 r1 = this.A09;
        int i4 = r1.A04;
        int i5 = i4 << 1;
        int i6 = this.A04;
        int i7 = r1.A01;
        int i8 = ((((i2 - (i3 / i5)) + i6) - 1) / i6) * i7;
        boolean z = false;
        if (j != 0) {
            while (true) {
                int i9 = this.A00;
                if (i9 >= i8) {
                    break;
                }
                int read = r25.read(this.A0B, i9, (int) Math.min((long) (i8 - i9), j));
                if (read == -1) {
                    break;
                }
                this.A00 += read;
            }
        }
        z = true;
        int i10 = this.A00 / i7;
        if (i10 > 0) {
            byte[] bArr = this.A0B;
            C95304dT r8 = this.A0A;
            int i11 = 0;
            do {
                for (int i12 = 0; i12 < i4; i12++) {
                    byte[] bArr2 = r8.A02;
                    int i13 = (i11 * i7) + (i12 << 2);
                    int i14 = (i4 << 2) + i13;
                    int i15 = (i7 / i4) - 4;
                    int i16 = (short) (((bArr[i13 + 1] & 255) << 8) | (bArr[i13] & 255));
                    int min = Math.min(bArr[i13 + 2] & 255, 88);
                    int[] iArr = A0D;
                    int i17 = iArr[min];
                    int i18 = (((i11 * i6) * i4) + i12) << 1;
                    C72463ee.A0V(bArr2, i16, i18);
                    bArr2[i18 + 1] = (byte) (i16 >> 8);
                    for (int i19 = 0; i19 < (i15 << 1); i19++) {
                        int i20 = bArr[(((i19 >> 3) * i4) << 2) + i14 + ((i19 >> 1) % 4)] & 255;
                        int i21 = i20 >> 4;
                        if (i19 % 2 == 0) {
                            i21 = i20 & 15;
                        }
                        int i22 = ((((i21 & 7) << 1) + 1) * i17) >> 3;
                        if ((i21 & 8) != 0) {
                            i22 = -i22;
                        }
                        i16 = Math.max(-32768, Math.min(i16 + i22, 32767));
                        i18 += i5;
                        C72463ee.A0V(bArr2, i16, i18);
                        bArr2[i18 + 1] = (byte) (i16 >> 8);
                        min = Math.max(0, Math.min(min + A0C[i21], iArr.length - 1));
                        i17 = iArr[min];
                    }
                }
                i11++;
            } while (i11 < i10);
            r8.A0S(0);
            r8.A0R(((i6 * i10) << 1) * i4);
            this.A00 -= i10 * i7;
            int i23 = r8.A00;
            this.A08.AbC(r8, i23);
            int i24 = this.A01 + i23;
            this.A01 = i24;
            if (i24 / i5 >= i2) {
                A00(i2);
            }
        }
        if (z && (i = this.A01 / i5) > 0) {
            A00(i);
        }
        return z;
    }
}
