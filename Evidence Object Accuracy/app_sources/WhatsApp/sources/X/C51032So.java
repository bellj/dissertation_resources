package X;

/* renamed from: X.2So  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C51032So extends AbstractC16350or {
    public final C16590pI A00;
    public final AbstractC16710pd A01;
    public final AbstractC16710pd A02 = AnonymousClass4Yq.A00(new C113925Jn(this));
    public final AbstractC16710pd A03 = AnonymousClass4Yq.A00(new C113935Jo(this));
    public final AnonymousClass1J7 A04;

    public C51032So(AbstractC001200n r2, C16590pI r3, AnonymousClass1J7 r4, int i) {
        super(r2);
        this.A00 = r3;
        this.A04 = r4;
        this.A01 = AnonymousClass4Yq.A00(new C113915Jm(i));
    }
}
