package X;

/* renamed from: X.1gS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C34631gS extends AnonymousClass1JQ implements AbstractC34381g3, AbstractC34391g4, AbstractC34531gI {
    public final C34361g1 A00;
    public final AbstractC14640lm A01;
    public final boolean A02;

    public C34631gS(AnonymousClass1JR r11, C34361g1 r12, AbstractC14640lm r13, String str, long j, boolean z, boolean z2) {
        super(C27791Jf.A03, r11, str, "regular_low", 3, j, z2);
        this.A01 = r13;
        this.A02 = z;
        this.A00 = r12;
    }

    public C34631gS(C34361g1 r10, AbstractC14640lm r11, long j, boolean z) {
        this(null, r10, r11, null, j, z, true);
    }

    @Override // X.AnonymousClass1JQ
    public AnonymousClass271 A01() {
        AnonymousClass1G4 A0T = C34621gR.A03.A0T();
        boolean z = this.A02;
        A0T.A03();
        C34621gR r1 = (C34621gR) A0T.A00;
        r1.A00 |= 1;
        r1.A02 = z;
        C34351g0 A04 = this.A00.A04();
        A0T.A03();
        C34621gR r12 = (C34621gR) A0T.A00;
        r12.A01 = A04;
        r12.A00 |= 2;
        AnonymousClass271 A01 = super.A01();
        AnonymousClass009.A05(A01);
        A01.A03();
        C27831Jk r13 = (C27831Jk) A01.A00;
        r13.A04 = (C34621gR) A0T.A02();
        r13.A00 |= 4096;
        return A01;
    }

    @Override // X.AbstractC34381g3
    public AbstractC14640lm ABH() {
        return this.A01;
    }

    @Override // X.AbstractC34391g4
    public C34361g1 AEJ() {
        return this.A00;
    }

    @Override // X.AbstractC34531gI
    public boolean AKD() {
        return !this.A02;
    }

    @Override // X.AnonymousClass1JQ
    public String toString() {
        StringBuilder sb = new StringBuilder("ArchiveChatMutation{rowId=");
        sb.append(this.A07);
        sb.append(", chatJid=");
        sb.append(this.A01);
        sb.append(", isArchived=");
        sb.append(this.A02);
        sb.append(", messageRange=");
        sb.append(this.A00);
        sb.append(", timestamp=");
        sb.append(this.A04);
        sb.append(", areDependenciesMissing=");
        sb.append(A05());
        sb.append(", operation=");
        sb.append(this.A05);
        sb.append(", collectionName=");
        sb.append(this.A06);
        sb.append(", keyId=");
        sb.append(super.A00);
        sb.append('}');
        return sb.toString();
    }
}
