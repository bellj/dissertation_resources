package X;

import android.content.Context;
import android.view.View;

/* renamed from: X.5hM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C121165hM extends AnonymousClass69F {
    @Override // X.AnonymousClass69F, X.AnonymousClass5X2
    public View buildPaymentHelpSupportSection(Context context, AbstractC28901Pl r5, String str) {
        C117705aT r2 = new C117705aT(context);
        r2.setContactInformation(r5, str, this.A02, this.A00);
        return r2;
    }
}
