package X;

import android.view.animation.Animation;
import com.whatsapp.components.CircularRevealView;

/* renamed from: X.4og  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class animation.Animation$AnimationListenerC102154og implements Animation.AnimationListener {
    public final /* synthetic */ CircularRevealView A00;

    public animation.Animation$AnimationListenerC102154og(CircularRevealView circularRevealView) {
        this.A00 = circularRevealView;
    }

    @Override // android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        this.A00.A0C = false;
    }

    @Override // android.view.animation.Animation.AnimationListener
    public void onAnimationRepeat(Animation animation) {
        this.A00.A0C = true;
    }

    @Override // android.view.animation.Animation.AnimationListener
    public void onAnimationStart(Animation animation) {
        this.A00.A0C = true;
    }
}
