package X;

import androidx.core.view.inputmethod.EditorInfoCompat;

/* renamed from: X.12L  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass12L {
    public final C15450nH A00;
    public final C14820m6 A01;
    public final C14850m9 A02;
    public final AnonymousClass11G A03;
    public final C14860mA A04;
    public volatile boolean A05;

    public AnonymousClass12L(C15450nH r1, C14820m6 r2, C14850m9 r3, AnonymousClass11G r4, C14860mA r5) {
        this.A02 = r3;
        this.A04 = r5;
        this.A00 = r1;
        this.A03 = r4;
        this.A01 = r2;
    }

    public byte[] A00() {
        AnonymousClass1G4 A0T = C43811xd.A0l.A0T();
        EnumC43821xe r2 = EnumC43821xe.A02;
        A0T.A03();
        C43811xd r1 = (C43811xd) A0T.A00;
        r1.A01 |= 4;
        r1.A0F = r2.value;
        A0T.A03();
        C43811xd r12 = (C43811xd) A0T.A00;
        r12.A01 |= 64;
        r12.A0K = r2.value;
        A0T.A03();
        C43811xd r13 = (C43811xd) A0T.A00;
        r13.A01 |= 16;
        r13.A04 = r2.value;
        A0T.A03();
        C43811xd r14 = (C43811xd) A0T.A00;
        r14.A01 |= 4096;
        r14.A0J = r2.value;
        A0T.A03();
        C43811xd r15 = (C43811xd) A0T.A00;
        r15.A01 |= EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
        r15.A0Z = r2.value;
        EnumC43821xe r3 = EnumC43821xe.A04;
        A0T.A03();
        C43811xd r5 = (C43811xd) A0T.A00;
        r5.A01 |= 134217728;
        r5.A0c = r3.value;
        A0T.A03();
        C43811xd r52 = (C43811xd) A0T.A00;
        r52.A01 |= 268435456;
        r52.A0b = r3.value;
        A0T.A03();
        C43811xd r16 = (C43811xd) A0T.A00;
        r16.A01 |= 256;
        r16.A0i = r2.value;
        A0T.A03();
        C43811xd r17 = (C43811xd) A0T.A00;
        r17.A01 |= 2;
        r17.A0j = r2.value;
        EnumC43821xe r53 = EnumC43821xe.A01;
        A0T.A03();
        C43811xd r6 = (C43811xd) A0T.A00;
        r6.A01 |= 524288;
        r6.A0k = r53.value;
        A0T.A03();
        C43811xd r62 = (C43811xd) A0T.A00;
        r62.A01 |= 67108864;
        r62.A0h = r53.value;
        A0T.A03();
        C43811xd r63 = (C43811xd) A0T.A00;
        r63.A01 |= EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING;
        r63.A03 = r2.value;
        A0T.A03();
        C43811xd r18 = (C43811xd) A0T.A00;
        r18.A01 |= EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
        r18.A0P = r2.value;
        A0T.A03();
        C43811xd r64 = (C43811xd) A0T.A00;
        r64.A01 |= 65536;
        r64.A0g = r2.value;
        A0T.A03();
        C43811xd r19 = (C43811xd) A0T.A00;
        r19.A01 |= 128;
        r19.A0R = r2.value;
        A0T.A03();
        C43811xd r65 = (C43811xd) A0T.A00;
        r65.A01 |= C25981Bo.A0F;
        r65.A0f = r2.value;
        A0T.A03();
        C43811xd r66 = (C43811xd) A0T.A00;
        r66.A01 |= 4194304;
        r66.A0G = r2.value;
        C15450nH r67 = this.A00;
        if (r67.A05(AbstractC15460nI.A0U)) {
            A0T.A03();
            C43811xd r7 = (C43811xd) A0T.A00;
            r7.A01 |= 2097152;
            r7.A0B = r2.value;
        }
        A0T.A03();
        C43811xd r72 = (C43811xd) A0T.A00;
        r72.A01 |= 1048576;
        r72.A0d = r2.value;
        A0T.A03();
        C43811xd r110 = (C43811xd) A0T.A00;
        r110.A01 |= 16384;
        r110.A0N = r2.value;
        A0T.A03();
        C43811xd r73 = (C43811xd) A0T.A00;
        r73.A01 |= 536870912;
        r73.A09 = r3.value;
        A0T.A03();
        C43811xd r74 = (C43811xd) A0T.A00;
        r74.A01 |= 1073741824;
        r74.A06 = r2.value;
        A0T.A03();
        C43811xd r111 = (C43811xd) A0T.A00;
        r111.A02 |= 1;
        r111.A0U = r3.value;
        A0T.A03();
        C43811xd r75 = (C43811xd) A0T.A00;
        r75.A01 |= 33554432;
        r75.A0X = r3.value;
        if (this.A05) {
            A0T.A03();
            C43811xd r112 = (C43811xd) A0T.A00;
            r112.A02 |= 2;
            r112.A0e = r3.value;
        }
        if (this.A03.A00()) {
            A0T.A03();
            C43811xd r113 = (C43811xd) A0T.A00;
            r113.A02 |= 4;
            r113.A0a = r3.value;
        }
        if (r67.A05(AbstractC15460nI.A0Y)) {
            A0T.A03();
            C43811xd r114 = (C43811xd) A0T.A00;
            r114.A02 |= 8;
            r114.A0D = r2.value;
        }
        C14850m9 r76 = this.A02;
        if (r76.A07(308)) {
            A0T.A03();
            C43811xd r115 = (C43811xd) A0T.A00;
            r115.A02 |= 16;
            r115.A0C = r53.value;
        }
        A0T.A03();
        C43811xd r116 = (C43811xd) A0T.A00;
        r116.A02 |= 32;
        r116.A0W = r3.value;
        A0T.A03();
        C43811xd r117 = (C43811xd) A0T.A00;
        r117.A02 |= 64;
        r117.A00 = r3.value;
        A0T.A03();
        C43811xd r118 = (C43811xd) A0T.A00;
        r118.A02 |= 128;
        r118.A08 = r3.value;
        A0T.A03();
        C43811xd r119 = (C43811xd) A0T.A00;
        r119.A02 |= 256;
        r119.A07 = r3.value;
        if (r76.A07(495)) {
            A0T.A03();
            C43811xd r120 = (C43811xd) A0T.A00;
            r120.A02 |= 512;
            r120.A0L = r2.value;
        }
        A0T.A03();
        C43811xd r121 = (C43811xd) A0T.A00;
        r121.A02 |= EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
        r121.A05 = r3.value;
        if (r76.A02(861) == 2 && !this.A04.A0M() && this.A01.A00.getLong("md_opt_in_awareness_period_deadline", 0) > 0) {
            A0T.A03();
            C43811xd r122 = (C43811xd) A0T.A00;
            r122.A02 |= EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
            r122.A0A = r3.value;
        }
        if (r76.A07(1011)) {
            A0T.A03();
            C43811xd r123 = (C43811xd) A0T.A00;
            r123.A02 |= 4096;
            r123.A0O = r3.value;
        }
        return A0T.A02().A02();
    }
}
