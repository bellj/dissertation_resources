package X;

import java.util.List;

/* renamed from: X.0Gz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C03230Gz extends AnonymousClass0H3 {
    public C03230Gz(List list) {
        super(list);
    }

    @Override // X.AnonymousClass0QR
    public /* bridge */ /* synthetic */ Object A04(AnonymousClass0U8 r2, float f) {
        return Integer.valueOf(A09(r2, f));
    }

    public int A09(AnonymousClass0U8 r7, float f) {
        Object obj;
        Object obj2 = r7.A0F;
        if (obj2 == null || (obj = r7.A09) == null) {
            throw new IllegalStateException("Missing values for keyframe.");
        }
        int intValue = ((Number) obj2).intValue();
        int intValue2 = ((Number) obj).intValue();
        AnonymousClass0SF r3 = this.A03;
        if (r3 != null) {
            r7.A08.floatValue();
            Integer valueOf = Integer.valueOf(intValue);
            Integer valueOf2 = Integer.valueOf(intValue2);
            A02();
            AnonymousClass0NB r0 = r3.A02;
            r0.A01 = valueOf;
            r0.A00 = valueOf2;
            Number number = (Number) r3.A01;
            if (number != null) {
                return number.intValue();
            }
        }
        return AnonymousClass0TF.A02(Math.max(0.0f, Math.min(1.0f, f)), intValue, intValue2);
    }
}
