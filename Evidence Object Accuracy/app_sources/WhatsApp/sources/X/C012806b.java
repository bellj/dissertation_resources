package X;

import android.util.Log;

/* renamed from: X.06b  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C012806b implements AnonymousClass05J {
    public final /* synthetic */ AnonymousClass01F A00;

    public C012806b(AnonymousClass01F r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass05J
    public /* bridge */ /* synthetic */ void ALs(Object obj) {
        StringBuilder sb;
        C06830Vg r6 = (C06830Vg) obj;
        AnonymousClass01F r2 = this.A00;
        C06790Vc r0 = (C06790Vc) r2.A0D.pollFirst();
        if (r0 == null) {
            sb = new StringBuilder("No IntentSenders were started for ");
            sb.append(this);
        } else {
            String str = r0.A01;
            int i = r0.A00;
            AnonymousClass01E A00 = r2.A0U.A00(str);
            if (A00 == null) {
                sb = new StringBuilder("Intent Sender result delivered for unknown Fragment ");
                sb.append(str);
            } else {
                A00.A0t(i, r6.A00, r6.A01);
                return;
            }
        }
        Log.w("FragmentManager", sb.toString());
    }
}
