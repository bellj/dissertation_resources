package X;

import android.view.ViewGroup;
import com.whatsapp.R;

/* renamed from: X.2gQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54252gQ extends AnonymousClass02M {
    public final ActivityC000800j A00;
    public final C74473i5 A01;
    public final int[] A02;
    public final int[] A03;

    public C54252gQ(ActivityC000800j r1, C74473i5 r2, int[] iArr, int[] iArr2) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = iArr;
        this.A03 = iArr2;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A02.length;
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r3, int i) {
        C55242i1 r32 = (C55242i1) r3;
        boolean A1V = C12960it.A1V(i, C12960it.A05(this.A01.A01.A01()));
        r32.A08(A1V, i);
        r32.A01.setChecked(A1V);
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        C55242i1 r2 = new C55242i1(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.group_profile_emoji_editor_color_choice), this.A02, this.A03);
        this.A01.A01.A05(this.A00, r2);
        C12960it.A14(r2.A00, this, r2, 9);
        return r2;
    }
}
