package X;

import com.whatsapp.util.Log;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: X.3DX  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3DX {
    public final C14330lG A00;
    public final C15820nx A01;
    public final AnonymousClass10N A02;
    public final AnonymousClass28Q A03;
    public final C27051Fv A04;
    public final AbstractC44761zV A05;
    public final C44791zY A06;
    public final C15810nw A07;
    public final C17050qB A08;
    public final C14830m7 A09;
    public final C16590pI A0A;
    public final C15890o4 A0B;
    public final C14820m6 A0C;
    public final C14850m9 A0D;
    public final C16120oU A0E;
    public final AnonymousClass317 A0F;
    public final C17220qS A0G;
    public final String A0H;
    public final List A0I;
    public final Map A0J = new ConcurrentHashMap();
    public final AtomicBoolean A0K = new AtomicBoolean(true);
    public final AtomicLong A0L = new AtomicLong(0);
    public final AtomicLong A0M = new AtomicLong(0);
    public final AtomicLong A0N = new AtomicLong(0);
    public final AtomicLong A0O = new AtomicLong(0);
    public final AtomicLong A0P = new AtomicLong(0);
    public final AtomicLong A0Q = new AtomicLong(0);

    public AnonymousClass3DX(C14330lG r4, C15820nx r5, AnonymousClass10N r6, AnonymousClass28Q r7, C27051Fv r8, AbstractC44761zV r9, C44791zY r10, C15810nw r11, C17050qB r12, C14830m7 r13, C16590pI r14, C15890o4 r15, C14820m6 r16, C14850m9 r17, C16120oU r18, AnonymousClass317 r19, C17220qS r20, String str, List list) {
        this.A0A = r14;
        this.A09 = r13;
        this.A0D = r17;
        this.A0H = str;
        this.A0I = list;
        this.A00 = r4;
        this.A02 = r6;
        this.A0E = r18;
        this.A07 = r11;
        this.A0G = r20;
        this.A05 = r9;
        this.A01 = r5;
        this.A03 = r7;
        this.A08 = r12;
        this.A06 = r10;
        this.A04 = r8;
        this.A0B = r15;
        this.A0C = r16;
        this.A0F = r19;
    }

    public void A00() {
        if (this.A0C.A09() != null) {
            AnonymousClass317 r4 = this.A0F;
            AtomicLong atomicLong = this.A0N;
            r4.A04 = Long.valueOf(atomicLong.get());
            Double valueOf = Double.valueOf(((double) atomicLong.get()) / ((double) this.A0P.get()));
            r4.A00 = valueOf;
            r4.A01 = valueOf;
            r4.A06 = r4.A04;
            r4.A07 = Long.valueOf(this.A0M.get() / 1048576);
            Log.i(C12960it.A0b("gdrive/encrypted-re-upload/", r4));
            this.A0E.A07(r4);
        }
        Iterator A00 = AbstractC16230of.A00(this.A02);
        while (A00.hasNext()) {
            ((AnonymousClass10L) A00.next()).APg();
        }
    }
}
