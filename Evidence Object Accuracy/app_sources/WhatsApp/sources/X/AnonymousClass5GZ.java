package X;

import java.security.cert.CertSelector;
import java.security.cert.Certificate;

/* renamed from: X.5GZ  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5GZ implements AbstractC117205Yy {
    public final CertSelector A00;

    public AnonymousClass5GZ(CertSelector certSelector) {
        this.A00 = certSelector;
    }

    @Override // X.AbstractC117205Yy, java.lang.Object
    public Object clone() {
        return new AnonymousClass5GZ(this.A00);
    }

    @Override // X.AbstractC117205Yy
    public /* bridge */ /* synthetic */ boolean ALJ(Object obj) {
        return this.A00.match((Certificate) obj);
    }
}
