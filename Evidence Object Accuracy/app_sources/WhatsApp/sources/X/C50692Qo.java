package X;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.2Qo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C50692Qo extends AbstractC015707l {
    public static final Parcelable.Creator CREATOR = new C98484il();
    public final AnonymousClass00O A00;

    public /* synthetic */ C50692Qo(Parcel parcel, ClassLoader classLoader) {
        super(parcel, classLoader);
        int readInt = parcel.readInt();
        String[] strArr = new String[readInt];
        parcel.readStringArray(strArr);
        Bundle[] bundleArr = new Bundle[readInt];
        parcel.readTypedArray(bundleArr, Bundle.CREATOR);
        this.A00 = new AnonymousClass00O(readInt);
        for (int i = 0; i < readInt; i++) {
            this.A00.put(strArr[i], bundleArr[i]);
        }
    }

    public C50692Qo(Parcelable parcelable) {
        super(parcelable);
        this.A00 = new AnonymousClass00O();
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("ExtendableSavedState{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" states=");
        sb.append(this.A00);
        sb.append("}");
        return sb.toString();
    }

    /* JADX DEBUG: Multi-variable search result rejected for r6v0, resolved type: java.lang.String[] */
    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: android.os.Bundle[] */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // X.AbstractC015707l, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        AnonymousClass00O r8 = this.A00;
        int size = r8.size();
        parcel.writeInt(size);
        String[] strArr = new String[size];
        Bundle[] bundleArr = new Bundle[size];
        for (int i2 = 0; i2 < size; i2++) {
            Object[] objArr = r8.A02;
            int i3 = i2 << 1;
            strArr[i2] = objArr[i3];
            bundleArr[i2] = objArr[i3 + 1];
        }
        parcel.writeStringArray(strArr);
        parcel.writeTypedArray(bundleArr, 0);
    }
}
