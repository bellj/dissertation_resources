package X;

import android.os.IBinder;

/* renamed from: X.3ph  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78883ph extends C98414ie implements AnonymousClass5Y5 {
    public C78883ph(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.auth.api.internal.IAuthService");
    }
}
