package X;

import android.widget.FrameLayout;
import java.util.Map;

/* renamed from: X.4NT  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4NT {
    public final FrameLayout A00;
    public final Map A01;

    public AnonymousClass4NT(FrameLayout frameLayout, Map map) {
        this.A00 = frameLayout;
        this.A01 = map;
    }
}
