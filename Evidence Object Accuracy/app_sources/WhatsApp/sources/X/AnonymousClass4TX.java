package X;

import com.whatsapp.util.ViewOnClickCListenerShape18S0100000_I1_1;

/* renamed from: X.4TX  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4TX {
    public AbstractC15290ms A00;
    public AnonymousClass4QD A01;
    public final AnonymousClass1AC A02;
    public final AnonymousClass19M A03;
    public final C231510o A04;
    public final AnonymousClass193 A05;
    public final AbstractView$OnClickListenerC34281fs A06 = new ViewOnClickCListenerShape18S0100000_I1_1(this, 21);

    public AnonymousClass4TX(AnonymousClass1AC r3, AnonymousClass19M r4, C231510o r5, AnonymousClass193 r6) {
        this.A03 = r4;
        this.A04 = r5;
        this.A02 = r3;
        this.A05 = r6;
    }
}
