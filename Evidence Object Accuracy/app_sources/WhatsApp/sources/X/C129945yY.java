package X;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.widget.ImageView;
import com.whatsapp.payments.ui.widget.PaymentMethodRow;
import com.whatsapp.util.Log;

/* renamed from: X.5yY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129945yY {
    public final C14900mE A00;
    public final C15570nT A01;
    public final C18640sm A02;
    public final C14830m7 A03;
    public final C16590pI A04;
    public final C18650sn A05;
    public final C18610sj A06;
    public final C17070qD A07;
    public final C121875kP A08;

    public C129945yY(C14900mE r1, C15570nT r2, C18640sm r3, C14830m7 r4, C16590pI r5, C18650sn r6, C18610sj r7, C17070qD r8, C121875kP r9) {
        this.A04 = r5;
        this.A03 = r4;
        this.A00 = r1;
        this.A01 = r2;
        this.A07 = r8;
        this.A08 = r9;
        this.A06 = r7;
        this.A02 = r3;
        this.A05 = r6;
    }

    public final void A00(Drawable drawable, ImageView imageView, AbstractC28901Pl r9, boolean z) {
        AbstractC30871Zd r1 = (AbstractC30871Zd) r9.A08;
        if (r1 == null || TextUtils.isEmpty(r1.A0E)) {
            StringBuilder A0k = C12960it.A0k("PAY: Failed to display card art, card art url missing, re-fetch: ");
            A0k.append(z);
            Log.w(A0k.toString());
            if (z) {
                A01(imageView, r9);
                return;
            }
            return;
        }
        C121875kP r0 = this.A08;
        String str = r1.A0E;
        r0.A04.A00(drawable, drawable, imageView, new C134036Db(imageView, r9, this, z), str);
    }

    public final void A01(ImageView imageView, AbstractC28901Pl r20) {
        C119775f5 r3 = (C119775f5) r20.A08;
        if (r3 == null || TextUtils.isEmpty(r3.A0C)) {
            Log.w(C12960it.A0b("PAY: fetchCardArtImageContentDetails card method data invalid: ", r3));
            return;
        }
        C14830m7 r12 = this.A03;
        Context context = this.A04.A00;
        C14900mE r9 = this.A00;
        C15570nT r10 = this.A01;
        C18610sj r14 = this.A06;
        C128235vm r7 = new C128235vm(context, r9, r10, this.A02, r12, this.A05, r14, new C129015x2(imageView, r20, r3, this), r20.A0A, r3.A0C);
        String str = r7.A08;
        if (!TextUtils.isEmpty(str)) {
            String str2 = r7.A09;
            if (!TextUtils.isEmpty(str2)) {
                String A0j = C117305Zk.A0j(r7.A02, r7.A04);
                C18610sj r8 = r7.A06;
                AnonymousClass1W9[] r2 = new AnonymousClass1W9[4];
                C12960it.A1M("action", "get-image-content", r2, 0);
                C12960it.A1M("credential-id", str, r2, 1);
                C12960it.A1M("image-content-id", str2, r2, 2);
                C12960it.A1M("nonce", A0j, r2, 3);
                r8.A0F(new C120165fj(r7.A00, r7.A01, r7.A05, r7), C117315Zl.A0G(r2), "get", C26061Bw.A0L);
                return;
            }
        }
        r7.A07.A00(C117305Zk.A0L(), str);
    }

    public void A02(AbstractC28901Pl r4, PaymentMethodRow paymentMethodRow) {
        C1311161i.A0A(r4, paymentMethodRow);
        A00(C16590pI.A00(this.A04).getDrawable(C1311161i.A00(((C30881Ze) r4).A01)), paymentMethodRow.A01, r4, false);
    }
}
