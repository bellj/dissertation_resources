package X;

import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.zip.ZipInputStream;

/* renamed from: X.5fR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119995fR extends AbstractC44461z0 {
    public final File A00;

    public C119995fR(File file) {
        super(file, 2, 10485760);
        this.A00 = file;
    }

    @Override // X.AbstractC44461z0
    public long A00(File file, ZipInputStream zipInputStream, byte[] bArr) {
        long j = 0;
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(new File(this.A00, file.getName()));
            while (true) {
                int read = zipInputStream.read(bArr);
                if (read == -1) {
                    break;
                }
                fileOutputStream.write(bArr, 0, read);
                j += (long) read;
                if (8192 + j > 10485760) {
                    break;
                }
            }
            fileOutputStream.close();
            return j;
        } catch (FileNotFoundException unused) {
            Log.e("PAY: NoviInviteAssetZipEntrySaver");
            return j;
        }
    }

    @Override // X.AbstractC44461z0
    public boolean A01(File file) {
        if ("webp".equals(C14350lI.A07(file.getAbsolutePath()))) {
            return true;
        }
        Log.e("PAY: NoviInviteAssetZipEntrySaver/store: Zip entry not webp");
        return false;
    }
}
