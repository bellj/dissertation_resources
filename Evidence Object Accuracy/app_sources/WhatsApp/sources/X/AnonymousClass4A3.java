package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.4A3  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4A3 extends Enum {
    public static final /* synthetic */ AnonymousClass4A3[] A00;
    public static final AnonymousClass4A3 A01;
    public static final AnonymousClass4A3 A02;

    public static AnonymousClass4A3 valueOf(String str) {
        return (AnonymousClass4A3) Enum.valueOf(AnonymousClass4A3.class, str);
    }

    public static AnonymousClass4A3[] values() {
        return (AnonymousClass4A3[]) A00.clone();
    }

    static {
        AnonymousClass4A3 r3 = new AnonymousClass4A3("FULL_SCREEN", 0);
        A02 = r3;
        AnonymousClass4A3 r1 = new AnonymousClass4A3("BOTTOMSHEET", 1);
        A01 = r1;
        AnonymousClass4A3[] r0 = new AnonymousClass4A3[2];
        C72453ed.A1J(r3, r1, r0);
        A00 = r0;
    }

    public AnonymousClass4A3(String str, int i) {
    }
}
