package X;

import android.text.Editable;
import android.text.TextWatcher;
import com.whatsapp.payments.ui.IndiaUpiEditTransactionDescriptionFragment;

/* renamed from: X.63v  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1317163v implements TextWatcher {
    public final /* synthetic */ IndiaUpiEditTransactionDescriptionFragment A00;

    @Override // android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
    }

    @Override // android.text.TextWatcher
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public C1317163v(IndiaUpiEditTransactionDescriptionFragment indiaUpiEditTransactionDescriptionFragment) {
        this.A00 = indiaUpiEditTransactionDescriptionFragment;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0017, code lost:
        if (r1.matches("^[a-zA-Z0-9\\s]*$") == false) goto L_0x0019;
     */
    @Override // android.text.TextWatcher
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onTextChanged(java.lang.CharSequence r7, int r8, int r9, int r10) {
        /*
            r6 = this;
            java.lang.String r1 = r7.toString()
            com.whatsapp.payments.ui.IndiaUpiEditTransactionDescriptionFragment r5 = r6.A00
            java.lang.String r0 = r5.A0A
            boolean r4 = r0.equals(r1)
            r3 = 1
            r4 = r4 ^ r3
            if (r1 == 0) goto L_0x0019
            java.lang.String r0 = "^[a-zA-Z0-9\\s]*$"
            boolean r0 = r1.matches(r0)
            r2 = 1
            if (r0 != 0) goto L_0x001a
        L_0x0019:
            r2 = 0
        L_0x001a:
            com.whatsapp.WaTextView r1 = r5.A02
            r0 = 0
            if (r2 == 0) goto L_0x0020
            r0 = 4
        L_0x0020:
            r1.setVisibility(r0)
            if (r2 != 0) goto L_0x002d
            com.whatsapp.WaTextView r1 = r5.A02
            r0 = 2131890237(0x7f12103d, float:1.941516E38)
            r1.setText(r0)
        L_0x002d:
            com.whatsapp.components.Button r0 = r5.A03
            if (r4 == 0) goto L_0x0037
            if (r2 == 0) goto L_0x0037
        L_0x0033:
            r0.setEnabled(r3)
            return
        L_0x0037:
            r3 = 0
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C1317163v.onTextChanged(java.lang.CharSequence, int, int, int):void");
    }
}
