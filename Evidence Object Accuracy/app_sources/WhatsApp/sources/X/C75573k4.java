package X;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.jid.UserJid;

/* renamed from: X.3k4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75573k4 extends AnonymousClass03U {
    public UserJid A00;
    public final ImageView A01;
    public final TextView A02;
    public final TextEmojiLabel A03;

    public C75573k4(View view) {
        super(view);
        ImageView A0L = C12970iu.A0L(view, R.id.contact_photo);
        this.A01 = A0L;
        A0L.setEnabled(false);
        TextEmojiLabel textEmojiLabel = (TextEmojiLabel) view.findViewById(R.id.contact_name);
        this.A03 = textEmojiLabel;
        C27531Hw.A06(textEmojiLabel);
        this.A02 = C12960it.A0J(view, R.id.date_time);
    }
}
