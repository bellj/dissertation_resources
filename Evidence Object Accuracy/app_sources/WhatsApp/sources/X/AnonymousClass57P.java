package X;

/* renamed from: X.57P  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass57P implements AnonymousClass23Q {
    public final /* synthetic */ C35891iw A00;

    public AnonymousClass57P(C35891iw r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass23Q
    public void APf() {
        C35891iw r1 = this.A00;
        if (r1.A02 != null) {
            r1.A03(null);
        }
    }

    @Override // X.AnonymousClass23Q
    public void AVf(C91934Tu r3) {
        C35891iw r1 = this.A00;
        if (r1.A02 != null) {
            C35891iw.A00(r3, r1, 1);
        }
    }
}
