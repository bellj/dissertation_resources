package X;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;

/* renamed from: X.667  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass667 implements AbstractC136166Lg {
    public final /* synthetic */ C126625tB A00;

    public AnonymousClass667(C126625tB r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC136166Lg
    public void AUE(C129975yb r13) {
        ByteBuffer byteBuffer;
        Map map = this.A00.A01;
        C129775yH r0 = (C129775yH) C12990iw.A0l(map, 0);
        if (r0 != null) {
            List list = r0.A00;
            if (0 < list.size()) {
                list.get(0);
                throw C12980iv.A0n("shouldCheckCondition");
            }
        }
        C129775yH r10 = (C129775yH) map.get(C12970iu.A0h());
        C129775yH r8 = (C129775yH) C12990iw.A0l(map, 2);
        if (!(r10 == null && r8 == null)) {
            if (r13.A01 == 1) {
                int i = r13.A00;
                int i2 = r13.A02;
                C128845wl[] r11 = r13.A0C;
                if (!(r11 == null || (byteBuffer = r11[0].A02) == null)) {
                    for (int i3 = 0; i3 < i; i3++) {
                        for (int i4 = 0; i4 < i2; i4++) {
                            C128845wl r02 = r11[0];
                            int i5 = (r02.A01 * i3) + (r02.A00 * i4);
                            byteBuffer.get(i5);
                            byteBuffer.get(i5 + 1);
                            byteBuffer.get(i5 + 2);
                        }
                    }
                }
                if (r10 != null) {
                    List list2 = r10.A00;
                    if (0 < list2.size()) {
                        list2.get(0);
                        throw C12980iv.A0n("shouldCheckCondition");
                    }
                }
                if (r8 != null) {
                    List list3 = r8.A00;
                    if (0 < list3.size()) {
                        list3.get(0);
                        throw C12980iv.A0n("shouldCheckCondition");
                    }
                    return;
                }
                return;
            }
            throw new AnonymousClass6L1("Clipping detection is not support when preview frame data is not in RGBA pixel format");
        }
    }
}
