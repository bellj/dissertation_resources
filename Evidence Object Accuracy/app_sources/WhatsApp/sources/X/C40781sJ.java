package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.1sJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C40781sJ extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C40781sJ A06;
    public static volatile AnonymousClass255 A07;
    public int A00;
    public long A01;
    public AnonymousClass1G8 A02;
    public String A03 = "";
    public String A04 = "";
    public boolean A05;

    static {
        C40781sJ r0 = new C40781sJ();
        A06 = r0;
        r0.A0W();
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            AnonymousClass1G8 r0 = this.A02;
            if (r0 == null) {
                r0 = AnonymousClass1G8.A05;
            }
            i2 = 0 + CodedOutputStream.A0A(r0, 1);
        }
        if ((this.A00 & 2) == 2) {
            i2 += CodedOutputStream.A07(2, this.A04);
        }
        if ((this.A00 & 4) == 4) {
            i2 += CodedOutputStream.A07(3, this.A03);
        }
        int i3 = this.A00;
        if ((i3 & 8) == 8) {
            i2 += CodedOutputStream.A05(4, this.A01);
        }
        if ((i3 & 16) == 16) {
            i2 += CodedOutputStream.A00(5);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            AnonymousClass1G8 r0 = this.A02;
            if (r0 == null) {
                r0 = AnonymousClass1G8.A05;
            }
            codedOutputStream.A0L(r0, 1);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0I(2, this.A04);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0I(3, this.A03);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0H(4, this.A01);
        }
        if ((this.A00 & 16) == 16) {
            codedOutputStream.A0J(5, this.A05);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
