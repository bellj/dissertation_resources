package X;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

/* renamed from: X.4bf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C94354bf {
    public int A00;
    public int A01;
    public ArrayList A02;
    public Map A03;

    public C94354bf() {
        this.A00 = 0;
        this.A01 = 0;
        this.A02 = C12960it.A0l();
    }

    public C94354bf(byte[] bArr) {
        int length = bArr.length;
        this.A03 = C12970iu.A11();
        int i = 0;
        this.A01 = 0;
        this.A00 = 0;
        ArrayList A0l = C12960it.A0l();
        ByteBuffer wrap = ByteBuffer.wrap(bArr);
        while (i < length) {
            short s = wrap.getShort();
            byte[] bArr2 = new byte[2];
            wrap.get(bArr2);
            int A01 = AnonymousClass3JS.A01(bArr2);
            byte[] bArr3 = new byte[A01];
            wrap.get(bArr3);
            A0l.add(new AnonymousClass4VO(bArr3, s));
            this.A03.put(Short.valueOf(s), Integer.valueOf(this.A00));
            this.A00++;
            i += A01 + 4;
        }
        if (i == length) {
            this.A01 = i;
            this.A02 = C12960it.A0l();
            this.A02 = C12980iv.A0x(A0l);
            return;
        }
        throw AnonymousClass1NR.A00("Error while parsing extension", (byte) 80);
    }

    public AnonymousClass4VO A00(short s) {
        int intValue;
        Number number = (Number) this.A03.get(Short.valueOf(s));
        if (number == null || (intValue = number.intValue()) >= this.A00) {
            return null;
        }
        return (AnonymousClass4VO) this.A02.get(intValue);
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("extensions{extensions=");
        A0k.append(Arrays.toString(this.A02.toArray()));
        A0k.append(", idx=");
        A0k.append(this.A00);
        A0k.append(", totalNetworkBytes=");
        A0k.append(this.A01);
        return C12970iu.A0v(A0k);
    }
}
