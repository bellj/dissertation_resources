package X;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import com.whatsapp.R;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

/* renamed from: X.3Uk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C68183Uk implements AbstractC38741od {
    public C58962tj A00;
    public final int A01;

    public C68183Uk(int i) {
        this.A01 = i;
    }

    @Override // X.AbstractC38741od
    public void AMK(AnonymousClass5XB r2) {
        AnonymousClass5TN r0;
        C68203Um r22 = (C68203Um) r2;
        if (r22.A00() && (r0 = r22.A06) != null) {
            r0.AML(r22);
        }
    }

    @Override // X.AbstractC38741od
    public /* bridge */ /* synthetic */ void ARx(AnonymousClass5XB r21) {
        AnonymousClass5TO r0;
        C68203Um r3 = (C68203Um) r21;
        if (r3.A04 == 1) {
            ArrayList A0w = C12980iv.A0w(2);
            C1099453q r10 = new C1099453q(r3, this);
            C44741zT r7 = r3.A05;
            int i = this.A01;
            WeakReference weakReference = r3.A09;
            C68203Um r5 = new C68203Um((ImageView) weakReference.get(), r7, null, null, r10, 2, i, i);
            C68203Um r11 = new C68203Um((ImageView) weakReference.get(), r7, null, null, r10, 3, Integer.MAX_VALUE, Integer.MAX_VALUE);
            A0w.add(r5);
            A0w.add(r11);
            r3.A02 = A0w;
            C58962tj r02 = this.A00;
            if (r02 != null) {
                r02.A01(r5, true);
                this.A00.A01(r11, true);
            }
        } else if (r3.A00() && (r0 = r3.A07) != null) {
            r0.ARz(r3);
        }
    }

    @Override // X.AbstractC38741od
    public /* bridge */ /* synthetic */ void AS3(AnonymousClass5XB r1) {
    }

    @Override // X.AbstractC38741od
    public void AS7(Bitmap bitmap, AnonymousClass5XB r5, boolean z) {
        C68203Um r52 = (C68203Um) r5;
        if (r52.A00()) {
            WeakReference weakReference = r52.A09;
            if (weakReference.get() != null) {
                ((View) weakReference.get()).setTag(R.id.loaded_image_url, r52.AHT());
            }
            r52.A08.AS6(bitmap, r52, z);
        }
    }
}
