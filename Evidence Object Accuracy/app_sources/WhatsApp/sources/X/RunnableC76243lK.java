package X;

import com.facebook.redex.EmptyBaseRunnable0;
import com.whatsapp.util.Log;

/* renamed from: X.3lK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final /* synthetic */ class RunnableC76243lK extends EmptyBaseRunnable0 implements Runnable {
    public final /* synthetic */ int A00;
    public final /* synthetic */ long A01;
    public final /* synthetic */ long A02;
    public final /* synthetic */ C1102354t A03;
    public final /* synthetic */ String A04;

    public /* synthetic */ RunnableC76243lK(C1102354t r1, String str, int i, long j, long j2) {
        this.A03 = r1;
        this.A04 = str;
        this.A00 = i;
        this.A01 = j;
        this.A02 = j2;
    }

    @Override // java.lang.Runnable
    public final void run() {
        AnonymousClass2KP A00;
        AnonymousClass1JT A05;
        C1102354t r7 = this.A03;
        String str = this.A04;
        int i = this.A00;
        long j = this.A01;
        long j2 = this.A02;
        if (str == null || (A00 = AnonymousClass2KP.A00(str)) == null || (A05 = r7.A02.A05(A00.A00, A00.A02)) == null) {
            Log.e("CompanionRegistrationLogger/no session id");
            return;
        }
        C856143m r6 = new C856143m();
        r6.A00 = Integer.valueOf(i);
        r6.A05 = A05.A01;
        r6.A04 = A05.A00;
        Long valueOf = Long.valueOf(C12980iv.A0D(r7.A03.A00()));
        r6.A03 = valueOf;
        r6.A01 = Long.valueOf(valueOf.longValue() - j);
        r6.A02 = Long.valueOf(j2);
        r7.A04.A07(r6);
    }
}
