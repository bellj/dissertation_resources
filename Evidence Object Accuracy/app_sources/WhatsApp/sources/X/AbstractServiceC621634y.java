package X;

import android.content.Intent;
import com.facebook.redex.RunnableBRunnable0Shape15S0100000_I1_1;
import com.facebook.redex.RunnableBRunnable0Shape1S1201000_I1;
import com.whatsapp.R;
import com.whatsapp.migration.export.service.MessagesExporterService;
import com.whatsapp.util.Log;
import java.util.Set;

/* renamed from: X.34y  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractServiceC621634y extends AbstractServiceC27491Hs {
    public int A00 = -1;
    public AbstractC15710nm A01;
    public AbstractC14440lR A02;
    public final AnonymousClass5Z2 A03 = C80893t8.create();
    public final String A04 = "xpm-msg-exporter-svc";
    public final Set A05 = C12970iu.A12();

    public final void A00() {
        synchronized (this) {
            if (this.A03.isEmpty() && this.A05.isEmpty()) {
                stopSelf(this.A00);
            }
        }
    }

    @Override // android.app.Service
    public final int onStartCommand(Intent intent, int i, int i2) {
        Set set;
        Integer valueOf;
        AbstractC15710nm r4;
        String str;
        String str2;
        String str3;
        RunnableBRunnable0Shape15S0100000_I1_1 runnableBRunnable0Shape15S0100000_I1_1;
        boolean z;
        synchronized (this) {
            if (i2 > this.A00) {
                this.A00 = i2;
            }
            set = this.A05;
            valueOf = Integer.valueOf(i2);
            set.add(valueOf);
        }
        MessagesExporterService messagesExporterService = (MessagesExporterService) this;
        if (intent == null) {
            Log.i("xpm-export-service-onStartCommand()/intent is null");
        } else {
            if ("ACTION_START_EXPORT".equals(intent.getAction())) {
                C15730no r5 = messagesExporterService.A00;
                synchronized (r5) {
                    if (r5.A00 == null) {
                        z = false;
                        if (r5.A01 != null) {
                        }
                    }
                    z = true;
                }
                if (z || messagesExporterService.A00.A09()) {
                    Log.i("xpm-export-service-onStartCommand()/export in progress");
                    r4 = ((AbstractServiceC621634y) messagesExporterService).A01;
                    str = "xpm-export-service-export-duplicated-start";
                    str2 = "xpm-export-service-onStartCommand: duplicated call with ACTION_START_EXPORT event - there is another task running export or cancellation";
                    r4.AaV(str, str2, false);
                }
            }
            if ("ACTION_CANCEL_EXPORT".equals(intent.getAction())) {
                if (messagesExporterService.A00.A09()) {
                    Log.i("xpm-export-service-onStartCommand()/cancellation in already in progress");
                    r4 = ((AbstractServiceC621634y) messagesExporterService).A01;
                    str = "xpm-export-service-cancel-duplicated-start";
                    str2 = "xpm-export-service-onStartCommand: duplicated call with ACTION_CANCEL_EXPORT event - there is another task running cancellation";
                    r4.AaV(str, str2, false);
                } else {
                    Log.i("xpm-export-service-onStartCommand()/action_cancel_export");
                    AnonymousClass3F9 r0 = messagesExporterService.A01;
                    C005602s A00 = r0.A00();
                    A00.A0A(C16590pI.A00(r0.A00).getString(R.string.export_notification_cancelling_export));
                    messagesExporterService.startForeground(31, A00.A01());
                    runnableBRunnable0Shape15S0100000_I1_1 = new RunnableBRunnable0Shape15S0100000_I1_1(messagesExporterService.A00, 47);
                    str3 = "cancel-export";
                }
            } else if ("ACTION_START_EXPORT".equals(intent.getAction())) {
                Log.i("xpm-export-service-onStartCommand()/action_start_export");
                AnonymousClass3F9 r02 = messagesExporterService.A01;
                C005602s A002 = r02.A00();
                A002.A0A(C16590pI.A00(r02.A00).getString(R.string.export_notification_exporting));
                messagesExporterService.startForeground(31, A002.A01());
                runnableBRunnable0Shape15S0100000_I1_1 = new RunnableBRunnable0Shape15S0100000_I1_1(messagesExporterService.A00, 48);
                str3 = "export-data";
            }
            StringBuilder A0h = C12960it.A0h();
            A0h.append(((AbstractServiceC621634y) messagesExporterService).A04);
            A0h.append("/");
            A0h.append(str3);
            A0h.append("; async task scheduled (foreground), start_id=");
            A0h.append(i2);
            C12960it.A1F(A0h);
            ((AbstractServiceC621634y) messagesExporterService).A02.Ab6(new RunnableBRunnable0Shape1S1201000_I1(messagesExporterService, runnableBRunnable0Shape15S0100000_I1_1, str3, i2, 0));
            synchronized (this) {
                ((AbstractServiceC621634y) messagesExporterService).A03.add(valueOf);
            }
        }
        synchronized (this) {
            set.remove(valueOf);
            A00();
        }
        return 2;
    }
}
