package X;

import android.graphics.Rect;
import android.transition.Transition;

/* renamed from: X.0Ae  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02140Ae extends Transition.EpicenterCallback {
    public final /* synthetic */ Rect A00;
    public final /* synthetic */ AnonymousClass0EI A01;

    public C02140Ae(Rect rect, AnonymousClass0EI r2) {
        this.A01 = r2;
        this.A00 = rect;
    }

    @Override // android.transition.Transition.EpicenterCallback
    public Rect onGetEpicenter(Transition transition) {
        Rect rect = this.A00;
        if (rect == null || rect.isEmpty()) {
            return null;
        }
        return rect;
    }
}
