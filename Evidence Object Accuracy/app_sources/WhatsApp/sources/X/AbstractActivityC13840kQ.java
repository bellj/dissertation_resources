package X;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import java.util.Collections;
import java.util.List;

/* renamed from: X.0kQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC13840kQ extends AbstractActivityC13850kR {
    public static final int A01 = -1;
    public C36051jF A00;

    public int A1m() {
        return -1;
    }

    /* renamed from: A1r */
    public void A1S() {
    }

    private View A1R() {
        if (A1n().A01) {
            return getWindow().getDecorView();
        }
        return null;
    }

    public AnonymousClass1Q6 A1n() {
        return new AnonymousClass1Q6(A1m());
    }

    public void A1o() {
    }

    public void A1p() {
    }

    public void A1q() {
    }

    public final void A1s() {
        this.A00.A01.A08("data_load");
    }

    public final void A1t() {
        this.A00.A01.A07("data_load");
    }

    public final void A1u() {
        this.A00.A01.A0C(230);
    }

    public final void A1v(String str) {
        this.A00.A01.A07(str);
    }

    public final void A1w(String str) {
        this.A00.A01.A08(str);
    }

    public final void A1x(String str) {
        this.A00.A01.A09(str);
    }

    public final void A1y(String str, boolean z, boolean z2) {
        this.A00.A01.A0B(str, z, z2);
    }

    public final void A1z(short s) {
        this.A00.A01.A0C(s);
    }

    @Override // X.ActivityC000800j, android.app.Activity, android.view.ContextThemeWrapper, android.content.ContextWrapper
    public void attachBaseContext(Context context) {
        String simpleName = getClass().getSimpleName();
        this.A00 = new C36051jF((C18420sQ) ((C250818a) ((AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class)).AA3.get()).A00.A01.AA1.get(), A1n(), simpleName);
        super.attachBaseContext(context);
    }

    @Override // X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        if (!getIntent().getBooleanExtra("key_perf_tracked", false)) {
            long longExtra = getIntent().getLongExtra("perf_start_time_ns", -1);
            String stringExtra = getIntent().getStringExtra("perf_origin");
            if (stringExtra == null) {
                stringExtra = getClass().getSimpleName();
            }
            C36051jF r7 = this.A00;
            View A1R = A1R();
            C48512Go r8 = new AbstractC14780m2() { // from class: X.2Go
                @Override // X.AbstractC14780m2
                public final boolean AM3() {
                    AbstractActivityC13840kQ.this.A1S();
                    return true;
                }
            };
            if (A1R != null && r7.A01.A06.A01) {
                C48522Gp r1 = new C48522Gp(A1R);
                r7.A00 = r1;
                C48532Gq r4 = new C48532Gq(r7, r8);
                AnonymousClass009.A01();
                AnonymousClass009.A01();
                if (r1.A01) {
                    r4.AM4(r1.A00);
                } else {
                    List list = r1.A03;
                    list.add(r4);
                    Collections.sort(list, new C48552Gs());
                }
            }
            if (r7.A01.A0D(stringExtra, longExtra)) {
                getIntent().putExtra("key_perf_tracked", true);
            }
        }
        super.onCreate(bundle);
    }

    @Override // X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        super.onStart();
    }

    @Override // android.app.Activity, android.content.Context, android.content.ContextWrapper
    public void startActivity(Intent intent) {
        super.startActivity(intent);
    }
}
