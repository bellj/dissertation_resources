package X;

import android.text.TextUtils;
import com.whatsapp.chatinfo.view.custom.ContactDetailsCard;

/* renamed from: X.3DR  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3DR {
    public final C253519b A00;
    public final C14900mE A01;
    public final ContactDetailsCard A02;
    public final C15610nY A03;
    public final AnonymousClass01d A04;
    public final AnonymousClass018 A05;
    public final C14850m9 A06;
    public final C17900ra A07;
    public final AnonymousClass14X A08;
    public final boolean A09;

    public AnonymousClass3DR(C253519b r1, C14900mE r2, ContactDetailsCard contactDetailsCard, C15610nY r4, AnonymousClass01d r5, AnonymousClass018 r6, C14850m9 r7, C51442Ut r8, C17900ra r9, AnonymousClass14X r10, boolean z) {
        this.A01 = r2;
        this.A09 = z;
        this.A06 = r7;
        this.A08 = r10;
        this.A00 = r1;
        this.A04 = r5;
        this.A03 = r4;
        this.A05 = r6;
        this.A02 = contactDetailsCard;
        contactDetailsCard.A0F = r8;
        this.A07 = r9;
    }

    public void A00(C15370n3 r7) {
        String A00 = this.A00.A00(r7);
        if (!r7.A0I() || TextUtils.isEmpty(A00)) {
            this.A02.setContactChatStatusVisibility(8);
            return;
        }
        StringBuilder A0h = C12960it.A0h();
        A0h.append(A00.substring(0, 1).toUpperCase(C12970iu.A14(this.A05)));
        String A0d = C12960it.A0d(A00.substring(1), A0h);
        ContactDetailsCard contactDetailsCard = this.A02;
        contactDetailsCard.setContactChatStatusVisibility(0);
        contactDetailsCard.setContactChatStatus(A0d);
    }
}
