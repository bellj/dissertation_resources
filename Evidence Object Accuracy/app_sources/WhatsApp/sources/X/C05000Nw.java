package X;

/* renamed from: X.0Nw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05000Nw {
    public final int A00;
    public final CharSequence A01;
    public final CharSequence A02;
    public final CharSequence A03;
    public final boolean A04;
    public final boolean A05;

    public C05000Nw(CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, boolean z, boolean z2) {
        this.A03 = charSequence;
        this.A02 = charSequence2;
        this.A01 = charSequence3;
        this.A04 = z;
        this.A05 = z2;
        this.A00 = i;
    }
}
