package X;

import com.whatsapp.jid.Jid;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.60z  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1310460z {
    public static final Set A03;
    public String A00;
    public ArrayList A01;
    public ArrayList A02;

    static {
        String[] strArr = new String[21];
        strArr[0] = "account.action";
        strArr[1] = "account.app_install_uuid";
        strArr[2] = "account.risk_period_uuid";
        strArr[3] = "account.client_request_id";
        strArr[4] = "account.device_locale";
        strArr[5] = "account.data_fetch_proof";
        strArr[6] = "account.password";
        strArr[7] = "account.password.password";
        strArr[8] = "account.receiver";
        strArr[9] = "account.wavi_only";
        strArr[10] = "pay.app_install_uuid";
        strArr[11] = "pay.risk_period_uuid";
        strArr[12] = "pay.client_request_id";
        strArr[13] = "pay.device_locale";
        strArr[14] = "pay.service";
        strArr[15] = "pay.transaction-type";
        strArr[16] = "pay.type";
        strArr[17] = "pay.version";
        strArr[18] = "account.version";
        strArr[19] = "account.composite_header";
        A03 = C12970iu.A13("account.graphql_input", strArr, 20);
    }

    public C1310460z(String str) {
        this.A00 = "";
        this.A01 = C12960it.A0l();
        this.A02 = C12960it.A0l();
        this.A00 = str;
    }

    public C1310460z(String str, ArrayList arrayList) {
        this.A00 = "";
        ArrayList A0l = C12960it.A0l();
        this.A01 = A0l;
        this.A02 = C12960it.A0l();
        this.A00 = str;
        A0l.addAll(arrayList);
    }

    public AnonymousClass1V8 A00() {
        AnonymousClass1W9[] r2;
        AnonymousClass1W9 r6;
        String d;
        ArrayList A0l = C12960it.A0l();
        Iterator it = this.A02.iterator();
        while (it.hasNext()) {
            A0l.add(((C1310460z) it.next()).A00());
        }
        ArrayList A0l2 = C12960it.A0l();
        Iterator it2 = this.A01.iterator();
        while (it2.hasNext()) {
            AnonymousClass61S r3 = (AnonymousClass61S) it2.next();
            AnonymousClass601 r0 = r3.A01;
            if (r0 == null) {
                Jid jid = r3.A00;
                if (jid != null) {
                    r6 = new AnonymousClass1W9(jid, r3.A03);
                } else {
                    String str = r3.A03;
                    Object obj = r3.A02;
                    if (obj instanceof String) {
                        d = (String) obj;
                    } else if (obj instanceof Integer) {
                        r6 = new AnonymousClass1W9(str, C12960it.A05(obj));
                    } else if (obj instanceof Long) {
                        r6 = new AnonymousClass1W9(str, C12980iv.A0G(obj));
                    } else if (obj instanceof Boolean) {
                        d = C12970iu.A1Y(obj) ? "true" : "false";
                    } else if (obj instanceof Double) {
                        d = Double.toString(((Number) obj).doubleValue());
                    } else {
                        throw C12980iv.A0u(C12970iu.A0s(obj.getClass(), C12960it.A0k("value type not supported ")));
                    }
                    r6 = new AnonymousClass1W9(str, d);
                }
            } else {
                r6 = new AnonymousClass1W9(r3.A03, r0.toString());
            }
            A0l2.add(r6);
        }
        String str2 = this.A00;
        AnonymousClass1V8[] r1 = null;
        if (A0l2.size() > 0) {
            r2 = C117305Zk.A1b(A0l2);
        } else {
            r2 = null;
        }
        if (A0l.size() > 0) {
            r1 = (AnonymousClass1V8[]) A0l.toArray(new AnonymousClass1V8[0]);
        }
        return new AnonymousClass1V8(str2, r2, r1);
    }

    public final void A01(C127685ut r7, String str) {
        Iterator it = this.A01.iterator();
        while (it.hasNext()) {
            AnonymousClass61S r3 = (AnonymousClass61S) it.next();
            Set set = A03;
            StringBuilder A0j = C12960it.A0j(str);
            A0j.append(".");
            if (!set.contains(C12960it.A0d(r3.A03, A0j))) {
                r3.A01 = new AnonymousClass601(r7, r3.A02);
            }
        }
        Iterator it2 = this.A02.iterator();
        while (it2.hasNext()) {
            C1310460z r2 = (C1310460z) it2.next();
            StringBuilder A0j2 = C12960it.A0j(str);
            A0j2.append(".");
            r2.A01(r7, C12960it.A0d(r2.A00, A0j2));
        }
    }
}
