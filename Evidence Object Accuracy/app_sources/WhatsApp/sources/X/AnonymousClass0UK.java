package X;

import android.os.Build;

/* renamed from: X.0UK  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0UK {
    public static int A00(EnumC03840Ji r2) {
        switch (r2.ordinal()) {
            case 0:
                return 0;
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 3;
            case 4:
                return 4;
            case 5:
                return 5;
            default:
                StringBuilder sb = new StringBuilder("Could not convert ");
                sb.append(r2);
                sb.append(" to int");
                throw new IllegalArgumentException(sb.toString());
        }
    }

    public static EnumC007503u A01(int i) {
        if (i == 0) {
            return EnumC007503u.EXPONENTIAL;
        }
        if (i == 1) {
            return EnumC007503u.LINEAR;
        }
        StringBuilder sb = new StringBuilder("Could not convert ");
        sb.append(i);
        sb.append(" to BackoffPolicy");
        throw new IllegalArgumentException(sb.toString());
    }

    /*  JADX ERROR: JadxRuntimeException in pass: SSATransform
        jadx.core.utils.exceptions.JadxRuntimeException: Not initialized variable reg: 4, insn: 0x0049: IF  (r4 I:??[int, boolean, OBJECT, ARRAY, byte, short, char]) == (0 ??[int, boolean, OBJECT, ARRAY, byte, short, char])  -> B:33:0x0053, block:B:21:0x0049
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVarsInBlock(SSATransform.java:171)
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:143)
        	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:60)
        	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
        */
    public static X.AnonymousClass0P0 A02(
/*
[97] Method generation error in method: X.0UK.A02(byte[]):X.0P0, file: classes.dex
    jadx.core.utils.exceptions.JadxRuntimeException: Code variable not set in r7v0 ??
    	at jadx.core.dex.instructions.args.SSAVar.getCodeVar(SSAVar.java:228)
    	at jadx.core.codegen.MethodGen.addMethodArguments(MethodGen.java:195)
    	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:151)
    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:344)
    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
    	at java.util.ArrayList.forEach(ArrayList.java:1259)
    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
    
*/

    public static EnumC004001t A03(int i) {
        if (i == 0) {
            return EnumC004001t.NOT_REQUIRED;
        }
        if (i == 1) {
            return EnumC004001t.CONNECTED;
        }
        if (i == 2) {
            return EnumC004001t.UNMETERED;
        }
        if (i == 3) {
            return EnumC004001t.NOT_ROAMING;
        }
        if (i == 4) {
            return EnumC004001t.METERED;
        }
        if (Build.VERSION.SDK_INT >= 30 && i == 5) {
            return EnumC004001t.TEMPORARILY_UNMETERED;
        }
        StringBuilder sb = new StringBuilder("Could not convert ");
        sb.append(i);
        sb.append(" to NetworkType");
        throw new IllegalArgumentException(sb.toString());
    }

    public static EnumC006603c A04(int i) {
        if (i == 0) {
            return EnumC006603c.RUN_AS_NON_EXPEDITED_WORK_REQUEST;
        }
        if (i == 1) {
            return EnumC006603c.DROP_WORK_REQUEST;
        }
        StringBuilder sb = new StringBuilder("Could not convert ");
        sb.append(i);
        sb.append(" to OutOfQuotaPolicy");
        throw new IllegalArgumentException(sb.toString());
    }

    public static EnumC03840Ji A05(int i) {
        if (i == 0) {
            return EnumC03840Ji.ENQUEUED;
        }
        if (i == 1) {
            return EnumC03840Ji.RUNNING;
        }
        if (i == 2) {
            return EnumC03840Ji.SUCCEEDED;
        }
        if (i == 3) {
            return EnumC03840Ji.FAILED;
        }
        if (i == 4) {
            return EnumC03840Ji.BLOCKED;
        }
        if (i == 5) {
            return EnumC03840Ji.CANCELLED;
        }
        StringBuilder sb = new StringBuilder("Could not convert ");
        sb.append(i);
        sb.append(" to State");
        throw new IllegalArgumentException(sb.toString());
    }
}
