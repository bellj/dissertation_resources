package X;

/* renamed from: X.0Og  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05100Og {
    public Object A00 = null;
    public boolean A01 = false;
    public final C14260l7 A02;
    public final AnonymousClass28D A03;
    public final Runnable A04 = new RunnableC09420cs(this);

    public C05100Og(C14260l7 r2, AnonymousClass28D r3) {
        this.A02 = r2;
        this.A03 = r3;
    }

    public void A00() {
        AnonymousClass28D r4 = this.A03;
        AbstractC14200l1 A0G = r4.A0G(40);
        if (this.A01 && A0G != null) {
            C14210l2 r2 = new C14210l2();
            C14260l7 r1 = this.A02;
            r2.A04(r1, 0);
            C28701Oq.A01(r1, r4, r2.A03(), A0G);
        }
    }
}
