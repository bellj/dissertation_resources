package X;

import android.content.SharedPreferences;
import com.whatsapp.util.Log;
import java.util.Iterator;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5yU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129905yU {
    public SharedPreferences A00;
    public final C14830m7 A01;
    public final C130155yt A02;
    public final C130025yg A03;
    public final C16630pM A04;

    public C129905yU(C14830m7 r1, C130155yt r2, C130025yg r3, C16630pM r4) {
        this.A01 = r1;
        this.A02 = r2;
        this.A04 = r4;
        this.A03 = r3;
    }

    public final synchronized SharedPreferences A00() {
        SharedPreferences sharedPreferences;
        sharedPreferences = this.A00;
        if (sharedPreferences == null) {
            sharedPreferences = this.A04.A01("novi_country_config");
            this.A00 = sharedPreferences;
        }
        AnonymousClass009.A05(sharedPreferences);
        return sharedPreferences;
    }

    public final Set A01(String str) {
        JSONArray jSONArray;
        String A0p = C12980iv.A0p(A00(), "country_config_lru");
        if (A0p == null) {
            jSONArray = C117315Zl.A0L();
        } else {
            jSONArray = new JSONArray(A0p);
        }
        C006202y r2 = new C006202y(4);
        for (int i = 0; i < jSONArray.length(); i++) {
            String string = jSONArray.getString(i);
            r2.A08(string, string);
        }
        r2.A08(str, str);
        return r2.A05().keySet();
    }

    public void A02(C127665ur r13, String str) {
        JSONObject A05;
        SharedPreferences A00 = A00();
        String string = A00.getString("country_config_locale", null);
        String obj = C1310561a.A02(this.A02.A06).toString();
        if (!obj.equals(string)) {
            A00().edit().remove("country_config_locale").remove("country_config_lru").remove("country_config").apply();
        }
        try {
            Set<Object> A01 = A01(str);
            JSONArray A0L = C117315Zl.A0L();
            for (Object obj2 : A01) {
                A0L.put(obj2);
            }
            String string2 = A00.getString("country_config", null);
            if (string2 == null) {
                A05 = C117295Zj.A0a();
            } else {
                A05 = C13000ix.A05(string2);
            }
            Iterator<String> keys = A05.keys();
            while (keys.hasNext()) {
                if (!A01.contains(keys.next())) {
                    keys.remove();
                }
            }
            JSONArray A0L2 = C117315Zl.A0L();
            for (C127375uO r3 : r13.A03) {
                A0L2.put(C117295Zj.A0a().put("name", r3.A00).put("type", r3.A01).put("is_supported", r3.A02));
            }
            JSONArray A0L3 = C117315Zl.A0L();
            for (C1309860t r0 : r13.A02) {
                A0L3.put(r0.A00());
            }
            JSONArray A0L4 = C117315Zl.A0L();
            for (C1309860t r02 : r13.A01) {
                A0L4.put(r02.A00());
            }
            A05.put(str, C117295Zj.A0a().put("subdivisions", A0L2).put("name", A0L3).put("address", A0L4).put("id", r13.A00.A07()).put("update_ts", this.A01.A00()));
            C12970iu.A1D(A00.edit().putString("country_config_locale", obj).putString("country_config_lru", A0L.toString()), "country_config", A05.toString());
        } catch (JSONException e) {
            Log.e(C12960it.A0b("[PAY] NoviCountryConfigCache/setCountryConfig/JSON exception: ", e));
        }
    }
}
