package X;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.3jp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75423jp extends AnonymousClass03U {
    public final ImageView A00;
    public final TextView A01;

    public /* synthetic */ C75423jp(View view) {
        super(view);
        this.A00 = C12970iu.A0L(view, R.id.participant_avatar);
        this.A01 = C12960it.A0J(view, R.id.participant_name);
    }
}
