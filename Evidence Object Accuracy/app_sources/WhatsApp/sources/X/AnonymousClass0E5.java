package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.0E5  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0E5 extends AbstractC015707l {
    public static final Parcelable.Creator CREATOR = new AnonymousClass0VM();
    public Parcelable A00;

    public AnonymousClass0E5(Parcel parcel, ClassLoader classLoader) {
        super(parcel, classLoader);
        this.A00 = parcel.readParcelable(classLoader == null ? AnonymousClass02H.class.getClassLoader() : classLoader);
    }

    public AnonymousClass0E5(Parcelable parcelable) {
        super(parcelable);
    }

    @Override // X.AbstractC015707l, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeParcelable(this.A00, 0);
    }
}
