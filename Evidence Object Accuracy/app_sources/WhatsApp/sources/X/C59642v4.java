package X;

/* renamed from: X.2v4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C59642v4 extends C37171lc {
    public final C48122Ek A00;
    public final AnonymousClass1WK A01;

    @Override // X.C37171lc
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C59642v4) {
                C59642v4 r5 = (C59642v4) obj;
                if (!C16700pc.A0O(this.A00, r5.A00) || !C16700pc.A0O(this.A01, r5.A01)) {
                }
            }
            return false;
        }
        return true;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C59642v4(C48122Ek r2, AnonymousClass1WK r3) {
        super(AnonymousClass39o.A0L);
        C16700pc.A0E(r2, 1);
        this.A00 = r2;
        this.A01 = r3;
    }

    @Override // X.C37171lc
    public int hashCode() {
        return C12990iw.A08(this.A01, this.A00.hashCode() * 31);
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("NearbyBusinessWidgetHeaderItem(searchLocation=");
        A0k.append(this.A00);
        A0k.append(", onLocationClickListener=");
        return C12960it.A0a(this.A01, A0k);
    }
}
