package X;

import java.util.ArrayList;

/* renamed from: X.5zT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C130475zT {
    public static final ArrayList A01;
    public final AnonymousClass1V8 A00;

    static {
        String[] strArr = new String[2];
        strArr[0] = "1";
        A01 = C12960it.A0m("2", strArr, 1);
    }

    public C130475zT(AnonymousClass3CS r7, String str) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy A0N = C117295Zj.A0N(A0M);
        C41141sy.A01(A0N, "action", "upi-get-blocked-vpas");
        if (str != null && C117295Zj.A1W(str, 0, true)) {
            C41141sy.A01(A0N, "hash", str);
        }
        A0N.A0A("2", "version", A01);
        C117295Zj.A1H(A0N, A0M);
        r7.A00(A0M, C117305Zk.A0q(r7.A00, A0M, C12960it.A0l()));
        this.A00 = A0M.A03();
    }
}
