package X;

/* renamed from: X.5Kr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C114225Kr extends AbstractC11120fm {
    public final Thread A00;

    public C114225Kr(Thread thread) {
        this.A00 = thread;
    }

    @Override // X.AbstractC114245Kt
    public Thread A0B() {
        return this.A00;
    }
}
