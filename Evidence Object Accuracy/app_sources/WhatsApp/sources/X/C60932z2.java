package X;

import com.whatsapp.search.views.itemviews.AudioPlayerView;
import com.whatsapp.search.views.itemviews.VoiceNoteProfileAvatarView;

/* renamed from: X.2z2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60932z2 extends AnonymousClass3WO {
    public final /* synthetic */ AnonymousClass5U1 A00;
    public final /* synthetic */ C44061y8 A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C60932z2(AnonymousClass5U0 r2, AnonymousClass5U1 r3, AnonymousClass5U1 r4, C44061y8 r5, AudioPlayerView audioPlayerView) {
        super(null, r2, r3, audioPlayerView);
        this.A01 = r5;
        this.A00 = r4;
    }

    @Override // X.AnonymousClass2MF
    public C30421Xi ACr() {
        return ((AbstractC44071y9) this.A01).A09;
    }

    @Override // X.AnonymousClass3WO, X.AnonymousClass2MF
    public void AWo(int i, boolean z) {
        super.AWo(i, z);
        C44061y8 r1 = this.A01;
        if (r1.A0D) {
            VoiceNoteProfileAvatarView voiceNoteProfileAvatarView = r1.A0A;
            AnonymousClass1IS r0 = ((AbstractC44071y9) r1).A09.A0z;
            voiceNoteProfileAvatarView.A02(0, false, r0.A02, C15380n4.A0J(r0.A00));
        }
    }
}
