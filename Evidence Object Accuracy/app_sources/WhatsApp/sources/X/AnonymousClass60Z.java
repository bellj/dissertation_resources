package X;

import android.text.TextUtils;
import android.util.Base64;
import com.whatsapp.util.Log;
import com.whatsapp.wamsys.JniBridge;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.interfaces.ECPublicKey;
import java.security.interfaces.RSAKey;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.ECPoint;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import javax.crypto.KeyAgreement;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.60Z  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass60Z {
    public final C14830m7 A00;
    public final C18600si A01;
    public final C130715zr A02;
    public final C130135yr A03;
    public final JniBridge A04;

    public AnonymousClass60Z(C14830m7 r1, C18600si r2, C130715zr r3, C130135yr r4, JniBridge jniBridge) {
        this.A00 = r1;
        this.A04 = jniBridge;
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = r4;
    }

    public static final String A00(String str, String str2, String str3) {
        JSONObject A0a = C117295Zj.A0a();
        A0a.put("clientDeviceID", str);
        if (!TextUtils.isEmpty(str2)) {
            A0a.put("clientReferenceID", str2);
        }
        A0a.put("vProvisionedTokenID", str3);
        return C117305Zk.A0l(String.valueOf(new SecureRandom().nextInt(100000)), "nonce", A0a);
    }

    public final String A01(int i) {
        PrivateKey A00 = this.A03.A00(i);
        if (A00 == null) {
            return null;
        }
        StringBuilder A0k = C12960it.A0k("-----BEGIN PRIVATE KEY-----\r\n");
        A0k.append(Base64.encodeToString(A00.getEncoded(), 0));
        return C12960it.A0d("-----END PRIVATE KEY-----\r\n\u0000", A0k);
    }

    public String A02(AnonymousClass6B5 r23, String str) {
        KeyPair generateKeyPair;
        byte[] A0D;
        String encodeToString;
        String str2;
        byte[] bArr;
        try {
            ECGenParameterSpec eCGenParameterSpec = new ECGenParameterSpec("secp256r1");
            KeyPairGenerator instance = KeyPairGenerator.getInstance("EC");
            instance.initialize(eCGenParameterSpec);
            generateKeyPair = instance.generateKeyPair();
            A0D = C003501n.A0D(12);
            encodeToString = Base64.encodeToString(A0D, 11);
            JSONObject A0a = C117295Zj.A0a();
            ECPoint w = ((ECPublicKey) generateKeyPair.getPublic()).getW();
            try {
                A0a.put("alg", "ECDH-ES").put("enc", "A256GCM").put("epk", C117295Zj.A0a().put("kty", "EC").put("crv", "P-256").put("x", Base64.encodeToString(w.getAffineX().toByteArray(), 2)).put("y", Base64.encodeToString(w.getAffineY().toByteArray(), 2)));
            } catch (JSONException e) {
                Log.e("PAY: BrazilTokenizationHelper/generateEncodedEloHeader failed: ", e);
            }
            try {
                str2 = Base64.encodeToString(C117315Zl.A0a(A0a.toString()), 11);
            } catch (UnsupportedEncodingException e2) {
                Log.e("PAY: BrazilTokenizationHelper/generateEncodedEloHeader failed: ", e2);
                str2 = "";
            }
            bArr = new byte[0];
            try {
                bArr = str2.getBytes("US-ASCII");
            } catch (UnsupportedEncodingException e3) {
                Log.e("PAY: BrazilTokenizationHelper/fetchBindingInfo failed generating aad: ", e3);
            }
        } catch (InvalidAlgorithmParameterException | NoSuchAlgorithmException e4) {
            Log.e("PAY: BrazilTokenizationHelper/fetchBindingInfo failed generating ephemeral key: ", e4);
        }
        try {
            PublicKey generatePublic = KeyFactory.getInstance("EC").generatePublic(new X509EncodedKeySpec(r23.A00));
            if (generatePublic != null) {
                byte[] bArr2 = new byte[0];
                try {
                    PrivateKey privateKey = generateKeyPair.getPrivate();
                    KeyAgreement instance2 = KeyAgreement.getInstance("ECDH");
                    instance2.init(privateKey);
                    instance2.doPhase(generatePublic, true);
                    bArr2 = instance2.generateSecret();
                } catch (InvalidKeyException | NoSuchAlgorithmException e5) {
                    Log.e("PAY: BrazilTokenizationHelper/fetchBindingInfo failed generating sharedSecret: ", e5);
                }
                byte[] bArr3 = new byte[0];
                try {
                    byte[] A03 = C16050oM.A03(0);
                    byte[] A032 = C16050oM.A03(0);
                    boolean z = true;
                    byte[] A033 = C16050oM.A03(1);
                    byte[] bytes = "A256GCM".getBytes(AnonymousClass01V.A08);
                    byte[] A034 = C16050oM.A03(256);
                    int length = A033.length;
                    int length2 = bArr2.length;
                    int length3 = bytes.length;
                    int length4 = A03.length;
                    int length5 = A032.length;
                    int length6 = A034.length;
                    int length7 = length + length2 + C16050oM.A03(length3).length + length3 + length4 + length5 + length6;
                    byte[] bArr4 = new byte[length7];
                    System.arraycopy(A033, 0, bArr4, 0, length);
                    int i = length + 0;
                    System.arraycopy(bArr2, 0, bArr4, i, length2);
                    int i2 = i + length2;
                    System.arraycopy(C16050oM.A03(length3), 0, bArr4, i2, C16050oM.A03(length3).length);
                    int length8 = i2 + C16050oM.A03(length3).length;
                    System.arraycopy(bytes, 0, bArr4, length8, length3);
                    int i3 = length8 + length3;
                    System.arraycopy(A03, 0, bArr4, i3, length4);
                    int i4 = i3 + length4;
                    System.arraycopy(A032, 0, bArr4, i4, length5);
                    int i5 = i4 + length5;
                    System.arraycopy(A034, 0, bArr4, i5, length6);
                    if (i5 + length6 != length7) {
                        z = false;
                    }
                    AnonymousClass009.A0B("length doesn't match", z);
                    bArr3 = MessageDigest.getInstance("SHA-256").digest(bArr4);
                } catch (UnsupportedEncodingException | NoSuchAlgorithmException e6) {
                    Log.e("PAY: BrazilTokenizationHelper/fetchBindingInfo failed symmetric key generation: ", e6);
                }
                byte[] bArr5 = new byte[0];
                try {
                    bArr5 = (byte[]) JniBridge.jvidispatchOIOOOO(4, (long) 16, bArr3, A0D, C117315Zl.A0a(str), bArr);
                } catch (UnsupportedEncodingException e7) {
                    Log.e("PAY: BrazilTokenizationHelper/fetchBindingInfo failed: ", e7);
                }
                if (bArr5 != null) {
                    int length9 = bArr5.length;
                    int i6 = length9 - 16;
                    byte[] copyOfRange = Arrays.copyOfRange(bArr5, 0, i6);
                    AnonymousClass009.A05(copyOfRange);
                    String encodeToString2 = Base64.encodeToString(copyOfRange, 11);
                    byte[] copyOfRange2 = Arrays.copyOfRange(bArr5, i6, length9);
                    AnonymousClass009.A05(copyOfRange2);
                    return TextUtils.join(".", new String[]{str2, "", encodeToString, encodeToString2, Base64.encodeToString(copyOfRange2, 11)});
                }
            }
            return null;
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e8) {
            throw new AssertionError(e8);
        }
    }

    public String A03(AnonymousClass6B7 r7, String str) {
        try {
            String A0l = C117305Zk.A0l("PS256", "alg", C117295Zj.A0a());
            PrivateKey A00 = this.A03.A00(1);
            String str2 = null;
            if (A00 != null) {
                try {
                    str2 = this.A02.A01(r7, C117295Zj.A0a().put("otp", str).put("certID", C130715zr.A00(((RSAKey) A00).getModulus())).toString(), false);
                } catch (JSONException e) {
                    Log.w("PAY: BrazilTokenizationHelper/generateStepUpJweToken failed: ", e);
                }
            }
            return A06(A0l, str2);
        } catch (JSONException e2) {
            Log.w("PAY: generateJwsTokenForCode failed: ", e2);
            return null;
        }
    }

    public String A04(String str) {
        if (!TextUtils.isEmpty(str)) {
            String A01 = A01(5);
            if (!TextUtils.isEmpty(A01)) {
                return C117305Zk.A0n((byte[]) JniBridge.jvidispatchOOO(9, str.getBytes(), A01.getBytes()));
            }
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x008f A[Catch: InvalidKeyException | NoSuchAlgorithmException -> 0x00b3, InvalidKeyException | NoSuchAlgorithmException -> 0x00b3, TryCatch #0 {InvalidKeyException | NoSuchAlgorithmException -> 0x00b3, blocks: (B:4:0x0009, B:6:0x001c, B:6:0x001c, B:7:0x0021, B:7:0x0021, B:8:0x0025, B:8:0x0025, B:9:0x002c, B:9:0x002c, B:10:0x0033, B:10:0x0033, B:12:0x003c, B:12:0x003c, B:14:0x0042, B:14:0x0042, B:16:0x0087, B:16:0x0087, B:19:0x008f, B:19:0x008f, B:21:0x0095, B:21:0x0095), top: B:26:0x0009 }] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0095 A[Catch: InvalidKeyException | NoSuchAlgorithmException -> 0x00b3, InvalidKeyException | NoSuchAlgorithmException -> 0x00b3, TryCatch #0 {InvalidKeyException | NoSuchAlgorithmException -> 0x00b3, blocks: (B:4:0x0009, B:6:0x001c, B:6:0x001c, B:7:0x0021, B:7:0x0021, B:8:0x0025, B:8:0x0025, B:9:0x002c, B:9:0x002c, B:10:0x0033, B:10:0x0033, B:12:0x003c, B:12:0x003c, B:14:0x0042, B:14:0x0042, B:16:0x0087, B:16:0x0087, B:19:0x008f, B:19:0x008f, B:21:0x0095, B:21:0x0095), top: B:26:0x0009 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A05(java.lang.String r12) {
        /*
            r11 = this;
            java.lang.String r5 = "HmacSHA256"
            boolean r0 = android.text.TextUtils.isEmpty(r12)
            r4 = 0
            if (r0 != 0) goto L_0x00b9
            java.lang.String r10 = "SHA-256"
            java.lang.String r3 = ""
            X.0si r2 = r11.A01     // Catch: NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
            android.content.SharedPreferences r1 = r2.A01()     // Catch: NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
            java.lang.String r0 = "payment_trusted_device_elo_wallet_store"
            r9 = 0
            java.lang.String r0 = r1.getString(r0, r4)     // Catch: NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
            if (r0 == 0) goto L_0x0021
            org.json.JSONObject r1 = X.C13000ix.A05(r0)     // Catch: JSONException -> 0x002c, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
            goto L_0x0025
        L_0x0021:
            org.json.JSONObject r1 = X.C117295Zj.A0a()     // Catch: JSONException -> 0x002c, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
        L_0x0025:
            java.lang.String r0 = "wallet_secret"
            java.lang.String r9 = r1.getString(r0)     // Catch: JSONException -> 0x002c, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
            goto L_0x0033
        L_0x002c:
            X.1Zj r1 = r2.A02     // Catch: NoSuchAlgorithmException | InvalidKeyException -> 0x00b3, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
            java.lang.String r0 = "Failed to get the wallet_secret"
            r1.A06(r0)     // Catch: NoSuchAlgorithmException | InvalidKeyException -> 0x00b3, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
        L_0x0033:
            X.5yr r1 = r11.A03     // Catch: NoSuchAlgorithmException | InvalidKeyException -> 0x00b3, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
            r0 = 5
            java.security.PrivateKey r8 = r1.A00(r0)     // Catch: NoSuchAlgorithmException | InvalidKeyException -> 0x00b3, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
            if (r8 == 0) goto L_0x008c
            boolean r0 = android.text.TextUtils.isEmpty(r9)     // Catch: NoSuchAlgorithmException | InvalidKeyException -> 0x00b3, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
            if (r0 != 0) goto L_0x008c
            java.lang.String r0 = "RSA/ECB/OAEPPadding"
            javax.crypto.Cipher r7 = javax.crypto.Cipher.getInstance(r0)     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException | UnsupportedEncodingException -> 0x0086, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
            java.lang.String r6 = "MGF1"
            java.security.spec.MGF1ParameterSpec r2 = new java.security.spec.MGF1ParameterSpec     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException | UnsupportedEncodingException -> 0x0086, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
            r2.<init>(r10)     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException | UnsupportedEncodingException -> 0x0086, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
            javax.crypto.spec.PSource$PSpecified r0 = javax.crypto.spec.PSource.PSpecified.DEFAULT     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException | UnsupportedEncodingException -> 0x0086, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
            javax.crypto.spec.OAEPParameterSpec r1 = new javax.crypto.spec.OAEPParameterSpec     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException | UnsupportedEncodingException -> 0x0086, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
            r1.<init>(r10, r6, r2, r0)     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException | UnsupportedEncodingException -> 0x0086, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
            r0 = 2
            r7.init(r0, r8, r1)     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException | UnsupportedEncodingException -> 0x0086, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
            byte[] r0 = android.util.Base64.decode(r9, r0)     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException | UnsupportedEncodingException -> 0x0086, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
            byte[] r2 = r7.doFinal(r0)     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException | UnsupportedEncodingException -> 0x0086, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
            java.lang.String r0 = X.AnonymousClass01V.A08     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException | UnsupportedEncodingException -> 0x0086, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
            java.lang.String r1 = new java.lang.String     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException | UnsupportedEncodingException -> 0x0086, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
            r1.<init>(r2, r0)     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException | UnsupportedEncodingException -> 0x0086, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
            java.lang.String r0 = "-----BEGIN (.*)-----"
            java.lang.String r1 = r1.replaceAll(r0, r3)     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException | UnsupportedEncodingException -> 0x0086, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
            java.lang.String r0 = "-----END (.*)----"
            java.lang.String r1 = r1.replaceAll(r0, r3)     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException | UnsupportedEncodingException -> 0x0086, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
            java.lang.String r0 = "\r\n"
            java.lang.String r1 = r1.replaceAll(r0, r3)     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException | UnsupportedEncodingException -> 0x0086, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
            java.lang.String r0 = "\n"
            java.lang.String r0 = r1.replaceAll(r0, r3)     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException | UnsupportedEncodingException -> 0x0086, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
            java.lang.String r0 = r0.trim()     // Catch: NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException | UnsupportedEncodingException -> 0x0086, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
            goto L_0x008d
        L_0x0086:
            r1 = move-exception
            java.lang.String r0 = "PAY: BrazilTokenizationHelper/fetchEloWalletSecret failed: "
            com.whatsapp.util.Log.e(r0, r1)     // Catch: NoSuchAlgorithmException | InvalidKeyException -> 0x00b3, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
        L_0x008c:
            r0 = 0
        L_0x008d:
            if (r0 != 0) goto L_0x0095
            java.lang.String r0 = "PAY: BrazilTokenizationHelper/signEloDataWithWalletSecret failed because secret is empty"
            com.whatsapp.util.Log.e(r0)     // Catch: NoSuchAlgorithmException | InvalidKeyException -> 0x00b3, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
            return r4
        L_0x0095:
            r3 = 2
            byte[] r2 = android.util.Base64.decode(r0, r3)     // Catch: NoSuchAlgorithmException | InvalidKeyException -> 0x00b3, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
            javax.crypto.Mac r1 = javax.crypto.Mac.getInstance(r5)     // Catch: NoSuchAlgorithmException | InvalidKeyException -> 0x00b3, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
            javax.crypto.spec.SecretKeySpec r0 = new javax.crypto.spec.SecretKeySpec     // Catch: NoSuchAlgorithmException | InvalidKeyException -> 0x00b3, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
            r0.<init>(r2, r5)     // Catch: NoSuchAlgorithmException | InvalidKeyException -> 0x00b3, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
            r1.init(r0)     // Catch: NoSuchAlgorithmException | InvalidKeyException -> 0x00b3, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
            byte[] r0 = r12.getBytes()     // Catch: NoSuchAlgorithmException | InvalidKeyException -> 0x00b3, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
            byte[] r0 = r1.doFinal(r0)     // Catch: NoSuchAlgorithmException | InvalidKeyException -> 0x00b3, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
            java.lang.String r0 = android.util.Base64.encodeToString(r0, r3)     // Catch: NoSuchAlgorithmException | InvalidKeyException -> 0x00b3, NoSuchAlgorithmException | InvalidKeyException -> 0x00b3
            return r0
        L_0x00b3:
            r1 = move-exception
            java.lang.String r0 = "PAY: BrazilTokenizationHelper/signEloDataWithWalletSecret failed: "
            com.whatsapp.util.Log.e(r0, r1)
        L_0x00b9:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass60Z.A05(java.lang.String):java.lang.String");
    }

    public final String A06(String str, String str2) {
        if (!TextUtils.isEmpty(str2)) {
            try {
                String str3 = AnonymousClass01V.A08;
                String join = TextUtils.join(".", new String[]{Base64.encodeToString(str.getBytes(str3), 11), Base64.encodeToString(str2.getBytes(str3), 11)});
                String A01 = A01(1);
                if (!TextUtils.isEmpty(A01)) {
                    StringBuilder A0j = C12960it.A0j(join);
                    A0j.append(".");
                    return C12960it.A0d(Base64.encodeToString((byte[]) JniBridge.jvidispatchOOO(7, join.getBytes(), A01.getBytes()), 11), A0j);
                }
            } catch (UnsupportedEncodingException e) {
                Log.w("PAY: generateJwsToken threw UnsupportedEncoding Exception: ", e);
            }
        }
        return null;
    }
}
