package X;

import java.util.Set;

/* renamed from: X.4Sf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C91564Sf {
    public final C14900mE A00;
    public final C18790t3 A01;
    public final C242814x A02;
    public final AbstractC14440lR A03;
    public final Set A04 = C12970iu.A12();

    public C91564Sf(C14900mE r2, C18790t3 r3, C242814x r4, AbstractC14440lR r5) {
        this.A00 = r2;
        this.A03 = r5;
        this.A01 = r3;
        this.A02 = r4;
    }
}
