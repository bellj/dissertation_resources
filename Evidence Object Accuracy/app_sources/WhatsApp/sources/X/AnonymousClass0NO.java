package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0NO  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0NO {
    public final List A00;
    public final List A01;
    public final List A02;

    public AnonymousClass0NO(List list) {
        this.A01 = list;
        this.A00 = new ArrayList(list.size());
        this.A02 = new ArrayList(list.size());
        for (int i = 0; i < list.size(); i++) {
            this.A00.add(new AnonymousClass0Gs(((C04860Ni) list.get(i)).A01.A00));
            this.A02.add(new AnonymousClass0H0(((C04860Ni) list.get(i)).A00.A00));
        }
    }
}
