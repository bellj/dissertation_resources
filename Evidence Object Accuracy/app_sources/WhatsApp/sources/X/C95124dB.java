package X;

import androidx.core.view.inputmethod.EditorInfoCompat;

/* renamed from: X.4dB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C95124dB {
    public static final int[] A00 = {2002, 2000, 1920, 1601, 1600, 1001, 1000, 960, 800, 800, 480, 400, 400, EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH};

    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0084, code lost:
        if (r4 != 3) goto L_0x0086;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0088, code lost:
        if (r4 != 11) goto L_0x0066;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x008f, code lost:
        if (r4 != 8) goto L_0x0066;
     */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x006c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C90714Oy A00(X.C95054d0 r11) {
        /*
            r0 = 16
            int r2 = r11.A04(r0)
            int r6 = r11.A04(r0)
            r9 = 4
            r1 = 65535(0xffff, float:9.1834E-41)
            r0 = 4
            if (r6 != r1) goto L_0x0018
            r0 = 24
            int r6 = r11.A04(r0)
            r0 = 7
        L_0x0018:
            int r6 = r6 + r0
            r0 = 44097(0xac41, float:6.1793E-41)
            if (r2 != r0) goto L_0x0020
            int r6 = r6 + 2
        L_0x0020:
            r8 = 2
            int r0 = r11.A04(r8)
            r7 = 3
            if (r0 != r7) goto L_0x0031
        L_0x0028:
            r11.A04(r8)
            boolean r0 = r11.A0C()
            if (r0 != 0) goto L_0x0028
        L_0x0031:
            r0 = 10
            int r10 = r11.A04(r0)
            boolean r0 = r11.A0C()
            if (r0 == 0) goto L_0x0046
            int r0 = r11.A04(r7)
            if (r0 <= 0) goto L_0x0046
            r11.A08(r8)
        L_0x0046:
            boolean r2 = r11.A0C()
            r1 = 48000(0xbb80, float:6.7262E-41)
            r0 = 44100(0xac44, float:6.1797E-41)
            r5 = 44100(0xac44, float:6.1797E-41)
            if (r2 == 0) goto L_0x0058
            r5 = 48000(0xbb80, float:6.7262E-41)
        L_0x0058:
            int r4 = r11.A04(r9)
            if (r5 != r0) goto L_0x006c
            r0 = 13
            if (r4 != r0) goto L_0x0092
            int[] r0 = X.C95124dB.A00
            r3 = r0[r4]
        L_0x0066:
            X.4Oy r0 = new X.4Oy
            r0.<init>(r5, r6, r3)
            return r0
        L_0x006c:
            if (r5 != r1) goto L_0x0092
            int[] r1 = X.C95124dB.A00
            int r0 = r1.length
            if (r4 >= r0) goto L_0x0092
            r3 = r1[r4]
            int r2 = r10 % 5
            r1 = 8
            r0 = 1
            if (r2 == r0) goto L_0x008d
            r0 = 11
            if (r2 == r8) goto L_0x0086
            if (r2 == r7) goto L_0x008d
            if (r2 != r9) goto L_0x0066
            if (r4 == r7) goto L_0x008a
        L_0x0086:
            if (r4 == r1) goto L_0x008a
            if (r4 != r0) goto L_0x0066
        L_0x008a:
            int r3 = r3 + 1
            goto L_0x0066
        L_0x008d:
            if (r4 == r7) goto L_0x008a
            if (r4 != r1) goto L_0x0066
            goto L_0x008a
        L_0x0092:
            r3 = 0
            goto L_0x0066
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95124dB.A00(X.4d0):X.4Oy");
    }

    public static void A01(C95304dT r2, int i) {
        r2.A0Q(7);
        byte[] bArr = r2.A02;
        bArr[0] = -84;
        bArr[1] = 64;
        bArr[2] = -1;
        bArr[3] = -1;
        bArr[4] = (byte) ((i >> 16) & 255);
        bArr[5] = (byte) ((i >> 8) & 255);
        bArr[6] = (byte) (i & 255);
    }
}
