package X;

import android.content.Context;
import android.view.View;

/* renamed from: X.3xU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83593xU extends AbstractC52172aN {
    public final /* synthetic */ AnonymousClass1OY A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C83593xU(Context context, AnonymousClass1OY r2) {
        super(context);
        this.A00 = r2;
    }

    @Override // X.AbstractC116465Vn
    public void onClick(View view) {
        AnonymousClass1OY r4 = this.A00;
        AbstractC15340mz fMessage = r4.getFMessage();
        AbstractC13890kV r2 = ((AbstractC28551Oa) r4).A0a;
        if (r2 != null) {
            int AH8 = r2.AH8(fMessage);
            if (AH8 == 0) {
                AH8 = 1;
            }
            r2.Ad0(fMessage, AH8 + 1);
            r4.A0s();
        }
    }
}
