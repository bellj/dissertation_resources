package X;

import android.content.ContentValues;
import android.util.Base64;
import com.whatsapp.MediaData;
import com.whatsapp.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.ObjectOutputStream;

/* renamed from: X.1Vq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30021Vq {
    public static Object A00(C15810nw r3, Object obj) {
        MediaData mediaData;
        MediaData mediaData2;
        MediaData mediaData3;
        File file;
        if (!(obj instanceof MediaData) || (file = (mediaData3 = (MediaData) obj).file) == null || !file.isAbsolute()) {
            boolean z = obj instanceof C16150oX;
            mediaData = obj;
            if (z) {
                C16150oX r1 = (C16150oX) obj;
                File file2 = r1.A0F;
                mediaData = obj;
                if (file2 != null) {
                    mediaData = obj;
                    if (file2.isAbsolute()) {
                        mediaData2 = r1.A01();
                    }
                }
            }
            return mediaData;
        }
        MediaData mediaData4 = new MediaData(mediaData3);
        mediaData4.autodownloadRetryEnabled = mediaData3.autodownloadRetryEnabled;
        mediaData4.progress = mediaData3.progress;
        mediaData4.A00 = mediaData3.A00;
        mediaData4.transcoded = mediaData3.transcoded;
        mediaData4.trimFrom = mediaData3.trimFrom;
        mediaData4.trimTo = mediaData3.trimTo;
        mediaData4.hasStreamingSidecar = mediaData3.hasStreamingSidecar;
        mediaData2 = mediaData4;
        mediaData2.file = new File(((File) r3.A03.get()).toURI().relativize(mediaData2.file.toURI()).getPath());
        mediaData = mediaData2;
        return mediaData;
    }

    public static void A01(ContentValues contentValues, C15810nw r4, AbstractC15340mz r5) {
        double d;
        double d2;
        String str;
        int i;
        AnonymousClass1IS r2 = r5.A0z;
        AbstractC14640lm r0 = r2.A00;
        AnonymousClass009.A05(r0);
        contentValues.put("key_remote_jid", r0.getRawString());
        contentValues.put("key_from_me", Integer.valueOf(r2.A02 ? 1 : 0));
        contentValues.put("key_id", r2.A01);
        contentValues.put("status", Integer.valueOf(r5.A0C));
        int i2 = 0;
        if (r5.A0r) {
            i2 = 2;
        }
        contentValues.put("needs_push", Integer.valueOf(i2));
        A02(contentValues, r5);
        contentValues.put("timestamp", Long.valueOf(r5.A0I));
        A04(contentValues, "media_url", r5.A0P());
        A04(contentValues, "media_mime_type", r5.A0N());
        contentValues.put("media_wa_type", Byte.valueOf(r5.A0y));
        contentValues.put("media_size", Long.valueOf(r5.A09()));
        A04(contentValues, "media_name", r5.A0O());
        A04(contentValues, "media_caption", r5.A0K());
        A04(contentValues, "media_hash", r5.A0M());
        contentValues.put("media_duration", Integer.valueOf(r5.A04()));
        contentValues.put("origin", Integer.valueOf(r5.A08));
        boolean z = r5 instanceof AnonymousClass1XP;
        if (!z) {
            d = 0.0d;
        } else {
            d = ((AnonymousClass1XP) r5).A00;
        }
        contentValues.put("latitude", Double.valueOf(d));
        if (!z) {
            d2 = 0.0d;
        } else {
            d2 = ((AnonymousClass1XP) r5).A01;
        }
        contentValues.put("longitude", Double.valueOf(d2));
        A03(contentValues, A00(r4, r5.A0H()));
        A04(contentValues, "remote_resource", r5.A0J());
        contentValues.put("received_timestamp", Long.valueOf(r5.A0G));
        contentValues.put("send_timestamp", (Integer) -1);
        contentValues.put("receipt_server_timestamp", (Integer) -1);
        contentValues.put("receipt_device_timestamp", (Integer) -1);
        A04(contentValues, "participant_hash", r5.A0l);
        contentValues.put("recipient_count", Integer.valueOf(r5.A0A));
        contentValues.put("quoted_row_id", Long.valueOf(r5.A0F));
        A04(contentValues, "mentioned_jids", AnonymousClass1Y6.A00(r5.A0o));
        if (!(r5 instanceof AbstractC16130oV)) {
            str = null;
        } else {
            str = ((AbstractC16130oV) r5).A09;
        }
        A04(contentValues, "multicast_id", str);
        contentValues.put("edit_version", Integer.valueOf(r5.A01));
        A04(contentValues, "media_enc_hash", r5.A0L());
        A04(contentValues, "payment_transaction_id", r5.A0m);
        contentValues.put("forwarded", Integer.valueOf(r5.A07()));
        contentValues.put("preview_type", Integer.valueOf(r5.A05()));
        contentValues.put("lookup_tables", Long.valueOf(r5.A0A()));
        if (!(r5 instanceof C30361Xc)) {
            i = 0;
        } else {
            i = ((C30361Xc) r5).A00;
        }
        contentValues.put("future_message_type", Integer.valueOf(i));
        contentValues.put("message_add_on_flags", Integer.valueOf(r5.A07));
    }

    public static void A02(ContentValues contentValues, AbstractC15340mz r10) {
        byte[] bArr;
        Throwable e;
        byte b = r10.A0y;
        if (b == 1 || b == 5 || b == 3 || b == 2 || b == 9 || b == 13 || b == 14 || b == 12 || b == 16 || b == 19 || b == 20 || b == 23 || b == 37 || b == 24 || b == 25 || b == 26 || b == 30 || b == 28 || b == 29 || b == 44 || b == 42 || b == 43 || b == 66) {
            if (r10.A03() != 0 || r10.A0I() == null) {
                bArr = r10.A13();
            } else {
                byte[] bArr2 = null;
                try {
                    bArr = r10.A0I().length() != 0 ? Base64.decode(r10.A0I(), 0) : null;
                    try {
                        r10.A0l(null);
                        r10.A0w(bArr);
                    } catch (ArrayIndexOutOfBoundsException | IllegalArgumentException | StringIndexOutOfBoundsException e2) {
                        e = e2;
                        bArr2 = bArr;
                        StringBuilder sb = new StringBuilder("bindMessageData/base64-decode/message.encoding:");
                        sb.append(r10.A03());
                        Log.e(sb.toString());
                        if (r10.A03() == 0 && r10.A0I() != null) {
                            StringBuilder sb2 = new StringBuilder("bindMessageData/base64-decode/message.data:");
                            sb2.append(r10.A0I().substring(0, Math.min(100, r10.A0I().length())));
                            Log.e(sb2.toString());
                        }
                        Log.e("bindMessageData/base64-decode/error", e);
                        bArr = bArr2;
                        A06(contentValues, "raw_data", bArr);
                        contentValues.putNull("data");
                        return;
                    }
                } catch (ArrayIndexOutOfBoundsException | IllegalArgumentException | StringIndexOutOfBoundsException e3) {
                    e = e3;
                }
            }
            A06(contentValues, "raw_data", bArr);
            contentValues.putNull("data");
            return;
        }
        A04(contentValues, "data", r10.A0I());
        contentValues.putNull("raw_data");
    }

    public static void A03(ContentValues contentValues, Object obj) {
        if (obj == null) {
            contentValues.putNull("thumb_image");
            return;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        new ObjectOutputStream(byteArrayOutputStream).writeObject(obj);
        contentValues.put("thumb_image", byteArrayOutputStream.toByteArray());
    }

    public static void A04(ContentValues contentValues, String str, String str2) {
        if (str2 == null) {
            contentValues.putNull(str);
        } else {
            contentValues.put(str, str2);
        }
    }

    public static void A05(ContentValues contentValues, String str, boolean z) {
        contentValues.put(str, Long.valueOf(z ? 1 : 0));
    }

    public static void A06(ContentValues contentValues, String str, byte[] bArr) {
        if (bArr == null) {
            contentValues.putNull(str);
        } else {
            contentValues.put(str, bArr);
        }
    }
}
