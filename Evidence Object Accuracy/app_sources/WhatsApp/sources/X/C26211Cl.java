package X;

import com.facebook.redex.RunnableBRunnable0Shape1S0200000_I0_1;
import java.util.List;

/* renamed from: X.1Cl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26211Cl {
    public final AbstractC15710nm A00;
    public final C251118d A01;
    public final C21340xG A02;
    public final C26201Ck A03;
    public final C14830m7 A04;
    public final AbstractC14440lR A05;

    public C26211Cl(AbstractC15710nm r1, C251118d r2, C21340xG r3, C26201Ck r4, C14830m7 r5, AbstractC14440lR r6) {
        this.A04 = r5;
        this.A01 = r2;
        this.A05 = r6;
        this.A00 = r1;
        this.A02 = r3;
        this.A03 = r4;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0013, code lost:
        if (r1.A07(1273) == false) goto L_0x0015;
     */
    /* JADX WARNING: Removed duplicated region for block: B:130:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x01d7  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x01b1 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x0036 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List A00() {
        /*
        // Method dump skipped, instructions count: 514
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C26211Cl.A00():java.util.List");
    }

    public void A01(List list) {
        C14850m9 r1 = this.A01.A00;
        if (r1.A07(450) && r1.A07(1273)) {
            this.A05.Ab2(new RunnableBRunnable0Shape1S0200000_I0_1(this, 25, list));
        }
    }
}
