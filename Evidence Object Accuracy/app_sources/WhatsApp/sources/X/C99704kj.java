package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4kj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C99704kj implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new C30221Wo(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C30221Wo[i];
    }
}
