package X;

/* renamed from: X.4UE  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4UE {
    public final int A00;
    public final AnonymousClass1JA A01;
    public final AnonymousClass1VC A02 = new AnonymousClass1VC();
    public final String A03;
    public final String A04;
    public final boolean A05;
    public final boolean A06;
    public final boolean A07;
    public final boolean A08;
    public final boolean A09;
    public final boolean A0A;
    public final boolean A0B;
    public final boolean A0C;
    public final boolean A0D;

    public AnonymousClass4UE(AnonymousClass1JA r2, String str, String str2, int i, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, boolean z9) {
        this.A01 = r2;
        this.A07 = z;
        this.A0C = z2;
        this.A0B = z3;
        this.A06 = z4;
        this.A0D = z5;
        this.A08 = z6;
        this.A09 = z7;
        this.A0A = z8;
        this.A00 = i;
        this.A05 = z9;
        this.A03 = str;
        this.A04 = str2;
    }
}
