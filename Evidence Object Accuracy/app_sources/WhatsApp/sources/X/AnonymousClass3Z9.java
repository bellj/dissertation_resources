package X;

import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.Jid;
import java.util.HashSet;
import java.util.List;

/* renamed from: X.3Z9  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3Z9 implements AbstractC21730xt {
    public final AbstractC15710nm A00;
    public final AbstractC116765Ws A01;

    public /* synthetic */ AnonymousClass3Z9(AbstractC15710nm r1, AbstractC116765Ws r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        this.A01.AXX();
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r3, String str) {
        this.A01.APl(C41151sz.A00(r3));
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r8, String str) {
        AnonymousClass1V8 A0E;
        AbstractC15710nm r6 = this.A00;
        if (!(r8.A0A(r6, C15580nU.class, "from") == null || (A0E = r8.A0E("unlink")) == null)) {
            List<AnonymousClass1V8> A0J = A0E.A0J("group");
            if (A0J.size() != 0) {
                HashSet A12 = C12970iu.A12();
                for (AnonymousClass1V8 r2 : A0J) {
                    Jid A0A = r2.A0A(r6, GroupJid.class, "jid");
                    if (A0A != null) {
                        A12.add(C12960it.A0D(A0A, r2.A05("error", -1)));
                    }
                }
                this.A01.AY0(A12);
                return;
            }
        }
        this.A01.APl(-1);
    }
}
