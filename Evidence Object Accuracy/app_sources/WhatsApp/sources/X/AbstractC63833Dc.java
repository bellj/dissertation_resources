package X;

import java.security.PublicKey;
import java.security.cert.X509Certificate;

/* renamed from: X.3Dc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC63833Dc {
    public final AbstractC116695Wl A00;

    public AbstractC63833Dc(AbstractC116695Wl r1) {
        this.A00 = r1;
    }

    public void A00(Integer num, PublicKey publicKey, X509Certificate x509Certificate) {
        if (this instanceof AnonymousClass304) {
            AnonymousClass304 r3 = (AnonymousClass304) this;
            Object obj = ((AnonymousClass17C) r3.A02.A01.get()).A00.get(r3.A00.A01);
            AnonymousClass009.A05(obj);
            ((AnonymousClass01N) obj).get();
            if (r3.A03 == null) {
                new AnonymousClass4VZ();
            }
            throw C12970iu.A0z();
        } else if (this instanceof AnonymousClass303) {
            AnonymousClass303 r32 = (AnonymousClass303) this;
            C19550uI r1 = r32.A02;
            C64063Ec r5 = r32.A00;
            C16820po r2 = r5.A01;
            Object obj2 = ((AnonymousClass17C) r1.A01.get()).A00.get(r2);
            AnonymousClass009.A05(obj2);
            AnonymousClass17B r4 = (AnonymousClass17B) ((AnonymousClass01N) obj2).get();
            AnonymousClass3EB r6 = new AnonymousClass3EB(r32.A01, r2, r1.A00);
            AnonymousClass4VZ r7 = r32.A03;
            if (r7 == null) {
                r7 = new AnonymousClass4VZ();
            }
            r4.AZG(r5, r6, r7, num, publicKey, x509Certificate);
        } else if (!(this instanceof AnonymousClass302)) {
            AnonymousClass301 r33 = (AnonymousClass301) this;
            C19550uI r12 = r33.A01;
            C16820po r22 = r33.A02;
            Object obj3 = ((AnonymousClass17C) r12.A01.get()).A00.get(r22);
            AnonymousClass009.A05(obj3);
            ((AnonymousClass17B) ((AnonymousClass01N) obj3).get()).AZD(new AnonymousClass3EB(r33.A00, r22, r12.A00), new AnonymousClass4VZ(), num, publicKey, x509Certificate);
        } else {
            AnonymousClass302 r34 = (AnonymousClass302) this;
            C19550uI r13 = r34.A01;
            C16820po r23 = r34.A02;
            Object obj4 = ((AnonymousClass17C) r13.A01.get()).A00.get(r23);
            AnonymousClass009.A05(obj4);
            ((AnonymousClass17B) ((AnonymousClass01N) obj4).get()).AZF(r13.A00(r23), new AnonymousClass3EB(r34.A00, r23, r13.A00), new AnonymousClass4VZ(), num, publicKey, x509Certificate);
        }
    }
}
