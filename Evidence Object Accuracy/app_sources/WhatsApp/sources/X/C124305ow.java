package X;

import android.text.TextUtils;
import java.util.Collections;
import java.util.List;

/* renamed from: X.5ow  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124305ow extends AbstractC16350or {
    public final AnonymousClass1IS A00;
    public final String A01;
    public final boolean A02;
    public final /* synthetic */ C118185bP A03;

    public C124305ow(C118185bP r1, AnonymousClass1IS r2, String str, boolean z) {
        this.A03 = r1;
        this.A00 = r2;
        this.A01 = str;
        this.A02 = z;
    }

    @Override // X.AbstractC16350or
    public void A02() {
        if (this.A02) {
            this.A03.A0O(false);
        }
        this.A03.A05 = null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x005e, code lost:
        if (r4 == null) goto L_0x0060;
     */
    @Override // X.AbstractC16350or
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ java.lang.Object A05(java.lang.Object[] r31) {
        /*
        // Method dump skipped, instructions count: 418
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C124305ow.A05(java.lang.Object[]):java.lang.Object");
    }

    @Override // X.AbstractC16350or
    public void A06() {
        if (this.A02) {
            this.A03.A0O(true);
        }
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        C127915vG r0;
        int i;
        C127915vG r6 = (C127915vG) obj;
        C118185bP r3 = this.A03;
        C30931Zj r4 = r3.A0d;
        StringBuilder A0k = C12960it.A0k("onTransactionDetailData loaded: ");
        A0k.append(C12960it.A1W(r6));
        C117295Zj.A1F(r4, A0k);
        if (r6.A01 == null) {
            r4.A06("onTransactionDetailData transactionInfo is null");
            r3.A08.A0B(C128315vu.A00(18));
        } else {
            if (this.A02) {
                r3.A0O(false);
            }
            if ("native".equals(r3.A0A)) {
                r3.A0h.Ab2(new Runnable(r6) { // from class: X.6It
                    public final /* synthetic */ C127915vG A01;

                    {
                        this.A01 = r2;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        C124305ow.this.A03.A0W.A04(Collections.singletonList(this.A01.A01.A0K));
                    }
                });
                if (r6.A02 != null) {
                    r3.A08.A0B(C128315vu.A00(3));
                }
            }
            if (r3 instanceof C123565nM) {
                C123565nM r2 = (C123565nM) r3;
                ((C118185bP) r2).A06 = r6;
                C129185xJ r42 = r2.A0D;
                AnonymousClass1IR r1 = r6.A01;
                AbstractC130195yx A00 = r42.A00(r1.A03);
                AnonymousClass009.A05(A00);
                A00.A06(r1);
                if (A00 instanceof C123765np) {
                    C123765np r43 = (C123765np) A00;
                    AbstractC121025h8 r02 = r43.A01;
                    AnonymousClass009.A05(r02);
                    if (r02.A02 == 1 && ((i = ((AbstractC130195yx) r43).A01.A02) == 608 || i == 602)) {
                        r2.A0T();
                        r0 = ((C118185bP) r2).A06;
                        if (r0 != null && r0.A01.A0E() && r2.A04) {
                            r2.A04 = false;
                            ((C118185bP) r2).A08.A0B(new C123525nI(502));
                        }
                    }
                }
                r2.A09();
                r0 = ((C118185bP) r2).A06;
                if (r0 != null) {
                    r2.A04 = false;
                    ((C118185bP) r2).A08.A0B(new C123525nI(502));
                }
            } else if (!(r3 instanceof C123575nN)) {
                r3.A06 = r6;
                if ("native".equals(r3.A0A)) {
                    r3.A08();
                }
                r3.A09();
            } else {
                C123575nN r44 = (C123575nN) r3;
                ((C118185bP) r44).A06 = r6;
                AnonymousClass1IR r22 = r6.A01;
                if (r22.A03 == 1000 || (!r22.A0F() && ((C118185bP) r44).A06.A01.A0J() && !TextUtils.isEmpty(((C118185bP) r44).A06.A01.A0K))) {
                    r44.A0S();
                } else {
                    r44.A08();
                    AnonymousClass016 r23 = ((C118185bP) r44).A02;
                    List list = (List) r23.A01();
                    if (list != null) {
                        list.clear();
                    }
                    C127915vG r03 = ((C118185bP) r44).A06;
                    if (!(r03 == null || r03.A01 == null)) {
                        r44.A0G(list);
                        r23.A0B(list);
                    }
                }
            }
            r3.A0A();
        }
        r3.A05 = null;
    }
}
