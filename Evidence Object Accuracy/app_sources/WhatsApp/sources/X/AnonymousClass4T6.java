package X;

import android.graphics.PointF;
import java.util.Arrays;
import java.util.List;

/* renamed from: X.4T6  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4T6 {
    public float A00;
    public float A01;
    public int A02;
    public PointF A03;
    public List A04;
    public final List A05;

    public AnonymousClass4T6(PointF pointF, AnonymousClass4IZ[] r3, C88934Ia[] r4, float f, float f2, int i) {
        this.A02 = i;
        this.A03 = pointF;
        this.A00 = f;
        this.A01 = f2;
        this.A04 = Arrays.asList(r4);
        this.A05 = Arrays.asList(r3);
    }
}
