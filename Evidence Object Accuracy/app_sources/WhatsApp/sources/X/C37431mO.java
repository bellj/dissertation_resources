package X;

import com.whatsapp.stickers.WebpUtils;
import com.whatsapp.util.Log;

/* renamed from: X.1mO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C37431mO {
    public static final void A00(AnonymousClass1KS r2) {
        AnonymousClass1KB r1 = r2.A04;
        if (r1 != null) {
            String str = r2.A06;
            if (str == null || str.length() == 0) {
                C37471mS[] r0 = r1.A08;
                if (r0 != null) {
                    r2.A06 = AnonymousClass1KS.A00(r0);
                }
            }
            r2.A0G = r1.A05;
            return;
        }
        String str2 = r2.A08;
        if (str2 == null || str2.length() == 0) {
            Log.w("Unable to fill metadata, file path is null or empty.");
            return;
        }
        AnonymousClass009.A05(str2);
        AnonymousClass1KB A00 = AnonymousClass1KB.A00(WebpUtils.fetchWebpMetadata(str2));
        if (A00 != null) {
            r2.A04 = A00;
            r2.A0G = A00.A05;
            C37471mS[] r02 = A00.A08;
            if (r02 != null) {
                r2.A06 = AnonymousClass1KS.A00(r02);
            }
        }
    }
}
