package X;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import com.whatsapp.R;
import com.whatsapp.wabloks.ui.WaBloksActivity;

/* renamed from: X.5pU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124425pU extends AbstractC119405dv {
    public Toolbar A00;
    public AnonymousClass3EV A01;
    public String A02;

    @Override // X.AbstractC119405dv
    public void A04(AbstractC115815Ta r1) {
    }

    public C124425pU(AnonymousClass018 r2, WaBloksActivity waBloksActivity) {
        super(r2, waBloksActivity);
        this.A01 = new AnonymousClass3EV(waBloksActivity, r2);
    }

    @Override // X.AbstractC119405dv
    public void A03(Intent intent, Bundle bundle) {
        if (bundle != null) {
            this.A02 = bundle.getString("bk_navigation_bar_title");
        }
        WaBloksActivity waBloksActivity = this.A04;
        Toolbar toolbar = (Toolbar) AnonymousClass00T.A05(waBloksActivity, R.id.wabloks_screen_toolbar);
        this.A00 = toolbar;
        toolbar.setTitle("");
        Toolbar toolbar2 = this.A00;
        toolbar2.A07();
        waBloksActivity.A1e(toolbar2);
        A00().A0M(true);
        this.A00.setNavigationIcon(this.A01.A00());
        this.A00.setBackgroundColor(waBloksActivity.getResources().getColor(R.color.wabloksui_screen_toolbar));
        this.A00.setNavigationOnClickListener(C117305Zk.A0A(this, 198));
    }

    @Override // X.AbstractC119405dv, X.C28791Pa, android.app.Application.ActivityLifecycleCallbacks
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        bundle.putString("bk_navigation_bar_title", this.A02);
        super.onActivitySaveInstanceState(activity, bundle);
    }
}
