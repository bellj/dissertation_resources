package X;

import android.database.Cursor;
import com.facebook.redex.RunnableBRunnable0Shape0S0301000_I0;
import com.facebook.redex.RunnableBRunnable0Shape4S0200000_I0_4;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.0yv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22340yv {
    public final AbstractC15710nm A00;
    public final C15570nT A01;
    public final C20870wS A02;
    public final C14830m7 A03;
    public final C22320yt A04;
    public final C16370ot A05;
    public final C20650w6 A06;
    public final C19990v2 A07;
    public final C15650ng A08;
    public final C21380xK A09;
    public final C16490p7 A0A;
    public final C238513g A0B;
    public final C22830zi A0C;
    public final C22810zg A0D;
    public final C242414t A0E;
    public final C14840m8 A0F;
    public final C246316g A0G;
    public final C238213d A0H;
    public final C22170ye A0I;
    public final C20660w7 A0J;
    public final C20220vP A0K;
    public final AnonymousClass1E9 A0L;
    public final Map A0M;

    public C22340yv(AbstractC15710nm r2, C15570nT r3, C20870wS r4, C14830m7 r5, C22320yt r6, C16370ot r7, C20650w6 r8, C19990v2 r9, C15650ng r10, C21380xK r11, C21620xi r12, C16490p7 r13, C238513g r14, C22830zi r15, C22810zg r16, C242414t r17, C14840m8 r18, C246316g r19, C238213d r20, C22170ye r21, C20660w7 r22, C20220vP r23, AnonymousClass1E9 r24) {
        this.A03 = r5;
        this.A00 = r2;
        this.A01 = r3;
        this.A07 = r9;
        this.A06 = r8;
        this.A0J = r22;
        this.A0I = r21;
        this.A0L = r24;
        this.A09 = r11;
        this.A02 = r4;
        this.A0D = r16;
        this.A08 = r10;
        this.A0H = r20;
        this.A0F = r18;
        this.A04 = r6;
        this.A05 = r7;
        this.A0G = r19;
        this.A0A = r13;
        this.A0K = r23;
        this.A0B = r14;
        this.A0C = r15;
        this.A0E = r17;
        this.A0M = r12.A02;
        r10.A00 = this;
    }

    public static boolean A00(AnonymousClass1IS r0, int i) {
        if (!r0.A02) {
            return i == 13 || i == 16 || i == 17;
        }
        return false;
    }

    public void A01(AbstractC43081wN r9, AnonymousClass1IS r10, int i) {
        this.A04.A01(new RunnableBRunnable0Shape0S0301000_I0(this, r10, r9, i, 4), 36);
    }

    public void A02(List list) {
        StringBuilder sb;
        this.A04.A00();
        HashMap hashMap = new HashMap();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            AnonymousClass1IS r1 = (AnonymousClass1IS) it.next();
            AbstractC14640lm r2 = r1.A00;
            AnonymousClass009.A05(r2);
            AbstractC15340mz A03 = this.A05.A03(r1);
            if (A03 != null) {
                if (C15380n4.A0N(r2)) {
                    this.A0L.A02(A03);
                } else {
                    List list2 = (List) hashMap.get(r2);
                    if (list2 == null) {
                        list2 = new ArrayList();
                        hashMap.put(r2, list2);
                    }
                    list2.add(A03);
                }
            }
        }
        for (Map.Entry entry : hashMap.entrySet()) {
            AbstractC14640lm r8 = (AbstractC14640lm) entry.getKey();
            List list3 = (List) entry.getValue();
            Collections.sort(list3, new Comparator() { // from class: X.1wM
                @Override // java.util.Comparator
                public final int compare(Object obj, Object obj2) {
                    return (int) (((AbstractC15340mz) obj).A11 - ((AbstractC15340mz) obj2).A11);
                }
            });
            AbstractC15340mz r7 = (AbstractC15340mz) list3.get(list3.size() - 1);
            C20650w6 r6 = this.A06;
            long j = r7.A11;
            long j2 = r7.A12;
            r6.A06.A00();
            AnonymousClass1PE A06 = r6.A09.A06(r8);
            if (A06 == null) {
                sb = new StringBuilder("msgstore/setchatseenonasynccommitthread/nochat/");
                sb.append(r8);
            } else {
                StringBuilder sb2 = new StringBuilder("msgstore/setchatseenonasynccommitthread/");
                sb2.append(r8);
                sb2.append("/");
                sb2.append(A06.A07());
                Log.i(sb2.toString());
                if (A06.A0P >= j2) {
                    sb = new StringBuilder();
                    sb.append("msgstore/setchatseenonasynccommitthread/");
                    sb.append(r8);
                    sb.append("/message already read");
                } else {
                    AnonymousClass134 r13 = r6.A0H;
                    C16310on A01 = r13.A04.get();
                    try {
                        int i = 0;
                        Cursor A09 = A01.A03.A09("SELECT COUNT(*) FROM available_message_view WHERE chat_row_id = ? AND (message_type != '8') AND sort_id > ? AND (message_type != '7') AND from_me = 0", new String[]{String.valueOf(r13.A01.A02(r8)), String.valueOf(j2)});
                        if (A09.moveToNext()) {
                            i = A09.getInt(0);
                        } else {
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append("msgstore/getnewercount/db no message for ");
                            sb3.append(r8);
                            Log.i(sb3.toString());
                        }
                        A09.close();
                        A01.close();
                        int A012 = r13.A01(r8, j2);
                        int A00 = r6.A0D.A00(r8, j2);
                        int i2 = i - A012;
                        if (i2 < A06.A06) {
                            A06.A0C(i2, A012, i, A00);
                            A06.A0O = j;
                            A06.A0P = j2;
                            if (r6.A0L.A01(r8)) {
                                A06.A0Q = j;
                                A06.A0R = j2;
                            }
                            r6.A08.A0B(A06);
                            r6.A0C.A02.post(new RunnableBRunnable0Shape4S0200000_I0_4(r6, 1, r8));
                        }
                        C20220vP r0 = this.A0K;
                        r0.A09(r8, r7);
                        r0.A08(r8);
                    } catch (Throwable th) {
                        try {
                            A01.close();
                        } catch (Throwable unused) {
                        }
                        throw th;
                    }
                }
            }
            Log.i(sb.toString());
            C20220vP r0 = this.A0K;
            r0.A09(r8, r7);
            r0.A08(r8);
        }
        if (!hashMap.isEmpty()) {
            this.A0K.A07();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x0284, code lost:
        if (r32 != false) goto L_0x0286;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x009a, code lost:
        if (r1.A04(r0) < r40.A11) goto L_0x009c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x010b, code lost:
        if (r32 == false) goto L_0x0225;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x01f0, code lost:
        if (r8 == 13) goto L_0x01f2;
     */
    /* JADX WARNING: Removed duplicated region for block: B:161:0x04ad A[Catch: all -> 0x05ab, TryCatch #1 {all -> 0x05b0, blocks: (B:88:0x022b, B:97:0x027a, B:189:0x057f, B:89:0x022f, B:91:0x0233, B:93:0x023d, B:96:0x0277, B:102:0x0288, B:104:0x02a5, B:106:0x02b9, B:110:0x02c7, B:112:0x02d3, B:114:0x02d7, B:115:0x02db, B:118:0x02e5, B:119:0x02ea, B:121:0x02f1, B:127:0x032a, B:128:0x0331, B:129:0x0352, B:130:0x0353, B:131:0x035a, B:132:0x0360, B:133:0x037e, B:142:0x0413, B:145:0x0420, B:146:0x0421, B:148:0x042f, B:149:0x0439, B:150:0x0447, B:153:0x047b, B:158:0x048e, B:159:0x04a4, B:161:0x04ad, B:167:0x04c0, B:168:0x04c2, B:169:0x04c4, B:176:0x04d2, B:178:0x04d6, B:180:0x04dc, B:181:0x04f3, B:183:0x04f9, B:185:0x050d, B:186:0x0563, B:187:0x0566, B:188:0x057c), top: B:204:0x022b }] */
    /* JADX WARNING: Removed duplicated region for block: B:171:0x04c9  */
    /* JADX WARNING: Removed duplicated region for block: B:172:0x04cc  */
    /* JADX WARNING: Removed duplicated region for block: B:178:0x04d6 A[Catch: all -> 0x05ab, TryCatch #1 {all -> 0x05b0, blocks: (B:88:0x022b, B:97:0x027a, B:189:0x057f, B:89:0x022f, B:91:0x0233, B:93:0x023d, B:96:0x0277, B:102:0x0288, B:104:0x02a5, B:106:0x02b9, B:110:0x02c7, B:112:0x02d3, B:114:0x02d7, B:115:0x02db, B:118:0x02e5, B:119:0x02ea, B:121:0x02f1, B:127:0x032a, B:128:0x0331, B:129:0x0352, B:130:0x0353, B:131:0x035a, B:132:0x0360, B:133:0x037e, B:142:0x0413, B:145:0x0420, B:146:0x0421, B:148:0x042f, B:149:0x0439, B:150:0x0447, B:153:0x047b, B:158:0x048e, B:159:0x04a4, B:161:0x04ad, B:167:0x04c0, B:168:0x04c2, B:169:0x04c4, B:176:0x04d2, B:178:0x04d6, B:180:0x04dc, B:181:0x04f3, B:183:0x04f9, B:185:0x050d, B:186:0x0563, B:187:0x0566, B:188:0x057c), top: B:204:0x022b }] */
    /* JADX WARNING: Removed duplicated region for block: B:184:0x050b  */
    /* JADX WARNING: Removed duplicated region for block: B:192:0x0587  */
    /* JADX WARNING: Removed duplicated region for block: B:198:0x05a2  */
    /* JADX WARNING: Removed duplicated region for block: B:214:0x04c7 A[EDGE_INSN: B:214:0x04c7->B:170:0x04c7 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x011d  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x0233 A[Catch: all -> 0x05ab, TryCatch #1 {all -> 0x05b0, blocks: (B:88:0x022b, B:97:0x027a, B:189:0x057f, B:89:0x022f, B:91:0x0233, B:93:0x023d, B:96:0x0277, B:102:0x0288, B:104:0x02a5, B:106:0x02b9, B:110:0x02c7, B:112:0x02d3, B:114:0x02d7, B:115:0x02db, B:118:0x02e5, B:119:0x02ea, B:121:0x02f1, B:127:0x032a, B:128:0x0331, B:129:0x0352, B:130:0x0353, B:131:0x035a, B:132:0x0360, B:133:0x037e, B:142:0x0413, B:145:0x0420, B:146:0x0421, B:148:0x042f, B:149:0x0439, B:150:0x0447, B:153:0x047b, B:158:0x048e, B:159:0x04a4, B:161:0x04ad, B:167:0x04c0, B:168:0x04c2, B:169:0x04c4, B:176:0x04d2, B:178:0x04d6, B:180:0x04dc, B:181:0x04f3, B:183:0x04f9, B:185:0x050d, B:186:0x0563, B:187:0x0566, B:188:0x057c), top: B:204:0x022b }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A03(X.AbstractC43081wN r38, com.whatsapp.jid.DeviceJid r39, X.AbstractC15340mz r40, int r41, long r42) {
        /*
        // Method dump skipped, instructions count: 1461
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22340yv.A03(X.1wN, com.whatsapp.jid.DeviceJid, X.0mz, int, long):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0050, code lost:
        if (r8 == 8) goto L_0x0052;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A04(X.AbstractC43081wN r6, X.AnonymousClass1IS r7, int r8) {
        /*
            r5 = this;
            X.0yt r0 = r5.A04
            r0.A00()
            X.0ot r0 = r5.A05
            X.0mz r2 = r0.A03(r7)
            r4 = 0
            if (r2 != 0) goto L_0x0020
            java.lang.String r0 = "msgstore/update/nosuchmessage: "
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>(r0)
            r3.append(r7)
        L_0x0018:
            java.lang.String r0 = r3.toString()
            com.whatsapp.util.Log.w(r0)
            return r4
        L_0x0020:
            int r0 = r2.A0C
            int r0 = X.C37381mH.A00(r0, r8)
            if (r0 < 0) goto L_0x0045
            java.lang.String r0 = "msgstore/update/statusdowngrade: "
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>(r0)
            r3.append(r7)
            java.lang.String r0 = " current:"
            r3.append(r0)
            int r0 = r2.A0C
            r3.append(r0)
            java.lang.String r0 = " new:"
            r3.append(r0)
            r3.append(r8)
            goto L_0x0018
        L_0x0045:
            r0 = 9
            if (r8 == r0) goto L_0x0052
            r0 = 10
            if (r8 == r0) goto L_0x0052
            r0 = 8
            r3 = 0
            if (r8 != r0) goto L_0x0079
        L_0x0052:
            r3 = 1
            byte r1 = r2.A0y
            r0 = 2
            if (r1 != r0) goto L_0x005c
            int r0 = r2.A08
            if (r0 == r3) goto L_0x0079
        L_0x005c:
            boolean r0 = X.C30041Vv.A0I(r1)
            if (r0 != 0) goto L_0x0079
            java.lang.String r0 = "msgstore/update/status-played-non-ptt or view-once: "
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>(r0)
            r3.append(r7)
            java.lang.String r0 = " type="
            r3.append(r0)
            java.lang.String r0 = X.C30041Vv.A0A(r1)
            r3.append(r0)
            goto L_0x0018
        L_0x0079:
            r2.A0Y(r8)
            if (r3 == 0) goto L_0x0083
            X.14t r0 = r5.A0E
            r0.A02(r2)
        L_0x0083:
            long r3 = r2.A0I
            r0 = 4
            if (r8 != r0) goto L_0x009e
            long r0 = java.lang.System.currentTimeMillis()
            long r0 = r0 - r3
            java.lang.String r4 = "msgstore/update/receipt/server/delay "
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>(r4)
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            com.whatsapp.util.Log.i(r0)
        L_0x009e:
            r0 = 11
            r1 = -1
            if (r8 == r0) goto L_0x00b4
            r0 = 12
            if (r8 == r0) goto L_0x00b4
            r0 = 16
            if (r8 == r0) goto L_0x00b4
            r0 = 17
            if (r8 == r0) goto L_0x00b4
            X.0xK r0 = r5.A09
            r0.A00(r2, r1)
        L_0x00b4:
            X.0ng r0 = r5.A08
            boolean r0 = r0.A0t(r2, r1)
            if (r0 == 0) goto L_0x00c1
            if (r6 == 0) goto L_0x00c1
            r6.Ab1(r2)
        L_0x00c1:
            r0 = 1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22340yv.A04(X.1wN, X.1IS, int):boolean");
    }
}
