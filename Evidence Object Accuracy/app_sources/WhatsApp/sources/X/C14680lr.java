package X;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageButton;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.conversation.waveforms.VoiceVisualizer;
import com.whatsapp.voicerecorder.VoiceNoteSeekBar;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0lr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14680lr {
    public final Context A00;
    public final View A01;
    public final ViewGroup A02;
    public final ViewGroup A03;
    public final ViewGroup A04;
    public final ViewGroup A05;
    public final ViewGroup A06;
    public final ImageButton A07;
    public final ImageButton A08;
    public final ImageButton A09;
    public final TextView A0A;
    public final C14900mE A0B;
    public final VoiceVisualizer A0C;
    public final VoiceVisualizer A0D;
    public final AnonymousClass018 A0E;
    public final AbstractC14440lR A0F;
    public final VoiceNoteSeekBar A0G;
    public final List A0H = new ArrayList();
    public final boolean A0I;

    public C14680lr(Context context, View view, C14900mE r18, AnonymousClass018 r19, C14850m9 r20, AbstractC14440lR r21) {
        boolean A07 = r20.A07(1139);
        View A0D = AnonymousClass028.A0D(view, R.id.draft_send_v2);
        TextView textView = (TextView) AnonymousClass028.A0D(view, R.id.voice_note_draft_time_v2);
        this.A00 = context;
        this.A0B = r18;
        this.A0F = r21;
        this.A0E = r19;
        this.A0I = A07;
        this.A09 = (ImageButton) AnonymousClass028.A0D(view, R.id.voice_note_draft_stop_btn_v2);
        this.A07 = (ImageButton) AnonymousClass028.A0D(view, R.id.voice_note_cancel_btn_v2);
        this.A08 = (ImageButton) AnonymousClass028.A0D(view, R.id.voice_note_draft_playback_btn_v2);
        this.A05 = (ViewGroup) AnonymousClass028.A0D(view, R.id.voice_note_draft_layout_v2);
        this.A04 = (ViewGroup) AnonymousClass028.A0D(view, R.id.voice_note_draft_v2);
        this.A06 = (ViewGroup) AnonymousClass028.A0D(view, R.id.voice_note_draft_preview_v2);
        this.A03 = (ViewGroup) AnonymousClass028.A0D(view, R.id.quoted_message_preview_container_v2);
        this.A02 = (ViewGroup) AnonymousClass028.A0D(view, R.id.draft_send_container_v2);
        this.A01 = A0D;
        this.A0A = textView;
        this.A0G = (VoiceNoteSeekBar) AnonymousClass028.A0D(view, R.id.voice_note_draft_seekbar_v2);
        this.A0D = (VoiceVisualizer) AnonymousClass028.A0D(view, R.id.voice_note_draft_preview_audio_visualizer);
        this.A0C = (VoiceVisualizer) AnonymousClass028.A0D(view, R.id.voice_note_draft_audio_visualizer);
        AnonymousClass028.A0a(textView, 2);
    }

    public void A00() {
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        AlphaAnimation alphaAnimation2 = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(250);
        alphaAnimation.setAnimationListener(new C83463xH(this));
        this.A09.startAnimation(alphaAnimation);
        AlphaAnimation alphaAnimation3 = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation3.setDuration(250);
        alphaAnimation3.setAnimationListener(new C83453xG(this));
        this.A07.startAnimation(alphaAnimation3);
        alphaAnimation2.setDuration(250);
        alphaAnimation2.setAnimationListener(new C83473xI(this));
        this.A02.startAnimation(alphaAnimation2);
    }

    public void A01(int i) {
        ImageButton imageButton = this.A09;
        AnonymousClass018 r3 = this.A0E;
        Context context = this.A00;
        imageButton.setImageDrawable(new AnonymousClass2GF(AnonymousClass00T.A04(context, i), r3));
        int i2 = R.string.voice_note_draft_pause_label;
        if (R.drawable.ic_resume_draft_preview == i) {
            i2 = R.string.voice_note_draft_resume_label;
        }
        imageButton.setContentDescription(context.getString(i2));
    }

    public final void A02(AbstractC28651Ol r4, List list) {
        int i;
        if (list.isEmpty()) {
            if (r4 != null) {
                i = r4.A03();
            } else {
                i = 0;
            }
            VoiceNoteSeekBar voiceNoteSeekBar = this.A0G;
            voiceNoteSeekBar.setMax(i);
            voiceNoteSeekBar.setProgress(0);
            if (this.A0I) {
                voiceNoteSeekBar.setVisibility(0);
            }
            this.A0D.setVisibility(8);
            return;
        }
        VoiceVisualizer voiceVisualizer = this.A0D;
        voiceVisualizer.setPlaybackPercentage(0.0f);
        voiceVisualizer.A02(list, 0.0f);
        voiceVisualizer.setVisibility(0);
        this.A0G.setVisibility(8);
    }

    public final void A03(boolean z, boolean z2) {
        if (!z) {
            this.A04.setVisibility(4);
            if (!z2) {
                this.A09.setVisibility(8);
            }
            this.A06.setVisibility(0);
            return;
        }
        Animation A00 = C88154Em.A00(false);
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        AlphaAnimation alphaAnimation2 = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(150);
        alphaAnimation.setAnimationListener(new C83483xJ(this));
        this.A04.startAnimation(alphaAnimation);
        Animation A002 = C88154Em.A00(true);
        A00.setAnimationListener(new C83533xO(A002, this, true));
        A002.setAnimationListener(new C83333x4(this));
        this.A09.startAnimation(A00);
        alphaAnimation2.setDuration(250);
        alphaAnimation2.setAnimationListener(new C83493xK(this));
        this.A06.startAnimation(alphaAnimation2);
    }
}
