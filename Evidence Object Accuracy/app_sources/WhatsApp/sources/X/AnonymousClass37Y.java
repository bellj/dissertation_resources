package X;

import android.content.Context;
import java.lang.ref.WeakReference;

/* renamed from: X.37Y  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass37Y extends AbstractC16350or {
    public final AnonymousClass4KU A00;
    public final AbstractC14640lm A01;
    public final AbstractC15850o0 A02;
    public final WeakReference A03;

    public AnonymousClass37Y(Context context, AnonymousClass4KU r3, AbstractC14640lm r4, AbstractC15850o0 r5) {
        this.A03 = C12970iu.A10(context);
        this.A01 = r4;
        this.A02 = r5;
        this.A00 = r3;
    }
}
