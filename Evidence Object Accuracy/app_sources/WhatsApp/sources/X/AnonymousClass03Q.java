package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.SystemClock;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.03Q  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass03Q {
    public static float A00 = 1.0f;
    public static long A01;
    public static Context A02;
    public static final float A03 = (((float) Math.sqrt(3.0d)) / 2.0f);
    public static final HashMap A04 = new HashMap(16);

    public static C04640Mm A00() {
        StringBuilder sb = new StringBuilder("hue_");
        sb.append(240.0f);
        return A01(new C08570bQ(), sb.toString());
    }

    public static C04640Mm A01(AbstractC12210hY r9, String str) {
        C04640Mm r5;
        HashMap hashMap = A04;
        Reference reference = (Reference) hashMap.get(str);
        if (reference == null || (r5 = (C04640Mm) reference.get()) == null) {
            Bitmap A89 = r9.A89();
            if (A89 == null) {
                return null;
            }
            r5 = new C04640Mm(A89);
            hashMap.put(str, new WeakReference(r5));
        }
        long uptimeMillis = SystemClock.uptimeMillis();
        long j = A01;
        if (j >= 600000 || j == 0) {
            A01 = uptimeMillis;
            Iterator it = hashMap.entrySet().iterator();
            while (it.hasNext()) {
                if (((Reference) ((Map.Entry) it.next()).getValue()).get() == null) {
                    it.remove();
                }
            }
        }
        return r5;
    }

    public static void A02(Canvas canvas, Paint paint, float f, float f2, float f3) {
        Path path = new Path();
        float f4 = 2.0f * f3;
        float f5 = f2 - f4;
        path.moveTo(f, f5);
        path.arcTo(new RectF(f - f3, f5 - f3, f + f3, f5 + f3), 30.0f, -240.0f, true);
        path.lineTo(f, f4 + f5);
        path.lineTo(f + (A03 * f3), f5 + (f3 * 0.5f));
        path.close();
        canvas.drawPath(path, paint);
    }
}
