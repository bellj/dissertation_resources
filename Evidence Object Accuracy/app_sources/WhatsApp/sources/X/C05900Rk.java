package X;

import android.graphics.PointF;

/* renamed from: X.0Rk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05900Rk {
    public final PointF A00;
    public final PointF A01;
    public final PointF A02;

    public C05900Rk() {
        this.A00 = new PointF();
        this.A01 = new PointF();
        this.A02 = new PointF();
    }

    public C05900Rk(PointF pointF, PointF pointF2, PointF pointF3) {
        this.A00 = pointF;
        this.A01 = pointF2;
        this.A02 = pointF3;
    }
}
