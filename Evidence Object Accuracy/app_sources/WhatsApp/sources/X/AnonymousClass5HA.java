package X;

import android.os.Process;

/* renamed from: X.5HA  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5HA extends Thread {
    public AnonymousClass5HA(Runnable runnable, String str) {
        super(runnable, str);
    }

    @Override // java.lang.Thread, java.lang.Runnable
    public final void run() {
        Process.setThreadPriority(10);
        super.run();
    }
}
