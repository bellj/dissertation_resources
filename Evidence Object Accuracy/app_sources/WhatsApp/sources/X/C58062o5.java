package X;

import android.view.View;
import android.view.animation.Animation;

/* renamed from: X.2o5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58062o5 extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ View A00;
    public final /* synthetic */ AnonymousClass2Zf A01;

    public C58062o5(View view, AnonymousClass2Zf r2) {
        this.A00 = view;
        this.A01 = r2;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        View view = this.A00;
        view.getLayoutParams().width = -2;
        view.requestLayout();
        C12980iv.A1H(view.getViewTreeObserver(), this, 4);
        view.clearAnimation();
    }
}
