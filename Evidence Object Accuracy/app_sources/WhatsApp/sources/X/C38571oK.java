package X;

/* renamed from: X.1oK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C38571oK extends AbstractC16110oT {
    public Integer A00;
    public Long A01;
    public String A02;

    public C38571oK() {
        super(2740, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(2, this.A01);
        r3.Abe(3, this.A02);
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        StringBuilder sb = new StringBuilder("WamStickerCommonQueryToStaticServer {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "httpResponseCode", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "params", this.A02);
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "queryType", obj);
        sb.append("}");
        return sb.toString();
    }
}
