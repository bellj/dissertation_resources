package X;

/* renamed from: X.23i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C458323i extends AbstractC16110oT {
    public Long A00;
    public Long A01;
    public String A02;

    public C458323i() {
        super(1172, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(5, this.A02);
        r3.Abe(2, this.A00);
        r3.Abe(1, this.A01);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("WamStatusTabOpen {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "psaCampaigns", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "statusAvailableUpdatesCount", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "statusSessionId", this.A01);
        sb.append("}");
        return sb.toString();
    }
}
