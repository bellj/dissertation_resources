package X;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.1HI  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1HI extends AnonymousClass1HG {
    public final Map A00 = new HashMap();
    public final AtomicBoolean A01 = new AtomicBoolean();

    public AnonymousClass1HI(AnonymousClass1HE r2, Executor executor) {
        super(r2, executor, 4);
    }
}
