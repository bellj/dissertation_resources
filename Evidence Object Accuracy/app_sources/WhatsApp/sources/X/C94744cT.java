package X;

import java.util.Collections;
import java.util.Map;

/* renamed from: X.4cT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C94744cT {
    public static final C94744cT A01 = new C94744cT(true);
    public final Map A00;

    public C94744cT(boolean z) {
        this.A00 = Collections.emptyMap();
    }

    static {
        try {
            Class.forName("com.google.protobuf.Extension");
        } catch (ClassNotFoundException unused) {
        }
    }

    public C94744cT() {
        this.A00 = C12970iu.A11();
    }
}
