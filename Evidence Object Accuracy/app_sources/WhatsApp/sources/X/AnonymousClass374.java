package X;

import java.lang.ref.WeakReference;

/* renamed from: X.374  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass374 extends AbstractC16350or {
    public final AbstractC37361mF A00;
    public final WeakReference A01;

    public AnonymousClass374(ActivityC13810kN r2, AbstractC37361mF r3) {
        super(r2);
        this.A01 = C12970iu.A10(r2);
        this.A00 = r3;
    }
}
