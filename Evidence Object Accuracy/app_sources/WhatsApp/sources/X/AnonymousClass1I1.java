package X;

/* renamed from: X.1I1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1I1 {
    public final long A00;
    public final boolean A01;
    public final boolean A02;
    public final boolean A03;
    public final boolean A04;

    public AnonymousClass1I1(long j, boolean z, boolean z2, boolean z3, boolean z4) {
        this.A02 = z2;
        this.A04 = z3;
        this.A01 = z;
        this.A03 = z4;
        this.A00 = j;
    }

    public static AnonymousClass1I1 A00(AnonymousClass1I0 r6, long j) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        if (r6 != null) {
            z = r6.A03;
            z2 = r6.A05;
            z3 = r6.A06;
            z4 = r6.A04;
        } else {
            z = false;
            z2 = false;
            z3 = false;
            z4 = false;
        }
        return new AnonymousClass1I1(j, z, z2, z3, z4);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Connectivity{connected=");
        sb.append(this.A01);
        sb.append(", roaming=");
        sb.append(this.A02);
        sb.append(", typeWifi=");
        sb.append(this.A04);
        sb.append(", typeMobile=");
        sb.append(this.A03);
        sb.append(", ntpEventTimeMillis=");
        sb.append(this.A00);
        sb.append('}');
        return sb.toString();
    }
}
