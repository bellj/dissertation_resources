package X;

import android.os.Message;

/* renamed from: X.2Ds  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Ds {
    public static Message A00(AbstractC14640lm r3, AbstractC14640lm r4, C15930o9 r5, String str, String str2, int i) {
        Message obtain = Message.obtain(null, 0, 188, 0, r5);
        obtain.getData().putString("id", str);
        obtain.getData().putParcelable("jid", r3);
        obtain.getData().putParcelable("contextJid", r4);
        obtain.getData().putString("msgId", str2);
        obtain.getData().putInt("retryCount", i);
        return obtain;
    }

    public static Message A01(String str, String str2, String str3, int i) {
        Message obtain = Message.obtain(null, 0, 27, 0);
        obtain.getData().putString("lg", str);
        obtain.getData().putString("lc", str2);
        obtain.getData().putString("userFeedback", str3);
        obtain.getData().putInt("deleteReason", i);
        return obtain;
    }
}
