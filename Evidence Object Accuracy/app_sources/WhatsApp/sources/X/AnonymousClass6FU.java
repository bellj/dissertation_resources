package X;

import android.text.TextUtils;

/* renamed from: X.6FU  reason: invalid class name */
/* loaded from: classes4.dex */
public final /* synthetic */ class AnonymousClass6FU implements Runnable {
    public final /* synthetic */ AnonymousClass607 A00;

    public /* synthetic */ AnonymousClass6FU(AnonymousClass607 r1) {
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public final void run() {
        AnonymousClass607 r3 = this.A00;
        C117315Zl.A0R(r3.A01, r3.A0E.A00().A01(r3.A0R), new AbstractC14590lg() { // from class: X.6EF
            @Override // X.AbstractC14590lg
            public final void accept(Object obj) {
                AnonymousClass607 r32 = AnonymousClass607.this;
                AbstractC28901Pl r12 = (AbstractC28901Pl) obj;
                C119775f5 r2 = (C119775f5) r12.A08;
                if (r2 != null && "VISA".equals(r2.A03) && TextUtils.isEmpty(r2.A06)) {
                    new C129205xL(r32.A00, r32.A01, r32.A03, r32.A0B, r32.A0C, 
                    /*  JADX ERROR: Method code generation error
                        jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0032: INVOKE  
                          (wrap: X.5xL : 0x002d: CONSTRUCTOR  (r4v0 X.5xL A[REMOVE]) = 
                          (wrap: android.content.Context : 0x001c: IGET  (r5v0 android.content.Context A[REMOVE]) = (r3v0 'r32' X.607) X.607.A00 android.content.Context)
                          (wrap: X.0mE : 0x001e: IGET  (r6v0 X.0mE A[REMOVE]) = (r3v0 'r32' X.607) X.607.A01 X.0mE)
                          (wrap: X.0sm : 0x0022: IGET  (r7v0 X.0sm A[REMOVE]) = (r3v0 'r32' X.607) X.607.A03 X.0sm)
                          (wrap: X.0sn : 0x0024: IGET  (r8v0 X.0sn A[REMOVE]) = (r3v0 'r32' X.607) X.607.A0B X.0sn)
                          (wrap: X.0sj : 0x0020: IGET  (r9v0 X.0sj A[REMOVE]) = (r3v0 'r32' X.607) X.607.A0C X.0sj)
                          (wrap: X.6Ac : 0x0028: CONSTRUCTOR  (r10v0 X.6Ac A[REMOVE]) = (r12v1 'r12' X.1Pl), (r3v0 'r32' X.607), (r2v1 'r2' X.5f5) call: X.6Ac.<init>(X.1Pl, X.607, X.5f5):void type: CONSTRUCTOR)
                         call: X.5xL.<init>(android.content.Context, X.0mE, X.0sm, X.0sn, X.0sj, X.6Lw):void type: CONSTRUCTOR)
                          (wrap: java.lang.String : 0x0030: IGET  (r0v4 java.lang.String A[REMOVE]) = (r3v0 'r32' X.607) X.607.A0R java.lang.String)
                         type: VIRTUAL call: X.5xL.A00(java.lang.String):void in method: X.6EF.accept(java.lang.Object):void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                        	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                        	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                        	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                        	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                        	at java.util.ArrayList.forEach(ArrayList.java:1259)
                        	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                        	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                        Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x002d: CONSTRUCTOR  (r4v0 X.5xL A[REMOVE]) = 
                          (wrap: android.content.Context : 0x001c: IGET  (r5v0 android.content.Context A[REMOVE]) = (r3v0 'r32' X.607) X.607.A00 android.content.Context)
                          (wrap: X.0mE : 0x001e: IGET  (r6v0 X.0mE A[REMOVE]) = (r3v0 'r32' X.607) X.607.A01 X.0mE)
                          (wrap: X.0sm : 0x0022: IGET  (r7v0 X.0sm A[REMOVE]) = (r3v0 'r32' X.607) X.607.A03 X.0sm)
                          (wrap: X.0sn : 0x0024: IGET  (r8v0 X.0sn A[REMOVE]) = (r3v0 'r32' X.607) X.607.A0B X.0sn)
                          (wrap: X.0sj : 0x0020: IGET  (r9v0 X.0sj A[REMOVE]) = (r3v0 'r32' X.607) X.607.A0C X.0sj)
                          (wrap: X.6Ac : 0x0028: CONSTRUCTOR  (r10v0 X.6Ac A[REMOVE]) = (r12v1 'r12' X.1Pl), (r3v0 'r32' X.607), (r2v1 'r2' X.5f5) call: X.6Ac.<init>(X.1Pl, X.607, X.5f5):void type: CONSTRUCTOR)
                         call: X.5xL.<init>(android.content.Context, X.0mE, X.0sm, X.0sn, X.0sj, X.6Lw):void type: CONSTRUCTOR in method: X.6EF.accept(java.lang.Object):void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                        	at jadx.core.codegen.InsnGen.addArgDot(InsnGen.java:93)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:767)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                        	... 23 more
                        Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0028: CONSTRUCTOR  (r10v0 X.6Ac A[REMOVE]) = (r12v1 'r12' X.1Pl), (r3v0 'r32' X.607), (r2v1 'r2' X.5f5) call: X.6Ac.<init>(X.1Pl, X.607, X.5f5):void type: CONSTRUCTOR in method: X.6EF.accept(java.lang.Object):void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:708)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                        	... 29 more
                        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6Ac, state: NOT_LOADED
                        	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                        	... 35 more
                        */
                    /*
                        this = this;
                        X.607 r3 = X.AnonymousClass607.this
                        X.1Pl r12 = (X.AbstractC28901Pl) r12
                        X.1ZY r2 = r12.A08
                        X.5f5 r2 = (X.C119775f5) r2
                        if (r2 == 0) goto L_0x0035
                        java.lang.String r1 = r2.A03
                        java.lang.String r0 = "VISA"
                        boolean r0 = r0.equals(r1)
                        if (r0 == 0) goto L_0x0035
                        java.lang.String r0 = r2.A06
                        boolean r0 = android.text.TextUtils.isEmpty(r0)
                        if (r0 == 0) goto L_0x0035
                        android.content.Context r5 = r3.A00
                        X.0mE r6 = r3.A01
                        X.0sj r9 = r3.A0C
                        X.0sm r7 = r3.A03
                        X.0sn r8 = r3.A0B
                        X.6Ac r10 = new X.6Ac
                        r10.<init>(r12, r3, r2)
                        X.5xL r4 = new X.5xL
                        r4.<init>(r5, r6, r7, r8, r9, r10)
                        java.lang.String r0 = r3.A0R
                        r4.A00(r0)
                    L_0x0035:
                        return
                    */
                    throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass6EF.accept(java.lang.Object):void");
                }
            });
        }
    }
