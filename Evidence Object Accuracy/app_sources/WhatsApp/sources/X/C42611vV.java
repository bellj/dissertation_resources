package X;

import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import androidx.fragment.app.DialogFragment;
import com.facebook.redex.RunnableBRunnable0Shape0S0201000_I0;
import com.facebook.redex.RunnableBRunnable0Shape3S0200000_I0_3;
import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;
import com.facebook.redex.ViewOnClickCListenerShape0S0200000_I0;
import com.whatsapp.MuteDialogFragment;
import com.whatsapp.R;
import com.whatsapp.conversationslist.ConversationsFragment;
import com.whatsapp.conversationslist.LeaveGroupsDialogFragment;
import com.whatsapp.conversationslist.ViewHolder;
import com.whatsapp.dialogs.CreateOrAddToContactsDialog;
import com.whatsapp.jid.Jid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

/* renamed from: X.1vV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C42611vV implements AnonymousClass02Q {
    public MenuItem A00;
    public MenuItem A01;
    public MenuItem A02;
    public MenuItem A03;
    public MenuItem A04;
    public MenuItem A05;
    public MenuItem A06;
    public MenuItem A07;
    public MenuItem A08;
    public MenuItem A09;
    public MenuItem A0A;
    public MenuItem A0B;
    public MenuItem A0C;
    public MenuItem A0D;
    public final C63943Do A0E = new C63943Do();
    public final /* synthetic */ ConversationsFragment A0F;

    public C42611vV(ConversationsFragment conversationsFragment) {
        this.A0F = conversationsFragment;
    }

    @Override // X.AnonymousClass02Q
    public boolean ALr(MenuItem menuItem, AbstractC009504t r16) {
        Object tag;
        Intent A0O;
        Collection collection;
        int itemId = menuItem.getItemId();
        ConversationsFragment conversationsFragment = this.A0F;
        C243915i r3 = conversationsFragment.A0d;
        int i = 5;
        if (conversationsFragment.A2G.size() == 1) {
            i = 1;
        }
        r3.A01 = i;
        if (itemId == R.id.menuitem_conversations_archive) {
            ArrayList arrayList = new ArrayList(conversationsFragment.A2G);
            conversationsFragment.A1O(0);
            if (!arrayList.isEmpty()) {
                conversationsFragment.A2B.Ab2(new RunnableBRunnable0Shape3S0200000_I0_3(this, 21, arrayList));
            }
        } else if (itemId == R.id.menuitem_conversations_unarchive) {
            ArrayList arrayList2 = new ArrayList(conversationsFragment.A2G);
            conversationsFragment.A1O(0);
            conversationsFragment.A07.post(new RunnableBRunnable0Shape3S0200000_I0_3(this, 20, arrayList2));
            if (conversationsFragment.A1D.A00.getBoolean("archive_v2_enabled", false)) {
                int size = arrayList2.size();
                conversationsFragment.A1S(conversationsFragment.A02().getQuantityString(R.plurals.conversations_unarchived_confirmation, size, Integer.valueOf(size)), conversationsFragment.A0I(R.string.undo), new ViewOnClickCListenerShape0S0200000_I0(this, 29, arrayList2));
                return true;
            }
        } else if (itemId == R.id.menuitem_conversations_delete) {
            conversationsFragment.A29.A06(conversationsFragment.A0C()).A00(new AbstractC14590lg() { // from class: X.3bM
                @Override // X.AbstractC14590lg
                public final void accept(Object obj) {
                    C42611vV r2 = C42611vV.this;
                    ConversationsFragment conversationsFragment2 = r2.A0F;
                    AbstractC14640lm A01 = ConversationsFragment.A01(conversationsFragment2);
                    conversationsFragment2.A1f = A01;
                    if (A01 != null) {
                        C42581vS r22 = conversationsFragment2.A10;
                        r22.A0E.A07(A01, new C1115159r(((AnonymousClass01E) conversationsFragment2).A0H, r22, A01));
                        return;
                    }
                    AnonymousClass01F r1 = ((AnonymousClass01E) conversationsFragment2).A0H;
                    if (r1 != null && conversationsFragment2.A2G.size() != 0) {
                        C70153ap r5 = new C70153ap(r1, r2);
                        C255719x r4 = conversationsFragment2.A29;
                        AnonymousClass390 r12 = new AnonymousClass390(r4.A03, r5, conversationsFragment2.A2G);
                        C12960it.A1E(r12, r4.A08);
                        r4.A00.A0J(new RunnableBRunnable0Shape8S0200000_I0_8(r12, 31, r5), 500);
                    }
                }
            });
            return true;
        } else if (itemId == R.id.menuitem_conversations_leave) {
            AbstractC14640lm A01 = ConversationsFragment.A01(conversationsFragment);
            conversationsFragment.A1f = A01;
            if (A01 != null) {
                C42581vS r2 = conversationsFragment.A10;
                r2.A0E.A07(A01, new C1115159r(((AnonymousClass01E) conversationsFragment).A0H, r2, A01));
                return true;
            }
            AnonymousClass01F r4 = ((AnonymousClass01E) conversationsFragment).A0H;
            if (r4 != null) {
                conversationsFragment.A2B.Aaz(new AnonymousClass392((DialogFragment) new LeaveGroupsDialogFragment(), r4, conversationsFragment.A1T, (Set) conversationsFragment.A2G, false), new Object[0]);
                return true;
            }
        } else if (itemId == R.id.menuitem_conversations_mute) {
            AbstractC14640lm A012 = ConversationsFragment.A01(conversationsFragment);
            conversationsFragment.A1f = A012;
            if (A012 != null) {
                collection = Collections.singleton(A012);
            } else {
                collection = conversationsFragment.A2G;
            }
            MuteDialogFragment.A01(collection).A1F(conversationsFragment.A0F(), null);
            return true;
        } else if (itemId == R.id.menuitem_conversations_unmute) {
            LinkedHashSet linkedHashSet = new LinkedHashSet(conversationsFragment.A2G);
            conversationsFragment.A1O(1);
            conversationsFragment.A2B.Ab2(new RunnableBRunnable0Shape3S0200000_I0_3(this, 22, linkedHashSet));
            return true;
        } else if (itemId == R.id.menuitem_conversations_pin) {
            HashSet hashSet = new HashSet(conversationsFragment.A2G);
            Set A0D = conversationsFragment.A1t.A0D();
            hashSet.removeAll(A0D);
            int size2 = hashSet.size();
            if (A0D.size() + size2 > 3) {
                conversationsFragment.A11.A01(A0D);
                return true;
            }
            conversationsFragment.A1O(1);
            conversationsFragment.A2B.Ab2(new RunnableBRunnable0Shape0S0201000_I0(this, hashSet, size2, 12));
            return true;
        } else if (itemId == R.id.menuitem_conversations_unpin) {
            LinkedHashSet linkedHashSet2 = new LinkedHashSet(conversationsFragment.A2G);
            conversationsFragment.A1O(1);
            conversationsFragment.A2B.Ab2(new RunnableBRunnable0Shape3S0200000_I0_3(this, 23, linkedHashSet2));
            return true;
        } else if (itemId == R.id.menuitem_conversations_create_shortcuit) {
            AbstractC14640lm A013 = ConversationsFragment.A01(conversationsFragment);
            conversationsFragment.A1f = A013;
            if (A013 != null) {
                conversationsFragment.A0R.A05(conversationsFragment.A0j.A0B(A013));
            }
            conversationsFragment.A1O(2);
            return true;
        } else {
            if (itemId == R.id.menuitem_conversations_contact_info) {
                AbstractC14640lm A014 = ConversationsFragment.A01(conversationsFragment);
                conversationsFragment.A1f = A014;
                if (A014 != null) {
                    C15370n3 A0B = conversationsFragment.A0j.A0B(A014);
                    conversationsFragment.A1O(2);
                    if (A0B.A0C != null) {
                        ActivityC000900k A0C = conversationsFragment.A0C();
                        A0C.startActivity(new C14960mK().A0h(A0C, A0B, 12));
                        return true;
                    }
                    boolean A0F = C15380n4.A0F(A0B.A0D);
                    ActivityC000900k A0C2 = conversationsFragment.A0C();
                    Jid jid = A0B.A0D;
                    if (A0F) {
                        A0O = C14960mK.A0K(A0C2, jid);
                    } else {
                        A0O = C14960mK.A0O(A0C2, jid, true, false, true);
                        C35741ib.A00(A0O, A0C2.getClass().getSimpleName());
                    }
                    A0C2.startActivity(A0O, null);
                    return true;
                }
            } else if (itemId == R.id.menuitem_conversations_add_new_contact) {
                AbstractC14640lm A015 = ConversationsFragment.A01(conversationsFragment);
                conversationsFragment.A1f = A015;
                if (A015 != null) {
                    CreateOrAddToContactsDialog.A00(conversationsFragment.A0j.A0B(A015)).A1F(conversationsFragment.A0E(), null);
                    return true;
                }
            } else {
                if (itemId == R.id.menuitem_conversations_mark_read) {
                    Iterator it = conversationsFragment.A2G.iterator();
                    while (it.hasNext()) {
                        AbstractC14640lm r8 = (AbstractC14640lm) it.next();
                        if (!C15380n4.A0O(r8)) {
                            conversationsFragment.A0s.A00(r8, null, null, true, true, true);
                            conversationsFragment.A1k.A07();
                        }
                    }
                } else if (itemId == R.id.menuitem_conversations_mark_unread) {
                    Iterator it2 = conversationsFragment.A2G.iterator();
                    while (it2.hasNext()) {
                        AbstractC14640lm r1 = (AbstractC14640lm) it2.next();
                        if (!C15380n4.A0F(r1) && !C15380n4.A0O(r1)) {
                            conversationsFragment.A0s.A01(r1, true);
                        }
                    }
                } else if (itemId == R.id.menuitem_conversations_select_all) {
                    conversationsFragment.A2H.clear();
                    for (int i2 = 0; i2 < conversationsFragment.A07.getChildCount(); i2++) {
                        View childAt = conversationsFragment.A07.getChildAt(i2);
                        if (!(childAt == null || (tag = childAt.getTag()) == null || !(tag instanceof ViewHolder))) {
                            ViewHolder viewHolder = (ViewHolder) tag;
                            AbstractC14640lm ADg = viewHolder.A02.ADg();
                            if (!conversationsFragment.A2G.contains(ADg)) {
                                conversationsFragment.A2G.add(ADg);
                                viewHolder.A05.setBackgroundResource(R.color.home_row_selection);
                                viewHolder.A0K(true, true);
                            }
                        }
                    }
                    Iterator it3 = conversationsFragment.A1C().iterator();
                    while (it3.hasNext()) {
                        AbstractC14640lm ADg2 = ((AbstractC51662Vw) it3.next()).ADg();
                        if (!conversationsFragment.A2G.contains(ADg2) && !C15380n4.A0O(ADg2)) {
                            conversationsFragment.A2G.add(ADg2);
                        }
                    }
                    if (conversationsFragment.A0D != null) {
                        conversationsFragment.A0D.A0B(String.format(AnonymousClass018.A00(conversationsFragment.A1E.A00), "%d", Integer.valueOf(conversationsFragment.A2G.size())));
                        conversationsFragment.A0D.A06();
                    }
                    if (!conversationsFragment.A2G.isEmpty()) {
                        AnonymousClass23N.A00(conversationsFragment.A0C(), conversationsFragment.A18, conversationsFragment.A02().getQuantityString(R.plurals.n_items_selected, conversationsFragment.A2G.size(), Integer.valueOf(conversationsFragment.A2G.size())));
                        return true;
                    }
                }
                conversationsFragment.A1O(1);
                return true;
            }
            return false;
        }
        return true;
    }

    @Override // X.AnonymousClass02Q
    public boolean AOg(Menu menu, AbstractC009504t r11) {
        this.A06 = menu.add(0, R.id.menuitem_conversations_pin, 0, (CharSequence) null).setIcon(R.drawable.ic_action_pin);
        this.A0A = menu.add(0, R.id.menuitem_conversations_unpin, 0, (CharSequence) null).setIcon(R.drawable.ic_action_unpin);
        this.A03 = menu.add(0, R.id.menuitem_conversations_delete, 0, (CharSequence) null).setIcon(R.drawable.ic_action_delete);
        this.A05 = menu.add(0, R.id.menuitem_conversations_mute, 0, (CharSequence) null).setIcon(R.drawable.ic_action_mute);
        this.A09 = menu.add(0, R.id.menuitem_conversations_unmute, 0, (CharSequence) null).setIcon(R.drawable.ic_action_unmute);
        MenuItem add = menu.add(0, R.id.menuitem_conversations_archive, 0, (CharSequence) null);
        ConversationsFragment conversationsFragment = this.A0F;
        this.A02 = add.setIcon(AnonymousClass2GE.A01(conversationsFragment.A01(), R.drawable.ic_action_archive, R.color.white));
        this.A08 = menu.add(0, R.id.menuitem_conversations_unarchive, 0, (CharSequence) null).setIcon(AnonymousClass2GE.A01(conversationsFragment.A01(), R.drawable.ic_action_unarchive, R.color.white));
        this.A04 = menu.add(0, R.id.menuitem_conversations_leave, 0, (CharSequence) null);
        this.A01 = menu.add(0, R.id.menuitem_conversations_create_shortcuit, 0, R.string.add_shortcut);
        this.A0D = menu.add(0, R.id.menuitem_conversations_contact_info, 0, R.string.contact_info);
        this.A00 = menu.add(0, R.id.menuitem_conversations_add_new_contact, 0, R.string.add_contact);
        this.A07 = menu.add(0, R.id.menuitem_conversations_mark_read, 0, R.string.mark_read);
        this.A0B = menu.add(0, R.id.menuitem_conversations_mark_unread, 0, R.string.mark_unread);
        this.A0C = menu.add(0, R.id.menuitem_conversations_select_all, 0, R.string.select_all_conversations);
        this.A06.setShowAsAction(2);
        this.A0A.setShowAsAction(2);
        this.A02.setShowAsAction(2);
        this.A08.setShowAsAction(2);
        this.A03.setShowAsAction(2);
        this.A05.setShowAsAction(2);
        this.A09.setShowAsAction(2);
        this.A04.setShowAsAction(8);
        this.A01.setShowAsAction(8);
        this.A0D.setShowAsAction(8);
        this.A00.setShowAsAction(8);
        this.A07.setShowAsAction(8);
        this.A0B.setShowAsAction(8);
        this.A0C.setShowAsAction(8);
        C63943Do r0 = this.A0E;
        r0.A00(R.id.menuitem_conversations_leave);
        r0.A00(R.id.menuitem_conversations_create_shortcuit);
        r0.A00(R.id.menuitem_conversations_contact_info);
        r0.A00(R.id.menuitem_conversations_add_new_contact);
        r0.A00(R.id.menuitem_conversations_mark_read);
        r0.A00(R.id.menuitem_conversations_mark_unread);
        r0.A00(R.id.menuitem_conversations_select_all);
        return true;
    }

    @Override // X.AnonymousClass02Q
    public void AP3(AbstractC009504t r4) {
        ConversationsFragment conversationsFragment = this.A0F;
        conversationsFragment.A1N(2);
        conversationsFragment.A0D = null;
        C53002cC r0 = conversationsFragment.A0v;
        if (r0 != null) {
            r0.setEnableState(true);
        }
        C53002cC r02 = conversationsFragment.A0u;
        if (r02 != null) {
            r02.setEnableState(true);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:113:0x0259 A[ADDED_TO_REGION, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0178  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x017d A[ADDED_TO_REGION] */
    @Override // X.AnonymousClass02Q
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean AU7(android.view.Menu r19, X.AbstractC009504t r20) {
        /*
        // Method dump skipped, instructions count: 688
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C42611vV.AU7(android.view.Menu, X.04t):boolean");
    }
}
