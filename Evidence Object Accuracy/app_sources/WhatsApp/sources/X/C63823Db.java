package X;

import com.whatsapp.Conversation;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.3Db  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63823Db {
    public final Set A00 = C12970iu.A12();

    public void A00() {
        Set<C89124It> set = this.A00;
        synchronized (set) {
            if (!set.isEmpty()) {
                for (C89124It r0 : set) {
                    Conversation conversation = r0.A00;
                    conversation.A3s.A0A(true);
                    conversation.A3s.A08();
                }
            }
        }
    }

    public void A01() {
        Set set = this.A00;
        synchronized (set) {
            if (!set.isEmpty()) {
                Iterator it = set.iterator();
                while (it.hasNext()) {
                    it.next();
                }
            }
        }
    }
}
