package X;

import android.net.Uri;
import android.os.Build;
import android.os.ConditionVariable;
import com.whatsapp.Mp4Ops;
import com.whatsapp.files.FileUtils$OsRename;
import com.whatsapp.stickers.WebpUtils;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.1Pn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28921Pn extends AbstractRunnableC14570le implements Comparable, AbstractC28931Po {
    public C37741mv A00;
    public C14430lQ A01;
    public AnonymousClass330 A02;
    public C43161wW A03;
    public File A04;
    public URL A05;
    public boolean A06;
    public final int A07;
    public final long A08;
    public final ConditionVariable A09;
    public final C21040wk A0A;
    public final AbstractC15710nm A0B;
    public final C14330lG A0C;
    public final C20870wS A0D;
    public final Mp4Ops A0E;
    public final C15450nH A0F;
    public final C18790t3 A0G;
    public final C14620lj A0H;
    public final C14620lj A0I;
    public final C14620lj A0J = new C14620lj();
    public final C14620lj A0K = new C14620lj();
    public final C14620lj A0L = new C14620lj();
    public final C14620lj A0M = new C14620lj();
    public final C14830m7 A0N;
    public final C16590pI A0O;
    public final C14950mJ A0P;
    public final C15660nh A0Q;
    public final C14850m9 A0R;
    public final C20110vE A0S;
    public final C19940uv A0T;
    public final C14410lO A0U;
    public final AnonymousClass155 A0V;
    public final C17040qA A0W;
    public final C14420lP A0X;
    public final AnonymousClass12J A0Y;
    public final C28781Oz A0Z;
    public final C41471ta A0a;
    public final C239713s A0b;
    public final C16630pM A0c;
    public final AbstractC37811n3 A0d;
    public final C22600zL A0e;
    public final C15860o1 A0f;
    public final C22590zK A0g;
    public final AnonymousClass2Ba A0h;
    public final C26511Dt A0i;
    public final C26481Dq A0j;
    public final C26521Du A0k;
    public final AbstractC14440lR A0l;
    public final C26501Ds A0m;
    public final C21710xr A0n;
    public final Object A0o;
    public final LinkedList A0p = new LinkedList();
    public final CountDownLatch A0q = new CountDownLatch(1);
    public final AtomicBoolean A0r = new AtomicBoolean();
    public volatile int A0s;
    public volatile boolean A0t;
    public volatile boolean A0u;

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x011a, code lost:
        if (r1 == 2) goto L_0x011c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C28921Pn(android.os.ConditionVariable r9, X.C21040wk r10, X.AbstractC15710nm r11, X.C14330lG r12, X.C14900mE r13, X.C20870wS r14, com.whatsapp.Mp4Ops r15, X.C15450nH r16, X.C18790t3 r17, X.C14830m7 r18, X.C16590pI r19, X.C14950mJ r20, X.C15660nh r21, X.C14850m9 r22, X.C20110vE r23, X.C19940uv r24, X.C14410lO r25, X.AnonymousClass155 r26, X.C17040qA r27, X.C14420lP r28, X.AnonymousClass12J r29, X.C41471ta r30, X.C239713s r31, X.C16630pM r32, X.C22600zL r33, X.C15860o1 r34, X.C22590zK r35, X.C26511Dt r36, X.C26481Dq r37, X.C26521Du r38, X.AbstractC14440lR r39, X.C26501Ds r40, X.C21710xr r41, int r42, int r43, long r44, boolean r46) {
        /*
        // Method dump skipped, instructions count: 371
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28921Pn.<init>(android.os.ConditionVariable, X.0wk, X.0nm, X.0lG, X.0mE, X.0wS, com.whatsapp.Mp4Ops, X.0nH, X.0t3, X.0m7, X.0pI, X.0mJ, X.0nh, X.0m9, X.0vE, X.0uv, X.0lO, X.155, X.0qA, X.0lP, X.12J, X.1ta, X.13s, X.0pM, X.0zL, X.0o1, X.0zK, X.1Dt, X.1Dq, X.1Du, X.0lR, X.1Ds, X.0xr, int, int, long, boolean):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0053  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AbstractC16130oV A00(X.C15660nh r14, X.C41471ta r15, X.C15860o1 r16) {
        /*
        // Method dump skipped, instructions count: 322
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28921Pn.A00(X.0nh, X.1ta, X.0o1):X.0oV");
    }

    public static void A01(AbstractC15710nm r4, C14330lG r5, C16590pI r6, C28921Pn r7, C28781Oz r8, C41471ta r9, C239713s r10, File file) {
        String str;
        C14370lK r1 = r9.A09;
        if ((r1 == C14370lK.A08 || r1 == C14370lK.A0T) && "application/pdf".equals(r9.A0H)) {
            AnonymousClass3JC r0 = new AnonymousClass3JC(file);
            try {
                r0.A04();
                boolean z = r0.A03;
                int i = 0;
                if (z) {
                    i = 3;
                }
                r8.A09(i);
            } catch (C87504Bt e) {
                r8.A09(3);
                Log.e("MediaDownload/Failed to parse document", e);
            } catch (IOException e2) {
                Log.e("MediaDownload/Failed to parse document", e2);
            }
        } else {
            if (AnonymousClass14P.A02(r1) || r1 == C14370lK.A05 || r1 == C14370lK.A0I) {
                if (r10.A06(r1, file)) {
                    try {
                        Mp4Ops.A04(file, false);
                        return;
                    } catch (C39361pl e3) {
                        if (!((AbstractRunnableC14570le) r7).A02.isCancelled()) {
                            Mp4Ops.A00(r6.A00, r4, file, e3, "check on download");
                            if (e3.errorCode < 300) {
                                str = "MediaDownload/suspicious video/audio found, file deleted";
                            } else {
                                return;
                            }
                        } else {
                            return;
                        }
                    }
                } else {
                    return;
                }
            } else if (r1 == C14370lK.A0S && WebpUtils.verifyWebpFileIntegrity(file.getAbsolutePath()) == null) {
                str = "MediaDownload/suspicious sticker found, file deleted";
            } else {
                return;
            }
            Log.w(str);
            r8.A09(1);
            A02(r5, file);
        }
    }

    public static boolean A02(C14330lG r2, File file) {
        File A0F = r2.A0F(file);
        if (A0F.exists() && !A0F.delete()) {
            StringBuilder sb = new StringBuilder("MediaDownload/MMS failed to delete stream check success file ");
            sb.append(A0F);
            Log.w(sb.toString());
        }
        return file.delete();
    }

    public static boolean A03(File file, File file2) {
        boolean z;
        String str;
        if (Build.VERSION.SDK_INT < 21) {
            z = file.renameTo(file2);
            if (!z) {
                str = "media-file-utils/download file rename failed";
                Log.i(str);
            }
            return z;
        }
        z = true;
        if (FileUtils$OsRename.attempt(file, file2) > 0) {
            if (FileUtils$OsRename.attempt(file, file2) > 0) {
                Log.i("media-file-utils/Second try rename failed");
                return false;
            }
            str = "media-file-utils/Second try rename succeeded";
            Log.i(str);
        }
        return z;
    }

    @Override // X.AbstractRunnableC14570le, X.C14580lf
    public void A04() {
        super.A04();
        this.A0K.A01();
        this.A0J.A01();
        this.A0L.A01();
        this.A0I.A01();
        this.A0H.A01();
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(30:52|(1:54)|56|(2:58|(1:60))(1:162)|61|(4:63|(1:67)|68|(6:72|73|48|(7:50|147|176|(1:178)|179|(4:183|(1:185)|186|(1:188)(2:192|(3:199|(1:(1:202)(1:204))|203)(1:198)))|(3:190|50f|207))(1:212)|213|214))|74|(1:76)|77|(4:79|(1:81)|82|(9:(1:85)|86|(3:88|233|96)|97|235|98|(9:100|(2:102|(1:106))|107|(1:116)(4:118|31f|122|329)|117|48|(0)(0)|213|214)|127|(4:132|(5:148|(1:150)(5:155|(1:157)|158|(1:160)|161)|151|(1:153)|154)(4:139|(1:141)|142|(1:144))|145|(6:147|73|48|(0)(0)|213|214)(5:163|(1:165)|166|(7:168|(1:170)|173|48|(0)(0)|213|214)|171))(1:131)))|90|(1:92)|93|86|(0)|97|235|98|(0)|127|(1:129)|132|(0)|148|(0)(0)|151|(0)|154|145|(0)(0)) */
    /* JADX WARNING: Code restructure failed: missing block: B:126:0x0332, code lost:
        com.whatsapp.util.Log.w("MediaDownload/call/unable to find existing file.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:172:0x04b8, code lost:
        if (((X.AbstractRunnableC14570le) r32).A02.isCancelled() != false) goto L_0x04ba;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0165, code lost:
        if (r3 == 2) goto L_0x0167;
     */
    /* JADX WARNING: Removed duplicated region for block: B:100:0x0289  */
    /* JADX WARNING: Removed duplicated region for block: B:147:0x03d9  */
    /* JADX WARNING: Removed duplicated region for block: B:150:0x03e7  */
    /* JADX WARNING: Removed duplicated region for block: B:153:0x03f8  */
    /* JADX WARNING: Removed duplicated region for block: B:155:0x041c  */
    /* JADX WARNING: Removed duplicated region for block: B:163:0x044c  */
    /* JADX WARNING: Removed duplicated region for block: B:212:0x0581  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x013c  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0231  */
    @Override // X.AbstractRunnableC14570le
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ java.lang.Object A05() {
        /*
        // Method dump skipped, instructions count: 1425
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28921Pn.A05():java.lang.Object");
    }

    public final File A07() {
        StringBuilder sb = new StringBuilder();
        String str = this.A0a.A0F;
        AnonymousClass009.A05(str);
        sb.append(str.replace('/', '-'));
        sb.append(".chk.tmp");
        return this.A0C.A0M(sb.toString());
    }

    public void A08() {
        byte[] A0G = this.A0Z.A0G();
        if (A0G != null) {
            this.A0L.A04(A0G);
        }
    }

    public void A09(long j) {
        this.A0K.A04(Long.valueOf(j));
    }

    public void A0A(AnonymousClass1RN r5) {
        C28781Oz r0 = this.A0Z;
        A0E(r0);
        LinkedList linkedList = this.A0p;
        synchronized (linkedList) {
            C28781Oz A02 = r0.A02();
            Iterator it = linkedList.iterator();
            while (it.hasNext()) {
                ((AbstractC28771Oy) it.next()).APQ(r5, A02);
            }
            linkedList.clear();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0059, code lost:
        if (r0.A0C == false) goto L_0x005b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0180, code lost:
        if (r1 == 2) goto L_0x0182;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x01b1, code lost:
        if (r11 == 15) goto L_0x01b3;
     */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x022a  */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x024c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0B(X.AnonymousClass1RN r19) {
        /*
        // Method dump skipped, instructions count: 695
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28921Pn.A0B(X.1RN):void");
    }

    public final void A0C(AnonymousClass1RN r5) {
        StringBuilder sb = new StringBuilder("MediaDownload/updateMessageAfterDownload/mediaHash=");
        C41471ta r3 = this.A0a;
        sb.append(r3.A0F);
        sb.append(" url=");
        sb.append(this.A05);
        sb.append(" status=");
        sb.append(r5);
        Log.i(sb.toString());
        if (this.A0u) {
            this.A0e.A04(this.A0d);
        }
        boolean z = false;
        if (r5.A00 == 0) {
            z = true;
        }
        this.A0Z.A0C(r5, r3.A01, z);
    }

    public final void A0D(AnonymousClass1RN r7) {
        A0C(r7);
        AnonymousClass2Ba r2 = this.A0h;
        if (r2 != null) {
            int i = r7.A00;
            boolean A01 = AnonymousClass1RN.A01(i);
            synchronized (r2) {
                r2.A0B = A01;
                r2.A02 = i;
            }
            r2.A05(4);
            synchronized (r2) {
                r2.A0F.clear();
            }
        }
        File file = this.A04;
        if (file != null && file.length() == 0) {
            A02(this.A0C, this.A04);
        }
        int i2 = r7.A00;
        if (i2 == 5 || i2 == 12 || i2 == 4) {
            this.A0Z.A05();
        }
    }

    public void A0E(C28781Oz r3) {
        StringBuilder sb = new StringBuilder("MediaDownload/publishDownloadDataWhenComplete; mediaHash=");
        sb.append(this.A0a.A0F);
        sb.append(" downloadData=");
        sb.append(r3.toString());
        Log.i(sb.toString());
        this.A0H.A04(r3.A02());
    }

    public final void A0F(File file) {
        C15450nH r2 = this.A0F;
        int i = this.A0a.A04;
        long length = file.length();
        if (i == 7 && length < ((long) r2.A02(AbstractC15460nI.A2E)) * 1024) {
            try {
                List A00 = C41391tS.A00(this.A0m.A01(Uri.fromFile(file)));
                if (A00 != null) {
                    C28781Oz r1 = this.A0Z;
                    Integer valueOf = Integer.valueOf(A00.size());
                    synchronized (r1) {
                        r1.A07 = valueOf;
                    }
                }
            } catch (IOException e) {
                Log.e("vcardloader/splitvcards/exception", e);
            }
        }
    }

    @Override // X.AbstractC28931Po
    public void A5g(AbstractC28771Oy r3) {
        LinkedList linkedList = this.A0p;
        synchronized (linkedList) {
            linkedList.add(r3);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0050, code lost:
        if (r3 == false) goto L_0x0052;
     */
    @Override // X.AbstractC28931Po
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A76(boolean r8) {
        /*
            r7 = this;
            if (r8 == 0) goto L_0x0005
            r7.A04()
        L_0x0005:
            java.lang.String r0 = "MediaDownload/cancelMediaDownload/mediaHash="
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            X.1ta r6 = r7.A0a
            java.lang.String r0 = r6.A0F
            r1.append(r0)
            r1.append(r0)
            java.lang.String r0 = " url="
            r1.append(r0)
            java.net.URL r0 = r7.A05
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            com.whatsapp.util.Log.i(r0)
            java.lang.Object r2 = r7.A0o
            monitor-enter(r2)
            X.330 r1 = r7.A02     // Catch: all -> 0x0087
            if (r1 == 0) goto L_0x003b
            monitor-enter(r1)     // Catch: all -> 0x0087
            X.5Wn r0 = r1.A00     // Catch: all -> 0x0035
            monitor-exit(r1)     // Catch: all -> 0x0035
            if (r0 == 0) goto L_0x003b
            goto L_0x0038
        L_0x0035:
            r0 = move-exception
            monitor-exit(r1)     // Catch: all -> 0x0035
            throw r0     // Catch: all -> 0x0087
        L_0x0038:
            r0.A75()     // Catch: all -> 0x0087
        L_0x003b:
            monitor-exit(r2)     // Catch: all -> 0x0087
            r7.cancel()
            X.1Oz r5 = r7.A0Z
            java.lang.Boolean r0 = r5.A04()
            r4 = 0
            if (r0 == 0) goto L_0x0069
            java.lang.Boolean r0 = r5.A04()
            boolean r3 = r0.booleanValue()
            if (r3 != 0) goto L_0x0065
        L_0x0052:
            r2 = 13
            r0 = 0
            X.1RN r1 = new X.1RN
            r1.<init>(r0, r0, r2, r4)
            int r0 = r6.A01
            r5.A0C(r1, r0, r4)
            r5.A05()
            r7.A0E(r5)
        L_0x0065:
            java.util.LinkedList r2 = r7.A0p
            monitor-enter(r2)
            goto L_0x006b
        L_0x0069:
            r3 = 0
            goto L_0x0052
        L_0x006b:
            java.util.Iterator r1 = r2.iterator()     // Catch: all -> 0x0084
        L_0x006f:
            boolean r0 = r1.hasNext()     // Catch: all -> 0x0084
            if (r0 == 0) goto L_0x007f
            java.lang.Object r0 = r1.next()     // Catch: all -> 0x0084
            X.1Oy r0 = (X.AbstractC28771Oy) r0     // Catch: all -> 0x0084
            r0.APP(r3)     // Catch: all -> 0x0084
            goto L_0x006f
        L_0x007f:
            r2.clear()     // Catch: all -> 0x0084
            monitor-exit(r2)     // Catch: all -> 0x0084
            return
        L_0x0084:
            r0 = move-exception
            monitor-exit(r2)     // Catch: all -> 0x0084
            throw r0
        L_0x0087:
            r0 = move-exception
            monitor-exit(r2)     // Catch: all -> 0x0087
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28921Pn.A76(boolean):void");
    }

    @Override // java.lang.Comparable
    public int compareTo(Object obj) {
        if (!(obj instanceof C28921Pn)) {
            return 0;
        }
        long j = ((C28921Pn) obj).A08;
        long j2 = this.A08;
        if (j < j2) {
            return -1;
        }
        return j2 >= j ? 0 : 1;
    }
}
