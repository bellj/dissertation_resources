package X;

import android.util.ArrayMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

/* renamed from: X.1J5  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1J5 {
    public static final ArrayMap A00(AnonymousClass1J6 r5, XmlPullParser xmlPullParser, String[] strArr) {
        ArrayMap arrayMap = new ArrayMap();
        int eventType = xmlPullParser.getEventType();
        do {
            if (eventType == 2) {
                arrayMap.put(strArr[0], A01(r5, xmlPullParser, strArr, true));
            } else if (eventType == 3) {
                if (xmlPullParser.getName().equals("map")) {
                    return arrayMap;
                }
                StringBuilder sb = new StringBuilder("Expected ");
                sb.append("map");
                sb.append(" end tag at: ");
                sb.append(xmlPullParser.getName());
                throw new XmlPullParserException(sb.toString());
            }
            eventType = xmlPullParser.next();
        } while (eventType != 1);
        StringBuilder sb2 = new StringBuilder("Document ended before ");
        sb2.append("map");
        sb2.append(" end tag");
        throw new XmlPullParserException(sb2.toString());
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:342:0x059f */
    /* JADX DEBUG: Multi-variable search result rejected for r4v2, resolved type: byte[] */
    /* JADX DEBUG: Multi-variable search result rejected for r4v3, resolved type: java.lang.Object */
    /* JADX DEBUG: Multi-variable search result rejected for r4v8, resolved type: boolean[] */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r4v4, types: [int[]] */
    /* JADX WARN: Type inference failed for: r4v5, types: [long[]] */
    /* JADX WARN: Type inference failed for: r4v6, types: [double[]] */
    /* JADX WARN: Type inference failed for: r4v7, types: [java.lang.String[]] */
    /* JADX WARN: Type inference failed for: r4v9, types: [java.util.HashSet, java.util.AbstractCollection] */
    /* JADX WARN: Type inference failed for: r4v10, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v11, types: [java.util.AbstractMap, java.util.HashMap] */
    /* JADX WARN: Type inference failed for: r4v12, types: [android.util.ArrayMap] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=byte[], code=boolean[], for r4v2, types: [byte[]] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.lang.Object A01(X.AnonymousClass1J6 r14, org.xmlpull.v1.XmlPullParser r15, java.lang.String[] r16, boolean r17) {
        /*
        // Method dump skipped, instructions count: 1954
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1J5.A01(X.1J6, org.xmlpull.v1.XmlPullParser, java.lang.String[], boolean):java.lang.Object");
    }

    public static final void A02(Object obj, String str, XmlSerializer xmlSerializer) {
        String str2;
        String str3;
        int i;
        int i2;
        if (obj == null) {
            str2 = "null";
            xmlSerializer.startTag(null, str2);
            if (str != null) {
                xmlSerializer.attribute(null, "name", str);
            }
        } else {
            String str4 = "string";
            if (!(obj instanceof String)) {
                if (obj instanceof Integer) {
                    str4 = "int";
                } else if (obj instanceof Long) {
                    str4 = "long";
                } else if (obj instanceof Float) {
                    str4 = "float";
                } else if (obj instanceof Double) {
                    str4 = "double";
                } else if (obj instanceof Boolean) {
                    str4 = "boolean";
                } else {
                    if (obj instanceof byte[]) {
                        byte[] bArr = (byte[]) obj;
                        if (bArr != null) {
                            xmlSerializer.startTag(null, "byte-array");
                            if (str != null) {
                                xmlSerializer.attribute(null, "name", str);
                            }
                            int length = bArr.length;
                            xmlSerializer.attribute(null, "num", Integer.toString(length));
                            StringBuilder sb = new StringBuilder(length << 1);
                            for (byte b : bArr) {
                                int i3 = (b >> 4) & 15;
                                if (i3 >= 10) {
                                    i = (i3 + 97) - 10;
                                } else {
                                    i = i3 + 48;
                                }
                                sb.append((char) i);
                                int i4 = b & 15;
                                if (i4 >= 10) {
                                    i2 = (i4 + 97) - 10;
                                } else {
                                    i2 = i4 + 48;
                                }
                                sb.append((char) i2);
                            }
                            xmlSerializer.text(sb.toString());
                            xmlSerializer.endTag(null, "byte-array");
                            return;
                        }
                    } else if (obj instanceof int[]) {
                        int[] iArr = (int[]) obj;
                        if (iArr != null) {
                            str3 = "int-array";
                            xmlSerializer.startTag(null, str3);
                            if (str != null) {
                                xmlSerializer.attribute(null, "name", str);
                            }
                            int length2 = iArr.length;
                            xmlSerializer.attribute(null, "num", Integer.toString(length2));
                            for (int i5 : iArr) {
                                xmlSerializer.startTag(null, "item");
                                xmlSerializer.attribute(null, "value", Integer.toString(i5));
                                xmlSerializer.endTag(null, "item");
                            }
                            xmlSerializer.endTag(null, str3);
                            return;
                        }
                    } else if (obj instanceof long[]) {
                        long[] jArr = (long[]) obj;
                        if (jArr != null) {
                            str3 = "long-array";
                            xmlSerializer.startTag(null, str3);
                            if (str != null) {
                                xmlSerializer.attribute(null, "name", str);
                            }
                            int length3 = jArr.length;
                            xmlSerializer.attribute(null, "num", Integer.toString(length3));
                            for (long j : jArr) {
                                xmlSerializer.startTag(null, "item");
                                xmlSerializer.attribute(null, "value", Long.toString(j));
                                xmlSerializer.endTag(null, "item");
                            }
                            xmlSerializer.endTag(null, str3);
                            return;
                        }
                    } else if (obj instanceof double[]) {
                        double[] dArr = (double[]) obj;
                        if (dArr != null) {
                            str3 = "double-array";
                            xmlSerializer.startTag(null, str3);
                            if (str != null) {
                                xmlSerializer.attribute(null, "name", str);
                            }
                            int length4 = dArr.length;
                            xmlSerializer.attribute(null, "num", Integer.toString(length4));
                            for (double d : dArr) {
                                xmlSerializer.startTag(null, "item");
                                xmlSerializer.attribute(null, "value", Double.toString(d));
                                xmlSerializer.endTag(null, "item");
                            }
                            xmlSerializer.endTag(null, str3);
                            return;
                        }
                    } else if (obj instanceof String[]) {
                        String[] strArr = (String[]) obj;
                        if (strArr != null) {
                            str3 = "string-array";
                            xmlSerializer.startTag(null, str3);
                            if (str != null) {
                                xmlSerializer.attribute(null, "name", str);
                            }
                            int length5 = strArr.length;
                            xmlSerializer.attribute(null, "num", Integer.toString(length5));
                            for (String str5 : strArr) {
                                xmlSerializer.startTag(null, "item");
                                xmlSerializer.attribute(null, "value", str5);
                                xmlSerializer.endTag(null, "item");
                            }
                            xmlSerializer.endTag(null, str3);
                            return;
                        }
                    } else if (obj instanceof boolean[]) {
                        boolean[] zArr = (boolean[]) obj;
                        if (zArr != null) {
                            str3 = "boolean-array";
                            xmlSerializer.startTag(null, str3);
                            if (str != null) {
                                xmlSerializer.attribute(null, "name", str);
                            }
                            int length6 = zArr.length;
                            xmlSerializer.attribute(null, "num", Integer.toString(length6));
                            for (boolean z : zArr) {
                                xmlSerializer.startTag(null, "item");
                                xmlSerializer.attribute(null, "value", Boolean.toString(z));
                                xmlSerializer.endTag(null, "item");
                            }
                            xmlSerializer.endTag(null, str3);
                            return;
                        }
                    } else if (obj instanceof Map) {
                        A03(str, (Map) obj, xmlSerializer);
                        return;
                    } else if (obj instanceof List) {
                        List list = (List) obj;
                        if (list != null) {
                            xmlSerializer.startTag(null, "list");
                            if (str != null) {
                                xmlSerializer.attribute(null, "name", str);
                            }
                            int size = list.size();
                            for (int i6 = 0; i6 < size; i6++) {
                                A02(list.get(i6), null, xmlSerializer);
                            }
                            xmlSerializer.endTag(null, "list");
                            return;
                        }
                    } else if (obj instanceof Set) {
                        Set<Object> set = (Set) obj;
                        if (set != null) {
                            str4 = "set";
                            xmlSerializer.startTag(null, str4);
                            if (str != null) {
                                xmlSerializer.attribute(null, "name", str);
                            }
                            for (Object obj2 : set) {
                                A02(obj2, null, xmlSerializer);
                            }
                            xmlSerializer.endTag(null, str4);
                            return;
                        }
                    } else if (!(obj instanceof CharSequence)) {
                        StringBuilder sb2 = new StringBuilder("writeValueXml: unable to write value ");
                        sb2.append(obj);
                        throw new RuntimeException(sb2.toString());
                    }
                    str2 = "null";
                    xmlSerializer.startTag(null, str2);
                }
                xmlSerializer.startTag(null, str4);
                if (str != null) {
                    xmlSerializer.attribute(null, "name", str);
                }
                xmlSerializer.attribute(null, "value", obj.toString());
                xmlSerializer.endTag(null, str4);
                return;
            }
            xmlSerializer.startTag(null, str4);
            if (str != null) {
                xmlSerializer.attribute(null, "name", str);
            }
            xmlSerializer.text(obj.toString());
            xmlSerializer.endTag(null, str4);
            return;
        }
        xmlSerializer.endTag(null, str2);
    }

    public static final void A03(String str, Map map, XmlSerializer xmlSerializer) {
        if (map == null) {
            xmlSerializer.startTag(null, "null");
            xmlSerializer.endTag(null, "null");
            return;
        }
        xmlSerializer.startTag(null, "map");
        if (str != null) {
            xmlSerializer.attribute(null, "name", str);
        }
        for (Map.Entry entry : map.entrySet()) {
            A02(entry.getValue(), (String) entry.getKey(), xmlSerializer);
        }
        xmlSerializer.endTag(null, "map");
    }
}
