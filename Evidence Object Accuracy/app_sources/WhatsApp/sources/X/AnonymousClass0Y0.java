package X;

import android.content.ClipData;
import android.net.Uri;
import android.os.Bundle;

/* renamed from: X.0Y0  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0Y0 implements AbstractC12620iD {
    public int A00;
    public int A01;
    public ClipData A02;
    public Uri A03;
    public Bundle A04;

    public AnonymousClass0Y0(ClipData clipData, int i) {
        this.A02 = clipData;
        this.A01 = i;
    }

    @Override // X.AbstractC12620iD
    public AnonymousClass0SR A6j() {
        return new AnonymousClass0SR(new AnonymousClass0Y2(this));
    }

    @Override // X.AbstractC12620iD
    public void Ac8(int i) {
        this.A00 = i;
    }

    @Override // X.AbstractC12620iD
    public void AcH(Uri uri) {
        this.A03 = uri;
    }

    @Override // X.AbstractC12620iD
    public void setExtras(Bundle bundle) {
        this.A04 = bundle;
    }
}
