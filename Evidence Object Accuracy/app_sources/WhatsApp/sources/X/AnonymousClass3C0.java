package X;

import com.whatsapp.backup.google.RestoreFromBackupActivity;
import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.3C0  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3C0 {
    public final AbstractC15710nm A00;
    public final C14330lG A01;
    public final C18790t3 A02;
    public final C33241dg A03;
    public final C15820nx A04;
    public final C27051Fv A05;
    public final AbstractC44761zV A06;
    public final C18640sm A07;
    public final C15810nw A08;
    public final C16590pI A09;
    public final C15890o4 A0A;
    public final C14820m6 A0B;
    public final C15880o3 A0C;
    public final C14850m9 A0D;
    public final C19930uu A0E;
    public final AbstractC14440lR A0F;
    public final WeakReference A0G;
    public final AtomicBoolean A0H;
    public final AtomicBoolean A0I;

    public AnonymousClass3C0(AbstractC15710nm r2, C14330lG r3, C18790t3 r4, C15820nx r5, RestoreFromBackupActivity restoreFromBackupActivity, C27051Fv r7, AbstractC44761zV r8, C18640sm r9, C15810nw r10, C16590pI r11, C15890o4 r12, C14820m6 r13, C15880o3 r14, C21630xj r15, C14850m9 r16, C19930uu r17, AbstractC14440lR r18, AtomicBoolean atomicBoolean, AtomicBoolean atomicBoolean2) {
        this.A09 = r11;
        this.A0D = r16;
        this.A00 = r2;
        this.A0E = r17;
        this.A0F = r18;
        this.A01 = r3;
        this.A02 = r4;
        this.A08 = r10;
        this.A04 = r5;
        this.A0C = r14;
        this.A05 = r7;
        this.A0A = r12;
        this.A0B = r13;
        this.A0G = C12970iu.A10(restoreFromBackupActivity);
        this.A0I = atomicBoolean;
        this.A0H = atomicBoolean2;
        this.A06 = r8;
        this.A07 = r9;
        this.A03 = new C33241dg(r15);
    }
}
