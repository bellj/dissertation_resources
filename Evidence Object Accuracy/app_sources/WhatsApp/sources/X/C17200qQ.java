package X;

import android.content.SharedPreferences;
import android.os.Build;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONException;

/* renamed from: X.0qQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17200qQ {
    public C29351Rv A00;
    public AbstractC31111a1 A01;
    public AnonymousClass2ST A02;
    public boolean A03;
    public final AbstractC15710nm A04;
    public final C15450nH A05;
    public final C14830m7 A06;
    public final C14820m6 A07;
    public final AnonymousClass122 A08;
    public final AbstractC31111a1 A09;
    public final C14850m9 A0A;
    public final C16120oU A0B;
    public final C16630pM A0C;

    public C17200qQ(AbstractC15710nm r2, C15450nH r3, C14830m7 r4, C14820m6 r5, AnonymousClass122 r6, AnonymousClass124 r7, C14850m9 r8, C16120oU r9, C16630pM r10) {
        this.A06 = r4;
        this.A0A = r8;
        this.A04 = r2;
        this.A0B = r9;
        this.A05 = r3;
        this.A08 = r6;
        this.A07 = r5;
        this.A0C = r10;
        this.A09 = new C31101a0(r2, r3, r7, r10);
    }

    public static final void A00(SharedPreferences sharedPreferences) {
        if (!sharedPreferences.edit().remove("client_static_keypair_enc").commit()) {
            Log.w("AuthKeyStore/failed to clear key pair");
        }
    }

    public final int A01(SharedPreferences sharedPreferences, C29361Rw r5, C29361Rw r6, int i) {
        if (r6 != null && r5 == null && this.A0A.A07(377)) {
            i = 5;
            Log.w("AuthKeyStore/recovering PWD key");
            A0A(sharedPreferences, r6.A02());
            C29361Rw A03 = A03(sharedPreferences);
            sharedPreferences.edit().remove("client_static_keypair_enc_success").remove("client_static_keypair_enc_failed").apply();
            if (A03 == null) {
                throw new RuntimeException("AuthKeyStore/failed to get client static key pair");
            }
        }
        return i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0071, code lost:
        if (A0F(r8.A02()) != false) goto L_0x0073;
     */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x012a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized X.C29351Rv A02() {
        /*
        // Method dump skipped, instructions count: 634
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17200qQ.A02():X.1Rv");
    }

    public final C29361Rw A03(SharedPreferences sharedPreferences) {
        String str;
        String string = sharedPreferences.getString("client_static_keypair_pwd_enc", null);
        if (string != null) {
            try {
                C31091Zz A00 = AnonymousClass122.A00(new JSONArray(string));
                if (A00 != null) {
                    if (A00.A00 != 2) {
                        str = "AuthKeyStore/readClientStaticKeypairEnc 3 not expected type";
                    } else {
                        byte[] A03 = this.A09.A03(EnumC31131a3.READ_ACTIVE, A00);
                        Log.i("AuthKeyStore/readClientStaticKeypairEnc 3");
                        if (A03 != null) {
                            return C29361Rw.A01(A03);
                        }
                        str = "AuthKeyStore/readClientStaticKeypairEnc/failed to read data";
                    }
                    Log.e(str);
                    return null;
                }
            } catch (JSONException unused) {
                return null;
            }
        }
        return null;
    }

    public final C29361Rw A04(EnumC31131a3 r6) {
        C31091Zz A00;
        AbstractC31111a1 r0;
        SharedPreferences A01 = this.A0C.A01("keystore");
        String string = A01.getString("client_static_keypair_enc", null);
        if (!(string == null || (A00 = AnonymousClass122.A00(new JSONArray(string))) == null)) {
            if (A00.A00 != 0) {
                Log.e("AuthKeyStore/readAndroidKeyStoreClientStaticKeypairEnc not supported type");
                A00(A01);
            } else if (Build.VERSION.SDK_INT < 23 || (r0 = this.A01) == null) {
                Log.e("AuthKeyStore/readAndroidKeyStoreClientStaticKeypairEnc/not supported sdk for type");
                A00(A01);
                return null;
            } else {
                byte[] A03 = r0.A03(r6, A00);
                Log.i("AuthKeyStore/readAndroidKeyStoreClientStaticKeypairEnc");
                if (A03 != null) {
                    return C29361Rw.A01(A03);
                }
            }
            Log.e("AuthKeyStore/readAndroidKeyStoreClientStaticKeypairEnc/failed to read data");
        }
        return null;
    }

    public final C29361Rw A05(EnumC31131a3 r6) {
        C29361Rw r4;
        try {
            r4 = A04(r6);
        } catch (JSONException unused) {
            r4 = null;
        }
        if (r4 == null) {
            try {
                return A04(r6);
            } catch (JSONException e) {
                StringBuilder sb = new StringBuilder("EncryptedKeyHelper/reportEncryptedKeyHelperProblem/");
                sb.append("read invalid json");
                Log.e(sb.toString(), e);
            }
        }
        return r4;
    }

    public final C29361Rw A06(boolean z) {
        SharedPreferences A01 = this.A0C.A01("keystore");
        StringBuilder sb = new StringBuilder("AuthKeyStore/generating new client static keypair/store 1 = ");
        sb.append(z);
        Log.i(sb.toString());
        C29361Rw A00 = C29361Rw.A00();
        byte[] A02 = A00.A02();
        if (!z || !A0F(A02)) {
            A0A(A01, A02);
        }
        this.A07.A00.edit().putInt("connection_lc", 0).apply();
        return A00;
    }

    public final void A07() {
        C31121a2 r0;
        if (this.A01 == null) {
            if (Build.VERSION.SDK_INT >= 23) {
                C16630pM r5 = this.A0C;
                SharedPreferences A01 = r5.A01("keystore");
                C14850m9 r3 = this.A0A;
                String A03 = r3.A03(388);
                StringBuilder sb = new StringBuilder();
                sb.append(Build.MANUFACTURER);
                sb.append(";");
                String obj = sb.toString();
                if (A03 == null || !A03.contains(obj) || !A01.contains("client_static_keypair_pwd_enc")) {
                    r0 = new C31121a2(this.A04, this.A05, r3, r5);
                    this.A01 = r0;
                }
            }
            r0 = null;
            this.A01 = r0;
        }
    }

    public synchronized void A08() {
        A07();
        Log.i("clearing client static key pair");
        boolean commit = this.A0C.A01("keystore").edit().remove("client_static_keypair_enc").remove("client_static_keypair_pwd_enc").commit();
        AbstractC31111a1 r0 = this.A01;
        if (r0 != null) {
            r0.A01();
        }
        this.A09.A01();
        this.A00 = null;
        if (!commit) {
            throw new RuntimeException("unable to clear client static keypair");
        }
    }

    public void A09(int i) {
        if (this.A0A.A07(1689)) {
            SharedPreferences A01 = this.A0C.A01("keystore");
            if (i > 5) {
                i = 5;
            } else if (i < 0) {
                i = 0;
            }
            A01.edit().putInt("remaining_auth_key_rotation_attempts", i).apply();
        }
    }

    public final void A0A(SharedPreferences sharedPreferences, byte[] bArr) {
        if (!A0C(sharedPreferences, bArr) && !A0C(sharedPreferences, bArr)) {
            throw new RuntimeException("unable to write client static keypair");
        }
    }

    public final boolean A0B(SharedPreferences sharedPreferences, AbstractC31111a1 r6, byte[] bArr) {
        C29361Rw A05;
        if (!A0D(r6.A00(bArr), "client_static_keypair_enc") || (A05 = A05(EnumC31131a3.READ_SELFTEST)) == null || !Arrays.equals(bArr, A05.A02())) {
            RuntimeException runtimeException = new RuntimeException();
            StringBuilder sb = new StringBuilder("EncryptedKeyHelper/reportEncryptedKeyHelperProblem/");
            sb.append("failed to store and read correct key");
            Log.e(sb.toString(), runtimeException);
            A00(sharedPreferences);
            return false;
        }
        Log.i("AuthKeyStore/storeAndCanReadAndroidKeyStoreKey/1");
        return true;
    }

    public final boolean A0C(SharedPreferences sharedPreferences, byte[] bArr) {
        C29361Rw A03;
        if (!A0D(this.A09.A00(bArr), "client_static_keypair_pwd_enc") || (A03 = A03(sharedPreferences)) == null || !Arrays.equals(bArr, A03.A02())) {
            RuntimeException runtimeException = new RuntimeException();
            StringBuilder sb = new StringBuilder("EncryptedKeyHelper/reportEncryptedKeyHelperProblem/");
            sb.append("failed to store and read correct key");
            Log.e(sb.toString(), runtimeException);
            return false;
        }
        Log.i("AuthKeyStore/storedAndCanRead/3");
        return true;
    }

    public final boolean A0D(C31091Zz r3, String str) {
        String A00;
        SharedPreferences A01 = this.A0C.A01("keystore");
        if (r3 == null || (A00 = r3.A00()) == null) {
            Log.e("AuthKeyStore/failed to store clientStaticKeypair/cant generate json");
            return false;
        }
        boolean commit = A01.edit().putString(str, A00).commit();
        if (!commit) {
            Log.e("AuthKeyStore/failed to store clientStaticKeypair");
        }
        return commit;
    }

    public final boolean A0E(C29361Rw r4) {
        try {
            A0A(this.A0C.A01("keystore"), r4.A02());
            return true;
        } catch (RuntimeException e) {
            StringBuilder sb = new StringBuilder("authkeystore/overwriteExistingKeypairPwd: ");
            sb.append(e.toString());
            Log.e(sb.toString());
            return false;
        }
    }

    public final boolean A0F(byte[] bArr) {
        SharedPreferences A01 = this.A0C.A01("keystore");
        if (!TextUtils.isEmpty(A01.getString("client_static_keypair_enc", null))) {
            return false;
        }
        AbstractC31111a1 r0 = this.A01;
        if (r0 != null && (A0B(A01, r0, bArr) || A0B(A01, this.A01, bArr))) {
            return true;
        }
        Log.w("AuthKeyStore/ensureEncKeyStored/failed to use enc csk");
        return false;
    }
}
