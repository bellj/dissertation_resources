package X;

import android.util.Pair;
import com.whatsapp.stickers.WebpUtils;
import com.whatsapp.util.Log;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;

/* renamed from: X.3J1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3J1 {
    public static int A00(AnonymousClass4QM r4, C41471ta r5, File file, String str, URL url) {
        String str2 = r5.A0E;
        if (str2 == null) {
            Log.w(C12960it.A0d(r5.A0F, C12960it.A0k("MediaDownload/checkMediaHash/message-supplied media hash is null mediaHash=")));
            str2 = r4.A01;
        }
        if (str == null) {
            StringBuilder A0k = C12960it.A0k("MediaDownload/MMS download failed to calculate hash; mediaHash=");
            C12980iv.A1Q(url, r5.A0F, "; url=", A0k);
            A0k.append("; downloadFile=");
            A0k.append(file.getAbsolutePath());
            A0k.append("; downloadFile.exists?=");
            A0k.append(file.exists());
            Log.w(A0k.toString());
            return 1;
        } else if (str.equals(str2)) {
            return 0;
        } else {
            StringBuilder A0k2 = C12960it.A0k("MediaDownload/MMS download failed due to hash mismatch; mediaHash=");
            C12980iv.A1Q(url, r5.A0F, "; url=", A0k2);
            A0k2.append("; receivedHash=");
            A0k2.append(str2);
            A0k2.append("; localHash=");
            Log.w(C12960it.A0d(str, A0k2));
            return 7;
        }
    }

    public static int A01(C41471ta r7, String str, URL url) {
        if (str == null) {
            StringBuilder A0k = C12960it.A0k("MediaDownload/MMS download failed during media decryption due to plaintext hash not calculated properly; mediaHash=");
            String str2 = r7.A0F;
            C12980iv.A1Q(url, str2, "; url=", A0k);
            C12960it.A1L("; mediaHash=", str2, "; calculatedHash=", A0k);
            A0k.append(str);
            A0k.append("; mediaSize=");
            Log.w(C12970iu.A0w(A0k, r7.A07));
            return 1;
        }
        String str3 = r7.A0F;
        if (str.equals(str3)) {
            return 0;
        }
        StringBuilder A0k2 = C12960it.A0k("MediaDownload/MMS download failed during media decryption due to plaintext hash mismatch; mediaHash=");
        C12980iv.A1Q(url, str3, "; url=", A0k2);
        C12960it.A1L("; mediaHash=", str3, "; calculatedHash=", A0k2);
        A0k2.append(str);
        A0k2.append("; mediaSize=");
        Log.w(C12970iu.A0w(A0k2, r7.A07));
        return 2;
    }

    public static void A02(AbstractC15710nm r24, C14330lG r25, C20870wS r26, C37891nB r27, C28921Pn r28, C28781Oz r29, C41471ta r30, C22590zK r31, C26511Dt r32, C26521Du r33, File file, int i, int i2) {
        File A01;
        Throwable e;
        StringBuilder sb;
        String str;
        byte[] bArr = r30.A0X;
        String str2 = r30.A0F;
        String str3 = r30.A0J;
        int i3 = r30.A00;
        long j = r30.A07;
        C14370lK r3 = r30.A09;
        if (i3 > 0 && bArr != null) {
            C14370lK r2 = C14370lK.A0S;
            if (r3 == r2) {
                File A0O = r25.A0O(str2, str3);
                if (A0O != null && !A0O.exists()) {
                    try {
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(i3);
                        if (C22190yg.A03(r26, r27, file, A0O, byteArrayOutputStream, str2, bArr, i2, i3, 16, j)) {
                            WebpUtils.createFirstThumbnail(byteArrayOutputStream.toByteArray(), i3, A0O.getAbsolutePath());
                            if (A0O.exists()) {
                                A0O.length();
                                byteArrayOutputStream.close();
                            }
                        }
                        byteArrayOutputStream.close();
                        return;
                    } catch (IOException e2) {
                        Log.e(C12960it.A0d(str2, C12960it.A0k("CreateStickerThumbnail failed; mediaHash=")), e2);
                        return;
                    }
                } else {
                    return;
                }
            } else {
                File A012 = C14330lG.A01(r25.A02.A07(".Thumbs"), str2, str3, ".prog.thumb.jpg");
                if (A012 != null && !A012.exists()) {
                    try {
                        FileOutputStream fileOutputStream = new FileOutputStream(A012);
                        try {
                            if (C22190yg.A03(r26, r27, file, A012, fileOutputStream, str2, bArr, i2, i3, 2, j)) {
                                fileOutputStream.write(C22190yg.A09);
                                if (A012.exists()) {
                                    A012.length();
                                    fileOutputStream.close();
                                }
                            }
                            fileOutputStream.close();
                            return;
                        } catch (Throwable th) {
                            try {
                                fileOutputStream.close();
                            } catch (Throwable unused) {
                            }
                            throw th;
                        }
                    } catch (FileNotFoundException e3) {
                        e = e3;
                        sb = C12960it.A0h();
                        str = "CreateProgressiveThumbnail failed could not find file progressive jpeg thumbnail; mediaHash=";
                        sb.append(str);
                        Log.e(C12960it.A0d(str2, sb), e);
                        return;
                    } catch (IOException e4) {
                        e = e4;
                        sb = C12960it.A0h();
                        str = "CreateProgressiveThumbnail failed;mediaHash=";
                        sb.append(str);
                        Log.e(C12960it.A0d(str2, sb), e);
                        return;
                    }
                } else {
                    return;
                }
            }
            if (r3 == C14370lK.A08 || r3 == C14370lK.A0T || r3 == r2) {
                File A0O2 = r25.A0O(str2, str3);
                if (A0O2 != null) {
                    A03(r24, r29, r30, r31, r32, r33, A0O2);
                }
            } else {
                boolean z = r30.A0U;
                if (AnonymousClass14P.A01(r3) && z && !r30.A0N && r29.A0G() == null && (A01 = C14330lG.A01(r25.A02.A07(".Thumbs"), str2, str3, ".prog.thumb.jpg")) != null) {
                    try {
                        BufferedInputStream A0h = C12990iw.A0h(A01);
                        r29.A0F(AnonymousClass3A5.A00(A0h));
                        A0h.close();
                    } catch (IOException e5) {
                        Log.e("MediaDownload/createProgressiveThumbnail/created progressive/thumbnail could not be read", e5);
                    }
                }
            }
            r28.A08();
            if (r3 == r2 && i == 2) {
                r29.A05();
            }
        }
    }

    public static void A03(AbstractC15710nm r4, C28781Oz r5, C41471ta r6, C22590zK r7, C26511Dt r8, C26521Du r9, File file) {
        C14370lK r3;
        C39901qj A00;
        if ((r5.A00() != 1 || !AnonymousClass14P.A02(r6.A09)) && (r3 = r6.A09) != C14370lK.A0P && (A00 = new C39891qi(r4, r7, r8, r9).A00(new C39881qh(r3, file, r6.A0H, r6.A0N))) != null) {
            byte[] bArr = A00.A02;
            if (bArr != null) {
                r5.A0F(bArr);
            }
            Pair pair = A00.A01;
            if (pair != null) {
                r5.A0A(C12960it.A05(pair.first));
                r5.A08(C12960it.A05(pair.second));
            }
            Pair pair2 = A00.A00;
            if (pair2 != null) {
                int A05 = C12960it.A05(pair2.first);
                synchronized (r5) {
                    r5.A09 = Integer.valueOf(A05);
                }
                int A052 = C12960it.A05(pair2.second);
                synchronized (r5) {
                    r5.A0A = Integer.valueOf(A052);
                }
            }
            byte[] bArr2 = A00.A03;
            synchronized (r5) {
                r5.A0K = bArr2;
            }
        }
    }

    public static void A04(C14330lG r2, AnonymousClass1RN r3, File file, File file2) {
        int i = r3.A00;
        if (i == 0 && !file.equals(file2)) {
            file.delete();
        } else if (i == 1) {
            file.delete();
            C28921Pn.A02(r2, file2);
        }
    }
}
