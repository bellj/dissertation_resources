package X;

import android.os.Message;
import com.facebook.redex.RunnableBRunnable0Shape7S0200000_I0_7;
import com.whatsapp.util.Log;
import java.util.Set;

/* renamed from: X.13Z  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass13Z implements AbstractC15920o8 {
    public final C18850tA A00;
    public final C16490p7 A01;
    public final C20710wC A02;
    public final C20660w7 A03;
    public final C22950zu A04;
    public final AbstractC14440lR A05;

    @Override // X.AbstractC15920o8
    public int[] ADF() {
        return new int[]{8};
    }

    public AnonymousClass13Z(C18850tA r1, C16490p7 r2, C20710wC r3, C20660w7 r4, C22950zu r5, AbstractC14440lR r6) {
        this.A05 = r6;
        this.A03 = r4;
        this.A00 = r1;
        this.A02 = r3;
        this.A04 = r5;
        this.A01 = r2;
    }

    @Override // X.AbstractC15920o8
    public boolean AI8(Message message, int i) {
        String str;
        boolean contains;
        if (8 != i) {
            return false;
        }
        C47882Dc r4 = (C47882Dc) message.obj;
        String str2 = r4.A01;
        if ("groups".equals(str2)) {
            if (this.A01.A00) {
                Log.i("DirtyBitHandler/onGroupsDirty call refetchGroups");
                C20710wC r2 = this.A02;
                r2.A03.set(true);
                if (r2.A0z) {
                    return true;
                }
                r2.A0F(3, true, true);
                return true;
            }
            str = "DirtyBitHandler/onGroupsDirty/no-db-access/skip";
        } else if ("account_sync".equals(str2)) {
            if (this.A01.A00) {
                C22950zu r3 = this.A04;
                Set set = r4.A02;
                C453921k r1 = new C453921k();
                if (set.isEmpty()) {
                    contains = true;
                    r1.A05 = true;
                    r1.A03 = true;
                    r1.A04 = true;
                    r1.A02 = true;
                } else {
                    r1.A02 = set.contains("device");
                    r1.A05 = set.contains("status");
                    r1.A03 = set.contains("picture");
                    r1.A04 = set.contains("privacy");
                    contains = set.contains("blocklist");
                }
                r1.A01 = contains;
                r3.A00(new C454021l(r1), true, false, true);
                return true;
            }
            str = "DirtyBitHandler/onAccountDirty/no-db-access/skip";
        } else if ("syncd_app_state".equals(str2)) {
            Log.i("OnDirtyMessageHandler/onSyncDDirty");
            this.A05.Ab2(new RunnableBRunnable0Shape7S0200000_I0_7(this, 2, r4));
            return true;
        } else {
            StringBuilder sb = new StringBuilder("OnDirtyMessageHandler/onDirty received unknown dirty bit category: ");
            sb.append(str2);
            Log.w(sb.toString());
            this.A03.A0H(str2, null);
            return true;
        }
        Log.i(str);
        return true;
    }
}
