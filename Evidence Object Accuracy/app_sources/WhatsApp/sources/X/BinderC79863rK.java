package X;

import android.os.IInterface;

/* renamed from: X.3rK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class BinderC79863rK extends AbstractBinderC73293fz implements IInterface {
    public final /* synthetic */ AbstractC115735Ss A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public BinderC79863rK(AbstractC115735Ss r2) {
        super("com.google.android.gms.maps.internal.IOnMapLoadedCallback");
        this.A00 = r2;
    }
}
