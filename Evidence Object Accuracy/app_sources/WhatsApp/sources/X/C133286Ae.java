package X;

import com.whatsapp.payments.ui.BrazilPayBloksActivity;
import java.util.Map;

/* renamed from: X.6Ae  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133286Ae implements AnonymousClass6MT {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AnonymousClass3FE A01;
    public final /* synthetic */ BrazilPayBloksActivity A02;
    public final /* synthetic */ Map A03;

    public C133286Ae(AnonymousClass3FE r1, BrazilPayBloksActivity brazilPayBloksActivity, Map map, int i) {
        this.A02 = brazilPayBloksActivity;
        this.A01 = r1;
        this.A00 = i;
        this.A03 = map;
    }

    @Override // X.AnonymousClass6MT
    public void ARg(C452120p r6) {
        int i;
        int i2 = r6.A00;
        if (i2 != 1448 || (i = this.A00) >= 1) {
            AbstractActivityC121705jc.A0l(this.A01, null, i2);
        } else {
            this.A02.A2r(this.A01, this.A03, i + 1);
        }
    }

    @Override // X.AnonymousClass6MT
    public void ARh(C460524g r3) {
        if ("COMPLETED".equals(r3.A02)) {
            C21860y6 r1 = ((AbstractActivityC121705jc) this.A02).A09;
            r1.A06(r1.A01("kyc"));
            C117315Zl.A0T(this.A01);
        }
    }
}
