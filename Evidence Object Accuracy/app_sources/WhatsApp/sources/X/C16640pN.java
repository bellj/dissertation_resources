package X;

/* renamed from: X.0pN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C16640pN {
    public static void A00(Class cls, Object obj) {
        if (obj == null) {
            StringBuilder sb = new StringBuilder();
            sb.append(cls.getCanonicalName());
            sb.append(" must be set");
            throw new IllegalStateException(sb.toString());
        }
    }
}
