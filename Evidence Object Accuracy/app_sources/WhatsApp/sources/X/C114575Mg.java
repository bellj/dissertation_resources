package X;

/* renamed from: X.5Mg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114575Mg extends AnonymousClass1TM {
    public AnonymousClass5NG A00;
    public AnonymousClass5NG A01;
    public AbstractC114775Na A02;
    public AnonymousClass5MA A03;
    public AnonymousClass5MA A04;
    public AnonymousClass5N2 A05;
    public AnonymousClass5N2 A06;
    public C114725Mv A07;
    public AnonymousClass5MX A08;
    public C114515Ma A09;
    public C114765Mz A0A;
    public C114765Mz A0B;

    /* JADX WARNING: Removed duplicated region for block: B:11:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x010c A[ORIG_RETURN, RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C114575Mg(X.AbstractC114775Na r8) {
        /*
        // Method dump skipped, instructions count: 276
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C114575Mg.<init>(X.5Na):void");
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        if (C94664cJ.A00("org.spongycastle.x509.allow_non-der_tbscert") == null || C94664cJ.A01("org.spongycastle.x509.allow_non-der_tbscert")) {
            return this.A02;
        }
        C94954co r4 = new C94954co(10);
        AnonymousClass5NG r1 = this.A01;
        if (!r1.A0C(C88784Hc.A04)) {
            C94954co.A02(r1, r4, 0, true);
        }
        r4.A06(this.A00);
        r4.A06(this.A07);
        r4.A06(this.A05);
        C94954co r12 = new C94954co(2);
        r12.A06(this.A0B);
        r4.A06(C94954co.A01(this.A0A, r12));
        AnonymousClass1TN r0 = this.A06;
        if (r0 == null) {
            r0 = new AnonymousClass5NZ();
        }
        r4.A06(r0);
        r4.A06(this.A09);
        AnonymousClass5MA r02 = this.A03;
        if (r02 != null) {
            C94954co.A02(r02, r4, 1, false);
        }
        AnonymousClass5MA r03 = this.A04;
        if (r03 != null) {
            C94954co.A02(r03, r4, 2, false);
        }
        AnonymousClass5MX r13 = this.A08;
        if (r13 != null) {
            C94954co.A02(r13, r4, 3, true);
        }
        return new AnonymousClass5NZ(r4);
    }
}
