package X;

/* renamed from: X.39C  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass39C extends AnonymousClass2B2 {
    public Integer A00;
    public boolean A01;
    public final long A02;
    public final long A03;
    public final long A04;
    public final C14830m7 A05;
    public final C16120oU A06;
    public final AnonymousClass4Xy A07 = new AnonymousClass4Xy();
    public final AnonymousClass4Xy A08 = new AnonymousClass4Xy();

    public AnonymousClass39C(C14830m7 r2, C16120oU r3, Integer num, int i, int i2, long j, long j2, long j3) {
        super(i2, num.intValue());
        this.A05 = r2;
        this.A06 = r3;
        this.A03 = j;
        this.A00 = Integer.valueOf(i);
        this.A04 = j2;
        this.A02 = j3;
    }
}
