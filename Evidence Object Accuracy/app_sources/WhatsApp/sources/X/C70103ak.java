package X;

import com.whatsapp.conversationslist.ConversationsFragment;
import com.whatsapp.util.Log;
import java.util.Collections;

/* renamed from: X.3ak  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70103ak implements AnonymousClass5WL {
    public final /* synthetic */ ConversationsFragment.DeleteBroadcastListDialogFragment A00;
    public final /* synthetic */ C15370n3 A01;

    public C70103ak(ConversationsFragment.DeleteBroadcastListDialogFragment deleteBroadcastListDialogFragment, C15370n3 r2) {
        this.A00 = deleteBroadcastListDialogFragment;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass5WL
    public void ASv() {
        this.A00.A1B();
    }

    @Override // X.AnonymousClass5WL
    public void ATy(boolean z) {
        Log.i("conversations/delete-list");
        ConversationsFragment.DeleteBroadcastListDialogFragment deleteBroadcastListDialogFragment = this.A00;
        deleteBroadcastListDialogFragment.A1B();
        C15370n3 r0 = this.A01;
        C63063Ac.A00((ActivityC13810kN) deleteBroadcastListDialogFragment.A0B(), deleteBroadcastListDialogFragment.A00, deleteBroadcastListDialogFragment.A03, deleteBroadcastListDialogFragment.A04, deleteBroadcastListDialogFragment.A07, Collections.singletonList(r0), z);
    }
}
