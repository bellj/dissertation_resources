package X;

import android.content.Context;
import com.whatsapp.twofactor.SettingsTwoFactorAuthActivity;

/* renamed from: X.4r5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103644r5 implements AbstractC009204q {
    public final /* synthetic */ SettingsTwoFactorAuthActivity A00;

    public C103644r5(SettingsTwoFactorAuthActivity settingsTwoFactorAuthActivity) {
        this.A00 = settingsTwoFactorAuthActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
