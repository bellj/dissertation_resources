package X;

import android.graphics.SurfaceTexture;
import android.opengl.EGL14;
import android.opengl.EGLConfig;
import android.opengl.EGLContext;
import android.opengl.EGLDisplay;
import android.opengl.EGLExt;
import android.opengl.EGLSurface;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.os.SystemClock;
import android.util.Log;
import android.view.Surface;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

/* renamed from: X.637  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass637 implements SurfaceTexture.OnFrameAvailableListener {
    public static final FloatBuffer A0S;
    public static final FloatBuffer A0T;
    public static final float[] A0U;
    public static final float[] A0V;
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public long A09;
    public SurfaceTexture A0A;
    public SurfaceTexture A0B;
    public EGLConfig A0C;
    public EGLContext A0D;
    public EGLDisplay A0E;
    public EGLSurface A0F;
    public boolean A0G;
    public final Object A0H;
    public final Object A0I;
    public final float[] A0J;
    public final float[] A0K;
    public final float[] A0L;
    public final float[] A0M;
    public final int[] A0N = new int[2];
    public volatile int A0O;
    public volatile int A0P;
    public volatile EGLSurface A0Q;
    public volatile EGLSurface A0R;

    static {
        float[] fArr = {-1.0f, -1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f};
        A0U = fArr;
        float[] fArr2 = {0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f};
        A0V = fArr2;
        ByteBuffer allocateDirect = ByteBuffer.allocateDirect(32);
        allocateDirect.order(ByteOrder.nativeOrder());
        FloatBuffer asFloatBuffer = allocateDirect.asFloatBuffer();
        asFloatBuffer.put(fArr);
        asFloatBuffer.position(0);
        A0T = asFloatBuffer;
        ByteBuffer allocateDirect2 = ByteBuffer.allocateDirect(32);
        allocateDirect2.order(ByteOrder.nativeOrder());
        FloatBuffer asFloatBuffer2 = allocateDirect2.asFloatBuffer();
        asFloatBuffer2.put(fArr2);
        asFloatBuffer2.position(0);
        A0S = asFloatBuffer2;
    }

    public AnonymousClass637() {
        EGLConfig eGLConfig;
        int A00;
        float[] fArr = new float[16];
        this.A0L = fArr;
        float[] fArr2 = new float[16];
        this.A0M = fArr2;
        float[] fArr3 = new float[16];
        this.A0J = fArr3;
        float[] fArr4 = new float[16];
        this.A0K = fArr4;
        this.A03 = -1;
        this.A0Q = EGL14.EGL_NO_SURFACE;
        this.A0R = EGL14.EGL_NO_SURFACE;
        this.A0H = C12970iu.A0l();
        this.A0I = C12970iu.A0l();
        this.A09 = 0;
        Matrix.setIdentityM(fArr, 0);
        Matrix.setIdentityM(fArr2, 0);
        Matrix.setIdentityM(fArr3, 0);
        Matrix.setIdentityM(fArr4, 0);
        EGLDisplay eglGetDisplay = EGL14.eglGetDisplay(0);
        this.A0E = eglGetDisplay;
        if (eglGetDisplay != EGL14.EGL_NO_DISPLAY) {
            int[] iArr = new int[2];
            if (EGL14.eglInitialize(eglGetDisplay, iArr, 0, iArr, 1)) {
                EGLConfig[] eGLConfigArr = new EGLConfig[1];
                if (!EGL14.eglChooseConfig(this.A0E, new int[]{12324, 8, 12323, 8, 12322, 8, 12321, 8, 12352, 4, 12610, 1, 12344}, 0, eGLConfigArr, 0, 1, new int[1], 0) || (eGLConfig = eGLConfigArr[0]) == null) {
                    throw C12990iw.A0m("Unable to find a suitable EGLConfig");
                }
                this.A0C = eGLConfig;
                this.A0D = EGL14.eglCreateContext(this.A0E, eGLConfig, EGL14.EGL_NO_CONTEXT, new int[]{12440, 2, 12344}, 0);
                int eglGetError = EGL14.eglGetError();
                if (eglGetError != 12288 || this.A0D == null) {
                    throw C12990iw.A0m(C12960it.A0d(Integer.toHexString(eglGetError), C12960it.A0k("eglCreateContext: EGL error: 0x")));
                }
                EGLSurface eglCreatePbufferSurface = EGL14.eglCreatePbufferSurface(this.A0E, this.A0C, new int[]{12375, 1, 12374, 1, 12344}, 0);
                this.A0F = eglCreatePbufferSurface;
                if (eglCreatePbufferSurface == null || EGL14.eglGetError() != 12288) {
                    this.A0F = EGL14.EGL_NO_SURFACE;
                }
                EGLDisplay eGLDisplay = this.A0E;
                EGLSurface eGLSurface = this.A0F;
                EGL14.eglMakeCurrent(eGLDisplay, eGLSurface, eGLSurface, this.A0D);
                int A002 = A00(35633, "precision mediump float;\nuniform mat4 uSurfaceTransformMatrix;\nuniform mat4 uSceneTransformMatrix;\nuniform mat4 uVideoTransformMatrix;\n\nattribute vec4 aPosition;\nattribute vec4 aTextureCoord;\n\nvarying vec2 vTextureCoord;\n\nvoid main() {\n  gl_Position = uSceneTransformMatrix * aPosition;\n  vTextureCoord = (uSurfaceTransformMatrix * uVideoTransformMatrix * aTextureCoord).xy;\n}\n");
                if (A002 != 0 && (A00 = A00(35632, "#extension GL_OES_EGL_image_external : require\n\nprecision mediump float;\nvarying vec2 vTextureCoord;\nuniform samplerExternalOES sTexture;\n\nvoid main() {\n    gl_FragColor = texture2D(sTexture, vTextureCoord);\n}\n")) != 0) {
                    this.A02 = GLES20.glCreateProgram();
                    A02("glCreateProgram");
                    if (this.A02 == 0) {
                        Log.e("GLSurfacePipe", "Could not create program");
                    }
                    GLES20.glAttachShader(this.A02, A002);
                    A02("glAttachShader");
                    GLES20.glAttachShader(this.A02, A00);
                    A02("glAttachShader");
                    GLES20.glLinkProgram(this.A02);
                    int[] iArr2 = new int[1];
                    GLES20.glGetProgramiv(this.A02, 35714, iArr2, 0);
                    if (iArr2[0] != 1) {
                        Log.e("GLSurfacePipe", "Could not link program: ");
                        Log.e("GLSurfacePipe", GLES20.glGetProgramInfoLog(this.A02));
                        GLES20.glDeleteProgram(this.A02);
                        this.A02 = 0;
                        return;
                    }
                    int glGetAttribLocation = GLES20.glGetAttribLocation(this.A02, "aPosition");
                    this.A04 = glGetAttribLocation;
                    A01(glGetAttribLocation, "aPosition");
                    int glGetAttribLocation2 = GLES20.glGetAttribLocation(this.A02, "aTextureCoord");
                    this.A05 = glGetAttribLocation2;
                    A01(glGetAttribLocation2, "aTextureCoord");
                    int glGetUniformLocation = GLES20.glGetUniformLocation(this.A02, "uSurfaceTransformMatrix");
                    this.A07 = glGetUniformLocation;
                    A01(glGetUniformLocation, "uSurfaceTransformMatrix");
                    int glGetUniformLocation2 = GLES20.glGetUniformLocation(this.A02, "uVideoTransformMatrix");
                    this.A08 = glGetUniformLocation2;
                    A01(glGetUniformLocation2, "uVideoTransformMatrix");
                    int glGetUniformLocation3 = GLES20.glGetUniformLocation(this.A02, "uSceneTransformMatrix");
                    this.A06 = glGetUniformLocation3;
                    A01(glGetUniformLocation3, "uSceneTransformMatrix");
                    return;
                }
                return;
            }
            this.A0E = EGL14.EGL_NO_DISPLAY;
            throw C12990iw.A0m("unable to initialize EGL14");
        }
        throw C12990iw.A0m("unable to get EGL14 display");
    }

    public static int A00(int i, String str) {
        int glCreateShader = GLES20.glCreateShader(i);
        A02(C12960it.A0W(i, "glCreateShader type="));
        GLES20.glShaderSource(glCreateShader, str);
        GLES20.glCompileShader(glCreateShader);
        int[] iArr = new int[1];
        GLES20.glGetShaderiv(glCreateShader, 35713, iArr, 0);
        if (iArr[0] != 0) {
            return glCreateShader;
        }
        StringBuilder A0k = C12960it.A0k("Could not compile shader ");
        A0k.append(i);
        Log.e("GLSurfacePipe", C12960it.A0d(":", A0k));
        Log.e("GLSurfacePipe", C12960it.A0d(GLES20.glGetShaderInfoLog(glCreateShader), C12960it.A0k(" ")));
        GLES20.glDeleteShader(glCreateShader);
        return 0;
    }

    public static void A01(int i, String str) {
        if (i < 0) {
            StringBuilder A0k = C12960it.A0k("Unable to locate '");
            A0k.append(str);
            throw C12990iw.A0m(C12960it.A0d("' in program", A0k));
        }
    }

    public static void A02(String str) {
        int glGetError = GLES20.glGetError();
        if (glGetError != 0) {
            StringBuilder A0j = C12960it.A0j(str);
            A0j.append(": glError 0x");
            String A0d = C12960it.A0d(Integer.toHexString(glGetError), A0j);
            Log.e("GLSurfacePipe", A0d);
            throw C12990iw.A0m(A0d);
        }
    }

    public SurfaceTexture A03(int i, int i2) {
        SurfaceTexture surfaceTexture = this.A0A;
        if (surfaceTexture != null && this.A01 == i && this.A00 == i2) {
            return surfaceTexture;
        }
        this.A01 = i;
        this.A00 = i2;
        if (surfaceTexture != null) {
            surfaceTexture.setOnFrameAvailableListener(null);
            this.A0A.release();
            this.A0A = null;
        }
        int i3 = this.A03;
        if (i3 != -1) {
            GLES20.glDeleteTextures(1, new int[]{i3}, 0);
            this.A03 = -1;
        }
        int[] iArr = new int[1];
        GLES20.glGenTextures(1, iArr, 0);
        A02("glGenTextures");
        int i4 = iArr[0];
        this.A03 = i4;
        GLES20.glBindTexture(36197, i4);
        A02(C12960it.A0f(C12960it.A0k("glBindTexture "), this.A03));
        GLES20.glTexParameterf(36197, 10241, 9729.0f);
        GLES20.glTexParameterf(36197, 10240, 9729.0f);
        GLES20.glTexParameteri(36197, 10242, 33071);
        GLES20.glTexParameteri(36197, 10243, 33071);
        A02("glTexParameter");
        SurfaceTexture surfaceTexture2 = new SurfaceTexture(this.A03);
        this.A0A = surfaceTexture2;
        surfaceTexture2.setOnFrameAvailableListener(this);
        return this.A0A;
    }

    public void A04() {
        if (this.A0E != EGL14.EGL_NO_DISPLAY) {
            synchronized (this.A0H) {
                if (this.A0Q != EGL14.EGL_NO_SURFACE) {
                    try {
                        EGL14.eglMakeCurrent(this.A0E, this.A0Q, this.A0Q, this.A0D);
                        EGLDisplay eGLDisplay = this.A0E;
                        EGLSurface eGLSurface = this.A0Q;
                        int[] iArr = this.A0N;
                        EGL14.eglQuerySurface(eGLDisplay, eGLSurface, 12375, iArr, 0);
                        EGL14.eglQuerySurface(this.A0E, this.A0Q, 12374, iArr, 1);
                        GLES20.glViewport(0, 0, iArr[0], iArr[1]);
                        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
                        GLES20.glClear(16384);
                        EGL14.eglSwapBuffers(this.A0E, this.A0Q);
                    } catch (RuntimeException unused) {
                    }
                    EGLDisplay eGLDisplay2 = this.A0E;
                    EGLSurface eGLSurface2 = this.A0F;
                    EGL14.eglMakeCurrent(eGLDisplay2, eGLSurface2, eGLSurface2, this.A0D);
                    EGL14.eglDestroySurface(this.A0E, this.A0Q);
                    this.A0Q = EGL14.EGL_NO_SURFACE;
                }
                this.A0B = null;
            }
            synchronized (this.A0I) {
                if (this.A0R != EGL14.EGL_NO_SURFACE) {
                    EGL14.eglDestroySurface(this.A0E, this.A0R);
                    this.A0R = EGL14.EGL_NO_SURFACE;
                }
            }
            EGLSurface eGLSurface3 = this.A0F;
            if (eGLSurface3 != EGL14.EGL_NO_SURFACE) {
                EGL14.eglDestroySurface(this.A0E, eGLSurface3);
            }
            int i = this.A03;
            if (i != -1) {
                GLES20.glDeleteTextures(1, new int[]{i}, 0);
            }
            int i2 = this.A02;
            if (i2 != 0) {
                GLES20.glDeleteProgram(i2);
            }
            EGLDisplay eGLDisplay3 = this.A0E;
            EGLSurface eGLSurface4 = EGL14.EGL_NO_SURFACE;
            EGL14.eglMakeCurrent(eGLDisplay3, eGLSurface4, eGLSurface4, EGL14.EGL_NO_CONTEXT);
            EGL14.eglDestroyContext(this.A0E, this.A0D);
            EGL14.eglReleaseThread();
            EGL14.eglTerminate(this.A0E);
        }
        this.A0E = EGL14.EGL_NO_DISPLAY;
        this.A0D = EGL14.EGL_NO_CONTEXT;
        this.A0C = null;
        this.A0F = EGL14.EGL_NO_SURFACE;
        this.A02 = 0;
        this.A03 = -1;
        SurfaceTexture surfaceTexture = this.A0A;
        if (surfaceTexture != null) {
            surfaceTexture.release();
            this.A0A = null;
        }
    }

    public void A05(SurfaceTexture surfaceTexture, int i) {
        if (this.A0E != EGL14.EGL_NO_DISPLAY) {
            synchronized (this.A0H) {
                this.A0O = i;
                if (this.A0B == surfaceTexture) {
                    float[] fArr = this.A0J;
                    Matrix.setIdentityM(fArr, 0);
                    Matrix.rotateM(fArr, 0, (float) i, 0.0f, 0.0f, 1.0f);
                } else {
                    if (this.A0Q != EGL14.EGL_NO_SURFACE) {
                        EGL14.eglDestroySurface(this.A0E, this.A0Q);
                        Matrix.setIdentityM(this.A0J, 0);
                        this.A0Q = EGL14.EGL_NO_SURFACE;
                    }
                    this.A0B = surfaceTexture;
                    if (surfaceTexture != null) {
                        float[] fArr2 = this.A0J;
                        Matrix.setIdentityM(fArr2, 0);
                        Matrix.rotateM(fArr2, 0, (float) i, 0.0f, 0.0f, 1.0f);
                        this.A0Q = EGL14.eglCreateWindowSurface(this.A0E, this.A0C, surfaceTexture, new int[]{12344}, 0);
                        if (this.A0Q == null || EGL14.eglGetError() != 12288) {
                            this.A0Q = EGL14.EGL_NO_SURFACE;
                        }
                    }
                }
            }
        }
    }

    public void A06(EGLSurface eGLSurface, float[] fArr, int i, long j) {
        int i2;
        int i3;
        int i4;
        int i5;
        if (eGLSurface != EGL14.EGL_NO_SURFACE) {
            try {
                EGL14.eglMakeCurrent(this.A0E, eGLSurface, eGLSurface, this.A0D);
                EGLDisplay eGLDisplay = this.A0E;
                int[] iArr = this.A0N;
                EGL14.eglQuerySurface(eGLDisplay, eGLSurface, 12375, iArr, 0);
                EGL14.eglQuerySurface(this.A0E, eGLSurface, 12374, iArr, 1);
                GLES20.glViewport(0, 0, iArr[0], iArr[1]);
                GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
                GLES20.glClear(16384);
                if (i % 180 == 0) {
                    i2 = this.A00;
                    i3 = this.A01;
                } else {
                    i2 = this.A01;
                    i3 = this.A00;
                }
                float f = ((float) i2) / ((float) i3);
                int i6 = iArr[0];
                float f2 = (float) i6;
                int i7 = iArr[1];
                float f3 = (float) i7;
                if (f2 / f3 > f) {
                    i4 = (int) (f3 * f);
                    i5 = i7;
                } else {
                    i5 = (int) (f2 / f);
                    i4 = i6;
                }
                int i8 = (i6 - i4) / 2;
                int i9 = (i7 - i5) / 2;
                if (i8 < 8 && i9 < 8) {
                    i4 = i6;
                    i5 = i7;
                    i8 = 0;
                    i9 = 0;
                }
                GLES20.glViewport(i8, i9, i4, i5);
                A02("draw start");
                GLES20.glUseProgram(this.A02);
                A02("glUseProgram");
                GLES20.glActiveTexture(33984);
                GLES20.glBindTexture(36197, this.A03);
                GLES20.glUniformMatrix4fv(this.A07, 1, false, this.A0L, 0);
                A02("glUniformMatrix4fv");
                GLES20.glUniformMatrix4fv(this.A08, 1, false, this.A0M, 0);
                A02("glUniformMatrix4fv");
                GLES20.glUniformMatrix4fv(this.A06, 1, false, fArr, 0);
                A02("glUniformMatrix4fv");
                int i10 = this.A04;
                GLES20.glEnableVertexAttribArray(i10);
                A02("glEnableVertexAttribArray");
                GLES20.glVertexAttribPointer(i10, 2, 5126, false, 8, (Buffer) A0T);
                A02("glVertexAttribPointer");
                int i11 = this.A05;
                GLES20.glEnableVertexAttribArray(i11);
                A02("glEnableVertexAttribArray");
                GLES20.glVertexAttribPointer(i11, 2, 5126, false, 8, (Buffer) A0S);
                A02("glVertexAttribPointer");
                GLES20.glDrawArrays(5, 0, 4);
                A02("glDrawArrays");
                GLES20.glDisableVertexAttribArray(i10);
                GLES20.glDisableVertexAttribArray(i11);
                GLES20.glBindTexture(36197, 0);
                GLES20.glUseProgram(0);
                EGLExt.eglPresentationTimeANDROID(this.A0E, eGLSurface, j);
                EGL14.eglSwapBuffers(this.A0E, eGLSurface);
            } catch (RuntimeException unused) {
            }
            EGLDisplay eGLDisplay2 = this.A0E;
            EGLSurface eGLSurface2 = this.A0F;
            EGL14.eglMakeCurrent(eGLDisplay2, eGLSurface2, eGLSurface2, this.A0D);
        }
    }

    public void A07(Surface surface, int i) {
        if (this.A0E != EGL14.EGL_NO_DISPLAY) {
            synchronized (this.A0I) {
                this.A0P = i;
                if (this.A0R != EGL14.EGL_NO_SURFACE) {
                    Matrix.setIdentityM(this.A0K, 0);
                    EGL14.eglDestroySurface(this.A0E, this.A0R);
                    this.A0R = EGL14.EGL_NO_SURFACE;
                }
                if (surface != null) {
                    float[] fArr = this.A0K;
                    Matrix.setIdentityM(fArr, 0);
                    Matrix.rotateM(fArr, 0, (float) i, 0.0f, 0.0f, 1.0f);
                    this.A0R = EGL14.eglCreateWindowSurface(this.A0E, this.A0C, surface, new int[]{12344}, 0);
                    if (this.A0R == null || EGL14.eglGetError() != 12288) {
                        this.A0R = EGL14.EGL_NO_SURFACE;
                    }
                }
            }
        }
    }

    @Override // android.graphics.SurfaceTexture.OnFrameAvailableListener
    public void onFrameAvailable(SurfaceTexture surfaceTexture) {
        SurfaceTexture surfaceTexture2 = this.A0A;
        if (surfaceTexture2 != null) {
            try {
                surfaceTexture2.updateTexImage();
                this.A0A.getTransformMatrix(this.A0L);
            } catch (RuntimeException unused) {
            }
            long timestamp = this.A0A.getTimestamp();
            if (!this.A0G) {
                long j = Long.MAX_VALUE;
                for (int i = 0; i < 3; i++) {
                    long nanoTime = System.nanoTime();
                    long elapsedRealtimeNanos = SystemClock.elapsedRealtimeNanos();
                    long nanoTime2 = System.nanoTime();
                    long j2 = nanoTime2 - nanoTime;
                    if (i == 0 || j2 < j) {
                        this.A09 = elapsedRealtimeNanos - ((nanoTime + nanoTime2) >> 1);
                        j = j2;
                    }
                }
                this.A0G = true;
            }
            if (Math.abs(((float) (SystemClock.elapsedRealtimeNanos() - timestamp)) / 1.0E9f) < 5.0f) {
                timestamp -= this.A09;
            }
            synchronized (this.A0H) {
                A06(this.A0Q, this.A0J, this.A0O, timestamp);
            }
            synchronized (this.A0I) {
                A06(this.A0R, this.A0K, this.A0P, timestamp);
            }
        }
    }
}
