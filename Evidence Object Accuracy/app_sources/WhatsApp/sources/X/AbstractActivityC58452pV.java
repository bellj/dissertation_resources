package X;

import com.whatsapp.WaInAppBrowsingActivity;

/* renamed from: X.2pV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC58452pV extends ActivityC13790kL {
    public boolean A00 = false;

    public AbstractActivityC58452pV() {
        ActivityC13830kP.A1P(this, 5);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            WaInAppBrowsingActivity waInAppBrowsingActivity = (WaInAppBrowsingActivity) this;
            AnonymousClass2FL r3 = (AnonymousClass2FL) ActivityC13830kP.A1K(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(r3, waInAppBrowsingActivity);
            ActivityC13810kN.A10(A1M, waInAppBrowsingActivity);
            ActivityC13790kL.A0f(A1M, waInAppBrowsingActivity, ActivityC13790kL.A0S(r3, A1M, waInAppBrowsingActivity, ActivityC13790kL.A0Y(A1M, waInAppBrowsingActivity)));
        }
    }
}
