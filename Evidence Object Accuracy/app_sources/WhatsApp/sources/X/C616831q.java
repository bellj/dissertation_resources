package X;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.net.Uri;
import java.io.File;

/* renamed from: X.31q  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C616831q extends AbstractC68833Wx {
    @Override // X.AbstractC35611iN
    public int getType() {
        return 1;
    }

    public C616831q(ContentResolver contentResolver, Uri uri, String str, String str2, long j, long j2, long j3) {
        super(contentResolver, uri, str, str2, j, j2, j3);
    }

    @Override // X.AbstractC35611iN
    public Bitmap Aem(int i) {
        File file;
        if (i < 144) {
            String str = this.A05;
            if (str == null) {
                file = null;
            } else {
                file = new File(str);
            }
            return C26521Du.A00(new C38901ot(96, false), file);
        }
        long j = (long) i;
        return A00(i, 2 * j * j);
    }

    @Override // X.AbstractC68833Wx
    public boolean equals(Object obj) {
        return (obj instanceof C616831q) && this.A04.equals(((AbstractC68833Wx) obj).A04);
    }

    @Override // X.AbstractC68833Wx
    public int hashCode() {
        return this.A04.toString().hashCode();
    }

    @Override // X.AbstractC68833Wx
    public String toString() {
        return C12970iu.A0w(C12960it.A0k("VideoObject"), this.A02);
    }
}
