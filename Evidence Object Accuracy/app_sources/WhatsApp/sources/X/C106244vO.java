package X;

import android.content.Context;
import android.view.View;

/* renamed from: X.4vO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106244vO implements AnonymousClass5WW {
    @Override // X.AnonymousClass5WW
    public void A6O(Context context, Object obj, Object obj2, Object obj3) {
        View view = (View) obj;
        C56002kA r5 = (C56002kA) obj2;
        if (r5.A01 > 0) {
            if (r5.A02 == 1) {
                view.setVerticalFadingEdgeEnabled(true);
            } else {
                view.setHorizontalFadingEdgeEnabled(true);
            }
            view.setFadingEdgeLength(r5.A01);
        }
    }

    @Override // X.AnonymousClass5WW
    public boolean Adc(Object obj, Object obj2, Object obj3, Object obj4) {
        return C12980iv.A1V(((C56002kA) obj).A01, ((C56002kA) obj2).A01);
    }

    @Override // X.AnonymousClass5WW
    public void Af8(Context context, Object obj, Object obj2, Object obj3) {
        View view = (View) obj;
        view.setVerticalFadingEdgeEnabled(false);
        view.setHorizontalFadingEdgeEnabled(false);
        view.setFadingEdgeLength(0);
    }
}
