package X;

/* renamed from: X.0aq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08260aq implements AbstractC12040hH {
    public final AnonymousClass0H9 A00;
    public final AnonymousClass0H9 A01;
    public final AnonymousClass0H9 A02;
    public final AnonymousClass0J5 A03;
    public final String A04;
    public final boolean A05;

    public C08260aq(AnonymousClass0H9 r1, AnonymousClass0H9 r2, AnonymousClass0H9 r3, AnonymousClass0J5 r4, String str, boolean z) {
        this.A04 = str;
        this.A03 = r4;
        this.A02 = r1;
        this.A00 = r2;
        this.A01 = r3;
        this.A05 = z;
    }

    @Override // X.AbstractC12040hH
    public AbstractC12470hy Aes(AnonymousClass0AA r2, AbstractC08070aX r3) {
        return new C07950aL(this, r3);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Trim Path: {start: ");
        sb.append(this.A02);
        sb.append(", end: ");
        sb.append(this.A00);
        sb.append(", offset: ");
        sb.append(this.A01);
        sb.append("}");
        return sb.toString();
    }
}
