package X;

import android.app.Activity;
import java.util.List;
import java.util.Map;

/* renamed from: X.0r7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17620r7 extends AbstractC17440qo {
    public final C17610r6 A00;

    public C17620r7(C17610r6 r4) {
        super("wa.action.commerce.SendNFMReplyMessage", "wa.action.commerce.ActionWithCallback");
        this.A00 = r4;
    }

    @Override // X.AbstractC17450qp
    public /* bridge */ /* synthetic */ Object A9j(C14220l3 r8, C1093651k r9, C14240l5 r10) {
        C14230l4 r102 = (C14230l4) r10;
        String str = r9.A00;
        if (!str.equals("wa.action.commerce.SendNFMReplyMessage")) {
            if (str.equals("wa.action.commerce.ActionWithCallback")) {
                List list = r8.A00;
                String str2 = (String) list.get(0);
                Map map = (Map) list.get(1);
                C1093751l r0 = ((C94724cR) list.get(2)).A00;
                C90184Mx r3 = new C90184Mx();
                r3.A01 = r0;
                r3.A00 = r102;
                C17610r6 r2 = this.A00;
                Activity A00 = AnonymousClass1AI.A00(r102);
                AbstractC17490qt r02 = (AbstractC17490qt) r2.A04.get(str2);
                if (r02 != null) {
                    r02.AZB(A00, r3, map);
                } else {
                    r2.A01.A00(r3, str2).A00("unsupported_action");
                    return null;
                }
            }
            return null;
        }
        List list2 = r8.A00;
        this.A00.A00(AnonymousClass1AI.A00(r102), (String) list2.get(0), (String) list2.get(1), (Map) list2.get(2));
        return null;
    }
}
