package X;

import java.util.Iterator;
import java.util.List;

/* renamed from: X.52B  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass52B implements AnonymousClass5T8 {
    @Override // X.AnonymousClass5T8
    public Object AJ9(AbstractC111885Be r5, C94394bk r6, Object obj, String str, List list) {
        StringBuilder A0h = C12960it.A0h();
        AbstractC117035Xw r1 = r6.A01.A00;
        if (obj instanceof List) {
            for (Object obj2 : r1.Aet(obj)) {
                if (obj2 instanceof String) {
                    C12970iu.A1V(obj2, A0h);
                }
            }
        }
        if (list != null) {
            Iterator it = C95264dP.A00(r6, String.class, list).iterator();
            while (it.hasNext()) {
                A0h.append(C12970iu.A0x(it));
            }
        }
        return A0h.toString();
    }
}
