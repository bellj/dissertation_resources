package X;

import android.util.Base64;
import com.whatsapp.util.Log;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;

/* renamed from: X.5xn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC129485xn {
    public List A00;
    public final JSONArray A01 = C117315Zl.A0L();

    public AbstractC129485xn A00(PublicKey... publicKeyArr) {
        ArrayList A0l = C12960it.A0l();
        try {
            for (PublicKey publicKey : publicKeyArr) {
                A0l.add(C130255z3.A00(publicKey));
            }
            this.A00 = A0l;
            return this;
        } catch (NoSuchAlgorithmException e) {
            Log.e("PAY: DefaultTrustTokenBuilder/generateKeyFingerprints", e);
            throw new C124785q7(e);
        }
    }

    public String A01() {
        if (!(this instanceof C120915gx)) {
            return ((C120925gy) this).A00;
        }
        try {
            return Base64.encodeToString(C117315Zl.A0a(((C120915gx) this).A00.toString()), 11);
        } catch (UnsupportedEncodingException e) {
            throw new C124785q7(e);
        }
    }
}
