package X;

import java.io.File;
import java.util.Comparator;

/* renamed from: X.2DJ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2DJ implements Comparator {
    @Override // java.util.Comparator
    public int compare(Object obj, Object obj2) {
        return Long.valueOf(((File) obj2).lastModified()).compareTo(Long.valueOf(((File) obj).lastModified()));
    }
}
