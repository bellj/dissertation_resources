package X;

import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.payments.ui.PaymentGroupParticipantPickerActivity;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;

/* renamed from: X.5oN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124055oN extends AbstractC16350or {
    public final /* synthetic */ PaymentGroupParticipantPickerActivity A00;

    public /* synthetic */ C124055oN(PaymentGroupParticipantPickerActivity paymentGroupParticipantPickerActivity) {
        this.A00 = paymentGroupParticipantPickerActivity;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        HashSet A12 = C12970iu.A12();
        PaymentGroupParticipantPickerActivity paymentGroupParticipantPickerActivity = this.A00;
        Iterator it = paymentGroupParticipantPickerActivity.A09.A02(paymentGroupParticipantPickerActivity.A0A).A07().iterator();
        while (true) {
            AnonymousClass1ZO r4 = null;
            if (it.hasNext()) {
                C15370n3 A0B = paymentGroupParticipantPickerActivity.A03.A0B(((AnonymousClass1YO) it.next()).A03);
                if (!A12.contains(A0B) && !((ActivityC13790kL) paymentGroupParticipantPickerActivity).A01.A0F(A0B.A0A())) {
                    UserJid A05 = C15370n3.A05(A0B);
                    if (A05 != null) {
                        r4 = C117305Zk.A0F(A05, paymentGroupParticipantPickerActivity.A0D);
                    }
                    paymentGroupParticipantPickerActivity.A0N.add(new C126985tl(A0B, r4));
                    A12.add(A0B);
                }
            } else {
                Collections.sort(paymentGroupParticipantPickerActivity.A0N, new Comparator(new C36071jH(((ActivityC13790kL) paymentGroupParticipantPickerActivity).A01, paymentGroupParticipantPickerActivity.A05, true), this) { // from class: X.6KL
                    public final /* synthetic */ C36071jH A00;
                    public final /* synthetic */ C124055oN A01;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                    }

                    /* JADX WARNING: Code restructure failed: missing block: B:4:0x002b, code lost:
                        if (r1 != 2) goto L_0x002d;
                     */
                    @Override // java.util.Comparator
                    /* Code decompiled incorrectly, please refer to instructions dump. */
                    public final int compare(java.lang.Object r8, java.lang.Object r9) {
                        /*
                            r7 = this;
                            X.5oN r0 = r7.A01
                            X.1jH r6 = r7.A00
                            X.5tl r8 = (X.C126985tl) r8
                            X.5tl r9 = (X.C126985tl) r9
                            com.whatsapp.payments.ui.PaymentGroupParticipantPickerActivity r2 = r0.A00
                            X.0zW r1 = r2.A0C
                            X.0n3 r5 = r8.A00
                            java.lang.Class<com.whatsapp.jid.UserJid> r4 = com.whatsapp.jid.UserJid.class
                            com.whatsapp.jid.Jid r0 = r5.A0B(r4)
                            com.whatsapp.jid.UserJid r0 = (com.whatsapp.jid.UserJid) r0
                            int r3 = r1.A00(r0)
                            X.0zW r1 = r2.A0C
                            X.0n3 r2 = r9.A00
                            com.whatsapp.jid.Jid r0 = r2.A0B(r4)
                            com.whatsapp.jid.UserJid r0 = (com.whatsapp.jid.UserJid) r0
                            int r1 = r1.A00(r0)
                            r0 = 2
                            if (r3 != r0) goto L_0x002f
                            if (r1 == r0) goto L_0x0037
                        L_0x002d:
                            r1 = -1
                        L_0x002e:
                            return r1
                        L_0x002f:
                            if (r3 == r0) goto L_0x002d
                            if (r1 != r0) goto L_0x0037
                            r1 = 1
                            if (r3 != r0) goto L_0x002e
                            goto L_0x002d
                        L_0x0037:
                            int r1 = r6.compare(r5, r2)
                            return r1
                        */
                        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass6KL.compare(java.lang.Object, java.lang.Object):int");
                    }
                });
                return null;
            }
        }
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        PaymentGroupParticipantPickerActivity paymentGroupParticipantPickerActivity = this.A00;
        paymentGroupParticipantPickerActivity.AaN();
        C117585aE r0 = paymentGroupParticipantPickerActivity.A0F;
        ArrayList arrayList = paymentGroupParticipantPickerActivity.A0N;
        r0.A00 = arrayList;
        r0.notifyDataSetChanged();
        AbstractC005102i A1U = paymentGroupParticipantPickerActivity.A1U();
        if (A1U != null) {
            AnonymousClass018 r6 = ((ActivityC13830kP) paymentGroupParticipantPickerActivity).A01;
            long size = (long) arrayList.size();
            Object[] A1b = C12970iu.A1b();
            C12960it.A1P(A1b, arrayList.size(), 0);
            A1U.A0H(r6.A0I(A1b, R.plurals.n_contacts, size));
        }
    }
}
