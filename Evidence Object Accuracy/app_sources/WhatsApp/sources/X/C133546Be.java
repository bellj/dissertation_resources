package X;

import com.whatsapp.R;
import com.whatsapp.payments.ui.stepup.NoviPayStepUpBloksActivity;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.6Be  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133546Be implements AnonymousClass6MX {
    public final /* synthetic */ AnonymousClass3FE A00;
    public final /* synthetic */ NoviPayStepUpBloksActivity A01;
    public final /* synthetic */ String A02;

    @Override // X.AnonymousClass6MX
    public void AY5(int i) {
    }

    public C133546Be(AnonymousClass3FE r1, NoviPayStepUpBloksActivity noviPayStepUpBloksActivity, String str) {
        this.A01 = noviPayStepUpBloksActivity;
        this.A02 = str;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass6MX
    public void AY6(C452120p r13, String str, String str2, boolean z) {
        if (!z || str == null) {
            NoviPayStepUpBloksActivity noviPayStepUpBloksActivity = this.A01;
            C004802e A0S = C12980iv.A0S(noviPayStepUpBloksActivity);
            C117305Zk.A17(A0S, C117295Zj.A0S(noviPayStepUpBloksActivity, A0S, R.string.novi_bloks_doc_upload_failed_message), this.A00, 76);
            return;
        }
        NoviPayStepUpBloksActivity noviPayStepUpBloksActivity2 = this.A01;
        Map map = noviPayStepUpBloksActivity2.A09;
        if (map == null) {
            map = C12970iu.A11();
            noviPayStepUpBloksActivity2.A09 = map;
        }
        map.put(this.A02, str);
        if (noviPayStepUpBloksActivity2.A04 == null || noviPayStepUpBloksActivity2.A09.size() != noviPayStepUpBloksActivity2.A04.A01.size()) {
            Map map2 = noviPayStepUpBloksActivity2.A09;
            AnonymousClass3FE r3 = this.A00;
            HashMap A11 = C12970iu.A11();
            A11.put("is_doc_upload_step_up_completed", "0");
            C117305Zk.A1F(r3, "document_list_number", Integer.toString(map2.size()), A11);
            return;
        }
        Map map3 = noviPayStepUpBloksActivity2.A09;
        AnonymousClass3FE r6 = this.A00;
        AnonymousClass61S[] r4 = new AnonymousClass61S[2];
        C1316663q r32 = noviPayStepUpBloksActivity2.A06;
        r4[0] = AnonymousClass61S.A00("entry_flow", r32.A03);
        C1310460z A0B = C117315Zl.A0B("step_up", C12960it.A0m(AnonymousClass61S.A00("metadata", r32.A04), r4, 1));
        C1310460z A01 = AnonymousClass61S.A01("novi-answer-document-upload-step-up-challenge");
        ArrayList arrayList = A01.A02;
        arrayList.add(A0B);
        Iterator A0n = C12960it.A0n(map3);
        while (A0n.hasNext()) {
            Map.Entry A15 = C12970iu.A15(A0n);
            AnonymousClass61S[] r2 = new AnonymousClass61S[2];
            AnonymousClass61S.A05("type", C12990iw.A0r(A15), r2, 0);
            C117305Zk.A1R("document", arrayList, C12960it.A0m(AnonymousClass61S.A00("id", C117305Zk.A0m(A15)), r2, 1));
        }
        C130155yt.A04(C117305Zk.A09(r6, noviPayStepUpBloksActivity2, 48), ((AbstractActivityC123635nW) noviPayStepUpBloksActivity2).A04, A01, noviPayStepUpBloksActivity2.A00, 4);
    }
}
