package X;

/* renamed from: X.5Nx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C115005Nx extends AbstractC94944cn {
    public int A00;
    public int A01;
    public AnonymousClass5XI A02;

    public C115005Nx(AnonymousClass5XI r3) {
        this.A02 = r3;
        if (r3 instanceof AbstractC117275Zf) {
            this.A00 = r3.ACZ();
            this.A01 = ((AbstractC117275Zf) r3).AB4();
            return;
        }
        StringBuilder A0k = C12960it.A0k("Digest ");
        A0k.append(r3.AAf());
        throw C12970iu.A0f(C12960it.A0d(" unsupported", A0k));
    }

    public final byte[] A05(int i, int i2) {
        byte[] bArr;
        byte[] bArr2;
        int length;
        int length2;
        int i3 = this.A01;
        byte[] bArr3 = new byte[i3];
        byte[] bArr4 = new byte[i2];
        int i4 = 0;
        for (int i5 = 0; i5 != i3; i5++) {
            bArr3[i5] = (byte) i;
        }
        byte[] bArr5 = super.A02;
        if (bArr5 == null || (length2 = bArr5.length) == 0) {
            bArr = new byte[0];
        } else {
            int i6 = i3 * (((length2 + i3) - 1) / i3);
            bArr = new byte[i6];
            for (int i7 = 0; i7 != i6; i7++) {
                C72463ee.A0X(bArr5, bArr, i7 % length2, i7);
            }
        }
        byte[] bArr6 = super.A01;
        if (bArr6 == null || (length = bArr6.length) == 0) {
            bArr2 = new byte[0];
        } else {
            int i8 = i3 * (((length + i3) - 1) / i3);
            bArr2 = new byte[i8];
            for (int i9 = 0; i9 != i8; i9++) {
                C72463ee.A0X(bArr6, bArr2, i9 % length, i9);
            }
        }
        int length3 = bArr.length;
        int length4 = bArr2.length;
        int i10 = length3 + length4;
        byte[] bArr7 = new byte[i10];
        System.arraycopy(bArr, 0, bArr7, 0, length3);
        System.arraycopy(bArr2, 0, bArr7, length3, length4);
        byte[] bArr8 = new byte[i3];
        int i11 = this.A00;
        int i12 = ((i2 + i11) - 1) / i11;
        byte[] bArr9 = new byte[i11];
        int i13 = 1;
        while (i13 <= i12) {
            AnonymousClass5XI r14 = this.A02;
            r14.update(bArr3, i4, i3);
            r14.update(bArr7, i4, i10);
            r14.A97(bArr9, i4);
            for (int i14 = 1; i14 < super.A00; i14++) {
                r14.update(bArr9, i4, i11);
                r14.A97(bArr9, i4);
            }
            for (int i15 = 0; i15 != i3; i15++) {
                C72463ee.A0X(bArr9, bArr8, i15 % i11, i15);
            }
            for (int i16 = 0; i16 != i10 / i3; i16++) {
                int i17 = i3 * i16;
                int i18 = (i3 + i17) - 1;
                int i19 = (bArr8[i3 - 1] & 255) + (bArr7[i18] & 255) + 1;
                bArr7[i18] = (byte) i19;
                int i20 = i19 >>> 8;
                for (int i21 = i3 - 2; i21 >= 0; i21--) {
                    int i22 = i17 + i21;
                    int i23 = i20 + (bArr8[i21] & 255) + (bArr7[i22] & 255);
                    bArr7[i22] = (byte) i23;
                    i20 = i23 >>> 8;
                }
            }
            if (i13 == i12) {
                int i24 = (i13 - 1) * i11;
                System.arraycopy(bArr9, 0, bArr4, i24, i2 - i24);
            } else {
                System.arraycopy(bArr9, 0, bArr4, (i13 - 1) * i11, i11);
            }
            i13++;
            i4 = 0;
        }
        return bArr4;
    }
}
