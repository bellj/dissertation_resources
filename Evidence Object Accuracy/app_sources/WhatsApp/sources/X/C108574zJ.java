package X;

import java.util.AbstractMap;

/* renamed from: X.4zJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C108574zJ implements AbstractC115615Sg {
    @Override // X.AbstractC115615Sg
    public final Object Ah9(Object obj, Object obj2) {
        AnonymousClass5IM r0;
        AnonymousClass5IM r3 = (AnonymousClass5IM) obj;
        AbstractMap abstractMap = (AbstractMap) obj2;
        if (!abstractMap.isEmpty()) {
            if (!r3.zzfa) {
                if (r3.isEmpty()) {
                    r0 = new AnonymousClass5IM();
                } else {
                    r0 = new AnonymousClass5IM(r3);
                }
                r3 = r0;
            }
            if (!r3.zzfa) {
                throw C12970iu.A0z();
            } else if (!abstractMap.isEmpty()) {
                r3.putAll(abstractMap);
            }
        }
        return r3;
    }
}
