package X;

/* renamed from: X.5fY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120055fY extends C38161nc {
    public C120055fY(C15550nR r1, C15610nY r2, C16590pI r3) {
        super(r1, r2, r3);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002b, code lost:
        if (r4 == null) goto L_0x002d;
     */
    @Override // X.C38161nc
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A00(X.AnonymousClass1IR r6, java.lang.String r7) {
        /*
            r5 = this;
            com.whatsapp.jid.UserJid r1 = r6.A0D
            if (r1 == 0) goto L_0x0012
            X.0nR r0 = r5.A00
            X.0n3 r1 = r0.A0B(r1)
            X.0nY r0 = r5.A01
            java.lang.String r4 = r0.A08(r1)
            if (r4 != 0) goto L_0x0038
        L_0x0012:
            X.1Zf r0 = r6.A0A
            if (r0 == 0) goto L_0x002d
            X.1ZR r0 = r0.A0C()
            boolean r1 = X.AnonymousClass1ZS.A02(r0)
            X.1Zf r0 = r6.A0A
            if (r1 != 0) goto L_0x004c
            X.1ZR r0 = r0.A0C()
            java.lang.Object r4 = r0.A00
            X.AnonymousClass009.A05(r4)
        L_0x002b:
            if (r4 != 0) goto L_0x0038
        L_0x002d:
            X.0pI r0 = r5.A02
            android.content.Context r1 = r0.A00
            r0 = 2131892451(0x7f1218e3, float:1.941965E38)
            java.lang.String r4 = r1.getString(r0)
        L_0x0038:
            X.0pI r0 = r5.A02
            android.content.Context r3 = r0.A00
            r2 = 2131887142(0x7f120426, float:1.9408883E38)
            java.lang.Object[] r1 = X.C12980iv.A1a()
            r0 = 0
            r1[r0] = r4
            r0 = 1
            java.lang.String r0 = X.C12960it.A0X(r3, r7, r1, r0, r2)
            return r0
        L_0x004c:
            java.lang.String r0 = r0.A0F()
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x002d
            X.1Zf r0 = r6.A0A
            java.lang.String r4 = r0.A0F()
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C120055fY.A00(X.1IR, java.lang.String):java.lang.String");
    }
}
