package X;

import android.util.Log;
import java.util.ArrayList;
import java.util.Map;

/* renamed from: X.06d  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C013006d implements AnonymousClass05J {
    public final /* synthetic */ AnonymousClass01F A00;

    public C013006d(AnonymousClass01F r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass05J
    public /* bridge */ /* synthetic */ void ALs(Object obj) {
        StringBuilder sb;
        Map map = (Map) obj;
        String[] strArr = (String[]) map.keySet().toArray(new String[0]);
        ArrayList arrayList = new ArrayList(map.values());
        int[] iArr = new int[arrayList.size()];
        for (int i = 0; i < arrayList.size(); i++) {
            int i2 = -1;
            if (((Boolean) arrayList.get(i)).booleanValue()) {
                i2 = 0;
            }
            iArr[i] = i2;
        }
        AnonymousClass01F r4 = this.A00;
        C06790Vc r0 = (C06790Vc) r4.A0D.pollFirst();
        if (r0 == null) {
            sb = new StringBuilder("No permissions were requested for ");
            sb.append(this);
        } else {
            String str = r0.A01;
            int i3 = r0.A00;
            AnonymousClass01E A00 = r4.A0U.A00(str);
            if (A00 == null) {
                sb = new StringBuilder("Permission request result delivered for unknown Fragment ");
                sb.append(str);
            } else {
                A00.A0j(i3, strArr, iArr);
                return;
            }
        }
        Log.w("FragmentManager", sb.toString());
    }
}
