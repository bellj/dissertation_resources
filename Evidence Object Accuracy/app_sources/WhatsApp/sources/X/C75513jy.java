package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.components.button.ThumbnailButton;

/* renamed from: X.3jy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75513jy extends AnonymousClass03U {
    public final View A00;
    public final TextView A01;
    public final ThumbnailButton A02;

    public C75513jy(View view) {
        super(view);
        this.A00 = view;
        this.A02 = (ThumbnailButton) view.findViewById(R.id.contact_row_photo);
        TextView A0J = C12960it.A0J(view, R.id.contact_name);
        this.A01 = A0J;
        AnonymousClass028.A0a(A0J, 2);
    }
}
