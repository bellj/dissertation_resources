package X;

/* renamed from: X.68m  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1328568m implements AnonymousClass1FK {
    public final /* synthetic */ AbstractC16870pt A00;
    public final /* synthetic */ C118185bP A01;

    public C1328568m(AbstractC16870pt r1, C118185bP r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass1FK
    public void AV3(C452120p r3) {
        this.A01.A0d.A06(C12960it.A0b("syncPendingTransaction onRequestError: ", r3));
        AbstractC16870pt r1 = this.A00;
        if (r1 != null) {
            r1.AKa(r3, 10);
        }
    }

    @Override // X.AnonymousClass1FK
    public void AVA(C452120p r3) {
        this.A01.A0d.A06(C12960it.A0b("syncPendingTransaction onResponseError: ", r3));
        AbstractC16870pt r1 = this.A00;
        if (r1 != null) {
            r1.AKa(r3, 10);
        }
    }

    @Override // X.AnonymousClass1FK
    public void AVB(C452220q r4) {
        this.A01.A0d.A06("syncPendingTransaction onResponseSuccess");
        AbstractC16870pt r2 = this.A00;
        if (r2 != null) {
            r2.AKa(null, 10);
        }
    }
}
