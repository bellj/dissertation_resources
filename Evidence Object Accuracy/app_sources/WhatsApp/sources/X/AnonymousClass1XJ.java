package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.1XJ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1XJ extends AnonymousClass1X7 implements AbstractC16400ox, AbstractC16420oz {
    public UserJid A00;
    public String A01;
    public String A02;

    public AnonymousClass1XJ(C16150oX r2, AnonymousClass1IS r3, AnonymousClass1XJ r4, long j, boolean z) {
        super(r2, r3, r4, j, z);
        this.A00 = r4.A00;
        this.A02 = r4.A02;
        this.A01 = r4.A01;
    }

    public AnonymousClass1XJ(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 37, j);
    }
}
