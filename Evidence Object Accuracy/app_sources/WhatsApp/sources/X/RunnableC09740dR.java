package X;

/* renamed from: X.0dR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09740dR implements Runnable {
    public final /* synthetic */ int A00;
    public final /* synthetic */ C08770bp A01;

    public RunnableC09740dR(C08770bp r1, int i) {
        this.A01 = r1;
        this.A00 = i;
    }

    @Override // java.lang.Runnable
    public void run() {
        int i = this.A00;
        if (i != -1) {
            this.A01.A08(i);
        }
    }
}
