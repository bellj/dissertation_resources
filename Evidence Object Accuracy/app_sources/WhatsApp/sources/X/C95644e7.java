package X;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URI;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.cert.CertPathBuilderException;
import java.security.cert.CertPathValidatorException;
import java.security.cert.CertSelector;
import java.security.cert.CertStore;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.PolicyQualifierInfo;
import java.security.cert.TrustAnchor;
import java.security.cert.X509CRL;
import java.security.cert.X509CRLSelector;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.security.cert.X509Extension;
import java.security.interfaces.DSAParams;
import java.security.interfaces.DSAPublicKey;
import java.security.spec.DSAPublicKeySpec;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.security.auth.x500.X500Principal;

/* renamed from: X.4e7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C95644e7 {
    public static final String A00 = C114715Mu.A05.A01;
    public static final String A01 = C114715Mu.A09.A01;
    public static final String A02 = C114715Mu.A0K.A01;

    public static PublicKey A00(List list, AnonymousClass5S2 r5, int i) {
        DSAPublicKey dSAPublicKey;
        PublicKey publicKey = ((Certificate) list.get(i)).getPublicKey();
        if (!(publicKey instanceof DSAPublicKey)) {
            return publicKey;
        }
        DSAPublicKey dSAPublicKey2 = (DSAPublicKey) publicKey;
        if (dSAPublicKey2.getParams() != null) {
            return dSAPublicKey2;
        }
        do {
            i++;
            if (i < list.size()) {
                PublicKey publicKey2 = ((Certificate) list.get(i)).getPublicKey();
                if (publicKey2 instanceof DSAPublicKey) {
                    dSAPublicKey = (DSAPublicKey) publicKey2;
                } else {
                    throw new CertPathValidatorException("DSA parameters cannot be inherited from previous certificate.");
                }
            } else {
                throw new CertPathValidatorException("DSA parameters cannot be inherited from previous certificate.");
            }
        } while (dSAPublicKey.getParams() == null);
        DSAParams params = dSAPublicKey.getParams();
        try {
            return KeyFactory.getInstance("DSA", ((AnonymousClass5GT) r5).A00).generatePublic(new DSAPublicKeySpec(dSAPublicKey2.getY(), params.getP(), params.getQ(), params.getG()));
        } catch (Exception e) {
            throw C12990iw.A0m(e.getMessage());
        }
    }

    public static TrustAnchor A01(String str, X509Certificate x509Certificate, Set set) {
        X509CertSelector x509CertSelector = new X509CertSelector();
        X500Principal issuerX500Principal = x509Certificate.getIssuerX500Principal();
        x509CertSelector.setSubject(issuerX500Principal);
        Iterator it = set.iterator();
        TrustAnchor trustAnchor = null;
        Exception e = null;
        AnonymousClass5N2 r6 = null;
        PublicKey publicKey = null;
        while (it.hasNext() && trustAnchor == null) {
            trustAnchor = (TrustAnchor) it.next();
            if (trustAnchor.getTrustedCert() != null) {
                if (x509CertSelector.match(trustAnchor.getTrustedCert())) {
                    publicKey = trustAnchor.getTrustedCert().getPublicKey();
                }
                trustAnchor = null;
            } else {
                if (!(trustAnchor.getCA() == null || trustAnchor.getCAName() == null || trustAnchor.getCAPublicKey() == null)) {
                    if (r6 == null) {
                        r6 = AnonymousClass5N2.A00(issuerX500Principal.getEncoded());
                    }
                    try {
                        if (r6.equals(AnonymousClass5N2.A00(trustAnchor.getCA().getEncoded()))) {
                            publicKey = trustAnchor.getCAPublicKey();
                        }
                    } catch (IllegalArgumentException unused) {
                    }
                }
                trustAnchor = null;
            }
            if (publicKey != null) {
                if (str == null) {
                    try {
                        x509Certificate.verify(publicKey);
                    } catch (Exception e2) {
                        e = e2;
                        trustAnchor = null;
                        publicKey = null;
                    }
                } else {
                    x509Certificate.verify(publicKey, str);
                }
            }
        }
        if (trustAnchor != null || e == null) {
            return trustAnchor;
        }
        throw AnonymousClass4C6.A00("TrustAnchor found but certificate validation failed.", e);
    }

    public static Collection A02(X509Certificate x509Certificate, List list, List list2) {
        AnonymousClass5MT r1;
        byte[] bArr;
        X509CertSelector x509CertSelector = new X509CertSelector();
        try {
            x509CertSelector.setSubject(C95354dZ.A01(x509Certificate).A01());
            try {
                byte[] extensionValue = x509Certificate.getExtensionValue(A00);
                if (extensionValue != null) {
                    byte[] A05 = AnonymousClass5NH.A05(extensionValue);
                    if (A05 instanceof AnonymousClass5MT) {
                        r1 = (AnonymousClass5MT) A05;
                    } else {
                        r1 = A05 != null ? new AnonymousClass5MT(AbstractC114775Na.A04(A05)) : null;
                    }
                    AnonymousClass5NH r0 = r1.A01;
                    if (r0 != null) {
                        bArr = r0.A00;
                    } else {
                        bArr = null;
                    }
                    if (bArr != null) {
                        x509CertSelector.setSubjectKeyIdentifier(new AnonymousClass5N5(bArr).A01());
                    }
                }
            } catch (Exception unused) {
            }
            AnonymousClass5GZ r12 = new AnonymousClass5GZ(new C89734Lc(x509CertSelector).A00);
            LinkedHashSet linkedHashSet = new LinkedHashSet();
            try {
                A0B(linkedHashSet, list, r12);
                A0B(linkedHashSet, list2, r12);
                return linkedHashSet;
            } catch (AnonymousClass4C6 e) {
                throw AnonymousClass4C6.A00("Issuer certificate cannot be searched.", e);
            }
        } catch (Exception e2) {
            throw AnonymousClass4C6.A00("Subject criteria for certificate selector to find issuer certificate could not be set.", e2);
        }
    }

    public static Collection A03(C112075By r3) {
        C112085Bz r32 = r3.A02;
        AnonymousClass5GZ r2 = r32.A09;
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        try {
            A0B(linkedHashSet, r32.A05, r2);
            A0B(linkedHashSet, r32.A01.getCertStores(), r2);
            if (!linkedHashSet.isEmpty()) {
                return linkedHashSet;
            }
            CertSelector certSelector = r2.A00;
            X509Certificate certificate = certSelector instanceof X509CertSelector ? ((X509CertSelector) certSelector).getCertificate() : null;
            if (certificate != null) {
                return Collections.singleton(certificate);
            }
            throw new CertPathBuilderException("No certificate found matching targetConstraints.");
        } catch (AnonymousClass4C6 e) {
            throw new C113345Hd(e);
        }
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:25:0x0013 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v0 */
    /* JADX WARN: Type inference failed for: r2v1, types: [java.lang.Throwable] */
    /* JADX WARN: Type inference failed for: r2v2, types: [java.lang.Throwable] */
    /* JADX WARN: Type inference failed for: r2v4, types: [X.5NE] */
    /* JADX WARN: Type inference failed for: r2v5 */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.Date A04(java.security.cert.CertPath r5, java.util.Date r6, int r7, int r8) {
        /*
            java.lang.String r4 = "Date of cert gen extension could not be read."
            r1 = 1
            if (r1 != r7) goto L_0x0046
            if (r8 <= 0) goto L_0x0046
            java.util.List r0 = r5.getCertificates()
            int r8 = r8 - r1
            java.security.cert.X509Certificate r3 = X.C72463ee.A0K(r0, r8)
            if (r8 != 0) goto L_0x0042
            r2 = 0
            java.security.cert.X509Certificate r1 = X.C72453ed.A0y(r5, r8)     // Catch: IOException -> 0x003d, IllegalArgumentException -> 0x0038
            X.1TK r0 = X.AbstractC72413eY.A07     // Catch: IOException -> 0x003d, IllegalArgumentException -> 0x0038
            java.lang.String r0 = r0.A01     // Catch: IOException -> 0x003d, IllegalArgumentException -> 0x0038
            byte[] r0 = r1.getExtensionValue(r0)     // Catch: IOException -> 0x003d, IllegalArgumentException -> 0x0038
            if (r0 == 0) goto L_0x0029
            X.1TL r0 = X.AnonymousClass1TL.A03(r0)     // Catch: IOException -> 0x003d, IllegalArgumentException -> 0x0038
            X.5NE r2 = X.AnonymousClass5NE.A01(r0)     // Catch: IOException -> 0x003d, IllegalArgumentException -> 0x0038
        L_0x0029:
            if (r2 == 0) goto L_0x0042
            java.util.Date r6 = r2.A0D()     // Catch: ParseException -> 0x0030
            return r6
        L_0x0030:
            r1 = move-exception
            java.lang.String r0 = "Date from date of cert gen extension could not be parsed."
            X.4C6 r0 = X.AnonymousClass4C6.A00(r0, r1)
            throw r0
        L_0x0038:
            X.4C6 r0 = X.AnonymousClass4C6.A00(r4, r2)
            throw r0
        L_0x003d:
            X.4C6 r0 = X.AnonymousClass4C6.A00(r4, r2)
            throw r0
        L_0x0042:
            java.util.Date r6 = r3.getNotBefore()
        L_0x0046:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95644e7.A04(java.security.cert.CertPath, java.util.Date, int, int):java.util.Date");
    }

    public static Set A05(X509CRL x509crl, Date date, List list, List list2, AnonymousClass5S2 r13) {
        X509CRLSelector x509CRLSelector = new X509CRLSelector();
        try {
            x509CRLSelector.addIssuerName(C95354dZ.A03(x509crl.getIssuerX500Principal()).A01());
            try {
                AnonymousClass1TL A07 = A07(A01, x509crl);
                BigInteger bigInteger = null;
                BigInteger bigInteger2 = A07 != null ? new BigInteger(1, AnonymousClass5NG.A00(A07).A01) : null;
                try {
                    byte[] extensionValue = x509crl.getExtensionValue(A02);
                    if (bigInteger2 != null) {
                        bigInteger = bigInteger2.add(BigInteger.valueOf(1));
                    }
                    x509CRLSelector.setMinCRLNumber(bigInteger);
                    C91724Sw r1 = new C91724Sw(x509CRLSelector);
                    r1.A03 = AnonymousClass1TT.A02(extensionValue);
                    r1.A02 = true;
                    r1.A00 = bigInteger2;
                    C113105Ga r5 = new C113105Ga(r1);
                    Set<X509CRL> A002 = AbstractC93044Yt.A00(date, list, list2, r5);
                    if (A002.isEmpty() && C94664cJ.A01("org.spongycastle.x509.enableCRLDP")) {
                        try {
                            CertificateFactory instance = CertificateFactory.getInstance("X.509", ((AnonymousClass5GT) r13).A00);
                            C114685Mr[] A03 = C114615Mk.A00(extensionValue).A03();
                            for (int i = 0; i < A03.length; i++) {
                                AnonymousClass5N0 r12 = A03[i].A00;
                                if (r12 != null && r12.A00 == 0) {
                                    AnonymousClass5N1[] A012 = C114705Mt.A01(r12.A01);
                                    for (int i2 = 0; i2 < A012.length; i2++) {
                                        AnonymousClass5N1 r2 = A012[i];
                                        if (r2.A00 == 6) {
                                            try {
                                                A002 = AbstractC93044Yt.A00(date, Collections.EMPTY_LIST, Collections.singletonList(C95174dG.A01(new URI(((AnonymousClass5VP) r2.A01).AGy()), instance, date)), r5);
                                                break;
                                            } catch (Exception unused) {
                                                continue;
                                            }
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            throw AnonymousClass4C6.A00(C12960it.A0d(e.getMessage(), C12960it.A0k("cannot create certificate factory: ")), e);
                        }
                    }
                    HashSet A12 = C12970iu.A12();
                    for (X509CRL x509crl2 : A002) {
                        Set<String> criticalExtensionOIDs = x509crl2.getCriticalExtensionOIDs();
                        if (criticalExtensionOIDs == null ? false : criticalExtensionOIDs.contains(C95674eA.A05)) {
                            A12.add(x509crl2);
                        }
                    }
                    return A12;
                } catch (Exception e2) {
                    throw AnonymousClass4C6.A00("Issuing distribution point extension value could not be read.", e2);
                }
            } catch (Exception e3) {
                throw AnonymousClass4C6.A00("CRL number extension could not be extracted from CRL.", e3);
            }
        } catch (IOException e4) {
            throw AnonymousClass4C6.A00("Cannot extract issuer from CRL.", e4);
        }
    }

    public static final Set A06(AbstractC114775Na r6) {
        HashSet A12 = C12970iu.A12();
        if (r6 != null) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            AnonymousClass1TP r3 = new AnonymousClass1TP(byteArrayOutputStream);
            Enumeration A0C = r6.A0C();
            while (A0C.hasMoreElements()) {
                try {
                    AnonymousClass1TN r0 = (AnonymousClass1TN) A0C.nextElement();
                    if (r0 != null) {
                        r3.A04(r0.Aer(), true);
                        A12.add(new PolicyQualifierInfo(byteArrayOutputStream.toByteArray()));
                        byteArrayOutputStream.reset();
                    } else {
                        throw C12990iw.A0i("null object detected");
                    }
                } catch (IOException e) {
                    throw new C113385Hh("Policy qualifier info cannot be decoded.", e);
                }
            }
        }
        return A12;
    }

    public static AnonymousClass1TL A07(String str, X509Extension x509Extension) {
        byte[] extensionValue = x509Extension.getExtensionValue(str);
        if (extensionValue == null) {
            return null;
        }
        try {
            return AnonymousClass1TL.A03(AnonymousClass5NH.A05(extensionValue));
        } catch (Exception e) {
            throw AnonymousClass4C6.A00(C12960it.A0d(str, C12960it.A0k("exception processing extension ")), e);
        }
    }

    public static AnonymousClass5C0 A08(AnonymousClass5C0 r3, AnonymousClass5C0 r4, List[] listArr) {
        AnonymousClass5C0 r0 = (AnonymousClass5C0) r4.getParent();
        if (r3 != null) {
            if (r0 == null) {
                for (int i = 0; i < listArr.length; i++) {
                    listArr[i] = C12960it.A0l();
                }
            } else {
                r0.A03.remove(r4);
                A0C(r4, listArr);
                return r3;
            }
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x008b A[ADDED_TO_REGION] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A09(java.lang.Object r5, java.security.cert.X509CRL r6, java.util.Date r7, X.C90584Ol r8) {
        /*
            X.1TK r0 = X.C114715Mu.A0K     // Catch: Exception -> 0x00b0, CRLException -> 0x00b7
            java.lang.String r0 = r0.A01     // Catch: Exception -> 0x00b0, CRLException -> 0x00b7
            byte[] r0 = r6.getExtensionValue(r0)     // Catch: Exception -> 0x00b0, CRLException -> 0x00b7
            if (r0 == 0) goto L_0x003c
            byte[] r0 = X.AnonymousClass5NH.A05(r0)     // Catch: Exception -> 0x00b0, CRLException -> 0x00b7
            X.5Ms r0 = X.C114695Ms.A00(r0)     // Catch: Exception -> 0x00b0, CRLException -> 0x00b7
            boolean r0 = r0.A03     // Catch: Exception -> 0x00b0, CRLException -> 0x00b7
            if (r0 == 0) goto L_0x003c
            r0 = r5
            java.security.cert.X509Certificate r0 = (java.security.cert.X509Certificate) r0
            java.math.BigInteger r0 = r0.getSerialNumber()
            java.security.cert.X509CRLEntry r3 = r6.getRevokedCertificate(r0)
            if (r3 == 0) goto L_0x005a
            javax.security.auth.x500.X500Principal r0 = r3.getCertificateIssuer()
            if (r0 != 0) goto L_0x002d
            javax.security.auth.x500.X500Principal r0 = r6.getIssuerX500Principal()
        L_0x002d:
            X.5N2 r1 = X.C95354dZ.A03(r0)
            X.5N2 r0 = X.C95354dZ.A00(r5)
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x005b
            return
        L_0x003c:
            X.5N2 r1 = X.C95354dZ.A00(r5)
            javax.security.auth.x500.X500Principal r0 = r6.getIssuerX500Principal()
            X.5N2 r0 = X.C95354dZ.A03(r0)
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x005a
            java.security.cert.X509Certificate r5 = (java.security.cert.X509Certificate) r5
            java.math.BigInteger r0 = r5.getSerialNumber()
            java.security.cert.X509CRLEntry r3 = r6.getRevokedCertificate(r0)
            if (r3 != 0) goto L_0x005b
        L_0x005a:
            return
        L_0x005b:
            boolean r0 = r3.hasExtensions()
            if (r0 == 0) goto L_0x007a
            boolean r0 = r3.hasUnsupportedCriticalExtension()
            if (r0 != 0) goto L_0x00a8
            X.1TK r0 = X.C114715Mu.A0T     // Catch: Exception -> 0x00a0
            java.lang.String r0 = r0.A01     // Catch: Exception -> 0x00a0
            X.1TL r0 = A07(r0, r3)     // Catch: Exception -> 0x00a0
            X.5ND r0 = X.AnonymousClass5ND.A00(r0)     // Catch: Exception -> 0x00a0
            if (r0 == 0) goto L_0x007a
            int r6 = r0.A0B()
            goto L_0x007b
        L_0x007a:
            r6 = 0
        L_0x007b:
            long r4 = r7.getTime()
            java.util.Date r0 = r3.getRevocationDate()
            long r1 = r0.getTime()
            int r0 = (r4 > r1 ? 1 : (r4 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x0097
            if (r6 == 0) goto L_0x0097
            r0 = 1
            if (r6 == r0) goto L_0x0097
            r0 = 2
            if (r6 == r0) goto L_0x0097
            r0 = 10
            if (r6 != r0) goto L_0x005a
        L_0x0097:
            r8.A00 = r6
            java.util.Date r0 = r3.getRevocationDate()
            r8.A01 = r0
            return
        L_0x00a0:
            r1 = move-exception
            java.lang.String r0 = "Reason code CRL entry extension could not be decoded."
            X.4C6 r0 = X.AnonymousClass4C6.A00(r0, r1)
            throw r0
        L_0x00a8:
            java.lang.String r1 = "CRL entry has unsupported critical extensions."
            r0 = 0
            X.4C6 r0 = X.AnonymousClass4C6.A00(r1, r0)
            throw r0
        L_0x00b0:
            r1 = move-exception
            X.5Hb r0 = new X.5Hb     // Catch: CRLException -> 0x00b7
            r0.<init>(r1)     // Catch: CRLException -> 0x00b7
            throw r0     // Catch: CRLException -> 0x00b7
        L_0x00b7:
            r1 = move-exception
            java.lang.String r0 = "Failed check for indirect CRL."
            X.4C6 r0 = X.AnonymousClass4C6.A00(r0, r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95644e7.A09(java.lang.Object, java.security.cert.X509CRL, java.util.Date, X.4Ol):void");
    }

    public static void A0A(PublicKey publicKey) {
        try {
            C114515Ma.A00(publicKey.getEncoded());
        } catch (Exception e) {
            throw new C113385Hh("Subject public key cannot be decoded.", e);
        }
    }

    public static void A0B(LinkedHashSet linkedHashSet, List list, AnonymousClass5GZ r5) {
        for (Object obj : list) {
            if (obj instanceof AnonymousClass5VS) {
                try {
                    linkedHashSet.addAll(((AnonymousClass5VS) obj).AE7(r5));
                } catch (C113175Gl e) {
                    throw AnonymousClass4C6.A00("Problem while picking certificates from X.509 store.", e);
                }
            } else {
                try {
                    linkedHashSet.addAll(((CertStore) obj).getCertificates(new C113465Hr(r5)));
                } catch (CertStoreException e2) {
                    throw AnonymousClass4C6.A00("Problem while picking certificates from certificate store.", e2);
                }
            }
        }
    }

    public static void A0C(AnonymousClass5C0 r1, List[] listArr) {
        listArr[r1.getDepth()].remove(r1);
        if (!r1.A03.isEmpty()) {
            Iterator children = r1.getChildren();
            while (children.hasNext()) {
                A0C((AnonymousClass5C0) children.next(), listArr);
            }
        }
    }
}
