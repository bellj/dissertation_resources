package X;

import com.whatsapp.jid.Jid;
import java.util.Arrays;
import java.util.HashSet;

/* renamed from: X.3GN  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3GN {
    public static boolean A00(C14850m9 r3, Jid jid) {
        if (jid == null) {
            return false;
        }
        if (!A01(r3, jid)) {
            String A03 = r3.A03(1036);
            if (AnonymousClass1US.A0C(A03) || !new HashSet(Arrays.asList(A03.split(","))).contains(jid.user)) {
                return false;
            }
        }
        return true;
    }

    public static boolean A01(C14850m9 r3, Jid jid) {
        if (jid != null) {
            String A03 = r3.A03(1035);
            if (!AnonymousClass1US.A0C(A03)) {
                return new HashSet(Arrays.asList(A03.split(","))).contains(jid.user);
            }
        }
        return false;
    }
}
