package X;

import com.facebook.redex.RunnableBRunnable0Shape5S0100000_I0_5;

/* renamed from: X.1Cz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26341Cz implements AbstractC16990q5 {
    public final C15990oG A00;
    public final C18240s8 A01;

    @Override // X.AbstractC16990q5
    public void AOp() {
    }

    public C26341Cz(C15990oG r1, C18240s8 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC16990q5
    public void AOo() {
        C18240s8 r2 = this.A01;
        r2.A00.submit(new RunnableBRunnable0Shape5S0100000_I0_5(this, 42));
    }
}
