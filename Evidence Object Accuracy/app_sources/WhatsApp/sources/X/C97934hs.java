package X;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

/* renamed from: X.4hs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C97934hs implements SensorEventListener {
    public final /* synthetic */ C35191hP A00;
    public final /* synthetic */ C35191hP A01;

    @Override // android.hardware.SensorEventListener
    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    public C97934hs(C35191hP r1, C35191hP r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // android.hardware.SensorEventListener
    public void onSensorChanged(SensorEvent sensorEvent) {
        SensorEventListener sensorEventListener;
        Sensor sensor;
        C35191hP r0 = this.A01;
        C35191hP r3 = this.A00;
        if (r0 == null) {
            r3.A0D(false);
        } else if (r0 == r3) {
            float f = sensorEvent.values[0];
            if (f >= 5.0f || f == r3.A0D.getMaximumRange()) {
                r3.A0D(false);
                return;
            } else {
                r3.A0D(true);
                return;
            }
        }
        SensorManager sensorManager = r3.A0F;
        if (!(sensorManager == null || (sensorEventListener = r3.A0E) == null || (sensor = r3.A0D) == null)) {
            sensorManager.unregisterListener(sensorEventListener, sensor);
            r3.A0F = null;
        }
        r3.A08();
    }
}
