package X;

import android.content.Intent;
import com.google.android.gms.common.internal.IAccountAccessor;
import java.util.Set;

/* renamed from: X.3eb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public interface AbstractC72443eb extends AnonymousClass5QV {
    void A7X(AnonymousClass5Sa v);

    void A8y();

    int AET();

    void AG7(IAccountAccessor iAccountAccessor, Set set);

    Intent AGl();

    boolean AJG();

    boolean AZd();

    boolean Aad();

    boolean Aae();

    boolean isConnected();
}
