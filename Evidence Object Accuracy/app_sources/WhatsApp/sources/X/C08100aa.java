package X;

import java.util.List;

/* renamed from: X.0aa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C08100aa implements AbstractC12660iI {
    public float A00 = -1.0f;
    public AnonymousClass0U8 A01 = null;
    public AnonymousClass0U8 A02;
    public final List A03;

    @Override // X.AbstractC12660iI
    public boolean isEmpty() {
        return false;
    }

    public C08100aa(List list) {
        this.A03 = list;
        this.A02 = A00(0.0f);
    }

    public final AnonymousClass0U8 A00(float f) {
        List list = this.A03;
        AnonymousClass0U8 r2 = (AnonymousClass0U8) list.get(list.size() - 1);
        if (f < r2.A01()) {
            for (int size = list.size() - 2; size >= 1; size--) {
                r2 = (AnonymousClass0U8) list.get(size);
                if (this.A02 == r2 || f < r2.A01() || f >= r2.A00()) {
                }
            }
            return (AnonymousClass0U8) list.get(0);
        }
        return r2;
    }

    @Override // X.AbstractC12660iI
    public AnonymousClass0U8 AC6() {
        return this.A02;
    }

    @Override // X.AbstractC12660iI
    public float ACj() {
        List list = this.A03;
        return ((AnonymousClass0U8) list.get(list.size() - 1)).A00();
    }

    @Override // X.AbstractC12660iI
    public float AGu() {
        return ((AnonymousClass0U8) this.A03.get(0)).A01();
    }

    @Override // X.AbstractC12660iI
    public boolean AJF(float f) {
        AnonymousClass0U8 r0 = this.A01;
        AnonymousClass0U8 r1 = this.A02;
        if (r0 == r1 && this.A00 == f) {
            return true;
        }
        this.A01 = r1;
        this.A00 = f;
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0011, code lost:
        if (r4 >= r2.A00()) goto L_0x0013;
     */
    @Override // X.AbstractC12660iI
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean AKI(float r4) {
        /*
            r3 = this;
            X.0U8 r2 = r3.A02
            float r0 = r2.A01()
            int r0 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
            if (r0 < 0) goto L_0x0013
            float r0 = r2.A00()
            int r1 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
            r0 = 1
            if (r1 < 0) goto L_0x0014
        L_0x0013:
            r0 = 0
        L_0x0014:
            r1 = 1
            if (r0 == 0) goto L_0x001d
            boolean r0 = r2.A02()
            r0 = r0 ^ r1
            return r0
        L_0x001d:
            X.0U8 r0 = r3.A00(r4)
            r3.A02 = r0
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C08100aa.AKI(float):boolean");
    }
}
