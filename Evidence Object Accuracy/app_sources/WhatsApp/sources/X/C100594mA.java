package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

/* renamed from: X.4mA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C100594mA implements Parcelable {
    @Deprecated
    public static final C100594mA A06;
    public static final C100594mA A07;
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(35);
    public final int A00;
    public final int A01;
    public final int A02;
    public final AnonymousClass1Mr A03;
    public final AnonymousClass1Mr A04;
    public final boolean A05;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    static {
        C100594mA A01;
        AnonymousClass4X6 r1 = new AnonymousClass4X6();
        if (!(r1 instanceof C77333n7)) {
            A01 = new C100594mA(r1.A01, r1.A02, r1.A00);
        } else {
            A01 = ((C77333n7) r1).A01();
        }
        A07 = A01;
        A06 = A01;
    }

    public C100594mA(AnonymousClass1Mr r2, AnonymousClass1Mr r3, int i) {
        this.A03 = r2;
        this.A01 = 0;
        this.A04 = r3;
        this.A02 = i;
        this.A05 = false;
        this.A00 = 0;
    }

    public C100594mA(Parcel parcel) {
        ArrayList A0l = C12960it.A0l();
        parcel.readList(A0l, null);
        this.A03 = AnonymousClass1Mr.copyOf(A0l);
        this.A01 = parcel.readInt();
        ArrayList A0l2 = C12960it.A0l();
        parcel.readList(A0l2, null);
        this.A04 = AnonymousClass1Mr.copyOf(A0l2);
        this.A02 = parcel.readInt();
        this.A05 = C12960it.A1S(parcel.readInt());
        this.A00 = parcel.readInt();
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C100594mA r5 = (C100594mA) obj;
            if (!(this.A03.equals(r5.A03) && this.A01 == r5.A01 && this.A04.equals(r5.A04) && this.A02 == r5.A02 && this.A05 == r5.A05 && this.A00 == r5.A00)) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return (((((C12990iw.A08(this.A04, (((this.A03.hashCode() + 31) * 31) + this.A01) * 31) * 31) + this.A02) * 31) + (this.A05 ? 1 : 0)) * 31) + this.A00;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(this.A03);
        parcel.writeInt(this.A01);
        parcel.writeList(this.A04);
        parcel.writeInt(this.A02);
        parcel.writeInt(this.A05 ? 1 : 0);
        parcel.writeInt(this.A00);
    }
}
