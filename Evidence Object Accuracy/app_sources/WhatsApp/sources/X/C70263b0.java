package X;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import com.whatsapp.R;

/* renamed from: X.3b0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70263b0 implements AbstractC41521tf {
    public final /* synthetic */ C60852yk A00;

    public C70263b0(C60852yk r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC41521tf
    public int AGm() {
        return this.A00.A0G.A06.A03();
    }

    @Override // X.AbstractC41521tf
    public void AQV() {
        this.A00.A1O();
    }

    @Override // X.AbstractC41521tf
    public void Adg(Bitmap bitmap, View view, AbstractC15340mz r7) {
        C60852yk r2 = this.A00;
        if (bitmap != null) {
            r2.setThumbnail(new BitmapDrawable(C12960it.A09(r2), bitmap));
            r2.A0G.A02(bitmap.getWidth(), bitmap.getHeight(), false);
            return;
        }
        r2.A04 = false;
        r2.setThumbnail(C12980iv.A0L(r2.getContext(), R.color.dark_gray));
    }

    @Override // X.AbstractC41521tf
    public void Adu(View view) {
        C60852yk r2 = this.A00;
        r2.A04 = false;
        r2.setThumbnail(new ColorDrawable(-7829368));
    }
}
