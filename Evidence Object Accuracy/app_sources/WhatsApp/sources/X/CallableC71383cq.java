package X;

import java.util.concurrent.Callable;

/* renamed from: X.3cq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class CallableC71383cq implements Callable {
    public final /* synthetic */ C56592lG A00;

    public CallableC71383cq(C56592lG r1) {
        this.A00 = r1;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: SSATransform
        jadx.core.utils.exceptions.JadxRuntimeException: Not initialized variable reg: 6, insn: 0x0067: MOVE  (r10 I:??[OBJECT, ARRAY]) = (r6 I:??[OBJECT, ARRAY]), block:B:24:0x0067
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVarsInBlock(SSATransform.java:171)
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:143)
        	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:60)
        	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
        */
    @Override // java.util.concurrent.Callable
    public final /* bridge */ /* synthetic */ java.lang.Object call() {
        /*
            r11 = this;
            X.2lG r4 = r11.A00
            java.lang.String r8 = "gaClientId"
            java.lang.String r3 = "Failed to close client id reading stream"
            X.0kx r0 = r4.A00
            X.0ky r0 = r0.A03
            X.C13020j0.A01(r0)
            android.content.Context r7 = r0.A01
            java.lang.String r0 = "ClientId should be loaded from worker thread"
            X.C13020j0.A06(r0)
            r10 = 0
            java.io.FileInputStream r6 = r7.openFileInput(r8)     // Catch: FileNotFoundException -> 0x0073, IOException -> 0x0059, all -> 0x0057
            r0 = 36
            byte[] r9 = new byte[r0]     // Catch: FileNotFoundException -> 0x0074, IOException -> 0x0055, all -> 0x0066
            r5 = 0
            int r2 = r6.read(r9, r5, r0)     // Catch: FileNotFoundException -> 0x0074, IOException -> 0x0055, all -> 0x0066
            int r0 = r6.available()     // Catch: FileNotFoundException -> 0x0074, IOException -> 0x0055, all -> 0x0066
            if (r0 <= 0) goto L_0x0034
            java.lang.String r0 = "clientId file seems corrupted, deleting it."
            r4.A0A(r0)     // Catch: FileNotFoundException -> 0x0074, IOException -> 0x0055, all -> 0x0066
        L_0x002d:
            r6.close()     // Catch: FileNotFoundException -> 0x0074, IOException -> 0x0055, all -> 0x0066
            r7.deleteFile(r8)     // Catch: FileNotFoundException -> 0x0074, IOException -> 0x0055, all -> 0x0066
            goto L_0x0076
        L_0x0034:
            r0 = 14
            if (r2 >= r0) goto L_0x003e
            java.lang.String r0 = "clientId file is empty, deleting it."
            r4.A0A(r0)     // Catch: FileNotFoundException -> 0x0074, IOException -> 0x0055, all -> 0x0066
            goto L_0x002d
        L_0x003e:
            r6.close()     // Catch: FileNotFoundException -> 0x0074, IOException -> 0x0055, all -> 0x0066
            java.lang.String r1 = new java.lang.String     // Catch: FileNotFoundException -> 0x0074, IOException -> 0x0055, all -> 0x0066
            r1.<init>(r9, r5, r2)     // Catch: FileNotFoundException -> 0x0074, IOException -> 0x0055, all -> 0x0066
            java.lang.String r0 = "Read client id from disk"
            r4.A0D(r0, r1)     // Catch: FileNotFoundException -> 0x0074, IOException -> 0x0055, all -> 0x0066
            r6.close()     // Catch: IOException -> 0x004f
            goto L_0x0053
        L_0x004f:
            r0 = move-exception
            r4.A0C(r3, r0)
        L_0x0053:
            r10 = r1
            goto L_0x007e
        L_0x0055:
            r1 = move-exception
            goto L_0x005b
        L_0x0057:
            r1 = move-exception
            goto L_0x0068
        L_0x0059:
            r1 = move-exception
            r6 = r10
        L_0x005b:
            java.lang.String r0 = "Error reading client id file, deleting it"
            r4.A0C(r0, r1)     // Catch: all -> 0x0066
            r7.deleteFile(r8)     // Catch: all -> 0x0066
            if (r6 == 0) goto L_0x007e
            goto L_0x0076
        L_0x0066:
            r1 = move-exception
            r10 = r6
        L_0x0068:
            if (r10 == 0) goto L_0x0072
            r10.close()     // Catch: IOException -> 0x006e
            throw r1
        L_0x006e:
            r0 = move-exception
            r4.A0C(r3, r0)
        L_0x0072:
            throw r1
        L_0x0073:
            r6 = r10
        L_0x0074:
            if (r6 == 0) goto L_0x007e
        L_0x0076:
            r6.close()     // Catch: IOException -> 0x007a
            goto L_0x007e
        L_0x007a:
            r0 = move-exception
            r4.A0C(r3, r0)
        L_0x007e:
            if (r10 != 0) goto L_0x0084
            java.lang.String r10 = X.C56592lG.A00(r4)
        L_0x0084:
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: X.CallableC71383cq.call():java.lang.Object");
    }
}
