package X;

import com.whatsapp.util.Log;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;

/* renamed from: X.0lb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14540lb extends AbstractC14550lc {
    public final C14410lO A00;

    public C14540lb(C14410lO r4, AbstractC14440lR r5) {
        super(new C002601e(null, new AnonymousClass01N() { // from class: X.1pF
            @Override // X.AnonymousClass01N, X.AnonymousClass01H
            public final Object get() {
                ThreadPoolExecutor A8a = AbstractC14440lR.this.A8a("MediaUploadQueue", new LinkedBlockingQueue(), 10, 10, 1, 5);
                A8a.allowCoreThreadTimeOut(true);
                return A8a;
            }
        }));
        this.A00 = r4;
    }

    /* renamed from: A06 */
    public boolean A05(AbstractC14470lU r3) {
        StringBuilder sb = new StringBuilder("mediauploadqueue/cancel ");
        sb.append(r3);
        Log.i(sb.toString());
        return super.A05(r3);
    }
}
