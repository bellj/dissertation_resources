package X;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/* renamed from: X.3dw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C72053dw extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ C16660pY this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C72053dw(C16660pY r2) {
        super(0);
        this.this$0 = r2;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        int size;
        Integer valueOf;
        Set singleton = Collections.singleton("com.bloks.www.whatsapp.commerce.address_message");
        C16700pc.A0B(singleton);
        Collection<C91284Rd> values = ((Map) this.this$0.A01.getValue()).values();
        ArrayList A0E = C16760pi.A0E(values);
        for (C91284Rd r0 : values) {
            A0E.add(r0.A01);
        }
        ArrayList A0l = C12960it.A0l();
        Iterator it = A0E.iterator();
        while (it.hasNext()) {
            Object next = it.next();
            if (!C16700pc.A0O(next, "com.bloks.www.whatsapp.commerce.galaxy_message")) {
                A0l.add(next);
            }
        }
        if (!(A0l instanceof Collection) || (valueOf = Integer.valueOf(A0l.size())) == null) {
            size = singleton.size() << 1;
        } else {
            size = valueOf.intValue() + singleton.size();
        }
        LinkedHashSet linkedHashSet = new LinkedHashSet(C17540qy.A03(size));
        linkedHashSet.addAll(singleton);
        AnonymousClass01Z.A0B(A0l, linkedHashSet);
        return linkedHashSet;
    }
}
