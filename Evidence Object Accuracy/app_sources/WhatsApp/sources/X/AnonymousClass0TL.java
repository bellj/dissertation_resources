package X;

import java.util.ArrayList;

/* renamed from: X.0TL  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0TL {
    public static final C05850Rf A00 = C05850Rf.A00("k", "x", "y");

    public static C08130ad A00(C05540Py r9, AbstractC08850bx r10) {
        ArrayList arrayList = new ArrayList();
        if (r10.A05() == EnumC03770Jb.BEGIN_ARRAY) {
            r10.A09();
            while (r10.A0H()) {
                boolean z = false;
                if (r10.A05() == EnumC03770Jb.BEGIN_OBJECT) {
                    z = true;
                }
                arrayList.add(new AnonymousClass0HJ(r9, AnonymousClass0U2.A01(r9, C08310av.A00, r10, AnonymousClass0UV.A00(), z, false)));
            }
            r10.A0B();
            AnonymousClass0TN.A01(arrayList);
        } else {
            arrayList.add(new AnonymousClass0U8(AnonymousClass0UD.A02(r10, AnonymousClass0UV.A00())));
        }
        return new C08130ad(arrayList);
    }

    public static AbstractC12590iA A01(C05540Py r7, AbstractC08850bx r8) {
        r8.A0A();
        C08130ad r5 = null;
        AnonymousClass0H9 r3 = null;
        AnonymousClass0H9 r2 = null;
        boolean z = false;
        while (r8.A05() != EnumC03770Jb.END_OBJECT) {
            int A04 = r8.A04(A00);
            if (A04 == 0) {
                r5 = A00(r7, r8);
            } else if (A04 != 1) {
                if (A04 != 2) {
                    r8.A0D();
                    r8.A0E();
                } else if (r8.A05() != EnumC03770Jb.STRING) {
                    r2 = AnonymousClass0TE.A01(r7, r8, true);
                } else {
                    r8.A0E();
                    z = true;
                }
            } else if (r8.A05() != EnumC03770Jb.STRING) {
                r3 = AnonymousClass0TE.A01(r7, r8, true);
            } else {
                r8.A0E();
                z = true;
            }
        }
        r8.A0C();
        if (z) {
            AnonymousClass0R5.A00("Lottie doesn't support expressions.");
            r7.A0E.add("Lottie doesn't support expressions.");
        }
        if (r5 != null) {
            return r5;
        }
        return new C08120ac(r3, r2);
    }
}
