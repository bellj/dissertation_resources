package X;

import com.whatsapp.util.Log;

/* renamed from: X.2NK  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2NK implements Runnable, AbstractC32491cF {
    public final AbstractC14640lm A00;
    public final AnonymousClass1P4 A01;

    public AnonymousClass2NK(AbstractC14640lm r1, AnonymousClass1P4 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC32491cF
    public void Aaw(int i) {
        StringBuilder sb = new StringBuilder("locationsunsubscriberesponsehandler/error ");
        sb.append(i);
        Log.e(sb.toString());
    }

    @Override // java.lang.Runnable
    public void run() {
        Log.i("locationsunsubscriberesponsehandler/success");
    }
}
