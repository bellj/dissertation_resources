package X;

import android.app.ActivityManager;
import android.app.ApplicationExitInfo;
import android.content.SharedPreferences;
import android.os.Build;
import com.facebook.redex.RunnableBRunnable0Shape5S0100000_I0_5;
import com.whatsapp.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ListIterator;

/* renamed from: X.0yG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21960yG {
    public final AnonymousClass01d A00;
    public final C14820m6 A01;
    public final C16120oU A02;
    public final AbstractC14440lR A03;

    public C21960yG(AnonymousClass01d r1, C14820m6 r2, C16120oU r3, AbstractC14440lR r4) {
        this.A00 = r1;
        this.A03 = r4;
        this.A02 = r3;
        this.A01 = r2;
    }

    public void A00() {
        if (Build.VERSION.SDK_INT >= 30) {
            this.A03.Ab2(new RunnableBRunnable0Shape5S0100000_I0_5(this, 41));
        }
    }

    public final void A01() {
        int i;
        ActivityManager A03 = this.A00.A03();
        if (A03 == null) {
            Log.e("Android11ExitReasonReporter Could not get activity manager");
            return;
        }
        ListIterator<ApplicationExitInfo> listIterator = A03.getHistoricalProcessExitReasons(null, 0, 100).listIterator();
        ArrayList arrayList = new ArrayList();
        HashMap hashMap = new HashMap();
        C14820m6 r8 = this.A01;
        SharedPreferences sharedPreferences = r8.A00;
        long j = sharedPreferences.getLong("last_exit_reason_sync_timestamp", -1);
        while (listIterator.hasNext()) {
            ApplicationExitInfo next = listIterator.next();
            if (next.getTimestamp() <= j) {
                break;
            }
            arrayList.add(next);
            Integer valueOf = Integer.valueOf(next.getReason());
            if (hashMap.containsKey(valueOf)) {
                i = Integer.valueOf(((Number) hashMap.get(valueOf)).intValue() + 1);
            } else {
                i = 1;
            }
            hashMap.put(valueOf, i);
        }
        ListIterator listIterator2 = arrayList.listIterator(arrayList.size());
        while (listIterator2.hasPrevious()) {
            ApplicationExitInfo applicationExitInfo = (ApplicationExitInfo) listIterator2.previous();
            AnonymousClass313 r7 = new AnonymousClass313();
            r7.A00 = Boolean.valueOf(ActivityManager.isLowMemoryKillReportSupported());
            r7.A01 = Double.valueOf((double) applicationExitInfo.getPss());
            r7.A04 = Long.valueOf((long) applicationExitInfo.getReason());
            r7.A07 = applicationExitInfo.getDescription();
            r7.A05 = Long.valueOf(applicationExitInfo.getTimestamp());
            r7.A02 = Double.valueOf((double) applicationExitInfo.getRss());
            r7.A06 = Long.valueOf((long) applicationExitInfo.getStatus());
            r7.A03 = Long.valueOf((long) applicationExitInfo.getImportance());
            try {
                InputStream traceInputStream = applicationExitInfo.getTraceInputStream();
                if (traceInputStream != null) {
                    while (new BufferedReader(new InputStreamReader(traceInputStream)).readLine() != null) {
                    }
                }
            } catch (IOException e) {
                Log.e("Android11ExitReasonReporter/could not get exit info", e);
            }
            this.A02.A07(r7);
            r8.A0q("last_exit_reason_sync_timestamp", applicationExitInfo.getTimestamp());
        }
        AnonymousClass43L r2 = new AnonymousClass43L();
        r2.A01 = hashMap.toString();
        r2.A00 = Long.valueOf(sharedPreferences.getLong("last_exit_reason_sync_timestamp", -1));
        this.A02.A07(r2);
    }
}
