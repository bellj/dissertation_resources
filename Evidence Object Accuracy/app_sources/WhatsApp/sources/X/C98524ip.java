package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.HashSet;

/* renamed from: X.4ip  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98524ip implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new C78913pk[i];
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        HashSet A12 = C12970iu.A12();
        ArrayList arrayList = null;
        int i = 0;
        C78923pl r4 = null;
        int i2 = 0;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            int i3 = 1;
            if (c != 1) {
                i3 = 2;
                if (c != 2) {
                    i3 = 3;
                    if (c != 3) {
                        i3 = 4;
                        if (c != 4) {
                            C95664e9.A0D(parcel, readInt);
                        } else {
                            r4 = (C78923pl) C95664e9.A07(parcel, C78923pl.CREATOR, readInt);
                        }
                    } else {
                        i2 = C95664e9.A02(parcel, readInt);
                    }
                } else {
                    arrayList = C95664e9.A0B(parcel, C78933pm.CREATOR, readInt);
                }
            } else {
                i = C95664e9.A02(parcel, readInt);
            }
            C12980iv.A1R(A12, i3);
        }
        if (parcel.dataPosition() == A01) {
            return new C78913pk(r4, arrayList, A12, i, i2);
        }
        throw new C113235Gs(parcel, C12960it.A0e("Overread allowed size end=", C12980iv.A0t(37), A01));
    }
}
