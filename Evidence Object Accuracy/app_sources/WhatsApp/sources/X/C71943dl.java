package X;

import com.whatsapp.catalogsearch.view.adapter.CatalogSearchResultsListAdapter;
import com.whatsapp.catalogsearch.view.fragment.CatalogSearchFragment;
import com.whatsapp.catalogsearch.view.viewmodel.CatalogSearchViewModel;
import com.whatsapp.jid.UserJid;

/* renamed from: X.3dl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C71943dl extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ CatalogSearchFragment this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C71943dl(CatalogSearchFragment catalogSearchFragment) {
        super(0);
        this.this$0 = catalogSearchFragment;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        CatalogSearchFragment catalogSearchFragment = this.this$0;
        AnonymousClass4JC r0 = catalogSearchFragment.A0E;
        if (r0 != null) {
            UserJid userJid = catalogSearchFragment.A0P;
            if (userJid == null) {
                throw C16700pc.A06("bizJid");
            }
            AnonymousClass3VZ r12 = new AnonymousClass3VZ(catalogSearchFragment);
            AnonymousClass3VV r11 = new AnonymousClass5TV() { // from class: X.3VV
                @Override // X.AnonymousClass5TV
                public final void AUH(C44691zO r10, int i) {
                    CatalogSearchFragment catalogSearchFragment2 = CatalogSearchFragment.this;
                    catalogSearchFragment2.A0R = C16700pc.A0N(catalogSearchFragment2, r10);
                    CatalogSearchViewModel A0X = C12990iw.A0X(catalogSearchFragment2);
                    UserJid userJid2 = catalogSearchFragment2.A0P;
                    if (userJid2 == null) {
                        throw C16700pc.A06("bizJid");
                    }
                    int i2 = catalogSearchFragment2.A00;
                    String str = r10.A0D;
                    C16700pc.A0B(str);
                    AnonymousClass1CR r3 = A0X.A03;
                    Integer valueOf = Integer.valueOf(i);
                    Integer A0h = C12970iu.A0h();
                    int i3 = 2;
                    if (i2 == 0) {
                        i3 = 1;
                    } else if (i2 == 1) {
                        i3 = 3;
                    } else if (i2 != 2) {
                        i3 = -1;
                    }
                    r3.A01.A00(new AnonymousClass3VK(r3, userJid2, A0h, Integer.valueOf(i3), valueOf, str), userJid2);
                }
            };
            C71573d9 r3 = r0.A00;
            AnonymousClass01J r2 = r3.A04;
            C14900mE A0R = C12970iu.A0R(r2);
            C15570nT A0S = C12970iu.A0S(r2);
            C15550nR A0O = C12960it.A0O(r2);
            C15610nY A0P = C12960it.A0P(r2);
            AnonymousClass018 A0R2 = C12960it.A0R(r2);
            C22700zV A0a = C12980iv.A0a(r2);
            return new CatalogSearchResultsListAdapter(catalogSearchFragment, C12980iv.A0W(r2), A0R, A0S, C12980iv.A0Z(r2), (AnonymousClass19T) r2.A2y.get(), new C37071lG(C12990iw.A0V(r3.A01.A1E)), r11, r12, A0O, A0a, A0P, A0R2, userJid);
        }
        throw C16700pc.A06("searchResultAdapterFactory");
    }
}
