package X;

import java.util.Arrays;

/* renamed from: X.4cs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C94994cs {
    public static final C94994cs A05 = new C94994cs(new int[0], new Object[0], 0, false);
    public int A00;
    public int A01 = -1;
    public boolean A02;
    public int[] A03;
    public Object[] A04;

    public C94994cs(int[] iArr, Object[] objArr, int i, boolean z) {
        this.A00 = i;
        this.A03 = iArr;
        this.A04 = objArr;
        this.A02 = z;
    }

    public static C94994cs A00() {
        return new C94994cs(new int[8], new Object[8], 0, true);
    }

    public final int A01() {
        int i;
        int i2;
        int i3;
        int i4 = this.A01;
        if (i4 != -1) {
            return i4;
        }
        int i5 = 0;
        for (int i6 = 0; i6 < this.A00; i6++) {
            int i7 = this.A03[i6];
            int i8 = i7 >>> 3;
            int i9 = i7 & 7;
            if (i9 != 0) {
                if (i9 == 1) {
                    i3 = AnonymousClass4DU.A04(i8) + 8;
                } else if (i9 == 2) {
                    i = AnonymousClass4DU.A04(i8);
                    int A02 = ((AbstractC111925Bi) this.A04[i6]).A02();
                    i2 = C72453ed.A07(A02) + A02;
                } else if (i9 == 3) {
                    i = AnonymousClass4DU.A04(i8) << 1;
                    i2 = ((C94994cs) this.A04[i6]).A01();
                } else if (i9 == 5) {
                    i3 = AnonymousClass4DU.A04(i8) + 4;
                } else {
                    throw new IllegalStateException(new C80443sG());
                }
                i5 += i3;
            } else {
                long A0G = C12980iv.A0G(this.A04[i6]);
                i = AnonymousClass4DU.A04(i8);
                i2 = C80253rx.A01(A0G);
            }
            i3 = i + i2;
            i5 += i3;
        }
        this.A01 = i5;
        return i5;
    }

    public final void A02(int i, Object obj) {
        if (this.A02) {
            int i2 = this.A00;
            int[] iArr = this.A03;
            if (i2 == iArr.length) {
                int i3 = i2 >> 1;
                if (i2 < 4) {
                    i3 = 8;
                }
                int i4 = i2 + i3;
                this.A03 = Arrays.copyOf(iArr, i4);
                this.A04 = Arrays.copyOf(this.A04, i4);
            }
            int[] iArr2 = this.A03;
            int i5 = this.A00;
            iArr2[i5] = i;
            this.A04[i5] = obj;
            this.A00 = i5 + 1;
            return;
        }
        throw C12970iu.A0z();
    }

    public final void A03(AbstractC115295Qy r8) {
        if (this.A00 != 0) {
            for (int i = 0; i < this.A00; i++) {
                int i2 = this.A03[i];
                Object obj = this.A04[i];
                int i3 = i2 >>> 3;
                int i4 = i2 & 7;
                if (i4 == 0) {
                    long A0G = C12980iv.A0G(obj);
                    C80253rx r1 = ((C1090250c) r8).A00;
                    AnonymousClass4DU.A07(r1, i3, 0);
                    r1.A09(A0G);
                } else if (i4 == 1) {
                    long A0G2 = C12980iv.A0G(obj);
                    C80253rx r0 = ((C1090250c) r8).A00;
                    AnonymousClass4DU.A07(r0, i3, 1);
                    r0.A0A(A0G2);
                } else if (i4 == 2) {
                    ((C1090250c) r8).A00.A0B((AbstractC111925Bi) obj, i3);
                } else if (i4 == 3) {
                    C80253rx r2 = ((C1090250c) r8).A00;
                    int i5 = i3 << 3;
                    r2.A05(i5 | 3);
                    ((C94994cs) obj).A03(r8);
                    r2.A05(i5 | 4);
                } else if (i4 == 5) {
                    int A052 = C12960it.A05(obj);
                    C80253rx r02 = ((C1090250c) r8).A00;
                    AnonymousClass4DU.A07(r02, i3, 5);
                    r02.A06(A052);
                } else {
                    throw new RuntimeException(new C80443sG());
                }
            }
        }
    }

    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj != null && (obj instanceof C94994cs)) {
                C94994cs r9 = (C94994cs) obj;
                int i = this.A00;
                if (i == r9.A00) {
                    int[] iArr = this.A03;
                    int[] iArr2 = r9.A03;
                    int i2 = 0;
                    while (true) {
                        if (i2 < i) {
                            if (iArr[i2] != iArr2[i2]) {
                                break;
                            }
                            i2++;
                        } else {
                            Object[] objArr = this.A04;
                            Object[] objArr2 = r9.A04;
                            for (int i3 = 0; i3 < i; i3++) {
                                if (objArr[i3].equals(objArr2[i3])) {
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    public final int hashCode() {
        int i = this.A00;
        int i2 = (i + 527) * 31;
        int[] iArr = this.A03;
        int i3 = 17;
        int i4 = 17;
        for (int i5 = 0; i5 < i; i5++) {
            i4 = (i4 * 31) + iArr[i5];
        }
        int i6 = (i2 + i4) * 31;
        Object[] objArr = this.A04;
        for (int i7 = 0; i7 < i; i7++) {
            i3 = C12990iw.A08(objArr[i7], i3 * 31);
        }
        return i6 + i3;
    }
}
