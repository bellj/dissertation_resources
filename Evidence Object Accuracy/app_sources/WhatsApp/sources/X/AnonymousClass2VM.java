package X;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.facebook.redex.ViewOnClickCListenerShape4S0100000_I0_4;
import com.whatsapp.R;
import com.whatsapp.util.ViewOnClickCListenerShape4S0200000_I0;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.2VM  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2VM extends AnonymousClass02M {
    public final Resources A00;
    public final AnonymousClass2VN A01;
    public final AbstractC14440lR A02;
    public final List A03 = new ArrayList();
    public final Map A04 = new HashMap();

    public AnonymousClass2VM(Resources resources, AnonymousClass2VN r3, AbstractC14440lR r4) {
        this.A02 = r4;
        this.A00 = resources;
        this.A01 = r3;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A03.size();
    }

    public void A0E(AnonymousClass01T r5, List list, int i) {
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(new AnonymousClass47A((File) it.next()));
        }
        if (i != 0) {
            if (i == 1) {
                arrayList.add(new AnonymousClass47C(Boolean.TRUE));
                arrayList.add(new AnonymousClass47B());
            } else {
                arrayList.add(new AnonymousClass47C(Boolean.FALSE));
                Object obj = r5.A00;
                AnonymousClass009.A05(obj);
                for (Integer num : (List) obj) {
                    arrayList.add(new AnonymousClass479(num));
                }
            }
        }
        List list2 = this.A03;
        AnonymousClass0SZ A00 = AnonymousClass0RD.A00(new C74563iJ(list2, arrayList));
        list2.clear();
        list2.addAll(arrayList);
        A00.A02(this);
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r5, int i) {
        AbstractC75193jS r52 = (AbstractC75193jS) r5;
        int itemViewType = getItemViewType(i);
        if (itemViewType == 0) {
            File file = (File) ((AnonymousClass4OJ) this.A03.get(i)).A01;
            AnonymousClass478 r53 = (AnonymousClass478) r52;
            AnonymousClass37N r1 = r53.A00;
            if (r1 != null) {
                r1.A03(true);
            }
            AnonymousClass37N r2 = new AnonymousClass37N(r53.A0H.getContext(), r53.A01, file);
            r53.A00 = r2;
            r53.A02.Aaz(r2, new Void[0]);
            this.A04.put(Integer.valueOf(i), r53.A00);
        } else if (itemViewType == 1) {
            AnonymousClass478 r54 = (AnonymousClass478) r52;
            int intValue = ((Number) ((AnonymousClass4OJ) this.A03.get(i)).A01).intValue();
            Resources resources = this.A00;
            AnonymousClass009.A05(resources);
            Drawable drawable = resources.getDrawable(intValue);
            AnonymousClass37N r0 = r54.A00;
            if (r0 != null) {
                r0.A03(true);
                r54.A00 = null;
            }
            r54.A01.setImageDrawable(drawable);
        } else if (itemViewType == 2) {
            AnonymousClass477 r55 = (AnonymousClass477) r52;
            int i2 = 4;
            if (((Boolean) ((AnonymousClass4OJ) this.A03.get(i)).A01).booleanValue()) {
                i2 = 0;
            }
            r55.A00.setVisibility(i2);
            r55.A01.setVisibility(i2);
        }
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        if (i == 0 || i == 1) {
            AbstractC14440lR r2 = this.A02;
            C74213hb r1 = new C74213hb(context);
            r1.setScaleType(ImageView.ScaleType.CENTER_CROP);
            AnonymousClass478 r3 = new AnonymousClass478(r1, r2);
            r3.A0H.setOnClickListener(new ViewOnClickCListenerShape4S0200000_I0(r3, 29, this));
            return r3;
        }
        LayoutInflater from = LayoutInflater.from(context);
        if (i == 3) {
            return new AnonymousClass476(from.inflate(R.layout.downloadable_wallpaper_footer_view, (ViewGroup) null));
        }
        AnonymousClass477 r32 = new AnonymousClass477(from.inflate(R.layout.downloadable_wallpaper_header_view, (ViewGroup) null));
        r32.A0H.setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(this, 31));
        return r32;
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        return ((AnonymousClass4OJ) this.A03.get(i)).A00;
    }
}
