package X;

import android.widget.SeekBar;
import android.widget.TextView;
import androidx.preference.SeekBarPreference;

/* renamed from: X.0X8  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0X8 implements SeekBar.OnSeekBarChangeListener {
    public final /* synthetic */ SeekBarPreference A00;

    public AnonymousClass0X8(SeekBarPreference seekBarPreference) {
        this.A00 = seekBarPreference;
    }

    @Override // android.widget.SeekBar.OnSeekBarChangeListener
    public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
        if (z) {
            SeekBarPreference seekBarPreference = this.A00;
            if (seekBarPreference.A0B || !seekBarPreference.A0A) {
                seekBarPreference.A0T(seekBar);
                return;
            }
        }
        SeekBarPreference seekBarPreference2 = this.A00;
        int i2 = i + seekBarPreference2.A01;
        TextView textView = seekBarPreference2.A07;
        if (textView != null) {
            textView.setText(String.valueOf(i2));
        }
    }

    @Override // android.widget.SeekBar.OnSeekBarChangeListener
    public void onStartTrackingTouch(SeekBar seekBar) {
        this.A00.A0A = true;
    }

    @Override // android.widget.SeekBar.OnSeekBarChangeListener
    public void onStopTrackingTouch(SeekBar seekBar) {
        SeekBarPreference seekBarPreference = this.A00;
        seekBarPreference.A0A = false;
        if (seekBar.getProgress() + seekBarPreference.A01 != seekBarPreference.A03) {
            seekBarPreference.A0T(seekBar);
        }
    }
}
