package X;

import android.view.ViewTreeObserver;

/* renamed from: X.3Nr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ViewTreeObserver$OnPreDrawListenerC66463Nr implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AnonymousClass238 A01;
    public final /* synthetic */ AnonymousClass2AS A02;

    public ViewTreeObserver$OnPreDrawListenerC66463Nr(AnonymousClass238 r1, AnonymousClass2AS r2, int i) {
        this.A01 = r1;
        this.A02 = r2;
        this.A00 = i;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        AnonymousClass238 r5 = this.A01;
        if (!r5.A0G) {
            AnonymousClass2AS r1 = this.A02;
            if (r1.A09.A02 || r1.A0A.A02) {
                r5.A0G = true;
                r5.A02.requestLayout();
            }
            return false;
        }
        C12980iv.A1G(r5.A02, this);
        int i = r5.A02.getLayoutParams().height;
        int height = r5.A02.getHeight();
        r5.A02.getLayoutParams().height = this.A00;
        r5.A02.requestLayout();
        int transcriptMode = r5.A03.getTranscriptMode();
        C52622bP r3 = new C52622bP(this, i, height);
        C58142oD r2 = new C58142oD(this, transcriptMode);
        r3.setDuration(300);
        r3.setAnimationListener(r2);
        r5.A02.startAnimation(r3);
        return false;
    }
}
