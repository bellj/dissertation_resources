package X;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.CircularProgressBar;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaImageView;
import java.io.File;
import java.util.Set;

/* renamed from: X.2yb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60762yb extends AbstractC60582yG {
    public C50362Pg A00;
    public final View A01;
    public final View A02;
    public final View A03 = findViewById(R.id.control_btn_holder);
    public final View A04;
    public final View A05;
    public final FrameLayout A06;
    public final ImageView A07 = C12970iu.A0L(this, R.id.icon);
    public final ImageView A08;
    public final TextView A09;
    public final TextView A0A;
    public final TextView A0B;
    public final TextView A0C;
    public final CircularProgressBar A0D;
    public final TextEmojiLabel A0E;
    public final TextEmojiLabel A0F;
    public final WaImageView A0G = ((WaImageView) findViewById(R.id.control_btn));
    public final AbstractC41521tf A0H = new C70173ar(this);

    public C60762yb(Context context, AbstractC13890kV r5, C16440p1 r6) {
        super(context, r5, r6);
        CircularProgressBar circularProgressBar = (CircularProgressBar) findViewById(R.id.progressbar);
        this.A0D = circularProgressBar;
        circularProgressBar.setMax(100);
        circularProgressBar.A0C = AnonymousClass00T.A00(context, R.color.media_message_progress_determinate);
        circularProgressBar.A0B = AnonymousClass00T.A00(context, R.color.circular_progress_bar_background);
        this.A0F = C12970iu.A0U(this, R.id.title);
        this.A0C = C12960it.A0I(this, R.id.media_transfer_eta);
        this.A02 = findViewById(R.id.content);
        this.A0B = C12960it.A0J(this, R.id.info);
        this.A01 = findViewById(R.id.bullet_info);
        this.A09 = C12960it.A0J(this, R.id.file_size);
        this.A0A = C12960it.A0J(this, R.id.file_type);
        this.A08 = C12970iu.A0L(this, R.id.preview);
        this.A04 = findViewById(R.id.preview_separator);
        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.document_frame);
        this.A06 = frameLayout;
        TextEmojiLabel A0U = C12970iu.A0U(this, R.id.caption);
        this.A0E = A0U;
        if (A0U != null) {
            A0U.setLongClickable(AbstractC28491Nn.A07(A0U));
        }
        this.A05 = findViewById(R.id.text_and_date);
        if (frameLayout != null) {
            frameLayout.setForeground(getInnerFrameForegroundDrawable());
        }
        A1P();
    }

    @Override // X.AnonymousClass1OY
    public void A0s() {
        A1P();
        A1H(false);
    }

    @Override // X.AbstractC42671vd, X.AnonymousClass1OY
    public void A0y() {
        Activity A02 = AnonymousClass12P.A02(this);
        if (A02 instanceof ActivityC13810kN) {
            C16440p1 r7 = (C16440p1) ((AbstractC16130oV) ((AbstractC28551Oa) this).A0O);
            AnonymousClass1CY r8 = ((AbstractC28551Oa) this).A0P;
            AnonymousClass009.A05(r8);
            C14900mE r5 = ((AnonymousClass1OY) this).A0J;
            AnonymousClass009.A05(r5);
            AbstractC15710nm r3 = ((AbstractC28551Oa) this).A0F;
            AnonymousClass009.A05(r3);
            AbstractC14440lR r9 = this.A1P;
            AnonymousClass009.A05(r9);
            AnonymousClass009.A05(((AnonymousClass1OY) this).A0N);
            AnonymousClass12P r2 = ((AnonymousClass1OY) this).A0I;
            AnonymousClass009.A05(r2);
            C15670ni r6 = this.A0z;
            AnonymousClass009.A05(r6);
            ActivityC13810kN r4 = (ActivityC13810kN) A02;
            C15890o4 r0 = ((AbstractC42671vd) this).A01;
            AnonymousClass009.A05(r0);
            if (RequestPermissionActivity.A0W(r4, r0)) {
                C16150oX A00 = AbstractC15340mz.A00(r7);
                if (r7.A0z.A02 || A00.A0P) {
                    File file = A00.A0F;
                    if (file == null || !file.exists()) {
                        A1O();
                    } else {
                        C26511Dt.A06(r2, r3, r4, r5, r6, r7, r8, r9);
                    }
                }
            }
        }
    }

    @Override // X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r2, boolean z) {
        if (!(r2 instanceof AbstractC30301Ww)) {
            boolean A1X = C12960it.A1X(r2, ((AbstractC28551Oa) this).A0O);
            super.A1D(r2, z);
            if (z || A1X) {
                A1P();
            }
        }
    }

    @Override // X.AbstractC42671vd
    public void A1M(View view, TextEmojiLabel textEmojiLabel, String str) {
        super.A1M(view, textEmojiLabel, str);
        if (TextUtils.isEmpty(str) && textEmojiLabel != null) {
            ViewGroup viewGroup = ((AnonymousClass1OY) this).A05;
            viewGroup.setPadding(getResources().getDimensionPixelSize(R.dimen.conversation_image_date_padding_right_on_media), 0, C12990iw.A07(this, R.dimen.conversation_image_date_padding_right_on_media), 0);
            C12970iu.A1F(viewGroup);
            ((AnonymousClass1OY) this).A0E.setTextColor(getSecondaryTextColor());
            View view2 = this.A05;
            if (view2 != null) {
                C12970iu.A0H(view2).topMargin = (-viewGroup.getMeasuredHeight()) - getResources().getDimensionPixelSize(R.dimen.conversation_document_info_view_bottom_padding);
            }
        }
    }

    public final void A1P() {
        CharSequence A0q;
        View view;
        C16440p1 r9 = (C16440p1) ((AbstractC16130oV) ((AbstractC28551Oa) this).A0O);
        AnonymousClass009.A05(((AbstractC16130oV) r9).A02);
        this.A07.setImageDrawable(C26511Dt.A02(getContext(), r9));
        String A16 = r9.A16();
        if (TextUtils.isEmpty(A16)) {
            A0q = getContext().getString(R.string.untitled_document);
        } else {
            A0q = A0q(AnonymousClass1US.A04(150, A16));
        }
        this.A0F.setText(A0q);
        C16460p3 A0G = r9.A0G();
        AnonymousClass009.A05(A0G);
        AbstractView$OnClickListenerC34281fs r1 = null;
        if (A0G.A04()) {
            this.A1O.A0A(this.A08, r9, this.A0H, r9.A0z, 480, false, false);
        } else {
            ImageView imageView = this.A08;
            imageView.setTag(null);
            imageView.setVisibility(8);
            this.A04.setVisibility(8);
        }
        A1M(this.A05, this.A0E, r9.A01);
        AbstractC16130oV fMessage = getFMessage();
        if (C30041Vv.A12(fMessage)) {
            this.A03.setVisibility(0);
            WaImageView waImageView = this.A0G;
            waImageView.setImageResource(R.drawable.inline_audio_cancel);
            waImageView.setOnClickListener(((AbstractC42671vd) this).A07);
            AnonymousClass23N.A02(waImageView, R.string.cancel);
            if (!r9.A0z.A02 || r9.A12 < -1) {
                C12960it.A0r(getContext(), waImageView, R.string.tb_button_downloading);
                view = this.A02;
            } else {
                C12960it.A0r(getContext(), waImageView, R.string.tb_button_uploading);
                view = this.A02;
                r1 = ((AbstractC42671vd) this).A0A;
            }
        } else {
            boolean A13 = C30041Vv.A13(fMessage);
            WaImageView waImageView2 = this.A0G;
            AnonymousClass028.A0g(waImageView2, new AnonymousClass04v());
            View view2 = this.A03;
            if (A13) {
                view2.setVisibility(8);
            } else {
                view2.setVisibility(0);
                if (C30041Vv.A11(getFMessage())) {
                    waImageView2.setImageResource(R.drawable.inline_audio_download);
                    C12960it.A0r(getContext(), waImageView2, R.string.button_download);
                    r1 = ((AbstractC42671vd) this).A08;
                    waImageView2.setOnClickListener(r1);
                    view = this.A02;
                } else {
                    waImageView2.setImageResource(R.drawable.inline_audio_upload);
                    C12960it.A0r(getContext(), waImageView2, R.string.retry);
                    waImageView2.setOnClickListener(((AbstractC42671vd) this).A09);
                }
            }
            view = this.A02;
            r1 = ((AbstractC42671vd) this).A0A;
        }
        view.setOnClickListener(r1);
        this.A0C.setVisibility(8);
        A0w();
        this.A09.setText(C44891zj.A03(((AbstractC28551Oa) this).A0K, ((AbstractC16130oV) r9).A01));
        int i = r9.A00;
        TextView textView = this.A0B;
        if (i != 0) {
            textView.setVisibility(0);
            this.A01.setVisibility(0);
            textView.setText(C26511Dt.A05(((AbstractC28551Oa) this).A0K, r9));
        } else {
            textView.setVisibility(8);
            this.A01.setVisibility(8);
        }
        this.A0A.setText(A0q(AnonymousClass1US.A04(10, AbstractC15340mz.A01(r9))));
        view.setOnLongClickListener(this.A1a);
        A1N(r9);
    }

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_document_left;
    }

    @Override // X.AbstractC42671vd, X.AbstractC28551Oa
    public C16440p1 getFMessage() {
        return (C16440p1) ((AbstractC16130oV) ((AbstractC28551Oa) this).A0O);
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_document_left;
    }

    @Override // X.AnonymousClass1OY
    public Set getInnerFrameLayouts() {
        Set innerFrameLayouts = super.getInnerFrameLayouts();
        FrameLayout frameLayout = this.A06;
        if (frameLayout != null) {
            innerFrameLayouts.add(frameLayout);
        }
        return innerFrameLayouts;
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_document_right;
    }

    @Override // X.AbstractC42671vd, X.AbstractC28551Oa
    public void setFMessage(AbstractC15340mz r2) {
        AnonymousClass009.A0F(r2 instanceof C16440p1);
        super.setFMessage(r2);
    }
}
