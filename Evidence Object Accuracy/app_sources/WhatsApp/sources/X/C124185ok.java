package X;

import android.net.Uri;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiQrTabActivity;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.lang.ref.WeakReference;

/* renamed from: X.5ok  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124185ok extends AbstractC16350or {
    public final int A00;
    public final int A01;
    public final Uri A02;
    public final C22190yg A03;
    public final WeakReference A04;

    public C124185ok(Uri uri, IndiaUpiQrTabActivity indiaUpiQrTabActivity, C22190yg r4, int i, int i2) {
        this.A03 = r4;
        this.A02 = uri;
        this.A01 = i;
        this.A00 = i2;
        this.A04 = C12970iu.A10(indiaUpiQrTabActivity);
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        int max = Math.max(this.A01, this.A00);
        try {
            return this.A03.A07(this.A02, max, max);
        } catch (C39351pj | IOException e) {
            Log.e("IndiaUpiQrTabActivity/loadImageRunnable Failed to load image", e);
            return null;
        }
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        IndiaUpiQrTabActivity indiaUpiQrTabActivity = (IndiaUpiQrTabActivity) this.A04.get();
        if (indiaUpiQrTabActivity != null && !indiaUpiQrTabActivity.AJN()) {
            Uri uri = this.A02;
            if (obj == null) {
                indiaUpiQrTabActivity.AaN();
                ((ActivityC13810kN) indiaUpiQrTabActivity).A05.A07(R.string.error_load_image, 0);
                return;
            }
            C12990iw.A1N(new AnonymousClass391(uri, indiaUpiQrTabActivity.A0E, indiaUpiQrTabActivity.A0C), ((ActivityC13830kP) indiaUpiQrTabActivity).A05);
        }
    }
}
