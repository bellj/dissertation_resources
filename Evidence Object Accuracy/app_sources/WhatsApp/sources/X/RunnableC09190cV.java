package X;

import android.content.Context;
import android.util.Log;
import androidx.biometric.FingerprintDialogFragment;
import com.whatsapp.R;

/* renamed from: X.0cV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09190cV implements Runnable {
    public final /* synthetic */ FingerprintDialogFragment A00;

    public RunnableC09190cV(FingerprintDialogFragment fingerprintDialogFragment) {
        this.A00 = fingerprintDialogFragment;
    }

    @Override // java.lang.Runnable
    public void run() {
        FingerprintDialogFragment fingerprintDialogFragment = this.A00;
        Context A0p = fingerprintDialogFragment.A0p();
        if (A0p == null) {
            Log.w("FingerprintFragment", "Not resetting the dialog. Context is null.");
            return;
        }
        fingerprintDialogFragment.A04.A04(1);
        fingerprintDialogFragment.A04.A05(A0p.getString(R.string.fingerprint_dialog_touch_sensor));
    }
}
