package X;

import java.util.Map;

/* renamed from: X.4BL  reason: invalid class name */
/* loaded from: classes3.dex */
public enum AnonymousClass4BL {
    OBJECT(2, 0),
    /* Fake field, exist only in values array */
    BOOLEAN(4, 1),
    /* Fake field, exist only in values array */
    CHAR(5, 2),
    /* Fake field, exist only in values array */
    FLOAT(6, 4),
    /* Fake field, exist only in values array */
    DOUBLE(7, 8),
    /* Fake field, exist only in values array */
    BYTE(8, 1),
    /* Fake field, exist only in values array */
    SHORT(9, 2),
    /* Fake field, exist only in values array */
    INT(10, 4),
    /* Fake field, exist only in values array */
    LONG(11, 8);
    
    public static final Map A00 = C12970iu.A11();
    public final int size;
    public final int typeId;

    static {
        AnonymousClass4BL[] values = values();
        for (AnonymousClass4BL r2 : values) {
            A00.put(Integer.valueOf(r2.typeId), r2);
        }
    }

    AnonymousClass4BL(int i, int i2) {
        this.typeId = i;
        this.size = i2;
    }
}
