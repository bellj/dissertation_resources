package X;

import java.util.Vector;

/* renamed from: X.0IF  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0IF extends AnonymousClass0eL {
    @Override // X.AnonymousClass0eL, java.lang.Runnable
    public void run() {
        Vector vector = C06090Sd.A02;
        synchronized (vector) {
            int size = vector.size();
            for (int i = 0; i < size; i++) {
                ((C06090Sd) vector.get(i)).A01();
            }
        }
    }
}
