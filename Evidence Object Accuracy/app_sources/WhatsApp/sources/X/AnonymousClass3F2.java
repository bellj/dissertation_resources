package X;

/* renamed from: X.3F2  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3F2 {
    public final C78603pB A00;
    public final AnonymousClass4XF A01;

    public /* synthetic */ AnonymousClass3F2(C78603pB r1, AnonymousClass4XF r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public final boolean equals(Object obj) {
        if (obj != null && (obj instanceof AnonymousClass3F2)) {
            AnonymousClass3F2 r4 = (AnonymousClass3F2) obj;
            if (C13300jT.A00(this.A01, r4.A01) && C13300jT.A00(this.A00, r4.A00)) {
                return true;
            }
        }
        return false;
    }

    public final int hashCode() {
        Object[] A1a = C12980iv.A1a();
        A1a[0] = this.A01;
        return C12960it.A06(this.A00, A1a);
    }

    public final String toString() {
        C13290jS r2 = new C13290jS(this);
        r2.A00(this.A01, "key");
        r2.A00(this.A00, "feature");
        return r2.toString();
    }
}
