package X;

/* renamed from: X.05r  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC011805r implements Runnable {
    public final /* synthetic */ LayoutInflater$Factory2C011505o A00;

    public RunnableC011805r(LayoutInflater$Factory2C011505o r1) {
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        LayoutInflater$Factory2C011505o r2 = this.A00;
        if ((r2.A00 & 1) != 0) {
            r2.A0Q(0);
        }
        if ((r2.A00 & 4096) != 0) {
            r2.A0Q(C43951xu.A03);
        }
        r2.A0a = false;
        r2.A00 = 0;
    }
}
