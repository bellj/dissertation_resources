package X;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.WaTextView;
import java.util.List;
import java.util.Map;

/* renamed from: X.5c5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118605c5 extends AnonymousClass02M {
    public final C37071lG A00;
    public final AnonymousClass018 A01;
    public final AnonymousClass1ZD A02;
    public final Map A03 = C12970iu.A11();

    public C118605c5(C37071lG r2, AnonymousClass018 r3, AnonymousClass1ZD r4) {
        this.A02 = r4;
        this.A01 = r3;
        this.A00 = r2;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A02.A04.A08.size() + 1;
    }

    @Override // X.AnonymousClass02M
    public void ANH(AnonymousClass03U r26, int i) {
        String str;
        String str2;
        AnonymousClass1ZD r6 = this.A02;
        AnonymousClass1ZK r2 = r6.A04;
        List list = r2.A08;
        if (i < list.size()) {
            C66023Lz r7 = (C66023Lz) list.get(i);
            C118785cN r10 = (C118785cN) r26;
            AnonymousClass018 r9 = this.A01;
            C44741zT r12 = (C44741zT) this.A03.get(r7.A00());
            AnonymousClass1ZI r8 = r7.A01;
            long j = r8.A01;
            int i2 = r7.A00;
            String A03 = r6.A03(r9, new AnonymousClass1ZI(r8.A00, r8.A02, j * ((long) i2)));
            WaImageView waImageView = r10.A00;
            Resources A09 = C12960it.A09(waImageView);
            r10.A03.setText(r7.A03);
            WaTextView waTextView = r10.A02;
            Object[] A1b = C12970iu.A1b();
            C12960it.A1O(A1b, i2);
            waTextView.setText(A09.getString(R.string.order_item_quantity_in_list, A1b));
            r10.A01.setText(A03);
            if (r12 == null) {
                waImageView.setImageDrawable(new ColorDrawable(A09.getColor(R.color.wds_cool_gray_100)));
            } else {
                r10.A04.A02(waImageView, r12, null, new AnonymousClass2E5() { // from class: X.66r
                    @Override // X.AnonymousClass2E5
                    public final void AS6(Bitmap bitmap, C68203Um r4, boolean z) {
                        ImageView imageView = (ImageView) r4.A09.get();
                        if (imageView != null) {
                            imageView.setBackgroundColor(0);
                            imageView.setImageBitmap(bitmap);
                            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                        }
                    }
                }, 2);
            }
        } else {
            C118865cV r102 = (C118865cV) r26;
            AnonymousClass018 r13 = this.A01;
            AnonymousClass1ZI r4 = r2.A06;
            String A032 = r6.A03(r13, r4);
            AnonymousClass1ZI r3 = r2.A03;
            String A033 = r6.A03(r13, r3);
            AnonymousClass1ZI r5 = r2.A04;
            String A034 = r6.A03(r13, r5);
            String A035 = r6.A03(r13, r2.A05);
            String A02 = r6.A02(r13);
            String str3 = null;
            if (r4 == null) {
                str = null;
            } else {
                str = r4.A02;
            }
            if (r3 == null) {
                str2 = null;
            } else {
                str2 = r3.A02;
            }
            if (r5 != null) {
                str3 = r5.A02;
            }
            if (!TextUtils.isEmpty(A032) || !TextUtils.isEmpty(A033) || !TextUtils.isEmpty(A034)) {
                r102.A08(0);
                r102.A09(r102.A05, r102.A06, r13, null, A035, R.string.order_details_subtotal_label_text);
                r102.A09(r102.A07, r102.A08, r13, str, A032, R.string.order_details_tax_label_text);
                r102.A09(r102.A01, r102.A02, r13, str2, A033, R.string.order_details_discount_label_text);
                r102.A09(r102.A03, r102.A04, r13, str3, A034, R.string.order_details_shipping_label_text);
            } else {
                r102.A08(8);
            }
            r102.A09.setText(A02);
        }
    }

    @Override // X.AnonymousClass02M
    public AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        if (i == 0) {
            return new C118785cN(C12960it.A0E(viewGroup).inflate(R.layout.payment_checkout_order_details_item_view, viewGroup, false), this.A00);
        }
        if (i == 1) {
            return new C118865cV(C12960it.A0E(viewGroup).inflate(R.layout.payment_checkout_order_details_charges, viewGroup, false));
        }
        throw C12960it.A0U(C12960it.A0W(i, "Unsupported view type - "));
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        return C12960it.A1V(i, this.A02.A04.A08.size()) ? 1 : 0;
    }
}
