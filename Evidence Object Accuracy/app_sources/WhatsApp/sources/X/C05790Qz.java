package X;

import android.widget.ListView;

/* renamed from: X.0Qz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05790Qz {
    public static void A00(ListView listView, int i) {
        listView.scrollListBy(i);
    }

    public static boolean A01(ListView listView, int i) {
        return listView.canScrollList(i);
    }
}
