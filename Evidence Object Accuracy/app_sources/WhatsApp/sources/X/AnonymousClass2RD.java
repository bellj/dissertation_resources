package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

/* renamed from: X.2RD  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2RD extends AnimatorListenerAdapter {
    public final /* synthetic */ C50742Qt A00;

    public AnonymousClass2RD(C50742Qt r1) {
        this.A00 = r1;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        C50742Qt r1 = this.A00;
        if (r1.A00 == animator) {
            r1.A00 = null;
        }
    }
}
