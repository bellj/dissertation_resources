package X;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.TimeZone;

/* renamed from: X.3GB  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3GB {
    public static Spannable A00(Context context, C30201Wm r16, AnonymousClass018 r17, long j) {
        TimeZone timeZone;
        Object obj;
        String str;
        int i;
        Object[] objArr;
        String str2 = r16.A01;
        if (AnonymousClass1US.A0C(str2)) {
            timeZone = TimeZone.getDefault();
        } else {
            timeZone = TimeZone.getTimeZone(str2);
        }
        Calendar instance = Calendar.getInstance(timeZone);
        instance.setTimeInMillis(j);
        int i2 = instance.get(7);
        ArrayList A0l = C12960it.A0l();
        for (C30191Wl r1 : r16.A02) {
            if (r1.A00 == i2) {
                A0l.add(r1);
            }
        }
        int i3 = R.color.business_hours_closed;
        String string = context.getString(R.string.business_hours_status_closed_today);
        if (str2 == null || TimeZone.getDefault().getOffset(j) == TimeZone.getTimeZone(str2).getOffset(j)) {
            obj = "";
        } else {
            obj = context.getString(R.string.business_hours_different_time_zone);
        }
        if (!A0l.isEmpty()) {
            C30191Wl r0 = (C30191Wl) A0l.get(0);
            int i4 = r0.A00;
            int i5 = r0.A01;
            if (i5 != 0) {
                if (i5 == 1) {
                    i3 = R.color.business_hours_open;
                    str = context.getString(R.string.business_hours_status_open_24h_open);
                    i = R.string.business_hours_status_open_24h;
                } else if (i5 == 2) {
                    i3 = R.color.business_hours_open;
                    str = context.getString(R.string.business_hours_status_appointment_only_open);
                    i = R.string.business_hours_status_appointment_only;
                }
                objArr = new Object[]{str};
                string = context.getString(i, objArr);
            } else {
                int i6 = (instance.get(11) * 60) + instance.get(12);
                Iterator it = A0l.iterator();
                boolean z = true;
                while (it.hasNext()) {
                    C30191Wl r8 = (C30191Wl) it.next();
                    Integer num = r8.A03;
                    AnonymousClass009.A05(num);
                    int intValue = num.intValue();
                    Integer num2 = r8.A02;
                    AnonymousClass009.A05(num2);
                    int intValue2 = num2.intValue();
                    if (i6 < intValue) {
                        Calendar A01 = A01(instance, i4, intValue);
                        str = context.getString(R.string.business_hours_status_closed_now_opens_at_closed_now);
                        Object[] objArr2 = new Object[3];
                        objArr2[0] = str;
                        objArr2[1] = AnonymousClass3JK.A03(r17, A01);
                        string = C12960it.A0X(context, obj, objArr2, 2, R.string.business_hours_status_closed_now_opens_at);
                        break;
                    } else if (i6 > intValue2) {
                        string = context.getString(R.string.business_hours_status_closed_now);
                        z = false;
                    } else {
                        Calendar A012 = A01(instance, i4, intValue2);
                        StringBuilder A0h = C12960it.A0h();
                        A0h.append(AnonymousClass3JK.A03(r17, A012));
                        if (A0l.size() > 1 && z) {
                            A0h.append(C12960it.A0X(context, " ", new Object[1], 0, R.string.business_hours_separator));
                            C30191Wl r2 = (C30191Wl) A0l.get(1);
                            Integer num3 = r2.A03;
                            AnonymousClass009.A05(num3);
                            Calendar A013 = A01(instance, i4, num3.intValue());
                            Integer num4 = r2.A02;
                            AnonymousClass009.A05(num4);
                            A0h.append(AnonymousClass3JK.A04(r17, A013, A01(instance, i4, num4.intValue())));
                        }
                        i3 = R.color.business_hours_open;
                        str = context.getString(R.string.business_hours_status_open_until_open);
                        i = R.string.business_hours_status_open_until;
                        objArr = new Object[]{str, A0h, obj};
                        string = context.getString(i, objArr);
                    }
                }
            }
            ForegroundColorSpan A0M = C12980iv.A0M(context, i3);
            String trim = string.trim();
            int indexOf = trim.indexOf(str);
            int length = str.length() + indexOf;
            SpannableString spannableString = new SpannableString(trim);
            spannableString.setSpan(A0M, indexOf, length, 33);
            spannableString.setSpan(new C52292aZ(context), indexOf, length, 33);
            return spannableString;
        }
        str = string;
        ForegroundColorSpan A0M = C12980iv.A0M(context, i3);
        String trim = string.trim();
        int indexOf = trim.indexOf(str);
        int length = str.length() + indexOf;
        SpannableString spannableString = new SpannableString(trim);
        spannableString.setSpan(A0M, indexOf, length, 33);
        spannableString.setSpan(new C52292aZ(context), indexOf, length, 33);
        return spannableString;
    }

    public static Calendar A01(Calendar calendar, int i, int i2) {
        Calendar calendar2 = (Calendar) calendar.clone();
        calendar2.set(7, i);
        calendar2.set(11, i2 / 60);
        calendar2.set(12, i2 % 60);
        calendar2.set(13, 0);
        calendar2.set(14, 0);
        return calendar2;
    }
}
