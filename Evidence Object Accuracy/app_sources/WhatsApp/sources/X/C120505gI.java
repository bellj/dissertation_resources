package X;

import android.content.Context;

/* renamed from: X.5gI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120505gI extends C126705tJ {
    public final Context A00;
    public final C14900mE A01;
    public final C15570nT A02;
    public final C18640sm A03;
    public final AnonymousClass102 A04;
    public final C14850m9 A05;
    public final C17220qS A06;
    public final C1329668y A07;
    public final C21860y6 A08;
    public final C18650sn A09;
    public final C17070qD A0A;
    public final AnonymousClass6BE A0B;
    public final C121265hX A0C;
    public final C18590sh A0D;

    public C120505gI(Context context, C14900mE r3, C15570nT r4, C18640sm r5, AnonymousClass102 r6, C14850m9 r7, C17220qS r8, C1308460e r9, C1329668y r10, C21860y6 r11, C18650sn r12, C18610sj r13, C17070qD r14, AnonymousClass6BE r15, C121265hX r16, C18590sh r17) {
        super(r9.A04, r13);
        this.A00 = context;
        this.A05 = r7;
        this.A01 = r3;
        this.A02 = r4;
        this.A06 = r8;
        this.A0D = r17;
        this.A0A = r14;
        this.A08 = r11;
        this.A04 = r6;
        this.A0B = r15;
        this.A03 = r5;
        this.A09 = r12;
        this.A07 = r10;
        this.A0C = r16;
    }

    public final void A00(AnonymousClass1ZR r13, C119755f3 r14, AbstractC136246Lu r15, String str) {
        C64513Fv r10 = super.A00;
        r10.A04("upi-generate-otp");
        C17220qS r2 = this.A06;
        String A01 = r2.A01();
        String A07 = this.A07.A07();
        C117295Zj.A1B(r2, new C120755gh(this.A00, this.A01, this.A09, r10, r15, this), new C126435ss(new AnonymousClass3CT(A01), C117315Zl.A0K(r13), str, this.A0D.A01(), (String) C117295Zj.A0R(r14.A06), A07).A00, A01);
    }

    public void A01(C119755f3 r31, AbstractC136246Lu r32) {
        StringBuilder A0k = C12960it.A0k("PAY: IndiaUpiOtpAction requestOtp withCallback: ");
        A0k.append(C12960it.A1W(r32));
        C12960it.A1F(A0k);
        AnonymousClass6BE r0 = this.A0B;
        r0.AeG();
        String str = r31.A0F;
        if (AnonymousClass1ZS.A02(r31.A09)) {
            Context context = this.A00;
            C14850m9 r11 = this.A05;
            C14900mE r10 = this.A01;
            C15570nT r9 = this.A02;
            C17220qS r8 = this.A06;
            C17070qD r7 = this.A0A;
            C21860y6 r6 = this.A08;
            C18610sj r5 = super.A01;
            new C120495gH(context, r10, r9, this.A04, r11, r8, this.A07, r6, this.A09, null, r5, r7, r0, this.A0C).A01(new AnonymousClass6AS(r31, r32, this));
            return;
        }
        A00(r31.A09, r31, r32, str);
    }
}
