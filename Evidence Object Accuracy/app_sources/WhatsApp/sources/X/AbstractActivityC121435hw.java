package X;

import com.whatsapp.payments.ui.BrazilPaymentActivity;

/* renamed from: X.5hw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC121435hw extends AbstractActivityC121685jC {
    public boolean A00 = false;

    public AbstractActivityC121435hw() {
        C117295Zj.A0p(this, 13);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            BrazilPaymentActivity brazilPaymentActivity = (BrazilPaymentActivity) this;
            AnonymousClass2FL r3 = (AnonymousClass2FL) ((AnonymousClass2FJ) generatedComponent());
            AnonymousClass01J A1M = ActivityC13830kP.A1M(r3, brazilPaymentActivity);
            ActivityC13810kN.A10(A1M, brazilPaymentActivity);
            AbstractActivityC119235dO.A1S(r3, A1M, brazilPaymentActivity, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(r3, A1M, brazilPaymentActivity, ActivityC13790kL.A0Y(A1M, brazilPaymentActivity)), brazilPaymentActivity));
            AbstractActivityC119235dO.A1W(A1M, brazilPaymentActivity);
            AbstractActivityC119235dO.A1X(A1M, brazilPaymentActivity);
            AbstractActivityC119235dO.A1R(r3, A1M, (C18620sk) A1M.AFB.get(), brazilPaymentActivity);
        }
    }
}
