package X;

import android.os.Build;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.whatsapp.voipcalling.DefaultCryptoCallback;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.util.HashMap;
import java.util.List;
import org.chromium.net.UrlRequest;

/* renamed from: X.5yG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129765yG {
    public double A00;
    public double A01;
    public double A02;
    public float A03;
    public float A04;
    public float A05;
    public int A06;
    public int A07;
    public int A08;
    public int A09;
    public int A0A;
    public int A0B;
    public int A0C;
    public int A0D;
    public int A0E;
    public int A0F;
    public int A0G;
    public int A0H;
    public int A0I;
    public int A0J;
    public int A0K;
    public int A0L;
    public int A0M;
    public int A0N;
    public long A0O;
    public long A0P;
    public C129845yO A0Q;
    public C129845yO A0R;
    public C129845yO A0S;
    public C129845yO A0T;
    public C129845yO A0U;
    public String A0V;
    public HashMap A0W = C12970iu.A11();
    public List A0X;
    public List A0Y;
    public boolean A0Z;
    public boolean A0a;
    public boolean A0b;
    public boolean A0c;
    public boolean A0d;
    public boolean A0e;
    public boolean A0f;
    public boolean A0g;
    public boolean A0h;
    public boolean A0i;
    public boolean A0j;
    public boolean A0k;
    public boolean A0l;
    public boolean A0m;
    public boolean A0n;
    public boolean A0o;
    public boolean A0p;
    public boolean A0q;
    public boolean A0r;
    public boolean A0s;
    public boolean A0t;
    public boolean A0u;
    public boolean A0v;
    public boolean A0w;
    public boolean A0x;
    public boolean A0y;
    public boolean A0z;
    public boolean A10;
    public boolean A11;
    public boolean A12;
    public boolean A13;
    public boolean A14;
    public boolean A15;
    public boolean A16;
    public boolean A17;
    public boolean A18;
    public boolean A19;
    public boolean A1A;
    public boolean A1B;
    public boolean A1C;
    public boolean A1D;
    public boolean A1E;
    public boolean A1F;
    public boolean A1G;
    public boolean A1H;
    public boolean A1I;
    public boolean A1J;
    public boolean A1K;
    public boolean A1L;
    public boolean A1M;
    public boolean A1N;
    public boolean A1O;
    public boolean A1P;
    public boolean A1Q;
    public boolean A1R;
    public boolean A1S;
    public boolean A1T;
    public boolean A1U;
    public boolean A1V;
    public boolean A1W;
    public boolean A1X;
    public boolean A1Y;
    public boolean A1Z;
    public boolean A1a;
    public float[] A1b;
    public int[] A1c;
    public int[] A1d;

    public C128385w1 A00() {
        if (!this.A1F || !this.A1W) {
            return new C128385w1(this);
        }
        throw C12960it.A0U("Unable to build setting modifications, video stabilization will not work correctly if optical stabilization is also enabled, please enable one at a time only.");
    }

    public void A01(C125475rJ r5, Object obj) {
        int i = r5.A00;
        switch (i) {
            case 1:
                this.A0k = C12970iu.A1Y(obj);
                this.A0l = true;
                return;
            case 2:
                this.A16 = C12970iu.A1Y(obj);
                this.A17 = true;
                return;
            case 3:
                this.A1W = C12970iu.A1Y(obj);
                this.A1X = true;
                return;
            case 4:
                this.A1F = C12970iu.A1Y(obj);
                this.A1G = true;
                return;
            case 5:
                this.A0z = C12970iu.A1Y(obj);
                this.A10 = true;
                return;
            case 6:
                this.A0c = C12970iu.A1Y(obj);
                this.A0d = true;
                return;
            case 7:
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
            case C43951xu.A01:
            case 28:
            case 29:
            case 40:
            case 41:
            case 43:
            default:
                throw C12990iw.A0m(C12960it.A0W(i, "Invalid Settings key: "));
            case 8:
                this.A1Q = C12970iu.A1Y(obj);
                this.A1R = true;
                return;
            case 9:
                this.A0C = C12960it.A05(obj);
                this.A0y = true;
                return;
            case 10:
                this.A0B = C12960it.A05(obj);
                this.A0w = true;
                return;
            case 11:
                this.A06 = C12960it.A05(obj);
                this.A0Z = true;
                return;
            case 12:
                this.A08 = C12960it.A05(obj);
                this.A0p = true;
                return;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                this.A0A = C12960it.A05(obj);
                this.A0s = true;
                return;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                this.A0E = C12960it.A05(obj);
                this.A1A = true;
                return;
            case 15:
                this.A0F = C12960it.A05(obj);
                this.A1B = true;
                return;
            case 17:
                this.A0H = C12960it.A05(obj);
                this.A1I = true;
                return;
            case 18:
                this.A0I = C12960it.A05(obj);
                this.A1K = true;
                return;
            case 19:
                this.A0J = C12960it.A05(obj);
                this.A1M = true;
                return;
            case 21:
                this.A0G = C12960it.A05(obj);
                this.A1H = true;
                return;
            case 22:
                this.A0D = C12960it.A05(obj);
                this.A19 = true;
                return;
            case 23:
                this.A0K = C12960it.A05(obj);
                this.A1S = true;
                return;
            case 24:
                this.A0M = C12960it.A05(obj);
                this.A1Y = true;
                return;
            case 25:
                this.A0N = C12960it.A05(obj);
                this.A1a = true;
                return;
            case 26:
                this.A05 = C72453ed.A02(obj);
                this.A1T = true;
                return;
            case 27:
                this.A0P = C12980iv.A0G(obj);
                this.A15 = true;
                return;
            case C25991Bp.A0S:
                this.A00 = ((Number) obj).doubleValue();
                this.A11 = true;
                return;
            case 31:
                this.A02 = ((Number) obj).doubleValue();
                this.A13 = true;
                return;
            case 32:
                this.A01 = ((Number) obj).doubleValue();
                this.A12 = true;
                return;
            case 33:
                this.A0S = (C129845yO) obj;
                this.A1N = true;
                return;
            case 34:
                this.A0R = (C129845yO) obj;
                this.A1J = true;
                return;
            case 35:
                this.A0T = (C129845yO) obj;
                this.A1V = true;
                return;
            case 36:
                this.A0Q = (C129845yO) obj;
                this.A1C = true;
                return;
            case 37:
                this.A0X = C130365zI.A00((List) obj);
                this.A0x = true;
                return;
            case 38:
                this.A0Y = C130365zI.A00((List) obj);
                this.A1E = true;
                return;
            case 39:
                int[] iArr = (int[]) obj;
                if (iArr != null && iArr.length == 2) {
                    this.A1d = new int[]{iArr[0], iArr[1]};
                    this.A1L = true;
                    return;
                }
                return;
            case 42:
                this.A0V = (String) obj;
                this.A14 = true;
                return;
            case 44:
                this.A0t = C12970iu.A1Y(obj);
                this.A0u = true;
                return;
            case 45:
                this.A0e = C12970iu.A1Y(obj);
                this.A0f = true;
                return;
            case DefaultCryptoCallback.E2E_EXTENDED_V2_KEY_LENGTH /* 46 */:
                this.A0O = C12980iv.A0G(obj);
                this.A0v = true;
                return;
            case 47:
                this.A09 = C12960it.A05(obj);
                this.A0q = true;
                return;
            case 48:
                this.A03 = C72453ed.A02(obj);
                this.A0a = true;
                return;
            case 49:
                if (Build.VERSION.SDK_INT >= 21) {
                    float[] fArr = (float[]) obj;
                    if (fArr == null || fArr.length == 4) {
                        this.A1b = fArr;
                        this.A0m = true;
                        return;
                    }
                    throw C12970iu.A0f("Color Correction Gains invalid length");
                }
                return;
            case SearchActionVerificationClientService.TIME_TO_SLEEP_IN_MS /* 50 */:
                this.A07 = C12960it.A05(obj);
                this.A0n = true;
                return;
            case 51:
                if (Build.VERSION.SDK_INT >= 21) {
                    int[] iArr2 = (int[]) obj;
                    if (iArr2 == null || iArr2.length == 18) {
                        this.A1c = iArr2;
                        this.A0o = true;
                        return;
                    }
                    throw C12970iu.A0f("Color Correction Transform invalid length");
                }
                return;
            case 52:
                this.A0r = true;
                return;
            case 53:
                this.A0g = C12970iu.A1Y(obj);
                this.A0h = true;
                return;
            case 54:
                this.A04 = C72453ed.A02(obj);
                this.A1D = true;
                return;
            case 55:
                this.A0W = (HashMap) obj;
                this.A0b = true;
                return;
            case 56:
                this.A0i = C12970iu.A1Y(obj);
                this.A0j = true;
                return;
            case 57:
                this.A18 = C12970iu.A1Y(obj);
                return;
            case 58:
                this.A0U = (C129845yO) obj;
                this.A1Z = true;
                return;
            case 59:
                this.A0L = C12960it.A05(obj);
                this.A1U = true;
                return;
            case 60:
                this.A1O = C12970iu.A1Y(obj);
                this.A1P = true;
                return;
        }
    }
}
