package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import com.whatsapp.util.Log;

/* renamed from: X.14g  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C241114g {
    public final C14830m7 A00;
    public final C16370ot A01;
    public final C20120vF A02;
    public final AnonymousClass1YX A03;
    public final C16490p7 A04;

    public C241114g(C14830m7 r2, C16370ot r3, C20120vF r4, C21620xi r5, C16490p7 r6) {
        this.A00 = r2;
        this.A01 = r3;
        this.A04 = r6;
        this.A02 = r4;
        this.A03 = r5.A01;
    }

    public C38091nV A00(AnonymousClass1IS r11) {
        AbstractC15340mz A03;
        int[] iArr;
        int length;
        if (r11 == null || (A03 = this.A01.A03(r11)) == null) {
            return null;
        }
        long j = A03.A11;
        C16310on A01 = this.A04.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT sidecar, chunk_lengths FROM message_streaming_sidecar WHERE message_row_id = ?", new String[]{Long.toString(j)});
            if (A09.moveToLast()) {
                byte[] blob = A09.getBlob(A09.getColumnIndexOrThrow("sidecar"));
                byte[] blob2 = A09.getBlob(A09.getColumnIndexOrThrow("chunk_lengths"));
                if (blob2 == null || (length = blob2.length) <= 0 || length % 4 != 0) {
                    iArr = null;
                } else {
                    int i = length >> 2;
                    iArr = new int[i];
                    for (int i2 = 0; i2 < i; i2++) {
                        int i3 = i2 << 2;
                        iArr[i2] = (blob2[i3 + 3] & 255) | ((blob2[i3] & 255) << 24) | ((blob2[i3 + 1] & 255) << 16) | ((blob2[i3 + 2] & 255) << 8);
                    }
                }
                C38091nV r0 = new C38091nV(blob, iArr);
                A09.close();
                A01.close();
                return r0;
            }
            A09.close();
            A01.close();
            return null;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A01(C38101nW r12, long j) {
        boolean z;
        byte[] bArr;
        if (r12 != null) {
            synchronized (r12) {
                z = r12.A01;
            }
            if (z) {
                byte[] A05 = r12.A05();
                int[] A06 = r12.A06();
                try {
                    C16310on A02 = this.A04.A02();
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("message_row_id", Long.valueOf(j));
                    contentValues.put("sidecar", A05);
                    if (A06 == null) {
                        bArr = null;
                    } else {
                        int length = A06.length;
                        bArr = new byte[length << 2];
                        for (int i = 0; i < length; i++) {
                            int i2 = i << 2;
                            int i3 = A06[i];
                            bArr[i2 + 3] = (byte) i3;
                            bArr[i2 + 2] = (byte) (i3 >> 8);
                            bArr[i2 + 1] = (byte) (i3 >> 16);
                            bArr[i2] = (byte) (i3 >> 24);
                        }
                    }
                    contentValues.put("chunk_lengths", bArr);
                    contentValues.put("timestamp", Long.valueOf(this.A00.A00()));
                    A02.A03.A06(contentValues, "message_streaming_sidecar", 5);
                    A02.close();
                    synchronized (r12) {
                        r12.A01 = false;
                    }
                } catch (SQLiteConstraintException e) {
                    Log.e("SidecarMessageStore/insertStreamingSidecar/", e);
                    throw e;
                }
            }
        }
    }
}
