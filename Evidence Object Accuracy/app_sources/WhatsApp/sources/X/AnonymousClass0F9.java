package X;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;

/* renamed from: X.0F9  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0F9 extends AnonymousClass0B6 {
    public int A00 = -1;
    public int A01 = 0;

    public AnonymousClass0F9(int i, int i2) {
        super(i, i2);
    }

    public AnonymousClass0F9(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public AnonymousClass0F9(ViewGroup.LayoutParams layoutParams) {
        super(layoutParams);
    }

    public AnonymousClass0F9(ViewGroup.MarginLayoutParams marginLayoutParams) {
        super(marginLayoutParams);
    }
}
