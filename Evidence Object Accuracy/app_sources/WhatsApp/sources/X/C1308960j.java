package X;

import android.util.Base64;
import com.whatsapp.util.Log;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.60j  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1308960j {
    public static AnonymousClass1V8 A00(C130125yq r4, AnonymousClass1V8 r5) {
        String A0W = C117295Zj.A0W(r5, "encrypted_ref_json");
        if (AnonymousClass1US.A0C(A0W)) {
            return r5;
        }
        byte[] A04 = r4.A04(Base64.decode(A0W, 0));
        if (A04 != null) {
            HashMap A11 = C12970iu.A11();
            try {
                JSONObject A05 = C13000ix.A05(new String(A04, AnonymousClass01V.A08));
                Iterator<String> keys = A05.keys();
                while (keys.hasNext()) {
                    String A0x = C12970iu.A0x(keys);
                    A11.put(A0x, A05.getString(A0x));
                }
                return A01(r5, A11);
            } catch (UnsupportedEncodingException | JSONException unused) {
                Log.e("PAY: EncryptionHandler/decryptAccountNode failed");
                return r5;
            }
        } else {
            throw new C124735q1("PAY: EncryptionHandler/decryptAccountNode Can't decrypt reference.");
        }
    }

    public static AnonymousClass1V8 A01(AnonymousClass1V8 r10, Map map) {
        AnonymousClass1W9 r0;
        String str = r10.A00;
        ArrayList A0l = C12960it.A0l();
        AnonymousClass1W9[] A0L = r10.A0L();
        if (A0L != null) {
            for (AnonymousClass1W9 r02 : A0L) {
                String str2 = r02.A02;
                String str3 = r02.A03;
                String A0t = C12970iu.A0t(str3, map);
                if (A0t != null) {
                    r0 = new AnonymousClass1W9(str2, A0t);
                } else {
                    r0 = new AnonymousClass1W9(str2, str3);
                }
                A0l.add(r0);
            }
        }
        AnonymousClass1W9[] A1b = C117305Zk.A1b(A0l);
        ArrayList A0l2 = C12960it.A0l();
        AnonymousClass1V8[] r3 = r10.A03;
        if (r3 != null) {
            for (AnonymousClass1V8 r03 : r3) {
                A0l2.add(A01(r03, map));
            }
        }
        byte[] bArr = r10.A01;
        if (A0l2.size() == 0 && bArr == null) {
            return new AnonymousClass1V8(str, A1b);
        }
        if (A0l2.size() > 0) {
            return new AnonymousClass1V8(str, A1b, (AnonymousClass1V8[]) A0l2.toArray(new AnonymousClass1V8[0]));
        }
        return new AnonymousClass1V8(str, bArr, A1b);
    }

    public static void A02(C1310460z r5, C130125yq r6, C127685ut r7) {
        String A0n;
        List list = r7.A02;
        if (list.size() != 0) {
            JSONObject A0a = C117295Zj.A0a();
            for (int i = 0; i < list.size(); i++) {
                try {
                    A0a.put(list.get(i).toString(), ((AnonymousClass601) list.get(i)).A02);
                } catch (JSONException unused) {
                    Log.e("Can't create the JSON object");
                }
            }
            byte[] A05 = r6.A05(A0a.toString().getBytes(), null);
            if (A05 == null || (A0n = C117305Zk.A0n(A05)) == null) {
                throw new C124735q1("can't encrypt");
            }
            AnonymousClass61S.A03("encrypted_ref_json", A0n, r5.A01);
        }
    }
}
