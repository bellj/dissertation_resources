package X;

import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jobqueue.job.SendPeerMessageJob;
import com.whatsapp.util.Log;
import java.security.SecureRandom;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/* renamed from: X.11g  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C233311g {
    public final C15570nT A00;
    public final C15450nH A01;
    public final C20670w8 A02;
    public final C233411h A03;
    public final C14830m7 A04;
    public final C14820m6 A05;
    public final AnonymousClass16X A06;
    public final C22090yV A07;
    public final AnonymousClass16W A08;
    public final AnonymousClass16Y A09;
    public final AnonymousClass15O A0A;

    public C233311g(C15570nT r1, C15450nH r2, C20670w8 r3, C233411h r4, C14830m7 r5, C14820m6 r6, AnonymousClass16X r7, C22090yV r8, AnonymousClass16W r9, AnonymousClass16Y r10, AnonymousClass15O r11) {
        this.A04 = r5;
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
        this.A08 = r9;
        this.A03 = r4;
        this.A06 = r7;
        this.A07 = r8;
        this.A0A = r11;
        this.A05 = r6;
        this.A09 = r10;
    }

    public AnonymousClass1JZ A00() {
        if (!A0A()) {
            int A00 = this.A08.A00();
            if (A00 == 0) {
                A00 = new SecureRandom().nextInt(65536);
            }
            AnonymousClass1JR r6 = new AnonymousClass1JR(0, A00 + 1);
            byte[] A0D = C003501n.A0D(32);
            long A002 = this.A04.A00();
            SharedPreferences sharedPreferences = this.A05.A00;
            AnonymousClass1JZ r5 = new AnonymousClass1JZ(new C27821Ji(new C34321fx(A06(), sharedPreferences.getInt("adv_raw_id", -1), sharedPreferences.getInt("adv_current_key_index", -1)), A0D, A002), r6);
            StringBuilder sb = new StringBuilder("SyncdKeyManager/generateAndShareNewKey syncdKey = ");
            sb.append(r5);
            Log.i(sb.toString());
            Collection<AnonymousClass1JU> A003 = this.A06.A00();
            if (A003.isEmpty()) {
                A07();
                A09(Collections.singleton(r5));
                return r5;
            }
            for (AnonymousClass1JU r3 : A003) {
                A08(r3.A05, new HashMap(Collections.singletonMap(r5.A01, r5)), true);
            }
        }
        return null;
    }

    public AnonymousClass1JZ A01() {
        AnonymousClass1JZ A01 = this.A08.A01();
        if (A01 != null) {
            long millis = TimeUnit.DAYS.toMillis((long) this.A01.A02(AbstractC15460nI.A1g));
            long A00 = this.A04.A00();
            C27821Ji r2 = A01.A00;
            if (A00 - r2.A00 <= millis && A0C(r2)) {
                return A01;
            }
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0163, code lost:
        if (r3.get(null) != null) goto L_0x0165;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.HashMap A02(java.lang.String r15, java.util.Collection r16) {
        /*
        // Method dump skipped, instructions count: 371
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C233311g.A02(java.lang.String, java.util.Collection):java.util.HashMap");
    }

    public final HashMap A03(Collection collection) {
        AnonymousClass1JZ r0;
        HashMap hashMap = new HashMap();
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            AnonymousClass1JR r5 = (AnonymousClass1JR) it.next();
            if (r5 != null) {
                AnonymousClass16W r7 = this.A08;
                C16310on A01 = r7.A00.get();
                try {
                    Cursor A09 = A01.A03.A09("SELECT device_id, epoch, key_data, timestamp, fingerprint FROM crypto_info WHERE device_id = ?  AND epoch = ? ", new String[]{String.valueOf(r5.A00()), String.valueOf(r5.A01())});
                    if (A09 != null) {
                        if (A09.moveToFirst()) {
                            r0 = r7.A02(A09);
                            A09.close();
                            A01.close();
                        } else {
                            A09.close();
                        }
                    }
                    A01.close();
                    r0 = null;
                } catch (Throwable th) {
                    try {
                        A01.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            } else {
                r0 = A01();
            }
            hashMap.put(r5, r0);
        }
        return hashMap;
    }

    public Set A04() {
        AnonymousClass16Y r0 = this.A09;
        HashSet hashSet = new HashSet();
        C16310on A01 = r0.A00.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT DISTINCT collection_name FROM missing_keys", null);
            while (A09.moveToNext()) {
                hashSet.add(A09.getString(A09.getColumnIndexOrThrow("collection_name")));
            }
            A09.close();
            A01.close();
            return hashSet;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public Set A05() {
        List<C34831gm> A04 = this.A07.A04((byte) 39);
        HashSet hashSet = new HashSet();
        for (C34831gm r0 : A04) {
            hashSet.addAll(Collections.unmodifiableSet(r0.A00));
        }
        return hashSet;
    }

    public final Set A06() {
        HashSet hashSet = new HashSet();
        for (AnonymousClass1JU r0 : this.A06.A00()) {
            hashSet.add(Integer.valueOf(r0.A03));
        }
        hashSet.add(0);
        return hashSet;
    }

    public final void A07() {
        int i;
        AnonymousClass1JZ A01 = this.A08.A01();
        if (A01 == null) {
            i = 3;
        } else {
            i = 1;
            if (!A0C(A01.A00)) {
                i = 2;
            }
        }
        C233411h r2 = this.A03;
        C34881gr r1 = new C34881gr();
        r1.A00 = Integer.valueOf(i);
        r2.A06.A07(r1);
    }

    public void A08(DeviceJid deviceJid, HashMap hashMap, boolean z) {
        C15570nT r0 = this.A00;
        r0.A08();
        C27631Ih r4 = r0.A05;
        if (r4 != null && !hashMap.isEmpty()) {
            AnonymousClass15O r2 = this.A0A;
            C34821gl r5 = new C34821gl(r2.A02(r4, true), this.A04.A00());
            ((AbstractC30271Wt) r5).A00 = deviceJid;
            r5.A15(hashMap);
            r5.A01 = z;
            if (this.A07.A01(r5) < 0) {
                Log.e("SyncdKeyManager/shareKeys unable to add peer message");
            } else {
                this.A02.A00(new SendPeerMessageJob(deviceJid, r5, null, 0));
            }
        }
    }

    public void A09(Set set) {
        C16310on A02 = this.A08.A00.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            AnonymousClass1YE A0A = A02.A03.A0A("INSERT OR IGNORE INTO crypto_info (device_id, epoch, key_data, timestamp, fingerprint) VALUES (?, ?, ?, ?, ?)");
            Iterator it = set.iterator();
            while (it.hasNext()) {
                AnonymousClass1JZ r4 = (AnonymousClass1JZ) it.next();
                SQLiteStatement sQLiteStatement = A0A.A00;
                sQLiteStatement.clearBindings();
                AnonymousClass1JR r6 = r4.A01;
                A0A.A01(1, (long) r6.A00());
                A0A.A01(2, (long) r6.A01());
                C27821Ji r42 = r4.A00;
                sQLiteStatement.bindBlob(3, r42.A02);
                A0A.A01(4, r42.A00);
                sQLiteStatement.bindBlob(5, r42.A01.A01().A02());
                if (sQLiteStatement.executeInsert() == -1) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("SyncdCryptoStore/saveKey failed to store key: ");
                    sb.append(r6);
                    Log.e(sb.toString());
                }
            }
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public boolean A0A() {
        for (C34821gl r1 : this.A07.A04((byte) 38)) {
            if (!((AbstractC30271Wt) r1).A01 && r1.A01) {
                return true;
            }
        }
        return false;
    }

    public final boolean A0B() {
        Set A04 = A04();
        if (!(!A04.isEmpty()) || !A05().isEmpty()) {
            return false;
        }
        StringBuilder sb = new StringBuilder("SyncdKeyManager/isKeyMissingOnAllClients: key(s) missing on all the clients for collection(s): ");
        sb.append(A04);
        Log.e(sb.toString());
        return true;
    }

    public boolean A0C(C27821Ji r8) {
        String str;
        C34321fx r6 = r8.A01;
        int i = r6.A01;
        SharedPreferences sharedPreferences = this.A05.A00;
        if (i != sharedPreferences.getInt("adv_raw_id", -1)) {
            str = "SyncdKeyManager/verifyFingerprintOfKey: fingerprint mismatch: rawId did not match";
        } else {
            int i2 = sharedPreferences.getInt("adv_current_key_index", -1);
            HashSet hashSet = new HashSet(r6.A02);
            for (int i3 = r6.A00 + 1; i3 <= i2; i3++) {
                hashSet.add(Integer.valueOf(i3));
            }
            if (A06().equals(hashSet)) {
                return true;
            }
            str = "SyncdKeyManager/verifyFingerprintOfKey: fingerprint mismatch: one of a peer device is no longer registered";
        }
        Log.i(str);
        return false;
    }
}
