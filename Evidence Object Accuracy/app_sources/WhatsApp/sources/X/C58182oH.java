package X;

import com.whatsapp.service.UnsentMessagesNetworkAvailableJob;
import com.whatsapp.voipcalling.SelfManagedConnectionService;

/* renamed from: X.2oH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C58182oH extends AnonymousClass5B7 {
    public final C58182oH A00 = this;
    public final AnonymousClass01J A01;

    public /* synthetic */ C58182oH(AnonymousClass01J r1) {
        this.A01 = r1;
    }

    @Override // X.AnonymousClass5B7
    public void A00(UnsentMessagesNetworkAvailableJob unsentMessagesNetworkAvailableJob) {
        AnonymousClass01J r1 = this.A01;
        unsentMessagesNetworkAvailableJob.A05 = C12960it.A0T(r1);
        unsentMessagesNetworkAvailableJob.A04 = (C19890uq) r1.ABw.get();
        unsentMessagesNetworkAvailableJob.A01 = (AnonymousClass12H) r1.AC5.get();
        unsentMessagesNetworkAvailableJob.A03 = (AnonymousClass132) r1.ALx.get();
        unsentMessagesNetworkAvailableJob.A02 = (C16490p7) r1.ACJ.get();
    }

    @Override // X.AnonymousClass5B7
    public void A01(SelfManagedConnectionService selfManagedConnectionService) {
        selfManagedConnectionService.A00 = (C237612x) this.A01.AI3.get();
    }
}
