package X;

import android.app.Activity;
import android.content.Context;
import com.whatsapp.R;

/* renamed from: X.18R  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass18R {
    public final C14900mE A00;
    public final C18640sm A01;

    public AnonymousClass18R(C14900mE r1, C18640sm r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public void A00(Activity activity, AbstractC43701xS r5, boolean z) {
        int i;
        if (!this.A01.A0B()) {
            if (C18640sm.A03((Context) activity)) {
                i = R.string.no_network_cannot_unblock_airplane;
                if (z) {
                    i = R.string.no_network_cannot_block_airplane;
                }
            } else {
                i = R.string.no_network_cannot_unblock;
                if (z) {
                    i = R.string.no_network_cannot_block;
                }
            }
            this.A00.A07(i, 0);
            return;
        }
        this.A00.A0C((AbstractC13860kS) activity);
        r5.A6f();
    }
}
