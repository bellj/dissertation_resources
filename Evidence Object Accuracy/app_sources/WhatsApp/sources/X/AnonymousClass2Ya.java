package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

/* renamed from: X.2Ya  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Ya extends AnimatorListenerAdapter {
    public final /* synthetic */ float A00;
    public final /* synthetic */ C64523Fw A01;

    public AnonymousClass2Ya(C64523Fw r1, float f) {
        this.A01 = r1;
        this.A00 = f;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
        this.A01.A0C.setAlpha(this.A00);
        onAnimationEnd(animator);
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        if (this.A00 == 0.0f) {
            C64523Fw r3 = this.A01;
            r3.A0C.setVisibility(8);
            AnonymousClass2CT r0 = r3.A0G.A0W;
            if (r0 != null) {
                r0.A00(false);
            }
        }
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        if (this.A00 == 1.0f) {
            C64523Fw r3 = this.A01;
            r3.A0C.setVisibility(0);
            AnonymousClass2CT r0 = r3.A0G.A0W;
            if (r0 != null) {
                r0.A00(true);
            }
        }
    }
}
