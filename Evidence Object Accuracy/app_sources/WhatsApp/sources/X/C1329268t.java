package X;

import com.whatsapp.R;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.68t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1329268t implements AnonymousClass1FK {
    public final int A00;
    public final AnonymousClass18M A01;
    public final AbstractC16870pt A02;
    public final WeakReference A03;

    public C1329268t(AnonymousClass18M r2, AbstractC16870pt r3, AbstractView$OnClickListenerC121765jx r4, int i) {
        this.A02 = r3;
        this.A00 = i;
        this.A01 = r2;
        this.A03 = C12970iu.A10(r4);
    }

    public final void A00(C452120p r4, AbstractView$OnClickListenerC121765jx r5) {
        int i;
        AbstractC16870pt r1 = this.A02;
        if (r1 != null) {
            r1.AKa(r4, this.A00);
        }
        r5.AaN();
        if (r4 != null) {
            AnonymousClass18M r2 = this.A01;
            if (r2 == null || (i = r2.ACm(null, r4.A00)) == 0) {
                i = R.string.payment_method_cannot_be_set_default;
            }
            r5.Ado(i);
        }
    }

    @Override // X.AnonymousClass1FK
    public void AV3(C452120p r4) {
        AbstractView$OnClickListenerC121765jx r2 = (AbstractView$OnClickListenerC121765jx) this.A03.get();
        if (r2 != null) {
            r2.A0K.A06(C12960it.A0b("setDefault/onRequestError. paymentNetworkError: ", r4));
            A00(r4, r2);
        }
    }

    @Override // X.AnonymousClass1FK
    public void AVA(C452120p r4) {
        AbstractView$OnClickListenerC121765jx r2 = (AbstractView$OnClickListenerC121765jx) this.A03.get();
        if (r2 != null) {
            r2.A0K.A04(C12960it.A0b("setDefault/onResponseError. paymentNetworkError: ", r4));
            A00(r4, r2);
        }
    }

    @Override // X.AnonymousClass1FK
    public void AVB(C452220q r7) {
        int i;
        AbstractView$OnClickListenerC121765jx r4 = (AbstractView$OnClickListenerC121765jx) this.A03.get();
        if (r4 != null) {
            r4.A0K.A06("setDefault Success");
            AbstractC16870pt r2 = this.A02;
            if (r2 != null) {
                r2.AKa(null, this.A00);
            }
            C130035yh r5 = r4.A0F;
            List list = ((AnonymousClass46O) r7).A00;
            if (r5 instanceof C121455i8) {
                Iterator it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    AbstractC28901Pl A0H = C117305Zk.A0H(it);
                    if (A0H.A0A.equals(r5.A04.A09.A0A)) {
                        r5.A01(A0H);
                        break;
                    }
                }
            } else {
                r5.A01.setImageResource(R.drawable.ic_settings_starred);
                r5.A02.setText(R.string.default_payment_method_set);
                r5.A00.setOnClickListener(null);
            }
            r4.AaN();
            if (!(this instanceof C121805k3)) {
                i = R.string.payment_method_set_as_default;
            } else {
                boolean equals = "p2m".equals(((C121805k3) this).A01);
                i = R.string.payment_method_set_as_default_p2p;
                if (equals) {
                    i = R.string.payment_method_set_as_default_p2m;
                }
            }
            r4.Adp(r4.getString(i));
        }
    }
}
