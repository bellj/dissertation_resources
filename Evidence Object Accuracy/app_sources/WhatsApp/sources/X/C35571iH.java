package X;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.whatsapp.CircularProgressBar;
import com.whatsapp.ui.media.MediaCaptionTextView;

/* renamed from: X.1iH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C35571iH {
    public View A00;
    public View A01;
    public View A02;
    public View A03;
    public View A04;
    public View A05;
    public View A06;
    public View A07;
    public ViewGroup A08;
    public ViewGroup A09;
    public ViewGroup A0A;
    public TextView A0B;
    public TextView A0C;
    public TextView A0D;
    public CircularProgressBar A0E;
    public MediaCaptionTextView A0F;
    public final /* synthetic */ AbstractC33621eg A0G;

    public C35571iH(AbstractC33621eg r1) {
        this.A0G = r1;
    }
}
