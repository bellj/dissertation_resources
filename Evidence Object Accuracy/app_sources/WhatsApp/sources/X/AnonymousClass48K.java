package X;

import com.whatsapp.R;

/* renamed from: X.48K  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass48K extends AbstractC93424a9 {
    public AnonymousClass48K() {
        super(new C92794Xl(R.dimen.wds_badge_icon_dimen_10, R.dimen.wds_badge_icon_dimen_12, R.dimen.wds_badge_icon_dimen_16, R.dimen.wds_badge_icon_dimen_20), new AnonymousClass48J());
    }
}
