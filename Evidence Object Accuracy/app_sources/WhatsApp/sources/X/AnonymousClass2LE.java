package X;

import com.whatsapp.CameraHomeFragment;
import com.whatsapp.HomeActivity;
import com.whatsapp.RequestPermissionActivity;

/* renamed from: X.2LE  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2LE extends AnonymousClass077 {
    public boolean A00;
    public final /* synthetic */ HomeActivity A01;

    public /* synthetic */ AnonymousClass2LE(HomeActivity homeActivity) {
        this.A01 = homeActivity;
    }

    public final void A00() {
        boolean z;
        HomeActivity homeActivity = this.A01;
        AnonymousClass1s8 r1 = homeActivity.A0b;
        if (r1.A08 != null) {
            r1.A07();
        } else {
            CameraHomeFragment cameraHomeFragment = homeActivity.A0L;
            if (cameraHomeFragment != null) {
                cameraHomeFragment.A0n(true);
            }
            boolean A07 = ((ActivityC13810kN) homeActivity).A0C.A07(815);
            AnonymousClass1s8 r4 = homeActivity.A0b;
            if (((ActivityC13810kN) homeActivity).A06.A05(AbstractC15460nI.A12) || (homeActivity.A1C.A07() && ((ActivityC13810kN) homeActivity).A0C.A07(611))) {
                z = true;
            } else {
                z = false;
            }
            r4.A0I(null, homeActivity, null, null, null, null, null, null, 0, false, z, false, A07);
        }
        boolean z2 = true;
        if (RequestPermissionActivity.A0T(homeActivity, homeActivity.A0r, 30) && homeActivity.A0o.A04(homeActivity.A1w)) {
            if (((ActivityC13790kL) homeActivity).A06.A01() < ((long) ((((ActivityC13810kN) homeActivity).A06.A02(AbstractC15460nI.A1p) << 10) << 10))) {
                C33471e8.A04(homeActivity, homeActivity, homeActivity.A15, 5);
            } else {
                z2 = false;
            }
        }
        AnonymousClass1s8 r0 = homeActivity.A0b;
        if (z2) {
            r0.A03();
        } else {
            r0.A08();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0102, code lost:
        if (r5 != false) goto L_0x0023;
     */
    @Override // X.AnonymousClass077, X.AnonymousClass070
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void ATP(int r11, float r12, int r13) {
        /*
        // Method dump skipped, instructions count: 272
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2LE.ATP(int, float, int):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00ad  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00bd  */
    /* JADX WARNING: Removed duplicated region for block: B:67:? A[RETURN, SYNTHETIC] */
    @Override // X.AnonymousClass077, X.AnonymousClass070
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void ATQ(int r12) {
        /*
        // Method dump skipped, instructions count: 287
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2LE.ATQ(int):void");
    }
}
