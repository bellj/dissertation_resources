package X;

import android.view.ViewTreeObserver;
import com.whatsapp.account.delete.DeleteAccountFeedback;

/* renamed from: X.4oA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnPreDrawListenerC101834oA implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ DeleteAccountFeedback A00;

    public ViewTreeObserver$OnPreDrawListenerC101834oA(DeleteAccountFeedback deleteAccountFeedback) {
        this.A00 = deleteAccountFeedback;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        DeleteAccountFeedback deleteAccountFeedback = this.A00;
        C12980iv.A1G(deleteAccountFeedback.A04, this);
        deleteAccountFeedback.A2e();
        return false;
    }
}
