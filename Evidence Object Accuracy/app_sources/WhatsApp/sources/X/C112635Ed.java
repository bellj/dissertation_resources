package X;

import java.io.Serializable;

/* renamed from: X.5Ed  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C112635Ed implements AbstractC16710pd, Serializable {
    public Object _value = AnonymousClass1WS.A00;
    public AnonymousClass1WK initializer;

    public C112635Ed(AnonymousClass1WK r2) {
        this.initializer = r2;
    }

    @Override // X.AbstractC16710pd
    public boolean AJW() {
        return C12960it.A1X(this._value, AnonymousClass1WS.A00);
    }

    @Override // X.AbstractC16710pd
    public Object getValue() {
        Object obj = this._value;
        if (obj != AnonymousClass1WS.A00) {
            return obj;
        }
        AnonymousClass1WK r0 = this.initializer;
        C16700pc.A0C(r0);
        Object AJ3 = r0.AJ3();
        this._value = AJ3;
        this.initializer = null;
        return AJ3;
    }

    @Override // java.lang.Object
    public String toString() {
        return AJW() ? String.valueOf(getValue()) : "Lazy value not initialized yet.";
    }

    private final Object writeReplace() {
        return new C112625Ec(getValue());
    }
}
