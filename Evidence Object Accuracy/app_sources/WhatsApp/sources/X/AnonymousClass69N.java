package X;

import android.content.Context;
import com.whatsapp.payments.ui.IndiaUpiMandatePaymentActivity;

/* renamed from: X.69N  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass69N implements AnonymousClass5W7 {
    public final /* synthetic */ Context A00;
    public final /* synthetic */ AnonymousClass1IR A01;
    public final /* synthetic */ C120065fZ A02;
    public final /* synthetic */ String A03;

    public AnonymousClass69N(Context context, AnonymousClass1IR r2, C120065fZ r3, String str) {
        this.A02 = r3;
        this.A00 = context;
        this.A01 = r2;
        this.A03 = str;
    }

    @Override // X.AnonymousClass5W7
    public void AQ8() {
        this.A02.A09.A00(this.A00, this.A01);
    }

    @Override // X.AnonymousClass5W7
    public void AWu() {
        Context context = this.A00;
        context.startActivity(IndiaUpiMandatePaymentActivity.A02(context, this.A01, this.A03, 1));
    }
}
