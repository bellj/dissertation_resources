package X;

import java.nio.charset.Charset;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;

/* renamed from: X.3qD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79203qD extends AbstractC113535Hy<String> implements AnonymousClass5Z3, RandomAccess {
    public static final C79203qD A01;
    public static final AnonymousClass5Z3 A02;
    public final List A00;

    public C79203qD(ArrayList arrayList) {
        this.A00 = arrayList;
    }

    @Override // X.AnonymousClass5Z3
    public final Object AG5(int i) {
        return this.A00.get(i);
    }

    @Override // X.AnonymousClass5Z3
    public final List AhD() {
        return Collections.unmodifiableList(this.A00);
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ void add(int i, Object obj) {
        A02();
        this.A00.add(i, obj);
        ((AbstractList) this).modCount++;
    }

    @Override // X.AbstractC113535Hy, java.util.AbstractList, java.util.List
    public final boolean addAll(int i, Collection collection) {
        A02();
        if (collection instanceof AnonymousClass5Z3) {
            collection = ((AnonymousClass5Z3) collection).AhD();
        }
        boolean addAll = this.A00.addAll(i, collection);
        ((AbstractList) this).modCount++;
        return addAll;
    }

    @Override // X.AbstractC113535Hy, java.util.AbstractCollection, java.util.List, java.util.Collection
    public final boolean addAll(Collection collection) {
        return addAll(size(), collection);
    }

    @Override // X.AbstractC113535Hy, java.util.AbstractList, java.util.AbstractCollection, java.util.List, java.util.Collection
    public final void clear() {
        A02();
        this.A00.clear();
        ((AbstractList) this).modCount++;
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object remove(int i) {
        A02();
        Object remove = this.A00.remove(i);
        ((AbstractList) this).modCount++;
        return A00(remove);
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object set(int i, Object obj) {
        A02();
        return A00(this.A00.set(i, obj));
    }

    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection
    public final int size() {
        return this.A00.size();
    }

    static {
        C79203qD r1 = new C79203qD(C12980iv.A0w(10));
        A01 = r1;
        ((AbstractC113535Hy) r1).A00 = false;
        A02 = r1;
    }

    public static String A00(Object obj) {
        if (obj instanceof String) {
            return (String) obj;
        }
        if (!(obj instanceof AbstractC111915Bh)) {
            return new String((byte[]) obj, C93094Zb.A03);
        }
        AbstractC111915Bh r4 = (AbstractC111915Bh) obj;
        Charset charset = C93094Zb.A03;
        if (r4.A02() == 0) {
            return "";
        }
        C79243qH r42 = (C79243qH) r4;
        return new String(r42.zzfp, r42.A04(), r42.A02(), charset);
    }

    @Override // X.AnonymousClass5Z3
    public final AnonymousClass5Z3 AhE() {
        return super.A00 ? new C113545Hz(this) : this;
    }

    @Override // X.AnonymousClass5Z5
    public final /* synthetic */ AnonymousClass5Z5 AhN(int i) {
        if (i >= size()) {
            ArrayList A0w = C12980iv.A0w(i);
            A0w.addAll(this.A00);
            return new C79203qD(A0w);
        }
        throw C72453ed.A0h();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0032, code lost:
        if (X.C12960it.A1T(X.C95164dF.A00.A02(r7.zzfp, r6, r7.A02() + r6)) != false) goto L_0x0034;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0034, code lost:
        r3.set(r9, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0037, code lost:
        return r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x005c, code lost:
        if (X.C95164dF.A00.A02(r7, 0, r7.length) == 0) goto L_0x0034;
     */
    @Override // java.util.AbstractList, java.util.List
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object get(int r9) {
        /*
            r8 = this;
            java.util.List r3 = r8.A00
            java.lang.Object r7 = r3.get(r9)
            boolean r0 = r7 instanceof java.lang.String
            if (r0 == 0) goto L_0x000b
            return r7
        L_0x000b:
            boolean r0 = r7 instanceof X.AbstractC111915Bh
            if (r0 == 0) goto L_0x004b
            X.5Bh r7 = (X.AbstractC111915Bh) r7
            java.nio.charset.Charset r5 = X.C93094Zb.A03
            int r0 = r7.A02()
            if (r0 != 0) goto L_0x0038
            java.lang.String r4 = ""
        L_0x001b:
            X.3qH r7 = (X.C79243qH) r7
            int r6 = r7.A04()
            byte[] r5 = r7.zzfp
            int r2 = r7.A02()
            int r2 = r2 + r6
            X.4bY r1 = X.C95164dF.A00
            int r0 = r1.A02(r5, r6, r2)
            boolean r0 = X.C12960it.A1T(r0)
            if (r0 == 0) goto L_0x0037
        L_0x0034:
            r3.set(r9, r4)
        L_0x0037:
            return r4
        L_0x0038:
            r0 = r7
            X.3qH r0 = (X.C79243qH) r0
            byte[] r2 = r0.zzfp
            int r1 = r0.A04()
            int r0 = r0.A02()
            java.lang.String r4 = new java.lang.String
            r4.<init>(r2, r1, r0, r5)
            goto L_0x001b
        L_0x004b:
            byte[] r7 = (byte[]) r7
            java.nio.charset.Charset r0 = X.C93094Zb.A03
            java.lang.String r4 = new java.lang.String
            r4.<init>(r7, r0)
            X.4bY r2 = X.C95164dF.A00
            int r1 = r7.length
            r0 = 0
            int r0 = r2.A02(r7, r0, r1)
            if (r0 != 0) goto L_0x0037
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C79203qD.get(int):java.lang.Object");
    }
}
