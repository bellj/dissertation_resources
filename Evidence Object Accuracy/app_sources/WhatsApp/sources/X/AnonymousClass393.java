package X;

import android.content.Context;
import android.webkit.URLUtil;
import java.io.File;
import java.util.HashMap;

/* renamed from: X.393  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass393 extends AbstractC16350or {
    public static final HashMap A0A = C12970iu.A11();
    public final int A00;
    public final long A01;
    public final C18790t3 A02;
    public final AnonymousClass10V A03;
    public final C16590pI A04;
    public final C22050yP A05;
    public final C41831uE A06;
    public final C18800t4 A07;
    public final C19930uu A08;
    public final AbstractC14440lR A09;

    public AnonymousClass393(C18790t3 r1, AnonymousClass10V r2, C16590pI r3, C22050yP r4, C41831uE r5, C18800t4 r6, C19930uu r7, AbstractC14440lR r8, int i, long j) {
        this.A04 = r3;
        this.A08 = r7;
        this.A09 = r8;
        this.A02 = r1;
        this.A05 = r4;
        this.A07 = r6;
        this.A03 = r2;
        this.A06 = r5;
        this.A00 = i;
        this.A01 = j;
    }

    public static File A00(Context context, AnonymousClass393 r3) {
        File file = new File(context.getCacheDir(), "ProfilePictureTemp");
        file.mkdirs();
        return new File(file, URLUtil.guessFileName(r3.A06.A05.toString(), null, null));
    }

    public static void A01(C18790t3 r7, AnonymousClass10V r8, C16590pI r9, C22050yP r10, C41831uE r11, C18800t4 r12, C19930uu r13, AbstractC14440lR r14, int i, long j) {
        HashMap hashMap = A0A;
        synchronized (hashMap) {
            AbstractC14640lm r4 = r11.A03;
            if (hashMap.containsKey(r4)) {
                AnonymousClass393 r3 = (AnonymousClass393) hashMap.get(r4);
                C41831uE r2 = r3.A06;
                if (!r2.A05.equals(r11.A05)) {
                    r3.A03(true);
                    hashMap.remove(r2.A03);
                } else if (r3.A00() != 1) {
                    hashMap.remove(r2.A03);
                }
            }
            AnonymousClass393 r6 = new AnonymousClass393(r7, r8, r9, r10, r11, r12, r13, r14, i, j);
            hashMap.put(r4, r6);
            C12960it.A1E(r6, r14);
        }
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:103:0x001d */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:81:0x01bf */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r7v0 */
    /* JADX WARN: Type inference failed for: r7v1, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r7v2 */
    /* JADX WARN: Type inference failed for: r7v3 */
    /* JADX WARN: Type inference failed for: r7v4, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r7v11 */
    /* JADX WARN: Type inference failed for: r7v12 */
    /* JADX WARN: Type inference failed for: r7v13 */
    /* JADX WARN: Type inference failed for: r7v14 */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00da, code lost:
        if (r15 != 0) goto L_0x00dc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x01bf, code lost:
        if (r4 == null) goto L_0x01c4;
     */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x01a7  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x01ac A[Catch: IOException -> 0x01b0, TRY_ENTER, TRY_LEAVE, TryCatch #4 {IOException -> 0x01b0, blocks: (B:44:0x0136, B:72:0x01ac), top: B:103:0x001d }] */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // X.AbstractC16350or
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ java.lang.Object A05(java.lang.Object[] r20) {
        /*
        // Method dump skipped, instructions count: 497
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass393.A05(java.lang.Object[]):java.lang.Object");
    }
}
