package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import com.whatsapp.reactions.ReactionEmojiTextView;

/* renamed from: X.3ep  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C72573ep extends AnimatorListenerAdapter {
    public final /* synthetic */ C53232dO A00;
    public final /* synthetic */ ReactionEmojiTextView A01;

    public C72573ep(C53232dO r1, ReactionEmojiTextView reactionEmojiTextView) {
        this.A00 = r1;
        this.A01 = reactionEmojiTextView;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        this.A01.setSelected(false);
    }
}
