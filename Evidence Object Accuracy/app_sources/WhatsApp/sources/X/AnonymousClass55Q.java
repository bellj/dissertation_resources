package X;

import android.graphics.drawable.Drawable;
import com.whatsapp.R;

/* renamed from: X.55Q  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass55Q implements AbstractC116685Wk {
    public final /* synthetic */ C14900mE A00;

    @Override // X.AbstractC116685Wk
    public void A7G() {
    }

    @Override // X.AbstractC116685Wk
    public void Ag4() {
    }

    public AnonymousClass55Q(C14900mE r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC116685Wk
    public void AdA(Drawable drawable) {
        C14900mE r1 = this.A00;
        int i = R.string.wallpaper_set_successful;
        if (drawable == null) {
            i = R.string.wallpaper_reset;
        }
        r1.A07(i, 0);
    }
}
