package X;

import android.content.Context;
import java.lang.ref.WeakReference;

/* renamed from: X.37K  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass37K extends AbstractC16350or {
    public final AnonymousClass018 A00;
    public final WeakReference A01;
    public final WeakReference A02;

    public AnonymousClass37K(Context context, AnonymousClass018 r3, AnonymousClass1KX r4) {
        this.A00 = r3;
        this.A01 = C12970iu.A10(context);
        this.A02 = C12970iu.A10(r4);
    }
}
