package X;

import android.content.Context;

/* renamed from: X.2yG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC60582yG extends AbstractC42671vd {
    public boolean A00;

    public AbstractC60582yG(Context context, AbstractC13890kV r2, AbstractC16130oV r3) {
        super(context, r2, r3);
        A0Z();
    }

    @Override // X.AbstractC42681ve, X.AnonymousClass1OZ, X.AbstractC28561Ob
    public void A0Z() {
        if (!this.A00) {
            this.A00 = true;
            C60762yb r2 = (C60762yb) this;
            AnonymousClass2P6 r3 = (AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent());
            AnonymousClass01J A08 = AnonymousClass1OY.A08(r3, r2);
            AnonymousClass1OY.A0L(A08, r2);
            AnonymousClass1OY.A0M(A08, r2);
            AnonymousClass1OY.A0K(A08, r2);
            AnonymousClass1OY.A0I(r3, A08, r2, AnonymousClass1OY.A09(A08, r2, AnonymousClass1OY.A0B(A08, r2)));
            AnonymousClass1OY.A0O(A08, r2);
            r2.A00 = r3.A01();
        }
    }
}
