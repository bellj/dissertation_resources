package X;

import android.os.Bundle;

/* renamed from: X.3CC  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3CC {
    public final Bundle A00;

    public AnonymousClass3CC(Bundle bundle) {
        if (bundle != null) {
            C65273Iw.A03("response", bundle);
            this.A00 = bundle;
            return;
        }
        throw C12970iu.A0f("Bundle is null");
    }

    public byte[] A00() {
        Bundle bundle = this.A00;
        if (bundle.containsKey("response")) {
            return bundle.getByteArray("response");
        }
        throw new IllegalStateException();
    }
}
