package X;

import java.util.List;

/* renamed from: X.4SX  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4SX {
    public final C44741zT A00;
    public final String A01;
    public final String A02;
    public final List A03;
    public final boolean A04;

    public AnonymousClass4SX(C44741zT r1, String str, String str2, List list, boolean z) {
        this.A01 = str;
        this.A02 = str2;
        this.A04 = z;
        this.A00 = r1;
        this.A03 = list;
    }
}
