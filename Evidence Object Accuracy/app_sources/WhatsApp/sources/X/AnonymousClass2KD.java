package X;

/* renamed from: X.2KD  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2KD {
    public final int A00;
    public final C15580nU A01;
    public final Long A02;
    public final String A03;

    public AnonymousClass2KD(C15580nU r1, Long l, String str, int i) {
        this.A01 = r1;
        this.A03 = str;
        this.A02 = l;
        this.A00 = i;
    }
}
