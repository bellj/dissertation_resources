package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import com.facebook.redex.RunnableBRunnable0Shape4S0200000_I0_4;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.119  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass119 {
    public final C16510p9 A00;
    public final C15600nX A01;
    public final C21620xi A02;
    public final C16490p7 A03;

    public AnonymousClass119(C16510p9 r1, C15600nX r2, C21620xi r3, C16490p7 r4) {
        this.A00 = r1;
        this.A02 = r3;
        this.A03 = r4;
        this.A01 = r2;
    }

    public void A00(C16310on r7, GroupJid groupJid) {
        if (!this.A01.A0C(groupJid)) {
            String[] strArr = {String.valueOf(this.A00.A02(groupJid))};
            ContentValues contentValues = new ContentValues();
            contentValues.put("invalid_state", (Integer) 1);
            r7.A03.A00("message_poll", contentValues, "message_row_id IN (SELECT _id FROM available_message_view WHERE (available_message_view.chat_row_id = ? AND available_message_view.message_type = 66))", strArr);
            r7.A03(new RunnableBRunnable0Shape4S0200000_I0_4(this, 23, groupJid));
        }
    }

    public void A01(C27671Iq r14) {
        C16490p7 r4 = this.A03;
        C16310on A01 = r4.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT selectable_options_count, invalid_state FROM message_poll WHERE message_row_id = ?", new String[]{Long.toString(r14.A11)});
            if (A09.moveToLast()) {
                r14.A01 = A09.getInt(A09.getColumnIndexOrThrow("selectable_options_count"));
                r14.A00 = A09.getInt(A09.getColumnIndexOrThrow("invalid_state"));
            }
            A09.close();
            A01.close();
            A01 = r4.get();
            try {
                Cursor A092 = A01.A03.A09("SELECT _id, option_sha256, option_name, vote_total FROM message_poll_option WHERE message_row_id = ?", new String[]{Long.toString(r14.A11)});
                if (A092.moveToFirst()) {
                    ArrayList arrayList = new ArrayList();
                    int columnIndexOrThrow = A092.getColumnIndexOrThrow("_id");
                    int columnIndexOrThrow2 = A092.getColumnIndexOrThrow("option_name");
                    int columnIndexOrThrow3 = A092.getColumnIndexOrThrow("option_sha256");
                    int columnIndexOrThrow4 = A092.getColumnIndexOrThrow("vote_total");
                    do {
                        long j = A092.getLong(columnIndexOrThrow);
                        String string = A092.getString(columnIndexOrThrow2);
                        String string2 = A092.getString(columnIndexOrThrow3);
                        int i = A092.getInt(columnIndexOrThrow4);
                        C27701Iu r0 = new C27701Iu(string, string2);
                        r0.A01 = j;
                        r0.A00 = i;
                        arrayList.add(r0);
                    } while (A092.moveToNext());
                    List list = r14.A04;
                    list.clear();
                    list.addAll(arrayList);
                }
                A092.close();
            } finally {
                try {
                    A01.close();
                } catch (Throwable unused) {
                }
            }
        } finally {
        }
    }

    public void A02(C27671Iq r7) {
        C16310on A02 = this.A03.A02();
        try {
            ContentValues contentValues = new ContentValues(3);
            contentValues.put("message_row_id", Long.valueOf(r7.A11));
            contentValues.put("selectable_options_count", Integer.valueOf(r7.A01));
            contentValues.put("invalid_state", Integer.valueOf(r7.A00));
            if (A02.A03.A06(contentValues, "message_poll", 5) < 0) {
                StringBuilder sb = new StringBuilder();
                sb.append("PollMessageStore/insertOrUpdateMessagePoll/insert error, rowId=");
                sb.append(r7.A11);
                Log.e(sb.toString());
            }
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A03(C27671Iq r12) {
        C16310on A02 = this.A03.A02();
        try {
            for (C27701Iu r6 : r12.A04) {
                ContentValues contentValues = new ContentValues(5);
                contentValues.put("message_row_id", Long.valueOf(r12.A11));
                contentValues.put("option_sha256", r6.A02);
                contentValues.put("option_name", r6.A03);
                contentValues.put("vote_total", Integer.valueOf(r6.A00));
                long j = r6.A01;
                if (j != -1) {
                    contentValues.put("_id", Long.valueOf(j));
                }
                long A06 = A02.A03.A06(contentValues, "message_poll_option", 5);
                if (A06 != -1) {
                    r6.A01 = A06;
                } else {
                    throw new SQLException("PollMessageStore/insertOrUpdatePollOptionTable the row was not updated");
                }
            }
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A04(C27671Iq r14) {
        C16310on A02 = this.A03.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            for (C27701Iu r10 : r14.A04) {
                ContentValues contentValues = new ContentValues(1);
                contentValues.put("vote_total", Integer.valueOf(r10.A00));
                if (A02.A03.A00("message_poll_option", contentValues, "_id = ?", new String[]{String.valueOf(r10.A01)}) != 1) {
                    throw new SQLException("updatePollOptionVoteTotals/updatePollOptionVoteTotals the row was not updated");
                }
            }
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
