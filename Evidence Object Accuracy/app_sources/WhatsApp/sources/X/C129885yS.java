package X;

import android.util.SparseArray;

/* renamed from: X.5yS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129885yS {
    public final SparseArray A00 = new SparseArray();
    public final SparseArray A01 = new SparseArray();
    public final SparseArray A02 = new SparseArray();
    public final AnonymousClass60S A03;

    public C129885yS(AnonymousClass60S r2) {
        this.A03 = r2;
    }

    public C119105ct A00(int i) {
        return (C119105ct) this.A02.get(this.A03.A02(i));
    }

    public AbstractC130695zp A01(int i) {
        return (AbstractC130695zp) this.A00.get(this.A03.A02(i));
    }

    public AbstractC130685zo A02(int i) {
        return (AbstractC130685zo) this.A01.get(this.A03.A02(i));
    }
}
