package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaTextView;

/* renamed from: X.2vS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59832vS extends AbstractC37191le {
    public final WaTextView A00;
    public final WaTextView A01;

    public C59832vS(View view) {
        super(view);
        this.A00 = C12960it.A0N(view, R.id.error_message);
        this.A01 = C12960it.A0N(view, R.id.load_categories_btn);
    }
}
