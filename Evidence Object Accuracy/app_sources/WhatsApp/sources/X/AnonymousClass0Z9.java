package X;

import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;

/* renamed from: X.0Z9  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Z9 implements AbstractC12560i7 {
    public final /* synthetic */ AnonymousClass0F6 A00;

    public AnonymousClass0Z9(AnonymousClass0F6 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC12560i7
    public boolean ARR(MotionEvent motionEvent, RecyclerView recyclerView) {
        int findPointerIndex;
        AnonymousClass0F6 r3 = this.A00;
        r3.A0H.A00.AXZ(motionEvent);
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            r3.A09 = motionEvent.getPointerId(0);
            r3.A02 = motionEvent.getX();
            r3.A03 = motionEvent.getY();
            VelocityTracker velocityTracker = r3.A0F;
            if (velocityTracker != null) {
                velocityTracker.recycle();
            }
            r3.A0F = VelocityTracker.obtain();
            if (r3.A0L == null) {
                List list = r3.A0O;
                if (!list.isEmpty()) {
                    View A05 = r3.A05(motionEvent);
                    int size = list.size();
                    while (true) {
                        size--;
                        if (size < 0) {
                            break;
                        }
                        C06590Ug r7 = (C06590Ug) list.get(size);
                        AnonymousClass03U r6 = r7.A0C;
                        View view = r6.A0H;
                        if (view == A05) {
                            r3.A02 -= r7.A01;
                            r3.A03 -= r7.A02;
                            r3.A0B(r6, true);
                            if (r3.A0S.remove(view)) {
                                AnonymousClass0Z1.A00.A7F(view);
                            }
                            r3.A0A(r6, r7.A0A);
                            r3.A07(motionEvent, r3.A0B, 0);
                        }
                    }
                }
            }
        } else if (actionMasked == 3 || actionMasked == 1) {
            r3.A09 = -1;
            r3.A0A(null, 0);
        } else {
            int i = r3.A09;
            if (i != -1 && (findPointerIndex = motionEvent.findPointerIndex(i)) >= 0) {
                r3.A06(motionEvent, actionMasked, findPointerIndex);
            }
        }
        VelocityTracker velocityTracker2 = r3.A0F;
        if (velocityTracker2 != null) {
            velocityTracker2.addMovement(motionEvent);
        }
        if (r3.A0L == null) {
            return false;
        }
        return true;
    }

    @Override // X.AbstractC12560i7
    public void AV2(boolean z) {
        if (z) {
            this.A00.A0A(null, 0);
        }
    }

    @Override // X.AbstractC12560i7
    public void AXa(MotionEvent motionEvent, RecyclerView recyclerView) {
        AnonymousClass0F6 r3 = this.A00;
        r3.A0H.A00.AXZ(motionEvent);
        VelocityTracker velocityTracker = r3.A0F;
        if (velocityTracker != null) {
            velocityTracker.addMovement(motionEvent);
        }
        if (r3.A09 != -1) {
            int actionMasked = motionEvent.getActionMasked();
            int findPointerIndex = motionEvent.findPointerIndex(r3.A09);
            if (findPointerIndex >= 0) {
                r3.A06(motionEvent, actionMasked, findPointerIndex);
            }
            AnonymousClass03U r1 = r3.A0L;
            if (r1 != null) {
                int i = 0;
                if (actionMasked != 1) {
                    if (actionMasked != 2) {
                        if (actionMasked == 3) {
                            VelocityTracker velocityTracker2 = r3.A0F;
                            if (velocityTracker2 != null) {
                                velocityTracker2.clear();
                            }
                        } else if (actionMasked == 6) {
                            int actionIndex = motionEvent.getActionIndex();
                            if (motionEvent.getPointerId(actionIndex) == r3.A09) {
                                if (actionIndex == 0) {
                                    i = 1;
                                }
                                r3.A09 = motionEvent.getPointerId(i);
                                r3.A07(motionEvent, r3.A0B, actionIndex);
                                return;
                            }
                            return;
                        } else {
                            return;
                        }
                    } else if (findPointerIndex >= 0) {
                        r3.A07(motionEvent, r3.A0B, findPointerIndex);
                        r3.A09(r1);
                        RecyclerView recyclerView2 = r3.A0M;
                        Runnable runnable = r3.A0R;
                        recyclerView2.removeCallbacks(runnable);
                        runnable.run();
                        r3.A0M.invalidate();
                        return;
                    } else {
                        return;
                    }
                }
                r3.A0A(null, 0);
                r3.A09 = -1;
            }
        }
    }
}
