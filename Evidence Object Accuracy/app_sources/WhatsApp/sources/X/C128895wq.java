package X;

import android.text.TextUtils;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;

/* renamed from: X.5wq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128895wq {
    public final C120465gE A00;
    public final C125875rx A01;
    public final Runnable A02;

    public C128895wq(C120465gE r1, C125875rx r2, Runnable runnable) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = runnable;
    }

    public void A00(C30821Yy r15, AbstractC28901Pl r16, UserJid userJid, AnonymousClass1ZR r18, C119835fB r19, C50952Rz r20, String str, String str2, String str3, String str4, String str5, String str6, String str7, long j, boolean z, boolean z2, boolean z3) {
        if (z3) {
            this.A02.run();
            return;
        }
        C120465gE r11 = this.A00;
        String str8 = r16.A0A;
        C125875rx r13 = this.A01;
        String str9 = r19.A0L;
        String str10 = r19.A0M;
        String str11 = r19.A0J;
        String str12 = r19.A0K;
        String str13 = r19.A0N;
        String str14 = r19.A0H;
        String str15 = r19.A0I;
        ArrayList A0l = C12960it.A0l();
        C117295Zj.A1M("sender-vpa", str9, A0l);
        if (!TextUtils.isEmpty(str10)) {
            C117295Zj.A1M("sender-vpa-id", str10, A0l);
        }
        if (!TextUtils.isEmpty(str11)) {
            AnonymousClass009.A05(str11);
            C117295Zj.A1M("receiver-vpa", str11, A0l);
        }
        if (!TextUtils.isEmpty(str12)) {
            C117295Zj.A1M("receiver-vpa-id", str12, A0l);
        }
        if (!TextUtils.isEmpty(null)) {
            C117295Zj.A1M("upi-bank-info", null, A0l);
        }
        C117295Zj.A1M("seq-no", str13, A0l);
        if (!TextUtils.isEmpty(str14)) {
            C117295Zj.A1M("mcc", str14, A0l);
        }
        if (!TextUtils.isEmpty(str3)) {
            C117295Zj.A1M("ref-id", str3, A0l);
        }
        if (!TextUtils.isEmpty(null)) {
            C117295Zj.A1M("ref-url", null, A0l);
        }
        if (!AnonymousClass1ZS.A02(r18)) {
            C117295Zj.A1M("payee-name", C117315Zl.A0K(r18), A0l);
        }
        if (!TextUtils.isEmpty(null)) {
            C117295Zj.A1M("mode", null, A0l);
        }
        if (!TextUtils.isEmpty(str15)) {
            C117295Zj.A1M("purpose-code", str15, A0l);
        }
        if (!TextUtils.isEmpty(str7)) {
            C117295Zj.A1M("note", str7, A0l);
        }
        C14850m9 r6 = r11.A05;
        if (r6.A07(1918)) {
            if (!TextUtils.isEmpty(str2)) {
                C117295Zj.A1M("merchant-token", str2, A0l);
            }
            String str16 = "1";
            String str17 = "0";
            if (z) {
                str17 = str16;
            }
            C117295Zj.A1M("merchant", str17, A0l);
            if (!z2) {
                str16 = "0";
            }
            C117295Zj.A1M("verified-merchant", str16, A0l);
        }
        AnonymousClass1V8 r4 = null;
        if (!TextUtils.isEmpty(str3) && !TextUtils.isEmpty(str6)) {
            ArrayList A0l2 = C12960it.A0l();
            C117295Zj.A1M("order-id", str3, A0l2);
            C117295Zj.A1M("payment-config-id", str6, A0l2);
            if (!TextUtils.isEmpty(str4) && r6.A07(1599)) {
                C117295Zj.A1M("discount-program-name", str4, A0l2);
            }
            if (j != 0) {
                A0l2.add(new AnonymousClass1W9("expiry-ts", j));
            }
            if (r6.A07(1330) && !TextUtils.isEmpty(str5)) {
                C117295Zj.A1M("order-type", str5, A0l2);
            }
            r4 = new AnonymousClass1V8("order", C117305Zk.A1b(A0l2));
        }
        AnonymousClass1V8 r3 = new AnonymousClass1V8(r4, "upi", C117305Zk.A1b(A0l));
        Log.i("PAY: IndiaUpiPayPrecheckAction sendPrecheck called");
        ArrayList A0l3 = C12960it.A0l();
        C117295Zj.A1M("action", "pay-precheck", A0l3);
        C117295Zj.A1M("country", "IN", A0l3);
        C117295Zj.A1M("credential-id", str8, A0l3);
        if (userJid != null) {
            A0l3.add(new AnonymousClass1W9(userJid, "receiver"));
        }
        A0l3.add(new AnonymousClass1W9("version", 1));
        C117295Zj.A1M("nonce", C117305Zk.A0j(r11.A02, r11.A03), A0l3);
        C117295Zj.A1M("device-id", r11.A0C.A01(), A0l3);
        C117295Zj.A1M("transaction-type", str, A0l3);
        if (r20 != null) {
            A0l3.add(new AnonymousClass1W9("offer_id", r20.A01));
        }
        AnonymousClass1V8 r7 = null;
        if (r15 != null) {
            r7 = r11.A08.A04(C30771Yt.A05, r15, "amount");
        }
        C64513Fv r10 = ((C126705tJ) r11).A00;
        if (r10 != null) {
            r10.A04("pay-precheck");
        }
        AnonymousClass1V8 r32 = new AnonymousClass1V8("account", C117305Zk.A1b(A0l3), new AnonymousClass1V8[]{r3, r7});
        r11.A0B.A02(185478318, "in_pay_precheck_tag");
        C117305Zk.A1J(r11.A08, new C120885gu(r11.A00, r11.A01, r11.A07, r10, r11, r20, r13), r32);
    }
}
