package X;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import androidx.fragment.app.DialogFragment;
import com.facebook.redex.IDxDListenerShape14S0100000_3_I1;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.payments.ui.NoviGetStartedFragment;
import com.whatsapp.payments.ui.PaymentBottomSheet;
import com.whatsapp.util.Log;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5bL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118145bL extends AnonymousClass015 {
    public C14580lf A00;
    public AbstractC30791Yv A01;
    public AbstractC28901Pl A02;
    public C127905vF A03;
    public C27691It A04 = C13000ix.A03();
    public C27691It A05 = C13000ix.A03();
    public C27691It A06 = C13000ix.A03();
    public C27691It A07 = C13000ix.A03();
    public C27691It A08 = C13000ix.A03();
    public C27691It A09 = C13000ix.A03();
    public String A0A = "";
    public boolean A0B = false;
    public final AnonymousClass016 A0C = C12980iv.A0T();
    public final AnonymousClass016 A0D = C12980iv.A0T();
    public final AnonymousClass016 A0E = C12980iv.A0T();
    public final AnonymousClass016 A0F = C12980iv.A0T();
    public final C20920wX A0G;
    public final C14900mE A0H;
    public final C15610nY A0I;
    public final C14830m7 A0J;
    public final AnonymousClass018 A0K;
    public final C20830wO A0L;
    public final C15650ng A0M;
    public final AnonymousClass102 A0N;
    public final C14850m9 A0O;
    public final AnonymousClass4UZ A0P = new C120125ff(this);
    public final C248217a A0Q;
    public final C18610sj A0R;
    public final C17900ra A0S;
    public final C17070qD A0T;
    public final C130155yt A0U;
    public final C129865yQ A0V;
    public final C20350vc A0W;
    public final C125685re A0X;
    public final C129325xX A0Y = new C129325xX();
    public final C130125yq A0Z;
    public final AnonymousClass60Y A0a;
    public final AnonymousClass61F A0b;
    public final C130105yo A0c;
    public final C127875vC A0d;
    public final C129685y8 A0e;
    public final AnonymousClass61C A0f;
    public final C129675y7 A0g;
    public final C129615y0 A0h;
    public final AnonymousClass61P A0i;
    public final C129175xI A0j;
    public final C22120yY A0k;
    public final C20320vZ A0l;
    public final C27691It A0m = C13000ix.A03();
    public final C27691It A0n = C13000ix.A03();
    public final C27691It A0o = C13000ix.A03();
    public final C27691It A0p = C13000ix.A03();
    public final C27691It A0q = C13000ix.A03();
    public final C27691It A0r = C13000ix.A03();
    public final C27691It A0s = C13000ix.A03();
    public final C27691It A0t = C13000ix.A03();
    public final C27691It A0u = C13000ix.A03();
    public final C27691It A0v = C13000ix.A03();
    public final C27691It A0w = C13000ix.A03();
    public final C27691It A0x = C13000ix.A03();
    public final C27691It A0y = C13000ix.A03();
    public final C27691It A0z = C13000ix.A03();
    public final C27691It A10 = C13000ix.A03();
    public final AbstractC14440lR A11;
    public volatile Context A12;

    public C118145bL(C20920wX r5, AnonymousClass12P r6, C14900mE r7, ActivityC13790kL r8, C15610nY r9, C14830m7 r10, AnonymousClass018 r11, C20830wO r12, C15650ng r13, AnonymousClass102 r14, C14850m9 r15, C248217a r16, C18610sj r17, AnonymousClass61M r18, C17900ra r19, C17070qD r20, C130155yt r21, C20350vc r22, C125685re r23, C130125yq r24, AnonymousClass60Y r25, AnonymousClass61F r26, C130105yo r27, C127875vC r28, C129685y8 r29, AnonymousClass61C r30, C129675y7 r31, AnonymousClass61P r32, C129175xI r33, C22120yY r34, C20320vZ r35, AbstractC14440lR r36) {
        this.A12 = r8;
        this.A0J = r10;
        this.A0O = r15;
        this.A0H = r7;
        this.A11 = r36;
        this.A0g = r31;
        this.A0X = r23;
        this.A0I = r9;
        this.A0K = r11;
        this.A0l = r35;
        this.A0a = r25;
        this.A0T = r20;
        this.A0G = r5;
        this.A0M = r13;
        this.A0U = r21;
        this.A0b = r26;
        this.A0e = r29;
        this.A0k = r34;
        this.A0j = r33;
        this.A0Z = r24;
        this.A0R = r17;
        this.A0c = r27;
        this.A0d = r28;
        C129865yQ r1 = new C129865yQ(r6, r8, r18);
        this.A0V = r1;
        this.A0N = r14;
        this.A0S = r19;
        this.A0Q = r16;
        this.A0i = r32;
        this.A0L = r12;
        this.A0W = r22;
        this.A0f = r30;
        this.A0h = new C129615y0(r21, r1, r31);
    }

    public static AnonymousClass61D A00(C118145bL r6) {
        return new AnonymousClass61D(r6.A0G, r6.A0J, r6.A0O, r6.A0U, r6.A0Z, r6.A0f);
    }

    public static /* synthetic */ void A01(AbstractC28901Pl r27, AbstractC14640lm r28, UserJid userJid, AnonymousClass1KC r30, C128095vY r31, C118145bL r32, AnonymousClass1KS r33, Integer num, List list, long j) {
        C1310460z r13;
        int i;
        JSONObject jSONObject;
        AnonymousClass61D A00 = A00(r32);
        AnonymousClass6AB r20 = new AbstractC136196Lo(r27, r28, userJid, r30, r31, r32, r33, num, list, j) { // from class: X.6AB
            public final /* synthetic */ long A00;
            public final /* synthetic */ AbstractC28901Pl A01;
            public final /* synthetic */ AbstractC14640lm A02;
            public final /* synthetic */ UserJid A03;
            public final /* synthetic */ AnonymousClass1KC A04;
            public final /* synthetic */ C128095vY A05;
            public final /* synthetic */ C118145bL A06;
            public final /* synthetic */ AnonymousClass1KS A07;
            public final /* synthetic */ Integer A08;
            public final /* synthetic */ List A09;

            {
                this.A06 = r6;
                this.A05 = r5;
                this.A02 = r2;
                this.A00 = r10;
                this.A07 = r7;
                this.A08 = r8;
                this.A04 = r4;
                this.A03 = r3;
                this.A01 = r1;
                this.A09 = r9;
            }

            @Override // X.AbstractC136196Lo
            public final void AV8(C130785zy r25) {
                Object obj;
                C118145bL r5 = this.A06;
                C128095vY r10 = this.A05;
                AbstractC14640lm r0 = this.A02;
                long j2 = this.A00;
                AnonymousClass1KS r02 = this.A07;
                Integer num2 = this.A08;
                AnonymousClass1KC r03 = this.A04;
                UserJid userJid2 = this.A03;
                AbstractC28901Pl r7 = this.A01;
                List list2 = this.A09;
                if (!r25.A06() || (obj = r25.A02) == null) {
                    C1316663q r4 = r25.A01;
                    if (r4 != null) {
                        r5.A0n.A0B(Boolean.FALSE);
                        boolean A02 = r4.A02();
                        r5.A03 = r5.A07(r7, r0, userJid2, r03, (C120995h5) r25.A02, r4, r02, num2, list2, j2);
                        if (A02) {
                            C127415uS.A00(r4, r5.A0g, "PASS", 2);
                            return;
                        }
                        return;
                    }
                    r5.A0C(r25.A00);
                    return;
                }
                C120995h5 r9 = (C120995h5) obj;
                C128365vz r8 = new AnonymousClass610("TRANSACTION_MODEL_CREATED", "SEND_MONEY").A00;
                r8.A0j = "REVIEW_TRANSACTION";
                C121035h9 r04 = r9.A00;
                if (r04 != null) {
                    r8.A0M = r04.A05;
                    r8.A01 = Boolean.TRUE;
                    AnonymousClass60Y r14 = r5.A0a;
                    AnonymousClass610 r15 = new AnonymousClass610("ABTU_TRIGGERED", "SEND_MONEY");
                    C128365vz r12 = r15.A00;
                    r12.A0j = "REVIEW_TRANSACTION";
                    r15.A05(r10.A04, r10.A03.A01, r10.A05, r10.A00);
                    r12.A0Q = "SUCCESSFUL";
                    C128365vz.A01(r12, r10.A01);
                    r14.A06(r12);
                } else {
                    r8.A01 = Boolean.FALSE;
                }
                r8.A0Q = "SUCCESSFUL";
                r8.A0n = "P2P";
                r8.A0m = r9.A05;
                r5.A0a.A05(r8);
                r5.A11.Ab2(new RunnableC135456In(r5.A07(r7, r0, userJid2, r03, r9, null, r02, num2, list2, j2), r5));
                C117315Zl.A0Q(r5.A04);
            }
        };
        String str = AnonymousClass600.A03;
        C130155yt r0 = A00.A03;
        String A05 = r0.A05();
        long A002 = A00.A01.A00();
        String A0n = C12990iw.A0n();
        C20920wX r02 = A00.A00;
        C1315163b r1 = r31.A05;
        AnonymousClass63Z r9 = r1.A03;
        UserJid userJid2 = r9.A00;
        AnonymousClass009.A05(userJid2);
        C126905td r132 = new C126905td(r02, userJid2);
        C1316363n r5 = r1.A05.A00;
        C1316363n r4 = r9.A01;
        C1316563p r3 = r1.A02;
        JSONObject A0a = C117295Zj.A0a();
        try {
            C117295Zj.A1L(str, A05, A0a, A002);
            A0a.put("client_idempotency_key", A0n);
            JSONObject A0a2 = C117295Zj.A0a();
            try {
                jSONObject = C117295Zj.A0a().put("country_code", r132.A00).put("national_number", r132.A01);
            } catch (JSONException unused) {
                Log.e("PAY: PhoneNumberPayload/toJson can't construct json");
                jSONObject = null;
            }
            A0a.put("receiver", A0a2.put("whatsapp_phone_number", jSONObject));
            A0a.put("sender_trading_currency", ((AbstractC30781Yu) C130325zE.A01(r5.A02, "sender_trading_amount", A0a)).A04);
            A0a.put("sender_local_currency", ((AbstractC30781Yu) C130325zE.A01(r5.A01, "sender_local_amount", A0a)).A04);
            A0a.put("receiver_trading_currency", ((AbstractC30781Yu) C130325zE.A01(r4.A02, "receiver_trading_amount", A0a)).A04);
            A0a.put("receiver_local_currency", ((AbstractC30781Yu) C130325zE.A01(r4.A01, "receiver_local_amount", A0a)).A04);
            A0a.put("sender_non_tradeable_quote_id", Long.toString(r5.A00));
            A0a.put("receiver_non_tradeable_quote_id", Long.toString(r4.A00));
            A0a.put("tradeable_coin_exchange_quote_id", Long.toString(r3.A01));
        } catch (JSONException unused2) {
            Log.e("PAY: IntentPayloadHelper/getSendP2pIntentPayload/toJson can't construct json");
        }
        C130125yq r10 = A00.A04;
        C129585xx r12 = new C129585xx(r10, "SEND_P2P", A0a);
        C1310460z r2 = new C1310460z("sender-info");
        r2.A02.add(AnonymousClass61D.A00(r5));
        C1310460z r52 = new C1310460z("receiver-info");
        r52.A02.add(AnonymousClass61D.A00(r4));
        AnonymousClass63X r11 = r31.A00;
        if (r11 != null) {
            AnonymousClass61S[] r15 = new AnonymousClass61S[2];
            AnonymousClass61S.A04("credential-id", r11.A00.A0A, r15);
            C1316563p r03 = r31.A04.A04;
            AnonymousClass009.A05(r03);
            r13 = C117315Zl.A0B("top-up", C12960it.A0m(new AnonymousClass61S("quote-id", r03.A01), r15, 1));
            C1316363n r152 = r11.A01;
            C1310460z A02 = C130325zE.A02(r152.A01, "local");
            C1310460z A022 = C130325zE.A02(r152.A02, "trading");
            ArrayList arrayList = r13.A02;
            arrayList.add(A02);
            arrayList.add(A022);
        } else {
            r13 = null;
        }
        AnonymousClass61S[] r14 = new AnonymousClass61S[5];
        AnonymousClass009.A05(r9);
        AnonymousClass61S A003 = AnonymousClass61S.A00("receiver", userJid2.getRawString());
        A003.A00 = userJid2;
        r14[0] = A003;
        r14[1] = AnonymousClass61S.A00("action", "novi-pay-precheck");
        r14[2] = AnonymousClass61S.A00("nonce", A0n);
        r14[3] = new AnonymousClass61S("quote-id", r3.A01);
        C1310460z A0F = C117295Zj.A0F(AnonymousClass61S.A00("send_p2p_signed_intent", C129585xx.A00(r10, r12)), r14, 4);
        if (!TextUtils.isEmpty(r31.A01)) {
            A0F.A01.add(AnonymousClass61S.A00("message", r31.A01));
        }
        ArrayList arrayList2 = A0F.A02;
        arrayList2.add(r2);
        arrayList2.add(r52);
        C126845tX r22 = r31.A06;
        C1310460z A023 = C130325zE.A02(r22.A00, "input");
        A023.A01.add(new AnonymousClass61S("in-sender-currency", r22.A01));
        arrayList2.add(A023);
        if (r13 != null) {
            arrayList2.add(r13);
            AnonymousClass009.A05(r11);
            String str2 = r11.A00.A0A;
            AnonymousClass009.A05(r11);
            C1316363n r23 = r11.A01;
            String l = Long.toString(r23.A00);
            AnonymousClass009.A05(r11);
            AnonymousClass6F2 r16 = r23.A02;
            AnonymousClass009.A05(r11);
            i = 2;
            A0F.A01.addAll(AnonymousClass61S.A02("deposit_signed_intent", C129585xx.A00(r10, new C129585xx(r10, "DEPOSIT_DEBIT_CARD", AnonymousClass61C.A00(r16, r23.A01, A0n, str2, l, str, A05, A002)))));
        } else {
            i = 2;
        }
        C130155yt.A04(C117305Zk.A09(r20, r31, 4), r0, A0F, i, 5);
    }

    public static boolean A02(C118145bL r0) {
        return !TextUtils.isEmpty((CharSequence) r0.A0z.A01());
    }

    public int A04() {
        AnonymousClass63Y r2 = (AnonymousClass63Y) this.A0E.A01();
        C128735wa r1 = (C128735wa) this.A0D.A01();
        if (!(r2 == null || r1 == null || !r1.A00())) {
            if (!C117305Zk.A1V(r2.A01, ((AbstractC30781Yu) r1.A01.A01).A04)) {
                return 1;
            }
        }
        return 0;
    }

    public int A05() {
        C17930rd A01 = this.A0S.A01();
        AnonymousClass009.A05(A01);
        AbstractC38041nQ A012 = this.A0T.A01(A01.A03);
        AnonymousClass009.A05(A012);
        ArrayList A0l = C12960it.A0l();
        for (AbstractC129495xo r0 : ((AnonymousClass6BF) A012).A01) {
            C127705uv A00 = r0.A00();
            if (A00 != null) {
                A0l.add(A00);
            }
        }
        int i = 0;
        if (A0l.size() > 0) {
            Iterator it = A0l.iterator();
            do {
                i = 1;
                if (!it.hasNext()) {
                    return 2;
                }
            } while (((C127705uv) it.next()).A03 != 0);
            if (A0l.size() != 1) {
                return 2;
            }
        }
        return i;
    }

    public final AnonymousClass63X A06(AbstractC28901Pl r9, AnonymousClass6F2 r10, C1315863i r11, C1316263m r12) {
        C1316563p r6;
        C127695uu r2;
        AnonymousClass6F2 r4 = r12.A02;
        if (r4.compareTo(r10) >= 0 || (r6 = r11.A04) == null || r9 == null) {
            return null;
        }
        String str = ((AbstractC30781Yu) r4.A00).A04;
        AbstractC30791Yv r3 = r10.A00;
        AbstractC30781Yu r22 = (AbstractC30781Yu) r3;
        if (str.equals(r22.A04)) {
            AnonymousClass6F2 A0R = C117305Zk.A0R(r3, r10.A01.A00.subtract(r4.A01.A00), r22.A01);
            AnonymousClass102 r7 = this.A0N;
            String str2 = r6.A03;
            boolean A1V = C117305Zk.A1V(A0R.A00, str2);
            BigDecimal bigDecimal = r6.A05;
            if (A1V) {
                r2 = new C127695uu(r7.A02(r6.A04), bigDecimal, 1, true);
            } else {
                r2 = new C127695uu(r7.A02(str2), bigDecimal, 0, true);
            }
            return new AnonymousClass63X(r9, new C1316363n(A0R.A05(r2), A0R, r6.A01));
        }
        throw C12970iu.A0f("Can't subtract two varying currency amounts");
    }

    public final C127905vF A07(AbstractC28901Pl r15, AbstractC14640lm r16, UserJid userJid, AnonymousClass1KC r18, C120995h5 r19, C1316663q r20, AnonymousClass1KS r21, Integer num, List list, long j) {
        AbstractC15340mz A02;
        AbstractC15340mz r8 = null;
        if (r21 == null) {
            C20320vZ r5 = this.A0l;
            AnonymousClass009.A05(r16);
            if (j != 0) {
                r8 = this.A0M.A0K.A00(j);
            }
            A02 = r5.A03(null, r16, r8, "", list, 0, false);
            if (C15380n4.A0J(r16)) {
                A02.A0e(userJid);
            }
        } else {
            C20350vc r52 = this.A0W;
            AnonymousClass009.A05(r16);
            if (j != 0) {
                r8 = this.A0M.A0K.A00(j);
            }
            A02 = r52.A02(r16, userJid, r8, r21, num);
        }
        C119825fA r4 = new C119825fA();
        r4.A03 = A02.A0z.A01;
        r4.A01 = r19;
        r4.A02 = r20;
        return new C127905vF(r15, r18, r19, r4, A02);
    }

    public final void A08(AbstractC001200n r13, AnonymousClass63Y r14, AnonymousClass63Y r15) {
        C127875vC r10 = this.A0d;
        C27691It A03 = C13000ix.A03();
        AbstractC30791Yv r6 = r14.A00;
        AbstractC30791Yv r0 = r15.A00;
        AbstractC30781Yu r62 = (AbstractC30781Yu) r6;
        String str = r62.A04;
        String str2 = ((AbstractC30781Yu) r0).A04;
        C129505xp r9 = new C129505xp(str, str2);
        C1315863i r2 = (C1315863i) r10.A00.A04(r9);
        if (r2 == null || !r2.A02(System.currentTimeMillis())) {
            ArrayList A0l = C12960it.A0l();
            BigDecimal bigDecimal = BigDecimal.ONE;
            int A00 = C117315Zl.A00(r62);
            BigDecimal movePointRight = bigDecimal.movePointRight(A00);
            AnonymousClass61S.A03("action", "novi-currency-exchange-rates", A0l);
            AnonymousClass61S.A03("sender-iso-code", str, A0l);
            AnonymousClass61S.A03("receiver-iso-code", str2, A0l);
            C1310460z A0B = C117315Zl.A0B("account", A0l);
            String plainString = movePointRight.toPlainString();
            C1310460z r22 = new C1310460z("amount");
            AnonymousClass61S[] r4 = new AnonymousClass61S[3];
            AnonymousClass61S.A04("value", plainString, r4);
            r4[1] = AnonymousClass61S.A00("offset", BigDecimal.ONE.movePointRight(A00).toString());
            C117305Zk.A1K(r22, "money", C12960it.A0m(AnonymousClass61S.A00("currency", str), r4, 2));
            A0B.A02.add(r22);
            r10.A03.A0B(new AbstractC136196Lo(r14, r15, r9, r10, A03) { // from class: X.6A7
                public final /* synthetic */ AnonymousClass63Y A00;
                public final /* synthetic */ AnonymousClass63Y A01;
                public final /* synthetic */ C129505xp A02;
                public final /* synthetic */ C127875vC A03;
                public final /* synthetic */ C27691It A04;

                {
                    this.A03 = r4;
                    this.A00 = r1;
                    this.A01 = r2;
                    this.A02 = r3;
                    this.A04 = r5;
                }

                @Override // X.AbstractC136196Lo
                public final void AV8(C130785zy r152) {
                    Object obj;
                    C1316563p r11;
                    C127875vC r5 = this.A03;
                    AnonymousClass63Y r8 = this.A00;
                    AnonymousClass63Y r92 = this.A01;
                    C129505xp r42 = this.A02;
                    C27691It r3 = this.A04;
                    if (r152.A06() && (obj = r152.A02) != null) {
                        AnonymousClass1V8 r7 = (AnonymousClass1V8) obj;
                        try {
                            AnonymousClass102 r1 = r5.A02;
                            C1316563p A002 = C1315863i.A00(r1, r7.A0F("trading").A0F("quote"));
                            C1316563p A003 = C1315863i.A00(r1, r7.A0F("sender").A0F("quote"));
                            C1316563p A004 = C1315863i.A00(r1, r7.A0F("receiver").A0F("quote"));
                            AnonymousClass1V8 A0E = r7.A0E("top-up");
                            if (A0E != null) {
                                r11 = C1315863i.A00(r1, A0E.A0F("quote"));
                            } else {
                                r11 = null;
                            }
                            C1315863i r72 = new C1315863i(r8, r92, A002, r11, A003, A004);
                            if (r72.A02(r5.A01.A00())) {
                                r5.A00.A08(r42, r72);
                                C130785zy.A02(r3, null, r72);
                                return;
                            }
                        } catch (AnonymousClass1V9 unused) {
                            Log.e("PAY: ExchangeRateRepository/fetchExchangeRate can't construct object");
                        }
                    }
                    C130785zy.A02(r3, r152.A00, null);
                }
            }, A0B, "get", 2);
        } else {
            C130785zy.A02(A03, null, r2);
        }
        A03.A05(r13, new AnonymousClass02B(r13, r14, r15, this) { // from class: X.65l
            public final /* synthetic */ AbstractC001200n A00;
            public final /* synthetic */ AnonymousClass63Y A01;
            public final /* synthetic */ AnonymousClass63Y A02;
            public final /* synthetic */ C118145bL A03;

            {
                this.A03 = r4;
                this.A01 = r2;
                this.A02 = r3;
                this.A00 = r1;
            }

            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                Object obj2;
                C118145bL r7 = this.A03;
                AnonymousClass63Y r63 = this.A01;
                AnonymousClass63Y r5 = this.A02;
                AbstractC001200n r42 = this.A00;
                C130785zy r11 = (C130785zy) obj;
                if (!r11.A06() || (obj2 = r11.A02) == null) {
                    r7.A0w.A0B(new C126125sN(
                    /*  JADX ERROR: Method code generation error
                        jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x007f: INVOKE  
                          (wrap: X.1It : 0x0073: IGET  (r2v0 X.1It A[REMOVE]) = (r7v0 'r7' X.5bL) X.5bL.A0w X.1It)
                          (wrap: X.5sN : 0x007c: CONSTRUCTOR  (r0v1 X.5sN A[REMOVE]) = 
                          (wrap: X.6DC : 0x0077: CONSTRUCTOR  (r1v0 X.6DC A[REMOVE]) = (r4v0 'r42' X.00n), (r6v0 'r63' X.63Y), (r5v0 'r5' X.63Y), (r7v0 'r7' X.5bL) call: X.6DC.<init>(X.00n, X.63Y, X.63Y, X.5bL):void type: CONSTRUCTOR)
                         call: X.5sN.<init>(X.6MB):void type: CONSTRUCTOR)
                         type: VIRTUAL call: X.017.A0B(java.lang.Object):void in method: X.65l.ANq(java.lang.Object):void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                        	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                        	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                        	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                        	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                        	at java.util.ArrayList.forEach(ArrayList.java:1259)
                        	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                        	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                        Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x007c: CONSTRUCTOR  (r0v1 X.5sN A[REMOVE]) = 
                          (wrap: X.6DC : 0x0077: CONSTRUCTOR  (r1v0 X.6DC A[REMOVE]) = (r4v0 'r42' X.00n), (r6v0 'r63' X.63Y), (r5v0 'r5' X.63Y), (r7v0 'r7' X.5bL) call: X.6DC.<init>(X.00n, X.63Y, X.63Y, X.5bL):void type: CONSTRUCTOR)
                         call: X.5sN.<init>(X.6MB):void type: CONSTRUCTOR in method: X.65l.ANq(java.lang.Object):void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                        	... 23 more
                        Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0077: CONSTRUCTOR  (r1v0 X.6DC A[REMOVE]) = (r4v0 'r42' X.00n), (r6v0 'r63' X.63Y), (r5v0 'r5' X.63Y), (r7v0 'r7' X.5bL) call: X.6DC.<init>(X.00n, X.63Y, X.63Y, X.5bL):void type: CONSTRUCTOR in method: X.65l.ANq(java.lang.Object):void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:708)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                        	... 29 more
                        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6DC, state: NOT_LOADED
                        	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                        	... 35 more
                        */
                    /*
                        this = this;
                        X.5bL r7 = r10.A03
                        X.63Y r6 = r10.A01
                        X.63Y r5 = r10.A02
                        X.00n r4 = r10.A00
                        X.5zy r11 = (X.C130785zy) r11
                        boolean r0 = r11.A06()
                        if (r0 == 0) goto L_0x0073
                        java.lang.Object r9 = r11.A02
                        if (r9 == 0) goto L_0x0073
                        X.1It r0 = r7.A0p
                        java.lang.Object r3 = r0.A01()
                        X.63i r9 = (X.C1315863i) r9
                        r0.A0B(r9)
                        X.1Yv r2 = r6.A01
                        r0 = r2
                        X.1Yu r0 = (X.AbstractC30781Yu) r0
                        java.lang.String r1 = r0.A04
                        X.1Yv r0 = r5.A01
                        boolean r0 = X.C117305Zk.A1V(r0, r1)
                        if (r0 == 0) goto L_0x0038
                        X.1Yv r0 = r6.A02
                        X.1Yw r0 = (X.C30801Yw) r0
                        boolean r0 = r0.A00(r2)
                        if (r0 != 0) goto L_0x0072
                    L_0x0038:
                        if (r3 == 0) goto L_0x0043
                        X.0mE r2 = r7.A0H
                        r1 = 2131889705(0x7f120e29, float:1.9414081E38)
                        r0 = 1
                        r2.A07(r1, r0)
                    L_0x0043:
                        java.util.concurrent.TimeUnit r8 = java.util.concurrent.TimeUnit.MICROSECONDS
                        X.63p r0 = r9.A05
                        long r2 = r0.A00
                        X.63p r0 = r9.A03
                        long r0 = r0.A00
                        long r2 = java.lang.Math.min(r2, r0)
                        X.63p r0 = r9.A02
                        long r0 = r0.A00
                        long r0 = java.lang.Math.min(r2, r0)
                        long r8 = r8.toMillis(r0)
                        X.0m7 r0 = r7.A0J
                        long r0 = r0.A00()
                        long r8 = r8 - r0
                        X.1It r2 = r7.A0v
                        X.6K1 r1 = new X.6K1
                        r1.<init>(r4, r6, r5, r7)
                        java.lang.Long r0 = java.lang.Long.valueOf(r8)
                        X.C117305Zk.A1B(r2, r1, r0)
                    L_0x0072:
                        return
                    L_0x0073:
                        X.1It r2 = r7.A0w
                        X.6DC r1 = new X.6DC
                        r1.<init>(r4, r6, r5, r7)
                        X.5sN r0 = new X.5sN
                        r0.<init>(r1)
                        r2.A0B(r0)
                        return
                    */
                    throw new UnsupportedOperationException("Method not decompiled: X.C1321265l.ANq(java.lang.Object):void");
                }
            });
        }

        /* JADX WARNING: Code restructure failed: missing block: B:23:0x0072, code lost:
            if (((X.AbstractC30781Yu) r5).A04.equals(r9) == false) goto L_0x0074;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void A09(X.C30821Yy r19, X.C1315863i r20) {
            /*
            // Method dump skipped, instructions count: 253
            */
            throw new UnsupportedOperationException("Method not decompiled: X.C118145bL.A09(X.1Yy, X.63i):void");
        }

        public final void A0A(C30821Yy r12, C1315863i r13) {
            C27691It r2;
            AnonymousClass4OZ r1;
            AbstractC30791Yv r0;
            Object A01 = this.A0r.A01();
            boolean A0G = this.A0b.A0G();
            C128735wa r22 = (C128735wa) this.A0D.A01();
            Object A012 = this.A0E.A01();
            if (A01 != null && A0G) {
                if (r22 != null) {
                    if (r22.A00()) {
                        if (!(r12 == null || r13 == null || A012 == null || (r0 = this.A01) == null)) {
                            C30821Yy A013 = r13.A01(r0, r12, 0);
                            C15370n3 A014 = this.A0L.A01(r22.A00);
                            C27691It r8 = this.A0u;
                            String A04 = this.A0I.A04(A014);
                            AbstractC30791Yv r6 = r22.A01.A02;
                            Context context = this.A12;
                            Context context2 = this.A12;
                            Object[] objArr = new Object[2];
                            objArr[0] = A04;
                            C117305Zk.A1A(r8, r6.AA7(context, C12960it.A0X(context2, r6.AAA(this.A0K, A013, 0), objArr, 1, R.string.novi_receive_amount_string)), 2);
                            return;
                        }
                    }
                }
                r2 = this.A0u;
                r1 = new AnonymousClass4OZ(1, null);
                r2.A0B(r1);
            }
            r2 = this.A0u;
            r1 = new AnonymousClass4OZ(0, null);
            r2.A0B(r1);
        }

        public final void A0B(AbstractC28901Pl r6) {
            C1315463e r2;
            AnonymousClass1ZY r0;
            int i = 2;
            if (r6 == null || (r0 = r6.A08) == null) {
                r2 = null;
            } else {
                this.A02 = r6;
                r2 = ((C119805f8) r0).A02;
                if ("ONBOARDED".equals(r2.A03)) {
                    boolean A0G = this.A0b.A0G();
                    if (A0G) {
                        i = 1;
                    }
                    A0D(r2, i, A0G);
                    this.A0r.A0A(r2);
                    return;
                }
            }
            A0D(r2, 2, false);
        }

        public final void A0C(C452120p r9) {
            int i;
            String str;
            String A0X;
            C129525xr r0 = (C129525xr) this.A0x.A01();
            if (r0 != null) {
                r0.A00();
            }
            if (r9 == null || !((i = r9.A00) == 542720028 || i == 426)) {
                this.A0V.A03(r9, new Runnable() { // from class: X.6HR
                    @Override // java.lang.Runnable
                    public final void run() {
                        C117315Zl.A0Q(C118145bL.this.A04);
                    }
                }, new Runnable() { // from class: X.6HU
                    @Override // java.lang.Runnable
                    public final void run() {
                        C117315Zl.A0Q(C118145bL.this.A04);
                    }
                });
                return;
            }
            Context context = this.A12;
            if (i == 542720028) {
                A0X = context.getString(R.string.novi_payment_send_money_generic_error_text);
            } else {
                Object[] A1b = C12970iu.A1b();
                C27691It r3 = this.A0t;
                if (r3.A01() != null) {
                    str = this.A0I.A08(this.A0L.A01((AbstractC14640lm) r3.A01()));
                } else {
                    str = "";
                }
                A0X = C12960it.A0X(context, str, A1b, 0, R.string.payments_receiver_not_updated_error_message);
            }
            this.A0w.A0B(new C126125sN(new AnonymousClass6MB(A0X) { // from class: X.6DB
                public final /* synthetic */ String A01;

                {
                    this.A01 = r2;
                }

                @Override // X.AnonymousClass6MB
                public final AnonymousClass04S ANN(Activity activity) {
                    C118145bL r2 = C118145bL.this;
                    return C130295zB.A00(r2.A12, C126995tm.A00(
                    /*  JADX ERROR: Method code generation error
                        jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0019: RETURN  
                          (wrap: X.04S : 0x0015: INVOKE  (r0v1 X.04S A[REMOVE]) = 
                          (wrap: android.content.Context : 0x0004: IGET  (r3v0 android.content.Context A[REMOVE]) = (r2v0 'r2' X.5bL) X.5bL.A12 android.content.Context)
                          (wrap: X.5tm : 0x000e: INVOKE  (r4v0 X.5tm A[REMOVE]) = 
                          (wrap: X.6HS : 0x000b: CONSTRUCTOR  (r0v0 X.6HS A[REMOVE]) = (r2v0 'r2' X.5bL) call: X.6HS.<init>(X.5bL):void type: CONSTRUCTOR)
                          (wrap: ?? : ?: SGET   com.whatsapp.R.string.ok int)
                         type: STATIC call: X.5tm.A00(java.lang.Runnable, int):X.5tm)
                          (null X.5tm)
                          (null java.lang.String)
                          (wrap: java.lang.String : 0x0002: IGET  (r7v0 java.lang.String A[REMOVE]) = (r9v0 'this' X.6DB A[IMMUTABLE_TYPE, THIS]) X.6DB.A01 java.lang.String)
                          false
                         type: STATIC call: X.5zB.A00(android.content.Context, X.5tm, X.5tm, java.lang.String, java.lang.String, boolean):X.04S)
                         in method: X.6DB.ANN(android.app.Activity):X.04S, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                        	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                        	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                        	at java.util.ArrayList.forEach(ArrayList.java:1259)
                        	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                        	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                        Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0015: INVOKE  (r0v1 X.04S A[REMOVE]) = 
                          (wrap: android.content.Context : 0x0004: IGET  (r3v0 android.content.Context A[REMOVE]) = (r2v0 'r2' X.5bL) X.5bL.A12 android.content.Context)
                          (wrap: X.5tm : 0x000e: INVOKE  (r4v0 X.5tm A[REMOVE]) = 
                          (wrap: X.6HS : 0x000b: CONSTRUCTOR  (r0v0 X.6HS A[REMOVE]) = (r2v0 'r2' X.5bL) call: X.6HS.<init>(X.5bL):void type: CONSTRUCTOR)
                          (wrap: ?? : ?: SGET   com.whatsapp.R.string.ok int)
                         type: STATIC call: X.5tm.A00(java.lang.Runnable, int):X.5tm)
                          (null X.5tm)
                          (null java.lang.String)
                          (wrap: java.lang.String : 0x0002: IGET  (r7v0 java.lang.String A[REMOVE]) = (r9v0 'this' X.6DB A[IMMUTABLE_TYPE, THIS]) X.6DB.A01 java.lang.String)
                          false
                         type: STATIC call: X.5zB.A00(android.content.Context, X.5tm, X.5tm, java.lang.String, java.lang.String, boolean):X.04S in method: X.6DB.ANN(android.app.Activity):X.04S, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:340)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                        	... 15 more
                        Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x000e: INVOKE  (r4v0 X.5tm A[REMOVE]) = 
                          (wrap: X.6HS : 0x000b: CONSTRUCTOR  (r0v0 X.6HS A[REMOVE]) = (r2v0 'r2' X.5bL) call: X.6HS.<init>(X.5bL):void type: CONSTRUCTOR)
                          (wrap: ?? : ?: SGET   com.whatsapp.R.string.ok int)
                         type: STATIC call: X.5tm.A00(java.lang.Runnable, int):X.5tm in method: X.6DB.ANN(android.app.Activity):X.04S, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                        	... 19 more
                        Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x000b: CONSTRUCTOR  (r0v0 X.6HS A[REMOVE]) = (r2v0 'r2' X.5bL) call: X.6HS.<init>(X.5bL):void type: CONSTRUCTOR in method: X.6DB.ANN(android.app.Activity):X.04S, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                        	... 25 more
                        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6HS, state: NOT_LOADED
                        	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                        	... 31 more
                        */
                    /*
                        this = this;
                        X.5bL r2 = X.C118145bL.this
                        java.lang.String r7 = r9.A01
                        android.content.Context r3 = r2.A12
                        r1 = 2131890036(0x7f120f74, float:1.9414752E38)
                        X.6HS r0 = new X.6HS
                        r0.<init>(r2)
                        X.5tm r4 = X.C126995tm.A00(r0, r1)
                        r5 = 0
                        r8 = 0
                        r6 = r5
                        X.04S r0 = X.C130295zB.A00(r3, r4, r5, r6, r7, r8)
                        return r0
                    */
                    throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass6DB.ANN(android.app.Activity):X.04S");
                }
            }));
        }

        /* JADX WARNING: Removed duplicated region for block: B:15:0x0060  */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0068  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void A0D(X.C1315463e r16, int r17, boolean r18) {
            /*
                r15 = this;
                int r4 = r15.A05()
                r1 = 2
                r0 = r17
                r5 = r16
                if (r4 != r1) goto L_0x0032
                if (r16 != 0) goto L_0x0035
            L_0x000d:
                X.1It r3 = r15.A08
                r8 = 2131232095(0x7f08055f, float:1.808029E38)
                r9 = 2131100317(0x7f06029d, float:1.7813012E38)
                android.content.Context r2 = r15.A12
                r1 = 2131886243(0x7f1200a3, float:1.940706E38)
                java.lang.String r7 = r2.getString(r1)
                r11 = 2131100529(0x7f060371, float:1.7813442E38)
                android.content.Context r1 = r15.A12
                android.graphics.Typeface r5 = X.C27531Hw.A03(r1)
                r6 = 0
                r10 = 0
                X.5vZ r4 = new X.5vZ
                r4.<init>(r5, r6, r7, r8, r9, r10, r11)
                X.C117305Zk.A1A(r3, r4, r0)
                return
            L_0x0032:
                r1 = 1
                if (r4 != r1) goto L_0x000d
            L_0x0035:
                X.1It r3 = r15.A08
                android.content.Context r2 = r15.A12
                r1 = 2131889969(0x7f120f31, float:1.9414617E38)
                java.lang.String r10 = r2.getString(r1)
                r12 = 0
                if (r16 != 0) goto L_0x0077
                android.content.Context r2 = r15.A12
                r1 = 2131889751(0x7f120e57, float:1.9414174E38)
            L_0x0048:
                java.lang.String r9 = r2.getString(r1)
                r14 = 2131100529(0x7f060371, float:1.7813442E38)
                android.content.Context r1 = r15.A12
                android.graphics.Typeface r8 = X.C27531Hw.A03(r1)
            L_0x0055:
                X.5yo r1 = r15.A0c
                boolean r1 = r1.A05()
                r11 = 2131232522(0x7f08070a, float:1.8081156E38)
                if (r1 == 0) goto L_0x0063
                r11 = 2131232523(0x7f08070b, float:1.8081158E38)
            L_0x0063:
                r1 = 2
                r13 = 8
                if (r4 != r1) goto L_0x0069
                r13 = 0
            L_0x0069:
                X.5vZ r7 = new X.5vZ
                r7.<init>(r8, r9, r10, r11, r12, r13, r14)
                X.4OZ r1 = new X.4OZ
                r1.<init>(r0, r7)
                r3.A0A(r1)
                return
            L_0x0077:
                if (r18 == 0) goto L_0x00a0
                X.63m r1 = r5.A01
                X.AnonymousClass009.A05(r1)
                X.6F2 r9 = r1.A02
                X.1Yv r8 = r9.A00
                android.content.Context r7 = r15.A12
                android.content.Context r6 = r15.A12
                r5 = 2131889721(0x7f120e39, float:1.9414114E38)
                java.lang.Object[] r2 = X.C12970iu.A1b()
                X.018 r1 = r15.A0K
                java.lang.String r1 = r9.A06(r1)
                java.lang.String r1 = X.C12960it.A0X(r6, r1, r2, r12, r5)
                java.lang.CharSequence r9 = r8.AA7(r7, r1)
                r14 = 2131100528(0x7f060370, float:1.781344E38)
                r8 = 0
                goto L_0x0055
            L_0x00a0:
                android.content.Context r2 = r15.A12
                r1 = 2131889706(0x7f120e2a, float:1.9414083E38)
                goto L_0x0048
            */
            throw new UnsupportedOperationException("Method not decompiled: X.C118145bL.A0D(X.63e, int, boolean):void");
        }

        public boolean A0E(AbstractC28901Pl r5) {
            C27691It r1;
            String str;
            int A05 = A05();
            if (r5 == null) {
                if (A05 == 2) {
                    r1 = this.A09;
                    str = "servicesDialog";
                } else {
                    this.A0x.A0B(new C129525xr(new AnonymousClass6MC() { // from class: X.6DF
                        @Override // X.AnonymousClass6MC
                        public final DialogFragment ANO(Activity activity) {
                            C118145bL r3 = C118145bL.this;
                            NoviGetStartedFragment noviGetStartedFragment = new NoviGetStartedFragment();
                            noviGetStartedFragment.A01 = new C126055sG(r3);
                            PaymentBottomSheet paymentBottomSheet = new PaymentBottomSheet();
                            paymentBottomSheet.A01 = noviGetStartedFragment;
                            paymentBottomSheet.A1G(false);
                            paymentBottomSheet.A00 = new IDxDListenerShape14S0100000_3_I1(r3, 27);
                            return paymentBottomSheet;
                        }
                    }));
                    return true;
                }
            } else if (this.A0b.A0G()) {
                return false;
            } else {
                r1 = this.A09;
                str = "loginScreen";
            }
            C126025sD.A00(r1, str);
            return true;
        }
    }
