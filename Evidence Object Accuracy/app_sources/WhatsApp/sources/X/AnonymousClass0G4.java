package X;

/* renamed from: X.0G4  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0G4 extends AnonymousClass08s {
    public C03060Fy A00;

    public AnonymousClass0G4(C03060Fy r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass08s, X.AbstractC018608t
    public void AXq(AnonymousClass072 r3) {
        C03060Fy r1 = this.A00;
        int i = r1.A01 - 1;
        r1.A01 = i;
        if (i == 0) {
            r1.A04 = false;
            r1.A0D();
        }
        r3.A09(this);
    }

    @Override // X.AnonymousClass08s, X.AbstractC018608t
    public void AXt(AnonymousClass072 r3) {
        C03060Fy r1 = this.A00;
        if (!r1.A04) {
            r1.A0F();
            r1.A04 = true;
        }
    }
}
