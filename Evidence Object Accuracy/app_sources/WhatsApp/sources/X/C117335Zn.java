package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;
import com.whatsapp.payments.ui.widget.PaymentAmountInputField;

/* renamed from: X.5Zn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117335Zn extends AnimatorListenerAdapter {
    public final /* synthetic */ PaymentAmountInputField A00;

    public C117335Zn(PaymentAmountInputField paymentAmountInputField) {
        this.A00 = paymentAmountInputField;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
        View view = this.A00.A08;
        if (view != null) {
            view.setTranslationX(0.0f);
        }
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        View view = this.A00.A08;
        if (view != null) {
            view.setTranslationX(0.0f);
        }
    }
}
