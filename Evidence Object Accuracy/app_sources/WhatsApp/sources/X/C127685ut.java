package X;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/* renamed from: X.5ut  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C127685ut {
    public String A00;
    public byte[] A01;
    public final List A02;
    public final Set A03 = C12970iu.A12();

    public C127685ut() {
        byte[] bytes = "wa_ref_id_salt".getBytes();
        this.A01 = bytes;
        this.A02 = new LinkedList();
        byte[] A03 = AnonymousClass61L.A03(bytes);
        this.A01 = A03;
        StringBuilder A0h = C12960it.A0h();
        byte[] bArr = new byte[2];
        System.arraycopy(A03, 0, bArr, 0, 2);
        char[] cArr = new char[4];
        int i = 0;
        do {
            int i2 = bArr[i] & 255;
            int i3 = i << 1;
            char[] cArr2 = AnonymousClass61L.A00;
            cArr[i3] = cArr2[i2 >>> 4];
            cArr[i3 + 1] = cArr2[i2 & 15];
            i++;
        } while (i < 2);
        A0h.append(new String(cArr));
        this.A00 = C12960it.A0d("_", A0h);
    }
}
