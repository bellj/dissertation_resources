package X;

import com.google.android.exoplayer2.Timeline;

/* renamed from: X.3lu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C76603lu extends Timeline {
    public final AnonymousClass4XL A00;

    public C76603lu(AnonymousClass4XL r1) {
        this.A00 = r1;
    }

    @Override // com.google.android.exoplayer2.Timeline
    public C94404bl A0B(C94404bl r15, int i, long j) {
        r15.A00(null, this.A00, C94404bl.A0F, -9223372036854775807L, -9223372036854775807L, -9223372036854775807L, -9223372036854775807L, false, true);
        r15.A0C = true;
        return r15;
    }
}
