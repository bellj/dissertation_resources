package X;

/* renamed from: X.1Ne  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28431Ne {
    public final int A00;
    public final long A01;
    public final String A02;

    public C28431Ne(int i, String str, long j) {
        this.A02 = str;
        this.A00 = i;
        this.A01 = j;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof C28431Ne)) {
            return false;
        }
        C28431Ne r7 = (C28431Ne) obj;
        if (this.A02.equals(r7.A02) && this.A00 == r7.A00 && this.A01 == r7.A01) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = this.A00;
        int hashCode = ((this.A02.hashCode() * 31) + (i ^ (i >>> 32))) * 31;
        long j = this.A01;
        return hashCode + ((int) (j ^ (j >>> 32)));
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.A02);
        sb.append(":");
        sb.append(this.A00);
        sb.append(":");
        sb.append(this.A01);
        sb.append(";");
        return sb.toString();
    }
}
