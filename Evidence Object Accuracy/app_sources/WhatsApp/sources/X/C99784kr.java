package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4kr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C99784kr implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new AnonymousClass1ZO(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new AnonymousClass1ZO[i];
    }
}
