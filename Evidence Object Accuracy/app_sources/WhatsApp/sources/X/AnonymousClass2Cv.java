package X;

import com.whatsapp.jid.DeviceJid;

/* renamed from: X.2Cv  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Cv {
    public final byte A00;
    public final DeviceJid A01;
    public final C29211Rh A02;
    public final C29211Rh A03;
    public final byte[] A04;
    public final byte[] A05;
    public final byte[] A06;

    public AnonymousClass2Cv(DeviceJid deviceJid, C29211Rh r2, C29211Rh r3, byte[] bArr, byte[] bArr2, byte[] bArr3, byte b) {
        this.A01 = deviceJid;
        this.A05 = bArr;
        this.A06 = bArr2;
        this.A04 = bArr3;
        this.A00 = b;
        this.A02 = r2;
        this.A03 = r3;
    }
}
