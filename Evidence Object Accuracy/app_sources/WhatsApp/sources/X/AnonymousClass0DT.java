package X;

import android.os.Bundle;
import android.view.View;

/* renamed from: X.0DT  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0DT extends AnonymousClass04v {
    public final /* synthetic */ AnonymousClass09V A00;

    public AnonymousClass0DT(AnonymousClass09V r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass04v
    public boolean A03(View view, int i, Bundle bundle) {
        if (i == 1048576) {
            AnonymousClass09V r1 = this.A00;
            if (r1.A0B) {
                r1.A03(EnumC03710Iv.ACCESSIBILITY_ACTION);
                return true;
            }
        }
        return super.A03(view, i, bundle);
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r4) {
        boolean z;
        super.A06(view, r4);
        if (this.A00.A0B) {
            r4.A02.addAction(1048576);
            z = true;
        } else {
            z = false;
        }
        r4.A0K(z);
    }
}
