package X;

import android.text.TextUtils;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* renamed from: X.1X7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1X7 extends AnonymousClass1X8 implements AbstractC16400ox, AbstractC16420oz {
    public AnonymousClass1X7(C16150oX r9, AnonymousClass1IS r10, AnonymousClass1X7 r11, long j) {
        super(r9, r10, r11, (byte) 1, j, false);
    }

    public AnonymousClass1X7(C16150oX r9, AnonymousClass1IS r10, AnonymousClass1X7 r11, long j, boolean z) {
        super(r9, r10, r11, r11.A0y, j, z);
    }

    public AnonymousClass1X7(AnonymousClass1IS r1, byte b, long j) {
        super(r1, b, j);
    }

    public AnonymousClass1X7(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 1, j);
    }

    @Override // X.AbstractC16420oz
    public void A6k(C39971qq r11) {
        StringBuilder sb;
        AbstractC27091Fz r0;
        C28891Pk r02;
        AbstractC27091Fz r03;
        UserJid userJid;
        if (this instanceof AnonymousClass1XU) {
            AnonymousClass1XU r6 = (AnonymousClass1XU) this;
            AnonymousClass1G3 r5 = r11.A03;
            AnonymousClass2Lw r4 = (AnonymousClass2Lw) r5.A05().A0T();
            AnonymousClass2Ly r2 = r5.A05().A03;
            if (r2 == null) {
                r2 = AnonymousClass2Ly.A07;
            }
            if (r2.A01 == 3) {
                r0 = (AbstractC27091Fz) r2.A03;
            } else {
                r0 = C40821sO.A0R;
            }
            C81963ur A1C = r6.A1C((C81963ur) r0.A0T(), r11.A08, r11.A06);
            if (A1C == null || (r02 = r6.A00) == null) {
                sb = new StringBuilder("MessageTemplateImage/buildE2eMessage: cannot send encrypted media message, ");
                sb.append((int) r6.A0y);
            } else {
                AnonymousClass2Lx A00 = C63173Ao.A00(r5, r02);
                A00.A03();
                AnonymousClass2Ly r1 = (AnonymousClass2Ly) A00.A00;
                r1.A03 = A1C.A02();
                r1.A01 = 3;
                r4.A06(A00);
                r4.A05(A00);
                r5.A0B(r4);
                return;
            }
        } else if (this instanceof AnonymousClass1X6) {
            AnonymousClass1X6 r7 = (AnonymousClass1X6) this;
            if (r7.ACL() != null) {
                r7.ACL().A0A(r7, r11);
                AnonymousClass1G3 r42 = r11.A03;
                C57752nZ r04 = ((C27081Fy) r42.A00).A0K;
                if (r04 == null) {
                    r04 = C57752nZ.A07;
                }
                AnonymousClass1G4 A0T = r04.A0T();
                C57722nW r05 = ((C57752nZ) A0T.A00).A05;
                if (r05 == null) {
                    r05 = C57722nW.A06;
                }
                C82513vk r62 = (C82513vk) r05.A0T();
                r62.A05();
                C57722nW r12 = (C57722nW) r62.A00;
                if (r12.A01 == 4) {
                    r03 = (AbstractC27091Fz) r12.A02;
                } else {
                    r03 = C40821sO.A0R;
                }
                C81963ur A1C2 = r7.A1C((C81963ur) r03.A0T(), r11.A08, r11.A06);
                r62.A03();
                C57722nW r13 = (C57722nW) r62.A00;
                r13.A02 = A1C2.A02();
                r13.A01 = 4;
                A0T.A03();
                C57752nZ r14 = (C57752nZ) A0T.A00;
                r14.A05 = (C57722nW) r62.A02();
                r14.A00 |= 1;
                r42.A07((C57752nZ) A0T.A02());
                return;
            }
            return;
        } else if (!(this instanceof AnonymousClass1XJ)) {
            AnonymousClass1G3 r3 = r11.A03;
            C40821sO r06 = ((C27081Fy) r3.A00).A0J;
            if (r06 == null) {
                r06 = C40821sO.A0R;
            }
            C81963ur A1B = A1B((C81963ur) r06.A0T(), r11);
            if (A1B == null) {
                return;
            }
            if (!A0y() || A0F().A00 == null) {
                r3.A03();
                C27081Fy r15 = (C27081Fy) r3.A00;
                r15.A0J = (C40821sO) A1B.A02();
                r15.A00 |= 4;
                return;
            }
            C57552nF r07 = ((C27081Fy) r3.A00).A03;
            if (r07 == null) {
                r07 = C57552nF.A08;
            }
            C56852m3 r22 = (C56852m3) r07.A0T();
            AbstractC27091Fz A02 = A1B.A02();
            r22.A03();
            C57552nF r16 = (C57552nF) r22.A00;
            r16.A05 = A02;
            r16.A01 = 3;
            r22.A05(AnonymousClass39x.A03);
            C35011h5.A03(r22, A0F().A00);
            r3.A06((C57552nF) r22.A02());
            return;
        } else {
            AnonymousClass1XJ r72 = (AnonymousClass1XJ) this;
            AnonymousClass1G3 r23 = r11.A03;
            C57542nE r08 = ((C27081Fy) r23.A00).A0V;
            if (r08 == null) {
                r08 = C57542nE.A07;
            }
            AnonymousClass1G4 A0T2 = r08.A0T();
            C57542nE r09 = ((C27081Fy) r23.A00).A0V;
            if (r09 == null) {
                r09 = C57542nE.A07;
            }
            C57382mw r010 = r09.A02;
            if (r010 == null) {
                r010 = C57382mw.A04;
            }
            AnonymousClass1G4 A0T3 = r010.A0T();
            C40821sO r011 = ((C57382mw) A0T3.A00).A01;
            if (r011 == null) {
                r011 = C40821sO.A0R;
            }
            boolean z = r11.A08;
            boolean z2 = r11.A06;
            C81963ur A1C3 = r72.A1C((C81963ur) r011.A0T(), z, z2);
            if (A1C3 == null || (userJid = r72.A00) == null) {
                sb = new StringBuilder("FMessageCatalog/buildE2eMessage/unable to send encrypted media message due to missing mediaKey or businessOwnerJid; message.key=");
                sb.append(r72.A0z);
                sb.append("; media_wa_type=");
                sb.append((int) r72.A0y);
                sb.append("; business_owner_jid=");
                sb.append(r72.A00);
            } else {
                String rawString = userJid.getRawString();
                A0T2.A03();
                C57542nE r17 = (C57542nE) A0T2.A00;
                r17.A00 |= 2;
                r17.A05 = rawString;
                if (!TextUtils.isEmpty(r72.A01)) {
                    String str = r72.A01;
                    A0T3.A03();
                    C57382mw r18 = (C57382mw) A0T3.A00;
                    r18.A00 |= 4;
                    r18.A02 = str;
                }
                if (!TextUtils.isEmpty(r72.A02)) {
                    String str2 = r72.A02;
                    A0T3.A03();
                    C57382mw r19 = (C57382mw) A0T3.A00;
                    r19.A00 |= 2;
                    r19.A03 = str2;
                }
                A0T3.A03();
                C57382mw r110 = (C57382mw) A0T3.A00;
                r110.A01 = (C40821sO) A1C3.A02();
                r110.A00 |= 1;
                A0T2.A03();
                C57542nE r111 = (C57542nE) A0T2.A00;
                r111.A02 = (C57382mw) A0T3.A02();
                r111.A00 |= 4;
                AnonymousClass1PG r63 = r11.A04;
                byte[] bArr = r11.A09;
                if (C32411c7.A0U(r63, r72, bArr)) {
                    C43261wh A0P = C32411c7.A0P(r11.A00, r11.A02, r63, r72, bArr, z2);
                    A0T2.A03();
                    C57542nE r112 = (C57542nE) A0T2.A00;
                    r112.A01 = A0P;
                    r112.A00 |= 32;
                }
                r23.A03();
                C27081Fy r24 = (C27081Fy) r23.A00;
                r24.A0V = (C57542nE) A0T2.A02();
                r24.A00 |= EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING;
                return;
            }
        }
        Log.w(sb.toString());
    }

    @Override // X.AbstractC16400ox
    public /* bridge */ /* synthetic */ AbstractC15340mz A7M(AnonymousClass1IS r8) {
        if (this instanceof AnonymousClass1XV) {
            AnonymousClass1XV r3 = (AnonymousClass1XV) this;
            return new AnonymousClass1XV(((AbstractC16130oV) r3).A02, r8, r3, r3.A0I, true);
        } else if (this instanceof AnonymousClass1X6) {
            AnonymousClass1X6 r32 = (AnonymousClass1X6) this;
            return new AnonymousClass1X6(((AbstractC16130oV) r32).A02, r8, r32, r32.A0I, true);
        } else if (this instanceof AnonymousClass1XU) {
            AnonymousClass1XU r33 = (AnonymousClass1XU) this;
            return new AnonymousClass1XU(((AbstractC16130oV) r33).A02, r8, r33, r33.A0I);
        } else if (!(this instanceof AnonymousClass1XJ)) {
            return new AnonymousClass1X7(((AbstractC16130oV) this).A02, r8, this, this.A0I, true);
        } else {
            AnonymousClass1XJ r34 = (AnonymousClass1XJ) this;
            return new AnonymousClass1XJ(((AbstractC16130oV) r34).A02, r8, r34, r34.A0I, true);
        }
    }
}
