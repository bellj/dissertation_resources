package X;

import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0n1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15360n1 extends AnonymousClass015 {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public long A04 = 1;
    public long A05 = Long.MIN_VALUE;
    public C91294Re A06;
    public AbstractC15340mz A07;
    public String A08;
    public String A09;
    public ArrayList A0A;
    public boolean A0B = true;
    public final AnonymousClass016 A0C = new AnonymousClass016();
    public final AnonymousClass016 A0D = new C27691It();
    public final AnonymousClass016 A0E = new C27691It();
    public final AnonymousClass016 A0F;
    public final AnonymousClass07E A0G;
    public final C15570nT A0H;
    public final AnonymousClass1AP A0I;
    public final C21830y3 A0J;
    public final C14830m7 A0K;
    public final AnonymousClass018 A0L;
    public final C241814n A0M;
    public final C19990v2 A0N;
    public final C15650ng A0O;
    public final AnonymousClass134 A0P;
    public final C14850m9 A0Q;
    public final AbstractC14640lm A0R;
    public final AnonymousClass15O A0S;
    public final C20320vZ A0T;
    public final C22190yg A0U;
    public final C27691It A0V = new C27691It();
    public final C27691It A0W = new C27691It();
    public final C27691It A0X = new C27691It();
    public final C27691It A0Y = new C27691It();
    public final AbstractC14440lR A0Z;
    public final AbstractC35401hl A0a;
    public final ArrayList A0b = new ArrayList();
    public final Set A0c = new HashSet();

    public C15360n1(AnonymousClass07E r9, C15570nT r10, AnonymousClass1AP r11, C21830y3 r12, C14830m7 r13, AnonymousClass018 r14, C241814n r15, C19990v2 r16, C15650ng r17, AnonymousClass134 r18, C14850m9 r19, AbstractC14640lm r20, AnonymousClass15O r21, C20320vZ r22, C22190yg r23, AbstractC14440lR r24, AbstractC35401hl r25) {
        AnonymousClass016 r3 = new AnonymousClass016();
        this.A0F = r3;
        this.A0Q = r19;
        this.A0P = r18;
        this.A0O = r17;
        this.A0Z = r24;
        this.A0N = r16;
        this.A0L = r14;
        this.A0M = r15;
        this.A0H = r10;
        this.A0a = r25;
        this.A0I = r11;
        this.A0U = r23;
        this.A0T = r22;
        this.A0S = r21;
        this.A0K = r13;
        this.A0J = r12;
        this.A0G = r9;
        this.A0R = r20;
        Map map = r9.A02;
        Number number = (Number) map.get("start_ref");
        if (number != null) {
            this.A04 = number.longValue();
        }
        Number number2 = (Number) map.get("start_sort_ref");
        if (number2 != null) {
            this.A05 = number2.longValue();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("quotedMessage_");
        sb.append("fMessageKeyJid");
        String obj = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("quotedMessage_");
        sb2.append("fMessageKeyFromMe");
        String obj2 = sb2.toString();
        StringBuilder sb3 = new StringBuilder();
        sb3.append("quotedMessage_");
        sb3.append("fMessageKeyId");
        String obj3 = sb3.toString();
        if (map.containsKey(obj) && map.containsKey(obj2) && map.containsKey(obj3)) {
            AbstractC15340mz A03 = this.A0O.A0K.A03(new AnonymousClass1IS(AbstractC14640lm.A01((String) map.get(obj)), (String) map.get(obj3), Boolean.TRUE.equals(map.get(obj2))));
            this.A07 = A03;
            if (A03 != null) {
                r3.A0A(A03);
                r11.A00.put(r20, this.A07);
            }
        }
    }

    public final C30031Vu A04(int i, long j) {
        C30031Vu A0A = this.A0O.A0A(this.A0R, i, this.A04, j, true);
        A0A.A00.getCount();
        return A0A;
    }

    public final void A05() {
        this.A0Y.A0B(new AnonymousClass4X8(this.A01, this.A02, this.A03));
    }

    public final void A06() {
        C47942Dj r1;
        ArrayList arrayList = this.A0b;
        if (arrayList.isEmpty()) {
            r1 = new C47942Dj(8, null);
        } else {
            r1 = new C47942Dj(0, this.A0L.A0J().format((long) arrayList.size()));
        }
        this.A0C.A0A(r1);
    }

    public void A07(int i, long j) {
        StringBuilder sb = new StringBuilder("conversation/recreatemessagelist/");
        sb.append(this.A04);
        sb.append(" ");
        sb.append(this.A0B);
        Log.i(sb.toString());
        C30031Vu A04 = A04(i, j);
        A09(A04.A01);
        A0A(A04.A02);
        if (this.A03 > A04.A00.getCount()) {
            this.A03 = 0;
            this.A01 = 0;
            this.A02 = 0;
        }
        this.A0E.A0B(new AnonymousClass4XH(new AnonymousClass4X8(this.A01, this.A02, this.A03), A04, this.A00, this.A0B));
    }

    public void A08(long j) {
        StringBuilder sb = new StringBuilder();
        AbstractC14640lm r6 = this.A0R;
        sb.append(r6.getRawString());
        sb.append("_");
        sb.append(100);
        sb.append("_");
        sb.append(j);
        String obj = sb.toString();
        Set set = this.A0c;
        synchronized (set) {
            if (this.A0B && set.add(obj)) {
                AbstractC14440lR r0 = this.A0Z;
                C15650ng r4 = this.A0O;
                long j2 = this.A04;
                r0.Ab6(new RunnableC55802jM(new AnonymousClass4KT(this), this.A0N, r4, this.A0P, r6, obj, set, j, j2));
            }
        }
    }

    public void A09(long j) {
        this.A04 = j;
        this.A0G.A04("start_ref", Long.valueOf(j));
    }

    public void A0A(long j) {
        this.A05 = j;
        this.A0G.A04("start_sort_ref", Long.valueOf(j));
    }

    public void A0B(AbstractC15340mz r9) {
        if (!C29941Vi.A00(this.A07, r9)) {
            this.A07 = r9;
            HashMap hashMap = this.A0I.A00;
            AnonymousClass07E r5 = this.A0G;
            if (r9 != null) {
                AnonymousClass1IS r7 = r9.A0z;
                StringBuilder sb = new StringBuilder();
                sb.append("quotedMessage_");
                sb.append("fMessageKeyJid");
                String obj = sb.toString();
                StringBuilder sb2 = new StringBuilder();
                sb2.append("quotedMessage_");
                sb2.append("fMessageKeyFromMe");
                String obj2 = sb2.toString();
                StringBuilder sb3 = new StringBuilder();
                sb3.append("quotedMessage_");
                sb3.append("fMessageKeyId");
                r5.A04(sb3.toString(), r7.A01);
                r5.A04(obj2, Boolean.valueOf(r7.A02));
                r5.A04(obj, C15380n4.A03(r7.A00));
                hashMap.put(this.A0R, r9);
            } else {
                StringBuilder sb4 = new StringBuilder();
                sb4.append("quotedMessage_");
                sb4.append("fMessageKeyJid");
                String obj3 = sb4.toString();
                StringBuilder sb5 = new StringBuilder();
                sb5.append("quotedMessage_");
                sb5.append("fMessageKeyFromMe");
                String obj4 = sb5.toString();
                StringBuilder sb6 = new StringBuilder();
                sb6.append("quotedMessage_");
                sb6.append("fMessageKeyId");
                r5.A03(sb6.toString());
                r5.A03(obj4);
                r5.A03(obj3);
                hashMap.remove(this.A0R);
            }
            this.A0F.A0B(r9);
        }
    }

    public void A0C(AbstractC15340mz r26, List list, int i, int i2, int i3, int i4, long j, boolean z) {
        boolean z2;
        C30031Vu r8;
        if (r26.A12 < this.A05) {
            z2 = true;
            r8 = this.A0O.A09(this.A0R, 100, r26.A11, j);
            r8.A00.getCount();
            A09(r8.A01);
            A0A(r8.A02);
        } else {
            z2 = false;
            r8 = null;
        }
        C27691It r6 = this.A0X;
        int A02 = this.A0P.A02(this.A0R, this.A05, r26.A12);
        if (this.A03 > 0 && A02 >= i2) {
            A02++;
        }
        r6.A0B(new C64203Eq(r8, r26, list, A02, i, i3, i4, j, z2, z));
    }

    public final boolean A0D(C15350n0 r10, Collection collection) {
        int A01;
        AbstractC15340mz A05;
        if (this.A03 > 0 && r10.getCount() > (A01 = r10.A01() + 1) && (A05 = r10.getItem(A01)) != null) {
            Iterator it = collection.iterator();
            while (it.hasNext()) {
                AbstractC15340mz r2 = (AbstractC15340mz) it.next();
                if (C29941Vi.A00(r2.A0z.A00, this.A0R) && r2.A12 >= A05.A12) {
                    return true;
                }
            }
        }
        return false;
    }
}
