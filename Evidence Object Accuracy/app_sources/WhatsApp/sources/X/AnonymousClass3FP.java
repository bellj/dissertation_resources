package X;

import android.content.Context;
import androidx.fragment.app.DialogFragment;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;

/* renamed from: X.3FP  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3FP {
    public final Context A00;
    public final AbstractC13860kS A01;
    public final C14900mE A02;
    public final C17070qD A03;
    public final C74503iB A04;
    public final Runnable A05;
    public final Runnable A06;
    public final boolean A07;

    public AnonymousClass3FP(Context context, AbstractC13860kS r2, C14900mE r3, C17070qD r4, C74503iB r5, Runnable runnable, Runnable runnable2, boolean z) {
        this.A02 = r3;
        this.A03 = r4;
        this.A00 = context;
        this.A04 = r5;
        this.A01 = r2;
        this.A06 = runnable;
        this.A05 = runnable2;
        this.A07 = z;
    }

    public final void A00(UserJid userJid, AbstractC38141na r5, String str, boolean z) {
        ArrayList A0l = C12960it.A0l();
        A0l.add(userJid);
        C74503iB r1 = this.A04;
        r1.A04(0);
        DialogFragment AFK = r5.AFK(str, A0l, z, this.A07);
        this.A01.Adm(AFK);
        r1.A00.A05(AFK, new AnonymousClass3RL(AFK, this));
    }

    public void A01(UserJid userJid, AnonymousClass5WB r11, String str) {
        AnonymousClass009.A0E(A02());
        C17070qD r1 = this.A03;
        AbstractC38141na AFL = r1.A02().AFL();
        AnonymousClass009.A05(AFL);
        r1.A03();
        C38051nR r0 = r1.A00;
        AnonymousClass009.A05(r0);
        C91674Sq r3 = new C91674Sq(userJid, AFL, r11, this, str);
        C12960it.A1E(new AnonymousClass37G(r0.A01, userJid, r3), r0.A03);
    }

    public boolean A02() {
        AbstractC38141na AFL = this.A03.A02().AFL();
        if (AFL == null) {
            return false;
        }
        return AFL.A6y();
    }
}
