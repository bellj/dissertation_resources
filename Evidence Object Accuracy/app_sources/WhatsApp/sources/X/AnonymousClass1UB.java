package X;

import android.content.Context;
import com.whatsapp.util.Log;

/* renamed from: X.1UB  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1UB {
    public static int A00(Context context) {
        try {
            return C471729i.A00.A00(context, 12451000);
        } catch (RuntimeException e) {
            StringBuilder sb = new StringBuilder("google-utils/checkGooglePlayServicesStatus/unexpected exception/");
            sb.append(e);
            Log.e(sb.toString());
            return 8;
        }
    }
}
