package X;

import android.database.DataSetObserver;

/* renamed from: X.3fU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C72983fU extends DataSetObserver {
    public final /* synthetic */ C55352iG A00;

    public C72983fU(C55352iG r1) {
        this.A00 = r1;
    }

    @Override // android.database.DataSetObserver
    public void onChanged() {
        this.A00.A06();
    }
}
