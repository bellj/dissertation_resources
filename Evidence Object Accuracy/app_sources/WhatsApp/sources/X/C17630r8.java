package X;

import java.util.Collections;
import java.util.Set;

/* renamed from: X.0r8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17630r8 {
    public static final Set A00(Object obj) {
        Set singleton = Collections.singleton(obj);
        C16700pc.A0B(singleton);
        return singleton;
    }
}
