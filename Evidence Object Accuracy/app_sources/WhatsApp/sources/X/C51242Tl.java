package X;

import android.graphics.Point;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import java.io.File;

/* renamed from: X.2Tl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C51242Tl implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C99934l6();
    public final C39341ph A00;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C51242Tl(C39341ph r1) {
        this.A00 = r1;
    }

    public /* synthetic */ C51242Tl(Parcel parcel) {
        Byte b;
        File file;
        Parcelable readParcelable = parcel.readParcelable(Uri.class.getClassLoader());
        AnonymousClass009.A05(readParcelable);
        C39341ph r2 = new C39341ph((Uri) readParcelable);
        this.A00 = r2;
        if (parcel.readByte() == 1) {
            b = Byte.valueOf(parcel.readByte());
        } else {
            b = null;
        }
        r2.A0C(b);
        String readString = parcel.readString();
        File file2 = null;
        if (readString == null) {
            file = null;
        } else {
            file = new File(readString);
        }
        r2.A0B(file);
        r2.A0D(parcel.readString());
        r2.A0E(parcel.readString());
        String readString2 = parcel.readString();
        synchronized (r2) {
            r2.A0A = readString2;
        }
        String readString3 = parcel.readString();
        synchronized (r2) {
            r2.A0B = readString3;
        }
        int readInt = parcel.readInt();
        synchronized (r2) {
            r2.A01 = readInt;
        }
        String readString4 = parcel.readString();
        file2 = readString4 != null ? new File(readString4) : file2;
        synchronized (r2) {
            r2.A06 = file2;
        }
        Rect rect = (Rect) parcel.readParcelable(Rect.class.getClassLoader());
        synchronized (r2) {
            r2.A04 = rect;
        }
        boolean z = parcel.readByte() != 1 ? false : true;
        synchronized (r2) {
            r2.A0F = z;
        }
        Point point = (Point) parcel.readParcelable(Point.class.getClassLoader());
        synchronized (r2) {
            r2.A03 = point;
        }
        r2.A0A(parcel.readInt());
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        String absolutePath;
        String str;
        Rect rect;
        C39341ph r1 = this.A00;
        parcel.writeParcelable(r1.A0G, i);
        Byte A06 = r1.A06();
        byte b = 0;
        if (A06 != null) {
            parcel.writeByte((byte) 1);
            b = A06.byteValue();
        }
        parcel.writeByte(b);
        String str2 = null;
        if (r1.A05() == null) {
            absolutePath = null;
        } else {
            absolutePath = r1.A05().getAbsolutePath();
        }
        parcel.writeString(absolutePath);
        parcel.writeString(r1.A07());
        parcel.writeString(r1.A09());
        parcel.writeString(r1.A08());
        synchronized (r1) {
            str = r1.A0B;
        }
        parcel.writeString(str);
        parcel.writeInt(r1.A01());
        if (r1.A03() != null) {
            str2 = r1.A03().getAbsolutePath();
        }
        parcel.writeString(str2);
        synchronized (r1) {
            rect = r1.A04;
        }
        parcel.writeParcelable(rect, i);
        parcel.writeByte(r1.A0F() ? (byte) 1 : 0);
        parcel.writeParcelable(r1.A02(), i);
        parcel.writeInt(r1.A00());
    }
}
