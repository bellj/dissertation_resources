package X;

import android.os.Build;

/* renamed from: X.0Fh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02900Fh extends AnonymousClass0OL {
    public C02900Fh() {
        super(3, 4);
    }

    @Override // X.AnonymousClass0OL
    public void A00(AbstractC12920im r3) {
        if (Build.VERSION.SDK_INT >= 23) {
            ((AnonymousClass0ZE) r3).A00.execSQL("UPDATE workspec SET schedule_requested_at=0 WHERE state NOT IN (2, 3, 5) AND schedule_requested_at=-1 AND interval_duration<>0");
        }
    }
}
