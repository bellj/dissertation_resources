package X;

import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.google.android.gms.wearable.internal.DataItemAssetParcelable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.3ow  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78453ow extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C99254k0();
    public byte[] A00;
    public final Uri A01;
    public final Map A02;

    public C78453ow(Uri uri, Bundle bundle, byte[] bArr) {
        this.A01 = uri;
        HashMap A11 = C12970iu.A11();
        bundle.setClassLoader(DataItemAssetParcelable.class.getClassLoader());
        Iterator<String> it = bundle.keySet().iterator();
        while (it.hasNext()) {
            String A0x = C12970iu.A0x(it);
            A11.put(A0x, bundle.getParcelable(A0x));
        }
        this.A02 = A11;
        this.A00 = bArr;
    }

    @Override // java.lang.Object
    public final String toString() {
        Object valueOf;
        String str;
        boolean isLoggable = Log.isLoggable("DataItem", 3);
        StringBuilder A0k = C12960it.A0k("DataItemParcelable[");
        A0k.append("@");
        A0k.append(Integer.toHexString(hashCode()));
        byte[] bArr = this.A00;
        if (bArr == null) {
            valueOf = "null";
        } else {
            valueOf = Integer.valueOf(bArr.length);
        }
        String valueOf2 = String.valueOf(valueOf);
        StringBuilder A0t = C12980iv.A0t(valueOf2.length() + 8);
        A0t.append(",dataSz=");
        A0k.append(C12960it.A0d(valueOf2, A0t));
        Map map = this.A02;
        A0k.append(C12960it.A0e(", numAssets=", C12980iv.A0t(23), map.size()));
        String valueOf3 = String.valueOf(this.A01);
        StringBuilder A0t2 = C12980iv.A0t(valueOf3.length() + 6);
        A0t2.append(", uri=");
        A0k.append(C12960it.A0d(valueOf3, A0t2));
        if (!isLoggable) {
            str = "]";
        } else {
            A0k.append("]\n  assets: ");
            Iterator A10 = C72453ed.A10(map);
            while (A10.hasNext()) {
                String A0x = C12970iu.A0x(A10);
                String valueOf4 = String.valueOf(map.get(A0x));
                StringBuilder A0t3 = C12980iv.A0t(C12970iu.A07(A0x) + 7 + valueOf4.length());
                A0t3.append("\n    ");
                C12990iw.A1T(A0t3, A0x);
                A0k.append(C12960it.A0d(valueOf4, A0t3));
            }
            str = "\n  ]";
        }
        return C12960it.A0d(str, A0k);
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A0B(parcel, this.A01, 2, i, false);
        Bundle A0D = C12970iu.A0D();
        A0D.setClassLoader(DataItemAssetParcelable.class.getClassLoader());
        Iterator A0n = C12960it.A0n(this.A02);
        while (A0n.hasNext()) {
            Map.Entry A15 = C12970iu.A15(A0n);
            A0D.putParcelable((String) A15.getKey(), new DataItemAssetParcelable((AnonymousClass5R2) A15.getValue()));
        }
        C95654e8.A03(A0D, parcel, 4);
        C95654e8.A0G(parcel, this.A00, 5, false);
        C95654e8.A06(parcel, A00);
    }
}
