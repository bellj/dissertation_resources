package X;

/* renamed from: X.1Xz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C30581Xz extends AnonymousClass1XB {
    public boolean A00;

    public C30581Xz(AnonymousClass1IS r2, long j) {
        super(r2, 58, j);
    }

    @Override // X.AbstractC15340mz
    public synchronized void A0l(String str) {
        this.A00 = Boolean.valueOf(str).booleanValue();
    }
}
