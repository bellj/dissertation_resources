package X;

import android.net.Uri;
import com.facebook.redex.RunnableBRunnable0Shape14S0100000_I1;
import com.google.android.exoplayer2.Timeline;
import java.util.concurrent.ExecutorService;

/* renamed from: X.2kG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56062kG extends AbstractC67703Sn implements AnonymousClass5SR {
    public long A00 = -9223372036854775807L;
    public AnonymousClass5QP A01;
    public boolean A02;
    public boolean A03 = true;
    public boolean A04;
    public final int A05 = 1048576;
    public final AnonymousClass4XB A06;
    public final AnonymousClass4XL A07;
    public final AbstractC116865Xf A08;
    public final AnonymousClass5SK A09;
    public final AbstractC47452At A0A;
    public final AnonymousClass5QO A0B;

    @Override // X.AnonymousClass2CD
    public void ALT() {
    }

    public C56062kG(AnonymousClass4XL r3, AbstractC116865Xf r4, AnonymousClass5SK r5, AbstractC47452At r6, AnonymousClass5QO r7) {
        this.A06 = r3.A02;
        this.A07 = r3;
        this.A0A = r6;
        this.A09 = r5;
        this.A08 = r4;
        this.A0B = r7;
    }

    @Override // X.AbstractC67703Sn
    public void A00() {
    }

    @Override // X.AbstractC67703Sn
    public void A02(AnonymousClass5QP r1) {
        this.A01 = r1;
        A03();
    }

    public final void A03() {
        Timeline r1 = new C76633lx(this.A07, this.A00, this.A04, this.A02);
        if (this.A03) {
            r1 = new C77173mr(r1, this);
        }
        A01(r1);
    }

    @Override // X.AnonymousClass2CD
    public AbstractC14080kp A8S(C28741Ov r13, AnonymousClass5VZ r14, long j) {
        AnonymousClass2BW A8D = this.A0A.A8D();
        AnonymousClass5QP r0 = this.A01;
        if (r0 != null) {
            A8D.A5p(r0);
        }
        Uri uri = this.A06.A00;
        AnonymousClass5SK r5 = this.A09;
        AbstractC116865Xf r4 = this.A08;
        return new C14060kn(uri, new AnonymousClass4P0(r13, super.A02.A02, 0), r4, r5, new AnonymousClass1Or(r13, super.A03.A02, 0), this, r14, A8D, this.A0B, this.A05);
    }

    @Override // X.AnonymousClass2CD
    public AnonymousClass4XL AED() {
        return this.A07;
    }

    @Override // X.AnonymousClass5SR
    public void AWB(long j, boolean z, boolean z2) {
        if (j == -9223372036854775807L) {
            j = this.A00;
        }
        if (this.A03 || this.A00 != j || this.A04 != z || this.A02 != z2) {
            this.A00 = j;
            this.A04 = z;
            this.A02 = z2;
            this.A03 = false;
            A03();
        }
    }

    @Override // X.AnonymousClass2CD
    public void Aa9(AbstractC14080kp r6) {
        C14060kn r62 = (C14060kn) r6;
        if (r62.A0G) {
            C106994wc[] r4 = r62.A0L;
            for (C106994wc r1 : r4) {
                r1.A02();
                if (r1.A0C != null) {
                    r1.A0C = null;
                    r1.A08 = null;
                }
            }
        }
        C93584aP r2 = r62.A0X;
        HandlerC52112aG r12 = r2.A00;
        if (r12 != null) {
            r12.A00(true);
        }
        ExecutorService executorService = r2.A02;
        executorService.execute(new RunnableBRunnable0Shape14S0100000_I1(r62, 4));
        executorService.shutdown();
        r62.A0O.removeCallbacksAndMessages(null);
        r62.A09 = null;
        r62.A0H = true;
    }
}
