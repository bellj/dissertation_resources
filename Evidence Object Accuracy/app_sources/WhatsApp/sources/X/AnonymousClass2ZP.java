package X;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

/* renamed from: X.2ZP  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2ZP extends Drawable {
    public final AbstractC454821u A00;

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -3;
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
    }

    public AnonymousClass2ZP(AbstractC454821u r1) {
        this.A00 = r1;
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        Rect bounds = getBounds();
        AbstractC454821u r3 = this.A00;
        float A02 = r3.A02() / 2.0f;
        r3.A0Q(C12980iv.A0K(), ((float) bounds.left) + A02, ((float) bounds.top) + A02, ((float) bounds.right) - A02, ((float) bounds.bottom) - A02);
        r3.A0P(canvas);
    }
}
