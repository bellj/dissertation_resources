package X;

import android.view.View;
import android.view.animation.Animation;
import com.whatsapp.voipcalling.VoipActivityV2;

/* renamed from: X.3xQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83553xQ extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ View A00;
    public final /* synthetic */ View A01;
    public final /* synthetic */ VoipActivityV2 A02;

    public C83553xQ(View view, View view2, VoipActivityV2 voipActivityV2) {
        this.A02 = voipActivityV2;
        this.A00 = view;
        this.A01 = view2;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        VoipActivityV2 voipActivityV2 = this.A02;
        voipActivityV2.hideView(this.A01);
        voipActivityV2.A0W.setVisibility(0);
        voipActivityV2.showView(this.A00);
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationStart(Animation animation) {
        VoipActivityV2 voipActivityV2 = this.A02;
        voipActivityV2.A0W.setVisibility(4);
        voipActivityV2.hideView(this.A00);
    }
}
