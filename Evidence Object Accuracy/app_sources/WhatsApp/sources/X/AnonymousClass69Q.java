package X;

import com.whatsapp.payments.ui.IndiaUpiMandateHistoryActivity;

/* renamed from: X.69Q  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass69Q implements AbstractC35651iS {
    public final /* synthetic */ IndiaUpiMandateHistoryActivity A00;

    @Override // X.AbstractC35651iS
    public void ATe(AnonymousClass1IR r1) {
    }

    public AnonymousClass69Q(IndiaUpiMandateHistoryActivity indiaUpiMandateHistoryActivity) {
        this.A00 = indiaUpiMandateHistoryActivity;
    }

    @Override // X.AbstractC35651iS
    public void ATf(AnonymousClass1IR r4) {
        IndiaUpiMandateHistoryActivity indiaUpiMandateHistoryActivity = this.A00;
        indiaUpiMandateHistoryActivity.A06.A04("payment transaction updated");
        C127085tv r1 = new C127085tv(1);
        r1.A01 = r4;
        indiaUpiMandateHistoryActivity.A03.A04(r1);
    }
}
