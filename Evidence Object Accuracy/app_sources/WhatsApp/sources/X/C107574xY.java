package X;

import android.util.SparseArray;
import com.google.android.exoplayer2.source.dash.DashMediaSource$Factory;
import com.google.android.exoplayer2.source.hls.HlsMediaSource$Factory;
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource$Factory;

/* renamed from: X.4xY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107574xY implements AnonymousClass5QA {
    public final SparseArray A00;
    public final AbstractC47452At A01;
    public final int[] A02;

    public C107574xY(AnonymousClass5SK r8, AbstractC47452At r9) {
        this.A01 = r9;
        SparseArray sparseArray = new SparseArray();
        try {
            sparseArray.put(0, (AnonymousClass5QA) DashMediaSource$Factory.class.asSubclass(AnonymousClass5QA.class).getConstructor(AbstractC47452At.class).newInstance(r9));
        } catch (Exception unused) {
        }
        try {
            sparseArray.put(1, (AnonymousClass5QA) SsMediaSource$Factory.class.asSubclass(AnonymousClass5QA.class).getConstructor(AbstractC47452At.class).newInstance(r9));
        } catch (Exception unused2) {
        }
        try {
            sparseArray.put(2, (AnonymousClass5QA) HlsMediaSource$Factory.class.asSubclass(AnonymousClass5QA.class).getConstructor(AbstractC47452At.class).newInstance(r9));
        } catch (Exception unused3) {
        }
        sparseArray.put(3, new C107564xX(r8, r9));
        this.A00 = sparseArray;
        this.A02 = new int[sparseArray.size()];
        int i = 0;
        while (true) {
            SparseArray sparseArray2 = this.A00;
            if (i < sparseArray2.size()) {
                this.A02[i] = sparseArray2.keyAt(i);
                i++;
            } else {
                return;
            }
        }
    }
}
