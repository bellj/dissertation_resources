package X;

import android.os.Build;
import androidx.window.extensions.layout.WindowLayoutComponent;
import java.lang.reflect.Method;

/* renamed from: X.0UQ  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0UQ {
    public static final AnonymousClass0UQ A00 = new AnonymousClass0UQ();
    public static final AbstractC16710pd A01 = AnonymousClass4Yq.A00(new C11000fa());

    public static final WindowLayoutComponent A00() {
        return (WindowLayoutComponent) A01.getValue();
    }

    public static final /* synthetic */ boolean A01(AnonymousClass0UQ r3, ClassLoader classLoader) {
        if (Build.VERSION.SDK_INT < 24 || !r3.A07(classLoader) || !r3.A05(classLoader) || !r3.A06(classLoader) || !r3.A04(classLoader)) {
            return false;
        }
        return true;
    }

    public static final boolean A03(AnonymousClass1WK r2) {
        try {
            return ((Boolean) r2.AJ3()).booleanValue();
        } catch (ClassNotFoundException | NoSuchMethodException unused) {
            return false;
        }
    }

    public final boolean A04(ClassLoader classLoader) {
        return A03(new C11020fc(classLoader));
    }

    public final boolean A05(ClassLoader classLoader) {
        return A03(new C11030fd(classLoader));
    }

    public final boolean A06(ClassLoader classLoader) {
        return A03(new C11040fe(classLoader));
    }

    public final boolean A07(ClassLoader classLoader) {
        return A03(new C11050ff(classLoader));
    }

    public final boolean A08(Method method, C71583dA r4) {
        return method.getReturnType().equals(AnonymousClass0LY.A00(r4));
    }
}
