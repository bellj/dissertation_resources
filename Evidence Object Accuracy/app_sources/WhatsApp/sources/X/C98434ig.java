package X;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

/* renamed from: X.4ig  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98434ig implements IInterface, AnonymousClass5Y3 {
    public final IBinder A00;
    public final String A01 = "com.google.android.gms.ads.identifier.internal.IAdvertisingIdService";

    @Override // android.os.IInterface
    public IBinder asBinder() {
        return this.A00;
    }

    public C98434ig(IBinder iBinder) {
        this.A00 = iBinder;
    }

    public final Parcel A00(int i, Parcel parcel) {
        try {
            parcel = Parcel.obtain();
            C12990iw.A18(this.A00, parcel, parcel, i);
            return parcel;
        } catch (RuntimeException e) {
            throw e;
        } finally {
            parcel.recycle();
        }
    }
}
