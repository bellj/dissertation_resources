package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;
import java.util.UUID;

/* renamed from: X.4m8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C100574m8 implements Parcelable {
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(2);
    public int A00;
    public final String A01;
    public final String A02;
    public final UUID A03;
    public final byte[] A04;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C100574m8(Parcel parcel) {
        this.A03 = new UUID(parcel.readLong(), parcel.readLong());
        this.A01 = parcel.readString();
        this.A02 = parcel.readString();
        this.A04 = parcel.createByteArray();
    }

    public C100574m8(String str, UUID uuid, byte[] bArr) {
        this.A03 = uuid;
        this.A01 = null;
        this.A02 = str;
        this.A04 = bArr;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (!(obj instanceof C100574m8)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        C100574m8 r4 = (C100574m8) obj;
        if (!AnonymousClass3JZ.A0H(this.A01, r4.A01) || !AnonymousClass3JZ.A0H(this.A02, r4.A02) || !AnonymousClass3JZ.A0H(this.A03, r4.A03) || !Arrays.equals(this.A04, r4.A04)) {
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        int i = this.A00;
        if (i != 0) {
            return i;
        }
        int hashCode = (((((this.A03.hashCode() * 31) + C72463ee.A04(this.A01)) * 31) + this.A02.hashCode()) * 31) + Arrays.hashCode(this.A04);
        this.A00 = hashCode;
        return hashCode;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        UUID uuid = this.A03;
        parcel.writeLong(uuid.getMostSignificantBits());
        parcel.writeLong(uuid.getLeastSignificantBits());
        parcel.writeString(this.A01);
        parcel.writeString(this.A02);
        parcel.writeByteArray(this.A04);
    }
}
