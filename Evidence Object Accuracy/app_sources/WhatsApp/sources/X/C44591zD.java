package X;

/* renamed from: X.1zD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C44591zD extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Boolean A02;
    public Boolean A03;
    public Boolean A04;
    public Boolean A05;
    public Boolean A06;
    public Boolean A07;
    public Integer A08;
    public Integer A09;
    public Long A0A;
    public Long A0B;
    public Long A0C;
    public Long A0D;
    public String A0E;

    public C44591zD() {
        super(932, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(14, this.A0A);
        r3.Abe(11, this.A08);
        r3.Abe(2, this.A0B);
        r3.Abe(10, this.A0C);
        r3.Abe(5, this.A00);
        r3.Abe(4, this.A01);
        r3.Abe(3, this.A02);
        r3.Abe(1, this.A03);
        r3.Abe(8, this.A04);
        r3.Abe(12, this.A09);
        r3.Abe(6, this.A05);
        r3.Abe(9, this.A06);
        r3.Abe(20, this.A0E);
        r3.Abe(7, this.A07);
        r3.Abe(13, this.A0D);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        StringBuilder sb = new StringBuilder("WamChatDatabaseRestoreEvent {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupFileIndex", this.A0A);
        Integer num = this.A08;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "backupRestoreStatusOfBackupFoundAtRestoreTime", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "databaseBackupVersion", this.A0B);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "databaseDumpAndRestoreRecoveryPercentage", this.A0C);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "databaseRepairEnabled", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "databaseRestoreCorrectJid", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "databaseRestoreFileIntegrityCheck", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "databaseRestoreOverallResult", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "databaseRestoreReindexingResult", this.A04);
        Integer num2 = this.A09;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "databaseRestoreResultDetails", obj2);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "databaseRestoreSqliteIntegrityCheckResult", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "dbDumpAndRestoreResult", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "genericFailureReason", this.A0E);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "hasOnlyIndexErrors", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "restoreTimeBackupFilesCount", this.A0D);
        sb.append("}");
        return sb.toString();
    }
}
