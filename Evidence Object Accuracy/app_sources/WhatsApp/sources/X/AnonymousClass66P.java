package X;

import android.graphics.SurfaceTexture;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Build;
import android.util.Log;
import android.view.Surface;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/* renamed from: X.66P  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass66P implements AbstractC136426Mm {
    public static final boolean A0C;
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public MediaRecorder A04;
    public Surface A05;
    public RandomAccessFile A06;
    public CountDownLatch A07;
    public final Object A08;
    public volatile SurfaceTexture A09;
    public volatile SurfaceTexture A0A;
    public volatile AnonymousClass637 A0B;

    @Override // X.AbstractC136426Mm
    public AnonymousClass60Q AeL(CamcorderProfile camcorderProfile, AnonymousClass6LR r3, FileDescriptor fileDescriptor, int i, int i2, boolean z, boolean z2) {
        return null;
    }

    static {
        boolean z = false;
        if (Build.VERSION.SDK_INT >= 21) {
            z = true;
        }
        A0C = z;
    }

    public AnonymousClass66P() {
        Object A0l = C12970iu.A0l();
        this.A08 = A0l;
        synchronized (A0l) {
            this.A07 = new CountDownLatch(1);
        }
    }

    public SurfaceTexture A00(int i, int i2, int i3, int i4) {
        SurfaceTexture surfaceTexture;
        this.A00 = i3;
        this.A03 = i4;
        this.A02 = i;
        this.A01 = i2;
        try {
            this.A07.await(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            Log.e("GLSurfacePipeCoordinatorImpl", C12960it.A0d(e.getMessage(), C12960it.A0k("Timeout when creating SurfaceNode: ")));
        }
        synchronized (this.A08) {
            SurfaceTexture surfaceTexture2 = this.A0A;
            if (surfaceTexture2 != null) {
                if (A0C) {
                    AnonymousClass637 r2 = this.A0B;
                    if (r2 == null) {
                        r2 = new AnonymousClass637();
                        this.A0B = r2;
                    }
                    r2.A05(surfaceTexture2, this.A03);
                    surfaceTexture2 = r2.A03(this.A02, this.A01);
                }
                this.A09 = surfaceTexture2;
                surfaceTexture = this.A09;
            } else {
                throw C12960it.A0U("SurfaceNode was not created");
            }
        }
        return surfaceTexture;
    }

    public void A01() {
        if (A0C) {
            AnonymousClass637 r1 = this.A0B;
            this.A0B = null;
            if (r1 != null) {
                r1.A04();
            }
            this.A09 = null;
        }
    }

    public void A02(int i) {
        this.A03 = i;
        if (A0C) {
            synchronized (this.A08) {
                SurfaceTexture surfaceTexture = this.A0A;
                AnonymousClass637 r1 = this.A0B;
                if (!(r1 == null || surfaceTexture == null)) {
                    r1.A05(surfaceTexture, this.A03);
                }
            }
        }
    }

    @Override // X.AbstractC136426Mm
    public AnonymousClass60Q AeM(CamcorderProfile camcorderProfile, AnonymousClass6LR r10, String str, int i, int i2, boolean z, boolean z2) {
        int i3;
        int i4;
        int i5;
        int i6;
        boolean z3 = true;
        if (i2 == 90 || i2 == 270) {
            i3 = camcorderProfile.videoFrameHeight;
            i4 = camcorderProfile.videoFrameWidth;
            i5 = this.A01;
            i6 = this.A02;
        } else {
            i3 = camcorderProfile.videoFrameWidth;
            i4 = camcorderProfile.videoFrameHeight;
            i5 = this.A02;
            i6 = this.A01;
        }
        float f = (float) i3;
        float f2 = (float) i4;
        float f3 = ((float) i5) / ((float) i6);
        if (f / f2 > f3) {
            i3 = (int) (f2 * f3);
        } else {
            i4 = (int) (f / f3);
        }
        int i7 = i3 - (i3 % 16);
        int i8 = i4 - (i4 % 16);
        int i9 = (this.A00 + this.A03) % 360;
        int i10 = i8;
        if (i9 % 180 == 0) {
            z3 = false;
            i10 = i7;
        }
        camcorderProfile.videoFrameWidth = i10;
        if (!z3) {
            i7 = i8;
        }
        camcorderProfile.videoFrameHeight = i7;
        this.A06 = new RandomAccessFile(str, "rws");
        MediaRecorder mediaRecorder = new MediaRecorder();
        this.A04 = mediaRecorder;
        mediaRecorder.setAudioSource(5);
        this.A04.setVideoSource(2);
        this.A04.setOrientationHint(i9);
        this.A04.setProfile(camcorderProfile);
        this.A04.setOutputFile(this.A06.getFD());
        this.A04.prepare();
        this.A05 = this.A04.getSurface();
        AnonymousClass637 r2 = this.A0B;
        if (r2 != null) {
            r2.A07(this.A05, this.A03);
        }
        this.A04.start();
        return C129375xc.A00(camcorderProfile, str, i9, i);
    }

    /* JADX INFO: finally extract failed */
    @Override // X.AbstractC136426Mm
    public void AeU() {
        try {
            try {
                MediaRecorder mediaRecorder = this.A04;
                if (mediaRecorder != null) {
                    mediaRecorder.stop();
                }
                MediaRecorder mediaRecorder2 = this.A04;
                if (mediaRecorder2 != null) {
                    mediaRecorder2.reset();
                    this.A04.release();
                    this.A04 = null;
                }
                AnonymousClass637 r0 = this.A0B;
                if (r0 != null) {
                    r0.A07(null, 0);
                }
                Surface surface = this.A05;
                if (surface != null) {
                    surface.release();
                    this.A05 = null;
                }
                RandomAccessFile randomAccessFile = this.A06;
                if (randomAccessFile != null) {
                    try {
                        randomAccessFile.close();
                    } catch (IOException unused) {
                    }
                    this.A06 = null;
                }
            } catch (RuntimeException e) {
                throw C117315Zl.A0J(e);
            }
        } catch (Throwable th) {
            MediaRecorder mediaRecorder3 = this.A04;
            if (mediaRecorder3 != null) {
                mediaRecorder3.reset();
                this.A04.release();
                this.A04 = null;
            }
            AnonymousClass637 r02 = this.A0B;
            if (r02 != null) {
                r02.A07(null, 0);
            }
            Surface surface2 = this.A05;
            if (surface2 != null) {
                surface2.release();
                this.A05 = null;
            }
            RandomAccessFile randomAccessFile2 = this.A06;
            if (randomAccessFile2 != null) {
                try {
                    randomAccessFile2.close();
                } catch (IOException unused2) {
                }
                this.A06 = null;
            }
            throw th;
        }
    }
}
