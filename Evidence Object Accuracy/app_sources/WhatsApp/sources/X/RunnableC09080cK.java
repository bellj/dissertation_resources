package X;

/* renamed from: X.0cK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09080cK implements Runnable {
    public final /* synthetic */ AnonymousClass0XR A00;

    public RunnableC09080cK(AnonymousClass0XR r1) {
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        AnonymousClass0XR r2 = this.A00;
        C02360Bs r0 = r2.A0E;
        if (r0 != null && AnonymousClass028.A0q(r0) && r2.A0E.getCount() > r2.A0E.getChildCount() && r2.A0E.getChildCount() <= r2.A06) {
            r2.A0D.setInputMethodMode(2);
            r2.Ade();
        }
    }
}
