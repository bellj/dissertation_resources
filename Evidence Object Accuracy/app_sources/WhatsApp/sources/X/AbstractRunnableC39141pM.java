package X;

/* renamed from: X.1pM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractRunnableC39141pM implements Runnable, AbstractC14600lh {
    public final AbstractC39111pJ A00;

    public AbstractRunnableC39141pM(AbstractC39111pJ r1) {
        this.A00 = r1;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:167:0x0009 */
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: X.1pR */
    /* JADX DEBUG: Multi-variable search result rejected for r9v1, resolved type: java.lang.Boolean */
    /* JADX DEBUG: Multi-variable search result rejected for r4v2, resolved type: X.1pR */
    /* JADX DEBUG: Multi-variable search result rejected for r4v3, resolved type: X.1pR */
    /* JADX DEBUG: Multi-variable search result rejected for r9v11, resolved type: java.lang.Boolean */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x01a7, code lost:
        if (r8.exists() != false) goto L_0x01a9;
     */
    /* JADX WARNING: Removed duplicated region for block: B:101:0x01bf  */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x01cf  */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x0247  */
    /* JADX WARNING: Removed duplicated region for block: B:159:0x02a7  */
    /* JADX WARNING: Removed duplicated region for block: B:161:0x02ac  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0104 A[DONT_GENERATE] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0122  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0132  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AbstractC39731qS A00() {
        /*
        // Method dump skipped, instructions count: 694
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractRunnableC39141pM.A00():X.1qS");
    }

    @Override // X.AbstractC14600lh
    public synchronized void cancel() {
        AbstractC39001p6 r0;
        AbstractC39111pJ r1 = this.A00;
        synchronized (r1) {
            r0 = r1.A00;
        }
        if (r0 != null) {
            r0.cancel();
        }
    }

    @Override // java.lang.Runnable
    public void run() {
        this.A00.A05.AQa(A00());
    }
}
