package X;

/* renamed from: X.3ry  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C80263ry extends C80273rz {
    public final int zzc;
    public final int zzd;

    public C80263ry(byte[] bArr, int i, int i2) {
        super(bArr);
        AbstractC111925Bi.A01(i, i + i2, bArr.length);
        this.zzc = i;
        this.zzd = i2;
    }
}
