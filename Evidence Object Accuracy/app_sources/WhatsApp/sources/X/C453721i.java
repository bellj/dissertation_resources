package X;

import android.os.Message;
import com.facebook.redex.RunnableBRunnable0Shape8S0100000_I0_8;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.jobqueue.job.SendRetryReceiptJob;
import com.whatsapp.util.Log;

/* renamed from: X.21i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C453721i {
    public final int A00;
    public final C22920zr A01;
    public final C20670w8 A02;
    public final C22320yt A03;
    public final C238613h A04;
    public final C20660w7 A05;
    public final C28941Pp A06;
    public final C29211Rh A07;
    public final C29211Rh A08;
    public final boolean A09;
    public final byte[] A0A;

    public C453721i(C22920zr r2, C20670w8 r3, C15990oG r4, C22320yt r5, C238613h r6, C20660w7 r7, C28941Pp r8, C29211Rh r9, C29211Rh r10, byte[] bArr, boolean z) {
        this.A05 = r7;
        this.A02 = r3;
        this.A01 = r2;
        this.A03 = r5;
        this.A04 = r6;
        this.A06 = r8;
        this.A00 = r4.A07.A01();
        this.A0A = bArr;
        this.A08 = r9;
        this.A07 = r10;
        this.A09 = z;
    }

    public void A00() {
        StringBuilder sb = new StringBuilder("need to send retry receipt; message.key=");
        C28941Pp r2 = this.A06;
        sb.append(r2.A03());
        sb.append(" participant = ");
        sb.append(r2.A08);
        Log.i(sb.toString());
        r2.A0b = true;
        int i = this.A00;
        byte[] A03 = C16050oM.A03(i);
        if (r2.A00() > 1) {
            this.A01.A01();
        }
        if (r2.A01 == 0 && r2.A00 == 0) {
            StringBuilder sb2 = new StringBuilder("recording local placeholder for retry receipt; message.key=");
            sb2.append(r2.A03());
            Log.i(sb2.toString());
            this.A03.A01(new RunnableBRunnable0Shape8S0100000_I0_8(this, 40), 50);
        }
        StringBuilder sb3 = new StringBuilder("axolotl sending retry receipt; message.key=");
        sb3.append(r2.A03());
        sb3.append("; localRegistrationId=");
        sb3.append(i);
        Log.i(sb3.toString());
        if (!this.A09) {
            C20660w7 r5 = this.A05;
            Jid jid = r2.A0g;
            String str = r2.A0k;
            Jid jid2 = r2.A08;
            UserJid userJid = r2.A0h;
            long j = r2.A0f;
            int A00 = r2.A00() + 1;
            byte[] bArr = this.A0A;
            C29211Rh r9 = this.A08;
            C29211Rh r10 = this.A07;
            long j2 = r2.A06;
            String str2 = r2.A0R;
            if (r5.A01.A06) {
                r5.A06.A08(Message.obtain(null, 0, 11, 0, new AnonymousClass2EB(jid, jid2, userJid, r9, r10, str, str2, A03, bArr, (byte) 5, A00, j, j2)), false);
                return;
            }
            return;
        }
        this.A02.A00(new SendRetryReceiptJob(r2, i));
    }
}
