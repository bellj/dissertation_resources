package X;

import com.whatsapp.R;

/* renamed from: X.4EH  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4EH {
    public static int A00(AbstractC28871Pi r1) {
        if (r1 instanceof AnonymousClass1XU) {
            return R.drawable.msg_status_image;
        }
        if (r1 instanceof AnonymousClass1XT) {
            return R.drawable.msg_status_doc;
        }
        if (r1 instanceof AnonymousClass1XN) {
            return R.drawable.msg_status_location;
        }
        if (r1 instanceof AnonymousClass1XQ) {
            return R.drawable.msg_status_gif;
        }
        if (r1 instanceof AnonymousClass1XS) {
            return R.drawable.msg_status_video;
        }
        return 0;
    }
}
