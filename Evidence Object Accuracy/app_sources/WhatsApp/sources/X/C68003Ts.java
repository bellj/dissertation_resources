package X;

import android.view.View;
import com.facebook.redex.ViewOnClickCListenerShape6S0100000_I1;
import com.whatsapp.R;

/* renamed from: X.3Ts  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68003Ts implements AbstractC116635Wf {
    public View A00;
    public final AnonymousClass01F A01;
    public final C53042cM A02;
    public final C252818u A03;
    public final C14830m7 A04;
    public final C26851Fb A05;
    public final AnonymousClass4S4 A06;

    public C68003Ts(AnonymousClass01F r1, C53042cM r2, C252818u r3, C14830m7 r4, C26851Fb r5, AnonymousClass4S4 r6) {
        this.A02 = r2;
        this.A01 = r1;
        this.A06 = r6;
        this.A03 = r3;
        this.A05 = r5;
        this.A04 = r4;
    }

    @Override // X.AbstractC116635Wf
    public void AIR() {
        C12970iu.A1G(this.A00);
    }

    @Override // X.AbstractC116635Wf
    public boolean AdK() {
        this.A02.getContext();
        return false;
    }

    @Override // X.AbstractC116635Wf
    public void AfF() {
        C53042cM r8 = this.A02;
        r8.getContext();
        View view = this.A00;
        if (view == null) {
            view = C12960it.A0F(C12960it.A0E(r8), r8, R.layout.coversations_smb_warning_notification_chat_banner);
            this.A00 = view;
        }
        C12960it.A0I(view, R.id.smb_warning_banner_body).setText(AnonymousClass1US.A00(r8.getContext(), new Object[]{AnonymousClass1US.A05(r8.getContext(), R.color.smb_soft_enforcement_banner_body_link_color)}, R.string.whatsapp_access_at_risk_body));
        r8.setBackgroundResource(R.color.smb_soft_enforcement_banner_background_color);
        AnonymousClass4S4 r3 = this.A06;
        r8.setOnClickListener(new ViewOnClickCListenerShape6S0100000_I1(this, 41));
        AnonymousClass028.A0D(view, R.id.close).setOnClickListener(new ViewOnClickCListenerShape6S0100000_I1(this, 40));
        view.setVisibility(0);
        if (!r3.A01.A07(1730)) {
            AnonymousClass30C r1 = new AnonymousClass30C();
            r1.A00 = 1;
            r1.A01 = 32;
            r3.A03.A06(r1);
        }
        new AnonymousClass316();
        throw C12980iv.A0n("source");
    }
}
