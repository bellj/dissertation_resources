package X;

/* renamed from: X.3G8  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3G8 {
    public static AbstractC14200l1 A00(AnonymousClass5XA r2) {
        if (r2 instanceof C1093151f) {
            Object obj = ((C1093151f) r2).A00;
            if (obj instanceof C94724cR) {
                return ((C94724cR) obj).A00;
            }
        }
        return C12980iv.A0V(r2.AeX());
    }
}
