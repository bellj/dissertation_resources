package X;

import java.io.File;
import java.io.FileFilter;

/* renamed from: X.1Q7  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1Q7 {
    public static final FileFilter A00 = new FileFilter() { // from class: X.5BI
        @Override // java.io.FileFilter
        public final boolean accept(File file) {
            String name = file.getName();
            if (!name.startsWith("cpu")) {
                return false;
            }
            for (int i = 3; i < name.length(); i++) {
                if (name.charAt(i) < '0' || name.charAt(i) > '9') {
                    return false;
                }
            }
            return true;
        }
    };

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00a9, code lost:
        if (r7 >= 1024) goto L_0x00da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00ab, code lost:
        r1 = r8[r7];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00af, code lost:
        if (r1 == 10) goto L_0x00da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00b3, code lost:
        if (r1 < 48) goto L_0x00c8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00b7, code lost:
        if (r1 > 57) goto L_0x00c8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00b9, code lost:
        r1 = r7 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00bb, code lost:
        if (r1 >= 1024) goto L_0x00cb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00bf, code lost:
        if (r8[r1] < 48) goto L_0x00cb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00c3, code lost:
        if (r8[r1] > 57) goto L_0x00cb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00c5, code lost:
        r1 = r1 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00c8, code lost:
        r7 = r7 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00cb, code lost:
        r0 = java.lang.Integer.parseInt(new java.lang.String(r8, r7, r1 - r7)) * 1000;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00d7, code lost:
        if (r0 <= r6) goto L_0x00da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00d9, code lost:
        r6 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x00a6, code lost:
        continue;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A00() {
        /*
        // Method dump skipped, instructions count: 229
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1Q7.A00():int");
    }

    public static int A01() {
        try {
            return new File("/sys/devices/system/cpu/").listFiles(A00).length;
        } catch (NullPointerException | SecurityException unused) {
            return -1;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x005e, code lost:
        if (0 == 0) goto L_0x0068;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A02(java.lang.String r6) {
        /*
            java.lang.String r1 = "/proc/cpuinfo"
            java.lang.String r2 = "DeviceInfo/searchFileForText bufferedReader.close failed"
            java.util.Locale r0 = java.util.Locale.US
            java.lang.String r6 = r6.toLowerCase(r0)
            r5 = 0
            r4 = 0
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch: Exception -> 0x0058, all -> 0x0069
            r3.<init>(r1)     // Catch: Exception -> 0x0058, all -> 0x0069
            java.lang.String r1 = X.AnonymousClass01V.A08     // Catch: UnsupportedEncodingException -> 0x001e, all -> 0x0053
            java.io.InputStreamReader r0 = new java.io.InputStreamReader     // Catch: UnsupportedEncodingException -> 0x001e, all -> 0x0053
            r0.<init>(r3, r1)     // Catch: UnsupportedEncodingException -> 0x001e, all -> 0x0053
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch: UnsupportedEncodingException -> 0x001e, all -> 0x0053
            r1.<init>(r0)     // Catch: UnsupportedEncodingException -> 0x001e, all -> 0x0053
            goto L_0x002e
        L_0x001e:
            r1 = move-exception
            java.lang.String r0 = "searchFileForText/unsupported-encoding: UTF-8"
            com.whatsapp.util.Log.i(r0, r1)     // Catch: all -> 0x0053
            java.io.InputStreamReader r0 = new java.io.InputStreamReader     // Catch: all -> 0x0053
            r0.<init>(r3)     // Catch: all -> 0x0053
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch: all -> 0x0053
            r1.<init>(r0)     // Catch: all -> 0x0053
        L_0x002e:
            r5 = r1
        L_0x002f:
            java.lang.String r1 = r5.readLine()     // Catch: all -> 0x0053
            if (r1 == 0) goto L_0x004f
            java.util.Locale r0 = java.util.Locale.US     // Catch: all -> 0x0053
            java.lang.String r0 = r1.toLowerCase(r0)     // Catch: all -> 0x0053
            boolean r0 = r0.contains(r6)     // Catch: all -> 0x0053
            if (r0 == 0) goto L_0x002f
            r1 = 1
            r3.close()     // Catch: Exception -> 0x0058, all -> 0x0069
            r5.close()     // Catch: Exception -> 0x0049
            goto L_0x004e
        L_0x0049:
            r0 = move-exception
            com.whatsapp.util.Log.w(r2, r0)
            return r1
        L_0x004e:
            return r1
        L_0x004f:
            r3.close()     // Catch: Exception -> 0x0058, all -> 0x0069
            goto L_0x0060
        L_0x0053:
            r0 = move-exception
            r3.close()     // Catch: all -> 0x0057
        L_0x0057:
            throw r0     // Catch: Exception -> 0x0058, all -> 0x0069
        L_0x0058:
            r1 = move-exception
            java.lang.String r0 = "DeviceInfo/searchFileForText read failed"
            com.whatsapp.util.Log.w(r0, r1)     // Catch: all -> 0x0069
            if (r5 == 0) goto L_0x0068
        L_0x0060:
            r5.close()     // Catch: Exception -> 0x0064
            return r4
        L_0x0064:
            r0 = move-exception
            com.whatsapp.util.Log.w(r2, r0)
        L_0x0068:
            return r4
        L_0x0069:
            r1 = move-exception
            if (r5 == 0) goto L_0x0074
            r5.close()     // Catch: Exception -> 0x0070
            throw r1
        L_0x0070:
            r0 = move-exception
            com.whatsapp.util.Log.w(r2, r0)
        L_0x0074:
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1Q7.A02(java.lang.String):boolean");
    }
}
