package X;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaFrameLayout;
import com.whatsapp.WaImageView;

/* renamed from: X.2qr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58562qr extends WaFrameLayout {
    public final TextEmojiLabel A00 = C12970iu.A0T(this, R.id.total_price);
    public final TextEmojiLabel A01 = C12970iu.A0T(this, R.id.quantity);
    public final TextEmojiLabel A02 = C12970iu.A0T(this, R.id.reference_id);
    public final TextEmojiLabel A03 = C12970iu.A0T(this, R.id.header_title);
    public final WaImageView A04 = C12980iv.A0X(this, R.id.thumbnail);

    public C58562qr(Context context) {
        super(context, null);
        LayoutInflater.from(context).inflate(R.layout.conversation_row_header_checkout, (ViewGroup) this, true);
    }
}
