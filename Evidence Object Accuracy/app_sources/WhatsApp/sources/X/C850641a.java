package X;

import com.whatsapp.group.GroupChatInfo;

/* renamed from: X.41a  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C850641a extends AnonymousClass2Dn {
    public final /* synthetic */ GroupChatInfo A00;

    public C850641a(GroupChatInfo groupChatInfo) {
        this.A00 = groupChatInfo;
    }

    @Override // X.AnonymousClass2Dn
    public void A00(AbstractC14640lm r2) {
        GroupChatInfo.A03(this.A00);
    }
}
