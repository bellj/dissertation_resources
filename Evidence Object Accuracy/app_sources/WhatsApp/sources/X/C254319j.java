package X;

import java.util.HashMap;
import java.util.Map;

/* renamed from: X.19j  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C254319j {
    public final C14830m7 A00;
    public final C16120oU A01;
    public final Map A02 = new HashMap();

    public C254319j(C14830m7 r2, C16120oU r3) {
        this.A00 = r2;
        this.A01 = r3;
    }

    public final void A00(int i, String str, boolean z) {
        C614130g r1 = new C614130g();
        r1.A02 = Integer.valueOf(i);
        r1.A04 = (Long) this.A02.get(str);
        r1.A00 = Boolean.valueOf(z);
        this.A01.A07(r1);
    }

    public void A01(String str, boolean z) {
        C614130g r1 = new C614130g();
        r1.A02 = 5;
        r1.A04 = (Long) this.A02.get(str);
        r1.A00 = Boolean.TRUE;
        r1.A01 = Boolean.valueOf(z);
        this.A01.A07(r1);
    }
}
