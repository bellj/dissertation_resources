package X;

import android.graphics.Bitmap;
import android.os.Build;

/* renamed from: X.4c9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C94594c9 {
    public static final AnonymousClass0DQ A00 = new AnonymousClass0DQ(12);

    public static int A00(Bitmap.Config config) {
        int i = AnonymousClass4FU.A00[config.ordinal()];
        if (i == 1) {
            return 4;
        }
        if (i == 2) {
            return 1;
        }
        if (i == 3 || i == 4) {
            return 2;
        }
        if (i == 5) {
            return 8;
        }
        throw C12980iv.A0u("The provided Bitmap.Config is not supported");
    }

    public static int A01(Bitmap bitmap) {
        if (bitmap == null) {
            return 0;
        }
        if (Build.VERSION.SDK_INT > 19) {
            try {
                return bitmap.getAllocationByteCount();
            } catch (NullPointerException unused) {
            }
        }
        return bitmap.getByteCount();
    }
}
