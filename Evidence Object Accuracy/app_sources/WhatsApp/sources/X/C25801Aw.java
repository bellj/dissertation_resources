package X;

import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* renamed from: X.1Aw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C25801Aw {
    public final C246516i A00 = new C246516i(5);

    public final AnonymousClass4K5 A00(UserJid userJid) {
        C246516i r2 = this.A00;
        AnonymousClass4K5 r1 = (AnonymousClass4K5) r2.get(userJid);
        if (r1 != null) {
            return r1;
        }
        AnonymousClass4K5 r12 = new AnonymousClass4K5();
        r12.A00.put("catalog_category_dummy_root_id", new AnonymousClass4SX(new C44741zT("catalog_category_dummy_root_id", null, null, 0, 0), "catalog_category_dummy_root_id", "root", new ArrayList(), false));
        r2.put(userJid, r12);
        return r12;
    }

    public List A01(UserJid userJid, String str) {
        ArrayList arrayList;
        C16700pc.A0E(str, 0);
        C16700pc.A0E(userJid, 1);
        synchronized (this) {
            Map map = A00(userJid).A00;
            AnonymousClass4SX r1 = (AnonymousClass4SX) map.get(str);
            arrayList = new ArrayList();
            if (r1 != null && !r1.A04) {
                for (String str2 : r1.A03) {
                    AnonymousClass4SX r0 = (AnonymousClass4SX) map.get(str2);
                    if (r0 != null) {
                        arrayList.add(r0);
                    }
                }
            }
        }
        return arrayList;
    }

    public void A02(AnonymousClass4SX r6, UserJid userJid, boolean z) {
        AnonymousClass4SX r0;
        synchronized (this) {
            String str = r6.A01;
            C16700pc.A0B(str);
            AnonymousClass4K5 A00 = A00(userJid);
            if (z && (r0 = (AnonymousClass4SX) A00(userJid).A00.get("catalog_category_dummy_root_id")) != null) {
                r0.A03.add(str);
            }
            A00.A00.put(str, r6);
        }
    }

    public void A03(AnonymousClass3CI r8, UserJid userJid, boolean z) {
        C16700pc.A0E(userJid, 2);
        synchronized (this) {
            for (Object obj : r8.A00) {
                C16700pc.A0B(obj);
                AnonymousClass4VK r0 = (AnonymousClass4VK) obj;
                AnonymousClass4SX r4 = r0.A00;
                C16700pc.A0B(r4);
                List list = r4.A03;
                list.clear();
                for (Object obj2 : r0.A01) {
                    C16700pc.A0B(obj2);
                    AnonymousClass4SX r1 = (AnonymousClass4SX) obj2;
                    list.add(r1.A01);
                    A02(r1, userJid, false);
                }
                A02(r4, userJid, z);
            }
        }
    }

    public boolean A04(UserJid userJid, String str) {
        C16700pc.A0E(str, 0);
        C16700pc.A0E(userJid, 1);
        synchronized (this) {
            AnonymousClass4SX r2 = (AnonymousClass4SX) A00(userJid).A00.get(str);
            boolean z = false;
            if (r2 == null) {
                return false;
            }
            if (!r2.A04) {
                if (!r2.A03.isEmpty()) {
                    z = true;
                }
            }
            return z;
        }
    }
}
