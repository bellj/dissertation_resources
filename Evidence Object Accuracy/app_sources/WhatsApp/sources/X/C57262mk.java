package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2mk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57262mk extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57262mk A04;
    public static volatile AnonymousClass255 A05;
    public int A00 = 0;
    public int A01;
    public AnonymousClass1K6 A02 = AnonymousClass277.A01;
    public Object A03;

    static {
        C57262mk r0 = new C57262mk();
        A04 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r6, Object obj, Object obj2) {
        C81683uP r1;
        boolean z = false;
        switch (r6.ordinal()) {
            case 0:
                return A04;
            case 1:
                AbstractC462925h r7 = (AbstractC462925h) obj;
                C57262mk r8 = (C57262mk) obj2;
                this.A02 = r7.Afr(this.A02, r8.A02);
                int i = r8.A00;
                switch ((i != 0 ? i != 2 ? null : EnumC87194Ao.A02 : EnumC87194Ao.A01).ordinal()) {
                    case 0:
                        if (this.A00 == 2) {
                            z = true;
                        }
                        this.A03 = r7.Afv(this.A03, r8.A03, z);
                        break;
                    case 1:
                        if (this.A00 != 0) {
                            z = true;
                        }
                        r7.Afw(z);
                        break;
                }
                if (r7 == C463025i.A00) {
                    int i2 = r8.A00;
                    if (i2 != 0) {
                        this.A00 = i2;
                    }
                    this.A01 |= r8.A01;
                }
                return this;
            case 2:
                AnonymousClass253 r72 = (AnonymousClass253) obj;
                AnonymousClass254 r82 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        int A03 = r72.A03();
                        if (A03 == 0) {
                            break;
                        } else if (A03 == 10) {
                            AnonymousClass1K6 r12 = this.A02;
                            if (!((AnonymousClass1K7) r12).A00) {
                                r12 = AbstractC27091Fz.A0G(r12);
                                this.A02 = r12;
                            }
                            r12.add((C57472n7) AbstractC27091Fz.A0H(r72, r82, C57472n7.A05));
                        } else if (A03 == 18) {
                            if (this.A00 == 2) {
                                r1 = (C81683uP) ((C57272ml) this.A03).A0T();
                            } else {
                                r1 = null;
                            }
                            AnonymousClass1G1 A0H = AbstractC27091Fz.A0H(r72, r82, C57272ml.A04);
                            this.A03 = A0H;
                            if (r1 != null) {
                                this.A03 = AbstractC27091Fz.A0C(r1, (C57272ml) A0H);
                            }
                            this.A00 = 2;
                        } else if (!A0a(r72, A03)) {
                            break;
                        }
                    } catch (C28971Pt e) {
                        throw AbstractC27091Fz.A0J(e, this);
                    } catch (IOException e2) {
                        throw AbstractC27091Fz.A0K(this, e2);
                    }
                }
            case 3:
                ((AnonymousClass1K7) this.A02).A00 = false;
                return null;
            case 4:
                return new C57262mk();
            case 5:
                return new C81673uO();
            case 6:
                break;
            case 7:
                if (A05 == null) {
                    synchronized (C57262mk.class) {
                        if (A05 == null) {
                            A05 = AbstractC27091Fz.A09(A04);
                        }
                    }
                }
                return A05;
            default:
                throw C12970iu.A0z();
        }
        return A04;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < this.A02.size(); i3++) {
            i2 = AbstractC27091Fz.A08((AnonymousClass1G1) this.A02.get(i3), 1, i2);
        }
        if (this.A00 == 2) {
            i2 = AbstractC27091Fz.A08((AnonymousClass1G0) this.A03, 2, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        for (int i = 0; i < this.A02.size(); i++) {
            codedOutputStream.A0L((AnonymousClass1G1) this.A02.get(i), 1);
        }
        if (this.A00 == 2) {
            AbstractC27091Fz.A0O(codedOutputStream, this.A03, 2);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
