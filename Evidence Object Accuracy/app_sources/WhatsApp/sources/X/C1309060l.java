package X;

import android.text.TextUtils;

/* renamed from: X.60l  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1309060l {
    public static String A00(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        StringBuilder A0k = C12960it.A0k("[data localized ");
        A0k.append(str.getBytes().length);
        return C12960it.A0d(" bytes]", A0k);
    }

    public static String A01(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        int length = (str.length() + 1) >> 1;
        StringBuilder A0k = C12960it.A0k("[");
        A0k.append(length);
        A0k.append(" char]");
        return C12960it.A0d(str.substring(length), A0k);
    }

    public static String A02(String str) {
        return (TextUtils.isEmpty(str) || !str.contains("@")) ? "" : "SCRUBBED-VPA";
    }

    public static void A03(StringBuilder sb, String str) {
        sb.append(A00(str));
    }
}
