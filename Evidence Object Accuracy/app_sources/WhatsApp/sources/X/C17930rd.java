package X;

import java.util.Collections;
import java.util.LinkedHashSet;

/* renamed from: X.0rd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17930rd {
    public static final C17930rd A0D;
    public static final C17930rd A0E;
    public static final C17930rd A0F;
    public static final C17930rd[] A0G = new C17930rd[0];
    public static final C17930rd[] A0H;
    public static final C17930rd[] A0I;
    public final int A00;
    public final int A01;
    public final AbstractC30791Yv A02;
    public final String A03;
    public final String A04;
    public final LinkedHashSet A05;
    public final boolean A06;
    public final boolean A07;
    public final boolean A08;
    public final int[] A09;
    public final int[] A0A;
    public final C32641cU[] A0B;
    public final C32641cU[] A0C;

    static {
        AbstractC30791Yv r15 = C30771Yt.A06;
        C17930rd r14 = new C17930rd(r15, "UNSET", "UNSET", new LinkedHashSet(Collections.singletonList(r15)), null, null, null, null, 0, 0, false, false, false);
        A0F = r14;
        AbstractC30791Yv r16 = C30771Yt.A05;
        C17930rd r152 = new C17930rd(r16, "IN", "91", new LinkedHashSet(Collections.singletonList(r16)), new int[]{2}, new int[]{2}, new C32641cU[]{new C32641cU("tos_no_wallet", "1", false), new C32641cU("add_bank", "1", false), new C32641cU("2fa", "1", false)}, new C32641cU[]{new C32641cU("add_payment_service", "1", false)}, 3, 3, true, true, true);
        A0E = r152;
        AbstractC30791Yv r17 = C30771Yt.A04;
        C17930rd r162 = new C17930rd(r17, "BR", "55", new LinkedHashSet(Collections.singletonList(r17)), new int[]{1, 4, 6}, new int[]{1, 4, 6}, new C32641cU[]{new C32641cU("tos_no_wallet", "1", false), new C32641cU("add_card", "1", false)}, new C32641cU[]{new C32641cU("add_business", "1", false)}, 1, 1, true, true, false);
        A0D = r162;
        A0H = new C17930rd[]{r14, r152, r162};
        A0I = new C17930rd[]{r152, r162, C468427x.A02, C468427x.A01};
    }

    public C17930rd(AbstractC30791Yv r1, String str, String str2, LinkedHashSet linkedHashSet, int[] iArr, int[] iArr2, C32641cU[] r7, C32641cU[] r8, int i, int i2, boolean z, boolean z2, boolean z3) {
        AnonymousClass009.A04(str);
        this.A03 = str;
        this.A04 = str2;
        this.A06 = z;
        this.A00 = i;
        this.A01 = i2;
        this.A02 = r1;
        this.A05 = linkedHashSet;
        this.A08 = z2;
        this.A09 = iArr;
        this.A0A = iArr2;
        this.A0C = r7;
        this.A0B = r8;
        this.A07 = z3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0028, code lost:
        return r3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C17930rd A00(java.lang.String r6) {
        /*
            if (r6 == 0) goto L_0x0029
            X.0rd[] r5 = X.C17930rd.A0H
            int r2 = r5.length
            r4 = 0
            r1 = 0
        L_0x0007:
            if (r1 >= r2) goto L_0x0016
            r3 = r5[r1]
            java.lang.String r0 = r3.A03
            boolean r0 = r0.equalsIgnoreCase(r6)
            if (r0 != 0) goto L_0x0028
            int r1 = r1 + 1
            goto L_0x0007
        L_0x0016:
            X.0rd[] r2 = X.C468427x.A00
            int r1 = r2.length
        L_0x0019:
            if (r4 >= r1) goto L_0x0029
            r3 = r2[r4]
            java.lang.String r0 = r3.A03
            boolean r0 = r0.equalsIgnoreCase(r6)
            if (r0 != 0) goto L_0x0028
            int r4 = r4 + 1
            goto L_0x0019
        L_0x0028:
            return r3
        L_0x0029:
            X.0rd r0 = X.C17930rd.A0F
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17930rd.A00(java.lang.String):X.0rd");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002c, code lost:
        return r3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C17930rd A01(java.lang.String r6) {
        /*
            boolean r0 = android.text.TextUtils.isEmpty(r6)
            if (r0 != 0) goto L_0x002d
            X.0rd[] r5 = X.C17930rd.A0H
            int r2 = r5.length
            r4 = 0
            r1 = 0
        L_0x000b:
            if (r1 >= r2) goto L_0x001a
            r3 = r5[r1]
            java.lang.String r0 = r3.A04
            boolean r0 = r0.equals(r6)
            if (r0 != 0) goto L_0x002c
            int r1 = r1 + 1
            goto L_0x000b
        L_0x001a:
            X.0rd[] r2 = X.C468427x.A00
            int r1 = r2.length
        L_0x001d:
            if (r4 >= r1) goto L_0x002d
            r3 = r2[r4]
            java.lang.String r0 = r3.A04
            boolean r0 = r0.equalsIgnoreCase(r6)
            if (r0 != 0) goto L_0x002c
            int r4 = r4 + 1
            goto L_0x001d
        L_0x002c:
            return r3
        L_0x002d:
            X.0rd r0 = X.C17930rd.A0F
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17930rd.A01(java.lang.String):X.0rd");
    }
}
