package X;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.database.sqlite.SQLiteException;
import android.os.OperationCanceledException;
import android.text.TextUtils;
import android.util.Pair;
import android.util.SparseIntArray;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.1hU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C35231hU {
    public AbstractC18860tB A00;
    public AtomicBoolean A01;
    public final AnonymousClass02P A02;
    public final AnonymousClass02P A03;
    public final AnonymousClass02P A04;
    public final AnonymousClass02P A05;
    public final AnonymousClass02P A06;
    public final AnonymousClass02P A07;
    public final AnonymousClass016 A08 = new AnonymousClass016();
    public final AnonymousClass016 A09 = new AnonymousClass016();
    public final AnonymousClass016 A0A = new AnonymousClass016();
    public final AnonymousClass016 A0B = new AnonymousClass016();
    public final AnonymousClass016 A0C = new AnonymousClass016();
    public final C15550nR A0D;
    public final C15610nY A0E;
    public final C15240mn A0F;
    public final C15250mo A0G;
    public final C15250mo A0H;
    public final C14850m9 A0I;
    public final C21230x5 A0J;
    public final AnonymousClass1AY A0K;

    public C35231hU(AnonymousClass017 r9, AnonymousClass017 r10, AnonymousClass017 r11, AnonymousClass017 r12, C15550nR r13, C15610nY r14, AnonymousClass018 r15, C15240mn r16, C14850m9 r17, C21230x5 r18, AnonymousClass1AY r19, AnonymousClass1AW r20) {
        AnonymousClass02P r3 = new AnonymousClass02P();
        this.A04 = r3;
        AnonymousClass02P r4 = new AnonymousClass02P();
        this.A07 = r4;
        AnonymousClass02P r6 = new AnonymousClass02P();
        this.A05 = r6;
        AnonymousClass02P r5 = new AnonymousClass02P();
        this.A02 = r5;
        AnonymousClass02P r2 = new AnonymousClass02P();
        this.A03 = r2;
        AnonymousClass02P r1 = new AnonymousClass02P();
        this.A06 = r1;
        this.A01 = new AtomicBoolean();
        this.A00 = new C35171hM(this);
        C15250mo r7 = new C15250mo(r15);
        r7.A01 = 100;
        this.A0G = r7;
        this.A0H = new C15250mo(r15);
        this.A0D = r13;
        this.A0E = r14;
        this.A0F = r16;
        this.A0K = r19;
        this.A0J = r18;
        this.A0I = r17;
        A0A(false);
        r20.A00(new AnonymousClass02O() { // from class: X.2WA
            /* JADX WARNING: Code restructure failed: missing block: B:19:0x00c5, code lost:
                if (r1 == -3) goto L_0x00c7;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:71:0x01f1, code lost:
                if (r1 == false) goto L_0x01f3;
             */
            /* JADX WARNING: Removed duplicated region for block: B:15:0x00b1  */
            /* JADX WARNING: Removed duplicated region for block: B:18:0x00c2  */
            /* JADX WARNING: Removed duplicated region for block: B:24:0x00dc  */
            /* JADX WARNING: Removed duplicated region for block: B:33:0x011e  */
            /* JADX WARNING: Removed duplicated region for block: B:58:0x016f  */
            /* JADX WARNING: Removed duplicated region for block: B:61:0x0190  */
            /* JADX WARNING: Removed duplicated region for block: B:65:0x01a1  */
            /* JADX WARNING: Removed duplicated region for block: B:66:0x01b3  */
            @Override // X.AnonymousClass02O
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object apply(java.lang.Object r30) {
                /*
                // Method dump skipped, instructions count: 541
                */
                throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2WA.apply(java.lang.Object):java.lang.Object");
            }
        }, r3, r6);
        r20.A00(new AnonymousClass02O() { // from class: X.2WB
            @Override // X.AnonymousClass02O
            public final Object apply(Object obj) {
                String join;
                String str;
                AbstractC14640lm r110;
                C35231hU r22 = C35231hU.this;
                AnonymousClass1KL r32 = (AnonymousClass1KL) obj;
                C15250mo r72 = (C15250mo) r32.A01;
                if (r72.A04 != null) {
                    return new ArrayList();
                }
                AnonymousClass016 r0 = r22.A08;
                r0.A0A(Boolean.TRUE);
                C15240mn r62 = r22.A0F;
                AnonymousClass02N r102 = r32.A00;
                StringBuilder sb = new StringBuilder("FtsMessageStore/findChats/");
                sb.append(r72.A01().length());
                C28181Ma r23 = new C28181Ma(sb.toString());
                ArrayList arrayList = new ArrayList();
                if (!r62.A0O()) {
                    str = "FtsMessageStore not ready";
                } else if (C15240mn.A02(r102)) {
                    r23.A02("cancelled");
                    r23.A01();
                    r0.A0A(Boolean.FALSE);
                    return arrayList;
                } else if (!(!r72.A02().isEmpty())) {
                    str = "empty";
                } else if (r62.A04() == 1) {
                    str = "v1";
                } else {
                    if (!(!r72.A02().isEmpty())) {
                        join = "";
                    } else {
                        List<Pair> A0I = r62.A0I(r102, r72, null);
                        HashSet hashSet = new HashSet();
                        for (Pair pair : A0I) {
                            for (Object obj2 : (List) pair.second) {
                                if (obj2 instanceof UserJid) {
                                    hashSet.add(obj2);
                                }
                            }
                        }
                        if (hashSet.isEmpty()) {
                            join = null;
                        } else {
                            String[] strArr = new String[hashSet.size()];
                            int i = 0;
                            Iterator it = hashSet.iterator();
                            while (it.hasNext()) {
                                StringBuilder sb2 = new StringBuilder("fts_jid:");
                                sb2.append(r62.A0D((AbstractC14640lm) it.next()));
                                strArr[i] = sb2.toString();
                                i++;
                            }
                            join = TextUtils.join(" OR ", strArr);
                        }
                    }
                    if (join == null) {
                        str = "no user";
                    } else {
                        String A0B = r62.A0B(r102, r72, null);
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append(A0B);
                        sb3.append(" ");
                        sb3.append(join);
                        String obj3 = sb3.toString();
                        r23.A02("matchterm");
                        try {
                            C16310on A01 = r62.A0C.get();
                            try {
                                Cursor A07 = A01.A03.A07(r102, "SELECT fts_jid, count(*) AS count FROM message_ftsv2 WHERE message_ftsv2 MATCH ? GROUP BY fts_jid", new String[]{obj3});
                                int columnIndexOrThrow = A07.getColumnIndexOrThrow("fts_jid");
                                int columnIndexOrThrow2 = A07.getColumnIndexOrThrow("count");
                                HashMap hashMap = new HashMap();
                                while (true) {
                                    if (A07.moveToNext()) {
                                        if (C15240mn.A02(r102)) {
                                            break;
                                        }
                                        String[] split = TextUtils.split(A07.getString(columnIndexOrThrow), " ");
                                        int i2 = A07.getInt(columnIndexOrThrow2);
                                        for (String str2 : split) {
                                            Integer num = (Integer) hashMap.get(str2);
                                            if (num == null) {
                                                hashMap.put(str2, Integer.valueOf(i2));
                                            } else {
                                                hashMap.put(str2, Integer.valueOf(num.intValue() + i2));
                                            }
                                        }
                                    } else {
                                        r23.A02("counted");
                                        ArrayList arrayList2 = new ArrayList(hashMap.entrySet());
                                        if (!C15240mn.A02(r102)) {
                                            Collections.sort(arrayList2, 
                                            /*  JADX ERROR: Method code generation error
                                                jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x01a1: INVOKE  
                                                  (r1v8 'arrayList2' java.util.ArrayList)
                                                  (wrap: X.2WI : 0x019e: CONSTRUCTOR  (r0v51 X.2WI A[REMOVE]) =  call: X.2WI.<init>():void type: CONSTRUCTOR)
                                                 type: STATIC call: java.util.Collections.sort(java.util.List, java.util.Comparator):void in method: X.2WB.apply(java.lang.Object):java.lang.Object, file: classes2.dex
                                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                                                	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                                                	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                                                	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                                                	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                                                	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:151)
                                                	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                                                	at jadx.core.codegen.RegionGen.makeLoop(RegionGen.java:189)
                                                	at jadx.core.dex.regions.loops.LoopRegion.generate(LoopRegion.java:173)
                                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                                                	at jadx.core.codegen.RegionGen.makeTryCatch(RegionGen.java:314)
                                                	at jadx.core.dex.regions.TryCatchRegion.generate(TryCatchRegion.java:85)
                                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                                                	at jadx.core.codegen.RegionGen.makeTryCatch(RegionGen.java:314)
                                                	at jadx.core.dex.regions.TryCatchRegion.generate(TryCatchRegion.java:85)
                                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                                                	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:151)
                                                	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                                                	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:151)
                                                	at jadx.core.codegen.RegionGen.connectElseIf(RegionGen.java:170)
                                                	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:147)
                                                	at jadx.core.codegen.RegionGen.connectElseIf(RegionGen.java:170)
                                                	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:147)
                                                	at jadx.core.codegen.RegionGen.connectElseIf(RegionGen.java:170)
                                                	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:147)
                                                	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                                	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                                                	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                                                	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                                                	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                                                	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                                                	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                                                	at java.util.ArrayList.forEach(ArrayList.java:1259)
                                                	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                                                	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                                                Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x019e: CONSTRUCTOR  (r0v51 X.2WI A[REMOVE]) =  call: X.2WI.<init>():void type: CONSTRUCTOR in method: X.2WB.apply(java.lang.Object):java.lang.Object, file: classes2.dex
                                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                                                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                                                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                                                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                                                	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                                                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                                                	... 67 more
                                                Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.2WI, state: NOT_LOADED
                                                	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                                                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                                                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                                                	... 73 more
                                                */
                                            /*
                                            // Method dump skipped, instructions count: 584
                                            */
                                            throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2WB.apply(java.lang.Object):java.lang.Object");
                                        }
                                    }, r4, r5);
                                    r20.A00(new AnonymousClass02O() { // from class: X.2WC
                                        @Override // X.AnonymousClass02O
                                        public final Object apply(Object obj) {
                                            String A0B;
                                            String str;
                                            C35231hU r32 = C35231hU.this;
                                            AnonymousClass1KL r42 = (AnonymousClass1KL) obj;
                                            C15250mo r110 = (C15250mo) r42.A01;
                                            if (r110.A02 != 0) {
                                                return new SparseIntArray();
                                            }
                                            AnonymousClass016 r0 = r32.A0A;
                                            r0.A0A(Boolean.TRUE);
                                            C15240mn r72 = r32.A0F;
                                            AnonymousClass02N r112 = r42.A00;
                                            StringBuilder sb = new StringBuilder("FtsMessageStore/getMediaCounts/");
                                            sb.append(r110.A01().length());
                                            C28181Ma r43 = new C28181Ma(sb.toString());
                                            SparseIntArray sparseIntArray = new SparseIntArray();
                                            if (!r72.A0O()) {
                                                str = "FtsMessageStore not ready";
                                            } else {
                                                if (!C15240mn.A02(r112)) {
                                                    if (r72.A04() == 1) {
                                                        str = "v1";
                                                    } else {
                                                        ArrayList arrayList = new ArrayList();
                                                        ArrayList arrayList2 = new ArrayList();
                                                        if (!TextUtils.isEmpty(r110.A01()) || r110.A04 != null) {
                                                            A0B = r72.A0B(r112, r110, null);
                                                        } else {
                                                            A0B = "";
                                                        }
                                                        C15250mo r62 = new C15250mo(r72.A04);
                                                        int[] iArr = C15240mn.A0H;
                                                        int i = 0;
                                                        for (int i2 : iArr) {
                                                            arrayList.add("SELECT count(*) AS count FROM message_ftsv2 WHERE message_ftsv2 MATCH ? ");
                                                            r62.A02 = i2;
                                                            String A0C = r72.A0C(r112, r62, "");
                                                            StringBuilder sb2 = new StringBuilder();
                                                            sb2.append(A0C);
                                                            sb2.append(" ");
                                                            sb2.append(A0B);
                                                            arrayList2.add(sb2.toString());
                                                        }
                                                        if (!C15240mn.A02(r112)) {
                                                            String join = TextUtils.join(" UNION ALL ", arrayList);
                                                            String[] strArr = (String[]) arrayList2.toArray(new String[0]);
                                                            r43.A02("matchterm");
                                                            try {
                                                                C16310on A01 = r72.A0C.get();
                                                                try {
                                                                    Cursor A07 = A01.A03.A07(r112, join, strArr);
                                                                    int columnIndexOrThrow = A07.getColumnIndexOrThrow("count");
                                                                    while (A07.moveToNext()) {
                                                                        if (C15240mn.A02(r112)) {
                                                                            r43.A02("cancelled");
                                                                            r43.A01();
                                                                            A07.close();
                                                                            A01.close();
                                                                            break;
                                                                        }
                                                                        int i3 = A07.getInt(columnIndexOrThrow);
                                                                        if (i3 > 0) {
                                                                            sparseIntArray.put(iArr[i], i3);
                                                                        }
                                                                        i++;
                                                                    }
                                                                    r43.A02("counted");
                                                                    A07.close();
                                                                    A01.close();
                                                                } catch (Throwable th) {
                                                                    try {
                                                                        A01.close();
                                                                    } catch (Throwable unused) {
                                                                    }
                                                                    throw th;
                                                                }
                                                            } catch (AnonymousClass04U unused2) {
                                                            } catch (SQLiteDatabaseCorruptException e) {
                                                                Log.e(e);
                                                                r72.A0B.A02();
                                                            } catch (SQLiteException e2) {
                                                                Log.e("FtsMessageStore/search/error", e2);
                                                                str = "error";
                                                            } catch (Exception e3) {
                                                                if (!(e3 instanceof OperationCanceledException)) {
                                                                    throw e3;
                                                                }
                                                            }
                                                            str = "complete";
                                                        }
                                                    }
                                                }
                                                r43.A02("cancelled");
                                                r43.A01();
                                                r0.A0A(Boolean.FALSE);
                                                return sparseIntArray;
                                            }
                                            r43.A02(str);
                                            r43.A01();
                                            r0.A0A(Boolean.FALSE);
                                            return sparseIntArray;
                                        }
                                    }, r4, r2);
                                    r3.A0D(r1, new AnonymousClass02B() { // from class: X.2WD
                                        @Override // X.AnonymousClass02B
                                        public final void ANq(Object obj) {
                                            C35231hU r42 = C35231hU.this;
                                            Pair pair = (Pair) obj;
                                            Object obj2 = pair.second;
                                            if (obj2 != null) {
                                                Object obj3 = pair.first;
                                                if (obj3 != null) {
                                                    r42.A0G.A08 = Boolean.valueOf(((Boolean) obj3).booleanValue());
                                                }
                                                C15250mo r22 = r42.A0G;
                                                r22.A00 = ((Number) obj2).intValue();
                                                r42.A01.set(true);
                                                r42.A0B.A0B(Boolean.TRUE);
                                                r42.A04.A0A(r22);
                                            }
                                        }
                                    });
                                    r1.A0D(r10, new AnonymousClass02B() { // from class: X.2WE
                                        @Override // X.AnonymousClass02B
                                        public final void ANq(Object obj) {
                                            C35231hU r22 = C35231hU.this;
                                            C15250mo r0 = r22.A0G;
                                            int intValue = ((Number) obj).intValue();
                                            r0.A02 = intValue;
                                            r22.A0H.A02 = intValue;
                                            boolean z = false;
                                            if (intValue == 0) {
                                                z = true;
                                            }
                                            r22.A0A(z);
                                        }
                                    });
                                    r1.A0D(r9, new AnonymousClass02B() { // from class: X.2WF
                                        @Override // X.AnonymousClass02B
                                        public final void ANq(Object obj) {
                                            C35231hU r22 = C35231hU.this;
                                            String str = (String) obj;
                                            C15250mo r110 = r22.A0G;
                                            r110.A03(str);
                                            r22.A0H.A03(str);
                                            boolean z = false;
                                            if (r110.A02 == 0) {
                                                z = true;
                                            }
                                            r22.A0A(z);
                                        }
                                    });
                                    r1.A0D(r11, new AnonymousClass02B() { // from class: X.2WG
                                        @Override // X.AnonymousClass02B
                                        public final void ANq(Object obj) {
                                            C35231hU r42 = C35231hU.this;
                                            AbstractC14640lm r62 = (AbstractC14640lm) obj;
                                            C15250mo r32 = r42.A0G;
                                            r32.A04 = r62;
                                            boolean z = false;
                                            r32.A0G = false;
                                            C15250mo r0 = r42.A0H;
                                            r0.A04 = r62;
                                            r0.A0G = false;
                                            r42.A0C.A0B(r42.A07());
                                            if (r32.A02 == 0) {
                                                z = true;
                                            }
                                            r42.A0A(z);
                                        }
                                    });
                                    r1.A0D(r12, new AnonymousClass02B() { // from class: X.2WH
                                        @Override // X.AnonymousClass02B
                                        public final void ANq(Object obj) {
                                            C35231hU r42 = C35231hU.this;
                                            C15250mo r32 = r42.A0G;
                                            r32.A06 = (C49662Lr) obj;
                                            boolean z = true;
                                            r32.A0G = true;
                                            r42.A0C.A0B(r42.A07());
                                            if (r32.A02 != 0) {
                                                z = false;
                                            }
                                            r42.A0A(z);
                                        }
                                    });
                                }

                                public AnonymousClass017 A00() {
                                    return this.A08;
                                }

                                public AnonymousClass017 A01() {
                                    return this.A02;
                                }

                                public AnonymousClass017 A02() {
                                    return this.A03;
                                }

                                public AnonymousClass017 A03() {
                                    return this.A0A;
                                }

                                public AnonymousClass017 A04() {
                                    return this.A0B;
                                }

                                public AnonymousClass017 A05() {
                                    return this.A0C;
                                }

                                public AnonymousClass017 A06() {
                                    return this.A05;
                                }

                                public final String A07() {
                                    List list;
                                    C15250mo r2 = this.A0G;
                                    String A01 = r2.A01();
                                    UserJid of = UserJid.of(r2.A04);
                                    if (of == null) {
                                        return A01;
                                    }
                                    C15370n3 A0B = this.A0D.A0B(of);
                                    C15610nY r8 = this.A0E;
                                    List A02 = r2.A02();
                                    AnonymousClass01T r0 = r2.A03;
                                    if (r0 == null || (list = (List) r0.A00) == null || list.isEmpty()) {
                                        Object obj = r2.A00().A00;
                                        AnonymousClass009.A05(obj);
                                        list = (List) obj;
                                    }
                                    boolean z = false;
                                    if (list.size() % 2 == 0) {
                                        z = true;
                                    }
                                    AnonymousClass009.A0B("tokenPositions.size must be even", z);
                                    Iterator it = list.iterator();
                                    ArrayList arrayList = new ArrayList();
                                    arrayList.add(0);
                                    for (Object obj2 : A02) {
                                        Object next = it.next();
                                        Object next2 = it.next();
                                        if (r8.A0M(A0B, Collections.singletonList(obj2), true)) {
                                            arrayList.add(next);
                                            arrayList.add(next2);
                                        }
                                    }
                                    arrayList.add(Integer.valueOf(A01.length()));
                                    Iterator it2 = arrayList.iterator();
                                    ArrayList arrayList2 = new ArrayList();
                                    while (it2.hasNext()) {
                                        Number number = (Number) it2.next();
                                        Number number2 = (Number) it2.next();
                                        if (number == null || number2 == null) {
                                            Log.e("messageSearchModel/invalid index in token mapping");
                                            break;
                                        } else if (!number.equals(number2)) {
                                            String trim = A01.subSequence(number.intValue(), number2.intValue()).toString().trim();
                                            if (trim.length() > 0) {
                                                arrayList2.add(trim);
                                            }
                                        }
                                    }
                                    return TextUtils.join(" ", arrayList2);
                                }

                                public void A08() {
                                    Pair pair;
                                    if (!this.A01.get()) {
                                        AnonymousClass02P r3 = this.A06;
                                        if (r3.A01() != null) {
                                            Object obj = ((Pair) r3.A01()).first;
                                            Number number = (Number) ((Pair) r3.A01()).second;
                                            if (number != null) {
                                                AnonymousClass016 r1 = this.A09;
                                                if (r1.A01() != null && ((Number) r1.A01()).intValue() != -1) {
                                                    pair = new Pair(obj, Integer.valueOf(number.intValue() + 1));
                                                } else if (Boolean.TRUE.equals(obj)) {
                                                    pair = new Pair(Boolean.FALSE, 0);
                                                } else {
                                                    return;
                                                }
                                                r3.A0B(pair);
                                            }
                                        }
                                    }
                                }

                                public void A09() {
                                    A0A(true);
                                }

                                public void A0A(boolean z) {
                                    AnonymousClass016 r0 = this.A0B;
                                    Boolean bool = Boolean.TRUE;
                                    r0.A0B(bool);
                                    this.A05.A0B(new C35531iB());
                                    AnonymousClass02P r2 = this.A06;
                                    if (!z) {
                                        bool = null;
                                    }
                                    r2.A0B(new Pair(bool, 0));
                                    this.A07.A0B(this.A0H);
                                    this.A0G.A0F = true;
                                }
                            }
