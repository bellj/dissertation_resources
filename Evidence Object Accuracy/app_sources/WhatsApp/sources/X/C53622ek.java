package X;

import android.os.Bundle;
import android.view.View;
import android.view.accessibility.AccessibilityNodeInfo;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.2ek  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53622ek extends AnonymousClass04v {
    public static final Map A05;
    public static final Map A06;
    public static final Map A07;
    public static final Map A08;
    public int A00 = 1056964608;
    public final C14260l7 A01;
    public final AnonymousClass28D A02;
    public final AnonymousClass28D A03;
    public final Map A04;

    static {
        HashMap A11 = C12970iu.A11();
        A11.put("button", "android.widget.Button");
        A11.put("checkbox", "android.widget.CompoundButton");
        A11.put("checked_text_view", "android.widget.CheckedTextView");
        A11.put("drop_down_list", "android.widget.Spinner");
        A11.put("edit_text", "android.widget.EditText");
        A11.put("grid", "android.widget.GridView");
        A11.put("image", "android.widget.ImageView");
        A11.put("list", "android.widget.AbsListView");
        A11.put("pager", "androidx.viewpager.widget.ViewPager");
        A11.put("radio_button", "android.widget.RadioButton");
        A11.put("seek_control", "android.widget.SeekBar");
        A11.put("switch", "android.widget.Switch");
        A11.put("tab_bar", "android.widget.TabWidget");
        A11.put("toggle_button", "android.widget.ToggleButton");
        A11.put("view_group", "android.view.ViewGroup");
        A11.put("web_view", "android.webkit.WebView");
        A11.put("progress_bar", "android.widget.ProgressBar");
        A11.put("action_bar_tab", "android.app.ActionBar$Tab");
        A11.put("drawer_layout", "androidx.drawerlayout.widget.DrawerLayout");
        A11.put("sliding_drawer", "android.widget.SlidingDrawer");
        A11.put("icon_menu", "com.android.internal.view.menu.IconMenuView");
        A11.put("toast", "android.widget.Toast$TN");
        A11.put("alert_dialog", "android.app.AlertDialog");
        A11.put("date_picker_dialog", "android.app.DatePickerDialog");
        A11.put("time_picker_dialog", "android.app.TimePickerDialog");
        A11.put("date_picker", "android.widget.DatePicker");
        A11.put("time_picker", "android.widget.TimePicker");
        A11.put("number_picker", "android.widget.NumberPicker");
        A11.put("scroll_view", "android.widget.ScrollView");
        A11.put("horizontal_scroll_view", "android.widget.HorizontalScrollView");
        A11.put("keyboard_key", "android.inputmethodservice.Keyboard$Key");
        A11.put("none", "");
        A08 = Collections.unmodifiableMap(A11);
        HashMap A112 = C12970iu.A11();
        A112.put("click", A00(C007804a.A05));
        A112.put("long_click", A00(C007804a.A0F));
        A112.put("scroll_forward", A00(C007804a.A0T));
        A112.put("scroll_backward", A00(C007804a.A0R));
        A112.put("expand", A00(C007804a.A0B));
        A112.put("collapse", A00(C007804a.A06));
        A112.put("dismiss", A00(C007804a.A0A));
        A112.put("scroll_up", A00(C007804a.A0X));
        A112.put("scroll_left", A00(C007804a.A0U));
        A112.put("scroll_down", A00(C007804a.A0S));
        A112.put("scroll_right", A00(C007804a.A0V));
        A112.put("custom", -1);
        A05 = Collections.unmodifiableMap(A112);
        HashMap A113 = C12970iu.A11();
        Integer A0g = C12970iu.A0g();
        A113.put("percent", A0g);
        Integer A0V = C12960it.A0V();
        A113.put("float", A0V);
        Integer A0i = C12980iv.A0i();
        A113.put("int", A0i);
        A07 = Collections.unmodifiableMap(A113);
        HashMap A114 = C12970iu.A11();
        A114.put("none", A0i);
        A114.put("single", A0V);
        A114.put("multiple", A0g);
        A06 = Collections.unmodifiableMap(A114);
    }

    public C53622ek(C14260l7 r9, AnonymousClass28D r10, AnonymousClass28D r11) {
        this.A02 = r10;
        this.A03 = r11;
        this.A01 = r9;
        HashMap A11 = C12970iu.A11();
        List<AnonymousClass28D> A0L = r10.A0L(55);
        if (A0L != null && !A0L.isEmpty()) {
            for (AnonymousClass28D r2 : A0L) {
                String A062 = AnonymousClass28D.A06(r2);
                String A0I = r2.A0I(36);
                AbstractC14200l1 A0G = r2.A0G(38);
                if (A062 != null) {
                    Map map = A05;
                    if (map.containsKey(A062)) {
                        int A052 = C12960it.A05(map.get(A062));
                        if (map.containsKey("custom") && A052 == C12960it.A05(map.get("custom"))) {
                            A052 = this.A00;
                            this.A00 = A052 + 1;
                        }
                        A11.put(Integer.valueOf(A052), new C90734Pa(A0G, A0I, A052));
                    }
                }
            }
        }
        this.A04 = A11;
    }

    public static Integer A00(C007804a r0) {
        AnonymousClass4D3.A00(r0);
        return Integer.valueOf(r0.A00());
    }

    @Override // X.AnonymousClass04v
    public boolean A03(View view, int i, Bundle bundle) {
        AbstractC14200l1 r3;
        C90734Pa r0 = (C90734Pa) C12990iw.A0l(this.A04, i);
        if (r0 == null || (r3 = r0.A01) == null) {
            return super.A03(view, i, bundle);
        }
        AnonymousClass28D r02 = this.A03;
        return C64983Hr.A02(C14250l6.A00(C14230l4.A00(this.A01, r02.A06), C14210l2.A02(r02), r3));
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r25) {
        Number number;
        Number number2;
        super.A06(view, r25);
        AnonymousClass28D r2 = this.A02;
        boolean A0O = r2.A0O(41, false);
        boolean A0O2 = r2.A0O(49, false);
        boolean A0O3 = r2.A0O(51, false);
        boolean A0O4 = r2.A0O(36, false);
        CharSequence A0I = r2.A0I(50);
        String A0I2 = r2.A0I(45);
        CharSequence A0I3 = r2.A0I(46);
        CharSequence A0I4 = r2.A0I(58);
        String A0I5 = r2.A0I(57);
        AnonymousClass28D A0F = r2.A0F(52);
        AnonymousClass28D A0F2 = r2.A0F(53);
        AnonymousClass28D A0F3 = r2.A0F(54);
        if (A0F != null) {
            String A0I6 = A0F.A0I(40);
            float A09 = A0F.A09(38, -1.0f);
            float A092 = A0F.A09(36, -1.0f);
            float A093 = A0F.A09(35, -1.0f);
            if (A09 >= 0.0f && A093 >= 0.0f && A092 >= 0.0f && (number2 = (Number) A07.get(A0I6)) != null) {
                r25.A0B(AnonymousClass0RX.A00(A09, A092, A093, number2.intValue()));
            }
        }
        if (A0F2 != null) {
            int A0B = A0F2.A0B(35, -1);
            int A0B2 = A0F2.A0B(38, -1);
            boolean A0O5 = A0F2.A0O(36, false);
            String A0J = A0F2.A0J(40, "none");
            if (A0B >= -1 && A0B2 >= -1 && (number = (Number) A06.get(A0J)) != null) {
                r25.A0I(AnonymousClass0TY.A01(A0B2, A0B, number.intValue(), A0O5));
            }
        }
        if (A0F3 != null) {
            int A0B3 = A0F3.A0B(35, -1);
            int A0B4 = A0F3.A0B(38, -1);
            int A0B5 = A0F3.A0B(36, -1);
            int A0B6 = A0F3.A0B(40, -1);
            if (A0B3 >= 0 && A0B4 >= 0 && A0B5 >= 0 && A0B6 >= 0) {
                r25.A0J(C06410Tm.A01(A0B4, A0B6, A0B3, A0B5, A0O, A0O2));
            }
        }
        Iterator A0o = C12960it.A0o(this.A04);
        while (A0o.hasNext()) {
            C90734Pa r11 = (C90734Pa) A0o.next();
            int i = r11.A00;
            Map map = A05;
            if (map.containsKey("click") && i == C12960it.A05(map.get("click"))) {
                r25.A02.setClickable(true);
            } else if (map.containsKey("long_click") && i == C12960it.A05(map.get("long_click"))) {
                r25.A02.setLongClickable(true);
            }
            String str = r11.A02;
            if (str != null) {
                r25.A09(new C007804a(i, str));
            } else {
                r25.A02.addAction(i);
            }
        }
        if (A0O3) {
            AccessibilityNodeInfo accessibilityNodeInfo = r25.A02;
            accessibilityNodeInfo.setCheckable(true);
            accessibilityNodeInfo.setChecked(A0O4);
        }
        if (A0I != null) {
            r25.A0H(A0I);
        }
        if (A0I2 != null && !A0I2.equals("none")) {
            Map map2 = A08;
            if (map2.containsKey(A0I2)) {
                r25.A02.setClassName((CharSequence) map2.get(A0I2));
            }
        }
        if (A0I3 != null) {
            r25.A0F(A0I3);
        }
        if (A0I4 != null) {
            r25.A0G(A0I4);
        }
        if (A0I5 != null && !A0I5.isEmpty()) {
            r25.A05();
            r25.A0C(A0I5);
        }
    }
}
