package X;

import com.google.common.collect.Multisets;
import java.util.Iterator;

/* renamed from: X.3ti  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C81253ti extends Multisets.EntrySet<E> {
    public final /* synthetic */ AbstractC113525Hx this$0;

    public C81253ti() {
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public C81253ti(AbstractC113525Hx r1) {
        this();
        this.this$0 = r1;
    }

    public void clear() {
        multiset().clear();
    }

    public boolean contains(Object obj) {
        if (!(obj instanceof AnonymousClass4Y5)) {
            return false;
        }
        AnonymousClass4Y5 r4 = (AnonymousClass4Y5) obj;
        if (r4.getCount() <= 0 || multiset().count(r4.getElement()) != r4.getCount()) {
            return false;
        }
        return true;
    }

    public Iterator iterator() {
        return this.this$0.entryIterator();
    }

    public AnonymousClass5Z2 multiset() {
        return this.this$0;
    }

    public boolean remove(Object obj) {
        if (obj instanceof AnonymousClass4Y5) {
            AnonymousClass4Y5 r5 = (AnonymousClass4Y5) obj;
            Object element = r5.getElement();
            int count = r5.getCount();
            if (count != 0) {
                return multiset().setCount(element, count, 0);
            }
        }
        return false;
    }

    public int size() {
        return this.this$0.distinctElements();
    }
}
