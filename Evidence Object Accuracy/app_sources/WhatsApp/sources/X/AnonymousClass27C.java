package X;

/* renamed from: X.27C  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass27C implements AnonymousClass27D {
    @Override // X.AnonymousClass27D
    public byte[] A7n(byte[] bArr, int i, int i2) {
        byte[] bArr2 = new byte[i2];
        System.arraycopy(bArr, i, bArr2, 0, i2);
        return bArr2;
    }
}
