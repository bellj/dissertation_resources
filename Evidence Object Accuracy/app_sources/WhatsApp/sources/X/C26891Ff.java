package X;

import android.content.SharedPreferences;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/* renamed from: X.1Ff  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26891Ff {
    public C44281ye A00;
    public final C14330lG A01;
    public final C14830m7 A02;
    public final C14820m6 A03;
    public final C20330va A04;

    public C26891Ff(C14330lG r1, C14830m7 r2, C14820m6 r3, C20330va r4) {
        this.A02 = r2;
        this.A01 = r1;
        this.A04 = r4;
        this.A03 = r3;
    }

    public synchronized int A00() {
        return this.A03.A00.getInt("business_activity_report_state", 0);
    }

    public synchronized C44281ye A01() {
        C44281ye r5;
        r5 = this.A00;
        if (r5 == null) {
            SharedPreferences sharedPreferences = this.A03.A00;
            String string = sharedPreferences.getString("business_activity_report_url", null);
            if (TextUtils.isEmpty(string)) {
                r5 = null;
            } else {
                r5 = new C44281ye(string, sharedPreferences.getString("business_activity_report_direct_url", null), sharedPreferences.getString("business_activity_report_name", null), sharedPreferences.getString("business_activity_report_media_key", null), sharedPreferences.getString("business_activity_report_file_sha", null), sharedPreferences.getString("business_activity_report_file_enc_sha", null), sharedPreferences.getLong("business_activity_report_size", 0), sharedPreferences.getLong("business_activity_report_timestamp", -1), sharedPreferences.getLong("business_activity_report_expiration_timestamp", 0));
                this.A00 = r5;
            }
        }
        return r5;
    }

    public synchronized void A02() {
        Log.i("BusinessActivityReportManager/reset");
        this.A00 = null;
        C14330lG r2 = this.A01;
        File A05 = r2.A05();
        if (A05.exists() && !A05.delete()) {
            Log.e("BusinessActivityReportManager/reset/failed-delete-report-file");
        }
        C14350lI.A0D(r2.A07(), 0);
        this.A03.A0K();
    }

    public synchronized void A03(C44281ye r7) {
        this.A00 = r7;
        C14820m6 r3 = this.A03;
        String str = r7.A08;
        SharedPreferences sharedPreferences = r3.A00;
        sharedPreferences.edit().putString("business_activity_report_url", str).apply();
        sharedPreferences.edit().putString("business_activity_report_name", r7.A06).apply();
        sharedPreferences.edit().putLong("business_activity_report_size", r7.A02).apply();
        sharedPreferences.edit().putLong("business_activity_report_expiration_timestamp", r7.A01).apply();
        sharedPreferences.edit().putString("business_activity_report_direct_url", r7.A03).apply();
        sharedPreferences.edit().putString("business_activity_report_media_key", r7.A07).apply();
        sharedPreferences.edit().putString("business_activity_report_file_sha", r7.A05).apply();
        sharedPreferences.edit().putString("business_activity_report_file_enc_sha", r7.A04).apply();
        r3.A0q("business_activity_report_timestamp", r7.A00);
        r3.A0O(2);
    }

    public synchronized void A04(AbstractC44311yh r5, String str) {
        FileInputStream fileInputStream;
        C14330lG r3 = this.A01;
        C14350lI.A0D(r3.A07(), 0);
        File A05 = r3.A05();
        File A0I = r3.A0I(str);
        try {
            fileInputStream = new FileInputStream(A05);
        } catch (IOException e) {
            Log.e("BusinessActivityReportManager/prepare-report-for-export/can't prepare report file", e);
        }
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(A0I);
            C14350lI.A0G(fileInputStream, fileOutputStream);
            fileOutputStream.close();
            fileInputStream.close();
            if (!A0I.setLastModified(this.A02.A00())) {
                Log.e("BusinessActivityReportManager/prepare-report-for-export/failed to update report file");
                r5.APk();
            } else {
                r5.AUd(str);
            }
        } catch (Throwable th) {
            try {
                fileInputStream.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
