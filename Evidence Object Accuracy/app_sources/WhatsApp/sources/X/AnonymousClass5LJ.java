package X;

/* renamed from: X.5LJ  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5LJ extends AbstractRunnableC75993ks {
    public final Runnable A00;

    public AnonymousClass5LJ(Runnable runnable, C89714La r2, long j) {
        super(r2, j);
        this.A00 = runnable;
    }

    @Override // java.lang.Runnable
    public void run() {
        this.A00.run();
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("Task[");
        A0k.append(C72453ed.A0p(this.A00, A0k));
        A0k.append(", ");
        A0k.append(super.A00);
        A0k.append(", ");
        A0k.append(this.A01);
        return C72453ed.A0t(A0k);
    }
}
