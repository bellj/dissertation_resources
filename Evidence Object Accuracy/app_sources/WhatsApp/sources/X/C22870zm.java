package X;

import android.content.Context;
import com.facebook.msys.mci.Execution;
import com.facebook.msys.mci.FileManager;
import com.facebook.msys.mci.JsonSerialization;
import com.facebook.msys.mci.NetworkSession;
import com.facebook.msys.mci.NotificationCenter;
import com.facebook.msys.mci.Proxies;
import com.facebook.msys.mci.ProxyProvider;
import com.facebook.simplejni.NativeHolder;
import com.whatsapp.util.Log;
import com.whatsapp.wamsys.JniBridge;

/* renamed from: X.0zm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22870zm {
    public boolean A00;
    public boolean A01;
    public boolean A02;
    public boolean A03;
    public final C21050wm A04;
    public final JniBridge A05;
    public final AnonymousClass159 A06;
    public final AnonymousClass158 A07;

    public C22870zm(C21050wm r1, JniBridge jniBridge, AnonymousClass159 r3, AnonymousClass158 r4) {
        this.A05 = jniBridge;
        this.A04 = r1;
        this.A06 = r3;
        this.A07 = r4;
    }

    public synchronized void A00() {
        if (this.A02) {
            JniBridge jniBridge = this.A05;
            JniBridge.jvidispatchI();
            JniBridge.jvidispatchIO(0, (NativeHolder) jniBridge.wajContext.get());
            this.A02 = false;
        }
    }

    public synchronized void A01(Context context) {
        if (!this.A00) {
            Log.i("WaMsysSetup/bootstrap");
            com.facebook.msys.mci.Log.registerLogger(new C37521mX());
            Proxies.configure(new ProxyProvider(new C37551mb(new C37531mY(this), new C37541ma(this))));
            Execution.initialize();
            FileManager.initialize(context.getCacheDir());
            NativeHolder nativeHolder = (NativeHolder) this.A05.wajContext.getAndSet((NativeHolder) JniBridge.jvidispatchO(0));
            if (nativeHolder != null) {
                nativeHolder.release();
            }
            this.A00 = true;
        }
    }

    public final synchronized void A02(Context context, C18790t3 r11, C18800t4 r12, C19930uu r13, AbstractC14440lR r14) {
        NotificationCenter notificationCenter;
        if (!this.A03) {
            AnonymousClass158 r3 = this.A07;
            NotificationCenter notificationCenter2 = new NotificationCenter(false);
            synchronized (r3) {
                AnonymousClass009.A05(notificationCenter2);
                r3.A00 = notificationCenter2;
            }
            String A00 = r13.A00();
            synchronized (r3) {
                notificationCenter = r3.A00;
                AnonymousClass009.A05(notificationCenter);
            }
            NetworkSession networkSession = new NetworkSession(A00, notificationCenter, new C37561md(r11, r12, r13, r14, context.getCacheDir()));
            AnonymousClass159 r1 = this.A06;
            synchronized (r1) {
                AnonymousClass009.A05(networkSession);
                r1.A00 = networkSession;
            }
            this.A03 = true;
        }
    }

    public synchronized void A03(C18790t3 r15, C16590pI r16, C14820m6 r17, AnonymousClass018 r18, C18800t4 r19, C19930uu r20, AbstractC14440lR r21, String str) {
        NetworkSession networkSession;
        if (!this.A02) {
            Log.i("WaMsysSetup/bootstrapForReg");
            Context context = r16.A00;
            A01(context);
            A02(context, r15, r19, r20, r21);
            JniBridge jniBridge = this.A05;
            AnonymousClass159 r2 = this.A06;
            synchronized (r2) {
                networkSession = r2.A00;
                AnonymousClass009.A05(networkSession);
            }
            if (0 != JniBridge.jvidispatchIOO(networkSession, (NativeHolder) jniBridge.wajContext.get())) {
                JniBridge.jvidispatchIIOOOOO((long) 0, r18.A06(), r18.A05(), str, r17.A0A(), (NativeHolder) jniBridge.wajContext.get());
                JsonSerialization.initialize();
                this.A02 = true;
            } else {
                throw new IllegalStateException("wa-msys/failed to initialize WCRManager");
            }
        }
    }
}
