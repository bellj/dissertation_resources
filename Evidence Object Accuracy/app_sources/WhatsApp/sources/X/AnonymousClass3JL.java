package X;

import android.accounts.Account;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.auth.TokenData;
import java.io.IOException;
import java.util.Locale;

/* renamed from: X.3JL  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3JL {
    public static final ComponentName A00 = new ComponentName("com.google.android.gms", "com.google.android.gms.auth.GetToken");
    public static final C63703Cp A01 = new C63703Cp("Auth", "GoogleAuthUtil");
    public static final String[] A02 = {"com.google", "com.google.work", "cn.google"};

    public static Object A00(ComponentName componentName, Context context, AnonymousClass5SW r11) {
        ServiceConnectionC97784hd r6 = new ServiceConnectionC97784hd();
        C94924cl A002 = C94924cl.A00(context);
        try {
            if (A002.A02(r6, new C65083Ib(componentName), "GoogleAuthUtil")) {
                try {
                    C13020j0.A06("BlockingServiceConnection.getService() called on main thread");
                    if (!r6.A00) {
                        r6.A00 = true;
                        return r11.Ah8((IBinder) r6.A01.take());
                    }
                    throw C12960it.A0U("Cannot call get on this connection more than once");
                } catch (RemoteException | InterruptedException e) {
                    C63703Cp r3 = A01;
                    Object[] A1a = C12980iv.A1a();
                    C12970iu.A1U("Error on service connection.", e, A1a);
                    Log.i("Auth", r3.A03.concat(String.format(Locale.US, "GoogleAuthUtil", A1a)));
                    throw new IOException("Error on service connection.", e);
                }
            } else {
                throw C12990iw.A0i("Could not bind to service.");
            }
        } finally {
            A002.A01(r6, new C65083Ib(componentName));
        }
    }

    public static String A01(Account account, Context context) {
        Bundle A0D = C12970iu.A0D();
        A02(account);
        C13020j0.A06("Calling this from your main thread can lead to deadlock");
        C13020j0.A07("oauth2:https://www.googleapis.com/auth/drive.appdata", "Scope cannot be empty or null.");
        A02(account);
        A03(context);
        Bundle bundle = new Bundle(A0D);
        String str = context.getApplicationInfo().packageName;
        bundle.putString("clientPackageName", str);
        if (TextUtils.isEmpty(bundle.getString("androidPackageName"))) {
            bundle.putString("androidPackageName", str);
        }
        bundle.putLong("service_connection_start_time_millis", SystemClock.elapsedRealtime());
        return ((TokenData) A00(A00, context, new C108144yZ(account, bundle))).A03;
    }

    public static void A02(Account account) {
        if (!TextUtils.isEmpty(account.name)) {
            for (String str : A02) {
                if (str.equals(account.type)) {
                    return;
                }
            }
            throw C12970iu.A0f("Account type not supported");
        }
        throw C12970iu.A0f("Account name cannot be empty!");
    }

    public static void A03(Context context) {
        try {
            C472329r.A01(context.getApplicationContext(), 8400000);
        } catch (AnonymousClass29w e) {
            throw new AnonymousClass4CA(e.getMessage());
        } catch (AnonymousClass29x e2) {
            int i = e2.zza;
            throw new C77443nI(new Intent(((C472829y) e2).zza), e2.getMessage(), i);
        }
    }

    public static void A04(Context context, String str) {
        C13020j0.A06("Calling this from your main thread can lead to deadlock");
        A03(context);
        Bundle A0D = C12970iu.A0D();
        String str2 = context.getApplicationInfo().packageName;
        A0D.putString("clientPackageName", str2);
        if (!A0D.containsKey("androidPackageName")) {
            A0D.putString("androidPackageName", str2);
        }
        A00(A00, context, new C108134yY(str, A0D));
    }

    public static /* synthetic */ void A05(Object obj) {
        if (obj == null) {
            C63703Cp r4 = A01;
            Object[] A1b = C12970iu.A1b();
            A1b[0] = "Binder call returned null.";
            Log.w("Auth", r4.A03.concat(String.format(Locale.US, "GoogleAuthUtil", A1b)));
            throw C12990iw.A0i("Service unavailable.");
        }
    }
}
