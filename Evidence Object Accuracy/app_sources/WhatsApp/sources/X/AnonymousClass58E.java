package X;

import java.util.Collection;
import java.util.List;

/* renamed from: X.58E  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass58E implements AnonymousClass5UW {
    public static final AnonymousClass58P A01 = new AnonymousClass58P();
    public final List A00;

    public AnonymousClass58E(List list) {
        this.A00 = list;
    }

    @Override // X.AnonymousClass5UW
    public boolean A9g(AnonymousClass4V4 r4) {
        C16700pc.A0E(r4, 0);
        List<AnonymousClass5UW> list = this.A00;
        if (!(!list.isEmpty())) {
            return false;
        }
        if (!(list instanceof Collection) || !list.isEmpty()) {
            for (AnonymousClass5UW r0 : list) {
                if (!r0.A9g(r4)) {
                    return false;
                }
            }
        }
        return true;
    }
}
