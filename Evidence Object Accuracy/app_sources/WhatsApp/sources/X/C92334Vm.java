package X;

import android.view.View;

/* renamed from: X.4Vm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92334Vm {
    public int A00;
    public int A01;
    public int A02;
    public final View A03;

    public C92334Vm(View view) {
        this.A03 = view;
    }

    public final void A00() {
        View view = this.A03;
        AnonymousClass028.A0Y(view, this.A02 - (view.getTop() - this.A01));
        AnonymousClass028.A0X(view, 0 - (view.getLeft() - this.A00));
    }
}
