package X;

import android.content.Context;
import android.graphics.Canvas;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.notification.PopupNotification;

/* renamed from: X.2y4  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2y4 extends C60752yZ {
    public boolean A00;
    public final /* synthetic */ PopupNotification A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2y4(Context context, PopupNotification popupNotification, C30421Xi r9) {
        super(context, null, popupNotification.A0d, popupNotification.A0e, r9);
        this.A01 = popupNotification;
        A0Z();
    }

    @Override // X.AbstractC60572yF, X.AbstractC42681ve, X.AnonymousClass1OZ, X.AbstractC28561Ob
    public void A0Z() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2P6 A07 = AnonymousClass1OY.A07(this);
            AnonymousClass01J A08 = AnonymousClass1OY.A08(A07, this);
            AnonymousClass1OY.A0L(A08, this);
            AnonymousClass1OY.A0M(A08, this);
            AnonymousClass1OY.A0K(A08, this);
            AnonymousClass1OY.A0I(A07, A08, this, AnonymousClass1OY.A09(A08, this, AnonymousClass1OY.A0B(A08, this)));
            AnonymousClass1OY.A0O(A08, this);
            AnonymousClass1OY.A0N(A08, this);
        }
    }

    @Override // X.AbstractC28551Oa, android.view.View
    public void onDraw(Canvas canvas) {
        View view = ((C60752yZ) this).A04;
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams != null) {
            layoutParams.width = C12960it.A09(this).getDimensionPixelSize(R.dimen.popup_notification_audio_width);
            view.setLayoutParams(layoutParams);
        }
        view.setBackground(C12970iu.A0C(view.getContext(), R.drawable.balloon_centered_normal));
        ((AbstractC28551Oa) this).A0Q = false;
        super.onDraw(canvas);
    }
}
