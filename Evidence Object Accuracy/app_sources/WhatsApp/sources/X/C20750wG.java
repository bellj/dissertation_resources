package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0201000_I0;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.0wG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20750wG {
    public long A00;
    public final AbstractC15710nm A01;
    public final C14900mE A02;
    public final C15570nT A03;
    public final C18790t3 A04;
    public final C20670w8 A05;
    public final C16240og A06;
    public final AnonymousClass10V A07;
    public final C14830m7 A08;
    public final C16590pI A09;
    public final C14850m9 A0A;
    public final C22050yP A0B;
    public final C17080qE A0C;
    public final C17220qS A0D;
    public final C18800t4 A0E;
    public final C20700wB A0F;
    public final C19930uu A0G;
    public final AbstractC14440lR A0H;

    public C20750wG(AbstractC15710nm r2, C14900mE r3, C15570nT r4, C18790t3 r5, C20670w8 r6, C16240og r7, AnonymousClass10V r8, C14830m7 r9, C16590pI r10, C14850m9 r11, C22050yP r12, C17080qE r13, C17220qS r14, C18800t4 r15, C20700wB r16, C19930uu r17, AbstractC14440lR r18) {
        this.A09 = r10;
        this.A08 = r9;
        this.A0A = r11;
        this.A02 = r3;
        this.A01 = r2;
        this.A03 = r4;
        this.A0G = r17;
        this.A0H = r18;
        this.A04 = r5;
        this.A0D = r14;
        this.A0F = r16;
        this.A06 = r7;
        this.A0B = r12;
        this.A0E = r15;
        this.A05 = r6;
        this.A07 = r8;
        this.A0C = r13;
    }

    public void A00(AbstractC14640lm r5, int i) {
        if (i == 500 || i == 501 || i == 503) {
            this.A00 = this.A08.A00() + 3600000;
        } else if (i == 401 && r5 != null && !C15380n4.A0J(r5)) {
            this.A07.A02(r5);
        }
    }

    public void A01(AbstractC14640lm r11, C37241lo r12, int i, int i2) {
        if (r11 != null && r11.isProtocolCompliant() && !C15380n4.A0F(r11)) {
            C14850m9 r1 = this.A0A;
            if (C41861uH.A00(r1, r11)) {
                return;
            }
            if ((r11 instanceof UserJid) && r1.A02(609) >= 1) {
                A03(r12, Collections.singleton(r11), i2);
            } else if (this.A00 < this.A08.A00()) {
                C20700wB r13 = this.A0F;
                if (((i2 & 1) != 0 && r13.A01.A02(r11)) || ((i2 & 2) != 0 && r13.A02.A02(r11))) {
                    this.A02.A0H(new Runnable(r11, this, r12, i, i2) { // from class: X.1uI
                        public final /* synthetic */ int A00;
                        public final /* synthetic */ int A01;
                        public final /* synthetic */ AbstractC14640lm A02;
                        public final /* synthetic */ C20750wG A03;
                        public final /* synthetic */ C37241lo A04;

                        {
                            this.A03 = r2;
                            this.A00 = r4;
                            this.A01 = r5;
                            this.A02 = r1;
                            this.A04 = r3;
                        }

                        @Override // java.lang.Runnable
                        public final void run() {
                            String str;
                            String str2;
                            C20750wG r7 = this.A03;
                            int i3 = this.A00;
                            int i4 = this.A01;
                            AbstractC14640lm r14 = this.A02;
                            C37241lo r8 = this.A04;
                            StringBuilder sb = new StringBuilder("ProfilePhotoManager/sendGetProfilePhoto photoId:");
                            sb.append(i3);
                            sb.append(" type:");
                            sb.append(i4);
                            sb.append(" jid:");
                            sb.append(r14);
                            Log.i(sb.toString());
                            C14850m9 r132 = r7.A0A;
                            C17220qS r15 = r7.A0D;
                            if (i4 == 1 || i4 != 2) {
                                str = "image";
                            } else {
                                str = "preview";
                            }
                            if (i3 > 0) {
                                str2 = Integer.toString(i3);
                            } else {
                                str2 = null;
                            }
                            C16590pI r4 = r7.A09;
                            C19930uu r112 = r7.A0G;
                            AbstractC14440lR r122 = r7.A0H;
                            AnonymousClass3ZO r123 = new AnonymousClass3ZO(r132, r14, r15, new C69423Ze(r7.A04, r7.A07, r4, r7.A0B, r7.A0C, r7, r8, r7.A0E, r7.A0F, r112, r122), str, str2);
                            C17220qS r113 = r123.A03;
                            String A01 = r113.A01();
                            AbstractC14640lm r72 = r123.A02;
                            String str3 = r123.A06;
                            boolean equals = "image".equals(str3);
                            ArrayList arrayList = new ArrayList();
                            if (equals || r123.A00()) {
                                arrayList.add(new AnonymousClass1W9("query", "url"));
                            }
                            String str4 = r123.A05;
                            if (str4 != null) {
                                arrayList.add(new AnonymousClass1W9("id", str4));
                            }
                            arrayList.add(new AnonymousClass1W9("type", str3));
                            r113.A0A(r123, new AnonymousClass1V8(new AnonymousClass1V8("picture", (AnonymousClass1W9[]) arrayList.toArray(new AnonymousClass1W9[0])), "iq", new AnonymousClass1W9[]{new AnonymousClass1W9("id", A01), new AnonymousClass1W9("xmlns", "w:profile:picture"), new AnonymousClass1W9(AnonymousClass1VY.A00, "to"), new AnonymousClass1W9(r72, "target"), new AnonymousClass1W9("type", "get")}), A01, 26, 0);
                        }
                    });
                }
            }
        }
    }

    public void A02(C27611If r15) {
        String A01;
        AbstractC14640lm r5 = r15.A0I;
        if (r5 != null && r5.isProtocolCompliant() && this.A06.A06 && this.A00 < this.A08.A00()) {
            Log.w("app/sendSetProfilePhoto");
            C41881uJ r8 = new C41881uJ(r5, this.A0D, r15, r15.A0K);
            byte[] bArr = r15.A05;
            boolean A0F = this.A03.A0F(r5);
            boolean z = false;
            if (bArr == null) {
                z = true;
            }
            r8.A00 = z;
            AnonymousClass1P4 r6 = r8.A04;
            if (r6 != null) {
                A01 = r6.A01;
            } else {
                A01 = r8.A02.A01();
            }
            C41141sy r3 = new C41141sy("iq");
            r3.A04(new AnonymousClass1W9("id", A01));
            r3.A04(new AnonymousClass1W9("xmlns", "w:profile:picture"));
            r3.A04(new AnonymousClass1W9(AnonymousClass1VY.A00, "to"));
            r3.A04(new AnonymousClass1W9("type", "set"));
            if (!A0F) {
                r3.A04(new AnonymousClass1W9(r8.A01, "target"));
            }
            if (r6 != null) {
                r3.A04(new AnonymousClass1W9("web", r6.A00));
            }
            C41141sy r2 = new C41141sy("picture");
            r2.A04(new AnonymousClass1W9("type", "image"));
            r2.A01 = bArr;
            r3.A05(r2.A03());
            r8.A02.A0D(r8, r3.A03(), A01, 25, 0);
        }
    }

    public final void A03(C37241lo r6, Set set, int i) {
        HashSet hashSet = new HashSet();
        for (Object obj : set) {
            C20700wB r1 = this.A0F;
            if (((i & 1) != 0 && r1.A01.A02(obj)) || ((i & 2) != 0 && r1.A02.A02(obj))) {
                hashSet.add(obj);
            }
        }
        if (!hashSet.isEmpty()) {
            new RunnableBRunnable0Shape0S0201000_I0(this, hashSet, i, 25).run();
        }
        if (r6 != null) {
            r6.A00(2);
        }
    }
}
