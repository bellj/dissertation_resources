package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5lc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122535lc extends AbstractC118835cS {
    public final TextView A00;

    public C122535lc(View view) {
        super(view);
        this.A00 = C12960it.A0I(view, R.id.title);
    }

    @Override // X.AbstractC118835cS
    public void A08(AbstractC125975s7 r8, int i) {
        int dimension;
        C123265ms r82 = (C123265ms) r8;
        TextView textView = this.A00;
        int paddingLeft = textView.getPaddingLeft();
        int paddingRight = textView.getPaddingRight();
        int i2 = r82.A01;
        int i3 = 0;
        if (i2 == 0) {
            dimension = 0;
        } else {
            dimension = (int) this.A0H.getResources().getDimension(i2);
        }
        int i4 = r82.A00;
        if (i4 != 0) {
            i3 = (int) this.A0H.getResources().getDimension(i4);
        }
        textView.setPadding(paddingLeft, dimension, paddingRight, i3);
        textView.setText(r82.A04);
        textView.setGravity(r82.A03);
        textView.setLinksClickable(true);
        C12990iw.A1F(textView);
        C12960it.A0s(textView.getContext(), textView, r82.A02);
    }
}
