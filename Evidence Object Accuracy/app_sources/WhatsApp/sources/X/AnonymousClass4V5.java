package X;

import java.io.File;

/* renamed from: X.4V5  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4V5 {
    public final C16590pI A00;

    public AnonymousClass4V5(C16590pI r2) {
        C16700pc.A0E(r2, 1);
        this.A00 = r2;
    }

    public final C64843Hc A00() {
        return new C64843Hc(new File(this.A00.A00.getCacheDir(), "avatar_profile_photo_poses"), 1048576);
    }
}
