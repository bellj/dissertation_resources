package X;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.app.INotificationSideChannel;
import android.util.Log;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.02x  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class ServiceConnectionC006102x implements ServiceConnection, Handler.Callback {
    public Set A00 = new HashSet();
    public final Context A01;
    public final Handler A02;
    public final HandlerThread A03;
    public final Map A04 = new HashMap();

    public ServiceConnectionC006102x(Context context) {
        this.A01 = context;
        HandlerThread handlerThread = new HandlerThread("NotificationManagerCompat");
        this.A03 = handlerThread;
        handlerThread.start();
        this.A02 = new Handler(handlerThread.getLooper(), this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0059, code lost:
        if (r9.A03 != false) goto L_0x005b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A00(X.C04910Nn r9) {
        /*
        // Method dump skipped, instructions count: 322
        */
        throw new UnsupportedOperationException("Method not decompiled: X.ServiceConnectionC006102x.A00(X.0Nn):void");
    }

    @Override // android.os.Handler.Callback
    public boolean handleMessage(Message message) {
        Set set;
        INotificationSideChannel r1;
        int i = message.what;
        if (i == 0) {
            Object obj = message.obj;
            Context context = this.A01;
            String string = Settings.Secure.getString(context.getContentResolver(), "enabled_notification_listeners");
            synchronized (AnonymousClass02r.A05) {
                if (string != null) {
                    if (!string.equals(AnonymousClass02r.A03)) {
                        String[] split = string.split(":", -1);
                        int length = split.length;
                        HashSet hashSet = new HashSet(length);
                        for (String str : split) {
                            ComponentName unflattenFromString = ComponentName.unflattenFromString(str);
                            if (unflattenFromString != null) {
                                hashSet.add(unflattenFromString.getPackageName());
                            }
                        }
                        AnonymousClass02r.A04 = hashSet;
                        AnonymousClass02r.A03 = string;
                    }
                }
                set = AnonymousClass02r.A04;
            }
            if (!set.equals(this.A00)) {
                this.A00 = set;
                List<ResolveInfo> queryIntentServices = context.getPackageManager().queryIntentServices(new Intent().setAction("android.support.BIND_NOTIFICATION_SIDE_CHANNEL"), 0);
                HashSet hashSet2 = new HashSet();
                for (ResolveInfo resolveInfo : queryIntentServices) {
                    if (set.contains(resolveInfo.serviceInfo.packageName)) {
                        ServiceInfo serviceInfo = resolveInfo.serviceInfo;
                        ComponentName componentName = new ComponentName(serviceInfo.packageName, serviceInfo.name);
                        if (resolveInfo.serviceInfo.permission != null) {
                            StringBuilder sb = new StringBuilder("Permission present on component ");
                            sb.append(componentName);
                            sb.append(", not adding listener record.");
                            Log.w("NotifManCompat", sb.toString());
                        } else {
                            hashSet2.add(componentName);
                        }
                    }
                }
                Iterator it = hashSet2.iterator();
                while (it.hasNext()) {
                    ComponentName componentName2 = (ComponentName) it.next();
                    Map map = this.A04;
                    if (!map.containsKey(componentName2)) {
                        if (Log.isLoggable("NotifManCompat", 3)) {
                            StringBuilder sb2 = new StringBuilder("Adding listener record for ");
                            sb2.append(componentName2);
                            Log.d("NotifManCompat", sb2.toString());
                        }
                        map.put(componentName2, new C04910Nn(componentName2));
                    }
                }
                Iterator it2 = this.A04.entrySet().iterator();
                while (it2.hasNext()) {
                    Map.Entry entry = (Map.Entry) it2.next();
                    if (!hashSet2.contains(entry.getKey())) {
                        if (Log.isLoggable("NotifManCompat", 3)) {
                            StringBuilder sb3 = new StringBuilder("Removing listener record for ");
                            sb3.append(entry.getKey());
                            Log.d("NotifManCompat", sb3.toString());
                        }
                        C04910Nn r12 = (C04910Nn) entry.getValue();
                        if (r12.A03) {
                            context.unbindService(this);
                            r12.A03 = false;
                        }
                        r12.A01 = null;
                        it2.remove();
                    }
                }
            }
            for (C04910Nn r13 : this.A04.values()) {
                r13.A02.add(obj);
                A00(r13);
            }
        } else if (i == 1) {
            C04710Mt r0 = (C04710Mt) message.obj;
            ComponentName componentName3 = r0.A00;
            IBinder iBinder = r0.A01;
            C04910Nn r2 = (C04910Nn) this.A04.get(componentName3);
            if (r2 != null) {
                if (iBinder == null) {
                    r1 = null;
                } else {
                    IInterface queryLocalInterface = iBinder.queryLocalInterface("android.support.v4.app.INotificationSideChannel");
                    if (queryLocalInterface == null || !(queryLocalInterface instanceof INotificationSideChannel)) {
                        r1 = new C06900Vo(iBinder);
                    } else {
                        r1 = (INotificationSideChannel) queryLocalInterface;
                    }
                }
                r2.A01 = r1;
                r2.A00 = 0;
                A00(r2);
                return true;
            }
        } else if (i == 2) {
            C04910Nn r14 = (C04910Nn) this.A04.get(message.obj);
            if (r14 != null) {
                if (r14.A03) {
                    this.A01.unbindService(this);
                    r14.A03 = false;
                }
                r14.A01 = null;
            }
        } else if (i != 3) {
            return false;
        } else {
            C04910Nn r02 = (C04910Nn) this.A04.get(message.obj);
            if (r02 != null) {
                A00(r02);
                return true;
            }
        }
        return true;
    }

    @Override // android.content.ServiceConnection
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (Log.isLoggable("NotifManCompat", 3)) {
            StringBuilder sb = new StringBuilder("Connected to service ");
            sb.append(componentName);
            Log.d("NotifManCompat", sb.toString());
        }
        this.A02.obtainMessage(1, new C04710Mt(componentName, iBinder)).sendToTarget();
    }

    @Override // android.content.ServiceConnection
    public void onServiceDisconnected(ComponentName componentName) {
        if (Log.isLoggable("NotifManCompat", 3)) {
            StringBuilder sb = new StringBuilder("Disconnected from service ");
            sb.append(componentName);
            Log.d("NotifManCompat", sb.toString());
        }
        this.A02.obtainMessage(2, componentName).sendToTarget();
    }
}
