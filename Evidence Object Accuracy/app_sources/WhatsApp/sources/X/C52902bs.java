package X;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.FrameLayout;

/* renamed from: X.2bs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52902bs extends FrameLayout.LayoutParams {
    public float A00 = 0.5f;
    public int A01 = 0;

    public C52902bs(int i, int i2) {
        super(i, i2);
    }

    public C52902bs(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C50572Qb.A06);
        this.A01 = obtainStyledAttributes.getInt(0, 0);
        this.A00 = obtainStyledAttributes.getFloat(1, 0.5f);
        obtainStyledAttributes.recycle();
    }

    public C52902bs(ViewGroup.LayoutParams layoutParams) {
        super(layoutParams);
    }
}
