package X;

import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.biz.order.view.fragment.OrderDetailFragment;
import java.util.List;

/* renamed from: X.2gr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54522gr extends AnonymousClass02M implements AbstractC37061lF {
    public final AnonymousClass4JI A00;
    public final C37071lG A01;
    public final OrderDetailFragment A02;
    public final List A03 = C12960it.A0l();

    public C54522gr(AnonymousClass4JI r2, C37071lG r3, OrderDetailFragment orderDetailFragment) {
        this.A01 = r3;
        this.A02 = orderDetailFragment;
        this.A00 = r2;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A03.size();
    }

    @Override // X.AbstractC37061lF
    public AnonymousClass4JV ACa(int i) {
        return (AnonymousClass4JV) this.A03.get(i);
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r2, int i) {
        ((AbstractC75653kC) r2).A08((AnonymousClass4JV) this.A03.get(i));
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        if (i == 0) {
            return new C59102ty(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.list_item_cart_header_item), null);
        }
        if (i == 1) {
            AnonymousClass4JI r2 = this.A00;
            return new C59122u0(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.list_item_cart_item), this, this.A01, this.A02, C12960it.A0R(r2.A00.A04));
        } else if (i == 2) {
            return new C84523zR(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.list_item_order_detail_footer));
        } else {
            throw C12960it.A0U("CartItemsAdapter/onCreateViewHolder/unhandled view type");
        }
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        return ((AnonymousClass4JV) this.A03.get(i)).A00;
    }
}
