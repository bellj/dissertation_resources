package X;

import com.whatsapp.conversation.conversationrow.message.MessageDetailsActivity;

/* renamed from: X.41W  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass41W extends AnonymousClass2Dn {
    public final /* synthetic */ MessageDetailsActivity A00;

    public AnonymousClass41W(MessageDetailsActivity messageDetailsActivity) {
        this.A00 = messageDetailsActivity;
    }

    @Override // X.AnonymousClass2Dn
    public void A00(AbstractC14640lm r2) {
        this.A00.A01.notifyDataSetChanged();
    }
}
