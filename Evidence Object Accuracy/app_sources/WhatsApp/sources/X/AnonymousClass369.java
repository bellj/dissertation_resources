package X;

import android.text.Editable;
import com.whatsapp.contact.picker.BaseSharedPreviewDialogFragment;
import com.whatsapp.contact.picker.SharedTextPreviewDialogFragment;

/* renamed from: X.369  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass369 extends C469928m {
    public boolean A00;
    public final /* synthetic */ SharedTextPreviewDialogFragment A01;

    public AnonymousClass369(SharedTextPreviewDialogFragment sharedTextPreviewDialogFragment) {
        this.A01 = sharedTextPreviewDialogFragment;
    }

    @Override // X.C469928m, android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        SharedTextPreviewDialogFragment sharedTextPreviewDialogFragment = this.A01;
        ActivityC000900k A0B = sharedTextPreviewDialogFragment.A0B();
        AnonymousClass19M r6 = sharedTextPreviewDialogFragment.A09;
        C42971wC.A06(A0B, sharedTextPreviewDialogFragment.A0E.getPaint(), editable, ((BaseSharedPreviewDialogFragment) sharedTextPreviewDialogFragment).A0C, r6, sharedTextPreviewDialogFragment.A0F);
        sharedTextPreviewDialogFragment.A1N(editable, this.A00);
    }
}
