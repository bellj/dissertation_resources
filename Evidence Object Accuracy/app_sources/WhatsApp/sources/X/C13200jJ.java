package X;

/* renamed from: X.0jJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C13200jJ implements AbstractC13190jI {
    public static final Object A02 = new Object();
    public volatile AbstractC13190jI A00;
    public volatile Object A01 = A02;

    public C13200jJ(AbstractC13190jI r2) {
        this.A00 = r2;
    }

    @Override // X.AbstractC13190jI
    public Object get() {
        Object obj;
        Object obj2 = this.A01;
        Object obj3 = A02;
        if (obj2 != obj3) {
            return obj2;
        }
        synchronized (this) {
            obj = this.A01;
            if (obj == obj3) {
                obj = this.A00.get();
                this.A01 = obj;
                this.A00 = null;
            }
        }
        return obj;
    }
}
