package X;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;

/* renamed from: X.3CU  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3CU {
    public final Map A00 = Collections.synchronizedMap(new WeakHashMap());
    public final Map A01 = Collections.synchronizedMap(new WeakHashMap());

    public final void A00(Status status, boolean z) {
        HashMap hashMap;
        HashMap hashMap2;
        Map map = this.A00;
        synchronized (map) {
            hashMap = new HashMap(map);
        }
        Map map2 = this.A01;
        synchronized (map2) {
            hashMap2 = new HashMap(map2);
        }
        Iterator A0s = C12990iw.A0s(hashMap);
        while (A0s.hasNext()) {
            Map.Entry A15 = C12970iu.A15(A0s);
            if (z || C12970iu.A1Y(A15.getValue())) {
                ((BasePendingResult) A15.getKey()).A06(status);
            }
        }
        Iterator A0s2 = C12990iw.A0s(hashMap2);
        while (A0s2.hasNext()) {
            Map.Entry A152 = C12970iu.A15(A0s2);
            if (z || C12970iu.A1Y(A152.getValue())) {
                ((C13690kA) A152.getKey()).A00(new C630439z(status));
            }
        }
    }
}
