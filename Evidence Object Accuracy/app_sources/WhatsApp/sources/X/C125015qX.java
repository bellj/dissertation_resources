package X;

/* renamed from: X.5qX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C125015qX {
    public static String A00(String str) {
        boolean z;
        if (str == null) {
            return null;
        }
        switch (str.hashCode()) {
            case -1149187101:
                return str.equals("SUCCESS") ? "SUCCESSFUL" : str;
            case -1031784143:
                return str.equals("CANCELLED") ? "CANCELLED" : str;
            case -591252731:
                return str.equals("EXPIRED") ? "EXPIRED" : str;
            case -368591510:
                z = str.equals("FAILURE");
                break;
            case 35394935:
                return str.equals("PENDING") ? "PENDING" : str;
            case 527514546:
                return str.equals("IN_REVIEW") ? "IN_REVIEW" : str;
            case 1350822958:
                return str.equals("DECLINED") ? "DECLINED" : str;
            case 1383663147:
                return str.equals("COMPLETED") ? "COMPLETED" : str;
            case 2066319421:
                z = str.equals("FAILED");
                break;
            default:
                return str;
        }
        return z ? "FAILED" : str;
    }
}
