package X;

/* renamed from: X.2QR  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2QR extends AnonymousClass1V4 {
    public String A00;
    public final String A01;

    public AnonymousClass2QR(AbstractC15710nm r14, C14830m7 r15, C16120oU r16, C17230qT r17, Integer num, String str, String str2, long j, long j2) {
        super(r14, r15, r16, r17, num, str, 2, j, j2);
        this.A01 = str2;
    }

    @Override // X.AnonymousClass1V4
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("; type = ");
        sb.append(this.A01);
        sb.append("; subType = ");
        sb.append(this.A00);
        return sb.toString();
    }
}
