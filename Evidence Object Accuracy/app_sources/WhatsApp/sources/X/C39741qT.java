package X;

import android.os.SystemClock;
import com.whatsapp.util.Log;

/* renamed from: X.1qT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C39741qT {
    public static final AnonymousClass00E A04 = new AnonymousClass00E(1, 10);
    public static final AnonymousClass00E A05 = new AnonymousClass00E(20, 200);
    public long A00 = -1;
    public final long A01;
    public final C16120oU A02;
    public final C39751qU A03;

    public C39741qT(C16120oU r3) {
        this.A02 = r3;
        this.A03 = new C39751qU();
        this.A01 = SystemClock.uptimeMillis();
    }

    public void A00() {
        long j = this.A00;
        boolean z = false;
        if (j > 0) {
            z = true;
        }
        AnonymousClass009.A0F(z);
        C39751qU r4 = this.A03;
        r4.A02 = Boolean.FALSE;
        r4.A09 = Long.valueOf(SystemClock.uptimeMillis() - j);
        r4.A0M = Long.valueOf(SystemClock.uptimeMillis() - this.A01);
        this.A02.A0B(r4, A04, false);
    }

    public void A01() {
        long j = this.A00;
        boolean z = false;
        if (j > 0) {
            z = true;
        }
        AnonymousClass009.A0F(z);
        C39751qU r4 = this.A03;
        r4.A09 = Long.valueOf(SystemClock.uptimeMillis() - j);
        StringBuilder sb = new StringBuilder("mediatranscodequeue/srcLength");
        sb.append(r4.A0J);
        sb.append(" destinationSize=");
        sb.append(r4.A07);
        sb.append(" compressionRate=");
        sb.append(((float) r4.A07.longValue()) / ((float) r4.A0J.longValue()));
        sb.append(" duration=");
        sb.append(r4.A09);
        sb.append(" width=");
        sb.append(r4.A08);
        sb.append(" height=");
        sb.append(r4.A06);
        sb.append(" isProgressiveJpeg=");
        sb.append(r4.A03);
        sb.append(" firstScanLength=");
        sb.append(r4.A0A);
        sb.append(" thumbnailLength=");
        sb.append(r4.A0L);
        Log.i(sb.toString());
        r4.A02 = Boolean.TRUE;
        r4.A0M = Long.valueOf(SystemClock.uptimeMillis() - this.A01);
        this.A02.A07(r4);
    }

    public void A02(int i, int i2) {
        C39751qU r2 = this.A03;
        r2.A08 = Long.valueOf((long) i);
        r2.A06 = Long.valueOf((long) i2);
    }

    public void A03(C38991p4 r6) {
        C39751qU r4 = this.A03;
        r4.A0K = Long.valueOf((long) r6.A03);
        r4.A0I = Long.valueOf((long) r6.A01);
        r4.A0G = Long.valueOf((long) (r6.A00() / 1000));
        r4.A0H = Long.valueOf(r6.A04 / 1000);
    }
}
