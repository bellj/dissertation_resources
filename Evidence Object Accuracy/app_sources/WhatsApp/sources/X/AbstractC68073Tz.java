package X;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import org.chromium.net.UrlRequest;

/* renamed from: X.3Tz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC68073Tz implements AbstractC116635Wf {
    public View A00;
    public final C53042cM A01;
    public final AbstractC26131Cd A02;

    public AbstractC68073Tz(C53042cM r1, AbstractC26131Cd r2) {
        this.A01 = r1;
        this.A02 = r2;
    }

    public View A00() {
        int i;
        View view = this.A00;
        if (view != null) {
            return view;
        }
        C53042cM r2 = this.A01;
        LayoutInflater A0E = C12960it.A0E(r2);
        if (!(this instanceof C58932s0)) {
            i = R.layout.conversations_payments_onboarding_banner;
        } else {
            i = R.layout.conversations_payments_incentive_banner;
        }
        View A0F = C12960it.A0F(A0E, r2, i);
        this.A00 = A0F;
        return A0F;
    }

    public void A01() {
        View view = this.A00;
        AnonymousClass009.A03(view);
        view.setVisibility(8);
        this.A02.A00();
    }

    public void A02(int i) {
        C14820m6 r3;
        long currentTimeMillis;
        String str;
        if (i != 1) {
            switch (i) {
                case 11:
                case 12:
                case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                    break;
                default:
                    return;
            }
        }
        AbstractC26131Cd r1 = this.A02;
        if (!(r1 instanceof C26141Ce)) {
            r3 = ((C26121Cc) r1).A01;
            currentTimeMillis = System.currentTimeMillis();
            str = "payments_incentive_banner_start_cool_off_timestamp";
        } else {
            r3 = ((C26141Ce) r1).A01;
            currentTimeMillis = System.currentTimeMillis();
            str = "payments_onboarding_banner_start_cool_off_timestamp";
        }
        r3.A0q(str, currentTimeMillis);
    }

    @Override // X.AbstractC116635Wf
    public void AIR() {
        C12970iu.A1G(this.A00);
    }

    @Override // X.AbstractC116635Wf
    public boolean AdK() {
        return this.A02.A02();
    }

    @Override // X.AbstractC116635Wf
    public void AfF() {
        String str;
        C17930rd A01;
        if (!(this instanceof C58942s1)) {
            if (this.A02.A02() && this.A00 == null) {
                this.A01.addView(A00());
            }
            View A00 = A00();
            TextEmojiLabel A0T = C12970iu.A0T(A00, R.id.banner_title);
            C53042cM r6 = this.A01;
            AbstractC28491Nn.A06(A0T, C12960it.A0X(r6.getContext(), AnonymousClass1US.A05(A00.getContext(), R.color.primary_light), new Object[1], 0, R.string.payments_incentive_banner_title));
            r6.setBackgroundResource(R.color.chat_banner_background);
            C12960it.A10(r6, this, 37);
            C12960it.A10(AnonymousClass028.A0D(A00, R.id.cancel), this, 36);
            A00.setVisibility(0);
            r6.A00(23, 1);
            return;
        }
        AbstractC26131Cd r5 = this.A02;
        if (r5.A02() && this.A00 == null) {
            this.A01.addView(A00());
        }
        View A002 = A00();
        TextEmojiLabel A0T2 = C12970iu.A0T(A002, R.id.banner_title);
        C53042cM r2 = this.A01;
        AbstractC28491Nn.A06(A0T2, r2.getContext().getString(R.string.payments_onboarding_banner_title));
        TextView A0I = C12960it.A0I(A002, R.id.banner_image);
        if (!(r5 instanceof C26141Ce) || (A01 = ((C26141Ce) r5).A02.A01()) == null) {
            str = null;
        } else {
            str = ((AbstractC30781Yu) A01.A02).A05;
        }
        A0I.setText(str);
        r2.setBackgroundResource(R.color.chat_banner_background);
        C12960it.A10(r2, this, 39);
        C12960it.A10(AnonymousClass028.A0D(A002, R.id.cancel), this, 38);
        A002.setVisibility(0);
        r2.A00(16, 1);
    }
}
