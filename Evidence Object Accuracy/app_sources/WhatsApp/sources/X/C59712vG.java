package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.2vG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59712vG extends AbstractC37191le {
    public final TextView A00;

    public C59712vG(View view) {
        super(view);
        this.A00 = C12960it.A0I(view, R.id.category_text);
    }
}
