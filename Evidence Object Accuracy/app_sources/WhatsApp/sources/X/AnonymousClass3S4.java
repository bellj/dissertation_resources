package X;

import com.whatsapp.notification.PopupNotification;
import com.whatsapp.notification.PopupNotificationViewPager;

/* renamed from: X.3S4  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3S4 implements AnonymousClass070 {
    public final /* synthetic */ PopupNotification A00;

    public AnonymousClass3S4(PopupNotification popupNotification) {
        this.A00 = popupNotification;
    }

    @Override // X.AnonymousClass070
    public void ATO(int i) {
        PopupNotification popupNotification;
        Integer num;
        if (i == 1) {
            this.A00.A1P = true;
        } else if (i == 0 && (num = (popupNotification = this.A00).A1I) != null) {
            popupNotification.A1M.remove(num.intValue());
            if (popupNotification.A1I.intValue() >= popupNotification.A1M.size()) {
                popupNotification.A1I = Integer.valueOf(popupNotification.A1I.intValue() - 1);
            }
            popupNotification.A0S.A06();
            PopupNotificationViewPager popupNotificationViewPager = popupNotification.A10;
            AnonymousClass2D6 r2 = popupNotification.A0S;
            int intValue = popupNotification.A1I.intValue();
            popupNotificationViewPager.setAdapter(r2);
            popupNotificationViewPager.A0F(intValue, false);
            popupNotification.A2Z(popupNotification.A10.getCurrentItem());
            if (popupNotification.A1M.size() == 1) {
                popupNotification.A2W();
            }
            popupNotification.A1I = null;
        }
    }

    @Override // X.AnonymousClass070
    public void ATP(int i, float f, int i2) {
        PopupNotification popupNotification = this.A00;
        if (popupNotification.A1P) {
            popupNotification.A13.A01(true);
            AbstractC15340mz r0 = popupNotification.A16;
            if (r0 != null) {
                popupNotification.A1L.add(r0.A0z);
            }
            C15370n3 r2 = popupNotification.A0p;
            if (r2 != null) {
                popupNotification.A1J.add(r2.A0B(AbstractC14640lm.class));
            }
        }
    }

    @Override // X.AnonymousClass070
    public void ATQ(int i) {
        PopupNotification popupNotification = this.A00;
        if (popupNotification.A1I == null) {
            popupNotification.A2Z(popupNotification.A10.getCurrentItem());
        }
    }
}
