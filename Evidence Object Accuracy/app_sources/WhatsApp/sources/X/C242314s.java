package X;

import android.content.ContentValues;
import android.database.Cursor;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.14s  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C242314s {
    public final AbstractC15710nm A00;
    public final C16490p7 A01;
    public final C19770ue A02;

    public C242314s(AbstractC15710nm r1, C16490p7 r2, C19770ue r3) {
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
    }

    public static final ContentValues A00(C30761Ys r3, long j) {
        ContentValues contentValues = new ContentValues(6);
        contentValues.put("message_row_id", Long.valueOf(j));
        contentValues.put("text_data", r3.A04);
        contentValues.put("extra_data", r3.A05);
        contentValues.put("button_type", Integer.valueOf(r3.A03));
        contentValues.put("used", Integer.valueOf(r3.A01 ? 1 : 0));
        contentValues.put("selected_index", Integer.valueOf(r3.A02));
        contentValues.put("otp_button_type", Integer.valueOf(r3.A06.get()));
        return contentValues;
    }

    public C30761Ys A01(long j) {
        C16310on A01 = this.A01.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT _id, text_data, extra_data, button_type, used, selected_index, otp_button_type FROM message_template_button WHERE message_row_id = ?", new String[]{String.valueOf(j)});
            if (A09 == null || !A09.moveToFirst()) {
                StringBuilder sb = new StringBuilder();
                sb.append("TemplateMessageStore/getTemplateButtonReplyData/Template button reply data doesn't exist in the table; messageRowId=");
                sb.append(j);
                Log.e(sb.toString());
                if (A09 != null) {
                    A09.close();
                }
                A01.close();
                return null;
            }
            long j2 = A09.getLong(A09.getColumnIndexOrThrow("_id"));
            String string = A09.getString(A09.getColumnIndexOrThrow("text_data"));
            String string2 = A09.getString(A09.getColumnIndexOrThrow("extra_data"));
            int i = A09.getInt(A09.getColumnIndexOrThrow("button_type"));
            boolean z = false;
            if (A09.getInt(A09.getColumnIndexOrThrow("used")) == 1) {
                z = true;
            }
            C30761Ys r5 = new C30761Ys(string, string2, i, A09.getInt(A09.getColumnIndexOrThrow("selected_index")), 0, z);
            r5.A00 = j2;
            A09.close();
            A01.close();
            return r5;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A02(AbstractC15340mz r29) {
        Long valueOf;
        AnonymousClass009.A0B("TemplateMessageStore/fillTemplateData/message needs to be FMessageTemplate.", r29 instanceof AbstractC28871Pi);
        long j = r29.A11;
        ArrayList arrayList = new ArrayList();
        C16310on A01 = this.A01.get();
        try {
            C16330op r4 = A01.A03;
            String valueOf2 = String.valueOf(j);
            Cursor A09 = r4.A09("SELECT content_text_data, footer_text_data, template_id, csat_trigger_expiration_ts FROM message_template WHERE message_row_id = ?", new String[]{valueOf2});
            if (A09 == null || !A09.moveToFirst()) {
                Log.e("TemplateMessageStore/getTemplateData/no template data in the table.");
                if (A09 != null) {
                    A09.close();
                }
                A01.close();
                AbstractC15710nm r42 = this.A00;
                StringBuilder sb = new StringBuilder("message.key");
                sb.append(r29.A0z);
                r42.AaV("TemplateMessageStore/fillTemplateData/template data is missing.", sb.toString(), true);
                ((AbstractC28871Pi) r29).Acz(new C28891Pk(null, "", null, null, null));
                return;
            }
            String string = A09.getString(A09.getColumnIndexOrThrow("content_text_data"));
            String string2 = A09.getString(A09.getColumnIndexOrThrow("footer_text_data"));
            String string3 = A09.getString(A09.getColumnIndexOrThrow("template_id"));
            long j2 = A09.getLong(A09.getColumnIndexOrThrow("csat_trigger_expiration_ts"));
            A09.close();
            Cursor A092 = r4.A09("SELECT _id, text_data, extra_data, button_type, used, selected_index, otp_button_type FROM message_template_button WHERE message_row_id = ?", new String[]{valueOf2});
            int columnIndexOrThrow = A092.getColumnIndexOrThrow("_id");
            int columnIndexOrThrow2 = A092.getColumnIndexOrThrow("text_data");
            int columnIndexOrThrow3 = A092.getColumnIndexOrThrow("extra_data");
            int columnIndexOrThrow4 = A092.getColumnIndexOrThrow("button_type");
            int columnIndexOrThrow5 = A092.getColumnIndexOrThrow("used");
            int columnIndexOrThrow6 = A092.getColumnIndexOrThrow("selected_index");
            int columnIndexOrThrow7 = A092.getColumnIndexOrThrow("otp_button_type");
            while (A092.moveToNext()) {
                long j3 = A092.getLong(columnIndexOrThrow);
                String string4 = A092.getString(columnIndexOrThrow2);
                String string5 = A092.getString(columnIndexOrThrow3);
                int i = A092.getInt(columnIndexOrThrow4);
                boolean z = false;
                if (A092.getInt(columnIndexOrThrow5) == 1) {
                    z = true;
                }
                C30761Ys r3 = new C30761Ys(string4, string5, i, A092.getInt(columnIndexOrThrow6), A092.getInt(columnIndexOrThrow7), z);
                r3.A00 = j3;
                arrayList.add(r3);
            }
            A092.close();
            A01.close();
            AnonymousClass009.A05(string);
            if (arrayList.size() == 0) {
                arrayList = null;
            }
            if (j2 == 0) {
                valueOf = null;
            } else {
                valueOf = Long.valueOf(j2);
            }
            ((AbstractC28871Pi) r29).Acz(new C28891Pk(valueOf, string, string2, string3, arrayList));
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A03(AbstractC15340mz r17) {
        if (!(r17 instanceof AbstractC28871Pi)) {
            throw new IllegalArgumentException("message must be template message");
        } else if (r17.A11 != -1) {
            C16490p7 r11 = this.A01;
            C16310on A02 = r11.A02();
            try {
                AnonymousClass1Lx A00 = A02.A00();
                AbstractC28871Pi r5 = (AbstractC28871Pi) r17;
                C28891Pk AH7 = r5.AH7();
                ContentValues contentValues = new ContentValues(5);
                contentValues.put("message_row_id", Long.valueOf(r17.A11));
                contentValues.put("content_text_data", AH7.A01);
                contentValues.put("footer_text_data", AH7.A02);
                contentValues.put("template_id", AH7.A03);
                contentValues.put("csat_trigger_expiration_ts", AH7.A00);
                boolean z = false;
                if (A02.A03.A06(contentValues, "message_template", 5) == r17.A11) {
                    z = true;
                }
                AnonymousClass009.A0C("TemplateMessageStore/insertOrUpdateTemplateData/inserted row should have same row_id", z);
                List<C30761Ys> list = r5.AH7().A04;
                if (list != null) {
                    for (C30761Ys r2 : list) {
                        long j = r17.A11;
                        C16310on A022 = r11.A02();
                        ContentValues A002 = A00(r2, j);
                        long j2 = r2.A00;
                        if (j2 == -1) {
                            r2.A00 = A022.A03.A02(A002, "message_template_button");
                        } else if (A022.A03.A00("message_template_button", A002, "_id = ?", new String[]{String.valueOf(j2)}) != 1) {
                            Log.e("TemplateMessageStore/insertOrUpdateTemplateButton/fail to update template button.");
                        }
                        A022.close();
                    }
                }
                A00.A00();
                A00.close();
                A02.close();
            } catch (Throwable th) {
                try {
                    A02.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } else {
            throw new IllegalArgumentException("main message part must be inserted before");
        }
    }

    public void A04(AbstractC28871Pi r9, long j) {
        boolean z = false;
        if (j > 0) {
            z = true;
        }
        AnonymousClass009.A0B("TemplateMessageStore/fillQuotedTemplateData/parent message row must be set", z);
        C16310on A01 = this.A01.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT content_text_data, footer_text_data FROM message_template_quoted WHERE message_row_id = ?", new String[]{String.valueOf(j)});
            if (A09 == null || !A09.moveToFirst()) {
                StringBuilder sb = new StringBuilder();
                sb.append("TemplateMessageStore/fillQuotedTemplateData/missing template info for quoted message; rowId=");
                sb.append(j);
                throw new IllegalArgumentException(sb.toString());
            }
            String string = A09.getString(A09.getColumnIndexOrThrow("content_text_data"));
            String string2 = A09.getString(A09.getColumnIndexOrThrow("footer_text_data"));
            A09.close();
            A01.close();
            AnonymousClass009.A05(string);
            r9.Acz(new C28891Pk(null, string, string2, null, null));
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
