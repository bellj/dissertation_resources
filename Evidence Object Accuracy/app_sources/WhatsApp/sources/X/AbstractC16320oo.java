package X;

import android.content.Context;
import android.text.TextUtils;
import android.util.Base64;
import com.facebook.redex.RunnableBRunnable0Shape10S0200000_I1;
import com.facebook.redex.RunnableBRunnable0Shape1S0100000_I0_1;
import com.google.android.gms.maps.model.LatLng;
import com.whatsapp.util.Log;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.0oo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC16320oo {
    public String A00;
    public String A01;
    public final AbstractC15710nm A02;
    public final C14900mE A03;
    public final C16430p0 A04;
    public final AnonymousClass1B4 A05;
    public final C16340oq A06;
    public final C17170qN A07;
    public final AnonymousClass018 A08;
    public final C14850m9 A09;
    public final AbstractC16350or A0A = new C623636w(this);
    public final AbstractC14440lR A0B;
    public final WeakReference A0C;

    public AbstractC16320oo(AbstractC15710nm r2, C14900mE r3, C16430p0 r4, AnonymousClass2K1 r5, AnonymousClass1B4 r6, C16340oq r7, C17170qN r8, AnonymousClass018 r9, C14850m9 r10, AbstractC14440lR r11) {
        this.A03 = r3;
        this.A02 = r2;
        this.A0B = r11;
        this.A08 = r9;
        this.A09 = r10;
        this.A05 = r6;
        this.A06 = r7;
        this.A07 = r8;
        this.A04 = r4;
        this.A0C = new WeakReference(r5);
        this.A01 = "1.0";
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:111:0x0293 */
    /* JADX DEBUG: Multi-variable search result rejected for r7v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r7v1, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r7v2, types: [X.4Jl] */
    /* JADX WARN: Type inference failed for: r7v3, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r7v5, types: [X.4TW] */
    /* JADX WARN: Type inference failed for: r7v6, types: [X.4T8] */
    public static /* synthetic */ void A00(AbstractC16320oo r15, AnonymousClass4N2 r16, C90934Pu r17) {
        JSONObject optJSONObject;
        Integer num;
        Integer num2;
        String str;
        String str2;
        Object obj;
        ArrayList arrayList;
        ArrayList arrayList2;
        String str3;
        String str4;
        Double d;
        int i = r16.A00;
        if (!(i == -1 || i == 3)) {
            if (i / 100 == 2) {
                JSONObject jSONObject = r16.A01;
                if (jSONObject != null) {
                    if (r15 instanceof C59432ui) {
                        obj = new ArrayList();
                        JSONArray jSONArray = jSONObject.getJSONArray("popular_categories");
                        ArrayList arrayList3 = new ArrayList();
                        if (jSONArray != null) {
                            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                                arrayList3.add(AnonymousClass2x6.A01(jSONArray.getJSONObject(i2)));
                            }
                            obj.add(new C89314Jm(arrayList3));
                        } else {
                            throw new JSONException("PopularCategoriesWidget/fromJson categories not found");
                        }
                    } else if (r15 instanceof C59422uh) {
                        HashMap hashMap = new HashMap();
                        JSONArray jSONArray2 = jSONObject.getJSONArray("tiles");
                        for (int i3 = 0; i3 < jSONArray2.length(); i3++) {
                            JSONObject jSONObject2 = jSONArray2.getJSONObject(i3);
                            hashMap.put(jSONObject2.getString("tile_id"), Integer.valueOf(jSONObject2.getInt("imprecise_location_tile_level")));
                        }
                        obj = new C89304Jl(hashMap);
                    } else if (r15 instanceof C59442uj) {
                        obj = new ArrayList();
                        JSONArray jSONArray3 = jSONObject.getJSONArray("categories");
                        for (int i4 = 0; i4 < jSONArray3.length(); i4++) {
                            JSONObject jSONObject3 = jSONArray3.getJSONObject(i4);
                            String string = jSONObject3.getString("id");
                            AnonymousClass009.A04(string);
                            String string2 = jSONObject3.getString("name");
                            AnonymousClass009.A04(string2);
                            obj.add(new C30211Wn(string, string2));
                        }
                    } else if (!(r15 instanceof C59462ul)) {
                        boolean z = r15 instanceof C59412ug;
                        ArrayList arrayList4 = new ArrayList();
                        if (!z) {
                            JSONArray jSONArray4 = jSONObject.getJSONArray("business_profiles");
                            for (int i5 = 0; i5 < jSONArray4.length(); i5++) {
                                arrayList4.add(AnonymousClass3M8.A00(jSONArray4.getJSONObject(i5)));
                            }
                            arrayList2 = new ArrayList();
                            JSONArray optJSONArray = jSONObject.optJSONArray("subcategories");
                            if (optJSONArray != null) {
                                for (int i6 = 0; i6 < optJSONArray.length(); i6++) {
                                    arrayList2.add(AnonymousClass2x6.A01(optJSONArray.getJSONObject(i6)));
                                }
                            }
                            arrayList = new ArrayList();
                            JSONArray optJSONArray2 = jSONObject.optJSONArray("filter_categories");
                            if (optJSONArray2 != null) {
                                for (int i7 = 0; i7 < optJSONArray2.length(); i7++) {
                                    arrayList.add(AnonymousClass2x6.A01(optJSONArray2.getJSONObject(i7)));
                                }
                            }
                            d = Double.valueOf(jSONObject.optDouble("proximity_weight"));
                            str3 = jSONObject.optString("ranking_logic_ver");
                            str4 = jSONObject.optString("page_id");
                        } else {
                            JSONArray jSONArray5 = jSONObject.getJSONArray("business_profiles");
                            for (int i8 = 0; i8 < jSONArray5.length(); i8++) {
                                arrayList4.add(AnonymousClass3M8.A00(jSONArray5.getJSONObject(i8)));
                            }
                            arrayList2 = new ArrayList();
                            arrayList = new ArrayList();
                            str4 = null;
                            d = Double.valueOf(0.0d);
                            str3 = null;
                        }
                        obj = new AnonymousClass4T8(d, str4, str3, arrayList4, arrayList2, arrayList);
                    } else {
                        C59462ul r152 = (C59462ul) r15;
                        ArrayList arrayList5 = new ArrayList();
                        JSONArray jSONArray6 = jSONObject.getJSONArray("categories");
                        for (int i9 = 0; i9 < jSONArray6.length(); i9++) {
                            JSONObject jSONObject4 = jSONArray6.getJSONObject(i9);
                            String string3 = jSONObject4.getString("id");
                            AnonymousClass009.A04(string3);
                            String string4 = jSONObject4.getString("name");
                            AnonymousClass009.A04(string4);
                            arrayList5.add(new C30211Wn(string3, string4));
                        }
                        ArrayList arrayList6 = new ArrayList();
                        JSONArray jSONArray7 = jSONObject.getJSONArray("businesses");
                        for (int i10 = 0; i10 < jSONArray7.length(); i10++) {
                            arrayList6.add(AnonymousClass3M8.A00(jSONArray7.getJSONObject(i10)));
                        }
                        ArrayList arrayList7 = new ArrayList();
                        JSONArray optJSONArray3 = jSONObject.optJSONArray("filter_categories");
                        if (optJSONArray3 != null) {
                            for (int i11 = 0; i11 < optJSONArray3.length(); i11++) {
                                arrayList7.add(AnonymousClass2x6.A01(optJSONArray3.getJSONObject(i11)));
                            }
                        }
                        obj = new AnonymousClass4TW(Double.valueOf(jSONObject.optDouble("proximity_weight")), jSONObject.optString("page_id"), r152.A05, jSONObject.optString("ranking_logic_ver"), arrayList5, arrayList6, arrayList7);
                    }
                    r17.A02 = obj;
                    i = 0;
                } else {
                    i = 1;
                    r15.A02.AaV("BusinessDirectoryNetworkRequest/parseNetworkResponse: cannot parse empty response from server", "", true);
                }
            } else if (i == 410) {
                i = 4;
            } else {
                StringBuilder sb = new StringBuilder("BusinessDirectoryNetworkRequest/parseNetworkResponse Request has failed with code: ");
                sb.append(i);
                Log.e(sb.toString());
                r17.A00 = 2;
                AnonymousClass4SZ r3 = new AnonymousClass4SZ(Integer.valueOf(i));
                JSONObject jSONObject5 = r16.A01;
                if (!(jSONObject5 == null || (optJSONObject = jSONObject5.optJSONObject("error")) == null)) {
                    if (optJSONObject.has("code")) {
                        num = Integer.valueOf(optJSONObject.optInt("code"));
                    } else {
                        num = null;
                    }
                    r3.A00 = num;
                    if (optJSONObject.has("error_subcode")) {
                        num2 = Integer.valueOf(optJSONObject.optInt("error_subcode"));
                    } else {
                        num2 = null;
                    }
                    r3.A01 = num2;
                    if (optJSONObject.has("message")) {
                        str = optJSONObject.optString("message");
                    } else {
                        str = null;
                    }
                    r3.A04 = str;
                    if (optJSONObject.has("fbtrace_id")) {
                        str2 = optJSONObject.optString("fbtrace_id");
                    } else {
                        str2 = null;
                    }
                    r3.A03 = str2;
                }
                r17.A01 = r3;
                return;
            }
        }
        r17.A00 = i;
    }

    public String A01() {
        if (this instanceof C59432ui) {
            return "home";
        }
        if (this instanceof C59422uh) {
            return "imprecise_location_tile";
        }
        if (this instanceof C59442uj) {
            return "categories";
        }
        if (!(this instanceof C59462ul)) {
            return !(this instanceof C59412ug) ? "businesses" : "recommendations";
        }
        return "query";
    }

    public final JSONObject A02() {
        HashMap hashMap;
        String str;
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("locale", AnonymousClass018.A00(this.A08.A00).toString());
        jSONObject.put("version", this.A01);
        if (!TextUtils.isEmpty(this.A00)) {
            jSONObject.put("credential", this.A00);
        }
        if (this instanceof C59432ui) {
            C59432ui r6 = (C59432ui) this;
            hashMap = new HashMap();
            hashMap.put("wa_biz_directory_lat", Double.valueOf(r6.A00));
            hashMap.put("wa_biz_directory_long", Double.valueOf(r6.A01));
            hashMap.put("screen_res", r6.A02 <= 240 ? "hdpi" : "xxhdpi");
        } else if (this instanceof C59422uh) {
            C59422uh r62 = (C59422uh) this;
            hashMap = new HashMap();
            JSONObject jSONObject2 = new JSONObject();
            LatLng latLng = r62.A01;
            jSONObject2.put("wa_biz_directory_lat", latLng.A00);
            jSONObject2.put("wa_biz_directory_long", latLng.A01);
            hashMap.put("location", jSONObject2);
            hashMap.put("max_tiles", Integer.valueOf(r62.A00));
        } else if (this instanceof C59442uj) {
            C59442uj r63 = (C59442uj) this;
            hashMap = new HashMap();
            hashMap.put("wa_biz_directory_lat", Double.valueOf(r63.A00));
            hashMap.put("wa_biz_directory_long", Double.valueOf(r63.A01));
            hashMap.put("radius", Double.valueOf(r63.A02));
            hashMap.put("location_type", r63.A03);
            hashMap.put("tiered_onboarding_supported", Boolean.valueOf(r63.A09.A07(1443)));
        } else if (this instanceof C59462ul) {
            C59462ul r64 = (C59462ul) this;
            hashMap = new HashMap();
            hashMap.put("query", r64.A04);
            hashMap.put("search_type", r64.A05);
            C48122Ek r4 = r64.A02;
            hashMap.put("wa_biz_directory_lat", r4.A02());
            hashMap.put("wa_biz_directory_long", r4.A03());
            hashMap.put("radius", r4.A05);
            hashMap.put("location_type", r4.A07);
            hashMap.put("business_load_all", Boolean.valueOf(r64.A07));
            hashMap.put("search_by_business_enabled", Boolean.valueOf(r64.A06));
            C14850m9 r42 = r64.A09;
            hashMap.put("ranking_logic_ver", r42.A03(1413));
            hashMap.put("tiered_onboarding_supported", Boolean.valueOf(r42.A07(1443)));
            AnonymousClass2K4 r5 = r64.A00;
            if (r5 != null) {
                JSONObject jSONObject3 = new JSONObject();
                jSONObject3.put("page_id", r5.A01);
                jSONObject3.put("page_size", r5.A00);
                hashMap.put("pagination", jSONObject3);
            }
            AnonymousClass2K3 r0 = r64.A01;
            if (r0 != null) {
                hashMap.put("filters", r0.A00());
            }
        } else if (!(this instanceof C59412ug)) {
            C59452uk r65 = (C59452uk) this;
            hashMap = new HashMap();
            C48122Ek r43 = r65.A05;
            hashMap.put("wa_biz_directory_lat", r43.A02());
            hashMap.put("wa_biz_directory_long", r43.A03());
            hashMap.put("radius", r43.A05);
            hashMap.put("location_type", r43.A07);
            C30211Wn r02 = r65.A06;
            if (r02 != null) {
                hashMap.put("category_id", r02.A00);
            }
            String str2 = r65.A00;
            AnonymousClass009.A04(str2);
            hashMap.put("businesses_list_inclusion_level", str2);
            String str3 = r65.A01;
            AnonymousClass009.A04(str3);
            hashMap.put("subcategories_list_inclusion_level", str3);
            String str4 = r65.A07;
            if (str4 != null) {
                hashMap.put("browse_use_case", str4);
            }
            hashMap.put("ranking_formula_ver", "linear_weights_v1");
            C14850m9 r44 = r65.A09;
            hashMap.put("ranking_logic_ver", r44.A03(1412));
            hashMap.put("tiered_onboarding_supported", Boolean.valueOf(r44.A07(1443)));
            if (r44.A07(1473)) {
                if (r65.A02 <= 240) {
                    str = "hdpi";
                } else {
                    str = "xxhdpi";
                }
                hashMap.put("category_icons_resolution", str);
            }
            AnonymousClass2K3 r03 = r65.A04;
            if (r03 != null) {
                hashMap.put("filters", r03.A00());
            }
            AnonymousClass2K4 r52 = r65.A03;
            if (r52 != null) {
                JSONObject jSONObject4 = new JSONObject();
                jSONObject4.put("page_id", r52.A01);
                jSONObject4.put("page_size", r52.A00);
                hashMap.put("pagination", jSONObject4);
            }
        } else {
            C59412ug r66 = (C59412ug) this;
            hashMap = new HashMap();
            hashMap.put("business_jid", r66.A00.getRawString());
            hashMap.put("nebula_experiment_id", Integer.valueOf(r66.A09.A02(1649)));
        }
        for (Map.Entry entry : hashMap.entrySet()) {
            jSONObject.put((String) entry.getKey(), entry.getValue());
        }
        return jSONObject;
    }

    public void A03() {
        int i;
        byte[] decode;
        C16250oh r1;
        int i2;
        C16340oq r2 = this.A06;
        C16290ol r6 = new C16290ol(this, r2);
        try {
            AnonymousClass1B7 r0 = r2.A01;
            C14850m9 r5 = r2.A03;
            C91854Tl r10 = new C91854Tl(r5.A02(966), r5.A02(965), r5.A02(967), r5.A02(968), r5.A02(969), r5.A02(970), r5.A02(971), r5.A02(972));
            if (!r0.A09.contains("WA_BizDirectorySearch")) {
                r2.A00.AaV("DirectoryTokenManagerImpl/generateACSToken", "WA_BizDirectorySearch is not registered with ACSTokenManager", false);
                A05(0);
                return;
            }
            Map map = r0.A01;
            if (!map.containsKey("WA_BizDirectorySearch")) {
                C16180oa r14 = new C16180oa(r0.A07);
                AnonymousClass2N3 r15 = new AnonymousClass2N3(r0.A06);
                C14830m7 r9 = r0.A04;
                C14850m9 r52 = r0.A05;
                AbstractC14440lR r4 = r0.A08;
                C16160oY r11 = new C16160oY(r0.A02, r0.A03, r14, r15, r9, r52, r4);
                r14.A02("token_length", r10.A06);
                r14.A02("shared_secret_length", r10.A04);
                r14.A03("max_time_to_live_in_sec", (long) r10.A07);
                r14.A02("max_redeem_count", r10.A03);
                r14.A02("lead_time_to_prefetch_sec", r10.A01);
                r14.A02("lead_redeem_count_to_prefetch", r10.A00);
                r14.A02("max_sign_retry_count", r10.A02);
                r14.A03("sign_retry_interval_sec", (long) r10.A05);
                map.put("WA_BizDirectorySearch", r11);
                r0.A00.put("WA_BizDirectorySearch", r14);
                r11.A09.execute(new RunnableBRunnable0Shape1S0100000_I0_1(r11, 44));
            }
            if (map.containsKey("WA_BizDirectorySearch")) {
                ((C16160oY) map.get("WA_BizDirectorySearch")).A00.add(r6);
            }
            if (map.containsKey("WA_BizDirectorySearch") && r0.A00.get("WA_BizDirectorySearch") != null) {
                C16160oY r92 = (C16160oY) map.get("WA_BizDirectorySearch");
                C16180oa r7 = r92.A05;
                String string = r7.A00().getString("original_token_string", null);
                long A00 = (r92.A07.A00() / 1000) - r7.A00().getLong("base_timestamp", 0);
                byte[] bArr = null;
                if (string != null) {
                    if (r7.A00().getInt("redeem_count", -1) >= r7.A00().getInt("max_redeem_count", -1) || A00 >= r7.A00().getLong("max_time_to_live_in_sec", 0)) {
                        decode = Base64.decode(string, 8);
                        if (!r92.A0F) {
                            r92.A09.execute(new RunnableBRunnable0Shape1S0100000_I0_1(r92, 43));
                            i2 = r7.A00().getInt("token_not_ready_reason", 0);
                        } else {
                            r1 = new C16250oh(decode, null, 13);
                        }
                    } else {
                        int i3 = r7.A00().getInt("redeem_count", -1) + 1;
                        r7.A02("redeem_count", i3);
                        int i4 = r7.A00().getInt("lead_redeem_count_to_prefetch", 0);
                        int i5 = r7.A00().getInt("lead_time_to_prefetch_sec", 0);
                        if (i3 >= i4 || (A00 > r7.A00().getLong("max_time_to_live_in_sec", 0) - ((long) i5) && !r92.A0F)) {
                            r92.A09.execute(new RunnableBRunnable0Shape1S0100000_I0_1(r92, 40));
                        }
                        decode = Base64.decode(string, 8);
                        String string2 = r7.A00().getString("shared_secret_string", null);
                        if (string2 != null) {
                            bArr = Base64.decode(string2, 8);
                        }
                        i2 = 0;
                        r7.A01(0);
                    }
                    r1 = new C16250oh(decode, bArr, i2);
                } else if (!r92.A0F) {
                    r92.A09.execute(new RunnableBRunnable0Shape1S0100000_I0_1(r92, 41));
                    r7.A01(13);
                    r1 = new C16250oh(null, null, 13);
                } else {
                    r1 = new C16250oh(null, null, r7.A00().getInt("token_not_ready_reason", 0));
                }
                byte[] bArr2 = r1.A01;
                if (!(bArr2 == null || r1.A02 == null)) {
                    String A002 = r2.A00(r1);
                    if (TextUtils.isEmpty(A002)) {
                        i = 2;
                        A05(i);
                        r6.A00();
                    }
                    StringBuilder sb = new StringBuilder();
                    sb.append(Base64.encodeToString(bArr2, 10));
                    sb.append("+");
                    sb.append(A002);
                    String obj = sb.toString();
                    if (!this.A0A.A02.isCancelled()) {
                        this.A00 = obj;
                        A04();
                    }
                    r6.A00();
                }
            }
            if (!r2.A02.A0B()) {
                i = 4;
                A05(i);
                r6.A00();
            }
        } catch (Exception unused) {
            Log.e("DirectoryTokenManagerImpl/generateACSToken Exception while generating ACS token");
            r2.A00.AaV("DirectoryTokenManagerImpl/generateACSToken", "Exception while generating ACS token", true);
            A05(5);
            r6.A00();
        }
    }

    public final void A04() {
        int i;
        if ((this instanceof C59432ui) || (this instanceof C59422uh) || (this instanceof C59442uj) || !(this instanceof C59462ul)) {
            this.A0B.Aaz(this.A0A, new Void[0]);
            return;
        }
        C90934Pu r3 = new C90934Pu();
        try {
            AnonymousClass1B4 r10 = this.A05;
            String str = AnonymousClass029.A06;
            JSONObject A02 = A02();
            String A01 = A01();
            C63933Dm r9 = new C63933Dm(this, r3);
            if (!r10.A03.A0B()) {
                i = -1;
            } else if (TextUtils.isEmpty(A02.toString())) {
                i = 3;
            } else {
                AnonymousClass1B6 r2 = r10.A02;
                C92454Vy r8 = new C92454Vy(r9, r10, str, A01, A02);
                C14850m9 r5 = r2.A00.A00;
                if (!r5.A07(450) || !r5.A07(1301)) {
                    if (r5.A07(450)) {
                        r5.A07(1301);
                    }
                } else if (C65233Is.A01()) {
                    r2.A00();
                } else {
                    synchronized (AnonymousClass1B6.class) {
                        if (!C65233Is.A01()) {
                            Context context = r2.A01.A00;
                            C13020j0.A02(context, "Context must not be null");
                            C13690kA r52 = new C13690kA();
                            if (C65233Is.A01()) {
                                r52.A01(null);
                            } else {
                                new Thread(new RunnableBRunnable0Shape10S0200000_I1(context, 19, r52)).start();
                            }
                            C13600jz r6 = r52.A00;
                            r6.A03.A00(new AnonymousClass514(new AbstractC115775Sw(r8, r2) { // from class: X.50u
                                public final /* synthetic */ C92454Vy A00;
                                public final /* synthetic */ AnonymousClass1B6 A01;

                                {
                                    this.A01 = r2;
                                    this.A00 = r1;
                                }

                                @Override // X.AbstractC115775Sw
                                public final void AOR(C13600jz r4) {
                                    AnonymousClass1B6 r22 = this.A01;
                                    C92454Vy r1 = this.A00;
                                    if (r4.A0A()) {
                                        r22.A00();
                                    }
                                    if (r4.A00() != null) {
                                        Log.e("CronetEngineProvider/installAndCreateCronetEngine cronet engine install failed");
                                    }
                                    r1.A00();
                                }
                            }, C13650k5.A00));
                            r6.A04();
                        } else {
                            r2.A00();
                            r8.A00();
                        }
                    }
                    return;
                }
                r8.A00();
                return;
            }
            r9.A00(new AnonymousClass4N2(null, i));
        } catch (Exception e) {
            if (e instanceof JSONException) {
                this.A02.AaV("BusinessDirectoryNetworkRequest/startCronetRequest: Error while generating the JSON: ", e.getMessage(), true);
            } else {
                Log.e("BusinessDirectoryNetworkRequest/startCronetRequest: generic error - ", e);
            }
            r3.A00 = 3;
            A06(r3);
        }
    }

    public void A05(int i) {
        Long l;
        if (!this.A0A.A02.isCancelled()) {
            AnonymousClass2K1 r1 = (AnonymousClass2K1) this.A0C.get();
            if (r1 != null) {
                if (i == 4) {
                    r1.APl(-1);
                } else {
                    A04();
                }
            }
            C16430p0 r4 = this.A04;
            Integer valueOf = Integer.valueOf(i);
            C16450p2 r2 = new C16450p2();
            r2.A00 = 1;
            r2.A09 = r4.A01;
            if (valueOf != null) {
                l = Long.valueOf(valueOf.longValue());
            } else {
                l = null;
            }
            r2.A01 = l;
            r2.A02 = null;
            r4.A04.A07(r2);
        }
    }

    public final void A06(C90934Pu r11) {
        AnonymousClass4SZ r0;
        AnonymousClass2K1 r1 = (AnonymousClass2K1) this.A0C.get();
        if (r1 != null) {
            int i = r11.A00;
            if (i == 0) {
                Object obj = r11.A02;
                if (obj != null) {
                    r1.AX4(obj);
                } else {
                    this.A02.AaV("BusinessDirectoryNetworkRequest/handleNetworkResult: Null response content", null, true);
                }
            } else {
                r1.APl(i);
                if (r11.A00 != 4 && (r0 = r11.A01) != null) {
                    C16430p0 r5 = this.A04;
                    String A01 = A01();
                    Integer num = r0.A02;
                    Integer num2 = r0.A00;
                    Integer num3 = r0.A01;
                    String str = r0.A04;
                    String str2 = r0.A03;
                    C16450p2 r2 = new C16450p2();
                    r2.A09 = r5.A01;
                    r2.A00 = 0;
                    r2.A06 = A01;
                    r2.A05 = Long.valueOf(num.longValue());
                    if (num2 != null) {
                        r2.A03 = Long.valueOf(num2.longValue());
                    }
                    if (num3 != null) {
                        r2.A04 = Long.valueOf(num3.longValue());
                    }
                    r2.A08 = str;
                    r2.A07 = str2;
                    r5.A04.A07(r2);
                }
            }
        }
    }
}
