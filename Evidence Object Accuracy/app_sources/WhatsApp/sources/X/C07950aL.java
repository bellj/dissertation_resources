package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0aL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07950aL implements AbstractC12470hy, AbstractC12030hG {
    public final AnonymousClass0QR A00;
    public final AnonymousClass0QR A01;
    public final AnonymousClass0QR A02;
    public final AnonymousClass0J5 A03;
    public final String A04;
    public final List A05 = new ArrayList();
    public final boolean A06;

    @Override // X.AbstractC12470hy
    public void Aby(List list, List list2) {
    }

    public C07950aL(C08260aq r5, AbstractC08070aX r6) {
        this.A04 = r5.A04;
        this.A06 = r5.A05;
        this.A03 = r5.A03;
        AnonymousClass0H1 r3 = new AnonymousClass0H1(r5.A02.A00);
        this.A02 = r3;
        AnonymousClass0H1 r2 = new AnonymousClass0H1(r5.A00.A00);
        this.A00 = r2;
        AnonymousClass0H1 r1 = new AnonymousClass0H1(r5.A01.A00);
        this.A01 = r1;
        r6.A03(r3);
        r6.A03(r2);
        r6.A03(r1);
        r3.A07.add(this);
        r2.A07.add(this);
        r1.A07.add(this);
    }

    @Override // X.AbstractC12030hG
    public void AYB() {
        int i = 0;
        while (true) {
            List list = this.A05;
            if (i < list.size()) {
                ((AbstractC12030hG) list.get(i)).AYB();
                i++;
            } else {
                return;
            }
        }
    }

    @Override // X.AbstractC12470hy
    public String getName() {
        return this.A04;
    }
}
