package X;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.RectF;

/* renamed from: X.0ig  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public interface AbstractC12860ig extends AbstractC12470hy {
    void A9C(Canvas canvas, Matrix matrix, int i);

    void AAy(Matrix matrix, RectF rectF, boolean z);
}
