package X;

import android.content.Context;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.Handler;
import java.util.Iterator;

/* renamed from: X.3HW  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3HW {
    public int A00 = 3;
    public int A01;
    public C72893fL A02;
    public boolean A03;
    public final Context A04;
    public final AudioManager A05;
    public final Handler A06;
    public final AnonymousClass5Pz A07;

    public AnonymousClass3HW(Context context, Handler handler, AnonymousClass5Pz r8) {
        boolean A1T;
        Context applicationContext = context.getApplicationContext();
        this.A04 = applicationContext;
        this.A06 = handler;
        this.A07 = r8;
        Object systemService = applicationContext.getSystemService("audio");
        C95314dV.A01(systemService);
        AudioManager audioManager = (AudioManager) systemService;
        this.A05 = audioManager;
        this.A01 = A00(audioManager, 3);
        if (AnonymousClass3JZ.A01 >= 23) {
            A1T = audioManager.isStreamMute(3);
        } else {
            A1T = C12960it.A1T(A00(audioManager, 3));
        }
        this.A03 = A1T;
        C72893fL r2 = new C72893fL(this);
        try {
            applicationContext.registerReceiver(r2, new IntentFilter("android.media.VOLUME_CHANGED_ACTION"));
            this.A02 = r2;
        } catch (RuntimeException e) {
            C64923Hl.A02("StreamVolumeManager", "Error registering stream volume receiver", e);
        }
    }

    public static int A00(AudioManager audioManager, int i) {
        try {
            return audioManager.getStreamVolume(i);
        } catch (RuntimeException e) {
            C64923Hl.A02("StreamVolumeManager", C12960it.A0W(i, "Could not retrieve stream volume for stream type "), e);
            return audioManager.getStreamMaxVolume(i);
        }
    }

    public int A01() {
        if (AnonymousClass3JZ.A01 >= 28) {
            return this.A05.getStreamMinVolume(this.A00);
        }
        return 0;
    }

    public final void A02() {
        boolean A1T;
        AudioManager audioManager = this.A05;
        int A00 = A00(audioManager, this.A00);
        int i = this.A00;
        if (AnonymousClass3JZ.A01 >= 23) {
            A1T = audioManager.isStreamMute(i);
        } else {
            A1T = C12960it.A1T(A00(audioManager, i));
        }
        if (this.A01 != A00 || this.A03 != A1T) {
            this.A01 = A00;
            this.A03 = A1T;
            Iterator it = ((SurfaceHolder$CallbackC67643Sh) this.A07).A00.A0W.iterator();
            if (it.hasNext()) {
                it.next();
                throw C12980iv.A0n("onDeviceVolumeChanged");
            }
        }
    }

    public void A03(int i) {
        if (this.A00 != i) {
            this.A00 = i;
            A02();
            C47492Ax r3 = ((SurfaceHolder$CallbackC67643Sh) this.A07).A00;
            AnonymousClass3HW r0 = r3.A0R;
            C92484Wc r1 = new C92484Wc(r0.A01(), r0.A05.getStreamMaxVolume(r0.A00));
            if (!r1.equals(r3.A0E)) {
                r3.A0E = r1;
                Iterator it = r3.A0W.iterator();
                if (it.hasNext()) {
                    it.next();
                    throw C12980iv.A0n("onDeviceInfoChanged");
                }
            }
        }
    }
}
