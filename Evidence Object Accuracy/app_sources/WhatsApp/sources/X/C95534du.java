package X;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.4du  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C95534du {
    public Set A00 = C12970iu.A12();
    public Set A01 = C12970iu.A12();
    public Set A02 = C12970iu.A12();
    public Set A03 = C12970iu.A12();
    public Set A04 = C12970iu.A12();
    public Set A05 = C12970iu.A12();
    public Set A06;
    public Set A07;
    public Set A08;
    public Set A09;
    public Set A0A;
    public Set A0B;

    public static final int A00(Collection collection) {
        int i = 0;
        if (collection != null) {
            for (Object obj : collection) {
                i += obj instanceof byte[] ? AnonymousClass1TT.A00((byte[]) obj) : obj.hashCode();
            }
        }
        return i;
    }

    public static final boolean A03(String str, String str2) {
        if (str2.startsWith(".")) {
            str2 = str2.substring(1);
        }
        String[] A04 = AnonymousClass1T7.A04(str2);
        String[] A042 = AnonymousClass1T7.A04(str);
        int length = A042.length;
        int length2 = A04.length;
        if (length > length2) {
            int i = length - length2;
            int i2 = -1;
            while (!A042[i2 + i].equals("")) {
                while (true) {
                    i2++;
                    if (i2 >= length2) {
                        return true;
                    }
                    if (i2 != -1) {
                        if (!A04[i2].equalsIgnoreCase(A042[i2 + i])) {
                            return false;
                        }
                    }
                }
            }
        }
        return false;
    }

    public final boolean A09(String str, String str2) {
        boolean equalsIgnoreCase;
        String substring = str.substring(str.indexOf(64) + 1);
        if (str2.indexOf(64) != -1) {
            if (!str.equalsIgnoreCase(str2)) {
                equalsIgnoreCase = substring.equalsIgnoreCase(str2.substring(1));
            }
        } else {
            equalsIgnoreCase = str2.charAt(0) != '.' ? substring.equalsIgnoreCase(str2) : A03(substring, str2);
        }
        return equalsIgnoreCase;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof C95534du)) {
            return false;
        }
        C95534du r4 = (C95534du) obj;
        return A0B(r4.A00, this.A00) && A0B(r4.A01, this.A01) && A0B(r4.A02, this.A02) && A0B(r4.A03, this.A03) && A0B(r4.A05, this.A05) && A0B(r4.A04, this.A04) && A0B(r4.A06, this.A06) && A0B(r4.A07, this.A07) && A0B(r4.A08, this.A08) && A0B(r4.A09, this.A09) && A0B(r4.A0B, this.A0B) && A0B(r4.A0A, this.A0A);
    }

    public int hashCode() {
        return A00(this.A00) + A00(this.A01) + A00(this.A02) + A00(this.A03) + A00(this.A05) + A00(this.A04) + A00(this.A06) + A00(this.A07) + A00(this.A08) + A00(this.A09) + A00(this.A0B) + A00(this.A0A);
    }

    public static final String A01(Set set) {
        StringBuilder A0k = C12960it.A0k("[");
        for (Object obj : set) {
            if (A0k.length() > 1) {
                A0k.append(",");
            }
            AnonymousClass5MZ A00 = AnonymousClass5MZ.A00(obj);
            A0k.append(A00.A01.A01);
            A0k.append(":");
            try {
                byte[] A01 = A00.A00.Aer().A01();
                A0k.append(C72463ee.A0I(A01, A01.length));
            } catch (IOException e) {
                C12970iu.A1V(e, A0k);
            }
        }
        return C12960it.A0d("]", A0k);
    }

    public static final void A02(StringBuilder sb, String str) {
        sb.append(str);
        sb.append(AnonymousClass1T7.A00);
    }

    public static boolean A04(AbstractC114775Na r8, AbstractC114775Na r9) {
        if (r9.A0B() >= 1 && r9.A0B() <= r8.A0B()) {
            C114605Mj A00 = C114605Mj.A00(r9.A0D(0));
            int i = 0;
            int i2 = 0;
            while (true) {
                if (i >= r8.A0B()) {
                    i = i2;
                    break;
                }
                if (C95344dY.A04(A00, C114605Mj.A00(r8.A0D(i)))) {
                    break;
                }
                i++;
                i2 = i;
            }
            if (r9.A0B() <= r8.A0B() - i) {
                for (int i3 = 0; i3 < r9.A0B(); i3++) {
                    C114605Mj A002 = C114605Mj.A00(r9.A0D(i3));
                    C114605Mj A003 = C114605Mj.A00(r8.A0D(i + i3));
                    if (A002.A00.A01.length == A003.A00.A01.length && A002.A03().A01.A04(A003.A03().A01)) {
                        if ((A002.A00.A01.length != 1 || !A002.A03().A01.A04(C114905Nn.A0Y)) ? C95344dY.A04(A002, A003) : A003.A03().A00.toString().startsWith(A002.A03().A00.toString())) {
                        }
                    }
                }
                return true;
            }
        }
        return false;
    }

    public static final boolean A05(byte[] bArr, byte[] bArr2) {
        int length = bArr.length;
        if (length != (bArr2.length >> 1)) {
            return false;
        }
        byte[] bArr3 = new byte[length];
        System.arraycopy(bArr2, length, bArr3, 0, length);
        byte[] bArr4 = new byte[length];
        byte[] bArr5 = new byte[length];
        for (int i = 0; i < length; i++) {
            bArr4[i] = (byte) (bArr2[i] & bArr3[i]);
            bArr5[i] = (byte) (bArr[i] & bArr3[i]);
        }
        return Arrays.equals(bArr4, bArr5);
    }

    public final String A06(Set set) {
        int length;
        int i;
        StringBuilder A0k = C12960it.A0k("[");
        Iterator it = set.iterator();
        while (it.hasNext()) {
            if (A0k.length() > 1) {
                A0k.append(",");
            }
            byte[] bArr = (byte[]) it.next();
            StringBuilder A0h = C12960it.A0h();
            int i2 = 0;
            while (true) {
                length = bArr.length;
                i = length >> 1;
                if (i2 >= i) {
                    break;
                }
                if (A0h.length() > 0) {
                    A0h.append(".");
                }
                A0h.append(Integer.toString(bArr[i2] & 255));
                i2++;
            }
            A0h.append("/");
            boolean z = true;
            while (i < length) {
                if (z) {
                    z = false;
                } else {
                    A0h.append(".");
                }
                A0h.append(Integer.toString(bArr[i] & 255));
                i++;
            }
            C12970iu.A1V(A0h, A0k);
        }
        return C12960it.A0d("]", A0k);
    }

    public void A07(AnonymousClass5N2 r4) {
        Set<AbstractC114775Na> set = this.A00;
        AbstractC114775Na A04 = AbstractC114775Na.A04(r4);
        if (!set.isEmpty()) {
            for (AbstractC114775Na r0 : set) {
                if (A04(A04, r0)) {
                    throw new C87514Bu("Subject distinguished name is from an excluded subtree");
                }
            }
        }
    }

    public void A08(AnonymousClass5N2 r4) {
        Set<AbstractC114775Na> set = this.A06;
        AbstractC114775Na A04 = AbstractC114775Na.A04((Object) r4.A01);
        if (set == null) {
            return;
        }
        if (!set.isEmpty() || A04.A0B() != 0) {
            for (AbstractC114775Na r0 : set) {
                if (A04(A04, r0)) {
                    return;
                }
            }
            throw new C87514Bu("Subject distinguished name is not from a permitted subtree");
        }
    }

    public final boolean A0A(String str, String str2) {
        String substring = str.substring(str.indexOf(58) + 1);
        int indexOf = substring.indexOf("//");
        if (indexOf != -1) {
            substring = substring.substring(indexOf + 2);
        }
        int lastIndexOf = substring.lastIndexOf(58);
        if (lastIndexOf != -1) {
            substring = substring.substring(0, lastIndexOf);
        }
        String substring2 = substring.substring(substring.indexOf(58) + 1);
        String substring3 = substring2.substring(substring2.indexOf(64) + 1);
        int indexOf2 = substring3.indexOf(47);
        if (indexOf2 != -1) {
            substring3 = substring3.substring(0, indexOf2);
        }
        return !str2.startsWith(".") ? substring3.equalsIgnoreCase(str2) : A03(substring3, str2);
    }

    public final boolean A0B(Collection collection, Collection collection2) {
        if (collection != collection2) {
            if (!(collection == null || collection2 == null || collection.size() != collection2.size())) {
                for (Object obj : collection) {
                    for (Object obj2 : collection2) {
                        if (obj != obj2) {
                            if (!(obj == null || obj2 == null)) {
                                if ((!(obj instanceof byte[]) || !(obj2 instanceof byte[])) ? obj.equals(obj2) : Arrays.equals((byte[]) obj, (byte[]) obj2)) {
                                    break;
                                }
                            }
                        } else {
                            break;
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    public String toString() {
        StringBuilder A0h = C12960it.A0h();
        A02(A0h, "permitted:");
        Set set = this.A06;
        if (set != null) {
            A02(A0h, "DN:");
            A02(A0h, set.toString());
        }
        Set set2 = this.A07;
        if (set2 != null) {
            A02(A0h, "DNS:");
            A02(A0h, set2.toString());
        }
        Set set3 = this.A08;
        if (set3 != null) {
            A02(A0h, "Email:");
            A02(A0h, set3.toString());
        }
        Set set4 = this.A0B;
        if (set4 != null) {
            A02(A0h, "URI:");
            A02(A0h, set4.toString());
        }
        Set set5 = this.A09;
        if (set5 != null) {
            A02(A0h, "IP:");
            A02(A0h, A06(set5));
        }
        Set set6 = this.A0A;
        if (set6 != null) {
            A02(A0h, "OtherName:");
            A02(A0h, A01(set6));
        }
        A02(A0h, "excluded:");
        if (!this.A00.isEmpty()) {
            A02(A0h, "DN:");
            A02(A0h, this.A00.toString());
        }
        if (!this.A01.isEmpty()) {
            A02(A0h, "DNS:");
            A02(A0h, this.A01.toString());
        }
        if (!this.A02.isEmpty()) {
            A02(A0h, "Email:");
            A02(A0h, this.A02.toString());
        }
        if (!this.A05.isEmpty()) {
            A02(A0h, "URI:");
            A02(A0h, this.A05.toString());
        }
        if (!this.A03.isEmpty()) {
            A02(A0h, "IP:");
            A02(A0h, A06(this.A03));
        }
        if (!this.A04.isEmpty()) {
            A02(A0h, "OtherName:");
            A02(A0h, A01(this.A04));
        }
        return A0h.toString();
    }
}
