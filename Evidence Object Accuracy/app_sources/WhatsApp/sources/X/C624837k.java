package X;

import com.whatsapp.group.GroupAdminPickerActivity;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.37k  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C624837k extends AbstractC16350or {
    public final C15610nY A00;
    public final AnonymousClass018 A01;
    public final String A02;
    public final WeakReference A03;
    public final List A04;

    public C624837k(C15610nY r3, AnonymousClass018 r4, GroupAdminPickerActivity groupAdminPickerActivity, String str, List list) {
        ArrayList A0l = C12960it.A0l();
        this.A04 = A0l;
        this.A00 = r3;
        this.A01 = r4;
        this.A03 = C12970iu.A10(groupAdminPickerActivity);
        A0l.addAll(list);
        this.A02 = str;
    }
}
