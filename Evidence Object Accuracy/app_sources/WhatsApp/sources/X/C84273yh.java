package X;

import java.util.Arrays;

/* renamed from: X.3yh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C84273yh extends AbstractC87994Dv {
    public final int A00;

    public C84273yh(int i) {
        this.A00 = i;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass() && this.A00 == ((C84273yh) obj).A00) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        Object[] objArr = new Object[1];
        C12960it.A1O(objArr, this.A00);
        return Arrays.hashCode(objArr);
    }
}
