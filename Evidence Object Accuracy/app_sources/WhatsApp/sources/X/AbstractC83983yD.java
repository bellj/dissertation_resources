package X;

/* renamed from: X.3yD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC83983yD extends AbstractC84003yF {
    @Override // X.AbstractC84003yF, X.AbstractC84013yG, X.AbstractC84023yH, X.AnonymousClass4UU
    public Object A00(int i) {
        C44791zY r4;
        String str;
        String str2;
        String A04;
        if (!(this instanceof C83963yB)) {
            C83973yC r0 = (C83973yC) this;
            C65113Ie r1 = r0.A00;
            r4 = r1.A08;
            str = r1.A0A;
            str2 = r0.A01;
            if (r0.A02) {
                A04 = null;
            } else {
                A04 = r1.A04();
            }
        } else {
            C83963yB r12 = (C83963yB) this;
            C65113Ie r02 = r12.A00;
            r4 = r02.A08;
            str = r02.A0A;
            str2 = r12.A01;
            A04 = r02.A04();
        }
        return r4.A03(str, str2, A04, 5000);
    }
}
