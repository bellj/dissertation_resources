package X;

import android.content.Context;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.WaImageButton;

/* renamed from: X.3F8  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3F8 {
    public final Context A00;
    public final WaImageButton A01;
    public final AnonymousClass018 A02;

    public AnonymousClass3F8(WaImageButton waImageButton, AnonymousClass018 r3) {
        this.A01 = waImageButton;
        this.A00 = waImageButton.getContext();
        this.A02 = r3;
    }

    public void A00(int i) {
        if (i == 0) {
            WaImageButton waImageButton = this.A01;
            AnonymousClass018 r2 = this.A02;
            Context context = this.A00;
            AnonymousClass2GF.A01(context, waImageButton, r2, R.drawable.input_send);
            C12960it.A0r(context, waImageButton, R.string.send);
        } else if (i == 1) {
            WaImageButton waImageButton2 = this.A01;
            waImageButton2.setImageResource(R.drawable.ic_done);
            C12960it.A0r(this.A00, waImageButton2, R.string.done);
        }
    }

    public void A01(boolean z) {
        int i;
        int i2 = R.dimen.space_tight;
        if (z) {
            i2 = R.dimen.space_base;
        }
        Context context = this.A00;
        int dimensionPixelSize = context.getResources().getDimensionPixelSize(i2);
        WaImageButton waImageButton = this.A01;
        ViewGroup.MarginLayoutParams A0H = C12970iu.A0H(waImageButton);
        AnonymousClass018 r3 = this.A02;
        C42941w9.A09(waImageButton, r3, A0H.leftMargin, A0H.topMargin, A0H.rightMargin, dimensionPixelSize);
        if (z) {
            AnonymousClass2GF.A01(context, waImageButton, r3, R.drawable.input_send);
            i = R.string.send;
        } else {
            waImageButton.setImageResource(R.drawable.ic_done);
            i = R.string.done;
        }
        C12960it.A0r(context, waImageButton, i);
    }
}
