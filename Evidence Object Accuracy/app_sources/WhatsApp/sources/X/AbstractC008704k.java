package X;

/* renamed from: X.04k  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC008704k extends AnonymousClass04Y {
    public boolean A00 = true;

    public abstract boolean A0D(AnonymousClass03U v);

    public abstract boolean A0E(AnonymousClass03U v);

    public abstract boolean A0F(AnonymousClass03U v, int i, int i2, int i3, int i4);

    public abstract boolean A0G(AnonymousClass03U v, AnonymousClass03U v2, int i, int i2, int i3, int i4);
}
