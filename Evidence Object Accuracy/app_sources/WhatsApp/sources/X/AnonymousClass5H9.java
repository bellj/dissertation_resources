package X;

/* renamed from: X.5H9  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5H9 extends Thread {
    public AnonymousClass5H9() {
        super("HybridData DestructorThread");
    }

    @Override // java.lang.Thread, java.lang.Runnable
    public void run() {
        while (true) {
            try {
                AnonymousClass5HR r4 = (AnonymousClass5HR) AnonymousClass4HT.A03.remove();
                r4.destruct();
                if (r4.A01 == null) {
                    AnonymousClass5HR r3 = (AnonymousClass5HR) AnonymousClass4HT.A01.A00.getAndSet(null);
                    while (r3 != null) {
                        AnonymousClass5HR r2 = r3.A00;
                        AnonymousClass5HR r1 = AnonymousClass4HT.A00.A00;
                        r3.A00 = r1.A00;
                        r1.A00 = r3;
                        r3.A00.A01 = r3;
                        r3.A01 = r1;
                        r3 = r2;
                    }
                }
                AnonymousClass5HR r12 = r4.A00;
                r12.A01 = r4.A01;
                r4.A01.A00 = r12;
            } catch (InterruptedException unused) {
            }
        }
    }
}
