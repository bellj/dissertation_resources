package X;

/* renamed from: X.50K  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass50K implements AbstractC115655Sk {
    public static final AbstractC115655Sk A00 = new AnonymousClass50K();

    @Override // X.AbstractC115655Sk
    public final boolean Agr(int i) {
        return i == 0 || i == 1 || i == 2 || i == 3;
    }
}
