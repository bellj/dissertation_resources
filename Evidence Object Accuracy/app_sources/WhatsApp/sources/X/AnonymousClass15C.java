package X;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.15C  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass15C {
    public final C240413z A00;

    public AnonymousClass15C(C240413z r1) {
        this.A00 = r1;
    }

    public List A00(String str) {
        try {
            C16310on A01 = this.A00.get();
            Cursor A09 = A01.A03.A09("SELECT plain_file_hash, encrypted_file_hash, media_key, mime_type, height, width, sticker_pack_id, file_path, url, file_size, direct_path, emojis, hash_of_image_part, is_avatar FROM stickers WHERE sticker_pack_id = ?", new String[]{str});
            ArrayList arrayList = new ArrayList();
            int columnIndexOrThrow = A09.getColumnIndexOrThrow("plain_file_hash");
            int columnIndexOrThrow2 = A09.getColumnIndexOrThrow("encrypted_file_hash");
            int columnIndexOrThrow3 = A09.getColumnIndexOrThrow("media_key");
            int columnIndexOrThrow4 = A09.getColumnIndexOrThrow("mime_type");
            int columnIndexOrThrow5 = A09.getColumnIndexOrThrow("height");
            int columnIndexOrThrow6 = A09.getColumnIndexOrThrow("width");
            int columnIndexOrThrow7 = A09.getColumnIndexOrThrow("sticker_pack_id");
            int columnIndexOrThrow8 = A09.getColumnIndexOrThrow("file_path");
            int columnIndexOrThrow9 = A09.getColumnIndexOrThrow("file_size");
            int columnIndexOrThrow10 = A09.getColumnIndexOrThrow("url");
            int columnIndexOrThrow11 = A09.getColumnIndexOrThrow("direct_path");
            int columnIndexOrThrow12 = A09.getColumnIndexOrThrow("emojis");
            int columnIndexOrThrow13 = A09.getColumnIndexOrThrow("hash_of_image_part");
            int columnIndexOrThrow14 = A09.getColumnIndexOrThrow("is_avatar");
            while (A09.moveToNext()) {
                AnonymousClass1KS r1 = new AnonymousClass1KS();
                r1.A0C = A09.getString(columnIndexOrThrow);
                r1.A07 = A09.getString(columnIndexOrThrow2);
                r1.A0A = A09.getString(columnIndexOrThrow3);
                r1.A0B = A09.getString(columnIndexOrThrow4);
                r1.A02 = A09.getInt(columnIndexOrThrow5);
                r1.A03 = A09.getInt(columnIndexOrThrow6);
                r1.A0E = A09.getString(columnIndexOrThrow7);
                boolean z = true;
                r1.A08 = A09.getString(columnIndexOrThrow8);
                r1.A01 = 1;
                r1.A00 = A09.getInt(columnIndexOrThrow9);
                r1.A0F = A09.getString(columnIndexOrThrow10);
                r1.A05 = A09.getString(columnIndexOrThrow11);
                r1.A06 = A09.getString(columnIndexOrThrow12);
                r1.A09 = A09.getString(columnIndexOrThrow13);
                if (A09.getInt(columnIndexOrThrow14) != 1) {
                    z = false;
                }
                r1.A0G = z;
                C37431mO.A00(r1);
                arrayList.add(r1);
            }
            A09.close();
            A01.close();
            return arrayList;
        } catch (SQLiteDatabaseCorruptException e) {
            Log.e("StickerDBTableHelper/getByPackId", e);
            return new ArrayList();
        }
    }
}
