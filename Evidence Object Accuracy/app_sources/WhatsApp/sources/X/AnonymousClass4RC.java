package X;

/* renamed from: X.4RC  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4RC {
    public final int A00 = 4225;
    public final String A01;
    public final String A02;
    public final boolean A03;

    public AnonymousClass4RC(String str, String str2, boolean z) {
        this.A02 = str;
        this.A01 = str2;
        this.A03 = z;
    }
}
