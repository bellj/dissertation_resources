package X;

import android.view.ViewTreeObserver;

/* renamed from: X.2Qm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ViewTreeObserver$OnPreDrawListenerC50672Qm implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ C50632Qh A00;

    public ViewTreeObserver$OnPreDrawListenerC50672Qm(C50632Qh r1) {
        this.A00 = r1;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        this.A00.A08();
        return true;
    }
}
