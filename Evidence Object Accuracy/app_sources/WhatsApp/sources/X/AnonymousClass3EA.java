package X;

import android.content.Context;
import android.widget.RadioButton;
import com.whatsapp.R;

/* renamed from: X.3EA  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3EA {
    public final Context A00;
    public final C53292dk A01;
    public final AnonymousClass018 A02;

    public AnonymousClass3EA(Context context, C53292dk r2, AnonymousClass018 r3) {
        this.A00 = context;
        this.A02 = r3;
        this.A01 = r2;
    }

    public void A00(int i) {
        C53292dk r0 = this.A01;
        RadioButton radioButton = r0.A02;
        radioButton.setChecked(false);
        RadioButton radioButton2 = r0.A03;
        radioButton2.setChecked(false);
        RadioButton radioButton3 = r0.A01;
        radioButton3.setChecked(false);
        if (i == 0) {
            radioButton = radioButton2;
        } else if (i == 1) {
            radioButton = radioButton3;
        } else if (i != 2) {
            throw C12960it.A0U("unknown status distribution mode");
        }
        radioButton.setChecked(true);
    }

    public void A01(int i, int i2) {
        AnonymousClass018 r6 = this.A02;
        String A0I = r6.A0I(new Object[]{Integer.valueOf(i)}, R.plurals.status_n_recipients_allow_list, (long) i);
        C53292dk r4 = this.A01;
        r4.A06.setText(A0I);
        Object[] objArr = new Object[1];
        C12960it.A1P(objArr, i2, 0);
        r4.A04.setText(r6.A0I(objArr, R.plurals.status_n_recipients_deny_list, (long) i2));
    }
}
