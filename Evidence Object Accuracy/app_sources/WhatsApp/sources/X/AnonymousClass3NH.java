package X;

import android.animation.ValueAnimator;
import android.view.View;
import com.whatsapp.numberkeyboard.NumberEntryKeyboard;

/* renamed from: X.3NH  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3NH implements View.OnTouchListener {
    public final /* synthetic */ NumberEntryKeyboard A00;

    public AnonymousClass3NH(NumberEntryKeyboard numberEntryKeyboard) {
        this.A00 = numberEntryKeyboard;
    }

    public final void A00() {
        NumberEntryKeyboard numberEntryKeyboard = this.A00;
        numberEntryKeyboard.A05 = null;
        if (numberEntryKeyboard.A0E) {
            AnonymousClass3BU r1 = (AnonymousClass3BU) numberEntryKeyboard.A0D.get(Long.valueOf(numberEntryKeyboard.A02));
            if (r1.A02.isRunning()) {
                r1.A05 = true;
                return;
            }
            ValueAnimator valueAnimator = r1.A03;
            if (!valueAnimator.isRunning()) {
                valueAnimator.start();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0051, code lost:
        if (r4 != 3) goto L_0x0053;
     */
    @Override // android.view.View.OnTouchListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouch(android.view.View r13, android.view.MotionEvent r14) {
        /*
        // Method dump skipped, instructions count: 381
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3NH.onTouch(android.view.View, android.view.MotionEvent):boolean");
    }
}
