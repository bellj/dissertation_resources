package X;

/* renamed from: X.1R4  reason: invalid class name */
/* loaded from: classes2.dex */
public enum AnonymousClass1R4 {
    YES,
    ERROR_UNSPECIFIED,
    /* Fake field, exist only in values array */
    ERROR_CONNECTIVITY,
    FAIL_INCORRECT,
    FAIL_MISMATCH,
    FAIL_TOO_MANY_GUESSES,
    FAIL_GUESSED_TOO_FAST,
    FAIL_RESET_TOO_SOON,
    FAIL_STALE,
    FAIL_TEMPORARILY_UNAVAILABLE,
    FAIL_BLOCKED
}
