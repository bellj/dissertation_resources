package X;

import java.util.Comparator;

/* renamed from: X.1wK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class C43051wK implements Comparator {
    public final /* synthetic */ boolean A00;

    public /* synthetic */ C43051wK(boolean z) {
        this.A00 = z;
    }

    @Override // java.util.Comparator
    public final int compare(Object obj, Object obj2) {
        long j;
        long j2;
        boolean z = this.A00;
        C42951wA r7 = (C42951wA) obj;
        C42951wA r8 = (C42951wA) obj2;
        boolean equals = r7.getClass().equals(r8.getClass());
        AbstractC15340mz r1 = r7.A00;
        if (equals) {
            j = r1.A11;
            j2 = r8.A00.A11;
        } else {
            AbstractC15340mz r0 = r8.A00;
            j = r1.A0I;
            j2 = r0.A0I;
        }
        int i = (j > j2 ? 1 : (j == j2 ? 0 : -1));
        if (z) {
            return -i;
        }
        return i;
    }
}
