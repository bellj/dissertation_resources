package X;

import android.database.Cursor;
import com.facebook.redex.EmptyBaseRunnable0;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.1z5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class RunnableC44511z5 extends EmptyBaseRunnable0 implements Runnable {
    public long A00 = 0;
    public final int A01;
    public final AbstractC15710nm A02;
    public final C14830m7 A03;
    public final C16510p9 A04;
    public final C15650ng A05;
    public final C21410xN A06;
    public final AnonymousClass15Q A07;
    public final C44501z4 A08;
    public final C21380xK A09;
    public final C26731Ep A0A;
    public final C22830zi A0B;
    public final C242414t A0C;

    public RunnableC44511z5(AbstractC15710nm r3, C14830m7 r4, C16510p9 r5, C15650ng r6, C21410xN r7, AnonymousClass15Q r8, C44501z4 r9, C21380xK r10, C26731Ep r11, C22830zi r12, C242414t r13, int i) {
        this.A03 = r4;
        this.A04 = r5;
        this.A02 = r3;
        this.A09 = r10;
        this.A05 = r6;
        this.A0B = r12;
        this.A0C = r13;
        this.A06 = r7;
        this.A07 = r8;
        this.A0A = r11;
        this.A08 = r9;
        this.A01 = i;
    }

    public int A00(int i, long j) {
        C16310on r4;
        Cursor cursor;
        int i2 = this.A01;
        int i3 = i2;
        if (i == 0) {
            r4 = this.A06.A04.get();
            try {
                C16330op r6 = r4.A03;
                StringBuilder sb = new StringBuilder();
                sb.append(C44531z7.A00);
                if (i2 > 0) {
                    sb.append(" LIMIT ");
                    sb.append(i2);
                }
                cursor = r6.A09(sb.toString(), new String[]{Long.toString(j)});
            } finally {
                try {
                    r4.close();
                } catch (Throwable unused) {
                }
            }
        } else if (i == 1) {
            C242414t r0 = this.A0C;
            if (i2 == 0) {
                i3 = -1;
            }
            r4 = r0.A01.get();
            try {
                cursor = r4.A03.A09(C44521z6.A02, new String[]{String.valueOf(i3)});
            } finally {
            }
        } else if (i == 2) {
            C242414t r2 = this.A0C;
            long j2 = j - 1209600000;
            if (i2 == 0) {
                i3 = -1;
            }
            r4 = r2.A01.get();
            try {
                cursor = r4.A03.A09(C44521z6.A00, new String[]{String.valueOf(j2), String.valueOf(i3)});
            } finally {
            }
        } else {
            throw new IllegalArgumentException("Invalid job type");
        }
        r4.close();
        try {
            ArrayList arrayList = new ArrayList();
            int i4 = 0;
            int i5 = 0;
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    AbstractC14640lm A05 = this.A04.A05(cursor.getLong(cursor.getColumnIndexOrThrow("chat_row_id")));
                    if (A05 == null) {
                        i4++;
                    } else {
                        Map map = this.A07.A03;
                        if (map.get(A05) == null && map.get(null) == null) {
                            AbstractC15340mz A02 = this.A05.A0K.A02(cursor, A05, false, true);
                            if (A02 != null) {
                                arrayList.add(A02);
                            } else {
                                Log.e("EphemeralUpdateRunnable/failed to get message");
                            }
                        } else {
                            i5 = 2;
                        }
                    }
                }
                if (i4 > 0) {
                    AbstractC15710nm r42 = this.A02;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("count: ");
                    sb2.append(i4);
                    r42.AaV("EphemeralUpdateRunnable/processMessages/null_jid", sb2.toString(), false);
                }
            }
            if (!arrayList.isEmpty()) {
                StringBuilder sb3 = new StringBuilder("EphemeralUpdateRunnable/performJobAction: ");
                sb3.append(i);
                Log.i(sb3.toString());
                if (i == 0) {
                    this.A05.A0i(arrayList, 15);
                } else if (i == 1) {
                    this.A05.A0j(arrayList);
                } else if (i == 2) {
                    C242414t r10 = this.A0C;
                    StringBuilder sb4 = new StringBuilder("ViewOnceMessageStore/expireMessages/");
                    sb4.append(arrayList.size());
                    sb4.append("/");
                    C28181Ma r8 = new C28181Ma(sb4.toString());
                    C16310on A022 = r10.A01.A02();
                    AnonymousClass1Lx A00 = A022.A00();
                    Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        AbstractC15340mz r1 = (AbstractC15340mz) it.next();
                        if (r1 instanceof AnonymousClass1XH) {
                            r10.A03((AbstractC15340mz) ((AnonymousClass1XH) r1), 2);
                        }
                    }
                    A00.A00();
                    r8.A02("success");
                    A00.close();
                    A022.close();
                    r8.A01();
                    this.A05.A0j(arrayList);
                    Iterator it2 = arrayList.iterator();
                    while (it2.hasNext()) {
                        this.A09.A00((AbstractC15340mz) it2.next(), -1);
                    }
                } else {
                    throw new IllegalArgumentException("Invalid job type");
                }
            }
            if (arrayList.size() == i2) {
                i5 = 1;
            }
            if (cursor != null) {
                cursor.close();
            }
            return i5;
        } catch (Throwable th) {
            if (cursor != null) {
                try {
                    cursor.close();
                } catch (Throwable unused2) {
                }
            }
            throw th;
        }
    }

    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    @Override // java.lang.Runnable
    public void run() {
        /*
        // Method dump skipped, instructions count: 542
        */
        throw new UnsupportedOperationException("Method not decompiled: X.RunnableC44511z5.run():void");
    }
}
