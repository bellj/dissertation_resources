package X;

import android.widget.AbsListView;
import com.whatsapp.calling.callhistory.CallLogActivity;

/* renamed from: X.4ok  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C102194ok implements AbsListView.OnScrollListener {
    public final /* synthetic */ CallLogActivity A00;

    @Override // android.widget.AbsListView.OnScrollListener
    public void onScrollStateChanged(AbsListView absListView, int i) {
    }

    public C102194ok(CallLogActivity callLogActivity) {
        this.A00 = callLogActivity;
    }

    @Override // android.widget.AbsListView.OnScrollListener
    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        this.A00.A2f();
    }
}
