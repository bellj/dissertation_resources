package X;

import android.text.TextUtils;
import android.util.Base64;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.1Fe  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26881Fe implements AbstractC13940ka, AbstractC15920o8 {
    public final AbstractC15710nm A00;
    public final C14900mE A01;
    public final C15570nT A02;
    public final C15450nH A03;
    public final C14650lo A04;
    public final AnonymousClass10A A05;
    public final C246716k A06;
    public final C22970zw A07;
    public final AnonymousClass1FZ A08;
    public final C19850um A09;
    public final C22700zV A0A;
    public final C20730wE A0B;
    public final C14830m7 A0C;
    public final C16590pI A0D;
    public final C14820m6 A0E;
    public final C15650ng A0F;
    public final C14850m9 A0G;
    public final C16120oU A0H;
    public final C20660w7 A0I;
    public final C17230qT A0J;
    public final C26871Fd A0K;
    public final C26861Fc A0L;
    public final C26841Fa A0M;
    public final C26851Fb A0N;
    public final C22990zy A0O;
    public final AbstractC14440lR A0P;
    public final Map A0Q = new HashMap();
    public final AnonymousClass01N A0R;
    public final AnonymousClass01N A0S;

    @Override // X.AbstractC15920o8
    public int[] ADF() {
        return new int[]{202};
    }

    @Override // X.AbstractC13940ka
    public void AR0() {
    }

    @Override // X.AbstractC13940ka
    public void AR1() {
    }

    public C26881Fe(AbstractC15710nm r2, C14900mE r3, C15570nT r4, C15450nH r5, C14650lo r6, AnonymousClass10A r7, C246716k r8, C22970zw r9, AnonymousClass1FZ r10, C19850um r11, C22700zV r12, C20730wE r13, C14830m7 r14, C16590pI r15, C14820m6 r16, C15650ng r17, C14850m9 r18, C16120oU r19, C20660w7 r20, C17230qT r21, C26871Fd r22, C26861Fc r23, C26841Fa r24, C26851Fb r25, C22990zy r26, AbstractC14440lR r27, AnonymousClass01N r28, AnonymousClass01N r29) {
        this.A0C = r14;
        this.A0G = r18;
        this.A01 = r3;
        this.A00 = r2;
        this.A0P = r27;
        this.A02 = r4;
        this.A0D = r15;
        this.A0H = r19;
        this.A0I = r20;
        this.A03 = r5;
        this.A0F = r17;
        this.A08 = r10;
        this.A0M = r24;
        this.A07 = r9;
        this.A0B = r13;
        this.A09 = r11;
        this.A0J = r21;
        this.A0O = r26;
        this.A0A = r12;
        this.A0E = r16;
        this.A06 = r8;
        this.A04 = r6;
        this.A05 = r7;
        this.A0N = r25;
        this.A0L = r23;
        this.A0K = r22;
        this.A0R = r28;
        this.A0S = r29;
    }

    public final void A00(String str) {
        ((AnonymousClass01J) AnonymousClass027.A00(AnonymousClass01J.class, this.A0D.A00)).A2D.get();
        AbstractC15710nm r2 = this.A00;
        StringBuilder sb = new StringBuilder("notificationType = ");
        sb.append(str);
        sb.append("; isSMB = ");
        sb.append(false);
        sb.append("; DirectoryEnabled = ");
        sb.append(false);
        r2.AaV("BusinessNotificationHandler/isSmbNotificationAllowed Trying to show a NUX Upsell notification to a not eligible user", sb.toString(), false);
    }

    public final boolean A01(AnonymousClass1JB r5, AnonymousClass1OT r6, String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        byte[] decode = Base64.decode(str.getBytes(), 0);
        Arrays.toString(decode);
        C42271uw r0 = new C42271uw(AnonymousClass1JA.A0G);
        r0.A02 = true;
        r0.A00 = r5;
        r0.A02(decode);
        this.A0B.A03(r0.A01(), true);
        this.A0I.A0E(r6);
        return true;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x014a, code lost:
        if ((r30.A0C.A00() - ((java.lang.Number) r2).longValue()) > 1000) goto L_0x014c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0185, code lost:
        if (r2 != false) goto L_0x00e0;
     */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x039c  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x020e  */
    @Override // X.AbstractC15920o8
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean AI8(android.os.Message r31, int r32) {
        /*
        // Method dump skipped, instructions count: 1214
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C26881Fe.AI8(android.os.Message, int):boolean");
    }
}
