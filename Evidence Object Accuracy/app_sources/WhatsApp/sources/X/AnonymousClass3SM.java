package X;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import com.facebook.redex.RunnableBRunnable0Shape14S0100000_I1;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/* renamed from: X.3SM  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3SM implements AbstractC117045Ya {
    public long A00;
    public ColorFilter A01;
    public Rect A02;
    public AbstractC117045Ya A03;
    public AnonymousClass3SN A04;
    public boolean A05 = false;
    public final AbstractC12250hc A06;
    public final Runnable A07 = new RunnableBRunnable0Shape14S0100000_I1(this, 0);
    public final ScheduledExecutorService A08;

    public AnonymousClass3SM(AbstractC12250hc r3, AbstractC117045Ya r4, AnonymousClass3SN r5, ScheduledExecutorService scheduledExecutorService) {
        this.A03 = r4;
        this.A04 = r5;
        this.A06 = r3;
        this.A08 = scheduledExecutorService;
    }

    public final synchronized void A00() {
        if (!this.A05) {
            this.A05 = true;
            this.A08.schedule(this.A07, 1000, TimeUnit.MILLISECONDS);
        }
    }

    @Override // X.AbstractC117045Ya
    public boolean A9D(Canvas canvas, Drawable drawable, int i) {
        this.A00 = this.A06.now();
        boolean A1S = C12960it.A1S(this.A03.A9D(canvas, drawable, i) ? 1 : 0);
        A00();
        return A1S;
    }

    @Override // X.AnonymousClass5WT
    public int AD9(int i) {
        return this.A03.AD9(i);
    }

    @Override // X.AbstractC117045Ya
    public int ADX() {
        return this.A03.ADX();
    }

    @Override // X.AbstractC117045Ya
    public int ADY() {
        return this.A03.ADY();
    }

    @Override // X.AbstractC117045Ya
    public void Abj(int i) {
        this.A03.Abj(i);
    }

    @Override // X.AbstractC117045Ya
    public void Abp(Rect rect) {
        this.A03.Abp(rect);
        this.A02 = rect;
    }

    @Override // X.AbstractC117045Ya
    public void Abu(ColorFilter colorFilter) {
        this.A03.Abu(colorFilter);
        this.A01 = colorFilter;
    }

    @Override // X.AnonymousClass5WT
    public int getFrameCount() {
        return this.A03.getFrameCount();
    }

    @Override // X.AnonymousClass5WT
    public int getLoopCount() {
        return this.A03.getLoopCount();
    }
}
