package X;

import android.database.Cursor;
import android.database.sqlite.SQLiteDiskIOException;
import android.net.Uri;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/* renamed from: X.0nh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15660nh {
    public static final String[] A0H = new String[0];
    public final AbstractC15710nm A00;
    public final C14330lG A01;
    public final C15450nH A02;
    public final C15810nw A03;
    public final C16590pI A04;
    public final C16370ot A05;
    public final C16510p9 A06;
    public final C15650ng A07;
    public final C20120vF A08;
    public final C003001i A09;
    public final AnonymousClass12I A0A;
    public final C20850wQ A0B;
    public final C16490p7 A0C;
    public final C242214r A0D;
    public final AnonymousClass134 A0E;
    public final C16630pM A0F;
    public final AbstractC14440lR A0G;

    public C15660nh(AbstractC15710nm r2, C14330lG r3, C15450nH r4, C15810nw r5, C16590pI r6, C16370ot r7, C16510p9 r8, C15650ng r9, C20120vF r10, C003001i r11, AnonymousClass12I r12, C20850wQ r13, C16490p7 r14, C242214r r15, AnonymousClass134 r16, C16630pM r17, AbstractC14440lR r18) {
        this.A06 = r8;
        this.A04 = r6;
        this.A00 = r2;
        this.A0G = r18;
        this.A01 = r3;
        this.A0E = r16;
        this.A03 = r5;
        this.A02 = r4;
        this.A09 = r11;
        this.A07 = r9;
        this.A05 = r7;
        this.A0A = r12;
        this.A0C = r14;
        this.A0D = r15;
        this.A08 = r10;
        this.A0F = r17;
        this.A0B = r13;
    }

    public static String A00(long j, boolean z, boolean z2, boolean z3) {
        StringBuilder sb;
        String str;
        String str2 = " ORDER BY sort_id ASC";
        String str3 = "";
        if (!z2 || j <= 0) {
            sb = new StringBuilder();
            sb.append(C32331bz.A01);
            if (z) {
                str = " AND chat_row_id = ?";
            } else {
                str = str3;
            }
            sb.append(str);
            if (j > 0) {
                str3 = " AND media_size > ?";
            }
            sb.append(str3);
        } else {
            sb = new StringBuilder();
            sb.append(C32331bz.A00);
            if (z) {
                str3 = " AND message.chat_row_id = ?";
            }
            sb.append(str3);
            sb.append(" AND file_size > ?");
        }
        if (!z3) {
            str2 = " ORDER BY sort_id DESC";
        }
        sb.append(str2);
        return sb.toString();
    }

    public int A01(AbstractC38361nx r9, AbstractC14640lm r10, int i) {
        C16150oX r1;
        StringBuilder sb = new StringBuilder("mediamsgstore/getMediaMessagesCount:");
        sb.append(r10);
        Log.i(sb.toString());
        C28181Ma r6 = new C28181Ma(false);
        r6.A04("mediamsgstore/getMediaMessagesCount/");
        String str = C32331bz.A07;
        int i2 = 0;
        String[] strArr = {String.valueOf(this.A06.A02(r10))};
        try {
            C16310on A01 = this.A0C.get();
            Cursor A09 = A01.A03.A09(str, strArr);
            try {
                if (A09 == null) {
                    Log.e("mediamsgstore/getMediaMessagesCount/db/cursor is null");
                } else {
                    while (A09.moveToNext() && !r9.Ada()) {
                        AbstractC15340mz A02 = this.A05.A02(A09, r10, false, true);
                        if ((A02 instanceof AbstractC16130oV) && (r1 = ((AbstractC16130oV) A02).A02) != null && (A02.A0z.A02 || r1.A0P)) {
                            File file = r1.A0F;
                            if (file != null && new File(Uri.fromFile(file).getPath()).exists() && (i2 = i2 + 1) > i) {
                                A09.close();
                                A01.close();
                                return i2;
                            }
                        }
                    }
                    A09.close();
                }
                A01.close();
                r6.A01();
                StringBuilder sb2 = new StringBuilder("mediamsgstore/getMediaMessagesCount/count:");
                sb2.append(i2);
                Log.i(sb2.toString());
                return i2;
            } catch (Throwable th) {
                if (A09 != null) {
                    try {
                        A09.close();
                    } catch (Throwable unused) {
                    }
                }
                throw th;
            }
        } catch (SQLiteDiskIOException e) {
            this.A0A.A00(1);
            throw e;
        }
    }

    public Cursor A02(AbstractC14640lm r8, byte b) {
        C16310on A01 = this.A0C.get();
        try {
            Cursor A09 = A01.A03.A09(C32301bw.A09, new String[]{String.valueOf(this.A06.A02(r8)), Byte.toString(b)});
            A01.close();
            return A09;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public Cursor A03(AbstractC14640lm r9, int i, long j) {
        StringBuilder sb = new StringBuilder("mediamsgstore/getMediaMessagesHeadCursor:");
        sb.append(r9);
        Log.i(sb.toString());
        C16310on A01 = this.A0C.get();
        try {
            StringBuilder sb2 = new StringBuilder(C32331bz.A08);
            C242214r.A02(sb2, true);
            if (i > 0) {
                sb2.append(" LIMIT ");
                sb2.append(i);
            }
            Cursor A09 = A01.A03.A09(sb2.toString(), new String[]{String.valueOf(this.A06.A02(r9)), String.valueOf(this.A0E.A03(j))});
            A01.close();
            return A09;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public Cursor A04(AbstractC14640lm r8, int i, long j) {
        StringBuilder sb = new StringBuilder("mediamsgstore/getMediaMessagesTailCursor:");
        sb.append(r8);
        Log.i(sb.toString());
        C16310on A01 = this.A0C.get();
        try {
            StringBuilder sb2 = new StringBuilder(C32331bz.A08);
            C242214r.A02(sb2, false);
            if (i > 0) {
                sb2.append(" LIMIT ");
                sb2.append(i);
            }
            Cursor A09 = A01.A03.A09(sb2.toString(), new String[]{String.valueOf(this.A06.A02(r8)), String.valueOf(this.A0E.A03(j))});
            A01.close();
            return A09;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public Cursor A05(AbstractC14640lm r8, long j) {
        StringBuilder sb;
        String str;
        StringBuilder sb2 = new StringBuilder("mediamsgstore/getMediaMessagesOrderedBySizeCursor jid:");
        sb2.append(r8);
        sb2.append(", fileSize:");
        sb2.append(j);
        Log.i(sb2.toString());
        C16310on A01 = this.A0C.get();
        try {
            boolean A0A = this.A08.A0A();
            boolean z = false;
            if (r8 != null) {
                z = true;
            }
            String str2 = "";
            if (A0A) {
                sb = new StringBuilder();
                sb.append(C32331bz.A00);
                sb.append(z ? " AND message.chat_row_id = ?" : str2);
                if (j > 0) {
                    str2 = " AND file_size > ?";
                }
                sb.append(str2);
                str = " ORDER BY file_size DESC";
            } else {
                sb = new StringBuilder();
                sb.append(C32331bz.A01);
                sb.append(z ? " AND chat_row_id = ?" : str2);
                if (j > 0) {
                    str2 = " AND media_size > ?";
                }
                sb.append(str2);
                str = " ORDER BY media_size DESC";
            }
            sb.append(str);
            Cursor A09 = A01.A03.A09(sb.toString(), A0F(r8, j));
            A01.close();
            return A09;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public Cursor A06(AbstractC14640lm r5, long j) {
        StringBuilder sb = new StringBuilder("mediamsgstore/getMediaMessagesOrderedByIDAscCursor jid:");
        sb.append(r5);
        sb.append(", fileSize:");
        sb.append(j);
        Log.i(sb.toString());
        C16310on A01 = this.A0C.get();
        try {
            boolean A0A = this.A08.A0A();
            boolean z = false;
            if (r5 != null) {
                z = true;
            }
            Cursor A09 = A01.A03.A09(A00(j, z, A0A, true), A0F(r5, j));
            A01.close();
            return A09;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public Cursor A07(AbstractC14640lm r5, long j) {
        StringBuilder sb = new StringBuilder("mediamsgstore/getMediaAndDocMessagesOrderedByIDDescCursor jid:");
        sb.append(r5);
        sb.append(", fileSize:");
        sb.append(j);
        Log.i(sb.toString());
        C16310on A01 = this.A0C.get();
        try {
            boolean A0A = this.A08.A0A();
            boolean z = false;
            if (r5 != null) {
                z = true;
            }
            Cursor A09 = A01.A03.A09(A00(j, z, A0A, false), A0F(r5, j));
            A01.close();
            return A09;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public C29221Ri A08(String str, byte b, boolean z) {
        String str2;
        String[] strArr;
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        C16150oX A02;
        C29221Ri r13;
        AnonymousClass009.A00();
        C16310on A01 = this.A0C.get();
        try {
            C20120vF r11 = this.A08;
            boolean A0A = r11.A0A();
            if (b == 0) {
                str2 = A0A ? C32331bz.A0A : "SELECT thumb_image, media_enc_hash, timestamp,media_hash,media_wa_type,media_url FROM legacy_available_messages_view WHERE media_hash=? AND  media_enc_hash IS NOT NULL AND media_wa_type in ('2' , '1' , '25' , '3' , '28' , '13' , '29' , '20' , '9' , '26' , '23' , '37' )  ORDER BY _id DESC";
                strArr = new String[]{str};
            } else {
                str2 = A0A ? C32331bz.A09 : "SELECT thumb_image, media_enc_hash, timestamp,media_hash,media_wa_type,media_url FROM messages AS messages INDEXED BY media_hash_index WHERE media_hash = ? AND media_enc_hash IS NOT NULL AND media_wa_type = ? AND _id NOT IN  (  SELECT _id FROM deleted_messages_ids_view ) ORDER BY _id DESC";
                strArr = new String[]{str, Byte.toString(b)};
            }
            try {
                Cursor A09 = A01.A03.A09(str2, strArr);
                if (A09 != null) {
                    try {
                        if (A0A) {
                            i = A09.getColumnIndexOrThrow("enc_file_hash");
                            i2 = A09.getColumnIndexOrThrow("timestamp");
                            i3 = A09.getColumnIndexOrThrow("file_hash");
                            i4 = A09.getColumnIndexOrThrow("message_type");
                            i5 = A09.getColumnIndexOrThrow("media_url");
                        } else {
                            i = A09.getColumnIndexOrThrow("media_enc_hash");
                            i2 = A09.getColumnIndexOrThrow("timestamp");
                            i3 = A09.getColumnIndexOrThrow("media_hash");
                            i4 = A09.getColumnIndexOrThrow("media_wa_type");
                            i5 = A09.getColumnIndexOrThrow("media_url");
                        }
                        while (A09.moveToNext()) {
                            if (A0A) {
                                A02 = r11.A02(A09);
                            } else {
                                byte[] blob = A09.getBlob(A09.getColumnIndexOrThrow("thumb_image"));
                                if (blob != null) {
                                    A02 = C20120vF.A00(str, blob);
                                } else {
                                    continue;
                                }
                            }
                            if (A02 != null) {
                                String string = A09.getString(i);
                                long j = A09.getLong(i2);
                                A09.getString(i3);
                                byte b2 = (byte) ((int) A09.getLong(i4));
                                String string2 = A09.getString(i5);
                                File file = A02.A0F;
                                if (file != null) {
                                    file.exists();
                                    byte[] bArr = A02.A0U;
                                    if (bArr != null && bArr.length == 32) {
                                        if (A02.A0P) {
                                            if (!A02.A0F.isAbsolute()) {
                                                A02.A0F = this.A03.A07(A02.A0F.getPath());
                                            }
                                            if (A02.A0F.exists()) {
                                                r13 = new C29221Ri(A02, string, string2, b2, j);
                                                A09.close();
                                                A01.close();
                                                return r13;
                                            }
                                        }
                                    }
                                }
                                if (!z) {
                                    r13 = new C29221Ri(A02, string, string2, b2, j);
                                    A09.close();
                                    A01.close();
                                    return r13;
                                }
                            } else {
                                continue;
                            }
                        }
                        A09.close();
                    } catch (Throwable th) {
                        try {
                            A09.close();
                        } catch (Throwable unused) {
                        }
                        throw th;
                    }
                }
                A01.close();
                return null;
            } catch (SQLiteDiskIOException e) {
                this.A0A.A00(1);
                throw e;
            }
        } catch (Throwable th2) {
            try {
                A01.close();
            } catch (Throwable unused2) {
            }
            throw th2;
        }
    }

    public AnonymousClass1IS A09(String str, String str2, byte[] bArr, boolean z) {
        C16310on A01;
        Cursor A09;
        byte[] bArr2;
        C20120vF r8 = this.A08;
        if (!r8.A0A()) {
            try {
                AnonymousClass009.A00();
                A01 = this.A0C.get();
                try {
                    A09 = A01.A03.A09("SELECT key_remote_jid, key_from_me, key_id, thumb_image FROM legacy_available_messages_view WHERE media_hash=? AND media_enc_hash=? AND media_wa_type in ('3', '1' )  ORDER BY _id DESC LIMIT 10", new String[]{str, str2});
                    AnonymousClass1IS r12 = null;
                    if (A09 != null) {
                        try {
                            int columnIndexOrThrow = A09.getColumnIndexOrThrow("key_remote_jid");
                            int columnIndexOrThrow2 = A09.getColumnIndexOrThrow("key_from_me");
                            int columnIndexOrThrow3 = A09.getColumnIndexOrThrow("key_id");
                            int columnIndexOrThrow4 = A09.getColumnIndexOrThrow("thumb_image");
                            AnonymousClass1IS r13 = null;
                            while (true) {
                                if (!A09.moveToNext()) {
                                    r12 = r13;
                                    break;
                                }
                                AbstractC14640lm A012 = AbstractC14640lm.A01(A09.getString(columnIndexOrThrow));
                                if (A012 != null) {
                                    boolean z2 = false;
                                    if (A09.getInt(columnIndexOrThrow2) == 1) {
                                        z2 = true;
                                    }
                                    AnonymousClass1IS r2 = new AnonymousClass1IS(A012, A09.getString(columnIndexOrThrow3), z2);
                                    byte[] blob = A09.getBlob(columnIndexOrThrow4);
                                    if (blob == null) {
                                        break;
                                    }
                                    C16150oX A00 = C20120vF.A00(str, blob);
                                    if (A00 != null && (bArr2 = A00.A0U) != null && bArr2.length == 32 && A00.A0P && Arrays.equals(bArr2, bArr)) {
                                        if (!z) {
                                            A09.close();
                                            A01.close();
                                            return r2;
                                        }
                                        r13 = r2;
                                    }
                                } else {
                                    Log.w("msgstore/getMediaMessageKeyByHashes/jid is null or invalid!");
                                }
                                r12 = null;
                            }
                            A09.close();
                        } finally {
                        }
                    }
                    return r12;
                } catch (SQLiteDiskIOException e) {
                    this.A0A.A00(1);
                    throw e;
                }
            } finally {
                try {
                    A01.close();
                } catch (Throwable unused) {
                }
            }
        } else {
            try {
                AnonymousClass1IS r122 = null;
                AnonymousClass009.A00();
                A01 = this.A0C.get();
                try {
                    A09 = A01.A03.A09(C32331bz.A0B, new String[]{str, str2});
                    if (A09 != null) {
                        try {
                            int columnIndexOrThrow5 = A09.getColumnIndexOrThrow("from_me");
                            int columnIndexOrThrow6 = A09.getColumnIndexOrThrow("key_id");
                            while (A09.moveToNext()) {
                                AbstractC14640lm A06 = this.A06.A06(A09);
                                if (A06 == null) {
                                    Log.w("msgstore/getMediaMessageKeyByHashes/jid is null or invalid!");
                                } else {
                                    boolean z3 = false;
                                    if (A09.getInt(columnIndexOrThrow5) == 1) {
                                        z3 = true;
                                    }
                                    AnonymousClass1IS r4 = new AnonymousClass1IS(A06, A09.getString(columnIndexOrThrow6), z3);
                                    C16150oX A02 = r8.A02(A09);
                                    byte[] bArr3 = A02.A0U;
                                    if (bArr3 != null && bArr3.length == 32 && A02.A0P && Arrays.equals(bArr3, bArr)) {
                                        if (!z) {
                                            A09.close();
                                            A01.close();
                                            return r4;
                                        }
                                        r122 = r4;
                                    }
                                }
                            }
                            A09.close();
                        } finally {
                        }
                    }
                    A01.close();
                    return r122;
                } catch (SQLiteDiskIOException e2) {
                    this.A0A.A00(1);
                    throw e2;
                }
            } finally {
                try {
                    A01.close();
                } catch (Throwable unused2) {
                }
            }
        }
    }

    public AbstractC16130oV A0A(String str) {
        if (str == null) {
            return null;
        }
        C16310on A01 = this.A0C.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT message_row_id, chat_row_id, autotransfer_retry_enabled, multicast_id, media_job_uuid, transferred, transcoded, file_path, file_size, suspicious_content, trim_from, trim_to, face_x, face_y, media_key, media_key_timestamp, width, height, has_streaming_sidecar, gif_attribution, thumbnail_height_width_ratio, direct_path, first_scan_sidecar, first_scan_length, message_url, mime_type, file_length, media_name, file_hash, media_duration, page_count, enc_file_hash, partial_media_hash, partial_media_enc_hash, is_animated_sticker, original_file_hash, mute_video, media_caption FROM message_media WHERE original_file_hash = ?", new String[]{str});
            if (A09 != null) {
                if (A09.moveToNext()) {
                    AbstractC15340mz A00 = this.A05.A00(A09.getLong(A09.getColumnIndexOrThrow("message_row_id")));
                    if (A00 instanceof AbstractC16130oV) {
                        AbstractC16130oV r1 = (AbstractC16130oV) A00;
                        A09.close();
                        A01.close();
                        return r1;
                    }
                }
                A09.close();
            }
            A01.close();
            return null;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public ArrayList A0B(AbstractC38361nx r10, AbstractC14640lm r11, int i, int i2) {
        String str;
        AbstractC16130oV r8;
        C16150oX r1;
        StringBuilder sb = new StringBuilder("mediamsgstore/getMediaMessages:");
        sb.append(r11);
        sb.append(" limit:");
        sb.append(i);
        Log.i(sb.toString());
        C28181Ma r6 = new C28181Ma(false);
        r6.A04("mediamsgstore/getMediaMessages/");
        ArrayList arrayList = new ArrayList();
        String valueOf = String.valueOf(this.A06.A02(r11));
        if (i2 == 2) {
            str = C32331bz.A05;
        } else if (i2 == 3) {
            str = C32331bz.A06;
        } else {
            str = C32331bz.A07;
        }
        try {
            C16310on A01 = this.A0C.get();
            Cursor A09 = A01.A03.A09(str, new String[]{valueOf});
            try {
                if (A09 == null) {
                    Log.e("mediamsgstore/getMediaMessages/db/cursor is null");
                } else {
                    while (A09.moveToNext() && (r10 == null || !r10.Ada())) {
                        AbstractC15340mz A02 = this.A05.A02(A09, r11, false, true);
                        if ((A02 instanceof AbstractC16130oV) && (r1 = (r8 = (AbstractC16130oV) A02).A02) != null && (r8.A0z.A02 || r1.A0P)) {
                            File file = r1.A0F;
                            if (file != null && new File(Uri.fromFile(file).getPath()).exists()) {
                                arrayList.add(r8);
                                if (arrayList.size() >= i) {
                                    break;
                                }
                            }
                        }
                    }
                }
                if (A09 != null) {
                    A09.close();
                }
                A01.close();
                r6.A01();
                StringBuilder sb2 = new StringBuilder("mediamsgstore/getMediaMessages/size:");
                sb2.append(arrayList.size());
                Log.i(sb2.toString());
                return arrayList;
            } catch (Throwable th) {
                if (A09 != null) {
                    try {
                        A09.close();
                    } catch (Throwable unused) {
                    }
                }
                throw th;
            }
        } catch (SQLiteDiskIOException e) {
            this.A0A.A00(1);
            throw e;
        }
    }

    public Collection A0C(AnonymousClass02N r5, File file, String str) {
        ArrayList arrayList = new ArrayList();
        for (AbstractC16130oV r1 : A0D(r5, str, (byte) 0)) {
            C16150oX r0 = r1.A02;
            if (r0 != null && file.equals(r0.A0F)) {
                arrayList.add(r1);
            }
        }
        return arrayList;
    }

    public Collection A0D(AnonymousClass02N r7, String str, byte b) {
        String str2;
        String[] strArr;
        AnonymousClass009.A00();
        C16310on r4 = null;
        boolean A0A = this.A08.A0A();
        if (b == 0) {
            if (A0A) {
                str2 = C32331bz.A04;
            } else {
                str2 = C29751Un.A02;
            }
            strArr = new String[]{str};
        } else {
            if (A0A) {
                str2 = C32331bz.A03;
            } else {
                str2 = C29751Un.A01;
            }
            strArr = new String[]{str, Byte.toString(b)};
        }
        try {
            ArrayList arrayList = new ArrayList();
            r4 = this.A0C.get();
            try {
                Cursor A07 = r4.A03.A07(r7, str2, strArr);
                while (A07.moveToNext()) {
                    try {
                        if (r7 != null) {
                            r7.A02();
                        }
                        AbstractC15340mz A01 = this.A05.A01(A07);
                        if (A01 instanceof AbstractC16130oV) {
                            arrayList.add((AbstractC16130oV) A01);
                        }
                    } catch (Throwable th) {
                        try {
                            A07.close();
                        } catch (Throwable unused) {
                        }
                        throw th;
                    }
                }
                A07.close();
                r4.close();
                return arrayList;
            } catch (SQLiteDiskIOException e) {
                this.A0A.A00(1);
                throw e;
            }
        } catch (Throwable th2) {
            try {
                r4.close();
            } catch (Throwable unused2) {
            }
            throw th2;
        }
    }

    public Collection A0E(String str, byte b) {
        File file;
        Collection<AbstractC16130oV> A0D = A0D(null, str, b);
        ArrayList arrayList = new ArrayList(A0D.size());
        for (AbstractC16130oV r2 : A0D) {
            C16150oX r1 = r2.A02;
            if (r1 != null && r1.A0P && (file = r1.A0F) != null && file.exists()) {
                arrayList.add(r2);
            }
        }
        return arrayList;
    }

    public final String[] A0F(AbstractC14640lm r5, long j) {
        ArrayList arrayList = new ArrayList();
        if (r5 != null) {
            arrayList.add(String.valueOf(this.A06.A02(r5)));
        }
        if (j > 0) {
            arrayList.add(String.valueOf(j));
        }
        if (arrayList.size() == 0) {
            return null;
        }
        return (String[]) arrayList.toArray(A0H);
    }
}
