package X;

import android.graphics.drawable.Drawable;
import com.whatsapp.settings.chat.wallpaper.WallPaperView;

/* renamed from: X.3WS  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3WS implements AbstractC116685Wk {
    public final /* synthetic */ C58592r2 A00;
    public final /* synthetic */ WallPaperView A01;
    public final /* synthetic */ Runnable A02;

    public AnonymousClass3WS(C58592r2 r1, WallPaperView wallPaperView, Runnable runnable) {
        this.A00 = r1;
        this.A02 = runnable;
        this.A01 = wallPaperView;
    }

    @Override // X.AbstractC116685Wk
    public void A7G() {
        WallPaperView wallPaperView = this.A01;
        wallPaperView.A04 = false;
        wallPaperView.setImageDrawable(null);
        wallPaperView.invalidate();
    }

    @Override // X.AbstractC116685Wk
    public void AdA(Drawable drawable) {
        this.A00.A00(drawable);
    }

    @Override // X.AbstractC116685Wk
    public void Ag4() {
        this.A02.run();
    }
}
