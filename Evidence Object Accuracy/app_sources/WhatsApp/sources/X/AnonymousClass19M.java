package X;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.whatsapp.emoji.EmojiDescriptor;
import java.lang.ref.SoftReference;
import java.util.Arrays;
import java.util.Map;

/* renamed from: X.19M  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass19M {
    public final AnonymousClass1EN A00;
    public final C39521q2 A01 = new C39521q2();
    public final C39521q2 A02 = new C39521q2();
    public final AnonymousClass1EM A03;

    public AnonymousClass19M(AnonymousClass1EN r2, AnonymousClass1EM r3) {
        this.A00 = r2;
        this.A03 = r3;
    }

    public static BitmapDrawable A00(Resources resources, AbstractC39531q3 r5, C39521q2 r6, int i) {
        Bitmap AKW;
        C39541q4 r3 = new C39541q4(i);
        Bitmap A00 = r6.A00(r3);
        if (A00 != null) {
            return new BitmapDrawable(resources, A00);
        }
        if (r5 == null || (AKW = r5.AKW(resources, i)) == null) {
            return null;
        }
        Map map = r6.A00;
        synchronized (map) {
            map.put(r3, new SoftReference(AKW));
        }
        return new BitmapDrawable(resources, AKW);
    }

    public static Drawable A01(Resources resources, C39541q4 r7, AbstractC39531q3 r8, C39521q2 r9) {
        int[] iArr = r7.A00;
        int length = iArr.length;
        if (length == 1) {
            return A00(resources, r8, r9, iArr[0]);
        }
        Bitmap A00 = r9.A00(r7);
        if (A00 != null) {
            return new BitmapDrawable(resources, A00);
        }
        BitmapDrawable[] bitmapDrawableArr = new BitmapDrawable[length];
        for (int i = 0; i < length; i++) {
            bitmapDrawableArr[i] = A00(resources, r8, r9, iArr[i]);
            if (bitmapDrawableArr[i] == null) {
                return null;
            }
        }
        Bitmap A002 = C39551q5.A00(bitmapDrawableArr);
        if (A002 == null) {
            return null;
        }
        Map map = r9.A00;
        synchronized (map) {
            map.put(r7, new SoftReference(A002));
        }
        return new BitmapDrawable(resources, A002);
    }

    public static void A02(Resources resources, ImageView imageView, AnonymousClass19M r8, int[] iArr) {
        imageView.setImageDrawable(r8.A04(resources, new C39511q1(iArr), 0.75f, -1));
    }

    public Drawable A03(Resources resources, AbstractC39501q0 r5, AbstractC32691cZ r6, long j) {
        C39541q4 A06 = A06(r6, j);
        if (A06 == null) {
            return null;
        }
        C39521q2 r1 = this.A01;
        Drawable A01 = A01(resources, A06, null, r1);
        if (A01 != null) {
            return A01;
        }
        Drawable A012 = A01(resources, A06, new AbstractC39531q3(r5, this) { // from class: X.1q6
            public final /* synthetic */ AbstractC39501q0 A00;
            public final /* synthetic */ AnonymousClass19M A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            /* JADX WARNING: Code restructure failed: missing block: B:50:0x00eb, code lost:
                if (r0 == false) goto L_0x00e8;
             */
            /* JADX WARNING: Removed duplicated region for block: B:32:0x0090 A[Catch: all -> 0x00ab, TryCatch #5 {, blocks: (B:7:0x000d, B:9:0x0022, B:38:0x00b7, B:40:0x00bd, B:54:0x00fa, B:59:0x0103, B:61:0x0105, B:62:0x011e, B:36:0x00ad, B:19:0x003b, B:20:0x005c, B:22:0x0067, B:23:0x006c, B:25:0x0072, B:27:0x0081, B:29:0x0086, B:30:0x0089, B:32:0x0090, B:33:0x0095, B:35:0x009a), top: B:72:0x000d }] */
            /* JADX WARNING: Removed duplicated region for block: B:35:0x009a A[Catch: all -> 0x00ab, TRY_LEAVE, TryCatch #5 {, blocks: (B:7:0x000d, B:9:0x0022, B:38:0x00b7, B:40:0x00bd, B:54:0x00fa, B:59:0x0103, B:61:0x0105, B:62:0x011e, B:36:0x00ad, B:19:0x003b, B:20:0x005c, B:22:0x0067, B:23:0x006c, B:25:0x0072, B:27:0x0081, B:29:0x0086, B:30:0x0089, B:32:0x0090, B:33:0x0095, B:35:0x009a), top: B:72:0x000d }] */
            @Override // X.AbstractC39531q3
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final android.graphics.Bitmap AKW(android.content.res.Resources r10, int r11) {
                /*
                // Method dump skipped, instructions count: 316
                */
                throw new UnsupportedOperationException("Method not decompiled: X.C39561q6.AKW(android.content.res.Resources, int):android.graphics.Bitmap");
            }
        }, r1);
        if (A012 == null) {
            return A01(resources, A06, new C39571q7(this), this.A02);
        }
        return A012;
    }

    public Drawable A04(Resources resources, AbstractC32691cZ r6, float f, long j) {
        int[] iArr;
        Drawable A05 = A05(resources, r6, j);
        if (A05 != null) {
            return A05;
        }
        if (!(r6 instanceof C32681cY)) {
            iArr = ((C39511q1) r6).A01;
        } else {
            C32681cY r62 = (C32681cY) r6;
            int i = r62.A01;
            iArr = new int[i];
            for (int i2 = 0; i2 < i; i2++) {
                iArr[i2] = Character.codePointAt(r62.A02, i2);
            }
        }
        return new C39581q8(iArr, f);
    }

    public Drawable A05(Resources resources, AbstractC32691cZ r5, long j) {
        C39541q4 A06 = A06(r5, j);
        if (A06 == null) {
            return null;
        }
        return A01(resources, A06, new C39571q7(this), this.A02);
    }

    public final C39541q4 A06(AbstractC32691cZ r13, long j) {
        if (j == -1) {
            j = EmojiDescriptor.A00(r13, false);
            if (j == -1) {
                return null;
            }
        }
        Long valueOf = Long.valueOf(j);
        if (valueOf == null) {
            return null;
        }
        long longValue = valueOf.longValue();
        int[] iArr = new int[5];
        int i = ((int) ((longValue >> 4) & 4095)) + 1;
        int i2 = 0;
        do {
            iArr[i2] = i;
            i2++;
            i = ((int) ((longValue >> ((i2 * 12) + 4)) & 4095)) + 1;
            if (i <= 1) {
                break;
            }
        } while (i2 < 5);
        return new C39541q4(Arrays.copyOf(iArr, i2));
    }
}
