package X;

import android.content.DialogInterface;
import android.os.Build;
import com.whatsapp.R;
import com.whatsapp.stickers.StickerStorePackPreviewActivity;

/* renamed from: X.3aI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69823aI implements AbstractC33611ef {
    public final /* synthetic */ StickerStorePackPreviewActivity A00;

    public C69823aI(StickerStorePackPreviewActivity stickerStorePackPreviewActivity) {
        this.A00 = stickerStorePackPreviewActivity;
    }

    public static /* synthetic */ void A00(C69823aI r2) {
        StickerStorePackPreviewActivity stickerStorePackPreviewActivity = r2.A00;
        if (!stickerStorePackPreviewActivity.isFinishing() && Build.VERSION.SDK_INT >= 17 && !stickerStorePackPreviewActivity.isDestroyed()) {
            stickerStorePackPreviewActivity.finish();
        }
    }

    @Override // X.AbstractC33611ef
    public void AWg(AnonymousClass1KZ r2) {
        StickerStorePackPreviewActivity.A02(r2, this.A00);
    }

    @Override // X.AbstractC33611ef
    public void AWi() {
        StickerStorePackPreviewActivity stickerStorePackPreviewActivity = this.A00;
        if (stickerStorePackPreviewActivity.A2f()) {
            stickerStorePackPreviewActivity.A0V = true;
            C18120rw.A00(stickerStorePackPreviewActivity, stickerStorePackPreviewActivity.A0G);
        } else if (stickerStorePackPreviewActivity.A0Y && !stickerStorePackPreviewActivity.isFinishing() && Build.VERSION.SDK_INT >= 17 && !stickerStorePackPreviewActivity.isDestroyed()) {
            C004802e A0S = C12980iv.A0S(stickerStorePackPreviewActivity);
            A0S.A06(R.string.sticker_pack_not_found);
            C12970iu.A1I(A0S);
            A0S.A04(new DialogInterface.OnDismissListener() { // from class: X.4hK
                @Override // android.content.DialogInterface.OnDismissListener
                public final void onDismiss(DialogInterface dialogInterface) {
                    C69823aI.A00(C69823aI.this);
                }
            });
            AnonymousClass04S create = A0S.create();
            create.setCanceledOnTouchOutside(true);
            create.show();
        }
    }
}
