package X;

import android.util.SparseArray;

/* renamed from: X.4VV  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4VV {
    public C92294Vi A00;
    public C92294Vi A01;
    public final SparseArray A02 = new SparseArray();

    public final synchronized void A00(C92294Vi r4) {
        C92294Vi r2 = r4.A02;
        C92294Vi r1 = r4.A01;
        if (r2 != null) {
            r2.A01 = r1;
        }
        if (r1 != null) {
            r1.A02 = r2;
        }
        r4.A02 = null;
        r4.A01 = null;
        if (r4 == this.A00) {
            this.A00 = r1;
        }
        if (r4 == this.A01) {
            this.A01 = r2;
        }
    }
}
