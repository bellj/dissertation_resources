package X;

/* renamed from: X.1Ny  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C28531Ny extends AbstractC16110oT {
    public Boolean A00;
    public Integer A01;
    public Integer A02;
    public Integer A03;
    public Integer A04;
    public Integer A05;
    public Integer A06;
    public Integer A07;
    public Integer A08;
    public Integer A09;
    public Integer A0A;
    public Long A0B;
    public Long A0C;
    public Long A0D;
    public Long A0E;
    public Long A0F;
    public Long A0G;
    public Long A0H;
    public Long A0I;
    public Long A0J;
    public Long A0K;
    public Long A0L;
    public Long A0M;
    public Long A0N;
    public Long A0O;
    public Long A0P;
    public Long A0Q;
    public Long A0R;
    public String A0S;
    public String A0T;
    public String A0U;
    public String A0V;
    public String A0W;
    public String A0X;
    public String A0Y;
    public String A0Z;

    public C28531Ny() {
        super(2896, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(20, this.A0S);
        r3.Abe(21, this.A01);
        r3.Abe(38, this.A02);
        r3.Abe(39, this.A00);
        r3.Abe(34, this.A0T);
        r3.Abe(35, this.A0U);
        r3.Abe(36, this.A0V);
        r3.Abe(2, this.A03);
        r3.Abe(29, this.A0B);
        r3.Abe(30, this.A0C);
        r3.Abe(22, this.A0D);
        r3.Abe(23, this.A0E);
        r3.Abe(24, this.A0F);
        r3.Abe(31, this.A0G);
        r3.Abe(25, this.A0H);
        r3.Abe(26, this.A0I);
        r3.Abe(3, this.A04);
        r3.Abe(17, this.A05);
        r3.Abe(4, this.A06);
        r3.Abe(16, this.A07);
        r3.Abe(32, this.A0J);
        r3.Abe(33, this.A08);
        r3.Abe(1, this.A0W);
        r3.Abe(10, this.A0K);
        r3.Abe(27, this.A0L);
        r3.Abe(8, this.A0M);
        r3.Abe(9, this.A0N);
        r3.Abe(5, this.A09);
        r3.Abe(14, this.A0O);
        r3.Abe(12, this.A0P);
        r3.Abe(28, this.A0Q);
        r3.Abe(11, this.A0R);
        r3.Abe(6, this.A0X);
        r3.Abe(7, this.A0Y);
        r3.Abe(18, this.A0A);
        r3.Abe(15, this.A0Z);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        String obj3;
        String obj4;
        String obj5;
        String obj6;
        String obj7;
        String obj8;
        String obj9;
        String obj10;
        StringBuilder sb = new StringBuilder("WamDirectoryBusinessSearchConsumerClient {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directoryBackendRankingLogicVer", this.A0S);
        Integer num = this.A01;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directoryBusinessListScreenSource", obj);
        Integer num2 = this.A02;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directoryBusinessListScreenType", obj2);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directoryFilterByDistance", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directoryFilterCatalog", this.A0T);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directoryFilterOpeningHours", this.A0U);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directoryFilterSelectedSubcategories", this.A0V);
        Integer num3 = this.A03;
        if (num3 == null) {
            obj3 = null;
        } else {
            obj3 = num3.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directoryLocationType", obj3);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directoryQuerySearchLengthOfQuery", this.A0B);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directoryQuerySearchNumberOfEmptyBusinessSearches", this.A0C);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directoryQuerySearchNumberOfEmptySearches", this.A0D);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directoryQuerySearchNumberOfFoundCategories", this.A0E);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directoryQuerySearchNumberOfRenders", this.A0F);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directoryQuerySearchNumberOfWords", this.A0G);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directoryQuerySearchRankOfSelectedCategory", this.A0H);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directoryQuerySearchVersion", this.A0I);
        Integer num4 = this.A04;
        if (num4 == null) {
            obj4 = null;
        } else {
            obj4 = num4.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directorySearchEntryPoint", obj4);
        Integer num5 = this.A05;
        if (num5 == null) {
            obj5 = null;
        } else {
            obj5 = num5.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directorySearchErrorType", obj5);
        Integer num6 = this.A06;
        if (num6 == null) {
            obj6 = null;
        } else {
            obj6 = num6.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directorySearchEventType", obj6);
        Integer num7 = this.A07;
        if (num7 == null) {
            obj7 = null;
        } else {
            obj7 = num7.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directorySearchLocationState", obj7);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directorySearchLoggingVersion", this.A0J);
        Integer num8 = this.A08;
        if (num8 == null) {
            obj8 = null;
        } else {
            obj8 = num8.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directorySelectedSubCategorySource", obj8);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directorySessionId", this.A0W);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "numberOfBusiness", this.A0K);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "numberOfRecentSearches", this.A0L);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "numberOfRootCategory", this.A0M);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "numberOfSubCategory", this.A0N);
        Integer num9 = this.A09;
        if (num9 == null) {
            obj9 = null;
        } else {
            obj9 = num9.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "popupAllowLocationSourceScreen", obj9);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "rankOfSelectedBusiness", this.A0O);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "rankOfSelectedNeighbourhood", this.A0P);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "rankOfSelectedRecentSearchItem", this.A0Q);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "rankOfSelectedRootCategory", this.A0R);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "selectedRootCategory", this.A0X);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "selectedSubCategory", this.A0Y);
        Integer num10 = this.A0A;
        if (num10 == null) {
            obj10 = null;
        } else {
            obj10 = num10.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "setLocationSource", obj10);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "subCategoryFilters", this.A0Z);
        sb.append("}");
        return sb.toString();
    }
}
