package X;

import android.os.Handler;
import com.facebook.redex.RunnableBRunnable0Shape7S0100000_I0_7;

/* renamed from: X.1Gc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C27121Gc extends C27131Gd {
    public final C15440nG A00;
    public final AnonymousClass133 A01;

    public C27121Gc(C15440nG r1, AnonymousClass133 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public final void A08() {
        if (this.A00.A00.A05(AbstractC15460nI.A18)) {
            for (AbstractC35911iz r1 : this.A01.A00()) {
                if (r1 instanceof C36211jV) {
                    C36211jV r12 = (C36211jV) r1;
                    Handler handler = r12.A00;
                    handler.removeCallbacksAndMessages(null);
                    handler.postDelayed(new RunnableBRunnable0Shape7S0100000_I0_7(r12, 21), 2000);
                }
            }
        }
    }
}
