package X;

import android.content.Context;
import com.whatsapp.migration.export.ui.ExportMigrationActivity;

/* renamed from: X.4qO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103214qO implements AbstractC009204q {
    public final /* synthetic */ ExportMigrationActivity A00;

    public C103214qO(ExportMigrationActivity exportMigrationActivity) {
        this.A00 = exportMigrationActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
