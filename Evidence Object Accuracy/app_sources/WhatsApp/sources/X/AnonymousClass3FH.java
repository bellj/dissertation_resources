package X;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.LongSparseArray;
import com.facebook.redex.RunnableBRunnable0Shape14S0100000_I1;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.3FH  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3FH {
    public final int A00;
    public final LongSparseArray A01;
    public final LongSparseArray A02;
    public final C92304Vj A03;
    public final ArrayList A04;

    public AnonymousClass3FH(C92304Vj r6, AnonymousClass28D r7) {
        Object A04;
        this.A03 = r6;
        int i = r7.A00;
        this.A00 = i;
        int size = r7.A0K().size();
        LongSparseArray longSparseArray = new LongSparseArray(size);
        C93294Zw r0 = r6.A00;
        if (r0 != null) {
            long j = (long) i;
            r0.A01.A00.A09(j, longSparseArray);
            this.A01 = longSparseArray;
            C93294Zw r02 = r6.A00;
            if (r02 != null) {
                C89824Ln r03 = r02.A00;
                if (r03 == null) {
                    A04 = null;
                } else {
                    A04 = r03.A00.A04(j, null);
                }
                this.A02 = (LongSparseArray) A04;
                this.A04 = C12980iv.A0w(size);
                return;
            }
            throw C12960it.A0U("Trying to access the LayoutCache from outside a layout call");
        }
        throw C12960it.A0U("Trying to access the LayoutCache from outside a layout call");
    }

    public final AnonymousClass3D8 A00(AnonymousClass3IP r11, AnonymousClass28D r12, int i, int i2) {
        C92304Vj r2 = this.A03;
        Context context = r2.A02;
        C106204vK r5 = new C106204vK(r12);
        Object obj = r2.A03;
        C14260l7 r0 = (C14260l7) obj;
        if (r0 != null) {
            r0.A01.get(R.id.bk_context_key_rendercore_extensions_creator);
        }
        return new AnonymousClass3D8(context, r11, r5, obj, r2.A01, i, i2);
    }

    public final C89954Ma A01(AnonymousClass28D r5, int i, int i2) {
        C89954Ma r2;
        C16700pc.A0E(r5, 0);
        long j = (long) r5.A00;
        LongSparseArray longSparseArray = this.A02;
        AnonymousClass3IP r3 = null;
        if (!(longSparseArray == null || (r2 = (C89954Ma) longSparseArray.get(j)) == null)) {
            AnonymousClass3D8 r32 = r2.A00;
            r3 = r32.A03.isDone() ? r32.A00() : r32.A05;
        }
        C89954Ma r33 = new C89954Ma(A00(r3, r5, i, i2), r5);
        this.A01.put(j, r33);
        this.A04.add(r33);
        return r33;
    }

    public final void A02() {
        int i = this.A00;
        ArrayList arrayList = this.A04;
        synchronized (AnonymousClass4ZQ.class) {
            Handler handler = AnonymousClass4ZQ.A00;
            if (handler == null) {
                HandlerThread handlerThread = new HandlerThread("BackgroundLayoutPreparer", 5);
                handlerThread.getThreadId();
                handlerThread.start();
                handler = new Handler(handlerThread.getLooper());
                AnonymousClass4ZQ.A00 = handler;
            }
            List list = AnonymousClass4ZQ.A01;
            Iterator it = list.iterator();
            while (it.hasNext()) {
                RunnableC55482ic r1 = (RunnableC55482ic) it.next();
                if (r1.A00 == i) {
                    r1.A02 = true;
                    handler.removeCallbacksAndMessages(r1);
                    it.remove();
                }
            }
            RunnableC55482ic r3 = new RunnableC55482ic(arrayList, i);
            list.add(r3);
            handler.postAtTime(new RunnableBRunnable0Shape14S0100000_I1(r3, 2), r3, 0);
        }
    }
}
