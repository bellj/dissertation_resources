package X;

import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;

/* renamed from: X.4xS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC107514xS implements AnonymousClass5SN {
    public C100624mD A00(C76733m7 r16, ByteBuffer byteBuffer) {
        String str;
        AnonymousClass5YX[] r1;
        C107454xM r0;
        CharsetDecoder charsetDecoder;
        if (this instanceof C76993mZ) {
            return ((C76993mZ) this).A06(byteBuffer.array(), byteBuffer.limit());
        }
        if (this instanceof C76983mY) {
            C76983mY r3 = (C76983mY) this;
            try {
                charsetDecoder = r3.A01;
                str = charsetDecoder.decode(byteBuffer).toString();
            } catch (CharacterCodingException unused) {
                r3.A01.reset();
                byteBuffer.rewind();
                try {
                    charsetDecoder = r3.A00;
                    str = charsetDecoder.decode(byteBuffer).toString();
                } catch (CharacterCodingException unused2) {
                    str = null;
                    r3.A00.reset();
                    byteBuffer.rewind();
                } catch (Throwable th) {
                    r3.A00.reset();
                    byteBuffer.rewind();
                    throw th;
                }
            } catch (Throwable th2) {
                r3.A01.reset();
                byteBuffer.rewind();
                throw th2;
            }
            charsetDecoder.reset();
            byteBuffer.rewind();
            byte[] bArr = new byte[byteBuffer.limit()];
            byteBuffer.get(bArr);
            String str2 = null;
            if (str == null) {
                r1 = new AnonymousClass5YX[1];
                r0 = new C107454xM(null, null, bArr);
            } else {
                Matcher matcher = C76983mY.A02.matcher(str);
                String str3 = null;
                for (int i = 0; matcher.find(i); i = matcher.end()) {
                    String A0F = C72463ee.A0F(matcher.group(1));
                    String group = matcher.group(2);
                    if (A0F != null) {
                        if (A0F.equals("streamurl")) {
                            str3 = group;
                        } else if (A0F.equals("streamtitle")) {
                            str2 = group;
                        }
                    }
                }
                r1 = new AnonymousClass5YX[1];
                r0 = new C107454xM(str2, str3, bArr);
            }
            r1[0] = r0;
            return new C100624mD(r1);
        } else if (this instanceof C76963mW) {
            C95304dT r02 = new C95304dT(byteBuffer.array(), byteBuffer.limit());
            return new C100624mD(new C107494xQ(r02.A0M(), r02.A0M(), Arrays.copyOfRange(r02.A02, r02.A01, r02.A00), r02.A0I(), r02.A0I()));
        } else if (byteBuffer.get() != 116) {
            return null;
        } else {
            C95054d0 r9 = new C95054d0(byteBuffer.array(), byteBuffer.limit());
            r9.A08(12);
            int A04 = r9.A04(12);
            C95314dV.A04(C12960it.A1T(r9.A00));
            int i2 = (r9.A02 + A04) - 4;
            r9.A09(C95054d0.A01(r9, 44, 12));
            r9.A08(16);
            ArrayList A0l = C12960it.A0l();
            while (true) {
                C95314dV.A04(C12960it.A1T(r9.A00));
                String str4 = null;
                if (r9.A02 >= i2) {
                    break;
                }
                r9.A08(48);
                int A042 = r9.A04(8);
                int A01 = C95054d0.A01(r9, 4, 12);
                C95314dV.A04(C12960it.A1T(r9.A00));
                int i3 = r9.A02 + A01;
                String str5 = null;
                while (true) {
                    C95314dV.A04(C12960it.A1T(r9.A00));
                    if (r9.A02 >= i3) {
                        break;
                    }
                    int A043 = r9.A04(8);
                    int A044 = r9.A04(8);
                    C95314dV.A04(C12960it.A1T(r9.A00));
                    int i4 = r9.A02 + A044;
                    if (A043 == 2) {
                        int A045 = r9.A04(16);
                        r9.A08(8);
                        if (A045 != 3) {
                        }
                        while (true) {
                            C95314dV.A04(C12960it.A1T(r9.A00));
                            if (r9.A02 < i4) {
                                int A046 = r9.A04(8);
                                Charset charset = C88814Hf.A01;
                                byte[] bArr2 = new byte[A046];
                                r9.A0B(bArr2, A046);
                                str4 = new String(bArr2, charset);
                                int A047 = r9.A04(8);
                                for (int i5 = 0; i5 < A047; i5++) {
                                    r9.A09(r9.A04(8));
                                }
                            }
                        }
                    } else if (A043 == 21) {
                        Charset charset2 = C88814Hf.A01;
                        byte[] bArr3 = new byte[A044];
                        r9.A0B(bArr3, A044);
                        str5 = new String(bArr3, charset2);
                    }
                    r9.A07(i4 << 3);
                }
                r9.A07(i3 << 3);
                if (!(str4 == null || str5 == null)) {
                    A0l.add(new C107414xI(A042, C12960it.A0d(str5, C12960it.A0j(str4))));
                }
            }
            if (!A0l.isEmpty()) {
                return new C100624mD(A0l);
            }
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0013, code lost:
        if (r2.arrayOffset() != 0) goto L_0x0015;
     */
    @Override // X.AnonymousClass5SN
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.C100624mD A8g(X.C76733m7 r4) {
        /*
            r3 = this;
            java.nio.ByteBuffer r2 = r4.A01
            int r0 = r2.position()
            if (r0 != 0) goto L_0x0015
            boolean r0 = r2.hasArray()
            if (r0 == 0) goto L_0x0015
            int r1 = r2.arrayOffset()
            r0 = 1
            if (r1 == 0) goto L_0x0016
        L_0x0015:
            r0 = 0
        L_0x0016:
            X.C95314dV.A03(r0)
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            int r0 = r4.flags
            r0 = r0 & r1
            boolean r0 = X.C12960it.A1V(r0, r1)
            if (r0 == 0) goto L_0x0026
            r0 = 0
            return r0
        L_0x0026:
            X.4mD r0 = r3.A00(r4, r2)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC107514xS.A8g(X.3m7):X.4mD");
    }
}
