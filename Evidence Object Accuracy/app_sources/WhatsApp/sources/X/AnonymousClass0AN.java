package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;

/* renamed from: X.0AN  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0AN extends Handler {
    public AnonymousClass0AN() {
        super(Looper.getMainLooper());
    }

    /* JADX INFO: finally extract failed */
    @Override // android.os.Handler
    public void handleMessage(Message message) {
        C04750Mx r2 = (C04750Mx) message.obj;
        if (message.what == 1) {
            RunnableC10240eG r4 = r2.A00;
            Object obj = r2.A01[0];
            if (r4.A03.get()) {
                try {
                    r4.A06.A0A(r4, obj);
                } catch (Throwable th) {
                    throw th;
                }
            } else {
                try {
                    AnonymousClass0ER r1 = r4.A06;
                    if (r1.A02 != r4) {
                        r1.A0A(r4, obj);
                    } else if (((AnonymousClass0QL) r1).A02) {
                        r1.A0B(obj);
                    } else {
                        r1.A04 = false;
                        SystemClock.uptimeMillis();
                        r1.A02 = null;
                        r1.A04(obj);
                    }
                } finally {
                    r4.A01.countDown();
                }
            }
            r4.A01.countDown();
            r4.A05 = AnonymousClass0JG.FINISHED;
        }
    }
}
