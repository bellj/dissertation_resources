package X;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.RadioButton;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.group.GroupAddPrivacyActivity;
import com.whatsapp.invites.NobodyDeprecatedDialogFragment;
import com.whatsapp.lastseen.LastSeenPrivacyActivity;
import com.whatsapp.profile.ProfilePhotoPrivacyActivity;
import com.whatsapp.status.posting.AboutStatusPrivacyActivity;

/* renamed from: X.2pc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC58522pc extends ActivityC13790kL {
    public RadioButton A00;
    public RadioButton A01;
    public RadioButton A02;
    public RadioButton A03;

    public void A2e() {
        if (this instanceof AboutStatusPrivacyActivity) {
            AboutStatusPrivacyActivity aboutStatusPrivacyActivity = (AboutStatusPrivacyActivity) this;
            int i = aboutStatusPrivacyActivity.A00;
            Intent A0A = C12970iu.A0A();
            A0A.putExtra("about", i);
            C12960it.A0q(aboutStatusPrivacyActivity, A0A);
        } else if (this instanceof ProfilePhotoPrivacyActivity) {
            ProfilePhotoPrivacyActivity profilePhotoPrivacyActivity = (ProfilePhotoPrivacyActivity) this;
            Intent A0A2 = C12970iu.A0A();
            A0A2.putExtra("profile_photo", profilePhotoPrivacyActivity.A00);
            C12960it.A0q(profilePhotoPrivacyActivity, A0A2);
        } else if (!(this instanceof LastSeenPrivacyActivity)) {
            GroupAddPrivacyActivity groupAddPrivacyActivity = (GroupAddPrivacyActivity) this;
            if (groupAddPrivacyActivity.A00 == 2 || !groupAddPrivacyActivity.A02) {
                Intent A0A3 = C12970iu.A0A();
                A0A3.putExtra("groupadd", groupAddPrivacyActivity.A00);
                C12960it.A0q(groupAddPrivacyActivity, A0A3);
                return;
            }
            groupAddPrivacyActivity.Adm(new NobodyDeprecatedDialogFragment());
        } else {
            LastSeenPrivacyActivity lastSeenPrivacyActivity = (LastSeenPrivacyActivity) this;
            Intent A0A4 = C12970iu.A0A();
            A0A4.putExtra("last_seen", lastSeenPrivacyActivity.A00);
            C12960it.A0q(lastSeenPrivacyActivity, A0A4);
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        A2e();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        int i;
        int i2;
        int i3;
        super.onCreate(bundle);
        setContentView(R.layout.generic_privacy_picker);
        AbstractC005102i A0N = C12970iu.A0N(this);
        A0N.A0M(true);
        boolean z = this instanceof AboutStatusPrivacyActivity;
        if (z) {
            i = R.string.settings_privacy_info;
        } else if (this instanceof ProfilePhotoPrivacyActivity) {
            i = R.string.settings_privacy_profile_photo;
        } else if (!(this instanceof LastSeenPrivacyActivity)) {
            i = R.string.settings_privacy_group_add_permissions;
        } else {
            i = R.string.settings_privacy_last_seen;
        }
        A0N.A0A(i);
        this.A01 = (RadioButton) findViewById(R.id.my_contacts_button);
        this.A00 = (RadioButton) findViewById(R.id.everyone_btn);
        this.A02 = (RadioButton) findViewById(R.id.my_contacts_except_button);
        this.A03 = (RadioButton) findViewById(R.id.nobody_btn);
        TextView A0M = C12970iu.A0M(this, R.id.header);
        if (z) {
            i2 = R.string.settings_privacy_about_privacy_title;
        } else if (this instanceof ProfilePhotoPrivacyActivity) {
            i2 = R.string.settings_privacy_profile_photo_privacy_title;
        } else if (!(this instanceof LastSeenPrivacyActivity)) {
            i2 = R.string.settings_privacy_group_add_permissions_title;
        } else {
            i2 = R.string.settings_privacy_last_seen_privacy_title;
        }
        A0M.setText(i2);
        if (z || (this instanceof ProfilePhotoPrivacyActivity)) {
            findViewById(R.id.footer).setVisibility(8);
        } else {
            if (!(this instanceof LastSeenPrivacyActivity)) {
                i3 = R.string.settings_privacy_group_add_permissions_message;
            } else {
                i3 = R.string.settings_privacy_last_seen_privacy_message;
            }
            C12970iu.A0M(this, R.id.footer).setText(i3);
        }
        this.A01.setText(R.string.privacy_contacts);
        this.A00.setText(R.string.privacy_everyone);
        this.A02.setText(R.string.group_add_permission_blacklist);
        this.A03.setText(R.string.privacy_nobody);
        C12960it.A12(this.A01, this, 8);
        C12960it.A12(this.A00, this, 9);
        C12960it.A12(this.A02, this, 10);
        C12960it.A12(this.A03, this, 7);
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        A2e();
        return false;
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        int i;
        super.onResume();
        if (this instanceof AboutStatusPrivacyActivity) {
            i = ((AboutStatusPrivacyActivity) this).A00;
        } else if (this instanceof ProfilePhotoPrivacyActivity) {
            i = ((ProfilePhotoPrivacyActivity) this).A00;
        } else if (!(this instanceof LastSeenPrivacyActivity)) {
            i = ((GroupAddPrivacyActivity) this).A00;
        } else {
            i = ((LastSeenPrivacyActivity) this).A00;
        }
        boolean z = false;
        this.A01.setChecked(C12970iu.A1W(i));
        this.A00.setChecked(C12960it.A1T(i));
        this.A03.setChecked(C12960it.A1V(i, 2));
        RadioButton radioButton = this.A02;
        if (i == 3) {
            z = true;
        }
        radioButton.setChecked(z);
    }
}
