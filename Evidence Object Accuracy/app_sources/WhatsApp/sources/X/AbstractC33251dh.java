package X;

import android.content.Context;
import com.whatsapp.util.Log;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicLong;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/* renamed from: X.1dh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC33251dh {
    public final AbstractC33221de A00;
    public final C33241dg A01;
    public final C17050qB A02;
    public final C21780xy A03;
    public final AnonymousClass15D A04;

    public AbstractC33251dh(AbstractC33221de r1, C33241dg r2, C17050qB r3, C21780xy r4, AnonymousClass15D r5) {
        this.A00 = r1;
        this.A04 = r5;
        this.A02 = r3;
        this.A01 = r2;
        this.A03 = r4;
    }

    public C44561zA A00() {
        int i;
        String str;
        String str2;
        AnonymousClass2AX r1;
        C44641zJ r0;
        C44641zJ r2;
        if (this instanceof AnonymousClass2AU) {
            return new C44561zA(1, null);
        }
        AnonymousClass2AW r7 = (AnonymousClass2AW) this;
        if (r7 instanceof C58732rI) {
            return new C44561zA(1, null);
        }
        StringBuilder sb = new StringBuilder("EncryptedBackupFile/verifyIntegrity/");
        EnumC16570pG A08 = r7.A08();
        sb.append(A08);
        Log.i(sb.toString());
        C28181Ma r4 = new C28181Ma("BackupFile/getFileDigestWithoutFooter/calculating-actual-digest");
        MessageDigest instance = MessageDigest.getInstance("MD5");
        StringBuilder sb2 = new StringBuilder("BackupFile/getFileDigestWithoutFooter/initial digest = ");
        sb2.append(C003501n.A04(instance.digest()));
        Log.i(sb2.toString());
        AbstractC33221de r8 = ((AbstractC33251dh) r7).A00;
        long AKR = r8.AKR();
        boolean z = r7 instanceof C58762rL;
        if (z) {
            i = 16;
        } else if (!(((C58752rK) r7) instanceof C58732rI)) {
            i = 20;
        } else {
            i = 0;
        }
        String ADG = r8.ADG(instance, AKR - ((long) i));
        r4.A01();
        StringBuilder sb3 = new StringBuilder("msgstore-integrity-checker/verify-integrity/actual-digest/  ");
        sb3.append(ADG);
        Log.i(sb3.toString());
        C47352Ah A06 = r7.A06();
        C16600pJ r22 = r7.A04;
        StringBuilder sb4 = new StringBuilder();
        sb4.append("EncryptedBackupFile/verifyIntegrity/");
        sb4.append(A08);
        sb4.append(" ");
        sb4.append(r8);
        sb4.append(" size=");
        sb4.append(r8.AKR());
        sb4.append(" modification time = ");
        sb4.append(r8.AKN());
        sb4.append("footer: ");
        sb4.append(A06);
        sb4.append("actualDigest: ");
        sb4.append(ADG);
        String obj = sb4.toString();
        int i2 = 2;
        r22.A00(obj, 2);
        if (A06 != null) {
            if (ADG == null) {
                byte[] bArr = A06.A01;
                if (bArr != null) {
                    str = Arrays.toString(bArr);
                } else {
                    str = "null";
                }
            } else {
                String A0A = r7.A0A();
                if (!(A0A == null || (r1 = r7.A00) == null)) {
                    if (r1 instanceof AnonymousClass2AZ) {
                        r0 = ((AnonymousClass2AZ) r1).A00;
                    } else if (r1 instanceof C47332Af) {
                        r0 = ((C47332Af) r1).A00;
                    }
                    if (C32781cj.A0E(r0, A0A)) {
                        i2 = 4;
                        AnonymousClass2AX r12 = r7.A00;
                        if (r12 instanceof AnonymousClass2AZ) {
                            r2 = ((AnonymousClass2AZ) r12).A00;
                        } else if (r12 instanceof C47332Af) {
                            r2 = ((C47332Af) r12).A00;
                        }
                        if ((r2.A00 & 8) == 8) {
                            C44631zI r13 = r2.A02;
                            if (r13 == null) {
                                r13 = C44631zI.A0e;
                            }
                            if ((r13.A01 & 4) == 4) {
                                str = r13.A06;
                            }
                        }
                    }
                }
                byte[] bArr2 = A06.A01;
                if (!z) {
                    if (bArr2 == null) {
                        Log.e("BackupFileCrypt12/verifyFooterIntegrity/jidSuffix is null");
                    } else {
                        str2 = r7.A0A();
                    }
                } else if (bArr2 != null) {
                    str = C47352Ah.A00(bArr2);
                } else {
                    str2 = null;
                }
                return A06.A01(r22, ADG, str2);
            }
            return new C44561zA(i2, str);
        }
        str = null;
        return new C44561zA(i2, str);
    }

    public C44561zA A01(C44581zC r25, C16590pI r26, File file, int i, int i2, boolean z) {
        C32871cs r4;
        C44561zA r1;
        byte[] bArr;
        InputStream inputStream;
        if (!(this instanceof AnonymousClass2AU)) {
            AnonymousClass2AW r5 = (AnonymousClass2AW) this;
            r4 = new C32871cs(((AbstractC33251dh) r5).A03.A00, file);
            try {
                InputStream A09 = r5.A09();
                AnonymousClass2AX A07 = r5.A07(A09, true);
                r5.A00 = A07;
                if (A07 == null) {
                    r1 = new C44561zA(5, null);
                } else {
                    r1 = r5.A00();
                    if (r1.A00 == 1) {
                        Log.i("BackupFile/restoreSingleFileBackup/file-integrity-check/success");
                        StringBuilder sb = new StringBuilder();
                        sb.append("BackupFile/restoreSingleFileBackup/key ");
                        EnumC16570pG A08 = r5.A08();
                        sb.append(A08);
                        Log.i(sb.toString());
                        AbstractC33221de r3 = ((AbstractC33251dh) r5).A00;
                        r3.AKR();
                        AnonymousClass15D r42 = ((AbstractC33251dh) r5).A04;
                        long AKR = r3.AKR();
                        AnonymousClass2AX r6 = r5.A00;
                        byte[] A01 = r6.A01();
                        if (!(r6 instanceof C47332Af)) {
                            bArr = ((C47312Ad) r6).A02;
                        } else {
                            bArr = ((C47332Af) r6).A01;
                        }
                        r42.A05();
                        AtomicLong atomicLong = new AtomicLong();
                        synchronized (r42) {
                            int i3 = AnonymousClass4GU.A00[A08.ordinal()];
                            if (i3 == 1) {
                                inputStream = AnonymousClass15D.A00(A09, atomicLong, r42.A00, A01, bArr);
                            } else if (i3 == 2) {
                                inputStream = AnonymousClass15D.A00(A09, atomicLong, r42.A01, A01, bArr);
                            } else if (i3 == 3) {
                                inputStream = AnonymousClass15D.A00(A09, atomicLong, r42.A02, A01, bArr);
                            } else {
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append("unsupported key selector ");
                                sb2.append(A08);
                                throw new IllegalArgumentException(sb2.toString());
                            }
                        }
                        byte[] bArr2 = new byte[C25981Bo.A0F];
                        while (true) {
                            int read = inputStream.read(bArr2);
                            if (read < 0) {
                                break;
                            }
                            r4.write(bArr2, 0, read);
                            if (r25 != null && i2 > 0) {
                                r25.A00(i, i2, atomicLong.get(), AKR);
                            }
                        }
                        inputStream.close();
                        r4.flush();
                        if (z) {
                            AnonymousClass2AX r62 = r5.A00;
                            if (!(r62 instanceof C47332Af)) {
                                C47312Ad r63 = (C47312Ad) r62;
                                byte[] bArr3 = r63.A03;
                                boolean z2 = true;
                                boolean z3 = false;
                                if (bArr3 != null) {
                                    z3 = true;
                                }
                                AnonymousClass009.A0C("backup-prefix/get-key/key is null", z3);
                                byte[] bArr4 = r63.A01;
                                if (bArr4 == null) {
                                    z2 = false;
                                }
                                AnonymousClass009.A0C("backup-prefix/get-key/account hash is null", z2);
                                Context context = r26.A00;
                                C32821cn r32 = r63.A00;
                                C32781cj.A09(context, r32.A00, r32.A04, bArr3, bArr4, r32.A02);
                                C44541z8 A05 = C32781cj.A05(context);
                                if (A05 != null) {
                                    A05.toString();
                                }
                            }
                        }
                    }
                }
                A09.close();
                r4.close();
                return r1;
            } finally {
            }
        } else {
            r4 = new C32871cs(this.A03.A00, file);
            try {
                FileInputStream AD0 = this.A00.AD0();
                FileChannel channel = AD0.getChannel();
                WritableByteChannel newChannel = Channels.newChannel(r4);
                long j = 0;
                for (long j2 = 0; j2 < channel.size(); j2 += 131072) {
                    j += channel.transferTo(j2, Math.min(131072L, channel.size() - j2), newChannel);
                    if (r25 != null && i2 > 0) {
                        r25.A00(i, i2, j, channel.size());
                    }
                }
                r4.flush();
                C44561zA r12 = new C44561zA(1, null);
                AD0.close();
                return r12;
            } finally {
                try {
                    r4.close();
                } catch (Throwable unused) {
                }
            }
        }
    }

    public AbstractC37461mR A02(Context context) {
        if (this instanceof AnonymousClass2AU) {
            return new C68103Uc((AnonymousClass2AU) this);
        }
        AnonymousClass2AW r1 = (AnonymousClass2AW) this;
        if (r1.A04(context)) {
            return new C68093Ub(r1);
        }
        Log.e("EncryptedBackupFile/failed to prepare for backup");
        return null;
    }

    public void A03(C16610pK r15, File file) {
        DeflaterOutputStream ACp;
        byte[] bArr;
        DeflaterOutputStream deflaterOutputStream;
        if (!(this instanceof AnonymousClass2AU)) {
            AnonymousClass2AW r6 = (AnonymousClass2AW) this;
            boolean z = false;
            if (r6.A00 != null) {
                z = true;
            }
            AnonymousClass009.A0C("prefix has not been initialized", z);
            File A00 = ((C32861cr) ((AbstractC33251dh) r6).A02.A05.get()).A02.A00("");
            FileOutputStream fileOutputStream = new FileOutputStream(A00);
            MessageDigest instance = MessageDigest.getInstance("MD5");
            StringBuilder sb = new StringBuilder("BackupFile/get-output-stream/initial digest = ");
            sb.append(C003501n.A04(instance.digest()));
            Log.i(sb.toString());
            instance.reset();
            ACp = new C71683dL(r6, A00, fileOutputStream, instance);
            try {
                FileInputStream fileInputStream = new FileInputStream(file);
                r6.A00.A00(ACp);
                AnonymousClass15D r3 = ((AbstractC33251dh) r6).A04;
                EnumC16570pG A08 = r6.A08();
                AnonymousClass2AX r1 = r6.A00;
                byte[] A01 = r1.A01();
                if (!(r1 instanceof C47332Af)) {
                    bArr = ((C47312Ad) r1).A02;
                } else {
                    bArr = ((C47332Af) r1).A01;
                }
                synchronized (r3) {
                    r3.A05();
                    int i = AnonymousClass4GU.A00[A08.ordinal()];
                    if (i == 1) {
                        deflaterOutputStream = new DeflaterOutputStream(AnonymousClass15D.A02(ACp, r3.A03, A01, bArr), new Deflater(1, false));
                    } else if (i == 2) {
                        deflaterOutputStream = new DeflaterOutputStream(AnonymousClass15D.A02(ACp, r3.A04, A01, bArr), new Deflater(1, false));
                    } else if (i == 3) {
                        deflaterOutputStream = new DeflaterOutputStream(AnonymousClass15D.A02(ACp, r3.A05, A01, bArr), new Deflater(1, false));
                    } else {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("Unexpected key selector (");
                        sb2.append(A08);
                        sb2.append(")");
                        throw new IllegalArgumentException(sb2.toString());
                    }
                }
                try {
                    long length = file.length();
                    byte[] bArr2 = new byte[C25981Bo.A0F];
                    long j = 0;
                    int i2 = -1;
                    while (true) {
                        int read = fileInputStream.read(bArr2);
                        if (read >= 0) {
                            ACp.write(bArr2, 0, read);
                            j += (long) read;
                            int i3 = (int) ((100 * j) / length);
                            if (i2 != i3) {
                                String.format(Locale.ENGLISH, "encrypter/encrypt %d/%d (%d%%)", Long.valueOf(j), Long.valueOf(length), Integer.valueOf(i3));
                                if (r15 != null) {
                                    ((AbstractC16350or) r15.A00).A02.A01(Integer.valueOf(i3));
                                }
                                i2 = i3;
                            }
                        } else {
                            ACp.close();
                            fileInputStream.close();
                            ACp.close();
                            return;
                        }
                    }
                } finally {
                }
            } finally {
            }
        } else {
            ACp = this.A00.ACp(this.A02);
            try {
                WritableByteChannel newChannel = Channels.newChannel(ACp);
                FileInputStream fileInputStream2 = new FileInputStream(file);
                try {
                    FileChannel channel = fileInputStream2.getChannel();
                    C14350lI.A0H(channel, newChannel);
                    if (channel != null) {
                        channel.close();
                    }
                    fileInputStream2.close();
                    if (newChannel != null) {
                        newChannel.close();
                    }
                    ACp.close();
                } catch (Throwable th) {
                    try {
                        fileInputStream2.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            } finally {
                try {
                    ACp.close();
                } catch (Throwable unused2) {
                }
            }
        }
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:35:? */
    /* JADX DEBUG: Multi-variable search result rejected for r6v2, resolved type: X.1dg */
    /* JADX DEBUG: Multi-variable search result rejected for r6v3, resolved type: X.1cn */
    /* JADX WARN: Multi-variable type inference failed */
    public boolean A04(Context context) {
        AnonymousClass2AX r6;
        AnonymousClass2AX r5;
        byte[] A03;
        byte[] A00;
        if (!(this instanceof AnonymousClass2AU)) {
            AnonymousClass2AW r0 = (AnonymousClass2AW) this;
            if (!(r0 instanceof C58762rL)) {
                AnonymousClass009.A0F(!r0.A02.A04());
                r6 = null;
                r6 = null;
                r6 = null;
                try {
                    C44541z8 A05 = C32781cj.A05(context);
                    if (A05 == null) {
                        r0.A04.A00("msgstore/backupDatabase/key is null", 3);
                        r5 = null;
                    } else {
                        StringBuilder sb = new StringBuilder();
                        sb.append("msgstore/backupDatabase/createPrefix v=");
                        C32821cn r4 = A05.A00;
                        sb.append(r4.A00);
                        Log.i(sb.toString());
                        r5 = new C47312Ad(r4, A05.A02, A05.A01, r4.A01);
                    }
                } catch (Exception e) {
                    Log.w("msgstore/backupDatabase/key/error", e);
                    r5 = r6;
                    r6 = r6;
                }
            } else {
                C58762rL r2 = (C58762rL) r0;
                if (!(r2 instanceof C58742rJ)) {
                    r5 = null;
                    try {
                        C44541z8 A052 = C32781cj.A05(context);
                        if (A052 == null) {
                            ((AnonymousClass2AW) r2).A04.A00("msgstore/backupDatabase/key is null", 3);
                        } else {
                            StringBuilder sb2 = new StringBuilder("msgstore/backupDatabase/key v=");
                            C32821cn r62 = A052.A00;
                            sb2.append(r62.A00);
                            Log.i(sb2.toString());
                            r5 = new AnonymousClass2AZ(r62, ((AbstractC33251dh) r2).A01, r2.A00, "2.22.17.70", r2.A0A(), A052.A02, A052.A01, r62.A01);
                            r6 = r62;
                        }
                    } catch (Exception e2) {
                        Log.w("msgstore/backupDatabase/key/error", e2);
                    }
                } else {
                    C15820nx r3 = ((AnonymousClass2AW) r2).A02;
                    r5 = null;
                    if (!(!r3.A04() || (A03 = r3.A01.A03()) == null || (A00 = C32891cu.A00(A03, C15820nx.A0A, 32)) == null)) {
                        byte[] A0D = C003501n.A0D(16);
                        String A0A = r2.A0A();
                        C33241dg r63 = ((AbstractC33251dh) r2).A01;
                        r5 = new C47332Af(r63, "2.22.17.70", A0A, A00, A0D);
                        r6 = r63;
                    }
                }
            }
            r0.A00 = r5;
            r0 = null;
            if (r5 == null) {
                return false;
            }
        }
        return true;
    }

    public boolean A05(AbstractC37491mU r11, boolean z) {
        BufferedInputStream bufferedInputStream;
        C32871cs r1;
        byte[] bArr;
        C32871cs r12;
        if (!(this instanceof AnonymousClass2AU)) {
            AnonymousClass2AW r3 = (AnonymousClass2AW) this;
            AbstractC33221de r13 = ((AbstractC33251dh) r3).A00;
            bufferedInputStream = new BufferedInputStream(r13.ADW());
            try {
                AnonymousClass2AX A07 = r3.A07(bufferedInputStream, true);
                r3.A00 = A07;
                if (A07 == null) {
                    Log.e("EncryptedBackupFile/restore-multi-file-backup/restore/failed to read prefix");
                } else {
                    r13.AKR();
                    AtomicLong atomicLong = new AtomicLong();
                    AnonymousClass15D r4 = ((AbstractC33251dh) r3).A04;
                    EnumC16570pG A08 = r3.A08();
                    AnonymousClass2AX r14 = r3.A00;
                    byte[] A01 = r14.A01();
                    if (!(r14 instanceof C47332Af)) {
                        bArr = ((C47312Ad) r14).A02;
                    } else {
                        bArr = ((C47332Af) r14).A01;
                    }
                    ZipInputStream A03 = r4.A03(A08, bufferedInputStream, atomicLong, A01, bArr);
                    try {
                        for (ZipEntry nextEntry = A03.getNextEntry(); nextEntry != null; nextEntry = A03.getNextEntry()) {
                            File file = (File) r11.apply(nextEntry.getName());
                            if (file != null) {
                                if (z) {
                                    r12 = ((AbstractC33251dh) r3).A02.A00(file);
                                } else {
                                    r12 = new C32871cs(((AbstractC33251dh) r3).A03.A00, file);
                                }
                                try {
                                    C14350lI.A0G(A03, r12);
                                    r12.close();
                                } catch (Throwable th) {
                                    try {
                                        r12.close();
                                    } catch (Throwable unused) {
                                    }
                                    throw th;
                                }
                            }
                            A03.closeEntry();
                        }
                        A03.close();
                        bufferedInputStream.close();
                        return true;
                    } catch (Exception e) {
                        Log.e("EncryptedBackupFile/restore-multi-file-backup/restore", e);
                        A03.close();
                    }
                }
                bufferedInputStream.close();
                return false;
            } finally {
            }
        } else {
            bufferedInputStream = new BufferedInputStream(this.A00.ADW());
            try {
                ZipInputStream A032 = this.A04.A03(EnumC16570pG.A08, bufferedInputStream, new AtomicLong(), null, null);
                try {
                    for (ZipEntry nextEntry2 = A032.getNextEntry(); nextEntry2 != null; nextEntry2 = A032.getNextEntry()) {
                        File file2 = (File) r11.apply(nextEntry2.getName());
                        if (file2 != null) {
                            if (z) {
                                r1 = this.A02.A00(file2);
                            } else {
                                r1 = new C32871cs(this.A03.A00, file2);
                            }
                            try {
                                C14350lI.A0G(A032, r1);
                                r1.close();
                            } catch (Throwable th2) {
                                try {
                                    r1.close();
                                } catch (Throwable unused2) {
                                }
                                throw th2;
                            }
                        }
                        A032.closeEntry();
                    }
                    A032.close();
                    bufferedInputStream.close();
                    return true;
                } catch (Exception e2) {
                    Log.e("unencrypted-backup-file/restore-multi-file-backup/restore failed", e2);
                    A032.close();
                    bufferedInputStream.close();
                    return false;
                }
            } finally {
            }
        }
    }
}
