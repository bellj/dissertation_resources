package X;

/* renamed from: X.3Ct  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C63743Ct {
    public final /* synthetic */ C14260l7 A00;
    public final /* synthetic */ C14230l4 A01;
    public final /* synthetic */ AbstractC14200l1 A02;
    public final /* synthetic */ AbstractC14200l1 A03;

    public C63743Ct(C14260l7 r1, C14230l4 r2, AbstractC14200l1 r3, AbstractC14200l1 r4) {
        this.A03 = r3;
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r4;
    }

    public void A00(AnonymousClass49P r5) {
        AbstractC14200l1 r3 = this.A02;
        if (r3 != null) {
            Object[] A1a = C12980iv.A1a();
            A1a[0] = this.A00;
            A1a[1] = r5.toString();
            C14250l6.A00(this.A01, new C14220l3(C16770pj.A0I(A1a)), r3);
        }
    }
}
