package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;

/* renamed from: X.2tq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59022tq extends AnonymousClass2EI {
    public final C63923Dl A00;
    public final C91864Tn A01;
    public final AnonymousClass2SJ A02;
    public final C18640sm A03;
    public final C19870uo A04;
    public final C17220qS A05;
    public final C19840ul A06;

    public C59022tq(C14650lo r1, C63923Dl r2, C91864Tn r3, AnonymousClass2SJ r4, C18640sm r5, C19870uo r6, C17220qS r7, C19840ul r8) {
        super(r1);
        this.A02 = r4;
        this.A06 = r8;
        this.A05 = r7;
        this.A01 = r3;
        this.A03 = r5;
        this.A00 = r2;
        this.A04 = r6;
    }

    public void A02() {
        if (!this.A03.A0B()) {
            this.A00.A00(-1);
            return;
        }
        String A01 = this.A05.A01();
        C91864Tn r4 = this.A01;
        String str = r4.A04;
        if (str == null) {
            this.A06.A03("view_collection_details_tag");
        }
        C19870uo r8 = this.A04;
        ArrayList A0l = C12960it.A0l();
        AnonymousClass2EI.A00("limit", Integer.toString(r4.A01), A0l);
        AnonymousClass2EI.A00("width", Integer.toString(r4.A02), A0l);
        AnonymousClass2EI.A00("height", Integer.toString(r4.A00), A0l);
        AnonymousClass2EI.A00("is_category", Boolean.toString(r4.A07), A0l);
        String str2 = r4.A06;
        if (str2 != null) {
            AnonymousClass2EI.A00("catalog_session_id", str2, A0l);
        }
        if (str != null) {
            AnonymousClass2EI.A00("after", str, A0l);
        }
        C14650lo r0 = super.A01;
        UserJid userJid = r4.A03;
        String A012 = r0.A07.A01(userJid);
        if (A012 != null) {
            AnonymousClass2EI.A00("direct_connection_encrypted_info", A012, A0l);
        }
        AnonymousClass1W9[] r2 = new AnonymousClass1W9[2];
        int A1a = C12990iw.A1a("id", r4.A05, r2);
        r2[1] = new AnonymousClass1W9(userJid, "biz_jid");
        AnonymousClass1V8 r42 = new AnonymousClass1V8("collection", r2, (AnonymousClass1V8[]) A0l.toArray(new AnonymousClass1V8[A1a]));
        AnonymousClass1W9[] r3 = new AnonymousClass1W9[5];
        r3[A1a] = new AnonymousClass1W9(AnonymousClass1VY.A00, "to");
        C12960it.A1M("id", A01, r3, 1);
        C12960it.A1M("smax_id", "30", r3, 2);
        C12960it.A1M("xmlns", "w:biz:catalog", r3, 3);
        C12960it.A1M("type", "get", r3, 4);
        r8.A02(this, new AnonymousClass1V8(r42, "iq", r3), A01, 270);
        StringBuilder A0k = C12960it.A0k("app/sendGetCollectionProductList jid=");
        A0k.append(userJid);
        Log.i(C12960it.A0d(" success", A0k));
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        if (this.A01.A04 == null) {
            this.A06.A02("view_collection_details_tag");
        }
        Log.e("GetCollectionProductListProtocol/sendGetCollectionProductLis/delivery-error");
        this.A00.A00(-1);
    }

    @Override // X.AnonymousClass1W3
    public void APB(UserJid userJid) {
        Log.e("GetCollectionProductListProtocol/sendGetCollectionProductLis/direct-connection-error");
        this.A00.A00(421);
    }

    @Override // X.AnonymousClass1W3
    public void APC(UserJid userJid) {
        Log.i("GetCollectionProductListProtocolonDirectConnectionSucceeded/retry-request");
        A02();
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r4, String str) {
        C91864Tn r2 = this.A01;
        if (r2.A04 == null) {
            this.A06.A02("view_collection_details_tag");
        }
        Log.e("GetCollectionProductListProtocol/sendGetCollectionProductList/response-error");
        int A00 = C41151sz.A00(r4);
        if (!A01(r2.A03, A00)) {
            this.A00.A00(A00);
        }
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r5, String str) {
        C44671zM A01;
        C91864Tn r3 = this.A01;
        if (r3.A04 == null) {
            this.A06.A02("view_collection_details_tag");
        }
        AnonymousClass2SJ r1 = this.A02;
        AnonymousClass1V8 A0E = r5.A0E("collection");
        if (A0E == null || (A01 = r1.A01(A0E)) == null) {
            Log.e(C12970iu.A0s(r3.A03, C12960it.A0k("GetCollectionProductListProtocol/sendGetCollectionProductList/onSuccess/emptyPage jid=")));
            this.A00.A00(0);
            return;
        }
        C90084Mn r2 = new C90084Mn(AnonymousClass3HN.A00(A0E.A0E("paging")), A01);
        StringBuilder A0k = C12960it.A0k("GetCollectionProductListProtocol/sendGetCollectionProductList/onSuccess jid=");
        A0k.append(r3.A03);
        C12960it.A1F(A0k);
        this.A00.A01(r2, r3);
    }
}
