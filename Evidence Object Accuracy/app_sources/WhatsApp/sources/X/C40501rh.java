package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1rh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C40501rh {
    public final C16630pM A00;
    public final String A01;

    public C40501rh(C16630pM r1, String str) {
        this.A00 = r1;
        this.A01 = str;
    }

    public static final C40511ri A00(String str) {
        String str2;
        try {
            JSONObject jSONObject = new JSONObject(str);
            UserJid userJid = UserJid.get(jSONObject.getString("uj"));
            String string = jSONObject.getString("s");
            if (jSONObject.has("a")) {
                str2 = jSONObject.getString("a");
            } else {
                str2 = null;
            }
            C40521rj r4 = new C40521rj(userJid, string, str2, jSONObject.getLong("ct"), jSONObject.getLong("lit"));
            r4.A02 = jSONObject.getBoolean("hcslm");
            r4.A00 = jSONObject.optInt("brc", -1);
            r4.A01 = jSONObject.optLong("fmts", -1);
            return new C40511ri(r4);
        } catch (AnonymousClass1MW e) {
            StringBuilder sb = new StringBuilder("CTWA: EntryPointConversionStore/getConversion/invalid jid error");
            sb.append(e);
            Log.e(sb.toString());
            return null;
        } catch (JSONException e2) {
            StringBuilder sb2 = new StringBuilder("CTWA: EntryPointConversionStore/getConversion/json error");
            sb2.append(e2);
            Log.e(sb2.toString());
            return null;
        }
    }
}
