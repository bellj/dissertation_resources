package X;

import android.database.Cursor;
import com.facebook.redex.EmptyBaseRunnable0;

/* renamed from: X.2ix  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class RunnableC55602ix extends EmptyBaseRunnable0 implements Runnable {
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ AnonymousClass3WN A02;
    public final /* synthetic */ C30421Xi A03;
    public final /* synthetic */ boolean A04;

    public /* synthetic */ RunnableC55602ix(AnonymousClass3WN r1, C30421Xi r2, int i, int i2, boolean z) {
        this.A02 = r1;
        this.A03 = r2;
        this.A00 = i;
        this.A04 = z;
        this.A01 = i2;
    }

    @Override // java.lang.Runnable
    public final void run() {
        AbstractC15340mz r4;
        AnonymousClass3WN r3 = this.A02;
        C30421Xi r5 = this.A03;
        int i = this.A00;
        boolean z = this.A04;
        int i2 = this.A01;
        AnonymousClass1A0 r1 = (AnonymousClass1A0) r3.A0C.get();
        AbstractC14640lm r10 = r5.A0z.A00;
        if (r10 != null) {
            C15650ng r9 = r1.A00;
            Cursor cursor = r9.A0A(r10, 1, r5.A11 + 1, -1, false).A00;
            try {
                if (cursor.moveToNext()) {
                    r4 = r9.A0K.A01(cursor);
                    if (r4 instanceof C30421Xi) {
                        if (!r4.A0z.A02) {
                            if (!AbstractC15340mz.A00((C30421Xi) r4).A0P) {
                            }
                        }
                        cursor.close();
                        r3.A01.A0H(new Runnable(r4, r5, i, i2, z) { // from class: X.2j4
                            public final /* synthetic */ int A00;
                            public final /* synthetic */ int A01;
                            public final /* synthetic */ AbstractC15340mz A03;
                            public final /* synthetic */ C30421Xi A04;
                            public final /* synthetic */ boolean A05;

                            {
                                this.A03 = r2;
                                this.A00 = r4;
                                this.A04 = r3;
                                this.A05 = r6;
                                this.A01 = r5;
                            }

                            /* JADX WARNING: Code restructure failed: missing block: B:12:0x0040, code lost:
                                if (r2 == 10) goto L_0x0042;
                             */
                            /* JADX WARNING: Code restructure failed: missing block: B:16:0x0058, code lost:
                                if (r8 == null) goto L_0x005a;
                             */
                            @Override // java.lang.Runnable
                            /* Code decompiled incorrectly, please refer to instructions dump. */
                            public final void run() {
                                /*
                                    r9 = this;
                                    X.3WN r7 = X.AnonymousClass3WN.this
                                    X.0mz r6 = r9.A03
                                    int r4 = r9.A00
                                    X.1Xi r2 = r9.A04
                                    boolean r1 = r9.A05
                                    int r5 = r9.A01
                                    java.lang.ref.WeakReference r0 = r7.A0D
                                    java.lang.Object r8 = r0.get()
                                    android.app.Activity r8 = (android.app.Activity) r8
                                    if (r6 == 0) goto L_0x0058
                                    if (r8 == 0) goto L_0x005a
                                    X.0m9 r0 = r7.A0A
                                    boolean r0 = X.AnonymousClass3GJ.A01(r0, r6, r2, r1)
                                    if (r0 == 0) goto L_0x0066
                                    r0 = 100
                                    if (r4 >= r0) goto L_0x0066
                                    X.1Xi r6 = (X.C30421Xi) r6
                                    X.19D r2 = r7.A06
                                    X.55D r3 = new X.55D
                                    r3.<init>(r7, r6, r4)
                                    r1 = 0
                                    r0 = 1
                                    X.1hP r4 = r2.A01(r8, r1, r0)
                                    r4.A0O = r6
                                    r4.A0A = r0
                                    int r2 = r6.A0C
                                    r0 = 9
                                    if (r2 == r0) goto L_0x0042
                                    r1 = 10
                                    r0 = 0
                                    if (r2 != r1) goto L_0x0043
                                L_0x0042:
                                    r0 = 1
                                L_0x0043:
                                    r4.A0T = r0
                                    r4.A0J = r3
                                    android.content.Context r3 = r8.getBaseContext()
                                    X.0mE r2 = r7.A01
                                    r1 = 29
                                    com.facebook.redex.RunnableBRunnable0Shape3S0300000_I1 r0 = new com.facebook.redex.RunnableBRunnable0Shape3S0300000_I1
                                    r0.<init>(r7, r4, r6, r1)
                                    X.AnonymousClass3GK.A01(r3, r2, r0, r5)
                                L_0x0057:
                                    return
                                L_0x0058:
                                    if (r8 != 0) goto L_0x0066
                                L_0x005a:
                                    android.view.View r0 = r7.A00
                                    android.content.Context r0 = r0.getContext()
                                L_0x0060:
                                    if (r4 <= 0) goto L_0x0057
                                    X.AnonymousClass3GK.A00(r0, r5)
                                    return
                                L_0x0066:
                                    android.content.Context r0 = r8.getBaseContext()
                                    goto L_0x0060
                                */
                                throw new UnsupportedOperationException("Method not decompiled: X.RunnableC55652j4.run():void");
                            }
                        });
                    }
                }
                cursor.close();
            } catch (Throwable th) {
                if (cursor != null) {
                    try {
                        cursor.close();
                    } catch (Throwable unused) {
                    }
                }
                throw th;
            }
        }
        r4 = null;
        r3.A01.A0H(new Runnable(r4, r5, i, i2, z) { // from class: X.2j4
            public final /* synthetic */ int A00;
            public final /* synthetic */ int A01;
            public final /* synthetic */ AbstractC15340mz A03;
            public final /* synthetic */ C30421Xi A04;
            public final /* synthetic */ boolean A05;

            {
                this.A03 = r2;
                this.A00 = r4;
                this.A04 = r3;
                this.A05 = r6;
                this.A01 = r5;
            }

            @Override // java.lang.Runnable
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void run() {
                /*
                    r9 = this;
                    X.3WN r7 = X.AnonymousClass3WN.this
                    X.0mz r6 = r9.A03
                    int r4 = r9.A00
                    X.1Xi r2 = r9.A04
                    boolean r1 = r9.A05
                    int r5 = r9.A01
                    java.lang.ref.WeakReference r0 = r7.A0D
                    java.lang.Object r8 = r0.get()
                    android.app.Activity r8 = (android.app.Activity) r8
                    if (r6 == 0) goto L_0x0058
                    if (r8 == 0) goto L_0x005a
                    X.0m9 r0 = r7.A0A
                    boolean r0 = X.AnonymousClass3GJ.A01(r0, r6, r2, r1)
                    if (r0 == 0) goto L_0x0066
                    r0 = 100
                    if (r4 >= r0) goto L_0x0066
                    X.1Xi r6 = (X.C30421Xi) r6
                    X.19D r2 = r7.A06
                    X.55D r3 = new X.55D
                    r3.<init>(r7, r6, r4)
                    r1 = 0
                    r0 = 1
                    X.1hP r4 = r2.A01(r8, r1, r0)
                    r4.A0O = r6
                    r4.A0A = r0
                    int r2 = r6.A0C
                    r0 = 9
                    if (r2 == r0) goto L_0x0042
                    r1 = 10
                    r0 = 0
                    if (r2 != r1) goto L_0x0043
                L_0x0042:
                    r0 = 1
                L_0x0043:
                    r4.A0T = r0
                    r4.A0J = r3
                    android.content.Context r3 = r8.getBaseContext()
                    X.0mE r2 = r7.A01
                    r1 = 29
                    com.facebook.redex.RunnableBRunnable0Shape3S0300000_I1 r0 = new com.facebook.redex.RunnableBRunnable0Shape3S0300000_I1
                    r0.<init>(r7, r4, r6, r1)
                    X.AnonymousClass3GK.A01(r3, r2, r0, r5)
                L_0x0057:
                    return
                L_0x0058:
                    if (r8 != 0) goto L_0x0066
                L_0x005a:
                    android.view.View r0 = r7.A00
                    android.content.Context r0 = r0.getContext()
                L_0x0060:
                    if (r4 <= 0) goto L_0x0057
                    X.AnonymousClass3GK.A00(r0, r5)
                    return
                L_0x0066:
                    android.content.Context r0 = r8.getBaseContext()
                    goto L_0x0060
                */
                throw new UnsupportedOperationException("Method not decompiled: X.RunnableC55652j4.run():void");
            }
        });
    }
}
