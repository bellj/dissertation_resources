package X;

import java.lang.reflect.Array;

/* renamed from: X.3Ch  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C63623Ch {
    public final int A00;
    public final int A01;
    public final byte[][] A02;

    public C63623Ch(int i, int i2) {
        int[] A07 = C13000ix.A07();
        A07[1] = i;
        A07[0] = i2;
        this.A02 = (byte[][]) Array.newInstance(byte.class, A07);
        this.A01 = i;
        this.A00 = i2;
    }

    public String toString() {
        String str;
        int i = this.A01;
        int i2 = this.A00;
        StringBuilder A0t = C12980iv.A0t(((i << 1) * i2) + 2);
        for (int i3 = 0; i3 < i2; i3++) {
            byte[] bArr = this.A02[i3];
            for (int i4 = 0; i4 < i; i4++) {
                byte b = bArr[i4];
                if (b == 0) {
                    str = " 0";
                } else if (b != 1) {
                    str = "  ";
                } else {
                    str = " 1";
                }
                A0t.append(str);
            }
            A0t.append('\n');
        }
        return A0t.toString();
    }
}
