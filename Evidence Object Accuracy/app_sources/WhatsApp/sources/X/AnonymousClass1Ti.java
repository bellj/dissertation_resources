package X;

/* renamed from: X.1Ti  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1Ti extends AbstractC16110oT {
    public Double A00;
    public String A01;
    public String A02;

    public AnonymousClass1Ti() {
        super(2128, new AnonymousClass00E(1, 1, 20), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A01);
        r3.Abe(2, this.A02);
        r3.Abe(3, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("WamSuperpackDecompressionFailure {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "assetName", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "exceptionMessage", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "freeSpaceAvailable", this.A00);
        sb.append("}");
        return sb.toString();
    }
}
