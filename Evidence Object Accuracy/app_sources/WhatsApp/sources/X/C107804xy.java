package X;

import com.google.android.search.verification.client.SearchActionVerificationClientService;

/* renamed from: X.4xy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C107804xy implements AnonymousClass5QH {
    public final float A00 = 0.7f;
    public final float A01 = 0.75f;
    public final int A02 = 25000;
    public final int A03 = SearchActionVerificationClientService.NOTIFICATION_ID;
    public final int A04 = 25000;
    public final AnonymousClass5Xd A05;

    public C107804xy() {
        AnonymousClass5Xd r4 = AnonymousClass5Xd.A00;
        this.A05 = r4;
    }
}
