package X;

import android.content.SharedPreferences;
import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5yo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130105yo {
    public SharedPreferences A00;
    public final C14830m7 A01;
    public final C16630pM A02;

    public C130105yo(C14830m7 r1, C16630pM r2) {
        this.A01 = r1;
        this.A02 = r2;
    }

    public static int A00(C130105yo r1) {
        if (r1.A05()) {
            return R.drawable.novi_logo_rc;
        }
        return R.drawable.novi_logo;
    }

    public static SharedPreferences.Editor A01(C130105yo r0) {
        return r0.A02().edit();
    }

    public final synchronized SharedPreferences A02() {
        SharedPreferences sharedPreferences;
        sharedPreferences = this.A00;
        if (sharedPreferences == null) {
            sharedPreferences = this.A02.A01("novi");
            this.A00 = sharedPreferences;
        }
        AnonymousClass009.A05(sharedPreferences);
        return sharedPreferences;
    }

    public C129895yT A03() {
        AbstractC128525wF r2;
        String string = A02().getString("limitation_data", "");
        if (TextUtils.isEmpty(string)) {
            return null;
        }
        try {
            JSONObject A05 = C13000ix.A05(string);
            JSONArray jSONArray = A05.getJSONArray("type");
            ArrayList A0l = C12960it.A0l();
            for (int i = 0; i < jSONArray.length(); i++) {
                A0l.add(jSONArray.get(i).toString());
            }
            AnonymousClass619 A02 = AnonymousClass619.A02(A05.getJSONObject("title"));
            AnonymousClass619 A022 = AnonymousClass619.A02(A05.getJSONObject("body"));
            C1316263m A01 = C1316263m.A01(A05.optString("balance", ""));
            ArrayList A0l2 = C12960it.A0l();
            JSONArray jSONArray2 = A05.getJSONArray("call-to-actions");
            for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                JSONObject jSONObject = jSONArray2.getJSONObject(i2);
                if (jSONObject.get("type").equals("LINK")) {
                    r2 = new C121115hH(jSONObject.optString("text", ""), jSONObject.optString("link-uri", ""));
                } else {
                    r2 = new C121125hI(C1316663q.A00(jSONObject.optString("step-up", "")), jSONObject.optString("text", ""));
                }
                A0l2.add(r2);
            }
            return new C129895yT(A022, A02, A01, A0l, A0l2);
        } catch (JSONException unused) {
            Log.e("PAY: Limitation fromJsonString threw exception");
            return null;
        }
    }

    public void A04(C129895yT r10) {
        String str;
        JSONObject jSONObject;
        JSONObject A0a;
        if (r10 != null) {
            JSONObject A0a2 = C117295Zj.A0a();
            try {
                JSONArray A0L = C117315Zl.A0L();
                int i = 0;
                int i2 = 0;
                while (true) {
                    List list = r10.A04;
                    if (i2 >= list.size()) {
                        break;
                    }
                    A0L.put(i2, list.get(i2));
                    i2++;
                }
                A0a2.put("type", A0L);
                A0a2.put("title", r10.A01.A07());
                A0a2.put("body", r10.A00.A07());
                C1316263m r7 = r10.A02;
                if (r7 != null) {
                    JSONObject A0a3 = C117295Zj.A0a();
                    try {
                        C117315Zl.A0W(r7.A02, "primary", A0a3);
                        C117315Zl.A0W(r7.A01, "local", A0a3);
                        A0a3.put("updateTsInMicroSeconds", r7.A00);
                        jSONObject = A0a3;
                    } catch (JSONException unused) {
                        Log.e("PAY: NoviBalance toJson threw exception");
                        jSONObject = A0a3;
                    }
                } else {
                    jSONObject = "";
                }
                A0a2.put("balance", jSONObject);
                JSONArray A0L2 = C117315Zl.A0L();
                while (true) {
                    List list2 = r10.A03;
                    if (i >= list2.size()) {
                        break;
                    }
                    AbstractC128525wF r72 = (AbstractC128525wF) list2.get(i);
                    if (!(r72 instanceof C121125hI)) {
                        C121115hH r73 = (C121115hH) r72;
                        A0a = C117295Zj.A0a();
                        A0a.put("type", "LINK");
                        A0a.put("text", ((AbstractC128525wF) r73).A00);
                        A0a.put("link-uri", r73.A00);
                    } else {
                        C121125hI r74 = (C121125hI) r72;
                        A0a = C117295Zj.A0a();
                        A0a.put("type", "STEP_UP");
                        A0a.put("text", ((AbstractC128525wF) r74).A00);
                        A0a.put("step-up", r74.A00.A01());
                    }
                    A0L2.put(i, A0a);
                    i++;
                }
                A0a2.put("call-to-actions", A0L2);
            } catch (JSONException unused2) {
                Log.e("PAY: Limitation toJson threw exception");
            }
            str = A0a2.toString();
        } else {
            str = "";
        }
        C12970iu.A1D(A01(this), "limitation_data", str);
    }

    public boolean A05() {
        String A0p = C12980iv.A0p(A02(), "env_tier");
        return "novi.wallet_core.rc".equals(A0p) || "novi.wallet_core.rc_stable".equals(A0p);
    }
}
