package X;

import com.whatsapp.jid.Jid;
import java.lang.ref.WeakReference;

/* renamed from: X.2BN  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2BN extends AbstractC16350or {
    public final AnonymousClass02N A00 = new AnonymousClass02N();
    public final C14900mE A01;
    public final AnonymousClass1BB A02;
    public final C20000v3 A03;
    public final C20050v8 A04;
    public final C15660nh A05;
    public final C242114q A06;
    public final AbstractC14640lm A07;
    public final C22710zW A08;
    public final C17070qD A09;
    public final WeakReference A0A;

    public AnonymousClass2BN(C14900mE r2, AbstractActivityC33001d7 r3, AnonymousClass1BB r4, C20000v3 r5, C20050v8 r6, C15660nh r7, C242114q r8, C15370n3 r9, C22710zW r10, C17070qD r11) {
        this.A01 = r2;
        this.A09 = r11;
        this.A03 = r5;
        this.A04 = r6;
        this.A05 = r7;
        this.A06 = r8;
        this.A02 = r4;
        this.A08 = r10;
        this.A0A = new WeakReference(r3);
        Jid A0B = r9.A0B(AbstractC14640lm.class);
        AnonymousClass009.A05(A0B);
        this.A07 = (AbstractC14640lm) A0B;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00dd, code lost:
        if (r2 != null) goto L_0x00df;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Void A08(java.lang.Void... r13) {
        /*
        // Method dump skipped, instructions count: 474
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2BN.A08(java.lang.Void[]):java.lang.Void");
    }
}
