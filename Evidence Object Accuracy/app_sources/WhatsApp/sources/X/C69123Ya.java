package X;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/* renamed from: X.3Ya  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69123Ya implements AbstractC116735Wp {
    public Cipher A00;
    public SecretKey A01;
    public byte[] A02;

    @Override // X.AbstractC116735Wp
    public byte[] A8i(byte[] bArr, byte[] bArr2, int i, int i2, long j) {
        try {
            this.A00.init(2, this.A01, new GCMParameterSpec(128, C69133Yb.A00(this.A02, j)));
            this.A00.updateAAD(bArr);
            return this.A00.doFinal(bArr2, 0, i2);
        } catch (InvalidAlgorithmParameterException e) {
            throw AnonymousClass1NR.A01("Invalid Algorithm Params", e, (byte) 80);
        } catch (InvalidKeyException e2) {
            throw AnonymousClass1NR.A01(" Invalid Key", e2, (byte) 80);
        } catch (BadPaddingException e3) {
            throw AnonymousClass1NR.A01("Bad padding", e3, (byte) 80);
        } catch (IllegalBlockSizeException e4) {
            throw AnonymousClass1NR.A01("Illegal block size ", e4, (byte) 80);
        }
    }

    @Override // X.AbstractC116735Wp
    public byte[] A9O(byte[] bArr, byte[] bArr2, int i, int i2, long j) {
        try {
            this.A00.init(1, this.A01, new GCMParameterSpec(128, C69133Yb.A00(this.A02, j)));
            this.A00.updateAAD(bArr);
            return this.A00.doFinal(bArr2, 0, i2);
        } catch (InvalidAlgorithmParameterException e) {
            throw AnonymousClass1NR.A01("Invalid Algorithm Params", e, (byte) 80);
        } catch (InvalidKeyException e2) {
            throw AnonymousClass1NR.A01(" Invalid Key", e2, (byte) 80);
        } catch (BadPaddingException e3) {
            throw AnonymousClass1NR.A01("Bad padding", e3, (byte) 80);
        } catch (IllegalBlockSizeException e4) {
            throw AnonymousClass1NR.A01("Illegal block size ", e4, (byte) 80);
        }
    }

    @Override // X.AbstractC116735Wp
    public void AId(byte[] bArr, byte[] bArr2) {
        if (bArr == null || bArr.length != 16) {
            throw AnonymousClass1NR.A00("Invalid key length.", (byte) 80);
        } else if (bArr2 == null || bArr2.length != 12) {
            throw AnonymousClass1NR.A00("Invalid iv length.", (byte) 80);
        } else {
            this.A02 = bArr2;
            this.A01 = new SecretKeySpec(bArr, "AES");
            try {
                try {
                    try {
                        this.A00 = Cipher.getInstance("AES/GCM/NoPadding", "AndroidOpenSSL");
                    } catch (NoSuchAlgorithmException | NoSuchProviderException | NoSuchPaddingException unused) {
                        this.A00 = Cipher.getInstance("AES/GCM/NoPadding", "SC");
                    }
                } catch (NoSuchAlgorithmException | NoSuchProviderException | NoSuchPaddingException unused2) {
                    this.A00 = Cipher.getInstance("AES/GCM/NoPadding");
                }
            } catch (NoSuchAlgorithmException e) {
                throw AnonymousClass1NR.A01("AES/GCM/NoPadding not found", e, (byte) 80);
            } catch (NoSuchPaddingException e2) {
                throw AnonymousClass1NR.A01("No such padding", e2, (byte) 80);
            }
        }
    }
}
