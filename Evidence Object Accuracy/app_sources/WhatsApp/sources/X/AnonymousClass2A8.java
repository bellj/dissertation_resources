package X;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import com.whatsapp.calling.videoparticipant.VideoCallParticipantView;

/* renamed from: X.2A8  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2A8 extends FrameLayout implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;

    public AnonymousClass2A8(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A01) {
            this.A01 = true;
            VideoCallParticipantView videoCallParticipantView = (VideoCallParticipantView) this;
            AnonymousClass01J r1 = ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06;
            videoCallParticipantView.A0N = (C14850m9) r1.A04.get();
            videoCallParticipantView.A0M = (AnonymousClass018) r1.ANb.get();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }
}
