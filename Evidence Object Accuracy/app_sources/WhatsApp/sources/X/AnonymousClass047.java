package X;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.preference.PreferenceScreen;

/* renamed from: X.047  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass047 {
    public long A00 = 0;
    public Context A01;
    public SharedPreferences.Editor A02;
    public SharedPreferences A03;
    public AbstractC005402n A04;
    public AbstractC005502o A05;
    public AbstractC005302m A06;
    public PreferenceScreen A07;
    public String A08;
    public boolean A09;

    public AnonymousClass047(Context context) {
        this.A01 = context;
        StringBuilder sb = new StringBuilder();
        sb.append(context.getPackageName());
        sb.append("_preferences");
        this.A08 = sb.toString();
        this.A03 = null;
    }

    public static void A00(Context context, int i) {
        StringBuilder sb = new StringBuilder();
        sb.append(context.getPackageName());
        sb.append("_preferences");
        String obj = sb.toString();
        SharedPreferences sharedPreferences = context.getSharedPreferences("_has_set_default_values", 0);
        AnonymousClass047 r1 = new AnonymousClass047(context);
        r1.A08 = obj;
        r1.A03 = null;
        r1.A03 = null;
        r1.A03(context, null, i);
        sharedPreferences.edit().putBoolean("_has_set_default_values", true).apply();
    }

    public SharedPreferences.Editor A01() {
        if (!this.A09) {
            return A02().edit();
        }
        SharedPreferences.Editor editor = this.A02;
        if (editor != null) {
            return editor;
        }
        SharedPreferences.Editor edit = A02().edit();
        this.A02 = edit;
        return edit;
    }

    public SharedPreferences A02() {
        SharedPreferences sharedPreferences = this.A03;
        if (sharedPreferences != null) {
            return sharedPreferences;
        }
        SharedPreferences sharedPreferences2 = this.A01.getSharedPreferences(this.A08, 0);
        this.A03 = sharedPreferences2;
        return sharedPreferences2;
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v6, types: [androidx.preference.PreferenceGroup, androidx.preference.Preference] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public androidx.preference.PreferenceScreen A03(android.content.Context r7, androidx.preference.PreferenceScreen r8, int r9) {
        /*
            r6 = this;
            r0 = 1
            r6.A09 = r0
            X.0Sh r5 = new X.0Sh
            r5.<init>(r7, r6)
            android.content.Context r1 = r5.A02
            android.content.res.Resources r0 = r1.getResources()
            android.content.res.XmlResourceParser r3 = r0.getXml(r9)
            java.lang.Object[] r4 = r5.A03     // Catch: all -> 0x00a3
            monitor-enter(r4)     // Catch: all -> 0x00a3
            android.util.AttributeSet r2 = android.util.Xml.asAttributeSet(r3)     // Catch: all -> 0x00a0
            r0 = 0
            r4[r0] = r1     // Catch: all -> 0x00a0
        L_0x001c:
            int r1 = r3.next()     // Catch: InflateException -> 0x009e, XmlPullParserException -> 0x0090, IOException -> 0x006a, all -> 0x00a0
            r0 = 2
            if (r1 == r0) goto L_0x0041
            r0 = 1
            if (r1 != r0) goto L_0x001c
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: InflateException -> 0x009e, XmlPullParserException -> 0x0090, IOException -> 0x006a, all -> 0x00a0
            r1.<init>()     // Catch: InflateException -> 0x009e, XmlPullParserException -> 0x0090, IOException -> 0x006a, all -> 0x00a0
            java.lang.String r0 = r3.getPositionDescription()     // Catch: InflateException -> 0x009e, XmlPullParserException -> 0x0090, IOException -> 0x006a, all -> 0x00a0
            r1.append(r0)     // Catch: InflateException -> 0x009e, XmlPullParserException -> 0x0090, IOException -> 0x006a, all -> 0x00a0
            java.lang.String r0 = ": No start tag found!"
            r1.append(r0)     // Catch: InflateException -> 0x009e, XmlPullParserException -> 0x0090, IOException -> 0x006a, all -> 0x00a0
            java.lang.String r1 = r1.toString()     // Catch: InflateException -> 0x009e, XmlPullParserException -> 0x0090, IOException -> 0x006a, all -> 0x00a0
            android.view.InflateException r0 = new android.view.InflateException     // Catch: InflateException -> 0x009e, XmlPullParserException -> 0x0090, IOException -> 0x006a, all -> 0x00a0
            r0.<init>(r1)     // Catch: InflateException -> 0x009e, XmlPullParserException -> 0x0090, IOException -> 0x006a, all -> 0x00a0
            throw r0     // Catch: InflateException -> 0x009e, XmlPullParserException -> 0x0090, IOException -> 0x006a, all -> 0x00a0
        L_0x0041:
            java.lang.String r0 = r3.getName()     // Catch: InflateException -> 0x009e, XmlPullParserException -> 0x0090, IOException -> 0x006a, all -> 0x00a0
            androidx.preference.Preference r1 = r5.A00(r2, r0)     // Catch: InflateException -> 0x009e, XmlPullParserException -> 0x0090, IOException -> 0x006a, all -> 0x00a0
            androidx.preference.PreferenceGroup r1 = (androidx.preference.PreferenceGroup) r1     // Catch: InflateException -> 0x009e, XmlPullParserException -> 0x0090, IOException -> 0x006a, all -> 0x00a0
            if (r8 != 0) goto L_0x0053
            X.047 r0 = r5.A00     // Catch: InflateException -> 0x009e, XmlPullParserException -> 0x0090, IOException -> 0x006a, all -> 0x00a0
            r1.A0H(r0)     // Catch: InflateException -> 0x009e, XmlPullParserException -> 0x0090, IOException -> 0x006a, all -> 0x00a0
            r8 = r1
        L_0x0053:
            r5.A02(r2, r8, r3)     // Catch: InflateException -> 0x009e, XmlPullParserException -> 0x0090, IOException -> 0x006a, all -> 0x00a0
            monitor-exit(r4)     // Catch: all -> 0x00a0
            r3.close()
            androidx.preference.PreferenceScreen r8 = (androidx.preference.PreferenceScreen) r8
            r8.A0H(r6)
            r1 = 0
            android.content.SharedPreferences$Editor r0 = r6.A02
            if (r0 == 0) goto L_0x0067
            r0.apply()
        L_0x0067:
            r6.A09 = r1
            return r8
        L_0x006a:
            r2 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: all -> 0x00a0
            r1.<init>()     // Catch: all -> 0x00a0
            java.lang.String r0 = r3.getPositionDescription()     // Catch: all -> 0x00a0
            r1.append(r0)     // Catch: all -> 0x00a0
            java.lang.String r0 = ": "
            r1.append(r0)     // Catch: all -> 0x00a0
            java.lang.String r0 = r2.getMessage()     // Catch: all -> 0x00a0
            r1.append(r0)     // Catch: all -> 0x00a0
            java.lang.String r1 = r1.toString()     // Catch: all -> 0x00a0
            android.view.InflateException r0 = new android.view.InflateException     // Catch: all -> 0x00a0
            r0.<init>(r1)     // Catch: all -> 0x00a0
            r0.initCause(r2)     // Catch: all -> 0x00a0
            throw r0     // Catch: all -> 0x00a0
        L_0x0090:
            r2 = move-exception
            java.lang.String r1 = r2.getMessage()     // Catch: all -> 0x00a0
            android.view.InflateException r0 = new android.view.InflateException     // Catch: all -> 0x00a0
            r0.<init>(r1)     // Catch: all -> 0x00a0
            r0.initCause(r2)     // Catch: all -> 0x00a0
            throw r0     // Catch: all -> 0x00a0
        L_0x009e:
            r0 = move-exception
            throw r0     // Catch: all -> 0x00a0
        L_0x00a0:
            r0 = move-exception
            monitor-exit(r4)     // Catch: all -> 0x00a0
            throw r0     // Catch: all -> 0x00a3
        L_0x00a3:
            r0 = move-exception
            r3.close()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass047.A03(android.content.Context, androidx.preference.PreferenceScreen, int):androidx.preference.PreferenceScreen");
    }
}
