package X;

import com.whatsapp.R;
import com.whatsapp.group.GroupChatInfo;
import com.whatsapp.util.Log;

/* renamed from: X.3ai  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70083ai implements AnonymousClass5WL {
    public final /* synthetic */ GroupChatInfo A00;

    public C70083ai(GroupChatInfo groupChatInfo) {
        this.A00 = groupChatInfo;
    }

    @Override // X.AnonymousClass5WL
    public void ASv() {
        C36021jC.A00(this.A00, 1);
    }

    @Override // X.AnonymousClass5WL
    public void ATy(boolean z) {
        Log.i("group_info/onclick_deleteGroup");
        GroupChatInfo groupChatInfo = this.A00;
        if (!((AbstractActivityC33001d7) groupChatInfo).A0C.A0C(groupChatInfo.A1C)) {
            C12960it.A1E(new AnonymousClass38O(groupChatInfo, ((AbstractActivityC33001d7) groupChatInfo).A01, groupChatInfo.A1C, z), ((ActivityC13830kP) groupChatInfo).A05);
        } else if (((ActivityC13810kN) groupChatInfo).A07.A0B()) {
            groupChatInfo.Ady(R.string.participant_removing, R.string.register_wait_message);
            C20660w7 r0 = groupChatInfo.A1H;
            C14860mA r7 = groupChatInfo.A1Z;
            r0.A06(new AnonymousClass32S(groupChatInfo.A0i, groupChatInfo, ((AbstractActivityC33001d7) groupChatInfo).A0H, groupChatInfo.A1C, r7));
        } else {
            ((ActivityC13810kN) groupChatInfo).A05.A07(R.string.failed_to_leave_group, 0);
        }
    }
}
