package X;

import android.hardware.biometrics.BiometricManager;

/* renamed from: X.0KI  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0KI {
    public static int A00(BiometricManager biometricManager, int i) {
        return biometricManager.canAuthenticate(i);
    }
}
