package X;

import android.view.View;
import android.view.animation.Animation;

/* renamed from: X.3x5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83343x5 extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ int A00 = 8;
    public final /* synthetic */ View A01;

    public C83343x5(View view) {
        this.A01 = view;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        this.A01.setVisibility(this.A00);
    }
}
