package X;

import android.content.Context;

/* renamed from: X.5gq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120845gq extends C120895gv {
    public final /* synthetic */ C128855wm A00;
    public final /* synthetic */ C120405g8 A01;
    public final /* synthetic */ String A02;
    public final /* synthetic */ boolean A03;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120845gq(Context context, C14900mE r8, C128855wm r9, C18650sn r10, C64513Fv r11, C120405g8 r12, String str, String str2, boolean z) {
        super(context, r8, r10, r11, str);
        this.A01 = r12;
        this.A00 = r9;
        this.A03 = z;
        this.A02 = str2;
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A02(C452120p r3) {
        super.A02(r3);
        this.A00.A00(r3, this.A03);
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A03(C452120p r3) {
        super.A03(r3);
        this.A00.A00(r3, this.A03);
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A04(AnonymousClass1V8 r6) {
        super.A04(r6);
        AnonymousClass68Z r4 = this.A01.A04;
        AnonymousClass1ZR A0I = C117305Zk.A0I(C117305Zk.A0J(), String.class, this.A02, "upiHandle");
        boolean z = this.A03;
        r4.A00(A0I, z);
        C128855wm r2 = this.A00;
        StringBuilder A0k = C12960it.A0k("PAY: IndiaUpiBlockListManager/on-success blocked: ");
        A0k.append(z);
        C12960it.A1F(A0k);
        r2.A01.A02.A0A((AbstractC13860kS) r2.A00);
        AnonymousClass5US r1 = r2.A02;
        if (r1 != null) {
            r1.AVD(null);
        }
    }
}
