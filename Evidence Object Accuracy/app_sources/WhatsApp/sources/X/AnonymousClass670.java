package X;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.widget.TextView;
import androidx.fragment.app.DialogFragment;
import com.facebook.redex.IDxDListenerShape14S0100000_3_I1;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.bloks.ui.BloksDialogFragment;
import com.whatsapp.util.Log;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/* renamed from: X.670  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass670 implements AbstractC72403eX {
    public final AbstractC15710nm A00;
    public final C14900mE A01;
    public final C15570nT A02;
    public final C128695wW A03;
    public final AnonymousClass60K A04;
    public final AnonymousClass01d A05;
    public final C14830m7 A06;
    public final AnonymousClass018 A07;
    public final C16120oU A08;

    public AnonymousClass670(AbstractC15710nm r1, C14900mE r2, C15570nT r3, C128695wW r4, AnonymousClass60K r5, AnonymousClass01d r6, C14830m7 r7, AnonymousClass018 r8, C16120oU r9) {
        this.A06 = r7;
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
        this.A08 = r9;
        this.A07 = r8;
        this.A05 = r6;
        this.A04 = r5;
        this.A03 = r4;
    }

    public static void A00(Activity activity, C64873Hg r8, C90184Mx r9, HashMap hashMap) {
        AnonymousClass6Lk r6 = (AnonymousClass6Lk) activity;
        Object remove = hashMap.remove("clear_backstack");
        Object remove2 = hashMap.remove("get_params_from_stack");
        Object remove3 = hashMap.remove("camera_permission");
        Stack stack = r8.A02;
        HashMap hashMap2 = new HashMap((HashMap) stack.peek());
        if (remove2 != null) {
            hashMap2.putAll(hashMap);
            hashMap = hashMap2;
        }
        if (remove != null) {
            AnonymousClass01F A0V = ((ActivityC000900k) r6).A0V();
            for (int i = 0; i < A0V.A03(); i++) {
                A0V.A0H();
                C64873Hg.A00(r8.A01);
                stack.pop();
            }
        }
        C64873Hg.A00(r8.A01);
        stack.add(C12970iu.A11());
        r8.A05(hashMap);
        AbstractC14200l1 r2 = r9.A01;
        if (r2 != null) {
            r8.A01(r9.A00, r2, "backpress");
        }
        if (remove3 != null) {
            AbstractActivityC121705jc r7 = (AbstractActivityC121705jc) ((AbstractC136496Mt) activity);
            if (!(r7 instanceof AbstractActivityC123635nW)) {
                RequestPermissionActivity.A0T(r7, r7.A03, 30);
                return;
            }
            AbstractActivityC123635nW r72 = (AbstractActivityC123635nW) r7;
            C1310561a.A03(r72, ((AbstractActivityC121705jc) r72).A03, r72.A0A);
        }
    }

    public static void A01(C64173En r2, AbstractActivityC119645em r3, String str, HashMap hashMap) {
        r3.A05 = BloksDialogFragment.A00(str, hashMap);
        C004902f r22 = new C004902f((AnonymousClass01F) r2.A06.get());
        r22.A07(r3.A05, R.id.bloks_fragment_container);
        r22.A0F(str);
        r22.A01();
    }

    @Override // X.AbstractC72403eX
    public void A6D(Activity activity, C90184Mx r7, HashMap hashMap) {
        Object remove = hashMap.remove("case");
        AnonymousClass009.A05(remove);
        String str = (String) remove;
        ((AbstractC136496Mt) activity).AZC(((AbstractActivityC119645em) ((AnonymousClass6Lk) activity)).A09.A01(r7.A00, r7.A01, str), str, hashMap);
    }

    @Override // X.AbstractC72403eX
    public void A92(Activity activity, String str) {
        AbstractActivityC119645em r5 = (AbstractActivityC119645em) ((AnonymousClass6Lk) activity);
        DialogFragment dialogFragment = (DialogFragment) r5.A0V().A0A("bloks-dialog");
        C64873Hg r2 = r5.A09;
        HashMap hashMap = r2.A01;
        AnonymousClass3FE r1 = (AnonymousClass3FE) hashMap.get("dialog");
        if (dialogFragment != null) {
            dialogFragment.A1B();
            C64873Hg.A00(hashMap);
            r2.A02.pop();
        }
        if (r1 != null) {
            r1.A00 = true;
            r1.A00(str);
        }
    }

    @Override // X.AbstractC72403eX
    public long ACv(Activity activity) {
        AnonymousClass17V r5 = ((AbstractActivityC121705jc) ((AbstractC136496Mt) activity)).A0K;
        if (r5.A00 > 0) {
            return r5.A01.A00() - r5.A00;
        }
        return -1;
    }

    @Override // X.AbstractC72403eX
    public String ACw(Activity activity) {
        return ((AbstractActivityC121705jc) ((AbstractC136496Mt) activity)).A0K.A00();
    }

    @Override // X.AbstractC72403eX
    public String AFv(Activity activity, String str, HashMap hashMap) {
        return ((AbstractC136496Mt) activity).AZE(str, hashMap);
    }

    @Override // X.AbstractC72403eX
    public boolean AI0(Activity activity, int i) {
        return ((AbstractC136496Mt) activity).AHz(i);
    }

    @Override // X.AbstractC72403eX
    public void AJC(Activity activity, C90184Mx r20, String str, String str2, String str3, Map map) {
        AnonymousClass3FE r5;
        AnonymousClass6Lk r6 = (AnonymousClass6Lk) activity;
        AbstractC14200l1 r3 = r20.A01;
        if (r3 != null) {
            r5 = ((AbstractActivityC119645em) r6).A09.A01(r20.A00, r3, "case");
        } else {
            r5 = null;
        }
        AnonymousClass60K r4 = this.A04;
        int parseInt = Integer.parseInt(str3);
        AbstractActivityC121705jc r62 = (AbstractActivityC121705jc) r6;
        C124945qQ r1 = r62.A08;
        if (r1 == null) {
            r1 = new C124945qQ();
            r62.A08 = r1;
        }
        C126685tH r2 = new C126685tH(r62.A06, r1);
        if (!AnonymousClass60K.A02.contains(str2)) {
            Log.e("Bloks: IQRequestHelper/sendIQRequest: Invalid XMLNS");
            if (r5 != null) {
                C117315Zl.A0S(r5);
                return;
            }
            return;
        }
        String A0x = C12970iu.A0x(map.keySet().iterator());
        AnonymousClass1V8 A00 = r4.A00(r2, A0x, (Map) map.get(A0x));
        C17220qS r11 = r4.A01;
        String A01 = r11.A01();
        AnonymousClass1W9[] r63 = new AnonymousClass1W9[4];
        r63[0] = new AnonymousClass1W9(AnonymousClass1VY.A00, "to");
        C12960it.A1M("type", str, r63, 1);
        C12960it.A1M("id", A01, r63, 2);
        C12960it.A1M("xmlns", str2, r63, 3);
        r11.A09(new AnonymousClass6DN(r5, r4, r2), new AnonymousClass1V8(A00, "iq", r63), A01, 204, ((long) parseInt) * 1000);
    }

    @Override // X.AbstractC72403eX
    public void AKP(Activity activity, C90184Mx r6, C64173En r7, String str, HashMap hashMap) {
        C64873Hg r3 = ((AbstractActivityC119645em) ((AnonymousClass6Lk) activity)).A09;
        A00(activity, r3, r6, hashMap);
        AbstractC14200l1 r2 = r6.A01;
        if (r2 != null) {
            r3.A01(r6.A00, r2, "dialog");
        }
        AnonymousClass01F r22 = (AnonymousClass01F) r7.A06.get();
        DialogFragment dialogFragment = (DialogFragment) r22.A0A("bloks-dialog");
        if (dialogFragment != null) {
            dialogFragment.A1B();
        }
        BloksDialogFragment.A00(str, hashMap).A1F(r22, "bloks-dialog");
    }

    @Override // X.AbstractC72403eX
    public void AYk(Activity activity, C90184Mx r5, C64173En r6, String str, HashMap hashMap) {
        C124815qA.A00 = str;
        C124815qA.A01 = hashMap;
        AnonymousClass6Lk r2 = (AnonymousClass6Lk) activity;
        Bundle extras = r2.getIntent().getExtras();
        if (extras != null) {
            extras.putString("screen_name", str);
        }
        AbstractActivityC119645em r22 = (AbstractActivityC119645em) r2;
        A00(activity, r22.A09, r5, hashMap);
        A01(r6, r22, str, hashMap);
    }

    @Override // X.AbstractC72403eX
    public void AYl(Activity activity, C90184Mx r5, C64173En r6, String str, HashMap hashMap) {
        C124815qA.A00 = str;
        C124815qA.A01 = hashMap;
        AnonymousClass6Lk r2 = (AnonymousClass6Lk) activity;
        Bundle extras = r2.getIntent().getExtras();
        if (extras != null) {
            extras.putString("screen_name", str);
        }
        AbstractActivityC119645em r22 = (AbstractActivityC119645em) r2;
        A00(activity, r22.A09, r5, hashMap);
        A01(r6, r22, str, hashMap);
    }

    @Override // X.AbstractC72403eX
    public void AZM(Activity activity, HashMap hashMap) {
        AnonymousClass6Lk r0 = (AnonymousClass6Lk) activity;
        activity.onBackPressed();
        if (hashMap != null) {
            ((AbstractMap) ((AbstractActivityC119645em) r0).A09.A02.peek()).putAll(hashMap);
        }
    }

    @Override // X.AbstractC72403eX
    public void Aah(Activity activity) {
        C117305Zk.A1L(((AbstractActivityC121705jc) ((AbstractC136496Mt) activity)).A0K);
    }

    @Override // X.AbstractC72403eX
    public void Abc(String str, ArrayList arrayList, HashMap hashMap, int i, int i2) {
        this.A08.A07(C124905qJ.A00(str, arrayList, hashMap, i, i2));
    }

    @Override // X.AbstractC72403eX
    public Map Abg() {
        return this.A03.A00(this.A02.A03());
    }

    @Override // X.AbstractC72403eX
    public void Abt(String str) {
        if (TextUtils.isEmpty(str)) {
            Log.e("WaExtensions/evaluate/bk.action.io.clipboard.SetString: text cannot be null or empty");
            return;
        }
        ClipboardManager A0B = this.A05.A0B();
        if (A0B != null) {
            try {
                A0B.setPrimaryClip(ClipData.newPlainText(str, str));
            } catch (NullPointerException | SecurityException e) {
                Log.e("bkinterpreter/clipboard/", e);
            }
        }
    }

    @Override // X.AbstractC72403eX
    public void Adi(Activity activity, C90184Mx r10, HashMap hashMap) {
        int i;
        C126645tD r5 = new C126645tD(((AbstractActivityC119645em) ((AnonymousClass6Lk) activity)).A09, this.A07);
        C64173En r2 = r10.A00.A00.A02;
        Object obj = hashMap.get("message");
        AnonymousClass009.A05(obj);
        String str = (String) obj;
        String str2 = (String) hashMap.get("title");
        AnonymousClass04S create = C12980iv.A0S((Context) r2.A05.get()).create();
        if (!TextUtils.isEmpty(str2)) {
            create.setTitle(str2);
        }
        AnonymousClass0U5 r0 = create.A00;
        r0.A0Q = str;
        TextView textView = r0.A0K;
        if (textView != null) {
            textView.setText(str);
        }
        create.setCanceledOnTouchOutside(false);
        AnonymousClass3FE A01 = r5.A00.A01(r10.A00, r10.A01, "alert_dialog");
        String str3 = (String) hashMap.get("button_info");
        if (!TextUtils.isEmpty(str3)) {
            String[] split = str3.split("\\|");
            for (int i2 = 0; i2 <= split.length - 2; i2 += 2) {
                String str4 = split[i2 + 1];
                DialogInterface.OnClickListener r4 = new DialogInterface.OnClickListener(create, A01) { // from class: X.62K
                    public final /* synthetic */ AnonymousClass04S A00;
                    public final /* synthetic */ AnonymousClass3FE A01;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                    }

                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i3) {
                        AnonymousClass3FE r22 = this.A01;
                        AnonymousClass04S r1 = this.A00;
                        r22.A00("on_press_positive");
                        r1.dismiss();
                    }
                };
                String str5 = split[i2];
                switch (str5.hashCode()) {
                    case -518392103:
                        if (str5.equals("neutral_btn_label")) {
                            i = -3;
                            r4 = new DialogInterface.OnClickListener(create, A01) { // from class: X.62J
                                public final /* synthetic */ AnonymousClass04S A00;
                                public final /* synthetic */ AnonymousClass3FE A01;

                                {
                                    this.A01 = r2;
                                    this.A00 = r1;
                                }

                                @Override // android.content.DialogInterface.OnClickListener
                                public final void onClick(DialogInterface dialogInterface, int i3) {
                                    AnonymousClass3FE r22 = this.A01;
                                    AnonymousClass04S r1 = this.A00;
                                    r22.A00("on_press_neutral");
                                    r1.dismiss();
                                }
                            };
                            continue;
                            create.A03(i, str4, r4);
                        }
                        break;
                    case 1820734407:
                        if (str5.equals("negative_btn_label")) {
                            i = -2;
                            r4 = new DialogInterface.OnClickListener(create, A01) { // from class: X.62L
                                public final /* synthetic */ AnonymousClass04S A00;
                                public final /* synthetic */ AnonymousClass3FE A01;

                                {
                                    this.A01 = r2;
                                    this.A00 = r1;
                                }

                                @Override // android.content.DialogInterface.OnClickListener
                                public final void onClick(DialogInterface dialogInterface, int i3) {
                                    AnonymousClass3FE r22 = this.A01;
                                    AnonymousClass04S r1 = this.A00;
                                    r22.A00("on_press_negative");
                                    r1.dismiss();
                                }
                            };
                            continue;
                            create.A03(i, str4, r4);
                        }
                        break;
                    case 2113821835:
                        if (str5.equals("positive_btn_label")) {
                            r4 = new DialogInterface.OnClickListener(create, A01) { // from class: X.62M
                                public final /* synthetic */ AnonymousClass04S A00;
                                public final /* synthetic */ AnonymousClass3FE A01;

                                {
                                    this.A01 = r2;
                                    this.A00 = r1;
                                }

                                @Override // android.content.DialogInterface.OnClickListener
                                public final void onClick(DialogInterface dialogInterface, int i3) {
                                    AnonymousClass3FE r22 = this.A01;
                                    AnonymousClass04S r1 = this.A00;
                                    r22.A00("on_press_positive");
                                    r1.dismiss();
                                }
                            };
                            break;
                        }
                        break;
                }
                i = -1;
                create.A03(i, str4, r4);
            }
        } else {
            create.A03(-1, r5.A01.A09(R.string.ok), new DialogInterface.OnClickListener(create, A01) { // from class: X.62N
                public final /* synthetic */ AnonymousClass04S A00;
                public final /* synthetic */ AnonymousClass3FE A01;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i3) {
                    AnonymousClass3FE r22 = this.A01;
                    AnonymousClass04S r1 = this.A00;
                    r22.A00("on_press_positive");
                    r1.dismiss();
                }
            });
        }
        create.setOnKeyListener(new DialogInterface.OnKeyListener() { // from class: X.62z
            @Override // android.content.DialogInterface.OnKeyListener
            public final boolean onKey(DialogInterface dialogInterface, int i3, KeyEvent keyEvent) {
                AnonymousClass3FE r1 = AnonymousClass3FE.this;
                if (i3 != 4) {
                    return true;
                }
                r1.A00("on_press_back");
                dialogInterface.dismiss();
                return true;
            }
        });
        create.setOnDismissListener(new IDxDListenerShape14S0100000_3_I1(A01, 0));
        create.show();
    }

    @Override // X.AbstractC72403eX
    public void Adw(Activity activity, C64173En r6, Boolean bool, String str) {
        ProgressDialog progressDialog = (ProgressDialog) r6.A07.get();
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            progressDialog.setMessage(str);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            if (bool.booleanValue()) {
                progressDialog.setCancelable(true);
                progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener(activity, this) { // from class: X.627
                    public final /* synthetic */ Activity A00;
                    public final /* synthetic */ AnonymousClass670 A01;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                    }

                    @Override // android.content.DialogInterface.OnCancelListener
                    public final void onCancel(DialogInterface dialogInterface) {
                        this.A00.onBackPressed();
                    }
                });
            } else {
                progressDialog.setCancelable(false);
            }
            progressDialog.show();
        }
    }

    @Override // X.AbstractC72403eX
    public void Ae0(String str) {
        this.A01.A0E(str, 0);
    }

    @Override // X.AbstractC72403eX
    public void AeA(Activity activity) {
        ((AbstractActivityC121705jc) ((AbstractC136496Mt) activity)).A0K.A01();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC72403eX
    public /* bridge */ /* synthetic */ Object Aeo(Long l, String str) {
        long j;
        double d;
        double d2;
        long longValue = l.longValue() - this.A06.A00();
        if (longValue > 0) {
            Long valueOf = Long.valueOf(longValue);
            switch (str.hashCode()) {
                case -1074026988:
                    if (str.equals("minute")) {
                        d = valueOf.doubleValue();
                        d2 = 60000.0d;
                        j = Double.valueOf(Math.ceil(d / d2)).longValue();
                        break;
                    }
                    j = valueOf.longValue();
                    break;
                case -906279820:
                    if (str.equals("second")) {
                        d = valueOf.doubleValue();
                        d2 = 1000.0d;
                        j = Double.valueOf(Math.ceil(d / d2)).longValue();
                        break;
                    }
                    j = valueOf.longValue();
                    break;
                case 3208676:
                    if (str.equals("hour")) {
                        d = valueOf.doubleValue();
                        d2 = 3600000.0d;
                        j = Double.valueOf(Math.ceil(d / d2)).longValue();
                        break;
                    }
                    j = valueOf.longValue();
                    break;
                default:
                    j = valueOf.longValue();
                    break;
            }
        } else {
            j = -1;
        }
        return Long.valueOf(j);
    }
}
