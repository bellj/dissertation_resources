package X;

import java.util.List;

/* renamed from: X.3Dx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C64023Dx {
    public final AnonymousClass1V8 A00;
    public final List A01;

    public C64023Dx(AbstractC15710nm r4, AnonymousClass1V8 r5) {
        AnonymousClass1V8.A01(r5, "next_screens");
        String[] A08 = C13000ix.A08();
        A08[0] = "screen";
        this.A01 = AnonymousClass3JT.A0A(r4, r5, A08, 5);
        this.A00 = r5;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || C64023Dx.class != obj.getClass()) {
            return false;
        }
        return this.A01.equals(((C64023Dx) obj).A01);
    }

    public int hashCode() {
        return C12970iu.A08(this.A01, C12970iu.A1b());
    }
}
