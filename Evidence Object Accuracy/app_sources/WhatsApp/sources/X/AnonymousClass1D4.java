package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1D4  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1D4 implements AbstractC16990q5 {
    public final C26211Cl A00;

    @Override // X.AbstractC16990q5
    public /* synthetic */ void AOp() {
    }

    public AnonymousClass1D4(C26211Cl r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC16990q5
    public void AOo() {
        C26211Cl r8 = this.A00;
        C14850m9 r1 = r8.A01.A00;
        if (r1.A07(450) && r1.A07(1273)) {
            List<C48142Em> A00 = r8.A00();
            ArrayList arrayList = new ArrayList();
            for (C48142Em r5 : A00) {
                if (((double) (((int) (System.currentTimeMillis() - r5.A00)) / 86400000)) > 90.0d) {
                    arrayList.add(r5);
                }
            }
            A00.removeAll(arrayList);
            r8.A01(A00);
        }
    }
}
