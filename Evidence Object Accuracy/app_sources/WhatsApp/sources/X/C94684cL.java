package X;

import com.facebook.msys.mci.DefaultCrypto;
import java.nio.charset.Charset;

/* renamed from: X.4cL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C94684cL {
    public static final Object A00 = C12970iu.A0l();
    public static final Charset A01 = Charset.forName("ISO-8859-1");
    public static final Charset A02 = Charset.forName(DefaultCrypto.UTF_8);

    public static boolean A00(Object[] objArr, Object[] objArr2) {
        int length;
        int length2;
        if (objArr == null) {
            length = 0;
        } else {
            length = objArr.length;
        }
        if (objArr2 == null) {
            length2 = 0;
        } else {
            length2 = objArr2.length;
        }
        int i = 0;
        int i2 = 0;
        while (true) {
            if (i >= length || objArr[i] != null) {
                while (i2 < length2 && objArr2[i2] == null) {
                    i2++;
                }
                boolean A1X = C12990iw.A1X(i, length);
                boolean A1X2 = C12990iw.A1X(i2, length2);
                if (!A1X) {
                    if (A1X != A1X2 || !objArr[i].equals(objArr2[i2])) {
                        break;
                    }
                    i++;
                    i2++;
                } else if (A1X2) {
                    return true;
                }
            } else {
                i++;
            }
        }
        return false;
    }
}
