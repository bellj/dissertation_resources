package X;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0fk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C11100fk extends C114085Kd {
    public static final int A00(AnonymousClass1WO r2) {
        Iterator it = r2.iterator();
        int i = 0;
        while (it.hasNext()) {
            it.next();
            i++;
            if (i < 0) {
                throw new ArithmeticException("Count overflow has happened.");
            }
        }
        return i;
    }

    public static final Iterable A01(AnonymousClass1WO r1) {
        return new C111935Bj(r1);
    }

    public static final List A02(AnonymousClass1WO r1) {
        C16700pc.A0E(r1, 0);
        return C16770pj.A0H(A03(r1));
    }

    public static final List A03(AnonymousClass1WO r1) {
        ArrayList arrayList = new ArrayList();
        A05(arrayList, r1);
        return arrayList;
    }

    public static final AnonymousClass1WO A04(AnonymousClass1J7 r1, AnonymousClass1WO r2) {
        return new C112835Ex(r1, r2);
    }

    public static final void A05(Collection collection, AnonymousClass1WO r3) {
        for (Object obj : r3) {
            collection.add(obj);
        }
    }
}
