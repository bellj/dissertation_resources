package X;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.biz.catalog.view.AspectRatioFrameLayout;

/* renamed from: X.34g  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass34g extends AbstractC84543zT {
    public final FrameLayout A00;
    public final LinearLayout A01 = ((LinearLayout) AnonymousClass028.A0D(this, R.id.button_frame));
    public final WaImageView A02 = C12980iv.A0X(this, R.id.kept_status);
    public final WaImageView A03 = C12980iv.A0X(this, R.id.starred_status);

    public abstract int getMark();

    public int getMarkTintColor() {
        return -1;
    }

    public abstract float getRatio();

    public AnonymousClass34g(Context context) {
        super(context);
        ((AspectRatioFrameLayout) this).A00 = getRatio();
        FrameLayout.inflate(context, R.layout.search_message_video_preview, this);
        FrameLayout frameLayout = (FrameLayout) AnonymousClass028.A0D(this, R.id.overlay);
        this.A00 = frameLayout;
        ImageView A0K = C12970iu.A0K(this, R.id.button_image);
        Drawable A04 = AnonymousClass00T.A04(context, getMark());
        if (A04 != null) {
            int markTintColor = getMarkTintColor();
            A04 = markTintColor != -1 ? AnonymousClass2GE.A03(context, A04, markTintColor) : A04;
            A0K.setImageDrawable(A04);
            frameLayout.setLayoutParams(new FrameLayout.LayoutParams(-1, A04.getIntrinsicHeight() << 1, 80));
            frameLayout.setVisibility(0);
            A0K.setImageDrawable(A04);
        }
    }

    @Override // X.AbstractC84543zT
    public void setMessage(AbstractC16130oV r6) {
        super.A02 = r6;
        WaImageView waImageView = this.A03;
        WaImageView waImageView2 = this.A02;
        if (r6 != null) {
            if (r6.A0v) {
                waImageView.setVisibility(0);
            } else {
                waImageView.setVisibility(8);
            }
            waImageView2.setVisibility(8);
        }
    }

    @Override // X.AbstractC84543zT
    public void setRadius(int i) {
        ((AbstractC84543zT) this).A00 = i;
        if (i > 0) {
            FrameLayout frameLayout = this.A00;
            frameLayout.setBackground(AnonymousClass00T.A04(getContext(), R.drawable.search_media_thumbnail_rounded_overlay));
            frameLayout.setLayoutParams(C12990iw.A0M());
            ((GradientDrawable) frameLayout.getBackground()).setCornerRadius((float) i);
        }
    }
}
