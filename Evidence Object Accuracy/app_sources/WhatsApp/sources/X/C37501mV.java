package X;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Build;
import android.util.Base64;
import com.whatsapp.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/* renamed from: X.1mV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37501mV {
    public static int A00(int i, int i2, int i3, int i4, int i5) {
        int i6 = ((i - 1) / i5) + 1;
        int i7 = ((i2 - 1) / i5) + 1;
        while (((i6 - 1) / 2) + 1 >= i3 && ((i7 - 1) / 2) + 1 >= i4) {
            i6 = ((i6 - 1) / 2) + 1;
            i7 = ((i7 - 1) / 2) + 1;
            i5 <<= 1;
        }
        return i5;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0004, code lost:
        if (r13 < 0) goto L_0x0006;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A01(X.C41591tm r11, int r12, int r13) {
        /*
            r3 = 1
            if (r12 < 0) goto L_0x0006
            r2 = 1
            if (r13 >= 0) goto L_0x0007
        L_0x0006:
            r2 = 0
        L_0x0007:
            java.lang.String r0 = "bitmaputils/wrong raw image/"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            r1.append(r12)
            java.lang.String r0 = ","
            r1.append(r0)
            r1.append(r13)
            java.lang.String r0 = r1.toString()
            X.AnonymousClass009.A0A(r0, r2)
            if (r2 != 0) goto L_0x0023
            return r3
        L_0x0023:
            android.graphics.BitmapFactory$Options r0 = r11.A02
            int r8 = r0.inSampleSize
            java.lang.Long r7 = r11.A03
            r6 = r12
            r5 = r13
            r4 = 1
            if (r7 != 0) goto L_0x0044
        L_0x002e:
            boolean r0 = r11.A04
            if (r0 == 0) goto L_0x0037
            int r12 = java.lang.Math.max(r12, r13)
            r13 = r12
        L_0x0037:
            int r2 = java.lang.Math.max(r8, r4)
            int r1 = r11.A01
            int r0 = r11.A00
            int r0 = A00(r12, r13, r1, r0, r2)
            return r0
        L_0x0044:
            long r2 = (long) r6
            long r0 = (long) r5
            long r2 = r2 * r0
            long r9 = r7.longValue()
            int r0 = (r2 > r9 ? 1 : (r2 == r9 ? 0 : -1))
            if (r0 <= 0) goto L_0x002e
            r1 = 2
            int r0 = r6 + -1
            int r0 = r0 / r1
            int r6 = r0 + 1
            int r0 = r5 + -1
            int r0 = r0 / r1
            int r5 = r0 + 1
            int r4 = r4 << 1
            goto L_0x0044
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C37501mV.A01(X.1tm, int, int):int");
    }

    public static Bitmap A02(BitmapFactory.Options options, InputStream inputStream) {
        if (Build.VERSION.SDK_INT >= 21) {
            return BitmapFactory.decodeStream(inputStream, null, options);
        }
        return A05(new C41591tm(options, null, Integer.MAX_VALUE, Integer.MAX_VALUE, false), inputStream).A02;
    }

    public static Path A03(RectF rectF) {
        RectF rectF2 = new RectF(rectF);
        float min = Math.min(rectF2.width(), rectF2.height());
        rectF2.right = rectF2.left + min;
        rectF2.bottom = rectF2.top + min;
        Path path = new Path();
        path.moveTo(rectF2.centerX(), rectF2.top);
        float f = rectF2.left;
        float f2 = rectF2.top;
        path.cubicTo(f, f2, f, f2, f, rectF2.centerY());
        float f3 = rectF2.left;
        float f4 = rectF2.bottom;
        path.cubicTo(f3, f4, f3, f4, rectF2.centerX(), rectF2.bottom);
        float f5 = rectF2.right;
        float f6 = rectF2.bottom;
        path.cubicTo(f5, f6, f5, f6, f5, rectF2.centerY());
        float f7 = rectF2.right;
        float f8 = rectF2.top;
        path.cubicTo(f7, f8, f7, f8, rectF2.centerX(), rectF2.top);
        path.close();
        return path;
    }

    public static C41601tn A04(C41591tm r5, File file) {
        Bitmap bitmap = null;
        if (Build.VERSION.SDK_INT >= 21) {
            BitmapFactory.Options options = r5.A02;
            if (!options.inJustDecodeBounds) {
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(file.getAbsolutePath(), options);
            }
            int i = options.outWidth;
            int i2 = options.outHeight;
            if (i <= 0 || i2 <= 0) {
                Log.e("bitmapcache/decode bad image");
                return new C41601tn(i, i2, null);
            }
            options.inSampleSize = A01(r5, i, i2);
            options.inJustDecodeBounds = false;
            try {
                bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), options);
            } catch (Throwable th) {
                Log.w("bitmaputils/error-on-decode-api21+", th);
            }
            return new C41601tn(i, i2, bitmap);
        }
        try {
            try {
                return A06(r5, AnonymousClass1NM.A00(new Object(file) { // from class: X.4JS
                    public final /* synthetic */ File A00;

                    {
                        this.A00 = r1;
                    }
                }.A00));
            } catch (IOException e) {
                Log.e("bitmaputils/decoder failed", e);
                return new C41601tn(0, 0, null);
            }
        } catch (Throwable th2) {
            Log.w("bitmaputils/error-on-decode", th2);
            return new C41601tn(r5.A01, r5.A00, null);
        }
    }

    public static C41601tn A05(C41591tm r1, InputStream inputStream) {
        try {
            return A06(r1, AnonymousClass1P1.A04(new Object(inputStream) { // from class: X.4JT
                public final /* synthetic */ InputStream A00;

                {
                    this.A00 = r1;
                }
            }.A00));
        } catch (IOException e) {
            Log.e("bitmaputils/decoder failed", e);
            return new C41601tn(0, 0, null);
        }
    }

    public static C41601tn A06(C41591tm r7, byte[] bArr) {
        BitmapFactory.Options options = r7.A02;
        if (!options.inJustDecodeBounds) {
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
        }
        int i = options.outWidth;
        int i2 = options.outHeight;
        if (i <= 0 || i2 <= 0) {
            Log.e("bitmaputils/decode bad image");
            return new C41601tn(i, i2, null);
        }
        options.inSampleSize = A01(r7, i, i2);
        options.inJustDecodeBounds = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        return new C41601tn(i, i2, BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options));
    }

    public static String A07(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        String encodeToString = Base64.encodeToString(byteArrayOutputStream.toByteArray(), 0);
        AnonymousClass1P1.A03(byteArrayOutputStream);
        return encodeToString;
    }
}
