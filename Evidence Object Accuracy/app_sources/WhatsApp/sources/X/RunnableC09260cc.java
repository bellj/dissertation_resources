package X;

import androidx.preference.PreferenceGroup;

/* renamed from: X.0cc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09260cc implements Runnable {
    public final /* synthetic */ PreferenceGroup A00;

    public RunnableC09260cc(PreferenceGroup preferenceGroup) {
        this.A00 = preferenceGroup;
    }

    @Override // java.lang.Runnable
    public void run() {
        synchronized (this) {
            this.A00.A06.clear();
        }
    }
}
