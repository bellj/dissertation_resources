package X;

import com.facebook.redex.RunnableBRunnable0Shape9S0100000_I0_9;
import java.util.TimerTask;

/* renamed from: X.1J9  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1J9 extends TimerTask {
    public final /* synthetic */ C27611If A00;

    public /* synthetic */ AnonymousClass1J9(C27611If r1) {
        this.A00 = r1;
    }

    @Override // java.util.TimerTask, java.lang.Runnable
    public void run() {
        C27611If r3 = this.A00;
        if (!r3.A03) {
            C27611If.A0O.remove(r3.A02.toString());
            if (!r3.A04) {
                r3.A07.A0H(new RunnableBRunnable0Shape9S0100000_I0_9(this, 34));
            }
            r3.A01(2);
            AnonymousClass1P4 r0 = r3.A0K;
            if (r0 != null) {
                r3.A0M.A0H(r0.A01, 500);
            }
        }
    }
}
