package X;

import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

/* renamed from: X.1v3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C42341v3 implements AbstractC21730xt {
    public final AbstractC15710nm A00;
    public final AbstractC42321v1 A01;
    public final C17220qS A02;
    public final Map A03 = Collections.synchronizedMap(new HashMap());
    public final boolean A04;

    public C42341v3(AbstractC15710nm r2, AbstractC42321v1 r3, C17220qS r4, boolean z) {
        this.A00 = r2;
        this.A02 = r4;
        this.A04 = z;
        this.A01 = r3;
    }

    public static final C42111ug A00(AnonymousClass1V8 r9, String str) {
        Long l;
        if (r9.A0E("error") != null) {
            AnonymousClass1V8 A0F = r9.A0F("error");
            Long valueOf = Long.valueOf(Math.min(A0F.A08("backoff", 7200) * 1000, 3600000L));
            String A0I = A0F.A0I("text", null);
            int A05 = A0F.A05("code", -1);
            long A08 = A0F.A08("backoff", -1);
            StringBuilder sb = new StringBuilder("connection/unisynciq/parse/");
            sb.append(str);
            sb.append("/error/error_text= ");
            sb.append(A0I);
            sb.append(", code: ");
            sb.append(A05);
            sb.append(", backoff:");
            sb.append(A08);
            Log.w(sb.toString());
            return new C42111ug(Integer.valueOf(A05), null, valueOf, false);
        }
        String A0I2 = r9.A0I("refresh", null);
        if (A0I2 != null) {
            l = Long.valueOf(Long.parseLong(A0I2) * 1000);
        } else {
            l = null;
        }
        return new C42111ug(null, l, null, true);
    }

    public static AnonymousClass1V8 A01(C28601Of r3, int i) {
        Integer valueOf = Integer.valueOf(i);
        Map map = r3.A00;
        AnonymousClass1W9[] r4 = null;
        if (!map.containsKey(valueOf)) {
            return null;
        }
        String str = (String) map.get(valueOf);
        if (!TextUtils.isEmpty(str)) {
            r4 = new AnonymousClass1W9[]{new AnonymousClass1W9("dhash", str)};
        }
        return new AnonymousClass1V8("consumer_status", r4);
    }

    public static AnonymousClass1V8 A02(C28601Of r3, int i) {
        Integer valueOf = Integer.valueOf(i);
        Map map = r3.A00;
        AnonymousClass1W9[] r4 = null;
        if (!map.containsKey(valueOf)) {
            return null;
        }
        String str = (String) map.get(valueOf);
        if (!TextUtils.isEmpty(str)) {
            r4 = new AnonymousClass1W9[]{new AnonymousClass1W9("dhash", str)};
        }
        return new AnonymousClass1V8("eligible_offers", r4);
    }

    public static Future A03(AnonymousClass1JA r4, C42341v3 r5, String str, List list, int i) {
        return r5.A04(new C50242Os(r4, list, i, false), C42391v8.A00(str), 64000);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:136:0x036e, code lost:
        if (r32 != false) goto L_0x0370;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:214:0x05f6, code lost:
        if ((r27 & 16) == 16) goto L_0x05f8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x02ab, code lost:
        if ((r27 & 16) == 16) goto L_0x02ad;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.concurrent.Future A04(X.C50242Os r40, java.lang.String r41, long r42) {
        /*
        // Method dump skipped, instructions count: 1687
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C42341v3.A04(X.2Os, java.lang.String, long):java.util.concurrent.Future");
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        AnonymousClass4UE r0 = (AnonymousClass4UE) this.A03.remove(str);
        if (r0 != null) {
            r0.A02.A00(new AnonymousClass2JZ(str));
        } else {
            Log.w("UniSyncProtocolHelper/onDeliveryFailure missing request");
        }
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r11, String str) {
        AnonymousClass4UE r2 = (AnonymousClass4UE) this.A03.remove(str);
        if (r2 != null) {
            AnonymousClass1V8 A0E = r11.A0E("error");
            int i = 0;
            long j = -1;
            if (A0E != null) {
                String A0I = A0E.A0I("code", null);
                if (A0I != null) {
                    i = Integer.parseInt(A0I);
                }
                String A0I2 = A0E.A0I("backoff", null);
                if (A0I2 != null) {
                    j = Math.min(Long.parseLong(A0I2) * 1000, 3600000L);
                }
            }
            StringBuilder sb = new StringBuilder("UniSyncProtocolHelper/handleSyncContactError sid=");
            String str2 = r2.A04;
            sb.append(str2);
            sb.append(" code=");
            sb.append(i);
            sb.append(" backoff=");
            sb.append(j);
            Log.i(sb.toString());
            this.A01.AI5(r2.A01, str2, 0, i, j);
            r2.A02.A01(null);
            return;
        }
        Log.w("UniSyncProtocolHelper/onError missing request");
    }

    /* JADX WARNING: Removed duplicated region for block: B:103:0x02b1  */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x02fc  */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x031c  */
    /* JADX WARNING: Removed duplicated region for block: B:137:0x0410  */
    /* JADX WARNING: Removed duplicated region for block: B:177:0x056c  */
    /* JADX WARNING: Removed duplicated region for block: B:180:0x0595  */
    /* JADX WARNING: Removed duplicated region for block: B:197:0x061b  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x01cd  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x01e4  */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x024f  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x025d  */
    @Override // X.AbstractC21730xt
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AX9(X.AnonymousClass1V8 r41, java.lang.String r42) {
        /*
        // Method dump skipped, instructions count: 1794
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C42341v3.AX9(X.1V8, java.lang.String):void");
    }
}
