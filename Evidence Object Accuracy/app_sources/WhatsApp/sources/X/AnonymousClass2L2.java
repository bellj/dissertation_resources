package X;

import com.whatsapp.util.Log;
import java.net.Socket;

/* renamed from: X.2L2  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2L2 {
    public final Socket A00;

    public AnonymousClass2L2(Socket socket) {
        this.A00 = socket;
    }

    public void A00() {
        try {
            Socket socket = this.A00;
            if (!socket.isOutputShutdown()) {
                socket.shutdownOutput();
            }
        } catch (Exception unused) {
        }
        try {
            Socket socket2 = this.A00;
            if (!socket2.isInputShutdown()) {
                socket2.shutdownInput();
            }
        } catch (Exception unused2) {
        }
        try {
            Socket socket3 = this.A00;
            if (!socket3.isClosed()) {
                socket3.close();
            }
        } catch (Exception e) {
            Log.i("ConnectionSocketDefault/closeSocket ", e);
        }
    }
}
