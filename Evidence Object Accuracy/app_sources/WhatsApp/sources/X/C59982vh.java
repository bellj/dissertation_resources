package X;

import com.google.android.material.chip.Chip;
import com.whatsapp.R;

/* renamed from: X.2vh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59982vh extends C60052vo {
    public C59982vh(Chip chip, AnonymousClass2Jw r2) {
        super(chip, r2);
    }

    @Override // X.C60052vo, X.AbstractC75703kH
    public void A08(AnonymousClass4UW r9) {
        C30211Wn r7 = ((C59692vE) r9).A00;
        boolean A00 = r9.A00();
        Chip chip = ((C60052vo) this).A00;
        if (A00) {
            chip.setChipIconResource(R.drawable.ic_settings_disable);
            chip.setChipIconVisible(true);
        } else {
            chip.setChipIconVisible(false);
        }
        super.A08(r9);
        String str = r7.A01;
        chip.setText(str);
        chip.setContentDescription(C12960it.A0X(chip.getContext(), str, new Object[1], 0, R.string.biz_accessibility_remove_selected_filter));
        C12990iw.A1C(chip, this, r9, r7, 7);
    }
}
