package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4jx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C99224jx implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        int i = 0;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            if (((char) readInt) != 2) {
                C95664e9.A0D(parcel, readInt);
            } else {
                i = C95664e9.A02(parcel, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78003oD(i);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new C78003oD[i];
    }
}
