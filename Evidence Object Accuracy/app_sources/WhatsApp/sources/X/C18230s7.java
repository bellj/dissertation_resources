package X;

import android.app.AlarmManager;
import android.app.PendingIntent;

/* renamed from: X.0s7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18230s7 {
    public final AnonymousClass01d A00;

    public C18230s7(AnonymousClass01d r1) {
        this.A00 = r1;
    }

    public boolean A00() {
        AlarmManager A04 = this.A00.A04();
        return A04 != null && A04.canScheduleExactAlarms();
    }

    public boolean A01(PendingIntent pendingIntent, int i, long j) {
        boolean A01;
        AlarmManager A04 = this.A00.A04();
        if (A04 == null) {
            return false;
        }
        if (C28391Mz.A05()) {
            A01 = A00();
        } else {
            A01 = C28391Mz.A01();
        }
        if (A01) {
            A04.setExact(i, j, pendingIntent);
            return true;
        }
        A04.set(i, j, pendingIntent);
        return true;
    }

    public boolean A02(PendingIntent pendingIntent, int i, long j) {
        AlarmManager A04 = this.A00.A04();
        if (A04 == null) {
            return false;
        }
        if (C28391Mz.A05()) {
            if (!A00()) {
                A04.setAndAllowWhileIdle(i, j, pendingIntent);
                return true;
            }
        } else if (!C28391Mz.A03()) {
            if (C28391Mz.A01()) {
                A04.setExact(i, j, pendingIntent);
                return true;
            }
            A04.set(i, j, pendingIntent);
            return true;
        }
        A04.setExactAndAllowWhileIdle(i, j, pendingIntent);
        return true;
    }
}
