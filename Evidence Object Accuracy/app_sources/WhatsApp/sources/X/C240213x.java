package X;

import android.content.ContentValues;
import android.database.Cursor;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* renamed from: X.13x  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C240213x {
    public final C15570nT A00;
    public final C241814n A01;
    public final C16510p9 A02;
    public final C19990v2 A03;
    public final C16490p7 A04;
    public final AnonymousClass150 A05;
    public final C14850m9 A06;
    public final C22140ya A07;

    public C240213x(C15570nT r1, C241814n r2, C16510p9 r3, C19990v2 r4, C16490p7 r5, AnonymousClass150 r6, C14850m9 r7, C22140ya r8) {
        this.A06 = r7;
        this.A02 = r3;
        this.A00 = r1;
        this.A03 = r4;
        this.A01 = r2;
        this.A04 = r5;
        this.A07 = r8;
        this.A05 = r6;
    }

    public final int A00(long j) {
        C16310on A01 = this.A04.get();
        try {
            int i = 0;
            Cursor A09 = A01.A03.A09("SELECT setting_duration FROM message_ephemeral_setting WHERE message_row_id = ?", new String[]{Long.toString(j)});
            if (A09 != null) {
                if (A09.moveToNext()) {
                    i = A09.getInt(A09.getColumnIndexOrThrow("setting_duration"));
                }
                A09.close();
            }
            A01.close();
            return i;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final AnonymousClass1PE A01(AnonymousClass1XK r4) {
        UserJid of = UserJid.of(r4.A0z.A00);
        if (of == null) {
            Log.e("EphemeralSettingMessageStore/getChatInfo/not a user");
            return null;
        }
        AnonymousClass1PE A06 = this.A03.A06(of);
        if (A06 != null) {
            return A06;
        }
        Log.w("EphemeralSettingMessageStore/getChatInfo/no chat");
        return null;
    }

    public Integer A02(AbstractC15340mz r10) {
        C16510p9 r1 = this.A02;
        AbstractC14640lm r0 = r10.A0z.A00;
        AnonymousClass009.A05(r0);
        long A02 = r1.A02(r0);
        C16310on A01 = this.A04.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT setting_duration FROM message_ephemeral_setting INNER JOIN message_view ON message_ephemeral_setting.message_row_id=message_view._id WHERE chat_row_id == ? AND sort_id < ? ORDER BY sort_id DESC LIMIT 1", new String[]{Long.toString(A02), Long.toString(r10.A12)});
            if (A09 == null || !A09.moveToNext()) {
                if (A09 != null) {
                    A09.close();
                }
                A01.close();
                return null;
            }
            Integer valueOf = Integer.valueOf(A09.getInt(A09.getColumnIndexOrThrow("setting_duration")));
            A09.close();
            A01.close();
            return valueOf;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A03(int i, long j, int i2) {
        C16310on A02 = this.A04.A02();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("message_row_id", Long.valueOf(j));
            contentValues.put("setting_duration", Integer.valueOf(i));
            contentValues.put("setting_reason", Integer.valueOf(i2));
            A02.A03.A06(contentValues, "message_ephemeral_setting", 5);
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001d, code lost:
        if (r0 <= r0) goto L_0x001f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A04(X.AnonymousClass1PE r13, X.AnonymousClass1XK r14) {
        /*
            r12 = this;
            if (r13 != 0) goto L_0x0009
            X.1PE r13 = r12.A01(r14)
            if (r13 != 0) goto L_0x0009
        L_0x0008:
            return
        L_0x0009:
            X.1PG r7 = r13.A0Y
            X.14n r5 = r12.A01
            int r9 = r14.A00
            int r0 = r7.expiration
            if (r9 != r0) goto L_0x002d
            long r0 = r14.A0I
            long r2 = r7.ephemeralSettingTimestamp
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 <= 0) goto L_0x002d
            int r2 = (r0 > r0 ? 1 : (r0 == r0 ? 0 : -1))
            if (r2 > 0) goto L_0x002d
        L_0x001f:
            int r3 = r14.A00
            long r1 = r14.A0I
            r0 = 0
            r13.A0A(r3, r1, r0)
            X.0p9 r0 = r12.A02
            r0.A0D(r13)
            return
        L_0x002d:
            int r1 = r14.A08()
            r0 = 1
            if (r1 != r0) goto L_0x0008
            X.1IS r0 = r14.A0z
            X.0lm r6 = r0.A00
            long r10 = r14.A0I
            java.lang.Long r8 = java.lang.Long.valueOf(r10)
            boolean r0 = r5.A03(r6, r7, r8, r9, r10)
            if (r0 == 0) goto L_0x0008
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C240213x.A04(X.1PE, X.1XK):void");
    }

    public void A05(AnonymousClass1XK r3) {
        Integer valueOf = Integer.valueOf(A00(r3.A11));
        if (valueOf == null || valueOf.intValue() <= 0) {
            valueOf = 0;
        }
        r3.A00 = valueOf.intValue();
    }
}
