package X;

import com.whatsapp.group.GroupProfileEmojiEditor;

/* renamed from: X.4uM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C105624uM implements AbstractC009404s {
    public final /* synthetic */ GroupProfileEmojiEditor A00;
    public final /* synthetic */ int[] A01;

    public C105624uM(GroupProfileEmojiEditor groupProfileEmojiEditor, int[] iArr) {
        this.A00 = groupProfileEmojiEditor;
        this.A01 = iArr;
    }

    @Override // X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        return (AnonymousClass015) cls.cast(new C74473i5(this.A01[0]));
    }
}
