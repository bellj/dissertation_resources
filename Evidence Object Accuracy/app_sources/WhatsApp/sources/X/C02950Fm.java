package X;

/* renamed from: X.0Fm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02950Fm extends AnonymousClass0OL {
    public C02950Fm() {
        super(11, 12);
    }

    @Override // X.AnonymousClass0OL
    public void A00(AbstractC12920im r3) {
        ((AnonymousClass0ZE) r3).A00.execSQL("ALTER TABLE workspec ADD COLUMN `out_of_quota_policy` INTEGER NOT NULL DEFAULT 0");
    }
}
