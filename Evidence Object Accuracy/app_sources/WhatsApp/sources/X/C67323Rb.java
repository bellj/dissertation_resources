package X;

import com.whatsapp.status.viewmodels.StatusesViewModel;

/* renamed from: X.3Rb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67323Rb implements AbstractC009404s {
    public final /* synthetic */ C48962Ip A00;
    public final /* synthetic */ boolean A01;

    public C67323Rb(C48962Ip r1, boolean z) {
        this.A00 = r1;
        this.A01 = z;
    }

    @Override // X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        C48962Ip r0 = this.A00;
        boolean z = this.A01;
        AnonymousClass01J r1 = r0.A00.A03;
        AbstractC14440lR A0T = C12960it.A0T(r1);
        return new StatusesViewModel((AnonymousClass12H) r1.AC5.get(), (C18470sV) r1.AK8.get(), (C244015j) r1.AK0.get(), (AnonymousClass1BD) r1.AKA.get(), (AnonymousClass1BE) r1.AHV.get(), A0T, z);
    }
}
