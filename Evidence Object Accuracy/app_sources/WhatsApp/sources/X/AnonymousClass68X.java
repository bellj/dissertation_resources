package X;

import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.68X  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass68X implements AbstractC136436Mn {
    public final /* synthetic */ AbstractActivityC121545iU A00;

    public AnonymousClass68X(AbstractActivityC121545iU r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC136436Mn
    public void AQw() {
        AbstractActivityC121545iU r3 = this.A00;
        r3.A0I.A0A("onGetChallengeFailure got; showErrorAndFinish", null);
        r3.A39();
    }

    @Override // X.AbstractC136436Mn
    public void AR3(C452120p r6, boolean z) {
        AbstractActivityC121545iU r3 = this.A00;
        r3.AaN();
        if (!z) {
            C30931Zj r4 = r3.A0I;
            r4.A0A("onGetToken got; failure", null);
            if (r3.A06.A07("upi-get-token")) {
                r4.A0A("retry get token", null);
                C1329668y r42 = ((AbstractActivityC121665jA) r3).A0B;
                synchronized (r42) {
                    try {
                        C18600si r2 = r42.A03;
                        JSONObject A0b = C117295Zj.A0b(r2);
                        A0b.remove("token");
                        A0b.remove("tokenTs");
                        C117295Zj.A1E(r2, A0b);
                    } catch (JSONException e) {
                        Log.w("PAY: IndiaUpiPaymentSharedPrefs deleteTokenAndKeys threw: ", e);
                    }
                }
                r3.A3B();
                r3.A36();
                return;
            }
            if (r6 != null) {
                r4.A0A(C12960it.A0b("onGetToken showErrorAndFinish error: ", r6), null);
                if (AnonymousClass69E.A02(r3, "upi-get-token", r6.A00, true)) {
                    return;
                }
            } else {
                r4.A0A("onGetToken showErrorAndFinish", null);
            }
            r3.A39();
        }
    }

    @Override // X.AbstractC136436Mn
    public void AUn(boolean z) {
        AbstractActivityC121545iU r3 = this.A00;
        if (r3.AJN()) {
            return;
        }
        if (z) {
            r3.A06.A03("upi-register-app");
            boolean z2 = r3.A0H;
            C30931Zj r1 = r3.A0I;
            if (z2) {
                r1.A0A("internal error ShowPinError", null);
                r3.A3C();
                return;
            }
            r1.A06("onRegisterApp registered ShowMainPane");
            r3.A3A();
            return;
        }
        r3.A0I.A0A("onRegisterApp not registered; showErrorAndFinish", null);
        r3.A39();
    }
}
