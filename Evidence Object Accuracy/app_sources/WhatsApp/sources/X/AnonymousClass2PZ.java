package X;

/* renamed from: X.2PZ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2PZ extends AnonymousClass1JW {
    public final int A00;
    public final int A01;
    public final AnonymousClass2LB A02;
    public final AbstractC14640lm A03;
    public final String A04;

    public AnonymousClass2PZ(AbstractC15710nm r2, AnonymousClass2LB r3, C15450nH r4, AbstractC14640lm r5, String str, int i, int i2) {
        super(r2, r4);
        this.A05 = 19;
        this.A03 = r5;
        this.A02 = r3;
        this.A01 = i;
        this.A00 = i2;
        this.A04 = str;
    }
}
