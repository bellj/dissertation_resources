package X;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.CancellationSignal;
import android.provider.MediaStore;
import java.util.ArrayList;

/* renamed from: X.2fx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54012fx extends AnonymousClass0ER {
    public static final String[] A04 = {"_id", "artist", "title", "_data", "duration", "_size"};
    public Cursor A00;
    public CancellationSignal A01;
    public final ContentResolver A02;
    public final ArrayList A03;

    public C54012fx(ContentResolver contentResolver, Context context, ArrayList arrayList) {
        super(context);
        this.A02 = contentResolver;
        if (arrayList == null) {
            this.A03 = C12960it.A0l();
        } else {
            this.A03 = arrayList;
        }
    }

    @Override // X.AnonymousClass0QL
    public void A01() {
        A00();
        Cursor cursor = this.A00;
        if (cursor != null && !cursor.isClosed()) {
            this.A00.close();
        }
        this.A00 = null;
    }

    @Override // X.AnonymousClass0QL
    public void A02() {
        A00();
    }

    @Override // X.AnonymousClass0QL
    public void A03() {
        Cursor cursor = this.A00;
        if (cursor != null) {
            A04(cursor);
        }
        boolean z = super.A03;
        super.A03 = false;
        this.A04 |= z;
        if (z || this.A00 == null) {
            A09();
        }
    }

    @Override // X.AnonymousClass0ER
    public /* bridge */ /* synthetic */ Object A06() {
        synchronized (this) {
            try {
                if (!C12960it.A1W(((AnonymousClass0ER) this).A01)) {
                    this.A01 = new CancellationSignal();
                } else {
                    throw new AnonymousClass04U(null);
                }
            } catch (Throwable th) {
                throw th;
            }
        }
        try {
            ArrayList arrayList = this.A03;
            int i = 0;
            String[] strArr = new String[arrayList == null ? 0 : arrayList.size() << 1];
            StringBuilder A0h = C12960it.A0h();
            while (arrayList != null && i < arrayList.size()) {
                A0h.append(" AND ");
                A0h.append("(");
                A0h.append("title");
                A0h.append(" LIKE ?");
                A0h.append(" OR ");
                A0h.append("artist");
                A0h.append(" LIKE ?");
                A0h.append(")");
                int i2 = i << 1;
                StringBuilder A0h2 = C12960it.A0h();
                A0h2.append("%");
                A0h2.append((String) arrayList.get(i));
                strArr[i2] = C12960it.A0d("%", A0h2);
                StringBuilder A0j = C12960it.A0j("%");
                A0j.append((String) arrayList.get(i));
                strArr[i2 + 1] = C12960it.A0d("%", A0j);
                i++;
            }
            Cursor query = this.A02.query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, A04, C12960it.A0Z(A0h, "(is_music!=0 OR is_podcast!=0)", C12960it.A0h()), strArr, "date_modified DESC", this.A01);
            if (query != null) {
                try {
                    query.getCount();
                } catch (RuntimeException e) {
                    query.close();
                    throw e;
                }
            }
            synchronized (this) {
                try {
                    this.A01 = null;
                } catch (Throwable th2) {
                    throw th2;
                }
            }
            return query;
        } catch (Throwable th3) {
            synchronized (this) {
                try {
                    this.A01 = null;
                    throw th3;
                } catch (Throwable th4) {
                    throw th4;
                }
            }
        }
    }

    @Override // X.AnonymousClass0ER
    public void A07() {
        synchronized (this) {
            CancellationSignal cancellationSignal = this.A01;
            if (cancellationSignal != null) {
                cancellationSignal.cancel();
            }
        }
    }

    @Override // X.AnonymousClass0ER
    public void A0B(Object obj) {
        Cursor cursor = (Cursor) obj;
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
    }

    /* renamed from: A0C */
    public void A04(Cursor cursor) {
        if (!this.A05) {
            Cursor cursor2 = this.A00;
            this.A00 = cursor;
            if (this.A06) {
                super.A04(cursor);
            }
            if (cursor2 != null && cursor2 != cursor && !cursor2.isClosed()) {
                cursor2.close();
            }
        } else if (cursor != null) {
            cursor.close();
        }
    }
}
