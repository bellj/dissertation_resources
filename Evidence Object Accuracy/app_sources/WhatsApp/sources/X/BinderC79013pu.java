package X;

import android.os.IInterface;

/* renamed from: X.3pu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class BinderC79013pu extends AbstractBinderC73263fw implements IInterface {
    public final /* synthetic */ C13690kA A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public BinderC79013pu(C13690kA r2) {
        super("com.google.android.gms.auth.blockstore.internal.IStoreBytesCallback");
        this.A00 = r2;
    }
}
