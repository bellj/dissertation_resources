package X;

import com.facebook.msys.mci.DefaultCrypto;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.whatsapp.net.tls13.WtCachedPsk;
import com.whatsapp.voipcalling.DefaultCryptoCallback;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.net.ssl.HandshakeCompletedEvent;
import javax.net.ssl.HandshakeCompletedListener;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;

/* renamed from: X.5Ib  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C113565Ib extends SSLSocket implements AbstractC115355Rf {
    public int A00;
    public long A01;
    public AnonymousClass49E A02;
    public AnonymousClass49I A03;
    public AnonymousClass584 A04;
    public AbstractC113555Ia A05;
    public AnonymousClass1NQ A06;
    public AnonymousClass1NQ A07;
    public AnonymousClass4V3 A08;
    public InputStream A09;
    public OutputStream A0A;
    public String A0B;
    public Set A0C;
    public boolean A0D;
    public boolean A0E;
    public boolean A0F;

    public static String A00(byte b) {
        if (b == 0) {
            return "close_notify";
        }
        if (b == 10) {
            return "unexpected_message";
        }
        if (b == 20) {
            return "bad_record_mac";
        }
        if (b == 22) {
            return "record_overflow";
        }
        if (b == 40) {
            return "handshake_failure";
        }
        if (b == 80) {
            return "internal_error";
        }
        if (b == 86) {
            return "inappropriate_fallback";
        }
        if (b == 90) {
            return "user_cancelled";
        }
        if (b == 120) {
            return "no_application_protocol";
        }
        if (b == 70) {
            return "protocol_version";
        }
        if (b == 71) {
            return "insufficient_security";
        }
        if (b == 109) {
            return "missing_extension";
        }
        if (b == 110) {
            return "unsupported_version";
        }
        if (b == 112) {
            return "unrecognized_name";
        }
        if (b == 113) {
            return "bad_certificate_status_response";
        }
        if (b == 115) {
            return "unknown_psk_identity";
        }
        if (b == 116) {
            return "certificate_required";
        }
        switch (b) {
            case 42:
                return "bad_certificate";
            case 43:
                return "unsupported_certificate";
            case 44:
                return "certificate_revoked";
            case 45:
                return "certificate_expired";
            case DefaultCryptoCallback.E2E_EXTENDED_V2_KEY_LENGTH /* 46 */:
                return "certificate_unknown";
            case 47:
                return "illegal_parameter";
            case 48:
                return "unknown_ca";
            case 49:
                return "access_denied";
            case SearchActionVerificationClientService.TIME_TO_SLEEP_IN_MS /* 50 */:
                return "decode_error";
            case 51:
                return "decrypt_error";
            default:
                return "invalid description";
        }
    }

    @Override // javax.net.ssl.SSLSocket
    public boolean getEnableSessionCreation() {
        return true;
    }

    @Override // javax.net.ssl.SSLSocket
    public String[] getEnabledProtocols() {
        return new String[]{"TLSv1.3", "TLSv1.2"};
    }

    @Override // javax.net.ssl.SSLSocket
    public String[] getSupportedProtocols() {
        return new String[]{"TLSv1.3", "TLSv1.2"};
    }

    @Override // javax.net.ssl.SSLSocket
    public boolean getUseClientMode() {
        return true;
    }

    @Override // javax.net.ssl.SSLSocket
    public void setEnableSessionCreation(boolean z) {
    }

    @Override // javax.net.ssl.SSLSocket
    public void setUseClientMode(boolean z) {
    }

    public C113565Ib() {
        this.A0E = false;
        this.A0D = false;
        this.A0F = false;
        this.A0C = C12970iu.A12();
    }

    public C113565Ib(AbstractC113555Ia r2) {
        A02(r2, this);
        this.A0B = null;
        this.A00 = -1;
        A07();
    }

    public C113565Ib(AbstractC113555Ia r1, String str, int i) {
        super(str, i);
        A02(r1, this);
        this.A0B = str;
        this.A00 = i;
        A07();
    }

    public C113565Ib(AbstractC113555Ia r1, String str, InetAddress inetAddress, int i, int i2) {
        super(str, i, inetAddress, i2);
        A02(r1, this);
        this.A0B = str;
        this.A00 = i;
        A07();
    }

    public C113565Ib(AbstractC113555Ia r2, InetAddress inetAddress, int i) {
        super(inetAddress, i);
        A02(r2, this);
        this.A0B = null;
        this.A00 = i;
        A07();
    }

    public C113565Ib(AbstractC113555Ia r2, InetAddress inetAddress, InetAddress inetAddress2, int i, int i2) {
        super(inetAddress, i, inetAddress2, i2);
        A02(r2, this);
        this.A0B = inetAddress.getHostName();
        this.A00 = i;
        A07();
    }

    public static void A01(C94354bf r4, ByteBuffer byteBuffer, short s) {
        AnonymousClass4VO r3 = new AnonymousClass4VO(byteBuffer.array(), s);
        ArrayList arrayList = r4.A02;
        int i = r4.A00;
        arrayList.add(i, r3);
        r4.A01 += r3.A01.length + 4;
        r4.A00 = i + 1;
    }

    public static void A02(AbstractC113555Ia r1, C113565Ib r2) {
        r2.A0E = false;
        r2.A0D = false;
        r2.A0F = false;
        r2.A0C = new HashSet();
        r2.A05 = r1;
    }

    public static void A03(C113565Ib r5, byte[] bArr, byte b) {
        byte[] A01 = AnonymousClass4Yk.A01(bArr, b);
        r5.A04.A0B.A01(A01, 0, A01.length, (byte) 22);
        r5.A04.A0D.A00(A01);
    }

    public static byte[] A04(AnonymousClass584 r11) {
        ByteBuffer allocate;
        long currentTimeMillis;
        short s;
        byte[] bArr;
        if (r11 != null) {
            byte[] bArr2 = r11.A0j;
            if (bArr2 == null || bArr2.length != 32) {
                throw AnonymousClass1NR.A00("Client random is not correctly initialized.", (byte) 80);
            } else if (r11.A0l != null) {
                C94354bf r4 = new C94354bf();
                try {
                    String str = r11.A0O;
                    if (str != null && !str.isEmpty()) {
                        byte[] bytes = str.getBytes(DefaultCrypto.UTF_8);
                        int length = bytes.length;
                        ByteBuffer allocate2 = ByteBuffer.allocate(length + 3);
                        C72453ed.A1Q(allocate2, length + 1);
                        allocate2.put((byte) length);
                        allocate2.put(bytes);
                        A01(r4, allocate2, 16);
                    }
                    ByteBuffer allocate3 = ByteBuffer.allocate(4);
                    allocate3.putShort(2);
                    allocate3.putShort(1027);
                    A01(r4, allocate3, 13);
                    ByteBuffer allocate4 = ByteBuffer.allocate(4);
                    allocate4.putShort(2);
                    allocate4.putShort(29);
                    A01(r4, allocate4, 10);
                    ByteBuffer allocate5 = ByteBuffer.allocate(2);
                    allocate5.put((byte) 1);
                    allocate5.put(r11.A00);
                    A01(r4, allocate5, 45);
                    ByteBuffer allocate6 = ByteBuffer.allocate(5);
                    allocate6.put((byte) 4);
                    allocate6.putShort(772);
                    allocate6.putShort(-1254);
                    A01(r4, allocate6, 43);
                    ByteBuffer allocate7 = ByteBuffer.allocate(4);
                    allocate7.putShort(2);
                    allocate7.putShort(1027);
                    A01(r4, allocate7, 50);
                    if (r11.A0a && r11.A0C.A03 != null && !r11.A0c) {
                        AnonymousClass4VO r3 = new AnonymousClass4VO(new byte[0], 42);
                        ArrayList arrayList = r4.A02;
                        int i = r4.A00;
                        arrayList.add(i, r3);
                        r4.A01 += r3.A01.length + 4;
                        r4.A00 = i + 1;
                    }
                    try {
                        byte[] bytes2 = r11.A0Q.getBytes(DefaultCrypto.UTF_8);
                        int length2 = bytes2.length;
                        ByteBuffer allocate8 = ByteBuffer.allocate(length2 + 5);
                        C72453ed.A1Q(allocate8, length2 + 3);
                        allocate8.put(AnonymousClass3JS.A05(length2));
                        allocate8.put(bytes2);
                        A01(r4, allocate8, 0);
                        if (r11.A0c && (bArr = r11.A0k) != null) {
                            ByteBuffer allocate9 = ByteBuffer.allocate(bArr.length + 2);
                            C72453ed.A1Q(allocate9, bArr.length);
                            allocate9.put(bArr);
                            A01(r4, allocate9, 44);
                        }
                        if (!r11.A0c || (s = r11.A0W) == 29) {
                            ByteBuffer allocate10 = ByteBuffer.allocate(38);
                            C72453ed.A1Q(allocate10, 36);
                            allocate10.putShort(29);
                            C72453ed.A1Q(allocate10, 32);
                            allocate10.put(r11.A0i);
                            A01(r4, allocate10, 51);
                            ByteBuffer allocate11 = ByteBuffer.allocate(r4.A01);
                            Iterator it = r4.A02.iterator();
                            while (it.hasNext()) {
                                AnonymousClass4VO r42 = (AnonymousClass4VO) it.next();
                                byte[] bArr3 = r42.A01;
                                int length3 = bArr3.length;
                                ByteBuffer allocate12 = ByteBuffer.allocate(length3 + 4);
                                allocate12.putShort(r42.A00);
                                C72453ed.A1Q(allocate12, length3);
                                allocate12.put(bArr3);
                                allocate11.put(allocate12.array());
                            }
                            byte[] array = allocate11.array();
                            WtCachedPsk wtCachedPsk = r11.A0C.A03;
                            if (wtCachedPsk == null) {
                                allocate = ByteBuffer.allocate(0);
                            } else {
                                allocate = ByteBuffer.allocate(wtCachedPsk.ticket.length + 6 + 6 + r11.A02 + 1 + 2);
                                byte[] bArr4 = r11.A0C.A03.ticket;
                                allocate.putShort(41);
                                C72453ed.A1Q(allocate, allocate.capacity() - 4);
                                int length4 = bArr4.length;
                                C72453ed.A1Q(allocate, length4 + 6);
                                C72453ed.A1Q(allocate, length4);
                                allocate.put(bArr4);
                                WtCachedPsk wtCachedPsk2 = r11.A0C.A03;
                                if (wtCachedPsk2.useTestTime) {
                                    currentTimeMillis = 3600000;
                                } else {
                                    currentTimeMillis = System.currentTimeMillis();
                                }
                                long j = currentTimeMillis - wtCachedPsk2.ticketIssuedTime;
                                if (j < 0) {
                                    j = 0;
                                }
                                long j2 = (j + wtCachedPsk2.ticketAgeAdd) % 4294967296L;
                                if (j2 < 0) {
                                    j2 += 4294967296L;
                                }
                                if (j2 < 0 || j2 >= 4294967296L) {
                                    StringBuilder A0k = C12960it.A0k("Invalid argument. The supplied long value = ");
                                    A0k.append(j2);
                                    throw AnonymousClass1NR.A00(C12960it.A0d(" does not  fit in 4 bytes.", A0k), (byte) 80);
                                }
                                allocate.put(new byte[]{(byte) ((int) ((j2 >> 24) & 255)), (byte) ((int) ((j2 >> 16) & 255)), (byte) ((int) ((j2 >> 8) & 255)), (byte) ((int) (j2 & 255))});
                            }
                            int length5 = array.length + allocate.capacity();
                            ByteBuffer allocate13 = ByteBuffer.allocate(r11.A0l.length + 35 + 2 + 2 + 1 + 1 + 2 + length5);
                            allocate13.putShort(771);
                            allocate13.put(r11.A0j);
                            allocate13.put((byte) r11.A0l.length);
                            allocate13.put(r11.A0l);
                            allocate13.putShort(2);
                            allocate13.putShort(4865);
                            allocate13.put((byte) 1);
                            allocate13.put((byte) 0);
                            C72453ed.A1Q(allocate13, length5);
                            allocate13.put(array);
                            if (r11.A0C.A03 != null) {
                                try {
                                    MessageDigest messageDigest = (MessageDigest) r11.A0D.A00.clone();
                                    byte[] copyOfRange = Arrays.copyOfRange(allocate13.array(), 0, allocate13.position());
                                    byte[] copyOfRange2 = Arrays.copyOfRange(allocate.array(), 0, allocate.position());
                                    messageDigest.update((byte) 1);
                                    messageDigest.update(AnonymousClass3JS.A05(allocate13.capacity()));
                                    messageDigest.update(copyOfRange);
                                    messageDigest.update(copyOfRange2);
                                    byte[] digest = messageDigest.digest();
                                    int i2 = r11.A02 + 1;
                                    ByteBuffer allocate14 = ByteBuffer.allocate(i2 + 2);
                                    C72453ed.A1Q(allocate14, i2);
                                    try {
                                        byte[] A09 = AnonymousClass3JS.A09(r11.A0P, AnonymousClass584.A00(r11.A09, r11, "finished", new byte[0], AnonymousClass584.A00(r11.A09, r11, "res binder", MessageDigest.getInstance(r11.A0P).digest(), r11.A09.A00(new byte[r11.A02], r11.A0C.A03.pskVal))), digest);
                                        allocate14.put((byte) A09.length);
                                        allocate14.put(A09);
                                        allocate.put(allocate14.array());
                                        allocate13.put(allocate.array());
                                    } catch (NoSuchAlgorithmException e) {
                                        throw C72453ed.A0f(e);
                                    }
                                } catch (CloneNotSupportedException e2) {
                                    throw C72453ed.A0f(e2);
                                }
                            }
                            return allocate13.array();
                        }
                        StringBuilder A0k2 = C12960it.A0k("Must use key group sent by HelloRetryRequest: ");
                        A0k2.append((int) s);
                        throw AnonymousClass1NR.A00(C12960it.A0e(" client key group: ", A0k2, 29), (byte) 80);
                    } catch (UnsupportedEncodingException e3) {
                        throw C72453ed.A0f(e3);
                    }
                } catch (UnsupportedEncodingException e4) {
                    throw C72453ed.A0f(e4);
                }
            } else {
                throw AnonymousClass1NR.A00("Legacy session id is not correctly initialized.", (byte) 80);
            }
        } else {
            throw AnonymousClass1NR.A00("Illegal argument. Context cannot be null.", (byte) 80);
        }
    }

    public final String A05() {
        StringBuilder A0k = C12960it.A0k("host=");
        AnonymousClass584 r2 = this.A04;
        A0k.append(r2.A0Q);
        A0k.append(" hrr=");
        A0k.append(r2.A0c);
        A0k.append(" r=");
        A0k.append(r2.A0g);
        A0k.append(" ed=");
        A0k.append(r2.A0Z);
        A0k.append(" eda=");
        A0k.append(r2.A0f);
        A0k.append(" s=");
        return C12960it.A0d(this.A08.A00.A00.A03, A0k);
    }

    public void A06() {
        C89634Ks A01;
        while (true) {
            boolean equals = this.A08.A00.A00.equals(AnonymousClass4Hp.A08);
            AnonymousClass584 r0 = this.A04;
            if (!equals) {
                AbstractC92904Xx r2 = r0.A0A;
                synchronized (r2) {
                    A01 = r2.A01();
                }
                if (!(A01 instanceof AnonymousClass466)) {
                    if (A01 instanceof C861245u) {
                        A0B(A01);
                        throw C12990iw.A0m("Redex: Unreachable code after no-return invoke");
                    }
                    this.A08.A00(A01);
                    if (A01 instanceof AnonymousClass464) {
                        A03(this, A04(this.A04), (byte) 1);
                    }
                }
            } else {
                if (!r0.A0b) {
                    A0C(new SSLException("Server must either choose a PSK or send certificates."), (byte) 2, (byte) 116, false);
                }
                if (this.A04.A0f) {
                    A03(this, new byte[0], (byte) 5);
                }
                AnonymousClass584 r1 = this.A04;
                if (r1.A0d && !r1.A0e) {
                    r1.A0B.A01(new byte[]{1}, 0, 1, (byte) 20);
                }
                AnonymousClass584 r12 = this.A04;
                AbstractC116735Wp A00 = r12.A0E.A00();
                A00.AId(C72463ee.A0a("client_hs_key", r12.A0U), C72463ee.A0a("client_hs_iv", this.A04.A0U));
                AnonymousClass584 r22 = this.A04;
                r22.A0B = new AnonymousClass46H(A00, r22.A0N);
                if (r22.A0X) {
                    A03(this, new byte[4], (byte) 11);
                }
                AnonymousClass584 r6 = this.A04;
                if (r6 != null) {
                    byte[] A012 = AnonymousClass4Yk.A01(AnonymousClass3JS.A09(r6.A0P, C72463ee.A0a("client_finished", r6.A0U), r6.A0D.A01()), (byte) 20);
                    this.A04.A0B.A01(A012, 0, A012.length, (byte) 22);
                    this.A08.A00(new C861645y(A012));
                    long currentTimeMillis = System.currentTimeMillis();
                    this.A0D = true;
                    AnonymousClass4AG r4 = AnonymousClass4AG.INFO;
                    StringBuilder A0k = C12960it.A0k("Handshake complete : session_resumed ");
                    AnonymousClass584 r13 = this.A04;
                    A0k.append(r13.A0g);
                    A0k.append(" early_data_sent ");
                    A0k.append(r13.A0Z);
                    A0k.append(" early_data_accepted ");
                    A0k.append(r13.A0f);
                    A0k.append(" client_cert_requested ");
                    A0k.append(r13.A0X);
                    A0k.append(" time_ms ");
                    AnonymousClass4ZF.A00(r4, C12970iu.A0w(A0k, currentTimeMillis - this.A01));
                    HandshakeCompletedEvent handshakeCompletedEvent = new HandshakeCompletedEvent(this, this.A07);
                    for (HandshakeCompletedListener handshakeCompletedListener : this.A0C) {
                        handshakeCompletedListener.handshakeCompleted(handshakeCompletedEvent);
                    }
                    return;
                }
                throw AnonymousClass1NR.A00("Illegal argument. Context cannot be null.", (byte) 80);
            }
        }
    }

    public void A07() {
        AnonymousClass4ZF.A00 = new AnonymousClass45Y((AnonymousClass46D) this.A05);
        A08();
        this.A02 = new AnonymousClass49E(this);
        this.A03 = new AnonymousClass49I(this);
        AnonymousClass584 r1 = new AnonymousClass584();
        this.A04 = r1;
        try {
            this.A08 = new AnonymousClass4V3(r1);
        } catch (AnonymousClass1NR e) {
            throw new IOException(e);
        }
    }

    public void A08() {
        this.A09 = super.getInputStream();
        this.A0A = super.getOutputStream();
    }

    public void A09() {
        super.close();
        this.A09.close();
        this.A0A.close();
    }

    public final synchronized void A0A() {
        this.A0F = true;
        AnonymousClass584 r1 = this.A04;
        r1.A0R = null;
        r1.A0S = null;
        if (this.A0E) {
            this.A02.close();
            this.A03.close();
        }
        A09();
    }

    public final void A0B(C89634Ks r7) {
        byte[] bArr = (byte[]) r7.A00;
        AnonymousClass4AG r4 = AnonymousClass4AG.DEBUG;
        StringBuilder A0k = C12960it.A0k("Received Alert: Level ");
        A0k.append((int) bArr[0]);
        A0k.append(" Description ");
        byte b = bArr[1];
        A0k.append(A00(b));
        A0k.append("(");
        A0k.append((int) b);
        AnonymousClass4ZF.A00(r4, C12960it.A0d(")", A0k));
        A0A();
        byte b2 = bArr[1];
        if (b2 == 0 || b2 == 50) {
            throw new IOException(new SSLException(C12960it.A0e("Received alert ", C12960it.A0h(), b2)));
        }
        throw new IOException(C12960it.A0d(A05(), C12960it.A0k("WATLS Exception\n")), new SSLException(C12960it.A0e("Received alert ", C12960it.A0h(), b2)));
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r8v2, types: [java.lang.Throwable] */
    public final synchronized void A0C(SSLException sSLException, byte b, byte b2, boolean z) {
        String str;
        if (z) {
            throw ((IOException) C72453ed.A0w(sSLException));
        }
        if (!this.A0F) {
            AnonymousClass4AG r6 = AnonymousClass4AG.DEBUG;
            StringBuilder A0h = C12960it.A0h();
            A0h.append("Sending Alert : type : ");
            A0h.append(b == 2 ? "FATAL" : "WARNING");
            A0h.append(" description : ");
            A0h.append(A00(b2));
            A0h.append("(");
            A0h.append((int) b2);
            A0h.append(") exception : ");
            if (sSLException == null) {
                str = "";
            } else {
                str = sSLException.toString();
            }
            String A0d = C12960it.A0d(str, A0h);
            StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
            AnonymousClass4WJ r1 = AnonymousClass4ZF.A00;
            String obj = stackTrace[2].toString();
            if (!(r1 instanceof AnonymousClass45Y)) {
                PrintStream printStream = System.err;
                StringBuilder A0h2 = C12960it.A0h();
                A0h2.append(r6);
                A0h2.append(": ");
                A0h2.append(obj);
                A0h2.append(" : ");
                printStream.println(C12960it.A0d(A0d, A0h2));
                if (sSLException != null) {
                    sSLException.printStackTrace(printStream);
                }
            }
            try {
                this.A04.A0B.A01(new byte[]{b, b2}, 0, 2, (byte) 21);
            } catch (Exception e) {
                AnonymousClass4ZF.A00(AnonymousClass4AG.ERROR, C12960it.A0Z(e, "Encountered exception. Nothing much can be done here. ", C12960it.A0h()));
            }
            A0A();
        }
        if (b == 2) {
            StringBuilder A0h3 = C12960it.A0h();
            A0h3.append("WATLS Exception\n");
            String A0d2 = C12960it.A0d(A05(), A0h3);
            SSLException sSLException2 = sSLException;
            if (sSLException != null) {
                sSLException2 = C72453ed.A0w(sSLException);
            }
            throw new IOException(A0d2, sSLException2);
        }
    }

    @Override // javax.net.ssl.SSLSocket
    public void addHandshakeCompletedListener(HandshakeCompletedListener handshakeCompletedListener) {
        this.A0C.add(handshakeCompletedListener);
    }

    @Override // java.net.Socket, java.io.Closeable, java.lang.AutoCloseable
    public synchronized void close() {
        if (!this.A0F) {
            if (this.A0E) {
                A0C(null, (byte) 1, (byte) 0, false);
            } else {
                A0A();
            }
        }
    }

    @Override // java.net.Socket
    public SocketChannel getChannel() {
        throw new AssertionError("Channels are not supported by WtSocket.");
    }

    @Override // javax.net.ssl.SSLSocket
    public String[] getEnabledCipherSuites() {
        return new String[]{"TLS_AES_128_GCM_SHA256", "use default"};
    }

    @Override // javax.net.ssl.SSLSocket
    public SSLSession getHandshakeSession() {
        return this.A06;
    }

    @Override // java.net.Socket
    public InputStream getInputStream() {
        AnonymousClass49E r0 = this.A02;
        if (r0 != null) {
            return r0;
        }
        throw C12990iw.A0i("Input stream is closed.");
    }

    @Override // javax.net.ssl.SSLSocket
    public boolean getNeedClientAuth() {
        return this.A05.getNeedClientAuth();
    }

    @Override // java.net.Socket
    public OutputStream getOutputStream() {
        AnonymousClass49I r0 = this.A03;
        if (r0 != null) {
            return r0;
        }
        throw C12990iw.A0i("Output stream is closed.");
    }

    @Override // javax.net.ssl.SSLSocket
    public SSLParameters getSSLParameters() {
        return this.A05;
    }

    @Override // javax.net.ssl.SSLSocket
    public SSLSession getSession() {
        return this.A07;
    }

    @Override // javax.net.ssl.SSLSocket
    public String[] getSupportedCipherSuites() {
        return new String[]{"TLS_AES_128_GCM_SHA256", "use default"};
    }

    @Override // javax.net.ssl.SSLSocket
    public boolean getWantClientAuth() {
        return this.A05.getWantClientAuth();
    }

    @Override // java.net.Socket
    public boolean isClosed() {
        return this.A0F;
    }

    @Override // javax.net.ssl.SSLSocket
    public void removeHandshakeCompletedListener(HandshakeCompletedListener handshakeCompletedListener) {
        this.A0C.remove(handshakeCompletedListener);
    }

    @Override // javax.net.ssl.SSLSocket
    public void setEnabledCipherSuites(String[] strArr) {
        this.A05.setCipherSuites(strArr);
    }

    @Override // javax.net.ssl.SSLSocket
    public void setEnabledProtocols(String[] strArr) {
        this.A05.setProtocols(strArr);
    }

    @Override // javax.net.ssl.SSLSocket
    public void setNeedClientAuth(boolean z) {
        this.A05.setNeedClientAuth(z);
    }

    @Override // javax.net.ssl.SSLSocket
    public void setSSLParameters(SSLParameters sSLParameters) {
        if (sSLParameters instanceof AbstractC113555Ia) {
            this.A05 = (AbstractC113555Ia) sSLParameters;
        }
    }

    @Override // javax.net.ssl.SSLSocket
    public void setWantClientAuth(boolean z) {
        this.A05.setWantClientAuth(z);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x00bb, code lost:
        if (r7 == null) goto L_0x00c8;
     */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x010b A[Catch: IOException -> 0x01f0, 1NR -> 0x01e5, Exception -> 0x01d1, TryCatch #3 {1NR -> 0x01e5, IOException -> 0x01f0, Exception -> 0x01d1, blocks: (B:3:0x0002, B:5:0x000d, B:7:0x008d, B:9:0x0095, B:11:0x009b, B:12:0x00aa, B:14:0x00bd, B:16:0x00c3, B:17:0x00c8, B:18:0x00ca, B:19:0x00ce, B:21:0x010b, B:22:0x0118, B:24:0x013f, B:25:0x0147, B:27:0x0153, B:28:0x015b, B:30:0x019d, B:32:0x01a3, B:34:0x01a7, B:35:0x01b6, B:37:0x01c3), top: B:49:0x0002 }] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x013f A[Catch: IOException -> 0x01f0, 1NR -> 0x01e5, Exception -> 0x01d1, TryCatch #3 {1NR -> 0x01e5, IOException -> 0x01f0, Exception -> 0x01d1, blocks: (B:3:0x0002, B:5:0x000d, B:7:0x008d, B:9:0x0095, B:11:0x009b, B:12:0x00aa, B:14:0x00bd, B:16:0x00c3, B:17:0x00c8, B:18:0x00ca, B:19:0x00ce, B:21:0x010b, B:22:0x0118, B:24:0x013f, B:25:0x0147, B:27:0x0153, B:28:0x015b, B:30:0x019d, B:32:0x01a3, B:34:0x01a7, B:35:0x01b6, B:37:0x01c3), top: B:49:0x0002 }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0153 A[Catch: IOException -> 0x01f0, 1NR -> 0x01e5, Exception -> 0x01d1, TryCatch #3 {1NR -> 0x01e5, IOException -> 0x01f0, Exception -> 0x01d1, blocks: (B:3:0x0002, B:5:0x000d, B:7:0x008d, B:9:0x0095, B:11:0x009b, B:12:0x00aa, B:14:0x00bd, B:16:0x00c3, B:17:0x00c8, B:18:0x00ca, B:19:0x00ce, B:21:0x010b, B:22:0x0118, B:24:0x013f, B:25:0x0147, B:27:0x0153, B:28:0x015b, B:30:0x019d, B:32:0x01a3, B:34:0x01a7, B:35:0x01b6, B:37:0x01c3), top: B:49:0x0002 }] */
    @Override // javax.net.ssl.SSLSocket
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void startHandshake() {
        /*
        // Method dump skipped, instructions count: 498
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C113565Ib.startHandshake():void");
    }
}
