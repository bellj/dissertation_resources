package X;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import com.facebook.redex.RunnableBRunnable0Shape7S0200000_I0_7;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import javax.crypto.NoSuchPaddingException;

/* renamed from: X.0o1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15860o1 implements AbstractC15870o2, AbstractC15840nz {
    public C16580pH A00;
    public C33161dY A01;
    public final AbstractC15710nm A02;
    public final C14900mE A03;
    public final C15570nT A04;
    public final C15450nH A05;
    public final C15820nx A06;
    public final C22330yu A07;
    public final C243915i A08;
    public final C15550nR A09;
    public final AnonymousClass10S A0A;
    public final C15610nY A0B;
    public final C15810nw A0C;
    public final C17050qB A0D;
    public final AnonymousClass01d A0E;
    public final C14830m7 A0F;
    public final C16590pI A0G;
    public final C15890o4 A0H;
    public final C14820m6 A0I;
    public final C19490uC A0J;
    public final C19990v2 A0K;
    public final C20830wO A0L;
    public final C21320xE A0M;
    public final C231410n A0N;
    public final C14850m9 A0O;
    public final C21780xy A0P;
    public final C244215l A0Q;
    public final AnonymousClass15E A0R;
    public final C244015j A0S;
    public final C16600pJ A0T;
    public final AnonymousClass15D A0U;
    public final C244115k A0V;
    public final String A0W;
    public final Map A0X = new ConcurrentHashMap();
    public final boolean A0Y;

    @Override // X.AbstractC15840nz
    public String AAp() {
        return "chat-settings";
    }

    public C15860o1(AbstractC15710nm r2, C14900mE r3, C15570nT r4, C15450nH r5, C15820nx r6, C22330yu r7, C243915i r8, C15550nR r9, AnonymousClass10S r10, C15610nY r11, C15810nw r12, C17050qB r13, AnonymousClass01d r14, C14830m7 r15, C16590pI r16, C15890o4 r17, C14820m6 r18, C19490uC r19, C19990v2 r20, C20830wO r21, C21320xE r22, C231410n r23, C14850m9 r24, C21780xy r25, C244215l r26, AnonymousClass15E r27, C244015j r28, C16600pJ r29, AnonymousClass15D r30, C244115k r31, String str, boolean z) {
        this.A0G = r16;
        this.A0F = r15;
        this.A0O = r24;
        this.A03 = r3;
        this.A02 = r2;
        this.A0U = r30;
        this.A04 = r4;
        this.A0K = r20;
        this.A0C = r12;
        this.A05 = r5;
        this.A0N = r23;
        this.A09 = r9;
        this.A0E = r14;
        this.A0B = r11;
        this.A0J = r19;
        this.A08 = r8;
        this.A0A = r10;
        this.A06 = r6;
        this.A0D = r13;
        this.A07 = r7;
        this.A0S = r28;
        this.A0H = r17;
        this.A0I = r18;
        this.A0M = r22;
        this.A0V = r31;
        this.A0T = r29;
        this.A0R = r27;
        this.A0L = r21;
        this.A0Q = r26;
        this.A0P = r25;
        this.A0W = str;
        this.A0Y = z;
        A0H();
    }

    public static C33181da A00(Jid jid, C15860o1 r1) {
        return r1.A08(jid.getRawString());
    }

    public static final boolean A01(C33181da r6) {
        String str = r6.A0C;
        if ("group_chat_defaults".equals(str) || "individual_chat_defaults".equals(str) || r6.A02 != 0 || r6.A0I || r6.A0H) {
            return false;
        }
        C33181da A02 = r6.A02();
        if (!TextUtils.equals(r6.A07(), A02.A07()) || !TextUtils.equals(r6.A08(), A02.A08()) || !TextUtils.equals(r6.A06(), A02.A06()) || !TextUtils.equals(r6.A05(), A02.A05()) || !TextUtils.equals(r6.A03(), A02.A03()) || !TextUtils.equals(r6.A04(), A02.A04()) || r6.A0B() != A02.A0B() || r6.A00 != 0 || r6.A02().A0E != A02.A02().A0E || r6.A05 != null || r6.A04 != null || r6.A0F) {
            return false;
        }
        return true;
    }

    public long A02(AbstractC14640lm r3) {
        C33181da A08 = A08(r3.getRawString());
        if (A08.A0F) {
            return A08.A03;
        }
        return 0;
    }

    public final synchronized C16580pH A03() {
        if (this.A00 == null) {
            C16580pH r2 = new C16580pH(this.A02, this.A0G, this.A0N, this.A0O, this.A0W);
            this.A00 = r2;
            if (this.A0Y) {
                C33161dY r1 = this.A01;
                AnonymousClass1I2 r0 = r2.A02;
                AnonymousClass009.A05(r1);
                r0.A01.add(r1);
            }
        }
        return this.A00;
    }

    public C33181da A04() {
        C33181da A08 = A08("group_chat_defaults");
        if (A08.A0A == null) {
            A08.A0A = Settings.System.DEFAULT_NOTIFICATION_URI.toString();
        }
        if (TextUtils.isEmpty(A08.A0B)) {
            A08.A0B = "1";
        }
        if (TextUtils.isEmpty(A08.A09)) {
            A08.A09 = Integer.toString(0);
        }
        if (TextUtils.isEmpty(A08.A08)) {
            A08.A08 = "FFFFFF";
        }
        return A08;
    }

    public C33181da A05() {
        C33181da A08 = A08("individual_chat_defaults");
        if (A08.A0A == null) {
            A08.A0A = Settings.System.DEFAULT_NOTIFICATION_URI.toString();
        }
        if (TextUtils.isEmpty(A08.A0B)) {
            A08.A0B = "1";
        }
        if (TextUtils.isEmpty(A08.A09)) {
            A08.A09 = Integer.toString(0);
        }
        if (TextUtils.isEmpty(A08.A08)) {
            A08.A08 = "FFFFFF";
        }
        if (A08.A06 == null) {
            A08.A06 = Settings.System.DEFAULT_RINGTONE_URI.toString();
        }
        if (TextUtils.isEmpty(A08.A07)) {
            A08.A07 = "1";
        }
        return A08;
    }

    public final C33181da A06() {
        int i = Build.VERSION.SDK_INT;
        C14830m7 r4 = this.A0F;
        C15450nH r2 = this.A05;
        AnonymousClass01d r3 = this.A0E;
        C15890o4 r5 = this.A0H;
        if (i >= 26) {
            return new C33271dj(r2, r3, r4, r5, this, this.A01);
        }
        return new C33181da(r2, r3, r4, r5, this);
    }

    public final C33181da A07(Cursor cursor) {
        C33181da A06 = A06();
        A06.A0C = cursor.getString(0);
        boolean z = true;
        A06.A02 = cursor.getLong(1);
        boolean z2 = false;
        if (cursor.getInt(2) == 1) {
            z2 = true;
        }
        A06.A0G = z2;
        boolean z3 = false;
        if (cursor.getInt(3) == 1) {
            z3 = true;
        }
        A06.A0I = z3;
        A06.A0A = cursor.getString(4);
        A06.A0B = cursor.getString(5);
        A06.A09 = cursor.getString(6);
        A06.A08 = cursor.getString(7);
        A06.A06 = cursor.getString(8);
        A06.A07 = cursor.getString(9);
        boolean z4 = false;
        if (cursor.getInt(10) == 1) {
            z4 = true;
        }
        A06.A0H = z4;
        boolean z5 = false;
        if (cursor.getInt(11) == 1) {
            z5 = true;
        }
        A06.A0F = z5;
        A06.A03 = cursor.getLong(12);
        boolean z6 = false;
        if (cursor.getInt(13) == 1) {
            z6 = true;
        }
        A06.A0D = z6;
        A06.A00 = cursor.getInt(14);
        if (cursor.getInt(cursor.getColumnIndexOrThrow("mute_reactions")) != 1) {
            z = false;
        }
        A06.A0E = z;
        if ("0".equals(A06.A08)) {
            A06.A08 = "000000";
        }
        String string = cursor.getString(cursor.getColumnIndexOrThrow("wallpaper_light_type"));
        if (string != null) {
            A06.A05 = new C33191db(0, string, cursor.getString(cursor.getColumnIndexOrThrow("wallpaper_light_value")));
        }
        String string2 = cursor.getString(cursor.getColumnIndexOrThrow("wallpaper_dark_type"));
        if (string2 != null) {
            A06.A04 = new C33191db(Integer.valueOf(cursor.getInt(cursor.getColumnIndexOrThrow("wallpaper_dark_opacity"))), string2, cursor.getString(cursor.getColumnIndexOrThrow("wallpaper_dark_value")));
        }
        return A06;
    }

    public final C33181da A08(String str) {
        Map map = this.A0X;
        C33181da r0 = (C33181da) map.get(str);
        if (r0 == null) {
            try {
                C16310on A01 = A03().get();
                try {
                    Cursor A08 = A01.A03.A08("settings", "jid = ?", null, null, C33201dc.A00, new String[]{str});
                    if (A08 != null) {
                        if (A08.moveToNext()) {
                            r0 = A07(A08);
                            A08.close();
                            A01.close();
                            r0.A0C = str;
                            map.put(str, r0);
                        } else {
                            A08.close();
                        }
                    }
                    A01.close();
                    r0 = A06();
                    r0.A0C = str;
                    map.put(str, r0);
                } catch (Throwable th) {
                    try {
                        A01.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            } catch (SQLiteDatabaseCorruptException e) {
                Log.e("chat-settings-store/get", e);
                A0F();
                throw e;
            }
        }
        return r0;
    }

    public Long A09(AbstractC14640lm r5, long j) {
        boolean z = false;
        if (j > 0) {
            z = true;
        }
        AnonymousClass009.A0A("Pinned time should be strictly positive", z);
        return A0A(r5, j, true);
    }

    public final Long A0A(AbstractC14640lm r15, long j, boolean z) {
        long j2 = j;
        if (z) {
            this.A08.A00(r15, 7);
        }
        C33181da A08 = A08(r15.getRawString());
        boolean z2 = A08.A0F;
        long j3 = A08.A03;
        try {
            C16310on A02 = A03().A02();
            A08.A0F = z;
            if (!z) {
                j2 = 0;
            }
            A08.A03 = j2;
            ContentValues contentValues = new ContentValues(2);
            contentValues.put("pinned", Boolean.valueOf(A08.A0F));
            contentValues.put("pinned_time", Long.valueOf(A08.A03));
            C16330op r12 = A02.A03;
            boolean z3 = true;
            if (r12.A00("settings", contentValues, "jid =?", new String[]{r15.getRawString()}) == 0) {
                contentValues.put("jid", r15.getRawString());
                r12.A02(contentValues, "settings");
            }
            if (z2 == z && j3 == A08.A03) {
                z3 = false;
            }
            A02.close();
            this.A0M.A05();
            if (z3) {
                return Long.valueOf(j3);
            }
            return null;
        } catch (SQLiteDatabaseCorruptException e) {
            Log.i("chat-settings-store/set-pin", e);
            A0F();
            throw e;
        }
    }

    public List A0B() {
        ArrayList arrayList = new ArrayList();
        C16310on A01 = A03().get();
        try {
            Cursor A08 = A01.A03.A08("settings", "muted_notifications = ? AND (mute_end > ? OR mute_end = ? )", "jid ASC", null, new String[]{"jid", "mute_end"}, new String[]{String.valueOf(0), String.valueOf(System.currentTimeMillis()), String.valueOf(-1)});
            int columnIndex = A08.getColumnIndex("jid");
            int columnIndex2 = A08.getColumnIndex("mute_end");
            while (A08.moveToNext()) {
                AbstractC14640lm A012 = AbstractC14640lm.A01(A08.getString(columnIndex));
                if (A012 != null && (A012 instanceof GroupJid)) {
                    arrayList.add(new C33281dk(A012, Long.valueOf(A08.getLong(columnIndex2))));
                }
            }
            A08.close();
            A01.close();
            return arrayList;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public Map A0C() {
        LinkedHashMap linkedHashMap = new LinkedHashMap(3);
        try {
            C16310on A01 = A03().get();
            Cursor A08 = A01.A03.A08("settings", "pinned != 0", "pinned_time DESC", null, new String[]{"jid", "pinned_time"}, null);
            if (A08 != null) {
                try {
                    int columnIndex = A08.getColumnIndex("jid");
                    int columnIndex2 = A08.getColumnIndex("pinned_time");
                    while (A08.moveToNext()) {
                        AbstractC14640lm A012 = AbstractC14640lm.A01(A08.getString(columnIndex));
                        long j = A08.getLong(columnIndex2);
                        if (A012 != null) {
                            linkedHashMap.put(A012, Long.valueOf(j));
                        }
                    }
                } catch (Throwable th) {
                    try {
                        A08.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            }
            if (A08 != null) {
                A08.close();
            }
            A01.close();
            return linkedHashMap;
        } catch (SQLiteDatabaseCorruptException e) {
            Log.i("chat-settings-store/get-pinned-jids", e);
            A0F();
            throw e;
        }
    }

    public Set A0D() {
        return new LinkedHashSet(A0C().keySet());
    }

    public final Set A0E() {
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        try {
            C16310on A01 = A03().get();
            Cursor A08 = A01.A03.A08("settings", "use_custom_notifications != 0", null, null, new String[]{"jid"}, null);
            while (A08 != null && A08.moveToNext()) {
                AbstractC14640lm A012 = AbstractC14640lm.A01(A08.getString(0));
                if (A012 != null) {
                    linkedHashSet.add(A012);
                }
            }
            if (A08 != null) {
                A08.close();
            }
            A01.close();
            return linkedHashSet;
        } catch (SQLiteDatabaseCorruptException e) {
            Log.i("chat-settings-store/get-pinned-jids", e);
            A0F();
            throw e;
        }
    }

    public void A0F() {
        this.A0X.clear();
        A03().A04();
        A0I();
        A0H();
    }

    public void A0G() {
        String A01;
        if (C33261di.A00) {
            C16310on A02 = A03().A02();
            try {
                NotificationManager A08 = this.A0E.A08();
                AnonymousClass009.A05(A08);
                for (NotificationChannel notificationChannel : C33301dm.A01(A08)) {
                    if (!C27381He.A01.contains(notificationChannel.getId()) && !"miscellaneous".equals(notificationChannel.getId()) && this.A01.A0L(notificationChannel, A02) && (A01 = C33161dY.A0M.A01(notificationChannel.getId())) != null) {
                        this.A0X.remove(A01);
                    }
                }
                A02.close();
            } catch (Throwable th) {
                try {
                    A02.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
    }

    public final void A0H() {
        C14830m7 r6 = this.A0F;
        C14900mE r1 = this.A03;
        C16590pI r7 = this.A0G;
        C19990v2 r10 = this.A0K;
        C15450nH r2 = this.A05;
        AnonymousClass01d r5 = this.A0E;
        C15610nY r4 = this.A0B;
        this.A01 = new C33161dY(r1, r2, this.A0A, r4, r5, r6, r7, this.A0H, this.A0I, r10, this.A0L, this.A0M, this.A0Q, this.A0V);
    }

    public final synchronized void A0I() {
        C16580pH r0 = this.A00;
        if (r0 != null) {
            r0.close();
            if (this.A0Y) {
                C16580pH r02 = this.A00;
                C33161dY r1 = this.A01;
                AnonymousClass1I2 r03 = r02.A02;
                AnonymousClass009.A05(r1);
                r03.A01.remove(r1);
            }
            this.A00 = null;
        }
    }

    public void A0J(C18330sH r7, C14820m6 r8) {
        if (Build.VERSION.SDK_INT >= 30) {
            SharedPreferences sharedPreferences = r8.A00;
            if (sharedPreferences.getInt("notification_channel_upgrade_version", 0) != 1) {
                for (AbstractC14640lm r1 : A0E()) {
                    r7.A07(r1);
                    A0M(A08(r1.getRawString()));
                    C15370n3 A0A = this.A09.A0A(r1);
                    if (A0A != null) {
                        r7.A03(this.A0G.A00, A0A);
                    }
                }
                sharedPreferences.edit().putInt("notification_channel_upgrade_version", 1).apply();
            }
        }
    }

    public void A0K(AbstractC14640lm r10, boolean z) {
        C33181da A08 = A08(r10.getRawString());
        if (C33261di.A00 && A08.A0I) {
            this.A01.A0K(r10.getRawString());
        }
        C16310on A02 = A03().A02();
        if (z) {
            try {
                if (A08.A0I) {
                    A08.A01 = System.currentTimeMillis();
                    ContentValues contentValues = new ContentValues(1);
                    contentValues.put("deleted", Long.valueOf(A08.A01));
                    A02.A03.A00("settings", contentValues, "jid = ?", new String[]{r10.getRawString()});
                } else {
                    this.A0X.remove(r10.getRawString());
                    A02.A03.A01("settings", "jid = ?", new String[]{r10.getRawString()});
                }
            } catch (Throwable th) {
                try {
                    A02.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
        A02.close();
    }

    public void A0L(C33181da r3) {
        if (!r3.A0I) {
            r3 = r3.A02();
        }
        A0P(r3.A0C, Settings.System.DEFAULT_NOTIFICATION_URI.toString());
        Log.i("chat-settings-store/set-underlying-message-tone-to-default updated message tone to default");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x00ed, code lost:
        if (r12.A01() == 0) goto L_0x00ef;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0M(X.C33181da r12) {
        /*
        // Method dump skipped, instructions count: 458
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15860o1.A0M(X.1da):void");
    }

    public final void A0N(String str, String str2) {
        C33181da A08 = A08(str);
        if (!TextUtils.equals(str2, A08.A08)) {
            A08.A08 = str2;
            A0M(A08);
        }
    }

    public final void A0O(String str, String str2) {
        C33181da A08 = A08(str);
        if (!TextUtils.equals(str2, A08.A09)) {
            A08.A09 = str2;
            A0M(A08);
        }
    }

    public final void A0P(String str, String str2) {
        C33181da A08 = A08(str);
        if (!TextUtils.equals(str2, A08.A0A)) {
            A08.A0A = str2;
            A0M(A08);
        }
    }

    public final void A0Q(String str, String str2) {
        C33181da A08 = A08(str);
        if (!TextUtils.equals(str2, A08.A0B)) {
            A08.A0B = str2;
            A0M(A08);
        }
    }

    public boolean A0R(AbstractC14640lm r7) {
        boolean z;
        NotificationChannel A03;
        C33181da A08 = A08(r7.getRawString());
        if (!C33261di.A00 || !A08.A0I || (A03 = this.A01.A03(r7.getRawString())) == null || A03.getImportance() >= 3) {
            z = false;
        } else {
            Log.i("chat-settings-store//cancelMute unmuting channel");
            this.A01.A0F(A03, r7.getRawString(), C33161dY.A00(A08.A0B()));
            z = true;
        }
        boolean A0T = A0T(r7, 0, A08.A0G);
        StringBuilder sb = new StringBuilder("chat-settings-store//cancelMute for jid:");
        sb.append(r7);
        sb.append(" channelChanged:");
        sb.append(z);
        sb.append(" dbchanged:");
        sb.append(A0T);
        Log.i(sb.toString());
        if (z || A0T) {
            return true;
        }
        return false;
    }

    public boolean A0S(AbstractC14640lm r2) {
        return A08(r2.getRawString()).A09();
    }

    public boolean A0T(AbstractC14640lm r10, long j, boolean z) {
        C33181da A08 = A08(r10.getRawString());
        if (j == A08.A02 && z == A08.A0G) {
            return false;
        }
        try {
            C16310on A02 = A03().A02();
            A08.A02 = j;
            if (A01(A08)) {
                A02.A03.A01("settings", "jid = ?", new String[]{r10.getRawString()});
                this.A0X.remove(r10.getRawString());
            } else {
                A08.A0G = z;
                ContentValues contentValues = new ContentValues(2);
                contentValues.put("mute_end", Long.valueOf(j));
                contentValues.put("muted_notifications", Boolean.valueOf(z));
                C16330op r3 = A02.A03;
                if (r3.A00("settings", contentValues, "jid = ?", new String[]{r10.getRawString()}) == 0) {
                    contentValues.put("jid", r10.getRawString());
                    r3.A02(contentValues, "settings");
                }
            }
            A02.close();
            this.A03.A0H(new RunnableBRunnable0Shape7S0200000_I0_7(this, 44, r10));
            return true;
        } catch (SQLiteDatabaseCorruptException e) {
            Log.i("chat-settings-store/setmute", e);
            A0F();
            throw e;
        }
    }

    public final boolean A0U(String str) {
        if (!C33261di.A00) {
            return false;
        }
        C16310on A02 = A03().A02();
        try {
            C33291dl r3 = C33161dY.A0M;
            String A00 = r3.A00(str);
            if (A00 != null) {
                NotificationManager A08 = this.A0E.A08();
                AnonymousClass009.A05(A08);
                if (this.A01.A0L(C33301dm.A00(A08, A00), A02)) {
                    this.A0X.remove(r3.A01(A00));
                    A02.close();
                    return true;
                }
            }
            A02.close();
            return false;
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:82:0x000e */
    /* JADX DEBUG: Multi-variable search result rejected for r0v2, resolved type: X.0o1 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v1, types: [boolean] */
    /* JADX WARN: Type inference failed for: r0v22, types: [X.0o1] */
    @Override // X.AbstractC15840nz
    public boolean A6K() {
        EnumC16570pG r11;
        ReentrantReadWriteLock.WriteLock writeLock;
        C16310on A02;
        File A022;
        String obj;
        C15820nx r0 = this.A06;
        C15860o1 A04 = r0.A04();
        if (A04 != 0) {
            r11 = EnumC16570pG.A07;
        } else {
            r11 = EnumC16570pG.A06;
        }
        try {
            C16590pI r10 = this.A0G;
            C14830m7 r02 = this.A0F;
            C14850m9 r03 = this.A0O;
            C14900mE r04 = this.A03;
            AbstractC15710nm r05 = this.A02;
            AnonymousClass15D r06 = this.A0U;
            C15570nT r07 = this.A04;
            C19990v2 r08 = this.A0K;
            C15810nw r09 = this.A0C;
            C15450nH r010 = this.A05;
            C231410n r011 = this.A0N;
            C15550nR r012 = this.A09;
            AnonymousClass01d r013 = this.A0E;
            C15610nY r014 = this.A0B;
            C19490uC r015 = this.A0J;
            C243915i r15 = this.A08;
            AnonymousClass10S r14 = this.A0A;
            C17050qB r016 = this.A0D;
            C22330yu r13 = this.A07;
            C244015j r9 = this.A0S;
            C15890o4 r8 = this.A0H;
            C14820m6 r7 = this.A0I;
            C21320xE r6 = this.A0M;
            C244115k r5 = this.A0V;
            C16600pJ r017 = this.A0T;
            AnonymousClass15E r4 = this.A0R;
            C20830wO r3 = this.A0L;
            C244215l r2 = this.A0Q;
            C21780xy r018 = this.A0P;
            A04 = new C15860o1(r05, r04, r07, r010, r0, r13, r15, r012, r14, r014, r09, r016, r013, r02, r10, r8, r7, r015, r08, r3, r6, r011, r03, r018, r2, r4, r9, r017, r06, r5, "chatsettingsbackup.db", false);
            writeLock = A03().A04.writeLock();
            writeLock.lock();
            try {
                StringBuilder sb = new StringBuilder("chat-settings-store/copy ");
                C16580pH A03 = A03();
                sb.append(A03.A00.A00.getDatabasePath(A03.A03));
                sb.append(" -> ");
                C16580pH A032 = A04.A03();
                sb.append(A032.A00.A00.getDatabasePath(A032.A03));
                Log.i(sb.toString());
                try {
                    A02 = A04.A03().A02();
                } catch (SQLiteDatabaseCorruptException e) {
                    Log.e("chat-settings-store/copy", e);
                    Log.e("chat-settings-store/backup/failed-to-copy");
                    A04.A03().close();
                }
            } catch (IOException | InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException e2) {
                Log.e("chat-settings-store/backup failed", e2);
            }
            try {
                AnonymousClass1Lx A00 = A02.A00();
                A02 = A03().get();
                try {
                    Cursor A08 = A02.A03.A08("settings", null, null, null, C33201dc.A00, null);
                    if (A08 != null) {
                        A08.getCount();
                        while (A08.moveToNext()) {
                            C33181da A07 = A07(A08);
                            if (A07.A0C != null) {
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append("chat-settings-store/copy-chat-settings ");
                                sb2.append(A07);
                                Log.i(sb2.toString());
                                A04.A0M(A07);
                            } else {
                                StringBuilder sb3 = new StringBuilder();
                                sb3.append("chat-settings-store/backup/null-jid/skipped ");
                                sb3.append(A07);
                                Log.e(sb3.toString());
                            }
                        }
                    }
                    A00.A00();
                    if (A08 != null) {
                        A08.close();
                    }
                    A02.close();
                    A00.close();
                    A02.close();
                    Log.i("chat-settings-store/backup/close-backup-db");
                    A04.A03().close();
                    if (r016.A02()) {
                        Log.i("chat-settings-store/backup/skip no media or read-only media");
                    } else {
                        EnumC16570pG r72 = EnumC16570pG.A05;
                        if (r11 == r72) {
                            A022 = r09.A02();
                            obj = "chatsettingsbackup.db.crypt1";
                        } else {
                            A022 = r09.A02();
                            StringBuilder sb4 = new StringBuilder("chatsettingsbackup.db.crypt");
                            sb4.append(r11.version);
                            obj = sb4.toString();
                        }
                        File file = new File(A022, obj);
                        List A072 = C32781cj.A07(r72, EnumC16570pG.A00());
                        A072.add(".crypt1");
                        File file2 = new File(r09.A02(), "chatsettingsbackup.db");
                        ArrayList A06 = C32781cj.A06(file2, A072);
                        C32781cj.A0C(file2, A06);
                        Iterator it = A06.iterator();
                        while (it.hasNext()) {
                            File file3 = (File) it.next();
                            if (!file3.equals(file) && file3.exists()) {
                                file3.delete();
                            }
                        }
                        StringBuilder sb5 = new StringBuilder();
                        sb5.append("chat-settings-store/backup/to ");
                        sb5.append(file);
                        Log.i(sb5.toString());
                        AbstractC33251dh A002 = C33231df.A00(r07, new C33211dd(file), null, r0, r016, r015, r018, r017, r11, r06);
                        if (!A002.A04(r10.A00)) {
                            Log.w("chat-settings-store/backup/prepare for backup failed");
                        } else {
                            C16580pH A033 = A04.A03();
                            A002.A03(null, A033.A00.A00.getDatabasePath(A033.A03));
                            if (!A04.A03().A04()) {
                                Log.w("chat-settings-store/backup failed to delete backup db");
                            }
                            writeLock.unlock();
                            return true;
                        }
                    }
                    if (!A04.A03().A04()) {
                        Log.w("chat-settings-store/backup failed to delete backup db");
                    }
                    writeLock.unlock();
                    return false;
                } finally {
                }
            } finally {
            }
        } catch (Throwable th) {
            if (!A04.A03().A04()) {
                Log.w("chat-settings-store/backup failed to delete backup db");
            }
            writeLock.unlock();
            throw th;
        }
    }

    @Override // X.AbstractC15870o2
    public Set AAi() {
        String str;
        String str2;
        HashSet hashSet = new HashSet();
        C33181da A05 = A05();
        C33191db r1 = A05.A04;
        if (!(r1 == null || !"USER_PROVIDED".equalsIgnoreCase(r1.A01) || (str2 = r1.A02) == null)) {
            hashSet.add(str2);
        }
        C33191db r12 = A05.A05;
        if (!(r12 == null || !"USER_PROVIDED".equalsIgnoreCase(r12.A01) || (str = r12.A02) == null)) {
            hashSet.add(str);
        }
        try {
            C16310on A01 = A03().get();
            Cursor A08 = A01.A03.A08("settings", "wallpaper_light_type = ?", null, null, new String[]{"wallpaper_light_value"}, new String[]{"USER_PROVIDED"});
            if (A08 != null) {
                while (A08.moveToNext()) {
                    try {
                        hashSet.add(A08.getString(A08.getColumnIndexOrThrow("wallpaper_light_value")));
                    } catch (Throwable th) {
                        try {
                            A08.close();
                        } catch (Throwable unused) {
                        }
                        throw th;
                    }
                }
            }
            if (A08 != null) {
                A08.close();
            }
            A01.close();
            return hashSet;
        } catch (SQLiteDatabaseCorruptException e) {
            Log.e("chat-settings-store/get-wallpaper-files", e);
            throw e;
        }
    }

    @Override // X.AbstractC15870o2
    public C33191db AHh(AbstractC14640lm r2, boolean z) {
        C33181da A08;
        if (r2 == null) {
            A08 = A05();
        } else {
            A08 = A08(r2.getRawString());
        }
        if (z) {
            return A08.A04;
        }
        return A08.A05;
    }
}
