package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.4A5  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4A5 extends Enum {
    public static final /* synthetic */ AnonymousClass4A5[] A00;
    public static final AnonymousClass4A5 A01;
    public static final AnonymousClass4A5 A02;

    public static AnonymousClass4A5 valueOf(String str) {
        return (AnonymousClass4A5) Enum.valueOf(AnonymousClass4A5.class, str);
    }

    public static AnonymousClass4A5[] values() {
        return (AnonymousClass4A5[]) A00.clone();
    }

    static {
        AnonymousClass4A5 r3 = new AnonymousClass4A5("NONE", 0);
        A02 = r3;
        AnonymousClass4A5 r1 = new AnonymousClass4A5("CHECKED", 1);
        A01 = r1;
        AnonymousClass4A5[] r0 = new AnonymousClass4A5[2];
        C72453ed.A1J(r3, r1, r0);
        A00 = r0;
    }

    public AnonymousClass4A5(String str, int i) {
    }
}
