package X;

import com.whatsapp.dependencybridge.di.DependencyBridgeModule;

/* renamed from: X.1t0  reason: invalid class name */
/* loaded from: classes2.dex */
public final /* synthetic */ class AnonymousClass1t0 implements AnonymousClass01H {
    public final /* synthetic */ DependencyBridgeModule A00;

    public /* synthetic */ AnonymousClass1t0(DependencyBridgeModule dependencyBridgeModule) {
        this.A00 = dependencyBridgeModule;
    }

    @Override // X.AnonymousClass01H
    public final Object get() {
        return new C41161t1(this.A00);
    }
}
