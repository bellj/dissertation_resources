package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.4TA  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4TA {
    public final UserJid A00;
    public final Integer A01;
    public final Integer A02;
    public final String A03;
    public final String A04;
    public final boolean A05;

    public AnonymousClass4TA(UserJid userJid, Integer num, Integer num2, String str, String str2, boolean z) {
        this.A00 = userJid;
        this.A03 = str;
        this.A02 = num;
        this.A01 = num2;
        this.A04 = str2;
        this.A05 = z;
    }
}
