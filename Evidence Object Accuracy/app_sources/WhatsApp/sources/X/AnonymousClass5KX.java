package X;

/* renamed from: X.5KX  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass5KX extends AbstractC113745Iv implements AnonymousClass1WK, AnonymousClass5ZV {
    public AnonymousClass5KX(Object obj) {
        super(obj);
    }

    @Override // X.AnonymousClass1WK
    public Object AJ3() {
        return C12980iv.A0r(this.receiver);
    }
}
