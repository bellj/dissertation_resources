package X;

import android.animation.ValueAnimator;

/* renamed from: X.0Uy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06760Uy implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ C08800bs A00;
    public final /* synthetic */ C14230l4 A01;
    public final /* synthetic */ C14220l3 A02;
    public final /* synthetic */ AbstractC14200l1 A03;

    public C06760Uy(C08800bs r1, C14230l4 r2, C14220l3 r3, AbstractC14200l1 r4) {
        this.A00 = r1;
        this.A03 = r4;
        this.A02 = r3;
        this.A01 = r2;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        AbstractC14200l1 r2 = this.A03;
        C14250l6.A00(this.A01, this.A02, r2);
    }
}
