package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2ml  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57272ml extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57272ml A04;
    public static volatile AnonymousClass255 A05;
    public double A00;
    public double A01;
    public int A02;
    public String A03 = "";

    static {
        C57272ml r0 = new C57272ml();
        A04 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r15, Object obj, Object obj2) {
        switch (r15.ordinal()) {
            case 0:
                return A04;
            case 1:
                AbstractC462925h r7 = (AbstractC462925h) obj;
                C57272ml r1 = (C57272ml) obj2;
                int i = this.A02;
                boolean A1R = C12960it.A1R(i);
                double d = this.A00;
                int i2 = r1.A02;
                this.A00 = r7.Afn(d, r1.A00, A1R, C12960it.A1R(i2));
                this.A01 = r7.Afn(this.A01, r1.A01, C12960it.A1V(i & 2, 2), C12960it.A1V(i2 & 2, 2));
                this.A03 = r7.Afy(this.A03, r1.A03, C12960it.A1V(i & 4, 4), C12960it.A1V(i2 & 4, 4));
                if (r7 == C463025i.A00) {
                    this.A02 = i | i2;
                }
                return this;
            case 2:
                AnonymousClass253 r72 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        try {
                            int A03 = r72.A03();
                            if (A03 == 0) {
                                break;
                            } else if (A03 == 9) {
                                this.A02 |= 1;
                                this.A00 = Double.longBitsToDouble(r72.A05());
                            } else if (A03 == 17) {
                                this.A02 |= 2;
                                this.A01 = Double.longBitsToDouble(r72.A05());
                            } else if (A03 == 26) {
                                String A0A = r72.A0A();
                                this.A02 |= 4;
                                this.A03 = A0A;
                            } else if (!A0a(r72, A03)) {
                                break;
                            }
                        } catch (IOException e) {
                            throw AbstractC27091Fz.A0K(this, e);
                        }
                    } catch (C28971Pt e2) {
                        throw AbstractC27091Fz.A0J(e2, this);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C57272ml();
            case 5:
                return new C81683uP();
            case 6:
                break;
            case 7:
                if (A05 == null) {
                    synchronized (C57272ml.class) {
                        if (A05 == null) {
                            A05 = AbstractC27091Fz.A09(A04);
                        }
                    }
                }
                return A05;
            default:
                throw C12970iu.A0z();
        }
        return A04;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A02;
        if ((i3 & 1) == 1) {
            i2 = 9;
        }
        if ((i3 & 2) == 2) {
            i2 += 9;
        }
        if ((i3 & 4) == 4) {
            i2 = AbstractC27091Fz.A04(3, this.A03, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A02 & 1) == 1) {
            codedOutputStream.A0G(1, Double.doubleToRawLongBits(this.A00));
        }
        if ((this.A02 & 2) == 2) {
            codedOutputStream.A0G(2, Double.doubleToRawLongBits(this.A01));
        }
        if ((this.A02 & 4) == 4) {
            codedOutputStream.A0I(3, this.A03);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
