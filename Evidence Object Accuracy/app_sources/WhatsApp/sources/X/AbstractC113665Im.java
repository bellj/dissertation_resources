package X;

/* renamed from: X.5Im  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC113665Im extends AbstractC113685Io implements AnonymousClass1WJ, AbstractC115505Ru {
    public final int arity;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AbstractC113665Im(X.AnonymousClass5WO r3) {
        /*
            r2 = this;
            r1 = 2
            if (r3 != 0) goto L_0x000a
            r0 = 0
        L_0x0004:
            r2.<init>(r3, r0)
            r2.arity = r1
            return
        L_0x000a:
            X.5X4 r0 = r3.ABi()
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC113665Im.<init>(X.5WO):void");
    }

    @Override // X.AnonymousClass1WJ
    public int AAk() {
        return this.arity;
    }

    @Override // X.AbstractC112665Eg, java.lang.Object
    public String toString() {
        if (this.completion != null) {
            return super.toString();
        }
        String obj = getClass().getGenericInterfaces()[0].toString();
        if (obj.startsWith("kotlin.jvm.functions.")) {
            obj = obj.substring(21);
        }
        C16700pc.A0B(obj);
        return obj;
    }
}
