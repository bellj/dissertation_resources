package X;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.Build;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.MenuItem;
import com.whatsapp.contact.picker.PhoneContactsSelector;
import com.whatsapp.util.Log;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.38K  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass38K extends AbstractC16350or {
    public final AnonymousClass01d A00;
    public final C16590pI A01;
    public final C15890o4 A02;
    public final WeakReference A03;

    public AnonymousClass38K(PhoneContactsSelector phoneContactsSelector, AnonymousClass01d r3, C16590pI r4, C15890o4 r5) {
        this.A01 = r4;
        this.A00 = r3;
        this.A02 = r5;
        this.A03 = C12970iu.A10(phoneContactsSelector);
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        Cursor query;
        AnonymousClass009.A00();
        Context context = this.A01.A00;
        C15890o4 r2 = this.A02;
        HashMap A11 = C12970iu.A11();
        if (r2.A02("android.permission.READ_CONTACTS") != 0) {
            Log.w("returning empty name map because contact permissions are denied");
        } else {
            Cursor query2 = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI, new String[]{"contact_id", "mimetype", "data2"}, "mimetype IN (?)", new String[]{"vnd.android.cursor.item/name"}, null);
            try {
                if (query2 == null) {
                    Log.e("null cursor returned from structured name query");
                } else {
                    int columnIndex = query2.getColumnIndex("contact_id");
                    if (columnIndex == -1) {
                        Log.e("invalid column index for the raw contact id");
                    } else {
                        int columnIndex2 = query2.getColumnIndex("mimetype");
                        if (columnIndex2 == -1) {
                            Log.e("invalid column index for the mimetype");
                        } else {
                            int columnIndex3 = query2.getColumnIndex("data2");
                            if (columnIndex3 == -1) {
                                Log.e("invalid column index for the given name");
                            } else {
                                while (query2.moveToNext()) {
                                    if (query2.isNull(columnIndex)) {
                                        Log.e("null raw contact id for record; skipping");
                                    } else if (query2.isNull(columnIndex2)) {
                                        Log.e("null mimetype for record; skipping");
                                    } else {
                                        Long valueOf = Long.valueOf(query2.getLong(columnIndex));
                                        if (((String) A11.get(valueOf)) == null) {
                                            String string = query2.getString(columnIndex2);
                                            if (string == null) {
                                                Log.e("mimetype was returned as null even though cursor said it wasn't null; skipping");
                                            } else if (string.hashCode() != -1079224304 || !string.equals("vnd.android.cursor.item/name")) {
                                                StringBuilder A0h = C12960it.A0h();
                                                A0h.append("unrecognized mimetype; skipping; mimetype=");
                                                Log.e(C12960it.A0d(string, A0h));
                                            } else {
                                                A11.put(valueOf, query2.getString(columnIndex3));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    query2.close();
                }
            } catch (Throwable th) {
                if (query2 != null) {
                    try {
                        query2.close();
                    } catch (Throwable unused) {
                    }
                }
                throw th;
            }
        }
        HashSet A12 = C12970iu.A12();
        String[] strArr = {"_id", "display_name"};
        ContentResolver A0C = this.A00.A0C();
        if (A0C == null) {
            Log.w("phone-contacts-selector/contact cr=null");
        } else {
            try {
                query = A0C.query(ContactsContract.Contacts.CONTENT_URI.buildUpon().appendQueryParameter("directory", String.valueOf(0L)).build(), strArr, null, null, null);
            } catch (Exception e) {
                Log.e("phone-contacts-selector/contact exception", e);
            }
            if (query == null) {
                Log.e("phone-contacts-selector/contact cursor was null");
            } else {
                while (query.moveToNext()) {
                    long j = query.getLong(0);
                    String string2 = query.getString(1);
                    String str = (String) A11.get(Long.valueOf(j));
                    if (!TextUtils.isEmpty(string2)) {
                        A12.add(new C51492Vb(string2, str, j));
                    }
                }
                query.close();
                if (Build.VERSION.SDK_INT >= 18 || (r1 = A08()) == null) {
                    List list = C12960it.A0l();
                }
                return new AnonymousClass4NQ(list, A12);
            }
        }
        A12 = C12970iu.A12();
        if (Build.VERSION.SDK_INT >= 18) {
        }
        List list = C12960it.A0l();
        return new AnonymousClass4NQ(list, A12);
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        AnonymousClass4NQ r14 = (AnonymousClass4NQ) obj;
        PhoneContactsSelector phoneContactsSelector = (PhoneContactsSelector) this.A03.get();
        if (!(phoneContactsSelector == null || phoneContactsSelector.AJN())) {
            phoneContactsSelector.A0K = null;
            if (Build.VERSION.SDK_INT >= 18) {
                phoneContactsSelector.A0c.removeAll(r14.A00);
            } else {
                Iterator it = phoneContactsSelector.A0b.iterator();
                while (it.hasNext()) {
                    Object next = it.next();
                    if (!r14.A01.contains(next)) {
                        phoneContactsSelector.A0c.remove(next);
                    }
                }
            }
            phoneContactsSelector.A0Z.A02();
            ArrayList arrayList = phoneContactsSelector.A0b;
            arrayList.clear();
            arrayList.addAll(r14.A01);
            List<C51492Vb> list = phoneContactsSelector.A0c;
            for (C51492Vb r10 : list) {
                Iterator it2 = arrayList.iterator();
                while (it2.hasNext()) {
                    C51492Vb r6 = (C51492Vb) it2.next();
                    if (r6.A04 == r10.A04) {
                        r6.A03 = true;
                    }
                }
            }
            if (list.isEmpty()) {
                phoneContactsSelector.A2g();
            }
            phoneContactsSelector.A2h(list.size());
            MenuItem menuItem = phoneContactsSelector.A00;
            if (menuItem != null) {
                menuItem.setVisible(!arrayList.isEmpty());
            }
            PhoneContactsSelector.A03(phoneContactsSelector);
        }
    }

    public final List A08() {
        ArrayList A0l = C12960it.A0l();
        String[] A08 = C13000ix.A08();
        A08[0] = "contact_id";
        ContentResolver A0C = this.A00.A0C();
        if (A0C == null) {
            Log.w("phone-contacts-selector/contact cr=null");
            return null;
        }
        try {
            Cursor query = A0C.query(ContactsContract.DeletedContacts.CONTENT_URI, A08, null, null, null);
            if (query != null) {
                int columnIndex = query.getColumnIndex("contact_id");
                while (query.moveToNext()) {
                    A0l.add(new C51492Vb(null, null, (long) query.getInt(columnIndex)));
                }
                query.close();
                return A0l;
            }
            Log.e("phone-contacts-selector/search deleted contact cursor was null");
            return null;
        } catch (Exception e) {
            Log.e("phone-contacts-selector/query deleted contact exception", e);
            return null;
        }
    }
}
