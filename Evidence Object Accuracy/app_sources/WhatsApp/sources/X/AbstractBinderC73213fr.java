package X;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;

/* renamed from: X.3fr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractBinderC73213fr extends Binder implements IInterface {
    @Override // android.os.IInterface
    public final IBinder asBinder() {
        return this;
    }

    public AbstractBinderC73213fr() {
        attachInterface(this, "com.google.android.gms.signin.internal.ISignInCallbacks");
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x004b  */
    @Override // android.os.Binder
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean onTransact(int r3, android.os.Parcel r4, android.os.Parcel r5, int r6) {
        /*
            r2 = this;
            r0 = 16777215(0xffffff, float:2.3509886E-38)
            if (r3 <= r0) goto L_0x000d
            boolean r0 = super.onTransact(r3, r4, r5, r6)
            if (r0 == 0) goto L_0x0010
        L_0x000b:
            r0 = 1
            return r0
        L_0x000d:
            X.C72463ee.A0Q(r2, r4)
        L_0x0010:
            r1 = r2
            X.3pz r1 = (X.AbstractBinderC79063pz) r1
            switch(r3) {
                case 3: goto L_0x0038;
                case 4: goto L_0x0035;
                case 5: goto L_0x0016;
                case 6: goto L_0x0035;
                case 7: goto L_0x0027;
                case 8: goto L_0x001b;
                case 9: goto L_0x0018;
                default: goto L_0x0016;
            }
        L_0x0016:
            r0 = 0
            return r0
        L_0x0018:
            android.os.Parcelable$Creator r1 = X.C78673pI.CREATOR
            goto L_0x0045
        L_0x001b:
            android.os.Parcelable$Creator r0 = X.C78323oj.CREATOR
            android.os.Parcelable r0 = X.C12970iu.A0F(r4, r0)
            X.3oj r0 = (X.C78323oj) r0
            r1.AgR(r0)
            goto L_0x004e
        L_0x0027:
            android.os.Parcelable$Creator r1 = com.google.android.gms.common.api.Status.CREATOR
            int r0 = r4.readInt()
            if (r0 == 0) goto L_0x0032
            r1.createFromParcel(r4)
        L_0x0032:
            android.os.Parcelable$Creator r1 = com.google.android.gms.auth.api.signin.GoogleSignInAccount.CREATOR
            goto L_0x0045
        L_0x0035:
            android.os.Parcelable$Creator r1 = com.google.android.gms.common.api.Status.CREATOR
            goto L_0x0045
        L_0x0038:
            android.os.Parcelable$Creator r1 = X.C56492ky.CREATOR
            int r0 = r4.readInt()
            if (r0 == 0) goto L_0x0043
            r1.createFromParcel(r4)
        L_0x0043:
            android.os.Parcelable$Creator r1 = X.C78683pJ.CREATOR
        L_0x0045:
            int r0 = r4.readInt()
            if (r0 == 0) goto L_0x004e
            r1.createFromParcel(r4)
        L_0x004e:
            r5.writeNoException()
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractBinderC73213fr.onTransact(int, android.os.Parcel, android.os.Parcel, int):boolean");
    }
}
