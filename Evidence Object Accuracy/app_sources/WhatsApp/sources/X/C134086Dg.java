package X;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.facebook.redex.IDxCListenerShape11S0100000_3_I1;
import com.whatsapp.R;

/* renamed from: X.6Dg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C134086Dg implements AnonymousClass5Wu {
    public View.OnClickListener A00;
    public View A01;
    public View A02;
    public ImageView A03;
    public TextView A04;

    /* renamed from: A00 */
    public void A6Q(AnonymousClass4OZ r5) {
        View view;
        IDxCListenerShape11S0100000_3_I1 iDxCListenerShape11S0100000_3_I1;
        if (r5 != null) {
            int i = r5.A00;
            if (i == -2 || i == -1) {
                C117315Zl.A0N(this.A04, r5.A01);
                this.A03.setVisibility(8);
                this.A02.setVisibility(8);
                this.A01.setVisibility(0);
                view = this.A01;
                iDxCListenerShape11S0100000_3_I1 = null;
            } else if (i == 0) {
                this.A01.setVisibility(8);
                this.A02.setVisibility(8);
                this.A03.setVisibility(8);
                return;
            } else if (i == 1) {
                this.A01.setVisibility(8);
                this.A03.setVisibility(8);
                this.A02.setVisibility(0);
                return;
            } else if (i == 2) {
                C117315Zl.A0N(this.A04, r5.A01);
                this.A02.setVisibility(8);
                this.A01.setVisibility(0);
                this.A03.setVisibility(0);
                view = this.A01;
                iDxCListenerShape11S0100000_3_I1 = C117305Zk.A0A(this, 79);
            } else {
                return;
            }
            view.setOnClickListener(iDxCListenerShape11S0100000_3_I1);
        }
    }

    @Override // X.AnonymousClass5Wu
    public int ADq() {
        return R.layout.novi_amount_input_description;
    }

    @Override // X.AnonymousClass5Wu
    public void AYL(View view) {
        this.A02 = AnonymousClass028.A0D(view, R.id.send_payment_amount_description_shimmer);
        this.A01 = AnonymousClass028.A0D(view, R.id.send_payment_amount_description_layout);
        this.A04 = C12960it.A0I(view, R.id.send_payment_amount_description);
        this.A03 = C12970iu.A0K(view, R.id.send_payment_switch_currency_icon);
    }
}
