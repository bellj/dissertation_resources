package X;

import java.io.IOException;

/* renamed from: X.4bo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C94434bo {
    public static final C95254dO A02 = C95254dO.A00();
    public volatile AbstractC111925Bi A00;
    public volatile AbstractC117135Yq A01;

    public int hashCode() {
        return 1;
    }

    public final AbstractC111925Bi A00() {
        AbstractC111925Bi r1;
        if (this.A00 != null) {
            return this.A00;
        }
        synchronized (this) {
            if (this.A00 != null) {
                return this.A00;
            }
            if (this.A01 == null) {
                r1 = AbstractC111925Bi.A00;
            } else {
                AbstractC117135Yq r4 = this.A01;
                try {
                    AbstractC80173rp r2 = (AbstractC80173rp) r4;
                    int i = r2.zzc;
                    if (i == -1) {
                        i = C72463ee.A0C(r2).Ah2(r2);
                        r2.zzc = i;
                    }
                    AnonymousClass4ML r3 = new AnonymousClass4ML(i);
                    C80253rx r22 = r3.A00;
                    AnonymousClass5XU A0C = C72463ee.A0C(r4);
                    C1090250c r0 = r22.A01;
                    if (r0 == null) {
                        r0 = new C1090250c(r22);
                    }
                    A0C.Agy(r0, r4);
                    if (r22.A02 - r22.A00 == 0) {
                        r1 = new C80273rz(r3.A01);
                    } else {
                        throw C12960it.A0U("Did not write as much data as expected.");
                    }
                } catch (IOException e) {
                    throw new RuntimeException(C12960it.A0d(" threw an IOException (should never happen).", C72453ed.A0v(r4, "ByteString")), e);
                }
            }
            this.A00 = r1;
            return this.A00;
        }
    }

    public final AbstractC117135Yq A01(AbstractC117135Yq r2) {
        if (this.A01 == null) {
            synchronized (this) {
                if (this.A01 == null) {
                    try {
                        this.A01 = r2;
                        this.A00 = AbstractC111925Bi.A00;
                    } catch (C868148y unused) {
                        this.A01 = r2;
                        this.A00 = AbstractC111925Bi.A00;
                    }
                }
            }
        }
        return this.A01;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C94434bo)) {
            return false;
        }
        C94434bo r4 = (C94434bo) obj;
        AbstractC117135Yq r2 = this.A01;
        AbstractC117135Yq r1 = r4.A01;
        if (r2 == null) {
            if (r1 == null) {
                return A00().equals(r4.A00());
            }
            return A01(r1.AhS()).equals(r1);
        } else if (r1 != null) {
            return r2.equals(r1);
        } else {
            return r2.equals(r4.A01(r2.AhS()));
        }
    }
}
