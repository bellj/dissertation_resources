package X;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.view.ActionProvider;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

/* renamed from: X.08g  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C017708g implements AbstractMenuItemC017808h {
    public char A00;
    public char A01;
    public int A02 = 16;
    public int A03 = 4096;
    public int A04 = 4096;
    public Context A05;
    public Intent A06;
    public ColorStateList A07 = null;
    public PorterDuff.Mode A08 = null;
    public Drawable A09;
    public MenuItem.OnMenuItemClickListener A0A;
    public CharSequence A0B;
    public CharSequence A0C;
    public CharSequence A0D;
    public CharSequence A0E;
    public boolean A0F = false;
    public boolean A0G = false;
    public final int A0H;

    @Override // X.AbstractMenuItemC017808h
    public AbstractC04730Mv AGz() {
        return null;
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public boolean collapseActionView() {
        return false;
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public boolean expandActionView() {
        return false;
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public View getActionView() {
        return null;
    }

    @Override // android.view.MenuItem
    public ContextMenu.ContextMenuInfo getMenuInfo() {
        return null;
    }

    @Override // android.view.MenuItem
    public SubMenu getSubMenu() {
        return null;
    }

    @Override // android.view.MenuItem
    public boolean hasSubMenu() {
        return false;
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public boolean isActionViewExpanded() {
        return false;
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public void setShowAsAction(int i) {
    }

    public C017708g(Context context, CharSequence charSequence) {
        this.A05 = context;
        this.A0H = 16908332;
        this.A0C = charSequence;
    }

    public final void A00() {
        Drawable drawable = this.A09;
        if (drawable == null) {
            return;
        }
        if (this.A0F || this.A0G) {
            Drawable A03 = C015607k.A03(drawable);
            this.A09 = A03;
            Drawable mutate = A03.mutate();
            this.A09 = mutate;
            if (this.A0F) {
                C015607k.A04(this.A07, mutate);
            }
            if (this.A0G) {
                C015607k.A07(this.A08, this.A09);
            }
        }
    }

    @Override // X.AbstractMenuItemC017808h
    public AbstractMenuItemC017808h Abw(CharSequence charSequence) {
        this.A0B = charSequence;
        return this;
    }

    @Override // X.AbstractMenuItemC017808h
    public AbstractMenuItemC017808h Acx(AbstractC04730Mv r2) {
        throw new UnsupportedOperationException();
    }

    @Override // X.AbstractMenuItemC017808h
    public AbstractMenuItemC017808h Ad2(CharSequence charSequence) {
        this.A0E = charSequence;
        return this;
    }

    @Override // android.view.MenuItem
    public ActionProvider getActionProvider() {
        throw new UnsupportedOperationException();
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public int getAlphabeticModifiers() {
        return this.A03;
    }

    @Override // android.view.MenuItem
    public char getAlphabeticShortcut() {
        return this.A00;
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public CharSequence getContentDescription() {
        return this.A0B;
    }

    @Override // android.view.MenuItem
    public int getGroupId() {
        return 0;
    }

    @Override // android.view.MenuItem
    public Drawable getIcon() {
        return this.A09;
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public ColorStateList getIconTintList() {
        return this.A07;
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public PorterDuff.Mode getIconTintMode() {
        return this.A08;
    }

    @Override // android.view.MenuItem
    public Intent getIntent() {
        return this.A06;
    }

    @Override // android.view.MenuItem
    public int getItemId() {
        return this.A0H;
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public int getNumericModifiers() {
        return this.A04;
    }

    @Override // android.view.MenuItem
    public char getNumericShortcut() {
        return this.A01;
    }

    @Override // android.view.MenuItem
    public int getOrder() {
        return 0;
    }

    @Override // android.view.MenuItem
    public CharSequence getTitle() {
        return this.A0C;
    }

    @Override // android.view.MenuItem
    public CharSequence getTitleCondensed() {
        CharSequence charSequence = this.A0D;
        return charSequence == null ? this.A0C : charSequence;
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public CharSequence getTooltipText() {
        return this.A0E;
    }

    @Override // android.view.MenuItem
    public boolean isCheckable() {
        return (this.A02 & 1) != 0;
    }

    @Override // android.view.MenuItem
    public boolean isChecked() {
        return (this.A02 & 2) != 0;
    }

    @Override // android.view.MenuItem
    public boolean isEnabled() {
        return (this.A02 & 16) != 0;
    }

    @Override // android.view.MenuItem
    public boolean isVisible() {
        return (this.A02 & 8) == 0;
    }

    @Override // android.view.MenuItem
    public MenuItem setActionProvider(ActionProvider actionProvider) {
        throw new UnsupportedOperationException();
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public MenuItem setActionView(int i) {
        throw new UnsupportedOperationException();
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public MenuItem setActionView(View view) {
        throw new UnsupportedOperationException();
    }

    @Override // android.view.MenuItem
    public MenuItem setAlphabeticShortcut(char c) {
        this.A00 = Character.toLowerCase(c);
        return this;
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public MenuItem setAlphabeticShortcut(char c, int i) {
        this.A00 = Character.toLowerCase(c);
        this.A03 = KeyEvent.normalizeMetaState(i);
        return this;
    }

    @Override // android.view.MenuItem
    public MenuItem setCheckable(boolean z) {
        this.A02 = (z ? 1 : 0) | (this.A02 & -2);
        return this;
    }

    @Override // android.view.MenuItem
    public MenuItem setChecked(boolean z) {
        int i = this.A02 & -3;
        int i2 = 0;
        if (z) {
            i2 = 2;
        }
        this.A02 = i2 | i;
        return this;
    }

    @Override // android.view.MenuItem
    public /* bridge */ /* synthetic */ MenuItem setContentDescription(CharSequence charSequence) {
        this.A0B = charSequence;
        return this;
    }

    @Override // android.view.MenuItem
    public MenuItem setEnabled(boolean z) {
        int i = this.A02 & -17;
        int i2 = 0;
        if (z) {
            i2 = 16;
        }
        this.A02 = i2 | i;
        return this;
    }

    @Override // android.view.MenuItem
    public MenuItem setIcon(int i) {
        this.A09 = AnonymousClass00T.A04(this.A05, i);
        A00();
        return this;
    }

    @Override // android.view.MenuItem
    public MenuItem setIcon(Drawable drawable) {
        this.A09 = drawable;
        A00();
        return this;
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public MenuItem setIconTintList(ColorStateList colorStateList) {
        this.A07 = colorStateList;
        this.A0F = true;
        A00();
        return this;
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public MenuItem setIconTintMode(PorterDuff.Mode mode) {
        this.A08 = mode;
        this.A0G = true;
        A00();
        return this;
    }

    @Override // android.view.MenuItem
    public MenuItem setIntent(Intent intent) {
        this.A06 = intent;
        return this;
    }

    @Override // android.view.MenuItem
    public MenuItem setNumericShortcut(char c) {
        this.A01 = c;
        return this;
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public MenuItem setNumericShortcut(char c, int i) {
        this.A01 = c;
        this.A04 = KeyEvent.normalizeMetaState(i);
        return this;
    }

    @Override // android.view.MenuItem
    public MenuItem setOnActionExpandListener(MenuItem.OnActionExpandListener onActionExpandListener) {
        throw new UnsupportedOperationException();
    }

    @Override // android.view.MenuItem
    public MenuItem setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        this.A0A = onMenuItemClickListener;
        return this;
    }

    @Override // android.view.MenuItem
    public MenuItem setShortcut(char c, char c2) {
        this.A01 = c;
        this.A00 = Character.toLowerCase(c2);
        return this;
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public MenuItem setShortcut(char c, char c2, int i, int i2) {
        this.A01 = c;
        this.A04 = KeyEvent.normalizeMetaState(i);
        this.A00 = Character.toLowerCase(c2);
        this.A03 = KeyEvent.normalizeMetaState(i2);
        return this;
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public /* bridge */ /* synthetic */ MenuItem setShowAsActionFlags(int i) {
        return this;
    }

    @Override // android.view.MenuItem
    public MenuItem setTitle(int i) {
        this.A0C = this.A05.getResources().getString(i);
        return this;
    }

    @Override // android.view.MenuItem
    public MenuItem setTitle(CharSequence charSequence) {
        this.A0C = charSequence;
        return this;
    }

    @Override // android.view.MenuItem
    public MenuItem setTitleCondensed(CharSequence charSequence) {
        this.A0D = charSequence;
        return this;
    }

    @Override // android.view.MenuItem
    public /* bridge */ /* synthetic */ MenuItem setTooltipText(CharSequence charSequence) {
        this.A0E = charSequence;
        return this;
    }

    @Override // android.view.MenuItem
    public MenuItem setVisible(boolean z) {
        int i = 8;
        int i2 = this.A02 & 8;
        if (z) {
            i = 0;
        }
        this.A02 = i2 | i;
        return this;
    }
}
