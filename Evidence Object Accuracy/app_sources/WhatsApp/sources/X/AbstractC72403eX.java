package X;

import android.app.Activity;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.3eX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public interface AbstractC72403eX {
    void A6D(Activity activity, C90184Mx v, HashMap hashMap);

    void A92(Activity activity, String str);

    long ACv(Activity activity);

    String ACw(Activity activity);

    String AFv(Activity activity, String str, HashMap hashMap);

    boolean AI0(Activity activity, int i);

    void AJC(Activity activity, C90184Mx v, String str, String str2, String str3, Map map);

    void AKP(Activity activity, C90184Mx v, C64173En v2, String str, HashMap hashMap);

    void AYk(Activity activity, C90184Mx v, C64173En v2, String str, HashMap hashMap);

    void AYl(Activity activity, C90184Mx v, C64173En v2, String str, HashMap hashMap);

    void AZM(Activity activity, HashMap hashMap);

    void Aah(Activity activity);

    void Abc(String str, ArrayList arrayList, HashMap hashMap, int i, int i2);

    Map Abg();

    void Abt(String str);

    void Adi(Activity activity, C90184Mx v, HashMap hashMap);

    void Adw(Activity activity, C64173En v, Boolean bool, String str);

    void Ae0(String str);

    void AeA(Activity activity);

    Object Aeo(Long l, String str);
}
