package X;

import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import com.whatsapp.R;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.1lG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37071lG {
    public boolean A00 = false;
    public final C252918v A01;
    public final Set A02;

    public C37071lG(C252918v r2) {
        this.A01 = r2;
        this.A02 = new HashSet();
        r2.A00();
    }

    public void A00() {
        C58962tj r1;
        if (!this.A00) {
            Set<C68203Um> set = this.A02;
            for (C68203Um r12 : set) {
                this.A01.A01(r12);
            }
            set.clear();
            C252918v r2 = this.A01;
            int i = r2.A00 - 1;
            r2.A00 = i;
            if (i == 0 && (r1 = r2.A02) != null) {
                r1.A02(false);
                r2.A02 = null;
            }
            this.A00 = true;
        }
    }

    public void A01(ImageView imageView, C44741zT r16, AnonymousClass5TN r17, AnonymousClass5TO r18, AnonymousClass2E5 r19, int i) {
        C68203Um r5 = new C68203Um(imageView, r16, new AnonymousClass5TN(r17, this) { // from class: X.53h
            public final /* synthetic */ AnonymousClass5TN A00;
            public final /* synthetic */ C37071lG A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AnonymousClass5TN
            public final void AML(C68203Um r3) {
                C37071lG r0 = this.A01;
                AnonymousClass5TN r1 = this.A00;
                r0.A02.add(r3);
                if (r1 != null) {
                    r1.AML(r3);
                }
            }
        }, new AnonymousClass5TO(r18) { // from class: X.53l
            public final /* synthetic */ AnonymousClass5TO A01;

            {
                this.A01 = r2;
            }

            @Override // X.AnonymousClass5TO
            public final void ARz(C68203Um r2) {
                AnonymousClass5TO r0 = this.A01;
                if (r0 != null) {
                    r0.ARz(r2);
                }
            }
        }, new AnonymousClass2E5(r19, this) { // from class: X.53p
            public final /* synthetic */ AnonymousClass2E5 A00;
            public final /* synthetic */ C37071lG A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AnonymousClass2E5
            public final void AS6(Bitmap bitmap, C68203Um r4, boolean z) {
                C37071lG r0 = this.A01;
                AnonymousClass2E5 r1 = this.A00;
                if (!z) {
                    r0.A02.remove(r4);
                }
                r1.AS6(bitmap, r4, z);
            }
        }, i, Integer.MAX_VALUE, Integer.MAX_VALUE);
        if (!TextUtils.isEmpty(r5.AHT())) {
            C252918v r4 = this.A01;
            if (r4.A02 != null) {
                View view = (View) r5.A09.get();
                if (view != null) {
                    view.setTag(R.id.image_id, r5.A05.A04);
                    view.setTag(R.id.image_quality, Integer.valueOf(r5.A04));
                    if (!r5.AHT().equals(view.getTag(R.id.loaded_image_url))) {
                        view.setTag(R.id.loaded_image_url, null);
                    }
                }
                r4.A02.A01(r5, true);
            }
        } else if (r18 != null) {
            r18.ARz(r5);
        }
    }

    public void A02(ImageView imageView, C44741zT r9, AnonymousClass5TN r10, AnonymousClass2E5 r11, int i) {
        A01(imageView, r9, r10, null, r11, i);
    }

    public void finalize() {
        super.finalize();
    }
}
