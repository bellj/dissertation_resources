package X;

import android.content.Context;
import com.google.android.material.chip.Chip;
import com.whatsapp.R;

/* renamed from: X.2vn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60042vn extends C60052vo {
    public final AnonymousClass018 A00;

    public C60042vn(Chip chip, AnonymousClass2Jw r2, AnonymousClass018 r3) {
        super(chip, r2);
        this.A00 = r3;
    }

    @Override // X.C60052vo, X.AbstractC75703kH
    public void A08(AnonymousClass4UW r5) {
        Chip chip = ((C60052vo) this).A00;
        chip.setChipIconResource(R.drawable.ic_business_location);
        chip.setChipIconVisible(true);
        super.A08(r5);
        boolean A00 = AnonymousClass4E6.A00(C12970iu.A14(this.A00));
        Context context = chip.getContext();
        int i = R.string.biz_dir_distance_metric;
        if (A00) {
            i = R.string.biz_dir_distance_imperial;
        }
        C12970iu.A19(context, chip, i);
        C12960it.A0r(chip.getContext(), chip, i);
        C12960it.A13(chip, this, r5, 13);
    }
}
