package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.3zb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C84613zb extends AbstractC90164Mv {
    public final boolean A00;
    public final boolean A01;

    public C84613zb(UserJid userJid, String str, boolean z, boolean z2) {
        super(userJid, str);
        this.A01 = z;
        this.A00 = z2;
    }
}
