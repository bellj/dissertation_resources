package X;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.Date;
import java.util.TimeZone;

/* renamed from: X.2ob  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class DialogC58302ob extends AnonymousClass27U {
    public final /* synthetic */ C20640w5 A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public DialogC58302ob(Activity activity, C20640w5 r8, AnonymousClass01d r9, C14830m7 r10, AnonymousClass018 r11) {
        super(activity, r9, r10, r11, R.layout.clock_wrong);
        this.A00 = r8;
    }

    @Override // X.AnonymousClass27U, android.app.Dialog
    public void onCreate(Bundle bundle) {
        long time;
        super.onCreate(bundle);
        Date date = new Date();
        Log.w(C12960it.A0d(date.toString(), C12960it.A0k("conversations/clock-wrong-time ")));
        Date date2 = this.A00.A00;
        if (date2 != null) {
            time = date2.getTime();
        } else {
            time = date.getTime();
        }
        Activity activity = this.A01;
        Object[] A1a = C12980iv.A1a();
        AnonymousClass018 r3 = this.A04;
        A1a[0] = C38121nY.A05(r3, AnonymousClass1MY.A04(r3, time), AnonymousClass3JK.A00(r3, time));
        ((TextView) findViewById(R.id.clock_wrong_date)).setText(C12960it.A0X(activity, TimeZone.getDefault().getDisplayName(C12970iu.A14(r3)), A1a, 1, R.string.clock_wrong_report_current_date_time));
        C12960it.A10(findViewById(R.id.close), this, 8);
    }
}
