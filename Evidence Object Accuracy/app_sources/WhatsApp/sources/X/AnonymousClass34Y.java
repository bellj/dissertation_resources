package X;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.components.button.ThumbnailButton;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.34Y  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass34Y extends AnonymousClass46v {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public FrameLayout A04;
    public WaImageView A05;
    public WaImageView A06;
    public C53202d5 A07;
    public C39271pZ A08;
    public List A09;
    public boolean A0A;
    public final C14900mE A0B;
    public final AnonymousClass130 A0C;
    public final AnonymousClass1J1 A0D;
    public final AnonymousClass018 A0E;
    public final C39091pH A0F;

    public AnonymousClass34Y(Context context, C14900mE r2, AnonymousClass130 r3, AnonymousClass1J1 r4, AnonymousClass018 r5, C39091pH r6) {
        super(context);
        A00();
        this.A0B = r2;
        this.A0C = r3;
        this.A0E = r5;
        this.A0F = r6;
        this.A0D = r4;
        A03();
    }

    @Override // X.AbstractC74143hO
    public void A00() {
        if (!this.A0A) {
            this.A0A = true;
            generatedComponent();
        }
    }

    @Override // X.AnonymousClass46z
    public View A01() {
        this.A07 = new C53202d5(getContext());
        FrameLayout.LayoutParams A0M = C12990iw.A0M();
        int A06 = C12980iv.A06(this);
        C42941w9.A0A(this.A07, this.A0E, 0, 0, A06, 0);
        this.A07.setLayoutParams(A0M);
        return this.A07;
    }

    @Override // X.AnonymousClass46z
    public View A02() {
        Context context = getContext();
        this.A04 = new FrameLayout(context);
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.search_attachment_icon_size);
        this.A00 = getResources().getDimensionPixelSize(R.dimen.contact_card_border_size);
        this.A02 = getResources().getDimensionPixelSize(R.dimen.contact_card_radius);
        this.A04.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
        this.A06 = A04(context, dimensionPixelSize);
        ThumbnailButton A04 = A04(context, dimensionPixelSize);
        this.A05 = A04;
        ArrayList A0l = C12960it.A0l();
        this.A09 = A0l;
        A0l.add(this.A06);
        A0l.add(A04);
        this.A01 = C12980iv.A06(this);
        int dimensionPixelSize2 = getResources().getDimensionPixelSize(R.dimen.search_attachment_contact_offset);
        this.A03 = dimensionPixelSize2;
        C42941w9.A09(this.A05, this.A0E, dimensionPixelSize2, 0, 0, 0);
        this.A04.addView(this.A05);
        this.A04.addView(this.A06);
        return this.A04;
    }

    public final ThumbnailButton A04(Context context, int i) {
        ThumbnailButton thumbnailButton = new ThumbnailButton(context);
        thumbnailButton.setLayoutParams(new FrameLayout.LayoutParams(i, i));
        thumbnailButton.setScaleType(ImageView.ScaleType.FIT_CENTER);
        thumbnailButton.A03 = AnonymousClass00T.A00(getContext(), R.color.search_attachment_background);
        thumbnailButton.A01 = (float) this.A00;
        thumbnailButton.A02 = (float) this.A02;
        thumbnailButton.A07 = false;
        AnonymousClass028.A0a(thumbnailButton, 2);
        return thumbnailButton;
    }

    public void A05(AbstractC15340mz r4, List list) {
        Runnable A01;
        this.A07.setSubText(null, null);
        C39271pZ r1 = this.A08;
        if (r1 != null) {
            this.A0F.A03(r1);
        }
        C39091pH r12 = this.A0F;
        synchronized (r12) {
            A01 = r12.A01(r4, null);
        }
        C39271pZ r2 = (C39271pZ) A01;
        this.A08 = r2;
        r2.A01(new AbstractC14590lg(r4, this, list) { // from class: X.3bh
            public final /* synthetic */ AbstractC15340mz A00;
            public final /* synthetic */ AnonymousClass34Y A01;
            public final /* synthetic */ List A02;

            {
                this.A01 = r2;
                this.A00 = r1;
                this.A02 = r3;
            }

            @Override // X.AbstractC14590lg
            public final void accept(Object obj) {
                String A08;
                AnonymousClass34Y r11 = this.A01;
                AbstractC15340mz r0 = this.A00;
                List list2 = this.A02;
                AnonymousClass4S8 r13 = (AnonymousClass4S8) obj;
                if (r0 instanceof C30411Xh) {
                    C30721Yo r3 = r13.A03;
                    if (r3 != null) {
                        r11.A0D.A08(r11.A06, r3);
                        r11.A07.setTitleAndDescription(AnonymousClass1US.A03(128, r3.A08()), null, list2);
                        List list3 = r3.A05;
                        if (!(list3 == null || list3.isEmpty())) {
                            r11.A07.setSubText(((C30741Yq) C12980iv.A0o(r3.A05)).A02, list2);
                            return;
                        }
                        return;
                    }
                    return;
                }
                List list4 = r13.A02;
                for (int i = 0; i < r11.A09.size(); i++) {
                    if (i < list4.size()) {
                        r11.A0D.A08((ImageView) r11.A09.get(i), (C30721Yo) list4.get(i));
                    }
                }
                int i2 = r13.A00;
                C30721Yo r02 = r13.A03;
                if (r02 == null) {
                    A08 = null;
                } else {
                    A08 = r02.A08();
                }
                if (A08 != null) {
                    int i3 = i2 - 1;
                    String A03 = AnonymousClass1US.A03(128, A08);
                    Object[] A1a = C12980iv.A1a();
                    A1a[0] = A03;
                    C12960it.A1P(A1a, i3, 1);
                    r11.A07.setTitleAndDescription(r11.A0E.A0I(A1a, R.plurals.contacts_array_title, (long) i3), null, list2);
                    return;
                }
                C53202d5 r5 = r11.A07;
                Object[] objArr = new Object[1];
                C12960it.A1P(objArr, i2, 0);
                r5.setTitleAndDescription(r11.A0E.A0I(objArr, R.plurals.n_contacts_message_title, (long) i2), null, null);
            }
        }, this.A0B.A06);
    }

    public void setMessage(C30351Xb r7, List list) {
        int i = this.A01;
        int i2 = ((i << 1) - this.A03) >> 1;
        C42941w9.A09(this.A04, this.A0E, i2, i, i2, i);
        AnonymousClass130 r2 = this.A0C;
        r2.A05(this.A06, R.drawable.avatar_contact);
        r2.A05(this.A05, R.drawable.avatar_contact);
        this.A05.setVisibility(0);
        this.A06.setVisibility(0);
        A05(r7, list);
    }

    public void setMessage(C30411Xh r7, List list) {
        AnonymousClass018 r1 = this.A0E;
        FrameLayout frameLayout = this.A04;
        int i = this.A01;
        C42941w9.A09(frameLayout, r1, i, i, i, i);
        this.A0C.A05(this.A06, R.drawable.avatar_contact);
        this.A05.setVisibility(8);
        String A01 = C47962Dl.A01(getContext(), r7);
        if (A01 == null) {
            A01 = "";
        }
        this.A07.setTitleAndDescription(AnonymousClass1US.A03(128, A01), null, list);
        A05(r7, list);
    }
}
