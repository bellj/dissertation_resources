package X;

/* renamed from: X.39D  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass39D extends AnonymousClass2B2 {
    public int A00;
    public int A01;
    public long A02;
    public long A03 = 0;
    public C43161wW A04;
    public boolean A05;
    public boolean A06;
    public final int A07;
    public final C22180yf A08;
    public final C16120oU A09;
    public final AnonymousClass4Xy A0A = new AnonymousClass4Xy();
    public final AnonymousClass4Xy A0B = new AnonymousClass4Xy();
    public final AnonymousClass4Xy A0C = new AnonymousClass4Xy();
    public final AnonymousClass4Xy A0D = new AnonymousClass4Xy();
    public final AnonymousClass1X4 A0E;

    public AnonymousClass39D(C22180yf r3, C16120oU r4, C43161wW r5, AnonymousClass1X4 r6, int i, int i2, int i3) {
        super(i2, i3);
        this.A09 = r4;
        this.A08 = r3;
        this.A0E = r6;
        this.A04 = r5;
        this.A00 = 0;
        this.A07 = i;
    }
}
