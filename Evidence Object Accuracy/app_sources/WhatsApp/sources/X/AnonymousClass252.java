package X;

import android.database.ContentObserver;

/* renamed from: X.252  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass252 extends ContentObserver {
    public final /* synthetic */ C15570nT A00;
    public final /* synthetic */ C249417m A01;
    public final /* synthetic */ C20730wE A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass252(C15570nT r2, C249417m r3, C20730wE r4) {
        super(null);
        this.A01 = r3;
        this.A00 = r2;
        this.A02 = r4;
    }

    @Override // android.database.ContentObserver
    public void onChange(boolean z) {
        C15570nT r1 = this.A00;
        r1.A08();
        if (r1.A00 != null) {
            r1.A08();
            this.A02.A05();
        }
    }
}
