package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import com.whatsapp.TextEmojiLabel;

/* renamed from: X.2YU  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2YU extends AnimatorListenerAdapter {
    public final /* synthetic */ TextEmojiLabel A00;
    public final /* synthetic */ C53182d3 A01;
    public final /* synthetic */ String A02;

    public AnonymousClass2YU(TextEmojiLabel textEmojiLabel, C53182d3 r2, String str) {
        this.A01 = r2;
        this.A00 = textEmojiLabel;
        this.A02 = str;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        this.A01.A0D = false;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        TextEmojiLabel textEmojiLabel = this.A00;
        textEmojiLabel.A0G(null, this.A02);
        textEmojiLabel.setVisibility(0);
        textEmojiLabel.setScaleX(0.0f);
        textEmojiLabel.setScaleY(0.0f);
    }
}
