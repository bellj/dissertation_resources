package X;

import java.util.List;
import java.util.Map;

/* renamed from: X.52C  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass52C implements AnonymousClass5T8 {
    @Override // X.AnonymousClass5T8
    public Object AJ9(AbstractC111885Be r4, C94394bk r5, Object obj, String str, List list) {
        int AKQ;
        if (list != null && list.size() > 0) {
            C83143wl r1 = ((C95264dP) list.get(0)).A02.A00;
            boolean z = r1.A01 instanceof C83133wk;
            AnonymousClass4YR r12 = r1;
            if (!z) {
                while (true) {
                    AnonymousClass4YR r13 = r12.A01;
                    if (r13 == null) {
                        break;
                    }
                    AnonymousClass4YR r0 = r13.A01;
                    r12 = r13;
                    if (r0 == null) {
                        r13.A01 = new C83113wi();
                        break;
                    }
                }
            }
            C92574Wl r02 = ((C95264dP) list.get(0)).A02;
            C92364Vp r14 = r5.A01;
            Object A00 = r02.A00(r14, obj, obj).A00();
            AbstractC117035Xw r15 = r14.A00;
            if (A00 instanceof List) {
                AKQ = r15.AKQ(A00);
                return Integer.valueOf(AKQ);
            }
        }
        AbstractC117035Xw r16 = r5.A01.A00;
        if (!(obj instanceof List) && !(obj instanceof Map)) {
            return null;
        }
        AKQ = r16.AKQ(obj);
        return Integer.valueOf(AKQ);
    }
}
