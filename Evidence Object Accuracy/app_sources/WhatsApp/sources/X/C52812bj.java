package X;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.location.PlaceInfo;
import java.util.List;

/* renamed from: X.2bj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52812bj extends BaseAdapter {
    public PlaceInfo A00;
    public List A01;
    public final Context A02;
    public final C38721ob A03;
    public final boolean A04;

    @Override // android.widget.Adapter
    public long getItemId(int i) {
        return 0;
    }

    @Override // android.widget.BaseAdapter, android.widget.Adapter
    public int getItemViewType(int i) {
        return 0;
    }

    @Override // android.widget.BaseAdapter, android.widget.Adapter
    public int getViewTypeCount() {
        return 1;
    }

    public C52812bj(Context context, C38721ob r2, boolean z) {
        this.A02 = context;
        this.A03 = r2;
        this.A04 = z;
    }

    @Override // android.widget.Adapter
    public int getCount() {
        return C12970iu.A09(this.A01);
    }

    @Override // android.widget.Adapter
    public Object getItem(int i) {
        List list = this.A01;
        if (list == null || i >= list.size()) {
            return null;
        }
        return this.A01.get(i);
    }

    @Override // android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(this.A02).inflate(R.layout.location_picker_row, (ViewGroup) null);
        }
        TextView A0J = C12960it.A0J(view, R.id.location_name);
        TextView A0J2 = C12960it.A0J(view, R.id.location_description);
        ImageView A0L = C12970iu.A0L(view, R.id.location_icon);
        if (this.A04) {
            C016307r.A00(AnonymousClass00T.A03(this.A02, R.color.primary_surface), A0L);
        }
        List list = this.A01;
        if (list != null && i < list.size()) {
            PlaceInfo placeInfo = (PlaceInfo) this.A01.get(i);
            A0J.setText(placeInfo.A06);
            int i2 = 0;
            if (!TextUtils.isEmpty(placeInfo.A0B)) {
                A0J2.setVisibility(0);
                A0J2.setText(placeInfo.A0B);
                A0J2.setSingleLine(true);
            } else {
                A0J2.setVisibility(8);
            }
            if (placeInfo == this.A00) {
                A0J.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.pin_location_red, 0);
            } else {
                A0J.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }
            if (placeInfo.A03 == 3) {
                i2 = this.A02.getResources().getDimensionPixelSize(R.dimen.place_icon_padding_fousquare);
            }
            A0L.setPadding(i2, i2, i2, i2);
            String str = placeInfo.A05;
            if (str != null) {
                this.A03.A01(A0L, str);
            } else {
                A0L.setImageDrawable(null);
                return view;
            }
        }
        return view;
    }
}
