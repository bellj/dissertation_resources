package X;

import android.database.Cursor;
import android.text.TextUtils;
import com.whatsapp.util.Log;

/* renamed from: X.0tZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19100tZ extends AbstractC18500sY implements AbstractC19010tQ {
    public final C20530vu A00;
    public final C20560vx A01;
    public final C20280vV A02;
    public final C20060v9 A03;
    public final C20090vC A04;
    public final C20120vF A05;
    public final C20130vG A06;
    public final C20500vr A07;
    public final C20570vy A08;
    public final C20540vv A09;
    public final C20520vt A0A;
    public final C20490vq A0B;
    public final C20510vs A0C;
    public final C20550vw A0D;

    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AM5() {
    }

    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AND() {
    }

    public C19100tZ(C20530vu r4, C20560vx r5, C20280vV r6, C20060v9 r7, C20090vC r8, C20120vF r9, C20130vG r10, C20500vr r11, C20570vy r12, C20540vv r13, C20520vt r14, C20490vq r15, C20510vs r16, C20550vw r17, C18480sW r18) {
        super(r18, "message_quoted", 2);
        this.A04 = r8;
        this.A0B = r15;
        this.A07 = r11;
        this.A0A = r14;
        this.A0C = r16;
        this.A00 = r4;
        this.A09 = r13;
        this.A06 = r10;
        this.A03 = r7;
        this.A05 = r9;
        this.A0D = r17;
        this.A02 = r6;
        this.A01 = r5;
        this.A08 = r12;
    }

    @Override // X.AbstractC18500sY
    public AnonymousClass2Ez A09(Cursor cursor) {
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow("parent_row_id");
        int columnIndexOrThrow2 = cursor.getColumnIndexOrThrow("parent_key_remote_jid");
        int columnIndexOrThrow3 = cursor.getColumnIndexOrThrow("_id");
        C16310on A02 = super.A05.A02();
        long j = -1;
        int i = 0;
        int i2 = 0;
        while (cursor.moveToNext()) {
            try {
                j = cursor.getLong(columnIndexOrThrow);
                if (cursor.isNull(columnIndexOrThrow3) || cursor.getLong(columnIndexOrThrow3) <= 0) {
                    i2++;
                } else {
                    AbstractC14640lm A01 = AbstractC14640lm.A01(cursor.getString(columnIndexOrThrow2));
                    if (A01 == null) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("QuotedMessageStore/QuotedMessageDatabaseMigration/processBatch/missing chatJid; rowId=");
                        sb.append(j);
                        Log.e(sb.toString());
                    } else {
                        AbstractC15340mz A012 = this.A04.A01(cursor);
                        if (A012 == null) {
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("QuotedMessageStore/QuotedMessageDatabaseMigration/processBatch/missing quotedMessage; rowId=");
                            sb2.append(j);
                            Log.e(sb2.toString());
                        } else {
                            A012.A0Z(2);
                            try {
                                C20520vt r0 = this.A0A;
                                r0.A04(A012, j);
                                AnonymousClass009.A05(A01);
                                A02.A03.A05(r0.A00(A01, A012, j), "message_quoted");
                                if (A012.A0G() != null) {
                                    A012.A0G().A01(A012.A0G().A06());
                                }
                                if (A012 instanceof AnonymousClass1XV) {
                                    this.A09.A01((AnonymousClass1XV) A012, j);
                                }
                                if (A012 instanceof AnonymousClass1XJ) {
                                    this.A00.A01((AnonymousClass1XJ) A012, j);
                                }
                                if (A012 instanceof C28581Od) {
                                    this.A01.A05((C28581Od) A012, j);
                                }
                                if (A012 instanceof AnonymousClass1XF) {
                                    this.A07.A02((AnonymousClass1XF) A012, j);
                                }
                                if (A012 instanceof AbstractC16390ow) {
                                    this.A02.A0F((AbstractC16390ow) A012, j);
                                }
                                if (A012.A0y()) {
                                    this.A02.A07(A012.A0F().A00, "message_quoted_ui_elements", j);
                                }
                                if (A012 instanceof AnonymousClass1XE) {
                                    this.A02.A0D((AnonymousClass1XE) A012, "message_quoted_ui_elements_reply", j);
                                }
                                if (A012 instanceof AnonymousClass1XD) {
                                    this.A02.A0A((AnonymousClass1XD) A012, "message_quoted_ui_elements_reply", j);
                                }
                                if (A012 instanceof AnonymousClass1XP) {
                                    this.A03.A01((AnonymousClass1XP) A012, j);
                                }
                                if (A012 instanceof AbstractC16130oV) {
                                    this.A05.A09((AbstractC16130oV) A012, j);
                                }
                                if (A012.A0z()) {
                                    this.A06.A01(A012, j);
                                }
                                if (A012 instanceof C30411Xh) {
                                    C20510vs r11 = this.A0C;
                                    String A14 = ((C30411Xh) A012).A14();
                                    if (!TextUtils.isEmpty(A14)) {
                                        r11.A06(A14, j);
                                    }
                                } else if (A012 instanceof C30351Xb) {
                                    this.A0C.A04((C30351Xb) A012, j);
                                }
                                if (A012 instanceof C28861Ph) {
                                    this.A0B.A00(A012, j, false);
                                }
                                if (A012 instanceof AnonymousClass1XC) {
                                    this.A08.A00((AnonymousClass1XC) A012, "SELECT message_row_id, service, expiration_timestamp FROM message_quoted_payment_invite WHERE message_row_id = ?");
                                }
                                if (A012 instanceof C30241Wq) {
                                    this.A0D.A00((C30241Wq) A012, j);
                                }
                                i++;
                            } catch (IllegalArgumentException | IllegalStateException unused) {
                                StringBuilder sb3 = new StringBuilder();
                                sb3.append("QuotedMessageStore/QuotedMessageDatabaseMigration/processBatch/missing information, skipping; rowId=");
                                sb3.append(j);
                                Log.e(sb3.toString());
                            }
                        }
                    }
                }
            } catch (Throwable th) {
                try {
                    A02.close();
                } catch (Throwable unused2) {
                }
                throw th;
            }
        }
        A02.close();
        return new AnonymousClass2Ez(i, j, i2);
    }

    @Override // X.AbstractC19010tQ
    public void onRollback() {
        C16310on A02 = super.A05.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            C16330op r3 = A02.A03;
            r3.A01("message_quoted", null, null);
            r3.A01("message_quoted_location", null, null);
            r3.A01("message_quoted_media", null, null);
            r3.A01("message_quoted_mentions", null, null);
            r3.A01("message_quoted_vcard", null, null);
            r3.A01("message_quoted_text", null, null);
            r3.A01("message_quoted_group_invite", null, null);
            r3.A01("message_quoted_product", null, null);
            r3.A01("message_quoted_order", null, null);
            r3.A01("message_quoted_ui_elements", null, null);
            r3.A01("message_quoted_ui_elements_reply", null, null);
            r3.A0B("DELETE FROM message_quoted_ui_elements_reply_legacy");
            C21390xL r1 = super.A06;
            r1.A03("quoted_message_ready");
            r1.A03("migration_message_quoted_index");
            r1.A03("migration_message_quoted_retry");
            A00.A00();
            A00.close();
            A02.close();
            Log.i("QuotedMessageStore/resetDatabaseMigration/done");
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
