package X;

import android.database.Cursor;
import com.whatsapp.stickers.WebpUtils;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileNotFoundException;

/* renamed from: X.12b  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C235412b {
    public final C002701f A00;
    public final AnonymousClass15F A01;
    public final C28101Kq A02;
    public final AnonymousClass15H A03;
    public final AnonymousClass15G A04;
    public volatile boolean A05 = false;

    public C235412b(C002701f r2, AnonymousClass15F r3, AnonymousClass15H r4, AnonymousClass15G r5) {
        this.A03 = r4;
        this.A01 = r3;
        this.A04 = r5;
        this.A00 = r2;
        this.A02 = new C28101Kq();
    }

    public final void A00() {
        if (!this.A05) {
            C28101Kq r2 = this.A02;
            synchronized (r2) {
                if (!this.A05) {
                    AnonymousClass15F r3 = this.A01;
                    for (C37421mM r4 : r3.A00(Integer.MAX_VALUE, 0)) {
                        if (r4.A01 == null) {
                            try {
                                AnonymousClass15G r0 = this.A04;
                                File A04 = r0.A00.A04(r4.A0A);
                                if (!A04.exists()) {
                                    throw new FileNotFoundException("StickerImageHashCalculator/getImageHash/could not get internally managed media file for sticker");
                                    break;
                                } else {
                                    r4.A01 = WebpUtils.A00(A04);
                                    r3.A01(r4);
                                }
                            } catch (FileNotFoundException e) {
                                Log.e("StarredStickers/initStickerDedupeMappings/could not get internally managed media file for sticker, dropping it from starred", e);
                                r3.A02(r4.A0A);
                            }
                        }
                        r2.A01(r4.A0A, r4.A01);
                    }
                    this.A05 = true;
                }
            }
        }
    }

    public boolean A01(String str) {
        boolean containsKey;
        AnonymousClass009.A00();
        if (this.A05) {
            C28101Kq r1 = this.A02;
            synchronized (r1) {
                containsKey = r1.A00.containsKey(str);
            }
            return containsKey;
        }
        boolean z = true;
        String[] strArr = {str};
        C16310on A01 = this.A01.A01.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT plaintext_hash, hash_of_image_part, timestamp, url, enc_hash, direct_path, mimetype, media_key, file_size, width, height, emojis, is_first_party, is_avatar FROM starred_stickers WHERE plaintext_hash = ? ORDER BY timestamp DESC", strArr);
            if (A09.getCount() <= 0) {
                z = false;
            }
            A09.close();
            A01.close();
            return z;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
