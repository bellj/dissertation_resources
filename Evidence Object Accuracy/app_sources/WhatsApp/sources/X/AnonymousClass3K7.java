package X;

import android.content.Context;
import android.content.DialogInterface;
import com.facebook.redex.RunnableBRunnable0Shape1S0200000_I0_1;
import com.whatsapp.R;
import com.whatsapp.calling.spam.CallSpamActivity;
import com.whatsapp.util.Log;

/* renamed from: X.3K7  reason: invalid class name */
/* loaded from: classes2.dex */
public final /* synthetic */ class AnonymousClass3K7 implements DialogInterface.OnClickListener {
    public final /* synthetic */ CallSpamActivity.ReportSpamOrBlockDialogFragment A00;

    public /* synthetic */ AnonymousClass3K7(CallSpamActivity.ReportSpamOrBlockDialogFragment reportSpamOrBlockDialogFragment) {
        this.A00 = reportSpamOrBlockDialogFragment;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        CallSpamActivity.ReportSpamOrBlockDialogFragment reportSpamOrBlockDialogFragment = this.A00;
        if (!reportSpamOrBlockDialogFragment.A08.A0B()) {
            Log.w("callspamactivity/spam/report/no-network-cannot-block-report");
            boolean A03 = C18640sm.A03((Context) reportSpamOrBlockDialogFragment.A0C());
            int i2 = R.string.no_network_cannot_block;
            if (A03) {
                i2 = R.string.no_network_cannot_block_airplane;
            }
            reportSpamOrBlockDialogFragment.A02.A07(i2, 0);
            return;
        }
        reportSpamOrBlockDialogFragment.A02.A0C(null);
        reportSpamOrBlockDialogFragment.A0F.Ab2(new RunnableBRunnable0Shape1S0200000_I0_1(reportSpamOrBlockDialogFragment, 44, reportSpamOrBlockDialogFragment.A0C()));
    }
}
