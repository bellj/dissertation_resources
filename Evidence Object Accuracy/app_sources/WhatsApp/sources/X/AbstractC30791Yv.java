package X;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import java.math.BigDecimal;
import org.json.JSONObject;

/* renamed from: X.1Yv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public interface AbstractC30791Yv extends Parcelable {
    CharSequence AA7(Context context, String str);

    String AA8(AnonymousClass018 v, C30821Yy v2);

    String AA9(AnonymousClass018 v, BigDecimal bigDecimal);

    String AAA(AnonymousClass018 v, C30821Yy v2, int i);

    String AAB(AnonymousClass018 v, BigDecimal bigDecimal, int i);

    BigDecimal AAD(AnonymousClass018 v, String str);

    CharSequence ABy(Context context);

    CharSequence ABz(Context context, int i);

    String AC1(AnonymousClass018 v);

    C30821Yy AEA();

    C30821Yy AEV();

    int AH1(AnonymousClass018 v);

    void AcK(C30821Yy v);

    JSONObject Aew();

    @Override // android.os.Parcelable
    void writeToParcel(Parcel parcel, int i);
}
