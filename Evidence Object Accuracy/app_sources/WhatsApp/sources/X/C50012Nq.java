package X;

/* renamed from: X.2Nq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C50012Nq extends AnonymousClass1JW {
    public final String A00;
    public final String A01;
    public final String A02;
    public final String A03;
    public final String A04;
    public final byte[] A05;

    public C50012Nq(AbstractC15710nm r1, C15450nH r2, String str, String str2, String str3, String str4, String str5, byte[] bArr) {
        super(r1, r2);
        this.A04 = str;
        this.A01 = str2;
        this.A00 = str3;
        this.A02 = str4;
        this.A03 = str5;
        this.A05 = bArr;
    }
}
