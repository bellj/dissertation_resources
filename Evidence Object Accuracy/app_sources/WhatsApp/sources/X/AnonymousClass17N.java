package X;

import java.util.UUID;

/* renamed from: X.17N  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass17N {
    public String A00;

    public String A00() {
        String str = this.A00;
        if (str == null) {
            str = UUID.randomUUID().toString();
            this.A00 = str;
            if (str == null) {
                throw new IllegalArgumentException("Required value was null.");
            }
        }
        return str;
    }
}
