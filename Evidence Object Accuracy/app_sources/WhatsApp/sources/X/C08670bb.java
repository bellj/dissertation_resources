package X;

import java.io.Closeable;
import java.io.IOException;
import java.util.logging.Level;

/* renamed from: X.0bb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C08670bb implements AbstractC12240hb {
    @Override // X.AbstractC12240hb
    public void Aa4(Object obj) {
        Closeable closeable = (Closeable) obj;
        if (closeable != null) {
            try {
                try {
                    closeable.close();
                } catch (IOException e) {
                    AnonymousClass0RJ.A00.log(Level.WARNING, "IOException thrown while closing Closeable.", (Throwable) e);
                }
            } catch (IOException unused) {
            }
        }
    }
}
