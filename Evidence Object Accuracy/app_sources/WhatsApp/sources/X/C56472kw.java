package X;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.2kw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56472kw extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C98754jC();
    public int A00;
    public Bundle A01;
    public C56422kr A02;
    public C78603pB[] A03;

    public C56472kw(Bundle bundle, C56422kr r2, C78603pB[] r3, int i) {
        this.A01 = bundle;
        this.A03 = r3;
        this.A00 = i;
        this.A02 = r2;
    }

    public C56472kw() {
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A01 = C95654e8.A01(parcel);
        C95654e8.A03(this.A01, parcel, 1);
        C95654e8.A0I(parcel, this.A03, 2, i);
        C95654e8.A07(parcel, 3, this.A00);
        C95654e8.A0B(parcel, this.A02, 4, i, false);
        C95654e8.A06(parcel, A01);
    }
}
