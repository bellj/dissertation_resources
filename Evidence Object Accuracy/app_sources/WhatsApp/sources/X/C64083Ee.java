package X;

/* renamed from: X.3Ee  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C64083Ee {
    public final AnonymousClass1V8 A00;
    public final String A01;
    public final String A02;
    public final String A03;
    public final String A04;
    public final String A05;
    public final String A06;

    public C64083Ee(AnonymousClass1V8 r11) {
        AnonymousClass1V8.A01(r11, "state");
        Long A0j = C12970iu.A0j();
        Long A0k = C12970iu.A0k();
        this.A02 = (String) AnonymousClass3JT.A04(null, r11, String.class, A0j, A0k, null, new String[]{"name"}, false);
        this.A01 = (String) AnonymousClass3JT.A03(null, r11, String.class, A0j, A0k, null, new String[]{"input_path"}, false);
        this.A03 = (String) AnonymousClass3JT.A03(null, r11, String.class, A0j, A0k, null, new String[]{"next"}, false);
        this.A06 = (String) AnonymousClass3JT.A03(null, r11, String.class, A0j, A0k, null, new String[]{"result_selector"}, false);
        this.A05 = (String) AnonymousClass3JT.A03(null, r11, String.class, A0j, A0k, null, new String[]{"result_path"}, false);
        this.A04 = (String) AnonymousClass3JT.A03(null, r11, String.class, A0j, A0k, null, new String[]{"output_path"}, false);
        this.A00 = r11;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C64083Ee.class != obj.getClass()) {
                return false;
            }
            C64083Ee r5 = (C64083Ee) obj;
            if (!this.A02.equals(r5.A02) || !C29941Vi.A00(this.A01, r5.A01) || !C29941Vi.A00(this.A03, r5.A03) || !C29941Vi.A00(this.A06, r5.A06) || !C29941Vi.A00(this.A05, r5.A05) || !C29941Vi.A00(this.A04, r5.A04)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] objArr = new Object[6];
        objArr[0] = this.A02;
        objArr[1] = this.A01;
        objArr[2] = this.A03;
        objArr[3] = this.A06;
        objArr[4] = this.A05;
        return C12980iv.A0B(this.A04, objArr, 5);
    }
}
