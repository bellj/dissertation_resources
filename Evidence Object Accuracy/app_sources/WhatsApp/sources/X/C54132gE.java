package X;

import android.text.TextUtils;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import com.whatsapp.R;
import com.whatsapp.WaEditText;
import com.whatsapp.polls.PollCreatorViewModel;

/* renamed from: X.2gE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54132gE extends AnonymousClass02L {
    public int A00 = -1;
    public final C14850m9 A01;
    public final PollCreatorViewModel A02;

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        return i == 0 ? 0 : 1;
    }

    public C54132gE(AnonymousClass02K r2, C14850m9 r3, PollCreatorViewModel pollCreatorViewModel) {
        super(r2);
        this.A01 = r3;
        this.A02 = pollCreatorViewModel;
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r6, int i) {
        AbstractC75183jR r62 = (AbstractC75183jR) r6;
        if (r62 instanceof AnonymousClass349) {
            AnonymousClass349 r63 = (AnonymousClass349) r62;
            C861846a r4 = (C861846a) A0E(i);
            boolean A1V = C12960it.A1V(this.A00, i);
            r63.A00 = ((AbstractC89694Ky) r4).A00;
            WaEditText waEditText = r63.A01;
            if (waEditText.getText() != null && !TextUtils.equals(C12970iu.A0p(waEditText), r4.A00)) {
                waEditText.setText(r4.A00);
            }
            r63.A02.setVisibility(C12990iw.A0A(A1V));
            if (A1V) {
                waEditText.requestFocus();
                ((InputMethodManager) waEditText.getContext().getSystemService("input_method")).showSoftInput(waEditText, 1);
                if (waEditText.getText() != null) {
                    waEditText.setSelection(waEditText.getText().length());
                }
            }
        } else if (r62 instanceof AnonymousClass348) {
            ((AnonymousClass348) r62).A00.setText(((C861946b) A0E(i)).A00);
        }
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        if (i == 0) {
            return new AnonymousClass348(C12960it.A0E(viewGroup).inflate(R.layout.poll_creator_title, viewGroup, false), this.A01, this.A02);
        }
        if (i == 1) {
            return new AnonymousClass349(C12960it.A0E(viewGroup).inflate(R.layout.poll_creator_option, viewGroup, false), this.A01, this.A02);
        }
        throw C12960it.A0U(C12960it.A0W(i, "Unrecognized view type = "));
    }
}
