package X;

import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* renamed from: X.5hE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C121085hE extends AnonymousClass6BG {
    public final C14900mE A00;
    public final C15570nT A01;
    public final AnonymousClass102 A02;
    public final C14850m9 A03;
    public final C248217a A04;
    public final AbstractC17860rW A05;
    public final C22710zW A06;
    public final C130155yt A07;
    public final AnonymousClass600 A08;
    public final C130125yq A09;
    public final AnonymousClass61F A0A;
    public final C130105yo A0B;
    public final C1329468w A0C;
    public final C128745wb A0D;
    public final C129105xB A0E;
    public final C125075qe A0F;
    public final C126105sL A0G;
    public final C22980zx A0H;
    public final AbstractC14440lR A0I;

    @Override // X.AbstractC16830pp
    public int AGi() {
        return 3;
    }

    public C121085hE(C14900mE r9, C15570nT r10, C15550nR r11, C15610nY r12, C16590pI r13, AnonymousClass102 r14, C14850m9 r15, C248217a r16, AbstractC17860rW r17, C22710zW r18, C17070qD r19, C130155yt r20, AnonymousClass600 r21, C130125yq r22, AnonymousClass61F r23, C130105yo r24, C1329468w r25, C128745wb r26, C129105xB r27, C125075qe r28, C126105sL r29, AnonymousClass14X r30, C22980zx r31, AbstractC14440lR r32) {
        super(r11, r12, r13, r19, r30, "NOVI");
        this.A03 = r15;
        this.A00 = r9;
        this.A01 = r10;
        this.A0I = r32;
        this.A08 = r21;
        this.A0G = r29;
        this.A07 = r20;
        this.A0A = r23;
        this.A0H = r31;
        this.A0D = r26;
        this.A0F = r28;
        this.A09 = r22;
        this.A0B = r24;
        this.A0E = r27;
        this.A06 = r18;
        this.A02 = r14;
        this.A05 = r17;
        this.A04 = r16;
        this.A0C = r25;
    }

    @Override // X.AnonymousClass6BG, X.AbstractC16830pp
    public List AF1(AnonymousClass1IR r7, AnonymousClass1IS r8) {
        C119825fA r1;
        C1316663q r0;
        AbstractC30891Zf r12 = r7.A0A;
        if (!(r12 instanceof C119825fA) || (r0 = (r1 = (C119825fA) r12).A02) == null || !r0.A02()) {
            return null;
        }
        ArrayList A0l = C12960it.A0l();
        AnonymousClass1W9[] r4 = new AnonymousClass1W9[3];
        C1316663q r3 = r1.A02;
        C12960it.A1M("entry_flow", r3.A03, r4, 0);
        C12960it.A1M("metadata", r3.A04, r4, 1);
        C12960it.A1M("type", "MANUAL_REVIEW__AUTO_TRIGGERED", r4, 2);
        A0l.add(new AnonymousClass1V8("step_up", r4));
        return A0l;
    }

    @Override // X.AbstractC16830pp
    public C47822Cw AHU(AnonymousClass1ZO r6, UserJid userJid, String str) {
        Object obj;
        if (!this.A03.A07(860)) {
            return null;
        }
        AnonymousClass61F r1 = this.A0A;
        if (!r1.A0E() || !r1.A0F() || this.A0B.A05() || userJid == null || !super.A01.A0a(userJid)) {
            return null;
        }
        HashMap A11 = C12970iu.A11();
        HashMap A112 = C12970iu.A11();
        if (r6 != null) {
            obj = C12990iw.A0l(r6.A07().A01, 2);
        } else {
            obj = "";
        }
        C12960it.A1J(obj, A11, 2);
        return new C47822Cw(null, userJid, A11, A112);
    }
}
