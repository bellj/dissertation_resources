package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* renamed from: X.09c  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C019309c extends BroadcastReceiver {
    public final /* synthetic */ AnonymousClass0Q2 A00;

    public C019309c(AnonymousClass0Q2 r1) {
        this.A00 = r1;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        this.A00.A04();
    }
}
