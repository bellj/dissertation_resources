package X;

import com.whatsapp.R;
import com.whatsapp.avatar.profilephoto.AvatarProfilePhotoImageView;

/* renamed from: X.5JJ  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5JJ extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ AvatarProfilePhotoImageView this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass5JJ(AvatarProfilePhotoImageView avatarProfilePhotoImageView) {
        super(0);
        this.this$0 = avatarProfilePhotoImageView;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        return Integer.valueOf(AnonymousClass00T.A00(this.this$0.getContext(), R.color.neutral_primary));
    }
}
