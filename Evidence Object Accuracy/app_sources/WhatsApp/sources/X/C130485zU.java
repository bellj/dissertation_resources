package X;

import com.whatsapp.jid.UserJid;
import java.util.ArrayList;

/* renamed from: X.5zU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C130485zU {
    public static final ArrayList A01;
    public final AnonymousClass1V8 A00;

    static {
        String[] strArr = new String[2];
        strArr[0] = "false";
        A01 = C12960it.A0m("true", strArr, 1);
    }

    public C130485zU(UserJid userJid, AnonymousClass3CS r8, String str) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy.A01(A0M, "xmlns", "w:pay");
        C41141sy A0O = C117295Zj.A0O(A0M);
        C41141sy.A01(A0O, "action", "upi-get-vpa");
        if (userJid == null) {
            AnonymousClass009.A07(String.format("Received null value for non-optional '%s'.", "account->user"));
        } else {
            A0O.A04(new AnonymousClass1W9(userJid, "user"));
        }
        ArrayList arrayList = A01;
        if (str != null) {
            A0O.A0A(str, "is_first_send", arrayList);
        }
        this.A00 = C117295Zj.A0I(A0O, A0M, r8);
    }
}
