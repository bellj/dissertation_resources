package X;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.SpannableStringBuilder;
import android.util.Base64;
import android.util.Pair;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

/* renamed from: X.4xu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107764xu implements AbstractC116805Wy {
    public final C95024cw A00;
    public final Map A01;
    public final Map A02;
    public final Map A03;
    public final long[] A04;

    public C107764xu(C95024cw r7, Map map, Map map2, Map map3) {
        this.A00 = r7;
        this.A03 = map2;
        this.A02 = map3;
        this.A01 = Collections.unmodifiableMap(map);
        TreeSet treeSet = new TreeSet();
        int i = 0;
        r7.A06(treeSet, false);
        long[] jArr = new long[treeSet.size()];
        Iterator it = treeSet.iterator();
        while (it.hasNext()) {
            jArr[i] = C12980iv.A0G(it.next());
            i++;
        }
        this.A04 = jArr;
    }

    @Override // X.AbstractC116805Wy
    public List ABx(long j) {
        C95024cw r10 = this.A00;
        Map map = this.A01;
        Map map2 = this.A03;
        Map map3 = this.A02;
        ArrayList A0l = C12960it.A0l();
        String str = r10.A06;
        r10.A03(str, A0l, j);
        TreeMap treeMap = new TreeMap();
        r10.A04(str, treeMap, j, false);
        r10.A05(str, map, map2, treeMap, j);
        ArrayList A0l2 = C12960it.A0l();
        Iterator it = A0l.iterator();
        while (it.hasNext()) {
            Pair pair = (Pair) it.next();
            String str2 = (String) map3.get(pair.second);
            if (str2 != null) {
                byte[] decode = Base64.decode(str2, 0);
                Bitmap decodeByteArray = BitmapFactory.decodeByteArray(decode, 0, decode.length);
                C91974Ty r5 = (C91974Ty) map2.get(pair.first);
                C94144bK r1 = new C94144bK();
                r1.A0C = decodeByteArray;
                r1.A02 = r5.A02;
                r1.A08 = 0;
                r1.A01 = r5.A01;
                r1.A07 = 0;
                r1.A06 = r5.A05;
                r1.A04 = r5.A04;
                r1.A00 = r5.A00;
                r1.A0A = r5.A08;
                A0l2.add(r1.A00());
            }
        }
        Iterator A0s = C12990iw.A0s(treeMap);
        while (A0s.hasNext()) {
            Map.Entry A15 = C12970iu.A15(A0s);
            C91974Ty r52 = (C91974Ty) map2.get(A15.getKey());
            C94144bK r4 = (C94144bK) A15.getValue();
            SpannableStringBuilder spannableStringBuilder = (SpannableStringBuilder) r4.A0E;
            AnonymousClass4DL[] r11 = (AnonymousClass4DL[]) spannableStringBuilder.getSpans(0, spannableStringBuilder.length(), AnonymousClass4DL.class);
            for (AnonymousClass4DL r0 : r11) {
                spannableStringBuilder.replace(spannableStringBuilder.getSpanStart(r0), spannableStringBuilder.getSpanEnd(r0), (CharSequence) "");
            }
            for (int i = 0; i < spannableStringBuilder.length(); i++) {
                if (spannableStringBuilder.charAt(i) == ' ') {
                    int i2 = i + 1;
                    int i3 = i2;
                    while (i3 < spannableStringBuilder.length() && spannableStringBuilder.charAt(i3) == ' ') {
                        i3++;
                    }
                    int i4 = i3 - i2;
                    if (i4 > 0) {
                        spannableStringBuilder.delete(i, i4 + i);
                    }
                }
            }
            if (spannableStringBuilder.length() > 0 && spannableStringBuilder.charAt(0) == ' ') {
                spannableStringBuilder.delete(0, 1);
            }
            for (int i5 = 0; i5 < spannableStringBuilder.length() - 1; i5++) {
                if (spannableStringBuilder.charAt(i5) == '\n') {
                    int i6 = i5 + 1;
                    if (spannableStringBuilder.charAt(i6) == ' ') {
                        spannableStringBuilder.delete(i6, i5 + 2);
                    }
                }
            }
            if (spannableStringBuilder.length() > 0 && spannableStringBuilder.charAt(spannableStringBuilder.length() - 1) == ' ') {
                spannableStringBuilder.delete(spannableStringBuilder.length() - 1, spannableStringBuilder.length());
            }
            for (int i7 = 0; i7 < spannableStringBuilder.length() - 1; i7++) {
                if (spannableStringBuilder.charAt(i7) == ' ') {
                    int i8 = i7 + 1;
                    if (spannableStringBuilder.charAt(i8) == '\n') {
                        spannableStringBuilder.delete(i7, i8);
                    }
                }
            }
            if (spannableStringBuilder.length() > 0 && spannableStringBuilder.charAt(spannableStringBuilder.length() - 1) == '\n') {
                spannableStringBuilder.delete(spannableStringBuilder.length() - 1, spannableStringBuilder.length());
            }
            float f = r52.A01;
            int i9 = r52.A06;
            r4.A01 = f;
            r4.A07 = i9;
            r4.A06 = r52.A05;
            r4.A02 = r52.A02;
            r4.A04 = r52.A04;
            float f2 = r52.A03;
            int i10 = r52.A07;
            r4.A05 = f2;
            r4.A09 = i10;
            r4.A0A = r52.A08;
            A0l2.add(r4.A00());
        }
        return A0l2;
    }

    @Override // X.AbstractC116805Wy
    public long ACn(int i) {
        return this.A04[i];
    }

    @Override // X.AbstractC116805Wy
    public int ACo() {
        return this.A04.length;
    }

    @Override // X.AbstractC116805Wy
    public int AEd(long j) {
        long[] jArr = this.A04;
        int A05 = AnonymousClass3JZ.A05(jArr, j, false);
        if (A05 >= jArr.length) {
            return -1;
        }
        return A05;
    }
}
