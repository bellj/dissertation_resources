package X;

import android.view.View;

/* renamed from: X.2VY  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2VY extends AnonymousClass04v {
    public final /* synthetic */ AnonymousClass2VX[] A00;

    public AnonymousClass2VY(AnonymousClass2VX[] r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r9) {
        super.A06(view, r9);
        AnonymousClass2VX[] r6 = this.A00;
        for (AnonymousClass2VX r3 : r6) {
            r9.A09(new C007804a(r3.A00, view.getContext().getString(r3.A01)));
        }
    }
}
