package X;

import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.2h1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54622h1 extends AbstractC018308n {
    public int A00;
    public AnonymousClass4LI A01;

    public C54622h1(AnonymousClass4LI r1, int i) {
        this.A01 = r1;
        this.A00 = i;
    }

    @Override // X.AbstractC018308n
    public void A01(Rect rect, View view, C05480Ps r9, RecyclerView recyclerView) {
        AnonymousClass02M r0;
        AbstractC69213Yj r4;
        int i;
        int A00 = RecyclerView.A00(view);
        if (A00 >= 0 && (r0 = recyclerView.A0N) != null && A00 <= r0.A0D() && (i = (r4 = this.A01.A00).A00) > 0) {
            int i2 = A00 % i;
            int width = (recyclerView.getWidth() - (r4.A07 * i)) / (i + 1);
            rect.left = width - ((i2 * width) / i);
            rect.right = ((i2 + 1) * width) / i;
            if (A00 < r4.A00) {
                rect.top = this.A00;
            }
            rect.bottom = this.A00;
        }
    }
}
