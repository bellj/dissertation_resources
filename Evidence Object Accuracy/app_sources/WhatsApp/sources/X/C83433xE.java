package X;

import android.view.animation.Animation;

/* renamed from: X.3xE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83433xE extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ C60212wE A00;

    public C83433xE(C60212wE r1) {
        this.A00 = r1;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationRepeat(Animation animation) {
        onAnimationStart(animation);
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationStart(Animation animation) {
        C60212wE r3 = this.A00;
        C60212wE.A00(r3.A01, 0.2f, 0.5f);
        C60212wE.A00(r3.A02, 0.25f, 0.75f);
    }
}
