package X;

import android.content.Intent;

/* renamed from: X.66l  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1323266l implements AbstractC42541vN {
    public final /* synthetic */ int A00 = 1;
    public final /* synthetic */ ActivityC13790kL A01;
    public final /* synthetic */ AnonymousClass3CY A02;
    public final /* synthetic */ AnonymousClass67N A03;

    public C1323266l(ActivityC13790kL r2, AnonymousClass3CY r3, AnonymousClass67N r4) {
        this.A03 = r4;
        this.A01 = r2;
        this.A02 = r3;
    }

    @Override // X.AbstractC42541vN
    public boolean ALt(Intent intent, int i, int i2) {
        EnumC868449c r0;
        this.A01.A2Z(this);
        if (i != this.A00) {
            return false;
        }
        AnonymousClass3CY r1 = this.A02;
        if (i2 == -1) {
            r0 = EnumC868449c.OK;
        } else if (i2 != 0) {
            r0 = EnumC868449c.ERROR;
        } else {
            r0 = EnumC868449c.CANCELLED;
        }
        r1.A00(r0);
        return true;
    }
}
