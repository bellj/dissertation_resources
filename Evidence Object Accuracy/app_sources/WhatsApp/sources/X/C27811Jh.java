package X;

import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONException;

/* renamed from: X.1Jh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C27811Jh {
    public final int A00;
    public final C27791Jf A01;
    public final AnonymousClass1JR A02;
    public final C27831Jk A03;
    public final String A04;
    public final byte[] A05;
    public final String[] A06;

    public C27811Jh(AnonymousClass1JQ r2) {
        this.A00 = r2.A03;
        this.A04 = AnonymousClass1JQ.A00(r2.A06());
        this.A02 = r2.A00;
        this.A06 = r2.A06();
        this.A01 = r2.A05;
        this.A03 = r2.A02();
        this.A05 = r2.A02;
    }

    public C27811Jh(C27791Jf r1, AnonymousClass1JR r2, C27831Jk r3, String str, byte[] bArr, String[] strArr, int i) {
        this.A04 = str;
        this.A00 = i;
        this.A02 = r2;
        this.A06 = strArr;
        this.A01 = r1;
        this.A03 = r3;
        this.A05 = bArr;
    }

    public C27811Jh(C27791Jf r2, AnonymousClass1JR r3, String str, byte[] bArr, byte[] bArr2, int i) {
        C27831Jk r0;
        this.A00 = i;
        this.A04 = str;
        this.A02 = r3;
        this.A06 = A01(str);
        this.A01 = r2;
        if (bArr != null) {
            r0 = (C27831Jk) AbstractC27091Fz.A0E(C27831Jk.A0R, bArr);
        } else {
            r0 = null;
        }
        this.A03 = r0;
        this.A05 = bArr2;
    }

    public static String A00(String str) {
        try {
            return new JSONArray(str).getString(0);
        } catch (JSONException unused) {
            throw new IllegalArgumentException("SyncMutationData/getValidMutationName: corrupt index");
        }
    }

    public static String[] A01(String str) {
        JSONArray jSONArray = new JSONArray(str);
        int length = jSONArray.length();
        String[] strArr = new String[length];
        for (int i = 0; i < jSONArray.length(); i++) {
            strArr[i] = jSONArray.getString(i);
            if (strArr[i].isEmpty()) {
                StringBuilder sb = new StringBuilder("SyncMutationData/getValidKeyArrayIndex: The key is empty in the keyArray at position: ");
                sb.append(i);
                throw new IllegalArgumentException(sb.toString());
            }
        }
        if (length > 0) {
            return strArr;
        }
        throw new IllegalArgumentException("SyncMutationData/getValidKeyArrayIndex: keyArray length should have action name");
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C27811Jh)) {
            return false;
        }
        C27811Jh r4 = (C27811Jh) obj;
        if (!this.A04.equals(r4.A04) || !C29941Vi.A00(this.A03, r4.A03) || !this.A01.equals(r4.A01)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A04, this.A03, this.A01});
    }

    public String toString() {
        return "SyncMutationData";
    }
}
