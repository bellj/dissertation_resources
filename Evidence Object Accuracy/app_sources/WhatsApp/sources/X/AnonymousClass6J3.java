package X;

/* renamed from: X.6J3  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6J3 implements Runnable {
    public final /* synthetic */ C129455xk A00;
    public final /* synthetic */ AnonymousClass60X A01;
    public final /* synthetic */ AnonymousClass60J A02;

    public AnonymousClass6J3(C129455xk r1, AnonymousClass60X r2, AnonymousClass60J r3) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
    }

    @Override // java.lang.Runnable
    public void run() {
        C129455xk r4 = this.A00;
        AnonymousClass60J r3 = this.A02;
        AnonymousClass643.A00(r4.A00, new Object[]{r4.A01, r3.A01(AnonymousClass60J.A0Z), r3}, 6);
    }
}
