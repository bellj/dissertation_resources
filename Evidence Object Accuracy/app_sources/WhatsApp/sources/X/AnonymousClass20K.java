package X;

/* renamed from: X.20K  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass20K implements AnonymousClass20L {
    public byte[] A00;

    public AnonymousClass20K(byte[] bArr) {
        this(bArr, bArr.length);
    }

    public AnonymousClass20K(byte[] bArr, int i) {
        byte[] bArr2 = new byte[i];
        this.A00 = bArr2;
        System.arraycopy(bArr, 0, bArr2, 0, i);
    }
}
