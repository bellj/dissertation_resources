package X;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;

/* renamed from: X.5E4  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5E4 implements Executor {
    public final Handler A00 = new HandlerC73373g8(Looper.getMainLooper());

    @Override // java.util.concurrent.Executor
    public final void execute(Runnable runnable) {
        this.A00.post(runnable);
    }
}
