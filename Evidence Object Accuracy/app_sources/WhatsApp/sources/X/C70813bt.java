package X;

/* renamed from: X.3bt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70813bt implements AnonymousClass5V4 {
    public final AnonymousClass1IS A00;
    public final /* synthetic */ AnonymousClass2I3 A01;

    public C70813bt(AnonymousClass1IS r1, AnonymousClass2I3 r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5V4
    public void AWJ(AnonymousClass21T r7) {
        AnonymousClass1IS r1 = this.A00;
        AnonymousClass2I3 r5 = this.A01;
        if (r1 == r5.A0B && r5.A0E != null) {
            AnonymousClass39B r12 = (AnonymousClass39B) r5.A0D;
            AnonymousClass21T r0 = r12.A06;
            if (r0 != null) {
                r12.A0f.setText(AnonymousClass2Bd.A01(r12.A0j, r12.A0k, (long) r0.A02()));
            }
            AnonymousClass39B r2 = (AnonymousClass39B) r5.A0D;
            r2.A0J.setVisibility(8);
            r2.A0K.setVisibility(8);
            r5.A0D.A01();
            AnonymousClass39B r22 = (AnonymousClass39B) r5.A0D;
            r22.A0Y.setVisibility(0);
            r22.A0Z.setVisibility(0);
            r5.A08.requestFocus();
            C91824Tg r13 = r5.A0U;
            r13.A01 = true;
            r13.A05.A00();
            r13.A04.A01();
            r13.A06.A02();
        }
    }
}
