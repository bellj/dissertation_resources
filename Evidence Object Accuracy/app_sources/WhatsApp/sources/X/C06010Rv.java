package X;

/* renamed from: X.0Rv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C06010Rv {
    public static final C06010Rv A04;
    public static final C06010Rv A05;
    public static final C06010Rv A06;
    public static final C06010Rv A07;
    public static final C06010Rv A08;
    public static final C06010Rv A09;
    public boolean A00 = true;
    public final float[] A01;
    public final float[] A02;
    public final float[] A03;

    static {
        C06010Rv r3 = new C06010Rv();
        A07 = r3;
        float[] fArr = r3.A01;
        fArr[0] = 0.55f;
        fArr[1] = 0.74f;
        float[] fArr2 = r3.A02;
        fArr2[0] = 0.35f;
        fArr2[1] = 1.0f;
        C06010Rv r32 = new C06010Rv();
        A09 = r32;
        float[] fArr3 = r32.A01;
        fArr3[0] = 0.3f;
        fArr3[1] = 0.5f;
        fArr3[2] = 0.7f;
        float[] fArr4 = r32.A02;
        fArr4[0] = 0.35f;
        fArr4[1] = 1.0f;
        C06010Rv r33 = new C06010Rv();
        A05 = r33;
        float[] fArr5 = r33.A01;
        fArr5[1] = 0.26f;
        fArr5[2] = 0.45f;
        float[] fArr6 = r33.A02;
        fArr6[0] = 0.35f;
        fArr6[1] = 1.0f;
        C06010Rv r34 = new C06010Rv();
        A06 = r34;
        float[] fArr7 = r34.A01;
        fArr7[0] = 0.55f;
        fArr7[1] = 0.74f;
        float[] fArr8 = r34.A02;
        fArr8[1] = 0.3f;
        fArr8[2] = 0.4f;
        C06010Rv r35 = new C06010Rv();
        A08 = r35;
        float[] fArr9 = r35.A01;
        fArr9[0] = 0.3f;
        fArr9[1] = 0.5f;
        fArr9[2] = 0.7f;
        float[] fArr10 = r35.A02;
        fArr10[1] = 0.3f;
        fArr10[2] = 0.4f;
        C06010Rv r36 = new C06010Rv();
        A04 = r36;
        float[] fArr11 = r36.A01;
        fArr11[1] = 0.26f;
        fArr11[2] = 0.45f;
        float[] fArr12 = r36.A02;
        fArr12[1] = 0.3f;
        fArr12[2] = 0.4f;
    }

    public C06010Rv() {
        this.A02 = r5;
        this.A01 = r4;
        this.A03 = r2;
        float[] fArr = {0.0f, 0.5f, 1.0f};
        float[] fArr2 = {0.0f, 0.5f, 1.0f};
        float[] fArr3 = {0.24f, 0.52f, 0.24f};
    }
}
