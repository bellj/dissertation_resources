package X;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.facebook.redex.ViewOnClickCListenerShape10S0100000_I1_4;
import com.whatsapp.R;
import com.whatsapp.backup.google.GoogleDriveRestoreAnimationView;
import com.whatsapp.registration.RegisterName;
import com.whatsapp.util.Log;

/* renamed from: X.2Ff  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class DialogC48262Ff extends AnonymousClass27U {
    public int A00 = 0;
    public ProgressBar A01;
    public TextView A02;
    public GoogleDriveRestoreAnimationView A03;
    public final C15880o3 A04;
    public final C16490p7 A05;
    public final C18350sJ A06;
    public final /* synthetic */ RegisterName A07;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public DialogC48262Ff(Activity activity, AnonymousClass01d r9, C14830m7 r10, AnonymousClass018 r11, C15880o3 r12, C16490p7 r13, RegisterName registerName, C18350sJ r15) {
        super(activity, r9, r10, r11, R.layout.backup_restore);
        this.A07 = registerName;
        this.A04 = r12;
        this.A05 = r13;
        this.A06 = r15;
    }

    public void A00(int i) {
        this.A00 = i;
        if (i == 1) {
            if (this.A03 == null) {
                this.A03 = (GoogleDriveRestoreAnimationView) findViewById(R.id.restore_animation_view);
            }
            findViewById(R.id.restore_actions_view).setVisibility(8);
            findViewById(R.id.restore_animation_view).setVisibility(0);
            this.A01 = (ProgressBar) findViewById(R.id.progress);
            this.A02 = (TextView) findViewById(R.id.progress_info);
            this.A01.setVisibility(0);
            this.A01.setIndeterminate(true);
            C88134Ek.A00(this.A01, AnonymousClass00T.A00(getContext(), R.color.media_message_progress_determinate));
            this.A02.setVisibility(0);
            this.A03.A01();
        } else if (i == 2) {
            GoogleDriveRestoreAnimationView googleDriveRestoreAnimationView = this.A03;
            if (googleDriveRestoreAnimationView == null) {
                googleDriveRestoreAnimationView = (GoogleDriveRestoreAnimationView) findViewById(R.id.restore_animation_view);
                this.A03 = googleDriveRestoreAnimationView;
            }
            googleDriveRestoreAnimationView.A04(false);
            findViewById(R.id.restore_actions_view).setVisibility(8);
            ProgressBar progressBar = this.A01;
            if (progressBar != null) {
                progressBar.setVisibility(8);
            }
            TextView textView = this.A02;
            if (textView != null) {
                textView.setVisibility(8);
            }
            TextView textView2 = (TextView) findViewById(R.id.msgrestore_result_box);
            textView2.setVisibility(0);
            AnonymousClass018 r8 = super.A04;
            C16490p7 r2 = this.A05;
            String A0I = r8.A0I(new Object[]{Integer.valueOf(r2.A00())}, R.plurals.gdrive_messages_restored_with_no_media_to_restore, (long) r2.A00());
            StringBuilder sb = new StringBuilder("restorebackupdialog/after-msgstore-verified/ ");
            sb.append(A0I);
            Log.i(sb.toString());
            textView2.setText(A0I);
            findViewById(R.id.next_btn).setVisibility(0);
        }
    }

    @Override // X.AnonymousClass27U, android.app.Dialog
    public void onCreate(Bundle bundle) {
        int i;
        super.onCreate(bundle);
        C41691tw.A05(getContext(), getWindow(), R.color.lightStatusBarBackgroundColor);
        findViewById(R.id.perform_restore).setOnClickListener(new ViewOnClickCListenerShape10S0100000_I1_4(this, 29));
        findViewById(R.id.dont_restore).setOnClickListener(new ViewOnClickCListenerShape10S0100000_I1_4(this, 27));
        findViewById(R.id.next_btn).setOnClickListener(new ViewOnClickCListenerShape10S0100000_I1_4(this, 28));
        if (bundle == null) {
            i = 0;
        } else {
            i = bundle.getInt("state");
        }
        A00(i);
        Window window = getWindow();
        AnonymousClass009.A05(window);
        window.setSoftInputMode(3);
        setTitle(R.string.activity_google_drive_title);
        ((TextView) findViewById(R.id.restore_info)).setText(super.A01.getString(R.string.local_restore_info_calculating, C38131nZ.A01(super.A04, this.A04.A06()).toString()));
    }

    @Override // android.app.Dialog
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override // android.app.Dialog
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 0) {
            return super.onOptionsItemSelected(menuItem);
        }
        this.A06.A09();
        Activity activity = super.A01;
        activity.startActivity(C14960mK.A01(activity));
        return true;
    }

    @Override // android.app.Dialog
    public Bundle onSaveInstanceState() {
        Bundle onSaveInstanceState = super.onSaveInstanceState();
        onSaveInstanceState.putInt("state", this.A00);
        return onSaveInstanceState;
    }
}
