package X;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

/* renamed from: X.3tm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C81293tm extends AnonymousClass5I8<E> {
    public final /* synthetic */ Set val$set1;
    public final /* synthetic */ Set val$set2;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C81293tm(Set set, Set set2) {
        super(null);
        this.val$set1 = set;
        this.val$set2 = set2;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public boolean contains(Object obj) {
        return this.val$set1.contains(obj) && this.val$set2.contains(obj);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public boolean containsAll(Collection collection) {
        return this.val$set1.containsAll(collection) && this.val$set2.containsAll(collection);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public boolean isEmpty() {
        return Collections.disjoint(this.val$set2, this.val$set1);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
    public AnonymousClass1I5 iterator() {
        return new C80783sx(this);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public int size() {
        int i = 0;
        for (Object obj : this.val$set1) {
            if (this.val$set2.contains(obj)) {
                i++;
            }
        }
        return i;
    }
}
