package X;

import com.whatsapp.jid.UserJid;
import java.util.Map;

/* renamed from: X.5wW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128695wW {
    public String A00;
    public Map A01;

    public synchronized Map A00(UserJid userJid) {
        Map map;
        map = this.A01;
        if (map == null || !userJid.getRawString().equals(this.A00)) {
            this.A00 = userJid.getRawString();
            map = C12970iu.A11();
            this.A01 = map;
        }
        return map;
    }
}
