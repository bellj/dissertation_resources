package X;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.widget.ImageView;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;
import com.facebook.redex.ViewOnClickCListenerShape10S0100000_I1_4;
import com.whatsapp.PagerSlidingTabStrip;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.qrcode.contactqr.ContactQrContactCardView;
import com.whatsapp.qrcode.contactqr.ContactQrMyCodeFragment;
import com.whatsapp.qrcode.contactqr.ErrorDialogFragment;
import com.whatsapp.qrcode.contactqr.QrScanCodeFragment;
import com.whatsapp.util.Log;

/* renamed from: X.34P  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass34P extends AbstractActivityC83733xo implements AbstractC14730lx, AnonymousClass5RQ {
    public Uri A00;
    public ImageView A01;
    public ViewPager A02;
    public AnonymousClass17S A03;
    public C22260yn A04;
    public AnonymousClass18U A05;
    public PagerSlidingTabStrip A06;
    public C250918b A07;
    public C251118d A08;
    public C15550nR A09;
    public C26311Cv A0A;
    public C22700zV A0B;
    public C15610nY A0C;
    public C253318z A0D;
    public C17170qN A0E;
    public C15890o4 A0F;
    public AnonymousClass018 A0G;
    public C22610zM A0H;
    public C15680nj A0I;
    public C16120oU A0J;
    public C17220qS A0K;
    public C22410z2 A0L;
    public C22710zW A0M;
    public C17070qD A0N;
    public C63883Dh A0O;
    public AnonymousClass2JY A0P;
    public C53742f9 A0Q;
    public ContactQrMyCodeFragment A0R;
    public QrScanCodeFragment A0S;
    public C22190yg A0T;
    public String A0U;
    public boolean A0V;
    public boolean A0W = false;
    public boolean A0X;
    public boolean A0Y;
    public final AbstractC116105Ud A0Z = new AbstractC116105Ud() { // from class: X.3Zf
        @Override // X.AbstractC116105Ud
        public final void AVK(String str, int i) {
            AnonymousClass34P r4 = AnonymousClass34P.this;
            if (!r4.AJN()) {
                r4.A0X = false;
                r4.AaN();
                if (i != 0) {
                    if (i == 1) {
                        C51262Tn.A02(null, null, r4.A0J, null, null, 1, 3, C51262Tn.A03(str));
                    } else if (i != 2 || r4.A2h(str, false, 3)) {
                        return;
                    }
                    AnonymousClass2JY r0 = r4.A0P;
                    r0.A06.Adl(ErrorDialogFragment.A00(6), "qr_code_scanning_dialog_fragment_tag");
                } else {
                    C004802e A0S = C12980iv.A0S(r4);
                    C12970iu.A1I(A0S);
                    A0S.A06(R.string.error_load_image);
                    A0S.A04(
                    /*  JADX ERROR: Method code generation error
                        jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0055: INVOKE  
                          (r1v0 'A0S' X.02e)
                          (wrap: X.4hH : 0x0052: CONSTRUCTOR  (r0v2 X.4hH A[REMOVE]) = (r4v0 'r4' X.34P) call: X.4hH.<init>(X.34P):void type: CONSTRUCTOR)
                         type: VIRTUAL call: X.02e.A04(android.content.DialogInterface$OnDismissListener):X.02e in method: X.3Zf.AVK(java.lang.String, int):void, file: classes2.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                        	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                        	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:151)
                        	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                        	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                        	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                        	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                        	at java.util.ArrayList.forEach(ArrayList.java:1259)
                        	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                        	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                        Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0052: CONSTRUCTOR  (r0v2 X.4hH A[REMOVE]) = (r4v0 'r4' X.34P) call: X.4hH.<init>(X.34P):void type: CONSTRUCTOR in method: X.3Zf.AVK(java.lang.String, int):void, file: classes2.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                        	... 29 more
                        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.4hH, state: NOT_LOADED
                        	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                        	... 35 more
                        */
                    /*
                        this = this;
                        X.34P r4 = X.AnonymousClass34P.this
                        boolean r0 = r4.AJN()
                        if (r0 != 0) goto L_0x002f
                        r2 = 0
                        r4.A0X = r2
                        r4.AaN()
                        r3 = 1
                        if (r15 == 0) goto L_0x0043
                        r1 = 6
                        if (r15 == r3) goto L_0x0030
                        r0 = 2
                        if (r15 != r0) goto L_0x002f
                        r0 = 3
                        boolean r0 = r4.A2h(r14, r2, r0)
                        if (r0 != 0) goto L_0x002f
                    L_0x001e:
                        X.2JY r0 = r4.A0P
                        com.whatsapp.qrcode.contactqr.ErrorDialogFragment r2 = com.whatsapp.qrcode.contactqr.ErrorDialogFragment.A00(r1)
                        X.0kN r1 = r0.A06
                        java.lang.String r0 = "qr_code_scanning_dialog_fragment_tag"
                        r1.Adl(r2, r0)
                    L_0x002b:
                        X.2JY r0 = r4.A0P
                        r0.A0Z = r3
                    L_0x002f:
                        return
                    L_0x0030:
                        X.0oU r7 = r4.A0J
                        r11 = 3
                        java.lang.Integer r10 = java.lang.Integer.valueOf(r3)
                        r5 = 0
                        boolean r12 = X.C51262Tn.A03(r14)
                        r8 = r5
                        r9 = r5
                        r6 = r5
                        X.C51262Tn.A02(r5, r6, r7, r8, r9, r10, r11, r12)
                        goto L_0x001e
                    L_0x0043:
                        X.02e r1 = X.C12980iv.A0S(r4)
                        X.C12970iu.A1I(r1)
                        r0 = 2131888077(0x7f1207cd, float:1.941078E38)
                        r1.A06(r0)
                        X.4hH r0 = new X.4hH
                        r0.<init>(r4)
                        r1.A04(r0)
                        X.C12970iu.A1J(r1)
                        goto L_0x002b
                    */
                    throw new UnsupportedOperationException("Method not decompiled: X.C69433Zf.AVK(java.lang.String, int):void");
                }
            };

            public abstract void A2g(boolean z);

            public static void A02(AnonymousClass34P r7) {
                if (r7.A0S == null) {
                    return;
                }
                if (r7.A0F.A02("android.permission.CAMERA") != 0) {
                    C35751ig r5 = new C35751ig(r7);
                    r5.A01 = R.drawable.permission_cam;
                    int[] iArr = {R.string.localized_app_name};
                    r5.A02 = R.string.permission_cam_access_on_contact_qr_scan_request;
                    r5.A0A = iArr;
                    int[] iArr2 = {R.string.localized_app_name};
                    r5.A03 = R.string.permission_cam_access_on_contact_qr_scan;
                    r5.A08 = iArr2;
                    r5.A0C = new String[]{"android.permission.CAMERA"};
                    r5.A06 = true;
                    r7.startActivityForResult(r5.A00(), 1);
                    return;
                }
                r7.A0S.A1A();
            }

            @Override // X.ActivityC13810kN, X.ActivityC000900k
            public void A1T(AnonymousClass01E r4) {
                super.A1T(r4);
                if (r4 instanceof ContactQrMyCodeFragment) {
                    ContactQrMyCodeFragment contactQrMyCodeFragment = (ContactQrMyCodeFragment) r4;
                    this.A0R = contactQrMyCodeFragment;
                    String str = this.A0U;
                    if (str != null) {
                        contactQrMyCodeFragment.A02 = str;
                        ContactQrContactCardView contactQrContactCardView = contactQrMyCodeFragment.A01;
                        if (contactQrContactCardView != null) {
                            contactQrContactCardView.setQrCode(C12960it.A0d(str, C12960it.A0k("https://wa.me/qr/")));
                        }
                    }
                } else if (r4 instanceof QrScanCodeFragment) {
                    this.A0S = (QrScanCodeFragment) r4;
                    ViewPager viewPager = this.A02;
                    if (viewPager == null) {
                        Log.e("BaseQrActivity/onAttachFragment/viewPagerNull");
                    } else if (1 == viewPager.getCurrentItem()) {
                        A02(this);
                    }
                }
            }

            public void A2e() {
                boolean A01;
                C41691tw.A03(this, R.color.lightStatusBarBackgroundColor);
                setTitle(getString(R.string.contact_qr_title));
                setContentView(R.layout.contact_qr);
                Toolbar toolbar = (Toolbar) AnonymousClass00T.A05(this, R.id.toolbar);
                ActivityC13790kL.A0c(this, toolbar, this.A0G);
                toolbar.setTitle(getString(R.string.contact_qr_title));
                toolbar.setNavigationOnClickListener(new ViewOnClickCListenerShape10S0100000_I1_4(this, 17));
                A1e(toolbar);
                this.A0O = new C63883Dh();
                this.A02 = (ViewPager) AnonymousClass00T.A05(this, R.id.contact_qr_pager);
                this.A06 = (PagerSlidingTabStrip) AnonymousClass00T.A05(this, R.id.contact_qr_tab_strip);
                ImageView imageView = (ImageView) AnonymousClass00T.A05(this, R.id.contact_qr_preview);
                this.A01 = imageView;
                AnonymousClass028.A0a(imageView, 2);
                C14830m7 r0 = ((ActivityC13790kL) this).A05;
                C14850m9 r02 = ((ActivityC13810kN) this).A0C;
                C14900mE r03 = ((ActivityC13810kN) this).A05;
                C15570nT r04 = ((ActivityC13790kL) this).A01;
                AbstractC14440lR r05 = ((ActivityC13830kP) this).A05;
                C16120oU r06 = this.A0J;
                AnonymousClass17S r07 = this.A03;
                C15450nH r08 = ((ActivityC13810kN) this).A06;
                AnonymousClass18U r09 = this.A05;
                C17220qS r010 = this.A0K;
                C15550nR r011 = this.A09;
                AnonymousClass01d r012 = ((ActivityC13810kN) this).A08;
                C15610nY r013 = this.A0C;
                C22260yn r014 = this.A04;
                C17070qD r15 = this.A0N;
                C253318z r14 = this.A0D;
                C22700zV r13 = this.A0B;
                C15680nj r11 = this.A0I;
                C22710zW r10 = this.A0M;
                C22410z2 r9 = this.A0L;
                C251118d r8 = this.A08;
                C18640sm r7 = ((ActivityC13810kN) this).A07;
                C26311Cv r6 = this.A0A;
                C22610zM r5 = this.A0H;
                int i = 0;
                AnonymousClass2JY r015 = new AnonymousClass2JY(r07, r014, this, r03, r09, r04, r08, this.A07, r8, r011, r6, r13, r013, r14, r7, r012, r0, this.A0E, r5, r11, r02, r06, r010, r9, r10, r15, r05, C12970iu.A0h(), false, true);
                this.A0P = r015;
                r015.A02 = true;
                C53742f9 r3 = new C53742f9(A0V(), this);
                this.A0Q = r3;
                this.A02.setAdapter(r3);
                this.A02.A0G(new C55382iJ(this));
                AnonymousClass028.A0c(this.A06, 0);
                this.A06.setViewPager(this.A02);
                String stringExtra = getIntent().getStringExtra("qrcode");
                if (stringExtra != null) {
                    this.A0V = true;
                    A2h(stringExtra, false, 5);
                }
                if (!this.A0V) {
                    A2g(false);
                }
                boolean booleanExtra = getIntent().getBooleanExtra("scan", false);
                this.A0Y = booleanExtra;
                AnonymousClass018 r016 = this.A0G;
                if (booleanExtra) {
                    A01 = C28141Kv.A00(r016);
                } else {
                    A01 = C28141Kv.A01(r016);
                }
                int i2 = !A01 ? 1 : 0;
                this.A02.A0F(i2, false);
                C53742f9 r32 = this.A0Q;
                do {
                    AnonymousClass4OE r017 = r32.A00[i];
                    r017.A00.setSelected(C12960it.A1V(i, i2));
                    i++;
                } while (i < 2);
            }

            public void A2f() {
                if (!this.A0F.A07()) {
                    AnonymousClass009.A05(this);
                    int i = Build.VERSION.SDK_INT;
                    int i2 = R.string.permission_storage_need_write_access_on_sharing_v30;
                    if (i < 30) {
                        i2 = R.string.permission_storage_need_write_access_on_sharing;
                    }
                    A2E(RequestPermissionActivity.A03(this, R.string.permission_storage_need_write_access_on_sharing_request, i2, false), 4);
                } else if (this.A0U == null) {
                    Log.e("BaseQrActivity/shareFailed/noQr");
                    ((ActivityC13810kN) this).A05.A07(R.string.share_failed, 0);
                } else {
                    A2C(R.string.contact_qr_wait);
                    AbstractC14440lR r3 = ((ActivityC13830kP) this).A05;
                    C14900mE r11 = ((ActivityC13810kN) this).A05;
                    C15570nT r12 = ((ActivityC13790kL) this).A01;
                    boolean z = true;
                    C625337p r8 = new C625337p(this, ((ActivityC13810kN) this).A04, r11, r12, C12960it.A0X(this, C12960it.A0d(this.A0U, C12960it.A0k("https://wa.me/qr/")), new Object[1], 0, R.string.contact_qr_email_body_with_link));
                    Bitmap[] bitmapArr = new Bitmap[1];
                    C15570nT r0 = ((ActivityC13790kL) this).A01;
                    r0.A08();
                    C27621Ig r7 = r0.A01;
                    AnonymousClass009.A05(r7);
                    if (((ActivityC13810kN) this).A09.A00.getInt("privacy_profile_photo", 0) != 0) {
                        z = false;
                    }
                    bitmapArr[0] = new C63803Cz(r7, getString(R.string.contact_qr_share_prompt), C12960it.A0d(this.A0U, C12960it.A0k("https://wa.me/qr/")), z).A00(this);
                    r3.Aaz(r8, bitmapArr);
                }
            }

            public boolean A2h(String str, boolean z, int i) {
                if (this.A0P.A0Z || this.A0X) {
                    return false;
                }
                return this.A0P.A02(str, i, z, false);
            }

            @Override // X.AbstractC14730lx
            public void AUT() {
                if (C36021jC.A03(this)) {
                    return;
                }
                if (this.A0V) {
                    finish();
                    return;
                }
                this.A01.setVisibility(8);
                this.A01.setImageBitmap(null);
                if (this.A0S != null) {
                    this.A0P.A0Z = false;
                    this.A0S.A08 = null;
                }
            }

            @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
            public void onActivityResult(int i, int i2, Intent intent) {
                super.onActivityResult(i, i2, intent);
                if (i != 1) {
                    if (i == 2) {
                        if (i2 == -1) {
                            if (intent != null) {
                                Uri data = intent.getData();
                                this.A00 = data;
                                if (data != null) {
                                    A2C(R.string.contact_qr_wait);
                                    C12990iw.A1N(new C625437q(this.A00, this, this.A0T, this.A01.getWidth(), this.A01.getHeight()), ((ActivityC13830kP) this).A05);
                                    return;
                                }
                                ((ActivityC13810kN) this).A05.A07(R.string.error_load_image, 0);
                            } else {
                                return;
                            }
                        }
                        this.A0X = false;
                    } else if (i == 3) {
                        this.A0S.A08 = null;
                    } else if (i == 4 && i2 == -1) {
                        A2f();
                    }
                } else if (i2 != 0) {
                    this.A0S.A1A();
                } else if (this.A0Y) {
                    finish();
                } else {
                    this.A02.A0F(!C28141Kv.A01(this.A0G) ? 1 : 0, true);
                }
            }

            @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
            public void onCreate(Bundle bundle) {
                super.onCreate(bundle);
                A2e();
            }

            /* JADX WARN: Multi-variable type inference failed */
            /* JADX WARN: Type inference failed for: r2v2 */
            /* JADX WARN: Type inference failed for: r2v3 */
            /* JADX WARN: Type inference failed for: r2v4 */
            /* JADX WARN: Type inference failed for: r2v5 */
            /* JADX WARNING: Unknown variable types count: 1 */
            @Override // android.app.Activity
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public boolean onPrepareOptionsMenu(android.view.Menu r5) {
                /*
                    r4 = this;
                    androidx.viewpager.widget.ViewPager r0 = r4.A02
                    int r3 = r0.getCurrentItem()
                    X.018 r0 = r4.A0G
                    boolean r2 = X.C28141Kv.A01(r0)
                    r1 = 1
                    if (r3 == 0) goto L_0x001b
                    if (r3 == r1) goto L_0x0012
                    r2 = -1
                L_0x0012:
                    r0 = 0
                    if (r2 == 0) goto L_0x001e
                    if (r2 != r1) goto L_0x001a
                    r5.setGroupVisible(r0, r0)
                L_0x001a:
                    return r1
                L_0x001b:
                    r2 = r2 ^ 1
                    goto L_0x0012
                L_0x001e:
                    r5.setGroupVisible(r0, r1)
                    return r1
                */
                throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass34P.onPrepareOptionsMenu(android.view.Menu):boolean");
            }

            @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
            public void onStart() {
                super.onStart();
                this.A0O.A01(getWindow(), ((ActivityC13810kN) this).A08);
                this.A01.setVisibility(8);
                this.A01.setImageBitmap(null);
            }

            @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
            public void onStop() {
                this.A0O.A00(getWindow());
                super.onStop();
            }
        }
