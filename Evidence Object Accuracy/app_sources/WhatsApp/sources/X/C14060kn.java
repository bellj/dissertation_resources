package X;

import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import com.facebook.redex.RunnableBRunnable0Shape0S0100000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0200000_I0;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0kn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C14060kn implements AbstractC14070ko, AbstractC14080kp, AbstractC14100kr, AbstractC14110ks, AbstractC14120kt {
    public static final C100614mC A0b;
    public static final Map A0c;
    public int A00;
    public int A01;
    public int A02;
    public long A03;
    public long A04;
    public long A05;
    public long A06;
    public AnonymousClass5WY A07;
    public C107484xP A08;
    public AbstractC14130ku A09;
    public AnonymousClass4R1 A0A;
    public boolean A0B;
    public boolean A0C;
    public boolean A0D;
    public boolean A0E;
    public boolean A0F;
    public boolean A0G;
    public boolean A0H;
    public boolean A0I;
    public boolean A0J;
    public C92504We[] A0K;
    public C106994wc[] A0L;
    public final long A0M;
    public final Uri A0N;
    public final Handler A0O;
    public final AnonymousClass4P0 A0P;
    public final AbstractC116865Xf A0Q;
    public final AnonymousClass1Or A0R;
    public final AnonymousClass5QB A0S;
    public final AnonymousClass5SR A0T;
    public final AnonymousClass5VZ A0U;
    public final AnonymousClass2BW A0V;
    public final AnonymousClass5QO A0W;
    public final C93584aP A0X = new C93584aP();
    public final AnonymousClass4MC A0Y;
    public final Runnable A0Z;
    public final Runnable A0a;

    static {
        HashMap hashMap = new HashMap();
        hashMap.put("Icy-MetaData", "1");
        A0c = Collections.unmodifiableMap(hashMap);
        C93844ap r1 = new C93844ap();
        r1.A0O = "icy";
        r1.A0R = "application/x-icy";
        A0b = new C100614mC(r1);
    }

    public C14060kn(Uri uri, AnonymousClass4P0 r7, AbstractC116865Xf r8, AnonymousClass5SK r9, AnonymousClass1Or r10, AnonymousClass5SR r11, AnonymousClass5VZ r12, AnonymousClass2BW r13, AnonymousClass5QO r14, int i) {
        this.A0N = uri;
        this.A0V = r13;
        this.A0Q = r8;
        this.A0P = r7;
        this.A0W = r14;
        this.A0R = r10;
        this.A0T = r11;
        this.A0U = r12;
        this.A0M = (long) i;
        this.A0S = new C107584xc(r9);
        this.A0Y = new AnonymousClass4MC();
        this.A0Z = new RunnableBRunnable0Shape0S0100000_I0(this, 1);
        this.A0a = new RunnableBRunnable0Shape0S0100000_I0(this, 2);
        Looper myLooper = Looper.myLooper();
        C95314dV.A01(myLooper);
        this.A0O = new Handler(myLooper, null);
        this.A0K = new C92504We[0];
        this.A0L = new C106994wc[0];
        this.A06 = -9223372036854775807L;
        this.A05 = -1;
        this.A03 = -9223372036854775807L;
        this.A00 = 1;
    }

    public final long A00() {
        long j;
        C106994wc[] r7 = this.A0L;
        long j2 = Long.MIN_VALUE;
        for (C106994wc r2 : r7) {
            synchronized (r2) {
                j = r2.A06;
            }
            j2 = Math.max(j2, j);
        }
        return j2;
    }

    public final AnonymousClass5X6 A01(C92504We r7) {
        C106994wc[] r2 = this.A0L;
        int length = r2.length;
        for (int i = 0; i < length; i++) {
            if (r7.equals(this.A0K[i])) {
                return r2[i];
            }
        }
        AnonymousClass5VZ r4 = this.A0U;
        C106994wc r22 = new C106994wc(this.A0O.getLooper(), this.A0P, this.A0Q, r4);
        r22.A0D = this;
        int i2 = length + 1;
        Object[] copyOf = Arrays.copyOf(this.A0K, i2);
        copyOf[length] = r7;
        this.A0K = (C92504We[]) copyOf;
        Object[] copyOf2 = Arrays.copyOf(this.A0L, i2);
        copyOf2[length] = r22;
        this.A0L = (C106994wc[]) copyOf2;
        return r22;
    }

    public void A02() {
        C93584aP r2 = this.A0X;
        AnonymousClass5QO r0 = this.A0W;
        int i = this.A00;
        int i2 = ((AnonymousClass4y7) r0).A00;
        if (i2 == -1) {
            i2 = 3;
            if (i == 7) {
                i2 = 6;
            }
        }
        IOException iOException = r2.A01;
        if (iOException == null) {
            HandlerC52112aG r22 = r2.A00;
            if (r22 != null) {
                if (i2 == Integer.MIN_VALUE) {
                    i2 = r22.A05;
                }
                IOException iOException2 = r22.A02;
                if (iOException2 != null && r22.A00 > i2) {
                    throw iOException2;
                }
                return;
            }
            return;
        }
        throw iOException;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0059, code lost:
        if (X.C95554dx.A05(r0) != false) goto L_0x005b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A03() {
        /*
        // Method dump skipped, instructions count: 246
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14060kn.A03():void");
    }

    public final void A04() {
        C67803Sy r9 = new C67803Sy(this.A0N, this, this.A0S, this, this.A0V, this.A0Y);
        if (this.A0G) {
            C95314dV.A04(A07());
            long j = this.A03;
            if (j == -9223372036854775807L || this.A06 <= j) {
                long j2 = this.A07.AGX(this.A06).A00.A00;
                long j3 = this.A06;
                r9.A09.A00 = j2;
                r9.A01 = j3;
                r9.A04 = true;
                r9.A05 = false;
                for (C106994wc r0 : this.A0L) {
                    r0.A07 = j3;
                }
                this.A06 = -9223372036854775807L;
            } else {
                this.A0D = true;
                this.A06 = -9223372036854775807L;
                return;
            }
        }
        C106994wc[] r5 = this.A0L;
        int i = 0;
        for (C106994wc r02 : r5) {
            i += r02.A00 + r02.A02;
        }
        this.A02 = i;
        C93584aP r3 = this.A0X;
        AnonymousClass5QO r03 = this.A0W;
        int i2 = this.A00;
        int i3 = ((AnonymousClass4y7) r03).A00;
        if (i3 == -1) {
            i3 = 3;
            if (i2 == 7) {
                i3 = 6;
            }
        }
        Looper myLooper = Looper.myLooper();
        C95314dV.A01(myLooper);
        r3.A01 = null;
        HandlerC52112aG r12 = new HandlerC52112aG(myLooper, this, r9, r3, i3, SystemClock.elapsedRealtime());
        C93584aP r2 = r12.A09;
        boolean z = false;
        if (r2.A00 == null) {
            z = true;
        }
        C95314dV.A04(z);
        r2.A00 = r12;
        r12.A02 = null;
        r2.A02.execute(r12);
        AnonymousClass3H3 r22 = r9.A03;
        AnonymousClass1Or r4 = this.A0R;
        r4.A03(new C28721Ot(r22.A04, r22, Collections.emptyMap()), new C28731Ou(null, null, 1, -1, 0, r4.A00(r9.A01), r4.A00(this.A03)));
    }

    public final void A05(int i) {
        C95314dV.A04(this.A0G);
        AnonymousClass4R1 r1 = this.A0A;
        boolean[] zArr = r1.A03;
        if (!zArr[i]) {
            C100614mC r5 = r1.A00.A02[i].A02[0];
            AnonymousClass1Or r2 = this.A0R;
            r2.A05(new C28731Ou(r5, null, 1, C95554dx.A00(r5.A0T), 0, r2.A00(this.A04), -9223372036854775807L));
            zArr[i] = true;
        }
    }

    public final void A06(int i) {
        C95314dV.A04(this.A0G);
        boolean[] zArr = this.A0A.A02;
        if (this.A0F && zArr[i]) {
            if (!this.A0L[i].A06(false)) {
                this.A06 = 0;
                this.A0F = false;
                this.A0E = true;
                this.A04 = 0;
                this.A02 = 0;
                for (C106994wc r1 : this.A0L) {
                    r1.A04(false);
                }
                this.A09.AOe(this);
            }
        }
    }

    public final boolean A07() {
        return this.A06 != -9223372036854775807L;
    }

    @Override // X.AbstractC14080kp
    public boolean A7e(long j) {
        boolean z;
        if (!this.A0D) {
            C93584aP r3 = this.A0X;
            if (r3.A01 == null && !this.A0F && (!this.A0G || this.A01 != 0)) {
                AnonymousClass4MC r1 = this.A0Y;
                synchronized (r1) {
                    z = false;
                    if (!r1.A00) {
                        z = true;
                        r1.A00 = true;
                        r1.notifyAll();
                    }
                }
                if (r3.A00 != null) {
                    return z;
                }
                A04();
                return true;
            }
        }
        return false;
    }

    @Override // X.AbstractC14080kp
    public void A8x(long j, boolean z) {
        long j2;
        int i;
        C95314dV.A04(this.A0G);
        if (!A07()) {
            boolean[] zArr = this.A0A.A01;
            int length = this.A0L.length;
            for (int i2 = 0; i2 < length; i2++) {
                C106994wc r8 = this.A0L[i2];
                boolean z2 = zArr[i2];
                C94934cm r2 = r8.A0T;
                synchronized (r8) {
                    int i3 = r8.A02;
                    if (i3 != 0) {
                        long[] jArr = r8.A0N;
                        int i4 = r8.A04;
                        if (j >= jArr[i4]) {
                            if (z2 && (i = r8.A03) != i3) {
                                i3 = i + 1;
                            }
                            int A00 = r8.A00(i4, i3, j, false);
                            if (A00 != -1) {
                                j2 = r8.A01(A00);
                            }
                        }
                    }
                    j2 = -1;
                }
                r2.A03(j2);
            }
        }
    }

    @Override // X.AbstractC14070ko
    public void A9V() {
        this.A0I = true;
        this.A0O.post(this.A0Z);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0057, code lost:
        if (r7 > r3) goto L_0x0059;
     */
    @Override // X.AbstractC14080kp
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long AAe(X.C94224bS r18, long r19) {
        /*
            r17 = this;
            r9 = r19
            r1 = r17
            boolean r0 = r1.A0G
            X.C95314dV.A04(r0)
            X.5WY r1 = r1.A07
            boolean r0 = r1.AK2()
            if (r0 != 0) goto L_0x0014
            r9 = 0
            return r9
        L_0x0014:
            X.4Xa r1 = r1.AGX(r9)
            X.4bc r0 = r1.A00
            long r7 = r0.A01
            X.4bc r0 = r1.A01
            long r5 = r0.A01
            r11 = r18
            long r3 = r11.A01
            r15 = 0
            int r0 = (r3 > r15 ? 1 : (r3 == r15 ? 0 : -1))
            if (r0 != 0) goto L_0x0031
            long r0 = r11.A00
            int r2 = (r0 > r15 ? 1 : (r0 == r15 ? 0 : -1))
            if (r2 != 0) goto L_0x0031
            return r9
        L_0x0031:
            long r13 = r19 - r3
            long r3 = r3 ^ r19
            long r1 = r19 ^ r13
            long r1 = r1 & r3
            int r0 = (r1 > r15 ? 1 : (r1 == r15 ? 0 : -1))
            if (r0 >= 0) goto L_0x003e
            r13 = -9223372036854775808
        L_0x003e:
            long r11 = r11.A00
            long r3 = r19 + r11
            long r1 = r19 ^ r3
            long r11 = r11 ^ r3
            long r1 = r1 & r11
            int r0 = (r1 > r15 ? 1 : (r1 == r15 ? 0 : -1))
            if (r0 >= 0) goto L_0x004f
            r3 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
        L_0x004f:
            r2 = 1
            int r0 = (r13 > r7 ? 1 : (r13 == r7 ? 0 : -1))
            if (r0 > 0) goto L_0x0059
            int r0 = (r7 > r3 ? 1 : (r7 == r3 ? 0 : -1))
            r1 = 1
            if (r0 <= 0) goto L_0x005a
        L_0x0059:
            r1 = 0
        L_0x005a:
            int r0 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
            if (r0 > 0) goto L_0x0077
            int r0 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
            if (r0 > 0) goto L_0x0077
        L_0x0062:
            if (r1 == 0) goto L_0x0079
            if (r2 == 0) goto L_0x0076
            long r0 = r7 - r19
            long r3 = java.lang.Math.abs(r0)
            long r0 = r5 - r19
            long r1 = java.lang.Math.abs(r0)
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 > 0) goto L_0x007c
        L_0x0076:
            return r7
        L_0x0077:
            r2 = 0
            goto L_0x0062
        L_0x0079:
            if (r2 != 0) goto L_0x007c
            return r13
        L_0x007c:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14060kn.AAe(X.4bS, long):long");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0052, code lost:
        if (r2 == Long.MAX_VALUE) goto L_0x0054;
     */
    @Override // X.AbstractC14080kp
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long AB2() {
        /*
            r12 = this;
            boolean r0 = r12.A0G
            X.C95314dV.A04(r0)
            X.4R1 r0 = r12.A0A
            boolean[] r7 = r0.A02
            boolean r0 = r12.A0D
            r10 = -9223372036854775808
            if (r0 == 0) goto L_0x0010
            return r10
        L_0x0010:
            boolean r0 = r12.A07()
            if (r0 == 0) goto L_0x0019
            long r0 = r12.A06
            return r0
        L_0x0019:
            boolean r0 = r12.A0B
            r8 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            if (r0 == 0) goto L_0x0054
            X.4wc[] r0 = r12.A0L
            int r6 = r0.length
            r5 = 0
            r2 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
        L_0x002b:
            if (r5 >= r6) goto L_0x0050
            boolean r0 = r7[r5]
            if (r0 == 0) goto L_0x0047
            X.4wc[] r0 = r12.A0L
            r1 = r0[r5]
            monitor-enter(r1)
            boolean r0 = r1.A0E     // Catch: all -> 0x004d
            monitor-exit(r1)
            if (r0 != 0) goto L_0x0047
            X.4wc[] r0 = r12.A0L
            r4 = r0[r5]
            monitor-enter(r4)
            long r0 = r4.A06     // Catch: all -> 0x004a
            monitor-exit(r4)
            long r2 = java.lang.Math.min(r2, r0)
        L_0x0047:
            int r5 = r5 + 1
            goto L_0x002b
        L_0x004a:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x004d:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x0050:
            int r0 = (r2 > r8 ? 1 : (r2 == r8 ? 0 : -1))
            if (r0 != 0) goto L_0x0058
        L_0x0054:
            long r2 = r12.A00()
        L_0x0058:
            int r0 = (r2 > r10 ? 1 : (r2 == r10 ? 0 : -1))
            if (r0 != 0) goto L_0x005e
            long r2 = r12.A04
        L_0x005e:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14060kn.AB2():long");
    }

    @Override // X.AbstractC14080kp
    public long AEf() {
        if (this.A01 == 0) {
            return Long.MIN_VALUE;
        }
        return AB2();
    }

    @Override // X.AbstractC14080kp
    public C100564m7 AHH() {
        C95314dV.A04(this.A0G);
        return this.A0A.A00;
    }

    @Override // X.AbstractC14080kp
    public boolean AJh() {
        boolean z;
        if (this.A0X.A00 != null) {
            AnonymousClass4MC r2 = this.A0Y;
            synchronized (r2) {
                z = r2.A00;
            }
            if (z) {
                return true;
            }
        }
        return false;
    }

    @Override // X.AbstractC14080kp
    public void ALS() {
        A02();
        if (this.A0D && !this.A0G) {
            throw new AnonymousClass496("Loading finished before preparation is complete.");
        }
    }

    @Override // X.AbstractC14100kr
    public /* bridge */ /* synthetic */ void ARt(AbstractC116335Va r19, long j, long j2, boolean z) {
        C67803Sy r4 = (C67803Sy) r19;
        C67753St r0 = r4.A0B;
        C28721Ot r7 = new C28721Ot(r0.A01, r4.A03, r0.A02);
        AnonymousClass1Or r2 = this.A0R;
        r2.A01(r7, new C28731Ou(null, null, 1, -1, 0, r2.A00(r4.A01), r2.A00(this.A03)));
        if (!z) {
            if (this.A05 == -1) {
                this.A05 = r4.A00;
            }
            for (C106994wc r02 : this.A0L) {
                r02.A04(false);
            }
            if (this.A01 > 0) {
                this.A09.AOe(this);
            }
        }
    }

    @Override // X.AbstractC14080kp
    public void AZS(AbstractC14130ku r3, long j) {
        this.A09 = r3;
        AnonymousClass4MC r1 = this.A0Y;
        synchronized (r1) {
            if (!r1.A00) {
                r1.A00 = true;
                r1.notifyAll();
            }
        }
        A04();
    }

    @Override // X.AbstractC14080kp
    public long AZt() {
        if (!this.A0E) {
            return -9223372036854775807L;
        }
        if (!this.A0D) {
            C106994wc[] r5 = this.A0L;
            int i = 0;
            for (C106994wc r0 : r5) {
                i += r0.A00 + r0.A02;
            }
            if (i <= this.A02) {
                return -9223372036854775807L;
            }
        }
        this.A0E = false;
        return this.A04;
    }

    @Override // X.AbstractC14070ko
    public void AbR(AnonymousClass5WY r4) {
        this.A0O.post(new RunnableBRunnable0Shape0S0200000_I0(this, 0, r4));
    }

    @Override // X.AbstractC14080kp
    public long AbT(long j) {
        C95314dV.A04(this.A0G);
        boolean[] zArr = this.A0A.A02;
        if (!this.A07.AK2()) {
            j = 0;
        }
        int i = 0;
        this.A0E = false;
        this.A04 = j;
        if (A07()) {
            this.A06 = j;
        } else {
            if (this.A00 != 7) {
                int length = this.A0L.length;
                for (int i2 = 0; i2 < length; i2++) {
                    if (this.A0L[i2].A05(j, false) || (!zArr[i2] && this.A0B)) {
                    }
                }
            }
            this.A0F = false;
            this.A06 = j;
            this.A0D = false;
            C93584aP r3 = this.A0X;
            if (r3.A00 != null) {
                C106994wc[] r2 = this.A0L;
                int length2 = r2.length;
                while (i < length2) {
                    r2[i].A02();
                    i++;
                }
                HandlerC52112aG r1 = r3.A00;
                C95314dV.A01(r1);
                r1.A00(false);
                return j;
            }
            r3.A01 = null;
            C106994wc[] r32 = this.A0L;
            int length3 = r32.length;
            while (i < length3) {
                r32[i].A04(false);
                i++;
            }
        }
        return j;
    }

    @Override // X.AbstractC14080kp
    public long AbW(AbstractC116795Wx[] r13, AbstractC117085Ye[] r14, boolean[] zArr, boolean[] zArr2, long j) {
        int length;
        boolean z;
        long j2 = j;
        C95314dV.A04(this.A0G);
        AnonymousClass4R1 r2 = this.A0A;
        C100564m7 r9 = r2.A00;
        boolean[] zArr3 = r2.A01;
        int i = this.A01;
        int i2 = i;
        int i3 = 0;
        int i4 = 0;
        while (true) {
            length = r14.length;
            if (i4 >= length) {
                break;
            }
            if (r13[i4] != null && (r14[i4] == null || !zArr[i4])) {
                int i5 = ((C107604xe) r13[i4]).A00;
                C95314dV.A04(zArr3[i5]);
                i2--;
                this.A01 = i2;
                zArr3[i5] = false;
                r13[i4] = null;
            }
            i4++;
        }
        if (!this.A0J ? j == 0 : i != 0) {
            z = false;
        } else {
            z = true;
        }
        for (int i6 = 0; i6 < length; i6++) {
            if (r13[i6] == null && r14[i6] != null) {
                AbstractC117085Ye r4 = r14[i6];
                boolean z2 = false;
                if (((AbstractC107814xz) r4).A03.length == 1) {
                    z2 = true;
                }
                C95314dV.A04(z2);
                AbstractC107814xz r42 = (AbstractC107814xz) r4;
                boolean z3 = false;
                if (r42.A03[0] == 0) {
                    z3 = true;
                }
                C95314dV.A04(z3);
                C100554m6 r43 = r42.A02;
                int i7 = 0;
                while (true) {
                    if (i7 >= r9.A01) {
                        i7 = -1;
                        break;
                    }
                    if (r9.A02[i7] == r43) {
                        break;
                    }
                    i7++;
                }
                C95314dV.A04(!zArr3[i7]);
                this.A01++;
                zArr3[i7] = true;
                r13[i6] = new C107604xe(this, i7);
                zArr2[i6] = true;
                if (!z) {
                    C106994wc r44 = this.A0L[i7];
                    if (!r44.A05(j2, true)) {
                        z = true;
                        if (r44.A00 + r44.A03 != 0) {
                        }
                    }
                    z = false;
                }
            }
        }
        if (this.A01 == 0) {
            this.A0F = false;
            this.A0E = false;
            C93584aP r8 = this.A0X;
            boolean z4 = false;
            if (r8.A00 != null) {
                z4 = true;
            }
            C106994wc[] r6 = this.A0L;
            int length2 = r6.length;
            if (z4) {
                while (i3 < length2) {
                    r6[i3].A02();
                    i3++;
                }
                HandlerC52112aG r3 = r8.A00;
                C95314dV.A01(r3);
                r3.A00(false);
            } else {
                while (i3 < length2) {
                    r6[i3].A04(false);
                    i3++;
                }
            }
        } else if (z) {
            j2 = AbT(j2);
            while (i3 < r13.length) {
                if (r13[i3] != null) {
                    zArr2[i3] = true;
                }
                i3++;
            }
        }
        this.A0J = true;
        return j2;
    }

    @Override // X.AbstractC14070ko
    public AnonymousClass5X6 Af4(int i, int i2) {
        return A01(new C92504We(i, false));
    }
}
