package X;

import java.util.List;

/* renamed from: X.2CW  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2CW extends AbstractC34011fR {
    public final int A00;
    public final C244515o A01;
    public final String A02;
    public final String A03;
    public final List A04;
    public final boolean A05;

    public AnonymousClass2CW(C244515o r1, String str, String str2, List list, int i, boolean z) {
        this.A01 = r1;
        this.A03 = str;
        this.A04 = list;
        this.A00 = i;
        this.A05 = z;
        this.A02 = str2;
    }
}
