package X;

import com.facebook.redex.RunnableBRunnable0Shape14S0100000_I1;
import com.whatsapp.businessdirectory.view.activity.BusinessDirectoryActivity;
import java.util.TimerTask;

/* renamed from: X.5IP  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5IP extends TimerTask {
    public final /* synthetic */ BusinessDirectoryActivity A00;

    public AnonymousClass5IP(BusinessDirectoryActivity businessDirectoryActivity) {
        this.A00 = businessDirectoryActivity;
    }

    @Override // java.util.TimerTask, java.lang.Runnable
    public void run() {
        this.A00.runOnUiThread(new RunnableBRunnable0Shape14S0100000_I1(this, 40));
    }
}
