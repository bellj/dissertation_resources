package X;

import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.group.GroupAdminPickerActivity;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1kG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C36651kG extends AnonymousClass02M {
    public ArrayList A00;
    public List A01 = new ArrayList();
    public final /* synthetic */ GroupAdminPickerActivity A02;

    public /* synthetic */ C36651kG(GroupAdminPickerActivity groupAdminPickerActivity) {
        this.A02 = groupAdminPickerActivity;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A01.size();
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r8, int i) {
        C55002hd r82 = (C55002hd) r8;
        C15370n3 r4 = (C15370n3) this.A01.get(i);
        r82.A01.setVisibility(8);
        r82.A04.A07(r4, this.A00, -1);
        GroupAdminPickerActivity groupAdminPickerActivity = this.A02;
        groupAdminPickerActivity.A0B.A06(r82.A00, r4);
        if (!groupAdminPickerActivity.A0A.A0L(r4, -1) || r4.A0U == null) {
            r82.A02.setVisibility(8);
        } else {
            TextEmojiLabel textEmojiLabel = r82.A02;
            textEmojiLabel.setVisibility(0);
            textEmojiLabel.A0G(this.A00, groupAdminPickerActivity.A0A.A09(r4));
        }
        String str = r4.A0R;
        TextEmojiLabel textEmojiLabel2 = r82.A03;
        if (str != null) {
            textEmojiLabel2.setVisibility(0);
            textEmojiLabel2.A0G(null, r4.A0R);
        } else {
            textEmojiLabel2.setVisibility(8);
        }
        r82.A0H.setTag(r4.A0B(UserJid.class));
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        GroupAdminPickerActivity groupAdminPickerActivity = this.A02;
        return new C55002hd(groupAdminPickerActivity.getLayoutInflater().inflate(R.layout.group_chat_info_row, viewGroup, false), groupAdminPickerActivity);
    }
}
