package X;

/* renamed from: X.51g  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C1093251g implements AnonymousClass5T5 {
    public final long A00;

    public C1093251g(long j) {
        this.A00 = j;
    }

    @Override // X.AnonymousClass5T5
    public boolean Aej(AnonymousClass28D r6) {
        return C12960it.A1T((((long) r6.A00) > this.A00 ? 1 : (((long) r6.A00) == this.A00 ? 0 : -1)));
    }
}
