package X;

import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.hardware.Camera;
import android.util.Log;
import java.io.ByteArrayInputStream;
import java.util.concurrent.CountDownLatch;

/* renamed from: X.63E  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass63E implements Camera.PictureCallback {
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ Rect A02;
    public final /* synthetic */ CallableC135866Kc A03;
    public final /* synthetic */ C129755yF A04;

    public AnonymousClass63E(Rect rect, CallableC135866Kc r2, C129755yF r3, int i, int i2) {
        this.A03 = r2;
        this.A00 = i;
        this.A02 = rect;
        this.A01 = i2;
        this.A04 = r3;
    }

    @Override // android.hardware.Camera.PictureCallback
    public void onPictureTaken(byte[] bArr, Camera camera) {
        int i;
        Integer valueOf;
        Float valueOf2;
        Float valueOf3;
        byte[] bArr2 = bArr;
        if (C130215yz.A00()) {
            bArr2 = C130215yz.A01();
        } else if (this.A03.A01.A0W.get()) {
            Log.d("Camera1Device", "Photo capture took too long, not invoking photo capture callback");
            return;
        }
        Rect rect = null;
        if (bArr2 != null) {
            int A00 = C130225z0.A00(bArr2);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(bArr2, 0, bArr2.length, options);
            rect = new Rect(0, 0, options.outWidth, options.outHeight);
            C124865qF.A00(rect, this.A02, this.A00, A00);
        }
        Rect rect2 = this.A02;
        int i2 = this.A00;
        CallableC135866Kc r5 = this.A03;
        AnonymousClass661 r9 = r5.A01;
        C129755yF r4 = new C129755yF(rect, rect2, i2, r9.A00);
        C125525rO r1 = AnonymousClass60J.A0f;
        AnonymousClass60B r12 = r5.A02;
        r4.A01(r1, r12.A00(AnonymousClass60B.A09));
        r4.A01(AnonymousClass60J.A0Z, bArr2);
        AnonymousClass63B r2 = r9.A0O;
        C125525rO r13 = AnonymousClass60J.A0g;
        if (r2.A0B) {
            i = r2.A09;
        } else {
            i = 0;
        }
        r4.A01(r13, Integer.valueOf(i));
        r4.A01(AnonymousClass60J.A0e, Integer.valueOf(this.A01));
        AnonymousClass60J r8 = new AnonymousClass60J(r4);
        C129455xk r11 = r5.A00;
        r9.A0T.A05(new AnonymousClass6J0(r11, r9, r8), r9.A0S.A03);
        if (bArr2 != null) {
            AnonymousClass60M r7 = new AnonymousClass60M(new ByteArrayInputStream(bArr2));
            C129755yF r6 = this.A04;
            C125525rO r132 = AnonymousClass60J.A0U;
            Long l = null;
            double A01 = r7.A01("ExposureTime");
            if (A01 != -1.0d) {
                l = Long.valueOf((long) (A01 * Math.pow(10.0d, 9.0d)));
            }
            r6.A01(r132, l);
            C125525rO r22 = AnonymousClass60J.A0b;
            Integer num = null;
            int A02 = r7.A02("PhotographicSensitivity");
            if (A02 == -1) {
                valueOf = null;
            } else {
                valueOf = Integer.valueOf(A02);
            }
            r6.A01(r22, valueOf);
            C125525rO r42 = AnonymousClass60J.A0P;
            double A012 = r7.A01("ApertureValue");
            if (A012 == -1.0d) {
                valueOf2 = null;
            } else {
                valueOf2 = Float.valueOf((float) A012);
            }
            r6.A01(r42, valueOf2);
            C125525rO r43 = AnonymousClass60J.A0W;
            double A013 = r7.A01("FocalLength");
            if (A013 == -1.0d) {
                valueOf3 = null;
            } else {
                valueOf3 = Float.valueOf((float) A013);
            }
            r6.A01(r43, valueOf3);
            C125525rO r23 = AnonymousClass60J.A0Q;
            int A022 = r7.A02("WhiteBalance");
            if (A022 != -1) {
                num = Integer.valueOf(A022);
            }
            r6.A01(r23, num);
        }
        AnonymousClass661.A01(r11, r9, r12, this.A04, r8);
        ((CountDownLatch) r9.A0N.A00.get()).countDown();
        if (rect != null) {
            rect.width();
            rect.height();
        } else {
            new IllegalStateException("JPEG byte array was null.");
        }
        AnonymousClass616.A00();
    }
}
