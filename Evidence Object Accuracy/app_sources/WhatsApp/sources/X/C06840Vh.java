package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.redex.IDxCreatorShape0S0000000_I1;
import java.util.Arrays;

/* renamed from: X.0Vh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06840Vh implements Parcelable {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape0S0000000_I1(26);
    public int A00;
    public int A01;
    public boolean A02;
    public int[] A03;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C06840Vh() {
    }

    public C06840Vh(Parcel parcel) {
        this.A01 = parcel.readInt();
        this.A00 = parcel.readInt();
        this.A02 = parcel.readInt() != 1 ? false : true;
        int readInt = parcel.readInt();
        if (readInt > 0) {
            int[] iArr = new int[readInt];
            this.A03 = iArr;
            parcel.readIntArray(iArr);
        }
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("FullSpanItem{mPosition=");
        sb.append(this.A01);
        sb.append(", mGapDir=");
        sb.append(this.A00);
        sb.append(", mHasUnwantedGapAfter=");
        sb.append(this.A02);
        sb.append(", mGapPerSpan=");
        sb.append(Arrays.toString(this.A03));
        sb.append('}');
        return sb.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        int length;
        parcel.writeInt(this.A01);
        parcel.writeInt(this.A00);
        parcel.writeInt(this.A02 ? 1 : 0);
        int[] iArr = this.A03;
        if (iArr == null || (length = iArr.length) <= 0) {
            parcel.writeInt(0);
            return;
        }
        parcel.writeInt(length);
        parcel.writeIntArray(this.A03);
    }
}
