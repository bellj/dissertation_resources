package X;

import android.os.Build;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.Map;
import java.util.regex.Pattern;

/* renamed from: X.0uu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19930uu {
    public static final String A05 = "2.22.17.70".replace(' ', '_');
    public String A00;
    public String A01;
    public String A02;
    public final C16590pI A03;
    public final AnonymousClass018 A04;

    public C19930uu(C16590pI r1, AnonymousClass018 r2) {
        this.A03 = r1;
        this.A04 = r2;
    }

    public synchronized String A00() {
        String str;
        str = this.A00;
        if (str == null) {
            str = A02(this.A03, "2.22.17.70");
            this.A00 = str;
        }
        return str;
    }

    public synchronized String A01() {
        String str;
        str = this.A02;
        if (str == null) {
            C28201Mc r5 = new C28201Mc();
            r5.A00 = A00();
            Map map = r5.A01;
            map.put("FBAN", "WhatsAppAndroid");
            map.put("FBAV", A05);
            map.put("FBBV", String.valueOf(221770000));
            map.put("FBLC", this.A04.A07());
            map.put("FBPN", this.A03.A00.getPackageName());
            StringBuilder sb = new StringBuilder();
            sb.append(r5.A00);
            sb.append(" [");
            for (Object obj : C28201Mc.A02) {
                sb.append(String.format(null, "%s/%s;", obj, r5.A00((String) map.get(obj))));
            }
            for (Object obj2 : C28201Mc.A03) {
                sb.append(String.format(null, "%s/%s;", obj2, r5.A00((String) map.get(obj2))));
            }
            sb.append("]");
            str = sb.toString();
            this.A02 = str;
        }
        return str;
    }

    public final String A02(C16590pI r12, String str) {
        String str2;
        String str3;
        String str4 = "unknown";
        Pattern compile = Pattern.compile("[^,\\.\\w\\-\\(\\)]");
        StringBuilder sb = new StringBuilder();
        sb.append(str.replace(' ', '_'));
        String obj = sb.toString();
        String replace = "Android".replace(' ', '_');
        String replace2 = r12.A00.getString(R.string.app_name).replace(' ', '_');
        try {
            str2 = compile.matcher(Build.VERSION.RELEASE).replaceAll("_");
        } catch (NoSuchFieldError e) {
            Log.e("app/user-agent/release", e);
            str2 = str4;
        }
        try {
            str3 = compile.matcher(Build.MANUFACTURER).replaceAll("_");
        } catch (NoSuchFieldError e2) {
            Log.e("app/user-agent/manufacturer", e2);
            str3 = str4;
        }
        try {
            str4 = compile.matcher(Build.MODEL).replaceAll("_");
        } catch (NoSuchFieldError e3) {
            Log.e("app/user-agent/model", e3);
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append(replace2);
        sb2.append("/");
        sb2.append(obj);
        sb2.append(" ");
        sb2.append(replace);
        sb2.append("/");
        sb2.append(str2);
        sb2.append(" Device/");
        sb2.append(str3);
        sb2.append("-");
        sb2.append(str4);
        sb2.append("");
        return sb2.toString();
    }
}
