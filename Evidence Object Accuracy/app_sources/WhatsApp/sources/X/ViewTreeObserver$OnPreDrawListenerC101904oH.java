package X;

import android.view.ViewTreeObserver;
import com.whatsapp.registration.ChangeNumber;

/* renamed from: X.4oH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnPreDrawListenerC101904oH implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ ChangeNumber A00;

    public ViewTreeObserver$OnPreDrawListenerC101904oH(ChangeNumber changeNumber) {
        this.A00 = changeNumber;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        ChangeNumber changeNumber = this.A00;
        C12980iv.A1G(changeNumber.A05, this);
        changeNumber.A2h();
        return false;
    }
}
