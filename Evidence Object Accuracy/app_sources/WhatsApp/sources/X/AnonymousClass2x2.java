package X;

import android.graphics.PorterDuff;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.phonematching.CountryAndPhoneNumberFragment;

/* renamed from: X.2x2  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2x2 extends AnonymousClass4WI {
    public final /* synthetic */ CountryAndPhoneNumberFragment A00;

    public AnonymousClass2x2(CountryAndPhoneNumberFragment countryAndPhoneNumberFragment) {
        this.A00 = countryAndPhoneNumberFragment;
    }

    @Override // X.AnonymousClass4WI
    public void A01(String str, String str2) {
        TextView textView;
        int i;
        boolean equals = str.equals("");
        CountryAndPhoneNumberFragment countryAndPhoneNumberFragment = this.A00;
        TextView textView2 = countryAndPhoneNumberFragment.A06;
        if (equals) {
            textView2.setText(R.string.register_choose_country);
        } else if (str2 == null) {
            textView2.setText(R.string.register_choose_country);
            C12960it.A0s(countryAndPhoneNumberFragment.A08, countryAndPhoneNumberFragment.A05, R.color.red_error);
            countryAndPhoneNumberFragment.A06.getBackground().setColorFilter(AnonymousClass00T.A00(countryAndPhoneNumberFragment.A08, R.color.red_error), PorterDuff.Mode.SRC_IN);
            textView = countryAndPhoneNumberFragment.A04;
            i = 0;
            textView.setVisibility(i);
        } else {
            textView2.setText(countryAndPhoneNumberFragment.A0D.A02(countryAndPhoneNumberFragment.A0B, str2));
        }
        C12960it.A0s(countryAndPhoneNumberFragment.A08, countryAndPhoneNumberFragment.A05, R.color.settings_item_subtitle_text);
        countryAndPhoneNumberFragment.A06.getBackground().setColorFilter(AnonymousClass00T.A00(countryAndPhoneNumberFragment.A08, R.color.settings_delete_account_spinner_tint), PorterDuff.Mode.SRC_IN);
        textView = countryAndPhoneNumberFragment.A04;
        i = 4;
        textView.setVisibility(i);
    }
}
