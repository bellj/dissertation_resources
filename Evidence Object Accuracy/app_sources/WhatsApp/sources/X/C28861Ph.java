package X;

import android.text.TextUtils;
import android.util.Base64;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.TextData;
import com.whatsapp.util.Log;
import java.util.ArrayList;

/* renamed from: X.1Ph  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28861Ph extends AbstractC15340mz implements AbstractC16400ox, AbstractC16410oy, AbstractC16420oz {
    public int A00;
    public int A01;
    public TextData A02;
    public String A03;
    public String A04;
    public String A05;
    public String A06;
    public byte[] A07;

    public C28861Ph(AnonymousClass1IS r2, byte b, long j) {
        super(r2, b, j);
        this.A01 = 0;
    }

    public C28861Ph(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 0, j);
        this.A01 = 0;
    }

    public C28861Ph(AnonymousClass1IS r8, C28861Ph r9, long j, boolean z) {
        super(r9, r8, j, z);
        this.A01 = 0;
        this.A05 = r9.A05;
        this.A03 = r9.A03;
        this.A06 = r9.A06;
        this.A02 = r9.A02;
        this.A07 = r9.A07;
        this.A01 = r9.A01;
        this.A00 = r9.A00;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x009b, code lost:
        if ((r3 & 128) != 128) goto L_0x00c9;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C28861Ph(X.C57712nV r7, X.AnonymousClass1IS r8, long r9) {
        /*
        // Method dump skipped, instructions count: 324
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28861Ph.<init>(X.2nV, X.1IS, long):void");
    }

    public String A14() {
        if (!(this instanceof C28851Pg)) {
            return A0I();
        }
        C28851Pg r4 = (C28851Pg) this;
        if (TextUtils.isEmpty(r4.A0I())) {
            return r4.A00.A01;
        }
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder("*");
        sb2.append(r4.A0I());
        sb2.append("*");
        sb.append(sb2.toString());
        sb.append("\n\n");
        sb.append(r4.A00.A01);
        return sb.toString();
    }

    public void A15(TextData textData) {
        byte[] bArr;
        if (!(textData == null || (bArr = this.A07) == null)) {
            textData.thumbnail = bArr;
            this.A07 = null;
        }
        this.A02 = textData;
    }

    public void A16(byte[] bArr) {
        TextData textData = this.A02;
        if (textData != null) {
            textData.thumbnail = bArr;
        } else {
            this.A07 = bArr;
        }
    }

    public byte[] A17() {
        TextData textData = this.A02;
        if (textData != null) {
            return textData.thumbnail;
        }
        return this.A07;
    }

    @Override // X.AbstractC16420oz
    public void A6k(C39971qq r15) {
        AnonymousClass4BU r5;
        EnumC87224Ar r4;
        byte[] bArr;
        String str;
        if (this instanceof C28851Pg) {
            C28851Pg r11 = (C28851Pg) this;
            AnonymousClass1G3 r52 = r15.A03;
            AnonymousClass2Lw r42 = (AnonymousClass2Lw) r52.A05().A0T();
            C28891Pk r0 = r11.A00;
            if (r0 != null) {
                AnonymousClass2Lx A00 = C63173Ao.A00(r52, r0);
                if (!TextUtils.isEmpty(r11.A0I())) {
                    String A0I = r11.A0I();
                    A00.A03();
                    AnonymousClass2Ly r1 = (AnonymousClass2Ly) A00.A00;
                    r1.A01 = 2;
                    r1.A03 = A0I;
                }
                r42.A06(A00);
                r42.A05(A00);
                r52.A0B(r42);
                return;
            }
            StringBuilder sb = new StringBuilder("MessageTemplateHsm: cannot send encrypted hsm title message, ");
            sb.append((int) r11.A0y);
            Log.w(sb.toString());
        } else if (this instanceof AnonymousClass1XL) {
            AnonymousClass1XL r112 = (AnonymousClass1XL) this;
            AnonymousClass1G3 r2 = r15.A03;
            C57462n5 r02 = ((C27081Fy) r2.A00).A0d;
            if (r02 == null) {
                r02 = C57462n5.A05;
            }
            AnonymousClass1G4 A0T = r02.A0T();
            String A0I2 = r112.A0I();
            A0T.A03();
            C57462n5 r12 = (C57462n5) A0T.A00;
            r12.A00 |= 2;
            r12.A03 = A0I2;
            if (!TextUtils.isEmpty(r112.A01)) {
                String str2 = r112.A01;
                A0T.A03();
                C57462n5 r13 = (C57462n5) A0T.A00;
                r13.A00 |= 1;
                r13.A04 = str2;
            }
            int i = r112.A00;
            A0T.A03();
            C57462n5 r14 = (C57462n5) A0T.A00;
            r14.A00 |= 8;
            r14.A01 = i;
            C43261wh A0P = C32411c7.A0P(r15.A00, r15.A02, r15.A04, r112, r15.A09, r15.A06);
            A0T.A03();
            C57462n5 r16 = (C57462n5) A0T.A00;
            r16.A02 = A0P;
            r16.A00 |= 4;
            r2.A03();
            C27081Fy r22 = (C27081Fy) r2.A00;
            r22.A0d = (C57462n5) A0T.A02();
            r22.A00 |= 8388608;
        } else if (this instanceof AnonymousClass1XE) {
            AnonymousClass1XE r113 = (AnonymousClass1XE) this;
            AbstractC33751f1 A18 = r113.A18();
            if (A18 != null) {
                A18.A6l(r15, r113);
            }
        } else if (!(this instanceof AnonymousClass1XD)) {
            AnonymousClass1IR r03 = this.A0L;
            if (r03 != null) {
                A0d(r03, r15);
            } else if (C35011h5.A04(this)) {
                AnonymousClass1G3 r23 = r15.A03;
                C14850m9 r9 = r15.A02;
                C15570nT r8 = r15.A00;
                AnonymousClass1PG r10 = r15.A04;
                byte[] bArr2 = r15.A09;
                boolean z = r15.A06;
                C57552nF r04 = ((C27081Fy) r23.A00).A03;
                if (r04 == null) {
                    r04 = C57552nF.A08;
                }
                C56852m3 r3 = (C56852m3) r04.A0T();
                if (!TextUtils.isEmpty(A0I())) {
                    r3.A05(AnonymousClass39x.A05);
                    String A0I3 = A0I();
                    r3.A03();
                    C57552nF r17 = (C57552nF) r3.A00;
                    r17.A01 = 1;
                    r17.A05 = A0I3;
                } else {
                    r3.A05(AnonymousClass39x.A02);
                }
                C35011h5.A03(r3, A0F().A00);
                if (C32411c7.A0U(r10, this, bArr2)) {
                    C43261wh A0P2 = C32411c7.A0P(r8, r9, r10, this, bArr2, z);
                    r3.A03();
                    C57552nF r18 = (C57552nF) r3.A00;
                    r18.A04 = A0P2;
                    r18.A00 |= 128;
                }
                r23.A06((C57552nF) r3.A02());
            } else if (!TextUtils.isEmpty(this.A05) || !TextUtils.isEmpty(this.A03) || this.A02 != null || A12(EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) || C32411c7.A0U(r15.A04, this, r15.A09)) {
                AnonymousClass1G3 r32 = r15.A03;
                C57712nV r05 = ((C27081Fy) r32.A00).A0D;
                if (r05 == null) {
                    r05 = C57712nV.A0O;
                }
                AnonymousClass1G4 A0T2 = r05.A0T();
                String A0I4 = A0I();
                A0T2.A03();
                C57712nV r19 = (C57712nV) A0T2.A00;
                r19.A01 |= 1;
                r19.A0K = A0I4;
                String A02 = C33771f3.A02(A0I());
                if (!TextUtils.isEmpty(A02)) {
                    A0T2.A03();
                    C57712nV r110 = (C57712nV) A0T2.A00;
                    r110.A01 |= 2;
                    r110.A0J = A02;
                }
                if (!TextUtils.isEmpty(this.A05)) {
                    String str3 = this.A05;
                    A0T2.A03();
                    C57712nV r111 = (C57712nV) A0T2.A00;
                    r111.A01 |= 16;
                    r111.A0M = str3;
                }
                if (!TextUtils.isEmpty(this.A03)) {
                    String str4 = this.A03;
                    A0T2.A03();
                    C57712nV r114 = (C57712nV) A0T2.A00;
                    r114.A01 |= 8;
                    r114.A0H = str4;
                }
                if (!TextUtils.isEmpty(this.A06)) {
                    String str5 = this.A06;
                    A0T2.A03();
                    C57712nV r115 = (C57712nV) A0T2.A00;
                    r115.A01 |= 4;
                    r115.A0G = str5;
                }
                int i2 = this.A00;
                if (i2 == 1) {
                    r5 = AnonymousClass4BU.A03;
                } else if (i2 == 2) {
                    r5 = AnonymousClass4BU.A04;
                } else if (i2 == 3) {
                    r5 = AnonymousClass4BU.A02;
                } else {
                    r5 = AnonymousClass4BU.A01;
                }
                A0T2.A03();
                C57712nV r43 = (C57712nV) A0T2.A00;
                r43.A01 |= 4194304;
                r43.A03 = r5.value;
                C14360lJ r82 = this.A0T;
                if (!(r82 == null || (str = r82.A04) == null || r82.A09 == null || r82.A07 == null || r82.A05 == null)) {
                    A0T2.A03();
                    C57712nV r116 = (C57712nV) A0T2.A00;
                    r116.A01 |= 4096;
                    r116.A0L = str;
                    byte[] bArr3 = r82.A09;
                    AbstractC27881Jp A01 = AbstractC27881Jp.A01(bArr3, 0, bArr3.length);
                    A0T2.A03();
                    C57712nV r44 = (C57712nV) A0T2.A00;
                    r44.A01 |= 32768;
                    r44.A0C = A01;
                    byte[] decode = Base64.decode(r82.A07, 0);
                    AbstractC27881Jp A012 = AbstractC27881Jp.A01(decode, 0, decode.length);
                    A0T2.A03();
                    C57712nV r117 = (C57712nV) A0T2.A00;
                    r117.A01 |= DefaultCrypto.BUFFER_SIZE;
                    r117.A0E = A012;
                    byte[] decode2 = Base64.decode(r82.A05, 0);
                    AbstractC27881Jp A013 = AbstractC27881Jp.A01(decode2, 0, decode2.length);
                    A0T2.A03();
                    C57712nV r118 = (C57712nV) A0T2.A00;
                    r118.A01 |= 16384;
                    r118.A0D = A013;
                    long j = r82.A02;
                    if (j > 0) {
                        A0T2.A03();
                        C57712nV r7 = (C57712nV) A0T2.A00;
                        r7.A01 |= 65536;
                        r7.A09 = j / 1000;
                    }
                    int i3 = r82.A00;
                    if (i3 > 0) {
                        A0T2.A03();
                        C57712nV r45 = (C57712nV) A0T2.A00;
                        r45.A01 |= C25981Bo.A0F;
                        r45.A07 = i3;
                    }
                    int i4 = r82.A01;
                    if (i4 > 0) {
                        A0T2.A03();
                        C57712nV r46 = (C57712nV) A0T2.A00;
                        r46.A01 |= 262144;
                        r46.A08 = i4;
                    }
                }
                if (this.A01 == 1) {
                    r4 = EnumC87224Ar.A02;
                } else {
                    r4 = EnumC87224Ar.A01;
                }
                A0T2.A03();
                C57712nV r119 = (C57712nV) A0T2.A00;
                r119.A01 |= 256;
                r119.A05 = r4.value;
                TextData textData = this.A02;
                if (textData != null) {
                    int i5 = textData.backgroundColor;
                    A0T2.A03();
                    C57712nV r120 = (C57712nV) A0T2.A00;
                    r120.A01 |= 64;
                    r120.A00 = i5;
                    int i6 = this.A02.textColor;
                    A0T2.A03();
                    C57712nV r121 = (C57712nV) A0T2.A00;
                    r121.A01 |= 32;
                    r121.A06 = i6;
                    AnonymousClass4BY A002 = AnonymousClass4BY.A00(this.A02.fontStyle);
                    A0T2.A03();
                    C57712nV r122 = (C57712nV) A0T2.A00;
                    r122.A01 |= 128;
                    r122.A02 = A002.value;
                    bArr = this.A02.thumbnail;
                } else {
                    bArr = this.A07;
                }
                if (bArr != null) {
                    AbstractC27881Jp A014 = AbstractC27881Jp.A01(bArr, 0, bArr.length);
                    A0T2.A03();
                    C57712nV r123 = (C57712nV) A0T2.A00;
                    r123.A01 |= 512;
                    r123.A0B = A014;
                }
                AnonymousClass1PG r102 = r15.A04;
                byte[] bArr4 = r15.A09;
                if (C32411c7.A0U(r102, this, bArr4)) {
                    C43261wh A0P3 = C32411c7.A0P(r15.A00, r15.A02, r102, this, bArr4, r15.A06);
                    A0T2.A03();
                    C57712nV r124 = (C57712nV) A0T2.A00;
                    r124.A0F = A0P3;
                    r124.A01 |= EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
                }
                r32.A03();
                C27081Fy r125 = (C27081Fy) r32.A00;
                r125.A0D = (C57712nV) A0T2.A02();
                r125.A00 |= 32;
            } else {
                AnonymousClass1G3 r06 = r15.A03;
                String A0I5 = A0I();
                r06.A03();
                C27081Fy r126 = (C27081Fy) r06.A00;
                r126.A00 |= 1;
                r126.A0h = A0I5;
            }
        } else {
            AnonymousClass1XD r1110 = (AnonymousClass1XD) this;
            AnonymousClass1G3 r33 = r15.A03;
            C57502nA r07 = ((C27081Fy) r33.A00).A04;
            if (r07 == null) {
                r07 = C57502nA.A06;
            }
            AnonymousClass1G4 A0T3 = r07.A0T();
            String A0I6 = r1110.A0I();
            A0T3.A03();
            C57502nA r127 = (C57502nA) A0T3.A00;
            r127.A01 = 2;
            r127.A04 = A0I6;
            EnumC87214Aq r47 = EnumC87214Aq.A01;
            A0T3.A03();
            C57502nA r128 = (C57502nA) A0T3.A00;
            r128.A00 |= 8;
            r128.A02 = r47.value;
            if (!TextUtils.isEmpty(r1110.A00)) {
                String str6 = r1110.A00;
                A0T3.A03();
                C57502nA r129 = (C57502nA) A0T3.A00;
                r129.A00 |= 1;
                r129.A05 = str6;
            }
            C43261wh A0P4 = C32411c7.A0P(r15.A00, r15.A02, r15.A04, r1110, r15.A09, r15.A06);
            A0T3.A03();
            C57502nA r130 = (C57502nA) A0T3.A00;
            r130.A03 = A0P4;
            r130.A00 |= 4;
            r33.A03();
            C27081Fy r131 = (C27081Fy) r33.A00;
            r131.A04 = (C57502nA) A0T3.A02();
            r131.A01 |= 2;
        }
    }

    @Override // X.AbstractC16410oy
    public /* bridge */ /* synthetic */ AbstractC15340mz A7L(AnonymousClass1IS r11, long j) {
        C28861Ph r4;
        String A18;
        TextData textData;
        if (!(this instanceof C28851Pg)) {
            if (!(this instanceof AnonymousClass1XL)) {
                r4 = new C28861Ph(r11, this, j, false);
                C30441Xk r0 = A0F().A00;
                if (r0 != null) {
                    r4.A0h(new C30441Xk(r0.A00, r0.A01, new ArrayList()));
                }
            } else {
                r4 = new C28861Ph(r11, j);
                r4.A0l(A0I());
            }
            if (this.A02 != null) {
                r4.A02 = null;
                textData = this.A02;
                r4.A16(textData.thumbnail);
            }
        } else {
            C28851Pg r6 = (C28851Pg) this;
            r4 = new C28861Ph(r11, j);
            if (C15380n4.A0N(r11.A00)) {
                A18 = r6.A14();
            } else {
                A18 = r6.A18();
            }
            r4.A0l(A18);
            if (((C28861Ph) r6).A02 != null) {
                r4.A02 = null;
                textData = ((C28861Ph) r6).A02;
                r4.A16(textData.thumbnail);
            }
        }
        return r4;
    }

    @Override // X.AbstractC16400ox
    public AbstractC15340mz A7M(AnonymousClass1IS r9) {
        if (this instanceof C28851Pg) {
            C28851Pg r4 = (C28851Pg) this;
            return new C28851Pg(r9, r4, r4.A0I);
        } else if (this instanceof AnonymousClass1XL) {
            AnonymousClass1XL r42 = (AnonymousClass1XL) this;
            return new AnonymousClass1XL(r9, r42, r42.A0I);
        } else if (this instanceof AnonymousClass1XE) {
            AnonymousClass1XE r43 = (AnonymousClass1XE) this;
            return new AnonymousClass1XE(r9, r43, r43.A0I);
        } else if (!(this instanceof AnonymousClass1XD)) {
            return new C28861Ph(r9, this, this.A0I, true);
        } else {
            AnonymousClass1XD r44 = (AnonymousClass1XD) this;
            return new AnonymousClass1XD(r9, r44, r44.A0I);
        }
    }
}
