package X;

import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;

/* renamed from: X.2yh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60822yh extends AbstractC60602yI implements AbstractC42481vH {
    public boolean A00;
    public boolean A01;
    public final View A02 = findViewById(R.id.name_in_group);
    public final View A03 = findViewById(R.id.sticker_bubble_header);
    public final C64853Hd A04;

    @Override // X.AbstractC42671vd, X.AnonymousClass1OY
    public void A0y() {
    }

    public C60822yh(Context context, AbstractC13890kV r13, C30061Vy r14, AnonymousClass1AB r15) {
        super(context, r13, r14);
        C14850m9 r6 = ((AbstractC28551Oa) this).A0L;
        C239613r r2 = ((AnonymousClass1OY) this).A0M;
        C16170oZ r3 = ((AnonymousClass1OY) this).A0R;
        AnonymousClass018 r5 = ((AbstractC28551Oa) this).A0K;
        AnonymousClass19O r10 = this.A1O;
        this.A04 = new C64853Hd(this, r2, r3, ((AbstractC42671vd) this).A01, r5, r6, ((AbstractC42671vd) this).A04, ((AbstractC42671vd) this).A05, r15, r10);
        A0X(true);
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x002d  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00c1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A0X(boolean r8) {
        /*
            r7 = this;
            X.0mz r6 = r7.A0O
            X.0oV r6 = (X.AbstractC16130oV) r6
            X.1Vy r6 = (X.C30061Vy) r6
            X.0mz r0 = r6.A0E()
            r3 = 0
            if (r0 != 0) goto L_0x0027
            X.1c2 r0 = r6.A0N
            if (r0 != 0) goto L_0x0027
            X.1IS r1 = r6.A0z
            X.0lm r0 = r1.A00
            boolean r0 = X.C15380n4.A0J(r0)
            if (r0 == 0) goto L_0x00d3
            boolean r0 = r1.A02
            if (r0 != 0) goto L_0x00d3
            X.3Fx r0 = r7.A0b
            boolean r0 = r0.A07()
            if (r0 != 0) goto L_0x00d3
        L_0x0027:
            r0 = 1
        L_0x0028:
            r7.A01 = r0
            r2 = 0
            if (r8 == 0) goto L_0x0046
            X.0kV r0 = r7.A0a
            if (r0 == 0) goto L_0x0042
            boolean r3 = r0.AKC(r6)
            X.3Hd r0 = r7.A04
            com.whatsapp.stickers.StickerView r1 = r0.A0H
            if (r3 == 0) goto L_0x00cf
            X.2iC r0 = new X.2iC
            r0.<init>(r7, r6)
            r1.A01 = r0
        L_0x0042:
            X.3Hd r0 = r7.A04
            r0.A02 = r3
        L_0x0046:
            X.3Hd r3 = r7.A04
            android.view.View$OnLongClickListener r0 = r7.A1a
            com.whatsapp.stickers.StickerView r1 = r3.A0H
            r1.setOnLongClickListener(r0)
            boolean r0 = r7.A00
            r1.A03 = r0
            r3.A03(r6, r8)
            X.0oV r1 = r7.getFMessage()
            boolean r0 = X.C30041Vv.A12(r1)
            if (r0 == 0) goto L_0x00c1
            r3.A01()
        L_0x0063:
            android.content.res.Resources r1 = r7.getResources()
            r0 = 2131165742(0x7f07022e, float:1.794571E38)
            int r5 = r1.getDimensionPixelSize(r0)
            android.view.ViewGroup r4 = r7.A05
            android.view.ViewGroup$LayoutParams r3 = r4.getLayoutParams()
            android.widget.LinearLayout$LayoutParams r3 = (android.widget.LinearLayout.LayoutParams) r3
            X.0mz r0 = r6.A0E()
            if (r0 != 0) goto L_0x00bc
            X.1c2 r0 = r6.A0N
            if (r0 != 0) goto L_0x00bc
            android.content.Context r2 = r4.getContext()
            X.0mz r0 = r7.A0O
            X.1IS r0 = r0.A0z
            boolean r1 = r0.A02
            r0 = 2131231165(0x7f0801bd, float:1.8078403E38)
            if (r1 == 0) goto L_0x0092
            r0 = 2131231174(0x7f0801c6, float:1.8078422E38)
        L_0x0092:
            android.graphics.drawable.Drawable r0 = X.C12970iu.A0C(r2, r0)
            r7.setDateWrapperBackground(r0)
        L_0x0099:
            r3.topMargin = r5
            r4.setLayoutParams(r3)
            r7.A0w()
            r7.A1N(r6)
            X.0m9 r1 = r7.A0L
            r0 = 1396(0x574, float:1.956E-42)
            boolean r0 = r1.A07(r0)
            if (r0 == 0) goto L_0x00bb
            X.0mz r0 = r7.A0O
            X.0oV r0 = (X.AbstractC16130oV) r0
            X.1Vy r0 = (X.C30061Vy) r0
            X.1KB r0 = r0.A01
            if (r0 == 0) goto L_0x00bb
            r7.A0x()
        L_0x00bb:
            return
        L_0x00bc:
            r7.setDateWrapperBackground(r2)
            int r5 = -r5
            goto L_0x0099
        L_0x00c1:
            boolean r0 = X.C30041Vv.A13(r1)
            if (r0 == 0) goto L_0x00cb
            r3.A02()
            goto L_0x0063
        L_0x00cb:
            r3.A00()
            goto L_0x0063
        L_0x00cf:
            r1.A01 = r2
            goto L_0x0042
        L_0x00d3:
            r0 = 0
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C60822yh.A0X(boolean):void");
    }

    @Override // X.AbstractC28551Oa
    public Point A0c(int i, int i2) {
        int i3;
        if (((AbstractC28551Oa) this).A0O.A0E() != null) {
            return super.A0c(i, i2);
        }
        int dimensionPixelOffset = getResources().getDimensionPixelOffset(R.dimen.space_base);
        View A0D = AnonymousClass028.A0D(this, R.id.sticker_root);
        int left = A0D.getLeft();
        ViewGroup viewGroup = ((AnonymousClass1OY) this).A05;
        int left2 = left + viewGroup.getLeft();
        int y = (int) (A0D.getY() + viewGroup.getY());
        if (C28141Kv.A01(((AbstractC28551Oa) this).A0K)) {
            i3 = ((left2 + viewGroup.getMeasuredWidth()) - i) - dimensionPixelOffset;
        } else {
            i3 = left2 + dimensionPixelOffset;
        }
        return new Point(i3, (y + viewGroup.getMeasuredHeight()) - getReactionsViewVerticalOverlap());
    }

    @Override // X.AnonymousClass1OY
    public void A0s() {
        A0X(false);
        A1H(false);
    }

    @Override // X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r2, boolean z) {
        boolean A1X = C12960it.A1X(r2, ((AbstractC28551Oa) this).A0O);
        super.A1D(r2, z);
        if (z || A1X) {
            A0X(A1X);
        }
    }

    @Override // X.AbstractC42481vH
    public void Ae9() {
        this.A04.A0H.A02();
    }

    @Override // X.AbstractC28551Oa
    public int getBubbleAlpha() {
        if (this.A01) {
            return 255;
        }
        return 0;
    }

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_sticker_right;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0016, code lost:
        if (r0 != null) goto L_0x001a;
     */
    @Override // X.AbstractC28551Oa
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int getContentWidth() {
        /*
            r2 = this;
            boolean r0 = r2.A01
            if (r0 == 0) goto L_0x0018
            X.0mz r1 = r2.A0O
            X.0mz r0 = r1.A0E()
            if (r0 != 0) goto L_0x0018
            X.1c2 r0 = r1.A0N
            if (r0 != 0) goto L_0x0018
            android.view.View r0 = r2.A03
            if (r0 == 0) goto L_0x0018
            android.view.View r0 = r2.A02
            if (r0 != 0) goto L_0x001a
        L_0x0018:
            android.view.View r0 = r2.A0D
        L_0x001a:
            int r0 = r0.getMeasuredWidth()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C60822yh.getContentWidth():int");
    }

    @Override // X.AbstractC42671vd, X.AbstractC28551Oa
    public C30061Vy getFMessage() {
        return (C30061Vy) ((AbstractC16130oV) ((AbstractC28551Oa) this).A0O);
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_sticker_left;
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_sticker_right;
    }

    @Override // X.AbstractC28551Oa
    public int getReactionsViewVerticalOverlap() {
        return getResources().getDimensionPixelOffset(R.dimen.space_base);
    }

    private void setDateWrapperBackground(Drawable drawable) {
        ViewGroup viewGroup = ((AnonymousClass1OY) this).A05;
        int dimensionPixelOffset = C12960it.A09(viewGroup).getDimensionPixelOffset(R.dimen.conversation_row_sticker_date_padding_horizontal);
        int paddingBottom = viewGroup.getPaddingBottom();
        int paddingTop = viewGroup.getPaddingTop();
        viewGroup.setBackground(drawable);
        viewGroup.setPadding(dimensionPixelOffset, paddingTop, dimensionPixelOffset, paddingBottom);
    }

    @Override // X.AbstractC42671vd, X.AbstractC28551Oa
    public void setFMessage(AbstractC15340mz r2) {
        AnonymousClass009.A0F(r2 instanceof C30061Vy);
        super.setFMessage(r2);
    }
}
