package X;

import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/* renamed from: X.3h0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73913h0 extends Animation {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AnonymousClass238 A01;

    @Override // android.view.animation.Animation
    public boolean willChangeBounds() {
        return true;
    }

    public C73913h0(AnonymousClass238 r1, int i) {
        this.A01 = r1;
        this.A00 = i;
    }

    @Override // android.view.animation.Animation
    public void applyTransformation(float f, Transformation transformation) {
        AnonymousClass238 r3 = this.A01;
        ViewGroup.LayoutParams layoutParams = r3.A02.getLayoutParams();
        int i = this.A00;
        layoutParams.height = i - ((int) (((float) i) * f));
        r3.A02.requestLayout();
    }
}
