package X;

import java.io.File;
import java.io.OutputStream;
import java.security.DigestOutputStream;
import java.security.MessageDigest;

/* renamed from: X.3dL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C71683dL extends DigestOutputStream {
    public long A00 = 0;
    public boolean A01 = false;
    public final /* synthetic */ AnonymousClass2AW A02;
    public final /* synthetic */ File A03;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C71683dL(AnonymousClass2AW r3, File file, OutputStream outputStream, MessageDigest messageDigest) {
        super(outputStream, messageDigest);
        this.A02 = r3;
        this.A03 = file;
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x00e2  */
    /* JADX WARNING: Removed duplicated region for block: B:34:? A[RETURN, SYNTHETIC] */
    @Override // java.io.OutputStream, java.io.Closeable, java.io.FilterOutputStream, java.lang.AutoCloseable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void close() {
        /*
        // Method dump skipped, instructions count: 285
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C71683dL.close():void");
    }

    @Override // java.security.DigestOutputStream, java.io.OutputStream, java.io.FilterOutputStream
    public void write(int i) {
        super.write(i);
        this.A00++;
    }

    @Override // java.io.OutputStream, java.io.FilterOutputStream
    public void write(byte[] bArr) {
        write(bArr, 0, bArr.length);
    }

    @Override // java.security.DigestOutputStream, java.io.OutputStream, java.io.FilterOutputStream
    public void write(byte[] bArr, int i, int i2) {
        super.write(bArr, i, i2);
        this.A00 += (long) (i2 - i);
    }
}
