package X;

import com.whatsapp.authentication.FingerprintBottomSheet;
import com.whatsapp.payments.pin.ui.PinBottomSheetDialogFragment;

/* renamed from: X.6Bx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133736Bx implements AnonymousClass6MY {
    public final /* synthetic */ FingerprintBottomSheet A00;
    public final /* synthetic */ AbstractC1308360d A01;

    public C133736Bx(FingerprintBottomSheet fingerprintBottomSheet, AbstractC1308360d r2) {
        this.A01 = r2;
        this.A00 = fingerprintBottomSheet;
    }

    @Override // X.AnonymousClass6MY
    public void AW2() {
        AbstractC1308360d r2 = this.A01;
        PinBottomSheetDialogFragment A00 = C125035qZ.A00();
        A00.A0B = new AnonymousClass6CF(A00, r2);
        r2.A05.Adm(A00);
    }

    @Override // X.AnonymousClass6MY
    public void AX7(byte[] bArr) {
        FingerprintBottomSheet fingerprintBottomSheet = this.A00;
        fingerprintBottomSheet.A1C();
        AbstractC1308360d r3 = this.A01;
        ActivityC13790kL r6 = r3.A05;
        C14900mE r7 = r3.A03;
        C18610sj r9 = r3.A0D;
        C18650sn r8 = r3.A0B;
        AnonymousClass60T r10 = r3.A0G;
        C129215xM r5 = new C129215xM(r6, r7, r8, r9, r10, "PIN");
        AnonymousClass6B7 A0C = C117315Zl.A0C(r10, "FB", "PIN");
        if (A0C != null) {
            AnonymousClass1V8 A02 = new C128545wH(A0C).A02(bArr);
            fingerprintBottomSheet.A1C();
            r3.A05(null, A02);
            return;
        }
        r5.A00(new C133406Aq(this, bArr), "FB");
    }
}
