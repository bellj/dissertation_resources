package X;

/* renamed from: X.5se  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C126295se {
    public final AnonymousClass1V8 A00;

    public C126295se(AnonymousClass3CT r6, String str) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy A0N = C117295Zj.A0N(A0M);
        C41141sy.A01(A0N, "action", "br-remove-merchant-account");
        if (C117295Zj.A1W(str, 1, false)) {
            C41141sy.A01(A0N, "nonce", str);
        }
        this.A00 = C117295Zj.A0J(A0N, A0M, r6);
    }
}
