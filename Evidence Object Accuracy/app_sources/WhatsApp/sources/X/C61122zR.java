package X;

import android.content.Context;
import com.whatsapp.R;

/* renamed from: X.2zR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C61122zR extends AbstractC61152zU {
    public C22680zT A00;
    public C15570nT A01;
    public C14650lo A02;
    public C15550nR A03;
    public C16590pI A04;
    public boolean A05;

    public C61122zR(Context context) {
        super(context);
        A00();
    }

    @Override // X.AbstractC74133hN
    public void A00() {
        if (!this.A05) {
            this.A05 = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            super.A04 = C12960it.A0R(A00);
            this.A04 = C12970iu.A0X(A00);
            this.A01 = C12970iu.A0S(A00);
            this.A03 = C12960it.A0O(A00);
            this.A00 = (C22680zT) A00.AGW.get();
            this.A02 = C12980iv.A0Y(A00);
        }
    }

    @Override // X.AbstractC61152zU
    public int getNegativeButtonTextResId() {
        return R.string.contact_qr_reciprocal_footer_dismiss;
    }

    @Override // X.AbstractC61152zU
    public int getPositiveButtonIconResId() {
        return R.drawable.ic_settings_name;
    }

    @Override // X.AbstractC61152zU
    public int getPositiveButtonTextResId() {
        return R.string.contact_qr_share_my_contact_button;
    }
}
