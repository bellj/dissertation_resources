package X;

import android.net.Uri;
import java.io.File;

/* renamed from: X.1iM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC35601iM implements AbstractC35611iN {
    public Long A00;
    public final long A01;
    public final Uri A02;
    public final AbstractC16130oV A03;
    public final File A04;

    public AbstractC35601iM(AbstractC16130oV r7, File file, long j) {
        this(Uri.fromFile(file), r7, file, j);
    }

    public AbstractC35601iM(Uri uri, AbstractC16130oV r2, File file, long j) {
        this.A04 = file;
        this.A02 = uri;
        this.A01 = j;
        this.A03 = r2;
    }

    @Override // X.AbstractC35611iN
    public final Uri AAE() {
        return this.A02;
    }

    @Override // X.AbstractC35611iN
    public final long ACQ() {
        return this.A01;
    }

    @Override // X.AbstractC35611iN
    public /* synthetic */ long ACb() {
        if (!(this instanceof C616431f)) {
            return 0;
        }
        return ((C616431f) this).A00;
    }

    @Override // X.AbstractC35611iN
    public final long getContentLength() {
        long j;
        Long l = this.A00;
        if (l == null) {
            File file = this.A04;
            if (file != null) {
                j = file.length();
            } else {
                j = 0;
            }
            l = Long.valueOf(j);
            this.A00 = l;
        }
        return l.longValue();
    }
}
