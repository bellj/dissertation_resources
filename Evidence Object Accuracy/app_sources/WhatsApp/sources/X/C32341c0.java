package X;

import android.content.Context;
import android.view.ContextThemeWrapper;
import android.widget.RadioGroup;
import androidx.appcompat.widget.AppCompatRadioButton;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;

/* renamed from: X.1c0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C32341c0 {
    public static int A00(C15550nR r1, C19990v2 r2, AbstractC14640lm r3) {
        AnonymousClass1PG A08;
        if (C15380n4.A0J(r3)) {
            C15370n3 A0A = r1.A0A(r3);
            if (A0A != null) {
                return A0A.A01;
            }
            return 0;
        } else if (!(r3 instanceof UserJid) || (A08 = r2.A08((UserJid) r3)) == null) {
            return 0;
        } else {
            return A08.expiration;
        }
    }

    public static AnonymousClass1PG A01(AbstractC15340mz r6) {
        Long l = r6.A0X;
        boolean z = false;
        if (r6.A04 > 0) {
            z = true;
        }
        long j = 0;
        if (!z && (l == null || l.longValue() <= 0)) {
            return null;
        }
        int i = r6.A04;
        if (l != null) {
            j = l.longValue();
        }
        return new AnonymousClass1PG(i, j, r6.A00);
    }

    public static String A02(Context context, int i) {
        if (context == null) {
            return "";
        }
        int i2 = R.string.ephemeral_setting_24_hour;
        if (i != 86400) {
            i2 = R.string.ephemeral_setting_seven_day;
            if (i != 604800) {
                if (i != 7776000) {
                    return "";
                }
                i2 = R.string.ephemeral_setting_90_day;
            }
        }
        return context.getString(i2);
    }

    public static String A03(Context context, int i, boolean z, boolean z2) {
        int i2;
        if (i <= 0) {
            if (z2) {
                i2 = R.string.default_disappearing_messages_off_status;
                if (z) {
                    i2 = R.string.default_disappearing_messages_off;
                }
            } else {
                i2 = R.string.ephemeral_setting_off_status;
                if (z) {
                    i2 = R.string.ephemeral_setting_off;
                }
            }
        } else if (i == 86400) {
            i2 = R.string.ephemeral_setting_24_hours;
            if (!z) {
                i2 = R.string.ephemeral_setting_24_hours_status;
            }
        } else if (i == 604800) {
            i2 = R.string.ephemeral_setting_seven_days;
            if (!z) {
                i2 = R.string.ephemeral_setting_seven_days_status;
            }
        } else if (i == 7776000) {
            i2 = R.string.ephemeral_setting_90_days;
            if (!z) {
                i2 = R.string.ephemeral_setting_90_days_status;
            }
        } else {
            int i3 = R.plurals.ephemeral_setting_in_seconds;
            if (i > 86400) {
                i /= 86400;
                i3 = R.plurals.ephemeral_setting_in_days;
            } else if (i >= 3600) {
                i /= 3600;
                i3 = R.plurals.ephemeral_setting_in_hours;
            } else if (i >= 60) {
                i /= 60;
                i3 = R.plurals.ephemeral_setting_in_minutes;
            }
            return context.getResources().getQuantityString(i3, i, Integer.valueOf(i));
        }
        return context.getString(i2);
    }

    public static String A04(AnonymousClass018 r6, int i) {
        int i2;
        if (i <= 0) {
            return "";
        }
        if (i >= 86400) {
            i /= 86400;
            i2 = R.plurals.tb_ephemeral_chat_expiration_in_days;
        } else if (i >= 3600) {
            i /= 3600;
            i2 = R.plurals.tb_ephemeral_chat_expiration_in_hours;
        } else if (i >= 60) {
            i /= 60;
            i2 = R.plurals.tb_ephemeral_chat_expiration_in_minutes;
        } else {
            i2 = R.plurals.tb_ephemeral_chat_expiration_in_seconds;
        }
        return r6.A0I(new Object[]{Integer.valueOf(i)}, i2, (long) i);
    }

    public static void A05(RadioGroup radioGroup, C14850m9 r11, int i, boolean z) {
        int[] iArr;
        if (r11.A07(1397)) {
            iArr = AnonymousClass01V.A0E;
        } else {
            iArr = AnonymousClass01V.A0F;
        }
        int i2 = -1;
        int length = iArr.length;
        AppCompatRadioButton[] appCompatRadioButtonArr = new AppCompatRadioButton[length];
        for (int i3 = 0; i3 < length; i3++) {
            int i4 = iArr[i3];
            Context context = radioGroup.getContext();
            AppCompatRadioButton appCompatRadioButton = new AppCompatRadioButton(new ContextThemeWrapper(context, (int) R.style.SettingsRadioButton));
            appCompatRadioButton.setId(AnonymousClass028.A02());
            appCompatRadioButton.setTag(Integer.valueOf(i4));
            appCompatRadioButton.setText(A03(context, i4, true, z));
            appCompatRadioButton.setLayoutParams(new RadioGroup.LayoutParams(-1, -2));
            appCompatRadioButtonArr[i3] = appCompatRadioButton;
            radioGroup.addView(appCompatRadioButton);
            if (i4 == i) {
                i2 = i3;
            }
        }
        if (i2 >= 0) {
            appCompatRadioButtonArr[i2].setChecked(true);
        }
    }
}
