package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.whatsapp.R;
import com.whatsapp.profile.ViewProfilePhoto;

/* renamed from: X.2a6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class HandlerC52022a6 extends Handler {
    public final /* synthetic */ ViewProfilePhoto A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public HandlerC52022a6(Looper looper, ViewProfilePhoto viewProfilePhoto) {
        super(looper);
        this.A00 = viewProfilePhoto;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        ViewProfilePhoto viewProfilePhoto = this.A00;
        C14900mE r2 = ((ActivityC13810kN) viewProfilePhoto).A05;
        boolean A0K = viewProfilePhoto.A0A.A0K();
        int i = R.string.failed_update_profile_photo;
        if (A0K) {
            i = R.string.failed_update_photo;
        }
        r2.A07(i, 0);
        viewProfilePhoto.findViewById(R.id.progress_bar).setVisibility(8);
    }
}
