package X;

/* renamed from: X.2wX  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2wX extends AnonymousClass4KB {
    public final AnonymousClass4EF A00;

    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof AnonymousClass2wX) && C16700pc.A0O(this.A00, ((AnonymousClass2wX) obj).A00));
    }

    public int hashCode() {
        return this.A00.hashCode();
    }

    public AnonymousClass2wX(AnonymousClass4EF r2) {
        super(AnonymousClass1WF.A00);
        this.A00 = r2;
    }

    public String toString() {
        return C12960it.A0a(this.A00, C12960it.A0k("FailedContinueSearch(failureType="));
    }
}
