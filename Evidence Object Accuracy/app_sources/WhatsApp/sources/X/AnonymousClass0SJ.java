package X;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;

/* renamed from: X.0SJ  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0SJ {
    public static final String A05 = C06390Tk.A01("WorkTimer");
    public final Object A00 = new Object();
    public final Map A01 = new HashMap();
    public final Map A02 = new HashMap();
    public final ScheduledExecutorService A03;
    public final ThreadFactory A04;

    public AnonymousClass0SJ() {
        ThreadFactoryC10660ez r1 = new ThreadFactoryC10660ez(this);
        this.A04 = r1;
        this.A03 = Executors.newSingleThreadScheduledExecutor(r1);
    }

    public void A00(String str) {
        synchronized (this.A00) {
            if (((RunnableC09690dM) this.A02.remove(str)) != null) {
                C06390Tk.A00().A02(A05, String.format("Stopping timer for %s", str), new Throwable[0]);
                this.A01.remove(str);
            }
        }
    }
}
