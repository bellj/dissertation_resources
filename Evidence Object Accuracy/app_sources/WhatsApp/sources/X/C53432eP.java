package X;

import android.view.View;
import android.widget.ImageView;
import com.whatsapp.util.ViewOnClickCListenerShape2S0201000_I1;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.2eP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53432eP extends AbstractC000600h {
    public final /* synthetic */ ActivityC000800j A00;
    public final /* synthetic */ ViewOnClickCListenerShape2S0201000_I1 A01;

    public C53432eP(ActivityC000800j r1, ViewOnClickCListenerShape2S0201000_I1 viewOnClickCListenerShape2S0201000_I1) {
        this.A01 = viewOnClickCListenerShape2S0201000_I1;
        this.A00 = r1;
    }

    @Override // X.AbstractC000600h
    public void A02(List list, List list2, List list3) {
        this.A00.A0k(null);
        AnonymousClass3DB r1 = (AnonymousClass3DB) this.A01.A01;
        AnonymousClass028.A0k(r1.A02, null);
        AnonymousClass028.A0k(r1.A04, null);
        ImageView imageView = r1.A03;
        if (imageView != null) {
            AnonymousClass028.A0k(imageView, null);
        }
    }

    @Override // X.AbstractC000600h
    public void A03(List list, Map map) {
        View A06;
        Iterator it = list.iterator();
        while (it.hasNext()) {
            String A0x = C12970iu.A0x(it);
            if (!map.containsKey(A0x) && (A06 = AbstractC454421p.A06(C12970iu.A0G(this.A00), A0x)) != null) {
                map.put(A0x, A06);
            }
        }
    }
}
