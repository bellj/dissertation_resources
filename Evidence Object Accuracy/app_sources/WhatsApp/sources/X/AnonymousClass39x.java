package X;

/* renamed from: X.39x  reason: invalid class name */
/* loaded from: classes2.dex */
public enum AnonymousClass39x {
    A06(0),
    A02(1),
    A05(2),
    A01(3),
    A03(4),
    A07(5),
    A04(6);
    
    public final int value;

    AnonymousClass39x(int i) {
        this.value = i;
    }

    public static AnonymousClass39x A00(int i) {
        switch (i) {
            case 0:
                return A06;
            case 1:
                return A02;
            case 2:
                return A05;
            case 3:
                return A01;
            case 4:
                return A03;
            case 5:
                return A07;
            case 6:
                return A04;
            default:
                return null;
        }
    }
}
