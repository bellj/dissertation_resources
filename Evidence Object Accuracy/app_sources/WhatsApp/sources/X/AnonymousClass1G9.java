package X;

/* renamed from: X.1G9  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1G9 extends AnonymousClass1G4 implements AnonymousClass1G2 {
    public AnonymousClass1G9() {
        super(AnonymousClass1G8.A05);
    }

    public void A05(String str) {
        A03();
        AnonymousClass1G8 r1 = (AnonymousClass1G8) this.A00;
        r1.A00 |= 4;
        r1.A01 = str;
    }

    public void A06(String str) {
        A03();
        AnonymousClass1G8 r1 = (AnonymousClass1G8) this.A00;
        r1.A00 |= 8;
        r1.A02 = str;
    }

    public void A07(String str) {
        A03();
        AnonymousClass1G8 r1 = (AnonymousClass1G8) this.A00;
        r1.A00 |= 1;
        r1.A03 = str;
    }

    public void A08(boolean z) {
        A03();
        AnonymousClass1G8 r1 = (AnonymousClass1G8) this.A00;
        r1.A00 |= 2;
        r1.A04 = z;
    }
}
