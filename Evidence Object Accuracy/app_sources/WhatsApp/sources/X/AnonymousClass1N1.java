package X;

import android.os.Handler;
import android.os.Looper;
import com.whatsapp.Statistics$Data;
import java.io.File;
import java.util.concurrent.CountDownLatch;

/* renamed from: X.1N1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1N1 extends Handler implements AbstractC20260vT {
    public Statistics$Data A00;
    public File A01;
    public File A02;
    public final CountDownLatch A03 = new CountDownLatch(1);
    public volatile boolean A04;
    public final /* synthetic */ C18790t3 A05;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass1N1(Looper looper, C18790t3 r4, C18640sm r5) {
        super(looper);
        this.A05 = r4;
        r5.A03(this);
    }

    @Override // X.AbstractC20260vT
    public void AOa(AnonymousClass1I1 r2) {
        this.A04 = r2.A02;
    }

    /* JADX WARNING: Removed duplicated region for block: B:55:0x012b A[Catch: all -> 0x022a, TryCatch #1 {, blocks: (B:3:0x0001, B:4:0x0005, B:5:0x0008, B:6:0x001f, B:7:0x0026, B:8:0x002b, B:10:0x0043, B:13:0x0051, B:15:0x0053, B:16:0x005b, B:19:0x0060, B:21:0x0066, B:22:0x006d, B:23:0x0074, B:25:0x009c, B:28:0x00ad, B:31:0x00b8, B:34:0x00c3, B:36:0x00cc, B:37:0x00d5, B:42:0x00e1, B:44:0x00eb, B:47:0x00f6, B:49:0x00ff, B:50:0x0108, B:51:0x0115, B:52:0x0122, B:53:0x0127, B:55:0x012b, B:65:0x0140, B:66:0x0149, B:67:0x0152, B:68:0x015b, B:69:0x0164, B:70:0x016d, B:71:0x0172, B:73:0x0176, B:83:0x018b, B:84:0x0194, B:85:0x019d, B:86:0x01a6, B:87:0x01ae, B:88:0x01b6, B:90:0x01de, B:92:0x01f5, B:93:0x0201, B:94:0x0206), top: B:102:0x0001, inners: #2, #4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0134  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0164 A[Catch: all -> 0x022a, TryCatch #1 {, blocks: (B:3:0x0001, B:4:0x0005, B:5:0x0008, B:6:0x001f, B:7:0x0026, B:8:0x002b, B:10:0x0043, B:13:0x0051, B:15:0x0053, B:16:0x005b, B:19:0x0060, B:21:0x0066, B:22:0x006d, B:23:0x0074, B:25:0x009c, B:28:0x00ad, B:31:0x00b8, B:34:0x00c3, B:36:0x00cc, B:37:0x00d5, B:42:0x00e1, B:44:0x00eb, B:47:0x00f6, B:49:0x00ff, B:50:0x0108, B:51:0x0115, B:52:0x0122, B:53:0x0127, B:55:0x012b, B:65:0x0140, B:66:0x0149, B:67:0x0152, B:68:0x015b, B:69:0x0164, B:70:0x016d, B:71:0x0172, B:73:0x0176, B:83:0x018b, B:84:0x0194, B:85:0x019d, B:86:0x01a6, B:87:0x01ae, B:88:0x01b6, B:90:0x01de, B:92:0x01f5, B:93:0x0201, B:94:0x0206), top: B:102:0x0001, inners: #2, #4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0176 A[Catch: all -> 0x022a, TryCatch #1 {, blocks: (B:3:0x0001, B:4:0x0005, B:5:0x0008, B:6:0x001f, B:7:0x0026, B:8:0x002b, B:10:0x0043, B:13:0x0051, B:15:0x0053, B:16:0x005b, B:19:0x0060, B:21:0x0066, B:22:0x006d, B:23:0x0074, B:25:0x009c, B:28:0x00ad, B:31:0x00b8, B:34:0x00c3, B:36:0x00cc, B:37:0x00d5, B:42:0x00e1, B:44:0x00eb, B:47:0x00f6, B:49:0x00ff, B:50:0x0108, B:51:0x0115, B:52:0x0122, B:53:0x0127, B:55:0x012b, B:65:0x0140, B:66:0x0149, B:67:0x0152, B:68:0x015b, B:69:0x0164, B:70:0x016d, B:71:0x0172, B:73:0x0176, B:83:0x018b, B:84:0x0194, B:85:0x019d, B:86:0x01a6, B:87:0x01ae, B:88:0x01b6, B:90:0x01de, B:92:0x01f5, B:93:0x0201, B:94:0x0206), top: B:102:0x0001, inners: #2, #4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x017f  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x01ae A[Catch: all -> 0x022a, TryCatch #1 {, blocks: (B:3:0x0001, B:4:0x0005, B:5:0x0008, B:6:0x001f, B:7:0x0026, B:8:0x002b, B:10:0x0043, B:13:0x0051, B:15:0x0053, B:16:0x005b, B:19:0x0060, B:21:0x0066, B:22:0x006d, B:23:0x0074, B:25:0x009c, B:28:0x00ad, B:31:0x00b8, B:34:0x00c3, B:36:0x00cc, B:37:0x00d5, B:42:0x00e1, B:44:0x00eb, B:47:0x00f6, B:49:0x00ff, B:50:0x0108, B:51:0x0115, B:52:0x0122, B:53:0x0127, B:55:0x012b, B:65:0x0140, B:66:0x0149, B:67:0x0152, B:68:0x015b, B:69:0x0164, B:70:0x016d, B:71:0x0172, B:73:0x0176, B:83:0x018b, B:84:0x0194, B:85:0x019d, B:86:0x01a6, B:87:0x01ae, B:88:0x01b6, B:90:0x01de, B:92:0x01f5, B:93:0x0201, B:94:0x0206), top: B:102:0x0001, inners: #2, #4 }] */
    @Override // android.os.Handler
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void handleMessage(android.os.Message r12) {
        /*
        // Method dump skipped, instructions count: 582
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1N1.handleMessage(android.os.Message):void");
    }
}
