package X;

import java.util.Comparator;

/* renamed from: X.1Ur  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class C29791Ur implements Comparator {
    public static final /* synthetic */ C29791Ur A00 = new C29791Ur();

    @Override // java.util.Comparator
    public final int compare(Object obj, Object obj2) {
        return ((String) obj).compareToIgnoreCase((String) obj2);
    }
}
