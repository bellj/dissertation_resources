package X;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.biz.catalog.CatalogMediaViewFragment;
import com.whatsapp.mediaview.MediaViewBaseFragment;
import com.whatsapp.mediaview.PhotoView;
import com.whatsapp.ui.media.MediaCaptionTextView;
import com.whatsapp.util.ViewOnClickCListenerShape17S0100000_I1;

/* renamed from: X.3YN  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3YN implements AnonymousClass5XC {
    public final /* synthetic */ CatalogMediaViewFragment A00;

    @Override // X.AnonymousClass5XC
    public void A8t(int i) {
    }

    @Override // X.AnonymousClass5XC
    public void AQb() {
    }

    public /* synthetic */ AnonymousClass3YN(CatalogMediaViewFragment catalogMediaViewFragment) {
        this.A00 = catalogMediaViewFragment;
    }

    @Override // X.AnonymousClass5XC
    public AnonymousClass01T A8c(int i) {
        CatalogMediaViewFragment catalogMediaViewFragment = this.A00;
        ViewGroup viewGroup = (ViewGroup) catalogMediaViewFragment.A04().inflate(R.layout.media_view_photo, (ViewGroup) null);
        ViewGroup A0P = C12980iv.A0P(viewGroup, R.id.footer);
        C619133b r6 = new C619133b(catalogMediaViewFragment.A01(), this);
        r6.A0M = new ViewOnClickCListenerShape17S0100000_I1(this, 15);
        if (i == catalogMediaViewFragment.A00) {
            AnonymousClass028.A0k(r6, AbstractC42671vd.A0Z(AnonymousClass19N.A00(i, catalogMediaViewFragment.A02.A0D)));
        }
        int i2 = 0;
        viewGroup.addView(r6, 0);
        ((PhotoView) r6).A01 = 0.2f;
        r6.A0Y = true;
        catalogMediaViewFragment.A05.A02(r6, (C44741zT) catalogMediaViewFragment.A02.A06.get(i), null, new AnonymousClass3VB(catalogMediaViewFragment, r6, i), 1);
        if (!TextUtils.isEmpty(catalogMediaViewFragment.A02.A0A)) {
            View inflate = catalogMediaViewFragment.A04().inflate(R.layout.media_view_caption, (ViewGroup) null);
            A0P.addView(inflate, 0);
            A0P.setBackground(C12980iv.A0L(catalogMediaViewFragment.A01(), R.color.media_view_footer_background));
            ((MediaCaptionTextView) AnonymousClass028.A0D(inflate, R.id.caption)).setCaptionText(catalogMediaViewFragment.A02.A0A);
        }
        if (!((MediaViewBaseFragment) catalogMediaViewFragment).A0G) {
            i2 = 8;
        }
        A0P.setVisibility(i2);
        return new AnonymousClass01T(viewGroup, AnonymousClass19N.A00(i, catalogMediaViewFragment.A02.A0D));
    }

    @Override // X.AnonymousClass5XC
    public /* bridge */ /* synthetic */ int AFp(Object obj) {
        int i = 0;
        while (true) {
            CatalogMediaViewFragment catalogMediaViewFragment = this.A00;
            if (i >= catalogMediaViewFragment.A02.A06.size()) {
                return 0;
            }
            if (AnonymousClass19N.A00(i, catalogMediaViewFragment.A02.A0D).equals(obj)) {
                return i;
            }
            i++;
        }
    }

    @Override // X.AnonymousClass5XC
    public int getCount() {
        return this.A00.A02.A06.size();
    }
}
