package X;

import java.util.Arrays;

/* renamed from: X.21B  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass21B implements AnonymousClass21A {
    public final int A00;
    public final AnonymousClass21A A01;
    public final AnonymousClass21A A02;

    public AnonymousClass21B(AnonymousClass21A r1, AnonymousClass21A r2, int i) {
        this.A00 = i;
        this.A01 = r1;
        this.A02 = r2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0037, code lost:
        if (r4.A01.AJL(r5) == false) goto L_0x0047;
     */
    @Override // X.AnonymousClass21A
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean AJL(X.C22020yM r5) {
        /*
            r4 = this;
            int r3 = r4.A00
            r2 = 0
            r1 = 1
            if (r3 == 0) goto L_0x003a
            if (r3 == r1) goto L_0x002c
            r0 = 2
            if (r3 != r0) goto L_0x0013
            X.21A r0 = r4.A01
            boolean r0 = r0.AJL(r5)
            r0 = r0 ^ r1
            return r0
        L_0x0013:
            java.lang.String r0 = "Operator with code "
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            r1.append(r3)
            java.lang.String r0 = " is not currently supported"
            r1.append(r0)
            java.lang.String r1 = r1.toString()
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>(r1)
            throw r0
        L_0x002c:
            X.21A r1 = r4.A02
            X.AnonymousClass009.A05(r1)
            X.21A r0 = r4.A01
            boolean r0 = r0.AJL(r5)
            if (r0 != 0) goto L_0x004d
            goto L_0x0047
        L_0x003a:
            X.21A r1 = r4.A02
            X.AnonymousClass009.A05(r1)
            X.21A r0 = r4.A01
            boolean r0 = r0.AJL(r5)
            if (r0 == 0) goto L_0x004e
        L_0x0047:
            boolean r0 = r1.AJL(r5)
            if (r0 == 0) goto L_0x004e
        L_0x004d:
            r2 = 1
        L_0x004e:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass21B.AJL(X.0yM):boolean");
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof AnonymousClass21B)) {
            return false;
        }
        AnonymousClass21B r4 = (AnonymousClass21B) obj;
        if (this.A00 != r4.A00 || !this.A01.equals(r4.A01) || !this.A02.equals(r4.A02)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.A00), this.A01, this.A02});
    }
}
