package X;

/* renamed from: X.3iP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74623iP extends AnonymousClass02K {
    @Override // X.AnonymousClass02K
    public boolean A00(Object obj, Object obj2) {
        return obj.equals(obj2);
    }

    @Override // X.AnonymousClass02K
    public boolean A01(Object obj, Object obj2) {
        return C12960it.A1V(((AnonymousClass4WS) obj).A00, ((AnonymousClass4WS) obj2).A00);
    }
}
