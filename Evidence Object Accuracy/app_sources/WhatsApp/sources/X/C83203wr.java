package X;

import android.view.animation.Animation;
import com.whatsapp.contact.picker.ContactPickerFragment;

/* renamed from: X.3wr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83203wr extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ ContactPickerFragment A00;

    public C83203wr(ContactPickerFragment contactPickerFragment) {
        this.A00 = contactPickerFragment;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        this.A00.A0A.setVisibility(8);
    }
}
