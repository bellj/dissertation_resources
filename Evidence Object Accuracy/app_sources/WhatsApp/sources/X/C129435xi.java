package X;

import android.content.SharedPreferences;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5xi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129435xi {
    public SharedPreferences A00;

    public C129435xi(C16630pM r2) {
        this.A00 = r2.A01("novi_funding_source_config");
    }

    public AnonymousClass602 A00() {
        String string = this.A00.getString("view_config_json", null);
        if (string != null && !TextUtils.isEmpty(string)) {
            try {
                AnonymousClass602 r6 = new AnonymousClass602();
                JSONObject jSONObject = C13000ix.A05(string).getJSONObject("funding_source_config");
                JSONArray jSONArray = jSONObject.getJSONArray("deposit");
                for (int i = 0; i < jSONArray.length(); i++) {
                    r6.A01.add(jSONArray.getString(i));
                }
                JSONArray jSONArray2 = jSONObject.getJSONArray("withdrawal");
                for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                    r6.A03.add(jSONArray2.getString(i2));
                }
                JSONArray jSONArray3 = jSONObject.getJSONArray("payment_settings");
                for (int i3 = 0; i3 < jSONArray3.length(); i3++) {
                    r6.A02.add(jSONArray3.getString(i3));
                }
                JSONArray jSONArray4 = jSONObject.getJSONArray("balance_top_up");
                for (int i4 = 0; i4 < jSONArray4.length(); i4++) {
                    r6.A00.add(jSONArray4.getString(i4));
                }
                return r6;
            } catch (JSONException e) {
                Log.e("[PAY] noviFundingSourceViewConfigCache/getCachedViewConfig/error converting from JSON: ", e);
            }
        }
        return null;
    }

    public void A01(AnonymousClass602 r4) {
        try {
            JSONObject A0a = C117295Zj.A0a();
            A0a.put("deposit", AnonymousClass602.A00(r4.A01));
            A0a.put("withdrawal", AnonymousClass602.A00(r4.A03));
            A0a.put("payment_settings", AnonymousClass602.A00(r4.A02));
            A0a.put("balance_top_up", AnonymousClass602.A00(r4.A00));
            C12970iu.A1D(this.A00.edit(), "view_config_json", C117295Zj.A0a().put("funding_source_config", A0a).toString());
        } catch (JSONException e) {
            Log.e("[PAY] noviFundingSourceViewConfigCache/cacheViewConfig/error converting to JSON: ", e);
        }
    }
}
