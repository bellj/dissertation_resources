package X;

import android.view.MenuItem;
import com.whatsapp.wabloks.ui.WaBloksActivity;

/* renamed from: X.5pY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124465pY extends AbstractC119415dw {
    public MenuItem A00;
    public MenuItem A01;
    public C126195sU A02;

    public C124465pY(AnonymousClass018 r1, WaBloksActivity waBloksActivity) {
        super(r1, waBloksActivity);
    }

    public final void A01() {
        MenuItem menuItem;
        MenuItem menuItem2;
        C126195sU r0 = this.A02;
        if (r0 != null) {
            if (r0.A00.A0O(41, false) && (menuItem2 = this.A00) != null) {
                menuItem2.setVisible(true);
            }
            if (this.A02.A00.A0O(44, false) && (menuItem = this.A01) != null) {
                menuItem.setVisible(true);
            }
        }
    }
}
