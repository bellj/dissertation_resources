package X;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.ScaleAnimation;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.preference.WaFontListPreference;

/* renamed from: X.2yj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60842yj extends AnonymousClass2xk {
    public int A00;
    public View A01;
    public TextEmojiLabel A02;
    public C64343Fe A03;
    public C26171Ch A04;
    public C17990rj A05;
    public C22180yf A06;
    public AbstractC35401hl A07;

    public C60842yj(Context context, AbstractC13890kV r5, C28861Ph r6) {
        super(context, r5, r6);
        AbstractC35401hl A00 = C65213Iq.A00(context);
        TextEmojiLabel A0T = C12970iu.A0T(this, R.id.message_text);
        this.A02 = A0T;
        AbstractC28491Nn.A03(A0T);
        this.A02.setAutoLinkMask(0);
        this.A02.setLinksClickable(false);
        this.A02.setFocusable(false);
        this.A02.setClickable(false);
        this.A02.setLongClickable(false);
        this.A07 = A00;
        A1M();
    }

    public static void A0X(View view) {
        ScaleAnimation scaleAnimation = new ScaleAnimation(0.85f, 0.8f, 0.85f, 0.8f, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setInterpolator(new DecelerateInterpolator());
        scaleAnimation.setDuration(500);
        scaleAnimation.setRepeatMode(2);
        scaleAnimation.setRepeatCount(-1);
        scaleAnimation.setFillBefore(true);
        scaleAnimation.setFillAfter(true);
        view.startAnimation(scaleAnimation);
    }

    @Override // X.AnonymousClass1OY
    public void A0s() {
        A1M();
        A1H(false);
    }

    @Override // X.AnonymousClass1OY
    public void A10(int i) {
        int paddingLeft;
        int dimensionPixelOffset;
        super.A10(i);
        View A0D = AnonymousClass028.A0D(this, R.id.conversation_text_row);
        if (((AbstractC28551Oa) this).A0O.A0E() == null && !A1N()) {
            if (AnonymousClass1OY.A0V(((AbstractC28551Oa) this).A0O, i, ((AbstractC28551Oa) this).A0R)) {
                A0D = this.A02;
                paddingLeft = A0D.getPaddingLeft();
                dimensionPixelOffset = 0;
            } else {
                paddingLeft = A0D.getPaddingLeft();
                dimensionPixelOffset = C12960it.A09(this).getDimensionPixelOffset(R.dimen.space_xTight);
            }
            C12990iw.A1A(A0D, paddingLeft, dimensionPixelOffset);
        }
    }

    @Override // X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r4, boolean z) {
        boolean A1X = C12960it.A1X(r4, ((AbstractC28551Oa) this).A0O);
        super.A1D(r4, z);
        if (z || A1X) {
            A1M();
            return;
        }
        TextEmojiLabel textEmojiLabel = this.A02;
        if (textEmojiLabel.getAnimation() == null) {
            String A0I = ((AbstractC28551Oa) this).A0O.A0I();
            AnonymousClass009.A05(A0I);
            if (C65213Iq.A01(((AbstractC28551Oa) this).A0L, A0I) != null) {
                A0X(textEmojiLabel);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0098, code lost:
        if (r14.A04 == false) goto L_0x009a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00bb, code lost:
        if (r6.A07(1961) == false) goto L_0x00bd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0101, code lost:
        if (android.text.TextUtils.isEmpty(com.whatsapp.acceptinvitelink.AcceptInviteLinkActivity.A02(android.net.Uri.parse(r8))) != false) goto L_0x0103;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0127, code lost:
        if (r3 != false) goto L_0x0129;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x002e, code lost:
        if (r27.A04.A00(r13) != false) goto L_0x0030;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x01e7, code lost:
        if (r17 != false) goto L_0x00e5;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A1M() {
        /*
        // Method dump skipped, instructions count: 532
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C60842yj.A1M():void");
    }

    public final boolean A1N() {
        AbstractC15340mz r6 = ((AbstractC28551Oa) this).A0O;
        C15570nT r1 = ((AnonymousClass1OY) this).A0L;
        C21250x7 r4 = this.A0v;
        C15550nR r2 = ((AnonymousClass1OY) this).A0X;
        C20710wC r5 = this.A11;
        C20830wO r3 = this.A0o;
        if ((TextUtils.isEmpty(AnonymousClass3IJ.A00(r6).A03) || !C65213Iq.A02(r1, r2, r3, r4, r5, r6)) && ((AbstractC28551Oa) this).A0O.A0N == null) {
            return false;
        }
        return true;
    }

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_text_center;
    }

    @Override // X.AbstractC28551Oa
    public C28861Ph getFMessage() {
        return (C28861Ph) ((AbstractC28551Oa) this).A0O;
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_text_left;
    }

    @Override // X.AbstractC28551Oa
    public int getMainChildMaxWidth() {
        if (this.A00 != 0) {
            return AnonymousClass3GD.A01(getContext(), this.A00);
        }
        return 0;
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_text_right;
    }

    @Override // X.AnonymousClass1OY
    public float getTextFontSize() {
        String A0I = ((AbstractC28551Oa) this).A0O.A0I();
        AnonymousClass009.A05(A0I);
        int A00 = AbstractC32741cf.A00(A0I);
        if (A00 <= 0 || A00 > 3) {
            return super.getTextFontSize();
        }
        float A02 = AnonymousClass1OY.A02(getResources(), ((AbstractC28551Oa) this).A0K, WaFontListPreference.A00);
        return A02 + (((Math.max(A02, Math.min(A02, (C12990iw.A0K(this).density * A02) / C12990iw.A0K(this).scaledDensity) * 1.5f) - A02) * ((float) (4 - A00))) / 3.0f);
    }

    @Override // X.AbstractC28551Oa
    public void setFMessage(AbstractC15340mz r2) {
        AnonymousClass009.A0F(r2 instanceof C28861Ph);
        ((AbstractC28551Oa) this).A0O = r2;
    }
}
