package X;

/* renamed from: X.69A  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass69A implements AnonymousClass17W {
    public final C14850m9 A00;
    public final AnonymousClass6BE A01;

    @Override // X.AnonymousClass17W
    public String ABE() {
        return "campaignID";
    }

    public AnonymousClass69A(C14850m9 r1, AnonymousClass6BE r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass17W
    public boolean A99(String str) {
        return "upi".equals(str);
    }

    @Override // X.AnonymousClass17W
    public String AAu() {
        return this.A00.A03(796);
    }
}
