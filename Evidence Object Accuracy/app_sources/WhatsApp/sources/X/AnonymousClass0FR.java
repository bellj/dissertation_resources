package X;

/* renamed from: X.0FR  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0FR extends AbstractC02880Ff {
    public final /* synthetic */ C07750a1 A00;

    @Override // X.AbstractC05330Pd
    public String A01() {
        return "INSERT OR IGNORE INTO `WorkTag` (`tag`,`work_spec_id`) VALUES (?,?)";
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass0FR(AnonymousClass0QN r1, C07750a1 r2) {
        super(r1);
        this.A00 = r2;
    }

    @Override // X.AbstractC02880Ff
    public void A03(AbstractC12830ic r3, Object obj) {
        AnonymousClass0N6 r4 = (AnonymousClass0N6) obj;
        String str = r4.A00;
        if (str == null) {
            r3.A6T(1);
        } else {
            r3.A6U(1, str);
        }
        String str2 = r4.A01;
        if (str2 == null) {
            r3.A6T(2);
        } else {
            r3.A6U(2, str2);
        }
    }
}
