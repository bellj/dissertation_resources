package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;
import com.whatsapp.util.Log;

/* renamed from: X.0vb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20340vb {
    public final C16490p7 A00;
    public final C14850m9 A01;

    public C20340vb(C16490p7 r1, C14850m9 r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public final C30921Zi A00(Cursor cursor, String str) {
        String str2 = str;
        if (str == null) {
            str2 = cursor.getString(cursor.getColumnIndexOrThrow("background_id"));
        }
        long j = cursor.getLong(cursor.getColumnIndexOrThrow("file_size"));
        int i = cursor.getInt(cursor.getColumnIndexOrThrow("width"));
        int i2 = cursor.getInt(cursor.getColumnIndexOrThrow("height"));
        C30921Zi r9 = new C30921Zi(str2, cursor.getString(cursor.getColumnIndexOrThrow("mime_type")), cursor.getString(cursor.getColumnIndexOrThrow("fullsize_url")), cursor.getString(cursor.getColumnIndexOrThrow("description")), cursor.getString(cursor.getColumnIndexOrThrow("lg")), i, i2, cursor.getInt(cursor.getColumnIndexOrThrow("placeholder_color")), cursor.getInt(cursor.getColumnIndexOrThrow("text_color")), cursor.getInt(cursor.getColumnIndexOrThrow("subtext_color")), j);
        C14850m9 r8 = this.A01;
        if (r8.A07(1084)) {
            byte[] blob = cursor.getBlob(cursor.getColumnIndexOrThrow("media_key"));
            long j2 = cursor.getLong(cursor.getColumnIndexOrThrow("media_key_timestamp"));
            String string = cursor.getString(cursor.getColumnIndexOrThrow("file_sha256"));
            String string2 = cursor.getString(cursor.getColumnIndexOrThrow("file_enc_sha256"));
            String string3 = cursor.getString(cursor.getColumnIndexOrThrow("direct_path"));
            boolean A07 = r8.A07(1084);
            r9.A08 = blob;
            r9.A00 = j2;
            r9.A04 = string;
            r9.A03 = string2;
            r9.A02 = string3;
            r9.A07 = A07;
        }
        return r9;
    }

    public C30921Zi A01(String str) {
        StringBuilder sb = new StringBuilder("PAY: PaymentBackgroundStore/getPaymentBackgroundById/id=");
        sb.append(str);
        Log.i(sb.toString());
        C16310on A01 = this.A00.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT file_size, width, height, mime_type, placeholder_color, text_color, subtext_color, media_key, media_key_timestamp, file_sha256, file_enc_sha256, direct_path, fullsize_url, description, lg FROM payment_background WHERE background_id =?", new String[]{str});
            if (A09.moveToNext()) {
                C30921Zi A00 = A00(A09, str);
                A09.close();
                A01.close();
                return A00;
            }
            A09.close();
            A01.close();
            StringBuilder sb2 = new StringBuilder("PAY: PaymentBackgroundStore/getPaymentBackgroundById/no background found for id=");
            sb2.append(str);
            Log.i(sb2.toString());
            return null;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final void A02(C16310on r9, C30921Zi r10) {
        byte[] bArr;
        long j;
        String str;
        String str2;
        int i;
        String str3 = r10.A05;
        boolean z = !TextUtils.isEmpty(str3);
        boolean A07 = this.A01.A07(1084);
        ContentValues contentValues = new ContentValues(15);
        String str4 = r10.A0F;
        contentValues.put("background_id", str4);
        contentValues.put("file_size", Long.valueOf(r10.A0E));
        contentValues.put("width", Integer.valueOf(r10.A0D));
        contentValues.put("height", Integer.valueOf(r10.A09));
        contentValues.put("mime_type", r10.A0G);
        contentValues.put("placeholder_color", Integer.valueOf(r10.A0A));
        contentValues.put("text_color", Integer.valueOf(r10.A0C));
        contentValues.put("subtext_color", Integer.valueOf(r10.A0B));
        String str5 = null;
        if (A07) {
            bArr = r10.A08;
        } else {
            bArr = null;
        }
        C30021Vq.A06(contentValues, "media_key", bArr);
        if (A07) {
            j = r10.A00;
        } else {
            j = 0;
        }
        contentValues.put("media_key_timestamp", Long.valueOf(j));
        if (A07) {
            str = r10.A04;
        } else {
            str = null;
        }
        C30021Vq.A04(contentValues, "file_sha256", str);
        if (A07) {
            str2 = r10.A03;
        } else {
            str2 = null;
        }
        C30021Vq.A04(contentValues, "file_enc_sha256", str2);
        if (A07) {
            str5 = r10.A02;
        }
        C30021Vq.A04(contentValues, "direct_path", str5);
        if (z) {
            contentValues.put("fullsize_url", str3);
            C30021Vq.A04(contentValues, "description", r10.A01);
            C30021Vq.A04(contentValues, "lg", r10.A06);
            i = 5;
        } else {
            i = 4;
        }
        if (r9.A03.A06(contentValues, "payment_background", i) == -1) {
            StringBuilder sb = new StringBuilder("PAY: PaymentBackgroundStore/insertOrReplacePaymentBackground/shouldReplace: ");
            sb.append(z);
            sb.append(", failed for id: ");
            sb.append(str4);
            Log.e(sb.toString());
        }
    }

    public void A03(C30921Zi r3) {
        StringBuilder sb = new StringBuilder("PAY: PaymentBackgroundStore/insertOrReplacePaymentBackground/id=");
        sb.append(r3.A0F);
        Log.i(sb.toString());
        C16310on A02 = this.A00.A02();
        try {
            A02(A02, r3);
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
