package X;

/* renamed from: X.3eG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C72253eG extends AnonymousClass1WI implements AnonymousClass1J7 {
    public final /* synthetic */ AnonymousClass4EX $entryPoint;
    public final /* synthetic */ AnonymousClass3DF this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C72253eG(AnonymousClass4EX r2, AnonymousClass3DF r3) {
        super(1);
        this.this$0 = r3;
        this.$entryPoint = r2;
    }

    @Override // X.AnonymousClass1J7
    public /* bridge */ /* synthetic */ Object AJ4(Object obj) {
        int i;
        boolean A1Y = C12970iu.A1Y(obj);
        this.this$0.A01.setVisibility(C13010iy.A00(A1Y ? 1 : 0));
        if (!A1Y) {
            AnonymousClass3DF r2 = this.this$0;
            AnonymousClass4EX r1 = this.$entryPoint;
            if (C16700pc.A0O(r1, C862046f.A00)) {
                i = 14;
            } else if (C16700pc.A0O(r1, C862146g.A00)) {
                i = 9;
            } else {
                throw C12990iw.A0v();
            }
            r2.A05.A02(i);
        }
        return AnonymousClass1WZ.A00;
    }
}
