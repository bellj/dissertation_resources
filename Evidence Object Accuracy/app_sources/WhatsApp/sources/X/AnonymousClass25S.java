package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.25S  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass25S extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass25S A03;
    public static volatile AnonymousClass255 A04;
    public int A00;
    public long A01;
    public AnonymousClass1G8 A02;

    static {
        AnonymousClass25S r0 = new AnonymousClass25S();
        A03 = r0;
        r0.A0W();
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            AnonymousClass1G8 r0 = this.A02;
            if (r0 == null) {
                r0 = AnonymousClass1G8.A05;
            }
            i2 = 0 + CodedOutputStream.A0A(r0, 1);
        }
        if ((this.A00 & 2) == 2) {
            i2 += CodedOutputStream.A05(2, this.A01);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            AnonymousClass1G8 r0 = this.A02;
            if (r0 == null) {
                r0 = AnonymousClass1G8.A05;
            }
            codedOutputStream.A0L(r0, 1);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0H(2, this.A01);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
