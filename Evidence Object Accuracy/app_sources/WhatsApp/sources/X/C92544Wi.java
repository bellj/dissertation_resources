package X;

/* renamed from: X.4Wi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C92544Wi {
    public final Object A00;
    public final String A01 = "LocationListener";

    public C92544Wi(Object obj) {
        this.A00 = obj;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C92544Wi) {
                C92544Wi r5 = (C92544Wi) obj;
                if (this.A00 != r5.A00 || !this.A01.equals(r5.A01)) {
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (System.identityHashCode(this.A00) * 31) + this.A01.hashCode();
    }
}
