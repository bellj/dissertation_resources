package X;

import android.view.ViewTreeObserver;

/* renamed from: X.4ne  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC101514ne implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ AnonymousClass2Ew A00;

    public ViewTreeObserver$OnGlobalLayoutListenerC101514ne(AnonymousClass2Ew r1) {
        this.A00 = r1;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        AnonymousClass2Ew r3 = this.A00;
        C12980iv.A1F(r3.A0F, this);
        if (!r3.A0W) {
            int A03 = r3.A03(r3.getMeasuredWidth()) - r3.A02(r3.getMeasuredWidth());
            r3.A0F.setSelectionFromTop(0, A03);
            r3.setScrollPos(A03);
        }
    }
}
