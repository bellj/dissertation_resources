package X;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.SparseArray;

/* renamed from: X.3SN  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3SN implements AbstractC117045Ya {
    public int A00;
    public int A01;
    public Bitmap.Config A02 = Bitmap.Config.ARGB_8888;
    public Rect A03;
    public final Paint A04;
    public final AnonymousClass5WT A05;
    public final AnonymousClass5XK A06;
    public final AnonymousClass4SI A07;
    public final C93144Zh A08;
    public final C63693Co A09;
    public final AnonymousClass4UM A0A;

    public AnonymousClass3SN(AnonymousClass5WT r2, AnonymousClass5XK r3, AnonymousClass4SI r4, C93144Zh r5, C63693Co r6, AnonymousClass4UM r7) {
        this.A0A = r7;
        this.A06 = r3;
        this.A05 = r2;
        this.A09 = r6;
        this.A08 = r5;
        this.A07 = r4;
        this.A04 = C12990iw.A0G(6);
        A00();
    }

    public final void A00() {
        int width;
        C63693Co r2 = this.A09;
        int width2 = r2.A00.A04.getWidth();
        this.A01 = width2;
        int i = -1;
        if (width2 == -1) {
            Rect rect = this.A03;
            if (rect == null) {
                width = -1;
            } else {
                width = rect.width();
            }
            this.A01 = width;
        }
        int height = r2.A00.A04.getHeight();
        this.A00 = height;
        if (height == -1) {
            Rect rect2 = this.A03;
            if (rect2 != null) {
                i = rect2.height();
            }
            this.A00 = i;
        }
    }

    public final boolean A01(Canvas canvas, int i, int i2) {
        C08870bz AB6;
        int i3 = 3;
        boolean z = false;
        C08870bz r3 = null;
        try {
            if (i2 == 0) {
                AB6 = this.A06.AB6(i);
                z = A02(canvas, AB6, i, 0);
                i3 = 1;
            } else if (i2 == 1) {
                AB6 = this.A06.AAr(i, this.A01, this.A00);
                if (C08870bz.A01(AB6)) {
                    if (!this.A09.A00((Bitmap) AB6.A04(), i)) {
                        AB6.close();
                    } else if (A02(canvas, AB6, i, 1)) {
                        z = true;
                    }
                }
                i3 = 2;
            } else if (i2 != 2) {
                if (i2 == 3) {
                    AB6 = this.A06.ACt(i);
                    z = A02(canvas, AB6, i, 3);
                    i3 = -1;
                }
                return z;
            } else {
                try {
                    AB6 = this.A0A.A00(this.A02, this.A01, this.A00);
                } catch (RuntimeException e) {
                    AbstractC12710iN r1 = AnonymousClass0UN.A00;
                    if (r1.AJi(5)) {
                        r1.Ag1(AnonymousClass3SN.class.getSimpleName(), "Failed to create frame bitmap", e);
                        return false;
                    }
                }
                if (C08870bz.A01(AB6)) {
                    if (!this.A09.A00((Bitmap) AB6.A04(), i)) {
                        AB6.close();
                    } else if (A02(canvas, AB6, i, 2)) {
                        z = true;
                    }
                    if (!z && i3 != -1) {
                        return A01(canvas, i, i3);
                    }
                    return z;
                }
            }
        } finally {
            if (r3 != null) {
                r3.close();
            }
        }
    }

    public final boolean A02(Canvas canvas, C08870bz r6, int i, int i2) {
        if (!C08870bz.A01(r6)) {
            return false;
        }
        Rect rect = this.A03;
        Bitmap bitmap = (Bitmap) r6.A04();
        if (rect == null) {
            canvas.drawBitmap(bitmap, 0.0f, 0.0f, this.A04);
        } else {
            canvas.drawBitmap(bitmap, (Rect) null, this.A03, this.A04);
        }
        if (i2 == 3) {
            return true;
        }
        this.A06.AQq(r6, i, i2);
        return true;
    }

    @Override // X.AbstractC117045Ya
    public boolean A9D(Canvas canvas, Drawable drawable, int i) {
        AnonymousClass4SI r11;
        boolean A01 = A01(canvas, i, 0);
        C93144Zh r6 = this.A08;
        if (!(r6 == null || (r11 = this.A07) == null)) {
            AnonymousClass5XK r10 = this.A06;
            for (int i2 = 1; i2 <= r6.A00; i2++) {
                int frameCount = (i + i2) % this.A05.getFrameCount();
                if (AnonymousClass0UN.A00.AJi(2)) {
                    AnonymousClass0UN.A01(C93144Zh.class, Integer.valueOf(frameCount), Integer.valueOf(i), "Preparing frame %d, last drawn: %d");
                }
                int hashCode = (hashCode() * 31) + frameCount;
                SparseArray sparseArray = r11.A01;
                synchronized (sparseArray) {
                    if (sparseArray.get(hashCode) != null) {
                        AnonymousClass0UN.A02(AnonymousClass4SI.class, Integer.valueOf(frameCount), "Already scheduled decode job for frame %d");
                    } else if (r10.A7c(frameCount)) {
                        AnonymousClass0UN.A02(AnonymousClass4SI.class, Integer.valueOf(frameCount), "Frame %d is cached already.");
                    } else {
                        RunnableC55812jN r8 = new RunnableC55812jN(this, r10, r11, frameCount, hashCode);
                        sparseArray.put(hashCode, r8);
                        r11.A04.execute(r8);
                    }
                }
            }
        }
        return A01;
    }

    @Override // X.AnonymousClass5WT
    public int AD9(int i) {
        return this.A05.AD9(i);
    }

    @Override // X.AbstractC117045Ya
    public int ADX() {
        return this.A00;
    }

    @Override // X.AbstractC117045Ya
    public int ADY() {
        return this.A01;
    }

    @Override // X.AbstractC117045Ya
    public void Abj(int i) {
        this.A04.setAlpha(i);
    }

    @Override // X.AbstractC117045Ya
    public void Abp(Rect rect) {
        this.A03 = rect;
        C63693Co r4 = this.A09;
        AnonymousClass3HI r3 = r4.A00;
        if (!AnonymousClass3HI.A00(rect, r3.A04).equals(r3.A03)) {
            r3 = new AnonymousClass3HI(rect, r3.A05, r3.A06, r3.A07);
        }
        if (r3 != r4.A00) {
            r4.A00 = r3;
            r4.A01 = new AnonymousClass3FZ(r3, r4.A03);
        }
        A00();
    }

    @Override // X.AbstractC117045Ya
    public void Abu(ColorFilter colorFilter) {
        this.A04.setColorFilter(colorFilter);
    }

    @Override // X.AnonymousClass5WT
    public int getFrameCount() {
        return this.A05.getFrameCount();
    }

    @Override // X.AnonymousClass5WT
    public int getLoopCount() {
        return this.A05.getLoopCount();
    }
}
