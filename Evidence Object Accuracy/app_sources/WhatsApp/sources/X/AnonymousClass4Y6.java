package X;

/* renamed from: X.4Y6  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass4Y6 {
    public void A00(C94804cZ r2, C94804cZ r3) {
        if (!(this instanceof C81323tp)) {
            ((C81333tq) this).A02.lazySet(r2, r3);
        } else {
            r2.next = r3;
        }
    }

    public void A01(C94804cZ r2, Thread thread) {
        if (!(this instanceof C81323tp)) {
            ((C81333tq) this).A03.lazySet(r2, thread);
        } else {
            r2.thread = thread;
        }
    }

    public boolean A02(C94814ca r2, C94814ca r3, AbstractC81363tt r4) {
        boolean z;
        if (!(this instanceof C81323tp)) {
            return AnonymousClass0KN.A00(r4, r2, r3, ((C81333tq) this).A00);
        }
        synchronized (r4) {
            if (r4.listeners == r2) {
                r4.listeners = r3;
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    public boolean A03(C94804cZ r2, C94804cZ r3, AbstractC81363tt r4) {
        boolean z;
        if (!(this instanceof C81323tp)) {
            return AnonymousClass0KN.A00(r4, r2, r3, ((C81333tq) this).A04);
        }
        synchronized (r4) {
            if (r4.waiters == r2) {
                r4.waiters = r3;
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    public boolean A04(AbstractC81363tt r3, Object obj, Object obj2) {
        boolean z;
        if (!(this instanceof C81323tp)) {
            return AnonymousClass0KN.A00(r3, null, obj2, ((C81333tq) this).A01);
        }
        synchronized (r3) {
            if (r3.value == null) {
                r3.value = obj2;
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }
}
