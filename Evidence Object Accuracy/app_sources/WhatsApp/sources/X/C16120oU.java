package X;

import android.content.SharedPreferences;
import com.facebook.redex.RunnableBRunnable0Shape0S0110000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0111000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0201000_I0;
import com.facebook.redex.RunnableBRunnable0Shape6S0100000_I0_6;
import com.whatsapp.fieldstats.events.WamCall;
import com.whatsapp.util.Log;
import java.util.Random;
import java.util.concurrent.CountDownLatch;

/* renamed from: X.0oU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16120oU {
    public AnonymousClass1N8 A00;
    public AnonymousClass1N8 A01;
    public AnonymousClass1N8 A02;
    public AnonymousClass1N7 A03;
    public AnonymousClass1N7 A04;
    public AnonymousClass1N7 A05;
    public AbstractC22800zf A06;
    public AbstractC22800zf A07;
    public boolean A08;
    public boolean A09;
    public boolean A0A;
    public final C14820m6 A0B;
    public final C14850m9 A0C;
    public final AnonymousClass16G A0D;
    public final AnonymousClass16D A0E;
    public final AnonymousClass15Y A0F;
    public final AnonymousClass16F A0G;
    public final AnonymousClass16E A0H;
    public final CountDownLatch A0I = new CountDownLatch(1);
    public final CountDownLatch A0J = new CountDownLatch(1);
    public final CountDownLatch A0K = new CountDownLatch(1);
    public volatile int A0L;
    public volatile boolean A0M;
    public volatile boolean A0N;
    public volatile boolean A0O;

    public C16120oU(C14820m6 r3, C14850m9 r4, AnonymousClass16G r5, AnonymousClass16D r6, AnonymousClass15Y r7, AnonymousClass16F r8, AnonymousClass16E r9) {
        this.A0C = r4;
        this.A0E = r6;
        this.A0H = r9;
        this.A0G = r8;
        this.A0F = r7;
        this.A0B = r3;
        this.A0D = r5;
        this.A0O = false;
        this.A0M = false;
        this.A0N = false;
    }

    public final Integer A00(AnonymousClass00E r10, int i, boolean z) {
        SharedPreferences sharedPreferences;
        AnonymousClass00E r3 = r10;
        AnonymousClass16G r2 = this.A0D;
        synchronized (r2) {
            sharedPreferences = r2.A00;
            if (sharedPreferences == null) {
                sharedPreferences = r2.A01.A02("field-stats-events-sampling");
                r2.A00 = sharedPreferences;
            }
        }
        int i2 = sharedPreferences.getInt(String.valueOf(i), 0);
        if (i2 != 0) {
            int abs = Math.abs(i2);
            r3 = new AnonymousClass00E(false, abs, abs, abs, abs);
        } else {
            i2 = r10.A03 * 1;
            if (z) {
                i2 = -i2;
            }
        }
        if (r3.A00()) {
            return Integer.valueOf(i2);
        }
        return null;
    }

    public void A01() {
        this.A0H.A01.execute(new RunnableBRunnable0Shape6S0100000_I0_6(this, 26));
    }

    public final void A02() {
        if (this.A03.A00() > this.A01.A00.A8e().A04.A05.remaining() && !this.A01.A00.A8e().A05()) {
            if (this.A01.A00.A6w()) {
                this.A01.A02();
                this.A03.A01();
            } else {
                A0D("no space in buffer for more events");
                return;
            }
        }
        if (this.A03.A00() > this.A01.A00()) {
            Log.e("wamruntime/logPrivateStatsEventInternal: dropping event because it is larger than the buffer itself");
            return;
        }
        AnonymousClass1N8 r2 = this.A01;
        AnonymousClass1N7 r0 = this.A03;
        r2.A04(r0.A00, r0.A01);
        this.A01.A01();
    }

    public final void A03() {
        if (this.A04.A00() > this.A02.A00.A8e().A04.A05.remaining() && !this.A02.A00.A8e().A05()) {
            if (this.A02.A00.A6w()) {
                this.A02.A02();
                this.A04.A01();
            } else {
                A04();
                return;
            }
        }
        if (this.A04.A00() > this.A02.A00()) {
            Log.e("wamruntime/logevent: dropping wam real time event because it is larger than the buffer itself");
            return;
        }
        AnonymousClass1N8 r2 = this.A02;
        AnonymousClass1N7 r0 = this.A04;
        r2.A04(r0.A00, r0.A01);
        this.A02.A01();
        this.A07.AbY(this.A02, false);
    }

    public final void A04() {
        AnonymousClass16F r5 = this.A0G;
        Long l = r5.A0I;
        if (l == null) {
            r5.A0I = 0L;
            l = 0L;
        }
        Long l2 = r5.A0J;
        if (l2 == null) {
            r5.A0J = 0L;
            l2 = 0L;
        }
        r5.A0I = Long.valueOf(l.longValue() + 1);
        r5.A0J = Long.valueOf(l2.longValue() + ((long) this.A05.A00()));
        r5.A05();
        Log.w("wamruntime/logevent: no room for a new event record");
    }

    public void A05(AbstractC16110oT r3) {
        A0B(r3, null, true);
    }

    @Deprecated
    public void A06(AbstractC16110oT r2) {
        A09(r2, 1);
        A0C(r2, "");
    }

    public void A07(AbstractC16110oT r3) {
        A0B(r3, null, false);
    }

    public void A08(AbstractC16110oT r3, int i) {
        A09(r3, i);
        StringBuilder sb = new StringBuilder("(with weight=");
        sb.append(i);
        sb.append(")");
        A0C(r3, sb.toString());
    }

    public final void A09(AbstractC16110oT r3, int i) {
        RunnableBRunnable0Shape0S0201000_I0 runnableBRunnable0Shape0S0201000_I0;
        ExecutorC27271Gr r0;
        if (r3.channel != 2) {
            runnableBRunnable0Shape0S0201000_I0 = new RunnableBRunnable0Shape0S0201000_I0(this, r3, i, 22);
            r0 = this.A0H.A01;
        } else {
            runnableBRunnable0Shape0S0201000_I0 = new RunnableBRunnable0Shape0S0201000_I0(this, r3, i, 21);
            r0 = this.A0H.A02;
        }
        r0.execute(runnableBRunnable0Shape0S0201000_I0);
    }

    public final void A0A(AbstractC16110oT r3, int i, boolean z) {
        if (A0H()) {
            this.A05.A03(r3, i);
            this.A05.A01();
            A0G(z);
            AnonymousClass16F r1 = this.A0G;
            if (r3 == r1) {
                r1.A0I = null;
                r1.A0J = null;
                r1.A00 = null;
                r1.A0K = null;
                r1.A0L = null;
                r1.A0M = null;
                r1.A0N = null;
                r1.A01 = null;
                r1.A02 = null;
                r1.A03 = null;
                r1.A04 = null;
                r1.A05 = null;
                r1.A06 = null;
                r1.A07 = null;
                r1.A08 = null;
                r1.A09 = null;
                r1.A0A = null;
                r1.A0B = null;
                r1.A0C = null;
                r1.A0D = null;
                r1.A0E = null;
                r1.A0F = null;
                r1.A0G = null;
                r1.A0H = null;
                r1.A05();
            }
            if (!z) {
                this.A00.A01();
            }
        }
    }

    public void A0B(AbstractC16110oT r4, AnonymousClass00E r5, boolean z) {
        String str;
        int i = r4.code;
        if (r5 == null) {
            r5 = r4.samplingRate;
        }
        Integer A00 = A00(r5, i, z);
        if (A00 != null) {
            A09(r4, A00.intValue());
            StringBuilder sb = new StringBuilder("(sampled with weight ");
            sb.append(A00);
            sb.append(")");
            str = sb.toString();
        } else {
            str = "(dropped)";
        }
        A0C(r4, str);
    }

    public final void A0C(AbstractC16110oT r4, String str) {
        if (r4 instanceof WamCall) {
            StringBuilder sb = new StringBuilder();
            sb.append("wamruntime/printevent");
            sb.append(str);
            sb.append(": ");
            sb.append(r4.toString());
            Log.i(sb.toString());
        }
    }

    public final void A0D(String str) {
        AnonymousClass16F r5 = this.A0G;
        Long l = r5.A0K;
        if (l == null) {
            r5.A0K = 0L;
            l = 0L;
        }
        Long l2 = r5.A0L;
        if (l2 == null) {
            r5.A0L = 0L;
            l2 = 0L;
        }
        r5.A0K = Long.valueOf(l.longValue() + 1);
        r5.A0L = Long.valueOf(l2.longValue() + ((long) this.A03.A00()));
        r5.A05();
        StringBuilder sb = new StringBuilder("wamruntime/recordPrivateStatsDroppedEvent: ");
        sb.append(str);
        Log.w(sb.toString());
    }

    public final void A0E(CountDownLatch countDownLatch) {
        ExecutorC27271Gr r0;
        CountDownLatch countDownLatch2 = this.A0J;
        AnonymousClass16E r02 = this.A0H;
        if (countDownLatch == countDownLatch2) {
            r0 = r02.A02;
        } else {
            r0 = r02.A01;
        }
        long j = r0.A05;
        Thread currentThread = Thread.currentThread();
        boolean z = false;
        if (j == currentThread.getId()) {
            z = true;
        }
        AnonymousClass009.A0A("Not running on this SerialExecutor", z);
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            StringBuilder sb = new StringBuilder("wamruntime: unexpected thread interrupt (");
            sb.append(e);
            sb.append(")");
            Log.a(sb.toString());
            currentThread.interrupt();
        }
    }

    public void A0F(boolean z) {
        int nextInt;
        RunnableBRunnable0Shape0S0110000_I0 runnableBRunnable0Shape0S0110000_I0 = new RunnableBRunnable0Shape0S0110000_I0(this, 10, z);
        AnonymousClass16E r2 = this.A0H;
        r2.A01.execute(runnableBRunnable0Shape0S0110000_I0);
        if (this.A0L <= 0 || (nextInt = new Random().nextInt(this.A0L) + 5) <= 0) {
            r2.A02.execute(new RunnableBRunnable0Shape0S0110000_I0(this, 9, z));
        } else {
            r2.A02.A02(new RunnableBRunnable0Shape0S0111000_I0(this, nextInt, 1, z), (long) (nextInt * 1000));
        }
    }

    public final void A0G(boolean z) {
        if (this.A05.A00() > this.A00.A00.A8e().A04.A05.remaining() && !this.A00.A00.A8e().A05()) {
            if (this.A00.A00.A6w()) {
                this.A00.A02();
                this.A0B.A1D(false);
                this.A05.A01();
            } else {
                A04();
                return;
            }
        }
        if (this.A05.A00() > this.A00.A00()) {
            Log.e("wamruntime/logevent: dropping event because it is larger than the buffer itself");
            return;
        }
        AnonymousClass1N8 r2 = this.A00;
        AnonymousClass1N7 r0 = this.A05;
        r2.A04(r0.A00, r0.A01);
        AnonymousClass1N8 r3 = this.A00;
        AnonymousClass1NA A8e = r3.A00.A8e();
        if (!A8e.A04()) {
            throw new UnsupportedOperationException("No event count available for rotated buffers");
        } else if (A8e.A00 == 1 && !z) {
            if (!r3.A02) {
                this.A0G.A0C = Boolean.TRUE;
            }
            AnonymousClass16F r1 = this.A0G;
            if (!r1.A0A()) {
                A0A(r1, 0, true);
            }
        }
    }

    public final boolean A0H() {
        if (!this.A08) {
            A0E(this.A0I);
            this.A0F.A00(0);
            this.A08 = true;
        }
        return this.A00.A02;
    }

    public final boolean A0I() {
        if (!this.A09) {
            A0E(this.A0J);
            this.A0F.A00(2);
            this.A09 = true;
        }
        return this.A01.A02;
    }

    public final boolean A0J() {
        if (!this.A0A) {
            A0E(this.A0K);
            this.A0F.A00(1);
            this.A0A = true;
        }
        return this.A02.A02;
    }
}
