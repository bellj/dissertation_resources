package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4lA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C99974lA implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new C29971Vl(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C29971Vl[i];
    }
}
