package X;

import android.database.Cursor;
import android.util.Pair;
import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.List;

/* renamed from: X.147  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass147 {
    public final C14900mE A00;
    public final C16590pI A01;
    public final AnonymousClass141 A02;
    public final AnonymousClass0o6 A03;
    public final AnonymousClass144 A04;
    public final AnonymousClass146 A05;
    public final AnonymousClass145 A06;
    public final AnonymousClass142 A07;

    public AnonymousClass147(C14900mE r1, C16590pI r2, AnonymousClass141 r3, AnonymousClass0o6 r4, AnonymousClass144 r5, AnonymousClass146 r6, AnonymousClass145 r7, AnonymousClass142 r8) {
        this.A01 = r2;
        this.A00 = r1;
        this.A05 = r6;
        this.A06 = r7;
        this.A04 = r5;
        this.A07 = r8;
        this.A03 = r4;
        this.A02 = r3;
    }

    public AnonymousClass1KZ A00(String str, String str2) {
        AnonymousClass1KZ r3;
        boolean z;
        List<AnonymousClass1KS> A01;
        String str3;
        String str4;
        String str5;
        StringBuilder sb = new StringBuilder("ThirdPartyStickerManager/fetchPack/");
        sb.append(str.hashCode());
        sb.append("/");
        sb.append(str2);
        Log.i(sb.toString());
        AnonymousClass0o6 r0 = this.A03;
        if (!r0.A02(str, str2)) {
            str5 = "ThirdPartyStickerManager/fetchPack/not using sticker cache";
        } else {
            AnonymousClass1KZ r1 = null;
            try {
                r1 = this.A04.A04(str, str2);
                if (r1.A0L) {
                    str5 = "ThirdPartyStickerManager/fetchPack/avoid caching is true";
                }
            } catch (Exception e) {
                Log.e("ThirdPartyStickerManager/fetchPack/could not fetch pack metadata", e);
            }
            C16310on A012 = r0.A00.get();
            try {
                Cursor A09 = A012.A03.A09("SELECT authority, sticker_pack_id, sticker_pack_name, sticker_pack_publisher, sticker_pack_image_data_hash, avoid_cache, is_animated_pack FROM third_party_whitelist_packs WHERE authority = ? AND sticker_pack_id = ? LIMIT 1", new String[]{str, str2});
                if (!A09.moveToNext()) {
                    r3 = null;
                } else {
                    r3 = AnonymousClass0o6.A00(A09, A09.getColumnIndexOrThrow("authority"), A09.getColumnIndexOrThrow("sticker_pack_id"), A09.getColumnIndexOrThrow("sticker_pack_name"), A09.getColumnIndexOrThrow("sticker_pack_publisher"), A09.getColumnIndex("sticker_pack_image_data_hash"), A09.getColumnIndex("avoid_cache"), A09.getColumnIndex("is_animated_pack"));
                }
                A09.close();
                if (r1 == null || !(r3 == null || (str4 = r3.A02) == null || !str4.equals(r1.A0E))) {
                    z = false;
                } else {
                    z = true;
                    r0.A01(r1, str, str2);
                    r3 = r1;
                    Log.i("ThirdPartyStickerManager/fetchPack/repopulate sticker pack db");
                    AnonymousClass142 r4 = this.A07;
                    File A013 = r4.A01(r1.A0D);
                    if (A013 != null && C14350lI.A0M(A013)) {
                        A013.toString();
                    }
                    r4.A00(r1, AnonymousClass144.A02(this.A01.A00, r1));
                }
                AnonymousClass145 r12 = this.A06;
                synchronized (r12) {
                    A01 = r12.A01(str, str2, null);
                }
                if (A01.isEmpty() || z) {
                    A01 = this.A04.A03(str, str2).A04;
                    r12.A02(str, str2, A01);
                    Log.i("ThirdPartyStickerManager/fetchPack/repopulating sticker cache");
                }
                for (AnonymousClass1KS r7 : A01) {
                    String str6 = r7.A0C;
                    if (str6 != null) {
                        A012 = this.A02.A00.get();
                        try {
                            Cursor A092 = A012.A03.A09("SELECT emojis FROM third_party_sticker_emoji_mapping WHERE plaintext_hash = ?", new String[]{str6});
                            if (A092 != null) {
                                if (A092.moveToNext()) {
                                    str3 = A092.getString(A092.getColumnIndexOrThrow("emojis"));
                                    A092.close();
                                    A012.close();
                                    r7.A06 = str3;
                                } else {
                                    A092.close();
                                }
                            }
                            A012.close();
                            str3 = null;
                            r7.A06 = str3;
                        } finally {
                        }
                    }
                }
                AnonymousClass009.A05(r3);
                r3.A04 = A01;
                if (z) {
                    this.A00.A0H(new RunnableBRunnable0Shape8S0200000_I0_8(this, 10, r3));
                }
                return r3;
            } finally {
                try {
                    A012.close();
                } catch (Throwable unused) {
                }
            }
        }
        Log.i(str5);
        return this.A04.A03(str, str2);
    }

    public File A01(String str) {
        Pair A00 = AnonymousClass144.A00(str);
        if (A00 != null) {
            if (this.A03.A02((String) A00.first, (String) A00.second)) {
                AnonymousClass142 r3 = this.A07;
                File A01 = r3.A01(str);
                if (A01 != null && A01.exists()) {
                    return A01;
                }
                try {
                    AnonymousClass1KZ A03 = this.A04.A03((String) A00.first, (String) A00.second);
                    return r3.A00(A03, AnonymousClass144.A02(this.A01.A00, A03));
                } catch (Exception e) {
                    Log.e("ThirdPartyStickerManager/getTrayIcon/error fetching pack", e);
                    return null;
                }
            }
        }
        return null;
    }
}
