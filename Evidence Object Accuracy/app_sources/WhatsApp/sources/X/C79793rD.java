package X;

import android.os.IBinder;
import com.google.android.gms.maps.internal.ICameraUpdateFactoryDelegate;

/* renamed from: X.3rD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79793rD extends C65873Li implements ICameraUpdateFactoryDelegate {
    public C79793rD(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.ICameraUpdateFactoryDelegate");
    }
}
