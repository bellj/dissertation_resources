package X;

/* renamed from: X.4zN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C108614zN implements AbstractC116375Ve {
    public static final C108614zN A00 = new C108614zN();

    @Override // X.AbstractC116375Ve
    public final boolean Ags(Class cls) {
        return AbstractC79123q5.class.isAssignableFrom(cls);
    }

    @Override // X.AbstractC116375Ve
    public final AbstractC115145Qj Ah5(Class cls) {
        if (!AbstractC79123q5.class.isAssignableFrom(cls)) {
            throw C12970iu.A0f(C12960it.A0c(cls.getName(), "Unsupported message type: "));
        }
        try {
            Class asSubclass = cls.asSubclass(AbstractC79123q5.class);
            AbstractC79123q5 r3 = (AbstractC79123q5) AbstractC79123q5.zzjr.get(asSubclass);
            if (r3 == null) {
                try {
                    String name = asSubclass.getName();
                    Class.forName(name, true, asSubclass.getClassLoader());
                    r3 = (AbstractC79123q5) AbstractC79123q5.zzjr.get(asSubclass);
                    if (r3 == null) {
                        throw C12960it.A0U(C12960it.A0c(name, "Unable to get default instance for: "));
                    }
                } catch (ClassNotFoundException e) {
                    throw new IllegalStateException("Class initialization cannot fail.", e);
                }
            }
            return (AbstractC115145Qj) r3.A05(3);
        } catch (Exception e2) {
            throw new RuntimeException(C12960it.A0c(cls.getName(), "Unable to get message info for "), e2);
        }
    }
}
