package X;

import android.view.View;

/* renamed from: X.0Dh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02630Dh extends AnonymousClass0Y9 {
    public int A00 = 0;
    public boolean A01 = false;
    public final /* synthetic */ AnonymousClass0PN A02;

    public C02630Dh(AnonymousClass0PN r2) {
        this.A02 = r2;
    }

    @Override // X.AnonymousClass0Y9, X.AbstractC12530i4
    public void AMC(View view) {
        int i = this.A00 + 1;
        this.A00 = i;
        AnonymousClass0PN r2 = this.A02;
        if (i == r2.A05.size()) {
            AbstractC12530i4 r1 = r2.A02;
            if (r1 != null) {
                r1.AMC(null);
            }
            this.A00 = 0;
            this.A01 = false;
            r2.A03 = false;
        }
    }

    @Override // X.AnonymousClass0Y9, X.AbstractC12530i4
    public void AMD(View view) {
        if (!this.A01) {
            this.A01 = true;
            AbstractC12530i4 r1 = this.A02.A02;
            if (r1 != null) {
                r1.AMD(null);
            }
        }
    }
}
