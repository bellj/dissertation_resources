package X;

import org.json.JSONObject;

/* renamed from: X.1As  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C25761As extends AbstractC124315pC {
    public int A00;

    @Override // X.AbstractC124315pC
    public String A01(long j) {
        return "whatsapp_galaxy_forward_flow_data_response";
    }

    @Override // X.AbstractC124315pC, X.AnonymousClass12Z
    public void A00(JSONObject jSONObject, long j) {
        JSONObject jSONObject2 = jSONObject.getJSONObject("whatsapp_galaxy_forward_flow_data_response");
        int i = 1;
        if (jSONObject2.optInt("status_code") == 200) {
            i = 0;
        }
        this.A00 = i;
        super.A00 = jSONObject2.getString("payload");
    }
}
