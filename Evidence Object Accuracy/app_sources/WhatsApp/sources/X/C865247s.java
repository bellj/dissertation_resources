package X;

import android.content.Context;

/* renamed from: X.47s  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C865247s extends AbstractC74123hM {
    public final C73823gr A00;

    public C865247s(Context context, int i) {
        super(context);
        setClipToOutline(true);
        C73823gr r0 = new C73823gr();
        this.A00 = r0;
        r0.A00(i);
        setOutlineProvider(r0);
    }

    public void setIsFullscreen(boolean z) {
        this.A00.A01(!z);
    }
}
