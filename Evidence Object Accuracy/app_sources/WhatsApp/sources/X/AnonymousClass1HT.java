package X;

import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.PowerManager;
import com.whatsapp.util.Log;

/* renamed from: X.1HT  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1HT extends BroadcastReceiver {
    public static boolean A04;
    public static final AnonymousClass1HT A05 = new AnonymousClass1HT();
    public AnonymousClass01d A00;
    public C21820y2 A01;
    public final Object A02 = new Object();
    public volatile boolean A03 = false;

    public static boolean A00(AnonymousClass01d r2) {
        PowerManager A0I = r2.A0I();
        KeyguardManager A07 = r2.A07();
        if (A0I == null || !A0I.isScreenOn()) {
            return false;
        }
        return "xiaomi".equalsIgnoreCase(Build.MANUFACTURER) || !A07.isKeyguardLocked() || !A07.inKeyguardRestrictedInputMode();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        String str;
        if (!this.A03) {
            synchronized (this.A02) {
                if (!this.A03) {
                    AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass22D.A00(context);
                    this.A00 = (AnonymousClass01d) r1.ALI.get();
                    this.A01 = (C21820y2) r1.AHx.get();
                    this.A03 = true;
                }
            }
        }
        boolean z = A04;
        String action = intent.getAction();
        if (action != null) {
            switch (action.hashCode()) {
                case -2128145023:
                    if (action.equals("android.intent.action.SCREEN_OFF")) {
                        str = "off";
                        break;
                    }
                    break;
                case -1454123155:
                    if (action.equals("android.intent.action.SCREEN_ON")) {
                        str = "on";
                        break;
                    }
                    break;
                case 823795052:
                    if (action.equals("android.intent.action.USER_PRESENT")) {
                        str = "unlock";
                        break;
                    }
                    break;
            }
            boolean A00 = true ^ A00(this.A00);
            A04 = A00;
            StringBuilder sb = new StringBuilder("ScreenLockReceiver; tag=");
            sb.append(str);
            sb.append("; locked=");
            sb.append(A00);
            sb.append("; oldLocked=");
            sb.append(z);
            Log.i(sb.toString());
            this.A01.A05(A04);
        }
        str = "unknown";
        boolean A00 = true ^ A00(this.A00);
        A04 = A00;
        StringBuilder sb = new StringBuilder("ScreenLockReceiver; tag=");
        sb.append(str);
        sb.append("; locked=");
        sb.append(A00);
        sb.append("; oldLocked=");
        sb.append(z);
        Log.i(sb.toString());
        this.A01.A05(A04);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("ScreenLockReceiver{locked=");
        sb.append(A04);
        sb.append('}');
        return sb.toString();
    }
}
