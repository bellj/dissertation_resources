package X;

import com.whatsapp.jid.UserJid;
import java.util.Map;
import java.util.Set;

/* renamed from: X.3Xs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69043Xs implements AnonymousClass13P {
    public final /* synthetic */ AbstractView$OnCreateContextMenuListenerC35851ir A00;

    public C69043Xs(AbstractView$OnCreateContextMenuListenerC35851ir r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass13P
    public void AUg(C30751Yr r7) {
        C35221hT r1;
        AbstractView$OnCreateContextMenuListenerC35851ir r5 = this.A00;
        C16030oK r3 = r5.A1J;
        AbstractC14640lm r2 = r5.A0c;
        UserJid userJid = r7.A06;
        synchronized (r3.A0W) {
            Map map = (Map) r3.A0A().get(r2);
            if (map != null) {
                if (userJid == null) {
                    r1 = (C35221hT) map.get(r2);
                } else {
                    r1 = (C35221hT) map.get(userJid);
                }
                if (r1 != null) {
                    if (C16030oK.A01(r1.A00, r3.A0K.A00())) {
                        r5.A0U(r7);
                    }
                }
            }
        }
    }

    @Override // X.AnonymousClass13P
    public void AUh(AbstractC14640lm r3, UserJid userJid) {
        AbstractView$OnCreateContextMenuListenerC35851ir r1 = this.A00;
        if (r1.A0c.equals(r3)) {
            r1.A0K();
        }
    }

    @Override // X.AnonymousClass13P
    public void AUi(AbstractC14640lm r4, UserJid userJid) {
        AbstractView$OnCreateContextMenuListenerC35851ir r2 = this.A00;
        if (r2.A0c.equals(r4)) {
            if (userJid == null) {
                userJid = UserJid.of(r4);
            }
            C30751Yr r0 = r2.A0o;
            if (r0 != null && r0.A06.equals(userJid)) {
                r2.A0o = null;
            }
            Set set = r2.A1U;
            synchronized (set) {
                set.add(userJid);
            }
            r2.A0K();
        }
    }
}
