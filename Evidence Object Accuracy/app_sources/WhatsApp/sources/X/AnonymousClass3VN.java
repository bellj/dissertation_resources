package X;

import android.content.Context;
import android.net.Uri;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.biz.catalog.view.CatalogMediaCard;
import com.whatsapp.jid.UserJid;
import java.util.List;

/* renamed from: X.3VN  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3VN implements AbstractC72383eV {
    public C30141Wg A00;
    public boolean A01;
    public final AnonymousClass18U A02;
    public final C254719n A03;
    public final AnonymousClass19S A04;
    public final AnonymousClass19X A05;
    public final CatalogMediaCard A06;
    public final C253619c A07;
    public final C16120oU A08;
    public final AnonymousClass18X A09;

    public AnonymousClass3VN(AnonymousClass18U r1, C254719n r2, AnonymousClass19S r3, AnonymousClass19X r4, CatalogMediaCard catalogMediaCard, C253619c r6, C16120oU r7, AnonymousClass18X r8) {
        this.A08 = r7;
        this.A02 = r1;
        this.A05 = r4;
        this.A04 = r3;
        this.A07 = r6;
        this.A06 = catalogMediaCard;
        this.A09 = r8;
        this.A03 = r2;
        r3.A03(this);
    }

    @Override // X.AbstractC72383eV
    public void A5n() {
        if (!this.A01) {
            this.A06.A0I.A06(6);
            this.A01 = true;
        }
    }

    @Override // X.AbstractC72383eV
    public void A7A() {
        this.A04.A04(this);
    }

    @Override // X.AbstractC72383eV
    public void A9s(UserJid userJid, int i) {
        AnonymousClass19X r3 = this.A05;
        if (r3.A05.A0J(userJid)) {
            r3.A04.A05(userJid);
        } else if (!r3.A00) {
            r3.A00 = true;
            r3.A06.A01(new C68323Uy(r3, userJid), new AnonymousClass28H(userJid, Boolean.TRUE, i, i));
        }
    }

    @Override // X.AbstractC72383eV
    public int AFx(UserJid userJid) {
        return this.A05.A05.A00(userJid);
    }

    @Override // X.AbstractC72383eV
    public AbstractC116285Uv AHC(C44691zO r2, UserJid userJid, boolean z) {
        return new AbstractC116285Uv(r2, this) { // from class: X.3aa
            public final /* synthetic */ C44691zO A00;
            public final /* synthetic */ AnonymousClass3VN A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC116285Uv
            public final void AOB(View view, AnonymousClass4TJ r6) {
                AnonymousClass3VN r3 = this.A01;
                String str = this.A00.A0C;
                if (str != null) {
                    r3.A02.Ab9(r3.A06.getContext(), Uri.parse(str));
                }
            }
        };
    }

    @Override // X.AbstractC72383eV
    public boolean AIA(UserJid userJid) {
        return this.A05.A05.A0H(userJid);
    }

    @Override // X.AbstractC72383eV
    public void AIo(UserJid userJid) {
        CatalogMediaCard catalogMediaCard = this.A06;
        AnonymousClass36H r3 = catalogMediaCard.A0I;
        r3.setSeeMoreClickListener(new AbstractC116275Uu() { // from class: X.3aY
            @Override // X.AbstractC116275Uu
            public final void AO9() {
                UserJid userJid2;
                AnonymousClass3VN r32 = AnonymousClass3VN.this;
                AnonymousClass18U r2 = r32.A02;
                Context context = r32.A06.getContext();
                C30141Wg r0 = r32.A00;
                AnonymousClass009.A05(r0);
                r2.Ab9(context, Uri.parse(r0.A0B));
                if (r32.A09.AKA()) {
                    AnonymousClass30V r1 = new AnonymousClass30V();
                    r1.A01 = C12970iu.A0h();
                    r1.A00 = C12960it.A0V();
                    C30141Wg r02 = r32.A00;
                    if (!(r02 == null || (userJid2 = r02.A04) == null)) {
                        r1.A02 = C248917h.A03(userJid2);
                    }
                    r32.A08.A07(r1);
                }
            }
        });
        r3.setCatalogBrandingDrawable(AnonymousClass00X.A04(null, catalogMediaCard.getResources(), R.drawable.ic_shops_logo));
    }

    @Override // X.AbstractC72383eV
    public void AQR(UserJid userJid) {
        List A08 = this.A05.A05.A08(userJid);
        if (A08 != null && !A08.isEmpty()) {
            this.A06.setupThumbnails(userJid, R.string.business_product_catalog_image_description, A08);
        }
    }

    @Override // X.AbstractC72383eV
    public boolean AdS() {
        return !this.A03.A01(this.A00);
    }
}
