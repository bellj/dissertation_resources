package X;

import com.whatsapp.R;
import com.whatsapp.util.ViewOnClickCListenerShape17S0100000_I1;
import com.whatsapp.util.ViewOnClickCListenerShape5S0200000_I1;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.3Vo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68483Vo implements AnonymousClass2K1 {
    public final C48122Ek A00;
    public final /* synthetic */ AnonymousClass2K0 A01;

    public C68483Vo(C48122Ek r1, AnonymousClass2K0 r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass2K1
    public void APl(int i) {
        AnonymousClass4RU r2 = this.A01.A08;
        if (r2 != null) {
            C91534Sc r1 = r2.A01;
            r1.A03 = null;
            r1.A04.clear();
            r1.A01 = 5;
            r1.A00 = i;
            r2.A00.A0B(r1);
        }
    }

    @Override // X.AnonymousClass2K1
    public /* bridge */ /* synthetic */ void AX4(Object obj) {
        AnonymousClass4T8 r12 = (AnonymousClass4T8) obj;
        AnonymousClass2K0 r2 = this.A01;
        if (r2.A08 != null) {
            List list = r12.A03;
            AnonymousClass3AQ.A00(this.A00, r12.A00, list);
            AnonymousClass4RU r7 = r2.A08;
            C91534Sc r6 = r7.A01;
            r6.A03 = null;
            List list2 = r6.A04;
            list2.clear();
            r6.A02 = r12;
            List<AnonymousClass2x6> list3 = r12.A04;
            if (list3.isEmpty()) {
                r6.A01 = 2;
            } else {
                ArrayList A0l = C12960it.A0l();
                for (AnonymousClass2x6 r9 : list3) {
                    A0l.add(new C59592uz(new ViewOnClickCListenerShape5S0200000_I1(r7, 17, r9), r9.A01, ((C30211Wn) r9).A01));
                }
                C27691It r10 = r7.A02.A00.A0O;
                if (r10.A01() != null && !list.isEmpty()) {
                    String A0X = C12960it.A0X(r7.A03.A00, ((C30211Wn) r10.A01()).A01, new Object[1], 0, R.string.biz_dir_other_parent_category_businesses);
                    A0l.add(new C84753zr());
                    A0l.add(new C59592uz(new ViewOnClickCListenerShape17S0100000_I1(r7, 27), "", A0X));
                }
                r6.A01 = 1;
                list2.clear();
                list2.addAll(A0l);
            }
            r7.A00.A0B(r6);
        }
    }
}
