package X;

import android.content.Intent;
import com.whatsapp.payments.ui.BrazilConfirmReceivePaymentFragment;
import com.whatsapp.payments.ui.BrazilPayBloksActivity;

/* renamed from: X.5k7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C121825k7 extends AnonymousClass6CT {
    public final /* synthetic */ BrazilConfirmReceivePaymentFragment A00;

    public C121825k7(BrazilConfirmReceivePaymentFragment brazilConfirmReceivePaymentFragment) {
        this.A00 = brazilConfirmReceivePaymentFragment;
    }

    @Override // X.AbstractC1311861p
    public void ALx() {
        BrazilConfirmReceivePaymentFragment brazilConfirmReceivePaymentFragment = this.A00;
        String A01 = brazilConfirmReceivePaymentFragment.A0I.A01();
        Intent A0D = C12990iw.A0D(brazilConfirmReceivePaymentFragment.A0B(), BrazilPayBloksActivity.class);
        if (A01 == null) {
            A01 = "brpay_p_add_card";
        }
        A0D.putExtra("screen_name", A01);
        brazilConfirmReceivePaymentFragment.A0v(A0D);
    }
}
