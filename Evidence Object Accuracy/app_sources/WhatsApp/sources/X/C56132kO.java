package X;

import android.net.Uri;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.util.regex.Pattern;

/* renamed from: X.2kO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C56132kO extends AbstractC67743Ss implements AnonymousClass5Yg {
    public static final Pattern A0F = Pattern.compile("^bytes (\\d+)-(\\d+)/(\\d+)$");
    public int A00;
    public long A01;
    public long A02;
    public long A03;
    public long A04;
    public AnonymousClass3H3 A05;
    public InputStream A06;
    public HttpURLConnection A07;
    public boolean A08;
    public byte[] A09;
    public final int A0A;
    public final int A0B;
    public final AnonymousClass4MA A0C;
    public final AnonymousClass4MA A0D;
    public final String A0E;

    @Deprecated
    public C56132kO() {
        this(null, null, 8000, 8000);
    }

    public /* synthetic */ C56132kO(AnonymousClass4MA r3, String str) {
        super(true);
        this.A0E = str;
        this.A0A = 8000;
        this.A0B = 8000;
        this.A0C = r3;
        this.A0D = new AnonymousClass4MA();
    }

    @Deprecated
    public C56132kO(AnonymousClass4MA r2, String str, int i, int i2) {
        super(true);
        this.A0E = str;
        this.A0A = i;
        this.A0B = i2;
        this.A0C = r2;
        this.A0D = new AnonymousClass4MA();
    }

    public final void A04() {
        HttpURLConnection httpURLConnection = this.A07;
        if (httpURLConnection != null) {
            try {
                httpURLConnection.disconnect();
            } catch (Exception e) {
                C64923Hl.A01("DefaultHttpDataSource", "Unexpected error while disconnecting", e);
            }
            this.A07 = null;
        }
    }

    @Override // X.AnonymousClass2BW
    public Uri AHS() {
        HttpURLConnection httpURLConnection = this.A07;
        if (httpURLConnection == null) {
            return null;
        }
        return Uri.parse(httpURLConnection.getURL().toString());
    }

    /* JADX WARNING: Removed duplicated region for block: B:78:0x0189  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x01ea  */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x01fb A[Catch: IOException -> 0x020a, TRY_LEAVE, TryCatch #8 {IOException -> 0x020a, blocks: (B:93:0x01f3, B:95:0x01fb), top: B:132:0x01f3 }] */
    @Override // X.AnonymousClass2BW
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long AYZ(X.AnonymousClass3H3 r24) {
        /*
        // Method dump skipped, instructions count: 650
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C56132kO.AYZ(X.3H3):long");
    }

    @Override // X.AnonymousClass2BW
    public void close() {
        int i;
        try {
            InputStream inputStream = this.A06;
            if (inputStream != null) {
                HttpURLConnection httpURLConnection = this.A07;
                long j = this.A03;
                if (j != -1) {
                    j -= this.A01;
                }
                if (httpURLConnection != null && (i = AnonymousClass3JZ.A01) >= 19 && i <= 20) {
                    try {
                        InputStream inputStream2 = httpURLConnection.getInputStream();
                        if (j == -1) {
                            if (inputStream2.read() == -1) {
                            }
                        } else if (j <= 2048) {
                        }
                        Class<?> cls = inputStream2.getClass();
                        String name = cls.getName();
                        if ("com.android.okhttp.internal.http.HttpTransport$ChunkedInputStream".equals(name) || "com.android.okhttp.internal.http.HttpTransport$FixedLengthInputStream".equals(name)) {
                            Method declaredMethod = cls.getSuperclass().getDeclaredMethod("unexpectedEndOfInput", new Class[0]);
                            declaredMethod.setAccessible(true);
                            declaredMethod.invoke(inputStream2, new Object[0]);
                        }
                    } catch (Exception unused) {
                    }
                }
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new AnonymousClass495(this.A05, e, 3);
                }
            }
        } finally {
            this.A06 = null;
            A04();
            if (this.A08) {
                this.A08 = false;
                A00();
            }
        }
    }

    @Override // X.AnonymousClass2BY
    public int read(byte[] bArr, int i, int i2) {
        try {
            if (this.A02 != this.A04) {
                if (this.A09 == null) {
                    this.A09 = new byte[4096];
                }
                while (true) {
                    long j = this.A02;
                    long j2 = this.A04;
                    if (j == j2) {
                        break;
                    }
                    byte[] bArr2 = this.A09;
                    int read = this.A06.read(bArr2, 0, (int) Math.min(j2 - j, (long) bArr2.length));
                    if (Thread.currentThread().isInterrupted()) {
                        throw new InterruptedIOException();
                    } else if (read != -1) {
                        this.A02 += (long) read;
                        A02(read);
                    } else {
                        throw new EOFException();
                    }
                }
            }
            if (i2 == 0) {
                return 0;
            }
            long j3 = this.A03;
            if (j3 != -1) {
                long j4 = j3 - this.A01;
                if (j4 == 0) {
                    return -1;
                }
                i2 = (int) Math.min((long) i2, j4);
            }
            int read2 = this.A06.read(bArr, i, i2);
            if (read2 != -1) {
                this.A01 += (long) read2;
                A02(read2);
                return read2;
            } else if (this.A03 == -1) {
                return -1;
            } else {
                throw new EOFException();
            }
        } catch (IOException e) {
            throw new AnonymousClass495(this.A05, e, 2);
        }
    }
}
