package X;

import android.content.Context;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.params.StreamConfigurationMap;
import java.util.List;

/* renamed from: X.5cp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C119065cp extends AbstractC130695zp {
    public static final Integer A1B = -1;
    public C129845yO A00;
    public Boolean A01;
    public Boolean A02;
    public Boolean A03;
    public Boolean A04;
    public Boolean A05;
    public Boolean A06;
    public Boolean A07;
    public Boolean A08;
    public Boolean A09;
    public Boolean A0A;
    public Boolean A0B;
    public Boolean A0C;
    public Boolean A0D;
    public Boolean A0E;
    public Boolean A0F;
    public Boolean A0G;
    public Boolean A0H;
    public Boolean A0I;
    public Boolean A0J;
    public Boolean A0K;
    public Boolean A0L;
    public Boolean A0M;
    public Boolean A0N;
    public Boolean A0O;
    public Boolean A0P;
    public Boolean A0Q;
    public Boolean A0R;
    public Boolean A0S;
    public Boolean A0T;
    public Boolean A0U;
    public Boolean A0V;
    public Boolean A0W;
    public Boolean A0X;
    public Boolean A0Y;
    public Float A0Z;
    public Float A0a;
    public Float A0b;
    public Integer A0c;
    public Integer A0d;
    public Integer A0e;
    public Integer A0f;
    public Integer A0g;
    public Integer A0h;
    public Integer A0i;
    public List A0j;
    public List A0k;
    public List A0l;
    public List A0m;
    public List A0n;
    public List A0o;
    public List A0p;
    public List A0q;
    public List A0r;
    public List A0s;
    public List A0t;
    public List A0u;
    public List A0v;
    public List A0w;
    public List A0x;
    public List A0y;
    public List A0z;
    public List A10;
    public List A11;
    public List A12;
    public List A13;
    public List A14;
    public List A15;
    public List A16;
    public final int A17;
    public final Context A18;
    public final CameraCharacteristics A19;
    public final StreamConfigurationMap A1A;

    public C119065cp(Context context, CameraCharacteristics cameraCharacteristics, int i) {
        this.A18 = context;
        this.A17 = i;
        this.A19 = cameraCharacteristics;
        this.A1A = (StreamConfigurationMap) cameraCharacteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:381:0x05ce, code lost:
        if (r2.hasSystemFeature(r0) != false) goto L_0x05d0;
     */
    @Override // X.AbstractC130695zp
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object A01(X.C125465rI r9) {
        /*
        // Method dump skipped, instructions count: 1746
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C119065cp.A01(X.5rI):java.lang.Object");
    }
}
