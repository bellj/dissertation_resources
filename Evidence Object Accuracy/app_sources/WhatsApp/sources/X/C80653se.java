package X;

import android.view.View;
import com.whatsapp.stickers.StickerStoreActivity;
import com.whatsapp.stickers.StickerStoreFeaturedTabFragment;
import com.whatsapp.stickers.StickerStoreMyTabFragment;

/* renamed from: X.3se  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C80653se extends AnonymousClass2UH {
    public final /* synthetic */ StickerStoreActivity A00;

    @Override // X.AnonymousClass2UH
    public void A00(View view, float f) {
    }

    public C80653se(StickerStoreActivity stickerStoreActivity) {
        this.A00 = stickerStoreActivity;
    }

    @Override // X.AnonymousClass2UH
    public void A01(View view, int i) {
        if (i == 5 || i == 4) {
            StickerStoreActivity stickerStoreActivity = this.A00;
            stickerStoreActivity.finish();
            stickerStoreActivity.overridePendingTransition(0, 0);
        } else if (i == 3) {
            StickerStoreActivity stickerStoreActivity2 = this.A00;
            StickerStoreFeaturedTabFragment stickerStoreFeaturedTabFragment = stickerStoreActivity2.A06;
            if (stickerStoreFeaturedTabFragment != null) {
                stickerStoreFeaturedTabFragment.A1A();
            }
            StickerStoreMyTabFragment stickerStoreMyTabFragment = stickerStoreActivity2.A07;
            if (stickerStoreMyTabFragment != null) {
                stickerStoreMyTabFragment.A1A();
            }
        }
    }
}
