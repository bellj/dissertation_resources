package X;

import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.telephony.TelephonyManager;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.2Zm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C51882Zm extends ConnectivityManager.NetworkCallback {
    public final C18640sm A00;
    public final C15890o4 A01;
    public final AtomicBoolean A02 = new AtomicBoolean(false);
    public volatile ConnectivityManager A03;
    public volatile Network A04;
    public volatile TelephonyManager A05;

    public C51882Zm(C18640sm r3, C15890o4 r4) {
        this.A01 = r4;
        this.A00 = r3;
    }

    public int A00() {
        NetworkCapabilities networkCapabilities;
        ConnectivityManager connectivityManager = this.A03;
        int i = 0;
        if (!(connectivityManager == null || this.A04 == null || (networkCapabilities = connectivityManager.getNetworkCapabilities(this.A04)) == null)) {
            i = 1;
            if (!networkCapabilities.hasTransport(1) && !networkCapabilities.hasTransport(3)) {
                if (!networkCapabilities.hasCapability(18)) {
                    return 3;
                }
                return 2;
            }
        }
        return i;
    }

    public int A01() {
        TelephonyManager telephonyManager = this.A05;
        if (telephonyManager == null || this.A01.A02("android.permission.READ_PHONE_STATE") != 0) {
            return 0;
        }
        return telephonyManager.getDataNetworkType();
    }

    public void A02(ConnectivityManager connectivityManager, TelephonyManager telephonyManager) {
        this.A03 = connectivityManager;
        this.A05 = telephonyManager;
    }

    public void A03(AnonymousClass1I0 r3) {
        if (r3 != null) {
            this.A02.set(r3.A03);
        }
    }

    public boolean A04() {
        NetworkCapabilities networkCapabilities;
        ConnectivityManager connectivityManager = this.A03;
        if (connectivityManager == null || this.A04 == null || (networkCapabilities = connectivityManager.getNetworkCapabilities(this.A04)) == null || networkCapabilities.hasCapability(11) || connectivityManager.getRestrictBackgroundStatus() != 3) {
            return false;
        }
        return true;
    }

    public boolean A05() {
        return this.A02.get();
    }

    @Override // android.net.ConnectivityManager.NetworkCallback
    public void onBlockedStatusChanged(Network network, boolean z) {
        AtomicBoolean atomicBoolean;
        boolean z2;
        if (!z) {
            this.A04 = network;
            atomicBoolean = this.A02;
            z2 = true;
        } else if (network.equals(this.A04)) {
            this.A04 = null;
            atomicBoolean = this.A02;
            z2 = false;
        } else {
            return;
        }
        atomicBoolean.set(z2);
        this.A00.A08();
    }

    @Override // android.net.ConnectivityManager.NetworkCallback
    public void onLost(Network network) {
        if (network.equals(this.A04)) {
            this.A04 = null;
            this.A02.set(false);
            this.A00.A08();
        }
    }

    @Override // android.net.ConnectivityManager.NetworkCallback
    public void onUnavailable() {
        this.A04 = null;
        this.A02.set(false);
        this.A00.A08();
    }
}
