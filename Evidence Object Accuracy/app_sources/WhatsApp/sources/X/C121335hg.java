package X;

import com.whatsapp.util.Log;

/* renamed from: X.5hg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C121335hg extends AbstractC130285z6 {
    public AnonymousClass619 A00;
    public AnonymousClass619 A01;

    public C121335hg(AnonymousClass1V8 r3) {
        try {
            this.A01 = AnonymousClass619.A01(r3, "title");
            this.A00 = AnonymousClass619.A01(r3, "description");
            r3.A0F("primary_action").A0H("text");
            AnonymousClass1V8 A0E = r3.A0E("secondary_action");
            if (A0E != null) {
                A0E.A0H("text");
            }
        } catch (AnonymousClass1V9 unused) {
            Log.e("PAY: ManualReviewChallenge parse manual review challenge failed.");
        }
    }
}
