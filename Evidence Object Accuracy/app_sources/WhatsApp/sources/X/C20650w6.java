package X;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.database.sqlite.SQLiteFullException;
import com.facebook.redex.RunnableBRunnable0Shape0S0210000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0300000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S1300000_I0;
import com.facebook.redex.RunnableBRunnable0Shape3S0200000_I0_3;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* renamed from: X.0w6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20650w6 {
    public final C14900mE A00;
    public final C243915i A01;
    public final C18850tA A02;
    public final C21370xJ A03;
    public final C16970q3 A04;
    public final C14820m6 A05;
    public final C22320yt A06;
    public final C241814n A07;
    public final C16510p9 A08;
    public final C19990v2 A09;
    public final C241214h A0A;
    public final C15680nj A0B;
    public final C21380xK A0C;
    public final AnonymousClass1C5 A0D;
    public final C20850wQ A0E;
    public final C16490p7 A0F;
    public final C20140vH A0G;
    public final AnonymousClass134 A0H;
    public final C19770ue A0I;
    public final C243715g A0J;
    public final C21400xM A0K;
    public final C238213d A0L;
    public final C22230yk A0M;
    public final C15860o1 A0N;

    public C20650w6(C14900mE r2, C243915i r3, C18850tA r4, C21370xJ r5, C16970q3 r6, C14820m6 r7, C22320yt r8, C241814n r9, C16510p9 r10, C19990v2 r11, C241214h r12, C15680nj r13, C21380xK r14, AnonymousClass1C5 r15, C20850wQ r16, C16490p7 r17, C20140vH r18, AnonymousClass134 r19, C19770ue r20, C243715g r21, C21400xM r22, C238213d r23, C22230yk r24, C15860o1 r25) {
        this.A00 = r2;
        this.A08 = r10;
        this.A09 = r11;
        this.A0G = r18;
        this.A0H = r19;
        this.A04 = r6;
        this.A02 = r4;
        this.A03 = r5;
        this.A0C = r14;
        this.A0M = r24;
        this.A01 = r3;
        this.A07 = r9;
        this.A0L = r23;
        this.A0N = r25;
        this.A0I = r20;
        this.A06 = r8;
        this.A0A = r12;
        this.A0K = r22;
        this.A0F = r17;
        this.A05 = r7;
        this.A0B = r13;
        this.A0D = r15;
        this.A0E = r16;
        this.A0J = r21;
    }

    public Long A00(AbstractC14640lm r8) {
        C18850tA r4 = this.A02;
        Set A06 = r4.A06(r8, false);
        Long A0A = this.A0N.A0A(r8, 0, false);
        if (A0A != null) {
            r4.A0O(A06);
            this.A0M.A0C(new C34081fY(r8, 12, 0), 0);
            return A0A;
        }
        r4.A0N(A06);
        return A0A;
    }

    public HashMap A01(List list) {
        Set A0D = this.A0N.A0D();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            AbstractC14640lm r6 = (AbstractC14640lm) it.next();
            if (!C15380n4.A0O(r6)) {
                A05(r6, true);
                this.A0M.A04(r6, 3, 0, 0);
            }
        }
        HashMap hashMap = new HashMap();
        Iterator it2 = list.iterator();
        while (it2.hasNext()) {
            AbstractC14640lm r1 = (AbstractC14640lm) it2.next();
            if (!C15380n4.A0O(r1) && A0D.contains(r1)) {
                hashMap.put(r1, A00(r1));
            }
        }
        return hashMap;
    }

    public void A02(AbstractC14640lm r5) {
        AnonymousClass1PE A06 = this.A09.A06(r5);
        if (A06 == null) {
            StringBuilder sb = new StringBuilder("msgstore/reset-show-group-description/no chat ");
            sb.append(r5);
            Log.w(sb.toString());
        } else if (!A06.A0h) {
            StringBuilder sb2 = new StringBuilder("msgstore/reset-show-group-description/nop ");
            sb2.append(r5);
            Log.i(sb2.toString());
        } else {
            A06.A0h = false;
            this.A06.A01(new RunnableBRunnable0Shape3S0200000_I0_3(this, 48, A06), 5);
        }
    }

    public final void A03(AbstractC14640lm r24, AbstractC15340mz r25, int i, int i2, boolean z, boolean z2) {
        AnonymousClass1IS r0;
        int i3;
        int i4;
        AnonymousClass1IS r02;
        AnonymousClass1PE A06 = this.A09.A06(r24);
        if (A06 == null) {
            StringBuilder sb = new StringBuilder("msgstore/setchatseen/nochat/");
            sb.append(r24);
            sb.append("/");
            if (r25 != null) {
                r02 = r25.A0z;
            } else {
                r02 = null;
            }
            sb.append(r02);
            Log.i(sb.toString());
            return;
        }
        StringBuilder sb2 = new StringBuilder("msgstore/setchatseen/");
        sb2.append(r24);
        sb2.append("/");
        sb2.append(A06.A07());
        sb2.append("/");
        if (r25 != null) {
            r0 = r25.A0z;
        } else {
            r0 = null;
        }
        sb2.append(r0);
        sb2.append("/");
        sb2.append(i);
        Log.i(sb2.toString());
        boolean z3 = true;
        boolean z4 = false;
        if (A06.A06 == -1) {
            z4 = true;
        }
        if (r25 != null) {
            AnonymousClass134 r2 = this.A0H;
            AbstractC14640lm r6 = r25.A0z.A00;
            AnonymousClass009.A05(r6);
            i3 = r2.A00(r6, r25.A12);
            AnonymousClass1C5 r22 = this.A0D;
            AnonymousClass009.A05(r6);
            i4 = r22.A00(r6, r25.A12);
        } else {
            i3 = 0;
            i4 = 0;
        }
        boolean A0C = A06.A0C(i, i2, i3, i4);
        long j = A06.A0S;
        long j2 = A06.A0L;
        if (j2 > j) {
            A06.A0S = j2;
            A06.A05 = 0;
        } else {
            z3 = A0C;
        }
        long A01 = C30041Vv.A01(r25);
        long A02 = C30041Vv.A02(r25);
        long j3 = A06.A0P;
        if (A01 == 1) {
            A02 = A06.A0N;
            long j4 = A06.A0U;
            if (A02 > j4) {
                A01 = A06.A0M;
            } else {
                A01 = A06.A0T;
                A02 = j4;
            }
        }
        if (j3 < A02 || z3 || A06.A0M == 0) {
            if (A06.A0M == 0) {
                C20140vH r03 = this.A0G;
                AbstractC14640lm r14 = A06.A0i;
                A06.A0M = r03.A02(r14);
                A06.A0N = this.A0H.A06(r14);
                A06.A0Z = null;
            }
            if (A06.A0K != 1) {
                A06.A0K = 1;
            }
            A06.A0O = A01;
            A06.A0P = A02;
            this.A06.A01(new Runnable(A06, this, r24, j3, j, z, z4, z2) { // from class: X.1tM
                public final /* synthetic */ long A00;
                public final /* synthetic */ long A01;
                public final /* synthetic */ AnonymousClass1PE A02;
                public final /* synthetic */ C20650w6 A03;
                public final /* synthetic */ AbstractC14640lm A04;
                public final /* synthetic */ boolean A05;
                public final /* synthetic */ boolean A06;
                public final /* synthetic */ boolean A07;

                {
                    this.A03 = r2;
                    this.A05 = r8;
                    this.A06 = r9;
                    this.A04 = r3;
                    this.A02 = r1;
                    this.A00 = r4;
                    this.A07 = r10;
                    this.A01 = r6;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    Set emptySet;
                    C16310on A012;
                    C20650w6 r13 = this.A03;
                    boolean z5 = this.A05;
                    boolean z6 = this.A06;
                    AbstractC14640lm r12 = this.A04;
                    AnonymousClass1PE r23 = this.A02;
                    long j5 = this.A00;
                    boolean z7 = this.A07;
                    long j6 = this.A01;
                    if (!z5 || !z6) {
                        emptySet = Collections.emptySet();
                    } else {
                        emptySet = r13.A02.A08(r12, true);
                    }
                    r13.A08.A0B(r23);
                    r13.A02.A0O(emptySet);
                    C243715g r10 = r13.A0J;
                    C28181Ma r62 = new C28181Ma(false);
                    r62.A04("msgstore/unsentreadreceiptsforjid");
                    ArrayList arrayList = new ArrayList();
                    C238213d r142 = r10.A0A;
                    if (r142.A00(r12)) {
                        AnonymousClass1PE A062 = r10.A03.A06(r12);
                        if (A062 == null) {
                            StringBuilder sb3 = new StringBuilder("msgstore/unsentreadreceiptsforjid/no chat for ");
                            sb3.append(r12);
                            Log.w(sb3.toString());
                        } else if (A062.A0O != A062.A0Q) {
                            String[] strArr = new String[3];
                            strArr[0] = String.valueOf(r10.A02.A02(r12));
                            strArr[1] = String.valueOf(A062.A0P);
                            if (r142.A01(r12)) {
                                j5 = A062.A0R;
                            }
                            strArr[2] = String.valueOf(j5);
                            try {
                                A012 = r10.A08.get();
                                try {
                                    Cursor A09 = A012.A03.A09(C32301bw.A0Q, strArr);
                                    if (A09 != null) {
                                        while (A09.moveToNext()) {
                                            AbstractC15340mz A022 = r10.A01.A02(A09, r12, false, true);
                                            if (A022 != null && A022.A0I > 1415214000000L) {
                                                arrayList.add(A022);
                                            }
                                        }
                                        A09.close();
                                    }
                                    A012.close();
                                } finally {
                                }
                            } catch (SQLiteDatabaseCorruptException e) {
                                Log.e(e);
                                r10.A07.A02();
                            } catch (SQLiteFullException e2) {
                                r10.A06.A00(0);
                                throw e2;
                            } catch (IllegalStateException e3) {
                                Log.i("msgstore/unsentreadreceiptsforjid/IllegalStateException ", e3);
                            }
                            StringBuilder sb4 = new StringBuilder("msgstore/unsentreadreceiptsforjid ");
                            sb4.append(arrayList.size());
                            sb4.append(" | time spent:");
                            sb4.append(r62.A01());
                            Log.i(sb4.toString());
                        }
                    }
                    if (z7) {
                        C21400xM r9 = r13.A0K;
                        HashSet hashSet = new HashSet();
                        long A023 = r9.A07.A02(r12);
                        A012 = r9.A0D.get();
                        try {
                            for (Byte b : C21400xM.A0L) {
                                byte byteValue = b.byteValue();
                                HashSet hashSet2 = new HashSet();
                                AnonymousClass1ED r4 = r9.A0I;
                                String[] strArr2 = {String.valueOf(A023), String.valueOf(j6), String.valueOf(0)};
                                C16330op r1 = A012.A03;
                                StringBuilder sb5 = new StringBuilder();
                                sb5.append(C40471re.A00(byteValue));
                                sb5.append(" WHERE ");
                                sb5.append("message_add_on.chat_row_id = ?");
                                sb5.append(" AND ");
                                sb5.append("message_add_on.message_add_on_type = ");
                                sb5.append((int) byteValue);
                                sb5.append(" AND ");
                                sb5.append("message_add_on._id > ?");
                                sb5.append(" AND ");
                                sb5.append("message_add_on.from_me = 0");
                                sb5.append(" AND ");
                                sb5.append("message_add_on.status = ?");
                                Cursor A092 = r1.A09(sb5.toString(), strArr2);
                                HashMap A00 = AnonymousClass1sB.A00(A092, byteValue);
                                while (A092.moveToNext()) {
                                    AnonymousClass1Iv A04 = r4.A04(A092, A00);
                                    if (A04 == null) {
                                        Log.e("MessageAddOnManager/getUnreadMessageAddOnReactionsFor unexpected fmessage");
                                    } else {
                                        A04.A15(A092, r9.A0B, A00);
                                        hashSet2.add(A04);
                                    }
                                }
                                A092.close();
                                hashSet.addAll(hashSet2);
                            }
                            A012.close();
                            r9.A01(hashSet);
                        } finally {
                        }
                    }
                    r13.A0C.A02.post(new RunnableBRunnable0Shape0S0300000_I0(r13, r12, arrayList, 42));
                }
            }, 2);
        }
    }

    public void A04(AbstractC14640lm r9, Runnable runnable, String str) {
        this.A06.A01(new RunnableBRunnable0Shape0S1300000_I0(this, r9, runnable, str, 4), 6);
    }

    public void A05(AbstractC14640lm r6, boolean z) {
        StringBuilder sb;
        String str;
        if (z) {
            this.A01.A00(r6, 3);
        }
        AnonymousClass1PE A06 = this.A09.A06(r6);
        if (A06 == null) {
            sb = new StringBuilder();
            str = "msgstore/archive/no chat ";
        } else if (A06.A0f == z) {
            sb = new StringBuilder();
            str = "msgstore/archive/nop ";
        } else {
            A06(z);
            A06.A0f = z;
            this.A03.A00();
            this.A06.A01(new RunnableBRunnable0Shape0S0210000_I0(this, A06, 8, z), 3);
            return;
        }
        sb.append(str);
        sb.append(r6);
        sb.append(" ");
        sb.append(z);
        Log.w(sb.toString());
    }

    public final void A06(boolean z) {
        if (z && this.A0B.A00() == 0 && !this.A05.A00.getBoolean("archive_v2_enabled", false)) {
            Log.i("chatMAnager/setChatArchived/Enabling archive2.0");
            this.A04.A04();
        }
    }

    public final void A07(boolean z) {
        boolean z2;
        synchronized (this.A0E) {
            C19990v2 r3 = this.A09;
            synchronized (r3) {
                z2 = r3.A00;
            }
            if (!z2) {
                C29551Tn r2 = new C29551Tn(this);
                synchronized (r3) {
                    r3.A0B();
                    boolean z3 = false;
                    if (r3.A03 == null) {
                        z3 = true;
                    }
                    AnonymousClass009.A0F(z3);
                    r3.A03 = r2;
                    if (!z) {
                        r3.A0B();
                    }
                }
            }
        }
    }

    public boolean A08(AbstractC14640lm r8) {
        AnonymousClass1PE A06 = this.A09.A06(r8);
        if (A06 != null) {
            return (A06.A06 == 0 && A06.A07 == 0 && A06.A0P == Math.max(A06.A0N, A06.A0U) && A06.A0L <= A06.A0S) ? false : true;
        }
        return false;
    }
}
