package X;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1KT  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1KT {
    public int A00;
    public C69893aP A01;
    public AnonymousClass1BQ A02;
    public C38711oa A03;
    public AnonymousClass1KU A04;
    public List A05;
    public boolean A06 = false;
    public boolean A07 = false;
    public final C14820m6 A08;
    public final C22210yi A09;
    public final AnonymousClass1KM A0A = new AnonymousClass1KN(this);
    public final AnonymousClass146 A0B;
    public final C235512c A0C;
    public final AbstractC14440lR A0D;
    public final HashMap A0E;
    public final HashMap A0F;
    public final HashSet A0G;

    public AnonymousClass1KT(C14820m6 r2, C22210yi r3, AnonymousClass146 r4, C235512c r5, AbstractC14440lR r6) {
        this.A0C = r5;
        this.A0D = r6;
        this.A09 = r3;
        this.A0B = r4;
        this.A08 = r2;
        this.A0G = new HashSet();
        this.A0F = new HashMap();
        this.A0E = new HashMap();
    }

    public List A00(List list, List list2) {
        HashSet hashSet = new HashSet();
        HashSet hashSet2 = new HashSet();
        ArrayList arrayList = new ArrayList();
        if (list != null) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                AnonymousClass1KS r2 = (AnonymousClass1KS) it.next();
                String str = r2.A09;
                if (!hashSet2.contains(str)) {
                    if (!AnonymousClass1US.A0C(str)) {
                        hashSet2.add(str);
                    }
                    arrayList.add(r2);
                    hashSet.add(r2.A0C);
                }
            }
        }
        if (list2 != null) {
            Iterator it2 = list2.iterator();
            while (it2.hasNext()) {
                AnonymousClass1KS r22 = (AnonymousClass1KS) it2.next();
                String str2 = r22.A09;
                if (!hashSet2.contains(str2)) {
                    if (!AnonymousClass1US.A0C(str2)) {
                        hashSet2.add(str2);
                    }
                    if (hashSet.add(r22.A0C)) {
                        arrayList.add(r22);
                    }
                }
            }
        }
        List<AnonymousClass1KZ> list3 = this.A05;
        if (list3 != null) {
            for (AnonymousClass1KZ r0 : list3) {
                for (AnonymousClass1KS r23 : r0.A04) {
                    String str3 = r23.A09;
                    if (!hashSet2.contains(str3)) {
                        if (!AnonymousClass1US.A0C(str3)) {
                            hashSet2.add(str3);
                        }
                        if (hashSet.add(r23.A0C)) {
                            arrayList.add(r23);
                        }
                    }
                }
            }
        }
        return arrayList;
    }

    public void A01() {
        C38711oa r1 = this.A03;
        if (r1 != null) {
            r1.A03(true);
        }
        C622035j r2 = new C622035j(this.A08, this.A0C, this, this.A00, true);
        this.A03 = r2;
        this.A0D.Aaz(r2, new Void[0]);
    }

    public void A02() {
        C38711oa r1 = this.A03;
        if (r1 != null) {
            r1.A03(true);
        }
        C622035j r2 = new C622035j(this.A08, this.A0C, this, this.A00, false);
        this.A03 = r2;
        this.A0D.Aaz(r2, new Void[0]);
    }

    public void A03() {
        this.A0B.A04(this.A0A);
    }

    public void A04(AnonymousClass1KU r2, int i) {
        C38711oa r0;
        this.A04 = r2;
        this.A00 = i;
        if (this.A05 != null || ((r0 = this.A03) != null && !((AbstractC16350or) r0).A02.isCancelled())) {
            List list = this.A05;
            if (list != null) {
                A05(list);
                return;
            }
            return;
        }
        A02();
    }

    public final void A05(List list) {
        this.A05 = list;
        AnonymousClass1KU r0 = this.A04;
        if (r0 != null) {
            r0.Acu(null, this.A0F, this.A0E, this.A0G, list);
        }
    }
}
