package X;

import android.os.Build;
import com.whatsapp.R;

/* renamed from: X.4GW  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4GW {
    public static final int A00;

    static {
        int i;
        int i2 = Build.VERSION.SDK_INT;
        if (i2 < 19 || (i2 < 21 && "samsung".equalsIgnoreCase(Build.MANUFACTURER))) {
            i = R.color.wds_cool_gray_alpha_30;
        } else {
            i = R.color.wds_white_alpha_30;
        }
        A00 = i;
    }
}
