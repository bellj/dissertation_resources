package X;

import android.os.Bundle;
import java.util.concurrent.locks.Lock;

/* renamed from: X.4yg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C108204yg implements AbstractC14980mM, AbstractC15000mO {
    public final /* synthetic */ C108324ys A00;

    public /* synthetic */ C108204yg(C108324ys r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC14990mN
    public final void onConnectionSuspended(int i) {
    }

    @Override // X.AbstractC14990mN
    public final void onConnected(Bundle bundle) {
        C108324ys r2 = this.A00;
        C13020j0.A01(r2.A0G);
        AnonymousClass5Yh r1 = r2.A05;
        C13020j0.A01(r1);
        r1.AgU(new BinderC80553sS(r2));
    }

    @Override // X.AbstractC15010mP
    public final void onConnectionFailed(C56492ky r4) {
        C108324ys r2 = this.A00;
        Lock lock = r2.A0K;
        lock.lock();
        try {
            if (!r2.A06 || r4.A00()) {
                r2.A03(r4);
            } else {
                r2.A00();
                r2.A02();
            }
        } finally {
            lock.unlock();
        }
    }
}
