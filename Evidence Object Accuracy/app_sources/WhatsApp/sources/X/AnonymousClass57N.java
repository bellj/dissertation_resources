package X;

import com.whatsapp.perf.profilo.ProfiloUploadService;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.Map;

/* renamed from: X.57N  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass57N implements AbstractC28461Nh {
    public final /* synthetic */ ProfiloUploadService A00;
    public final /* synthetic */ File A01;

    public AnonymousClass57N(ProfiloUploadService profiloUploadService, File file) {
        this.A00 = profiloUploadService;
        this.A01 = file;
    }

    @Override // X.AbstractC28461Nh
    public void AOZ(String str) {
    }

    @Override // X.AbstractC28461Nh
    public void AOt(long j) {
        this.A01.delete();
    }

    @Override // X.AbstractC28461Nh
    public void APq(String str) {
        Log.e(C12960it.A0d(str, C12960it.A0k("ProfiloUpload/Error: ")));
    }

    @Override // X.AbstractC28461Nh
    public void AV9(String str, Map map) {
    }
}
