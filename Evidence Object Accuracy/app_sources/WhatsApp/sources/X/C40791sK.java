package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.1sK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C40791sK extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C40791sK A04;
    public static volatile AnonymousClass255 A05;
    public int A00;
    public long A01;
    public C40801sL A02;
    public AnonymousClass1G8 A03;

    static {
        C40791sK r0 = new C40791sK();
        A04 = r0;
        r0.A0W();
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            AnonymousClass1G8 r0 = this.A03;
            if (r0 == null) {
                r0 = AnonymousClass1G8.A05;
            }
            i2 = 0 + CodedOutputStream.A0A(r0, 1);
        }
        if ((this.A00 & 2) == 2) {
            C40801sL r02 = this.A02;
            if (r02 == null) {
                r02 = C40801sL.A01;
            }
            i2 += CodedOutputStream.A0A(r02, 2);
        }
        if ((this.A00 & 4) == 4) {
            i2 += CodedOutputStream.A05(3, this.A01);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            AnonymousClass1G8 r0 = this.A03;
            if (r0 == null) {
                r0 = AnonymousClass1G8.A05;
            }
            codedOutputStream.A0L(r0, 1);
        }
        if ((this.A00 & 2) == 2) {
            C40801sL r02 = this.A02;
            if (r02 == null) {
                r02 = C40801sL.A01;
            }
            codedOutputStream.A0L(r02, 2);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0H(3, this.A01);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
