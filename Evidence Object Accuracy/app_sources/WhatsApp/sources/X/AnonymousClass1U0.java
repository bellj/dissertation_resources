package X;

import java.util.LinkedHashMap;

/* renamed from: X.1U0  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1U0 extends ThreadLocal {
    public final /* synthetic */ C29621Ty A00;

    public AnonymousClass1U0(C29621Ty r1) {
        this.A00 = r1;
    }

    @Override // java.lang.ThreadLocal
    public Object initialValue() {
        return new LinkedHashMap(16, 0.75f, true);
    }
}
