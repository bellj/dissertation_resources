package X;

import java.io.File;

/* renamed from: X.4Vr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C92384Vr {
    public final long A00;
    public final long A01;
    public final File A02;
    public final byte[] A03;

    public /* synthetic */ C92384Vr(File file, byte[] bArr, long j, long j2) {
        this.A02 = file;
        this.A00 = j;
        this.A01 = j2;
        this.A03 = bArr;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("DownloadResult{fileSize=");
        A0k.append(this.A00);
        A0k.append(", roundTripTime=");
        A0k.append(this.A01);
        return C12970iu.A0v(A0k);
    }
}
