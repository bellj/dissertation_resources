package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.2O0  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2O0 extends AnonymousClass1JW {
    public final String A00;
    public final List A01 = new ArrayList();

    public AnonymousClass2O0(AbstractC15710nm r2, C15450nH r3, String str) {
        super(r2, r3);
        this.A05 = 39;
        this.A00 = str;
    }
}
