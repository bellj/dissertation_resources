package X;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5lI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122335lI extends AbstractC118825cR {
    public final ImageView A00;
    public final TextView A01;

    public C122335lI(View view) {
        super(view);
        this.A00 = C12970iu.A0K(view, R.id.icon);
        this.A01 = C12960it.A0I(view, R.id.text);
    }
}
