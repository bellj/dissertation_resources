package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3oY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78213oY extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C99404kF();
    public final int A00;
    public final String A01;

    public C78213oY(int i, String str) {
        this.A00 = i;
        this.A01 = str;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 2, this.A00);
        C95654e8.A0C(parcel, this.A01, 3, A00);
    }
}
