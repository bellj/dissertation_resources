package X;

/* renamed from: X.69l  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1330969l implements AnonymousClass6MM {
    public final /* synthetic */ AnonymousClass605 A00;
    public final /* synthetic */ C128545wH A01;
    public final /* synthetic */ C129275xS A02;
    public final /* synthetic */ String A03;

    public C1330969l(AnonymousClass605 r1, C128545wH r2, C129275xS r3, String str) {
        this.A00 = r1;
        this.A03 = str;
        this.A02 = r3;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass6MM
    public void APo(C452120p r3) {
        this.A02.A00(r3, null);
    }

    @Override // X.AnonymousClass6MM
    public void AX5(String str) {
        C130775zx r0 = this.A00.A01;
        this.A02.A00(null, this.A01.A02(C130775zx.A00(Boolean.TRUE, str, this.A03, null, null, new Object[0], C117295Zj.A03(r0.A01))));
    }
}
