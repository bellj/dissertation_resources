package X;

import android.view.View;

/* renamed from: X.3Fb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64313Fb {
    public C90004Mf A00;
    public C90004Mf A01;
    public AnonymousClass4AR A02;
    public Float A03;

    public C64313Fb(AnonymousClass4AR r1, Float f) {
        this.A02 = r1;
        this.A03 = f;
    }

    public final int A00(View view, AbstractC06220Sq r4) {
        AnonymousClass4AR r1 = this.A02;
        switch (r1.ordinal()) {
            case 0:
                return r4.A0B(view);
            case 1:
            default:
                throw C12970iu.A0f(C12960it.A0b("Invalid gravity :", r1));
            case 2:
                return r4.A08(view);
        }
    }

    public final int A01(AbstractC06220Sq r4, AnonymousClass02H r5) {
        AnonymousClass4AR r1 = this.A02;
        switch (r1.ordinal()) {
            case 0:
                Float f = this.A03;
                if (r5.A0S()) {
                    return r4.A06() + Math.round(f.floatValue());
                }
                return 0;
            case 1:
            default:
                throw C12970iu.A0f(C12960it.A0b("Invalid gravity :", r1));
            case 2:
                if (r5.A0S()) {
                    return r4.A02();
                }
                return r4.A01();
        }
    }

    public View A02(AnonymousClass02H r9) {
        C90004Mf r1;
        if (r9.A14()) {
            r1 = this.A00;
            if (r1 == null || r1.A01 != r9) {
                r1 = new C90004Mf(new AnonymousClass0Ez(r9), r9);
                this.A00 = r1;
            }
        } else {
            r1 = this.A01;
            if (r1 == null || r1.A01 != r9) {
                r1 = new C90004Mf(new AnonymousClass0F0(r9), r9);
                this.A01 = r1;
            }
        }
        AbstractC06220Sq r7 = r1.A00;
        int A06 = r9.A06();
        View view = null;
        if (A06 != 0) {
            int i = Integer.MAX_VALUE;
            int A01 = A01(r7, r9);
            for (int i2 = 0; i2 < A06; i2++) {
                View A0D = r9.A0D(i2);
                int A05 = C12980iv.A05(A00(A0D, r7), A01);
                if (A05 < i) {
                    view = A0D;
                    i = A05;
                }
            }
        }
        return view;
    }

    public int[] A03(View view, AnonymousClass02H r7) {
        int[] A07 = C13000ix.A07();
        if (r7.A14()) {
            C90004Mf r1 = this.A00;
            if (r1 == null || r1.A01 != r7) {
                r1 = new C90004Mf(new AnonymousClass0Ez(r7), r7);
                this.A00 = r1;
            }
            AbstractC06220Sq r0 = r1.A00;
            A07[0] = A00(view, r0) - A01(r0, r7);
        } else {
            A07[0] = 0;
        }
        if (r7.A15()) {
            C90004Mf r12 = this.A01;
            if (r12 == null || r12.A01 != r7) {
                r12 = new C90004Mf(new AnonymousClass0F0(r7), r7);
                this.A01 = r12;
            }
            AbstractC06220Sq r02 = r12.A00;
            A07[1] = A00(view, r02) - A01(r02, r7);
            return A07;
        }
        A07[1] = 0;
        return A07;
    }
}
