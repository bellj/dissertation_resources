package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import com.facebook.redex.RunnableBRunnable0Shape0S0311000_I0;
import com.whatsapp.jid.Jid;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/* renamed from: X.131  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass131 {
    public final C15570nT A00;
    public final C22640zP A01;
    public final AnonymousClass10T A02;
    public final C15890o4 A03;
    public final C20750wG A04;
    public final AbstractC14440lR A05;

    public AnonymousClass131(C15570nT r1, C22640zP r2, AnonymousClass10T r3, C15890o4 r4, C20750wG r5, AbstractC14440lR r6) {
        this.A00 = r1;
        this.A05 = r6;
        this.A02 = r3;
        this.A04 = r5;
        this.A03 = r4;
        this.A01 = r2;
    }

    public Bitmap A00(Context context, C15370n3 r17, float f, int i) {
        Bitmap bitmap;
        Jid A0B = r17.A0B(AbstractC14640lm.class);
        try {
            boolean z = false;
            if (((float) i) >= context.getResources().getDisplayMetrics().density * 96.0f) {
                z = true;
            }
            if (A0B != null && ((r17.A0K() && !C15380n4.A0O(r17.A0D)) || (!r17.A0K() && !C15380n4.A0F(r17.A0D) && !C15380n4.A0M(A0B) && r17.A0f))) {
                int i2 = z ? r17.A04 : r17.A05;
                if ((!this.A00.A0F(r17.A0D) && !(r17 instanceof AnonymousClass1VR) && r17.A0A + 604800000 < System.currentTimeMillis()) || i2 == 0) {
                    this.A05.Ab2(new RunnableBRunnable0Shape0S0311000_I0(this, r17, A0B, i2, 0, z));
                }
            }
            if (r17.A0X) {
                try {
                    InputStream A02 = A02(r17, z);
                    try {
                        if (A02 == null) {
                            r17.A0X = false;
                            return null;
                        }
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inDither = true;
                        options.inScaled = false;
                        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                        Bitmap bitmap2 = C37501mV.A05(new C41591tm(options, null, i, i, true), A02).A02;
                        if (bitmap2 == null) {
                            bitmap = null;
                        } else {
                            bitmap = C21270x9.A00(bitmap2, f, i);
                        }
                        if (bitmap == null) {
                            StringBuilder sb = new StringBuilder();
                            sb.append("contactPhotosBitmapManager/getphotofast/");
                            sb.append(A0B);
                            sb.append(" decodeStream returns null");
                            Log.e(sb.toString());
                        }
                        A02.close();
                        return bitmap;
                    } catch (Throwable th) {
                        if (A02 != null) {
                            try {
                                A02.close();
                            } catch (Throwable unused) {
                            }
                        }
                        throw th;
                    }
                } catch (IOException unused2) {
                }
            }
            return null;
        } catch (OutOfMemoryError e) {
            Log.e("contactPhotosBitmapManager/getphotofast/out-of-memory ", e);
            return null;
        }
    }

    public Bitmap A01(Context context, C15370n3 r6, float f, int i) {
        AnonymousClass10T r0 = this.A02;
        String A0E = r6.A0E(f, i);
        C18720su r3 = r0.A02;
        Bitmap bitmap = (Bitmap) r3.A01().A00(A0E);
        if (bitmap != null) {
            return bitmap;
        }
        Bitmap A00 = A00(context, r6, f, i);
        if (A00 != null) {
            r3.A01().A03(r6.A0E(f, i), A00);
        }
        return A00;
    }

    public InputStream A02(C15370n3 r6, boolean z) {
        File A01;
        if (!r6.A0X) {
            return null;
        }
        AnonymousClass10T r1 = this.A02;
        if (z) {
            A01 = r1.A00(r6);
            if (A01 == null || !A01.exists()) {
                A01 = r1.A01(r6);
                if (r6.A04 > 0 && this.A03.A0A(Environment.getExternalStorageState())) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("contactPhotosBitmapManager/getphotostream/");
                    sb.append(r6.A0D);
                    sb.append(" full file missing id:");
                    sb.append(r6.A04);
                    Log.e(sb.toString());
                    r6.A04 = 0;
                }
            }
        } else {
            A01 = r1.A01(r6);
            if (A01 == null || !A01.exists()) {
                A01 = r1.A00(r6);
                if (r6.A05 > 0) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("contactPhotosBitmapManager/getphotostream/");
                    sb2.append(r6.A0D);
                    sb2.append(" thumb file missing id:");
                    sb2.append(r6.A05);
                    Log.e(sb2.toString());
                    r6.A05 = 0;
                }
            }
        }
        if (A01 == null || !A01.exists()) {
            return null;
        }
        try {
            return new FileInputStream(A01);
        } catch (FileNotFoundException e) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("contactPhotosBitmapManager/getphotostream/");
            sb3.append(r6.A0D);
            sb3.append(" photo file not found");
            Log.e(sb3.toString(), e);
            return null;
        }
    }
}
