package X;

import java.util.Map;

/* renamed from: X.174  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass174 implements AbstractC17310qb {
    public final /* synthetic */ C19960ux A00;

    public AnonymousClass174(C19960ux r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass6MF
    public /* bridge */ /* synthetic */ AbstractC120015fT A8U(String str, String str2, String str3, Map map, long j) {
        AnonymousClass01J r1 = this.A00.A01;
        return new AnonymousClass39Z((C18790t3) r1.AJw.get(), (C14820m6) r1.AN3.get(), (C14850m9) r1.A04.get(), (AnonymousClass18L) r1.A89.get(), C18000rk.A00(r1.AMu), str, str2, str3, map, r1.A8L, r1.A8M);
    }
}
