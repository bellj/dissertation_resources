package X;

import android.content.Context;
import com.whatsapp.payments.IDxRCallbackShape0S0200000_3_I1;
import java.io.UnsupportedEncodingException;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5xY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129335xY {
    public final Context A00;
    public final C14900mE A01;
    public final C15570nT A02;
    public final C18640sm A03;
    public final C14830m7 A04;
    public final AnonymousClass102 A05;
    public final C17220qS A06;
    public final C18650sn A07;
    public final C18610sj A08;
    public final C17070qD A09;
    public final AnonymousClass60T A0A;
    public final C30931Zj A0B = C30931Zj.A00("BrazilAddCredentialAction", "network", "BR");
    public final C129945yY A0C;

    public C129335xY(Context context, C14900mE r5, C15570nT r6, C18640sm r7, C14830m7 r8, AnonymousClass102 r9, C17220qS r10, C18650sn r11, C18610sj r12, C17070qD r13, AnonymousClass60T r14, C129945yY r15) {
        this.A04 = r8;
        this.A00 = context;
        this.A01 = r5;
        this.A02 = r6;
        this.A06 = r10;
        this.A09 = r13;
        this.A0C = r15;
        this.A08 = r12;
        this.A05 = r9;
        this.A03 = r7;
        this.A07 = r11;
        this.A0A = r14;
    }

    public final void A00(AnonymousClass6B7 r19, C126935tg r20, Boolean bool, String str, String str2, String str3) {
        try {
            JSONObject A0a = C117295Zj.A0a();
            A0a.put("pushAccountData", str);
            A0a.put("phone", str2);
            byte[] A9N = r19.A00.A9N(C117315Zl.A0a(C117305Zk.A0l("M", "issuer", A0a)), C003501n.A0D(16));
            String A0U = C117295Zj.A0U(this.A02, this.A04);
            C17220qS r11 = this.A06;
            String A01 = r11.A01();
            r11.A09(new IDxRCallbackShape0S0200000_3_I1(this.A00, this.A01, this.A07, r20, this, 0), new C130455zR(new C130465zS(r19.A03, A9N), new AnonymousClass3CT(A01), str3, A0U, r19.A05, (bool == null || bool.booleanValue()) ? "1" : "0", Long.parseLong(r19.A04)).A00, A01, 204, 0);
        } catch (UnsupportedEncodingException | JSONException e) {
            throw new Error(e);
        }
    }
}
