package X;

import android.database.ContentObserver;

/* renamed from: X.3fR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C72953fR extends ContentObserver {
    public C72953fR() {
        super(null);
    }

    @Override // android.database.ContentObserver
    public final void onChange(boolean z) {
        C95244dN.A0A.set(true);
    }
}
