package X;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import com.whatsapp.R;
import java.util.List;

/* renamed from: X.2gb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54362gb extends AnonymousClass02M {
    public int A00;
    public List A01;
    public final /* synthetic */ C469728k A02;

    public C54362gb(C469728k r1) {
        this.A02 = r1;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        int A09 = C12970iu.A09(this.A01);
        List list = this.A01;
        int i = this.A00;
        if (list != null) {
            i -= list.size();
        }
        return (i <= 0 || A09 <= 0) ? A09 : A09 + 1;
    }

    @Override // X.AnonymousClass02M
    public void ANH(AnonymousClass03U r8, int i) {
        Context context;
        int i2;
        String A02;
        int itemViewType = getItemViewType(i);
        if (itemViewType == 0) {
            C75423jp r82 = (C75423jp) r8;
            C15370n3 r6 = (C15370n3) this.A01.get(i);
            C469728k r4 = this.A02;
            TextView textView = r82.A01;
            if (!TextUtils.isEmpty(r6.A0K)) {
                A02 = r6.A0K;
            } else if (r6.A0L()) {
                A02 = C15610nY.A02(r6, false);
            } else {
                String A09 = r4.A0L.A09(C15370n3.A02(r6));
                if (!TextUtils.isEmpty(A09)) {
                    textView.setSingleLine(false);
                } else if (!TextUtils.isEmpty(r6.A0U)) {
                    A09 = r4.A0I.A09(r6);
                    textView.setSingleLine(false);
                    context = r4.A00;
                    i2 = R.color.secondary_text;
                    C12960it.A0s(context, textView, i2);
                    textView.setText(A09);
                    textView.setEllipsize(TextUtils.TruncateAt.END);
                    r4.A0E.A07(r82.A00, r6, false);
                } else {
                    A09 = r4.A0K.A0G(C248917h.A01(r6));
                    textView.setSingleLine(true);
                }
                context = r4.A00;
                i2 = R.color.primary_text;
                C12960it.A0s(context, textView, i2);
                textView.setText(A09);
                textView.setEllipsize(TextUtils.TruncateAt.END);
                r4.A0E.A07(r82.A00, r6, false);
            }
            textView.setText(A02);
            textView.setSingleLine(false);
            C12960it.A0s(r4.A00, textView, R.color.primary_text);
            textView.setEllipsize(TextUtils.TruncateAt.END);
            r4.A0E.A07(r82.A00, r6, false);
        } else if (itemViewType == 1) {
            TextView textView2 = ((C75333jg) r8).A00;
            Context context2 = this.A02.A00;
            Object[] objArr = new Object[1];
            List list = this.A01;
            int i3 = this.A00;
            if (list != null) {
                i3 -= list.size();
            }
            C12960it.A1P(objArr, i3, 0);
            textView2.setText(context2.getString(R.string.additional_participant_count, objArr));
        }
    }

    @Override // X.AnonymousClass02M
    public AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = this.A02.A01;
        if (i != 0) {
            return new C75333jg(layoutInflater.inflate(R.layout.accept_invite_participant_count, viewGroup, false));
        }
        return new C75423jp(layoutInflater.inflate(R.layout.accept_invite_participant, viewGroup, false));
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        List list = this.A01;
        int i2 = this.A00;
        if (list != null) {
            i2 -= list.size();
        }
        return (i2 <= 0 || i != this.A01.size()) ? 0 : 1;
    }
}
