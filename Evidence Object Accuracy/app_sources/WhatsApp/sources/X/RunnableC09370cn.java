package X;

import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.0cn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09370cn implements Runnable {
    public final /* synthetic */ C06120Sg A00;

    public RunnableC09370cn(C06120Sg r1) {
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        C06120Sg r3 = this.A00;
        if (r3.A03 != null) {
            AnonymousClass0ST r0 = r3.A03;
            Object obj = r0.A00;
            if (obj != null) {
                synchronized (r3) {
                    Iterator it = new ArrayList(r3.A02).iterator();
                    while (it.hasNext()) {
                        ((AbstractC12010hE) it.next()).AVI(obj);
                    }
                }
            }
            Throwable th = r0.A01;
            synchronized (r3) {
                ArrayList arrayList = new ArrayList(r3.A01);
                if (arrayList.isEmpty()) {
                    AnonymousClass0R5.A01("Lottie encountered an error but no failure listener was added:", th);
                } else {
                    Iterator it2 = arrayList.iterator();
                    while (it2.hasNext()) {
                        ((AbstractC12010hE) it2.next()).AVI(th);
                    }
                }
            }
        }
    }
}
