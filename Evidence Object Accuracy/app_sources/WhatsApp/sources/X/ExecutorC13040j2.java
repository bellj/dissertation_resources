package X;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;

/* renamed from: X.0j2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ExecutorC13040j2 implements Executor {
    public static final Handler A00 = new Handler(Looper.getMainLooper());

    @Override // java.util.concurrent.Executor
    public void execute(Runnable runnable) {
        A00.post(runnable);
    }
}
