package X;

import android.graphics.Bitmap;

/* renamed from: X.536  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass536 implements AbstractC49122Jj {
    public final AnonymousClass1IS A00;
    public final /* synthetic */ AnonymousClass2I3 A01;
    public final /* synthetic */ Bitmap[] A02;

    public AnonymousClass536(AnonymousClass1IS r1, AnonymousClass2I3 r2, Bitmap[] bitmapArr) {
        this.A01 = r2;
        this.A02 = bitmapArr;
        this.A00 = r1;
    }

    @Override // X.AbstractC49122Jj
    public void ATN(C14320lF r8, boolean z) {
        AnonymousClass1IS r2 = this.A00;
        AnonymousClass2I3 r1 = this.A01;
        if (r2 == r1.A0B) {
            AnonymousClass3JH r3 = r8.A07;
            int i = r1.A06;
            r1.A07(r2, r3, r1.A04(r8), this.A02, i);
        }
    }
}
