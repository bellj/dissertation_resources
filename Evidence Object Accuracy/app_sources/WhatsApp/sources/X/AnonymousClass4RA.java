package X;

/* renamed from: X.4RA  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4RA {
    public final int A00;
    public final int A01;
    public final long A02;
    public final C78423ot A03;

    public AnonymousClass4RA(C78423ot r1, int i, int i2, long j) {
        this.A03 = r1;
        this.A00 = i;
        this.A02 = j;
        this.A01 = i2;
    }
}
