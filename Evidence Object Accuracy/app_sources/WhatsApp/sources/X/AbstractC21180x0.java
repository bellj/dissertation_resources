package X;

import java.util.Collection;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0x0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public interface AbstractC21180x0 {
    void A9P(short s, boolean z);

    void A9Q(int i, short s);

    Collection AAh();

    boolean AID();

    boolean AJj(int i);

    void AKu(int i, String str, int i2);

    void AKv(int i, String str, long j);

    void AKw(int i, String str, String str2);

    void AKx(int i, String str, boolean z);

    void AKy(String str, int i, int i2, int i3);

    void AKz(String str, int i, int i2, long j);

    void AL0(String str, String str2, int i, int i2);

    void AL1(String str, int i, int i2, boolean z);

    void AL2(String str, double d, int i);

    void AL3(String str, String[] strArr, int i);

    void AL4(AbstractC29031Pz v, int i);

    void AL5(int i, int i2);

    void AL6(int i, int i2, short s);

    void AL7(int i, short s);

    void AL8(String str, int i, int i2, short s);

    boolean ALA(int i);

    void ALB(int i, String str);

    void ALC(int i, String str, int i2);

    void ALD(String str, TimeUnit timeUnit, int i, long j);

    void ALE(int i);

    void ALF(int i, int i2);

    void ALG(int i, String str, String str2);

    void ALH(String str, String str2, int i, int i2, boolean z);

    void ALI(String str, String str2, TimeUnit timeUnit, int i, long j);

    Long AZ2();

    Integer AZ3();

    String AZK();

    void AgE();
}
