package X;

import android.view.View;
import android.widget.TextView;
import androidx.appcompat.widget.SwitchCompat;
import com.whatsapp.R;

/* renamed from: X.5lT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122445lT extends AbstractC118825cR {
    public final View A00;
    public final TextView A01;
    public final TextView A02;
    public final SwitchCompat A03;

    public C122445lT(View view) {
        super(view);
        this.A02 = C12960it.A0I(view, R.id.novi_hub_toggle_title);
        this.A01 = C12960it.A0I(view, R.id.novi_hub_toggle_desc);
        this.A03 = (SwitchCompat) AnonymousClass028.A0D(view, R.id.toggle_button);
        this.A00 = AnonymousClass028.A0D(view, R.id.toggle_container);
    }
}
