package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import com.whatsapp.reactions.ReactionEmojiTextView;
import com.whatsapp.reactions.ReactionsTrayViewModel;
import com.whatsapp.util.ViewOnClickCListenerShape18S0100000_I1_1;

/* renamed from: X.2YR  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2YR extends AnimatorListenerAdapter {
    public final /* synthetic */ ReactionEmojiTextView A00;
    public final /* synthetic */ ViewOnClickCListenerShape18S0100000_I1_1 A01;

    public AnonymousClass2YR(ReactionEmojiTextView reactionEmojiTextView, ViewOnClickCListenerShape18S0100000_I1_1 viewOnClickCListenerShape18S0100000_I1_1) {
        this.A01 = viewOnClickCListenerShape18S0100000_I1_1;
        this.A00 = reactionEmojiTextView;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
        animator.removeListener(this);
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        ReactionsTrayViewModel reactionsTrayViewModel = ((C53232dO) this.A01.A00).A09;
        String A0q = C12980iv.A0q(this.A00);
        C36161jQ r3 = reactionsTrayViewModel.A0A;
        if (A0q.equals(((AnonymousClass4X9) r3.A01()).A00)) {
            A0q = "";
        }
        reactionsTrayViewModel.A04(0);
        r3.A0B(new AnonymousClass4X9(((AnonymousClass4X9) r3.A01()).A00, A0q, true));
    }
}
