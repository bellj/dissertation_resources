package X;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import androidx.appcompat.widget.SwitchCompat;
import com.whatsapp.R;
import com.whatsapp.payments.ui.BrazilFbPayHubActivity;
import java.util.List;

/* renamed from: X.5iL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractView$OnClickListenerC121485iL extends AbstractActivityC119255dQ implements View.OnClickListener, AbstractC136336Md, AbstractC136326Mc, AnonymousClass6M5, AnonymousClass6LZ {
    public View A00;
    public View A01;
    public View A02;
    public View A03;
    public View A04;
    public View A05;
    public ListView A06;
    public SwitchCompat A07;
    public C20380vf A08;
    public C21860y6 A09;
    public C20400vh A0A;
    public C18650sn A0B;
    public C18660so A0C;
    public C20390vg A0D;
    public C18600si A0E;
    public C243515e A0F;
    public C18610sj A0G;
    public C17070qD A0H;
    public C129135xE A0I;
    public AnonymousClass1A7 A0J;
    public AnonymousClass60T A0K;
    public C117615aH A0L;
    public C129285xT A0M;
    public C129735yD A0N;
    public C1329368u A0O;

    @Override // X.AbstractC136326Mc
    public String AEP(AbstractC28901Pl r5) {
        int i;
        BrazilFbPayHubActivity brazilFbPayHubActivity = (BrazilFbPayHubActivity) this;
        StringBuilder A0h = C12960it.A0h();
        boolean A08 = brazilFbPayHubActivity.A04.A08();
        AnonymousClass1ZY r0 = r5.A08;
        if (A08) {
            if (r0 == null || r0.A0A()) {
                if (r5.A01 == 2) {
                    A0h.append(brazilFbPayHubActivity.getString(R.string.p2p_default_method_message_enabled));
                }
                if (r5.A03 == 2) {
                    if (A0h.length() > 0) {
                        A0h.append("\n");
                    }
                    A0h.append(brazilFbPayHubActivity.getString(R.string.p2m_default_method_message_enabled));
                }
                return A0h.toString();
            }
        } else if (r0 == null || r0.A0A()) {
            if (r5.A01 != 2) {
                return null;
            }
            i = R.string.default_payment_method_set;
            return brazilFbPayHubActivity.getString(i);
        }
        i = R.string.payment_method_unverified;
        return brazilFbPayHubActivity.getString(i);
    }

    @Override // X.AnonymousClass6M5
    public void AfR(List list) {
        C117615aH r0 = this.A0L;
        r0.A02 = list;
        r0.notifyDataSetChanged();
        C125085qf.A00(this.A06);
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        if (view.getId() == R.id.add_new_account || view.getId() == R.id.p2p_onboarding_nudge_button) {
            ALz(C12960it.A1T(this.A0L.getCount()));
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        int A01 = C117305Zk.A01(this, R.layout.fb_pay_hub);
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            C117295Zj.A0h(this, A1U, R.string.payment_settings, A01);
        }
        this.A03 = findViewById(R.id.payment_methods_container);
        this.A04 = findViewById(R.id.p2p_onboarding_nudge_container);
        findViewById(R.id.p2p_onboarding_nudge_button).setOnClickListener(this);
        BrazilFbPayHubActivity brazilFbPayHubActivity = (BrazilFbPayHubActivity) this;
        this.A0L = new C117615aH(brazilFbPayHubActivity, ((ActivityC13830kP) brazilFbPayHubActivity).A01, ((AbstractView$OnClickListenerC121485iL) brazilFbPayHubActivity).A0H, brazilFbPayHubActivity);
        ListView listView = (ListView) findViewById(R.id.methods_list);
        this.A06 = listView;
        listView.setAdapter((ListAdapter) this.A0L);
        AbstractC14440lR r9 = ((ActivityC13830kP) this).A05;
        C17070qD r8 = this.A0H;
        C30941Zk r21 = new C30941Zk();
        C18600si r7 = this.A0E;
        C21860y6 r13 = this.A09;
        C18610sj r4 = this.A0G;
        AnonymousClass1A7 r3 = this.A0J;
        C1329368u r10 = new C1329368u(this, this.A08, r13, this.A0C, this.A0D, r7, this.A0F, r4, r8, r3, r21, this, this, new C133786Cc(), r9, false);
        this.A0O = r10;
        r10.A02(false, false);
        this.A06.setOnItemClickListener(new AdapterView.OnItemClickListener() { // from class: X.659
            @Override // android.widget.AdapterView.OnItemClickListener
            public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
                AbstractView$OnClickListenerC121485iL r1 = AbstractView$OnClickListenerC121485iL.this;
                r1.ATY(C117315Zl.A08(r1.A0L.A02, i));
            }
        });
        View findViewById = findViewById(R.id.add_new_account);
        this.A01 = findViewById;
        findViewById.setOnClickListener(this);
        AnonymousClass2GE.A07(C117305Zk.A06(this, R.id.change_pin_icon), A01);
        AnonymousClass2GE.A07(C117305Zk.A06(this, R.id.add_new_account_icon), A01);
        AnonymousClass2GE.A07(C117305Zk.A06(this, R.id.fingerprint_setting_icon), A01);
        AnonymousClass2GE.A07(C117305Zk.A06(this, R.id.delete_payments_account_icon), A01);
        AnonymousClass2GE.A07(C117305Zk.A06(this, R.id.request_payment_account_info_icon), A01);
        this.A05 = findViewById(R.id.pin_container);
        this.A02 = findViewById(R.id.fingerprint_container);
        this.A07 = (SwitchCompat) findViewById(R.id.toggle_fingerprint);
        AbstractC14440lR r102 = ((ActivityC13830kP) brazilFbPayHubActivity).A05;
        C17070qD r92 = ((AbstractView$OnClickListenerC121485iL) brazilFbPayHubActivity).A0H;
        AnonymousClass605 r82 = brazilFbPayHubActivity.A07;
        C129285xT r72 = new C129285xT(brazilFbPayHubActivity, brazilFbPayHubActivity.A01, brazilFbPayHubActivity.A03, r92, brazilFbPayHubActivity.A06, r82, brazilFbPayHubActivity.A08, r102);
        this.A0M = r72;
        AnonymousClass61E r6 = r72.A05;
        boolean A06 = r6.A00.A06();
        boolean z = false;
        AbstractView$OnClickListenerC121485iL r32 = (AbstractView$OnClickListenerC121485iL) r72.A08;
        if (A06) {
            r32.A02.setVisibility(0);
            if (r6.A02() == 1) {
                z = true;
            }
            r32.A07.setChecked(z);
            r72.A00 = true;
        } else {
            r32.A02.setVisibility(8);
        }
        C117295Zj.A0n(findViewById(R.id.change_pin), this, 11);
        C117295Zj.A0n(this.A02, this, 12);
        this.A00 = findViewById(R.id.account_actions_container);
        this.A0N = brazilFbPayHubActivity.A0C;
        findViewById(R.id.delete_payments_account_action).setOnClickListener(new C123895o7(this));
        findViewById(R.id.request_dyi_report_action).setOnClickListener(new C123905o8(this));
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        return ((BrazilFbPayHubActivity) this).A0C.A01(null, this, i);
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i, Bundle bundle) {
        return ((BrazilFbPayHubActivity) this).A0C.A01(bundle, this, i);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        C1329368u r2 = this.A0O;
        C124285ou r1 = r2.A02;
        if (r1 != null) {
            r1.A03(true);
        }
        r2.A02 = null;
        AbstractC35651iS r12 = r2.A00;
        if (r12 != null) {
            r2.A09.A04(r12);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        this.A0O.A00(true);
        C129285xT r4 = this.A0M;
        boolean A03 = r4.A07.A03();
        boolean z = false;
        AbstractView$OnClickListenerC121485iL r2 = (AbstractView$OnClickListenerC121485iL) r4.A08;
        if (A03) {
            r2.A05.setVisibility(0);
            AnonymousClass61E r1 = r4.A05;
            if (r1.A00.A06()) {
                r4.A00 = false;
                if (r1.A02() == 1) {
                    z = true;
                }
                r2.A07.setChecked(z);
                r4.A00 = true;
                return;
            }
            return;
        }
        r2.A05.setVisibility(8);
    }
}
