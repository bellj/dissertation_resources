package X;

import android.widget.AbsListView;
import com.whatsapp.conversationslist.ConversationsFragment;

/* renamed from: X.4oo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C102234oo implements AbsListView.OnScrollListener {
    public final /* synthetic */ ConversationsFragment A00;

    @Override // android.widget.AbsListView.OnScrollListener
    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
    }

    public C102234oo(ConversationsFragment conversationsFragment) {
        this.A00 = conversationsFragment;
    }

    @Override // android.widget.AbsListView.OnScrollListener
    public void onScrollStateChanged(AbsListView absListView, int i) {
        if (i != 0) {
            if (i == 1) {
                this.A00.A1r.A01(2);
            }
            ViewTreeObserver$OnGlobalLayoutListenerC33691ev r0 = this.A00.A22;
            if (r0 != null) {
                r0.A00();
                return;
            }
            return;
        }
        this.A00.A1r.A00();
    }
}
