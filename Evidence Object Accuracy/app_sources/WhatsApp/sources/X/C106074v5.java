package X;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/* renamed from: X.4v5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106074v5 implements AnonymousClass5Pk {
    public final Executor A00;
    public final Executor A01;
    public final Executor A02 = Executors.newFixedThreadPool(2, new ThreadFactoryC71543d6("FrescoIoBoundExecutor"));
    public final Executor A03;

    public C106074v5(int i) {
        this.A01 = Executors.newFixedThreadPool(i, new ThreadFactoryC71543d6("FrescoDecodeExecutor"));
        this.A00 = Executors.newFixedThreadPool(i, new ThreadFactoryC71543d6("FrescoBackgroundExecutor"));
        this.A03 = Executors.newFixedThreadPool(1, new ThreadFactoryC71543d6("FrescoLightWeightBackgroundExecutor"));
    }
}
