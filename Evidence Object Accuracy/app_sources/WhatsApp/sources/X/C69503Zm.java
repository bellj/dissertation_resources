package X;

/* renamed from: X.3Zm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69503Zm implements AbstractC29031Pz {
    public final /* synthetic */ AnonymousClass3DS A00;
    public final /* synthetic */ AbstractC14640lm A01;
    public final /* synthetic */ AbstractC15340mz A02;

    @Override // X.AbstractC29031Pz
    public String ADy() {
        return "offset_from_end_of_chat";
    }

    public C69503Zm(AnonymousClass3DS r1, AbstractC14640lm r2, AbstractC15340mz r3) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
    }

    @Override // X.AbstractC29031Pz
    public void A98(C28631Oi r5) {
        String str;
        int A00 = this.A00.A05.A00(this.A01, this.A02.A12);
        if (A00 < 100) {
            int i = A00 / 25;
            StringBuilder A0h = C12960it.A0h();
            A0h.append(i * 25);
            A0h.append("-");
            str = C12960it.A0f(A0h, ((i + 1) * 25) - 1);
        } else {
            str = A00 < 1000 ? "100-999" : A00 < 10000 ? "1000-9999" : "10000+";
        }
        r5.A00(str.length(), "offset_from_end_of_chat", str);
    }
}
