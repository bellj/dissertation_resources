package X;

import androidx.biometric.BiometricFragment;

/* renamed from: X.0dW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09790dW implements Runnable {
    public final /* synthetic */ int A00;
    public final /* synthetic */ BiometricFragment A01;
    public final /* synthetic */ CharSequence A02;

    public RunnableC09790dW(BiometricFragment biometricFragment, CharSequence charSequence, int i) {
        this.A01 = biometricFragment;
        this.A00 = i;
        this.A02 = charSequence;
    }

    @Override // java.lang.Runnable
    public void run() {
        AnonymousClass0EP r0 = this.A01.A01;
        AnonymousClass0PW r2 = r0.A04;
        if (r2 == null) {
            r2 = new C02450Ci(r0);
            r0.A04 = r2;
        }
        r2.A01(this.A00, this.A02);
    }
}
