package X;

import android.graphics.SurfaceTexture;
import android.view.TextureView;
import com.whatsapp.util.Log;

/* renamed from: X.4mg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class TextureView$SurfaceTextureListenerC100914mg implements TextureView.SurfaceTextureListener {
    public final /* synthetic */ C849240i A00;

    @Override // android.view.TextureView.SurfaceTextureListener
    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
    }

    public TextureView$SurfaceTextureListenerC100914mg(C849240i r1) {
        this.A00 = r1;
    }

    @Override // android.view.TextureView.SurfaceTextureListener
    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i2) {
        StringBuilder A0h = C12960it.A0h();
        C849240i r1 = this.A00;
        A0h.append(r1.A09);
        A0h.append("/onSurfaceTextureAvailable port = ");
        Log.i(C12960it.A0f(A0h, r1.hashCode()));
        r1.A05();
    }

    @Override // android.view.TextureView.SurfaceTextureListener
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        C849240i r0 = this.A00;
        r0.hashCode();
        r0.A04();
        return true;
    }

    @Override // android.view.TextureView.SurfaceTextureListener
    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
        StringBuilder A0h = C12960it.A0h();
        C849240i r1 = this.A00;
        A0h.append(r1.A09);
        A0h.append("/surfaceTextureSizeChanged port = ");
        A0h.append(r1.hashCode());
        A0h.append(", size: ");
        A0h.append(i);
        Log.i(C12960it.A0e("x", A0h, i2));
        AnonymousClass009.A01();
        ((Number) AnonymousClass5B1.A00(r1, new CallableC112555Dv(r1, i, i2))).intValue();
        AnonymousClass5X3 r0 = r1.A02;
        if (r0 != null) {
            r0.ATu(r1);
        }
    }
}
