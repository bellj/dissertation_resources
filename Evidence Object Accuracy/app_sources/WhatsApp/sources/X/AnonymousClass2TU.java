package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2TU  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2TU extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass2TU A09;
    public static volatile AnonymousClass255 A0A;
    public int A00;
    public AbstractC27881Jp A01;
    public AbstractC27881Jp A02;
    public AbstractC27881Jp A03;
    public AbstractC27881Jp A04;
    public AbstractC27881Jp A05;
    public AbstractC27881Jp A06;
    public AbstractC27881Jp A07;
    public AbstractC27881Jp A08;

    static {
        AnonymousClass2TU r0 = new AnonymousClass2TU();
        A09 = r0;
        r0.A0W();
    }

    public AnonymousClass2TU() {
        AbstractC27881Jp r0 = AbstractC27881Jp.A01;
        this.A05 = r0;
        this.A04 = r0;
        this.A03 = r0;
        this.A06 = r0;
        this.A08 = r0;
        this.A07 = r0;
        this.A01 = r0;
        this.A02 = r0;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r6, Object obj, Object obj2) {
        switch (r6.ordinal()) {
            case 0:
                return A09;
            case 1:
                AbstractC462925h r7 = (AbstractC462925h) obj;
                AnonymousClass2TU r8 = (AnonymousClass2TU) obj2;
                boolean z = true;
                if ((this.A00 & 1) != 1) {
                    z = false;
                }
                AbstractC27881Jp r2 = this.A05;
                boolean z2 = true;
                if ((r8.A00 & 1) != 1) {
                    z2 = false;
                }
                this.A05 = r7.Afm(r2, r8.A05, z, z2);
                boolean z3 = false;
                if ((this.A00 & 2) == 2) {
                    z3 = true;
                }
                AbstractC27881Jp r3 = this.A04;
                boolean z4 = false;
                if ((r8.A00 & 2) == 2) {
                    z4 = true;
                }
                this.A04 = r7.Afm(r3, r8.A04, z3, z4);
                boolean z5 = false;
                if ((this.A00 & 4) == 4) {
                    z5 = true;
                }
                AbstractC27881Jp r32 = this.A03;
                boolean z6 = false;
                if ((r8.A00 & 4) == 4) {
                    z6 = true;
                }
                this.A03 = r7.Afm(r32, r8.A03, z5, z6);
                boolean z7 = false;
                if ((this.A00 & 8) == 8) {
                    z7 = true;
                }
                AbstractC27881Jp r33 = this.A06;
                boolean z8 = false;
                if ((r8.A00 & 8) == 8) {
                    z8 = true;
                }
                this.A06 = r7.Afm(r33, r8.A06, z7, z8);
                boolean z9 = false;
                if ((this.A00 & 16) == 16) {
                    z9 = true;
                }
                AbstractC27881Jp r34 = this.A08;
                boolean z10 = false;
                if ((r8.A00 & 16) == 16) {
                    z10 = true;
                }
                this.A08 = r7.Afm(r34, r8.A08, z9, z10);
                boolean z11 = false;
                if ((this.A00 & 32) == 32) {
                    z11 = true;
                }
                AbstractC27881Jp r35 = this.A07;
                boolean z12 = false;
                if ((r8.A00 & 32) == 32) {
                    z12 = true;
                }
                this.A07 = r7.Afm(r35, r8.A07, z11, z12);
                boolean z13 = false;
                if ((this.A00 & 64) == 64) {
                    z13 = true;
                }
                AbstractC27881Jp r36 = this.A01;
                boolean z14 = false;
                if ((r8.A00 & 64) == 64) {
                    z14 = true;
                }
                this.A01 = r7.Afm(r36, r8.A01, z13, z14);
                boolean z15 = false;
                if ((this.A00 & 128) == 128) {
                    z15 = true;
                }
                AbstractC27881Jp r37 = this.A02;
                boolean z16 = false;
                if ((r8.A00 & 128) == 128) {
                    z16 = true;
                }
                this.A02 = r7.Afm(r37, r8.A02, z15, z16);
                if (r7 == C463025i.A00) {
                    this.A00 |= r8.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r72 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        try {
                            int A03 = r72.A03();
                            if (A03 == 0) {
                                break;
                            } else if (A03 == 10) {
                                this.A00 |= 1;
                                this.A05 = r72.A08();
                            } else if (A03 == 18) {
                                this.A00 |= 2;
                                this.A04 = r72.A08();
                            } else if (A03 == 26) {
                                this.A00 |= 4;
                                this.A03 = r72.A08();
                            } else if (A03 == 34) {
                                this.A00 |= 8;
                                this.A06 = r72.A08();
                            } else if (A03 == 42) {
                                this.A00 |= 16;
                                this.A08 = r72.A08();
                            } else if (A03 == 50) {
                                this.A00 |= 32;
                                this.A07 = r72.A08();
                            } else if (A03 == 58) {
                                this.A00 |= 64;
                                this.A01 = r72.A08();
                            } else if (A03 == 66) {
                                this.A00 |= 128;
                                this.A02 = r72.A08();
                            } else if (!A0a(r72, A03)) {
                                break;
                            }
                        } catch (C28971Pt e) {
                            e.unfinishedMessage = this;
                            throw new RuntimeException(e);
                        }
                    } catch (IOException e2) {
                        C28971Pt r1 = new C28971Pt(e2.getMessage());
                        r1.unfinishedMessage = this;
                        throw new RuntimeException(r1);
                    }
                }
            case 3:
                return null;
            case 4:
                return new AnonymousClass2TU();
            case 5:
                return new AnonymousClass2TV();
            case 6:
                break;
            case 7:
                if (A0A == null) {
                    synchronized (AnonymousClass2TU.class) {
                        if (A0A == null) {
                            A0A = new AnonymousClass255(A09);
                        }
                    }
                }
                return A0A;
            default:
                throw new UnsupportedOperationException();
        }
        return A09;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A09(this.A05, 1);
        }
        if ((i3 & 2) == 2) {
            i2 += CodedOutputStream.A09(this.A04, 2);
        }
        if ((i3 & 4) == 4) {
            i2 += CodedOutputStream.A09(this.A03, 3);
        }
        if ((i3 & 8) == 8) {
            i2 += CodedOutputStream.A09(this.A06, 4);
        }
        if ((i3 & 16) == 16) {
            i2 += CodedOutputStream.A09(this.A08, 5);
        }
        if ((i3 & 32) == 32) {
            i2 += CodedOutputStream.A09(this.A07, 6);
        }
        if ((i3 & 64) == 64) {
            i2 += CodedOutputStream.A09(this.A01, 7);
        }
        if ((i3 & 128) == 128) {
            i2 += CodedOutputStream.A09(this.A02, 8);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0K(this.A05, 1);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0K(this.A04, 2);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0K(this.A03, 3);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0K(this.A06, 4);
        }
        if ((this.A00 & 16) == 16) {
            codedOutputStream.A0K(this.A08, 5);
        }
        if ((this.A00 & 32) == 32) {
            codedOutputStream.A0K(this.A07, 6);
        }
        if ((this.A00 & 64) == 64) {
            codedOutputStream.A0K(this.A01, 7);
        }
        if ((this.A00 & 128) == 128) {
            codedOutputStream.A0K(this.A02, 8);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
