package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.4BM  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4BM extends Enum {
    public static final /* synthetic */ AnonymousClass4BM[] A00;
    public static final AnonymousClass4BM A01;
    public static final AnonymousClass4BM A02;
    public static final AnonymousClass4BM A03;
    public static final AnonymousClass4BM A04;
    public final String id;
    public final int rootCategoryVersion;

    static {
        AnonymousClass4BM r14 = new AnonymousClass4BM("APPAREL_CLOTHING", "1086422341396773", 0, 1);
        A01 = r14;
        AnonymousClass4BM r34 = new AnonymousClass4BM("SHOPPING_RETAIL", "200600219953504", 1, 2);
        AnonymousClass4BM r33 = new AnonymousClass4BM("AUTOMOTIVE", "1223524174334504", 2, 1);
        AnonymousClass4BM r2 = new AnonymousClass4BM("RESTAURANT", "273819889375819", 3, 1);
        A04 = r2;
        AnonymousClass4BM r1 = new AnonymousClass4BM("GROCERY_STORE", "150108431712141", 4, 1);
        A02 = r1;
        AnonymousClass4BM r32 = new AnonymousClass4BM("PIZZA_PLACE", "180256082015845", 5, 1);
        AnonymousClass4BM r31 = new AnonymousClass4BM("FOOD_BEVERAGE", "1562965077339698", 6, 2);
        AnonymousClass4BM r30 = new AnonymousClass4BM("EDUCATION", "2250", 7, 2);
        AnonymousClass4BM r29 = new AnonymousClass4BM("SPORTS_RECREATION", "186982054657561", 8, 2);
        AnonymousClass4BM r28 = new AnonymousClass4BM("LOCAL_SERVICE", "1758418281071392", 9, 2);
        AnonymousClass4BM r27 = new AnonymousClass4BM("ADVERTISING_MARKETING", "1757592557789532", 10, 2);
        AnonymousClass4BM r26 = new AnonymousClass4BM("AGRICULTURE", "1574325646194878", 11, 1);
        AnonymousClass4BM r25 = new AnonymousClass4BM("ARTS_ENTERTAINMENT", "133436743388217", 12, 2);
        AnonymousClass4BM r24 = new AnonymousClass4BM("BEAUTY_COSMETIC_PERSONAL_CARE", "139225689474222", 13, 2);
        AnonymousClass4BM r23 = new AnonymousClass4BM("COMMERCIAL_INDUSTRIAL", "243290832429433", 14, 2);
        AnonymousClass4BM r22 = new AnonymousClass4BM("COMMUNITY_ORGANIZATION", "152880021441864", 15, 2);
        AnonymousClass4BM r21 = new AnonymousClass4BM("FINANCE", "1022050661163852", 16, 1);
        AnonymousClass4BM r20 = new AnonymousClass4BM("HOTEL_LODGING", "505091123022329", 17, 2);
        AnonymousClass4BM r19 = new AnonymousClass4BM("INTEREST", "1500", 18, 2);
        AnonymousClass4BM r18 = new AnonymousClass4BM("LEGAL", "241113486274430", 19, 1);
        AnonymousClass4BM r17 = new AnonymousClass4BM("MEDIA", "1314020451960517", 20, 2);
        AnonymousClass4BM r16 = new AnonymousClass4BM("MEDIA_NEWS_COMPANY", "2233", 21, 2);
        AnonymousClass4BM r13 = new AnonymousClass4BM("MEDICAL_HEALTH", "145118935550090", 22, 1);
        AnonymousClass4BM r12 = new AnonymousClass4BM("NON_GOV_ORG", "2235", 23, 1);
        AnonymousClass4BM r11 = new AnonymousClass4BM("NON_PROFIT_ORG", "2603", 24, 1);
        AnonymousClass4BM r10 = new AnonymousClass4BM("PUBLIC_GOV_SERVICE", "147714868971098", 25, 1);
        AnonymousClass4BM r9 = new AnonymousClass4BM("REAL_STATE", "198327773511962", 26, 2);
        AnonymousClass4BM r8 = new AnonymousClass4BM("SCIENCE_TECH_ENGINEERING", "297544187300691", 27, 2);
        AnonymousClass4BM r7 = new AnonymousClass4BM("TRAVEL_TRANSPORT", "128232937246338", 28, 2);
        AnonymousClass4BM r6 = new AnonymousClass4BM("VEHICLE_AIRCRAFT_BOAT", "180410821995109", 29, 2);
        AnonymousClass4BM r4 = new AnonymousClass4BM("OTHERS", "others", 30, 1);
        A03 = r4;
        AnonymousClass4BM[] r3 = new AnonymousClass4BM[31];
        C72453ed.A1J(r14, r34, r3);
        C12980iv.A1P(r33, r2, r1, r3);
        C12970iu.A1R(r32, r31, r30, r29, r3);
        C72453ed.A1G(r28, r27, r26, r25, r3);
        C72453ed.A1H(r24, r23, r22, r21, r3);
        C72453ed.A1I(r20, r19, r18, r3);
        C12960it.A1G(r17, r16, r13, r12, r3);
        r3[24] = r11;
        C12960it.A1H(r10, r9, r8, r7, r3);
        r3[29] = r6;
        r3[30] = r4;
        A00 = r3;
    }

    public AnonymousClass4BM(String str, String str2, int i, int i2) {
        this.id = str2;
        this.rootCategoryVersion = i2;
    }

    public static boolean A00(String str) {
        AnonymousClass4BM[] values = values();
        for (AnonymousClass4BM r1 : values) {
            if (r1.id.equals(str) && r1.rootCategoryVersion == 2) {
                return true;
            }
        }
        return false;
    }

    public static AnonymousClass4BM[] values() {
        return (AnonymousClass4BM[]) A00.clone();
    }
}
