package X;

import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;

/* renamed from: X.1DO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1DO implements AbstractC18270sB {
    public final C15570nT A00;
    public final C17010q7 A01;
    public final C20730wE A02;
    public final C15510nN A03;
    public final AbstractC14440lR A04;

    public AnonymousClass1DO(C15570nT r1, C17010q7 r2, C20730wE r3, C15510nN r4, AbstractC14440lR r5) {
        this.A00 = r1;
        this.A04 = r5;
        this.A02 = r3;
        this.A03 = r4;
        this.A01 = r2;
    }

    @Override // X.AbstractC18270sB
    public void ARF() {
        C15570nT r0 = this.A00;
        r0.A08();
        if (r0.A00 != null && this.A03.A01()) {
            AbstractC14440lR r3 = this.A04;
            r3.Ab4(new RunnableBRunnable0Shape4S0100000_I0_4(this.A02, 32), "ContactSyncHourlyCron/contactSyncMethods::fullSyncAndInitialize");
            r3.Ab4(new RunnableBRunnable0Shape4S0100000_I0_4(this.A01, 31), "ContactSyncHourlyCron/contactDiscoverySyncHelper::syncOutContactIfNoSyncInLastDay");
        }
    }
}
