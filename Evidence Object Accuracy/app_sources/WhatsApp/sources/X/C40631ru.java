package X;

import android.text.TextUtils;
import java.util.Arrays;

/* renamed from: X.1ru  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C40631ru {
    public long A00;
    public Boolean A01;
    public Integer A02;

    public C40631ru(Boolean bool, Integer num, long j) {
        this.A00 = j;
        this.A02 = num;
        this.A01 = bool;
    }

    public static C40631ru A00(String str) {
        String[] split = str.split(",");
        long A01 = C40601rr.A01(split, 0);
        Integer num = null;
        if (split.length > 1) {
            String str2 = split[1];
            if (!"null".equals(str2)) {
                num = Integer.valueOf(C28421Nd.A00(str2, 0));
            }
        }
        return new C40631ru(C40601rr.A02(split, 2), num, A01);
    }

    public String toString() {
        return TextUtils.join(",", Arrays.asList(Long.valueOf(this.A00), this.A02, this.A01));
    }
}
