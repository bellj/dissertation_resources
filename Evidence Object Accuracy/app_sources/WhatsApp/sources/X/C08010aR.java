package X;

import android.graphics.Path;
import android.graphics.PointF;
import java.util.List;

/* renamed from: X.0aR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08010aR implements AbstractC12850if, AbstractC12030hG, AbstractC12870ih {
    public AnonymousClass0OG A00 = new AnonymousClass0OG();
    public boolean A01;
    public final Path A02 = new Path();
    public final AnonymousClass0AA A03;
    public final AnonymousClass0QR A04;
    public final AnonymousClass0QR A05;
    public final AnonymousClass0QR A06;
    public final AnonymousClass0QR A07;
    public final AnonymousClass0QR A08;
    public final AnonymousClass0QR A09;
    public final AnonymousClass0QR A0A;
    public final EnumC03790Jd A0B;
    public final String A0C;
    public final boolean A0D;

    public C08010aR(AnonymousClass0AA r10, C08180ai r11, AbstractC08070aX r12) {
        AnonymousClass0H1 r0;
        this.A03 = r10;
        this.A0C = r11.A08;
        EnumC03790Jd r3 = r11.A07;
        this.A0B = r3;
        this.A0D = r11.A09;
        AnonymousClass0H1 r8 = new AnonymousClass0H1(r11.A04.A00);
        this.A08 = r8;
        AnonymousClass0QR A88 = r11.A06.A88();
        this.A09 = A88;
        AnonymousClass0H1 r6 = new AnonymousClass0H1(r11.A05.A00);
        this.A0A = r6;
        AnonymousClass0H1 r5 = new AnonymousClass0H1(r11.A02.A00);
        this.A06 = r5;
        AnonymousClass0H1 r4 = new AnonymousClass0H1(r11.A03.A00);
        this.A07 = r4;
        EnumC03790Jd r2 = EnumC03790Jd.A01;
        if (r3 == r2) {
            this.A04 = new AnonymousClass0H1(r11.A00.A00);
            r0 = new AnonymousClass0H1(r11.A01.A00);
        } else {
            r0 = null;
            this.A04 = null;
        }
        this.A05 = r0;
        r12.A03(r8);
        r12.A03(A88);
        r12.A03(r6);
        r12.A03(r5);
        r12.A03(r4);
        if (r3 == r2) {
            r12.A03(this.A04);
            r12.A03(this.A05);
        }
        r8.A07.add(this);
        A88.A07.add(this);
        r6.A07.add(this);
        r5.A07.add(this);
        r4.A07.add(this);
        if (r3 == r2) {
            this.A04.A07.add(this);
            this.A05.A07.add(this);
        }
    }

    @Override // X.AbstractC12480hz
    public void A5q(AnonymousClass0SF r2, Object obj) {
        AnonymousClass0QR r0;
        if (obj == AbstractC12810iX.A0C) {
            r0 = this.A08;
        } else if (obj == AbstractC12810iX.A0D) {
            r0 = this.A0A;
        } else if (obj == AbstractC12810iX.A02) {
            r0 = this.A09;
        } else if (obj != AbstractC12810iX.A08 || (r0 = this.A04) == null) {
            if (obj == AbstractC12810iX.A0A) {
                r0 = this.A06;
            } else if (obj != AbstractC12810iX.A09 || (r0 = this.A05) == null) {
                if (obj == AbstractC12810iX.A0B) {
                    r0 = this.A07;
                } else {
                    return;
                }
            }
        }
        r0.A08(r2);
    }

    @Override // X.AbstractC12850if
    public Path AF0() {
        double floatValue;
        float f;
        float f2;
        float cos;
        float sin;
        double d;
        float f3;
        float f4;
        double floatValue2;
        boolean z = this.A01;
        Path path = this.A02;
        if (!z) {
            path.reset();
            if (!this.A0D) {
                switch (this.A0B.ordinal()) {
                    case 0:
                        float floatValue3 = ((Number) this.A08.A03()).floatValue();
                        AnonymousClass0QR r0 = this.A0A;
                        if (r0 == null) {
                            floatValue = 0.0d;
                        } else {
                            floatValue = (double) ((Number) r0.A03()).floatValue();
                        }
                        double radians = Math.toRadians(floatValue - 90.0d);
                        double d2 = (double) floatValue3;
                        float f5 = (float) (6.283185307179586d / d2);
                        float f6 = f5 / 2.0f;
                        float f7 = floatValue3 - ((float) ((int) floatValue3));
                        if (f7 != 0.0f) {
                            radians += (double) ((1.0f - f7) * f6);
                        }
                        float floatValue4 = ((Number) this.A06.A03()).floatValue();
                        float floatValue5 = ((Number) this.A04.A03()).floatValue();
                        AnonymousClass0QR r02 = this.A05;
                        if (r02 != null) {
                            f = ((Number) r02.A03()).floatValue() / 100.0f;
                        } else {
                            f = 0.0f;
                        }
                        AnonymousClass0QR r03 = this.A07;
                        if (r03 != null) {
                            f2 = ((Number) r03.A03()).floatValue() / 100.0f;
                        } else {
                            f2 = 0.0f;
                        }
                        if (f7 != 0.0f) {
                            f3 = ((floatValue4 - floatValue5) * f7) + floatValue5;
                            double d3 = (double) f3;
                            cos = (float) (d3 * Math.cos(radians));
                            sin = (float) (d3 * Math.sin(radians));
                            path.moveTo(cos, sin);
                            d = radians + ((double) ((f5 * f7) / 2.0f));
                        } else {
                            double d4 = (double) floatValue4;
                            cos = (float) (d4 * Math.cos(radians));
                            sin = (float) (d4 * Math.sin(radians));
                            path.moveTo(cos, sin);
                            d = radians + ((double) f6);
                            f3 = 0.0f;
                        }
                        double ceil = Math.ceil(d2) * 2.0d;
                        int i = 0;
                        boolean z2 = false;
                        while (true) {
                            double d5 = (double) i;
                            if (d5 >= ceil) {
                                PointF pointF = (PointF) this.A09.A03();
                                path.offset(pointF.x, pointF.y);
                                path.close();
                                path.close();
                                this.A00.A00(path);
                                break;
                            } else {
                                float f8 = floatValue5;
                                if (z2) {
                                    f8 = floatValue4;
                                }
                                if (f3 == 0.0f || d5 != ceil - 2.0d) {
                                    f4 = f6;
                                } else {
                                    f4 = (f5 * f7) / 2.0f;
                                }
                                if (f3 != 0.0f && d5 == ceil - 1.0d) {
                                    f8 = f3;
                                }
                                double d6 = (double) f8;
                                float cos2 = (float) (d6 * Math.cos(d));
                                float sin2 = (float) (d6 * Math.sin(d));
                                if (f == 0.0f && f2 == 0.0f) {
                                    path.lineTo(cos2, sin2);
                                } else {
                                    double atan2 = (double) ((float) (Math.atan2((double) sin, (double) cos) - 1.5707963267948966d));
                                    float cos3 = (float) Math.cos(atan2);
                                    float sin3 = (float) Math.sin(atan2);
                                    double atan22 = (double) ((float) (Math.atan2((double) sin2, (double) cos2) - 1.5707963267948966d));
                                    float cos4 = (float) Math.cos(atan22);
                                    float sin4 = (float) Math.sin(atan22);
                                    float f9 = f2;
                                    float f10 = f;
                                    float f11 = floatValue4;
                                    float f12 = floatValue5;
                                    if (z2) {
                                        f9 = f;
                                        f10 = f2;
                                        f11 = f12;
                                        f12 = floatValue4;
                                    }
                                    float f13 = f11 * f9 * 0.47829f;
                                    float f14 = cos3 * f13;
                                    float f15 = f13 * sin3;
                                    float f16 = f12 * f10 * 0.47829f;
                                    float f17 = cos4 * f16;
                                    float f18 = f16 * sin4;
                                    if (f7 != 0.0f) {
                                        if (i == 0) {
                                            f14 *= f7;
                                            f15 *= f7;
                                        } else if (d5 == ceil - 1.0d) {
                                            f17 *= f7;
                                            f18 *= f7;
                                        }
                                    }
                                    path.cubicTo(cos - f14, sin - f15, cos2 + f17, sin2 + f18, cos2, sin2);
                                }
                                d += (double) f4;
                                z2 = !z2;
                                i++;
                                sin = sin2;
                                cos = cos2;
                            }
                        }
                        break;
                    case 1:
                        int floor = (int) Math.floor((double) ((Number) this.A08.A03()).floatValue());
                        AnonymousClass0QR r04 = this.A0A;
                        if (r04 == null) {
                            floatValue2 = 0.0d;
                        } else {
                            floatValue2 = (double) ((Number) r04.A03()).floatValue();
                        }
                        double radians2 = Math.toRadians(floatValue2 - 90.0d);
                        double d7 = (double) floor;
                        float floatValue6 = ((Number) this.A07.A03()).floatValue() / 100.0f;
                        float floatValue7 = ((Number) this.A06.A03()).floatValue();
                        double d8 = (double) floatValue7;
                        float cos5 = (float) (Math.cos(radians2) * d8);
                        float sin5 = (float) (Math.sin(radians2) * d8);
                        path.moveTo(cos5, sin5);
                        double d9 = (double) ((float) (6.283185307179586d / d7));
                        double d10 = radians2 + d9;
                        double ceil2 = Math.ceil(d7);
                        int i2 = 0;
                        while (((double) i2) < ceil2) {
                            float cos6 = (float) (Math.cos(d10) * d8);
                            float sin6 = (float) (d8 * Math.sin(d10));
                            if (floatValue6 != 0.0f) {
                                double atan23 = (double) ((float) (Math.atan2((double) sin5, (double) cos5) - 1.5707963267948966d));
                                float cos7 = (float) Math.cos(atan23);
                                float sin7 = (float) Math.sin(atan23);
                                double atan24 = (double) ((float) (Math.atan2((double) sin6, (double) cos6) - 1.5707963267948966d));
                                float f19 = floatValue7 * floatValue6 * 0.25f;
                                path.cubicTo(cos5 - (cos7 * f19), sin5 - (sin7 * f19), cos6 + (((float) Math.cos(atan24)) * f19), sin6 + (f19 * ((float) Math.sin(atan24))), cos6, sin6);
                            } else {
                                path.lineTo(cos6, sin6);
                            }
                            d10 += d9;
                            i2++;
                            sin5 = sin6;
                            cos5 = cos6;
                        }
                        PointF pointF = (PointF) this.A09.A03();
                        path.offset(pointF.x, pointF.y);
                        path.close();
                        path.close();
                        this.A00.A00(path);
                        break;
                    default:
                        path.close();
                        this.A00.A00(path);
                        break;
                }
            }
            this.A01 = true;
        }
        return path;
    }

    @Override // X.AbstractC12030hG
    public void AYB() {
        this.A01 = false;
        this.A03.invalidateSelf();
    }

    @Override // X.AbstractC12480hz
    public void Aan(C06430To r1, C06430To r2, List list, int i) {
        AnonymousClass0U0.A01(this, r1, r2, list, i);
    }

    @Override // X.AbstractC12470hy
    public void Aby(List list, List list2) {
        for (int i = 0; i < list.size(); i++) {
            AbstractC12470hy r2 = (AbstractC12470hy) list.get(i);
            if (r2 instanceof C07950aL) {
                C07950aL r22 = (C07950aL) r2;
                if (r22.A03 == AnonymousClass0J5.SIMULTANEOUSLY) {
                    this.A00.A00.add(r22);
                    r22.A05.add(this);
                }
            }
        }
    }

    @Override // X.AbstractC12470hy
    public String getName() {
        return this.A0C;
    }
}
