package X;

import android.app.Activity;
import android.graphics.Bitmap;
import android.print.PrintManager;
import com.whatsapp.util.Log;

/* renamed from: X.1Bi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C25921Bi {
    public final C14330lG A00;
    public final AbstractC14440lR A01;

    public C25921Bi(C14330lG r1, AbstractC14440lR r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public static void A00(Activity activity, Bitmap bitmap, String str) {
        PrintManager A00 = AnonymousClass01d.A00(activity);
        if (A00 == null) {
            Log.e("PAY: payments-display-qr/print/no-print-manager");
        } else {
            A00.print(str, new C52122aH(activity, bitmap), null);
        }
    }
}
