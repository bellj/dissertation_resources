package X;

import com.facebook.profilo.core.ProvidersRegistry;
import com.facebook.profilo.provider.atrace.Atrace;

/* renamed from: X.1Sc  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1Sc extends AnonymousClass1SY {
    public static final int A00 = ProvidersRegistry.A00.A02("atrace");

    public AnonymousClass1Sc() {
        super("profilo_atrace");
    }

    @Override // X.AnonymousClass1SY
    public void disable() {
        Atrace.restoreSystrace(A00());
    }

    @Override // X.AnonymousClass1SY
    public void enable() {
        Atrace.enableSystrace(A00());
    }

    @Override // X.AnonymousClass1SY
    public int getSupportedProviders() {
        return A00;
    }

    @Override // X.AnonymousClass1SY
    public int getTracingProviders() {
        if (Atrace.isEnabled()) {
            return A00;
        }
        return 0;
    }
}
