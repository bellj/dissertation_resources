package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.1Jl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C27841Jl extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C27841Jl A05;
    public static volatile AnonymousClass255 A06;
    public int A00;
    public int A01;
    public AbstractC27881Jp A02;
    public AbstractC27881Jp A03;
    public C27831Jk A04;

    static {
        C27841Jl r0 = new C27841Jl();
        A05 = r0;
        r0.A0W();
    }

    public C27841Jl() {
        AbstractC27881Jp r0 = AbstractC27881Jp.A01;
        this.A02 = r0;
        this.A03 = r0;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A09(this.A02, 1);
        }
        if ((i3 & 2) == 2) {
            C27831Jk r0 = this.A04;
            if (r0 == null) {
                r0 = C27831Jk.A0R;
            }
            i2 += CodedOutputStream.A0A(r0, 2);
        }
        int i4 = this.A00;
        if ((i4 & 4) == 4) {
            i2 += CodedOutputStream.A09(this.A03, 3);
        }
        if ((i4 & 8) == 8) {
            i2 += CodedOutputStream.A03(4, this.A01);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0K(this.A02, 1);
        }
        if ((this.A00 & 2) == 2) {
            C27831Jk r0 = this.A04;
            if (r0 == null) {
                r0 = C27831Jk.A0R;
            }
            codedOutputStream.A0L(r0, 2);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0K(this.A03, 3);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0E(4, this.A01);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
