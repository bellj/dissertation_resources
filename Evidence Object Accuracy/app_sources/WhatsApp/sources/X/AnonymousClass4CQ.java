package X;

/* renamed from: X.4CQ  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4CQ extends IllegalStateException {
    public final int currentCapacity;
    public final int requiredCapacity;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass4CQ(int r3, int r4) {
        /*
            r2 = this;
            java.lang.String r0 = "Buffer too small ("
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            r1.append(r3)
            java.lang.String r0 = " < "
            r1.append(r0)
            r1.append(r4)
            java.lang.String r0 = ")"
            java.lang.String r0 = X.C12960it.A0d(r0, r1)
            r2.<init>(r0)
            r2.currentCapacity = r3
            r2.requiredCapacity = r4
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass4CQ.<init>(int, int):void");
    }
}
