package X;

import androidx.work.impl.WorkDatabase;
import java.util.Set;

/* renamed from: X.0dn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09960dn implements Runnable {
    public final /* synthetic */ WorkDatabase A00;
    public final /* synthetic */ C07630Zn A01;
    public final /* synthetic */ String A02;

    public RunnableC09960dn(WorkDatabase workDatabase, C07630Zn r2, String str) {
        this.A01 = r2;
        this.A00 = workDatabase;
        this.A02 = str;
    }

    @Override // java.lang.Runnable
    public void run() {
        AbstractC12700iM A0B = this.A00.A0B();
        String str = this.A02;
        C004401z AHn = A0B.AHn(str);
        if (AHn != null && (!C004101u.A08.equals(AHn.A09))) {
            C07630Zn r3 = this.A01;
            synchronized (r3.A06) {
                r3.A08.put(str, AHn);
                Set set = r3.A09;
                set.add(AHn);
                r3.A04.A01(set);
            }
        }
    }
}
