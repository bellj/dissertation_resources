package X;

import android.content.Context;
import android.os.Looper;

/* renamed from: X.3o6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77933o6 extends AbstractC77963o9 {
    @Override // X.AbstractC95064d1, X.AbstractC72443eb
    public final int AET() {
        return 17895000;
    }

    public C77933o6(Context context, Looper looper, AbstractC14990mN r10, AbstractC15010mP r11, AnonymousClass3BW r12) {
        super(context, looper, r10, r11, r12, 258);
    }
}
