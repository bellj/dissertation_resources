package X;

import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.3Xk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68963Xk implements AbstractC28461Nh {
    public final /* synthetic */ C15450nH A00;
    public final /* synthetic */ C91644Sn A01;
    public final /* synthetic */ C455121y A02;
    public final /* synthetic */ C28481Nj A03;
    public final /* synthetic */ C455221z A04;

    @Override // X.AbstractC28461Nh
    public /* synthetic */ void AOt(long j) {
    }

    public C68963Xk(C15450nH r1, C91644Sn r2, C455121y r3, C28481Nj r4, C455221z r5) {
        this.A02 = r3;
        this.A01 = r2;
        this.A04 = r5;
        this.A03 = r4;
        this.A00 = r1;
    }

    @Override // X.AbstractC28461Nh
    public void AOZ(String str) {
    }

    @Override // X.AbstractC28461Nh
    public void APq(String str) {
        Log.e(C12960it.A0d(str, C12960it.A0k("mediaupload/finalizeupload/error = ")));
    }

    @Override // X.AbstractC28461Nh
    public void AV9(String str, Map map) {
        String str2 = null;
        try {
            JSONObject A05 = C13000ix.A05(str);
            str2 = A05.optString("url");
            this.A01.A02 = A05.optString("direct_path");
        } catch (JSONException e) {
            Log.e("mediaupload/jsonexception", e);
        }
        if (TextUtils.isEmpty(str2)) {
            C455221z r0 = this.A04;
            C28481Nj r2 = this.A03;
            str2 = new C37781mz(((AbstractC37791n0) r0).A02, ((AbstractC37791n0) r0).A01, ((AbstractC37791n0) r0).A00, null, null).AAJ(this.A00, r2, true);
        }
        this.A01.A03 = str2;
    }
}
