package X;

import X.AbstractC001200n;
import X.AbstractC16350or;
import X.AnonymousClass074;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.renderscript.RenderScript;
import android.util.SparseBooleanArray;
import android.view.View;
import com.whatsapp.Conversation;
import com.whatsapp.R;
import com.whatsapp.contact.picker.ContactPickerFragment;
import com.whatsapp.dialogs.ProgressDialogFragment;
import com.whatsapp.group.GroupChatInfo;
import com.whatsapp.group.GroupProfileEmojiEditor;
import com.whatsapp.profile.WebImagePicker;
import com.whatsapp.registration.RegisterPhone;
import com.whatsapp.registration.VerifyPhoneNumber;
import com.whatsapp.registration.VerifyTwoFactorAuth;
import com.whatsapp.settings.SettingsDataUsageActivity;
import com.whatsapp.settings.chat.wallpaper.WallpaperImagePreview;
import com.whatsapp.stickers.AddThirdPartyStickerPackActivity;
import com.whatsapp.stickers.StickerStorePackPreviewActivity;
import com.whatsapp.util.Log;
import java.io.File;
import java.lang.ref.WeakReference;

/* renamed from: X.0or  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC16350or {
    public AnonymousClass054 A00;
    public AbstractC001200n A01;
    public final AsyncTaskC16360os A02;

    public AbstractC16350or() {
        this.A02 = new AsyncTaskC16360os(this);
    }

    public AbstractC16350or(AbstractC001200n r4) {
        this();
        AnonymousClass009.A01();
        AnonymousClass009.A0F(((C009804x) r4.ADr()).A02 != AnonymousClass05I.DESTROYED);
        this.A01 = r4;
        this.A00 = new AnonymousClass054() { // from class: com.whatsapp.util.WaAsyncTask$$ExternalSyntheticLambda0
            public final /* synthetic */ boolean A01 = true;

            @Override // X.AnonymousClass054
            public final void AWQ(AnonymousClass074 r42, AbstractC001200n r5) {
                AbstractC16350or r2 = AbstractC16350or.this;
                boolean z = this.A01;
                if (r42.equals(AnonymousClass074.ON_DESTROY)) {
                    r2.A03(z);
                }
            }
        };
        r4.ADr().A00(this.A00);
    }

    public final int A00() {
        AsyncTask.Status status = this.A02.getStatus();
        if (status == AsyncTask.Status.PENDING) {
            return 0;
        }
        return status == AsyncTask.Status.RUNNING ? 1 : 2;
    }

    public AbstractC001200n A01(Class cls) {
        if (AbstractC001200n.class.equals(cls)) {
            return this.A01;
        }
        return (AbstractC001200n) cls.cast(this.A01);
    }

    public void A02() {
        AnonymousClass02N r0;
        if (this instanceof C627938p) {
            C627938p r4 = (C627938p) this;
            StickerStorePackPreviewActivity stickerStorePackPreviewActivity = r4.A02;
            C91014Qc r02 = stickerStorePackPreviewActivity.A0L;
            if (r02 != null) {
                int i = r4.A00;
                SparseBooleanArray sparseBooleanArray = r02.A01;
                if (sparseBooleanArray != null) {
                    sparseBooleanArray.put(i, false);
                }
            }
            C54472gm r1 = stickerStorePackPreviewActivity.A0M;
            if (r1 != null) {
                r1.A03(r4.A00);
            }
        } else if (this instanceof C628238s) {
            ((C628238s) this).A04.A0C.AL6(453128091, 2, 4);
        } else if (this instanceof C28521Nt) {
            C28521Nt r12 = (C28521Nt) this;
            r12.A02.clear();
            RenderScript renderScript = r12.A00;
            if (renderScript != null) {
                renderScript.destroy();
            }
        } else if (this instanceof C49342Kj) {
            VerifyTwoFactorAuth verifyTwoFactorAuth = (VerifyTwoFactorAuth) ((C49342Kj) this).A0B.get();
            if (verifyTwoFactorAuth != null) {
                verifyTwoFactorAuth.A08.setEnabled(true);
                verifyTwoFactorAuth.A05.setProgress(100);
            }
        } else if (this instanceof C628638w) {
            AbstractActivityC452520u r2 = (AbstractActivityC452520u) ((C628638w) this).A09.get();
            if (r2 != null) {
                C36021jC.A00(r2, 9);
                r2.A00 = null;
                if (r2 instanceof RegisterPhone) {
                    ((RegisterPhone) r2).A0H = null;
                }
            }
        } else if (this instanceof AnonymousClass393) {
            AnonymousClass393 r13 = (AnonymousClass393) this;
            File A00 = AnonymousClass393.A00(r13.A04.A00, r13);
            if (A00.exists()) {
                A00.delete();
            }
        } else if (this instanceof C627738n) {
            C627738n r03 = (C627738n) this;
            AnonymousClass4V0 r14 = r03.A01;
            AnonymousClass192 r22 = r03.A00;
            r14.A00(r22.A01);
            r22.A09.AcF(r22.A01);
            StringBuilder sb = new StringBuilder("dictionaryloader/prepare/onCancelled/dictionaryAvailable=");
            sb.append(r22.A01);
            Log.i(sb.toString());
        } else if (this instanceof C628038q) {
            ((C628038q) this).A01.AUW();
        } else if (this instanceof C628438u) {
            C628438u r04 = (C628438u) this;
            r04.A02.A04(r04.A00);
        } else if (this instanceof C628138r) {
            ContactPickerFragment contactPickerFragment = (ContactPickerFragment) ((C628138r) this).A04.get();
            if (contactPickerFragment != null && contactPickerFragment.A0c()) {
                Log.i("contactpicker/fetchContactUsingPhoneNumber/canceled");
                contactPickerFragment.A0p = null;
                contactPickerFragment.A0E.setVisibility(8);
                contactPickerFragment.A0D.setVisibility(0);
            }
        } else if (!(this instanceof C628538v)) {
            if (this instanceof AnonymousClass38W) {
                r0 = ((AnonymousClass38W) this).A00;
            } else if (this instanceof AnonymousClass2BN) {
                r0 = ((AnonymousClass2BN) this).A00;
            } else {
                return;
            }
            r0.A01();
        } else {
            ContactPickerFragment contactPickerFragment2 = (ContactPickerFragment) ((C628538v) this).A07.get();
            if (contactPickerFragment2 != null && contactPickerFragment2.A0c()) {
                Log.i("contactpicker/existencecheck/canceled");
                contactPickerFragment2.A0o = null;
                contactPickerFragment2.A0m.AaN();
            }
        }
    }

    public final void A03(boolean z) {
        this.A02.cancel(z);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: BlockProcessor
        jadx.core.utils.exceptions.JadxRuntimeException: CFG modification limit reached, blocks count: 3765
        	at jadx.core.dex.visitors.blocks.BlockProcessor.processBlocksTree(BlockProcessor.java:66)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.visit(BlockProcessor.java:44)
        */
    public java.lang.Object A05(java.lang.Object... r40) {
        /*
        // Method dump skipped, instructions count: 19476
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC16350or.A05(java.lang.Object[]):java.lang.Object");
    }

    public void A06() {
        int i;
        ActivityC13810kN r1;
        WeakReference weakReference;
        WeakReference weakReference2;
        WeakReference weakReference3;
        View view;
        View view2;
        ActivityC13810kN r12;
        if (this instanceof C627038g) {
            weakReference = ((C627038g) this).A08;
        } else if (!(this instanceof C626438a)) {
            if (this instanceof C39331pg) {
                weakReference2 = ((C39331pg) this).A05;
            } else if (this instanceof AnonymousClass38Y) {
                AnonymousClass38Y r3 = (AnonymousClass38Y) this;
                Context context = (Context) r3.A06.get();
                if (context != null) {
                    if (r3.A00 == null) {
                        ProgressDialog progressDialog = new ProgressDialog(context);
                        r3.A00 = progressDialog;
                        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() { // from class: X.4fB
                            @Override // android.content.DialogInterface.OnCancelListener
                            public final void onCancel(DialogInterface dialogInterface) {
                                AnonymousClass38Y.this.A03(true);
                            }
                        });
                        r3.A00.setCanceledOnTouchOutside(false);
                    }
                    if (!r3.A00.isShowing()) {
                        r3.A00.setMessage(context.getString(R.string.help_loading_progress_label));
                        r3.A00.setIndeterminate(true);
                        r3.A00.show();
                        return;
                    }
                    return;
                }
                return;
            } else if (this instanceof C627338j) {
                C627338j r2 = (C627338j) this;
                C91694Ss r0 = r2.A08;
                if (r0 != null) {
                    ActivityC13810kN r13 = r0.A00;
                    if (!r13.isFinishing()) {
                        r13.A2C(R.string.register_preparing);
                    }
                }
                C22760zb r32 = r2.A03.A00;
                StringBuilder sb = new StringBuilder("contactsupporttask/priv/last=");
                SharedPreferences sharedPreferences = r32.A0H.A00;
                sb.append(C41701tx.A01(sharedPreferences.getInt("privacy_last_seen", 0), "last"));
                Log.i(sb.toString());
                StringBuilder sb2 = new StringBuilder("contactsupporttask/priv/pic=");
                sb2.append(C41701tx.A01(sharedPreferences.getInt("privacy_profile_photo", 0), "profile"));
                Log.i(sb2.toString());
                StringBuilder sb3 = new StringBuilder("contactsupporttask/priv/status=");
                sb3.append(C41701tx.A01(sharedPreferences.getInt("privacy_status", 0), "status"));
                Log.i(sb3.toString());
                StringBuilder sb4 = new StringBuilder("contactsupporttask/priv/readreceipts=");
                sb4.append(sharedPreferences.getBoolean("read_receipts_enabled", true));
                Log.i(sb4.toString());
                C15860o1 r33 = r32.A0W;
                try {
                    C16310on A01 = r33.A03().get();
                    Cursor A08 = A01.A03.A08("settings", null, null, null, C33201dc.A00, null);
                    if (A08 != null) {
                        while (A08.moveToNext()) {
                            try {
                                StringBuilder sb5 = new StringBuilder();
                                sb5.append("contactsupporttask");
                                sb5.append("/settings/");
                                sb5.append(AbstractC14640lm.A01(A08.getString(0)));
                                sb5.append(" muteEndTime:");
                                sb5.append(A08.getLong(1));
                                sb5.append(" showNotificationsWhenMuted:");
                                sb5.append(A08.getInt(2));
                                sb5.append(" useCustomNotifications:");
                                sb5.append(A08.getInt(3));
                                sb5.append(" messageTone:");
                                sb5.append(A08.getString(4));
                                sb5.append(" messageVibrate:");
                                sb5.append(A08.getString(5));
                                sb5.append(" messagePopup:");
                                sb5.append(A08.getString(6));
                                sb5.append(" messageLight:");
                                sb5.append(A08.getString(7));
                                sb5.append(" callTone:");
                                sb5.append(A08.getString(8));
                                sb5.append(" callVibrate:");
                                sb5.append(A08.getString(9));
                                sb5.append(" statusMuted:");
                                sb5.append(A08.getString(10));
                                sb5.append(" pinned:");
                                sb5.append(A08.getString(11));
                                sb5.append(" pinned_time:");
                                sb5.append(A08.getLong(12));
                                sb5.append(" lowPriorityNotifications:");
                                sb5.append(A08.getInt(13));
                                sb5.append(" mediaVisibility:");
                                sb5.append(A08.getInt(14));
                                sb5.append(" reactions:");
                                sb5.append(A08.getInt(15));
                                Log.i(sb5.toString());
                            } catch (Throwable th) {
                                try {
                                    A08.close();
                                } catch (Throwable unused) {
                                }
                                throw th;
                            }
                        }
                        A08.close();
                    }
                    A01.close();
                } catch (Exception e) {
                    StringBuilder sb6 = new StringBuilder();
                    sb6.append("contactsupporttask");
                    sb6.append("/settings/exception");
                    Log.e(sb6.toString(), e);
                }
                if (C33261di.A00) {
                    for (Object obj : C33301dm.A01(r33.A01.A04())) {
                        StringBuilder sb7 = new StringBuilder();
                        sb7.append("contactsupporttask");
                        sb7.append("/setting/channel:");
                        sb7.append(obj.toString());
                        Log.i(sb7.toString());
                    }
                    return;
                }
                return;
            } else if (this instanceof C627938p) {
                C627938p r14 = (C627938p) this;
                StickerStorePackPreviewActivity stickerStorePackPreviewActivity = r14.A02;
                C91014Qc r02 = stickerStorePackPreviewActivity.A0L;
                int i2 = r14.A00;
                SparseBooleanArray sparseBooleanArray = r02.A01;
                if (sparseBooleanArray != null) {
                    sparseBooleanArray.put(i2, true);
                }
                stickerStorePackPreviewActivity.A0M.A03(i2);
                return;
            } else if (this instanceof AnonymousClass38X) {
                AnonymousClass38X r4 = (AnonymousClass38X) this;
                ActivityC000900k r34 = (ActivityC000900k) r4.A06.get();
                if (r34 != null) {
                    AddThirdPartyStickerPackActivity.AddStickerPackDialogFragment A00 = AddThirdPartyStickerPackActivity.AddStickerPackDialogFragment.A00(r4.A04, r4.A03, r4.A05);
                    r4.A00 = A00;
                    A00.A1F(r34.A0V(), "add");
                    return;
                }
                return;
            } else if (this instanceof AnonymousClass38V) {
                AnonymousClass38V r03 = (AnonymousClass38V) this;
                Resources resources = r03.A03;
                if (resources != null) {
                    WallpaperImagePreview wallpaperImagePreview = r03.A05;
                    wallpaperImagePreview.setImageDrawable(resources.getDrawable(r03.A00));
                    wallpaperImagePreview.setVisibility(0);
                    return;
                }
                return;
            } else if (!(this instanceof AnonymousClass38G)) {
                if (this instanceof AnonymousClass2GQ) {
                    weakReference3 = ((AnonymousClass2GQ) this).A02;
                } else if (this instanceof C48322Fo) {
                    weakReference3 = ((C48322Fo) this).A02;
                } else if (this instanceof AnonymousClass38M) {
                    AnonymousClass38M r15 = (AnonymousClass38M) this;
                    ActivityC13810kN r04 = (ActivityC13810kN) r15.A03.get();
                    if (!(r04 == null || r04.AJN())) {
                        r15.A02.Ady(0, R.string.register_wait_message);
                        return;
                    }
                    return;
                } else if (this instanceof C49342Kj) {
                    C49342Kj r35 = (C49342Kj) this;
                    VerifyTwoFactorAuth verifyTwoFactorAuth = (VerifyTwoFactorAuth) r35.A0B.get();
                    if (verifyTwoFactorAuth != null) {
                        Log.i("verifytwofactorauth/verifycodetask/pre");
                        verifyTwoFactorAuth.A08.setEnabled(false);
                        verifyTwoFactorAuth.A05.setProgress(0);
                        C36021jC.A01(verifyTwoFactorAuth, r35.A01);
                        return;
                    }
                    return;
                } else if (this instanceof C626938f) {
                    C626938f r22 = (C626938f) this;
                    StringBuilder sb8 = new StringBuilder("verifycode/");
                    sb8.append(r22.A08);
                    Log.i(sb8.toString());
                    AbstractC44441yy r23 = (AbstractC44441yy) r22.A0A.get();
                    if (r23 != null) {
                        ActivityC13790kL r24 = (ActivityC13790kL) r23;
                        if (r24.A0B.A00() != 8) {
                            C36021jC.A01(r24, 23);
                            return;
                        }
                        return;
                    }
                    return;
                } else if (this instanceof C627438k) {
                    C627438k r36 = (C627438k) this;
                    StringBuilder sb9 = new StringBuilder("requestcode/");
                    String str = r36.A0C;
                    sb9.append(str);
                    Log.i(sb9.toString());
                    AbstractC44431yx r25 = (AbstractC44431yx) r36.A0F.get();
                    if (r25 != null) {
                        boolean z = r36.A0G;
                        VerifyPhoneNumber verifyPhoneNumber = (VerifyPhoneNumber) r25;
                        if (str.equals("sms")) {
                            verifyPhoneNumber.A3B(0);
                            verifyPhoneNumber.A0e.A00();
                            verifyPhoneNumber.A0d.A00();
                            if (verifyPhoneNumber.A1A) {
                                verifyPhoneNumber.A19 = true;
                                verifyPhoneNumber.registerReceiver(verifyPhoneNumber.A0n, new IntentFilter("com.google.android.gms.auth.api.phone.SMS_RETRIEVED"));
                            } else {
                                IntentFilter intentFilter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
                                intentFilter.setPriority(Integer.MAX_VALUE);
                                verifyPhoneNumber.registerReceiver(verifyPhoneNumber.A0j, intentFilter);
                                verifyPhoneNumber.A16 = true;
                            }
                            verifyPhoneNumber.A0m.A04();
                            if (z) {
                                C36021jC.A01(verifyPhoneNumber, 34);
                            }
                        } else if (str.equals("voice") || str.equals("flash")) {
                            if (str.equals("flash")) {
                                verifyPhoneNumber.A3B(16);
                            }
                            if (z) {
                                verifyPhoneNumber.A38(R.string.register_voice_request_message);
                            }
                        }
                        verifyPhoneNumber.A0g.A01();
                        return;
                    }
                    return;
                } else if (this instanceof AnonymousClass38U) {
                    ((AnonymousClass38U) this).A01.A06(0, R.string.loading_spinner);
                    return;
                } else if (this instanceof C628638w) {
                    AbstractActivityC452520u r05 = (AbstractActivityC452520u) ((C628638w) this).A09.get();
                    if (r05 != null) {
                        r05.A2e();
                        return;
                    }
                    return;
                } else if (this instanceof C627838o) {
                    C627838o r42 = (C627838o) this;
                    WebImagePicker webImagePicker = r42.A02;
                    ProgressDialog progressDialog2 = new ProgressDialog(webImagePicker);
                    r42.A00 = progressDialog2;
                    progressDialog2.setProgressStyle(1);
                    r42.A00.setMessage(webImagePicker.getString(R.string.photo_loading));
                    r42.A00.setCancelable(true);
                    r42.A00.setOnCancelListener(new DialogInterface.OnCancelListener() { // from class: X.3K5
                        @Override // android.content.DialogInterface.OnCancelListener
                        public final void onCancel(DialogInterface dialogInterface) {
                            C627838o r37 = C627838o.this;
                            ProgressDialog progressDialog3 = r37.A00;
                            if (progressDialog3 != null) {
                                progressDialog3.dismiss();
                            }
                            r37.A00 = null;
                            r37.A03(true);
                            WebImagePicker webImagePicker2 = r37.A02;
                            if (webImagePicker2.A0B == r37) {
                                webImagePicker2.A0B = null;
                            }
                        }
                    });
                    r42.A00.show();
                    return;
                } else if (this instanceof C621935i) {
                    ((C621935i) this).A00.A0B(new C90424Nv(null));
                    return;
                } else if (this instanceof AnonymousClass24M) {
                    AnonymousClass24M r37 = (AnonymousClass24M) this;
                    C244615p r26 = r37.A08;
                    if (r26 != null) {
                        try {
                            r26.A05(r37, "map-download", 0.0f, 3, 1000, 1000);
                        } catch (IllegalArgumentException e2) {
                            Log.w("MapDownload/registerListener/GPS error ", e2);
                        }
                    }
                    AnonymousClass1XP r27 = r37.A0I;
                    r27.A02 = 1;
                    r37.A0E.A08(r27, -1);
                    return;
                } else if (this instanceof C627638m) {
                    C627638m r38 = (C627638m) this;
                    Context context2 = (Context) r38.A0K.get();
                    if (context2 != null) {
                        if (r38.A01 == null) {
                            ProgressDialog progressDialog3 = new ProgressDialog(context2);
                            r38.A01 = progressDialog3;
                            progressDialog3.setOnCancelListener(new DialogInterface.OnCancelListener() { // from class: X.4f6
                                @Override // android.content.DialogInterface.OnCancelListener
                                public final void onCancel(DialogInterface dialogInterface) {
                                    C627638m.this.A03(true);
                                }
                            });
                            r38.A01.setCancelable(false);
                        }
                        if (!r38.A01.isShowing()) {
                            ProgressDialog progressDialog4 = r38.A01;
                            boolean A002 = r38.A0C.A00();
                            int i3 = R.string.searching;
                            if (A002) {
                                i3 = R.string.contact_us_faq_search_dialog_message;
                            }
                            progressDialog4.setMessage(context2.getString(i3));
                            r38.A01.setIndeterminate(true);
                            r38.A01.show();
                            return;
                        }
                        return;
                    }
                    return;
                } else if (this instanceof AnonymousClass38Q) {
                    AnonymousClass4V2 r06 = (AnonymousClass4V2) ((AnonymousClass38Q) this).A03.get();
                    if (r06 != null) {
                        r06.A00.A06.setVisibility(0);
                        return;
                    }
                    return;
                } else if (this instanceof AnonymousClass38T) {
                    ActivityC13810kN r28 = (ActivityC13810kN) ((AnonymousClass38T) this).A00.get();
                    if (!(r28 == null || r28.AJN())) {
                        r28.Ady(0, R.string.searching_image);
                        return;
                    }
                    return;
                } else if (this instanceof C626638c) {
                    ActivityC13810kN r16 = (ActivityC13810kN) ((C626638c) this).A08.get();
                    if (r16 != null) {
                        r16.A2C(R.string.loading_spinner);
                        return;
                    }
                    return;
                } else if (this instanceof C36371jm) {
                    r1 = (ActivityC13810kN) ((C36371jm) this).A06.get();
                    if (!(r1 == null || r1.isFinishing())) {
                        i = R.string.updating_group_admins;
                        r1.Ady(i, R.string.register_wait_message);
                    }
                    return;
                } else if (this instanceof AnonymousClass38P) {
                    AnonymousClass38P r29 = (AnonymousClass38P) this;
                    AbstractC001200n A012 = r29.A01(GroupProfileEmojiEditor.class);
                    AnonymousClass3PD r07 = new AnonymousClass024() { // from class: X.3PD
                        @Override // X.AnonymousClass024
                        public final void accept(Object obj2) {
                            Bitmap bitmap;
                            AnonymousClass38P r210 = AnonymousClass38P.this;
                            GroupProfileEmojiEditor groupProfileEmojiEditor = (GroupProfileEmojiEditor) obj2;
                            r210.A00 = C12960it.A05(groupProfileEmojiEditor.A06.A00.A01());
                            r210.A02 = groupProfileEmojiEditor.A00;
                            r210.A04 = (Uri) groupProfileEmojiEditor.getIntent().getParcelableExtra("emojiEditorImageResult");
                            r210.A01 = ((ActivityC13810kN) groupProfileEmojiEditor).A08.A0C();
                            try {
                                bitmap = Bitmap.createBitmap(640, 640, Bitmap.Config.ARGB_8888);
                            } catch (OutOfMemoryError unused2) {
                                bitmap = null;
                            }
                            r210.A03 = bitmap;
                        }
                    };
                    if (A012 != null) {
                        r07.accept(A012);
                        return;
                    }
                    return;
                } else if (this instanceof AnonymousClass32Z) {
                    GroupChatInfo groupChatInfo = (GroupChatInfo) ((AnonymousClass32Z) this).A00.get();
                    if (groupChatInfo != null) {
                        groupChatInfo.A02.setVisibility(0);
                        groupChatInfo.A1P.setVisibility(8);
                        return;
                    }
                    return;
                } else if (this instanceof C45011zv) {
                    C45011zv r210 = (C45011zv) this;
                    for (AbstractC45001zu r08 : r210.A05.A01()) {
                        r08.ANC();
                    }
                    r210.A02.A00(1, true);
                    return;
                } else if (this instanceof AnonymousClass392) {
                    AnonymousClass392 r39 = (AnonymousClass392) this;
                    AnonymousClass01F r211 = (AnonymousClass01F) r39.A05.get();
                    if (r211 != null) {
                        ProgressDialogFragment A003 = ProgressDialogFragment.A00(R.string.processing, R.string.register_wait_message);
                        r39.A01 = A003;
                        A003.A1F(r211, "count_progress");
                        return;
                    }
                    return;
                } else if (this instanceof C627138h) {
                    WeakReference weakReference4 = ((C627138h) this).A09;
                    if (weakReference4.get() != null) {
                        ((AbstractC13860kS) weakReference4.get()).Ady(0, R.string.register_wait_message);
                        return;
                    }
                    return;
                } else if (this instanceof C628038q) {
                    ((C628038q) this).A01.AXH();
                    return;
                } else if (this instanceof AnonymousClass38S) {
                    ((AnonymousClass38S) this).A00.A06(0, R.string.checking_phone_number_progress_spinner);
                    return;
                } else if (this instanceof C628438u) {
                    C628438u r310 = (C628438u) this;
                    ActivityC13810kN r212 = (ActivityC13810kN) r310.A05.get();
                    if (r212 != null) {
                        r212.Ady(0, R.string.loading_biz_profile);
                    }
                    r310.A02.A03(r310.A00);
                    return;
                } else if (this instanceof C626738d) {
                    C626738d r213 = (C626738d) this;
                    AbstractC14050kl r5 = (AbstractC14050kl) r213.A0A.get();
                    r213.A01 = r213.A05.A04();
                    if (r5 != null) {
                        boolean z2 = r213.A0B;
                        Conversation conversation = (Conversation) r5;
                        conversation.A3q = new C28181Ma("conversation/search/searchTask");
                        if (conversation.A2U.A04() != 5) {
                            conversation.A2C(R.string.searching);
                            return;
                        } else if (conversation.A0L != null && conversation.A0J != null && (view = conversation.A0M) != null && (view2 = conversation.A0K) != null) {
                            if (z2) {
                                view.setVisibility(0);
                            } else {
                                view2.setVisibility(0);
                            }
                            conversation.A0L.setEnabled(false);
                            conversation.A0J.setEnabled(false);
                            return;
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                } else if (this instanceof AnonymousClass38Z) {
                    weakReference = ((AnonymousClass38Z) this).A06;
                } else if (this instanceof C628138r) {
                    ContactPickerFragment contactPickerFragment = (ContactPickerFragment) ((C628138r) this).A04.get();
                    if (contactPickerFragment != null && contactPickerFragment.A0c()) {
                        Log.i("contactpicker/fetchcontactusingphonenumber/started");
                        contactPickerFragment.A0E.setVisibility(0);
                        contactPickerFragment.A0D.setVisibility(8);
                        return;
                    }
                    return;
                } else if (this instanceof C628538v) {
                    ContactPickerFragment contactPickerFragment2 = (ContactPickerFragment) ((C628538v) this).A07.get();
                    if (contactPickerFragment2 != null && contactPickerFragment2.A0c()) {
                        Log.i("contactpicker/existencecheck/started");
                        contactPickerFragment2.A0m.Ady(0, R.string.searching);
                        return;
                    }
                    return;
                } else if (this instanceof AnonymousClass38O) {
                    WeakReference weakReference5 = ((AnonymousClass38O) this).A03;
                    if (weakReference5.get() != null) {
                        ((ActivityC13810kN) weakReference5.get()).A2C(R.string.register_wait_message);
                        return;
                    }
                    return;
                } else if (this instanceof C471329b) {
                    C471329b r214 = (C471329b) this;
                    for (AnonymousClass29Z r09 : r214.A01.A01()) {
                        r09.ANE();
                    }
                    r214.A03.A00(1, true);
                    return;
                } else if (this instanceof AnonymousClass29F) {
                    AnonymousClass29F r010 = (AnonymousClass29F) this;
                    r010.A0A.schedule(new AnonymousClass29E(r010), C26061Bw.A0L);
                    return;
                } else if (this instanceof C48222Fb) {
                    C36021jC.A01(((C48222Fb) this).A00, 104);
                    return;
                } else if (this instanceof AnonymousClass24R) {
                    weakReference2 = ((AnonymousClass24R) this).A0F;
                } else if (this instanceof C48212Fa) {
                    C36021jC.A01(((C48212Fa) this).A00, 104);
                    return;
                } else if ((this instanceof C627238i) && (r12 = (ActivityC13810kN) ((C627238i) this).A00.get()) != null) {
                    r12.A2C(R.string.register_connecting);
                    return;
                } else {
                    return;
                }
                r1 = (ActivityC13810kN) weakReference3.get();
                if (!(r1 == null || r1.AJN())) {
                    i = 0;
                    r1.Ady(i, R.string.register_wait_message);
                }
                return;
            } else {
                Log.i("settings-data-usage-activity/load storage size task/started");
                SettingsDataUsageActivity settingsDataUsageActivity = ((AnonymousClass38G) this).A01;
                if (settingsDataUsageActivity.A03 == -1) {
                    settingsDataUsageActivity.A0C.setText(R.string.calculating);
                    return;
                }
                return;
            }
            AbstractC13860kS r215 = (AbstractC13860kS) weakReference2.get();
            if (r215 != null) {
                r215.Ady(0, R.string.media_loading);
                return;
            }
            return;
        } else {
            weakReference = ((C626438a) this).A07;
        }
        r1 = (ActivityC13810kN) weakReference.get();
        if (r1 != null) {
            i = R.string.processing;
            r1.Ady(i, R.string.register_wait_message);
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v1082, resolved type: java.util.HashMap */
    /* JADX DEBUG: Multi-variable search result rejected for r1v1086, resolved type: java.util.HashMap */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r4v131, types: [java.lang.Object] */
    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    public void A07(java.lang.Object r21) {
        /*
        // Method dump skipped, instructions count: 13970
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC16350or.A07(java.lang.Object):void");
    }
}
