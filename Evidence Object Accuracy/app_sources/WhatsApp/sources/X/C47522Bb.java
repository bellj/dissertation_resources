package X;

import com.whatsapp.videoplayback.ExoPlaybackControlView;
import com.whatsapp.videoplayback.ExoPlayerErrorFrame;

/* renamed from: X.2Bb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class C47522Bb {
    public final /* synthetic */ AnonymousClass21S A00;

    public /* synthetic */ C47522Bb(AnonymousClass21S r1) {
        this.A00 = r1;
    }

    public final void A00(String str, boolean z, int i) {
        AnonymousClass21S r2 = this.A00;
        boolean z2 = false;
        if (i == 1) {
            r2.A0K(str, z);
        } else if (i == 2) {
            ExoPlaybackControlView exoPlaybackControlView = r2.A0C;
            if (exoPlaybackControlView != null) {
                exoPlaybackControlView.setPlayControlVisibility(0);
            }
            r2.A08();
            r2.A07();
        }
        C47482Aw r3 = r2.A0Y;
        if (i == 1) {
            z2 = true;
        }
        r3.A06 = str;
        AnonymousClass3FK r22 = r3.A03;
        if (r22 != null && r3.A07 != z2) {
            int i2 = r3.A00;
            if (z2) {
                if (i2 == 2) {
                    r22.A01(str);
                }
            } else if (i2 == 2) {
                ExoPlayerErrorFrame exoPlayerErrorFrame = r22.A03;
                exoPlayerErrorFrame.setLoadingViewVisibility(0);
                C12970iu.A1G(exoPlayerErrorFrame.A02);
            }
            r3.A07 = z2;
        }
    }
}
