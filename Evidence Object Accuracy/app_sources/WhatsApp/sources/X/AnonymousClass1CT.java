package X;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import com.whatsapp.usernotice.UserNoticeBottomSheetDialogFragment;
import com.whatsapp.util.Log;
import java.util.Map;

/* renamed from: X.1CT  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1CT {
    public final AnonymousClass12P A00;
    public final C14850m9 A01;
    public final AnonymousClass12O A02;

    public AnonymousClass1CT(AnonymousClass12P r1, C14850m9 r2, AnonymousClass12O r3) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
    }

    public void A00(Context context, String str, Map map) {
        String str2;
        if (!str.equals("open-modal")) {
            if (!str.equals("open-link")) {
                StringBuilder sb = new StringBuilder("UserNoticeLinkActionHandler/handleAction unknown action: ");
                sb.append(str);
                sb.append(" with params: ");
                sb.append(map);
                str2 = sb.toString();
            } else {
                String str3 = (String) map.get("link");
                if (str3 == null) {
                    str2 = "UserNoticeLinkActionHandler/handleOpenLink null url";
                } else {
                    this.A00.A06(context, new Intent("android.intent.action.VIEW", Uri.parse(str3)));
                    return;
                }
            }
            Log.e(str2);
            return;
        }
        A01(context, false);
    }

    public void A01(Context context, boolean z) {
        C43841xg r2;
        String str;
        String str2;
        AbstractC15710nm r1;
        String str3;
        int i;
        AnonymousClass12O r5 = this.A02;
        AnonymousClass12N r4 = r5.A08;
        C43831xf A01 = r4.A01();
        if (A01 != null && (z || (i = A01.A01) == 3 || i == 4)) {
            int i2 = A01.A00;
            C14850m9 r6 = r5.A03;
            if (C43901xo.A00(r6, i2)) {
                StringBuilder sb = new StringBuilder("UserNoticeManager/getModal/green alert disabled, notice: ");
                sb.append(i2);
                str2 = sb.toString();
            } else {
                C43891xl A03 = r5.A07.A03(A01);
                if (A03 != null) {
                    if (z || A01.A01 == 3) {
                        r2 = A03.A04;
                        if (r2 == null) {
                            Log.e("UserNoticeManager/getModal/no content for stage 3");
                            r1 = r5.A00;
                            str3 = "UserNoticeManager/getModal/modal/noContent";
                            r1.AaV(str3, null, true);
                        } else {
                            if (!z) {
                                C43881xk r12 = r2.A00;
                                if (!r5.A0C(r12)) {
                                    str2 = "UserNoticeManager/getModal/modal not shown as per timing";
                                } else {
                                    r5.A0A(r12, C43901xo.A01(r6, A01));
                                    str = "UserNoticeManager/getModal/has modal";
                                    Log.i(str);
                                }
                            }
                            if (r4.A01() != null || !C43901xo.A01(this.A01, r4.A01())) {
                                Bundle bundle = new Bundle();
                                r2.A01(bundle);
                                UserNoticeBottomSheetDialogFragment userNoticeBottomSheetDialogFragment = new UserNoticeBottomSheetDialogFragment();
                                userNoticeBottomSheetDialogFragment.A0U(bundle);
                                ((ActivityC13810kN) AnonymousClass12P.A01(context, ActivityC13810kN.class)).Adm(userNoticeBottomSheetDialogFragment);
                                return;
                            }
                            Log.i("GreenAlert/launchModal");
                            Intent intent = new Intent();
                            intent.setClassName(context.getPackageName(), "com.whatsapp.greenalert.GreenAlertActivity");
                            intent.putExtra("page", 0);
                            context.startActivity(intent);
                            return;
                        }
                    } else {
                        r2 = A03.A03;
                        if (r2 == null) {
                            Log.e("UserNoticeManager/getModal/no content for stage 4");
                            r1 = r5.A00;
                            str3 = "UserNoticeManager/getModal/blockingModal/noContent";
                            r1.AaV(str3, null, true);
                        } else {
                            C43881xk r13 = r2.A00;
                            if (!r5.A0C(r13)) {
                                str2 = "UserNoticeManager/getModal/blocking modal not shown as per timing";
                            } else {
                                r5.A0A(r13, C43901xo.A01(r6, A01));
                                str = "UserNoticeManager/getModal/has blocking modal";
                                Log.i(str);
                                if (r4.A01() != null) {
                                }
                                Bundle bundle = new Bundle();
                                r2.A01(bundle);
                                UserNoticeBottomSheetDialogFragment userNoticeBottomSheetDialogFragment = new UserNoticeBottomSheetDialogFragment();
                                userNoticeBottomSheetDialogFragment.A0U(bundle);
                                ((ActivityC13810kN) AnonymousClass12P.A01(context, ActivityC13810kN.class)).Adm(userNoticeBottomSheetDialogFragment);
                                return;
                            }
                        }
                    }
                }
            }
            Log.i(str2);
        }
        Log.i("UserNoticeLinkActionHandler/handleOpenModal/no modal");
    }
}
