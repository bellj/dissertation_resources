package X;

import android.text.TextUtils;
import com.whatsapp.payments.ui.NoviPayBloksActivity;
import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.6BX  reason: invalid class name */
/* loaded from: classes4.dex */
public final /* synthetic */ class AnonymousClass6BX implements AbstractC136286Ly {
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ AnonymousClass3FE A02;
    public final /* synthetic */ AnonymousClass6B7 A03;
    public final /* synthetic */ NoviPayBloksActivity A04;
    public final /* synthetic */ String A05;
    public final /* synthetic */ String A06;
    public final /* synthetic */ String A07;
    public final /* synthetic */ String A08;
    public final /* synthetic */ boolean A09;

    public /* synthetic */ AnonymousClass6BX(AnonymousClass3FE r1, AnonymousClass6B7 r2, NoviPayBloksActivity noviPayBloksActivity, String str, String str2, String str3, String str4, int i, int i2, boolean z) {
        this.A04 = noviPayBloksActivity;
        this.A02 = r1;
        this.A05 = str;
        this.A06 = str2;
        this.A00 = i;
        this.A01 = i2;
        this.A07 = str3;
        this.A03 = r2;
        this.A09 = z;
        this.A08 = str4;
    }

    @Override // X.AbstractC136286Ly
    public final void AT6(C1315463e r17) {
        NoviPayBloksActivity noviPayBloksActivity = this.A04;
        AnonymousClass3FE r5 = this.A02;
        String str = this.A05;
        String str2 = this.A06;
        int i = this.A00;
        int i2 = this.A01;
        String str3 = this.A07;
        AnonymousClass6B7 r7 = this.A03;
        boolean z = this.A09;
        String str4 = this.A08;
        if (r17 == null) {
            C117315Zl.A0S(r5);
            return;
        }
        long A02 = C117315Zl.A02(noviPayBloksActivity);
        try {
            JSONObject put = C117295Zj.A0a().put("type", "card").put("number", str).put("cvv", str2).put("expiry_month", i).put("expiry_year", i2);
            if (!TextUtils.isEmpty(str3)) {
                put.put("billing_address", C117295Zj.A0a().put("zip", str3));
            }
            String A01 = noviPayBloksActivity.A07.A01(r7, put.toString(), true);
            AnonymousClass009.A05(A01);
            AnonymousClass61S[] r72 = new AnonymousClass61S[3];
            AnonymousClass61S.A05("action", "novi-add-card", r72, 0);
            AnonymousClass61S.A05("encrypted_card_metadata", A01, r72, 1);
            C1310460z A0F = C117295Zj.A0F(new AnonymousClass61S("is-top-up", z), r72, 2);
            if (str4 != null) {
                A0F.A01.add(AnonymousClass61S.A00("client_request_id", str4));
            }
            if (((ActivityC13810kN) noviPayBloksActivity).A0C.A07(822)) {
                AnonymousClass61C r73 = ((AbstractActivityC123635nW) noviPayBloksActivity).A0C;
                String str5 = r17.A02;
                JSONObject A04 = r73.A04(A02);
                AnonymousClass61C.A01(str4, A04);
                try {
                    A04.put("account_id", str5);
                } catch (JSONException unused) {
                    Log.e("PAY: SignedIntentPayloadManager/addNoviAccountId/toJson can't construct json");
                }
                try {
                    A04.put("encrypted_card_metadata", A01);
                    A04.put("is_top_up", z);
                    A04.put("encrypted_nonce", JSONObject.NULL);
                } catch (JSONException unused2) {
                    Log.e("PAY: IntentPayloadHelper/getAddCardIntent/toJson can't construct json");
                }
                A0F.A01.add(AnonymousClass61S.A00("add_card_signed_intent", C129585xx.A00(((AbstractActivityC123635nW) noviPayBloksActivity).A06, new C129585xx(r73.A04, "ADD_CARD", A04))));
            }
            C130155yt.A04(C117305Zk.A09(r5, noviPayBloksActivity, 31), ((AbstractActivityC123635nW) noviPayBloksActivity).A04, A0F, 17, 5);
        } catch (JSONException e) {
            throw new AssertionError(e);
        }
    }
}
