package X;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.components.button.ThumbnailButton;
import com.whatsapp.webpagepreview.WebPagePreviewView;

/* renamed from: X.3b4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70303b4 implements AbstractC41521tf {
    public final /* synthetic */ C55162ht A00;

    @Override // X.AbstractC41521tf
    public /* synthetic */ void AQV() {
    }

    public C70303b4(C55162ht r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC41521tf
    public int AGm() {
        C55162ht r2 = this.A00;
        return r2.A09.A06.A03(r2.A08.A0L.getContext());
    }

    @Override // X.AbstractC41521tf
    public void Adg(Bitmap bitmap, View view, AbstractC15340mz r8) {
        WebPagePreviewView webPagePreviewView = this.A00.A08;
        webPagePreviewView.A0L.setVisibility(0);
        if (bitmap == null || bitmap.getHeight() == 0 || bitmap.getWidth() == 0) {
            ThumbnailButton thumbnailButton = webPagePreviewView.A0L;
            thumbnailButton.setImageDrawable(AnonymousClass2GE.A01(thumbnailButton.getContext(), R.drawable.ic_group_invite_link, R.color.white_alpha_80));
            webPagePreviewView.A0L.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            webPagePreviewView.A0L.setScaleY(1.5f);
            webPagePreviewView.A0L.setScaleX(1.5f);
            ThumbnailButton thumbnailButton2 = webPagePreviewView.A0L;
            C12970iu.A18(thumbnailButton2.getContext(), thumbnailButton2, R.color.media_link_thumbnail_background);
            return;
        }
        C12990iw.A1E(webPagePreviewView.A0L);
        webPagePreviewView.A0L.setImageBitmap(bitmap);
    }

    @Override // X.AbstractC41521tf
    public void Adu(View view) {
        WebPagePreviewView webPagePreviewView = this.A00.A08;
        C12990iw.A1E(webPagePreviewView.A0L);
        C12990iw.A1D(webPagePreviewView.A0L);
    }
}
