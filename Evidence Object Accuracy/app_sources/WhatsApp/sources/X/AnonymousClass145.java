package X;

import android.net.Uri;
import android.text.TextUtils;
import com.whatsapp.stickers.WebpUtils;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/* renamed from: X.145  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass145 {
    public static final Comparator A02 = new Comparator() { // from class: X.1o9
        @Override // java.util.Comparator
        public final int compare(Object obj, Object obj2) {
            return ((File) obj).getName().substring(0, 2).compareTo(((File) obj2).getName().substring(0, 2));
        }
    };
    public final C14330lG A00;
    public final C38481oA A01;

    public AnonymousClass145(C14330lG r3, C16590pI r4) {
        C38481oA r0 = new C38481oA(r4.A00);
        this.A00 = r3;
        this.A01 = r0;
    }

    public final File A00(String str, String str2) {
        File file = new File(this.A00.A03.A00.getCacheDir(), "stickers_cache");
        C14330lG.A03(file, false);
        StringBuilder sb = new StringBuilder();
        sb.append(Uri.encode(str));
        sb.append(File.separatorChar);
        sb.append(Uri.encode(str2));
        return new File(file, sb.toString());
    }

    public final synchronized List A01(String str, String str2, String str3) {
        List list;
        AnonymousClass1KB A00;
        File A002 = A00(str, str2);
        if (A002.exists()) {
            File[] listFiles = A002.listFiles();
            Arrays.sort(listFiles, A02);
            int length = listFiles.length;
            ArrayList arrayList = new ArrayList(length);
            String A01 = AnonymousClass144.A01(str, str2);
            int i = 0;
            while (true) {
                if (i >= length) {
                    arrayList.size();
                    list = Collections.unmodifiableList(arrayList);
                    break;
                }
                File file = listFiles[i];
                String name = file.getName();
                String decode = Uri.decode(C14350lI.A08(name.substring(3)));
                AnonymousClass1KS r1 = new AnonymousClass1KS();
                r1.A0C = decode;
                r1.A08 = new File(A002, name).getAbsolutePath();
                r1.A01 = 2;
                r1.A0B = "image/webp";
                r1.A03 = 512;
                r1.A02 = 512;
                r1.A09 = WebpUtils.A00(file);
                r1.A0E = A01;
                byte[] fetchWebpMetadata = WebpUtils.fetchWebpMetadata(file.getAbsolutePath());
                if (!(fetchWebpMetadata == null || (A00 = AnonymousClass1KB.A00(fetchWebpMetadata)) == null)) {
                    r1.A04 = A00;
                }
                boolean z = false;
                if (str3 != null) {
                    z = true;
                }
                if (z && TextUtils.equals(str3, decode)) {
                    arrayList.size();
                    list = Collections.singletonList(r1);
                    break;
                }
                arrayList.add(r1);
                i++;
            }
        } else {
            list = Collections.emptyList();
        }
        return list;
    }

    public synchronized void A02(String str, String str2, List list) {
        StringBuilder sb;
        C38481oA r4;
        InputStream inputStream;
        if (list.size() < 100) {
            File A00 = A00(str, str2);
            C14350lI.A0C(A00);
            if (A00.exists() || A00.mkdirs()) {
                for (int i = 0; i < list.size(); i++) {
                    AnonymousClass1KS r6 = (AnonymousClass1KS) list.get(i);
                    String str3 = r6.A0C;
                    if (i < 100) {
                        if (i < 10) {
                            sb = new StringBuilder("0");
                        } else {
                            sb = new StringBuilder();
                        }
                        sb.append(i);
                        sb.append("_");
                        sb.append(Uri.encode(str3));
                        sb.append(".webp");
                        File file = new File(A00, sb.toString());
                        try {
                            r4 = this.A01;
                        } catch (IOException e) {
                            Log.e("error closing the input stream.", e);
                        }
                        try {
                            inputStream = r4.A00.getContentResolver().openInputStream(Uri.parse(r6.A08));
                            if (inputStream != null) {
                                try {
                                    if (C14350lI.A0P(file, inputStream)) {
                                        if (r6.A04 != null) {
                                            WebpUtils.A01(file, r6.A04.A01());
                                        }
                                        file.getAbsolutePath();
                                        inputStream.close();
                                    }
                                } catch (Throwable th) {
                                    if (inputStream != null) {
                                        try {
                                            inputStream.close();
                                        } catch (Throwable unused) {
                                        }
                                    }
                                    throw th;
                                }
                            }
                        } catch (FileNotFoundException e2) {
                            Log.e("error openUri", e2);
                            inputStream = null;
                        }
                        C14350lI.A0C(A00);
                        if (inputStream != null) {
                            inputStream.close();
                        }
                    } else {
                        StringBuilder sb2 = new StringBuilder("ThirdPartyStickerStorage/createStickerFileName/sticker index is more more than 100. Index: ");
                        sb2.append(i);
                        throw new IllegalStateException(sb2.toString());
                    }
                }
            } else {
                A00.getAbsolutePath();
            }
        } else {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("ThirdPartyStickerStorage/addStickersInPack/total amount of stickers in pack is more than 100, size: ");
            sb3.append(list.size());
            throw new IllegalStateException(sb3.toString());
        }
    }
}
