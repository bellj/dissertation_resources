package X;

import android.content.ContentValues;
import android.database.Cursor;
import com.whatsapp.jid.Jid;

/* renamed from: X.1Cv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26311Cv {
    public final C43651xN A00;

    public C26311Cv(C232010t r2) {
        this.A00 = new C43651xN(r2);
    }

    public C43661xO A00(AbstractC14640lm r13) {
        C43651xN r1 = this.A00;
        C28181Ma r4 = new C28181Ma(true);
        r4.A03();
        C16310on A01 = r1.A00.get();
        try {
            Cursor A03 = AbstractC21570xd.A03(A01, "wa_last_entry_point", "wa_last_entry_point.jid = ?", null, "CONTACT_ENTRY_POINT", C43651xN.A00, new String[]{r13.getRawString()});
            C43661xO r2 = null;
            if (A03 == null) {
                StringBuilder sb = new StringBuilder();
                sb.append("contact-mgr-db/unable to get contact by jid ");
                sb.append(r13);
                AnonymousClass009.A07(sb.toString());
                A01.close();
                return null;
            }
            if (A03.moveToNext()) {
                r2 = new C43661xO(A03);
            }
            A03.close();
            A01.close();
            r4.A00();
            return r2;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A01(C43661xO r7) {
        C43651xN r5 = this.A00;
        Jid jid = r7.A01;
        if (jid == null) {
            AnonymousClass009.A07("setOrUpdateLastEntryPoint/jid is null");
            return;
        }
        C28181Ma r3 = new C28181Ma(true);
        r3.A03();
        ContentValues contentValues = new ContentValues(4);
        contentValues.put("jid", jid.getRawString());
        contentValues.put("entry_point_type", r7.A03);
        contentValues.put("entry_point_id", r7.A02);
        contentValues.put("entry_point_time", Long.valueOf(r7.A00));
        try {
            C16310on A02 = r5.A00.A02();
            AbstractC21570xd.A04(contentValues, A02, "wa_last_entry_point");
            A02.close();
        } catch (IllegalArgumentException e) {
            StringBuilder sb = new StringBuilder("setOrUpdateLastEntryPoint/unable to update entry point for jid ");
            sb.append(jid);
            AnonymousClass009.A08(sb.toString(), e);
        }
        r3.A00();
    }
}
