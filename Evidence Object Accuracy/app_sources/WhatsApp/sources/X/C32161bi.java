package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.1bi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C32161bi extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C32161bi A06;
    public static volatile AnonymousClass255 A07;
    public int A00;
    public long A01;
    public long A02;
    public AnonymousClass1K6 A03 = AnonymousClass277.A01;
    public String A04 = "";
    public String A05 = "";

    static {
        C32161bi r0 = new C32161bi();
        A06 = r0;
        r0.A0W();
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i;
        int i2 = ((AbstractC27091Fz) this).A00;
        if (i2 != -1) {
            return i2;
        }
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i = CodedOutputStream.A06(1, this.A02) + 0;
        } else {
            i = 0;
        }
        if ((i3 & 2) == 2) {
            i += CodedOutputStream.A07(2, this.A04);
        }
        if ((this.A00 & 4) == 4) {
            i += CodedOutputStream.A07(4, this.A05);
        }
        for (int i4 = 0; i4 < this.A03.size(); i4++) {
            i += CodedOutputStream.A0A((AnonymousClass1G1) this.A03.get(i4), 8);
        }
        if ((this.A00 & 8) == 8) {
            i += CodedOutputStream.A06(10, this.A01);
        }
        int A00 = i + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0H(1, this.A02);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0I(2, this.A04);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0I(4, this.A05);
        }
        for (int i = 0; i < this.A03.size(); i++) {
            codedOutputStream.A0L((AnonymousClass1G1) this.A03.get(i), 8);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0H(10, this.A01);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
