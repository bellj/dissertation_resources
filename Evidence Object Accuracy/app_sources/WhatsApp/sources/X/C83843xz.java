package X;

import com.whatsapp.qrcode.AuthenticationActivity;
import com.whatsapp.util.Log;

/* renamed from: X.3xz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83843xz extends AnonymousClass4UT {
    public final /* synthetic */ AuthenticationActivity A00;

    public C83843xz(AuthenticationActivity authenticationActivity) {
        this.A00 = authenticationActivity;
    }

    @Override // X.AnonymousClass4UT
    public void A00() {
        Log.i("AuthenticationActivity/fingerprint-success-animation-end");
        AuthenticationActivity authenticationActivity = this.A00;
        authenticationActivity.setResult(-1);
        authenticationActivity.finish();
    }
}
