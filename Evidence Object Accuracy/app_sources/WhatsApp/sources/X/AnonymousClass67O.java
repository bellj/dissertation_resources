package X;

import java.util.Map;

/* renamed from: X.67O  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass67O implements AnonymousClass2DQ {
    public final /* synthetic */ C14230l4 A00;
    public final /* synthetic */ C14220l3 A01;
    public final /* synthetic */ C119575ef A02;

    public AnonymousClass67O(C14230l4 r1, C14220l3 r2, C119575ef r3) {
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass2DQ
    public void AQD(Map map) {
        Object obj = this.A01.A00.get(4);
        if (obj instanceof C94724cR) {
            C14250l6 r3 = new C14250l6(this.A00);
            C1093751l r2 = ((C94724cR) obj).A00;
            C14210l2 r1 = new C14210l2();
            r1.A04(map, 0);
            r3.A01(r1.A03(), r2);
        }
    }
}
