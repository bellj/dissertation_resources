package X;

import com.whatsapp.payments.ui.BrazilPaymentActivity;

/* renamed from: X.69P  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass69P implements AbstractC35651iS {
    public final /* synthetic */ BrazilPaymentActivity A00;

    @Override // X.AbstractC35651iS
    public void ATe(AnonymousClass1IR r1) {
    }

    public AnonymousClass69P(BrazilPaymentActivity brazilPaymentActivity) {
        this.A00 = brazilPaymentActivity;
    }

    @Override // X.AbstractC35651iS
    public void ATf(AnonymousClass1IR r6) {
        BrazilPaymentActivity brazilPaymentActivity;
        C121255hW r3;
        short s;
        if (r6.A02 != 401) {
            if (r6.A0H()) {
                brazilPaymentActivity = this.A00;
                r3 = brazilPaymentActivity.A0N;
                s = 2;
            } else {
                int i = r6.A02;
                brazilPaymentActivity = this.A00;
                r3 = brazilPaymentActivity.A0N;
                s = 3;
                if (i == 402) {
                    s = 49;
                }
            }
            r3.A04(brazilPaymentActivity.A00, s);
            brazilPaymentActivity.A0a = false;
            brazilPaymentActivity.A0F.A04(brazilPaymentActivity.A0c);
        }
    }
}
