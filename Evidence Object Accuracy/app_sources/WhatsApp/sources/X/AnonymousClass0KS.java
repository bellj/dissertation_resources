package X;

import android.app.Dialog;
import android.os.Build;
import android.view.View;

/* renamed from: X.0KS  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0KS {
    public static View A00(Dialog dialog, int i) {
        if (Build.VERSION.SDK_INT >= 28) {
            return (View) AnonymousClass0KR.A00(dialog, i);
        }
        View findViewById = dialog.findViewById(i);
        if (findViewById != null) {
            return findViewById;
        }
        throw new IllegalArgumentException("ID does not reference a View inside this Dialog");
    }
}
