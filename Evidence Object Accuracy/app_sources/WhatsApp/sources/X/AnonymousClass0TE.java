package X;

/* renamed from: X.0TE  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0TE {
    public static AnonymousClass0H4 A00(C05540Py r3, AbstractC08850bx r4) {
        return new AnonymousClass0H4(AnonymousClass0TN.A00(r3, C08280as.A00, r4, 1.0f, false));
    }

    public static AnonymousClass0H9 A01(C05540Py r3, AbstractC08850bx r4, boolean z) {
        float f;
        if (z) {
            f = AnonymousClass0UV.A00();
        } else {
            f = 1.0f;
        }
        return new AnonymousClass0H9(AnonymousClass0TN.A00(r3, C08290at.A00, r4, f, false));
    }

    public static AnonymousClass0HA A02(C05540Py r3, AbstractC08850bx r4) {
        return new AnonymousClass0HA(AnonymousClass0TN.A00(r3, C08300au.A00, r4, 1.0f, false));
    }
}
