package X;

import com.whatsapp.util.Log;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.3Xj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68953Xj implements AbstractC28461Nh {
    public AnonymousClass3FM A00;
    public final C19950uw A01;
    public final C19940uv A02;
    public final C91454Ru A03;
    public final String A04;

    @Override // X.AbstractC28461Nh
    public /* synthetic */ void AOt(long j) {
    }

    public C68953Xj(C19950uw r1, C19940uv r2, C91454Ru r3, String str) {
        this.A02 = r2;
        this.A01 = r1;
        this.A04 = str;
        this.A03 = r3;
    }

    @Override // X.AbstractC28461Nh
    public void AOZ(String str) {
        Log.e(C12960it.A0d(str, C12960it.A0k("httpresumecheck/connected to url: ")));
    }

    @Override // X.AbstractC28461Nh
    public void APq(String str) {
        Log.e(C12960it.A0d(str, C12960it.A0k("httpresumecheck/error = ")));
    }

    @Override // X.AbstractC28461Nh
    public void AV9(String str, Map map) {
        try {
            JSONObject A05 = C13000ix.A05(str);
            if (!A05.has("resume")) {
                return;
            }
            if ("complete".equals(A05.optString("resume"))) {
                this.A00.A05 = A05.optString("url");
                this.A00.A03 = A05.optString("direct_path");
                this.A00.A02 = AnonymousClass4AV.COMPLETE;
                return;
            }
            this.A00.A01 = A05.optInt("resume");
            this.A00.A02 = AnonymousClass4AV.RESUME;
        } catch (JSONException e) {
            Log.w("mediaupload/MMS upload resume form post failed to parse JSON response; ", e);
            this.A00.A02 = AnonymousClass4AV.FAILURE;
        }
    }
}
