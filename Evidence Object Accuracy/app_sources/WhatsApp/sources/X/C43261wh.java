package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.msys.mci.DefaultCrypto;
import com.google.protobuf.CodedOutputStream;
import java.io.IOException;
import org.wawebrtc.MediaCodecVideoEncoder;

/* renamed from: X.1wh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C43261wh extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C43261wh A0O;
    public static volatile AnonymousClass255 A0P;
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public long A05;
    public AbstractC27881Jp A06;
    public AbstractC27881Jp A07;
    public AnonymousClass1K6 A08 = AnonymousClass277.A01;
    public C57092mS A09;
    public C57432n2 A0A;
    public C57662nQ A0B;
    public AnonymousClass1PH A0C;
    public C27081Fy A0D;
    public AnonymousClass1G8 A0E;
    public String A0F = "";
    public String A0G;
    public String A0H;
    public String A0I;
    public String A0J;
    public String A0K = "";
    public String A0L = "";
    public String A0M = "";
    public boolean A0N;

    static {
        C43261wh r0 = new C43261wh();
        A0O = r0;
        r0.A0W();
    }

    public C43261wh() {
        AbstractC27881Jp r0 = AbstractC27881Jp.A01;
        this.A06 = r0;
        this.A07 = r0;
        this.A0H = "";
        this.A0G = "";
        this.A0I = "";
        this.A0J = "";
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r16, Object obj, Object obj2) {
        int i;
        int i2;
        AnonymousClass1G9 r1;
        switch (r16.ordinal()) {
            case 0:
                return A0O;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                C43261wh r3 = (C43261wh) obj2;
                int i3 = this.A00;
                boolean z = true;
                if ((i3 & 1) != 1) {
                    z = false;
                }
                String str = this.A0M;
                int i4 = r3.A00;
                boolean z2 = true;
                if ((i4 & 1) != 1) {
                    z2 = false;
                }
                this.A0M = r8.Afy(str, r3.A0M, z, z2);
                boolean z3 = false;
                if ((i3 & 2) == 2) {
                    z3 = true;
                }
                String str2 = this.A0K;
                boolean z4 = false;
                if ((i4 & 2) == 2) {
                    z4 = true;
                }
                this.A0K = r8.Afy(str2, r3.A0K, z3, z4);
                this.A0D = (C27081Fy) r8.Aft(this.A0D, r3.A0D);
                boolean z5 = false;
                if ((this.A00 & 8) == 8) {
                    z5 = true;
                }
                String str3 = this.A0L;
                boolean z6 = false;
                if ((r3.A00 & 8) == 8) {
                    z6 = true;
                }
                this.A0L = r8.Afy(str3, r3.A0L, z5, z6);
                this.A08 = r8.Afr(this.A08, r3.A08);
                int i5 = this.A00;
                boolean z7 = false;
                if ((i5 & 16) == 16) {
                    z7 = true;
                }
                String str4 = this.A0F;
                int i6 = r3.A00;
                boolean z8 = false;
                if ((i6 & 16) == 16) {
                    z8 = true;
                }
                this.A0F = r8.Afy(str4, r3.A0F, z7, z8);
                boolean z9 = false;
                if ((i5 & 32) == 32) {
                    z9 = true;
                }
                AbstractC27881Jp r2 = this.A06;
                boolean z10 = false;
                if ((i6 & 32) == 32) {
                    z10 = true;
                }
                this.A06 = r8.Afm(r2, r3.A06, z9, z10);
                int i7 = this.A00;
                boolean z11 = false;
                if ((i7 & 64) == 64) {
                    z11 = true;
                }
                int i8 = this.A01;
                int i9 = r3.A00;
                boolean z12 = false;
                if ((i9 & 64) == 64) {
                    z12 = true;
                }
                this.A01 = r8.Afp(i8, r3.A01, z11, z12);
                boolean z13 = false;
                if ((i7 & 128) == 128) {
                    z13 = true;
                }
                int i10 = this.A04;
                boolean z14 = false;
                if ((i9 & 128) == 128) {
                    z14 = true;
                }
                this.A04 = r8.Afp(i10, r3.A04, z13, z14);
                boolean z15 = false;
                if ((i7 & 256) == 256) {
                    z15 = true;
                }
                boolean z16 = this.A0N;
                boolean z17 = false;
                if ((i9 & 256) == 256) {
                    z17 = true;
                }
                this.A0N = r8.Afl(z15, z16, z17, r3.A0N);
                this.A0A = (C57432n2) r8.Aft(this.A0A, r3.A0A);
                this.A0E = (AnonymousClass1G8) r8.Aft(this.A0E, r3.A0E);
                int i11 = this.A00;
                boolean z18 = false;
                if ((i11 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
                    z18 = true;
                }
                int i12 = this.A03;
                int i13 = r3.A00;
                boolean z19 = false;
                if ((i13 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
                    z19 = true;
                }
                this.A03 = r8.Afp(i12, r3.A03, z18, z19);
                boolean z20 = false;
                if ((i11 & 4096) == 4096) {
                    z20 = true;
                }
                long j = this.A05;
                boolean z21 = false;
                if ((i13 & 4096) == 4096) {
                    z21 = true;
                }
                this.A05 = r8.Afs(j, r3.A05, z20, z21);
                boolean z22 = false;
                if ((i11 & DefaultCrypto.BUFFER_SIZE) == 8192) {
                    z22 = true;
                }
                AbstractC27881Jp r22 = this.A07;
                boolean z23 = false;
                if ((i13 & DefaultCrypto.BUFFER_SIZE) == 8192) {
                    z23 = true;
                }
                this.A07 = r8.Afm(r22, r3.A07, z22, z23);
                this.A0B = (C57662nQ) r8.Aft(this.A0B, r3.A0B);
                int i14 = this.A00;
                boolean z24 = false;
                if ((i14 & 32768) == 32768) {
                    z24 = true;
                }
                String str5 = this.A0H;
                int i15 = r3.A00;
                boolean z25 = false;
                if ((i15 & 32768) == 32768) {
                    z25 = true;
                }
                this.A0H = r8.Afy(str5, r3.A0H, z24, z25);
                boolean z26 = false;
                if ((i14 & 65536) == 65536) {
                    z26 = true;
                }
                String str6 = this.A0G;
                boolean z27 = false;
                if ((i15 & 65536) == 65536) {
                    z27 = true;
                }
                this.A0G = r8.Afy(str6, r3.A0G, z26, z27);
                boolean z28 = false;
                if ((i14 & C25981Bo.A0F) == 131072) {
                    z28 = true;
                }
                int i16 = this.A02;
                boolean z29 = false;
                if ((i15 & C25981Bo.A0F) == 131072) {
                    z29 = true;
                }
                this.A02 = r8.Afp(i16, r3.A02, z28, z29);
                this.A0C = (AnonymousClass1PH) r8.Aft(this.A0C, r3.A0C);
                this.A09 = (C57092mS) r8.Aft(this.A09, r3.A09);
                int i17 = this.A00;
                boolean z30 = false;
                if ((i17 & 1048576) == 1048576) {
                    z30 = true;
                }
                String str7 = this.A0I;
                int i18 = r3.A00;
                boolean z31 = false;
                if ((i18 & 1048576) == 1048576) {
                    z31 = true;
                }
                this.A0I = r8.Afy(str7, r3.A0I, z30, z31);
                boolean z32 = false;
                if ((i17 & 2097152) == 2097152) {
                    z32 = true;
                }
                String str8 = this.A0J;
                boolean z33 = false;
                if ((i18 & 2097152) == 2097152) {
                    z33 = true;
                }
                this.A0J = r8.Afy(str8, r3.A0J, z32, z33);
                if (r8 == C463025i.A00) {
                    this.A00 = i17 | i18;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                AnonymousClass254 r32 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        try {
                            int A03 = r82.A03();
                            switch (A03) {
                                case 0:
                                    break;
                                case 10:
                                    String A0A = r82.A0A();
                                    this.A00 = 1 | this.A00;
                                    this.A0M = A0A;
                                    continue;
                                case 18:
                                    String A0A2 = r82.A0A();
                                    this.A00 |= 2;
                                    this.A0K = A0A2;
                                    continue;
                                case 26:
                                    i = 4;
                                    AnonymousClass1G3 r12 = (this.A00 & 4) == 4 ? (AnonymousClass1G3) this.A0D.A0T() : null;
                                    C27081Fy r0 = (C27081Fy) r82.A09(r32, C27081Fy.A0i.A0U());
                                    this.A0D = r0;
                                    if (r12 != null) {
                                        r12.A04(r0);
                                        this.A0D = (C27081Fy) r12.A01();
                                    }
                                    i2 = this.A00;
                                    break;
                                case 34:
                                    String A0A3 = r82.A0A();
                                    this.A00 |= 8;
                                    this.A0L = A0A3;
                                    continue;
                                case 122:
                                    String A0A4 = r82.A0A();
                                    AnonymousClass1K6 r13 = this.A08;
                                    if (!((AnonymousClass1K7) r13).A00) {
                                        r13 = AbstractC27091Fz.A0G(r13);
                                        this.A08 = r13;
                                    }
                                    r13.add(A0A4);
                                    continue;
                                case 146:
                                    String A0A5 = r82.A0A();
                                    this.A00 |= 16;
                                    this.A0F = A0A5;
                                    continue;
                                case 154:
                                    this.A00 |= 32;
                                    this.A06 = r82.A08();
                                    continue;
                                case 160:
                                    this.A00 |= 64;
                                    this.A01 = r82.A02();
                                    continue;
                                case 168:
                                    this.A00 |= 128;
                                    this.A04 = r82.A02();
                                    continue;
                                case MediaCodecVideoEncoder.MIN_ENCODER_WIDTH /* 176 */:
                                    this.A00 |= 256;
                                    this.A0N = r82.A0F();
                                    continue;
                                case 186:
                                    i = 512;
                                    C81593uG r14 = (this.A00 & 512) == 512 ? (C81593uG) this.A0A.A0T() : null;
                                    C57432n2 r02 = (C57432n2) r82.A09(r32, C57432n2.A05.A0U());
                                    this.A0A = r02;
                                    if (r14 != null) {
                                        r14.A04(r02);
                                        this.A0A = (C57432n2) r14.A01();
                                    }
                                    i2 = this.A00;
                                    break;
                                case 194:
                                    int i19 = this.A00;
                                    i = EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
                                    if ((i19 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
                                        r1 = (AnonymousClass1G9) this.A0E.A0T();
                                    } else {
                                        r1 = null;
                                    }
                                    AnonymousClass1G8 r03 = (AnonymousClass1G8) r82.A09(r32, AnonymousClass1G8.A05.A0U());
                                    this.A0E = r03;
                                    if (r1 != null) {
                                        r1.A04(r03);
                                        this.A0E = (AnonymousClass1G8) r1.A01();
                                    }
                                    i2 = this.A00;
                                    break;
                                case 200:
                                    this.A00 |= EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
                                    this.A03 = r82.A02();
                                    continue;
                                case 208:
                                    this.A00 |= 4096;
                                    this.A05 = r82.A06();
                                    continue;
                                case 218:
                                    this.A00 |= DefaultCrypto.BUFFER_SIZE;
                                    this.A07 = r82.A08();
                                    continue;
                                case 226:
                                    i = 16384;
                                    C81613uI r15 = (this.A00 & 16384) == 16384 ? (C81613uI) this.A0B.A0T() : null;
                                    C57662nQ r04 = (C57662nQ) r82.A09(r32, C57662nQ.A0D.A0U());
                                    this.A0B = r04;
                                    if (r15 != null) {
                                        r15.A04(r04);
                                        this.A0B = (C57662nQ) r15.A01();
                                    }
                                    i2 = this.A00;
                                    break;
                                case 234:
                                    String A0A6 = r82.A0A();
                                    this.A00 |= 32768;
                                    this.A0H = A0A6;
                                    continue;
                                case 242:
                                    String A0A7 = r82.A0A();
                                    this.A00 |= 65536;
                                    this.A0G = A0A7;
                                    continue;
                                case 248:
                                    this.A00 |= C25981Bo.A0F;
                                    this.A02 = r82.A02();
                                    continue;
                                case 258:
                                    i = 262144;
                                    AnonymousClass1PI r17 = (this.A00 & 262144) == 262144 ? (AnonymousClass1PI) this.A0C.A0T() : null;
                                    AnonymousClass1PH r05 = (AnonymousClass1PH) r82.A09(r32, AnonymousClass1PH.A02.A0U());
                                    this.A0C = r05;
                                    if (r17 != null) {
                                        r17.A04(r05);
                                        this.A0C = (AnonymousClass1PH) r17.A01();
                                    }
                                    i2 = this.A00;
                                    break;
                                case 266:
                                    i = 524288;
                                    C81583uF r18 = (this.A00 & 524288) == 524288 ? (C81583uF) this.A09.A0T() : null;
                                    C57092mS r06 = (C57092mS) r82.A09(r32, C57092mS.A03.A0U());
                                    this.A09 = r06;
                                    if (r18 != null) {
                                        r18.A04(r06);
                                        this.A09 = (C57092mS) r18.A01();
                                    }
                                    i2 = this.A00;
                                    break;
                                case 274:
                                    String A0A8 = r82.A0A();
                                    this.A00 |= 1048576;
                                    this.A0I = A0A8;
                                    continue;
                                case 282:
                                    String A0A9 = r82.A0A();
                                    this.A00 |= 2097152;
                                    this.A0J = A0A9;
                                    continue;
                                default:
                                    if (!A0a(r82, A03)) {
                                        break;
                                    } else {
                                        continue;
                                    }
                            }
                            this.A00 = i2 | i;
                        } catch (C28971Pt e) {
                            e.unfinishedMessage = this;
                            throw new RuntimeException(e);
                        }
                    } catch (IOException e2) {
                        C28971Pt r19 = new C28971Pt(e2.getMessage());
                        r19.unfinishedMessage = this;
                        throw new RuntimeException(r19);
                    }
                }
            case 3:
                ((AnonymousClass1K7) this.A08).A00 = false;
                return null;
            case 4:
                return new C43261wh();
            case 5:
                return new C81603uH();
            case 6:
                break;
            case 7:
                if (A0P == null) {
                    synchronized (C43261wh.class) {
                        if (A0P == null) {
                            A0P = new AnonymousClass255(A0O);
                        }
                    }
                }
                return A0P;
            default:
                throw new UnsupportedOperationException();
        }
        return A0O;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i;
        int i2 = ((AbstractC27091Fz) this).A00;
        if (i2 != -1) {
            return i2;
        }
        if ((this.A00 & 1) == 1) {
            i = CodedOutputStream.A07(1, this.A0M) + 0;
        } else {
            i = 0;
        }
        if ((this.A00 & 2) == 2) {
            i += CodedOutputStream.A07(2, this.A0K);
        }
        if ((this.A00 & 4) == 4) {
            C27081Fy r0 = this.A0D;
            if (r0 == null) {
                r0 = C27081Fy.A0i;
            }
            i += CodedOutputStream.A0A(r0, 3);
        }
        if ((this.A00 & 8) == 8) {
            i += CodedOutputStream.A07(4, this.A0L);
        }
        int i3 = 0;
        for (int i4 = 0; i4 < this.A08.size(); i4++) {
            i3 += CodedOutputStream.A0B((String) this.A08.get(i4));
        }
        int size = i + i3 + this.A08.size();
        if ((this.A00 & 16) == 16) {
            size += CodedOutputStream.A07(18, this.A0F);
        }
        int i5 = this.A00;
        if ((i5 & 32) == 32) {
            size += CodedOutputStream.A09(this.A06, 19);
        }
        if ((i5 & 64) == 64) {
            size += CodedOutputStream.A04(20, this.A01);
        }
        if ((i5 & 128) == 128) {
            size += CodedOutputStream.A04(21, this.A04);
        }
        if ((i5 & 256) == 256) {
            size += CodedOutputStream.A00(22);
        }
        if ((i5 & 512) == 512) {
            C57432n2 r02 = this.A0A;
            if (r02 == null) {
                r02 = C57432n2.A05;
            }
            size += CodedOutputStream.A0A(r02, 23);
        }
        if ((this.A00 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            AnonymousClass1G8 r03 = this.A0E;
            if (r03 == null) {
                r03 = AnonymousClass1G8.A05;
            }
            size += CodedOutputStream.A0A(r03, 24);
        }
        int i6 = this.A00;
        if ((i6 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            size += CodedOutputStream.A04(25, this.A03);
        }
        if ((i6 & 4096) == 4096) {
            size += CodedOutputStream.A05(26, this.A05);
        }
        if ((i6 & DefaultCrypto.BUFFER_SIZE) == 8192) {
            size += CodedOutputStream.A09(this.A07, 27);
        }
        if ((i6 & 16384) == 16384) {
            C57662nQ r04 = this.A0B;
            if (r04 == null) {
                r04 = C57662nQ.A0D;
            }
            size += CodedOutputStream.A0A(r04, 28);
        }
        if ((this.A00 & 32768) == 32768) {
            size += CodedOutputStream.A07(29, this.A0H);
        }
        if ((this.A00 & 65536) == 65536) {
            size += CodedOutputStream.A07(30, this.A0G);
        }
        int i7 = this.A00;
        if ((i7 & C25981Bo.A0F) == 131072) {
            size += CodedOutputStream.A04(31, this.A02);
        }
        if ((i7 & 262144) == 262144) {
            AnonymousClass1PH r05 = this.A0C;
            if (r05 == null) {
                r05 = AnonymousClass1PH.A02;
            }
            size += CodedOutputStream.A0A(r05, 32);
        }
        if ((this.A00 & 524288) == 524288) {
            C57092mS r06 = this.A09;
            if (r06 == null) {
                r06 = C57092mS.A03;
            }
            size += CodedOutputStream.A0A(r06, 33);
        }
        if ((this.A00 & 1048576) == 1048576) {
            size += CodedOutputStream.A07(34, this.A0I);
        }
        if ((this.A00 & 2097152) == 2097152) {
            size += CodedOutputStream.A07(35, this.A0J);
        }
        int A00 = size + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A0M);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0I(2, this.A0K);
        }
        if ((this.A00 & 4) == 4) {
            C27081Fy r0 = this.A0D;
            if (r0 == null) {
                r0 = C27081Fy.A0i;
            }
            codedOutputStream.A0L(r0, 3);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0I(4, this.A0L);
        }
        for (int i = 0; i < this.A08.size(); i++) {
            codedOutputStream.A0I(15, (String) this.A08.get(i));
        }
        if ((this.A00 & 16) == 16) {
            codedOutputStream.A0I(18, this.A0F);
        }
        if ((this.A00 & 32) == 32) {
            codedOutputStream.A0K(this.A06, 19);
        }
        if ((this.A00 & 64) == 64) {
            codedOutputStream.A0F(20, this.A01);
        }
        if ((this.A00 & 128) == 128) {
            codedOutputStream.A0F(21, this.A04);
        }
        if ((this.A00 & 256) == 256) {
            codedOutputStream.A0J(22, this.A0N);
        }
        if ((this.A00 & 512) == 512) {
            C57432n2 r02 = this.A0A;
            if (r02 == null) {
                r02 = C57432n2.A05;
            }
            codedOutputStream.A0L(r02, 23);
        }
        if ((this.A00 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            AnonymousClass1G8 r03 = this.A0E;
            if (r03 == null) {
                r03 = AnonymousClass1G8.A05;
            }
            codedOutputStream.A0L(r03, 24);
        }
        if ((this.A00 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            codedOutputStream.A0F(25, this.A03);
        }
        if ((this.A00 & 4096) == 4096) {
            codedOutputStream.A0H(26, this.A05);
        }
        if ((this.A00 & DefaultCrypto.BUFFER_SIZE) == 8192) {
            codedOutputStream.A0K(this.A07, 27);
        }
        if ((this.A00 & 16384) == 16384) {
            C57662nQ r04 = this.A0B;
            if (r04 == null) {
                r04 = C57662nQ.A0D;
            }
            codedOutputStream.A0L(r04, 28);
        }
        if ((this.A00 & 32768) == 32768) {
            codedOutputStream.A0I(29, this.A0H);
        }
        if ((this.A00 & 65536) == 65536) {
            codedOutputStream.A0I(30, this.A0G);
        }
        if ((this.A00 & C25981Bo.A0F) == 131072) {
            codedOutputStream.A0F(31, this.A02);
        }
        if ((this.A00 & 262144) == 262144) {
            AnonymousClass1PH r05 = this.A0C;
            if (r05 == null) {
                r05 = AnonymousClass1PH.A02;
            }
            codedOutputStream.A0L(r05, 32);
        }
        if ((this.A00 & 524288) == 524288) {
            C57092mS r06 = this.A09;
            if (r06 == null) {
                r06 = C57092mS.A03;
            }
            codedOutputStream.A0L(r06, 33);
        }
        if ((this.A00 & 1048576) == 1048576) {
            codedOutputStream.A0I(34, this.A0I);
        }
        if ((this.A00 & 2097152) == 2097152) {
            codedOutputStream.A0I(35, this.A0J);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
