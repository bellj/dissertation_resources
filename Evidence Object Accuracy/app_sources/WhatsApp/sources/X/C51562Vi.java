package X;

import java.util.ArrayList;

/* renamed from: X.2Vi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C51562Vi {
    public C15250mo A00;
    public final AnonymousClass02P A01;
    public final AnonymousClass02P A02;
    public final AnonymousClass016 A03 = new AnonymousClass016();
    public final C15610nY A04;
    public final C20830wO A05;
    public final C15680nj A06;
    public final C21230x5 A07;
    public final AnonymousClass1AY A08;

    public C51562Vi(AnonymousClass017 r4, AnonymousClass017 r5, AnonymousClass017 r6, AnonymousClass017 r7, C15610nY r8, AnonymousClass018 r9, C20830wO r10, C15680nj r11, C21230x5 r12, AnonymousClass1AY r13, AnonymousClass1AW r14) {
        AnonymousClass02P r1 = new AnonymousClass02P();
        this.A02 = r1;
        AnonymousClass02P r2 = new AnonymousClass02P();
        this.A01 = r2;
        this.A04 = r8;
        this.A06 = r11;
        this.A08 = r13;
        this.A07 = r12;
        this.A05 = r10;
        this.A00 = new C15250mo(r9);
        r14.A00(new AnonymousClass02O() { // from class: X.2W5
            @Override // X.AnonymousClass02O
            public final Object apply(Object obj) {
                C28181Ma r52;
                try {
                    C51562Vi r102 = C51562Vi.this;
                    AnonymousClass1KL r15 = (AnonymousClass1KL) obj;
                    AnonymousClass016 r42 = r102.A03;
                    r42.A0A(Boolean.TRUE);
                    C15250mo r132 = (C15250mo) r15.A01;
                    int A00 = C34961gz.A00(0);
                    C21230x5 r62 = r102.A07;
                    r62.ALF(926875649, A00);
                    r62.AKy("token_count", 926875649, A00, r132.A02().size());
                    r62.AKy("domain", 926875649, A00, 0);
                    AnonymousClass02N r112 = r15.A00;
                    StringBuilder sb = new StringBuilder("ContactSearchManager/getForContactsQuery/");
                    sb.append(r132.A01().length());
                    r52 = new C28181Ma(sb.toString());
                    ArrayList arrayList = new ArrayList();
                    try {
                        r112.A02();
                        Integer num = 0;
                        if (!num.equals(Integer.valueOf(r132.A02)) || r132.A04 != null || (!(!r132.A02().isEmpty()) && r132.A06 == null)) {
                            r52.A02("empty");
                            r62.AL5(926875649, A00);
                            r42.A0A(Boolean.FALSE);
                        } else {
                            ArrayList arrayList2 = new ArrayList();
                            if (!r132.A02().isEmpty()) {
                                arrayList2.add(new AnonymousClass2W4(r102.A04, r102.A05, r132.A02()));
                            }
                            C49662Lr r16 = r132.A06;
                            if (r16 != null) {
                                arrayList2.add(r102.A08.A00(r16));
                            }
                            r52.A02("filter");
                            C34961gz.A01(r62, Integer.valueOf(A00), "filter");
                            r112.A02();
                            for (C15370n3 r3 : r102.A05.A02()) {
                                r112.A02();
                                if (r3.A0C != null && !r102.A06.A04().contains(r3.A0B(AbstractC14640lm.class))) {
                                    AbstractC14640lm r0 = (AbstractC14640lm) r3.A0B(AbstractC14640lm.class);
                                    AnonymousClass009.A05(r0);
                                    if (C20830wO.A00(r0, arrayList2)) {
                                        arrayList.add(r3);
                                    }
                                }
                            }
                            r52.A02("done");
                            r42.A0A(Boolean.FALSE);
                            r62.AL6(926875649, A00, 2);
                        }
                    } catch (AnonymousClass04U unused) {
                        r52.A02("cancelled");
                        if (r52.A00() < 300) {
                            r62.AL5(926875649, A00);
                        } else {
                            r62.AL6(926875649, A00, 4);
                        }
                    }
                    return arrayList;
                } finally {
                    r52.A01();
                }
            }
        }, r1, r2);
        r1.A0D(r4, new AnonymousClass02B() { // from class: X.2W6
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                C51562Vi r0 = C51562Vi.this;
                C15250mo r15 = r0.A00;
                r15.A03((String) obj);
                r0.A02.A0B(r15);
            }
        });
        r1.A0D(r5, new AnonymousClass02B() { // from class: X.2W7
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                C51562Vi r22 = C51562Vi.this;
                C15250mo r15 = r22.A00;
                r15.A02 = ((Number) obj).intValue();
                r22.A02.A0B(r15);
            }
        });
        r1.A0D(r6, new AnonymousClass02B() { // from class: X.2W8
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                C51562Vi r0 = C51562Vi.this;
                C15250mo r15 = r0.A00;
                r15.A04 = (AbstractC14640lm) obj;
                r0.A02.A0B(r15);
            }
        });
        r1.A0D(r7, new AnonymousClass02B() { // from class: X.2W9
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                C51562Vi r0 = C51562Vi.this;
                C15250mo r15 = r0.A00;
                r15.A06 = (C49662Lr) obj;
                r0.A02.A0B(r15);
            }
        });
    }

    public AnonymousClass017 A00() {
        return this.A01;
    }

    public AnonymousClass017 A01() {
        return this.A03;
    }

    public void A02() {
        this.A01.A0B(new ArrayList());
    }

    public void A03() {
        this.A02.A0A(this.A00);
    }
}
