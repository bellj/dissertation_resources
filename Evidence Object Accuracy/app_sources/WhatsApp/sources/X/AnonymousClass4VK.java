package X;

import java.util.List;

/* renamed from: X.4VK  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4VK {
    public final AnonymousClass4SX A00;
    public final List A01;

    public AnonymousClass4VK(AnonymousClass4SX r1, List list) {
        this.A00 = r1;
        this.A01 = list;
    }

    public String toString() {
        StringBuilder A0h = C12960it.A0h();
        for (AnonymousClass4SX r0 : this.A01) {
            A0h.append(r0.A01);
            A0h.append(",");
        }
        Object[] A1a = C12980iv.A1a();
        A1a[0] = this.A00.A01;
        A1a[1] = A0h;
        return String.format("CategoryResponse{responseCategoryId=%s, subCategories=%s}", A1a);
    }
}
