package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

/* renamed from: X.1y7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C44051y7 implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C99944l7();
    public int A00;
    public int A01;
    public int A02;
    public String A03;
    public String A04;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C44051y7() {
    }

    public /* synthetic */ C44051y7(Parcel parcel) {
        if (parcel != null) {
            this.A04 = parcel.readString();
            this.A03 = parcel.readString();
            this.A01 = parcel.readInt();
            this.A02 = parcel.readInt();
            this.A00 = parcel.readInt();
        }
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C44051y7)) {
            return false;
        }
        C44051y7 r4 = (C44051y7) obj;
        if (r4.A04.equals(this.A04) && r4.A03.equals(this.A03) && r4.A01 == this.A01 && r4.A02 == this.A02 && r4.A00 == this.A00) {
            return true;
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A04, this.A03, Integer.valueOf(this.A01), Integer.valueOf(this.A02), Integer.valueOf(this.A00)});
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A04);
        parcel.writeString(this.A03);
        parcel.writeInt(this.A01);
        parcel.writeInt(this.A02);
        parcel.writeInt(this.A00);
    }
}
