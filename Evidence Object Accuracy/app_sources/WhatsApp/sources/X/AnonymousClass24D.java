package X;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.emoji.EmojiDescriptor;
import com.whatsapp.util.Log;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import java.util.Random;

/* renamed from: X.24D  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass24D {
    public static final Random A00 = new Random();
    public static final int[] A01 = {-5886863, -7296959, -4087745, -8838856, -5339276, -1002704, -4803801, -3760180, -7640688, -30068, -11222427, -33941, -14236452, -11023873, -9148566, -8482653, -11102465, -9558658, -8729691, -14404032, -8219446};
    public static final int[] A02 = {0, 1, 2, 3, 5};

    public static int A00(int i, int i2, boolean z) {
        if (z) {
            if (i < 50) {
                return 24;
            }
            if (i < 150) {
                if (i2 < 640) {
                    return 16;
                }
            } else if (i2 < 560) {
                return 14;
            } else {
                if (i2 < 640) {
                    return 16;
                }
            }
            return 19;
        } else if (i < 50) {
            return 32;
        } else {
            return i < 150 ? 24 : 16;
        }
    }

    public static int A01(CharSequence charSequence) {
        int i = 0;
        int A022 = A02(charSequence, 0, charSequence.length());
        ArrayList A04 = C33771f3.A04(charSequence.toString());
        if (A04 == null) {
            return A022;
        }
        Iterator it = A04.iterator();
        while (it.hasNext()) {
            String str = (String) it.next();
            i += str.length() - A05(str).length();
        }
        return A022 - i;
    }

    public static int A02(CharSequence charSequence, int i, int i2) {
        StringBuilder sb = new StringBuilder();
        while (i < i2) {
            if (charSequence.charAt(i) != '\n') {
                sb.append(charSequence.charAt(i));
            }
            i++;
        }
        return AnonymousClass2VC.A00(sb.toString());
    }

    public static Typeface A03(Context context, int i) {
        if (i == 1) {
            return Typeface.SERIF;
        }
        if (i == 2) {
            return AnonymousClass33L.A03(context);
        }
        if (i == 3) {
            Typeface typeface = AnonymousClass33L.A0B;
            if (typeface != null) {
                return typeface;
            }
            Typeface createFromAsset = Typeface.createFromAsset(context.getAssets(), "fonts/Bryndan-Write.ttf");
            AnonymousClass33L.A0B = createFromAsset;
            return createFromAsset;
        } else if (i != 5) {
            return Typeface.SANS_SERIF;
        } else {
            return AnonymousClass33L.A02(context);
        }
    }

    public static CharSequence A04(CharSequence charSequence, int i, int i2, int i3, int i4) {
        StringBuilder sb = new StringBuilder();
        for (int i5 = i; i5 < i2 && i3 >= 0 && i4 > 0; i5++) {
            sb.append(charSequence.charAt(i5));
            if (charSequence.charAt(i5) == '\n') {
                i3--;
            } else {
                i4--;
            }
        }
        if (!(sb.charAt(sb.length() - 1) != '\n' || charSequence.charAt(i) == '\n' || i == i2 - 1)) {
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb.toString();
    }

    public static String A05(String str) {
        int i = 0;
        try {
            String host = new URL(str).getHost();
            int indexOf = str.indexOf(host);
            if (indexOf < 0) {
                StringBuilder sb = new StringBuilder("cannot find host ");
                sb.append(host);
                sb.append(" in ");
                sb.append(str);
                Log.e(sb.toString());
                return str.substring(0, Math.min(34, str.length()));
            }
            String substring = str.substring(indexOf + host.length());
            if (host.toLowerCase(Locale.US).startsWith("www.")) {
                host = host.substring(4);
            }
            int length = substring.length();
            if (length > 12 || host.length() + length > 34) {
                int min = Math.min(length, Math.max(length - 12, (host.length() + length) - 34));
                substring = substring.substring(0, length - min);
                i = min;
            }
            if (substring.length() == 1) {
                substring = "";
            }
            StringBuilder sb2 = new StringBuilder();
            sb2.append(host);
            sb2.append(substring);
            String obj = sb2.toString();
            int length2 = obj.length();
            if (length2 > 34) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append("…");
                sb3.append(obj.substring(length2 - 34));
                obj = sb3.toString();
            }
            if (i <= 0) {
                return obj;
            }
            StringBuilder sb4 = new StringBuilder();
            sb4.append(obj);
            sb4.append("…");
            return sb4.toString();
        } catch (MalformedURLException e) {
            StringBuilder sb5 = new StringBuilder("unvalid url ");
            sb5.append(str);
            Log.e(sb5.toString(), e);
            return str.substring(0, Math.min(34, str.length()));
        }
    }

    public static String A06(String str) {
        C32681cY r5 = new C32681cY(str);
        int i = 0;
        int i2 = 0;
        while (i < str.length()) {
            if (str.charAt(i) == '\n') {
                i2 += 50;
            } else {
                i2++;
            }
            if (i2 > 700) {
                break;
            }
            r5.A00 = i;
            i += r5.A01(i, EmojiDescriptor.A00(r5, false));
        }
        return str.substring(0, i);
    }

    public static void A07(Context context, View view) {
        int i;
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (((float) context.getResources().getDisplayMetrics().widthPixels) > context.getResources().getDimension(R.dimen.status_text_composer_small_link_preview_max_width) + (context.getResources().getDimension(R.dimen.status_text_composer_input_margin_small) * 2.0f)) {
            i = (int) context.getResources().getDimension(R.dimen.status_text_composer_small_link_preview_max_width);
        } else {
            i = -1;
        }
        layoutParams.width = i;
        view.setLayoutParams(layoutParams);
        view.requestLayout();
    }

    public static void A08(TextView textView, int i) {
        if (i < 150) {
            textView.setGravity(17);
            if (C28391Mz.A00()) {
                textView.setTextAlignment(4);
                return;
            }
            return;
        }
        textView.setGravity(16);
        if (C28391Mz.A00()) {
            textView.setTextAlignment(5);
            textView.setTextDirection(5);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x001c, code lost:
        if (r0.A00 <= 0) goto L_0x001e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A09(X.C14320lF r6, int r7) {
        /*
            r4 = 0
            if (r6 == 0) goto L_0x0044
            byte[] r1 = r6.A0I
            if (r1 == 0) goto L_0x0044
            android.graphics.Bitmap r5 = r6.A05
            if (r5 != 0) goto L_0x0015
            int r0 = r1.length     // Catch: OutOfMemoryError -> 0x0044
            android.graphics.Bitmap r5 = android.graphics.BitmapFactory.decodeByteArray(r1, r4, r0)     // Catch: OutOfMemoryError -> 0x0044
            r6.A05 = r5     // Catch: OutOfMemoryError -> 0x0044
            if (r5 != 0) goto L_0x0015
            return r4
        L_0x0015:
            X.2V4 r0 = r6.A06
            if (r0 == 0) goto L_0x001e
            int r0 = r0.A00
            r3 = 1
            if (r0 > 0) goto L_0x001f
        L_0x001e:
            r3 = 0
        L_0x001f:
            r0 = 150(0x96, float:2.1E-43)
            r2 = 0
            if (r7 < r0) goto L_0x0025
            r2 = 1
        L_0x0025:
            byte[] r1 = r6.A0I
            r0 = 0
            if (r1 != 0) goto L_0x002b
            r0 = 1
        L_0x002b:
            if (r3 != 0) goto L_0x0044
            if (r2 != 0) goto L_0x0044
            if (r0 != 0) goto L_0x0044
            int r1 = r5.getWidth()
            int r0 = r5.getHeight()
            float r1 = (float) r1
            float r0 = (float) r0
            float r1 = r1 / r0
            r0 = 1068708659(0x3fb33333, float:1.4)
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 <= 0) goto L_0x0044
            r4 = 1
        L_0x0044:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass24D.A09(X.0lF, int):boolean");
    }
}
