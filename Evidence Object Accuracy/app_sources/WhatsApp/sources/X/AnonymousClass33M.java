package X;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Picture;
import android.graphics.RectF;
import com.whatsapp.R;
import java.util.Calendar;
import org.json.JSONObject;

/* renamed from: X.33M  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass33M extends AnonymousClass33A {
    public int A00;
    public int A01;
    public Paint A02;
    public Paint A03;
    public Picture A04;
    public Picture A05;
    public C91474Rw A06;
    public C91474Rw A07;
    public C91474Rw A08;
    public C91474Rw A09;
    public Boolean A0A;
    public final RectF A0B;
    public final AnonymousClass018 A0C;
    public final AbstractC92674Wx A0D;
    public final AnonymousClass3C8 A0E;
    public final AnonymousClass3EU A0F;
    public final boolean A0G;

    @Override // X.AbstractC454821u
    public boolean A0B() {
        return true;
    }

    @Override // X.AbstractC454821u
    public String A0G() {
        return "analog-clock";
    }

    @Override // X.AbstractC454821u
    public boolean A0J() {
        return false;
    }

    public AnonymousClass33M(Context context, AnonymousClass018 r3, JSONObject jSONObject) {
        this(context, r3, false);
        this.A00 = jSONObject.getInt("hour");
        this.A01 = jSONObject.getInt("minute");
        this.A0A = Boolean.valueOf(jSONObject.getBoolean("theme"));
        super.A0A(jSONObject);
    }

    public AnonymousClass33M(Context context, AnonymousClass018 r11, boolean z) {
        super(context);
        this.A0B = C12980iv.A0K();
        this.A02 = C12990iw.A0G(1);
        this.A03 = C12990iw.A0G(1);
        this.A0D = new AnonymousClass45K(this);
        this.A0C = r11;
        this.A0G = z;
        this.A0A = Boolean.FALSE;
        Calendar instance = Calendar.getInstance(C12970iu.A14(r11));
        this.A00 = instance.get(10);
        this.A01 = instance.get(12);
        Context context2 = ((AnonymousClass33A) this).A00;
        this.A04 = AnonymousClass33A.A02(context2, "clockDarkTheme.svg");
        Paint paint = this.A02;
        paint.setColor(Color.parseColor("#ECB439"));
        this.A06 = new C91474Rw(paint, 190.0f, 249.0f, 398.0f, 263.0f, 7.0f, 7.0f);
        this.A07 = new C91474Rw(paint, 185.0f, 251.0f, 479.0f, 261.0f, 5.0f, 5.0f);
        this.A05 = AnonymousClass33A.A02(context2, "clockLightTheme.svg");
        Paint paint2 = this.A03;
        paint2.setColor(Color.parseColor("#DC5842"));
        this.A08 = new C91474Rw(paint2, 201.0f, 248.0f, 370.0f, 264.0f, 8.0f, 8.0f);
        this.A09 = new C91474Rw(paint2, 185.0f, 251.0f, 479.0f, 262.0f, 5.5f, 5.5f);
        this.A0F = new AnonymousClass3EU(context, r11);
        this.A0E = new AnonymousClass3C8();
    }

    @Override // X.AbstractC454821u
    public void A05() {
        this.A0F.A00 = false;
    }

    @Override // X.AbstractC454821u
    public void A08(float f, int i) {
        A07(f, 2);
        this.A0F.A00(f);
    }

    @Override // X.AbstractC454821u
    public boolean A0C() {
        return this.A0D.A01;
    }

    @Override // X.AbstractC454821u
    public boolean A0D() {
        int i = this.A01;
        int i2 = this.A00;
        Calendar instance = Calendar.getInstance(C12970iu.A14(this.A0C));
        this.A00 = instance.get(10);
        int i3 = instance.get(12);
        this.A01 = i3;
        return (i == i3 && i2 == this.A00) ? false : true;
    }

    @Override // X.AbstractC454821u
    public boolean A0E() {
        this.A0E.A00(this.A0D);
        return true;
    }

    @Override // X.AbstractC454821u
    public String A0H(Context context) {
        return context.getString(R.string.doodle_item_analog_clock);
    }

    @Override // X.AbstractC454821u
    public void A0I(Canvas canvas) {
        A0P(canvas);
    }

    @Override // X.AbstractC454821u
    public void A0N(JSONObject jSONObject) {
        super.A0N(jSONObject);
        jSONObject.put("hour", this.A00);
        jSONObject.put("minute", this.A01);
        jSONObject.put("theme", this.A0A);
    }

    @Override // X.AbstractC454821u
    public void A0P(Canvas canvas) {
        Picture picture;
        Paint paint;
        C91474Rw r11;
        C91474Rw r3;
        boolean z = this.A0G;
        if (!z) {
            canvas.save();
            RectF rectF = super.A02;
            canvas.scale(0.67f, 0.67f, rectF.centerX(), rectF.centerY());
        }
        AbstractC92674Wx r1 = this.A0D;
        float A00 = r1.A00();
        boolean booleanValue = this.A0A.booleanValue();
        if (r1.A01 && r1.A00 >= 0.0f) {
            booleanValue = !booleanValue;
        }
        if (booleanValue) {
            picture = this.A04;
        } else {
            picture = this.A05;
        }
        RectF rectF2 = super.A02;
        rectF2.sort();
        canvas.save();
        C12990iw.A12(canvas, rectF2, ((AbstractC454821u) this).A00);
        canvas.scale(rectF2.width() / ((float) picture.getHeight()), rectF2.height() / ((float) picture.getWidth()), rectF2.left, rectF2.top);
        canvas.translate(rectF2.left, rectF2.top);
        canvas.scale(A00, A00, (float) (picture.getWidth() >> 1), (float) (picture.getHeight() >> 1));
        canvas.save();
        canvas.drawPicture(picture);
        canvas.restore();
        float width = (float) (picture.getWidth() >> 1);
        float height = (float) (picture.getHeight() >> 1);
        if (booleanValue) {
            paint = this.A02;
        } else {
            paint = this.A03;
        }
        canvas.drawCircle(width, height, 26.0f, paint);
        canvas.save();
        if (booleanValue) {
            r11 = this.A06;
        } else {
            r11 = this.A08;
        }
        double d = (((((double) (this.A00 + 9)) % 12.0d) / 12.0d) * 360.0d) + (((double) (this.A01 * 30)) / 60.0d);
        RectF rectF3 = this.A0B;
        rectF3.set(r11.A03);
        canvas.rotate((float) ((int) d), (float) (picture.getWidth() >> 1), (float) (picture.getHeight() >> 1));
        canvas.drawRoundRect(rectF3, r11.A00, r11.A01, r11.A02);
        canvas.restore();
        canvas.save();
        if (booleanValue) {
            r3 = this.A07;
        } else {
            r3 = this.A09;
        }
        rectF3.set(r3.A03);
        canvas.rotate((float) ((int) (((((double) (this.A01 + 45)) % 60.0d) / 60.0d) * 360.0d)), (float) (picture.getWidth() >> 1), (float) (picture.getHeight() >> 1));
        canvas.drawRoundRect(rectF3, r3.A00, r3.A01, r3.A02);
        canvas.restore();
        canvas.restore();
        if (!z) {
            canvas.restore();
            float width2 = (rectF2.width() * 0.67f) / 2.0f;
            this.A0F.A01(canvas, new RectF(rectF2.centerX() - width2, rectF2.centerY() - width2, rectF2.centerX() + width2, rectF2.centerY() + width2), ((AbstractC454821u) this).A00);
        }
    }

    @Override // X.AnonymousClass45J, X.AbstractC454821u
    public void A0Q(RectF rectF, float f, float f2, float f3, float f4) {
        super.A0Q(rectF, f, f2, f3, f4);
        this.A0F.A00(rectF.width() / 1020.0f);
    }
}
