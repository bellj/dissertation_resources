package X;

import com.whatsapp.gallerypicker.GalleryPickerFragment;
import java.lang.ref.WeakReference;

/* renamed from: X.383  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass383 extends AbstractC16350or {
    public final int A00;
    public final int A01;
    public final C16590pI A02;
    public final AnonymousClass018 A03;
    public final AnonymousClass2FO A04;
    public final C19390u2 A05;
    public final WeakReference A06;

    public AnonymousClass383(C16590pI r2, AnonymousClass018 r3, GalleryPickerFragment galleryPickerFragment, AnonymousClass2FO r5, C19390u2 r6, int i, int i2) {
        this.A02 = r2;
        this.A04 = r5;
        this.A03 = r3;
        this.A05 = r6;
        this.A06 = C12970iu.A10(galleryPickerFragment);
        this.A01 = i;
        this.A00 = i2;
    }

    public static AbstractC35581iK A00(AnonymousClass2FO r2, String str, int i) {
        AnonymousClass2JH r0 = new AnonymousClass2JH();
        r0.A01 = 2;
        r0.A00 = i;
        r0.A02 = 2;
        r0.A03 = str;
        return r2.A00(r0);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x008b, code lost:
        if (r2.intValue() == r4.getCount()) goto L_0x008d;
     */
    @Override // X.AbstractC16350or
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ java.lang.Object A05(java.lang.Object[] r23) {
        /*
        // Method dump skipped, instructions count: 435
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass383.A05(java.lang.Object[]):java.lang.Object");
    }
}
