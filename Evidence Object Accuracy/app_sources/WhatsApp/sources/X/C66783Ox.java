package X;

import com.whatsapp.documentpicker.DocumentPickerActivity;

/* renamed from: X.3Ox  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C66783Ox implements AnonymousClass07L {
    public final /* synthetic */ DocumentPickerActivity A00;

    @Override // X.AnonymousClass07L
    public boolean AUY(String str) {
        return false;
    }

    public C66783Ox(DocumentPickerActivity documentPickerActivity) {
        this.A00 = documentPickerActivity;
    }

    @Override // X.AnonymousClass07L
    public boolean AUX(String str) {
        DocumentPickerActivity documentPickerActivity = this.A00;
        documentPickerActivity.A0G = str;
        documentPickerActivity.A0H = C32751cg.A02(documentPickerActivity.A0A, str);
        documentPickerActivity.A0B.getFilter().filter(documentPickerActivity.A0G);
        return false;
    }
}
