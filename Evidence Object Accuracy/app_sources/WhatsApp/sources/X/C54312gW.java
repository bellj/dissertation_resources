package X;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.WaImageButton;
import com.whatsapp.WaTextView;
import java.util.List;

/* renamed from: X.2gW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54312gW extends AnonymousClass02M {
    public List A00;
    public final AnonymousClass4J7 A01;

    public C54312gW(AnonymousClass4J7 r1) {
        this.A01 = r1;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A00.size();
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r10, int i) {
        AbstractC55182hv r102 = (AbstractC55182hv) r10;
        Object obj = this.A00.get(i);
        if (!(r102 instanceof C60062vp)) {
            C60072vq r103 = (C60072vq) r102;
            AnonymousClass40I r2 = (AnonymousClass40I) obj;
            WaTextView waTextView = ((AbstractC55182hv) r103).A01;
            AnonymousClass2x6 r1 = r2.A00;
            waTextView.setText(((C30211Wn) r1).A01);
            r103.A08(Color.parseColor(r1.A00));
            AnonymousClass1CN r4 = r103.A00;
            String str = r1.A01;
            WaImageButton waImageButton = ((AbstractC55182hv) r103).A00;
            r4.A00.A00(null, AnonymousClass2GE.A01(waImageButton.getContext(), R.drawable.ic_business_category, R.color.icon_secondary), waImageButton, null, str);
            View view = r103.A0H;
            AnonymousClass2GE.A05(view.getContext(), waImageButton, R.color.white);
            AbstractView$OnClickListenerC34281fs.A02(view, r103, r2, 20);
            AbstractView$OnClickListenerC34281fs.A00(waImageButton, r103, 28);
            return;
        }
        View view2 = r102.A0H;
        Context context = view2.getContext();
        C12970iu.A19(context, r102.A01, R.string.biz_dir_view_all);
        r102.A08(AnonymousClass00T.A00(context, R.color.searchChipBackground));
        WaImageButton waImageButton2 = r102.A00;
        C12990iw.A0x(context, waImageButton2, R.drawable.chevron);
        AnonymousClass2GE.A05(context, waImageButton2, R.color.secondary_text);
        AbstractView$OnClickListenerC34281fs.A02(view2, r102, obj, 21);
        AbstractView$OnClickListenerC34281fs.A00(waImageButton2, r102, 29);
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        View A0F = C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.popular_categories_cell);
        if (i == 1) {
            return new C60072vq(A0F, (AnonymousClass1CN) this.A01.A00.A04.A5v.get());
        }
        if (i == 2) {
            return new C60062vp(A0F);
        }
        throw C12960it.A0U(C12960it.A0W(i, "PopularCategoriesAdapter/onCreateViewHolder unhandled view type: "));
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        return ((AnonymousClass4N9) this.A00.get(i)).A00;
    }
}
