package X;

import android.content.Context;

/* renamed from: X.1LD  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1LD {
    public static final String A04 = String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY, %s TEXT NOT NULL, %s INTEGER DEFAULT 0);", "queue", "_id", "item", "encrypted");
    public final Context A00;
    public final AnonymousClass2C4 A01;
    public final AnonymousClass1LH A02;
    public final AnonymousClass1LG A03;

    public AnonymousClass1LD(Context context, AnonymousClass2C4 r5, AnonymousClass1LH r6) {
        StringBuilder sb = new StringBuilder("_jobqueue-");
        sb.append("WhatsAppJobManager");
        this.A03 = new AnonymousClass1LG(context, sb.toString());
        this.A00 = context;
        this.A01 = r5;
        this.A02 = r6;
    }
}
