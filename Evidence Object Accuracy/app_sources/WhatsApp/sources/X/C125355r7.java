package X;

import com.whatsapp.R;

/* renamed from: X.5r7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C125355r7 {
    public static final int[] A00 = {R.attr.pinAnimationType, R.attr.pinBackgroundDrawable, R.attr.pinBackgroundIsSquare, R.attr.pinCharacterMask, R.attr.pinCharacterSize, R.attr.pinCharacterSpacing, R.attr.pinFontSize, R.attr.pinLineColors, R.attr.pinLineStroke, R.attr.pinLineStrokeCentered, R.attr.pinLineStrokeSelected, R.attr.pinRepeatedHint, R.attr.pinTextBottomPadding};
    public static final int[] A01 = {R.attr.formActionOnTop, R.attr.formInputLength, R.attr.formTitle, R.attr.formValidationError};
    public static final int[] A02 = {R.attr.key_digit_color, R.attr.key_digit_height, R.attr.key_digit_size, R.attr.keypad_bg_color};
}
