package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.1ZM  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1ZM implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C100354lm();
    public final String A00;
    public final String A01;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass1ZM(Parcel parcel) {
        this.A00 = parcel.readString();
        this.A01 = parcel.readString();
    }

    public AnonymousClass1ZM(String str, String str2) {
        this.A00 = str;
        this.A01 = str2;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A00);
        parcel.writeString(this.A01);
    }
}
