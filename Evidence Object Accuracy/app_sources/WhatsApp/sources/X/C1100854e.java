package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.54e  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1100854e implements AnonymousClass5W1 {
    public C64503Fu A00;

    @Override // X.AnonymousClass5W1
    public int ADd() {
        return 2;
    }

    public C1100854e(C64503Fu r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5W1
    public UserJid ADh() {
        C15370n3 A02 = this.A00.A02();
        if (A02 == null) {
            return null;
        }
        return (UserJid) A02.A0B(UserJid.class);
    }
}
