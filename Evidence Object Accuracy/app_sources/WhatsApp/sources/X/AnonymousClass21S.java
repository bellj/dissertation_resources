package X;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import com.facebook.redex.RunnableBRunnable0Shape13S0100000_I0_13;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import com.whatsapp.videoplayback.ExoPlaybackControlView;

/* renamed from: X.21S  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass21S extends AnonymousClass21T {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public long A05;
    public AudioManager.OnAudioFocusChangeListener A06;
    public Uri A07;
    public C47492Ax A08;
    public C77353n9 A09;
    public AbstractC47452At A0A;
    public C47442As A0B;
    public ExoPlaybackControlView A0C;
    public AnonymousClass2B2 A0D;
    public boolean A0E;
    public boolean A0F;
    public boolean A0G;
    public boolean A0H;
    public boolean A0I;
    public boolean A0J;
    public boolean A0K;
    public boolean A0L;
    public boolean A0M;
    public boolean A0N;
    public boolean A0O;
    public boolean A0P;
    public final Activity A0Q;
    public final Handler A0R;
    public final AnonymousClass5XZ A0S;
    public final C14900mE A0T;
    public final AnonymousClass01d A0U;
    public final AnonymousClass018 A0V;
    public final AbstractC14440lR A0W;
    public final AnonymousClass2B1 A0X;
    public final C47482Aw A0Y;

    public AnonymousClass21S(Activity activity, C14900mE r4, AnonymousClass01d r5, AnonymousClass018 r6, AbstractC14440lR r7, AnonymousClass2B1 r8, AnonymousClass2B2 r9, boolean z, boolean z2) {
        this.A0R = new Handler(Looper.getMainLooper());
        this.A07 = new Uri.Builder().build();
        this.A00 = Integer.MAX_VALUE;
        this.A04 = 5;
        this.A03 = -1;
        this.A02 = 0;
        this.A0S = new C67633Sg(this);
        this.A0T = r4;
        this.A0W = r7;
        this.A0U = r5;
        this.A0V = r6;
        this.A0Q = activity;
        C47482Aw r0 = new C47482Aw(activity, z2);
        this.A0Y = r0;
        r0.setLayoutResizingEnabled(z);
        this.A0X = r8;
        this.A0D = r9;
    }

    public AnonymousClass21S(Activity activity, Uri uri, C14900mE r15, AnonymousClass01d r16, AnonymousClass018 r17, AbstractC14440lR r18, AbstractC107844y2 r19, AnonymousClass2B2 r20) {
        this(activity, r15, r16, r17, r18, null, r20, true, false);
        this.A07 = uri;
        r19.A00 = new C47522Bb(this);
        this.A0A = r19;
    }

    public final AnonymousClass2CD A0E() {
        Uri uri = this.A07;
        AbstractC47452At r3 = this.A0A;
        if (r3 == null) {
            Activity activity = this.A0Q;
            r3 = new C107874y5(activity, AnonymousClass3JZ.A09(activity, activity.getString(R.string.app_name)));
            this.A0A = r3;
        }
        C56042kE r2 = new C56042kE(uri, C106974wa.A0M, r3);
        return this.A0I ? new C56022kC(r2, this.A00) : r2;
    }

    public void A0F() {
        hashCode();
        if (this.A08 == null) {
            ExoPlaybackControlView exoPlaybackControlView = this.A0C;
            if (exoPlaybackControlView != null) {
                if ((this.A0Q.getWindow().getDecorView().getSystemUiVisibility() & 4) == 0) {
                    exoPlaybackControlView.A0F.setVisibility(0);
                    if (exoPlaybackControlView.A09) {
                        exoPlaybackControlView.A0E.setVisibility(0);
                    }
                    exoPlaybackControlView.A04();
                    exoPlaybackControlView.A03();
                    exoPlaybackControlView.A05();
                } else {
                    exoPlaybackControlView.A02();
                }
            }
            A0H();
            this.A0G = true;
            if (this.A0O) {
                C47492Ax r1 = this.A08;
                if (r1 != null) {
                    r1.AcW(true);
                    ExoPlaybackControlView exoPlaybackControlView2 = this.A0C;
                    if (exoPlaybackControlView2 != null) {
                        exoPlaybackControlView2.A03 = null;
                        exoPlaybackControlView2.A04 = new AbstractC116315Uy() { // from class: X.5AV
                            @Override // X.AbstractC116315Uy
                            public final void AWO() {
                                AnonymousClass21S.this.A02++;
                            }
                        };
                    }
                    this.A0T.A0H(new RunnableBRunnable0Shape13S0100000_I0_13(this, 21));
                }
            } else if (this.A0C == null) {
                AnonymousClass2B2 r0 = this.A0D;
                if (r0 != null) {
                    r0.A00();
                }
                this.A08.A08(A0E(), true);
            } else {
                C47492Ax r12 = this.A08;
                AnonymousClass009.A05(r12);
                r12.AcW(false);
                ExoPlaybackControlView exoPlaybackControlView3 = this.A0C;
                if (exoPlaybackControlView3 != null) {
                    exoPlaybackControlView3.A03 = new AnonymousClass5AT(this);
                    exoPlaybackControlView3.A04 = new AbstractC116315Uy() { // from class: X.5AU
                        @Override // X.AbstractC116315Uy
                        public final void AWO() {
                            AnonymousClass21S r13 = AnonymousClass21S.this;
                            r13.A0J(r13.A0E());
                        }
                    };
                }
            }
        }
    }

    public void A0G() {
        boolean z;
        C47492Ax r0 = this.A08;
        if (r0 != null) {
            if (r0.AFl() == 1) {
                z = false;
                this.A0M = false;
            } else {
                this.A0M = true;
                z = false;
                this.A08.A0A(false);
            }
            this.A0N = z;
            this.A0E = z;
            this.A0L = z;
            this.A0K = z;
            AnonymousClass2B2 r02 = this.A0D;
            if (r02 != null) {
                r02.A00();
            }
            this.A08.A08(A0E(), true);
            this.A0G = true;
        }
    }

    public final void A0H() {
        C47492Ax A00;
        boolean z;
        if (this.A08 == null) {
            C47482Aw r2 = this.A0Y;
            this.A09 = new C77353n9(r2.getContext());
            AnonymousClass2B1 r0 = this.A0X;
            if (r0 != null) {
                Context context = r2.getContext();
                C77353n9 r4 = this.A09;
                C47432Ar r3 = r0.A00;
                int i = r3.A00;
                if (i < C47432Ar.A08) {
                    r3.A00 = i + 1;
                    z = true;
                } else {
                    z = false;
                }
                C107834y1 r8 = new C107834y1(32768);
                C106404ve.A00("bufferForPlaybackMs", "0", 100, 0);
                C106404ve.A00("bufferForPlaybackAfterRebufferMs", "0", 100, 0);
                C106404ve.A00("minBufferMs", "bufferForPlaybackMs", 700, 100);
                C106404ve.A00("minBufferMs", "bufferForPlaybackAfterRebufferMs", 700, 100);
                C106404ve.A00("maxBufferMs", "minBufferMs", 1000, 700);
                A00 = AnonymousClass3A4.A00(context, new C106404ve(r8, 700, 1000, 100, 100), new C106474vl(context, z), r4);
            } else {
                Context context2 = r2.getContext();
                C106464vk r5 = new C106464vk(r2.getContext());
                C77353n9 r42 = this.A09;
                C107834y1 r82 = new C107834y1(32768);
                C106404ve.A00("bufferForPlaybackMs", "0", 1000, 0);
                C106404ve.A00("bufferForPlaybackAfterRebufferMs", "0", 1000, 0);
                C106404ve.A00("minBufferMs", "bufferForPlaybackMs", 1000, 1000);
                C106404ve.A00("minBufferMs", "bufferForPlaybackAfterRebufferMs", 1000, 1000);
                C106404ve.A00("maxBufferMs", "minBufferMs", 2000, 1000);
                A00 = AnonymousClass3A4.A00(context2, new C106404ve(r82, 1000, 2000, 1000, 1000), r5, r42);
            }
            this.A08 = A00;
            float f = 1.0f;
            if (this.A0J) {
                f = 0.0f;
            }
            A00.A04(f);
            this.A08.A5h(this.A0S);
            r2.setPlayer(this.A08);
            if (this.A0P) {
                long j = this.A05;
                int i2 = (j > -9223372036854775807L ? 1 : (j == -9223372036854775807L ? 0 : -1));
                C47492Ax r1 = this.A08;
                int i3 = this.A01;
                if (i2 == 0) {
                    r1.AbS(i3, -9223372036854775807L);
                } else {
                    r1.AbS(i3, j);
                }
            } else {
                int i4 = this.A03;
                if (i4 >= 0) {
                    C47492Ax r32 = this.A08;
                    r32.AbS(r32.ACF(), (long) i4);
                    this.A03 = -1;
                }
            }
        }
    }

    public final void A0I() {
        AudioManager A0G;
        if (!this.A0F && (A0G = this.A0U.A0G()) != null) {
            AudioManager.OnAudioFocusChangeListener onAudioFocusChangeListener = this.A06;
            if (onAudioFocusChangeListener == null) {
                onAudioFocusChangeListener = new C97974hw();
                this.A06 = onAudioFocusChangeListener;
            }
            A0G.requestAudioFocus(onAudioFocusChangeListener, 3, 2);
        }
    }

    public final void A0J(AnonymousClass2CD r3) {
        ExoPlaybackControlView exoPlaybackControlView = this.A0C;
        if (exoPlaybackControlView != null) {
            exoPlaybackControlView.A03 = null;
            exoPlaybackControlView.A04 = null;
        }
        A0H();
        AnonymousClass2B2 r0 = this.A0D;
        if (r0 != null) {
            r0.A00();
        }
        C47492Ax r02 = this.A08;
        if (r02 != null && r02.AFl() == 1) {
            this.A08.A08(r3, true);
        }
        A0I();
    }

    public void A0K(String str, boolean z) {
        StringBuilder sb = new StringBuilder("ExoPlayerVideoPlayer/onError=");
        sb.append(str);
        Log.e(sb.toString());
        AnonymousClass5V3 r0 = super.A02;
        if (r0 != null) {
            r0.APt(str, z);
        }
        AnonymousClass2B2 r1 = this.A0D;
        if (r1 != null && !(r1 instanceof AnonymousClass39C)) {
            ((AnonymousClass39D) r1).A0A.A00();
        }
    }
}
