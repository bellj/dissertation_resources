package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.facebook.redex.IDxObserverShape3S0100000_1_I1;
import com.whatsapp.R;
import com.whatsapp.WaRoundCornerImageView;
import com.whatsapp.WaTextView;
import com.whatsapp.calling.callgrid.viewmodel.CallGridViewModel;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.VideoPort;
import java.util.Map;

/* renamed from: X.2vx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60112vx extends AbstractC55202hx {
    public int A00;
    public Drawable A01;
    public WaRoundCornerImageView A02;
    public AnonymousClass3CK A03;
    public AnonymousClass2ZX A04;
    public AbstractC116665Wi A05;
    public VideoPort A06;
    public String A07;
    public final int A08;
    public final View A09;
    public final View A0A;
    public final View A0B;
    public final ViewGroup A0C;
    public final FrameLayout A0D;
    public final ImageView A0E;
    public final ConstraintLayout A0F;
    public final WaRoundCornerImageView A0G;
    public final WaTextView A0H;
    public final AnonymousClass1CP A0I;
    public final AnonymousClass5X3 A0J = new C70943c8(this);

    public C60112vx(View view, C18720su r11, C89374Js r12, CallGridViewModel callGridViewModel, AnonymousClass1CP r14, AnonymousClass130 r15, C15610nY r16, boolean z) {
        super(view, r11, r12, callGridViewModel, r15, r16);
        this.A0A = AnonymousClass028.A0D(view, R.id.mute_image);
        this.A09 = AnonymousClass028.A0D(view, R.id.dark_overlay);
        this.A0E = C12970iu.A0K(view, R.id.frame_overlay);
        this.A0F = (ConstraintLayout) AnonymousClass028.A0D(view, R.id.video_container);
        ViewGroup A0P = C12980iv.A0P(view, R.id.video_status_container);
        this.A0C = A0P;
        this.A0D = (FrameLayout) AnonymousClass028.A0D(view, R.id.loading_state);
        this.A0G = (WaRoundCornerImageView) AnonymousClass028.A0D(view, R.id.call_grid_blur_background);
        this.A02 = (WaRoundCornerImageView) view.findViewById(R.id.gradient_overlay);
        this.A0I = r14;
        this.A0H = A0P != null ? (WaTextView) A0P.findViewById(R.id.status) : null;
        this.A0B = AnonymousClass028.A0D(view, z ? R.id.texture_view : R.id.surface_view);
        this.A00 = view.getResources().getDimensionPixelSize(R.dimen.call_grid_border_radius);
        ((AbstractC55202hx) this).A00 = view.getResources().getDimensionPixelSize(R.dimen.call_grid_border_width_for_speaker);
        int color = C12960it.A09(view).getColor(R.color.black);
        this.A08 = color;
        AnonymousClass2ZX r3 = new AnonymousClass2ZX(this.A00, color);
        this.A04 = r3;
        View view2 = super.A0H;
        AnonymousClass009.A0A("FrameLayout required as root to support corner rounding via overlay", view2 instanceof FrameLayout);
        ((FrameLayout) view2).setForeground(r3);
    }

    @Override // X.AbstractC55202hx
    public void A0G(C64363Fg r10) {
        String str;
        WaTextView waTextView;
        int dimensionPixelSize;
        String A04;
        if (this.A06 == null) {
            this.A06 = this.A0I.A00(this.A0B);
        }
        if (r10.A0G) {
            str = "preview";
        } else {
            str = "display";
        }
        this.A07 = str;
        if (A07() && !((AbstractC55202hx) this).A05.A0S.equals(r10.A0S)) {
            StringBuilder A0h = C12960it.A0h();
            A0h.append(this.A07);
            Log.w(C12960it.A0d("bind() called with new participant before unbind()", A0h));
            A09();
        }
        CallGridViewModel callGridViewModel = ((AbstractC55202hx) this).A04;
        int i = 8;
        if (callGridViewModel != null && !A07()) {
            IDxObserverShape3S0100000_1_I1 iDxObserverShape3S0100000_1_I1 = new IDxObserverShape3S0100000_1_I1(this, 44);
            ((AbstractC55202hx) this).A03 = iDxObserverShape3S0100000_1_I1;
            AnonymousClass3CZ r6 = callGridViewModel.A0E;
            UserJid userJid = r10.A0S;
            Map map = r6.A01;
            if (!map.containsKey(userJid)) {
                map.put(userJid, null);
            }
            r6.A00.put(userJid, iDxObserverShape3S0100000_1_I1);
            C15370n3 r62 = r10.A0R;
            A0E(this.A0G, r62, true, false);
            this.A0D.setVisibility(C12960it.A02(r10.A0Q ? 1 : 0));
            View view = this.A0B;
            if (r10.A0G) {
                A04 = view.getContext().getString(R.string.you);
            } else {
                A04 = ((AbstractC55202hx) this).A09.A04(r62);
            }
            view.setContentDescription(A04);
        }
        boolean z = !A07();
        ((AbstractC55202hx) this).A05 = r10;
        if (r10.A0A) {
            WaRoundCornerImageView waRoundCornerImageView = this.A02;
            if (waRoundCornerImageView != null) {
                Context context = waRoundCornerImageView.getContext();
                if (this.A01 == null) {
                    GradientDrawable.Orientation orientation = GradientDrawable.Orientation.TOP_BOTTOM;
                    int[] A07 = C13000ix.A07();
                    A07[0] = AnonymousClass00T.A00(context, R.color.transparent);
                    A07[1] = AnonymousClass00T.A00(context, R.color.black_alpha_40);
                    this.A01 = new GradientDrawable(orientation, A07);
                }
                waRoundCornerImageView.setVisibility(0);
                waRoundCornerImageView.setImageDrawable(this.A01);
            }
        } else {
            WaRoundCornerImageView waRoundCornerImageView2 = this.A02;
            if (waRoundCornerImageView2 != null) {
                waRoundCornerImageView2.setVisibility(8);
            }
        }
        if (((AbstractC55202hx) this).A05.A06 != 0) {
            View view2 = super.A0H;
            int dimensionPixelSize2 = view2.getResources().getDimensionPixelSize(((AbstractC55202hx) this).A05.A06);
            if (dimensionPixelSize2 != this.A00) {
                this.A00 = dimensionPixelSize2;
                if (this.A04 != null) {
                    AnonymousClass2ZX r2 = new AnonymousClass2ZX(dimensionPixelSize2, this.A08);
                    this.A04 = r2;
                    AnonymousClass009.A0A("FrameLayout required as root to support corner rounding via overlay", view2 instanceof FrameLayout);
                    ((FrameLayout) view2).setForeground(r2);
                }
            }
        }
        if (z) {
            AnonymousClass5X3 r1 = this.A0J;
            VideoPort videoPort = this.A06;
            if (videoPort != null) {
                videoPort.setListener(r1);
            }
        }
        ConstraintLayout constraintLayout = this.A0F;
        if (constraintLayout.getBackground() instanceof GradientDrawable) {
            ((GradientDrawable) constraintLayout.getBackground()).setCornerRadius((float) this.A00);
        }
        int i2 = r10.A05;
        if (i2 == -1) {
            ViewGroup viewGroup = this.A0C;
            if (viewGroup != null) {
                viewGroup.setVisibility(8);
            }
        } else {
            ViewGroup viewGroup2 = this.A0C;
            if (!(viewGroup2 == null || (waTextView = this.A0H) == null)) {
                viewGroup2.setVisibility(0);
                C64363Fg r0 = ((AbstractC55202hx) this).A05;
                if (r0 != null) {
                    viewGroup2.setRotation((float) r0.A03);
                }
                waTextView.setText(i2);
                waTextView.setVisibility(0);
            }
        }
        this.A0B.setVisibility(0);
        View view3 = this.A0A;
        if (r10.A0N) {
            i = 0;
        }
        view3.setVisibility(i);
        view3.setRotation((float) ((AbstractC55202hx) this).A05.A03);
        if (r10.A0N || !r10.A0I) {
            A0I(false);
        }
        Bitmap bitmap = r10.A07;
        ImageView imageView = this.A0E;
        imageView.setImageBitmap(bitmap);
        int i3 = 0;
        if (bitmap == null) {
            i3 = 8;
        }
        imageView.setVisibility(i3);
        if (((AbstractC55202hx) this).A05.A02 == 0) {
            dimensionPixelSize = 0;
        } else {
            dimensionPixelSize = super.A0H.getResources().getDimensionPixelSize(((AbstractC55202hx) this).A05.A02);
        }
        Rect rect = new Rect(0, 0, dimensionPixelSize, dimensionPixelSize);
        View view4 = super.A0H;
        view4.setPadding(rect.left, rect.top, rect.right, rect.bottom);
        AnonymousClass2ZX r02 = this.A04;
        if (r02 != null) {
            r02.A00 = rect;
        }
        if (r10.A0L) {
            C12960it.A13(view4, this, r10, 18);
            view4.setOnTouchListener(new View.OnTouchListener() { // from class: X.4nJ
                @Override // android.view.View.OnTouchListener
                public final boolean onTouch(View view5, MotionEvent motionEvent) {
                    C60112vx r12 = C60112vx.this;
                    r12.A0C(motionEvent, r12.A09);
                    return false;
                }
            });
            view4.setOnLongClickListener(new View.OnLongClickListener(r10) { // from class: X.4n4
                public final /* synthetic */ C64363Fg A01;

                {
                    this.A01 = r2;
                }

                @Override // android.view.View.OnLongClickListener
                public final boolean onLongClick(View view5) {
                    C60112vx r03 = C60112vx.this;
                    C64363Fg r12 = this.A01;
                    AnonymousClass3CK r04 = r03.A03;
                    if (r04 == null) {
                        return false;
                    }
                    r04.A00(r12);
                    return true;
                }
            });
        }
    }

    public void A0H() {
        View view = this.A0B;
        if (view instanceof SurfaceView) {
            ((SurfaceView) view).setZOrderMediaOverlay(true);
        }
        this.A04 = null;
        View view2 = super.A0H;
        AnonymousClass009.A0A("FrameLayout required as root to support corner rounding via overlay", view2 instanceof FrameLayout);
        ((FrameLayout) view2).setForeground(null);
    }

    public final void A0I(boolean z) {
        GradientDrawable gradientDrawable = (GradientDrawable) this.A0F.getBackground();
        int i = ((AbstractC55202hx) this).A00;
        int i2 = -1;
        if (gradientDrawable != null) {
            if (!z) {
                i2 = 0;
            }
            gradientDrawable.setStroke(i, i2);
        }
    }
}
