package X;

import android.content.SharedPreferences;

/* renamed from: X.2SX  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2SX {
    public long A00;
    public long A01;
    public long A02;
    public boolean A03;
    public final C14820m6 A04;
    public final C22050yP A05;
    public final C28641Ok A06;

    public AnonymousClass2SX(C14820m6 r1, C22050yP r2, C28641Ok r3) {
        this.A05 = r2;
        this.A04 = r1;
        this.A06 = r3;
    }

    public void A00() {
        SharedPreferences sharedPreferences;
        SharedPreferences.Editor edit;
        String str;
        AbstractC14640lm r3 = this.A06.A00.A0J;
        long j = this.A00 + 1;
        this.A00 = j;
        if (r3 != null && j == 1) {
            if (C15380n4.A0F(r3)) {
                sharedPreferences = this.A04.A00;
                edit = sharedPreferences.edit();
                str = "ptt_pause_tap_broadcast";
            } else {
                boolean A0J = C15380n4.A0J(r3);
                sharedPreferences = this.A04.A00;
                edit = sharedPreferences.edit();
                if (A0J) {
                    str = "ptt_pause_tap_group";
                } else {
                    str = "ptt_pause_tap_individual";
                }
            }
            edit.putLong(str, sharedPreferences.getLong(str, 0) + 1).apply();
        }
    }

    public void A01(long j, long j2, boolean z) {
        SharedPreferences sharedPreferences;
        SharedPreferences.Editor edit;
        String str;
        this.A05.A05(2, j, j2, this.A01, this.A02, this.A00, z, this.A03);
        AbstractC14640lm r1 = this.A06.A00.A0J;
        if (r1 != null) {
            if (C15380n4.A0F(r1)) {
                sharedPreferences = this.A04.A00;
                edit = sharedPreferences.edit();
                str = "ptt_cancel_broadcast";
            } else {
                boolean A0J = C15380n4.A0J(r1);
                sharedPreferences = this.A04.A00;
                edit = sharedPreferences.edit();
                if (A0J) {
                    str = "ptt_cancel_group";
                } else {
                    str = "ptt_cancel_individual";
                }
            }
            edit.putLong(str, sharedPreferences.getLong(str, 0) + 1).apply();
        }
    }

    public void A02(long j, long j2, boolean z) {
        SharedPreferences sharedPreferences;
        SharedPreferences.Editor edit;
        String str;
        AbstractC14640lm r1 = this.A06.A00.A0J;
        this.A05.A05(1, j, j2, this.A01, this.A02, this.A00, z, this.A03);
        if (r1 != null) {
            if (C15380n4.A0F(r1)) {
                sharedPreferences = this.A04.A00;
                edit = sharedPreferences.edit();
                str = "ptt_send_broadcast";
            } else {
                boolean A0J = C15380n4.A0J(r1);
                sharedPreferences = this.A04.A00;
                edit = sharedPreferences.edit();
                if (A0J) {
                    str = "ptt_send_group";
                } else {
                    str = "ptt_send_individual";
                }
            }
            edit.putLong(str, sharedPreferences.getLong(str, 0) + 1).apply();
        }
    }
}
