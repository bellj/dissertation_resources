package X;

import android.util.Pair;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;

/* renamed from: X.13s  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C239713s {
    public final AbstractC15710nm A00;
    public final C14330lG A01;
    public final C15450nH A02;
    public final C14820m6 A03;
    public final C14850m9 A04;
    public final AnonymousClass155 A05;
    public final AnonymousClass152 A06;

    public C239713s(AbstractC15710nm r1, C14330lG r2, C15450nH r3, C14820m6 r4, C14850m9 r5, AnonymousClass155 r6, AnonymousClass152 r7) {
        this.A04 = r5;
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
        this.A05 = r6;
        this.A06 = r7;
        this.A03 = r4;
    }

    public static float A00(int i, int i2, int i3, int i4, long j) {
        if (i2 == 0 || i3 == 0) {
            return 3.0f;
        }
        float f = (((float) ((i << 10) << 10)) * 8000.0f) / ((float) (((i2 * i3) * 3) + 96000));
        float max = Math.max(0.0f, f - ((float) j)) / f;
        return (((float) (i4 - 3)) * max * max) + 3.0f;
    }

    public static Pair A01(int i, int i2, int i3) {
        Integer valueOf;
        Integer valueOf2;
        if (i > i2) {
            if (i > i3) {
                valueOf = Integer.valueOf(i3);
                i2 = (i2 * i3) / i;
                valueOf2 = Integer.valueOf(i2);
                return new Pair(valueOf, valueOf2);
            }
        } else if (i2 > i3) {
            valueOf = Integer.valueOf((i * i3) / i2);
            valueOf2 = Integer.valueOf(i3);
            return new Pair(valueOf, valueOf2);
        }
        valueOf = Integer.valueOf(i);
        valueOf2 = Integer.valueOf(i2);
        return new Pair(valueOf, valueOf2);
    }

    public static boolean A02(C38981p3 r13, C38991p4 r14, byte b) {
        int i = r14.A03;
        int i2 = r14.A01;
        long j = r14.A04;
        if (!r14.A08) {
            if (b != 13) {
                int i3 = r13.A02;
                if (i > i3 || i2 > i3) {
                    if (i < i2) {
                        i = (i * i3) / i2;
                        i2 = i3;
                    } else {
                        i2 = (i2 * i3) / i;
                        i = i3;
                    }
                }
                long j2 = j / 1000;
                if (((long) (((Math.min((float) r13.A01, ((float) (i * i2)) * A00(r13.A00, i, i2, 9, j)) * ((float) j2)) / 8.0f) + ((float) ((j2 * 96000) / 8)))) < r14.A06.length()) {
                    return true;
                }
                return false;
            } else if (r14.A07 || r14.A06.length() <= 262144) {
                return false;
            } else {
                float f = (float) (i * i2);
                if (((double) (((float) r14.A00()) / f)) <= ((double) Math.max(2.0f, Math.min(10.0f, 153600.0f / f))) * 1.1d) {
                    return false;
                }
            }
        }
        return true;
    }

    public C38981p3 A03(boolean z, boolean z2) {
        Float A01;
        C38981p3 r3 = new C38981p3(this.A02.A02(AbstractC15460nI.A1p), 640, 5000000);
        if (!z && !z2) {
            C14850m9 r5 = this.A04;
            if (r5.A07(597)) {
                C38981p3 r4 = new C38981p3(r5.A02(596), r5.A02(594), r5.A02(595) * 1000);
                if (r5.A07(662)) {
                    int i = this.A03.A00.getInt("video_quality", 0);
                    if (i == 0) {
                        if (r5.A07(660) && (A01 = this.A05.A01(0)) != null && A01.floatValue() >= ((float) r5.A02(661))) {
                            return r4;
                        }
                    } else if (i == 1) {
                    }
                }
                return r4;
            }
        }
        return r3;
    }

    public boolean A04(long j, long j2) {
        int i;
        if (j > 262144) {
            if (j2 == 0) {
                i = 0;
            } else {
                i = (int) ((j * 8) / (j2 * 1000));
            }
            if (i > this.A02.A02(AbstractC15460nI.A2F)) {
                return true;
            }
        }
        return false;
    }

    public boolean A05(C14370lK r5, File file) {
        if (r5 == C14370lK.A0X || r5 == C14370lK.A04 || r5 == C14370lK.A0a) {
            if (C39011p7.A02(this.A04.A07(422)) != 1) {
                return false;
            }
        } else if (r5 == C14370lK.A05) {
            return AnonymousClass1p5.A00(file);
        } else {
            if (r5 != C14370lK.A0B && r5 != C14370lK.A0Z && r5 != C14370lK.A0R && r5 != C14370lK.A06 && r5 != C14370lK.A0S && r5 != C14370lK.A0G && r5 != C14370lK.A0L && r5 != C14370lK.A07) {
                return false;
            }
        }
        return true;
    }

    public boolean A06(C14370lK r4, File file) {
        if (file != null) {
            try {
                if (C14370lK.A0X == r4 || C14370lK.A04 == r4 || C14370lK.A0a == r4) {
                    if (AnonymousClass152.A04(file, false).A01 != 0) {
                        return true;
                    }
                } else if ((C14370lK.A05 == r4 || C14370lK.A0I == r4) && AnonymousClass152.A04(file, false).A01 == 2) {
                    return true;
                }
            } catch (IOException e) {
                Log.e("transcodeutils/isEligibleForMp4Check exception", e);
                return false;
            }
        }
        return false;
    }

    public boolean A07(AbstractC16130oV r9) {
        try {
            C16150oX r3 = r9.A02;
            boolean z = false;
            if (r9.A08 != null && (r3 == null || r3.A0F == null)) {
                return false;
            }
            AnonymousClass009.A05(r3);
            if (r3.A0O) {
                return false;
            }
            C14370lK A01 = C14370lK.A01(r9.A0y, ((AbstractC15340mz) r9).A08);
            C14370lK r4 = C14370lK.A04;
            if (A01 == r4) {
                File file = r3.A0F;
                if (file != null) {
                    try {
                        C38911ou.A03(file);
                    } catch (IOException unused) {
                    }
                }
                return true;
            }
            if (A01 == C14370lK.A0B || A01 == C14370lK.A0Z || A01 == C14370lK.A0G || A01 == C14370lK.A06 || A01 == C14370lK.A0R || A01 == C14370lK.A0V || A01 == C14370lK.A07) {
                if (r3.A0F != null) {
                    return false;
                }
            } else if (A01 == C14370lK.A0X || A01 == r4 || A01 == C14370lK.A0a) {
                String str = r3.A0H;
                if (str != null && C22200yh.A0I(this.A01, str).exists()) {
                    z = true;
                }
                File file2 = r3.A0F;
                long j = r9.A01;
                if (!z && j <= ((long) this.A02.A02(AbstractC15460nI.A1p)) * 1048576) {
                    return !this.A06.A0E(file2);
                }
            } else if (A01 == C14370lK.A05 || A01 == C14370lK.A0I) {
                return !this.A06.A0D(r3.A0F);
            } else {
                if (A01 != C14370lK.A0S) {
                    return false;
                }
            }
            return true;
        } catch (IOException e) {
            Log.e("transcodeutils/needtranscodemedia exception", e);
            return true;
        }
    }
}
