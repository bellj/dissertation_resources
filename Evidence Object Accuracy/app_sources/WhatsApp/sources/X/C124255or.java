package X;

import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiBankPickerActivity;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.5or  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124255or extends AbstractC16350or {
    public final String A00;
    public final ArrayList A01;
    public final /* synthetic */ IndiaUpiBankPickerActivity A02;

    public C124255or(IndiaUpiBankPickerActivity indiaUpiBankPickerActivity, String str, ArrayList arrayList) {
        ArrayList arrayList2;
        this.A02 = indiaUpiBankPickerActivity;
        if (arrayList != null) {
            arrayList2 = C12980iv.A0x(arrayList);
        } else {
            arrayList2 = null;
        }
        this.A01 = arrayList2;
        this.A00 = str;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00a8, code lost:
        r3 = r3 + 1;
     */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00b9 A[SYNTHETIC] */
    @Override // X.AbstractC16350or
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ java.lang.Object A05(java.lang.Object[] r19) {
        /*
            r18 = this;
            java.util.ArrayList r13 = X.C12960it.A0l()
            r12 = r18
            java.util.ArrayList r11 = r12.A01
            if (r11 == 0) goto L_0x0010
            boolean r0 = r11.isEmpty()
            if (r0 == 0) goto L_0x001a
        L_0x0010:
            com.whatsapp.payments.ui.IndiaUpiBankPickerActivity r0 = r12.A02
            java.util.List r0 = r0.A0J
            if (r0 == 0) goto L_0x001a
            r13.addAll(r0)
        L_0x0019:
            return r13
        L_0x001a:
            com.whatsapp.payments.ui.IndiaUpiBankPickerActivity r10 = r12.A02
            java.util.List r0 = r10.A0H
            if (r0 == 0) goto L_0x0019
            java.util.Iterator r17 = r0.iterator()
        L_0x0024:
            boolean r0 = r17.hasNext()
            if (r0 == 0) goto L_0x0019
            java.lang.Object r9 = r17.next()
            X.1Zb r9 = (X.AbstractC30851Zb) r9
            java.lang.String r2 = r9.A0B()
            X.018 r1 = r10.A01
            r0 = 1
            boolean r0 = X.C32751cg.A03(r1, r2, r11, r0)
            if (r0 != 0) goto L_0x00ae
            java.lang.String r2 = r9.A0B()
            java.lang.String r1 = r12.A00
            X.018 r3 = r10.A01
            boolean r0 = android.text.TextUtils.isEmpty(r2)
            r8 = 0
            if (r0 != 0) goto L_0x00b1
            java.util.regex.Pattern r0 = X.C32751cg.A02
            java.util.regex.Matcher r2 = r0.matcher(r2)
            java.lang.String r0 = " "
            java.lang.String r7 = r2.replaceAll(r0)
            java.util.regex.Pattern r0 = X.C35001h3.A00
            java.util.regex.Matcher r0 = r0.matcher(r7)
            boolean r0 = r0.find()
            r16 = r0 ^ 1
            if (r16 == 0) goto L_0x006a
            java.lang.String r7 = X.AnonymousClass1US.A08(r7)
        L_0x006a:
            java.text.BreakIterator r6 = X.C32751cg.A01(r3)
            r6.setText(r7)
            int r15 = r6.first()
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x00ab
            java.lang.String r0 = X.AnonymousClass1US.A08(r1)
        L_0x007f:
            char[] r5 = r0.toCharArray()
            int r4 = r5.length
            r3 = 0
        L_0x0085:
            if (r3 >= r4) goto L_0x00ae
            char r2 = r5[r3]
        L_0x0089:
            int r14 = r6.next()
            r1 = r15
            r15 = r14
            r0 = -1
            if (r14 == r0) goto L_0x00b1
            java.lang.String r1 = r7.substring(r1, r14)
            if (r16 != 0) goto L_0x009c
            java.lang.String r1 = X.AnonymousClass1US.A08(r1)
        L_0x009c:
            boolean r0 = r1.isEmpty()
            if (r0 != 0) goto L_0x0089
            char r0 = r1.charAt(r8)
            if (r2 != r0) goto L_0x0089
            int r3 = r3 + 1
            goto L_0x0085
        L_0x00ab:
            java.lang.String r0 = ""
            goto L_0x007f
        L_0x00ae:
            r13.add(r9)
        L_0x00b1:
            X.0os r0 = r12.A02
            boolean r0 = r0.isCancelled()
            if (r0 == 0) goto L_0x0024
            return r13
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C124255or.A05(java.lang.Object[]):java.lang.Object");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000e, code lost:
        if (r1 != false) goto L_0x0010;
     */
    @Override // X.AbstractC16350or
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A06() {
        /*
            r3 = this;
            com.whatsapp.payments.ui.IndiaUpiBankPickerActivity r0 = r3.A02
            android.view.View r2 = r0.A01
            java.util.ArrayList r0 = r3.A01
            if (r0 == 0) goto L_0x0010
            boolean r1 = r0.isEmpty()
            r0 = 8
            if (r1 == 0) goto L_0x0011
        L_0x0010:
            r0 = 0
        L_0x0011:
            r2.setVisibility(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C124255or.A06():void");
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        List list = (List) obj;
        IndiaUpiBankPickerActivity indiaUpiBankPickerActivity = this.A02;
        indiaUpiBankPickerActivity.A0D = null;
        if (list.isEmpty()) {
            indiaUpiBankPickerActivity.A04.setText(C12960it.A0X(indiaUpiBankPickerActivity, indiaUpiBankPickerActivity.A0F, C12970iu.A1b(), 0, R.string.search_no_results));
        }
        if (list.isEmpty()) {
            indiaUpiBankPickerActivity.A04.setVisibility(0);
            indiaUpiBankPickerActivity.A00.setVisibility(8);
        } else {
            indiaUpiBankPickerActivity.A00.setVisibility(0);
            indiaUpiBankPickerActivity.A04.setVisibility(8);
        }
        indiaUpiBankPickerActivity.A02.setVisibility(8);
        C118595c4 r0 = indiaUpiBankPickerActivity.A0C;
        r0.A00 = list;
        r0.A02();
        indiaUpiBankPickerActivity.A05.A0Z(0);
    }
}
