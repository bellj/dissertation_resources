package X;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.widget.CompoundButton;
import java.lang.reflect.Field;

/* renamed from: X.0TP  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0TP {
    public static Field A00;
    public static boolean A01;

    public static Drawable A00(CompoundButton compoundButton) {
        if (Build.VERSION.SDK_INT >= 23) {
            return AnonymousClass0L2.A00(compoundButton);
        }
        if (!A01) {
            try {
                Field declaredField = CompoundButton.class.getDeclaredField("mButtonDrawable");
                A00 = declaredField;
                declaredField.setAccessible(true);
            } catch (NoSuchFieldException e) {
                Log.i("CompoundButtonCompat", "Failed to retrieve mButtonDrawable field", e);
            }
            A01 = true;
        }
        Field field = A00;
        if (field != null) {
            try {
                return (Drawable) field.get(compoundButton);
            } catch (IllegalAccessException e2) {
                Log.i("CompoundButtonCompat", "Failed to get button drawable via reflection", e2);
                A00 = null;
            }
        }
        return null;
    }

    public static void A01(ColorStateList colorStateList, CompoundButton compoundButton) {
        if (Build.VERSION.SDK_INT >= 21) {
            C05780Qy.A00(colorStateList, compoundButton);
        } else if (compoundButton instanceof AbstractC12370ho) {
            ((AbstractC12370ho) compoundButton).setSupportButtonTintList(colorStateList);
        }
    }

    public static void A02(PorterDuff.Mode mode, CompoundButton compoundButton) {
        if (Build.VERSION.SDK_INT >= 21) {
            C05780Qy.A01(mode, compoundButton);
        } else if (compoundButton instanceof AbstractC12370ho) {
            ((AbstractC12370ho) compoundButton).setSupportButtonTintMode(mode);
        }
    }
}
