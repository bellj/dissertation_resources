package X;

import android.util.Base64;
import java.security.MessageDigest;
import java.security.PublicKey;

/* renamed from: X.5z3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130255z3 {
    public static String A00(PublicKey publicKey) {
        Object[] A1b = C12970iu.A1b();
        A1b[0] = Base64.encodeToString(MessageDigest.getInstance("SHA-256").digest(publicKey.getEncoded()), 11);
        return String.format("fp:%s", A1b);
    }
}
