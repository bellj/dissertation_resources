package X;

import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import com.whatsapp.voipcalling.CallLinkInfo;
import java.util.List;

/* renamed from: X.0sa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18520sa {
    public Drawable A01(C14260l7 r15, AnonymousClass28D r16, AnonymousClass28D r17) {
        String str;
        String str2;
        GradientDrawable.Orientation orientation;
        float f;
        Drawable A01;
        int i = r16.A01;
        if (i == 13318) {
            try {
                return C64963Hp.A01(r15, r17, AnonymousClass3JW.A05(r16.A0I(35)));
            } catch (AnonymousClass491 e) {
                e = e;
                str = "ColorDrawableUtils";
                str2 = "Error parsing color for ColorDrawable";
            }
        } else if (i == 13322) {
            AnonymousClass28D A0F = r16.A0F(35);
            AnonymousClass28D A0F2 = r16.A0F(36);
            if (A0F == null || A0F2 == null) {
                throw new RuntimeException("Gradient drawable received with null begin or end color");
            }
            AnonymousClass28D A0F3 = r16.A0F(38);
            String A0I = r16.A0I(40);
            try {
                if (A0I == null) {
                    orientation = GradientDrawable.Orientation.TOP_BOTTOM;
                } else {
                    switch (A0I.hashCode()) {
                        case -1451623918:
                            if (A0I.equals("bottom_left_to_top_right")) {
                                orientation = GradientDrawable.Orientation.BL_TR;
                                break;
                            }
                            StringBuilder sb = new StringBuilder("can't parse orientation value: ");
                            sb.append(A0I);
                            throw new AnonymousClass491(sb.toString());
                        case -1118360059:
                            if (A0I.equals("top_to_bottom")) {
                                orientation = GradientDrawable.Orientation.TOP_BOTTOM;
                                break;
                            }
                            StringBuilder sb = new StringBuilder("can't parse orientation value: ");
                            sb.append(A0I);
                            throw new AnonymousClass491(sb.toString());
                        case 404498110:
                            if (A0I.equals("top_left_to_bottom_right")) {
                                orientation = GradientDrawable.Orientation.TL_BR;
                                break;
                            }
                            StringBuilder sb = new StringBuilder("can't parse orientation value: ");
                            sb.append(A0I);
                            throw new AnonymousClass491(sb.toString());
                        case 1553519760:
                            if (A0I.equals("left_to_right")) {
                                orientation = GradientDrawable.Orientation.LEFT_RIGHT;
                                break;
                            }
                            StringBuilder sb = new StringBuilder("can't parse orientation value: ");
                            sb.append(A0I);
                            throw new AnonymousClass491(sb.toString());
                        default:
                            StringBuilder sb = new StringBuilder("can't parse orientation value: ");
                            sb.append(A0I);
                            throw new AnonymousClass491(sb.toString());
                    }
                }
            } catch (AnonymousClass491 e2) {
                C28691Op.A01("GradientDrawableUtils", "Error parsing orientation for GradientDrawable", e2);
                orientation = GradientDrawable.Orientation.TOP_BOTTOM;
            }
            Integer valueOf = Integer.valueOf(AnonymousClass4Di.A00(r15, A0F));
            Integer valueOf2 = Integer.valueOf(AnonymousClass4Di.A00(r15, A0F2));
            GradientDrawable gradientDrawable = new GradientDrawable(orientation, A0F3 != null ? new int[]{valueOf.intValue(), Integer.valueOf(AnonymousClass4Di.A00(r15, A0F3)).intValue(), valueOf2.intValue()} : new int[]{valueOf.intValue(), valueOf2.intValue()});
            if (r17 == null) {
                return gradientDrawable;
            }
            try {
                String A0I2 = r17.A0I(46);
                if (A0I2 == null) {
                    f = 0.0f;
                } else {
                    f = AnonymousClass3JW.A01(A0I2);
                }
                float[] fArr = new float[8];
                C64973Hq.A01(fArr, f, C64973Hq.A00(r17.A0M(56), 15));
                gradientDrawable.setCornerRadii(fArr);
                return gradientDrawable;
            } catch (AnonymousClass491 unused) {
                C28691Op.A00("GradientDrawableUtils", "Error parsing Corner radius for Box decoration");
                return gradientDrawable;
            }
        } else if (i == 13330) {
            return AnonymousClass3B0.A00(r15, r16, r17);
        } else {
            if (i == 13332) {
                StateListDrawable stateListDrawable = new StateListDrawable();
                Drawable[] drawableArr = new Drawable[6];
                List A0L = r16.A0L(35);
                for (int i2 = 0; i2 < A0L.size(); i2++) {
                    AnonymousClass28D r8 = (AnonymousClass28D) A0L.get(i2);
                    AnonymousClass28D A0F4 = r8.A0F(35);
                    if (A0F4 == null) {
                        C28691Op.A00("StateDrawableUtils", "Null Drawable model when creating children of a StateDrawable");
                        A01 = new ColorDrawable();
                    } else {
                        A01 = C65093Ic.A00().A06.A01(r15, A0F4, r17);
                    }
                    String A0J = r8.A0J(36, "");
                    switch (A0J.hashCode()) {
                        case -691041417:
                            if (A0J.equals("focused")) {
                                drawableArr[1] = A01;
                                break;
                            } else {
                                break;
                            }
                        case -318264286:
                            if (A0J.equals("pressed")) {
                                drawableArr[3] = A01;
                                break;
                            } else {
                                break;
                            }
                        case 270940796:
                            if (A0J.equals("disabled")) {
                                drawableArr[4] = A01;
                                break;
                            } else {
                                break;
                            }
                        case 1191572123:
                            if (A0J.equals("selected")) {
                                drawableArr[2] = A01;
                                break;
                            } else {
                                break;
                            }
                        case 1544803905:
                            if (A0J.equals(CallLinkInfo.DEFAULT_CALL_LINK_CALL_ID)) {
                                drawableArr[0] = A01;
                                break;
                            } else {
                                break;
                            }
                    }
                }
                int i3 = 5;
                do {
                    if (drawableArr[i3] != null) {
                        stateListDrawable.addState(C88474Fu.A00[i3], drawableArr[i3]);
                    }
                    i3--;
                } while (i3 >= 0);
                return stateListDrawable;
            } else if (i != 13340) {
                return new ColorDrawable();
            } else {
                try {
                    return C64963Hp.A01(r15, r17, AnonymousClass4Di.A00(r15, r16.A0F(35)));
                } catch (AnonymousClass491 e3) {
                    e = e3;
                    str = "ThemedColorDrawableUtils";
                    str2 = "Parse error for ThemedColorDrawable";
                }
            }
        }
        C28691Op.A01(str, str2, e);
        return new AnonymousClass2ZU();
    }
}
