package X;

import android.os.ConditionVariable;
import com.facebook.redex.RunnableBRunnable0Shape4S0200000_I0_4;
import com.facebook.redex.RunnableBRunnable0Shape6S0100000_I0_6;

/* renamed from: X.0sA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18260sA implements AbstractC18270sB {
    public boolean A00;
    public boolean A01;
    public final C22490zA A02;
    public final C14900mE A03;
    public final C15570nT A04;
    public final C19500uD A05;
    public final C15820nx A06;
    public final C22730zY A07;
    public final C19380u1 A08;
    public final C18280sC A09;
    public final C18230s7 A0A;
    public final C17050qB A0B;
    public final AnonymousClass01d A0C;
    public final C14830m7 A0D;
    public final C16590pI A0E;
    public final C14820m6 A0F;
    public final AnonymousClass018 A0G;
    public final C44951zp A0H = new C44951zp();
    public final C15880o3 A0I;
    public final C14850m9 A0J;
    public final C16120oU A0K;
    public final C21820y2 A0L;
    public final C16600pJ A0M;
    public final AbstractC14440lR A0N;
    public final C21710xr A0O;
    public final AnonymousClass01H A0P;

    public C18260sA(C22490zA r3, C14900mE r4, C15570nT r5, C19500uD r6, C15820nx r7, C22730zY r8, C19380u1 r9, C18280sC r10, C18230s7 r11, C17050qB r12, AnonymousClass01d r13, C14830m7 r14, C16590pI r15, C14820m6 r16, AnonymousClass018 r17, C15880o3 r18, C14850m9 r19, C16120oU r20, C21820y2 r21, C16600pJ r22, AbstractC14440lR r23, C21710xr r24, AnonymousClass01H r25) {
        this.A0E = r15;
        this.A0D = r14;
        this.A0J = r19;
        this.A03 = r4;
        this.A0A = r11;
        this.A04 = r5;
        this.A0N = r23;
        this.A0K = r20;
        this.A0P = r25;
        this.A08 = r9;
        this.A0C = r13;
        this.A0G = r17;
        this.A0O = r24;
        this.A06 = r7;
        this.A05 = r6;
        this.A0B = r12;
        this.A0I = r18;
        this.A0F = r16;
        this.A09 = r10;
        this.A0L = r21;
        this.A0M = r22;
        this.A07 = r8;
        this.A02 = r3;
        r21.A03(new AbstractC44971zr(r14, this) { // from class: X.1zq
            public final /* synthetic */ C14830m7 A00;
            public final /* synthetic */ C18260sA A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC44971zr
            public final void AVX(boolean z) {
                C18260sA r42 = this.A01;
                if (r42.A02(z)) {
                    C44981zs r32 = new C44981zs();
                    r32.A04 = Long.valueOf(System.currentTimeMillis());
                    r32.A03 = 2;
                    r42.A01(new C44991zt(null, r42, r32), -1);
                }
            }
        });
    }

    public final void A00() {
        int i;
        String str;
        C16600pJ r5 = this.A0M;
        synchronized (r5) {
            r5.A00 = true;
        }
        r5.A00("localbackupmanager/sendCreateBackupKeyIfNeeded/started", 2);
        if (C32781cj.A0D(this.A0E.A00)) {
            byte[] A0D = C003501n.A0D(16);
            byte[] A0F = C32781cj.A0F(A0D);
            if (A0F != null) {
                ConditionVariable conditionVariable = new ConditionVariable(false);
                this.A05.A01(new RunnableBRunnable0Shape6S0100000_I0_6(conditionVariable, 6), A0F, A0D, 1);
                r5.A00("localbackupmanager/backup/waiting-for-the-key", 2);
                if (!conditionVariable.block(32000)) {
                    i = 4;
                    str = "localbackupmanager/backup/backup-key-not-received";
                } else {
                    r5.A00("localbackupmanager/backup/key-received", 2);
                    return;
                }
            } else {
                i = 3;
                str = "localbackupmanager/backup/backup-key/null/account-hash/null";
            }
            r5.A00(str, i);
        }
    }

    public void A01(AbstractC45001zu r14, long j) {
        C44951zp r8 = this.A0H;
        r8.A03(r14);
        AnonymousClass01H r10 = this.A0P;
        this.A03.A0I(new RunnableBRunnable0Shape4S0200000_I0_4(this, 17, new C45011zv(this.A02, this.A08, this.A0C, this, r8, this.A0I, r10, j)));
    }

    public final boolean A02(boolean z) {
        C15570nT r0 = this.A04;
        r0.A08();
        return r0.A00 != null && this.A00 && z && !this.A0B.A02() && this.A09.A00.A01();
    }

    @Override // X.AbstractC18270sB
    public void ARF() {
        if (A02(this.A0L.A00)) {
            C44981zs r3 = new C44981zs();
            r3.A04 = Long.valueOf(System.currentTimeMillis());
            r3.A03 = 2;
            A01(new C44991zt(null, this, r3), -1);
        }
    }
}
