package X;

import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import com.whatsapp.util.Log;
import java.util.regex.Pattern;

/* renamed from: X.1Uj  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1Uj {
    public static String A00(C16330op r4, String str, String str2) {
        String str3 = "";
        try {
            Cursor A09 = r4.A09("SELECT sql FROM sqlite_master WHERE type = ? AND name = ?", new String[]{str, str2});
            if (A09 != null && A09.moveToNext()) {
                str3 = A09.getString(A09.getColumnIndexOrThrow("sql"));
            }
            if (A09 != null) {
                A09.close();
                return str3;
            }
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder("BaseDatabaseTable/getSqlFor view = ");
            sb.append(str2);
            Log.e(sb.toString(), e);
        }
        return str3;
    }

    public static void A01(C16330op r3, String str, String str2) {
        String A00 = C29771Up.A00(str2);
        StringBuilder sb = new StringBuilder("DROP_");
        sb.append(str2);
        C29721Uk.A00(str, "dropLoggableDatabaseTables", sb.toString());
        r3.A0B(A00);
    }

    public static boolean A02(C16330op r5, String str, String str2, String str3, String str4, String str5) {
        String trim = str3.trim();
        String trim2 = str4.trim();
        if (A03(str, trim, trim2)) {
            return false;
        }
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("ALTER TABLE ");
            sb.append(str2);
            sb.append(" ADD ");
            sb.append(trim);
            sb.append(" ");
            sb.append(trim2);
            String obj = sb.toString();
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str5);
            sb2.append("/addColumnIfNotExists/ALTER_TABLE");
            sb2.toString();
            r5.A0B(obj);
            return true;
        } catch (SQLiteException e) {
            StringBuilder sb3 = new StringBuilder("databasehelper/addColumnIfNotExists/alter_table ");
            sb3.append(trim);
            Log.w(sb3.toString(), e);
            return false;
        }
    }

    public static boolean A03(String str, String str2, String str3) {
        String trim = str2.trim();
        String trim2 = str3.trim();
        StringBuilder sb = new StringBuilder();
        sb.append(trim);
        sb.append(" ");
        sb.append(trim2);
        if (!str.contains(sb.toString())) {
            StringBuilder sb2 = new StringBuilder("`");
            sb2.append(trim);
            sb2.append("`\t");
            sb2.append(trim2);
            if (!str.contains(sb2.toString())) {
                StringBuilder sb3 = new StringBuilder("(`*)");
                sb3.append(trim);
                sb3.append("(`*)(\\s+)");
                sb3.append(trim2);
                if (!Pattern.compile(sb3.toString()).matcher(str).find()) {
                    return false;
                }
            }
        }
        return true;
    }
}
