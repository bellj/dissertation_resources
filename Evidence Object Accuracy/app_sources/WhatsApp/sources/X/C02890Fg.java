package X;

import android.database.sqlite.SQLiteDatabase;

/* renamed from: X.0Fg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02890Fg extends AnonymousClass0OL {
    public C02890Fg() {
        super(1, 2);
    }

    @Override // X.AnonymousClass0OL
    public void A00(AbstractC12920im r3) {
        SQLiteDatabase sQLiteDatabase = ((AnonymousClass0ZE) r3).A00;
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS `SystemIdInfo` (`work_spec_id` TEXT NOT NULL, `system_id` INTEGER NOT NULL, PRIMARY KEY(`work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )");
        sQLiteDatabase.execSQL("INSERT INTO SystemIdInfo(work_spec_id, system_id) SELECT work_spec_id, alarm_id AS system_id FROM alarmInfo");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS alarmInfo");
        sQLiteDatabase.execSQL("INSERT OR IGNORE INTO worktag(tag, work_spec_id) SELECT worker_class_name AS tag, id AS work_spec_id FROM workspec");
    }
}
