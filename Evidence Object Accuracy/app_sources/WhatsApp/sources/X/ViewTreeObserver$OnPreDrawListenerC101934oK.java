package X;

import android.view.ViewTreeObserver;

/* renamed from: X.4oK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnPreDrawListenerC101934oK implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ AnonymousClass3FC A00;

    public ViewTreeObserver$OnPreDrawListenerC101934oK(AnonymousClass3FC r1) {
        this.A00 = r1;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        AnonymousClass3FC r1 = this.A00;
        C12980iv.A1G(r1.A02, this);
        r1.A01();
        return false;
    }
}
