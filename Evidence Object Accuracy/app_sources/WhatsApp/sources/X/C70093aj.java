package X;

import com.whatsapp.conversationslist.ConversationsFragment;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.3aj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70093aj implements AnonymousClass5WL {
    public final /* synthetic */ ConversationsFragment.BulkDeleteConversationDialogFragment A00;
    public final /* synthetic */ List A01;

    public C70093aj(ConversationsFragment.BulkDeleteConversationDialogFragment bulkDeleteConversationDialogFragment, List list) {
        this.A00 = bulkDeleteConversationDialogFragment;
        this.A01 = list;
    }

    @Override // X.AnonymousClass5WL
    public void ASv() {
        this.A00.A1B();
    }

    @Override // X.AnonymousClass5WL
    public void ATy(boolean z) {
        Log.i("conversations/bulk-delete");
        ConversationsFragment.BulkDeleteConversationDialogFragment bulkDeleteConversationDialogFragment = this.A00;
        bulkDeleteConversationDialogFragment.A1B();
        ArrayList A0l = C12960it.A0l();
        Iterator it = this.A01.iterator();
        while (it.hasNext()) {
            A0l.add(bulkDeleteConversationDialogFragment.A01.A0B(C12990iw.A0b(it)));
        }
        C63063Ac.A00((ActivityC13810kN) C13010iy.A01(bulkDeleteConversationDialogFragment), bulkDeleteConversationDialogFragment.A00, bulkDeleteConversationDialogFragment.A02, bulkDeleteConversationDialogFragment.A04, bulkDeleteConversationDialogFragment.A07, A0l, z);
    }
}
