package X;

import java.util.List;

/* renamed from: X.5oo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124225oo extends AbstractC16350or {
    public final AnonymousClass018 A00;
    public final C15240mn A01;
    public final C17070qD A02;
    public final C30941Zk A03;
    public final AnonymousClass6M3 A04;
    public final C129795yJ A05;
    public final AnonymousClass14X A06;
    public final String A07;
    public final boolean A08 = true;
    public final boolean A09;

    public /* synthetic */ C124225oo(AnonymousClass018 r2, C15240mn r3, C17070qD r4, C30941Zk r5, AnonymousClass6M3 r6, C129795yJ r7, AnonymousClass14X r8, String str, boolean z) {
        this.A07 = str;
        this.A09 = z;
        this.A01 = r3;
        this.A04 = r6;
        this.A03 = r5;
        this.A02 = r4;
        this.A05 = r7;
        this.A06 = r8;
        this.A00 = r2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:56:0x0198  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x01eb  */
    @Override // X.AbstractC16350or
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ java.lang.Object A05(java.lang.Object[] r15) {
        /*
        // Method dump skipped, instructions count: 510
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C124225oo.A05(java.lang.Object[]):java.lang.Object");
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        AnonymousClass01T r6 = (AnonymousClass01T) obj;
        AnonymousClass6M3 r4 = this.A04;
        String str = this.A07;
        C30941Zk r2 = this.A03;
        Object obj2 = r6.A00;
        AnonymousClass009.A05(obj2);
        Object obj3 = r6.A01;
        AnonymousClass009.A05(obj3);
        r4.AVd(r2, str, (List) obj2, (List) obj3);
    }
}
