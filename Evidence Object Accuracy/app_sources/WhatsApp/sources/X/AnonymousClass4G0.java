package X;

import java.util.HashMap;
import java.util.Map;

/* renamed from: X.4G0  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4G0 {
    public static final Map A00;

    static {
        HashMap A11 = C12970iu.A11();
        A00 = A11;
        A11.put(AnonymousClass4BP.A06, new C1094551t());
        A11.put(AnonymousClass4BP.A0D, new AnonymousClass520());
        A11.put(AnonymousClass4BP.A0K, new AnonymousClass527());
        A11.put(AnonymousClass4BP.A05, new C1094451s());
        A11.put(AnonymousClass4BP.A0J, new AnonymousClass526());
        A11.put(AnonymousClass4BP.A0A, new C1095051y());
        A11.put(AnonymousClass4BP.A0B, new C1094951x());
        A11.put(AnonymousClass4BP.A07, new C1094751v());
        A11.put(AnonymousClass4BP.A08, new C1094651u());
        A11.put(AnonymousClass4BP.A0G, new AnonymousClass528());
        A11.put(AnonymousClass4BP.A0H, new AnonymousClass523());
        A11.put(AnonymousClass4BP.A04, new C1094351r());
        A11.put(AnonymousClass4BP.A09, new C1094851w());
        A11.put(AnonymousClass4BP.A0E, new AnonymousClass521());
        A11.put(AnonymousClass4BP.A01, new C1094051o());
        A11.put(AnonymousClass4BP.A03, new C1094251q());
        A11.put(AnonymousClass4BP.A0C, new AnonymousClass522());
        A11.put(AnonymousClass4BP.A0L, new AnonymousClass525());
        A11.put(AnonymousClass4BP.A0I, new AnonymousClass524());
        A11.put(AnonymousClass4BP.A02, new C1094151p());
        A11.put(AnonymousClass4BP.A0F, new C1095151z());
    }
}
