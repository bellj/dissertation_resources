package X;

/* renamed from: X.5xH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129165xH {
    public C16380ov A00;
    public AbstractC16350or A01;
    public final C20370ve A02;
    public final AbstractC136316Mb A03;
    public final C130115yp A04;
    public final AbstractC14440lR A05;

    public C129165xH(C20370ve r1, AbstractC136316Mb r2, C130115yp r3, AbstractC14440lR r4) {
        this.A03 = r2;
        this.A05 = r4;
        this.A04 = r3;
        this.A02 = r1;
    }

    public void A00(Runnable runnable, String str) {
        if (this.A01 == null) {
            C124265os r1 = new C124265os(this, runnable, str);
            this.A01 = r1;
            C12960it.A1E(r1, this.A05);
        }
    }
}
