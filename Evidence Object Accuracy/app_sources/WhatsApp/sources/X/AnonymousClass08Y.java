package X;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

/* renamed from: X.08Y  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass08Y {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public Context A04;
    public Bundle A05;
    public View A06;
    public View A07;
    public ViewGroup A08;
    public AnonymousClass0XO A09;
    public AnonymousClass07H A0A;
    public boolean A0B;
    public boolean A0C;
    public boolean A0D;
    public boolean A0E = false;
    public boolean A0F;

    public AnonymousClass08Y(int i) {
        this.A01 = i;
    }
}
