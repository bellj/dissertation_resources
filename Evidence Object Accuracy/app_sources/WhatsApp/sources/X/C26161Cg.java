package X;

import com.facebook.redex.RunnableBRunnable0Shape5S0200000_I0_5;
import com.whatsapp.R;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;

/* renamed from: X.1Cg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26161Cg {
    public final AbstractC15710nm A00;
    public final C14330lG A01;
    public final C14900mE A02;
    public final C14830m7 A03;
    public final C15650ng A04;
    public final AnonymousClass12H A05;
    public final AbstractC14440lR A06;
    public final Map A07 = new HashMap();

    public C26161Cg(AbstractC15710nm r2, C14330lG r3, C14900mE r4, C14830m7 r5, C15650ng r6, AnonymousClass12H r7, AbstractC14440lR r8) {
        this.A03 = r5;
        this.A02 = r4;
        this.A00 = r2;
        this.A06 = r8;
        this.A01 = r3;
        this.A04 = r6;
        this.A05 = r7;
    }

    public void A00(AbstractC16130oV r12) {
        byte b = r12.A0y;
        long A02 = this.A03.A02(r12.A0I);
        String str = r12.A05;
        if (str == null) {
            this.A06.Ab2(new RunnableBRunnable0Shape5S0200000_I0_5(this, 45, r12));
            return;
        }
        C14900mE r6 = this.A02;
        AbstractC15710nm r5 = this.A00;
        int i = ((AbstractC15340mz) r12).A08;
        ArrayList arrayList = new ArrayList(3);
        C14330lG r1 = this.A01;
        arrayList.add(r1.A0B(b, i, 2));
        arrayList.add(r1.A0B(b, i, 1));
        arrayList.add(r1.A0B(b, i, 3));
        C58652r8 r4 = new C58652r8(r5, r6, str, arrayList, A02);
        this.A07.put(r12, r4);
        C16150oX r2 = r12.A02;
        AnonymousClass009.A05(r2);
        r2.A0a = true;
        r2.A0C = 0;
        this.A05.A08(r12, -1);
        C70603bY r0 = new AbstractC14590lg(r12) { // from class: X.3bY
            public final /* synthetic */ AbstractC16130oV A01;

            {
                this.A01 = r2;
            }

            /* JADX WARNING: Code restructure failed: missing block: B:10:0x0040, code lost:
                if (X.C37381mH.A00(((X.AbstractC15340mz) r3).A0C, 2) >= 0) goto L_0x002f;
             */
            @Override // X.AbstractC14590lg
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void accept(java.lang.Object r6) {
                /*
                    r5 = this;
                    X.1Cg r4 = X.C26161Cg.this
                    X.0oV r3 = r5.A01
                    java.io.File r6 = (java.io.File) r6
                    java.util.Map r0 = r4.A07
                    r0.remove(r3)
                    X.0oX r2 = r3.A02
                    X.AnonymousClass009.A05(r2)
                    r0 = 0
                    r2.A0a = r0
                    X.AnonymousClass009.A05(r2)
                    r2.A0L = r0
                    r2.A0F = r6
                    long r0 = r6.length()
                    r2.A0A = r0
                    r0 = 1
                    r2.A0O = r0
                    X.0oX r1 = X.AbstractC15340mz.A00(r3)
                    java.lang.String r0 = r3.A08
                    if (r0 == 0) goto L_0x0038
                    java.io.File r0 = r1.A0F
                    if (r0 == 0) goto L_0x0038
                L_0x002f:
                    r1 = 1
                L_0x0030:
                    r2.A0P = r1
                    X.0ng r0 = r4.A04
                    r0.A0W(r3)
                    return
                L_0x0038:
                    int r1 = r3.A0C
                    r0 = 2
                    int r0 = X.C37381mH.A00(r1, r0)
                    r1 = 0
                    if (r0 < 0) goto L_0x0030
                    goto L_0x002f
                */
                throw new UnsupportedOperationException("Method not decompiled: X.C70603bY.accept(java.lang.Object):void");
            }
        };
        Executor executor = r6.A06;
        r4.A01(r0, executor);
        ((C14580lf) r4).A00.A03(new AbstractC14590lg(r12) { // from class: X.5AO
            public final /* synthetic */ AbstractC16130oV A01;

            {
                this.A01 = r2;
            }

            @Override // X.AbstractC14590lg
            public final void accept(Object obj) {
                C26161Cg.this.A01(this.A01, (Throwable) obj);
            }
        }, executor);
        this.A06.Ab2(r4);
    }

    public final void A01(AbstractC16130oV r4, Throwable th) {
        this.A07.remove(r4);
        C16150oX r1 = r4.A02;
        AnonymousClass009.A05(r1);
        r1.A0a = false;
        this.A05.A08(r4, -1);
        if (th instanceof FileNotFoundException) {
            this.A02.A05(R.string.invalid_url_for_download, 1);
        }
    }
}
