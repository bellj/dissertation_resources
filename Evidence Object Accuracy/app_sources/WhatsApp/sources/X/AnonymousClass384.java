package X;

import android.content.res.Resources;
import com.whatsapp.R;
import com.whatsapp.group.GroupChatInfo;

/* renamed from: X.384  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass384 extends AbstractC16350or {
    public final C14900mE A00;
    public final C15570nT A01;
    public final C14830m7 A02;
    public final C21320xE A03;
    public final C15370n3 A04;
    public final C20660w7 A05;
    public final String A06;

    public AnonymousClass384(C14900mE r1, C15570nT r2, C14830m7 r3, C21320xE r4, C15370n3 r5, C20660w7 r6, String str) {
        this.A02 = r3;
        this.A00 = r1;
        this.A01 = r2;
        this.A05 = r6;
        this.A03 = r4;
        this.A04 = r5;
        this.A06 = str;
    }

    public void A08(int i, String str) {
        C14900mE r1;
        int i2;
        GroupChatInfo groupChatInfo = (GroupChatInfo) ((AnonymousClass32Z) this).A00.get();
        if (groupChatInfo != null) {
            if (i == 403) {
                r1 = ((ActivityC13810kN) groupChatInfo).A05;
                i2 = R.string.group_error_description_not_allowed;
            } else if (i == 406) {
                int A02 = ((ActivityC13810kN) groupChatInfo).A06.A02(AbstractC15460nI.A1P);
                C14900mE r3 = ((ActivityC13810kN) groupChatInfo).A05;
                Resources resources = groupChatInfo.getResources();
                Object[] A1b = C12970iu.A1b();
                C12960it.A1P(A1b, A02, 0);
                r3.A0E(resources.getQuantityString(R.plurals.description_reach_limit, A02, A1b), 0);
                groupChatInfo.A1H.A0M(false);
                return;
            } else if (i != 409) {
                r1 = ((ActivityC13810kN) groupChatInfo).A05;
                i2 = R.string.group_error_description;
            } else {
                groupChatInfo.A1H.A0C(groupChatInfo.A1C);
                groupChatInfo.Adl(GroupChatInfo.DescriptionConflictDialogFragment.A00(str), null);
                return;
            }
            r1.A07(i2, 0);
        }
    }
}
