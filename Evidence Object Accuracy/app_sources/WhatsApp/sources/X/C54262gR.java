package X;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.whatsapp.R;

/* renamed from: X.2gR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54262gR extends AnonymousClass02M {
    public final Activity A00;
    public final AnonymousClass0QI A01;
    public final AnonymousClass4KD A02;
    public final C15610nY A03;
    public final AnonymousClass1J1 A04;
    public final C15600nX A05;
    public final AnonymousClass11F A06;
    public final AnonymousClass12F A07;
    public final boolean A08 = true;

    public C54262gR(Activity activity, AnonymousClass4KD r5, C15610nY r6, AnonymousClass1J1 r7, C15600nX r8, AnonymousClass11F r9, AnonymousClass12F r10) {
        this.A03 = r6;
        this.A07 = r10;
        this.A06 = r9;
        this.A04 = r7;
        this.A05 = r8;
        this.A00 = activity;
        this.A01 = new AnonymousClass0QI(new C55292i9(this, r6), C15370n3.class);
        this.A02 = r5;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A01.A03;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:36:? A[RETURN, SYNTHETIC] */
    @Override // X.AnonymousClass02M
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ void ANH(X.AnonymousClass03U r10, int r11) {
        /*
            r9 = this;
            X.2hc r10 = (X.C54992hc) r10
            X.0QI r0 = r9.A01
            java.lang.Object r7 = r0.A02(r11)
            X.0n3 r7 = (X.C15370n3) r7
            X.0nY r8 = r9.A03
            X.1J1 r2 = r9.A04
            X.4KD r6 = r9.A02
            X.1Vj r0 = r7.A0F
            if (r0 == 0) goto L_0x00a9
            int r1 = r0.A00
            r0 = 3
            if (r1 != r0) goto L_0x00a9
            com.whatsapp.components.button.ThumbnailButton r1 = r10.A03
            r0 = 2131231136(0x7f0801a0, float:1.8078344E38)
            r1.setImageResource(r0)
        L_0x0021:
            X.1Pb r0 = r10.A01
            r0.A06(r7)
            X.1Vj r0 = r7.A0F
            if (r0 == 0) goto L_0x0068
            int r1 = r0.A00
            r0 = 3
            if (r1 != r0) goto L_0x0068
            com.whatsapp.TextEmojiLabel r0 = r10.A00
            android.content.res.Resources r1 = r0.getResources()
            r0 = 2131886287(0x7f1200cf, float:1.9407149E38)
        L_0x0038:
            java.lang.CharSequence r1 = r1.getText(r0)
        L_0x003c:
            com.whatsapp.TextEmojiLabel r0 = r10.A00
            r2 = 0
            r0.A0G(r2, r1)
            X.1Vj r0 = r7.A0F
            if (r0 == 0) goto L_0x0060
            int r1 = r0.A00
            r0 = 3
            if (r1 != r0) goto L_0x0060
            com.whatsapp.WaImageButton r1 = r10.A02
            r1.setOnClickListener(r2)
            r0 = 4
        L_0x0051:
            r1.setVisibility(r0)
            boolean r0 = r9.A08
            if (r0 != 0) goto L_0x005f
            r1.setOnClickListener(r2)
            r0 = 4
            r1.setVisibility(r0)
        L_0x005f:
            return
        L_0x0060:
            com.whatsapp.WaImageButton r1 = r10.A02
            r0 = 5
            X.AbstractView$OnClickListenerC34281fs.A03(r1, r10, r6, r7, r0)
            r0 = 0
            goto L_0x0051
        L_0x0068:
            java.lang.Class<X.1JV> r1 = X.AnonymousClass1JV.class
            com.whatsapp.jid.Jid r0 = r7.A0B(r1)
            if (r0 == 0) goto L_0x0092
            com.whatsapp.jid.Jid r1 = X.C15370n3.A03(r7, r1)
            X.0nW r1 = (X.AbstractC15590nW) r1
            X.0nX r0 = r10.A04
            X.1YM r0 = r0.A02(r1)
            X.1JO r0 = r0.A07()
            java.util.Set r0 = r0.A00
            int r0 = r0.size()
            if (r0 > 0) goto L_0x00a0
            com.whatsapp.TextEmojiLabel r0 = r10.A00
            android.content.res.Resources r1 = r0.getResources()
            r0 = 2131893184(0x7f121bc0, float:1.9421137E38)
            goto L_0x0038
        L_0x0092:
            java.lang.Class<X.0nW> r1 = X.AbstractC15590nW.class
            com.whatsapp.jid.Jid r0 = r7.A0B(r1)
            if (r0 == 0) goto L_0x00a6
            com.whatsapp.jid.Jid r1 = X.C15370n3.A03(r7, r1)
            X.0nW r1 = (X.AbstractC15590nW) r1
        L_0x00a0:
            r0 = 1
            java.lang.String r1 = r8.A0E(r1, r0, r0)
            goto L_0x003c
        L_0x00a6:
            java.lang.String r1 = ""
            goto L_0x003c
        L_0x00a9:
            if (r2 == 0) goto L_0x00b2
            com.whatsapp.components.button.ThumbnailButton r0 = r10.A03
            r2.A06(r0, r7)
            goto L_0x0021
        L_0x00b2:
            com.whatsapp.components.button.ThumbnailButton r5 = r10.A03
            X.11F r4 = r10.A05
            android.content.res.Resources r3 = r5.getResources()
            r2 = 2131231149(0x7f0801ad, float:1.807837E38)
            android.content.res.Resources$Theme r1 = X.C12980iv.A0I(r5)
            X.51L r0 = X.AnonymousClass51L.A00
            android.graphics.drawable.Drawable r0 = r4.A00(r1, r3, r0, r2)
            r5.setImageDrawable(r0)
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C54262gR.ANH(X.03U, int):void");
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        AnonymousClass11F r4 = this.A06;
        C15610nY r2 = this.A03;
        AnonymousClass12F r5 = this.A07;
        return new C54992hc(C12960it.A0F(LayoutInflater.from(this.A00), viewGroup, R.layout.add_groups_to_parent_group_row_item), r2, this.A05, r4, r5);
    }
}
