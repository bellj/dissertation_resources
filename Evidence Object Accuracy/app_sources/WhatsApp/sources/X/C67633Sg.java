package X;

import com.google.android.exoplayer2.Timeline;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.List;

/* renamed from: X.3Sg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67633Sg implements AnonymousClass5XZ {
    public final /* synthetic */ AnonymousClass21S A00;

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void AQ4(boolean z) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void ARX(boolean z) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void ARY(boolean z) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void ASS(AnonymousClass4XL r1, int i) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void ATm(boolean z, int i) {
    }

    @Override // X.AnonymousClass5XZ
    public void ATo(C94344be r1) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void ATq(int i) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void ATr(int i) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void ATx(int i) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void AVk() {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void AWU(List list) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void AXV(Timeline timeline, int i) {
        AnonymousClass4DD.A00(this, timeline, i);
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void AXW(Timeline timeline, Object obj, int i) {
    }

    public C67633Sg(AnonymousClass21S r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5XZ
    public void ATs(AnonymousClass3A1 r5) {
        String str;
        if (r5.type == 1) {
            Exception exc = (Exception) r5.cause;
            if (exc instanceof AnonymousClass4CL) {
                AnonymousClass4CL r1 = (AnonymousClass4CL) exc;
                str = r1.codecInfo == null ? r1.getCause() instanceof C87424Bl ? "error querying decoder" : r1.secureDecoderRequired ? "error no secure decoder" : "no secure decoder" : "error instantiating decoder";
                StringBuilder A0k = C12960it.A0k("ExoPlayerVideoPlayer/error in playback errorMessage=");
                A0k.append(str);
                A0k.append(" playerid=");
                AnonymousClass21S r2 = this.A00;
                Log.e(C12960it.A0f(A0k, r2.hashCode()), r5);
                r2.A0K(r2.A0Q.getString(R.string.error_video_playback), true);
            }
        }
        str = null;
        StringBuilder A0k = C12960it.A0k("ExoPlayerVideoPlayer/error in playback errorMessage=");
        A0k.append(str);
        A0k.append(" playerid=");
        AnonymousClass21S r2 = this.A00;
        Log.e(C12960it.A0f(A0k, r2.hashCode()), r5);
        r2.A0K(r2.A0Q.getString(R.string.error_video_playback), true);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:45:0x007d, code lost:
        if (r6 != false) goto L_0x0053;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x00f5, code lost:
        if (r7 != 2) goto L_0x003e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0086  */
    /* JADX WARNING: Removed duplicated region for block: B:86:? A[RETURN, SYNTHETIC] */
    @Override // X.AnonymousClass5XZ
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void ATt(boolean r6, int r7) {
        /*
        // Method dump skipped, instructions count: 257
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C67633Sg.ATt(boolean, int):void");
    }

    @Override // X.AnonymousClass5XZ
    public void AXl(C100564m7 r5, C92524Wg r6) {
        String str;
        hashCode();
        AnonymousClass21S r3 = this.A00;
        AnonymousClass4W3 r1 = r3.A09.A00;
        if (r1 != null) {
            if (r1.A00(2) == 1) {
                str = "ExoPlayerVideoPlayer/unplayable video track";
            } else if (r1.A00(1) == 1) {
                str = "ExoPlayerVideoPlayer/unplayable audio track";
            } else {
                return;
            }
            Log.i(str);
            r3.A0K(r3.A0Q.getString(R.string.error_video_playback), true);
        }
    }
}
