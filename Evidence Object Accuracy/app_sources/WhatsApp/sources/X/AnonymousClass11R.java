package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.whatsapp.util.Log;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.11R  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass11R extends BroadcastReceiver {
    public boolean A00;
    public final Set A01 = Collections.newSetFromMap(new ConcurrentHashMap());

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        boolean z = false;
        int intExtra = intent.getIntExtra("state", 0);
        if (intExtra >= 1) {
            z = true;
        }
        if (z != this.A00) {
            this.A00 = z;
            for (AnonymousClass11Q r0 : this.A01) {
                r0.ARE(z);
            }
        }
        StringBuilder sb = new StringBuilder("headsetPluggedStateMonitor/headset ");
        sb.append(intExtra);
        Log.i(sb.toString());
    }
}
