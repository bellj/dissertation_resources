package X;

import com.facebook.redex.ViewOnClickCListenerShape7S0100000_I1_1;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.3Vv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68553Vv implements AbstractC116565Vy {
    public C48122Ek A00;
    public final AnonymousClass016 A01 = C12980iv.A0T();
    public final AnonymousClass2K0 A02;
    public final C89344Jp A03 = new C89344Jp(this);
    public final C89354Jq A04 = new C89354Jq(this);
    public final C91524Sb A05;
    public final C14850m9 A06;
    public final Map A07 = new Hashtable();
    public final AnonymousClass1WK A08 = new C112785Es(this);

    public C68553Vv(AnonymousClass2K0 r2, C14850m9 r3) {
        this.A06 = r3;
        this.A05 = new C91524Sb();
        this.A02 = r2;
        r2.A06 = this;
        r2.A05 = this;
    }

    public List A00() {
        ArrayList A0l = C12960it.A0l();
        A0l.add(new C84733zp());
        A0l.add(new C59492up(new ViewOnClickCListenerShape7S0100000_I1_1(this, 15)));
        return A0l;
    }

    public void A01() {
        this.A01.A0A(this.A05);
    }

    public final void A02() {
        Object obj;
        Object obj2;
        Map map = this.A07;
        Iterator A0o = C12960it.A0o(map);
        while (A0o.hasNext()) {
            if (((AnonymousClass4N6) A0o.next()).A00 == 2) {
                return;
            }
        }
        ArrayList A0l = C12960it.A0l();
        if (this.A06.A07(1890)) {
            C48122Ek r1 = this.A00;
            AnonymousClass1WK r3 = this.A08;
            A0l.add(new C59642v4(r1, r3));
            C48122Ek r0 = this.A00;
            if (r0 != null) {
                String str = r0.A07;
                if ("device".equals(str) || "pin_on_map".equals(str) || "manual".equals(str)) {
                    AnonymousClass4N6 r02 = (AnonymousClass4N6) C12990iw.A0l(map, 0);
                    if (r02 == null || (obj2 = r02.A01) == null) {
                        A0l.add(new AnonymousClass407(false));
                    } else {
                        AnonymousClass4T8 r12 = (AnonymousClass4T8) obj2;
                        if (r12.A03.isEmpty()) {
                            A0l.add(new C59632v3(r3));
                        } else {
                            A0l.add(new C59652v5(this.A00, this.A04, r12.A03));
                        }
                    }
                }
            }
            A0l.add(new C59622v2(this.A03));
        }
        AnonymousClass4N6 r03 = (AnonymousClass4N6) map.get(C12960it.A0V());
        if (r03 == null || (obj = r03.A01) == null) {
            A0l.add(new C84813zx());
        } else {
            A0l.add(new C59472un(this, ((C89314Jm) C12980iv.A0o((List) obj)).A00));
            A0l.add(new C84753zr());
        }
        A0l.addAll(A00());
        synchronized (C68553Vv.class) {
            C91524Sb r13 = this.A05;
            r13.A01 = 1;
            List list = r13.A04;
            list.clear();
            list.addAll(A0l);
            A01();
        }
    }

    public final void A03(int i) {
        synchronized (C68553Vv.class) {
            C91524Sb r1 = this.A05;
            r1.A01 = 4;
            r1.A00 = i;
            A01();
        }
    }

    @Override // X.AbstractC116565Vy
    public void ANR(int i) {
        AnonymousClass4N6 r1 = (AnonymousClass4N6) this.A07.get(C12980iv.A0i());
        if (r1 != null) {
            r1.A00 = 2;
        }
        A03(i);
    }

    @Override // X.AbstractC116565Vy
    public void ANS(AnonymousClass4T8 r4) {
        synchronized (C68553Vv.class) {
            AnonymousClass4N6 r1 = (AnonymousClass4N6) this.A07.get(C12980iv.A0i());
            if (r1 != null) {
                r1.A00 = 1;
                r1.A01 = r4;
            }
            A02();
        }
    }
}
