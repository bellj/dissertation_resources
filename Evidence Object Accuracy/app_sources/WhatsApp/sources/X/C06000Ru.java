package X;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0Ru  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06000Ru {
    public static final Object A04 = new Object();
    public AbstractC11510gP A00;
    public final Context A01;
    public final String A02;
    public final Map A03;

    public C06000Ru(Drawable.Callback callback, String str, Map map) {
        if (!TextUtils.isEmpty(str) && str.charAt(str.length() - 1) != '/') {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append('/');
            str = sb.toString();
        }
        this.A02 = str;
        if (!(callback instanceof View)) {
            AnonymousClass0R5.A00("LottieDrawable must be inside of a view for images to work.");
            this.A03 = new HashMap();
            return;
        }
        this.A01 = ((View) callback).getContext();
        this.A03 = map;
        this.A00 = null;
    }
}
