package X;

import android.os.Handler;
import android.os.Looper;

/* renamed from: X.29t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class HandlerC472529t extends Handler {
    public HandlerC472529t() {
    }

    public HandlerC472529t(Looper looper) {
        super(looper);
    }

    public HandlerC472529t(Looper looper, Handler.Callback callback) {
        super(looper, callback);
    }
}
