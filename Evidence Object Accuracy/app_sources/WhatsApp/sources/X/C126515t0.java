package X;

/* renamed from: X.5t0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C126515t0 {
    public final AnonymousClass1V8 A00;

    public C126515t0(AnonymousClass3CS r5) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy.A01(A0M, "xmlns", "w:pay");
        C41141sy A0O = C117295Zj.A0O(A0M);
        C41141sy.A01(A0O, "action", "upi-get-banks");
        C41141sy.A01(A0O, "version", "2");
        C117295Zj.A1H(A0O, A0M);
        r5.A00(A0M, C117305Zk.A0q(r5.A00, A0M, C12960it.A0l()));
        this.A00 = A0M.A03();
    }
}
