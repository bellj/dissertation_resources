package X;

import android.content.Context;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.util.Log;

/* renamed from: X.5o5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123875o5 extends AbstractView$OnClickListenerC34281fs {
    public final /* synthetic */ AbstractActivityC121475iK A00;

    public C123875o5(AbstractActivityC121475iK r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractView$OnClickListenerC34281fs
    public void A04(View view) {
        int i;
        AbstractActivityC121475iK r1 = this.A00;
        C004802e r3 = new C004802e(r1, R.style.FbPayDialogTheme);
        Context baseContext = r1.getBaseContext();
        String str = r1.A0V;
        if (str.equals("business")) {
            i = R.string.dyi_business_export_report_dialog;
        } else if (!str.equals("personal")) {
            Log.e(C12960it.A0d(str, C12960it.A0k("PAY: DyiReportBaseActivity/getDyiExportDialogLabelTextRes - this payment account type is not supported. Payment account type = ")));
            i = -1;
        } else {
            i = R.string.dyi_export_report_dialog;
        }
        r3.A0A(baseContext.getString(i));
        C72463ee.A0S(r3);
        C117295Zj.A0q(r3, this, 10, R.string.dyi_export_report_diablog_positive_label);
        C12970iu.A1J(r3);
    }
}
