package X;

import android.content.Context;
import com.whatsapp.components.button.ThumbnailButton;

/* renamed from: X.2Ug  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC51372Ug extends ThumbnailButton {
    public boolean A00;

    public AbstractC51372Ug(Context context) {
        super(context);
        A00();
    }
}
