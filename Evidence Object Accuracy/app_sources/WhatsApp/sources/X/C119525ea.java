package X;

import org.json.JSONObject;

/* renamed from: X.5ea  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119525ea extends C130745zu {
    public final String A00;

    public C119525ea(AnonymousClass1V8 r2) {
        super(r2);
        this.A00 = r2.A0H("url");
    }

    public C119525ea(JSONObject jSONObject) {
        super(jSONObject);
        this.A00 = jSONObject.getString("url");
    }

    @Override // X.C130745zu
    public JSONObject A01() {
        return super.A01().put("url", this.A00);
    }
}
