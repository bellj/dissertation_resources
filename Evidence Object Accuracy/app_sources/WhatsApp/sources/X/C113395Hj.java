package X;

import java.security.PublicKey;
import java.security.cert.CertPath;
import java.security.cert.CertPathChecker;
import java.security.cert.CertPathParameters;
import java.security.cert.CertPathValidatorException;
import java.security.cert.CertPathValidatorResult;
import java.security.cert.CertPathValidatorSpi;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.PKIXCertPathChecker;
import java.security.cert.PKIXCertPathValidatorResult;
import java.security.cert.PKIXParameters;
import java.security.cert.PKIXRevocationChecker;
import java.security.cert.TrustAnchor;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* renamed from: X.5Hj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C113395Hj extends CertPathValidatorSpi {
    public final AnonymousClass5S2 A00;
    public final boolean A01;

    public C113395Hj() {
        this(false);
    }

    public C113395Hj(boolean z) {
        this.A00 = new AnonymousClass5GT();
        this.A01 = z;
    }

    public static void A00(X509Certificate x509Certificate) {
        if (x509Certificate instanceof AnonymousClass5S0) {
            RuntimeException e = null;
            try {
                if (((AbstractC113485Ht) ((AnonymousClass5S0) x509Certificate)).c.A03 != null) {
                    return;
                }
            } catch (RuntimeException e2) {
                e = e2;
            }
            throw AnonymousClass4C6.A00("unable to process TBSCertificate", e);
        }
        try {
            byte[] tBSCertificate = x509Certificate.getTBSCertificate();
            if (!(tBSCertificate instanceof C114575Mg) && tBSCertificate != null) {
                new C114575Mg(AbstractC114775Na.A04(tBSCertificate));
            }
        } catch (IllegalArgumentException e3) {
            throw AnonymousClass4C6.A00(e3.getMessage(), null);
        } catch (CertificateEncodingException e4) {
            throw AnonymousClass4C6.A00("unable to process TBSCertificate", e4);
        }
    }

    @Override // java.security.cert.CertPathValidatorSpi
    public /* bridge */ /* synthetic */ CertPathChecker engineGetRevocationChecker() {
        return new C113425Hn(this.A00);
    }

    @Override // java.security.cert.CertPathValidatorSpi
    public CertPathValidatorResult engineValidate(CertPath certPath, CertPathParameters certPathParameters) {
        C112085Bz r3;
        AnonymousClass5N2 r25;
        PublicKey publicKey;
        HashSet A12;
        HashSet A122;
        if (certPathParameters instanceof PKIXParameters) {
            C93814am r1 = new C93814am((PKIXParameters) certPathParameters);
            if (certPathParameters instanceof C113415Hm) {
                C113415Hm r32 = (C113415Hm) certPathParameters;
                r1.A08 = r32.A09;
                r1.A00 = r32.A00;
            }
            r3 = new C112085Bz(r1);
        } else if (certPathParameters instanceof C112075By) {
            r3 = ((C112075By) certPathParameters).A02;
        } else if (certPathParameters instanceof C112085Bz) {
            r3 = (C112085Bz) certPathParameters;
        } else {
            StringBuilder A0k = C12960it.A0k("Parameters must be a ");
            A0k.append(PKIXParameters.class.getName());
            throw C72463ee.A0J(C12960it.A0d(" instance.", A0k));
        }
        Set set = r3.A08;
        if (set != null) {
            List<? extends Certificate> certificates = certPath.getCertificates();
            int size = certificates.size();
            X509Certificate x509Certificate = null;
            if (!certificates.isEmpty()) {
                Date date = new Date();
                Date date2 = r3.A03;
                if (date2 != null) {
                    date = new Date(date2.getTime());
                }
                PKIXParameters pKIXParameters = r3.A01;
                Set<String> initialPolicies = pKIXParameters.getInitialPolicies();
                try {
                    TrustAnchor A01 = C95644e7.A01(pKIXParameters.getSigProvider(), C72463ee.A0K(certificates, certificates.size() - 1), set);
                    if (A01 != null) {
                        A00(A01.getTrustedCert());
                        C93814am r12 = new C93814am(r3);
                        r12.A05 = Collections.singleton(A01);
                        C112085Bz r7 = new C112085Bz(r12);
                        ArrayList A0l = C12960it.A0l();
                        PKIXParameters pKIXParameters2 = r7.A01;
                        AnonymousClass5WR r33 = null;
                        for (PKIXCertPathChecker pKIXCertPathChecker : pKIXParameters2.getCertPathCheckers()) {
                            pKIXCertPathChecker.init(false);
                            if (!(pKIXCertPathChecker instanceof PKIXRevocationChecker)) {
                                A0l.add(pKIXCertPathChecker);
                            } else if (r33 == null) {
                                r33 = pKIXCertPathChecker instanceof AnonymousClass5WR ? (AnonymousClass5WR) pKIXCertPathChecker : new AnonymousClass5GI(pKIXCertPathChecker);
                            } else {
                                throw new CertPathValidatorException("only one PKIXRevocationChecker allowed");
                            }
                        }
                        if (r7.A0A && r33 == null) {
                            r33 = new C113425Hn(this.A00);
                        }
                        int i = size + 1;
                        ArrayList[] arrayListArr = new ArrayList[i];
                        for (int i2 = 0; i2 < i; i2++) {
                            arrayListArr[i2] = C12960it.A0l();
                        }
                        HashSet A123 = C12970iu.A12();
                        A123.add("2.5.29.32.0");
                        AnonymousClass5C0 r0 = new AnonymousClass5C0("2.5.29.32.0", null, C12960it.A0l(), A123, C12970iu.A12(), 0, false);
                        arrayListArr[0].add(r0);
                        AnonymousClass4Y9 r17 = new AnonymousClass4Y9();
                        HashSet A124 = C12970iu.A12();
                        int i3 = i;
                        if (pKIXParameters2.isExplicitPolicyRequired()) {
                            i3 = 0;
                        }
                        int i4 = i;
                        if (pKIXParameters2.isAnyPolicyInhibited()) {
                            i4 = 0;
                        }
                        if (pKIXParameters2.isPolicyMappingInhibited()) {
                            i = 0;
                        }
                        X509Certificate trustedCert = A01.getTrustedCert();
                        try {
                            if (trustedCert != null) {
                                r25 = C95354dZ.A02(trustedCert);
                                publicKey = trustedCert.getPublicKey();
                            } else {
                                r25 = C95354dZ.A03(A01.getCA());
                                publicKey = A01.getCAPublicKey();
                            }
                            try {
                                C95644e7.A0A(publicKey);
                                AnonymousClass5GZ r9 = r7.A09;
                                if (r9 != null) {
                                    if (!r9.A00.match((Certificate) certificates.get(0))) {
                                        throw C113385Hh.A00("Target certificate in certification path does not match targetConstraints.", null, certPath, 0);
                                    }
                                }
                                int size2 = certificates.size() - 1;
                                int i5 = size;
                                while (size2 >= 0) {
                                    int i6 = size - size2;
                                    x509Certificate = C72463ee.A0K(certificates, size2);
                                    boolean A1V = C12960it.A1V(size2, certificates.size() - 1);
                                    try {
                                        A00(x509Certificate);
                                        C95674eA.A09(publicKey, certPath, trustedCert, date, r25, r33, r7, size2, A1V);
                                        boolean z = this.A01;
                                        C95674eA.A0H(certPath, r17, size2, z);
                                        r0 = C95674eA.A07(certPath, C95674eA.A06(certPath, A124, r0, arrayListArr, size2, i4, z), size2);
                                        if (i3 > 0 || r0 != null) {
                                            if (i6 != size) {
                                                if (x509Certificate == null || x509Certificate.getVersion() != 1) {
                                                    C95674eA.A0B(certPath, size2);
                                                    r0 = C95674eA.A08(certPath, r0, arrayListArr, size2, i);
                                                    C95674eA.A0G(certPath, r17, size2);
                                                    int A0F = C72453ed.A0F(certPath, size2, i3);
                                                    int A0F2 = C72453ed.A0F(certPath, size2, i);
                                                    int A0F3 = C72453ed.A0F(certPath, size2, i4);
                                                    i3 = C95674eA.A00(certPath, size2, A0F);
                                                    i = C95674eA.A01(certPath, size2, A0F2);
                                                    i4 = C95674eA.A02(certPath, size2, A0F3);
                                                    C95674eA.A0C(certPath, size2);
                                                    if (!C72463ee.A0Z(C72453ed.A0y(certPath, size2))) {
                                                        if (i5 > 0) {
                                                            i5--;
                                                        } else {
                                                            throw C113385Hh.A00("Max path length not greater than zero", null, certPath, size2);
                                                        }
                                                    }
                                                    i5 = C95674eA.A03(certPath, size2, i5);
                                                    C95674eA.A0D(certPath, size2);
                                                    Set<String> criticalExtensionOIDs = x509Certificate.getCriticalExtensionOIDs();
                                                    if (criticalExtensionOIDs != null) {
                                                        A122 = C72453ed.A0z(criticalExtensionOIDs);
                                                    } else {
                                                        A122 = C12970iu.A12();
                                                    }
                                                    C95674eA.A0E(certPath, A0l, A122, size2);
                                                    r25 = C95354dZ.A02(x509Certificate);
                                                    try {
                                                        publicKey = C95644e7.A00(certPath.getCertificates(), this.A00, size2);
                                                        C95644e7.A0A(publicKey);
                                                        trustedCert = x509Certificate;
                                                    } catch (CertPathValidatorException e) {
                                                        throw new CertPathValidatorException("Next working key could not be retrieved.", e, certPath, size2);
                                                    }
                                                } else if (i6 != 1 || !x509Certificate.equals(A01.getTrustedCert())) {
                                                    throw new CertPathValidatorException("Version 1 certificates can't be used as CA ones.", null, certPath, size2);
                                                }
                                            }
                                            size2--;
                                        } else {
                                            throw C113385Hh.A00("No valid policy tree found when one expected.", null, certPath, size2);
                                        }
                                    } catch (AnonymousClass4C6 e2) {
                                        throw new CertPathValidatorException(e2.getMessage(), e2._underlyingException, certPath, size2);
                                    }
                                }
                                if (!C72463ee.A0Z(x509Certificate) && i3 != 0) {
                                    i3--;
                                }
                                int i7 = size2 + 1;
                                int A04 = C95674eA.A04(certPath, i7, i3);
                                Set<String> criticalExtensionOIDs2 = x509Certificate.getCriticalExtensionOIDs();
                                if (criticalExtensionOIDs2 != null) {
                                    A12 = C72453ed.A0z(criticalExtensionOIDs2);
                                    A12.remove(C95674eA.A04);
                                    A12.remove(C114715Mu.A0E.A01);
                                } else {
                                    A12 = C12970iu.A12();
                                }
                                C95674eA.A0F(certPath, A0l, A12, i7);
                                AnonymousClass5C0 A05 = C95674eA.A05(certPath, initialPolicies, A124, r7, r0, arrayListArr, i7);
                                if (A04 > 0 || A05 != null) {
                                    return new PKIXCertPathValidatorResult(A01, A05, x509Certificate.getPublicKey());
                                }
                                throw new CertPathValidatorException("Path processing failed on policy.", null, certPath, size2);
                            } catch (CertPathValidatorException e3) {
                                throw C113385Hh.A00("Algorithm identifier of public key of trust anchor could not be read.", e3, certPath, -1);
                            }
                        } catch (RuntimeException e4) {
                            throw C113385Hh.A00("Subject of trust anchor could not be (re)encoded.", e4, certPath, -1);
                        }
                    } else {
                        throw new CertPathValidatorException("Trust anchor for certification path not found.", null, certPath, -1);
                    }
                } catch (AnonymousClass4C6 e5) {
                    throw new CertPathValidatorException(e5.getMessage(), e5._underlyingException, certPath, certificates.size() - 1);
                }
            } else {
                throw new CertPathValidatorException("Certification path is empty.", null, certPath, -1);
            }
        } else {
            throw C72463ee.A0J("trustAnchors is null, this is not allowed for certification path validation.");
        }
    }
}
