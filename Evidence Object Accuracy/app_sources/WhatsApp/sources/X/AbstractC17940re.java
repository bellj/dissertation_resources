package X;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import java.util.SortedSet;

/* renamed from: X.0re  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC17940re<E> extends AbstractC17950rf<E> implements Set<E> {
    public transient AnonymousClass1Mr asList;

    public static boolean shouldTrim(int i, int i2) {
        return i < (i2 >> 1) + (i2 >> 2);
    }

    public boolean isHashCodeFast() {
        return false;
    }

    @Override // X.AbstractC17950rf, java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.Set
    public abstract AnonymousClass1I5 iterator();

    @Override // X.AbstractC17950rf
    public AnonymousClass1Mr asList() {
        AnonymousClass1Mr r0 = this.asList;
        if (r0 != null) {
            return r0;
        }
        AnonymousClass1Mr createAsList = createAsList();
        this.asList = createAsList;
        return createAsList;
    }

    public static C17960rg builderWithExpectedSize(int i) {
        C28251Mi.checkNonnegative(i, "expectedSize");
        return new C17960rg(i);
    }

    public static int chooseTableSize(int i) {
        int max = Math.max(i, 2);
        boolean z = true;
        if (max < 751619276) {
            int highestOneBit = Integer.highestOneBit(max - 1) << 1;
            while (((double) highestOneBit) * 0.7d < ((double) max)) {
                highestOneBit <<= 1;
            }
            return highestOneBit;
        }
        if (max >= 1073741824) {
            z = false;
        }
        if (z) {
            return 1073741824;
        }
        throw new IllegalArgumentException(String.valueOf("collection too large"));
    }

    public static AbstractC17940re construct(int i, Object... objArr) {
        Object[] objArr2 = objArr;
        if (i == 0) {
            return of();
        }
        if (i == 1) {
            return of(objArr[0]);
        }
        int chooseTableSize = chooseTableSize(i);
        Object[] objArr3 = new Object[chooseTableSize];
        int i2 = chooseTableSize - 1;
        int i3 = 0;
        int i4 = 0;
        for (int i5 = 0; i5 < i; i5++) {
            Object obj = objArr[i5];
            C28331Mt.checkElementNotNull(obj, i5);
            int hashCode = obj.hashCode();
            int smear = C28301Mo.smear(hashCode);
            while (true) {
                int i6 = smear & i2;
                Object obj2 = objArr3[i6];
                if (obj2 == null) {
                    objArr[i4] = obj;
                    objArr3[i6] = obj;
                    i3 += hashCode;
                    i4++;
                    break;
                } else if (!obj2.equals(obj)) {
                    smear++;
                }
            }
        }
        Arrays.fill(objArr, i4, i, (Object) null);
        if (i4 == 1) {
            return new C28341Mu(objArr[0]);
        }
        if (chooseTableSize(i4) < (chooseTableSize >> 1)) {
            return construct(i4, objArr);
        }
        if (shouldTrim(i4, objArr.length)) {
            objArr2 = Arrays.copyOf(objArr, i4);
        }
        return new C28351Mv(objArr2, i3, objArr3, i2, i4);
    }

    public static AbstractC17940re copyOf(Collection collection) {
        if ((collection instanceof AbstractC17940re) && !(collection instanceof SortedSet)) {
            AbstractC17940re r1 = (AbstractC17940re) collection;
            if (!r1.isPartialView()) {
                return r1;
            }
        }
        Object[] array = collection.toArray();
        return construct(array.length, array);
    }

    public static AbstractC17940re copyOf(Object[] objArr) {
        int length = objArr.length;
        if (length == 0) {
            return of();
        }
        if (length != 1) {
            return construct(length, (Object[]) objArr.clone());
        }
        return of(objArr[0]);
    }

    public AnonymousClass1Mr createAsList() {
        return AnonymousClass1Mr.asImmutableList(toArray());
    }

    @Override // java.util.Collection, java.lang.Object, java.util.Set
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof AbstractC17940re) || !isHashCodeFast() || !((AbstractC17940re) obj).isHashCodeFast() || hashCode() == obj.hashCode()) {
            return C28281Ml.equalsImpl(this, obj);
        }
        return false;
    }

    @Override // java.util.Collection, java.lang.Object, java.util.Set
    public int hashCode() {
        return C28281Ml.hashCodeImpl(this);
    }

    public static AbstractC17940re of() {
        return C28351Mv.EMPTY;
    }

    public static AbstractC17940re of(Object obj) {
        return new C28341Mu(obj);
    }

    public static AbstractC17940re of(Object obj, Object obj2) {
        return construct(2, obj, obj2);
    }

    public static AbstractC17940re of(Object obj, Object obj2, Object obj3) {
        return construct(3, obj, obj2, obj3);
    }

    public static AbstractC17940re of(Object obj, Object obj2, Object obj3, Object obj4, Object obj5) {
        return construct(5, obj, obj2, obj3, obj4, obj5);
    }

    public static AbstractC17940re of(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object... objArr) {
        int length = objArr.length;
        boolean z = false;
        if (length <= 2147483641) {
            z = true;
        }
        if (z) {
            int i = length + 6;
            Object[] objArr2 = new Object[i];
            objArr2[0] = obj;
            objArr2[1] = obj2;
            objArr2[2] = obj3;
            objArr2[3] = obj4;
            objArr2[4] = obj5;
            objArr2[5] = obj6;
            System.arraycopy(objArr, 0, objArr2, 6, length);
            return construct(i, objArr2);
        }
        throw new IllegalArgumentException(String.valueOf("the total number of elements must fit in an int"));
    }

    @Override // X.AbstractC17950rf
    public Object writeReplace() {
        return new C28361Mw(toArray());
    }
}
