package X;

import android.security.keystore.KeyGenParameterSpec;
import com.whatsapp.util.Log;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.security.spec.ECGenParameterSpec;
import java.util.Calendar;
import javax.security.auth.x500.X500Principal;

/* renamed from: X.5ye  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130005ye {
    public final C30931Zj A00 = C117305Zk.A0V("Secp256r1KeyStoreHelper", "infra");
    public final KeyStore A01;

    public C130005ye(KeyStore keyStore) {
        this.A01 = keyStore;
    }

    public KeyPair A00() {
        try {
            KeyGenParameterSpec build = new KeyGenParameterSpec.Builder("alias-payments-br-trusted-device-key", 4).setAlgorithmParameterSpec(new ECGenParameterSpec("secp256r1")).setKeySize(256).setDigests("SHA-256").build();
            KeyPairGenerator instance = KeyPairGenerator.getInstance("EC", "AndroidKeyStore");
            instance.initialize(build);
            return instance.generateKeyPair();
        } catch (Exception e) {
            this.A00.A0A("", e);
            return null;
        }
    }

    public KeyPair A01(String str) {
        boolean z;
        try {
            KeyStore keyStore = this.A01;
            Certificate certificate = keyStore.getCertificate(str);
            if (certificate != null) {
                if (certificate instanceof X509Certificate) {
                    try {
                        ((X509Certificate) certificate).checkValidity();
                        z = true;
                    } catch (CertificateExpiredException | CertificateNotYetValidException unused) {
                        z = false;
                    }
                    if (!z) {
                        A03(str);
                        return null;
                    }
                }
                return new KeyPair(certificate.getPublicKey(), (PrivateKey) keyStore.getKey(str, null));
            }
        } catch (Exception unused2) {
            Log.e("PAY: Secp256r1KeyStoreHelper/retrieveKeyPair failed");
        }
        return null;
    }

    public KeyPair A02(String str, Calendar calendar, Calendar calendar2) {
        try {
            KeyGenParameterSpec.Builder digests = new KeyGenParameterSpec.Builder(str, 12).setAlgorithmParameterSpec(new ECGenParameterSpec("secp256r1")).setKeySize(256).setDigests("SHA-256");
            StringBuilder A0h = C12960it.A0h();
            A0h.append("CN=");
            KeyGenParameterSpec build = digests.setCertificateSubject(new X500Principal(C12960it.A0d(str, A0h))).setCertificateSerialNumber(BigInteger.TEN).setCertificateNotBefore(calendar.getTime()).setCertificateNotAfter(calendar2.getTime()).setKeyValidityStart(calendar.getTime()).setKeyValidityEnd(calendar2.getTime()).build();
            KeyPairGenerator instance = KeyPairGenerator.getInstance("EC", "AndroidKeyStore");
            instance.initialize(build);
            return instance.generateKeyPair();
        } catch (Exception e) {
            this.A00.A0A("", e);
            return null;
        }
    }

    public void A03(String str) {
        try {
            KeyStore keyStore = this.A01;
            if (keyStore != null && keyStore.containsAlias(str)) {
                keyStore.deleteEntry(str);
            }
        } catch (KeyStoreException unused) {
            Log.e("PAY: Secp256r1KeyStoreHelper/deleteKeyPair failed");
        }
    }
}
