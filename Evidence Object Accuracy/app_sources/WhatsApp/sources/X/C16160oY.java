package X;

import android.util.Base64;
import com.facebook.redex.RunnableBRunnable0Shape1S0100000_I0_1;
import com.whatsapp.util.Log;
import java.util.concurrent.CopyOnWriteArrayList;
import org.whispersystems.curve25519.NativeVOPRFExtension;

/* renamed from: X.0oY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16160oY {
    public CopyOnWriteArrayList A00;
    public NativeVOPRFExtension A01;
    public final int A02;
    public final C16240og A03;
    public final C16190ob A04;
    public final C16180oa A05;
    public final AnonymousClass2N3 A06;
    public final C14830m7 A07;
    public final C14850m9 A08;
    public final ExecutorC27271Gr A09;
    public final String A0A = "WA_BizDirectorySearch";
    public volatile int A0B;
    public volatile long A0C;
    public volatile long A0D;
    public volatile String A0E;
    public volatile boolean A0F;
    public volatile boolean A0G = false;
    public volatile byte[] A0H;
    public volatile byte[] A0I;

    public C16160oY(C16240og r4, C16190ob r5, C16180oa r6, AnonymousClass2N3 r7, C14830m7 r8, C14850m9 r9, AbstractC14440lR r10) {
        this.A07 = r8;
        this.A08 = r9;
        this.A05 = r6;
        this.A06 = r7;
        r7.A00 = this;
        this.A04 = r5;
        this.A03 = r4;
        this.A00 = new CopyOnWriteArrayList();
        this.A09 = new ExecutorC27271Gr(r10, false);
        this.A02 = "WA_BizDirectorySearch".equals("WA_BizDirectorySearch") ? 2 : 1;
    }

    public final synchronized void A00() {
        this.A0E = null;
        this.A0H = null;
        this.A0B = 0;
        this.A0G = false;
        this.A0F = false;
        C16180oa r3 = this.A05;
        r3.A04("original_token_string", null);
        r3.A04("next_original_token_string", null);
        r3.A03("base_timestamp", 0);
        r3.A03("time_to_live_in_seconds", 0);
        r3.A04("blinding_factor_string", null);
        r3.A02("redeem_count", -1);
        r3.A04("shared_secret_string", null);
    }

    public final void A01(int i) {
        int i2 = this.A0B;
        C16180oa r3 = this.A05;
        if (i2 < r3.A00().getInt("max_sign_retry_count", 0)) {
            this.A0B++;
            this.A09.A02(new RunnableBRunnable0Shape1S0100000_I0_1(this, 42), r3.A00().getLong("sign_retry_interval_sec", 0) * ((long) this.A0B) * ((long) this.A0B) * 1000);
            return;
        }
        int i3 = 10;
        if (i == 5) {
            i3 = 9;
        }
        r3.A01(i3);
        A03(false);
    }

    public synchronized void A02(boolean z) {
        if (!this.A0F) {
            this.A0F = true;
            NativeVOPRFExtension nativeVOPRFExtension = this.A01;
            if (nativeVOPRFExtension == null) {
                nativeVOPRFExtension = new NativeVOPRFExtension();
                this.A01 = nativeVOPRFExtension;
            }
            C16180oa r5 = this.A05;
            byte[] bArr = new byte[r5.A00().getInt("token_length", 0)];
            AnonymousClass23U r3 = nativeVOPRFExtension.A00;
            r3.A00(bArr);
            this.A0I = bArr;
            byte[] bArr2 = null;
            int i = 0;
            while (i < 256) {
                bArr2 = new byte[r5.A00().getInt("token_length", 0)];
                r3.A00(bArr2);
                bArr2[31] = (byte) (bArr2[31] & 31);
                if (nativeVOPRFExtension.A00(bArr2)) {
                    break;
                }
                i++;
            }
            if (i >= 256) {
                Log.w("ACSToken/generateCredentialToken cannot generate valid blindingFactor");
                r5.A01(5);
            } else {
                byte[] A02 = nativeVOPRFExtension.A02(this.A0I, bArr2, r5.A00().getInt("token_length", 0));
                if (A02 == null) {
                    Log.e("ACSToken/generateCredentialToken failed to blind the token");
                    r5.A01(7);
                } else {
                    this.A0G = z;
                    this.A0H = A02;
                    if (z) {
                        r5.A04("next_original_token_string", Base64.encodeToString(this.A0I, 10));
                        r5.A04("blinding_factor_string", Base64.encodeToString(bArr2, 10));
                    } else {
                        r5.A04("original_token_string", Base64.encodeToString(this.A0I, 10));
                        r5.A04("blinding_factor_string", Base64.encodeToString(bArr2, 10));
                        r5.A04("shared_secret_string", null);
                        r5.A02("redeem_count", -1);
                        r5.A03("base_timestamp", 0);
                        r5.A03("time_to_live_in_seconds", 0);
                    }
                    this.A0B = 0;
                    if (this.A03.A04 == 2) {
                        this.A0E = this.A06.A00(this.A0H, this.A0A);
                    } else {
                        A01(5);
                    }
                }
            }
            A03(true);
        }
    }

    public final synchronized void A03(boolean z) {
        this.A0F = false;
        C16180oa r2 = this.A05;
        r2.A04("blinding_factor_string", null);
        if (this.A0G) {
            r2.A04("next_original_token_string", null);
        } else {
            r2.A04("original_token_string", null);
        }
        this.A0G = false;
        this.A0H = null;
        if (!z) {
            this.A0E = null;
            this.A0B = 0;
        }
    }
}
