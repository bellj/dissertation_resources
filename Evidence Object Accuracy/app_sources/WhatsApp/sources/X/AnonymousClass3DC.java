package X;

import android.content.Context;
import java.io.File;
import java.util.UUID;

/* renamed from: X.3DC  reason: invalid class name */
/* loaded from: classes2.dex */
public final /* synthetic */ class AnonymousClass3DC {
    public final /* synthetic */ C39741qT A00;
    public final /* synthetic */ AnonymousClass1KC A01;
    public final /* synthetic */ AnonymousClass1qN A02;
    public final /* synthetic */ C26771Et A03;
    public final /* synthetic */ AnonymousClass1qQ A04;
    public final /* synthetic */ AnonymousClass3JD A05;

    public /* synthetic */ AnonymousClass3DC(C39741qT r1, AnonymousClass1KC r2, AnonymousClass1qN r3, C26771Et r4, AnonymousClass1qQ r5, AnonymousClass3JD r6) {
        this.A03 = r4;
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = r1;
        this.A04 = r5;
        this.A05 = r6;
    }

    public final void A00(File file, boolean z) {
        C26771Et r1 = this.A03;
        AnonymousClass1qN r3 = this.A02;
        AnonymousClass1KC r5 = this.A01;
        C39741qT r4 = this.A00;
        AnonymousClass1qQ r0 = this.A04;
        AnonymousClass3JD r9 = this.A05;
        if (z && file != null) {
            r3.A02.A04(new C91414Rq(file, file.getName(), C26521Du.A03(C26521Du.A01(file)), file.length()));
        }
        AnonymousClass3Y9 r8 = new AbstractC39761qV(r5, r3, r1, r0) { // from class: X.3Y9
            public final /* synthetic */ AnonymousClass1KC A00;
            public final /* synthetic */ AnonymousClass1qN A01;
            public final /* synthetic */ C26771Et A02;
            public final /* synthetic */ AnonymousClass1qQ A03;

            {
                this.A02 = r3;
                this.A00 = r1;
                this.A03 = r4;
                this.A01 = r2;
            }

            @Override // X.AbstractC39761qV
            public final void AQa(AbstractC39731qS r10) {
                C26771Et r42 = this.A02;
                AnonymousClass1KC r7 = this.A00;
                AnonymousClass1qQ r32 = this.A03;
                AnonymousClass1qN r2 = this.A01;
                if (r10.A02) {
                    C26811Ex r12 = r42.A02;
                    String str = r32.A08;
                    Context context = r42.A00.A00;
                    if (str != null) {
                        AnonymousClass458 r33 = new AnonymousClass458(context, r12.A00, r12.A02, r7, str);
                        r12.A01(r33.A03, r33);
                    }
                }
                r2.A04.A04(r10);
            }
        };
        C39771qW r7 = r3.A06;
        C39781qX r6 = r3.A05;
        StringBuilder A0h = C12960it.A0h();
        C12970iu.A1V(UUID.randomUUID(), A0h);
        r1.A04.A01(new C39121pK(r4, r5, r6, r7, r8, r9, r1.A00(file, C12960it.A0d(".mp4", A0h)), file, r0.A02, r0.A03), C14370lK.A04);
    }
}
