package X;

import com.whatsapp.util.Log;

/* renamed from: X.1ja  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C36261ja {
    public final AbstractC14640lm A00;
    public final boolean A01;

    public C36261ja(AbstractC14640lm r1, boolean z) {
        this.A00 = r1;
        this.A01 = z;
    }

    public void A00(int i) {
        if (!(this instanceof AnonymousClass2Q8)) {
            StringBuilder sb = new StringBuilder("locationssubscriberesponsehandler/error ");
            sb.append(i);
            Log.e(sb.toString());
            return;
        }
        ((AnonymousClass2Q8) this).A00.A00(i);
    }

    public void A01(int i) {
        Log.i("locationssubscriberesponsehandler/subscription list updated");
    }

    public void A02(int i) {
        StringBuilder sb = new StringBuilder("locationssubscriberesponsehandler/success ");
        sb.append(i);
        Log.i(sb.toString());
    }
}
