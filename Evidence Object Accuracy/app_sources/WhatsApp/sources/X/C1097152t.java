package X;

import androidx.core.view.inputmethod.EditorInfoCompat;

/* renamed from: X.52t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1097152t implements AbstractC116455Vm {
    public final /* synthetic */ C33421e0 A00;

    public C1097152t(C33421e0 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC116455Vm
    public void AMr() {
        C12960it.A0v(this.A00.A05);
    }

    @Override // X.AbstractC116455Vm
    public void APc(int[] iArr) {
        AbstractC36671kL.A08(this.A00.A05, iArr, EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH);
    }
}
