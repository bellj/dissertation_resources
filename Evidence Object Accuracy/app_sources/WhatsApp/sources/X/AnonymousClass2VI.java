package X;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import com.google.android.gms.maps.model.LatLng;
import com.whatsapp.R;
import com.whatsapp.WaButton;
import com.whatsapp.components.button.ThumbnailButton;
import com.whatsapp.jid.UserJid;
import com.whatsapp.location.WaMapView;
import com.whatsapp.util.ViewOnClickCListenerShape4S0200000_I0;

/* renamed from: X.2VI  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2VI extends FrameLayout implements AnonymousClass004 {
    public FrameLayout A00;
    public WaButton A01;
    public ThumbnailButton A02;
    public AnonymousClass1J1 A03;
    public AnonymousClass2P7 A04;
    public boolean A05;
    public final C15570nT A06;
    public final AnonymousClass130 A07;
    public final C14830m7 A08;
    public final C20830wO A09;
    public final C16030oK A0A;
    public final C244415n A0B;
    public final WaMapView A0C;

    public AnonymousClass2VI(Context context, C15570nT r3, AnonymousClass130 r4, AnonymousClass1J1 r5, C14830m7 r6, C20830wO r7, C16030oK r8, C244415n r9) {
        super(context);
        if (!this.A05) {
            this.A05 = true;
            generatedComponent();
        }
        this.A08 = r6;
        this.A06 = r3;
        this.A0B = r9;
        this.A07 = r4;
        this.A03 = r5;
        this.A0A = r8;
        this.A09 = r7;
        FrameLayout.inflate(context, R.layout.search_row_location_map_preview, this);
        this.A0C = (WaMapView) AnonymousClass028.A0D(this, R.id.search_map_preview_map);
        this.A01 = (WaButton) AnonymousClass028.A0D(this, R.id.search_map_preview_thumb_button);
        this.A00 = (FrameLayout) AnonymousClass028.A0D(this, R.id.search_map_preview_avatar_container);
        this.A02 = (ThumbnailButton) AnonymousClass028.A0D(this, R.id.search_map_preview_contact_thumbnail);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A04;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A04 = r0;
        }
        return r0.generatedComponent();
    }

    private void setMessage(AnonymousClass1XO r9) {
        this.A00.setVisibility(8);
        WaMapView waMapView = this.A0C;
        C244415n r6 = this.A0B;
        LatLng latLng = new LatLng(((AnonymousClass1XP) r9).A00, ((AnonymousClass1XP) r9).A01);
        waMapView.A01(latLng, null, r6);
        waMapView.A00(latLng);
        if (((AnonymousClass1XP) r9).A01 != 0.0d || ((AnonymousClass1XP) r9).A00 != 0.0d) {
            WaButton waButton = this.A01;
            waButton.setOnClickListener(new ViewOnClickCListenerShape4S0200000_I0(r9, 26, this));
            waButton.setContentDescription(getContext().getString(R.string.location_button));
        }
    }

    public void setMessage(AnonymousClass1XP r3) {
        this.A0C.setVisibility(0);
        if (r3 instanceof AnonymousClass1XO) {
            setMessage((AnonymousClass1XO) r3);
        } else {
            setMessage((C30341Xa) r3);
        }
    }

    private void setMessage(C30341Xa r8) {
        long A04;
        C15370n3 A01;
        this.A00.setVisibility(0);
        C16030oK r1 = this.A0A;
        boolean z = r8.A0z.A02;
        if (z) {
            A04 = r1.A05(r8);
        } else {
            A04 = r1.A04(r8);
        }
        boolean A02 = C65173Ik.A02(this.A08, r8, A04);
        WaMapView waMapView = this.A0C;
        C244415n r12 = this.A0B;
        waMapView.A02(r12, r8, A02);
        Context context = getContext();
        C15570nT r5 = this.A06;
        View.OnClickListener A00 = C65173Ik.A00(context, r5, r12, r8, A02);
        WaButton waButton = this.A01;
        waButton.setOnClickListener(A00);
        waButton.setContentDescription(getContext().getString(R.string.conversation_row_live_location_button));
        ThumbnailButton thumbnailButton = this.A02;
        AnonymousClass130 r3 = this.A07;
        AnonymousClass1J1 r2 = this.A03;
        C20830wO r13 = this.A09;
        if (z) {
            r5.A08();
            A01 = r5.A01;
            AnonymousClass009.A05(A01);
        } else {
            UserJid A0C = r8.A0C();
            if (A0C != null) {
                A01 = r13.A01(A0C);
            } else {
                r3.A05(thumbnailButton, R.drawable.avatar_contact);
                return;
            }
        }
        r2.A06(thumbnailButton, A01);
    }
}
