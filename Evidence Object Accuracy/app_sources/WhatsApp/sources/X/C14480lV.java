package X;

/* renamed from: X.0lV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C14480lV {
    public final boolean A00;
    public final boolean A01;
    public final boolean A02;

    public C14480lV(boolean z, boolean z2, boolean z3) {
        this.A02 = z;
        this.A01 = z2;
        this.A00 = z3;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("[interactive=");
        sb.append(this.A02);
        sb.append(", has_status=");
        sb.append(this.A01);
        sb.append(", has_nonstatus=");
        sb.append(this.A00);
        sb.append("]");
        return sb.toString();
    }
}
