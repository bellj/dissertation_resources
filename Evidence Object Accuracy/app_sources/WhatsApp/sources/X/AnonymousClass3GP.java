package X;

import android.content.Context;
import android.os.Handler;
import java.util.Set;

/* renamed from: X.3GP  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3GP {
    public static final C63703Cp A00 = new C63703Cp("GoogleSignInCommon", new String[0]);

    public static void A00(Context context) {
        C64813Gz.A00(context).A01();
        Set<AnonymousClass1U8> set = AnonymousClass1U8.A00;
        synchronized (set) {
        }
        for (AnonymousClass1U8 r0 : set) {
            r0.A07();
        }
        synchronized (C65863Lh.A0I) {
            C65863Lh r1 = C65863Lh.A0F;
            if (r1 != null) {
                r1.A0D.incrementAndGet();
                Handler handler = r1.A06;
                handler.sendMessageAtFrontOfQueue(handler.obtainMessage(10));
            }
        }
    }
}
