package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* renamed from: X.1F5  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1F5 {
    public final C15570nT A00;
    public final C14820m6 A01;
    public final C22100yW A02;
    public final C22130yZ A03;
    public final C234911w A04;
    public final C18770sz A05;

    public AnonymousClass1F5(C15570nT r1, C14820m6 r2, C22100yW r3, C22130yZ r4, C234911w r5, C18770sz r6) {
        this.A00 = r1;
        this.A05 = r6;
        this.A04 = r5;
        this.A01 = r2;
        this.A03 = r4;
        this.A02 = r3;
    }

    public final boolean A00(UserJid userJid, long j, long j2) {
        AnonymousClass1YF A09 = this.A05.A09(userJid);
        if (j > 0 && A09 != null) {
            long j3 = A09.A01;
            if (j < j3 && j2 == j3 && A09.A03 < this.A01.A00.getLong("adv_last_device_job_ts", 0)) {
                StringBuilder sb = new StringBuilder("contactsyncDevicesupdater/update/usync fetch error, expectedTs=");
                sb.append(j2);
                sb.append("; responseTs=");
                sb.append(j);
                Log.e(sb.toString());
                C234911w r0 = this.A04;
                r0.A00.A07(new C42041uZ());
                return true;
            }
        }
        return false;
    }
}
