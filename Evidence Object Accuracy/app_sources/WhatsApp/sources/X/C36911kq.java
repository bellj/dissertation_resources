package X;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.conversationslist.ViewHolder;
import com.whatsapp.search.IteratingPlayer;
import com.whatsapp.search.SearchViewModel;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.util.HashSet;
import java.util.Map;
import org.chromium.net.UrlRequest;

/* renamed from: X.1kq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C36911kq extends AnonymousClass02M implements AbstractC33071dF {
    public RecyclerView A00;
    public String A01 = "";
    public HashSet A02 = new HashSet();
    public Map A03;
    public boolean A04 = false;
    public final Activity A05;
    public final AbstractC009904y A06;
    public final AbstractC05270Ox A07 = new C54752hE(this);
    public final AnonymousClass12P A08;
    public final C253519b A09;
    public final AbstractC15710nm A0A;
    public final AnonymousClass4JF A0B;
    public final C14900mE A0C;
    public final C15570nT A0D;
    public final C15450nH A0E;
    public final AnonymousClass11L A0F;
    public final C14650lo A0G;
    public final C238013b A0H;
    public final C16430p0 A0I;
    public final AnonymousClass130 A0J;
    public final C15550nR A0K;
    public final C15610nY A0L;
    public final AnonymousClass1J1 A0M;
    public final C63563Cb A0N;
    public final C63543Bz A0O;
    public final AnonymousClass01d A0P;
    public final C14830m7 A0Q;
    public final C16590pI A0R;
    public final C15890o4 A0S;
    public final C14820m6 A0T;
    public final AnonymousClass018 A0U;
    public final C19990v2 A0V;
    public final C20830wO A0W;
    public final C241714m A0X;
    public final C15600nX A0Y;
    public final C236812p A0Z;
    public final AnonymousClass10Y A0a;
    public final AnonymousClass1BK A0b;
    public final C21400xM A0c;
    public final C15670ni A0d;
    public final AnonymousClass19M A0e;
    public final C14850m9 A0f;
    public final C20710wC A0g;
    public final C16030oK A0h;
    public final C244415n A0i;
    public final AnonymousClass13H A0j;
    public final C22710zW A0k;
    public final C17070qD A0l;
    public final AnonymousClass14X A0m;
    public final C16630pM A0n;
    public final IteratingPlayer A0o;
    public final C67563Rz A0p;
    public final C44091yC A0q;
    public final SearchViewModel A0r;
    public final C15860o1 A0s;
    public final AnonymousClass12F A0t;
    public final AnonymousClass1CY A0u;
    public final AbstractC14440lR A0v;
    public final C39091pH A0w;

    public static boolean A00(int i) {
        return i == 9 || i == 8 || i == 10;
    }

    public C36911kq(Activity activity, AbstractC009904y r7, AnonymousClass12P r8, C253519b r9, AbstractC15710nm r10, AnonymousClass4JF r11, C14900mE r12, C15570nT r13, C15450nH r14, AnonymousClass11L r15, C14650lo r16, C238013b r17, C16430p0 r18, AnonymousClass130 r19, C15550nR r20, C15610nY r21, AnonymousClass1J1 r22, AnonymousClass01d r23, C14830m7 r24, C16590pI r25, C15890o4 r26, C14820m6 r27, AnonymousClass018 r28, C19990v2 r29, C20830wO r30, C241714m r31, C15600nX r32, C236812p r33, AnonymousClass10Y r34, AnonymousClass1BK r35, C21400xM r36, C15670ni r37, AnonymousClass19M r38, C14850m9 r39, C20710wC r40, C16030oK r41, C244415n r42, AnonymousClass13H r43, C22710zW r44, C17070qD r45, AnonymousClass14X r46, C16630pM r47, IteratingPlayer iteratingPlayer, SearchViewModel searchViewModel, C15860o1 r50, AnonymousClass12F r51, AnonymousClass1CY r52, AbstractC14440lR r53, C39091pH r54) {
        this.A0Q = r24;
        this.A0f = r39;
        this.A05 = activity;
        this.A0j = r43;
        this.A06 = r7;
        this.A0u = r52;
        this.A0C = r12;
        this.A0A = r10;
        this.A0v = r53;
        this.A0D = r13;
        this.A0R = r25;
        this.A0V = r29;
        this.A0e = r38;
        this.A0E = r14;
        this.A0i = r42;
        this.A0m = r46;
        this.A08 = r8;
        this.A0J = r19;
        this.A0K = r20;
        this.A09 = r9;
        this.A0P = r23;
        this.A0L = r21;
        this.A0U = r28;
        this.A0l = r45;
        this.A0X = r31;
        this.A0b = r35;
        this.A0r = searchViewModel;
        this.A0o = iteratingPlayer;
        this.A0H = r17;
        this.A0g = r40;
        this.A0a = r34;
        this.A0t = r51;
        this.A0s = r50;
        this.A0M = r22;
        this.A0w = r54;
        this.A0d = r37;
        this.A0S = r26;
        this.A0c = r36;
        this.A0T = r27;
        this.A0Z = r33;
        this.A0k = r44;
        this.A0G = r16;
        this.A0h = r41;
        this.A0Y = r32;
        this.A0n = r47;
        this.A0F = r15;
        this.A0W = r30;
        this.A0B = r11;
        this.A0I = r18;
        this.A0N = new C63563Cb(new ExecutorC27271Gr(r53, true));
        this.A0p = new C67563Rz(this, this);
        this.A0O = new C63543Bz(activity);
        this.A0q = new C44091yC(r25, r28);
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void A08(AnonymousClass03U r2) {
        AbstractC51682Vy r22 = (AbstractC51682Vy) r2;
        r22.A08();
        this.A02.add(r22);
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void A09(AnonymousClass03U r2) {
        AbstractC51682Vy r22 = (AbstractC51682Vy) r2;
        r22.A09();
        this.A02.remove(r22);
    }

    @Override // X.AnonymousClass02M
    public void A0A(AnonymousClass03U r1) {
        ((AbstractC51682Vy) r1).A0A();
    }

    @Override // X.AnonymousClass02M
    public void A0B(RecyclerView recyclerView) {
        recyclerView.A0n(this.A07);
        recyclerView.A0n(this.A0o.A04);
        this.A00 = recyclerView;
    }

    @Override // X.AnonymousClass02M
    public void A0C(RecyclerView recyclerView) {
        recyclerView.A0o(this.A07);
        IteratingPlayer iteratingPlayer = this.A0o;
        recyclerView.A0o(iteratingPlayer.A04);
        iteratingPlayer.A01();
        this.A00 = null;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A0q.size();
    }

    @Override // X.AbstractC33071dF
    public int ADJ(int i) {
        while (i >= 0) {
            if (AJU(i)) {
                return i;
            }
            i--;
        }
        return -1;
    }

    @Override // X.AbstractC33071dF
    public boolean AJU(int i) {
        if (i == -1) {
            return false;
        }
        int A00 = this.A0q.A00(i);
        return A00 == 1 || A00 == 12;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0016, code lost:
        if (r3 != r1.A00) goto L_0x0018;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x0347, code lost:
        if (r3.A07(442) != false) goto L_0x0349;
     */
    @Override // X.AnonymousClass02M
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ void ANH(X.AnonymousClass03U r19, int r20) {
        /*
        // Method dump skipped, instructions count: 931
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C36911kq.ANH(X.03U, int):void");
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        if (i == -1) {
            return new AnonymousClass445(new View(viewGroup.getContext()));
        }
        if (i != 99) {
            if (i == 1) {
                return new AnonymousClass447(new C52972c9(viewGroup.getContext()));
            }
            if (!(i == 2 || i == 3)) {
                if (i == 4) {
                    return new AnonymousClass449(new C620834j(viewGroup.getContext()));
                }
                switch (i) {
                    case 6:
                        return new AnonymousClass448(new C620934k(viewGroup.getContext()));
                    case 7:
                        Context context = viewGroup.getContext();
                        C14830m7 r12 = this.A0Q;
                        C15570nT r6 = this.A0D;
                        AnonymousClass19M r14 = this.A0e;
                        C63543Bz r10 = this.A0O;
                        C15550nR r7 = this.A0K;
                        AnonymousClass01d r11 = this.A0P;
                        C15610nY r8 = this.A0L;
                        AnonymousClass018 r13 = this.A0U;
                        AnonymousClass12F r2 = this.A0t;
                        C16630pM r15 = this.A0n;
                        SearchViewModel searchViewModel = this.A0r;
                        C621134m r4 = new C621134m(context, r6, r7, r8, this.A0N, r10, r11, r12, r13, r14, r15, r2);
                        r4.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                        return new AnonymousClass44H(searchViewModel, r4);
                    case 8:
                        return new AnonymousClass44K(this.A0r, new C621534v(viewGroup.getContext()));
                    case 9:
                        return new AnonymousClass44K(this.A0r, new AnonymousClass34f(viewGroup.getContext()));
                    case 10:
                        return new AnonymousClass44K(this.A0r, new AnonymousClass34u(viewGroup.getContext()));
                    case 11:
                        C14830m7 r1 = this.A0Q;
                        AnonymousClass1CY r16 = this.A0u;
                        C14900mE r17 = this.A0C;
                        AbstractC15710nm r18 = this.A0A;
                        C15570nT r19 = this.A0D;
                        AbstractC14440lR r110 = this.A0v;
                        AnonymousClass19M r142 = this.A0e;
                        C15450nH r122 = this.A0E;
                        C63543Bz r112 = this.A0O;
                        AnonymousClass12P r102 = this.A08;
                        C15550nR r9 = this.A0K;
                        AnonymousClass01d r82 = this.A0P;
                        C15610nY r72 = this.A0L;
                        AnonymousClass018 r62 = this.A0U;
                        AnonymousClass12F r5 = this.A0t;
                        C15670ni r42 = this.A0d;
                        C15890o4 r3 = this.A0S;
                        C16630pM r22 = this.A0n;
                        SearchViewModel searchViewModel2 = this.A0r;
                        AnonymousClass2Uy r132 = new AnonymousClass2Uy(viewGroup.getContext(), r102, r18, r17, r19, r122, r9, r72, this.A0N, r112, r82, r1, r3, r62, r42, r142, r22, r5, r16, r110);
                        r132.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                        return new AnonymousClass44E(searchViewModel2, r132);
                    case 12:
                        return new AnonymousClass446(new C52972c9(viewGroup.getContext()));
                    case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                        return new C617231w(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.search_no_result, viewGroup, false));
                    case UrlRequest.Status.READING_RESPONSE /* 14 */:
                        Context context2 = viewGroup.getContext();
                        C14830m7 r123 = this.A0Q;
                        C15570nT r63 = this.A0D;
                        AnonymousClass19M r143 = this.A0e;
                        C63543Bz r103 = this.A0O;
                        C15550nR r73 = this.A0K;
                        AnonymousClass01d r113 = this.A0P;
                        C15610nY r83 = this.A0L;
                        AnonymousClass018 r133 = this.A0U;
                        AnonymousClass12F r23 = this.A0t;
                        C16630pM r152 = this.A0n;
                        SearchViewModel searchViewModel3 = this.A0r;
                        AnonymousClass34b r43 = new AnonymousClass34b(context2, r63, r73, r83, this.A0N, r103, r113, r123, r133, r143, r152, r23);
                        r43.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                        return new AnonymousClass44D(searchViewModel3, r43);
                    case 15:
                        Context context3 = viewGroup.getContext();
                        C14830m7 r124 = this.A0Q;
                        C15570nT r64 = this.A0D;
                        AnonymousClass19M r144 = this.A0e;
                        C63543Bz r104 = this.A0O;
                        C15550nR r74 = this.A0K;
                        AnonymousClass01d r114 = this.A0P;
                        C15610nY r84 = this.A0L;
                        AnonymousClass018 r134 = this.A0U;
                        AnonymousClass12F r24 = this.A0t;
                        C16630pM r153 = this.A0n;
                        SearchViewModel searchViewModel4 = this.A0r;
                        AnonymousClass34d r44 = new AnonymousClass34d(context3, r64, r74, r84, this.A0N, r104, r114, r124, r134, r144, r153, r24);
                        r44.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                        return new AnonymousClass44I(searchViewModel4, r44);
                    case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                        Context context4 = viewGroup.getContext();
                        C14830m7 r115 = this.A0Q;
                        C15570nT r52 = this.A0D;
                        AnonymousClass19M r135 = this.A0e;
                        C63543Bz r92 = this.A0O;
                        C15550nR r65 = this.A0K;
                        AnonymousClass01d r105 = this.A0P;
                        C15610nY r75 = this.A0L;
                        AnonymousClass018 r125 = this.A0U;
                        AnonymousClass12F r154 = this.A0t;
                        return new AnonymousClass44G(this.A0r, new C621234o(context4, r52, r65, r75, this.A0N, r92, r105, r115, r125, r135, this.A0n, r154));
                    case 17:
                        Context context5 = viewGroup.getContext();
                        C14830m7 r116 = this.A0Q;
                        C15570nT r53 = this.A0D;
                        AnonymousClass19M r136 = this.A0e;
                        C63543Bz r93 = this.A0O;
                        C15550nR r66 = this.A0K;
                        AnonymousClass01d r106 = this.A0P;
                        C15610nY r76 = this.A0L;
                        AnonymousClass018 r126 = this.A0U;
                        AnonymousClass12F r155 = this.A0t;
                        return new AnonymousClass44G(this.A0r, new C621334p(context5, r53, r66, r76, this.A0N, r93, r106, r116, r126, r136, this.A0n, r155));
                    case 18:
                        Context context6 = viewGroup.getContext();
                        C14830m7 r117 = this.A0Q;
                        C15570nT r54 = this.A0D;
                        AnonymousClass19M r137 = this.A0e;
                        C63543Bz r94 = this.A0O;
                        C15550nR r67 = this.A0K;
                        AnonymousClass01d r107 = this.A0P;
                        C15610nY r77 = this.A0L;
                        AnonymousClass018 r127 = this.A0U;
                        AnonymousClass12F r156 = this.A0t;
                        return new AnonymousClass44G(this.A0r, new AnonymousClass34q(context6, r54, r67, r77, this.A0N, r94, r107, r117, r127, r137, this.A0n, r156));
                    case 19:
                        Context context7 = viewGroup.getContext();
                        C14830m7 r128 = this.A0Q;
                        C15570nT r68 = this.A0D;
                        AnonymousClass19M r157 = this.A0e;
                        C63543Bz r108 = this.A0O;
                        C15550nR r78 = this.A0K;
                        AnonymousClass01d r118 = this.A0P;
                        C15610nY r85 = this.A0L;
                        AnonymousClass018 r138 = this.A0U;
                        AnonymousClass1BK r145 = this.A0b;
                        AnonymousClass12F r32 = this.A0t;
                        C16630pM r25 = this.A0n;
                        SearchViewModel searchViewModel5 = this.A0r;
                        C63563Cb r95 = this.A0N;
                        C621434s r45 = new C621434s(context7, r68, r78, r85, r95, r108, r118, r128, r138, r145, r157, r25, r32);
                        r45.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                        return new AnonymousClass44J(r78, r95, searchViewModel5, r45);
                    case C43951xu.A01 /* 20 */:
                        Context context8 = viewGroup.getContext();
                        C14830m7 r111 = this.A0Q;
                        C15570nT r119 = this.A0D;
                        AnonymousClass19M r158 = this.A0e;
                        C63543Bz r139 = this.A0O;
                        C244415n r129 = this.A0i;
                        AnonymousClass130 r1110 = this.A0J;
                        C15550nR r109 = this.A0K;
                        SearchViewModel searchViewModel6 = this.A0r;
                        AnonymousClass01d r86 = this.A0P;
                        C15610nY r79 = this.A0L;
                        AnonymousClass018 r69 = this.A0U;
                        AnonymousClass12F r55 = this.A0t;
                        AnonymousClass1J1 r46 = this.A0M;
                        C16030oK r33 = this.A0h;
                        C16630pM r26 = this.A0n;
                        AnonymousClass34e r0 = new AnonymousClass34e(context8, r119, r1110, r109, r79, r46, this.A0N, r139, r86, r111, r69, this.A0W, r158, r33, r129, r26, r55);
                        r0.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                        return new AnonymousClass44F(searchViewModel6, r0);
                    case 21:
                        Context context9 = viewGroup.getContext();
                        C14830m7 r120 = this.A0Q;
                        C14900mE r159 = this.A0C;
                        C15570nT r1310 = this.A0D;
                        AnonymousClass19M r1210 = this.A0e;
                        C63543Bz r1111 = this.A0O;
                        AnonymousClass130 r1010 = this.A0J;
                        C15550nR r96 = this.A0K;
                        AnonymousClass01d r87 = this.A0P;
                        C15610nY r710 = this.A0L;
                        AnonymousClass018 r610 = this.A0U;
                        AnonymousClass12F r56 = this.A0t;
                        C39091pH r47 = this.A0w;
                        SearchViewModel searchViewModel7 = this.A0r;
                        AnonymousClass34c r02 = new AnonymousClass34c(context9, r159, r1310, r1010, r96, r710, this.A0M, this.A0N, r1111, r87, r120, r610, r1210, this.A0n, r56, r47);
                        r02.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                        return new AnonymousClass44C(searchViewModel7, r02);
                    case 22:
                        return new AnonymousClass44A(new C620734i(viewGroup.getContext()));
                    case 23:
                        C16430p0 r48 = this.A0I;
                        AnonymousClass4JF r03 = this.A0B;
                        Context context10 = viewGroup.getContext();
                        C71573d9 r04 = r03.A00;
                        C51112Sw r27 = r04.A03;
                        C52922bu r121 = new C52922bu(context10, (AnonymousClass11I) r04.A04.A2F.get());
                        r121.A00 = (AnonymousClass11I) r27.A0Y.A2F.get();
                        return new AnonymousClass44B(r48, r121);
                    default:
                        StringBuilder sb = new StringBuilder("Invalid viewType: ");
                        sb.append(i);
                        throw new UnsupportedOperationException(sb.toString());
                }
            }
        }
        AnonymousClass3J9 r482 = AnonymousClass3J9.A03;
        if (i == 99) {
            r482 = AnonymousClass3J9.A02;
        }
        C14830m7 r130 = this.A0Q;
        C14850m9 r131 = this.A0f;
        AnonymousClass13H r140 = this.A0j;
        C15570nT r141 = this.A0D;
        C16590pI r146 = this.A0R;
        AbstractC14440lR r147 = this.A0v;
        C19990v2 r148 = this.A0V;
        C15450nH r149 = this.A0E;
        AnonymousClass14X r150 = this.A0m;
        AnonymousClass130 r151 = this.A0J;
        C15550nR r160 = this.A0K;
        C253519b r161 = this.A09;
        C241714m r162 = this.A0X;
        C15610nY r163 = this.A0L;
        AnonymousClass018 r164 = this.A0U;
        C17070qD r165 = this.A0l;
        C238013b r166 = this.A0H;
        C20710wC r167 = this.A0g;
        AnonymousClass10Y r168 = this.A0a;
        AnonymousClass12F r169 = this.A0t;
        C15860o1 r1510 = this.A0s;
        AnonymousClass1J1 r1410 = this.A0M;
        C21400xM r1112 = this.A0c;
        C14820m6 r1011 = this.A0T;
        C236812p r97 = this.A0Z;
        C22710zW r88 = this.A0k;
        C14650lo r711 = this.A0G;
        C15600nX r611 = this.A0Y;
        AnonymousClass11L r57 = this.A0F;
        C51532Vf r49 = this.A0r.A0L;
        ViewHolder viewHolder = new ViewHolder(viewGroup.getContext(), LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.conversations_row, viewGroup, false), r161, r141, r149, r57, r711, r166, r151, r160, r163, r1410, this.A0N, r49, r130, r146, r1011, r164, r148, r162, r611, r97, r168, r1112, r131, r167, r140, r88, r165, r150, r1510, r169, r482, r147);
        this.A06.A00(viewHolder);
        return viewHolder;
    }

    @Override // X.AbstractC33071dF
    public boolean AdR() {
        return Boolean.TRUE.equals(this.A0r.A09.A01());
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        return this.A0q.A00(i);
    }
}
