package X;

import java.util.Arrays;

/* renamed from: X.1Rc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C29161Rc {
    public final int A00;
    public final String A01;
    public final String A02;
    public final byte[] A03;
    public final byte[] A04;
    public final byte[] A05;
    public final byte[] A06;
    public final byte[] A07;
    public final byte[] A08;

    public C29161Rc(String str, String str2, byte[] bArr, byte[] bArr2, byte[] bArr3, byte[] bArr4, byte[] bArr5, byte[] bArr6, int i) {
        this.A00 = i;
        this.A01 = str;
        this.A02 = str2;
        this.A07 = bArr;
        this.A06 = bArr2;
        this.A08 = bArr3;
        this.A04 = bArr4;
        this.A03 = bArr5;
        this.A05 = bArr6;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("WamsysMMSUploadResponse{errorCode=");
        sb.append(this.A00);
        sb.append(", directPath='");
        sb.append(this.A01);
        sb.append('\'');
        sb.append(", downloadUrl='");
        sb.append(this.A02);
        sb.append('\'');
        sb.append(", mediaHash=");
        sb.append(Arrays.toString(this.A07));
        sb.append(", mediaEncHash=");
        sb.append(Arrays.toString(this.A06));
        sb.append(", partialMediaHash=");
        sb.append(Arrays.toString((byte[]) null));
        sb.append(", partialMediaEncHash=");
        sb.append(Arrays.toString((byte[]) null));
        sb.append('}');
        return sb.toString();
    }
}
