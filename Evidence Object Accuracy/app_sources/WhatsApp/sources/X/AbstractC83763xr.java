package X;

import android.content.Context;
import android.util.AttributeSet;
import com.whatsapp.WaFrameLayout;

/* renamed from: X.3xr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC83763xr extends WaFrameLayout {
    public AbstractC83763xr(Context context) {
        super(context);
        A01();
    }

    public AbstractC83763xr(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A01();
    }
}
