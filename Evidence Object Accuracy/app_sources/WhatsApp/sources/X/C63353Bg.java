package X;

import android.text.TextUtils;
import android.text.format.DateUtils;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.whatsapp.audiopicker.AudioPickerActivity;

/* renamed from: X.3Bg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63353Bg {
    public final int A00;
    public final int A01;
    public final String A02;
    public final String A03;
    public final String A04;
    public final String A05;
    public final String A06;
    public final String A07;
    public final /* synthetic */ AudioPickerActivity A08;

    public C63353Bg(AudioPickerActivity audioPickerActivity, String str, String str2, String str3, int i, int i2, int i3) {
        int i4 = i3;
        this.A08 = audioPickerActivity;
        this.A00 = i;
        this.A02 = (TextUtils.isEmpty(str) || str.equalsIgnoreCase("<unknown>")) ? null : str;
        this.A07 = str2;
        this.A03 = str3;
        this.A01 = i4;
        this.A05 = DateUtils.formatElapsedTime(C12980iv.A0D((long) i2));
        AnonymousClass018 r8 = ((ActivityC13830kP) audioPickerActivity).A01;
        long A02 = ((long) ((ActivityC13810kN) audioPickerActivity).A06.A02(AbstractC15460nI.A1p)) * SearchActionVerificationClientService.MS_TO_NS;
        long j = (long) i4;
        if (j >= A02 - 60000 && j < A02) {
            i4 = (int) (j - 60000);
        }
        this.A06 = (String) C44891zj.A00(r8, (long) i4, true, false).first;
        this.A04 = C38131nZ.A06(((ActivityC13830kP) audioPickerActivity).A01, (long) Math.max(0, i2));
    }
}
