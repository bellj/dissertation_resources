package X;

import android.content.Context;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.2xs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60462xs extends AnonymousClass1OY {
    public C20370ve A00;
    public AnonymousClass14X A01;
    public boolean A02;
    public final TextView A03 = C12960it.A0J(this, R.id.info);

    public C60462xs(Context context, AbstractC13890kV r3, AbstractC15340mz r4) {
        super(context, r3, r4);
        A0Z();
        A1M();
    }

    @Override // X.AnonymousClass1OZ, X.AbstractC28561Ob
    public void A0Z() {
        if (!this.A02) {
            this.A02 = true;
            AnonymousClass2P6 A07 = AnonymousClass1OY.A07(this);
            AnonymousClass01J A08 = AnonymousClass1OY.A08(A07, this);
            AnonymousClass1OY.A0L(A08, this);
            AnonymousClass1OY.A0M(A08, this);
            AnonymousClass1OY.A0K(A08, this);
            AnonymousClass1OY.A0I(A07, A08, this, AnonymousClass1OY.A09(A08, this, AnonymousClass1OY.A0B(A08, this)));
            this.A01 = (AnonymousClass14X) A08.AFM.get();
            this.A00 = (C20370ve) A08.AEu.get();
        }
    }

    @Override // X.AnonymousClass1OY
    public void A0s() {
        A1M();
        A1H(false);
    }

    @Override // X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r2, boolean z) {
        boolean A1X = C12960it.A1X(r2, getFMessage());
        super.A1D(r2, z);
        if (z || A1X) {
            A1M();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A1M() {
        /*
            r5 = this;
            android.widget.TextView r4 = r5.A03
            android.content.res.Resources r0 = r5.getResources()
            float r0 = X.AnonymousClass1OY.A00(r0)
            r4.setTextSize(r0)
            r0 = 2131231246(0x7f08020e, float:1.8078568E38)
            r4.setBackgroundResource(r0)
            X.0mz r1 = r5.getFMessage()
            boolean r0 = r1 instanceof X.AnonymousClass1XW
            if (r0 != 0) goto L_0x0026
            boolean r0 = r1 instanceof X.AnonymousClass1XY
            if (r0 != 0) goto L_0x0026
            java.lang.String r0 = "PAY: message is not FMessagePaymentRequestDeclined or FMessagePaymentRequestCancelled"
            java.lang.IllegalStateException r0 = X.C12960it.A0U(r0)
            throw r0
        L_0x0026:
            X.1XX r1 = (X.AnonymousClass1XX) r1
            java.lang.String r1 = r1.A00
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            r3 = 0
            if (r0 != 0) goto L_0x005c
            X.0ve r0 = r5.A00
            X.1IR r2 = r0.A0N(r1, r3)
            if (r2 == 0) goto L_0x005d
            X.14X r1 = r5.A01
            X.0mz r0 = r5.getFMessage()
            java.lang.String r1 = r1.A0Q(r2, r0)
        L_0x0043:
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x005f
            r0 = 37
            X.C12960it.A13(r4, r5, r2, r0)
            r4.setText(r1)
            android.content.Context r1 = r5.getContext()
            r0 = 2131099838(0x7f0600be, float:1.781204E38)
            X.C12960it.A0s(r1, r4, r0)
            return
        L_0x005c:
            r2 = r3
        L_0x005d:
            r1 = r3
            goto L_0x0043
        L_0x005f:
            r4.setOnClickListener(r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C60462xs.A1M():void");
    }

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_divider;
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_divider;
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_divider;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0007, code lost:
        if ((r3 instanceof X.AnonymousClass1XW) != false) goto L_0x0009;
     */
    @Override // X.AbstractC28551Oa
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setFMessage(X.AbstractC15340mz r3) {
        /*
            r2 = this;
            boolean r0 = r3 instanceof X.AnonymousClass1XY
            if (r0 != 0) goto L_0x0009
            boolean r1 = r3 instanceof X.AnonymousClass1XW
            r0 = 0
            if (r1 == 0) goto L_0x000a
        L_0x0009:
            r0 = 1
        L_0x000a:
            X.AnonymousClass009.A0F(r0)
            r2.A0O = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C60462xs.setFMessage(X.0mz):void");
    }
}
