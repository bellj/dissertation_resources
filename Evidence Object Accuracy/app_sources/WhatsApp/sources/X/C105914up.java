package X;

import android.os.SystemClock;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.WeakHashMap;

/* renamed from: X.4up  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C105914up implements AbstractC11570gV {
    public long A00;
    public C91754Sz A01;
    public final AbstractC12220hZ A02;
    public final C87764Cv A03;
    public final AnonymousClass4YA A04;
    public final AnonymousClass4YA A05;
    public final AnonymousClass5S7 A06;
    public final Map A07 = new WeakHashMap();

    public C105914up(AbstractC12220hZ r3, C87764Cv r4, AnonymousClass5S7 r5) {
        this.A06 = r5;
        this.A05 = new AnonymousClass4YA(new C106064v4(this, r5));
        this.A04 = new AnonymousClass4YA(new C106064v4(this, r5));
        this.A03 = r4;
        this.A02 = r3;
        this.A01 = (C91754Sz) r3.get();
        this.A00 = SystemClock.uptimeMillis();
    }

    public static void A00(AnonymousClass4SJ r2) {
        if (r2 != null) {
            AnonymousClass4I0 r0 = r2.A03;
            Object obj = r2.A04;
            AnonymousClass4Vh r1 = r0.A00;
            synchronized (r1) {
                r1.A03.remove(obj);
            }
        }
    }

    public final synchronized C08870bz A01(AnonymousClass4SJ r4) {
        AnonymousClass0RA.A01(C12960it.A1T(r4.A01 ? 1 : 0));
        r4.A00++;
        return C08870bz.A00(new C105984uw(r4, this), r4.A02.A04());
    }

    public final synchronized C08870bz A02(AnonymousClass4SJ r2) {
        return (!r2.A01 || r2.A00 != 0) ? null : r2.A02;
    }

    public final void A03() {
        ArrayList arrayList;
        Object obj;
        synchronized (this) {
            C91754Sz r0 = this.A01;
            int i = r0.A03;
            int i2 = r0.A00;
            AnonymousClass4YA r6 = this.A04;
            int A00 = r6.A00();
            AnonymousClass4YA r5 = this.A05;
            int A02 = C72463ee.A02(i2, A00 - r5.A00(), i);
            C91754Sz r02 = this.A01;
            int A022 = C72463ee.A02(r02.A02, r6.A01() - r5.A01(), r02.A04);
            int max = Math.max(A02, 0);
            int max2 = Math.max(A022, 0);
            if (r5.A00() > max || r5.A01() > max2) {
                arrayList = C12960it.A0l();
                while (true) {
                    if (r5.A00() <= max && r5.A01() <= max2) {
                        break;
                    }
                    synchronized (r5) {
                        LinkedHashMap linkedHashMap = r5.A02;
                        if (linkedHashMap.isEmpty()) {
                            obj = null;
                        } else {
                            obj = C72463ee.A0L(linkedHashMap).next();
                        }
                    }
                    r5.A02(obj);
                    arrayList.add(r6.A02(obj));
                }
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    A05((AnonymousClass4SJ) it.next());
                }
            } else {
                arrayList = null;
            }
        }
        if (arrayList != null) {
            Iterator it2 = arrayList.iterator();
            while (it2.hasNext()) {
                C08870bz A023 = A02((AnonymousClass4SJ) it2.next());
                if (A023 != null) {
                    A023.close();
                }
            }
            Iterator it3 = arrayList.iterator();
            while (it3.hasNext()) {
                A00((AnonymousClass4SJ) it3.next());
            }
        }
    }

    public final synchronized void A04() {
        if (this.A00 + this.A01.A05 <= SystemClock.uptimeMillis()) {
            this.A00 = SystemClock.uptimeMillis();
            this.A01 = (C91754Sz) this.A02.get();
        }
    }

    public final synchronized void A05(AnonymousClass4SJ r3) {
        AnonymousClass0RA.A01(C12960it.A1T(r3.A01 ? 1 : 0));
        r3.A01 = true;
    }
}
