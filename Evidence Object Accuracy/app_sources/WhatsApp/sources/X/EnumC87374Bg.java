package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.4Bg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class EnumC87374Bg extends Enum implements AnonymousClass5WQ {
    public static final /* synthetic */ EnumC87374Bg[] A00;
    public static final EnumC87374Bg A01;
    public static final EnumC87374Bg A02;

    static {
        C115025Nz r5 = new C115025Nz();
        A01 = r5;
        AnonymousClass5O0 r3 = new AnonymousClass5O0();
        A02 = r3;
        A00 = new EnumC87374Bg[]{r5, r3, new AnonymousClass5O1()};
    }

    public static EnumC87374Bg valueOf(String str) {
        return (EnumC87374Bg) Enum.valueOf(EnumC87374Bg.class, str);
    }

    public static EnumC87374Bg[] values() {
        return (EnumC87374Bg[]) A00.clone();
    }

    public /* synthetic */ EnumC87374Bg(String str, int i) {
    }
}
