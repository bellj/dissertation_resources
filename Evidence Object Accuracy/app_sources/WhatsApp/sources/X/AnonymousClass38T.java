package X;

import java.io.File;
import java.lang.ref.WeakReference;

/* renamed from: X.38T  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass38T extends AbstractC16350or {
    public WeakReference A00;
    public final C14900mE A01;
    public final C18790t3 A02;
    public final C16120oU A03;
    public final AnonymousClass4Xy A04 = new AnonymousClass4Xy();
    public final File A05;

    public AnonymousClass38T(ActivityC13810kN r2, C14900mE r3, C18790t3 r4, C16120oU r5, File file) {
        this.A01 = r3;
        this.A02 = r4;
        this.A03 = r5;
        this.A00 = C12970iu.A10(r2);
        this.A05 = file;
    }
}
