package X;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.4Sj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C91604Sj {
    public final View A00;
    public final ImageView A01;
    public final TextView A02;
    public final TextView A03;
    public final TextView A04;

    public C91604Sj(View view) {
        this.A01 = C12970iu.A0L(view, R.id.icon);
        this.A04 = C12960it.A0J(view, R.id.title);
        this.A03 = C12960it.A0J(view, R.id.size);
        this.A02 = C12960it.A0J(view, R.id.date);
        this.A00 = view.findViewById(R.id.selection_check);
    }
}
