package X;

/* renamed from: X.2J9  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2J9 implements AnonymousClass2JA {
    public final /* synthetic */ C48302Fl A00;

    public AnonymousClass2J9(C48302Fl r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass2JA
    public C59442uj A87(C48122Ek r14, AnonymousClass2K1 r15, AnonymousClass1B4 r16) {
        AnonymousClass01J r1 = this.A00.A03;
        AbstractC15710nm r2 = (AbstractC15710nm) r1.A4o.get();
        AnonymousClass018 r10 = (AnonymousClass018) r1.ANb.get();
        C14850m9 r11 = (C14850m9) r1.A04.get();
        C16340oq r8 = (C16340oq) r1.A5z.get();
        C17170qN r9 = (C17170qN) r1.AMt.get();
        return new C59442uj(r2, (C14900mE) r1.A8X.get(), (C16430p0) r1.A5y.get(), r14, r15, r16, r8, r9, r10, r11, (AbstractC14440lR) r1.ANe.get());
    }
}
