package X;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* renamed from: X.0e5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC10130e5 implements Runnable {
    public final /* synthetic */ C05230Ot A00;

    public RunnableC10130e5(C05230Ot r1) {
        this.A00 = r1;
    }

    /* JADX INFO: finally extract failed */
    public final Set A00() {
        HashSet hashSet = new HashSet();
        C05230Ot r4 = this.A00;
        AnonymousClass0QN r2 = r4.A06;
        AnonymousClass0ZK r1 = new AnonymousClass0ZK("SELECT * FROM room_table_modification_log WHERE invalidated = 1;");
        r2.A01();
        r2.A02();
        Cursor AZi = ((AnonymousClass0ZH) r2.A00).A00().A00().AZi(r1);
        while (AZi.moveToNext()) {
            try {
                hashSet.add(Integer.valueOf(AZi.getInt(0)));
            } catch (Throwable th) {
                AZi.close();
                throw th;
            }
        }
        AZi.close();
        if (!hashSet.isEmpty()) {
            ((C02980Fp) r4.A09).A00.executeUpdateDelete();
        }
        return hashSet;
    }

    /* JADX INFO: finally extract failed */
    @Override // java.lang.Runnable
    public void run() {
        ReentrantReadWriteLock.ReadLock readLock;
        AbstractC12920im r0;
        try {
            C05230Ot r3 = this.A00;
            AnonymousClass0QN r4 = r3.A06;
            readLock = r4.A09.readLock();
            Set set = null;
            try {
                readLock.lock();
                r0 = r4.A0A;
            } catch (SQLiteException | IllegalStateException e) {
                Log.e("ROOM", "Cannot run invalidation tracker. Is the db closed?", e);
            }
            if (r0 != null && ((AnonymousClass0ZE) r0).A00.isOpen()) {
                if (!r3.A0A) {
                    ((AnonymousClass0ZH) r4.A00).A00().A00();
                }
                if (!r3.A0A) {
                    Log.e("ROOM", "database is not initialized even though it is open");
                } else if (r3.A03.compareAndSet(true, false) && !((AnonymousClass0ZE) ((AnonymousClass0ZH) r4.A00).A00().A00()).A00.inTransaction()) {
                    if (r4.A05) {
                        SQLiteDatabase sQLiteDatabase = ((AnonymousClass0ZE) ((AnonymousClass0ZH) r4.A00).A00().A00()).A00;
                        sQLiteDatabase.beginTransaction();
                        try {
                            set = A00();
                            sQLiteDatabase.setTransactionSuccessful();
                            sQLiteDatabase.endTransaction();
                        } catch (Throwable th) {
                            sQLiteDatabase.endTransaction();
                            throw th;
                        }
                    } else {
                        set = A00();
                    }
                    if (set != null && !set.isEmpty()) {
                        AnonymousClass03E r2 = r3.A04;
                        synchronized (r2) {
                            Iterator it = r2.iterator();
                            if (it.hasNext()) {
                                ((Map.Entry) it.next()).getValue();
                                throw new NullPointerException("notifyByTableInvalidStatus");
                            }
                        }
                    }
                }
            }
        } finally {
            readLock.unlock();
        }
    }
}
