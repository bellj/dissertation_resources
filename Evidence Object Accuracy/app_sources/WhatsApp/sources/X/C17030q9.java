package X;

import android.os.Build;
import android.os.LocaleList;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;

/* renamed from: X.0q9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17030q9 {
    public final C43581xG A00 = new C43581xG();
    public final C22150yc A01;
    public final C14830m7 A02;
    public final C246516i A03 = new C246516i(10);
    public final HashMap A04 = new HashMap();
    public final HashSet A05 = new HashSet();

    public C17030q9(C22150yc r3, C14830m7 r4) {
        this.A02 = r4;
        this.A01 = r3;
    }

    public static AnonymousClass22G A00(C43551xD r6, String str) {
        if (!TextUtils.isEmpty(str)) {
            for (AnonymousClass22G r3 : r6.A02) {
                if (r3.A01 == 1) {
                    AnonymousClass22H r2 = (AnonymousClass22H) r3.A04;
                    if ((r2.A00 & 2) == 2 && r2.A06.equals(str)) {
                        return r3;
                    }
                }
            }
        }
        return null;
    }

    public static Locale[] A01(AnonymousClass018 r6, Locale locale) {
        Locale A00 = AnonymousClass018.A00(r6.A00);
        if (locale == null || TextUtils.isEmpty(locale.getLanguage())) {
            locale = Locale.ENGLISH;
        }
        ArrayList arrayList = new ArrayList();
        if (Build.VERSION.SDK_INT >= 24) {
            LocaleList localeList = LocaleList.getDefault();
            int size = localeList.size();
            for (int i = 0; i < size; i++) {
                arrayList.add(localeList.get(i));
            }
            if (!arrayList.contains(A00)) {
                arrayList.add(0, A00);
            }
        } else {
            arrayList.add(A00);
        }
        int size2 = arrayList.size();
        while (true) {
            size2--;
            if (size2 < 0) {
                break;
            }
            arrayList.add(size2 + 1, new Locale(((Locale) arrayList.get(size2)).getLanguage(), ""));
        }
        if (!arrayList.contains(locale)) {
            arrayList.add(locale);
        }
        return (Locale[]) arrayList.toArray(new Locale[0]);
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00c7, code lost:
        if (r2.length == 0) goto L_0x00c9;
     */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x01c0 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C43551xD A02(java.lang.String r28, java.util.Locale[] r29) {
        /*
        // Method dump skipped, instructions count: 477
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17030q9.A02(java.lang.String, java.util.Locale[]):X.1xD");
    }
}
