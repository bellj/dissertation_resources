package X;

import android.icu.text.UnicodeSet;
import android.os.Build;

/* renamed from: X.4b7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C94014b7 {
    public static final C94014b7 A02 = new C94014b7("");
    public static final boolean A03 = C12990iw.A1X(Build.VERSION.SDK_INT, 24);
    public final UnicodeSet A00;
    public final String A01;

    public C94014b7(String str) {
        UnicodeSet unicodeSet;
        this.A01 = str;
        if (A03) {
            unicodeSet = str.isEmpty() ? UnicodeSet.EMPTY : new UnicodeSet(str);
        } else {
            unicodeSet = null;
        }
        this.A00 = unicodeSet;
    }

    public boolean A00(int i) {
        int type;
        UnicodeSet unicodeSet;
        if (A03 && (unicodeSet = this.A00) != null) {
            return unicodeSet.contains(i);
        }
        String str = this.A01;
        int hashCode = str.hashCode();
        if (hashCode != -1633268329) {
            if (hashCode != -1605334735 || !str.equals("[:digit:]")) {
                return false;
            }
            return Character.isDigit(i);
        } else if (!str.equals("[:^S:]") || (type = Character.getType(i)) == 26 || type == 25 || type == 27 || type == 28) {
            return false;
        } else {
            return true;
        }
    }
}
