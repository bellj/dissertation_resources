package X;

import com.facebook.redex.RunnableBRunnable0Shape11S0200000_I1_1;

/* renamed from: X.57A  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass57A implements AnonymousClass5UJ {
    public final /* synthetic */ C37271lv A00;

    public AnonymousClass57A(C37271lv r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5UJ
    public void ALi(AbstractC14640lm r4) {
        if (r4 != null) {
            this.A00.A0U.execute(new RunnableBRunnable0Shape11S0200000_I1_1(this, 6, r4));
        }
    }
}
