package X;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.3A0  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3A0 extends Exception {
    public static final long serialVersionUID = 3026362227162912146L;
    public final String message;
    public final List throwables;

    public AnonymousClass3A0(List list) {
        this.throwables = Collections.unmodifiableList(C12980iv.A0x(list));
        StringBuilder A0h = C12960it.A0h();
        A0h.append(list.size());
        A0h.append(" exceptions occurred: ");
        Iterator it = list.iterator();
        while (it.hasNext()) {
            A0h.append(((Throwable) it.next()).getMessage());
            A0h.append(";");
        }
        this.message = A0h.toString();
    }

    @Override // java.lang.Throwable
    public String getMessage() {
        return this.message;
    }
}
