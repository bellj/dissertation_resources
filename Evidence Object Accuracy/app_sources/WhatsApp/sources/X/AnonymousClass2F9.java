package X;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Looper;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.2F9  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2F9 extends AbstractC44141yJ {
    public static ProgressDialogC48342Fq A0B;
    public static final AtomicReference A0C = new AtomicReference(null);
    public boolean A00;
    public final Activity A01;
    public final C48292Fk A02;
    public final AnonymousClass2KF A03;
    public final C27041Fu A04;
    public final C18850tA A05;
    public final AbstractC32851cq A06 = new AnonymousClass2KE(this);
    public final C26041Bu A07;
    public final AtomicBoolean A08 = new AtomicBoolean(false);
    public final boolean A09;
    public final boolean A0A;

    public AnonymousClass2F9(Activity activity, C14900mE r23, C15570nT r24, C48292Fk r25, C27041Fu r26, C18850tA r27, C17050qB r28, C14950mJ r29, C19490uC r30, C15880o3 r31, C20850wQ r32, C26041Bu r33, C27021Fs r34, C19890uq r35, C20740wF r36, C25651Af r37, C18350sJ r38, C15860o1 r39, AbstractC15850o0 r40, C15830ny r41, AbstractC14440lR r42, boolean z, boolean z2) {
        super(r23, r24, r28, r29, r30, r31, r32, r34, r35, r36, r37, r38, r39, r40, r41, r42);
        this.A01 = activity;
        this.A02 = r25;
        this.A0A = z;
        this.A03 = new AnonymousClass2KF(Looper.getMainLooper(), new WeakReference(activity));
        this.A05 = r27;
        this.A07 = r33;
        this.A09 = z2;
        this.A04 = r26;
    }

    public final Dialog A01(int i, int i2) {
        C004802e r2 = new C004802e(this.A01);
        r2.A06(i2);
        r2.A0B(false);
        r2.setPositiveButton(R.string.retry, new DialogInterface.OnClickListener(i) { // from class: X.2KG
            public final /* synthetic */ int A00;

            {
                this.A00 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i3) {
                AnonymousClass2F9 r22 = AnonymousClass2F9.this;
                C36021jC.A00(r22.A01, this.A00);
                r22.A00 = true;
                r22.A03(true, false);
            }
        });
        r2.setNegativeButton(R.string.msg_store_do_not_restore, new DialogInterface.OnClickListener(i) { // from class: X.2KH
            public final /* synthetic */ int A00;

            {
                this.A00 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i3) {
                AnonymousClass2F9 r1 = AnonymousClass2F9.this;
                int i4 = this.A00;
                Activity activity = r1.A01;
                C36021jC.A00(activity, i4);
                C36021jC.A01(activity, 106);
            }
        });
        return r2.create();
    }

    public void A02() {
        int A04 = super.A06.A04();
        StringBuilder sb = new StringBuilder("verifymsgstore/usehistoryifexists/backupfilesfound ");
        sb.append(A04);
        Log.i(sb.toString());
        if (A04 > 0) {
            C36021jC.A01(this.A01, 103);
        } else {
            A03(false, true);
        }
    }

    public void A03(boolean z, boolean z2) {
        String str;
        this.A00 = z;
        StringBuilder sb = new StringBuilder("verifymsgstore/preparemsgstore isregname=");
        boolean z3 = this.A0A;
        sb.append(z3);
        sb.append(" restorefrombackup=");
        sb.append(z);
        sb.append(" skipdialog=");
        if (z2) {
            str = "true";
        } else {
            str = "false";
        }
        sb.append(str);
        Log.i(sb.toString());
        if (!z2) {
            Activity activity = this.A01;
            if (!activity.isFinishing() && (!z3 || this.A00)) {
                C36021jC.A01(activity, 100);
            }
        }
        if (!this.A0C.A0F()) {
            super.A00.A0A(0);
        } else {
            A00();
        }
    }
}
