package X;

import android.os.CountDownTimer;
import android.view.View;

/* renamed from: X.2Zx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class CountDownTimerC51982Zx extends CountDownTimer {
    public final /* synthetic */ View A00;
    public final /* synthetic */ C14260l7 A01;
    public final /* synthetic */ C57802ne A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public CountDownTimerC51982Zx(View view, C14260l7 r4, C57802ne r5, long j) {
        super(j, 1000);
        this.A02 = r5;
        this.A00 = view;
        this.A01 = r4;
    }

    @Override // android.os.CountDownTimer
    public void onFinish() {
        C14260l7 r5 = this.A01;
        C14270l8 A03 = AnonymousClass3JV.A03(r5);
        AnonymousClass28D r3 = this.A02.A00;
        A03.A07(new C82763w9(this), (long) r3.A00);
        AbstractC14200l1 A0G = r3.A0G(36);
        if (A0G != null) {
            C28701Oq.A01(r5, r3, C14210l2.A02(r3), A0G);
        }
    }

    @Override // android.os.CountDownTimer
    public void onTick(long j) {
        this.A02.A01.Ad9(this.A00, C12980iv.A0D(j));
    }
}
