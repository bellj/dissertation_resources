package X;

import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/* renamed from: X.1z0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC44461z0 {
    public final int A00 = DefaultCrypto.BUFFER_SIZE;
    public final int A01;
    public final long A02;
    public final String A03;

    public AbstractC44461z0(File file, int i, long j) {
        this.A02 = j;
        this.A01 = i;
        this.A03 = file.getCanonicalPath();
    }

    public long A00(File file, ZipInputStream zipInputStream, byte[] bArr) {
        if (this instanceof AnonymousClass31X) {
            File parentFile = file.getParentFile();
            if (parentFile != null && !parentFile.exists()) {
                parentFile.mkdirs();
            }
            long j = 0;
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                while (true) {
                    int read = zipInputStream.read(bArr);
                    if (read == -1) {
                        break;
                    }
                    fileOutputStream.write(bArr, 0, read);
                    j += (long) read;
                    if (8192 + j > 10485760) {
                        break;
                    }
                }
                fileOutputStream.close();
                return j;
            } catch (FileNotFoundException e) {
                Log.e("CommerceBloksAssetZipEntrySaver/saveFile", e);
                return j;
            }
        } else if (!(this instanceof C44451yz)) {
            AnonymousClass22V r7 = (AnonymousClass22V) this;
            File file2 = new File(r7.A00, file.getName());
            long j2 = 0;
            try {
                FileOutputStream fileOutputStream2 = new FileOutputStream(file2);
                while (true) {
                    int read2 = zipInputStream.read(bArr);
                    if (read2 == -1) {
                        break;
                    }
                    fileOutputStream2.write(bArr, 0, read2);
                    j2 += (long) read2;
                    if (8192 + j2 > 524288000) {
                        break;
                    }
                }
                r7.A01.add(C14350lI.A08(file2.getName()));
                fileOutputStream2.close();
                return j2;
            } catch (FileNotFoundException e2) {
                Log.e("PAY: PaymentBackgroundBatchZipEntrySaver", e2);
                return j2;
            }
        } else {
            File parentFile2 = file.getParentFile();
            if (parentFile2 != null && !parentFile2.exists()) {
                parentFile2.mkdirs();
            }
            long j3 = 0;
            try {
                FileOutputStream fileOutputStream3 = new FileOutputStream(file);
                while (true) {
                    int read3 = zipInputStream.read(bArr);
                    if (read3 == -1) {
                        break;
                    }
                    fileOutputStream3.write(bArr, 0, read3);
                    j3 += (long) read3;
                    if (8192 + j3 > 5242880) {
                        break;
                    }
                }
                fileOutputStream3.close();
                return j3;
            } catch (FileNotFoundException e3) {
                Log.e("DownloadableWallpaperZipEntrySaver", e3);
                return j3;
            }
        }
    }

    public boolean A01(File file) {
        String str;
        if (this instanceof AnonymousClass31X) {
            return true;
        }
        boolean z = this instanceof C44451yz;
        String canonicalPath = file.getCanonicalPath();
        StringBuilder sb = new StringBuilder();
        String str2 = this.A03;
        if (!z) {
            sb.append(str2);
            sb.append("/backgrounds");
            if (!canonicalPath.startsWith(sb.toString())) {
                str = "PAY: PaymentBackgroundBatchZipEntrySaver/invalid zip content";
            } else if ("webp".equals(C14350lI.A07(file.getAbsolutePath()))) {
                return true;
            } else {
                str = "PAY: PaymentBackgroundBatchZipEntrySaver/store: Zip entry not webp";
            }
            Log.e(str);
            return false;
        }
        sb.append(str2);
        sb.append('/');
        sb.append("thumbnails");
        if (!canonicalPath.startsWith(sb.toString())) {
            return false;
        }
        if ("jpg".equals(C14350lI.A07(file.getAbsolutePath())) || file.isDirectory()) {
            return true;
        }
        return false;
    }

    public boolean A02(ZipInputStream zipInputStream) {
        String canonicalPath;
        String name;
        String str;
        if (!(this instanceof AnonymousClass31X)) {
            int i = this.A00;
            byte[] bArr = new byte[i];
            long j = 0;
            int i2 = 0;
            while (true) {
                ZipEntry nextEntry = zipInputStream.getNextEntry();
                if (nextEntry == null) {
                    return true;
                }
                File A05 = C14350lI.A05(this.A03, nextEntry.getName());
                if (A05 == null || !A01(A05)) {
                    nextEntry.getName();
                } else {
                    j += A00(A05, zipInputStream, bArr);
                    if (((long) i) + j > this.A02) {
                        str = "SafeZipEntrySaver: File being unzipped is too big.";
                        break;
                    }
                    i2++;
                    if (i2 > this.A01) {
                        str = "SafeZipEntrySaver: Too many files to unzip.";
                        break;
                    }
                }
            }
            Log.e(str);
            return false;
        }
        AnonymousClass31X r7 = (AnonymousClass31X) this;
        byte[] bArr2 = new byte[DefaultCrypto.BUFFER_SIZE];
        long j2 = 0;
        while (true) {
            ZipEntry nextEntry2 = zipInputStream.getNextEntry();
            if (nextEntry2 == null) {
                return true;
            }
            if (C16700pc.A0O(C14350lI.A07(nextEntry2.getName()), "json")) {
                canonicalPath = r7.A03;
                name = new File(nextEntry2.getName()).getName();
            } else {
                canonicalPath = r7.A00.getCanonicalPath();
                name = nextEntry2.getName();
            }
            File A052 = C14350lI.A05(canonicalPath, name);
            if (A052 != null) {
                j2 += r7.A00(A052, zipInputStream, bArr2);
                if (((long) DefaultCrypto.BUFFER_SIZE) + j2 > 15728640) {
                    Log.e("CommerceBloksAssetZipEntrySaver/saveInputStream: File being unzipped is too big.");
                    return false;
                }
            } else {
                nextEntry2.getName();
            }
        }
    }
}
