package X;

import android.content.SharedPreferences;

/* renamed from: X.3Ga  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C64563Ga {
    public final long A00;
    public final /* synthetic */ C15070mW A01;

    public /* synthetic */ C64563Ga(C15070mW r4, long j) {
        this.A01 = r4;
        C13020j0.A05("monitoring");
        if (j > 0) {
            this.A00 = j;
            return;
        }
        throw new IllegalArgumentException();
    }

    public static final void A00(C64563Ga r4) {
        C15070mW r0 = r4.A01;
        long currentTimeMillis = System.currentTimeMillis();
        SharedPreferences.Editor edit = r0.A01.edit();
        edit.remove("monitoring".concat(":count"));
        edit.remove("monitoring".concat(":value"));
        edit.putLong("monitoring".concat(":start"), currentTimeMillis);
        edit.commit();
    }
}
