package X;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;

/* renamed from: X.1Kx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class HandlerC28161Kx extends Handler {
    public HandlerThread A00;
    public C28151Kw A01;
    public C14690ls A02;

    public HandlerC28161Kx(HandlerThread handlerThread, C28151Kw r3, C14690ls r4) {
        super(handlerThread.getLooper());
        this.A00 = handlerThread;
        this.A01 = r3;
        this.A02 = r4;
    }

    public synchronized void A00() {
        this.A00.quit();
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        C28151Kw r3 = this.A01;
        r3.A00.set(Double.doubleToRawLongBits((double) this.A02.A00()));
        sendEmptyMessageDelayed(0, (long) 50);
    }
}
