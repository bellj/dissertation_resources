package X;

import com.facebook.redex.IDxNFunctionShape17S0100000_2_I1;
import java.util.Arrays;

/* renamed from: X.4Wz  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4Wz {
    public final AnonymousClass1V8 A00;
    public final AbstractC115485Rs A01;

    public AnonymousClass4Wz(AbstractC15710nm r5, AnonymousClass1V8 r6) {
        AnonymousClass1V8.A01(r6, "payout");
        this.A01 = (AbstractC115485Rs) AnonymousClass3JT.A06(r6, "PayoutBank|PayoutPrepaidCard", C12980iv.A0x(Arrays.asList(new IDxNFunctionShape17S0100000_2_I1(r5, 30), new IDxNFunctionShape17S0100000_2_I1(r5, 31))), new String[0]);
        this.A00 = r6;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || AnonymousClass4Wz.class != obj.getClass()) {
            return false;
        }
        return this.A01.equals(((AnonymousClass4Wz) obj).A01);
    }

    public int hashCode() {
        return C12980iv.A0B(this.A01, new Object[1], 0);
    }
}
