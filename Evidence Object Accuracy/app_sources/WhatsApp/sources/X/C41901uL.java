package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.1uL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C41901uL extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C41901uL A04;
    public static volatile AnonymousClass255 A05;
    public int A00;
    public int A01;
    public int A02;
    public long A03;

    static {
        C41901uL r0 = new C41901uL();
        A04 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r16, Object obj, Object obj2) {
        switch (r16.ordinal()) {
            case 0:
                return A04;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                C41901uL r7 = (C41901uL) obj2;
                int i = this.A00;
                boolean z = true;
                if ((i & 1) != 1) {
                    z = false;
                }
                int i2 = this.A02;
                int i3 = r7.A00;
                boolean z2 = true;
                if ((i3 & 1) != 1) {
                    z2 = false;
                }
                this.A02 = r8.Afp(i2, r7.A02, z, z2);
                boolean z3 = false;
                if ((i & 2) == 2) {
                    z3 = true;
                }
                long j = this.A03;
                boolean z4 = false;
                if ((i3 & 2) == 2) {
                    z4 = true;
                }
                this.A03 = r8.Afs(j, r7.A03, z3, z4);
                boolean z5 = false;
                if ((i & 4) == 4) {
                    z5 = true;
                }
                int i4 = this.A01;
                boolean z6 = false;
                if ((i3 & 4) == 4) {
                    z6 = true;
                }
                this.A01 = r8.Afp(i4, r7.A01, z5, z6);
                if (r8 == C463025i.A00) {
                    this.A00 = i | i3;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        try {
                            int A03 = r82.A03();
                            if (A03 == 0) {
                                break;
                            } else if (A03 == 8) {
                                this.A00 |= 1;
                                this.A02 = r82.A02();
                            } else if (A03 == 16) {
                                this.A00 |= 2;
                                this.A03 = r82.A06();
                            } else if (A03 == 24) {
                                this.A00 |= 4;
                                this.A01 = r82.A02();
                            } else if (!A0a(r82, A03)) {
                                break;
                            }
                        } catch (C28971Pt e) {
                            e.unfinishedMessage = this;
                            throw new RuntimeException(e);
                        }
                    } catch (IOException e2) {
                        C28971Pt r1 = new C28971Pt(e2.getMessage());
                        r1.unfinishedMessage = this;
                        throw new RuntimeException(r1);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C41901uL();
            case 5:
                return new C81433u0();
            case 6:
                break;
            case 7:
                if (A05 == null) {
                    synchronized (C41901uL.class) {
                        if (A05 == null) {
                            A05 = new AnonymousClass255(A04);
                        }
                    }
                }
                return A05;
            default:
                throw new UnsupportedOperationException();
        }
        return A04;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A04(1, this.A02);
        }
        if ((i3 & 2) == 2) {
            i2 += CodedOutputStream.A06(2, this.A03);
        }
        if ((i3 & 4) == 4) {
            i2 += CodedOutputStream.A04(3, this.A01);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0F(1, this.A02);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0H(2, this.A03);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0F(3, this.A01);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
