package X;

/* renamed from: X.43I  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass43I extends AbstractC16110oT {
    public Integer A00;
    public String A01;

    public AnonymousClass43I() {
        super(3198, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
        r3.Abe(2, this.A01);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamDeepLinkMsgSent {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "deepLinkAction", C12960it.A0Y(this.A00));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "deepLinkSessionId", this.A01);
        return C12960it.A0d("}", A0k);
    }
}
