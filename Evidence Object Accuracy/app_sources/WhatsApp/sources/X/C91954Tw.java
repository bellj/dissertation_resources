package X;

import android.util.SparseIntArray;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.msys.mci.DefaultCrypto;

/* renamed from: X.4Tw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C91954Tw {
    public final int A00;
    public final AbstractC11580gW A01;
    public final C93604aR A02 = C93064Yw.A00();
    public final C93604aR A03;
    public final C93604aR A04;
    public final C93604aR A05;
    public final AbstractC115035Pl A06 = C106114v9.A00();
    public final AbstractC115035Pl A07;
    public final AbstractC115035Pl A08;
    public final String A09;

    public /* synthetic */ C91954Tw() {
        C08650bZ r0;
        int i;
        int i2;
        AnonymousClass4Yy.A00();
        int i3 = AnonymousClass4FT.A00;
        int i4 = i3 * 4194304;
        int i5 = C25981Bo.A0F;
        SparseIntArray sparseIntArray = new SparseIntArray();
        do {
            sparseIntArray.put(i5, i3);
            i5 <<= 1;
        } while (i5 <= 4194304);
        this.A03 = new C93604aR(sparseIntArray, 4194304, i4, i3);
        synchronized (C08650bZ.class) {
            r0 = C08650bZ.A00;
            if (r0 == null) {
                r0 = new C08650bZ();
                C08650bZ.A00 = r0;
            }
        }
        this.A01 = r0;
        SparseIntArray sparseIntArray2 = new SparseIntArray();
        sparseIntArray2.put(EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH, 5);
        sparseIntArray2.put(EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH, 5);
        sparseIntArray2.put(4096, 5);
        sparseIntArray2.put(DefaultCrypto.BUFFER_SIZE, 5);
        sparseIntArray2.put(16384, 5);
        sparseIntArray2.put(32768, 5);
        sparseIntArray2.put(65536, 5);
        sparseIntArray2.put(C25981Bo.A0F, 5);
        sparseIntArray2.put(262144, 2);
        sparseIntArray2.put(524288, 2);
        sparseIntArray2.put(1048576, 2);
        int min = (int) Math.min(Runtime.getRuntime().maxMemory(), 2147483647L);
        if (min < 16777216) {
            i = 3145728;
        } else {
            i = 12582912;
            if (min < 33554432) {
                i = 6291456;
            }
        }
        int min2 = (int) Math.min(Runtime.getRuntime().maxMemory(), 2147483647L);
        if (min2 < 16777216) {
            i2 = min2 >> 1;
        } else {
            i2 = (min2 >> 2) * 3;
        }
        this.A04 = new C93604aR(sparseIntArray2, i, i2, -1);
        this.A07 = C106114v9.A00();
        SparseIntArray sparseIntArray3 = new SparseIntArray();
        sparseIntArray3.put(16384, 5);
        this.A05 = new C93604aR(sparseIntArray3, 81920, 1048576, -1);
        this.A08 = C106114v9.A00();
        this.A09 = "legacy";
        this.A00 = 4194304;
        AnonymousClass4Yy.A00();
    }
}
