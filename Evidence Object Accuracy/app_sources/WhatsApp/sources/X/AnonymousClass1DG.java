package X;

import android.content.Context;
import java.io.File;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1DG  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1DG implements AbstractC16990q5 {
    public final C14830m7 A00;
    public final C16590pI A01;
    public final C14820m6 A02;
    public final C17220qS A03;
    public final C15510nN A04;
    public final AbstractC14440lR A05;

    @Override // X.AbstractC16990q5
    public /* synthetic */ void AOp() {
    }

    public AnonymousClass1DG(C14830m7 r1, C16590pI r2, C14820m6 r3, C17220qS r4, C15510nN r5, AbstractC14440lR r6) {
        this.A04 = r5;
        this.A01 = r2;
        this.A02 = r3;
        this.A05 = r6;
        this.A03 = r4;
        this.A00 = r1;
    }

    @Override // X.AbstractC16990q5
    public void AOo() {
        if (this.A04.A01()) {
            C16590pI r7 = this.A01;
            Context context = r7.A00;
            C14830m7 r6 = this.A00;
            C14820m6 r8 = this.A02;
            if (!new File(context.getFilesDir(), "backup_token").exists() || r6.A00() - r8.A00.getLong("backup_token_file_timestamp", -1) > TimeUnit.DAYS.toMillis(14)) {
                new C452420s(r6, r7, r8, this.A03, this.A05).A00();
            }
        }
    }
}
