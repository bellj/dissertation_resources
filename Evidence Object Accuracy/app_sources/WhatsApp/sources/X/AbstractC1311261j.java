package X;

import com.whatsapp.payments.ui.widget.PaymentMethodRow;

/* renamed from: X.61j  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public interface AbstractC1311261j extends AbstractC136326Mc {
    int AEM(AbstractC28901Pl v);

    String AEN(AbstractC28901Pl v);

    boolean AdN(AbstractC28901Pl v);

    boolean AdT();

    boolean AdV();

    void Adj(AbstractC28901Pl v, PaymentMethodRow paymentMethodRow);
}
