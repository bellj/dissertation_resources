package X;

/* renamed from: X.30l  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C614630l extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Integer A02;
    public String A03;
    public String A04;

    public C614630l() {
        super(3222, AbstractC16110oT.A00(), 2, 0);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
        r3.Abe(2, this.A03);
        r3.Abe(3, this.A01);
        r3.Abe(4, this.A04);
        r3.Abe(5, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamStructuredMessageReceive {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "bizPlatform", C12960it.A0Y(this.A00));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "businessOwnerJid", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "messageClass", C12960it.A0Y(this.A01));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "messageClassAttributes", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "messageMediaType", C12960it.A0Y(this.A02));
        return C12960it.A0d("}", A0k);
    }
}
