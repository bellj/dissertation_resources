package X;

import android.text.TextUtils;
import android.util.SparseArray;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/* renamed from: X.1EN  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1EN extends AnonymousClass1BJ {
    public final SparseArray A00 = new SparseArray();
    public final AnonymousClass1EM A01;
    public final Set A02 = new HashSet();

    public AnonymousClass1EN(AbstractC15710nm r14, C18790t3 r15, C18640sm r16, C14830m7 r17, C16590pI r18, C17170qN r19, C14820m6 r20, AnonymousClass1BH r21, C18810t5 r22, C18800t4 r23, AnonymousClass1EM r24, AbstractC14440lR r25) {
        super(r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r25);
        this.A01 = r24;
    }

    @Override // X.AnonymousClass1BJ
    public Map A04(String str, String str2, String str3, String str4, int i) {
        Map A04 = super.A04(str, str2, str3, null, i);
        A04.put("id", Integer.toString(i));
        return A04;
    }

    public final File A0E(String str, int i, boolean z) {
        String str2;
        File filesDir = this.A08.A00.getFilesDir();
        StringBuilder sb = new StringBuilder("downloadable/doodle_emoji_");
        sb.append(i);
        sb.append("_");
        sb.append(str);
        if (z) {
            str2 = "_temp";
        } else {
            str2 = "";
        }
        sb.append(str2);
        return new File(filesDir, sb.toString());
    }

    public synchronized void A0F(int i) {
        C39451pv A02;
        if (!this.A02.contains(Integer.valueOf(i)) && (A02 = A02()) != null) {
            A0G(i, A02.A04(i));
        }
    }

    public final synchronized void A0G(int i, String str) {
        if (!TextUtils.isEmpty(str)) {
            File A0E = A0E(str, i, false);
            if (!A0E.exists()) {
                StringBuilder sb = new StringBuilder();
                sb.append("DoodleEmojiManager/loadFilePaths subdirectory for bundle=");
                sb.append(i);
                sb.append(" hash=");
                sb.append(str);
                sb.append(" doesn't exist");
                Log.e(sb.toString());
            } else {
                String[] list = A0E.list();
                if (list == null) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("DoodleEmojiManager/loadFilePaths no files found in ");
                    sb2.append(A0E.toString());
                    Log.e(sb2.toString());
                } else {
                    String absolutePath = A0E.getAbsolutePath();
                    for (String str2 : list) {
                        int parseInt = Integer.parseInt(str2.split("\\.")[0].split("e")[1]);
                        SparseArray sparseArray = this.A00;
                        if (C14350lI.A07(str2).equals("obi")) {
                            AnonymousClass1EM r1 = this.A01;
                            if (C37871n9.A03(AnonymousClass1EM.A04, parseInt)) {
                                if (!r1.A00.A07(2025)) {
                                }
                                sparseArray.put(parseInt, new File(absolutePath, str2));
                            }
                        } else if (sparseArray.indexOfKey(parseInt) < 0) {
                            sparseArray.put(parseInt, new File(absolutePath, str2));
                        }
                    }
                    this.A02.add(Integer.valueOf(i));
                }
            }
        }
    }
}
