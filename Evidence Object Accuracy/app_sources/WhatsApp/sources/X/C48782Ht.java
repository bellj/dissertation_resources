package X;

import android.graphics.Point;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.facebook.redex.RunnableBRunnable0Shape0S0000000_I0;
import com.facebook.redex.RunnableBRunnable0Shape1S0200000_I0_1;
import com.whatsapp.calling.callgrid.viewmodel.CallGridViewModel;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.CallInfo;
import com.whatsapp.voipcalling.CallLinkInfo;
import com.whatsapp.voipcalling.VideoPort;
import com.whatsapp.voipcalling.Voip;
import com.whatsapp.voipcalling.camera.VoipCameraManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.2Ht  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C48782Ht extends AbstractC16220oe {
    public int A00;
    public long A01 = 0;
    public Handler A02;
    public C29631Ua A03;
    public CallInfo A04;
    public VideoPort A05;
    public String A06;
    public final long A07;
    public final C14830m7 A08;
    public final C15890o4 A09;
    public final AnonymousClass2NO A0A = new AnonymousClass485(this);
    public final VoipCameraManager A0B;
    public final List A0C = new ArrayList();
    public final Map A0D = new HashMap();
    public final Set A0E = new HashSet();

    public C48782Ht(C14830m7 r4, C15890o4 r5, C14850m9 r6, VoipCameraManager voipCameraManager) {
        this.A08 = r4;
        this.A0B = voipCameraManager;
        this.A09 = r5;
        this.A02 = new Handler(Looper.getMainLooper(), new Handler.Callback() { // from class: X.3La
            @Override // android.os.Handler.Callback
            public final boolean handleMessage(Message message) {
                VideoPort videoPort;
                C48782Ht r42 = C48782Ht.this;
                int i = message.what;
                if (i == 1) {
                    CallInfo A06 = r42.A06(null);
                    if (A06 != null && A06.callState == Voip.CallState.ACTIVE && !A06.callEnding) {
                        AnonymousClass1S6 r1 = A06.self;
                        AnonymousClass1S6 defaultPeerInfo = A06.getDefaultPeerInfo();
                        if ((defaultPeerInfo == null || defaultPeerInfo.A07) && r1.A04 == 1) {
                            C29631Ua r0 = r42.A03;
                            if (r0 != null) {
                                r0.A0x.execute(new RunnableBRunnable0Shape0S0000000_I0(6));
                            }
                            for (C89404Jv r02 : r42.A0C) {
                                r02.A00.A0L.A0B(Boolean.TRUE);
                            }
                        }
                    }
                } else if (i == 2) {
                    r42.A02.removeMessages(2);
                    if (!(r42.A06(null) == null || (videoPort = r42.A05) == null)) {
                        r42.A0B.removeCameraErrorListener(r42.A0A);
                        r42.A0A(null);
                        r42.A05 = null;
                        r42.A0A(videoPort);
                        return true;
                    }
                } else if (i == 3 || i == 4) {
                    Handler handler = r42.A02;
                    handler.removeMessages(3);
                    handler.removeMessages(4);
                    CallInfo A062 = r42.A06(null);
                    if (A062 != null) {
                        r42.A09(A062);
                        return true;
                    }
                }
                return true;
            }
        });
        this.A07 = (long) Math.min(1000, r6.A02(1960));
    }

    public AnonymousClass3HK A05() {
        CallInfo A06 = A06(null);
        if (A06 != null) {
            return new AnonymousClass3HK(A06);
        }
        Map emptyMap = Collections.emptyMap();
        return new AnonymousClass3HK(AbstractC17190qP.copyOf(emptyMap), Voip.CallState.NONE);
    }

    public final CallInfo A06(CallInfo callInfo) {
        String str;
        if (callInfo == null) {
            Voip.CallState currentCallState = Voip.getCurrentCallState();
            int currentCallLinkState = Voip.getCurrentCallLinkState();
            if (currentCallState != Voip.CallState.LINK || currentCallLinkState == 4) {
                callInfo = Voip.getCallInfo();
                if (callInfo == null) {
                    return null;
                }
            } else {
                CallLinkInfo callLinkInfo = Voip.getCallLinkInfo();
                AnonymousClass009.A05(callLinkInfo);
                return CallInfo.convertCallLinkInfoToCallInfo(callLinkInfo);
            }
        }
        if (callInfo.isCallLinkLobbyState || (str = this.A06) == null || !str.equals(callInfo.callWaitingInfo.A04)) {
            return callInfo;
        }
        return CallInfo.convertCallWaitingInfoToCallInfo(callInfo);
    }

    /* renamed from: A07 */
    public void A04(AnonymousClass2OR r3) {
        super.A04(r3);
        if (!A01().iterator().hasNext()) {
            this.A02.removeCallbacksAndMessages(null);
        }
    }

    public void A08(CallInfo callInfo) {
        Handler handler = this.A02;
        handler.removeMessages(4);
        long currentTimeMillis = System.currentTimeMillis();
        long j = this.A01;
        if (j != 0) {
            long j2 = j + this.A07;
            if (currentTimeMillis < j2) {
                handler.sendEmptyMessageDelayed(4, j2 - currentTimeMillis);
                return;
            }
        }
        A09(callInfo);
    }

    public final void A09(CallInfo callInfo) {
        CallInfo A06 = A06(callInfo);
        this.A04 = A06;
        if (A06 != null) {
            AnonymousClass3HK r2 = new AnonymousClass3HK(A06);
            for (AnonymousClass2OR r0 : A01()) {
                r0.A04(r2);
            }
            long j = A06.callDuration;
            for (AnonymousClass2OR r1 : A01()) {
                if (r1 instanceof CallGridViewModel) {
                    ((CallGridViewModel) r1).A05.A0B(Long.valueOf(j));
                }
            }
            this.A01 = System.currentTimeMillis();
        }
    }

    public void A0A(VideoPort videoPort) {
        if (this.A06 == null) {
            return;
        }
        if (this.A09.A02("android.permission.CAMERA") != 0) {
            Log.w("voip/CallDatasource/ camera permissions not granted, unable to set video preview port");
            return;
        }
        int videoPreviewPort = Voip.setVideoPreviewPort(videoPort, this.A06);
        if (videoPort == null) {
            Voip.setVideoPreviewSize(0, 0);
        } else if (videoPreviewPort == 0) {
            this.A00 = 0;
            Point windowSize = videoPort.getWindowSize();
            Voip.setVideoPreviewSize(windowSize.x, windowSize.y);
            this.A0B.addCameraErrorListener(this.A0A);
        } else {
            int i = this.A00;
            this.A00 = i + 1;
            if (i >= 10) {
                C29631Ua r2 = this.A03;
                if (r2 != null) {
                    r2.A0m(null, null, 15);
                    return;
                }
                return;
            }
            this.A02.postDelayed(new RunnableBRunnable0Shape1S0200000_I0_1(this, 28, videoPort), 500);
        }
    }

    public void A0B(String str) {
        String str2 = this.A06;
        if (!str.equals(str2)) {
            if (str2 != null) {
                for (AnonymousClass2OR r2 : A01()) {
                    if (r2 instanceof CallGridViewModel) {
                        CallGridViewModel callGridViewModel = (CallGridViewModel) r2;
                        callGridViewModel.A02 = null;
                        callGridViewModel.A07.A0B(null);
                    }
                }
            }
            this.A06 = str;
            CallInfo A06 = A06(null);
            if (A06 != null) {
                A08(A06);
            }
        }
    }
}
