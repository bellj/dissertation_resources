package X;

import java.io.File;

/* renamed from: X.4TY  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4TY {
    public final C43161wW A00;
    public final C14370lK A01;
    public final AbstractC37701mr A02;
    public final File A03;
    public final File A04;
    public final String A05;
    public final String A06;

    public AnonymousClass4TY(C14370lK r2, AbstractC37701mr r3, File file, File file2, String str, String str2) {
        this.A02 = r3;
        this.A04 = file;
        this.A03 = file2;
        this.A05 = str;
        this.A06 = str2;
        this.A01 = r2;
        this.A00 = new C43161wW(r2);
    }
}
