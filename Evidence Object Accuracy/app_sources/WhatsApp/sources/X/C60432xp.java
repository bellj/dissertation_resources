package X;

import android.content.Context;
import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.conversation.conversationrow.DynamicButtonsLayout;
import com.whatsapp.conversation.conversationrow.DynamicButtonsRowContentLayout;
import com.whatsapp.conversation.conversationrow.NativeFlowButtonsRowContentLayout;

/* renamed from: X.2xp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60432xp extends AnonymousClass1OY {
    public boolean A00;
    public final TextEmojiLabel A01 = C12970iu.A0U(this, R.id.title_text_message);
    public final AbstractC13890kV A02;
    public final DynamicButtonsLayout A03 = ((DynamicButtonsLayout) findViewById(R.id.dynamic_reply_buttons));
    public final DynamicButtonsRowContentLayout A04 = ((DynamicButtonsRowContentLayout) findViewById(R.id.dynamic_reply_buttons_message_content));
    public final NativeFlowButtonsRowContentLayout A05 = ((NativeFlowButtonsRowContentLayout) findViewById(R.id.native_flow_action_button_content));

    public C60432xp(Context context, AbstractC13890kV r4, C28861Ph r5) {
        super(context, r4, r5);
        A0Z();
        this.A02 = r4;
        TextEmojiLabel textEmojiLabel = this.A01;
        AbstractC28491Nn.A03(textEmojiLabel);
        textEmojiLabel.setAutoLinkMask(0);
        textEmojiLabel.setLinksClickable(false);
        textEmojiLabel.setClickable(false);
        textEmojiLabel.setLongClickable(false);
        A1M();
    }

    @Override // X.AnonymousClass1OZ, X.AbstractC28561Ob
    public void A0Z() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2P6 A07 = AnonymousClass1OY.A07(this);
            AnonymousClass01J A08 = AnonymousClass1OY.A08(A07, this);
            AnonymousClass1OY.A0L(A08, this);
            AnonymousClass1OY.A0M(A08, this);
            AnonymousClass1OY.A0K(A08, this);
            AnonymousClass1OY.A0I(A07, A08, this, AnonymousClass1OY.A09(A08, this, AnonymousClass1OY.A0B(A08, this)));
        }
    }

    @Override // X.AnonymousClass1OY
    public void A0s() {
        A1M();
        A1H(false);
    }

    @Override // X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r2, boolean z) {
        boolean A1X = C12960it.A1X(r2, getFMessage());
        super.A1D(r2, z);
        if (z || A1X) {
            A1M();
        }
    }

    public final void A1M() {
        this.A04.A00(this);
        AbstractC15340mz fMessage = getFMessage();
        if (!TextUtils.isEmpty(fMessage.A0I())) {
            String A0I = fMessage.A0I();
            TextEmojiLabel textEmojiLabel = this.A01;
            A17(textEmojiLabel, getFMessage(), A0I, false, false);
            textEmojiLabel.setVisibility(0);
        } else {
            this.A01.setVisibility(8);
        }
        AnonymousClass3AZ.A00(this, this.A02, this.A03, this.A05, fMessage.A0F().A00);
    }

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_button_title_text_left;
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_button_title_text_left;
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_button_title_text_right;
    }

    @Override // X.AnonymousClass1OY, X.AbstractC28551Oa, android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        AnonymousClass1OY.A0G(this.A03, this);
    }

    @Override // X.AnonymousClass1OY, X.AbstractC28551Oa, android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        setMeasuredDimension(getMeasuredWidth(), AnonymousClass1OY.A04(this, this.A03, getMeasuredHeight()));
    }
}
