package X;

import com.whatsapp.util.Log;

/* renamed from: X.2QH  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2QH implements AnonymousClass2QI {
    public final C14370lK A00;

    public AnonymousClass2QH(C14370lK r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass2QI
    public C37891nB A8r(byte[] bArr) {
        try {
            Log.i("MmsImageCipherKeyProvider deriveKeys (HKDFv3.deriveSecrets start)");
            return AbstractC65063Hz.A00(C32891cu.A00(bArr, this.A00.A03, 80));
        } finally {
            Log.i("MmsImageCipherKeyProvider deriveKeys (HKDFv3.deriveSecrets end)");
        }
    }
}
