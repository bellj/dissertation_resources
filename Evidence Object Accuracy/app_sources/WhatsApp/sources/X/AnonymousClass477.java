package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.477  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass477 extends AbstractC75193jS {
    public final View A00;
    public final View A01;
    public final TextView A02;

    public AnonymousClass477(View view) {
        super(view);
        this.A00 = AnonymousClass028.A0D(view, R.id.wallpaper_header_chevron_view);
        TextView A0I = C12960it.A0I(view, R.id.wallpaper_header_title);
        this.A02 = A0I;
        this.A01 = AnonymousClass028.A0D(view, R.id.wallpaper_header_description);
        C27531Hw.A06(A0I);
    }
}
