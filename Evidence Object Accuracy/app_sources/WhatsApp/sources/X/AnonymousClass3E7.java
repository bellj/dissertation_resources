package X;

import java.util.Arrays;

/* renamed from: X.3E7  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3E7 {
    public final AnonymousClass1V8 A00;
    public final byte[] A01;

    public AnonymousClass3E7(AnonymousClass1V8 r10) {
        AnonymousClass1V8.A01(r10, "iq");
        this.A01 = (byte[]) AnonymousClass3JT.A04(null, r10, byte[].class, 1L, 1024L, null, new String[]{"cat", "#elementValue"}, false);
        this.A00 = r10;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || AnonymousClass3E7.class != obj.getClass()) {
            return false;
        }
        return Arrays.equals(this.A01, ((AnonymousClass3E7) obj).A01);
    }

    public int hashCode() {
        return (Arrays.hashCode(new Object[0]) * 31) + Arrays.hashCode(this.A01);
    }
}
