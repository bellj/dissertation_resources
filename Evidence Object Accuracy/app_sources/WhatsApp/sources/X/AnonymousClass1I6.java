package X;

import java.util.concurrent.LinkedTransferQueue;

/* renamed from: X.1I6  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1I6 extends LinkedTransferQueue<Runnable> {
    @Override // java.util.concurrent.LinkedTransferQueue, java.util.concurrent.BlockingQueue, java.util.Queue
    public /* bridge */ /* synthetic */ boolean offer(Object obj) {
        return tryTransfer(obj);
    }
}
