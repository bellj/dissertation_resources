package X;

/* renamed from: X.5sa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C126255sa {
    public final AnonymousClass1V8 A00;

    public C126255sa(AnonymousClass3CS r11, String str) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy A0N = C117295Zj.A0N(A0M);
        C41141sy.A01(A0N, "action", "br-get-autofill-address");
        if (AnonymousClass3JT.A0E(str, 1, 9, false)) {
            C41141sy.A01(A0N, "cep", str);
        }
        this.A00 = C117295Zj.A0I(A0N, A0M, r11);
    }
}
