package X;

import com.whatsapp.util.Log;

/* renamed from: X.248  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass248 extends AbstractC32541cK implements Runnable, AbstractC32491cF {
    public int A00 = 60;
    public C29901Ve A01;
    public final C21320xE A02;
    public final C14860mA A03;

    public AnonymousClass248(C21320xE r2, C29901Ve r3, C14860mA r4) {
        this.A03 = r4;
        this.A02 = r2;
        this.A01 = r3;
    }

    @Override // X.AbstractC32491cF
    public void Aaw(int i) {
        StringBuilder sb = new StringBuilder("BroadcastListResponseHandler/request failed : ");
        sb.append(i);
        sb.append(" | ");
        C29901Ve r2 = this.A01;
        sb.append(r2);
        sb.append(" | ");
        sb.append(this.A00);
        Log.e(sb.toString());
        super.A01.cancel();
        this.A02.A09(r2, false);
    }

    @Override // java.lang.Runnable
    public void run() {
        super.A01.cancel();
        StringBuilder sb = new StringBuilder("BroadcastListResponseHandler/request success/");
        sb.append(this.A00);
        Log.i(sb.toString());
    }
}
