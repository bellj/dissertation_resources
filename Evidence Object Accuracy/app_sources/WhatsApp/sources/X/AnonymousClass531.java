package X;

import android.net.Uri;
import com.whatsapp.R;

/* renamed from: X.531  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass531 implements AnonymousClass24S {
    public final /* synthetic */ AnonymousClass3UW A00;

    @Override // X.AnonymousClass24S
    public void AY7(Uri uri) {
    }

    @Override // X.AnonymousClass24S
    public void AY8(Uri uri) {
    }

    public AnonymousClass531(AnonymousClass3UW r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass24S
    public void AQ8() {
        this.A00.A02.A07(R.string.share_failed, 0);
    }
}
