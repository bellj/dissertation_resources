package X;

import android.content.Context;
import android.content.res.Resources;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.CircularProgressBar;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.components.TextAndDateLayout;
import com.whatsapp.conversation.conversationrow.ConversationRowImage$RowImageView;
import java.util.Collections;

/* renamed from: X.2yL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60632yL extends AbstractC42671vd {
    public AnonymousClass19N A00;
    public boolean A01;
    public boolean A02;
    public final View A03;
    public final ViewGroup A04;
    public final ViewGroup A05;
    public final FrameLayout A06;
    public final ImageView A07;
    public final LinearLayout A08;
    public final TextView A09 = C12960it.A0I(this, R.id.control_btn);
    public final TextView A0A;
    public final TextView A0B;
    public final CircularProgressBar A0C;
    public final TextEmojiLabel A0D;
    public final TextEmojiLabel A0E;
    public final TextEmojiLabel A0F;
    public final TextEmojiLabel A0G;
    public final TextAndDateLayout A0H;
    public final ConversationRowImage$RowImageView A0I = ((ConversationRowImage$RowImageView) AnonymousClass028.A0D(this, R.id.image));
    public final AbstractC41521tf A0J = new C70243ay(this);

    public C60632yL(Context context, AbstractC13890kV r10, AnonymousClass1XV r11) {
        super(context, r10, r11);
        A0Z();
        CircularProgressBar circularProgressBar = (CircularProgressBar) AnonymousClass028.A0D(this, R.id.progress_bar);
        this.A0C = circularProgressBar;
        circularProgressBar.A0B = 0;
        this.A07 = C12970iu.A0K(this, R.id.cancel_download);
        this.A03 = AnonymousClass028.A0D(this, R.id.control_frame);
        TextEmojiLabel A0T = C12970iu.A0T(this, R.id.caption);
        this.A0D = A0T;
        this.A0H = (TextAndDateLayout) AnonymousClass028.A0D(this, R.id.text_and_date);
        TextEmojiLabel A0T2 = C12970iu.A0T(this, R.id.view_product_btn);
        TextEmojiLabel A0T3 = C12970iu.A0T(this, R.id.product_title);
        this.A0G = A0T3;
        this.A0E = C12970iu.A0T(this, R.id.product_body);
        this.A0F = C12970iu.A0T(this, R.id.product_footer);
        FrameLayout frameLayout = (FrameLayout) AnonymousClass028.A0D(this, R.id.product_content_date_layout);
        this.A06 = frameLayout;
        this.A05 = (ViewGroup) AnonymousClass028.A0D(this, R.id.date_wrapper);
        this.A0B = C12960it.A0I(this, R.id.date);
        this.A04 = (ViewGroup) AnonymousClass028.A0D(frameLayout, R.id.date_wrapper);
        this.A0A = C12960it.A0I(frameLayout, R.id.date);
        LinearLayout linearLayout = (LinearLayout) AnonymousClass028.A0D(this, R.id.product_message_view);
        this.A08 = linearLayout;
        AbstractC28491Nn.A03(A0T);
        A0T.setAutoLinkMask(0);
        A0T.setLinksClickable(false);
        A0T.setFocusable(false);
        A0T.setLongClickable(false);
        A0T2.A0G(null, getContext().getString(R.string.view_product));
        A0T3.setAutoLinkMask(0);
        A0T3.setLinksClickable(false);
        A0T3.setFocusable(false);
        A0T3.setLongClickable(false);
        linearLayout.setOnLongClickListener(this.A1a);
        C12960it.A0y(linearLayout, this, 36);
        A0X(true);
    }

    private void A0X(boolean z) {
        AbstractView$OnClickListenerC34281fs A0A;
        int A00;
        AnonymousClass1XV r2 = (AnonymousClass1XV) ((AbstractC16130oV) ((AbstractC28551Oa) this).A0O);
        C16150oX A002 = AbstractC15340mz.A00(r2);
        if (z) {
            this.A09.setTag(Collections.singletonList(r2));
        }
        ConversationRowImage$RowImageView conversationRowImage$RowImageView = this.A0I;
        conversationRowImage$RowImageView.setImageBitmap(null);
        conversationRowImage$RowImageView.setImageData(new C16150oX(A002));
        conversationRowImage$RowImageView.setFullWidth(false);
        conversationRowImage$RowImageView.setPaddingOnTopOnly(true);
        AbstractC16130oV fMessage = getFMessage();
        if (C30041Vv.A12(fMessage)) {
            View view = this.A03;
            view.setVisibility(0);
            CircularProgressBar circularProgressBar = this.A0C;
            ImageView imageView = this.A07;
            TextView textView = this.A09;
            AbstractC42671vd.A0a(view, circularProgressBar, textView, imageView, true, !z, false);
            C12960it.A0r(getContext(), conversationRowImage$RowImageView, R.string.image_transfer_in_progress);
            if (r2.A0z.A02) {
                conversationRowImage$RowImageView.setOnClickListener(((AbstractC42671vd) this).A0A);
            } else {
                conversationRowImage$RowImageView.setOnClickListener(null);
            }
            AbstractView$OnClickListenerC34281fs r6 = ((AbstractC42671vd) this).A07;
            textView.setOnClickListener(r6);
            circularProgressBar.setOnClickListener(r6);
        } else {
            boolean A13 = C30041Vv.A13(fMessage);
            View view2 = this.A03;
            if (A13) {
                view2.setVisibility(8);
                CircularProgressBar circularProgressBar2 = this.A0C;
                ImageView imageView2 = this.A07;
                TextView textView2 = this.A09;
                AbstractC42671vd.A0a(view2, circularProgressBar2, textView2, imageView2, false, false, false);
                C12960it.A0r(getContext(), conversationRowImage$RowImageView, R.string.view_product);
                A0A = ((AbstractC42671vd) this).A0A;
                textView2.setOnClickListener(A0A);
            } else {
                view2.setVisibility(0);
                CircularProgressBar circularProgressBar3 = this.A0C;
                ImageView imageView3 = this.A07;
                TextView textView3 = this.A09;
                AbstractC42671vd.A0a(view2, circularProgressBar3, textView3, imageView3, false, !z, false);
                conversationRowImage$RowImageView.setContentDescription(null);
                if (!C30041Vv.A11(getFMessage())) {
                    textView3.setText(R.string.retry);
                    textView3.setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_upload, 0, 0, 0);
                    textView3.setOnClickListener(((AbstractC42671vd) this).A09);
                    A0A = ((AbstractC42671vd) this).A0A;
                } else {
                    A0A = AnonymousClass1OY.A0A(textView3, this, r2);
                }
            }
            conversationRowImage$RowImageView.setOnClickListener(A0A);
        }
        A0w();
        conversationRowImage$RowImageView.setOnLongClickListener(this.A1a);
        SpannableString A01 = this.A00.A01(r2);
        String str = r2.A09;
        String str2 = r2.A02;
        String str3 = r2.A05;
        Resources A09 = C12960it.A09(this);
        TextEmojiLabel textEmojiLabel = this.A0G;
        textEmojiLabel.setTextSize(getTextFontSize());
        TextEmojiLabel textEmojiLabel2 = this.A0D;
        textEmojiLabel2.setTextSize(AnonymousClass1OY.A02(getResources(), ((AbstractC28551Oa) this).A0K, -1));
        textEmojiLabel2.setTypeface(null, 0);
        textEmojiLabel2.setTextColor(getSecondaryTextColor());
        textEmojiLabel2.setVisibility(8);
        TextAndDateLayout textAndDateLayout = this.A0H;
        textAndDateLayout.setMaxTextLineCount(2);
        textAndDateLayout.invalidate();
        if (!TextUtils.isEmpty(str)) {
            setMessageText(str, textEmojiLabel, r2);
            textEmojiLabel.setVisibility(0);
        } else {
            textEmojiLabel.setVisibility(8);
        }
        View A0D = AnonymousClass028.A0D(this, R.id.product_content_layout);
        boolean z2 = r2.A0z.A02;
        if (z2 || C30041Vv.A0i(r2)) {
            A0D.setVisibility(8);
            this.A05.setVisibility(0);
        } else if (!TextUtils.isEmpty(str2) || !TextUtils.isEmpty(str3)) {
            boolean isEmpty = TextUtils.isEmpty(str2);
            TextEmojiLabel textEmojiLabel3 = this.A0E;
            if (!isEmpty) {
                setMessageText(str2, textEmojiLabel3, r2);
                textEmojiLabel3.setVisibility(0);
            } else {
                textEmojiLabel3.setVisibility(8);
            }
            boolean isEmpty2 = TextUtils.isEmpty(str3);
            TextEmojiLabel textEmojiLabel4 = this.A0F;
            if (!isEmpty2) {
                A17(textEmojiLabel4, r2, str3, true, false);
                textEmojiLabel4.setVisibility(0);
            } else {
                textEmojiLabel4.setVisibility(8);
            }
            A0D.setVisibility(0);
            this.A05.setVisibility(8);
        } else {
            A0D.setVisibility(8);
        }
        A19(r2);
        if (!TextUtils.isEmpty(A01)) {
            textEmojiLabel2.A0D(AnonymousClass3J9.A01, A01, getHighlightTerms(), 300, false);
            textEmojiLabel2.setVisibility(0);
        } else if (!TextUtils.isEmpty(str)) {
            textEmojiLabel.setVisibility(8);
            textAndDateLayout.setMaxTextLineCount(1);
            textEmojiLabel2.setVisibility(0);
            textEmojiLabel2.A0F(str, null, 150, false);
            textEmojiLabel2.setTextSize(getTextFontSize());
            textEmojiLabel2.setTypeface(null, 1);
            C12980iv.A14(A09, textEmojiLabel2, R.color.catalog_list_product_primary_color);
        }
        conversationRowImage$RowImageView.A07 = false;
        conversationRowImage$RowImageView.setOutgoing(z2);
        int i = A002.A08;
        if (i == 0 || (A00 = A002.A06) == 0) {
            i = 100;
            A00 = AnonymousClass19O.A00(r2, 100);
            if (A00 <= 0) {
                i = C27531Hw.A01(getContext());
                A00 = (i * 9) >> 4;
            }
        }
        conversationRowImage$RowImageView.A04(i, A00);
        C12990iw.A1E(conversationRowImage$RowImageView);
        if (!z && this.A02) {
            this.A1O.A0D(r2);
        }
        this.A02 = false;
        this.A1O.A07(conversationRowImage$RowImageView, r2, this.A0J);
    }

    @Override // X.AbstractC42681ve, X.AnonymousClass1OZ, X.AbstractC28561Ob
    public void A0Z() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass2P6 A07 = AnonymousClass1OY.A07(this);
            AnonymousClass01J A08 = AnonymousClass1OY.A08(A07, this);
            AnonymousClass1OY.A0L(A08, this);
            AnonymousClass1OY.A0M(A08, this);
            AnonymousClass1OY.A0K(A08, this);
            AnonymousClass1OY.A0I(A07, A08, this, AnonymousClass1OY.A09(A08, this, AnonymousClass1OY.A0B(A08, this)));
            AnonymousClass1OY.A0O(A08, this);
            this.A00 = (AnonymousClass19N) A08.A32.get();
        }
    }

    @Override // X.AnonymousClass1OY
    public void A0s() {
        A1H(false);
        A0X(false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002a, code lost:
        if (r0.exists() == false) goto L_0x002c;
     */
    @Override // X.AbstractC42671vd, X.AnonymousClass1OY
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0y() {
        /*
            r6 = this;
            X.0o4 r0 = r6.A01
            if (r0 == 0) goto L_0x000b
            boolean r0 = X.AnonymousClass1OY.A0U(r6)
            if (r0 != 0) goto L_0x000b
            return
        L_0x000b:
            X.0mz r5 = r6.A0O
            X.0oV r5 = (X.AbstractC16130oV) r5
            X.1XV r5 = (X.AnonymousClass1XV) r5
            X.0oX r4 = X.AbstractC15340mz.A00(r5)
            X.1IS r0 = r5.A0z
            boolean r3 = r0.A02
            if (r3 != 0) goto L_0x0020
            boolean r0 = r4.A0P
            if (r0 != 0) goto L_0x0020
            return
        L_0x0020:
            java.io.File r0 = r4.A0F
            r2 = 0
            if (r0 == 0) goto L_0x002c
            boolean r0 = r0.exists()
            r1 = 1
            if (r0 != 0) goto L_0x002d
        L_0x002c:
            r1 = 0
        L_0x002d:
            java.lang.String r0 = "viewmessage/ from_me:"
            java.lang.StringBuilder r0 = X.C12960it.A0k(r0)
            X.AnonymousClass1OY.A0P(r4, r5, r0, r3)
            if (r1 != 0) goto L_0x0046
            boolean r0 = r6.A1O()
            if (r0 == 0) goto L_0x0046
            java.lang.String r0 = "viewmessage/ no file to download from receiver side"
            com.whatsapp.util.Log.w(r0)
            return
        L_0x0046:
            com.whatsapp.conversation.conversationrow.ConversationRowImage$RowImageView r0 = r6.A0I
            r6.A14(r0, r5, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C60632yL.A0y():void");
    }

    @Override // X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r2, boolean z) {
        boolean A1X = C12960it.A1X(r2, ((AbstractC28551Oa) this).A0O);
        super.A1D(r2, z);
        if (z || A1X) {
            A0X(A1X);
        }
    }

    @Override // X.AnonymousClass1OY, android.view.ViewGroup, android.view.View
    public void dispatchSetPressed(boolean z) {
        super.dispatchSetPressed(z);
        ConversationRowImage$RowImageView conversationRowImage$RowImageView = this.A0I;
        boolean isPressed = isPressed();
        if (conversationRowImage$RowImageView.A0A != isPressed) {
            conversationRowImage$RowImageView.A0A = isPressed;
            conversationRowImage$RowImageView.A02();
            conversationRowImage$RowImageView.invalidate();
        }
    }

    @Override // X.AnonymousClass1OY
    public int getBroadcastDrawableId() {
        return AnonymousClass1OY.A03(this);
    }

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_product_left;
    }

    @Override // X.AnonymousClass1OY
    public TextView getDateView() {
        AnonymousClass1XV r1 = (AnonymousClass1XV) ((AbstractC16130oV) ((AbstractC28551Oa) this).A0O);
        if ((!TextUtils.isEmpty(r1.A02) || !TextUtils.isEmpty(r1.A05)) && !r1.A0z.A02 && !C30041Vv.A0i(r1)) {
            return this.A0A;
        }
        return this.A0B;
    }

    @Override // X.AnonymousClass1OY
    public ViewGroup getDateWrapper() {
        AnonymousClass1XV r1 = (AnonymousClass1XV) ((AbstractC16130oV) ((AbstractC28551Oa) this).A0O);
        if ((!TextUtils.isEmpty(r1.A02) || !TextUtils.isEmpty(r1.A05)) && !r1.A0z.A02 && !C30041Vv.A0i(r1)) {
            return this.A04;
        }
        return this.A05;
    }

    @Override // X.AbstractC42671vd, X.AbstractC28551Oa
    public AnonymousClass1XV getFMessage() {
        return (AnonymousClass1XV) ((AbstractC16130oV) ((AbstractC28551Oa) this).A0O);
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_product_left;
    }

    @Override // X.AbstractC28551Oa
    public int getMainChildMaxWidth() {
        int i = 72;
        if (((AbstractC28551Oa) this).A0R) {
            i = 100;
        }
        return AnonymousClass3GD.A01(getContext(), i);
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_product_right;
    }

    @Override // X.AbstractC42671vd, X.AbstractC28551Oa
    public void setFMessage(AbstractC15340mz r2) {
        AnonymousClass009.A0F(r2 instanceof AnonymousClass1XV);
        super.setFMessage(r2);
    }
}
