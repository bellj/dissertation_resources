package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.whatsapp.shareinvitelink.ShareInviteLinkActivity;

/* renamed from: X.3fO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C72923fO extends BroadcastReceiver {
    public final /* synthetic */ ShareInviteLinkActivity A00;

    public C72923fO(ShareInviteLinkActivity shareInviteLinkActivity) {
        this.A00 = shareInviteLinkActivity;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if ("android.nfc.action.ADAPTER_STATE_CHANGED".equals(intent.getAction())) {
            this.A00.invalidateOptionsMenu();
        }
    }
}
