package X;

import com.whatsapp.calling.callgrid.viewmodel.CallGridViewModel;

/* renamed from: X.3DO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3DO {
    public float A00;
    public float A01;
    public boolean A02;
    public final int A03;
    public final int A04;
    public final int A05;
    public final int A06;
    public final int A07;
    public final CallGridViewModel A08;

    public AnonymousClass3DO(CallGridViewModel callGridViewModel, int i, int i2, int i3, int i4, int i5, boolean z) {
        this.A08 = callGridViewModel;
        this.A04 = i;
        this.A05 = i2;
        this.A06 = i3;
        this.A07 = i4;
        this.A03 = i5;
        this.A02 = z;
    }

    public void A00() {
        int i;
        int i2 = this.A05;
        boolean z = this.A02;
        if (z) {
            i = this.A04;
        } else {
            i = 0;
        }
        float f = this.A01;
        if (f < 0.0f) {
            i2 = (int) (((float) i2) + (((float) this.A07) * f));
            if (z) {
                i = (int) (((float) i) + (((float) this.A06) * f));
            }
        }
        float f2 = this.A00;
        if (f2 > 0.0f) {
            i = (int) (((float) i) + (((float) this.A03) * f2));
        }
        AnonymousClass016 r1 = this.A08.A0B;
        Object A01 = r1.A01();
        AnonymousClass009.A05(A01);
        C91984Tz r0 = (C91984Tz) A01;
        r0.A05 = i2;
        r0.A02 = i;
        r1.A0B(r0);
    }
}
