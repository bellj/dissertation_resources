package X;

/* renamed from: X.2GK  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2GK {
    public final AnonymousClass17S A00;
    public final C22260yn A01;
    public final C14900mE A02;
    public final AnonymousClass18U A03;
    public final C15570nT A04;
    public final C15450nH A05;
    public final C250918b A06;
    public final C251118d A07;
    public final C15550nR A08;
    public final C26311Cv A09;
    public final C22700zV A0A;
    public final C15610nY A0B;
    public final C253318z A0C;
    public final C18640sm A0D;
    public final AnonymousClass01d A0E;
    public final C14830m7 A0F;
    public final C17170qN A0G;
    public final C22610zM A0H;
    public final C15680nj A0I;
    public final C14850m9 A0J;
    public final C16120oU A0K;
    public final C17220qS A0L;
    public final C22410z2 A0M;
    public final C22710zW A0N;
    public final C17070qD A0O;
    public final AbstractC14440lR A0P;

    public AnonymousClass2GK(AnonymousClass17S r2, C22260yn r3, C14900mE r4, AnonymousClass18U r5, C15570nT r6, C15450nH r7, C250918b r8, C251118d r9, C15550nR r10, C26311Cv r11, C22700zV r12, C15610nY r13, C253318z r14, C18640sm r15, AnonymousClass01d r16, C14830m7 r17, C17170qN r18, C22610zM r19, C15680nj r20, C14850m9 r21, C16120oU r22, C17220qS r23, C22410z2 r24, C22710zW r25, C17070qD r26, AbstractC14440lR r27) {
        this.A0F = r17;
        this.A0J = r21;
        this.A02 = r4;
        this.A04 = r6;
        this.A0P = r27;
        this.A0K = r22;
        this.A00 = r2;
        this.A05 = r7;
        this.A03 = r5;
        this.A0L = r23;
        this.A08 = r10;
        this.A0E = r16;
        this.A0B = r13;
        this.A01 = r3;
        this.A0O = r26;
        this.A0C = r14;
        this.A0A = r12;
        this.A0I = r20;
        this.A0N = r25;
        this.A0M = r24;
        this.A07 = r9;
        this.A0D = r15;
        this.A09 = r11;
        this.A0H = r19;
        this.A0G = r18;
        this.A06 = r8;
    }

    public AnonymousClass2JY A00(ActivityC13810kN r49, boolean z, boolean z2) {
        C14830m7 r1 = this.A0F;
        C14850m9 r12 = this.A0J;
        C14900mE r13 = this.A02;
        C15570nT r14 = this.A04;
        AbstractC14440lR r15 = this.A0P;
        C16120oU r16 = this.A0K;
        AnonymousClass17S r17 = this.A00;
        C15450nH r18 = this.A05;
        AnonymousClass18U r19 = this.A03;
        C17220qS r110 = this.A0L;
        C15550nR r152 = this.A08;
        AnonymousClass01d r142 = this.A0E;
        C15610nY r132 = this.A0B;
        C22260yn r122 = this.A01;
        C17070qD r11 = this.A0O;
        C253318z r10 = this.A0C;
        C22700zV r9 = this.A0A;
        C15680nj r8 = this.A0I;
        C22710zW r7 = this.A0N;
        C22410z2 r6 = this.A0M;
        C251118d r5 = this.A07;
        C18640sm r4 = this.A0D;
        C26311Cv r3 = this.A09;
        C22610zM r2 = this.A0H;
        return new AnonymousClass2JY(r17, r122, r49, r13, r19, r14, r18, this.A06, r5, r152, r3, r9, r132, r10, r4, r142, r1, this.A0G, r2, r8, r12, r16, r110, r6, r7, r11, r15, null, z, z2);
    }
}
