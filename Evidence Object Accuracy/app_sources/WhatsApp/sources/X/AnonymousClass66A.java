package X;

/* renamed from: X.66A  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass66A implements AbstractC136176Lh {
    public final /* synthetic */ C130185yw A00;

    public AnonymousClass66A(C130185yw r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC136176Lh
    public void AQg(boolean z) {
        EnumC124565pk r1;
        C130185yw r2 = this.A00;
        if (z) {
            r1 = EnumC124565pk.AUTOFOCUS_SUCCESS;
        } else {
            r1 = EnumC124565pk.AUTOFOCUS_FAILED;
        }
        r2.A09(r1, null);
    }
}
