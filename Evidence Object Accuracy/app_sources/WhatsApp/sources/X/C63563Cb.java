package X;

import android.os.Handler;
import com.facebook.redex.RunnableBRunnable0Shape3S0300000_I1;
import java.util.concurrent.Executor;

/* renamed from: X.3Cb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63563Cb {
    public Handler A00 = C12970iu.A0E();
    public Executor A01;

    public C63563Cb(Executor executor) {
        this.A01 = executor;
    }

    public void A00(AnonymousClass5U2 r4, AbstractCallableC112595Dz r5) {
        this.A01.execute(new RunnableBRunnable0Shape3S0300000_I1(this, r5, r4, 20));
    }
}
