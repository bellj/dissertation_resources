package X;

import android.content.Context;
import android.util.SparseArray;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0l7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14260l7 {
    public final Context A00;
    public final SparseArray A01;
    public final C64173En A02;
    public final Map A03 = Collections.synchronizedMap(new HashMap());
    public final Map A04 = Collections.synchronizedMap(new HashMap());
    public final Map A05 = new HashMap();
    public final boolean A06;

    public C14260l7(Context context, SparseArray sparseArray, C64173En r4) {
        this.A00 = context;
        this.A02 = r4;
        this.A06 = r4.A02.A01;
        this.A01 = sparseArray;
    }

    public Context A00() {
        return this.A00;
    }

    public C64173En A01() {
        return this.A02;
    }

    public Object A02(int i) {
        Object obj = this.A01.get(i);
        if (obj != null) {
            return obj;
        }
        StringBuilder sb = new StringBuilder("Null value associated with key: ");
        sb.append(this.A00.getResources().getResourceEntryName(i));
        throw new NullPointerException(sb.toString());
    }

    public boolean A03() {
        return this.A06;
    }
}
