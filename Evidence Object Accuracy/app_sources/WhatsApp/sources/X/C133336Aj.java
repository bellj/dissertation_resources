package X;

import com.whatsapp.util.Log;

/* renamed from: X.6Aj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133336Aj implements AnonymousClass6MV {
    public final /* synthetic */ C129345xZ A00;

    public C133336Aj(C129345xZ r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass6MV
    public void APo(C452120p r4) {
        Log.e("PAY: BrazilDeviceRegistrationAction/getPaymentProviderPublicKey iq returned null");
        this.A00.A00(C117305Zk.A0L(), null);
    }

    @Override // X.AnonymousClass6MV
    public void AVE(AnonymousClass6B7 r3) {
        this.A00.A00(null, r3);
    }
}
