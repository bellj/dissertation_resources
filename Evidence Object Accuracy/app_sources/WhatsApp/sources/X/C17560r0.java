package X;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.0r0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17560r0 {
    public static final Set A00;

    static {
        HashSet hashSet = new HashSet();
        hashSet.add(2498049);
        hashSet.add(2498048);
        hashSet.add(2498050);
        hashSet.add(2498051);
        hashSet.add(2498052);
        hashSet.add(2498053);
        hashSet.add(2498056);
        A00 = Collections.unmodifiableSet(hashSet);
    }
}
