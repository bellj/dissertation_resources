package X;

import android.content.Context;

/* renamed from: X.18d  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C251118d {
    public final C14850m9 A00;
    public final AnonymousClass13N A01;

    public C251118d(C14850m9 r1, AnonymousClass13N r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public boolean A00() {
        C14850m9 r1 = this.A00;
        return r1.A07(450) && r1.A07(1594);
    }

    public boolean A01() {
        C14850m9 r1 = this.A00;
        return r1.A07(450) && r1.A07(1890);
    }

    public boolean A02() {
        C14850m9 r1 = this.A00;
        return r1.A07(450) && r1.A07(1512);
    }

    public boolean A03() {
        C14850m9 r1 = this.A00;
        return r1.A07(450) && r1.A07(1206);
    }

    public boolean A04() {
        C14850m9 r1 = this.A00;
        return r1.A07(450) && r1.A07(1280);
    }

    public boolean A05(Context context) {
        C14850m9 r1 = this.A00;
        return r1.A07(450) && this.A01.A06(context) && r1.A07(1770);
    }
}
