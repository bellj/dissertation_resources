package X;

import android.util.Log;
import java.util.ArrayList;
import java.util.Arrays;

/* renamed from: X.3mQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C76903mQ extends AnonymousClass4Y4 {
    public int A00;
    public AnonymousClass4IH A01;
    public AnonymousClass4TO A02;
    public AnonymousClass4SQ A03;
    public boolean A04;

    @Override // X.AnonymousClass4Y4
    public void A00(boolean z) {
        super.A00(z);
        if (z) {
            this.A03 = null;
            this.A02 = null;
            this.A01 = null;
        }
        this.A00 = 0;
        this.A04 = false;
    }

    @Override // X.AnonymousClass4Y4
    public boolean A01(C89944Lz r21, C95304dT r22, long j) {
        int i;
        if (this.A03 != null) {
            return false;
        }
        AnonymousClass4TO r0 = this.A02;
        AnonymousClass4SQ r4 = null;
        if (r0 == null) {
            C92954Yd.A00(r22, 1, false);
            r22.A09();
            int A0C = r22.A0C();
            int A09 = r22.A09();
            int A08 = r22.A08();
            if (A08 <= 0) {
                A08 = -1;
            }
            int A082 = r22.A08();
            if (A082 <= 0) {
                A082 = -1;
            }
            r22.A08();
            int A0C2 = r22.A0C();
            int pow = (int) Math.pow(2.0d, (double) (A0C2 & 15));
            int pow2 = (int) Math.pow(2.0d, (double) ((A0C2 & 240) >> 4));
            r22.A0C();
            this.A02 = new AnonymousClass4TO(Arrays.copyOf(r22.A02, r22.A00), A0C, A09, A08, A082, pow, pow2);
        } else {
            AnonymousClass4IH r02 = this.A01;
            if (r02 == null) {
                C92954Yd.A00(r22, 3, false);
                r22.A0O((int) r22.A0G());
                long A0G = r22.A0G();
                String[] strArr = new String[(int) A0G];
                for (int i2 = 0; ((long) i2) < A0G; i2++) {
                    strArr[i2] = r22.A0O((int) r22.A0G());
                }
                if ((r22.A0C() & 1) != 0) {
                    this.A01 = new AnonymousClass4IH(strArr);
                } else {
                    throw AnonymousClass496.A00("framing bit expected to be set");
                }
            } else {
                int i3 = r22.A00;
                byte[] bArr = new byte[i3];
                System.arraycopy(r22.A02, 0, bArr, 0, i3);
                int i4 = r0.A04;
                C92954Yd.A00(r22, 5, false);
                int A0C3 = r22.A0C() + 1;
                C92774Xj r7 = new C92774Xj(r22.A02);
                r7.A01(r22.A01 << 3);
                for (int i5 = 0; i5 < A0C3; i5++) {
                    if (r7.A00(24) == 5653314) {
                        int A00 = r7.A00(16);
                        int A002 = r7.A00(24);
                        long j2 = 0;
                        int i6 = 0;
                        if (!r7.A02()) {
                            boolean A02 = r7.A02();
                            while (i6 < A002) {
                                if (!A02 || r7.A02()) {
                                    r7.A00(5);
                                }
                                i6++;
                            }
                        } else {
                            r7.A00(5);
                            while (i6 < A002) {
                                int i7 = 0;
                                for (int i8 = A002 - i6; i8 > 0; i8 >>>= 1) {
                                    i7++;
                                }
                                int A003 = r7.A00(i7);
                                for (int i9 = 0; i9 < A003 && i6 < A002; i9++) {
                                    i6++;
                                }
                            }
                        }
                        int A004 = r7.A00(4);
                        if (A004 <= 2) {
                            if (A004 == 1 || A004 == 2) {
                                r7.A01(32);
                                r7.A01(32);
                                int A005 = r7.A00(4) + 1;
                                r7.A01(1);
                                if (A004 != 1) {
                                    j2 = ((long) A002) * ((long) A00);
                                } else if (A00 != 0) {
                                    j2 = (long) Math.floor(Math.pow((double) ((long) A002), 1.0d / ((double) ((long) A00))));
                                }
                                r7.A01((int) (j2 * ((long) A005)));
                            }
                        } else {
                            throw AnonymousClass496.A00(C12960it.A0W(A004, "lookup type greater than 2 not decodable: "));
                        }
                    } else {
                        throw AnonymousClass496.A00(C12960it.A0f(C12960it.A0k("expected code book to start with [0x56, 0x43, 0x42] at "), (r7.A01 << 3) + r7.A00));
                    }
                }
                int A006 = r7.A00(6) + 1;
                for (int i10 = 0; i10 < A006; i10++) {
                    if (r7.A00(16) != 0) {
                        throw AnonymousClass496.A00("placeholder of time domain transforms not zeroed out");
                    }
                }
                int A007 = r7.A00(6) + 1;
                for (int i11 = 0; i11 < A007; i11++) {
                    int A008 = r7.A00(16);
                    if (A008 == 0) {
                        r7.A01(8);
                        r7.A01(16);
                        r7.A01(16);
                        r7.A01(6);
                        r7.A01(8);
                        int A009 = r7.A00(4) + 1;
                        for (int i12 = 0; i12 < A009; i12++) {
                            r7.A01(8);
                        }
                    } else if (A008 == 1) {
                        int A0010 = r7.A00(5);
                        int i13 = -1;
                        int[] iArr = new int[A0010];
                        for (int i14 = 0; i14 < A0010; i14++) {
                            int A0011 = r7.A00(4);
                            iArr[i14] = A0011;
                            if (A0011 > i13) {
                                i13 = iArr[i14];
                            }
                        }
                        int i15 = i13 + 1;
                        int[] iArr2 = new int[i15];
                        for (int i16 = 0; i16 < i15; i16++) {
                            iArr2[i16] = r7.A00(3) + 1;
                            int A0012 = r7.A00(2);
                            if (A0012 > 0) {
                                r7.A01(8);
                            }
                            for (int i17 = 0; i17 < (1 << A0012); i17++) {
                                r7.A01(8);
                            }
                        }
                        r7.A01(2);
                        int A0013 = r7.A00(4);
                        int i18 = 0;
                        int i19 = 0;
                        for (int i20 = 0; i20 < A0010; i20++) {
                            i18 += iArr2[iArr[i20]];
                            while (i19 < i18) {
                                r7.A01(A0013);
                                i19++;
                            }
                        }
                    } else {
                        throw AnonymousClass496.A00(C12960it.A0W(A008, "floor type greater than 1 not decodable: "));
                    }
                }
                int A0014 = r7.A00(6) + 1;
                for (int i21 = 0; i21 < A0014; i21++) {
                    if (r7.A00(16) <= 2) {
                        r7.A01(24);
                        r7.A01(24);
                        r7.A01(24);
                        int A0015 = r7.A00(6) + 1;
                        r7.A01(8);
                        int[] iArr3 = new int[A0015];
                        for (int i22 = 0; i22 < A0015; i22++) {
                            int A0016 = r7.A00(3);
                            int i23 = 0;
                            if (r7.A02()) {
                                i23 = r7.A00(5);
                            }
                            iArr3[i22] = (i23 << 3) + A0016;
                        }
                        for (int i24 = 0; i24 < A0015; i24++) {
                            int i25 = 0;
                            do {
                                if ((iArr3[i24] & (1 << i25)) != 0) {
                                    r7.A01(8);
                                }
                                i25++;
                            } while (i25 < 8);
                        }
                    } else {
                        throw AnonymousClass496.A00("residueType greater than 2 is not decodable");
                    }
                }
                int A0017 = r7.A00(6) + 1;
                for (int i26 = 0; i26 < A0017; i26++) {
                    int A0018 = r7.A00(16);
                    if (A0018 != 0) {
                        Log.e("VorbisUtil", C12960it.A0W(A0018, "mapping type other than 0 not supported: "));
                    } else {
                        if (r7.A02()) {
                            i = r7.A00(4) + 1;
                        } else {
                            i = 1;
                        }
                        if (r7.A02()) {
                            int A0019 = r7.A00(8) + 1;
                            for (int i27 = 0; i27 < A0019; i27++) {
                                int i28 = 0;
                                for (int i29 = i4 - 1; i29 > 0; i29 >>>= 1) {
                                    i28++;
                                }
                                r7.A01(i28);
                                r7.A01(i28);
                            }
                        }
                        if (r7.A00(2) == 0) {
                            if (i > 1) {
                                for (int i30 = 0; i30 < i4; i30++) {
                                    r7.A01(4);
                                }
                            }
                            for (int i31 = 0; i31 < i; i31++) {
                                r7.A01(8);
                                r7.A01(8);
                                r7.A01(8);
                            }
                        } else {
                            throw AnonymousClass496.A00("to reserved bits must be zero after mapping coupling steps");
                        }
                    }
                }
                int A0020 = r7.A00(6) + 1;
                AnonymousClass4II[] r3 = new AnonymousClass4II[A0020];
                for (int i32 = 0; i32 < A0020; i32++) {
                    boolean A022 = r7.A02();
                    r7.A00(16);
                    r7.A00(16);
                    r7.A00(8);
                    r3[i32] = new AnonymousClass4II(A022);
                }
                if (r7.A02()) {
                    int i33 = 0;
                    for (int i34 = A0020 - 1; i34 > 0; i34 >>>= 1) {
                        i33++;
                    }
                    r4 = new AnonymousClass4SQ(r02, r0, bArr, r3, i33);
                } else {
                    throw AnonymousClass496.A00("framing bit after modes not set as expected");
                }
            }
        }
        this.A03 = r4;
        if (r4 != null) {
            AnonymousClass4TO r32 = r4.A02;
            ArrayList A0l = C12960it.A0l();
            A0l.add(r32.A06);
            A0l.add(r4.A03);
            C93844ap A0021 = C93844ap.A00();
            A0021.A0R = "audio/vorbis";
            A0021.A03 = r32.A01;
            A0021.A0A = r32.A00;
            A0021.A04 = r32.A04;
            A0021.A0D = r32.A05;
            A0021.A0S = A0l;
            r21.A00 = new C100614mC(A0021);
        }
        return true;
    }
}
