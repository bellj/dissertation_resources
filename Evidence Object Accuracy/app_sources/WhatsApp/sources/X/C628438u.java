package X;

import com.whatsapp.jid.UserJid;
import java.lang.ref.WeakReference;
import java.util.concurrent.CountDownLatch;

/* renamed from: X.38u  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C628438u extends AbstractC16350or {
    public AnonymousClass2Cu A00;
    public final C15570nT A01;
    public final AnonymousClass10A A02;
    public final C20730wE A03;
    public final UserJid A04;
    public final WeakReference A05;
    public final CountDownLatch A06 = new CountDownLatch(1);

    public C628438u(C15570nT r3, ActivityC13790kL r4, AnonymousClass10A r5, C20730wE r6, UserJid userJid) {
        this.A05 = C12970iu.A10(r4);
        this.A01 = r3;
        this.A03 = r6;
        this.A02 = r5;
        this.A04 = userJid;
        this.A00 = new C84413zG(this, userJid);
    }
}
