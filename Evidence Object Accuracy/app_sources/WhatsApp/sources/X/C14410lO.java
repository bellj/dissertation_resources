package X;

import android.net.Uri;
import android.text.TextUtils;
import com.whatsapp.InteractiveAnnotation;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0lO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14410lO {
    public final C21040wk A00;
    public final AbstractC15710nm A01;
    public final C14330lG A02;
    public final C14900mE A03;
    public final C15570nT A04;
    public final C002701f A05;
    public final C15450nH A06;
    public final C18790t3 A07;
    public final C14830m7 A08;
    public final C16590pI A09;
    public final C15660nh A0A;
    public final C241114g A0B;
    public final C14850m9 A0C;
    public final C19950uw A0D;
    public final C20110vE A0E;
    public final C19940uv A0F;
    public final AnonymousClass14Q A0G;
    public final AnonymousClass14P A0H;
    public final AnonymousClass14D A0I;
    public final AnonymousClass155 A0J;
    public final C14420lP A0K;
    public final AnonymousClass14C A0L;
    public final AnonymousClass153 A0M;
    public final AnonymousClass14R A0N;
    public final C16630pM A0O;
    public final C20320vZ A0P;
    public final C22600zL A0Q;
    public final AbstractC14440lR A0R;
    public final C22870zm A0S;

    public C14410lO(C21040wk r2, AbstractC15710nm r3, C14330lG r4, C14900mE r5, C15570nT r6, C002701f r7, C15450nH r8, C18790t3 r9, C14830m7 r10, C16590pI r11, C15660nh r12, C241114g r13, C14850m9 r14, C19950uw r15, C20110vE r16, C19940uv r17, AnonymousClass14Q r18, AnonymousClass14P r19, AnonymousClass14D r20, AnonymousClass155 r21, C14420lP r22, AnonymousClass14C r23, AnonymousClass153 r24, AnonymousClass14R r25, C16630pM r26, C20320vZ r27, C22600zL r28, AbstractC14440lR r29, C22870zm r30) {
        this.A09 = r11;
        this.A08 = r10;
        this.A0C = r14;
        this.A03 = r5;
        this.A01 = r3;
        this.A04 = r6;
        this.A0R = r29;
        this.A02 = r4;
        this.A0H = r19;
        this.A07 = r9;
        this.A00 = r2;
        this.A06 = r8;
        this.A0S = r30;
        this.A0Q = r28;
        this.A0G = r18;
        this.A0P = r27;
        this.A0J = r21;
        this.A0M = r24;
        this.A0K = r22;
        this.A0A = r12;
        this.A0N = r25;
        this.A0B = r13;
        this.A0I = r20;
        this.A0F = r17;
        this.A0D = r15;
        this.A0E = r16;
        this.A0O = r26;
        this.A0L = r23;
        this.A05 = r7;
    }

    public C38421o4 A00(Uri uri, C16150oX r20, C32731ce r21, AbstractC15340mz r22, String str, List list, List list2, List list3, byte b, int i, int i2, boolean z) {
        String str2;
        ArrayList arrayList = new ArrayList();
        if (list.size() > 1) {
            byte[] A01 = AnonymousClass15O.A01(this.A04, this.A08, false);
            if (A01 != null) {
                str2 = C003501n.A03(A01);
            } else {
                throw new IllegalStateException("multicast id could not be created");
            }
        } else {
            str2 = null;
        }
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(A03(uri, r20.A02(), r21, (AbstractC14640lm) it.next(), r22, str, str2, list2, list3, b, i, i2, z));
        }
        return new C38421o4(arrayList);
    }

    public C14430lQ A01(String str) {
        C14430lQ r0;
        if (str == null) {
            return null;
        }
        C14420lP r3 = this.A0K;
        synchronized (r3) {
            C006202y r1 = r3.A00;
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(1);
            if (r1.A04(sb.toString()) != null) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append(str);
                sb2.append(1);
                r0 = (C14430lQ) r1.A04(sb2.toString());
            } else {
                r0 = null;
            }
        }
        return r0;
    }

    public AnonymousClass1KC A02(AnonymousClass1K9 r6, boolean z) {
        C14420lP r1;
        C14430lQ A02;
        AbstractC14500lX r0;
        String str = r6.A03;
        if (str == null || (A02 = (r1 = this.A0K).A02(str, 0)) == null) {
            return null;
        }
        C14450lS r2 = new C14450lS(A02, r1, this.A0R, z);
        C14380lL r02 = r6.A00;
        C14370lK r12 = r02.A05;
        if (r02.A0A) {
            r0 = new C14490lW(r12);
        } else {
            r0 = new C38431o5(r12);
        }
        return new AnonymousClass1KC(r0, A02, r2, r6);
    }

    public AbstractC16130oV A03(Uri uri, C16150oX r12, C32731ce r13, AbstractC14640lm r14, AbstractC15340mz r15, String str, String str2, List list, List list2, byte b, int i, int i2, boolean z) {
        C20320vZ r8 = this.A0P;
        AbstractC15340mz A01 = r8.A01(r8.A07.A02(r14, true), b, this.A08.A00());
        if (A01 instanceof AbstractC16130oV) {
            AbstractC16130oV r4 = (AbstractC16130oV) A01;
            r4.A02 = r12;
            ((AbstractC15340mz) r4).A02 = 0;
            r4.A0Y(1);
            r4.A07 = null;
            r4.A01 = 0;
            ((AbstractC15340mz) r4).A08 = i;
            r8.A04(r4, r15);
            if (str != null) {
                r4.A03 = str.trim();
                if (TextUtils.isEmpty(r4.A15())) {
                    r4.A03 = null;
                }
            }
            File file = r12.A0F;
            if (file == null) {
                AnonymousClass009.A05(uri);
                r4.A07 = uri.toString();
                r4.A01 = 0;
            } else {
                r4.A07 = file.getName();
                r4.A01 = r12.A0F.length();
            }
            if (b == 2 || b == 3 || b == 43 || b == 13) {
                long j = r12.A0D;
                if (j > 0 || r12.A0E > 0) {
                    r4.A00 = (int) ((r12.A0E - j) / 1000);
                } else {
                    r4.A00 = C22200yh.A07(r12.A0F);
                }
            }
            r4.A0v(list);
            r12.A0A = r4.A01;
            r12.A0L = true;
            r4.A0m(str2);
            if (z) {
                r4.A0T(4);
            }
            if (list2 != null && !list2.isEmpty()) {
                r12.A0V = (InteractiveAnnotation[]) list2.toArray(new InteractiveAnnotation[0]);
            }
            if (i2 > 0) {
                r4.A0T(1);
                ((AbstractC15340mz) r4).A05 = i2;
            }
            C14850m9 r2 = this.A0C;
            boolean A07 = r2.A07(815);
            boolean A072 = r2.A07(1267);
            if (A07 && A072 && r13 != null) {
                r4.A0K = r13;
            }
            return r4;
        }
        StringBuilder sb = new StringBuilder("FMessageFactory/newFMessageMedia/wrong message type; mediaWaType=");
        sb.append((int) b);
        throw new IllegalArgumentException(sb.toString());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0015, code lost:
        if (r1 == false) goto L_0x0017;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Boolean A04() {
        /*
            r3 = this;
            X.0m9 r1 = r3.A0C
            r0 = 147(0x93, float:2.06E-43)
            boolean r0 = r1.A07(r0)
            if (r0 == 0) goto L_0x0017
            X.0zm r2 = r3.A0S
            monitor-enter(r2)
            boolean r1 = r2.A01     // Catch: all -> 0x0010
            goto L_0x0013
        L_0x0010:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x0013:
            monitor-exit(r2)
            r0 = 1
            if (r1 != 0) goto L_0x0018
        L_0x0017:
            r0 = 0
        L_0x0018:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14410lO.A04():java.lang.Boolean");
    }
}
