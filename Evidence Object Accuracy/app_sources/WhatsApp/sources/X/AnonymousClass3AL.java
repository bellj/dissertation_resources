package X;

import android.util.JsonReader;
import com.facebook.redex.RunnableBRunnable0Shape11S0200000_I1_1;
import java.io.IOException;
import java.io.StringReader;

/* renamed from: X.3AL  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3AL {
    public static void A00(AbstractC116535Vv r5, String str) {
        try {
            JsonReader jsonReader = new JsonReader(new StringReader(str));
            C67883Tg r0 = new C67883Tg(jsonReader);
            r0.ALh();
            C12970iu.A0E().post(new RunnableBRunnable0Shape11S0200000_I1_1(AnonymousClass3AF.A00(r0), 0, r5));
            jsonReader.close();
        } catch (IOException e) {
            r5.APq(e.getMessage());
        }
    }
}
