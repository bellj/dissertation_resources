package X;

import java.util.Arrays;

/* renamed from: X.2rz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58922rz extends AbstractC87994Dv {
    public final int A00;

    public C58922rz(int i) {
        this.A00 = i;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass() && this.A00 == ((C58922rz) obj).A00) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        Object[] A1b = C12970iu.A1b();
        C12960it.A1O(A1b, this.A00);
        return Arrays.hashCode(A1b);
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("NoParamsStatusMessage{type=");
        A0k.append(this.A00);
        return C12970iu.A0v(A0k);
    }
}
