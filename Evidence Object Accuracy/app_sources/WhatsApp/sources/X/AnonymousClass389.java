package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.status.playback.fragment.StatusPlaybackContactFragment;

/* renamed from: X.389  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass389 extends AbstractC16350or {
    public int A00;
    public final C15650ng A01;
    public final AnonymousClass10U A02;
    public final C242714w A03;
    public final C18470sV A04;
    public final UserJid A05;
    public final AnonymousClass1IS A06;
    public final AnonymousClass01H A07;
    public final boolean A08;

    public AnonymousClass389(C15650ng r1, AnonymousClass10U r2, C242714w r3, C18470sV r4, UserJid userJid, AnonymousClass1IS r6, StatusPlaybackContactFragment statusPlaybackContactFragment, AnonymousClass01H r8, boolean z) {
        super(statusPlaybackContactFragment);
        this.A04 = r4;
        this.A01 = r1;
        this.A02 = r2;
        this.A03 = r3;
        this.A07 = r8;
        this.A06 = r6;
        this.A08 = z;
        this.A05 = userJid;
    }
}
