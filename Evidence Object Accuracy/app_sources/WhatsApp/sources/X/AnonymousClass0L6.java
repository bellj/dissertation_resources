package X;

import android.view.View;
import android.widget.PopupWindow;

/* renamed from: X.0L6  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0L6 {
    public static void A00(View view, PopupWindow popupWindow, int i, int i2, int i3) {
        popupWindow.showAsDropDown(view, i, i2, i3);
    }
}
