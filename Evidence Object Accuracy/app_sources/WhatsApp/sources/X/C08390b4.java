package X;

import android.view.View;

/* renamed from: X.0b4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08390b4 implements AbstractC12080hL {
    public final /* synthetic */ AnonymousClass09V A00;

    public C08390b4(AnonymousClass09V r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC12080hL
    public int AFn(View view, int i) {
        return Math.min(view == null ? 0 : view.getMeasuredHeight(), i);
    }
}
