package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.2Et  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C48182Et {
    public final int A00;
    public final int A01;
    public final UserJid A02;
    public final String A03;
    public final String A04;

    public C48182Et(UserJid userJid, String str, String str2, int i, int i2) {
        this.A01 = i;
        this.A02 = userJid;
        this.A04 = str;
        this.A03 = str2;
        this.A00 = i2;
    }
}
