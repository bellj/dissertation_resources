package X;

import android.content.Context;
import android.os.PowerManager;
import com.facebook.redex.RunnableBRunnable0Shape9S0200000_I0_9;
import org.whispersystems.jobqueue.Job;

/* renamed from: X.0w8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20670w8 {
    public AnonymousClass1LC A00;
    public final AbstractC15710nm A01;
    public final C16590pI A02;
    public final C14850m9 A03;

    public C20670w8(AbstractC15710nm r1, C16590pI r2, C14850m9 r3) {
        this.A02 = r2;
        this.A03 = r3;
        this.A01 = r1;
    }

    public void A00(Job job) {
        AnonymousClass1LC r6 = this.A00;
        if (job.parameters.wakeLock) {
            Context context = r6.A00;
            String obj = job.toString();
            long j = job.parameters.wakeLockTimeout;
            PowerManager.WakeLock newWakeLock = ((PowerManager) context.getSystemService("power")).newWakeLock(1, obj);
            if (j == 0) {
                newWakeLock.acquire();
            } else {
                newWakeLock.acquire(j);
            }
            job.A02 = newWakeLock;
        }
        r6.A01.execute(new RunnableBRunnable0Shape9S0200000_I0_9(r6, 1, job));
    }
}
