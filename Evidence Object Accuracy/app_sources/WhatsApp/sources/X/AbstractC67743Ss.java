package X;

import android.os.SystemClock;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

/* renamed from: X.3Ss  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC67743Ss implements AnonymousClass2BW {
    public int A00;
    public AnonymousClass3H3 A01;
    public final ArrayList A02 = C12980iv.A0w(1);
    public final boolean A03;

    public AbstractC67743Ss(boolean z) {
        this.A03 = z;
    }

    public final void A00() {
        AnonymousClass4PE r12;
        float f;
        int i;
        AnonymousClass4PE r8;
        int i2;
        AnonymousClass3H3 r11 = this.A01;
        for (int i3 = 0; i3 < this.A00; i3++) {
            boolean z = this.A03;
            C67733Sr r14 = (C67733Sr) ((AnonymousClass5QP) this.A02.get(i3));
            synchronized (r14) {
                if (z) {
                    if ((r11.A00 & 8) != 8) {
                        C95314dV.A04(C12960it.A1U(r14.A01));
                        long elapsedRealtime = SystemClock.elapsedRealtime();
                        int i4 = (int) (elapsedRealtime - r14.A05);
                        r14.A07 += (long) i4;
                        long j = r14.A06;
                        long j2 = r14.A04;
                        r14.A06 = j + j2;
                        if (i4 > 0) {
                            float f2 = (((float) j2) * 8000.0f) / ((float) i4);
                            C64683Gm r2 = r14.A0B;
                            int sqrt = (int) Math.sqrt((double) j2);
                            if (r2.A00 != 1) {
                                Collections.sort(r2.A05, C64683Gm.A07);
                                r2.A00 = 1;
                            }
                            int i5 = r2.A02;
                            if (i5 > 0) {
                                AnonymousClass4PE[] r0 = r2.A06;
                                i5--;
                                r2.A02 = i5;
                                r12 = r0[i5];
                            } else {
                                r12 = new AnonymousClass4PE();
                            }
                            int i6 = r2.A01;
                            r2.A01 = i6 + 1;
                            r12.A01 = i6;
                            r12.A02 = sqrt;
                            r12.A00 = f2;
                            ArrayList arrayList = r2.A05;
                            arrayList.add(r12);
                            int i7 = r2.A03 + sqrt;
                            while (true) {
                                r2.A03 = i7;
                                while (true) {
                                    int i8 = r2.A04;
                                    if (i7 <= i8) {
                                        break;
                                    }
                                    i = i7 - i8;
                                    r8 = (AnonymousClass4PE) arrayList.get(0);
                                    i2 = r8.A02;
                                    if (i2 <= i) {
                                        i7 -= i2;
                                        r2.A03 = i7;
                                        arrayList.remove(0);
                                        if (i5 < 5) {
                                            AnonymousClass4PE[] r02 = r2.A06;
                                            i5++;
                                            r2.A02 = i5;
                                            r02[i5] = r8;
                                        }
                                    }
                                }
                                r8.A02 = i2 - i;
                                i7 -= i;
                            }
                            if (r14.A07 >= 2000 || r14.A06 >= 524288) {
                                if (r2.A00 != 0) {
                                    Collections.sort(arrayList, C64683Gm.A08);
                                    r2.A00 = 0;
                                }
                                float f3 = 0.5f * ((float) r2.A03);
                                int i9 = 0;
                                int i10 = 0;
                                while (true) {
                                    if (i9 < arrayList.size()) {
                                        AnonymousClass4PE r1 = (AnonymousClass4PE) arrayList.get(i9);
                                        i10 += r1.A02;
                                        if (((float) i10) >= f3) {
                                            f = r1.A00;
                                            break;
                                        }
                                        i9++;
                                    } else {
                                        f = arrayList.isEmpty() ? Float.NaN : ((AnonymousClass4PE) arrayList.get(arrayList.size() - 1)).A00;
                                    }
                                }
                                r14.A02 = (long) f;
                            }
                            r14.A09(i4, r14.A04, r14.A02);
                            r14.A05 = elapsedRealtime;
                            r14.A04 = 0;
                        }
                        r14.A01--;
                    }
                }
            }
        }
        this.A01 = null;
    }

    public final void A01() {
        for (int i = 0; i < this.A00; i++) {
            this.A02.get(i);
        }
    }

    public final void A02(int i) {
        AnonymousClass3H3 r6 = this.A01;
        for (int i2 = 0; i2 < this.A00; i2++) {
            boolean z = this.A03;
            C67733Sr r4 = (C67733Sr) ((AnonymousClass5QP) this.A02.get(i2));
            synchronized (r4) {
                if (z) {
                    if ((r6.A00 & 8) != 8) {
                        r4.A04 += (long) i;
                    }
                }
            }
        }
    }

    public final void A03(AnonymousClass3H3 r6) {
        this.A01 = r6;
        for (int i = 0; i < this.A00; i++) {
            boolean z = this.A03;
            C67733Sr r3 = (C67733Sr) ((AnonymousClass5QP) this.A02.get(i));
            synchronized (r3) {
                if (z) {
                    if ((r6.A00 & 8) != 8) {
                        int i2 = r3.A01;
                        if (i2 == 0) {
                            r3.A05 = SystemClock.elapsedRealtime();
                        }
                        r3.A01 = i2 + 1;
                    }
                }
            }
        }
    }

    @Override // X.AnonymousClass2BW
    public final void A5p(AnonymousClass5QP r3) {
        ArrayList arrayList = this.A02;
        if (!arrayList.contains(r3)) {
            arrayList.add(r3);
            this.A00++;
        }
    }

    @Override // X.AnonymousClass2BW
    public /* synthetic */ Map AGF() {
        HttpURLConnection httpURLConnection;
        if (!(this instanceof C56132kO) || (httpURLConnection = ((C56132kO) this).A07) == null) {
            return Collections.emptyMap();
        }
        return httpURLConnection.getHeaderFields();
    }
}
