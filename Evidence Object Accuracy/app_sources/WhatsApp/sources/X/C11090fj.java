package X;

/* renamed from: X.0fj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C11090fj extends AnonymousClass1WI implements AnonymousClass1J7 {
    public C11090fj() {
        super(1);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0015, code lost:
        if (r3.getRect().top == 0) goto L_0x0017;
     */
    @Override // X.AnonymousClass1J7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object AJ4(java.lang.Object r3) {
        /*
            r2 = this;
            androidx.window.sidecar.SidecarDisplayFeature r3 = (androidx.window.sidecar.SidecarDisplayFeature) r3
            r0 = 0
            X.C16700pc.A0E(r3, r0)
            android.graphics.Rect r0 = r3.getRect()
            int r0 = r0.left
            if (r0 == 0) goto L_0x0017
            android.graphics.Rect r0 = r3.getRect()
            int r1 = r0.top
            r0 = 0
            if (r1 != 0) goto L_0x0018
        L_0x0017:
            r0 = 1
        L_0x0018:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C11090fj.AJ4(java.lang.Object):java.lang.Object");
    }
}
