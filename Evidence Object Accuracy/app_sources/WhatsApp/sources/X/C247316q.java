package X;

import com.whatsapp.util.Log;

/* renamed from: X.16q  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C247316q implements AbstractC21730xt {
    public AnonymousClass1WC A00;
    public final AnonymousClass018 A01;
    public final C17220qS A02;

    public C247316q(AnonymousClass018 r2, C17220qS r3) {
        C16700pc.A0E(r3, 1);
        C16700pc.A0E(r2, 2);
        this.A02 = r3;
        this.A01 = r2;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        C16700pc.A0E(str, 0);
        Log.e(C16700pc.A08("GetCommerceMetadataProtocolHelper/onDeliveryFailure: Network failed  while sending the payload: ", str));
        AnonymousClass1WC r0 = this.A00;
        if (r0 == null) {
            C16700pc.A0K("listener");
            throw new RuntimeException("Redex: Unreachable code after no-return invoke");
        }
        AnonymousClass0t9 r1 = r0.A00;
        r1.A06.set(false);
        AnonymousClass1WD r02 = r1.A01;
        if (r02 != null) {
            r02.A00();
        }
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r4, String str) {
        C16700pc.A0E(r4, 1);
        Log.e("GetCommerceMetadataProtocolHelper/response-error");
        AnonymousClass1V8 A0E = r4.A0E("error");
        if (A0E != null) {
            A0E.A05("code", 0);
            AnonymousClass1WC r0 = this.A00;
            if (r0 == null) {
                C16700pc.A0K("listener");
                throw new RuntimeException("Redex: Unreachable code after no-return invoke");
            }
            AnonymousClass0t9 r2 = r0.A00;
            r2.A06.set(false);
            AnonymousClass1WD r02 = r2.A01;
            if (r02 != null) {
                r02.A00();
            }
        }
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:58:0x00b1 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v0 */
    /* JADX WARN: Type inference failed for: r2v4 */
    /* JADX WARN: Type inference failed for: r2v5, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // X.AbstractC21730xt
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AX9(X.AnonymousClass1V8 r29, java.lang.String r30) {
        /*
        // Method dump skipped, instructions count: 455
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C247316q.AX9(X.1V8, java.lang.String):void");
    }
}
