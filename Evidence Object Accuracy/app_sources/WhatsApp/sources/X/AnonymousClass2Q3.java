package X;

import android.content.Context;
import android.util.AttributeSet;
import com.whatsapp.util.MarqueeToolbar;

/* renamed from: X.2Q3  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2Q3 extends MarqueeToolbar {
    public boolean A00;

    public AnonymousClass2Q3(Context context) {
        super(context);
        A0I();
    }

    public AnonymousClass2Q3(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A0I();
    }

    public AnonymousClass2Q3(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A0I();
    }
}
