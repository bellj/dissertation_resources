package X;

import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.inappsupport.ui.ContactUsActivity;
import com.whatsapp.inappsupport.ui.SupportTopicsActivity;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.regex.Pattern;

/* renamed from: X.1iw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C35891iw {
    public static Pattern A0C;
    public AnonymousClass23Q A00 = new AnonymousClass57P(this);
    public AnonymousClass23Q A01 = new AnonymousClass57Q(this);
    public ContactUsActivity A02;
    public GroupJid A03;
    public boolean A04;
    public boolean A05;
    public final C14900mE A06;
    public final C19990v2 A07;
    public final C27151Gf A08 = new C35901iy(this);
    public final C21320xE A09;
    public final C14850m9 A0A;
    public final C35881iv A0B = new C35881iv(this);

    public C35891iw(C14900mE r2, C19990v2 r3, C21320xE r4, C14850m9 r5, ContactUsActivity contactUsActivity) {
        this.A0A = r5;
        this.A06 = r2;
        this.A07 = r3;
        this.A09 = r4;
        this.A02 = contactUsActivity;
        A0C = Pattern.compile("[^\\p{L}\\p{N}\\p{P}\\p{Z}]");
    }

    public static /* synthetic */ void A00(C91934Tu r13, C35891iw r14, int i) {
        ContactUsActivity contactUsActivity = r14.A02;
        AnonymousClass009.A05(contactUsActivity);
        int i2 = r13.A00;
        if (i2 != 0) {
            ArrayList arrayList = new ArrayList(i2);
            for (int i3 = 0; i3 < i2; i3++) {
                arrayList.add(new AnonymousClass3M6(null, (String) r13.A04.get(i3), (String) r13.A06.get(i3), (String) r13.A03.get(i3), (String) r13.A07.get(i3), null, false));
            }
            int i4 = 2;
            if (i == 2) {
                i4 = 1;
            }
            String stringExtra = contactUsActivity.getIntent().getStringExtra("com.whatsapp.inappsupport.ui.ContactUsActivity.from");
            ContactUsActivity contactUsActivity2 = r14.A02;
            if (!"biz-directory-browsing".equals(stringExtra)) {
                String str = r13.A01;
                ArrayList A2e = contactUsActivity2.A2e(arrayList);
                Intent intent = new Intent(contactUsActivity2, SupportTopicsActivity.class);
                intent.putParcelableArrayListExtra("com.whatsapp.inappsupport.ui.SupportTopicsActivity.support_topics", A2e);
                intent.putExtra("com.whatsapp.inappsupport.ui.SupportTopicsActivity.ui_version", 2);
                intent.putExtra("com.whatsapp.inappsupport.ui.SupportTopicsActivity.contact_us_action", i4);
                intent.putExtra("com.whatsapp.inappsupport.ui.ContactUsActvity.support_type", i);
                intent.putExtra("com.whatsapp.inappsupport.ui.ContactUsActivity.debug_info", str);
                contactUsActivity2.startActivityForResult(intent, 11);
                return;
            }
        } else if (i != 2) {
            r14.A03(null);
            return;
        }
        r14.A01();
    }

    public void A01() {
        ContactUsActivity contactUsActivity = this.A02;
        AnonymousClass009.A05(contactUsActivity);
        String trim = contactUsActivity.A00.getText().toString().trim();
        boolean isChecked = this.A02.A02.isChecked();
        contactUsActivity.A2f(3, trim);
        contactUsActivity.A0K.A01(contactUsActivity, contactUsActivity.A0C, contactUsActivity.A0P, trim, contactUsActivity.A0O, contactUsActivity.A0Q, new ArrayList(), null, isChecked);
    }

    public void A02(int i) {
        AnonymousClass23Q r12;
        ContactUsActivity contactUsActivity = this.A02;
        AnonymousClass009.A05(contactUsActivity);
        AnonymousClass009.A05(contactUsActivity);
        int length = A0C.matcher(contactUsActivity.A00.getText().toString().trim()).replaceAll("").getBytes().length;
        if (this.A05 || length >= 10) {
            ContactUsActivity contactUsActivity2 = this.A02;
            AnonymousClass009.A05(contactUsActivity2);
            if ("biz-directory-browsing".equals(contactUsActivity2.getIntent().getStringExtra("com.whatsapp.inappsupport.ui.ContactUsActivity.from"))) {
                A01();
                return;
            }
            this.A02.A01.setVisibility(8);
            ContactUsActivity contactUsActivity3 = this.A02;
            contactUsActivity3.A00.setBackgroundDrawable(AnonymousClass00T.A04(contactUsActivity3, R.drawable.description_field_background_state_list));
            ContactUsActivity contactUsActivity4 = this.A02;
            if (i == 1) {
                r12 = this.A00;
            } else {
                r12 = this.A01;
            }
            String trim = contactUsActivity4.A00.getText().toString().trim();
            AbstractC14440lR r11 = ((ActivityC13830kP) contactUsActivity4).A05;
            String str = contactUsActivity4.A0P;
            String str2 = contactUsActivity4.A0Q;
            C627638m r1 = contactUsActivity4.A0F;
            if (r1 != null && r1.A00() == 1) {
                contactUsActivity4.A0F.A03(false);
            }
            C18790t3 r15 = contactUsActivity4.A06;
            C22650zQ r14 = contactUsActivity4.A0N;
            C14950mJ r10 = ((ActivityC13790kL) contactUsActivity4).A06;
            C252018m r9 = contactUsActivity4.A0M;
            AnonymousClass018 r8 = contactUsActivity4.A08;
            AnonymousClass10G r7 = contactUsActivity4.A03;
            C17050qB r6 = contactUsActivity4.A07;
            AnonymousClass11G r5 = contactUsActivity4.A0D;
            C627638m r0 = new C627638m(r7, contactUsActivity4, r15, r6, ((ActivityC13810kN) contactUsActivity4).A09, r8, r10, contactUsActivity4.A0C, r5, r12, contactUsActivity4.A0K, r9, r14, str, str2, trim, null, new Uri[0]);
            contactUsActivity4.A0F = r0;
            r11.Aaz(r0, new Void[0]);
            return;
        }
        ContactUsActivity contactUsActivity5 = this.A02;
        contactUsActivity5.A00.setBackgroundDrawable(AnonymousClass00T.A04(contactUsActivity5, R.drawable.describe_problem_edittext_bg_error));
        ContactUsActivity contactUsActivity6 = this.A02;
        int i2 = R.string.describe_problem_description_further;
        if (length == 0) {
            i2 = R.string.describe_problem_description;
        }
        contactUsActivity6.A01.setText(i2);
        this.A02.A01.setVisibility(0);
    }

    public void A03(String str) {
        ContactUsActivity contactUsActivity = this.A02;
        AnonymousClass009.A05(contactUsActivity);
        contactUsActivity.A2P(contactUsActivity.getString(R.string.support_ticket_sending));
        ContactUsActivity contactUsActivity2 = this.A02;
        String trim = contactUsActivity2.A00.getText().toString().trim();
        if (!this.A02.A02.isChecked()) {
            str = null;
        }
        boolean isChecked = this.A02.A02.isChecked();
        C35881iv r5 = this.A0B;
        AnonymousClass1BS r3 = contactUsActivity2.A0E;
        StringBuilder sb = new StringBuilder("ChatSupportTicketManager/contactSupport called, shouldUploadLogs=");
        sb.append(isChecked);
        Log.i(sb.toString());
        C36201jU r6 = new C36201jU(r3.A00, new C35871iu(r3, r5, isChecked), r3.A01);
        C17220qS r52 = r6.A02;
        String A01 = r52.A01();
        C41141sy r32 = new C41141sy("iq");
        r32.A04(new AnonymousClass1W9("id", A01));
        r32.A04(new AnonymousClass1W9("type", "set"));
        r32.A04(new AnonymousClass1W9(AnonymousClass1VY.A00, "to"));
        r32.A04(new AnonymousClass1W9("xmlns", "fb:thrift_iq"));
        r32.A04(new AnonymousClass1W9("smax_id", "3"));
        r32.A05(new AnonymousClass1V8("description", trim, (AnonymousClass1W9[]) null));
        if (!TextUtils.isEmpty(str)) {
            r32.A05(new AnonymousClass1V8("debug_information_json", str, (AnonymousClass1W9[]) null));
        }
        r52.A0D(r6, r32.A03(), A01, 256, 32000);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0038, code lost:
        if (r0.A07.A0D(r2) == false) goto L_0x003a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A04(boolean r6) {
        /*
            r5 = this;
            com.whatsapp.inappsupport.ui.ContactUsActivity r0 = r5.A02
            r4 = 1
            if (r0 == 0) goto L_0x009a
            boolean r0 = r5.A04
            if (r0 != 0) goto L_0x009a
            X.0m9 r1 = r5.A0A
            r0 = 819(0x333, float:1.148E-42)
            boolean r2 = r1.A07(r0)
            if (r2 == 0) goto L_0x0062
            com.whatsapp.jid.GroupJid r1 = r5.A03
            if (r1 == 0) goto L_0x0062
            X.0v2 r0 = r5.A07
            boolean r0 = r0.A0D(r1)
            if (r0 == 0) goto L_0x0062
            java.lang.String r0 = "SupportContactUsPresenter/openChatOrShowTicketHaveCreatedDialog - opening chat"
            com.whatsapp.util.Log.i(r0)
            com.whatsapp.inappsupport.ui.ContactUsActivity r0 = r5.A02
            r0.AaN()
            com.whatsapp.inappsupport.ui.ContactUsActivity r3 = r5.A02
            com.whatsapp.jid.GroupJid r2 = r5.A03
            X.1iw r0 = r3.A0G
            if (r2 == 0) goto L_0x003a
            X.0v2 r0 = r0.A07
            boolean r0 = r0.A0D(r2)
            r1 = 1
            if (r0 != 0) goto L_0x003b
        L_0x003a:
            r1 = 0
        L_0x003b:
            java.lang.String r0 = "Support group to open doesn't exist"
            X.AnonymousClass009.A0A(r0, r1)
            java.lang.String r1 = "contactusactivity/tryopensupportchat/exists/"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            com.whatsapp.util.Log.i(r0)
            X.0mK r0 = new X.0mK
            r0.<init>()
            android.content.Intent r0 = r0.A0i(r3, r2)
            r3.A2G(r0, r4)
        L_0x005d:
            r5.A04 = r4
        L_0x005f:
            boolean r0 = r5.A04
            return r0
        L_0x0062:
            if (r6 != 0) goto L_0x006a
            if (r2 == 0) goto L_0x006a
            com.whatsapp.jid.GroupJid r0 = r5.A03
            if (r0 != 0) goto L_0x005f
        L_0x006a:
            java.lang.String r0 = "SupportContactUsPresenter/openChatOrShowTicketHaveCreatedDialog - showing dialog"
            com.whatsapp.util.Log.i(r0)
            com.whatsapp.inappsupport.ui.ContactUsActivity r0 = r5.A02
            r0.AaN()
            com.whatsapp.inappsupport.ui.ContactUsActivity r3 = r5.A02
            r1 = 2131887092(0x7f1203f4, float:1.9408781E38)
            r0 = 0
            java.lang.Object[] r0 = new java.lang.Object[r0]
            X.2AC r2 = com.whatsapp.MessageDialogFragment.A01(r0, r1)
            r1 = 2131890036(0x7f120f74, float:1.9414752E38)
            X.4fF r0 = new X.4fF
            r0.<init>()
            r2.A02(r0, r1)
            r2.A00 = r4
            androidx.fragment.app.DialogFragment r2 = r2.A01()
            X.01F r1 = r3.A0V()
            r0 = 0
            r2.A1F(r1, r0)
            goto L_0x005d
        L_0x009a:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C35891iw.A04(boolean):boolean");
    }
}
