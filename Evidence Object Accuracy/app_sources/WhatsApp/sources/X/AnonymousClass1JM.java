package X;

import java.util.Arrays;

/* renamed from: X.1JM  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1JM {
    public final AnonymousClass1JS A00;
    public final AnonymousClass1JU A01;
    public final String A02;

    public AnonymousClass1JM(AnonymousClass1JS r1, AnonymousClass1JU r2, String str) {
        this.A02 = str;
        this.A01 = r2;
        this.A00 = r1;
    }

    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        AnonymousClass1JM r4 = (AnonymousClass1JM) obj;
        if (!r4.A02.equals(this.A02) || !r4.A01.equals(this.A01) || !r4.A00.equals(this.A00)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A02, this.A01, this.A00});
    }
}
