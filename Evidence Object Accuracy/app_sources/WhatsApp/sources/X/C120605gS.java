package X;

import android.content.Context;
import com.whatsapp.util.Log;
import java.util.ArrayList;

/* renamed from: X.5gS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120605gS extends C120895gv {
    public final /* synthetic */ C120545gM A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120605gS(Context context, C14900mE r8, C18650sn r9, C64513Fv r10, C120545gM r11) {
        super(context, r8, r9, r10, "upi-get-banks");
        this.A00 = r11;
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A02(C452120p r2) {
        super.A02(r2);
        AnonymousClass6MP r0 = this.A00.A00;
        if (r0 != null) {
            r0.ANA(r2);
        }
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A03(C452120p r2) {
        super.A03(r2);
        AnonymousClass6MP r0 = this.A00.A00;
        if (r0 != null) {
            r0.ANA(r2);
        }
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A04(AnonymousClass1V8 r8) {
        super.A04(r8);
        C120545gM r6 = this.A00;
        AbstractC43531xB AEz = r6.A08.A02().AEz();
        AnonymousClass009.A05(AEz);
        ArrayList AYv = AEz.AYv(r6.A03, r8);
        C1308460e r1 = r6.A05;
        C127295uG A03 = r1.A03(((C126705tJ) r6).A00, AYv);
        ArrayList arrayList = A03.A01;
        ArrayList arrayList2 = A03.A02;
        C119715ez r3 = A03.A00;
        if (C120545gM.A00(r3, r6.A06, arrayList, arrayList2)) {
            r1.A0A(r3, arrayList, arrayList2);
            AnonymousClass6MP r0 = r6.A00;
            if (r0 != null) {
                r0.AN9(r3, null, arrayList, arrayList2);
                return;
            }
            return;
        }
        StringBuilder A0k = C12960it.A0k("PAY: received invalid data from get-banks: banks: ");
        A0k.append(arrayList);
        A0k.append(" psps: ");
        A0k.append(arrayList2);
        A0k.append(" pspRouting: ");
        A0k.append(r3);
        Log.w(C12960it.A0d(" , try get bank list directly.", A0k));
        AnonymousClass6MP r12 = r6.A00;
        if (r12 != null) {
            r12.AN9(null, C117305Zk.A0L(), null, null);
        }
    }
}
