package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1RM  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1RM {
    public final C15450nH A00;
    public final C14830m7 A01;
    public final AnonymousClass1RO A02;

    public AnonymousClass1RM(C15450nH r1, C14830m7 r2, AnonymousClass1RO r3) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    public int A00() {
        C16310on A01 = this.A02.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT COUNT(*) FROM prekeys WHERE sent_to_server = 0 AND direct_distribution = 0", null);
            if (A09.moveToNext()) {
                int i = A09.getInt(0);
                A09.close();
                A01.close();
                return i;
            }
            throw new SQLiteException("unable to fetch count from table");
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public List A01() {
        ArrayList arrayList = new ArrayList();
        C16310on A01 = this.A02.get();
        try {
            Cursor A08 = A01.A03.A08("prekeys", "sent_to_server = 0 AND direct_distribution = 0", null, String.valueOf(this.A00.A02(AbstractC15460nI.A1K)), new String[]{"prekey_id", "record"}, null);
            while (A08.moveToNext()) {
                arrayList.add(new AnonymousClass1RW(A08.getInt(0), A08.getBlob(1)));
            }
            A08.close();
            A01.close();
            return arrayList;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A02(int i) {
        C16310on A02 = this.A02.A02();
        try {
            C16330op r6 = A02.A03;
            String[] strArr = {String.valueOf(i)};
            StringBuilder sb = new StringBuilder();
            sb.append("axolotl deleted ");
            sb.append((long) r6.A01("prekeys", "prekey_id = ?", strArr));
            sb.append(" pre keys with id ");
            sb.append(i);
            Log.i(sb.toString());
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A03(int[] iArr) {
        AnonymousClass1RO r7 = this.A02;
        C16310on A02 = r7.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            long A002 = this.A01.A00() / 1000;
            int i = 0;
            while (true) {
                int length = iArr.length;
                if (i < length) {
                    int min = Math.min(i + 200, length);
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("sent_to_server", Boolean.TRUE);
                    contentValues.put("upload_timestamp", Long.valueOf(A002));
                    StringBuilder sb = new StringBuilder("?");
                    String[] strArr = new String[min - i];
                    for (int i2 = i; i2 < min; i2++) {
                        strArr[i2 - i] = String.valueOf(iArr[i2]);
                        if (i2 != i) {
                            sb.append(",?");
                        }
                    }
                    C16310on A022 = r7.A02();
                    C16330op r9 = A022.A03;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("prekey_id IN (");
                    sb2.append((Object) sb);
                    sb2.append(")");
                    int A003 = r9.A00("prekeys", contentValues, sb2.toString(), strArr);
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("updated ");
                    sb3.append(A003);
                    sb3.append(" prekeys; values=");
                    sb3.append(contentValues);
                    Log.i(sb3.toString());
                    A022.close();
                    i = min;
                } else {
                    C16310on A023 = r7.A02();
                    ContentValues contentValues2 = new ContentValues();
                    contentValues2.put("upload_timestamp", Long.valueOf(A002));
                    A023.A03.A02(contentValues2, "prekey_uploads");
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append("axolotl addPreKeyUpload ts:");
                    sb4.append(A002);
                    Log.i(sb4.toString());
                    A023.close();
                    A00.A00();
                    A00.close();
                    A02.close();
                    return;
                }
            }
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public byte[] A04(int i) {
        C16310on A01 = this.A02.get();
        try {
            Cursor A08 = A01.A03.A08("prekeys", "prekey_id = ?", null, null, new String[]{"record"}, new String[]{String.valueOf(i)});
            if (!A08.moveToNext()) {
                A08.close();
                A01.close();
                return null;
            }
            byte[] blob = A08.getBlob(0);
            A08.close();
            A01.close();
            return blob;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
