package X;

/* renamed from: X.0an  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08230an implements AbstractC12040hH {
    public final int A00;
    public final AnonymousClass0H7 A01;
    public final String A02;
    public final boolean A03;

    public C08230an(AnonymousClass0H7 r1, String str, int i, boolean z) {
        this.A02 = str;
        this.A00 = i;
        this.A01 = r1;
        this.A03 = z;
    }

    @Override // X.AbstractC12040hH
    public AbstractC12470hy Aes(AnonymousClass0AA r2, AbstractC08070aX r3) {
        return new C07970aN(r2, this, r3);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("ShapePath{name=");
        sb.append(this.A02);
        sb.append(", index=");
        sb.append(this.A00);
        sb.append('}');
        return sb.toString();
    }
}
