package X;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.2h6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC54672h6 extends AbstractC018308n {
    public final Rect A00 = C12980iv.A0J();
    public final Drawable A01;

    public abstract boolean A03(int i, int i2);

    public AbstractC54672h6(Drawable drawable) {
        this.A01 = drawable;
    }

    @Override // X.AbstractC018308n
    public void A00(Canvas canvas, C05480Ps r11, RecyclerView recyclerView) {
        int width;
        int i;
        View childAt;
        canvas.save();
        if (recyclerView.A0d) {
            i = recyclerView.getPaddingLeft();
            width = C12980iv.A08(recyclerView);
            canvas.clipRect(i, recyclerView.getPaddingTop(), width, C12980iv.A07(recyclerView));
        } else {
            width = recyclerView.getWidth();
            i = 0;
        }
        int childCount = recyclerView.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt2 = recyclerView.getChildAt(i2);
            if (i2 == childCount - 1) {
                childAt = null;
            } else {
                childAt = recyclerView.getChildAt(i2 + 1);
            }
            if (A03(recyclerView.A0N.getItemViewType(RecyclerView.A00(childAt2)), recyclerView.A0N.getItemViewType(RecyclerView.A00(childAt)))) {
                Rect rect = this.A00;
                RecyclerView.A03(childAt2, rect);
                int round = rect.bottom + Math.round(childAt2.getTranslationY());
                Drawable drawable = this.A01;
                drawable.setBounds(i, round - drawable.getIntrinsicHeight(), width, round);
                drawable.draw(canvas);
            }
        }
        canvas.restore();
    }

    @Override // X.AbstractC018308n
    public void A01(Rect rect, View view, C05480Ps r7, RecyclerView recyclerView) {
        int A00 = RecyclerView.A00(view);
        if (A03(recyclerView.A0N.getItemViewType(A00), recyclerView.A0N.getItemViewType(A00 + 1))) {
            rect.set(0, 0, 0, this.A01.getIntrinsicHeight());
        } else {
            rect.setEmpty();
        }
    }
}
