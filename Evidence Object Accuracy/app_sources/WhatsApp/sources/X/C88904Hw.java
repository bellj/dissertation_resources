package X;

import com.facebook.msys.mci.DefaultCrypto;
import java.util.Collections;
import java.util.Set;

/* renamed from: X.4Hw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C88904Hw {
    public static final C92324Vl A00;
    public static final C92324Vl A01 = A00(C108844zk.A00, "404,502");
    public static final C92324Vl A02 = A00(C108854zl.A00, 3600);
    public static final C92324Vl A03 = A00(C108864zm.A00, 86400000L);
    public static final C92324Vl A04 = A00(C108884zo.A00, 60000);
    public static final C92324Vl A05 = A00(C108894zp.A00, 61000);
    public static final C92324Vl A06 = A00(C108904zq.A00, 86400000L);
    public static final C92324Vl A07 = A00(C108914zr.A00, "");
    public static final C92324Vl A08 = A00(C108924zs.A00, C12980iv.A0i());
    public static final C92324Vl A09;
    public static final C92324Vl A0A = A00(C108954zv.A00, 10000L);
    public static final C92324Vl A0B = A00(C108964zw.A00, 5000L);
    public static final C92324Vl A0C = A00(C108974zx.A00, 5000L);
    public static final C92324Vl A0D = A00(C108984zy.A00, 60000L);
    public static final C92324Vl A0E = A00(AnonymousClass500.A00, 1800000L);
    public static final C92324Vl A0F = A00(AnonymousClass501.A00, 86400000L);
    public static final C92324Vl A0G = A00(AnonymousClass502.A00, 5000L);
    public static final C92324Vl A0H;
    public static final C92324Vl A0I;
    public static final C92324Vl A0J = A00(C108834zj.A00, Boolean.TRUE);
    public static final C92324Vl A0K = new C92324Vl(C108754zb.A00, "GAv4", "GAv4-SVC");
    public static final C92324Vl A0L = A00(C108874zn.A00, 60L);
    public static final C92324Vl A0M = A00(C108994zz.A00, Double.valueOf(0.5d));
    public static final C92324Vl A0N = new C92324Vl(AnonymousClass504.A00, 2000, 20000);
    public static final C92324Vl A0O = A00(AnonymousClass506.A00, 2000);
    public static final C92324Vl A0P = A00(AnonymousClass507.A00, 100);
    public static final C92324Vl A0Q = new C92324Vl(AnonymousClass508.A00, 1800000L, 120000L);
    public static final C92324Vl A0R = A00(AnonymousClass509.A00, 5000L);
    public static final C92324Vl A0S = A00(C108944zu.A00, 120000L);
    public static final C92324Vl A0T = A00(AnonymousClass505.A00, 7200000L);
    public static final C92324Vl A0U = A00(AnonymousClass50A.A00, 7200000L);
    public static final C92324Vl A0V = A00(AnonymousClass50B.A00, 32400000L);
    public static final C92324Vl A0W = A00(AnonymousClass50C.A00, 20);
    public static final C92324Vl A0X = A00(AnonymousClass50D.A00, 20);
    public static final C92324Vl A0Y = A00(AnonymousClass50E.A00, "http://www.google-analytics.com");
    public static final C92324Vl A0Z = A00(AnonymousClass50F.A00, "https://ssl.google-analytics.com");
    public static final C92324Vl A0a = A00(C108734zZ.A00, "/collect");
    public static final C92324Vl A0b = A00(C108744za.A00, "/batch");
    public static final C92324Vl A0c = A00(C108764zc.A00, 2036);
    public static final C92324Vl A0d = A00(C108774zd.A00, "BATCH_BY_COUNT");
    public static final C92324Vl A0e = A00(C108784ze.A00, "GZIP");
    public static final C92324Vl A0f = A00(C108794zf.A00, 20);
    public static final C92324Vl A0g;
    public static final C92324Vl A0h;
    public static final Set A0i = Collections.synchronizedSet(C12970iu.A12());

    static {
        Boolean bool = Boolean.FALSE;
        A0I = A00(C108724zY.A00, bool);
        Integer valueOf = Integer.valueOf((int) DefaultCrypto.BUFFER_SIZE);
        A0g = A00(C108804zg.A00, valueOf);
        A0h = A00(C108814zh.A00, valueOf);
        A00 = A00(C108824zi.A00, valueOf);
        A09 = A00(C108934zt.A00, bool);
        A0H = A00(AnonymousClass503.A00, bool);
    }

    public static C92324Vl A00(AbstractC115215Qq r1, Object obj) {
        return new C92324Vl(r1, obj, obj);
    }
}
