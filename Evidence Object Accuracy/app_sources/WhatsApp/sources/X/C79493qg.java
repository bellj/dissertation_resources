package X;

import java.nio.ByteBuffer;
import java.util.Arrays;

/* renamed from: X.3qg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79493qg extends C79503qh implements Cloneable {
    public int A00 = 0;
    public long A01 = 0;
    public long A02 = 0;
    public long A03;
    public C79363qT A04;
    public C79353qS A05;
    public C79483qf A06;
    public C79473qe A07;
    public boolean A08;
    public byte[] A09;
    public byte[] A0A;
    public byte[] A0B;
    public int[] A0C;
    public C79463qd[] A0D;

    public C79493qg() {
        if (C79463qd.A00 == null) {
            synchronized (C94684cL.A00) {
                if (C79463qd.A00 == null) {
                    C79463qd.A00 = new C79463qd[0];
                }
            }
        }
        this.A0D = C79463qd.A00;
        byte[] bArr = C88764Ha.A00;
        this.A09 = bArr;
        this.A04 = null;
        this.A0A = bArr;
        this.A06 = null;
        this.A03 = 180000;
        this.A07 = null;
        this.A0B = bArr;
        this.A0C = C88764Ha.A01;
        this.A05 = null;
        this.A08 = false;
        ((C79503qh) this).A00 = null;
        ((AbstractC94974cq) this).A00 = -1;
    }

    @Override // X.C79503qh, X.AbstractC94974cq
    public final int A03() {
        int length;
        int i;
        int A03 = super.A03();
        long j = this.A01;
        if (j != 0) {
            A03 += C95484do.A02(j) + 1;
        }
        boolean equals = "".equals("");
        if (!equals) {
            A03 += C95484do.A00(2);
        }
        C79463qd[] r0 = this.A0D;
        int i2 = 0;
        if (r0 != null && r0.length > 0) {
            int i3 = 0;
            while (true) {
                C79463qd[] r1 = this.A0D;
                if (i3 >= r1.length) {
                    break;
                }
                C79463qd r12 = r1[i3];
                if (r12 != null) {
                    int A032 = r12.A03();
                    ((AbstractC94974cq) r12).A00 = A032;
                    A03 = C79503qh.A00(A032, 1, A03);
                }
                i3++;
            }
        }
        byte[] bArr = this.A09;
        byte[] bArr2 = C88764Ha.A00;
        if (!Arrays.equals(bArr, bArr2)) {
            A03 = C79503qh.A00(bArr.length, 1, A03);
        }
        if (!Arrays.equals(this.A0A, bArr2)) {
            A03 = C79503qh.A00(this.A0A.length, 1, A03);
        }
        C79483qf r13 = this.A06;
        if (r13 != null) {
            int A033 = r13.A03();
            ((AbstractC94974cq) r13).A00 = A033;
            A03 = C79503qh.A00(A033, 1, A03);
        }
        if (!equals) {
            A03 += C95484do.A00(8);
        }
        C79363qT r02 = this.A04;
        if (r02 != null) {
            A03 += AnonymousClass4UO.A05(r02.Ah0(), 1);
        }
        int i4 = this.A00;
        if (i4 != 0) {
            A03 += C72453ed.A07(i4) + 1;
        }
        if (!equals) {
            A03 = A03 + C95484do.A00(13) + C95484do.A00(14);
        }
        long j2 = this.A03;
        if (j2 != 180000) {
            A03 += 1 + C95484do.A02(C72453ed.A0V(j2));
        }
        C79473qe r3 = this.A07;
        if (r3 != null) {
            int A034 = r3.A03();
            ((AbstractC94974cq) r3).A00 = A034;
            A03 = C79503qh.A00(A034, 2, A03);
        }
        long j3 = this.A02;
        if (j3 != 0) {
            A03 += 2 + C95484do.A02(j3);
        }
        byte[] bArr3 = this.A0B;
        if (!Arrays.equals(bArr3, bArr2)) {
            A03 = C79503qh.A00(bArr3.length, 2, A03);
        }
        int[] iArr = this.A0C;
        if (iArr != null && (length = iArr.length) > 0) {
            int i5 = 0;
            do {
                int i6 = iArr[i2];
                if (i6 >= 0) {
                    i = C72453ed.A07(i6);
                } else {
                    i = 10;
                }
                i5 += i;
                i2++;
            } while (i2 < length);
            A03 = A03 + i5 + (length << 1);
        }
        C79353qS r03 = this.A05;
        if (r03 != null) {
            A03 += AnonymousClass4UO.A05(r03.Ah0(), 2);
        }
        if (!equals) {
            A03 += C95484do.A00(24);
        }
        if (this.A08) {
            A03 += 3;
        }
        return !equals ? A03 + C95484do.A00(26) : A03;
    }

    @Override // X.C79503qh, X.AbstractC94974cq
    public final void A05(C95484do r12) {
        long j = this.A01;
        if (j != 0) {
            r12.A05(8);
            r12.A09(j);
        }
        boolean equals = "".equals("");
        if (!equals) {
            r12.A07(2, "");
        }
        C79463qd[] r0 = this.A0D;
        int i = 0;
        if (r0 != null && r0.length > 0) {
            int i2 = 0;
            while (true) {
                C79463qd[] r1 = this.A0D;
                if (i2 >= r1.length) {
                    break;
                }
                C79463qd r13 = r1[i2];
                if (r13 != null) {
                    r12.A0B(r13, 3);
                }
                i2++;
            }
        }
        byte[] bArr = this.A09;
        byte[] bArr2 = C88764Ha.A00;
        if (!Arrays.equals(bArr, bArr2)) {
            r12.A08(4, bArr);
        }
        if (!Arrays.equals(this.A0A, bArr2)) {
            r12.A08(6, this.A0A);
        }
        C79483qf r14 = this.A06;
        if (r14 != null) {
            r12.A0B(r14, 7);
        }
        if (!equals) {
            r12.A07(8, "");
        }
        C79363qT r15 = this.A04;
        if (r15 != null) {
            r12.A0A(r15, 9);
        }
        int i3 = this.A00;
        if (i3 != 0) {
            r12.A05(88);
            r12.A06(i3);
        }
        if (!equals) {
            r12.A07(13, "");
            r12.A07(14, "");
        }
        long j2 = this.A03;
        if (j2 != 180000) {
            r12.A05(120);
            r12.A09(C72453ed.A0V(j2));
        }
        C79473qe r16 = this.A07;
        if (r16 != null) {
            r12.A0B(r16, 16);
        }
        long j3 = this.A02;
        if (j3 != 0) {
            r12.A06(136);
            r12.A09(j3);
        }
        byte[] bArr3 = this.A0B;
        if (!Arrays.equals(bArr3, bArr2)) {
            r12.A08(18, bArr3);
        }
        int[] iArr = this.A0C;
        if (iArr != null && iArr.length > 0) {
            while (true) {
                int[] iArr2 = this.A0C;
                if (i >= iArr2.length) {
                    break;
                }
                int i4 = iArr2[i];
                r12.A06(160);
                if (i4 >= 0) {
                    r12.A06(i4);
                } else {
                    r12.A09((long) i4);
                }
                i++;
            }
        }
        C79353qS r17 = this.A05;
        if (r17 != null) {
            r12.A0A(r17, 23);
        }
        if (!equals) {
            r12.A07(24, "");
        }
        boolean z = this.A08;
        if (z) {
            r12.A06(200);
            byte b = z ? (byte) 1 : 0;
            ByteBuffer byteBuffer = r12.A02;
            if (byteBuffer.hasRemaining()) {
                byteBuffer.put(b);
            } else {
                throw new C867548r(byteBuffer.position(), byteBuffer.limit());
            }
        }
        if (!equals) {
            r12.A07(26, "");
        }
        super.A05(r12);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v2, resolved type: X.3qd[] */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // X.AbstractC94974cq, java.lang.Object
    public final /* synthetic */ Object clone() {
        int length;
        try {
            C79493qg r2 = (C79493qg) super.A06();
            C79463qd[] r0 = this.A0D;
            if (r0 != null && (length = r0.length) > 0) {
                r2.A0D = new C79463qd[length];
                int i = 0;
                while (true) {
                    C79463qd[] r3 = this.A0D;
                    if (i >= r3.length) {
                        break;
                    }
                    if (r3[i] != null) {
                        r2.A0D[i] = r3[i].clone();
                    }
                    i++;
                }
            }
            C79363qT r02 = this.A04;
            if (r02 != null) {
                r2.A04 = r02;
            }
            C79483qf r03 = this.A06;
            if (r03 != null) {
                r2.A06 = (C79483qf) r03.clone();
            }
            C79473qe r04 = this.A07;
            if (r04 != null) {
                r2.A07 = (C79473qe) r04.clone();
            }
            int[] iArr = this.A0C;
            if (iArr != null && iArr.length > 0) {
                r2.A0C = (int[]) iArr.clone();
            }
            C79353qS r05 = this.A05;
            if (r05 != null) {
                r2.A05 = r05;
            }
            return r2;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0042, code lost:
        if (r0 != null) goto L_0x0044;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x009b, code lost:
        if (r1.length != 0) goto L_0x0044;
     */
    @Override // java.lang.Object
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean equals(java.lang.Object r8) {
        /*
            r7 = this;
            r6 = 1
            if (r8 == r7) goto L_0x00ca
            boolean r0 = r8 instanceof X.C79493qg
            r5 = 0
            if (r0 == 0) goto L_0x0044
            X.3qg r8 = (X.C79493qg) r8
            long r3 = r7.A01
            long r1 = r8.A01
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x0044
            long r3 = r7.A02
            long r1 = r8.A02
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x0044
            java.lang.String r0 = ""
            boolean r0 = r0.equals(r0)
            if (r0 == 0) goto L_0x0044
            int r1 = r7.A00
            int r0 = r8.A00
            if (r1 != r0) goto L_0x0044
            X.3qd[] r1 = r7.A0D
            X.3qd[] r0 = r8.A0D
            boolean r0 = X.C94684cL.A00(r1, r0)
            if (r0 == 0) goto L_0x0044
            byte[] r1 = r7.A09
            byte[] r0 = r8.A09
            boolean r0 = java.util.Arrays.equals(r1, r0)
            if (r0 == 0) goto L_0x0044
            X.3qT r1 = r7.A04
            X.3qT r0 = r8.A04
            if (r1 != 0) goto L_0x0045
            if (r0 == 0) goto L_0x004c
        L_0x0044:
            return r5
        L_0x0045:
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x004c
            return r5
        L_0x004c:
            byte[] r1 = r7.A0A
            byte[] r0 = r8.A0A
            boolean r0 = java.util.Arrays.equals(r1, r0)
            if (r0 == 0) goto L_0x0044
            X.3qf r1 = r7.A06
            X.3qf r0 = r8.A06
            if (r1 != 0) goto L_0x005f
            if (r0 == 0) goto L_0x0066
            return r5
        L_0x005f:
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0066
            return r5
        L_0x0066:
            long r3 = r7.A03
            long r1 = r8.A03
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x0044
            X.3qe r1 = r7.A07
            X.3qe r0 = r8.A07
            if (r1 != 0) goto L_0x0077
            if (r0 == 0) goto L_0x007e
            return r5
        L_0x0077:
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x007e
            return r5
        L_0x007e:
            byte[] r1 = r7.A0B
            byte[] r0 = r8.A0B
            boolean r0 = java.util.Arrays.equals(r1, r0)
            if (r0 == 0) goto L_0x0044
            int[] r2 = r7.A0C
            int[] r1 = r8.A0C
            if (r2 == 0) goto L_0x0098
            int r0 = r2.length
            if (r0 == 0) goto L_0x0098
            boolean r0 = java.util.Arrays.equals(r2, r1)
            if (r0 != 0) goto L_0x009d
            return r5
        L_0x0098:
            if (r1 == 0) goto L_0x009d
            int r0 = r1.length
            if (r0 != 0) goto L_0x0044
        L_0x009d:
            X.3qS r1 = r7.A05
            X.3qS r0 = r8.A05
            if (r1 != 0) goto L_0x00a6
            if (r0 == 0) goto L_0x00ad
            return r5
        L_0x00a6:
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x00ad
            return r5
        L_0x00ad:
            boolean r1 = r7.A08
            boolean r0 = r8.A08
            if (r1 != r0) goto L_0x0044
            X.5BY r1 = r7.A00
            if (r1 == 0) goto L_0x00c2
            int r0 = r1.A00
            if (r0 == 0) goto L_0x00c2
            X.5BY r0 = r8.A00
            boolean r0 = r1.equals(r0)
            return r0
        L_0x00c2:
            X.5BY r0 = r8.A00
            if (r0 == 0) goto L_0x00ca
            int r0 = r0.A00
            if (r0 != 0) goto L_0x0044
        L_0x00ca:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C79493qg.equals(java.lang.Object):boolean");
    }

    @Override // java.lang.Object
    public final int hashCode() {
        int length;
        int i;
        int i2 = 0;
        int hashCode = "".hashCode();
        int i3 = 1237;
        int A0A = ((((((((C72453ed.A0A(C72453ed.A0A((C79493qg.class.getName().hashCode() + 527) * 31, this.A01), this.A02) * 31) + hashCode) * 31) + this.A00) * 31) + hashCode) * 31 * 31) + 1237) * 31;
        C79463qd[] r4 = this.A0D;
        if (r4 == null) {
            length = 0;
        } else {
            length = r4.length;
        }
        int i4 = 0;
        for (int i5 = 0; i5 < length; i5++) {
            C79463qd r1 = r4[i5];
            if (r1 != null) {
                i4 = C12990iw.A08(r1, i4 * 31);
            }
        }
        int hashCode2 = ((A0A + i4) * 31) + Arrays.hashCode(this.A09);
        long j = this.A03;
        int A0D = ((((((((((((((((((((hashCode2 * 31) + C72453ed.A0D(this.A04)) * 31) + Arrays.hashCode(this.A0A)) * 31) + hashCode) * 31) + hashCode) * 31) + C72453ed.A0D(this.A06)) * 31) + hashCode) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + C72453ed.A0D(this.A07)) * 31) + Arrays.hashCode(this.A0B)) * 31) + hashCode) * 31 * 31;
        int[] iArr = this.A0C;
        if (iArr == null || iArr.length == 0) {
            i = 0;
        } else {
            i = Arrays.hashCode(iArr);
        }
        int A0D2 = (((A0D + i) * 31 * 31) + C72453ed.A0D(this.A05)) * 31;
        if (this.A08) {
            i3 = 1231;
        }
        int i6 = (A0D2 + i3) * 31;
        AnonymousClass5BY r12 = ((C79503qh) this).A00;
        if (!(r12 == null || r12.A00 == 0)) {
            i2 = r12.hashCode();
        }
        return i6 + i2;
    }
}
