package X;

import android.os.SystemClock;
import com.whatsapp.jid.UserJid;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/* renamed from: X.1BD  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1BD {
    public static final Map A0K = new HashMap();
    public C458223h A00;
    public C458423j A01;
    public final AbstractC22060yS A02;
    public final C15550nR A03;
    public final C14830m7 A04;
    public final AnonymousClass10U A05;
    public final C242714w A06;
    public final C18470sV A07;
    public final C22180yf A08;
    public final C14850m9 A09;
    public final C16120oU A0A;
    public final C17040qA A0B;
    public final C21230x5 A0C;
    public final AbstractC14440lR A0D;
    public final LinkedHashMap A0E = new C457923e(this);
    public final Map A0F;
    public final Map A0G;
    public final Random A0H = new Random();
    public final boolean A0I;
    public final boolean A0J;

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x004d, code lost:
        if (r11.A07(1267) == false) goto L_0x004f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass1BD(X.C16210od r4, X.C15550nR r5, X.C14830m7 r6, X.AnonymousClass10U r7, X.C242714w r8, X.C18470sV r9, X.C22180yf r10, X.C14850m9 r11, X.C16120oU r12, X.C17040qA r13, X.C21230x5 r14, X.AbstractC14440lR r15) {
        /*
            r3 = this;
            r3.<init>()
            java.util.Random r0 = new java.util.Random
            r0.<init>()
            r3.A0H = r0
            X.23e r0 = new X.23e
            r0.<init>(r3)
            r3.A0E = r0
            X.23f r2 = new X.23f
            r2.<init>(r3)
            r3.A02 = r2
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            r3.A0G = r0
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            r3.A0F = r0
            r3.A04 = r6
            r3.A09 = r11
            r3.A0D = r15
            r3.A0A = r12
            r3.A07 = r9
            r3.A03 = r5
            r3.A08 = r10
            r3.A05 = r7
            r3.A0B = r13
            r3.A06 = r8
            r3.A0C = r14
            r0 = 815(0x32f, float:1.142E-42)
            boolean r0 = r11.A07(r0)
            r3.A0I = r0
            if (r0 == 0) goto L_0x004f
            r0 = 1267(0x4f3, float:1.775E-42)
            boolean r1 = r11.A07(r0)
            r0 = 1
            if (r1 != 0) goto L_0x0050
        L_0x004f:
            r0 = 0
        L_0x0050:
            r3.A0J = r0
            r4.A03(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1BD.<init>(X.0od, X.0nR, X.0m7, X.10U, X.14w, X.0sV, X.0yf, X.0m9, X.0oU, X.0qA, X.0x5, X.0lR):void");
    }

    public static int A00(UserJid userJid, List list) {
        for (int i = 0; i < list.size(); i++) {
            if (userJid.equals(((AnonymousClass1V2) list.get(i)).A0A)) {
                return i;
            }
        }
        return -1;
    }

    public static UserJid A01(AbstractC15340mz r1) {
        if (r1.A0z.A02) {
            return C29831Uv.A00;
        }
        return UserJid.of(r1.A0B());
    }

    public static String A02(Map map) {
        String str;
        if (map == null || map.size() == 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        boolean z = true;
        for (Map.Entry entry : map.entrySet()) {
            StringBuilder sb2 = new StringBuilder();
            if (z) {
                str = "";
            } else {
                str = ",";
            }
            sb2.append(str);
            sb2.append((String) entry.getKey());
            sb2.append(":");
            sb2.append(entry.getValue());
            sb.append(sb2.toString());
            z = false;
        }
        return sb.toString();
    }

    public void A03() {
        C458223h r3 = this.A00;
        if (r3 != null && r3.A07) {
            C458523k r2 = new C458523k();
            r2.A02 = Long.valueOf(r3.A05);
            r2.A04 = Long.valueOf(SystemClock.elapsedRealtime() - r3.A06);
            r2.A05 = Long.valueOf((long) r3.A03);
            r2.A03 = Long.valueOf((long) r3.A02);
            int i = r3.A00;
            if (i != 0) {
                r2.A01 = Integer.valueOf(i);
            }
            int i2 = r3.A01;
            if (i2 != 0) {
                r2.A00 = Integer.valueOf(i2);
            }
            this.A0A.A06(r2);
            this.A00 = null;
        }
    }

    public void A04(UserJid userJid, Integer num, Integer num2, String str, List list, List list2, List list3, Map map) {
        int intValue;
        int intValue2;
        if (this.A00 != null) {
            boolean z = true;
            if (!(num == null || (intValue2 = num.intValue()) == 4 || intValue2 == 1 || intValue2 == 3 || intValue2 == 2)) {
                z = false;
            }
            if (num2 == null) {
                if (z && userJid != C29831Uv.A00) {
                    Iterator it = Arrays.asList(list, list2, list3).iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            intValue = -1;
                            break;
                        }
                        intValue = A00(userJid, (List) it.next());
                        if (intValue >= 0) {
                            break;
                        }
                    }
                } else {
                    intValue = 0;
                }
            } else {
                intValue = num2.intValue();
            }
            this.A01 = new C458423j(this.A04, this.A08, userJid, num, str, list, list2, list3, map, this.A0E, intValue, this.A00.A05, this.A0H.nextLong());
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0076  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A05(X.AbstractC15340mz r5, int r6, long r7, boolean r9, boolean r10) {
        /*
            r4 = this;
            X.23g r3 = new X.23g
            r3.<init>()
            X.23h r0 = r4.A00
            if (r0 != 0) goto L_0x009c
            r0 = 0
        L_0x000b:
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            r3.A08 = r0
            X.0yf r0 = r4.A08
            int r0 = X.C20870wS.A01(r0, r5)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r3.A03 = r0
            java.lang.Long r0 = java.lang.Long.valueOf(r7)
            r3.A07 = r0
            int r2 = r5.A08
            r1 = 3
            if (r2 == r1) goto L_0x0036
            r1 = 4
            if (r2 == r1) goto L_0x009a
            r0 = 5
            if (r2 == r0) goto L_0x0098
            r0 = 10
            if (r2 == r0) goto L_0x0098
            r0 = 11
            if (r2 == r0) goto L_0x0098
        L_0x0036:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r1)
            r3.A05 = r0
            java.lang.Integer r0 = java.lang.Integer.valueOf(r6)
            r3.A06 = r0
            boolean r0 = r4.A0I
            if (r0 == 0) goto L_0x0052
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r9)
            r3.A00 = r0
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r10)
            r3.A01 = r0
        L_0x0052:
            boolean r0 = r4.A0J
            if (r0 == 0) goto L_0x0088
            X.0sV r0 = r4.A07
            X.0xL r2 = r0.A03
            java.lang.String r1 = "status_distribution"
            r0 = 0
            int r2 = r2.A00(r1, r0)
            r1 = 3
            if (r2 == 0) goto L_0x006c
            r0 = 2
            r1 = 1
            if (r2 == r1) goto L_0x0093
            if (r2 != r0) goto L_0x006c
            r1 = 4
        L_0x006c:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r1)
        L_0x0070:
            r3.A02 = r0
            X.1ce r0 = r5.A0K
            if (r0 == 0) goto L_0x0088
            int r2 = r0.A00
            r1 = 3
            if (r2 == 0) goto L_0x0082
            r0 = 2
            r1 = 1
            if (r2 == r1) goto L_0x008e
            if (r2 != r0) goto L_0x0082
            r1 = 4
        L_0x0082:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r1)
        L_0x0086:
            r3.A04 = r0
        L_0x0088:
            X.0oU r0 = r4.A0A
            r0.A06(r3)
            return
        L_0x008e:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            goto L_0x0086
        L_0x0093:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            goto L_0x0070
        L_0x0098:
            r1 = 2
            goto L_0x0036
        L_0x009a:
            r1 = 1
            goto L_0x0036
        L_0x009c:
            long r0 = r0.A05
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1BD.A05(X.0mz, int, long, boolean, boolean):void");
    }

    public void A06(Boolean bool) {
        this.A00 = new C458223h(this.A0H.nextLong(), bool.booleanValue());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000b, code lost:
        if (r0 == false) goto L_0x000d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A07(java.util.Map r4, int r5) {
        /*
            r3 = this;
            X.23h r1 = r3.A00
            if (r1 == 0) goto L_0x000d
            boolean r0 = r1.A04
            if (r0 != 0) goto L_0x000d
            boolean r0 = r1.A07
            r1 = 1
            if (r0 != 0) goto L_0x000e
        L_0x000d:
            r1 = 0
        L_0x000e:
            java.lang.String r0 = "Report tab open only once per session"
            X.AnonymousClass009.A0A(r0, r1)
            X.23i r2 = new X.23i
            r2.<init>()
            X.23h r0 = r3.A00
            if (r0 != 0) goto L_0x003e
            r0 = 0
        L_0x001e:
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            r2.A01 = r0
            long r0 = (long) r5
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            r2.A00 = r0
            java.lang.String r0 = A02(r4)
            r2.A02 = r0
            X.0oU r0 = r3.A0A
            r0.A06(r2)
            X.23h r1 = r3.A00
            if (r1 == 0) goto L_0x003d
            r0 = 1
            r1.A04 = r0
        L_0x003d:
            return
        L_0x003e:
            long r0 = r0.A05
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1BD.A07(java.util.Map, int):void");
    }
}
