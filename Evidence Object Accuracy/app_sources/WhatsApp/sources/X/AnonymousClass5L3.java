package X;

/* renamed from: X.5L3  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5L3 extends C10710f4 implements AbstractC02760Dw {
    public final boolean A00 = A0m();

    @Override // X.C10710f4
    public boolean A0c() {
        return true;
    }

    public AnonymousClass5L3() {
        A0Y(null);
    }

    @Override // X.C10710f4
    public boolean A0b() {
        return this.A00;
    }

    public final boolean A0m() {
        AbstractC114145Kj r1;
        AbstractC114145Kj r2;
        AbstractC02790Dz A0L = A0L();
        C10710f4 r12 = null;
        if (!(A0L instanceof C114295Ky) || (r2 = (AbstractC114145Kj) A0L) == null || (r12 = r2.A00) != null) {
            if (r12 != null) {
                while (!r12.A0b()) {
                    AbstractC02790Dz A0L2 = r12.A0L();
                    if ((A0L2 instanceof C114295Ky) && (r1 = (AbstractC114145Kj) A0L2) != null) {
                        r12 = r1.A00;
                        if (r12 == null) {
                            throw C16700pc.A06("job");
                        }
                    }
                }
                return true;
            }
            return false;
        }
        throw C16700pc.A06("job");
    }
}
