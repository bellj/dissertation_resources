package X;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.view.View;
import android.widget.ImageView;
import com.whatsapp.R;
import java.lang.ref.WeakReference;

/* renamed from: X.1oc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38731oc implements AbstractC38741od {
    public final Drawable A00;
    public final Drawable A01;

    public C38731oc(Drawable drawable, Drawable drawable2) {
        this.A00 = drawable2;
        this.A01 = drawable;
    }

    public static final boolean A00(AnonymousClass53K r3) {
        View view;
        WeakReference weakReference = r3.A07;
        return (weakReference == null || (view = (View) weakReference.get()) == null || view.getTag(R.id.loaded_image_id) == null || !view.getTag(R.id.loaded_image_id).equals(r3.A06)) ? false : true;
    }

    @Override // X.AbstractC38741od
    public /* bridge */ /* synthetic */ void AMK(AnonymousClass5XB r3) {
        ImageView imageView;
        AnonymousClass53K r32 = (AnonymousClass53K) r3;
        WeakReference weakReference = r32.A07;
        if (weakReference != null && (imageView = (ImageView) weakReference.get()) != null && A00(r32)) {
            Drawable drawable = r32.A03;
            if (drawable == null) {
                drawable = this.A01;
            }
            imageView.setImageDrawable(drawable);
        }
    }

    @Override // X.AbstractC38741od
    public /* bridge */ /* synthetic */ void ARx(AnonymousClass5XB r3) {
        ImageView imageView;
        AnonymousClass53K r32 = (AnonymousClass53K) r3;
        WeakReference weakReference = r32.A07;
        if (!(weakReference == null || (imageView = (ImageView) weakReference.get()) == null || !A00(r32))) {
            Drawable drawable = r32.A02;
            if (drawable == null) {
                drawable = this.A00;
            }
            imageView.setImageDrawable(drawable);
        }
        AnonymousClass5WK r0 = r32.A04;
        if (r0 != null) {
            r0.ARw();
        }
    }

    @Override // X.AbstractC38741od
    public void AS3(AnonymousClass5XB r4) {
        View view;
        AnonymousClass53K r42 = (AnonymousClass53K) r4;
        WeakReference weakReference = r42.A07;
        if (weakReference != null && (view = (View) weakReference.get()) != null) {
            view.setTag(R.id.loaded_image_id, r42.A06);
        }
    }

    @Override // X.AbstractC38741od
    public /* bridge */ /* synthetic */ void AS7(Bitmap bitmap, AnonymousClass5XB r7, boolean z) {
        ImageView imageView;
        Drawable drawable;
        AnonymousClass53K r72 = (AnonymousClass53K) r7;
        WeakReference weakReference = r72.A07;
        if (weakReference != null && (imageView = (ImageView) weakReference.get()) != null && A00(r72)) {
            if ((imageView.getDrawable() == null || (imageView.getDrawable() instanceof ColorDrawable)) && !z) {
                Drawable[] drawableArr = new Drawable[2];
                if (imageView.getDrawable() == null) {
                    drawable = new ColorDrawable(0);
                } else {
                    drawable = imageView.getDrawable();
                }
                drawableArr[0] = drawable;
                drawableArr[1] = new BitmapDrawable(imageView.getResources(), bitmap);
                TransitionDrawable transitionDrawable = new TransitionDrawable(drawableArr);
                transitionDrawable.setCrossFadeEnabled(true);
                transitionDrawable.startTransition(200);
                imageView.setImageDrawable(transitionDrawable);
            } else {
                imageView.setImageBitmap(bitmap);
            }
            AnonymousClass5WK r0 = r72.A04;
            if (r0 != null) {
                r0.AXT();
            }
        }
    }
}
