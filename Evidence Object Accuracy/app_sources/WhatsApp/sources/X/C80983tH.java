package X;

import java.util.Map;

/* renamed from: X.3tH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C80983tH extends AnonymousClass5DL {
    public final /* synthetic */ AnonymousClass5I4 this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C80983tH(AnonymousClass5I4 r2) {
        super(r2, null);
        this.this$0 = r2;
    }

    @Override // X.AnonymousClass5DL
    public Map.Entry getOutput(int i) {
        return new C80903t9(this.this$0, i);
    }
}
