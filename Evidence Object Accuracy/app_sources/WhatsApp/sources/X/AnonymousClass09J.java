package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

/* renamed from: X.09J  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass09J extends AnimatorListenerAdapter {
    public boolean A00 = false;
    public final /* synthetic */ Animator A01;
    public final /* synthetic */ C08790br A02;
    public final /* synthetic */ C14260l7 A03;
    public final /* synthetic */ C14230l4 A04;
    public final /* synthetic */ AbstractC14200l1 A05;

    public AnonymousClass09J(Animator animator, C08790br r3, C14260l7 r4, C14230l4 r5, AbstractC14200l1 r6) {
        this.A02 = r3;
        this.A05 = r6;
        this.A01 = animator;
        this.A04 = r5;
        this.A03 = r4;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
        this.A00 = true;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        AbstractC14200l1 r5 = this.A05;
        if (r5 != null) {
            C14210l2 r4 = new C14210l2();
            r4.A05(this.A01, 0);
            C14230l4 r2 = this.A04;
            r4.A05(Boolean.valueOf(!this.A00), 1);
            r4.A05(this.A03, 2);
            C14250l6.A00(r2, r4.A03(), r5);
            this.A00 = false;
        }
    }
}
