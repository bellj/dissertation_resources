package X;

import com.whatsapp.util.Log;
import java.math.BigDecimal;

/* renamed from: X.5zk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130645zk {
    public final AnonymousClass619 A00;
    public final C127325uJ A01;
    public final C130575zd A02;
    public final C130575zd A03;
    public final String A04;

    public C130645zk(AnonymousClass619 r1, C127325uJ r2, C130575zd r3, C130575zd r4, String str) {
        this.A04 = str;
        this.A02 = r3;
        this.A03 = r4;
        this.A00 = r1;
        this.A01 = r2;
    }

    public static C130645zk A00(AnonymousClass102 r10, AnonymousClass1V8 r11) {
        C130575zd r7;
        C130575zd r8;
        AnonymousClass619 r5;
        C127325uJ r6;
        try {
            String A0I = r11.A0I("country_alpha2", null);
            AnonymousClass1V8 A0E = r11.A0E("north_east_boundary");
            AnonymousClass1V8 A0E2 = r11.A0E("south_west_boundary");
            if (A0E == null) {
                r7 = null;
            } else {
                r7 = C130575zd.A00(A0E);
            }
            if (A0E2 == null) {
                r8 = null;
            } else {
                r8 = C130575zd.A00(A0E2);
            }
            AnonymousClass1V8 A0E3 = r11.A0E("digital_currency_description");
            if (A0E3 == null) {
                r5 = null;
            } else {
                r5 = AnonymousClass619.A00(A0E3);
            }
            AnonymousClass1V8 A0E4 = r11.A0E("quote");
            if (A0E4 == null) {
                r6 = null;
            } else {
                A0E4.A0H("id");
                A0E4.A08("expiry-ts", 0);
                r6 = new C127325uJ(r10.A02(A0E4.A0H("source-iso-code")), r10.A02(A0E4.A0H("target-iso-code")), new BigDecimal(A0E4.A0H("exchange-rate")));
            }
            return new C130645zk(r5, r6, r7, r8, A0I);
        } catch (AnonymousClass1V9 unused) {
            Log.e("PAY: NoviHomeCountryInfo/fromProtocolTreeNode can't parse the node");
            return null;
        }
    }
}
