package X;

import android.app.Activity;
import android.content.Intent;
import android.text.SpannableStringBuilder;
import android.view.View;
import java.io.Serializable;
import java.util.AbstractList;
import org.npci.commonlibrary.NPCIFragment;

/* renamed from: X.5Zm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C117325Zm {
    public static int A00(int i) {
        if (i == 1) {
            return 90;
        }
        if (i != 2) {
            return i != 3 ? 0 : 270;
        }
        return 180;
    }

    public static View A01(AbstractList abstractList, int i) {
        return (View) abstractList.get(i);
    }

    public static AbstractC136546My A02(AbstractList abstractList, int i) {
        return (AbstractC136546My) abstractList.get(i);
    }

    public static void A03(Intent intent, Serializable serializable, NPCIFragment nPCIFragment) {
        intent.putExtra("credBlocks", serializable);
        ((Activity) nPCIFragment.A01).setResult(250, intent);
        ((Activity) nPCIFragment.A01).finish();
    }

    public static void A04(SpannableStringBuilder spannableStringBuilder, Object obj, String str, String str2) {
        int length = str.length();
        spannableStringBuilder.setSpan(obj, length - str2.length(), length, 33);
    }

    public static void A05(C17220qS r7, AbstractC21730xt r8, AnonymousClass1V8 r9, String str) {
        r7.A09(r8, r9, str, 204, 0);
    }

    public static void A06(C17220qS r7, AbstractC21730xt r8, AnonymousClass1V8 r9, String str) {
        r7.A09(r8, r9, str, 204, 0);
    }

    public static void A07(AnonymousClass3FW r2) {
        r2.A01("product_flow", "p2m");
    }

    public static void A08(String str, String str2, String str3, StringBuilder sb) {
        sb.append(str);
        sb.append(str2);
        sb.append(str3);
    }

    public static void A09(StringBuilder sb) {
        sb.append(" ");
        sb.append("•");
        sb.append("•");
    }
}
