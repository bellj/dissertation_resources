package X;

import java.io.Serializable;

/* renamed from: X.5BU  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5BU implements Serializable {
    public final Object value;

    public static final Throwable A00(Object obj) {
        if (obj instanceof AnonymousClass5BR) {
            return ((AnonymousClass5BR) obj).exception;
        }
        return null;
    }

    public static final boolean A01(Object obj) {
        return obj instanceof AnonymousClass5BR;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        return (obj instanceof AnonymousClass5BU) && C16700pc.A0O(this.value, ((AnonymousClass5BU) obj).value);
    }

    @Override // java.lang.Object
    public int hashCode() {
        return C72453ed.A0D(this.value);
    }

    @Override // java.lang.Object
    public String toString() {
        Object obj = this.value;
        if (obj instanceof AnonymousClass5BR) {
            return obj.toString();
        }
        StringBuilder A0k = C12960it.A0k("Success(");
        A0k.append(obj);
        return C12970iu.A0u(A0k);
    }
}
