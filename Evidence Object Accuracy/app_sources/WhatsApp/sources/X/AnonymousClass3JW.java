package X;

import android.graphics.Color;
import android.widget.ImageView;
import java.text.NumberFormat;
import java.text.ParseException;

/* renamed from: X.3JW  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3JW {
    public static final ThreadLocal A00 = new AnonymousClass5HK("#dp");
    public static final ThreadLocal A01 = new AnonymousClass5HK("#%");
    public static final ThreadLocal A02 = new AnonymousClass5HK("#px");
    public static final ThreadLocal A03 = new AnonymousClass5HK("#sp");

    public static float A00(String str) {
        try {
            return A04(str, A01) * 100.0f;
        } catch (ParseException e) {
            throw new AnonymousClass491(C12960it.A0d(str, C12960it.A0k("can't parse pixel value: ")), e);
        }
    }

    public static float A01(String str) {
        try {
            if (str.endsWith("px")) {
                return A04(str, A02);
            }
            if (str.endsWith("sp")) {
                return (float) Math.round(A04(str, A03) * C65093Ic.A00().A00.getResources().getDisplayMetrics().scaledDensity);
            }
            if (str.endsWith("%")) {
                return A00(str);
            }
            return (float) Math.round(A04(str, A00) * (((float) C65093Ic.A00().A00.getResources().getDisplayMetrics().densityDpi) / 160.0f));
        } catch (ParseException e) {
            throw new AnonymousClass491(C12960it.A0d(str, C12960it.A0k("can't parse pixel value: ")), e);
        }
    }

    public static float A02(String str) {
        if (str != null) {
            return A01(str);
        }
        return 24.0f;
    }

    public static float A03(String str) {
        try {
            return A04(str, A03);
        } catch (ParseException e) {
            throw new AnonymousClass491(C12960it.A0d(str, C12960it.A0k("can't parse scaled pixel value: ")), e);
        }
    }

    public static float A04(String str, ThreadLocal threadLocal) {
        return ((NumberFormat) threadLocal.get()).parse(str).floatValue();
    }

    public static int A05(String str) {
        try {
            return Color.parseColor(str);
        } catch (IllegalArgumentException e) {
            throw new AnonymousClass491(C12960it.A0d(str, C12960it.A0k("can't parse color value: ")), e);
        }
    }

    public static int A06(String str) {
        switch (str.hashCode()) {
            case -1078030475:
                if (str.equals("medium")) {
                    return 2;
                }
                break;
            case 3154575:
                if (str.equals("full")) {
                    return 0;
                }
                break;
            case 3327612:
                if (str.equals("long")) {
                    return 1;
                }
                break;
            case 109413500:
                if (str.equals("short")) {
                    return 3;
                }
                break;
        }
        throw new AnonymousClass491(C12960it.A0d(str, C12960it.A0k("Can't parse unknown datetime format: ")));
    }

    public static int A07(String str) {
        switch (str.hashCode()) {
            case -1364013995:
                if (str.equals("center")) {
                    return 1;
                }
                break;
            case 100571:
                if (str.equals("end")) {
                    return 8388613;
                }
                break;
            case 109757538:
                if (str.equals("start")) {
                    return 8388611;
                }
                break;
        }
        throw new AnonymousClass491(C12960it.A0d(str, C12960it.A0k("can't parse unknown textAlign: ")));
    }

    public static int A08(String str) {
        switch (str.hashCode()) {
            case -2141169668:
                if (str.equals("cap_words")) {
                    return 139265;
                }
                break;
            case -1831299680:
                if (str.equals("cap_letters")) {
                    return 135169;
                }
                break;
            case -1413853096:
                if (str.equals("amount")) {
                    return 12290;
                }
                break;
            case -1034364087:
                if (str.equals("number")) {
                    return 2;
                }
                break;
            case 3076014:
                if (str.equals("date")) {
                    return 20;
                }
                break;
            case 3556653:
                if (str.equals("text")) {
                    return 131073;
                }
                break;
            case 96619420:
                if (str.equals("email")) {
                    return 33;
                }
                break;
            case 106642798:
                if (str.equals("phone")) {
                    return 3;
                }
                break;
            case 260133443:
                if (str.equals("cap_sentences")) {
                    return 180225;
                }
                break;
            case 1216389502:
                if (str.equals("passcode")) {
                    return 18;
                }
                break;
            case 1216985755:
                if (str.equals("password")) {
                    return 129;
                }
                break;
        }
        throw new AnonymousClass491(C12960it.A0d(str, C12960it.A0k("can't parse unknown inputType: ")));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0027, code lost:
        if (r2.equals("bold") == false) goto L_0x0008;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A09(java.lang.String r2) {
        /*
            int r0 = r2.hashCode()
            r1 = 3
            switch(r0) {
                case -1178781136: goto L_0x0046;
                case -1039745817: goto L_0x003c;
                case 3029637: goto L_0x0021;
                case 1223860979: goto L_0x002a;
                case 1734741290: goto L_0x0018;
                default: goto L_0x0008;
            }
        L_0x0008:
            java.lang.String r0 = "can't parse unknown typeface: "
            java.lang.StringBuilder r0 = X.C12960it.A0k(r0)
            java.lang.String r1 = X.C12960it.A0d(r2, r0)
            X.491 r0 = new X.491
            r0.<init>(r1)
            throw r0
        L_0x0018:
            java.lang.String r0 = "bold_italic"
            boolean r0 = r2.equals(r0)
            if (r0 != 0) goto L_0x003b
            goto L_0x0008
        L_0x0021:
            java.lang.String r0 = "bold"
            boolean r0 = r2.equals(r0)
            if (r0 != 0) goto L_0x003a
            goto L_0x0008
        L_0x002a:
            java.lang.String r0 = "semibold"
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x0008
            java.lang.String r1 = "text_style_ignored"
            java.lang.String r0 = "semibold is not supported, defaulting to bold"
            X.C28691Op.A00(r1, r0)
        L_0x003a:
            r1 = 1
        L_0x003b:
            return r1
        L_0x003c:
            java.lang.String r0 = "normal"
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x0008
            r1 = 0
            return r1
        L_0x0046:
            java.lang.String r0 = "italic"
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x0008
            r1 = 2
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3JW.A09(java.lang.String):int");
    }

    public static ImageView.ScaleType A0A(String str) {
        switch (str.hashCode()) {
            case -1881872635:
                if (str.equals("stretch")) {
                    return ImageView.ScaleType.FIT_XY;
                }
                break;
            case 94852023:
                if (str.equals("cover")) {
                    return ImageView.ScaleType.CENTER_CROP;
                }
                break;
            case 951526612:
                if (str.equals("contain")) {
                    return ImageView.ScaleType.FIT_CENTER;
                }
                break;
        }
        throw new AnonymousClass491(C12960it.A0d(str, C12960it.A0k("can't parse unknown scaleType: ")));
    }

    public static C64803Gy A0B(String str) {
        float A012;
        AnonymousClass4AC r0;
        if ("auto".equalsIgnoreCase(str)) {
            return C64803Gy.A02;
        }
        if (str.endsWith("%")) {
            A012 = Float.parseFloat(str.substring(0, str.length() - 1));
            r0 = AnonymousClass4AC.PERCENT;
        } else {
            A012 = A01(str);
            r0 = AnonymousClass4AC.PIXEL;
        }
        return new C64803Gy(r0, A012);
    }
}
