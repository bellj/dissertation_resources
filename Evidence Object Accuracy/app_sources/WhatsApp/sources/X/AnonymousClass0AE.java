package X;

import android.hardware.fingerprint.FingerprintManager;

/* renamed from: X.0AE  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0AE extends FingerprintManager.AuthenticationCallback {
    public final /* synthetic */ AnonymousClass04A A00;

    public AnonymousClass0AE(AnonymousClass04A r1) {
        this.A00 = r1;
    }

    @Override // android.hardware.fingerprint.FingerprintManager.AuthenticationCallback
    public void onAuthenticationError(int i, CharSequence charSequence) {
        this.A00.A01(i, charSequence);
    }

    @Override // android.hardware.fingerprint.FingerprintManager.AuthenticationCallback
    public void onAuthenticationFailed() {
        this.A00.A00();
    }

    @Override // android.hardware.fingerprint.FingerprintManager.AuthenticationCallback
    public void onAuthenticationHelp(int i, CharSequence charSequence) {
        this.A00.A02(i, charSequence);
    }

    @Override // android.hardware.fingerprint.FingerprintManager.AuthenticationCallback
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult authenticationResult) {
        this.A00.A03(new AnonymousClass0MS(AnonymousClass049.A03(AnonymousClass04C.A01(authenticationResult))));
    }
}
