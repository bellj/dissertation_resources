package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5li  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122595li extends AbstractC118835cS {
    public TextView A00;
    public TextView A01;

    public C122595li(View view) {
        super(view);
        this.A01 = C12960it.A0I(view, R.id.header);
        this.A00 = C12960it.A0I(view, R.id.description);
    }

    @Override // X.AbstractC118835cS
    public void A08(AbstractC125975s7 r3, int i) {
        C122795m7 r32 = (C122795m7) r3;
        this.A01.setText(r32.A01);
        String str = r32.A00;
        if (str != null) {
            TextView textView = this.A00;
            textView.setText(str);
            textView.setVisibility(0);
        }
    }
}
