package X;

/* renamed from: X.5t3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C126545t3 {
    public final AnonymousClass1V8 A00;

    public C126545t3(AnonymousClass3CT r11, String str, String str2) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy A0N = C117295Zj.A0N(A0M);
        C41141sy.A01(A0N, "action", "upi-log-event");
        if (AnonymousClass3JT.A0E("U66", 1, 100, false)) {
            C41141sy.A01(A0N, "event-id", "U66");
        }
        if (C117305Zk.A1X(str, 1, false)) {
            C41141sy.A01(A0N, "event-info", str);
        }
        if (C117305Zk.A1X(str2, 1, false)) {
            C41141sy.A01(A0N, "event-dl-info", str2);
        }
        this.A00 = C117295Zj.A0J(A0N, A0M, r11);
    }
}
