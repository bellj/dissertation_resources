package X;

/* renamed from: X.433  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass433 extends AbstractC16110oT {
    public Integer A00;

    public AnonymousClass433() {
        super(3154, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamGatingReviewInteract {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "gatingReviewActionType", C12960it.A0Y(this.A00));
        return C12960it.A0d("}", A0k);
    }
}
