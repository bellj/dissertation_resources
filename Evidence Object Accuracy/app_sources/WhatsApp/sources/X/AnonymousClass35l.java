package X;

import android.graphics.drawable.Drawable;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaTextView;

/* renamed from: X.35l  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass35l extends AbstractC75693kG {
    public final View A00;
    public final WaTextView A01;

    public AnonymousClass35l(View view) {
        super(view);
        View view2 = this.A0H;
        this.A01 = C12960it.A0N(view2, R.id.title);
        this.A00 = AnonymousClass028.A0D(view2, R.id.avocado_pack_layout);
    }

    @Override // X.AbstractC75693kG
    public void A08(View.OnClickListener onClickListener, AnonymousClass4OU r5, boolean z) {
        Drawable drawable;
        if (r5 instanceof AnonymousClass47I) {
            WaTextView waTextView = this.A01;
            waTextView.setText(((AnonymousClass47I) r5).A00);
            if (z) {
                drawable = AnonymousClass00T.A04(waTextView.getContext(), R.drawable.shape_avatar_sticker_picker_item);
            } else {
                drawable = null;
            }
            waTextView.setBackground(drawable);
            waTextView.setTextColor(AnonymousClass00T.A03(waTextView.getContext(), R.color.selector_emoji_icons));
            waTextView.setSelected(z);
            this.A00.setOnClickListener(onClickListener);
            return;
        }
        throw C12970iu.A0f("item should be AvocadoHeaderTextItem");
    }
}
