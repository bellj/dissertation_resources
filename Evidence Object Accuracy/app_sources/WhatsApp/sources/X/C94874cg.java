package X;

import java.util.Arrays;

/* renamed from: X.4cg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C94874cg {
    public static final EnumC87114Ag A02 = EnumC87114Ag.STRETCH;
    public static final EnumC87064Ab A03 = EnumC87064Ab.STRETCH;
    public static final AnonymousClass4BA A04 = AnonymousClass4BA.A03;
    public static final AnonymousClass4BI A05 = AnonymousClass4BI.A03;
    public static final EnumC87304Az A06 = EnumC87304Az.A01;
    public int A00 = 0;
    public float[] A01 = new float[0];

    public static void A00(C94874cg r3, int i) {
        int i2 = r3.A00 + i;
        float[] fArr = r3.A01;
        int length = fArr.length;
        if (i2 > length) {
            int i3 = length << 1;
            if (i3 < i2) {
                i3 += i2 - i3;
            }
            r3.A01 = Arrays.copyOf(fArr, i3);
        }
    }

    public String toString() {
        AnonymousClass4BI r1;
        AnonymousClass4BA r12;
        EnumC87294Ay r13;
        StringBuilder A0h = C12960it.A0h();
        int i = 0;
        while (i < this.A00) {
            AnonymousClass49Z[] values = AnonymousClass49Z.values();
            float[] fArr = this.A01;
            switch (values[(int) fArr[i]].ordinal()) {
                case 0:
                    int i2 = (int) fArr[i + 1];
                    if (i2 == 0) {
                        r13 = EnumC87294Ay.A01;
                    } else if (i2 == 1) {
                        r13 = EnumC87294Ay.A02;
                    } else if (i2 == 2) {
                        r13 = EnumC87294Ay.A03;
                    } else {
                        throw C12970iu.A0f(C12960it.A0W(i2, "Unknown enum value: "));
                    }
                    C72453ed.A1K(r13, "  direction: ", "\n", A0h);
                    i += 2;
                    break;
                case 1:
                    int i3 = (int) fArr[i + 1];
                    if (i3 == 0) {
                        r12 = AnonymousClass4BA.A03;
                    } else if (i3 == 1) {
                        r12 = AnonymousClass4BA.A04;
                    } else if (i3 == 2) {
                        r12 = AnonymousClass4BA.A01;
                    } else if (i3 == 3) {
                        r12 = AnonymousClass4BA.A02;
                    } else {
                        throw C12970iu.A0f(C12960it.A0W(i3, "Unknown enum value: "));
                    }
                    C72453ed.A1K(r12, "  flexDirection: ", "\n", A0h);
                    i += 2;
                    break;
                case 2:
                    int i4 = (int) fArr[i + 1];
                    if (i4 == 0) {
                        r1 = AnonymousClass4BI.A03;
                    } else if (i4 == 1) {
                        r1 = AnonymousClass4BI.A01;
                    } else if (i4 == 2) {
                        r1 = AnonymousClass4BI.A02;
                    } else if (i4 == 3) {
                        r1 = AnonymousClass4BI.A05;
                    } else if (i4 == 4) {
                        r1 = AnonymousClass4BI.A04;
                    } else if (i4 == 5) {
                        r1 = AnonymousClass4BI.A06;
                    } else {
                        throw C12970iu.A0f(C12960it.A0W(i4, "Unknown enum value: "));
                    }
                    C72453ed.A1K(r1, "  justifyContent: ", "\n", A0h);
                    i += 2;
                    break;
                case 3:
                    C72453ed.A1K(EnumC87114Ag.values()[(int) this.A01[i + 1]], "  alignContent: ", "\n", A0h);
                    i += 2;
                    break;
                case 4:
                    C72453ed.A1K(EnumC87064Ab.values()[(int) this.A01[i + 1]], "  alignItems: ", "\n", A0h);
                    i += 2;
                    break;
                case 5:
                    C72453ed.A1K(EnumC87304Az.values()[(int) this.A01[i + 1]], "  flexWrap: ", "\n", A0h);
                    i += 2;
                    break;
                case 6:
                    C72453ed.A1K(EnumC87134Ai.values()[(int) this.A01[i + 1]], "  overflow: ", "\n", A0h);
                    i += 2;
                    break;
                case 7:
                    AnonymousClass39w A00 = AnonymousClass39w.A00((int) fArr[i + 1]);
                    float f = fArr[i + 2];
                    C72453ed.A1K(A00, "  padding", ": ", A0h);
                    A0h.append(f);
                    A0h.append("\n");
                    i += 3;
                    break;
                case 8:
                    AnonymousClass39w A002 = AnonymousClass39w.A00((int) fArr[i + 1]);
                    float f2 = fArr[i + 2];
                    C72453ed.A1K(A002, "  padding", ": ", A0h);
                    A0h.append(f2);
                    A0h.append("%\n");
                    i += 3;
                    break;
                case 9:
                    AnonymousClass39w A003 = AnonymousClass39w.A00((int) fArr[i + 1]);
                    float f3 = fArr[i + 2];
                    C72453ed.A1K(A003, "  border", ": ", A0h);
                    A0h.append(f3);
                    A0h.append("\n");
                    i += 3;
                    break;
            }
        }
        if (A0h.length() <= 0) {
            return "";
        }
        StringBuilder A0k = C12960it.A0k("{\n");
        C12970iu.A1V(A0h, A0k);
        return C12960it.A0d("}", A0k);
    }
}
