package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.msys.mci.DefaultCrypto;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.google.protobuf.CodedOutputStream;
import java.io.IOException;
import org.wawebrtc.MediaCodecVideoEncoder;

/* renamed from: X.2Sz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C51132Sz extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C51132Sz A0P;
    public static volatile AnonymousClass255 A0Q;
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public int A09;
    public long A0A;
    public long A0B;
    public AbstractC27881Jp A0C;
    public AbstractC27881Jp A0D;
    public AbstractC27881Jp A0E;
    public AbstractC27881Jp A0F;
    public AbstractC41941uP A0G = C56822m0.A02;
    public AnonymousClass2TQ A0H;
    public AnonymousClass2TU A0I;
    public AnonymousClass2T5 A0J;
    public AnonymousClass2TC A0K;
    public String A0L = "";
    public boolean A0M;
    public boolean A0N;
    public boolean A0O;

    static {
        C51132Sz r0 = new C51132Sz();
        A0P = r0;
        r0.A0W();
    }

    public C51132Sz() {
        AbstractC27881Jp r0 = AbstractC27881Jp.A01;
        this.A0C = r0;
        this.A0E = r0;
        this.A0D = r0;
        this.A0F = r0;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r16, Object obj, Object obj2) {
        int A02;
        int i;
        AnonymousClass2TV r2;
        AnonymousClass2TR r22;
        AnonymousClass2TE r23;
        AnonymousClass2T6 r24;
        switch (r16.ordinal()) {
            case 0:
                return A0P;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                C51132Sz r1 = (C51132Sz) obj2;
                int i2 = this.A01;
                boolean z = true;
                if ((i2 & 1) != 1) {
                    z = false;
                }
                long j = this.A0B;
                int i3 = r1.A01;
                boolean z2 = true;
                if ((i3 & 1) != 1) {
                    z2 = false;
                }
                this.A0B = r8.Afs(j, r1.A0B, z, z2);
                boolean z3 = false;
                if ((i2 & 2) == 2) {
                    z3 = true;
                }
                boolean z4 = this.A0N;
                boolean z5 = false;
                if ((i3 & 2) == 2) {
                    z5 = true;
                }
                this.A0N = r8.Afl(z3, z4, z5, r1.A0N);
                this.A0J = (AnonymousClass2T5) r8.Aft(this.A0J, r1.A0J);
                this.A0K = (AnonymousClass2TC) r8.Aft(this.A0K, r1.A0K);
                int i4 = this.A01;
                boolean z6 = false;
                if ((i4 & 16) == 16) {
                    z6 = true;
                }
                String str = this.A0L;
                int i5 = r1.A01;
                boolean z7 = false;
                if ((i5 & 16) == 16) {
                    z7 = true;
                }
                this.A0L = r8.Afy(str, r1.A0L, z6, z7);
                boolean z8 = false;
                if ((i4 & 32) == 32) {
                    z8 = true;
                }
                int i6 = this.A09;
                boolean z9 = false;
                if ((i5 & 32) == 32) {
                    z9 = true;
                }
                this.A09 = r8.Afp(i6, r1.A09, z8, z9);
                boolean z10 = false;
                if ((i4 & 64) == 64) {
                    z10 = true;
                }
                boolean z11 = this.A0O;
                boolean z12 = false;
                if ((i5 & 64) == 64) {
                    z12 = true;
                }
                this.A0O = r8.Afl(z10, z11, z12, r1.A0O);
                boolean z13 = false;
                if ((i4 & 128) == 128) {
                    z13 = true;
                }
                int i7 = this.A04;
                boolean z14 = false;
                if ((i5 & 128) == 128) {
                    z14 = true;
                }
                this.A04 = r8.Afp(i7, r1.A04, z13, z14);
                boolean z15 = false;
                if ((i4 & 256) == 256) {
                    z15 = true;
                }
                int i8 = this.A03;
                boolean z16 = false;
                if ((i5 & 256) == 256) {
                    z16 = true;
                }
                this.A03 = r8.Afp(i8, r1.A03, z15, z16);
                this.A0G = r8.Afq(this.A0G, r1.A0G);
                this.A0H = (AnonymousClass2TQ) r8.Aft(this.A0H, r1.A0H);
                int i9 = this.A01;
                boolean z17 = false;
                if ((i9 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
                    z17 = true;
                }
                int i10 = this.A02;
                int i11 = r1.A01;
                boolean z18 = false;
                if ((i11 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
                    z18 = true;
                }
                this.A02 = r8.Afp(i10, r1.A02, z17, z18);
                boolean z19 = false;
                if ((i9 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
                    z19 = true;
                }
                int i12 = this.A00;
                boolean z20 = false;
                if ((i11 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
                    z20 = true;
                }
                this.A00 = r8.Afp(i12, r1.A00, z19, z20);
                boolean z21 = false;
                if ((i9 & 4096) == 4096) {
                    z21 = true;
                }
                int i13 = this.A05;
                boolean z22 = false;
                if ((i11 & 4096) == 4096) {
                    z22 = true;
                }
                this.A05 = r8.Afp(i13, r1.A05, z21, z22);
                this.A0I = (AnonymousClass2TU) r8.Aft(this.A0I, r1.A0I);
                int i14 = this.A01;
                boolean z23 = false;
                if ((i14 & 16384) == 16384) {
                    z23 = true;
                }
                int i15 = this.A08;
                int i16 = r1.A01;
                boolean z24 = false;
                if ((i16 & 16384) == 16384) {
                    z24 = true;
                }
                this.A08 = r8.Afp(i15, r1.A08, z23, z24);
                boolean z25 = false;
                if ((i14 & 32768) == 32768) {
                    z25 = true;
                }
                AbstractC27881Jp r3 = this.A0C;
                boolean z26 = false;
                if ((i16 & 32768) == 32768) {
                    z26 = true;
                }
                this.A0C = r8.Afm(r3, r1.A0C, z25, z26);
                boolean z27 = false;
                if ((this.A01 & 65536) == 65536) {
                    z27 = true;
                }
                AbstractC27881Jp r4 = this.A0E;
                boolean z28 = false;
                if ((r1.A01 & 65536) == 65536) {
                    z28 = true;
                }
                this.A0E = r8.Afm(r4, r1.A0E, z27, z28);
                int i17 = this.A01;
                boolean z29 = false;
                if ((i17 & C25981Bo.A0F) == 131072) {
                    z29 = true;
                }
                boolean z30 = this.A0M;
                int i18 = r1.A01;
                boolean z31 = false;
                if ((i18 & C25981Bo.A0F) == 131072) {
                    z31 = true;
                }
                this.A0M = r8.Afl(z29, z30, z31, r1.A0M);
                boolean z32 = false;
                if ((i17 & 262144) == 262144) {
                    z32 = true;
                }
                int i19 = this.A07;
                boolean z33 = false;
                if ((i18 & 262144) == 262144) {
                    z33 = true;
                }
                this.A07 = r8.Afp(i19, r1.A07, z32, z33);
                boolean z34 = false;
                if ((i17 & 524288) == 524288) {
                    z34 = true;
                }
                int i20 = this.A06;
                boolean z35 = false;
                if ((i18 & 524288) == 524288) {
                    z35 = true;
                }
                this.A06 = r8.Afp(i20, r1.A06, z34, z35);
                boolean z36 = false;
                if ((i17 & 1048576) == 1048576) {
                    z36 = true;
                }
                long j2 = this.A0A;
                boolean z37 = false;
                if ((i18 & 1048576) == 1048576) {
                    z37 = true;
                }
                this.A0A = r8.Afs(j2, r1.A0A, z36, z37);
                boolean z38 = false;
                if ((i17 & 2097152) == 2097152) {
                    z38 = true;
                }
                AbstractC27881Jp r32 = this.A0D;
                boolean z39 = false;
                if ((i18 & 2097152) == 2097152) {
                    z39 = true;
                }
                this.A0D = r8.Afm(r32, r1.A0D, z38, z39);
                boolean z40 = false;
                if ((this.A01 & 4194304) == 4194304) {
                    z40 = true;
                }
                AbstractC27881Jp r42 = this.A0F;
                boolean z41 = false;
                if ((r1.A01 & 4194304) == 4194304) {
                    z41 = true;
                }
                this.A0F = r8.Afm(r42, r1.A0F, z40, z41);
                if (r8 == C463025i.A00) {
                    this.A01 |= r1.A01;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                AnonymousClass254 r12 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        try {
                            int A03 = r82.A03();
                            switch (A03) {
                                case 0:
                                    break;
                                case 8:
                                    this.A01 |= 1;
                                    this.A0B = r82.A06();
                                    continue;
                                case 24:
                                    this.A01 |= 2;
                                    this.A0N = r82.A0F();
                                    continue;
                                case 42:
                                    if ((this.A01 & 4) == 4) {
                                        r24 = (AnonymousClass2T6) this.A0J.A0T();
                                    } else {
                                        r24 = null;
                                    }
                                    AnonymousClass2T5 r0 = (AnonymousClass2T5) r82.A09(r12, AnonymousClass2T5.A0E.A0U());
                                    this.A0J = r0;
                                    if (r24 != null) {
                                        r24.A04(r0);
                                        this.A0J = (AnonymousClass2T5) r24.A01();
                                    }
                                    this.A01 |= 4;
                                    continue;
                                case SearchActionVerificationClientService.TIME_TO_SLEEP_IN_MS /* 50 */:
                                    if ((this.A01 & 8) == 8) {
                                        r23 = (AnonymousClass2TE) this.A0K.A0T();
                                    } else {
                                        r23 = null;
                                    }
                                    AnonymousClass2TC r02 = (AnonymousClass2TC) r82.A09(r12, AnonymousClass2TC.A05.A0U());
                                    this.A0K = r02;
                                    if (r23 != null) {
                                        r23.A04(r02);
                                        this.A0K = (AnonymousClass2TC) r23.A01();
                                    }
                                    this.A01 |= 8;
                                    continue;
                                case 58:
                                    String A0A = r82.A0A();
                                    this.A01 |= 16;
                                    this.A0L = A0A;
                                    continue;
                                case 77:
                                    this.A01 |= 32;
                                    this.A09 = r82.A01();
                                    continue;
                                case 80:
                                    this.A01 |= 64;
                                    this.A0O = r82.A0F();
                                    continue;
                                case 96:
                                    A02 = r82.A02();
                                    if (!(A02 == 0 || A02 == 1)) {
                                        switch (A02) {
                                            case 100:
                                            case 101:
                                            case 102:
                                            case 103:
                                            case 104:
                                            case 105:
                                            case 106:
                                            case 107:
                                            case C43951xu.A03:
                                            case 109:
                                            case 110:
                                            case 111:
                                            case 112:
                                                break;
                                            default:
                                                i = 12;
                                                break;
                                        }
                                    }
                                    this.A01 |= 128;
                                    this.A04 = A02;
                                    continue;
                                    break;
                                case 104:
                                    A02 = r82.A02();
                                    if (A02 != 0 && A02 != 1 && A02 != 2 && A02 != 3 && A02 != 4 && A02 != 5) {
                                        i = 13;
                                        break;
                                    } else {
                                        this.A01 |= 256;
                                        this.A03 = A02;
                                        continue;
                                    }
                                    break;
                                case 112:
                                    AbstractC41941uP r33 = this.A0G;
                                    if (!((AnonymousClass1K7) r33).A00) {
                                        r33 = AbstractC27091Fz.A0F(r33);
                                        this.A0G = r33;
                                    }
                                    C56822m0 r34 = (C56822m0) r33;
                                    r34.A02(r34.A00, r82.A02());
                                    continue;
                                case 114:
                                    int A04 = r82.A04(r82.A02());
                                    AbstractC41941uP r25 = this.A0G;
                                    if (!((AnonymousClass1K7) r25).A00 && r82.A00() > 0) {
                                        this.A0G = AbstractC27091Fz.A0F(r25);
                                    }
                                    while (r82.A00() > 0) {
                                        C56822m0 r35 = (C56822m0) this.A0G;
                                        r35.A02(r35.A00, r82.A02());
                                    }
                                    r82.A03 = A04;
                                    r82.A0B();
                                    continue;
                                    break;
                                case 122:
                                    if ((this.A01 & 512) == 512) {
                                        r22 = (AnonymousClass2TR) this.A0H.A0T();
                                    } else {
                                        r22 = null;
                                    }
                                    AnonymousClass2TQ r03 = (AnonymousClass2TQ) r82.A09(r12, AnonymousClass2TQ.A03.A0U());
                                    this.A0H = r03;
                                    if (r22 != null) {
                                        r22.A04(r03);
                                        this.A0H = (AnonymousClass2TQ) r22.A01();
                                    }
                                    this.A01 |= 512;
                                    continue;
                                case 128:
                                    this.A01 |= EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
                                    this.A02 = r82.A02();
                                    continue;
                                case 136:
                                    this.A01 |= EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
                                    this.A00 = r82.A02();
                                    continue;
                                case MediaCodecVideoEncoder.MIN_ENCODER_HEIGHT /* 144 */:
                                    this.A01 |= 4096;
                                    this.A05 = r82.A02();
                                    continue;
                                case 154:
                                    if ((this.A01 & DefaultCrypto.BUFFER_SIZE) == 8192) {
                                        r2 = (AnonymousClass2TV) this.A0I.A0T();
                                    } else {
                                        r2 = null;
                                    }
                                    AnonymousClass2TU r04 = (AnonymousClass2TU) r82.A09(r12, AnonymousClass2TU.A09.A0U());
                                    this.A0I = r04;
                                    if (r2 != null) {
                                        r2.A04(r04);
                                        this.A0I = (AnonymousClass2TU) r2.A01();
                                    }
                                    this.A01 |= DefaultCrypto.BUFFER_SIZE;
                                    continue;
                                case 160:
                                    A02 = r82.A02();
                                    if (A02 != 0 && A02 != 1) {
                                        i = 20;
                                        break;
                                    } else {
                                        this.A01 |= 16384;
                                        this.A08 = A02;
                                        continue;
                                    }
                                    break;
                                case 170:
                                    this.A01 |= 32768;
                                    this.A0C = r82.A08();
                                    continue;
                                case 178:
                                    this.A01 |= 65536;
                                    this.A0E = r82.A08();
                                    continue;
                                case 184:
                                    this.A01 |= C25981Bo.A0F;
                                    this.A0M = r82.A0F();
                                    continue;
                                case 192:
                                    this.A01 |= 262144;
                                    this.A07 = r82.A02();
                                    continue;
                                case 240:
                                    A02 = r82.A02();
                                    if (A02 != 0 && A02 != 1 && A02 != 2) {
                                        i = 30;
                                        break;
                                    } else {
                                        this.A01 |= 524288;
                                        this.A06 = A02;
                                        continue;
                                    }
                                    break;
                                case 248:
                                    this.A01 |= 1048576;
                                    this.A0A = r82.A06();
                                    continue;
                                case 258:
                                    this.A01 |= 2097152;
                                    this.A0D = r82.A08();
                                    continue;
                                case 274:
                                    this.A01 |= 4194304;
                                    this.A0F = r82.A08();
                                    continue;
                                default:
                                    if (!A0a(r82, A03)) {
                                        break;
                                    } else {
                                        continue;
                                    }
                            }
                            super.A0X(i, A02);
                        } catch (C28971Pt e) {
                            e.unfinishedMessage = this;
                            throw new RuntimeException(e);
                        }
                    } catch (IOException e2) {
                        C28971Pt r13 = new C28971Pt(e2.getMessage());
                        r13.unfinishedMessage = this;
                        throw new RuntimeException(r13);
                    }
                }
                break;
            case 3:
                ((AnonymousClass1K7) this.A0G).A00 = false;
                return null;
            case 4:
                return new C51132Sz();
            case 5:
                return new AnonymousClass2T0();
            case 6:
                break;
            case 7:
                if (A0Q == null) {
                    synchronized (C51132Sz.class) {
                        if (A0Q == null) {
                            A0Q = new AnonymousClass255(A0P);
                        }
                    }
                }
                return A0Q;
            default:
                throw new UnsupportedOperationException();
        }
        return A0P;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i;
        int i2;
        int i3 = ((AbstractC27091Fz) this).A00;
        if (i3 != -1) {
            return i3;
        }
        int i4 = this.A01;
        if ((i4 & 1) == 1) {
            i = CodedOutputStream.A06(1, this.A0B) + 0;
        } else {
            i = 0;
        }
        if ((i4 & 2) == 2) {
            i += CodedOutputStream.A00(3);
        }
        if ((i4 & 4) == 4) {
            AnonymousClass2T5 r0 = this.A0J;
            if (r0 == null) {
                r0 = AnonymousClass2T5.A0E;
            }
            i += CodedOutputStream.A0A(r0, 5);
        }
        if ((this.A01 & 8) == 8) {
            AnonymousClass2TC r02 = this.A0K;
            if (r02 == null) {
                r02 = AnonymousClass2TC.A05;
            }
            i += CodedOutputStream.A0A(r02, 6);
        }
        if ((this.A01 & 16) == 16) {
            i += CodedOutputStream.A07(7, this.A0L);
        }
        int i5 = this.A01;
        if ((i5 & 32) == 32) {
            i += 5;
        }
        if ((i5 & 64) == 64) {
            i += CodedOutputStream.A00(10);
        }
        if ((i5 & 128) == 128) {
            i += CodedOutputStream.A02(12, this.A04);
        }
        if ((i5 & 256) == 256) {
            i += CodedOutputStream.A02(13, this.A03);
        }
        int i6 = 0;
        for (int i7 = 0; i7 < this.A0G.size(); i7++) {
            C56822m0 r03 = (C56822m0) this.A0G;
            r03.A01(i7);
            int i8 = r03.A01[i7];
            if (i8 >= 0) {
                i2 = CodedOutputStream.A01(i8);
            } else {
                i2 = 10;
            }
            i6 += i2;
        }
        int size = i + i6 + this.A0G.size();
        if ((this.A01 & 512) == 512) {
            AnonymousClass2TQ r04 = this.A0H;
            if (r04 == null) {
                r04 = AnonymousClass2TQ.A03;
            }
            size += CodedOutputStream.A0A(r04, 15);
        }
        int i9 = this.A01;
        if ((i9 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            size += CodedOutputStream.A04(16, this.A02);
        }
        if ((i9 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            size += CodedOutputStream.A04(17, this.A00);
        }
        if ((i9 & 4096) == 4096) {
            size += CodedOutputStream.A04(18, this.A05);
        }
        if ((i9 & DefaultCrypto.BUFFER_SIZE) == 8192) {
            AnonymousClass2TU r05 = this.A0I;
            if (r05 == null) {
                r05 = AnonymousClass2TU.A09;
            }
            size += CodedOutputStream.A0A(r05, 19);
        }
        int i10 = this.A01;
        if ((i10 & 16384) == 16384) {
            size += CodedOutputStream.A02(20, this.A08);
        }
        if ((i10 & 32768) == 32768) {
            size += CodedOutputStream.A09(this.A0C, 21);
        }
        if ((i10 & 65536) == 65536) {
            size += CodedOutputStream.A09(this.A0E, 22);
        }
        if ((i10 & C25981Bo.A0F) == 131072) {
            size += CodedOutputStream.A00(23);
        }
        if ((i10 & 262144) == 262144) {
            size += CodedOutputStream.A03(24, this.A07);
        }
        if ((i10 & 524288) == 524288) {
            size += CodedOutputStream.A02(30, this.A06);
        }
        if ((i10 & 1048576) == 1048576) {
            size += CodedOutputStream.A06(31, this.A0A);
        }
        if ((i10 & 2097152) == 2097152) {
            size += CodedOutputStream.A09(this.A0D, 32);
        }
        if ((i10 & 4194304) == 4194304) {
            size += CodedOutputStream.A09(this.A0F, 34);
        }
        int A00 = size + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A01 & 1) == 1) {
            codedOutputStream.A0H(1, this.A0B);
        }
        if ((this.A01 & 2) == 2) {
            codedOutputStream.A0J(3, this.A0N);
        }
        if ((this.A01 & 4) == 4) {
            AnonymousClass2T5 r0 = this.A0J;
            if (r0 == null) {
                r0 = AnonymousClass2T5.A0E;
            }
            codedOutputStream.A0L(r0, 5);
        }
        if ((this.A01 & 8) == 8) {
            AnonymousClass2TC r02 = this.A0K;
            if (r02 == null) {
                r02 = AnonymousClass2TC.A05;
            }
            codedOutputStream.A0L(r02, 6);
        }
        if ((this.A01 & 16) == 16) {
            codedOutputStream.A0I(7, this.A0L);
        }
        if ((this.A01 & 32) == 32) {
            codedOutputStream.A0D(9, this.A09);
        }
        if ((this.A01 & 64) == 64) {
            codedOutputStream.A0J(10, this.A0O);
        }
        if ((this.A01 & 128) == 128) {
            codedOutputStream.A0E(12, this.A04);
        }
        if ((this.A01 & 256) == 256) {
            codedOutputStream.A0E(13, this.A03);
        }
        for (int i = 0; i < this.A0G.size(); i++) {
            C56822m0 r03 = (C56822m0) this.A0G;
            r03.A01(i);
            codedOutputStream.A0E(14, r03.A01[i]);
        }
        if ((this.A01 & 512) == 512) {
            AnonymousClass2TQ r04 = this.A0H;
            if (r04 == null) {
                r04 = AnonymousClass2TQ.A03;
            }
            codedOutputStream.A0L(r04, 15);
        }
        if ((this.A01 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            codedOutputStream.A0F(16, this.A02);
        }
        if ((this.A01 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            codedOutputStream.A0F(17, this.A00);
        }
        if ((this.A01 & 4096) == 4096) {
            codedOutputStream.A0F(18, this.A05);
        }
        if ((this.A01 & DefaultCrypto.BUFFER_SIZE) == 8192) {
            AnonymousClass2TU r05 = this.A0I;
            if (r05 == null) {
                r05 = AnonymousClass2TU.A09;
            }
            codedOutputStream.A0L(r05, 19);
        }
        if ((this.A01 & 16384) == 16384) {
            codedOutputStream.A0E(20, this.A08);
        }
        if ((this.A01 & 32768) == 32768) {
            codedOutputStream.A0K(this.A0C, 21);
        }
        if ((this.A01 & 65536) == 65536) {
            codedOutputStream.A0K(this.A0E, 22);
        }
        if ((this.A01 & C25981Bo.A0F) == 131072) {
            codedOutputStream.A0J(23, this.A0M);
        }
        if ((this.A01 & 262144) == 262144) {
            codedOutputStream.A0E(24, this.A07);
        }
        if ((this.A01 & 524288) == 524288) {
            codedOutputStream.A0E(30, this.A06);
        }
        if ((this.A01 & 1048576) == 1048576) {
            codedOutputStream.A0H(31, this.A0A);
        }
        if ((this.A01 & 2097152) == 2097152) {
            codedOutputStream.A0K(this.A0D, 32);
        }
        if ((this.A01 & 4194304) == 4194304) {
            codedOutputStream.A0K(this.A0F, 34);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
