package X;

import com.google.android.exoplayer2.Timeline;

/* renamed from: X.3HD  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3HD {
    public C28741Ov A00;
    public C28741Ov A01;
    public C28741Ov A02;
    public AnonymousClass1Mr A03 = AnonymousClass1Mr.of();
    public AbstractC17190qP A04 = AbstractC17190qP.of();
    public final AnonymousClass4YJ A05;

    public AnonymousClass3HD(AnonymousClass4YJ r2) {
        this.A05 = r2;
    }

    public static C28741Ov A00(AbstractC47512Az r9, AnonymousClass4YJ r10, C28741Ov r11, AnonymousClass1Mr r12) {
        Object A0C;
        int i;
        Timeline ACE = r9.ACE();
        int AC8 = r9.AC8();
        boolean A0D = ACE.A0D();
        if (A0D) {
            A0C = null;
        } else {
            A0C = ACE.A0C(AC8);
        }
        if (r9.AJt() || A0D) {
            i = -1;
        } else {
            i = ACE.A09(r10, AC8, false).A02(C95214dK.A01(r9.AC9()) - r10.A02);
        }
        for (int i2 = 0; i2 < r12.size(); i2++) {
            C28741Ov r6 = (C28741Ov) r12.get(i2);
            boolean AJt = r9.AJt();
            int AC2 = r9.AC2();
            int AC3 = r9.AC3();
            if (r6.A04.equals(A0C)) {
                if (AJt) {
                    if (r6.A00 == AC2 && r6.A01 == AC3) {
                        return r6;
                    }
                } else if (r6.A00 == -1 && r6.A02 == i) {
                    return r6;
                }
            }
        }
        if (r12.isEmpty() && r11 != null) {
            boolean AJt2 = r9.AJt();
            int AC22 = r9.AC2();
            int AC32 = r9.AC3();
            if (r11.A04.equals(A0C) && (!AJt2 ? r11.A00 == -1 && r11.A02 == i : r11.A00 == AC22 && r11.A01 == AC32)) {
                return r11;
            }
        }
        return null;
    }

    public final void A01(Timeline timeline) {
        boolean contains;
        C28241Mh builder = AbstractC17190qP.builder();
        if (this.A03.isEmpty()) {
            A02(timeline, this.A01, builder);
            if (!AnonymousClass28V.A00(this.A02, this.A01)) {
                A02(timeline, this.A02, builder);
            }
            if (!AnonymousClass28V.A00(this.A00, this.A01)) {
                contains = AnonymousClass28V.A00(this.A00, this.A02);
            }
            this.A04 = builder.build();
        }
        for (int i = 0; i < this.A03.size(); i++) {
            A02(timeline, (C28741Ov) this.A03.get(i), builder);
        }
        contains = this.A03.contains(this.A00);
        if (!contains) {
            A02(timeline, this.A00, builder);
        }
        this.A04 = builder.build();
    }

    public final void A02(Timeline timeline, C28741Ov r4, C28241Mh r5) {
        if (r4 != null) {
            int A04 = timeline.A04(r4.A04);
            Object obj = timeline;
            if (A04 == -1) {
                Object obj2 = this.A04.get(r4);
                obj = obj2;
                if (obj2 == null) {
                    return;
                }
            }
            r5.put(r4, obj);
        }
    }
}
