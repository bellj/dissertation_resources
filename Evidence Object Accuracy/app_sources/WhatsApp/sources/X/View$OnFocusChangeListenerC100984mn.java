package X;

import android.view.View;
import com.whatsapp.search.views.TokenizedSearchInput;

/* renamed from: X.4mn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class View$OnFocusChangeListenerC100984mn implements View.OnFocusChangeListener {
    public final /* synthetic */ TokenizedSearchInput A00;

    public View$OnFocusChangeListenerC100984mn(TokenizedSearchInput tokenizedSearchInput) {
        this.A00 = tokenizedSearchInput;
    }

    @Override // android.view.View.OnFocusChangeListener
    public void onFocusChange(View view, boolean z) {
        this.A00.A0C.A0X(z);
    }
}
