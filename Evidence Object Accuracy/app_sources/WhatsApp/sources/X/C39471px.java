package X;

import java.util.HashMap;

/* renamed from: X.1px  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C39471px {
    public final C39451pv A00;
    public final HashMap A01;

    public C39471px(C39451pv r1, HashMap hashMap) {
        this.A00 = r1;
        this.A01 = hashMap;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C39471px)) {
            return false;
        }
        C39471px r4 = (C39471px) obj;
        if (!this.A00.equals(r4.A00) || !this.A01.equals(r4.A01)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return ((159 + this.A00.hashCode()) * 53) + this.A01.hashCode();
    }
}
