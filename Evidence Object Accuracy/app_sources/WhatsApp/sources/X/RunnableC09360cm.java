package X;

import androidx.work.impl.workers.ConstraintTrackingWorker;

/* renamed from: X.0cm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09360cm implements Runnable {
    public final /* synthetic */ ConstraintTrackingWorker A00;

    public RunnableC09360cm(ConstraintTrackingWorker constraintTrackingWorker) {
        this.A00 = constraintTrackingWorker;
    }

    @Override // java.lang.Runnable
    public void run() {
        this.A00.A05();
    }
}
