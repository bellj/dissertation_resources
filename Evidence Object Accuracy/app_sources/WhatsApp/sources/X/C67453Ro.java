package X;

import android.graphics.drawable.GradientDrawable;
import com.whatsapp.R;

/* renamed from: X.3Ro  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class C67453Ro implements AbstractC11810gu {
    public final /* synthetic */ AbstractActivityC33001d7 A00;

    public /* synthetic */ C67453Ro(AbstractActivityC33001d7 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC11810gu
    public final void AQr(C06030Rx r8) {
        AbstractActivityC33001d7 r5 = this.A00;
        AnonymousClass2Ew r2 = (AnonymousClass2Ew) r5.findViewById(R.id.content);
        if (!C41691tw.A08(r5) && r8 != null) {
            r8.A04.get(C06010Rv.A08);
        }
        r2.setColor(AnonymousClass00T.A00(r5, R.color.primary));
        AnonymousClass00T.A00(r5, R.color.primary);
        r5.findViewById(R.id.bottom_shade).setBackground(new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, new int[]{1711276032, 0}));
        r5.findViewById(R.id.top_shade).setBackground(new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{855638016, 0}));
    }
}
