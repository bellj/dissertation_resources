package X;

/* renamed from: X.0da  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09830da implements Runnable {
    public final /* synthetic */ RunnableC09840db A00;
    public final /* synthetic */ AnonymousClass024 A01;
    public final /* synthetic */ Object A02;

    public RunnableC09830da(RunnableC09840db r1, AnonymousClass024 r2, Object obj) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = obj;
    }

    @Override // java.lang.Runnable
    public void run() {
        this.A01.accept(this.A02);
    }
}
