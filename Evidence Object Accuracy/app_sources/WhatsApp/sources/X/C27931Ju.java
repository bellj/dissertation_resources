package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.1Ju  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C27931Ju extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C27931Ju A04;
    public static volatile AnonymousClass255 A05;
    public int A00;
    public C27921Jt A01;
    public C27901Jr A02;
    public C27911Js A03;

    static {
        C27931Ju r0 = new C27931Ju();
        A04 = r0;
        r0.A0W();
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            C27901Jr r0 = this.A02;
            if (r0 == null) {
                r0 = C27901Jr.A02;
            }
            i2 = 0 + CodedOutputStream.A0A(r0, 1);
        }
        if ((this.A00 & 2) == 2) {
            C27911Js r02 = this.A03;
            if (r02 == null) {
                r02 = C27911Js.A02;
            }
            i2 += CodedOutputStream.A0A(r02, 2);
        }
        if ((this.A00 & 4) == 4) {
            C27921Jt r03 = this.A01;
            if (r03 == null) {
                r03 = C27921Jt.A02;
            }
            i2 += CodedOutputStream.A0A(r03, 3);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            C27901Jr r0 = this.A02;
            if (r0 == null) {
                r0 = C27901Jr.A02;
            }
            codedOutputStream.A0L(r0, 1);
        }
        if ((this.A00 & 2) == 2) {
            C27911Js r02 = this.A03;
            if (r02 == null) {
                r02 = C27911Js.A02;
            }
            codedOutputStream.A0L(r02, 2);
        }
        if ((this.A00 & 4) == 4) {
            C27921Jt r03 = this.A01;
            if (r03 == null) {
                r03 = C27921Jt.A02;
            }
            codedOutputStream.A0L(r03, 3);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
