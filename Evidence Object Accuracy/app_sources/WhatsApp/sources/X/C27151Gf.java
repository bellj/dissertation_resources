package X;

import android.os.Handler;
import com.facebook.redex.RunnableBRunnable0Shape1S0200000_I0_1;
import com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3;
import com.facebook.redex.RunnableBRunnable0Shape7S0100000_I0_7;
import com.whatsapp.chatinfo.ContactInfoActivity;
import com.whatsapp.community.CommunityTabViewModel;
import com.whatsapp.group.GroupChatInfo;
import com.whatsapp.group.NewGroup;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.util.Log;
import java.util.Map;

/* renamed from: X.1Gf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C27151Gf {
    public void A00(AbstractC14640lm r6) {
        if (this instanceof C27141Ge) {
            C27141Ge r2 = (C27141Ge) this;
            if (r2.A00.A00.A05(AbstractC15460nI.A18) && r6 != null && r6.getType() == 1) {
                for (AbstractC35911iz r1 : r2.A01.A00()) {
                    if (r1 instanceof C36211jV) {
                        C36211jV r12 = (C36211jV) r1;
                        Handler handler = r12.A00;
                        handler.removeCallbacksAndMessages(null);
                        handler.postDelayed(new RunnableBRunnable0Shape7S0100000_I0_7(r12, 21), 2000);
                    }
                }
            }
        } else if (this instanceof C35901iy) {
            C35901iy r3 = (C35901iy) this;
            C35891iw r13 = r3.A00;
            if (r6 == r13.A03) {
                r13.A06.A0H(new RunnableBRunnable0Shape7S0100000_I0_7(r3, 17));
            }
        } else if (this instanceof C32921cx) {
            Log.i("newgroup/onConversationAdded");
            NewGroup newGroup = ((C32921cx) this).A00;
            AnonymousClass01T r0 = newGroup.A05;
            if (r0 != null && r0.A00.equals(r6)) {
                Log.i("newgroup/onConversationAdded/processing runAfterTempConversationAddedToDb");
                ((Runnable) newGroup.A05.A01).run();
                newGroup.A05 = null;
            }
        } else if (this instanceof AnonymousClass27r) {
            AnonymousClass27r r22 = (AnonymousClass27r) this;
            StringBuilder sb = new StringBuilder("joinSubgroup/onConversationAdded/");
            sb.append(r6);
            Log.i(sb.toString());
            if (r6 instanceof C15580nU) {
                C468027s r23 = r22.A00;
                int intValue = ((Number) r23.A0U.A01()).intValue();
                if (intValue == 2 || intValue == 6) {
                    r23.A05(4);
                }
            }
        }
    }

    public void A01(AbstractC14640lm r6) {
        boolean z;
        if (this instanceof C468127t) {
            ((C468127t) this).A00.A04(r6);
        } else if (this instanceof C32901cv) {
            ((C32901cv) this).A00.A1G.A0G();
        } else if (this instanceof C32931cy) {
            GroupChatInfo groupChatInfo = ((C32931cy) this).A00;
            if (r6.equals(groupChatInfo.A1C) && !(!((ActivityC13810kN) groupChatInfo).A0E)) {
                groupChatInfo.startActivity(C14960mK.A02(groupChatInfo.getApplicationContext()).addFlags(603979776));
            }
        } else if (this instanceof AnonymousClass27r) {
            AnonymousClass27r r1 = (AnonymousClass27r) this;
            if (r6 instanceof C15580nU) {
                C468027s r2 = r1.A00;
                if (((Number) r2.A0U.A01()).intValue() == 4) {
                    r2.A05(2);
                }
            }
        } else if (this instanceof C32961d1) {
            CommunityTabViewModel communityTabViewModel = ((C32961d1) this).A00;
            if (r6 instanceof C15580nU) {
                Map map = communityTabViewModel.A04;
                if (!map.isEmpty()) {
                    for (Map.Entry entry : map.entrySet()) {
                        AnonymousClass1PE r22 = (AnonymousClass1PE) entry.getKey();
                        AbstractC14640lm A05 = r22.A05();
                        if ((A05 instanceof C15580nU) && r6.equals(A05)) {
                            communityTabViewModel.A04(r22);
                            communityTabViewModel.A06(true);
                            return;
                        }
                    }
                }
            }
        } else if (this instanceof C468227u) {
            C468227u r12 = (C468227u) this;
            if (GroupJid.of(r6) != null) {
                C37271lv r3 = r12.A00;
                if (r3.A0N.A00(r3.A01)) {
                    loop0: while (true) {
                        z = false;
                        for (AnonymousClass1OU r0 : r3.A0Z) {
                            if (z || r0.A02 == r6) {
                                z = true;
                            }
                        }
                    }
                    if (z) {
                        r3.A0U.execute(new RunnableBRunnable0Shape3S0100000_I0_3(r3, 47));
                    }
                }
            }
        } else if (this instanceof C32971d2) {
            ContactInfoActivity contactInfoActivity = ((C32971d2) this).A00;
            if (r6.equals(contactInfoActivity.A2v()) && !(!((ActivityC13810kN) contactInfoActivity).A0E)) {
                contactInfoActivity.startActivity(C14960mK.A02(contactInfoActivity.getApplicationContext()).addFlags(603979776));
            }
        } else if (this instanceof C27401Hg) {
            C27401Hg r32 = (C27401Hg) this;
            r32.A01.Ab2(new RunnableBRunnable0Shape1S0200000_I0_1(r32, 6, r6));
        }
    }
}
