package X;

/* renamed from: X.5cj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119005cj extends AbstractC129405xf {
    public final /* synthetic */ C129455xk A00;
    public final /* synthetic */ AnonymousClass60X A01;

    public C119005cj(C129455xk r1, AnonymousClass60X r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC129405xf
    public void A00(Exception exc) {
        AnonymousClass60X r1 = this.A01;
        r1.A0G = false;
        AnonymousClass616.A00();
        r1.A04(this.A00, exc);
    }

    @Override // X.AbstractC129405xf
    public /* bridge */ /* synthetic */ void A01(Object obj) {
        this.A01.A0G = false;
    }
}
