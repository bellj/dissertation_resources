package X;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiBankAccountDetailsActivity;
import com.whatsapp.payments.ui.IndiaUpiChangePinActivity;
import com.whatsapp.payments.ui.IndiaUpiPinPrimerFullSheetActivity;

/* renamed from: X.5aR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class View$OnClickListenerC117685aR extends LinearLayout implements View.OnClickListener, AnonymousClass004 {
    public View A00;
    public View A01;
    public TextView A02;
    public C15450nH A03;
    public AnonymousClass018 A04;
    public C14850m9 A05;
    public AnonymousClass1ZR A06;
    public AbstractC136106La A07;
    public AnonymousClass2P7 A08;
    public boolean A09;

    public View$OnClickListenerC117685aR(Context context) {
        super(context);
        if (!this.A09) {
            this.A09 = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A03 = (C15450nH) A00.AII.get();
            this.A05 = C12960it.A0S(A00);
            this.A04 = C12960it.A0R(A00);
        }
        LayoutInflater.from(context).inflate(R.layout.india_upi_pin_widget, this);
        int A002 = AnonymousClass00T.A00(context, R.color.settings_icon);
        C117295Zj.A0m(this, R.id.change_icon, A002);
        C117295Zj.A0m(this, R.id.reset_icon, A002);
        C117295Zj.A0m(this, R.id.switch_payment_provider_icon, A002);
    }

    public void A00() {
        this.A06 = C117305Zk.A0I(C117305Zk.A0J(), Boolean.class, Boolean.TRUE, "isPinSet");
        this.A02.setText(R.string.forgot_upi_pin);
        this.A00.setVisibility(0);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A08;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A08 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        Intent A02;
        int i;
        if (view.getId() == R.id.reset_upi_pin_container) {
            IndiaUpiBankAccountDetailsActivity indiaUpiBankAccountDetailsActivity = (IndiaUpiBankAccountDetailsActivity) this.A07;
            boolean A1Y = C12970iu.A1Y(this.A06.A00);
            C30861Zc r1 = indiaUpiBankAccountDetailsActivity.A00;
            if (A1Y) {
                A02 = IndiaUpiPinPrimerFullSheetActivity.A02(indiaUpiBankAccountDetailsActivity, r1, true);
                i = 1017;
            } else {
                A02 = IndiaUpiPinPrimerFullSheetActivity.A02(indiaUpiBankAccountDetailsActivity, r1, false);
                i = 1016;
            }
            indiaUpiBankAccountDetailsActivity.startActivityForResult(A02, i);
        } else if (view.getId() == R.id.change_upi_pin_container) {
            IndiaUpiBankAccountDetailsActivity indiaUpiBankAccountDetailsActivity2 = (IndiaUpiBankAccountDetailsActivity) this.A07;
            Intent A0D = C12990iw.A0D(indiaUpiBankAccountDetailsActivity2, IndiaUpiChangePinActivity.class);
            C117315Zl.A0M(A0D, indiaUpiBankAccountDetailsActivity2.A00);
            indiaUpiBankAccountDetailsActivity2.startActivity(A0D);
        } else if (view.getId() == R.id.switch_payment_provider_container) {
            IndiaUpiBankAccountDetailsActivity indiaUpiBankAccountDetailsActivity3 = (IndiaUpiBankAccountDetailsActivity) this.A07;
            RunnableC134626Fi r3 = new Runnable() { // from class: X.6Fi
                @Override // java.lang.Runnable
                public final void run() {
                    C36021jC.A01(IndiaUpiBankAccountDetailsActivity.this, 100);
                }
            };
            C12960it.A1E(new C124115od(indiaUpiBankAccountDetailsActivity3, r3, 104), ((AbstractView$OnClickListenerC121765jx) indiaUpiBankAccountDetailsActivity3).A0H);
        }
    }
}
