package X;

import android.view.View;
import com.whatsapp.R;

/* renamed from: X.2vg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59972vg extends AbstractC37191le implements AbstractC116555Vx {
    public C59502uq A00;
    public final C90914Ps A01;

    public C59972vg(View view) {
        super(view);
        this.A01 = new C90914Ps(C12990iw.A0R(view, R.id.bread_crumbs_list), this);
    }

    @Override // X.AbstractC116555Vx
    public void ARZ(AnonymousClass3F6 r2) {
        this.A00.A00.ARZ(r2);
    }

    @Override // X.AbstractC116555Vx
    public void AVV() {
        this.A00.A00.AVV();
    }
}
