package X;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import com.whatsapp.util.Log;

/* renamed from: X.1Dx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26551Dx {
    public C44791zY A00;
    public C44931zn A01;
    public C44941zo A02;
    public String A03;
    public final AbstractC15710nm A04;
    public final C18790t3 A05;
    public final C15820nx A06;
    public final C22730zY A07;
    public final AnonymousClass10N A08;
    public final AnonymousClass10J A09;
    public final AnonymousClass10K A0A;
    public final C27051Fv A0B;
    public final AbstractC44761zV A0C = new C44831zc(this);
    public final AbstractC44761zV A0D = new C44821zb(this);
    public final C19380u1 A0E;
    public final C18640sm A0F;
    public final C15810nw A0G;
    public final C16590pI A0H;
    public final C15890o4 A0I;
    public final C14820m6 A0J;
    public final C14850m9 A0K;
    public final C19930uu A0L;
    public final AbstractC14440lR A0M;
    public final Object A0N = new Object();

    public C26551Dx(AbstractC15710nm r2, C18790t3 r3, C15820nx r4, C22730zY r5, AnonymousClass10N r6, AnonymousClass10J r7, AnonymousClass10K r8, C27051Fv r9, C19380u1 r10, C18640sm r11, C15810nw r12, C16590pI r13, C15890o4 r14, C14820m6 r15, C14850m9 r16, C19930uu r17, AbstractC14440lR r18) {
        this.A0H = r13;
        this.A0K = r16;
        this.A04 = r2;
        this.A0L = r17;
        this.A0M = r18;
        this.A05 = r3;
        this.A0G = r12;
        this.A0E = r10;
        this.A06 = r4;
        this.A0B = r9;
        this.A0I = r14;
        this.A0J = r15;
        this.A0F = r11;
        this.A07 = r5;
        this.A08 = r6;
        this.A0A = r8;
        this.A09 = r7;
    }

    public C44791zY A00() {
        C44791zY r0;
        synchronized (this.A0N) {
            r0 = this.A00;
        }
        return r0;
    }

    public C44791zY A01(String str, String str2) {
        C44791zY r3;
        synchronized (this.A0N) {
            Context context = this.A0H.A00;
            C14850m9 r12 = this.A0K;
            AbstractC15710nm r5 = this.A04;
            C19930uu r13 = this.A0L;
            C18790t3 r6 = this.A05;
            C15810nw r10 = this.A0G;
            r3 = new C44791zY(context, r5, r6, this.A06, this.A0B, this.A0F, r10, this.A0I, r12, r13, this.A0M, str, str2);
            this.A00 = r3;
        }
        return r3;
    }

    public void A02() {
        synchronized (this.A0N) {
            C44791zY r1 = this.A00;
            if (r1 != null) {
                synchronized (r1) {
                    Log.i("gdrive-api-v2/cancel");
                    r1.A01 = true;
                    r1.A09(false);
                }
            }
        }
        this.A0E.A00(2, false);
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:26:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A03() {
        /*
            r6 = this;
            java.lang.String r0 = "gdrive-service/cancel-pending-backup-and-restore-if-any"
            com.whatsapp.util.Log.i(r0)
            X.0m6 r4 = r6.A0J
            boolean r0 = X.C44771zW.A0G(r4)
            java.lang.String r2 = "gdrive-service/drive-api/null"
            r3 = 0
            if (r0 != 0) goto L_0x00a8
            X.0zY r1 = r6.A07
            java.util.concurrent.atomic.AtomicBoolean r0 = r1.A0c
            boolean r0 = r0.get()
            if (r0 != 0) goto L_0x00a8
            boolean r0 = X.C44771zW.A0H(r4)
            if (r0 == 0) goto L_0x007f
            java.util.concurrent.atomic.AtomicBoolean r0 = r1.A0e
            r0.getAndSet(r3)
            android.os.ConditionVariable r0 = r1.A0G
            r0.open()
            X.1zY r0 = r6.A00()
            if (r0 == 0) goto L_0x0065
            java.lang.String r0 = "gdrive-service/cancel-media-restore/interrupt-drive-api"
            com.whatsapp.util.Log.i(r0)
            r6.A02()
            android.os.ConditionVariable r0 = r1.A0F
            r0.open()
            android.os.ConditionVariable r0 = r1.A0C
            r0.open()
            X.10N r0 = r6.A08
            r0.A06()
        L_0x0047:
            r4.A0V(r3)
        L_0x004a:
            r0 = 10
            r6.A05(r0)
            android.content.SharedPreferences r2 = r4.A00
            java.lang.String r1 = "gdrive_user_initiated_backup"
            boolean r0 = r2.getBoolean(r1, r3)
            if (r0 == 0) goto L_0x0064
            android.content.SharedPreferences$Editor r0 = r2.edit()
            android.content.SharedPreferences$Editor r0 = r0.putBoolean(r1, r3)
            r0.apply()
        L_0x0064:
            return
        L_0x0065:
            com.whatsapp.util.Log.i(r2)
            android.os.ConditionVariable r0 = r1.A0F
            r0.open()
            android.os.ConditionVariable r0 = r1.A0C
            r0.open()
            X.0lR r2 = r6.A0M
            r1 = 26
            com.facebook.redex.RunnableBRunnable0Shape2S0100000_I0_2 r0 = new com.facebook.redex.RunnableBRunnable0Shape2S0100000_I0_2
            r0.<init>(r6, r1)
            r2.Ab2(r0)
            goto L_0x004a
        L_0x007f:
            boolean r0 = X.C44771zW.A0I(r4)
            if (r0 == 0) goto L_0x00a2
            java.util.concurrent.atomic.AtomicBoolean r0 = r1.A0f
            r0.getAndSet(r3)
            android.os.ConditionVariable r0 = r1.A0G
            r0.open()
            r6.A02()
            android.os.ConditionVariable r0 = r1.A0E
            r0.open()
            android.os.ConditionVariable r0 = r1.A0B
            r0.open()
            X.10K r0 = r6.A0A
            r0.A03()
            goto L_0x0047
        L_0x00a2:
            java.lang.String r0 = "gdrive-service/cancel/nothing-to-cancel"
            com.whatsapp.util.Log.i(r0)
            goto L_0x004a
        L_0x00a8:
            X.0zY r5 = r6.A07
            java.util.concurrent.atomic.AtomicBoolean r0 = r5.A0c
            r0.getAndSet(r3)
            X.C44921zm.A01()
            android.os.ConditionVariable r0 = r5.A0G
            r0.open()
            X.1zY r0 = r6.A00()
            if (r0 == 0) goto L_0x00d8
            java.lang.String r0 = "gdrive-service/cancel-backup/interrupt-drive-api"
            com.whatsapp.util.Log.i(r0)
            r6.A02()
            android.os.ConditionVariable r0 = r5.A0D
            r0.open()
            android.os.ConditionVariable r0 = r5.A0A
            r0.open()
        L_0x00cf:
            X.10K r0 = r6.A0A
            r0.A03()
            r5.A04 = r3
            goto L_0x0047
        L_0x00d8:
            com.whatsapp.util.Log.i(r2)
            android.os.ConditionVariable r0 = r5.A0D
            r0.open()
            android.os.ConditionVariable r0 = r5.A0A
            r0.open()
            X.0lR r2 = r6.A0M
            r1 = 25
            com.facebook.redex.RunnableBRunnable0Shape2S0100000_I0_2 r0 = new com.facebook.redex.RunnableBRunnable0Shape2S0100000_I0_2
            r0.<init>(r6, r1)
            r2.Ab2(r0)
            goto L_0x00cf
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C26551Dx.A03():void");
    }

    public void A04() {
        synchronized (this.A0N) {
            this.A01 = null;
        }
    }

    public void A05(int i) {
        String str;
        C44941zo r1;
        C44931zn r12;
        String A04 = C44771zW.A04(i);
        if (i != 10) {
            TextUtils.join("\n", Thread.currentThread().getStackTrace());
            StringBuilder sb = new StringBuilder("gdrive-service/set-error/");
            sb.append(A04);
            Log.e(sb.toString());
        }
        C14820m6 r6 = this.A0J;
        r6.A0S(i);
        Object obj = this.A0N;
        synchronized (obj) {
            str = this.A03;
        }
        if (C44771zW.A0H(r6) || "action_restore_media".equals(str)) {
            AnonymousClass10N r13 = this.A08;
            Bundle A00 = this.A09.A00();
            for (AnonymousClass10L r0 : r13.A01()) {
                r0.APx(i, A00);
            }
            synchronized (obj) {
                r1 = this.A02;
            }
            if (r1 != null) {
                r1.A09 = Integer.valueOf(C44771zW.A00(i));
            }
        } else if (C44771zW.A0I(r6) || "action_restore".equals(str)) {
            AnonymousClass10N r14 = this.A08;
            Bundle A002 = this.A09.A00();
            for (AnonymousClass10L r02 : r14.A01()) {
                r02.APy(i, A002);
            }
        } else {
            if (C44771zW.A0G(r6) || "action_backup".equals(str)) {
                synchronized (obj) {
                    r12 = this.A01;
                }
                if (r12 != null && r12.A0D == null) {
                    r12.A0D = Integer.valueOf(C44771zW.A00(i));
                }
            } else if (str != null) {
                if (i != 10) {
                    StringBuilder sb2 = new StringBuilder("gdrive-service/set-error/unexpected-service-start-action/");
                    sb2.append(str);
                    Log.e(sb2.toString());
                    return;
                }
                return;
            } else if (i != 10) {
                Log.e("gdrive-service/set-error/unexpected-service-start-action/null", new Throwable());
                return;
            } else {
                Log.i("gdrive-service/set-error/action-is-null and nothing is pending (probably backup attempt failed)");
            }
            this.A08.A07(i, this.A09.A00());
        }
    }
}
