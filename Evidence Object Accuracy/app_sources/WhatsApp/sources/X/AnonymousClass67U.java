package X;

import com.whatsapp.payments.ui.NoviSelfieCameraView;
import java.io.File;

/* renamed from: X.67U  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass67U implements AnonymousClass2AQ {
    public final /* synthetic */ NoviSelfieCameraView A00;

    @Override // X.AnonymousClass2AQ
    public void onShutter() {
    }

    public AnonymousClass67U(NoviSelfieCameraView noviSelfieCameraView) {
        this.A00 = noviSelfieCameraView;
    }

    @Override // X.AnonymousClass2AQ
    public void ATk(byte[] bArr, boolean z) {
        NoviSelfieCameraView noviSelfieCameraView = this.A00;
        AbstractC14440lR r2 = noviSelfieCameraView.A03;
        File file = noviSelfieCameraView.A04;
        AnonymousClass009.A05(file);
        r2.Ab5(new C124125oe(noviSelfieCameraView, file, bArr), new Void[0]);
    }
}
