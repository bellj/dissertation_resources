package X;

import android.content.Context;
import android.hardware.biometrics.BiometricManager;

/* renamed from: X.0XW  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0XW implements AbstractC11660ge {
    public final Context A00;

    public AnonymousClass0XW(Context context) {
        this.A00 = context.getApplicationContext();
    }

    @Override // X.AbstractC11660ge
    public BiometricManager AAq() {
        return C06280Sw.A01(this.A00);
    }
}
