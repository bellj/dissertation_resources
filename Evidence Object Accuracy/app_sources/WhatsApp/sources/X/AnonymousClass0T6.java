package X;

import android.view.PointerIcon;
import android.view.View;

/* renamed from: X.0T6  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0T6 {
    public static void A00(PointerIcon pointerIcon, View view) {
        view.setPointerIcon(pointerIcon);
    }

    public static void A01(View view) {
        view.dispatchFinishTemporaryDetach();
    }

    public static void A02(View view) {
        view.dispatchStartTemporaryDetach();
    }
}
