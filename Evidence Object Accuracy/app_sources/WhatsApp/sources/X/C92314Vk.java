package X;

/* renamed from: X.4Vk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C92314Vk {
    public int A00 = -1;
    public boolean A01;
    public final C92864Xs A02 = new C92864Xs();
    public final C95304dT A03 = new C95304dT(new byte[65025], 0);

    public boolean A00(AnonymousClass5Yf r10) {
        C92864Xs r6;
        int i;
        C95314dV.A04(C12960it.A1W(r10));
        if (this.A01) {
            this.A01 = false;
            this.A03.A0Q(0);
        }
        while (!this.A01) {
            int i2 = this.A00;
            if (i2 < 0) {
                C92864Xs r7 = this.A02;
                if (!r7.A00(r10, -1) || !r7.A01(r10, true)) {
                    return false;
                }
                int i3 = r7.A01;
                if ((r7.A03 & 1) == 1 && this.A03.A00 == 0) {
                    int i4 = 0;
                    int i5 = 0;
                    while (0 + i5 < r7.A02) {
                        i5++;
                        int i6 = r7.A06[i5 + 0];
                        i4 += i6;
                        if (i6 != 255) {
                            break;
                        }
                    }
                    i3 += i4;
                    i2 = i5 + 0;
                } else {
                    i2 = 0;
                }
                r10.Ae3(i3);
                this.A00 = i2;
            }
            int i7 = 0;
            int i8 = 0;
            do {
                int i9 = i2 + i8;
                r6 = this.A02;
                if (i9 >= r6.A02) {
                    break;
                }
                i8++;
                i = r6.A06[i8 + i2];
                i7 += i;
            } while (i == 255);
            int i10 = i2 + i8;
            if (i7 > 0) {
                C95304dT r2 = this.A03;
                r2.A0P(r2.A00 + i7);
                r10.readFully(r2.A02, r2.A00, i7);
                r2.A0R(r2.A00 + i7);
                this.A01 = C12980iv.A1V(r6.A06[i10 - 1], 255);
            }
            if (i10 == r6.A02) {
                i10 = -1;
            }
            this.A00 = i10;
        }
        return true;
    }
}
