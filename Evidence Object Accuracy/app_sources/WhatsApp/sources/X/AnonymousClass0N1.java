package X;

import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Set;

/* renamed from: X.0N1  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0N1 {
    public final AnonymousClass0QN A00;
    public final Set A01 = Collections.newSetFromMap(new IdentityHashMap());

    public AnonymousClass0N1(AnonymousClass0QN r2) {
        this.A00 = r2;
    }
}
