package X;

import android.view.ViewGroup;
import android.view.animation.Animation;

/* renamed from: X.3xC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83413xC extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ ViewGroup A00;
    public final /* synthetic */ AnonymousClass35V A01;

    public C83413xC(ViewGroup viewGroup, AnonymousClass35V r2) {
        this.A01 = r2;
        this.A00 = viewGroup;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        this.A01.A00 = null;
        this.A00.setVisibility(8);
    }
}
