package X;

import java.util.Iterator;

/* renamed from: X.1WQ  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1WQ implements AnonymousClass1WO {
    public final AnonymousClass1J7 A00;
    public final AnonymousClass1WO A01;
    public final boolean A02;

    public AnonymousClass1WQ(AnonymousClass1J7 r1, AnonymousClass1WO r2, boolean z) {
        this.A01 = r2;
        this.A02 = z;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass1WO
    public Iterator iterator() {
        return new AnonymousClass1WR(this);
    }
}
