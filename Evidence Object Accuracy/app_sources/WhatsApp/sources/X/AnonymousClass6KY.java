package X;

import android.graphics.Matrix;
import android.graphics.Rect;
import java.util.concurrent.Callable;

/* renamed from: X.6KY  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6KY implements Callable {
    public final /* synthetic */ Rect A00;
    public final /* synthetic */ AnonymousClass662 A01;

    public AnonymousClass6KY(Rect rect, AnonymousClass662 r2) {
        this.A01 = r2;
        this.A00 = rect;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        Rect rect = this.A00;
        float[] fArr = {(float) rect.centerX(), (float) rect.centerY()};
        AnonymousClass662 r2 = this.A01;
        if (r2.A04 != null) {
            Matrix matrix = new Matrix();
            r2.A04.invert(matrix);
            matrix.mapPoints(fArr);
        }
        r2.A0V.A03(rect, r2.A06, r2.A0j, r2.A0A, fArr, r2.A0K);
        return null;
    }
}
