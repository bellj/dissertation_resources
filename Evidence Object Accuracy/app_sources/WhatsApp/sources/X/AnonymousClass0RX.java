package X;

import android.os.Build;
import android.view.accessibility.AccessibilityNodeInfo;

/* renamed from: X.0RX  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0RX {
    public final Object A00;

    public AnonymousClass0RX(Object obj) {
        this.A00 = obj;
    }

    public static AnonymousClass0RX A00(float f, float f2, float f3, int i) {
        AccessibilityNodeInfo.RangeInfo rangeInfo;
        if (Build.VERSION.SDK_INT >= 19) {
            rangeInfo = AccessibilityNodeInfo.RangeInfo.obtain(i, f, f2, f3);
        } else {
            rangeInfo = null;
        }
        return new AnonymousClass0RX(rangeInfo);
    }
}
