package X;

import android.content.DialogInterface;
import com.facebook.redex.IDxDListenerShape14S0100000_3_I1;
import com.whatsapp.R;
import com.whatsapp.authentication.FingerprintBottomSheet;
import com.whatsapp.jid.UserJid;
import com.whatsapp.payments.ui.BrazilPaymentActivity;
import com.whatsapp.payments.ui.widget.PaymentView;
import java.security.Signature;
import java.util.concurrent.TimeUnit;

/* renamed from: X.66p */
/* loaded from: classes4.dex */
public class C1323666p implements AnonymousClass21K, AbstractC1311361k {
    public final /* synthetic */ AnonymousClass21K A00;
    public final /* synthetic */ C119425dx A01;

    @Override // X.AnonymousClass21K
    public /* synthetic */ void AMg(Signature signature) {
    }

    public C1323666p(AnonymousClass21K r1, C119425dx r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public void A01() {
        C119425dx r1 = this.A01;
        r1.A01.A1B();
        C004802e A0S = C12980iv.A0S(r1.A00);
        A0S.A07(R.string.payments_biometric_invalidated_key_title);
        A0S.A06(R.string.payments_biometric_invalidated_key_error);
        C117295Zj.A0q(A0S, this, 2, R.string.ok);
        A0S.A0B(false);
        A0S.A05();
    }

    @Override // X.AbstractC136506Mu
    public void AKZ(C452120p r2) {
        this.A01.A05.AKZ(r2);
        APo(r2);
    }

    @Override // X.AbstractC136506Mu
    public void AKb() {
        this.A01.A05.AKb();
    }

    @Override // X.AbstractC136506Mu
    public void AKk() {
        this.A01.A05.AKk();
    }

    @Override // X.AbstractC136506Mu
    public void AKl() {
        this.A01.A05.AKl();
    }

    @Override // X.AnonymousClass21K
    public void AMb(int i, CharSequence charSequence) {
        if (i == 7) {
            TimeUnit timeUnit = TimeUnit.MILLISECONDS;
            C119425dx r5 = this.A01;
            C14830m7 r6 = r5.A02;
            long seconds = timeUnit.toSeconds(r6.A00()) + 30;
            r5.A04.A02(seconds);
            long j = (seconds * 1000) + 500;
            if (j > r6.A00()) {
                r5.A01.A1L(j);
            }
        }
        this.A00.AMb(i, charSequence);
    }

    @Override // X.AnonymousClass21K
    public void AMc() {
        this.A00.AMc();
    }

    @Override // X.AnonymousClass21K
    public void AMe(int i, CharSequence charSequence) {
        this.A00.AMe(i, charSequence);
    }

    @Override // X.AnonymousClass21K
    public void AMf(byte[] bArr) {
        C119425dx r2 = this.A01;
        r2.A01.A0A = true;
        r2.A00.A2C(R.string.payment_verifying);
    }

    @Override // X.AbstractC1311361k
    public void APo(C452120p r13) {
        AnonymousClass04S A00;
        C32631cT A002;
        C119425dx r2 = this.A01;
        r2.A00.AaN();
        FingerprintBottomSheet fingerprintBottomSheet = r2.A01;
        fingerprintBottomSheet.A0A = false;
        int i = r13.A00;
        if (i == 1441) {
            fingerprintBottomSheet.A1L(r13.A02 * 1000);
            return;
        }
        C133326Ai r22 = r2.A05;
        BrazilPaymentActivity brazilPaymentActivity = r22.A04;
        brazilPaymentActivity.A0N.A03(brazilPaymentActivity.A00, "error_code", (long) i);
        brazilPaymentActivity.A0N.A05(r13, "br_get_provider_key_tag");
        int i2 = r13.A00;
        if (i2 == 454) {
            AnonymousClass1V8 r0 = r13.A05;
            if (!(r0 == null || (A002 = C32631cT.A00(r0)) == null)) {
                ((AbstractActivityC121685jC) brazilPaymentActivity).A0D.A04(AnonymousClass4EV.A00(((AbstractActivityC121685jC) brazilPaymentActivity).A07, A002));
            }
            BrazilPaymentActivity.A02(r22.A00, brazilPaymentActivity);
            return;
        }
        if (i2 == 2896003 || i2 == 2896004) {
            AnonymousClass61I.A03(AnonymousClass61I.A00(((ActivityC13790kL) brazilPaymentActivity).A05, null, ((AbstractActivityC121685jC) brazilPaymentActivity).A0U, null, false), brazilPaymentActivity.A0K, "incentive_unavailable", "payment_confirm_prompt");
            A00 = brazilPaymentActivity.A08.A00(brazilPaymentActivity, null, new DialogInterface.OnDismissListener(r22.A00, r22) { // from class: X.62y
                public final /* synthetic */ FingerprintBottomSheet A00;
                public final /* synthetic */ C133326Ai A01;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // android.content.DialogInterface.OnDismissListener
                public final void onDismiss(DialogInterface dialogInterface) {
                    C133326Ai r02 = this.A01;
                    FingerprintBottomSheet fingerprintBottomSheet2 = this.A00;
                    BrazilPaymentActivity brazilPaymentActivity2 = r02.A04;
                    ((AbstractActivityC121685jC) brazilPaymentActivity2).A01 = 7;
                    brazilPaymentActivity2.A2m(null);
                    if (fingerprintBottomSheet2.A0c()) {
                        fingerprintBottomSheet2.A1C();
                    }
                }
            }, null, null, r13.A00);
        } else {
            if (i2 == 444 || i2 == 478) {
                brazilPaymentActivity.A0J.A01.A01("FB", "PIN");
            }
            AnonymousClass69D r5 = brazilPaymentActivity.A08;
            int i3 = r13.A00;
            C15610nY r3 = brazilPaymentActivity.A03;
            C20830wO r1 = ((AbstractActivityC121685jC) brazilPaymentActivity).A08;
            UserJid userJid = ((AbstractActivityC121685jC) brazilPaymentActivity).A0G;
            AnonymousClass009.A05(userJid);
            String A04 = r3.A04(r1.A01(userJid));
            FingerprintBottomSheet fingerprintBottomSheet2 = r22.A00;
            A00 = r5.A00(brazilPaymentActivity, new DialogInterface.OnDismissListener(fingerprintBottomSheet2, r22) { // from class: X.62x
                public final /* synthetic */ FingerprintBottomSheet A00;
                public final /* synthetic */ C133326Ai A01;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // android.content.DialogInterface.OnDismissListener
                public final void onDismiss(DialogInterface dialogInterface) {
                    C133326Ai r02 = this.A01;
                    FingerprintBottomSheet fingerprintBottomSheet3 = this.A00;
                    PaymentView paymentView = r02.A04.A0V;
                    if (paymentView != null) {
                        paymentView.A0t.setText((CharSequence) null);
                    }
                    if (fingerprintBottomSheet3.A0c()) {
                        fingerprintBottomSheet3.A1C();
                    }
                }
            }, new IDxDListenerShape14S0100000_3_I1(fingerprintBottomSheet2, 7), new DialogInterface.OnDismissListener(fingerprintBottomSheet2, r22) { // from class: X.62w
                public final /* synthetic */ FingerprintBottomSheet A00;
                public final /* synthetic */ C133326Ai A01;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // android.content.DialogInterface.OnDismissListener
                public final void onDismiss(DialogInterface dialogInterface) {
                    C133326Ai r02 = this.A01;
                    FingerprintBottomSheet fingerprintBottomSheet3 = this.A00;
                    AnonymousClass6B7 A01 = r02.A04.A0J.A01("FB", "PIN", false);
                    boolean A0c = fingerprintBottomSheet3.A0c();
                    if (A01 != null) {
                        if (A0c) {
                            fingerprintBottomSheet3.A1J();
                        }
                    } else if (A0c) {
                        fingerprintBottomSheet3.A1C();
                    }
                }
            }, A04, i3);
        }
        A00.show();
    }

    @Override // X.AbstractC1311361k
    public void AVJ(String str) {
        C119425dx r3 = this.A01;
        r3.A00.AaN();
        FingerprintBottomSheet fingerprintBottomSheet = r3.A01;
        fingerprintBottomSheet.A1G(false);
        fingerprintBottomSheet.A03.setEnabled(false);
        fingerprintBottomSheet.A02.setEnabled(false);
        C133326Ai r32 = r3.A05;
        BrazilPaymentActivity brazilPaymentActivity = r32.A04;
        brazilPaymentActivity.A0N.A06("br_get_provider_key_tag", 2);
        String str2 = r32.A05;
        C30821Yy r5 = r32.A01;
        AbstractC28901Pl r6 = r32.A02;
        String str3 = r32.A06;
        int i = 1;
        if (brazilPaymentActivity.A2q(r5, ((AbstractActivityC121685jC) brazilPaymentActivity).A01) == null) {
            i = 0;
        }
        brazilPaymentActivity.A2u(r5, r6, r32.A03, str2, str, str3, i);
        this.A00.AMf(null);
    }
}
