package X;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.ActionProvider;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewDebug;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import java.util.ArrayList;

/* renamed from: X.0Xp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C07340Xp implements AbstractMenuItemC017808h {
    public char A00;
    public char A01;
    public int A02 = 16;
    public int A03 = 0;
    public int A04 = 4096;
    public int A05 = 4096;
    public int A06 = 0;
    public Intent A07;
    public ColorStateList A08 = null;
    public PorterDuff.Mode A09 = null;
    public Drawable A0A;
    public MenuItem.OnActionExpandListener A0B;
    public MenuItem.OnMenuItemClickListener A0C;
    public View A0D;
    public AnonymousClass07H A0E;
    public AnonymousClass0CK A0F;
    public AbstractC04730Mv A0G;
    public CharSequence A0H;
    public CharSequence A0I;
    public CharSequence A0J;
    public CharSequence A0K;
    public boolean A0L = false;
    public boolean A0M = false;
    public boolean A0N = false;
    public boolean A0O = false;
    public final int A0P;
    public final int A0Q;
    public final int A0R;
    public final int A0S;

    public C07340Xp(AnonymousClass07H r3, CharSequence charSequence, int i, int i2, int i3, int i4, int i5) {
        this.A0E = r3;
        this.A0R = i2;
        this.A0Q = i;
        this.A0P = i3;
        this.A0S = i4;
        this.A0I = charSequence;
        this.A06 = i5;
    }

    public void A00(View view) {
        int i;
        this.A0D = view;
        this.A0G = null;
        if (view != null && view.getId() == -1 && (i = this.A0R) > 0) {
            view.setId(i);
        }
        AnonymousClass07H r1 = this.A0E;
        r1.A0D = true;
        r1.A0E(true);
    }

    public boolean A01() {
        if ((this.A06 & 8) == 0) {
            return false;
        }
        if (this.A0D == null) {
            AbstractC04730Mv r0 = this.A0G;
            if (r0 == null) {
                return false;
            }
            View onCreateActionView = ((AnonymousClass0DX) r0).A00.onCreateActionView(this);
            this.A0D = onCreateActionView;
            if (onCreateActionView == null) {
                return false;
            }
        }
        return true;
    }

    @Override // X.AbstractMenuItemC017808h
    public AbstractC04730Mv AGz() {
        return this.A0G;
    }

    @Override // X.AbstractMenuItemC017808h
    public AbstractMenuItemC017808h Abw(CharSequence charSequence) {
        this.A0H = charSequence;
        this.A0E.A0E(false);
        return this;
    }

    @Override // X.AbstractMenuItemC017808h
    public AbstractMenuItemC017808h Acx(AbstractC04730Mv r3) {
        AbstractC04730Mv r1 = this.A0G;
        if (r1 != null) {
            r1.A00 = null;
        }
        this.A0D = null;
        this.A0G = r3;
        this.A0E.A0E(true);
        AbstractC04730Mv r12 = this.A0G;
        if (r12 != null) {
            AnonymousClass0CL r13 = (AnonymousClass0CL) r12;
            r13.A00 = new C07430Xy(this);
            ((AnonymousClass0DX) r13).A00.setVisibilityListener(r13);
        }
        return this;
    }

    @Override // X.AbstractMenuItemC017808h
    public AbstractMenuItemC017808h Ad2(CharSequence charSequence) {
        this.A0K = charSequence;
        this.A0E.A0E(false);
        return this;
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public boolean collapseActionView() {
        if ((this.A06 & 8) != 0) {
            if (this.A0D == null) {
                return true;
            }
            MenuItem.OnActionExpandListener onActionExpandListener = this.A0B;
            if (onActionExpandListener == null || onActionExpandListener.onMenuItemActionCollapse(this)) {
                return this.A0E.A0L(this);
            }
        }
        return false;
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public boolean expandActionView() {
        MenuItem.OnActionExpandListener onActionExpandListener;
        if (!A01() || ((onActionExpandListener = this.A0B) != null && !onActionExpandListener.onMenuItemActionExpand(this))) {
            return false;
        }
        return this.A0E.A0M(this);
    }

    @Override // android.view.MenuItem
    public ActionProvider getActionProvider() {
        throw new UnsupportedOperationException("This is not supported, use MenuItemCompat.getActionProvider()");
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public View getActionView() {
        View view = this.A0D;
        if (view != null) {
            return view;
        }
        AbstractC04730Mv r0 = this.A0G;
        if (r0 == null) {
            return null;
        }
        View onCreateActionView = ((AnonymousClass0DX) r0).A00.onCreateActionView(this);
        this.A0D = onCreateActionView;
        return onCreateActionView;
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public int getAlphabeticModifiers() {
        return this.A04;
    }

    @Override // android.view.MenuItem
    public char getAlphabeticShortcut() {
        return this.A00;
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public CharSequence getContentDescription() {
        return this.A0H;
    }

    @Override // android.view.MenuItem
    public int getGroupId() {
        return this.A0Q;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0019, code lost:
        if (r1 != null) goto L_0x001b;
     */
    @Override // android.view.MenuItem
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.drawable.Drawable getIcon() {
        /*
            r3 = this;
            android.graphics.drawable.Drawable r1 = r3.A0A
            if (r1 != 0) goto L_0x001b
            int r2 = r3.A03
            if (r2 == 0) goto L_0x0045
            X.07H r0 = r3.A0E
            android.content.Context r1 = r0.A0N
            X.05t r0 = X.C012005t.A01()
            android.graphics.drawable.Drawable r1 = r0.A04(r1, r2)
            r0 = 0
            r3.A03 = r0
            r3.A0A = r1
            if (r1 == 0) goto L_0x0044
        L_0x001b:
            boolean r0 = r3.A0O
            if (r0 == 0) goto L_0x0044
            boolean r0 = r3.A0L
            if (r0 != 0) goto L_0x0027
            boolean r0 = r3.A0M
            if (r0 == 0) goto L_0x0044
        L_0x0027:
            android.graphics.drawable.Drawable r0 = X.C015607k.A03(r1)
            android.graphics.drawable.Drawable r1 = r0.mutate()
            boolean r0 = r3.A0L
            if (r0 == 0) goto L_0x0038
            android.content.res.ColorStateList r0 = r3.A08
            X.C015607k.A04(r0, r1)
        L_0x0038:
            boolean r0 = r3.A0M
            if (r0 == 0) goto L_0x0041
            android.graphics.PorterDuff$Mode r0 = r3.A09
            X.C015607k.A07(r0, r1)
        L_0x0041:
            r0 = 0
            r3.A0O = r0
        L_0x0044:
            return r1
        L_0x0045:
            r1 = 0
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C07340Xp.getIcon():android.graphics.drawable.Drawable");
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public ColorStateList getIconTintList() {
        return this.A08;
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public PorterDuff.Mode getIconTintMode() {
        return this.A09;
    }

    @Override // android.view.MenuItem
    public Intent getIntent() {
        return this.A07;
    }

    @Override // android.view.MenuItem
    @ViewDebug.CapturedViewProperty
    public int getItemId() {
        return this.A0R;
    }

    @Override // android.view.MenuItem
    public ContextMenu.ContextMenuInfo getMenuInfo() {
        return null;
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public int getNumericModifiers() {
        return this.A05;
    }

    @Override // android.view.MenuItem
    public char getNumericShortcut() {
        return this.A01;
    }

    @Override // android.view.MenuItem
    public int getOrder() {
        return this.A0P;
    }

    @Override // android.view.MenuItem
    public SubMenu getSubMenu() {
        return this.A0F;
    }

    @Override // android.view.MenuItem
    @ViewDebug.CapturedViewProperty
    public CharSequence getTitle() {
        return this.A0I;
    }

    @Override // android.view.MenuItem
    public CharSequence getTitleCondensed() {
        CharSequence charSequence = this.A0J;
        if (charSequence == null) {
            charSequence = this.A0I;
        }
        return (Build.VERSION.SDK_INT >= 18 || charSequence == null || (charSequence instanceof String)) ? charSequence : charSequence.toString();
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public CharSequence getTooltipText() {
        return this.A0K;
    }

    @Override // android.view.MenuItem
    public boolean hasSubMenu() {
        return this.A0F != null;
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public boolean isActionViewExpanded() {
        return this.A0N;
    }

    @Override // android.view.MenuItem
    public boolean isCheckable() {
        return (this.A02 & 1) == 1;
    }

    @Override // android.view.MenuItem
    public boolean isChecked() {
        return (this.A02 & 2) == 2;
    }

    @Override // android.view.MenuItem
    public boolean isEnabled() {
        return (this.A02 & 16) != 0;
    }

    @Override // android.view.MenuItem
    public boolean isVisible() {
        AbstractC04730Mv r0 = this.A0G;
        if (r0 == null || !((AnonymousClass0DX) r0).A00.overridesItemVisibility()) {
            if ((this.A02 & 8) == 0) {
                return true;
            }
            return false;
        } else if ((this.A02 & 8) != 0 || !((AnonymousClass0DX) this.A0G).A00.isVisible()) {
            return false;
        } else {
            return true;
        }
    }

    @Override // android.view.MenuItem
    public MenuItem setActionProvider(ActionProvider actionProvider) {
        throw new UnsupportedOperationException("This is not supported, use MenuItemCompat.setActionProvider()");
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public /* bridge */ /* synthetic */ MenuItem setActionView(int i) {
        Context context = this.A0E.A0N;
        A00(LayoutInflater.from(context).inflate(i, (ViewGroup) new LinearLayout(context), false));
        return this;
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public /* bridge */ /* synthetic */ MenuItem setActionView(View view) {
        A00(view);
        return this;
    }

    @Override // android.view.MenuItem
    public MenuItem setAlphabeticShortcut(char c) {
        if (this.A00 != c) {
            this.A00 = Character.toLowerCase(c);
            this.A0E.A0E(false);
        }
        return this;
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public MenuItem setAlphabeticShortcut(char c, int i) {
        if (this.A00 == c && this.A04 == i) {
            return this;
        }
        this.A00 = Character.toLowerCase(c);
        this.A04 = KeyEvent.normalizeMetaState(i);
        this.A0E.A0E(false);
        return this;
    }

    @Override // android.view.MenuItem
    public MenuItem setCheckable(boolean z) {
        int i = this.A02;
        int i2 = (z ? 1 : 0) | (i & -2);
        this.A02 = i2;
        if (i != i2) {
            this.A0E.A0E(false);
        }
        return this;
    }

    @Override // android.view.MenuItem
    public MenuItem setChecked(boolean z) {
        int i = this.A02;
        if ((i & 4) != 0) {
            AnonymousClass07H r9 = this.A0E;
            int groupId = getGroupId();
            ArrayList arrayList = r9.A07;
            int size = arrayList.size();
            r9.A07();
            for (int i2 = 0; i2 < size; i2++) {
                C07340Xp r10 = (C07340Xp) arrayList.get(i2);
                if (r10.getGroupId() == groupId && (r10.A02 & 4) != 0 && r10.isCheckable()) {
                    boolean z2 = false;
                    if (r10 == this) {
                        z2 = true;
                    }
                    int i3 = r10.A02;
                    int i4 = i3 & -3;
                    int i5 = 0;
                    if (z2) {
                        i5 = 2;
                    }
                    int i6 = i5 | i4;
                    r10.A02 = i6;
                    if (i3 != i6) {
                        r10.A0E.A0E(false);
                    }
                }
            }
            r9.A06();
        } else {
            int i7 = i & -3;
            int i8 = 0;
            if (z) {
                i8 = 2;
            }
            int i9 = i8 | i7;
            this.A02 = i9;
            if (i != i9) {
                this.A0E.A0E(false);
                return this;
            }
        }
        return this;
    }

    @Override // android.view.MenuItem
    public /* bridge */ /* synthetic */ MenuItem setContentDescription(CharSequence charSequence) {
        Abw(charSequence);
        return this;
    }

    @Override // android.view.MenuItem
    public MenuItem setEnabled(boolean z) {
        int i;
        int i2 = this.A02;
        if (z) {
            i = i2 | 16;
        } else {
            i = i2 & -17;
        }
        this.A02 = i;
        this.A0E.A0E(false);
        return this;
    }

    @Override // android.view.MenuItem
    public MenuItem setIcon(int i) {
        this.A0A = null;
        this.A03 = i;
        this.A0O = true;
        this.A0E.A0E(false);
        return this;
    }

    @Override // android.view.MenuItem
    public MenuItem setIcon(Drawable drawable) {
        this.A03 = 0;
        this.A0A = drawable;
        this.A0O = true;
        this.A0E.A0E(false);
        return this;
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public MenuItem setIconTintList(ColorStateList colorStateList) {
        this.A08 = colorStateList;
        this.A0L = true;
        this.A0O = true;
        this.A0E.A0E(false);
        return this;
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public MenuItem setIconTintMode(PorterDuff.Mode mode) {
        this.A09 = mode;
        this.A0M = true;
        this.A0O = true;
        this.A0E.A0E(false);
        return this;
    }

    @Override // android.view.MenuItem
    public MenuItem setIntent(Intent intent) {
        this.A07 = intent;
        return this;
    }

    @Override // android.view.MenuItem
    public MenuItem setNumericShortcut(char c) {
        if (this.A01 != c) {
            this.A01 = c;
            this.A0E.A0E(false);
        }
        return this;
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public MenuItem setNumericShortcut(char c, int i) {
        if (this.A01 == c && this.A05 == i) {
            return this;
        }
        this.A01 = c;
        this.A05 = KeyEvent.normalizeMetaState(i);
        this.A0E.A0E(false);
        return this;
    }

    @Override // android.view.MenuItem
    public MenuItem setOnActionExpandListener(MenuItem.OnActionExpandListener onActionExpandListener) {
        this.A0B = onActionExpandListener;
        return this;
    }

    @Override // android.view.MenuItem
    public MenuItem setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        this.A0C = onMenuItemClickListener;
        return this;
    }

    @Override // android.view.MenuItem
    public MenuItem setShortcut(char c, char c2) {
        this.A01 = c;
        this.A00 = Character.toLowerCase(c2);
        this.A0E.A0E(false);
        return this;
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public MenuItem setShortcut(char c, char c2, int i, int i2) {
        this.A01 = c;
        this.A05 = KeyEvent.normalizeMetaState(i);
        this.A00 = Character.toLowerCase(c2);
        this.A04 = KeyEvent.normalizeMetaState(i2);
        this.A0E.A0E(false);
        return this;
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public void setShowAsAction(int i) {
        int i2 = i & 3;
        if (i2 == 0 || i2 == 1 || i2 == 2) {
            this.A06 = i;
            AnonymousClass07H r1 = this.A0E;
            r1.A0D = true;
            r1.A0E(true);
            return;
        }
        throw new IllegalArgumentException("SHOW_AS_ACTION_ALWAYS, SHOW_AS_ACTION_IF_ROOM, and SHOW_AS_ACTION_NEVER are mutually exclusive.");
    }

    @Override // X.AbstractMenuItemC017808h, android.view.MenuItem
    public /* bridge */ /* synthetic */ MenuItem setShowAsActionFlags(int i) {
        setShowAsAction(i);
        return this;
    }

    @Override // android.view.MenuItem
    public MenuItem setTitle(int i) {
        setTitle(this.A0E.A0N.getString(i));
        return this;
    }

    @Override // android.view.MenuItem
    public MenuItem setTitle(CharSequence charSequence) {
        this.A0I = charSequence;
        this.A0E.A0E(false);
        AnonymousClass0CK r0 = this.A0F;
        if (r0 != null) {
            r0.setHeaderTitle(charSequence);
        }
        return this;
    }

    @Override // android.view.MenuItem
    public MenuItem setTitleCondensed(CharSequence charSequence) {
        this.A0J = charSequence;
        this.A0E.A0E(false);
        return this;
    }

    @Override // android.view.MenuItem
    public /* bridge */ /* synthetic */ MenuItem setTooltipText(CharSequence charSequence) {
        Ad2(charSequence);
        return this;
    }

    @Override // android.view.MenuItem
    public MenuItem setVisible(boolean z) {
        int i = this.A02;
        int i2 = i & -9;
        int i3 = 8;
        if (z) {
            i3 = 0;
        }
        int i4 = i3 | i2;
        this.A02 = i4;
        if (i != i4) {
            AnonymousClass07H r1 = this.A0E;
            r1.A0F = true;
            r1.A0E(true);
        }
        return this;
    }

    @Override // java.lang.Object
    public String toString() {
        CharSequence charSequence = this.A0I;
        if (charSequence != null) {
            return charSequence.toString();
        }
        return null;
    }
}
