package X;

/* renamed from: X.44Q  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass44Q extends C36071jH {
    public final /* synthetic */ C626838e A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass44Q(C15570nT r2, C15610nY r3, C626838e r4) {
        super(r2, r3, true);
        this.A00 = r4;
    }

    @Override // X.C36071jH
    public int A00(C15370n3 r2, C15370n3 r3) {
        if (r2.A0K == null) {
            if (r3.A0K != null) {
                return 1;
            }
        } else if (r3.A0K == null) {
            return -1;
        }
        return super.compare(r2, r3);
    }
}
