package X;

import com.whatsapp.jid.UserJid;
import java.util.Arrays;

/* renamed from: X.1zP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C44701zP {
    public final UserJid A00;
    public final String A01;

    public C44701zP(UserJid userJid, String str) {
        this.A01 = str;
        this.A00 = userJid;
    }

    public boolean equals(Object obj) {
        UserJid userJid;
        if (this != obj) {
            if (!(obj instanceof C44701zP)) {
                return false;
            }
            C44701zP r4 = (C44701zP) obj;
            if (!C29941Vi.A00(this.A01, r4.A01)) {
                return false;
            }
            UserJid userJid2 = this.A00;
            if (userJid2 != null && (userJid = r4.A00) != null && !userJid2.equals(userJid)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A01, this.A00});
    }
}
