package X;

/* renamed from: X.4Ba  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public enum EnumC87314Ba {
    A02(1),
    A06(2),
    A07(3),
    A05(4),
    A01(5),
    A03(6),
    A04(7);
    
    public final int value;

    EnumC87314Ba(int i) {
        this.value = i;
    }

    public static EnumC87314Ba A00(int i) {
        switch (i) {
            case 1:
                return A02;
            case 2:
                return A06;
            case 3:
                return A07;
            case 4:
                return A05;
            case 5:
                return A01;
            case 6:
                return A03;
            case 7:
                return A04;
            default:
                return null;
        }
    }
}
