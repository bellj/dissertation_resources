package X;

/* renamed from: X.4SQ  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4SQ {
    public final int A00;
    public final AnonymousClass4IH A01;
    public final AnonymousClass4TO A02;
    public final byte[] A03;
    public final AnonymousClass4II[] A04;

    public AnonymousClass4SQ(AnonymousClass4IH r1, AnonymousClass4TO r2, byte[] bArr, AnonymousClass4II[] r4, int i) {
        this.A02 = r2;
        this.A01 = r1;
        this.A03 = bArr;
        this.A04 = r4;
        this.A00 = i;
    }
}
