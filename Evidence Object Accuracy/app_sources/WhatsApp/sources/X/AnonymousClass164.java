package X;

import android.content.SharedPreferences;

/* renamed from: X.164  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass164 {
    public SharedPreferences A00;
    public final C16630pM A01;

    public AnonymousClass164(C16630pM r1) {
        this.A01 = r1;
    }

    public final synchronized SharedPreferences A00() {
        SharedPreferences sharedPreferences;
        sharedPreferences = this.A00;
        if (sharedPreferences == null) {
            sharedPreferences = this.A01.A01("tos_gating_prefs");
            this.A00 = sharedPreferences;
        }
        return sharedPreferences;
    }

    public void A01(String str, int i) {
        SharedPreferences.Editor edit = A00().edit();
        StringBuilder sb = new StringBuilder("tos_acceptance_state_");
        sb.append(str);
        edit.putInt(sb.toString(), i).apply();
    }

    public void A02(String str, long j) {
        SharedPreferences.Editor edit = A00().edit();
        StringBuilder sb = new StringBuilder("tos_last_refresh_timestamp_");
        sb.append(str);
        edit.putLong(sb.toString(), j).apply();
    }
}
