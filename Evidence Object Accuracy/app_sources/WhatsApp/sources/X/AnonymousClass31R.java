package X;

/* renamed from: X.31R  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass31R extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Boolean A02;
    public Boolean A03;
    public Integer A04;
    public Integer A05;
    public Long A06;
    public Long A07;
    public Long A08;
    public Long A09;
    public Long A0A;
    public Long A0B;
    public Long A0C;
    public Long A0D;
    public Long A0E;
    public Long A0F;
    public Long A0G;
    public Long A0H;
    public Long A0I;

    public AnonymousClass31R() {
        super(1038, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(24, this.A00);
        r3.Abe(25, this.A01);
        r3.Abe(16, this.A06);
        r3.Abe(22, this.A02);
        r3.Abe(4, this.A07);
        r3.Abe(10, this.A08);
        r3.Abe(3, this.A09);
        r3.Abe(11, this.A0A);
        r3.Abe(18, this.A0B);
        r3.Abe(26, this.A03);
        r3.Abe(14, this.A04);
        r3.Abe(2, this.A0C);
        r3.Abe(5, this.A0D);
        r3.Abe(12, this.A0E);
        r3.Abe(15, this.A0F);
        r3.Abe(13, this.A0G);
        r3.Abe(1, this.A05);
        r3.Abe(23, this.A0H);
        r3.Abe(17, this.A0I);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamMediaPicker {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "audienceSelectorClicked", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "audienceSelectorUpdated", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "chatRecipients", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "isViewOnce", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "mediaPickerChanged", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "mediaPickerCroppedRotated", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "mediaPickerDeleted", this.A09);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "mediaPickerDrawing", this.A0A);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "mediaPickerFilter", this.A0B);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "mediaPickerHasLocationSticker", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "mediaPickerOrigin", C12960it.A0Y(this.A04));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "mediaPickerSent", this.A0C);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "mediaPickerSentUnchanged", this.A0D);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "mediaPickerStickers", this.A0E);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "mediaPickerT", this.A0F);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "mediaPickerText", this.A0G);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "mediaType", C12960it.A0Y(this.A05));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "photoGalleryDurationT", this.A0H);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "statusRecipients", this.A0I);
        return C12960it.A0d("}", A0k);
    }
}
