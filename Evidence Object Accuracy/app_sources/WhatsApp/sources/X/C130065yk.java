package X;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.whatsapp.payments.ui.BrazilPayBloksActivity;
import java.util.HashMap;

/* renamed from: X.5yk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130065yk {
    public final C16590pI A00;
    public final C14820m6 A01;
    public final C14850m9 A02;
    public final C21860y6 A03;
    public final C18660so A04;
    public final C22710zW A05;
    public final C130015yf A06;

    public C130065yk(C16590pI r1, C14820m6 r2, C14850m9 r3, C21860y6 r4, C18660so r5, C22710zW r6, C130015yf r7) {
        this.A00 = r1;
        this.A01 = r2;
        this.A05 = r6;
        this.A03 = r4;
        this.A06 = r7;
        this.A04 = r5;
        this.A02 = r3;
    }

    public Intent A00(Context context, C30881Ze r5, String str) {
        Intent A0D = C12990iw.A0D(context, BrazilPayBloksActivity.class);
        A0D.putExtra("screen_params", A02(r5, str));
        A0D.putExtra("screen_name", "brpay_p_card_verify_options");
        A0D.putExtra("payment_method_credential_id", r5.A0A);
        return A0D;
    }

    public String A01() {
        C32641cU A00 = this.A03.A00();
        if (A00 == null) {
            return null;
        }
        String str = A00.A03;
        if (str.equals("tos_no_wallet")) {
            return "brpay_p_tos";
        }
        if (!this.A06.A03()) {
            return "brpay_p_pin_nux_create";
        }
        if (str.equals("add_card")) {
            return "brpay_p_compliance_kyc_next_screen_router";
        }
        return null;
    }

    public HashMap A02(C30881Ze r4, String str) {
        String str2;
        HashMap A11 = C12970iu.A11();
        A11.put("credential_id", r4.A0A);
        if (str != null) {
            A11.put("verify_methods", str);
        }
        A11.put("source", "pay_flow");
        A11.put("network_name", C30881Ze.A07(r4.A01));
        AbstractC30871Zd r1 = (AbstractC30871Zd) r4.A08;
        if (r1 != null && !TextUtils.isEmpty(r1.A0E)) {
            A11.put("card_image_url", r1.A0E);
        }
        A11.put("readable_name", C1311161i.A05(this.A00.A00, r4));
        if (r4.A08.A0A()) {
            str2 = "1";
        } else {
            str2 = "0";
        }
        A11.put("verified_state", str2);
        return A11;
    }

    public void A03(Intent intent, String str) {
        if (this.A02.A07(1519)) {
            AbstractActivityC119645em.A0O(intent, "onboarding_context", str);
        }
    }
}
