package X;

import java.util.Collections;
import java.util.List;

/* renamed from: X.0HB  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0HB extends AbstractC08110ab {
    public AnonymousClass0HB() {
        super(Collections.singletonList(new AnonymousClass0U8(new AnonymousClass0OM(1.0f, 1.0f))));
    }

    public AnonymousClass0HB(List list) {
        super(list);
    }

    @Override // X.AbstractC12590iA
    public AnonymousClass0QR A88() {
        return new AnonymousClass0Gx(this.A00);
    }
}
