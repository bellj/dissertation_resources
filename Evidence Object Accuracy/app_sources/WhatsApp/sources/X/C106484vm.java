package X;

import android.content.Context;

/* renamed from: X.4vm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106484vm implements AnonymousClass5SH {
    public int A00 = 0;
    public AbstractC117015Xu A01 = AbstractC117015Xu.A00;
    public final Context A02;

    public C106484vm(Context context) {
        this.A02 = context;
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x00e8  */
    @Override // X.AnonymousClass5SH
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AbstractC117055Yb[] A8T(android.os.Handler r26, X.AbstractC72373eU r27, X.AnonymousClass5SO r28, X.AnonymousClass5SS r29, X.AnonymousClass5XS r30) {
        /*
        // Method dump skipped, instructions count: 448
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C106484vm.A8T(android.os.Handler, X.3eU, X.5SO, X.5SS, X.5XS):X.5Yb[]");
    }
}
