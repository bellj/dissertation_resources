package X;

import com.whatsapp.util.Log;
import java.util.HashMap;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5z2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130245z2 {
    public static boolean A00(C126185sT r12, String str, String str2) {
        JSONObject A0a;
        try {
            JSONObject A05 = C13000ix.A05(str2);
            JSONArray jSONArray = A05.getJSONArray("url");
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject jSONObject = jSONArray.getJSONObject(i);
                String string = jSONObject.getString("regex");
                HashMap A11 = C12970iu.A11();
                Matcher matcher = Pattern.compile(string, 2).matcher(str);
                if (matcher.find()) {
                    int groupCount = matcher.groupCount();
                    if (groupCount >= 1) {
                        JSONArray jSONArray2 = jSONObject.getJSONArray("param");
                        int i2 = 0;
                        while (i2 < groupCount) {
                            i2++;
                            A11.put(jSONArray2.getJSONObject(i2).getString("name"), matcher.group(i2));
                        }
                    }
                    if (A05.has("opts")) {
                        A0a = A05.getJSONObject("opts");
                    } else {
                        A0a = C117295Zj.A0a();
                    }
                    if (jSONObject.has("opts")) {
                        JSONObject jSONObject2 = jSONObject.getJSONObject("opts");
                        Iterator<String> keys = jSONObject2.keys();
                        while (keys.hasNext()) {
                            String A0x = C12970iu.A0x(keys);
                            A0a.put(A0x, jSONObject2.get(A0x));
                        }
                    }
                    r12.A00 = new JSONObject(A11);
                    return true;
                }
            }
        } catch (JSONException e) {
            Log.e(e);
        }
        return false;
    }
}
