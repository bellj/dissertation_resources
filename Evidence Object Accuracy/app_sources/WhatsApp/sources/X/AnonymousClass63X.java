package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.63X  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass63X implements Parcelable {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(28);
    public final AbstractC28901Pl A00;
    public final C1316363n A01;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass63X(AbstractC28901Pl r1, C1316363n r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.A00, i);
        parcel.writeParcelable(this.A01, i);
    }
}
