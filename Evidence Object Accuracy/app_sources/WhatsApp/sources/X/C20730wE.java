package X;

import android.os.PowerManager;
import com.facebook.redex.RunnableBRunnable0Shape2S0200000_I0_2;
import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;

/* renamed from: X.0wE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20730wE {
    public final AbstractC15710nm A00;
    public final C15570nT A01;
    public final C20770wI A02;
    public final C20670w8 A03;
    public final C15550nR A04;
    public final AnonymousClass1DP A05;
    public final C20840wP A06;
    public final C18640sm A07;
    public final AnonymousClass01d A08;
    public final C14830m7 A09;
    public final C20820wN A0A;
    public final C18470sV A0B;
    public final C20790wK A0C;
    public final C14850m9 A0D;
    public final C20660w7 A0E;
    public final C14910mF A0F;
    public final C20780wJ A0G;
    public final AbstractC14440lR A0H;

    public C20730wE(AbstractC15710nm r2, C15570nT r3, C20770wI r4, C20670w8 r5, C15550nR r6, AnonymousClass1DP r7, C20840wP r8, C18640sm r9, AnonymousClass01d r10, C14830m7 r11, C20820wN r12, C18470sV r13, C20790wK r14, C14850m9 r15, C20660w7 r16, C14910mF r17, C20780wJ r18, AbstractC14440lR r19) {
        this.A09 = r11;
        this.A0D = r15;
        this.A00 = r2;
        this.A01 = r3;
        this.A0H = r19;
        this.A0E = r16;
        this.A0B = r13;
        this.A03 = r5;
        this.A04 = r6;
        this.A08 = r10;
        this.A0F = r17;
        this.A02 = r4;
        this.A0G = r18;
        this.A0C = r14;
        this.A0A = r12;
        this.A07 = r9;
        this.A05 = r7;
        this.A06 = r8;
    }

    public C42351v4 A00(AnonymousClass1JB r6, AnonymousClass1JA r7, Collection collection) {
        if (!this.A07.A0B()) {
            Log.i("contactsyncmethods/network_unavailable");
            return C42351v4.A04;
        }
        AnonymousClass009.A0E(r6.A00());
        boolean z = false;
        if (r7.scope == EnumC42711vh.MULTI_PROTOCOLS_QUERY) {
            z = true;
        }
        AnonymousClass009.A0E(z);
        collection.size();
        C42271uw r3 = new C42271uw(r7);
        r3.A02 = true;
        r3.A00 = r6;
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            AbstractC14640lm r1 = (AbstractC14640lm) it.next();
            this.A04.A0B(r1);
            if (r1 != null) {
                r3.A07.add(r1);
            }
        }
        try {
            return (C42351v4) A03(r3.A01(), false).get();
        } catch (InterruptedException | ExecutionException unused) {
            return C42351v4.A03;
        }
    }

    public C42351v4 A01(C42251uu r2) {
        if (!this.A07.A0B()) {
            Log.i("contactsyncmethods/network_unavailable");
            return C42351v4.A04;
        }
        try {
            return (C42351v4) A03(r2, false).get();
        } catch (InterruptedException | ExecutionException unused) {
            return C42351v4.A03;
        }
    }

    public AnonymousClass1VC A02(AnonymousClass1JB r5, AnonymousClass1JA r6, Collection collection, boolean z, boolean z2) {
        AnonymousClass009.A0E(r5.A00());
        boolean z3 = false;
        if (r6.scope == EnumC42711vh.MULTI_PROTOCOLS_QUERY) {
            z3 = true;
        }
        AnonymousClass009.A0E(z3);
        collection.size();
        C42271uw r3 = new C42271uw(r6);
        r3.A02 = z;
        r3.A00 = r5;
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            AbstractC14640lm r1 = (AbstractC14640lm) it.next();
            this.A04.A0B(r1);
            if (r1 != null) {
                r3.A07.add(r1);
            }
        }
        return A03(r3.A01(), z2);
    }

    public final AnonymousClass1VC A03(C42251uu r6, boolean z) {
        C42701vg r4 = new C42701vg(z);
        r6.A03.add(r4);
        AnonymousClass1DP r3 = this.A05;
        r3.A0R.execute(new RunnableBRunnable0Shape2S0200000_I0_2(r3, 45, r6));
        return r4;
    }

    public void A04() {
        C15570nT r1 = this.A01;
        r1.A08();
        if (r1.A00 != null) {
            r1.A08();
            this.A0H.Ab2(new RunnableBRunnable0Shape4S0100000_I0_4(this, 33));
        }
    }

    public void A05() {
        AnonymousClass1JA r0;
        this.A01.A08();
        if (this.A0F.A00 == 3) {
            r0 = AnonymousClass1JA.A02;
        } else {
            r0 = AnonymousClass1JA.A08;
        }
        C42271uw r1 = new C42271uw(r0);
        r1.A03 = true;
        r1.A04 = true;
        r1.A00 = AnonymousClass1JB.A09;
        A03(r1.A01(), true);
    }

    public void A06() {
        AnonymousClass1JA r0;
        this.A01.A08();
        if (this.A0F.A00 == 3) {
            r0 = AnonymousClass1JA.A02;
        } else {
            r0 = AnonymousClass1JA.A08;
        }
        C42271uw r2 = new C42271uw(r0);
        r2.A03 = true;
        r2.A04 = true;
        r2.A00 = AnonymousClass1JB.A09;
        r2.A02 = true;
        A03(r2.A01(), true);
    }

    public final void A07(AnonymousClass1JB r8, AnonymousClass1JA r9, boolean z) {
        PowerManager.WakeLock A00;
        Set emptySet = Collections.emptySet();
        PowerManager A0I = this.A08.A0I();
        if (A0I == null) {
            Log.w("contactsyncmethods/forceFullSync pm=null");
            A00 = null;
        } else {
            A00 = C39151pN.A00(A0I, "fullsync", 1);
        }
        if (A00 != null) {
            try {
                try {
                    A00.acquire();
                    Log.i("contactsyncmethods/forceFullSync/wl/acquire");
                } catch (RuntimeException e) {
                    Log.e("contactsyncmethods/forceFullSync", e);
                    this.A00.AaV("contactsyncmethods/forceFullSync", e.getMessage(), true);
                }
            } finally {
                if (A00 != null && A00.isHeld()) {
                    A00.release();
                    Log.i("contactsyncmethods/forceFullSync/wl/release");
                }
            }
        }
        C42271uw r1 = new C42271uw(r9);
        r1.A02 = true;
        r1.A01 = z;
        r1.A00 = r8;
        r1.A07.addAll(emptySet);
        A01(r1.A01());
    }

    public void A08(C15370n3 r4) {
        Jid A0B = r4.A0B(UserJid.class);
        if (A0B != null) {
            C15570nT r1 = this.A01;
            r1.A08();
            if (!A0B.equals(r1.A05) && !C15380n4.A0M(A0B) && r4.A0C == null) {
                r1.A08();
                if (!r4.A0e) {
                    C42271uw r12 = new C42271uw(AnonymousClass1JA.A09);
                    r12.A04 = true;
                    r12.A00 = AnonymousClass1JB.A09;
                    r12.A07.add(A0B);
                    A03(r12.A01(), true);
                }
            }
        }
    }

    public void A09(List list) {
        this.A01.A08();
        list.size();
        if (!list.isEmpty()) {
            HashSet hashSet = new HashSet();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                Jid A0B = ((C15370n3) it.next()).A0B(UserJid.class);
                if (A0B != null) {
                    hashSet.add(A0B);
                }
            }
            A02(AnonymousClass1JB.A09, AnonymousClass1JA.A07, hashSet, false, true);
        }
    }
}
