package X;

import android.graphics.drawable.ColorDrawable;
import android.view.View;
import com.whatsapp.R;
import java.util.Set;

/* renamed from: X.2i0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class View$OnClickListenerC55232i0 extends AnonymousClass03U implements View.OnClickListener, View.OnLongClickListener {
    public final int A00;
    public final ColorDrawable A01;
    public final AnonymousClass1s8 A02;
    public final AnonymousClass2T1 A03;
    public final C457522x A04;
    public final Set A05;

    public View$OnClickListenerC55232i0(AnonymousClass1s8 r3, AnonymousClass2T1 r4, C457522x r5, Set set) {
        super(r4);
        this.A03 = r4;
        this.A05 = set;
        this.A04 = r5;
        r4.setOnClickListener(this);
        r4.setOnLongClickListener(this);
        this.A02 = r3;
        int A00 = AnonymousClass00T.A00(r4.getContext(), R.color.camera_thumb);
        this.A00 = A00;
        this.A01 = new ColorDrawable(A00);
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        AnonymousClass2UD r0;
        AnonymousClass1s8 r3 = this.A02;
        AnonymousClass2T1 r2 = this.A03;
        C49632Lo r02 = r3.A0C;
        if (r02 != null && (r0 = r02.A03) != null && r0.A00 != null && !r3.A0Q()) {
            if (r3.A0v.isEmpty()) {
                r3.A0K(r2.A05, r2, false);
            } else {
                r3.A0J(r2.A05);
            }
        }
    }

    @Override // android.view.View.OnLongClickListener
    public boolean onLongClick(View view) {
        AnonymousClass2UD r0;
        AnonymousClass1s8 r2 = this.A02;
        AnonymousClass2T1 r1 = this.A03;
        C49632Lo r02 = r2.A0C;
        if (r02 == null || (r0 = r02.A03) == null || r0.A00 == null || r2.A0Q()) {
            return true;
        }
        r2.A0J(r1.A05);
        return true;
    }
}
