package X;

import android.content.SharedPreferences;
import android.database.Cursor;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

/* renamed from: X.1DK  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1DK implements AbstractC16990q5 {
    public final AnonymousClass15H A00;
    public final C255619w A01;
    public final AnonymousClass01H A02;

    @Override // X.AbstractC16990q5
    public /* synthetic */ void AOp() {
    }

    public AnonymousClass1DK(AnonymousClass15H r1, C255619w r2, AnonymousClass01H r3) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
    }

    @Override // X.AbstractC16990q5
    public void AOo() {
        int i;
        int i2;
        int i3;
        int i4;
        C002701f r15;
        File A02;
        C255619w r4 = this.A01;
        C255519v r0 = r4.A03;
        r0.A00();
        if (r0.A00) {
            C855343e r9 = new C855343e();
            SharedPreferences sharedPreferences = r4.A01.A00;
            r9.A02 = Long.valueOf((long) sharedPreferences.getInt("sticker_suggestion_triggered_count", 0));
            r9.A00 = Long.valueOf((long) sharedPreferences.getInt("sticker_suggestion_icon_clicked_count", 0));
            r9.A01 = Long.valueOf((long) sharedPreferences.getInt("sticker_suggestion_sticker_sent_count", 0));
            r9.A03 = sharedPreferences.getString("sticker_suggestion_num_suggestions_array", "[]");
            r4.A02.A07(r9);
            sharedPreferences.edit().putInt("sticker_suggestion_triggered_count", 0).putInt("sticker_suggestion_icon_clicked_count", 0).putInt("sticker_suggestion_sticker_sent_count", 0).putString("sticker_suggestion_num_suggestions_array", "[]").apply();
            r4.A00 = null;
        }
        AnonymousClass15H r02 = this.A00;
        ArrayList arrayList = new ArrayList();
        AnonymousClass165 r03 = r02.A01;
        Long valueOf = Long.valueOf((long) r03.A00().getInt("sticker_send_count", 0));
        arrayList.add(valueOf);
        Long valueOf2 = Long.valueOf((long) r03.A00().getInt("sticker_send_from_recent_count", 0));
        arrayList.add(valueOf2);
        Long valueOf3 = Long.valueOf((long) r03.A00().getInt("sticker_send_from_favorites_count", 0));
        arrayList.add(valueOf3);
        Long valueOf4 = Long.valueOf((long) r03.A00().getInt("sticker_send_from_pack_count", 0));
        arrayList.add(valueOf4);
        Long valueOf5 = Long.valueOf((long) r03.A00().getInt("sticker_send_from_emotion_count", 0));
        arrayList.add(valueOf5);
        Long valueOf6 = Long.valueOf((long) r03.A00().getInt("sticker_send_from_search_count", 0));
        arrayList.add(valueOf6);
        Long valueOf7 = Long.valueOf((long) r03.A00().getInt("sticker_send_from_forward_count", 0));
        arrayList.add(valueOf7);
        Long valueOf8 = Long.valueOf((long) r03.A00().getInt("sticker_send_first_party_count", 0));
        arrayList.add(valueOf8);
        Long valueOf9 = Long.valueOf((long) r03.A00().getInt("sticker_send_animated_count", 0));
        arrayList.add(valueOf9);
        Object obj = r03.A04;
        synchronized (obj) {
            i = r03.A00().getInt("sticker_picker_opened_count", 0);
        }
        Long valueOf10 = Long.valueOf((long) i);
        arrayList.add(valueOf10);
        synchronized (obj) {
            i2 = r03.A00().getInt("sticker_search_opened_count", 0);
        }
        Long valueOf11 = Long.valueOf((long) i2);
        arrayList.add(valueOf11);
        synchronized (r03.A02) {
            i3 = r03.A00().getInt("sticker_add_to_favorites_count", 0);
        }
        Long valueOf12 = Long.valueOf((long) i3);
        arrayList.add(valueOf12);
        synchronized (r03.A03) {
            i4 = r03.A00().getInt("sticker_pack_delete_count", 0);
        }
        Long valueOf13 = Long.valueOf((long) i4);
        arrayList.add(valueOf13);
        C856843t r1 = new C856843t();
        r1.A04 = valueOf;
        r1.A0B = valueOf2;
        r1.A09 = valueOf3;
        r1.A0A = valueOf4;
        r1.A08 = valueOf5;
        r1.A0C = valueOf6;
        r1.A05 = valueOf7;
        r1.A07 = valueOf8;
        r1.A06 = valueOf9;
        r1.A02 = valueOf10;
        r1.A03 = valueOf11;
        r1.A00 = valueOf12;
        r1.A01 = valueOf13;
        Iterator it = arrayList.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Number number = (Number) it.next();
            if (number != null && number.longValue() > 0) {
                r02.A00.A07(r1);
                r03.A00().edit().putInt("sticker_send_count", 0).putInt("sticker_send_from_recent_count", 0).putInt("sticker_send_from_favorites_count", 0).putInt("sticker_send_from_pack_count", 0).putInt("sticker_send_from_emotion_count", 0).putInt("sticker_send_from_search_count", 0).putInt("sticker_send_from_forward_count", 0).putInt("sticker_send_first_party_count", 0).putInt("sticker_send_animated_count", 0).putInt("sticker_picker_opened_count", 0).putInt("sticker_search_opened_count", 0).putInt("sticker_add_to_favorites_count", 0).putInt("sticker_pack_delete_count", 0).apply();
                break;
            }
        }
        AnonymousClass1DM r3 = (AnonymousClass1DM) this.A02.get();
        C28181Ma r5 = new C28181Ma("cleanUpOrphanInternalStickerFiles");
        r5.A03();
        C615230r r2 = new C615230r();
        HashSet hashSet = new HashSet();
        C16310on A01 = r3.A01.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT plaintext_hash FROM ( SELECT plain_file_hash as plaintext_hash FROM stickers UNION SELECT plaintext_hash as plaintext_hash FROM recent_stickers UNION SELECT plaintext_hash as plaintext_hash FROM starred_stickers )", null);
            while (A09.moveToNext()) {
                String string = A09.getString(0);
                if (string != null) {
                    hashSet.add(string);
                } else {
                    Log.e("InternalStickerFileReferenceManager/getAllInternalStickerPlainTextHashes/a sticker plaintextHash is null");
                }
            }
            A09.close();
            A01.close();
            for (AbstractC470728v r12 : r3.A03.A02()) {
                if (r12 instanceof C470828w) {
                    hashSet.add(((C470828w) r12).A00.A0C);
                }
            }
            r2.A04 = Long.valueOf(r5.A00());
            r5.A02("finished db query");
            try {
                r15 = r3.A00;
                A02 = r15.A02();
                AnonymousClass009.A05(A02);
            } catch (Exception e) {
                Log.e("InternalStickerFileReferenceManager/cleanUpOrphanInternalStickerFiles/exception", e);
                r2.A00 = 2;
                r2.A05 = e.getMessage();
            }
            if (A02.exists()) {
                HashSet hashSet2 = new HashSet(hashSet.size());
                Iterator it2 = hashSet.iterator();
                while (it2.hasNext()) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(((String) it2.next()).replace('/', '-'));
                    sb.append(".webp");
                    hashSet2.add(sb.toString());
                }
                String[] list = A02.list();
                r5.A02("finished string conversion");
                StringBuilder sb2 = new StringBuilder();
                sb2.append("cleanUpOrphanInternalStickerFiles/total file count: ");
                int length = list.length;
                sb2.append(length);
                Log.i(sb2.toString());
                long j = 0;
                long A00 = r5.A00();
                int i5 = 0;
                for (String str : list) {
                    if (!str.endsWith(".png") && !hashSet2.contains(str)) {
                        File file = new File(A02, str);
                        if (file.exists()) {
                            i5++;
                            j += file.length();
                            C14350lI.A0M(file);
                            C003001i r04 = r15.A04;
                            String absolutePath = file.getAbsolutePath();
                            C16310on A022 = r04.A01.A02();
                            A022.A03.A01("media_refs", "path = ?", new String[]{absolutePath});
                            A022.close();
                        }
                    }
                }
                r2.A03 = Long.valueOf(r5.A00() - A00);
                r2.A01 = Long.valueOf((long) i5);
                r2.A02 = Long.valueOf(j / 1024);
                r2.A00 = 1;
                r5.A02("finished orphan file deletion");
                StringBuilder sb3 = new StringBuilder();
                sb3.append("cleanUpOrphanInternalStickerFiles/total orphan file count: ");
                sb3.append(i5);
                Log.i(sb3.toString());
                r3.A02.A07(r2);
            }
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
