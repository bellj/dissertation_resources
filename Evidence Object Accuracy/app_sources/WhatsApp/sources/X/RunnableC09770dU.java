package X;

import android.view.View;

/* renamed from: X.0dU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09770dU implements Runnable {
    public final /* synthetic */ View A00;
    public final /* synthetic */ View A01;
    public final /* synthetic */ AnonymousClass0U5 A02;

    public RunnableC09770dU(View view, View view2, AnonymousClass0U5 r3) {
        this.A02 = r3;
        this.A01 = view;
        this.A00 = view2;
    }

    @Override // java.lang.Runnable
    public void run() {
        AnonymousClass0U5.A01(this.A02.A0M, this.A01, this.A00);
    }
}
