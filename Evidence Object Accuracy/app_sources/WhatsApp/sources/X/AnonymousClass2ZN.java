package X;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;

/* renamed from: X.2ZN  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2ZN extends Drawable {
    public final Paint A00;

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -3;
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
    }

    public AnonymousClass2ZN(int i) {
        Paint A0A = C12960it.A0A();
        this.A00 = A0A;
        if (A0A.getColor() != i) {
            A0A.setColor(i);
            invalidateSelf();
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        canvas.drawRect(getBounds(), this.A00);
    }
}
