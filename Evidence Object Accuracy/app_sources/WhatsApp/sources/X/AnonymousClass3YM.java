package X;

import android.animation.ValueAnimator;
import android.text.Layout;
import android.text.TextPaint;

/* renamed from: X.3YM  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3YM implements AnonymousClass290 {
    public AnonymousClass291 A00;
    public final ValueAnimator A01 = ValueAnimator.ofFloat(0.8f, 1.0f);
    public final AnonymousClass2Ab A02;
    public final AbstractC471028y A03;
    public final C91494Ry A04;
    public final int[] A05;

    public AnonymousClass3YM(AnonymousClass2Ab r2, AbstractC471028y r3, C91494Ry r4, int[] iArr) {
        this.A05 = iArr;
        this.A04 = r4;
        this.A03 = r3;
        this.A02 = r2;
    }

    @Override // X.AnonymousClass290
    public void AA0(TextPaint textPaint, CharSequence charSequence, int i) {
        int i2 = 25;
        int i3 = 50;
        while (true) {
            int i4 = i3 - i2;
            if (i4 > 1) {
                C91494Ry r2 = this.A04;
                float f = (float) ((i4 >> 1) + i2);
                r2.A00 = f;
                this.A03.setEntryTextSize(f);
                int i5 = (Layout.getDesiredWidth(charSequence, textPaint) > ((float) i) ? 1 : (Layout.getDesiredWidth(charSequence, textPaint) == ((float) i) ? 0 : -1));
                float f2 = r2.A00;
                if (i5 >= 0) {
                    i3 = (int) f2;
                } else {
                    i2 = (int) f2;
                }
            } else {
                C91494Ry r0 = this.A04;
                float f3 = r0.A00 - 1.0f;
                r0.A00 = f3;
                this.A03.setEntryTextSize(f3 - 1.0f);
                return;
            }
        }
    }

    @Override // X.AnonymousClass291
    public void dismiss() {
        this.A00.dismiss();
    }
}
