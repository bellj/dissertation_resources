package X;

import androidx.core.view.inputmethod.EditorInfoCompat;

/* renamed from: X.4wv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107184wv implements AnonymousClass5X7 {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public long A09;
    public long A0A;
    public long A0B;
    public C100614mC A0C;
    public AnonymousClass5X6 A0D;
    public String A0E;
    public String A0F;
    public boolean A0G;
    public boolean A0H;
    public final C95054d0 A0I;
    public final C95304dT A0J;
    public final String A0K;

    @Override // X.AnonymousClass5X7
    public void AYo() {
    }

    public C107184wv(String str) {
        this.A0K = str;
        C95304dT A05 = C95304dT.A05(EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH);
        this.A0J = A05;
        byte[] bArr = A05.A02;
        this.A0I = new C95054d0(bArr, bArr.length);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:54:0x013f, code lost:
        if (r13.A0H == false) goto L_0x01ec;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x0218, code lost:
        throw new X.AnonymousClass496();
     */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x018f  */
    @Override // X.AnonymousClass5X7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A7a(X.C95304dT r14) {
        /*
        // Method dump skipped, instructions count: 573
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C107184wv.A7a(X.4dT):void");
    }

    @Override // X.AnonymousClass5X7
    public void A8b(AbstractC14070ko r2, C92824Xo r3) {
        r3.A03();
        this.A0D = C92824Xo.A00(r2, r3);
        this.A0F = r3.A02();
    }

    @Override // X.AnonymousClass5X7
    public void AYp(long j, int i) {
        this.A0B = j;
    }

    @Override // X.AnonymousClass5X7
    public void AbP() {
        this.A08 = 0;
        this.A0H = false;
    }
}
