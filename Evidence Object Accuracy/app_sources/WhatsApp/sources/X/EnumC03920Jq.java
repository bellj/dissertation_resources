package X;

/* renamed from: X.0Jq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public enum EnumC03920Jq {
    STATIC(0),
    ANIMATED(1),
    ANIMATED_WHILE_LOADING(2),
    ANIMATED_WHILE_LOADED(3),
    DISABLED(4);
    
    public final String value;

    EnumC03920Jq(int i) {
        this.value = r2;
    }

    public static EnumC03920Jq A00(String str) {
        EnumC03920Jq[] values = values();
        for (EnumC03920Jq r1 : values) {
            if (r1.toString().equals(str)) {
                return r1;
            }
        }
        StringBuilder sb = new StringBuilder("Error finding BackgroundMode enum value for: ");
        sb.append(str);
        C28691Op.A00("CdsOpenScreenConfig", sb.toString());
        return STATIC;
    }

    @Override // java.lang.Enum, java.lang.Object
    public String toString() {
        return this.value;
    }
}
