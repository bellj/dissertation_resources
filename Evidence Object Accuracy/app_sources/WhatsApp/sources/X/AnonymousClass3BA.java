package X;

import android.os.Bundle;

/* renamed from: X.3BA  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3BA {
    public final Bundle A00;

    public /* synthetic */ AnonymousClass3BA(Bundle bundle) {
        C65273Iw.A03("requestMessage", bundle);
        C65273Iw.A01(bundle, Bundle.class, "auxAttributes");
        C65273Iw.A01(bundle, Boolean.class, "useDebugKey");
        this.A00 = bundle;
    }
}
