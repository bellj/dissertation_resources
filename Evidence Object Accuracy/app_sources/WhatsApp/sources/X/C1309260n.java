package X;

import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.60n  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1309260n {
    public final List A00;

    public C1309260n(List list) {
        this.A00 = list;
    }

    public static int A00(AnonymousClass1V8 r3, List list, JSONObject jSONObject, int i) {
        int i2;
        try {
            if (r3 != null) {
                i2 = r3.A04("sequence");
            } else {
                if (jSONObject != null) {
                    i2 = jSONObject.getInt("sequence");
                }
            }
            if (i2 != 0) {
                return i2 != -1 ? i2 : i;
            }
            if (list.get(0) != null) {
                Log.e("sequence attribute is missing on backend side. or 0 by default");
            }
        } catch (AnonymousClass1V9 | JSONException e) {
            Log.e(C12960it.A0d(e.getMessage(), C12960it.A0k("sequence attribute is missing on backend side. ")));
            return i;
        }
    }

    public static C1309260n A01(AnonymousClass1V8 r5) {
        if (r5 == null) {
            return null;
        }
        List A0J = r5.A0J("instruction");
        ArrayList A0x = C12980iv.A0x(Collections.nCopies(A0J.size(), null));
        for (int i = 0; i < A0J.size(); i++) {
            AnonymousClass1V8 r2 = (AnonymousClass1V8) A0J.get(i);
            A0x.set(A00(r2, A0x, null, i), r2.A0H("text"));
        }
        return new C1309260n(A0x);
    }
}
