package X;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

/* renamed from: X.2bC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52542bC extends ViewGroup {
    public int A00;
    public int A01;
    public boolean A02 = false;

    public C52542bC(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, C50572Qb.A09, 0, 0);
        this.A01 = obtainStyledAttributes.getDimensionPixelSize(1, 0);
        this.A00 = obtainStyledAttributes.getDimensionPixelSize(0, 0);
        obtainStyledAttributes.recycle();
    }

    public int getItemSpacing() {
        return this.A00;
    }

    public int getLineSpacing() {
        return this.A01;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int paddingLeft;
        int paddingRight;
        int i5;
        int i6;
        if (getChildCount() != 0) {
            boolean z2 = true;
            if (AnonymousClass028.A05(this) == 1) {
                paddingLeft = getPaddingRight();
                paddingRight = getPaddingLeft();
            } else {
                z2 = false;
                paddingLeft = getPaddingLeft();
                paddingRight = getPaddingRight();
            }
            int paddingTop = getPaddingTop();
            int i7 = (i3 - i) - paddingRight;
            int i8 = paddingLeft;
            int i9 = paddingTop;
            for (int i10 = 0; i10 < getChildCount(); i10++) {
                View childAt = getChildAt(i10);
                if (childAt.getVisibility() != 8) {
                    ViewGroup.LayoutParams layoutParams = childAt.getLayoutParams();
                    if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
                        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
                        i6 = C05670Qm.A01(marginLayoutParams);
                        i5 = C05670Qm.A00(marginLayoutParams);
                    } else {
                        i5 = 0;
                        i6 = 0;
                    }
                    int measuredWidth = i8 + i6 + childAt.getMeasuredWidth();
                    if (!this.A02 && measuredWidth > i7) {
                        i9 = this.A01 + paddingTop;
                        i8 = paddingLeft;
                    }
                    int i11 = i8 + i6;
                    int measuredWidth2 = childAt.getMeasuredWidth() + i11;
                    paddingTop = childAt.getMeasuredHeight() + i9;
                    if (z2) {
                        i11 = i7 - measuredWidth2;
                        measuredWidth2 = (i7 - i8) - i6;
                    }
                    childAt.layout(i11, i9, measuredWidth2, paddingTop);
                    i8 += i6 + i5 + childAt.getMeasuredWidth() + this.A00;
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0090, code lost:
        if (r10 != 1073741824) goto L_0x0092;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0098, code lost:
        if (r8 != 1073741824) goto L_0x009a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001d, code lost:
        if (r10 == 1073741824) goto L_0x001f;
     */
    @Override // android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r19, int r20) {
        /*
            r18 = this;
            r13 = r19
            int r11 = android.view.View.MeasureSpec.getSize(r13)
            int r10 = android.view.View.MeasureSpec.getMode(r13)
            r12 = r20
            int r9 = android.view.View.MeasureSpec.getSize(r12)
            int r8 = android.view.View.MeasureSpec.getMode(r12)
            r0 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r10 == r0) goto L_0x001f
            r0 = 1073741824(0x40000000, float:2.0)
            r7 = 2147483647(0x7fffffff, float:NaN)
            if (r10 != r0) goto L_0x0020
        L_0x001f:
            r7 = r11
        L_0x0020:
            r6 = r18
            int r17 = r6.getPaddingLeft()
            int r5 = r6.getPaddingTop()
            int r0 = r6.getPaddingRight()
            int r7 = r7 - r0
            r1 = r5
            r4 = 0
            r3 = 0
        L_0x0032:
            int r0 = r6.getChildCount()
            if (r4 >= r0) goto L_0x008a
            android.view.View r15 = r6.getChildAt(r4)
            int r2 = r15.getVisibility()
            r0 = 8
            if (r2 == r0) goto L_0x0084
            r6.measureChild(r15, r13, r12)
            android.view.ViewGroup$LayoutParams r14 = r15.getLayoutParams()
            boolean r0 = r14 instanceof android.view.ViewGroup.MarginLayoutParams
            if (r0 == 0) goto L_0x0087
            android.view.ViewGroup$MarginLayoutParams r14 = (android.view.ViewGroup.MarginLayoutParams) r14
            int r2 = r14.leftMargin
            int r14 = r14.rightMargin
        L_0x0055:
            int r0 = r17 + r2
            int r16 = r15.getMeasuredWidth()
            int r0 = r0 + r16
            if (r0 <= r7) goto L_0x006a
            boolean r0 = r6.A02
            if (r0 != 0) goto L_0x006a
            int r17 = r6.getPaddingLeft()
            int r1 = r6.A01
            int r1 = r1 + r5
        L_0x006a:
            int r0 = r17 + r2
            int r5 = r15.getMeasuredWidth()
            int r0 = r0 + r5
            int r5 = r15.getMeasuredHeight()
            int r5 = r5 + r1
            if (r0 <= r3) goto L_0x0079
            r3 = r0
        L_0x0079:
            int r2 = r2 + r14
            int r0 = r15.getMeasuredWidth()
            int r2 = r2 + r0
            int r0 = r6.A00
            int r2 = r2 + r0
            int r17 = r17 + r2
        L_0x0084:
            int r4 = r4 + 1
            goto L_0x0032
        L_0x0087:
            r2 = 0
            r14 = 0
            goto L_0x0055
        L_0x008a:
            r0 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r10 == r0) goto L_0x00a4
            r0 = 1073741824(0x40000000, float:2.0)
            if (r10 == r0) goto L_0x00a8
        L_0x0092:
            r0 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r8 == r0) goto L_0x009e
            r0 = 1073741824(0x40000000, float:2.0)
            if (r8 == r0) goto L_0x00a2
        L_0x009a:
            r6.setMeasuredDimension(r3, r5)
            return
        L_0x009e:
            int r9 = java.lang.Math.min(r5, r9)
        L_0x00a2:
            r5 = r9
            goto L_0x009a
        L_0x00a4:
            int r11 = java.lang.Math.min(r3, r11)
        L_0x00a8:
            r3 = r11
            goto L_0x0092
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C52542bC.onMeasure(int, int):void");
    }

    public void setItemSpacing(int i) {
        this.A00 = i;
    }

    public void setLineSpacing(int i) {
        this.A01 = i;
    }

    public void setSingleLine(boolean z) {
        this.A02 = z;
    }
}
