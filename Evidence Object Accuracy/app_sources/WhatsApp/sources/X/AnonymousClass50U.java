package X;

/* renamed from: X.50U  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass50U implements AbstractC116395Vg {
    @Override // X.AbstractC116395Vg
    public final boolean Ags(Class cls) {
        return false;
    }

    @Override // X.AbstractC116395Vg
    public final AbstractC115245Qt Ah6(Class cls) {
        throw C12960it.A0U("This should never be called.");
    }
}
