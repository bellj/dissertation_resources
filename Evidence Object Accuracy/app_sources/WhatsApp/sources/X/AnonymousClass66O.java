package X;

import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.SystemClock;
import android.util.Log;
import java.io.FileDescriptor;
import java.io.RandomAccessFile;

/* renamed from: X.66O  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass66O implements AbstractC136426Mm {
    public MediaRecorder A00;
    public final MediaRecorder.OnErrorListener A01 = new AnonymousClass63Q(this);
    public final MediaRecorder.OnInfoListener A02 = new AnonymousClass63R(this);
    public final AbstractC136406Mk A03;
    public final boolean A04;

    public AnonymousClass66O(AbstractC136406Mk r2, boolean z) {
        this.A04 = z;
        this.A03 = r2;
    }

    public final void A00(CamcorderProfile camcorderProfile, FileDescriptor fileDescriptor, int i, boolean z) {
        MediaRecorder mediaRecorder = new MediaRecorder();
        this.A00 = mediaRecorder;
        AbstractC136406Mk r3 = this.A03;
        r3.AVw(mediaRecorder);
        boolean z2 = this.A04;
        MediaRecorder mediaRecorder2 = this.A00;
        if (z2) {
            mediaRecorder2.setAudioSource(5);
            this.A00.setProfile(camcorderProfile);
        } else {
            mediaRecorder2.setOutputFormat(camcorderProfile.fileFormat);
            this.A00.setVideoFrameRate(camcorderProfile.videoFrameRate);
            this.A00.setVideoSize(camcorderProfile.videoFrameWidth, camcorderProfile.videoFrameHeight);
            this.A00.setVideoEncodingBitRate(camcorderProfile.videoBitRate);
            this.A00.setVideoEncoder(camcorderProfile.videoCodec);
        }
        MediaRecorder mediaRecorder3 = this.A00;
        if (!z) {
            i = 0;
        }
        mediaRecorder3.setOrientationHint(i);
        this.A00.setOutputFile(fileDescriptor);
        this.A00.setOnInfoListener(this.A02);
        this.A00.setOnErrorListener(this.A01);
        this.A00.prepare();
        r3.AWK(this.A00);
        this.A00.start();
    }

    @Override // X.AbstractC136426Mm
    public AnonymousClass60Q AeL(CamcorderProfile camcorderProfile, AnonymousClass6LR r11, FileDescriptor fileDescriptor, int i, int i2, boolean z, boolean z2) {
        A00(camcorderProfile, null, i2, true);
        C129375xc r3 = new C129375xc(null, camcorderProfile.videoFrameWidth, camcorderProfile.videoFrameHeight, i2, i);
        r3.A01(AnonymousClass60Q.A0P, Long.valueOf(SystemClock.elapsedRealtime()));
        return new AnonymousClass60Q(r3);
    }

    @Override // X.AbstractC136426Mm
    public AnonymousClass60Q AeM(CamcorderProfile camcorderProfile, AnonymousClass6LR r4, String str, int i, int i2, boolean z, boolean z2) {
        A00(camcorderProfile, new RandomAccessFile(str, "rws").getFD(), i2, z);
        return C129375xc.A00(camcorderProfile, str, i2, i);
    }

    @Override // X.AbstractC136426Mm
    public void AeU() {
        MediaRecorder mediaRecorder = this.A00;
        if (mediaRecorder != null) {
            try {
                try {
                    mediaRecorder.stop();
                } catch (RuntimeException e) {
                    Log.e("SimpleVideoCapture", "stopVideoRecording", e);
                    throw C117315Zl.A0J(e);
                }
            } finally {
                this.A00.reset();
                this.A00.release();
                this.A00 = null;
            }
        }
    }
}
