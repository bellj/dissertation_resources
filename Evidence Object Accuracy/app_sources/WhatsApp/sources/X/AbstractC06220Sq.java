package X;

import android.graphics.Rect;
import android.view.View;

/* renamed from: X.0Sq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC06220Sq {
    public int A00 = Integer.MIN_VALUE;
    public final Rect A01 = new Rect();
    public final AnonymousClass02H A02;

    public abstract int A01();

    public abstract int A02();

    public abstract int A03();

    public abstract int A04();

    public abstract int A05();

    public abstract int A06();

    public abstract int A07();

    public abstract int A08(View view);

    public abstract int A09(View view);

    public abstract int A0A(View view);

    public abstract int A0B(View view);

    public abstract int A0C(View view);

    public abstract int A0D(View view);

    public abstract void A0E(int i);

    public /* synthetic */ AbstractC06220Sq(AnonymousClass02H r2) {
        this.A02 = r2;
    }

    public static AbstractC06220Sq A00(AnonymousClass02H r1, int i) {
        if (i == 0) {
            return new AnonymousClass0Ez(r1);
        }
        if (i == 1) {
            return new AnonymousClass0F0(r1);
        }
        throw new IllegalArgumentException("invalid orientation");
    }
}
