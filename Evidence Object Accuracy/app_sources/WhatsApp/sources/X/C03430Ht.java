package X;

import android.graphics.Matrix;

/* renamed from: X.0Ht  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C03430Ht extends AbstractC03240Ha implements AbstractC12130hQ {
    public Matrix A00;
    public C08910c3 A01;
    public C08910c3 A02;
    public C08910c3 A03;
    public C08910c3 A04;
    public String A05;

    @Override // X.AnonymousClass0OO
    public String A00() {
        return "image";
    }

    @Override // X.AbstractC12130hQ
    public void Ad4(Matrix matrix) {
        this.A00 = matrix;
    }
}
