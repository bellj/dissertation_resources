package X;

import android.hardware.camera2.CameraManager;
import android.os.Handler;
import java.util.concurrent.Callable;

/* renamed from: X.6Ke  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class CallableC135886Ke implements Callable {
    public final /* synthetic */ AnonymousClass662 A00;
    public final /* synthetic */ C117435Zx A01;
    public final /* synthetic */ String A02;

    public CallableC135886Ke(AnonymousClass662 r1, C117435Zx r2, String str) {
        this.A00 = r1;
        this.A02 = str;
        this.A01 = r2;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        CameraManager cameraManager = this.A00.A0N;
        String str = this.A02;
        C117435Zx r1 = this.A01;
        cameraManager.openCamera(str, r1, (Handler) null);
        return r1;
    }
}
