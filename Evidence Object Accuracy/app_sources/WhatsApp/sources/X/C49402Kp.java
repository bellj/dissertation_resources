package X;

import android.net.TrafficStats;
import android.os.Build;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.net.SocketFactory;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

/* renamed from: X.2Kp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C49402Kp {
    public static final Socket A05 = new Socket();
    public ThreadPoolExecutor A00;
    public SSLSocketFactory A01;
    public boolean A02;
    public final C18800t4 A03;
    public final AbstractC14440lR A04;

    public C49402Kp(C18800t4 r1, AbstractC14440lR r2) {
        this.A04 = r2;
        this.A03 = r1;
    }

    public final synchronized ThreadPoolExecutor A00() {
        ThreadPoolExecutor threadPoolExecutor;
        threadPoolExecutor = this.A00;
        if (threadPoolExecutor == null) {
            AbstractC14440lR r1 = this.A04;
            threadPoolExecutor = new AnonymousClass1ID((AnonymousClass168) r1, "happy-eyeball", new ArrayBlockingQueue(2), new AnonymousClass1I8("happy-eyeball", 1), TimeUnit.SECONDS, 2, 2, 30, false);
            this.A00 = threadPoolExecutor;
        }
        return threadPoolExecutor;
    }

    public final void A01(AnonymousClass2L7 r8, InetSocketAddress inetSocketAddress, int i, boolean z) {
        try {
            try {
                TrafficStats.setThreadStatsTag(1);
                SSLSocketFactory sSLSocketFactory = this.A01;
                Socket createSocket = SocketFactory.getDefault().createSocket();
                StringBuilder sb = new StringBuilder();
                sb.append("HappyEyeball");
                sb.append("/try_connect/");
                sb.append(inetSocketAddress.getAddress());
                sb.append(" (method? ");
                sb.append(z);
                sb.append(')');
                Log.i(sb.toString());
                createSocket.connect(inetSocketAddress, i);
                if (z) {
                    createSocket = sSLSocketFactory.createSocket(createSocket, inetSocketAddress.getHostName(), inetSocketAddress.getPort(), true);
                    ((SSLSocket) createSocket).startHandshake();
                }
                AnonymousClass2L2 r1 = new AnonymousClass2L2(createSocket);
                if (!r8.A00(r1.A00)) {
                    Log.i("HappyEyeball/closeSocket");
                    r1.A00();
                }
            } catch (IOException | ClassCastException e) {
                if (!(e instanceof ClassCastException) || Build.VERSION.SDK_INT == 26) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("HappyEyeball/connectAndCountDown/");
                    sb2.append(inetSocketAddress.getAddress());
                    sb2.append("/couldn't connect to ip");
                    Log.e(sb2.toString(), e);
                    synchronized (this) {
                        if (this.A02) {
                            r8.A00(A05);
                        } else {
                            this.A02 = true;
                        }
                    }
                } else {
                    throw ((ClassCastException) e);
                }
            }
        } finally {
            TrafficStats.clearThreadStatsTag();
        }
    }
}
