package X;

import android.text.TextUtils;
import java.util.Arrays;

/* renamed from: X.0p8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16500p8 {
    public static final String A00 = TextUtils.join(", ", Arrays.asList("sort_id", "from_me", "key_id", "status", "broadcast", "data", "timestamp", "media_url", "media_mime_type", "message_type", "media_size", "media_name", "media_caption", "media_hash", "media_duration", "origin", "latitude", "longitude", "thumb_image", "raw_data", "recipient_count", "participant_hash", "starred", "quoted_row_id", "mentioned_jids", "multicast_id", "edit_version", "receipt_server_timestamp", "media_enc_hash", "payment_transaction_id", "origination_flags", "preview_type", "received_timestamp", "_id", "text_data", "lookup_tables", "sender_jid_row_id", "sender_jid_raw_string", "chat_row_id", "future_message_type", "message_add_on_flags", "table_version"));
}
