package X;

import sun.misc.Unsafe;

/* renamed from: X.3sK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C80483sK extends AnonymousClass4YS {
    public C80483sK(Unsafe unsafe) {
        super(unsafe);
    }

    @Override // X.AnonymousClass4YS
    public final byte A01(Object obj, long j) {
        return this.A00.getByte(obj, j);
    }

    @Override // X.AnonymousClass4YS
    public final double A02(Object obj, long j) {
        return this.A00.getDouble(obj, j);
    }

    @Override // X.AnonymousClass4YS
    public final float A03(Object obj, long j) {
        return this.A00.getFloat(obj, j);
    }

    @Override // X.AnonymousClass4YS
    public final void A06(Object obj, long j, byte b) {
        this.A00.putByte(obj, j, b);
    }

    @Override // X.AnonymousClass4YS
    public final void A07(Object obj, long j, double d) {
        this.A00.putDouble(obj, j, d);
    }

    @Override // X.AnonymousClass4YS
    public final void A08(Object obj, long j, float f) {
        this.A00.putFloat(obj, j, f);
    }

    @Override // X.AnonymousClass4YS
    public final void A0B(Object obj, long j, boolean z) {
        this.A00.putBoolean(obj, j, z);
    }

    @Override // X.AnonymousClass4YS
    public final boolean A0C(Object obj, long j) {
        return this.A00.getBoolean(obj, j);
    }
}
