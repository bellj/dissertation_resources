package X;

import com.whatsapp.biz.BusinessProfileExtraFieldsActivity;

/* renamed from: X.53L  reason: invalid class name */
/* loaded from: classes3.dex */
public final /* synthetic */ class AnonymousClass53L implements AbstractC30111Wd {
    public final /* synthetic */ BusinessProfileExtraFieldsActivity A00;

    public /* synthetic */ AnonymousClass53L(BusinessProfileExtraFieldsActivity businessProfileExtraFieldsActivity) {
        this.A00 = businessProfileExtraFieldsActivity;
    }

    @Override // X.AbstractC30111Wd
    public final void ANP(C30141Wg r2) {
        AnonymousClass3IU r0;
        BusinessProfileExtraFieldsActivity businessProfileExtraFieldsActivity = this.A00;
        if (r2 != null && (r0 = businessProfileExtraFieldsActivity.A00) != null) {
            r0.A02(r2);
        }
    }
}
