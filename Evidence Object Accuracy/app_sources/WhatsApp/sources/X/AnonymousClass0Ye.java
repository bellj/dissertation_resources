package X;

import android.util.Log;
import androidx.biometric.BiometricFragment;
import com.whatsapp.R;
import java.util.concurrent.Executor;

/* renamed from: X.0Ye  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Ye implements AnonymousClass02B {
    public final /* synthetic */ BiometricFragment A00;

    public AnonymousClass0Ye(BiometricFragment biometricFragment) {
        this.A00 = biometricFragment;
    }

    @Override // X.AnonymousClass02B
    public /* bridge */ /* synthetic */ void ANq(Object obj) {
        if (((Boolean) obj).booleanValue()) {
            BiometricFragment biometricFragment = this.A00;
            if (biometricFragment.A1K()) {
                String A0I = biometricFragment.A0I(R.string.fingerprint_not_recognized);
                if (A0I == null) {
                    A0I = biometricFragment.A0I(R.string.default_error_msg);
                }
                biometricFragment.A01.A04(2);
                biometricFragment.A01.A05(A0I);
            }
            AnonymousClass0EP r1 = biometricFragment.A01;
            if (!r1.A0I) {
                Log.w("BiometricFragment", "Failure not sent to client. Client is not awaiting a result.");
            } else {
                Executor executor = r1.A0H;
                if (executor == null) {
                    executor = new ExecutorC10570eq();
                }
                executor.execute(new RunnableC09140cQ(biometricFragment));
            }
            AnonymousClass0EP r2 = biometricFragment.A01;
            AnonymousClass016 r12 = r2.A0D;
            if (r12 == null) {
                r12 = new AnonymousClass016();
                r2.A0D = r12;
            }
            AnonymousClass0EP.A00(r12, false);
        }
    }
}
