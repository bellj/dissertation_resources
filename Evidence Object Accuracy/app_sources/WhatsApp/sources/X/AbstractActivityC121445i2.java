package X;

import com.whatsapp.payments.ui.BrazilPaymentContactSupportActivity;

/* renamed from: X.5i2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC121445i2 extends AbstractActivityC119325dX {
    public boolean A00 = false;

    public AbstractActivityC121445i2() {
        C117295Zj.A0p(this, 17);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            BrazilPaymentContactSupportActivity brazilPaymentContactSupportActivity = (BrazilPaymentContactSupportActivity) this;
            AnonymousClass2FL r3 = (AnonymousClass2FL) ((AnonymousClass2FJ) generatedComponent());
            AnonymousClass01J A1M = ActivityC13830kP.A1M(r3, brazilPaymentContactSupportActivity);
            ActivityC13810kN.A10(A1M, brazilPaymentContactSupportActivity);
            ((ActivityC13790kL) brazilPaymentContactSupportActivity).A08 = ActivityC13790kL.A0S(r3, A1M, brazilPaymentContactSupportActivity, ActivityC13790kL.A0Y(A1M, brazilPaymentContactSupportActivity));
            brazilPaymentContactSupportActivity.A00 = (C123405n6) r3.A03.get();
        }
    }
}
