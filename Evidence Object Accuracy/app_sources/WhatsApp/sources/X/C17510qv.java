package X;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;
import com.whatsapp.jid.Jid;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

/* renamed from: X.0qv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C17510qv implements AbstractC17490qt {
    public final C14900mE A00;
    public final C16170oZ A01;
    public final C18010rl A02;
    public final C15650ng A03;
    public final AbstractC14440lR A04;

    public C17510qv(C14900mE r2, C16170oZ r3, C18010rl r4, C15650ng r5, AbstractC14440lR r6) {
        C16700pc.A0E(r3, 1);
        C16700pc.A0E(r6, 2);
        C16700pc.A0E(r5, 3);
        C16700pc.A0E(r2, 4);
        C16700pc.A0E(r4, 5);
        this.A01 = r3;
        this.A04 = r6;
        this.A03 = r5;
        this.A00 = r2;
        this.A02 = r4;
    }

    @Override // X.AbstractC17490qt
    public void AZB(Activity activity, C90184Mx r20, Map map) {
        Intent intent;
        Bundle extras;
        boolean z;
        if (activity != null && (intent = activity.getIntent()) != null && (extras = intent.getExtras()) != null) {
            String string = extras.getString("chat_id");
            AbstractC14640lm r7 = (AbstractC14640lm) Jid.getNullable(string);
            String string2 = extras.getString("message_id");
            long j = extras.getLong("message_row_id", 0);
            String string3 = extras.getString("action_name");
            if (!(r7 == null || string2 == null || string3 == null || map == null || !map.containsKey("extension_message_response"))) {
                Object obj = map.get("extension_message_response");
                if (obj == null) {
                    throw new NullPointerException("null cannot be cast to non-null type kotlin.collections.Map<*, *>");
                } else if (((Map) obj).containsKey("body")) {
                    Object obj2 = map.get("extension_message_response");
                    if (obj2 == null) {
                        throw new NullPointerException("null cannot be cast to non-null type kotlin.collections.Map<*, *>");
                    } else if (((Map) obj2).containsKey("params")) {
                        Object obj3 = map.get("extension_message_response");
                        if (obj3 != null) {
                            Map map2 = (Map) obj3;
                            Object obj4 = map2.get("body");
                            if (obj4 != null) {
                                String str = (String) obj4;
                                Object obj5 = map2.get("params");
                                if (obj5 != null) {
                                    Map map3 = (Map) obj5;
                                    if (map.containsKey("disable_cta")) {
                                        Object obj6 = map.get("disable_cta");
                                        if (obj6 != null) {
                                            z = ((Boolean) obj6).booleanValue();
                                        } else {
                                            throw new NullPointerException("null cannot be cast to non-null type kotlin.Boolean");
                                        }
                                    } else {
                                        z = true;
                                    }
                                    this.A01.A0H(r7, str, "galaxy_message", new JSONObject(map3).toString(), j);
                                    this.A04.Ab2(new Runnable(string2, string, string3, z) { // from class: X.2iz
                                        public final /* synthetic */ String A01;
                                        public final /* synthetic */ String A02;
                                        public final /* synthetic */ String A03;
                                        public final /* synthetic */ boolean A04;

                                        {
                                            this.A01 = r2;
                                            this.A02 = r3;
                                            this.A03 = r4;
                                            this.A04 = r5;
                                        }

                                        @Override // java.lang.Runnable
                                        public final void run() {
                                            AbstractC16390ow r0;
                                            C16470p4 ABf;
                                            AnonymousClass1Z7 r02;
                                            C17510qv r4 = C17510qv.this;
                                            String str2 = this.A01;
                                            String str3 = this.A02;
                                            String str4 = this.A03;
                                            boolean z2 = this.A04;
                                            C16700pc.A0E(r4, 0);
                                            String valueOf = String.valueOf(str2);
                                            C16700pc.A0C(str3);
                                            C16700pc.A0C(str4);
                                            AnonymousClass1IS r1 = new AnonymousClass1IS(AbstractC14640lm.A01(str3), valueOf, false);
                                            C15650ng r5 = r4.A03;
                                            AbstractC15340mz A03 = r5.A0K.A03(r1);
                                            Object obj7 = null;
                                            if ((A03 instanceof AbstractC16390ow) && (r0 = (AbstractC16390ow) A03) != null && (ABf = r0.ABf()) != null && ABf.A00 == 5 && (r02 = ABf.A03) != null) {
                                                List list = r02.A00;
                                                if (list != null) {
                                                    Iterator it = list.iterator();
                                                    while (true) {
                                                        if (!it.hasNext()) {
                                                            break;
                                                        }
                                                        Object next = it.next();
                                                        if (C16700pc.A0O(((AnonymousClass1Z9) next).A01.A00, str4)) {
                                                            obj7 = next;
                                                            break;
                                                        }
                                                    }
                                                    AnonymousClass1Z9 r3 = (AnonymousClass1Z9) obj7;
                                                    if (r3 != null) {
                                                        r3.A00 = z2;
                                                    }
                                                }
                                                r5.A0W(A03);
                                            }
                                        }
                                    });
                                    this.A00.A0H(new RunnableBRunnable0Shape8S0200000_I0_8(this, 45, r20));
                                    return;
                                }
                                throw new NullPointerException("null cannot be cast to non-null type kotlin.collections.Map<*, *>");
                            }
                            throw new NullPointerException("null cannot be cast to non-null type kotlin.String");
                        }
                        throw new NullPointerException("null cannot be cast to non-null type kotlin.collections.Map<*, *>");
                    }
                }
            }
            this.A00.A0H(new RunnableBRunnable0Shape8S0200000_I0_8(this, 46, r20));
        }
    }
}
