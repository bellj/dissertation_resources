package X;

import android.net.Uri;
import com.whatsapp.Mp4Ops;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Collections;
import java.util.Map;

/* renamed from: X.3Sw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67783Sw implements AnonymousClass2BW {
    public long A00 = 0;
    public AnonymousClass3IS A01;
    public FileInputStream A02;
    public boolean A03;
    public final AnonymousClass5Yg A04;
    public final AbstractC15710nm A05;
    public final Mp4Ops A06;
    public final C16590pI A07;
    public final File A08;

    @Override // X.AnonymousClass2BW
    public void A5p(AnonymousClass5QP r1) {
    }

    public C67783Sw(AbstractC15710nm r6, Mp4Ops mp4Ops, C16590pI r8, String str) {
        this.A07 = r8;
        this.A06 = mp4Ops;
        this.A05 = r6;
        C107884y6 r0 = new C107884y6(str);
        this.A04 = new C56132kO(r0.A02, r0.A03, r0.A00, r0.A01);
        this.A08 = new File(r8.A00.getExternalCacheDir(), C12990iw.A0n());
    }

    @Override // X.AnonymousClass2BW
    public /* synthetic */ Map AGF() {
        return Collections.emptyMap();
    }

    @Override // X.AnonymousClass2BW
    public Uri AHS() {
        return this.A04.AHS();
    }

    @Override // X.AnonymousClass2BW
    public long AYZ(AnonymousClass3H3 r25) {
        long j;
        long AYZ;
        AnonymousClass3H3 r5 = r25;
        long j2 = r5.A03;
        this.A00 = j2;
        if (this.A03) {
            File file = this.A08;
            long length = file.length();
            if (this.A00 < length) {
                FileInputStream fileInputStream = new FileInputStream(file);
                this.A02 = fileInputStream;
                fileInputStream.skip(this.A00);
                j = (length - this.A00) + 0;
                Uri uri = r5.A04;
                byte[] bArr = r5.A07;
                String str = r5.A05;
                int i = r5.A00;
                int i2 = 1;
                if (bArr != null) {
                    i2 = 2;
                }
                r5 = new AnonymousClass3H3(uri, str, Collections.emptyMap(), bArr, i2, i, length - length, length, -1);
                AYZ = j + this.A04.AYZ(r5);
                if (AYZ >= 0 && !this.A03) {
                    C16590pI r4 = this.A07;
                    this.A01 = new AnonymousClass3IS(this.A05, this.A06, r4, this.A08, AYZ);
                }
                return AYZ;
            }
        } else if (j2 != 0) {
            Uri uri2 = r5.A04;
            byte[] bArr2 = r5.A07;
            String str2 = r5.A05;
            int i3 = r5.A00;
            int i4 = 1;
            if (bArr2 != null) {
                i4 = 2;
            }
            r5 = new AnonymousClass3H3(uri2, str2, Collections.emptyMap(), bArr2, i4, i3, 0, 0, -1);
        }
        j = 0;
        AYZ = j + this.A04.AYZ(r5);
        if (AYZ >= 0) {
            C16590pI r4 = this.A07;
            this.A01 = new AnonymousClass3IS(this.A05, this.A06, r4, this.A08, AYZ);
        }
        return AYZ;
    }

    @Override // X.AnonymousClass2BW
    public void close() {
        this.A04.close();
        FileInputStream fileInputStream = this.A02;
        if (fileInputStream != null) {
            fileInputStream.close();
            this.A02 = null;
        }
        this.A00 = 0;
    }

    @Override // X.AnonymousClass2BY
    public int read(byte[] bArr, int i, int i2) {
        FileInputStream fileInputStream;
        if (this.A01 != null) {
            if (!this.A03) {
                byte[] bArr2 = new byte[256];
                File file = this.A08;
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                while (this.A01.A00 == 0) {
                    try {
                        fileOutputStream.write(bArr2, 0, this.A04.read(bArr2, 0, 256));
                        fileOutputStream.flush();
                        if (!this.A01.A03(file.length())) {
                            this.A03 = C12970iu.A1W(this.A01.A00);
                        }
                    } catch (Throwable th) {
                        try {
                            fileOutputStream.close();
                        } catch (Throwable unused) {
                        }
                        throw th;
                    }
                }
                if (this.A03) {
                    fileOutputStream.close();
                } else {
                    throw C12990iw.A0i("Mp4StreamCheckedDataSource/Mp4StreamCheck not successful");
                }
            }
            File file2 = this.A08;
            long length = file2.length();
            if (length <= 0) {
                throw C12990iw.A0i("Mp4StreamCheckedDataSource/videoHeadForStreamCheck is empty");
            } else if (this.A00 < length) {
                FileInputStream fileInputStream2 = this.A02;
                if (fileInputStream2 == null) {
                    fileInputStream2 = new FileInputStream(file2);
                    this.A02 = fileInputStream2;
                }
                int read = fileInputStream2.read(bArr, i, i2);
                long j = this.A00 + ((long) read);
                this.A00 = j;
                if (j >= length && (fileInputStream = this.A02) != null) {
                    fileInputStream.close();
                    this.A02 = null;
                }
                return read;
            } else {
                int read2 = this.A04.read(bArr, i, i2);
                this.A00 += (long) read2;
                return read2;
            }
        } else {
            throw C12990iw.A0i("Mp4StreamCheckedDataSource/Mp4StreamCheck not initialized");
        }
    }
}
