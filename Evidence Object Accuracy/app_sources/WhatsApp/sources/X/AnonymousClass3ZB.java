package X;

import android.os.Handler;
import com.facebook.redex.RunnableBRunnable0Shape15S0100000_I1_1;
import com.facebook.redex.RunnableBRunnable0Shape1S0101000_I1;
import com.facebook.redex.RunnableBRunnable0Shape1S1100000_I1;
import com.whatsapp.util.Log;
import java.lang.ref.WeakReference;

/* renamed from: X.3ZB  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3ZB implements AbstractC21730xt {
    public final Handler A00 = C12970iu.A0E();
    public final C17220qS A01;
    public final WeakReference A02;

    public AnonymousClass3ZB(AnonymousClass2B8 r2, C17220qS r3) {
        this.A02 = C12970iu.A10(r2);
        this.A01 = r3;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        Log.e("sendVerifyLinkRequest/delivery failure ");
        Object obj = this.A02.get();
        if (obj != null) {
            this.A00.post(new RunnableBRunnable0Shape15S0100000_I1_1(obj, 31));
        }
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r6, String str) {
        int A00 = C41151sz.A00(r6);
        Log.w(C12960it.A0W(A00, "sendVerifyLinkRequest/response-error "));
        Object obj = this.A02.get();
        if (obj != null) {
            this.A00.post(new RunnableBRunnable0Shape1S0101000_I1(obj, A00, 7));
        }
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r8, String str) {
        Handler handler;
        int i;
        int i2;
        String str2;
        AnonymousClass1V8 A0E = r8.A0E("response");
        Object obj = this.A02.get();
        if (A0E == null) {
            int A00 = C41151sz.A00(r8);
            if (obj != null) {
                this.A00.post(new RunnableBRunnable0Shape1S0101000_I1(obj, A00, 6));
            }
            Log.w(C12960it.A0f(C12960it.A0j("sendVerifyLinkRequest/response-error "), A00));
            return;
        }
        AnonymousClass1V8 A0E2 = A0E.A0E("status");
        if (A0E2 == null) {
            if (obj != null) {
                handler = this.A00;
                i = 29;
                handler.post(new RunnableBRunnable0Shape15S0100000_I1_1(obj, i));
            }
            Log.w("sendVerifyLinkRequest/response-error -1");
            return;
        }
        try {
            if (A0E2.A0G() != null) {
                i2 = Integer.parseInt(A0E2.A0G());
                if (i2 == 200) {
                    AnonymousClass1V8 A0E3 = A0E.A0E("url");
                    if (A0E3 == null) {
                        str2 = null;
                    } else {
                        str2 = A0E3.A0G();
                    }
                    if (obj != null) {
                        this.A00.post(new RunnableBRunnable0Shape1S1100000_I1(3, str2, obj));
                        return;
                    }
                    return;
                }
            } else {
                i2 = 0;
            }
            if (obj != null) {
                this.A00.post(new RunnableBRunnable0Shape1S0101000_I1(obj, i2, 5));
            }
            Log.w(C12960it.A0f(C12960it.A0j("sendVerifyLinkRequest/response-error "), i2));
        } catch (NumberFormatException unused) {
            if (obj != null) {
                handler = this.A00;
                i = 30;
            }
        }
    }
}
