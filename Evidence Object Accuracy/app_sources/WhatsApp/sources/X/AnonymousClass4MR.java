package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.4MR  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4MR {
    public final AnonymousClass3HZ A00;
    public final List A01;

    public AnonymousClass4MR(AnonymousClass3HZ r5) {
        this.A00 = r5;
        ArrayList A0l = C12960it.A0l();
        this.A01 = A0l;
        A0l.add(new C64403Fk(r5, new int[]{1}));
    }
}
