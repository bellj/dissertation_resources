package X;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.4bk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C94394bk {
    public static final AnonymousClass5H1 A0A = new AnonymousClass5H1();
    public int A00 = 0;
    public final C92364Vp A01;
    public final C92574Wl A02;
    public final Object A03;
    public final Object A04;
    public final Object A05;
    public final HashMap A06 = C12970iu.A11();
    public final List A07;
    public final boolean A08;
    public final boolean A09;

    public C94394bk(C92364Vp r3, C92574Wl r4, Object obj, boolean z) {
        C95094d8.A03(obj, "root can not be null");
        C95094d8.A03(r3, "configuration can not be null");
        this.A08 = z;
        this.A02 = r4;
        this.A04 = obj;
        this.A01 = r3;
        AnonymousClass4YL r1 = ((AnonymousClass52M) r3.A00).A01;
        this.A05 = r1.A01();
        this.A03 = r1.A01();
        this.A07 = C12960it.A0l();
        this.A09 = r3.A03.contains(EnumC87084Ad.SUPPRESS_EXCEPTIONS);
    }

    public Object A00() {
        C92574Wl r3 = this.A02;
        if (!r3.A00.A05()) {
            return this.A05;
        }
        if (this.A00 != 0) {
            AbstractC117035Xw r0 = this.A01.A00;
            Object obj = this.A05;
            int AKQ = r0.AKQ(obj);
            if (AKQ <= 0) {
                return null;
            }
            return ((List) obj).get(AKQ - 1);
        } else if (this.A09) {
            return null;
        } else {
            throw new C82803wD(C12960it.A0d(r3.toString(), C12960it.A0k("No results for path: ")));
        }
    }

    public List A01() {
        ArrayList A0l = C12960it.A0l();
        if (this.A00 > 0) {
            for (Object obj : this.A01.A00.Aet(this.A03)) {
                A0l.add(obj);
            }
        }
        return A0l;
    }

    public void A02(AbstractC111885Be r5, Object obj, String str) {
        if (this.A08) {
            this.A07.add(r5);
        }
        C92364Vp r3 = this.A01;
        AbstractC117035Xw r2 = r3.A00;
        r2.Abk(this.A05, this.A00, obj);
        r2.Abk(this.A03, this.A00, str);
        this.A00++;
        Collection collection = r3.A02;
        if (!collection.isEmpty()) {
            Iterator it = collection.iterator();
            if (it.hasNext()) {
                it.next();
                throw C12980iv.A0n("resultFound");
            }
        }
    }
}
