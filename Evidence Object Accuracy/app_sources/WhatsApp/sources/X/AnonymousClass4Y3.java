package X;

/* renamed from: X.4Y3  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4Y3 {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public C91234Qy A04;
    public AnonymousClass4W6 A05;
    public boolean A06;
    public final AnonymousClass5X6 A07;
    public final AnonymousClass4UI A08 = new AnonymousClass4UI();
    public final C95304dT A09 = new C95304dT();
    public final C95304dT A0A = C95304dT.A05(1);
    public final C95304dT A0B = new C95304dT();

    public AnonymousClass4Y3(AnonymousClass5X6 r2, C91234Qy r3, AnonymousClass4W6 r4) {
        this.A07 = r2;
        this.A05 = r4;
        this.A04 = r3;
        this.A05 = r4;
        this.A04 = r3;
        r2.AA6(r4.A03.A07);
        A02();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001d, code lost:
        if (r14.A0F[r1] == false) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0026, code lost:
        if (r17 != 0) goto L_0x0028;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A00(int r16, int r17) {
        /*
            r15 = this;
            X.4SP r0 = r15.A01()
            r13 = 0
            if (r0 != 0) goto L_0x0008
            return r13
        L_0x0008:
            int r8 = r0.A00
            if (r8 == 0) goto L_0x0044
            X.4UI r0 = r15.A08
            X.4dT r4 = r0.A0H
        L_0x0010:
            X.4UI r14 = r15.A08
            int r1 = r15.A01
            boolean r0 = r14.A07
            if (r0 == 0) goto L_0x001f
            boolean[] r0 = r14.A0F
            boolean r0 = r0[r1]
            r11 = 1
            if (r0 != 0) goto L_0x0020
        L_0x001f:
            r11 = 0
        L_0x0020:
            r9 = 1
            r10 = r17
            if (r11 != 0) goto L_0x0028
            r3 = 0
            if (r17 == 0) goto L_0x0029
        L_0x0028:
            r3 = 1
        L_0x0029:
            X.4dT r2 = r15.A0A
            byte[] r1 = r2.A02
            r0 = 0
            if (r3 == 0) goto L_0x0032
            r0 = 128(0x80, float:1.794E-43)
        L_0x0032:
            X.C72463ee.A0O(r0, r1, r8, r13)
            r2.A0S(r13)
            X.5X6 r7 = r15.A07
            r7.AbD(r2, r9, r9)
            r7.AbD(r4, r8, r9)
            if (r3 != 0) goto L_0x004d
            int r8 = r8 + r9
            return r8
        L_0x0044:
            byte[] r0 = r0.A04
            X.4dT r4 = r15.A09
            int r8 = r0.length
            r4.A0U(r0, r8)
            goto L_0x0010
        L_0x004d:
            r4 = 6
            r6 = 3
            r5 = 2
            r12 = 8
            if (r11 != 0) goto L_0x0084
            X.4dT r3 = r15.A0B
            r3.A0Q(r12)
            byte[] r2 = r3.A02
            r2[r13] = r13
            r2[r9] = r9
            int r0 = r17 >> 8
            X.C72463ee.A0V(r2, r0, r5)
            X.C72463ee.A0V(r2, r10, r6)
            r1 = 4
            r5 = r16
            int r0 = r16 >> 24
            X.C72463ee.A0V(r2, r0, r1)
            r1 = 5
            int r0 = r16 >> 16
            X.C72463ee.A0V(r2, r0, r1)
            int r0 = r16 >> 8
            X.C72463ee.A0V(r2, r0, r4)
            r0 = 7
            X.C72463ee.A0V(r2, r5, r0)
            r7.AbD(r3, r12, r9)
            int r8 = r8 + r9
            int r8 = r8 + r12
            return r8
        L_0x0084:
            X.4dT r11 = r14.A0H
            int r1 = r11.A0F()
            r0 = -2
            r11.A0T(r0)
            int r4 = r1 * 6
            int r4 = r4 + r5
            if (r17 == 0) goto L_0x00b2
            X.4dT r3 = r15.A0B
            r3.A0Q(r4)
            byte[] r2 = r3.A02
            r11.A0V(r2, r13, r4)
            byte r0 = r2[r5]
            r1 = r0 & 255(0xff, float:3.57E-43)
            int r1 = r1 << r12
            byte r0 = r2[r6]
            r0 = r0 & 255(0xff, float:3.57E-43)
            r1 = r1 | r0
            int r1 = r1 + r17
            int r0 = r1 >> 8
            X.C72463ee.A0V(r2, r0, r5)
            X.C72463ee.A0V(r2, r1, r6)
            r11 = r3
        L_0x00b2:
            r7.AbD(r11, r4, r9)
            int r8 = r8 + r9
            int r8 = r8 + r4
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass4Y3.A00(int, int):int");
    }

    public AnonymousClass4SP A01() {
        AnonymousClass4SP[] r0;
        if (this.A06) {
            AnonymousClass4UI r1 = this.A08;
            int i = r1.A05.A02;
            AnonymousClass4SP r12 = r1.A06;
            if (!(r12 == null && ((r0 = this.A05.A03.A0A) == null || (r12 = r0[i]) == null)) && r12.A03) {
                return r12;
            }
        }
        return null;
    }

    public void A02() {
        AnonymousClass4UI r3 = this.A08;
        r3.A01 = 0;
        r3.A04 = 0;
        r3.A08 = false;
        r3.A07 = false;
        r3.A09 = false;
        r3.A06 = null;
        this.A01 = 0;
        this.A02 = 0;
        this.A00 = 0;
        this.A03 = 0;
        this.A06 = false;
    }

    public boolean A03() {
        this.A01++;
        if (this.A06) {
            int i = this.A00 + 1;
            this.A00 = i;
            int[] iArr = this.A08.A0C;
            int i2 = this.A02;
            if (i != iArr[i2]) {
                return true;
            }
            this.A02 = i2 + 1;
            this.A00 = 0;
        }
        return false;
    }
}
