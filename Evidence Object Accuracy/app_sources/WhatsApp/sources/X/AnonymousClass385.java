package X;

/* renamed from: X.385  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass385 extends AbstractC16350or {
    public final int A00;
    public final long A01;
    public final C15650ng A02;
    public final C15660nh A03;
    public final AbstractC14640lm A04;
    public final AnonymousClass2Am A05;
    public final boolean A06;

    public AnonymousClass385(C15650ng r1, C15660nh r2, AbstractC14640lm r3, AnonymousClass2Am r4, int i, long j, boolean z) {
        this.A05 = r4;
        this.A02 = r1;
        this.A03 = r2;
        this.A04 = r3;
        this.A01 = j;
        this.A06 = z;
        this.A00 = i;
    }
}
