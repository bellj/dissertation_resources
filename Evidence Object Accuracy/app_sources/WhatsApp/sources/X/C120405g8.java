package X;

import android.content.Context;

/* renamed from: X.5g8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120405g8 extends C126705tJ {
    public final Context A00;
    public final C14900mE A01;
    public final C18640sm A02;
    public final C17220qS A03;
    public final AnonymousClass68Z A04;
    public final C18650sn A05;

    public C120405g8(Context context, C14900mE r3, C18640sm r4, C17220qS r5, AnonymousClass68Z r6, C1308460e r7, C18650sn r8, C18610sj r9) {
        super(r7.A04, r9);
        this.A00 = context;
        this.A01 = r3;
        this.A03 = r5;
        this.A02 = r4;
        this.A04 = r6;
        this.A05 = r8;
    }
}
