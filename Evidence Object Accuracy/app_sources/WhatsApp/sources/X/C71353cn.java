package X;

import java.lang.Character;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* renamed from: X.3cn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C71353cn implements Iterator {
    public static final Pattern A08;
    public static final Pattern A09;
    public static final Pattern A0A;
    public static final Pattern A0B;
    public static final Pattern A0C = Pattern.compile("\\d{1,5}-+\\d{1,5}\\s{0,4}\\(\\d{1,4}");
    public static final Pattern A0D = Pattern.compile("(?:(?:[0-3]?\\d/[01]?\\d)|(?:[01]?\\d/[0-3]?\\d))/(?:[12]\\d)?\\d{2}");
    public static final Pattern A0E = Pattern.compile("[12]\\d{3}[-/]?[01]\\d[-/]?[0-3]\\d [0-2]\\d$");
    public static final Pattern A0F = Pattern.compile(":[0-5]\\d");
    public int A00 = 0;
    public long A01;
    public AnonymousClass3F5 A02 = null;
    public AnonymousClass4AA A03 = AnonymousClass4AA.NOT_READY;
    public final AnonymousClass39u A04;
    public final C20920wX A05;
    public final CharSequence A06;
    public final String A07;

    static {
        StringBuilder A0k = C12960it.A0k("[^");
        A0k.append("(\\[（［");
        A0k.append(")\\]）］");
        String A0d = C12960it.A0d("]", A0k);
        String A01 = A01(0, 3);
        StringBuilder A0k2 = C12960it.A0k("(?:[");
        A0k2.append("(\\[（［");
        A0k2.append("])?(?:");
        A0k2.append(A0d);
        A0k2.append("+[");
        A0k2.append(")\\]）］");
        A0k2.append("])?");
        A0k2.append(A0d);
        C12960it.A1L("+(?:[", "(\\[（［", "]", A0k2);
        C12960it.A1L(A0d, "+[", ")\\]）］", A0k2);
        C12960it.A1L("])", A01, A0d, A0k2);
        A0A = Pattern.compile(C12960it.A0d("*", A0k2));
        String A012 = A01(0, 2);
        String A013 = A01(0, 4);
        String A014 = A01(0, 19);
        String A0d2 = C12960it.A0d(A013, C12960it.A0k("[-x‐-―−ー－-／  ­​⁠　()（）［］.\\[\\]/~⁓∼～]"));
        String A0d3 = C12960it.A0d(A01(1, 19), C12960it.A0k("\\p{Nd}"));
        String A0d4 = C12960it.A0d("+＋", C12960it.A0j("(\\[（［"));
        StringBuilder A0k3 = C12960it.A0k("[");
        A0k3.append(A0d4);
        String A0d5 = C12960it.A0d("]", A0k3);
        A09 = Pattern.compile(A0d5);
        StringBuilder A0k4 = C12960it.A0k("\\p{Z}[^");
        A0k4.append(A0d4);
        A08 = Pattern.compile(C12960it.A0d("\\p{Nd}]*", A0k4));
        StringBuilder A0k5 = C12960it.A0k("(?:");
        A0k5.append(A0d5);
        A0k5.append(A0d2);
        C12960it.A1L(")", A012, A0d3, A0k5);
        C12960it.A1L("(?:", A0d2, A0d3, A0k5);
        C12960it.A1L(")", A014, "(?:", A0k5);
        A0k5.append(C20920wX.A09);
        A0B = Pattern.compile(C12960it.A0d(")?", A0k5), 66);
    }

    public C71353cn(AnonymousClass39u r4, C20920wX r5, String str, String str2, long j) {
        if (r5 == null || r4 == null) {
            throw null;
        } else if (j >= 0) {
            this.A05 = r5;
            this.A06 = str == null ? "" : str;
            this.A07 = str2;
            this.A04 = r4;
            this.A01 = j;
        } else {
            throw new IllegalArgumentException();
        }
    }

    public static CharSequence A00(CharSequence charSequence, Pattern pattern) {
        Matcher matcher = pattern.matcher(charSequence);
        return matcher.find() ? charSequence.subSequence(0, matcher.start()) : charSequence;
    }

    public static String A01(int i, int i2) {
        if (i2 >= i) {
            StringBuilder A0k = C12960it.A0k("{");
            A0k.append(i);
            A0k.append(",");
            A0k.append(i2);
            return C12960it.A0d("}", A0k);
        }
        throw new IllegalArgumentException();
    }

    public static boolean A02(char c) {
        if (Character.isLetter(c) || Character.getType(c) == 6) {
            Character.UnicodeBlock of = Character.UnicodeBlock.of(c);
            if (of.equals(Character.UnicodeBlock.BASIC_LATIN) || of.equals(Character.UnicodeBlock.LATIN_1_SUPPLEMENT) || of.equals(Character.UnicodeBlock.LATIN_EXTENDED_A) || of.equals(Character.UnicodeBlock.LATIN_EXTENDED_ADDITIONAL) || of.equals(Character.UnicodeBlock.LATIN_EXTENDED_B) || of.equals(Character.UnicodeBlock.COMBINING_DIACRITICAL_MARKS)) {
                return true;
            }
        }
        return false;
    }

    public final AnonymousClass3F5 A03(String str, int i) {
        char charAt;
        char charAt2;
        if (A0A.matcher(str).matches()) {
            AnonymousClass39u r3 = this.A04;
            if (r3.compareTo(AnonymousClass39u.A01) >= 0) {
                if (i > 0 && !A09.matcher(str).lookingAt() && ((charAt2 = this.A06.charAt(i - 1)) == '%' || Character.getType(charAt2) == 26 || A02(charAt2))) {
                    return null;
                }
                int length = str.length() + i;
                CharSequence charSequence = this.A06;
                if (length < charSequence.length() && ((charAt = charSequence.charAt(length)) == '%' || Character.getType(charAt) == 26 || A02(charAt))) {
                    return null;
                }
            }
            C20920wX r5 = this.A05;
            String str2 = this.A07;
            C71133cR r6 = new C71133cR();
            r5.A0H(r6, str, str2, true, true);
            if (r3.A00(r5, r6, str)) {
                r6.hasCountryCodeSource = false;
                r6.countryCodeSource_ = AnonymousClass4AP.FROM_NUMBER_WITH_PLUS_SIGN;
                r6.hasRawInput = false;
                r6.rawInput_ = "";
                r6.hasPreferredDomesticCarrierCode = false;
                r6.preferredDomesticCarrierCode_ = "";
                return new AnonymousClass3F5(r6, str, i);
            }
            return null;
        }
        return null;
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        AnonymousClass3F5 r0;
        AnonymousClass4AA r1 = this.A03;
        if (r1 == AnonymousClass4AA.NOT_READY) {
            int i = this.A00;
            Pattern pattern = A0B;
            CharSequence charSequence = this.A06;
            Matcher matcher = pattern.matcher(charSequence);
            while (this.A01 > 0 && matcher.find(i)) {
                int start = matcher.start();
                CharSequence A00 = A00(charSequence.subSequence(start, matcher.end()), C20920wX.A0O);
                if (!A0C.matcher(A00).find() && !A0D.matcher(A00).find()) {
                    if (A0E.matcher(A00).find()) {
                        if (A0F.matcher(charSequence.toString().substring(A00.length() + start)).lookingAt()) {
                            continue;
                        }
                    }
                    String charSequence2 = A00.toString();
                    r0 = A03(charSequence2, start);
                    if (r0 != null) {
                        break;
                    }
                    Matcher matcher2 = A08.matcher(charSequence2);
                    if (matcher2.find()) {
                        String substring = charSequence2.substring(0, matcher2.start());
                        Pattern pattern2 = C20920wX.A0R;
                        CharSequence A002 = A00(substring, pattern2);
                        r0 = A03(A002.toString(), start);
                        if (r0 != null) {
                            break;
                        }
                        this.A01--;
                        int end = matcher2.end();
                        r0 = A03(A00(charSequence2.substring(end), pattern2).toString(), start + end);
                        if (r0 != null) {
                            break;
                        }
                        long j = this.A01 - 1;
                        this.A01 = j;
                        if (j > 0) {
                            while (matcher2.find()) {
                                end = matcher2.start();
                            }
                            CharSequence A003 = A00(charSequence2.substring(0, end), pattern2);
                            if (!A003.equals(A002)) {
                                r0 = A03(A003.toString(), start);
                                if (r0 != null) {
                                    break;
                                }
                                this.A01--;
                            } else {
                                continue;
                            }
                        } else {
                            continue;
                        }
                    } else {
                        continue;
                    }
                }
                i = start + A00.length();
                this.A01--;
            }
            r0 = null;
            this.A02 = r0;
            if (r0 == null) {
                r1 = AnonymousClass4AA.DONE;
            } else {
                this.A00 = r0.A00 + r0.A02.length();
                r1 = AnonymousClass4AA.READY;
            }
            this.A03 = r1;
        }
        return C12970iu.A1Z(r1, AnonymousClass4AA.READY);
    }

    @Override // java.util.Iterator
    public /* bridge */ /* synthetic */ Object next() {
        if (hasNext()) {
            AnonymousClass3F5 r1 = this.A02;
            this.A02 = null;
            this.A03 = AnonymousClass4AA.NOT_READY;
            return r1;
        }
        throw new NoSuchElementException();
    }

    @Override // java.util.Iterator
    public void remove() {
        throw C12970iu.A0z();
    }
}
