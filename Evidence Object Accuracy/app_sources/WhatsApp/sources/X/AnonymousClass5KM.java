package X;

/* renamed from: X.5KM  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5KM extends AnonymousClass1WI implements AnonymousClass5ZQ {
    public AnonymousClass5KM() {
        super(2);
    }

    @Override // X.AnonymousClass5ZQ
    public Object AJ5(Object obj, Object obj2) {
        return ((AnonymousClass5X4) obj).plus((AnonymousClass5X4) obj2);
    }
}
