package X;

/* renamed from: X.0Yl  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Yl implements AnonymousClass02B {
    public boolean A00 = true;
    public final /* synthetic */ AnonymousClass02P A01;

    public AnonymousClass0Yl(AnonymousClass02P r2) {
        this.A01 = r2;
    }

    @Override // X.AnonymousClass02B
    public void ANq(Object obj) {
        AnonymousClass02P r2 = this.A01;
        Object A01 = r2.A01();
        if (!this.A00) {
            if (A01 == null) {
                if (obj == null) {
                    return;
                }
            } else if (A01.equals(obj)) {
                return;
            }
        }
        this.A00 = false;
        r2.A0B(obj);
    }
}
