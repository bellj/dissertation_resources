package X;

/* renamed from: X.4vt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106554vt implements AnonymousClass5Q1 {
    public final C76673m1 A00;
    public final C106494vn A01;
    public final AnonymousClass5Xx[] A02;

    public C106554vt(AnonymousClass5Xx... r6) {
        C76673m1 r4 = new C76673m1();
        C106494vn r3 = new C106494vn();
        int length = r6.length;
        AnonymousClass5Xx[] r1 = new AnonymousClass5Xx[length + 2];
        this.A02 = r1;
        System.arraycopy(r6, 0, r1, 0, length);
        this.A00 = r4;
        this.A01 = r3;
        r1[length] = r4;
        r1[length + 1] = r3;
    }
}
