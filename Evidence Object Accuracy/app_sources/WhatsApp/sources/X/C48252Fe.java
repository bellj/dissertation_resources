package X;

import com.whatsapp.status.playback.fragment.StatusPlaybackBaseFragment;
import com.whatsapp.status.playback.fragment.StatusPlaybackContactFragment;

/* renamed from: X.2Fe  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C48252Fe {
    public final AbstractC15340mz A00;
    public final /* synthetic */ StatusPlaybackBaseFragment A01;
    public final /* synthetic */ StatusPlaybackContactFragment A02;

    public C48252Fe(AbstractC15340mz r1, StatusPlaybackContactFragment statusPlaybackContactFragment) {
        this.A02 = statusPlaybackContactFragment;
        this.A01 = statusPlaybackContactFragment;
        this.A00 = r1;
    }
}
