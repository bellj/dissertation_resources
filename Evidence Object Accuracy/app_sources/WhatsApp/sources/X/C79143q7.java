package X;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.3q7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79143q7<E> extends AbstractC113535Hy<E> {
    public static final C79143q7 A01;
    public final List A00;

    public C79143q7(List list) {
        this.A00 = list;
    }

    @Override // java.util.AbstractList, java.util.List
    public final void add(int i, Object obj) {
        A02();
        this.A00.add(i, obj);
        ((AbstractList) this).modCount++;
    }

    @Override // java.util.AbstractList, java.util.List
    public final Object get(int i) {
        return this.A00.get(i);
    }

    @Override // java.util.AbstractList, java.util.List
    public final Object remove(int i) {
        A02();
        Object remove = this.A00.remove(i);
        ((AbstractList) this).modCount++;
        return remove;
    }

    @Override // java.util.AbstractList, java.util.List
    public final Object set(int i, Object obj) {
        A02();
        Object obj2 = this.A00.set(i, obj);
        ((AbstractList) this).modCount++;
        return obj2;
    }

    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection
    public final int size() {
        return this.A00.size();
    }

    static {
        C79143q7 r1 = new C79143q7(C12980iv.A0w(10));
        A01 = r1;
        ((AbstractC113535Hy) r1).A00 = false;
    }

    @Override // X.AnonymousClass5Z5
    public final /* synthetic */ AnonymousClass5Z5 AhN(int i) {
        if (i >= size()) {
            ArrayList A0w = C12980iv.A0w(i);
            A0w.addAll(this.A00);
            return new C79143q7(A0w);
        }
        throw C72453ed.A0h();
    }
}
