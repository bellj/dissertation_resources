package X;

import android.hardware.Camera;
import java.util.concurrent.Callable;

/* renamed from: X.6Km  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class CallableC135966Km implements Callable {
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ C129535xs A02;
    public final /* synthetic */ AnonymousClass661 A03;
    public final /* synthetic */ AbstractC1311561m A04;

    public CallableC135966Km(C129535xs r1, AnonymousClass661 r2, AbstractC1311561m r3, int i, int i2) {
        this.A03 = r2;
        this.A02 = r1;
        this.A01 = i;
        this.A04 = r3;
        this.A00 = i2;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        try {
            AnonymousClass616.A00();
            AnonymousClass661 r3 = this.A03;
            if (!(r3.A0Z == null || r3.A0Z == this.A02.A02)) {
                r3.A0Z.A01();
                r3.A0Z = null;
            }
            AnonymousClass60S r5 = r3.A0J;
            int i = AnonymousClass60S.A02;
            if (i == -1) {
                r5.A01.A06("Number of cameras must be loaded on background thread.");
                i = Camera.getNumberOfCameras();
                AnonymousClass60S.A02 = i;
            }
            if (i != 0) {
                int i2 = this.A01;
                if (r5.A01.A09()) {
                    if (r5.A02(i2) == -1) {
                        int i3 = AnonymousClass60S.A02;
                        if (i3 == -1) {
                            AnonymousClass616.A01("CameraInventory", "Camera count was not initialised");
                            i3 = Camera.getNumberOfCameras();
                            AnonymousClass60S.A02 = i3;
                        }
                        if (i3 == 0) {
                            throw new C118895cY();
                        } else if (i2 == 0) {
                            if (r5.A02(1) != -1) {
                                AnonymousClass616.A02("CameraInventory", "Requested back camera doesn't exist, using front instead");
                                i2 = 1;
                            }
                            StringBuilder A0k = C12960it.A0k("found ");
                            A0k.append(AnonymousClass60S.A02);
                            throw C12990iw.A0m(C12960it.A0d(" cameras with bad facing constants", A0k));
                        } else {
                            if (i2 == 1 && r5.A02(0) != -1) {
                                AnonymousClass616.A02("CameraInventory", "Requested front camera doesn't exist, using back instead");
                                i2 = 0;
                            }
                            StringBuilder A0k = C12960it.A0k("found ");
                            A0k.append(AnonymousClass60S.A02);
                            throw C12990iw.A0m(C12960it.A0d(" cameras with bad facing constants", A0k));
                        }
                    }
                    AbstractC1311561m r2 = this.A04;
                    AnonymousClass661.A03(r3, r2, i2);
                    C127255uC A00 = AnonymousClass661.A00(this.A02, r3, r2, this.A00);
                    AnonymousClass616.A00();
                    return A00;
                }
                throw C12990iw.A0m("Cannot resolve camera facing, not on the Optic thread");
            }
            throw new C118895cY();
        } catch (Exception e) {
            AnonymousClass616.A00();
            AnonymousClass661 r0 = this.A03;
            C128435w6.A00(r0);
            AnonymousClass661.A02(r0);
            throw e;
        }
    }
}
