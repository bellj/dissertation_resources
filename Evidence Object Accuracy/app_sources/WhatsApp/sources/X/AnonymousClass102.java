package X;

import android.os.Parcel;
import org.json.JSONObject;

/* renamed from: X.102  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass102 {
    public static final AbstractC30791Yv[] A01 = {C30771Yt.A06, C30771Yt.A05, C30771Yt.A04};
    public final AnonymousClass173 A00;

    public AnonymousClass102(AnonymousClass173 r1) {
        this.A00 = r1;
    }

    public static AbstractC30791Yv A00(Parcel parcel) {
        int readInt = parcel.readInt();
        if (readInt == 0) {
            return new C30771Yt(parcel);
        }
        if (readInt != 1) {
            return C30771Yt.A06;
        }
        return new C30801Yw(parcel);
    }

    public static AbstractC30791Yv A01(JSONObject jSONObject, int i) {
        if (jSONObject == null) {
            return C30771Yt.A06;
        }
        if (i != 1) {
            return new C30771Yt(jSONObject);
        }
        return new C30801Yw(jSONObject);
    }

    public AbstractC30791Yv A02(String str) {
        if (str != null) {
            AbstractC30791Yv[] r4 = A01;
            for (AbstractC30791Yv r0 : r4) {
                if (str.equals(((AbstractC30781Yu) r0).A04)) {
                    return r0;
                }
            }
            AnonymousClass173 r1 = this.A00;
            r1.A00();
            AbstractC30791Yv r02 = (AbstractC30791Yv) r1.A01.get(str);
            if (r02 != null) {
                return r02;
            }
            r1.A00();
            AbstractC30791Yv r03 = (AbstractC30791Yv) r1.A00.get(str);
            if (r03 != null) {
                return r03;
            }
        }
        return C30771Yt.A06;
    }
}
