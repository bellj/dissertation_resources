package X;

import androidx.appcompat.widget.ActionBarOverlayLayout;

/* renamed from: X.0cE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09020cE implements Runnable {
    public final /* synthetic */ ActionBarOverlayLayout A00;

    public RunnableC09020cE(ActionBarOverlayLayout actionBarOverlayLayout) {
        this.A00 = actionBarOverlayLayout;
    }

    @Override // java.lang.Runnable
    public void run() {
        ActionBarOverlayLayout actionBarOverlayLayout = this.A00;
        actionBarOverlayLayout.A01();
        actionBarOverlayLayout.A05 = actionBarOverlayLayout.A07.animate().translationY((float) (-actionBarOverlayLayout.A07.getHeight())).setListener(actionBarOverlayLayout.A0K);
    }
}
