package X;

import android.app.Activity;
import java.util.Set;

/* renamed from: X.18N  reason: invalid class name */
/* loaded from: classes2.dex */
public interface AnonymousClass18N {
    void A9p(AnonymousClass5US v, C18610sj v2);

    Set AAs();

    boolean AJE(AnonymousClass1ZR v);

    boolean AJQ();

    void Acr();

    boolean AdQ();

    void Afe(Activity activity, AnonymousClass5US v, C18610sj v2, String str, boolean z);

    void clear();

    int size();
}
