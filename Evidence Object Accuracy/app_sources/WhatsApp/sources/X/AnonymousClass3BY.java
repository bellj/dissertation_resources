package X;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.calling.callhistory.CallLogActivity;

/* renamed from: X.3BY  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3BY {
    public final ImageView A00;
    public final TextView A01;
    public final TextView A02;
    public final TextView A03;
    public final TextView A04;
    public final TextView A05;
    public final /* synthetic */ CallLogActivity A06;

    public AnonymousClass3BY(View view, CallLogActivity callLogActivity) {
        this.A06 = callLogActivity;
        this.A00 = C12970iu.A0L(view, R.id.call_type_icon);
        this.A04 = C12960it.A0J(view, R.id.call_type);
        this.A02 = C12960it.A0J(view, R.id.call_date);
        this.A03 = C12960it.A0J(view, R.id.call_duration);
        this.A01 = C12960it.A0J(view, R.id.call_data);
        this.A05 = C12960it.A0I(view, R.id.caller_info);
    }
}
