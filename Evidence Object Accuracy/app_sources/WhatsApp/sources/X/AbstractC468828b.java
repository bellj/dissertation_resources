package X;

import java.util.NoSuchElementException;

/* renamed from: X.28b  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC468828b extends AnonymousClass1I5 {
    public Object next;
    public AnonymousClass4BR state = AnonymousClass4BR.NOT_READY;

    public abstract Object computeNext();

    public final Object endOfData() {
        this.state = AnonymousClass4BR.DONE;
        return null;
    }

    @Override // java.util.Iterator
    public final boolean hasNext() {
        AnonymousClass4BR r3 = this.state;
        if (r3 != AnonymousClass4BR.FAILED) {
            switch (r3.ordinal()) {
                case 0:
                    return true;
                case 1:
                default:
                    return tryToComputeNext();
                case 2:
                    return false;
            }
        } else {
            throw new IllegalStateException();
        }
    }

    @Override // java.util.Iterator
    public final Object next() {
        if (hasNext()) {
            this.state = AnonymousClass4BR.NOT_READY;
            Object obj = this.next;
            this.next = null;
            return obj;
        }
        throw new NoSuchElementException();
    }

    private boolean tryToComputeNext() {
        this.state = AnonymousClass4BR.FAILED;
        this.next = computeNext();
        if (this.state == AnonymousClass4BR.DONE) {
            return false;
        }
        this.state = AnonymousClass4BR.READY;
        return true;
    }
}
