package X;

import com.facebook.msys.mci.DefaultCrypto;
import java.io.EOFException;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.charset.Charset;

/* renamed from: X.0f6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C10730f6 implements AbstractC02750Dv, Cloneable, ByteChannel, AbstractC12950ip {
    public static final byte[] A02;
    public long A00;
    public C94484bt A01;

    @Override // X.AbstractC02750Dv
    public C10730f6 AB0() {
        return this;
    }

    @Override // X.AbstractC117195Yx, java.io.Closeable, java.lang.AutoCloseable, java.nio.channels.Channel
    public void close() {
    }

    @Override // java.io.Flushable
    public void flush() {
    }

    @Override // java.nio.channels.Channel
    public boolean isOpen() {
        return true;
    }

    static {
        byte[] bytes = "0123456789abcdef".getBytes(C88844Hi.A05);
        C16700pc.A0A(bytes);
        A02 = bytes;
    }

    public static final C08960c8 A00(C10730f6 r7, int i) {
        AnonymousClass4F1.A00(r7.A00, 0, (long) i);
        C94484bt r3 = r7.A01;
        C94484bt r72 = r3;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        while (i3 < i) {
            if (r3 == null) {
                C16700pc.A09();
                throw new RuntimeException("Redex: Unreachable code after no-return invoke");
            }
            int i5 = r3.A00;
            int i6 = r3.A01;
            if (i5 != i6) {
                i3 += i5 - i6;
                i4++;
                r3 = r3.A02;
            } else {
                throw new AssertionError("s.limit == s.pos");
            }
        }
        byte[][] bArr = new byte[i4];
        int[] iArr = new int[i4 << 1];
        int i7 = 0;
        while (i2 < i) {
            if (r72 == null) {
                C16700pc.A09();
                throw new RuntimeException("Redex: Unreachable code after no-return invoke");
            }
            bArr[i7] = r72.A06;
            int i8 = r72.A00;
            int i9 = r72.A01;
            i2 += i8 - i9;
            iArr[i7] = Math.min(i2, i);
            iArr[i7 + i4] = i9;
            r72.A05 = true;
            i7++;
            r72 = r72.A02;
        }
        return new C114505Lu(iArr, bArr);
    }

    public final byte A01(long j) {
        byte[] bArr;
        long j2;
        AnonymousClass4F1.A00(this.A00, j, 1);
        C94484bt r7 = this.A01;
        if (r7 != null) {
            long j3 = this.A00;
            if (j3 - j >= j) {
                long j4 = 0;
                while (true) {
                    int i = r7.A00;
                    int i2 = r7.A01;
                    long j5 = ((long) (i - i2)) + j4;
                    if (j5 > j) {
                        bArr = r7.A06;
                        j2 = (((long) i2) + j) - j4;
                        break;
                    }
                    r7 = r7.A02;
                    if (r7 == null) {
                        C16700pc.A09();
                        throw new RuntimeException("Redex: Unreachable code after no-return invoke");
                    }
                    j4 = j5;
                }
            } else {
                while (j3 > j) {
                    r7 = r7.A03;
                    if (r7 == null) {
                        C16700pc.A09();
                        throw new RuntimeException("Redex: Unreachable code after no-return invoke");
                    }
                    j3 -= (long) (r7.A00 - r7.A01);
                }
                bArr = r7.A06;
                j2 = (((long) r7.A01) + j) - j3;
            }
            return bArr[(int) j2];
        }
        C16700pc.A09();
        throw null;
    }

    public int A02(byte[] bArr, int i, int i2) {
        AnonymousClass4F1.A00((long) bArr.length, (long) i, (long) i2);
        C94484bt r6 = this.A01;
        if (r6 == null) {
            return -1;
        }
        int i3 = r6.A00;
        int i4 = r6.A01;
        int min = Math.min(i2, i3 - i4);
        System.arraycopy(r6.A06, i4, bArr, i, min);
        int i5 = r6.A01 + min;
        r6.A01 = i5;
        this.A00 -= (long) min;
        if (i5 == r6.A00) {
            this.A01 = r6.A00();
            C95204dJ.A01(r6);
        }
        return min;
    }

    public final long A03() {
        return this.A00;
    }

    public String A04() {
        return A06(C88844Hi.A05, this.A00);
    }

    public String A05(long j) {
        return A06(C88844Hi.A05, j);
    }

    public String A06(Charset charset, long j) {
        if (j < 0 || j > ((long) Integer.MAX_VALUE)) {
            StringBuilder sb = new StringBuilder("byteCount: ");
            sb.append(j);
            throw new IllegalArgumentException(sb.toString());
        } else if (this.A00 < j) {
            throw new EOFException();
        } else if (j == 0) {
            return "";
        } else {
            C94484bt r4 = this.A01;
            if (r4 == null) {
                C16700pc.A09();
                throw new RuntimeException("Redex: Unreachable code after no-return invoke");
            }
            int i = r4.A01;
            if (((long) i) + j > ((long) r4.A00)) {
                return new String(A0K(j), charset);
            }
            int i2 = (int) j;
            String str = new String(r4.A06, i, i2, charset);
            int i3 = r4.A01 + i2;
            r4.A01 = i3;
            this.A00 -= j;
            if (i3 == r4.A00) {
                this.A01 = r4.A00();
                C95204dJ.A01(r4);
            }
            return str;
        }
    }

    /* renamed from: A07 */
    public C10730f6 clone() {
        C10730f6 r5 = new C10730f6();
        if (this.A00 != 0) {
            C94484bt r0 = this.A01;
            if (r0 != null) {
                C94484bt A01 = r0.A01();
                r5.A01 = A01;
                A01.A03 = A01;
                A01.A02 = A01;
                C94484bt r2 = this.A01;
                if (r2 != null) {
                    while (true) {
                        r2 = r2.A02;
                        if (r2 == this.A01) {
                            r5.A00 = this.A00;
                            break;
                        }
                        C94484bt r02 = r5.A01;
                        if (r02 == null) {
                            C16700pc.A09();
                            throw new RuntimeException("Redex: Unreachable code after no-return invoke");
                        }
                        C94484bt r1 = r02.A03;
                        if (r1 == null) {
                            C16700pc.A09();
                            throw new RuntimeException("Redex: Unreachable code after no-return invoke");
                        } else if (r2 == null) {
                            C16700pc.A09();
                            throw new RuntimeException("Redex: Unreachable code after no-return invoke");
                        } else {
                            r1.A04(r2.A01());
                        }
                    }
                } else {
                    C16700pc.A09();
                    throw new RuntimeException("Redex: Unreachable code after no-return invoke");
                }
            } else {
                C16700pc.A09();
                throw new RuntimeException("Redex: Unreachable code after no-return invoke");
            }
        }
        return r5;
    }

    public C08960c8 A08() {
        return new C08960c8(A0K(this.A00));
    }

    public final C94484bt A09(int i) {
        if (i <= 8192) {
            C94484bt r0 = this.A01;
            if (r0 == null) {
                C94484bt A00 = C95204dJ.A00();
                this.A01 = A00;
                A00.A03 = A00;
                A00.A02 = A00;
                return A00;
            }
            C94484bt r1 = r0.A03;
            if (r1 == null) {
                C16700pc.A09();
                throw new RuntimeException("Redex: Unreachable code after no-return invoke");
            } else if (r1.A00 + i <= 8192 && r1.A04) {
                return r1;
            } else {
                C94484bt A002 = C95204dJ.A00();
                r1.A04(A002);
                return A002;
            }
        } else {
            throw new IllegalArgumentException("unexpected capacity");
        }
    }

    public final void A0A() {
        A0G(this.A00);
    }

    public /* bridge */ /* synthetic */ void A0B() {
        A0C(34);
    }

    public void A0C(int i) {
        C94484bt A09 = A09(1);
        byte[] bArr = A09.A06;
        int i2 = A09.A00;
        A09.A00 = i2 + 1;
        bArr[i2] = (byte) i;
        this.A00++;
    }

    public void A0D(int i) {
        C94484bt A09 = A09(4);
        byte[] bArr = A09.A06;
        int i2 = A09.A00;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((i >>> 24) & 255);
        int i4 = i3 + 1;
        bArr[i3] = (byte) ((i >>> 16) & 255);
        int i5 = i4 + 1;
        bArr[i4] = (byte) ((i >>> 8) & 255);
        bArr[i5] = (byte) (i & 255);
        A09.A00 = i5 + 1;
        this.A00 += 4;
    }

    /* renamed from: A0E */
    public void A0F(int i, String str, int i2) {
        long j;
        long j2;
        char c;
        if (i < 0) {
            StringBuilder sb = new StringBuilder("beginIndex < 0: ");
            sb.append(i);
            throw new IllegalArgumentException(sb.toString());
        } else if (i2 >= i) {
            int length = str.length();
            if (i2 > length) {
                StringBuilder sb2 = new StringBuilder("endIndex > string.length: ");
                sb2.append(i2);
                sb2.append(" > ");
                sb2.append(length);
                throw new IllegalArgumentException(sb2.toString());
            }
            while (i < i2) {
                char charAt = str.charAt(i);
                if (charAt < 128) {
                    C94484bt A09 = A09(1);
                    byte[] bArr = A09.A06;
                    int i3 = A09.A00 - i;
                    int min = Math.min(i2, 8192 - i3);
                    int i4 = i + 1;
                    bArr[i + i3] = (byte) charAt;
                    while (i4 < min) {
                        char charAt2 = str.charAt(i4);
                        if (charAt2 >= 128) {
                            break;
                        }
                        bArr[i4 + i3] = (byte) charAt2;
                        i4++;
                    }
                    int i5 = A09.A00;
                    int i6 = (i3 + i4) - i5;
                    A09.A00 = i5 + i6;
                    this.A00 += (long) i6;
                    i = i4;
                } else {
                    if (charAt < 2048) {
                        C94484bt A092 = A09(2);
                        byte[] bArr2 = A092.A06;
                        int i7 = A092.A00;
                        bArr2[i7] = (byte) ((charAt >> 6) | 192);
                        bArr2[i7 + 1] = (byte) ((charAt & '?') | 128);
                        A092.A00 = i7 + 2;
                        j = this.A00;
                        j2 = 2;
                    } else if (charAt < 55296 || charAt > 57343) {
                        C94484bt A093 = A09(3);
                        byte[] bArr3 = A093.A06;
                        int i8 = A093.A00;
                        bArr3[i8] = (byte) ((charAt >> '\f') | 224);
                        bArr3[i8 + 1] = (byte) ((63 & (charAt >> 6)) | 128);
                        bArr3[i8 + 2] = (byte) ((charAt & '?') | 128);
                        A093.A00 = i8 + 3;
                        j = this.A00;
                        j2 = 3;
                    } else {
                        int i9 = i + 1;
                        if (i9 < i2) {
                            c = str.charAt(i9);
                        } else {
                            c = 0;
                        }
                        if (charAt > 56319 || 56320 > c || 57343 < c) {
                            A0C(63);
                            i = i9;
                        } else {
                            int i10 = (((charAt & 1023) << 10) | (c & 1023)) + 65536;
                            C94484bt A094 = A09(4);
                            byte[] bArr4 = A094.A06;
                            int i11 = A094.A00;
                            bArr4[i11] = (byte) ((i10 >> 18) | 240);
                            bArr4[i11 + 1] = (byte) (((i10 >> 12) & 63) | 128);
                            bArr4[i11 + 2] = (byte) (((i10 >> 6) & 63) | 128);
                            bArr4[i11 + 3] = (byte) ((i10 & 63) | 128);
                            A094.A00 = i11 + 4;
                            this.A00 += 4;
                            i += 2;
                        }
                    }
                    this.A00 = j + j2;
                    i++;
                }
            }
        } else {
            StringBuilder sb3 = new StringBuilder("endIndex < beginIndex: ");
            sb3.append(i2);
            sb3.append(" < ");
            sb3.append(i);
            throw new IllegalArgumentException(sb3.toString());
        }
    }

    public void A0G(long j) {
        while (j > 0) {
            C94484bt r6 = this.A01;
            if (r6 != null) {
                int i = r6.A00;
                int i2 = r6.A01;
                int min = (int) Math.min(j, (long) (i - i2));
                long j2 = (long) min;
                this.A00 -= j2;
                j -= j2;
                int i3 = i2 + min;
                r6.A01 = i3;
                if (i3 == i) {
                    this.A01 = r6.A00();
                    C95204dJ.A01(r6);
                }
            } else {
                throw new EOFException();
            }
        }
    }

    /* renamed from: A0H */
    public void A0I(String str) {
        C16700pc.A0D(str, 0);
        A0F(0, str, str.length());
    }

    public void A0J(C10730f6 r10, long j) {
        C94484bt r6;
        int i;
        long j2 = j;
        if (r10 != this) {
            AnonymousClass4F1.A00(r10.A00, 0, j2);
            while (j2 > 0) {
                C94484bt r4 = r10.A01;
                if (r4 == null) {
                    C16700pc.A09();
                    throw new RuntimeException("Redex: Unreachable code after no-return invoke");
                }
                if (j2 < ((long) (r4.A00 - r4.A01))) {
                    C94484bt r0 = this.A01;
                    if (!(r0 == null || (r6 = r0.A03) == null || !r6.A04)) {
                        long j3 = ((long) r6.A00) + j2;
                        if (r6.A05) {
                            i = 0;
                        } else {
                            i = r6.A01;
                        }
                        if (j3 - ((long) i) <= ((long) DefaultCrypto.BUFFER_SIZE)) {
                            r4.A05(r6, (int) j2);
                            r10.A00 -= j2;
                            this.A00 += j2;
                            return;
                        }
                    }
                    r4 = r4.A02((int) j2);
                    r10.A01 = r4;
                }
                long j4 = (long) (r4.A00 - r4.A01);
                r10.A01 = r4.A00();
                C94484bt r02 = this.A01;
                if (r02 == null) {
                    this.A01 = r4;
                    r4.A03 = r4;
                    r4.A02 = r4;
                } else {
                    C94484bt r03 = r02.A03;
                    if (r03 == null) {
                        C16700pc.A09();
                        throw new RuntimeException("Redex: Unreachable code after no-return invoke");
                    } else {
                        r03.A04(r4);
                        r4.A03();
                    }
                }
                r10.A00 -= j4;
                this.A00 += j4;
                j2 -= j4;
            }
            return;
        }
        throw new IllegalArgumentException("source == this");
    }

    public byte[] A0K(long j) {
        if (j < 0 || j > ((long) Integer.MAX_VALUE)) {
            StringBuilder sb = new StringBuilder("byteCount: ");
            sb.append(j);
            throw new IllegalArgumentException(sb.toString());
        } else if (this.A00 >= j) {
            int i = (int) j;
            byte[] bArr = new byte[i];
            int i2 = 0;
            while (i2 < i) {
                int A022 = A02(bArr, i2, i - i2);
                if (A022 != -1) {
                    i2 += A022;
                } else {
                    throw new EOFException();
                }
            }
            return bArr;
        } else {
            throw new EOFException();
        }
    }

    @Override // X.AbstractC117195Yx
    public long AZp(C10730f6 r6, long j) {
        if (j >= 0) {
            long j2 = this.A00;
            if (j2 == 0) {
                return -1;
            }
            if (j > j2) {
                j = j2;
            }
            r6.A0J(this, j);
            return j;
        }
        StringBuilder sb = new StringBuilder("byteCount < 0: ");
        sb.append(j);
        throw new IllegalArgumentException(sb.toString());
    }

    @Override // X.AbstractC02750Dv
    public boolean AaY(long j) {
        return this.A00 >= j;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C10730f6) {
                long j = this.A00;
                C10730f6 r6 = (C10730f6) obj;
                if (j == r6.A00) {
                    if (j != 0) {
                        C94484bt r9 = this.A01;
                        if (r9 == null) {
                            C16700pc.A09();
                            throw new RuntimeException("Redex: Unreachable code after no-return invoke");
                        }
                        C94484bt r8 = r6.A01;
                        if (r8 == null) {
                            C16700pc.A09();
                            throw new RuntimeException("Redex: Unreachable code after no-return invoke");
                        }
                        int i = r9.A01;
                        int i2 = r8.A01;
                        long j2 = 0;
                        while (j2 < j) {
                            long min = (long) Math.min(r9.A00 - i, r8.A00 - i2);
                            for (long j3 = 0; j3 < min; j3++) {
                                i++;
                                i2++;
                                if (r9.A06[i] == r8.A06[i2]) {
                                }
                            }
                            if (i == r9.A00) {
                                r9 = r9.A02;
                                if (r9 == null) {
                                    C16700pc.A09();
                                    throw new RuntimeException("Redex: Unreachable code after no-return invoke");
                                }
                                i = r9.A01;
                            }
                            if (i2 == r8.A00) {
                                r8 = r8.A02;
                                if (r8 == null) {
                                    C16700pc.A09();
                                    throw new RuntimeException("Redex: Unreachable code after no-return invoke");
                                }
                                i2 = r8.A01;
                            }
                            j2 += min;
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        C94484bt r5 = this.A01;
        if (r5 == null) {
            return 0;
        }
        int i = 1;
        do {
            int i2 = r5.A00;
            for (int i3 = r5.A01; i3 < i2; i3++) {
                i = (i * 31) + r5.A06[i3];
            }
            r5 = r5.A02;
            if (r5 == null) {
                C16700pc.A09();
                throw new RuntimeException("Redex: Unreachable code after no-return invoke");
            }
        } while (r5 != r5);
        return i;
    }

    @Override // java.nio.channels.ReadableByteChannel
    public int read(ByteBuffer byteBuffer) {
        C16700pc.A0D(byteBuffer, 0);
        C94484bt r6 = this.A01;
        if (r6 == null) {
            return -1;
        }
        int remaining = byteBuffer.remaining();
        int i = r6.A00;
        int i2 = r6.A01;
        int min = Math.min(remaining, i - i2);
        byteBuffer.put(r6.A06, i2, min);
        int i3 = r6.A01 + min;
        r6.A01 = i3;
        this.A00 -= (long) min;
        if (i3 == r6.A00) {
            this.A01 = r6.A00();
            C95204dJ.A01(r6);
        }
        return min;
    }

    @Override // X.AbstractC02750Dv
    public byte readByte() {
        long j = this.A00;
        if (j != 0) {
            C94484bt r7 = this.A01;
            if (r7 == null) {
                C16700pc.A09();
                throw new RuntimeException("Redex: Unreachable code after no-return invoke");
            }
            int i = r7.A01;
            int i2 = r7.A00;
            int i3 = i + 1;
            byte b = r7.A06[i];
            this.A00 = j - 1;
            if (i3 == i2) {
                this.A01 = r7.A00();
                C95204dJ.A01(r7);
                return b;
            }
            r7.A01 = i3;
            return b;
        }
        throw new EOFException();
    }

    @Override // java.lang.Object
    public String toString() {
        C08960c8 A00;
        long j = this.A00;
        if (j <= ((long) Integer.MAX_VALUE)) {
            int i = (int) j;
            if (i == 0) {
                A00 = C08960c8.A02;
            } else {
                A00 = A00(this, i);
            }
            return A00.toString();
        }
        StringBuilder sb = new StringBuilder("size > Integer.MAX_VALUE: ");
        sb.append(j);
        throw new IllegalStateException(sb.toString());
    }

    @Override // java.nio.channels.WritableByteChannel
    public int write(ByteBuffer byteBuffer) {
        C16700pc.A0D(byteBuffer, 0);
        int remaining = byteBuffer.remaining();
        int i = remaining;
        while (i > 0) {
            C94484bt A09 = A09(1);
            int i2 = A09.A00;
            int min = Math.min(i, 8192 - i2);
            byteBuffer.get(A09.A06, i2, min);
            i -= min;
            A09.A00 += min;
        }
        this.A00 += (long) remaining;
        return remaining;
    }
}
