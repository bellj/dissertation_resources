package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4jt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C99184jt implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        float f = 0.0f;
        float f2 = 0.0f;
        int i = 0;
        int i2 = 0;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                i = C95664e9.A02(parcel, readInt);
            } else if (c == 2) {
                f = C95664e9.A00(parcel, readInt);
            } else if (c == 3) {
                f2 = C95664e9.A00(parcel, readInt);
            } else if (c != 4) {
                C95664e9.A0D(parcel, readInt);
            } else {
                C95664e9.A0F(parcel, readInt, 4);
                i2 = parcel.readInt();
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78403or(f, f2, i, i2);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new C78403or[i];
    }
}
