package X;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import com.whatsapp.util.Log;

/* renamed from: X.0y4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21840y4 {
    public final Handler A00;
    public final C16240og A01;
    public final AnonymousClass01d A02;
    public final C16590pI A03;
    public final AnonymousClass22A A04;
    public final C14910mF A05;
    public final C21810y1 A06;
    public final C21820y2 A07;
    public final AbstractC14440lR A08;

    public C21840y4(C16240og r10, C21830y3 r11, C18230s7 r12, AnonymousClass01d r13, C16590pI r14, C14910mF r15, C21810y1 r16, C21820y2 r17, AbstractC14440lR r18) {
        this.A03 = r14;
        this.A08 = r18;
        this.A02 = r13;
        this.A05 = r15;
        this.A01 = r10;
        this.A06 = r16;
        this.A07 = r17;
        this.A04 = new AnonymousClass22A(r11, r12, r13, r14, this, r15, r16);
        this.A00 = new Handler(Looper.getMainLooper(), new Handler.Callback(r17) { // from class: X.22B
            public final /* synthetic */ C21820y2 A01;

            {
                this.A01 = r2;
            }

            @Override // android.os.Handler.Callback
            public final boolean handleMessage(Message message) {
                C21840y4 r3 = C21840y4.this;
                C21820y2 r2 = this.A01;
                if (message.what != 1) {
                    return false;
                }
                if (r2.A00) {
                    return true;
                }
                r3.A01(false);
                return true;
            }
        });
    }

    public void A00() {
        C14910mF r5 = this.A05;
        if (r5.A00 == 1) {
            r5.A00 = 2;
            AnonymousClass22A r7 = this.A04;
            PendingIntent A01 = AnonymousClass1UY.A01(r7.A03.A00, 0, new Intent("com.whatsapp.alarm.AVAILABLE_TIMEOUT").setPackage("com.whatsapp"), 134217728);
            if (!r7.A01.A02(A01, 2, SystemClock.elapsedRealtime() + 15000)) {
                Log.w("AvailabilityTimeoutAlarmBroadcastReceiver/startAvailableTimeoutAlarm AlarmManager is null");
            }
        }
        StringBuilder sb = new StringBuilder("presencestatemanager/startTransitionToUnavailable/new-state ");
        sb.append(r5);
        Log.i(sb.toString());
    }

    public final void A01(boolean z) {
        Context context = this.A03.A00;
        C21820y2 r4 = this.A07;
        AnonymousClass01d r1 = this.A02;
        AnonymousClass009.A01();
        if (AnonymousClass1HT.A04) {
            boolean z2 = !AnonymousClass1HT.A00(r1);
            AnonymousClass1HT.A04 = z2;
            StringBuilder sb = new StringBuilder("ScreenLockReceiver manual check; locked=");
            sb.append(z2);
            Log.i(sb.toString());
            r4.A05(AnonymousClass1HT.A04);
        }
        C14910mF r5 = this.A05;
        int i = r5.A00;
        if (i != 1) {
            if (i == 2) {
                this.A04.A00();
                r5.A00 = 1;
            } else if (z) {
                r5.A00 = 1;
                C16240og r42 = this.A01;
                if (r42.A04 != 1) {
                    C21810y1 r0 = this.A06;
                    r0.A00 = true;
                    r0.A00();
                }
                if (r42.A04 != 2) {
                    this.A08.Aaz(new AnonymousClass22C(context, r42), new Void[0]);
                }
            }
        }
        StringBuilder sb2 = new StringBuilder("presencestatemanager/setAvailable/new-state: ");
        sb2.append(r5);
        sb2.append(" setIfUnavailable:");
        sb2.append(z);
        Log.i(sb2.toString());
    }
}
