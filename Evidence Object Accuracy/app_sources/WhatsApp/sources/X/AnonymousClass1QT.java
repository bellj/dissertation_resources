package X;

import java.io.File;
import java.util.zip.ZipFile;

/* renamed from: X.1QT  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1QT extends AnonymousClass1QU {
    public File A00;
    public AnonymousClass1QW[] A01;
    public final int A02;
    public final AnonymousClass1QJ A03;
    public final ZipFile A04;
    public final /* synthetic */ AnonymousClass1QO A05;
    public final /* synthetic */ AnonymousClass1QO A06;

    public AnonymousClass1QT(AnonymousClass1QO r3, AnonymousClass1QO r4) {
        this.A05 = r3;
        this.A06 = r3;
        this.A04 = new ZipFile(r3.A01);
        this.A03 = r4;
        this.A00 = new File(((AnonymousClass1QJ) r3).A02.getApplicationInfo().nativeLibraryDir);
        this.A02 = r3.A00;
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x00c5  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00ca  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.AnonymousClass1QW[] A00() {
        /*
        // Method dump skipped, instructions count: 326
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1QT.A00():X.1QW[]");
    }
}
