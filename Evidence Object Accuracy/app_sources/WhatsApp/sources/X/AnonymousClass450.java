package X;

import com.whatsapp.R;
import com.whatsapp.community.CommunityHomeActivity;

/* renamed from: X.450  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass450 extends AnonymousClass384 {
    public final /* synthetic */ CommunityHomeActivity A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass450(C14900mE r9, C15570nT r10, CommunityHomeActivity communityHomeActivity, C14830m7 r12, C21320xE r13, C15370n3 r14, C20660w7 r15, String str) {
        super(r9, r10, r12, r13, r14, r15, str);
        this.A00 = communityHomeActivity;
    }

    @Override // X.AnonymousClass384
    public void A08(int i, String str) {
        ((ActivityC13810kN) this.A00).A05.A07(R.string.edit_community_description_error, 0);
    }
}
