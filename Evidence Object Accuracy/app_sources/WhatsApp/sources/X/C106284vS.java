package X;

import android.content.Context;
import android.view.View;

/* renamed from: X.4vS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106284vS implements AnonymousClass5WW {
    @Override // X.AnonymousClass5WW
    public void A6O(Context context, Object obj, Object obj2, Object obj3) {
        ((View) obj).setOverScrollMode(((C56002kA) obj2).A03);
    }

    @Override // X.AnonymousClass5WW
    public boolean Adc(Object obj, Object obj2, Object obj3, Object obj4) {
        return C12980iv.A1V(((C56002kA) obj).A03, ((C56002kA) obj2).A03);
    }

    @Override // X.AnonymousClass5WW
    public /* bridge */ /* synthetic */ void Af8(Context context, Object obj, Object obj2, Object obj3) {
    }
}
