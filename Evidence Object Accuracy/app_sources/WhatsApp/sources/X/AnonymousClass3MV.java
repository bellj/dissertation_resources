package X;

import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;
import com.google.android.exoplayer2.Timeline;
import com.whatsapp.videoplayback.ExoPlaybackControlView;
import java.util.Formatter;
import java.util.List;

/* renamed from: X.3MV  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3MV implements View.OnClickListener, AnonymousClass5XZ, SeekBar.OnSeekBarChangeListener {
    public boolean A00;
    public final /* synthetic */ ExoPlaybackControlView A01;

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void AQ4(boolean z) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void ARX(boolean z) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void ARY(boolean z) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void ASS(AnonymousClass4XL r1, int i) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void ATm(boolean z, int i) {
    }

    @Override // X.AnonymousClass5XZ
    public void ATo(C94344be r1) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void ATq(int i) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void ATr(int i) {
    }

    @Override // X.AnonymousClass5XZ
    public void ATs(AnonymousClass3A1 r1) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void AVk() {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void AWU(List list) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void AXV(Timeline timeline, int i) {
        AnonymousClass4DD.A00(this, timeline, i);
    }

    @Override // X.AnonymousClass5XZ
    public void AXl(C100564m7 r1, C92524Wg r2) {
    }

    public /* synthetic */ AnonymousClass3MV(ExoPlaybackControlView exoPlaybackControlView) {
        this.A01 = exoPlaybackControlView;
    }

    @Override // X.AnonymousClass5XZ
    public void ATt(boolean z, int i) {
        ExoPlaybackControlView exoPlaybackControlView = this.A01;
        exoPlaybackControlView.A04();
        exoPlaybackControlView.A05();
    }

    @Override // X.AnonymousClass5XZ
    public void ATx(int i) {
        ExoPlaybackControlView exoPlaybackControlView = this.A01;
        exoPlaybackControlView.A03();
        exoPlaybackControlView.A05();
    }

    @Override // X.AnonymousClass5XZ
    public void AXW(Timeline timeline, Object obj, int i) {
        ExoPlaybackControlView exoPlaybackControlView = this.A01;
        exoPlaybackControlView.A03();
        exoPlaybackControlView.A05();
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        AnonymousClass2B0 r0;
        ExoPlaybackControlView exoPlaybackControlView = this.A01;
        AbstractC115455Rp r02 = exoPlaybackControlView.A03;
        if (r02 != null) {
            AnonymousClass21S r1 = ((AnonymousClass5AT) r02).A00;
            r1.A0J(r1.A0E());
        }
        if (exoPlaybackControlView.A0E == view && (r0 = exoPlaybackControlView.A01) != null) {
            int AFl = r0.AFl();
            AnonymousClass2B0 r3 = exoPlaybackControlView.A01;
            if (AFl == 4) {
                r3.AbS(r3.ACF(), 0);
            } else {
                r3.AcW(!r3.AFj());
            }
        }
        exoPlaybackControlView.A06(300);
    }

    @Override // android.widget.SeekBar.OnSeekBarChangeListener
    public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
        long A0D;
        if (z) {
            ExoPlaybackControlView exoPlaybackControlView = this.A01;
            TextView textView = exoPlaybackControlView.A0L;
            StringBuilder sb = exoPlaybackControlView.A0Q;
            Formatter formatter = exoPlaybackControlView.A0R;
            long duration = exoPlaybackControlView.getDuration();
            if (duration == -9223372036854775807L) {
                A0D = 0;
            } else {
                A0D = C12980iv.A0D(duration * ((long) i));
            }
            textView.setText(AnonymousClass2Bd.A01(sb, formatter, A0D));
        }
    }

    @Override // android.widget.SeekBar.OnSeekBarChangeListener
    public void onStartTrackingTouch(SeekBar seekBar) {
        ExoPlaybackControlView exoPlaybackControlView = this.A01;
        exoPlaybackControlView.removeCallbacks(exoPlaybackControlView.A0O);
        AbstractC116315Uy r0 = exoPlaybackControlView.A04;
        if (r0 != null) {
            r0.AWO();
        }
        AnonymousClass2B0 r02 = exoPlaybackControlView.A01;
        if (r02 != null && r02.AFj()) {
            exoPlaybackControlView.A01.AcW(false);
            this.A00 = true;
        }
        exoPlaybackControlView.A0A = true;
    }

    @Override // android.widget.SeekBar.OnSeekBarChangeListener
    public void onStopTrackingTouch(SeekBar seekBar) {
        long A0D;
        ExoPlaybackControlView exoPlaybackControlView = this.A01;
        exoPlaybackControlView.A0A = false;
        AnonymousClass2B0 r8 = exoPlaybackControlView.A01;
        if (r8 != null) {
            int progress = seekBar.getProgress();
            long duration = exoPlaybackControlView.getDuration();
            if (duration == -9223372036854775807L) {
                A0D = 0;
            } else {
                A0D = C12980iv.A0D(duration * ((long) progress));
            }
            r8.AbS(r8.ACF(), A0D);
        }
        AnonymousClass2B0 r1 = exoPlaybackControlView.A01;
        if (r1 != null && this.A00) {
            r1.AcW(true);
        }
        this.A00 = false;
        exoPlaybackControlView.A06(3000);
    }
}
