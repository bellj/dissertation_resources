package X;

import java.util.concurrent.atomic.AtomicLong;

/* renamed from: X.20P  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass20P {
    public AtomicLong A00 = new AtomicLong(0);
    public AtomicLong A01 = new AtomicLong(0);
    public final AnonymousClass2ST A02;
    public final AnonymousClass20Q A03;
    public final AnonymousClass20Q A04;

    public AnonymousClass20P(AnonymousClass2ST r4, byte[] bArr, byte[] bArr2) {
        this.A04 = new AnonymousClass20Q(bArr);
        this.A03 = new AnonymousClass20Q(bArr2);
        this.A02 = r4;
    }
}
