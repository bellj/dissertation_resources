package X;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.5yJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129795yJ {
    public final AnonymousClass018 A00;

    public C129795yJ(AnonymousClass018 r1) {
        this.A00 = r1;
    }

    public C122245l9 A00(long j) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(new Date(j));
        return new C122245l9(this.A00, new GregorianCalendar(instance.get(1), instance.get(2), 1), instance.get(6));
    }

    public AnonymousClass6L2 A01(long j) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(new Date(j));
        return new AnonymousClass6L2(this.A00, new GregorianCalendar(instance.get(1), instance.get(2), instance.get(5)), instance.get(6));
    }

    public List A02(List list) {
        ArrayList A0l = C12960it.A0l();
        Iterator it = list.iterator();
        AnonymousClass6L2 r2 = null;
        while (it.hasNext()) {
            AnonymousClass6L2 A01 = A01(C117315Zl.A09(it).A06);
            if (r2 != null) {
                if (!r2.equals(A01)) {
                    A0l.add(r2);
                } else {
                    r2.count++;
                }
            }
            A01.count = 0;
            r2 = A01;
            r2.count++;
        }
        if (r2 != null) {
            A0l.add(r2);
        }
        return A0l;
    }
}
