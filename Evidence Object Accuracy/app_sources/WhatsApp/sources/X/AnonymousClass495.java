package X;

import java.io.IOException;

/* renamed from: X.495  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass495 extends IOException {
    public final AnonymousClass3H3 dataSpec;
    public final int type;

    public AnonymousClass495(AnonymousClass3H3 r1, IOException iOException, int i) {
        super(iOException);
        this.dataSpec = r1;
        this.type = i;
    }

    public AnonymousClass495(AnonymousClass3H3 r2, IOException iOException, String str) {
        super(str, iOException);
        this.dataSpec = r2;
        this.type = 1;
    }

    public AnonymousClass495(AnonymousClass3H3 r2, String str) {
        super(str);
        this.dataSpec = r2;
        this.type = 1;
    }
}
