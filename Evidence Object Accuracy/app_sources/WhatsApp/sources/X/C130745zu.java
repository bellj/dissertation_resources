package X;

import java.util.AbstractCollection;
import java.util.Iterator;
import org.json.JSONObject;

/* renamed from: X.5zu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130745zu {
    public final int A00;
    public final int A01;

    public C130745zu(AnonymousClass1V8 r2) {
        this.A01 = r2.A04("offset");
        this.A00 = r2.A04("length");
    }

    public C130745zu(JSONObject jSONObject) {
        this.A01 = jSONObject.getInt("offset");
        this.A00 = jSONObject.getInt("length");
    }

    public static void A00(AbstractCollection abstractCollection, Iterator it) {
        abstractCollection.add(((C130745zu) it.next()).A01().toString());
    }

    public JSONObject A01() {
        return C117295Zj.A0a().put("offset", this.A01).put("length", this.A00);
    }
}
