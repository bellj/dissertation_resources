package X;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;
import com.whatsapp.BidiToolbar;
import com.whatsapp.profile.ViewProfilePhoto;

/* renamed from: X.3XK  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3XK implements AnonymousClass5X1 {
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ Drawable A02;
    public final /* synthetic */ BidiToolbar A03;
    public final /* synthetic */ ViewProfilePhoto A04;
    public final /* synthetic */ boolean A05;

    @Override // X.AnonymousClass5X1
    public void APV(int i) {
    }

    @Override // X.AnonymousClass5X1
    public void AVv(View view) {
    }

    public AnonymousClass3XK(Drawable drawable, BidiToolbar bidiToolbar, ViewProfilePhoto viewProfilePhoto, int i, int i2, boolean z) {
        this.A04 = viewProfilePhoto;
        this.A05 = z;
        this.A02 = drawable;
        this.A03 = bidiToolbar;
        this.A01 = i;
        this.A00 = i2;
    }

    @Override // X.AnonymousClass5X1
    public void APG(View view) {
        boolean z = this.A05;
        ViewProfilePhoto viewProfilePhoto = this.A04;
        if (z) {
            viewProfilePhoto.onBackPressed();
            return;
        }
        viewProfilePhoto.finish();
        viewProfilePhoto.overridePendingTransition(0, 0);
    }

    @Override // X.AnonymousClass5X1
    public void AWA(View view, float f) {
        float f2;
        int i;
        float f3 = 1.0f - f;
        if (f3 < 0.8f) {
            f2 = 0.0f;
        } else {
            f2 = (f3 - 0.8f) / 0.19999999f;
        }
        this.A02.setAlpha((int) (255.0f * f2));
        this.A03.setAlpha(f2);
        if (Build.VERSION.SDK_INT >= 21 && (i = this.A01) != 0) {
            ViewProfilePhoto viewProfilePhoto = this.A04;
            viewProfilePhoto.getWindow().setStatusBarColor(C016907y.A03(f2, i, -16777216));
            viewProfilePhoto.getWindow().setNavigationBarColor(C016907y.A03(f2, this.A00, -16777216));
        }
    }
}
