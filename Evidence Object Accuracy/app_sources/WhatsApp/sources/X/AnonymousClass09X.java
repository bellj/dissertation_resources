package X;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/* renamed from: X.09X  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass09X extends Service implements AbstractC001200n {
    public final AnonymousClass0OR A00 = new AnonymousClass0OR(this);

    @Override // X.AbstractC001200n
    public AbstractC009904y ADr() {
        return this.A00.A02;
    }

    @Override // android.app.Service
    public IBinder onBind(Intent intent) {
        this.A00.A00(AnonymousClass074.ON_START);
        return null;
    }

    @Override // android.app.Service
    public void onCreate() {
        this.A00.A00(AnonymousClass074.ON_CREATE);
        super.onCreate();
    }

    @Override // android.app.Service
    public void onDestroy() {
        AnonymousClass0OR r1 = this.A00;
        r1.A00(AnonymousClass074.ON_STOP);
        r1.A00(AnonymousClass074.ON_DESTROY);
        super.onDestroy();
    }

    @Override // android.app.Service
    public void onStart(Intent intent, int i) {
        this.A00.A00(AnonymousClass074.ON_START);
        super.onStart(intent, i);
    }
}
