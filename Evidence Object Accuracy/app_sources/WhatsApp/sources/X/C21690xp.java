package X;

import android.os.Message;
import com.whatsapp.util.Log;

/* renamed from: X.0xp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21690xp {
    public final C16240og A00;
    public final C17220qS A01;

    public C21690xp(C16240og r1, C17220qS r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public void A00() {
        if (this.A00.A06) {
            Log.i("sendmethods/sendGetABProps");
            this.A01.A08(Message.obtain(null, 0, 220, 1), false);
        }
    }
}
