package X;

import android.app.Activity;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.IDxObserverShape3S0100000_1_I1;
import com.whatsapp.R;
import com.whatsapp.mediacomposer.MediaComposerFragment;
import com.whatsapp.mediacomposer.doodle.shapepicker.ShapePickerRecyclerView;
import com.whatsapp.mediacomposer.doodle.shapepicker.ShapePickerView;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.1KW  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1KW {
    public float A00;
    public int A01;
    public C621935i A02;
    public AbstractC64423Fm A03;
    public boolean A04;
    public final Activity A05;
    public final HandlerThread A06;
    public final View A07;
    public final View A08;
    public final ViewGroup A09;
    public final AbstractC001200n A0A;
    public final AnonymousClass018 A0B;
    public final AnonymousClass19M A0C;
    public final MediaComposerFragment A0D;
    public final AnonymousClass1BT A0E;
    public final AnonymousClass1BU A0F;
    public final C89554Kk A0G = new C89554Kk(this);
    public final AbstractC47302Ac A0H;
    public final AnonymousClass1KX A0I;
    public final HandlerC52062aB A0J;
    public final HandlerC52072aC A0K;
    public final C54822hL A0L;
    public final C470928x A0M;
    public final C470028n A0N;
    public final C53932fp A0O;
    public final ShapePickerRecyclerView A0P;
    public final ShapePickerView A0Q;
    public final AnonymousClass1CV A0R;
    public final AnonymousClass33R A0S;
    public final AnonymousClass33Q A0T;
    public final AnonymousClass1AB A0U;
    public final AnonymousClass1KM A0V = new AnonymousClass1KV(this);
    public final AnonymousClass146 A0W;
    public final C252718t A0X;
    public final C36161jQ A0Y = new C36161jQ(Boolean.FALSE);
    public final AbstractC14440lR A0Z;
    public final Map A0a = new HashMap();
    public final Map A0b = new HashMap();
    public final Set A0c = new LinkedHashSet();
    public final ConcurrentHashMap A0d = new ConcurrentHashMap();
    public final ConcurrentHashMap A0e = new ConcurrentHashMap();
    public final ConcurrentHashMap A0f;

    public AnonymousClass1KW(Activity activity, ViewGroup viewGroup, AbstractC001200n r26, AbstractC001400p r27, AnonymousClass018 r28, AnonymousClass19M r29, MediaComposerFragment mediaComposerFragment, AnonymousClass1BT r31, AnonymousClass1BU r32, AbstractC47302Ac r33, ShapePickerView shapePickerView, AnonymousClass1CV r35, AnonymousClass1AB r36, AnonymousClass146 r37, C235512c r38, C252718t r39, AbstractC14440lR r40) {
        this.A05 = activity;
        this.A0X = r39;
        this.A0Z = r40;
        this.A0C = r29;
        this.A0E = r31;
        this.A0B = r28;
        this.A0W = r37;
        this.A0R = r35;
        this.A0F = r32;
        this.A0U = r36;
        this.A0A = r26;
        this.A0H = r33;
        this.A09 = viewGroup;
        this.A0D = mediaComposerFragment;
        this.A0O = (C53932fp) new AnonymousClass02A(new AnonymousClass3RW(), r27).A00(C53932fp.class);
        this.A0N = (C470028n) new AnonymousClass02A(new AnonymousClass3RW(), r27).A00(C470028n.class);
        AnonymousClass1KX r1 = new AnonymousClass1KX(this);
        this.A0I = r1;
        this.A0Q = shapePickerView;
        this.A08 = AnonymousClass028.A0D(shapePickerView, R.id.shape_picker_gradient);
        this.A07 = AnonymousClass028.A0D(shapePickerView, R.id.shape_picker_header);
        ShapePickerRecyclerView shapePickerRecyclerView = (ShapePickerRecyclerView) AnonymousClass028.A0D(shapePickerView, R.id.shapes);
        this.A0P = shapePickerRecyclerView;
        shapePickerRecyclerView.setAdapter(r1);
        List<AbstractC470728v> A00 = AnonymousClass3GW.A00();
        this.A0f = new ConcurrentHashMap();
        for (AbstractC470728v r6 : A00) {
            this.A0f.put(r6.AH5(), r6);
        }
        this.A0S = new AnonymousClass33R((RecyclerView) shapePickerView.findViewById(R.id.emoji_shape_subcategories_recyclerview), this.A0G, this.A0P);
        AnonymousClass33Q r12 = new AnonymousClass33Q((RecyclerView) shapePickerView.findViewById(R.id.sticker_shape_subcategories_recyclerview), this.A0G, this.A0P, r38);
        this.A0T = r12;
        this.A03 = r12;
        ((AbstractC64423Fm) r12).A01 = true;
        r12.A01();
        C54822hL r13 = new C54822hL(this.A05, this);
        this.A0L = r13;
        this.A0P.A0n(r13);
        this.A0P.setItemAnimator(null);
        this.A0O.A01.A05(r26, new AnonymousClass02B() { // from class: X.3QQ
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                AnonymousClass1KW r4 = AnonymousClass1KW.this;
                if (C12970iu.A1Y(obj)) {
                    AnonymousClass1BT r14 = r4.A0E;
                    for (AbstractC470728v r2 : r14.A03(r14.A00())) {
                        r4.A0f.put(r2.AH5(), r2);
                    }
                    r4.A0I.A0G();
                }
            }
        });
        this.A0O.A00.A05(r26, new AnonymousClass02B() { // from class: X.4tI
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                AnonymousClass1KW r2 = AnonymousClass1KW.this;
                C90424Nv r4 = (C90424Nv) obj;
                if (r4.A01) {
                    r2.A03(r4.A00, false);
                    r2.A0I.A0G();
                }
            }
        });
        this.A0O.A02.A05(r26, new AnonymousClass02B() { // from class: X.3QT
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                AnonymousClass1KZ r0;
                AnonymousClass1KW r5 = AnonymousClass1KW.this;
                int A05 = C12960it.A05(obj);
                if (A05 == 1) {
                    AnonymousClass1KX r14 = r5.A0I;
                    AnonymousClass009.A0F(r14.A05.A0O.A04(1));
                    r14.A04(r14.A04.size());
                } else if (A05 == 2) {
                    C53932fp r02 = r5.A0O;
                    ArrayList A0l = C12960it.A0l();
                    Map map = r02.A03;
                    Iterator A0o = C12960it.A0o(map);
                    while (A0o.hasNext()) {
                        AnonymousClass017 r15 = (AnonymousClass017) A0o.next();
                        if (!(r15 == null || r15.A01() == null || (r0 = ((C90434Nw) r15.A01()).A00) == null)) {
                            A0l.add(r0);
                        }
                    }
                    for (int i = 0; i < A0l.size(); i++) {
                        r5.A02((AnonymousClass1KZ) A0l.get(i));
                    }
                    AnonymousClass1KX r62 = r5.A0I;
                    AnonymousClass009.A0F(r62.A05.A0O.A04(2));
                    Iterator it = A0l.iterator();
                    while (it.hasNext()) {
                        AnonymousClass1KZ r03 = (AnonymousClass1KZ) it.next();
                        r62.A03.put(r03.A0D, r62.A0F(r03));
                    }
                    r62.A0H();
                    ((AnonymousClass02M) r62).A01.A02(r62.A04.size(), r62.A0D() - r62.A04.size());
                    AbstractC001200n r3 = r5.A0A;
                    IDxObserverShape3S0100000_1_I1 iDxObserverShape3S0100000_1_I1 = new IDxObserverShape3S0100000_1_I1(r5, 82);
                    Iterator A0o2 = C12960it.A0o(map);
                    while (A0o2.hasNext()) {
                        ((AnonymousClass017) A0o2.next()).A05(r3, iDxObserverShape3S0100000_1_I1);
                    }
                } else if (A05 == 3) {
                    AnonymousClass1BU r2 = r5.A0F;
                    synchronized (r2) {
                        r2.A04.clear();
                    }
                    Iterator A0o3 = C12960it.A0o(r5.A0b);
                    while (A0o3.hasNext()) {
                        r2.A01((Collection) A0o3.next());
                    }
                    AnonymousClass1KX r16 = r5.A0I;
                    AnonymousClass009.A0F(r16.A05.A0O.A04(3));
                    r16.A05(r16.A0D());
                    r16.A0I();
                } else {
                    throw C12970iu.A0f(C12960it.A0W(A05, "Unknown state: "));
                }
            }
        });
        C53932fp r0 = this.A0O;
        C621935i r14 = new C621935i(r0.A01, r0.A02, r0.A00, r31, r38);
        this.A02 = r14;
        ((C38711oa) r14).A00 = this.A0O;
        r40.Aaz(r14, new Void[0]);
        this.A0N.A03.A05(r26, new AnonymousClass02B() { // from class: X.3QS
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                AbstractC64423Fm r15;
                AnonymousClass1KW r4 = AnonymousClass1KW.this;
                int A05 = C12960it.A05(obj);
                AnonymousClass1KX r16 = r4.A0I;
                r16.A02 = null;
                r16.A0G();
                r16.A02();
                AbstractC64423Fm r02 = r4.A03;
                r02.A01 = false;
                r02.A01();
                if (A05 == 0) {
                    r15 = r4.A0T;
                } else {
                    r15 = r4.A0S;
                }
                r4.A03 = r15;
                r15.A01 = true;
                r15.A01();
                if (r4.A03.A06.getVisibility() == 0) {
                    AbstractC64423Fm r03 = r4.A03;
                    AnonymousClass009.A05(r03);
                    r03.A09.A0F(0);
                }
                r4.A0P.A0Y(0);
            }
        });
        this.A0N.A01.A05(r26, new AnonymousClass02B() { // from class: X.4tJ
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                AnonymousClass1KW r2 = AnonymousClass1KW.this;
                if (!C12970iu.A1Y(obj) || r2.A0Q.getVisibility() != 0) {
                    r2.A0P.A0Y(0);
                    r2.A0L.A01 = 0;
                }
            }
        });
        this.A0N.A02.A05(r26, new AnonymousClass02B() { // from class: X.3QU
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                AnonymousClass1KW r15 = AnonymousClass1KW.this;
                String str = (String) obj;
                if (TextUtils.isEmpty(str)) {
                    AnonymousClass1KX r16 = r15.A0I;
                    r16.A02 = null;
                    r16.A0G();
                    r16.A02();
                    return;
                }
                AnonymousClass1KX r4 = r15.A0I;
                AnonymousClass1CV r3 = r15.A0R;
                C470028n r02 = r15.A0N;
                AnonymousClass009.A05(r02);
                int intValue = ((Number) r02.A03.A01()).intValue();
                boolean z = false;
                if (intValue == 0) {
                    z = true;
                }
                AnonymousClass4VM A002 = r3.A00(str, null, !z);
                AnonymousClass4VM r03 = r4.A02;
                if (r03 != null) {
                    r03.A00 = null;
                }
                r4.A02 = A002;
                A002.A00(r4);
            }
        });
        this.A0N.A00.A05(r26, new AnonymousClass02B() { // from class: X.4tK
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                AbstractC64423Fm r15 = AnonymousClass1KW.this.A03;
                r15.A01 = !((Boolean) obj).booleanValue();
                r15.A01();
            }
        });
        this.A0M = new C470928x(activity, this.A07, r26, r27, r28, r35, r39, this.A0Y);
        this.A0Y.A05(r26, new AnonymousClass02B() { // from class: X.3QR
            /* JADX WARNING: Code restructure failed: missing block: B:16:0x0045, code lost:
                if (X.C12970iu.A1Y(r5.A0Y.A01()) != false) goto L_0x002e;
             */
            @Override // X.AnonymousClass02B
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void ANq(java.lang.Object r8) {
                /*
                    r7 = this;
                    X.1KW r5 = X.AnonymousClass1KW.this
                    boolean r1 = X.C12970iu.A1Y(r8)
                    X.28x r4 = r5.A0M
                    com.whatsapp.ClearableEditText r3 = r4.A0A
                    boolean r0 = r3.hasFocus()
                    boolean r6 = X.C12960it.A1S(r0)
                    if (r1 == 0) goto L_0x0048
                    android.view.ViewGroup r2 = r5.A09
                    r0 = 2131365969(0x7f0a1051, float:1.8351818E38)
                    android.view.View r0 = r2.findViewById(r0)
                    if (r0 == 0) goto L_0x0037
                    android.view.View r1 = r5.A07
                    if (r0 == r1) goto L_0x0037
                    r2.removeView(r0)
                L_0x0026:
                    com.whatsapp.mediacomposer.doodle.shapepicker.ShapePickerView r0 = r5.A0Q
                    r0.removeView(r1)
                    r2.addView(r1)
                L_0x002e:
                    if (r6 == 0) goto L_0x0036
                    X.AnonymousClass009.A05(r4)
                    r3.requestFocus()
                L_0x0036:
                    return
                L_0x0037:
                    android.view.View r1 = r5.A07
                    if (r0 != r1) goto L_0x0026
                    X.1jQ r0 = r5.A0Y
                    java.lang.Object r0 = r0.A01()
                    boolean r0 = X.C12970iu.A1Y(r0)
                    if (r0 == 0) goto L_0x0026
                    goto L_0x002e
                L_0x0048:
                    android.view.ViewGroup r1 = r5.A09
                    r2 = 2131365969(0x7f0a1051, float:1.8351818E38)
                    android.view.View r0 = r1.findViewById(r2)
                    r1.removeView(r0)
                    com.whatsapp.mediacomposer.doodle.shapepicker.ShapePickerView r1 = r5.A0Q
                    android.view.View r0 = r1.findViewById(r2)
                    if (r0 != 0) goto L_0x002e
                    android.view.View r0 = r5.A07
                    r1.addView(r0)
                    goto L_0x002e
                */
                throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3QR.ANq(java.lang.Object):void");
            }
        });
        shapePickerView.findViewById(R.id.shape_picker_header_background).setVisibility(0);
        this.A08.setVisibility(8);
        this.A0P.setClipToPadding(true);
        this.A0P.A0l(new C54632h2(this, this.A05.getResources().getDimensionPixelSize(R.dimen.shape_picker_shape_top_margin), this.A05.getResources().getDimensionPixelSize(R.dimen.shape_picker_section_top_margin), r28.A04().A06));
        r37.A03(this.A0V);
        HandlerThread handlerThread = new HandlerThread("Shapes Thread", 1);
        this.A06 = handlerThread;
        handlerThread.start();
        this.A0K = new HandlerC52072aC(activity, handlerThread.getLooper(), this);
        this.A0J = new HandlerC52062aB(activity, activity.getMainLooper(), this);
        int dimensionPixelSize = this.A05.getResources().getDimensionPixelSize(R.dimen.doodle_shape_picker_grid_size);
        for (int min = Math.min(((this.A05.getResources().getDisplayMetrics().widthPixels * this.A05.getResources().getDisplayMetrics().heightPixels) / (dimensionPixelSize * dimensionPixelSize)) + 1, A00.size() - 1); min >= 0; min--) {
            Message obtain = Message.obtain(this.A0K, 0, 0, 0, null);
            String AH5 = ((AbstractC470728v) A00.get(min)).AH5();
            Bundle bundle = new Bundle();
            bundle.putString("tag_bundle_key", AH5);
            obtain.setData(bundle);
            obtain.sendToTarget();
        }
    }

    public static /* synthetic */ Bundle A00(String str) {
        Bundle bundle = new Bundle();
        bundle.putString("tag_bundle_key", str);
        return bundle;
    }

    public static /* synthetic */ String A01(Bundle bundle) {
        String string = bundle.getString("tag_bundle_key");
        AnonymousClass009.A05(string);
        return string;
    }

    public final void A02(AnonymousClass1KZ r6) {
        ArrayList arrayList = new ArrayList();
        for (AnonymousClass1KS r2 : r6.A04) {
            arrayList.add(new C470828w(r2, this.A0U));
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            C470828w r22 = (C470828w) it.next();
            this.A0f.put(r22.AH5(), r22);
        }
        Map map = this.A0a;
        String str = r6.A0D;
        map.put(str, r6);
        this.A0b.put(str, arrayList);
    }

    public final void A03(Collection collection, boolean z) {
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            C470828w r2 = new C470828w((AnonymousClass1KS) it.next(), this.A0U);
            this.A0f.put(r2.AH5(), r2);
            linkedHashSet.add(r2);
        }
        if (z) {
            Set set = this.A0c;
            linkedHashSet.addAll(set);
            set.clear();
        }
        Set<AbstractC470728v> set2 = this.A0c;
        set2.addAll(linkedHashSet);
        AnonymousClass1BU r3 = this.A0F;
        synchronized (r3) {
            Map map = r3.A03;
            map.clear();
            for (AbstractC470728v r0 : set2) {
                AnonymousClass1BU.A00(r0, map);
            }
        }
    }
}
