package X;

import java.util.Collection;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0x5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21230x5 implements AbstractC21180x0 {
    public final C14850m9 A00;
    public final AbstractC21180x0 A01;
    public volatile Boolean A02;

    public C21230x5(C14850m9 r1, AbstractC21180x0 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public final boolean A00() {
        if (this.A02 == null) {
            synchronized (this) {
                if (this.A02 == null) {
                    this.A02 = Boolean.valueOf(this.A00.A07(433));
                }
            }
        }
        return this.A02.booleanValue();
    }

    @Override // X.AbstractC21180x0
    public void A9P(short s, boolean z) {
        this.A01.A9P(630, true);
    }

    @Override // X.AbstractC21180x0
    public void A9Q(int i, short s) {
        this.A01.A9Q(300000, 113);
    }

    @Override // X.AbstractC21180x0
    public Collection AAh() {
        return this.A01.AAh();
    }

    @Override // X.AbstractC21180x0
    public boolean AID() {
        return this.A01.AID();
    }

    @Override // X.AbstractC21180x0
    public boolean AJj(int i) {
        return this.A01.AJj(i);
    }

    @Override // X.AbstractC21180x0
    public void AKu(int i, String str, int i2) {
        if (!A00()) {
            this.A01.AKu(689639794, "scrollSurface", i2);
        }
    }

    @Override // X.AbstractC21180x0
    public void AKv(int i, String str, long j) {
        if (!A00()) {
            this.A01.AKv(i, str, j);
        }
    }

    @Override // X.AbstractC21180x0
    public void AKw(int i, String str, String str2) {
        if (!A00()) {
            this.A01.AKw(i, str, str2);
        }
    }

    @Override // X.AbstractC21180x0
    public void AKx(int i, String str, boolean z) {
        if (!A00()) {
            this.A01.AKx(i, str, z);
        }
    }

    @Override // X.AbstractC21180x0
    public void AKy(String str, int i, int i2, int i3) {
        if (!A00()) {
            this.A01.AKy(str, i, i2, i3);
        }
    }

    @Override // X.AbstractC21180x0
    public void AKz(String str, int i, int i2, long j) {
        if (!A00()) {
            this.A01.AKz(str, i, i2, j);
        }
    }

    @Override // X.AbstractC21180x0
    public void AL0(String str, String str2, int i, int i2) {
        if (!A00()) {
            this.A01.AL0(str, str2, i, i2);
        }
    }

    @Override // X.AbstractC21180x0
    public void AL1(String str, int i, int i2, boolean z) {
        if (!A00()) {
            this.A01.AL1(str, i, i2, z);
        }
    }

    @Override // X.AbstractC21180x0
    public void AL2(String str, double d, int i) {
        if (!A00()) {
            this.A01.AL2(str, d, 689639794);
        }
    }

    @Override // X.AbstractC21180x0
    public void AL3(String str, String[] strArr, int i) {
        if (!A00()) {
            this.A01.AL3("inflated_rows", strArr, i);
        }
    }

    @Override // X.AbstractC21180x0
    public void AL4(AbstractC29031Pz r2, int i) {
        if (!A00()) {
            this.A01.AL4(r2, i);
        }
    }

    @Override // X.AbstractC21180x0
    public void AL5(int i, int i2) {
        if (!A00()) {
            this.A01.AL5(926875649, i2);
        }
    }

    @Override // X.AbstractC21180x0
    public void AL6(int i, int i2, short s) {
        if (!A00()) {
            this.A01.AL6(i, i2, s);
        }
    }

    @Override // X.AbstractC21180x0
    public void AL7(int i, short s) {
        if (!A00()) {
            this.A01.AL7(i, s);
        }
    }

    @Override // X.AbstractC21180x0
    public void AL8(String str, int i, int i2, short s) {
        if (!A00()) {
            this.A01.AL8("start_foreground_service", i, i2, 213);
        }
    }

    @Override // X.AbstractC21180x0
    public boolean ALA(int i) {
        return this.A01.ALA(689639794);
    }

    @Override // X.AbstractC21180x0
    public void ALB(int i, String str) {
        if (!A00()) {
            this.A01.ALB(i, str);
        }
    }

    @Override // X.AbstractC21180x0
    public void ALC(int i, String str, int i2) {
        if (!A00()) {
            this.A01.ALC(i, str, i2);
        }
    }

    @Override // X.AbstractC21180x0
    public void ALD(String str, TimeUnit timeUnit, int i, long j) {
        if (!A00()) {
            this.A01.ALD(str, timeUnit, i, j);
        }
    }

    @Override // X.AbstractC21180x0
    public void ALE(int i) {
        if (!A00()) {
            this.A01.ALE(i);
        }
    }

    @Override // X.AbstractC21180x0
    public void ALF(int i, int i2) {
        if (!A00()) {
            this.A01.ALF(i, i2);
        }
    }

    @Override // X.AbstractC21180x0
    public void ALG(int i, String str, String str2) {
        if (!A00()) {
            this.A01.ALG(i, str, str2);
        }
    }

    @Override // X.AbstractC21180x0
    public void ALH(String str, String str2, int i, int i2, boolean z) {
        if (!A00()) {
            this.A01.ALH("perf_origin", str2, i, i2, z);
        }
    }

    @Override // X.AbstractC21180x0
    public void ALI(String str, String str2, TimeUnit timeUnit, int i, long j) {
        if (!A00()) {
            this.A01.ALI("perf_origin", str2, timeUnit, i, j);
        }
    }

    @Override // X.AbstractC21180x0
    public Long AZ2() {
        return this.A01.AZ2();
    }

    @Override // X.AbstractC21180x0
    public Integer AZ3() {
        return this.A01.AZ3();
    }

    @Override // X.AbstractC21180x0
    public String AZK() {
        return this.A01.AZK();
    }

    @Override // X.AbstractC21180x0
    public void AgE() {
        this.A01.AgE();
    }
}
