package X;

/* renamed from: X.0Ih  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C03570Ih extends AbstractC92144Us {
    public final C94004b6 A00;
    public final Throwable A01;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C03570Ih) {
                C03570Ih r5 = (C03570Ih) obj;
                if (!C16700pc.A0O(this.A01, r5.A01) || !C16700pc.A0O(this.A00, r5.A00)) {
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (this.A01.hashCode() * 31) + this.A00.hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Failure(exception=");
        sb.append(this.A01);
        sb.append(", fetchSummaryData=");
        sb.append(this.A00);
        sb.append(')');
        return sb.toString();
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C03570Ih(C94004b6 r2, Throwable th) {
        super(r2);
        C16700pc.A0E(th, 1);
        this.A01 = th;
        this.A00 = r2;
    }

    public final Throwable A01() {
        return this.A01;
    }
}
