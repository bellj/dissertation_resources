package X;

import android.content.Context;
import com.whatsapp.payments.ui.NoviPayBloksActivity;
import com.whatsapp.util.Log;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.5fq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120235fq extends AbstractC451020e {
    public final /* synthetic */ AnonymousClass3FE A00;
    public final /* synthetic */ NoviPayBloksActivity A01;
    public final /* synthetic */ Map A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120235fq(Context context, C14900mE r2, AnonymousClass3FE r3, C18650sn r4, NoviPayBloksActivity noviPayBloksActivity, Map map) {
        super(context, r2, r4);
        this.A01 = noviPayBloksActivity;
        this.A00 = r3;
        this.A02 = map;
    }

    @Override // X.AbstractC451020e
    public void A02(C452120p r5) {
        C129865yQ r3 = ((AbstractActivityC123635nW) this.A01).A05;
        AnonymousClass3FE r2 = this.A00;
        r3.A03(r5, new Runnable() { // from class: X.6Gd
            @Override // java.lang.Runnable
            public final void run() {
                C117305Zk.A1E(AnonymousClass3FE.this);
            }
        }, new Runnable() { // from class: X.6Ga
            @Override // java.lang.Runnable
            public final void run() {
                AnonymousClass3FE.this.A00("on_forced_update");
            }
        });
    }

    @Override // X.AbstractC451020e
    public void A03(C452120p r5) {
        C129865yQ r3 = ((AbstractActivityC123635nW) this.A01).A05;
        AnonymousClass3FE r2 = this.A00;
        r3.A03(r5, new Runnable() { // from class: X.6Gc
            @Override // java.lang.Runnable
            public final void run() {
                C117305Zk.A1E(AnonymousClass3FE.this);
            }
        }, new Runnable() { // from class: X.6Ge
            @Override // java.lang.Runnable
            public final void run() {
                AnonymousClass3FE.this.A00("on_forced_update");
            }
        });
    }

    @Override // X.AbstractC451020e
    public void A04(AnonymousClass1V8 r21) {
        C126795tS[] r6;
        AnonymousClass619 r16;
        try {
            AnonymousClass1V8 A0F = r21.A0F("account");
            NoviPayBloksActivity noviPayBloksActivity = this.A01;
            int A05 = A0F.A05("tos-version", 0);
            AnonymousClass1V8 A0F2 = A0F.A0F("tos");
            AnonymousClass1V8 A0F3 = A0F2.A0F("header");
            AnonymousClass619 A01 = AnonymousClass619.A01(A0F3, "title");
            AnonymousClass619 A012 = AnonymousClass619.A01(A0F3, "description");
            AnonymousClass619 A013 = AnonymousClass619.A01(A0F2, "footer");
            List A0J = A0F2.A0J("section");
            AnonymousClass619 r17 = null;
            if (A0J.size() > 0) {
                r6 = new C126795tS[A0J.size()];
                Iterator it = A0J.iterator();
                while (it.hasNext()) {
                    AnonymousClass1V8 A0d = C117305Zk.A0d(it);
                    int A052 = A0d.A05("sequence", 0);
                    AnonymousClass619 A014 = AnonymousClass619.A01(A0d, "title");
                    AnonymousClass619 A015 = AnonymousClass619.A01(A0d, "description");
                    if (A052 >= 0 && A052 < A0J.size()) {
                        r6[A052] = new C126795tS(A014, A015);
                    }
                }
            } else {
                r6 = null;
            }
            AnonymousClass1V8 A0E = A0F.A0E("retos");
            if (A0E != null) {
                r16 = AnonymousClass619.A01(A0E, "title");
                r17 = AnonymousClass619.A01(A0E, "description");
            } else {
                r16 = null;
            }
            C128085vX r12 = new C128085vX(A01, A012, A013, r16, r17, r6, A05);
            noviPayBloksActivity.A05 = r12;
            noviPayBloksActivity.A31(this.A00, r12, this.A02);
        } catch (AnonymousClass1V9 unused) {
            Log.e("PAY: NoviPayBloksActivity/getTosContent can't parse account node");
            ((AbstractActivityC123635nW) this.A01).A05.A02(null, new Runnable() { // from class: X.6Gb
                @Override // java.lang.Runnable
                public final void run() {
                    C117305Zk.A1E(AnonymousClass3FE.this);
                }
            }, null);
        }
    }
}
