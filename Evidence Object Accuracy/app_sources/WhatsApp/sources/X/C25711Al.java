package X;

import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;
import javax.crypto.AEADBadTagException;

/* renamed from: X.1Al  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C25711Al {
    public int A00;
    public final C15570nT A01;
    public final C15820nx A02;
    public final AnonymousClass10I A03;
    public final C22730zY A04;
    public final C15810nw A05;
    public final C17050qB A06;
    public final C16590pI A07;
    public final C14820m6 A08;
    public final C19490uC A09;
    public final C15880o3 A0A;
    public final C21780xy A0B;
    public final C16600pJ A0C;
    public final AnonymousClass15D A0D;

    public C25711Al(C15570nT r1, C15820nx r2, AnonymousClass10I r3, C22730zY r4, C15810nw r5, C17050qB r6, C16590pI r7, C14820m6 r8, C19490uC r9, C15880o3 r10, C21780xy r11, C16600pJ r12, AnonymousClass15D r13) {
        this.A07 = r7;
        this.A0D = r13;
        this.A01 = r1;
        this.A05 = r5;
        this.A09 = r9;
        this.A02 = r2;
        this.A06 = r6;
        this.A0A = r10;
        this.A08 = r8;
        this.A0C = r12;
        this.A03 = r3;
        this.A04 = r4;
        this.A0B = r11;
    }

    public boolean A00(Uri uri) {
        try {
            File file = (File) this.A05.A03.get();
            if (file != null) {
                EnumC16570pG r13 = EnumC16570pG.A06;
                AnonymousClass15D r14 = this.A0D;
                C15570nT r5 = this.A01;
                C19490uC r10 = this.A09;
                C15820nx r8 = this.A02;
                C17050qB r9 = this.A06;
                C16600pJ r12 = this.A0C;
                return C33231df.A00(r5, new C68083Ua(uri, this.A07), null, r8, r9, r10, this.A0B, r12, r13, r14).A05(new AbstractC37491mU(file) { // from class: X.3UZ
                    public final /* synthetic */ File A00;

                    {
                        this.A00 = r1;
                    }

                    @Override // X.AbstractC37491mU
                    public final Object apply(Object obj) {
                        File file2 = this.A00;
                        String str = (String) obj;
                        if (!TextUtils.isEmpty(str)) {
                            try {
                                File A05 = C14350lI.A05(file2.getCanonicalPath(), str);
                                File parentFile = A05.getParentFile();
                                if (parentFile != null && !parentFile.exists()) {
                                    parentFile.mkdirs();
                                }
                                return A05;
                            } catch (IOException e) {
                                Log.e("backup-export-storage/restore-with-uri/restore-multi-file-backup/failed/", e);
                            }
                        }
                        return null;
                    }
                }, true);
            }
        } catch (IOException e) {
            if (Build.VERSION.SDK_INT >= 19 && e.getCause() != null && (e.getCause() instanceof AEADBadTagException)) {
                return true;
            }
            if (e.getMessage() != null && e.getMessage().contains("mac check in GCM failed")) {
                return true;
            }
        } catch (Exception e2) {
            Log.e("backup-export-storage/restore/failed/", e2);
            return false;
        }
        return false;
    }
}
