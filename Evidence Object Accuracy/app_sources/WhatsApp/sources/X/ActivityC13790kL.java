package X;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import com.facebook.redex.ViewOnClickCListenerShape6S0100000_I1;
import com.whatsapp.R;
import com.whatsapp.WaInAppBrowsingActivity;
import com.whatsapp.avatar.home.AvatarHomeActivity;
import com.whatsapp.avatar.home.AvatarHomeViewModel;
import com.whatsapp.conversation.conversationrow.message.MessageDetailsActivity;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.mediacomposer.MediaComposerActivity;
import com.whatsapp.mediacomposer.MediaComposerFragment;
import com.whatsapp.support.faq.FaqItemActivity;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/* renamed from: X.0kL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ActivityC13790kL extends AbstractActivityC13800kM implements AbstractC13880kU {
    public AnonymousClass12P A00;
    public C15570nT A01;
    public C252818u A02;
    public C22670zS A03;
    public C15810nw A04;
    public C14830m7 A05;
    public C14950mJ A06;
    public C15880o3 A07;
    public C249317l A08;
    public AnonymousClass2FO A09;
    public C21840y4 A0A;
    public C15510nN A0B;
    public C21820y2 A0C;
    public C252718t A0D;
    public boolean A0E = true;
    public AnonymousClass2GP A0F;
    public boolean A0G;
    public final Set A0H = new CopyOnWriteArraySet();

    public boolean A2c() {
        return false;
    }

    public static MenuItem A0P(Menu menu) {
        MenuItem findItem = menu.findItem(R.id.menu_cart);
        findItem.setActionView(R.layout.menu_item_cart);
        AnonymousClass23N.A01(findItem.getActionView());
        return findItem;
    }

    public static Toolbar A0Q(ActivityC000800j r1) {
        return (Toolbar) r1.findViewById(R.id.toolbar);
    }

    public static AvatarHomeViewModel A0R(ViewOnClickCListenerShape6S0100000_I1 viewOnClickCListenerShape6S0100000_I1) {
        AvatarHomeActivity avatarHomeActivity = (AvatarHomeActivity) viewOnClickCListenerShape6S0100000_I1.A00;
        C16700pc.A0E(avatarHomeActivity, 0);
        return (AvatarHomeViewModel) avatarHomeActivity.A0M.getValue();
    }

    public static C249317l A0S(AnonymousClass2FL r1, AnonymousClass01J r2, ActivityC13790kL r3, AnonymousClass01N r4) {
        r3.A01 = (C15570nT) r4.get();
        r3.A04 = (C15810nw) r2.A73.get();
        r3.A09 = r1.A06();
        r3.A06 = (C14950mJ) r2.AKf.get();
        r3.A00 = (AnonymousClass12P) r2.A0H.get();
        r3.A02 = (C252818u) r2.AMy.get();
        r3.A03 = (C22670zS) r2.A0V.get();
        r3.A0A = (C21840y4) r2.ACr.get();
        r3.A07 = (C15880o3) r2.ACF.get();
        r3.A0C = (C21820y2) r2.AHx.get();
        r3.A0B = (C15510nN) r2.AHZ.get();
        return (C249317l) r2.A8B.get();
    }

    public static C39341ph A0T(MediaComposerFragment mediaComposerFragment) {
        return ((MediaComposerActivity) ((AnonymousClass21Y) mediaComposerFragment.A0B())).A1B.A00(mediaComposerFragment.A00);
    }

    public static C15580nU A0U(Intent intent, String str) {
        C15580nU A04 = C15580nU.A04(intent.getStringExtra(str));
        AnonymousClass009.A05(A04);
        return A04;
    }

    public static UserJid A0V(Intent intent, String str) {
        UserJid nullable = UserJid.getNullable(intent.getStringExtra(str));
        AnonymousClass009.A05(nullable);
        return nullable;
    }

    public static String A0W(Activity activity) {
        Intent intent = activity.getIntent();
        if (intent == null) {
            return null;
        }
        return intent.getStringExtra("extra_referral_screen");
    }

    public static List A0X(Activity activity) {
        return C15380n4.A07(UserJid.class, activity.getIntent().getStringArrayListExtra("jids"));
    }

    public static AnonymousClass01N A0Y(AnonymousClass01J r1, ActivityC13790kL r2) {
        r2.A05 = (C14830m7) r1.ALb.get();
        r2.A0D = (C252718t) r1.A9K.get();
        return r1.AAr;
    }

    private void A0Z() {
        getResources().getConfiguration().fontScale = getApplicationContext().getResources().getConfiguration().fontScale;
        getResources().updateConfiguration(getResources().getConfiguration(), getResources().getDisplayMetrics());
    }

    public static void A0a(Activity activity) {
        if (AbstractC454421p.A00) {
            Window window = activity.getWindow();
            window.requestFeature(12);
            window.requestFeature(13);
        }
    }

    public static void A0b(Context context, Menu menu) {
        menu.add(0, R.id.menuitem_contactqr_share, 0, R.string.contact_qr_share).setIcon(AnonymousClass2GE.A01(context, R.drawable.ic_share, R.color.shareIconTint)).setShowAsAction(2);
        menu.add(0, R.id.menuitem_contactqr_revoke, 0, R.string.contact_qr_revoke);
    }

    public static void A0c(Context context, Toolbar toolbar, AnonymousClass018 r5) {
        toolbar.setNavigationIcon(new AnonymousClass2GF(AnonymousClass2GE.A04(context.getResources().getDrawable(R.drawable.ic_back), context.getResources().getColor(R.color.lightActionBarItemDrawableTint)), r5));
    }

    public static void A0d(TextView textView, MessageDetailsActivity messageDetailsActivity, long j) {
        textView.setText(C38131nZ.A01(((ActivityC13830kP) messageDetailsActivity).A01, ((ActivityC13790kL) messageDetailsActivity).A05.A02(j)));
    }

    public static void A0e(AnonymousClass01J r1, AnonymousClass2GL r2, C249317l r3) {
        ((ActivityC13790kL) r2).A08 = r3;
        r2.A06 = (C231510o) r1.AHO.get();
        r2.A03 = (C15550nR) r1.A45.get();
        r2.A04 = (C15610nY) r1.AMe.get();
        r2.A07 = (AnonymousClass193) r1.A6S.get();
        r2.A0A = (C16630pM) r1.AIc.get();
        r2.A0C = C18000rk.A00(r1.A4v);
    }

    public static void A0f(AnonymousClass01J r1, WaInAppBrowsingActivity waInAppBrowsingActivity, C249317l r3) {
        ((ActivityC13790kL) waInAppBrowsingActivity).A08 = r3;
        waInAppBrowsingActivity.A03 = (AnonymousClass18U) r1.AAU.get();
        waInAppBrowsingActivity.A04 = (C22180yf) r1.A5U.get();
    }

    public static /* synthetic */ void A0h(ActivityC13790kL r4) {
        ((ActivityC13810kN) r4).A09.A00.edit().putBoolean("smb_client_viewed_eu_tos_update", true).apply();
        r4.A00.A06(r4, new Intent("android.intent.action.VIEW", r4.A02.A00(((ActivityC13810kN) r4).A09.A00.getString("smb_eu_tos_update_url", null))));
    }

    public static /* synthetic */ void A0j(ActivityC13790kL r4, Integer num) {
        Intent className = new Intent().setClassName(r4.getPackageName(), "com.whatsapp.blockinguserinteraction.BlockingUserInteractionActivity");
        className.putExtra("blocking_type", 1);
        if (num.intValue() == 1) {
            r4.finish();
            r4.startActivity(className);
        }
    }

    public static void A0k(FaqItemActivity faqItemActivity) {
        faqItemActivity.A02 += System.currentTimeMillis() - faqItemActivity.A01;
        faqItemActivity.A01 = System.currentTimeMillis();
        faqItemActivity.setResult(-1, new Intent().putExtra("article_id", faqItemActivity.A00).putExtra("total_time_spent", faqItemActivity.A02));
    }

    private boolean A0l() {
        return getApplicationContext().getResources().getConfiguration().fontScale == getResources().getConfiguration().fontScale;
    }

    public void A2T() {
    }

    public void A2U() {
        if (Boolean.TRUE.equals(this.A07.A03.A01())) {
            Intent className = new Intent().setClassName(getPackageName(), "com.whatsapp.blockinguserinteraction.BlockingUserInteractionActivity");
            className.putExtra("blocking_type", 0);
            finish();
            startActivity(className);
        }
    }

    public void A2V() {
        int A00 = this.A0B.A00();
        C15570nT r0 = this.A01;
        r0.A08();
        if (r0.A00 == null && A00 == 11 && !isFinishing()) {
            Intent intent = new Intent();
            intent.setClassName(getPackageName(), "com.whatsapp.migration.export.ui.ExportMigrationDataExportedActivity");
            intent.setFlags(33554432);
            startActivity(intent);
            finish();
        }
    }

    public void A2W() {
        if (this.A08.A00() == 1 || this.A08.A00() == 4 || this.A08.A00() == 3) {
            Intent className = new Intent().setClassName(getPackageName(), "com.whatsapp.blockinguserinteraction.BlockingUserInteractionActivity");
            className.putExtra("blocking_type", 1);
            finish();
            startActivity(className);
        } else if (this.A08.A00() == 0) {
            C249317l r0 = this.A08;
            r0.A01.A05(this, new AnonymousClass02B() { // from class: X.2GT
                @Override // X.AnonymousClass02B
                public final void ANq(Object obj) {
                    ActivityC13790kL.A0j(ActivityC13790kL.this, (Integer) obj);
                }
            });
        }
    }

    public void A2X() {
    }

    public void A2Y(AbstractC42541vN r3) {
        synchronized (this.A0H) {
            this.A0H.add(r3);
        }
    }

    public void A2Z(AbstractC42541vN r3) {
        synchronized (this.A0H) {
            this.A0H.remove(r3);
        }
    }

    public void A2a(List list) {
        C14900mE r2;
        int i;
        if (list.size() != 1) {
            boolean contains = list.contains(AnonymousClass1VX.A00);
            r2 = ((ActivityC13810kN) this).A05;
            i = R.string.sending_messages;
            if (contains) {
                i = R.string.sending_messages_and_status;
            }
        } else if (!C15380n4.A0N((Jid) list.get(0))) {
            r2 = ((ActivityC13810kN) this).A05;
            i = R.string.sending_message;
        } else {
            return;
        }
        r2.A07(i, 1);
    }

    public void A2b(boolean z) {
        this.A0E = z;
    }

    public boolean A2d() {
        return this.A03.A03();
    }

    @Override // X.AbstractC13880kU
    public /* synthetic */ AnonymousClass00E AGM() {
        return AnonymousClass01V.A03;
    }

    @Override // X.ActivityC000900k, android.app.Activity
    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.dump(str, fileDescriptor, printWriter, strArr);
    }

    @Override // X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        synchronized (this.A0H) {
            for (AbstractC42541vN r0 : this.A0H) {
                if (r0 != null) {
                    r0.ALt(intent, i, i2);
                }
            }
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        if (!A0l()) {
            A0Z();
        }
        super.onCreate(bundle);
        this.A0F = new AnonymousClass2GP(Looper.getMainLooper(), this.A0A, this.A0C);
        C29661Ud r2 = this.A0O;
        if (C29661Ud.A02) {
            r2.A00 = (DialogFragment) r2.A01.A0V().A0A(C29661Ud.A03);
        }
        this.A03.A00(this);
    }

    @Override // android.app.Activity, android.view.View.OnCreateContextMenuListener
    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        super.onCreateContextMenu(contextMenu, view, contextMenuInfo);
    }

    @Override // android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        this.A0H.clear();
        super.onDestroy();
    }

    @Override // X.ActivityC000800j, android.app.Activity, android.view.KeyEvent.Callback
    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 82 || !this.A0G) {
            return super.onKeyDown(i, keyEvent);
        }
        return true;
    }

    @Override // android.app.Activity, android.view.KeyEvent.Callback
    public boolean onKeyLongPress(int i, KeyEvent keyEvent) {
        if (i == 4) {
            this.A0G = true;
        }
        return super.onKeyLongPress(i, keyEvent);
    }

    @Override // android.app.Activity, android.view.KeyEvent.Callback
    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (i == 4) {
            this.A0G = false;
        }
        return super.onKeyUp(i, keyEvent);
    }

    @Override // X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        if (this.A0F.hasMessages(0)) {
            this.A0F.removeMessages(0);
        }
        this.A0A.A00();
    }

    @Override // X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        if (this.A0E) {
            this.A0F.sendEmptyMessageDelayed(0, 3000);
        }
        if (!A2d()) {
            return;
        }
        if (this.A03.A05()) {
            Intent className = new Intent().setClassName(getPackageName(), "com.whatsapp.authentication.AppAuthenticationActivity");
            className.setFlags(C25981Bo.A0F);
            A2E(className, 202);
            overridePendingTransition(0, 0);
            return;
        }
        this.A03.A01(false);
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        super.onStart();
        A2U();
        A2W();
        A2V();
    }
}
