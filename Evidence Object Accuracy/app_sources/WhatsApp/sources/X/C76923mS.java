package X;

/* renamed from: X.3mS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C76923mS extends C76713m5 {
    public final boolean isSurfaceValid;
    public final int surfaceIdentityHashCode;

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0010, code lost:
        if (r3.isValid() != false) goto L_0x0012;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C76923mS(android.view.Surface r3, X.C95494dp r4, java.lang.Throwable r5) {
        /*
            r2 = this;
            r2.<init>(r4, r5)
            int r0 = java.lang.System.identityHashCode(r3)
            r2.surfaceIdentityHashCode = r0
            if (r3 == 0) goto L_0x0012
            boolean r1 = r3.isValid()
            r0 = 0
            if (r1 == 0) goto L_0x0013
        L_0x0012:
            r0 = 1
        L_0x0013:
            r2.isSurfaceValid = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C76923mS.<init>(android.view.Surface, X.4dp, java.lang.Throwable):void");
    }
}
