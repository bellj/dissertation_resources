package X;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5lq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122675lq extends AbstractC118835cS {
    public final View A00;
    public final ImageView A01;
    public final TextView A02;

    public C122675lq(View view) {
        super(view);
        this.A00 = AnonymousClass028.A0D(view, R.id.bg);
        this.A01 = C12970iu.A0K(view, R.id.img);
        this.A02 = C12960it.A0I(view, R.id.text);
    }

    @Override // X.AbstractC118835cS
    public void A08(AbstractC125975s7 r4, int i) {
        View view;
        Context context;
        int i2;
        C123195ml r42 = (C123195ml) r4;
        if (r42 != null) {
            this.A02.setText(r42.A01);
            boolean z = r42.A02;
            ImageView imageView = this.A01;
            if (z) {
                imageView.setImageResource(R.drawable.ic_action_info_filled);
                imageView.setRotation(180.0f);
                C016307r.A00(AnonymousClass00T.A03(imageView.getContext(), R.color.txn_blurb_error_icon_color), imageView);
                view = this.A00;
                context = view.getContext();
                i2 = R.drawable.txn_details_rounded_error_blurb_bg;
            } else {
                imageView.setImageResource(R.drawable.ic_clock_filled);
                imageView.setRotation(0.0f);
                C016307r.A00(AnonymousClass00T.A03(imageView.getContext(), R.color.txn_blurb_icon_color), imageView);
                view = this.A00;
                context = view.getContext();
                i2 = R.drawable.txn_details_rounded_blurb_bg;
            }
            view.setBackground(AnonymousClass00T.A04(context, i2));
            view.setOnClickListener(r42.A00);
        }
    }
}
