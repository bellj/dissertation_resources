package X;

import android.view.View;
import com.google.android.material.appbar.AppBarLayout;

/* renamed from: X.4rl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C104064rl implements AnonymousClass07F {
    public final /* synthetic */ AppBarLayout A00;

    public C104064rl(AppBarLayout appBarLayout) {
        this.A00 = appBarLayout;
    }

    @Override // X.AnonymousClass07F
    public C018408o AMH(View view, C018408o r5) {
        AppBarLayout appBarLayout = this.A00;
        C018408o r1 = null;
        if (appBarLayout.getFitsSystemWindows()) {
            r1 = r5;
        }
        if (!C015407h.A01(appBarLayout.A04, r1)) {
            appBarLayout.A04 = r1;
            appBarLayout.A03 = -1;
            appBarLayout.A00 = -1;
            appBarLayout.A01 = -1;
        }
        return r5;
    }
}
