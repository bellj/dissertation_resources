package X;

/* renamed from: X.5F2  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5F2 implements AnonymousClass5WP {
    public final AnonymousClass5L9 A00;

    @Override // X.AnonymousClass5WP
    public boolean AJD() {
        return false;
    }

    public AnonymousClass5F2(AnonymousClass5L9 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5WP
    public AnonymousClass5L9 ADu() {
        return this.A00;
    }

    public String toString() {
        return super.toString();
    }
}
