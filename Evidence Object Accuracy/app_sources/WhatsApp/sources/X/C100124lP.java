package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4lP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C100124lP implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new AnonymousClass2NE(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new AnonymousClass2NE[i];
    }
}
