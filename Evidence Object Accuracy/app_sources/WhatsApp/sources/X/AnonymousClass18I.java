package X;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.18I  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass18I {
    public final List A00 = new ArrayList();
    public final Map A01 = new HashMap();

    public final C89994Me A00(C14270l8 r6, Object obj, String str, String str2) {
        C91084Qj r3;
        List list = this.A00;
        Iterator it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                r3 = new C91084Qj(r6);
                list.add(r3);
                break;
            }
            r3 = (C91084Qj) it.next();
            Object obj2 = r3.A01.get();
            if (r3.A00.A00 || obj2 == null) {
                it.remove();
            } else if (obj2 == r6) {
                break;
            }
        }
        Map map = r3.A02;
        Set set = (Set) map.get(str);
        HashSet hashSet = set;
        if (set == null) {
            HashSet hashSet2 = new HashSet(1);
            hashSet2.add(str2);
            map.put(str, hashSet2);
            hashSet = hashSet2;
        }
        hashSet.add(str2);
        return new C89994Me(r3.A00, obj);
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0028 A[Catch: all -> 0x0078, TryCatch #0 {, blocks: (B:4:0x0003, B:7:0x000c, B:10:0x0019, B:11:0x0022, B:13:0x0028, B:16:0x003e, B:18:0x0048, B:19:0x004c, B:21:0x0052, B:23:0x0068, B:24:0x006c, B:25:0x0072), top: B:30:0x0001 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void A01(java.lang.String r7, java.lang.Object r8) {
        /*
            r6 = this;
            monitor-enter(r6)
            if (r8 != 0) goto L_0x000c
            java.util.Map r1 = r6.A01     // Catch: all -> 0x0078
            boolean r0 = r1.containsKey(r7)     // Catch: all -> 0x0078
            if (r0 != 0) goto L_0x0019
            goto L_0x0076
        L_0x000c:
            java.util.Map r1 = r6.A01     // Catch: all -> 0x0078
            java.lang.Object r0 = r1.get(r7)     // Catch: all -> 0x0078
            boolean r0 = r8.equals(r0)     // Catch: all -> 0x0078
            if (r0 == 0) goto L_0x0019
            goto L_0x0076
        L_0x0019:
            r1.put(r7, r8)     // Catch: all -> 0x0078
            java.util.List r0 = r6.A00     // Catch: all -> 0x0078
            java.util.Iterator r5 = r0.iterator()     // Catch: all -> 0x0078
        L_0x0022:
            boolean r0 = r5.hasNext()     // Catch: all -> 0x0078
            if (r0 == 0) goto L_0x0076
            java.lang.Object r1 = r5.next()     // Catch: all -> 0x0078
            X.4Qj r1 = (X.C91084Qj) r1     // Catch: all -> 0x0078
            java.lang.ref.WeakReference r0 = r1.A01     // Catch: all -> 0x0078
            java.lang.Object r4 = r0.get()     // Catch: all -> 0x0078
            X.0l8 r4 = (X.C14270l8) r4     // Catch: all -> 0x0078
            X.3l2 r0 = r1.A00     // Catch: all -> 0x0078
            boolean r0 = r0.A00     // Catch: all -> 0x0078
            if (r0 != 0) goto L_0x0072
            if (r4 == 0) goto L_0x0072
            java.util.Map r0 = r1.A02     // Catch: all -> 0x0078
            java.lang.Object r0 = r0.get(r7)     // Catch: all -> 0x0078
            java.util.Set r0 = (java.util.Set) r0     // Catch: all -> 0x0078
            if (r0 == 0) goto L_0x0022
            java.util.Iterator r3 = r0.iterator()     // Catch: all -> 0x0078
        L_0x004c:
            boolean r0 = r3.hasNext()     // Catch: all -> 0x0078
            if (r0 == 0) goto L_0x0022
            java.lang.Object r1 = r3.next()     // Catch: all -> 0x0078
            java.lang.String r1 = (java.lang.String) r1     // Catch: all -> 0x0078
            r0 = 0
            com.facebook.redex.RunnableBRunnable0Shape0S1200000_I0 r2 = new com.facebook.redex.RunnableBRunnable0Shape0S1200000_I0     // Catch: all -> 0x0078
            r2.<init>(r4, r8, r1, r0)     // Catch: all -> 0x0078
            android.os.Looper r1 = android.os.Looper.getMainLooper()     // Catch: all -> 0x0078
            android.os.Looper r0 = android.os.Looper.myLooper()     // Catch: all -> 0x0078
            if (r1 != r0) goto L_0x006c
            r2.run()     // Catch: all -> 0x0078
            goto L_0x004c
        L_0x006c:
            android.os.Handler r0 = X.C14270l8.A0L     // Catch: all -> 0x0078
            r0.post(r2)     // Catch: all -> 0x0078
            goto L_0x004c
        L_0x0072:
            r5.remove()     // Catch: all -> 0x0078
            goto L_0x0022
        L_0x0076:
            monitor-exit(r6)
            return
        L_0x0078:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass18I.A01(java.lang.String, java.lang.Object):void");
    }
}
