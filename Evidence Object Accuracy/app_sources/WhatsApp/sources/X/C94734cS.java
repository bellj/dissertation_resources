package X;

/* renamed from: X.4cS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C94734cS {
    public final C92574Wl A00;

    public C94734cS(String str, AnonymousClass5T6[] r3) {
        this.A00 = C94314bb.A00(str, r3);
    }

    public static C93934az A00(String str) {
        C89104Ir r2 = new C89104Ir();
        if (str == null || str.length() == 0) {
            throw C12970iu.A0f("json string can not be null or empty");
        }
        C92364Vp r22 = r2.A00;
        return new C93934az(r22, r22.A00.AYu(str));
    }
}
