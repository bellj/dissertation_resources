package X;

import java.util.Map;

/* renamed from: X.44P  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass44P extends AnonymousClass12X {
    @Override // X.AnonymousClass12X
    public Object A00(Map map, Object obj) {
        int i = 2498014;
        if (!map.containsKey(2498014)) {
            i = 2498018;
            if (!map.containsKey(2498018)) {
                i = 2498019;
                if (!map.containsKey(2498019)) {
                    return null;
                }
            }
        }
        return ((AnonymousClass3H5) map.get(i)).A01;
    }
}
