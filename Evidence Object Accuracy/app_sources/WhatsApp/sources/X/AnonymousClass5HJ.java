package X;

import java.util.Random;

/* renamed from: X.5HJ  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5HJ extends ThreadLocal {
    @Override // java.lang.ThreadLocal
    public Object initialValue() {
        return new Random();
    }
}
