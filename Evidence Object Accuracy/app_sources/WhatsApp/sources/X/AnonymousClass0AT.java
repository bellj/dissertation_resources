package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/* renamed from: X.0AT  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0AT extends Handler {
    public final Handler.Callback A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass0AT(Looper looper, Handler.Callback callback) {
        super(looper, new AnonymousClass0VF(callback));
        C16700pc.A0E(looper, 1);
        this.A00 = callback;
    }

    public final void A00(Message message) {
        C16700pc.A0E(message, 0);
        if (C16700pc.A0O(Looper.myLooper(), getLooper())) {
            this.A00.handleMessage(message);
        } else {
            sendMessageAtFrontOfQueue(message);
        }
    }
}
