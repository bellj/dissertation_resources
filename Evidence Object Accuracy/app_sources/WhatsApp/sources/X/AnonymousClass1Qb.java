package X;

import java.io.Closeable;
import java.io.InputStream;

/* renamed from: X.1Qb  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1Qb implements Closeable {
    public final AnonymousClass1QX A00;
    public final InputStream A01;

    public AnonymousClass1Qb(AnonymousClass1QX r1, InputStream inputStream) {
        this.A00 = r1;
        this.A01 = inputStream;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.A01.close();
    }
}
