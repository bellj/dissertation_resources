package X;

import android.transition.Transition;
import com.whatsapp.profile.ProfileInfoActivity;

/* renamed from: X.3xv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83803xv extends AbstractC100714mM {
    public final /* synthetic */ ProfileInfoActivity A00;

    public C83803xv(ProfileInfoActivity profileInfoActivity) {
        this.A00 = profileInfoActivity;
    }

    @Override // X.AbstractC100714mM, android.transition.Transition.TransitionListener
    public void onTransitionEnd(Transition transition) {
        this.A00.A01.animate().scaleX(1.0f).scaleY(1.0f).setDuration(125);
    }
}
