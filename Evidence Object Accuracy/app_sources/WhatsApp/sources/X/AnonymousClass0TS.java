package X;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0TS  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0TS {
    public static final Object A00 = new Object();
    public static volatile ArrayList A01;

    public static String A00(XmlResourceParser xmlResourceParser, String str) {
        String attributeValue = xmlResourceParser.getAttributeValue("http://schemas.android.com/apk/res/android", str);
        return attributeValue == null ? xmlResourceParser.getAttributeValue(null, str) : attributeValue;
    }

    public static ArrayList A01(Context context) {
        if (A01 == null) {
            synchronized (A00) {
                if (A01 == null) {
                    ArrayList arrayList = new ArrayList();
                    Intent intent = new Intent("android.intent.action.MAIN");
                    intent.addCategory("android.intent.category.LAUNCHER");
                    intent.setPackage(context.getPackageName());
                    List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, 128);
                    if (queryIntentActivities != null) {
                        for (ResolveInfo resolveInfo : queryIntentActivities) {
                            ActivityInfo activityInfo = resolveInfo.activityInfo;
                            Bundle bundle = activityInfo.metaData;
                            if (bundle != null && bundle.containsKey("android.app.shortcuts")) {
                                ArrayList arrayList2 = new ArrayList();
                                XmlResourceParser loadXmlMetaData = activityInfo.loadXmlMetaData(context.getPackageManager(), "android.app.shortcuts");
                                if (loadXmlMetaData == null) {
                                    StringBuilder sb = new StringBuilder("Failed to open android.app.shortcuts meta-data resource of ");
                                    sb.append(activityInfo.name);
                                    throw new IllegalArgumentException(sb.toString());
                                }
                                while (true) {
                                    try {
                                        int next = loadXmlMetaData.next();
                                        if (next == 1) {
                                            break;
                                        } else if (next == 2 && loadXmlMetaData.getName().equals("share-target")) {
                                            String A002 = A00(loadXmlMetaData, "targetClass");
                                            ArrayList arrayList3 = new ArrayList();
                                            ArrayList arrayList4 = new ArrayList();
                                            while (true) {
                                                int next2 = loadXmlMetaData.next();
                                                if (next2 == 1) {
                                                    break;
                                                } else if (next2 != 2) {
                                                    if (next2 == 3 && loadXmlMetaData.getName().equals("share-target")) {
                                                        break;
                                                    }
                                                } else {
                                                    String name = loadXmlMetaData.getName();
                                                    if (name.equals("data")) {
                                                        A00(loadXmlMetaData, "scheme");
                                                        A00(loadXmlMetaData, "host");
                                                        A00(loadXmlMetaData, "port");
                                                        A00(loadXmlMetaData, "path");
                                                        A00(loadXmlMetaData, "pathPattern");
                                                        A00(loadXmlMetaData, "pathPrefix");
                                                        arrayList3.add(new AnonymousClass0MY(A00(loadXmlMetaData, "mimeType")));
                                                    } else if (name.equals("category")) {
                                                        arrayList4.add(A00(loadXmlMetaData, "name"));
                                                    }
                                                }
                                            }
                                            AnonymousClass0NK r0 = (arrayList3.isEmpty() || A002 == null || arrayList4.isEmpty()) ? null : new AnonymousClass0NK(A002, (AnonymousClass0MY[]) arrayList3.toArray(new AnonymousClass0MY[arrayList3.size()]), (String[]) arrayList4.toArray(new String[arrayList4.size()]));
                                            if (r0 != null) {
                                                arrayList2.add(r0);
                                            }
                                        }
                                    } catch (Exception e) {
                                        Log.e("ShareTargetXmlParser", "Failed to parse the Xml resource: ", e);
                                    }
                                }
                                loadXmlMetaData.close();
                                arrayList.addAll(arrayList2);
                            }
                        }
                    }
                    A01 = arrayList;
                }
            }
        }
        return A01;
    }
}
