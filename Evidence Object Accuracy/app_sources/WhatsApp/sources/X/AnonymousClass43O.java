package X;

/* renamed from: X.43O  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass43O extends AbstractC16110oT {
    public Integer A00;
    public String A01;

    public AnonymousClass43O() {
        super(2490, new AnonymousClass00E(1, 1, 5), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(2, this.A01);
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamQplHealth {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "qplHealthEventData", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "qplHealthEventType", C12960it.A0Y(this.A00));
        return C12960it.A0d("}", A0k);
    }
}
