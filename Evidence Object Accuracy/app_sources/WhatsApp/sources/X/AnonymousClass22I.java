package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/* renamed from: X.22I  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass22I extends Handler {
    public final /* synthetic */ C21300xC A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass22I(Looper looper, C21300xC r2) {
        super(looper);
        this.A00 = r2;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        this.A00.A00();
    }
}
