package X;

import android.view.accessibility.AccessibilityManager;

/* renamed from: X.0Qw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05770Qw {
    public static void A00(AccessibilityManager accessibilityManager, AbstractC11760go r2) {
        accessibilityManager.addTouchExplorationStateChangeListener(new accessibility.AccessibilityManager$TouchExplorationStateChangeListenerC06990Wg(r2));
    }

    public static void A01(AccessibilityManager accessibilityManager, AbstractC11760go r2) {
        accessibilityManager.removeTouchExplorationStateChangeListener(new accessibility.AccessibilityManager$TouchExplorationStateChangeListenerC06990Wg(r2));
    }
}
