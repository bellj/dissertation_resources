package X;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.RunnableBRunnable0Shape0S0101000_I0;
import com.whatsapp.R;
import com.whatsapp.calling.controls.viewmodel.ParticipantsListViewModel;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.VoipCallControlBottomSheetV2;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1kg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C36811kg extends AnonymousClass02L {
    public static final C74613iO A0D = new C74613iO();
    public RecyclerView A00;
    public AbstractC15710nm A01;
    public ParticipantsListViewModel A02;
    public C15550nR A03;
    public C15610nY A04;
    public AnonymousClass01d A05;
    public C21250x7 A06;
    public C20710wC A07;
    public UserJid A08;
    public AnonymousClass12F A09;
    public AnonymousClass4LR A0A;
    public final AnonymousClass28F A0B;
    public final AnonymousClass1J1 A0C;

    public C36811kg(Context context, AnonymousClass130 r4, C21270x9 r5, C14820m6 r6, C14850m9 r7) {
        super(A0D);
        this.A0B = new C1102954z(r4, AnonymousClass1SF.A0M(r6, r7));
        this.A0C = r5.A04(context, "voip-call-control-bottom-sheet");
        A07(true);
    }

    @Override // X.AnonymousClass02M
    public long A00(int i) {
        int i2;
        C92194Ux r1 = (C92194Ux) super.A0E(i);
        if (r1 instanceof C849140h) {
            i2 = ((C849140h) r1).A02.hashCode();
        } else {
            i2 = r1.A00;
        }
        return (long) i2;
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void A0A(AnonymousClass03U r1) {
        ((AbstractC75743kL) r1).A08();
    }

    @Override // X.AnonymousClass02M
    public void A0B(RecyclerView recyclerView) {
        this.A00 = recyclerView;
    }

    @Override // X.AnonymousClass02M
    public void A0C(RecyclerView recyclerView) {
        Log.i("voip/ParticipantsListAdapter/onDetachedFromRecyclerView");
        this.A0C.A00();
    }

    @Override // X.AnonymousClass02L
    public /* bridge */ /* synthetic */ Object A0E(int i) {
        return super.A0E(i);
    }

    @Override // X.AnonymousClass02L
    public void A0F(List list) {
        super.A0F(list == null ? null : new ArrayList(list));
    }

    public void A0G() {
        if (this.A00 != null) {
            for (int i = 0; i < A0D(); i++) {
                C92194Ux r2 = (C92194Ux) super.A0E(i);
                if (r2.A00 == 4) {
                    AnonymousClass03U A0C = this.A00.A0C(i);
                    if (A0C instanceof AbstractC75743kL) {
                        ((AbstractC75743kL) A0C).A09(r2);
                        return;
                    }
                    return;
                }
            }
        }
    }

    public final void A0H(int i) {
        AnonymousClass4LR r0 = this.A0A;
        if (r0 != null) {
            VoipCallControlBottomSheetV2 voipCallControlBottomSheetV2 = r0.A00;
            if (voipCallControlBottomSheetV2.A0C != null && voipCallControlBottomSheetV2.A0B != null) {
                StringBuilder sb = new StringBuilder("voip/VoipCallControlBottomSheetV2 scroll to position: ");
                sb.append(i);
                Log.i(sb.toString());
                voipCallControlBottomSheetV2.A0G.A08 = null;
                voipCallControlBottomSheetV2.A0C.post(new RunnableBRunnable0Shape0S0101000_I0(voipCallControlBottomSheetV2, i, 29));
            }
        }
    }

    public void A0I(UserJid userJid) {
        this.A08 = userJid;
        if (userJid != null) {
            for (int i = 0; i < A0D(); i++) {
                C92194Ux r1 = (C92194Ux) super.A0E(i);
                if ((r1 instanceof C849140h) && ((C849140h) r1).A02.equals(this.A08)) {
                    A0H(i);
                }
            }
        }
    }

    public void A0J(UserJid userJid) {
        C60212wE r5;
        C849140h r0;
        StringBuilder sb = new StringBuilder("voip/ParticipantsListAdapter/updateProfilePhoto ");
        sb.append(userJid);
        Log.i(sb.toString());
        for (int i = 0; i < A0D(); i++) {
            C92194Ux r1 = (C92194Ux) super.A0E(i);
            if ((r1 instanceof C849140h) && this.A00 != null && ((C849140h) r1).A02.equals(userJid)) {
                AnonymousClass03U A0C = this.A00.A0C(i);
                if ((A0C instanceof C60212wE) && (r0 = (r5 = (C60212wE) A0C).A00) != null) {
                    r5.A08.A02(r5.A02, r5.A07, r0.A01, true);
                }
            }
        }
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r3, int i) {
        C92194Ux r1 = (C92194Ux) super.A0E(i);
        AnonymousClass009.A05(r1);
        ((AbstractC75743kL) r3).A09(r1);
        if ((r1 instanceof C849140h) && ((C849140h) r1).A02.equals(this.A08)) {
            A0H(i);
        }
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
        if (i == 0) {
            return new C60182wB(from.inflate(R.layout.voip_call_control_sheet_add_participant_button_row, viewGroup, false), this.A02);
        }
        if (i == 2 || i == 3) {
            return new C60192wC(from.inflate(R.layout.voip_call_control_sheet_inline_rounded_button_row, viewGroup, false), this.A02);
        }
        if (i != 4) {
            boolean z = true;
            if (i != 1) {
                z = false;
            }
            AnonymousClass009.A0A("Unknown list item type", z);
            View inflate = from.inflate(R.layout.voip_call_control_sheet_participant_row, viewGroup, false);
            ParticipantsListViewModel participantsListViewModel = this.A02;
            C15610nY r5 = this.A04;
            AnonymousClass12F r9 = this.A09;
            return new C60212wE(inflate, participantsListViewModel, r5, this.A0B, this.A0C, this.A05, r9);
        }
        return new C60202wD(from.inflate(R.layout.voip_call_control_sheet_linked_group_call_header_row, viewGroup, false), this.A02, this.A03, this.A04, this.A06, this.A07);
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        C92194Ux r0 = (C92194Ux) super.A0E(i);
        AnonymousClass009.A05(r0);
        return r0.A00;
    }
}
