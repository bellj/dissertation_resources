package X;

import android.content.SharedPreferences;
import android.util.Base64;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

/* renamed from: X.1W5  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1W5 implements AnonymousClass1W6 {
    public final /* synthetic */ C246816l A00;

    public AnonymousClass1W5(C246816l r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass1W6
    public void AQu(UserJid userJid) {
        C246816l r2 = this.A00;
        r2.A08.A0Z(userJid.getRawString());
        r2.A04(userJid);
        r2.A04.AaV("direct-connection-public-key-error-response", "", false);
    }

    @Override // X.AnonymousClass1W6
    public void AQv(UserJid userJid, String str) {
        String str2;
        Log.i("DirectConnectionManager/onGetBusinessPublicKeySuccess");
        try {
            X509Certificate[] x509CertificateArr = (X509Certificate[]) CertificateFactory.getInstance("X.509").generateCertificates(new ByteArrayInputStream(str.getBytes(AnonymousClass01V.A08))).toArray(new X509Certificate[0]);
            TrustManagerFactory instance = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            instance.init((KeyStore) null);
            ((X509TrustManager) instance.getTrustManagers()[0]).checkServerTrusted(x509CertificateArr, "RSA");
            String encodeToString = Base64.encodeToString(x509CertificateArr[0].getEncoded(), 2);
            C246816l r4 = this.A00;
            C14820m6 r0 = r4.A08;
            String rawString = userJid.getRawString();
            SharedPreferences.Editor edit = r0.A00.edit();
            StringBuilder sb = new StringBuilder("smb_business_direct_connection_public_key_");
            sb.append(rawString);
            edit.putString(sb.toString(), encodeToString).apply();
            r4.A03(userJid);
        } catch (UnsupportedEncodingException | IllegalArgumentException | GeneralSecurityException e) {
            Log.e("DirectConnectionManager/generateEncryptionStringFromSignedInfo/", e);
            C246816l r1 = this.A00;
            r1.A04(userJid);
            boolean z = e instanceof NoSuchAlgorithmException;
            AbstractC15710nm r2 = r1.A04;
            String obj = e.toString();
            if (z) {
                str2 = "direct-connection-certificate-exception-no-such-algorithm";
            } else {
                str2 = "direct-connection-certificate-exception";
            }
            r2.AaV(str2, obj, true);
        }
    }
}
