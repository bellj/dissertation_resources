package X;

import com.whatsapp.R;
import com.whatsapp.status.StatusesFragment;

/* renamed from: X.55q  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1104655q implements AbstractC32851cq {
    public final /* synthetic */ StatusesFragment A00;

    public C1104655q(StatusesFragment statusesFragment) {
        this.A00 = statusesFragment;
    }

    @Override // X.AbstractC32851cq
    public void AUa(String str) {
        StatusesFragment statusesFragment = this.A00;
        int A04 = C72453ed.A04();
        int A03 = C72453ed.A03();
        ((AbstractC13860kS) C13010iy.A01(statusesFragment)).Adr(new Object[0], A04, A03);
    }

    @Override // X.AbstractC32851cq
    public void AUb() {
        ((AbstractC13860kS) C13010iy.A01(this.A00)).Adr(new Object[0], R.string.alert, R.string.permission_storage_need_access);
    }

    @Override // X.AbstractC32851cq
    public void AXw(String str) {
        StatusesFragment statusesFragment = this.A00;
        int A04 = C72453ed.A04();
        int A03 = C72453ed.A03();
        ((AbstractC13860kS) C13010iy.A01(statusesFragment)).Adr(new Object[0], A04, A03);
    }

    @Override // X.AbstractC32851cq
    public void AXx() {
        ((AbstractC13860kS) C13010iy.A01(this.A00)).Adr(new Object[0], R.string.alert, R.string.permission_storage_need_access);
    }
}
