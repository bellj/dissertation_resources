package X;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import com.whatsapp.R;

/* renamed from: X.35n  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass35n extends C622335s {
    public View A00;
    public View A01;
    public boolean A02;

    public AnonymousClass35n(Context context, LayoutInflater layoutInflater, C14850m9 r3, AnonymousClass1AB r4, AnonymousClass1KZ r5, C235512c r6, AbstractC116245Ur r7, int i) {
        super(context, layoutInflater, r3, r4, r5, r6, r7, i);
    }

    @Override // X.C622335s, X.AbstractC69213Yj
    public void A03(View view) {
        super.A03(view);
        this.A00 = view.findViewById(R.id.empty);
        this.A01 = view.findViewById(R.id.more_info_button);
        C12970iu.A1G(((C622335s) this).A01);
    }
}
