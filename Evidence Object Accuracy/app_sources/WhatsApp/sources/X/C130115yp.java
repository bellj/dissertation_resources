package X;

import android.content.res.Resources;

/* renamed from: X.5yp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130115yp {
    public final Resources A00;
    public final AnonymousClass018 A01;
    public final C20830wO A02;
    public final C14850m9 A03;
    public final C17070qD A04;
    public final AbstractC136536Mx A05;
    public final AnonymousClass14X A06;

    public C130115yp(Resources resources, AnonymousClass018 r2, C20830wO r3, C14850m9 r4, C17070qD r5, AbstractC136536Mx r6, AnonymousClass14X r7) {
        this.A00 = resources;
        this.A03 = r4;
        this.A06 = r7;
        this.A01 = r2;
        this.A04 = r5;
        this.A02 = r3;
        this.A05 = r6;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x006c, code lost:
        if (X.AnonymousClass1ZD.A00(r3.A01) == 4) goto L_0x006e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01fe, code lost:
        if (r5.contains(r8) == false) goto L_0x0200;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0051, code lost:
        if (android.text.TextUtils.isEmpty(r4.A01) == false) goto L_0x0053;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C128335vw A00(android.content.Context r50, X.EnumC124545pi r51, X.AbstractC16390ow r52) {
        /*
        // Method dump skipped, instructions count: 695
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C130115yp.A00(android.content.Context, X.5pi, X.0ow):X.5vw");
    }

    public boolean A01(AnonymousClass1IR r4) {
        if (r4 == null) {
            return false;
        }
        C17070qD r1 = this.A04;
        return this.A06.A0b(r4, r1.A02().ABq(), r1.A02().AEE());
    }
}
