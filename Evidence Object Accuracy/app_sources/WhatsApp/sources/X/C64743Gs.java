package X;

import android.webkit.JavascriptInterface;

/* renamed from: X.3Gs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64743Gs {
    public final /* synthetic */ AnonymousClass39H A00;

    public /* synthetic */ C64743Gs(AnonymousClass39H r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00ae, code lost:
        if (r0 != 1) goto L_0x00b0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00d4, code lost:
        if (r7 != 3) goto L_0x00e7;
     */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0080  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ void A00(X.C64743Gs r5, int r6, int r7) {
        /*
        // Method dump skipped, instructions count: 240
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C64743Gs.A00(X.3Gs, int, int):void");
    }

    @JavascriptInterface
    public void postPlayerEvent(int i, int i2) {
        if (i2 >= 0 && i2 < 2147483) {
            this.A00.A0C.A0H(new Runnable(i, i2) { // from class: X.3l9
                public final /* synthetic */ int A00;
                public final /* synthetic */ int A01;

                {
                    this.A00 = r2;
                    this.A01 = r3;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    C64743Gs.A00(C64743Gs.this, this.A00, this.A01);
                }
            });
        }
    }
}
