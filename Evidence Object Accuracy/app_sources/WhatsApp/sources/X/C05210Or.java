package X;

/* renamed from: X.0Or  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05210Or {
    public int A00;
    public int A01;
    public int A02 = 0;
    public int A03;
    public int A04;
    public int A05 = 0;
    public boolean A06;
    public boolean A07 = true;
    public boolean A08;

    public String toString() {
        StringBuilder sb = new StringBuilder("LayoutState{mAvailable=");
        sb.append(this.A00);
        sb.append(", mCurrentPosition=");
        sb.append(this.A01);
        sb.append(", mItemDirection=");
        sb.append(this.A03);
        sb.append(", mLayoutDirection=");
        sb.append(this.A04);
        sb.append(", mStartLine=");
        sb.append(this.A05);
        sb.append(", mEndLine=");
        sb.append(this.A02);
        sb.append('}');
        return sb.toString();
    }
}
