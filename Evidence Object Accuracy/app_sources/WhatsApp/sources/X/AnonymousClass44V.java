package X;

import com.whatsapp.chatinfo.ContactInfoActivity;
import java.util.Set;

/* renamed from: X.44V  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass44V extends AbstractC33331dp {
    public final /* synthetic */ ContactInfoActivity A00;

    public AnonymousClass44V(ContactInfoActivity contactInfoActivity) {
        this.A00 = contactInfoActivity;
    }

    @Override // X.AbstractC33331dp
    public void A00(Set set) {
        ContactInfoActivity contactInfoActivity = this.A00;
        contactInfoActivity.A31();
        contactInfoActivity.A0b();
    }
}
