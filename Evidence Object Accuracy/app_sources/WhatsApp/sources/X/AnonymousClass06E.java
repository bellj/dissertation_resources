package X;

/* renamed from: X.06E  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass06E extends AnonymousClass06F {
    public final boolean A00;

    public AnonymousClass06E(AnonymousClass06H r1, boolean z) {
        super(r1);
        this.A00 = z;
    }

    @Override // X.AnonymousClass06F
    public boolean A00() {
        return this.A00;
    }
}
