package X;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.17f  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C248717f {
    public final Set A00;
    public final Set A01;

    public C248717f(Set set, Set set2) {
        this.A00 = Collections.unmodifiableSet(new HashSet(set));
        this.A01 = Collections.unmodifiableSet(new HashSet(set2));
    }
}
