package X;

import android.content.Intent;

/* renamed from: X.29y  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C472829y extends Exception {
    public final Intent zza;

    public C472829y(Intent intent, String str) {
        super(str);
        this.zza = intent;
    }
}
