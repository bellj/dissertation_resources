package X;

import android.view.CollapsibleActionView;
import android.view.View;
import android.widget.FrameLayout;

/* renamed from: X.0Bd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02310Bd extends FrameLayout implements AnonymousClass07D {
    public final CollapsibleActionView A00;

    public C02310Bd(View view) {
        super(view.getContext());
        this.A00 = (CollapsibleActionView) view;
        addView(view);
    }

    public View getWrappedView() {
        return (View) this.A00;
    }

    @Override // X.AnonymousClass07D
    public void onActionViewCollapsed() {
        this.A00.onActionViewCollapsed();
    }

    @Override // X.AnonymousClass07D
    public void onActionViewExpanded() {
        this.A00.onActionViewExpanded();
    }
}
