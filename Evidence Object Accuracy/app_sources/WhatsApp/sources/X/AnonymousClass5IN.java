package X;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/* renamed from: X.5IN  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5IN<K, V> extends LinkedHashMap<K, V> {
    public static final AnonymousClass5IN A00;
    public boolean zza = true;

    static {
        AnonymousClass5IN r1 = new AnonymousClass5IN();
        A00 = r1;
        r1.zza = false;
    }

    public AnonymousClass5IN() {
    }

    public AnonymousClass5IN(Map map) {
        super(map);
    }

    @Override // java.util.LinkedHashMap, java.util.AbstractMap, java.util.Map, java.util.HashMap
    public final void clear() {
        if (this.zza) {
            super.clear();
            return;
        }
        throw C12970iu.A0z();
    }

    @Override // java.util.LinkedHashMap, java.util.AbstractMap, java.util.Map, java.util.HashMap
    public final Set entrySet() {
        return isEmpty() ? Collections.emptySet() : super.entrySet();
    }

    @Override // java.util.AbstractMap, java.util.Map, java.lang.Object
    public final boolean equals(Object obj) {
        boolean equals;
        if (obj instanceof Map) {
            Map map = (Map) obj;
            if (this != map) {
                if (size() == map.size()) {
                    Iterator A0s = C12990iw.A0s(this);
                    while (A0s.hasNext()) {
                        Map.Entry A15 = C12970iu.A15(A0s);
                        if (map.containsKey(A15.getKey())) {
                            Object value = A15.getValue();
                            Object obj2 = map.get(A15.getKey());
                            if (!(value instanceof byte[]) || !(obj2 instanceof byte[])) {
                                equals = value.equals(obj2);
                                continue;
                            } else {
                                equals = Arrays.equals((byte[]) value, (byte[]) obj2);
                                continue;
                            }
                            if (!equals) {
                            }
                        }
                    }
                }
            }
            return true;
        }
        return false;
    }

    @Override // java.util.AbstractMap, java.util.Map, java.lang.Object
    public final int hashCode() {
        int i;
        int i2;
        Iterator A0s = C12990iw.A0s(this);
        int i3 = 0;
        while (A0s.hasNext()) {
            Map.Entry A15 = C12970iu.A15(A0s);
            Object key = A15.getKey();
            if (key instanceof byte[]) {
                byte[] bArr = (byte[]) key;
                int length = bArr.length;
                i = length;
                for (int i4 = 0; i4 < 0 + length; i4++) {
                    i = (i * 31) + bArr[i4];
                }
                if (i == 0) {
                    i = 1;
                }
            } else {
                i = key.hashCode();
            }
            Object value = A15.getValue();
            if (value instanceof byte[]) {
                byte[] bArr2 = (byte[]) value;
                int length2 = bArr2.length;
                i2 = length2;
                for (int i5 = 0; i5 < 0 + length2; i5++) {
                    i2 = (i2 * 31) + bArr2[i5];
                }
                if (i2 == 0) {
                    i2 = 1;
                }
            } else {
                i2 = value.hashCode();
            }
            i3 += i2 ^ i;
        }
        return i3;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.AbstractMap, java.util.Map, java.util.HashMap
    public final Object put(Object obj, Object obj2) {
        if (this.zza) {
            return super.put(obj, obj2);
        }
        throw C12970iu.A0z();
    }

    @Override // java.util.AbstractMap, java.util.Map, java.util.HashMap
    public final void putAll(Map map) {
        if (this.zza) {
            Iterator A10 = C72453ed.A10(map);
            while (A10.hasNext()) {
                map.get(A10.next());
            }
            super.putAll(map);
            return;
        }
        throw C12970iu.A0z();
    }

    @Override // java.util.AbstractMap, java.util.Map, java.util.HashMap
    public final Object remove(Object obj) {
        if (this.zza) {
            return super.remove(obj);
        }
        throw C12970iu.A0z();
    }
}
