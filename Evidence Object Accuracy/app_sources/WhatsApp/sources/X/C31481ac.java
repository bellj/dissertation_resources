package X;

/* renamed from: X.1ac  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C31481ac {
    public static C31491ad A00(byte[] bArr) {
        int i = bArr[0] & 255;
        if (i == 5) {
            byte[] bArr2 = new byte[32];
            System.arraycopy(bArr, 1, bArr2, 0, 32);
            return new C31491ad(bArr2);
        }
        StringBuilder sb = new StringBuilder("Bad key type: ");
        sb.append(i);
        throw new C31561ak(sb.toString());
    }

    public static C31731b1 A01() {
        C90604On A01 = C32001bS.A00().A01();
        return new C31731b1(new C31721b0(A01.A00), new C31491ad(A01.A01));
    }

    public static byte[] A02(C31721b0 r3, C31491ad r4) {
        return C32001bS.A00().A03(r4.A00, r3.A00);
    }
}
