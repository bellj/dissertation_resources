package X;

import android.util.Log;
import android.util.Pair;
import java.util.List;

/* renamed from: X.2nq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C57922nq extends AnonymousClass4WH {
    public final /* synthetic */ String A00;

    public C57922nq(String str) {
        this.A00 = str;
    }

    @Override // X.AnonymousClass4WH
    public /* bridge */ /* synthetic */ void A00(Object obj) {
        Pair A01 = C64943Hn.A01((AnonymousClass28D) obj, this.A00);
        int A05 = C12960it.A05(A01.second);
        if (A05 < 0) {
            Log.w("ComponentTreeMutator", "removeChildById: No existing child found with specified ID in parent. No child has been removed from the parent.");
        } else {
            ((List) A01.first).remove(A05);
        }
    }
}
