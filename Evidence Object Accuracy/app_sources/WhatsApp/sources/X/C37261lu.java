package X;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.calling.callgrid.viewmodel.CallGridViewModel;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1lu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37261lu extends AnonymousClass02M {
    public RecyclerView A00;
    public C89374Js A01;
    public AnonymousClass3CK A02;
    public AbstractC116665Wi A03;
    public CallGridViewModel A04;
    public boolean A05;
    public final AnonymousClass2JB A06;
    public final AnonymousClass2JC A07;
    public final AnonymousClass2JD A08;
    public final AnonymousClass2JE A09;
    public final AnonymousClass2JF A0A;
    public final C27131Gd A0B = new AnonymousClass1lZ(this);
    public final C14850m9 A0C;
    public final List A0D = new ArrayList();

    public C37261lu(AnonymousClass2JB r2, AnonymousClass2JC r3, AnonymousClass2JD r4, AnonymousClass2JE r5, AnonymousClass2JF r6, C14850m9 r7) {
        this.A0C = r7;
        this.A06 = r2;
        this.A07 = r3;
        this.A08 = r4;
        this.A09 = r5;
        this.A0A = r6;
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void A06(AnonymousClass03U r8, List list, int i) {
        AbstractC55202hx r82 = (AbstractC55202hx) r8;
        View view = r82.A0H;
        A0G(view);
        List list2 = this.A0D;
        r82.A0G((C64363Fg) list2.get(i));
        boolean z = false;
        if (list.size() > 0 && (list.get(0) instanceof Bundle) && ((Bundle) list.get(0)).getBoolean("update_contact")) {
            r82.A08();
        }
        if ((r82 instanceof C60142w0) && this.A00 != null && !(this instanceof C60102vv) && !this.A05) {
            int size = list2.size();
            int height = this.A00.getHeight();
            CallGridViewModel callGridViewModel = this.A04;
            if (callGridViewModel != null && ((Boolean) callGridViewModel.A0I.A01()).booleanValue()) {
                z = true;
            }
            int A00 = C94554c5.A00(size, height, z);
            Log.i("VoiceParticipantViewHolder/setItemViewHeight get called");
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            int i2 = layoutParams.height;
            if (i2 != A00) {
                StringBuilder sb = new StringBuilder("VoiceParticipantViewHolder/setItemViewHeight Height Mismatch, layoutParams.height: ");
                sb.append(i2);
                sb.append(", itemViewHeightPx: ");
                sb.append(A00);
                Log.i(sb.toString());
                layoutParams.height = A00;
                view.setLayoutParams(layoutParams);
            }
            int i3 = 0;
            if (size > 2) {
                i3 = 2;
                if (size <= 8) {
                    i3 = 1;
                }
            }
            r82.A0A(i3);
        }
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void A0A(AnonymousClass03U r1) {
        ((AbstractC55202hx) r1).A09();
    }

    @Override // X.AnonymousClass02M
    public void A0B(RecyclerView recyclerView) {
        this.A00 = recyclerView;
    }

    @Override // X.AnonymousClass02M
    public void A0C(RecyclerView recyclerView) {
        this.A00 = null;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A0D.size();
    }

    public View A0E(int i, ViewGroup viewGroup) {
        LayoutInflater from;
        int i2;
        View frameLayout;
        Context context = viewGroup.getContext();
        switch (i) {
            case 1:
                from = LayoutInflater.from(context);
                i2 = R.layout.video_call_participant_pip_view;
                frameLayout = from.inflate(i2, viewGroup, false);
                break;
            case 2:
            default:
                from = LayoutInflater.from(context);
                i2 = R.layout.video_call_participant_view_v2;
                frameLayout = from.inflate(i2, viewGroup, false);
                break;
            case 3:
                from = LayoutInflater.from(context);
                i2 = R.layout.audio_call_participant_view_v2;
                frameLayout = from.inflate(i2, viewGroup, false);
                break;
            case 4:
                from = LayoutInflater.from(context);
                i2 = R.layout.audio_call_participant_view_single_tile;
                frameLayout = from.inflate(i2, viewGroup, false);
                break;
            case 5:
                from = LayoutInflater.from(context);
                i2 = R.layout.audio_call_participant_view_1on1;
                frameLayout = from.inflate(i2, viewGroup, false);
                break;
            case 6:
                from = LayoutInflater.from(context);
                i2 = R.layout.audio_call_participant_large_tile;
                frameLayout = from.inflate(i2, viewGroup, false);
                break;
            case 7:
                from = LayoutInflater.from(context);
                i2 = R.layout.audio_call_participant_pip_tile;
                frameLayout = from.inflate(i2, viewGroup, false);
                break;
            case 8:
                from = LayoutInflater.from(context);
                i2 = R.layout.audio_call_participant_landscape_pip_tile;
                frameLayout = from.inflate(i2, viewGroup, false);
                break;
            case 9:
                from = LayoutInflater.from(context);
                i2 = R.layout.audio_call_participant_picture_in_picture_tile;
                frameLayout = from.inflate(i2, viewGroup, false);
                break;
            case 10:
                from = LayoutInflater.from(context);
                i2 = R.layout.video_call_participant_picture_in_picture_view;
                frameLayout = from.inflate(i2, viewGroup, false);
                break;
            case 11:
                frameLayout = new FrameLayout(context);
                frameLayout.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
                break;
        }
        A0G(frameLayout);
        return frameLayout;
    }

    public AbstractC55202hx A0F(View view, int i) {
        boolean z;
        boolean z2 = false;
        if (!this.A0C.A07(1502) || !(i == 2 || i == 1)) {
            z = false;
        } else {
            z = true;
        }
        switch (i) {
            case 3:
            case 6:
            case 7:
            case 8:
            case 9:
                AnonymousClass2JD r0 = this.A08;
                C89374Js r6 = this.A01;
                CallGridViewModel callGridViewModel = this.A04;
                AnonymousClass01J r1 = r0.A00.A03;
                AnonymousClass130 r8 = (AnonymousClass130) r1.A41.get();
                return new C60142w0(view, (C18720su) r1.A2c.get(), r6, callGridViewModel, r8, (C15610nY) r1.AMe.get());
            case 4:
                AnonymousClass2JE r02 = this.A09;
                C89374Js r62 = this.A01;
                CallGridViewModel callGridViewModel2 = this.A04;
                AnonymousClass01J r12 = r02.A00.A03;
                AnonymousClass130 r82 = (AnonymousClass130) r12.A41.get();
                return new C60132vz(view, (C18720su) r12.A2c.get(), r62, callGridViewModel2, r82, (C15610nY) r12.AMe.get());
            case 5:
                AnonymousClass2JC r03 = this.A07;
                C89374Js r63 = this.A01;
                CallGridViewModel callGridViewModel3 = this.A04;
                AnonymousClass01J r13 = r03.A00.A03;
                AnonymousClass130 r83 = (AnonymousClass130) r13.A41.get();
                return new C60122vy(view, (C18720su) r13.A2c.get(), r63, callGridViewModel3, r83, (C15610nY) r13.AMe.get(), (AnonymousClass018) r13.ANb.get());
            case 10:
            default:
                if (i == 0 || i == 1 || i == 10 || i == 2) {
                    z2 = true;
                }
                AnonymousClass009.A0A("Unknown view holder type", z2);
                AnonymousClass2JB r04 = this.A06;
                C89374Js r64 = this.A01;
                CallGridViewModel callGridViewModel4 = this.A04;
                AnonymousClass01J r14 = r04.A00.A03;
                AnonymousClass130 r9 = (AnonymousClass130) r14.A41.get();
                AnonymousClass1CP r84 = (AnonymousClass1CP) r14.AMO.get();
                return new C60112vx(view, (C18720su) r14.A2c.get(), r64, callGridViewModel4, r84, r9, (C15610nY) r14.AMe.get(), z);
            case 11:
                AnonymousClass01J r15 = this.A0A.A00.A03;
                AnonymousClass130 r2 = (AnonymousClass130) r15.A41.get();
                return new C849040g(view, (C18720su) r15.A2c.get(), r2, (C15610nY) r15.AMe.get());
        }
    }

    public final void A0G(View view) {
        int i;
        RecyclerView recyclerView = this.A00;
        if (recyclerView != null && (recyclerView.getLayoutManager() instanceof LinearLayoutManager)) {
            int size = this.A0D.size();
            int width = this.A00.getWidth();
            if (size == 3) {
                i = width / 3;
            } else {
                i = (int) (((double) width) / 3.25d);
            }
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            if (i != layoutParams.height || i != layoutParams.width) {
                layoutParams.height = i;
                layoutParams.width = i;
                view.setLayoutParams(layoutParams);
            }
        }
    }

    public void A0H(UserJid userJid) {
        if (userJid != null) {
            int i = 0;
            while (true) {
                List list = this.A0D;
                if (i >= list.size()) {
                    return;
                }
                if (!userJid.equals(((C64363Fg) list.get(i)).A0S)) {
                    i++;
                } else if (i != -1) {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("update_contact", true);
                    super.A01.A04(bundle, i, 1);
                    return;
                } else {
                    return;
                }
            }
        }
    }

    public void A0I(List list) {
        List list2 = this.A0D;
        AnonymousClass0SZ A00 = AnonymousClass0RD.A00(new C74543iH(list2, list));
        boolean z = false;
        if (list2.size() != list.size()) {
            z = true;
        }
        this.A05 = z;
        list2.clear();
        list2.addAll(list);
        A00.A02(this);
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0026  */
    /* JADX WARNING: Removed duplicated region for block: B:15:? A[RETURN, SYNTHETIC] */
    @Override // X.AnonymousClass02M
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ void ANH(X.AnonymousClass03U r3, int r4) {
        /*
            r2 = this;
            X.2hx r3 = (X.AbstractC55202hx) r3
            boolean r0 = r2 instanceof X.C60102vv
            if (r0 != 0) goto L_0x000f
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            super.A06(r3, r0, r4)
        L_0x000e:
            return
        L_0x000f:
            if (r4 < 0) goto L_0x002b
            java.util.List r1 = r2.A0D
            int r0 = r1.size()
            if (r4 >= r0) goto L_0x002b
            java.lang.Object r0 = r1.get(r4)
            X.3Fg r0 = (X.C64363Fg) r0
        L_0x001f:
            r3.A0G(r0)
            boolean r0 = r3 instanceof X.C60142w0
            if (r0 == 0) goto L_0x000e
            r0 = 3
            r3.A0A(r0)
            return
        L_0x002b:
            r0 = 0
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C37261lu.ANH(X.03U, int):void");
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        StringBuilder sb = new StringBuilder("CallGridAdapter/onCreateViewHolder, viewType: ");
        sb.append(i);
        Log.i(sb.toString());
        AbstractC55202hx A0F = A0F(A0E(i, viewGroup), i);
        if (A0F instanceof C60112vx) {
            ((C60112vx) A0F).A05 = new C1100754d(this);
        }
        A0F.A0F(this.A02);
        return A0F;
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0014  */
    @Override // X.AnonymousClass02M
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int getItemViewType(int r6) {
        /*
            r5 = this;
            if (r6 < 0) goto L_0x002b
            java.util.List r1 = r5.A0D
            int r0 = r1.size()
            if (r6 >= r0) goto L_0x002b
            java.lang.Object r0 = r1.get(r6)
            X.3Fg r0 = (X.C64363Fg) r0
        L_0x0010:
            r4 = 11
            if (r0 == 0) goto L_0x0059
            boolean r1 = r0.A0C
            r3 = 1
            if (r1 == 0) goto L_0x002d
            boolean r1 = r0.A0H
            if (r1 == 0) goto L_0x0028
            boolean r1 = r0.A09
            if (r1 != 0) goto L_0x0028
            int r2 = r0.A05
            r1 = -1
            r0 = 10
            if (r2 == r1) goto L_0x002a
        L_0x0028:
            r0 = 9
        L_0x002a:
            return r0
        L_0x002b:
            r0 = 0
            goto L_0x0010
        L_0x002d:
            boolean r1 = r0.A0A
            r2 = 3
            if (r1 != 0) goto L_0x0059
            boolean r1 = r0.A0H
            if (r1 != 0) goto L_0x0045
            java.util.List r1 = r5.A0D
            int r1 = r1.size()
            if (r1 != r3) goto L_0x0056
            boolean r0 = r0.A0B
            r4 = 5
            if (r0 == 0) goto L_0x0059
            r4 = 4
            return r4
        L_0x0045:
            boolean r0 = r0.A09
            if (r0 == 0) goto L_0x0058
            java.util.List r0 = r5.A0D
            int r0 = r0.size()
            if (r0 >= r2) goto L_0x0056
            boolean r0 = r5 instanceof X.C60102vv
            r4 = 6
            if (r0 == 0) goto L_0x0059
        L_0x0056:
            r4 = 3
            return r4
        L_0x0058:
            r4 = 0
        L_0x0059:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C37261lu.getItemViewType(int):int");
    }
}
