package X;

import android.net.Uri;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.3H3  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3H3 {
    public final int A00;
    public final int A01;
    public final long A02;
    public final long A03;
    public final Uri A04;
    public final String A05;
    public final Map A06;
    public final byte[] A07;

    @Deprecated
    public AnonymousClass3H3(Uri uri, long j, long j2) {
        this(uri, null, Collections.emptyMap(), null, 1, 0, j - j, j, j2);
    }

    public AnonymousClass3H3(Uri uri, String str, Map map, byte[] bArr, int i, int i2, long j, long j2, long j3) {
        boolean z = true;
        C95314dV.A03(C12990iw.A1W(((j + j2) > 0 ? 1 : ((j + j2) == 0 ? 0 : -1))));
        C95314dV.A03(C12990iw.A1W((j2 > 0 ? 1 : (j2 == 0 ? 0 : -1))));
        if (j3 <= 0 && j3 != -1) {
            z = false;
        }
        C95314dV.A03(z);
        this.A04 = uri;
        this.A01 = i;
        this.A07 = (bArr == null || bArr.length == 0) ? null : bArr;
        this.A06 = Collections.unmodifiableMap(new HashMap(map));
        this.A03 = j2;
        this.A02 = j3;
        this.A05 = str;
        this.A00 = i2;
    }

    public String toString() {
        String str;
        StringBuilder A0k = C12960it.A0k("DataSpec[");
        int i = this.A01;
        if (i == 1) {
            str = "GET";
        } else if (i == 2) {
            str = "POST";
        } else if (i == 3) {
            str = "HEAD";
        } else {
            throw new IllegalStateException();
        }
        A0k.append(str);
        A0k.append(" ");
        A0k.append(this.A04);
        A0k.append(", ");
        A0k.append(this.A03);
        A0k.append(", ");
        A0k.append(this.A02);
        A0k.append(", ");
        A0k.append(this.A05);
        A0k.append(", ");
        A0k.append(this.A00);
        return C12960it.A0d("]", A0k);
    }
}
