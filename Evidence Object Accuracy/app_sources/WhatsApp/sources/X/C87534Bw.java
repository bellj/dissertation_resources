package X;

/* renamed from: X.4Bw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C87534Bw extends Exception {
    public final int errorCode;

    public C87534Bw(int i, String str) {
        super(str);
        this.errorCode = i;
    }
}
