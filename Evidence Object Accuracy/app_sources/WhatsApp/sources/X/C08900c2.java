package X;

import java.util.List;

/* renamed from: X.0c2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08900c2 implements Cloneable {
    public long A00 = 0;
    public C04880Nk A01;
    public C03360Hm A02;
    public C08910c3 A03;
    public C08910c3 A04;
    public C08910c3 A05;
    public AnonymousClass0JC A06;
    public AnonymousClass0JC A07;
    public AnonymousClass0JK A08;
    public AnonymousClass0JL A09;
    public AnonymousClass0JM A0A;
    public AnonymousClass0JN A0B;
    public AnonymousClass0JO A0C;
    public AnonymousClass0JW A0D;
    public AnonymousClass0JD A0E;
    public AnonymousClass0JE A0F;
    public AbstractC08890c1 A0G;
    public AbstractC08890c1 A0H;
    public AbstractC08890c1 A0I;
    public AbstractC08890c1 A0J;
    public AbstractC08890c1 A0K;
    public Boolean A0L;
    public Boolean A0M;
    public Boolean A0N;
    public Float A0O;
    public Float A0P;
    public Float A0Q;
    public Float A0R;
    public Float A0S;
    public Float A0T;
    public Float A0U;
    public Integer A0V;
    public String A0W;
    public String A0X;
    public String A0Y;
    public String A0Z;
    public String A0a;
    public List A0b;
    public C08910c3[] A0c;

    public static C08900c2 A00() {
        C08900c2 r5 = new C08900c2();
        r5.A00 = -1;
        C03360Hm r7 = C03360Hm.A01;
        r5.A0G = r7;
        AnonymousClass0JC r6 = AnonymousClass0JC.NonZero;
        r5.A07 = r6;
        Float valueOf = Float.valueOf(1.0f);
        r5.A0O = valueOf;
        r5.A0J = null;
        r5.A0T = valueOf;
        r5.A05 = new C08910c3(1.0f);
        r5.A09 = AnonymousClass0JL.Butt;
        r5.A0A = AnonymousClass0JM.Miter;
        r5.A0S = Float.valueOf(4.0f);
        r5.A0c = null;
        r5.A04 = new C08910c3(0.0f);
        r5.A0P = valueOf;
        r5.A02 = r7;
        r5.A0b = null;
        r5.A03 = new C08910c3(AnonymousClass0JP.pt, 12.0f);
        r5.A0V = 400;
        r5.A08 = AnonymousClass0JK.Normal;
        r5.A0D = AnonymousClass0JW.None;
        r5.A0E = AnonymousClass0JD.LTR;
        r5.A0C = AnonymousClass0JO.Start;
        Boolean bool = Boolean.TRUE;
        r5.A0M = bool;
        r5.A01 = null;
        r5.A0Z = null;
        r5.A0Y = null;
        r5.A0X = null;
        r5.A0L = bool;
        r5.A0N = bool;
        r5.A0I = r7;
        r5.A0R = valueOf;
        r5.A0W = null;
        r5.A06 = r6;
        r5.A0a = null;
        r5.A0H = null;
        r5.A0Q = valueOf;
        r5.A0K = null;
        r5.A0U = valueOf;
        r5.A0F = AnonymousClass0JE.None;
        r5.A0B = AnonymousClass0JN.auto;
        return r5;
    }

    @Override // java.lang.Object
    public Object clone() {
        C08900c2 r1 = (C08900c2) super.clone();
        C08910c3[] r0 = this.A0c;
        if (r0 != null) {
            r1.A0c = (C08910c3[]) r0.clone();
        }
        return r1;
    }
}
