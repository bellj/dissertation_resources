package X;

import com.whatsapp.R;

/* renamed from: X.13B  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass13B {
    public final C28601Of A00;
    public final C28601Of A01;

    public AnonymousClass13B() {
        AnonymousClass1YB r4 = new AnonymousClass1YB();
        r4.A01("com.whatsapp.intrumentation.sample", Integer.valueOf((int) R.string.instrumentation_sample_device_name));
        r4.A01("com.facebook.assistantplayground", Integer.valueOf((int) R.string.assistant_playground_device_name));
        Integer valueOf = Integer.valueOf((int) R.string.stella_device_name);
        r4.A01("com.facebook.stella_debug", valueOf);
        r4.A01("com.facebook.stella", valueOf);
        this.A01 = r4.A00();
        AnonymousClass1YB r1 = new AnonymousClass1YB();
        Integer valueOf2 = Integer.valueOf((int) R.drawable.ic_device_stella);
        r1.A01("com.facebook.stella_debug", valueOf2);
        r1.A01("com.facebook.stella", valueOf2);
        this.A00 = r1.A00();
    }
}
