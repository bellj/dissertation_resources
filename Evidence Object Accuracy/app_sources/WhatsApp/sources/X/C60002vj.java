package X;

import com.google.android.material.chip.Chip;
import com.whatsapp.R;

/* renamed from: X.2vj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60002vj extends C60052vo {
    public C60002vj(Chip chip, AnonymousClass2Jw r2) {
        super(chip, r2);
    }

    @Override // X.C60052vo, X.AbstractC75703kH
    public void A08(AnonymousClass4UW r4) {
        Chip chip = ((C60052vo) this).A00;
        chip.setChipIconResource(R.drawable.ic_filter);
        super.A08(r4);
        C12970iu.A19(chip.getContext(), chip, R.string.biz_dir_filter);
        C12960it.A0z(chip, this, 25);
    }
}
