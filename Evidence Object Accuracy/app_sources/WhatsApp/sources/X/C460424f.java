package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.24f  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C460424f implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C99814ku();
    public final String A00;
    public final List A01;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public /* synthetic */ C460424f(Parcel parcel) {
        this.A00 = parcel.readString();
        ArrayList arrayList = new ArrayList();
        this.A01 = arrayList;
        parcel.readStringList(arrayList);
    }

    public C460424f(String str, List list) {
        this.A00 = str;
        this.A01 = list;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A00);
        parcel.writeStringList(this.A01);
    }
}
