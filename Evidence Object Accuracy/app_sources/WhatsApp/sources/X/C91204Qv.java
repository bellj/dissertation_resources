package X;

import java.util.List;

/* renamed from: X.4Qv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C91204Qv {
    public final int A00;
    public final long A01;
    public final AnonymousClass5XM A02;
    public final List A03;

    public /* synthetic */ C91204Qv(AnonymousClass5XM r1, List list, int i, long j) {
        this.A03 = list;
        this.A02 = r1;
        this.A00 = i;
        this.A01 = j;
    }
}
