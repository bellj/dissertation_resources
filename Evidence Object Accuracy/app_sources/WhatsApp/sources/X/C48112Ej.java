package X;

import android.os.Handler;
import com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3;
import com.google.android.gms.maps.model.LatLng;
import com.whatsapp.util.ViewOnClickCListenerShape13S0100000_I0;
import java.util.List;

/* renamed from: X.2Ej  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C48112Ej extends AnonymousClass017 {
    public int A00 = 0;
    public C48122Ek A01;
    public final Handler A02 = new Handler();
    public final AnonymousClass1CO A03;
    public final AnonymousClass1B0 A04;
    public final AnonymousClass1B3 A05;
    public final AbstractC49242Jy A06;
    public final AbstractC14440lR A07;
    public final Runnable A08 = new RunnableBRunnable0Shape3S0100000_I0_3(this, 21);

    public C48112Ej(AnonymousClass1CO r3, AnonymousClass1B0 r4, AnonymousClass1B3 r5, AbstractC49242Jy r6, AbstractC14440lR r7) {
        this.A07 = r7;
        this.A03 = r3;
        this.A05 = r5;
        this.A04 = r4;
        this.A06 = r6;
        r7.Ab2(new RunnableBRunnable0Shape3S0100000_I0_3(this, 22));
    }

    public static AnonymousClass4T7 A01(AnonymousClass4T7 r10, double d, double d2) {
        List<AnonymousClass4T7> list = r10.A05;
        if (list.isEmpty()) {
            return r10;
        }
        AnonymousClass4T7 r9 = null;
        double d3 = Double.MAX_VALUE;
        for (AnonymousClass4T7 r0 : list) {
            AnonymousClass4T7 A01 = A01(r0, d, d2);
            double A00 = (double) AnonymousClass3AR.A00(new LatLng(A01.A01, A01.A02), new LatLng(d, d2));
            if (A00 < d3) {
                r9 = A01;
                d3 = A00;
            }
        }
        AnonymousClass009.A05(r9);
        return r9;
    }

    public static boolean A02(AnonymousClass4T7 r7, double d, double d2) {
        List<AnonymousClass4T7> list = r7.A05;
        if (!list.isEmpty()) {
            for (AnonymousClass4T7 r0 : list) {
                if (A02(r0, d, d2)) {
                    break;
                }
            }
        }
        if (((double) AnonymousClass3AR.A00(new LatLng(r7.A01, r7.A02), new LatLng(d, d2))) > r7.A03 + 500.0d) {
            return false;
        }
        return true;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final C37171lc A0C() {
        String str;
        switch (this.A00) {
            case 0:
                return new C84833zz(new ViewOnClickCListenerShape13S0100000_I0(this, 23));
            case 1:
                return new C84773zt();
            case 2:
                C48122Ek r2 = this.A01;
                if (r2 != null) {
                    return new C59562uw(r2, new ViewOnClickCListenerShape13S0100000_I0(this, 24));
                }
                break;
            case 3:
            case 6:
            case 7:
                return new C84763zs();
            case 4:
            case 5:
                break;
            default:
                throw new IllegalArgumentException("SearchLocationItemLiveData/buildHeader: Invalid location state");
        }
        C48122Ek A00 = this.A05.A00();
        if (A00 == null) {
            str = null;
        } else {
            str = A00.A06;
        }
        return new C59542uu(new ViewOnClickCListenerShape13S0100000_I0(this, 25), str);
    }

    public final void A0D(C48122Ek r24) {
        int i;
        C48122Ek r14 = r24;
        AnonymousClass1CO r11 = this.A03;
        Double d = r14.A03;
        double doubleValue = d.doubleValue();
        Double d2 = r14.A04;
        double doubleValue2 = d2.doubleValue();
        if (A02(r11.A00(), doubleValue, doubleValue2)) {
            i = 2;
        } else {
            AnonymousClass4T7 A00 = r11.A00();
            i = 4;
            if (((double) AnonymousClass3AR.A00(new LatLng(A00.A01, A00.A02), new LatLng(doubleValue, doubleValue2))) <= A00.A00) {
                i = 5;
            }
        }
        this.A00 = i;
        if (i == 5) {
            AnonymousClass4T7 A01 = A01(r11.A00(), d.doubleValue(), d2.doubleValue());
            r14 = new C48122Ek(Double.valueOf(A01.A03), Double.valueOf(A01.A01), Double.valueOf(A01.A02), null, null, null, A01.A04, "nearest_neighborhood");
        }
        this.A01 = r14;
    }
}
