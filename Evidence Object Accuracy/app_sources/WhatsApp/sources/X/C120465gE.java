package X;

import android.content.Context;

/* renamed from: X.5gE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120465gE extends C126705tJ {
    public final Context A00;
    public final C14900mE A01;
    public final C15570nT A02;
    public final C14830m7 A03;
    public final AnonymousClass102 A04;
    public final C14850m9 A05;
    public final C1329668y A06;
    public final C18650sn A07;
    public final C18610sj A08;
    public final C17070qD A09;
    public final AnonymousClass17Z A0A;
    public final C121265hX A0B;
    public final C18590sh A0C;
    public final AbstractC14440lR A0D;

    public C120465gE(Context context, C14900mE r3, C15570nT r4, C14830m7 r5, AnonymousClass102 r6, C14850m9 r7, C1308460e r8, C1329668y r9, C18650sn r10, C18610sj r11, C17070qD r12, AnonymousClass17Z r13, C121265hX r14, C18590sh r15, AbstractC14440lR r16) {
        super(r8.A04, r11);
        this.A03 = r5;
        this.A05 = r7;
        this.A00 = context;
        this.A01 = r3;
        this.A02 = r4;
        this.A0D = r16;
        this.A0C = r15;
        this.A09 = r12;
        this.A08 = r11;
        this.A04 = r6;
        this.A0A = r13;
        this.A07 = r10;
        this.A06 = r9;
        this.A0B = r14;
    }
}
