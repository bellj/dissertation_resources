package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.2Ar  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C47432Ar {
    public static final int A08;
    public int A00 = 0;
    public final C14900mE A01;
    public final AnonymousClass01d A02;
    public final AnonymousClass018 A03;
    public final AbstractC14440lR A04;
    public final AnonymousClass2B1 A05 = new AnonymousClass2B1(this);
    public final List A06 = new ArrayList();
    public final List A07 = new ArrayList();

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0013, code lost:
        if (android.os.Build.MODEL.equals("SAMSUNG-SM-J320A") == false) goto L_0x0015;
     */
    static {
        /*
            java.lang.String r1 = android.os.Build.MANUFACTURER
            java.lang.String r0 = "samsung"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0015
            java.lang.String r1 = android.os.Build.MODEL
            java.lang.String r0 = "SAMSUNG-SM-J320A"
            boolean r1 = r1.equals(r0)
            r0 = 4
            if (r1 != 0) goto L_0x0016
        L_0x0015:
            r0 = 1
        L_0x0016:
            X.C47432Ar.A08 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C47432Ar.<clinit>():void");
    }

    public C47432Ar(C14900mE r2, AnonymousClass01d r3, AnonymousClass018 r4, AbstractC14440lR r5) {
        this.A01 = r2;
        this.A04 = r5;
        this.A02 = r3;
        this.A03 = r4;
    }
}
