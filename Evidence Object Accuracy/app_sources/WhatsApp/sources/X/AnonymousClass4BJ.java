package X;

/* renamed from: X.4BJ  reason: invalid class name */
/* loaded from: classes3.dex */
public enum AnonymousClass4BJ {
    /* Fake field, exist only in values array */
    UNIX_LINES(0, 1),
    /* Fake field, exist only in values array */
    CASE_INSENSITIVE(1, 2),
    /* Fake field, exist only in values array */
    COMMENTS(2, 4),
    /* Fake field, exist only in values array */
    MULTILINE(3, 8),
    /* Fake field, exist only in values array */
    DOTALL(4, 32),
    /* Fake field, exist only in values array */
    UNICODE_CASE(5, 64),
    /* Fake field, exist only in values array */
    UNICODE_CHARACTER_CLASS(6, 256);
    
    public final int code;
    public final char flag;

    AnonymousClass4BJ(int i, int i2) {
        this.code = i2;
        this.flag = r2;
    }
}
