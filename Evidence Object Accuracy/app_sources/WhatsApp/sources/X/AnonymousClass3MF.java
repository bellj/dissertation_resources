package X;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;
import java.util.List;

/* renamed from: X.3MF  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3MF implements TextWatcher {
    public boolean A00 = false;
    public final C14260l7 A01;
    public final AnonymousClass3C4 A02;
    public final AnonymousClass28D A03;
    public final List A04;

    public AnonymousClass3MF(C14260l7 r2, AnonymousClass28D r3) {
        this.A03 = r3;
        this.A01 = r2;
        this.A02 = (AnonymousClass3C4) AnonymousClass3JV.A04(r2, r3);
        this.A04 = C12960it.A0l();
    }

    @Override // android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        View view;
        View view2;
        AnonymousClass3C4 r3 = this.A02;
        r3.A0E = editable;
        r3.A0O = false;
        if (!this.A00) {
            this.A00 = true;
            for (TextWatcher textWatcher : this.A04) {
                textWatcher.afterTextChanged(r3.A0E);
            }
            AnonymousClass28D r4 = this.A03;
            if (r4.A0O(63, false)) {
                C89054Im r2 = r4.A03;
                if (r2 == null) {
                    view = null;
                } else {
                    view = r2.A00;
                }
                if (r3.A04 != ((TextView) view).getLineCount()) {
                    if (r2 == null) {
                        view2 = null;
                    } else {
                        view2 = r2.A00;
                    }
                    r3.A04 = ((TextView) view2).getLineCount();
                    AnonymousClass3JV.A03(this.A01).A07(new C57932nr(this), (long) r4.A00);
                }
            }
            AbstractC14200l1 A0G = r4.A0G(48);
            if (A0G != null) {
                C14210l2 r0 = new C14210l2();
                r0.A05(r4, 0);
                C14260l7 r1 = this.A01;
                C28701Oq.A01(r1, r4, C14210l2.A01(r0, r1, 1), A0G);
            }
            this.A00 = false;
        }
    }

    @Override // android.text.TextWatcher
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        if (!this.A00) {
            this.A00 = true;
            for (TextWatcher textWatcher : this.A04) {
                textWatcher.beforeTextChanged(charSequence, i, i2, i3);
            }
            this.A00 = false;
        }
    }

    @Override // android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        if (!this.A00) {
            this.A00 = true;
            for (TextWatcher textWatcher : this.A04) {
                textWatcher.onTextChanged(charSequence, i, i2, i3);
            }
            this.A00 = false;
        }
    }
}
