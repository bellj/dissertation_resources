package X;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/* renamed from: X.4b5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C93994b5 {
    public static final C93994b5 A02 = new C93994b5();
    public final AbstractC115285Qx A00 = new C1090150b();
    public final ConcurrentMap A01 = new ConcurrentHashMap();

    public final AnonymousClass5XU A00(Class cls) {
        AbstractC115255Qu r8;
        AbstractC94184bO r5;
        AnonymousClass4DX r9;
        AnonymousClass4DV r4;
        AbstractC115665Sl r6;
        AnonymousClass4DX r2;
        AnonymousClass4DV r1;
        Class cls2;
        if (cls != null) {
            ConcurrentMap concurrentMap = this.A01;
            AnonymousClass5XU r42 = (AnonymousClass5XU) concurrentMap.get(cls);
            if (r42 == null) {
                C1090150b r12 = (C1090150b) this.A00;
                if (AbstractC80173rp.class.isAssignableFrom(cls) || (cls2 = C95684eB.A03) == null || cls2.isAssignableFrom(cls)) {
                    AbstractC115245Qt Ah6 = r12.A00.Ah6(cls);
                    AnonymousClass50S r43 = (AnonymousClass50S) Ah6;
                    int i = r43.A00;
                    if ((i & 2) == 2) {
                        if (AbstractC80173rp.class.isAssignableFrom(cls)) {
                            r2 = C95684eB.A02;
                            r1 = AnonymousClass4H3.A00;
                        } else {
                            r2 = C95684eB.A00;
                            r1 = AnonymousClass4H3.A01;
                            if (r1 == null) {
                                throw C12960it.A0U("Protobuf runtime is not correctly loaded.");
                            }
                        }
                        r42 = new AnonymousClass50Z(r1, r43.A01, r2);
                    } else {
                        boolean isAssignableFrom = AbstractC80173rp.class.isAssignableFrom(cls);
                        boolean z = true;
                        if ((i & 1) != 1) {
                            z = false;
                        }
                        if (isAssignableFrom) {
                            r8 = AnonymousClass4H5.A01;
                            r5 = AbstractC94184bO.A01;
                            r9 = C95684eB.A02;
                            if (z) {
                                r4 = AnonymousClass4H3.A00;
                                r6 = AnonymousClass4H4.A01;
                            } else {
                                r4 = null;
                                r6 = AnonymousClass4H4.A01;
                            }
                        } else {
                            r8 = AnonymousClass4H5.A00;
                            r5 = AbstractC94184bO.A00;
                            if (z) {
                                r9 = C95684eB.A00;
                                r4 = AnonymousClass4H3.A01;
                                if (r4 != null) {
                                    r6 = AnonymousClass4H4.A00;
                                } else {
                                    throw C12960it.A0U("Protobuf runtime is not correctly loaded.");
                                }
                            } else {
                                r9 = C95684eB.A01;
                                r4 = null;
                                r6 = AnonymousClass4H4.A00;
                            }
                        }
                        r42 = C1090050a.A0B(r4, r5, r6, Ah6, r8, r9);
                    }
                    AnonymousClass5XU r0 = (AnonymousClass5XU) concurrentMap.putIfAbsent(cls, r42);
                    if (r0 != null) {
                        return r0;
                    }
                } else {
                    throw C12970iu.A0f("Message classes must extend GeneratedMessage or GeneratedMessageLite");
                }
            }
            return r42;
        }
        throw C12980iv.A0n("messageType");
    }
}
