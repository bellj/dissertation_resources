package X;

import com.whatsapp.wabloks.ui.WaBloksActivity;

/* renamed from: X.6Es  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C134466Es implements AbstractC136386Mi {
    public final AnonymousClass018 A00;

    public C134466Es(AnonymousClass018 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC136386Mi
    public AbstractC119415dw A8P(WaBloksActivity waBloksActivity) {
        return new C124445pW(this.A00, waBloksActivity);
    }

    @Override // X.AbstractC136386Mi
    public AbstractC119405dv A8Q(WaBloksActivity waBloksActivity, C129125xD r4) {
        return new C124435pV(this.A00, waBloksActivity);
    }
}
