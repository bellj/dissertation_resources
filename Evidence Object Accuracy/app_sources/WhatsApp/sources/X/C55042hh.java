package X;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.2hh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C55042hh extends AnonymousClass03U {
    public final ImageView A00;
    public final ImageView A01;
    public final ImageView A02;
    public final TextView A03;
    public final TextView A04;
    public final AnonymousClass3CN A05;
    public final C14830m7 A06;
    public final AnonymousClass018 A07;

    public C55042hh(View view, AnonymousClass3CN r3, C14830m7 r4, AnonymousClass018 r5) {
        super(view);
        this.A06 = r4;
        this.A07 = r5;
        this.A05 = r3;
        this.A01 = C12970iu.A0K(view, R.id.device_icon);
        this.A03 = C12960it.A0I(view, R.id.name);
        this.A04 = C12960it.A0I(view, R.id.status);
        this.A02 = C12970iu.A0K(view, R.id.sync_badge);
        this.A00 = C12970iu.A0K(view, R.id.error_badge);
    }
}
