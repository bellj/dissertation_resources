package X;

import android.graphics.drawable.Drawable;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/* renamed from: X.2bQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52632bQ extends Animation {
    public final int A00;
    public final Drawable A01;
    public final ViewGroup A02;

    @Override // android.view.animation.Animation
    public boolean willChangeBounds() {
        return false;
    }

    public C52632bQ(Drawable drawable, ViewGroup viewGroup, int i) {
        this.A02 = viewGroup;
        this.A00 = i;
        this.A01 = drawable;
    }

    @Override // android.view.animation.Animation
    public void applyTransformation(float f, Transformation transformation) {
        int i = this.A00;
        int i2 = i - ((int) (((float) i) * f));
        ViewGroup viewGroup = this.A02;
        Drawable background = viewGroup.getBackground();
        if (!(background instanceof AnonymousClass2Zg)) {
            return;
        }
        if (f == 1.0f) {
            AnonymousClass2Zg.A00(this.A01, viewGroup);
            return;
        }
        AnonymousClass2Zg r1 = (AnonymousClass2Zg) background;
        r1.A00 = i2;
        r1.invalidateSelf();
    }
}
