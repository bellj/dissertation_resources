package X;

import android.database.Cursor;
import com.facebook.redex.RunnableBRunnable0Shape0S0201000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0311000_I0;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1Em  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26701Em {
    public final C22490zA A00;
    public final C14900mE A01;
    public final C15570nT A02;
    public final AnonymousClass12H A03;
    public final C21400xM A04;
    public final C14850m9 A05;
    public final ExecutorC27271Gr A06;

    public C26701Em(C22490zA r3, C14900mE r4, C15570nT r5, AnonymousClass12H r6, C21400xM r7, C14850m9 r8, AbstractC14440lR r9) {
        this.A05 = r8;
        this.A01 = r4;
        this.A02 = r5;
        this.A03 = r6;
        this.A04 = r7;
        this.A00 = r3;
        this.A06 = new ExecutorC27271Gr(r9, true);
    }

    public static boolean A00(AbstractC15340mz r4, byte b) {
        int i;
        if (r4 == null || (i = r4.A07) == 0) {
            return false;
        }
        if (b == 56 && (i & 1) == 1) {
            if (r4.A0V != null) {
                return false;
            }
        } else if (!(r4 instanceof C27671Iq) || b != 67) {
            if (!(b == 68 && (i & 4) == 4 && r4.A18 == null)) {
                return false;
            }
        } else if (!((i & 2) == 2 && ((C27671Iq) r4).A03 == null)) {
            return false;
        }
        return true;
    }

    public synchronized void A01(AbstractC15340mz r12, byte b) {
        if (A00(r12, b)) {
            C21400xM r7 = this.A04;
            ArrayList arrayList = new ArrayList();
            C16310on A01 = r7.A0D.get();
            AnonymousClass1ED r6 = r7.A0I;
            Cursor A012 = AnonymousClass1ED.A01(A01, b, r12.A11);
            try {
                HashMap A00 = AnonymousClass1sB.A00(A012, b);
                while (A012.moveToNext()) {
                    AnonymousClass1Iv A04 = r6.A04(A012, A00);
                    if (A04 == null) {
                        Log.e("MessageAddOnManager/getMessageAddOnForParentMessage unexpected fmessage");
                    } else {
                        boolean z = A04 instanceof C27711Iw;
                        if (!z || (r12 instanceof C27671Iq)) {
                            A04.A15(A012, r7.A0B, A00);
                            A04.A02 = new C40711sC(r12.A0B(), r12.A0z);
                            if (z) {
                                C27711Iw r3 = (C27711Iw) A04;
                                List A002 = r7.A0G.A06.A00(r3.A11);
                                List list = r3.A05;
                                list.clear();
                                list.addAll(A002);
                                AnonymousClass1EI.A00((C27671Iq) r12, r3);
                            }
                            arrayList.add(A04);
                        } else {
                            Log.e("MessageAddOnManager/getMessageAddOnForParentMessage parent is not poll for poll vote");
                        }
                    }
                }
                A012.close();
                A01.close();
                if (b == 56) {
                    ArrayList arrayList2 = new ArrayList();
                    Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        AnonymousClass1Iv r1 = (AnonymousClass1Iv) it.next();
                        if (r1 instanceof AnonymousClass1X9) {
                            arrayList2.add((AnonymousClass1X9) r1);
                        }
                    }
                    C40481rf r13 = new C40481rf(this.A02, arrayList2);
                    if (r12.A0V == null) {
                        r12.A0V = r13;
                    } else {
                        throw new IllegalStateException("FMessage/setMessageReactions re-assigning messageReactions");
                    }
                } else if ((r12 instanceof C27671Iq) && b == 67) {
                    ArrayList arrayList3 = new ArrayList();
                    Iterator it2 = arrayList.iterator();
                    while (it2.hasNext()) {
                        AnonymousClass1Iv r14 = (AnonymousClass1Iv) it2.next();
                        if (r14 instanceof C27711Iw) {
                            arrayList3.add(r14);
                        }
                    }
                    C27671Iq r122 = (C27671Iq) r12;
                    if (r122.A03 == null) {
                        r122.A03 = arrayList3;
                    } else {
                        throw new IllegalStateException("FMessagePoll/setPollVotes re-assigning pollVotes");
                    }
                } else if (b == 68) {
                    boolean z2 = true;
                    if (arrayList.size() > 1) {
                        z2 = false;
                    }
                    AnonymousClass009.A0A("Multiple KeepInChat messages linked to a parent message", z2);
                    Iterator it3 = arrayList.iterator();
                    while (it3.hasNext()) {
                        AnonymousClass1Iv r15 = (AnonymousClass1Iv) it3.next();
                        if (r15 instanceof C30311Wx) {
                            C30311Wx r16 = (C30311Wx) r15;
                            r12.A18 = r16;
                            if (r16 != null) {
                                r12.A06 = r16.A01;
                            }
                        }
                    }
                }
            } catch (Throwable th) {
                if (A012 != null) {
                    try {
                        A012.close();
                    } catch (Throwable unused) {
                    }
                }
                throw th;
            }
        }
    }

    public void A02(AbstractC15340mz r9, Runnable runnable, byte b) {
        boolean A01 = this.A00.A00().A01(r9.A0z.A00);
        if (A00(r9, b)) {
            this.A06.execute(new RunnableBRunnable0Shape0S0311000_I0(this, r9, runnable, b, 6, A01));
        } else {
            A03(r9, runnable, b, false, A01);
        }
    }

    public final void A03(AbstractC15340mz r10, Runnable runnable, byte b, boolean z, boolean z2) {
        this.A01.A0H(new RunnableBRunnable0Shape0S0311000_I0(this, runnable, r10, b, 7, z));
        if (z2) {
            this.A06.execute(new RunnableBRunnable0Shape0S0201000_I0(this, r10, b, 26));
        }
    }
}
