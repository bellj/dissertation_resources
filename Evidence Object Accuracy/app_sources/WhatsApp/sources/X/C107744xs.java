package X;

import java.util.Collections;
import java.util.List;

/* renamed from: X.4xs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107744xs implements AbstractC116805Wy {
    public final long[] A00;
    public final C93834ao[] A01;

    public C107744xs(long[] jArr, C93834ao[] r2) {
        this.A01 = r2;
        this.A00 = jArr;
    }

    @Override // X.AbstractC116805Wy
    public List ABx(long j) {
        C93834ao r1;
        int A06 = AnonymousClass3JZ.A06(this.A00, j, false);
        if (A06 == -1 || (r1 = this.A01[A06]) == C93834ao.A0G) {
            return Collections.emptyList();
        }
        return Collections.singletonList(r1);
    }

    @Override // X.AbstractC116805Wy
    public long ACn(int i) {
        boolean z = true;
        C95314dV.A03(C12990iw.A1W(i));
        long[] jArr = this.A00;
        if (i >= jArr.length) {
            z = false;
        }
        C95314dV.A03(z);
        return jArr[i];
    }

    @Override // X.AbstractC116805Wy
    public int ACo() {
        return this.A00.length;
    }

    @Override // X.AbstractC116805Wy
    public int AEd(long j) {
        long[] jArr = this.A00;
        int A05 = AnonymousClass3JZ.A05(jArr, j, false);
        if (A05 >= jArr.length) {
            return -1;
        }
        return A05;
    }
}
