package X;

/* renamed from: X.3E5  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3E5 {
    public final AnonymousClass1V8 A00;
    public final String A01;

    public AnonymousClass3E5(AnonymousClass1V8 r3) {
        AnonymousClass1V8.A01(r3, "screen_data");
        this.A01 = AnonymousClass3JT.A07(r3, "params", C13000ix.A08());
        this.A00 = r3;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || AnonymousClass3E5.class != obj.getClass()) {
            return false;
        }
        return this.A01.equals(((AnonymousClass3E5) obj).A01);
    }

    public int hashCode() {
        return C12970iu.A08(this.A01, C12970iu.A1b());
    }
}
