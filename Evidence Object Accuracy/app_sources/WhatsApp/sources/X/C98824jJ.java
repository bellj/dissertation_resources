package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

/* renamed from: X.4jJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98824jJ implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        ArrayList arrayList = null;
        String str = null;
        int i = 0;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                i = C95664e9.A02(parcel, readInt);
            } else if (c != 2) {
                str = C95664e9.A09(parcel, str, c, 3, readInt);
            } else {
                arrayList = C95664e9.A0B(parcel, C78533p4.CREATOR, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78443ov(str, arrayList, i);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new C78443ov[i];
    }
}
