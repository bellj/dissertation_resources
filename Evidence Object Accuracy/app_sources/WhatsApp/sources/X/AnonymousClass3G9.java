package X;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.TypedValue;

/* renamed from: X.3G9  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3G9 {
    public static int A00(Context context) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(new TypedValue().data, new int[]{16843499});
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(0, -1);
        obtainStyledAttributes.recycle();
        return dimensionPixelSize;
    }

    public static int A01(Context context, float f) {
        return Math.round((f * ((float) context.getResources().getDisplayMetrics().densityDpi)) / 160.0f);
    }
}
