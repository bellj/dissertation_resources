package X;

import android.view.ViewGroup;
import com.facebook.redex.IDxObserverShape5S0100000_3_I1;
import java.util.List;

/* renamed from: X.5c2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118575c2 extends AnonymousClass02M {
    public List A00 = C12960it.A0l();
    public final /* synthetic */ ActivityC121715je A01;

    public C118575c2(ActivityC121715je r2) {
        this.A01 = r2;
    }

    public static void A00(IDxObserverShape5S0100000_3_I1 iDxObserverShape5S0100000_3_I1, Object obj) {
        C118575c2 r0 = ((ActivityC121715je) iDxObserverShape5S0100000_3_I1.A00).A03;
        r0.A00 = (List) obj;
        r0.A02();
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A00.size();
    }

    @Override // X.AnonymousClass02M
    public void ANH(AnonymousClass03U r3, int i) {
        if (!(r3 instanceof AbstractC118825cR)) {
            if (r3 instanceof AbstractC118835cS) {
                ((AbstractC118835cS) r3).A08((AbstractC125975s7) this.A00.get(i), i);
                return;
            } else if (!(r3 instanceof AbstractC122475lW)) {
                throw C12990iw.A0m(C30931Zj.A01("PaymentComponentListActivity", "unsupported holder"));
            }
        }
        ((AbstractC118825cR) r3).A08((AbstractC125975s7) this.A00.get(i), i);
    }

    @Override // X.AnonymousClass02M
    public AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        return this.A01.A2e(viewGroup, i);
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        return ((AbstractC125975s7) this.A00.get(i)).A00;
    }
}
