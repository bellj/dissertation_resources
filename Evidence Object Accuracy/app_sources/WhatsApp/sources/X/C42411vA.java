package X;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.facebook.redex.RunnableBRunnable0Shape3S0200000_I0_3;
import com.whatsapp.R;
import com.whatsapp.WaImageView;

/* renamed from: X.1vA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C42411vA extends AnonymousClass1PZ {
    public Activity A00;
    public C15370n3 A01;
    public boolean A02;
    public final C22640zP A03;
    public final C15550nR A04;
    public final AnonymousClass11D A05;
    public final AnonymousClass2JT A06;
    public final C14830m7 A07;
    public final C19990v2 A08;
    public final C15600nX A09;
    public final C20710wC A0A;
    public final AnonymousClass5UJ A0B = new AnonymousClass5UJ() { // from class: X.57B
        @Override // X.AnonymousClass5UJ
        public final void ALi(AbstractC14640lm r3) {
            C42411vA r1 = C42411vA.this;
            if (r1.A0f.equals(r3)) {
                r1.A02();
            }
        }
    };
    public final AnonymousClass11A A0C;
    public final AnonymousClass1E5 A0D;
    public final C15580nU A0E;

    public C42411vA(ActivityC000800j r32, C253519b r33, C14900mE r34, AnonymousClass2TT r35, AnonymousClass10A r36, C22330yu r37, C22640zP r38, AnonymousClass130 r39, C15550nR r40, AnonymousClass10S r41, AnonymousClass11D r42, C15610nY r43, AnonymousClass1J1 r44, AnonymousClass131 r45, C14830m7 r46, AnonymousClass018 r47, C19990v2 r48, C20830wO r49, C15600nX r50, C15370n3 r51, AnonymousClass19M r52, C14850m9 r53, C20710wC r54, AnonymousClass11A r55, C244215l r56, AnonymousClass1E5 r57, C15580nU r58, AnonymousClass12F r59, AbstractC14440lR r60) {
        super(r32, r33, r34, r35, r36, r37, r39, r41, r43, r44, r45, r47, null, r48, r49, r50, r51, r52, r53, r54, r56, r58, r59, r60);
        this.A07 = r46;
        this.A04 = r40;
        this.A03 = r38;
        this.A0C = r55;
        this.A09 = r50;
        this.A05 = r42;
        this.A0E = r58;
        this.A08 = r48;
        this.A0D = r57;
        this.A0A = r54;
        this.A06 = new AnonymousClass2JT(r43, new AnonymousClass2JU() { // from class: X.556
            @Override // X.AnonymousClass2JU
            public final void AR6() {
                C42411vA.this.A03();
            }
        }, r58, r60);
    }

    @Override // X.AnonymousClass1PZ
    public ViewGroup A01(Context context) {
        boolean A0A = this.A03.A0A(this.A0E);
        this.A02 = A0A;
        if (A0A) {
            return (ViewGroup) LayoutInflater.from(context).inflate(R.layout.conversation_actionbar_subgroup, (ViewGroup) null, false);
        }
        return super.A01(context);
    }

    @Override // X.AnonymousClass1PZ
    public void A02() {
        super.A02();
        if (this.A02 != this.A03.A0A(this.A0E)) {
            A04(this.A00);
        }
        this.A06.A00();
        A05();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0089, code lost:
        if (r0 == false) goto L_0x008b;
     */
    @Override // X.AnonymousClass1PZ
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A04(android.app.Activity r6) {
        /*
            r5 = this;
            super.A04(r6)
            android.widget.TextView r0 = r5.A08
            android.content.Context r2 = r0.getContext()
            X.0n3 r0 = r5.A0F
            boolean r0 = r0.A0Y
            if (r0 == 0) goto L_0x0033
            r1 = 2131893163(0x7f121bab, float:1.9421095E38)
        L_0x0012:
            java.lang.String r1 = r2.getString(r1)
        L_0x0016:
            android.widget.TextView r0 = r5.A08
            r0.setText(r1)
            android.view.ViewGroup r2 = r5.A03
            r1 = 19
            com.facebook.redex.ViewOnClickCListenerShape0S0200000_I0 r0 = new com.facebook.redex.ViewOnClickCListenerShape0S0200000_I0
            r0.<init>(r5, r1, r6)
            r2.setOnClickListener(r0)
            android.view.ViewGroup r1 = r5.A03
            r0 = 2131886128(0x7f120030, float:1.9406826E38)
            X.AnonymousClass23N.A02(r1, r0)
            r5.A05()
            return
        L_0x0033:
            X.0zP r4 = r5.A03
            boolean r0 = r4.A07()
            if (r0 == 0) goto L_0x005c
            X.0v2 r0 = r5.A08
            X.0nU r3 = r5.A0E
            int r1 = r0.A02(r3)
            r0 = 3
            if (r1 != r0) goto L_0x005c
            r0 = 2131887257(0x7f120499, float:1.9409116E38)
            java.lang.String r1 = r2.getString(r0)
            boolean r0 = r4.A0A(r3)
            if (r0 == 0) goto L_0x0016
            X.4rd r0 = new X.4rd
            r0.<init>(r6, r5)
            r5.A06(r0)
            goto L_0x0016
        L_0x005c:
            boolean r0 = r4.A07()
            if (r0 == 0) goto L_0x0078
            X.0v2 r1 = r5.A08
            X.0nU r0 = r5.A0E
            int r1 = r1.A02(r0)
            r0 = 2
            if (r1 != r0) goto L_0x0078
            X.3PH r0 = new X.3PH
            r0.<init>(r6, r5)
            r5.A06(r0)
            java.lang.String r1 = ""
            goto L_0x0016
        L_0x0078:
            X.0n3 r0 = r5.A0F
            boolean r0 = r0.A0W
            if (r0 == 0) goto L_0x008b
            X.0nX r1 = r5.A09
            X.0nU r0 = r5.A0E
            boolean r0 = r1.A0D(r0)
            r1 = 2131890043(0x7f120f7b, float:1.9414767E38)
            if (r0 != 0) goto L_0x0012
        L_0x008b:
            r1 = 2131892196(0x7f1217e4, float:1.9419133E38)
            goto L_0x0012
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C42411vA.A04(android.app.Activity):void");
    }

    public final void A05() {
        int i = 0;
        boolean z = false;
        if (this.A0F.A01 > 0) {
            z = true;
        }
        WaImageView waImageView = super.A0C;
        if (!z) {
            i = 8;
        }
        waImageView.setVisibility(i);
        if (z) {
            super.A0C.setImageResource(R.drawable.ic_ephemeral_v2);
        }
    }

    public final synchronized void A06(AnonymousClass024 r4) {
        C15370n3 r0 = this.A01;
        if (r0 != null) {
            r4.accept(r0);
        } else {
            this.A0h.Ab2(new RunnableBRunnable0Shape3S0200000_I0_3(this, 1, r4));
        }
    }

    @Override // X.AnonymousClass1PZ, X.C28791Pa, android.app.Application.ActivityLifecycleCallbacks
    public void onActivityCreated(Activity activity, Bundle bundle) {
        super.onActivityCreated(activity, bundle);
        this.A00 = activity;
        AnonymousClass11A r0 = this.A0C;
        r0.A00.add(this.A0B);
    }

    @Override // X.AnonymousClass1PZ, X.C28791Pa, android.app.Application.ActivityLifecycleCallbacks
    public void onActivityDestroyed(Activity activity) {
        super.onActivityDestroyed(activity);
        AnonymousClass11A r0 = this.A0C;
        r0.A00.remove(this.A0B);
        AnonymousClass2JT r3 = this.A06;
        AnonymousClass2JS r2 = r3.A00;
        if (r2 != null) {
            r2.A03(false);
            Handler handler = r2.A00;
            if (handler != null) {
                handler.removeCallbacks(r2.A01);
            }
            r2.A00 = null;
            r2.A01 = null;
            r3.A00 = null;
        }
    }
}
