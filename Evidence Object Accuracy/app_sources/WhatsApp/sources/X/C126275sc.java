package X;

/* renamed from: X.5sc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C126275sc {
    public final AnonymousClass1V8 A00;

    public C126275sc(AnonymousClass3CS r11, String str, String str2) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy A0N = C117295Zj.A0N(A0M);
        C41141sy.A01(A0N, "action", "br-get-merchant-status");
        if (C117295Zj.A1V(str, 1, false)) {
            C41141sy.A01(A0N, "device-id", str);
        }
        if (AnonymousClass3JT.A0E(str2, 11, 14, false)) {
            C41141sy.A01(A0N, "tax-id", str2);
        }
        this.A00 = C117295Zj.A0I(A0N, A0M, r11);
    }
}
