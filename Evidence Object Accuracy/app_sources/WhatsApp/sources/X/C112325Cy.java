package X;

import java.util.Enumeration;
import java.util.NoSuchElementException;

/* renamed from: X.5Cy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C112325Cy implements Enumeration {
    public final /* synthetic */ AnonymousClass5MN A00;

    @Override // java.util.Enumeration
    public boolean hasMoreElements() {
        return false;
    }

    @Override // java.util.Enumeration
    public Object nextElement() {
        throw new NoSuchElementException("Empty Enumeration");
    }

    public /* synthetic */ C112325Cy(AnonymousClass5MN r1) {
        this.A00 = r1;
    }
}
