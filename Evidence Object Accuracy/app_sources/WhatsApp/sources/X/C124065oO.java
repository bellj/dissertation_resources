package X;

import android.content.Intent;
import com.whatsapp.payments.ui.BrazilPayBloksActivity;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/* renamed from: X.5oO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124065oO extends AbstractC16350or {
    public final C125605rW A00;
    public final C17070qD A01;

    public C124065oO(C125605rW r1, C17070qD r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        List A0Z = C117295Zj.A0Z(this.A01);
        if (A0Z.size() <= 0) {
            return null;
        }
        Collections.sort(A0Z, new Comparator() { // from class: X.6KJ
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                AbstractC28901Pl r11 = (AbstractC28901Pl) obj2;
                AbstractC30871Zd r0 = (AbstractC30871Zd) ((AbstractC28901Pl) obj).A08;
                long j = Long.MAX_VALUE;
                if (r0 != null) {
                    long j2 = r0.A06;
                    if (j2 >= 0) {
                        j = j2;
                    }
                }
                AbstractC30871Zd r02 = (AbstractC30871Zd) r11.A08;
                long j3 = Long.MAX_VALUE;
                if (r02 != null) {
                    long j4 = r02.A06;
                    if (j4 >= 0) {
                        j3 = j4;
                    }
                }
                return (j > j3 ? 1 : (j == j3 ? 0 : -1));
            }
        });
        AbstractC28901Pl r5 = (AbstractC28901Pl) C12980iv.A0o(A0Z);
        AnonymousClass1ZY r0 = r5.A08;
        if (r0 == null || ((AbstractC30871Zd) r0).A06 < 0) {
            return null;
        }
        return C117315Zl.A05(Integer.valueOf(A0Z.size()), r5);
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        String str;
        AnonymousClass01T r6 = (AnonymousClass01T) obj;
        if (r6 != null) {
            Object obj2 = r6.A01;
            AnonymousClass009.A05(obj2);
            AbstractC28901Pl r2 = (AbstractC28901Pl) obj2;
            ActivityC13790kL r4 = this.A00.A00;
            if (r2 == null) {
                C36021jC.A01(r4, 102);
                return;
            }
            HashMap A11 = C12970iu.A11();
            A11.put("credential_id", r2.A0A);
            A11.put("last4", C117295Zj.A0R(r2.A09));
            AbstractC30871Zd r22 = (AbstractC30871Zd) r2.A08;
            if (r22 != null) {
                str = C12960it.A0f(C12960it.A0k(""), r22.A04);
            } else {
                str = "-1";
            }
            A11.put("remaining_retries", str);
            Intent A0D = C12990iw.A0D(r4, BrazilPayBloksActivity.class);
            A0D.putExtra("screen_name", "brpay_p_reset_pin_from_card");
            A0D.putExtra("screen_params", A11);
            r4.startActivity(A0D);
            return;
        }
        C36021jC.A01(this.A00.A00, 102);
    }
}
