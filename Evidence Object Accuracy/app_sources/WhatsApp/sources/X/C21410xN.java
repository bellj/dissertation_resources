package X;

import android.content.ContentValues;
import android.database.Cursor;
import com.whatsapp.jid.UserJid;

/* renamed from: X.0xN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21410xN {
    public final C15550nR A00;
    public final C241814n A01;
    public final C16510p9 A02;
    public final C19990v2 A03;
    public final C16490p7 A04;
    public final C242214r A05;
    public final AnonymousClass150 A06;

    public C21410xN(C15550nR r1, C241814n r2, C16510p9 r3, C19990v2 r4, C16490p7 r5, C242214r r6, AnonymousClass150 r7) {
        this.A02 = r3;
        this.A03 = r4;
        this.A00 = r1;
        this.A01 = r2;
        this.A04 = r5;
        this.A05 = r6;
        this.A06 = r7;
    }

    public static final int A00(C16310on r5, AbstractC15340mz r6) {
        ContentValues contentValues = new ContentValues(2);
        contentValues.put("duration", Integer.valueOf(r6.A04));
        Long l = r6.A0Y;
        AnonymousClass009.A05(l);
        contentValues.put("expire_timestamp", l);
        return r5.A03.A00("message_ephemeral", contentValues, "message_row_id = ?", new String[]{String.valueOf(r6.A11)});
    }

    public AnonymousClass1PG A01(AbstractC15340mz r11, long j) {
        int i;
        int i2;
        AnonymousClass1IS r3 = r11.A0z;
        if (!r3.A02) {
            int i3 = r11.A04;
            if (i3 > 0 && !this.A01.A01(i3)) {
                C19990v2 r2 = this.A03;
                C15550nR r1 = this.A00;
                AbstractC14640lm r0 = r3.A00;
                AnonymousClass009.A05(r0);
                r11.A0V(C32341c0.A00(r1, r2, r0));
                r11.A0j(0L);
            }
        } else if (r11.A11() && !r11.A1B && !(r11 instanceof AnonymousClass1XK)) {
            AbstractC14640lm r5 = r3.A00;
            int i4 = 1;
            if (C15380n4.A0J(r5)) {
                C15370n3 A0A = this.A00.A0A(r5);
                if (A0A != null && (i2 = A0A.A01) > 0) {
                    r11.A0V(i2);
                }
            } else if (r5 instanceof UserJid) {
                AnonymousClass1PE A06 = this.A03.A06(r5);
                if (A06 != null) {
                    AnonymousClass1PG r8 = A06.A0Y;
                    if (r8 != null && ((i = r8.expiration) > 0 || r8.ephemeralSettingTimestamp > 0)) {
                        r11.A0V(i);
                        r11.A0j(Long.valueOf(r8.ephemeralSettingTimestamp));
                        int i5 = r8.disappearingMessagesInitiator;
                        if (i5 == 2) {
                            r11.A00 = 2;
                        } else if (i5 == 1) {
                            r11.A00 = 1;
                        } else {
                            r11.A00 = 0;
                        }
                    }
                } else {
                    AnonymousClass150 r12 = this.A06;
                    UserJid A02 = r12.A02(r5);
                    if (A02 != null) {
                        int A00 = r12.A00(A02);
                        long A01 = r12.A01(A02);
                        if (A02.equals(r5)) {
                            i4 = 2;
                        }
                        r11.A0V(A00);
                        r11.A0j(Long.valueOf(A01));
                        r11.A00 = i4;
                    }
                }
            }
        }
        int i6 = r11.A04;
        if (i6 > 0) {
            r11.A0Y = Long.valueOf(j + (((long) i6) * 1000));
        }
        Long l = r11.A0X;
        if (l == null) {
            l = 0L;
        }
        return new AnonymousClass1PG(i6, l.longValue(), r11.A00);
    }

    public void A02(AbstractC15340mz r8) {
        C16310on A01 = this.A04.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT duration, expire_timestamp, keep_in_chat FROM message_ephemeral WHERE message_row_id = ?", new String[]{Long.toString(r8.A11)});
            if (A09 != null) {
                if (A09.moveToNext()) {
                    int columnIndexOrThrow = A09.getColumnIndexOrThrow("duration");
                    int columnIndexOrThrow2 = A09.getColumnIndexOrThrow("expire_timestamp");
                    int columnIndexOrThrow3 = A09.getColumnIndexOrThrow("keep_in_chat");
                    r8.A0V(A09.getInt(columnIndexOrThrow));
                    r8.A0Y = Long.valueOf(A09.getLong(columnIndexOrThrow2));
                    r8.A06 = A09.getInt(columnIndexOrThrow3);
                }
                A09.close();
            }
            A01.close();
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A03(AbstractC15340mz r6) {
        C16310on A02 = this.A04.A02();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("message_row_id", Long.valueOf(r6.A11));
            contentValues.put("duration", Integer.valueOf(r6.A04));
            Long l = r6.A0Y;
            AnonymousClass009.A05(l);
            contentValues.put("expire_timestamp", l);
            A02.A03.A02(contentValues, "message_ephemeral");
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
