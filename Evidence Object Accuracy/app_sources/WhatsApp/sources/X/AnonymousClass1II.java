package X;

import android.os.HandlerThread;

/* renamed from: X.1II  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1II {
    public int A00 = 1;
    public HandlerThread A01;
    public AnonymousClass2BI A02;
    public AnonymousClass1IJ A03;
    public final AnonymousClass2BH A04 = new AnonymousClass2BH(this);

    public AnonymousClass1II(HandlerThread handlerThread) {
        this.A01 = handlerThread;
    }

    public synchronized void A00(AnonymousClass1IJ r5, long j) {
        AnonymousClass2BI r2 = this.A02;
        if (r2 == null) {
            r2 = new AnonymousClass2BI(this.A01.getLooper(), this.A04);
            this.A02 = r2;
        }
        AnonymousClass009.A06(r5, "timerCallback cannot be null, or we are using the resources without any actual use");
        AnonymousClass009.A06(r2, "Handler for timer cannot be null");
        boolean z = true;
        if (this.A00 != 1) {
            z = false;
        }
        AnonymousClass009.A0C("Timer is in running state, please call cancel or wait for timer to complete before starting the timer again", z);
        this.A03 = r5;
        this.A00 = 2;
        r2.A00(j);
    }
}
