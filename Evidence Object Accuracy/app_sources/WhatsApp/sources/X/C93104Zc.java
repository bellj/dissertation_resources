package X;

import com.facebook.msys.mci.DefaultCrypto;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

/* renamed from: X.4Zc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C93104Zc {
    public static final AnonymousClass4PR A00;
    public static final ByteBuffer A01;
    public static final Charset A02 = Charset.forName(DefaultCrypto.UTF_8);
    public static final Charset A03 = Charset.forName("ISO-8859-1");
    public static final byte[] A04;

    static {
        byte[] bArr = new byte[0];
        A04 = bArr;
        A01 = ByteBuffer.wrap(bArr);
        AnonymousClass4PR r2 = new AnonymousClass4PR(bArr);
        try {
            int i = r2.A00 + r2.A01;
            r2.A00 = i;
            int i2 = i - 0;
            if (i2 > 0) {
                int i3 = i2 - 0;
                r2.A01 = i3;
                r2.A00 = i - i3;
            } else {
                r2.A01 = 0;
            }
            A00 = r2;
        } catch (C868148y e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static Object A00(Object obj, Object obj2) {
        AbstractC80173rp r3 = (AbstractC80173rp) ((AbstractC117135Yq) obj);
        C80013rZ A06 = AnonymousClass50T.A06(r3);
        C80013rZ.A00(A06);
        AbstractC80173rp r1 = A06.A00;
        C72463ee.A0C(r1).AhA(r1, r3);
        AbstractC117135Yq r4 = (AbstractC117135Yq) obj2;
        if (A06.AhS().getClass().isInstance(r4)) {
            C80013rZ.A00(A06);
            AbstractC80173rp r12 = A06.A00;
            C72463ee.A0C(r12).AhA(r12, (AbstractC80173rp) ((AnonymousClass50T) r4));
            return A06.AhL();
        }
        throw C12970iu.A0f("mergeFrom(MessageLite) can only merge messages of the same type.");
    }
}
