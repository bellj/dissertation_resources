package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.text.TextUtils;
import android.util.Base64;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.0vV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20280vV {
    public final C16490p7 A00;
    public final AnonymousClass102 A01;
    public final AnonymousClass172 A02;

    public C20280vV(C16490p7 r1, AnonymousClass102 r2, AnonymousClass172 r3) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
    }

    public static AnonymousClass1Z6 A00(JSONObject jSONObject) {
        byte[] bArr;
        String str = null;
        String optString = jSONObject.optString("title");
        if (!TextUtils.isEmpty(optString)) {
            str = optString;
        }
        String str2 = null;
        String optString2 = jSONObject.optString("sub_title");
        if (!TextUtils.isEmpty(optString2)) {
            str2 = optString2;
        }
        String str3 = null;
        String optString3 = jSONObject.optString("header_thumbnail");
        if (!TextUtils.isEmpty(optString3)) {
            str3 = optString3;
        }
        if (!TextUtils.isEmpty(str3)) {
            bArr = Base64.decode(str3, 0);
        } else {
            bArr = null;
        }
        if (!TextUtils.isEmpty(str) || !TextUtils.isEmpty(str2) || bArr != null) {
            return new AnonymousClass1Z6(str, str2, bArr);
        }
        return null;
    }

    public static AnonymousClass1Z7 A01(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            AnonymousClass009.A05(str);
            JSONObject jSONObject = new JSONObject(str);
            ArrayList arrayList = new ArrayList();
            JSONArray optJSONArray = jSONObject.optJSONArray("buttons");
            if (optJSONArray != null) {
                for (int i = 0; i < optJSONArray.length(); i++) {
                    JSONObject jSONObject2 = optJSONArray.getJSONObject(i);
                    if (jSONObject2 != null) {
                        String optString = jSONObject2.optString("name");
                        String optString2 = jSONObject2.optString("params");
                        arrayList.add(new AnonymousClass1Z9(new AnonymousClass1Z8(optString, optString2), jSONObject2.optBoolean("selected")));
                    }
                }
            }
            return new AnonymousClass1Z7(arrayList);
        } catch (JSONException e) {
            Log.w("NativeFlowMessageConverter/parseJSON/deserialization error", e);
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:101:0x029c  */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x02a1 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:108:0x0248 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String A02(X.C16470p4 r13) {
        /*
        // Method dump skipped, instructions count: 675
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20280vV.A02(X.0p4):java.lang.String");
    }

    public int A03(C16470p4 r6) {
        if (r6 == null) {
            return 0;
        }
        int i = r6.A00;
        if (i == 1) {
            return 1;
        }
        if (i == 2) {
            return 3;
        }
        if (i == 3) {
            AnonymousClass1ZD r0 = r6.A01;
            if (r0 == null || r0.A04.A08.isEmpty()) {
                return 7;
            }
            return 4;
        } else if (i != 4) {
            return i != 5 ? 0 : 6;
        } else {
            return 5;
        }
    }

    public final ContentValues A04(AnonymousClass1XE r9) {
        String str;
        JSONObject jSONObject;
        ContentValues contentValues = new ContentValues();
        AnonymousClass1ZL r2 = r9.A00;
        if (r2 != null) {
            int i = r2.A04;
            int i2 = 1;
            if (i != 1) {
                i2 = 0;
                if (i == 2) {
                    i2 = 8;
                }
            }
            contentValues.put("element_type", Integer.valueOf(i2));
            String str2 = r2.A03;
            contentValues.put("reply_values", str2);
            if (i2 == 8) {
                JSONObject jSONObject2 = new JSONObject();
                try {
                    jSONObject2.put("description", str2);
                    jSONObject2.put("footer_text", r2.A01);
                    jSONObject2.put("response_message_type", r2.A04);
                    AnonymousClass1ZM r3 = r2.A00;
                    if (r3 == null) {
                        jSONObject = null;
                    } else {
                        jSONObject = new JSONObject();
                        jSONObject.put("native_flow_response_name", r3.A00);
                        jSONObject.put("native_flow_response_params_json", r3.A01);
                    }
                    jSONObject2.put("native_flow_response_content", jSONObject);
                } catch (JSONException e) {
                    Log.w("InteractiveResponseMessageConverter/toJSONObject/serialization error", e);
                    jSONObject2 = null;
                }
                if (jSONObject2 != null) {
                    str = jSONObject2.toString();
                } else {
                    str = null;
                }
            } else {
                str = r2.A01;
            }
            contentValues.put("reply_description", str);
        }
        return contentValues;
    }

    public final void A05(ContentValues contentValues, String str, int i, long j) {
        C16310on A02 = this.A00.A02();
        try {
            C16330op r4 = A02.A03;
            if (r4.A00(str, contentValues, "element_type = ? AND message_row_id = ?", new String[]{String.valueOf(i), String.valueOf(j)}) == 0) {
                r4.A02(contentValues, str);
            }
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final void A06(AbstractC15340mz r19, String str, long j) {
        AnonymousClass1Z8 r13;
        String str2;
        C16310on A01 = this.A00.get();
        try {
            Cursor A09 = A01.A03.A09(str, new String[]{String.valueOf(j)});
            if (A09 != null) {
                if (A09.moveToFirst()) {
                    int i = A09.getInt(A09.getColumnIndexOrThrow("element_type"));
                    String string = A09.getString(A09.getColumnIndexOrThrow("element_content"));
                    if (i == 2 && !TextUtils.isEmpty(string)) {
                        try {
                            AnonymousClass009.A05(string);
                            JSONObject jSONObject = new JSONObject(string);
                            ArrayList arrayList = new ArrayList();
                            JSONArray optJSONArray = jSONObject.optJSONArray("buttons");
                            if (optJSONArray != null) {
                                for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                                    JSONObject jSONObject2 = optJSONArray.getJSONObject(i2);
                                    JSONObject optJSONObject = jSONObject2.optJSONObject("native_flow_info");
                                    if (optJSONObject != null) {
                                        String string2 = optJSONObject.getString("name");
                                        JSONObject optJSONObject2 = optJSONObject.optJSONObject("params");
                                        if (optJSONObject2 == null) {
                                            str2 = null;
                                        } else {
                                            str2 = optJSONObject2.toString();
                                        }
                                        r13 = new AnonymousClass1Z8(string2, str2);
                                    } else {
                                        r13 = null;
                                    }
                                    arrayList.add(new AnonymousClass1ZN(r13, jSONObject2.optString("id"), jSONObject2.optString("displayText"), jSONObject2.optInt("button_type", 0), jSONObject2.optBoolean("selected")));
                                }
                            }
                            r19.A0h(new C30441Xk(jSONObject.optString("content"), jSONObject.optString("footer"), arrayList));
                        } catch (JSONException e) {
                            Log.w("ButtonsConverter/parseJSON/deserialization error", e);
                        }
                    }
                }
                A09.close();
            }
            A01.close();
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final void A07(C30441Xk r14, String str, long j) {
        String str2;
        if (r14 != null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("message_row_id", Long.valueOf(j));
            contentValues.put("element_type", (Integer) 2);
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("content", r14.A00);
                jSONObject.put("footer", r14.A01);
                JSONArray jSONArray = new JSONArray();
                for (AnonymousClass1ZN r2 : r14.A02) {
                    JSONObject jSONObject2 = new JSONObject();
                    jSONObject2.put("id", r2.A04);
                    String str3 = r2.A03;
                    if (str3 != null) {
                        jSONObject2.put("displayText", str3);
                    }
                    jSONObject2.put("selected", r2.A00);
                    jSONObject2.put("button_type", r2.A01);
                    AnonymousClass1Z8 r22 = r2.A02;
                    if (r22 != null) {
                        JSONObject jSONObject3 = new JSONObject();
                        jSONObject3.put("name", r22.A00);
                        String str4 = r22.A01;
                        if (str4 != null && str4.length() > 0) {
                            jSONObject3.put("params", new JSONObject(str4));
                        }
                        jSONObject2.put("native_flow_info", jSONObject3);
                    }
                    jSONArray.put(jSONObject2);
                }
                jSONObject.put("buttons", jSONArray);
                str2 = jSONObject.toString();
            } catch (JSONException e) {
                Log.w("ButtonsConverter/toJSONObject/serialization error", e);
                str2 = null;
            }
            if (!TextUtils.isEmpty(str2)) {
                contentValues.put("element_content", str2);
            }
            A05(contentValues, str, 2, j);
        }
    }

    public void A08(AnonymousClass1XD r8) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("element_type", (Integer) 2);
        contentValues.put("reply_values", r8.A0I());
        contentValues.put("reply_description", r8.A00);
        contentValues.put("message_row_id", Long.valueOf(r8.A11));
        A05(contentValues, "message_ui_elements_reply", 2, r8.A11);
    }

    public final void A09(AnonymousClass1XD r8, String str) {
        boolean z = false;
        if (r8.A11 > 0) {
            z = true;
        }
        StringBuilder sb = new StringBuilder("MessageUIElementsStore/fillReplyDataIfAvailable/message must have row_id set; key=");
        sb.append(r8.A0z);
        AnonymousClass009.A0B(sb.toString(), z);
        String[] strArr = {String.valueOf(r8.A11)};
        C16310on A01 = this.A00.get();
        try {
            Cursor A09 = A01.A03.A09(str, strArr);
            if (A09 != null) {
                if (A09.moveToLast()) {
                    String string = A09.getString(A09.getColumnIndexOrThrow("reply_values"));
                    String string2 = A09.getString(A09.getColumnIndexOrThrow("reply_description"));
                    r8.A0l(string);
                    r8.A00 = string2;
                }
                A09.close();
            }
            A01.close();
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final void A0A(AnonymousClass1XD r8, String str, long j) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("element_type", (Integer) 2);
        contentValues.put("reply_values", r8.A0I());
        contentValues.put("reply_description", r8.A00);
        contentValues.put("message_row_id", Long.valueOf(j));
        A05(contentValues, str, 2, j);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001e, code lost:
        if (r1 != 2) goto L_0x0020;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0B(X.AnonymousClass1XE r9) {
        /*
            r8 = this;
            r2 = r8
            android.content.ContentValues r3 = r8.A04(r9)
            long r0 = r9.A11
            java.lang.Long r1 = java.lang.Long.valueOf(r0)
            java.lang.String r0 = "message_row_id"
            r3.put(r0, r1)
            long r6 = r9.A11
            X.1ZL r0 = r9.A00
            if (r0 == 0) goto L_0x0020
            int r1 = r0.A04
            r5 = 1
            if (r1 == r5) goto L_0x0021
            r0 = 2
            r5 = 8
            if (r1 == r0) goto L_0x0021
        L_0x0020:
            r5 = 0
        L_0x0021:
            java.lang.String r4 = "message_ui_elements_reply"
            r2.A05(r3, r4, r5, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20280vV.A0B(X.1XE):void");
    }

    public final void A0C(AnonymousClass1XE r9, String str) {
        AnonymousClass1ZL r3;
        int i = 1;
        boolean z = false;
        if (r9.A11 > 0) {
            z = true;
        }
        StringBuilder sb = new StringBuilder("MessageUIElementsStore/fillReplyDataIfAvailable/message must have row_id set; key=");
        sb.append(r9.A0z);
        AnonymousClass009.A0B(sb.toString(), z);
        String[] strArr = {String.valueOf(r9.A11)};
        C16310on A01 = this.A00.get();
        try {
            Cursor A09 = A01.A03.A09(str, strArr);
            if (A09 != null) {
                if (A09.moveToLast()) {
                    int i2 = A09.getInt(A09.getColumnIndexOrThrow("element_type"));
                    if (i2 != 1) {
                        i = 0;
                        if (i2 == 8) {
                            i = 2;
                        }
                    }
                    String string = A09.getString(A09.getColumnIndexOrThrow("reply_description"));
                    if (1 == i || i == 0) {
                        r3 = new AnonymousClass1ZL(A09.getString(A09.getColumnIndexOrThrow("reply_values")), string, null, i);
                    } else {
                        AnonymousClass1ZM r7 = null;
                        if (!TextUtils.isEmpty(string)) {
                            try {
                                JSONObject jSONObject = new JSONObject(string);
                                if (2 == jSONObject.optInt("response_message_type")) {
                                    String optString = jSONObject.optString("description", "");
                                    JSONObject optJSONObject = jSONObject.optJSONObject("native_flow_response_content");
                                    if (optJSONObject != null) {
                                        r7 = new AnonymousClass1ZM(optJSONObject.optString("native_flow_response_name", ""), optJSONObject.optString("native_flow_response_params_json", ""));
                                    }
                                    r3 = new AnonymousClass1ZL(r7, optString);
                                }
                            } catch (JSONException e) {
                                Log.w("InteractiveResponseMessageConverter/parseJSON/deserialization error", e);
                            }
                        }
                    }
                    r9.A00 = r3;
                }
                A09.close();
            }
            A01.close();
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001b, code lost:
        if (r1 != 2) goto L_0x001d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0D(X.AnonymousClass1XE r9, java.lang.String r10, long r11) {
        /*
            r8 = this;
            r2 = r8
            android.content.ContentValues r3 = r8.A04(r9)
            r6 = r11
            java.lang.Long r1 = java.lang.Long.valueOf(r11)
            java.lang.String r0 = "message_row_id"
            r3.put(r0, r1)
            X.1ZL r0 = r9.A00
            if (r0 == 0) goto L_0x001d
            int r1 = r0.A04
            r5 = 1
            if (r1 == r5) goto L_0x001e
            r0 = 2
            r5 = 8
            if (r1 == r0) goto L_0x001e
        L_0x001d:
            r5 = 0
        L_0x001e:
            r4 = r10
            r2.A05(r3, r4, r5, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20280vV.A0D(X.1XE, java.lang.String, long):void");
    }

    public void A0E(AbstractC16390ow r9, long j) {
        if (r9.ABf() != null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("message_row_id", Long.valueOf(j));
            contentValues.put("element_type", Integer.valueOf(A03(r9.ABf())));
            String A02 = A02(r9.ABf());
            if (!TextUtils.isEmpty(A02)) {
                contentValues.put("element_content", A02);
            }
            A05(contentValues, "message_ui_elements", A03(r9.ABf()), j);
        }
    }

    public void A0F(AbstractC16390ow r10, long j) {
        if (r10.ABf() != null) {
            try {
                C16310on A02 = this.A00.A02();
                ContentValues contentValues = new ContentValues();
                contentValues.put("message_row_id", Long.valueOf(j));
                contentValues.put("element_type", Integer.valueOf(A03(r10.ABf())));
                String A022 = A02(r10.ABf());
                if (!TextUtils.isEmpty(A022)) {
                    contentValues.put("element_content", A022);
                }
                A05(contentValues, "message_quoted_ui_elements", A03(r10.ABf()), j);
                A02.close();
            } catch (SQLiteConstraintException e) {
                StringBuilder sb = new StringBuilder("MessageUIElementsStore/insertOrUpdateQuotedMultiElementMessage/fail to insert. Error quotedMessage is: ");
                sb.append(e);
                Log.e(sb.toString());
            }
        }
    }

    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    public final void A0G(X.AbstractC16390ow r30, java.lang.String r31, long r32) {
        /*
        // Method dump skipped, instructions count: 636
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20280vV.A0G(X.0ow, java.lang.String, long):void");
    }
}
