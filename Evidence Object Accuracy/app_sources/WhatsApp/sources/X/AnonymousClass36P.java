package X;

import android.content.Context;
import android.util.AttributeSet;
import com.whatsapp.R;
import com.whatsapp.usernotice.UserNoticeModalIconView;

/* renamed from: X.36P  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass36P extends AbstractC83773xs {
    public AnonymousClass37O A00;
    public AbstractC14440lR A01;

    public abstract int getTargetIconSize();

    public AnonymousClass36P(Context context) {
        super(context);
    }

    public AnonymousClass36P(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public AnonymousClass36P(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void A02(C43851xh r6) {
        setContentDescription(r6.A02);
        AnonymousClass37O r0 = this.A00;
        if (r0 != null) {
            r0.A03(true);
        }
        if (r6.A00(getContext()) == null) {
            A03(r6);
            return;
        }
        AnonymousClass37O r4 = new AnonymousClass37O(r6, this);
        this.A00 = r4;
        this.A01.Aaz(r4, r6.A00(getContext()));
    }

    public void A03(C43851xh r5) {
        int i;
        if (!(this instanceof UserNoticeModalIconView)) {
            if (!(r5 instanceof C43921xq)) {
                setColorFilter(getResources().getColor(R.color.user_notice_icon_tint));
                i = R.drawable.user_notice_banner_icon;
            } else {
                i = R.drawable.ga_banner;
            }
            setImageResource(i);
            return;
        }
        UserNoticeModalIconView userNoticeModalIconView = (UserNoticeModalIconView) this;
        userNoticeModalIconView.setBackground(AnonymousClass00T.A04(userNoticeModalIconView.getContext(), R.drawable.user_notice_modal_default_icon_background));
        userNoticeModalIconView.A00.setImageResource(R.drawable.user_notice_banner_icon);
        userNoticeModalIconView.A00.setColorFilter(userNoticeModalIconView.getResources().getColor(R.color.user_notice_icon_tint));
        userNoticeModalIconView.A00.setVisibility(0);
    }
}
