package X;

/* renamed from: X.03F  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass03F implements Runnable {
    public final /* synthetic */ AnonymousClass017 A00;

    public AnonymousClass03F(AnonymousClass017 r1) {
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        Object obj;
        AnonymousClass017 r3 = this.A00;
        synchronized (r3.A06) {
            obj = r3.A09;
            r3.A09 = AnonymousClass017.A0A;
        }
        r3.A0B(obj);
    }
}
