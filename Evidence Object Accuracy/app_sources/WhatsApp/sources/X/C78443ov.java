package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.3ov  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78443ov extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C98824jJ();
    public final int A00;
    public final String A01;
    public final HashMap A02;

    public C78443ov(String str, ArrayList arrayList, int i) {
        this.A00 = i;
        HashMap A11 = C12970iu.A11();
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            C78533p4 r7 = (C78533p4) arrayList.get(i2);
            String str2 = r7.A01;
            HashMap A112 = C12970iu.A11();
            ArrayList arrayList2 = r7.A02;
            C13020j0.A01(arrayList2);
            int size2 = arrayList2.size();
            for (int i3 = 0; i3 < size2; i3++) {
                C78543p5 r0 = (C78543p5) r7.A02.get(i3);
                A112.put(r0.A02, r0.A01);
            }
            A11.put(str2, A112);
        }
        this.A02 = A11;
        C13020j0.A01(str);
        this.A01 = str;
        Iterator A0L = C72463ee.A0L(A11);
        while (A0L.hasNext()) {
            Map map = (Map) A11.get(A0L.next());
            Iterator A10 = C72453ed.A10(map);
            while (A10.hasNext()) {
                ((C78633pE) map.get(A10.next())).A01 = this;
            }
        }
    }

    @Override // java.lang.Object
    public final String toString() {
        StringBuilder A0h = C12960it.A0h();
        HashMap hashMap = this.A02;
        Iterator A0L = C72463ee.A0L(hashMap);
        while (A0L.hasNext()) {
            String A0x = C12970iu.A0x(A0L);
            A0h.append(A0x);
            A0h.append(":\n");
            Map map = (Map) hashMap.get(A0x);
            Iterator A10 = C72453ed.A10(map);
            while (A10.hasNext()) {
                String A0x2 = C12970iu.A0x(A10);
                A0h.append("  ");
                C12990iw.A1T(A0h, A0x2);
                A0h.append(map.get(A0x2));
            }
        }
        return A0h.toString();
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 1, this.A00);
        ArrayList A0l = C12960it.A0l();
        HashMap hashMap = this.A02;
        Iterator A0L = C72463ee.A0L(hashMap);
        while (A0L.hasNext()) {
            String A0x = C12970iu.A0x(A0L);
            A0l.add(new C78533p4(A0x, (Map) hashMap.get(A0x)));
        }
        C95654e8.A0F(parcel, A0l, 2, false);
        C95654e8.A0D(parcel, this.A01, 3, false);
        C95654e8.A06(parcel, A00);
    }
}
