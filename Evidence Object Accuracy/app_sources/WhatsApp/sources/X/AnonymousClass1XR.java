package X;

/* renamed from: X.1XR  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1XR extends AnonymousClass1X3 implements AbstractC16400ox, AbstractC16420oz {
    public AnonymousClass1XR(C16150oX r9, AnonymousClass1IS r10, AnonymousClass1XR r11, long j) {
        super(r9, r10, (AbstractC16130oV) r11, (byte) 13, j, false);
    }

    public AnonymousClass1XR(C16150oX r9, AnonymousClass1IS r10, AnonymousClass1XR r11, long j, boolean z) {
        super(r9, r10, r11, r11.A0y, j, z);
    }

    public AnonymousClass1XR(AnonymousClass1IS r2, byte b, long j) {
        super(r2, (byte) 29, j);
    }

    public AnonymousClass1XR(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 13, j);
    }

    public AnonymousClass1XR(C40851sR r9, AnonymousClass1IS r10, long j, boolean z, boolean z2) {
        super(r9, r10, (byte) 13, j, z, z2);
        A1D(r9);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0125, code lost:
        if (r5 != null) goto L_0x00ea;
     */
    @Override // X.AbstractC16420oz
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A6k(X.C39971qq r16) {
        /*
        // Method dump skipped, instructions count: 330
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1XR.A6k(X.1qq):void");
    }

    @Override // X.AbstractC16400ox
    public /* bridge */ /* synthetic */ AbstractC15340mz A7M(AnonymousClass1IS r8) {
        if (!(this instanceof AnonymousClass1XQ)) {
            return new AnonymousClass1XR(((AbstractC16130oV) this).A02, r8, this, this.A0I, true);
        }
        AnonymousClass1XQ r3 = (AnonymousClass1XQ) this;
        return new AnonymousClass1XQ(((AbstractC16130oV) r3).A02, r8, r3, r3.A0I);
    }
}
