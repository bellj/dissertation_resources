package X;

import com.whatsapp.HomeActivity;

/* renamed from: X.0lu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC14700lu extends ActivityC13790kL {
    public boolean A00 = false;

    public AbstractActivityC14700lu() {
        A0R(new C49782Mo(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            HomeActivity homeActivity = (HomeActivity) this;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r3 = r2.A1E;
            ((ActivityC13830kP) homeActivity).A05 = (AbstractC14440lR) r3.ANe.get();
            ((ActivityC13810kN) homeActivity).A0C = (C14850m9) r3.A04.get();
            ((ActivityC13810kN) homeActivity).A05 = (C14900mE) r3.A8X.get();
            ((ActivityC13810kN) homeActivity).A03 = (AbstractC15710nm) r3.A4o.get();
            ((ActivityC13810kN) homeActivity).A04 = (C14330lG) r3.A7B.get();
            ((ActivityC13810kN) homeActivity).A0B = (AnonymousClass19M) r3.A6R.get();
            ((ActivityC13810kN) homeActivity).A0A = (C18470sV) r3.AK8.get();
            ((ActivityC13810kN) homeActivity).A06 = (C15450nH) r3.AII.get();
            ((ActivityC13810kN) homeActivity).A08 = (AnonymousClass01d) r3.ALI.get();
            ((ActivityC13810kN) homeActivity).A0D = (C18810t5) r3.AMu.get();
            ((ActivityC13810kN) homeActivity).A09 = (C14820m6) r3.AN3.get();
            ((ActivityC13810kN) homeActivity).A07 = (C18640sm) r3.A3u.get();
            ((ActivityC13790kL) homeActivity).A05 = (C14830m7) r3.ALb.get();
            ((ActivityC13790kL) homeActivity).A0D = (C252718t) r3.A9K.get();
            ((ActivityC13790kL) homeActivity).A01 = (C15570nT) r3.AAr.get();
            ((ActivityC13790kL) homeActivity).A04 = (C15810nw) r3.A73.get();
            ((ActivityC13790kL) homeActivity).A09 = r2.A06();
            ((ActivityC13790kL) homeActivity).A06 = (C14950mJ) r3.AKf.get();
            ((ActivityC13790kL) homeActivity).A00 = (AnonymousClass12P) r3.A0H.get();
            ((ActivityC13790kL) homeActivity).A02 = (C252818u) r3.AMy.get();
            ((ActivityC13790kL) homeActivity).A03 = (C22670zS) r3.A0V.get();
            ((ActivityC13790kL) homeActivity).A0A = (C21840y4) r3.ACr.get();
            ((ActivityC13790kL) homeActivity).A07 = (C15880o3) r3.ACF.get();
            ((ActivityC13790kL) homeActivity).A0C = (C21820y2) r3.AHx.get();
            ((ActivityC13790kL) homeActivity).A0B = (C15510nN) r3.AHZ.get();
            ((ActivityC13790kL) homeActivity).A08 = (C249317l) r3.A8B.get();
            homeActivity.A0S = (C21740xu) r3.AM1.get();
            homeActivity.A0p = (C16590pI) r3.AMg.get();
            homeActivity.A1P = (AnonymousClass1AU) r3.AJD.get();
            homeActivity.A0t = (C19990v2) r3.A3M.get();
            homeActivity.A0Q = (C20640w5) r3.AHk.get();
            homeActivity.A1T = (AnonymousClass19H) r3.AJN.get();
            homeActivity.A1W = (AnonymousClass12U) r3.AJd.get();
            homeActivity.A15 = (C16120oU) r3.ANE.get();
            homeActivity.A0s = (C20650w6) r3.A3A.get();
            homeActivity.A1e = (AnonymousClass19Z) r3.A2o.get();
            homeActivity.A1G = (C22660zR) r3.AAk.get();
            homeActivity.A0Z = (AnonymousClass1AV) r3.AJH.get();
            homeActivity.A0T = (C20670w8) r3.AMw.get();
            homeActivity.A1d = (C21260x8) r3.A2k.get();
            homeActivity.A0k = (C21270x9) r3.A4A.get();
            homeActivity.A10 = (C20040v7) r3.AAK.get();
            homeActivity.A1L = (AnonymousClass17R) r3.AIz.get();
            homeActivity.A1g = (C21280xA) r3.AMU.get();
            homeActivity.A0g = (C15550nR) r3.A45.get();
            homeActivity.A1b = (AnonymousClass1AW) r3.AAX.get();
            homeActivity.A17 = (C19890uq) r3.ABw.get();
            homeActivity.A0h = (C15610nY) r3.AMe.get();
            homeActivity.A0c = (AnonymousClass2GJ) r2.A0C.get();
            homeActivity.A1F = r2.A0J();
            homeActivity.A0z = (C15240mn) r3.A8F.get();
            homeActivity.A1E = (C14910mF) r3.ACs.get();
            homeActivity.A1H = (C18910tG) r3.AHz.get();
            homeActivity.A1D = (C17070qD) r3.AFC.get();
            homeActivity.A13 = r3.A3J();
            homeActivity.A1a = (C21300xC) r3.A0c.get();
            homeActivity.A0d = (C243915i) r3.A37.get();
            homeActivity.A0y = (C15650ng) r3.A4m.get();
            homeActivity.A0V = (C15820nx) r3.A6U.get();
            homeActivity.A16 = (C20710wC) r3.A8m.get();
            homeActivity.A11 = (AnonymousClass12H) r3.AC5.get();
            homeActivity.A1S = (AnonymousClass12F) r3.AJM.get();
            homeActivity.A14 = (C22050yP) r3.A7v.get();
            homeActivity.A1R = (C240514a) r3.AJL.get();
            homeActivity.A0o = (C17050qB) r3.ABL.get();
            homeActivity.A18 = (C14840m8) r3.ACi.get();
            homeActivity.A1B = (C22630zO) r3.AD7.get();
            homeActivity.A0U = (C18330sH) r3.AN4.get();
            homeActivity.A0i = (AnonymousClass10T) r3.A47.get();
            homeActivity.A1Z = (C21880y8) r3.AD5.get();
            homeActivity.A1K = (AnonymousClass1AY) r3.AIy.get();
            homeActivity.A0R = (AnonymousClass1FO) r3.AJf.get();
            homeActivity.A0W = (C251118d) r3.A2D.get();
            homeActivity.A19 = (C20220vP) r3.AC3.get();
            homeActivity.A12 = (C16490p7) r3.ACJ.get();
            homeActivity.A1O = (AnonymousClass1AZ) r3.AJ8.get();
            homeActivity.A0a = r3.A2X();
            homeActivity.A0r = (C15890o4) r3.AN1.get();
            homeActivity.A0e = (C22640zP) r3.A3Z.get();
            homeActivity.A0x = (C15680nj) r3.A4e.get();
            homeActivity.A1Q = (AnonymousClass1AM) r3.AJG.get();
            homeActivity.A1Y = (C14920mG) r3.ALr.get();
            homeActivity.A0w = (C21320xE) r3.A4Y.get();
            homeActivity.A1C = (C22710zW) r3.AF7.get();
            homeActivity.A0q = (C18360sK) r3.AN0.get();
            homeActivity.A1V = (C20780wJ) r3.AJT.get();
            homeActivity.A1A = (C20230vQ) r3.ACe.get();
            homeActivity.A0f = (C21220x4) r3.ACk.get();
            homeActivity.A1J = new C49512La((C14850m9) r3.A04.get(), (C16120oU) r3.ANE.get());
            homeActivity.A0X = (C16430p0) r3.A5y.get();
            homeActivity.A1f = (C237212t) r3.ALv.get();
            homeActivity.A1N = (C20810wM) r3.AJ5.get();
            homeActivity.A0m = (AnonymousClass19D) r3.ABo.get();
            homeActivity.A0l = (C17010q7) r3.A43.get();
            homeActivity.A0u = (C20830wO) r3.A4W.get();
            homeActivity.A0n = (AnonymousClass11P) r3.ABp.get();
            homeActivity.A1k = C18000rk.A00(r3.ADw);
            homeActivity.A1m = C18000rk.A00(r3.AID);
            homeActivity.A1h = C18000rk.A00(r3.A4v);
            homeActivity.A1l = C18000rk.A00(r3.AGl);
            homeActivity.A0Y = (C25601Aa) r3.A2E.get();
            C25611Ab r0 = (C25611Ab) r3.AJ0.get();
            if (r0 != null) {
                homeActivity.A1M = r0;
                homeActivity.A1U = (C88064Ed) r3.AJP.get();
                homeActivity.A1i = C18000rk.A00(r2.A0G);
                homeActivity.A1j = C18000rk.A00(r2.A15);
                return;
            }
            throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
        }
    }
}
