package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.1Zc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C30861Zc extends AbstractC28901Pl {
    public static final Parcelable.Creator CREATOR = new C99764kp();

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C30861Zc(C17930rd r1, int i, int i2, long j, long j2) {
        AnonymousClass009.A05(r1);
        this.A07 = r1;
        this.A05 = j;
        this.A06 = j2;
        this.A00 = i2;
        this.A01 = i;
    }

    public /* synthetic */ C30861Zc(Parcel parcel) {
        A09(parcel);
    }

    @Override // X.AbstractC28901Pl, java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("[ BANK:");
        sb.append(super.toString());
        sb.append(" ]");
        return sb.toString();
    }
}
