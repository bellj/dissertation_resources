package X;

import android.graphics.Typeface;
import android.view.accessibility.CaptioningManager;

/* renamed from: X.4dQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C95274dQ {
    public static final C95274dQ A06 = new C95274dQ(null, -1, -16777216, 0, 0, -1);
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final int A04;
    public final Typeface A05;

    public C95274dQ(Typeface typeface, int i, int i2, int i3, int i4, int i5) {
        this.A03 = i;
        this.A00 = i2;
        this.A04 = i3;
        this.A02 = i4;
        this.A01 = i5;
        this.A05 = typeface;
    }

    public static C95274dQ A00(CaptioningManager.CaptionStyle captionStyle) {
        if (AnonymousClass3JZ.A01 >= 21) {
            return A01(captionStyle);
        }
        return new C95274dQ(captionStyle.getTypeface(), captionStyle.foregroundColor, captionStyle.backgroundColor, 0, captionStyle.edgeType, captionStyle.edgeColor);
    }

    public static C95274dQ A01(CaptioningManager.CaptionStyle captionStyle) {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        if (captionStyle.hasForegroundColor()) {
            i = captionStyle.foregroundColor;
        } else {
            i = -1;
        }
        if (captionStyle.hasBackgroundColor()) {
            i2 = captionStyle.backgroundColor;
        } else {
            i2 = -16777216;
        }
        if (captionStyle.hasWindowColor()) {
            i3 = captionStyle.windowColor;
        } else {
            i3 = 0;
        }
        if (captionStyle.hasEdgeType()) {
            i4 = captionStyle.edgeType;
        } else {
            i4 = 0;
        }
        if (captionStyle.hasEdgeColor()) {
            i5 = captionStyle.edgeColor;
        } else {
            i5 = -1;
        }
        return new C95274dQ(captionStyle.getTypeface(), i, i2, i3, i4, i5);
    }
}
