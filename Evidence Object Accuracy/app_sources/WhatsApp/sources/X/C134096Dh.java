package X;

import android.view.View;
import android.widget.TextView;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.whatsapp.R;
import com.whatsapp.WaImageView;

/* renamed from: X.6Dh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C134096Dh implements AnonymousClass5Wu {
    public View A00;
    public View A01;
    public TextView A02;
    public TextView A03;
    public ShimmerFrameLayout A04;
    public WaImageView A05;

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0058  */
    /* renamed from: A00 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A6Q(X.AnonymousClass4OZ r5) {
        /*
            r4 = this;
            if (r5 == 0) goto L_0x0012
            int r1 = r5.A00
            r0 = -2
            if (r1 == r0) goto L_0x001d
            r0 = -1
            if (r1 == r0) goto L_0x001d
            if (r1 == 0) goto L_0x0069
            r0 = 1
            if (r1 == r0) goto L_0x0013
            r0 = 2
            if (r1 == r0) goto L_0x001d
        L_0x0012:
            return
        L_0x0013:
            java.lang.Object r2 = r5.A01
            if (r2 == 0) goto L_0x0012
            com.facebook.shimmer.ShimmerFrameLayout r0 = r4.A04
            r0.A01()
            goto L_0x0026
        L_0x001d:
            java.lang.Object r2 = r5.A01
            if (r2 == 0) goto L_0x0012
            com.facebook.shimmer.ShimmerFrameLayout r0 = r4.A04
            r0.A00()
        L_0x0026:
            X.5vZ r2 = (X.C128105vZ) r2
            com.whatsapp.WaImageView r0 = r4.A05
            android.content.Context r3 = r0.getContext()
            int r0 = r2.A02
            android.graphics.drawable.Drawable r1 = X.AnonymousClass00T.A04(r3, r0)
            if (r1 == 0) goto L_0x0041
            int r0 = r2.A01
            if (r0 == 0) goto L_0x0041
            int r0 = X.AnonymousClass00T.A00(r3, r0)
            X.C015607k.A0A(r1, r0)
        L_0x0041:
            com.whatsapp.WaImageView r0 = r4.A05
            r0.setImageDrawable(r1)
            android.widget.TextView r1 = r4.A03
            java.lang.String r0 = r2.A06
            r1.setText(r0)
            android.widget.TextView r1 = r4.A02
            java.lang.CharSequence r0 = r2.A05
            r1.setText(r0)
            android.graphics.Typeface r1 = r2.A04
            if (r1 == 0) goto L_0x005d
            android.widget.TextView r0 = r4.A02
            r0.setTypeface(r1)
        L_0x005d:
            android.widget.TextView r1 = r4.A02
            int r0 = r2.A03
            X.C12960it.A0s(r3, r1, r0)
            android.view.View r1 = r4.A00
            int r0 = r2.A00
            goto L_0x006d
        L_0x0069:
            android.view.View r1 = r4.A01
            r0 = 8
        L_0x006d:
            r1.setVisibility(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C134096Dh.A6Q(X.4OZ):void");
    }

    @Override // X.AnonymousClass5Wu
    public int ADq() {
        return R.layout.payment_service_container;
    }

    @Override // X.AnonymousClass5Wu
    public void AYL(View view) {
        this.A01 = view;
        this.A03 = C12960it.A0I(view, R.id.payment_service_title);
        this.A02 = C12960it.A0I(view, R.id.payment_service_subtitle);
        this.A05 = C12980iv.A0X(view, R.id.payment_service_icon);
        this.A00 = AnonymousClass028.A0D(view, R.id.expand_services_button);
        this.A04 = (ShimmerFrameLayout) AnonymousClass028.A0D(view, R.id.payment_service_subtitle_shimmer);
    }
}
