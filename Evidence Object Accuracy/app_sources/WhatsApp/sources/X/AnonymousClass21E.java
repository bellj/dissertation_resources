package X;

import java.util.Arrays;

/* renamed from: X.21E  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass21E implements Comparable {
    public final Class A00;
    public final Object A01;

    public AnonymousClass21E(Class cls, Object obj) {
        if (cls != Boolean.class && cls != String.class && cls != Integer.class && cls != Float.class) {
            throw new IllegalArgumentException("Invalid type: must be one of {Boolean, Integer, Float, String}");
        } else if (cls.isInstance(obj)) {
            this.A01 = obj;
            this.A00 = cls;
        } else {
            throw new IllegalArgumentException("Mismatched args: value is not an instance of type");
        }
    }

    @Override // java.lang.Comparable
    public int compareTo(Object obj) {
        if (obj instanceof AnonymousClass21E) {
            AnonymousClass21E r4 = (AnonymousClass21E) obj;
            Object obj2 = r4.A01;
            Class cls = r4.A00;
            Class cls2 = this.A00;
            if (!cls2.equals(cls)) {
                throw new IllegalArgumentException("compareTo objects have mismatched types");
            } else if (cls2 == Boolean.class) {
                throw new IllegalArgumentException("compareTo should not be called on boolean types");
            } else if (cls2 == String.class) {
                return ((String) this.A01).compareTo((String) obj2);
            } else {
                if (cls2 == Integer.class) {
                    return AnonymousClass048.A00(((Number) this.A01).intValue(), ((Number) obj2).intValue());
                }
                if (cls2 == Float.class) {
                    return Float.compare(((Number) this.A01).floatValue(), ((Number) obj2).floatValue());
                }
                throw new IllegalStateException("Invalid type: must be one of {Boolean, Integer, Float, String}");
            }
        } else {
            throw new IllegalArgumentException("compareTo o should be an instance of ConfigPrimitive");
        }
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (!(obj instanceof AnonymousClass21E)) {
            return false;
        }
        AnonymousClass21E r5 = (AnonymousClass21E) obj;
        Object obj2 = r5.A01;
        if (!this.A00.equals(r5.A00) || !this.A01.equals(obj2)) {
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A01, this.A00});
    }

    @Override // java.lang.Object
    public String toString() {
        return this.A01.toString();
    }
}
