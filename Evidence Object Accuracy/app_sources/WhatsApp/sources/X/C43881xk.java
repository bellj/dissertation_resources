package X;

import java.util.Arrays;

/* renamed from: X.1xk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C43881xk {
    public final C43861xi A00;
    public final C43871xj A01;
    public final C43871xj A02;

    public C43881xk(C43861xi r1, C43871xj r2, C43871xj r3) {
        this.A02 = r2;
        this.A00 = r1;
        this.A01 = r3;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C43881xk r5 = (C43881xk) obj;
            C43871xj r1 = this.A02;
            C43871xj r0 = r5.A02;
            if (r1 != r0 && (r1 == null || !r1.equals(r0))) {
                return false;
            }
            C43861xi r12 = this.A00;
            C43861xi r02 = r5.A00;
            if (r12 != r02 && (r12 == null || !r12.equals(r02))) {
                return false;
            }
            C43871xj r13 = this.A01;
            C43871xj r03 = r5.A01;
            if (r13 != r03 && (r13 == null || !r13.equals(r03))) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A02, this.A00, this.A01});
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("UserNoticeContentTiming{start=");
        sb.append(this.A02);
        sb.append(", duration=");
        sb.append(this.A00);
        sb.append(", end=");
        sb.append(this.A01);
        sb.append('}');
        return sb.toString();
    }
}
