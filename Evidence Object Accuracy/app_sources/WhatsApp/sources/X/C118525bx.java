package X;

import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.SpinnerAdapter;
import com.whatsapp.R;
import com.whatsapp.payments.ui.stepup.NoviTextInputQuestionRow;
import java.util.List;

/* renamed from: X.5bx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118525bx extends AnonymousClass02M {
    public List A00 = C12960it.A0l();

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A00.size();
    }

    @Override // X.AnonymousClass02M
    public void ANH(AnonymousClass03U r9, int i) {
        NoviTextInputQuestionRow noviTextInputQuestionRow = ((C118745cJ) r9).A00;
        noviTextInputQuestionRow.A02.setVisibility(8);
        noviTextInputQuestionRow.A01.setVisibility(8);
        C127455uW r4 = (C127455uW) this.A00.get(i);
        noviTextInputQuestionRow.A00 = i;
        C127995vO r7 = r4.A00;
        String str = r7.A05;
        if ("TEXT".equals(str)) {
            noviTextInputQuestionRow.A02.setVisibility(0);
            noviTextInputQuestionRow.A07.setText(r7.A04);
            noviTextInputQuestionRow.A04.setHint(r7.A02);
            C129445xj r2 = r4.A01;
            noviTextInputQuestionRow.A05.addTextChangedListener(new C1317563z(noviTextInputQuestionRow, r4, r2));
            noviTextInputQuestionRow.A05.setOnFocusChangeListener(new View.OnFocusChangeListener(r2) { // from class: X.64l
                public final /* synthetic */ C129445xj A01;

                {
                    this.A01 = r2;
                }

                @Override // android.view.View.OnFocusChangeListener
                public final void onFocusChange(View view, boolean z) {
                    NoviTextInputQuestionRow noviTextInputQuestionRow2 = NoviTextInputQuestionRow.this;
                    C129445xj r22 = this.A01;
                    if (z) {
                        r22.A01("INPUT_BOX", noviTextInputQuestionRow2.A00);
                    }
                }
            });
        } else if ("DROP_DOWN".equals(str)) {
            noviTextInputQuestionRow.A01.setVisibility(0);
            noviTextInputQuestionRow.A06.setText(r7.A04);
            noviTextInputQuestionRow.A08.setAdapter((SpinnerAdapter) new C117575aD(noviTextInputQuestionRow.getContext(), r7.A00));
            String str2 = r7.A02;
            noviTextInputQuestionRow.A03.setHint(str2);
            noviTextInputQuestionRow.A08.setPrompt(str2);
            C129445xj r3 = r4.A01;
            EditText editText = noviTextInputQuestionRow.A03.A0L;
            AnonymousClass009.A03(editText);
            C117295Zj.A0o(editText, noviTextInputQuestionRow, r3, 31);
            noviTextInputQuestionRow.A08.setSelection(Integer.MIN_VALUE, false);
            noviTextInputQuestionRow.A08.setOnItemSelectedListener(new AnonymousClass65F(editText, noviTextInputQuestionRow, r4, r3));
        }
    }

    @Override // X.AnonymousClass02M
    public AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        return new C118745cJ(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.novi_text_input_question_row_container));
    }
}
