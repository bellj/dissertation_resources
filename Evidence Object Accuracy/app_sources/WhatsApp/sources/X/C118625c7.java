package X;

import android.view.ViewGroup;
import com.whatsapp.R;
import java.util.List;

/* renamed from: X.5c7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118625c7 extends AnonymousClass02M {
    public final C15570nT A00;
    public final C247116o A01;
    public final C37071lG A02;
    public final C21270x9 A03;
    public final AnonymousClass018 A04;
    public final C14850m9 A05;
    public final AnonymousClass14X A06;
    public final AnonymousClass19O A07;
    public final List A08 = C12960it.A0l();

    public C118625c7(C15570nT r2, C247116o r3, C37071lG r4, C21270x9 r5, AnonymousClass018 r6, C14850m9 r7, AnonymousClass14X r8, AnonymousClass19O r9) {
        this.A05 = r7;
        this.A06 = r8;
        this.A03 = r5;
        this.A02 = r4;
        this.A04 = r6;
        this.A07 = r9;
        this.A00 = r2;
        this.A01 = r3;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A08.size();
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r2, int i) {
        ((AbstractC118815cQ) r2).A08((C125965s6) this.A08.get(i));
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        switch (i) {
            case 0:
                return new C122155l0(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_checkout_order_details_header), this.A03);
            case 1:
                return new C122135ky(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_checkout_order_details_status));
            case 2:
                return new C122175l2(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_checkout_order_details_item_view), this.A02, this.A04, this.A07);
            case 3:
                return new C122185l3(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_checkout_order_details_charges));
            case 4:
                return new C122115kw(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_checkout_order_details_saving));
            case 5:
                return new C122165l1(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_checkout_order_details_links));
            case 6:
                return new C122145kz(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_checkout_order_details_payment_options), this.A01);
            case 7:
                return new C122125kx(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_checkout_order_details_zigzag_edge_view));
            case 8:
                return new C122085kt(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_checkout_order_details_header_shimmer));
            case 9:
                return new C122105kv(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_checkout_order_details_status_shimmer));
            case 10:
                return new C122095ku(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_checkout_order_details_item_view_shimmer));
            case 11:
                return new C122075ks(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_checkout_order_details_charges_shimmer));
            default:
                throw C12960it.A0U("PaymentCheckoutOrderDetailsAdapter/onCreateViewHolder/unhandled view type");
        }
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        return ((C125965s6) this.A08.get(i)).A00;
    }
}
