package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.686  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass686 implements AnonymousClass5YY {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(7);
    public long A00;
    public long A01;
    public String A02;
    public boolean A03;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass686(AnonymousClass1V8 r9, String str) {
        if (str != null) {
            this.A03 = C28421Nd.A00(str, 0) == 1;
        }
        if (r9 != null) {
            String A0I = r9.A0I("created-ts", null);
            if (A0I != null) {
                this.A00 = C28421Nd.A01(A0I, 0) * 1000;
            }
            String A0I2 = r9.A0I("updated-ts", null);
            if (A0I2 != null) {
                this.A01 = C28421Nd.A01(A0I2, 0) * 1000;
            }
            String A0I3 = r9.A0I("complaint-status", null);
            if (!TextUtils.isEmpty(A0I3)) {
                this.A02 = A0I3;
            }
        }
    }

    public AnonymousClass686(Parcel parcel) {
        this.A03 = C12960it.A1S(parcel.readByte());
        this.A00 = parcel.readLong();
        this.A01 = parcel.readLong();
        this.A02 = parcel.readString();
    }

    public AnonymousClass686(String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                JSONObject A05 = C13000ix.A05(str);
                this.A03 = A05.optBoolean("is-complaint-eligible", false);
                this.A00 = A05.optLong("created-ts");
                this.A01 = A05.optLong("updated-ts");
                this.A02 = A05.optString("complaint-status");
            } catch (JSONException e) {
                Log.w("PAY: IndiaUpiTransactionComplaintData threw: ", e);
            }
        }
    }

    public String A00() {
        JSONObject A0a = C117295Zj.A0a();
        try {
            A0a.put("is-complaint-eligible", this.A03);
            long j = this.A00;
            if (j > 0) {
                A0a.put("created-ts", j);
            }
            long j2 = this.A01;
            if (j2 > 0) {
                A0a.put("updated-ts", j2);
            }
            String str = this.A02;
            if (str != null) {
                A0a.put("complaint-status", str);
            }
        } catch (JSONException e) {
            Log.w("PAY: IndiaUpiTransactionComplaintData toJson threw: ", e);
        }
        return A0a.toString();
    }

    @Override // X.AnonymousClass5YY
    public String ABW() {
        return this.A02;
    }

    @Override // X.AnonymousClass5YY
    public long AHR() {
        return this.A01;
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("IndiaUpiTransactionComplaintData{isComplaintEligible='");
        A0k.append(this.A03);
        A0k.append('\'');
        A0k.append(", createdTs='");
        A0k.append(this.A00);
        A0k.append('\'');
        A0k.append(", updatedTs='");
        A0k.append(this.A01);
        A0k.append('\'');
        A0k.append(", complaintStatus='");
        A0k.append(this.A02);
        A0k.append('\'');
        return C12970iu.A0v(A0k);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte(this.A03 ? (byte) 1 : 0);
        parcel.writeLong(this.A00);
        parcel.writeLong(this.A01);
        parcel.writeString(this.A02);
    }
}
