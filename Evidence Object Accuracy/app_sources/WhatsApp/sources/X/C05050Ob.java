package X;

/* renamed from: X.0Ob  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05050Ob {
    public double A00;
    public double A01;
    public double A02;
    public double A03;

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append("(");
        sb.append(this.A01);
        sb.append(", ");
        sb.append(this.A03);
        sb.append(", ");
        sb.append(this.A02);
        sb.append(", ");
        sb.append(this.A00);
        sb.append(")");
        return sb.toString();
    }
}
