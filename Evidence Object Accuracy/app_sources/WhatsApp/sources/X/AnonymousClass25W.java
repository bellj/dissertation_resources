package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.25W  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass25W extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass25W A06;
    public static volatile AnonymousClass255 A07;
    public int A00;
    public int A01;
    public AnonymousClass1K6 A02 = AnonymousClass277.A01;
    public String A03 = "";
    public String A04 = "";
    public boolean A05;

    static {
        AnonymousClass25W r0 = new AnonymousClass25W();
        A06 = r0;
        r0.A0W();
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i;
        int i2 = ((AbstractC27091Fz) this).A00;
        if (i2 != -1) {
            return i2;
        }
        if ((this.A00 & 1) == 1) {
            i = CodedOutputStream.A07(1, this.A04) + 0;
        } else {
            i = 0;
        }
        if ((this.A00 & 2) == 2) {
            i += CodedOutputStream.A07(2, this.A03);
        }
        int i3 = 0;
        for (int i4 = 0; i4 < this.A02.size(); i4++) {
            i3 += CodedOutputStream.A0B((String) this.A02.get(i4));
        }
        int size = i + i3 + this.A02.size();
        int i5 = this.A00;
        if ((i5 & 4) == 4) {
            size += CodedOutputStream.A03(4, this.A01);
        }
        if ((i5 & 8) == 8) {
            size += CodedOutputStream.A00(5);
        }
        int A00 = size + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A04);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0I(2, this.A03);
        }
        for (int i = 0; i < this.A02.size(); i++) {
            codedOutputStream.A0I(3, (String) this.A02.get(i));
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0E(4, this.A01);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0J(5, this.A05);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
