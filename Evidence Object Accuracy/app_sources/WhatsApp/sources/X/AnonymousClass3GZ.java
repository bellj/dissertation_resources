package X;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* renamed from: X.3GZ  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3GZ {
    public static final ArrayList A01;
    public static final ArrayList A02;
    public static final ArrayList A03;
    public static final ArrayList A04;
    public final AnonymousClass1V8 A00;

    static {
        String[] strArr = new String[3];
        strArr[0] = "FBPAY";
        strArr[1] = "NOVI";
        A03 = C12960it.A0m("UPI", strArr, 2);
        String[] strArr2 = new String[2];
        strArr2[0] = "1";
        A04 = C12960it.A0m("2", strArr2, 1);
        String[] strArr3 = new String[2];
        strArr3[0] = "0";
        A01 = C12960it.A0m("1", strArr3, 1);
        String[] strArr4 = new String[2];
        strArr4[0] = "0";
        A02 = C12960it.A0m("1", strArr4, 1);
    }

    public AnonymousClass3GZ(AnonymousClass3BL r10, Long l) {
        C41141sy r4 = new C41141sy("iq");
        C41141sy r2 = new C41141sy("accept_pay");
        if (l != null && AnonymousClass3JT.A0D(l, 0, true)) {
            r2.A04(new AnonymousClass1W9("tos-version", l.longValue()));
        }
        r2.A0A("NOVI", "service", A03);
        r2.A0A("2", "version", A04);
        r2.A0A("1", "consumer", A01);
        r4.A05(r2.A03());
        AnonymousClass1V8 r1 = r10.A00;
        r4.A07(r1, C12960it.A0l());
        List asList = Arrays.asList(new String[0]);
        r4.A09(r1, asList, C12960it.A0l());
        ArrayList A0x = C12980iv.A0x(Arrays.asList(new String[0]));
        A0x.addAll(0, asList);
        AnonymousClass3CT r0 = r10.A01;
        if (r0 != null) {
            r0.A00(r4, A0x);
        }
        this.A00 = r4.A03();
    }
}
