package X;

import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;

/* renamed from: X.03E  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass03E implements Iterable {
    public int A00 = 0;
    public AnonymousClass05U A01;
    public AnonymousClass05U A02;
    public WeakHashMap A03 = new WeakHashMap();

    public AnonymousClass05U A00(Object obj) {
        AnonymousClass05U r1 = this.A02;
        while (r1 != null && !r1.A02.equals(obj)) {
            r1 = r1.A00;
        }
        return r1;
    }

    public Object A01(Object obj) {
        AnonymousClass05U A00 = A00(obj);
        if (A00 == null) {
            return null;
        }
        this.A00--;
        WeakHashMap weakHashMap = this.A03;
        if (!weakHashMap.isEmpty()) {
            for (AnonymousClass076 r0 : weakHashMap.keySet()) {
                r0.Aea(A00);
            }
        }
        AnonymousClass05U r1 = A00.A01;
        AnonymousClass05U r02 = A00.A00;
        if (r1 != null) {
            r1.A00 = r02;
        } else {
            this.A02 = r02;
        }
        AnonymousClass05U r03 = A00.A00;
        if (r03 != null) {
            r03.A01 = r1;
        } else {
            this.A01 = r1;
        }
        A00.A00 = null;
        A00.A01 = null;
        return A00.A03;
    }

    public Object A02(Object obj, Object obj2) {
        AnonymousClass05U A00 = A00(obj);
        if (A00 != null) {
            return A00.A03;
        }
        AnonymousClass05U r1 = new AnonymousClass05U(obj, obj2);
        this.A00++;
        AnonymousClass05U r0 = this.A01;
        if (r0 == null) {
            this.A02 = r1;
        } else {
            r0.A00 = r1;
            r1.A01 = r0;
        }
        this.A01 = r1;
        return null;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof AnonymousClass03E) {
            AnonymousClass03E r7 = (AnonymousClass03E) obj;
            if (this.A00 == r7.A00) {
                Iterator it = iterator();
                Iterator it2 = r7.iterator();
                while (it.hasNext() && it2.hasNext()) {
                    Map.Entry entry = (Map.Entry) it.next();
                    Object next = it2.next();
                    if (entry == null) {
                        if (next == null) {
                        }
                    } else if (!entry.equals(next)) {
                        return false;
                    }
                }
                if (it.hasNext() || it2.hasNext()) {
                    return false;
                }
                return true;
            }
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        Iterator it = iterator();
        int i = 0;
        while (it.hasNext()) {
            i += ((Map.Entry) it.next()).hashCode();
        }
        return i;
    }

    @Override // java.lang.Iterable
    public Iterator iterator() {
        C02420Cf r2 = new C02420Cf(this.A02, this.A01);
        this.A03.put(r2, Boolean.FALSE);
        return r2;
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        Iterator it = iterator();
        while (it.hasNext()) {
            sb.append(it.next().toString());
            if (it.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }
}
