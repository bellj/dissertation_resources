package X;

import android.content.Context;
import android.os.Handler;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;

/* renamed from: X.0Y4  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Y4 implements AbstractC12360hn {
    public static final int A0L = ViewConfiguration.getDoubleTapTimeout();
    public static final int A0M = ViewConfiguration.getTapTimeout();
    public float A00;
    public float A01;
    public float A02;
    public float A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public GestureDetector.OnDoubleTapListener A08;
    public MotionEvent A09;
    public MotionEvent A0A;
    public VelocityTracker A0B;
    public boolean A0C;
    public boolean A0D;
    public boolean A0E;
    public boolean A0F;
    public boolean A0G;
    public boolean A0H;
    public boolean A0I;
    public final Handler A0J = new AnonymousClass0AQ(this);
    public final GestureDetector.OnGestureListener A0K;

    public AnonymousClass0Y4(Context context, GestureDetector.OnGestureListener onGestureListener) {
        this.A0K = onGestureListener;
        if (onGestureListener instanceof GestureDetector.OnDoubleTapListener) {
            this.A08 = (GestureDetector.OnDoubleTapListener) onGestureListener;
        }
        if (context == null) {
            throw new IllegalArgumentException("Context must not be null");
        } else if (onGestureListener != null) {
            this.A0H = true;
            ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
            int scaledTouchSlop = viewConfiguration.getScaledTouchSlop();
            int scaledDoubleTapSlop = viewConfiguration.getScaledDoubleTapSlop();
            this.A06 = viewConfiguration.getScaledMinimumFlingVelocity();
            this.A05 = viewConfiguration.getScaledMaximumFlingVelocity();
            this.A07 = scaledTouchSlop * scaledTouchSlop;
            this.A04 = scaledDoubleTapSlop * scaledDoubleTapSlop;
        } else {
            throw new IllegalArgumentException("OnGestureListener must not be null");
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:107:0x0227  */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x023e  */
    @Override // X.AbstractC12360hn
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean AXZ(android.view.MotionEvent r15) {
        /*
        // Method dump skipped, instructions count: 630
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0Y4.AXZ(android.view.MotionEvent):boolean");
    }

    @Override // X.AbstractC12360hn
    public void AcG(boolean z) {
        this.A0H = z;
    }
}
