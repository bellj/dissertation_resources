package X;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.LinkedList;

/* renamed from: X.1aO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C31341aO {
    public final C31331aN A00;

    public C31341aO(C31331aN r1) {
        this.A00 = r1;
    }

    public C31361aQ A00(C31351aP r14) {
        C31361aQ r0;
        synchronized (C93434aA.A02) {
            try {
                C31331aN r3 = this.A00;
                C29311Rr A00 = r3.A00(r14);
                LinkedList linkedList = A00.A00;
                if (linkedList.isEmpty()) {
                    try {
                        int nextInt = SecureRandom.getInstance("SHA1PRNG").nextInt(Integer.MAX_VALUE);
                        try {
                            byte[] bArr = new byte[32];
                            SecureRandom.getInstance("SHA1PRNG").nextBytes(bArr);
                            C31731b1 A01 = C31481ac.A01();
                            linkedList.clear();
                            linkedList.add(new C32091bb(A01.A01, new C31691ax(A01.A00), bArr, nextInt, 0));
                            r3.A01(r14, A00);
                        } catch (NoSuchAlgorithmException e) {
                            throw new AssertionError(e);
                        }
                    } catch (NoSuchAlgorithmException e2) {
                        throw new AssertionError(e2);
                    }
                }
                if (!linkedList.isEmpty()) {
                    C32091bb r1 = (C32091bb) linkedList.get(0);
                    int i = r1.A00.A01;
                    int i2 = r1.A00().A00;
                    byte[] bArr2 = r1.A00().A01;
                    C31471ab r02 = r1.A00.A04;
                    if (r02 == null) {
                        r02 = C31471ab.A03;
                    }
                    r0 = new C31361aQ(C31481ac.A00(r02.A02.A04()), bArr2, i, i2);
                } else {
                    throw new C31541ai("No key state in record!");
                }
            } catch (C31541ai | C31561ak e3) {
                throw new AssertionError(e3);
            }
        }
        return r0;
    }
}
