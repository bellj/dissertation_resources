package X;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

/* renamed from: X.3BK  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3BK {
    public final Collection A00;
    public final Collection A01;

    public AnonymousClass3BK(String... strArr) {
        Collection emptySet;
        int length = strArr.length;
        if (length == 0) {
            emptySet = Collections.emptySet();
        } else if (length == 1) {
            emptySet = Collections.singleton(strArr[0]);
        } else if (length == 2 || length == 3 || length == 4 || length == 5) {
            emptySet = Arrays.asList(strArr);
        } else {
            emptySet = new HashSet(Arrays.asList(strArr));
        }
        this.A00 = emptySet;
        this.A01 = C12970iu.A12();
        for (String str : strArr) {
            Collection collection = this.A01;
            Object[] objArr = (Object[]) AnonymousClass3B8.A02.A00.get(str);
            AnonymousClass009.A05(objArr);
            Collections.addAll(collection, objArr);
        }
    }
}
