package X;

import android.location.Location;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.facebook.redex.RunnableBRunnable0Shape10S0200000_I1;
import com.google.android.gms.common.api.internal.BasePendingResult;

/* renamed from: X.3fy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractBinderC73283fy extends Binder implements IInterface {
    @Override // android.os.IInterface
    public final IBinder asBinder() {
        return this;
    }

    public AbstractBinderC73283fy(String str) {
        attachInterface(this, str);
    }

    @Override // android.os.Binder
    public final boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        if (i <= 16777215) {
            C72463ee.A0Q(this, parcel);
        } else if (super.onTransact(i, parcel, parcel2, i2)) {
            return true;
        }
        if (!(this instanceof BinderC79753r9)) {
            BinderC79763rA r5 = (BinderC79763rA) this;
            if (i != 1) {
                return false;
            }
            Location location = (Location) C12970iu.A0F(parcel, Location.CREATOR);
            synchronized (r5) {
                AnonymousClass4PH r4 = r5.A00;
                r4.A00.execute(new RunnableBRunnable0Shape10S0200000_I1(r4, 12, new C108254yl(location)));
            }
            return true;
        }
        BinderC79753r9 r52 = (BinderC79753r9) this;
        if (i == 1) {
            ((BasePendingResult) r52.A00).A05(((C78663pH) C12970iu.A0F(parcel, C78663pH.CREATOR)).A00);
            return true;
        } else if (i != 2) {
            return false;
        } else {
            return true;
        }
    }
}
