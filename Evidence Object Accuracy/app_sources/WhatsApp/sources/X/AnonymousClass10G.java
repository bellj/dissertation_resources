package X;

import android.util.Pair;
import com.whatsapp.util.Log;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/* renamed from: X.10G  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass10G {
    public C22760zb A00;

    public File A00(File file, boolean z, boolean z2, boolean z3) {
        ArrayList arrayList;
        File file2;
        C22760zb r3 = this.A00;
        if (z) {
            arrayList = Log.getLatestLogs(3);
        } else {
            arrayList = new ArrayList();
        }
        if (file != null) {
            arrayList.add(file);
        }
        if (z2) {
            File[] A00 = AnonymousClass127.A00(r3.A0F);
            if (A00.length > 0) {
                Collections.addAll(arrayList, A00);
            } else {
                Log.w("debug-builder/getZippedInfoFiles no ANR traces to send");
            }
        }
        if (arrayList.size() != 0) {
            if (z3) {
                file2 = r3.A04.A0L("logs");
            } else {
                file2 = r3.A0D.A07("logs.zip");
            }
            try {
                ZipOutputStream zipOutputStream = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(file2)));
                byte[] bArr = new byte[16384];
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    File file3 = (File) it.next();
                    try {
                        BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file3), 16384);
                        try {
                            zipOutputStream.putNextEntry(new ZipEntry(file3.getName()));
                            while (true) {
                                int read = bufferedInputStream.read(bArr, 0, 16384);
                                if (read == -1) {
                                    break;
                                }
                                zipOutputStream.write(bArr, 0, read);
                            }
                            bufferedInputStream.close();
                        } catch (Throwable th) {
                            try {
                                bufferedInputStream.close();
                            } catch (Throwable unused) {
                            }
                            throw th;
                            break;
                        }
                    } catch (IOException e) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("debug-builder/cant zip file ");
                        sb.append(file3.getName());
                        Log.e(sb.toString(), e);
                    }
                }
                zipOutputStream.close();
                return file2;
            } catch (IOException e2) {
                Log.e("debug-builder/zip ", e2);
            }
        }
        return null;
    }

    public String A01(String str) {
        C22760zb r6 = this.A00;
        ArrayList latestLogs = Log.getLatestLogs(1);
        String str2 = null;
        if (latestLogs.size() < 1) {
            Log.e("debug-builder/upload-logs no logs found to be uploaded.");
        } else {
            Pair A02 = C14350lI.A02(r6.A0Q, (File) latestLogs.get(0), 8388608, 41943040);
            boolean booleanValue = ((Boolean) A02.first).booleanValue();
            File file = (File) A02.second;
            if (file != null) {
                str2 = r6.A05(file, str, false);
                if (booleanValue) {
                    file.delete();
                    return str2;
                }
            }
        }
        return str2;
    }
}
