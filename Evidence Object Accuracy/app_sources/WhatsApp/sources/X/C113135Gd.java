package X;

import java.io.OutputStream;

/* renamed from: X.5Gd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C113135Gd implements AnonymousClass5VT {
    public final byte[] A00;
    public final byte[] A01;

    public C113135Gd() {
        byte[] bArr = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102};
        this.A01 = bArr;
        byte[] bArr2 = new byte[128];
        this.A00 = bArr2;
        int i = 0;
        int i2 = 0;
        do {
            bArr2[i2] = -1;
            i2++;
        } while (i2 < 128);
        do {
            bArr2[bArr[i]] = (byte) i;
            i++;
        } while (i < 16);
        bArr2[65] = bArr2[97];
        bArr2[66] = bArr2[98];
        bArr2[67] = bArr2[99];
        bArr2[68] = bArr2[100];
        bArr2[69] = bArr2[101];
        bArr2[70] = bArr2[102];
    }

    public byte[] A00(String str, int i, int i2) {
        if (i2 < 0 || i > str.length() - i2) {
            throw new IndexOutOfBoundsException("invalid offset and/or length specified");
        } else if ((i2 & 1) == 0) {
            int i3 = i2 >>> 1;
            byte[] bArr = new byte[i3];
            for (int i4 = 0; i4 < i3; i4++) {
                byte[] bArr2 = this.A00;
                int i5 = i + 1;
                byte b = bArr2[str.charAt(i)];
                i = i5 + 1;
                int i6 = (b << 4) | bArr2[str.charAt(i5)];
                if (i6 >= 0) {
                    bArr[i4] = (byte) i6;
                } else {
                    throw C12990iw.A0i("invalid characters encountered in Hex string");
                }
            }
            return bArr;
        } else {
            throw C12990iw.A0i("a hexadecimal encoding must have an even number of characters");
        }
    }

    @Override // X.AnonymousClass5VT
    public int A9L(OutputStream outputStream, byte[] bArr, int i, int i2) {
        byte[] bArr2 = new byte[72];
        while (i2 > 0) {
            int min = Math.min(36, i2);
            int i3 = min + i;
            int i4 = 0;
            for (int i5 = i; i5 < i3; i5++) {
                int i6 = bArr[i5] & 255;
                int i7 = i4 + 1;
                byte[] bArr3 = this.A01;
                C72463ee.A0X(bArr3, bArr2, i6 >>> 4, i4);
                i4 = i7 + 1;
                C72463ee.A0X(bArr3, bArr2, i6 & 15, i7);
            }
            outputStream.write(bArr2, 0, i4 - 0);
            i += min;
            i2 -= min;
        }
        return i2 << 1;
    }
}
