package X;

import com.whatsapp.status.playback.fragment.StatusPlaybackContactFragment;
import java.util.Set;

/* renamed from: X.44o  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C858944o extends AbstractC33331dp {
    public final /* synthetic */ StatusPlaybackContactFragment A00;

    public C858944o(StatusPlaybackContactFragment statusPlaybackContactFragment) {
        this.A00 = statusPlaybackContactFragment;
    }

    @Override // X.AbstractC33331dp
    public void A00(Set set) {
        this.A00.A1K();
    }
}
