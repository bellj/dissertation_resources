package X;

import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import com.facebook.redex.RunnableBRunnable0Shape11S0100000_I0_11;
import com.whatsapp.jobqueue.job.GetStatusPrivacyJob;
import com.whatsapp.report.ReportActivity;
import com.whatsapp.util.Log;
import org.whispersystems.jobqueue.Job;

/* renamed from: X.1j2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC35941j2 {
    public void A00(int i) {
        String str;
        String str2;
        AbstractC32491cF r0;
        AbstractC450820c r3;
        Handler handler;
        int i2;
        int i3;
        Message message;
        StringBuilder sb;
        Handler handler2;
        int i4;
        int i5;
        AbstractC32461cC r02;
        SharedPreferences sharedPreferences;
        int i6;
        if (!(this instanceof AnonymousClass2OL)) {
            if (this instanceof AnonymousClass2NZ) {
                r0 = ((AnonymousClass2NZ) this).A01;
            } else if (!(this instanceof C49582Lj)) {
                if (this instanceof AnonymousClass2U4) {
                    C450720b r1 = ((AnonymousClass2U4) this).A00.A0H;
                    Log.i("xmpp/reader/read/client_config_error");
                    r3 = r1.A00;
                    handler = null;
                    i2 = 0;
                    i3 = 27;
                } else if (this instanceof C49852Na) {
                    r0 = ((C49852Na) this).A01;
                } else if (!(this instanceof C49872Nc)) {
                    if (this instanceof C49882Nd) {
                        int i7 = ((C49882Nd) this).A00;
                        sb = new StringBuilder("xmpp/reader/read/on-qr-deny-error ");
                        sb.append(i);
                        sb.append(" ");
                        sb.append(i7);
                    } else if (this instanceof C49912Ng) {
                        r0 = ((C49912Ng) this).A01;
                    } else if (this instanceof AnonymousClass2N8) {
                        AnonymousClass2DY r4 = ((AnonymousClass2N8) this).A00;
                        if (i != 404) {
                            StringBuilder sb2 = new StringBuilder("error in response while running get status privacy job");
                            GetStatusPrivacyJob getStatusPrivacyJob = r4.A00;
                            StringBuilder sb3 = new StringBuilder("; persistentId=");
                            sb3.append(((Job) getStatusPrivacyJob).A01);
                            sb2.append(sb3.toString());
                            sb2.append("; code=");
                            sb2.append(i);
                            Log.e(sb2.toString());
                        } else {
                            StringBuilder sb4 = new StringBuilder("get status privacy job response is 'no settings found on server'");
                            GetStatusPrivacyJob getStatusPrivacyJob2 = r4.A00;
                            StringBuilder sb5 = new StringBuilder("; persistentId=");
                            sb5.append(((Job) getStatusPrivacyJob2).A01);
                            sb4.append(sb5.toString());
                            Log.i(sb4.toString());
                        }
                        r4.A01.set(i);
                        return;
                    } else if (this instanceof AnonymousClass2OS) {
                        AnonymousClass2OS r12 = (AnonymousClass2OS) this;
                        if (i == 404) {
                            C450720b r13 = r12.A00.A0H;
                            Log.i("xmpp/reader/on-get-pre-key-digest-none");
                            r3 = r13.A00;
                            handler2 = null;
                            i4 = 0;
                            i5 = 83;
                        } else if (i == 503) {
                            C450720b r14 = r12.A00.A0H;
                            Log.i("xmpp/reader/on-get-pre-key-digest-server-error");
                            r3 = r14.A00;
                            handler2 = null;
                            i4 = 0;
                            i5 = 84;
                        } else {
                            return;
                        }
                        message = Message.obtain(handler2, i4, i5, i4);
                        r3.AYY(message);
                        return;
                    } else if (this instanceof C49892Ne) {
                        C49892Ne r15 = (C49892Ne) this;
                        AbstractC32491cF r03 = r15.A02;
                        if (r03 != null) {
                            r03.Aaw(i);
                        }
                        AbstractC14640lm r16 = r15.A00;
                        sb = new StringBuilder("xmpp/reader/read/on-qr-convo-seen-error ");
                        sb.append(i);
                        sb.append(" ");
                        sb.append(r16);
                    } else if (this instanceof C49922Nh) {
                        r0 = ((C49922Nh) this).A01;
                    } else if (this instanceof C49932Ni) {
                        r0 = ((C49932Ni) this).A01;
                    } else if (this instanceof C49952Nk) {
                        str2 = "xmpp/reader/read/on-qr-disconnect-error ";
                    } else if (this instanceof C50022Nr) {
                        C50022Nr r2 = (C50022Nr) this;
                        r2.A00.A0H.A04(r2.A03, i);
                        return;
                    } else if (this instanceof AnonymousClass2MH) {
                        return;
                    } else {
                        if (this instanceof AnonymousClass2NJ) {
                            r0 = ((AnonymousClass2NJ) this).A00;
                        } else if (this instanceof AnonymousClass2LO) {
                            return;
                        } else {
                            if (this instanceof AnonymousClass2NL) {
                                r0 = ((AnonymousClass2NL) this).A00;
                            } else if (this instanceof AnonymousClass2NM) {
                                ((AnonymousClass2NM) this).A00.A00(i);
                                return;
                            } else if (this instanceof C50182Ol) {
                                r0 = ((C50182Ol) this).A00;
                            } else if (this instanceof C49812Mt) {
                                int i8 = 2019;
                                if (i != 401) {
                                    i8 = 2020;
                                    if (i != 403) {
                                        i8 = 2021;
                                        if (i != 404) {
                                            i8 = 2018;
                                        }
                                    }
                                }
                                C20710wC.A02(i8, null);
                                return;
                            } else if (this instanceof C49532Ld) {
                                r0 = ((C49532Ld) this).A00;
                            } else if (this instanceof C50212Oo) {
                                r0 = ((C50212Oo) this).A00;
                            } else if (this instanceof AnonymousClass2LN) {
                                r0 = ((AnonymousClass2LN) this).A00;
                            } else if (this instanceof AnonymousClass2MT) {
                                r0 = ((AnonymousClass2MT) this).A02;
                            } else if (!(this instanceof AnonymousClass2MQ)) {
                                if (this instanceof AnonymousClass2MX) {
                                    r02 = ((AnonymousClass2MX) this).A00;
                                } else if (this instanceof C50202On) {
                                    r0 = ((C50202On) this).A02;
                                } else if (this instanceof AnonymousClass2N5) {
                                    ((AnonymousClass2N5) this).A00.APl(i);
                                    return;
                                } else if (this instanceof C50152Oi) {
                                    r0 = ((C50152Oi) this).A00;
                                } else if (!(this instanceof AnonymousClass34I)) {
                                    if (this instanceof C49752Mj) {
                                        r0 = ((C49752Mj) this).A00;
                                    } else if (this instanceof C49772Mn) {
                                        C49762Mm r17 = ((C49772Mn) this).A01;
                                        if (i == 404) {
                                            ReportActivity reportActivity = r17.A00;
                                            if (reportActivity.A0M.A01() == 1) {
                                                reportActivity.A0M.A04();
                                                return;
                                            }
                                            return;
                                        }
                                        str2 = "send-get-gdpr-report/failed/error ";
                                    } else if (this instanceof AnonymousClass2Mr) {
                                        C49802Mq r22 = ((AnonymousClass2Mr) this).A01;
                                        StringBuilder sb6 = new StringBuilder("send-request-gdpr-report/failed/error ");
                                        sb6.append(i);
                                        Log.e(sb6.toString());
                                        ReportActivity reportActivity2 = r22.A00;
                                        ((ActivityC13810kN) reportActivity2).A05.A0H(new RunnableBRunnable0Shape11S0100000_I0_11(reportActivity2, 10));
                                        return;
                                    } else if (this instanceof AnonymousClass2NH) {
                                        r0 = ((AnonymousClass2NH) this).A01;
                                    } else if (this instanceof AnonymousClass2MO) {
                                        StringBuilder sb7 = new StringBuilder("ViewGroupInviteActivity/failed-to-get-group-photo/");
                                        sb7.append(i);
                                        Log.w(sb7.toString());
                                        return;
                                    } else if (this instanceof AnonymousClass2NI) {
                                        r02 = ((AnonymousClass2NI) this).A00;
                                    } else if (this instanceof AnonymousClass2Li) {
                                        r0 = ((AnonymousClass2Li) this).A01;
                                    } else if (this instanceof AnonymousClass2NX) {
                                        return;
                                    } else {
                                        if (this instanceof AnonymousClass2NS) {
                                            StringBuilder sb8 = new StringBuilder("change number failed; code=");
                                            sb8.append(i);
                                            Log.w(sb8.toString());
                                            C450720b r23 = ((AnonymousClass2NS) this).A00.A0H;
                                            StringBuilder sb9 = new StringBuilder("xmpp/reader/on-change-number-error = ");
                                            sb9.append(i);
                                            Log.i(sb9.toString());
                                            r3 = r23.A00;
                                            handler = null;
                                            i2 = 0;
                                            i3 = 201;
                                        } else if (this instanceof AnonymousClass2LT) {
                                            C450720b r24 = ((AnonymousClass2LT) this).A00.A0H;
                                            StringBuilder sb10 = new StringBuilder("xmpp/reader/on-number-normalization-error = ");
                                            sb10.append(i);
                                            Log.i(sb10.toString());
                                            r3 = r24.A00;
                                            handler = null;
                                            i2 = 0;
                                            i3 = 199;
                                        } else {
                                            return;
                                        }
                                    }
                                    r0.Aaw(i);
                                    return;
                                } else {
                                    C21680xo r42 = ((AnonymousClass34I) this).A00.A0D;
                                    r42.A03(i);
                                    if (i == 207 || i == 304 || (i >= 400 && i <= 503)) {
                                        synchronized (r42) {
                                            sharedPreferences = r42.A01;
                                            i6 = sharedPreferences.getInt("ab_props:sys:fetch_attemp_count", 0);
                                        }
                                        int i9 = i6 + 1;
                                        r42.A02(i9);
                                        if (i9 >= 3) {
                                            long A00 = r42.A02.A00();
                                            synchronized (r42) {
                                                SharedPreferences.Editor edit = sharedPreferences.edit();
                                                edit.putLong("ab_props:sys:last_refresh_time", A00);
                                                edit.apply();
                                            }
                                            r42.A02(0);
                                            return;
                                        }
                                        return;
                                    }
                                    str2 = "ABPropsManager/onABPropError; unknown error code: ";
                                }
                                r02.APl(i);
                                return;
                            } else {
                                C32561cM r04 = ((AnonymousClass2MQ) this).A00;
                                if (r04 != null) {
                                    r04.A00.A00 = i;
                                    return;
                                }
                                return;
                            }
                        }
                    }
                    str = sb.toString();
                    Log.e(str);
                } else {
                    r0 = ((C49872Nc) this).A01;
                }
                message = Message.obtain(handler, i2, i3, i);
                r3.AYY(message);
                return;
            } else {
                r0 = ((C49582Lj) this).A00;
            }
            if (r0 == null) {
                return;
            }
            r0.Aaw(i);
            return;
        }
        str2 = "Connection/sendClearFbnsToken/failed to clear code=";
        StringBuilder sb11 = new StringBuilder(str2);
        sb11.append(i);
        str = sb11.toString();
        Log.e(str);
    }

    public void A01(AnonymousClass1V8 r8) {
        if (this instanceof AnonymousClass2OW) {
            C450720b r4 = ((AnonymousClass2OW) this).A00.A0H;
            StringBuilder sb = new StringBuilder("xmpp/reader/read/ping_response; timestamp=");
            sb.append(0L);
            Log.i(sb.toString());
            r4.A00.ATl(0);
        } else if (!(this instanceof C35931j1)) {
            for (AnonymousClass1V8 r3 : r8.A0J("error")) {
                if (r3 != null) {
                    String A0I = r3.A0I("code", null);
                    String A0I2 = r3.A0I("text", null);
                    if (A0I != null) {
                        int parseInt = Integer.parseInt(A0I);
                        if (!(this instanceof AnonymousClass2NF)) {
                            A00(parseInt);
                        } else {
                            AnonymousClass2NF r32 = (AnonymousClass2NF) this;
                            r32.A00.A0H.A05(r32.A01, r32.A02, A0I2, parseInt);
                        }
                    }
                }
            }
        } else {
            C35931j1 r33 = (C35931j1) this;
            AnonymousClass1V8 A0F = r8.A0F("error");
            r33.A01.set(A0F.A05("code", 0));
            AnonymousClass1V8 A0E = A0F.A0E("identity");
            if (A0E != null) {
                r33.A02.set(A0E.A01);
            }
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r2v19, resolved type: android.os.Bundle */
    /* JADX DEBUG: Multi-variable search result rejected for r9v7, resolved type: com.whatsapp.jid.UserJid[] */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r6v5, types: [byte[][], java.io.Serializable] */
    /* JADX WARNING: Removed duplicated region for block: B:177:0x03e2  */
    /* JADX WARNING: Removed duplicated region for block: B:543:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02(X.AnonymousClass1V8 r38) {
        /*
        // Method dump skipped, instructions count: 3646
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC35941j2.A02(X.1V8):void");
    }

    public void A03(Exception exc) {
        AbstractC34031fT r0;
        if (this instanceof AnonymousClass2NZ) {
            r0 = ((AnonymousClass2NZ) this).A00;
        } else if (this instanceof C49852Na) {
            r0 = ((C49852Na) this).A00;
        } else if (this instanceof C49872Nc) {
            r0 = ((C49872Nc) this).A00;
        } else if (this instanceof C49912Ng) {
            r0 = ((C49912Ng) this).A00;
        } else if (this instanceof AnonymousClass2NF) {
            AnonymousClass2NF r1 = (AnonymousClass2NF) this;
            r1.A00.A0H.A05(r1.A01, r1.A02, exc.getMessage(), -1);
            return;
        } else if (this instanceof C49892Ne) {
            r0 = ((C49892Ne) this).A01;
        } else if (this instanceof C49922Nh) {
            r0 = ((C49922Nh) this).A00;
        } else if (this instanceof C49932Ni) {
            r0 = ((C49932Ni) this).A00;
        } else if (this instanceof AnonymousClass2Li) {
            r0 = ((AnonymousClass2Li) this).A00;
            r0.Ab0(exc);
        } else {
            return;
        }
        if (r0 == null) {
            return;
        }
        r0.Ab0(exc);
    }
}
