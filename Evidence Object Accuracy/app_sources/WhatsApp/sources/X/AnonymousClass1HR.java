package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BadParcelableException;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.SparseArray;
import com.whatsapp.util.Log;
import java.lang.reflect.Array;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1HR  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1HR extends BroadcastReceiver {
    public AnonymousClass01d A00;
    public C16120oU A01;
    public final C18280sC A02;
    public final AnonymousClass143 A03;
    public final Object A04 = new Object();
    public volatile boolean A05 = false;

    public AnonymousClass1HR(C18280sC r2, AnonymousClass143 r3) {
        this.A02 = r2;
        this.A03 = r3;
    }

    public static Object A00(Object obj) {
        Class<?> cls = obj.getClass();
        if (cls.isPrimitive() || (obj instanceof Integer) || (obj instanceof Long) || (obj instanceof Float) || (obj instanceof Double) || (obj instanceof CharSequence) || (obj instanceof Boolean) || (obj instanceof Byte) || (obj instanceof Character)) {
            return obj;
        }
        if (cls.isArray()) {
            JSONArray jSONArray = new JSONArray();
            int length = Array.getLength(obj);
            for (int i = 0; i < length; i++) {
                jSONArray.put(A00(Array.get(obj, i)));
            }
            return jSONArray;
        } else if (obj instanceof List) {
            JSONArray jSONArray2 = new JSONArray();
            for (Object obj2 : (List) obj) {
                jSONArray2.put(A00(obj2));
            }
            return jSONArray2;
        } else if (obj instanceof Bundle) {
            return A01((Bundle) obj);
        } else {
            if (obj instanceof SparseArray) {
                SparseArray sparseArray = (SparseArray) obj;
                JSONObject jSONObject = new JSONObject();
                int size = sparseArray.size();
                for (int i2 = 0; i2 < size; i2++) {
                    jSONObject.put(Integer.toString(sparseArray.keyAt(i2)), A00(sparseArray.valueAt(i2)));
                }
                return jSONObject;
            }
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("class", cls.getCanonicalName());
            jSONObject2.put("string", obj.toString());
            return jSONObject2;
        }
    }

    public static JSONObject A01(Bundle bundle) {
        JSONObject jSONObject = new JSONObject();
        for (String str : bundle.keySet()) {
            Object obj = bundle.get(str);
            if (str == null) {
                str = "null";
            }
            jSONObject.put(str, A00(obj));
        }
        return jSONObject;
    }

    public static /* synthetic */ void A02(Context context, AnonymousClass1HR r6, AnonymousClass01d r7, C14850m9 r8) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.BATTERY_CHANGED");
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            intentFilter.addAction("android.os.action.POWER_SAVE_MODE_CHANGED");
        }
        if ("samsung".equalsIgnoreCase(Build.MANUFACTURER) && r8.A07(580)) {
            intentFilter.addAction("com.samsung.android.action.WARNING_NOTIFICATION");
        }
        context.registerReceiver(r6, intentFilter);
        if (i >= 21) {
            r6.A03(r7);
        } else {
            r6.A03.A05(false);
        }
    }

    public final void A03(AnonymousClass01d r3) {
        boolean isPowerSaveMode;
        PowerManager A0I = r3.A0I();
        if (A0I == null) {
            Log.w("battery-receiver/on-action-power-save-mode-changed pm=null");
            isPowerSaveMode = true;
        } else {
            isPowerSaveMode = A0I.isPowerSaveMode();
        }
        this.A03.A05(isPowerSaveMode);
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (!this.A05) {
            synchronized (this.A04) {
                if (!this.A05) {
                    AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass22D.A00(context);
                    this.A01 = (C16120oU) r1.ANE.get();
                    this.A00 = (AnonymousClass01d) r1.ALI.get();
                    this.A05 = true;
                }
            }
        }
        String action = intent.getAction();
        switch (action.hashCode()) {
            case -1538406691:
                if (action.equals("android.intent.action.BATTERY_CHANGED")) {
                    C32441cA r2 = new C32441cA(intent);
                    C18280sC r12 = this.A02;
                    if (!r12.A00.equals(r2)) {
                        r12.A00 = r2;
                        for (AbstractC22240yl r0 : r12.A01()) {
                            r0.ANB(r2);
                        }
                        StringBuilder sb = new StringBuilder("battery changed; newEvent=");
                        sb.append(r2);
                        Log.i(sb.toString());
                        return;
                    }
                    return;
                }
                break;
            case -1209048666:
                if (action.equals("com.samsung.android.action.WARNING_NOTIFICATION")) {
                    String str = intent.getPackage();
                    if (str == null || str.equals(context.getPackageName())) {
                        AnonymousClass28R r22 = new AnonymousClass28R();
                        if (intent.getDataString() != null) {
                            r22.A00 = intent.getDataString();
                        }
                        Bundle extras = intent.getExtras();
                        if (extras != null) {
                            extras.keySet();
                            try {
                                r22.A01 = extras.toString();
                                r22.A02 = A01(extras).toString();
                            } catch (BadParcelableException | JSONException e) {
                                Log.e("battery-receiver/samsung-warning/unable-to-serialize-extras", e);
                            }
                        }
                        this.A01.A07(r22);
                        return;
                    }
                    return;
                }
                break;
            case 1779291251:
                if (action.equals("android.os.action.POWER_SAVE_MODE_CHANGED")) {
                    A03(this.A00);
                    return;
                }
                break;
        }
        StringBuilder sb2 = new StringBuilder("Unexpected action: ");
        sb2.append(intent.getAction());
        throw new IllegalArgumentException(sb2.toString());
    }
}
