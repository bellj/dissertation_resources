package X;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.RectF;
import android.net.Uri;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.cardview.widget.CardView;
import com.whatsapp.Mp4Ops;
import com.whatsapp.R;
import com.whatsapp.SuspiciousLinkWarningDialogFragment;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.status.playback.fragment.OpenLinkConfirmationDialogFragment;
import com.whatsapp.util.Log;
import com.whatsapp.webpagepreview.WebPagePreviewView;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.35V  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass35V extends AbstractC33641ei {
    public View A00;
    public Integer A01;
    public final Context A02 = A02();
    public final AbstractC15710nm A03;
    public final Mp4Ops A04;
    public final C18790t3 A05;
    public final TextEmojiLabel A06;
    public final C22640zP A07;
    public final C16590pI A08;
    public final AnonymousClass1BK A09;
    public final C14850m9 A0A;
    public final C28861Ph A0B;
    public final AnonymousClass1BD A0C;
    public final C92784Xk A0D;
    public final C53052cO A0E;
    public final AbstractC14440lR A0F;
    public final boolean A0G;
    public final boolean A0H;

    public AnonymousClass35V(AbstractC15710nm r20, C14900mE r21, AnonymousClass18U r22, Mp4Ops mp4Ops, C18790t3 r24, C22640zP r25, AnonymousClass01d r26, C16590pI r27, AnonymousClass018 r28, AnonymousClass1BK r29, C14850m9 r30, AnonymousClass1CH r31, AbstractC15340mz r32, AnonymousClass1BD r33, C48242Fd r34, AbstractC14440lR r35) {
        super(r22, r21, r26, r28, r31, r34);
        Bitmap decodeByteArray;
        int width;
        int height;
        AnonymousClass009.A05(r32);
        C28861Ph r8 = (C28861Ph) r32;
        this.A0B = r8;
        this.A0A = r30;
        this.A09 = r29;
        this.A07 = r25;
        this.A0C = r33;
        this.A0F = r35;
        this.A08 = r27;
        this.A04 = mp4Ops;
        this.A03 = r20;
        this.A05 = r24;
        boolean A07 = r30.A07(1522);
        this.A0G = A07;
        this.A0H = r30.A07(1877);
        C53052cO A00 = C53052cO.A00(A02(), r29, r8, true, A07);
        this.A0E = A00;
        TextEmojiLabel A0U = C12970iu.A0U(A00, R.id.message_text);
        this.A06 = A0U;
        this.A0D = A00.A07;
        boolean z = A0U.getText().length() <= 350;
        CharSequence text = A0U.getText();
        if (text instanceof Spanned) {
            Spanned spanned = (Spanned) text;
            AnonymousClass3ML[] r7 = (AnonymousClass3ML[]) spanned.getSpans(0, text.length(), AnonymousClass3ML.class);
            C52282aY[] r0 = (C52282aY[]) spanned.getSpans(0, text.length(), C52282aY.class);
            int length = r7.length;
            if (length > 0 || r0.length > 0) {
                this.A01 = 1;
                AnonymousClass1BD.A0K.put(r8.A0z.A01, C12970iu.A0h());
            }
            if (A07 && C30041Vv.A0q(r8) && z && length > 0) {
                AnonymousClass3ML r1 = r7[0];
                boolean z2 = r1.A06;
                this.A01 = Integer.valueOf(z2 ? 2 : 3);
                String str = r1.A04;
                AnonymousClass1BK r12 = this.A09;
                C28861Ph r10 = this.A0B;
                if (r12.A00(r10.A0B(), r10, str) == null) {
                    CardView cardView = this.A0E.A00;
                    this.A00 = cardView;
                    if (cardView == null) {
                        Log.e("StatusPlaybackText/showInlineLinkPreview wrong layout used for rendering text status");
                    } else {
                        cardView.setVisibility(0);
                        View findViewById = this.A00.findViewById(R.id.web_page_preview);
                        WebPagePreviewView webPagePreviewView = (WebPagePreviewView) AnonymousClass028.A0D(findViewById, R.id.link_preview_frame);
                        View A0D = AnonymousClass028.A0D(webPagePreviewView, R.id.link_preview_content);
                        if (str.equals(C33771f3.A01(r10.A14()))) {
                            webPagePreviewView.A0C(r10, str, this.A07.A07());
                            if (!(r10.A17() == null || r10.A14() == null || AnonymousClass24D.A01(r10.A14()) >= 150)) {
                                DisplayMetrics displayMetrics = A02().getResources().getDisplayMetrics();
                                if (((float) displayMetrics.heightPixels) / displayMetrics.density >= 640.0f) {
                                    try {
                                        byte[] A17 = r10.A17();
                                        decodeByteArray = BitmapFactory.decodeByteArray(A17, 0, A17.length);
                                        width = decodeByteArray.getWidth();
                                        height = decodeByteArray.getHeight();
                                    } catch (OutOfMemoryError unused) {
                                    }
                                    if (width >= 300) {
                                        if (((float) width) / ((float) height) > 1.4f) {
                                            try {
                                                int dimension = (int) A02().getResources().getDimension(R.dimen.status_text_composer_large_link_preview_width);
                                                C12980iv.A1B(webPagePreviewView, R.id.cancel, 8);
                                                AnonymousClass3IJ A002 = AnonymousClass3IJ.A00(r10);
                                                ViewGroup.LayoutParams layoutParams = this.A00.getLayoutParams();
                                                layoutParams.width = (int) A02().getResources().getDimension(R.dimen.status_text_composer_large_link_preview_width);
                                                this.A00.setLayoutParams(layoutParams);
                                                int height2 = (decodeByteArray.getHeight() * dimension) / decodeByteArray.getWidth();
                                                Bitmap createScaledBitmap = Bitmap.createScaledBitmap(decodeByteArray, dimension, height2, true);
                                                if (!this.A0H || !A002.A04 || !C28391Mz.A01() || A002.A01 == 4) {
                                                    webPagePreviewView.A00();
                                                    webPagePreviewView.A07(dimension, height2);
                                                    webPagePreviewView.setImageLargeThumbWithBitmap(createScaledBitmap);
                                                } else {
                                                    A0C(createScaledBitmap, A002, webPagePreviewView, dimension, height2);
                                                    findViewById = A0D;
                                                }
                                            } catch (OutOfMemoryError unused2) {
                                                AnonymousClass24D.A07(A02(), this.A00);
                                            }
                                            this.A00.requestLayout();
                                            webPagePreviewView.setVisibility(0);
                                        }
                                    }
                                }
                            }
                            AnonymousClass24D.A07(A02(), this.A00);
                            webPagePreviewView.setVisibility(0);
                        } else {
                            webPagePreviewView.setVisibility(8);
                        }
                        findViewById.setOnClickListener(new AnonymousClass47Z(this, str, z2));
                        findViewById.setOnLongClickListener(new View.OnLongClickListener(str) { // from class: X.4nC
                            public final /* synthetic */ String A01;

                            {
                                this.A01 = r2;
                            }

                            @Override // android.view.View.OnLongClickListener
                            public final boolean onLongClick(View view) {
                                return AnonymousClass35V.this.A0H(this.A01);
                            }
                        });
                        return;
                    }
                }
                this.A01 = 1;
            }
        }
    }

    @Override // X.AbstractC33641ei
    public long A01() {
        return this.A0D.A03;
    }

    @Override // X.AbstractC33641ei
    public void A07() {
        AbstractC33641ei.A00(this.A0D, this);
    }

    @Override // X.AbstractC33641ei
    public void A08() {
        this.A0D.A02();
    }

    public final void A0C(Bitmap bitmap, AnonymousClass3IJ r8, WebPagePreviewView webPagePreviewView, int i, int i2) {
        FrameLayout frameLayout = (FrameLayout) AnonymousClass028.A0D(webPagePreviewView, R.id.large_thumb_frame);
        AnonymousClass2Bd.A02(r8, webPagePreviewView);
        webPagePreviewView.A09.getLayoutParams().width = i;
        webPagePreviewView.A09.getLayoutParams().height = i2;
        webPagePreviewView.A09.requestLayout();
        webPagePreviewView.setVideoLargeThumbWithBitmap(bitmap);
        frameLayout.setOnClickListener(new C863847b(frameLayout, r8, this, i, i2));
    }

    public final void A0D(FrameLayout frameLayout, AnonymousClass3IJ r10, int i, int i2) {
        C14320lF r0;
        String obj = Uri.parse(this.A0B.A06).buildUpon().appendQueryParameter("wa_logging_event", "video_play_open").build().toString();
        int i3 = r10.A01;
        C14900mE r2 = super.A01;
        AbstractC14440lR r6 = this.A0F;
        C18790t3 r3 = this.A05;
        AnonymousClass018 r5 = super.A03;
        if (i3 == 4) {
            return;
        }
        if (obj == null || (r0 = (C14320lF) C49132Jk.A00.get(obj)) == null) {
            if (frameLayout != null) {
                C49132Jk.A00(r2, r3, new AnonymousClass537(frameLayout, this, i, i2), r5, r6, obj);
            }
        } else if (frameLayout != null) {
            A0E(frameLayout, r0.A07, i, i2);
        }
    }

    public void A0E(FrameLayout frameLayout, AnonymousClass3JH r18, int i, int i2) {
        if (r18 != null) {
            String str = r18.A02;
            View A0D = AnonymousClass028.A0D(frameLayout, R.id.large_thumb);
            C14900mE r10 = super.A01;
            AbstractC14440lR r13 = this.A0F;
            AnonymousClass01d r11 = super.A02;
            AnonymousClass018 r12 = super.A03;
            Activity A00 = AnonymousClass12P.A00(this.A02);
            C16590pI r5 = this.A08;
            Mp4Ops mp4Ops = this.A04;
            AnonymousClass21S r7 = new AnonymousClass21S(AnonymousClass12P.A00(A00), Uri.parse(str), r10, r11, r12, r13, new C865147r(this.A03, mp4Ops, r5, AnonymousClass3JZ.A09(A00, A00.getString(R.string.app_name))), null);
            C47482Aw r1 = r7.A0Y;
            frameLayout.addView(r1);
            ViewGroup.LayoutParams layoutParams = r1.getLayoutParams();
            layoutParams.height = i2;
            layoutParams.width = i;
            r1.setLayoutParams(layoutParams);
            r1.setVisibility(0);
            A0D.setVisibility(8);
            r7.A07();
        }
    }

    public void A0F(String str, Set set, boolean z) {
        A04();
        String str2 = this.A0B.A0z.A01;
        AnonymousClass1BD.A0K.put(str2, 1);
        if (set != null) {
            ((ActivityC13810kN) AnonymousClass12P.A01(A02(), ActivityC13810kN.class)).Adm(new SuspiciousLinkWarningDialogFragment(this, str, str2, set));
        } else if (z) {
            ((ActivityC13810kN) AnonymousClass12P.A01(A02(), ActivityC13810kN.class)).Adm(new OpenLinkConfirmationDialogFragment(super.A00, this, str, str2));
        } else {
            super.A00.AbB(A02(), Uri.parse(str), 0, 1);
        }
    }

    public boolean A0G(float f, float f2, boolean z) {
        CharSequence text = this.A06.getText();
        boolean z2 = false;
        if (text instanceof Spanned) {
            Spanned spanned = (Spanned) text;
            AnonymousClass3ML[] r6 = (AnonymousClass3ML[]) spanned.getSpans(0, spanned.length(), AnonymousClass3ML.class);
            int length = r6.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                }
                AnonymousClass3ML r2 = r6[i];
                Iterator it = r2.A05.iterator();
                while (it.hasNext()) {
                    if (((RectF) it.next()).contains(f, f2)) {
                        z2 = true;
                        if (z) {
                            String str = r2.A04;
                            String A05 = AnonymousClass24D.A05(str);
                            AnonymousClass1BK r22 = this.A09;
                            C28861Ph r1 = this.A0B;
                            Set A00 = r22.A00(r1.A0B(), r1, str);
                            AnonymousClass1BD.A0K.put(r1.A0z.A01, 1);
                            A0F(str, A00, A05.contains("…"));
                        }
                    }
                }
                i++;
            }
        }
        return z2;
    }

    public boolean A0H(String str) {
        ClipboardManager A0B = super.A02.A0B();
        if (A0B != null) {
            if (!TextUtils.isEmpty(str)) {
                try {
                    A0B.setPrimaryClip(ClipData.newPlainText(str, str));
                    super.A01.A07(R.string.link_copied_confirmation, 0);
                    return true;
                } catch (NullPointerException | SecurityException e) {
                    Log.e("invitelink/copy/npe", e);
                }
            }
            return true;
        }
        super.A01.A07(R.string.view_contact_unsupport, 0);
        return true;
    }
}
