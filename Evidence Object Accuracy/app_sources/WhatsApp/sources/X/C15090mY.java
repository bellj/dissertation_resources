package X;

import android.app.AppOpsManager;
import android.content.Context;
import android.os.Build;

/* renamed from: X.0mY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15090mY {
    public final Context A00;

    public C15090mY(Context context) {
        this.A00 = context;
    }

    public final boolean A00(int i) {
        boolean z = false;
        if (Build.VERSION.SDK_INT >= 19) {
            z = true;
        }
        if (z) {
            try {
                AppOpsManager appOpsManager = (AppOpsManager) this.A00.getSystemService("appops");
                if (appOpsManager != null) {
                    appOpsManager.checkPackage(i, "com.google.android.gms");
                    return true;
                }
                throw new NullPointerException("context.getSystemService(Context.APP_OPS_SERVICE) is null");
            } catch (SecurityException unused) {
                return false;
            }
        } else {
            String[] packagesForUid = this.A00.getPackageManager().getPackagesForUid(i);
            if (packagesForUid != null) {
                for (String str : packagesForUid) {
                    if ("com.google.android.gms".equals(str)) {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
