package X;

import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import com.whatsapp.R;
import com.whatsapp.biz.catalog.view.CategoryMediaCard;

/* renamed from: X.2dH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC53212dH extends LinearLayout implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;

    public AbstractC53212dH(Context context) {
        super(context);
        A01();
    }

    public AbstractC53212dH(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A01();
    }

    public AbstractC53212dH(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A01();
    }

    public AbstractC53212dH(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        A01();
    }

    public static int A00(DisplayMetrics displayMetrics, View view, WindowManager windowManager) {
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        int dimensionPixelSize = view.getResources().getDimensionPixelSize(R.dimen.catalog_media_grid_horizontal_padding) << 1;
        return (Math.min(displayMetrics.widthPixels, displayMetrics.heightPixels) - dimensionPixelSize) - (view.getResources().getDimensionPixelSize(R.dimen.catalog_media_grid_spacing) << 1);
    }

    public void A01() {
        if (this instanceof CategoryMediaCard) {
            CategoryMediaCard categoryMediaCard = (CategoryMediaCard) this;
            if (!categoryMediaCard.A03) {
                categoryMediaCard.A03 = true;
                categoryMediaCard.A02 = C12960it.A0R(AnonymousClass2P6.A00(categoryMediaCard.generatedComponent()));
            }
        } else if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }
}
