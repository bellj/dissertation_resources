package X;

import com.whatsapp.R;

/* renamed from: X.6Ct  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133956Ct implements AbstractC136356Mf {
    public final /* synthetic */ long A00;
    public final /* synthetic */ AbstractC118085bF A01;

    @Override // X.AbstractC136356Mf
    public void AOm() {
    }

    public C133956Ct(AbstractC118085bF r1, long j) {
        this.A01 = r1;
        this.A00 = j;
    }

    @Override // X.AbstractC136356Mf
    public void AOE() {
        AbstractC118085bF r4 = this.A01;
        C12970iu.A1C(C117295Zj.A05(r4.A06), "payment_incentive_offer_dismissed", this.A00);
        r4.A00.A0A(new AnonymousClass60P(new AnonymousClass609(-1, -1, R.dimen.nux_icon_size, R.dimen.nux_icon_size), null, new C1310260x(new Object[]{""}, 0), new C1310260x(new Object[]{""}, 0), new C1310260x(new Object[]{""}, 0), R.id.payment_incentive_nux_view, 8, -1, -1, 0));
    }
}
