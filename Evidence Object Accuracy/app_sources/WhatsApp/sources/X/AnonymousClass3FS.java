package X;

import android.view.animation.AlphaAnimation;
import android.view.animation.Interpolator;
import com.whatsapp.WaImageView;

/* renamed from: X.3FS  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3FS {
    public int A00 = 0;
    public int A01;
    public int A02;
    public boolean A03;
    public boolean A04 = false;
    public final int A05;
    public final int A06;
    public final int A07;
    public final C89514Kg A08;
    public final C47322Ae A09;
    public final C63573Cc A0A;
    public final C63583Cd A0B;

    public AnonymousClass3FS(C89514Kg r2, C47322Ae r3, C63573Cc r4, C63583Cd r5, int i, int i2, int i3) {
        this.A05 = i;
        this.A07 = i2;
        this.A06 = i3;
        this.A09 = r3;
        this.A0B = r5;
        this.A08 = r2;
        this.A0A = r4;
        r3.A02 = true;
        r3.A00 = new C1109657o(this);
    }

    public void A00(int i, int i2) {
        int i3;
        this.A01 = i2;
        this.A0A.A01.A07.A01(i2);
        if (this.A02 != i || this.A04) {
            if (this.A04) {
                i3 = this.A00;
            } else {
                i3 = 0;
            }
            A01(i3, i);
        }
    }

    public void A01(int i, int i2) {
        if (!this.A03) {
            C63573Cc r0 = this.A0A;
            r0.A00(i);
            this.A00 = i;
            this.A02 = i2;
            DialogC51702Ye r2 = r0.A01;
            r2.A06.setSizeAndInvalidate((float) i2);
            if (this.A04) {
                r2.A07.A01(this.A01);
                this.A04 = false;
            }
        }
    }

    public final void A02(boolean z) {
        Interpolator r0;
        C63573Cc r1 = this.A0A;
        boolean z2 = !this.A0B.A01.A03.A00.isEmpty();
        DialogC51702Ye r3 = r1.A01;
        WaImageView waImageView = r3.A05;
        if (waImageView != null) {
            int i = 4;
            int visibility = waImageView.getVisibility();
            if (!z2) {
                if (visibility == 4) {
                    return;
                }
            } else if (visibility == 0) {
                return;
            }
            WaImageView waImageView2 = r3.A05;
            if (z2) {
                i = 0;
            }
            waImageView2.setVisibility(i);
            if (z) {
                float f = 0.0f;
                float f2 = 1.0f;
                if (z2) {
                    f2 = 0.0f;
                    f = 1.0f;
                }
                AlphaAnimation alphaAnimation = new AlphaAnimation(f2, f);
                if (z2) {
                    r0 = new AnonymousClass078();
                } else {
                    r0 = new C016007o();
                }
                alphaAnimation.setInterpolator(r0);
                alphaAnimation.setDuration(100);
                r3.A05.startAnimation(alphaAnimation);
            }
        }
    }
}
