package X;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* renamed from: X.3GW  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3GW {
    public static AbstractC470728v[] A00 = {new AnonymousClass3YK(6), new AnonymousClass3YK(7), new AnonymousClass3YK(8)};
    public static AbstractC470728v[] A01 = {new AnonymousClass3YK(0), new AnonymousClass3YK(1), new AnonymousClass3YK(2), new AnonymousClass3YK(3), new AnonymousClass3YK(4), new AnonymousClass3YK(5)};

    public static List A00() {
        ArrayList A0l = C12960it.A0l();
        for (EnumC869149l r0 : EnumC869149l.values()) {
            A0l.addAll(Arrays.asList(r0.shapeData));
        }
        return A0l;
    }
}
