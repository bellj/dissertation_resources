package X;

import com.whatsapp.util.Log;

/* renamed from: X.5hk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C121375hk extends AbstractC130285z6 {
    public String A00;
    public String A01;
    public String A02;

    public C121375hk(AnonymousClass1V8 r3, String str) {
        try {
            this.A00 = str;
            AnonymousClass1V8 A0F = r3.A0F("tds_url");
            this.A02 = A0F.A0H("url");
            this.A01 = A0F.A0H("termination_url");
        } catch (AnonymousClass1V9 unused) {
            Log.e("PAY: TDSChallenge parsing has failed.");
        }
    }
}
