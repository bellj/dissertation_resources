package X;

import android.content.Context;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import com.facebook.redex.EmptyBaseViewOnClick0CListener;
import com.facebook.redex.IDxLListenerShape13S0100000_1_I1;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.status.StatusConfirmMuteDialogFragment;
import com.whatsapp.status.StatusConfirmUnmuteDialogFragment;
import com.whatsapp.status.playback.fragment.StatusPlaybackBaseFragment;
import com.whatsapp.status.playback.fragment.StatusPlaybackContactFragment;
import java.util.List;

/* renamed from: X.2ju  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class View$OnClickListenerC55932ju extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public ViewTreeObserver A00;
    public final View A01;
    public final ViewTreeObserver.OnGlobalLayoutListener A02 = new IDxLListenerShape13S0100000_1_I1(this, 11);
    public final AnonymousClass07N A03;
    public final StatusPlaybackBaseFragment A04;
    public final int[] A05 = new int[2];
    public final int[] A06 = new int[2];

    public View$OnClickListenerC55932ju(Context context, View view, AnonymousClass018 r6, StatusPlaybackBaseFragment statusPlaybackBaseFragment) {
        this.A03 = new AnonymousClass07N(context, view, C28141Kv.A01(r6) ? 5 : 3, R.attr.actionOverflowMenuStyle);
        this.A01 = view;
        this.A04 = statusPlaybackBaseFragment;
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        if (this.A00 == null) {
            ViewTreeObserver viewTreeObserver = this.A01.getViewTreeObserver();
            this.A00 = viewTreeObserver;
            viewTreeObserver.addOnGlobalLayoutListener(this.A02);
        }
        View view2 = this.A01;
        int[] iArr = this.A05;
        view2.getLocationOnScreen(iArr);
        int[] iArr2 = this.A06;
        iArr2[0] = iArr[0];
        iArr2[1] = iArr[1];
        AnonymousClass07N r3 = this.A03;
        AnonymousClass07H r6 = r3.A04;
        r6.clear();
        StatusPlaybackBaseFragment statusPlaybackBaseFragment = this.A04;
        StatusPlaybackContactFragment statusPlaybackContactFragment = (StatusPlaybackContactFragment) statusPlaybackBaseFragment;
        boolean z = C15860o1.A00(statusPlaybackContactFragment.A0P, statusPlaybackContactFragment.A0R).A0H;
        int i = R.id.menuitem_conversations_mute;
        int i2 = R.string.mute_status;
        if (z) {
            i = R.id.menuitem_conversations_unmute;
            i2 = R.string.unmute_status;
        }
        r6.add(0, i, 0, i2);
        UserJid userJid = statusPlaybackContactFragment.A0P;
        if (C15380n4.A0L(userJid) && userJid != C29831Uv.A00) {
            r6.add(0, R.id.menuitem_conversations_message_contact, 0, R.string.message);
            r6.add(0, R.id.menuitem_conversations_voice_call_contact, 0, R.string.audio_call);
            r6.add(0, R.id.menuitem_conversations_video_call_contact, 0, R.string.video_call);
            r6.add(0, R.id.menuitem_conversations_contact_info, 0, R.string.view_contact);
        }
        r3.A00 = new AbstractC11640gc() { // from class: X.3On
            @Override // X.AbstractC11640gc
            public final void APH(AnonymousClass07N r4) {
                View$OnClickListenerC55932ju r2 = View$OnClickListenerC55932ju.this;
                ViewTreeObserver viewTreeObserver2 = r2.A00;
                if (viewTreeObserver2 != null) {
                    if (viewTreeObserver2.isAlive()) {
                        r2.A00.removeGlobalOnLayoutListener(r2.A02);
                    }
                    r2.A00 = null;
                }
                StatusPlaybackBaseFragment statusPlaybackBaseFragment2 = r2.A04;
                statusPlaybackBaseFragment2.A07 = false;
                statusPlaybackBaseFragment2.A19();
            }
        };
        r3.A01 = new AbstractC11650gd() { // from class: X.3Oo
            @Override // X.AbstractC11650gd
            public final boolean onMenuItemClick(MenuItem menuItem) {
                String str;
                String str2;
                StatusPlaybackContactFragment statusPlaybackContactFragment2 = (StatusPlaybackContactFragment) StatusPlaybackBaseFragment.this;
                ActivityC000900k A0B = statusPlaybackContactFragment2.A0B();
                if (A0B == null) {
                    return false;
                }
                int itemId = menuItem.getItemId();
                if (itemId == R.id.menuitem_conversations_unmute) {
                    UserJid userJid2 = statusPlaybackContactFragment2.A0P;
                    List list = statusPlaybackContactFragment2.A0b;
                    if (list != null) {
                        str2 = ((AbstractC15340mz) list.get(statusPlaybackContactFragment2.A00)).A0z.A01;
                    } else {
                        str2 = null;
                    }
                    C51122Sx.A01(StatusConfirmUnmuteDialogFragment.A00(userJid2, C12980iv.A0l(statusPlaybackContactFragment2.A00), str2, statusPlaybackContactFragment2.A0a, null), statusPlaybackContactFragment2);
                    return true;
                } else if (itemId == R.id.menuitem_conversations_mute) {
                    UserJid userJid3 = statusPlaybackContactFragment2.A0P;
                    List list2 = statusPlaybackContactFragment2.A0b;
                    if (list2 != null) {
                        str = ((AbstractC15340mz) list2.get(statusPlaybackContactFragment2.A00)).A0z.A01;
                    } else {
                        str = null;
                    }
                    C51122Sx.A01(StatusConfirmMuteDialogFragment.A00(userJid3, C12980iv.A0l(statusPlaybackContactFragment2.A00), str, statusPlaybackContactFragment2.A0a, null), statusPlaybackContactFragment2);
                    return true;
                } else if (itemId == R.id.menuitem_conversations_voice_call_contact) {
                    statusPlaybackContactFragment2.A0Y.A01(A0B, statusPlaybackContactFragment2.A0A.A0B(statusPlaybackContactFragment2.A0P), 22, false);
                    return true;
                } else if (itemId == R.id.menuitem_conversations_video_call_contact) {
                    statusPlaybackContactFragment2.A0Y.A01(A0B, statusPlaybackContactFragment2.A0A.A0B(statusPlaybackContactFragment2.A0P), 22, true);
                    return true;
                } else if (itemId == R.id.menuitem_conversations_contact_info) {
                    C33651en r2 = ((StatusPlaybackBaseFragment) statusPlaybackContactFragment2).A03;
                    AnonymousClass009.A06(r2, "getViewHolder() is accessed before StatusPlaybackBaseFragment#onCreateView()");
                    statusPlaybackContactFragment2.A1N(statusPlaybackContactFragment2.A0A.A0B(statusPlaybackContactFragment2.A0P), r2);
                    return true;
                } else if (itemId != R.id.menuitem_conversations_message_contact) {
                    return true;
                } else {
                    C51122Sx.A00(new C14960mK().A0i(statusPlaybackContactFragment2.A0p(), statusPlaybackContactFragment2.A0P), statusPlaybackContactFragment2);
                    return true;
                }
            }
        };
        r3.A00();
        statusPlaybackBaseFragment.A07 = true;
        statusPlaybackBaseFragment.A19();
    }
}
