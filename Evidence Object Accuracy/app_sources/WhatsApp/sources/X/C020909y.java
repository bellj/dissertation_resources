package X;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;

/* renamed from: X.09y  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C020909y extends Drawable.ConstantState {
    public final Drawable.ConstantState A00;

    public C020909y(Drawable.ConstantState constantState) {
        this.A00 = constantState;
    }

    @Override // android.graphics.drawable.Drawable.ConstantState
    public boolean canApplyTheme() {
        return this.A00.canApplyTheme();
    }

    @Override // android.graphics.drawable.Drawable.ConstantState
    public int getChangingConfigurations() {
        return this.A00.getChangingConfigurations();
    }

    @Override // android.graphics.drawable.Drawable.ConstantState
    public Drawable newDrawable() {
        C013606j r1 = new C013606j();
        ((AbstractC013706k) r1).A00 = this.A00.newDrawable();
        return r1;
    }

    @Override // android.graphics.drawable.Drawable.ConstantState
    public Drawable newDrawable(Resources resources) {
        C013606j r1 = new C013606j();
        ((AbstractC013706k) r1).A00 = this.A00.newDrawable(resources);
        return r1;
    }

    @Override // android.graphics.drawable.Drawable.ConstantState
    public Drawable newDrawable(Resources resources, Resources.Theme theme) {
        C013606j r1 = new C013606j();
        ((AbstractC013706k) r1).A00 = this.A00.newDrawable(resources, theme);
        return r1;
    }
}
