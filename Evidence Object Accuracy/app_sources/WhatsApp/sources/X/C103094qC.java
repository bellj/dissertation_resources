package X;

import android.content.Context;
import com.whatsapp.group.GroupAdminPickerActivity;

/* renamed from: X.4qC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103094qC implements AbstractC009204q {
    public final /* synthetic */ GroupAdminPickerActivity A00;

    public C103094qC(GroupAdminPickerActivity groupAdminPickerActivity) {
        this.A00 = groupAdminPickerActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
