package X;

import android.database.Cursor;
import android.database.DataSetObserver;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.gallery.DocumentsGalleryFragment;
import com.whatsapp.gallery.GalleryFragmentBase;
import com.whatsapp.gallery.LinksGalleryFragment;
import java.io.File;
import java.util.Locale;

/* renamed from: X.2gi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC54432gi extends AnonymousClass02M {
    public int A00 = -1;
    public Cursor A01 = null;
    public DataSetObserver A02 = new C73003fW(this);
    public boolean A03 = false;

    public AbstractC54432gi() {
        A07(true);
        Cursor cursor = this.A01;
        if (cursor != null) {
            cursor.registerDataSetObserver(this.A02);
        }
    }

    @Override // X.AnonymousClass02M
    public long A00(int i) {
        Cursor cursor;
        if (!this.A03 || (cursor = this.A01) == null || !cursor.moveToPosition(i)) {
            return 0;
        }
        return this.A01.getLong(this.A00);
    }

    @Override // X.AnonymousClass02M
    public void A07(boolean z) {
        super.A07(true);
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        Cursor cursor;
        if (!this.A03 || (cursor = this.A01) == null) {
            return 0;
        }
        return cursor.getCount();
    }

    public Cursor A0E(Cursor cursor) {
        boolean z;
        Cursor cursor2 = this.A01;
        if (cursor == cursor2) {
            return null;
        }
        if (cursor2 != null) {
            cursor2.unregisterDataSetObserver(this.A02);
        }
        this.A01 = cursor;
        if (cursor != null) {
            cursor.registerDataSetObserver(this.A02);
            this.A00 = cursor.getColumnIndexOrThrow("_id");
            z = true;
        } else {
            this.A00 = -1;
            z = false;
        }
        this.A03 = z;
        A02();
        return cursor2;
    }

    @Override // X.AnonymousClass02M
    public void ANH(AnonymousClass03U r10, int i) {
        CharSequence A02;
        String str;
        if (!this.A03) {
            throw C12960it.A0U("this should only be called when the cursor is valid");
        } else if (this.A01.moveToPosition(i)) {
            Cursor cursor = this.A01;
            if (!(this instanceof C616131a)) {
                C55102hn r102 = (C55102hn) r10;
                AbstractC16130oV A00 = ((C16520pA) cursor).A00();
                AnonymousClass009.A05(A00);
                C16440p1 r5 = (C16440p1) A00;
                r102.A00 = r5;
                ImageView imageView = r102.A05;
                DocumentsGalleryFragment documentsGalleryFragment = r102.A0B;
                imageView.setImageDrawable(C26511Dt.A02(documentsGalleryFragment.A0p(), r5));
                boolean isEmpty = TextUtils.isEmpty(r5.A15());
                TextView textView = r102.A09;
                if (!isEmpty) {
                    A02 = AnonymousClass3J9.A02(documentsGalleryFragment.A0p(), ((GalleryFragmentBase) documentsGalleryFragment).A05, r5.A15(), ((AbstractC13890kV) C13010iy.A01(documentsGalleryFragment)).AGT());
                } else if (!TextUtils.isEmpty(r5.A16())) {
                    A02 = C14350lI.A08(r5.A16());
                } else {
                    A02 = documentsGalleryFragment.A0I(R.string.untitled_document);
                }
                textView.setText(A02);
                File file = AbstractC15340mz.A00(r5).A0F;
                TextView textView2 = r102.A08;
                if (file != null) {
                    textView2.setText(C44891zj.A03(((GalleryFragmentBase) documentsGalleryFragment).A05, file.length()));
                    textView2.setVisibility(0);
                    r102.A03.setVisibility(0);
                } else {
                    textView2.setVisibility(8);
                    r102.A03.setVisibility(8);
                }
                if (r5.A00 != 0) {
                    TextView textView3 = r102.A07;
                    textView3.setVisibility(0);
                    r102.A01.setVisibility(0);
                    textView3.setText(C26511Dt.A05(((GalleryFragmentBase) documentsGalleryFragment).A05, r5));
                } else {
                    r102.A07.setVisibility(8);
                    r102.A01.setVisibility(8);
                }
                String A002 = AnonymousClass14P.A00(((AbstractC16130oV) r5).A06);
                Locale locale = Locale.US;
                String upperCase = A002.toUpperCase(locale);
                if (TextUtils.isEmpty(upperCase) && !TextUtils.isEmpty(r5.A16())) {
                    String A16 = r5.A16();
                    AnonymousClass009.A05(A16);
                    upperCase = C14350lI.A07(A16).toUpperCase(locale);
                }
                r102.A0A.setText(upperCase);
                TextView textView4 = r102.A06;
                if (file != null) {
                    textView4.setText(C38131nZ.A0A(((GalleryFragmentBase) documentsGalleryFragment).A05, r5.A0I, false));
                    str = C38131nZ.A0A(((GalleryFragmentBase) documentsGalleryFragment).A05, r5.A0I, true);
                } else {
                    str = "";
                    textView4.setText(str);
                }
                textView4.setContentDescription(str);
                View view = r102.A04;
                View view2 = r102.A02;
                if (r5.A0v) {
                    view2.setVisibility(8);
                    view.setVisibility(0);
                } else {
                    view2.setVisibility(8);
                    view.setVisibility(8);
                }
                boolean AJm = ((AbstractC13890kV) C13010iy.A01(documentsGalleryFragment)).AJm(r5);
                View view3 = r102.A0H;
                if (AJm) {
                    C12970iu.A18(documentsGalleryFragment.A01(), view3, R.color.multi_selection);
                    view3.setSelected(true);
                    return;
                }
                view3.setBackgroundResource(R.drawable.selector_orange_gradient);
                view3.setSelected(false);
                return;
            }
            int A06 = C12990iw.A06(cursor, "link_index");
            LinksGalleryFragment linksGalleryFragment = ((C616131a) this).A00;
            C15650ng r0 = ((GalleryFragmentBase) linksGalleryFragment).A06;
            ((C55162ht) r10).A08(r0.A0K.A02(cursor, ((GalleryFragmentBase) linksGalleryFragment).A0D, false, true), A06);
        } else {
            throw C12960it.A0U(C12960it.A0W(i, "couldn't move cursor to position "));
        }
    }
}
