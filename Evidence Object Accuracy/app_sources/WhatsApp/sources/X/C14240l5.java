package X;

/* renamed from: X.0l5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14240l5 {
    public AnonymousClass4Yz A00;
    public C90764Pd A01;
    public AbstractC17450qp A02;

    public C14240l5(AnonymousClass4Yz r3, C90764Pd r4, AbstractC17450qp r5) {
        if (!(r5 instanceof C67943Tm)) {
            this.A02 = new C67943Tm(r5);
            this.A01 = r4;
            this.A00 = r3;
            return;
        }
        throw new RuntimeException("passed extensions that were already chained");
    }
}
