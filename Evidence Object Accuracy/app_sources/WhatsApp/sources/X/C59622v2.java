package X;

/* renamed from: X.2v2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C59622v2 extends C37171lc {
    public final C89344Jp A00;

    @Override // X.C37171lc
    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof C59622v2) && C16700pc.A0O(this.A00, ((C59622v2) obj).A00));
    }

    @Override // X.C37171lc
    public int hashCode() {
        return this.A00.hashCode();
    }

    public C59622v2(C89344Jp r2) {
        super(AnonymousClass39o.A0N);
        this.A00 = r2;
    }

    public String toString() {
        return C12960it.A0a(this.A00, C12960it.A0k("NearbyBusinessLocationRequestItem(nearbyLocationRequestClickListener="));
    }
}
