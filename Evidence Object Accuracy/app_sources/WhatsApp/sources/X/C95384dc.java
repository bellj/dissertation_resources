package X;

import java.util.Arrays;

/* renamed from: X.4dc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C95384dc {
    public static final C08960c8 A00;
    public static final char[] A01 = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    static {
        byte[] copyOf = Arrays.copyOf(new byte[0], 0);
        C16700pc.A0A(copyOf);
        A00 = new C08960c8(copyOf);
    }

    public static final String A00(C08960c8 r8) {
        byte[] bArr = r8.data;
        int length = bArr.length;
        char[] cArr = new char[length << 1];
        int i = 0;
        for (byte b : bArr) {
            int i2 = i + 1;
            char[] cArr2 = A01;
            cArr[i] = cArr2[(b >> 4) & 15];
            i = i2 + 1;
            cArr[i2] = cArr2[b & 15];
        }
        return new String(cArr);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:133:0x01ce, code lost:
        if (r9 < 65536) goto L_0x01d0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:139:0x01dd, code lost:
        if (r9 < 65536) goto L_0x01df;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:0x01e9, code lost:
        if (r9 < 65536) goto L_0x01eb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00c6, code lost:
        if (r9 < 65536) goto L_0x00c8;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.lang.String A01(X.C08960c8 r16) {
        /*
        // Method dump skipped, instructions count: 532
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95384dc.A01(X.0c8):java.lang.String");
    }

    public static final C08960c8 A02() {
        return A00;
    }

    public static final C08960c8 A03(String str) {
        byte[] bytes = str.getBytes(C88844Hi.A05);
        C16700pc.A0A(bytes);
        C08960c8 r0 = new C08960c8(bytes);
        r0.A01 = str;
        return r0;
    }
}
