package X;

import android.content.Context;

/* renamed from: X.2kd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56282kd extends AnonymousClass2RP {
    public static final AbstractC77683ng A00;
    public static final AnonymousClass4DN A01;
    public static final AnonymousClass1UE A02;

    static {
        AnonymousClass4DN r3 = new AnonymousClass4DN();
        A01 = r3;
        C77563nU r2 = new C77563nU();
        A00 = r2;
        A02 = new AnonymousClass1UE(r2, r3, "SmsRetriever.API");
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C56282kd(android.app.Activity r9) {
        /*
            r8 = this;
            X.1UE r6 = X.C56282kd.A02
            X.4yp r2 = new X.4yp
            r2.<init>()
            r5 = 0
            r3 = r9
            android.os.Looper r1 = r9.getMainLooper()
            java.lang.String r0 = "Looper must not be null."
            X.C13020j0.A02(r1, r0)
            if (r1 != 0) goto L_0x0018
            android.os.Looper r1 = android.os.Looper.getMainLooper()
        L_0x0018:
            X.4a7 r7 = new X.4a7
            r7.<init>(r1, r2)
            r2 = r8
            r4 = r9
            r2.<init>(r3, r4, r5, r6, r7)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C56282kd.<init>(android.app.Activity):void");
    }

    public C56282kd(Context context) {
        super(context, null, A02, new C108294yp());
    }
}
