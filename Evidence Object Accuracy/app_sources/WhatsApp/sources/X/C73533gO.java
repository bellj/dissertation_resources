package X;

import android.text.style.ForegroundColorSpan;

/* renamed from: X.3gO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C73533gO extends ForegroundColorSpan implements AbstractC115425Rm {
    public final boolean A00;

    public C73533gO(int i, boolean z) {
        super(i);
        this.A00 = z;
    }
}
