package X;

import java.security.SecureRandom;

/* renamed from: X.5GG  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5GG implements AnonymousClass5Wv {
    public SecureRandom A00;

    @Override // X.AnonymousClass5Wv
    public int A5m(byte[] bArr, int i) {
        int length = bArr.length;
        byte b = (byte) (length - i);
        while (i < length - 1) {
            bArr[i] = (byte) this.A00.nextInt();
            i++;
        }
        bArr[i] = b;
        return b;
    }

    @Override // X.AnonymousClass5Wv
    public int AYq(byte[] bArr) {
        int length = bArr.length;
        int i = bArr[length - 1] & 255;
        if (i <= length) {
            return i;
        }
        throw new C114965Nt("pad block corrupted");
    }

    @Override // X.AnonymousClass5Wv
    public void AIb(SecureRandom secureRandom) {
        if (secureRandom == null) {
            secureRandom = C95234dM.A00();
        }
        this.A00 = secureRandom;
    }
}
