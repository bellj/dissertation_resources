package X;

import android.os.Handler;

/* renamed from: X.2JT  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2JT {
    public AnonymousClass2JS A00;
    public String A01;
    public final C15610nY A02;
    public final AnonymousClass2JU A03;
    public final AbstractC15590nW A04;
    public final AbstractC14440lR A05;

    public AnonymousClass2JT(C15610nY r1, AnonymousClass2JU r2, AbstractC15590nW r3, AbstractC14440lR r4) {
        this.A05 = r4;
        this.A02 = r1;
        this.A04 = r3;
        this.A03 = r2;
    }

    public void A00() {
        AnonymousClass2JS r2 = this.A00;
        boolean z = true;
        if (r2 != null) {
            z = false;
            r2.A03(false);
            Handler handler = r2.A00;
            if (handler != null) {
                handler.removeCallbacks(r2.A01);
            }
            r2.A00 = null;
            r2.A01 = null;
            this.A00 = null;
        }
        AnonymousClass2JS r22 = new AnonymousClass2JS(this.A02, this, this.A04, z);
        this.A00 = r22;
        this.A05.Aaz(r22, new Void[0]);
    }
}
