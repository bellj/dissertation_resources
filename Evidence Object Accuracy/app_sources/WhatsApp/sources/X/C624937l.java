package X;

import com.whatsapp.invites.ViewGroupInviteActivity;
import java.lang.ref.WeakReference;

/* renamed from: X.37l  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C624937l extends AbstractC16350or {
    public int A00;
    public C15580nU A01;
    public final C20660w7 A02;
    public final C32511cH A03;
    public final WeakReference A04;

    public C624937l(ViewGroupInviteActivity viewGroupInviteActivity, C20660w7 r3, C32511cH r4) {
        this.A02 = r3;
        this.A04 = C12970iu.A10(viewGroupInviteActivity);
        this.A03 = r4;
    }
}
