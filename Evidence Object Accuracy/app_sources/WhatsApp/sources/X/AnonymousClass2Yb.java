package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;
import android.view.animation.Interpolator;
import com.facebook.redex.IDxLAdapterShape0S0100000_1_I1;
import com.whatsapp.R;
import com.whatsapp.settings.chat.wallpaper.SolidColorWallpaperPreview;

/* renamed from: X.2Yb  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Yb extends AnimatorListenerAdapter {
    public final /* synthetic */ View A00;
    public final /* synthetic */ View A01;
    public final /* synthetic */ Interpolator A02;
    public final /* synthetic */ ViewTreeObserver$OnPreDrawListenerC66493Nu A03;

    public AnonymousClass2Yb(View view, View view2, Interpolator interpolator, ViewTreeObserver$OnPreDrawListenerC66493Nu r4) {
        this.A03 = r4;
        this.A02 = interpolator;
        this.A00 = view;
        this.A01 = view2;
    }

    public final void A00(View view) {
        if (view != null) {
            view.animate().setDuration(250).alpha(1.0f).translationY(0.0f).setInterpolator(this.A02).setListener(new IDxLAdapterShape0S0100000_1_I1(this, 4));
        }
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
        super.onAnimationCancel(animator);
        SolidColorWallpaperPreview solidColorWallpaperPreview = this.A03.A04;
        solidColorWallpaperPreview.A05.setBackgroundColor(solidColorWallpaperPreview.getResources().getColor(R.color.white));
        solidColorWallpaperPreview.A0C = false;
        solidColorWallpaperPreview.A09.setScrollEnabled(true);
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        super.onAnimationEnd(animator);
        SolidColorWallpaperPreview solidColorWallpaperPreview = this.A03.A04;
        solidColorWallpaperPreview.A05.setBackgroundColor(solidColorWallpaperPreview.getResources().getColor(R.color.white));
        solidColorWallpaperPreview.A06.animate().setDuration(250).alpha(1.0f).setInterpolator(this.A02);
        A00(this.A00);
        A00(this.A01);
    }
}
