package X;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;

/* renamed from: X.0CB  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0CB extends C015807m {
    public boolean A00 = true;

    public AnonymousClass0CB(Drawable drawable) {
        super(drawable);
    }

    @Override // X.C015807m, android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        if (this.A00) {
            super.draw(canvas);
        }
    }

    @Override // X.C015807m, android.graphics.drawable.Drawable
    public void setHotspot(float f, float f2) {
        if (this.A00) {
            super.setHotspot(f, f2);
        }
    }

    @Override // X.C015807m, android.graphics.drawable.Drawable
    public void setHotspotBounds(int i, int i2, int i3, int i4) {
        if (this.A00) {
            super.setHotspotBounds(i, i2, i3, i4);
        }
    }

    @Override // X.C015807m, android.graphics.drawable.Drawable
    public boolean setState(int[] iArr) {
        if (this.A00) {
            return super.A00.setState(iArr);
        }
        return false;
    }

    @Override // X.C015807m, android.graphics.drawable.Drawable
    public boolean setVisible(boolean z, boolean z2) {
        if (this.A00) {
            return super.setVisible(z, z2);
        }
        return false;
    }
}
