package X;

/* renamed from: X.1ul  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C42161ul {
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:195:0x00f1 */
    /* JADX WARN: Type inference failed for: r6v1, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r6v2 */
    /* JADX WARN: Type inference failed for: r6v7, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x0271, code lost:
        if ("catalog_exists".equals(r1.A0I("status", null)) == false) goto L_0x0273;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x02a6, code lost:
        if (r1 == false) goto L_0x02a8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x02c0, code lost:
        if (r1 == false) goto L_0x02c2;
     */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x0263  */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x027e  */
    /* JADX WARNING: Removed duplicated region for block: B:139:0x02de  */
    /* JADX WARNING: Removed duplicated region for block: B:142:0x02ed  */
    /* JADX WARNING: Removed duplicated region for block: B:145:0x02f9  */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x0308  */
    /* JADX WARNING: Removed duplicated region for block: B:156:0x0337  */
    /* JADX WARNING: Removed duplicated region for block: B:170:0x0376  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C30141Wg A00(com.whatsapp.jid.UserJid r10, X.AnonymousClass1V8 r11) {
        /*
        // Method dump skipped, instructions count: 1004
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C42161ul.A00(com.whatsapp.jid.UserJid, X.1V8):X.1Wg");
    }

    public static Boolean A01(AnonymousClass1V8 r0, String str) {
        AnonymousClass1V8 A0E = r0.A0E(str);
        if (A0E == null || A0E.A0G() == null) {
            return null;
        }
        return Boolean.valueOf(A0E.A0G().trim().equalsIgnoreCase("true"));
    }

    public static String A02(AnonymousClass1V8 r0, String str) {
        AnonymousClass1V8 A0E = r0.A0E(str);
        if (A0E != null) {
            return A0E.A0G();
        }
        return null;
    }
}
