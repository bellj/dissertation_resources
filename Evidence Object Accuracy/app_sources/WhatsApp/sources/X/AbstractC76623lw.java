package X;

import android.util.Pair;
import com.google.android.exoplayer2.Timeline;
import java.util.Arrays;

/* renamed from: X.3lw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC76623lw extends Timeline {
    public final int A00;
    public final AnonymousClass5XM A01;

    public AbstractC76623lw(AnonymousClass5XM r2) {
        this.A01 = r2;
        this.A00 = r2.getLength();
    }

    @Override // com.google.android.exoplayer2.Timeline
    public final C94404bl A0B(C94404bl r6, int i, long j) {
        int i2;
        Object valueOf;
        boolean z = this instanceof C76493lj;
        if (!z) {
            int[] iArr = ((C76503lk) this).A04;
            int i3 = i + 1;
            i2 = Arrays.binarySearch(iArr, i3);
            if (i2 >= 0) {
                do {
                    i2--;
                    if (i2 < 0) {
                        break;
                    }
                } while (iArr[i2] == i3);
            } else {
                i2 = -(i2 + 2);
            }
        } else {
            i2 = i / ((C76493lj) this).A01;
        }
        int A0F = A0F(i2);
        int A0E = A0E(i2);
        A0G(i2).A0B(r6, i - A0F, j);
        if (!z) {
            valueOf = ((C76503lk) this).A06[i2];
        } else {
            valueOf = Integer.valueOf(i2);
        }
        if (!C94404bl.A0F.equals(r6.A09)) {
            valueOf = Pair.create(valueOf, r6.A09);
        }
        r6.A09 = valueOf;
        r6.A00 += A0E;
        r6.A01 += A0E;
        return r6;
    }

    public int A0E(int i) {
        if (!(this instanceof C76493lj)) {
            return ((C76503lk) this).A03[i];
        }
        return i * ((C76493lj) this).A00;
    }

    public int A0F(int i) {
        if (!(this instanceof C76493lj)) {
            return ((C76503lk) this).A04[i];
        }
        return i * ((C76493lj) this).A01;
    }

    public Timeline A0G(int i) {
        if (!(this instanceof C76493lj)) {
            return ((C76503lk) this).A05[i];
        }
        return ((C76493lj) this).A03;
    }
}
