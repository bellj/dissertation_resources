package X;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.util.Log;
import androidx.room.IMultiInstanceInvalidationCallback;
import androidx.room.MultiInstanceInvalidationService;
import java.util.HashMap;

/* renamed from: X.0AL  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0AL extends Binder implements IInterface {
    public final /* synthetic */ MultiInstanceInvalidationService A00;

    @Override // android.os.IInterface
    public IBinder asBinder() {
        return this;
    }

    public AnonymousClass0AL(MultiInstanceInvalidationService multiInstanceInvalidationService) {
        this.A00 = multiInstanceInvalidationService;
        attachInterface(this, "androidx.room.IMultiInstanceInvalidationService");
    }

    @Override // android.os.Binder
    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        IInterface queryLocalInterface;
        int i3;
        IInterface queryLocalInterface2;
        if (i == 1) {
            parcel.enforceInterface("androidx.room.IMultiInstanceInvalidationService");
            IBinder readStrongBinder = parcel.readStrongBinder();
            if (readStrongBinder == null) {
                queryLocalInterface = null;
            } else {
                queryLocalInterface = readStrongBinder.queryLocalInterface("androidx.room.IMultiInstanceInvalidationCallback");
                if (queryLocalInterface == null || !(queryLocalInterface instanceof IMultiInstanceInvalidationCallback)) {
                    queryLocalInterface = new AnonymousClass0ZD(readStrongBinder);
                }
            }
            String readString = parcel.readString();
            if (readString != null) {
                MultiInstanceInvalidationService multiInstanceInvalidationService = this.A00;
                RemoteCallbackList remoteCallbackList = multiInstanceInvalidationService.A01;
                synchronized (remoteCallbackList) {
                    i3 = multiInstanceInvalidationService.A00 + 1;
                    multiInstanceInvalidationService.A00 = i3;
                    Integer valueOf = Integer.valueOf(i3);
                    if (remoteCallbackList.register(queryLocalInterface, valueOf)) {
                        multiInstanceInvalidationService.A03.put(valueOf, readString);
                    } else {
                        multiInstanceInvalidationService.A00--;
                    }
                }
                parcel2.writeNoException();
                parcel2.writeInt(i3);
                return true;
            }
            i3 = 0;
            parcel2.writeNoException();
            parcel2.writeInt(i3);
            return true;
        } else if (i == 2) {
            parcel.enforceInterface("androidx.room.IMultiInstanceInvalidationService");
            IBinder readStrongBinder2 = parcel.readStrongBinder();
            if (readStrongBinder2 == null) {
                queryLocalInterface2 = null;
            } else {
                queryLocalInterface2 = readStrongBinder2.queryLocalInterface("androidx.room.IMultiInstanceInvalidationCallback");
                if (queryLocalInterface2 == null || !(queryLocalInterface2 instanceof IMultiInstanceInvalidationCallback)) {
                    queryLocalInterface2 = new AnonymousClass0ZD(readStrongBinder2);
                }
            }
            int readInt = parcel.readInt();
            MultiInstanceInvalidationService multiInstanceInvalidationService2 = this.A00;
            RemoteCallbackList remoteCallbackList2 = multiInstanceInvalidationService2.A01;
            synchronized (remoteCallbackList2) {
                remoteCallbackList2.unregister(queryLocalInterface2);
                multiInstanceInvalidationService2.A03.remove(Integer.valueOf(readInt));
            }
            parcel2.writeNoException();
            return true;
        } else if (i == 3) {
            parcel.enforceInterface("androidx.room.IMultiInstanceInvalidationService");
            int readInt2 = parcel.readInt();
            String[] createStringArray = parcel.createStringArray();
            MultiInstanceInvalidationService multiInstanceInvalidationService3 = this.A00;
            RemoteCallbackList remoteCallbackList3 = multiInstanceInvalidationService3.A01;
            synchronized (remoteCallbackList3) {
                HashMap hashMap = multiInstanceInvalidationService3.A03;
                String str = (String) hashMap.get(Integer.valueOf(readInt2));
                if (str == null) {
                    Log.w("ROOM", "Remote invalidation client ID not registered");
                } else {
                    int beginBroadcast = remoteCallbackList3.beginBroadcast();
                    for (int i4 = 0; i4 < beginBroadcast; i4++) {
                        int intValue = ((Integer) remoteCallbackList3.getBroadcastCookie(i4)).intValue();
                        String str2 = (String) hashMap.get(Integer.valueOf(intValue));
                        if (readInt2 != intValue && str.equals(str2)) {
                            try {
                                AnonymousClass0ZD r1 = (AnonymousClass0ZD) ((IMultiInstanceInvalidationCallback) remoteCallbackList3.getBroadcastItem(i4));
                                Parcel obtain = Parcel.obtain();
                                try {
                                    obtain.writeInterfaceToken("androidx.room.IMultiInstanceInvalidationCallback");
                                    obtain.writeStringArray(createStringArray);
                                    r1.A00.transact(1, obtain, null, 1);
                                    obtain.recycle();
                                } catch (Throwable th) {
                                    obtain.recycle();
                                    throw th;
                                    break;
                                }
                            } catch (RemoteException e) {
                                Log.w("ROOM", "Error invoking a remote callback", e);
                            }
                        }
                    }
                    remoteCallbackList3.finishBroadcast();
                }
            }
            return true;
        } else if (i != 1598968902) {
            return super.onTransact(i, parcel, parcel2, i2);
        } else {
            parcel2.writeString("androidx.room.IMultiInstanceInvalidationService");
            return true;
        }
    }
}
