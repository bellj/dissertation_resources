package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0101000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S1200000_I0;
import com.facebook.redex.RunnableBRunnable0Shape7S0100000_I0_7;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.util.Log;

/* renamed from: X.1jU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C36201jU implements AbstractC21730xt {
    public final C14900mE A00;
    public final C35871iu A01;
    public final C17220qS A02;

    public C36201jU(C14900mE r1, C35871iu r2, C17220qS r3) {
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        Log.e("ChatSupportTicketProtocolHelper/onDeliveryFailure");
        this.A00.A0I(new RunnableBRunnable0Shape7S0100000_I0_7(this.A01, 14));
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r5, String str) {
        StringBuilder sb = new StringBuilder("ChatSupportTicketProtocolHelper/onError: error response:");
        sb.append(r5);
        Log.e(sb.toString());
        AnonymousClass1V8 A0E = r5.A0E("error");
        if (A0E != null) {
            this.A00.A0I(new RunnableBRunnable0Shape0S0101000_I0(this, A0E.A05("code", 0), 10));
        }
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r6, String str) {
        String str2;
        AnonymousClass1V8 A0E = r6.A0E("response");
        Jid jid = null;
        if (A0E != null) {
            AnonymousClass1V8 A0E2 = A0E.A0E("ticket_id");
            if (A0E2 != null) {
                str2 = A0E2.A0G();
            } else {
                str2 = null;
            }
            AnonymousClass1V8 A0E3 = A0E.A0E("group_jid");
            if (A0E3 != null) {
                try {
                    String A0G = A0E3.A0G();
                    Jid jid2 = Jid.get(A0G);
                    if (jid2 instanceof GroupJid) {
                        jid = jid2;
                    } else {
                        throw new AnonymousClass1MW(A0G);
                    }
                } catch (AnonymousClass1MW unused) {
                    Log.e("ChatSupportTicketProtocolHelper/onSuccess called with invalid jid");
                }
            }
            if (str2 != null) {
                StringBuilder sb = new StringBuilder("ChatSupportTicketProtocolHelper/onSuccess called, ticketId=");
                sb.append(str2);
                Log.i(sb.toString());
                this.A00.A0I(new RunnableBRunnable0Shape0S1200000_I0(this, jid, str2, 20));
                return;
            }
        }
        Log.e("ChatSupportTicketProtocolHelper/onSuccess called but ticketId is null, posting an error");
        this.A00.A0I(new RunnableBRunnable0Shape7S0100000_I0_7(this, 15));
    }
}
