package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4lO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C100114lO implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new AnonymousClass1ZR(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new AnonymousClass1ZR[i];
    }
}
