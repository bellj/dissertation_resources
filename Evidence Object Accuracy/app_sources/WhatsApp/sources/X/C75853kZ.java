package X;

import android.graphics.Bitmap;

/* renamed from: X.3kZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75853kZ extends AbstractC75863ka {
    public C08870bz A00;
    public final C94234bT A01;
    public volatile Bitmap A02;

    public C75853kZ(C08870bz r2, C94234bT r3) {
        C08870bz A03 = r2.A03();
        this.A00 = A03;
        this.A02 = (Bitmap) A03.A04();
        this.A01 = r3;
    }

    @Override // X.AbstractC08840bw
    public int A00() {
        return C94594c9.A01(this.A02);
    }

    @Override // X.AbstractC08840bw
    public synchronized boolean A01() {
        return C12980iv.A1X(this.A00);
    }

    @Override // X.AbstractC08840bw, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        C08870bz r1;
        synchronized (this) {
            r1 = this.A00;
            this.A00 = null;
            this.A02 = null;
        }
        if (r1 != null) {
            r1.close();
        }
    }
}
