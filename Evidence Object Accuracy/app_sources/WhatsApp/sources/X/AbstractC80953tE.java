package X;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

/* renamed from: X.3tE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC80953tE extends AbstractC80923tB implements Serializable {
    public static final long serialVersionUID = 0;
    public final transient AbstractC17190qP map;
    public final transient int size;

    public AbstractC80953tE(AbstractC17190qP r1, int i) {
        this.map = r1;
        this.size = i;
    }

    @Override // X.AnonymousClass51S, X.AnonymousClass5XH
    public AbstractC17190qP asMap() {
        return this.map;
    }

    @Override // X.AnonymousClass5XH
    @Deprecated
    public final void clear() {
        throw C12970iu.A0z();
    }

    @Override // X.AnonymousClass51S
    public boolean containsValue(Object obj) {
        return obj != null && super.containsValue(obj);
    }

    @Override // X.AnonymousClass51S
    public Map createAsMap() {
        throw new AssertionError("should never be called");
    }

    @Override // X.AnonymousClass51S
    public Set createKeySet() {
        throw new AssertionError("unreachable");
    }

    @Override // X.AnonymousClass51S
    public AbstractC17950rf createValues() {
        return new C81013tK(this);
    }

    @Override // X.AnonymousClass51S
    public AbstractC17940re keySet() {
        return this.map.keySet();
    }

    @Override // X.AnonymousClass51S, X.AnonymousClass5XH
    @Deprecated
    public final boolean put(Object obj, Object obj2) {
        throw C12970iu.A0z();
    }

    @Override // X.AnonymousClass5XH
    public int size() {
        return this.size;
    }

    @Override // X.AnonymousClass51S
    public AnonymousClass1I5 valueIterator() {
        return new C81313to(this);
    }

    @Override // X.AnonymousClass51S, X.AnonymousClass5XH
    public AbstractC17950rf values() {
        return (AbstractC17950rf) super.values();
    }
}
