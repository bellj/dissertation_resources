package X;

/* renamed from: X.0Pc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05320Pc {
    public Object A00;
    public Object A01;

    public boolean equals(Object obj) {
        if (!(obj instanceof AnonymousClass01T)) {
            return false;
        }
        AnonymousClass01T r4 = (AnonymousClass01T) obj;
        Object obj2 = r4.A00;
        Object obj3 = this.A00;
        if (obj2 != obj3 && (obj2 == null || !obj2.equals(obj3))) {
            return false;
        }
        Object obj4 = r4.A01;
        Object obj5 = this.A01;
        if (obj4 == obj5 || (obj4 != null && obj4.equals(obj5))) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        Object obj = this.A00;
        int i = 0;
        int hashCode = obj == null ? 0 : obj.hashCode();
        Object obj2 = this.A01;
        if (obj2 != null) {
            i = obj2.hashCode();
        }
        return hashCode ^ i;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Pair{");
        sb.append(String.valueOf(this.A00));
        sb.append(" ");
        sb.append(String.valueOf(this.A01));
        sb.append("}");
        return sb.toString();
    }
}
