package X;

import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

/* renamed from: X.16r  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C247416r implements AbstractC21730xt {
    public AnonymousClass1WT A00;
    public final AnonymousClass018 A01;
    public final C17220qS A02;

    public C247416r(AnonymousClass018 r2, C17220qS r3) {
        C16700pc.A0E(r3, 1);
        C16700pc.A0E(r2, 2);
        this.A02 = r3;
        this.A01 = r2;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        C16700pc.A0E(str, 0);
        Log.e(C16700pc.A08("GetCommerceTranslationsMetadataProtocolHelper/onDeliveryFailure: Network failed  while sending the payload: ", str));
        AnonymousClass1WT r0 = this.A00;
        if (r0 == null) {
            C16700pc.A0K("listener");
            throw new RuntimeException("Redex: Unreachable code after no-return invoke");
        } else {
            r0.A00.A07.set(false);
        }
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r4, String str) {
        C16700pc.A0E(r4, 1);
        Log.e("GetCommerceTranslationsMetadataProtocolHelper/response-error");
        AnonymousClass1V8 A0E = r4.A0E("error");
        if (A0E != null) {
            A0E.A05("code", 0);
        }
        AnonymousClass1WT r0 = this.A00;
        if (r0 == null) {
            C16700pc.A0K("listener");
            throw new RuntimeException("Redex: Unreachable code after no-return invoke");
        } else {
            r0.A00.A07.set(false);
        }
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r13, String str) {
        String str2;
        AnonymousClass1V8 A0E;
        AnonymousClass1V8[] r9;
        AnonymousClass1V8 A0E2;
        String A0I;
        Long A0N;
        AnonymousClass1V8 A0E3;
        C16700pc.A0E(r13, 1);
        AnonymousClass1V8 A0E4 = r13.A0E("commerce_metadata");
        if (A0E4 == null || (A0E3 = A0E4.A0E("translations")) == null || (str2 = A0E3.A0I("locale", null)) == null) {
            str2 = "";
        }
        int i = 0;
        boolean z = false;
        if (str2.length() == 0) {
            z = true;
        }
        if (z) {
            Log.e("GetCommerceTranslationsMetadataProtocolHelper/onSuccess can not find locale value in response!");
            AnonymousClass1WT r0 = this.A00;
            if (r0 == null) {
                C16700pc.A0K("listener");
                throw new RuntimeException("Redex: Unreachable code after no-return invoke");
            }
            r0.A00.A07.set(false);
        } else {
            long time = (new Date().getTime() / 1000) + 86400000;
            if (!(A0E4 == null || (A0E2 = A0E4.A0E("translations")) == null || (A0I = A0E2.A0I("expires_at", null)) == null || (A0N = AnonymousClass1WU.A0N(A0I)) == null)) {
                time = A0N.longValue();
            }
            HashMap hashMap = new HashMap();
            if (!(A0E4 == null || (A0E = A0E4.A0E("translations")) == null || (r9 = A0E.A03) == null)) {
                ArrayList arrayList = new ArrayList();
                int length = r9.length;
                while (i < length) {
                    AnonymousClass1V8 r4 = r9[i];
                    i++;
                    if (C16700pc.A0O(r4.A00, "string")) {
                        arrayList.add(r4);
                    }
                }
                ArrayList arrayList2 = new ArrayList();
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    AnonymousClass1V8 r7 = (AnonymousClass1V8) it.next();
                    if (!(r7.A0I("name", null) == null || r7.A0I("value", null) == null)) {
                        String A0I2 = r7.A0I("name", null);
                        C16700pc.A0C(A0I2);
                        C16700pc.A0B(A0I2);
                        String A0I3 = r7.A0I("value", null);
                        C16700pc.A0C(A0I3);
                        C16700pc.A0B(A0I3);
                        hashMap.put(A0I2, A0I3);
                    }
                    arrayList2.add(AnonymousClass1WZ.A00);
                }
            }
            AnonymousClass1WT r02 = this.A00;
            if (r02 == null) {
                C16700pc.A0K("listener");
                throw new RuntimeException("Redex: Unreachable code after no-return invoke");
            }
            C30081Wa r3 = new C30081Wa(str2, hashMap, time);
            C247116o r1 = r02.A00;
            r1.A07.set(false);
            C14820m6 r6 = r1.A04;
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("locale", r3.A01);
            jSONObject.put("expiresAt", r3.A00);
            JSONArray jSONArray = new JSONArray();
            for (Map.Entry entry : r3.A02.entrySet()) {
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put("name", entry.getKey());
                jSONObject2.put("value", entry.getValue());
                jSONArray.put(jSONObject2);
            }
            jSONObject.put("strings", jSONArray);
            r6.A00.edit().putString("commerce_metadata_tanslations", jSONObject.toString()).apply();
        }
    }
}
