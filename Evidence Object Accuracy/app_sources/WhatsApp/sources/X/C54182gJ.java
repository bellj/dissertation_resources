package X;

import android.view.ViewGroup;
import com.facebook.redex.IDxObserverShape4S0100000_2_I1;
import com.whatsapp.R;

/* renamed from: X.2gJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54182gJ extends AnonymousClass02M {
    public final C91574Sg A00;

    public C54182gJ(AnonymousClass02K r4) {
        this.A00 = new C91574Sg(new AnonymousClass0SC(r4).A00(), new AnonymousClass0Z2(this));
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        C71203cY r0 = this.A00.A01;
        if (r0 == null) {
            r0 = C71203cY.A01;
        }
        return r0.A00.size();
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r4, int i) {
        C54942hX r42 = (C54942hX) r4;
        C71203cY r0 = this.A00.A01;
        if (r0 == null) {
            r0 = C71203cY.A01;
        }
        AnonymousClass4QG r2 = (AnonymousClass4QG) r0.A00.get(i);
        r42.A00 = r2;
        r42.A02.setText(r2.A02.A00);
        r42.A01.setChecked(r2.A00);
        r2.A01.A08(new IDxObserverShape4S0100000_2_I1(r42, 42));
    }

    @Override // X.AnonymousClass02M
    public AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        return new C54942hX(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.icebreaker_questions_item_view));
    }
}
