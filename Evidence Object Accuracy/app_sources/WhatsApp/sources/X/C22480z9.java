package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.jobqueue.job.SendPaymentInviteSetupJob;
import java.util.Map;

/* renamed from: X.0z9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22480z9 {
    public final C20670w8 A00;
    public final C14830m7 A01;
    public final C15650ng A02;
    public final C18600si A03;
    public final C18610sj A04;
    public final C22710zW A05;
    public final C17070qD A06;
    public final C22140ya A07;

    public C22480z9(C20670w8 r1, C14830m7 r2, C15650ng r3, C18600si r4, C18610sj r5, C22710zW r6, C17070qD r7, C22140ya r8) {
        this.A01 = r2;
        this.A00 = r1;
        this.A06 = r7;
        this.A02 = r3;
        this.A03 = r4;
        this.A07 = r8;
        this.A04 = r5;
        this.A05 = r6;
    }

    public void A00(UserJid userJid, int i, boolean z) {
        AbstractC30571Xy r2;
        C18600si r3 = this.A03;
        for (UserJid userJid2 : r3.A07(r3.A01().getString("payments_inviter_jids_with_expiry", "")).keySet()) {
            A01(userJid2, i, userJid2.equals(userJid));
            C22140ya r22 = this.A07;
            long A00 = this.A01.A00();
            boolean equals = userJid2.equals(userJid);
            AnonymousClass1IS A02 = r22.A03.A02(userJid2, true);
            if (z) {
                r2 = new C32191bl(A02, A00);
                r2.A00 = i;
                r2.A01 = equals;
            } else {
                r2 = new C32281bu(A02, A00);
                r2.A00 = i;
                r2.A01 = equals;
            }
            this.A02.A0Z(r2, 16);
        }
    }

    public final void A01(UserJid userJid, int i, boolean z) {
        this.A00.A00(new SendPaymentInviteSetupJob(userJid, i, z));
        C18600si r4 = this.A03;
        Map A07 = r4.A07(r4.A01().getString("payments_inviter_jids_with_expiry", ""));
        A07.remove(userJid);
        r4.A01().edit().putString("payments_inviter_jids_with_expiry", C18600si.A00(A07)).apply();
    }
}
