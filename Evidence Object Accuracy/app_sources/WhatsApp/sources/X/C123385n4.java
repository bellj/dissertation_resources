package X;

import android.content.Context;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.5n4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123385n4 extends AbstractC118045bB {
    public C127025tp A00;
    public C122845mC A01 = new C122845mC();
    public List A02;
    public final C16590pI A03;
    public final AnonymousClass60Y A04;
    public final C129235xO A05;

    public C123385n4(C16590pI r2, AnonymousClass60Y r3, AnonymousClass61F r4, C129235xO r5) {
        super(r4);
        this.A03 = r2;
        this.A04 = r3;
        this.A05 = r5;
    }

    @Override // X.AbstractC118045bB
    public void A06(AbstractC001200n r9, ActivityC13790kL r10, C125985s8 r11) {
        if (r11.A00 != 3) {
            super.A06(r9, r10, r11);
            return;
        }
        ArrayList A0l = C12960it.A0l();
        List<C127025tp> list = this.A02;
        AnonymousClass009.A05(list);
        C127025tp r3 = null;
        for (C127025tp r1 : list) {
            if (r1.A00) {
                r3 = r1;
            }
            A0l.add(r1.A01);
        }
        if (r3 != null) {
            this.A04.A05(AnonymousClass610.A03("FI_SELECTED", "NOVI_HUB", "SELECT_FI_TYPE").A00);
            C127055ts.A00(((AbstractC118045bB) this).A01, 500);
            C129235xO r5 = this.A05;
            String str = r3.A01.A0A;
            AnonymousClass016 A0T = C12980iv.A0T();
            ArrayList A0l2 = C12960it.A0l();
            AnonymousClass61S.A03("action", "novi-update-method-top-up-state", A0l2);
            A0l2.add(new AnonymousClass61S("is-top-up", true));
            AnonymousClass61S.A03("credential-id", str, A0l2);
            C130155yt.A01(new AbstractC136196Lo(A0T, r5, A0l) { // from class: X.6A0
                public final /* synthetic */ AnonymousClass016 A00;
                public final /* synthetic */ C129235xO A01;
                public final /* synthetic */ List A02;
                public final /* synthetic */ boolean A03 = true;

                {
                    this.A01 = r3;
                    this.A00 = r2;
                    this.A02 = r4;
                }

                @Override // X.AbstractC136196Lo
                public final void AV8(C130785zy r112) {
                    Object obj;
                    AnonymousClass1ZY r8;
                    C129235xO r6 = this.A01;
                    AnonymousClass016 r32 = this.A00;
                    List list2 = this.A02;
                    boolean z = this.A03;
                    if (r112.A06() && (obj = r112.A02) != null) {
                        AnonymousClass1V8 r4 = (AnonymousClass1V8) obj;
                        AnonymousClass1V8 A0E = r4.A0E("card");
                        if (A0E != null || (A0E = r4.A0E("bank")) != null) {
                            AbstractC28901Pl A01 = r6.A07.A01(A0E);
                            if (A01 != null) {
                                Iterator it = list2.iterator();
                                while (it.hasNext()) {
                                    AbstractC28901Pl A0H = C117305Zk.A0H(it);
                                    if (A0H instanceof C30881Ze) {
                                        r8 = A0H.A08;
                                        if (A0H.A0A.equals(A01.A0A)) {
                                            AnonymousClass009.A05(r8);
                                            ((C119765f4) r8).A06 = z;
                                        } else if (z) {
                                            AnonymousClass009.A05(r8);
                                            ((C119765f4) r8).A06 = false;
                                        }
                                    } else if (A0H instanceof C30861Zc) {
                                        r8 = A0H.A08;
                                        if (A0H.A0A.equals(A01.A0A)) {
                                            AnonymousClass009.A05(r8);
                                            ((C119735f1) r8).A05 = z;
                                        } else if (z) {
                                            AnonymousClass009.A05(r8);
                                            ((C119735f1) r8).A05 = false;
                                        }
                                    }
                                    A0H.A08 = r8;
                                }
                                r6.A02.A00().A04(
                                /*  JADX ERROR: Method code generation error
                                    jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0092: INVOKE  
                                      (wrap: X.1nR : 0x0089: INVOKE  (r1v3 X.1nR A[REMOVE]) = (wrap: X.0qD : 0x0087: IGET  (r0v5 X.0qD A[REMOVE]) = (r6v0 'r6' X.5xO) X.5xO.A02 X.0qD) type: VIRTUAL call: X.0qD.A00():X.1nR)
                                      (wrap: X.67b : 0x008f: CONSTRUCTOR  (r0v6 X.67b A[REMOVE]) = (r3v0 'r32' X.016) call: X.67b.<init>(X.016):void type: CONSTRUCTOR)
                                      (r2v0 'list2' java.util.List)
                                     type: VIRTUAL call: X.1nR.A04(X.20l, java.util.List):void in method: X.6A0.AV8(X.5zy):void, file: classes4.dex
                                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                                    	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                                    	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                    	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                                    	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                                    	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                    	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                                    	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                                    	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                    	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                                    	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                                    	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                    	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                                    	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                                    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                                    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                                    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                                    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                                    	at java.util.ArrayList.forEach(ArrayList.java:1259)
                                    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                                    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                                    Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x008f: CONSTRUCTOR  (r0v6 X.67b A[REMOVE]) = (r3v0 'r32' X.016) call: X.67b.<init>(X.016):void type: CONSTRUCTOR in method: X.6A0.AV8(X.5zy):void, file: classes4.dex
                                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                                    	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                                    	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                                    	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                                    	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                                    	... 33 more
                                    Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.67b, state: NOT_LOADED
                                    	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                                    	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                                    	... 39 more
                                    */
                                /*
                                    this = this;
                                    X.5xO r6 = r10.A01
                                    X.016 r3 = r10.A00
                                    java.util.List r2 = r10.A02
                                    boolean r5 = r10.A03
                                    boolean r0 = r11.A06()
                                    r7 = 0
                                    if (r0 == 0) goto L_0x0096
                                    java.lang.Object r4 = r11.A02
                                    if (r4 == 0) goto L_0x0096
                                    X.1V8 r4 = (X.AnonymousClass1V8) r4
                                    java.lang.String r0 = "card"
                                    X.1V8 r1 = r4.A0E(r0)
                                    if (r1 != 0) goto L_0x0025
                                    java.lang.String r0 = "bank"
                                    X.1V8 r1 = r4.A0E(r0)
                                    if (r1 == 0) goto L_0x00a4
                                L_0x0025:
                                    X.0zx r0 = r6.A07
                                    X.1Pl r4 = r0.A01(r1)
                                    if (r4 == 0) goto L_0x0096
                                    java.util.Iterator r9 = r2.iterator()
                                L_0x0031:
                                    boolean r0 = r9.hasNext()
                                    if (r0 == 0) goto L_0x0087
                                    X.1Pl r7 = X.C117305Zk.A0H(r9)
                                    boolean r0 = r7 instanceof X.C30881Ze
                                    if (r0 == 0) goto L_0x0062
                                    X.1ZY r8 = r7.A08
                                    java.lang.String r1 = r7.A0A
                                    java.lang.String r0 = r4.A0A
                                    boolean r0 = r1.equals(r0)
                                    if (r0 == 0) goto L_0x0056
                                    X.AnonymousClass009.A05(r8)
                                    r0 = r8
                                    X.5f4 r0 = (X.C119765f4) r0
                                    r0.A06 = r5
                                L_0x0053:
                                    r7.A08 = r8
                                    goto L_0x0031
                                L_0x0056:
                                    if (r5 == 0) goto L_0x0053
                                    X.AnonymousClass009.A05(r8)
                                    r1 = r8
                                    X.5f4 r1 = (X.C119765f4) r1
                                    r0 = 0
                                    r1.A06 = r0
                                    goto L_0x0053
                                L_0x0062:
                                    boolean r0 = r7 instanceof X.C30861Zc
                                    if (r0 == 0) goto L_0x0031
                                    X.1ZY r8 = r7.A08
                                    java.lang.String r1 = r7.A0A
                                    java.lang.String r0 = r4.A0A
                                    boolean r0 = r1.equals(r0)
                                    if (r0 == 0) goto L_0x007b
                                    X.AnonymousClass009.A05(r8)
                                    r0 = r8
                                    X.5f1 r0 = (X.C119735f1) r0
                                    r0.A05 = r5
                                    goto L_0x0053
                                L_0x007b:
                                    if (r5 == 0) goto L_0x0053
                                    X.AnonymousClass009.A05(r8)
                                    r1 = r8
                                    X.5f1 r1 = (X.C119735f1) r1
                                    r0 = 0
                                    r1.A05 = r0
                                    goto L_0x0053
                                L_0x0087:
                                    X.0qD r0 = r6.A02
                                    X.1nR r1 = r0.A00()
                                    X.67b r0 = new X.67b
                                    r0.<init>(r3)
                                    r1.A04(r0, r2)
                                    return
                                L_0x0096:
                                    X.20p r2 = r11.A00
                                    X.63q r1 = r11.A01
                                    X.5zy r0 = new X.5zy
                                    r0.<init>(r2, r7)
                                    r0.A01 = r1
                                    r3.A0A(r0)
                                L_0x00a4:
                                    return
                                */
                                throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass6A0.AV8(X.5zy):void");
                            }
                        }, r5.A03, C117305Zk.A0Q(A0l2));
                        A0T.A05(r9, new AnonymousClass02B(r9, r10, this) { // from class: X.65f
                            public final /* synthetic */ AbstractC001200n A00;
                            public final /* synthetic */ ActivityC13790kL A01;
                            public final /* synthetic */ C123385n4 A02;

                            {
                                this.A02 = r3;
                                this.A00 = r1;
                                this.A01 = r2;
                            }

                            @Override // X.AnonymousClass02B
                            public final void ANq(Object obj) {
                                C123385n4 r4 = this.A02;
                                AbstractC001200n r2 = this.A00;
                                ActivityC13790kL r32 = this.A01;
                                C130785zy r6 = (C130785zy) obj;
                                C127055ts.A00(((AbstractC118045bB) r4).A01, 501);
                                if (r6.A06()) {
                                    r4.A08(false);
                                    List A01 = AnonymousClass612.A01((List) r6.A02);
                                    r4.A02 = A01;
                                    r4.A07(r4.A03.A00, r2, A01);
                                    r4.A04();
                                    return;
                                }
                                C452120p r0 = r6.A00;
                                if (r0 == null || r0.A00 != 542720003) {
                                    C130295zB.A01(r32, C126995tm.A00(null, R.string.ok), r4.A03.A00.getString(R.string.payment_method_cannot_be_set_default)).show();
                                }
                            }
                        });
                    }
                }

                public final void A07(Context context, AbstractC001200n r5, List list) {
                    if (list == null) {
                        A08(false);
                        return;
                    }
                    Iterator it = list.iterator();
                    while (it.hasNext()) {
                        C127025tp r1 = (C127025tp) it.next();
                        if (r1.A00) {
                            this.A00 = r1;
                        }
                    }
                    C133856Cj r12 = new AnonymousClass6M7(context, r5, this, list) { // from class: X.6Cj
                        public final /* synthetic */ Context A00;
                        public final /* synthetic */ AbstractC001200n A01;
                        public final /* synthetic */ C123385n4 A02;
                        public final /* synthetic */ List A03;

                        {
                            this.A02 = r3;
                            this.A03 = r4;
                            this.A00 = r1;
                            this.A01 = r2;
                        }

                        @Override // X.AnonymousClass6M7
                        public final void ARd(C127025tp r10) {
                            C123385n4 r6 = this.A02;
                            List<C127025tp> list2 = this.A03;
                            Context context2 = this.A00;
                            AbstractC001200n r3 = this.A01;
                            if (!r10.A00) {
                                for (C127025tp r13 : list2) {
                                    if (r13.A00) {
                                        r6.A00 = r10;
                                    }
                                    r13.A00 = false;
                                }
                                r10.A00 = true;
                                AbstractC28901Pl r7 = r10.A01;
                                AnonymousClass1ZY r14 = r7.A08;
                                if ((!(r14 instanceof C119765f4) || !((C119765f4) r14).A06) && (!(r14 instanceof C119735f1) || !((C119735f1) r14).A05)) {
                                    r6.A08(true);
                                } else {
                                    r6.A08(false);
                                }
                                AnonymousClass610 A03 = AnonymousClass610.A03("FI_DEFAULT_SELECTED", "NOVI_HUB", "SELECT_FI_TYPE");
                                String str = r7.A0A;
                                C128365vz r15 = A03.A00;
                                r15.A0S = str;
                                r6.A04.A05(r15);
                            }
                            r6.A07(context2, r3, list2);
                            r6.A04();
                        }
                    };
                    C122845mC r0 = new C122845mC();
                    r0.A01 = list;
                    r0.A00 = r12;
                    this.A01 = r0;
                }

                public final void A08(boolean z) {
                    ((AbstractC118045bB) this).A01.A0B(new C127055ts(new AnonymousClass5s9(Boolean.valueOf(z)), 201));
                }
            }
