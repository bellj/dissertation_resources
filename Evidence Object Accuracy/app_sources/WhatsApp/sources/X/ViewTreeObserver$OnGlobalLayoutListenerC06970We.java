package X;

import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build;
import android.view.Display;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/* renamed from: X.0We  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC06970We implements ViewTreeObserver.OnGlobalLayoutListener {
    public boolean A00;
    public final int A01;
    public final View A02;
    public final List A03 = Collections.synchronizedList(new LinkedList());

    public ViewTreeObserver$OnGlobalLayoutListenerC06970We(View view, boolean z) {
        float f;
        this.A02 = view;
        this.A00 = z;
        float f2 = 100.0f * view.getContext().getResources().getDisplayMetrics().density;
        if (f2 >= 0.0f) {
            f = f2 + 0.5f;
        } else {
            f = f2 - 0.5f;
        }
        this.A01 = (int) f;
        view.getViewTreeObserver().addOnGlobalLayoutListener(this);
    }

    public static int A00(View view) {
        Display defaultDisplay = ((WindowManager) view.getContext().getSystemService("window")).getDefaultDisplay();
        Point point = new Point();
        defaultDisplay.getSize(point);
        return point.y;
    }

    public static int A01(View view) {
        Display defaultDisplay = ((WindowManager) view.getContext().getSystemService("window")).getDefaultDisplay();
        Point point = new Point();
        if (view.getRootWindowInsets() != null) {
            defaultDisplay.getRealSize(point);
            return point.y - view.getRootWindowInsets().getStableInsetBottom();
        }
        defaultDisplay.getSize(point);
        return point.y;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        int height;
        Rect rect = new Rect();
        View view = this.A02;
        view.getWindowVisibleDisplayFrame(rect);
        int i = Build.VERSION.SDK_INT;
        if (i >= 23) {
            height = A01(view);
        } else if (i >= 21) {
            height = A00(view);
        } else {
            height = view.getRootView().getHeight();
        }
        int i2 = height - rect.bottom;
        boolean z = this.A00;
        int i3 = this.A01;
        if (!z) {
            if (i2 > i3) {
                this.A00 = true;
                List<C05370Ph> list = this.A03;
                synchronized (list) {
                    for (C05370Ph r0 : list) {
                        if (r0 != null) {
                            r0.A02(i2);
                        }
                    }
                }
            }
        } else if (i2 > i3) {
            List<C05370Ph> list2 = this.A03;
            synchronized (list2) {
                for (C05370Ph r02 : list2) {
                    if (r02 != null) {
                        r02.A01(i2);
                    }
                }
            }
        } else if (i2 < i3) {
            this.A00 = false;
            List<C05370Ph> list3 = this.A03;
            synchronized (list3) {
                for (C05370Ph r03 : list3) {
                    if (r03 != null) {
                        r03.A00();
                    }
                }
            }
        }
    }
}
