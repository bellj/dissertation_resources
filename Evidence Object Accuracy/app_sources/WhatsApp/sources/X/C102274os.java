package X;

import android.widget.AbsListView;
import com.whatsapp.group.GroupParticipantsSearchFragment;

/* renamed from: X.4os  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C102274os implements AbsListView.OnScrollListener {
    public int A00 = 0;
    public final /* synthetic */ GroupParticipantsSearchFragment A01;

    @Override // android.widget.AbsListView.OnScrollListener
    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
    }

    public C102274os(GroupParticipantsSearchFragment groupParticipantsSearchFragment) {
        this.A01 = groupParticipantsSearchFragment;
    }

    @Override // android.widget.AbsListView.OnScrollListener
    public void onScrollStateChanged(AbsListView absListView, int i) {
        int i2 = this.A00;
        if (i2 == 0 && i != i2) {
            this.A01.A07.A01(absListView);
        }
        this.A00 = i;
    }
}
