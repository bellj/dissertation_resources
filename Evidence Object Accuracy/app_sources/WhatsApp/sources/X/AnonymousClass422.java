package X;

import com.whatsapp.quickcontact.QuickContactActivity;
import java.util.Collection;

/* renamed from: X.422  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass422 extends C27131Gd {
    public final /* synthetic */ QuickContactActivity A00;

    public AnonymousClass422(QuickContactActivity quickContactActivity) {
        this.A00 = quickContactActivity;
    }

    @Override // X.C27131Gd
    public void A00(AbstractC14640lm r2) {
        QuickContactActivity quickContactActivity = this.A00;
        quickContactActivity.A2f();
        quickContactActivity.A0Y.A04();
    }

    @Override // X.C27131Gd
    public void A05(Collection collection) {
        QuickContactActivity quickContactActivity = this.A00;
        quickContactActivity.A2f();
        AbstractC51412Uo r0 = quickContactActivity.A0Y;
        r0.A00();
        r0.A03();
    }

    @Override // X.C27131Gd
    public void A07(Collection collection) {
        QuickContactActivity quickContactActivity = this.A00;
        quickContactActivity.A2f();
        AbstractC51412Uo r0 = quickContactActivity.A0Y;
        r0.A00();
        r0.A03();
    }
}
