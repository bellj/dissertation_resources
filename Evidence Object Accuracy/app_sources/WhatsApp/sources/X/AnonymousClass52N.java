package X;

/* renamed from: X.52N  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass52N implements AbstractC22060yS {
    public final /* synthetic */ C27661Io A00;

    public AnonymousClass52N(C27661Io r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC22060yS
    public void AMF() {
        C27661Io.A00(this.A00, "appBackgrounded");
    }

    @Override // X.AbstractC22060yS
    public void AMG() {
        C27661Io.A00(this.A00, "appForegrounded");
    }
}
