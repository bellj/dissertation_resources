package X;

import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;
import java.util.Map;

/* renamed from: X.5wz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128985wz {
    public C39041pA A00;
    public C16590pI A01;
    public Map A02;
    public final C16630pM A03;

    public C128985wz(C16590pI r4, C16630pM r5) {
        this.A03 = r5;
        this.A01 = r4;
        try {
            this.A00 = C39041pA.A00(new File(r4.A00.getCacheDir(), "bk_cache_dir"), 10485760);
        } catch (IOException unused) {
            Log.e("BkCacheSaveOnDiskHelper/const unable to initialize disk cache for bk cache");
        }
        this.A02 = C12970iu.A11();
    }

    public final void A00() {
        C12970iu.A1D(this.A03.A01("bloks").edit(), "bk_cache_lookup_map", this.A02.values().toString());
    }
}
