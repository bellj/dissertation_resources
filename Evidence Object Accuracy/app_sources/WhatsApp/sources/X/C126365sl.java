package X;

import java.util.Arrays;

/* renamed from: X.5sl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C126365sl {
    public final AnonymousClass1V8 A00;

    public C126365sl(C128665wT r6) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy r2 = new C41141sy("account");
        C41141sy.A01(r2, "action", "get-purpose-limiting-key");
        if (C117295Zj.A1X("cd7962b7", false)) {
            C41141sy.A01(r2, "purpose", "cd7962b7");
        }
        C117295Zj.A1H(r2, A0M);
        AnonymousClass1V8 r22 = r6.A00;
        A0M.A07(r22, C12960it.A0l());
        A0M.A09(r22, Arrays.asList(new String[0]), C12960it.A0l());
        this.A00 = A0M.A03();
    }
}
