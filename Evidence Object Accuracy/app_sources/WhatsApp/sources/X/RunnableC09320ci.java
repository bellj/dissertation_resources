package X;

import androidx.recyclerview.widget.StaggeredGridLayoutManager;

/* renamed from: X.0ci  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09320ci implements Runnable {
    public final /* synthetic */ StaggeredGridLayoutManager A00;

    public RunnableC09320ci(StaggeredGridLayoutManager staggeredGridLayoutManager) {
        this.A00 = staggeredGridLayoutManager;
    }

    @Override // java.lang.Runnable
    public void run() {
        this.A00.A1T();
    }
}
