package X;

import android.text.TextUtils;
import android.util.Log;
import java.net.UnknownHostException;

/* renamed from: X.3Hl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C64923Hl {
    public static String A00(String str, Throwable th) {
        String str2;
        if (th != null) {
            Throwable th2 = th;
            while (true) {
                if (!(th2 instanceof UnknownHostException)) {
                    th2 = th2.getCause();
                    if (th2 == null) {
                        str2 = Log.getStackTraceString(th).trim().replace("\t", "    ");
                        break;
                    }
                } else {
                    str2 = "UnknownHostException (no network)";
                    break;
                }
            }
        } else {
            str2 = null;
        }
        if (TextUtils.isEmpty(str2)) {
            return str;
        }
        StringBuilder A0j = C12960it.A0j(str);
        A0j.append("\n  ");
        A0j.append(str2.replace("\n", "\n  "));
        A0j.append('\n');
        return A0j.toString();
    }

    public static void A01(String str, String str2, Throwable th) {
        Log.e(str, A00(str2, th));
    }

    public static void A02(String str, String str2, Throwable th) {
        Log.w(str, A00(str2, th));
    }
}
