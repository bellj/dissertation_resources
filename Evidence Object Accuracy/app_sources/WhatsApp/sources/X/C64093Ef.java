package X;

import java.util.List;

/* renamed from: X.3Ef  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C64093Ef {
    public final float A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final int A04;
    public final C71183cW A05;
    public final List A06 = C12980iv.A0w(5);
    public final int[] A07;

    public C64093Ef(C71183cW r2, float f, int i, int i2, int i3, int i4) {
        this.A05 = r2;
        this.A02 = i;
        this.A03 = i2;
        this.A04 = i3;
        this.A01 = i4;
        this.A00 = f;
        this.A07 = new int[3];
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:81:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.C82583vr A00(int[] r13, int r14, int r15) {
        /*
        // Method dump skipped, instructions count: 262
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C64093Ef.A00(int[], int, int):X.3vr");
    }

    public final boolean A01(int[] iArr) {
        float f = this.A00;
        float f2 = f / 2.0f;
        int i = 0;
        while (C12980iv.A02(iArr, f, i) < f2) {
            i++;
            if (i >= 3) {
                return true;
            }
        }
        return false;
    }
}
