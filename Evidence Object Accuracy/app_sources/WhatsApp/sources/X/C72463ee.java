package X;

import android.os.Binder;
import android.os.Handler;
import android.os.Parcel;
import com.facebook.redex.IDxCreatorShape1S0000000_2_I1;
import com.google.android.exoplayer2.Timeline;
import com.whatsapp.R;
import java.security.InvalidAlgorithmParameterException;
import java.security.cert.X509Certificate;
import java.util.AbstractMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Logger;
import org.whispersystems.curve25519.JavaCurve25519Provider;

/* renamed from: X.3ee  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C72463ee {
    public static byte A00(int i, long j, long j2) {
        return (byte) ((int) (j2 | (j << i)));
    }

    public static int A01(int i) {
        return (i & 267386880) >>> 20;
    }

    public static int A02(int i, int i2, int i3) {
        return Math.min(i3, i - i2);
    }

    public static int A03(int i, int i2, int i3) {
        return Math.max(i3, Math.min(i, i2));
    }

    public static int A04(String str) {
        if (str == null) {
            return 0;
        }
        return str.hashCode();
    }

    public static int A05(List list) {
        int size = list.size();
        int i = size << 1;
        if (size == 0) {
            return 10;
        }
        return i;
    }

    public static int A06(int[] iArr, int i, int i2, int i3) {
        return i3 + i2 + iArr[i];
    }

    public static long A07(byte[] bArr, int i, int i2) {
        return (JavaCurve25519Provider.A00(bArr, i) >>> i2) & 2097151;
    }

    public static long A08(byte[] bArr, int i, int i2) {
        return (JavaCurve25519Provider.A01(bArr, i) >>> i2) & 2097151;
    }

    public static long A09(int[] iArr, int i) {
        return (long) (iArr[i] & 1048575);
    }

    public static IDxCreatorShape1S0000000_2_I1 A0A(int i) {
        return new IDxCreatorShape1S0000000_2_I1(i);
    }

    public static C94404bl A0B(C94404bl r2, Timeline timeline, int i) {
        return timeline.A0B(r2, i, 0);
    }

    public static AnonymousClass5XU A0C(Object obj) {
        return C93994b5.A02.A00(obj.getClass());
    }

    public static IllegalStateException A0D() {
        return new IllegalStateException();
    }

    public static String A0E(CharSequence charSequence, int i, int i2) {
        return charSequence.subSequence(i, i2).toString();
    }

    public static String A0F(String str) {
        if (str == null) {
            return str;
        }
        return str.toLowerCase(Locale.US);
    }

    public static String A0G(String str, Object[] objArr) {
        return String.format(Locale.US, str, objArr);
    }

    public static String A0H(StringBuilder sb, char c) {
        sb.append(c);
        return sb.toString();
    }

    public static String A0I(byte[] bArr, int i) {
        return AnonymousClass1T7.A02(C95374db.A02(bArr, 0, i));
    }

    public static InvalidAlgorithmParameterException A0J(String str) {
        return new InvalidAlgorithmParameterException(str);
    }

    public static X509Certificate A0K(List list, int i) {
        return (X509Certificate) list.get(i);
    }

    public static Iterator A0L(AbstractMap abstractMap) {
        return abstractMap.keySet().iterator();
    }

    public static Logger A0M(Class cls) {
        return Logger.getLogger(cls.getName());
    }

    public static AnonymousClass1TL A0N(AnonymousClass1TN[] r0, int i) {
        return r0[i].Aer();
    }

    public static void A0O(int i, byte[] bArr, int i2, int i3) {
        bArr[i3] = (byte) (i | i2);
    }

    public static void A0P(int i, byte[] bArr, int i2, int i3) {
        bArr[i3] = (byte) (i ^ i2);
    }

    public static void A0Q(Binder binder, Parcel parcel) {
        parcel.enforceInterface(binder.getInterfaceDescriptor());
    }

    public static void A0R(Handler handler, Object obj) {
        handler.sendMessage(handler.obtainMessage(1, obj));
    }

    public static void A0S(C004802e r2) {
        r2.setNegativeButton(R.string.cancel, null);
    }

    public static void A0T(Object obj, int i, int i2) {
        System.arraycopy(obj, i, obj, i + 1, i2 - i);
    }

    public static void A0U(StringBuilder sb, AnonymousClass5XE r2) {
        sb.append(r2.AAf());
    }

    public static void A0V(byte[] bArr, int i, int i2) {
        bArr[i2] = (byte) (i & 255);
    }

    public static void A0W(byte[] bArr, int i, int i2) {
        bArr[i2] = (byte) (i >>> 8);
    }

    public static void A0X(byte[] bArr, byte[] bArr2, int i, int i2) {
        bArr2[i2] = bArr[i];
    }

    public static boolean A0Y(int i, int i2) {
        return i > i2;
    }

    public static boolean A0Z(X509Certificate x509Certificate) {
        return x509Certificate.getSubjectDN().equals(x509Certificate.getIssuerDN());
    }

    public static byte[] A0a(Object obj, Map map) {
        return (byte[]) map.get(obj);
    }

    public static byte[] A0b(List list, int i) {
        return (byte[]) list.get(i);
    }

    public static byte[] A0c(AnonymousClass1TN r1) {
        return r1.Aer().A02("DER");
    }
}
