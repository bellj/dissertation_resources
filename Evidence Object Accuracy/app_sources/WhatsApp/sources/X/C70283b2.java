package X;

import android.graphics.Bitmap;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaImageView;

/* renamed from: X.3b2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70283b2 implements AbstractC41521tf {
    public final /* synthetic */ C60912yy A00;

    @Override // X.AbstractC41521tf
    public /* synthetic */ void AQV() {
    }

    public C70283b2(C60912yy r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC41521tf
    public int AGm() {
        return this.A00.A01.getDimensionPixelSize(R.dimen.link_preview_thumb_width);
    }

    @Override // X.AbstractC41521tf
    public void Adg(Bitmap bitmap, View view, AbstractC15340mz r5) {
        WaImageView waImageView = this.A00.A00;
        if (bitmap != null) {
            waImageView.setImageBitmap(bitmap);
        } else {
            waImageView.setVisibility(8);
        }
    }

    @Override // X.AbstractC41521tf
    public void Adu(View view) {
        this.A00.A00.setVisibility(8);
    }
}
