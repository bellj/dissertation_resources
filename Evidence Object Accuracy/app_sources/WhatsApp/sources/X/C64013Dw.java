package X;

/* renamed from: X.3Dw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C64013Dw {
    public final AnonymousClass1V8 A00;
    public final AnonymousClass3EI A01;

    public C64013Dw(AbstractC15710nm r3, AnonymousClass1V8 r4, AnonymousClass3BF r5) {
        AnonymousClass1V8.A01(r4, "iq");
        this.A01 = (AnonymousClass3EI) AnonymousClass3JT.A05(r4, new AbstractC116095Uc(r3, r5.A00) { // from class: X.58t
            public final /* synthetic */ AbstractC15710nm A00;
            public final /* synthetic */ AnonymousClass1V8 A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC116095Uc
            public final Object A63(AnonymousClass1V8 r42) {
                return new AnonymousClass3EI(this.A00, r42, this.A01);
            }
        }, new String[0]);
        this.A00 = r4;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || C64013Dw.class != obj.getClass()) {
            return false;
        }
        return this.A01.equals(((C64013Dw) obj).A01);
    }

    public int hashCode() {
        return C12970iu.A08(this.A01, C12970iu.A1b());
    }
}
