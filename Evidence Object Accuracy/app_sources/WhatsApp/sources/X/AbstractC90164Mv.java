package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.4Mv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC90164Mv {
    public final UserJid A00;
    public final String A01;

    public AbstractC90164Mv(UserJid userJid, String str) {
        this.A00 = userJid;
        this.A01 = str;
    }
}
