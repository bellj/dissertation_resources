package X;

import android.content.Context;
import com.whatsapp.Me;
import com.whatsapp.community.CommunityAdminDialogFragment;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.concurrent.Callable;

/* renamed from: X.0nT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15570nT {
    public Me A00;
    public C27621Ig A01;
    public AnonymousClass1MX A02;
    public AnonymousClass1MU A03;
    public C27481Hq A04;
    public C27631Ih A05;
    public boolean A06 = false;
    public final C230910i A07;
    public final AnonymousClass1HL A08 = new AnonymousClass1HL();
    public final C16590pI A09;
    public final C14820m6 A0A;

    public C15570nT(C230910i r2, C16590pI r3, C14820m6 r4) {
        this.A09 = r3;
        this.A0A = r4;
        this.A07 = r2;
    }

    public static void A00(CommunityAdminDialogFragment communityAdminDialogFragment) {
        AnonymousClass4KE r0 = communityAdminDialogFragment.A01;
        int i = communityAdminDialogFragment.A00;
        UserJid userJid = communityAdminDialogFragment.A02;
        AnonymousClass3EZ r1 = r0.A00;
        if (!r1.A02.A0F(userJid)) {
            return;
        }
        if (i == 3) {
            r1.A04.A00();
        } else if (i == 4) {
            r1.A04.A01();
        }
    }

    public Me A01() {
        ClassNotFoundException e;
        IOException e2;
        Throwable th;
        AnonymousClass1MV r1;
        Me me;
        Log.i("memanager/getoldme");
        A08();
        Context context = this.A09.A00;
        Me me2 = null;
        if (!new File(context.getFilesDir(), "me_old").exists()) {
            return null;
        }
        try {
            FileInputStream openFileInput = context.openFileInput("me_old");
            try {
                r1 = new AnonymousClass1MV(this, openFileInput);
                me = (Me) r1.readObject();
            } catch (Throwable th2) {
                th = th2;
            }
            try {
                r1.close();
                if (openFileInput == null) {
                    return me;
                }
                try {
                    openFileInput.close();
                    return me;
                } catch (IOException e3) {
                    e2 = e3;
                    me2 = me;
                    Log.e("memanager/read_old_me/io_error", e2);
                    return me2;
                } catch (ClassNotFoundException e4) {
                    e = e4;
                    me2 = me;
                    Log.w("memanager/read_old_me/serialization_error", e);
                    return me2;
                }
            } catch (Throwable th3) {
                th = th3;
                me2 = me;
                if (openFileInput != null) {
                    try {
                        openFileInput.close();
                    } catch (Throwable unused) {
                    }
                }
                throw th;
            }
        } catch (IOException e5) {
            e2 = e5;
        } catch (ClassNotFoundException e6) {
            e = e6;
        }
    }

    public synchronized AnonymousClass1MU A02() {
        A09();
        return this.A03;
    }

    public UserJid A03() {
        A08();
        C27631Ih r0 = this.A05;
        AnonymousClass009.A05(r0);
        return r0;
    }

    public String A04() {
        A08();
        C27631Ih r0 = this.A05;
        if (r0 == null) {
            return null;
        }
        return r0.user;
    }

    public String A05() {
        return this.A0A.A00.getString("push_name", "");
    }

    public void A06() {
        Log.i("memanager/clearMe");
        A08();
        A0A(null);
    }

    public void A07() {
        Log.i("memanager/deleteoldme");
        A08();
        new File(this.A09.A00.getFilesDir(), "me_old").delete();
    }

    public void A08() {
        AnonymousClass1HL r1 = this.A08;
        if (r1.A06()) {
            r1.A04(new Callable() { // from class: X.1MT
                @Override // java.util.concurrent.Callable
                public final Object call() {
                    C15570nT r4 = C15570nT.this;
                    try {
                        C003401m.A01("MeManager/loadMe");
                        AnonymousClass1HL r3 = r4.A08;
                        r3.A03();
                        Log.i("memanager/load-me");
                        Context context = r4.A09.A00;
                        if (new File(context.getFilesDir(), "me").exists()) {
                            try {
                                FileInputStream openFileInput = context.openFileInput("me");
                                try {
                                    AnonymousClass1MV r12 = new AnonymousClass1MV(r4, openFileInput);
                                    r4.A0A((Me) r12.readObject());
                                    r3.A01();
                                    r12.close();
                                    if (openFileInput != null) {
                                        openFileInput.close();
                                    }
                                } catch (Throwable th) {
                                    if (openFileInput != null) {
                                        try {
                                            openFileInput.close();
                                        } catch (Throwable unused) {
                                        }
                                    }
                                    throw th;
                                }
                            } catch (IOException e) {
                                Log.e("memanager/read_me/io_error", e);
                            } catch (ClassNotFoundException e2) {
                                Log.w("memanager/read_me/serialization_error", e2);
                            }
                        }
                        if (r3.A06()) {
                            r3.A02();
                        }
                        C003401m.A00();
                        return null;
                    } catch (Throwable th2) {
                        AnonymousClass1HL r13 = r4.A08;
                        if (r13.A06()) {
                            r13.A02();
                        }
                        C003401m.A00();
                        throw th2;
                    }
                }
            });
            r1.A00();
        }
    }

    public final synchronized void A09() {
        if (!this.A06) {
            A0B(Jid.getNullable(this.A0A.A00.getString("self_lid", "")));
            this.A06 = true;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0014 A[Catch: 1MW -> 0x0021, TryCatch #0 {1MW -> 0x0021, blocks: (B:5:0x0008, B:7:0x000c, B:8:0x0010, B:10:0x0014, B:11:0x001e), top: B:20:0x0008 }] */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x001e A[Catch: 1MW -> 0x0021, TRY_LEAVE, TryCatch #0 {1MW -> 0x0021, blocks: (B:5:0x0008, B:7:0x000c, B:8:0x0010, B:10:0x0014, B:11:0x001e), top: B:20:0x0008 }] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0059  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0A(com.whatsapp.Me r5) {
        /*
            r4 = this;
            r4.A00 = r5
            r2 = 0
            if (r5 == 0) goto L_0x0006
            goto L_0x0008
        L_0x0006:
            r1 = r2
            goto L_0x0010
        L_0x0008:
            java.lang.String r0 = r5.jabber_id     // Catch: 1MW -> 0x0021
            if (r0 == 0) goto L_0x0006
            X.1Ih r1 = X.C27631Ih.A02(r0)     // Catch: 1MW -> 0x0021
        L_0x0010:
            r4.A05 = r1     // Catch: 1MW -> 0x0021
            if (r1 == 0) goto L_0x001e
            r0 = 0
            com.whatsapp.jid.DeviceJid r0 = com.whatsapp.jid.DeviceJid.getFromUserJidAndDeviceId(r1, r0)     // Catch: 1MW -> 0x0021
            X.1Hq r0 = (X.C27481Hq) r0     // Catch: 1MW -> 0x0021
            r4.A04 = r0     // Catch: 1MW -> 0x0021
            goto L_0x002b
        L_0x001e:
            r4.A04 = r2     // Catch: 1MW -> 0x0021
            goto L_0x002b
        L_0x0021:
            r1 = move-exception
            java.lang.String r0 = "memanager/setMe/invalid_jid_error"
            com.whatsapp.util.Log.e(r0, r1)
            r4.A05 = r2
            r4.A04 = r2
        L_0x002b:
            X.1Ih r0 = r4.A05
            if (r0 != 0) goto L_0x0059
            r4.A01 = r2
        L_0x0031:
            java.lang.String r0 = "memanager/setMe me: "
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            com.whatsapp.Me r0 = r4.A00
            r1.append(r0)
            java.lang.String r0 = ", myUserJid: "
            r1.append(r0)
            X.1Ih r0 = r4.A05
            r1.append(r0)
            java.lang.String r0 = ", myDeviceJid: "
            r1.append(r0)
            X.1Hq r0 = r4.A04
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            com.whatsapp.util.Log.i(r0)
            return
        L_0x0059:
            X.1Ig r1 = new X.1Ig
            r1.<init>(r0)
            r4.A01 = r1
            X.0m6 r0 = r4.A0A
            android.content.SharedPreferences r3 = r0.A00
            java.lang.String r0 = "profile_photo_thumb_id"
            r2 = 0
            int r0 = r3.getInt(r0, r2)
            r1.A05 = r0
            X.1Ig r1 = r4.A01
            java.lang.String r0 = "profile_photo_full_id"
            int r0 = r3.getInt(r0, r2)
            r1.A04 = r0
            X.1Ig r1 = r4.A01
            java.lang.String r0 = r4.A05()
            r1.A0U = r0
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15570nT.A0A(com.whatsapp.Me):void");
    }

    public final synchronized void A0B(Jid jid) {
        if (jid instanceof AnonymousClass1MU) {
            AnonymousClass1MU r3 = (AnonymousClass1MU) jid;
            this.A03 = r3;
            try {
                this.A02 = new AnonymousClass1MX(r3, 0);
            } catch (AnonymousClass1MW e) {
                Log.w("memanager/setMyLidDeviceJid/invalid_jid_error", e);
            }
        } else if (jid instanceof AnonymousClass1MX) {
            AnonymousClass1MX r32 = (AnonymousClass1MX) jid;
            this.A03 = (AnonymousClass1MU) r32.userJid;
            this.A02 = r32;
        }
    }

    public void A0C(String str) {
        this.A0A.A00.edit().putString("push_name", str).apply();
        A08();
        C27621Ig r0 = this.A01;
        if (r0 != null) {
            r0.A0U = str;
        }
    }

    public final boolean A0D(Me me, String str) {
        Throwable e;
        StringBuilder sb;
        String str2;
        StringBuilder sb2 = new StringBuilder("memanager/save ");
        sb2.append(str);
        Log.i(sb2.toString());
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(this.A09.A00.openFileOutput(str, 0));
            try {
                objectOutputStream.writeObject(me);
                objectOutputStream.close();
                return true;
            } catch (Throwable th) {
                try {
                    objectOutputStream.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } catch (FileNotFoundException e2) {
            e = e2;
            sb = new StringBuilder();
            str2 = "memanager/save/notfounderror ";
            sb.append(str2);
            sb.append(str);
            Log.e(sb.toString(), e);
            return false;
        } catch (IOException e3) {
            e = e3;
            sb = new StringBuilder();
            str2 = "memanager/save/ioerror ";
            sb.append(str2);
            sb.append(str);
            Log.e(sb.toString(), e);
            return false;
        }
    }

    public boolean A0E(DeviceJid deviceJid) {
        return (deviceJid == null || !A0F(deviceJid.getUserJid()) || deviceJid.device == 0) ? false : true;
    }

    public boolean A0F(Jid jid) {
        if (jid == null) {
            return false;
        }
        A08();
        return jid.equals(this.A05) || jid.equals(A02());
    }
}
