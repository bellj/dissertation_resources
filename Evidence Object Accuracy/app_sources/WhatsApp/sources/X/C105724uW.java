package X;

import com.whatsapp.mediacomposer.MediaComposerActivity;

/* renamed from: X.4uW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C105724uW implements AnonymousClass070 {
    public final /* synthetic */ MediaComposerActivity A00;

    @Override // X.AnonymousClass070
    public void ATO(int i) {
    }

    @Override // X.AnonymousClass070
    public void ATP(int i, float f, int i2) {
    }

    public C105724uW(MediaComposerActivity mediaComposerActivity) {
        this.A00 = mediaComposerActivity;
    }

    @Override // X.AnonymousClass070
    public void ATQ(int i) {
        MediaComposerActivity mediaComposerActivity = this.A00;
        mediaComposerActivity.A2q(mediaComposerActivity.A0d.A0L(i));
        mediaComposerActivity.A2k();
        mediaComposerActivity.A2l();
    }
}
