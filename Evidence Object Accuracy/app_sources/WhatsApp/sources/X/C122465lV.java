package X;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5lV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122465lV extends AbstractC118825cR {
    public final ImageView A00;
    public final ImageView A01;
    public final LinearLayout A02;
    public final TextView A03;
    public final TextView A04;

    public C122465lV(View view) {
        super(view);
        this.A01 = C12970iu.A0K(view, R.id.add_method_row_icon_left);
        this.A04 = C12960it.A0I(view, R.id.add_method_row_title);
        this.A03 = C12960it.A0I(view, R.id.add_method_row_subtitle);
        this.A02 = C117305Zk.A07(view, R.id.add_method_row_container);
        this.A00 = C12970iu.A0K(view, R.id.add_method_row_add_icon);
    }
}
