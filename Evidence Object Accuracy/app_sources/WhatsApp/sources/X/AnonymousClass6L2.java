package X;

import com.whatsapp.R;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

/* renamed from: X.6L2  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6L2 extends GregorianCalendar {
    public int count;
    public int id;
    public AnonymousClass018 whatsAppLocale;

    public AnonymousClass6L2(AnonymousClass018 r2, Calendar calendar, int i) {
        this.whatsAppLocale = r2;
        this.id = i;
        setTime(calendar.getTime());
    }

    @Override // java.util.Calendar, java.lang.Object
    public String toString() {
        DateFormat A07;
        long timeInMillis = getTimeInMillis();
        if (timeInMillis <= 0) {
            return this.whatsAppLocale.A09(R.string.unknown);
        }
        AnonymousClass018 r4 = this.whatsAppLocale;
        Locale A14 = C12970iu.A14(r4);
        Calendar instance = Calendar.getInstance(A14);
        instance.setTimeInMillis(timeInMillis);
        if (instance.get(1) == Calendar.getInstance(A14).get(1)) {
            A07 = AnonymousClass1MY.A06(r4);
        } else {
            A07 = AnonymousClass1MY.A07(r4, 0);
        }
        return A07.format(instance.getTime());
    }
}
