package X;

import android.os.Parcel;
import android.os.RemoteException;
import android.util.Log;

/* renamed from: X.3sU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C80573sU extends AbstractC92124Uq {
    public boolean A00 = true;
    public final AnonymousClass4W9 A01;
    public final C93444aB A02 = new C93444aB();
    public final Object A03 = C12970iu.A0l();

    public C80573sU() {
        throw C12960it.A0U("Default constructor called");
    }

    public /* synthetic */ C80573sU(AnonymousClass4W9 r2) {
        this.A01 = r2;
    }

    /* JADX INFO: finally extract failed */
    @Override // X.AbstractC92124Uq
    public final void A00() {
        super.A00();
        synchronized (this.A03) {
            if (this.A00) {
                AnonymousClass4W9 r7 = this.A01;
                synchronized (r7.A05) {
                    if (r7.A00 != null) {
                        try {
                            Object A00 = r7.A00();
                            C13020j0.A01(A00);
                            C79963rU r4 = (C79963rU) ((AnonymousClass5YL) A00);
                            Parcel obtain = Parcel.obtain();
                            obtain.writeInterfaceToken(r4.A01);
                            Parcel obtain2 = Parcel.obtain();
                            try {
                                C12990iw.A18(r4.A00, obtain, obtain2, 3);
                                obtain.recycle();
                                obtain2.recycle();
                            } catch (Throwable th) {
                                obtain.recycle();
                                obtain2.recycle();
                                throw th;
                            }
                        } catch (RemoteException e) {
                            Log.e(r7.A06, "Could not finalize native handle", e);
                        }
                    }
                }
                this.A00 = false;
            }
        }
    }

    public final void finalize() {
        try {
            synchronized (this.A03) {
                if (this.A00) {
                    Log.w("FaceDetector", "FaceDetector was not released with FaceDetector.release()");
                    A00();
                }
            }
        } finally {
            super.finalize();
        }
    }
}
