package X;

import android.content.Context;
import android.os.Looper;

/* renamed from: X.3o2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77893o2 extends AbstractC77963o9 {
    @Override // X.AbstractC95064d1, X.AbstractC72443eb
    public final int AET() {
        return 11925000;
    }

    public C77893o2(Context context, Looper looper, AbstractC14980mM r10, AbstractC15000mO r11, AnonymousClass3BW r12) {
        super(context, looper, r10, r11, r12, 40);
    }
}
