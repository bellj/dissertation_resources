package X;

import com.whatsapp.Conversation;

/* renamed from: X.593  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass593 implements AbstractC29031Pz {
    public final /* synthetic */ Conversation A00;
    public final /* synthetic */ AnonymousClass3DS A01;
    public final /* synthetic */ AbstractC14640lm A02;

    @Override // X.AbstractC29031Pz
    public String ADy() {
        return "wallpaper";
    }

    public AnonymousClass593(Conversation conversation, AnonymousClass3DS r2, AbstractC14640lm r3) {
        this.A01 = r2;
        this.A00 = conversation;
        this.A02 = r3;
    }

    @Override // X.AbstractC29031Pz
    public void A98(C28631Oi r4) {
        C33191db AHh = this.A01.A08.AHh(this.A02, C41691tw.A08(this.A00));
        if (AHh != null) {
            String str = AHh.A01;
            r4.A00(str.length(), "wallpaper", str);
        }
    }
}
