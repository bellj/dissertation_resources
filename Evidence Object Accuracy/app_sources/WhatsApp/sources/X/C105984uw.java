package X;

/* renamed from: X.4uw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C105984uw implements AbstractC12240hb {
    public final /* synthetic */ AnonymousClass4SJ A00;
    public final /* synthetic */ C105914up A01;

    public C105984uw(AnonymousClass4SJ r1, C105914up r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC12240hb
    public void Aa4(Object obj) {
        boolean z;
        C08870bz A02;
        C105914up r3 = this.A01;
        AnonymousClass4SJ r4 = this.A00;
        synchronized (r3) {
            int i = r4.A00;
            AnonymousClass0RA.A01(C12960it.A1U(i));
            int i2 = i - 1;
            r4.A00 = i2;
            if (r4.A01 || i2 != 0) {
                z = false;
            } else {
                r3.A05.A03(r4.A04, r4);
                z = true;
            }
            A02 = r3.A02(r4);
        }
        if (A02 != null) {
            A02.close();
        }
        if (z) {
            AnonymousClass4I0 r0 = r4.A03;
            Object obj2 = r4.A04;
            AnonymousClass4Vh r1 = r0.A00;
            synchronized (r1) {
                r1.A03.add(obj2);
            }
        }
        r3.A04();
        r3.A03();
    }
}
