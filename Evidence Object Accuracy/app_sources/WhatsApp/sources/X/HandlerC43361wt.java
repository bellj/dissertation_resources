package X;

import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import com.whatsapp.util.Log;
import java.util.concurrent.CountDownLatch;

/* renamed from: X.1wt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class HandlerC43361wt extends Handler {
    public final boolean A00 = C20640w5.A00();
    public final /* synthetic */ C19890uq A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public HandlerC43361wt(Looper looper, C19890uq r3) {
        super(looper);
        this.A01 = r3;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        if (this.A00) {
            Log.w("xmpp/handler/unsupported");
            return;
        }
        int i = message.arg1;
        if (i == 0) {
            C19890uq r5 = this.A01;
            r5.A0B = false;
            boolean z = message.getData().getBoolean("should_register", false);
            C19890uq.A00(r5);
            if (z) {
                C15570nT r0 = r5.A0I;
                r0.A08();
                C27631Ih r1 = r0.A05;
                AnonymousClass10C r4 = r5.A07;
                AnonymousClass009.A05(r4);
                if (!r5.A0x) {
                    Log.i("xmpp/handler/registered");
                    r5.A05 = r1;
                    r5.A08 = r4;
                    r5.A0b.A00 = r4;
                    r5.A0J(true, false, false);
                    if (r5.A09 != null) {
                        Log.i("message-handler-callback/ready");
                        if (r4.A0L.A00.getBoolean("refresh_broadcast_lists", false)) {
                            r4.A0f.A04();
                        }
                    }
                    r5.A0x = true;
                }
            }
        } else if (i == 1) {
            if (message.getData().getBoolean("should_unregister", false)) {
                this.A01.A0x = false;
            }
            C19890uq r52 = this.A01;
            Log.i("xmpp/handler/stop");
            if (r52.A0y) {
                r52.A0y = false;
                synchronized (r52.A0q) {
                    C43321wp r12 = r52.A0Y;
                    if (!r12.A00) {
                        r52.A08.A00();
                    }
                    r12.A00(true);
                }
                if (r52.A09 != null) {
                    r52.A0S.A00.unregisterReceiver(r52.A0D);
                    r52.A0f.AeS();
                    HandlerThread handlerThread = r52.A04;
                    AnonymousClass009.A05(handlerThread);
                    handlerThread.quit();
                    try {
                        r52.A04.join(120000);
                    } catch (InterruptedException e) {
                        Log.w("interrupted while waiting on connectivity handler thread to exit", e);
                        Thread.currentThread().interrupt();
                    }
                    if (r52.A04.isAlive()) {
                        Log.e("xmpp/handler/stop connectivity-handler-thread still alive");
                    }
                    r52.A04 = null;
                    C19890uq.A0z = new CountDownLatch(1);
                    C19890uq.A11.set(false);
                    ((Handler) r52.A09).obtainMessage(3).sendToTarget();
                    r52.A09 = null;
                    C17220qS r02 = r52.A0c;
                    r02.A01 = null;
                    r02.A00 = null;
                } else {
                    r52.A06.quit();
                }
            }
            r52.A0B = true;
        } else if (i == 2) {
            Bundle data = message.getData();
            boolean z2 = data.getBoolean("reset", false);
            boolean z3 = data.getBoolean("force", false);
            boolean z4 = data.getBoolean("force_no_ongoing_backoff", false);
            boolean z5 = data.getBoolean("check_connection", false);
            boolean z6 = data.getBoolean("notify_on_failure", false);
            String string = data.getString("ip_address");
            String string2 = data.getString("cl_sess");
            boolean z7 = data.getBoolean("fgservice", false);
            int i2 = data.getInt("connect_reason", 0);
            if (z3) {
                this.A01.A03 = 0;
            }
            if (z2) {
                this.A01.A0n.A02();
            }
            C19890uq r53 = this.A01;
            long j = r53.A03;
            if (j <= 0 || SystemClock.elapsedRealtime() >= j) {
                C19890uq.A00(r53);
                r53.A0E(string2, string, i2, z3, z4, z5, z6, z7);
            }
        } else if (i != 3) {
            C19890uq.A00(this.A01);
        } else {
            Bundle data2 = message.getData();
            C19890uq r13 = this.A01;
            C19890uq.A00(r13);
            if (data2.getBoolean("long_connect", false)) {
                r13.A09();
            }
        }
    }
}
