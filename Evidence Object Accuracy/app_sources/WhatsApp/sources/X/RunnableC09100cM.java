package X;

import androidx.appcompat.widget.SearchView;

/* renamed from: X.0cM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09100cM implements Runnable {
    public final /* synthetic */ SearchView A00;

    public RunnableC09100cM(SearchView searchView) {
        this.A00 = searchView;
    }

    @Override // java.lang.Runnable
    public void run() {
        AnonymousClass0BR r1 = this.A00.A0E;
        if (r1 instanceof AnonymousClass0E1) {
            r1.A77(null);
        }
    }
}
