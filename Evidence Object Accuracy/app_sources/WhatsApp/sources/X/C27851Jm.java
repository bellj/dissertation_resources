package X;

import com.facebook.msys.mci.DefaultCrypto;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

/* renamed from: X.1Jm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C27851Jm {
    public static final AnonymousClass253 A00;
    public static final ByteBuffer A01;
    public static final Charset A02 = Charset.forName("ISO-8859-1");
    public static final Charset A03 = Charset.forName(DefaultCrypto.UTF_8);
    public static final byte[] A04;

    static {
        byte[] bArr = new byte[0];
        A04 = bArr;
        A01 = ByteBuffer.wrap(bArr);
        AnonymousClass253 r0 = new AnonymousClass253(bArr, 0, 0);
        try {
            r0.A04(0);
            A00 = r0;
        } catch (C28971Pt e) {
            throw new IllegalArgumentException(e);
        }
    }
}
