package X;

import android.content.Context;
import com.whatsapp.settings.chat.wallpaper.WallpaperCategoriesActivity;

/* renamed from: X.4qq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103494qq implements AbstractC009204q {
    public final /* synthetic */ WallpaperCategoriesActivity A00;

    public C103494qq(WallpaperCategoriesActivity wallpaperCategoriesActivity) {
        this.A00 = wallpaperCategoriesActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
