package X;

import android.content.Context;

/* renamed from: X.5np  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123765np extends AbstractC130195yx {
    public int A00 = 12;
    public AbstractC121025h8 A01;
    public final Context A02;
    public final C14830m7 A03;

    public C123765np(Context context, C14830m7 r3, AnonymousClass018 r4, AnonymousClass14X r5) {
        super(context, r4, r5, 4);
        this.A03 = r3;
        this.A02 = context;
    }

    @Override // X.AbstractC130195yx
    public CharSequence A03() {
        AbstractC121025h8 r0 = this.A01;
        AnonymousClass009.A05(r0);
        AnonymousClass6F2 r3 = r0.A06.A02;
        return r3.A00.AA7(this.A02, r3.A06(this.A06));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0039, code lost:
        if (r0 == false) goto L_0x003e;
     */
    @Override // X.AbstractC130195yx
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A04() {
        /*
            r3 = this;
            int r0 = r3.A08()
            switch(r0) {
                case 1: goto L_0x0042;
                case 2: goto L_0x0030;
                case 3: goto L_0x002a;
                case 4: goto L_0x0024;
                case 5: goto L_0x003c;
                case 6: goto L_0x001e;
                case 7: goto L_0x0018;
                case 8: goto L_0x0042;
                case 9: goto L_0x0012;
                case 10: goto L_0x000c;
                case 11: goto L_0x002a;
                default: goto L_0x0007;
            }
        L_0x0007:
            java.lang.String r0 = super.A04()
            return r0
        L_0x000c:
            android.content.Context r2 = r3.A02
            r1 = 2131889894(0x7f120ee6, float:1.9414464E38)
            goto L_0x0047
        L_0x0012:
            android.content.Context r2 = r3.A02
            r1 = 2131889893(0x7f120ee5, float:1.9414462E38)
            goto L_0x0047
        L_0x0018:
            android.content.Context r2 = r3.A02
            r1 = 2131890678(0x7f1211f6, float:1.9416055E38)
            goto L_0x0047
        L_0x001e:
            android.content.Context r2 = r3.A02
            r1 = 2131890557(0x7f12117d, float:1.941581E38)
            goto L_0x0047
        L_0x0024:
            android.content.Context r2 = r3.A02
            r1 = 2131890677(0x7f1211f5, float:1.9416053E38)
            goto L_0x0047
        L_0x002a:
            android.content.Context r2 = r3.A02
            r1 = 2131890674(0x7f1211f2, float:1.9416046E38)
            goto L_0x0047
        L_0x0030:
            boolean r0 = r3.A0A()
            android.content.Context r2 = r3.A02
            r1 = 2131890684(0x7f1211fc, float:1.9416067E38)
            if (r0 != 0) goto L_0x0047
            goto L_0x003e
        L_0x003c:
            android.content.Context r2 = r3.A02
        L_0x003e:
            r1 = 2131890685(0x7f1211fd, float:1.9416069E38)
            goto L_0x0047
        L_0x0042:
            android.content.Context r2 = r3.A02
            r1 = 2131890689(0x7f121201, float:1.9416077E38)
        L_0x0047:
            java.lang.String r0 = r2.getString(r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C123765np.A04():java.lang.String");
    }

    @Override // X.AbstractC130195yx
    public void A06(AnonymousClass1IR r2) {
        boolean z;
        super.A06(r2);
        AbstractC1316063k r0 = super.A02.A01;
        AnonymousClass009.A05(r0);
        this.A01 = (AbstractC121025h8) r0;
        switch (A08()) {
            case 1:
            case 2:
            case 3:
            case 5:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
                z = false;
                break;
            case 4:
            case 6:
            case 7:
                z = true;
                break;
            default:
                return;
        }
        super.A03 = z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0029, code lost:
        if (r0 == 1) goto L_0x002b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A08() {
        /*
            r5 = this;
            int r4 = r5.A00
            r0 = 12
            if (r4 != r0) goto L_0x0019
            X.5h8 r3 = r5.A01
            r2 = 3
            r1 = 1
            r4 = 2
            if (r3 == 0) goto L_0x0031
            X.63r r0 = r3.A05
            if (r0 == 0) goto L_0x001a
            int r0 = r0.A01
            if (r0 != r4) goto L_0x0029
            r4 = 11
        L_0x0017:
            r5.A00 = r4
        L_0x0019:
            return r4
        L_0x001a:
            X.63o r0 = r3.A03
            if (r0 == 0) goto L_0x0031
            int r0 = r0.A00
            if (r0 == r1) goto L_0x002e
            if (r0 == r4) goto L_0x002b
            if (r0 != r2) goto L_0x0031
            r4 = 10
            goto L_0x0017
        L_0x0029:
            if (r0 != r1) goto L_0x001a
        L_0x002b:
            r4 = 9
            goto L_0x0017
        L_0x002e:
            r4 = 8
            goto L_0x0017
        L_0x0031:
            X.1IR r0 = r5.A01
            int r0 = r0.A02
            switch(r0) {
                case 602: goto L_0x0017;
                case 603: goto L_0x0045;
                case 604: goto L_0x0043;
                case 605: goto L_0x0041;
                case 606: goto L_0x003f;
                case 607: goto L_0x003d;
                case 608: goto L_0x0017;
                case 609: goto L_0x003b;
                default: goto L_0x0038;
            }
        L_0x0038:
            r4 = 12
            goto L_0x0017
        L_0x003b:
            r4 = 5
            goto L_0x0017
        L_0x003d:
            r4 = 4
            goto L_0x0017
        L_0x003f:
            r4 = 6
            goto L_0x0017
        L_0x0041:
            r4 = 7
            goto L_0x0017
        L_0x0043:
            r4 = 3
            goto L_0x0017
        L_0x0045:
            r4 = 1
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C123765np.A08():int");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0018, code lost:
        if (r0 != 1) goto L_0x001a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A09() {
        /*
            r5 = this;
            X.5h8 r4 = r5.A01
            r1 = 0
            if (r4 == 0) goto L_0x000d
            int r0 = r5.A08()
            r3 = 1
            switch(r0) {
                case 1: goto L_0x0011;
                case 2: goto L_0x001e;
                case 3: goto L_0x002b;
                case 4: goto L_0x0038;
                case 5: goto L_0x003e;
                case 6: goto L_0x0044;
                case 7: goto L_0x004a;
                case 8: goto L_0x000e;
                case 9: goto L_0x005c;
                case 10: goto L_0x0050;
                case 11: goto L_0x0056;
                default: goto L_0x000d;
            }
        L_0x000d:
            return r1
        L_0x000e:
            android.content.Context r2 = r5.A02
            goto L_0x001a
        L_0x0011:
            android.content.Context r2 = r5.A02
            int r0 = r4.A02
            r1 = 2131889979(0x7f120f3b, float:1.9414637E38)
            if (r0 == r3) goto L_0x0065
        L_0x001a:
            r1 = 2131890780(0x7f12125c, float:1.9416261E38)
            goto L_0x0065
        L_0x001e:
            int r0 = r4.A02
            if (r0 != r3) goto L_0x0025
            java.lang.String r1 = ""
            return r1
        L_0x0025:
            android.content.Context r2 = r5.A02
            r1 = 2131890781(0x7f12125d, float:1.9416263E38)
            goto L_0x0065
        L_0x002b:
            android.content.Context r2 = r5.A02
            int r0 = r4.A02
            r1 = 2131889975(0x7f120f37, float:1.9414629E38)
            if (r0 != r3) goto L_0x0065
            r1 = 2131889980(0x7f120f3c, float:1.9414639E38)
            goto L_0x0065
        L_0x0038:
            android.content.Context r2 = r5.A02
            r1 = 2131890779(0x7f12125b, float:1.941626E38)
            goto L_0x0065
        L_0x003e:
            android.content.Context r2 = r5.A02
            r1 = 2131890783(0x7f12125f, float:1.9416268E38)
            goto L_0x0065
        L_0x0044:
            android.content.Context r2 = r5.A02
            r1 = 2131890782(0x7f12125e, float:1.9416266E38)
            goto L_0x0065
        L_0x004a:
            android.content.Context r2 = r5.A02
            r1 = 2131890675(0x7f1211f3, float:1.9416049E38)
            goto L_0x0065
        L_0x0050:
            android.content.Context r2 = r5.A02
            r1 = 2131889878(0x7f120ed6, float:1.9414432E38)
            goto L_0x0065
        L_0x0056:
            android.content.Context r2 = r5.A02
            r1 = 2131889880(0x7f120ed8, float:1.9414436E38)
            goto L_0x0065
        L_0x005c:
            X.63r r0 = r4.A05
            if (r0 == 0) goto L_0x000d
            android.content.Context r2 = r5.A02
            r1 = 2131889877(0x7f120ed5, float:1.941443E38)
        L_0x0065:
            java.lang.String r1 = r2.getString(r1)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C123765np.A09():java.lang.String");
    }

    public boolean A0A() {
        AbstractC121025h8 r0 = this.A01;
        AnonymousClass009.A05(r0);
        boolean A1V = C12960it.A1V(r0.A02, 1);
        boolean A1V2 = C12960it.A1V(super.A01.A02, 608);
        if (!A1V || !A1V2) {
            return false;
        }
        return true;
    }
}
