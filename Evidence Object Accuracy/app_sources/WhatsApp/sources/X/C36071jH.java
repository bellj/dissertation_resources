package X;

import java.text.Collator;
import java.util.Comparator;

/* renamed from: X.1jH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C36071jH implements Comparator {
    public final C15570nT A00;
    public final C15610nY A01;
    public final Collator A02;
    public final boolean A03;

    public C36071jH(C15570nT r3, C15610nY r4, boolean z) {
        this.A00 = r3;
        this.A01 = r4;
        this.A03 = z;
        Collator instance = Collator.getInstance(AnonymousClass018.A00(r4.A04.A00));
        instance.setDecomposition(1);
        this.A02 = instance;
    }

    /* renamed from: A00 */
    public int compare(C15370n3 r16, C15370n3 r17) {
        char c;
        char c2;
        C15570nT r2 = this.A00;
        boolean A0F = r2.A0F(r16.A0D);
        if (A0F == r2.A0F(r17.A0D)) {
            C15610nY r4 = this.A01;
            if (r4.A0L(r16, 1)) {
                c = 2;
            } else {
                c = 0;
                if (C15610nY.A03(r16)) {
                    c = 1;
                }
            }
            if (r4.A0L(r17, 1)) {
                c2 = 2;
            } else {
                c2 = 0;
                if (C15610nY.A03(r17)) {
                    c2 = 1;
                }
            }
            if (c == c2) {
                Collator collator = this.A02;
                boolean z = this.A03;
                return collator.compare(r4.A0C(r16, -1, false, z, false), r4.A0C(r17, -1, false, z, false));
            } else if (c < c2) {
                return -1;
            }
        } else if (A0F) {
            return -1;
        }
        return 1;
    }
}
