package X;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* renamed from: X.61N  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass61N {
    public C129845yO A00;
    public C129845yO A01;
    public C129845yO A02;
    public final int A03;
    public final int A04;
    public final int A05;
    public final boolean A06 = true;

    public AnonymousClass61N(int i, int i2, int i3) {
        this.A04 = i;
        this.A03 = i2;
        this.A05 = i3;
    }

    public static double A00(int i, int i2) {
        if (i == 0 || i2 == 0) {
            return 0.0d;
        }
        return ((double) Math.max(i, i2)) / ((double) Math.min(i, i2));
    }

    public static C129845yO A01(C129845yO r9, C129845yO r10) {
        int i = r9.A02;
        int i2 = r9.A01;
        double A00 = A00(i, i2);
        int i3 = r10.A02;
        int i4 = r10.A01;
        if (Math.abs(A00 - A00(i3, i4)) <= ((double) 0.02f)) {
            return r9;
        }
        if ((i - i2) * (i3 - i4) < 0) {
            r10 = new C129845yO(i4, i3);
        }
        float f = (float) r10.A01;
        float f2 = (float) r10.A02;
        int i5 = (int) (((float) i) * (f / f2));
        if (i5 <= i2) {
            return new C129845yO(i, i5);
        }
        return new C129845yO((int) (((float) i2) * (f2 / f)), i2);
    }

    public static C129845yO A02(C129845yO r18, List list, Set set, double d) {
        C129845yO r7 = null;
        int i = r18.A02;
        int i2 = r18.A01;
        int max = Math.max(i, i2);
        int min = Math.min(i, i2);
        if (d <= 0.0d) {
            d = A00(i, i2);
        }
        double d2 = Double.MAX_VALUE;
        Iterator it = list.iterator();
        boolean z = false;
        while (it.hasNext()) {
            C129845yO r6 = (C129845yO) it.next();
            int i3 = r6.A02;
            int i4 = r6.A01;
            int max2 = Math.max(i3, i4);
            int min2 = Math.min(i3, i4);
            double A00 = A00(i3, i4);
            if (set == null || set.isEmpty() || set.contains(Double.valueOf(A00))) {
                if (max2 == max && min2 == min && Math.abs(A00 - d) <= 0.001d) {
                    return r6;
                }
                double abs = Math.abs(d - A00);
                double d3 = abs - d2;
                if (d3 <= 0.001d) {
                    if (Math.abs(d3) > 0.001d) {
                        d2 = abs;
                        r7 = null;
                        z = false;
                    }
                    if (max2 < max || min2 < min) {
                        if (!z) {
                            if (r7 != null && Long.signum((((long) i3) * ((long) i4)) - (((long) r7.A02) * ((long) r7.A01))) <= 0) {
                            }
                            r7 = r6;
                        }
                    } else if (!z) {
                        r7 = r6;
                        z = true;
                    } else if (r7 != null && Long.signum((((long) i3) * ((long) i4)) - (((long) r7.A02) * ((long) r7.A01))) < 0) {
                        r7 = r6;
                    }
                }
            }
        }
        return r7;
    }

    public static List A03(List list, int i) {
        ArrayList A0w = C12980iv.A0w(list.size());
        C129845yO r5 = null;
        for (int i2 = 0; i2 < list.size(); i2++) {
            C129845yO r3 = (C129845yO) list.get(i2);
            if (r3.A02 * r3.A01 <= i) {
                A0w.add(r3);
            }
            if (r5 == null || r3.A02 * r3.A01 < r5.A02 * r5.A01) {
                r5 = r3;
            }
        }
        if (A0w.isEmpty() && r5 != null) {
            A0w.add(r5);
        }
        return A0w;
    }

    /* JADX WARNING: Removed duplicated region for block: B:49:0x00eb  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0102  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0165  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.C127235uA A04(java.util.List r22, java.util.List r23, java.util.List r24, int r25, int r26) {
        /*
        // Method dump skipped, instructions count: 359
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass61N.A04(java.util.List, java.util.List, java.util.List, int, int):X.5uA");
    }
}
