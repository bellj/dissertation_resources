package X;

import org.json.JSONObject;

/* renamed from: X.2SC  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2SC {
    public AnonymousClass20C A00;
    public final /* synthetic */ C50942Ry A01;

    public AnonymousClass2SC(AnonymousClass102 r7, C50942Ry r8, AnonymousClass1V8 r9) {
        AnonymousClass20C r0;
        this.A01 = r8;
        AnonymousClass1V8 A0F = r9.A0F("min_amount").A0F("money");
        AbstractC30791Yv A02 = r7.A02(A0F.A0H("currency"));
        long A06 = (long) A0F.A06(A0F.A0H("value"), "value");
        int A062 = A0F.A06(A0F.A0H("offset"), "offset");
        if (A062 <= 0) {
            r0 = new AnonymousClass20C(A02, A06);
        } else {
            r0 = new AnonymousClass20C(A02, A062, A06);
        }
        this.A00 = r0;
    }

    public AnonymousClass2SC(C50942Ry r6, String str) {
        this.A01 = r6;
        JSONObject jSONObject = new JSONObject(str);
        new AnonymousClass20C(C30771Yt.A06, 0);
        this.A00 = AnonymousClass20C.A00(new JSONObject(jSONObject.getString("min_amount")));
    }
}
