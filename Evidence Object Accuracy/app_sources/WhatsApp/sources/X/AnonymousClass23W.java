package X;

/* renamed from: X.23W  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass23W {
    public int A00 = 0;
    public int A01 = 0;
    public int A02 = 0;
    public long A03 = 0;
    public boolean A04 = false;
    public final AnonymousClass23X[] A05;

    public AnonymousClass23W(int i, int i2) {
        AnonymousClass23X[] r1 = new AnonymousClass23X[i];
        this.A05 = r1;
        for (int i3 = 0; i3 < i; i3++) {
            r1[i3] = new AnonymousClass23X(i2);
        }
    }
}
