package X;

/* renamed from: X.0dS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09750dS implements Runnable {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AnonymousClass052 A01;
    public final /* synthetic */ AnonymousClass0MP A02;

    public RunnableC09750dS(AnonymousClass052 r1, AnonymousClass0MP r2, int i) {
        this.A01 = r1;
        this.A00 = i;
        this.A02 = r2;
    }

    @Override // java.lang.Runnable
    public void run() {
        AnonymousClass05J r0;
        AnonymousClass052 r4 = this.A01;
        int i = this.A00;
        Object obj = this.A02.A00;
        String str = (String) r4.A06.get(Integer.valueOf(i));
        if (str != null) {
            r4.A00.remove(str);
            AnonymousClass06Y r02 = (AnonymousClass06Y) r4.A07.get(str);
            if (r02 == null || (r0 = r02.A00) == null) {
                r4.A02.remove(str);
                r4.A05.put(str, obj);
                return;
            }
            r0.ALs(obj);
        }
    }
}
