package X;

import com.whatsapp.util.Log;

/* renamed from: X.1py  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C39481py {
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ AnonymousClass1BJ A02;

    public C39481py(AnonymousClass1BJ r1, int i, int i2) {
        this.A02 = r1;
        this.A00 = i;
        this.A01 = i2;
    }

    public void A00() {
        AnonymousClass1BJ r3 = this.A02;
        int i = this.A00;
        boolean z = true;
        if (r3.A01(i) != 1) {
            z = false;
        }
        AnonymousClass009.A0F(z);
        Log.e("CategoryManager/onManifestError/manifest was errory");
        r3.A09(2, i);
        r3.A0C(null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0099, code lost:
        if ((r3 + 3600000) <= r5.A07.A00()) goto L_0x009b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(X.C39451pv r9) {
        /*
            r8 = this;
            X.1BJ r5 = r8.A02
            int r6 = r8.A00
            r7 = 0
            int r3 = r8.A01
            int r1 = r5.A01(r6)
            r0 = 1
            if (r1 == r0) goto L_0x000f
            r0 = 0
        L_0x000f:
            X.AnonymousClass009.A0F(r0)
            boolean r0 = r5 instanceof X.AnonymousClass1BI
            if (r0 != 0) goto L_0x00aa
            java.lang.String r2 = "doodle_emoji"
        L_0x0018:
            if (r9 != 0) goto L_0x0033
            java.lang.String r1 = "CategoryManager/onManifestReady/No info in manifest for category "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            com.whatsapp.util.Log.e(r0)
            r1 = 2
        L_0x002c:
            r5.A09(r1, r6)
            r5.A0C(r7)
            return
        L_0x0033:
            java.lang.String r0 = r9.A01
            boolean r0 = r2.equals(r0)
            X.AnonymousClass009.A0F(r0)
            X.1pv r2 = r5.A02()
            if (r2 == 0) goto L_0x0076
            java.lang.String r1 = r2.A02()
            java.lang.String r0 = r9.A02()
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0073
            java.lang.String r1 = r2.A04(r6)
            java.lang.String r0 = r9.A04(r6)
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0076
            boolean r0 = r5.A0D(r6)
            if (r0 == 0) goto L_0x0073
            r0 = 5
            r5.A09(r0, r6)
            java.lang.String r0 = r2.A04(r6)
            r5.A0C(r0)
            r5.A07(r6)
            return
        L_0x0073:
            r5.A06()
        L_0x0076:
            if (r3 != 0) goto L_0x009b
            monitor-enter(r5)
            android.util.SparseArray r0 = r5.A02     // Catch: all -> 0x00ae
            java.lang.Object r0 = r0.get(r6)     // Catch: all -> 0x00ae
            java.lang.Long r0 = (java.lang.Long) r0     // Catch: all -> 0x00ae
            if (r0 != 0) goto L_0x0084
            goto L_0x0089
        L_0x0084:
            long r3 = r0.longValue()     // Catch: all -> 0x00ae
            goto L_0x008b
        L_0x0089:
            r3 = 0
        L_0x008b:
            monitor-exit(r5)
            r0 = 3600000(0x36ee80, double:1.7786363E-317)
            long r3 = r3 + r0
            X.0m7 r0 = r5.A07
            long r1 = r0.A00()
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            r1 = 4
            if (r0 > 0) goto L_0x002c
        L_0x009b:
            r0 = 3
            r5.A09(r0, r6)
            X.0lR r1 = r5.A0E
            com.facebook.redex.RunnableBRunnable0Shape0S0201000_I0 r0 = new com.facebook.redex.RunnableBRunnable0Shape0S0201000_I0
            r0.<init>(r5, r9, r6)
            r1.Ab2(r0)
            return
        L_0x00aa:
            java.lang.String r2 = "filter"
            goto L_0x0018
        L_0x00ae:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C39481py.A01(X.1pv):void");
    }
}
