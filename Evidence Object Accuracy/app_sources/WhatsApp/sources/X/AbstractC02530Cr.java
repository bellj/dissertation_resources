package X;

import java.util.ArrayList;

/* renamed from: X.0Cr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC02530Cr extends AnonymousClass0QV {
    public ArrayList A00 = new ArrayList();

    public abstract void A0I();

    @Override // X.AnonymousClass0QV
    public void A07(AnonymousClass0NX r4) {
        super.A07(r4);
        int size = this.A00.size();
        for (int i = 0; i < size; i++) {
            ((AnonymousClass0QV) this.A00.get(i)).A07(r4);
        }
    }

    @Override // X.AnonymousClass0QV
    public void A0G() {
        this.A00.clear();
        super.A0G();
    }
}
