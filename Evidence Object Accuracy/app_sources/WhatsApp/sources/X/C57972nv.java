package X;

import android.util.Log;
import android.util.Pair;
import java.util.List;

/* renamed from: X.2nv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C57972nv extends AnonymousClass4WH {
    public final /* synthetic */ int A00;
    public final /* synthetic */ String A01;
    public final /* synthetic */ List A02;

    public C57972nv(List list, int i, String str) {
        this.A01 = str;
        this.A00 = i;
        this.A02 = list;
    }

    @Override // X.AnonymousClass4WH
    public /* bridge */ /* synthetic */ void A00(Object obj) {
        Pair A01 = C64943Hn.A01((AnonymousClass28D) obj, this.A01);
        int A05 = C12960it.A05(A01.second);
        if (A05 < 0) {
            Log.w("ComponentTree", "insertChildrenRelativeToId: No existing child found with specified ID in parent. New children have not been added to parent.");
        } else {
            ((List) A01.first).addAll(A05 + this.A00, C64943Hn.A02(this.A02));
        }
    }
}
