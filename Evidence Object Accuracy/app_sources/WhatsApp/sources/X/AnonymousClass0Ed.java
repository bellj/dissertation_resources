package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.redex.IDxCreatorShape0S0000000_I1;

/* renamed from: X.0Ed  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Ed extends C02280As {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape0S0000000_I1(22);
    public int A00;

    public AnonymousClass0Ed(Parcel parcel) {
        super(parcel);
        this.A00 = parcel.readInt();
    }

    public AnonymousClass0Ed(Parcelable parcelable, int i) {
        super(parcelable);
        this.A00 = i;
    }

    @Override // android.view.AbsSavedState, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.A00);
    }
}
