package X;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/* renamed from: X.0Pp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C05450Pp {
    public int A00;
    public C006503b A01;
    public C006503b A02;
    public EnumC03840Ji A03;
    public Set A04;
    public UUID A05;

    public C05450Pp(C006503b r2, C006503b r3, EnumC03840Ji r4, List list, UUID uuid, int i) {
        this.A05 = uuid;
        this.A03 = r4;
        this.A01 = r2;
        this.A04 = new HashSet(list);
        this.A02 = r3;
        this.A00 = i;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && C05450Pp.class == obj.getClass()) {
            C05450Pp r4 = (C05450Pp) obj;
            if (this.A00 == r4.A00 && this.A05.equals(r4.A05) && this.A03 == r4.A03 && this.A01.equals(r4.A01) && this.A04.equals(r4.A04)) {
                return this.A02.equals(r4.A02);
            }
        }
        return false;
    }

    public int hashCode() {
        return (((((((((this.A05.hashCode() * 31) + this.A03.hashCode()) * 31) + this.A01.hashCode()) * 31) + this.A04.hashCode()) * 31) + this.A02.hashCode()) * 31) + this.A00;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("WorkInfo{mId='");
        sb.append(this.A05);
        sb.append('\'');
        sb.append(", mState=");
        sb.append(this.A03);
        sb.append(", mOutputData=");
        sb.append(this.A01);
        sb.append(", mTags=");
        sb.append(this.A04);
        sb.append(", mProgress=");
        sb.append(this.A02);
        sb.append('}');
        return sb.toString();
    }
}
