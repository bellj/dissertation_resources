package X;

import java.util.NoSuchElementException;

/* renamed from: X.28Y  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass28Y extends AnonymousClass28W {
    public int position;
    public final int size;

    public abstract Object get(int i);

    public AnonymousClass28Y(int i, int i2) {
        C28291Mn.A02(i2, i);
        this.size = i;
        this.position = i2;
    }

    @Override // java.util.Iterator, java.util.ListIterator
    public final boolean hasNext() {
        return this.position < this.size;
    }

    @Override // java.util.ListIterator
    public final boolean hasPrevious() {
        return this.position > 0;
    }

    @Override // java.util.Iterator, java.util.ListIterator
    public final Object next() {
        if (hasNext()) {
            int i = this.position;
            this.position = i + 1;
            return get(i);
        }
        throw new NoSuchElementException();
    }

    @Override // java.util.ListIterator
    public final int nextIndex() {
        return this.position;
    }

    @Override // java.util.ListIterator
    public final Object previous() {
        if (hasPrevious()) {
            int i = this.position - 1;
            this.position = i;
            return get(i);
        }
        throw new NoSuchElementException();
    }

    @Override // java.util.ListIterator
    public final int previousIndex() {
        return this.position - 1;
    }
}
