package X;

/* renamed from: X.4cb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C94824cb {
    public long A00;
    public long A01;
    public Object A02;
    public boolean A03;

    public /* synthetic */ C94824cb(Object obj) {
        long currentTimeMillis = System.currentTimeMillis();
        this.A02 = obj;
        this.A01 = -1;
        this.A00 = currentTimeMillis;
        this.A03 = false;
    }

    public /* synthetic */ C94824cb(Object obj, long j, long j2) {
        this.A02 = obj;
        this.A01 = j;
        this.A00 = j2;
        this.A03 = false;
    }

    public C94824cb(Object obj, long j, boolean z) {
        long currentTimeMillis = System.currentTimeMillis();
        this.A02 = obj;
        this.A01 = j;
        this.A00 = currentTimeMillis;
        this.A03 = z;
    }
}
