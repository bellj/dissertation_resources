package X;

/* renamed from: X.1TR  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1TR {
    public int A00 = 0;
    public String A01;

    public AnonymousClass1TR(String str) {
        this.A01 = str;
    }

    public String A00() {
        int i = this.A00;
        if (i == -1) {
            return null;
        }
        String str = this.A01;
        int indexOf = str.indexOf(46, i);
        if (indexOf == -1) {
            String substring = str.substring(i);
            this.A00 = -1;
            return substring;
        }
        String substring2 = str.substring(i, indexOf);
        this.A00 = indexOf + 1;
        return substring2;
    }
}
