package X;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import java.io.File;
import java.io.IOException;

/* renamed from: X.3Wx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC68833Wx implements AbstractC35611iN {
    public final long A00;
    public final long A01;
    public final long A02;
    public final ContentResolver A03;
    public final Uri A04;
    public final String A05;
    public final String A06;

    public AbstractC68833Wx(ContentResolver contentResolver, Uri uri, String str, String str2, long j, long j2, long j3) {
        this.A03 = contentResolver;
        this.A02 = j;
        this.A04 = uri;
        this.A05 = str;
        this.A06 = str2;
        this.A01 = j2;
        this.A00 = j3;
    }

    public Bitmap A00(int i, long j) {
        File file;
        int i2;
        ParcelFileDescriptor openFileDescriptor;
        if (!(this instanceof C616831q)) {
            Uri uri = this.A04;
            ContentResolver contentResolver = this.A03;
            AnonymousClass009.A05(uri);
            Bitmap bitmap = null;
            try {
                openFileDescriptor = contentResolver.openFileDescriptor(uri, "r");
            } catch (IOException | IllegalArgumentException | NullPointerException unused) {
            }
            try {
                Bitmap A02 = AnonymousClass3Il.A02(openFileDescriptor, i, j);
                if (openFileDescriptor != null) {
                    openFileDescriptor.close();
                }
                bitmap = A02;
                if (bitmap == null) {
                    return bitmap;
                }
                if (!(this instanceof C616931r)) {
                    i2 = 0;
                } else {
                    i2 = ((C616931r) this).A00;
                }
                return AnonymousClass3Il.A01(bitmap, i2);
            } catch (Throwable th) {
                if (openFileDescriptor != null) {
                    try {
                        openFileDescriptor.close();
                    } catch (Throwable unused2) {
                    }
                }
                throw th;
            }
        } else {
            String str = this.A05;
            if (str == null) {
                file = null;
            } else {
                file = new File(str);
            }
            return C26521Du.A00(new C38901ot(512, false), file);
        }
    }

    @Override // X.AbstractC35611iN
    public Uri AAE() {
        return this.A04;
    }

    @Override // X.AbstractC35611iN
    public long ACQ() {
        return this.A01;
    }

    @Override // X.AbstractC35611iN
    public /* synthetic */ long ACb() {
        return 0;
    }

    @Override // X.AbstractC35611iN
    public String AES() {
        return this.A06;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof AbstractC68833Wx)) {
            return false;
        }
        return this.A04.equals(((AbstractC68833Wx) obj).A04);
    }

    @Override // X.AbstractC35611iN
    public long getContentLength() {
        return this.A00;
    }

    public int hashCode() {
        return this.A04.hashCode();
    }

    public String toString() {
        return this.A04.toString();
    }
}
