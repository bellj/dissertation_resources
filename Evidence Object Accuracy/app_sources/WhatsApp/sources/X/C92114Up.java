package X;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.maps.internal.IUiSettingsDelegate;

/* renamed from: X.4Up  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C92114Up {
    public final IUiSettingsDelegate A00;

    public C92114Up(IUiSettingsDelegate iUiSettingsDelegate) {
        this.A00 = iUiSettingsDelegate;
    }

    public void A00() {
        try {
            C65873Li r2 = (C65873Li) this.A00;
            Parcel A01 = r2.A01();
            A01.writeInt(0);
            r2.A03(3, A01);
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }
}
