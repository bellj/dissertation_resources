package X;

import java.io.File;

/* renamed from: X.330  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass330 extends AbstractC38591oM {
    public final C43161wW A00;
    public final C14370lK A01;
    public final File A02;
    public final String A03 = "application/octet-stream";
    public final String A04;

    public AnonymousClass330(C15450nH r11, C18790t3 r12, C14950mJ r13, C14850m9 r14, C20110vE r15, C43161wW r16, C14370lK r17, C22600zL r18, File file, String str) {
        super(r11, r12, r13, r14, r15, r18, null);
        this.A04 = str;
        this.A02 = file;
        this.A01 = r17;
        this.A00 = r16;
    }
}
