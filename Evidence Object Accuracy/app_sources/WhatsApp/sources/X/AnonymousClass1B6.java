package X;

import com.whatsapp.util.Log;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Calendar;
import java.util.HashSet;
import org.chromium.net.CronetEngine;

/* renamed from: X.1B6  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1B6 {
    public final C251118d A00;
    public final C16590pI A01;

    public AnonymousClass1B6(C251118d r1, C16590pI r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public final void A00() {
        synchronized (AnonymousClass1B6.class) {
            try {
                CronetEngine.Builder builder = new CronetEngine.Builder(this.A01.A00);
                String str = AnonymousClass029.A07;
                HashSet hashSet = new HashSet();
                for (X509Certificate x509Certificate : AnonymousClass1NT.A01) {
                    try {
                        hashSet.add(MessageDigest.getInstance("SHA-256").digest(x509Certificate.getPublicKey().getEncoded()));
                    } catch (NoSuchAlgorithmException unused) {
                        Log.e("FacebookSslSocketFactory/getSHA256EncodedPublicKeys certificate encoding failed");
                    }
                }
                Calendar instance = Calendar.getInstance();
                instance.add(13, Integer.MAX_VALUE);
                builder.addPublicKeyPins(str, hashSet, true, instance.getTime()).enableHttp2(true).enableHttpCache(0, 0).build();
                throw new RuntimeException("Redex: Unreachable code after no-return invoke");
            } catch (Exception e) {
                Log.e("CronetEngineProvider/buildCronetEngine cronet engine building failed", e);
            }
        }
    }
}
