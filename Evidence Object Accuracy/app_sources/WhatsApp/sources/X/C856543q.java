package X;

/* renamed from: X.43q  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C856543q extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Integer A02;
    public Integer A03;
    public Integer A04;
    public Integer A05;
    public Integer A06;
    public Integer A07;
    public Long A08;
    public String A09;

    public C856543q() {
        super(1522, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(9, this.A02);
        r3.Abe(10, this.A03);
        r3.Abe(6, this.A09);
        r3.Abe(12, this.A00);
        r3.Abe(11, this.A01);
        r3.Abe(5, this.A04);
        r3.Abe(8, this.A05);
        r3.Abe(4, this.A08);
        r3.Abe(1, this.A06);
        r3.Abe(2, this.A07);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamViewBusinessProfile {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "bizFbSize", C12960it.A0Y(this.A02));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "bizIgSize", C12960it.A0Y(this.A03));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "catalogSessionId", this.A09);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "hasCoverPhoto", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "isProfileLinked", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "linkedAccount", C12960it.A0Y(this.A04));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "profileEntryPoint", C12960it.A0Y(this.A05));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "scrollDepth", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "viewBusinessProfileAction", C12960it.A0Y(this.A06));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "websiteSource", C12960it.A0Y(this.A07));
        return C12960it.A0d("}", A0k);
    }
}
