package X;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.3aW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69963aW implements AnonymousClass5Wu {
    public ImageView A00;
    public ImageView A01;
    public TextView A02;
    public final AnonymousClass018 A03;
    public final C14850m9 A04;
    public final C22460z7 A05;
    public final AbstractC116085Ub A06;

    public C69963aW(AnonymousClass018 r1, C14850m9 r2, C22460z7 r3, AbstractC116085Ub r4) {
        this.A04 = r2;
        this.A03 = r1;
        this.A05 = r3;
        this.A06 = r4;
    }

    @Override // X.AnonymousClass5Wu
    public /* bridge */ /* synthetic */ void A6Q(Object obj) {
        ImageView imageView;
        C30921Zi A01;
        AnonymousClass1IR r11 = (AnonymousClass1IR) obj;
        this.A02.setVisibility(8);
        if (r11 != null && r11.A08 != null) {
            TextView textView = this.A02;
            textView.setText(AnonymousClass14X.A02(textView.getContext(), this.A03, r11.A00(), r11.A08));
            this.A02.setVisibility(0);
            TextView textView2 = this.A02;
            C12980iv.A14(textView2.getResources(), textView2, R.color.payments_currency_amount_text_color);
            this.A02.setAlpha(1.0f);
            boolean Adb = this.A06.Adb(r11);
            TextView textView3 = this.A02;
            if (Adb) {
                C92984Yl.A00(textView3);
            } else {
                C92984Yl.A01(textView3);
            }
            C14850m9 r1 = this.A04;
            if ((r1.A07(605) || r1.A07(629)) && (A01 = r11.A01()) != null) {
                this.A00.setImageDrawable(null);
                this.A02.setTextColor(A01.A0C);
                if (Adb) {
                    this.A02.setAlpha(0.54f);
                }
                this.A00.setBackgroundColor(A01.A0A);
                ViewGroup.LayoutParams layoutParams = this.A00.getLayoutParams();
                layoutParams.height = (int) (((float) layoutParams.width) / (((float) A01.A0D) / ((float) A01.A09)));
                this.A00.requestLayout();
                String str = A01.A01;
                if (!TextUtils.isEmpty(str)) {
                    this.A00.setContentDescription(str);
                }
                this.A05.A01(this.A00, A01, layoutParams.width, layoutParams.height, false);
                this.A00.setVisibility(0);
                imageView = this.A01;
            } else {
                this.A01.setVisibility(0);
                imageView = this.A00;
            }
            imageView.setVisibility(8);
        }
    }

    @Override // X.AnonymousClass5Wu
    public int ADq() {
        return R.layout.conversation_row_payment_amount_summary;
    }

    @Override // X.AnonymousClass5Wu
    public void AYL(View view) {
        this.A02 = C12960it.A0I(view, R.id.amount_container);
        this.A01 = C12970iu.A0K(view, R.id.conversation_row_payment_pattern);
        this.A00 = C12970iu.A0K(view, R.id.conversation_row_expressive_payment_background);
    }
}
