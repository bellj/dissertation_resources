package X;

import com.whatsapp.businesscollection.view.activity.CollectionProductListActivity;

/* renamed from: X.3VY  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3VY implements AbstractC116525Vu {
    public final /* synthetic */ CollectionProductListActivity A00;

    public AnonymousClass3VY(CollectionProductListActivity collectionProductListActivity) {
        this.A00 = collectionProductListActivity;
    }

    @Override // X.AbstractC116525Vu
    public void ARn(C44691zO r3, long j) {
        CollectionProductListActivity collectionProductListActivity = this.A00;
        C12960it.A0w(((ActivityC13810kN) collectionProductListActivity).A00, ((ActivityC13830kP) collectionProductListActivity).A01, j);
    }

    @Override // X.AbstractC116525Vu
    public void AUV(C44691zO r9, String str, String str2, int i, long j) {
        C53802fF r0 = ((AbstractActivityC59392ue) this.A00).A0E;
        r0.A03.A01(r9, r0.A04, str, str2, j);
    }
}
