package X;

import android.content.Intent;
import android.net.Uri;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import com.whatsapp.payments.ui.stepup.NoviTextInputStepUpActivity;

/* renamed from: X.5a5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117515a5 extends ClickableSpan {
    public final /* synthetic */ NoviTextInputStepUpActivity A00;
    public final /* synthetic */ String A01;

    public C117515a5(NoviTextInputStepUpActivity noviTextInputStepUpActivity, String str) {
        this.A00 = noviTextInputStepUpActivity;
        this.A01 = str;
    }

    @Override // android.text.style.ClickableSpan
    public void onClick(View view) {
        NoviTextInputStepUpActivity noviTextInputStepUpActivity = this.A00;
        Uri A01 = new AnonymousClass608(noviTextInputStepUpActivity.A0D, "139701971311671").A01();
        C118125bJ r4 = noviTextInputStepUpActivity.A0M;
        AnonymousClass610 r2 = new AnonymousClass610("HELP_LINK_CLICK", "TEXT_INPUT", "LINK");
        String str = this.A01;
        C128365vz r1 = r2.A00;
        r1.A0L = str;
        r1.A0R = A01.toString();
        r4.A04(r2);
        noviTextInputStepUpActivity.startActivity(new Intent("android.intent.action.VIEW", A01));
    }

    @Override // android.text.style.ClickableSpan, android.text.style.CharacterStyle
    public void updateDrawState(TextPaint textPaint) {
        C117295Zj.A0k(this.A00.getResources(), textPaint);
    }
}
