package X;

import com.facebook.redex.RunnableBRunnable0Shape11S0200000_I1_1;
import com.whatsapp.gifvideopreview.GifVideoPreviewActivity;
import java.io.File;
import java.lang.ref.WeakReference;

/* renamed from: X.3XP  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3XP implements AbstractC39381po {
    public final WeakReference A00;

    @Override // X.AbstractC39381po
    public void AQA(Exception exc) {
    }

    public /* synthetic */ AnonymousClass3XP(GifVideoPreviewActivity gifVideoPreviewActivity) {
        this.A00 = C12970iu.A10(gifVideoPreviewActivity);
    }

    @Override // X.AbstractC39381po
    public void AQX(File file, String str, byte[] bArr) {
        AnonymousClass2GL r1 = (AnonymousClass2GL) this.A00.get();
        if (file == null) {
            if (r1 != null) {
                r1.A00.setVisibility(8);
            }
        } else if (r1 != null) {
            r1.A02.postDelayed(new RunnableBRunnable0Shape11S0200000_I1_1(r1, 28, file), 50);
        }
    }
}
