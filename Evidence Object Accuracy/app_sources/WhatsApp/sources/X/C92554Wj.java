package X;

/* renamed from: X.4Wj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C92554Wj {
    public final C94244bU A00;
    public final AnonymousClass28D A01;

    public /* synthetic */ C92554Wj(C94244bU r1, AnonymousClass28D r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof C92554Wj)) {
            return false;
        }
        C92554Wj r4 = (C92554Wj) obj;
        if (this.A01 != r4.A01 || !this.A00.equals(r4.A00)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return this.A01.hashCode() ^ this.A00.hashCode();
    }
}
