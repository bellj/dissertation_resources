package X;

import android.content.Context;
import android.content.res.Resources;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.facebook.redex.ViewOnClickCListenerShape1S0400000_I1;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaTextView;
import com.whatsapp.polls.PollVoterViewModel;
import java.util.List;

/* renamed from: X.2yf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60802yf extends AnonymousClass2xi {
    public long A00 = -1;
    public PollVoterViewModel A01;
    public List A02;
    public final ViewGroup A03;
    public final LinearLayout A04;
    public final TextEmojiLabel A05;
    public final WaTextView A06;
    public final WaTextView A07;
    public final AnonymousClass1HC A08;
    public final List A09 = C12960it.A0l();

    public C60802yf(Context context, AbstractC13890kV r12, PollVoterViewModel pollVoterViewModel, C27671Iq r14) {
        super(context, r12, r14);
        this.A01 = pollVoterViewModel;
        ViewGroup viewGroup = (ViewGroup) AnonymousClass028.A0D(this, R.id.poll_text_row);
        this.A03 = viewGroup;
        this.A05 = C12970iu.A0T(this, R.id.poll_name);
        this.A04 = (LinearLayout) AnonymousClass028.A0D(this, R.id.poll_options);
        this.A08 = new AnonymousClass1HC(AnonymousClass028.A0D(this, R.id.invalid_poll_text));
        WaTextView A0N = C12960it.A0N(this, R.id.poll_vote);
        this.A07 = A0N;
        WaTextView A0N2 = C12960it.A0N(this, R.id.poll_change_vote);
        this.A06 = A0N2;
        C12960it.A13(A0N, this, pollVoterViewModel, 38);
        C12960it.A13(A0N2, this, pollVoterViewModel, 39);
        viewGroup.setOnClickListener(new ViewOnClickCListenerShape1S0400000_I1(this, pollVoterViewModel, r14, context, 2));
        viewGroup.setOnLongClickListener(this.A1a);
        A1N(false, false);
    }

    @Override // X.AnonymousClass1OY
    public void A0s() {
        A1H(false);
        A1N(false, true);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0016, code lost:
        if (r0 == false) goto L_0x0012;
     */
    @Override // X.AnonymousClass1OY
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A1D(X.AbstractC15340mz r4, boolean r5) {
        /*
            r3 = this;
            X.0mz r0 = r3.getFMessage()
            r2 = 1
            r1 = 0
            boolean r0 = X.C12960it.A1X(r4, r0)
            super.A1D(r4, r5)
            if (r5 != 0) goto L_0x0016
            if (r0 == 0) goto L_0x0015
        L_0x0011:
            r2 = 0
        L_0x0012:
            r3.A1N(r1, r2)
        L_0x0015:
            return
        L_0x0016:
            if (r0 != 0) goto L_0x0011
            goto L_0x0012
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C60802yf.A1D(X.0mz, boolean):void");
    }

    public void A1M(C27671Iq r4, boolean z) {
        int i;
        WaTextView waTextView = this.A07;
        Resources resources = getResources();
        int i2 = R.color.poll_vote_no_selection;
        if (z) {
            i2 = R.color.poll_vote_selection;
        }
        C12980iv.A14(resources, waTextView, i2);
        waTextView.setClickable(z);
        PollVoterViewModel pollVoterViewModel = this.A01;
        if (pollVoterViewModel == null || r4 == null || !r4.equals(pollVoterViewModel.A00) || !pollVoterViewModel.A01) {
            i = R.string.poll_vote;
        } else {
            int size = pollVoterViewModel.A0B.size();
            i = R.string.poll_remove_vote;
            if (size > 0) {
                i = R.string.poll_submit;
            }
        }
        waTextView.setText(i);
    }

    public final void A1N(boolean z, boolean z2) {
        C27671Iq r4 = (C27671Iq) getFMessage();
        String str = r4.A02;
        if (str != null) {
            setMessageText(str, this.A05, r4);
        }
        if (this.A01 != null) {
            RunnableC55532ik r3 = new Runnable(r4, z, z2) { // from class: X.2ik
                public final /* synthetic */ C27671Iq A01;
                public final /* synthetic */ boolean A02;
                public final /* synthetic */ boolean A03;

                {
                    this.A01 = r2;
                    this.A02 = r3;
                    this.A03 = r4;
                }

                /* JADX WARNING: Code restructure failed: missing block: B:100:0x0212, code lost:
                    if (r1.A0B.size() < r2.size()) goto L_0x0214;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:24:0x0067, code lost:
                    if (r1.A01 != false) goto L_0x0073;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:28:0x0071, code lost:
                    if (r0.size() == 0) goto L_0x0073;
                 */
                /* JADX WARNING: Removed duplicated region for block: B:119:0x0267 A[LOOP:3: B:117:0x0261->B:119:0x0267, LOOP_END] */
                /* JADX WARNING: Removed duplicated region for block: B:123:0x02c2 A[LOOP:4: B:121:0x02bc->B:123:0x02c2, LOOP_END] */
                /* JADX WARNING: Removed duplicated region for block: B:32:0x008b  */
                /* JADX WARNING: Removed duplicated region for block: B:48:0x00d2  */
                /* JADX WARNING: Removed duplicated region for block: B:60:0x00fe A[LOOP:1: B:58:0x00f8->B:60:0x00fe, LOOP_END] */
                /* JADX WARNING: Removed duplicated region for block: B:64:0x0112  */
                @Override // java.lang.Runnable
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public final void run() {
                    /*
                    // Method dump skipped, instructions count: 762
                    */
                    throw new UnsupportedOperationException("Method not decompiled: X.RunnableC55532ik.run():void");
                }
            };
            this.A04.setTag(r4.A0z);
            C26701Em r2 = this.A1H;
            if (!C26701Em.A00(r4, (byte) 67)) {
                r3.run();
            } else {
                r2.A02(r4, r3, (byte) 67);
            }
        }
    }

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_poll_left;
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_poll_left;
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_poll_right;
    }

    public List getPollVoteSelectedOptionIds() {
        return this.A02;
    }

    public void setAllCheckboxCheckable(boolean z) {
        for (C63473Bs r0 : this.A09) {
            r0.A02 = z;
        }
    }

    @Override // X.AbstractC28551Oa
    public void setFMessage(AbstractC15340mz r2) {
        AnonymousClass009.A0F(r2 instanceof C27671Iq);
        ((AbstractC28551Oa) this).A0O = r2;
    }
}
