package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2m9  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2m9 extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass2m9 A01;
    public static volatile AnonymousClass255 A02;
    public AnonymousClass1K6 A00 = AnonymousClass277.A01;

    static {
        AnonymousClass2m9 r0 = new AnonymousClass2m9();
        A01 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r3, Object obj, Object obj2) {
        switch (r3.ordinal()) {
            case 0:
                return A01;
            case 1:
                this.A00 = ((AbstractC462925h) obj).Afr(this.A00, ((AnonymousClass2m9) obj2).A00);
                return this;
            case 2:
                AnonymousClass253 r4 = (AnonymousClass253) obj;
                AnonymousClass254 r5 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        try {
                            int A03 = r4.A03();
                            if (A03 == 0) {
                                break;
                            } else if (A03 == 10) {
                                AnonymousClass1K6 r1 = this.A00;
                                if (!((AnonymousClass1K7) r1).A00) {
                                    r1 = AbstractC27091Fz.A0G(r1);
                                    this.A00 = r1;
                                }
                                r1.add((C34851go) AbstractC27091Fz.A0H(r4, r5, C34851go.A02));
                            } else if (!A0a(r4, A03)) {
                                break;
                            }
                        } catch (IOException e) {
                            throw AbstractC27091Fz.A0K(this, e);
                        }
                    } catch (C28971Pt e2) {
                        throw AbstractC27091Fz.A0J(e2, this);
                    }
                }
            case 3:
                AbstractC27091Fz.A0R(this.A00);
                return null;
            case 4:
                return new AnonymousClass2m9();
            case 5:
                return new C81743uV();
            case 6:
                break;
            case 7:
                if (A02 == null) {
                    synchronized (AnonymousClass2m9.class) {
                        if (A02 == null) {
                            A02 = AbstractC27091Fz.A09(A01);
                        }
                    }
                }
                return A02;
            default:
                throw C12970iu.A0z();
        }
        return A01;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < this.A00.size(); i3++) {
            i2 = AbstractC27091Fz.A08((AnonymousClass1G1) this.A00.get(i3), 1, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        for (int i = 0; i < this.A00.size(); i++) {
            codedOutputStream.A0L((AnonymousClass1G1) this.A00.get(i), 1);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
