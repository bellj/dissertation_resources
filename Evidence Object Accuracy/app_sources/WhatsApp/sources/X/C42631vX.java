package X;

import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;

/* renamed from: X.1vX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C42631vX {
    public static void A00(View view) {
        TypedValue typedValue = new TypedValue();
        view.getContext().getTheme().resolveAttribute(16843534, typedValue, true);
        view.setBackgroundResource(typedValue.resourceId);
    }

    public static void A01(View view, int i, int i2) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams != null) {
            layoutParams.width = i;
            layoutParams.height = i2;
            view.setLayoutParams(layoutParams);
        }
    }
}
