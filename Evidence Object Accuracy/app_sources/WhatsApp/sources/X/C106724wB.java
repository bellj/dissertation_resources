package X;

import android.util.SparseArray;

/* renamed from: X.4wB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C106724wB implements AbstractC116785Ww {
    public long A00;
    public AbstractC14070ko A01;
    public C76813mH A02;
    public boolean A03;
    public boolean A04;
    public boolean A05;
    public boolean A06;
    public final SparseArray A07 = new SparseArray();
    public final C93764ah A08 = new C93764ah();
    public final C95304dT A09 = C95304dT.A05(4096);
    public final AnonymousClass4YB A0A = new AnonymousClass4YB(0);

    @Override // X.AbstractC116785Ww
    public void AIa(AbstractC14070ko r1) {
        this.A01 = r1;
    }

    @Override // X.AbstractC116785Ww
    public int AZn(AnonymousClass5Yf r24, AnonymousClass4IG r25) {
        long j;
        int A03;
        int A0F;
        long j2;
        AnonymousClass5X7 r2;
        AnonymousClass5WY r4;
        long j3;
        long j4;
        AbstractC14070ko r12 = this.A01;
        C95314dV.A01(r12);
        long length = r24.getLength();
        if (length != -1) {
            C93764ah r22 = this.A08;
            if (!r22.A03) {
                if (!r22.A05) {
                    int min = (int) Math.min(20000L, length);
                    long j5 = length - ((long) min);
                    if (r24.AFo() != j5) {
                        r25.A00 = j5;
                        return 1;
                    }
                    C95304dT r11 = r22.A06;
                    r11.A0Q(min);
                    r24.Aaj();
                    C95304dT.A06(r24, r11, min);
                    int i = r11.A01;
                    int i2 = r11.A00 - 4;
                    while (true) {
                        if (i2 < i) {
                            j4 = -9223372036854775807L;
                            break;
                        }
                        if (C72453ed.A0J(r11.A02, i2) == 442) {
                            r11.A0S(i2 + 4);
                            j4 = C93764ah.A00(r11);
                            if (j4 != -9223372036854775807L) {
                                break;
                            }
                        }
                        i2--;
                    }
                    r22.A02 = j4;
                    r22.A05 = true;
                    return 0;
                }
                if (r22.A02 != -9223372036854775807L) {
                    if (!r22.A04) {
                        int min2 = (int) Math.min(20000L, length);
                        long j6 = (long) 0;
                        if (r24.AFo() != j6) {
                            r25.A00 = j6;
                            return 1;
                        }
                        C95304dT r8 = r22.A06;
                        r8.A0Q(min2);
                        r24.Aaj();
                        C95304dT.A06(r24, r8, min2);
                        int i3 = r8.A01;
                        int i4 = r8.A00;
                        while (true) {
                            if (i3 >= i4 - 3) {
                                j3 = -9223372036854775807L;
                                break;
                            }
                            if (C72453ed.A0J(r8.A02, i3) == 442) {
                                r8.A0S(i3 + 4);
                                j3 = C93764ah.A00(r8);
                                if (j3 != -9223372036854775807L) {
                                    break;
                                }
                            }
                            i3++;
                        }
                        r22.A01 = j3;
                        r22.A04 = true;
                        return 0;
                    }
                    long j7 = r22.A01;
                    if (j7 != -9223372036854775807L) {
                        AnonymousClass4YB r5 = r22.A07;
                        r22.A00 = r5.A03(r22.A02) - r5.A03(j7);
                    }
                }
                C95304dT r3 = r22.A06;
                byte[] bArr = AnonymousClass3JZ.A0A;
                r3.A0U(bArr, bArr.length);
                r22.A03 = true;
                r24.Aaj();
                return 0;
            }
        }
        if (!this.A06) {
            this.A06 = true;
            C93764ah r1 = this.A08;
            long j8 = r1.A00;
            if (j8 != -9223372036854775807L) {
                C76813mH r0 = new C76813mH(r1.A07, j8, length);
                this.A02 = r0;
                r4 = r0.A02;
            } else {
                r4 = new C106904wT(j8, 0);
            }
            r12.AbR(r4);
        }
        C76813mH r13 = this.A02;
        if (r13 != null && r13.A00 != null) {
            return r13.A00(r24, r25);
        }
        r24.Aaj();
        if (length != -1) {
            j = length - r24.AFf();
        } else {
            j = -1;
        }
        if (j == -1 || j >= 4) {
            C95304dT r42 = this.A09;
            if (r24.AZ5(r42.A02, 0, 4, true) && (A03 = C95304dT.A03(r42, 0)) != 441) {
                if (A03 == 442) {
                    r24.AZ4(r42.A02, 0, 10);
                    r42.A0S(9);
                    A0F = (r42.A0C() & 7) + 14;
                } else if (A03 == 443) {
                    C95304dT.A06(r24, r42, 2);
                    r42.A0S(0);
                    A0F = r42.A0F() + 6;
                } else if (((A03 & -256) >> 8) != 1) {
                    r24.Ae3(1);
                    return 0;
                } else {
                    int i5 = A03 & 255;
                    SparseArray sparseArray = this.A07;
                    AnonymousClass4T3 r7 = (AnonymousClass4T3) sparseArray.get(i5);
                    if (!this.A03) {
                        if (r7 == null) {
                            if (i5 == 189) {
                                r2 = new C107134wq(null);
                                this.A04 = true;
                                this.A00 = r24.AFo();
                            } else if ((i5 & 224) == 192) {
                                r2 = new C107154ws(null);
                                this.A04 = true;
                                this.A00 = r24.AFo();
                            } else if ((i5 & 240) == 224) {
                                r2 = new C107194ww(null);
                                this.A05 = true;
                                this.A00 = r24.AFo();
                            }
                            r2.A8b(this.A01, new C92824Xo(Integer.MIN_VALUE, i5, 256));
                            r7 = new AnonymousClass4T3(r2, this.A0A);
                            sparseArray.put(i5, r7);
                        }
                        if (!this.A04 || !this.A05) {
                            j2 = 1048576;
                        } else {
                            j2 = this.A00 + 8192;
                        }
                        if (r24.AFo() > j2) {
                            this.A03 = true;
                            this.A01.A9V();
                        }
                    }
                    C95304dT.A06(r24, r42, 2);
                    r42.A0S(0);
                    A0F = r42.A0F() + 6;
                    if (r7 != null) {
                        r42.A0Q(A0F);
                        r24.readFully(r42.A02, 0, A0F);
                        r42.A0S(6);
                        C95054d0 r82 = r7.A04;
                        r42.A0V(r82.A03, 0, 3);
                        r82.A07(0);
                        r82.A08(8);
                        r7.A01 = r82.A0C();
                        r7.A00 = r82.A0C();
                        r42.A0V(r82.A03, 0, C95054d0.A01(r82, 6, 8));
                        r82.A07(0);
                        long j9 = 0;
                        if (r7.A01) {
                            r82.A08(4);
                            r82.A08(1);
                            long A02 = C95054d0.A02(r82, ((long) r82.A04(3)) << 30);
                            if (!r7.A02 && r7.A00) {
                                r82.A08(1);
                                r7.A05.A03(C95054d0.A02(r82, ((long) C95054d0.A01(r82, 4, 3)) << 30));
                                r7.A02 = true;
                            }
                            j9 = r7.A05.A03(A02);
                        }
                        AnonymousClass5X7 r32 = r7.A03;
                        r32.AYp(j9, 4);
                        r32.A7a(r42);
                        r32.AYo();
                        r42.A0R(r42.A02.length);
                        return 0;
                    }
                }
                r24.Ae3(A0F);
                return 0;
            }
        }
        return -1;
    }

    @Override // X.AbstractC116785Ww
    public void AbQ(long j, long j2) {
        AnonymousClass4YB r6 = this.A0A;
        int i = 0;
        if (r6.A01() == -9223372036854775807L || !(r6.A00() == 0 || r6.A00() == j2)) {
            synchronized (r6) {
                r6.A00 = j2;
                r6.A01 = -9223372036854775807L;
            }
        }
        C76813mH r0 = this.A02;
        if (r0 != null) {
            r0.A01(j2);
        }
        while (true) {
            SparseArray sparseArray = this.A07;
            if (i < sparseArray.size()) {
                AnonymousClass4T3 r1 = (AnonymousClass4T3) sparseArray.valueAt(i);
                r1.A02 = false;
                r1.A03.AbP();
                i++;
            } else {
                return;
            }
        }
    }

    @Override // X.AbstractC116785Ww
    public boolean Ae5(AnonymousClass5Yf r10) {
        byte[] bArr = new byte[14];
        r10.AZ4(bArr, 0, 14);
        if (442 != (((bArr[0] & 255) << 24) | ((bArr[1] & 255) << 16) | ((bArr[2] & 255) << 8) | (bArr[3] & 255)) || (bArr[4] & 196) != 68 || (bArr[6] & 4) != 4 || (bArr[8] & 4) != 4 || (bArr[9] & 1) != 1 || (bArr[12] & 3) != 3) {
            return false;
        }
        r10.A5r(bArr[13] & 7);
        r10.AZ4(bArr, 0, 3);
        if (1 == (((bArr[0] & 255) << 16) | ((bArr[1] & 255) << 8) | (bArr[2] & 255))) {
            return true;
        }
        return false;
    }
}
