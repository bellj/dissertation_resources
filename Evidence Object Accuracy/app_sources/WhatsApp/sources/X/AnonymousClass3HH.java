package X;

import java.util.ArrayList;

/* renamed from: X.3HH  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3HH {
    public static final ArrayList A07;
    public static final ArrayList A08;
    public static final ArrayList A09;
    public final AnonymousClass1V8 A00;
    public final AnonymousClass3EM A01;
    public final String A02;
    public final String A03;
    public final String A04;
    public final String A05;
    public final String A06;

    static {
        String[] strArr = new String[3];
        strArr[0] = "FBPAY";
        strArr[1] = "NOVI";
        A09 = C12960it.A0m("UPI", strArr, 2);
        String[] strArr2 = new String[2];
        strArr2[0] = "0";
        A07 = C12960it.A0m("1", strArr2, 1);
        String[] strArr3 = new String[2];
        strArr3[0] = "0";
        A08 = C12960it.A0m("1", strArr3, 1);
    }

    public AnonymousClass3HH(AbstractC15710nm r16, AnonymousClass1V8 r17, AnonymousClass3GZ r18) {
        AnonymousClass1V8.A01(r17, "iq");
        AnonymousClass1V8 r3 = r18.A00;
        Long A0j = C12970iu.A0j();
        Long A0k = C12970iu.A0k();
        this.A04 = (String) AnonymousClass3JT.A03(null, r17, String.class, A0j, A0k, "1", new String[]{"accept_pay", "outage"}, false);
        this.A05 = (String) AnonymousClass3JT.A03(null, r17, String.class, A0j, A0k, "1", new String[]{"accept_pay", "sandbox"}, false);
        this.A06 = AnonymousClass3JT.A08(r17, A09, new String[]{"accept_pay", "service"});
        this.A02 = AnonymousClass3JT.A09(r17, A07, new String[]{"accept_pay", "consumer"});
        this.A03 = AnonymousClass3JT.A09(r17, A08, new String[]{"accept_pay", "merchant"});
        this.A01 = (AnonymousClass3EM) AnonymousClass3JT.A05(r17, new AbstractC116095Uc(r16, r3) { // from class: X.58r
            public final /* synthetic */ AbstractC15710nm A00;
            public final /* synthetic */ AnonymousClass1V8 A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC116095Uc
            public final Object A63(AnonymousClass1V8 r4) {
                return new AnonymousClass3EM(this.A00, r4, this.A01);
            }
        }, new String[0]);
        this.A00 = r17;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || AnonymousClass3HH.class != obj.getClass()) {
                return false;
            }
            AnonymousClass3HH r5 = (AnonymousClass3HH) obj;
            if (!C29941Vi.A00(this.A06, r5.A06) || !this.A02.equals(r5.A02) || !this.A03.equals(r5.A03) || !C29941Vi.A00(this.A04, r5.A04) || !C29941Vi.A00(this.A05, r5.A05) || !this.A01.equals(r5.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] objArr = new Object[6];
        objArr[0] = this.A06;
        objArr[1] = this.A02;
        objArr[2] = this.A03;
        objArr[3] = this.A04;
        objArr[4] = this.A05;
        return C12980iv.A0B(this.A01, objArr, 5);
    }
}
