package X;

import android.view.View;

/* renamed from: X.0P8  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0P8 {
    public C05080Oe A00 = new C05080Oe();
    public final AbstractC12650iG A01;

    public AnonymousClass0P8(AbstractC12650iG r2) {
        this.A01 = r2;
    }

    public View A00(int i, int i2, int i3, int i4) {
        AbstractC12650iG r6 = this.A01;
        int AEy = r6.AEy();
        int AEw = r6.AEw();
        int i5 = -1;
        if (i2 > i) {
            i5 = 1;
        }
        View view = null;
        while (i != i2) {
            View ABJ = r6.ABJ(i);
            int ABN = r6.ABN(ABJ);
            int ABL = r6.ABL(ABJ);
            C05080Oe r2 = this.A00;
            r2.A04 = AEy;
            r2.A03 = AEw;
            r2.A02 = ABN;
            r2.A01 = ABL;
            r2.A00 = 0;
            r2.A00 = i3 | 0;
            if (r2.A00()) {
                return ABJ;
            }
            if (i4 != 0) {
                r2.A00 = 0;
                r2.A00 = i4 | 0;
                if (r2.A00()) {
                    view = ABJ;
                }
            }
            i += i5;
        }
        return view;
    }

    public boolean A01(View view) {
        C05080Oe r4 = this.A00;
        AbstractC12650iG r0 = this.A01;
        int AEy = r0.AEy();
        int AEw = r0.AEw();
        int ABN = r0.ABN(view);
        int ABL = r0.ABL(view);
        r4.A04 = AEy;
        r4.A03 = AEw;
        r4.A02 = ABN;
        r4.A01 = ABL;
        r4.A00 = 0;
        r4.A00 = 24579;
        return r4.A00();
    }
}
