package X;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.whatsapp.R;
import java.util.Iterator;

/* renamed from: X.5lo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122655lo extends AbstractC118835cS {
    public final LinearLayout A00;
    public final LinearLayout A01;
    public final ShimmerFrameLayout A02;

    public C122655lo(View view) {
        super(view);
        this.A00 = C117305Zk.A07(view, R.id.instructions);
        this.A01 = C117305Zk.A07(view, R.id.instructions_static_shimmer);
        this.A02 = (ShimmerFrameLayout) AnonymousClass028.A0D(view, R.id.instructions_shimmer);
    }

    @Override // X.AbstractC118835cS
    public void A08(AbstractC125975s7 r9, int i) {
        C123115md r92 = (C123115md) r9;
        LinearLayout linearLayout = this.A00;
        linearLayout.removeAllViews();
        View view = this.A0H;
        int dimension = (int) view.getResources().getDimension(R.dimen.payment_settings_card_separator_height);
        Iterator it = r92.A01.iterator();
        while (it.hasNext()) {
            String A0x = C12970iu.A0x(it);
            TextView textView = new TextView(view.getContext());
            textView.setText(A0x);
            textView.setPadding(0, dimension, 0, dimension);
            textView.setGravity(8388611);
            textView.setTextSize(14.0f);
            textView.setLineSpacing(6.0f, 1.0f);
            C12960it.A0s(textView.getContext(), textView, R.color.settings_item_subtitle_text);
            linearLayout.addView(textView);
        }
        int i2 = r92.A00;
        ShimmerFrameLayout shimmerFrameLayout = this.A02;
        if (i2 == 1) {
            shimmerFrameLayout.A01();
            this.A01.setVisibility(0);
            return;
        }
        shimmerFrameLayout.A00();
        this.A01.setVisibility(8);
    }
}
