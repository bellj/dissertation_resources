package X;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

/* renamed from: X.3cZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C71213cZ<T> implements Collection<T>, AbstractC16910px {
    public final boolean A00;
    public final Object[] A01;

    @Override // java.util.Collection
    public Object[] toArray(Object[] objArr) {
        C16700pc.A0E(objArr, 0);
        return AnonymousClass3IA.A01(this, objArr);
    }

    public C71213cZ(Object[] objArr, boolean z) {
        this.A01 = objArr;
        this.A00 = z;
    }

    @Override // java.util.Collection
    public boolean add(Object obj) {
        throw C12980iv.A0u("Operation is not supported for read-only collection");
    }

    @Override // java.util.Collection
    public boolean addAll(Collection collection) {
        throw C12980iv.A0u("Operation is not supported for read-only collection");
    }

    @Override // java.util.Collection
    public void clear() {
        throw C12980iv.A0u("Operation is not supported for read-only collection");
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0025 A[ORIG_RETURN, RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:19:? A[RETURN, SYNTHETIC] */
    @Override // java.util.Collection
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean contains(java.lang.Object r6) {
        /*
            r5 = this;
            java.lang.Object[] r4 = r5.A01
            r0 = 0
            X.C16700pc.A0E(r4, r0)
            r3 = 0
            int r2 = r4.length
            if (r6 != 0) goto L_0x0014
        L_0x000a:
            if (r3 >= r2) goto L_0x0025
            int r1 = r3 + 1
            r0 = r4[r3]
            if (r0 == 0) goto L_0x0022
            r3 = r1
            goto L_0x000a
        L_0x0014:
            if (r3 >= r2) goto L_0x0025
            int r1 = r3 + 1
            r0 = r4[r3]
            boolean r0 = r6.equals(r0)
            if (r0 != 0) goto L_0x0022
            r3 = r1
            goto L_0x0014
        L_0x0022:
            r0 = 1
            if (r3 >= 0) goto L_0x0026
        L_0x0025:
            r0 = 0
        L_0x0026:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C71213cZ.contains(java.lang.Object):boolean");
    }

    @Override // java.util.Collection
    public boolean containsAll(Collection collection) {
        C16700pc.A0E(collection, 0);
        if (collection.isEmpty()) {
            return true;
        }
        Iterator<T> it = collection.iterator();
        while (it.hasNext()) {
            if (!contains(it.next())) {
                return false;
            }
        }
        return true;
    }

    @Override // java.util.Collection
    public boolean isEmpty() {
        return C12960it.A1T(this.A01.length);
    }

    @Override // java.util.Collection, java.lang.Iterable
    public Iterator iterator() {
        Object[] objArr = this.A01;
        C16700pc.A0E(objArr, 0);
        return new AnonymousClass5DM(objArr);
    }

    @Override // java.util.Collection
    public boolean remove(Object obj) {
        throw C12980iv.A0u("Operation is not supported for read-only collection");
    }

    @Override // java.util.Collection
    public boolean removeAll(Collection collection) {
        throw C12980iv.A0u("Operation is not supported for read-only collection");
    }

    @Override // java.util.Collection
    public boolean retainAll(Collection collection) {
        throw C12980iv.A0u("Operation is not supported for read-only collection");
    }

    @Override // java.util.Collection
    public final /* bridge */ int size() {
        return this.A01.length;
    }

    @Override // java.util.Collection
    public final Object[] toArray() {
        Object[] objArr = this.A01;
        boolean z = this.A00;
        C16700pc.A0E(objArr, 0);
        if (z && C16700pc.A0O(objArr.getClass(), Object[].class)) {
            return objArr;
        }
        Object[] copyOf = Arrays.copyOf(objArr, objArr.length, Object[].class);
        C16700pc.A0B(copyOf);
        return copyOf;
    }
}
