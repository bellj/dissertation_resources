package X;

import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.util.Log;
import com.caverock.androidsvg.SVGImageView;
import java.io.IOException;
import java.io.InputStream;

/* renamed from: X.0AJ  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0AJ extends AsyncTask {
    public int A00;
    public Context A01;
    public final /* synthetic */ SVGImageView A02;

    public AnonymousClass0AJ(Context context, SVGImageView sVGImageView, int i) {
        this.A02 = sVGImageView;
        this.A01 = context;
        this.A00 = i;
    }

    @Override // android.os.AsyncTask
    public /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        try {
            Context context = this.A01;
            int i = this.A00;
            Resources resources = context.getResources();
            C06560Ud r0 = new C06560Ud();
            InputStream openRawResource = resources.openRawResource(i);
            AnonymousClass0Q5 A0N = r0.A0N(openRawResource);
            try {
                openRawResource.close();
                return A0N;
            } catch (IOException unused) {
                return A0N;
            }
        } catch (C11160fq e) {
            Log.e("SVGImageView", String.format("Error loading resource 0x%x: %s", Integer.valueOf(this.A00), e.getMessage()));
            return null;
        }
    }

    @Override // android.os.AsyncTask
    public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        SVGImageView sVGImageView = this.A02;
        sVGImageView.A01 = (AnonymousClass0Q5) obj;
        sVGImageView.A00();
    }
}
