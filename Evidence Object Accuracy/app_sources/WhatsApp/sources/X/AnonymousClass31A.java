package X;

import com.google.android.search.verification.client.SearchActionVerificationClientService;

/* renamed from: X.31A  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass31A extends AbstractC16110oT {
    public Boolean A00;
    public Integer A01;
    public Integer A02;
    public Long A03;
    public Long A04;
    public Long A05;
    public Long A06;
    public Long A07;
    public String A08;

    public AnonymousClass31A() {
        super(2496, new AnonymousClass00E(1000, SearchActionVerificationClientService.NOTIFICATION_ID, 20000), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(10, this.A01);
        r3.Abe(1, this.A03);
        r3.Abe(6, this.A00);
        r3.Abe(3, this.A04);
        r3.Abe(8, this.A05);
        r3.Abe(5, this.A06);
        r3.Abe(9, this.A02);
        r3.Abe(7, this.A07);
        r3.Abe(4, this.A08);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamReceiptStanzaReceive {");
        String str = null;
        Integer num = this.A01;
        if (num != null) {
            str = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "messageType", str);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "receiptStanzaDuration", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "receiptStanzaHasOrphaned", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "receiptStanzaOfflineCount", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "receiptStanzaProcessedCount", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "receiptStanzaRetryVer", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "receiptStanzaStage", C12960it.A0Y(this.A02));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "receiptStanzaTotalCount", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "receiptStanzaType", this.A08);
        return C12960it.A0d("}", A0k);
    }
}
