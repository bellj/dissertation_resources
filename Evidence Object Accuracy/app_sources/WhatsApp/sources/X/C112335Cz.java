package X;

import java.util.Enumeration;
import java.util.NoSuchElementException;

/* renamed from: X.5Cz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C112335Cz implements Enumeration {
    public int A00 = 0;
    public final /* synthetic */ AbstractC114775Na A01;

    public C112335Cz(AbstractC114775Na r2) {
        this.A01 = r2;
    }

    @Override // java.util.Enumeration
    public Object nextElement() {
        int i = this.A00;
        AnonymousClass1TN[] r1 = this.A01.A00;
        if (i < r1.length) {
            this.A00 = i + 1;
            return r1[i];
        }
        throw new NoSuchElementException();
    }

    @Override // java.util.Enumeration
    public boolean hasMoreElements() {
        return C12990iw.A1Y(this.A00, this.A01.A00.length);
    }
}
