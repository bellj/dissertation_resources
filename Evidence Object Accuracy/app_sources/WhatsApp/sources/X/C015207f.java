package X;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.util.Xml;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/* renamed from: X.07f  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C015207f {
    public static final ThreadLocal A00 = new ThreadLocal();

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v76, types: [java.lang.Object[], java.lang.Object] */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00a0, code lost:
        if (r9.hasValue(3) != false) goto L_0x00a2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00b3, code lost:
        if (r9.hasValue(2) != false) goto L_0x00b5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0104, code lost:
        if (r26 > 100.0f) goto L_0x0106;
     */
    /* JADX WARNING: Removed duplicated region for block: B:145:0x02ea  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x009b  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00ae  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00c6  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x00ff  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0114  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0128  */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x0156  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0159  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.content.res.ColorStateList A00(android.content.res.Resources.Theme r30, android.content.res.Resources r31, android.util.AttributeSet r32, org.xmlpull.v1.XmlPullParser r33) {
        /*
        // Method dump skipped, instructions count: 799
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C015207f.A00(android.content.res.Resources$Theme, android.content.res.Resources, android.util.AttributeSet, org.xmlpull.v1.XmlPullParser):android.content.res.ColorStateList");
    }

    public static ColorStateList A01(Resources.Theme theme, Resources resources, XmlPullParser xmlPullParser) {
        int next;
        AttributeSet asAttributeSet = Xml.asAttributeSet(xmlPullParser);
        do {
            next = xmlPullParser.next();
            if (next == 2) {
                return A00(theme, resources, asAttributeSet, xmlPullParser);
            }
        } while (next != 1);
        throw new XmlPullParserException("No start tag found");
    }
}
