package X;

import android.view.ViewTreeObserver;
import android.widget.Switch;
import com.whatsapp.registration.ChangeNumberNotifyContacts;

/* renamed from: X.4oP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnPreDrawListenerC101984oP implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ ChangeNumberNotifyContacts A00;
    public final /* synthetic */ boolean A01;

    public ViewTreeObserver$OnPreDrawListenerC101984oP(ChangeNumberNotifyContacts changeNumberNotifyContacts, boolean z) {
        this.A00 = changeNumberNotifyContacts;
        this.A01 = z;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        ChangeNumberNotifyContacts changeNumberNotifyContacts = this.A00;
        C12980iv.A1G(changeNumberNotifyContacts.A04, this);
        Switch r2 = changeNumberNotifyContacts.A09;
        boolean z = this.A01;
        r2.setChecked(!z);
        changeNumberNotifyContacts.A09.setChecked(z);
        return false;
    }
}
