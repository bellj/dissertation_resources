package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1nX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38111nX {
    public List A00;
    public final byte[] A01;

    public C38111nX() {
        this(null);
    }

    public C38111nX(byte[] bArr) {
        this.A01 = bArr;
    }

    public List A00() {
        byte[] bArr = this.A01;
        if (bArr == null) {
            return null;
        }
        List list = this.A00;
        if (list != null) {
            return list;
        }
        int length = bArr.length;
        this.A00 = new ArrayList(length);
        for (byte b : bArr) {
            this.A00.add(Float.valueOf(((float) b) / 100.0f));
        }
        return this.A00;
    }

    public /* bridge */ /* synthetic */ Object clone() {
        return new C38111nX(this.A01);
    }
}
