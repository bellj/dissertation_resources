package X;

/* renamed from: X.4TE  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4TE {
    public final float A00;
    public final float A01;
    public final float A02;
    public final float A03;
    public final float A04;
    public final float A05;

    public AnonymousClass4TE(float f, float f2, float f3, float f4, float f5, float f6) {
        this.A01 = f;
        this.A05 = f2;
        this.A02 = f3;
        this.A00 = f4;
        this.A03 = f5;
        this.A04 = f6;
    }
}
