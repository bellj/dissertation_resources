package X;

import android.content.Intent;
import com.whatsapp.payments.ui.BrazilPayBloksActivity;
import com.whatsapp.payments.ui.BrazilPaymentActivity;
import java.util.HashMap;

/* renamed from: X.5k8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C121835k8 extends AnonymousClass6CT {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AnonymousClass6CN A01;

    public C121835k8(AnonymousClass6CN r1, int i) {
        this.A01 = r1;
        this.A00 = i;
    }

    @Override // X.AbstractC1311861p
    public void ALx() {
        BrazilPaymentActivity brazilPaymentActivity = this.A01.A03;
        String A01 = brazilPaymentActivity.A0S.A01();
        Intent A0D = C12990iw.A0D(brazilPaymentActivity, BrazilPayBloksActivity.class);
        if (A01 == null) {
            A01 = "brpay_p_add_card";
        }
        A0D.putExtra("screen_name", A01);
        AbstractActivityC119645em.A0O(A0D, "referral_screen", "payment_method_picker");
        if (this.A00 == 1) {
            HashMap A11 = C12970iu.A11();
            A11.put("add_debit_only", "1");
            A0D.putExtra("screen_params", A11);
        }
        brazilPaymentActivity.startActivity(A0D);
    }
}
