package X;

import java.util.Locale;

/* renamed from: X.0Sr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06230Sr {
    public int A00;
    public int A01 = 0;
    public AnonymousClass0SB A02 = new AnonymousClass0SB();
    public String A03;

    public static boolean A01(int i) {
        return i == 32 || i == 10 || i == 13 || i == 9;
    }

    public C06230Sr(String str) {
        String trim = str.trim();
        this.A03 = trim;
        this.A00 = trim.length();
    }

    public float A02() {
        AnonymousClass0SB r3 = this.A02;
        float A00 = r3.A00(this.A03, this.A01, this.A00);
        if (!Float.isNaN(A00)) {
            this.A01 = r3.A00;
        }
        return A00;
    }

    public float A03() {
        A0E();
        AnonymousClass0SB r3 = this.A02;
        float A00 = r3.A00(this.A03, this.A01, this.A00);
        if (!Float.isNaN(A00)) {
            this.A01 = r3.A00;
        }
        return A00;
    }

    public float A04(float f) {
        if (Float.isNaN(f)) {
            return Float.NaN;
        }
        A0E();
        return A02();
    }

    public int A05() {
        int i = this.A01;
        int i2 = this.A00;
        if (i == i2) {
            return -1;
        }
        int i3 = i + 1;
        this.A01 = i3;
        if (i3 < i2) {
            return this.A03.charAt(i3);
        }
        return -1;
    }

    public C08910c3 A06() {
        float A02 = A02();
        if (Float.isNaN(A02)) {
            return null;
        }
        AnonymousClass0JP A07 = A07();
        if (A07 == null) {
            A07 = AnonymousClass0JP.px;
        }
        return new C08910c3(A07, A02);
    }

    public AnonymousClass0JP A07() {
        if (!A0D()) {
            String str = this.A03;
            char charAt = str.charAt(this.A01);
            int i = this.A01;
            if (charAt == '%') {
                this.A01 = i + 1;
                return AnonymousClass0JP.percent;
            } else if (i <= this.A00 - 2) {
                try {
                    AnonymousClass0JP valueOf = AnonymousClass0JP.valueOf(str.substring(i, i + 2).toLowerCase(Locale.US));
                    this.A01 += 2;
                    return valueOf;
                } catch (IllegalArgumentException unused) {
                    return null;
                }
            }
        }
        return null;
    }

    public Boolean A08(Object obj) {
        if (obj == null) {
            return null;
        }
        A0E();
        int i = this.A01;
        if (i == this.A00) {
            return null;
        }
        char charAt = this.A03.charAt(i);
        if (charAt != '0' && charAt != '1') {
            return null;
        }
        boolean z = true;
        this.A01++;
        if (charAt != '1') {
            z = false;
        }
        return Boolean.valueOf(z);
    }

    public Integer A09() {
        int i = this.A01;
        if (i == this.A00) {
            return null;
        }
        String str = this.A03;
        this.A01 = i + 1;
        return Integer.valueOf(str.charAt(i));
    }

    public String A0A() {
        int i;
        String str;
        char charAt;
        int A05;
        if (A0D() || ((charAt = (str = this.A03).charAt((i = this.A01))) != '\'' && charAt != '\"')) {
            return null;
        }
        do {
            A05 = A05();
            if (A05 == -1) {
                this.A01 = i;
                return null;
            }
        } while (A05 != charAt);
        int i2 = this.A01 + 1;
        this.A01 = i2;
        return str.substring(i + 1, i2 - 1);
    }

    public String A0B(char c, boolean z) {
        if (!A0D()) {
            String str = this.A03;
            char charAt = str.charAt(this.A01);
            if ((z || !A01(charAt)) && charAt != c) {
                int i = this.A01;
                while (true) {
                    int A05 = A05();
                    if (A05 == -1 || A05 == c || (!z && A01(A05))) {
                        break;
                    }
                }
                return str.substring(i, this.A01);
            }
        }
        return null;
    }

    public void A0C() {
        while (true) {
            int i = this.A01;
            if (i < this.A00 && A01(this.A03.charAt(i))) {
                this.A01++;
            } else {
                return;
            }
        }
    }

    public boolean A0D() {
        return this.A01 == this.A00;
    }

    public boolean A0E() {
        A0C();
        int i = this.A01;
        if (i == this.A00 || this.A03.charAt(i) != ',') {
            return false;
        }
        this.A01++;
        A0C();
        return true;
    }

    public boolean A0F(char c) {
        int i = this.A01;
        if (i >= this.A00 || this.A03.charAt(i) != c) {
            return false;
        }
        this.A01++;
        return true;
    }

    public boolean A0G(String str) {
        int length = str.length();
        int i = this.A01;
        if (i > this.A00 - length || !this.A03.substring(i, i + length).equals(str)) {
            return false;
        }
        this.A01 += length;
        return true;
    }
}
