package X;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.CircularProgressBar;
import com.whatsapp.Conversation;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.conversation.conversationrow.ConversationRowVideo$RowVideoView;
import java.io.File;
import java.util.Collections;

/* renamed from: X.2yK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C60622yK extends AbstractC42671vd {
    public boolean A00;
    public final View A01;
    public final View A02;
    public final ImageView A03;
    public final TextView A04 = C12960it.A0J(this, R.id.control_btn);
    public final TextView A05;
    public final CircularProgressBar A06;
    public final TextEmojiLabel A07;
    public final ConversationRowVideo$RowVideoView A08 = ((ConversationRowVideo$RowVideoView) findViewById(R.id.thumb));
    public final AbstractC41521tf A09 = new C70213av(this);

    public C60622yK(Context context, AbstractC13890kV r4, AnonymousClass1X2 r5) {
        super(context, r4, r5);
        A0Z();
        CircularProgressBar circularProgressBar = (CircularProgressBar) findViewById(R.id.progress_bar);
        this.A06 = circularProgressBar;
        this.A05 = C12960it.A0J(this, R.id.info);
        this.A03 = C12970iu.A0L(this, R.id.button_image);
        this.A01 = findViewById(R.id.control_frame);
        TextEmojiLabel A0U = C12970iu.A0U(this, R.id.caption);
        this.A07 = A0U;
        AbstractC28491Nn.A03(A0U);
        this.A02 = findViewById(R.id.text_and_date);
        circularProgressBar.setMax(100);
        circularProgressBar.A0B = 0;
        A0X(true);
    }

    private void A0X(boolean z) {
        String A03;
        AbstractC16130oV r2 = (AbstractC16130oV) ((AbstractC28551Oa) this).A0O;
        C16150oX A00 = AbstractC15340mz.A00(r2);
        if (z) {
            this.A04.setTag(Collections.singletonList(r2));
        }
        TextView textView = this.A05;
        textView.setVisibility(8);
        ConversationRowVideo$RowVideoView conversationRowVideo$RowVideoView = this.A08;
        conversationRowVideo$RowVideoView.setKeepRatio(((AbstractC28551Oa) this).A0R);
        conversationRowVideo$RowVideoView.setFullWidth(((AbstractC28551Oa) this).A0R);
        AnonymousClass1OY.A0H(conversationRowVideo$RowVideoView, this, r2, r2.A0z);
        if (((AbstractC28551Oa) this).A0R) {
            int A01 = C27531Hw.A01(getContext());
            int A002 = AnonymousClass19O.A00(r2, A01);
            if (A002 <= 0) {
                A002 = (A01 * 9) >> 4;
            }
            conversationRowVideo$RowVideoView.A02(A01, A002, true);
        }
        AbstractC16130oV fMessage = getFMessage();
        if (C30041Vv.A12(fMessage)) {
            View view = this.A01;
            CircularProgressBar circularProgressBar = this.A06;
            ImageView imageView = this.A03;
            TextView textView2 = this.A04;
            AbstractC42671vd.A0a(view, circularProgressBar, textView2, imageView, true, !z, false);
            conversationRowVideo$RowVideoView.setVisibility(0);
            C12960it.A0r(getContext(), conversationRowVideo$RowVideoView, R.string.video_transfer_in_progress);
            conversationRowVideo$RowVideoView.setOnClickListener(null);
            AbstractView$OnClickListenerC34281fs r5 = ((AbstractC42671vd) this).A07;
            textView2.setOnClickListener(r5);
            circularProgressBar.setOnClickListener(r5);
        } else if (C30041Vv.A13(fMessage)) {
            conversationRowVideo$RowVideoView.setVisibility(0);
            View view2 = this.A01;
            CircularProgressBar circularProgressBar2 = this.A06;
            ImageView imageView2 = this.A03;
            TextView textView3 = this.A04;
            AbstractC42671vd.A0a(view2, circularProgressBar2, textView3, imageView2, false, false, false);
            textView3.setVisibility(8);
            imageView2.setVisibility(0);
            imageView2.setImageResource(R.drawable.ic_video_play_conv);
            C12960it.A0r(getContext(), imageView2, R.string.play_video);
            conversationRowVideo$RowVideoView.setContentDescription(C12960it.A0X(getContext(), C38131nZ.A02(((AbstractC28551Oa) this).A0K, r2.A00, 0), new Object[1], 0, R.string.video_duration_seconds));
            AbstractView$OnClickListenerC34281fs r52 = ((AbstractC42671vd) this).A0A;
            imageView2.setOnClickListener(r52);
            textView3.setOnClickListener(r52);
            conversationRowVideo$RowVideoView.setOnClickListener(r52);
        } else {
            TextView textView4 = this.A04;
            conversationRowVideo$RowVideoView.setOnClickListener(AnonymousClass1OY.A0A(textView4, this, r2));
            C12960it.A0r(getContext(), conversationRowVideo$RowVideoView, R.string.button_download);
            textView4.setVisibility(0);
            ImageView imageView3 = this.A03;
            imageView3.setVisibility(8);
            AbstractC42671vd.A0a(this.A01, this.A06, textView4, imageView3, false, !z, false);
        }
        A0w();
        conversationRowVideo$RowVideoView.setOnLongClickListener(this.A1a);
        Context context = getContext();
        AnonymousClass009.A05(context);
        conversationRowVideo$RowVideoView.A05 = C92994Ym.A00(context);
        this.A1O.A07(conversationRowVideo$RowVideoView, r2, this.A09);
        int i = r2.A00;
        if (i == 0) {
            i = C22200yh.A07(A00.A0F);
            r2.A00 = i;
        }
        AnonymousClass018 r4 = ((AbstractC28551Oa) this).A0K;
        if (i != 0) {
            A03 = C38131nZ.A04(r4, (long) i);
        } else {
            A03 = C44891zj.A03(r4, r2.A01);
        }
        textView.setText(A03);
        textView.setVisibility(0);
        if (C28141Kv.A01(((AbstractC28551Oa) this).A0K)) {
            textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.mark_video, 0, 0, 0);
        } else {
            textView.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, AnonymousClass2GF.A00(getContext(), ((AbstractC28551Oa) this).A0K, R.drawable.mark_video), (Drawable) null);
        }
        A1M(this.A02, this.A07, getFMessage().A15());
    }

    @Override // X.AbstractC42681ve, X.AnonymousClass1OZ, X.AbstractC28561Ob
    public void A0Z() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2P6 A07 = AnonymousClass1OY.A07(this);
            AnonymousClass01J A08 = AnonymousClass1OY.A08(A07, this);
            AnonymousClass1OY.A0L(A08, this);
            AnonymousClass1OY.A0M(A08, this);
            AnonymousClass1OY.A0K(A08, this);
            AnonymousClass1OY.A0I(A07, A08, this, AnonymousClass1OY.A09(A08, this, AnonymousClass1OY.A0B(A08, this)));
            AnonymousClass1OY.A0O(A08, this);
        }
    }

    @Override // X.AnonymousClass1OY
    public int A0n(int i) {
        if (!AnonymousClass1OY.A0T(this)) {
            return super.A0n(i);
        }
        return 0;
    }

    @Override // X.AnonymousClass1OY
    public void A0s() {
        A0X(false);
        A1H(false);
    }

    @Override // X.AbstractC42671vd, X.AnonymousClass1OY
    public void A0y() {
        if (((AbstractC42671vd) this).A01 == null || AnonymousClass1OY.A0U(this)) {
            AbstractC16130oV r6 = (AbstractC16130oV) ((AbstractC28551Oa) this).A0O;
            C16150oX A00 = AbstractC15340mz.A00(r6);
            if (A00.A0P) {
                int i = 1;
                if (A00.A07 == 1) {
                    ((AnonymousClass1OY) this).A0J.A05(R.string.gallery_unsafe_video_removed, 1);
                    return;
                }
                boolean z = false;
                File file = A00.A0F;
                if (file != null) {
                    z = C12980iv.A0h(file).exists();
                }
                StringBuilder A0k = C12960it.A0k("viewmessage/ from_me:");
                AnonymousClass1IS r3 = r6.A0z;
                AnonymousClass1OY.A0P(A00, r6, A0k, r3.A02);
                if (!z) {
                    AnonymousClass1OY.A0S(this, r3);
                    return;
                }
                boolean A08 = ((AbstractC28551Oa) this).A0b.A08();
                if (A08) {
                    i = 3;
                }
                AnonymousClass2TS r2 = new AnonymousClass2TS(getContext());
                r2.A07 = A08;
                AbstractC14640lm r0 = r3.A00;
                AnonymousClass009.A05(r0);
                r2.A03 = r0;
                r2.A04 = r3;
                r2.A02 = i;
                r2.A06 = C12960it.A1W(AbstractC35731ia.A01(getContext(), Conversation.class));
                Intent A002 = r2.A00();
                ConversationRowVideo$RowVideoView conversationRowVideo$RowVideoView = this.A08;
                if (conversationRowVideo$RowVideoView != null) {
                    AbstractC454421p.A07(getContext(), A002, conversationRowVideo$RowVideoView);
                }
                AnonymousClass1OY.A0E(A002, this, conversationRowVideo$RowVideoView, r3);
            }
        }
    }

    @Override // X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r2, boolean z) {
        boolean A1X = C12960it.A1X(r2, ((AbstractC28551Oa) this).A0O);
        super.A1D(r2, z);
        if (z || A1X) {
            A0X(A1X);
        }
    }

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_legacy_video_left;
    }

    @Override // X.AbstractC42671vd, X.AbstractC28551Oa
    public AnonymousClass1X2 getFMessage() {
        return (AnonymousClass1X2) ((AbstractC16130oV) ((AbstractC28551Oa) this).A0O);
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_legacy_video_left;
    }

    @Override // X.AnonymousClass1OY
    public Drawable getKeepDrawable() {
        if (AnonymousClass1OY.A0T(this)) {
            return AnonymousClass2GE.A01(getContext(), R.drawable.keep, R.color.white);
        }
        return super.getKeepDrawable();
    }

    @Override // X.AbstractC28551Oa
    public int getMainChildMaxWidth() {
        return AnonymousClass3GD.A02(this);
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        throw C12960it.A0U("this row type does not support outgoing messages");
    }

    @Override // X.AnonymousClass1OY
    public Drawable getStarDrawable() {
        if (AnonymousClass1OY.A0T(this)) {
            return AnonymousClass00T.A04(getContext(), R.drawable.message_star_media);
        }
        return super.getStarDrawable();
    }

    @Override // X.AbstractC28551Oa, android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    @Override // X.AbstractC42671vd, X.AbstractC28551Oa
    public void setFMessage(AbstractC15340mz r2) {
        AnonymousClass009.A0F(r2 instanceof AnonymousClass1X2);
        super.setFMessage(r2);
    }
}
