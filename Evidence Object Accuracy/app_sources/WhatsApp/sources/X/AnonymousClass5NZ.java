package X;

/* renamed from: X.5NZ  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5NZ extends AbstractC114775Na {
    public int A00 = -1;

    public AnonymousClass5NZ() {
    }

    public AnonymousClass5NZ(C94954co r2) {
        super(r2);
    }

    public AnonymousClass5NZ(AnonymousClass1TN[] r2) {
        super(r2);
    }

    public AnonymousClass5NZ(AnonymousClass1TN[] r2, boolean z) {
        super(r2, false);
    }

    @Override // X.AbstractC114775Na, X.AnonymousClass1TL
    public AnonymousClass1TL A06() {
        return this;
    }

    @Override // X.AnonymousClass1TL
    public int A05() {
        int i = this.A00;
        if (i < 0) {
            int length = super.A00.length;
            i = 0;
            for (int i2 = 0; i2 < length; i2++) {
                i = C72453ed.A0R(super.A00, i2, i);
            }
            this.A00 = i;
        }
        return AnonymousClass1TQ.A00(i) + 1 + i;
    }
}
