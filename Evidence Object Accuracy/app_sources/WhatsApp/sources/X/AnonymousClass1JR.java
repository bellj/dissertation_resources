package X;

import java.util.Arrays;

/* renamed from: X.1JR  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1JR {
    public final byte[] A00;

    public AnonymousClass1JR(int i, int i2) {
        if (i == 0 && i2 == 0) {
            this.A00 = new byte[0];
            return;
        }
        this.A00 = r3;
        byte[] bArr = {(byte) (i >> 8), (byte) i, (byte) (i2 >> 24), (byte) (i2 >> 16), (byte) (i2 >> 8), (byte) i2};
    }

    public AnonymousClass1JR(byte[] bArr) {
        this.A00 = bArr;
    }

    public int A00() {
        byte[] bArr = this.A00;
        if (bArr.length == 0) {
            return 0;
        }
        return (bArr[1] & 255) | ((bArr[0] & 255) << 8);
    }

    public int A01() {
        byte[] bArr = this.A00;
        if (bArr.length == 0) {
            return 0;
        }
        return (bArr[5] & 255) | ((bArr[2] & 255) << 24) | ((bArr[3] & 255) << 16) | ((bArr[4] & 255) << 8);
    }

    public C34851go A02() {
        AnonymousClass1G4 A0T = C34851go.A02.A0T();
        byte[] bArr = this.A00;
        AbstractC27881Jp A01 = AbstractC27881Jp.A01(bArr, 0, bArr.length);
        A0T.A03();
        C34851go r1 = (C34851go) A0T.A00;
        r1.A00 |= 1;
        r1.A01 = A01;
        return (C34851go) A0T.A02();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AnonymousClass1JR)) {
            return false;
        }
        return Arrays.equals(this.A00, ((AnonymousClass1JR) obj).A00);
    }

    public int hashCode() {
        return Arrays.hashCode(this.A00);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("SyncdKeyId{deviceId=");
        sb.append(A00());
        sb.append(", epoch=");
        sb.append(A01());
        sb.append("}");
        return sb.toString();
    }
}
