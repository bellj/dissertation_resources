package X;

import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import java.text.Collator;
import java.util.Comparator;
import java.util.Map;

/* renamed from: X.3cl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C71333cl implements Comparator {
    public final C15610nY A00;
    public final Collator A01;
    public final Map A02 = C12970iu.A11();

    public C71333cl(C15610nY r3, AnonymousClass018 r4) {
        this.A00 = r3;
        Collator instance = Collator.getInstance(C12970iu.A14(r4));
        this.A01 = instance;
        instance.setDecomposition(1);
    }

    /* renamed from: A00 */
    public int compare(C15370n3 r7, C15370n3 r8) {
        String A01 = A01(r7);
        String A012 = A01(r8);
        if (A01 == null && A012 == null) {
            return 0;
        }
        if (A01 != null) {
            if (A012 != null) {
                int compare = this.A01.compare(A01, A012);
                if (compare != 0) {
                    return compare;
                }
                Jid jid = r7.A0D;
                if (jid == null && r8.A0D == null) {
                    return 0;
                }
                if (jid != null) {
                    Jid jid2 = r8.A0D;
                    if (jid2 != null) {
                        return jid.getRawString().compareTo(jid2.getRawString());
                    }
                }
            }
            return -1;
        }
        return 1;
    }

    public String A01(C15370n3 r5) {
        if (r5 == null) {
            return null;
        }
        String str = r5.A0Q;
        if (str != null && str.length() > 0) {
            return str;
        }
        if (r5.A0D == null) {
            return null;
        }
        Map map = this.A02;
        String A0t = C12970iu.A0t(r5.A0B(UserJid.class), map);
        if (A0t != null) {
            return A0t;
        }
        String A04 = this.A00.A04(r5);
        map.put(r5.A0B(UserJid.class), A04);
        return A04;
    }
}
