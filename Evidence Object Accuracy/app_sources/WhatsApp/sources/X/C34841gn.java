package X;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.1gn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C34841gn extends AbstractC30271Wt {
    public long A00;
    public Set A01;

    public C34841gn(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 50, j);
        this.A01 = new HashSet();
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C34841gn(C57112mV r5, AnonymousClass1IS r6, long j) {
        super(r6, (byte) 50, j);
        Set unmodifiableSet = Collections.unmodifiableSet(new HashSet(r5.A02));
        long j2 = r5.A01;
        this.A01 = unmodifiableSet;
        this.A00 = j2;
    }

    @Override // X.AbstractC16420oz
    public void A6k(C39971qq r6) {
        AnonymousClass1G4 A0T = C57112mV.A03.A0T();
        Set set = this.A01;
        A0T.A03();
        C57112mV r2 = (C57112mV) A0T.A00;
        AnonymousClass1K6 r1 = r2.A02;
        if (!((AnonymousClass1K7) r1).A00) {
            r1 = AbstractC27091Fz.A0G(r1);
            r2.A02 = r1;
        }
        AnonymousClass1G5.A01(set, r1);
        long j = this.A00;
        A0T.A03();
        C57112mV r12 = (C57112mV) A0T.A00;
        r12.A00 |= 1;
        r12.A01 = j;
        C56882m6 r22 = (C56882m6) C57692nT.A0D.A0T();
        r22.A05(EnumC87324Bb.A01);
        r22.A03();
        C57692nT r13 = (C57692nT) r22.A00;
        r13.A05 = (C57112mV) A0T.A02();
        r13.A00 |= 256;
        r6.A03.A09((C57692nT) r22.A02());
    }
}
