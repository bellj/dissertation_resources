package X;

import com.google.protobuf.UnsafeUtil;

/* renamed from: X.4Z8  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4Z8 {
    public static final AnonymousClass4US A00 = ((!UnsafeUtil.A02 || !UnsafeUtil.A03) ? new C82523vl() : new C82533vm());

    public static int A00(CharSequence charSequence) {
        int length = charSequence.length();
        int i = 0;
        while (i < length && charSequence.charAt(i) < 128) {
            i++;
        }
        int i2 = length;
        while (true) {
            if (i >= length) {
                break;
            }
            char charAt = charSequence.charAt(i);
            if (charAt < 2048) {
                i2 += (127 - charAt) >>> 31;
                i++;
            } else {
                int length2 = charSequence.length();
                int i3 = 0;
                while (i < length2) {
                    char charAt2 = charSequence.charAt(i);
                    if (charAt2 < 2048) {
                        i3 += (127 - charAt2) >>> 31;
                    } else {
                        i3 += 2;
                        if (55296 <= charAt2 && charAt2 <= 57343) {
                            if (Character.codePointAt(charSequence, i) >= 65536) {
                                i++;
                            } else {
                                throw new AnonymousClass4CO(i, length2);
                            }
                        }
                    }
                    i++;
                }
                i2 += i3;
            }
        }
        if (i2 >= length) {
            return i2;
        }
        throw C12970iu.A0f(C12970iu.A0w(C12960it.A0k("UTF-8 length does not fit in int: "), ((long) i2) + 4294967296L));
    }
}
