package X;

/* renamed from: X.65w  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass65w implements AbstractC009404s {
    public final C14900mE A00;
    public final C14830m7 A01;
    public final C14850m9 A02;
    public final C129965ya A03;
    public final C129215xM A04;
    public final AnonymousClass60T A05;
    public final AnonymousClass60U A06;
    public final AnonymousClass61E A07;
    public final AnonymousClass605 A08;
    public final C130015yf A09;
    public final AbstractC14440lR A0A;
    public final String A0B;
    public final String A0C;

    public AnonymousClass65w(C14900mE r1, C14830m7 r2, C14850m9 r3, C129965ya r4, C129215xM r5, AnonymousClass60T r6, AnonymousClass60U r7, AnonymousClass61E r8, AnonymousClass605 r9, C130015yf r10, AbstractC14440lR r11, String str, String str2) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
        this.A0A = r11;
        this.A0C = str;
        this.A06 = r7;
        this.A08 = r9;
        this.A0B = str2;
        this.A03 = r4;
        this.A09 = r10;
        this.A04 = r5;
        this.A07 = r8;
        this.A05 = r6;
    }

    @Override // X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        C14830m7 r2 = this.A01;
        C14850m9 r3 = this.A02;
        C14900mE r1 = this.A00;
        AbstractC14440lR r11 = this.A0A;
        String str = this.A0C;
        AnonymousClass60U r7 = this.A06;
        AnonymousClass605 r9 = this.A08;
        C129965ya r4 = this.A03;
        C130015yf r10 = this.A09;
        return new C123595nP(r1, r2, r3, r4, this.A04, this.A05, r7, this.A07, r9, r10, r11, str, this.A0B);
    }
}
