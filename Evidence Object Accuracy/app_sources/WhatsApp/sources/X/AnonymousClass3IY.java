package X;

import android.content.Context;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.util.Base64;
import android.util.DisplayMetrics;
import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.R;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import javax.crypto.Mac;
import javax.crypto.SecretKey;

/* renamed from: X.3IY  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3IY {
    public static final byte[] A03 = Base64.decode("PkTwKSZqUfAUyR0rPQ8hYJ0wNsQQ3dW1+3SCnyTXIfEAxxS75FwkDf47wNv/c8pP3p0GXKR6OOQmhyERwx74fw1RYSU10I4r1gyBVDbRJ40pidjM41G1I1oN", 0);
    public int A00;
    public boolean A01;
    public final byte[] A02;

    public AnonymousClass3IY(byte[] bArr) {
        this.A02 = bArr;
    }

    public static String A00(Context context, String str) {
        int i;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        String packageName = context.getPackageName();
        if (packageName == null || !"com.whatsapp".equals(packageName)) {
            throw new AssertionError();
        }
        try {
            String str2 = AnonymousClass01V.A08;
            byteArrayOutputStream.write(packageName.getBytes(str2));
            InputStream resourceAsStream = AnonymousClass3IY.class.getResourceAsStream("/res/drawable-hdpi/about_logo.png");
            if (resourceAsStream == null) {
                resourceAsStream = AnonymousClass3IY.class.getResourceAsStream("/res/drawable-hdpi-v4/about_logo.png");
            }
            if (resourceAsStream == null) {
                resourceAsStream = AnonymousClass3IY.class.getResourceAsStream("/res/drawable-xxhdpi-v4/about_logo.png");
            }
            if (resourceAsStream == null) {
                Resources resources = context.getResources();
                DisplayMetrics displayMetrics = new DisplayMetrics();
                displayMetrics.setToDefaults();
                displayMetrics.density = 1.5f;
                displayMetrics.densityDpi = 240;
                displayMetrics.scaledDensity = 1.5f;
                float f = (float) 240;
                displayMetrics.xdpi = f;
                displayMetrics.ydpi = f;
                resourceAsStream = new Resources(resources.getAssets(), displayMetrics, resources.getConfiguration()).openRawResource(R.drawable.about_logo);
            }
            if (resourceAsStream != null) {
                byte[] bArr = new byte[DefaultCrypto.BUFFER_SIZE];
                try {
                    try {
                        int read = resourceAsStream.read(bArr);
                        while (true) {
                            if (read != -1) {
                                byteArrayOutputStream.write(bArr, 0, read);
                                read = resourceAsStream.read(bArr);
                            } else {
                                try {
                                    break;
                                } catch (IOException unused) {
                                }
                            }
                        }
                        SecretKey A06 = C003501n.A06(byteArrayOutputStream.toByteArray(), A03, 128, 512);
                        A06.getEncoded();
                        try {
                            Mac instance = Mac.getInstance("HMACSHA1");
                            try {
                                instance.init(A06);
                                Signature[] A02 = AnonymousClass01U.A02(context);
                                if (A02 == null || (r1 = A02.length) == 0) {
                                    throw new AssertionError();
                                }
                                for (Signature signature : A02) {
                                    instance.update(signature.toByteArray());
                                }
                                instance.update(C003501n.A0E(context));
                                try {
                                    instance.update(str.getBytes(str2));
                                    return new AnonymousClass3IY(instance.doFinal()).toString();
                                } catch (UnsupportedEncodingException e) {
                                    throw new AssertionError(e);
                                }
                            } catch (InvalidKeyException e2) {
                                throw new AssertionError(e2);
                            }
                        } catch (NoSuchAlgorithmException e3) {
                            throw new AssertionError(e3);
                        }
                    } catch (IOException unused2) {
                        throw new AssertionError();
                    }
                } finally {
                    try {
                        resourceAsStream.close();
                    } catch (IOException unused3) {
                    }
                }
            } else {
                throw new AssertionError();
            }
        } catch (IOException e4) {
            throw new Error(e4);
        }
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            return obj != null && AnonymousClass3IY.class.equals(obj.getClass()) && Arrays.equals(this.A02, ((AnonymousClass3IY) obj).A02);
        }
        return true;
    }

    public int hashCode() {
        if (this.A01) {
            return this.A00;
        }
        int hashCode = Arrays.hashCode(this.A02);
        this.A00 = hashCode;
        this.A01 = true;
        return hashCode;
    }

    public String toString() {
        return Base64.encodeToString(this.A02, 2);
    }
}
