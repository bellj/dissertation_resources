package X;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;

/* renamed from: X.0FA  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0FA extends AnonymousClass0B6 {
    public AnonymousClass0QU A00;
    public boolean A01;

    public AnonymousClass0FA(int i, int i2) {
        super(i, i2);
    }

    public AnonymousClass0FA(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public AnonymousClass0FA(ViewGroup.LayoutParams layoutParams) {
        super(layoutParams);
    }

    public AnonymousClass0FA(ViewGroup.MarginLayoutParams marginLayoutParams) {
        super(marginLayoutParams);
    }
}
