package X;

import android.content.res.ColorStateList;
import android.graphics.Shader;

/* renamed from: X.06x  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C014606x {
    public int A00;
    public final ColorStateList A01;
    public final Shader A02;

    public C014606x(ColorStateList colorStateList, Shader shader, int i) {
        this.A02 = shader;
        this.A01 = colorStateList;
        this.A00 = i;
    }
}
