package X;

import com.whatsapp.payments.ui.NoviSelfieCameraView;
import com.whatsapp.payments.ui.stepup.NoviCaptureVideoSelfieActivity;
import com.whatsapp.payments.ui.widget.NoviSelfieFaceAnimationView;
import java.io.File;
import java.util.List;

/* renamed from: X.6Ib  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final /* synthetic */ class RunnableC135336Ib implements Runnable {
    public final /* synthetic */ NoviCaptureVideoSelfieActivity A00;
    public final /* synthetic */ List A01;

    public /* synthetic */ RunnableC135336Ib(NoviCaptureVideoSelfieActivity noviCaptureVideoSelfieActivity, List list) {
        this.A00 = noviCaptureVideoSelfieActivity;
        this.A01 = list;
    }

    @Override // java.lang.Runnable
    public final void run() {
        NoviCaptureVideoSelfieActivity noviCaptureVideoSelfieActivity = this.A00;
        List list = this.A01;
        int i = noviCaptureVideoSelfieActivity.A00;
        if (i <= 0) {
            noviCaptureVideoSelfieActivity.A08.setVisibility(8);
            NoviSelfieCameraView noviSelfieCameraView = noviCaptureVideoSelfieActivity.A0C;
            RunnableC135346Ic r2 = new Runnable(list) { // from class: X.6Ic
                public final /* synthetic */ List A01;

                {
                    this.A01 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    NoviCaptureVideoSelfieActivity noviCaptureVideoSelfieActivity2 = NoviCaptureVideoSelfieActivity.this;
                    List list2 = this.A01;
                    NoviSelfieFaceAnimationView noviSelfieFaceAnimationView = noviCaptureVideoSelfieActivity2.A0F;
                    boolean z = noviCaptureVideoSelfieActivity2.A0G;
                    noviSelfieFaceAnimationView.A00();
                    noviSelfieFaceAnimationView.A0F = z;
                    noviSelfieFaceAnimationView.A09.setVisibility(0);
                    noviSelfieFaceAnimationView.A02 = C12970iu.A0E();
                    noviSelfieFaceAnimationView.A04(list2);
                }
            };
            AnonymousClass6H3 r1 = new Runnable() { // from class: X.6H3
                @Override // java.lang.Runnable
                public final void run() {
                    NoviCaptureVideoSelfieActivity noviCaptureVideoSelfieActivity2 = NoviCaptureVideoSelfieActivity.this;
                    noviCaptureVideoSelfieActivity2.A0D.A04(noviCaptureVideoSelfieActivity2, new C126015sC(2));
                }
            };
            noviSelfieCameraView.A08 = r2;
            noviSelfieCameraView.A06 = r1;
            noviSelfieCameraView.A00 = list.size() * 4000;
            File A00 = noviSelfieCameraView.A02.A00("selfie.jpeg");
            AnonymousClass009.A05(A00);
            noviSelfieCameraView.A04 = A00;
            File A002 = noviSelfieCameraView.A02.A00("selfie.mp4");
            AnonymousClass009.A05(A002);
            noviSelfieCameraView.A05 = A002;
            noviSelfieCameraView.Aei(new AnonymousClass67U(noviSelfieCameraView), true);
            return;
        }
        if (i == 3) {
            noviCaptureVideoSelfieActivity.A04.setVisibility(8);
        }
        int i2 = noviCaptureVideoSelfieActivity.A00;
        noviCaptureVideoSelfieActivity.A08.setText(AbstractC27291Gt.A07(C12970iu.A14(((ActivityC13830kP) noviCaptureVideoSelfieActivity).A01), Integer.toString(i2)));
        noviCaptureVideoSelfieActivity.A00--;
        noviCaptureVideoSelfieActivity.A01.postDelayed(new RunnableC135336Ib(noviCaptureVideoSelfieActivity, list), 1000);
    }
}
