package X;

import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.ArrayList;

/* renamed from: X.5gL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120535gL extends C126705tJ {
    public final C14900mE A00;
    public final C16590pI A01;
    public final C18650sn A02;
    public final AnonymousClass6BE A03;
    public final C30931Zj A04 = C117305Zk.A0V("IndiaUpiRegisterAliasAction", "network");
    public final C18590sh A05;

    public C120535gL(C14900mE r3, C16590pI r4, C1308460e r5, C18650sn r6, C18610sj r7, AnonymousClass6BE r8, C18590sh r9) {
        super(r5.A04, r7);
        this.A01 = r4;
        this.A00 = r3;
        this.A05 = r9;
        this.A02 = r6;
        this.A03 = r8;
    }

    public static /* synthetic */ void A00(C452120p r2, C120535gL r3, String str) {
        AnonymousClass6BE r1;
        int i;
        if (str.equalsIgnoreCase("add")) {
            r1 = r3.A03;
            i = 22;
        } else if (str.equalsIgnoreCase("port")) {
            r1 = r3.A03;
            i = 24;
        } else {
            return;
        }
        r1.AKa(r2, i);
    }

    public void A01(AnonymousClass1ZR r14, AnonymousClass1ZR r15, AnonymousClass3CQ r16, String str, String str2) {
        Log.i("PAY: registerAlias called");
        C64513Fv r9 = super.A00;
        r9.A04("register-alias");
        ArrayList A0l = C12960it.A0l();
        C117295Zj.A1M("alias_value", (String) AnonymousClass1ZS.A01(r15), A0l);
        C117295Zj.A1M("alias_type", "mobile_number", A0l);
        if (!TextUtils.isEmpty(str)) {
            C117295Zj.A1M("vpa_id", str, A0l);
        }
        if (!AnonymousClass1ZS.A02(r14)) {
            C117295Zj.A1M("vpa", (String) r14.A00, A0l);
        }
        ArrayList A0l2 = C12960it.A0l();
        C117295Zj.A1M("action", "register-alias", A0l2);
        C117295Zj.A1M("device_id", this.A05.A01(), A0l2);
        C117295Zj.A1M("op", str2, A0l2);
        C117305Zk.A1H(super.A01, new C120835gp(this.A01.A00, this.A00, this.A02, r9, this, r16, str2), new AnonymousClass1V8(new AnonymousClass1V8("alias", C117305Zk.A1b(A0l)), "account", C117305Zk.A1b(A0l2)));
    }
}
