package X;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;

/* renamed from: X.2k9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C55992k9 extends AbstractC65073Ia implements AnonymousClass5SC {
    public static AnonymousClass5WW A0A = new C106214vL();
    public static AnonymousClass5WW A0B = new C106224vM();
    public static AnonymousClass5WW A0C = new C106234vN();
    public static AnonymousClass5WW A0D = new AnonymousClass3ST();
    public int A00 = -1;
    public int A01 = 0;
    public int A02;
    public long A03;
    public Drawable A04;
    public Drawable A05;
    public View.OnClickListener A06;
    public C14260l7 A07;
    public AnonymousClass28D A08;
    public boolean A09 = true;

    public C55992k9(long j, boolean z) {
        super(EnumC869849t.VIEW);
        this.A03 = j;
        C93304Zx[] r2 = {new C93304Zx(A0A, this), new C93304Zx(A0B, this), new C93304Zx(A0D, this), new C93304Zx(A0C, this)};
        int i = 0;
        do {
            A04(r2[i]);
            i++;
        } while (i < 4);
        this.A02 = 0;
        C93304Zx[] r22 = {new C93304Zx(new C67583Sb(), this), new C93304Zx(new C106294vT(), this)};
        int i2 = 0;
        do {
            A04(r22[i2]);
            i2++;
        } while (i2 < 2);
        if (!z) {
            A04(new C93304Zx(new C106334vX(this), this));
        }
    }

    @Override // X.AnonymousClass5SC
    public /* bridge */ /* synthetic */ Object A8B(Context context) {
        return new C55972k3(context);
    }
}
