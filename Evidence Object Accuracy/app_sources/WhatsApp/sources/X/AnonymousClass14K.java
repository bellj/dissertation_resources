package X;

import android.util.Base64;
import com.whatsapp.net.tls13.WtCachedPsk;
import com.whatsapp.util.Log;
import com.whatsapp.watls13.WtPersistentSession;
import java.io.File;
import java.security.cert.Certificate;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Random;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSessionContext;

/* renamed from: X.14K  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass14K implements SSLSessionContext {
    public AnonymousClass14J A00;
    public final Map A01 = new AnonymousClass1NN(this);
    public volatile int A02 = 64;
    public volatile long A03 = 172800;

    public AnonymousClass14K(AnonymousClass14J r3) {
        this.A00 = r3;
    }

    @Override // javax.net.ssl.SSLSessionContext
    public Enumeration getIds() {
        SSLSession[] sSLSessionArr;
        Map map = this.A01;
        synchronized (map) {
            sSLSessionArr = (SSLSession[]) map.values().toArray(new SSLSession[0]);
        }
        return new AnonymousClass1NO(this, Arrays.asList(sSLSessionArr).iterator());
    }

    @Override // javax.net.ssl.SSLSessionContext
    public synchronized SSLSession getSession(byte[] bArr) {
        AnonymousClass1NQ r5;
        Map map;
        WtCachedPsk[] wtCachedPskArr;
        WtPersistentSession wtPersistentSession;
        AnonymousClass1NP r7 = new AnonymousClass1NP(this, bArr);
        try {
            map = this.A01;
        } catch (AnonymousClass1NR e) {
            StringBuilder sb = new StringBuilder();
            sb.append("Encountered Exception ");
            sb.append(e.toString());
            Log.e(sb.toString());
        }
        synchronized (map) {
            AnonymousClass1NQ r6 = (AnonymousClass1NQ) map.get(r7);
            if (r6 == null) {
                AnonymousClass14J r52 = this.A00;
                if (r52 != null) {
                    synchronized (r52) {
                        if (r52.A01() == null) {
                            wtPersistentSession = null;
                        } else {
                            wtPersistentSession = r52.A00(new File(r52.A01(), Base64.encodeToString(bArr, 10)));
                        }
                    }
                    if (wtPersistentSession != null) {
                        r6 = new AnonymousClass1NQ(this, wtPersistentSession.A02, wtPersistentSession.A01, wtPersistentSession.A00);
                        r6.A08 = wtPersistentSession.A04;
                        r6.A07 = wtPersistentSession.A03;
                        r6.A02 = System.currentTimeMillis();
                        map.put(new AnonymousClass1NP(this, bArr), r6);
                    }
                }
                r5 = null;
            }
            if (r6.isValid()) {
                String peerHost = r6.getPeerHost();
                int peerPort = r6.getPeerPort();
                String cipherSuite = r6.getCipherSuite();
                r5 = new AnonymousClass1NQ(this, peerHost, cipherSuite, peerPort);
                LinkedHashSet linkedHashSet = r6.A07;
                WtCachedPsk wtCachedPsk = null;
                if (linkedHashSet != null && !linkedHashSet.isEmpty()) {
                    Random random = new Random();
                    LinkedHashSet linkedHashSet2 = r6.A07;
                    if (!(linkedHashSet2 == null || (wtCachedPskArr = (WtCachedPsk[]) linkedHashSet2.toArray(new WtCachedPsk[0])) == null)) {
                        int nextInt = random.nextInt(wtCachedPskArr.length);
                        r6.A07.remove(wtCachedPskArr[nextInt]);
                        wtCachedPsk = wtCachedPskArr[nextInt];
                    }
                }
                Certificate[] certificateArr = (Certificate[]) r6.A08.get(Byte.valueOf(wtCachedPsk.certsID));
                if (certificateArr != null) {
                    r5.A03 = wtCachedPsk;
                    r5.A01(certificateArr);
                }
                AnonymousClass14J r1 = this.A00;
                if (r1 != null) {
                    r1.A02(new WtPersistentSession(peerHost, cipherSuite, r6.A07, r6.A08, peerPort), r7.A01);
                }
            } else {
                map.remove(r7);
                AnonymousClass14J r12 = this.A00;
                if (r12 != null) {
                    r12.A03(r7.A01);
                }
                r5 = null;
            }
        }
        return r5;
    }

    @Override // javax.net.ssl.SSLSessionContext
    public int getSessionCacheSize() {
        return this.A02;
    }

    @Override // javax.net.ssl.SSLSessionContext
    public int getSessionTimeout() {
        return (int) this.A03;
    }

    @Override // javax.net.ssl.SSLSessionContext
    public void setSessionCacheSize(int i) {
        if (i >= 0) {
            this.A02 = i;
            return;
        }
        throw new IllegalArgumentException("Cache size < 0");
    }

    @Override // javax.net.ssl.SSLSessionContext
    public void setSessionTimeout(int i) {
        if (i >= 0) {
            this.A03 = (long) i;
            Map map = this.A01;
            synchronized (map) {
                Iterator it = map.values().iterator();
                while (it.hasNext()) {
                    SSLSession sSLSession = (SSLSession) it.next();
                    if (!sSLSession.isValid()) {
                        it.remove();
                        AnonymousClass14J r1 = this.A00;
                        if (r1 != null) {
                            r1.A03(sSLSession.getId());
                        }
                    }
                }
            }
            return;
        }
        throw new IllegalArgumentException("Timeout < 0");
    }
}
