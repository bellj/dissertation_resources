package X;

import org.json.JSONObject;

/* renamed from: X.5ec  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119545ec extends C130745zu {
    public final String A00;

    public C119545ec(AnonymousClass1V8 r2) {
        super(r2);
        this.A00 = r2.A0H("style");
    }

    public C119545ec(JSONObject jSONObject) {
        super(jSONObject);
        String str = "inline_style";
        this.A00 = jSONObject.getString(!jSONObject.has(str) ? "style" : str);
    }

    @Override // X.C130745zu
    public JSONObject A01() {
        return super.A01().put("inline_style", this.A00);
    }
}
