package X;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/* renamed from: X.5IM  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5IM<K, V> extends LinkedHashMap<K, V> {
    public static final AnonymousClass5IM A00;
    public boolean zzfa = true;

    static {
        AnonymousClass5IM r1 = new AnonymousClass5IM();
        A00 = r1;
        r1.zzfa = false;
    }

    public AnonymousClass5IM() {
    }

    public AnonymousClass5IM(Map map) {
        super(map);
    }

    @Override // java.util.LinkedHashMap, java.util.AbstractMap, java.util.Map, java.util.HashMap
    public final Set entrySet() {
        return isEmpty() ? Collections.emptySet() : super.entrySet();
    }

    public static int A00(Object obj) {
        if (obj instanceof byte[]) {
            byte[] bArr = (byte[]) obj;
            int length = bArr.length;
            int i = length;
            for (int i2 = 0; i2 < 0 + length; i2++) {
                i = (i * 31) + bArr[i2];
            }
            if (i == 0) {
                return 1;
            }
            return i;
        } else if (!(obj instanceof AbstractC115135Qi)) {
            return obj.hashCode();
        } else {
            throw C12970iu.A0z();
        }
    }

    @Override // java.util.LinkedHashMap, java.util.AbstractMap, java.util.Map, java.util.HashMap
    public final void clear() {
        if (this.zzfa) {
            super.clear();
            return;
        }
        throw C12970iu.A0z();
    }

    @Override // java.util.AbstractMap, java.util.Map, java.lang.Object
    public final boolean equals(Object obj) {
        boolean equals;
        if (obj instanceof Map) {
            Map map = (Map) obj;
            if (this != map) {
                if (size() == map.size()) {
                    Iterator A0s = C12990iw.A0s(this);
                    while (A0s.hasNext()) {
                        Map.Entry A15 = C12970iu.A15(A0s);
                        if (map.containsKey(A15.getKey())) {
                            Object value = A15.getValue();
                            Object obj2 = map.get(A15.getKey());
                            if (!(value instanceof byte[]) || !(obj2 instanceof byte[])) {
                                equals = value.equals(obj2);
                                continue;
                            } else {
                                equals = Arrays.equals((byte[]) value, (byte[]) obj2);
                                continue;
                            }
                            if (!equals) {
                            }
                        }
                    }
                }
            }
            return true;
        }
        return false;
    }

    @Override // java.util.AbstractMap, java.util.Map, java.lang.Object
    public final int hashCode() {
        Iterator A0s = C12990iw.A0s(this);
        int i = 0;
        while (A0s.hasNext()) {
            Map.Entry A15 = C12970iu.A15(A0s);
            i += A00(A15.getValue()) ^ A00(A15.getKey());
        }
        return i;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.AbstractMap, java.util.Map, java.util.HashMap
    public final Object put(Object obj, Object obj2) {
        if (this.zzfa) {
            return super.put(obj, obj2);
        }
        throw C12970iu.A0z();
    }

    @Override // java.util.AbstractMap, java.util.Map, java.util.HashMap
    public final void putAll(Map map) {
        if (this.zzfa) {
            Iterator A10 = C72453ed.A10(map);
            while (A10.hasNext()) {
                map.get(A10.next());
            }
            super.putAll(map);
            return;
        }
        throw C12970iu.A0z();
    }

    @Override // java.util.AbstractMap, java.util.Map, java.util.HashMap
    public final Object remove(Object obj) {
        if (this.zzfa) {
            return super.remove(obj);
        }
        throw C12970iu.A0z();
    }
}
