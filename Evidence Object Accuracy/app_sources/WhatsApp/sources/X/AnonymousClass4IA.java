package X;

import android.os.Bundle;

/* renamed from: X.4IA  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4IA {
    public final Bundle A00;

    public /* synthetic */ AnonymousClass4IA(Bundle bundle) {
        if (bundle != null) {
            C65273Iw.A01(bundle, Boolean.class, "useDebugKey");
            this.A00 = bundle;
            return;
        }
        throw C12970iu.A0f("Bundle is null");
    }
}
