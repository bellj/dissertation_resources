package X;

import android.graphics.Path;
import android.graphics.PointF;
import java.util.List;

/* renamed from: X.0aP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07990aP implements AbstractC12850if, AbstractC12030hG, AbstractC12870ih {
    public AnonymousClass0OG A00 = new AnonymousClass0OG();
    public boolean A01;
    public final Path A02 = new Path();
    public final AnonymousClass0AA A03;
    public final AnonymousClass0QR A04;
    public final AnonymousClass0QR A05;
    public final C08140ae A06;
    public final String A07;

    public C07990aP(AnonymousClass0AA r4, C08140ae r5, AbstractC08070aX r6) {
        this.A07 = r5.A02;
        this.A03 = r4;
        AnonymousClass0H2 r2 = new AnonymousClass0H2(r5.A00.A00);
        this.A05 = r2;
        AnonymousClass0QR A88 = r5.A01.A88();
        this.A04 = A88;
        this.A06 = r5;
        r6.A03(r2);
        r6.A03(A88);
        r2.A07.add(this);
        A88.A07.add(this);
    }

    @Override // X.AbstractC12480hz
    public void A5q(AnonymousClass0SF r2, Object obj) {
        AnonymousClass0QR r0;
        if (obj == AbstractC12810iX.A01) {
            r0 = this.A05;
        } else if (obj == AbstractC12810iX.A02) {
            r0 = this.A04;
        } else {
            return;
        }
        r0.A08(r2);
    }

    @Override // X.AbstractC12850if
    public Path AF0() {
        boolean z = this.A01;
        Path path = this.A02;
        if (!z) {
            path.reset();
            C08140ae r5 = this.A06;
            if (!r5.A03) {
                PointF pointF = (PointF) this.A05.A03();
                float f = pointF.x / 2.0f;
                float f2 = pointF.y / 2.0f;
                float f3 = f * 0.55228f;
                float f4 = 0.55228f * f2;
                path.reset();
                if (r5.A04) {
                    float f5 = -f2;
                    path.moveTo(0.0f, f5);
                    float f6 = 0.0f - f3;
                    float f7 = -f;
                    float f8 = 0.0f - f4;
                    path.cubicTo(f6, f5, f7, f8, f7, 0.0f);
                    float f9 = f4 + 0.0f;
                    path.cubicTo(f7, f9, f6, f2, 0.0f, f2);
                    float f10 = f3 + 0.0f;
                    path.cubicTo(f10, f2, f, f9, f, 0.0f);
                    path.cubicTo(f, f8, f10, f5, 0.0f, f5);
                } else {
                    float f11 = -f2;
                    path.moveTo(0.0f, f11);
                    float f12 = f3 + 0.0f;
                    float f13 = 0.0f - f4;
                    path.cubicTo(f12, f11, f, f13, f, 0.0f);
                    float f14 = f4 + 0.0f;
                    path.cubicTo(f, f14, f12, f2, 0.0f, f2);
                    float f15 = 0.0f - f3;
                    float f16 = -f;
                    path.cubicTo(f15, f2, f16, f14, f16, 0.0f);
                    path.cubicTo(f16, f13, f15, f11, 0.0f, f11);
                }
                PointF pointF2 = (PointF) this.A04.A03();
                path.offset(pointF2.x, pointF2.y);
                path.close();
                this.A00.A00(path);
            }
            this.A01 = true;
        }
        return path;
    }

    @Override // X.AbstractC12030hG
    public void AYB() {
        this.A01 = false;
        this.A03.invalidateSelf();
    }

    @Override // X.AbstractC12480hz
    public void Aan(C06430To r1, C06430To r2, List list, int i) {
        AnonymousClass0U0.A01(this, r1, r2, list, i);
    }

    @Override // X.AbstractC12470hy
    public void Aby(List list, List list2) {
        for (int i = 0; i < list.size(); i++) {
            AbstractC12470hy r2 = (AbstractC12470hy) list.get(i);
            if (r2 instanceof C07950aL) {
                C07950aL r22 = (C07950aL) r2;
                if (r22.A03 == AnonymousClass0J5.SIMULTANEOUSLY) {
                    this.A00.A00.add(r22);
                    r22.A05.add(this);
                }
            }
        }
    }

    @Override // X.AbstractC12470hy
    public String getName() {
        return this.A07;
    }
}
