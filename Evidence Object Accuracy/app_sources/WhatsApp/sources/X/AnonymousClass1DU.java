package X;

import java.io.File;
import java.util.Map;

/* renamed from: X.1DU  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1DU {
    public final C16590pI A00;
    public final AnonymousClass180 A01;
    public final AnonymousClass1DR A02;
    public final C16120oU A03;

    public AnonymousClass1DU(C16590pI r1, AnonymousClass180 r2, AnonymousClass1DR r3, C16120oU r4) {
        this.A00 = r1;
        this.A03 = r4;
        this.A01 = r2;
        this.A02 = r3;
    }

    public final void A00(File file, Map map) {
        int indexOf;
        String substring;
        String substring2;
        String name = file.getName();
        C47722Cd r3 = new C47722Cd();
        r3.A00 = 0;
        r3.A01 = 1L;
        r3.A05 = "native";
        int indexOf2 = name.indexOf("_");
        if (indexOf2 > 0 && (substring2 = name.substring(0, indexOf2)) != null) {
            r3.A03 = substring2;
        }
        int i = indexOf2 + 1;
        if (i > 1 && (indexOf = name.indexOf("_", i)) >= 0 && (substring = name.substring(i, indexOf)) != null && map.containsKey(substring)) {
            r3.A02 = AnonymousClass180.A00((File) map.get(substring));
        }
        this.A03.A05(r3);
    }
}
