package X;

/* renamed from: X.5K3  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5K3 extends AnonymousClass1WI implements AnonymousClass1J7 {
    public AnonymousClass5K3() {
        super(1);
    }

    @Override // X.AnonymousClass1J7
    public Object AJ4(Object obj) {
        if (!(obj instanceof AbstractC114195Ko)) {
            return null;
        }
        return obj;
    }
}
