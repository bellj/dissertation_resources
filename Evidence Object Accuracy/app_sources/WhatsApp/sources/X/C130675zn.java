package X;

/* renamed from: X.5zn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130675zn {
    public int A00;
    public int A01;
    public String A02;
    public String A03;
    public String A04;
    public String A05;
    public String A06;
    public String A07;
    public String A08;
    public String A09;
    public String A0A;
    public boolean A0B;

    public C130675zn() {
    }

    public C130675zn(AnonymousClass1V8 r5) {
        String str = r5.A00;
        if (str.equals("otp")) {
            this.A0A = "otp";
            this.A04 = r5.A0I("type", null);
            this.A03 = r5.A0I("value", null);
            this.A00 = C28421Nd.A00(r5.A0I("length", null), 6);
            this.A01 = C28421Nd.A00(r5.A0I("resend-interval-sec", null), 60);
        } else if (str.equals("app-to-app")) {
            this.A0A = "app-to-app";
            this.A05 = r5.A0I("value", null);
            this.A08 = r5.A0I("request-payload", null);
            this.A06 = r5.A0I("source", null);
            this.A07 = r5.A0I("intent-action", null);
        } else if (str.equals("customer-service")) {
            this.A0A = "customer-service";
            this.A09 = C117295Zj.A0W(r5, "value");
        }
        this.A0B = "1".equals(r5.A0I("disabled", null));
        this.A02 = r5.A0I("identifier", null);
    }
}
