package X;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import com.whatsapp.R;
import com.whatsapp.migration.export.ui.ExportMigrationActivity;
import com.whatsapp.util.Log;

/* renamed from: X.3F9  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3F9 {
    public final C16590pI A00;
    public final C18360sK A01;
    public final AnonymousClass018 A02;

    public AnonymousClass3F9(C16590pI r1, C18360sK r2, AnonymousClass018 r3) {
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
    }

    public final C005602s A00() {
        Context context = this.A00.A00;
        Intent A0D = C12990iw.A0D(context, ExportMigrationActivity.class);
        A0D.setAction("com.whatsapp.export.ACTION_OPENED_VIA_NOTIFICATION");
        C005602s A00 = C22630zO.A00(context);
        A00.A0J = "other_notifications@1";
        int i = Build.VERSION.SDK_INT;
        int i2 = -2;
        if (i >= 26) {
            i2 = -1;
        }
        A00.A03 = i2;
        A00.A09 = AnonymousClass1UY.A00(context, 0, A0D, 134217728);
        C18360sK.A01(A00, R.drawable.notifybar);
        if (i >= 21) {
            A00.A06 = 1;
        }
        return A00;
    }

    public void A01(int i) {
        Context context = this.A00.A00;
        String string = context.getResources().getString(R.string.export_notification_exporting);
        if (i >= 0) {
            StringBuilder A0k = C12960it.A0k("MessagesExporterNotificationManager/onProgress (");
            A0k.append(i);
            Log.i(C12960it.A0d("%)", A0k));
            A02(string, C12990iw.A0o(context.getResources(), C12970iu.A0r(this.A02, i), C12970iu.A1b(), 0, R.string.export_notification_export_percentage), i, false);
        }
    }

    public final void A02(String str, String str2, int i, boolean z) {
        boolean z2 = true;
        int i2 = 100;
        if (i == -1) {
            z2 = false;
            i2 = 0;
            i = 0;
        }
        C005602s A00 = A00();
        A00.A03(i2, i, false);
        A00.A0D(z);
        A00.A0E(z2);
        A00.A0A(str);
        A00.A09(str2);
        this.A01.A03(31, A00.A01());
    }
}
