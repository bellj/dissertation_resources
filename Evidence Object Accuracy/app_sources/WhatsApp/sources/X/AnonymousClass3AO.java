package X;

import android.view.View;
import java.util.HashMap;

/* renamed from: X.3AO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3AO {
    public static void A00(View view, C14260l7 r16, AnonymousClass28D r17, AbstractC21000wf r18) {
        AnonymousClass3PJ r4;
        String A0I = r17.A0I(42);
        AbstractC14200l1 A0G = r17.A0G(44);
        if (A0G != null) {
            r4 = new AnonymousClass024(r16, r17, A0G) { // from class: X.3PJ
                public final /* synthetic */ C14260l7 A00;
                public final /* synthetic */ AnonymousClass28D A01;
                public final /* synthetic */ AbstractC14200l1 A02;

                {
                    this.A01 = r2;
                    this.A02 = r3;
                    this.A00 = r1;
                }

                @Override // X.AnonymousClass024
                public final void accept(Object obj) {
                    C28701Oq.A01(this.A00, this.A01, C14210l2.A01(new C14210l2(), AnonymousClass3GC.A01((HashMap) obj), 0), this.A02);
                }
            };
        } else {
            r4 = null;
        }
        r18.A6b(r16.A00, view, r4, A0I, r17.A0I(43), r17.A0I(45), r17.A0I(41), r17.A0I(48), AnonymousClass28D.A06(r17), r17.A0I(36), r17.A0I(40), r17.A0O(46, false), r17.A0O(38, false));
    }
}
