package X;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.gallery.MediaGalleryFragment;
import com.whatsapp.gallery.MediaGalleryFragmentBase;
import com.whatsapp.gallerypicker.MediaPickerFragment;
import com.whatsapp.storage.StorageUsageMediaGalleryFragment;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.2TW  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2TW extends AnonymousClass02M implements AnonymousClass2TX {
    public int A00 = 10;
    public long A01 = 0;
    public boolean A02;
    public final MediaGalleryFragmentBase A03;
    public final Map A04 = new HashMap();
    public final /* synthetic */ MediaGalleryFragmentBase A05;

    public AnonymousClass2TW(MediaGalleryFragmentBase mediaGalleryFragmentBase, MediaGalleryFragmentBase mediaGalleryFragmentBase2) {
        this.A05 = mediaGalleryFragmentBase;
        A07(true);
        this.A03 = mediaGalleryFragmentBase2;
    }

    @Override // X.AnonymousClass02M
    public long A00(int i) {
        AbstractC35611iN r1;
        AbstractC35581iK r0 = this.A05.A0H;
        if (r0 != null) {
            r1 = r0.AEC(i);
        } else {
            r1 = null;
        }
        if (!super.A00 || r1 == null) {
            return 0;
        }
        Uri AAE = r1.AAE();
        Map map = this.A04;
        Number number = (Number) map.get(AAE);
        if (number == null) {
            long j = this.A01;
            this.A01 = 1 + j;
            number = Long.valueOf(j);
            map.put(AAE, number);
        }
        return number.longValue();
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void A0A(AnonymousClass03U r3) {
        AnonymousClass2T3 r1 = ((C54902hT) r3).A00;
        r1.setImageBitmap(null);
        r1.A00 = null;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A05.A00;
    }

    public final void A0E(C54912hU r8, StorageUsageMediaGalleryFragment storageUsageMediaGalleryFragment, int i) {
        AbstractC13890kV r0;
        boolean z;
        MediaGalleryFragmentBase mediaGalleryFragmentBase = this.A05;
        if (mediaGalleryFragmentBase.A0O && mediaGalleryFragmentBase.A0H != null) {
            boolean isChecked = r8.A00.isChecked();
            int count = mediaGalleryFragmentBase.A0H.getCount() - 1;
            int i2 = mediaGalleryFragmentBase.A02;
            boolean z2 = true;
            if (!(i2 == 0 || i2 == 1)) {
                z2 = false;
            }
            int i3 = 0;
            if (z2) {
                int i4 = 0;
                while (i3 <= i) {
                    int i5 = ((AnonymousClass2WL) mediaGalleryFragmentBase.A0T.get(i3)).count;
                    if (i3 == i) {
                        count = (i5 + i4) - 1;
                    } else {
                        i4 += i5;
                    }
                    i3++;
                }
                i3 = i4;
            }
            ArrayList arrayList = new ArrayList();
            while (i3 <= count) {
                AbstractC35611iN AEC = mediaGalleryFragmentBase.A0H.AEC(i3);
                if (AEC instanceof AbstractC35601iM) {
                    arrayList.add(((AbstractC35601iM) AEC).A03);
                }
                i3++;
            }
            int size = arrayList.size();
            if (isChecked) {
                if (size != 0) {
                    if (!storageUsageMediaGalleryFragment.A1J()) {
                        ((AbstractC13890kV) storageUsageMediaGalleryFragment.A0C()).AeE((AbstractC15340mz) arrayList.get(0));
                    }
                    r0 = (AbstractC13890kV) storageUsageMediaGalleryFragment.A0C();
                    z = true;
                } else {
                    return;
                }
            } else if (size != 0) {
                r0 = (AbstractC13890kV) storageUsageMediaGalleryFragment.A0C();
                z = false;
            } else {
                return;
            }
            r0.Acq(arrayList, z);
            ((MediaGalleryFragmentBase) storageUsageMediaGalleryFragment).A06.A02();
        }
    }

    @Override // X.AnonymousClass2TX
    public int ABl(int i) {
        MediaGalleryFragmentBase mediaGalleryFragmentBase = this.A05;
        int i2 = mediaGalleryFragmentBase.A02;
        if (i2 == 0 || i2 == 1) {
            return ((AnonymousClass2WL) mediaGalleryFragmentBase.A0T.get(i)).count;
        }
        return mediaGalleryFragmentBase.A00;
    }

    @Override // X.AnonymousClass2TX
    public int ADH() {
        MediaGalleryFragmentBase mediaGalleryFragmentBase = this.A05;
        int i = mediaGalleryFragmentBase.A02;
        if (i == 0 || i == 1) {
            return mediaGalleryFragmentBase.A0T.size();
        }
        return 1;
    }

    @Override // X.AnonymousClass2TX
    public long ADI(int i) {
        MediaGalleryFragmentBase mediaGalleryFragmentBase = this.A05;
        int i2 = mediaGalleryFragmentBase.A02;
        if (i2 == 0 || i2 == 1) {
            return -((Calendar) mediaGalleryFragmentBase.A0T.get(i)).getTimeInMillis();
        }
        return 1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0038, code lost:
        if (r0 == 1) goto L_0x003a;
     */
    @Override // X.AnonymousClass2TX
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ void ANF(X.AnonymousClass03U r8, int r9) {
        /*
            r7 = this;
            X.2hU r8 = (X.C54912hU) r8
            com.whatsapp.gallery.MediaGalleryFragmentBase r4 = r7.A05
            int r1 = r4.A02
            r0 = 1
            if (r1 == 0) goto L_0x005c
            if (r1 == r0) goto L_0x005c
            r0 = 2
            if (r1 != r0) goto L_0x0053
            android.widget.TextView r1 = r8.A01
            r0 = 2131891856(0x7f121690, float:1.9418444E38)
        L_0x0013:
            r1.setText(r0)
        L_0x0016:
            boolean r0 = r4.A0P
            if (r0 == 0) goto L_0x0087
            android.widget.CheckBox r6 = r8.A00
            r0 = 0
            r6.setVisibility(r0)
            boolean r0 = r4.A0O
            r6.setEnabled(r0)
            X.1iK r1 = r4.A0H
            if (r1 == 0) goto L_0x0077
            boolean r0 = r4.A0O
            if (r0 == 0) goto L_0x0077
            int r1 = r1.getCount()
            r5 = 1
            int r1 = r1 - r5
            int r0 = r4.A02
            if (r0 == 0) goto L_0x003a
            r3 = 0
            if (r0 != r5) goto L_0x006c
        L_0x003a:
            r2 = 0
            r3 = 0
        L_0x003c:
            if (r2 > r9) goto L_0x006c
            java.util.List r0 = r4.A0T
            java.lang.Object r0 = r0.get(r2)
            X.2WL r0 = (X.AnonymousClass2WL) r0
            if (r2 != r9) goto L_0x004f
            int r1 = r0.count
            int r1 = r1 + r3
            int r1 = r1 - r5
        L_0x004c:
            int r2 = r2 + 1
            goto L_0x003c
        L_0x004f:
            int r0 = r0.count
            int r3 = r3 + r0
            goto L_0x004c
        L_0x0053:
            r0 = 3
            if (r1 != r0) goto L_0x0016
            android.widget.TextView r1 = r8.A01
            r0 = 2131891916(0x7f1216cc, float:1.9418566E38)
            goto L_0x0013
        L_0x005c:
            android.widget.TextView r1 = r8.A01
            java.util.List r0 = r4.A0T
            java.lang.Object r0 = r0.get(r9)
            java.lang.String r0 = r0.toString()
            r1.setText(r0)
            goto L_0x0016
        L_0x006c:
            if (r3 > r1) goto L_0x0079
            boolean r0 = r4.A1K(r3)
            if (r0 == 0) goto L_0x0077
            int r3 = r3 + 1
            goto L_0x006c
        L_0x0077:
            r0 = 0
            goto L_0x007a
        L_0x0079:
            r0 = 1
        L_0x007a:
            r6.setChecked(r0)
            r1 = 0
            com.facebook.redex.ViewOnClickCListenerShape0S0201000_I0 r0 = new com.facebook.redex.ViewOnClickCListenerShape0S0201000_I0
            r0.<init>(r7, r8, r9, r1)
            r6.setOnClickListener(r0)
            return
        L_0x0087:
            android.widget.CheckBox r1 = r8.A00
            r0 = 8
            r1.setVisibility(r0)
            android.view.View r1 = r8.A0H
            r0 = 0
            r1.setOnClickListener(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2TW.ANF(X.03U, int):void");
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r11, int i) {
        C54902hT r112 = (C54902hT) r11;
        int i2 = this.A00;
        MediaGalleryFragmentBase mediaGalleryFragmentBase = this.A05;
        if (i2 < mediaGalleryFragmentBase.A08.getChildCount()) {
            int childCount = mediaGalleryFragmentBase.A08.getChildCount();
            this.A00 = childCount;
            C04810Nd A00 = mediaGalleryFragmentBase.A08.getRecycledViewPool().A00(1);
            A00.A00 = childCount;
            ArrayList arrayList = A00.A03;
            while (arrayList.size() > childCount) {
                arrayList.remove(arrayList.size() - 1);
            }
        }
        AnonymousClass2T3 r3 = (AnonymousClass2T3) r112.A0H;
        r3.A00 = null;
        r3.setScaleType(ImageView.ScaleType.CENTER_CROP);
        AbstractC35581iK r0 = mediaGalleryFragmentBase.A0H;
        if (r0 != null) {
            AbstractC35611iN AEC = r0.AEC(i);
            r3.setMediaItem(AEC);
            if (r3 instanceof C616631i) {
                C616631i r9 = (C616631i) r3;
                boolean z = false;
                if (mediaGalleryFragmentBase.A02 == 3) {
                    z = true;
                }
                r9.A08 = z;
            }
            if (r3.getTag() instanceof AnonymousClass23D) {
                mediaGalleryFragmentBase.A0L.A01((AnonymousClass23D) r3.getTag());
            }
            if (AEC != null) {
                AnonymousClass3X6 r2 = new AnonymousClass3X6(AEC, this, r3, r112);
                r3.setTag(r2);
                mediaGalleryFragmentBase.A0L.A02(r2, new AnonymousClass3XE(AEC, this, r3, r2));
                r3.setChecked(mediaGalleryFragmentBase.A1K(i));
            } else {
                r3.setScaleType(ImageView.ScaleType.CENTER);
                r3.setBackgroundColor(mediaGalleryFragmentBase.A01);
                r3.setImageDrawable(null);
                r3.setChecked(false);
            }
            if (!this.A02) {
                this.A02 = true;
                r3.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver$OnPreDrawListenerC101964oN(this, r3));
            }
        }
    }

    @Override // X.AnonymousClass2TX
    public /* bridge */ /* synthetic */ AnonymousClass03U AOh(ViewGroup viewGroup) {
        MediaGalleryFragmentBase mediaGalleryFragmentBase = this.A05;
        AnonymousClass018 r3 = mediaGalleryFragmentBase.A0E;
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.media_gallery_section_row, viewGroup, false);
        AnonymousClass028.A0c(inflate, (!r3.A04().A06 ? 1 : 0) ^ 1);
        inflate.setClickable(false);
        C54912hU r32 = new C54912hU(inflate);
        if (mediaGalleryFragmentBase.A0Q) {
            TextView textView = r32.A01;
            textView.setTextColor(AnonymousClass00T.A00(textView.getContext(), R.color.gallery_recents_header_text));
        }
        return r32;
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        AnonymousClass2T3 r1;
        MediaGalleryFragmentBase mediaGalleryFragmentBase = this.A03;
        MediaGalleryFragmentBase mediaGalleryFragmentBase2 = this.A05;
        if (mediaGalleryFragmentBase2 instanceof StorageUsageMediaGalleryFragment) {
            r1 = new C616631i(mediaGalleryFragmentBase2.A0B());
        } else if ((mediaGalleryFragmentBase2 instanceof MediaPickerFragment) || !(mediaGalleryFragmentBase2 instanceof MediaGalleryFragment)) {
            r1 = new AnonymousClass2T1(mediaGalleryFragmentBase2.A0B());
        } else {
            C616631i r12 = new C616631i(mediaGalleryFragmentBase2.A0B());
            r12.A00 = 2;
            r1 = r12;
        }
        return new C54902hT(mediaGalleryFragmentBase, r1);
    }

    @Override // X.AnonymousClass2TX
    public /* bridge */ /* synthetic */ boolean AWm(MotionEvent motionEvent, AnonymousClass03U r6, int i) {
        C54912hU r62 = (C54912hU) r6;
        MediaGalleryFragmentBase mediaGalleryFragmentBase = this.A05;
        if (!mediaGalleryFragmentBase.A0P) {
            return false;
        }
        float x = motionEvent.getX();
        CheckBox checkBox = r62.A00;
        if (x < checkBox.getX() || !mediaGalleryFragmentBase.A0O) {
            return false;
        }
        checkBox.setChecked(!checkBox.isChecked());
        A0E(r62, (StorageUsageMediaGalleryFragment) mediaGalleryFragmentBase, i);
        return true;
    }
}
