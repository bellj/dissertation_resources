package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.2Qr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C50722Qr {
    public final long A00;
    public final C28601Of A01;
    public final C28601Of A02;
    public final UserJid A03;
    public final AnonymousClass1OT A04;
    public final String A05;
    public final String A06;
    public final byte[] A07;

    public C50722Qr(AnonymousClass1OT r3, String str) {
        this.A03 = null;
        this.A05 = null;
        this.A06 = str;
        C28601Of r0 = C28601Of.A01;
        this.A01 = r0;
        this.A02 = r0;
        this.A04 = r3;
        this.A07 = null;
        this.A00 = 0;
    }

    public C50722Qr(C28601Of r2, C28601Of r3, UserJid userJid, AnonymousClass1OT r5, String str, byte[] bArr, long j) {
        this.A03 = userJid;
        this.A05 = str;
        this.A06 = null;
        this.A04 = r5;
        this.A01 = r2;
        this.A02 = r3;
        this.A07 = bArr;
        this.A00 = j;
    }
}
