package X;

import android.database.Cursor;

/* renamed from: X.3Zl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69493Zl implements AbstractC29031Pz {
    public final /* synthetic */ long A00;
    public final /* synthetic */ AnonymousClass3DS A01;

    @Override // X.AbstractC29031Pz
    public String ADy() {
        return "is_starred";
    }

    public C69493Zl(AnonymousClass3DS r1, long j) {
        this.A01 = r1;
        this.A00 = j;
    }

    @Override // X.AbstractC29031Pz
    public void A98(C28631Oi r10) {
        C242114q r0 = this.A01.A06;
        long j = this.A00;
        C16310on A01 = r0.A0D.get();
        try {
            boolean z = true;
            Cursor A09 = A01.A03.A09("SELECT starred FROM message_view WHERE _id = ?", new String[]{Long.toString(j)});
            if (A09.moveToNext()) {
                if (A09.getInt(0) != 1) {
                    z = false;
                }
                Boolean valueOf = Boolean.valueOf(z);
                A09.close();
                A01.close();
                if (valueOf != null) {
                    r10.A00(1, "is_starred", Boolean.valueOf(valueOf.booleanValue()));
                    return;
                }
                return;
            }
            A09.close();
            A01.close();
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
