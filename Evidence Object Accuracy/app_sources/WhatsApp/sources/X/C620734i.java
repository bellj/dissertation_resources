package X;

import android.content.Context;
import android.view.ViewGroup;
import com.google.android.material.chip.Chip;

/* renamed from: X.34i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C620734i extends AbstractC621034l {
    public boolean A00;

    public C620734i(Context context) {
        super(context);
        A01();
        setLayoutParams(new ViewGroup.MarginLayoutParams(-1, -2));
    }

    public static void A00(Context context, Chip chip, int i, int i2) {
        chip.setChipIcon(AnonymousClass2GE.A03(context, C12970iu.A0C(context, i), i2));
        chip.setChipIconSize((float) AnonymousClass3G9.A01(context, 18.0f));
        chip.setChipStartPadding((float) AnonymousClass3G9.A01(context, 1.0f));
        chip.setTextStartPadding((float) AnonymousClass3G9.A01(context, 1.0f));
        chip.setIconStartPadding((float) AnonymousClass3G9.A01(context, 8.0f));
        chip.setIconEndPadding((float) AnonymousClass3G9.A01(context, 4.0f));
    }

    @Override // X.AbstractC74153hP
    public void A01() {
        if (!this.A00) {
            this.A00 = true;
            ((AbstractC621034l) this).A00 = C12960it.A0R(AnonymousClass2P6.A00(generatedComponent()));
        }
    }
}
