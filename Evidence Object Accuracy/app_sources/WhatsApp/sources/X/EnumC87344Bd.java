package X;

/* renamed from: X.4Bd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public enum EnumC87344Bd implements AbstractC115135Qi {
    A01(0),
    /* Fake field, exist only in values array */
    EF15(1),
    /* Fake field, exist only in values array */
    EF23(2),
    /* Fake field, exist only in values array */
    EF31(3),
    /* Fake field, exist only in values array */
    EF39(4);
    
    public final int value;

    EnumC87344Bd(int i) {
        this.value = i;
    }
}
