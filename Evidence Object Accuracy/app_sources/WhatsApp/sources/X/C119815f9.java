package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5f9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119815f9 extends AbstractC30891Zf {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(14);
    public long A00;
    public Boolean A01;
    public String A02;
    public String A03;
    public String A04;
    public String A05;

    @Override // X.AnonymousClass1ZP
    public void A01(AnonymousClass102 r1, AnonymousClass1V8 r2, int i) {
    }

    @Override // X.AnonymousClass1ZP
    public String A03() {
        return null;
    }

    @Override // X.AbstractC30891Zf
    public int A05() {
        return 0;
    }

    @Override // X.AbstractC30891Zf
    public int A06() {
        return 0;
    }

    @Override // X.AbstractC30891Zf
    public long A08() {
        return 0;
    }

    @Override // X.AbstractC30891Zf
    public long A09() {
        return 0;
    }

    @Override // X.AbstractC30891Zf
    public AnonymousClass1ZR A0B() {
        return null;
    }

    @Override // X.AbstractC30891Zf
    public AnonymousClass1ZR A0C() {
        return null;
    }

    @Override // X.AbstractC30891Zf
    public AnonymousClass1ZR A0D() {
        return null;
    }

    @Override // X.AbstractC30891Zf
    public String A0E() {
        return null;
    }

    @Override // X.AbstractC30891Zf
    public String A0F() {
        return null;
    }

    @Override // X.AbstractC30891Zf
    public String A0I() {
        return null;
    }

    @Override // X.AbstractC30891Zf
    public void A0K(int i) {
    }

    @Override // X.AbstractC30891Zf
    public void A0L(int i) {
    }

    @Override // X.AbstractC30891Zf
    public void A0M(int i) {
    }

    @Override // X.AbstractC30891Zf
    public void A0O(long j) {
    }

    @Override // X.AbstractC30891Zf
    public void A0S(String str) {
    }

    @Override // X.AbstractC30891Zf
    public void A0T(String str) {
    }

    @Override // X.AbstractC30891Zf
    public void A0U(String str) {
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // X.AnonymousClass1ZP
    public void A02(List list, int i) {
        String str;
        if (!TextUtils.isEmpty(this.A04)) {
            C117295Zj.A1O("nonce", this.A04, list);
        }
        if (!TextUtils.isEmpty(this.A03)) {
            C117295Zj.A1O("device-id", this.A03, list);
        }
        Boolean bool = this.A01;
        if (bool != null) {
            if (bool.booleanValue()) {
                str = "1";
            } else {
                str = "0";
            }
            C117295Zj.A1O("is_first_send", str, list);
        }
    }

    @Override // X.AbstractC30891Zf, X.AnonymousClass1ZP
    public void A04(String str) {
        try {
            super.A04(str);
            JSONObject A05 = C13000ix.A05(str);
            this.A00 = A05.optLong("expiryTs", this.A00);
            this.A04 = A05.optString("nonce", this.A04);
            this.A03 = A05.optString("deviceId", this.A03);
            this.A02 = A05.optString("amount", this.A02);
            this.A05 = A05.optString("sender-alias", this.A05);
            if (A05.has("isFirstSend")) {
                this.A01 = Boolean.valueOf(A05.optBoolean("isFirstSend", false));
            }
        } catch (JSONException e) {
            Log.w("PAY: BrazilTransactionCountryData fromDBString threw: ", e);
        }
    }

    @Override // X.AbstractC30891Zf
    public long A07() {
        return this.A00;
    }

    @Override // X.AbstractC30891Zf
    public String A0G() {
        return this.A05;
    }

    @Override // X.AbstractC30891Zf
    public String A0H() {
        try {
            JSONObject A0J = A0J();
            long j = this.A00;
            if (j > 0) {
                A0J.put("expiryTs", j);
            }
            String str = this.A04;
            if (str != null) {
                A0J.put("nonce", str);
            }
            String str2 = this.A02;
            if (str2 != null) {
                A0J.put("amount", str2);
            }
            String str3 = this.A03;
            if (str3 != null) {
                A0J.put("deviceId", str3);
            }
            String str4 = this.A05;
            if (str4 != null) {
                A0J.put("sender-alias", str4);
            }
            Boolean bool = this.A01;
            if (bool != null) {
                A0J.put("isFirstSend", bool);
            }
            return A0J.toString();
        } catch (JSONException e) {
            Log.w("PAY: BrazilTransactionCountryData toDBString threw: ", e);
            return null;
        }
    }

    @Override // X.AbstractC30891Zf
    public void A0N(long j) {
        this.A00 = j;
    }

    @Override // X.AbstractC30891Zf
    public void A0R(AbstractC30891Zf r6) {
        super.A0R(r6);
        C119815f9 r62 = (C119815f9) r6;
        long j = r62.A00;
        if (j > 0) {
            this.A00 = j;
        }
        String str = r62.A04;
        if (str != null) {
            this.A04 = str;
        }
        String str2 = r62.A03;
        if (str2 != null) {
            this.A03 = str2;
        }
        String str3 = r62.A02;
        if (str3 != null) {
            this.A02 = str3;
        }
        String str4 = r62.A05;
        if (str4 != null) {
            this.A05 = str4;
        }
        Boolean bool = r62.A01;
        if (bool != null) {
            this.A01 = bool;
        }
    }

    @Override // X.AbstractC30891Zf
    public void A0V(String str) {
        this.A05 = str;
    }

    @Override // X.AbstractC30891Zf, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.A04);
        parcel.writeString(this.A03);
        parcel.writeString(this.A02);
        parcel.writeSerializable(this.A01);
    }
}
