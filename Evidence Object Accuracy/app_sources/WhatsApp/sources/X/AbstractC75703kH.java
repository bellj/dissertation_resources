package X;

import android.view.View;

/* renamed from: X.3kH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC75703kH extends AnonymousClass03U {
    public final AnonymousClass2Jw A00;

    public abstract void A08(AnonymousClass4UW v);

    public AbstractC75703kH(View view, AnonymousClass2Jw r2) {
        super(view);
        this.A00 = r2;
    }
}
