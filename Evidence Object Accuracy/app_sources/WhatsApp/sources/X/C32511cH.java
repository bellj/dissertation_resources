package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.1cH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C32511cH {
    public long A00;
    public C15580nU A01;
    public UserJid A02;
    public String A03;

    public C32511cH(C15580nU r1, UserJid userJid, String str, long j) {
        this.A01 = r1;
        this.A02 = userJid;
        this.A03 = str;
        this.A00 = j;
    }
}
