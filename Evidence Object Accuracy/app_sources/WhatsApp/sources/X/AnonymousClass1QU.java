package X;

import java.io.Closeable;

/* renamed from: X.1QU  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1QU implements Closeable {
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        if (this instanceof AnonymousClass1QT) {
            ((AnonymousClass1QT) this).A04.close();
        }
    }
}
