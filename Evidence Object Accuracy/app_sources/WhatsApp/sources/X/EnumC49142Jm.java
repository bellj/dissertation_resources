package X;

/* renamed from: X.2Jm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public enum EnumC49142Jm {
    A02(1),
    A03(0),
    /* Fake field, exist only in values array */
    EF3(3),
    /* Fake field, exist only in values array */
    EF2(2);
    
    public static final EnumC49142Jm[] A00;
    public final int bits;

    static {
        EnumC49142Jm r3;
        EnumC49142Jm r2;
        EnumC49142Jm r7 = A02;
        A00 = new EnumC49142Jm[]{A03, r7, r2, r3};
    }

    EnumC49142Jm(int i) {
        this.bits = i;
    }
}
