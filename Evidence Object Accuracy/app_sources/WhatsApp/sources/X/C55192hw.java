package X;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.emoji.EmojiContainerView;

/* renamed from: X.2hw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C55192hw extends AnonymousClass03U {
    public int A00;
    public C37471mS A01;
    public final ImageView A02;
    public final EmojiContainerView A03;
    public final AnonymousClass19M A04;
    public final AnonymousClass5UC A05;
    public final C16630pM A06;

    public C55192hw(LayoutInflater layoutInflater, ViewGroup viewGroup, AnonymousClass19M r7, AnonymousClass5UC r8, C16630pM r9, int i) {
        super(layoutInflater.inflate(R.layout.emoji_search_preview, viewGroup, false));
        this.A04 = r7;
        this.A05 = r8;
        this.A06 = r9;
        View view = this.A0H;
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.height = i;
        view.setLayoutParams(layoutParams);
        EmojiContainerView emojiContainerView = (EmojiContainerView) view.findViewById(R.id.emoji_preview_container);
        this.A03 = emojiContainerView;
        this.A02 = C12970iu.A0L(view, R.id.emoji);
        emojiContainerView.setVisibility(0);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002e, code lost:
        if (r1 != false) goto L_0x0030;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A08() {
        /*
            r5 = this;
            X.1mS r0 = r5.A01
            r4 = 0
            if (r0 != 0) goto L_0x000b
            com.whatsapp.emoji.EmojiContainerView r0 = r5.A03
            r0.setOnClickListener(r4)
            return
        L_0x000b:
            android.view.View r2 = r5.A0H
            android.view.ViewGroup$LayoutParams r1 = r2.getLayoutParams()
            int r0 = r1.height
            r1.width = r0
            r2.setLayoutParams(r1)
            com.whatsapp.emoji.EmojiContainerView r3 = r5.A03
            r0 = 12
            X.C12960it.A0x(r3, r5, r0)
            X.1mS r0 = r5.A01
            int[] r0 = r0.A00
            boolean r2 = X.AnonymousClass3JU.A03(r0)
            boolean r1 = X.AnonymousClass3JU.A02(r0)
            if (r2 != 0) goto L_0x0030
            r0 = 0
            if (r1 == 0) goto L_0x0031
        L_0x0030:
            r0 = 1
        L_0x0031:
            r3.A04 = r0
            if (r1 == 0) goto L_0x0063
            X.0pM r1 = r5.A06
            X.1mS r0 = r5.A01
            int[] r0 = r0.A00
            java.lang.Object r0 = r0.clone()
            int[] r0 = (int[]) r0
            int[] r1 = X.AnonymousClass3JF.A03(r1, r0)
            X.1mS r0 = new X.1mS
            r0.<init>(r1)
            r5.A01 = r0
            X.3Mm r4 = new X.3Mm
            r4.<init>()
        L_0x0051:
            r3.setOnLongClickListener(r4)
        L_0x0054:
            android.widget.ImageView r1 = r5.A02
            X.1mS r0 = r5.A01
            java.lang.String r0 = r0.toString()
            r1.setContentDescription(r0)
            r5.A09()
            return
        L_0x0063:
            if (r2 == 0) goto L_0x0051
            X.0pM r1 = r5.A06
            X.1mS r0 = r5.A01
            int[] r0 = r0.A00
            java.lang.Object r2 = r0.clone()
            int[] r2 = (int[]) r2
            java.lang.String r0 = "emoji_modifiers"
            android.content.SharedPreferences r1 = r1.A01(r0)
            java.lang.String r0 = X.AnonymousClass3JF.A00(r2)
            int r0 = X.C12970iu.A01(r1, r0)
            int[] r1 = X.AnonymousClass3JU.A08(r2, r0)
            X.1mS r0 = new X.1mS
            r0.<init>(r1)
            r5.A01 = r0
            X.3Ws r1 = new X.3Ws
            r1.<init>()
            X.3Mx r0 = new X.3Mx
            r0.<init>(r1, r5)
            r3.setOnLongClickListener(r0)
            goto L_0x0054
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C55192hw.A08():void");
    }

    public final void A09() {
        this.A02.setImageDrawable(this.A04.A04(this.A0H.getResources(), new C39511q1(this.A01.A00), 0.75f, -1));
    }
}
