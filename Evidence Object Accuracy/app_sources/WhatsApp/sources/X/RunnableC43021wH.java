package X;

import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import com.facebook.redex.EmptyBaseRunnable0;
import com.whatsapp.Conversation;
import com.whatsapp.R;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.notification.DirectReplyService;
import com.whatsapp.notification.MessageOTPNotificationBroadcastReceiver;

/* renamed from: X.1wH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class RunnableC43021wH extends EmptyBaseRunnable0 implements Runnable {
    public final int A00;
    public final Context A01;
    public final C16210od A02;
    public final C22490zA A03;
    public final C14900mE A04;
    public final C15570nT A05;
    public final C18330sH A06;
    public final C21290xB A07;
    public final AnonymousClass130 A08;
    public final C15550nR A09;
    public final C22700zV A0A;
    public final C15610nY A0B;
    public final C21270x9 A0C;
    public final AnonymousClass11P A0D;
    public final C22160yd A0E;
    public final AnonymousClass01d A0F;
    public final C18360sK A0G;
    public final C15890o4 A0H;
    public final C14820m6 A0I;
    public final AnonymousClass018 A0J;
    public final C19990v2 A0K;
    public final C15680nj A0L;
    public final C15600nX A0M;
    public final C21310xD A0N;
    public final C14850m9 A0O;
    public final C20710wC A0P;
    public final AbstractC14640lm A0Q;
    public final C20220vP A0R;
    public final AnonymousClass11U A0S;
    public final C22630zO A0T;
    public final AnonymousClass13L A0U;
    public final C243815h A0V;
    public final AbstractC15340mz A0W;
    public final C15510nN A0X;
    public final C21820y2 A0Y;
    public final C15860o1 A0Z;
    public final C23000zz A0a;
    public final C21300xC A0b;
    public final C22450z6 A0c;
    public final C237612x A0d;
    public final C21280xA A0e;
    public final boolean A0f;
    public final boolean A0g;
    public final boolean A0h;
    public final boolean A0i;
    public final boolean A0j;

    public RunnableC43021wH(Context context, C16210od r3, C22490zA r4, C14900mE r5, C15570nT r6, C18330sH r7, C21290xB r8, C22670zS r9, AnonymousClass130 r10, C15550nR r11, C22700zV r12, C15610nY r13, C21270x9 r14, AnonymousClass11P r15, C22160yd r16, AnonymousClass01d r17, C18360sK r18, C15890o4 r19, C14820m6 r20, AnonymousClass018 r21, C19990v2 r22, C15680nj r23, C15600nX r24, C21310xD r25, C14850m9 r26, C20710wC r27, AbstractC14640lm r28, C20220vP r29, AnonymousClass11U r30, C22630zO r31, AnonymousClass13L r32, C243815h r33, AbstractC15340mz r34, C15510nN r35, C21820y2 r36, C15860o1 r37, C23000zz r38, C21300xC r39, C22450z6 r40, C237612x r41, C21280xA r42, int i, boolean z, boolean z2, boolean z3, boolean z4) {
        this.A01 = context;
        this.A0O = r26;
        this.A04 = r5;
        this.A05 = r6;
        this.A0K = r22;
        this.A0E = r16;
        this.A0C = r14;
        this.A0e = r42;
        this.A07 = r8;
        this.A08 = r10;
        this.A09 = r11;
        this.A0F = r17;
        this.A0B = r13;
        this.A0J = r21;
        this.A0V = r33;
        this.A0b = r39;
        this.A0P = r27;
        this.A0Z = r37;
        this.A0c = r40;
        this.A0T = r31;
        this.A06 = r7;
        this.A0N = r25;
        this.A0R = r29;
        this.A0a = r38;
        this.A0A = r12;
        this.A0H = r19;
        this.A0I = r20;
        this.A0L = r23;
        this.A0G = r18;
        this.A0Y = r36;
        this.A0M = r24;
        this.A0U = r32;
        this.A0X = r35;
        this.A02 = r3;
        this.A0D = r15;
        this.A0d = r41;
        this.A0S = r30;
        this.A03 = r4;
        this.A0W = r34;
        this.A0i = z;
        this.A0h = z2;
        this.A0g = z3;
        this.A0Q = r28;
        this.A00 = i;
        this.A0f = z4;
        this.A0j = r9.A06();
    }

    public static final AnonymousClass01T A00(Context context, C15370n3 r7, boolean z) {
        Uri withAppendedId = ContentUris.withAppendedId(C42961wB.A00, r7.A08());
        String str = Conversation.A59;
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.Conversation");
        intent.setData(withAppendedId);
        intent.setAction(str);
        intent.addFlags(335544320);
        return new AnonymousClass01T(2, intent.putExtra("fromNotification", true).putExtra("last_notification_keep_in_chat", z));
    }

    public final void A01(C005602s r7, C15370n3 r8, int i, boolean z) {
        if (DirectReplyService.A03()) {
            r7.A0N.add(DirectReplyService.A00(this.A01, r8, "com.whatsapp.intent.action.DIRECT_REPLY_FROM_MESSAGE", i, z));
            return;
        }
        Context context = this.A01;
        Jid jid = r8.A0D;
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.notification.PopupNotification");
        intent.putExtra("popup_notification_extra_quick_reply_jid", C15380n4.A03(jid));
        intent.putExtra("popup_notification_extra_dismiss_notification", true);
        r7.A04(R.drawable.ic_action_reply, context.getString(R.string.notification_quick_reply), AnonymousClass1UY.A00(context, 0, intent, 134217728));
    }

    public final void A02(C005602s r5, C15370n3 r6, AbstractC15340mz r7) {
        Context context = this.A01;
        Intent intent = new Intent(context, MessageOTPNotificationBroadcastReceiver.class);
        intent.putExtra("extra_remote_jid", C15380n4.A03(r6.A0D));
        intent.putExtra("extra_message_key_id", r7.A0z.A01);
        r5.A04(R.drawable.ic_action_copy, context.getString(R.string.notification_otp_copy_code), AnonymousClass1UY.A01(context, 36, intent, 134217728));
        if (Build.VERSION.SDK_INT >= 29) {
            r5.A0R = false;
        }
        this.A0E.A04((C28851Pg) r7, 2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:195:0x0528, code lost:
        if (r9.A0y() != false) goto L_0x052a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0164, code lost:
        if (A05((X.AbstractC14640lm) r4.A0B(X.AbstractC14640lm.class), r7, r6.A00.A0G) == false) goto L_0x0166;
     */
    /* JADX WARNING: Removed duplicated region for block: B:100:0x031f  */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x032f  */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x0354  */
    /* JADX WARNING: Removed duplicated region for block: B:189:0x0515  */
    /* JADX WARNING: Removed duplicated region for block: B:202:0x0547  */
    /* JADX WARNING: Removed duplicated region for block: B:243:0x042e A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:246:0x034e A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0194  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01b5  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0206  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x026e  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x02f0  */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x030f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A03(java.lang.StringBuilder r39, java.util.ArrayList r40, int r41, int r42, int r43, boolean r44, boolean r45, boolean r46) {
        /*
        // Method dump skipped, instructions count: 1598
        */
        throw new UnsupportedOperationException("Method not decompiled: X.RunnableC43021wH.A03(java.lang.StringBuilder, java.util.ArrayList, int, int, int, boolean, boolean, boolean):void");
    }

    public boolean A04(C15370n3 r5) {
        Jid A0B = r5.A0B(AbstractC14640lm.class);
        AnonymousClass009.A05(A0B);
        AbstractC14640lm r1 = (AbstractC14640lm) A0B;
        if (!this.A0P.A0Z(r5, r1)) {
            C14850m9 r3 = this.A0O;
            C23000zz r0 = this.A0a;
            C22700zV r2 = this.A0A;
            UserJid of = UserJid.of(r1);
            if (!C28081Kn.A01(r2, r3, of, r0) && !C28061Kl.A01(r2, r3, of)) {
                return true;
            }
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0038, code lost:
        if ((r2 + 86400000) >= r7) goto L_0x003a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A05(X.AbstractC14640lm r11, int r12, long r13) {
        /*
            r10 = this;
            r9 = 0
            r0 = 50
            if (r12 <= r0) goto L_0x0040
            X.0m6 r0 = r10.A0I
            android.content.SharedPreferences r3 = r0.A00
            java.lang.String r2 = "last_read_conversation_time"
            r0 = 0
            long r7 = r3.getLong(r2, r0)
            X.0v2 r0 = r10.A0K
            java.util.concurrent.ConcurrentHashMap r0 = r0.A0B()
            java.lang.Object r0 = r0.get(r11)
            X.1PE r0 = (X.AnonymousClass1PE) r0
            if (r0 != 0) goto L_0x0041
            r2 = 0
        L_0x0021:
            long r13 = r13 - r2
            r4 = 300000(0x493e0, double:1.482197E-318)
            int r0 = (r13 > r4 ? 1 : (r13 == r4 ? 0 : -1))
            r6 = 0
            if (r0 >= 0) goto L_0x002b
            r6 = 1
        L_0x002b:
            r4 = 0
            int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r0 == 0) goto L_0x003a
            r0 = 86400000(0x5265c00, double:4.2687272E-316)
            long r2 = r2 + r0
            int r1 = (r2 > r7 ? 1 : (r2 == r7 ? 0 : -1))
            r0 = 1
            if (r1 < 0) goto L_0x003b
        L_0x003a:
            r0 = 0
        L_0x003b:
            if (r6 != 0) goto L_0x003f
            if (r0 == 0) goto L_0x0040
        L_0x003f:
            r9 = 1
        L_0x0040:
            return r9
        L_0x0041:
            long r2 = r0.A0X
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: X.RunnableC43021wH.A05(X.0lm, int, long):boolean");
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            RunnableC43021wH r5 = (RunnableC43021wH) obj;
            AbstractC15340mz r2 = this.A0W;
            AbstractC15340mz r0 = r5.A0W;
            if ((r2 != r0 && (r2 == null || r0 == null || !r0.A0z.equals(r2.A0z))) || this.A0g != r5.A0g || this.A0h != r5.A0h || this.A0i != r5.A0i || !C29941Vi.A00(this.A0Q, r5.A0Q) || this.A00 != r5.A00 || this.A0f != r5.A0f) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        int hashCode;
        AbstractC15340mz r0 = this.A0W;
        int i = 0;
        if (r0 == null) {
            hashCode = 0;
        } else {
            hashCode = r0.hashCode();
        }
        int i2 = ((((((hashCode * 31) + (this.A0i ? 1 : 0)) * 31) + (this.A0h ? 1 : 0)) * 31) + (this.A0g ? 1 : 0)) * 31;
        AbstractC14640lm r02 = this.A0Q;
        if (r02 != null) {
            i = r02.hashCode();
        }
        return ((((i2 + i) * 31) + this.A00) * 31) + (this.A0f ? 1 : 0);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Can't wrap try/catch for region: R(56:120|(1:122)|123|(53:125|(2:127|(2:129|(1:131)))|132|133|(2:140|(5:142|(4:145|(3:486|151|491)|487|143)|484|153|(1:155)))|156|(1:158)|160|161|(1:200)|(2:169|(42:171|172|(2:174|(39:176|177|(4:180|(3:456|194|(1:196))|184|(36:192|453|193|454|201|(4:205|(1:207)|208|(7:212|458|213|215|(1:217)|218|(29:220|224|(1:368)(1:228)|229|(2:231|(23:237|238|239|(12:242|(1:244)|246|(1:248)|250|251|(2:(1:359)|360)(1:255)|256|(1:258)|259|260|(2:262|(1:(1:357))(1:265)))|266|(8:269|(1:271)|273|(5:460|350|(1:352)|(1:286)(4:(4:336|337|309|(3:311|(1:313)(1:330)|314)(1:331))|(5:290|(2:332|(1:334))(2:296|(1:298))|(1:302)|303|(2:305|(1:308)))|309|(0)(0))|287)|279|(0)|286|287)|315|(3:317|(1:323)|(1:325))|326|(2:329|327)|492|369|(4:372|(6:375|(1:377)|378|(2:380|494)(1:495)|381|373)|493|382)|383|(2:385|(1:387))(1:388)|447|389|(1:393)(1:392)|(1:395)|396|406|(1:409)|(4:411|(2:413|(2:415|(1:417)))(1:424)|(1:419)|(4:421|(1:423)|42|43)(1:499))(1:498))(1:362))(1:363)|(23:367|239|(9:242|(0)|246|(0)|250|251|(1:253)|(0)|360)|266|(8:269|(0)|273|(7:275|460|350|(0)|(0)|286|287)|279|(0)|286|287)|315|(0)|326|(1:327)|492|369|(4:372|(1:373)|493|382)|383|(0)(0)|447|389|(0)|393|(0)|396|406|(1:409)|(0)(0))|238|239|(0)|266|(0)|315|(0)|326|(1:327)|492|369|(0)|383|(0)(0)|447|389|(0)|393|(0)|396|406|(0)|(0)(0))))|223|224|(1:226)|368|229|(0)(0)|(1:365)|367|239|(0)|266|(0)|315|(0)|326|(1:327)|492|369|(0)|383|(0)(0)|447|389|(0)|393|(0)|396|406|(0)|(0)(0)))|197|453|193|454|201|(5:203|205|(0)|208|(8:210|212|458|213|215|(0)|218|(0)))|223|224|(0)|368|229|(0)(0)|(0)|367|239|(0)|266|(0)|315|(0)|326|(1:327)|492|369|(0)|383|(0)(0)|447|389|(0)|393|(0)|396|406|(0)|(0)(0)))|198|177|(5:180|(1:182)|456|194|(0))|197|453|193|454|201|(0)|223|224|(0)|368|229|(0)(0)|(0)|367|239|(0)|266|(0)|315|(0)|326|(1:327)|492|369|(0)|383|(0)(0)|447|389|(0)|393|(0)|396|406|(0)|(0)(0)))|199|172|(0)|198|177|(0)|197|453|193|454|201|(0)|223|224|(0)|368|229|(0)(0)|(0)|367|239|(0)|266|(0)|315|(0)|326|(1:327)|492|369|(0)|383|(0)(0)|447|389|(0)|393|(0)|396|406|(0)|(0)(0))|152|133|(4:136|138|140|(0))|156|(0)|160|161|(1:163)|200|(2:169|(0))|199|172|(0)|198|177|(0)|197|453|193|454|201|(0)|223|224|(0)|368|229|(0)(0)|(0)|367|239|(0)|266|(0)|315|(0)|326|(1:327)|492|369|(0)|383|(0)(0)|447|389|(0)|393|(0)|396|406|(0)|(0)(0)) */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x02d7, code lost:
        if (r4 == null) goto L_0x02d9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0043, code lost:
        if (r3.A08(r0.getRawString()).A0I == false) goto L_0x0045;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:159:0x0420, code lost:
        if (r0 == false) goto L_0x0422;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:221:0x0565, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:222:0x0566, code lost:
        com.whatsapp.util.Log.e("androidwear/pairedcheck/failed", r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:245:0x063e, code lost:
        if (r12 == false) goto L_0x0640;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:249:0x0645, code lost:
        if (r11 == false) goto L_0x0647;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:272:0x06db, code lost:
        if (r25 != false) goto L_0x06dd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:288:0x071c, code lost:
        if (r7 != false) goto L_0x07a8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:339:0x0885, code lost:
        if (r11.equals("2") == false) goto L_0x071e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:361:0x08da, code lost:
        if (r20 != 1) goto L_0x068d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:397:0x0a22, code lost:
        r7 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:398:0x0a23, code lost:
        r1 = new java.lang.StringBuilder("messagenotification/postSummaryNotification uid=");
        r1.append(android.os.Process.myUid());
        com.whatsapp.util.Log.i(r1.toString(), r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:399:0x0a3c, code lost:
        if (X.C38241nl.A04() != false) goto L_0x0a3e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:400:0x0a3e, code lost:
        if (r1 == null) goto L_0x0b97;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:405:0x0a4c, code lost:
        r0.A0L(r0.A08(r0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:426:0x0b1e, code lost:
        if (X.C38241nl.A0B(r7.toString()) == false) goto L_0x0b20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:427:0x0b20, code lost:
        throw r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:441:0x0b97, code lost:
        throw r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x01eb, code lost:
        if (r5 == 0) goto L_0x01ed;
     */
    /* JADX WARNING: Removed duplicated region for block: B:142:0x03cb  */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x0414  */
    /* JADX WARNING: Removed duplicated region for block: B:171:0x0446  */
    /* JADX WARNING: Removed duplicated region for block: B:174:0x0476  */
    /* JADX WARNING: Removed duplicated region for block: B:179:0x04ab A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:196:0x04df  */
    /* JADX WARNING: Removed duplicated region for block: B:203:0x04f4 A[Catch: Exception -> 0x0565, all -> 0x0b98, TryCatch #6 {Exception -> 0x0565, blocks: (B:201:0x04ee, B:203:0x04f4, B:205:0x04fa, B:207:0x04fe, B:208:0x0511, B:210:0x0515, B:212:0x051b, B:213:0x0533, B:214:0x0539, B:215:0x053e, B:218:0x054c), top: B:454:0x04ee, outer: #4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:207:0x04fe A[Catch: Exception -> 0x0565, all -> 0x0b98, TryCatch #6 {Exception -> 0x0565, blocks: (B:201:0x04ee, B:203:0x04f4, B:205:0x04fa, B:207:0x04fe, B:208:0x0511, B:210:0x0515, B:212:0x051b, B:213:0x0533, B:214:0x0539, B:215:0x053e, B:218:0x054c), top: B:454:0x04ee, outer: #4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:217:0x054b  */
    /* JADX WARNING: Removed duplicated region for block: B:220:0x0564  */
    /* JADX WARNING: Removed duplicated region for block: B:226:0x0580  */
    /* JADX WARNING: Removed duplicated region for block: B:231:0x05b5  */
    /* JADX WARNING: Removed duplicated region for block: B:241:0x062b A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:244:0x063c  */
    /* JADX WARNING: Removed duplicated region for block: B:248:0x0644  */
    /* JADX WARNING: Removed duplicated region for block: B:268:0x06b5 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:271:0x06da  */
    /* JADX WARNING: Removed duplicated region for block: B:281:0x06f3 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:311:0x07ae  */
    /* JADX WARNING: Removed duplicated region for block: B:317:0x07e2  */
    /* JADX WARNING: Removed duplicated region for block: B:329:0x083c A[LOOP:3: B:327:0x0836->B:329:0x083c, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:331:0x0849  */
    /* JADX WARNING: Removed duplicated region for block: B:352:0x08ba  */
    /* JADX WARNING: Removed duplicated region for block: B:359:0x08d3  */
    /* JADX WARNING: Removed duplicated region for block: B:363:0x08f3  */
    /* JADX WARNING: Removed duplicated region for block: B:365:0x0920  */
    /* JADX WARNING: Removed duplicated region for block: B:371:0x095f A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:375:0x0968  */
    /* JADX WARNING: Removed duplicated region for block: B:385:0x09e7  */
    /* JADX WARNING: Removed duplicated region for block: B:388:0x09f5  */
    /* JADX WARNING: Removed duplicated region for block: B:391:0x09fe A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:395:0x0a17 A[Catch: SecurityException -> 0x0a22, TryCatch #1 {SecurityException -> 0x0a22, blocks: (B:389:0x09f6, B:392:0x0a00, B:393:0x0a0a, B:395:0x0a17, B:396:0x0a1c), top: B:447:0x09f6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:408:0x0a79 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:411:0x0a9a  */
    /* JADX WARNING: Removed duplicated region for block: B:497:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:498:? A[RETURN, SYNTHETIC] */
    @Override // java.lang.Runnable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
        // Method dump skipped, instructions count: 3036
        */
        throw new UnsupportedOperationException("Method not decompiled: X.RunnableC43021wH.run():void");
    }
}
