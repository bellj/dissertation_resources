package X;

import java.util.List;

/* renamed from: X.48h  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C866548h extends AbstractC34011fR {
    public final C22410z2 A00;
    public final String A01;
    public final List A02;

    public C866548h(C22410z2 r1, String str, List list) {
        this.A00 = r1;
        this.A02 = list;
        this.A01 = str;
    }

    @Override // X.AbstractC34011fR
    public String A00() {
        StringBuilder A0k = C12960it.A0k("qr_contacts count: ");
        A0k.append(this.A02.size());
        A0k.append(" checksum: ");
        return C12960it.A0d(this.A01, A0k);
    }

    @Override // X.AbstractC34011fR
    public void A01() {
        this.A00.A04(this.A01, this.A02);
    }
}
