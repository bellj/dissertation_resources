package X;

/* renamed from: X.0Pr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05470Pr {
    public final int A00;
    public final int A01;
    public final int A02;
    public final String A03;
    public final String A04;
    public final String A05;
    public final boolean A06;

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x005a, code lost:
        if (r0 != false) goto L_0x005c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C05470Pr(java.lang.String r3, java.lang.String r4, java.lang.String r5, int r6, int r7, boolean r8) {
        /*
            r2 = this;
            r2.<init>()
            r2.A04 = r3
            r2.A05 = r4
            r2.A06 = r8
            r2.A02 = r6
            if (r4 == 0) goto L_0x0060
            java.util.Locale r0 = java.util.Locale.US
            java.lang.String r1 = r4.toUpperCase(r0)
            java.lang.String r0 = "INT"
            boolean r0 = r1.contains(r0)
            if (r0 == 0) goto L_0x0023
            r1 = 3
        L_0x001c:
            r2.A00 = r1
            r2.A03 = r5
            r2.A01 = r7
            return
        L_0x0023:
            java.lang.String r0 = "CHAR"
            boolean r0 = r1.contains(r0)
            if (r0 != 0) goto L_0x005e
            java.lang.String r0 = "CLOB"
            boolean r0 = r1.contains(r0)
            if (r0 != 0) goto L_0x005e
            java.lang.String r0 = "TEXT"
            boolean r0 = r1.contains(r0)
            if (r0 != 0) goto L_0x005e
            java.lang.String r0 = "BLOB"
            boolean r0 = r1.contains(r0)
            if (r0 != 0) goto L_0x0060
            java.lang.String r0 = "REAL"
            boolean r0 = r1.contains(r0)
            if (r0 != 0) goto L_0x005c
            java.lang.String r0 = "FLOA"
            boolean r0 = r1.contains(r0)
            if (r0 != 0) goto L_0x005c
            java.lang.String r0 = "DOUB"
            boolean r0 = r1.contains(r0)
            r1 = 1
            if (r0 == 0) goto L_0x001c
        L_0x005c:
            r1 = 4
            goto L_0x001c
        L_0x005e:
            r1 = 2
            goto L_0x001c
        L_0x0060:
            r1 = 5
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C05470Pr.<init>(java.lang.String, java.lang.String, java.lang.String, int, int, boolean):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0041, code lost:
        if (r1.equals(r0) != false) goto L_0x0060;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x005e, code lost:
        if (r2 != 0) goto L_0x0060;
     */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0064  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x007a A[ORIG_RETURN, RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:52:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r6) {
        /*
            r5 = this;
            r3 = 1
            if (r5 == r6) goto L_0x007b
            r4 = 0
            if (r6 == 0) goto L_0x0043
            java.lang.Class r1 = r5.getClass()
            java.lang.Class r0 = r6.getClass()
            if (r1 != r0) goto L_0x0043
            X.0Pr r6 = (X.C05470Pr) r6
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 20
            if (r1 < r0) goto L_0x0051
            int r2 = r5.A02
            int r1 = r6.A02
        L_0x001c:
            if (r2 != r1) goto L_0x0043
            java.lang.String r1 = r5.A04
            java.lang.String r0 = r6.A04
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0043
            boolean r1 = r5.A06
            boolean r0 = r6.A06
            if (r1 != r0) goto L_0x0043
            int r2 = r5.A01
            r1 = 2
            if (r2 != r3) goto L_0x0044
            int r0 = r6.A01
            if (r0 != r1) goto L_0x0060
            java.lang.String r1 = r5.A03
            if (r1 == 0) goto L_0x0060
            java.lang.String r0 = r6.A03
        L_0x003d:
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0060
        L_0x0043:
            return r4
        L_0x0044:
            if (r2 != r1) goto L_0x005e
            int r0 = r6.A01
            if (r0 != r3) goto L_0x0060
            java.lang.String r1 = r6.A03
            if (r1 == 0) goto L_0x0060
            java.lang.String r0 = r5.A03
            goto L_0x003d
        L_0x0051:
            int r0 = r5.A02
            r2 = 0
            if (r0 <= 0) goto L_0x0057
            r2 = 1
        L_0x0057:
            int r0 = r6.A02
            r1 = 0
            if (r0 <= 0) goto L_0x001c
            r1 = 1
            goto L_0x001c
        L_0x005e:
            if (r2 == 0) goto L_0x0074
        L_0x0060:
            int r0 = r6.A01
            if (r2 != r0) goto L_0x0074
            java.lang.String r1 = r5.A03
            java.lang.String r0 = r6.A03
            if (r1 == 0) goto L_0x0071
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0074
            return r4
        L_0x0071:
            if (r0 == 0) goto L_0x0074
            return r4
        L_0x0074:
            int r1 = r5.A00
            int r0 = r6.A00
            if (r1 == r0) goto L_0x007b
            r3 = 0
        L_0x007b:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C05470Pr.equals(java.lang.Object):boolean");
    }

    public int hashCode() {
        int hashCode = ((this.A04.hashCode() * 31) + this.A00) * 31;
        int i = 1237;
        if (this.A06) {
            i = 1231;
        }
        return ((hashCode + i) * 31) + this.A02;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Column{name='");
        sb.append(this.A04);
        sb.append('\'');
        sb.append(", type='");
        sb.append(this.A05);
        sb.append('\'');
        sb.append(", affinity='");
        sb.append(this.A00);
        sb.append('\'');
        sb.append(", notNull=");
        sb.append(this.A06);
        sb.append(", primaryKeyPosition=");
        sb.append(this.A02);
        sb.append(", defaultValue='");
        sb.append(this.A03);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }
}
