package X;

import java.util.Map;

/* renamed from: X.2Kw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C49472Kw extends AbstractC49422Kr {
    public final C15570nT A00;
    public final C50232Or A01;
    public final C14830m7 A02;
    public final C14820m6 A03;
    public final C16120oU A04;
    public final C450720b A05;
    public final C230810h A06;
    public final C18610sj A07;
    public final C17070qD A08;
    public final C22980zx A09;
    public final C20320vZ A0A;

    public C49472Kw(AbstractC15710nm r8, C15570nT r9, C50232Or r10, C14830m7 r11, C14820m6 r12, C14850m9 r13, C16120oU r14, C450720b r15, C230810h r16, C18610sj r17, C17070qD r18, C22980zx r19, C20320vZ r20, Map map) {
        super(r8, r13, r14, r15, map);
        this.A02 = r11;
        this.A00 = r9;
        this.A04 = r14;
        this.A0A = r20;
        this.A08 = r18;
        this.A09 = r19;
        this.A03 = r12;
        this.A06 = r16;
        this.A07 = r17;
        this.A01 = r10;
        this.A05 = r15;
    }

    public static void A00(C28941Pp r4, AnonymousClass1V8 r5) {
        C15930o9 r0;
        C15930o9 A00 = AnonymousClass2SE.A00(r5);
        if (A00.A00 == 2) {
            r4.A0A = A00;
        } else {
            r4.A09 = A00;
        }
        int A05 = r5.A05("count", 0);
        if (r4.A0J == null || r4.A00() == A05) {
            r4.A0J = Integer.valueOf(A05);
            String A0I = r5.A0I("mediareason", null);
            if (A0I != null) {
                if (A0I.equals("retry")) {
                    Boolean bool = r4.A0F;
                    if (bool == null || bool.booleanValue()) {
                        r4.A0F = true;
                    } else {
                        throw new AnonymousClass1V9("mediareason retry may not mismatch between two enc nodes in the same message");
                    }
                } else {
                    StringBuilder sb = new StringBuilder("unknown mediareason ");
                    sb.append(A0I);
                    throw new AnonymousClass1V9(sb.toString());
                }
            }
            int A052 = r5.A05("duration", -1);
            if (A052 >= 0) {
                r4.A0G = Integer.valueOf(A052);
            }
            C15930o9 r1 = r4.A0A;
            if (r1 != null && (r0 = r4.A09) != null && r1.A01 != r0.A01) {
                throw new AnonymousClass1V9("ciphertext version may not mismatch between two enc nodes in the same message");
            } else if ("hide".equals(r5.A0I("decrypt-fail", null)) || "peer".equals(r4.A0R)) {
                r4.A00 = 1;
            } else {
                r4.A00 = 0;
            }
        } else {
            throw new AnonymousClass1V9("retry count may not mismatch between two enc nodes in the same message");
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x00b4, code lost:
        if (r10 == null) goto L_0x00b6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:200:0x0547, code lost:
        if (r5.A09 == null) goto L_0x0549;
     */
    @Override // X.AbstractC49422Kr
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(X.AnonymousClass1V8 r58) {
        /*
        // Method dump skipped, instructions count: 1787
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C49472Kw.A01(X.1V8):void");
    }
}
