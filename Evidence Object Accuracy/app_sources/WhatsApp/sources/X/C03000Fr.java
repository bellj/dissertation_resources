package X;

import android.view.View;
import android.view.ViewGroup;

/* renamed from: X.0Fr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C03000Fr extends AnonymousClass0ZL {
    @Override // X.AbstractC12410hs
    public float ADB(View view, ViewGroup viewGroup) {
        boolean z = true;
        if (AnonymousClass028.A05(viewGroup) != 1) {
            z = false;
        }
        float translationX = view.getTranslationX();
        float width = (float) viewGroup.getWidth();
        if (z) {
            return translationX + width;
        }
        return translationX - width;
    }
}
