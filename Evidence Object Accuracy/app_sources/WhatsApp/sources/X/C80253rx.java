package X;

import java.util.logging.Level;
import java.util.logging.Logger;

/* renamed from: X.3rx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C80253rx extends AnonymousClass4DU {
    public static final Logger A04 = C72463ee.A0M(C80253rx.class);
    public static final boolean A05 = C95634e6.A06;
    public int A00;
    public C1090250c A01;
    public final int A02;
    public final byte[] A03;

    public static int A01(long j) {
        if ((-128 & j) == 0) {
            return 1;
        }
        if (j < 0) {
            return 10;
        }
        int i = 2;
        if ((-34359738368L & j) != 0) {
            i = 6;
            j >>>= 28;
        }
        if ((-2097152 & j) != 0) {
            i += 2;
            j >>>= 14;
        }
        return (j & -16384) != 0 ? i + 1 : i;
    }

    public C80253rx(byte[] bArr, int i) {
        int length = bArr.length;
        if ((i | 0 | (length - i)) >= 0) {
            this.A03 = bArr;
            this.A00 = 0;
            this.A02 = i;
            return;
        }
        Object[] objArr = new Object[3];
        C12960it.A1P(objArr, length, 0);
        C12960it.A1P(objArr, 0, 1);
        C12960it.A1P(objArr, i, 2);
        throw C12970iu.A0f(String.format("Array range is invalid. Buffer.length=%d, offset=%d, length=%d", objArr));
    }

    public static int A00(int i) {
        return C72453ed.A07(i);
    }

    @Deprecated
    public static int A02(AbstractC117135Yq r4, AnonymousClass5XU r5, int i) {
        int A042 = AnonymousClass4DU.A04(i) << 1;
        AnonymousClass50T r42 = (AnonymousClass50T) r4;
        AbstractC80173rp r2 = (AbstractC80173rp) r42;
        int i2 = r2.zzc;
        if (i2 == -1) {
            i2 = r5.Ah2(r42);
            r2.zzc = i2;
        }
        return A042 + i2;
    }

    public static AnonymousClass493 A03(C80253rx r3, Throwable th) {
        return new AnonymousClass493(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(r3.A00), Integer.valueOf(r3.A02), 1), th);
    }

    public final void A04(byte b) {
        try {
            byte[] bArr = this.A03;
            int i = this.A00;
            this.A00 = i + 1;
            bArr[i] = b;
        } catch (IndexOutOfBoundsException e) {
            throw A03(this, e);
        }
    }

    public final void A05(int i) {
        if (A05 && !AnonymousClass4ZV.A00()) {
            int i2 = this.A02;
            int i3 = this.A00;
            if (i2 - i3 >= 5) {
                int i4 = i & -128;
                byte[] bArr = this.A03;
                this.A00 = i3 + 1;
                long j = (long) i3;
                if (i4 == 0) {
                    C95634e6.A0A(bArr, (byte) i, j);
                    return;
                }
                C95634e6.A0A(bArr, (byte) (i | 128), j);
                int i5 = i >>> 7;
                int i6 = i5 & -128;
                int i7 = this.A00;
                this.A00 = i7 + 1;
                long j2 = (long) i7;
                if (i6 != 0) {
                    C95634e6.A0A(bArr, (byte) (i5 | 128), j2);
                    i5 >>>= 7;
                    int i8 = i5 & -128;
                    int i9 = this.A00;
                    this.A00 = i9 + 1;
                    j2 = (long) i9;
                    if (i8 != 0) {
                        C95634e6.A0A(bArr, (byte) (i5 | 128), j2);
                        i5 >>>= 7;
                        int i10 = i5 & -128;
                        int i11 = this.A00;
                        this.A00 = i11 + 1;
                        j2 = (long) i11;
                        if (i10 != 0) {
                            C95634e6.A0A(bArr, (byte) (i5 | 128), j2);
                            i5 >>>= 7;
                            int i12 = this.A00;
                            this.A00 = i12 + 1;
                            j2 = (long) i12;
                        }
                    }
                }
                C95634e6.A0A(bArr, (byte) i5, j2);
                return;
            }
        }
        while ((i & -128) != 0) {
            try {
                byte[] bArr2 = this.A03;
                int i13 = this.A00;
                this.A00 = i13 + 1;
                bArr2[i13] = (byte) ((i & 127) | 128);
                i >>>= 7;
            } catch (IndexOutOfBoundsException e) {
                throw A03(this, e);
            }
        }
        byte[] bArr3 = this.A03;
        int i14 = this.A00;
        this.A00 = i14 + 1;
        bArr3[i14] = (byte) i;
    }

    public final void A06(int i) {
        try {
            byte[] bArr = this.A03;
            int i2 = this.A00;
            int i3 = i2 + 1;
            this.A00 = i3;
            bArr[i2] = (byte) i;
            int i4 = i3 + 1;
            this.A00 = i4;
            bArr[i3] = (byte) (i >> 8);
            int i5 = i4 + 1;
            this.A00 = i5;
            bArr[i4] = (byte) (i >> 16);
            this.A00 = i5 + 1;
            bArr[i5] = (byte) (i >>> 24);
        } catch (IndexOutOfBoundsException e) {
            throw A03(this, e);
        }
    }

    public final void A07(int i, int i2) {
        AnonymousClass4DU.A07(this, i, 0);
        if (i2 >= 0) {
            A05(i2);
        } else {
            A09((long) i2);
        }
    }

    public final void A08(int i, String str) {
        int A00;
        AnonymousClass4DU.A07(this, i, 2);
        int i2 = this.A00;
        try {
            int length = str.length();
            int A07 = C72453ed.A07(length * 3);
            int A072 = C72453ed.A07(length);
            if (A072 == A07) {
                int i3 = i2 + A072;
                this.A00 = i3;
                A00 = C94634cG.A00.A00(str, this.A03, i3, this.A02 - i3);
                this.A00 = i2;
                A05((A00 - i2) - A072);
            } else {
                A05(C94634cG.A00(str));
                byte[] bArr = this.A03;
                int i4 = this.A00;
                A00 = C94634cG.A00.A00(str, bArr, i4, this.A02 - i4);
            }
            this.A00 = A00;
        } catch (AnonymousClass4CN e) {
            this.A00 = i2;
            A04.logp(Level.WARNING, "com.google.protobuf.CodedOutputStream", "inefficientWriteStringNoTag", "Converting ill-formed UTF-16. Your Protocol Buffer will not round trip correctly!", (Throwable) e);
            byte[] bytes = str.getBytes(C93104Zc.A02);
            try {
                int length2 = bytes.length;
                A05(length2);
                A0C(bytes, 0, length2);
            } catch (AnonymousClass493 e2) {
                throw e2;
            } catch (IndexOutOfBoundsException e3) {
                throw new AnonymousClass493(e3);
            }
        } catch (IndexOutOfBoundsException e4) {
            throw new AnonymousClass493(e4);
        }
    }

    public final void A09(long j) {
        if (!A05 || this.A02 - this.A00 < 10) {
            while ((j & -128) != 0) {
                try {
                    byte[] bArr = this.A03;
                    int i = this.A00;
                    this.A00 = i + 1;
                    bArr[i] = (byte) ((((int) j) & 127) | 128);
                    j >>>= 7;
                } catch (IndexOutOfBoundsException e) {
                    throw A03(this, e);
                }
            }
            byte[] bArr2 = this.A03;
            int i2 = this.A00;
            this.A00 = i2 + 1;
            bArr2[i2] = (byte) ((int) j);
            return;
        }
        while (true) {
            int i3 = ((j & -128) > 0 ? 1 : ((j & -128) == 0 ? 0 : -1));
            byte[] bArr3 = this.A03;
            int i4 = this.A00;
            this.A00 = i4 + 1;
            long j2 = (long) i4;
            int i5 = (int) j;
            if (i3 == 0) {
                C95634e6.A0A(bArr3, (byte) i5, j2);
                return;
            } else {
                C95634e6.A0A(bArr3, (byte) ((i5 & 127) | 128), j2);
                j >>>= 7;
            }
        }
    }

    public final void A0A(long j) {
        try {
            byte[] bArr = this.A03;
            int i = this.A00;
            int i2 = i + 1;
            this.A00 = i2;
            bArr[i] = (byte) ((int) j);
            int i3 = i2 + 1;
            this.A00 = i3;
            C72453ed.A15(j, bArr, 8, i2);
            int i4 = i3 + 1;
            this.A00 = i4;
            C72453ed.A15(j, bArr, 16, i3);
            int i5 = i4 + 1;
            this.A00 = i5;
            C72453ed.A15(j, bArr, 24, i4);
            int i6 = i5 + 1;
            this.A00 = i6;
            C72453ed.A15(j, bArr, 32, i5);
            int i7 = i6 + 1;
            this.A00 = i7;
            C72453ed.A15(j, bArr, 40, i6);
            int i8 = i7 + 1;
            this.A00 = i8;
            C72453ed.A15(j, bArr, 48, i7);
            this.A00 = i8 + 1;
            C72453ed.A15(j, bArr, 56, i8);
        } catch (IndexOutOfBoundsException e) {
            throw A03(this, e);
        }
    }

    public final void A0B(AbstractC111925Bi r4, int i) {
        AnonymousClass4DU.A07(this, i, 2);
        A05(r4.A02());
        C80273rz r42 = (C80273rz) r4;
        A0C(r42.zzb, r42.A03(), r42.A02());
    }

    public final void A0C(byte[] bArr, int i, int i2) {
        try {
            System.arraycopy(bArr, i, this.A03, this.A00, i2);
            this.A00 += i2;
        } catch (IndexOutOfBoundsException e) {
            Object[] objArr = new Object[3];
            C12960it.A1P(objArr, this.A00, 0);
            C12960it.A1P(objArr, this.A02, 1);
            C12960it.A1P(objArr, i2, 2);
            throw new AnonymousClass493(String.format("Pos: %d, limit: %d, len: %d", objArr), e);
        }
    }
}
