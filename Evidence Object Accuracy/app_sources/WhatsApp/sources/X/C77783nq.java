package X;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;

/* renamed from: X.3nq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77783nq extends BasePendingResult {
    public final AnonymousClass5SX A00;

    @Override // com.google.android.gms.common.api.internal.BasePendingResult
    public final AnonymousClass5SX A02(Status status) {
        return this.A00;
    }

    public C77783nq(AnonymousClass5SX r2) {
        super(null);
        this.A00 = r2;
    }
}
