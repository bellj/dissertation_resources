package X;

import java.util.List;

/* renamed from: X.5Gk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C113165Gk implements AnonymousClass5VU {
    public int A00 = -1;
    public final int A01;
    public final List A02;

    public C113165Gk(List list) {
        this.A02 = list;
        this.A01 = list.size();
    }

    @Override // X.AnonymousClass5VU
    public String readLine() {
        int i = this.A00 + 1;
        this.A00 = i;
        if (i < this.A01) {
            return ((C90644Or) this.A02.get(i)).A00;
        }
        return null;
    }
}
