package X;

/* renamed from: X.1xj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C43871xj {
    public final long A00;

    public C43871xj(long j) {
        this.A00 = j;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass() && this.A00 == ((C43871xj) obj).A00) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return Long.valueOf(this.A00).hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Timestamp{timeInMillis=");
        sb.append(this.A00);
        sb.append('}');
        return sb.toString();
    }
}
