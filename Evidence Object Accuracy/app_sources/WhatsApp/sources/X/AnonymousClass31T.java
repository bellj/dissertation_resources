package X;

/* renamed from: X.31T  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass31T extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Boolean A02;
    public Boolean A03;
    public Boolean A04;
    public Integer A05;
    public Integer A06;
    public Integer A07;
    public Integer A08;
    public Integer A09;
    public Long A0A;
    public Long A0B;
    public Long A0C;
    public Long A0D;
    public Long A0E;
    public Long A0F;
    public Long A0G;
    public Long A0H;
    public Long A0I;
    public Long A0J;
    public String A0K;

    public AnonymousClass31T() {
        super(1658, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(23, this.A00);
        r3.Abe(25, this.A01);
        r3.Abe(4, this.A05);
        r3.Abe(17, this.A0K);
        r3.Abe(18, this.A0A);
        r3.Abe(19, this.A02);
        r3.Abe(22, this.A03);
        r3.Abe(14, this.A0B);
        r3.Abe(16, this.A0C);
        r3.Abe(7, this.A0D);
        r3.Abe(5, this.A0E);
        r3.Abe(8, this.A0F);
        r3.Abe(9, this.A04);
        r3.Abe(10, this.A0G);
        r3.Abe(3, this.A06);
        r3.Abe(6, this.A0H);
        r3.Abe(2, this.A0I);
        r3.Abe(11, this.A07);
        r3.Abe(1, this.A0J);
        r3.Abe(26, this.A08);
        r3.Abe(27, this.A09);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamStatusItemView {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "isPosterBiz", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "isPosterInAddressBook", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "mediaType", C12960it.A0Y(this.A05));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "psaCampaignId", this.A0K);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "psaCampaignItemIndex", this.A0A);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "psaLinkAvailable", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "psaLinkClick", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "statusItemImpressionCount", this.A0B);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "statusItemIndex", this.A0C);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "statusItemLength", this.A0D);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "statusItemLoadTime", this.A0E);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "statusItemReplied", this.A0F);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "statusItemUnread", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "statusItemViewCount", this.A0G);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "statusItemViewResult", C12960it.A0Y(this.A06));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "statusItemViewTime", this.A0H);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "statusRowIndex", this.A0I);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "statusRowSection", C12960it.A0Y(this.A07));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "statusViewerSessionId", this.A0J);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "urlStatusClicked", C12960it.A0Y(this.A08));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "urlStatusType", C12960it.A0Y(this.A09));
        return C12960it.A0d("}", A0k);
    }
}
