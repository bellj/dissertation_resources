package X;

import android.view.animation.Animation;
import android.widget.LinearLayout;

/* renamed from: X.2oB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58122oB extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ AnonymousClass2AT A00;

    public C58122oB(AnonymousClass2AT r1) {
        this.A00 = r1;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        AnonymousClass2AS r3 = this.A00.A00;
        r3.clearAnimation();
        r3.A0E = false;
        r3.setEnabled(true);
        r3.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
    }
}
