package X;

import android.os.Bundle;
import java.io.IOException;
import java.util.concurrent.CancellationException;

/* renamed from: X.3TM  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3TM implements AbstractC115765Sv {
    public final /* synthetic */ C13510jq A00;

    public AnonymousClass3TM(C13510jq r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC115765Sv
    public final /* synthetic */ Object Ael(C13600jz r5) {
        Object obj;
        synchronized (r5.A04) {
            C13020j0.A04("Task is not yet complete", r5.A02);
            if (r5.A05) {
                throw new CancellationException("Task is already canceled.");
            } else if (!IOException.class.isInstance(r5.A00)) {
                Exception exc = r5.A00;
                if (exc == null) {
                    obj = r5.A01;
                } else {
                    throw new C113255Gu(exc);
                }
            } else {
                throw ((Throwable) IOException.class.cast(r5.A00));
            }
        }
        return C13510jq.A00((Bundle) obj);
    }
}
