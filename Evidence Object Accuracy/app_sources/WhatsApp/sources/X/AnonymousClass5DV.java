package X;

import android.content.Context;
import java.util.concurrent.Callable;

/* renamed from: X.5DV  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5DV implements Callable {
    public final /* synthetic */ Context A00;

    public AnonymousClass5DV(Context context) {
        this.A00 = context;
    }

    @Override // java.util.concurrent.Callable
    public final /* synthetic */ Object call() {
        return this.A00.getSharedPreferences("google_sdk_flags", 0);
    }
}
