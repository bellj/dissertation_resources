package X;

/* renamed from: X.13d  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C238213d {
    public final C14820m6 A00;
    public final C18770sz A01;
    public final C14840m8 A02;

    public C238213d(C14820m6 r1, C18770sz r2, C14840m8 r3) {
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = r1;
    }

    public boolean A00(AbstractC14640lm r2) {
        if (!A01(r2)) {
            return this.A02.A05() && !this.A01.A06().A00.isEmpty();
        }
        return true;
    }

    public boolean A01(AbstractC14640lm r4) {
        return C15380n4.A0J(r4) || this.A00.A00.getBoolean("read_receipts_enabled", true);
    }

    public boolean A02(AbstractC15340mz r6) {
        return A01(r6.A0z.A00) && r6.A0I >= 1415214000000L && !(r6 instanceof AnonymousClass1Iv);
    }
}
