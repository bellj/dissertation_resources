package X;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Locale;

/* renamed from: X.1Yn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30711Yn {
    public static final C30711Yn A01 = new C30711Yn("USD");
    public static final C30711Yn A02 = new C30711Yn("XXX");
    public final String A00;

    public C30711Yn(String str) {
        if (str.length() == 3) {
            this.A00 = str.toUpperCase(Locale.US);
            return;
        }
        StringBuilder sb = new StringBuilder("invalid currency code; currencyCode=");
        sb.append(str);
        throw new IllegalArgumentException(sb.toString());
    }

    public static int A00(String str) {
        Number number = (Number) AnonymousClass3B6.A01.get(str.toUpperCase(Locale.US));
        if (number == null) {
            return 2;
        }
        return number.intValue();
    }

    public final AnonymousClass1Z0 A01(AnonymousClass018 r7, int i, boolean z) {
        String A08 = r7.A08(AnonymousClass1Z0.A00(AbstractC27291Gt.A03(AnonymousClass018.A00(r7.A00))));
        if (A08.isEmpty()) {
            A08 = AnonymousClass1Z0.A0A;
        }
        AnonymousClass1Z2 r5 = new AnonymousClass1Z2(A08, z);
        Locale A00 = AnonymousClass018.A00(r7.A00);
        String str = r5.A01.A00;
        String str2 = r5.A00.A00;
        if (!str.equals(str2)) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(";");
            sb.append(str2);
            str = sb.toString();
        }
        AnonymousClass1Z0 r1 = new AnonymousClass1Z0(r5, new AnonymousClass1Z1(str, A00), r7);
        r1.A00 = this.A00;
        r1.A01 = A02(r7);
        r1.A07.A03(i);
        return r1;
    }

    public String A02(AnonymousClass018 r3) {
        HashMap hashMap = AnonymousClass3B6.A02;
        String str = this.A00;
        Number number = (Number) hashMap.get(str);
        if (number == null) {
            return str;
        }
        return A04(r3.A08(number.intValue()));
    }

    public String A03(AnonymousClass018 r5, BigDecimal bigDecimal, boolean z) {
        AnonymousClass1Z0 A012 = A01(r5, bigDecimal.scale(), z);
        String A022 = A012.A07.A02(bigDecimal);
        if (!A012.A02.A02) {
            return A022;
        }
        boolean z2 = false;
        if (bigDecimal.compareTo(BigDecimal.ZERO) < 0) {
            z2 = true;
        }
        return A012.A01(A022, z2);
    }

    public final String A04(String str) {
        if (str.isEmpty()) {
            return this.A00;
        }
        AnonymousClass3HA r8 = AnonymousClass3HA.A03;
        if (r8.A01(str)) {
            return str;
        }
        int length = str.length();
        StringBuilder sb = new StringBuilder(length + 2);
        int i = 0;
        while (i < length) {
            int codePointAt = str.codePointAt(i);
            String[] strArr = (String[]) AnonymousClass3B1.A00.get(Integer.valueOf(codePointAt));
            if (strArr != null && strArr.length != 0) {
                if (strArr.length != 1) {
                    int length2 = strArr.length;
                    int i2 = 0;
                    while (true) {
                        if (i2 >= length2) {
                            break;
                        }
                        String str2 = strArr[i2];
                        if (!r8.A01(str2)) {
                            i2++;
                        } else if (str2 != null) {
                            sb.append(str2);
                        }
                    }
                } else {
                    sb.append(strArr[0]);
                }
                i += Character.charCount(codePointAt);
            }
            sb.appendCodePoint(codePointAt);
            i += Character.charCount(codePointAt);
        }
        return sb.toString();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.A00.equals(((C30711Yn) obj).A00);
    }

    public int hashCode() {
        return this.A00.hashCode();
    }
}
