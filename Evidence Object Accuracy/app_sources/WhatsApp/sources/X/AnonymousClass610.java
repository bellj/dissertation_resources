package X;

import com.whatsapp.payments.ui.NoviCreateClaimActivity;

/* renamed from: X.610  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass610 {
    public final C128365vz A00;

    public AnonymousClass610(String str, String str2) {
        C128365vz r0 = new C128365vz();
        this.A00 = r0;
        r0.A0X = str;
        r0.A0F = str2;
    }

    public AnonymousClass610(String str, String str2, String str3) {
        C128365vz r0 = new C128365vz();
        this.A00 = r0;
        r0.A0X = str;
        r0.A0j = str2;
        r0.A0Y = str3;
    }

    public AnonymousClass610(String str, String str2, String str3, String str4) {
        C128365vz r0 = new C128365vz();
        this.A00 = r0;
        r0.A0X = str;
        r0.A0j = str3;
        r0.A0F = str2;
        r0.A0Y = str4;
    }

    public static AnonymousClass610 A00(NoviCreateClaimActivity noviCreateClaimActivity, String str, String str2) {
        AnonymousClass610 r4 = new AnonymousClass610(str, "REPORT_TRANSACTION", "CREATE_CLAIM", str2);
        AnonymousClass1IR r3 = noviCreateClaimActivity.A07;
        if (r3 != null) {
            String str3 = r3.A0K;
            C128365vz r2 = r4.A00;
            r2.A0m = str3;
            r2.A0Q = C125015qX.A00(C31001Zq.A05(r3.A03, r3.A02));
        }
        return r4;
    }

    public static AnonymousClass610 A01(String str) {
        return new AnonymousClass610(str, "NOVI_HUB", "HOME_TAB", "LIST");
    }

    public static AnonymousClass610 A02(String str, String str2) {
        return new AnonymousClass610(str, str2, "REVIEW_TRANSACTION", "SCREEN");
    }

    public static AnonymousClass610 A03(String str, String str2, String str3) {
        return new AnonymousClass610(str, str2, str3, "BUTTON");
    }

    public static Long A04(AbstractC30791Yv r1, AnonymousClass6F2 r2) {
        return Long.valueOf(C130325zE.A00(r1, r2.A01));
    }

    public void A05(C1315863i r9, C1316263m r10, C1315163b r11, AnonymousClass63X r12) {
        C128365vz r3 = this.A00;
        r3.A0n = "P2P";
        r3.A0Z = Long.toString(r9.A05.A01);
        if (r10 != null) {
            AnonymousClass6F2 r2 = r10.A02;
            AbstractC30791Yv r1 = r2.A00;
            r3.A0l = ((AbstractC30781Yu) r1).A04;
            r3.A0C = A04(r1, r2);
            AnonymousClass6F2 r22 = r10.A01;
            AbstractC30791Yv r13 = r22.A00;
            r3.A0U = ((AbstractC30781Yu) r13).A04;
            r3.A05 = A04(r13, r22);
        }
        if (r11 != null) {
            C1316363n r0 = r11.A05.A00;
            AnonymousClass6F2 r6 = r0.A01;
            AnonymousClass6F2 r7 = r0.A02;
            C1316363n r02 = r11.A03.A01;
            AnonymousClass6F2 r4 = r02.A01;
            AnonymousClass6F2 r5 = r02.A02;
            AbstractC30791Yv r23 = r6.A00;
            r3.A0d = ((AbstractC30781Yu) r23).A04;
            AbstractC30791Yv r14 = r7.A00;
            r3.A0e = ((AbstractC30781Yu) r14).A04;
            r3.A0B = A04(r14, r7);
            r3.A0A = A04(r23, r6);
            AbstractC30791Yv r24 = r4.A00;
            r3.A0a = ((AbstractC30781Yu) r24).A04;
            AbstractC30791Yv r15 = r5.A00;
            r3.A0b = ((AbstractC30781Yu) r15).A04;
            r3.A08 = A04(r15, r5);
            r3.A07 = A04(r24, r4);
        }
        if (r12 != null) {
            r3.A01 = Boolean.TRUE;
            r3.A0M = r12.A00.A0A;
            C1316363n r42 = r12.A01;
            AnonymousClass6F2 r16 = r42.A02;
            C30821Yy r03 = r16.A01;
            AbstractC30791Yv r25 = r16.A00;
            r3.A04 = Long.valueOf(C130325zE.A00(r25, r03));
            r3.A0H = ((AbstractC30781Yu) r25).A04;
            AnonymousClass6F2 r17 = r42.A01;
            C30821Yy r04 = r17.A01;
            AbstractC30791Yv r26 = r17.A00;
            r3.A03 = Long.valueOf(C130325zE.A00(r26, r04));
            r3.A0G = ((AbstractC30781Yu) r26).A04;
            return;
        }
        r3.A01 = Boolean.FALSE;
    }

    public void A06(C1316263m r5) {
        C128365vz r3 = this.A00;
        AnonymousClass6F2 r2 = r5.A02;
        AbstractC30791Yv r1 = r2.A00;
        r3.A0l = ((AbstractC30781Yu) r1).A04;
        r3.A0C = A04(r1, r2);
        AnonymousClass6F2 r22 = r5.A01;
        AbstractC30791Yv r12 = r22.A00;
        r3.A0U = ((AbstractC30781Yu) r12).A04;
        r3.A05 = A04(r12, r22);
    }

    public void A07(C1316263m r5, C1309460p r6) {
        if (r6 != null) {
            C128365vz r3 = this.A00;
            r3.A0Z = r6.A02.A04;
            AnonymousClass6F2 r2 = r6.A01;
            if (r2 != null) {
                AbstractC30791Yv r1 = r2.A00;
                r3.A0H = ((AbstractC30781Yu) r1).A04;
                r3.A04 = A04(r1, r2);
            }
            AnonymousClass6F2 r22 = r6.A00;
            AbstractC30791Yv r12 = r22.A00;
            r3.A0G = ((AbstractC30781Yu) r12).A04;
            r3.A03 = A04(r12, r22);
        }
        if (r5 != null) {
            A06(r5);
        }
    }

    public void A08(C1316263m r5, C1309460p r6) {
        if (r6 != null) {
            C128365vz r3 = this.A00;
            r3.A0Z = r6.A02.A04;
            AnonymousClass6F2 r2 = r6.A01;
            if (r2 != null) {
                AbstractC30791Yv r1 = r2.A00;
                r3.A0e = ((AbstractC30781Yu) r1).A04;
                r3.A0B = A04(r1, r2);
            }
            AnonymousClass6F2 r22 = r6.A00;
            AbstractC30791Yv r12 = r22.A00;
            r3.A0d = ((AbstractC30781Yu) r12).A04;
            r3.A0A = A04(r12, r22);
        }
        if (r5 != null) {
            A06(r5);
        }
    }
}
