package X;

import com.whatsapp.util.Log;

/* renamed from: X.5Jt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C113985Jt extends AnonymousClass1WI implements AnonymousClass1J7 {
    public C113985Jt() {
        super(1);
    }

    @Override // X.AnonymousClass1J7
    public /* bridge */ /* synthetic */ Object AJ4(Object obj) {
        Throwable th = (Throwable) obj;
        C16700pc.A0E(th, 0);
        Log.e(th);
        return AnonymousClass1WZ.A00;
    }
}
