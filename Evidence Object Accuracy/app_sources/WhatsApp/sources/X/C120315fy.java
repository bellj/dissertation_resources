package X;

import android.content.Context;
import android.text.TextUtils;
import com.whatsapp.payments.IDxRCallbackShape2S0100000_3_I1;
import com.whatsapp.util.Log;
import java.util.ArrayList;

/* renamed from: X.5fy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120315fy extends AbstractC124175oj {
    public final Context A00;
    public final C14900mE A01;
    public final C15570nT A02;
    public final C18640sm A03;
    public final C14830m7 A04;
    public final AnonymousClass102 A05;
    public final C18650sn A06;
    public final C18610sj A07;
    public final C17070qD A08;
    public final C129565xv A09;
    public final C128325vv A0A;
    public final C128755wc A0B;
    public final C18590sh A0C;
    public final AbstractC14440lR A0D;

    public C120315fy(Context context, C14900mE r9, C15570nT r10, C18640sm r11, C14830m7 r12, AnonymousClass102 r13, C18650sn r14, C18600si r15, C18610sj r16, C17070qD r17, C129565xv r18, C128325vv r19, C128755wc r20, C18590sh r21, AnonymousClass1BY r22, C22120yY r23, AbstractC14440lR r24) {
        super(r11, r15, r16, r22, r23);
        this.A04 = r12;
        this.A00 = context;
        this.A01 = r9;
        this.A02 = r10;
        this.A0D = r24;
        this.A0C = r21;
        this.A08 = r17;
        this.A09 = r18;
        this.A07 = r16;
        this.A05 = r13;
        this.A03 = r11;
        this.A06 = r14;
        this.A0A = r19;
        this.A0B = r20;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        String str;
        String str2;
        String str3;
        AnonymousClass01T r9 = (AnonymousClass01T) obj;
        String str4 = (String) r9.A00;
        C452120p r1 = (C452120p) r9.A01;
        if (str4 == null) {
            Log.e(C12960it.A0b("PAY: BrazilMerchantRegAction token error: ", r1));
            this.A0B.A00(r1);
            return;
        }
        String A0U = C117295Zj.A0U(this.A02, this.A04);
        ArrayList A0l = C12960it.A0l();
        C117295Zj.A1M("action", "br-reg-merchant", A0l);
        C117295Zj.A1M("nonce", A0U, A0l);
        C128325vv r7 = this.A0A;
        String str5 = r7.A0I;
        AnonymousClass009.A04(str5);
        C117295Zj.A1M("tax-id", str5, A0l);
        String str6 = r7.A07;
        AnonymousClass009.A04(str6);
        C117295Zj.A1M("business-name", str6, A0l);
        String str7 = r7.A0A;
        AnonymousClass009.A04(str7);
        C117295Zj.A1M("email", str7, A0l);
        String str8 = r7.A0D;
        AnonymousClass009.A04(str8);
        C117295Zj.A1M("owner-name", str8, A0l);
        String str9 = r7.A09;
        AnonymousClass009.A04(str9);
        C117295Zj.A1M("cpf", str9, A0l);
        C117295Zj.A1M("bank-token", str4, A0l);
        String str10 = r7.A06;
        AnonymousClass009.A04(str10);
        C117295Zj.A1M("bank-code", str10, A0l);
        String str11 = r7.A05;
        AnonymousClass009.A04(str11);
        C117295Zj.A1M("bank-branch", str11, A0l);
        String str12 = r7.A04;
        AnonymousClass009.A04(str12);
        C117295Zj.A1M("bank-account-type", str12, A0l);
        C117295Zj.A1M("device-id", this.A0C.A01(), A0l);
        AnonymousClass1V8[] r4 = new AnonymousClass1V8[2];
        r4[0] = this.A09.A00(r7.A02, r7.A01, r7.A00);
        AnonymousClass1W9[] r3 = new AnonymousClass1W9[8];
        if (TextUtils.isEmpty(r7.A0B)) {
            str = "";
        } else {
            str = r7.A0B;
        }
        C12960it.A1M("house-number", str, r3, 0);
        String str13 = r7.A0G;
        AnonymousClass009.A04(str13);
        C12960it.A1M("street", str13, r3, 1);
        if (TextUtils.isEmpty(r7.A0H)) {
            str2 = "";
        } else {
            str2 = r7.A0H;
        }
        C117295Zj.A1P("extra-line", str2, r3);
        if (TextUtils.isEmpty(r7.A0C)) {
            str3 = "";
        } else {
            str3 = r7.A0C;
        }
        C117305Zk.A1Q("neighborhood", str3, r3);
        String str14 = r7.A08;
        AnonymousClass009.A04(str14);
        r3[4] = new AnonymousClass1W9("city", str14);
        String str15 = r7.A0F;
        AnonymousClass009.A04(str15);
        r3[5] = new AnonymousClass1W9("state", str15);
        r3[6] = new AnonymousClass1W9("country", "BR");
        String str16 = r7.A0E;
        AnonymousClass009.A04(str16);
        r3[7] = new AnonymousClass1W9("post-code", str16);
        r4[1] = new AnonymousClass1V8("address", r3);
        C117305Zk.A1J(this.A07, new IDxRCallbackShape2S0100000_3_I1(this.A00, this.A01, this.A06, this, 5), C117295Zj.A0L(A0l, r4));
    }
}
