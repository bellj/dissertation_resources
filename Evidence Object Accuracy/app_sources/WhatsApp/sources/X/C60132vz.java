package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.calling.callgrid.viewmodel.CallGridViewModel;

/* renamed from: X.2vz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60132vz extends C60142w0 {
    public final TextView A00;

    public C60132vz(View view, C18720su r3, C89374Js r4, CallGridViewModel callGridViewModel, AnonymousClass130 r6, C15610nY r7) {
        super(view, r3, r4, callGridViewModel, r6, r7);
        ((AbstractC55202hx) this).A01 = 0;
        this.A00 = C12960it.A0I(view, R.id.audio_call_status);
    }

    @Override // X.C60142w0, X.AbstractC55202hx
    public void A0G(C64363Fg r1) {
        A0M(r1);
        super.A0G(r1);
    }

    public void A0M(C64363Fg r4) {
        if (r4.A05 != -1) {
            TextView textView = this.A00;
            String string = textView.getContext().getString(r4.A05);
            textView.setVisibility(0);
            textView.setText(string);
            return;
        }
        this.A00.setVisibility(8);
    }
}
