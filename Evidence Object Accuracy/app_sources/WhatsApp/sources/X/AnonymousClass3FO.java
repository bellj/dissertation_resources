package X;

import android.content.ContentValues;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

/* renamed from: X.3FO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3FO {
    public ContentValues A00 = new ContentValues();
    public String A01 = "";
    public String A02 = "";
    public List A03 = C12960it.A0l();
    public Set A04 = C12970iu.A12();
    public Set A05 = C12970iu.A12();
    public byte[] A06;

    public boolean equals(Object obj) {
        if (!(obj instanceof AnonymousClass3FO)) {
            return false;
        }
        AnonymousClass3FO r6 = (AnonymousClass3FO) obj;
        String str = this.A01;
        if (str == null || !str.equals(r6.A01) || !this.A00.equals(r6.A00) || !this.A04.equals(r6.A04) || !this.A05.equals(r6.A05)) {
            return false;
        }
        byte[] bArr = this.A06;
        if (bArr != null && Arrays.equals(bArr, r6.A06)) {
            return true;
        }
        if (!this.A02.equals(r6.A02)) {
            return false;
        }
        List list = this.A03;
        List list2 = r6.A03;
        if (list.equals(list2) || list.size() == 1 || list2.size() == 1) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        byte[] bArr = this.A06;
        Object[] objArr = new Object[5];
        objArr[0] = this.A01;
        objArr[1] = this.A00;
        objArr[2] = this.A04;
        objArr[3] = this.A05;
        if (bArr == null) {
            return C12980iv.A0B(this.A02, objArr, 4);
        }
        objArr[4] = bArr;
        return Arrays.deepHashCode(objArr);
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("propName: ");
        A0k.append(this.A01);
        A0k.append(", paramMap: ");
        C12970iu.A1V(this.A00, A0k);
        A0k.append(", propmMap_TYPE: ");
        C12970iu.A1V(this.A04, A0k);
        A0k.append(", propGroupSet: ");
        C12970iu.A1V(this.A05, A0k);
        List list = this.A03;
        if (list.size() > 1) {
            A0k.append(", propValue_vector size: ");
            A0k.append(list.size());
        }
        byte[] bArr = this.A06;
        if (bArr != null) {
            A0k.append(", propValue_bytes size: ");
            A0k.append(bArr.length);
        }
        A0k.append(", propValue: ");
        return C12960it.A0d(this.A02, A0k);
    }
}
