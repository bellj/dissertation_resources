package X;

import android.os.Bundle;
import android.view.View;

/* renamed from: X.2eg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53582eg extends AnonymousClass04v {
    public final /* synthetic */ DialogC53322dr A00;

    public C53582eg(DialogC53322dr r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass04v
    public boolean A03(View view, int i, Bundle bundle) {
        if (i == 1048576) {
            DialogC53322dr r1 = this.A00;
            if (r1.A02) {
                r1.cancel();
                return true;
            }
        }
        return super.A03(view, i, bundle);
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r4) {
        boolean z;
        super.A06(view, r4);
        if (this.A00.A02) {
            r4.A02.addAction(1048576);
            z = true;
        } else {
            z = false;
        }
        r4.A0K(z);
    }
}
