package X;

import java.util.Comparator;

/* renamed from: X.0eS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C10340eS implements Comparator {
    @Override // java.util.Comparator
    public int compare(Object obj, Object obj2) {
        AnonymousClass03S r5 = (AnonymousClass03S) obj;
        AnonymousClass03S r6 = (AnonymousClass03S) obj2;
        int i = r5.A03;
        int i2 = r6.A03;
        float f = r5.A02;
        float f2 = r6.A02;
        if (i != i2) {
            return i - i2;
        }
        if (f != f2) {
            return (int) Math.signum(f - f2);
        }
        return r5.A06 - r6.A06;
    }
}
