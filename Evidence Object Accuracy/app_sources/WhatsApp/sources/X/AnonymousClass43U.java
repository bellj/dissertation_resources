package X;

/* renamed from: X.43U  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass43U extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Boolean A02;

    public AnonymousClass43U() {
        super(3102, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 2, 113760892);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
        r3.Abe(2, this.A01);
        r3.Abe(3, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamCtwaLoggingRefactorV2CustomerPerformance {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "v1AndV2Match", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "v1DataAvailable", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "v2DataAvailable", this.A02);
        return C12960it.A0d("}", A0k);
    }
}
