package X;

/* renamed from: X.43T  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass43T extends AbstractC16110oT {
    public Integer A00;
    public String A01;
    public String A02;

    public AnonymousClass43T() {
        super(470, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
        r3.Abe(7, this.A01);
        r3.Abe(21, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamContactUsSession {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "contactUsExitState", C12960it.A0Y(this.A00));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "contactUsProblemDescription", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "languageCode", this.A02);
        return C12960it.A0d("}", A0k);
    }
}
