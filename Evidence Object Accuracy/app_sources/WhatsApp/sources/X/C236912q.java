package X;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioDeviceCallback;
import android.media.AudioDeviceInfo;
import android.media.AudioManager;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.Voip;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.12q  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C236912q extends BroadcastReceiver implements BluetoothProfile.ServiceListener {
    public int A00 = -1;
    public BluetoothAdapter A01;
    public BluetoothHeadset A02;
    public AudioDeviceCallback A03;
    public WeakReference A04;
    public final Context A05;
    public final AnonymousClass01d A06;
    public final Set A07 = Collections.newSetFromMap(new ConcurrentHashMap());

    public static String A00(int i) {
        return i != 0 ? i != 1 ? i != 2 ? i != 3 ? "UNKNOWN BLUETOOTH CONNECTION STATE" : "DISCONNECTING" : "CONNECTED" : "CONNECTING" : "DISCONNECTED";
    }

    public C236912q(Context context, AnonymousClass01d r3) {
        this.A05 = context;
        this.A06 = r3;
    }

    public static List A01(AudioManager audioManager) {
        List<AudioDeviceInfo> A02 = A02(audioManager);
        ArrayList arrayList = new ArrayList();
        for (AudioDeviceInfo audioDeviceInfo : A02) {
            arrayList.add(audioDeviceInfo.getAddress());
        }
        return arrayList;
    }

    public static List A02(AudioManager audioManager) {
        ArrayList arrayList = new ArrayList();
        if (audioManager != null) {
            AudioDeviceInfo[] devices = audioManager.getDevices(2);
            for (AudioDeviceInfo audioDeviceInfo : devices) {
                if (A03(audioDeviceInfo)) {
                    arrayList.add(audioDeviceInfo);
                }
            }
        }
        return arrayList;
    }

    public static boolean A03(AudioDeviceInfo audioDeviceInfo) {
        int type = audioDeviceInfo.getType();
        return type == 7 || type == 26;
    }

    public final void A04() {
        this.A03 = new C73153fl(this);
    }

    public final void A05(int i) {
        if (this.A00 != i) {
            this.A00 = i;
            for (AbstractC35201hQ r0 : this.A07) {
                r0.ANJ(i);
            }
        }
    }

    public void A06(AbstractC35201hQ r5) {
        Set set = this.A07;
        if (set.isEmpty()) {
            if (C28391Mz.A05()) {
                AudioManager A0G = this.A06.A0G();
                if (A0G != null) {
                    A04();
                    AudioDeviceCallback audioDeviceCallback = this.A03;
                    AnonymousClass009.A05(audioDeviceCallback);
                    A0G.registerAudioDeviceCallback(audioDeviceCallback, null);
                }
            } else {
                this.A05.registerReceiver(this, new IntentFilter("android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"));
            }
        }
        set.add(r5);
    }

    public void A07(AbstractC35201hQ r3) {
        Set set = this.A07;
        if (set.remove(r3) && set.isEmpty()) {
            if (C28391Mz.A05()) {
                AudioManager A0G = this.A06.A0G();
                if (A0G != null) {
                    AudioDeviceCallback audioDeviceCallback = this.A03;
                    AnonymousClass009.A05(audioDeviceCallback);
                    A0G.unregisterAudioDeviceCallback(audioDeviceCallback);
                    return;
                }
                return;
            }
            this.A05.unregisterReceiver(this);
        }
    }

    public boolean A08() {
        AudioManager A0G;
        try {
            BluetoothAdapter bluetoothAdapter = this.A01;
            if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled() || (A0G = this.A06.A0G()) == null) {
                return false;
            }
            if (!C28391Mz.A05()) {
                BluetoothHeadset bluetoothHeadset = this.A02;
                if (bluetoothHeadset == null || bluetoothHeadset.getConnectedDevices().isEmpty()) {
                    return false;
                }
            } else if (A02(A0G).isEmpty()) {
                return false;
            }
            return true;
        } catch (Exception e) {
            Log.e(e);
            return false;
        }
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if ("android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED".equals(intent.getAction())) {
            int intExtra = intent.getIntExtra("android.bluetooth.profile.extra.STATE", 0);
            int intExtra2 = intent.getIntExtra("android.bluetooth.profile.extra.PREVIOUS_STATE", 0);
            StringBuilder sb = new StringBuilder("BluetoothHeadsetMonitor/bluetoothConnectionReceiver [");
            sb.append(A00(intExtra2));
            sb.append(" -> ");
            sb.append(A00(intExtra));
            sb.append("]");
            Log.i(sb.toString());
            if (intExtra != intExtra2) {
                A05(intExtra);
            }
        }
    }

    @Override // android.bluetooth.BluetoothProfile.ServiceListener
    public void onServiceConnected(int i, BluetoothProfile bluetoothProfile) {
        AnonymousClass2Nu r3;
        List<BluetoothDevice> connectedDevices;
        if (i == 1) {
            BluetoothHeadset bluetoothHeadset = (BluetoothHeadset) bluetoothProfile;
            this.A02 = bluetoothHeadset;
            WeakReference weakReference = this.A04;
            if (weakReference != null) {
                r3 = (AnonymousClass2Nu) weakReference.get();
                if (r3 != null && !r3.A03) {
                    if (bluetoothHeadset != null) {
                        AudioManager A0G = this.A06.A0G();
                        StringBuilder sb = new StringBuilder("BluetoothHeadsetMonitor/onServiceConnected ");
                        sb.append(this.A02);
                        sb.append(", devices: ");
                        if (C28391Mz.A05()) {
                            connectedDevices = A01(A0G);
                        } else {
                            connectedDevices = this.A02.getConnectedDevices();
                        }
                        sb.append(connectedDevices);
                        sb.append(", ");
                        sb.append(r3);
                        Log.i(sb.toString());
                        r3.A05(Voip.getCallInfo());
                        return;
                    }
                    return;
                }
            } else {
                r3 = null;
            }
            StringBuilder sb2 = new StringBuilder("BluetoothHeadsetMonitor/onServiceConnected VoipInterface already Destroyed ");
            sb2.append(r3);
            Log.w(sb2.toString());
        }
    }

    @Override // android.bluetooth.BluetoothProfile.ServiceListener
    public void onServiceDisconnected(int i) {
        AnonymousClass2Nu r2;
        if (i == 1) {
            WeakReference weakReference = this.A04;
            if (weakReference != null) {
                r2 = (AnonymousClass2Nu) weakReference.get();
            } else {
                r2 = null;
            }
            StringBuilder sb = new StringBuilder("BluetoothHeadsetMonitor/onServiceDisconnected ");
            sb.append(this.A02);
            sb.append(", ");
            sb.append(r2);
            Log.i(sb.toString());
            this.A02 = null;
            if (r2 == null || r2.A03) {
                StringBuilder sb2 = new StringBuilder("BluetoothHeadsetMonitor/onServiceDisconnected VoipInterface already Destroyed ");
                sb2.append(r2);
                Log.w(sb2.toString());
                return;
            }
            r2.A09(Voip.getCallInfo(), false);
        }
    }
}
