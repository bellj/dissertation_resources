package X;

import android.media.MediaCodec;
import android.os.Bundle;
import android.os.Handler;
import android.view.Surface;
import java.nio.ByteBuffer;

/* renamed from: X.4x8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107314x8 implements AnonymousClass5XG {
    public ByteBuffer[] A00;
    public ByteBuffer[] A01;
    public final MediaCodec A02;

    public /* synthetic */ C107314x8(MediaCodec mediaCodec) {
        this.A02 = mediaCodec;
    }

    @Override // X.AnonymousClass5XG
    public ByteBuffer ADV(int i) {
        if (AnonymousClass3JZ.A01 >= 21) {
            return this.A02.getInputBuffer(i);
        }
        return this.A00[i];
    }

    @Override // X.AnonymousClass5XG
    public ByteBuffer AEo(int i) {
        if (AnonymousClass3JZ.A01 >= 21) {
            return this.A02.getOutputBuffer(i);
        }
        return this.A01[i];
    }

    @Override // X.AnonymousClass5XG
    public void Aa7(int i, long j) {
        this.A02.releaseOutputBuffer(i, j);
    }

    @Override // X.AnonymousClass5XG
    public void AcN(Handler handler, AnonymousClass5SL r4) {
        this.A02.setOnFrameRenderedListener(new MediaCodec.OnFrameRenderedListener(r4, this) { // from class: X.4i0
            public final /* synthetic */ AnonymousClass5SL A00;
            public final /* synthetic */ C107314x8 A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // android.media.MediaCodec.OnFrameRenderedListener
            public final void onFrameRendered(MediaCodec mediaCodec, long j, long j2) {
                this.A00.AQp(this.A01, j, j2);
            }
        }, handler);
    }

    @Override // X.AnonymousClass5XG
    public void AcR(Surface surface) {
        this.A02.setOutputSurface(surface);
    }

    @Override // X.AnonymousClass5XG
    public void AcS(Bundle bundle) {
        this.A02.setParameters(bundle);
    }
}
