package X;

/* renamed from: X.1gQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C34611gQ extends AnonymousClass1JQ implements AbstractC34381g3, AbstractC34531gI {
    public final long A00;
    public final AbstractC14640lm A01;
    public final boolean A02;

    public C34611gQ(AnonymousClass1JR r11, AbstractC14640lm r12, String str, long j, long j2, boolean z, boolean z2) {
        super(C27791Jf.A03, r11, str, "regular_high", 2, j2, z2);
        this.A01 = r12;
        this.A02 = z;
        this.A00 = j;
    }

    @Override // X.AnonymousClass1JQ
    public AnonymousClass271 A01() {
        AnonymousClass1G4 A0T = C34601gP.A03.A0T();
        boolean z = this.A02;
        A0T.A03();
        C34601gP r1 = (C34601gP) A0T.A00;
        r1.A00 |= 1;
        r1.A02 = z;
        if (z) {
            long j = this.A00;
            A0T.A03();
            C34601gP r12 = (C34601gP) A0T.A00;
            r12.A00 |= 2;
            r12.A01 = j;
        }
        AnonymousClass271 A01 = super.A01();
        AnonymousClass009.A05(A01);
        A01.A03();
        C27831Jk r13 = (C27831Jk) A01.A00;
        r13.A0E = (C34601gP) A0T.A02();
        r13.A00 |= 8;
        return A01;
    }

    @Override // X.AbstractC34381g3
    public AbstractC14640lm ABH() {
        return this.A01;
    }

    @Override // X.AbstractC34531gI
    public boolean AKD() {
        return !this.A02;
    }

    @Override // X.AnonymousClass1JQ
    public String toString() {
        StringBuilder sb = new StringBuilder("MuteChatMutation{rowId=");
        sb.append(this.A07);
        sb.append(", chatJid=");
        sb.append(this.A01);
        sb.append(", muteEndTimestamp=");
        sb.append(this.A00);
        sb.append(", isMuted=");
        sb.append(this.A02);
        sb.append(", timestamp=");
        sb.append(this.A04);
        sb.append(", areDependenciesMissing=");
        sb.append(A05());
        sb.append(", operation=");
        sb.append(this.A05);
        sb.append(", collectionName=");
        sb.append(this.A06);
        sb.append(", keyId=");
        sb.append(super.A00);
        sb.append('}');
        return sb.toString();
    }
}
