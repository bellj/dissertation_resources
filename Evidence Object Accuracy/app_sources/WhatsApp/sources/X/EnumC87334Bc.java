package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4Bc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public enum EnumC87334Bc implements Parcelable {
    LONG_PRESS,
    TRIGGERED;
    
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(53);

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(ordinal());
    }
}
