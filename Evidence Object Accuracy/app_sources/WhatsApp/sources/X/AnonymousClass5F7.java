package X;

/* renamed from: X.5F7  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5F7 implements AnonymousClass5VL {
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0068, code lost:
        if (r1 == '.') goto L_0x006a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x006a, code lost:
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x006c, code lost:
        if (r2 >= r6) goto L_0x0076;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x006e, code lost:
        r1 = r10.charAt(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0072, code lost:
        if (r1 < '0') goto L_0x0076;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0074, code lost:
        if (r1 <= '9') goto L_0x006a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0076, code lost:
        if (r2 == r6) goto L_0x00a5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x007a, code lost:
        if (r1 == 'E') goto L_0x0080;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x007e, code lost:
        if (r1 != 'e') goto L_0x0093;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0080, code lost:
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0082, code lost:
        if (r2 == r6) goto L_0x00a6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0084, code lost:
        r1 = r10.charAt(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x008a, code lost:
        if (r1 == '+') goto L_0x008e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x008c, code lost:
        if (r1 != '-') goto L_0x0093;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x008e, code lost:
        r2 = r2 + 1;
        r10.charAt(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0093, code lost:
        if (r2 != r6) goto L_0x0096;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0095, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0096, code lost:
        if (r2 >= r6) goto L_0x00a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0098, code lost:
        r0 = r10.charAt(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x009c, code lost:
        if (r0 < '0') goto L_0x00a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x009e, code lost:
        if (r0 > '9') goto L_0x00a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x00a0, code lost:
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00a3, code lost:
        if (r2 != r6) goto L_0x00a6;
     */
    @Override // X.AnonymousClass5VL
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean ALX(java.lang.String r10) {
        /*
            r9 = this;
            r7 = 0
            if (r10 == 0) goto L_0x00a6
            int r6 = r10.length()
            r8 = 1
            if (r6 == 0) goto L_0x00a5
            java.lang.String r0 = r10.trim()
            if (r0 != r10) goto L_0x00a5
            char r1 = r10.charAt(r7)
            boolean r0 = X.C95224dL.A00(r1)
            if (r0 != 0) goto L_0x00a5
            boolean r0 = X.C95224dL.A01(r1)
            if (r0 != 0) goto L_0x00a5
            r2 = 1
        L_0x0021:
            if (r2 >= r6) goto L_0x0040
            char r1 = r10.charAt(r2)
            r0 = 125(0x7d, float:1.75E-43)
            if (r1 == r0) goto L_0x00a5
            r0 = 93
            if (r1 == r0) goto L_0x00a5
            r0 = 44
            if (r1 == r0) goto L_0x00a5
            r0 = 58
            if (r1 == r0) goto L_0x00a5
            boolean r0 = X.C95224dL.A01(r1)
            if (r0 != 0) goto L_0x00a5
            int r2 = r2 + 1
            goto L_0x0021
        L_0x0040:
            boolean r0 = X.C95224dL.A02(r10)
            if (r0 != 0) goto L_0x00a5
            char r1 = r10.charAt(r7)
            r5 = 45
            r4 = 57
            r3 = 48
            if (r1 < r3) goto L_0x0054
            if (r1 <= r4) goto L_0x0056
        L_0x0054:
            if (r1 != r5) goto L_0x00a6
        L_0x0056:
            r2 = 1
        L_0x0057:
            if (r2 >= r6) goto L_0x0064
            char r1 = r10.charAt(r2)
            if (r1 < r3) goto L_0x0064
            if (r1 > r4) goto L_0x0064
            int r2 = r2 + 1
            goto L_0x0057
        L_0x0064:
            if (r2 == r6) goto L_0x00a5
            r0 = 46
            if (r1 != r0) goto L_0x006c
        L_0x006a:
            int r2 = r2 + 1
        L_0x006c:
            if (r2 >= r6) goto L_0x0076
            char r1 = r10.charAt(r2)
            if (r1 < r3) goto L_0x0076
            if (r1 <= r4) goto L_0x006a
        L_0x0076:
            if (r2 == r6) goto L_0x00a5
            r0 = 69
            if (r1 == r0) goto L_0x0080
            r0 = 101(0x65, float:1.42E-43)
            if (r1 != r0) goto L_0x0093
        L_0x0080:
            int r2 = r2 + 1
            if (r2 == r6) goto L_0x00a6
            char r1 = r10.charAt(r2)
            r0 = 43
            if (r1 == r0) goto L_0x008e
            if (r1 != r5) goto L_0x0093
        L_0x008e:
            int r2 = r2 + 1
            r10.charAt(r2)
        L_0x0093:
            if (r2 != r6) goto L_0x0096
            return r7
        L_0x0096:
            if (r2 >= r6) goto L_0x00a3
            char r0 = r10.charAt(r2)
            if (r0 < r3) goto L_0x00a3
            if (r0 > r4) goto L_0x00a3
            int r2 = r2 + 1
            goto L_0x0096
        L_0x00a3:
            if (r2 != r6) goto L_0x00a6
        L_0x00a5:
            return r8
        L_0x00a6:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5F7.ALX(java.lang.String):boolean");
    }
}
