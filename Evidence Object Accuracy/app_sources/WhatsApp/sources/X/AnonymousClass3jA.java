package X;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.conversation.selectlist.SelectListBottomSheet;

/* renamed from: X.3jA  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass3jA extends AbstractC05270Ox {
    public final /* synthetic */ SelectListBottomSheet A00;

    public AnonymousClass3jA(SelectListBottomSheet selectListBottomSheet) {
        this.A00 = selectListBottomSheet;
    }

    @Override // X.AbstractC05270Ox
    public void A01(RecyclerView recyclerView, int i, int i2) {
        SelectListBottomSheet selectListBottomSheet = this.A00;
        if (((AnonymousClass01E) selectListBottomSheet).A0A != null) {
            boolean canScrollVertically = recyclerView.canScrollVertically(-1);
            View findViewById = ((AnonymousClass01E) selectListBottomSheet).A0A.findViewById(R.id.shadow_top);
            int i3 = 4;
            if (canScrollVertically) {
                i3 = 0;
            }
            findViewById.setVisibility(i3);
        }
        if (((AnonymousClass01E) selectListBottomSheet).A0A != null) {
            boolean canScrollVertically2 = recyclerView.canScrollVertically(1);
            View findViewById2 = ((AnonymousClass01E) selectListBottomSheet).A0A.findViewById(R.id.shadow_bottom);
            int i4 = 4;
            if (canScrollVertically2) {
                i4 = 0;
            }
            findViewById2.setVisibility(i4);
        }
    }
}
