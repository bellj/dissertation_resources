package X;

/* renamed from: X.5bX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118265bX extends AnonymousClass0Yo {
    public final /* synthetic */ C128375w0 A00;

    public C118265bX(C128375w0 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass0Yo, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.isAssignableFrom(C123385n4.class)) {
            C128375w0 r0 = this.A00;
            return new C123385n4(r0.A0B, r0.A0c, r0.A0d, r0.A0k);
        }
        throw C12970iu.A0f("Invalid viewModel for NoviPayHubManageTopUpCardViewModel");
    }
}
