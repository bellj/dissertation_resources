package X;

import com.whatsapp.jid.Jid;

/* renamed from: X.3EL  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3EL {
    public final Jid A00;
    public final AnonymousClass1V8 A01;
    public final String A02;

    public AnonymousClass3EL(AbstractC15710nm r24, AnonymousClass1V8 r25, AnonymousClass1V8 r26) {
        AnonymousClass1V8.A01(r25, "iq");
        Long A0j = C12970iu.A0j();
        Long A0k = C12970iu.A0k();
        AnonymousClass3JT.A04(null, r25, String.class, A0j, A0k, "error", new String[]{"type"}, false);
        this.A02 = (String) AnonymousClass3JT.A04(null, r25, String.class, A0j, A0k, AnonymousClass3JT.A04(null, r26, String.class, A0j, A0k, null, new String[]{"id"}, false), new String[]{"id"}, true);
        this.A00 = (Jid) AnonymousClass3JT.A04(r24, r25, Jid.class, A0j, A0k, AnonymousClass3JT.A04(r24, r26, Jid.class, A0j, A0k, null, new String[]{"to"}, false), new String[]{"from"}, true);
        this.A01 = r25;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || AnonymousClass3EL.class != obj.getClass()) {
                return false;
            }
            AnonymousClass3EL r5 = (AnonymousClass3EL) obj;
            if (!this.A02.equals(r5.A02) || !this.A00.equals(r5.A00)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] A1a = C12980iv.A1a();
        A1a[0] = this.A02;
        return C12960it.A06(this.A00, A1a);
    }
}
