package X;

/* renamed from: X.0kM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC13800kM extends ActivityC13810kN {
    public boolean A00 = false;

    public AbstractActivityC13800kM() {
        A0m();
    }

    private void A0m() {
        A0R(new AnonymousClass2GU(this));
    }

    @Override // X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            ActivityC13790kL r1 = (ActivityC13790kL) this;
            AnonymousClass2FL r3 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r2 = r3.A1E;
            ((ActivityC13830kP) r1).A05 = (AbstractC14440lR) r2.ANe.get();
            ((ActivityC13810kN) r1).A0C = (C14850m9) r2.A04.get();
            ((ActivityC13810kN) r1).A05 = (C14900mE) r2.A8X.get();
            ((ActivityC13810kN) r1).A03 = (AbstractC15710nm) r2.A4o.get();
            ((ActivityC13810kN) r1).A04 = (C14330lG) r2.A7B.get();
            ((ActivityC13810kN) r1).A0B = (AnonymousClass19M) r2.A6R.get();
            ((ActivityC13810kN) r1).A0A = (C18470sV) r2.AK8.get();
            ((ActivityC13810kN) r1).A06 = (C15450nH) r2.AII.get();
            ((ActivityC13810kN) r1).A08 = (AnonymousClass01d) r2.ALI.get();
            ((ActivityC13810kN) r1).A0D = (C18810t5) r2.AMu.get();
            ((ActivityC13810kN) r1).A09 = (C14820m6) r2.AN3.get();
            ((ActivityC13810kN) r1).A07 = (C18640sm) r2.A3u.get();
            r1.A05 = (C14830m7) r2.ALb.get();
            r1.A0D = (C252718t) r2.A9K.get();
            r1.A01 = (C15570nT) r2.AAr.get();
            r1.A04 = (C15810nw) r2.A73.get();
            r1.A09 = r3.A06();
            r1.A06 = (C14950mJ) r2.AKf.get();
            r1.A00 = (AnonymousClass12P) r2.A0H.get();
            r1.A02 = (C252818u) r2.AMy.get();
            r1.A03 = (C22670zS) r2.A0V.get();
            r1.A0A = (C21840y4) r2.ACr.get();
            r1.A07 = (C15880o3) r2.ACF.get();
            r1.A0C = (C21820y2) r2.AHx.get();
            r1.A0B = (C15510nN) r2.AHZ.get();
            r1.A08 = (C249317l) r2.A8B.get();
        }
    }
}
