package X;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;

/* renamed from: X.2og  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58352og extends AbstractC75803kU {
    public final Context A00;
    public final AbstractC001200n A01;
    public final C15570nT A02;
    public final AnonymousClass130 A03;
    public final C15550nR A04;
    public final C15610nY A05;
    public final AnonymousClass131 A06;
    public final AnonymousClass018 A07;
    public final AnonymousClass2VG A08;

    public C58352og(Context context, AbstractC001200n r4, C15570nT r5, AnonymousClass130 r6, C15550nR r7, C15610nY r8, AnonymousClass131 r9, AnonymousClass018 r10, AnonymousClass2VG r11) {
        this.A02 = r5;
        this.A03 = r6;
        this.A04 = r7;
        this.A05 = r8;
        this.A07 = r10;
        this.A06 = r9;
        this.A00 = context;
        this.A01 = r4;
        this.A08 = r11;
        C12970iu.A1P(r4, r11.A05, this, 59);
    }

    @Override // X.AnonymousClass01A
    public int A01() {
        return C12980iv.A0z(this.A08.A05).size() + 1;
    }

    @Override // X.AnonymousClass01A
    public CharSequence A04(int i) {
        if (i == 0) {
            AnonymousClass018 r6 = this.A07;
            Context context = this.A00;
            int size = C12980iv.A0z(this.A08.A03.A02).size();
            Resources resources = context.getResources();
            Object[] A1b = C12970iu.A1b();
            A1b[0] = AnonymousClass3J7.A02(context, r6, size);
            return resources.getQuantityString(R.plurals.reactions_bottom_sheet_all_tab_title, size, A1b);
        }
        AnonymousClass3D7 r62 = (AnonymousClass3D7) C12980iv.A0z(this.A08.A05).get(i - 1);
        AnonymousClass018 r1 = this.A07;
        Context context2 = this.A00;
        String A02 = AnonymousClass3J7.A02(context2, r1, C12980iv.A0z(r62.A02).size());
        Object[] A1a = C12980iv.A1a();
        A1a[0] = r62.A03;
        return C12960it.A0X(context2, A02, A1a, 1, R.string.reactions_bottom_sheet_tab_title);
    }

    @Override // X.AbstractC75803kU
    public /* bridge */ /* synthetic */ int A0F(Object obj) {
        int i;
        AnonymousClass2VG r3 = this.A08;
        Object obj2 = ((AnonymousClass01T) obj).A01;
        AnonymousClass009.A05(obj2);
        AnonymousClass3D7 r2 = (AnonymousClass3D7) obj2;
        if (r2.A03.equals(r3.A03.A03)) {
            return 0;
        }
        int indexOf = C12980iv.A0z(r3.A05).indexOf(r2);
        if (indexOf < 0 || (i = indexOf + 1) == -1) {
            return -2;
        }
        return i;
    }

    @Override // X.AbstractC75803kU
    public /* bridge */ /* synthetic */ Object A0G(ViewGroup viewGroup, int i) {
        AnonymousClass3D7 r11;
        Context context = this.A00;
        RecyclerView recyclerView = new RecyclerView(context);
        recyclerView.setId(R.id.reactions_bottom_sheet_tab_recycler_view);
        recyclerView.setPadding(0, context.getResources().getDimensionPixelSize(R.dimen.space_base_halfStep), 0, 0);
        recyclerView.setClipToPadding(false);
        AnonymousClass2VG r12 = this.A08;
        if (i == 0) {
            r11 = r12.A03;
        } else {
            r11 = (AnonymousClass3D7) C12980iv.A0z(r12.A05).get(i - 1);
        }
        C12990iw.A1K(recyclerView);
        C15570nT r5 = this.A02;
        AnonymousClass130 r6 = this.A03;
        C15550nR r7 = this.A04;
        C15610nY r8 = this.A05;
        AnonymousClass018 r10 = this.A07;
        recyclerView.setAdapter(new AnonymousClass2VF(this.A01, r5, r6, r7, r8, this.A06, r10, r11, r12));
        viewGroup.addView(recyclerView);
        return new AnonymousClass01T(recyclerView, r11);
    }

    @Override // X.AbstractC75803kU
    public void A0I(ViewGroup viewGroup, Object obj, int i) {
        viewGroup.removeView((View) ((AnonymousClass01T) obj).A00);
    }

    @Override // X.AbstractC75803kU
    public boolean A0J(View view, Object obj) {
        return C12970iu.A1Z(view, ((AnonymousClass01T) obj).A00);
    }
}
