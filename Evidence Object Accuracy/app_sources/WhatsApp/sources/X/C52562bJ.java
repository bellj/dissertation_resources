package X;

import android.graphics.Outline;
import android.view.View;
import android.view.ViewOutlineProvider;
import com.whatsapp.R;
import com.whatsapp.voipcalling.VoipCallControlBottomSheetV2;

/* renamed from: X.2bJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52562bJ extends ViewOutlineProvider {
    public final /* synthetic */ VoipCallControlBottomSheetV2 A00;

    public C52562bJ(VoipCallControlBottomSheetV2 voipCallControlBottomSheetV2) {
        this.A00 = voipCallControlBottomSheetV2;
    }

    @Override // android.view.ViewOutlineProvider
    public void getOutline(View view, Outline outline) {
        float dimension = C12960it.A09(this.A00.A06).getDimension(R.dimen.call_control_bottom_sheet_rounded_corner_radius);
        outline.setRoundRect(0, 0, view.getWidth(), view.getHeight() + ((int) dimension), dimension);
    }
}
