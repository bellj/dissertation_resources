package X;

import java.util.regex.Pattern;

/* renamed from: X.4aQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C93594aQ {
    public static final AbstractC17940re A03 = AbstractC17940re.of((Object) "filled", (Object) "open");
    public static final AbstractC17940re A04 = AbstractC17940re.of((Object) "dot", (Object) "sesame", (Object) "circle");
    public static final AbstractC17940re A05 = AbstractC17940re.of((Object) "after", (Object) "before", (Object) "outside");
    public static final AbstractC17940re A06 = AbstractC17940re.of((Object) "auto", (Object) "none");
    public static final Pattern A07 = Pattern.compile("\\s+");
    public final int A00;
    public final int A01;
    public final int A02;

    public C93594aQ(int i, int i2, int i3) {
        this.A01 = i;
        this.A00 = i2;
        this.A02 = i3;
    }
}
