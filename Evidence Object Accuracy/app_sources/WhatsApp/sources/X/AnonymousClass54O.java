package X;

import com.facebook.redex.IDxCListenerShape8S0100000_1_I1;
import com.whatsapp.businessdirectory.view.activity.DirectorySetLocationMapActivity;

/* renamed from: X.54O  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass54O implements AbstractC116575Vz {
    public final /* synthetic */ DirectorySetLocationMapActivity A00;

    public AnonymousClass54O(DirectorySetLocationMapActivity directorySetLocationMapActivity) {
        this.A00 = directorySetLocationMapActivity;
    }

    @Override // X.AbstractC116575Vz
    public void AQs(int i) {
        this.A00.A2h(new IDxCListenerShape8S0100000_1_I1(this, 10), i);
    }

    @Override // X.AbstractC116575Vz
    public void AQt(String str) {
        DirectorySetLocationMapActivity directorySetLocationMapActivity = this.A00;
        directorySetLocationMapActivity.A08.A02(str);
        directorySetLocationMapActivity.A2e();
    }
}
