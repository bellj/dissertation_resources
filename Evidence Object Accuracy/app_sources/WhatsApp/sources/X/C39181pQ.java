package X;

import java.io.File;

/* renamed from: X.1pQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C39181pQ extends AbstractC39111pJ {
    public final File A00;
    public final boolean A01;

    public C39181pQ(C39741qT r8, AbstractC14470lU r9, C39781qX r10, C39771qW r11, AbstractC39761qV r12, File file, File file2, boolean z) {
        super(r8, r9, r10, r11, r12, file2);
        this.A00 = file;
        this.A01 = z;
    }
}
