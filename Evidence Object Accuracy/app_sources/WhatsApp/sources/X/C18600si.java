package X;

import android.content.SharedPreferences;
import android.text.TextUtils;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.0si  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18600si {
    public SharedPreferences A00;
    public final C14830m7 A01;
    public final C30931Zj A02 = C30931Zj.A00("PaymentSharedPrefs", "infra", "COMMON");
    public final C16630pM A03;

    public C18600si(C14830m7 r4, C16630pM r5) {
        this.A01 = r4;
        this.A03 = r5;
    }

    public static final String A00(Map map) {
        JSONObject jSONObject = new JSONObject();
        try {
            for (Map.Entry entry : map.entrySet()) {
                jSONObject.put(((Jid) entry.getKey()).getRawString(), entry.getValue());
            }
        } catch (JSONException e) {
            StringBuilder sb = new StringBuilder("PAY: PaymentSharedPrefs/getRawFromJidsWithExpiration/exception: ");
            sb.append(e);
            Log.e(sb.toString());
        }
        return jSONObject.toString();
    }

    public final synchronized SharedPreferences A01() {
        SharedPreferences sharedPreferences;
        sharedPreferences = this.A00;
        if (sharedPreferences == null) {
            sharedPreferences = this.A03.A02("com.whatsapp_payment_preferences");
            this.A00 = sharedPreferences;
        }
        return sharedPreferences;
    }

    public C32631cT A02() {
        String string = A01().getString("payment_step_up_info", null);
        if (string != null) {
            return C32631cT.A01(string);
        }
        return null;
    }

    public Boolean A03() {
        if (A01().contains("payment_is_first_send")) {
            return Boolean.valueOf(A01().getBoolean("payment_is_first_send", false));
        }
        return null;
    }

    public String A04() {
        return A01().getString("payments_setup_country_specific_info", "");
    }

    public String A05() {
        return A01().getString("payments_sent_payment_with_account", "");
    }

    public String A06() {
        String string = A01().getString("payment_trusted_device_elo_wallet_store", null);
        try {
            return (string != null ? new JSONObject(string) : new JSONObject()).getString("wallet_id");
        } catch (JSONException unused) {
            this.A02.A06("Failed to get the wallet_id");
            return null;
        }
    }

    public final Map A07(String str) {
        HashMap hashMap = new HashMap();
        if (!TextUtils.isEmpty(str)) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                long A00 = this.A01.A00();
                Iterator<String> keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String next = keys.next();
                    UserJid nullable = UserJid.getNullable(next);
                    long j = jSONObject.getLong(next);
                    if (nullable != null && j >= A00) {
                        hashMap.put(nullable, Long.valueOf(j));
                    }
                }
            } catch (JSONException e) {
                StringBuilder sb = new StringBuilder("PAY: PaymentSharedPrefs/getJidsWithExpirationFromRaw/exception: ");
                sb.append(e);
                Log.e(sb.toString());
            }
        }
        return hashMap;
    }

    public void A08() {
        A01().edit().remove("payment_step_up_info").apply();
    }

    public void A09() {
        A01().edit().putBoolean("payments_resume_onboarding_banner_started", true).apply();
    }

    public void A0A(int i) {
        A01().edit().putInt("payments_upi_transactions_sync_status", i).apply();
    }

    public void A0B(int i, String str) {
        String str2;
        SharedPreferences.Editor edit = A01().edit();
        if ("personal".equals(str)) {
            str2 = "payment_dyi_report_state";
        } else {
            str2 = "business_payment_dyi_report_state";
        }
        edit.putInt(str2, i).apply();
    }

    public void A0C(long j) {
        A01().edit().putLong("payments_upi_last_transactions_sync_time", j).apply();
    }

    public void A0D(long j) {
        A01().edit().putLong("payments_enabled_till", j).apply();
    }

    public void A0E(String str) {
        SharedPreferences.Editor remove;
        String str2;
        SharedPreferences.Editor edit = A01().edit();
        if ("personal".equals(str)) {
            remove = edit.remove("payment_dyi_report_state").remove("payment_dyi_report_timestamp");
            str2 = "payment_dyi_report_expiration_timestamp";
        } else {
            remove = edit.remove("business_payment_dyi_report_state").remove("business_payment_dyi_report_timestamp");
            str2 = "business_payment_dyi_report_expiration_timestamp";
        }
        remove.remove(str2).apply();
    }

    public void A0F(String str) {
        A01().edit().putString("payments_setup_country_specific_info", str).apply();
    }

    public void A0G(String str) {
        A01().edit().putString("payments_block_list", str).apply();
    }

    public void A0H(String str) {
        A01().edit().putString("payments_sent_payment_with_account", str).apply();
    }

    public void A0I(String str) {
        A01().edit().putString("payments_support_phone_number", str).apply();
    }

    public void A0J(String str, long j) {
        String str2;
        SharedPreferences.Editor edit = A01().edit();
        if ("personal".equals(str)) {
            str2 = "payment_dyi_report_timestamp";
        } else {
            str2 = "business_payment_dyi_report_timestamp";
        }
        edit.putLong(str2, j).apply();
    }

    public void A0K(boolean z) {
        SharedPreferences.Editor remove;
        String str;
        SharedPreferences.Editor edit = A01().edit();
        if (z) {
            remove = edit.remove("payments_setup_completed_steps").remove("payments_merchant_setup_completed_steps").remove("payments_methods_last_sync_time").remove("payments_card_can_receive_payment").remove("payments_all_transactions_last_sync_time").remove("payments_pending_transactions_last_sync_time").remove("payments_nagged_transactions").remove("payments_sent_payment_with_account").remove("payments_sandbox").remove("payments_invitee_jids").remove("payments_inviter_jids").remove("payments_enabled_till").remove("payments_support_phone_number").remove("payments_device_id").remove("payments_network_id_map").remove("payment_trusted_device_credential").remove("payment_trusted_device_credential_encrypted_aes").remove("payments_trusted_device_credential_network_map").remove("payment_kyc_info").remove("payment_step_up_info").remove("payment_dyi_report_expiration_timestamp").remove("payment_dyi_report_timestamp").remove("payment_dyi_report_state").remove("payments_invitee_jids_with_expiry").remove("payments_inviter_jids_with_expiry").remove("payment_usync_triggered").remove("payments_has_willow_account").remove("payment_incentive_offer_details").remove("payment_incentive_user_claim_info").remove("payment_incentive_tooltip_viewed").remove("payments_last_two_factor_nudge_time").remove("payments_two_factor_nudge_count").remove("payments_upi_pin_primer_dialog_shown").remove("payment_trusted_device_elo_wallet_store").remove("payment_account_recovered").remove("payments_home_account_recovery_banner_dismissed").remove("payments_upi_transactions_sync_status").remove("payments_upi_last_transactions_sync_time");
            str = "payments_resume_onboarding_banner_started";
        } else {
            remove = edit.remove("payments_merchant_setup_completed_steps").remove("payment_smb_upsell_view_count").remove("business_payment_dyi_report_expiration_timestamp").remove("business_payment_dyi_report_timestamp");
            str = "business_payment_dyi_report_state";
        }
        remove.remove(str).apply();
    }

    public void A0L(boolean z) {
        A01().edit().putBoolean("payments_sandbox", z).apply();
    }

    public boolean A0M() {
        return this.A01.A00() - A01().getLong("payments_methods_last_sync_time", 0) > TimeUnit.HOURS.toMillis(1);
    }
}
