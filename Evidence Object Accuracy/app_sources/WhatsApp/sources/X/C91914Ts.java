package X;

import android.util.SparseArray;

/* renamed from: X.4Ts  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C91914Ts {
    public AnonymousClass4T5 A00;
    public AnonymousClass4P9 A01;
    public final int A02;
    public final int A03;
    public final SparseArray A04 = new SparseArray();
    public final SparseArray A05 = new SparseArray();
    public final SparseArray A06 = new SparseArray();
    public final SparseArray A07 = new SparseArray();
    public final SparseArray A08 = new SparseArray();

    public C91914Ts(int i, int i2) {
        this.A03 = i;
        this.A02 = i2;
    }
}
