package X;

import android.text.TextUtils;
import com.whatsapp.util.Log;

/* renamed from: X.1Zj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30931Zj {
    public String A00;
    public String A01;
    public String A02;
    public final C31011Zr A03;

    public C30931Zj(C31011Zr r1, String str, String str2, String str3) {
        this.A03 = r1;
        this.A00 = str;
        this.A02 = str2;
        this.A01 = str3;
    }

    public static C30931Zj A00(String str, String str2, String str3) {
        return new C30931Zj(new C31011Zr(), str, str2, str3);
    }

    public static String A01(String str, String str2) {
        StringBuilder sb = new StringBuilder("PAY: ");
        StringBuilder sb2 = new StringBuilder(" - ");
        sb2.append(str);
        sb2.append(":");
        sb.append(sb2.toString());
        if (!TextUtils.isEmpty(str2)) {
            StringBuilder sb3 = new StringBuilder(" ");
            sb3.append(str2);
            sb.append(sb3.toString());
        }
        return sb.toString();
    }

    public String A02(String str) {
        return A03(this.A02, str).toString();
    }

    public final StringBuilder A03(String str, String str2) {
        StringBuilder sb = new StringBuilder("PAY: ");
        String str3 = this.A01;
        if (!TextUtils.isEmpty(str3)) {
            sb.append(String.format("[%s]", str3));
        }
        if (TextUtils.isEmpty(str)) {
            str = this.A02;
        }
        sb.append(String.format("[%s]", str));
        sb.append(" - ");
        sb.append(this.A00);
        sb.append(":");
        if (!TextUtils.isEmpty(str2)) {
            sb.append(" ");
            sb.append(str2);
        }
        return sb;
    }

    public void A04(String str) {
        A03(null, str);
    }

    public void A05(String str) {
        String obj = A03(null, str).toString();
        if (TextUtils.isEmpty(obj)) {
            Log.e((Throwable) null);
        } else {
            Log.e(obj);
        }
    }

    public void A06(String str) {
        A09(null, str, null);
    }

    public void A07(String str) {
        A03(null, str);
    }

    public void A08(String str, String str2, Throwable th) {
        String obj = A03(str, str2).toString();
        if (TextUtils.isEmpty(obj)) {
            Log.e(th);
        } else if (th == null) {
            Log.e(obj);
        } else {
            Log.e(obj, th);
        }
    }

    public void A09(String str, String str2, C31021Zs[] r8) {
        int length;
        StringBuilder A03 = A03(str, str2);
        if (r8 != null && (length = r8.length) > 0) {
            A03.append(" [");
            int i = 0;
            do {
                C31021Zs r1 = r8[i];
                A03.append("{");
                A03.append(r1.A00);
                A03.append(" : ");
                A03.append(r1.A01);
                A03.append("}");
                if (i != length - 1) {
                    A03.append(",");
                }
                i++;
            } while (i < length);
            A03.append("]");
        }
        Log.i(A03.toString());
    }

    public void A0A(String str, Throwable th) {
        String obj = A03(null, str).toString();
        if (TextUtils.isEmpty(obj)) {
            Log.e(th);
        } else if (th == null) {
            Log.e(obj);
        } else {
            Log.e(obj, th);
        }
    }
}
