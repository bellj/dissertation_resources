package X;

import android.os.Message;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.Collections;
import java.util.HashMap;

/* renamed from: X.3FT  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3FT {
    public C63633Ci A00;
    public final ActivityC13810kN A01;
    public final C14900mE A02;
    public final C15570nT A03;
    public final C15550nR A04;
    public final C15610nY A05;
    public final C18640sm A06;
    public final C21320xE A07;
    public final C15600nX A08;
    public final C20710wC A09;
    public final C15580nU A0A;
    public final C20660w7 A0B;
    public final C14860mA A0C;

    public AnonymousClass3FT(ActivityC13810kN r1, C14900mE r2, C15570nT r3, C63633Ci r4, C15550nR r5, C15610nY r6, C18640sm r7, C21320xE r8, C15600nX r9, C20710wC r10, C15580nU r11, C20660w7 r12, C14860mA r13) {
        this.A02 = r2;
        this.A03 = r3;
        this.A0C = r13;
        this.A0B = r12;
        this.A04 = r5;
        this.A05 = r6;
        this.A09 = r10;
        this.A07 = r8;
        this.A08 = r9;
        this.A06 = r7;
        this.A01 = r1;
        this.A0A = r11;
        this.A00 = r4;
    }

    public void A00(UserJid userJid, Integer num) {
        int A02;
        if (!A02()) {
            C15600nX r3 = this.A08;
            C15580nU r8 = this.A0A;
            int A022 = r3.A04.A02(r8);
            C14850m9 r1 = r3.A0B;
            if (A022 == 1) {
                A02 = r1.A02(1655);
            } else {
                A02 = r1.A02(1304) - 1;
            }
            if (A02 > r3.A02(r8).A0A().size()) {
                this.A01.Ady(R.string.participant_adding, R.string.register_wait_message);
                AnonymousClass32U r4 = new AnonymousClass32U(this.A07, this, this.A09, r8, userJid, this.A0C, num, Collections.singletonList(userJid));
                C20660w7 r12 = this.A0B;
                if (r12.A01.A06) {
                    Log.i("sendmethods/sendAddAdmins");
                    r12.A06.A08(Message.obtain(null, 0, 91, 0, r4), false);
                }
            } else if (this.A09.A0b(r8)) {
                C20710wC.A02(3019, Integer.valueOf(A02));
            } else {
                HashMap A11 = C12970iu.A11();
                C12960it.A1K(userJid, A11, 419);
                C20710wC.A02(3003, A11);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0010, code lost:
        if (r2 != 2) goto L_0x0012;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(com.whatsapp.jid.UserJid r14, java.lang.Integer r15) {
        /*
            r13 = this;
            r6 = r13
            X.0nX r0 = r13.A08
            X.0nU r8 = r13.A0A
            r9 = r14
            X.1YO r0 = r0.A01(r8, r14)
            if (r0 == 0) goto L_0x0012
            int r2 = r0.A01
            r1 = 2
            r0 = 1
            if (r2 == r1) goto L_0x0013
        L_0x0012:
            r0 = 0
        L_0x0013:
            r3 = 0
            if (r0 == 0) goto L_0x0045
            X.0nT r0 = r13.A03
            boolean r0 = r0.A0F(r14)
            if (r0 == 0) goto L_0x008f
            X.0wC r0 = r13.A09
            boolean r0 = r0.A0b(r8)
            if (r0 == 0) goto L_0x008f
            X.0kN r2 = r13.A01
            r1 = 2131887732(0x7f120674, float:1.941008E38)
            java.lang.Object[] r0 = new java.lang.Object[r3]
            r2.Adr(r0, r3, r1)
            X.3Ci r0 = r13.A00
            if (r0 == 0) goto L_0x0044
            X.3EZ r1 = r0.A02
            X.0nT r0 = r1.A02
            boolean r0 = r0.A0F(r14)
            if (r0 == 0) goto L_0x0044
            X.3F3 r1 = r1.A04
            r0 = 1
            r1.A02(r0)
        L_0x0044:
            return
        L_0x0045:
            X.0sm r0 = r13.A06
            boolean r0 = r0.A0B()
            if (r0 != 0) goto L_0x0059
            X.0mE r1 = r13.A02
            X.0kN r0 = r13.A01
            int r0 = X.C18640sm.A01(r0)
            r1.A07(r0, r3)
            return
        L_0x0059:
            X.0kN r2 = r13.A01
            r1 = 2131890183(0x7f121007, float:1.941505E38)
            r0 = 2131891267(0x7f121443, float:1.941725E38)
            r2.Ady(r1, r0)
            X.0mA r10 = r13.A0C
            X.0wC r7 = r13.A09
            X.0xE r5 = r13.A07
            java.util.List r12 = java.util.Collections.singletonList(r14)
            r11 = r15
            X.32V r4 = new X.32V
            r4.<init>(r5, r6, r7, r8, r9, r10, r11, r12)
            X.0w7 r1 = r13.A0B
            X.0og r0 = r1.A01
            boolean r0 = r0.A06
            if (r0 == 0) goto L_0x0044
            java.lang.String r0 = "sendmethods/sendRemoveAdmins"
            com.whatsapp.util.Log.i(r0)
            X.0qS r2 = r1.A06
            r1 = 0
            r0 = 92
            android.os.Message r0 = android.os.Message.obtain(r1, r3, r0, r3, r4)
            r2.A08(r0, r3)
            return
        L_0x008f:
            X.0kN r5 = r13.A01
            r4 = 2131888096(0x7f1207e0, float:1.9410818E38)
            java.lang.Object[] r2 = X.C12970iu.A1b()
            X.0nY r1 = r13.A05
            X.0nR r0 = r13.A04
            X.0n3 r0 = r0.A0B(r14)
            java.lang.String r0 = r1.A04(r0)
            r2[r3] = r0
            r5.Adr(r2, r3, r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3FT.A01(com.whatsapp.jid.UserJid, java.lang.Integer):void");
    }

    public boolean A02() {
        if (this.A06.A0B()) {
            return false;
        }
        this.A02.A07(C18640sm.A01(this.A01.getApplicationContext()), 0);
        return true;
    }
}
