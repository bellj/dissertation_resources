package X;

import android.net.Uri;
import com.facebook.redex.RunnableBRunnable0Shape13S0100000_I0_13;
import com.whatsapp.util.Log;
import java.util.Collections;
import java.util.Map;

/* renamed from: X.2CZ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2CZ implements AnonymousClass2BW {
    public C47522Bb A00;
    public boolean A01;
    public final ActivityC13810kN A02;
    public final C14900mE A03;
    public final C16170oZ A04;
    public final C15610nY A05;
    public final AbstractC20260vT A06 = new AbstractC20260vT() { // from class: X.55k
        @Override // X.AbstractC20260vT
        public final void AOa(AnonymousClass1I1 r6) {
            AnonymousClass2Ba r2;
            AnonymousClass2CZ r4 = AnonymousClass2CZ.this;
            if (!r4.A02.isFinishing() && r6.A01 && (r2 = r4.A0D) != null && r2.A00() == 4 && r2.A08()) {
                Log.i("MediaViewStreamingVideoPlayer/auto-retry");
                C47522Bb r0 = r4.A00;
                if (r0 != null) {
                    r0.A00("", true, 2);
                }
            }
        }
    };
    public final C18640sm A07;
    public final C16590pI A08;
    public final C20830wO A09;
    public final AnonymousClass2BV A0A;
    public final AnonymousClass1CH A0B;
    public final C22370yy A0C;
    public final AnonymousClass2Ba A0D;

    @Override // X.AnonymousClass2BW
    public void A5p(AnonymousClass5QP r1) {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00cb, code lost:
        if (r1 == false) goto L_0x00cd;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass2CZ(X.ActivityC13810kN r14, X.C14900mE r15, X.C16170oZ r16, X.C15610nY r17, X.C18640sm r18, X.C16590pI r19, X.C14950mJ r20, X.C20830wO r21, X.AnonymousClass1CH r22, X.C22370yy r23, X.AnonymousClass39D r24, X.AnonymousClass1X4 r25, X.C47522Bb r26) {
        /*
        // Method dump skipped, instructions count: 298
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2CZ.<init>(X.0kN, X.0mE, X.0oZ, X.0nY, X.0sm, X.0pI, X.0mJ, X.0wO, X.1CH, X.0yy, X.39D, X.1X4, X.2Bb):void");
    }

    @Override // X.AnonymousClass2BW
    public /* synthetic */ Map AGF() {
        return Collections.emptyMap();
    }

    @Override // X.AnonymousClass2BW
    public Uri AHS() {
        return this.A0A.AHS();
    }

    @Override // X.AnonymousClass2BW
    public long AYZ(AnonymousClass3H3 r4) {
        this.A03.A0H(new RunnableBRunnable0Shape13S0100000_I0_13(this, 24));
        return this.A0A.AYZ(r4);
    }

    @Override // X.AnonymousClass2BW
    public void close() {
        this.A03.A0H(new RunnableBRunnable0Shape13S0100000_I0_13(this, 23));
        this.A0A.close();
    }

    @Override // X.AnonymousClass2BY
    public int read(byte[] bArr, int i, int i2) {
        return this.A0A.read(bArr, i, i2);
    }
}
