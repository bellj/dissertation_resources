package X;

import android.view.animation.Animation;

/* renamed from: X.3xF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83443xF extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ AbstractC36001jA A00;

    public C83443xF(AbstractC36001jA r1) {
        this.A00 = r1;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        AbstractC36001jA r2 = this.A00;
        r2.A0G(r2.A0J.getHeight());
        r2.A0O(null, true);
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationStart(Animation animation) {
        AbstractC36001jA r1 = this.A00;
        r1.A0F(r1.A0J.getHeight());
        r1.A0S(true);
    }
}
