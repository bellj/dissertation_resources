package X;

/* renamed from: X.30n  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C614830n extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Integer A02;
    public Integer A03;
    public Long A04;
    public String A05;

    public C614830n() {
        super(2066, new AnonymousClass00E(1, 20, 20), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(8, this.A00);
        r3.Abe(2, this.A01);
        r3.Abe(1, this.A04);
        r3.Abe(7, this.A02);
        r3.Abe(3, this.A03);
        r3.Abe(5, this.A05);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamCameraTtc {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "cameraApi", C12960it.A0Y(this.A00));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "cameraFacing", C12960it.A0Y(this.A01));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "cameraTtcDuration", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "cameraType", C12960it.A0Y(this.A02));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "flashMode", C12960it.A0Y(this.A03));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "requestedPhotoResolution", this.A05);
        return C12960it.A0d("}", A0k);
    }
}
