package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;

/* renamed from: X.3pH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78663pH extends AnonymousClass1U5 implements AnonymousClass5SX {
    public static final C78663pH A01 = new C78663pH(Status.A09);
    public static final Parcelable.Creator CREATOR = new C98924jT();
    public final Status A00;

    @Override // X.AnonymousClass5SX
    public final Status AGw() {
        return this.A00;
    }

    public C78663pH(Status status) {
        this.A00 = status;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A0B(parcel, this.A00, 1, i, false);
        C95654e8.A06(parcel, A00);
    }
}
