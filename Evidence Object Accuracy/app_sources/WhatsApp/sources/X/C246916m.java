package X;

import java.security.PublicKey;
import java.security.spec.MGF1ParameterSpec;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.OAEPParameterSpec;
import javax.crypto.spec.PSource;

/* renamed from: X.16m  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C246916m {
    public byte[] A00 = null;
    public byte[] A01 = null;

    public static String A00(String str) {
        String[] split = str.split(",");
        StringBuilder sb = new StringBuilder();
        sb.append("CN");
        sb.append("=");
        String obj = sb.toString();
        for (String str2 : split) {
            String trim = str2.trim();
            if (trim.startsWith(obj)) {
                return trim.substring(obj.length());
            }
        }
        return null;
    }

    public static byte[] A01(PublicKey publicKey, SecretKey secretKey) {
        Cipher instance = Cipher.getInstance("RSA/ECB/OAEPWithSHA-256AndMGF1Padding");
        instance.init(1, publicKey, new OAEPParameterSpec("SHA-256", "MGF1", MGF1ParameterSpec.SHA256, PSource.PSpecified.DEFAULT));
        return instance.doFinal(secretKey.getEncoded());
    }

    public AnonymousClass1W0 A02(String str, PublicKey publicKey) {
        KeyGenerator instance = KeyGenerator.getInstance("AES");
        instance.init(128);
        SecretKey generateKey = instance.generateKey();
        byte[] bArr = new byte[16];
        C002901h.A00().nextBytes(bArr);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(bArr);
        byte[] bytes = str.getBytes();
        Cipher instance2 = Cipher.getInstance("AES/GCM/NoPadding");
        instance2.init(1, generateKey, ivParameterSpec);
        C30071Vz r1 = new C30071Vz(generateKey, instance2.doFinal(bytes), instance2.getIV());
        SecretKey secretKey = r1.A00;
        byte[] A01 = A01(publicKey, secretKey);
        this.A00 = secretKey.getEncoded();
        byte[] bArr2 = r1.A02;
        this.A01 = bArr2;
        return new AnonymousClass1W0(A01, r1.A01, bArr2);
    }
}
