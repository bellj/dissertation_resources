package X;

import com.whatsapp.payments.ui.mapper.register.IndiaUpiMapperPendingActivity;

/* renamed from: X.5dY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC119335dY extends ActivityC13790kL {
    public boolean A00 = false;

    public AbstractActivityC119335dY() {
        C117295Zj.A0p(this, 112);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            IndiaUpiMapperPendingActivity indiaUpiMapperPendingActivity = (IndiaUpiMapperPendingActivity) this;
            AnonymousClass2FL r3 = (AnonymousClass2FL) ((AnonymousClass2FJ) generatedComponent());
            AnonymousClass01J A1M = ActivityC13830kP.A1M(r3, indiaUpiMapperPendingActivity);
            ActivityC13810kN.A10(A1M, indiaUpiMapperPendingActivity);
            ((ActivityC13790kL) indiaUpiMapperPendingActivity).A08 = ActivityC13790kL.A0S(r3, A1M, indiaUpiMapperPendingActivity, ActivityC13790kL.A0Y(A1M, indiaUpiMapperPendingActivity));
            indiaUpiMapperPendingActivity.A00 = C117305Zk.A0T(A1M);
        }
    }
}
