package X;

import java.util.Map;

/* renamed from: X.33p  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C620333p extends AbstractC1112458q {
    public final /* synthetic */ AnonymousClass5WA A00;
    public final /* synthetic */ AnonymousClass17Q A01;
    public final /* synthetic */ AnonymousClass3FB A02;
    public final /* synthetic */ AnonymousClass3BF A03;

    public C620333p(AnonymousClass5WA r1, AnonymousClass17Q r2, AnonymousClass3FB r3, AnonymousClass3BF r4) {
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = r4;
        this.A00 = r1;
    }

    @Override // X.AbstractC1112458q, X.AbstractC21730xt
    public void AP1(String str) {
        C16700pc.A0E(str, 0);
        AnonymousClass17Q r1 = this.A01;
        r1.A06.A02.A05(this.A02.hashCode(), 105);
        this.A00.AQE(new AnonymousClass3FA("XMPP not connected", null, 1));
    }

    @Override // X.AbstractC1112458q, X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r7, String str) {
        C16700pc.A0F(str, r7);
        AnonymousClass3BF r2 = this.A03;
        AnonymousClass17Q r5 = this.A01;
        AnonymousClass3EW r0 = new AnonymousClass3EG(r5.A02, r7, r2).A01;
        C16700pc.A0B(r0);
        AnonymousClass3FA A00 = C63163An.A00(r0);
        AnonymousClass3FB r3 = this.A02;
        int hashCode = r3.hashCode();
        Long valueOf = Long.valueOf(A00.A00);
        AnonymousClass17L r02 = r5.A06;
        AnonymousClass17Q.A00(r02, r5, valueOf, hashCode);
        r02.A02.A05(r3.hashCode(), 467);
        this.A00.AQE(A00);
    }

    @Override // X.AbstractC1112458q, X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r9, String str) {
        String str2;
        C16700pc.A0E(r9, 1);
        AnonymousClass17Q r7 = this.A01;
        AnonymousClass3FB r4 = this.A02;
        int hashCode = r4.hashCode();
        AnonymousClass17L r3 = r7.A06;
        AnonymousClass1Q5 r2 = r3.A02;
        r2.A03(hashCode, "iqResponse");
        Map map = r4.A02;
        if (map != null) {
            Object obj = map.get("action");
            if ((obj instanceof String) && (str2 = (String) obj) != null) {
                r3.A02(r4.hashCode(), "action", str2);
            }
        }
        AnonymousClass3EI r6 = new C64013Dw(r7.A02, r9, this.A03).A01;
        C16700pc.A0B(r6);
        AnonymousClass3HE r5 = r6.A01;
        AnonymousClass3E2 r0 = r5.A03;
        if (r0 != null) {
            for (AnonymousClass3EH r02 : r0.A01.A01) {
                C16700pc.A0B(r02);
                AnonymousClass17Q.A01(r7, r02);
            }
        }
        AnonymousClass3E1 r03 = r5.A02;
        if (r03 != null) {
            r3.A02(r4.hashCode(), "num_screens_to_prefetch", String.valueOf(r03.A01.A01.size()));
        }
        r2.A05(r4.hashCode(), 467);
        this.A00.AQF(r6);
    }
}
