package X;

import android.os.Handler;
import android.os.Looper;

/* renamed from: X.61K  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass61K {
    public static final Handler A00 = C12970iu.A0E();

    public static void A00(Runnable runnable) {
        if (A02()) {
            runnable.run();
        } else {
            A00.post(runnable);
        }
    }

    public static void A01(String str) {
        if (A02()) {
            StringBuilder A0j = C12960it.A0j(str);
            A0j.append(" Current thread: ");
            throw new IllegalThreadStateException(C12960it.A0d(Thread.currentThread().getName(), A0j));
        }
    }

    public static boolean A02() {
        return C12970iu.A1Z(Looper.getMainLooper().getThread(), Thread.currentThread());
    }
}
