package X;

import java.util.List;

/* renamed from: X.523  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass523 implements AnonymousClass5T7 {
    @Override // X.AnonymousClass5T7
    public boolean A9i(AbstractC94534c0 r5, AbstractC94534c0 r6, AnonymousClass4RG r7) {
        int length;
        if (!C72473ef.A00(r6 instanceof C83013wY ? 1 : 0)) {
            return false;
        }
        int intValue = r6.A04().A00.intValue();
        if (r5 instanceof C82973wU) {
            length = r5.A06().A01.length();
        } else if (!(r5 instanceof C83003wX)) {
            return false;
        } else {
            C83003wX A03 = r5.A03();
            if (A03.A08() instanceof List) {
                length = C72453ed.A0C(A03.A08());
            } else {
                length = -1;
            }
        }
        if (length == intValue) {
            return true;
        }
        return false;
    }
}
