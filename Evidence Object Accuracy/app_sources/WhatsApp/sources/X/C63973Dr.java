package X;

import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.Jid;

/* renamed from: X.3Dr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63973Dr {
    public final C17220qS A00;
    public final AnonymousClass5WE A01;

    public C63973Dr(C17220qS r1, AnonymousClass5WE r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public void A00(GroupJid groupJid, GroupJid groupJid2, String str, String str2, String str3, String str4, String str5) {
        String str6;
        AnonymousClass1V8 r2;
        Jid jid;
        Jid jid2 = groupJid;
        C17220qS r10 = this.A00;
        Jid jid3 = groupJid2;
        boolean A1W = C12960it.A1W(str3);
        boolean A1W2 = C12960it.A1W(str2);
        AnonymousClass1W9[] r7 = new AnonymousClass1W9[2 + (A1W ? 1 : 0) + (A1W2 ? 1 : 0)];
        C12960it.A1M("query", str5, r7, 0);
        C12960it.A1M("type", str4, r7, 1);
        int i = 2;
        if (A1W) {
            C12960it.A1M("id", str3, r7, 2);
            i = 3;
        }
        if (A1W2) {
            C12960it.A1M("invite", str2, r7, i);
            r2 = new AnonymousClass1V8("picture", r7);
            jid = AnonymousClass1VY.A00;
        } else {
            AnonymousClass009.A05(jid2);
            if (groupJid2 == null) {
                str6 = "parent_group";
                jid3 = jid2;
            } else {
                str6 = "sub_group";
            }
            AnonymousClass1W9[] r22 = new AnonymousClass1W9[2];
            C12960it.A1M("type", str6, r22, 0);
            r22[1] = new AnonymousClass1W9(jid3, "jid");
            r2 = new AnonymousClass1V8(new AnonymousClass1V8("query_linked", r22), "picture", r7);
            jid = jid2;
        }
        AnonymousClass1W9[] r3 = new AnonymousClass1W9[4];
        C12960it.A1M("id", str, r3, 0);
        C12960it.A1M("xmlns", "w:g2", r3, 1);
        C12960it.A1M("type", "get", r3, 2);
        r10.A09(new AnonymousClass3ZI(groupJid2, this.A01, str4), AnonymousClass1V8.A00(jid, r2, r3, 3), str, 300, 32000);
    }
}
