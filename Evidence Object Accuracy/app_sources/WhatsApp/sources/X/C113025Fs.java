package X;

import java.security.SecureRandom;

/* renamed from: X.5Fs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C113025Fs implements AnonymousClass20L {
    public SecureRandom A00;
    public AnonymousClass20L A01;

    public C113025Fs(SecureRandom secureRandom, AnonymousClass20L r2) {
        this.A00 = secureRandom;
        this.A01 = r2;
    }
}
