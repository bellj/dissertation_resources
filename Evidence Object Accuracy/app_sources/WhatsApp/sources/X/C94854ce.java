package X;

/* renamed from: X.4ce  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C94854ce {
    public C94854ce A00;
    public final int A01;
    public final String A02;
    public final C94464br A03;
    public final C94464br A04;
    public final C94464br A05;

    public C94854ce(C94854ce r8, C94464br r9, C94464br r10) {
        this(r8.A02, r9, r10, r8.A04, r8.A01);
        this.A00 = r8.A00;
    }

    public C94854ce(String str, C94464br r2, C94464br r3, C94464br r4, int i) {
        this.A05 = r2;
        this.A03 = r3;
        this.A04 = r4;
        this.A01 = i;
        this.A02 = str;
    }

    public static C94854ce A00(C94854ce r7, C94464br r8, C94464br r9) {
        int i;
        if (r7 == null) {
            return null;
        }
        C94854ce A00 = A00(r7.A00, r8, r9);
        r7.A00 = A00;
        C94464br r5 = r7.A05;
        int i2 = r5.A00;
        C94464br r3 = r7.A03;
        int i3 = r3.A00;
        int i4 = r8.A00;
        if (r9 == null) {
            i = Integer.MAX_VALUE;
        } else {
            i = r9.A00;
        }
        if (i4 >= i3 || i <= i2) {
            return r7;
        }
        if (i4 > i2) {
            if (i < i3) {
                r7.A00 = new C94854ce(r7, r9, r3);
            }
            return new C94854ce(r7, r5, r8);
        } else if (i >= i3) {
            return A00;
        } else {
            return new C94854ce(r7, r9, r3);
        }
    }
}
