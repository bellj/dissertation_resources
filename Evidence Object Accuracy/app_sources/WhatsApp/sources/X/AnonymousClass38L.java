package X;

import com.whatsapp.gallery.GalleryFragmentBase;
import java.lang.ref.WeakReference;

/* renamed from: X.38L  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass38L extends AbstractC16350or {
    public AnonymousClass02N A00;
    public final C15250mo A01;
    public final AbstractC14640lm A02;
    public final WeakReference A03;

    public AnonymousClass38L(C15250mo r2, GalleryFragmentBase galleryFragmentBase, AbstractC14640lm r4) {
        this.A03 = C12970iu.A10(galleryFragmentBase);
        this.A02 = r4;
        this.A01 = r2;
    }
}
