package X;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.os.Build;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

/* renamed from: X.0UO  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0UO {
    public char A00;
    public char A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public int A09;
    public int A0A;
    public int A0B;
    public int A0C;
    public int A0D;
    public ColorStateList A0E = null;
    public PorterDuff.Mode A0F = null;
    public Menu A0G;
    public AbstractC04730Mv A0H;
    public CharSequence A0I;
    public CharSequence A0J;
    public CharSequence A0K;
    public CharSequence A0L;
    public String A0M;
    public String A0N;
    public boolean A0O;
    public boolean A0P;
    public boolean A0Q;
    public boolean A0R;
    public boolean A0S;
    public boolean A0T;
    public final /* synthetic */ AnonymousClass0Ax A0U;

    public AnonymousClass0UO(Menu menu, AnonymousClass0Ax r3) {
        this.A0U = r3;
        this.A0G = menu;
        this.A04 = 0;
        this.A02 = 0;
        this.A05 = 0;
        this.A03 = 0;
        this.A0P = true;
        this.A0O = true;
    }

    public final Object A00(String str, Class[] clsArr, Object[] objArr) {
        try {
            Constructor<?> constructor = Class.forName(str, false, this.A0U.A00.getClassLoader()).getConstructor(clsArr);
            constructor.setAccessible(true);
            return constructor.newInstance(objArr);
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder("Cannot instantiate class: ");
            sb.append(str);
            Log.w("SupportMenuInflater", sb.toString(), e);
            return null;
        }
    }

    public final void A01(MenuItem menuItem) {
        MenuItem enabled = menuItem.setChecked(this.A0R).setVisible(this.A0T).setEnabled(this.A0S);
        boolean z = false;
        boolean z2 = false;
        if (this.A09 >= 1) {
            z2 = true;
        }
        enabled.setCheckable(z2).setTitleCondensed(this.A0K).setIcon(this.A0A);
        int i = this.A0D;
        if (i >= 0) {
            menuItem.setShowAsAction(i);
        }
        if (this.A0N != null) {
            AnonymousClass0Ax r3 = this.A0U;
            Context context = r3.A00;
            if (!context.isRestricted()) {
                Object obj = r3.A01;
                if (obj == null) {
                    obj = r3.A00(context);
                    r3.A01 = obj;
                }
                menuItem.setOnMenuItemClickListener(new AnonymousClass0W2(obj, this.A0N));
            } else {
                throw new IllegalStateException("The android:onClick attribute cannot be used within a restricted context");
            }
        }
        if (this.A09 >= 2) {
            if (menuItem instanceof C07340Xp) {
                C07340Xp r2 = (C07340Xp) menuItem;
                r2.A02 = 4 | (r2.A02 & -5);
            } else if (menuItem instanceof AnonymousClass0CJ) {
                AnonymousClass0CJ r4 = (AnonymousClass0CJ) menuItem;
                try {
                    Method method = r4.A00;
                    if (method == null) {
                        method = r4.A01.getClass().getDeclaredMethod("setExclusiveCheckable", Boolean.TYPE);
                        r4.A00 = method;
                    }
                    method.invoke(r4.A01, true);
                } catch (Exception e) {
                    Log.w("MenuItemWrapper", "Error while calling setExclusiveCheckable", e);
                }
            }
        }
        String str = this.A0M;
        if (str != null) {
            menuItem.setActionView((View) A00(str, AnonymousClass0Ax.A05, this.A0U.A03));
            z = true;
        }
        int i2 = this.A06;
        if (i2 > 0) {
            if (!z) {
                menuItem.setActionView(i2);
            } else {
                Log.w("SupportMenuInflater", "Ignoring attribute 'itemActionViewLayout'. Action view already specified.");
            }
        }
        AbstractC04730Mv r1 = this.A0H;
        if (r1 != null) {
            if (menuItem instanceof AbstractMenuItemC017808h) {
                ((AbstractMenuItemC017808h) menuItem).Acx(r1);
            } else {
                Log.w("MenuItemCompat", "setActionProvider: item does not implement SupportMenuItem; ignoring");
            }
        }
        CharSequence charSequence = this.A0I;
        boolean z3 = menuItem instanceof AbstractMenuItemC017808h;
        if (z3) {
            ((AbstractMenuItemC017808h) menuItem).Abw(charSequence);
        } else if (Build.VERSION.SDK_INT >= 26) {
            AnonymousClass0UJ.A04(menuItem, charSequence);
        }
        CharSequence charSequence2 = this.A0L;
        if (z3) {
            ((AbstractMenuItemC017808h) menuItem).Ad2(charSequence2);
        } else if (Build.VERSION.SDK_INT >= 26) {
            AnonymousClass0UJ.A05(menuItem, charSequence2);
        }
        char c = this.A00;
        int i3 = this.A07;
        if (z3) {
            ((AbstractMenuItemC017808h) menuItem).setAlphabeticShortcut(c, i3);
        } else if (Build.VERSION.SDK_INT >= 26) {
            AnonymousClass0UJ.A02(menuItem, c, i3);
        }
        char c2 = this.A01;
        int i4 = this.A0C;
        if (z3) {
            ((AbstractMenuItemC017808h) menuItem).setNumericShortcut(c2, i4);
        } else if (Build.VERSION.SDK_INT >= 26) {
            AnonymousClass0UJ.A03(menuItem, c2, i4);
        }
        PorterDuff.Mode mode = this.A0F;
        if (mode != null) {
            if (z3) {
                ((AbstractMenuItemC017808h) menuItem).setIconTintMode(mode);
            } else if (Build.VERSION.SDK_INT >= 26) {
                AnonymousClass0UJ.A01(mode, menuItem);
            }
        }
        ColorStateList colorStateList = this.A0E;
        if (colorStateList != null) {
            AnonymousClass07G.A00(colorStateList, menuItem);
        }
    }
}
