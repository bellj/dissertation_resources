package X;

import com.whatsapp.util.Log;
import java.security.GeneralSecurityException;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.X509Certificate;
import java.util.Map;
import org.json.JSONException;

/* renamed from: X.3Ww  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68823Ww implements AbstractC116705Wm {
    public final /* synthetic */ AbstractC63833Dc A00;
    public final /* synthetic */ C64663Gk A01;
    public final /* synthetic */ C16820po A02;
    public final /* synthetic */ AnonymousClass17H A03;

    public C68823Ww(AbstractC63833Dc r1, C64663Gk r2, C16820po r3, AnonymousClass17H r4) {
        this.A03 = r4;
        this.A02 = r3;
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC116705Wm
    public void AP0(Exception exc) {
        PublicKey publicKey;
        C64663Gk r4 = this.A01;
        AbstractC63833Dc r3 = this.A00;
        if (r4 != null) {
            try {
                X509Certificate x509Certificate = r4.A04;
                String str = r4.A03;
                if (str == null) {
                    publicKey = null;
                } else {
                    publicKey = C63083Af.A00(str);
                }
                r3.A00(r4.A02, publicKey, x509Certificate);
                return;
            } catch (GeneralSecurityException unused) {
            }
        }
        Log.e("FBUserEntityManagement : Network failed  while sending the payload");
        r3.A00.AOy();
    }

    @Override // X.AbstractC116705Wm
    public void APp(Exception exc) {
        PublicKey publicKey;
        C64663Gk r4 = this.A01;
        AbstractC63833Dc r3 = this.A00;
        if (r4 != null) {
            try {
                X509Certificate x509Certificate = r4.A04;
                String str = r4.A03;
                if (str == null) {
                    publicKey = null;
                } else {
                    publicKey = C63083Af.A00(str);
                }
                r3.A00(r4.A02, publicKey, x509Certificate);
                return;
            } catch (GeneralSecurityException unused) {
            }
        }
        Log.e("FBUserEntityManagement : On error response while sending the payload");
        r3.A00.APp(exc);
    }

    @Override // X.AbstractC116705Wm
    public void AXB(Integer num, String str, String str2, String str3, String str4, String str5) {
        try {
            AnonymousClass17H r2 = this.A03;
            AnonymousClass17H.A00(this.A00, this.A02, r2, num, str2, str3, str4, str5);
        } catch (CertificateExpiredException unused) {
            AnonymousClass17H r3 = this.A03;
            AnonymousClass17E r22 = r3.A02;
            C16820po r1 = this.A02;
            try {
                Map A00 = r22.A00();
                A00.remove(r1);
                r22.A01(A00);
            } catch (CertificateException | JSONException e) {
                AnonymousClass009.A0D(e);
            }
            Object obj = ((AnonymousClass17G) r3.A03.get()).A00.get(r1);
            AnonymousClass009.A05(obj);
            ((AnonymousClass17F) ((AnonymousClass01N) obj).get()).A9q(new C68813Wv(this));
        }
    }
}
