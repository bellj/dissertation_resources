package X;

/* renamed from: X.1Km  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C28071Km extends AbstractC16110oT {
    public Boolean A00;
    public Integer A01;

    public C28071Km() {
        super(3150, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A01);
        r3.Abe(2, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        StringBuilder sb = new StringBuilder("WamGatedChatOpened {");
        Integer num = this.A01;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "chatGatedReason", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "selfInitiated", this.A00);
        sb.append("}");
        return sb.toString();
    }
}
