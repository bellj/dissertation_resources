package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.jid.Jid;

/* renamed from: X.1VY  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1VY extends Jid implements Parcelable {
    public static final AnonymousClass1VY A00 = new AnonymousClass1VY();
    public static final Parcelable.Creator CREATOR = new C100074lK();

    @Override // com.whatsapp.jid.Jid
    public String getServer() {
        return "s.whatsapp.net";
    }

    @Override // com.whatsapp.jid.Jid
    public int getType() {
        return 13;
    }

    public AnonymousClass1VY() {
        super("");
    }

    public AnonymousClass1VY(Parcel parcel) {
        super(parcel);
    }
}
