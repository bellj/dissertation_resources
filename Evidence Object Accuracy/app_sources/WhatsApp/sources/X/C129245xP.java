package X;

import android.app.Activity;
import android.content.Context;
import com.whatsapp.payments.ui.IndiaUpiSendPaymentToVpaFragment;
import com.whatsapp.payments.ui.PaymentBottomSheet;

/* renamed from: X.5xP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129245xP {
    public IndiaUpiSendPaymentToVpaFragment A00;
    public PaymentBottomSheet A01;
    public final Activity A02;
    public final Context A03;
    public final AbstractC13860kS A04;
    public final AnonymousClass18T A05;
    public final AnonymousClass18S A06;
    public final String A07;

    public C129245xP(Activity activity, AnonymousClass18T r2, AnonymousClass18S r3, String str) {
        this.A02 = activity;
        this.A03 = activity;
        this.A07 = str;
        this.A04 = (AbstractC13860kS) activity;
        this.A06 = r3;
        this.A05 = r2;
    }

    public void A00(PaymentBottomSheet paymentBottomSheet) {
        IndiaUpiSendPaymentToVpaFragment indiaUpiSendPaymentToVpaFragment = this.A00;
        if (indiaUpiSendPaymentToVpaFragment == null) {
            indiaUpiSendPaymentToVpaFragment = new IndiaUpiSendPaymentToVpaFragment();
            this.A00 = indiaUpiSendPaymentToVpaFragment;
        }
        indiaUpiSendPaymentToVpaFragment.A0N = this;
        if (paymentBottomSheet != null) {
            this.A01 = paymentBottomSheet;
            paymentBottomSheet.A1L(indiaUpiSendPaymentToVpaFragment);
            return;
        }
        PaymentBottomSheet paymentBottomSheet2 = new PaymentBottomSheet();
        this.A01 = paymentBottomSheet2;
        paymentBottomSheet2.A01 = this.A00;
        this.A04.Adm(paymentBottomSheet2);
    }
}
