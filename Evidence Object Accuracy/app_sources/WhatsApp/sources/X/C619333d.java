package X;

/* renamed from: X.33d  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C619333d extends AnonymousClass1V4 {
    public final int A00;

    public C619333d(AbstractC15710nm r14, C14830m7 r15, C16120oU r16, C17230qT r17, Integer num, String str, String str2, long j, long j2) {
        super(r14, r15, r16, r17, num, str, 3, j, j2);
        this.A00 = A00(str2);
    }

    public static int A00(String str) {
        if ("offer".equals(str)) {
            return 0;
        }
        if ("accept".equals(str)) {
            return 1;
        }
        if ("reject".equals(str)) {
            return 2;
        }
        if ("video".equals(str)) {
            return 3;
        }
        if ("terminate".equals(str)) {
            return 4;
        }
        if ("enc_rekey".equals(str)) {
            return 5;
        }
        throw C12970iu.A0f(C12960it.A0d(str, C12960it.A0k("CallStanza/getCallStanzaType not expected type ")));
    }

    @Override // X.AnonymousClass1V4
    public String toString() {
        StringBuilder A0h = C12960it.A0h();
        A0h.append(super.toString());
        A0h.append("; type = ");
        return C12960it.A0f(A0h, this.A00);
    }
}
