package X;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.SparseIntArray;
import com.whatsapp.location.LocationSharingService;
import com.whatsapp.util.Log;

/* renamed from: X.3LT  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3LT implements LocationListener {
    public long A00;
    public Location A01;
    public Location A02;
    public PowerManager.WakeLock A03;
    public final SparseIntArray A04 = new SparseIntArray();
    public final C244615p A05;
    public final C18280sC A06;
    public final AnonymousClass143 A07;
    public final AnonymousClass01d A08;
    public final C14830m7 A09;
    public final C14820m6 A0A;
    public final AbstractC27501Ht A0B;

    @Override // android.location.LocationListener
    public void onProviderDisabled(String str) {
    }

    @Override // android.location.LocationListener
    public void onProviderEnabled(String str) {
    }

    @Override // android.location.LocationListener
    public void onStatusChanged(String str, int i, Bundle bundle) {
    }

    public AnonymousClass3LT(C244615p r2, C18280sC r3, AnonymousClass143 r4, AnonymousClass01d r5, C14830m7 r6, C14820m6 r7, AbstractC27501Ht r8) {
        this.A09 = r6;
        this.A08 = r5;
        this.A0A = r7;
        this.A06 = r3;
        this.A05 = r2;
        this.A07 = r4;
        this.A0B = r8;
    }

    public final void A00() {
        long j = this.A00;
        if (j != 0) {
            int A0D = (int) C12980iv.A0D(j - (j % 3600000));
            SparseIntArray sparseIntArray = this.A04;
            sparseIntArray.put(A0D, sparseIntArray.get(A0D, 0) + ((int) (System.currentTimeMillis() - j)));
            StringBuilder A0h = C12960it.A0h();
            for (int i = 0; i < sparseIntArray.size(); i++) {
                int keyAt = sparseIntArray.keyAt(i);
                int i2 = sparseIntArray.get(keyAt);
                if (i != 0) {
                    A0h.append(";");
                }
                A0h.append(keyAt);
                A0h.append(",");
                A0h.append(i2);
            }
            C14820m6 r0 = this.A0A;
            C12970iu.A1D(C12960it.A08(r0), "location_shared_duration", A0h.toString());
            this.A00 = 0;
        }
    }

    public final void A01(Location location) {
        String str;
        this.A02 = location;
        LocationSharingService locationSharingService = (LocationSharingService) this.A0B;
        if (locationSharingService.A0J) {
            locationSharingService.A08.A0O(location);
            AnonymousClass13O r7 = locationSharingService.A09;
            AnonymousClass13N r1 = r7.A03;
            C30751Yr A05 = r1.A05(location);
            r7.A06.A09(A05.A06, r1.A04(A05, null), C12980iv.A0D(r7.A01.A00() - A05.A05));
        }
        long A00 = locationSharingService.A05.A00();
        long j = locationSharingService.A00;
        if (A00 > j) {
            str = C12970iu.A0w(C12960it.A0k("LocationSharingService/onLocationUpdate/stop this service since passed maxEndTime; maxEndTime="), j);
        } else if (!locationSharingService.A08.A0d()) {
            str = "LocationSharingService/onLocationUpdate/stop this service, no longer sharing live location";
        } else if (locationSharingService.A0I) {
            locationSharingService.A08.A0O(location);
            if (!locationSharingService.A08.A0e()) {
                locationSharingService.A08.A0I();
                return;
            }
            return;
        } else {
            return;
        }
        Log.i(str);
        locationSharingService.A0I = false;
        locationSharingService.A02();
    }

    @Override // android.location.LocationListener
    public void onLocationChanged(Location location) {
        Log.i("MyLocationUpdater/onLocationChanged");
        if (AnonymousClass13N.A03(location, this.A02)) {
            A01(location);
            this.A01 = location;
            return;
        }
        if (location.getAccuracy() < 80.0f) {
            this.A01 = location;
        }
        if (this.A01 != null && this.A02.getTime() + 40000 < this.A01.getTime()) {
            A01(this.A01);
        }
    }
}
