package X;

import android.hardware.Camera;
import android.util.Log;
import java.io.IOException;
import java.util.concurrent.Callable;

/* renamed from: X.6KQ  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6KQ implements Callable {
    public final /* synthetic */ Camera A00;
    public final /* synthetic */ AnonymousClass661 A01;

    public AnonymousClass6KQ(Camera camera, AnonymousClass661 r2) {
        this.A01 = r2;
        this.A00 = camera;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        AnonymousClass661 r5 = this.A01;
        C128965wx r1 = r5.A0L;
        Camera camera = this.A00;
        r1.A00(true, camera);
        try {
            camera.setPreviewTexture(null);
        } catch (IOException e) {
            Log.e("Camera1Device", "Unable to remove the current SurfaceTexture", e);
        }
        r5.A0M.A01(camera);
        camera.release();
        return null;
    }
}
