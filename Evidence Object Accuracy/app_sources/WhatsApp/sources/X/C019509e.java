package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* renamed from: X.09e  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C019509e extends BroadcastReceiver {
    public final /* synthetic */ C03160Gk A00;

    public C019509e(C03160Gk r1) {
        this.A00 = r1;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (intent != null && intent.getAction() != null && intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")) {
            C06390Tk.A00().A02(C03160Gk.A03, "Network broadcast received", new Throwable[0]);
            C03160Gk r1 = this.A00;
            r1.A04(r1.A00());
        }
    }
}
