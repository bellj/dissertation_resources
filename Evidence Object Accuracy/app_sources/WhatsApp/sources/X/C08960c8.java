package X;

import java.io.EOFException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Arrays;

/* renamed from: X.0c8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08960c8 implements Comparable, Serializable {
    public static final C08960c8 A02 = C95384dc.A02();
    public static final long serialVersionUID = 1;
    public transient int A00;
    public transient String A01;
    public final byte[] data;

    public C08960c8(byte[] bArr) {
        C16700pc.A0D(bArr, 1);
        this.data = bArr;
    }

    public byte A00(int i) {
        return this.data[i];
    }

    public int A01() {
        return this.data.length;
    }

    /* renamed from: A02 */
    public int compareTo(C08960c8 r8) {
        C16700pc.A0D(r8, 0);
        int A01 = A01();
        int A012 = r8.A01();
        int min = Math.min(A01, A012);
        for (int i = 0; i < min; i++) {
            int A00 = A00(i) & 255;
            int A002 = r8.A00(i) & 255;
            if (A00 != A002) {
                if (A00 < A002) {
                    return -1;
                } else {
                    return 1;
                }
            }
        }
        if (A01 == A012) {
            return 0;
        }
        if (A01 < A012) {
            return -1;
        }
        return 1;
    }

    public String A03() {
        return C95384dc.A00(this);
    }

    public boolean A04(C08960c8 r3, int i, int i2, int i3) {
        return r3.A05(this.data, 0, 0, i3);
    }

    public boolean A05(byte[] bArr, int i, int i2, int i3) {
        C16700pc.A0D(bArr, 1);
        if (i >= 0) {
            byte[] bArr2 = this.data;
            if (i <= bArr2.length - i3 && i2 >= 0 && i2 <= bArr.length - i3) {
                for (int i4 = 0; i4 < i3; i4++) {
                    if (bArr2[i4 + i] == bArr[i4 + i2]) {
                    }
                }
                return true;
            }
        }
        return false;
    }

    public byte[] A06() {
        return this.data;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        C08960c8 r6;
        byte[] bArr;
        int length;
        if (obj == this || ((obj instanceof C08960c8) && (r6 = (C08960c8) obj).A01() == (length = (bArr = this.data).length) && r6.A05(bArr, 0, 0, length))) {
            return true;
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        int i = this.A00;
        if (i != 0) {
            return i;
        }
        int hashCode = Arrays.hashCode(this.data);
        this.A00 = hashCode;
        return hashCode;
    }

    private final void readObject(ObjectInputStream objectInputStream) {
        int readInt = objectInputStream.readInt();
        int i = 0;
        if (readInt >= 0) {
            byte[] bArr = new byte[readInt];
            while (i < readInt) {
                int read = objectInputStream.read(bArr, i, readInt - i);
                if (read != -1) {
                    i += read;
                } else {
                    throw new EOFException();
                }
            }
            C08960c8 r2 = new C08960c8(bArr);
            Field declaredField = C08960c8.class.getDeclaredField("data");
            C16700pc.A0A(declaredField);
            declaredField.setAccessible(true);
            declaredField.set(this, r2.data);
            return;
        }
        StringBuilder sb = new StringBuilder("byteCount < 0: ");
        sb.append(readInt);
        throw new IllegalArgumentException(sb.toString());
    }

    @Override // java.lang.Object
    public String toString() {
        return C95384dc.A01(this);
    }

    private final void writeObject(ObjectOutputStream objectOutputStream) {
        objectOutputStream.writeInt(this.data.length);
        objectOutputStream.write(this.data);
    }
}
