package X;

import java.util.Locale;

/* renamed from: X.5HH  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5HH extends ThreadLocal {
    @Override // java.lang.ThreadLocal
    public Object initialValue() {
        return Locale.US;
    }
}
