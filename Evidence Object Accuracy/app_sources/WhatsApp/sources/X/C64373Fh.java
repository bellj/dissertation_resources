package X;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Stack;

/* renamed from: X.3Fh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C64373Fh {
    public final Stack A00 = new Stack();
    public final Stack A01 = new Stack();

    public final ArrayList A00() {
        ArrayList A0l = C12960it.A0l();
        Iterator it = this.A01.iterator();
        while (it.hasNext()) {
            A0l.add(((AnonymousClass4VP) ((C17520qw) it.next()).first).A01);
        }
        return A0l;
    }

    public final ArrayList A01() {
        ArrayList A0l = C12960it.A0l();
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            A0l.add(((AnonymousClass4VP) ((C17520qw) it.next()).first).A01);
        }
        return A0l;
    }

    public final void A02() {
        Stack stack = this.A00;
        if (stack.size() >= 1) {
            stack.pop();
        }
    }

    public final void A03() {
        Stack stack = this.A01;
        if (stack.size() >= 1) {
            stack.pop();
        }
    }
}
