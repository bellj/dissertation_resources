package X;

/* renamed from: X.4wG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C106774wG implements AbstractC116785Ww {
    public int A00;
    public int A01 = 1;
    public int A02;
    public int A03;
    public long A04;
    public long A05;
    public AbstractC14070ko A06;
    public C76853mL A07;
    public C76843mK A08;
    public boolean A09;
    public boolean A0A;
    public final C76863mM A0B = new C76863mM();
    public final C95304dT A0C = C95304dT.A05(9);
    public final C95304dT A0D = C95304dT.A05(4);
    public final C95304dT A0E = new C95304dT();
    public final C95304dT A0F = C95304dT.A05(11);

    public final C95304dT A00(AnonymousClass5Yf r5) {
        int i = this.A02;
        C95304dT r3 = this.A0E;
        int length = r3.A02.length;
        if (i > length) {
            r3.A0U(new byte[Math.max(length << 1, i)], 0);
        } else {
            r3.A0S(0);
        }
        r3.A0R(this.A02);
        r5.readFully(r3.A02, 0, this.A02);
        return r3;
    }

    @Override // X.AbstractC116785Ww
    public void AIa(AbstractC14070ko r1) {
        this.A06 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:152:0x03a7, code lost:
        return -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x0205, code lost:
        if (r7 != 1) goto L_0x00bc;
     */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x00da A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:165:0x0007 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00c1 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00dc  */
    @Override // X.AbstractC116785Ww
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int AZn(X.AnonymousClass5Yf r24, X.AnonymousClass4IG r25) {
        /*
        // Method dump skipped, instructions count: 936
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C106774wG.AZn(X.5Yf, X.4IG):int");
    }

    @Override // X.AbstractC116785Ww
    public void AbQ(long j, long j2) {
        if (j == 0) {
            this.A01 = 1;
            this.A09 = false;
        } else {
            this.A01 = 3;
        }
        this.A00 = 0;
    }

    @Override // X.AbstractC116785Ww
    public boolean Ae5(AnonymousClass5Yf r5) {
        C95304dT r3 = this.A0D;
        r5.AZ4(r3.A02, 0, 3);
        r3.A0S(0);
        if (r3.A0D() != 4607062) {
            return false;
        }
        r5.AZ4(r3.A02, 0, 2);
        r3.A0S(0);
        if ((r3.A0F() & 250) != 0) {
            return false;
        }
        r5.AZ4(r3.A02, 0, 4);
        int A03 = C95304dT.A03(r3, 0);
        r5.Aaj();
        r5.A5r(A03);
        C95304dT.A06(r5, r3, 4);
        if (C95304dT.A03(r3, 0) == 0) {
            return true;
        }
        return false;
    }
}
