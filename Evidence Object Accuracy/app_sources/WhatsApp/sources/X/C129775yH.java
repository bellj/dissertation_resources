package X;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: X.5yH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129775yH {
    public volatile List A00 = Collections.unmodifiableList(C12960it.A0l());

    public synchronized void A00() {
        this.A00 = Collections.unmodifiableList(C12960it.A0l());
    }

    public synchronized boolean A01(Object obj) {
        if (this.A00.contains(obj)) {
            return false;
        }
        ArrayList A0w = C12980iv.A0w(this.A00.size() + 1);
        A0w.addAll(this.A00);
        A0w.add(obj);
        this.A00 = Collections.unmodifiableList(A0w);
        return true;
    }

    public synchronized boolean A02(Object obj) {
        boolean z;
        z = false;
        if (this.A00.contains(obj)) {
            int size = this.A00.size();
            int i = size - 1;
            ArrayList A0w = C12980iv.A0w(i);
            int indexOf = this.A00.indexOf(obj);
            if (indexOf > 0) {
                A0w.addAll(this.A00.subList(0, indexOf));
            }
            z = true;
            if (indexOf < i) {
                A0w.addAll(this.A00.subList(indexOf + 1, size));
            }
            this.A00 = Collections.unmodifiableList(A0w);
        }
        return z;
    }
}
