package X;

import android.util.SparseArray;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/* renamed from: X.0pG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public enum EnumC16570pG {
    A08(0),
    A04(12),
    A05(13),
    A06(14),
    A07(15);
    
    public static SparseArray A00;
    public static EnumC16570pG A01;
    public static EnumC16570pG A02;
    public final int version;

    EnumC16570pG(int i) {
        this.version = i;
    }

    public static synchronized EnumC16570pG A00() {
        EnumC16570pG r6;
        synchronized (EnumC16570pG.class) {
            r6 = A01;
            if (r6 == null) {
                r6 = A07;
                EnumC16570pG[] values = values();
                for (EnumC16570pG r2 : values) {
                    if (r2.version > r6.version) {
                        r6 = r2;
                    }
                }
                A01 = r6;
            }
        }
        return r6;
    }

    public static synchronized EnumC16570pG A01() {
        EnumC16570pG r6;
        synchronized (EnumC16570pG.class) {
            r6 = A02;
            if (r6 == null) {
                r6 = A04;
                EnumC16570pG[] values = values();
                for (EnumC16570pG r2 : values) {
                    if (r2.version < r6.version) {
                        r6 = r2;
                    }
                }
                A02 = r6;
            }
        }
        return r6;
    }

    public static synchronized EnumC16570pG A02(int i) {
        EnumC16570pG r0;
        synchronized (EnumC16570pG.class) {
            if (A00 == null) {
                A03();
            }
            r0 = (EnumC16570pG) A00.get(i);
        }
        return r0;
    }

    public static synchronized void A03() {
        synchronized (EnumC16570pG.class) {
            A00 = new SparseArray(values().length);
            EnumC16570pG[] values = values();
            for (EnumC16570pG r2 : values) {
                A00.append(r2.version, r2);
            }
        }
    }

    public static synchronized EnumC16570pG[] A04(EnumC16570pG r6, EnumC16570pG r7) {
        EnumC16570pG[] r0;
        synchronized (EnumC16570pG.class) {
            if (A00 == null) {
                A03();
            }
            ArrayList arrayList = new ArrayList();
            int i = 0;
            while (true) {
                SparseArray sparseArray = A00;
                if (i < sparseArray.size()) {
                    int keyAt = sparseArray.keyAt(i);
                    if (keyAt >= r6.version && keyAt <= r7.version) {
                        arrayList.add((EnumC16570pG) A00.get(keyAt));
                    }
                    i++;
                } else {
                    Collections.sort(arrayList, new Comparator() { // from class: X.5CU
                        @Override // java.util.Comparator
                        public final int compare(Object obj, Object obj2) {
                            return ((EnumC16570pG) obj).version - ((EnumC16570pG) obj2).version;
                        }
                    });
                    r0 = (EnumC16570pG[]) arrayList.toArray(new EnumC16570pG[0]);
                }
            }
        }
        return r0;
    }
}
