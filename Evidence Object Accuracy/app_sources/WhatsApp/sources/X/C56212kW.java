package X;

import java.util.HashMap;

/* renamed from: X.2kW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56212kW extends AbstractC64703Go {
    public int A00;
    public int A01;
    public String A02;

    public final String toString() {
        HashMap A11 = C12970iu.A11();
        A11.put("language", this.A02);
        Integer A0i = C12980iv.A0i();
        A11.put("screenColors", A0i);
        A11.put("screenWidth", Integer.valueOf(this.A00));
        A11.put("screenHeight", Integer.valueOf(this.A01));
        A11.put("viewportWidth", A0i);
        return AbstractC64703Go.A01("viewportHeight", A0i, A11);
    }
}
