package X;

import android.text.TextUtils;

/* renamed from: X.6DL  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6DL implements AbstractC21730xt {
    public final /* synthetic */ AnonymousClass68P A00;
    public final /* synthetic */ AbstractC116705Wm A01;

    public AnonymousClass6DL(AnonymousClass68P r1, AbstractC116705Wm r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        this.A01.AP0(new AnonymousClass2JZ(str));
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r5, String str) {
        this.A00.A01.AaV("BaseFetchCertificateProtocolHelper failed with a server error", null, false);
        this.A01.APp(new C47732Ce(r5, str));
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r11, String str) {
        Integer num;
        try {
            AbstractC116705Wm r3 = this.A01;
            AnonymousClass1V8 A0F = r11.A0F("reply");
            String str2 = null;
            String A0I = A0F.A0I("algorithm", null);
            if (A0I == null) {
                A0I = "rsa2048";
            }
            AnonymousClass1V8 A0F2 = A0F.A0F("encryption_pem");
            AnonymousClass1V8 A0F3 = A0F.A0F("signature_pem");
            String A0G = A0F2.A0G();
            if (A0G != null) {
                String A0G2 = A0F3.A0G();
                if (A0G2 != null) {
                    AnonymousClass1V8 A0E = A0F.A0E("password_pem");
                    String str3 = null;
                    if (A0E != null) {
                        str3 = A0E.A0G();
                        if (!TextUtils.isEmpty(str3)) {
                            str2 = A0E.A0H("key_id");
                            try {
                                num = Integer.valueOf(A0E.A0H("ttl"));
                            } catch (NumberFormatException e) {
                                throw new AnonymousClass1V9(e);
                            }
                        } else {
                            throw new AnonymousClass1V9("empty key");
                        }
                    } else {
                        num = null;
                    }
                    r3.AXB(num, A0I, A0G, A0G2, str3, str2);
                    return;
                }
                throw new AnonymousClass1V9("missing sig");
            }
            throw new AnonymousClass1V9("missing cert");
        } catch (AnonymousClass1V9 e2) {
            this.A01.APp(e2);
            throw e2;
        }
    }
}
