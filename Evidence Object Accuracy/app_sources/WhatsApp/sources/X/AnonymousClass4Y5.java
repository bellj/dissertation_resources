package X;

/* renamed from: X.4Y5  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass4Y5 {
    public abstract int getCount();

    public abstract Object getElement();

    public boolean equals(Object obj) {
        if (!(obj instanceof AnonymousClass4Y5)) {
            return false;
        }
        AnonymousClass4Y5 r4 = (AnonymousClass4Y5) obj;
        if (getCount() != r4.getCount() || !AnonymousClass28V.A00(getElement(), r4.getElement())) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return C72453ed.A0D(getElement()) ^ getCount();
    }

    public String toString() {
        String valueOf = String.valueOf(getElement());
        int count = getCount();
        if (count == 1) {
            return valueOf;
        }
        StringBuilder A0t = C12980iv.A0t(valueOf.length() + 14);
        A0t.append(valueOf);
        return C12960it.A0e(" x ", A0t, count);
    }
}
