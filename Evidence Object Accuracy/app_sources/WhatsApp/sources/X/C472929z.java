package X;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.Signature;
import android.util.Log;

/* renamed from: X.29z  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C472929z {
    public static C472929z A02;
    public final Context A00;
    public volatile String A01;

    public C472929z(Context context) {
        this.A00 = context.getApplicationContext();
    }

    public static C472929z A00(Context context) {
        C13020j0.A01(context);
        synchronized (C472929z.class) {
            if (A02 == null) {
                synchronized (AnonymousClass4HZ.class) {
                    if (AnonymousClass4HZ.A00 != null) {
                        Log.w("GoogleCertificates", "GoogleCertificates has been initialized already");
                    } else if (context != null) {
                        AnonymousClass4HZ.A00 = context.getApplicationContext();
                    }
                }
                A02 = new C472929z(context);
            }
        }
        return A02;
    }

    public static final boolean A01(PackageInfo packageInfo, boolean z) {
        Signature[] signatureArr;
        if (packageInfo != null && (signatureArr = packageInfo.signatures) != null) {
            AbstractBinderC78743pT[] r3 = z ? C88324Fe.A00 : new AbstractBinderC78743pT[]{C88324Fe.A00[0]};
            if (signatureArr.length != 1) {
                Log.w("GoogleSignatureVerifier", "Package has more than one signature.");
            } else {
                int i = 0;
                BinderC78773pW r1 = new BinderC78773pW(signatureArr[0].toByteArray());
                while (true) {
                    if (i >= r3.length) {
                        break;
                    } else if (!r3[i].equals(r1)) {
                        i++;
                    } else if (r3[i] != null) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
