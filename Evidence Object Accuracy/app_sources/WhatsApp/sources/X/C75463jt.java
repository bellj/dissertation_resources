package X;

import android.view.View;
import com.facebook.redex.ViewOnClickCListenerShape3S0200000_I1_1;
import com.whatsapp.R;
import com.whatsapp.WaButton;
import com.whatsapp.polls.PollResultsViewModel;

/* renamed from: X.3jt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75463jt extends AnonymousClass03U {
    public long A00;
    public final WaButton A01;

    public C75463jt(View view, PollResultsViewModel pollResultsViewModel) {
        super(view);
        WaButton waButton = (WaButton) AnonymousClass028.A0D(view, R.id.poll_results_see_all_button);
        this.A01 = waButton;
        waButton.setOnClickListener(new ViewOnClickCListenerShape3S0200000_I1_1(this, 23, pollResultsViewModel));
    }
}
