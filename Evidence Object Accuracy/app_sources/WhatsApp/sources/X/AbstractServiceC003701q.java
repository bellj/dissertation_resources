package X;

import com.whatsapp.util.Log;

/* renamed from: X.01q  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractServiceC003701q extends AbstractServiceC003801r {
    @Override // X.AbstractServiceC003801r
    public AbstractC12330hk A01() {
        try {
            return super.A01();
        } catch (SecurityException e) {
            if (e.getMessage().contains("Caller no longer running")) {
                StringBuilder sb = new StringBuilder("WaJobIntentService/'Caller no longer running' failure for ");
                sb.append(getClass().getSimpleName());
                Log.e(sb.toString(), e);
                return null;
            }
            throw e;
        }
    }
}
