package X;

import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.RunnableBRunnable0Shape6S0100000_I0_6;
import com.whatsapp.stickers.StickerStoreFeaturedTabFragment;
import com.whatsapp.stickers.StickerStoreTabFragment;
import com.whatsapp.util.Log;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.2B6  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2B6 {
    public void A00(List list) {
        if (this instanceof C621735g) {
            C621735g r5 = (C621735g) this;
            Collections.sort(list, new C38681oX());
            StickerStoreFeaturedTabFragment stickerStoreFeaturedTabFragment = r5.A00;
            ((StickerStoreTabFragment) stickerStoreFeaturedTabFragment).A0E = list;
            stickerStoreFeaturedTabFragment.A08 = false;
            C38671oW r0 = ((StickerStoreTabFragment) stickerStoreFeaturedTabFragment).A0D;
            if (r0 == null) {
                AnonymousClass2V3 r2 = new AnonymousClass2V3(stickerStoreFeaturedTabFragment, list);
                ((StickerStoreTabFragment) stickerStoreFeaturedTabFragment).A0D = r2;
                RecyclerView recyclerView = ((StickerStoreTabFragment) stickerStoreFeaturedTabFragment).A04;
                if (recyclerView != null) {
                    recyclerView.setLayoutFrozen(false);
                    recyclerView.A0j(r2, true, true);
                    recyclerView.A0r(true);
                    recyclerView.requestLayout();
                }
                stickerStoreFeaturedTabFragment.A1B();
            } else {
                r0.A00 = list;
                r0.A02();
            }
            if (((StickerStoreTabFragment) stickerStoreFeaturedTabFragment).A07.A07(1396) && ((StickerStoreTabFragment) stickerStoreFeaturedTabFragment).A08.A01()) {
                ((StickerStoreTabFragment) stickerStoreFeaturedTabFragment).A0C.A0J(new C69813aH(r5), "meta-avatar", false);
            }
            stickerStoreFeaturedTabFragment.A1B();
        } else if (!(this instanceof C621835h)) {
            AnonymousClass2B5 r22 = (AnonymousClass2B5) this;
            list.size();
            int size = list.size();
            AnonymousClass2B7 r3 = r22.A00;
            if (size == 0) {
                C235512c r32 = r3.A0B;
                RunnableBRunnable0Shape6S0100000_I0_6 runnableBRunnable0Shape6S0100000_I0_6 = new RunnableBRunnable0Shape6S0100000_I0_6(r22, 2);
                if (r32.A0J.A02(C14370lK.A0S, 0, 1048576, true, false, false, false)) {
                    r32.A0Y.Aaz(new AnonymousClass376(new C621835h(r32, runnableBRunnable0Shape6S0100000_I0_6), r32), new Object[0]);
                    return;
                }
                Log.i("StickerRepository/downloadInitialPackAsync/autodownload is not safe, going to do nothing");
                return;
            }
            r3.A06.A04.A00.edit().putBoolean("sticker_picker_initial_download", true).apply();
            r3.A00();
        } else {
            C621835h r1 = (C621835h) this;
            Iterator it = list.iterator();
            while (it.hasNext()) {
                AnonymousClass1KZ r33 = (AnonymousClass1KZ) it.next();
                if (r33.A0D.equals("whatsappcuppy")) {
                    C235512c r23 = r1.A00;
                    if (r23.A0J.A02(C14370lK.A0S, 0, r33.A08, true, false, false, false)) {
                        r23.A0H(r33, new AbstractC38651oS(r1.A01) { // from class: X.59X
                            public final /* synthetic */ Runnable A00;

                            {
                                this.A00 = r1;
                            }

                            @Override // X.AbstractC38651oS
                            public final void AWb(AnonymousClass4S5 r34) {
                                Runnable runnable = this.A00;
                                if (r34.A03) {
                                    runnable.run();
                                }
                            }
                        }, 4, false);
                        return;
                    }
                    Log.i("StickerRepository/downloadInitialPackAsync/autodownload is not safe, going to do nothing");
                }
            }
        }
    }
}
