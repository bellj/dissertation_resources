package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0310000_I0;
import com.whatsapp.util.Log;

/* renamed from: X.17w  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C250417w {
    public final C14900mE A00;
    public final C21290xB A01;
    public final C14830m7 A02;
    public final C14820m6 A03;
    public final C20650w6 A04;
    public final C19990v2 A05;
    public final C21320xE A06;
    public final C15650ng A07;
    public final AnonymousClass10Y A08;
    public final C22230yk A09;
    public final C20220vP A0A;

    public C250417w(C14900mE r1, C21290xB r2, C14830m7 r3, C14820m6 r4, C20650w6 r5, C19990v2 r6, C21320xE r7, C15650ng r8, AnonymousClass10Y r9, C22230yk r10, C20220vP r11) {
        this.A02 = r3;
        this.A00 = r1;
        this.A05 = r6;
        this.A04 = r5;
        this.A01 = r2;
        this.A09 = r10;
        this.A07 = r8;
        this.A08 = r9;
        this.A0A = r11;
        this.A03 = r4;
        this.A06 = r7;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0096, code lost:
        if (r13 == 0) goto L_0x0036;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(X.AbstractC14640lm r18, X.AnonymousClass1IS r19, java.lang.Integer r20, boolean r21, boolean r22, boolean r23) {
        /*
            r17 = this;
            r5 = r17
            X.0w6 r10 = r5.A04
            r11 = r18
            boolean r0 = r10.A08(r11)
            if (r0 == 0) goto L_0x0063
            r8 = 0
            r6 = r20
            if (r20 == 0) goto L_0x0034
            X.0v2 r0 = r5.A05
            X.1Tm r1 = r0.A07(r11)
            X.10Y r0 = r5.A08
            X.0mz r0 = r0.A01(r11)
            r7 = r19
            if (r0 == 0) goto L_0x0029
            X.1IS r0 = r0.A0z
            boolean r0 = r0.equals(r7)
            if (r0 != 0) goto L_0x0034
        L_0x0029:
            int r13 = r1.A00
            r9 = -1
            if (r13 != r9) goto L_0x007b
            int r0 = r6.intValue()
            if (r0 != r9) goto L_0x007b
        L_0x0034:
            r12 = r8
            r13 = 0
        L_0x0036:
            if (r22 == 0) goto L_0x003d
            X.0vP r0 = r5.A0A
            r0.A08(r11)
        L_0x003d:
            X.0vP r0 = r5.A0A
            if (r13 == 0) goto L_0x0042
            r8 = r12
        L_0x0042:
            r0.A09(r11, r8)
            r14 = 0
            r16 = r23
            r15 = r21
            r10.A03(r11, r12, r13, r14, r15, r16)
            X.0mE r3 = r5.A00
            r0 = 49
            com.facebook.redex.RunnableBRunnable0Shape2S0200000_I0_2 r2 = new com.facebook.redex.RunnableBRunnable0Shape2S0200000_I0_2
            r2.<init>(r5, r0, r11)
            r0 = 300(0x12c, double:1.48E-321)
            r3.A0J(r2, r0)
            if (r21 == 0) goto L_0x0063
            X.0yk r1 = r5.A09
            r0 = 1
            r1.A06(r11, r0)
        L_0x0063:
            X.0m6 r1 = r5.A03
            X.0m7 r0 = r5.A02
            long r2 = r0.A00()
            android.content.SharedPreferences r0 = r1.A00
            android.content.SharedPreferences$Editor r1 = r0.edit()
            java.lang.String r0 = "last_read_conversation_time"
            android.content.SharedPreferences$Editor r0 = r1.putLong(r0, r2)
            r0.apply()
        L_0x007a:
            return
        L_0x007b:
            java.lang.String r4 = " req="
            java.lang.String r3 = " local="
            java.lang.String r2 = "app/setConversationSeen/qr/invalid  "
            if (r13 <= 0) goto L_0x0099
            int r1 = r6.intValue()
            if (r1 == r9) goto L_0x007a
            X.0ng r0 = r5.A07
            X.0ot r0 = r0.A0K
            X.0mz r12 = r0.A03(r7)
            if (r12 == 0) goto L_0x0099
            if (r13 < r1) goto L_0x0099
            int r13 = r13 - r1
            if (r13 != 0) goto L_0x003d
            goto L_0x0036
        L_0x0099:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r2)
            r0.append(r7)
            r0.append(r3)
            r0.append(r13)
            r0.append(r4)
            r0.append(r6)
            java.lang.String r0 = r0.toString()
            com.whatsapp.util.Log.e(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C250417w.A00(X.0lm, X.1IS, java.lang.Integer, boolean, boolean, boolean):void");
    }

    public void A01(AbstractC14640lm r9, boolean z) {
        C20650w6 r3 = this.A04;
        r3.A01.A00(r9, 8);
        AnonymousClass1PE A06 = r3.A09.A06(r9);
        if (A06 == null) {
            StringBuilder sb = new StringBuilder("msgstore/setchatunseen/nochat/");
            sb.append(r9);
            Log.i(sb.toString());
        } else {
            StringBuilder sb2 = new StringBuilder("msgstore/setchatunseen/");
            sb2.append(r9);
            sb2.append("/");
            sb2.append(A06.A07());
            Log.i(sb2.toString());
            A06.A0C(-1, 0, 0, 0);
            r3.A06.A01(new RunnableBRunnable0Shape0S0310000_I0(r3, r9, A06, 2, z), 1);
        }
        this.A06.A07(r9);
        if (z) {
            this.A09.A06(r9, false);
        }
    }

    public void A02(AbstractC14640lm r8, boolean z, boolean z2) {
        A00(r8, null, null, z, z2, false);
    }
}
