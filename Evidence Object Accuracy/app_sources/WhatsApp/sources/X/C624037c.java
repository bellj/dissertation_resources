package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.mentions.MentionPickerView;

/* renamed from: X.37c  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C624037c extends AbstractC16350or {
    public final C15650ng A00;
    public final UserJid A01;
    public final CharSequence A02;
    public final /* synthetic */ MentionPickerView A03;

    public /* synthetic */ C624037c(C15650ng r1, UserJid userJid, MentionPickerView mentionPickerView, CharSequence charSequence) {
        this.A03 = mentionPickerView;
        this.A00 = r1;
        this.A01 = userJid;
        this.A02 = charSequence;
    }
}
