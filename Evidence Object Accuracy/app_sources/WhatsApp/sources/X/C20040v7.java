package X;

import java.util.Collections;
import java.util.Map;

/* renamed from: X.0v7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20040v7 {
    public final C241714m A00;
    public final C18460sU A01;
    public final C20850wQ A02;
    public final C16490p7 A03;
    public final C21390xL A04;
    public final Map A05 = Collections.synchronizedMap(new C246516i(200));

    public C20040v7(C241714m r3, C18460sU r4, C20850wQ r5, C16490p7 r6, C21390xL r7) {
        this.A01 = r4;
        this.A00 = r3;
        this.A04 = r7;
        this.A03 = r6;
        this.A02 = r5;
    }
}
