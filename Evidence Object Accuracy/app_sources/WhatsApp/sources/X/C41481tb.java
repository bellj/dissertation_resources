package X;

/* renamed from: X.1tb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C41481tb {
    public final int A00;
    public final String A01;

    public C41481tb(int i, String str) {
        this.A00 = i;
        this.A01 = str;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C41481tb)) {
            return false;
        }
        C41481tb r4 = (C41481tb) obj;
        if (this.A00 != r4.A00 || !this.A01.equals(r4.A01)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return ((this.A01.hashCode() + 31) * 31) + this.A00;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("FileDownloadToken{reportType=");
        sb.append(this.A00);
        sb.append(", fileHash='");
        sb.append(this.A01);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }
}
