package X;

/* renamed from: X.23p  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C458923p implements AbstractC14590lg {
    public int A00 = -1;
    public int A01;
    public final /* synthetic */ C38421o4 A02;
    public final /* synthetic */ AnonymousClass109 A03;
    public final /* synthetic */ AnonymousClass1KC A04;

    public C458923p(C38421o4 r2, AnonymousClass109 r3, AnonymousClass1KC r4) {
        this.A03 = r3;
        this.A02 = r2;
        this.A04 = r4;
    }

    @Override // X.AbstractC14590lg
    public /* bridge */ /* synthetic */ void accept(Object obj) {
        Number number = (Number) obj;
        int intValue = number.intValue();
        if (intValue != this.A00) {
            this.A00 = intValue;
            if (intValue >= this.A01 + 5) {
                this.A01 = intValue;
                this.A02.A01.size();
            }
            AnonymousClass109 r3 = this.A03;
            C26751Er r2 = r3.A0F;
            C38421o4 r1 = this.A02;
            if (C26751Er.A00(r1, new AbstractC458623l() { // from class: X.57d
                @Override // X.AbstractC458623l
                public final boolean A67(C16150oX r7, AbstractC16130oV r8, Object obj2) {
                    Number number2 = (Number) obj2;
                    boolean z = false;
                    if (r8.A0y != 1) {
                        long j = r7.A0C;
                        long intValue2 = (long) number2.intValue();
                        if (j != intValue2) {
                            z = true;
                        }
                        r7.A0C = intValue2;
                    }
                    return z;
                }
            }, number)) {
                r3.A09.A09(r1.A01, 8);
            }
        }
    }
}
