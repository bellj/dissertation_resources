package X;

import android.database.Cursor;
import com.whatsapp.util.Log;

/* renamed from: X.0vH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20140vH {
    public final C15570nT A00;
    public final C14830m7 A01;
    public final C16510p9 A02;
    public final C19990v2 A03;
    public final C20290vW A04;
    public final C20850wQ A05;
    public final C16490p7 A06;
    public final AnonymousClass134 A07;
    public final C14850m9 A08;

    public C20140vH(C15570nT r1, C14830m7 r2, C16510p9 r3, C19990v2 r4, C20290vW r5, C20850wQ r6, C16490p7 r7, AnonymousClass134 r8, C14850m9 r9) {
        this.A01 = r2;
        this.A08 = r9;
        this.A00 = r1;
        this.A04 = r5;
        this.A02 = r3;
        this.A07 = r8;
        this.A03 = r4;
        this.A06 = r7;
        this.A05 = r6;
    }

    @Deprecated
    public int A00(long j, long j2) {
        String str;
        int i = 0;
        String[] strArr = {Long.toString(j), Long.toString(j2)};
        C16310on A01 = this.A06.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT COUNT(*) FROM available_message_view WHERE (message_type != '8') AND _id > ? AND _id <= ?", strArr);
            if (A09.moveToNext()) {
                i = A09.getInt(0);
                StringBuilder sb = new StringBuilder();
                sb.append("msgstore/getmessagesatid/pos:");
                sb.append(i);
                str = sb.toString();
            } else {
                str = "msgstore/getmessagesatid/db no messages";
            }
            Log.i(str);
            A09.close();
            A01.close();
            return i;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x003c, code lost:
        if (r2 != null) goto L_0x003e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long A01() {
        /*
            r4 = this;
            X.0p7 r0 = r4.A06
            X.0on r3 = r0.get()
            X.0op r2 = r3.A03     // Catch: all -> 0x0045
            java.lang.String r1 = " SELECT _id FROM available_message_view ORDER BY sort_id DESC LIMIT 1"
            r0 = 0
            android.database.Cursor r2 = r2.A09(r1, r0)     // Catch: all -> 0x0045
            if (r2 == 0) goto L_0x003a
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: all -> 0x0035
            r1.<init>()     // Catch: all -> 0x0035
            java.lang.String r0 = "msgstore/lastmsgid/count "
            r1.append(r0)     // Catch: all -> 0x0035
            int r0 = r2.getCount()     // Catch: all -> 0x0035
            r1.append(r0)     // Catch: all -> 0x0035
            java.lang.String r0 = r1.toString()     // Catch: all -> 0x0035
            com.whatsapp.util.Log.i(r0)     // Catch: all -> 0x0035
            boolean r0 = r2.moveToNext()     // Catch: all -> 0x0035
            if (r0 == 0) goto L_0x003a
            r0 = 0
            long r0 = r2.getLong(r0)     // Catch: all -> 0x0035
            goto L_0x003e
        L_0x0035:
            r0 = move-exception
            r2.close()     // Catch: all -> 0x0039
        L_0x0039:
            throw r0     // Catch: all -> 0x0045
        L_0x003a:
            r0 = 1
            if (r2 == 0) goto L_0x0041
        L_0x003e:
            r2.close()     // Catch: all -> 0x0045
        L_0x0041:
            r3.close()
            return r0
        L_0x0045:
            r0 = move-exception
            r3.close()     // Catch: all -> 0x0049
        L_0x0049:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20140vH.A01():long");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x004a, code lost:
        if (r2 != null) goto L_0x004c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long A02(X.AbstractC14640lm r6) {
        /*
            r5 = this;
            r0 = 1
            java.lang.String[] r2 = new java.lang.String[r0]
            X.0p9 r0 = r5.A02
            long r0 = r0.A02(r6)
            java.lang.String r0 = java.lang.String.valueOf(r0)
            r4 = 0
            r2[r4] = r0
            X.0p7 r0 = r5.A06
            X.0on r3 = r0.get()
            X.0op r1 = r3.A03     // Catch: all -> 0x0053
            java.lang.String r0 = "   SELECT _id FROM available_message_view WHERE chat_row_id = ? ORDER BY sort_id DESC LIMIT 1"
            android.database.Cursor r2 = r1.A09(r0, r2)     // Catch: all -> 0x0053
            if (r2 == 0) goto L_0x0048
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: all -> 0x0043
            r1.<init>()     // Catch: all -> 0x0043
            java.lang.String r0 = "msgstore/lastmsgid/count "
            r1.append(r0)     // Catch: all -> 0x0043
            int r0 = r2.getCount()     // Catch: all -> 0x0043
            r1.append(r0)     // Catch: all -> 0x0043
            java.lang.String r0 = r1.toString()     // Catch: all -> 0x0043
            com.whatsapp.util.Log.i(r0)     // Catch: all -> 0x0043
            boolean r0 = r2.moveToNext()     // Catch: all -> 0x0043
            if (r0 == 0) goto L_0x0048
            long r0 = r2.getLong(r4)     // Catch: all -> 0x0043
            goto L_0x004c
        L_0x0043:
            r0 = move-exception
            r2.close()     // Catch: all -> 0x0047
        L_0x0047:
            throw r0     // Catch: all -> 0x0053
        L_0x0048:
            r0 = 1
            if (r2 == 0) goto L_0x004f
        L_0x004c:
            r2.close()     // Catch: all -> 0x0053
        L_0x004f:
            r3.close()
            return r0
        L_0x0053:
            r0 = move-exception
            r3.close()     // Catch: all -> 0x0057
        L_0x0057:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20140vH.A02(X.0lm):long");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x003a, code lost:
        if (r2 != null) goto L_0x003c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long A03(X.AbstractC14640lm r6, int r7) {
        /*
            r5 = this;
            r0 = 2
            java.lang.String[] r2 = new java.lang.String[r0]
            X.0p9 r0 = r5.A02
            long r0 = r0.A02(r6)
            java.lang.String r0 = java.lang.String.valueOf(r0)
            r4 = 0
            r2[r4] = r0
            r1 = 1
            int r7 = r7 + r1
            java.lang.String r0 = java.lang.String.valueOf(r7)
            r2[r1] = r0
            X.0p7 r0 = r5.A06
            X.0on r3 = r0.get()
            X.0op r1 = r3.A03     // Catch: all -> 0x0043
            java.lang.String r0 = " SELECT _id FROM available_message_view WHERE chat_row_id = ? AND message_type != 7 ORDER BY sort_id DESC LIMIT ?"
            android.database.Cursor r2 = r1.A09(r0, r2)     // Catch: all -> 0x0043
            if (r2 == 0) goto L_0x0038
            boolean r0 = r2.moveToLast()     // Catch: all -> 0x0033
            if (r0 == 0) goto L_0x0038
            long r0 = r2.getLong(r4)     // Catch: all -> 0x0033
            goto L_0x003c
        L_0x0033:
            r0 = move-exception
            r2.close()     // Catch: all -> 0x0037
        L_0x0037:
            throw r0     // Catch: all -> 0x0043
        L_0x0038:
            r0 = 1
            if (r2 == 0) goto L_0x003f
        L_0x003c:
            r2.close()     // Catch: all -> 0x0043
        L_0x003f:
            r3.close()
            return r0
        L_0x0043:
            r0 = move-exception
            r3.close()     // Catch: all -> 0x0047
        L_0x0047:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20140vH.A03(X.0lm, int):long");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0080, code lost:
        if (r1 != null) goto L_0x0082;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long A04(X.AbstractC14640lm r9, int[] r10, long r11) {
        /*
            r8 = this;
            r0 = 0
            X.1Ma r5 = new X.1Ma
            r5.<init>(r0)
            java.lang.String r0 = "rowidstore/getRowIdByTimestampExcludeTypes"
            r5.A04(r0)
            java.lang.String r0 = "SELECT _id FROM available_message_view WHERE chat_row_id = ? AND "
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>(r0)
            int r0 = r10.length
            java.lang.String r2 = " AND "
            if (r0 == 0) goto L_0x004c
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r0 = X.C242214r.A00(r10)
            r1.append(r0)
            r1.append(r2)
            java.lang.String r0 = r1.toString()
        L_0x002a:
            r3.append(r0)
            java.lang.String r0 = "timestamp > 0"
            r3.append(r0)
            r3.append(r2)
            java.lang.String r0 = "timestamp <= ?"
            r3.append(r0)
            java.lang.String r0 = " ORDER BY sort_id DESC LIMIT 1"
            r3.append(r0)
            java.lang.String r7 = r3.toString()
            X.0p7 r0 = r8.A06
            X.0on r4 = r0.get()
            goto L_0x004f
        L_0x004c:
            java.lang.String r0 = ""
            goto L_0x002a
        L_0x004f:
            X.0op r6 = r4.A03     // Catch: all -> 0x00a6
            r0 = 2
            java.lang.String[] r3 = new java.lang.String[r0]     // Catch: all -> 0x00a6
            X.0p9 r0 = r8.A02     // Catch: all -> 0x00a6
            long r0 = r0.A02(r9)     // Catch: all -> 0x00a6
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch: all -> 0x00a6
            r2 = 0
            r3[r2] = r0     // Catch: all -> 0x00a6
            r1 = 1
            java.lang.String r0 = java.lang.Long.toString(r11)     // Catch: all -> 0x00a6
            r3[r1] = r0     // Catch: all -> 0x00a6
            android.database.Cursor r1 = r6.A09(r7, r3)     // Catch: all -> 0x00a6
            if (r1 == 0) goto L_0x007e
            boolean r0 = r1.moveToNext()     // Catch: all -> 0x0079
            if (r0 == 0) goto L_0x007e
            long r2 = r1.getLong(r2)     // Catch: all -> 0x0079
            goto L_0x0082
        L_0x0079:
            r0 = move-exception
            r1.close()     // Catch: all -> 0x007d
        L_0x007d:
            throw r0     // Catch: all -> 0x00a6
        L_0x007e:
            r2 = 0
            if (r1 == 0) goto L_0x0085
        L_0x0082:
            r1.close()     // Catch: all -> 0x00a6
        L_0x0085:
            r4.close()
            java.lang.String r0 = "rowidstore/getRowIdByTimestampExcludeTypes "
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>(r0)
            r4.append(r2)
            java.lang.String r0 = " | time spent:"
            r4.append(r0)
            long r0 = r5.A01()
            r4.append(r0)
            java.lang.String r0 = r4.toString()
            com.whatsapp.util.Log.i(r0)
            return r2
        L_0x00a6:
            r0 = move-exception
            r4.close()     // Catch: all -> 0x00aa
        L_0x00aa:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20140vH.A04(X.0lm, int[], long):long");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002d, code lost:
        if (r1 != null) goto L_0x002f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A05(X.AbstractC14640lm r6) {
        /*
            r5 = this;
            r4 = 1
            java.lang.String[] r3 = new java.lang.String[r4]
            X.0p9 r0 = r5.A02
            long r0 = r0.A02(r6)
            java.lang.String r1 = java.lang.String.valueOf(r0)
            r0 = 0
            r3[r0] = r1
            X.0p7 r0 = r5.A06
            X.0on r2 = r0.get()
            X.0op r1 = r2.A03     // Catch: all -> 0x0036
            java.lang.String r0 = "SELECT  1 FROM available_message_view WHERE chat_row_id = ? AND message_type != ('7') AND from_me = 1 LIMIT 1"
            android.database.Cursor r1 = r1.A09(r0, r3)     // Catch: all -> 0x0036
            if (r1 == 0) goto L_0x002c
            int r0 = r1.getCount()     // Catch: all -> 0x0027
            if (r0 <= 0) goto L_0x002c
            goto L_0x002f
        L_0x0027:
            r0 = move-exception
            r1.close()     // Catch: all -> 0x002b
        L_0x002b:
            throw r0     // Catch: all -> 0x0036
        L_0x002c:
            r4 = 0
            if (r1 == 0) goto L_0x0032
        L_0x002f:
            r1.close()     // Catch: all -> 0x0036
        L_0x0032:
            r2.close()
            return r4
        L_0x0036:
            r0 = move-exception
            r2.close()     // Catch: all -> 0x003a
        L_0x003a:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20140vH.A05(X.0lm):boolean");
    }
}
