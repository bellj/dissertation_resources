package X;

/* renamed from: X.3Hr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64983Hr {
    public static Number A00(double d) {
        int i = (int) d;
        if (((double) i) == d) {
            return Integer.valueOf(i);
        }
        return Double.valueOf(d);
    }

    public static Number A01(float f) {
        return A00((double) f);
    }

    public static boolean A02(Object obj) {
        String obj2;
        if (obj instanceof Number) {
            return C12960it.A1S(C12960it.A05(obj));
        }
        if (obj instanceof Boolean) {
            return C12970iu.A1Y(obj);
        }
        StringBuilder A0k = C12960it.A0k("Expected Number or Boolean: ");
        if (obj == null) {
            obj2 = "null";
        } else {
            obj2 = obj.toString();
        }
        throw C12970iu.A0f(C12960it.A0d(obj2, A0k));
    }
}
