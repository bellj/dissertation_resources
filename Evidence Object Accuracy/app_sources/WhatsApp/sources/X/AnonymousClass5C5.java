package X;

import java.util.Comparator;

/* renamed from: X.5C5  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5C5 implements Comparator {
    @Override // java.util.Comparator
    public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
        return obj.getClass().getCanonicalName().compareTo(obj2.getClass().getCanonicalName());
    }
}
