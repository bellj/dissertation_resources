package X;

import android.net.Uri;
import com.whatsapp.R;
import com.whatsapp.util.Log;

/* renamed from: X.5bI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C118115bI extends AnonymousClass015 {
    public final AnonymousClass016 A00 = new AnonymousClass016(AnonymousClass617.A00(null));
    public final C18640sm A01;
    public final C14830m7 A02;
    public final C18600si A03;
    public final C18610sj A04;
    public final C22710zW A05;
    public final C17070qD A06;
    public final C128405w3 A07;
    public final C18590sh A08;

    public C118115bI(Uri uri, C18640sm r8, C14830m7 r9, C18600si r10, C18610sj r11, C22710zW r12, C17070qD r13, C128405w3 r14, C18590sh r15) {
        String lastPathSegment = uri.getLastPathSegment();
        this.A02 = r9;
        this.A07 = r14;
        this.A08 = r15;
        this.A06 = r13;
        this.A03 = r10;
        this.A04 = r11;
        this.A05 = r12;
        this.A01 = r8;
        String A01 = r15.A01();
        AnonymousClass1W9[] r5 = new AnonymousClass1W9[2];
        boolean A1a = C12990iw.A1a("action", "verify-deep-link", r5);
        r5[1] = new AnonymousClass1W9("device-id", A01);
        AnonymousClass1W9[] r2 = new AnonymousClass1W9[1];
        C12960it.A1M("payload", lastPathSegment, r2, A1a ? 1 : 0);
        r11.A0E(new AnonymousClass6DJ(this), new AnonymousClass1V8(new AnonymousClass1V8("link", r2), "account", r5), "get");
    }

    public final void A04(Throwable th, int i) {
        Log.e(C12960it.A0W(i, "PAY ViralityLinkViewModel verifyInviteCode on ErrorCode : "), th);
        AnonymousClass016 r5 = this.A00;
        C128405w3 r1 = this.A07;
        int i2 = R.string.virality_payments_not_enabled_title;
        if (i == 405) {
            i2 = R.string.virality_payments_incorrect_app_title;
        }
        int A00 = r1.A00(i);
        int i3 = R.string.cancel;
        if (i == 405) {
            i3 = R.string.ok;
        }
        r5.A0A(AnonymousClass617.A02(new C127775v2(i2, A00, i3, 0), th));
    }
}
