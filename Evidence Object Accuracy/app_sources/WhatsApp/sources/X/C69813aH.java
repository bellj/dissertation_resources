package X;

import com.whatsapp.stickers.StickerStoreFeaturedTabFragment;
import com.whatsapp.stickers.StickerStoreTabFragment;
import com.whatsapp.util.Log;

/* renamed from: X.3aH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69813aH implements AbstractC33611ef {
    public final /* synthetic */ C621735g A00;

    public C69813aH(C621735g r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC33611ef
    public void AWg(AnonymousClass1KZ r4) {
        StickerStoreFeaturedTabFragment stickerStoreFeaturedTabFragment = this.A00.A00;
        ((StickerStoreTabFragment) stickerStoreFeaturedTabFragment).A0E.add(0, r4);
        ((StickerStoreTabFragment) stickerStoreFeaturedTabFragment).A0D.A04(0);
    }

    @Override // X.AbstractC33611ef
    public void AWi() {
        Log.e("StickerStoreFeaturedTabFragment/updatePackList: The avatar config is true but the avatar sticker pack is not available!");
    }
}
