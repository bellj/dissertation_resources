package X;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.concurrent.Executor;

/* renamed from: X.4Lj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C89784Lj {
    public final Deque A00 = new ArrayDeque();
    public final Executor A01;

    public C89784Lj(Executor executor) {
        this.A01 = executor;
    }
}
