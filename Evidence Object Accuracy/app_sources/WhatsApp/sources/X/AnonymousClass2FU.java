package X;

import com.whatsapp.contact.picker.ContactPicker;
import com.whatsapp.nativelibloader.WhatsAppLibLoader;

/* renamed from: X.2FU  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2FU extends AbstractActivityC28171Kz {
    public boolean A00 = false;

    public AnonymousClass2FU() {
        A0R(new C102974q0(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            ContactPicker contactPicker = (ContactPicker) this;
            AnonymousClass2FL r3 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r2 = r3.A1E;
            ((ActivityC13830kP) contactPicker).A05 = (AbstractC14440lR) r2.ANe.get();
            ((ActivityC13810kN) contactPicker).A0C = (C14850m9) r2.A04.get();
            ((ActivityC13810kN) contactPicker).A05 = (C14900mE) r2.A8X.get();
            ((ActivityC13810kN) contactPicker).A03 = (AbstractC15710nm) r2.A4o.get();
            ((ActivityC13810kN) contactPicker).A04 = (C14330lG) r2.A7B.get();
            ((ActivityC13810kN) contactPicker).A0B = (AnonymousClass19M) r2.A6R.get();
            ((ActivityC13810kN) contactPicker).A0A = (C18470sV) r2.AK8.get();
            ((ActivityC13810kN) contactPicker).A06 = (C15450nH) r2.AII.get();
            ((ActivityC13810kN) contactPicker).A08 = (AnonymousClass01d) r2.ALI.get();
            ((ActivityC13810kN) contactPicker).A0D = (C18810t5) r2.AMu.get();
            ((ActivityC13810kN) contactPicker).A09 = (C14820m6) r2.AN3.get();
            ((ActivityC13810kN) contactPicker).A07 = (C18640sm) r2.A3u.get();
            ((ActivityC13790kL) contactPicker).A05 = (C14830m7) r2.ALb.get();
            ((ActivityC13790kL) contactPicker).A0D = (C252718t) r2.A9K.get();
            ((ActivityC13790kL) contactPicker).A01 = (C15570nT) r2.AAr.get();
            ((ActivityC13790kL) contactPicker).A04 = (C15810nw) r2.A73.get();
            ((ActivityC13790kL) contactPicker).A09 = r3.A06();
            ((ActivityC13790kL) contactPicker).A06 = (C14950mJ) r2.AKf.get();
            ((ActivityC13790kL) contactPicker).A00 = (AnonymousClass12P) r2.A0H.get();
            ((ActivityC13790kL) contactPicker).A02 = (C252818u) r2.AMy.get();
            ((ActivityC13790kL) contactPicker).A03 = (C22670zS) r2.A0V.get();
            ((ActivityC13790kL) contactPicker).A0A = (C21840y4) r2.ACr.get();
            ((ActivityC13790kL) contactPicker).A07 = (C15880o3) r2.ACF.get();
            ((ActivityC13790kL) contactPicker).A0C = (C21820y2) r2.AHx.get();
            ((ActivityC13790kL) contactPicker).A0B = (C15510nN) r2.AHZ.get();
            ((ActivityC13790kL) contactPicker).A08 = (C249317l) r2.A8B.get();
            ((AbstractActivityC28171Kz) contactPicker).A05 = (C20650w6) r2.A3A.get();
            ((AbstractActivityC28171Kz) contactPicker).A09 = (C18470sV) r2.AK8.get();
            ((AbstractActivityC28171Kz) contactPicker).A02 = (C20670w8) r2.AMw.get();
            ((AbstractActivityC28171Kz) contactPicker).A03 = (C15550nR) r2.A45.get();
            ((AbstractActivityC28171Kz) contactPicker).A0B = (C19890uq) r2.ABw.get();
            ((AbstractActivityC28171Kz) contactPicker).A0A = (C20710wC) r2.A8m.get();
            ((AbstractActivityC28171Kz) contactPicker).A0E = (AbstractC15850o0) r2.ANA.get();
            ((AbstractActivityC28171Kz) contactPicker).A04 = (C17050qB) r2.ABL.get();
            ((AbstractActivityC28171Kz) contactPicker).A0C = (C20740wF) r2.AIA.get();
            ((AbstractActivityC28171Kz) contactPicker).A0D = (C18350sJ) r2.AHX.get();
            ((AbstractActivityC28171Kz) contactPicker).A06 = (C20820wN) r2.ACG.get();
            ((AbstractActivityC28171Kz) contactPicker).A08 = (C26041Bu) r2.ACL.get();
            ((AbstractActivityC28171Kz) contactPicker).A00 = (AnonymousClass2FR) r3.A0P.get();
            ((AbstractActivityC28171Kz) contactPicker).A07 = (C20850wQ) r2.ACI.get();
            contactPicker.A01 = (C239613r) r2.AI9.get();
            contactPicker.A00 = (C20640w5) r2.AHk.get();
            contactPicker.A02 = (C16170oZ) r2.AM4.get();
            contactPicker.A06 = (WhatsAppLibLoader) r2.ANa.get();
        }
    }
}
