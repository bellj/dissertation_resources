package X;

import com.whatsapp.community.CommunityDeleteDialogFragment;
import java.lang.ref.WeakReference;

/* renamed from: X.37T  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass37T extends AbstractC16350or {
    public final /* synthetic */ long A00;
    public final /* synthetic */ CommunityDeleteDialogFragment A01;
    public final /* synthetic */ C15370n3 A02;
    public final /* synthetic */ WeakReference A03;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass37T(AbstractC001200n r1, CommunityDeleteDialogFragment communityDeleteDialogFragment, C15370n3 r3, WeakReference weakReference, long j) {
        super(r1);
        this.A01 = communityDeleteDialogFragment;
        this.A02 = r3;
        this.A00 = j;
        this.A03 = weakReference;
    }
}
