package X;

/* renamed from: X.2Vt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C51652Vt extends AnonymousClass2Vu {
    public C51652Vt(Object obj, int i) {
        super(obj, i);
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass2Vu r5 = (AnonymousClass2Vu) obj;
            if (this.A00 != r5.A00 || !this.A01.equals(r5.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return (this.A01.hashCode() * 31) + this.A00;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("[type: ");
        sb.append(this.A00);
        sb.append(", data: ");
        sb.append(this.A01);
        sb.append("]");
        return sb.toString();
    }
}
