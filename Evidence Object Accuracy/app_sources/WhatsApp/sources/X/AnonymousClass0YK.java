package X;

import android.os.Bundle;

/* renamed from: X.0YK  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0YK implements AbstractC11790gs {
    public final AbstractC11790gs A00;
    public final AbstractC009904y A01;
    public final AnonymousClass054 A02;

    public AnonymousClass0YK(AbstractC11790gs r1, AbstractC009904y r2, AnonymousClass054 r3) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
    }

    @Override // X.AbstractC11790gs
    public void AQn(String str, Bundle bundle) {
        this.A00.AQn(str, bundle);
    }
}
