package X;

import java.util.Arrays;
import java.util.Vector;

/* renamed from: X.5GA  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5GA implements AbstractC117285Zg {
    public int A00;
    public int A01;
    public int A02;
    public long A03;
    public long A04;
    public Vector A05;
    public AnonymousClass5XE A06;
    public AnonymousClass5XE A07;
    public boolean A08;
    public byte[] A09;
    public byte[] A0A = null;
    public byte[] A0B;
    public byte[] A0C;
    public byte[] A0D;
    public byte[] A0E = new byte[16];
    public byte[] A0F = new byte[16];
    public byte[] A0G = new byte[24];
    public byte[] A0H;
    public byte[] A0I;
    public byte[] A0J;
    public byte[] A0K;
    public byte[] A0L;

    @Override // X.AnonymousClass5XQ
    public byte[] AE1() {
        byte[] bArr = this.A0K;
        return bArr == null ? new byte[this.A01] : AnonymousClass1TT.A02(bArr);
    }

    @Override // X.AnonymousClass5XQ
    public int AEp(int i) {
        int i2 = i + this.A02;
        boolean z = this.A08;
        int i3 = this.A01;
        if (z) {
            return i2 + i3;
        }
        int i4 = i2 - i3;
        if (i2 < i3) {
            return 0;
        }
        return i4;
    }

    @Override // X.AbstractC117285Zg
    public AnonymousClass5XE AHO() {
        return this.A07;
    }

    @Override // X.AnonymousClass5XQ
    public int AHQ(int i) {
        int i2 = i + this.A02;
        if (!this.A08) {
            int i3 = this.A01;
            i2 -= i3;
            if (i2 < i3) {
                return 0;
            }
        }
        return i2 - (i2 % 16);
    }

    public AnonymousClass5GA(AnonymousClass5XE r3, AnonymousClass5XE r4) {
        if (r3.AAt() != 16) {
            throw C12970iu.A0f("'hashCipher' must have a block size of 16");
        } else if (r4.AAt() != 16) {
            throw C12970iu.A0f("'mainCipher' must have a block size of 16");
        } else if (r3.AAf().equals(r4.AAf())) {
            this.A06 = r3;
            this.A07 = r4;
        } else {
            throw C12970iu.A0f("'hashCipher' and 'mainCipher' must be the same algorithm");
        }
    }

    public static void A00(byte[] bArr, byte[] bArr2) {
        int i = 15;
        do {
            C72463ee.A0P(bArr[i], bArr, bArr2[i], i);
            i--;
        } while (i >= 0);
    }

    public static byte[] A01(byte[] bArr) {
        byte[] bArr2 = new byte[16];
        int i = 16;
        int i2 = 0;
        while (true) {
            i--;
            if (i >= 0) {
                int i3 = bArr[i] & 255;
                C72463ee.A0O(i2, bArr2, i3 << 1, i);
                i2 = (i3 >>> 7) & 1;
            } else {
                C72463ee.A0P(135 >>> ((1 - i2) << 3), bArr2, bArr2[15], 15);
                return bArr2;
            }
        }
    }

    @Override // X.AnonymousClass5XQ
    public int A97(byte[] bArr, int i) {
        byte[] bArr2;
        if (!this.A08) {
            int i2 = this.A02;
            int i3 = this.A01;
            if (i2 >= i3) {
                int i4 = i2 - i3;
                this.A02 = i4;
                bArr2 = new byte[i3];
                System.arraycopy(this.A0L, i4, bArr2, 0, i3);
            } else {
                throw new C114965Nt("data too short");
            }
        } else {
            bArr2 = null;
        }
        int i5 = this.A00;
        if (i5 > 0) {
            byte[] bArr3 = this.A0I;
            byte b = Byte.MIN_VALUE;
            while (true) {
                bArr3[i5] = b;
                i5++;
                if (i5 >= 16) {
                    break;
                }
                b = 0;
            }
            byte[] bArr4 = this.A0B;
            byte[] bArr5 = this.A0D;
            A00(bArr5, bArr4);
            A00(bArr3, bArr5);
            this.A06.AZY(bArr3, bArr3, 0, 0);
            A00(this.A0H, this.A0I);
        }
        int i6 = this.A02;
        if (i6 > 0) {
            if (this.A08) {
                byte[] bArr6 = this.A0L;
                byte b2 = Byte.MIN_VALUE;
                while (true) {
                    bArr6[i6] = b2;
                    i6++;
                    if (i6 >= 16) {
                        break;
                    }
                    b2 = 0;
                }
                A00(this.A09, bArr6);
            }
            byte[] bArr7 = this.A0E;
            A00(bArr7, this.A0B);
            byte[] bArr8 = new byte[16];
            this.A06.AZY(bArr7, bArr8, 0, 0);
            byte[] bArr9 = this.A0L;
            A00(bArr9, bArr8);
            int length = bArr.length;
            int i7 = this.A02;
            if (length >= i + i7) {
                System.arraycopy(bArr9, 0, bArr, i, i7);
                if (!this.A08) {
                    byte[] bArr10 = this.A0L;
                    int i8 = this.A02;
                    byte b3 = Byte.MIN_VALUE;
                    while (true) {
                        bArr10[i8] = b3;
                        i8++;
                        if (i8 >= 16) {
                            break;
                        }
                        b3 = 0;
                    }
                    A00(this.A09, bArr10);
                }
            } else {
                throw new C114975Nu("Output buffer too short");
            }
        }
        byte[] bArr11 = this.A09;
        byte[] bArr12 = this.A0E;
        A00(bArr11, bArr12);
        A00(bArr11, this.A0C);
        AnonymousClass5XE r6 = this.A06;
        r6.AZY(bArr11, bArr11, 0, 0);
        byte[] bArr13 = this.A09;
        A00(bArr13, this.A0H);
        int i9 = this.A01;
        byte[] bArr14 = new byte[i9];
        this.A0K = bArr14;
        System.arraycopy(bArr13, 0, bArr14, 0, i9);
        int i10 = this.A02;
        if (this.A08) {
            int length2 = bArr.length;
            int i11 = i + i10;
            int i12 = this.A01;
            if (length2 >= i11 + i12) {
                System.arraycopy(this.A0K, 0, bArr, i11, i12);
                i10 += this.A01;
            } else {
                throw new C114975Nu("Output buffer too short");
            }
        } else if (!AnonymousClass1TT.A01(this.A0K, bArr2)) {
            throw new C114965Nt("mac check in OCB failed");
        }
        r6.reset();
        this.A07.reset();
        byte[] bArr15 = this.A0I;
        if (bArr15 != null) {
            Arrays.fill(bArr15, (byte) 0);
        }
        byte[] bArr16 = this.A0L;
        if (bArr16 != null) {
            Arrays.fill(bArr16, (byte) 0);
        }
        this.A00 = 0;
        this.A02 = 0;
        this.A03 = 0;
        this.A04 = 0;
        byte[] bArr17 = this.A0D;
        if (bArr17 != null) {
            Arrays.fill(bArr17, (byte) 0);
        }
        byte[] bArr18 = this.A0H;
        if (bArr18 != null) {
            Arrays.fill(bArr18, (byte) 0);
        }
        System.arraycopy(this.A0F, 0, bArr12, 0, 16);
        byte[] bArr19 = this.A09;
        if (bArr19 != null) {
            Arrays.fill(bArr19, (byte) 0);
        }
        byte[] bArr20 = this.A0J;
        if (bArr20 != null) {
            AZX(bArr20, 0, bArr20.length);
        }
        return i10;
    }

    @Override // X.AnonymousClass5XQ
    public String AAf() {
        StringBuilder A0h = C12960it.A0h();
        C72463ee.A0U(A0h, this.A07);
        return C12960it.A0d("/OCB", A0h);
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0104  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0109  */
    /* JADX WARNING: Removed duplicated region for block: B:53:? A[RETURN, SYNTHETIC] */
    @Override // X.AnonymousClass5XQ
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AIf(X.AnonymousClass20L r11, boolean r12) {
        /*
        // Method dump skipped, instructions count: 316
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5GA.AIf(X.20L, boolean):void");
    }

    @Override // X.AnonymousClass5XQ
    public void AZX(byte[] bArr, int i, int i2) {
        Vector vector;
        for (int i3 = 0; i3 < i2; i3++) {
            byte[] bArr2 = this.A0I;
            int i4 = this.A00;
            C72463ee.A0X(bArr, bArr2, i + i3, i4);
            int i5 = i4 + 1;
            this.A00 = i5;
            if (i5 == bArr2.length) {
                long j = this.A03 + 1;
                this.A03 = j;
                int i6 = 0;
                if (j == 0) {
                    i6 = 64;
                } else {
                    while ((1 & j) == 0) {
                        i6++;
                        j >>>= 1;
                    }
                }
                while (true) {
                    int size = this.A05.size();
                    vector = this.A05;
                    if (i6 < size) {
                        break;
                    }
                    vector.addElement(A01((byte[]) vector.lastElement()));
                }
                byte[] bArr3 = this.A0D;
                A00(bArr3, (byte[]) vector.elementAt(i6));
                byte[] bArr4 = this.A0I;
                A00(bArr4, bArr3);
                this.A06.AZY(bArr4, bArr4, 0, 0);
                A00(this.A0H, this.A0I);
                this.A00 = 0;
            }
        }
    }

    @Override // X.AnonymousClass5XQ
    public int AZZ(byte[] bArr, int i, int i2, byte[] bArr2, int i3) {
        Vector vector;
        if (bArr.length >= i + i2) {
            int i4 = 0;
            for (int i5 = 0; i5 < i2; i5++) {
                byte[] bArr3 = this.A0L;
                int i6 = this.A02;
                C72463ee.A0X(bArr, bArr3, i + i5, i6);
                int i7 = i6 + 1;
                this.A02 = i7;
                if (i7 == bArr3.length) {
                    int i8 = i3 + i4;
                    if (bArr2.length >= i8 + 16) {
                        if (this.A08) {
                            A00(this.A09, bArr3);
                            this.A02 = 0;
                        }
                        byte[] bArr4 = this.A0E;
                        long j = this.A04 + 1;
                        this.A04 = j;
                        int i9 = 0;
                        if (j == 0) {
                            i9 = 64;
                        } else {
                            while ((1 & j) == 0) {
                                i9++;
                                j >>>= 1;
                            }
                        }
                        while (true) {
                            int size = this.A05.size();
                            vector = this.A05;
                            if (i9 < size) {
                                break;
                            }
                            vector.addElement(A01((byte[]) vector.lastElement()));
                        }
                        A00(bArr4, (byte[]) vector.elementAt(i9));
                        byte[] bArr5 = this.A0L;
                        A00(bArr5, bArr4);
                        this.A07.AZY(bArr5, bArr5, 0, 0);
                        byte[] bArr6 = this.A0L;
                        A00(bArr6, bArr4);
                        System.arraycopy(bArr6, 0, bArr2, i8, 16);
                        if (!this.A08) {
                            byte[] bArr7 = this.A09;
                            byte[] bArr8 = this.A0L;
                            A00(bArr7, bArr8);
                            System.arraycopy(bArr8, 16, bArr8, 0, this.A01);
                            this.A02 = this.A01;
                        }
                        i4 += 16;
                    } else {
                        throw new C114975Nu("Output buffer too short");
                    }
                }
            }
            return i4;
        }
        throw new AnonymousClass5O2("Input buffer too short");
    }
}
