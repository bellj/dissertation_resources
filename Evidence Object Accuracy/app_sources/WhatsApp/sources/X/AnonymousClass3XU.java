package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0101000_I0;
import com.whatsapp.registration.report.BanReportViewModel;
import java.io.IOException;

/* renamed from: X.3XU  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3XU implements AbstractC44401yr {
    public final /* synthetic */ AnonymousClass3XW A00;

    public AnonymousClass3XU(AnonymousClass3XW r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC44401yr
    public void A6t(AnonymousClass23Z r7) {
        AnonymousClass3XW r0 = this.A00;
        BanReportViewModel banReportViewModel = r0.A01;
        int i = r0.A00 + 1;
        C12970iu.A0E().postDelayed(new RunnableBRunnable0Shape0S0101000_I0(banReportViewModel, i, 20), ((long) i) * 5000);
    }

    @Override // X.AbstractC44401yr
    public void AOz(IOException iOException) {
        this.A00.A01.A02.A0A(C12960it.A0V());
    }

    @Override // X.AbstractC44401yr
    public void APp(Exception exc) {
        this.A00.A01.A02.A0A(C12960it.A0V());
    }
}
