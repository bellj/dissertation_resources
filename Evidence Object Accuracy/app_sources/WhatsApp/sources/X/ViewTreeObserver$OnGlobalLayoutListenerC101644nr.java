package X;

import android.graphics.drawable.Drawable;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

/* renamed from: X.4nr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC101644nr implements ViewTreeObserver.OnGlobalLayoutListener {
    public final ViewGroup A00;

    public ViewTreeObserver$OnGlobalLayoutListenerC101644nr(ViewGroup viewGroup) {
        this.A00 = viewGroup;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        ViewGroup viewGroup = this.A00;
        C12980iv.A1E(viewGroup, this);
        Drawable background = viewGroup.getBackground();
        if (background instanceof AnonymousClass2Zg) {
            AnonymousClass2Zg.A00(((AnonymousClass2Zg) background).A01, viewGroup);
        }
    }
}
