package X;

/* renamed from: X.091  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass091 implements AnonymousClass02B {
    public int A00 = -1;
    public final AnonymousClass017 A01;
    public final AnonymousClass02B A02;

    public AnonymousClass091(AnonymousClass017 r2, AnonymousClass02B r3) {
        this.A01 = r2;
        this.A02 = r3;
    }

    @Override // X.AnonymousClass02B
    public void ANq(Object obj) {
        int i = this.A00;
        int i2 = this.A01.A01;
        if (i != i2) {
            this.A00 = i2;
            this.A02.ANq(obj);
        }
    }
}
