package X;

/* renamed from: X.4PP  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4PP {
    public final AnonymousClass4DS A00;
    public final AnonymousClass4IV A01;
    public final boolean A02;

    public AnonymousClass4PP(AnonymousClass4DS r1, AnonymousClass4IV r2, boolean z) {
        this.A01 = r2;
        this.A02 = z;
        this.A00 = r1;
    }
}
