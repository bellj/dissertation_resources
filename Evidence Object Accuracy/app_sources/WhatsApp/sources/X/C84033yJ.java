package X;

import com.whatsapp.backup.google.RestoreFromBackupActivity;
import com.whatsapp.util.Log;

/* renamed from: X.3yJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C84033yJ extends AbstractC44761zV {
    public final /* synthetic */ RestoreFromBackupActivity A00;

    @Override // X.AbstractC44761zV
    public String toString() {
        return "one-time-setup-condition";
    }

    public C84033yJ(RestoreFromBackupActivity restoreFromBackupActivity) {
        this.A00 = restoreFromBackupActivity;
    }

    @Override // X.AbstractC44761zV
    public boolean A00() {
        RestoreFromBackupActivity restoreFromBackupActivity = this.A00;
        if (!restoreFromBackupActivity.A0u.get()) {
            return restoreFromBackupActivity.A0G.A0M.A00();
        }
        Log.i("gdrive-activity/one-time-setup-task/cancelled");
        return false;
    }
}
