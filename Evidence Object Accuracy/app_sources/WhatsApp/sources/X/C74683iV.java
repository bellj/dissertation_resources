package X;

import com.whatsapp.settings.chat.wallpaper.downloadable.picker.DownloadableWallpaperGridLayoutManager;

/* renamed from: X.3iV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74683iV extends AbstractC05290Oz {
    public final /* synthetic */ DownloadableWallpaperGridLayoutManager A00;

    public C74683iV(DownloadableWallpaperGridLayoutManager downloadableWallpaperGridLayoutManager) {
        this.A00 = downloadableWallpaperGridLayoutManager;
    }

    @Override // X.AbstractC05290Oz
    public int A00(int i) {
        int itemViewType = this.A00.A00.getItemViewType(i);
        int i2 = 1;
        if (!(itemViewType == 0 || itemViewType == 1)) {
            i2 = 3;
            if (!(itemViewType == 2 || itemViewType == 3)) {
                throw C12980iv.A0u(C12960it.A0W(itemViewType, "Invalid viewType: "));
            }
        }
        return i2;
    }
}
