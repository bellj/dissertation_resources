package X;

import android.content.ContentValues;
import android.database.Cursor;
import com.whatsapp.util.Log;
import java.util.Map;

/* renamed from: X.14h  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C241214h {
    public final C16510p9 A00;
    public final C16490p7 A01;

    public C241214h(C16510p9 r1, C16490p7 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    /* JADX INFO: finally extract failed */
    public void A00(AnonymousClass1PE r14) {
        ContentValues contentValues;
        synchronized (r14) {
            contentValues = new ContentValues();
            contentValues.put("last_activity_ts", Long.valueOf(r14.A0J));
            contentValues.put("last_activity_seen_ts", Long.valueOf(r14.A0I));
            contentValues.put("join_ts", Long.valueOf(r14.A0H));
        }
        AbstractC14640lm r10 = r14.A0i;
        C16490p7 r7 = this.A01;
        C16310on A02 = r7.A02();
        try {
            C16510p9 r8 = this.A00;
            if (A02.A03.A00("community_chat", contentValues, "chat_row_id = ?", new String[]{String.valueOf(r8.A02(r10))}) == 0) {
                contentValues.put("chat_row_id", Long.valueOf(r8.A02(r10)));
                A02 = r7.A02();
                try {
                    long A022 = A02.A03.A02(contentValues, "community_chat");
                    A02.close();
                    r14.A0V = A022;
                } finally {
                    try {
                        A02.close();
                    } catch (Throwable unused) {
                    }
                }
            }
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused2) {
            }
            throw th;
        }
    }

    public void A01(Map map) {
        C28181Ma r5 = new C28181Ma("CommunityChatStore/loadData");
        C16310on A01 = this.A01.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT chat_row_id, last_activity_ts, last_activity_seen_ts, join_ts FROM community_chat", null);
            if (A09 == null) {
                r5.A02("null");
                r5.A01();
            } else {
                int columnIndexOrThrow = A09.getColumnIndexOrThrow("chat_row_id");
                int columnIndexOrThrow2 = A09.getColumnIndexOrThrow("last_activity_ts");
                int columnIndexOrThrow3 = A09.getColumnIndexOrThrow("last_activity_seen_ts");
                int columnIndexOrThrow4 = A09.getColumnIndexOrThrow("join_ts");
                while (A09.moveToNext()) {
                    AbstractC14640lm A05 = this.A00.A05(A09.getLong(columnIndexOrThrow));
                    if (A05 == null) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("CommunityChatStore/failed to find chatJid by row id: ");
                        sb.append(A09.getLong(columnIndexOrThrow));
                        Log.w(sb.toString());
                    } else {
                        Object obj = map.get(A05);
                        AnonymousClass1PE r8 = (AnonymousClass1PE) obj;
                        if (r8 == null) {
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("CommunityChatStore/missing chat in map: ");
                            sb2.append(A05);
                            Log.w(sb2.toString());
                        } else {
                            long j = A09.getLong(columnIndexOrThrow2);
                            synchronized (obj) {
                                r8.A0J = j;
                            }
                            long j2 = A09.getLong(columnIndexOrThrow3);
                            synchronized (obj) {
                                r8.A0I = j2;
                            }
                            r8.A0H = A09.getLong(columnIndexOrThrow4);
                            map.put(A05, r8);
                        }
                    }
                }
                r5.A01();
                A09.close();
            }
            A01.close();
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
