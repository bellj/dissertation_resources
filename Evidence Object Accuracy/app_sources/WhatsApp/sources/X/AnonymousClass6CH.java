package X;

import android.view.View;
import com.whatsapp.payments.pin.ui.PinBottomSheetDialogFragment;

/* renamed from: X.6CH  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6CH implements AnonymousClass6MZ {
    public final /* synthetic */ int A00;
    public final /* synthetic */ ActivityC13790kL A01;
    public final /* synthetic */ PinBottomSheetDialogFragment A02;
    public final /* synthetic */ AbstractC118055bC A03;
    public final /* synthetic */ String A04;
    public final /* synthetic */ String A05;
    public final /* synthetic */ String A06;

    public AnonymousClass6CH(ActivityC13790kL r1, PinBottomSheetDialogFragment pinBottomSheetDialogFragment, AbstractC118055bC r3, String str, String str2, String str3, int i) {
        this.A03 = r3;
        this.A02 = pinBottomSheetDialogFragment;
        this.A04 = str;
        this.A06 = str2;
        this.A05 = str3;
        this.A00 = i;
        this.A01 = r1;
    }

    @Override // X.AnonymousClass6MZ
    public void AOP(String str) {
        PinBottomSheetDialogFragment pinBottomSheetDialogFragment = this.A02;
        pinBottomSheetDialogFragment.A1N();
        AbstractC118055bC r7 = this.A03;
        AnonymousClass605 r2 = r7.A07;
        String str2 = this.A04;
        String str3 = this.A06;
        C129275xS r3 = new C129275xS(this.A01, null, pinBottomSheetDialogFragment, r7, this.A05, str2, str3, 1, this.A00);
        r2.A01(new AbstractC136296Lz(r3, str, str2) { // from class: X.6C5
            public final /* synthetic */ C129275xS A01;
            public final /* synthetic */ String A02;
            public final /* synthetic */ String A03;

            {
                this.A02 = r3;
                this.A03 = r4;
                this.A01 = r2;
            }

            @Override // X.AbstractC136296Lz
            public final void AVF(C128545wH r72) {
                AnonymousClass605 r5 = AnonymousClass605.this;
                String str4 = this.A02;
                String str5 = this.A03;
                r5.A06.A00(new C1330969l(r5, r72, this.A01, str5), r72, str4);
            }
        }, new AnonymousClass6M0() { // from class: X.6C7
            @Override // X.AnonymousClass6M0
            public final void AVD(C452120p r32) {
                C129275xS.this.A00(r32, null);
            }
        }, str3);
    }

    @Override // X.AnonymousClass6MZ
    public void AQi(View view) {
        this.A03.A00.A0A(Boolean.TRUE);
    }
}
