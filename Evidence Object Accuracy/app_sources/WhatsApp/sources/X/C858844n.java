package X;

import com.whatsapp.status.StatusesFragment;
import java.util.Set;

/* renamed from: X.44n  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C858844n extends AbstractC33331dp {
    public final /* synthetic */ StatusesFragment A00;

    public C858844n(StatusesFragment statusesFragment) {
        this.A00 = statusesFragment;
    }

    @Override // X.AbstractC33331dp
    public void A00(Set set) {
        StatusesFragment statusesFragment = this.A00;
        statusesFragment.A0g.getFilter().filter(statusesFragment.A0t);
    }
}
