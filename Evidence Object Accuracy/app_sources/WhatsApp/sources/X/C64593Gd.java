package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.3Gd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64593Gd {
    public int A00;
    public final Map A01;

    public C64593Gd(Map map, int i) {
        this.A01 = map;
        this.A00 = i;
    }

    public static C64593Gd A00(Context context, AnonymousClass4OP r15, List list) {
        File file;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        Iterator it = list.iterator();
        boolean z = false;
        boolean z2 = false;
        while (true) {
            int i = 1;
            if (it.hasNext()) {
                AbstractC15340mz A0f = C12980iv.A0f(it);
                boolean z3 = A0f instanceof AnonymousClass1X7;
                String str = null;
                if (z3 || (A0f instanceof AnonymousClass1X3)) {
                    file = AbstractC15340mz.A00((AbstractC16130oV) A0f).A0F;
                    if (file != null) {
                        if (!z3) {
                            z2 = true;
                        }
                        z = true;
                    }
                } else if (A0f instanceof C28861Ph) {
                    C28861Ph r2 = (C28861Ph) A0f;
                    C53052cO A00 = C53052cO.A00(context, null, r2, false, false);
                    AnonymousClass009.A00();
                    int i2 = C16590pI.A00(r15.A01).getDisplayMetrics().widthPixels;
                    int round = Math.round(((float) i2) / 0.5625f);
                    C12980iv.A1A(A00, i2, round);
                    Bitmap createBitmap = Bitmap.createBitmap(i2, round, Bitmap.Config.ARGB_8888);
                    Canvas canvas = new Canvas(createBitmap);
                    A00.layout(0, 0, i2, round);
                    A00.draw(canvas);
                    file = null;
                    try {
                        try {
                            StringBuilder A0k = C12960it.A0k("share-");
                            A0k.append(C003501n.A02(r2.A0z.A01));
                            File A0M = r15.A00.A0M(C12960it.A0d(".png", A0k));
                            FileOutputStream fileOutputStream = new FileOutputStream(A0M);
                            try {
                                createBitmap.compress(Bitmap.CompressFormat.JPEG, 75, fileOutputStream);
                                fileOutputStream.flush();
                                fileOutputStream.close();
                                createBitmap.recycle();
                                file = A0M;
                            } catch (Throwable th) {
                                try {
                                    fileOutputStream.close();
                                } catch (Throwable unused) {
                                }
                                throw th;
                                break;
                            }
                        } catch (Throwable th2) {
                            createBitmap.recycle();
                            throw th2;
                        }
                    } catch (FileNotFoundException e) {
                        StringBuilder A0h = C12960it.A0h();
                        A0h.append("File not found: ");
                        Log.e(C12960it.A0d(e.getMessage(), A0h));
                        createBitmap.recycle();
                    } catch (IOException unused2) {
                        createBitmap.recycle();
                    }
                    List list2 = A00.A0A;
                    if (list2.size() > 0) {
                        str = C12960it.A0g(list2, 0);
                    }
                    if (file != null) {
                        z = true;
                    }
                } else {
                    continue;
                }
                linkedHashMap.put(A0f, new AnonymousClass4OO(file, str));
            } else {
                int size = linkedHashMap.size();
                if (z) {
                    if (z2) {
                        i = 5;
                    } else if (size != 1) {
                        i = 3;
                    }
                } else if (!z2) {
                    return new C64593Gd(linkedHashMap, 0);
                } else {
                    i = 4;
                    if (size == 1) {
                        i = 2;
                    }
                }
                return new C64593Gd(linkedHashMap, i);
            }
        }
    }
}
