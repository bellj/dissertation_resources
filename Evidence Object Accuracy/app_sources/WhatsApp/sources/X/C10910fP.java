package X;

import java.util.HashMap;

/* renamed from: X.0fP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C10910fP extends HashMap<String, Object> {
    public final /* synthetic */ AnonymousClass04L this$0;
    public final /* synthetic */ long val$currentTime;
    public final /* synthetic */ String val$surface = null;

    public C10910fP(AnonymousClass04L r3, long j) {
        this.this$0 = r3;
        this.val$currentTime = j;
        put("duration", Long.valueOf(j - r3.A0H));
        put("surface", "unknown");
    }
}
