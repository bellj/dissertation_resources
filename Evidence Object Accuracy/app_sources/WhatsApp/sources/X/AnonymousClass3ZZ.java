package X;

import android.text.TextUtils;

/* renamed from: X.3ZZ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3ZZ implements AbstractC33751f1 {
    @Override // X.AbstractC33751f1
    public int ADz() {
        return 26;
    }

    @Override // X.AbstractC33751f1
    public void A6l(C39971qq r14, AnonymousClass1XE r15) {
        AnonymousClass1ZL r5 = r15.A00;
        if (r5 != null && r5.A04 == 1) {
            AnonymousClass1G3 r2 = r14.A03;
            C57512nB r0 = ((C27081Fy) r2.A00).A0O;
            if (r0 == null) {
                r0 = C57512nB.A06;
            }
            AnonymousClass1G4 A0T = r0.A0T();
            String str = r5.A03;
            C57512nB r1 = (C57512nB) AnonymousClass1G4.A00(A0T);
            r1.A00 |= 1;
            r1.A05 = str;
            String str2 = r5.A01;
            if (!TextUtils.isEmpty(str2)) {
                C57512nB r12 = (C57512nB) AnonymousClass1G4.A00(A0T);
                r12.A00 |= 16;
                r12.A04 = str2;
            }
            EnumC87264Av r3 = EnumC87264Av.A01;
            C57512nB r13 = (C57512nB) AnonymousClass1G4.A00(A0T);
            r13.A00 |= 2;
            r13.A01 = r3.value;
            C57032mM r02 = r13.A03;
            if (r02 == null) {
                r02 = C57032mM.A02;
            }
            AnonymousClass1G4 A0T2 = r02.A0T();
            String str3 = r5.A02;
            if (str3 != null) {
                C57032mM r16 = (C57032mM) AnonymousClass1G4.A00(A0T2);
                r16.A00 |= 1;
                r16.A01 = str3;
            }
            C57512nB r17 = (C57512nB) AnonymousClass1G4.A00(A0T);
            r17.A03 = (C57032mM) A0T2.A02();
            r17.A00 |= 4;
            AnonymousClass1PG r9 = r14.A04;
            byte[] bArr = r14.A09;
            if (C32411c7.A0U(r9, r15, bArr)) {
                C43261wh A0P = C32411c7.A0P(r14.A00, r14.A02, r9, r15, bArr, r14.A06);
                C57512nB r18 = (C57512nB) AnonymousClass1G4.A00(A0T);
                r18.A02 = A0P;
                r18.A00 |= 8;
            }
            C27081Fy r22 = (C27081Fy) AnonymousClass1G4.A00(r2);
            r22.A0O = (C57512nB) A0T.A02();
            r22.A00 |= 1073741824;
        }
    }
}
