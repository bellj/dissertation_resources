package X;

import android.view.ViewGroup;

/* renamed from: X.2gg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54412gg extends AnonymousClass02M {
    public AnonymousClass02M A00;

    public C54412gg(AnonymousClass02M r3) {
        this.A00 = r3;
        A07(r3.A00);
        r3.A01.registerObserver(new C74833ir(this));
    }

    @Override // X.AnonymousClass02M
    public long A00(int i) {
        long A0E = A0E(i);
        long j = A0E & 4294967295L;
        int i2 = (j > 4294967295L ? 1 : (j == 4294967295L ? 0 : -1));
        AnonymousClass02M r1 = this.A00;
        if (i2 == 0) {
            return ((AnonymousClass2TX) r1).ADI((int) (A0E >> 32));
        }
        return r1.A00((int) j);
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        AnonymousClass02M r0 = this.A00;
        return r0.A0D() + ((AnonymousClass2TX) r0).ADH();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0029, code lost:
        r2 = r2 << 32;
        r0 = (long) (r6 + (r9 - r5));
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final long A0E(int r9) {
        /*
            r8 = this;
            X.02M r7 = r8.A00
            X.2TX r7 = (X.AnonymousClass2TX) r7
            int r3 = r7.ADH()
            r2 = 0
            r6 = 0
            r5 = 0
        L_0x000b:
            r4 = 32
            if (r2 >= r3) goto L_0x001a
            if (r5 != r9) goto L_0x001e
            long r2 = (long) r2
            long r2 = r2 << r4
            r0 = 4294967295(0xffffffff, double:2.1219957905E-314)
        L_0x0018:
            long r2 = r2 | r0
            return r2
        L_0x001a:
            int r0 = r3 + -1
            long r2 = (long) r0
            goto L_0x0029
        L_0x001e:
            int r5 = r5 + 1
            int r1 = r7.ABl(r2)
            int r0 = r5 + r1
            if (r0 <= r9) goto L_0x002e
            long r2 = (long) r2
        L_0x0029:
            long r2 = r2 << r4
            int r9 = r9 - r5
            int r6 = r6 + r9
            long r0 = (long) r6
            goto L_0x0018
        L_0x002e:
            int r6 = r6 + r1
            int r2 = r2 + 1
            r5 = r0
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C54412gg.A0E(int):long");
    }

    @Override // X.AnonymousClass02M
    public void ANH(AnonymousClass03U r9, int i) {
        long A0E = A0E(i);
        long j = A0E & 4294967295L;
        int i2 = (j > 4294967295L ? 1 : (j == 4294967295L ? 0 : -1));
        AnonymousClass02M r1 = this.A00;
        if (i2 == 0) {
            ((AnonymousClass2TX) r1).ANF(r9, (int) (A0E >> 32));
        } else {
            r1.ANH(r9, (int) j);
        }
    }

    @Override // X.AnonymousClass02M
    public AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        if (i == -1000) {
            return ((AnonymousClass2TX) this.A00).AOh(viewGroup);
        }
        return this.A00.AOl(viewGroup, i);
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        long A0E = A0E(i) & 4294967295L;
        if (A0E == 4294967295L) {
            return -1000;
        }
        return this.A00.getItemViewType((int) A0E);
    }
}
