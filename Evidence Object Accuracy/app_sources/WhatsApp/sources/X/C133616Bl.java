package X;

import android.util.Base64;
import java.util.Map;

/* renamed from: X.6Bl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C133616Bl implements AbstractC16960q2 {
    @Override // X.AbstractC16960q2
    public Class A9u() {
        return EnumC124635pr.class;
    }

    @Override // X.AbstractC16960q2
    public /* bridge */ /* synthetic */ Object Aam(Enum r6, Object obj, Map map) {
        AnonymousClass1ZD r0;
        AnonymousClass1ZK r02;
        AnonymousClass1ZD r03;
        AnonymousClass1ZD r04;
        AnonymousClass1ZD r05;
        AnonymousClass1ZD r06;
        AnonymousClass1ZD r07;
        byte[] bArr;
        AnonymousClass1ZD r08;
        C16380ov r7 = (C16380ov) obj;
        EnumC124635pr r62 = (EnumC124635pr) r6;
        C16700pc.A0E(r7, 0);
        C16700pc.A0E(r62, 1);
        switch (C117315Zl.A01(r62, C125245qw.A00)) {
            case 1:
                C16470p4 r09 = r7.A00;
                if (r09 == null || (r04 = r09.A01) == null) {
                    return null;
                }
                return r04.A07;
            case 2:
                C16470p4 r010 = r7.A00;
                if (r010 == null || (r08 = r010.A01) == null) {
                    return null;
                }
                return r08.A06;
            case 3:
                C16470p4 r011 = r7.A00;
                if (r011 == null || (r07 = r011.A01) == null || (bArr = r07.A0C) == null) {
                    return null;
                }
                return Base64.encodeToString(bArr, 0);
            case 4:
                C16470p4 r012 = r7.A00;
                if (r012 == null || (r06 = r012.A01) == null) {
                    return null;
                }
                return r06.A08;
            case 5:
                C16470p4 r013 = r7.A00;
                if (r013 == null || (r05 = r013.A01) == null) {
                    return null;
                }
                AnonymousClass1ZI r4 = r05.A05;
                AbstractC30791Yv r3 = r05.A03;
                if (r4 == null || r3 == null) {
                    return null;
                }
                C94074bD r2 = new C94074bD();
                r2.A02 = r4.A01;
                r2.A01 = r4.A00;
                r2.A03 = r3;
                return r2.A00();
            case 6:
                C16470p4 r014 = r7.A00;
                if (r014 == null || (r0 = r014.A01) == null || (r02 = r0.A04) == null) {
                    return "UNKNOWN";
                }
                switch (AnonymousClass1ZD.A00(r02.A01)) {
                    case 1:
                        return "PENDING";
                    case 2:
                        return "PROCESSING";
                    case 3:
                        return "COMPLETED";
                    case 4:
                        return "CANCELED";
                    case 5:
                        return "PARTIALLY_SHIPPED";
                    case 6:
                        return "SHIPPED";
                    default:
                        return "UNKNOWN";
                }
            case 7:
                C16470p4 r015 = r7.A00;
                if (r015 == null || (r03 = r015.A01) == null) {
                    return "UNKNOWN";
                }
                String str = r03.A09;
                if ("digital-goods".equals(str)) {
                    return "DIGITAL_GOODS";
                }
                if ("physical-goods".equals(str)) {
                    return "PHYSICAL_GOODS";
                }
                return "UNKNOWN";
            default:
                throw new C113285Gx();
        }
    }
}
