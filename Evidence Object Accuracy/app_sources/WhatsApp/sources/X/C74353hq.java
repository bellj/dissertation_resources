package X;

import android.view.View;
import com.whatsapp.R;

/* renamed from: X.3hq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74353hq extends AnonymousClass04v {
    public final /* synthetic */ AbstractC14670lq A00;

    public C74353hq(AbstractC14670lq r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r5) {
        super.A06(view, r5);
        r5.A09(new C007804a(16, this.A00.A0j.getString(R.string.voice_note_draft_send_description)));
    }
}
