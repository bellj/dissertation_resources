package X;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.LinearLayout;

/* renamed from: X.0Bl  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Bl extends LinearLayout.LayoutParams {
    public AnonymousClass0Bl(int i) {
        super(i, -2);
    }

    public AnonymousClass0Bl(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public AnonymousClass0Bl(ViewGroup.LayoutParams layoutParams) {
        super(layoutParams);
    }
}
