package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.5pq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class EnumC124625pq extends Enum implements AnonymousClass5UX {
    public static final /* synthetic */ EnumC124625pq[] A00;
    public static final EnumC124625pq A01;
    public static final EnumC124625pq A02;
    public static final EnumC124625pq A03;
    public static final EnumC124625pq A04;
    public final String fieldName;

    public static EnumC124625pq valueOf(String str) {
        return (EnumC124625pq) Enum.valueOf(EnumC124625pq.class, str);
    }

    public static EnumC124625pq[] values() {
        return (EnumC124625pq[]) A00.clone();
    }

    static {
        EnumC124625pq r7 = new EnumC124625pq("CURRENCY", "currency", 0);
        A01 = r7;
        EnumC124625pq r6 = new EnumC124625pq("VALUE", "value", 1);
        A04 = r6;
        EnumC124625pq r5 = new EnumC124625pq("OFFSET", "offset", 2);
        A03 = r5;
        EnumC124625pq r4 = new EnumC124625pq("FORMATTED_AMOUNT", "formatted_amount", 3);
        A02 = r4;
        EnumC124625pq r1 = new EnumC124625pq("FORMATTED_AMOUNT_WITH_CURRENCY", "formatted_amount_with_currency", 4);
        EnumC124625pq[] r0 = new EnumC124625pq[5];
        C72453ed.A1F(r7, r6, r5, r4, r0);
        r0[4] = r1;
        A00 = r0;
    }

    public EnumC124625pq(String str, String str2, int i) {
        this.fieldName = str2;
    }

    @Override // X.AnonymousClass5UX
    public String ACu() {
        return this.fieldName;
    }
}
