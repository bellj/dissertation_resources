package X;

import android.content.SharedPreferences;
import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;
import com.whatsapp.util.Log;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.0y8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21880y8 {
    public SharedPreferences A00;
    public ExecutorC27271Gr A01;
    public ConcurrentHashMap A02 = new ConcurrentHashMap(4);
    public final C14830m7 A03;
    public final C14850m9 A04;
    public final C21860y6 A05;
    public final C16630pM A06;
    public final C21850y5 A07;
    public final C21870y7 A08;
    public final AbstractC14440lR A09;
    public volatile boolean A0A;
    public volatile boolean A0B;
    public volatile boolean A0C;
    public volatile boolean A0D;
    public volatile boolean A0E;

    public C21880y8(C14830m7 r3, C14850m9 r4, C21860y6 r5, C16630pM r6, C21850y5 r7, C21870y7 r8, AbstractC14440lR r9) {
        this.A03 = r3;
        this.A04 = r4;
        this.A09 = r9;
        this.A07 = r7;
        this.A05 = r5;
        this.A08 = r8;
        this.A06 = r6;
    }

    public final long A00() {
        C14850m9 r2 = this.A04;
        if (r2.A02(1486) == 2) {
            return -1;
        }
        try {
            long A02 = (long) r2.A02(1392);
            if (A02 > 0) {
                return A02;
            }
            return 1209600;
        } catch (IllegalArgumentException unused) {
            Log.e("noticebadgemanager/getbadgeexpiretimeinseconds duration abprops is not defined");
            return 1209600;
        }
    }

    public final synchronized SharedPreferences A01() {
        SharedPreferences sharedPreferences;
        sharedPreferences = this.A00;
        if (sharedPreferences == null) {
            sharedPreferences = this.A06.A01("notice_store");
            this.A00 = sharedPreferences;
        }
        return sharedPreferences;
    }

    public final void A02() {
        for (Map.Entry<String, ?> entry : A01().getAll().entrySet()) {
            try {
                Integer valueOf = Integer.valueOf(entry.getKey());
                try {
                    JSONObject jSONObject = new JSONObject((String) entry.getValue());
                    this.A02.put(valueOf, new C33411dz(jSONObject.getInt("viewId"), jSONObject.getInt("badgeStage"), jSONObject.getLong("enabledTimeInSeconds"), jSONObject.getLong("selectedTimeInSeconds")));
                } catch (NumberFormatException e) {
                    StringBuilder sb = new StringBuilder("noticebadgemanager/loadFromFile corrupted number ");
                    sb.append(e.toString());
                    Log.e(sb.toString());
                    A01().edit().remove(entry.getKey()).apply();
                } catch (JSONException e2) {
                    StringBuilder sb2 = new StringBuilder("noticebadgemanager/loadFromFile bad json ");
                    sb2.append(e2.toString());
                    Log.e(sb2.toString());
                    A01().edit().remove(entry.getKey()).apply();
                }
            } catch (NumberFormatException unused) {
                Log.e("noticebadgemanager/loadfromfile notice id key is corrupted");
            }
        }
    }

    public final void A03(int i, int i2) {
        ConcurrentHashMap concurrentHashMap = this.A02;
        if (concurrentHashMap.size() == 0) {
            A02();
        }
        C33411dz r4 = (C33411dz) concurrentHashMap.get(Integer.valueOf(i));
        if (r4 != null) {
            int i3 = r4.A00;
            if ((i2 > i3 || i3 <= 0) && i3 != -1) {
                r4.A00 = i2;
                if (i3 < 4 && i2 == 4) {
                    r4.A03 = this.A03.A00() / 1000;
                }
                A06(r4, i);
                return;
            }
            return;
        }
        throw new IllegalArgumentException("Invalid noticeId");
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A04(int r17, int r18, boolean r19) {
        /*
            r16 = this;
            r2 = r16
            java.util.concurrent.ConcurrentHashMap r5 = r2.A02
            r3 = r17
            java.lang.Integer r1 = java.lang.Integer.valueOf(r3)
            boolean r0 = r5.containsKey(r1)
            r4 = -1
            r11 = 0
            r10 = r18
            if (r0 != 0) goto L_0x0031
            if (r19 == 0) goto L_0x0026
            r12 = -1
            r14 = -1
            X.1dz r9 = new X.1dz
            r9.<init>(r10, r11, r12, r14)
            r2.A05(r9, r3)
        L_0x0022:
            r2.A06(r9, r3)
        L_0x0025:
            return
        L_0x0026:
            r12 = -1
            r14 = -1
            r11 = r4
            X.1dz r9 = new X.1dz
            r9.<init>(r10, r11, r12, r14)
            goto L_0x0022
        L_0x0031:
            java.lang.Object r9 = r5.get(r1)
            X.1dz r9 = (X.C33411dz) r9
            if (r9 == 0) goto L_0x0025
            int r0 = r9.A01
            if (r10 == r0) goto L_0x006a
            r9.A01 = r10
            r8 = 1
        L_0x0040:
            int r0 = r9.A00
            if (r19 == 0) goto L_0x006c
            if (r0 != r4) goto L_0x004c
            r9.A00 = r11
            r2.A05(r9, r3)
            r8 = 1
        L_0x004c:
            int r1 = r9.A00
            r0 = 4
            if (r1 >= r0) goto L_0x0077
            X.0m7 r0 = r2.A03
            long r6 = r0.A00()
            r0 = 1000(0x3e8, double:4.94E-321)
            long r6 = r6 / r0
            long r0 = r9.A02
            long r6 = r6 - r0
            long r4 = r2.A00()
            int r0 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r0 < 0) goto L_0x0077
            r0 = 9
            r9.A00 = r0
            goto L_0x0022
        L_0x006a:
            r8 = 0
            goto L_0x0040
        L_0x006c:
            if (r0 < 0) goto L_0x0077
            r0 = -1
            r9.A02 = r0
            r9.A03 = r0
            r9.A00 = r4
            goto L_0x0022
        L_0x0077:
            if (r8 == 0) goto L_0x0025
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C21880y8.A04(int, int, boolean):void");
    }

    public final void A05(C33411dz r9, int i) {
        long A00 = this.A03.A00() / 1000;
        r9.A02 = A00;
        C21850y5 r7 = this.A07;
        C50252Ot r6 = new C50252Ot();
        r6.A01 = Long.valueOf((long) i);
        r6.A00 = 1;
        r6.A03 = 1L;
        if (A00 > 0) {
            r6.A02 = Long.valueOf(A00 / 60);
        }
        r7.A00.A05(r6);
    }

    public final void A06(C33411dz r5, int i) {
        this.A02.put(Integer.valueOf(i), r5);
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("viewId", r5.A01);
            jSONObject.put("badgeStage", r5.A00);
            jSONObject.put("enabledTimeInSeconds", r5.A02);
            jSONObject.put("selectedTimeInSeconds", r5.A03);
            A01().edit().putString(String.valueOf(i), jSONObject.toString()).apply();
        } catch (JSONException e) {
            StringBuilder sb = new StringBuilder("noticebadgemanager/savenotice JEX ");
            sb.append(e.toString());
            Log.e(sb.toString());
        }
    }

    public final void A07(Runnable runnable) {
        ExecutorC27271Gr r2 = this.A01;
        if (r2 == null) {
            r2 = new ExecutorC27271Gr(this.A09, false);
            this.A01 = r2;
        }
        r2.execute(runnable);
    }

    public boolean A08() {
        int i;
        if (this.A0C) {
            ConcurrentHashMap concurrentHashMap = this.A02;
            for (Object obj : concurrentHashMap.keySet()) {
                C33411dz r2 = (C33411dz) concurrentHashMap.get(obj);
                if (r2 != null && (i = r2.A00) > -1 && i < 4) {
                    if ((this.A03.A00() / 1000) - r2.A02 < A00()) {
                        return true;
                    }
                    A07(new RunnableBRunnable0Shape8S0200000_I0_8(this, 29, obj));
                }
            }
        }
        return false;
    }
}
