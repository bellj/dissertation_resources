package X;

import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Base64;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.io.ByteArrayInputStream;
import java.security.GeneralSecurityException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1W7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1W7 implements AbstractC21730xt {
    public AnonymousClass1W8 A00;
    public final UserJid A01;
    public final C17220qS A02;

    public AnonymousClass1W7(UserJid userJid, C17220qS r2) {
        this.A01 = userJid;
        this.A02 = r2;
    }

    public final void A00() {
        AnonymousClass1W8 r2 = this.A00;
        if (r2 != null) {
            UserJid userJid = this.A01;
            Log.e("DirectConnectionManager/loadPhoneNumberSignature/onGetPhoneNumberSignatureError");
            C246816l r0 = r2.A00;
            r0.A04(userJid);
            r0.A04.AaV("direct-connection-get-phone-signature-error-response", "", false);
        }
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        A00();
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r3, String str) {
        StringBuilder sb = new StringBuilder("GetPhoneNumberSignature/delivery-error with iqId ");
        sb.append(str);
        Log.w(sb.toString());
        A00();
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r19, String str) {
        AnonymousClass4RM r10;
        AbstractC15710nm r1;
        String str2;
        AnonymousClass1V8 A0E = r19.A0E("signed_user_info");
        if (A0E != null) {
            AnonymousClass1V8 A0E2 = A0E.A0E("phone_number");
            AnonymousClass1V8 A0E3 = A0E.A0E("ttl_timestamp");
            AnonymousClass1V8 A0E4 = A0E.A0E("phone_number_signature");
            AnonymousClass1V8 A0E5 = A0E.A0E("business_domain");
            if (!(A0E2 == null || A0E3 == null || A0E4 == null || A0E5 == null)) {
                String A0G = A0E2.A0G();
                String A0G2 = A0E3.A0G();
                String A0G3 = A0E4.A0G();
                String A0G4 = A0E5.A0G();
                if (!TextUtils.isEmpty(A0G) && !TextUtils.isEmpty(A0G2) && !TextUtils.isEmpty(A0G3) && !TextUtils.isEmpty(A0G4)) {
                    AnonymousClass1W8 r6 = this.A00;
                    if (r6 != null) {
                        UserJid userJid = this.A01;
                        Log.i("DirectConnectionManager/loadPhoneNumberSignature/onGetPhoneNumberSignatureSuccess");
                        C246816l r3 = r6.A00;
                        String str3 = r3.A02;
                        if (TextUtils.isEmpty(str3)) {
                            C14820m6 r12 = r3.A08;
                            str3 = r12.A0D(userJid.getRawString());
                            if (TextUtils.isEmpty(str3)) {
                                String rawString = userJid.getRawString();
                                SharedPreferences sharedPreferences = r12.A00;
                                StringBuilder sb = new StringBuilder("dc_default_postcode_");
                                sb.append(rawString);
                                str3 = sharedPreferences.getString(sb.toString(), null);
                            }
                        }
                        if (str3 == null) {
                            r3.A04(userJid);
                            r1 = r3.A04;
                            str2 = "direct-connection-empty-postcode";
                        } else {
                            C20140vH r0 = r3.A09;
                            UserJid userJid2 = r6.A01;
                            if (r0.A05(userJid2) || r3.A08.A0D(userJid2.getRawString()) != null) {
                                r10 = new AnonymousClass4RM(A0G, A0G2, A0G3, str3);
                            } else {
                                r10 = new AnonymousClass4RM(null, A0G2, null, str3);
                            }
                            if (r3.A0A.A07(1867)) {
                                SharedPreferences sharedPreferences2 = r3.A07.A00.A00;
                                AnonymousClass4RM r13 = null;
                                String string = sharedPreferences2.getString("latest_biz_backend_request_id", null);
                                if (string != null) {
                                    if (string.equals("252")) {
                                        r13 = new AnonymousClass4RM(A0G, A0G2, A0G3, str3);
                                    }
                                    sharedPreferences2.edit().remove("latest_biz_backend_request_id").apply();
                                    if (r13 != null) {
                                        r10 = r13;
                                    }
                                }
                            }
                            try {
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'", Locale.US);
                                String str4 = r10.A03;
                                Date parse = simpleDateFormat.parse(str4);
                                if (parse != null) {
                                    try {
                                        String str5 = r10.A02;
                                        if (TextUtils.isEmpty(str5)) {
                                            Log.e("DirectConnectionManager/generateEncryptionStringFromSignedInfo/Empty postcode");
                                            r3.A04.AaV("direct-connection-empty-postcode", "", false);
                                        } else {
                                            String A00 = r3.A00(userJid);
                                            if (A00 == null) {
                                                Log.e("DirectConnectionManager/generateEncryptionStringFromSignedInfo/Null certificate");
                                                r3.A04.AaV("direct-connection-failed-to-load-certificate-from-preferences", "", false);
                                            } else {
                                                X509Certificate x509Certificate = (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(Base64.decode(A00, 2)));
                                                if (!A0G4.equals(C246916m.A00(x509Certificate.getSubjectX500Principal().getName()))) {
                                                    Log.e("DirectConnectionManager/generateEncryptionStringFromSignedInfo/Incorrect CN in certificate");
                                                    r3.A04.AaV("direct-connection-certificate-common-name-mismatch", "", false);
                                                    r3.A08.A0Z(userJid.getRawString());
                                                } else {
                                                    C246916m r62 = r3.A06;
                                                    JSONObject jSONObject = new JSONObject();
                                                    String str6 = r10.A00;
                                                    if (str6 != null) {
                                                        jSONObject.put("phone_number", str6);
                                                    }
                                                    jSONObject.put("ttl_timestamp", str4);
                                                    String str7 = r10.A01;
                                                    if (str7 != null) {
                                                        jSONObject.put("phone_number_signature", str7);
                                                    }
                                                    jSONObject.put("postcode", str5);
                                                    String A002 = r62.A02(jSONObject.toString(), x509Certificate.getPublicKey()).A00();
                                                    if (A002 != null) {
                                                        C14820m6 r02 = r3.A08;
                                                        String rawString2 = userJid.getRawString();
                                                        SharedPreferences sharedPreferences3 = r02.A00;
                                                        SharedPreferences.Editor edit = sharedPreferences3.edit();
                                                        StringBuilder sb2 = new StringBuilder("dc_business_domain_");
                                                        sb2.append(rawString2);
                                                        edit.putString(sb2.toString(), A0G4).apply();
                                                        if (r3.A02 == null) {
                                                            String rawString3 = userJid.getRawString();
                                                            SharedPreferences.Editor edit2 = sharedPreferences3.edit();
                                                            StringBuilder sb3 = new StringBuilder("smb_business_direct_connection_enc_string_");
                                                            sb3.append(rawString3);
                                                            edit2.putString(sb3.toString(), A002).apply();
                                                            String rawString4 = userJid.getRawString();
                                                            long time = parse.getTime();
                                                            SharedPreferences.Editor edit3 = sharedPreferences3.edit();
                                                            StringBuilder sb4 = new StringBuilder("smb_business_direct_connection_enc_string_expired_timestamp_");
                                                            sb4.append(rawString4);
                                                            edit3.putLong(sb4.toString(), time).apply();
                                                        } else {
                                                            r3.A01 = A002;
                                                            r3.A00 = parse.getTime();
                                                        }
                                                        r3.A05(userJid);
                                                        return;
                                                    }
                                                }
                                            }
                                        }
                                    } catch (IllegalArgumentException | GeneralSecurityException | JSONException e) {
                                        Log.e("DirectConnectionManager/generateEncryptionStringFromSignedInfo/", e);
                                        r3.A04.AaV("direct-connection-fail-to-generate-encryption-string", e.toString(), false);
                                    }
                                    r3.A04(userJid);
                                    return;
                                }
                            } catch (ParseException e2) {
                                Log.e("DirectConnectionManager/getExpirationDateFromSignedUserInfo/Invalid timestamp", e2);
                            }
                            r3.A04(userJid);
                            r1 = r3.A04;
                            str2 = "direct-connection-invalid-expiration-date";
                        }
                        r1.AaV(str2, "", false);
                        return;
                    }
                    return;
                }
            }
        }
        A00();
    }
}
