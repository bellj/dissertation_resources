package X;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.DashPathEffect;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.RectF;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0aU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC08040aU implements AbstractC12030hG, AbstractC12860ig, AbstractC12870ih {
    public AnonymousClass0QR A00;
    public final Paint A01;
    public final Path A02 = new Path();
    public final Path A03 = new Path();
    public final PathMeasure A04 = new PathMeasure();
    public final RectF A05 = new RectF();
    public final AnonymousClass0AA A06;
    public final AnonymousClass0QR A07;
    public final AnonymousClass0QR A08;
    public final AnonymousClass0QR A09;
    public final AbstractC08070aX A0A;
    public final List A0B;
    public final List A0C = new ArrayList();
    public final float[] A0D;

    public AbstractC08040aU(Paint.Cap cap, Paint.Join join, AnonymousClass0AA r8, AnonymousClass0H9 r9, AnonymousClass0H9 r10, AnonymousClass0HA r11, AbstractC08070aX r12, List list, float f) {
        AnonymousClass0H1 r1;
        C020609t r13 = new C020609t(1);
        this.A01 = r13;
        this.A06 = r8;
        this.A0A = r12;
        r13.setStyle(Paint.Style.STROKE);
        r13.setStrokeCap(cap);
        r13.setStrokeJoin(join);
        r13.setStrokeMiter(f);
        this.A08 = new AnonymousClass0H0(r11.A00);
        this.A09 = new AnonymousClass0H1(r9.A00);
        if (r10 == null) {
            r1 = null;
        } else {
            r1 = new AnonymousClass0H1(r10.A00);
        }
        this.A07 = r1;
        this.A0B = new ArrayList(list.size());
        this.A0D = new float[list.size()];
        for (int i = 0; i < list.size(); i++) {
            this.A0B.add(new AnonymousClass0H1(((AbstractC08110ab) list.get(i)).A00));
        }
        r12.A03(this.A08);
        r12.A03(this.A09);
        for (int i2 = 0; i2 < this.A0B.size(); i2++) {
            r12.A03((AnonymousClass0QR) this.A0B.get(i2));
        }
        AnonymousClass0QR r0 = this.A07;
        if (r0 != null) {
            r12.A03(r0);
        }
        this.A08.A07.add(this);
        this.A09.A07.add(this);
        for (int i3 = 0; i3 < list.size(); i3++) {
            ((AnonymousClass0QR) this.A0B.get(i3)).A07.add(this);
        }
        AnonymousClass0QR r02 = this.A07;
        if (r02 != null) {
            r02.A07.add(this);
        }
    }

    @Override // X.AbstractC12480hz
    public void A5q(AnonymousClass0SF r3, Object obj) {
        AnonymousClass0QR r0;
        if (obj == AbstractC12810iX.A0S) {
            r0 = this.A08;
        } else if (obj == AbstractC12810iX.A0G) {
            r0 = this.A09;
        } else if (obj == AbstractC12810iX.A00) {
            AnonymousClass0QR r1 = this.A00;
            if (r1 != null) {
                this.A0A.A0O.remove(r1);
            }
            if (r3 == null) {
                this.A00 = null;
                return;
            }
            AnonymousClass0Gu r02 = new AnonymousClass0Gu(r3, null);
            this.A00 = r02;
            r02.A07.add(this);
            this.A0A.A03(this.A00);
            return;
        } else {
            return;
        }
        r0.A08(r3);
    }

    @Override // X.AbstractC12860ig
    public void A9C(Canvas canvas, Matrix matrix, int i) {
        float f;
        float floatValue;
        float[] fArr = (float[]) AnonymousClass0UV.A03.get();
        fArr[0] = 0.0f;
        fArr[1] = 0.0f;
        fArr[2] = 37394.73f;
        fArr[3] = 39575.234f;
        matrix.mapPoints(fArr);
        if (fArr[0] != fArr[2] && fArr[1] != fArr[3]) {
            AnonymousClass0H0 r2 = (AnonymousClass0H0) this.A08;
            AnonymousClass0U8 AC6 = r2.A06.AC6();
            AnonymousClass0MI.A00();
            int A09 = (int) ((((((float) i) / 255.0f) * ((float) r2.A09(AC6, r2.A01()))) / 100.0f) * 255.0f);
            Paint paint = this.A01;
            int i2 = 0;
            paint.setAlpha(Math.max(0, Math.min(255, A09)));
            paint.setStrokeWidth(((AnonymousClass0H1) this.A09).A09() * AnonymousClass0UV.A02(matrix));
            if (paint.getStrokeWidth() > 0.0f) {
                List list = this.A0B;
                if (!list.isEmpty()) {
                    float A02 = AnonymousClass0UV.A02(matrix);
                    for (int i3 = 0; i3 < list.size(); i3++) {
                        float[] fArr2 = this.A0D;
                        float floatValue2 = ((Number) ((AnonymousClass0QR) list.get(i3)).A03()).floatValue();
                        fArr2[i3] = floatValue2;
                        float f2 = 0.1f;
                        if (i3 % 2 == 0) {
                            f2 = 1.0f;
                        }
                        if (floatValue2 < f2) {
                            fArr2[i3] = f2;
                        }
                        fArr2[i3] = fArr2[i3] * A02;
                    }
                    AnonymousClass0QR r0 = this.A07;
                    if (r0 == null) {
                        floatValue = 0.0f;
                    } else {
                        floatValue = A02 * ((Number) r0.A03()).floatValue();
                    }
                    paint.setPathEffect(new DashPathEffect(this.A0D, floatValue));
                }
                AnonymousClass0MI.A00();
                AnonymousClass0QR r02 = this.A00;
                if (r02 != null) {
                    paint.setColorFilter((ColorFilter) r02.A03());
                }
                while (true) {
                    List list2 = this.A0C;
                    if (i2 >= list2.size()) {
                        break;
                    }
                    AnonymousClass0N7 r9 = (AnonymousClass0N7) list2.get(i2);
                    if (r9.A00 != null) {
                        C07950aL r8 = r9.A00;
                        if (r8 != null) {
                            Path path = this.A02;
                            path.reset();
                            List list3 = r9.A01;
                            int size = list3.size();
                            while (true) {
                                size--;
                                if (size < 0) {
                                    break;
                                }
                                path.addPath(((AbstractC12850if) list3.get(size)).AF0(), matrix);
                            }
                            PathMeasure pathMeasure = this.A04;
                            pathMeasure.setPath(path, false);
                            float length = pathMeasure.getLength();
                            while (pathMeasure.nextContour()) {
                                length += pathMeasure.getLength();
                            }
                            float floatValue3 = (((Number) r8.A01.A03()).floatValue() * length) / 360.0f;
                            float floatValue4 = ((((Number) r8.A02.A03()).floatValue() * length) / 100.0f) + floatValue3;
                            float floatValue5 = ((((Number) r8.A00.A03()).floatValue() * length) / 100.0f) + floatValue3;
                            float f3 = 0.0f;
                            for (int size2 = list3.size() - 1; size2 >= 0; size2--) {
                                Path path2 = this.A03;
                                path2.set(((AbstractC12850if) list3.get(size2)).AF0());
                                path2.transform(matrix);
                                pathMeasure.setPath(path2, false);
                                float length2 = pathMeasure.getLength();
                                float f4 = 1.0f;
                                if (floatValue5 > length) {
                                    float f5 = floatValue5 - length;
                                    if (f5 < f3 + length2 && f3 < f5) {
                                        if (floatValue4 > length) {
                                            f = (floatValue4 - length) / length2;
                                        } else {
                                            f = 0.0f;
                                        }
                                        f4 = Math.min(f5 / length2, 1.0f);
                                        AnonymousClass0UV.A04(path2, f, f4, 0.0f);
                                        canvas.drawPath(path2, paint);
                                        f3 += length2;
                                    }
                                }
                                float f6 = f3 + length2;
                                if (f6 >= floatValue4 && f3 <= floatValue5) {
                                    if (f6 > floatValue5 || floatValue4 >= f3) {
                                        f = floatValue4 < f3 ? 0.0f : (floatValue4 - f3) / length2;
                                        if (floatValue5 <= f6) {
                                            f4 = (floatValue5 - f3) / length2;
                                        }
                                        AnonymousClass0UV.A04(path2, f, f4, 0.0f);
                                    }
                                    canvas.drawPath(path2, paint);
                                }
                                f3 += length2;
                            }
                        }
                    } else {
                        Path path3 = this.A02;
                        path3.reset();
                        int size3 = r9.A01.size();
                        while (true) {
                            size3--;
                            if (size3 < 0) {
                                break;
                            }
                            path3.addPath(((AbstractC12850if) r9.A01.get(size3)).AF0(), matrix);
                        }
                        AnonymousClass0MI.A00();
                        canvas.drawPath(path3, paint);
                    }
                    AnonymousClass0MI.A00();
                    i2++;
                }
            }
        }
        AnonymousClass0MI.A00();
    }

    @Override // X.AbstractC12860ig
    public void AAy(Matrix matrix, RectF rectF, boolean z) {
        Path path = this.A02;
        path.reset();
        int i = 0;
        while (true) {
            List list = this.A0C;
            if (i < list.size()) {
                AnonymousClass0N7 r2 = (AnonymousClass0N7) list.get(i);
                for (int i2 = 0; i2 < r2.A01.size(); i2++) {
                    path.addPath(((AbstractC12850if) r2.A01.get(i2)).AF0(), matrix);
                }
                i++;
            } else {
                RectF rectF2 = this.A05;
                path.computeBounds(rectF2, false);
                float A09 = ((AnonymousClass0H1) this.A09).A09() / 2.0f;
                rectF2.set(rectF2.left - A09, rectF2.top - A09, rectF2.right + A09, rectF2.bottom + A09);
                rectF.set(rectF2);
                rectF.set(rectF.left - 1.0f, rectF.top - 1.0f, rectF.right + 1.0f, rectF.bottom + 1.0f);
                AnonymousClass0MI.A00();
                return;
            }
        }
    }

    @Override // X.AbstractC12030hG
    public void AYB() {
        this.A06.invalidateSelf();
    }

    @Override // X.AbstractC12480hz
    public void Aan(C06430To r1, C06430To r2, List list, int i) {
        AnonymousClass0U0.A01(this, r1, r2, list, i);
    }

    @Override // X.AbstractC12470hy
    public void Aby(List list, List list2) {
        AnonymousClass0N7 r5 = null;
        C07950aL r6 = null;
        for (int size = list.size() - 1; size >= 0; size--) {
            AbstractC12470hy r2 = (AbstractC12470hy) list.get(size);
            if (r2 instanceof C07950aL) {
                C07950aL r22 = (C07950aL) r2;
                if (r22.A03 == AnonymousClass0J5.INDIVIDUALLY) {
                    r6 = r22;
                }
            }
        }
        if (r6 != null) {
            r6.A05.add(this);
        }
        for (int size2 = list2.size() - 1; size2 >= 0; size2--) {
            AbstractC12470hy r3 = (AbstractC12470hy) list2.get(size2);
            if (r3 instanceof C07950aL) {
                C07950aL r23 = (C07950aL) r3;
                if (r23.A03 == AnonymousClass0J5.INDIVIDUALLY) {
                    if (r5 != null) {
                        this.A0C.add(r5);
                    }
                    r5 = new AnonymousClass0N7(r23);
                    r23.A05.add(this);
                }
            }
            if (r3 instanceof AbstractC12850if) {
                if (r5 == null) {
                    r5 = new AnonymousClass0N7(r6);
                }
                r5.A01.add(r3);
            }
        }
        if (r5 != null) {
            this.A0C.add(r5);
        }
    }
}
