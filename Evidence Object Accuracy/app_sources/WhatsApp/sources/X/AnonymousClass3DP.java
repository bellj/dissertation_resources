package X;

import android.content.ActivityNotFoundException;
import com.whatsapp.R;
import com.whatsapp.community.CommunityMembersActivity;
import com.whatsapp.jid.UserJid;

/* renamed from: X.3DP  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3DP {
    public boolean A00;
    public final C14900mE A01;
    public final C15570nT A02;
    public final AnonymousClass3EZ A03;
    public final CommunityMembersActivity A04;
    public final C15550nR A05;
    public final C15610nY A06;
    public final AnonymousClass198 A07;
    public final C254219i A08;

    public AnonymousClass3DP(C14900mE r1, C15570nT r2, AnonymousClass3EZ r3, CommunityMembersActivity communityMembersActivity, C15550nR r5, C15610nY r6, AnonymousClass198 r7, C254219i r8) {
        this.A01 = r1;
        this.A02 = r2;
        this.A07 = r7;
        this.A04 = communityMembersActivity;
        this.A05 = r5;
        this.A06 = r6;
        this.A08 = r8;
        this.A03 = r3;
    }

    public final void A00(UserJid userJid, boolean z) {
        if (userJid == null) {
            this.A01.A07(R.string.group_add_contact_failed, 0);
            return;
        }
        try {
            this.A04.startActivityForResult(this.A08.A00(this.A05.A0B(userJid), userJid, z), 10);
            this.A07.A02(z, 8);
        } catch (ActivityNotFoundException unused) {
            this.A01.A07(R.string.activity_not_found, 0);
        }
    }
}
