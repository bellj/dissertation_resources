package X;

import android.util.Pair;

/* renamed from: X.2EF  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2EF {
    public final Pair A00;
    public final Object A01;

    public AnonymousClass2EF(Pair pair, Object obj) {
        this.A01 = obj;
        this.A00 = pair;
    }
}
