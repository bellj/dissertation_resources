package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5f7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119795f7 extends AnonymousClass1ZX {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(1);
    public AnonymousClass1ZR A00;
    public C119755f3 A01;
    public String A02;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // X.AnonymousClass1ZP
    public void A01(AnonymousClass102 r4, AnonymousClass1V8 r5, int i) {
        String A0W = C117295Zj.A0W(r5, "display-state");
        if (TextUtils.isEmpty(A0W)) {
            A0W = "ACTIVE";
        }
        this.A06 = A0W;
        this.A08 = r5.A0I("merchant-id", null);
        super.A02 = r5.A0I("business-name", null);
        this.A03 = r5.A0I("country", null);
        this.A04 = r5.A0I("credential-id", null);
        this.A00 = AnonymousClass1ZS.A00(r5.A0I("vpa", null), "upiHandle");
        this.A02 = r5.A0I("vpa-id", null);
        AnonymousClass1V8 A0E = r5.A0E("bank");
        if (A0E != null) {
            C119755f3 r0 = new C119755f3();
            this.A01 = r0;
            r0.A01(r4, A0E, i);
        }
    }

    @Override // X.AnonymousClass1ZP
    public void A02(List list, int i) {
        throw C12980iv.A0u("PAY: IndiaUpiMerchantData toNetwork unsupported");
    }

    @Override // X.AnonymousClass1ZP
    public String A03() {
        return A0B().toString();
    }

    @Override // X.AnonymousClass1ZP
    public void A04(String str) {
        if (str != null) {
            try {
                A0C(C13000ix.A05(str));
            } catch (JSONException unused) {
                Log.e("PAY: IndiaUpiMerchantMethodData fromDBString threw JSONException");
            }
        }
    }

    @Override // X.AnonymousClass1ZY
    public AbstractC28901Pl A05() {
        return new AnonymousClass1ZW(C17930rd.A00("IN"), this, this.A04, this.A07, this.A08, super.A02, this.A0D, this.A0E);
    }

    @Override // X.AnonymousClass1ZY
    public LinkedHashSet A09() {
        return new LinkedHashSet(Collections.singletonList(C30771Yt.A05));
    }

    @Override // X.AnonymousClass1ZX
    public JSONObject A0B() {
        JSONObject A0B = super.A0B();
        try {
            AnonymousClass1ZR r1 = this.A00;
            if (!AnonymousClass1ZS.A03(r1)) {
                C117315Zl.A0U(r1, "vpaHandle", A0B);
            }
            String str = this.A02;
            if (str != null) {
                A0B.put("vpaId", str);
            }
            if (this.A01 != null) {
                JSONObject A0a = C117295Zj.A0a();
                AnonymousClass1ZR r12 = ((AbstractC30851Zb) this.A01).A02;
                if (r12 != null) {
                    C117315Zl.A0U(r12, "accountNumber", A0a);
                }
                AnonymousClass1ZR r13 = ((AbstractC30851Zb) this.A01).A01;
                if (r13 != null) {
                    C117315Zl.A0U(r13, "bankName", A0a);
                }
                A0B.put("bank", A0a);
                return A0B;
            }
        } catch (JSONException unused) {
            Log.e("PAY: IndiaUpiMerchantMethodData toJSONObject threw JSONException");
        }
        return A0B;
    }

    @Override // X.AnonymousClass1ZX
    public void A0C(JSONObject jSONObject) {
        super.A0C(jSONObject);
        this.A00 = AnonymousClass1ZS.A00(jSONObject.optString("vpaHandle"), "upiHandle");
        this.A02 = jSONObject.optString("vpaId");
        JSONObject optJSONObject = jSONObject.optJSONObject("bank");
        if (optJSONObject != null) {
            C119755f3 r2 = new C119755f3();
            this.A01 = r2;
            ((AbstractC30851Zb) r2).A02 = AnonymousClass1ZS.A00(optJSONObject.optString("accountNumber", null), "bankAccountNumber");
            ((AbstractC30851Zb) this.A01).A01 = AnonymousClass1ZS.A00(optJSONObject.optString("bankName"), "bankName");
        }
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("IndiaUpiMerchantMethodData{version=");
        A0k.append(1);
        A0k.append(", vpaId='");
        A0k.append(this.A02);
        A0k.append('\'');
        A0k.append(", vpaHandle=");
        A0k.append(this.A00);
        A0k.append("} ");
        return C12960it.A0d(super.toString(), A0k);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A08);
        parcel.writeString(super.A02);
        parcel.writeString(this.A06);
        parcel.writeString(this.A03);
        parcel.writeString(this.A04);
        parcel.writeParcelable(this.A00, i);
        parcel.writeString(this.A02);
        parcel.writeParcelable(this.A01, i);
    }
}
