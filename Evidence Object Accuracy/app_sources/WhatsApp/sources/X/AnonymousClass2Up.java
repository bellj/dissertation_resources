package X;

import android.content.Intent;
import com.whatsapp.R;

/* renamed from: X.2Up  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Up {
    public final int A00;
    public final Intent A01;
    public final String A02;

    public AnonymousClass2Up(Intent intent) {
        this.A01 = intent;
        this.A02 = null;
        this.A00 = 0;
    }

    public AnonymousClass2Up(Intent intent, String str) {
        this.A01 = intent;
        this.A02 = str;
        this.A00 = R.drawable.clear;
    }
}
