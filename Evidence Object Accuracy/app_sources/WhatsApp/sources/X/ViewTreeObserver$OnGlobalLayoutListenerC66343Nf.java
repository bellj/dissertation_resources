package X;

import android.view.View;
import android.view.ViewTreeObserver;
import com.whatsapp.conversation.ConversationListView;

/* renamed from: X.3Nf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC66343Nf implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ int A00;
    public final /* synthetic */ View A01;
    public final /* synthetic */ C15350n0 A02;
    public final /* synthetic */ ConversationListView A03;
    public final /* synthetic */ C64203Eq A04;

    public ViewTreeObserver$OnGlobalLayoutListenerC66343Nf(View view, C15350n0 r2, ConversationListView conversationListView, C64203Eq r4, int i) {
        this.A03 = conversationListView;
        this.A02 = r2;
        this.A04 = r4;
        this.A00 = i;
        this.A01 = view;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        ConversationListView conversationListView = this.A03;
        C12980iv.A1F(conversationListView, this);
        C15350n0 r2 = this.A02;
        C64203Eq r4 = this.A04;
        r2.A0V.add(r4.A09.A0z);
        conversationListView.A02();
        conversationListView.A09(this.A01, this.A00, r4.A06, r4.A01);
    }
}
