package X;

/* renamed from: X.1PW  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1PW {
    public final int A00;
    public final int A01;
    public final long A02;
    public final long A03;
    public final String A04;
    public final String A05;
    public final String A06;
    public final String A07;
    public final String A08;
    public final byte[] A09;

    public AnonymousClass1PW(String str, String str2, String str3, String str4, String str5, byte[] bArr, int i, int i2, long j, long j2) {
        this.A01 = i;
        this.A02 = j;
        this.A00 = i2;
        this.A09 = bArr;
        this.A05 = str;
        this.A07 = str2;
        this.A04 = str3;
        this.A06 = str4;
        this.A08 = str5;
        this.A03 = j2;
    }
}
